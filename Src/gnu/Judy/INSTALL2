# @(#) $Revision: 4.3 $ $Source: /judy/INSTALL2 $


=== QUICK INSTALLATION OF JUDY LIBRARY AND MANUAL ENTRIES ===

1.  ./configure
2.  make
3.  make install		# note: must be SUPERUSER for this step.

				# This installs /opt/Judy/* and symlinks to
				# files there from /usr/include/, /usr/lib/,
				# /usr/share/man/, and /usr/share/doc/Judy/.

(Installation done!  The rest is optional but recommended.)

cc -o interL src/apps/demo/interL.c -lJudy	# verifies Judy installed.
./interL			# simple interactive Judy example.
		-- OR --
cd src/apps/demo
run_demo			# interactive view, build, and run interL.

man Judy			# nroff -man version, or...
file:/opt/Judy/usr/share/doc/Judy/Judy_3x.htm	# from LOCAL Web browser.

Other demo programs:  See the end of this file.


=== DETAILS FOR THE DEVELOPER ===

* CONTENTS:  The Judy package's deliverable files include the following
  kinds of files:

  - object code libraries
  - one header file
  - manual entries (both HTML and nroff -man versions)
  - some documents

  These deliverable files are constructed in local platform-specific and
  flavor-specific subdirectories (under the top of the tree where the
  source tarchive is unpacked).


* CONFIGURE:  To prepare Judy for building one of three flavors, invoke
  one of the following commands:

    ./configure			# default flavor = "product", or,
    ./configure -f debug	# debuggable flavor, or,
    ./configure -f cov		# C-Cover flavor (requires C-Cover).

  If successful the configure script builds ./Makefile.


* BUILD:  Next say:

    make

  The Judy makefile emits announcement lines, so the following command
  is also appropriate to hide details while watching progress:

    make | tee make.out | grep ^====


* INSTALL:

    make install		# as superuser.

  The install target must be invoked as superuser.  It copies the
  ./<platform>/<flavor>/deliver/ tree to /opt/Judy/, then creates
  symlinks to files in that tree from locations below /usr/lib/,
  /usr/include/, /usr/share/man/, and /usr/share/doc/Judy/.


* OTHER TARGETS:  The Judy makefile contains many convenience targets in
  addition to the default "all" target, including:

    make uninstall		# delete /opt/Judy and symlinks.
    make clobber		# delete built files from local (untar) tree.
    make distclean		# revert to distribution status.
    make tarchive_src		# make a source tarchive.
    make tarchive_del		# make a deliverables tarchive.

  Additional optional targets may be found by viewing the Makefile.


WHAT'S NEXT?

* MANUAL ENTRIES:  After "make install" you might want to say "man Judy"
  to begin reading the (nroff -man) manual entries.  Note, there are
  also some HTML versions of the manual entries in
  /opt/Judy/usr/share/doc/Judy (also symlinked from
  /usr/share/doc/Judy/, and also in doc/ext/ in your untar location),
  such as /opt/Judy/usr/share/doc/Judy/Judy_3x.htm (the starting point).
  These HTML files might have to be placed in a browseable location to
  be used, or try this URL in your local browser:

    file:/opt/Judy/usr/share/doc/Judy/Judy_3x.htm

* DEMOS:  You'll find some simple demo programs, a README file, and a
  Makefile in /opt/Judy/usr/share/doc/Judy/demo (also symlinked from
  /usr/share/doc/Judy/demo, and also in src/apps/demo/ in your untar
  location).

  Note:  To make the demos you must be superuser or else work in your
  untar location or in a private directory containing a copy of the
  files.  For example:

    cd src/apps/demo
    run_demo

  Furthermore, making demos using the "constructed demo Makefile"
  requires using a "constructed files location" such as:

    cd src/linux_ia32/product/deliver/usr/share/doc/Judy/demo
    make
    interSL
