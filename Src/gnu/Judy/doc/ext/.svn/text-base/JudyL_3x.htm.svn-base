<HTML>
<HEAD>
<!-- @(#) $Revision: 4.53 $ $Source: /judy/doc/ext/JudyL_3x.htm $ --->
<TITLE>JudyL(3X)</TITLE>
</HEAD>

<BODY>
<TABLE border=0 width="100%"><TR>
<TD width="40%" align="left">JudyL(3X)</TD>
<TD width="10%" align="center">     </TD>
<TD width="40%" align="right">JudyL(3X)</TD>
</TR></TABLE>

<P>
<DL>

<!----------------->
<DT><B>NAME</B></DT>
<DD>
JudyL macros -
C library for creating and accessing a dynamic array of words, using
any value of a word as an index

<!----------------->
<P>
<DT><B>SYNOPSIS</B></DT>
<DD>
<B><PRE>
cc [flags] <I>sourcefiles</I> -lJudy

#include &lt;Judy.h&gt;

int      Rc_int;                          // return code - integer
Word_t   Rc_word;                         // return code - unsigned word
Word_t   Index, Index1, Index2, Nth;
Word_t * PValue;                          // pointer to return value

Pvoid_t PJLArray = (Pvoid_t) NULL;        // initialize JudyL array


<A href="#JLI" >JLI</A>( PValue,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLIns">JudyLIns()</A>
<A href="#JLD" >JLD</A>( Rc_int,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLDel">JudyLDel()</A>
<A href="#JLG" >JLG</A>( PValue,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLGet">JudyLGet()</A>
<A href="#JLC"  >JLC</A>( Rc_word, PJLArray, Index1, Index2); // <A href="JudyL_funcs_3x.htm#JudyLCount">JudyLCount()</A>
<A href="#JLBC"  >JLBC</A>(PValue,  PJLArray, Nth, Index);     // <A href="JudyL_funcs_3x.htm#JudyLByCount">JudyLByCount()</A>
<A href="#JLFA" >JLFA</A>(Rc_word, PJLArray);                 // <A href="JudyL_funcs_3x.htm#JudyLFreeArray">JudyLFreeArray()</A>
<A href="#JLMU"   >JLMU</A>(Rc_word, PJLArray);                 // <A href="JudyL_funcs_3x.htm#JudyLMemUsed">JudyLMemUsed()</A>
<A href="#JLF" >JLF</A>( PValue,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLFirst">JudyLFirst()</A>
<A href="#JLN" >JLN</A>( PValue,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLNext">JudyLNext()</A>
<A href="#JLL" >JLL</A>( PValue,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLLast">JudyLLast()</A>
<A href="#JLP" >JLP</A>( PValue,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLPrev">JudyLPrev()</A>
<A href="#JLFE">JLFE</A>(Rc_int,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLFirstEmpty">JudyLFirstEmpty()</A>
<A href="#JLNE" >JLNE</A>(Rc_int,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLNextEmpty">JudyLNextEmpty()</A>
<A href="#JLLE" >JLLE</A>(Rc_int,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLLastEmpty">JudyLLastEmpty()</A>
<A href="#JLPE" >JLPE</A>(Rc_int,  PJLArray, Index);          // <A href="JudyL_funcs_3x.htm#JudyLPrevEmpty">JudyLPrevEmpty()</A>
</PRE></B>

<!----------------->
<P>
<DT><B>DESCRIPTION</B></DT>
<DD>
A JudyL array is the equivalent of an array of word-sized values.
A value is addressed by an <B>Index</B> (key).
The array may be sparse, and the <B>Index</B> may be any word-sized number.
Memory to support the array is allocated as index/value pairs are inserted,
and released as index/value pairs are deleted.
<P>
As with an ordinary array, there are no duplicate indexes in a JudyL array.
<P>
The value may be used as a scalar, or a pointer to a structure or block of data
(or even another Judy array).
<P>
A JudyL array is allocated with a <B>NULL</B> pointer
<PRE>
Pvoid_t PJLArray = (Pvoid_t) NULL;
</PRE>

<P>
Using the macros described here, rather than the
<A href="JudyL_funcs_3x.htm">JudyL function calls</A>,
the default error handling sends a
message to the standard error and terminates the program with <B>exit(1)</B>.
For other error handling methods, see the
<A href="#JLERR">ERRORS</A> section.

<P>
Because the macro forms are faster and have a simpler error
handling interface than the equivalent
<A href="JudyL_funcs_3x.htm">JudyL functions</A>,
they are the preferred way of calling the JudyL functions.

<P>
<DL>
<DT><A name="JLI"><B>JLI(PValue, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLIns">JudyLIns()</A></DT>
<DD>
Insert an <B>Index</B> and value in the JudyL array <B>PJLArray</B>.
If the <B>Index</B> is successfully inserted,
the value is initialized to 0. If the <B>Index</B> was already present,
the value is not modified.
<P>
Return <B>PValue</B> pointing to <B>Index</B>'s value.
Your program must use this pointer to modify the value,
for example:
<PRE>
*PValue = 1234;
</PRE>
<P>
<B>Note</B>:
<B>JLI()</B> and <B>JLD()</B> reorganize the JudyL array.
Therefore,
pointers returned from previous <B>JudyL</B> calls become
invalid and must be reacquired.

<P>
<DT><A name="JLD"><B>JLD(Rc_int, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLDel">JudyLDel()</A></DT>
<DD>
Delete the <B>Index</B>/value pair from the JudyL array.
<P>
Return <B>Rc_int</B> set to 1 if successful.
Return <B>Rc_int</B> set to 0 if <B>Index</B> was not present.

<P>
<DT><A name="JLG"><B>JLG(PValue, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLGet">JudyLGet()</A></DT>
<DD>
Get the pointer to <B>Index</B>'s value.
<P>
Return <B>PValue</B> pointing to <B>Index</B>'s value.
Return <B>PValue</B> set to <B>NULL</B> if the <B>Index</B> was not present.

<P>
<DT><A name="JLC"><B>JLC(Rc_word, PJLArray, Index1, Index2)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLCount">JudyLCount()</A></DT>
<DD>
Count the number of indexes present in the JudyL array <B>PJLArray</B> between
<B>Index1</B> and <B>Index2</B> (inclusive).
<P>
Return <B>Rc_word</B> set to the count.
A return value of 0 can be valid as a count.
<P>
To count all indexes present in a JudyL array, use:
<PRE>
JLC(Rc_word, PJLArray, 0, -1);
</PRE>

<P>
<DT><A name="JLBC"><B>JLBC(PValue, PJLArray, Nth, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLByCount">JudyLByCount()</A></DT>
<DD>
Locate the <B>Nth</B> index that is present in the JudyL array
<B>PJLArray</B> (<B>Nth</B> = 1 returns the first index present).
<P>
Return <B>PValue</B> pointing to its value and <B>Index</B>
set to the <B>Nth</B> index if found, otherwise return
<B>PValue</B> set to <B>NULL</B> (the value of <B>Index</B>
contains no useful information).

<P>
<DT><A name="JLFA"><B>JLFA(Rc_word, PJLArray)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLFreeArray">JudyLFreeArray()</A></DT>
<DD>
Given a pointer to a JudyL array, free the entire array (much faster
than using a
<B>JLN()</B>, <B>JLD()</B> loop).
<P>
Return <B>Rc_word</B> set to the number of bytes freed and <B>PJLArray</B>
set to <B>NULL</B>.
<P>
<DT><A name="JLMU"><B>JLMU(Rc_word, PJLArray)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLMemUsed">JudyLMemUsed()</A></DT>
<DD>
Return <B>Rc_word</B> set to the number of bytes of memory currently in use
by <B>PJLArray</B>.
This is a very fast routine, and may be used after
a <B>JLI()</B> or <B>JLD()</B> call with little performance impact.
<P>
<DT><B>JudyL Search Functions</B></DT>
<DD>
<B>JLF()</B>, <B>JLN()</B>, <B>JLL()</B>, <B>JLP()</B>
allow you to search for indexes
in the array.
You may search inclusively or exclusively,
in either forward or reverse directions.
If successful,
<B>Index</B> is returned set to the found index, and
<B>PValue</B> is returned set to a pointer to <B>Index</B>'s value.
If unsuccessful,
<B>PValue</B> is returned set to <B>NULL</B>,
and <B>Index</B> contains no useful information.
<B>PValue</B> must be tested for non-<B>NULL</B> prior
to using <B>Index</B>,
since a search failure is possible.
<P>
<B>JLFE()</B>, <B>JLNE()</B>, <B>JLLE()</B>, <B>JLPE()</B> allow you to search for
indexes that are not present ("empty") in the array.
You may search inclusively or exclusively,
in either forward or reverse directions.
If successful,
<B>Index</B> is returned set to a not present ("empty") index, and
<B>Rc_int</B> is returned set to 1.
If unsuccessful,
<B>Rc_int</B> is returned set to 0, and
and <B>Index</B> contains no useful information.
<B>Rc_int</B> must be checked prior to using <B>Index</B>,
since a search failure is possible.

<P>
<DT><A name="JLF"><B>JLF(PValue, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLFirst">JudyLFirst()</A></DT>
<DD>
Search (inclusive) for the first index present that is equal to or greater than the
passed <B>Index</B>.
(Start with <B>Index</B> = 0 to find the first index in the array.)
<B>JLF()</B> is typically used to <I>begin</I> a sorted-order scan of
the indexes present in a JudyL array.

<P>
<DT><A name="JLN"><B>JLN(PValue, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLNext">JudyLNext()</A></DT>
<DD>
Search (exclusive) for the next index present that is greater than the passed
<B>Index</B>.
<B>JLN()</B> is typically used to <I>continue</I> a sorted-order scan of
the indexes present in a JudyL array, or to locate a "neighbor" of a given
index.

<P>
<DT><A name="JLL"><B>JLL(PValue, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLLast">JudyLLast()</A></DT>
<DD>
Search (inclusive) for the last index present that is equal
to or less than the passed <B>Index</B>.
(Start with <B>Index</B> = -1, that is, all ones, to find
the last index in the array.)
<B>JLL()</B> is typically used to <I>begin</I> a reverse-sorted-order
scan of the indexes present in a JudyL array.

<P>
<DT><A name="JLP"><B>JLP(PValue, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLPrev">JudyLPrev()</A></DT>
<DD>
Search (exclusive) for the previous index present that is less than the
passed <B>Index</B>.
<B>JLP()</B> is typically used to <I>continue</I> a reverse-sorted-order
scan of the indexes present in a JudyL array, or to locate a "neighbor" of
a given index.

<P>
<DT><A name="JLFE"><B>JLFE(Rc_int, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLFirstEmpty">JudyLFirstEmpty()</A></DT>
<DD>
Search (inclusive) for the first index absent that is equal to or greater than the passed
<B>Index</B>.
(Start with <B>Index</B> = 0 to find the first index absent in the array.)

<P>
<DT><A name="JLNE"><B>JLNE(Rc_int, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLNextEmpty">JudyLNextEmpty()</A></DT>
<DD>
Search (exclusive) for the next index absent that is greater than the passed <B>Index</B>.

<P>
<DT><A name="JLLE"><B>JLLE(Rc_int, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLLastEmpty">JudyLLastEmpty()</A></DT>
<DD>
Search (inclusive) for the last index absent that is equal to or less than
the passed <B>Index</B>.
(Start with <B>Index</B> = -1, that is, all ones, to find the last index absent
in the array.)

<P>
<DT><A name="JLPE"><B>JLPE(Rc_int, PJLArray, Index)</B></A> // <A href="JudyL_funcs_3x.htm#JudyLPrevEmpty">JudyLPrevEmpty()</A></DT>
<DD>
Search (exclusive) for the previous index absent that is less than the passed
<B>Index</B>.

<P>
<DT><B>Multi-dimensional JudyL Arrays</B></DT>
<DD>
Storing in a JudyL (or JudySL) array a value that is actually a pointer
to another JudyL (or JudySL) array supports dynamic multi-dimensional
arrays, or similarly, arrays where one or more of the keys/indexes is
longer than one word and treated as a series of words.
Multi-dimensional arrays (trees) built using JudyL (or JudySL) arrays
(as the branch nodes) are usually very fast and efficient.
When the dimensionality (and/or length of each key/index) of the array
is fixed, the implementation is straightforward and can be thought of as:
<PRE>
Array[dim1, dim2, dim3, dim4];
</PRE>
<P>
However, for applications where the tree is dynamic, such as when a
JudyL (or JudySL) value (subsidiary pointer) points to a user-defined
node (that is, it "escapes" from the hierarchy of JudyL/JudySL arrays)
because the number of dimensions (and/or the length of one or more
keys/indexes) is dynamic, the pointer must be recognizable as pointing
to such a node rather than another Judy array.
A value of 4 in the least significant 3 bits of a JudyL (or JudySL)
array pointer indicates that the pointer is not in fact to a subsidiary
Judy array, but rather it points (with the least three bits masked off)
to a user-defined location.
The <B>Judy.h</B> header file defines two values:  <B>JLAP_MASK</B> to
mask the last 3 bits of the pointer, and <B>JLAP_INVALID</B> that has
the value 0x4 for checking the value of the pointer (<B>JLAP_INVALID ==
0x4</B>).
<P>
The following example code segment can be used to determine whether or
not a pointer points to another JudyL or JudySL array:
<P>
<PRE>
// Obtain a pointer to a value:

    JLG (PValue, PJLArray, Index1);

// Check if it is a JudyL/JudySL array pointer:

    if ((*PValue &amp; JLAP_MASK) == JLAP_INVALID) // not a Judy array pointer.
    {
       UPointer = (UPointer_t) (*PValue &amp; ~JLAP_MASK);  // mask and cast.
       printf("User object pointer is 0x%lx\n", (Word_t) UPointer);
       ...
    }
    else         // a JudyL array pointer, traverse through it:
    {
       Pvoid_t PJLArray1 = (Pvoid_t) *PValue;
       JLG(PValue1, PJLArray1, Index2);
       ...
    }
</PRE>
<P>
Note:  This works because <I>malloc</I>() guarantees to return a pointer
with the least 3 bits == 0x0.
You must add <B>JLAP_INVALID</B> to the pointer, and remove it before
using the pointer, to note that it is not a pointer to a JudyL array.
</DL>

<!----------------->
<P>
<DT><A name="JLERR"><B>ERRORS</B></A></DT>
<DD>
There are two categories of Judy error returns:
<BR>
1) User programming errors (bugs) such as memory corruption or
invalid pointers.
<BR>
2) Out-of-memory (<I>malloc</I>() failure) when modifying a JudyL array with <B>JLI()</B>
or <B>JLD()</B>.
<P>There are three methods of handling errors when using the macros:
<BR>
1) Default error handling.
<BR>
2) User-specified <B>JUDYERROR()</B> macro.
<BR>
3) Disable macro error handling.
<DL>
<P>
<DT><B>Default Error Handling Method</B></DT>
<DD>
The default is to print error messages to <B>stderr</B>, for example:
<P><PRE>
File 'YourCfile.c', line 1234: JudyLIns(), JU_ERRNO_* == 2, ID == 321
</PRE>
This indicates that an error occurred in a <B>JLI()</B> call at line 1234 in 'YourCfile.c'.
JU_ERRNO_* == 2 is JU_ERRNO_NOMEM (as defined in the Judy.h file).
The ID number indicates the Judy source line number where the error was detected.
<P>
Your program then terminates with an <B>exit(1)</B>.

<P>
<DT><B>User-Specified JUDYERROR() Macro Method</B> </DT>
<DD>
<P>
The <B>JUDYERROR()</B> macro provides flexibility for handling error returns
as needed to suit your program while still accessing JudyL arrays using macros
instead of function calls.  You can modify <B>JUDYERROR()</B> to distinguish between
the two types of errors (described above), and explicitly test for the remaining
JU_ERRNO_NOMEM errors possible in your program.
<P>
<PRE>
// This is an example of JudyL macro API to continue when out of memory.

#ifndef JUDYERROR_NOTEST
#include &lt;stdio.h&gt;  // needed for fprintf()

// This is the macro that the Judy macro APIs use for return codes of -1:

#define JUDYERROR(CallerFile, CallerLine, JudyFunc, JudyErrno, JudyErrID) \
{                                                                         \
    if ((JudyErrno) != JU_ERRNO_NOMEM) /* ! a malloc() failure */         \
    {                                                                     \
        (void) fprintf(stderr, "File '%s', line %d: %s(), "               \
            "JU_ERRNO_* == %d, ID == %d\n",                               \
            CallerFile, CallerLine,                                       \
            JudyFunc, JudyErrno, JudyErrID);                              \
        exit(1);                                                          \
    }                                                                     \
}
#endif // JUDYERROR_NOTEST not defined
</PRE>
This error handling macro must be included before the <B>#include &lt;Judy.h&gt;</B>
statement in your program.

<P>
<DT><B>Disable Macro Error Handling</B> </DT>
<DD>
<P>
When your program is "bug free",
the only errors occurring should be <I>malloc</I>(3) errors.
You can turn off error handling included in
the JudyL macros by using
<PRE>
#define JUDYERROR_NOTEST 1
</PRE>
(in your program code), or
<PRE>
cc -DJUDYERROR_NOTEST <I>sourcefile</I> -lJudy
</PRE>
(on your command line).
</DL>

<!----------------->
<P>
<DT><B>EXAMPLE</B></DT>
<DD>
Read a series of index/value pairs from the standard input, store each
in a JudyL array, and then print out in sorted order.

<P>
<PRE>
#include &lt;stdio.h&gt;
#include &lt;Judy.h&gt;

Word_t   Index;                     // array index
Word_t   Value;                     // array element value
Word_t * PValue;                    // pointer to array element value
int      Rc_int;                    // return code

Pvoid_t  PJLArray = (Pvoid_t) NULL; // initialize JudyL array

while (scanf("%lu %lu", &amp;Index, &amp;Value))
{
    JLI(PValue, PJLArray, Index);
    If (PValue == PJERR) goto process_malloc_failure;
    *PValue = Value;                 // store new value
}
// Next, visit all the stored indexes in sorted order, first ascending,
// then descending, and delete each index during the second pass.

Index = 0;
JLF(PValue, PJLArray, Index);
while (PValue != NULL)
{
    printf("%lu %lu\n", Index, *PValue));
    JLN(PValue, PJLArray, Index);
}

Index = -1;
JLL(PValue, PJLArray, Index);
while (PValue != NULL)
{
    printf("%lu %lu\n", Index, *PValue));

    JLD(Rc_int, PJLArray, Index);
    if (Rc_int == JERR) goto process_malloc_failure;

    JLP(PValue, PJLArray, Index);
}

</PRE>

<!----------------->
<P>
<DT><B>AUTHOR</B></DT>
<DD>
Judy was invented and implemented by Hewlett-Packard.

<!----------------->
<P>
<DT><B>SEE ALSO</B></DT>
<DD>
<A href="Judy_3x.htm">Judy(3X)</A>,
<A href="Judy1_3x.htm">Judy1(3X)</A>,
<A href="Judy1_funcs_3x.htm">Judy1_funcs(3X)</A>,
<A href="JudyL_funcs_3x.htm">JudyL_funcs(3X)</A>,
<A href="JudySL_3x.htm">JudySL(3X)</A>,
<A href="JudySL_funcs_3x.htm">JudySL_funcs(3X)</A>,
<BR>
<I>malloc</I>(3),
<BR>
the Judy website,
<A href="http://www.hp.com/go/judy/">
http://www.hp.com/go/judy/</A>,
for more information and Application Notes.
</DL>

</BODY>
</HTML>
