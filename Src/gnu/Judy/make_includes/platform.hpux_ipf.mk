# @(#) $Revision: 4.24 $ $Source: /judy/make_includes/platform.hpux_ipf.mk $

# Makefile fragment for Judy* for platform "hpux_ipf" (HP-UX on IPF).
# See platform.hpux_pa.mk for more comments.
#
# Note:  To build Judy non-chrooted (as a developer) on an hpux_ipf system with
# native compilers (not cross-compilers), such as mercedes.fc, you must support
# this makefile by creating fake "relocated native PA compilers" symlinks, used
# when building internal tools:
#
#   ! mkdir -p /CLO/BUILD_ENV/Exports
#   ! ln -s /opt/ansic/bin/cc /CLO/BUILD_ENV/Exports/cc
#   ! ln -s /opt/ansic/bin/cc /CLO/BUILD_ENV/Exports/cc64

include make_includes/platform.hpux.mk_sub

PLATFORM	= hpux_ipf

LIB_LIST	= lib_hpux_ipf_32a
LIBS_LIST	= lib_hpux_ipf_32a    lib_hpux_ipf_64a
LIBS_ALL_LIST	= lib_hpux_ipf_32a    lib_hpux_ipf_64a \
		  lib_hpux_ipf_32sl   lib_hpux_ipf_64sl
LIBS_PIC_LIST	= lib_hpux_ipf_32PICa lib_hpux_ipf_64PICa

LIB_ID_PATH	= /hpux32

# Note:  +DD32 is the default on IPF and takes less space and time for programs
# that do not need the larger address space:

CC_OPTS_LIB_HPUX_IPF_32 = +DD32
CC_OPTS_LIB_HPUX_IPF_64 = +DD64

# TBD:  Use +O2, same as hpux_pa, until we have an official IPF compiler and
# can resolve how it should be used.  Also, -Wall is replaced by +w1 on newer
# compilers, but that is very noisy for Judy (and might not be repairable), so
# use +w2 (default) instead; for now (020510), don't list it at all, to be
# compatible with older compilers still in the BE on judyn.
#
# Note:  For cov flavor only, ignore warning 67 about unrecognized pragmas
# until/unless covc finds a way to deal with this.  Newer IPF compilers no
# longer send cc -E processing through a separate cpp.ansi, meaning the pragmas
# cause many warnings during the cc -E phase invoked by covc.  Turning off all
# pragma warnings is not ideal, but much simpler than passing every $CC
# invocation through sed to hide them, and preferable to using +legacy_hpc.

CC_OPTS_product = +O2 -DJU_HPUX -DJU_HPUX_IPF -DJU_FLAVOR_PRODUCT
CC_OPTS_cov	= +O2 -DJU_HPUX -DJU_HPUX_IPF -DJU_FLAVOR_COV     +W67
CC_OPTS_debug	= -g  -DJU_HPUX -DJU_HPUX_IPF -DJU_FLAVOR_DEBUG

UNIFDEF_OPTS_PLATFORM = -t -DJU_HPUX -DJU_HPUX_IPF

# Use special native tools in IPF cross-compile BE for building internal tools
# (programs):
#
# Note:  The only tool actually needed for this purpose is cc.
#
# Note:  At least as of 020319, the Exports/ wrappers are not quite smart
# enough.  To do a 64-bit link requires naming cc64 explicitly, hence
# $CC_TOOL_SUFFIX below.

CCPATH_TOOL = /CLO/BUILD_ENV/Exports/cc$(CC_TOOL_SUFFIX)
