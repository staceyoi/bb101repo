# @(#) $Revision: 4.21 $ $Source: /judy/make_includes/platform.hpux_pa.mk $

# Makefile fragment for Judy* for platform "hpux_pa" (HP-UX on PA-RISC).

# Include HP-UX common settings:

include make_includes/platform.hpux.mk_sub

# Ensure $PLATFORM is set:

PLATFORM = hpux_pa

# Platform-specific library lists for HP-UX/PA for various "lib" targets:

LIB_LIST	= lib11_32a
LIBS_LIST	= lib11_32a    lib20_64a
LIBS_ALL_LIST	= lib11_32a    lib20_64a lib11_32sl lib20_32sl lib20_64sl
LIBS_PIC_LIST	= lib20_32PICa lib20_64PICa

# Platform-specific $CC options for different kinds of libraries:

CC_OPTS_LIB11_32 = +DA1.1
CC_OPTS_LIB20_32 = +DA2.0
CC_OPTS_LIB20_64 = +DD64

# Unfortunately there are CC_OPTS differences between both platforms AND
# flavors, so spell them out here by flavor, meaning this file cannot be
# flavor-ignorant:

CC_OPTS_product = -Wall +O2 -DJU_HPUX -DJU_HPUX_PA -DJU_FLAVOR_PRODUCT
CC_OPTS_cov	= -Wall +O2 -DJU_HPUX -DJU_HPUX_PA -DJU_FLAVOR_COV
CC_OPTS_debug	= -Wall -g  -DJU_HPUX -DJU_HPUX_PA -DJU_FLAVOR_DEBUG

# How to process a text (non-C) file for a specific platform:

UNIFDEF_OPTS_PLATFORM = -t  -DJU_HPUX -DJU_HPUX_PA

# When compiling a generic (once-only, not per-library) constructed tool
# (program) on a PA 2.0 box, a warning is issued unless this option is given:
#
# (On other platforms the macro is null.)

CC_OPTS_TOOL_GEN = +DA1.1

# Use normal tools for building internal tools (programs):
#
# Note:  The only tool actually needed for this purpose is cc.

CCPATH_TOOL = $(CCPATH)

# Waiver (hide) the following HP-UX-only, PIC-only warning from makelog by
# indenting it in cc output:
#
# On HP-UX, we do want to use +O3, although currently we do not (see
# make_includes/flavor.product.mk 4.1, although there's not much explanation in
# the delta logs, but see also later versions of that file), so keep this
# supporting code around although $PIC_SED is not used; and note, no one should
# redefine a Judy routine.  Note the tab in the sed script.

PIC_WARNING =	cc: warning 8006: Do not use optimization levels higher than 2 to generate a shared library if a user of that library may redefine a routine within that library (8006)

PIC_SED_VALUE =	| sed '/^$(PIC_WARNING)$$/s/^/	/'
