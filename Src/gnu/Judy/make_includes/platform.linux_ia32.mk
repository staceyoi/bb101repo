# @(#) $Revision: 4.21 $ $Source: /judy/make_includes/platform.linux_ia32.mk $

# Makefile fragment for Judy* for platform "linux_ia32".
# See platform.hpux_pa.mk for more comments.

# Include Linux common settings:

include make_includes/platform.linux.mk_sub

# Ensure $PLATFORM is set:

PLATFORM = linux_ia32

# Platform-specific library lists for Linux/IA32 for various "lib" targets:

LIB_LIST	= lib32a
LIBS_LIST	= lib32a
LIBS_ALL_LIST	= lib32a lib32so
LIBS_PIC_LIST	= lib32PICa

# Unfortunately there are CC_OPTS differences between both platforms AND
# flavors, so spell them out here by flavor, meaning this file cannot be
# flavor-ignorant:

CC_OPTS_product = -Wall -O2 -DJU_LINUX -DJU_LINUX_IA32 -DJU_FLAVOR_PRODUCT
CC_OPTS_cov	= -Wall -O2 -DJU_LINUX -DJU_LINUX_IA32 -DJU_FLAVOR_COV
CC_OPTS_debug	= -Wall -g  -DJU_LINUX -DJU_LINUX_IA32 -DJU_FLAVOR_DEBUG

UNIFDEF_OPTS_PLATFORM = -t  -DJU_LINUX -DJU_LINUX_IA32

# Judy is normally built for Linux on an i686 (Pentium Pro) system (see uname
# -m output), which requires no special compiler options.  To build for a
# "lesser" Intel system such as i586 (Pentium), consider adding a flag like:
#
#	CC_OPTS = -mcpu=i586
#
# As written above this won't work, there are details to be resolved.
