# @(#) $Revision: 4.15 $ $Source: /judy/make_includes/platform.linux_ipf.mk $

# Makefile fragment for Judy* for platform "linux_ipf".
# See platform.hpux_pa.mk for more comments.

# Include Linux common settings:

include make_includes/platform.linux.mk_sub

# Ensure $PLATFORM is set:

PLATFORM = linux_ipf

# Platform-specific library lists for Linux/IPF for various "lib" targets:

LIB_LIST	= lib64a
LIBS_LIST	= lib64a
LIBS_ALL_LIST	= lib64a lib64so
LIBS_PIC_LIST	= lib64PICa

# Unfortunately there are CC_OPTS differences between both platforms AND
# flavors, so spell them out here by flavor, meaning this file cannot be
# flavor-ignorant:

CC_OPTS_product = -Wall -O2 -DJU_LINUX -DJU_LINUX_IPF -DJU_FLAVOR_PRODUCT
CC_OPTS_cov	= -Wall -O2 -DJU_LINUX -DJU_LINUX_IPF -DJU_FLAVOR_COV
CC_OPTS_debug	= -Wall -g  -DJU_LINUX -DJU_LINUX_IPF -DJU_FLAVOR_DEBUG

UNIFDEF_OPTS_PLATFORM = -t  -DJU_LINUX -DJU_LINUX_IPF
