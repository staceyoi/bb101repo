# @(#) $Revision: 4.23 $ $Source: /judy/make_includes/platform.win_ia32.mk $

# Makefile fragment for Judy* for platform "win_ia32" (using MKS Toolkit and
# Visual Studio / VC++).
#
# Note:  Porting the Judy makefile from HP-UX and Linux to win_ia32 was harder
# than any previous port.  Ended up requiring BOTH MKS and VC++.  The VC++
# compiler and linker are better than MKS versions (and in fact we need them to
# compile at all), but nmake (in VC++) doesn't work well with a UNIX-style
# makefile (various problems), and since the makefile needs MKS anyway for ksh,
# might as well use its make command too, which works now (after some minor
# tweaking).

# See platform.hpux_pa.mk for more comments.  TBD:  Should common parts of
# platform.win_*.mk be combined in a new platform.win.mk_sub?

PLATFORM = win_ia32

# Commands to run, using full paths where appropriate to avoid ambiguity:
#
# Note:  MKS uses unusual path names.  Giving the "user" versions of these
# names to make(1), even with quote marks to hide spaces, seems to fail, so
# instead use "real" names (either way without *.exe suffixes, they are
# unnecessary in this context).

SHELL	 = /PROGRA~1/MKSTOO~1/mksnt/ksh

# Use VC++ compile tools rather than MKS compile tools:

# CCPATH = /PROGRA~1/MKSTOO~1/mksnt/cc
CCPATH	 = /PROGRA~1/MICROS~3/VC98/BIN/CL
# LDPATH = /PROGRA~1/MKSTOO~1/mksnt/cc
LDPATH	 = /PROGRA~1/MICROS~3/VC98/BIN/LINK

# Can't find an "ar" command in VC++ so use the MKS version:
#
# TBD:  Noted that in win_ipf cross-compile context the command to use was
# "LIB"; see win_ipf variation of this file.

ARPATH	 = /PROGRA~1/MKSTOO~1/mksnt/ar
AR	 = $(ARPATH)
AR_OPT1	 = -r

# Use normal tools for building internal tools (programs):

CCPATH_TOOL = $(CCPATH)

# MKS lacks some commands that it's possible to do without:

CSTRIP	 = cstrip_not_present

# HP-UX has rmnl(1) but MKS does not, so use grep instead; note the tab:

RMNL	 = grep -v '^[ 	]*$$'

ECHO	 = echo
PWD	 = /PROGRA~1/MKSTOO~1/mksnt/pwd
BANG	 = bang_not_present

# Platform-specific library lists for win_ia32 for various "lib" targets:

LIB_LIST      = lib32lib
LIBS_LIST     = lib32lib
LIBS_ALL_LIST = lib32lib
LIBS_PIC_LIST = TBD

# Suffix for $LIB_ID (same as what's passed in $LIB_SUFFIX for a recursive
# make):

LIB_ID_SUFFIX = lib

# Linker option to build a shared library:
#
# TBD:  Fill this in when appropriate.

LD_OPT_SL = ?

TARCHIVE_LIBS_PIC =

# Unfortunately there are CC_OPTS differences between both platforms AND
# flavors, so spell them out here by flavor, meaning this file cannot be
# flavor-ignorant:
#
# CL -nologo hides the banner.
# CL -W4 is like -Wall, and "4" is the highest number it seems to accept.

CC_OPTS_product = -O2 -W4  -DJU_WIN -DJU_WIN_IA32 -DJU_FLAVOR_PRODUCT -nologo
CC_OPTS_cov	= -O2 -W4  -DJU_WIN -DJU_WIN_IA32 -DJU_FLAVOR_COV     -nologo
CC_OPTS_debug	= -GZ -W4  -DJU_WIN -DJU_WIN_IA32 -DJU_FLAVOR_DEBUG   -nologo

UNIFDEF_OPTS_PLATFORM = -t -DJU_WIN -DJU_WIN_IA32

# TBD:  MKS has compress, but directory paths look like:
#
#   C:/Program Files/MKS Toolkit/etc/cat1
#
# Unclear where to put manual entries or whether to compress them.  Also, MKS
# compress complains:  "Output in LZW compress format is not supported."  For
# now just drop them uncompressed.

COMPRESSPATH   = cat
MANDIR_SUFFIX  =
MANFILE_SUFFIX =

# Platform win_ia32 VC++ "CL" compiler needs -Fo to specify the object file and
# -Fe to specify the executable file:

CCoo = -Fo
CCoe = -Fe
