# @(#) $Revision: 4.5 $ $Source: /judy/make_includes/platform.win_ipf.mk $

# Makefile fragment for Judy* for platform "win_ipf" (using MKS Toolkit and
# Visual Studio / VC++).  See platform.win_ia32.mk for more comments.

PLATFORM = win_ipf

# Commands to run, using full paths where appropriate to avoid ambiguity:
#
# Note:  MKS uses unusual path names.  Giving the "user" versions of these
# names to make(1), even with quote marks to hide spaces, seems to fail, so
# instead use "real" names (either way without *.exe suffixes).

SHELL	 = /mks/mksnt/sh

# Use VC++ compile tools rather than MKS compile tools:

# CCPATH = /PROGRA~1/MKSTOO~1/mksnt/cc
CCPATH	 = /PROGRA~1/MICROS~3/Bin/Win64/CL
# LDPATH = /PROGRA~1/MKSTOO~1/mksnt/cc
LDPATH	 = /PROGRA~1/MICROS~3/Bin/Win64/LINK

# Must use the "lib" command:

ARPATH	 = /PROGRA~1/MICROS~3/Bin/Win64/LIB
AR	 = $(ARPATH)
AR_OPT2	 = -out:

CCPATH_TOOL = $(CCPATH)

# MKS lacks some commands that it's possible to do without:

CSTRIP	 = cstrip_not_present

# HP-UX has rmnl(1) but MKS does not, so use grep instead; note the tab:

RMNL	 = grep -v '^[ 	]*$$'

ECHO	 = echo
PWD	 = /mks/mksnt/pwd
BANG	 = bang_not_present

# Platform-specific library lists for win_ipf for various "lib" targets:

LIB_LIST      = lib64lib
LIBS_LIST     = lib64lib
LIBS_ALL_LIST = lib64lib
LIBS_PIC_LIST = TBD

# Suffix for $LIB_ID (same as what's passed in $LIB_SUFFIX for a recursive
# make):

LIB_ID_SUFFIX = lib

# Linker option to build a shared library:
#
# TBD:  Fill this in when appropriate.

LD_OPT_SL = ?

TARCHIVE_LIBS_PIC =

# Unfortunately there are CC_OPTS differences between both platforms AND
# flavors, so spell them out here by flavor, meaning this file cannot be
# flavor-ignorant:
#
# CL -nologo hides the banner.
# CL -W4 is like -Wall, and "4" is the highest number it seems to accept.
# This platform does not define __LP64__ because it's not a 64-bit data model,
# but Judy wants JU_64BIT anyway because it uses 64-bit words to match pointer
# sizes.

CC_OPTS_product = -O2 -W4  -DJU_WIN -DJU_WIN_IPF -DJU_FLAVOR_PRODUCT -nologo -DJU_64BIT
CC_OPTS_cov	= -O2 -W4  -DJU_WIN -DJU_WIN_IPF -DJU_FLAVOR_COV     -nologo -DJU_64BIT
CC_OPTS_debug	= -GZ -W4  -DJU_WIN -DJU_WIN_IPF -DJU_FLAVOR_DEBUG   -nologo -DJU_64BIT

UNIFDEF_OPTS_PLATFORM = -t -DJU_WIN -DJU_WIN_IPF

# At least on this platform, for make to recognize that a tool is already
# built, the filename + auto-added suffix must be used:

TOOL_SUFFIX = .exe

# TBD:  MKS has compress, but directory paths look like:
#
#   C:/Program Files/MKS Toolkit/etc/cat1
#
# Unclear where to put manual entries or whether to compress them.  Also, MKS
# compress complains:  "Output in LZW compress format is not supported."  For
# now just drop them uncompressed.

COMPRESSPATH   = cat
MANDIR_SUFFIX  =
MANFILE_SUFFIX =

# Platform win_ipf VC++ "CL" compiler needs -Fo to specify the object file and
# -Fe to specify the executable file:

CCoo = -Fo
CCoe = -Fe
