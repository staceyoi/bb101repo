// Copyright (C) 2000 - 2002 Hewlett-Packard Company
//
// This program is free software; you can redistribute it and/or modify it
// under the term of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
// for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// _________________

#ifndef __lint
static char * version = "@(#) $Revision: 4.5 $ $Source: /judy/src/apps/demo/JudySort.c $";
#endif

// Judy demonstration code:  Judy equivalent of sort -u.  While Judy is not
// primarily intended as a sorting algorithm, in many cases it's faster to
// store values in a Judy array and read them back in sorted order than to sort
// them using standard algorithms.
//
// Usage:  <program> <file-to-sort>
//
// Code and comments are included if you want to modify the program to output
// duplicates as an exercise.
//
// Note:  If an input line is longer than BUFSIZ, it's broken into two or more
// lines.

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "Judy.h"

main (int argc, char ** argv)
{
    Pvoid_t  PJArray = (Pvoid_t) NULL;	// Judy array.
    PPvoid_t PPValue;			// Judy array element.
    char     Index [BUFSIZ];		// string to sort.
    char *   Pc;			// place in Index.
    FILE *   fpin;			// to read file.
    JError_t JError;                    // Judy error structure


// CHECK FOR REQUIRED INPUT FILE PARAMETER:

    if (argc != 2)
    {
	(void) fprintf (stderr, "Usage: %s <file-to-sort>\n", argv[0]);
	(void) fputs   ("Uses each line as a JudySL index.\n", stderr);
	exit (1);
    }


// OPEN INPUT FILE:

    if ((fpin = fopen (argv[1], "r")) == (FILE *) NULL)
    {
	(void) fprintf (stderr, "%s: Cannot open file \"%s\": %s "
			"(errno = %d)\n", argv[0], argv[1], strerror (errno),
			errno);
	exit (1);
    }


// INPUT/STORE LOOP:
//
// Read each input line (up to Index size) and save the line as an index into a
// JudySL array.  If the line doesn't overflow Index, it ends with a newline,
// which must be removed for sorting comparable to sort(1).

    while (fgets (Index, sizeof (Index), fpin) != (char *) NULL)
    {
	if ((Pc = strchr (Index, '\n')) != (char *) NULL)
	    *Pc = '\0';				// trim at newline.

	if ((PPValue = JudySLIns (&PJArray, Index, &JError)) == PPJERR)
	{
            fprintf(stderr, "File: '%s', line: %d: Judy error: %d\n",
                    __FILE__, __LINE__, JU_ERRNO(&JError));
	    exit (1);
	}

// To modify the program to output duplication counts, like uniq -d, or to emit
// multiple copies of repeated strings:

#ifdef notdef
	++(*((PWord_t) PPValue));
#endif
    }


// PRINT SORTED STRINGS:
//
// To output all strings and not just the unique ones, output Index multiple
// times = *((PWord_t) PPValue).

    Index[0] = '\0';				// start with smallest string.

    for (PPValue  = JudySLFirst (PJArray, Index, &JError);
	 PPValue != (PPvoid_t) NULL;
	 PPValue  = JudySLNext  (PJArray, Index, &JError))
    {
	(void) puts (Index);			// see comment above.
    }

    exit (0);
    /*NOTREACHED*/

} // main()
