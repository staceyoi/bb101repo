/* Generic support for 64-bit ELF
   Copyright 1993 Free Software Foundation, Inc.

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

#include "bfd.h"
#include "sysdep.h"
#include "libbfd.h"
#include "elf-bfd.h"

   /* This does not include any relocations, but should be good enough
          for GDB to read the file.  */

/* Defining TARGET_LITTLE_SYM here makes a duplicate definitino of
   bfd_elf64_little_generic_vec with elf64-gen.c because elf64-target.h
   makes a function definition if TARGET_LITTLE_SYM is defined.
   */

/* #define TARGET_LITTLE_SYM               bfd_elf64_little_generic_vec */
#define TARGET_LITTLE_NAME              "elf64-little"
/*j
#define TARGET_BIG_SYM                  bfd_elf64_big_generic_vec
 */
#define TARGET_BIG_SYM                  bfd_elf64_ia64_vec
#define TARGET_BIG_NAME                 "elf64-big"
#define ELF_ARCH                        bfd_arch_ia64
#define ELF_MACHINE_CODE                EM_NONE
#define ELF_MACHINE_ALT1                50
#define ELF_MACHINE_ALT2                1999
#define bfd_elf64_bfd_reloc_type_lookup bfd_default_reloc_type_lookup
#define elf_info_to_howto               _bfd_elf_no_info_to_howto

/* RM: We need to make bfd sections out of UNWIND type sections */
#define elf_backend_section_from_shdr   ia64_elf_section_from_shdr

#include "elf/common.h"
#include "elf_em.h"
/* Create a new bfd section from an ELF section header. */

boolean
ia64_elf_section_from_shdr(bfd *abfd, Elf_Internal_Shdr *hdr, char *name)
{
  switch (hdr->sh_type)
    {
      case SHT_IA_64_UNWIND:
        return _bfd_elf_make_section_from_shdr (abfd, hdr, name);

      case SHT_INIT_ARRAY:
        return _bfd_elf_make_section_from_shdr (abfd, hdr, name);
	
      default:
        return _bfd_elf_make_section_from_shdr (abfd, hdr, name);
    }
}



#include "elf64-target.h"

/* RM: We also need a version that handles little endian elf data
   structures for backward compatibility */
const bfd_target bfd_elf64_ia64_little_header_vec =
{
  /* name: identify kind of target */
  "elf64-little",

  /* flavour: general indication about file */
  bfd_target_elf_flavour,

  /* byteorder: data is big endian */
  BFD_ENDIAN_BIG,

  /* header_byteorder: header is little endian */
  BFD_ENDIAN_LITTLE,
  
  /* object_flags: mask of all file flags */
  (HAS_RELOC | EXEC_P | HAS_LINENO | HAS_DEBUG | HAS_SYMS | HAS_LOCALS |
   DYNAMIC | WP_TEXT | D_PAGED),

  /* section_flags: mask of all section flags */
  (SEC_HAS_CONTENTS | SEC_ALLOC | SEC_LOAD | SEC_RELOC | SEC_READONLY |
   SEC_CODE | SEC_DATA | SEC_DEBUGGING | SEC_EXCLUDE | SEC_SORT_ENTRIES),

   /* leading_symbol_char: is the first char of a user symbol
      predictable, and if so what is it */
  elf_symbol_leading_char,

  /* ar_pad_char: pad character for filenames within an archive header
     FIXME:  this really has nothing to do with ELF, this is a characteristic
     of the archiver and/or os and should be independently tunable */
  '/',

  /* ar_max_namelen: maximum number of characters in an archive header
     FIXME:  this really has nothing to do with ELF, this is a characteristic
     of the archiver and should be independently tunable.  This value is
     a WAG (wild a** guess) */
  14,

  /* Routines to byte-swap various sized integers from the data sections */
  bfd_getb64, bfd_getb_signed_64, bfd_putb64,
    bfd_getb32, bfd_getb_signed_32, bfd_putb32,
    bfd_getb16, bfd_getb_signed_16, bfd_putb16,

  /* Routines to byte-swap various sized integers from the file headers */
  bfd_getl64, bfd_getl_signed_64, bfd_putl64,
    bfd_getl32, bfd_getl_signed_32, bfd_putl32,
    bfd_getl16, bfd_getl_signed_16, bfd_putl16,
  
  /* bfd_check_format: check the format of a file being read */
  { _bfd_dummy_target,		/* unknown format */
    bfd_elf64_object_p,		/* assembler/linker output (object file) */
    bfd_elf64_archive_p,	/* an archive */
    bfd_elf64_core_file_p	/* a core file */
  },

  /* bfd_set_format: set the format of a file being written */
  { bfd_false,
    bfd_elf64_mkobject,
    bfd_elf64_mkarchive,
    bfd_false
  },

  /* bfd_write_contents: write cached information into a file being written */
  { bfd_false,
    bfd_elf64_write_object_contents,
    bfd_elf64_write_archive_contents,
    bfd_false
  },

      BFD_JUMP_TABLE_GENERIC (bfd_elf64),
      BFD_JUMP_TABLE_COPY (bfd_elf64),
      BFD_JUMP_TABLE_CORE (bfd_elf64),
#ifdef bfd_elf64_archive_functions
      BFD_JUMP_TABLE_ARCHIVE (bfd_elf64_archive),
#else
      BFD_JUMP_TABLE_ARCHIVE (_bfd_archive_coff),
#endif
      BFD_JUMP_TABLE_SYMBOLS (bfd_elf64),
      BFD_JUMP_TABLE_RELOCS (bfd_elf64),
      BFD_JUMP_TABLE_WRITE (bfd_elf64),
      BFD_JUMP_TABLE_LINK (bfd_elf64),
      BFD_JUMP_TABLE_DYNAMIC (bfd_elf64),

  NULL,  /* alternative_target  */

  /* backend_data: */
  (PTR) &elf64_bed,
};

const bfd_target bfd_elf64_ia64_little_vec =
{
  /* name: identify kind of target */
  "elf64-little",

  /* flavour: general indication about file */
  bfd_target_elf_flavour,

  /* byteorder: data is little endian */
  BFD_ENDIAN_LITTLE,

  /* header_byteorder: header is little endian */
  BFD_ENDIAN_LITTLE,
  
  /* object_flags: mask of all file flags */
  (HAS_RELOC | EXEC_P | HAS_LINENO | HAS_DEBUG | HAS_SYMS | HAS_LOCALS |
   DYNAMIC | WP_TEXT | D_PAGED),

  /* section_flags: mask of all section flags */
  (SEC_HAS_CONTENTS | SEC_ALLOC | SEC_LOAD | SEC_RELOC | SEC_READONLY |
   SEC_CODE | SEC_DATA | SEC_DEBUGGING | SEC_EXCLUDE | SEC_SORT_ENTRIES),

   /* leading_symbol_char: is the first char of a user symbol
      predictable, and if so what is it */
  elf_symbol_leading_char,

  /* ar_pad_char: pad character for filenames within an archive header
     FIXME:  this really has nothing to do with ELF, this is a characteristic
     of the archiver and/or os and should be independently tunable */
  '/',

  /* ar_max_namelen: maximum number of characters in an archive header
     FIXME:  this really has nothing to do with ELF, this is a characteristic
     of the archiver and should be independently tunable.  This value is
     a WAG (wild a** guess) */
  14,

  /* Routines to byte-swap various sized integers from the data sections */
  bfd_getl64, bfd_getl_signed_64, bfd_putl64,
    bfd_getl32, bfd_getl_signed_32, bfd_putl32,
    bfd_getl16, bfd_getl_signed_16, bfd_putl16,

  /* Routines to byte-swap various sized integers from the file headers */
  bfd_getl64, bfd_getl_signed_64, bfd_putl64,
    bfd_getl32, bfd_getl_signed_32, bfd_putl32,
    bfd_getl16, bfd_getl_signed_16, bfd_putl16,
  
  /* bfd_check_format: check the format of a file being read */
  { _bfd_dummy_target,		/* unknown format */
    bfd_elf64_object_p,		/* assembler/linker output (object file) */
    bfd_elf64_archive_p,	/* an archive */
    bfd_elf64_core_file_p	/* a core file */
  },

  /* bfd_set_format: set the format of a file being written */
  { bfd_false,
    bfd_elf64_mkobject,
    bfd_elf64_mkarchive,
    bfd_false
  },

  /* bfd_write_contents: write cached information into a file being written */
  { bfd_false,
    bfd_elf64_write_object_contents,
    bfd_elf64_write_archive_contents,
    bfd_false
  },

      BFD_JUMP_TABLE_GENERIC (bfd_elf64),
      BFD_JUMP_TABLE_COPY (bfd_elf64),
      BFD_JUMP_TABLE_CORE (bfd_elf64),
#ifdef bfd_elf64_archive_functions
      BFD_JUMP_TABLE_ARCHIVE (bfd_elf64_archive),
#else
      BFD_JUMP_TABLE_ARCHIVE (_bfd_archive_coff),
#endif
      BFD_JUMP_TABLE_SYMBOLS (bfd_elf64),
      BFD_JUMP_TABLE_RELOCS (bfd_elf64),
      BFD_JUMP_TABLE_WRITE (bfd_elf64),
      BFD_JUMP_TABLE_LINK (bfd_elf64),
      BFD_JUMP_TABLE_DYNAMIC (bfd_elf64),
  
  NULL, /* alternative_target */

  /* backend_data: */
  (PTR) &elf64_bed,
};
