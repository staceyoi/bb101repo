/* ELF core file support for BFD.
   Copyright (C) 1995, 1996, 1997, 1998 Free Software Foundation, Inc.

This file is part of BFD, the Binary File Descriptor library.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */



#include <string.h>

#ifdef HAVE_SYS_PROCFS_H		/* Some core file support requires host /proc files */
#include <signal.h>
#include <sys/procfs.h>

/* Solaris includes the field pr_who that indicates the thread number within
   the process.  */

#ifdef PIOCOPENLWP
#define get_thread(STATUS) ((((prstatus_t *)(STATUS))->pr_who << 16) \
			    | ((prstatus_t *)(STATUS))->pr_pid)
#else
#define get_thread(STATUS) (((prstatus_t *)(STATUS))->pr_pid)
#endif
#endif   /* ifdef HAVE_SYS_PROCFS_H */

#ifdef HAVE_SYS_CORE_H

#include <sys/core.h>

#ifndef HP_IA64
#include "elf/hppa.h"
#else
#include "elf/common.h"
#include <elf_em.h>
#include <sys/uc_access.h>
#define REGISTER_BYTES (128*12 + (588 - 127)*8)
#define REGISTER_BYTE(N) \
    ((N) <= 127 ? (N) * 12    \
     : (128) * 12 + ((N)-128) * 8)
#define REGISTER_RAW_SIZE(N) \
    ((N) <= 127 ? 12 \
      : (N) <= 319 ? 1 \
      : 8)

#define AR0_REGNUM	456
#define ARLAST_REGNUM	583
#define BR0_REGNUM 	448
#define CFM_REGNUM	585
#define FR0_REGNUM	0
#define FRLAST_REGNUM  	127
#define GR0_REGNUM 	320
#define NAT_BITPOS(bs_addr) (((bs_addr) >> 3) & 0x3f)
#define NAT_COLLECTION_ADDR(bs_addr) ((bs_addr) | 0x1f8)
#define NR0_REGNUM 	128
#define PC_REGNUM  	584
#define PR0_REGNUM	256
#define PRLAST_REGNUM	319
#define PSR_REGNUM	586
#define REASON_REGNUM	587
#define REGISTER_SIZE 	8
#define SIZE_OF_FRAME(cfm)	(cfm & 0x7f)

static uint64_t bfd_reg_bs_addr PARAMS ((int, uint64_t, uint64_t));

Elf_Internal_Phdr*
elf_read_program_header PARAMS ((bfd*, Elf_Internal_Ehdr*));

boolean
elf_elfcore_read_hp_core (bfd *abfd, Elf_Internal_Phdr *phdr);

extern uint64_t unswizzle PARAMS ((uint64_t));
extern uint64_t swizzle PARAMS ((uint64_t));
#endif  /* HP_IA64 */

static int can_get_ar_nbrs[] = { 16, 17, 18, 32, 36, 40, 64, 65, 66 };
static int fr0_value[3] = { 0x00000000, 0x00000000, 0x00000000 };
static int fr1_value[3] = { 0x0000ffff, 0x80000000, 0x00000000 };

#endif /* ifdef HAVE_SYS_CORE_H */

char*
elf_core_file_failing_command (bfd *abfd)
{
  return elf_tdata (abfd)->core_command;
}

int
elf_core_file_failing_signal (bfd *abfd)
{
  return elf_tdata (abfd)->core_signal;
}

int
elf_core_file_failing_si_code (bfd *abfd)
{
  return elf_tdata (abfd)->core_si_code;
}


boolean
elf_core_file_matches_executable_p (bfd *core_bfd, bfd *exec_bfd)
{
  char* corename;

  /* xvecs must match if both are ELF files for the same target. */

  if (core_bfd->xvec != exec_bfd->xvec)
    {
      bfd_set_error (bfd_error_system_call);
      return false;
    }
  
  /* See if the name in the corefile matches the executable name. */
#ifdef HP_HPUX
  corename = elf_tdata (core_bfd)->core_command;
#else
  corename = elf_tdata (core_bfd)->core_program;
#endif
  if (corename != NULL)
    {
      const char* execname = strrchr (exec_bfd->filename, '/');
      execname = execname ? execname + 1 : exec_bfd->filename;

      if (strcmp(execname, corename) != 0)
	return false;
    }

  return true;
}


#ifdef HP_IA64
extern ucontext_t *core_ucontext;
#endif

extern int is_live_core (void);

#ifdef HAVE_SYS_CORE_H
static boolean
bfd_proc_info (bfd *abfd, char *buf, long long sz, long long filepos)
{
  uint64_t ar_bsp;		/* AR17 */
  uint64_t ar_bspstore;		/* AR18 */
  uint64_t ar_rnat;		/* AR19 */
  uint64_t ar_value;
  uint64_t brs[8];
  uint64_t cfm_value;
  uint64_t cr_value;
  int      gr_regno;
  uint64_t grs[32];
  int i;
  unsigned int nats;
  uint64_t nat_collection;
  uint64_t nat_collection_addr;
  asection *newsect;
  int regno;
  uint64_t reg_addr;
  uint64_t regval;
  int 	   shift_size;
  struct proc_info *status = (struct proc_info *) 0;
  uint64_t values[128 - 32];  /* 128 -32 stacked registers max */

  /* These variable were not used and TOHECC compiler 
     with +wlint was emitting a warning for this.
     So this has been commented for now */
  /* static uint64_t zero_value = 0;*/


#ifdef HP_IA64
  /* Nothing is allocated for FR0 or FR1, thus the -2 below */
  fp_regval_t fp_values[FRLAST_REGNUM - FR0_REGNUM -2 + 1];
  uint64_t pr_values;
#endif


  nat_collection_addr = (unsigned long)(long) -1;  /* Have not read nat_collection */

  if (sz == sizeof (struct proc_info))
    {
#ifdef HP_IA64
      /* Save the list of ucontexts in bfd. */  
      ucontext_t * uc = &(((struct proc_info *)(void *) buf)->pinfo_context);
      char secname[128], *p;
      asection *regsect = NULL;

      struct uc_list* new_ulist = (struct uc_list*) bfd_zalloc (abfd,
                                                sizeof (struct uc_list));
      new_ulist->uc = (ucontext_t *) bfd_zalloc (abfd, sizeof (ucontext_t));
      memcpy (new_ulist->uc, uc, sizeof(ucontext_t));
      new_ulist->next = elf_tdata (abfd)->ulist;
      elf_tdata (abfd)->ulist = new_ulist;

      sprintf (secname, ".reg/%lld", uc->__uc_misc.__uc_core_lwpid); 
      p = bfd_alloc (abfd, strlen(secname) + 1);
      strcpy (p, secname);
      newsect = bfd_make_section (abfd, p);
      if (newsect == NULL)
	return false;
      newsect->flags = SEC_IN_MEMORY | SEC_HAS_CONTENTS;
      newsect->contents = bfd_alloc(abfd, REGISTER_BYTES);
      newsect->_raw_size = REGISTER_BYTES;
      newsect->filepos = (long) filepos;

/* Bindu 052102: signal number will be -1, if the core is not generated due
 * to a signal. I.e., the core file may be generated by using ttrace 
 * TT_PROC_CORE request. Let's set the signal number to 0 and get along.
 * Remove this when JAGae09946 defect in kernel is fixed.
 */
      if ((uc->__uc_misc.__uc_core_sig == (unsigned long)(long) -1) && is_live_core ())
	{
	  uc->__uc_misc.__uc_core_sig = 0;
	}

      if (uc->__uc_misc.__uc_core_sig != (unsigned long)(long) -1)
	{
	  /* this is the magic needed to set inferior_pid 
	     in add_to_thread_list */
	  regsect = bfd_make_section (abfd, ".reg");
	  if (regsect == NULL)
	    return false;
	  regsect->flags = SEC_IN_MEMORY | SEC_HAS_CONTENTS;
	  regsect->contents = bfd_alloc(abfd, REGISTER_BYTES);
	  regsect->_raw_size = REGISTER_BYTES;
	  regsect->filepos = (long) filepos;
	}

      /* GR0 is not readable.  Its value and its NAT byte are zero */
      regval = 0;
      memcpy(newsect->contents + REGISTER_BYTE(NR0_REGNUM), &regval,
             REGISTER_RAW_SIZE (NR0_REGNUM));
      memcpy(newsect->contents + REGISTER_BYTE(GR0_REGNUM), &regval,
             REGISTER_RAW_SIZE (GR0_REGNUM));

      
      __uc_get_grs(&(((struct proc_info *) (void *) buf)->pinfo_context),
                   1, 31, grs, &nats);
      for (i = 1; i < 32; i++) {
        regval = nats & (1 << i);
        memcpy(newsect->contents + REGISTER_BYTE(NR0_REGNUM + i), &regval,
               REGISTER_RAW_SIZE (NR0_REGNUM + i));
        regval = grs[i-1];
        memcpy(newsect->contents + REGISTER_BYTE(GR0_REGNUM + i), &regval,
               REGISTER_RAW_SIZE (GR0_REGNUM + i));
        
      }

      __uc_get_brs(&(((struct proc_info *)(void *) buf)->pinfo_context),
                   0, 8, brs);
      for (i = 0; i < 8; i++) {
        regval = brs[i];
        memcpy(newsect->contents + REGISTER_BYTE(BR0_REGNUM + i), &regval,
               REGISTER_RAW_SIZE (BR0_REGNUM + i));        
      }
      

      __uc_get_ip(&(((struct proc_info *)(void *) buf)->pinfo_context),
                  &regval);
      memcpy(newsect->contents + REGISTER_BYTE (PC_REGNUM), &regval,
             REGISTER_RAW_SIZE (PC_REGNUM));

      /* More registers to get */

      /* __uc_get_frs  2..127 
	 FR0 has a value of zero (all bits zero)
	 FR1 has a value of 1.0, see fr1_value.
	 The other floating point registers are provided by __uc_get_frs
	 We store FP registers in 12 bytes, __uc_get_frs uses 16 bytes
	 for each with the real bits in the low (high address) 12 bytes.
	 FR0_REGNUM == 0
	 FRLAST_REGNUM == 127
      */

      memcpy(newsect->contents + REGISTER_BYTE (FR0_REGNUM), 
	     &fr0_value,
	     REGISTER_RAW_SIZE (FR0_REGNUM));

      memcpy(newsect->contents + REGISTER_BYTE (FR0_REGNUM + 1), 
	     &fr1_value,
	     REGISTER_RAW_SIZE (FR0_REGNUM + 1));
      /* The count is FRLAST_REGNUM - FR0_REGNUM -2 + 1 
	 because 2 registers are not provided by __uc_get_frs/
         */
      __uc_get_frs(&(((struct proc_info *)(void *) buf)->pinfo_context),
		   2, FRLAST_REGNUM - FR0_REGNUM - 1, fp_values);
      for (i = FR0_REGNUM + 2; i <= FRLAST_REGNUM; i++)
	{
	  memcpy(newsect->contents + REGISTER_BYTE (i),
		 ((char*) &fp_values[i -2]) + 4, /* low 12 bytes of 16 */
		 REGISTER_RAW_SIZE (FR0_REGNUM + i));
	}
      /* __uc_get_prs 
	 I'm not sure that each predicate is stored as the low byte
	 of each 64-bit value returned.  This is what is coded.  It might
	 be that the predicates are packed one per byte as if the values
	 were characters.

	 We store a predicate in the first byte of the 16 bytes allocated
	 for it.
      */

      __uc_get_prs(&(((struct proc_info *)(void *) buf)->pinfo_context), &pr_values);
      for (i = PR0_REGNUM; i <= PRLAST_REGNUM; i++)
	{
	  newsect->contents[REGISTER_BYTE (i)] =
	      	((pr_values & (1ULL << (i - PR0_REGNUM))) != 0);
	}
      /* __uc_get_cfm 	CFM_REGNUM */

      __uc_get_cfm(&(((struct proc_info *)(void *) buf)->pinfo_context),
		   &cfm_value);
      memcpy(newsect->contents + REGISTER_BYTE (CFM_REGNUM), 
	     &cfm_value,
	     REGISTER_RAW_SIZE (CFM_REGNUM));

      /* __uc_get_um - no known need for this */
      /* __uc_get_ar 
	 We can only get some of the AR registers (numbers in can_get_ar_nbrs).
	 The others are set to zero.  We set everything to zero and then
	 read in the ones allowed.
      */

      memset (newsect->contents + REGISTER_BYTE (AR0_REGNUM),
	      0,
	      REGISTER_RAW_SIZE (AR0_REGNUM) * (ARLAST_REGNUM - AR0_REGNUM + 1)
	     );
      
      /* initialize for compiler warning */
      ar_bsp = ar_bspstore = ar_rnat = 0; 
      for (i = 0;  i < (sizeof(can_get_ar_nbrs) / sizeof (int));  i++)
	{
	  regno = AR0_REGNUM + can_get_ar_nbrs[i];
	  __uc_get_ar(&(((struct proc_info *)(void *) buf)->pinfo_context), 
		      can_get_ar_nbrs[i], &ar_value);
	  memcpy (newsect->contents + REGISTER_BYTE (regno), 
		  &ar_value, 
		  REGISTER_RAW_SIZE (regno));
	  if (regno == AR0_REGNUM + 17)
	    ar_bsp = ar_value;
	  else if (regno == AR0_REGNUM + 18)
	    ar_bspstore = ar_value;
	  else if (regno == AR0_REGNUM + 19)
	    ar_rnat = ar_value;
	}

      /* __uc_get_cr 
	 CFM_REGNUM	low 38 bits of __cr_isr		CR_ISR
	 PSR_REGNUM	__cr_ipsr                       CR_ISR
      */

      __uc_get_cr(&(((struct proc_info *)(void *) buf)->pinfo_context),
		  16 /* CR_IPSR - Interruption Processor Status Register */,
		  &cr_value);
      memcpy (newsect->contents + REGISTER_BYTE (PSR_REGNUM),
	      &cr_value,
	      REGISTER_RAW_SIZE (PSR_REGNUM));


      /* __uc_get_ed  Exception Deferal bit  - no known use */

      /* Get the stacked GR's and the Nat bits 
	 Read registers GR0_REGNUM + 32 .. GR0_REGNUM + SIZE_OF_FRAME - 1
	 and copy them to newsect->contents.  Zero out the area for
	 the remaining registers.
	 */
      
      for (gr_regno = 32;  gr_regno < 128;  gr_regno++)
	{
	  regno = GR0_REGNUM + gr_regno;
	  reg_addr = bfd_reg_bs_addr (regno, ar_bsp, cfm_value);
	  if (reg_addr)
	    {  /* The register exists, get it from the backing store */

	      __uc_get_rsebs (&(((struct proc_info *)(void *) buf)->pinfo_context),
			     (uint64_t *) (long) reg_addr,
			     1,
			     values); 
	      memcpy(newsect->contents + REGISTER_BYTE(GR0_REGNUM + gr_regno), 
		     &values[0],
		     REGISTER_RAW_SIZE (GR0_REGNUM + gr_regno));

	      /* Get and set the NAT bit for this register.  Where to get it?
		 from uc_access.h:
		 Where to find the NaT:
		   addr < ar.bspstore & ~0x1ff               In backing store
		   ar.bspstore & ~0x1ff <= addr < ar.bspstore      ar.rnat
		   ar.bspstore <= addr < ar.bsp		__uc_get_rsebs
                 */
	      if (reg_addr >= (ar_bspstore & ~0x1ff) && reg_addr < ar_bspstore)
		{ /* NAT bit is in ar.rnat == ?? */
		  shift_size = (unsigned int) NAT_BITPOS(reg_addr);
		  *(newsect->contents + REGISTER_BYTE(NR0_REGNUM + gr_regno))
		    = (ar_rnat >> shift_size) & 1;
		}
	      else
		{ /* Use __uc_get_rsebs to read NAT collection 
		     We cache the last one in nat_collection that we
		     read from nat_collection_addr, which is initialized to -1.
		     */
		  if (NAT_COLLECTION_ADDR(reg_addr) != nat_collection_addr)
		    { /* Must read in a new nat collection word */
		      nat_collection_addr = NAT_COLLECTION_ADDR(reg_addr);

		      __uc_get_rsebs (&(((struct proc_info *) 
					   (void *)buf)->pinfo_context),
				     (uint64_t *) (long) nat_collection_addr,
				     1,
				     values); 
		      nat_collection = values[0];
		      shift_size = (unsigned int) NAT_BITPOS(reg_addr);
		      *(newsect->contents 
			   + REGISTER_BYTE(NR0_REGNUM + gr_regno))
			 = (nat_collection >> shift_size) & 1;
		    }
		}
	    }
	  else
	    { /* zero out the GR and the NaT bit */

	      memset (newsect->contents + REGISTER_BYTE(GR0_REGNUM + gr_regno),
		      0,
		      REGISTER_RAW_SIZE (GR0_REGNUM + gr_regno));
	      memset (newsect->contents + REGISTER_BYTE(NR0_REGNUM + gr_regno),
		      0,
		      REGISTER_RAW_SIZE (NR0_REGNUM + gr_regno));
	    }
	}
#else      
      newsect = bfd_make_section (abfd, ".reg");
      if (newsect == NULL)
	return false;
      newsect->_raw_size = sizeof(status->hw_regs.ss_wide.ss_64);
      newsect->filepos = filepos + (long) &status->hw_regs.ss_wide.ss_64;
      newsect->flags = SEC_HAS_CONTENTS;
      newsect->alignment_power = 2;

      newsect = bfd_make_section (abfd, ".reg2");
      if (newsect == NULL)
	return false;
      newsect->_raw_size = sizeof(status->hw_regs.ss_fpblock);
      newsect->filepos = filepos + (long) &status->hw_regs.ss_fpblock;
      newsect->flags = SEC_HAS_CONTENTS;
      newsect->alignment_power = 2;

      newsect = bfd_make_section (abfd, ".flags");
      if (newsect == NULL)
	return false;
      newsect->_raw_size = sizeof(status->hw_regs.ss_flags);
      newsect->filepos = filepos + (long) &status->hw_regs.ss_flags;
      newsect->flags = SEC_HAS_CONTENTS;
      newsect->alignment_power = 2;
#endif  

#ifdef HAVE_PRSTATUS_T
      if ((core_prstatus (abfd) = bfd_alloc (abfd, sz)) != NULL)
	{
	  memcpy (core_prstatus (abfd), buf, sz);
#ifdef HP_IA64
	  /* Set core_ucontext to be sure it's always set */
	  core_ucontext = core_prstatus (abfd);
	  if (uc->__uc_misc.__uc_core_sig != -1)
	    {
	      /* Set core_ucontext to the "real" one, i.e., the one that 
		 has the signal */
	      core_ucontext = core_prstatus (abfd);
	      if (regsect != NULL && newsect != NULL)
		memcpy (regsect->contents, newsect->contents, REGISTER_BYTES);
	    }
#endif
	}
#endif /* ifdef HAVE_PRSTATUS_T */
    }
  return true;
}

#ifdef HP_IA64
/* Return 0 if regno (gdb register number, e.g. GR0_REGNUM) is not
   a stacked register in the backing store.  Otherwise, return its 
   address in the backing store.

   Mostly cloned from gdb reg_bs_addr.

   ar_bsp is  AR17
   cfm current frame marker, actually we pass all of cr_isr, bits 37:0 are
	the cfm.
   */

static uint64_t
bfd_reg_bs_addr (int regno, uint64_t ar_bsp, uint64_t cfm)
{
  int           last_reg;
  uint64_t     last_reg_addr;
  uint64_t     nat_addr;
  uint64_t     reg_addr;

  if (regno < GR0_REGNUM + 32)
    return 0;  /* only stacked registers GR32 .. GR127 have BS addresses */
  
  last_reg = 31 + (int) SIZE_OF_FRAME(cfm) + GR0_REGNUM;
  if (regno > last_reg)
    return 0;

  /* The last register is 31 + SIZE_OF_FRAME.  It would be stored at
     BSP - REGISTER_SIZE, unless that is a NaT collection, in which case
     it would be the previous slot.
     */
  
  last_reg_addr = ar_bsp - REGISTER_SIZE;
  if (last_reg_addr == NAT_COLLECTION_ADDR (last_reg_addr))
    last_reg_addr -= REGISTER_SIZE;
  reg_addr = last_reg_addr - ((last_reg - regno) * REGISTER_SIZE);

  /* We need to reduce reg_addr by one slot for every NaT collection stored
     between it and last_reg_addr.  nat_addr starts out as the first
     nat collection before last_reg_addr.
     */
    
  nat_addr = NAT_COLLECTION_ADDR (last_reg_addr) - (64 * REGISTER_SIZE);
  while (nat_addr >= reg_addr)
    {
      nat_addr -= (64 * REGISTER_SIZE);
      reg_addr -= REGISTER_SIZE;
    }
  return reg_addr;
}
#endif

static boolean
elf_corefile_proc (bfd *abfd, Elf_Internal_Phdr *hdr)
{
  char *buf = NULL;		/* Entire CORE_PROC segment contents */
  asection *newsect;
  struct proc_info *pi;

  if (hdr->p_filesz > 0
      && (buf = (char *) bfd_malloc ((size_t) hdr->p_filesz)) != NULL
      && bfd_seek (abfd, hdr->p_offset, SEEK_SET) != -1
      && bfd_read ((PTR) buf, hdr->p_filesz, 1, abfd) == hdr->p_filesz)
    {
      pi = (struct proc_info *)(void *) buf;

      if (! bfd_proc_info (abfd, (char*) pi, (long long) hdr->p_filesz, hdr->p_offset))
	return false;
#ifndef HP_IA64      
      newsect = bfd_make_section (abfd, ".prstatus");
      if (newsect == NULL)
	return false;
      newsect->_raw_size = hdr->p_filesz;
      newsect->filepos = hdr->p_offset;
      newsect->flags = SEC_ALLOC | SEC_HAS_CONTENTS;
      newsect->alignment_power = 2;
#endif      

    }
  if (buf != NULL)
    {
      free (buf);
    }
  else if (hdr->p_filesz > 0)
    {
      return false;
    }
  return true;

}
#endif /* #ifdef HAVE_SYS_CORE_H */


/*  Core files are simply standard ELF formatted files that partition
    the file using the execution view of the file (program header table)
    rather than the linking view.  In fact, there is no section header
    table in a core file.

    The process status information (including the contents of the general
    register set) and the floating point register set are stored in a
    segment of type PT_NOTE.  We handcraft a couple of extra bfd sections
    that allow standard bfd access to the general registers (.reg) and the
    floating point registers (.reg2).

 */

const bfd_target *
elf_core_file_p (bfd *abfd)
{
  Elf_External_Ehdr x_ehdr;	/* Elf file header, external form */
  Elf_Internal_Ehdr *i_ehdrp;	/* Elf file header, internal form */
  Elf_Internal_Phdr *i_phdrp = NULL;	/* Elf program header, internal form */
  unsigned int phindex;
  struct elf_backend_data *ebd;
  struct elf_obj_tdata *preserved_tdata = elf_tdata (abfd);
  struct elf_obj_tdata *new_tdata = NULL;

  /* Read in the ELF header in external format.  */
  if (bfd_read ((PTR) & x_ehdr, sizeof (x_ehdr), 1, abfd) != sizeof (x_ehdr))
    {
      if (bfd_get_error () != bfd_error_system_call)
	bfd_set_error (bfd_error_wrong_format);
      return NULL;
    }

  /* Check the magic number. */
  if (elf_file_p (&x_ehdr) == false)
    goto wrong;

  /* FIXME: Check EI_VERSION here ! */

  /* Check the address size ("class"). */
  if (x_ehdr.e_ident[EI_CLASS] != ELFCLASS)
    goto wrong;

  /* Check the byteorder. */
  switch (x_ehdr.e_ident[EI_DATA])
    {
    case ELFDATA2MSB:		/* Big-endian */
      if (! bfd_big_endian (abfd))
	goto wrong;
      break;
    case ELFDATA2LSB:		/* Little-endian */
      if (! bfd_little_endian (abfd))
	goto wrong;
      break;
    default:
      goto wrong;
    }

  /* Give abfd an elf_obj_tdata. */
  new_tdata = 
    (struct elf_obj_tdata *) bfd_zalloc (abfd, sizeof (struct elf_obj_tdata));
  if (new_tdata == NULL)
    return NULL;
  elf_tdata (abfd) = new_tdata;  

  /* Swap in the rest of the header, now that we have the byte order. */
  i_ehdrp = elf_elfheader (abfd);
  elf_swap_ehdr_in (abfd, &x_ehdr, i_ehdrp);

#if defined(DEBUG) && (DEBUG & 1)
  elf_debug_file (i_ehdrp);
#endif

  ebd = get_elf_backend_data (abfd);

  /* Check that the ELF e_machine field matches what this particular
     BFD format expects.  */

  if (ebd->elf_machine_code != i_ehdrp->e_machine
      && (ebd->elf_machine_alt1 == 0
	  || i_ehdrp->e_machine != ebd->elf_machine_alt1)
      && (ebd->elf_machine_alt2 == 0
	  || i_ehdrp->e_machine != ebd->elf_machine_alt2))
    {
      const bfd_target * const *target_ptr;

      if (ebd->elf_machine_code != EM_NONE)
	goto wrong;

      /* This is the generic ELF target.  Let it match any ELF target
	 for which we do not have a specific backend.  */

      for (target_ptr = bfd_target_vector; *target_ptr != NULL; target_ptr++)
	{
	  struct elf_backend_data *back;

	  if ((*target_ptr)->flavour != bfd_target_elf_flavour)
	    continue;
	  back = (struct elf_backend_data *) (*target_ptr)->backend_data;
	  if (back->elf_machine_code == i_ehdrp->e_machine)
	    {
	      /* target_ptr is an ELF backend which matches this
		 object file, so reject the generic ELF target.  */
	      goto wrong;
	    }
	}
    }

  /* If there is no program header, or the type is not a core file, then
     we are hosed. */
  if (i_ehdrp->e_phoff == 0 || i_ehdrp->e_type != ET_CORE)
    goto wrong;

  /* Does BFD's idea of the phdr size match the size
     recorded in the file? */
  if (i_ehdrp->e_phentsize != sizeof (Elf_External_Phdr))
    goto wrong;

  /* Allocate space for the program headers. */
  i_phdrp = (Elf_Internal_Phdr *)
    bfd_alloc (abfd, sizeof (*i_phdrp) * i_ehdrp->e_phnum);
  if (!i_phdrp)
    goto fail;

  elf_tdata (abfd)->phdr = i_phdrp;

  if (bfd_seek (abfd, i_ehdrp->e_phoff, SEEK_SET) == -1)
    goto fail;

  /* Read and convert to internal form. */
  for (phindex = 0; phindex < i_ehdrp->e_phnum; ++phindex)
    {
      Elf_External_Phdr x_phdr;
      if (bfd_read ((PTR) &x_phdr, sizeof (x_phdr), 1, abfd)
	  != sizeof (x_phdr))
	goto fail;

      elf_swap_phdr_in (abfd, &x_phdr, i_phdrp + phindex);
    }

  /* Process each program header. */
  for (phindex = 0; phindex < i_ehdrp->e_phnum; ++phindex)
    {
      if (! bfd_section_from_phdr (abfd, i_phdrp + phindex, phindex,
				   i_ehdrp))
	goto fail;
    }

  /* Set the machine architecture. */
  if (! bfd_default_set_arch_mach (abfd, ebd->arch, 0))
    {
      /* It's OK if this fails for the generic target.  */
      if (ebd->elf_machine_code != EM_NONE)
	goto fail;
    }

  /* Save the entry point from the ELF header. */
  bfd_get_start_address (abfd) = i_ehdrp->e_entry;

  /* Let the backend double check the format and override global
     information.  */
  if (ebd->elf_backend_object_p)
    {
      if ((*ebd->elf_backend_object_p) (abfd) == false)
	goto wrong;
    }

  return abfd->xvec;

wrong:
  bfd_set_error (bfd_error_wrong_format);
fail:
  if (i_phdrp != NULL)
    bfd_release (abfd, i_phdrp);
  if (new_tdata != NULL)
    bfd_release (abfd, new_tdata);
  elf_tdata (abfd) = preserved_tdata;
  return NULL;
}
