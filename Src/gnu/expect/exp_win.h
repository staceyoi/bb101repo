/* exp_win.h - window support

Written by: Don Libes, NIST, 10/25/93

This file is in the public domain.  However, the author and NIST
would appreciate credit if you use this file or parts of it.
*/

int exp_window_size_set(int fd);
int exp_window_size_get(int fd);

void exp_win_rows_set(char *rows);
void exp_win_rows_get(char *rows);
void exp_win_columns_set(char *columns);
void exp_win_columns_get(char *columns);

void exp_win2_rows_set(int fd, char *rows);
void exp_win2_rows_get(int fd, char *rows);
void exp_win2_columns_set(int fd, char *columns);
void exp_win2_columns_get(int fd, char *columns);
