/* Interface between the Java unwind share library and GDB. */
   

#if !defined (JAVALIB_H)
#define JAVALIB_H 1
#define JAVA_UNWIND_OK 0
#define JAVA_UNWIND_ERR -1

/* Use these register numbers when talking to gdb. */
#define GR0_REGNUM 320
#define BR0_REGNUM 448
#define PFS_REGNUM 520

/* Describe the basic frame info for stack unwinding. */

struct basic_frame_info {
  CORE_ADDR   pc;
  CORE_ADDR   sp;
  CORE_ADDR   fp; 

  /* extra for IA. */
  CORE_ADDR   bsp;
  CORE_ADDR   cfm;

  /* Pointer to extra info of a Java frame. GDB should save this ptr 
     to frame_info , and give the ptr back to libjunwind.sl when it 
     calls java_get_next_frame().  When GDB cleans up the frame_info, 
     it should free this ptr if ptr !=0.
   */
  void  *java_ptr;
};


/* The prototypes of the API functions that gdb can use to obtain Java 
   frame infomation. */

/* Given a frame pc, return 1 if it's a Java frame, otherwise return 0. */

extern int is_java_frame (CORE_ADDR  pc);


/* Given the current frame's basic_frame_info, fill in its previous frame info. 
   Return 1 if current is a Java frame, otherwise return 0. */

extern int get_prev_java_frame_info (struct basic_frame_info *current, 
				struct basic_frame_info *prev);


/* Given a frame's basic_frame_info, return a string that describe the frame
   info if it is a Java frame, otherwise return NULL. GDB need to print out 
   this string in a backtrace command and free the string space after using 
   it. */ 

extern char *get_java_frame_str (struct basic_frame_info *frame);


/* GDB needs to call this function for java unwind lib cleanup whenever 
   the target Java process starts to run again. */

extern void cleanup_java_lib (void);


/* The GDB functions that the Java unwind library needs to use. */

/* The general function for reading target memory. 
   Return 1 for success, 0 for unsuccess. */

extern int java_read_memory (CORE_ADDR memaddr, char *myaddr, int len);

extern int is_java_debug_version(void);

/* Get a general register value in a frame. On IA, for the topmost frame, any
 * register can be read, whereas, gdb will be able to provide reliable 
 * information only about the stacked registers (GR32 - num_of_stacked_regs)
 * in the lower frames. To access nth GR use the regnum GR0_REGNUM+n. Same 
 * with BRs.
 */

extern int java_get_saved_register (struct basic_frame_info *frame, int regnum,
                CORE_ADDR *reg);


/* Given any C/C++ expression, whose value is no more than 8 bytes
   return the value of the expression and the actual bytes of the value. */

int java_evaluate_expression (char *name, unsigned long long *reg);

/* Get the next frame of a given frame in a stack. */

extern int java_get_next_frame (struct basic_frame_info *frame, 
	struct basic_frame_info *next);

/* GDB tells if the given frame is the topmost frame in the stack? */

extern int is_topmost_frame (struct basic_frame_info *frame);

/* Sets the top_frm to tomost frame. Returns 1 if success.
  Returns 0 otherwise. */

extern int java_get_topmost_frame (struct basic_frame_info *top_frm);

/* GDB tells if the given frame is the caller of a signal handler. */

extern int is_next_frame_signal_handler (struct basic_frame_info *frame);

#endif    /* !define JAVALIB_H */
