/* Interface between the mixed mode aries helper library and GDB. */

#if !defined (MIXED_MODE_H)
#define MIXED_MODE_H 1

#include "defs.h"
#include <inttypes.h>
#include <machine/sys/param.h>
#include "parser-defs.h"

/*
 * jini: This is ugly since this is just the copy of the unwind_table_entry
 * structure definition that exists in tm-hppa.h. I am currently avoiding the
 * inclusion of tm-hppa.h so as to avoid tonnes of symbol clashes. 
 * Unwind table and descriptor.
 */

struct pa32_unwind_table_entry
  {
    CORE_ADDR region_start;
    CORE_ADDR region_end;

    unsigned int Cannot_unwind:1;	/* 0 */
    unsigned int Millicode:1;	/* 1 */
    unsigned int Millicode_save_sr0:1;	/* 2 */
    unsigned int Region_description:2;	/* 3..4 */
    unsigned int reserved1:1;	/* 5 */
    unsigned int Entry_SR:1;	/* 6 */
    unsigned int Entry_FR:4;	/* number saved *//* 7..10 */
    unsigned int Entry_GR:5;	/* number saved *//* 11..15 */
    unsigned int Args_stored:1;	/* 16 */
    unsigned int Variable_Frame:1;	/* 17 */
    unsigned int Separate_Package_Body:1;	/* 18 */
    unsigned int Frame_Extension_Millicode:1;	/* 19 */
    unsigned int Stack_Overflow_Check:1;	/* 20 */
    unsigned int Two_Instruction_SP_Increment:1;	/* 21 */
    unsigned int Ada_Region:1;	/* 22 */
    unsigned int cxx_info:1;	/* 23 */
    unsigned int cxx_try_catch:1;	/* 24 */
    unsigned int sched_entry_seq:1;	/* 25 */
    unsigned int reserved2:1;	/* 26 */
    unsigned int Save_SP:1;	/* 27 */
    unsigned int Save_RP:1;	/* 28 */
    unsigned int Save_MRP_in_frame:1;	/* 29 */
    unsigned int extn_ptr_defined:1;	/* 30 */
    unsigned int Cleanup_defined:1;	/* 31 */
    unsigned int MPE_XL_interrupt_marker:1;	/* 0 */
    unsigned int HP_UX_interrupt_marker:1;	/* 1 */
    unsigned int Large_frame:1;	/* 2 */
    unsigned int Pseudo_SP_Set:1;	/* 3 */
    unsigned int reserved4:1;	/* 4 */
    unsigned int Total_frame_size:27;	/* 5..31 */

    /* This is *NOT* part of an actual unwind_descriptor in an object
       file.  It is *ONLY* part of the "internalized" descriptors that
       we create from those in a file.
     */
    struct
      {
	unsigned int stub_type:4;	/* 0..3 */
	unsigned int padding:28;	/* 4..31 */
      }
    stub_unwind;
  };


struct pa64_unwind_table_entry
  {
    CORE_ADDR region_start;
    CORE_ADDR region_end;

    unsigned int Cannot_unwind:1;	/* 0 */
    unsigned int Millicode:1;	/* 1 */
    unsigned int reserved0:1;	/* 2 */
    unsigned int Region_description:2;	/* 3..4 */
    unsigned int reserved1:1;	/* 5 */
    unsigned int Entry_SR:1;	/* 6 */
    unsigned int Entry_FR:4;	/* number saved *//* 7..10 */
    unsigned int Entry_GR:5;	/* number saved *//* 11..15 */
    unsigned int Args_stored:1;	/* 16 */
    unsigned int reserved2:3;	/* 17..19 */
    unsigned int Stack_Overflow_Check:1;	/* 20 */
    unsigned int Two_Instruction_SP_Increment:1;	/* 21 */
    unsigned int reserved3:1;	/* 22 */
    unsigned int cxx_info:1;	/* 23 */
    unsigned int cxx_try_catch:1;	/* 24 */
    unsigned int sched_entry_seq:1;	/* 25 */
    unsigned int reserved4:1;	/* 26 */
    unsigned int Save_SP:1;	/* 27 */
    unsigned int Save_RP:1;	/* 28 */
    unsigned int Save_MRP_in_frame:1;	/* 29 */
    unsigned int reserved5:1;	/* 30 */
    unsigned int Cleanup_defined:1;	/* 31 */
    unsigned int reserved6:1;	/* 32 */
    unsigned int HP_UX_interrupt_marker:1;	/* 33 */
    unsigned int Large_frame:1;	/* 34 */
    unsigned int alloca_frame:1;	/* 35 */
    unsigned int reserved7:1;	/* 36 */
    unsigned int Total_frame_size:27;	/* 37..63 */
  };

/* The gaps represent linker stubs used in MPE and space for future
   expansion.  */
enum unwind_stub_types
  {
    LONG_BRANCH = 1,
    PARAMETER_RELOCATION = 2,
    LONG_BRANCH_MRP = 7,
    EXPORT = 10,
    IMPORT = 11,
    IMPORT_SHLIB = 12,
    LONG_BRANCH_SHLIB = 14,
  };

/* Have a single union to represent the 32 and the 64 bit PA unwind
 * structures. */
 
union hppa_unwind_table_entry {
  struct pa32_unwind_table_entry u32;
  struct pa64_unwind_table_entry u64;
};

/* The structures below have been copied from tm-hppa.h. */
struct hppa_obj_unwind_info
  {
    union hppa_unwind_table_entry *table;   /* Pointer to unwind info */
    union hppa_unwind_table_entry *cache;   /* Pointer to last entry we found */
    int last;                   /* Index of last entry */
  };

/* The opd_data structure is the same as that on IPF. */

typedef struct hppa_obj_private_struct
  {
    struct hppa_obj_unwind_info *unwind_info;        /* a pointer */
    struct so_list *so_info;    /* a pointer  */
    opd_data *opd;              /* a pointer  */
    int n_opd_entries;
  }
hppa_obj_private_data_t;

#define PA_FLAGS_REGNUM 0
#define PA_RP_REGNUM 2             /* return pointer */
#define PA_FP_REGNUM 3		/* Contains address of executing stack */
				/* frame */
#define PA_AP_REGNUM 29
#define PA_SP_REGNUM 30            /* Contains address of top of stack */
#define PA_FP0_REGNUM 64
#define PA32_NUM_REGS 128
#define PA64_NUM_REGS 96

/* Copy of macros from tm-hppa*.h. */
#define FRAME_SAVED_PC_SYSCALL_IN_SIGTRAMP(FRAME, TMP) \
{ \
  *(TMP) = read_memory_integer ((FRAME)->frame + (24 * 4) + 640 + (31 * 8), 8); \
}
                                                                                
#define FRAME_SAVED_PC_IN_SIGTRAMP(FRAME, TMP) \
{ \
  *(TMP) = read_memory_integer ((FRAME)->frame + (24 * 4) + 640 + (33 * 8), 8); \
}

#define FRAME_BASE_BEFORE_SIGTRAMP(FRAME, TMP) \
{ \
  *(TMP) = read_memory_integer ((FRAME)->frame + (24 * 4) + 640 + (30 * 8), 8); \
}

/* '4' below stands for the instruction size. */
#define REDIRECT_THROUGH_DUMMY_INST_LENGTH (4 * 11)
#define PC_IN_REDIRECT_THROUGH_DUMMY(pc) \
  (redirect_through_dummy_addr != UINT64_MAX && \
   (pc) >= redirect_through_dummy_addr && \
   (pc) < redirect_through_dummy_addr + REDIRECT_THROUGH_DUMMY_INST_LENGTH)

#if defined (EXTRA_FRAME_INFO) || defined (FRAME_FIND_SAVED_REGS)
struct mixed_mode_frame_saved_regs
  {
    /* Allocating a greater amount of space here. For PA64, the last few elements
       should be unused. */
    CORE_ADDR regs[PA32_NUM_REGS];
  };
#endif

#define PA32_REGISTER_BYTES (PA32_NUM_REGS * 4)
#define PA64_REGISTER_BYTES (PA64_NUM_REGS * 8)
#define PA32_FP4_REGNUM 72
#define PA64_FP4_REGNUM 68

/* Sizes (in bytes) of the PA unwind entries.  */
#define PA_UNWIND_ENTRY_SIZE 16
#define PA_STUB_UNWIND_ENTRY_SIZE 8
                                                                                    
#define PA32_REGISTER_NAMES  \
 {"flags",  "r1",      "rp",      "r3",    "r4",     "r5",      "r6",     "r7",    \
  "r8",     "r9",      "r10",     "r11",   "r12",    "r13",     "r14",    "r15",   \
  "r16",    "r17",     "r18",     "r19",   "r20",    "r21",     "r22",    "r23",   \
  "r24",    "r25",     "r26",     "dp",    "ret0",   "ret1",    "sp",     "r31",   \
  "sar",    "pcoqh",   "pcsqh",   "pcoqt", "pcsqt",  "eiem",    "iir",    "isr",   \
  "ior",    "ipsw",    "goto",    "sr4",   "sr0",    "sr1",     "sr2",    "sr3",   \
  "sr5",    "sr6",     "sr7",     "cr0",   "cr8",    "cr9",     "ccr",    "cr12",  \
  "cr13",   "cr24",    "cr25",    "cr26",  "mpsfu_high","mpsfu_low","mpsfu_ovflo","pad",\
  "fpsr",    "fpe1",   "fpe2",    "fpe3",  "fpe4",   "fpe5",    "fpe6",   "fpe7",  \
  "fr4",     "fr4R",   "fr5",     "fr5R",  "fr6",    "fr6R",    "fr7",    "fr7R",  \
  "fr8",     "fr8R",   "fr9",     "fr9R",  "fr10",   "fr10R",   "fr11",   "fr11R", \
  "fr12",    "fr12R",  "fr13",    "fr13R", "fr14",   "fr14R",   "fr15",   "fr15R", \
  "fr16",    "fr16R",  "fr17",    "fr17R", "fr18",   "fr18R",   "fr19",   "fr19R", \
  "fr20",    "fr20R",  "fr21",    "fr21R", "fr22",   "fr22R",   "fr23",   "fr23R", \
  "fr24",    "fr24R",  "fr25",    "fr25R", "fr26",   "fr26R",   "fr27",   "fr27R", \
  "fr28",    "fr28R",  "fr29",    "fr29R", "fr30",   "fr30R",   "fr31",   "fr31R"}

#define PA64_REGISTER_NAMES \
 {"flags",  "r1",      "rp",      "r3",    "r4",     "r5",      "r6",     "r7",    \
  "r8",     "r9",      "r10",     "r11",   "r12",    "r13",     "r14",    "r15",   \
  "r16",    "r17",     "r18",     "r19",   "r20",    "r21",     "r22",    "r23",   \
  "r24",    "r25",     "r26",     "dp",    "ret0",   "ret1",    "sp",     "r31",   \
  "sar",    "pcoqh",   "pcsqh",   "pcoqt", "pcsqt",  "eiem",    "iir",    "isr",   \
  "ior",    "ipsw",    "goto",    "sr4",   "sr0",    "sr1",     "sr2",    "sr3",   \
  "sr5",    "sr6",     "sr7",     "cr0",   "cr8",    "cr9",     "ccr",    "cr12",  \
  "cr13",   "cr24",    "cr25",    "cr26",  "mpsfu_high","mpsfu_low","mpsfu_ovflo","pad",\
  "fpsr",    "fpe1",   "fpe2",    "fpe3",  "fr4",    "fr5",     "fr6",    "fr7", \
  "fr8",     "fr9",    "fr10",    "fr11",  "fr12",   "fr13",    "fr14",   "fr15", \
  "fr16",    "fr17",   "fr18",    "fr19",  "fr20",   "fr21",    "fr22",   "fr23", \
  "fr24",    "fr25",   "fr26",    "fr27",   "fr28",  "fr29",    "fr30",   "fr31"}

#define IS_ARIES_PA2IA_PROCESS(name) \
        (name && STREQ ("aries_pa2ia_process", name))

/* The general function for reading target memory. 
   Return 1 for success, 0 for unsuccess. */

extern int mixed_mode_read_memory (CORE_ADDR memaddr, char *myaddr, int len);

int mixed_mode_map_name_to_register (char *str, int len);

void
mixed_mode_get_saved_register (char *raw_buffer, int *optimized,
                               CORE_ADDR *addrp, struct frame_info *frame,
                               int regnum, enum lval_type *lval);

void
mixed_mode_do_registers_info (int regnum, int fpregs);

#endif    /* !define MIXED_MODE_H */
