#ifndef RTC_H
#define RTC_H

/* srikanth, 991001, stuff common to infrtc.c (inferior code) and
   gdbrtc.c (superior code)
*/

enum root_set {
  PROCESS_STACK,
  DATA_SPACE,
  REGISTER_SET,
  SBA,
  HEAP,
  NON_HEAP
};

typedef struct dangling_ptrs_struct
{
  enum root_set location;
  char *ptr;
} dangling_ptrs_struct;

typedef struct dangling_ptrs
{
  int size;
  int count;
  dangling_ptrs_struct *ptrs;
} dangling_ptrs;

/* Suresh, Jan 08: To hold the pc & library index lists. */
struct pc_tuple {
      void *pc; /* To hold the stack trace pc */
      int library_index_of_pc; /* library index */
};

/* For each allocated block in the heap, we gather some information
   into a chunk_info structure as described below. These chunk_infos
   are allocated separately (not as a part of the block itself) for
   a number of reasons :

   (1) We don't want buggy programs to corrupt the chunk_info : Well,
       buggy programs can corrupt most anything kept anywhere :-) but
       the odds are better for us this way.

   (2) This separation allows us to shutdown RTC at will and free up
       the resources. This is not done as of now, but could be taken
       up in the future.

   (3) Third and most important, it allows us to treat all allocation
       primitives in a uniform manner : For example, the allocation
       functions valloc() and mmap() are supposed to return a block
       aligned at a page boundary. There is no easy way we could do
       that if we prepend the chunk_info to the block itself. It is
       impossible to append the chunk_info the block.

   (4) Certain operations should have better paging and caching
       behavior this way. For example, after the mark step, when
       we sweep, we will "touch" fewer pages.
*/

typedef struct rtc_chunk_info {

    /* kilroy was here ?  */
    unsigned scanned     : 1;
    
    /* We intercept and record all mmaps, but scan only 
       anonymous mmap regions.
       This flag indicates that this chunk need not be
       scanned though it is present in the chunk_info list.
       JAGag22951 -- mithun */
    unsigned do_not_scan:   1;

    /* lest we flog a dead horse ... */
    unsigned old_leak    : 1;

    /* Is this a heap block that the application could free ? For the
       sake of simplicity, we treat mmapped regions and sbroken regions
       as though they are mallocated blocks. However if the user
       attempts to free any of these, we don't want to be caught
       napping.
    */
    unsigned heap_block  : 1;

    /* Is this block padded with guard bytes at both ends ? Only
       heap blocks allocated via malloc, calloc, or realloc are paddable.
       For the sake of simplicity, we treat mmapped regions, shmat blocks
       and sbroken regions as though they are mallocated blocks. However
       we don't want to write cookie bytes in the "red zone" in these cases.
       Likewise for valloc blocks. Attempting to allocate pad bytes in all
       these cases will lead to alignment problems.
    */
    unsigned padded_block  : 1;
    
    /* JAGae73849: Keeps track of allocation that were done even before 
       librtc can be initialized. These were extracted from the kernel using 
       pstat_getprocvm in function find_mmaps_in_startup. These allocations
       are explicitly marked during mark phase.
    */ 
    unsigned preinit_mmap  : 1;

    unsigned new  : 1;

    /* JAGag43560: This is to keep track of blocks that have been freed. This field 
       is only used when the dangling check option is set
    */
    unsigned freed_block  : 1;

    /* If this identifies a leak, where are its kith and kin ? While
       this may appear to be wasteful, it is necessitated by a need to
       avoid mallocation while collecting garbage. There is no telling
       where the other threads are at that time and if any of them
       is stopped inside a thread-safe mallocator, the garbage
       collector may never halt and that would be a very bad thing.
    */ 
    struct rtc_chunk_info * next_leak;

    /* Where is the chunk_info for the next block in the page ? */
    struct rtc_chunk_info * next;

    /* Pointer to the first byte of mallocated block of space. We
       can safely assume that it is aligned suitably to hold a
       pointer. 
    */
    char * base;        

    /* Pointer *past* the last byte. We can't assume anything
       about its alignment. *end is certainly, absolutely, most
       definitely not a part of this block.
    */
    char * end;         

    /* The allocating stack trace pc & corresponding library index.*/
    struct  pc_tuple *pc_tuple;

    /* record pointers to dangling blocks. */
    dangling_ptrs *dptr;

} rtc_chunk_info;

/* Event codes for internal as well as external errors. When the
   RTC module finds something amiss, it calls the function rtc_event()
   and passes one of the event codes below. The debugger should have
   a breakpoint in this routine to detect errors.
*/

enum rtc_event {
    RTC_FCLOSE_FAILED = -5,
    RTC_FOPEN_FAILED = -4,
    RTC_UNSAFE_NOW = -3,
    RTC_MUTEX_LOCK_FAILED = -2,
    RTC_NOT_RUNNING = -1,
    RTC_NO_ERROR = 0,
    RTC_NO_MEMORY,
    RTC_BAD_FREE,
    RTC_BAD_REALLOC,
    RTC_ALLOCATED_WATCHED_BLOCK,
    RTC_DEALLOCATED_WATCHED_BLOCK,
    RTC_SBRK_NEGATIVE,
    RTC_STACK_MISSING,  /* not missing so much as we can't locate it */
    RTC_MALLOC_MISSING,
    RTC_BAD_HEADER,
    RTC_BAD_FOOTER,
    RTC_HUGE_BLOCK,
    RTC_HEAP_GROWTH,
    RTC_NOMEM, /* if null-check is enabled and malloc returns null */
    RTC_MEM_NULL, /* JAGaf48255 - gdb should report if malloc returns null pointer */
    RTC_BAD_MEMCPY,  /* JAGaf74522 */
    RTC_BAD_MEMCCPY,
    RTC_BAD_MEMSET,
    RTC_BAD_MEMMOVE,
    RTC_BAD_BZERO,
    RTC_BAD_BCOPY,
    RTC_BAD_STRCPY,
    RTC_BAD_STRNCPY,
    RTC_BAD_STRCAT,
    RTC_BAD_STRNCAT,

    RTC_DISABLED,
    RTC_NO_LIBRTC,
    RTC_NO_STACK,
    RTC_NO_RECORDS,
    RTC_IN_SYSCALL, 
    RTC_HIGH_MEM,  /* JAGaf87040 */ 

    /* Thread related events */
    RTC_THREAD_EVENT_START,
    RTC_NUM_WAITERS,
    RTC_NON_RECURSIVE_RELOCK,
    RTC_UNLOCK_NOT_OWN,
    RTC_MIXED_SCHED_POLICY,
    RTC_CONDVAR_MULTIPLE_MUTEXES,
    RTC_CONDVAR_WAIT_NO_MUTEX,
    RTC_DELETED_OBJ_USED,
    RTC_THREAD_EXIT_OWN_MUTEX,
    RTC_THREAD_EXIT_NO_JOIN_DETACH,
    RTC_THREAD_STACK_UTILIZATION,
    RTC_THREAD_EVENT_END,

    /* New events for enhanced corruption checks */
    RTC_FREED_BLOCK_CORRUPTED,
    /* New events for salvage freed blocks*/
    RTC_INTERNAL_GC,
    RTC_API_OOB_WRITE,
    RTC_API_OOB_READ
};

#define DEFAULT_MIN_LEAK_SIZE 0
#define DEFAULT_FRAME_COUNT 4
#define RTC_MAX_THREADS (8 * 1024)
#define DEFAULT_REPEAT_CNT  100
#define DANGLING_PTRS_SIZE 20

/* From gdbrtc.c */
extern void prepare_for_rtc ( int );
extern boolean threaded_program ( void );
extern void snoop_on_the_heap ( void );
extern void decode_nomem_event ( void );
extern void print_context_for_overrun ( char *, CORE_ADDR , size_t , int );
extern void decode_rtc_event ( void );
extern void set_rtc_catch_nomem ( void );
extern void _initialize_gdbrtc ( void );
extern void decode_rtc_compiler_event (void);

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
#define DEFAULT_HEADER_SIZE 16
#define DEFAULT_FOOTER_SIZE 1
#else
#define DEFAULT_HEADER_SIZE 8
#define DEFAULT_FOOTER_SIZE 1
#endif

enum {
    RTC_CWD,		/* idx for current working dir */
    RTC_CONFIG_FILE,    /* idx for configuration file rtcconfig */
    RTC_START_TIME,     /* idx for keeping start time for batch RTC */
    RTC_FILE,           /* idx for list of file names to be traced */
    RTC_OUTPUT_DIR,     /* idx for output dir  */
    RTC_LAST            /* last element of enum struct */
};

#endif

