/* Implement a cached obstack.
   Written by Fred Fish (fnf@cygnus.com)
   Rewritten by Priya Raghunath(praghuna@cup.hp.com)
   Copyright 1995,2001 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "obstack.h"
#include "bcache.h"
#include "gdb_string.h"

/*
 * srikanth, 990314, JAGaa80452, the bcache seems to be an extremely high
 * overhead data structure. See that the overhead (which is really every
 * byte that is not used to store GDB's data i.e., cells used for house
 * keeping info like pointers, hash chain heads etc.,) is of the order of
 * O(m * n * 64k) where m is the number of load modules compiled with -g,
 * and n is the number of strings that have unique length. This spells doom
 * for applications wih a large number of shared libraries (m increases)
 * and C++ (n increases since we also stick demangled names into the cache.)
 * When debugging HP's C compiler more than 140 MB or about 48% of memory is
 * due to the bcache overhead. Not the data just the overhead !
 *
 * Further research, communication with Cygnus and Fred Fish (the original
 * implementor) shows that this module is extremely effective saving as much
 * as 60% in gcc + stabs + linux combination. So I am disabling it just for
 * the Wildebeest and only for non-doom mode.
 * Priya-Check whether during merge it is better to disable it for PA.
 *
 */

/* The old hash function was stolen from SDBM. This is what DB 3.0 uses now,
 * and is better than the old one. 
 */

unsigned long
hash(void *addr, int length)
{
		const unsigned char *k, *e;
		unsigned long h;
		
		k = (const unsigned char *)addr;
		e = k+length;
		for (h=0; k< e;++k)
		{
				h *=16777619;
				h ^= *k;
		}
		return (h);
}

/*
 * Priya, 072601 ,every time a psymbol is encountered ,lookup_cache is called
 * to check if the symbol already exists 
 */
static void* 
lookup_cache (void *bytes, int count, 
	unsigned long int hashval, struct bcache *bcachep)
{
    void *location = NULL;
    struct hashlink **hashtablep;
    struct hashlink *linkp;
    unsigned int hashval1 = (unsigned int) (hashval % BCACHE_HASH1_SIZE);
    unsigned int hashval2 = (unsigned int) (hashval % BCACHE_HASH2_SIZE);
    
    hashtablep = bcachep->hashtable[hashval1];
    if (hashtablep)
    {
	linkp = hashtablep[hashval2];
	while (linkp)
	{
	    if ((*((char *) (BCACHE_DATA (linkp))) == *((char *) bytes))
	    	    ? (memcmp (BCACHE_DATA (linkp), bytes, count) == 0)
	    	    : 0)
	    {
		location = BCACHE_DATA (linkp);
		break;
	    }
	    linkp = linkp->next;
	}
    }
    return (location);
}

/* 
 * Priya 072601 bcache uses a open hash table approach where each element in 
 * the hash table is the head of a linked list of symbols .We use a index hash 
 * table to access the hash table buckets.Both the index hash table and the
 *  hash tables are addressed using hash values.Note that bcache needs to 
 * be initialised to 0.
 */

void* 
bcache(void *bytes, int count, struct bcache *bcachep)
{
    void *location;
    struct hashlink *newlink;
    struct hashlink **linkpp;
    struct hashlink ***hashtablepp;
    int i;
    unsigned char *name = (unsigned char *) bytes;
    unsigned long int hashval = 0, g;


/* Priya 072601 This hash function is a variant of HashPJW() that is used in
 * UNIX object files that use the ELF format.This hash  function accept a 
 * pointer to a string and return an integer.The final hash key is the modulo 
 * generated by dividing the returned integer by the size of the table.
 */

    for (i = 0; i < count; i++)
    {
	hashval += ( hashval << 4 ) + name[i];
	if ( g = hashval & 0xF0000000 )
	    hashval ^= g >> 24;
	 hashval &= ~g;
    }

    location = lookup_cache(bytes, count, hashval, bcachep);
    if (!location)
    {
	unsigned int hashval1 = (unsigned int) (hashval % BCACHE_HASH1_SIZE);
	unsigned int hashval2 = (unsigned int) (hashval % BCACHE_HASH2_SIZE);
	bcachep->cache_misses++;
	hashtablepp = &bcachep->hashtable[hashval1];
	if (*hashtablepp == NULL)
	{
	    *hashtablepp = (struct hashlink **)(void *)
		obstack_alloc (&bcachep->cache, BCACHE_HASH2_SIZE * sizeof (struct hashlink *));
	    bcachep->cache_bytes += BCACHE_HASH2_SIZE * sizeof (struct hashlink *);
	    memset (*hashtablepp, 0, BCACHE_HASH2_SIZE * sizeof (struct hashlink *));
	}
	linkpp = &(*hashtablepp)[hashval2];
	newlink = (struct hashlink *)(void *) 
	    obstack_alloc (&bcachep->cache, BCACHE_DATA_ALIGNMENT + count);
	bcachep->cache_bytes += BCACHE_DATA_ALIGNMENT + count;
	memcpy (BCACHE_DATA(newlink), bytes, count);
	newlink->next = *linkpp;
	*linkpp = newlink;
	location = BCACHE_DATA (newlink);
    }	
    else
    {
	bcachep->cache_savings += count;
	bcachep->cache_hits++;
    }
    return (location);
}

void
print_bcache_statistics (struct bcache *bcachep, char *id)
{
    int h1idx,h2idx, temp;
    int hcount = 0, lcount = 0, ocount = 0, tcount=0,lmax=0, lmaxh=0, lmaxt=0;
    struct hashlink **hash1p;
    struct hashlink *linkp;
    
for (h1idx = 0; h1idx < BCACHE_HASH1_SIZE; h1idx++)
    {
        hash1p = bcachep->hashtable[h1idx];
        if (hash1p)
        {
            tcount++;
            for (h2idx = 0; h2idx < BCACHE_HASH2_SIZE; h2idx++)
            {
                linkp = hash1p[h2idx];
                if (linkp!=NULL)
                {
                    hcount++;
                    for (temp = 0; linkp != NULL; linkp = linkp->next)
                    {
                	lcount++;
                	temp++;
                	if (temp > 1)
                    		ocount ++;
                     }
                    
                    if (temp > lmax)
                    {
                        lmax = temp;
                        lmaxt = h1idx;
                        lmaxh = h2idx;
                    }
                }
             }
         }
      }
    printf_filtered ("  Cached '%s' statistics:\n", id);
    printf_filtered ("    Cache hits: %d\n", bcachep->cache_hits);
    printf_filtered ("    Cache misses: %d\n", bcachep->cache_misses);
    printf_filtered ("    Cache hit ratio: ");
    if (bcachep->cache_hits + bcachep->cache_misses > 0)
    {
        printf_filtered ("%d%%\n", ((bcachep->cache_hits) * 100) /
                (bcachep->cache_hits + bcachep->cache_misses));
    }
    else
    {
        printf_filtered ("(not applicable)\n");
    }
    printf_filtered ("    Space used for caching: %d\n", bcachep->cache_bytes);
    printf_filtered ("    Space saved by cache hits: %d\n", bcachep->cache_savings);
    printf_filtered ("    Number of bcache overflows: %d\n", bcachep->bcache_overflows);
    printf_filtered ("    Number of hash table buckets used: %d\n", hcount);
    printf_filtered ("    Number of index buckets used: %d\n", tcount);
    printf_filtered ("    Number of chained items: %d\n", lcount);
    printf_filtered ("    Average hash table population: ");
    if (tcount > 0)
    {
    printf_filtered ("%d%%\n", (hcount * 100) / (tcount * BCACHE_HASH2_SIZE));
    }
    else
    {
    printf_filtered ("(not applicable)\n");
    }
    printf_filtered ("    Average chain length ");
    if (hcount > 0)
    {
        printf_filtered ("%d\n", lcount / hcount);
    }
    else
    {
        printf_filtered ("(not applicable)\n");
    }
    printf_filtered ("    Maximum chain length %d at %d:%d\n", lmax, lmaxt, lmaxh);
    printf_filtered ("    Number of elements not at the head of link list %d \n"
, ocount);
}

