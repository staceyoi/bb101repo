/* Include file cached obstack implementation.
   Written by Fred Fish (fnf@cygnus.com)
   Rewritten by Priya Raghunath (praghuna@cup.hp.com)
   Copyright 1995,2001 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#ifndef BCACHE_H
#define BCACHE_H

/* 072601 Priya-The widest dispersal of modulo results will occur when the 
 * hash value and the table size are relatively prime (no common factors).
 * Keeping that into mind and the classic memory-for-speed trade-offs ,
 * these choice of numbers have been arrived at.
 * BCACHE_HASH1_SIZE defines the size of the index table and BCACHE_HASH2_SIZE
 * defines the size of the hash tables.
*/

#define BCACHE_HASH1_SIZE 4097 
#define BCACHE_HASH2_SIZE 307  

/* Note that the user data is stored in data[].  Since it can be any type,
   it needs to have the same alignment  as the most strict alignment of
   any type on the host machine.  So do it the same way obstack does. */

struct hashlink
{
    struct hashlink *next;
    union
    {
	char data[1];
	double dummy;
    } d;
};

/* BCACHE_DATA is used to get the address of the cached data. */
#define BCACHE_DATA(p) ((p)->d.data)

/* BCACHE_DATA_ALIGNMENT is used to get the offset of the start of
   cached data within the hashlink struct.  This value, plus the
   size of the cached data, is the amount of space to allocate for
   a hashlink struct to hold the next pointer and the data. */

#define BCACHE_DATA_ALIGNMENT \
        (((char *) BCACHE_DATA((struct hashlink*) 0) - (char *) 0))
    
struct bcache
{
    struct obstack cache;
    struct hashlink **hashtable[BCACHE_HASH1_SIZE];
    int cache_hits;
    int cache_misses;
    int cache_bytes;
    int cache_savings;
    int bcache_overflows;
};


extern void* 
  bcache PARAMS ((void *bytes, int count, struct bcache *bcachep));

/* The hash function */
extern unsigned long hash(void *addr, int length);

#if MAINTENANCE_CMDS

extern void
print_bcache_statistics PARAMS ((struct bcache *, char *));

#endif /* MAINTENANCE_CMDS */

#endif /* BCACHE_H */
