/* Get info from stack frames;
   convert between frames, blocks, functions and pc values.
   Copyright 1986, 87, 88, 89, 91, 94, 95, 96, 97, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "symtab.h"
#include "bfd.h"
#include "symfile.h"
#include "objfiles.h"
#include "frame.h"
#include "gdbcore.h"
#include "value.h"		/* for read_register */
#include "target.h"		/* for target_has_stack */
#include "inferior.h"		/* for read_pc */
#include "annotate.h"
#include "javalib.h"		/* for Java stacktrace */
#include <strings.h>		/* for bzero */

#include <ctype.h>
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#define DEBUG(x) 

#ifdef HP_IA64
/* jini: QXCR1000875632: Limit the nbr of stack frames to be displayed to avoid
   gdb running out of memory on encountering an infinite stack trace. */
#define MAX_NBR_STACK_FRAMES 10000
extern bool unwind_all_frames;
int total_stack_frames = 0;
#endif

int java_debugging = 0; /* governs whether we are debugging mixed Java/C/C++ */

#ifdef HP_IA64
#include "pa_save_state.h"
#include <assert.h>
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
extern bool mixed_mode_pa_unwind;
extern int mixed_mode_debug_aries;
extern CORE_ADDR inferior_aries_text_start;
extern CORE_ADDR inferior_aries_text_end;
extern CORE_ADDR aries_pa2ia_process_addr;
extern int context_nbr;
extern void mixed_mode_init_extra_frame_info (int, struct frame_info *);
extern void mixed_mode_init_frame_ap (struct frame_info *);
extern int mixed_mode_frameless_function_invocation (struct frame_info *);
struct frame_info * get_mixed_mode_pa_frame (struct frame_info  *trailing);
#endif

/* from ia64-tdep.c or hppa-tdep.c */
extern CORE_ADDR frame_chain (struct frame_info * frame);
extern CORE_ADDR frame_saved_pc (struct frame_info *);
extern void do_handle_bad_pc (struct frame_info * frame, 
		struct frame_info * prev);
extern int pc_in_user_sendsig(CORE_ADDR pc);
extern void init_extra_frame_info (int fromleaf, struct frame_info *frame);
extern void do_java_stack_unwind (struct frame_info * next_frame,
		struct frame_info * prev);
extern int frameless_function_invocation (struct frame_info *frame);
extern int frame_chain_valid (CORE_ADDR chain, struct frame_info *frame);
extern void ia64_frame_find_saved_regs (struct frame_info *,
				struct frame_saved_regs *);

extern int next_command_inline_idx;
extern int step_start_function_inline_index;
extern int executing_next_command;

// JAGag29592
extern struct frame_info *currently_printing_frame;

// watchpoint
#ifdef HP_IA64
extern boolean reached_main_in_frame_chain;
#endif

extern boolean print_bad_pc_unwind_msg;

/* Prototypes for exported functions. */

void _initialize_blockframe (void);

/* A default FRAME_CHAIN_VALID, in the form that is suitable for most
   targets.  If FRAME_CHAIN_VALID returns zero it means that the given
   frame is the outermost one and has no caller. */

int
file_frame_chain_valid (CORE_ADDR chain, struct frame_info *thisframe)
{
  return ((chain) != 0
	  && !inside_entry_file (FRAME_SAVED_PC (thisframe)));
}

/* Use the alternate method of avoiding running up off the end of the
   frame chain or following frames back into the startup code.  See
   the comments in objfiles.h. */

int
func_frame_chain_valid (CORE_ADDR chain, struct frame_info *thisframe)
{
  return ((chain) != 0
	  && !inside_main_func ((thisframe)->pc)
	  && !inside_entry_func ((thisframe)->pc));
}

/* A very simple method of determining a valid frame */

int
nonnull_frame_chain_valid (CORE_ADDR chain, struct frame_info *thisframe)
{
  return ((chain) != 0);
}
/* Is ADDR inside the startup file?  Note that if your machine
   has a way to detect the bottom of the stack, there is no need
   to call this function from FRAME_CHAIN_VALID; the reason for
   doing so is that some machines have no way of detecting bottom
   of stack. 

   A PC of zero is always considered to be the bottom of the stack. */

int
inside_entry_file (CORE_ADDR addr)
{
  if (addr == 0)
    return 1;
  if (symfile_objfile == 0)
    return 0;
  if (CALL_DUMMY_LOCATION == AT_ENTRY_POINT)
    {
      /* Do not stop backtracing if the pc is in the call dummy
         at the entry point.  */
      /* FIXME: Won't always work with zeros for the last two arguments */
      if (PC_IN_CALL_DUMMY (addr, 0, 0))
	return 0;
    }
  return (addr >= symfile_objfile->ei.entry_file_lowpc &&
	  addr < symfile_objfile->ei.entry_file_highpc);
}

/* Test a specified PC value to see if it is in the range of addresses
   that correspond to the main() function.  See comments above for why
   we might want to do this.

   Typically called from FRAME_CHAIN_VALID.

   A PC of zero is always considered to be the bottom of the stack. */

int
inside_main_func (CORE_ADDR pc)
{
  if (pc == 0)
    return 1;
  if (symfile_objfile == 0)
    return 0;

  pc = SWIZZLE(pc);

  /* If the addr range is not set up at symbol reading time, set it up now.
     This is for FRAME_CHAIN_VALID_ALTERNATE. I do this for coff, because
     it is unable to set it up and symbol reading time. */

  if (symfile_objfile->ei.main_func_lowpc == INVALID_ENTRY_LOWPC &&
      symfile_objfile->ei.main_func_highpc == INVALID_ENTRY_HIGHPC)
    {
      struct symbol *mainsym;

      mainsym = lookup_symbol (default_main, NULL, VAR_NAMESPACE, NULL, NULL);
      if (mainsym && SYMBOL_CLASS (mainsym) == LOC_BLOCK)
	{
	  symfile_objfile->ei.main_func_lowpc =
	    SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (mainsym)));
	  symfile_objfile->ei.main_func_highpc =
	    SWIZZLE(BLOCK_END (SYMBOL_BLOCK_VALUE (mainsym)));
	}
    }
  return (symfile_objfile->ei.main_func_lowpc <= pc &&
	  symfile_objfile->ei.main_func_highpc > pc);
}

/* Test a specified PC value to see if it is in the range of addresses
   that correspond to the process entry point function.  See comments
   in objfiles.h for why we might want to do this.

   Typically called from FRAME_CHAIN_VALID.

   A PC of zero is always considered to be the bottom of the stack. */

int
inside_entry_func (CORE_ADDR pc)
{
  if (pc == 0)
    return 1;
  if (symfile_objfile == 0)
    return 0;
  if (CALL_DUMMY_LOCATION == AT_ENTRY_POINT)
    {
      /* Do not stop backtracing if the pc is in the call dummy
         at the entry point.  */
      /* FIXME: Won't always work with zeros for the last two arguments */
      if (PC_IN_CALL_DUMMY (pc, 0, 0))
	return 0;
    }
  return (symfile_objfile->ei.entry_func_lowpc <= pc &&
	  symfile_objfile->ei.entry_func_highpc > pc);
}

/* Info about the innermost stack frame (contents of FP register) */

static struct frame_info *current_frame;

/* Cache for frame addresses already read by gdb.  Valid only while
   inferior is stopped.  Control variables for the frame cache should
   be local to this module.  */

static struct obstack frame_cache_obstack;

void *
frame_obstack_alloc (unsigned long size)
{
  return obstack_alloc (&frame_cache_obstack, size);
}

void
frame_saved_regs_zalloc (struct frame_info *fi)
{
  fi->saved_regs = (CORE_ADDR *)
    frame_obstack_alloc (SIZEOF_FRAME_SAVED_REGS);
  memset (fi->saved_regs, 0, SIZEOF_FRAME_SAVED_REGS);
}


/* Return the innermost (currently executing) stack frame.  */

struct frame_info *
get_current_frame ()
{
  if (current_frame == NULL)
    {
      if (target_has_stack)
        {
#ifdef HP_IA64
          /* Unwinding is (re)started here. (Re)initialize mixed mode
           * unwind related variables. */
          if (!mixed_mode_debug_aries)
            {
               context_nbr = -2;
               mixed_mode_pa_unwind = false;
            }
          /* jini: QXCR1000875632: (Re)initialize total_stack_frames to 0. */
          total_stack_frames = 0;
#endif
	  current_frame = create_new_frame (read_fp (), read_pc (), 0);
#ifdef HP_IA64
          if (!mixed_mode_debug_aries)
            {
               current_frame = get_mixed_mode_pa_frame (current_frame);
            }
#endif
        }
      else
	error ("No stack.");
    }
  return current_frame;
}

void
set_current_frame (struct frame_info *frame)
{
  current_frame = frame;
}

/* if the pc passed in is in inlined context, this function creates a list
   of inlined functions. 

   It uses the line table to create the frames for the call graph of inline
   functions. If A -->B -->C -->D where B, C and D are inline functions, this
   function will create 3 inline frames for B, C and D. If D is the inline
   function where the control is currently, D->prev = C, C->prev = B and,
   finally, B->prev = A. A->next is B and B->next is C  and so on.

  We use context_ref entry in a line table to find the caller context. The pc 
  of the caller is the pc of the line table entry indexed using context_ref.

 At the end, this function returns the pc of the bottom most inline frame. In 
 the above exampe it will be the pc of caller of B.
*/

/* Srikanth, moved this inline frame creation/manipulation functions
   from ia64-tdep.c to here. This implementation is common code for
   both PA & IA64.
*/
#ifdef INLINE_SUPPORT
static CORE_ADDR
create_inline_frames (struct frame_info *prev, struct frame_info **inline_frame_top,
		      struct frame_info **inline_frame_bot)
{

  CORE_ADDR frame;
#ifdef HP_IA64
  CORE_ADDR rse_fp;
#endif

  /* holds the pc of first non-inlined fn */
  CORE_ADDR curr_pc; 

  int curr_inline_idx;
  int inline_frame_cnt = 0;
  struct frame_info *inline_frame = NULL;
  struct symtab_and_line curr_sal;
#ifdef GDB_TARGET_IS_HPPA_20W
  extern int hppa_pc_in_gcc_function (CORE_ADDR);
#endif
  if (!inline_debugging)
    return 0;

  curr_pc = prev->pc;
#ifdef GDB_TARGET_IS_HPPA_20W
  if (hppa_pc_in_gcc_function (curr_pc))
    return 0;
#endif
  frame = prev->frame;
#ifdef HP_IA64
  rse_fp = prev->rse_fp;
#endif
  inline_frame = *inline_frame_top =  *inline_frame_bot = NULL;

  /* prev is not the topmost frame */
  /* pc is past the call site. Decrement by 1 */
  if (prev->next)
    {
      if (!prev->next->inline_idx) 
        {
#ifdef HP_IA64
          curr_pc -= (curr_pc % BUNDLE_SIZE) > 0 ? 1 : BUNDLE_SIZE - 2;
#else
          curr_pc --;
#endif
	}
      curr_sal = find_pc_inline_idx_line (curr_pc, -1, 0);
      /* If the callee is not inlined, we want to retain the prev->pc 
         as the pc of this frame. */
      if (!prev->next->inline_idx)
        {
           curr_sal.pc = prev->pc;
        }
    }
  else
    {
      /* Find the inline_idx of the breakpoint at the topmost frame's pc
	 if there exists one. */
      curr_inline_idx = bpstat_find_inline_idx (&prev->pc);
      if (curr_inline_idx == -1)
        {
	  /* No breakpoint here. */
          int nxt_idx;
	  if (executing_next_command)
	    {
	      /* Pick sal from current function or from the callers. */
	      nxt_idx = next_command_inline_idx;
              curr_sal = find_pc_inline_idx_line (curr_pc, nxt_idx, 0);
              while (curr_sal.line == 0 && nxt_idx > 0)
                {
                  nxt_idx = get_inline_contextref_idx (nxt_idx);
                  curr_sal = find_pc_inline_idx_line (curr_pc, nxt_idx, 0);
                }
	    }
	  else if (step_start_function_inline_index >= 0)
	    {
	      /* Stepping.
		 First check in the same function.
                 if no, check in the callers. (WE may have just stepped out of this
                 function.
                 if not, check in the callees, (We may have just stepped into a new routine. */
	      
	      /* Pick sal from current or caller */
	      nxt_idx = step_start_function_inline_index;
	      curr_sal = find_pc_inline_idx_line (curr_pc, nxt_idx, 0);
	      if ((curr_sal.line != 0 && curr_sal.pc == curr_pc) || curr_sal.line == 0)
	        {
	          while (curr_sal.line == 0 && nxt_idx > 0)
                    {
                      nxt_idx = get_inline_contextref_idx (nxt_idx);
                      curr_sal = find_pc_inline_idx_line (curr_pc, nxt_idx, 0);
                    }
	        }
	      else
	        {
		  /* If we are stopped in the middle of the line in the current function
		     better check the callee to see if we have stepped into a new function.
		     We don't want to stop in the middle of the line if there is a call here. */
		  curr_sal.line = 0;
		}

	      /* Now check in the callee. We may have stepped into a new function. 
	         Pick the outermost callee. */
	      if (curr_sal.line == 0)
		{
		  curr_sal = find_pc_inline_idx_line (curr_pc, -1, 0);
		  if (curr_sal.inline_idx > step_start_function_inline_index) 
		    {
                      nxt_idx = get_inline_contextref_idx (curr_sal.inline_idx);
		      while (   nxt_idx > 0
			     && nxt_idx != step_start_function_inline_index)
		        {
		          curr_sal = find_pc_inline_idx_line (curr_pc, nxt_idx, 0);
		          if (curr_sal.inline_idx == 0 || curr_sal.line == 0)
			    break;
                          nxt_idx = get_inline_contextref_idx (curr_sal.inline_idx);
		        }
		    }
		}
	      if (curr_sal.line == 0)
		curr_sal = find_pc_inline_idx_line (curr_pc,
		 step_start_function_inline_index, 0);
	    }
	  else
	    curr_sal = find_pc_inline_idx_line (curr_pc, -1, 0);
        }
      else
	{
	  /* Breakpoint */
          curr_sal = find_pc_inline_idx_line (curr_pc, curr_inline_idx, 0);
	}
    }

  /* Not inlined. */
  if (curr_sal.inline_idx == 0)
    return 0;

  curr_inline_idx = curr_sal.inline_idx;

  while (curr_inline_idx > 0)
    {
      inline_frame = (struct frame_info *)
            frame_obstack_alloc (sizeof (struct frame_info));
      bzero (inline_frame, sizeof (struct frame_info));

      inline_frame_cnt++; /* nested inline frames */
      inline_frame->pc = curr_pc;
      inline_frame->frame = frame;
#ifdef HP_IA64
      inline_frame->rse_fp = rse_fp;
#endif
      inline_frame->inline_idx = curr_inline_idx;

      /* update top for the first frame only */
      if (inline_frame_cnt == 1) 
         *inline_frame_top = *inline_frame_bot = inline_frame;

      if (*inline_frame_bot != inline_frame)
        {
          (*inline_frame_bot)->prev = inline_frame;
          inline_frame->next = (*inline_frame_bot);
        }
      
      /* bottom ptr to the newly allocated frame */
      *inline_frame_bot = inline_frame;

      if( (curr_pc = get_inline_actual_contextref_pc( curr_inline_idx )) == 0 ) {
          curr_pc = get_inline_contextref_pc (curr_inline_idx);
      }
      curr_inline_idx = get_inline_contextref_idx (curr_inline_idx);

    } /* end of while */
  
  return inline_frame_cnt > 0 ? curr_pc : 0;
}

/* Sets up the next inlined frame and returns true if the current
   frame is not the tomost frame. Used to fake a step.
   Returns true if a next frame can be set up. */
boolean
set_next_inline_frame ()
{

  CORE_ADDR frame;
#ifdef HP_IA64
  CORE_ADDR rse_fp;
#endif
  CORE_ADDR next_pc = 0;
  CORE_ADDR curr_pc;

  int curr_inline_idx;
  int next_inline_idx;
  struct frame_info *inline_frame = NULL;
  struct frame_info *curr_frame = get_current_frame ();
  struct symtab_and_line curr_sal;

  if (!inline_debugging)
    return false;

  curr_pc = curr_frame->pc;
  frame = curr_frame->frame;
#ifdef HP_IA64
  rse_fp = curr_frame->rse_fp;
#endif

  /* Pick the innermost frame. */
  curr_sal = find_pc_inline_idx_line (curr_pc, -1, 0);

  /* If inline_idx is same, return as there are no nore frames
     to setup. */
  if (curr_sal.inline_idx == curr_frame->inline_idx)
    return false;

  /* If innermost is noninline frame, ther are no more inline
     frames to setup */
  if (curr_sal.inline_idx == 0)
    return false;

  /* Let's see if this PC really belongs to this inline_idx. This will fail (i.e., 
     curr_sal.line set to 0) when the PC belongs to both the caller and the callee
     and when the PC is at the end of the callee. JAGaf69066 */
  curr_sal = find_pc_inline_idx_line (curr_pc, curr_sal.inline_idx, 0);

  if (curr_sal.line == 0)
    return false;

  next_inline_idx = curr_sal.inline_idx;
  curr_inline_idx = curr_sal.inline_idx;

  while (curr_inline_idx && curr_inline_idx != curr_frame->inline_idx )
    {
      next_inline_idx = curr_inline_idx;
      next_pc = get_inline_contextref_pc (curr_inline_idx);
      curr_inline_idx = get_inline_contextref_idx (curr_inline_idx);
    }

  if (next_inline_idx == curr_inline_idx)
    return false;

  /* When we come here we have next_inline_idx pointing to the next
     of current frame's inline_idx. */

  inline_frame = (struct frame_info *)
           frame_obstack_alloc (sizeof (struct frame_info));
  bzero (inline_frame, sizeof (struct frame_info));

  inline_frame->pc = next_pc;
  inline_frame->frame = frame;
#ifdef HP_IA64
  inline_frame->rse_fp = rse_fp;
#endif
  inline_frame->inline_idx = next_inline_idx;
  inline_frame->prev = curr_frame;
  curr_frame->next = inline_frame;
  set_current_frame (inline_frame);
  return true;
}
#endif

/* Create an arbitrary (i.e. address specified by user) or innermost frame.
   Always returns a non-NULL value.  */

struct frame_info *
create_new_frame (CORE_ADDR addr, CORE_ADDR pc, void *pa_ssp)
{
  struct frame_info *fi;
  char *name;
  static struct frame_info bogus_frame;
  extern int in_startup_no_prev_fp;
#ifdef HP_IA64
  pa_save_state_t *pa_save_state_ptr = (pa_save_state_t *)pa_ssp;
#endif

#ifdef INLINE_SUPPORT
  CORE_ADDR new_pc;
  struct frame_info *inline_top;
  struct frame_info *inline_bot;
#endif

  if (in_startup_no_prev_fp)
    {
      memset (&bogus_frame, 0, sizeof (bogus_frame));
      return &bogus_frame;
    }

  fi = (struct frame_info *)
    (void *) obstack_alloc (&frame_cache_obstack,
		   sizeof (struct frame_info));

  /* Initialize everything to zero */
  bzero (fi, sizeof (struct frame_info));

  /* Arbitrary frame */
  fi->saved_regs = NULL;
  fi->next = NULL;
  fi->prev = NULL;
  fi->frame = addr;

#ifdef HP_IA64
  fi->pa_save_state_ptr = NULL;
  if (mixed_mode_pa_unwind && pa_save_state_ptr)
    {
      fi->pa_save_state_ptr = (pa_save_state_t *)
       (void *) obstack_alloc (&frame_cache_obstack, sizeof (pa_save_state_t));
      memcpy (fi->pa_save_state_ptr,
              pa_save_state_ptr,
              sizeof (pa_save_state_t));
    }
#endif

#if    defined(SKIP_TRAMPOLINE_CODE) \
    && (defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64))
#ifdef HP_IA64
  if (!mixed_mode_pa_unwind)
#endif
  {
    CORE_ADDR tmp;
    /* Skip over shared library trampoline if necessary.  */
    if ( 
#ifdef COVARIANT_SUPPORT
    /* 		Co-variant return type
         If the pc is in outbound or inbound outbound thunk,
       let gdb create the frame for the thunk. We use this frame info
       to skip the "thunk" while doing "backtrace", "frame <frameno>",
       "finish", "return <expr>" ,"step into", "step out" at/of the
       Co-variant function.
     */ 
	!IS_PC_IN_COVARIANT_THUNK (pc) 
         && 
#endif
	((tmp = SKIP_TRAMPOLINE_CODE (pc) ) != 0))
          pc = tmp;
    }
#endif

  fi->pc = SWIZZLE(pc);
  find_pc_partial_function (pc, &name, (CORE_ADDR *) NULL, (CORE_ADDR *) NULL);
  fi->signal_handler_caller = IN_SIGTRAMP (fi->pc, name);

/* bindu
   PC is getting ignored by the INIT_EXTRA_FRAME_INFO on ia64.
   When we skip the trampoline code here, we are making the calls to the 
   unwind library with the PC in the .stub code. I am ignoring the call to 
   INIT_EXTRA_FRAME_INFO when the pc is changed by the SKIP_TRAMPOLINE_CODE.
   We need to FIX the init_extra_frame_info sometime.
*/
#ifdef INIT_EXTRA_FRAME_INFO
#ifdef HP_IA64
  if (   inferior_aries_text_start
      && inferior_aries_text_end
      && in_mixed_mode_pa_lib (pc, NULL))
    {
      /* jini: Call the routine to handle the PA frame directly. */
      mixed_mode_init_extra_frame_info (0, fi);
    }
  else
#endif /* HP_IA64 */
    INIT_EXTRA_FRAME_INFO (0, fi);
#endif

  /* Initialize the argument pointer if there is one */
#ifdef INIT_FRAME_AP  
    INIT_FRAME_AP (fi);
#endif

#ifdef HP_IA64
  if (   inferior_aries_text_start
      && inferior_aries_text_end
      && in_mixed_mode_pa_lib (pc, NULL))
    {
      /* jini: Call the routine to handle the PA frame directly. */
      mixed_mode_init_frame_ap (fi);
    }
#endif


#ifdef INLINE_SUPPORT
  /* link fi to inline frames and adjust pc */
  if (
#ifdef HP_IA64
     !mixed_mode_pa_unwind &&
#endif
     (new_pc = create_inline_frames (fi, &inline_top, &inline_bot)) != 0)
    {
       fi->pc = new_pc; /* adjust pc */
       inline_bot->prev = fi; /* link inline frames on top of prev */
       fi->next = inline_bot;
       fi = inline_top;
    }
#endif

  fi->pc = SWIZZLE(pc);

  return fi;
}

/* Return the frame that FRAME calls (NULL if FRAME is the innermost
   frame).  */

struct frame_info *
get_next_frame (struct frame_info *frame)
{
  return frame->next;
}

/* Flush the entire frame cache.  */

void
flush_cached_frames ()
{
  /* RM: destroy target specific stuff */
#ifdef DESTROY_EXTRA_FRAME_INFO
  DESTROY_EXTRA_FRAME_INFO (current_frame);
#endif

  /* Since we can't really be sure what the first object allocated was */
  obstack_free (&frame_cache_obstack, 0);
  obstack_init (&frame_cache_obstack);

  current_frame = NULL;		/* Invalidate cache */
  select_frame (NULL, -1);
  //JAGag29592: Invalidate currently_printing_frame too.
  currently_printing_frame = NULL;
  annotate_frames_invalid ();
}

/* Flush the frame cache, and start a new one if necessary.  */

void
reinit_frame_cache ()
{
  flush_cached_frames ();

  /* FIXME: The inferior_pid test is wrong if there is a corefile.  */
  if (inferior_pid != 0)
    {
      select_frame (get_current_frame (), 0);
    }
}

/* Return nonzero if the function for this frame lacks a prologue.  Many
   machines can define FRAMELESS_FUNCTION_INVOCATION to just call this
   function.  */

int
frameless_look_for_prologue (struct frame_info *frame)
{
  CORE_ADDR func_start, after_prologue;

  func_start = get_pc_function_start (frame->pc);
  if (func_start)
    {
      func_start += FUNCTION_START_OFFSET;
      /* This is faster, since only care whether there *is* a
         prologue, not how long it is.  */
      return PROLOGUE_FRAMELESS_P (func_start);
    }
  else if (frame->pc == 0)
    /* A frame with a zero PC is usually created by dereferencing a
       NULL function pointer, normally causing an immediate core dump
       of the inferior. Mark function as frameless, as the inferior
       has no chance of setting up a stack frame.  */
    return 1;
  else
    /* If we can't find the start of the function, we don't really
       know whether the function is frameless, but we should be able
       to get a reasonable (i.e. best we can do under the
       circumstances) backtrace by saying that it isn't.  */
    return 0;
}

/* Default a few macros that people seldom redefine.  */

#if !defined (INIT_FRAME_PC)
#define INIT_FRAME_PC(fromleaf, prev) \
  prev->pc = (fromleaf ? SAVED_PC_AFTER_CALL (prev->next) : \
	      prev->next ? FRAME_SAVED_PC (prev->next) : read_pc());
#endif

#ifndef FRAME_CHAIN_COMBINE
#define	FRAME_CHAIN_COMBINE(chain, thisframe) (chain)
#endif

/* Return a structure containing various interesting information
   about the frame that called NEXT_FRAME.  Returns NULL
   if there is no such frame.  */

struct frame_info *
get_prev_frame (struct frame_info *next_frame)
{
  CORE_ADDR address = 0;
  struct frame_info *prev;
  int fromleaf = 0;
  char *name;

#ifdef INLINE_SUPPORT
  CORE_ADDR new_pc = 0;
  struct frame_info *inline_top;
  struct frame_info *inline_bot;
  struct linetable *best_linetable;
#endif 

  /* If the requested entry is in the cache, return it.
     Otherwise, figure out what the address should be for the entry
     we're about to add to the cache. */

  if (!next_frame)
    {
#if 0
      /* This screws value_of_variable, which just wants a nice clean
         NULL return from block_innermost_frame if there are no frames.
         I don't think I've ever seen this message happen otherwise.
         And returning NULL here is a perfectly legitimate thing to do.  */
      if (!current_frame)
	{
	  error ("You haven't set up a process's stack to examine.");
	}
#endif

      return current_frame;
    }

  /* If we have the prev one, return it */
  if (next_frame->prev)
    {
      return next_frame->prev;
    }

  /* If next_frame is a Java frame */
  if (java_debugging && (is_java_frame(next_frame->pc))) 
  {
    /* Create the previous frame */
    prev = (struct frame_info *)
      (void *) obstack_alloc (&frame_cache_obstack, sizeof (struct frame_info));
    bzero (prev, sizeof (struct frame_info));
#ifdef DO_JAVA_STACK_UNWIND
    DO_JAVA_STACK_UNWIND (next_frame, prev);
#endif
    prev->next=next_frame;
    prev->prev = 0;
    prev->signal_handler_caller = 0;
    prev->saved_regs = 0;
    next_frame->prev=prev;
    return prev;
  }
		
  /* Handle bad pc values for top frame or for frame below signal handler */
#ifdef HANDLE_BAD_PC
  if (HANDLE_BAD_PC(next_frame))
   {
    /* Create the previous frame */
    prev = (struct frame_info *)
      (void *) obstack_alloc (&frame_cache_obstack, sizeof (struct frame_info));
    bzero (prev, sizeof (struct frame_info));
    do_handle_bad_pc(next_frame, prev);
    prev->next=next_frame;
    prev->prev = 0;
    prev->signal_handler_caller = 0;
    prev->saved_regs = 0;
    next_frame->prev=prev;
    return prev;
  }
#endif /* #ifdef HANDLE_BAD_PC */

  /* On some machines it is possible to call a function without
     setting up a stack frame for it.  On these machines, we
     define this macro to take two args; a frameinfo pointer
     identifying a frame and a variable to set or clear if it is
     or isn't leafless.  */

  /* Still don't want to worry about this except on the innermost
     frame.  This macro will set FROMLEAF if NEXT_FRAME is a
     frameless function invocation.  */
  if (!(next_frame->next))
    {
#ifdef HP_IA64
     if (next_frame->pa_save_state_ptr && mixed_mode_pa_unwind)
       {
         fromleaf = mixed_mode_frameless_function_invocation (next_frame);
       }
     else
#endif
      fromleaf = FRAMELESS_FUNCTION_INVOCATION (next_frame);
      if (fromleaf)
#ifndef HP_IA64
	address = FRAME_FP (next_frame);
#else
	/* While the above line make sense if we don't have a frame for this
	 * routine, for IPF, we need to do FRAME_CHAIN.  There may be
	 * a bug where we are creating a gdb frame where the prodcedure
	 * does not have a frame.  -- Michael Coulter  8/19/04
	 */

	address = FRAME_CHAIN (next_frame);
#endif
    }

  if (!fromleaf)
    {
      /* Two macros defined in tm.h specify the machine-dependent
         actions to be performed here.
         First, get the frame's chain-pointer.
         If that is zero, the frame is the outermost frame or a leaf
         called by the outermost frame.  This means that if start
         calls main without a frame, we'll return 0 (which is fine
         anyway).

         Nope; there's a problem.  This also returns when the current
         routine is a leaf of main.  This is unacceptable.  We move
         this to after the ffi test; I'd rather have backtraces from
         start go curfluy than have an abort called from main not show
         main.  */
      address = FRAME_CHAIN (next_frame);
      if (!FRAME_CHAIN_VALID (address, next_frame))
	{
#ifdef HANDLE_BAD_PC
	  /* If the current frame is a signal handler caller frame, give
	     out a frame with bad pc, so that in the next round we can
	     handle this kind of frame. (see HANDLE_BAD_PC block at the
	     start of this function.) */
          if (next_frame && next_frame->signal_handler_caller)
	    {
	      prev = (struct frame_info *)
	        (void *) obstack_alloc (&frame_cache_obstack,
                       	       sizeof (struct frame_info));
	      bzero (prev, sizeof (struct frame_info));
	      prev->saved_regs = NULL;
	      next_frame->prev = prev;
	      prev->next = next_frame;
	      prev->prev = (struct frame_info *) 0;
	      prev->frame = 0;
	      prev->signal_handler_caller = 0;
	      prev->pc = 0;
	      return prev;
	    }
#endif /* HANDLE_BAD_PC */
	  return 0;
	}
      address = FRAME_CHAIN_COMBINE (address, next_frame);
    }

#ifdef HP_IA64
  if (    !mixed_mode_debug_aries && mixed_mode_pa_unwind
      && (UINT64_MAX == address || UINT64_MAX - 1 == address))
    {
      /* An address of UINT64_MAX signifies that we need to switch to
         IPF unwinding. Reset mixed_mode_pa_unwind. Return the cached
         IA frame that was obtained while skipping the aries frames.
         Chain the frames together before the return. */
      /* UINT64_MAX - 1 is a special case to denote a PA to PA BOR call. */

      if (UINT64_MAX == address)
        {
          mixed_mode_pa_unwind = false;
          next_frame->prev = next_frame->non_aries_ia_frame;
          next_frame->non_aries_ia_frame->next = next_frame;
          return next_frame->non_aries_ia_frame;
        }
      else
        {
          /* The UINT64_MAX - 1 case */
          next_frame->prev = get_mixed_mode_pa_frame (NULL);
          next_frame->prev->non_aries_ia_frame = next_frame->non_aries_ia_frame;
          next_frame->prev->next = next_frame;
          return next_frame->prev;
        }
    }
#endif

  if (address == 0)
    {
#ifdef HANDLE_BAD_PC
      /* If the current frame is a signal handler caller frame, give out
	 a frame with bad (0) pc, so that in the next round we can
	 handle this kind of frame. (see HANDLE_BAD_PC block at the start
	 of this function.) */
      if (next_frame && next_frame->signal_handler_caller)
	{
	  prev = (struct frame_info *)
	    (void *) obstack_alloc (&frame_cache_obstack,
                   	   sizeof (struct frame_info));
	  bzero (prev, sizeof (struct frame_info));
	  prev->saved_regs = NULL;
	  next_frame->prev = prev;
	  prev->next = next_frame;
	  prev->prev = (struct frame_info *) 0;
	  prev->frame = 0;
	  prev->signal_handler_caller = 0;
	  prev->pc = 0;
	  return prev;
	}
#endif /* HANDLE_BAD_PC */
      return 0;
    }

#ifdef HP_IA64
  /* jini: QXCR1000875632: Avoid running out of memory with infinitely recursive
     stacks. */
  if (!unwind_all_frames && total_stack_frames >= MAX_NBR_STACK_FRAMES)
    {
      error ("Maximum nbr of stack frames (10000) has been reached or exceeded.\n"
             "Halting unwinding to avoid gdb running out of memory. Please set\n"
             "unwind-all-frames to 'on' to unwind further.");
    }

  total_stack_frames ++;
#endif

  prev = (struct frame_info *)
    (void *) obstack_alloc (&frame_cache_obstack,
		   sizeof (struct frame_info));

  bzero (prev, sizeof (struct frame_info));
  prev->saved_regs = NULL;
  if (next_frame)
    next_frame->prev = prev;
  prev->next = next_frame;
  prev->prev = (struct frame_info *) 0;
  prev->frame = address;
  prev->signal_handler_caller = 0;

#ifdef HP_IA64
  prev->non_aries_ia_frame = 0;

  if (mixed_mode_pa_unwind && next_frame->pa_save_state_ptr)
    {
      prev->non_aries_ia_frame = next_frame->non_aries_ia_frame;
    }
#endif

/* This change should not be needed, FIXME!  We should
   determine whether any targets *need* INIT_FRAME_PC to happen
   after INIT_EXTRA_FRAME_INFO and come up with a simple way to
   express what goes on here.

   INIT_EXTRA_FRAME_INFO is called from two places: create_new_frame
   (where the PC is already set up) and here (where it isn't).
   INIT_FRAME_PC is only called from here, always after
   INIT_EXTRA_FRAME_INFO.

   The catch is the MIPS, where INIT_EXTRA_FRAME_INFO requires the PC
   value (which hasn't been set yet).  Some other machines appear to
   require INIT_EXTRA_FRAME_INFO before they can do INIT_FRAME_PC.  Phoo.

   We shouldn't need INIT_FRAME_PC_FIRST to add more complication to
   an already overcomplicated part of GDB.   gnu@cygnus.com, 15Sep92.

   Assuming that some machines need INIT_FRAME_PC after
   INIT_EXTRA_FRAME_INFO, one possible scheme:

   SETUP_INNERMOST_FRAME()
   Default version is just create_new_frame (read_fp ()),
   read_pc ()).  Machines with extra frame info would do that (or the
   local equivalent) and then set the extra fields.
   SETUP_ARBITRARY_FRAME(argc, argv)
   Only change here is that create_new_frame would no longer init extra
   frame info; SETUP_ARBITRARY_FRAME would have to do that.
   INIT_PREV_FRAME(fromleaf, prev)
   Replace INIT_EXTRA_FRAME_INFO and INIT_FRAME_PC.  This should
   also return a flag saying whether to keep the new frame, or
   whether to discard it, because on some machines (e.g.  mips) it
   is really awkward to have FRAME_CHAIN_VALID called *before*
   INIT_EXTRA_FRAME_INFO (there is no good way to get information
   deduced in FRAME_CHAIN_VALID into the extra fields of the new frame).
   std_frame_pc(fromleaf, prev)
   This is the default setting for INIT_PREV_FRAME.  It just does what
   the default INIT_FRAME_PC does.  Some machines will call it from
   INIT_PREV_FRAME (either at the beginning, the end, or in the middle).
   Some machines won't use it.
   kingdon@cygnus.com, 13Apr93, 31Jan94, 14Dec94.  */

#ifdef INIT_FRAME_PC_FIRST
  INIT_FRAME_PC_FIRST (fromleaf, prev);
#endif

#ifdef INIT_EXTRA_FRAME_INFO
  /* jini: mixed mode corefile support: If next frame is an IPF frame,
     the previous frame will not ever be a mixed mode PA frame. It can be
     an aries frame. So mixed_mode_init_extra_frame_info() is not needed
     to be called here. */
  INIT_EXTRA_FRAME_INFO (fromleaf, prev);
#endif

  /* This entry is in the frame queue now, which is good since
     FRAME_SAVED_PC may use that queue to figure out its value
     (see tm-sparc.h).  We want the pc saved in the inferior frame. */
  INIT_FRAME_PC (fromleaf, prev);

  /* Initialize the argument pointer if there is one */
#ifdef INIT_FRAME_AP  
  INIT_FRAME_AP (prev);
#endif
  
#ifdef HP_IA64
  if (mixed_mode_pa_unwind)
    {
      /* jini: Call the routine to handle the PA frame directly. */
      mixed_mode_init_frame_ap (prev);
    }
#endif

   /* If ->frame and ->pc are unchanged, we are in the process of getting
     ourselves into an infinite backtrace.  Some architectures check this
     in FRAME_CHAIN or thereabouts, but it seems like there is no reason
     this can't be an architecture-independent check.  */
  if (next_frame != NULL)
    {
      if (prev->frame == next_frame->frame
#ifdef REGISTER_STACK_ENGINE_FP
          && prev->rse_fp == next_frame->rse_fp
#endif
	  && prev->pc == next_frame->pc)
	{
	  next_frame->prev = NULL;
	  obstack_free (&frame_cache_obstack, prev);
	  return NULL;
	}
    }

  find_pc_partial_function (prev->pc, &name,
			    (CORE_ADDR *) NULL, (CORE_ADDR *) NULL);
  if (IN_SIGTRAMP (prev->pc, name))
    prev->signal_handler_caller = 1;

#ifdef INLINE_SUPPORT
  /* Check if the frame we are returning is in inlined function
     context; if it is create dummy frames for nested inlined functions.
     This function returns a list of such inlined functions; We link the
     list with the frame we are about to return; 
   */
  if ((new_pc = create_inline_frames (prev, &inline_top, &inline_bot)) != 0)
    {
       if (prev->next) /* link with next_frame */
         {
            prev->next->prev = inline_top;
            inline_top->next = prev->next;
         }
         prev->pc = new_pc; /* adjust pc for already allocated prev frame */

         /* link prev frame with inlined frames */
         inline_bot->prev = prev; /* link inline frames on top of prev */
         prev->next = inline_bot;
         prev = inline_top;
    }
#endif 

#ifdef HP_IA64
  /* jini: mixed mode corefile debugging: Dechain the prev frame from the
     next frame if we are not debugging aries and prev->pc lies in the
     aries text range. This is done since the backtrace should include only
     the PA and the non-aries IPF frames. */
  if (!mixed_mode_debug_aries &&
      inferior_aries_text_start <= prev->pc &&
      prev->pc < inferior_aries_text_end)
    {
      next_frame->prev = NULL;
    }
#endif
  return prev;
}

CORE_ADDR
get_frame_pc (struct frame_info *frame)
{
  return frame->pc;
}


#ifdef FRAME_FIND_SAVED_REGS
/* XXX - deprecated.  This is a compatibility function for targets
   that do not yet implement FRAME_INIT_SAVED_REGS.  */
/* Find the addresses in which registers are saved in FRAME.  */

void
get_frame_saved_regs (struct frame_info *frame, struct frame_saved_regs *saved_regs_addr)
{
  if (frame->saved_regs == NULL)
    {
      frame->saved_regs = (CORE_ADDR *)
	frame_obstack_alloc (SIZEOF_FRAME_SAVED_REGS);
    }
  if (saved_regs_addr == NULL)
    {
      struct frame_saved_regs saved_regs;
      FRAME_FIND_SAVED_REGS (frame, saved_regs);
      memcpy (frame->saved_regs, &saved_regs, SIZEOF_FRAME_SAVED_REGS);
    }
  else
    {
      FRAME_FIND_SAVED_REGS (frame, *saved_regs_addr);
      memcpy (frame->saved_regs, saved_regs_addr, SIZEOF_FRAME_SAVED_REGS);
    }
}
#endif

/* Return the innermost lexical block in execution
   in a specified stack frame.  The frame address is assumed valid.  */

struct block *
get_frame_block_1 (struct frame_info *frame)
{
  CORE_ADDR pc;

  pc = frame->pc;
  if (frame->next != 0 && frame->next->signal_handler_caller == 0)
    /* We are not in the innermost frame and we were not interrupted
       by a signal.  We need to subtract one to get the correct block,
       in case the call instruction was the last instruction of the block.
       If there are any machines on which the saved pc does not point to
       after the call insn, we probably want to make frame->pc point after
       the call insn anyway.  */
    --pc;
  return block_for_pc (pc);
}

struct block *
get_frame_block (struct frame_info *frame)
{
  CORE_ADDR pc;
  int current_frame_inline_idx = 0;
  register struct block *b;
  register int bot, top, half;
  struct blockvector *bl;
  struct symtab * symtab;

  if (!inline_debugging)
      return get_frame_block_1 (frame);

  pc = frame->pc;

  /* If ``frame'' has a callee and that callee is an inlined function
     then frame->pc is not really pointing *after* the call. Indeed
     there is no call insn there. What we have is the "context reference"
     PC and this should not be decremented !!!
  */
  if (frame->next != 0 && frame->next->inline_idx == 0 && 
                        frame->next->signal_handler_caller == 0)
    /* We are not in the innermost frame and we were not interrupted
       by a signal.  We need to subtract one to get the correct block,
       in case the call instruction was the last instruction of the block.
       If there are any machines on which the saved pc does not point to
       after the call insn, we probably want to make frame->pc point after
       the call insn anyway.  */
    --pc;

  /* We used to do a "return block_for_pc (pc);" from here. Now we inline
     that function and its callees into here to get the code below.
  */

  /* Also when PC's overlap, we want to return that block whose
     inline index matches the given frame's inline index.
  */

  current_frame_inline_idx = frame->inline_idx;

  if ((symtab = find_pc_sect_symtab (pc, 0)) == 0)
      return 0;

  bl = BLOCKVECTOR (symtab);
  b = BLOCKVECTOR_BLOCK (bl, 0);

  /* Use binary search to find the last block that starts before PC. */
  bot = 0;
  top = BLOCKVECTOR_NBLOCKS (bl);

  while (top - bot > 1)
  {
      half = (top - bot + 1) >> 1;
      b = BLOCKVECTOR_BLOCK (bl, bot + half);
      if (SWIZZLE(BLOCK_START (b)) <= pc)
	  bot += half;
      else
	  top = bot + half;
  }

  /* Now search backward for a block that ends after PC.  */
  while (bot >= 0)
  {
      b = BLOCKVECTOR_BLOCK (bl, bot);
      if (SWIZZLE(BLOCK_END (b)) > pc && b->inline_idx == current_frame_inline_idx)
	  return b;
      bot--;
  }
  return 0;
}

struct block *
get_current_block ()
{
# ifdef KGDB
  /* the call to read_pc() errors out if target not attached which
     seems too severe ... instead just report no current block.
  */
  if (!attach_flag)
    return (struct block *)0;
# endif

  return block_for_pc (read_pc ());
}

CORE_ADDR
get_pc_function_start (CORE_ADDR pc)
{
  register struct block *bl;
  register struct symbol *symbol;
  register struct minimal_symbol *msymbol;
  CORE_ADDR fstart;

  if ((bl = block_for_pc (pc)) != NULL &&
      (symbol = block_function (bl)) != NULL)
    {
      bl = SYMBOL_BLOCK_VALUE (symbol);
      fstart = SWIZZLE(BLOCK_START (bl));
    }
  else if ((msymbol = lookup_minimal_symbol_by_pc (pc)) != NULL)
    {
      fstart = SYMBOL_VALUE_ADDRESS (msymbol);
    }
  else
    {
      fstart = 0;
    }
  return (fstart);
}

/* Return the symbol for the function executing in frame FRAME.  */

struct symbol *
get_frame_function (struct frame_info *frame)
{
  register struct block *bl = get_frame_block (frame);
  if (bl == 0)
    return 0;
  return block_function (bl);
}


/* Return the blockvector immediately containing the innermost lexical block
   containing the specified pc value and section, or 0 if there is none.
   PINDEX is a pointer to the index value of the block.  If PINDEX
   is NULL, we don't pass this information back to the caller.  */

struct blockvector *
blockvector_for_pc_sect (register CORE_ADDR pc, struct sec *section, int *pindex,
			 struct symtab *symtab)
{
  register struct block *b;
  register int bot, top, half;
  struct blockvector *bl;

  if (symtab == 0)		/* if no symtab specified by caller */
    {
      /* First search all symtabs for one whose file contains our pc */
      if ((symtab = find_pc_sect_symtab (pc, section)) == 0)
	return 0;
    }

  bl = BLOCKVECTOR (symtab);
  b = BLOCKVECTOR_BLOCK (bl, 0);

  /* Then search that symtab for the smallest block that wins.  */
  /* Use binary search to find the last block that starts before PC.  */

  bot = 0;
  top = BLOCKVECTOR_NBLOCKS (bl);

  while (top - bot > 1)
    {
      half = (top - bot + 1) >> 1;
      b = BLOCKVECTOR_BLOCK (bl, bot + half);
      if (SWIZZLE(BLOCK_START (b)) <= pc)
	bot += half;
      else
	top = bot + half;
    }

  /* Now search backward for a block that ends after PC.  */

  while (bot >= 0)
    {
      b = BLOCKVECTOR_BLOCK (bl, bot);
      if (SWIZZLE(BLOCK_END (b)) > pc)
	{
          /* On PA, even when gdb is NOT invoked with the --inline option,
             we add the scopes contributed by the inlined code to the block
             vector. Simply don't pick those here when we shouldn't.
          */
          if (inline_debugging || !b->inline_idx)
            { 
	      if (pindex)
	        *pindex = bot;
	      return bl;
            }
	}
      bot--;
    }
  return 0;
}

/* Return the blockvector immediately containing the innermost lexical block
   containing the specified pc value, or 0 if there is none.
   Backward compatibility, no section.  */

struct blockvector *
blockvector_for_pc (register CORE_ADDR pc, int *pindex)
{
  return blockvector_for_pc_sect (pc, find_pc_mapped_section (pc),
				  pindex, NULL);
}

/* Return the innermost lexical block containing the specified pc value
   in the specified section, or 0 if there is none.  */

struct block *
block_for_pc_sect (register CORE_ADDR pc, struct sec *section)
{
  register struct blockvector *bl;
  int index = 0; /* initialize for compiler warning */

  bl = blockvector_for_pc_sect (pc, section, &index, NULL);
  if (bl)
    return BLOCKVECTOR_BLOCK (bl, index);
  return 0;
}

/* Return the innermost lexical block containing the specified pc value,
   or 0 if there is none.  Backward compatibility, no section.  */

struct block *
block_for_pc (register CORE_ADDR pc)
{
  return block_for_pc_sect (pc, find_pc_mapped_section (pc));
}

/* Return the function containing pc value PC in section SECTION.
   Returns 0 if function is not known.  */

struct symbol *
find_pc_sect_function (CORE_ADDR pc, struct sec *section)
{
  register struct block *b = block_for_pc_sect (pc, section);
  if (b == 0)
    return 0;
  return block_function (b);
}

/* Return the function containing pc value PC.
   Returns 0 if function is not known.  Backward compatibility, no section */

struct symbol *
find_pc_function (CORE_ADDR pc)
{
  /* bloomberg hack */
  pc = SWIZZLE (pc); 
  return find_pc_sect_function (pc, find_pc_mapped_section (pc));
}

/* These variables are used to cache the most recent result
 * of find_pc_partial_function. */

static CORE_ADDR cache_pc_function_low = 0;
static CORE_ADDR cache_pc_function_high = 0;
static char *cache_pc_function_name = 0;
static struct sec *cache_pc_function_section = NULL;

/* srikanth, 000129, caching the most recent result alone works
   out rather poorly. It is all too easy to get into the "ping-pong"
   situation. Let us cache the last 64 values and maintain them
   sorted on a MRU basis. I am leaving the old singleton cache
   intact to minimize source changes.
*/

#define PC_FUNCTION_CACHE_SIZE 64

static struct pc_function_cache {
    CORE_ADDR low;
    CORE_ADDR high;
    char * name;
    struct sec * section;
} pc_function_cache [PC_FUNCTION_CACHE_SIZE];

/* Clear cache, e.g. when symbol table is discarded. */

void
clear_pc_function_cache ()
{
  int i;

  cache_pc_function_low = 0;
  cache_pc_function_high = 0;
  cache_pc_function_name = (char *) 0;
  cache_pc_function_section = NULL;

  for (i=0; i < PC_FUNCTION_CACHE_SIZE; i++)
    {
      pc_function_cache[i].low     = 0;
      pc_function_cache[i].high    = 0;
      pc_function_cache[i].name    = 0;
      pc_function_cache[i].section = 0;
    }
}

/* Finds the "function" (text symbol) that is smaller than PC but
   greatest of all of the potential text symbols in SECTION.  Sets
   *NAME and/or *ADDRESS conditionally if that pointer is non-null.
   If ENDADDR is non-null, then set *ENDADDR to be the end of the
   function (exclusive), but passing ENDADDR as non-null means that
   the function might cause symbols to be read.  This function either
   succeeds or fails (not halfway succeeds).  If it succeeds, it sets
   *NAME, *ADDRESS, and *ENDADDR to real information and returns 1.
   If it fails, it sets *NAME, *ADDRESS, and *ENDADDR to zero and
   returns 0.  */

int
find_pc_sect_partial_function_1 (CORE_ADDR pc, asection *section, char **name,
				 CORE_ADDR *address, CORE_ADDR *endaddr)
{
  struct partial_symtab *pst;
  struct symbol *f;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *next_symbol;
  struct partial_symbol *psb;
  struct obj_section *osect;
  char *symname;
  CORE_ADDR mapped_pc;
  char *tmp_funcname;
  int cache_pc_function_name_len;

  mapped_pc = overlay_mapped_address (pc, section);

  if (mapped_pc >= cache_pc_function_low &&
      mapped_pc < cache_pc_function_high &&
      section == cache_pc_function_section)
    goto return_cached_value;

  /* If sigtramp is in the u area, it counts as a function (especially
     important for step_1).  */
#if defined SIGTRAMP_START
  if (IN_SIGTRAMP (mapped_pc, (char *) NULL))
    {
      cache_pc_function_low = SIGTRAMP_START (mapped_pc);
      cache_pc_function_high = SIGTRAMP_END (mapped_pc);
      cache_pc_function_name = "<sigtramp>";
      cache_pc_function_section = section;
      goto return_cached_value;
    }
#endif

  msymbol = lookup_minimal_symbol_by_pc_section (mapped_pc, section);

#ifdef HP_IA64
#ifndef KGDB
  /* Poorva:01/21/2003 For elf object files we have the high pc
     stored - use that instead of doing a psymtab to symtab 
     expansion and reading in the debug info of a file.
   */
  if (object_files && object_files->sf && (object_files->sf->sym_read == elf_symfile_read))
  {
    /* jini: Sept 2006: mixed mode changes: PA32 SOM files have their
       bfd_section set to zero. Refer JAGaf59103. Moreover MSYMBOL_INFO
       is set to the symbol type. So MSYMBOL_INFO (msymbol) will be
       greater than zero for PA32 SOM libs also. The last check regarding
       the target flavour is just to be on the safe side. I don't think
       it will ever be false. JAGag21714 */

    if (msymbol && MSYMBOL_INFO (msymbol)
        && SYMBOL_BFD_SECTION(msymbol)
        && (bfd_get_flavour (SYMBOL_BFD_SECTION(msymbol)->owner)
           != bfd_target_som_flavour))
      {
	cache_pc_function_low = SYMBOL_VALUE_ADDRESS (msymbol);
	cache_pc_function_high = SWIZZLE(cache_pc_function_low
	  + (CORE_ADDR) (MSYMBOL_INFO (msymbol)));
	cache_pc_function_name = SYMBOL_NAME (msymbol);
	cache_pc_function_section = section;
	goto return_cached_value;
      }
  }
#endif
#endif

  pst = find_pc_sect_psymtab (mapped_pc, section);
  if (pst)
    {
      /* Need to read the symbols to get a good value for the end address.  */
      if (endaddr != NULL && !pst->readin)
	{
	  /* Need to get the terminal in case symbol-reading produces
	     output.  */
	  target_terminal_ours_for_output ();
	  PSYMTAB_TO_SYMTAB (pst);
	}

      if (pst->readin)
	{
	  /* Checking whether the msymbol has a larger value is for the
	     "pathological" case mentioned in print_frame_info.  */
	  f = find_pc_sect_function (mapped_pc, section);
	  if (f != NULL
	      && (msymbol == NULL
		  || (SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (f)))
		      >= SYMBOL_VALUE_ADDRESS (msymbol))))
	    {
	      cache_pc_function_low = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (f)));
	      cache_pc_function_high = SWIZZLE(BLOCK_END (SYMBOL_BLOCK_VALUE (f)));
	      cache_pc_function_name = SYMBOL_NAME (f);
	      cache_pc_function_section = section;
	      goto return_cached_value;
	    }
	}
      /* RM: For DOOM, it may be the case that we have a psymtab for a file
       * with no debug information. In that case, pst->readin will be 
       * false even after we have attempted to expand pst. We should fall 
       * through to the minimal symbol processing code in this case.
       */
      else if (!endaddr)
	{
	  /* Now that static symbols go in the minimal symbol table, perhaps
	     we could just ignore the partial symbols.  But at least for now
	     we use the partial or minimal symbol, whichever is larger.  */
	  psb = find_pc_sect_psymbol (pst, mapped_pc, section);

	  if (psb
	      && (msymbol == NULL ||
		  (SYMBOL_VALUE_ADDRESS (psb)
		   >= SYMBOL_VALUE_ADDRESS (msymbol))))
	    {
	      /* This case isn't being cached currently. */
	      if (address)
		*address = SYMBOL_VALUE_ADDRESS (psb);
	      if (name)
		*name = SYMBOL_NAME (psb);
	      /* endaddr non-NULL can't happen here.  */

              cache_pc_function_low = 0;
              cache_pc_function_high = 0;
              cache_pc_function_name = 0;
              cache_pc_function_section = 0;
	      return 1;
	    }
	}
    }

  /* Not in the normal symbol tables, see if the pc is in a known section.
     If it's not, then give up.  This ensures that anything beyond the end
     of the text seg doesn't appear to be part of the last function in the
     text segment.  */

  osect = find_pc_sect_section (mapped_pc, section);

  if (!osect)
    msymbol = NULL;

  /* Must be in the minimal symbol table.  */
  if (msymbol == NULL)
    {
      /* No available symbol.  */
      if (name != NULL)
	*name = 0;
      if (address != NULL)
	*address = 0;
      if (endaddr != NULL)
	*endaddr = 0;
      return 0;
    }

  cache_pc_function_low = SYMBOL_VALUE_ADDRESS (msymbol);
  cache_pc_function_name = SYMBOL_NAME (msymbol);
  cache_pc_function_section = section;

  /* RM: Linker stubs can cause "holes" between a function and its
     successor. Check to see if pc is greater than the end of the
     function starting at cache_pc_function_low. */
  f = find_pc_function (cache_pc_function_low);
  if ((f != NULL) &&
      (pc > BLOCK_END (SYMBOL_BLOCK_VALUE (f))))
    {
      /* function ends earlier than pc */
      if (name != NULL)
        *name = 0;
      if (address != NULL)
        *address = 0;
      if (endaddr != NULL)
        *endaddr = 0;

      cache_pc_function_low = 0;
      cache_pc_function_high = 0;
      cache_pc_function_name = 0;
      cache_pc_function_section = 0;

      return 0;     
    }
  
  /* Use the lesser of the next minimal symbol in the same section, or
     the end of the section, as the end of the function.  */

  /* Step over other symbols at this same address, and symbols in
     other sections, to find the next symbol in this section with
     a different address.  */

  /* srikanth, 000518, do not use pointer arithmetic to figure out the
     next symbol. This will not work with the new scheme of feedback
     driven quicker-sort (the minsym vector is not sorted any more.)
  */
  next_symbol = lookup_next_minimal_symbol (msymbol);
  while (SYMBOL_NAME (next_symbol) != NULL)
    {
      if (SYMBOL_VALUE_ADDRESS (next_symbol) != SYMBOL_VALUE_ADDRESS (msymbol)
	  && SYMBOL_BFD_SECTION (next_symbol) == SYMBOL_BFD_SECTION (msymbol))
	break;
      next_symbol = lookup_next_minimal_symbol (next_symbol);
    }

  /* MC: skip over relocation only symbols left in for DOOM */
#ifdef IS_RELOC_ONLY_SYMBOL
  while (SYMBOL_NAME (next_symbol) != NULL
	 && IS_RELOC_ONLY_SYMBOL (SYMBOL_NAME (next_symbol)))
    {
      next_symbol = lookup_next_minimal_symbol (next_symbol);
    }
#endif
  
  /* Check if this is a c++ contructor with multiple entry points. Skip 
     all the entry points in the minimal symbol table to get the right 
     end address. This only happens on IA where the psymtab doesnot 
     contain the symbol for the constructor *Bindu 09/18/00***/

#ifdef HP_IA64
  /* Does this work with the new mangling scheme ?? Check */
  if (strncmp ("__ct__", cache_pc_function_name, 6) == 0)
    {
      /* What if there are embedded _'s ?? done-bindu*/
      tmp_funcname = cache_pc_function_name + 6;
      cache_pc_function_name_len = (int) strlen (cache_pc_function_name);
      if ((*(cache_pc_function_name + 
             cache_pc_function_name_len - 2) == '_')
           &&(*(cache_pc_function_name + 
                cache_pc_function_name_len - 1) == '1'
              || *(cache_pc_function_name + 
                cache_pc_function_name_len - 1) == '2'))
        {
           tmp_funcname [cache_pc_function_name_len - 8] = '\0';
        }

      /* What if next_symbol is 0 ? done-bindu*/
      symname = SYMBOL_NAME (next_symbol);
      while ((symname != NULL) && (strncmp ("__ct__", symname, 6) == 0) &&
             (strncmp (tmp_funcname, symname+6, strlen (tmp_funcname)) == 0))
        {
          next_symbol++;
          symname = SYMBOL_NAME (next_symbol);

          /* Check for RELOC_ONLY symbols */
#ifdef IS_RELOC_ONLY_SYMBOL
	  while (symname != NULL
	         && IS_RELOC_ONLY_SYMBOL (symname))
	    {
	      next_symbol++;
	      symname = SYMBOL_NAME (next_symbol);
	    }
#endif

        }
      cache_pc_function_name = SYMBOL_NAME (msymbol);

      /* What if we are left with a IS_RELOC_ONLY symbol here ?? fixed-bindu*/
    }

#endif

  if (SYMBOL_NAME (next_symbol) != NULL
      && SYMBOL_VALUE_ADDRESS (next_symbol) < osect->endaddr)
    cache_pc_function_high = SYMBOL_VALUE_ADDRESS (next_symbol);
  else
    /* We got the start address from the last msymbol in the objfile.
       So the end address is the end of the section.  */
    cache_pc_function_high = osect->endaddr;

return_cached_value:

  if (address)
    {
      if (pc_in_unmapped_range (pc, section))
	*address = overlay_unmapped_address (cache_pc_function_low, section);
      else
	*address = cache_pc_function_low;
    }

  if (name)
    *name = cache_pc_function_name;

  if (endaddr)
    {
      if (pc_in_unmapped_range (pc, section))
	{
	  /* Because the high address is actually beyond the end of
	     the function (and therefore possibly beyond the end of
	     the overlay), we must actually convert (high - 1)
	     and then add one to that. */

	  *endaddr = 1 + overlay_unmapped_address (cache_pc_function_high - 1,
						   section);
	}
      else
	*endaddr = cache_pc_function_high;
    }

  return 1;
}

int
find_pc_sect_partial_function (CORE_ADDR pc, asection *section, char **name,
			       CORE_ADDR *address, CORE_ADDR *endaddr)
{
  struct pc_function_cache this, last;
  int i;
  int status;

  if (pc >= pc_function_cache[0].low && pc < pc_function_cache[0].high &&
         section == pc_function_cache[0].section)
    goto return_cached_value;

  /* Not so lucky ... */

  last = pc_function_cache[0];

  /* Do we have the desired info in cache ?  As we scan the list of
     entries left to right, looking for the pc of interest, we will
     bubble the entries away from the beginning i.e., the "most recently
     used" slot. This would create a vacuum in the very first slot. 
     When we find the pc we are looking for, we will move that entry to
     this slot. Alternately, if the entry is not in the cache and we
     have to determine it the hard way, we will use this slot as the
     destination. This guarentees that the list is sorted on a MRU
     basis at all times.
  */

  for (i=1; i < PC_FUNCTION_CACHE_SIZE; i++)
    {
      this = pc_function_cache[i];
      pc_function_cache[i] = last;

      if (pc >= this.low &&  pc < this.high && section == this.section)
        {
          pc_function_cache[0] = this;
          goto return_cached_value;
        }

      last = this;
    }

  /* Tough luck ! Let us work harder ... */
  status = find_pc_sect_partial_function_1 (pc, section, name,
                                                 address, endaddr);
  if (!status || cache_pc_function_low == 0)
    {
      /* If we failed, we don't want to record the junky results...
         Surely reinserting the LRU item is better than leaving the
         MRU entry there ? Otherwise, several successive failed
         calls would cause redundant entries.
      */
      pc_function_cache[0] = last;  /* :-( */
      return status;
    }
  else 
    {
      pc_function_cache[0].low = cache_pc_function_low;
      pc_function_cache[0].high = cache_pc_function_high;
      pc_function_cache[0].name = cache_pc_function_name;
      pc_function_cache[0].section = cache_pc_function_section;
      return status;
    }

return_cached_value :

  /* fill in the values from the MRU entry. The one we looked up
     just now has already been elevated to MRU status ...
  */

  if (address)
    *address = pc_function_cache[0].low;
  if (name)
    *name = pc_function_cache[0].name;
  if (endaddr)
    *endaddr = pc_function_cache[0].high;
  return 1;
}


/* Backward compatibility, no section argument */

int
find_pc_partial_function (CORE_ADDR pc, char **name, CORE_ADDR *address, CORE_ADDR *endaddr)
{
  asection *section;

  section = find_pc_overlay (pc);
  return find_pc_sect_partial_function (pc, section, name, address, endaddr);
}

/* Return the innermost stack frame executing inside of BLOCK,
   or NULL if there is no such frame.  If BLOCK is NULL, just return NULL.  */

struct frame_info *
block_innermost_frame (struct block *block)
{
  struct frame_info *frame;
  register CORE_ADDR start;
  register CORE_ADDR end;

  if (block == NULL)
    return NULL;

  start = SWIZZLE(BLOCK_START (block));
  end = SWIZZLE(BLOCK_END (block));

  frame = NULL;
  while (1)
    {
      frame = get_prev_frame (frame);
      if (frame == NULL)
	return NULL;
      if (frame->pc >= start && frame->pc < end)
	return frame;
    }
}

/* Return the full FRAME which corresponds to the given CORE_ADDR
   or NULL if no FRAME on the chain corresponds to CORE_ADDR.  */

struct frame_info *
find_frame_addr_in_frame_chain (CORE_ADDR frame_addr, boolean *unw_prob)
{
  struct frame_info *frame = NULL;

  if (unw_prob)
    *unw_prob = false;

  if (frame_addr == (CORE_ADDR) 0)
    return NULL;

  boolean print_bad_pc_unwind_msg_save = print_bad_pc_unwind_msg;

  print_bad_pc_unwind_msg = false;

  while (1)
    {
      struct frame_info *old_frame = frame;

      frame = get_prev_frame (frame);
      if (frame == NULL)
      {
#ifdef HP_IA64
        // Check if we are yet to go below the frame_addr. If so, we
        // still may have the frame below, but we couldn't unwind till
        // that point. In this scenario, mark unw_prob as true.
#ifdef REGISTER_STACK_ENGINE_FP
        if (old_frame && (old_frame->rse_fp >= frame_addr))
#else
        if (old_frame && (FRAME_FP (old_frame) >= frame_addr))
#endif
          {
            // Did we reach main ? If not, we probably couldn't unwind
            // till the bottom of frame chain.
            if (unw_prob && !reached_main_in_frame_chain)
              *unw_prob = true;
          }
#endif

        print_bad_pc_unwind_msg = print_bad_pc_unwind_msg_save;
	return NULL;
      }
#ifdef REGISTER_STACK_ENGINE_FP
      /* 010402: srk: Can't rely on frame's $sp with +O1, since $sp may not 
	 be set if all local variables are in registers, so use the rse_fp
	 instead  */
      if (frame->rse_fp == frame_addr)
#else
      if (FRAME_FP (frame) == frame_addr)
#endif
        {
          print_bad_pc_unwind_msg = print_bad_pc_unwind_msg_save;
	  return frame;
        }
    }
}

#ifdef SIGCONTEXT_PC_OFFSET
/* Get saved user PC for sigtramp from sigcontext for BSD style sigtramp.  */

CORE_ADDR
sigtramp_saved_pc (struct frame_info *frame)
{
  CORE_ADDR sigcontext_addr;
  char buf[TARGET_PTR_BIT / TARGET_CHAR_BIT];
  int ptrbytes = TARGET_PTR_BIT / TARGET_CHAR_BIT;
  int sigcontext_offs = (2 * TARGET_INT_BIT) / TARGET_CHAR_BIT;

  /* Get sigcontext address, it is the third parameter on the stack.  */
  if (frame->next)
    sigcontext_addr = read_memory_integer (FRAME_ARGS_ADDRESS (frame->next)
					   + FRAME_ARGS_SKIP
					   + sigcontext_offs,
					   ptrbytes);
  else
    sigcontext_addr = read_memory_integer (read_register (SP_REGNUM)
					   + sigcontext_offs,
					   ptrbytes);

  /* Don't cause a memory_error when accessing sigcontext in case the stack
     layout has changed or the stack is corrupt.  */
  target_read_memory (sigcontext_addr + SIGCONTEXT_PC_OFFSET, buf, ptrbytes);
  return extract_unsigned_integer (buf, ptrbytes);
}
#endif /* SIGCONTEXT_PC_OFFSET */


/* Are we in a call dummy?  The code below which allows DECR_PC_AFTER_BREAK
   below is for infrun.c, which may give the macro a pc without that
   subtracted out.  */

extern CORE_ADDR text_end;

int
pc_in_call_dummy_before_text_end (CORE_ADDR pc, CORE_ADDR sp, CORE_ADDR frame_address)
{
  return ((pc) >= (text_end -
#ifdef INSTRUCTION_SIZE
		   (REGISTER_SIZE / INSTRUCTION_SIZE) *
#endif
		   CALL_DUMMY_LENGTH)
	  && (pc) <= text_end + DECR_PC_AFTER_BREAK);
}

int
pc_in_call_dummy_after_text_end (CORE_ADDR pc, CORE_ADDR sp, CORE_ADDR frame_address)
{
  return ((pc) >= text_end
	  && (pc) <= (text_end +
#ifdef INSTRUCTION_SIZE
		      (REGISTER_SIZE / INSTRUCTION_SIZE) * 
#endif
		      CALL_DUMMY_LENGTH +
		      DECR_PC_AFTER_BREAK));
}

/* Is the PC in a call dummy?  SP and FRAME_ADDRESS are the bottom and
   top of the stack frame which we are checking, where "bottom" and
   "top" refer to some section of memory which contains the code for
   the call dummy.  Calls to this macro assume that the contents of
   SP_REGNUM and FP_REGNUM (or the saved values thereof), respectively,
   are the things to pass.

   This won't work on the 29k, where SP_REGNUM and FP_REGNUM don't
   have that meaning, but the 29k doesn't use ON_STACK.  This could be
   fixed by generalizing this scheme, perhaps by passing in a frame
   and adding a few fields, at least on machines which need them for
   PC_IN_CALL_DUMMY.

   Something simpler, like checking for the stack segment, doesn't work,
   since various programs (threads implementations, gcc nested function
   stubs, etc) may either allocate stack frames in another segment, or
   allocate other kinds of code on the stack.  */

int
pc_in_call_dummy_on_stack (CORE_ADDR pc, CORE_ADDR sp, CORE_ADDR frame_address)
{
  return (INNER_THAN ((sp), (pc))
	  && (frame_address != 0)
	  && INNER_THAN ((pc), (frame_address)));
}

int
pc_in_call_dummy_at_entry_point (CORE_ADDR pc, CORE_ADDR sp, CORE_ADDR frame_address)
{
  return ((pc) >= CALL_DUMMY_ADDRESS ()
	  && (pc) <= (CALL_DUMMY_ADDRESS () + DECR_PC_AFTER_BREAK));
}


/*
 * GENERIC DUMMY FRAMES
 * 
 * The following code serves to maintain the dummy stack frames for
 * inferior function calls (ie. when gdb calls into the inferior via
 * call_function_by_hand).  This code saves the machine state before 
 * the call in host memory, so we must maintain an independant stack 
 * and keep it consistant etc.  I am attempting to make this code 
 * generic enough to be used by many targets.
 *
 * The cheapest and most generic way to do CALL_DUMMY on a new target
 * is probably to define CALL_DUMMY to be empty, CALL_DUMMY_LENGTH to
 * zero, and CALL_DUMMY_LOCATION to AT_ENTRY.  Then you must remember
 * to define PUSH_RETURN_ADDRESS, because no call instruction will be
 * being executed by the target.  Also FRAME_CHAIN_VALID as
 * generic_{file,func}_frame_chain_valid and FIX_CALL_DUMMY as
 * generic_fix_call_dummy.  */

/* Dummy frame.  This saves the processor state just prior to setting
   up the inferior function call.  Older targets save the registers
   on the target stack (but that really slows down function calls).  */

struct dummy_frame
{
  struct dummy_frame *next;

  CORE_ADDR pc;
  CORE_ADDR fp;
  CORE_ADDR sp;
  CORE_ADDR top;
  char *registers;
};

static struct dummy_frame *dummy_frame_stack = NULL;

/* Function: find_dummy_frame(pc, fp, sp)
   Search the stack of dummy frames for one matching the given PC, FP and SP.
   This is the work-horse for pc_in_call_dummy and read_register_dummy     */

char *
generic_find_dummy_frame (CORE_ADDR pc, CORE_ADDR fp)
{
  struct dummy_frame *dummyframe;

  if (pc != entry_point_address ())
    return 0;

  for (dummyframe = dummy_frame_stack; dummyframe != NULL;
       dummyframe = dummyframe->next)
    if (fp == dummyframe->fp
	|| fp == dummyframe->sp
	|| fp == dummyframe->top)
      /* The frame in question lies between the saved fp and sp, inclusive */
      return dummyframe->registers;

  return 0;
}

/* Function: pc_in_call_dummy (pc, fp)
   Return true if this is a dummy frame created by gdb for an inferior call */

int
generic_pc_in_call_dummy (CORE_ADDR pc, CORE_ADDR sp, CORE_ADDR fp)
{
  /* if find_dummy_frame succeeds, then PC is in a call dummy */
  /* Note: SP and not FP is passed on. */
  return (generic_find_dummy_frame (pc, sp) != 0);
}

/* Function: read_register_dummy 
   Find a saved register from before GDB calls a function in the inferior */

CORE_ADDR
generic_read_register_dummy (CORE_ADDR pc, CORE_ADDR fp, int regno)
{
  char *dummy_regs = generic_find_dummy_frame (pc, fp);

  if (dummy_regs)
    return extract_address (&dummy_regs[REGISTER_BYTE (regno)],
			    REGISTER_RAW_SIZE (regno));
  else
    return 0;
}

/* Save all the registers on the dummy frame stack.  Most ports save the
   registers on the target stack.  This results in lots of unnecessary memory
   references, which are slow when debugging via a serial line.  Instead, we
   save all the registers internally, and never write them to the stack.  The
   registers get restored when the called function returns to the entry point,
   where a breakpoint is laying in wait.  */

void
generic_push_dummy_frame ()
{
  struct dummy_frame *dummy_frame;
  CORE_ADDR fp = (get_current_frame ())->frame;

  /* check to see if there are stale dummy frames, 
     perhaps left over from when a longjump took us out of a 
     function that was called by the debugger */

  dummy_frame = dummy_frame_stack;
  while (dummy_frame)
    if (INNER_THAN (dummy_frame->fp, fp))	/* stale -- destroy! */
      {
	dummy_frame_stack = dummy_frame->next;
	free (dummy_frame->registers);
	free (dummy_frame);
	dummy_frame = dummy_frame_stack;
      }
    else
      dummy_frame = dummy_frame->next;

  dummy_frame = xmalloc (sizeof (struct dummy_frame));
  dummy_frame->registers = xmalloc (REGISTER_BYTES);

  dummy_frame->pc = read_pc ();
  dummy_frame->sp = read_sp ();
  dummy_frame->top = dummy_frame->sp;
  dummy_frame->fp = fp;
  read_register_bytes (0, dummy_frame->registers, REGISTER_BYTES);
  dummy_frame->next = dummy_frame_stack;
  dummy_frame_stack = dummy_frame;
}

void
generic_save_dummy_frame_tos (CORE_ADDR sp)
{
  dummy_frame_stack->top = sp;
}

/* Restore the machine state from either the saved dummy stack or a
   real stack frame. */

void
generic_pop_current_frame (void (*popper) (struct frame_info * frame))
{
  struct frame_info *frame = get_current_frame ();

  if (PC_IN_CALL_DUMMY (frame->pc, frame->frame, frame->frame))
    generic_pop_dummy_frame ();
  else
    (*popper) (frame);
}

/* Function: pop_dummy_frame
   Restore the machine state from a saved dummy stack frame. */

void
generic_pop_dummy_frame ()
{
  struct dummy_frame *dummy_frame = dummy_frame_stack;

  /* FIXME: what if the first frame isn't the right one, eg..
     because one call-by-hand function has done a longjmp into another one? */

  if (!dummy_frame)
    error ("Can't pop dummy frame!");
  dummy_frame_stack = dummy_frame->next;
  write_register_bytes (0, dummy_frame->registers, REGISTER_BYTES);
  flush_cached_frames ();

  free (dummy_frame->registers);
  free (dummy_frame);
}

/* Function: frame_chain_valid 
   Returns true for a user frame or a call_function_by_hand dummy frame,
   and false for the CRT0 start-up frame.  Purpose is to terminate backtrace */

int
generic_file_frame_chain_valid (CORE_ADDR fp, struct frame_info *fi)
{
  if (PC_IN_CALL_DUMMY (FRAME_SAVED_PC (fi), fp, fp))
    return 1;			/* don't prune CALL_DUMMY frames */
  else				/* fall back to default algorithm (see frame.h) */
    return (fp != 0
	    && (INNER_THAN (fi->frame, fp) || fi->frame == fp)
	    && !inside_entry_file (FRAME_SAVED_PC (fi)));
}

int
generic_func_frame_chain_valid (CORE_ADDR fp, struct frame_info *fi)
{
  if (PC_IN_CALL_DUMMY ((fi)->pc, fp, fp))
    return 1;			/* don't prune CALL_DUMMY frames */
  else				/* fall back to default algorithm (see frame.h) */
    return (fp != 0
	    && (INNER_THAN (fi->frame, fp) || fi->frame == fp)
	    && !inside_main_func ((fi)->pc)
	    && !inside_entry_func ((fi)->pc));
}

/* Function: fix_call_dummy
   Stub function.  Generic dumy frames typically do not need to fix
   the frame being created */

void
generic_fix_call_dummy (char *dummy, CORE_ADDR pc, CORE_ADDR fun, int nargs,
                        struct value **args, struct type *type, int gcc_p)
{
  return;
}

/* Function: get_saved_register
   Find register number REGNUM relative to FRAME and put its (raw,
   target format) contents in *RAW_BUFFER.  

   Set *OPTIMIZED if the variable was optimized out (and thus can't be
   fetched).  Note that this is never set to anything other than zero
   in this implementation.

   Set *LVAL to lval_memory, lval_register, or not_lval, depending on
   whether the value was fetched from memory, from a register, or in a
   strange and non-modifiable way (e.g. a frame pointer which was
   calculated rather than fetched).  We will use not_lval for values
   fetched from generic dummy frames.

   Set *ADDRP to the address, either in memory on as a REGISTER_BYTE
   offset into the registers array.  If the value is stored in a dummy
   frame, set *ADDRP to zero.

   To use this implementation, define a function called
   "get_saved_register" in your target code, which simply passes all
   of its arguments to this function.

   The argument RAW_BUFFER must point to aligned memory.  */

void
generic_get_saved_register (char *raw_buffer, int *optimized, CORE_ADDR *addrp,
                            struct frame_info *frame, int regnum, enum lval_type *lval)
{
  if (!target_has_registers)
    error ("No registers.");

  /* Normal systems don't optimize out things with register numbers.  */
  if (optimized != NULL)
    *optimized = 0;

  if (addrp)			/* default assumption: not found in memory */
    *addrp = 0;

  /* Note: since the current frame's registers could only have been
     saved by frames INTERIOR TO the current frame, we skip examining
     the current frame itself: otherwise, we would be getting the
     previous frame's registers which were saved by the current frame.  */

  while (frame && ((frame = frame->next) != NULL))
    {
      if (PC_IN_CALL_DUMMY (frame->pc, frame->frame, frame->frame))
	{
	  if (lval)		/* found it in a CALL_DUMMY frame */
	    *lval = not_lval;
	  if (raw_buffer)
	    memcpy (raw_buffer,
		    generic_find_dummy_frame (frame->pc, frame->frame) +
		    REGISTER_BYTE (regnum),
		    REGISTER_RAW_SIZE (regnum));
	  return;
	}

      FRAME_INIT_SAVED_REGS (frame);
      if (frame->saved_regs != NULL
	  && frame->saved_regs[regnum] != 0)
	{
	  if (lval)		/* found it saved on the stack */
	    *lval = lval_memory;
	  if (regnum == SP_REGNUM)
	    {
	      if (raw_buffer)	/* SP register treated specially */
		store_address (raw_buffer, REGISTER_RAW_SIZE (regnum),
			       frame->saved_regs[regnum]);
	    }
	  else
	    {
	      if (addrp)	/* any other register */
		*addrp = frame->saved_regs[regnum];
	      if (raw_buffer)
		read_memory (frame->saved_regs[regnum], raw_buffer,
			     REGISTER_RAW_SIZE (regnum));
	    }
	  return;
	}
    }

  /* If we get thru the loop to this point, it means the register was
     not saved in any frame.  Return the actual live-register value.  */

  if (lval)			/* found it in a live register */
    *lval = lval_register;
  if (addrp)
    *addrp = REGISTER_BYTE (regnum);
  if (raw_buffer)
    read_register_gen (regnum, raw_buffer);
}

void
_initialize_blockframe (void)
{
  obstack_init (&frame_cache_obstack);
}
