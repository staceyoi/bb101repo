/* Everything about breakpoints, for GDB.
   Copyright 1986, 87, 89, 90, 91, 92, 93, 94, 95, 96, 97, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>

#include "defs.h"
#include <ctype.h>
#include "symtab.h"
#include "frame.h"
#include "breakpoint.h"
#include "gdbtypes.h"
#include "expression.h"
#include "gdbcore.h"
#include "gdbcmd.h"
#include "value.h"
#include "command.h"
#include "inferior.h"
#include "gdbthread.h"
#include "target.h"
#include "language.h"
#include "gdb_string.h"
#include "demangle.h"
#include "annotate.h"
#include "symfile.h"
#include "objfiles.h"
#include "top.h"
#include "rtc.h"
#include "thread.h"
#ifdef UI_OUT
#include "ui-out.h"
#endif

#include "gdb-events.h"
#include "unwind_dld.h"

#ifdef HP_IA64
#include <sys/utsname.h>
#include <sys/dyntune.h>
#endif

void (*break_hook) PARAMS ((struct breakpoint *));
void (*delete_hook) PARAMS ((struct breakpoint *));
void (*disable_hook) PARAMS ((struct breakpoint *));
void (*enable_hook) PARAMS ((struct breakpoint *));

extern int shlib_load_completed; /* JAGaf35513 */

extern struct symbol *lookup_func_symbol_from_objfile (struct objfile *, const char *);

extern int is_solib_mapped_private (void);

extern void initiate_sigwait_signals (void); /* infttrace */


/* local function prototypes */

static void
catch_command_1 PARAMS ((char *, int, int));

static void
enable_delete_command PARAMS ((char *, int));

static void
enable_delete_breakpoint PARAMS ((struct breakpoint *));

static void
enable_once_command PARAMS ((char *, int));

static void
enable_once_breakpoint PARAMS ((struct breakpoint *));

static void
     disable_command PARAMS ((char *, int));

static void
     enable_command PARAMS ((char *, int));

     static void
     map_breakpoint_numbers PARAMS ((char *, void (*)(struct breakpoint *)));

     static void
     ignore_command PARAMS ((char *, int));

     static int
     breakpoint_re_set_one PARAMS ((char *));

     void
     delete_command PARAMS ((char *, int));

     static void
     clear_command PARAMS ((char *, int));

     static void
     catch_command PARAMS ((char *, int));

     static void
     handle_gnu_4_16_catch_command PARAMS ((char *, int, int));


     static struct symtabs_and_lines
     get_catch_sals PARAMS ((int));

     static void
     watch_command PARAMS ((char *, int));

     static void
     watch_target_command PARAMS ((char *, int));

     static int
     can_use_hardware_watchpoint PARAMS ((struct value *));

     void
     tbreak_command PARAMS ((char *, int));
     
     static void 
     tbreak_at_finish_command (char *, int);


     static void
     break_command_1 PARAMS ((char *, int, int));

     static void
     mention PARAMS ((struct breakpoint *));

     struct breakpoint *
       set_raw_breakpoint PARAMS ((struct symtab_and_line));

     static void
     check_duplicates PARAMS ((CORE_ADDR, asection *));

     static void
     describe_other_breakpoints PARAMS ((CORE_ADDR, asection *));

     static void
     breakpoints_info PARAMS ((char *, int));

     static void
     breakpoint_1 PARAMS ((int, int));

     static void procedure_bp ( int);

     static bpstat
       bpstat_alloc PARAMS ((struct breakpoint *, bpstat));

     static int
     breakpoint_cond_eval PARAMS ((char *));

     static void
     cleanup_executing_breakpoints PARAMS ((PTR));

     static void
     commands_command PARAMS ((char *, int));

     static void
     condition_command PARAMS ((char *, int));

     /* Command Added for the Purpose of setting breakpoints
        on every debuggable procedure and associating a set
        of commands to it  4/3/02 ,Dinesh */
    
     static void rbreak_commands (char *, int);
  
     static int get_number_trailer (char **, int);

     int
     get_number PARAMS ((char **));

     void
     set_breakpoint_count PARAMS ((int));

    /*  JAGae31194 */
     int 
     is_file_colon_line PARAMS((char *));

/* JAGae68084 persistent deferred breakpoints */
int if_yes_to_all_commands;
int breakall;
int xbreakall;
int rbreakall;
char *input;

extern char *shllib_name;

extern void rbreak_all_command (char *, int);

void place_breakpoints_in_shlib(void);

static void break_at_finish_command(char *arg, int from_tty);


typedef enum
  {
    mark_inserted,
    mark_uninserted
  }
insertion_state_t;

static void xbreak_command (char *, int);


     static int
     remove_breakpoint PARAMS ((struct breakpoint *, insertion_state_t));
static enum print_stop_action print_it_typical (bpstat);

static enum print_stop_action print_bp_stop_message (bpstat bs);

     typedef struct
       {
	 enum exception_event_kind kind;
	 int enable;
       }
args_for_catchpoint_enable;

     typedef struct
       {
	 char **exp_string_p;
	 struct block *block;
	 struct expression *result;
       }
args_for_parse_exp_1;

     typedef struct
       {
	 char **addr_string_p;
	 char ***canonical_p;
	 struct symtabs_and_lines result;
       }
args_for_decode_line_1;

     static struct symtab_and_line *
       cover_target_enable_exception_callback PARAMS ((args_for_catchpoint_enable *));

     static int
     watchpoint_check PARAMS ((char *));

     int cover_parse_exp_1(args_for_parse_exp_1 *);

     void
     maintenance_info_breakpoints PARAMS ((char *, int));

#ifdef GET_LONGJMP_TARGET
     static void
     create_longjmp_breakpoint PARAMS ((char *));
#endif

     static enum bptype
     convert_to_hardware_breakpoint (enum bptype);

     boolean
     is_hw_break (enum bptype);

     /* JAGaf26864 - Making this a non-static function as it is required
        in infttrace.c.
     */
     int
     next_hw_breakpoint (boolean);

     static int
     hw_breakpoint_used_count PARAMS ((void));

     static int
     hw_internal_bp_used_count PARAMS ((void));

     static int
     hw_watchpoint_used_count PARAMS ((enum bptype, int *));

     static void
     hbreak_command PARAMS ((char *, int));

     static void
     thbreak_command PARAMS ((char *, int));

     static void
     watch_command_1 PARAMS ((char *, int, int));

     static void
     rwatch_command PARAMS ((char *, int));

     static void
     awatch_command PARAMS ((char *, int));

     static void
     do_enable_breakpoint PARAMS ((struct breakpoint *, enum bpdisp));

static void create_fork_vfork_event_catchpoint (int tempflag,
						char *cond_string,
						enum bptype bp_kind);

static void break_at_finish_at_depth_command_1 (char *arg,
						int flag, int from_tty);

void break_at_finish_command_1 (char *arg, int flag, int from_tty);

/* JAGaf54451 - breakpoint not triggered using <stop in/at> in non-dbx mode */
/* Command list pointer for the "stop" placeholder */ 
struct cmd_list_element *stop_hook_command;
/* JAGaf54451 - END */

static void stop_command (char *arg, int from_tty);

static void stopin_command (char *arg, int from_tty);

static void stopat_command (char *arg, int from_tty);

static char *ep_find_event_name_end (char *arg);

static char *ep_parse_optional_if_clause (char **arg);

static char *ep_parse_optional_filename (char **arg);

#if defined(CHILD_INSERT_EXEC_CATCHPOINT)
static void catch_exec_command_1 (char *arg, int tempflag, int from_tty);
#endif

static void catch_exception_command_1
  (enum exception_event_kind ex_event, char *arg, int tempflag, int from_tty);

static void tcatch_command (char *arg, int from_tty);

static void ep_skip_leading_whitespace (char **s);

/* Prototypes for exported functions. */

static void awatch_command (char *, int);

static void do_enable_breakpoint (struct breakpoint *, enum bpdisp);

/* If FALSE, gdb will not use hardware support for watchpoints, even
   if such is available. */
     static int can_use_hw_watchpoints;

     void _initialize_breakpoint (void); 

     void set_breakpoint_count (int);

     int
     ep_is_exception_catchpoint PARAMS ((struct breakpoint * ep));

     extern void
     select_and_print_frame PARAMS ((struct frame_info * fi, int level));

     int debug_traces;

     extern int addressprint;	/* Print machine addresses? */

#if defined (GET_LONGJMP_TARGET) || defined (SOLIB_ADD)
     static int internal_breakpoint_number = -1;
#endif

void
parse_breakpoint_sals (char **address,
		       struct symtabs_and_lines *sals,
		       char ***addr_string,
		       enum how_to_resolve_multiple_symbols resolve);

/* Are we executing breakpoint commands?  */
     static int executing_breakpoint_commands;

/* Walk the following statement or block through all breakpoints.
   ALL_BREAKPOINTS_SAFE does so even if the statment deletes the current
   breakpoint.  */
/* RM: yes, but ALL_BREAKPOINTS_SAFE is _not_ safe if we delete the
 * following breakpoint in the chain. Since a breakpoint's related_breakpoint
 * field may point to any other breakpoint, be careful when deleting
 * related_breakpoints.
 */
#define ALL_BREAKPOINTS(b)  for (b = breakpoint_chain; b; b = b->next)

#define ALL_BREAKPOINTS_SAFE(b,tmp)	\
	for (b = breakpoint_chain;	\
	     b? (tmp=b->next, 1): 0;	\
	     b = tmp)

/* True if SHIFT_INST_REGS defined, false otherwise.  */

     int must_shift_inst_regs =
#if defined(SHIFT_INST_REGS)
     1
#else
     0
#endif
      ;

/* This contains the range of breakpoints that has been set
   by rbp , Dinesh 04/15/02 */ 

struct break_commands_list {
     int min; 
     int max;
     char *break_commands;
     struct command_line *commands_list;
     struct break_commands_list *next;
    } *break_commands_chain;

/* True if breakpoint hit counts should be displayed in breakpoint info.  */

     int show_breakpoint_hit_counts = 1;

/* Chain of all breakpoints defined.  */

     struct breakpoint *breakpoint_chain;

/* srikanth, JAGaa80394, in MT programs, under certain circumstances
   deleted breakpoints may appear to trigger. This happens when two
   or more threads hit the same breakpoint, we report it on behalf
   of one thread and then the user deletes the breakpoint and continues
   the process. The other thread(s) have already "hit" the breakpoint
   and are stopped. When we try to analyze the cause for their stoppage
   the breakpoint will be missing and we would report a message
   saying, "Program received signal SIGTRAP ..."

   The fix is to remeber the deleted breakpoints in a separate chain
   and if thread T2 got a SIGTRAP and there is no breakpoint at
   the PC value for thread T2, and if there was a breakpoint, to
   silently ignore the SIGTRAP.
*/
struct deleted_breakpoint {
  CORE_ADDR address;
  struct deleted_breakpoint * next;
} * deleted_breakpoint_chain;

/* Number of last breakpoint made.  */

     int breakpoint_count;

/* Procedural Breakpoint flag */

int procedure_flag = 0;

/* JAGaf23775 - xbp command should override overload menu selection.
   This variable is used to overide or skip menu . 
   As of now it is set to true in xbreak_command */
boolean skip_bp_menu = false;

/* Pointer to current exception event record */
     static struct exception_event_record *current_exception_event;

/* Indicator of whether exception catchpoints should be nuked
   between runs of a program */
     int exception_catchpoints_are_fragile = 0;

/* Indicator of when exception catchpoints set-up should be
   reinitialized -- e.g. when program is re-run */
     int exception_support_initialized = 0;

/* This function returns a pointer to the string representation of the
   pathname of the dynamically-linked library that has just been
   loaded.

   This function must be used only when SOLIB_HAVE_LOAD_EVENT is TRUE,
   or undefined results are guaranteed.

   This string's contents are only valid immediately after the
   inferior has stopped in the dynamic linker hook, and becomes
   invalid as soon as the inferior is continued.  Clients should make
   a copy of this string if they wish to continue the inferior and
   then access the string.  */

#ifndef SOLIB_LOADED_LIBRARY_PATHNAME
#define SOLIB_LOADED_LIBRARY_PATHNAME(pid) ""
#endif

/* This function returns a pointer to the string representation of the
   pathname of the dynamically-linked library that has just been
   unloaded.

   This function must be used only when SOLIB_HAVE_UNLOAD_EVENT is
   TRUE, or undefined results are guaranteed.

   This string's contents are only valid immediately after the
   inferior has stopped in the dynamic linker hook, and becomes
   invalid as soon as the inferior is continued.  Clients should make
   a copy of this string if they wish to continue the inferior and
   then access the string.  */

#ifndef SOLIB_UNLOADED_LIBRARY_PATHNAME
#define SOLIB_UNLOADED_LIBRARY_PATHNAME(pid) ""
#endif

/* This function is called by the "catch load" command.  It allows the
   debugger to be notified by the dynamic linker when a specified
   library file (or any library file, if filename is NULL) is loaded.  */

#ifndef SOLIB_CREATE_CATCH_LOAD_HOOK
#define SOLIB_CREATE_CATCH_LOAD_HOOK(pid,tempflag,filename,cond_string) \
   error ("catch of library loads not yet implemented on this platform")
#endif

/* This function is called by the "catch unload" command.  It allows
   the debugger to be notified by the dynamic linker when a specified
   library file (or any library file, if filename is NULL) is
   unloaded.  */

#ifndef SOLIB_CREATE_CATCH_UNLOAD_HOOK
#define SOLIB_CREATE_CATCH_UNLOAD_HOOK(pid,tempflag,filename,cond_string) \
   error ("catch of library unloads not yet implemented on this platform")
#endif

/* Set this when you don't want catch_errors()'s underlying error() to print
   the error message. */
extern int donot_print_errors;

/* Bindu 10/27/03: To remember whether sbrk_event_breakpoint is set or not. */
static int sbrk_breakpoint_set = 0;

static void create_sbrk_event_breakpoint (char*);

#define CREATE_SBRK_BREAKPOINTS \
  {  \
    create_sbrk_event_breakpoint ("brk");\
    create_sbrk_event_breakpoint ("sbrk");\
    create_sbrk_event_breakpoint ("mmap");  \
    sbrk_breakpoint_set = 1; \
  }

typedef struct {
  struct expression *exp;
  value_ptr valp;
} args_for_evaluate_expression;

/* JAGag06703 */
#define DEBUG(x)
extern int  extract_predicate_register PARAMS ((CORE_ADDR));
extern int  ia64_insert_breakpoint_1 PARAMS ((CORE_ADDR, char*, struct breakpoint*));
extern boolean no_predication_handling;

/* Same as evaluate_expression but can be used for catch_errors. */
int 
evaluate_expression_1 (PTR args)
{
  args_for_evaluate_expression *args1 = args;
  args1->valp = evaluate_expression (args1->exp);
  return 1;
}

/* This type is used by parse_expression_1 */
typedef struct {
  char* exp_string;
  struct expression* exp;
} args_for_parse_expression;

/* Same as parse_expression but can be used for catch_errors. */
int 
parse_expression_1 (PTR args)
{
  args_for_parse_expression *args1 = args;
  args1->exp = parse_expression (args1->exp_string);
  return 1;
}

/* Same as value_fetch_lazy, but can be used for catch_errors. */
int 
value_fetch_lazy_1 (PTR args)
{
  value_ptr valp = args;
  value_fetch_lazy (valp);
  return 1;
}


boolean 
are_addrs_equal (CORE_ADDR addr1, CORE_ADDR addr2)
{
  if (!is_swizzled)	/* PA32, PA64 & IA64 64-bit, i.e, no swizzling. */
    return (addr1 == addr2);
#ifdef HP_IA64
  else			/* IA64 32-bit address. */
    /* Some kernel addresses in 32-bit mode have more than 32-bits in the 
     * address.  Though the debugger doesn't set breakpoints there, it has 
     * to deal with comparing those because some times the inferior can 
     * be stopped at those addresses.  Swizzle check can't be used there.
     */
    if ((addr1 >> 32 & 0xFFFFFFF) || (addr2 >> 32 & 0xFFFFFFF))
      return (addr1 == addr2);
#endif /* HP_IA64 */
    else
      return (SWIZZLE (addr1) == SWIZZLE (addr2));
}


/* Set breakpoint count to NUM.  */

     void
     set_breakpoint_count (int num)
{
  breakpoint_count = num;
  set_internalvar (lookup_internalvar ("bpnum"),
		   value_from_longest (builtin_type_int, (LONGEST) num));
}

int
get_next_breakpoint_count ()
{
  set_breakpoint_count (breakpoint_count + 1);
  return breakpoint_count;
}

/* Used in run_command to zero the hit count when a new run starts. */

void
clear_breakpoint_hit_counts ()
{
  struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    b->hit_count = 0;
}

void
invalidate_shadow_contents ()
{
  struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    b->shadow_contents_known_to_be_valid = 0;
}

/* Default address, symtab and line to put a breakpoint at
   for "break" command with no arg.
   if default_breakpoint_valid is zero, the other three are
   not valid, and "break" with no arg is an error.

   This set by print_stack_frame, which calls set_default_breakpoint.  */

int default_breakpoint_valid;
CORE_ADDR default_breakpoint_address;
struct symtab *default_breakpoint_symtab;
int default_breakpoint_line;

/* *PP is a string denoting a breakpoint.  Get the number of the breakpoint.
   Advance *PP after the string and any trailing whitespace.

   Currently the string can either be a number or "$" followed by the name
   of a convenience variable.  Making it an expression wouldn't work well
   for map_breakpoint_numbers (e.g. "4 + 5 + 6").
   
   TRAILER is a character which can be found after the number; most
   commonly this is `-'.  If you don't want a trailer, use \0.  */ 
static int
get_number_trailer (char **pp, int trailer)
{
  int retval = 0;	/* default */
  char *p = *pp;

  if (p == NULL)
    /* Empty line means refer to the last breakpoint.  */
    return breakpoint_count;
  else if (*p == '$')
    {
      /* Make a copy of the name, so we can null-terminate it
         to pass to lookup_internalvar().  */
      char *varname;
      char *start = ++p;
      value_ptr val;

      while (isalnum (*p) || *p == '_')
	p++;
      varname = (char *) alloca (p - start + 1);
      strncpy (varname, start, p - start);
      varname[p - start] = '\0';
      val = value_of_internalvar (lookup_internalvar (varname));
      if (TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_INT)
	retval = (int) value_as_long (val);
      else
	{
	  printf_filtered ("Convenience variable must have integer value.\n");
	  retval = 0;
	}
    }
  else
    {
      if (*p == '-')
	++p;
      while (*p >= '0' && *p <= '9')
	++p;
      if (p == *pp)
	/* There is no number here.  (e.g. "cond a == b").  */
	{
	  /* Skip non-numeric token */
	  while (*p && !isspace((int) *p))
	    ++p;
	  /* Return zero, which caller must interpret as error. */
	  retval = 0;
	}
      else
	retval = atoi (*pp);
    }
  if (!(isspace (*p) || *p == '\0' || *p == trailer))
    {
      /* Trailing junk: return 0 and let caller print error msg. */
      while (!(isspace (*p) || *p == '\0' || *p == trailer))
	++p;
      retval = 0;
    }
  while (isspace (*p))
    p++;
  *pp = p;
  return retval;
}

/* Like get_number_trailer, but don't allow a trailer.  */
int
get_number (char **pp)
{
  int retval;
  char *p = *pp;

  if (p == NULL)
    /* Empty line means refer to the last breakpoint.  */
    return breakpoint_count;
  else if (*p == '$')
    {
      /* Make a copy of the name, so we can null-terminate it
         to pass to lookup_internalvar().  */
      char *varname;
      char *start = ++p;
      value_ptr val;

      while (isalnum (*p) || *p == '_')
	p++;
      varname = (char *) alloca (p - start + 1);
      strncpy (varname, start, p - start);
      varname[p - start] = '\0';
      val = value_of_internalvar (lookup_internalvar (varname));
      if (TYPE_CODE (VALUE_TYPE (val)) != TYPE_CODE_INT)
	error (
		"Convenience variables used to specify breakpoints must have integer values."
	  );
      retval = (int) value_as_long (val);
    }
  else
    {
      if (*p == '-')
	++p;
      while (*p >= '0' && *p <= '9')
	++p;
      if (p == *pp)
	/* There is no number here.  (e.g. "cond a == b").  */
	error_no_arg ("breakpoint number");
      retval = atoi (*pp);
    }
  if (!(isspace (*p) || *p == '\0'))
    error ("breakpoint number expected");
  while (isspace (*p))
    p++;
  *pp = p;
  return retval;
}

/* Parse a number or a range.
 * A number will be of the form handled by get_number.
 * A range will be of the form <number1> - <number2>, and 
 * will represent all the integers between number1 and number2,
 * inclusive.
 *
 * While processing a range, this fuction is called iteratively;
 * At each call it will return the next value in the range.
 *
 * At the beginning of parsing a range, the char pointer PP will
 * be advanced past <number1> and left pointing at the '-' token.
 * Subsequent calls will not advance the pointer until the range
 * is completed.  The call that completes the range will advance
 * pointer PP past <number2>.
 */

int 
get_number_or_range (char **pp)
{
  static int last_retval, end_value;
  static char *end_ptr;
  static int in_range = 0;

  if (**pp != '-')
    {
      /* Default case: pp is pointing either to a solo number, 
	 or to the first number of a range.  */
      last_retval = get_number_trailer (pp, '-');
      if (**pp == '-')
	{
	  char **temp;

	  /* This is the start of a range (<number1> - <number2>).
	     Skip the '-', parse and remember the second number,
	     and also remember the end of the final token.  */

	  temp = &end_ptr; 
	  end_ptr = *pp + 1; 
	  while (isspace ((int) *end_ptr))
	    end_ptr++;	/* skip white space */
	  end_value = get_number (temp);
	  if (end_value < last_retval) 
	    {
	      error ("inverted range");
	    }
	  else if (end_value == last_retval)
	    {
	      /* degenerate range (number1 == number2).  Advance the
		 token pointer so that the range will be treated as a
		 single number.  */ 
	      *pp = end_ptr;
	    }
	  else
	    in_range = 1;
	}
    }
  else if (! in_range)
    error ("negative value");
  else
    {
      /* pp points to the '-' that betokens a range.  All
	 number-parsing has already been done.  Return the next
	 integer value (one greater than the saved previous value).
	 Do not advance the token pointer 'pp' until the end of range
	 is reached.  */

      if (++last_retval == end_value)
	{
	  /* End of range reached; advance token pointer.  */
	  *pp = end_ptr;
	  in_range = 0;
	}
    }
  return last_retval;
}

/* Go through the breakpoint chain and disable the expressions being watched for. */
void
disable_break_exp_chain (struct objfile *obj)
{
  struct breakpoint *b;
  ALL_BREAKPOINTS (b)
  /* for watchpoints */
    if (b->exp)
      {
        if (lookup_func_symbol_from_objfile (obj, b->exp_string) ||
            lookup_minimal_symbol (b->exp_string, NULL, obj))
          {
            /* remove watchpoints, for all memory references of value_chain of watchpoint b */
            remove_breakpoint (b, mark_uninserted);
            if (b->enable != user_disabled)
              b->enable = heap_disabled;
            free (b->exp);
            b->exp = NULL;
            continue;
          }
      }         
}


/* condition N EXP -- set break condition of breakpoint N to EXP.  */

static void
condition_command (char *arg, int from_tty)
{
  register struct breakpoint *b;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  register struct breakpoint *related_breakpoint;
#endif
  char *p;
  register int bnum;

  if (arg == 0)
    error_no_arg ("breakpoint number");

  p = arg;
  bnum = get_number (&p);
  if (bnum == 0)
    error ("Bad breakpoint argument: '%s'", arg);

  ALL_BREAKPOINTS (b)
    if (b->number == bnum)
    {
      if (b->cond)
	{
	  free ((PTR) b->cond);
	  b->cond = 0;
	}
      if (b->cond_string != NULL)
	free ((PTR) b->cond_string);

      if (*p == 0)
	{
	  b->cond = 0;
	  b->cond_string = NULL;
	  if (from_tty)
	    printf_filtered ("Breakpoint %d now unconditional.\n", bnum);
	}
      else
	{
	  arg = p;
	  /* I don't know if it matters whether this is the string the user
	     typed in or the decompiled expression.  */
	  b->cond_string = savestring (arg, (int)strlen (arg));
	  b->cond = parse_exp_1 (&arg, block_for_pc (b->address), 0);

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
          /* If inline_debugging == BP_ALL then apply the condition on all 
             related breakpoints ie all the instances of this inline.
          */
          if (inline_debugging == BP_ALL && b->inline_idx != 0)
            {
              for (related_breakpoint = b->related_breakpoint; related_breakpoint != NULL;
                   related_breakpoint = related_breakpoint->related_breakpoint)
                {
                  related_breakpoint->cond_string = savestring (arg, (int)strlen (arg));
                  related_breakpoint->cond = parse_exp_1 (&arg,
                                                          block_for_pc (related_breakpoint->address),
                                                          0);
                }
            }              
#endif 
	  if (*arg)
	    error ("Junk at end of expression");
	}
      breakpoints_changed ();
      return;
    }

  error ("No breakpoint number %d.", bnum);
}

static void
xbreak_command (char *regexp, int from_tty)
{
  /* min and max holds the lower and upper bound of the breakpoint
     number of the breakpoints that are set by rbreak */
  int lowbp = 0;
  int highbp = 0;
  char tmpbuf[128];
  struct break_commands_list *temp;

  if (regexp)
    error ("The \"xbp\" command does not take any arguments.");
  
  lowbp = breakpoint_count;
  procedure_flag = 1;
  skip_bp_menu = true;
  rbreak_command_1 (regexp, 0, from_tty);
  highbp = breakpoint_count;
  procedure_flag = 0;
  skip_bp_menu = false;

  if (lowbp == highbp)
    return;
  else
    {  
      printf_filtered ("Breakpoints set from %d to %d\n",lowbp+1,highbp);
      sprintf(tmpbuf,"%d-%d",lowbp+1,highbp);
      temp = xmalloc (sizeof (struct break_commands_list));
      temp->min = lowbp + 1;
      temp->max = highbp;
      sprintf(tmpbuf,"%d-%d",lowbp+1,highbp);
      temp->break_commands = strsave(tmpbuf);
      temp->commands_list = NULL;
      temp->next=break_commands_chain; 
      break_commands_chain=temp;
    }

}
  
/* This is the function that actually combines both the rbreak 
   and commands command by calling the appropriate function of
   the respective commands , 04/03/02 Dinesh */

static void
rbreak_commands (char *regexp, int from_tty)
{
  /* min and max holds the lower and upper bound of the breakpoint
     number of the breakpoints that are set by rbreak */
  int lowbp = 0;
  int highbp = 0;
  register struct breakpoint *b;
  struct command_line *l;
  struct break_commands_list *temp;
  char tmpbuf[128];
  char tmpbuf1[128];

  if (regexp)
    error ("The \"rbp\" command does not take any arguments.");
  
  lowbp = breakpoint_count;
  procedure_flag = 1;
  
  rbreak_command_1 (regexp, 1, from_tty);
 
  highbp = breakpoint_count;
  procedure_flag = 0;
     
     
/* No breakpoint set No need to associate a command to it */
  if (lowbp == highbp)
    return;
  else
    {  
      printf_filtered ("\nBreakpoints set from %d to %d\n",lowbp+1,highbp);
      sprintf(tmpbuf,"%d-%d",lowbp+1,highbp);
      temp = xmalloc (sizeof (struct break_commands_list));
      temp->min = lowbp + 1;
      temp->max = highbp;
      sprintf(tmpbuf,"%d-%d",lowbp+1,highbp);
      temp->break_commands = strsave(tmpbuf);
  
      sprintf (tmpbuf,
               "Type commands to execute when this breakpoint is hit (one command per line)."
              );

      /* Reading the command list */ 
      l = read_command_lines (tmpbuf, from_tty);
      temp->commands_list=l;
      temp->next=break_commands_chain; 
      break_commands_chain=temp;
    }
  /* Associating the command to the breakpoint already planted 
   by the rbreak command */
  ALL_BREAKPOINTS (b)
    if (b->number >= lowbp+1 && b->number <= highbp)
      {
        free_command_lines (&b->commands);
        b->commands = l;
        breakpoints_changed ();
      }
  return;
}


/* Find out whether this breakpoint is in the range of 
   breakpoints set by the command rbp, 04/15/02 , Dinesh */

static int
check_duplicates_commands (int bpt_number)
{
  int is_bp_procedure = 0;
  struct break_commands_list *rbp_list;
  rbp_list = break_commands_chain;
  while (rbp_list)
  {
    if (bpt_number >= rbp_list->min && bpt_number <= rbp_list->max)
      {
       is_bp_procedure = 1;
       break;
      }
    rbp_list = rbp_list->next;
  }

 return(is_bp_procedure);
}

/* ARGSUSED */
static void
commands_command (char *arg, int from_tty)
{
  register struct breakpoint *b;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  register struct breakpoint *related_breakpoint;
#endif
  char *p;
  register int bnum;
  struct command_line *l;

  /* If we allowed this, we would have problems with when to
     free the storage, if we change the commands currently
     being read from.  */

  if (executing_breakpoint_commands)
    error ("Can't use the \"commands\" command among a breakpoint's commands.");

  p = arg;
  bnum = get_number (&p);

  if (check_duplicates_commands(bnum))
    error ("Cannot associate commands to procedure breakpoint(s)");
    
  if (p && *p)
    error ("Unexpected extra arguments following breakpoint number.");

  ALL_BREAKPOINTS (b)
    if (b->number == bnum)
    {
      char tmpbuf[128];
      sprintf (tmpbuf, "Type commands for when breakpoint %d is hit, one per line.", bnum);
      l = read_command_lines (tmpbuf, from_tty);
      free_command_lines (&b->commands);
      b->commands = l;

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
      /* If inline_debugging == BP_ALL, then apply the "commands" command on all
         related breakpoints.
      */
      if (inline_debugging == BP_ALL && b->inline_idx != 0)
        {
          for (related_breakpoint = b->related_breakpoint; related_breakpoint != NULL;
               related_breakpoint = related_breakpoint->related_breakpoint)
            {
              related_breakpoint->commands = l;
            }
        }
#endif
      breakpoints_changed ();
      return;
    }
  error ("No breakpoint number %d.", bnum);
}


/* Like target_read_memory() but if breakpoints are inserted, return
   the shadow contents instead of the breakpoints themselves.

   Read "memory data" from whatever target or inferior we have. 
   Returns zero if successful, errno value if not.  EIO is used
   for address out of bounds.  If breakpoints are inserted, returns
   shadow contents, not the breakpoints themselves.  From breakpoint.c.  */

int
read_memory_nobpt (CORE_ADDR memaddr, char *myaddr, unsigned len)
{
  int status;
  struct breakpoint *b;
  CORE_ADDR bp_addr = 0;
  int bp_size = 0;

  if (BREAKPOINT_FROM_PC (&bp_addr, &bp_size) == NULL)
    /* No breakpoints on this machine. */
    return target_read_memory (memaddr, myaddr, len);

  ALL_BREAKPOINTS (b)
  {
    if (b->type == bp_none)
      warning ("reading through apparently deleted breakpoint #%d?", 
	       b->number);

    /* memory breakpoint? */
    if (b->type == bp_watchpoint
	|| b->type == bp_hardware_watchpoint
	|| b->type == bp_read_watchpoint
	|| b->type == bp_access_watchpoint)
      continue;
    /* bp in memory? */
    if (!b->inserted)
      continue;
    /* Addresses and length of the part of the breakpoint that
       we need to copy.  */
    /* XXXX The m68k, sh and h8300 have different local and remote
       breakpoint values.  BREAKPOINT_FROM_PC still manages to
       correctly determine the breakpoints memory address and size
       for these targets. */
    bp_addr = b->address;
    bp_size = 0;
    if (BREAKPOINT_FROM_PC (&bp_addr, &bp_size) == NULL)
      continue;
    if (bp_size == 0)
      /* bp isn't valid */
      continue;
    if (bp_addr + bp_size <= memaddr)
      /* The breakpoint is entirely before the chunk of memory we
         are reading.  */
      continue;
    if (bp_addr >= memaddr + len)
      /* The breakpoint is entirely after the chunk of memory we are
         reading. */
      continue;
    /* Copy the breakpoint from the shadow contents, and recurse for
       the things before and after.  */
    {
      /* Offset within shadow_contents.  */
      int bptoffset = 0;

      if (bp_addr < memaddr)
	{
	  /* Only copy the second part of the breakpoint.  */
	  bp_size -= memaddr - bp_addr;
	  bptoffset = (int) (memaddr - bp_addr);
	  bp_addr = memaddr;
	}

      if (bp_addr + bp_size > memaddr + len)
	{
	  /* Only copy the first part of the breakpoint.  */
	  bp_size -= (bp_addr + bp_size) - (memaddr + len);
	}

      memcpy (myaddr + bp_addr - memaddr,
	      b->shadow_contents + bptoffset, bp_size);

      if (bp_addr > memaddr)
	{
	  /* Copy the section of memory before the breakpoint.  */
	  status = read_memory_nobpt (memaddr, myaddr, (unsigned int) (bp_addr - memaddr));
	  if (status != 0)
	    return status;
	}

      if (bp_addr + bp_size < memaddr + len)
	{
	  /* Copy the section of memory after the breakpoint.  */
	  status = read_memory_nobpt
	    (bp_addr + bp_size,
	     myaddr + bp_addr + bp_size - memaddr,
	     (unsigned int)(memaddr + len - (bp_addr + bp_size)));
	  if (status != 0)
	    return status;
	}
      return 0;
    }
  }
  /* Nothing overlaps.  Just call read_memory_noerr.  */
  return target_read_memory (memaddr, myaddr, len);
}

/* insert_breakpoints is used when starting or continuing the program.
   remove_breakpoints is used when the program stops.
   Both return zero if successful,
   or an `errno' value if could not write the inferior.  */

int attach_msg_given = 0;
int
insert_breakpoints ()
{
  register struct breakpoint *b, *temp = NULL;
  int return_val = 0;	/* return success code. */
  int val = 0;
  int disabled_breaks = 0;

  static char message1[] = "Error inserting catchpoint %d:\n";
  static char message[sizeof (message1) + 30];
  char *so;


  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    if (b->enable == permanent)
       /* Permanent breakpoints cannot be inserted or removed.  */
       continue;
    else if (b->type != bp_watchpoint
	&& b->type != bp_hardware_watchpoint
	&& b->type != bp_read_watchpoint
	&& b->type != bp_access_watchpoint
	&& b->type != bp_catch_fork
	&& b->type != bp_catch_vfork
	&& b->type != bp_catch_exec
	&& b->type != bp_catch_throw
	&& b->type != bp_catch_catch
#ifdef DYNLINK_HAS_BREAK_HOOK
	&& b->type != bp_catch_load
	&& b->type != bp_catch_unload
#endif
	&& b->enable != disabled
	&& b->enable != user_disabled
	&& b->enable != shlib_disabled
	&& b->enable != call_disabled
	&& !b->inserted
	&& !b->duplicate)
      {
	if (is_hw_break (b->type))
	  val = target_insert_hw_breakpoint (b->address, b->shadow_contents);
	else
	  {
	    /* Check to see if breakpoint is in an overlay section;
	       if so, we should set the breakpoint at the LMA address.
	       Only if the section is currently mapped should we ALSO
	       set a break at the VMA address. */
	    if (overlay_debugging && b->section &&
		section_is_overlay (b->section))
	      {
		CORE_ADDR addr;

		addr = overlay_unmapped_address (b->address, b->section);
#ifdef HP_IA64
		val = ia64_insert_breakpoint_1 (addr, b->shadow_contents, b);
#else
		val = target_insert_breakpoint (addr, b->shadow_contents);
#endif
		/* This would be the time to check val, to see if the
		   breakpoint write to the load address succeeded.  
		   However, this might be an ordinary occurrance, eg. if 
		   the unmapped overlay is in ROM.  */
		val = 0;	/* in case unmapped address failed */
		if (section_is_mapped (b->section))
#ifdef HP_IA64
		  val = ia64_insert_breakpoint_1 (b->address, 
						  b->shadow_contents, b);
#else
		  val = target_insert_breakpoint (b->address,
						  b->shadow_contents);
#endif
	      }
	    else		/* ordinary (non-overlay) address */
#ifdef GDB_TARGET_IS_HPPA
             /* srikanth, 000131, don't bother to reread the shadow
                 contents. It is not going to change as long as the
                 process is the same.
              */
              if (b->shadow_contents_known_to_be_valid)
                val = target_insert_breakpoint (b->address, 0);
              else 
                val = target_insert_breakpoint (b->address, b->shadow_contents);
             
             b->shadow_contents_known_to_be_valid = 1;
#else
               val = ia64_insert_breakpoint_1 (b->address, b->shadow_contents, b);
#endif    
	  }
#if defined (DISABLE_UNSETTABLE_BREAK)
	/* Bindu 031405: If the address is in the shared library, then convert
	   it to hardware breakpoint. */
        /* No hardware breakpoints for bp_longjmp, bp_longjmp_resume */
	if (val  && DISABLE_UNSETTABLE_BREAK (b->address) &&
	    (   b->type == bp_breakpoint
	     || b->type == bp_finish
	     || b->type == bp_until
	     || b->type == bp_watchpoint_scope
	     || b->type == bp_through_sigtramp
	     || b->type == bp_rtc_event
	     || b->type == bp_rtc_compiler_event
	     || b->type == bp_catch_nomem
	     || b->type == bp_step_resume))
	  {
	    int i = next_hw_breakpoint (   b->type == bp_finish
                                        || b->type == bp_until
                                        || b->type == bp_watchpoint_scope
                                        || b->type == bp_through_sigtramp
					|| b->type == bp_rtc_event
					|| b->type == bp_rtc_compiler_event
                                        || b->type == bp_step_resume);
            int target_resources_ok =
              TARGET_CAN_USE_HARDWARE_WATCHPOINT (bp_hardware_breakpoint,
                                                i + 1, 0);
	    b->type = convert_to_hardware_breakpoint (b->type);
            if (target_resources_ok > 0)
	      {
                val = target_insert_hw_breakpoint (b->address, b->shadow_contents);
	      }
	    if (info_verbose && target_resources_ok < 0)
	      warning ("HW breakpoints reached their limit. Cannot set breakpoint %d", b->number);
	  }
#endif
	if (val)
	  {
#ifdef GDB_TARGET_IS_HPPA
            b->shadow_contents_known_to_be_valid = 0;
#endif
	    /* Can't set the breakpoint.  */
#if defined (DISABLE_UNSETTABLE_BREAK)
	    if (DISABLE_UNSETTABLE_BREAK (b->address))
	      {
		/* See also: disable_breakpoints_in_shlibs. */
		val = 0;
		b->enable = shlib_disabled;
                /* No message for batch rtc attach */
	        if (!batch_rtc && !attach_msg_given)
	        {
                /* re-check for b->type for longjmps */
		if (!disabled_breaks && (b->type != bp_longjmp && b->type != bp_longjmp_resume))
		  {
		    so = PC_SOLIB(b->address);
#ifdef GDB_TARGET_IS_HPUX
		    target_terminal_ours_for_output ();
                    warning ("Cannot insert breakpoint %d: in %s", b->number, so);
                    /*pes,JAGae20530 : If a user attaches a process in gdb and 
		      does a continue to hit the breakpoint in a shared library,
		      do warn the following HP-UX limitation */
		    if ( attach_flag ) 
 		     {
#ifdef HP_IA64	       
		       struct utsname uts;
		       int sys_ver = 0;
		       uname (&uts);
		       if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
	 	       warning ("uname: Couldn't resolve the release identifier"				" of the Operating system.");
		       if (sys_ver == 31)
			{
		  	  uint64_t value = 0;
			  /* Check whether the varibale is set. If not present
                             ask user to install the patches.
                          */
			  if (gettune ((const char *)"shlib_debug_enable", 
				&value) == -1 && errno == ENOENT)
			   {
			     warning ("This is because your shared libraries"
				      " are not mapped private. To attach to a"
		                      " process and debug its shared libraries"
		                      " you must prepare the program with \n"
		                      "\"/opt/langtools/bin/pxdb -s on" 
                                      " <executable>  \" or \n \"chatr +dbg" 
		                      " enable <executable> \"."
			              " or \n install the patches PHKL_38651"
				      " and PHKL_38778 \n");
			  }
			 else if (value == 0)
			    warning ("This is because your shared libraries"
                                     " are not mapped private. To attach to a"
                                     " process and debug its shared libraries\n"
  				     "Please set the kernel variable "
			             "\"shlib_debug_enable\" to 1 to enable the"
				     " shared library debugging\n");
			}
		      else	
#endif	                
		        {	
                          warning ("This is because your shared libraries"
                                   " are not mapped private. To attach to a"
                                   " process and debug its shared libraries"
                                   " you must prepare the program with \n"
                                   "\"/opt/langtools/bin/pxdb -s on"
                                   " <executable>  \" or \n \"chatr +dbg"
                                   " enable <executable> \".");
                          warning ("Add this to your Makefile for debug "
				   "builds so that each rebuilt debuggable"
				   " a.out would have this feature turned on.");
	
	                }
		      }
		    /* Fix for JAGae68606. 	- Bharath, Apr 29 2003 */
		    if ( strstr (so, "dld.so") != NULL ) 
 		     {
		        warning ("dld.so is a shared library and will never be loaded");
		        warning ("as private, so breakpoint %d has been ignored.", b->number);
	             }
		    printf_filtered ("Temporarily disabling shared library breakpoints:");		
#else
		    fprintf_unfiltered (gdb_stderr,
			       "Cannot insert breakpoint %d: in %s\n", b->number, so);
		    warning ("Temporarily disabling shared library breakpoints:");		
#endif
		  }
		disabled_breaks = 1;
		printf_filtered ("%d ", b->number);
	        }
	      }
	    else
#endif
	      {
		target_terminal_ours_for_output ();
		fprintf_unfiltered (gdb_stderr, "Cannot insert breakpoint %d:\n", b->number);

#ifdef ONE_PROCESS_WRITETEXT
		fprintf_unfiltered (gdb_stderr,
		   "The same program may be running in another process.\n");
#endif
		memory_error (val, b->address);		/* which bombs us out */
	      }
	  }
	else
#ifdef HP_IA64 
          if (!b->duplicate)
          /* We might encounter duplicates while trying to insert breaks. */ 
#endif
	    b->inserted = 1;

          if (val)
	  return_val = val; /*remember failure */
      
     
      }
    else if (ep_is_exception_catchpoint (b)
	     && b->enable != disabled
	     && b->enable != shlib_disabled
	     && b->enable != call_disabled
	     && !b->inserted
	     && !b->duplicate)

      {
	/* If we get here, we must have a callback mechanism for exception
	   events -- with g++ style embedded label support, we insert
	   ordinary breakpoints and not catchpoints. */
	struct symtab_and_line *sal;
	args_for_catchpoint_enable args;
	sprintf (message, message1, b->number);		/* Format possible error message */

#ifdef HP_IA64
	val = ia64_insert_breakpoint_1 (b->address, b->shadow_contents, b);
#else
	val = target_insert_breakpoint (b->address, b->shadow_contents);
#endif

	if (val)
	  {
	    /* Couldn't set breakpoint for some reason */
	    target_terminal_ours_for_output ();
	    fprintf_unfiltered (gdb_stderr,
		  "Cannot insert catchpoint %d; disabling it\n", b->number);
	    b->enable = disabled;
	  }
	else
	  {
	    /* Bp set, now make sure callbacks are enabled */
	    args.kind = b->type == bp_catch_catch ? EX_EVENT_CATCH : EX_EVENT_THROW;
	    args.enable = 1;
	    sal = (struct symtab_and_line *)(long)
	      catch_errors ((catch_errors_ftype *)
			      cover_target_enable_exception_callback,
			    (char *) &args,
			    message, RETURN_MASK_ALL);
	    if (sal && (sal != (struct symtab_and_line *)-1L))
	      {
		b->inserted = 1;
	      }
	    /* Check if something went wrong; sal == 0 can be ignored */
	    if (sal == (struct symtab_and_line *) -1L)
	      {
		/* something went wrong */
		target_terminal_ours_for_output ();
		fprintf_unfiltered (gdb_stderr, "Cannot insert catchpoint %d; disabling it\n", b->number);
		b->enable = disabled;
	      }
	  }
      }
    else if (   (   (   (   b->type == bp_hardware_watchpoint
			 || b->type == bp_read_watchpoint
			 || b->type == bp_access_watchpoint)
		     && (b->enable == enabled)) 
		 || (b->enable == heap_disabled))
             && b->disposition != del_at_next_stop
	     && !b->inserted
	     && !b->duplicate)
      {
	struct frame_info *saved_frame;
	int saved_level, within_current_scope;
	value_ptr mark = value_mark ();
	value_ptr v;
        boolean unw_prob = false;

	/* Save the current frame and level so we can restore it after
	   evaluating the watchpoint expression on its own frame.  */
	saved_frame = selected_frame;
	saved_level = selected_frame_level;

	/* Determine if the watchpoint is within scope.  */
	if (b->exp_valid_block == NULL)
	  within_current_scope = 1;
	else
	  {
	    struct frame_info *fi;

	    /* There might be no current frame at this moment if we are
	       resuming from a step over a breakpoint.
	       Set up current frame before trying to find the watchpoint
	       frame.  */
	    get_current_frame ();
	    fi = find_frame_addr_in_frame_chain (b->watchpoint_frame,
                                                 &unw_prob);
	    within_current_scope = (fi != NULL);
	    if (within_current_scope)
	      select_frame (fi, -1);
	  }

	if (within_current_scope)
	  {
	    /* Evaluate the expression and cut the chain of values
	       produced off from the value chain.  */
            /* Try to enable heap_disabled watchpoint as well. */
	    if (b->enable == heap_disabled)
	      {
		args_for_evaluate_expression args;
		int mem_cnt = 0;
		int i, other_type_used, target_resources_ok = 0;
		donot_print_errors = 1;

                /* Breakpoint expression gets re-set to null on unloads, so 
                   we need to re-parse the string expression to re-create it. 
                   If we can't do it, leave the breakpoint heap_disabled. */
                if (b->exp == NULL)
                  {
                    args_for_parse_expression parse_args;
                    parse_args.exp_string = b->exp_string;
                    parse_args.exp = NULL;
                    if (!catch_errors ((catch_errors_ftype *) parse_expression_1,
                                       (char*) &parse_args, 
                                       NULL, RETURN_MASK_ALL))
                      {
                        /* Can't parse expression, leave it as heap_disabled.
                           Cleanup! */
                        donot_print_errors = 0;
                        value_release_to_mark (mark);
                        continue;
                      }
                    b->exp = parse_args.exp;
                  }

		args.exp = b->exp;
		args.valp = NULL;
		if (!catch_errors ((catch_errors_ftype *) evaluate_expression_1,
					(char *) &args,
					NULL, RETURN_MASK_ALL))
    		  {
		    /* Can't evaluate expression, leave it as heap_disabled.
		       Cleanup! */
		    donot_print_errors = 0;
	    	    value_release_to_mark (mark);
		    continue;
		  }
		if (VALUE_LAZY (args.valp))
		  if (!catch_errors ((catch_errors_ftype *) value_fetch_lazy_1,
				(char*) args.valp,
				NULL, RETURN_MASK_ALL))
		    {
		    /* Can't evaluate expression, leave it as heap_disabled.
		       Cleanup! */
		      donot_print_errors = 0;
		      value_release_to_mark (mark);
		      continue;
		    }
		/* Enable this watchpoint now. */
		donot_print_errors = 0;
		mem_cnt = can_use_hardware_watchpoint (args.valp);
		if (mem_cnt != 0)
		  {
		    i = hw_watchpoint_used_count (bp_hardware_watchpoint,
					 &other_type_used);
      		    target_resources_ok = TARGET_CAN_USE_HARDWARE_WATCHPOINT (
				     bp_hardware_watchpoint, i + mem_cnt,
				     other_type_used);
		    if ((mem_cnt && target_resources_ok > 0))
		      b->type = bp_hardware_watchpoint;
    	  	  }
		b->enable = enabled;
                release_value (args.valp);
		b->val = args.valp;
	      }
	    v = evaluate_expression (b->exp);
	    if (VALUE_LAZY (v))
	      value_fetch_lazy (v);
	    value_release_to_mark (mark);

	    b->val_chain = v;
	    b->inserted = 1;

	    /* Look at each value on the value chain.  */
	    for (; v; v = v->next)
	      {
		/* If it's a memory location, and GDB actually needed
		   its contents to evaluate the expression, then we
		   must watch it.  */
		if (v->lval == lval_memory
		    && ! VALUE_LAZY (v))
		  {
		    struct type *vtype = check_typedef (VALUE_TYPE (v));

		    /* We only watch structs and arrays if user asked
		       for it explicitly, never if they just happen to
		       appear in the middle of some value chain.  */

		    if (v == b->val_chain
			|| (TYPE_CODE (vtype) != TYPE_CODE_STRUCT
			    && TYPE_CODE (vtype) != TYPE_CODE_ARRAY))
                      {
			CORE_ADDR addr;
			int len, type;

			addr = VALUE_ADDRESS (v) + VALUE_OFFSET (v);
			len = TYPE_LENGTH (VALUE_TYPE (v));
			type = 0;
			if (b->type == bp_read_watchpoint)
			  type = 1;
			else if (b->type == bp_access_watchpoint)
			  type = 2;

                        if (debug_traces)
                          printf_filtered("Adding watchpoint addr = 0x%lx, len = %d\n", addr, len);

			val = target_insert_watchpoint (addr, len, type);
			if (val == -1)
			  {
                            /* Don't exit the loop, try to insert
			       every value on the value chain.  That's
			       because we will be removing all the
			       watches below, and removing a
			       watchpoint we didn't insert could have
			       adverse effects.  */
			    b->inserted = 0;
			  }
		        val = 0;
		      }
		  }
	      }
	    /* Failure to insert a watchpoint on any memory value in the
	       value chain brings us here.  */
	    if (!b->inserted)
	      {
		remove_breakpoint (b, mark_uninserted);
	        warning ("Hardware watchpoint %d: Could not insert watchpoint\n",
		         b->number);
		val = -1;
	      }
	  }
	else if (!unw_prob)
	  {
	    printf_filtered ("Hardware watchpoint %d deleted ", b->number);
	    printf_filtered ("because the program has left the block \n");
	    printf_filtered ("in which its expression is valid.\n");
	    if (b->related_breakpoint)
	      b->related_breakpoint->disposition = del_at_next_stop;
	    b->disposition = del_at_next_stop;
	  }

	/* Restore the frame and level.  */
	if ((saved_frame != selected_frame) ||
	    (saved_level != selected_frame_level))
	  select_and_print_frame (saved_frame, saved_level);

       if (val)
	  return_val = val;	/* remember failure */
      }
    else if ((b->type == bp_catch_fork
	      || b->type == bp_catch_vfork
#ifdef DYNLINK_HAS_BREAK_HOOK
	      || b->type == bp_catch_unload
	      || b->type == bp_catch_load
#endif
	      || b->type == bp_catch_exec)
	     && b->enable == enabled
	     && !b->inserted
	     && !b->duplicate)
      {
	val = -1;
	switch (b->type)
	  {
	  case bp_catch_fork:
	    val = target_insert_fork_catchpoint (inferior_pid);
	    break;
	  case bp_catch_vfork:
	    val = target_insert_vfork_catchpoint (inferior_pid);
	    break;
	  case bp_catch_exec:
	    val = target_insert_exec_catchpoint (inferior_pid);
	    break;
	  case bp_catch_load:
	  case bp_catch_unload:
	    val = 0;		/* Handled in shared library implementation */
	    break;
	  default:
	    val = -1;
	    break;
	  }
	if (val < 0)
	  {
	    target_terminal_ours_for_output ();
	    fprintf_unfiltered (gdb_stderr, "Cannot insert catchpoint %d:\n", b->number);
	  }
	else
	  b->inserted = 1;

        if (val)
	  return_val = val;	/* remember failure */
      }
  }
  if (disabled_breaks)
    {
      printf_filtered ("\n");
      attach_msg_given = 1;
    }

 return return_val;
}

int
remove_breakpoint_at (CORE_ADDR pc)
{
  register struct breakpoint *b;
  int val;

  ALL_BREAKPOINTS (b)
  {
    if (b->inserted && are_addrs_equal (b->address, pc))
      {
        val = remove_breakpoint (b, mark_uninserted);
        if (val != 0)
          return val;
      }
  }
  return 0;
}

int
remove_breakpoints ()
{
  register struct breakpoint *b;
  int val;
  int return_value;

  return_value = 0;
  ALL_BREAKPOINTS (b)
  {
    if (b->inserted)
      {
	val = remove_breakpoint (b, mark_uninserted);
	if (val != 0)
	  return_value = val;
      }
  }
  return return_value;
}

int
remove_hw_watchpoints (void)
{
  register struct breakpoint *b;
  int val;

  ALL_BREAKPOINTS (b)
  {
    if (b->inserted
	&& (b->type == bp_hardware_watchpoint
	    || b->type == bp_read_watchpoint
	    || b->type == bp_access_watchpoint))
      {
	val = remove_breakpoint (b, mark_uninserted);
	if (val != 0)
	  return val;
      }
  }
  return 0;
}

int
reattach_breakpoints (int pid)
{
  register struct breakpoint *b;
  int val;
  int saved_inferior_pid = inferior_pid;

  inferior_pid = pid;		/* Because remove_breakpoint will use this global. */
  ALL_BREAKPOINTS (b)
  {
    if (b->inserted)
      {
	remove_breakpoint (b, mark_inserted);
	if ( is_hw_break (b->type))
	  val = target_insert_hw_breakpoint (b->address, b->shadow_contents);
	else
#ifdef HP_IA64
          val = ia64_insert_breakpoint_1 (b->address, b->shadow_contents, b);
#else
	  val = target_insert_breakpoint (b->address, b->shadow_contents);
#endif

	if (val != 0)
	  {
	    inferior_pid = saved_inferior_pid;
	    return val;
	  }
      }
  }
  inferior_pid = saved_inferior_pid;
  return 0;
}

void
update_breakpoints_after_exec ()
{
  struct breakpoint *b;
  struct breakpoint *temp = NULL;
 
  invalidate_shadow_contents ();
  /* Doing this first prevents the badness of having delete_breakpoint()
     write a breakpoint's current "shadow contents" to lift the bp.  That
     shadow is NOT valid after an exec()!
   */
  mark_breakpoints_out ();

  /* We need to disable shared library breakpoints here and try to replace
     them at each library load. */
  disable_breakpoints_in_shlibs (0, NULL);

  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    /* Solib breakpoints must be explicitly reset after an exec(). */
    if (b->type == bp_shlib_event)
      {
	delete_breakpoint (b);
	continue;
      }

    if (   b->type == bp_rtc_event
    	|| b->type == bp_hw_rtc_event
    	|| b->type == bp_rtc_compiler_event
    	|| b->type == bp_hw_rtc_compiler_event
	|| b ->type == bp_catch_nomem
	|| b ->type == bp_hw_catch_nomem)
      {
        delete_breakpoint (b);
        continue;
      }

    if (b->type == bp_fix_event)
      {
        delete_breakpoint (b);
        continue;
      }

    /* Thread event breakpoints must be set anew after an exec().  */
    if (b->type == bp_thread_event)
      {
	delete_breakpoint (b);
	continue;
      }

    /* Step-resume breakpoints are meaningless after an exec(). */
    if ((b->type == bp_step_resume) || (b->type == bp_hw_step_resume))
      {
	delete_breakpoint (b);
	continue;
      }

    /* Ditto the sigtramp handler breakpoints. */
    if (b->type == bp_through_sigtramp || b->type == bp_hw_through_sigtramp)
      {
	delete_breakpoint (b);
	continue;
      }

    /* Ditto the exception-handling catchpoints. */
    if (ep_is_exception_catchpoint (b))
      {
	delete_breakpoint (b);
	continue;
      }

    /* Don't delete an exec catchpoint, because else the inferior
       won't stop when it ought!

       Similarly, we probably ought to keep vfork catchpoints, 'cause
       on this target, we may not be able to stop when the vfork is seen,
       but only when the subsequent exec is seen.  (And because deleting
       fork catchpoints here but not vfork catchpoints will seem mysterious
       to users, keep those too.)

       ??rehrauer: Let's hope that merely clearing out this catchpoint's
       target address field, if any, is sufficient to have it be reset
       automagically.  Certainly on HP-UX that's true.
     */
    if ((b->type == bp_catch_exec) ||
	(b->type == bp_catch_vfork) ||
#ifdef DYNLINK_HAS_BREAK_HOOK
	(b->type == bp_catch_load) ||
	(b->type == bp_catch_unload) ||
#endif
	(b->type == bp_catch_fork))
      {
	b->address = (CORE_ADDR) NULL;
	continue;
      }

    /* bp_finish is a special case.  The only way we ought to be able
       to see one of these when an exec() has happened, is if the user
       caught a vfork, and then said "finish".  Ordinarily a finish just
       carries them to the call-site of the current callee, by setting
       a temporary bp there and resuming.  But in this case, the finish
       will carry them entirely through the vfork & exec.

       We don't want to allow a bp_finish to remain inserted now.  But
       we can't safely delete it, 'cause finish_command has a handle to
       the bp on a bpstat, and will later want to delete it.  There's a
       chance (and I've seen it happen) that if we delete the bp_finish
       here, that its storage will get reused by the time finish_command
       gets 'round to deleting the "use to be a bp_finish" breakpoint.
       We really must allow finish_command to delete a bp_finish.

       In the absense of a general solution for the "how do we know it's
       safe to delete something others may have handles to?" problem, what
       we'll do here is just uninsert the bp_finish, and let finish_command
       delete it.

       (We know the bp_finish is "doomed" in the sense that it's momentary,
       and will be deleted as soon as finish_command sees the inferior stopped.
       So it doesn't matter that the bp's address is probably bogus in the
       new a.out, unlike e.g., the solib breakpoints.)
     */
    if ((b->type == bp_finish) || (b->type == bp_hw_finish))
      {
	continue;
      }

    /* Without a symbolic address, we have little hope of the
       pre-exec() address meaning the same thing in the post-exec()
       a.out.
     */
    /* RM: for watchpoints, we need an exp_string */
    if (((b->type == bp_watchpoint || b->type == bp_hardware_watchpoint) &&
	 b->exp_string == NULL) ||
	((b->type != bp_watchpoint && b->type != bp_hardware_watchpoint) && b->addr_string == NULL))
      {
	delete_breakpoint (b);
	continue;
      }

    /* If this breakpoint has survived the above battery of checks, then
       it must have a symbolic address.  Be sure that it gets reevaluated
       to a target address, rather than reusing the old evaluation.
     */
    b->address = (CORE_ADDR) NULL;
  }
}

int
detach_breakpoints (int pid)
{
#ifdef GDB_TARGET_IS_HPUX
  extern boolean keep_as_protected;
#endif
  register struct breakpoint *b;
  int val = 0;
  int saved_inferior_pid = inferior_pid;

  if (pid == inferior_pid)
    error ("Cannot detach breakpoints of inferior_pid");

  inferior_pid = pid;		/* Because remove_breakpoint will use this global. */
#ifdef SET_TRACE_BIT
  SET_TRACE_BIT (UNSET_FLAGS); /*remove dld breakpoint _asm_break */
#endif
  keep_as_protected = 1;
  ALL_BREAKPOINTS (b)
  {
    if (b->inserted)
      {
	val = remove_breakpoint (b, mark_inserted);
	if (val != 0)
	  {
	    break;
	  }
      }
  }
  inferior_pid = saved_inferior_pid;
#ifdef GDB_TARGET_IS_HPUX
  keep_as_protected = 0;
#endif
  return val;
}

static int
remove_breakpoint (struct breakpoint *b, insertion_state_t is)
{
  int val;

  if (b->enable == permanent)
    /* Permanent breakpoints cannot be inserted or removed.  */
    return 0;


  if (b->type == bp_none)
    warning ("attempted to remove apparently deleted breakpoint #%d?\n", b->number);

  if (b->type != bp_watchpoint
      && b->type != bp_hardware_watchpoint
      && b->type != bp_read_watchpoint
      && b->type != bp_access_watchpoint
      && b->type != bp_catch_fork
      && b->type != bp_catch_vfork
      && b->type != bp_catch_exec
      && b->type != bp_catch_catch
#ifdef DYNLINK_HAS_BREAK_HOOK
      && b->type != bp_catch_load
      && b->type != bp_catch_unload
#endif

      && b->type != bp_catch_throw)

    {
      if (is_hw_break (b->type))
	val = target_remove_hw_breakpoint (b->address, b->shadow_contents);
      else
	{
	  /* Check to see if breakpoint is in an overlay section;
	     if so, we should remove the breakpoint at the LMA address.
	     If that is not equal to the raw address, then we should 
	     presumable remove the breakpoint there as well.  */
	  if (overlay_debugging && b->section &&
	      section_is_overlay (b->section))
	    {
	      CORE_ADDR addr;

	      addr = overlay_unmapped_address (b->address, b->section);
	      val = target_remove_breakpoint (addr, b->shadow_contents);
	      /* This would be the time to check val, to see if the
	         shadow breakpoint write to the load address succeeded.  
	         However, this might be an ordinary occurrance, eg. if 
	         the unmapped overlay is in ROM.  */
	      val = 0;		/* in case unmapped address failed */
	      if (section_is_mapped (b->section))
		val = target_remove_breakpoint (b->address,
						b->shadow_contents);
	    }
	  else			/* ordinary (non-overlay) address */
	    val = target_remove_breakpoint (b->address, b->shadow_contents);
	}
      if (val)
	return val;
      b->inserted = (is == mark_inserted);
    }
  else if ((b->type == bp_hardware_watchpoint ||
	    b->type == bp_read_watchpoint ||
	    b->type == bp_access_watchpoint)
	   && b->enable == enabled
	   && !b->duplicate)
    {
      value_ptr v, n;

      b->inserted = (is == mark_inserted);
      /* Walk down the saved value chain.  */
      for (v = b->val_chain; v; v = v->next)
	{
	  /* For each memory reference remove the watchpoint
	     at that address.  */
          if (VALUE_LVAL (v) == lval_memory
	      && ! VALUE_LAZY (v))
	    {
	      struct type *vtype = check_typedef (VALUE_TYPE (v));

	      if (v == b->val_chain
		  || (TYPE_CODE (vtype) != TYPE_CODE_STRUCT
		      && TYPE_CODE (vtype) != TYPE_CODE_ARRAY))
		{
		  CORE_ADDR addr;
		  int len, type;

		  addr = VALUE_ADDRESS (v) + VALUE_OFFSET (v);
		  len = TYPE_LENGTH (VALUE_TYPE (v));
		  type   = 0;
		  if (b->type == bp_read_watchpoint)
		    type = 1; 
		  else if (b->type == bp_access_watchpoint)
		    type = 2;

		  val = target_remove_watchpoint (addr, len, type);
		  if (val == -1)
		    b->inserted = 1;
		  val = 0;
		}
           }
	}
      /* Failure to remove any of the hardware watchpoints comes here.  */
      if ((is == mark_uninserted) && (b->inserted))
	warning ("Hardware watchpoint %d: Could not remove watchpoint\n",
		 b->number);

      /* Free the saved value chain.  We will construct a new one
         the next time the watchpoint is inserted.  */
      for (v = b->val_chain; v; v = n)
	{
	  n = v->next;
	  value_free (v);
	}
      b->val_chain = NULL;
    }
  else if ((b->type == bp_catch_fork ||
	    b->type == bp_catch_vfork ||
#ifdef DYNLINK_HAS_BREAK_HOOK
	    b->type == bp_catch_load ||
	    b->type == bp_catch_unload ||
#endif
	    b->type == bp_catch_exec)
	   && b->enable == enabled
	   && !b->duplicate)
    {
      val = -1;
      switch (b->type)
	{
	case bp_catch_fork:
	  val = target_remove_fork_catchpoint (inferior_pid);
	  break;
	case bp_catch_vfork:
	  val = target_remove_vfork_catchpoint (inferior_pid);
	  break;
	case bp_catch_exec:
	  val = target_remove_exec_catchpoint (inferior_pid);
	  break;
#ifdef DYNLINK_HAS_BREAK_HOOK
	case bp_catch_load:
	case bp_catch_unload:
	  val = 0;
	  break;
#endif
	default:
#ifdef GDB_TARGET_IS_HPPA
          warning ("Internal error, %s line %d.", __FILE__, __LINE__);
	  break;
#endif
	  val = -1;
	  break;
	}
      if (val)
	return val;
      b->inserted = (is == mark_inserted);
    }
  else if ((b->type == bp_catch_catch ||
	    b->type == bp_catch_throw)
	   && b->enable == enabled
	   && !b->duplicate)
    {

      val = target_remove_breakpoint (b->address, b->shadow_contents);
      if (val)
	return val;
      b->inserted = (is == mark_inserted);
    }
  else if (ep_is_exception_catchpoint (b)
	   && b->inserted	/* sometimes previous insert doesn't happen */
	   && b->enable == enabled
	   && !b->duplicate)
    {

      val = target_remove_breakpoint (b->address, b->shadow_contents);
      if (val)
	return val;

      b->inserted = (is == mark_inserted);
    }

  return 0;
}

/* Clear the "inserted" flag in all breakpoints.  */

void
mark_breakpoints_out ()
{
  register struct breakpoint *b;

  ALL_BREAKPOINTS (b)
  /* RM: remove hardware watchpoints. The memory protections are
     not actually active when we reach here (beacuse we are in a
     syscall), so there's no effect on the executable image. But
     this does ensure that the memory_page_dictionary data
     structure gets re-initialized */
    if (b->type == bp_hardware_watchpoint &&
	b->enable == enabled &&
	b->inserted &&
	!b->duplicate)
    remove_breakpoint (b, mark_uninserted);
  else
    b->inserted = 0;
}

/* Clear the "inserted" flag in all breakpoints and delete any breakpoints
   which should go away between runs of the program.

   Plus other such housekeeping that has to be done for breakpoints
   between runs.

   Note: this function gets called at the end of a run (by generic_mourn_inferior)
   and when a run begins (by init_wait_for_inferior). */



void
breakpoint_init_inferior (enum inf_context context)
{
  register struct breakpoint *b, *temp = NULL;
  static int warning_needed = 0;
  struct deleted_breakpoint * old_bp, *next;

  /* srikanth, this is a good time to forget deleted breakpoints */

  old_bp = deleted_breakpoint_chain;
  while (old_bp)
    {
      next = old_bp -> next;
      free (old_bp);
      old_bp = next;
    }
  deleted_breakpoint_chain = 0;

  /* RM: Surely we want to disable shlib breakpoints here? */
  disable_breakpoints_in_shlibs (0, NULL);

  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    b->inserted = 0;

    switch (b->type)
      {
      case bp_call_dummy:
      case bp_watchpoint_scope:
      case bp_hw_watchpoint_scope:
      case bp_step_resume:
      case bp_hw_step_resume:

	/* If the call dummy breakpoint is at the entry point it will
	   cause problems when the inferior is rerun, so we better
	   get rid of it. 

	   Also get rid of scope breakpoints and step resume breakpoints.
         */
	if (step_resume_breakpoint == b)
	  step_resume_breakpoint = NULL;
	delete_breakpoint (b);
	break;

      case bp_watchpoint:
      case bp_hardware_watchpoint:
      case bp_read_watchpoint:
      case bp_access_watchpoint:

	/* Likewise for watchpoints on local expressions.  */
	if (b->exp_valid_block != NULL)
	  delete_breakpoint (b);
	else if (b->language == language_fortran && context == inf_exited)
	  { 
	    /* Fortran commons have static addresses, but need a context
	       for finding the symbols, so delete them so to avoid
	       "symbol not found" problems  */
	    delete_breakpoint (b);
	    warning_needed = 1;
	  }
	break;

      case bp_catch_nomem:  /* JAGaf51525 */
      case bp_hw_catch_nomem: 
	/* These breakpoints are created dynamically or on each run, so
	   delete them from the last run. */
	delete_breakpoint (b);
	break;

      default:
	/* Likewise for exception catchpoints in dynamic-linked
	   executables where required */
	if (ep_is_exception_catchpoint (b) &&
	    exception_catchpoints_are_fragile)
	  {
	    warning_needed = 1;
	    delete_breakpoint (b);
	  }
	break;
      }
  }

  if (exception_catchpoints_are_fragile)
    exception_support_initialized = 0;

  /* Don't issue the warning unless it's really needed... */
  if (warning_needed && (context != inf_exited))
    {
      warning ("Exception catchpoints or watchpoints from last run were deleted.");
      warning ("You must reinsert them explicitly.");
      warning_needed = 0;
    }
}

/* breakpoint_here_p (PC) returns 1 if an enabled breakpoint exists at PC.
   When continuing from a location with a breakpoint,
   we actually single step once before calling insert_breakpoints.  */

enum breakpoint_here
breakpoint_here_p (CORE_ADDR pc)
{
  register struct breakpoint *b;
  int any_breakpoint_here = 0;

  ALL_BREAKPOINTS (b)
    if (b->enable == enabled
	&& b->enable != shlib_disabled
	&& b->enable != call_disabled
	&& b->enable != user_disabled
	&& are_addrs_equal (b->address, pc))  /* bp is enabled and matches pc */
     {
      if (overlay_debugging &&
	section_is_overlay (b->section) &&
	!section_is_mapped (b->section))
      continue;			/* unmapped overlay -- can't be a match */
      else if (b->enable == permanent)
	  return permanent_breakpoint_here;
      else
	  any_breakpoint_here = 1;
    }

  return any_breakpoint_here ? ordinary_breakpoint_here : 0 ;
}

/* Suresh, QXCR1000776341, Mar 08: breakpoint_is_disabled_or_enabled_once() returns 1 if a 
   disabled or an enabled once breakpoint exists at PC. */

enum breakpoint_here
breakpoint_is_disabled_or_enabled_once (CORE_ADDR pc)
{
  register struct breakpoint *b;
  int any_breakpoint_here = 0;

  ALL_BREAKPOINTS (b)
    if ( are_addrs_equal (b->address, pc) && 
         ( b->enable == user_disabled || b->enable == disabled || 
          ( b->enable == enabled && b->disposition == disable)))
     {
      if (overlay_debugging &&
	section_is_overlay (b->section) &&
	!section_is_mapped (b->section))
         continue;			/* unmapped overlay -- can't be a match */
      else
	 any_breakpoint_here = 1;
    }

  return any_breakpoint_here ? ordinary_breakpoint_here : 0 ;
}
int 
breakpoint_was_here_p (CORE_ADDR pc)
{
  struct deleted_breakpoint *old_bp;

  old_bp = deleted_breakpoint_chain;
  while (old_bp)
    {
      if (old_bp -> address == pc)
        return 1;
      old_bp = old_bp->next;
    }

  return 0;
}



/* breakpoint_inserted_here_p (PC) is just like breakpoint_here_p (),
   but it returns a pointer to appropriate struct breakpoint if there
   is actually a breakpoint inserted at PC. Otherwise returns 0. 
   Since there are no callers to this function as of now, I am
   slightly tightening the semantics of this routine to match the
   name of this function and suit our needs. Watchpoints don't 
   qualify  -- srikanth.
*/

struct breakpoint *
breakpoint_inserted_here_p (CORE_ADDR pc)
{
  register struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    {
      if (!are_addrs_equal (b->address, pc)
	  || b->type == bp_none 
	  || b->type == bp_watchpoint
	  || b->type == bp_hardware_watchpoint
	  || b->type == bp_read_watchpoint
	  || b->type == bp_access_watchpoint
	  || !b->inserted)
	continue;

      if (   overlay_debugging 
	  && section_is_overlay (b->section)
	  && !section_is_mapped (b->section))
	continue;                 /* unmapped overlay -- can't be a match */
	  
      return b;
    }

  return 0;
}

/* Return nonzero if FRAME is a dummy frame.  We can't use PC_IN_CALL_DUMMY
   because figuring out the saved SP would take too much time, at least using
   get_saved_register on the 68k.  This means that for this function to
   work right a port must use the bp_call_dummy breakpoint.  */

int
frame_in_dummy (struct frame_info *frame)
{
#ifdef CALL_DUMMY
#ifdef USE_GENERIC_DUMMY_FRAMES
  return generic_pc_in_call_dummy (frame->pc, frame->frame, frame->frame);
#else
  struct breakpoint *b;

  ALL_BREAKPOINTS (b)
  {
    static ULONGEST dummy[] = CALL_DUMMY;

    if (b->type == bp_call_dummy
	&& b->frame == frame->frame

    /* We need to check the PC as well as the frame on the sparc,
       for signals.exp in the testsuite.  */
	&& (frame->pc
	    >= (b->address
		- sizeof (dummy) / sizeof (LONGEST) * REGISTER_SIZE))
	&& frame->pc <= b->address)
      return 1;
  }
  return 0;
#endif /* GENERIC_DUMMY_FRAMES */
#else /* CALL_DUMMY */
  return 0;
#endif /* CALL_DUMMY */
}

/* breakpoint_match_thread (PC, PID) returns true if the breakpoint at PC
   is valid for process/thread PID.  */

int
breakpoint_thread_match (CORE_ADDR pc, int pid)
{
  struct breakpoint *b;
  int thread;

#ifdef HP_MXN
  if (is_mxn)
    {
      int utid;
      utid = get_pthread_for (pid);
      if (utid == -1)
	thread = -(get_lwp_for (pid));
      else 
	thread = utid;
    }
  else
#endif
    thread = pid_to_thread_id (pid);

  ALL_BREAKPOINTS (b)
    if (b->enable != disabled
	&& b->enable != shlib_disabled
	&& b->enable != call_disabled
	&& are_addrs_equal (b->address, pc)
	&& (b->thread == -1 || b->thread == thread))
    if (overlay_debugging &&
	section_is_overlay (b->section) &&
	!section_is_mapped (b->section))
      continue;			/* unmapped overlay -- can't be a match */
    else
      return 1;

  return 0;
}


/* bpstat stuff.  External routines' interfaces are documented
   in breakpoint.h.  */

int
ep_is_catchpoint (struct breakpoint *ep)
{
  return
    (ep->type == bp_catch_load)
    || (ep->type == bp_catch_unload)
    || (ep->type == bp_catch_fork)
    || (ep->type == bp_catch_vfork)
    || (ep->type == bp_catch_exec)
    || (ep->type == bp_catch_catch)
    || (ep->type == bp_catch_throw)


  /* ??rehrauer: Add more kinds here, as are implemented... */
    ;
}

int
ep_is_shlib_catchpoint (struct breakpoint *ep)
{
  return
    (ep->type == bp_catch_load)
    || (ep->type == bp_catch_unload)
    ;
}

int
ep_is_exception_catchpoint (struct breakpoint *ep)
{
  return
    (ep->type == bp_catch_catch)
    || (ep->type == bp_catch_throw)
    || (ep->type == bp_catch_ex_internal)
    ;
}

/* Clear a bpstat so that it says we are not at any breakpoint.
   Also free any storage that is part of a bpstat.  */

void
bpstat_clear (bpstat *bsp)
{
  bpstat p;
  bpstat q;

  if (bsp == 0)
    return;
  p = *bsp;
  while (p != NULL)
    {
      q = p->next;
      if (p->old_val != NULL)
	value_free (p->old_val);
      free_command_lines(&p->commands);
      p->commands=NULL;
      free ((PTR) p);
      p = q;
    }
  *bsp = NULL;
}

/* Return a copy of a bpstat.  Like "bs1 = bs2" but all storage that
   is part of the bpstat is copied as well.  */

bpstat
bpstat_copy (bpstat bs)
{
  bpstat p = NULL;
  bpstat tmp;
  bpstat retval = NULL;

  if (bs == NULL)
    return bs;

  for (; bs != NULL; bs = bs->next)
    {
      tmp = (bpstat) xmalloc (sizeof (*tmp));
      memcpy (tmp, bs, sizeof (*tmp));
      if (bs->commands != NULL)
	tmp->commands = copy_command_lines (bs->commands);
      if (p == NULL)
	/* This is the first thing in the chain.  */
	retval = tmp;
      else
	p->next = tmp;
      p = tmp;
    }
  p->next = NULL;
  return retval;
}

/* Find the bpstat associated with this breakpoint */

bpstat
bpstat_find_breakpoint (bpstat bsp, struct breakpoint *breakpoint)
{
  if (bsp == NULL)
    return NULL;

  for (; bsp != NULL; bsp = bsp->next)
    {
      if (bsp->breakpoint_at == breakpoint)
	return bsp;
    }
  return NULL;
}

/* Find a step_resume breakpoint associated with this bpstat.
   (If there are multiple step_resume bp's on the list, this function
   will arbitrarily pick one.)

   It is an error to use this function if BPSTAT doesn't contain a
   step_resume breakpoint.

   See wait_for_inferior's use of this function.
 */
struct breakpoint *
bpstat_find_step_resume_breakpoint (bpstat bsp)
{
  if (bsp == NULL)
    error ("Internal error (bpstat_find_step_resume_breakpoint)");

  for (; bsp != NULL; bsp = bsp->next)
    {
      if ((bsp->breakpoint_at != NULL) &&
	  ((bsp->breakpoint_at->type == bp_step_resume)
	  || (bsp->breakpoint_at->type == bp_hw_step_resume)))
	return bsp->breakpoint_at;
    }

  error ("Internal error (no step_resume breakpoint found)");
  return NULL;
}

/* Given a PC, find the inline_idx of the breakpoint at that PC.
   If there are multiple breakpoints at that PC, pick the largest
   (innermost inline instance).
   If no breakpoint is placed at this PC, return -1. */
int
bpstat_find_inline_idx (CORE_ADDR * pc)
{
  register struct breakpoint *b, *temp = NULL;
  int inline_idx = -1;

  /* Get the address where the breakpoint would have been.  */
  CORE_ADDR bp_addr = *pc - DECR_PC_AFTER_BREAK;

  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    /* RM: If breakpoint has been deleted, keep going. */
    if (b->type == bp_none)
      continue;

    /* Ignore the disbaled/deferred/user_diabled 
       breakpoints/watchpoints. 
    */
    if (b->enable == disabled
        || b->enable == user_disabled
	|| b->enable == shlib_disabled
	|| b->enable == call_disabled
	|| b->enable == heap_disabled)
      continue;

    /* Ignore the non-breakpoints */
    if (   b->type != bp_breakpoint
	&& b->type != bp_finish
	&& b->type != bp_hardware_breakpoint)
      continue;

    if (   !are_addrs_equal (b->address, bp_addr)	/* address doesn't match or */
	     || (   overlay_debugging /* overlay doesn't match */
		 && section_is_overlay (b->section)
		 && !section_is_mapped (b->section)))
	continue;


    /* Come here if the break address matches */
    if (b->inline_idx > inline_idx)
      inline_idx = b->inline_idx;

  }

  return inline_idx;
}


/* Return the breakpoint number of the first breakpoint we are stopped
   at.  *BSP upon return is a bpstat which points to the remaining
   breakpoints stopped at (but which is not guaranteed to be good for
   anything but further calls to bpstat_num).
   Return 0 if passed a bpstat which does not indicate any breakpoints.  */

int
bpstat_num (bpstat *bsp)
{
  struct breakpoint *b;

  if ((*bsp) == NULL)
    return 0;			/* No more breakpoint values */
  else
    {
      b = (*bsp)->breakpoint_at;
      *bsp = (*bsp)->next;
      if (b == NULL)
	return -1;		/* breakpoint that's been deleted since */
      else
	return b->number;	/* We have its number */
    }
}

/* Modify BS so that the actions will not be performed.  */

void
bpstat_clear_actions (bpstat bs)
{
  for (; bs != NULL; bs = bs->next)
    {
      bs->commands = NULL;
      if (bs->old_val != NULL)
	{
	  value_free (bs->old_val);
	  bs->old_val = NULL;
	}
    }
}

/* Stub for cleaning up our state if we error-out of a breakpoint command */
/* ARGSUSED */
static void
cleanup_executing_breakpoints (PTR ignore)
{
  executing_breakpoint_commands = 0;
}

/* Execute all the commands associated with all the breakpoints at this
   location.  Any of these commands could cause the process to proceed
   beyond this point, etc.  We look out for such changes by checking
   the global "breakpoint_proceeded" after each command.  */

void
bpstat_do_actions (bpstat *bsp)
{
  bpstat bs;
  struct cleanup *old_chain;
  struct command_line *cmd;

  /* Avoid endless recursion if a `source' command is contained
     in bs->commands.  */
  if (executing_breakpoint_commands)
    return;

  executing_breakpoint_commands = 1;
  old_chain = make_cleanup (cleanup_executing_breakpoints, 0);

top:
  bs = *bsp;

  breakpoint_proceeded = 0;
  for (; bs != NULL; bs = bs->next)
    {
      cmd = bs->commands;
      while (cmd != NULL)
	{
	  execute_control_command (cmd);

	  if (breakpoint_proceeded)
	    break;
	  else
	    cmd = cmd->next;
	}
      if (breakpoint_proceeded)
	/* The inferior is proceeded by the command; bomb out now.
	   The bpstat chain has been blown away by wait_for_inferior.
	   But since execution has stopped again, there is a new bpstat
	   to look at, so start over.  */
	goto top;
      else
	bs->commands = NULL;
    }

  executing_breakpoint_commands = 0;
  discard_cleanups (old_chain);
}

/* This is the normal print function for a bpstat.  In the future,
   much of this logic could (should?) be moved to bpstat_stop_status,
   by having it set different print_it values.

   Current scheme: When we stop, bpstat_print() is called.  It loops
   through the bpstat list of things causing this stop, calling the
   print_bp_stop_message function on each one. The behavior of the
   print_bp_stop_message function depends on the print_it field of
   bpstat. If such field so indicates, call this function here.

   Return values from this routine (ultimately used by bpstat_print()
   and normal_stop() to decide what to do): 
   PRINT_NOTHING: Means we already printed all we needed to print,
   don't print anything else.
   PRINT_SRC_ONLY: Means we printed something, and we do *not* desire
   that something to be followed by a location.
   PRINT_SCR_AND_LOC: Means we printed something, and we *do* desire
   that something to be followed by a location.
   PRINT_UNKNOWN: Means we printed nothing or we need to do some more
   analysis.  */

static enum print_stop_action
print_it_typical (bpstat bs)
{
  enum print_stop_action retval;  /* return value for exit code */
#ifdef UI_OUT
  struct cleanup *old_chain;
  struct ui_stream *stb;
  stb = ui_out_stream_new (uiout);
  old_chain = make_cleanup_ui_out_stream_delete (stb);
#endif /* UI_OUT */

  /* bs->breakpoint_at can be NULL if it was a momentary breakpoint
     which has since been deleted.  */
  if (bs->breakpoint_at == NULL)
      retval = PRINT_UNKNOWN;
  else switch (bs->breakpoint_at->type)
    {
    case bp_breakpoint:
    case bp_procedure:
    case bp_hardware_breakpoint:
      /* I think the user probably only wants to see one breakpoint
         number, not all of them.  */
      annotate_breakpoint (bs->breakpoint_at->number);
#ifdef UI_OUT
      ui_out_text (uiout, "\nBreakpoint ");
      if (ui_out_is_mi_like_p (uiout))
	ui_out_field_string (uiout, "reason", "breakpoint-hit");
      ui_out_field_int (uiout, "bkptno", bs->breakpoint_at->number);
      ui_out_text (uiout, ", ");
#else
      printf_filtered ("\nBreakpoint %d, ", bs->breakpoint_at->number);
#endif
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_catch_nomem:
    case bp_hw_catch_nomem:
      annotate_breakpoint (bs->breakpoint_at->number);
#ifdef RTC
      decode_nomem_event ();
#endif
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_rtc_event:
    case bp_hw_rtc_event:
      annotate_breakpoint (bs->breakpoint_at->number);
#ifdef RTC
      decode_rtc_event ();
#endif
      retval = PRINT_SRC_AND_LOC;
      break;
      
    case bp_rtc_compiler_event:
    case bp_hw_rtc_compiler_event:
      annotate_breakpoint (bs->breakpoint_at->number);
#ifdef RTC
      decode_rtc_compiler_event ();
#endif
      retval = PRINT_SRC_AND_LOC;
      break;
      
    case bp_rantomain_event:
      retval = PRINT_NOTHING;
      break;

    case bp_fix_event:
      annotate_breakpoint (bs->breakpoint_at->number);
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_shlib_event:
      /* Did we stop because the user set the stop_on_solib_events
	 variable?  (If so, we report this as a generic, "Stopped due
	 to shlib event" message.) */
      printf_filtered ("Stopped due to shared library event\n");
      retval = PRINT_NOTHING;
      break;

    case bp_thread_event:
      /* Not sure how we will get here. 
	 GDB should not stop for these breakpoints.  */
      printf_filtered ("Thread Event Breakpoint: gdb should not stop!\n");
      retval = PRINT_NOTHING;
      break;

    case bp_catch_load:
      annotate_catchpoint (bs->breakpoint_at->number);
      printf_filtered ("\nCatchpoint %d (", bs->breakpoint_at->number);
      printf_filtered ("loaded");
      printf_filtered (" %s), ", bs->breakpoint_at->triggered_dll_pathname);
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_catch_unload:
      annotate_catchpoint (bs->breakpoint_at->number);
      printf_filtered ("\nCatchpoint %d (", bs->breakpoint_at->number);
      printf_filtered ("unloaded");
      printf_filtered (" %s), ", bs->breakpoint_at->triggered_dll_pathname);
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_catch_fork:
      annotate_catchpoint (bs->breakpoint_at->number);
      printf_filtered ("\nCatchpoint %d (", bs->breakpoint_at->number);
      printf_filtered ("forked");
      printf_filtered (" process %d), ", 
		       bs->breakpoint_at->forked_inferior_pid);
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_catch_vfork:
      annotate_catchpoint (bs->breakpoint_at->number);
      printf_filtered ("\nCatchpoint %d (", bs->breakpoint_at->number);
      printf_filtered ("vforked");
      printf_filtered (" process %d), ", 
		       bs->breakpoint_at->forked_inferior_pid);
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_catch_exec:
      annotate_catchpoint (bs->breakpoint_at->number);
      printf_filtered ("\nCatchpoint %d (exec'd %s), ",
		       bs->breakpoint_at->number,
		       bs->breakpoint_at->exec_pathname);
      retval = PRINT_SRC_AND_LOC;
      break;

    case bp_catch_catch:
      if (current_exception_event && 
	  (CURRENT_EXCEPTION_KIND == EX_EVENT_CATCH))
	{
	  annotate_catchpoint (bs->breakpoint_at->number);
	  printf_filtered ("\nCatchpoint %d (exception caught), ", 
			   bs->breakpoint_at->number);
	  printf_filtered ("throw location ");
	  if (CURRENT_EXCEPTION_THROW_PC && CURRENT_EXCEPTION_THROW_LINE)
	    printf_filtered ("%s:%d",
			     CURRENT_EXCEPTION_THROW_FILE,
			     CURRENT_EXCEPTION_THROW_LINE);
#ifdef PC_SOLIB
	  else if (CURRENT_EXCEPTION_THROW_PC)
	    printf_filtered ("0x%llx:%s",
			     CURRENT_EXCEPTION_THROW_PC,
			     solib_and_mainfile_address (CURRENT_EXCEPTION_THROW_PC));
#endif
	
	  else
	    printf_filtered ("unknown");

	  printf_filtered (", catch location ");
	  if (CURRENT_EXCEPTION_CATCH_PC && CURRENT_EXCEPTION_CATCH_LINE)
	    printf_filtered ("%s:%d",
			     CURRENT_EXCEPTION_CATCH_FILE,
			     CURRENT_EXCEPTION_CATCH_LINE);
#ifdef PC_SOLIB
          else if (CURRENT_EXCEPTION_CATCH_PC)
            printf_filtered ("0x%llx:%s",
                             CURRENT_EXCEPTION_CATCH_PC,
                             solib_and_mainfile_address (CURRENT_EXCEPTION_CATCH_PC));
#endif
	  else
	    printf_filtered ("unknown");

	  printf_filtered ("\n");
	  /* don't bother to print location frame info */
	  retval = PRINT_SRC_ONLY;
	}
      else
	{
	  /* really throw, some other bpstat will handle it */
	  retval = PRINT_UNKNOWN;	
	}
      break;

    case bp_catch_throw:
      if (current_exception_event && 
	  (CURRENT_EXCEPTION_KIND == EX_EVENT_THROW))
	{
	  annotate_catchpoint (bs->breakpoint_at->number);
	  printf_filtered ("\nCatchpoint %d (exception thrown), ",
			   bs->breakpoint_at->number);
	  printf_filtered ("throw location ");
	  if (CURRENT_EXCEPTION_THROW_PC && CURRENT_EXCEPTION_THROW_LINE)
	    printf_filtered ("%s:%d",
			     CURRENT_EXCEPTION_THROW_FILE,
			     CURRENT_EXCEPTION_THROW_LINE);
#ifdef PC_SOLIB
          else if (CURRENT_EXCEPTION_THROW_PC)
            printf_filtered ("0x%llx:%s",
                             CURRENT_EXCEPTION_THROW_PC,
                             solib_and_mainfile_address (CURRENT_EXCEPTION_THROW_PC));
#endif
	  else
	    printf_filtered ("unknown");

	  printf_filtered (", catch location ");
	  if (CURRENT_EXCEPTION_CATCH_PC && CURRENT_EXCEPTION_CATCH_LINE)
	    printf_filtered ("%s:%d",
			     CURRENT_EXCEPTION_CATCH_FILE,
			     CURRENT_EXCEPTION_CATCH_LINE);
#ifdef PC_SOLIB
          else if (CURRENT_EXCEPTION_CATCH_PC)
            printf_filtered ("0x%llx:%s",
                             CURRENT_EXCEPTION_CATCH_PC,
                             solib_and_mainfile_address (CURRENT_EXCEPTION_CATCH_PC));
#endif
	  else
	    printf_filtered ("unknown");

	  printf_filtered ("\n");
	  /* don't bother to print location frame info */
	  retval = PRINT_SRC_ONLY; 
	}
      else
	{
	  /* really catch, some other bpstat will handle it */
	  retval = PRINT_UNKNOWN;	
	}
      break;

    case bp_watchpoint:
    case bp_hardware_watchpoint:
      if (bs->old_val != NULL)
	{
	  annotate_watchpoint (bs->breakpoint_at->number);
#ifdef UI_OUT
	  if (ui_out_is_mi_like_p (uiout) )
	    ui_out_field_string (uiout, "reason", "watchpoint-trigger");
	  mention (bs->breakpoint_at);
	  ui_out_tuple_begin (uiout, "value");
	  ui_out_text (uiout, "\nOld value = ");
	  value_print (bs->old_val, stb->stream, 0, Val_pretty_default);
	  ui_out_field_stream (uiout, "old", stb);
	  ui_out_text (uiout, "\nNew value = ");
	  value_print (bs->breakpoint_at->val, stb->stream, 0, Val_pretty_default);
	  ui_out_field_stream (uiout, "new", stb);
	  ui_out_tuple_end (uiout);
	  ui_out_text (uiout, "\n");
#else
	  mention (bs->breakpoint_at);
	  printf_filtered ("\nOld value = ");
	  value_print (bs->old_val, gdb_stdout, 0, Val_pretty_default);
	  printf_filtered ("\nNew value = ");
	  value_print (bs->breakpoint_at->val, gdb_stdout, 0,
		       Val_pretty_default);
	  printf_filtered ("\n");
#endif
	  value_free (bs->old_val);
	  bs->old_val = NULL;
	}
      /* More than one watchpoint may have been triggered.  */
      retval = PRINT_UNKNOWN;
      break;

    case bp_read_watchpoint:
#ifdef UI_OUT
      if (ui_out_is_mi_like_p (uiout))
	ui_out_field_string (uiout, "reason", "read-watchpoint-trigger");
      mention (bs->breakpoint_at);
      ui_out_tuple_begin (uiout, "value");
      ui_out_text (uiout, "\nValue = ");
      value_print (bs->breakpoint_at->val, stb->stream, 0, Val_pretty_default);
      ui_out_field_stream (uiout, "value", stb);
      ui_out_tuple_end (uiout);
      ui_out_text (uiout, "\n");
#else
      mention (bs->breakpoint_at);
      printf_filtered ("\nValue = ");
      value_print (bs->breakpoint_at->val, gdb_stdout, 0,
		   Val_pretty_default);
      printf_filtered ("\n");
#endif
      retval = PRINT_UNKNOWN;
      break;

    case bp_access_watchpoint:
#ifdef UI_OUT
      if (bs->old_val != NULL)     
	{
	  annotate_watchpoint (bs->breakpoint_at->number);
	  if (ui_out_is_mi_like_p (uiout) )
	    ui_out_field_string (uiout, "reason", "access-watchpoint-trigger");
	  mention (bs->breakpoint_at);
	  ui_out_tuple_begin (uiout, "value");
	  ui_out_text (uiout, "\nOld value = ");
	  value_print (bs->old_val, stb->stream, 0, Val_pretty_default);
	  ui_out_field_stream (uiout, "old", stb);
	  value_free (bs->old_val);
	  bs->old_val = NULL;
	  ui_out_text (uiout, "\nNew value = ");
	}
      else 
	{
	  mention (bs->breakpoint_at);
	  if (ui_out_is_mi_like_p (uiout))
	    ui_out_field_string (uiout, "reason", "access-watchpoint-trigger");
	  ui_out_tuple_begin (uiout, "value");
	  ui_out_text (uiout, "\nValue = ");
	}
      value_print (bs->breakpoint_at->val, stb->stream, 0,Val_pretty_default);
      ui_out_field_stream (uiout, "new", stb);
      ui_out_tuple_end (uiout);
      ui_out_text (uiout, "\n");
#else
      if (bs->old_val != NULL)     
	{
	  annotate_watchpoint (bs->breakpoint_at->number);
	  mention (bs->breakpoint_at);
	  printf_filtered ("\nOld value = ");
	  value_print (bs->old_val, gdb_stdout, 0, Val_pretty_default);
	  value_free (bs->old_val);
	  bs->old_val = NULL;
	  printf_filtered ("\nNew value = ");
	}
      else 
	{
	  mention (bs->breakpoint_at);
	  printf_filtered ("\nValue = ");
	}
      value_print (bs->breakpoint_at->val, gdb_stdout, 0,
		   Val_pretty_default);
      printf_filtered ("\n");
#endif
      retval = PRINT_UNKNOWN;
      break;

    case bp_finish:
    case bp_hw_finish:
#ifdef UI_OUT
      if (ui_out_is_mi_like_p (uiout))
	ui_out_field_string (uiout, "reason", "function-finished");
#endif
      retval = PRINT_UNKNOWN;
      break;

    case bp_until:
    case bp_hw_until:
#ifdef UI_OUT
      if (ui_out_is_mi_like_p (uiout))
	ui_out_field_string (uiout, "reason", "location-reached");
#endif
      retval = PRINT_UNKNOWN;
      break;

    case bp_none:
#ifdef GET_LONGJMP_TARGET
    case bp_longjmp:
    case bp_hw_longjmp:
    case bp_longjmp_resume:
    case bp_hw_longjmp_resume:
#endif /* GET_LONGJMP_TARGET */
    case bp_step_resume:
    case bp_hw_step_resume:
    case bp_through_sigtramp:
    case bp_hw_through_sigtramp:
    case bp_watchpoint_scope:
    case bp_hw_watchpoint_scope:
    case bp_call_dummy:
    default:
      retval = PRINT_UNKNOWN;
      break;
    }
#ifdef UI_OUT
    do_cleanups(old_chain);
#endif /* UI_OUT */
    return retval;
}


/* Generic routine for printing messages indicating why we
   stopped. The behavior of this function depends on the value
   'print_it' in the bpstat structure.  Under some circumstances we
   may decide not to print anything here and delegate the task to
   normal_stop(). */

static enum print_stop_action
print_bp_stop_message (bpstat bs)
{
  switch (bs->print_it)
    {
    case print_it_noop:
      /* Nothing should be printed for this bpstat entry. */
      return PRINT_UNKNOWN;

    case print_it_done:
      /* We still want to print the frame, but we already printed the
         relevant messages. */
      return PRINT_SRC_AND_LOC;

    case print_it_normal:
      /* Normal case, we handle everything in print_it_typical. */
      return print_it_typical (bs);

    default:
      internal_error ("print_bp_stop_message: unrecognized enum value");
      return PRINT_UNKNOWN;
    }
}

/* Print a message indicating what happened.  This is called from
   normal_stop().  The input to this routine is the head of the bpstat
   list - a list of the eventpoints that caused this stop.  This
   routine calls the generic print routine for printing a message
   about reasons for stopping.  This will print (for example) the
   "Breakpoint n," part of the output.  The return value of this
   routine is one of:

   PRINT_UNKNOWN: Means we printed nothing
   PRINT_SRC_AND_LOC: Means we printed something, and expect subsequent
   code to print the location. An example is 
   "Breakpoint 1, " which should be followed by
   the location.
   PRINT_SRC_ONLY: Means we printed something, but there is no need
   to also print the location part of the message.
   An example is the catch/throw messages, which
   don't require a location appended to the end.  
   PRINT_NOTHING: We have done some printing and we don't need any 
   further info to be printed.*/

enum print_stop_action
bpstat_print (bpstat bs)
{
  int val=PRINT_NOTHING;
  int ret_val = PRINT_UNKNOWN;
  /* Maybe another breakpoint in the chain caused us to stop.
     (Currently all watchpoints go on the bpstat whether hit or not.
     That probably could (should) be changed, provided care is taken
     with respect to bpstat_explains_signal).  */
    if(!bs) return ret_val;
    for (; bs; bs = bs->next)
    {
      val = print_bp_stop_message (bs);
   
   /* JAGaa80388: Go through the entire list of bs instead of returning at
      the first match. Give higher priority to PRINT_SRC_AND_LOC and then 
      to PRINT_SRC_ONLY for the value being returned by this function */

      if(ret_val != PRINT_SRC_AND_LOC 
	 && (val == PRINT_SRC_AND_LOC 
	     || val == PRINT_SRC_ONLY))
         ret_val = val;	
    }

  /* We reached the end of the chain, or we got a null BS to start
     with and nothing was printed. */
    return ((ret_val == PRINT_UNKNOWN) ? val : ret_val);
}

/* Evaluate the expression EXP and return 1 if value is zero.
   This is used inside a catch_errors to evaluate the breakpoint condition. 
   The argument is a "struct expression *" that has been cast to char * to 
   make it pass through catch_errors.  */

static int
breakpoint_cond_eval (char *exp)
{
  value_ptr mark = value_mark ();
  int i = !value_true (evaluate_expression ((struct expression *)(void *) exp));
  value_free_to_mark (mark);
  return i;
}

/* Allocate a new bpstat and chain it to the current one.  */
/*cbs is Current "bs" value */

static bpstat
bpstat_alloc (register struct breakpoint *b, bpstat cbs)
{
  bpstat bs;

  bs = (bpstat) xmalloc (sizeof (*bs));
  cbs->next = bs;
  bs->breakpoint_at = b;
  /* If the condition is false, etc., don't do the commands.  */
  bs->commands = NULL;
  bs->old_val = NULL;
  bs->print_it = print_it_normal;
  return bs;
}

/* Possible return values for watchpoint_check (this can't be an enum
   because of check_errors).  */
/* The watchpoint has been deleted.  */
#define WP_DELETED 1
/* The value has changed.  */
#define WP_VALUE_CHANGED 2
/* The value has not changed.  */
#define WP_VALUE_NOT_CHANGED 3

#define BP_TEMPFLAG 1
#define BP_HARDWAREFLAG 2
#define BP_NOMEMFLAG 4
#define BP_MATCH_ONE 8	/* Set if exactly one symbol should match. */

/* Check watchpoint condition.  */

static int
watchpoint_check (char *p)
{
  bpstat bs = (bpstat)(void *) p;
  struct breakpoint *b;
  struct frame_info *fr;
  int within_current_scope;
  boolean unw_prob = false;

  b = bs->breakpoint_at;

  if (b->exp_valid_block == NULL)
    within_current_scope = 1;
  else
    {
      /* There is no current frame at this moment.  If we're going to have
         any chance of handling watchpoints on local variables, we'll need
         the frame chain (so we can determine if we're in scope).  */
      reinit_frame_cache ();
      /* ??? RM: If we couldn't reconstruct the frame pointer, there's no
       * hope of evaluating the variable at this point. What to do?
       */
      if (((get_current_frame ())->frame) == ((CORE_ADDR)(long) -1))
	{
	  return WP_VALUE_NOT_CHANGED;
	}

      fr = find_frame_addr_in_frame_chain (b->watchpoint_frame,
                                           &unw_prob);
      within_current_scope = (fr != NULL);
      if (within_current_scope)
        {
	  /* If we end up stopping, the current frame will get selected
	     in normal_stop.  So this call to select_frame won't affect
	     the user.  */
	  select_frame (fr, -1);
        }
      else if (unw_prob)
        {
          // We didn't reach the main/pthread start function, so
          // we haven't evaluated the whole stack for the variable.
          // Return no change for the watchpoint (what else to do ?)
          return WP_VALUE_NOT_CHANGED;
        }
    }

  if (within_current_scope)
    {
      /* Ignore the heap_disabled watchpoints. */
      if ((b->enable == user_disabled) || (b->enable == heap_disabled))
	return WP_VALUE_NOT_CHANGED;
      /* We use value_{,free_to_}mark because it could be a
         *long* time before we return to the command level and
         call free_all_values.  We can't call free_all_values because
         we might be in the middle of evaluating a function call.  */

      args_for_evaluate_expression args;
      value_ptr new_val;
      args.exp = b->exp;

      value_ptr mark = value_mark ();

      donot_print_errors = 1;
      if (!catch_errors ((catch_errors_ftype *) evaluate_expression_1,
                             (char *) &args,
                              NULL, RETURN_MASK_ALL))
        {
           /* Can't evaluate expression, leave it as heap_disabled.
              Cleanup! */
            remove_breakpoint(b, mark_uninserted);
            b->exp = 0;
            b->val = NULL;
            b->enable = heap_disabled;
            donot_print_errors = 0;
            value_free_to_mark (mark);
            return WP_VALUE_NOT_CHANGED;
        }
      else
          new_val = args.valp; 

      if (VALUE_LAZY (new_val))
        {
          args.valp = new_val;

          donot_print_errors = 1;
          if (!catch_errors ((catch_errors_ftype *) value_fetch_lazy_1,
                             (char*) args.valp,
                             NULL, RETURN_MASK_ALL))
            {
              /* If we can't fetch the value for the watchpoint, the 
                 memory it's watching must have been unloaded. Instead of
                 printing an error msg, clear the WP and make it heap 
                 disabled. It will get re-enabled in the next incarnation
                 of the library (if there is one). Issuing a warning about
                 disabled breakpoints on every dlclose() is annoying; that's
                 why it was taken out. */
              remove_breakpoint(b, mark_uninserted);
              b->exp = 0;
              b->val = NULL;
              b->enable = heap_disabled;
              donot_print_errors = 0;
              value_free_to_mark (mark);
              return WP_VALUE_NOT_CHANGED;
            }
          donot_print_errors = 0;
        }
      /* value_equal does not work for arrays so replaced with memcmp
	 which should work for any type */
      if (memcmp (&b->val->aligner.contents, &new_val->aligner.contents,
		  TYPE_LENGTH (check_typedef (VALUE_TYPE (b->val)))) != 0)
	{
	  release_value (new_val);
	  value_free_to_mark (mark);
	  bs->old_val = b->val;
	  b->val = new_val;
	  /* We will stop here */
	  return WP_VALUE_CHANGED;
	}
      else
	{
	  /* Nothing changed, don't do anything.  */
	  value_free_to_mark (mark);
	  /* We won't stop here */
	  return WP_VALUE_NOT_CHANGED;
	}
    }
  else
    {
      /* This seems like the only logical thing to do because
         if we temporarily ignored the watchpoint, then when
         we reenter the block in which it is valid it contains
         garbage (in the case of a function, it may have two
         garbage values, one before and one after the prologue).
         So we can't even detect the first assignment to it and
         watch after that (since the garbage may or may not equal
         the first value assigned).  */
#ifdef UI_OUT
      if (ui_out_is_mi_like_p (uiout))
	ui_out_field_string (uiout, "reason", "watchpoint-scope");
      ui_out_text (uiout, "\nWatchpoint ");
      ui_out_field_int (uiout, "wpnum", b->number);
      ui_out_text (uiout, " deleted because the program has left the block in\n\
which its expression is valid.\n");     
#else
      printf_filtered ("\
Watchpoint %d deleted because the program has left the block in\n\
which its expression is valid.\n", b->number);
      if (b->related_breakpoint)
	b->related_breakpoint->disposition = del_at_next_stop;
#endif /* else ifdef UI_OUT */
      b->disposition = del_at_next_stop;

      return WP_DELETED;
    }
}


/* stop_for_this_bp - update the breakpoint status, bs for stopping at 
   breakpoint bkpt.
 */

void stop_for_this_bp (struct breakpoint *bkpt, bpstat bs)
{
  /* We will stop here */
  if (bkpt->disposition == disable)
    bkpt->enable = disabled;
  bs->commands = bkpt->commands;
  if (bkpt->silent)
    bs->print = 0;
  if (bs->commands &&
      (STREQ ("silent", bs->commands->line) ||
       (xdb_commands && STREQ ("Q", bs->commands->line))))
    {
      bs->commands = bs->commands->next;
      bs->print = 0;
    }
  bs->commands = copy_command_lines (bs->commands);
} /* end stop_for_this_bp */

/* Get a bpstat associated with having just stopped at address *PC
   and frame address CORE_ADDRESS.  Update *PC to point at the
   breakpoint (if we hit a breakpoint).  NOT_A_BREAKPOINT is nonzero
   if this is known to not be a real breakpoint (it could still be a
   watchpoint, though).  */

/* Determine whether we stopped at a breakpoint, etc, or whether we
   don't understand this stop.  Result is a chain of bpstat's such that:

   if we don't understand the stop, the result is a null pointer.

   if we understand why we stopped, the result is not null.

   Each element of the chain refers to a particular breakpoint or
   watchpoint at which we have stopped.  (We may have stopped for
   several reasons concurrently.)

   Each element of the chain has valid next, breakpoint_at,
   commands, FIXME??? fields.

 */

bpstat
bpstat_stop_status (CORE_ADDR *pc, int not_a_breakpoint)
{
  register struct breakpoint *b, *temp = NULL;
  CORE_ADDR bp_addr;
  /* True if we've hit a breakpoint (as opposed to a watchpoint).  */
  int real_breakpoint = 0;
  /* Root of the chain of bpstat's */
  struct bpstats root_bs[1];
  /* Pointer to the last thing in the chain currently.  */
  bpstat bs = root_bs;
  static char message1[] =
  "Error evaluating expression for watchpoint %d\n";
  char message[sizeof (message1) + 30 /* slop */ ];

  /* Get the address where the breakpoint would have been.  */
  bp_addr = *pc - DECR_PC_AFTER_BREAK;

  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    /* RM: If breakpoint has been deleted, keep going. */
    if (b->type == bp_none)
      continue;

    /* Ignore the disbaled/deferred/user_disabled
       breakpoints/watchpoints. 
    */
    if (b->enable == disabled
	|| b->enable == shlib_disabled
	|| b->enable == call_disabled
	|| b->enable == heap_disabled
	|| b->enable == user_disabled)
      continue;

    if (b->type != bp_watchpoint
	&& b->type != bp_hardware_watchpoint
	&& b->type != bp_read_watchpoint
	&& b->type != bp_access_watchpoint
	&& !is_hw_break (b->type)
	&& b->type != bp_catch_fork
	&& b->type != bp_catch_vfork
	&& b->type != bp_catch_exec
#ifdef DYNLINK_HAS_BREAK_HOOK
	&& b->type != bp_catch_load
	&& b->type != bp_catch_unload
#endif
	&& b->type != bp_catch_catch
	&& b->type != bp_catch_throw)	/* a non-watchpoint bp */
      if (!are_addrs_equal (b->address, bp_addr) ||	/* address doesn't match or */
	  (overlay_debugging &&	/* overlay doesn't match */
	   section_is_overlay (b->section) &&
	   !section_is_mapped (b->section)))
	continue;

    if (   (is_hw_break (b->type))
	&& !are_addrs_equal (b->address, (bp_addr - DECR_PC_AFTER_HW_BREAK)))
      {
#ifdef HP_IA64
	/* HW breakpoints on IPF hit for all instructions in the bundle. */
	/* Compare the bundles. */
	if (are_addrs_equal (b->address & (~0xF),
			     (bp_addr - DECR_PC_AFTER_HW_BREAK) & (~0xF)))
	  {
	    /* Don't stop if bundle matches and address doesn't match. */
    	    bs = bpstat_alloc (b, bs);	/* Alloc a bpstat to explain stop */
    	    bs->print = 0;
            bs->print_it = print_it_noop;
            bs->stop = 0;
            continue;
	  }
#endif
        continue;
      }

    if (b->type != bp_watchpoint
	&& b->type != bp_hardware_watchpoint
	&& b->type != bp_read_watchpoint
	&& b->type != bp_access_watchpoint
	&& not_a_breakpoint)
      continue;

    /* Is this a catchpoint of a load or unload?  If so, did we
       get a load or unload of the specified library?  If not,
       ignore it.
     */
    if ((b->type == bp_catch_load)
#if defined(SOLIB_HAVE_LOAD_EVENT)
	&& (!SOLIB_HAVE_LOAD_EVENT (inferior_pid)
	    || ((b->dll_pathname != NULL)
		&& (strcmp (b->dll_pathname, SOLIB_LOADED_LIBRARY_PATHNAME (inferior_pid)) != 0))))
#endif
      continue;

    if ((b->type == bp_catch_unload)
#if defined(SOLIB_HAVE_LOAD_EVENT)
	&& (!SOLIB_HAVE_UNLOAD_EVENT (inferior_pid)
	    || ((b->dll_pathname != NULL)
		&& (strcmp (b->dll_pathname, SOLIB_UNLOADED_LIBRARY_PATHNAME (inferior_pid)) != 0))))
#endif
      continue;

    if ((b->type == bp_catch_fork)
	&& !target_has_forked (inferior_pid, &b->forked_inferior_pid))
      continue;

    if ((b->type == bp_catch_vfork)
	&& !target_has_vforked (inferior_pid, &b->forked_inferior_pid))
      continue;

    if ((b->type == bp_catch_exec)
	&& !target_has_execd (inferior_pid, &b->exec_pathname))
      continue;

    /* If it is a exception catchpoint and break address doesn't match
       continue. */
    if (ep_is_exception_catchpoint (b) && !are_addrs_equal (b->address, bp_addr))
      continue;

    if (ep_is_exception_catchpoint (b) &&
	!(current_exception_event = target_get_current_exception_event ()))
      continue;


    /* JAG06703; Suresh: predicated support for H/W breakpoint case... 
       check if the QP register used in the instruction is set; if set then 
       we stop at the h/w breakpoint, else we should NOT stop and silently 
       continue... 

       OK, for the h/w breakpoints, we don't have the instruction stashed in the
       breakpoint structure, so we will read it from the target memory
    */
#ifdef HP_IA64
    if (is_hw_break (b->type))
       {
         int predicate_used;
         CORE_ADDR regvalue=0;

         /* If its one of the special instructions that modify processor state
            then we need stop and report the h/w bkpt, extract_predicate_register()
            will return p0 register (whose value is always 1) in case if its a 
            special instruction.
         */
         predicate_used = extract_predicate_register(b->address);

         /* Now that we have read the instruction and extracted the QP used, 
            lets get the current runtime value of the register */
         regvalue = read_register( predicate_used + PR0_REGNUM);

         /* If we need the old behavior, we need to stop and report this breakpoint.
            Setting the regvalue to 1 should achieve this.. */
	 if( no_predication_handling ) regvalue = 1;
         DEBUG(printf_filtered("the value of the pred register %d = %d \n", \
                   predicate_used, regvalue);)
         /* this is what we need to NOT stop and silently continue... */
         if ( regvalue == 0 )
            {
              bs = bpstat_alloc (b, bs);  
              bs->print = 0;
              bs->print_it = print_it_noop;
              bs->stop = 0; /* don't stop */
              continue;
            }
        };
#endif /* HP_IA64 */

    /* Come here if it's a watchpoint, or if the break address matches */

    bs = bpstat_alloc (b, bs);	/* Alloc a bpstat to explain stop */

    /* Watchpoints may change this, if not found to have triggered. */
    bs->stop = 1;
    bs->print = 1;

    sprintf (message, message1, b->number);
    if (b->type == bp_watchpoint || b->type == bp_hardware_watchpoint)
      {
	switch (catch_errors ((catch_errors_ftype *) watchpoint_check, (char *) bs, message,
			      RETURN_MASK_ALL))
	  {
	  case WP_DELETED:
	    /* We've already printed what needs to be printed.  */
	    bs->print_it = print_it_done;
	    /* Stop.  */
	    break;
	  case WP_VALUE_CHANGED:
	    /* Stop.  */
	    ++(b->hit_count);
	    break;
	  case WP_VALUE_NOT_CHANGED:
	    /* Don't stop.  */
	    bs->print_it = print_it_noop;
	    bs->stop = 0;
	    continue;
	  default:
	    /* Can't happen.  */
	    /* FALLTHROUGH */
	  case 0:
	    /* Error from catch_errors.  */
	    printf_filtered ("Watchpoint %d deleted.\n", b->number);
	    if (b->related_breakpoint)
	      b->related_breakpoint->disposition = del_at_next_stop;
	    b->disposition = del_at_next_stop;
	    /* We've already printed what needs to be printed.  */
	    bs->print_it = print_it_done;

	    /* Stop.  */
	    break;
	  }
      }
    else if (b->type == bp_read_watchpoint || b->type == bp_access_watchpoint)
      {
	CORE_ADDR addr;
	value_ptr v;
	int found = 0;

	addr = target_stopped_data_address ();
	if (addr == 0)
	  continue;
	for (v = b->val_chain; v; v = v->next)
	  {
             if (VALUE_LVAL (v) == lval_memory
		&& ! VALUE_LAZY (v))
	      {
		struct type *vtype = check_typedef (VALUE_TYPE (v));

		if (v == b->val_chain
		    || (TYPE_CODE (vtype) != TYPE_CODE_STRUCT
			&& TYPE_CODE (vtype) != TYPE_CODE_ARRAY))
		  {
		    CORE_ADDR vaddr;

		    vaddr = VALUE_ADDRESS (v) + VALUE_OFFSET (v);
		    /* Exact match not required.  Within range is
                       sufficient.  */
		    if (addr >= vaddr &&
			addr < vaddr + TYPE_LENGTH (VALUE_TYPE (v)))
		      found = 1;
		  }
              }
	  }
	if (found)
	  switch (catch_errors ((catch_errors_ftype *) watchpoint_check, (char *) bs, message,
				RETURN_MASK_ALL))
	    {
	    case WP_DELETED:
	      /* We've already printed what needs to be printed.  */
	      bs->print_it = print_it_done;
	      /* Stop.  */
	      break;
	    case WP_VALUE_CHANGED:
              if (b->type == bp_read_watchpoint)
		{
		  /* Don't stop: read watchpoints shouldn't fire if
		     the value has changed.  This is for targets which
		     cannot set read-only watchpoints.  */
		  bs->print_it = print_it_noop;
		  bs->stop = 0;
		  continue;
		}
	      ++(b->hit_count);
	      break;
	    case WP_VALUE_NOT_CHANGED:
	      /* Stop.  */
	      ++(b->hit_count);
	      break;
	    default:
	      /* Can't happen.  */
	    case 0:
	      /* Error from catch_errors.  */
	      printf_filtered ("Watchpoint %d deleted.\n", b->number);
	      if (b->related_breakpoint)
		b->related_breakpoint->disposition = del_at_next_stop;
	      b->disposition = del_at_next_stop;
	      /* We've already printed what needs to be printed.  */
	      bs->print_it = print_it_done;
	      break;
	    }
        else	/* found == 0 */
	  {
	    /* This is a case where some watchpoint(s) triggered,
	       but not at the address of this watchpoint (FOUND
	       was left zero).  So don't print anything for this
	       watchpoint.  */
	    bs->print_it = print_it_noop;
	    bs->stop = 0;
            continue;
	  }
      }
    else
      {
	/* By definition, an encountered breakpoint is a triggered
	   breakpoint. */
	++(b->hit_count);

	if (DECR_PC_AFTER_BREAK != 0 || must_shift_inst_regs)
	  real_breakpoint = 1;
      }

    if (b->frame && (b->frame != (get_current_frame ())->frame
#ifdef REGISTER_STACK_ENGINE_FP
		     || b->rse_fp != (get_current_frame ())->rse_fp
#endif
		    )
       )
      bs->stop = 0;
    else
      {
	int value_is_zero = 0;

	if (b->cond)
	  {
	    /* Need to select the frame, with all that implies
	       so that the conditions will have the right context.  */
	    select_frame (get_current_frame (), 0);
	    value_is_zero
	      = catch_errors ((catch_errors_ftype *) breakpoint_cond_eval, (char *) (b->cond),
			      "Error in testing breakpoint condition:\n",
			      RETURN_MASK_ALL);
	    /* FIXME-someday, should give breakpoint # */
	    free_all_values ();
	  }
	if (b->cond && value_is_zero)
	  {
	    bs->stop = 0;
             /* Don't consider this a hit.  */
	    --(b->hit_count);
	  }
	else if (b->ignore_count > 0)
	  {
	    b->ignore_count--;
	    annotate_ignore_count_change ();
	    bs->stop = 0;
	  }
	else
	  {
	    stop_for_this_bp (b, bs);
	  }
      }
    /* Print nothing for this entry if we dont stop or if we dont print.  */
    if (bs->stop == 0 || bs->print == 0)
      bs->print_it = print_it_noop;
  }

  bs->next = NULL;		/* Terminate the chain */
  bs = root_bs->next;		/* Re-grab the head of the chain */

  if (real_breakpoint && bs)
    {
      if (is_hw_break (bs->breakpoint_at->type))
	{
	  if (DECR_PC_AFTER_HW_BREAK != 0)
	    {
	      *pc = *pc - DECR_PC_AFTER_HW_BREAK;
	      write_pc (*pc);
	    }
	}
      else
	{
	  if (DECR_PC_AFTER_BREAK != 0 || must_shift_inst_regs)
	    {
	       *pc = bp_addr;
#if defined (SHIFT_INST_REGS)
	      SHIFT_INST_REGS ();
#else /* No SHIFT_INST_REGS.  */
	      write_pc (bp_addr);
#endif /* No SHIFT_INST_REGS.  */
	    }
	}
    }

  /* The value of a hardware watchpoint hasn't changed, but the
     intermediate memory locations we are watching may have.  */
  if (bs && !bs->stop &&
      (bs->breakpoint_at->type == bp_hardware_watchpoint ||
       bs->breakpoint_at->type == bp_read_watchpoint ||
       bs->breakpoint_at->type == bp_access_watchpoint))
    {
      remove_breakpoints ();
      insert_breakpoints ();
    }
  return bs;
}

/* Tell what to do about this bpstat.  */
struct bpstat_what
bpstat_what (bpstat bs)
{
  /* Classify each bpstat as one of the following.  */
  enum class
    {
      /* This bpstat element has no effect on the main_action.  */
      no_effect = 0,

      /* There was a watchpoint, stop but don't print.  */
      wp_silent,

      /* There was a watchpoint, stop and print.  */
      wp_noisy,

      /* There was a breakpoint but we're not stopping.  */
      bp_nostop,

      /* There was a breakpoint, stop but don't print.  */
      bp_silent,

      /* There was a breakpoint, stop and print.  */
      bp_noisy,

      /* We hit the longjmp breakpoint.  */
      long_jump,

      /* We hit the longjmp_resume breakpoint.  */
      long_resume,

      /* We hit the step_resume breakpoint.  */
      step_resume,

      /* We hit the through_sigtramp breakpoint.  */
      through_sig,

      /* We hit the shared library event breakpoint.  */
      shlib_event,

      /* We caught a shared library event.  */
      catch_shlib_event,

      /* This is just used to count how many enums there are.  */
      class_last
    };

  /* Here is the table which drives this routine.  So that we can
     format it pretty, we define some abbreviations for the
     enum bpstat_what codes.  */
#define kc BPSTAT_WHAT_KEEP_CHECKING
#define ss BPSTAT_WHAT_STOP_SILENT
#define sn BPSTAT_WHAT_STOP_NOISY
#define sgl BPSTAT_WHAT_SINGLE
#define slr BPSTAT_WHAT_SET_LONGJMP_RESUME
#define clr BPSTAT_WHAT_CLEAR_LONGJMP_RESUME
#define clrs BPSTAT_WHAT_CLEAR_LONGJMP_RESUME_SINGLE
#define sr BPSTAT_WHAT_STEP_RESUME
#define ts BPSTAT_WHAT_THROUGH_SIGTRAMP
#define shl BPSTAT_WHAT_CHECK_SHLIBS
#define shlr BPSTAT_WHAT_CHECK_SHLIBS_RESUME_FROM_HOOK

/* "Can't happen."  Might want to print an error message.
   abort() is not out of the question, but chances are GDB is just
   a bit confused, not unusable.  */
#define err BPSTAT_WHAT_STOP_NOISY

  /* Given an old action and a class, come up with a new action.  */
  /* One interesting property of this table is that wp_silent is the same
     as bp_silent and wp_noisy is the same as bp_noisy.  That is because
     after stopping, the check for whether to step over a breakpoint
     (BPSTAT_WHAT_SINGLE type stuff) is handled in proceed() without
     reference to how we stopped.  We retain separate wp_silent and bp_silent
     codes in case we want to change that someday.  */

  /* step_resume entries: a step resume breakpoint overrides another
     breakpoint of signal handling (see comment in wait_for_inferior
     at first IN_SIGTRAMP where we set the step_resume breakpoint).  */
  /* We handle the through_sigtramp_breakpoint the same way; having both
     one of those and a step_resume_breakpoint is probably very rare (?).  */

  static const enum bpstat_what_main_action
    table[(int) class_last][(int) BPSTAT_WHAT_LAST] =
  {
  /*                              old action */
  /*       kc    ss    sn    sgl    slr   clr    clrs   sr    ts   shl   shlr
   */
/*no_effect */ 
   {kc, ss, sn, sgl, slr, clr, clrs, sr, ts, shl, shlr},
/*wp_silent */ 
   {ss, ss, sn, ss, ss, ss, ss, sr, ts, shl, shlr},
/*wp_noisy */ 
   {sn, sn, sn, sn, sn, sn, sn, sr, ts, shl, shlr},
/*bp_nostop */ 
   {sgl, ss, sn, sgl, slr, clrs, clrs, sr, ts, shl, shlr},
/*bp_silent */ 
   {ss, ss, sn, ss, ss, ss, ss, sr, ts, shl, shlr},
/*bp_noisy */ 
   {sn, sn, sn, sn, sn, sn, sn, sr, ts, shl, shlr},
/*long_jump */ 
   {slr, ss, sn, slr, err, err, err, sr, ts, shl, shlr},
/*long_resume */ 
   {clr, ss, sn, clrs, err, err, err, sr, ts, shl, shlr},
/*step_resume */ 
   {sr, sr, sr, sr, sr, sr, sr, sr, ts, shl, shlr},
/*through_sig */ 
   {ts, ts, ts, ts, ts, ts, ts, ts, ts, shl, shlr},
/*shlib */ 
   {shl, shl, shl, shl, shl, shl, shl, shl, ts, shl, shlr},
/*catch_shlib */ 
   {shlr, shlr, shlr, shlr, shlr, shlr, shlr, shlr, ts, shlr, shlr}
  };

#undef kc
#undef ss
#undef sn
#undef sgl
#undef slr
#undef clr
#undef clrs
#undef err
#undef sr
#undef ts
#undef shl
#undef shlr
  enum bpstat_what_main_action current_action = BPSTAT_WHAT_KEEP_CHECKING;
  struct bpstat_what retval;

  retval.call_dummy = 0;
  for (; bs != NULL; bs = bs->next)
    {
      enum class bs_class = no_effect;
      if (bs->breakpoint_at == NULL)
	/* I suspect this can happen if it was a momentary breakpoint
	   which has since been deleted.  */
	continue;
      switch (bs->breakpoint_at->type)
	{
	case bp_none:
	  continue;

        case bp_rantomain_event:
	  bs_class = bp_nostop;
          break;

	case bp_rantomain_sigwait_event:
	  initiate_sigwait_signals ();
	  bs_class = bp_nostop;
          break;

	case bp_rtc_event :
	case bp_hw_rtc_event :
        /*Suresh: Oct 07: For doing Internal GC, we should not stop in gdb 
          after doing it */
#ifdef RTC
          if (((enum rtc_event)(CORE_ADDR)read_register(ARG0_REGNUM))
                                                   ==RTC_INTERNAL_GC)
          {
             decode_rtc_event();
	     bs_class = bp_nostop;
	     break;
          }
#endif
	  if (bs->stop)
	    {
	      if (bs->print)
		bs_class = bp_noisy;
	      else
		bs_class = bp_silent;
	    }
	  else
	    bs_class = bp_nostop;
	  break;
	case bp_rtc_compiler_event :
	case bp_hw_rtc_compiler_event :
	case bp_catch_nomem :
	case bp_hw_catch_nomem :
	case bp_fix_event :
	case bp_breakpoint:
	case bp_procedure:
	case bp_hardware_breakpoint:
	case bp_until:
	case bp_hw_until:
	case bp_finish:
	case bp_hw_finish:
	  if (bs->stop)
	    {
	      if (bs->print)
		bs_class = bp_noisy;
	      else
		bs_class = bp_silent;
	    }
	  else
	    bs_class = bp_nostop;
	  break;
	case bp_watchpoint:
	case bp_hardware_watchpoint:
	case bp_read_watchpoint:
	case bp_access_watchpoint:
	  if (bs->stop)
	    {
	      if (bs->print)
		bs_class = wp_noisy;
	      else
		bs_class = wp_silent;
	    }
	  else
	    /* There was a watchpoint, but we're not stopping.  This requires
	       no further action.  */
	    bs_class = no_effect;
	  break;
#ifdef GET_LONGJMP_TARGET
	case bp_longjmp:
	case bp_hw_longjmp:
	  bs_class = long_jump;
	  break;
	case bp_longjmp_resume:
	case bp_hw_longjmp_resume:
	  bs_class = long_resume;
	  break;
#endif /* GET_LONGJMP_TARGET */
	case bp_step_resume:
	case bp_hw_step_resume:
	  if (bs->stop)
	    {
	      bs_class = step_resume;
	    }
	  else
	    /* It is for the wrong frame.  */
	    bs_class = bp_nostop;
	  break;
	case bp_through_sigtramp:
	case bp_hw_through_sigtramp:
	  bs_class = through_sig;
	  break;
	case bp_watchpoint_scope:
	case bp_hw_watchpoint_scope:
	  bs_class = bp_nostop;
	  break;
	case bp_shlib_event:
	  bs_class = shlib_event;
	  break;
        case bp_thread_event:
	  bs_class = bp_nostop;
	  break;
	case bp_catch_load:
	case bp_catch_unload:
	  /* Only if this catchpoint triggered should we cause the
	     step-out-of-dld behaviour.  Otherwise, we ignore this
	     catchpoint.
	   */
	  if (bs->stop)
	    bs_class = catch_shlib_event;
	  else
	    bs_class = no_effect;
	  break;
	case bp_catch_fork:
	case bp_catch_vfork:
	case bp_catch_exec:
	  if (bs->stop)
	    {
	      if (bs->print)
		bs_class = bp_noisy;
	      else
		bs_class = bp_silent;
	    }
	  else
	    /* There was a catchpoint, but we're not stopping.  This requires
	       no further action.  */
	    bs_class = no_effect;
	  break;
	case bp_catch_catch:
	  if (!bs->stop || CURRENT_EXCEPTION_KIND != EX_EVENT_CATCH)
	    bs_class = bp_nostop;
	  else if (bs->stop)
	    bs_class = bs->print ? bp_noisy : bp_silent;
	  break;
	case bp_catch_throw:
	  if (!bs->stop || CURRENT_EXCEPTION_KIND != EX_EVENT_THROW)
	    bs_class = bp_nostop;
	  else if (bs->stop)
	    bs_class = bs->print ? bp_noisy : bp_silent;
	  break;
	case bp_catch_ex_internal:
	  bs_class = bp_nostop;
          break;
	case bp_mmap:
	  bs_class = bp_nostop;
	  break;
	case bp_call_dummy:
	  /* Make sure the action is stop (silent or noisy), so infrun.c
	     pops the dummy frame.  */
	  bs_class = bp_silent;
	  retval.call_dummy = 1;
	  break;
        case bp_sbrk_event:
	  /* Just continue. insert_breakpoints() during continue will take
	     care of re-evaluating the heap_disabled watchpoints. */
	  bs_class = bp_nostop;
          break;
        case bp_thread_tracing_event:
	  bs_class = bp_nostop;
          break;

	default:
	  continue;
	}
      current_action = table[(int) bs_class][(int) current_action];
    }
  retval.main_action = current_action;
  return retval;
}

/* Nonzero if we should step constantly (e.g. watchpoints on machines
   without hardware support).  This isn't related to a specific bpstat,
   just to things like whether watchpoints are set.  */

int
bpstat_should_step ()
{
  struct breakpoint *b;
  ALL_BREAKPOINTS (b)
    if (b->enable == enabled && b->type == bp_watchpoint)
    return 1;
  return 0;
}

/* Nonzero if there are enabled hardware watchpoints. */
int
bpstat_have_active_hw_watchpoints ()
{
  struct breakpoint *b;
  ALL_BREAKPOINTS (b)
    if ((b->enable == enabled) &&
	(b->inserted) &&
	((b->type == bp_hardware_watchpoint) ||
	 (b->type == bp_read_watchpoint) ||
	 (b->type == bp_access_watchpoint)))
    return 1;
  return 0;
}


/* Given a bpstat that records zero or more triggered eventpoints, this
   function returns another bpstat which contains only the catchpoints
   on that first list, if any.
 */
void
bpstat_get_triggered_catchpoints (bpstat ep_list, bpstat *cp_list)
{
  struct bpstats root_bs[1];
  bpstat bs = root_bs;
  struct breakpoint *ep;
  char *dll_pathname;

  bpstat_clear (cp_list);
  root_bs->next = NULL;

  for (; ep_list != NULL; ep_list = ep_list->next)
    {
      /* Is this eventpoint a catchpoint?  If not, ignore it. */
      ep = ep_list->breakpoint_at;
      if (ep == NULL)
	break;
      if ((ep->type != bp_catch_load) &&
	  (ep->type != bp_catch_unload) &&
	  (ep->type != bp_catch_catch) &&
	  (ep->type != bp_catch_throw))		/* add fork/vfork here ?  */
	continue;

      /* Yes; add it to the list. */
      bs = bpstat_alloc (ep, bs);
      *bs = *ep_list;
      bs->next = NULL;
      bs = root_bs->next;
#if defined(SOLIB_ADD)
      /* Also, for each triggered catchpoint, tag it with the name of
         the library that caused this trigger.  (We copy the name now,
         because it's only guaranteed to be available NOW, when the
         catchpoint triggers.  Clients who may wish to know the name
         later must get it from the catchpoint itself.)
       */
      if (ep->triggered_dll_pathname != NULL)
	free (ep->triggered_dll_pathname);
      if (ep->type == bp_catch_load)
	dll_pathname = SOLIB_LOADED_LIBRARY_PATHNAME (inferior_pid);
      else
	dll_pathname = SOLIB_UNLOADED_LIBRARY_PATHNAME (inferior_pid);
#else
      dll_pathname = NULL;
#endif
      if (dll_pathname)
	{
	  ep->triggered_dll_pathname = (char *) 
	    xmalloc (strlen (dll_pathname) + 1);
	  strcpy (ep->triggered_dll_pathname, dll_pathname);
	}
      else
	ep->triggered_dll_pathname = NULL;
    }

  *cp_list = bs;
}

#ifdef IMPLICIT_SHLIB_EVENT_NOTIFICATION_LACKING
/* 
 * caught_active_load : 
 * 
 * srikanth, 980830, CLLbs14756 : returns a boolean indicating whether 
 * any of the libraries loaded by dld at this time is the subject of an 
 * active catch load;  An informative message regarding the triggered
 * catch load points is also produced.
 *
 */

boolean
caught_active_load ()
{

  boolean caught = false;
  register struct breakpoint *b;
  extern boolean som_solib_isloaded (char *);
  boolean named_catch = false;
  struct breakpoint * caught_bp = 0; /* Initialize to fix compiler warning.*/

  ALL_BREAKPOINTS (b)
  {
    if (b->type != bp_catch_load)
      continue;

    if (b->enable == disabled
	|| b->enable == shlib_disabled
	|| b->enable == call_disabled
	|| ! is_stop_for_bp (b)
       )
      continue;

    if (b->dll_pathname == NULL)	/* catch anything */
      {
	caught = true;
	caught_bp = b;
      }
    else if (som_solib_isloaded (b->dll_pathname))	/* catch this */
      {
	caught = true;
	caught_bp = b;
        named_catch = true;
	annotate_catchpoint (b->number);
	printf_filtered ("\nCatchpoint %d (", b->number);
	printf_filtered ("loaded");
	printf_filtered (" %s)  ", b->dll_pathname);
      }
  }

  if (caught)
    {
      if (!named_catch)
	{
	  /* 
	   * This is a case of catch load <any library>
	   * Let us simply report all libraries that have been
	   * loaded at this point. Mimic the info share command 
	   */
	  printf_filtered ("\nCaught shared library load\n");
	  som_sharedlibrary_info_command ("", 0);	/* ignores its args */
	}
      printf_filtered ("\n");
      stop_for_this_bp ( caught_bp, stop_bpstat);
    }

  return caught;
}

/* JAGaa80453/JAGaf03519 - To catch the unloaded shared libraries */ 

/* caught_active_unload : 
   Returns a boolean indicating whether any of the libraries loaded by dld
   at this time is the subject of an active catch unload; 
   An informative message regarding the triggered
   catch unload points is also produced. */

boolean
caught_active_unload ()
{

  boolean caught = false;
  register struct breakpoint *b;
  extern boolean som_solib_isloaded (char *);
  boolean named_catch = false; 
  struct breakpoint * caught_bp;

  ALL_BREAKPOINTS (b)
    {
      if (b->type != bp_catch_unload) 
        continue;

      if (b->enable == disabled ||
          b->enable == shlib_disabled ||
          b->enable == call_disabled)
        continue;

      if (b->dll_pathname == NULL)   /* catch anything */
        {
         caught = true;
	 caught_bp = b;
         }
      else if (som_solib_isloaded (b->dll_pathname))  /* catch this */
        {
          caught = true;
          named_catch = false;
          annotate_catchpoint (b->number);
          printf_filtered ("\nCatchpoint %d (", b->number);
	  printf_filtered ("unloaded");
          printf_filtered (" %s)  ", b->dll_pathname); 
        }
    }

    if (caught)
      { 
        if (!named_catch)
          {
            /* 
             * This is a case of catch load <any library>
             * Let us simply report all libraries that have been
             * loaded at this point. Mimic the info share command 
             */
            printf_filtered ("\nCaught shared library unload\n");
            som_sharedlibrary_info_command("", 0); /* ignores its args */
          }
        printf_filtered("\n");
      }

    return caught;
} 

#endif

/* Print B to gdb_stdout. */
static void
print_one_breakpoint (struct breakpoint *b,
		      CORE_ADDR *last_addr)
{
  register struct command_line *l;
  register struct symbol *sym = NULL;
  struct ep_type_description
    {
      enum bptype type;
      char *description;
    };
  static struct ep_type_description bptypes[] =
  {
    {bp_none, "?deleted?"},
    {bp_breakpoint, "breakpoint"},
    {bp_hardware_breakpoint, "hw breakpoint"},
    {bp_until, "until"},
    {bp_hw_until, "hw until"},
    {bp_finish, "finish"},
    {bp_finish, "hw finish"},
    {bp_watchpoint, "watchpoint"},
    {bp_hardware_watchpoint, "hw watchpoint"},
    {bp_read_watchpoint, "read watchpoint"},
    {bp_access_watchpoint, "acc watchpoint"},
    {bp_longjmp, "longjmp"},
    {bp_hw_longjmp, "hw longjmp"},
    {bp_longjmp_resume, "longjmp resume"},
    {bp_hw_longjmp_resume, "hw longjmp resume"},
    {bp_step_resume, "step resume"},
    {bp_hw_step_resume, "hardware step resume"},
    {bp_through_sigtramp, "sigtramp"},
    {bp_hw_through_sigtramp, "hw sigtramp"},
    {bp_watchpoint_scope, "watchpoint scope"},
    {bp_hw_watchpoint_scope, "hw watchpoint scope"},
    {bp_call_dummy, "call dummy"},
    {bp_shlib_event, "shlib events"},
    {bp_sbrk_event, "sbrk event"},
    {bp_rtc_event, "RTC events"},
    {bp_hw_rtc_event, "hw RTC events"},
    {bp_rantomain_event, "Ready for profiling event"},
    {bp_rantomain_sigwait_event, "Ready for sigwait event"},
    {bp_fix_event, "fix event"},
    {bp_thread_event, "thread events"},
    {bp_catch_load, "catch load"},
    {bp_catch_unload, "catch unload"},
    {bp_catch_fork, "catch fork"},
    {bp_catch_vfork, "catch vfork"},
    {bp_catch_exec, "catch exec"},
    {bp_catch_catch, "catch catch"},
    {bp_catch_throw, "catch throw"},
    {bp_catch_ex_internal, "catch exception internal"},
    {bp_mmap, "mmap"},
    {bp_catch_nomem, "catch nomem"},
    {bp_hw_catch_nomem, "hw catch nomem"},
    {bp_rtc_compiler_event, "RTC compiler generated events"},
    {bp_hw_rtc_compiler_event, "hw RTC compiler generated events"},
    {bp_procedure, "breakpoint"},
    {bp_thread_tracing_event, "thread tracing event"}
  };
 
  /* Must keep this in sync with enum bpdisp in breakpoint.h */ 
  static char *bpdisps[] =
  {"del", "dstp", "dis", "keep"};

  /* JAGaf82662 - must keep in sync with enum enable in breakpoint.h */
  /* JAGaf70508 - Added the status of user_disabled field */
  static char bpenables[] = "nynnnyn";
  char wrap_indent[80];
#ifdef UI_OUT
  struct ui_stream *stb = ui_out_stream_new (uiout);
  struct cleanup *old_chain = make_cleanup_ui_out_stream_delete (stb);
#endif

  annotate_record ();
#ifdef UI_OUT
  ui_out_tuple_begin (uiout, "bkpt");
#endif

  /* 1 */
  annotate_field (0);
#ifdef UI_OUT
  ui_out_field_int (uiout, "number", b->number);
#else
  printf_filtered ("%-3d ", b->number);
#endif

  /* 2 */
  annotate_field (1);
  if (((int) b->type > (sizeof (bptypes) / sizeof (bptypes[0])))
      || ((int) b->type != bptypes[(int) b->type].type))
    internal_error ("bptypes table does not describe type #%d.",
		    (int) b->type);
#ifdef UI_OUT
  ui_out_field_string (uiout, "type", bptypes[(int) b->type].description);
#else
  printf_filtered ("%-14s ", bptypes[(int) b->type].description);
#endif

  /* 3 */
  annotate_field (2);
#ifdef UI_OUT
  ui_out_field_string (uiout, "disp", bpdisps[(int) b->disposition]);
#else
  printf_filtered ("%-4s ", bpdisps[(int) b->disposition]);
#endif

  /* 4 */
  annotate_field (3);
#ifdef UI_OUT
  ui_out_field_fmt (uiout, "enabled", "%c", bpenables[(int) b->enable]);
  ui_out_spaces (uiout, 2);
#else
  printf_filtered ("%-3c ", bpenables[(int) b->enable]);
#endif
  
  /* 5 and 6 */
  strcpy (wrap_indent, "                           ");
  if (addressprint)
    strcat (wrap_indent, "           ");
  switch (b->type)
    {
    case bp_none:
      internal_error ("print_one_breakpoint: bp_none encountered\n");
      break;

    case bp_watchpoint:
    case bp_hardware_watchpoint:
    case bp_read_watchpoint:
    case bp_access_watchpoint:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
#ifdef UI_OUT
      if (addressprint)
        {
          if (b->enable == heap_disabled)
            {
              annotate_field (4);
              ui_out_text (uiout, "(deferred) ");
            }
          else
            {
              ui_out_field_skip (uiout, "addr");
            }
        }
      annotate_field (5);
      if (b->exp)
          print_expression (b->exp, stb->stream);
      else
          fputs_filtered(b->exp_string, stb->stream);

      ui_out_field_stream (uiout, "what", stb);
#else
      if (b->enable == heap_disabled)
        {
          annotate_field (4);
          printf_filtered ("(deferred) ");
        }
      annotate_field (5);
      if (b->exp)
          print_expression (b->exp, gdb_stdout);
      else
          printf_filtered ("%s", b->exp_string);
#endif
      break;
      
    case bp_catch_load:
    case bp_catch_unload:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
#ifdef UI_OUT
      if (addressprint)
	ui_out_field_skip (uiout, "addr");
      annotate_field (5);
      if (b->dll_pathname == NULL)
	{
	  ui_out_field_string (uiout, "what", "<any library>");
	  ui_out_spaces (uiout, 1);
	}
      else
	{
	  ui_out_text (uiout, "library \"");
	  ui_out_field_string (uiout, "what", b->dll_pathname);
	  ui_out_text (uiout, "\" ");
	}
#else
      annotate_field (5);
      if (b->dll_pathname == NULL)
	printf_filtered ("<any library> ");
      else
	printf_filtered ("library \"%s\" ", b->dll_pathname);
#endif
      break;
      
    case bp_catch_nomem:
    case bp_hw_catch_nomem:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
      annotate_field (5);
      break;
    case bp_catch_fork:
    case bp_catch_vfork:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
#ifdef UI_OUT
      if (addressprint)
	ui_out_field_skip (uiout, "addr");
      annotate_field (5);
      if (b->forked_inferior_pid != 0)
	{
	  ui_out_text (uiout, "process ");
	  ui_out_field_int (uiout, "what", b->forked_inferior_pid);
	  ui_out_spaces (uiout, 1);
	}
#else
      annotate_field (5);
      if (b->forked_inferior_pid != 0)
	printf_filtered ("process %d ", b->forked_inferior_pid);
      break;
#endif
      
    case bp_catch_exec:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
#ifdef UI_OUT
      if (addressprint)
	ui_out_field_skip (uiout, "addr");
      annotate_field (5);
      if (b->exec_pathname != NULL)
	{
	  ui_out_text (uiout, "program \"");
	  ui_out_field_string (uiout, "what", b->exec_pathname);
	  ui_out_text (uiout, "\" ");
	}
#else
      annotate_field (5);
      if (b->exec_pathname != NULL)
	printf_filtered ("program \"%s\" ", b->exec_pathname);
#endif
      break;

    case bp_catch_catch:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
#ifdef UI_OUT
      if (addressprint)
	ui_out_field_skip (uiout, "addr");
      annotate_field (5);
      ui_out_field_string (uiout, "what", "exception catch");
      ui_out_spaces (uiout, 1);
#else
      annotate_field (5);
      printf_filtered ("exception catch ");
#endif
      break;

    case bp_catch_throw:
      /* Field 4, the address, is omitted (which makes the columns
	 not line up too nicely with the headers, but the effect
	 is relatively readable).  */
#ifdef UI_OUT
      if (addressprint)
	ui_out_field_skip (uiout, "addr");
      annotate_field (5);
      ui_out_field_string (uiout, "what", "exception throw");
      ui_out_spaces (uiout, 1);
#else
      annotate_field (5);
      printf_filtered ("exception throw ");
#endif
      break;
      
    case bp_breakpoint:
    case bp_sbrk_event:
    case bp_thread_tracing_event:
    case bp_procedure:
    case bp_hardware_breakpoint:
    case bp_until:
    case bp_hw_until:
    case bp_finish:
    case bp_hw_finish:
#ifdef GET_LONGJMP_TARGET
    case bp_longjmp:
    case bp_hw_longjmp:
    case bp_longjmp_resume:
    case bp_hw_longjmp_resume:
#endif /* GET_LONGJMP_TARGET */
    case bp_step_resume:
    case bp_hw_step_resume:
    case bp_through_sigtramp:
    case bp_hw_through_sigtramp:
    case bp_watchpoint_scope:
    case bp_hw_watchpoint_scope:
    case bp_call_dummy:
    case bp_shlib_event:
    case bp_rantomain_event:
    case bp_rantomain_sigwait_event:
    case bp_rtc_event:
    case bp_hw_rtc_event:
    case bp_rtc_compiler_event:
    case bp_hw_rtc_compiler_event:
    case bp_fix_event:
    case bp_thread_event:
#ifdef UI_OUT
      if (addressprint)
	{
	  annotate_field (4);
	  if (!b->address && b->enable == shlib_disabled)
	  	ui_out_text (uiout, "(deferred) ");
	  else
	  	ui_out_field_core_addr (uiout, "addr",
			 SWIZZLE (b->address));
	}
      annotate_field (5);
      *last_addr = b->address;
      if (b->source_file)
	{
          /* If you try to find innermost lexical block of a pc which belongs
             to inline function we end up recognizing the non inline function
             which called this inline function. So if we see it is an inline
             function we will not find the block containing this pc. Instead 
             use the "addr_string" value in breakpoints structure 
             to display the name.
          */   
          char *func_name = NULL;
          if (b->inline_idx)
            func_name = b->addr_string;
          else 
	    sym = find_pc_sect_function (b->address, b->section);
	  if (sym || b->inline_idx)
	    {
	      ui_out_text (uiout, "in ");
              if (sym)
	        ui_out_field_string (uiout, "func",
				     SYMBOL_SOURCE_NAME (sym));
              else if (b->inline_idx)
                ui_out_field_string (uiout, "func",
                                     func_name);
	      ui_out_wrap_hint (uiout, wrap_indent);
	      ui_out_text (uiout, " at ");
	    }
	  ui_out_field_string (uiout, "file", b->source_file);
	  ui_out_text (uiout, ":");
	  ui_out_field_int (uiout, "line", b->line_number);

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
          if (b->inline_idx)
            ui_out_text (uiout," (inlined)");
#endif
	}
      else if (!b->address && b->enable == shlib_disabled)
        {
	  ui_out_text (uiout, "in ");
	  ui_out_field_string (uiout, "addr_string", b->addr_string);
	}
      else
	{
	  print_address_symbolic (b->address, stb->stream, demangle, "");
	  ui_out_field_stream (uiout, "at", stb);
	}
#else
      if (addressprint)
	{
	  annotate_field (4);
	  if (!b->address && b->enable == shlib_disabled)
	    printf_filtered ("(deferred) ");
	  else
	    printf_filtered ("%s ", longest_local_hex_string_custom((LONGEST)SWIZZLE(b->address), "08l"));
	}
      annotate_field (5);
      *last_addr = b->address;
      if (b->source_file)
	{
	  sym = find_pc_sect_function (b->address, b->section);
	  if (sym)
	    {
	      fputs_filtered ("in ", gdb_stdout);
	      fputs_filtered (SYMBOL_SOURCE_NAME (sym), gdb_stdout);
	      wrap_here (wrap_indent);
	      fputs_filtered (" at ", gdb_stdout);
	    }
	  fputs_filtered (b->source_file, gdb_stdout);
	  printf_filtered (":%d", b->line_number);
	}
      else if (!b->address && b->enable == shlib_disabled)
	/* srikanth, JAGab25505, print name for deferred bp's */
        {
	  fputs_filtered ("in ", gdb_stdout);
	  fputs_filtered (b->addr_string, gdb_stdout);
	}
      else
        {
          char *so_name = NULL;
	  print_address_symbolic (b->address, gdb_stdout, demangle, " ");
#ifdef PC_SOLIB
	  /* Print the library name, when the source information is not
	     available. */
          if (!hp_ia64_testsuite && !hp_wdb_testsuite)
            if ((so_name = solib_and_mainfile_address (b->address)) != NULL)
	      {
	        wrap_here (wrap_indent);
                printf_filtered (" from %s", so_name);
	      }
#endif /* PC_SOLIB */
	}
#endif
      break;
    default:
      break;
    }
  
  if (b->thread != -1)
    {
#ifdef UI_OUT
      /* FIXME: This seems to be redundant and lost here; see the
	 "stop only in" line a little further down. */
      ui_out_text (uiout, " thread ");
      ui_out_field_int (uiout, "thread", b->thread);
#else
#ifdef HP_MXN
      if (is_mxn)
        printf_filtered (" user thread %d", b->thread);
      else
#endif
	printf_filtered (" thread %d", b->thread);
#endif
    }
  
#ifdef UI_OUT
  ui_out_text (uiout, "\n");
#else
  printf_filtered ("\n");
#endif
  
  if (b->frame)
    {
      annotate_field (6);
#ifdef UI_OUT
      ui_out_text (uiout, "\tstop only in stack frame at ");
      if (ui_out_is_mi_like_p (uiout))
        ui_out_field_core_addr (uiout, "frame", b->frame);
      else
        print_address_numeric (b->frame, 1, gdb_stdout);
      ui_out_text (uiout, "\n");
#else
      printf_filtered ("\tstop only in stack frame at ");
      print_address_numeric (b->frame, 1, gdb_stdout);
      printf_filtered ("\n");
#endif
    }
  
  if (b->cond)
    {
      annotate_field (7);
#ifdef UI_OUT
      ui_out_text (uiout, "\tstop only if ");
      /* JAGaf35513 */
      if (b->enable == enabled)
      	print_expression (b->cond, stb->stream);
      else
	ui_out_text (uiout, b->cond_string);
      ui_out_field_stream (uiout, "cond", stb);
      ui_out_text (uiout, "\n");
#else
      printf_filtered ("\tstop only if ");
      /* JAGaf35513 */
      if (b->enable == enabled)
	print_expression (b->cond, gdb_stdout);
      else
	printf_filtered ("%s", b->cond_string);
      printf_filtered ("\n");
#endif
    }
  
  if (b->thread != -1)
    {
      /* FIXME should make an annotation for this */
#ifdef HP_MXN
      if (is_mxn)
#ifdef UI_OUT
        {
      	  ui_out_text (uiout, "\tstop only in user thread ");
          ui_out_field_int (uiout, "thread", b->thread);
          ui_out_text (uiout, "\n");
        }
#else
        printf_filtered ("\tstop only in user thread %d\n", b->thread);
#endif //UI_OUT		
      else
#endif //HP_MXN
#ifdef UI_OUT
        {
      	  ui_out_text (uiout, "\tstop only in thread ");
          ui_out_field_int (uiout, "thread", b->thread);
          ui_out_text (uiout, "\n");
        }
#else
	printf_filtered ("\tstop only in thread %d\n", b->thread);
#endif
    }
  
  if (show_breakpoint_hit_counts && b->hit_count)
    {
      /* FIXME should make an annotation for this */
#ifdef UI_OUT
      if (ep_is_catchpoint (b))
	ui_out_text (uiout, "\tcatchpoint");
      else
	ui_out_text (uiout, "\tbreakpoint");
      ui_out_text (uiout, " already hit ");
      ui_out_field_int (uiout, "times", b->hit_count);
      if (b->hit_count == 1)
	ui_out_text (uiout, " time\n");
      else
	ui_out_text (uiout, " times\n");
#else
      if (ep_is_catchpoint (b))
	printf_filtered ("\tcatchpoint");
      else
	printf_filtered ("\tbreakpoint");
      printf_filtered (" already hit %d time%s\n",
		       b->hit_count, (b->hit_count == 1 ? "" : "s"));
#endif
    }
  
#ifdef UI_OUT
  /* Output the count also if it is zero, but only if this is
     mi. FIXME: Should have a better test for this. */
  if (ui_out_is_mi_like_p (uiout))
    if (show_breakpoint_hit_counts && b->hit_count == 0)
      ui_out_field_int (uiout, "times", b->hit_count);
#endif

  if (b->ignore_count)
    {
      annotate_field (8);
#ifdef UI_OUT
      ui_out_text (uiout, "\tignore next ");
      ui_out_field_int (uiout, "ignore", b->ignore_count);
      ui_out_text (uiout, " hits\n");
#else
      printf_filtered ("\tignore next %d hits\n", b->ignore_count);
#endif
    }
  
  if ((l = b->commands))
    {
      annotate_field (9);
#ifdef UI_OUT
      ui_out_tuple_begin (uiout, "script");
      print_command_lines (uiout, l, 4);
      ui_out_tuple_end (uiout);
#else
      while (l)
	{
	  print_command_line (l, 4, gdb_stdout);
	  l = l->next;
	}
#endif
    }
#ifdef UI_OUT
  ui_out_tuple_end (uiout);
  do_cleanups (old_chain);
#endif
}

struct captured_breakpoint_query_args
  {
    int bnum;
  };

static int
do_captured_breakpoint_query (void *data)
{
  struct captured_breakpoint_query_args *args = data;
  register struct breakpoint *b;
  CORE_ADDR dummy_addr = 0;
  ALL_BREAKPOINTS (b)
    {
      if (args->bnum == b->number)
	{
	  print_one_breakpoint (b, &dummy_addr);
	  return GDB_RC_OK;
	}
    }
  return GDB_RC_NONE;
}

enum gdb_rc
gdb_breakpoint_query (/* output object, */ int bnum)
{
  struct captured_breakpoint_query_args args;
  args.bnum = bnum;
  /* For the moment we don't trust print_one_breakpoint() to not throw
     an error. */
  return catch_errors (do_captured_breakpoint_query, &args,
		       NULL, RETURN_MASK_ALL);
}

/* Print information about procedure breakpoints 
   Dinesh, 4/24/02 */

static void
procedure_bp (int bnum)
{
  char tmpbuf[128];
  struct break_commands_list *rbp_list;
  struct command_line *l;
  rbp_list = break_commands_chain;
  if (bnum == -1 && rbp_list && annotation_level <= 1 )
    {
      printf_filtered ("\nProcedure breakpoint(s)\n");
      printf_filtered ("Num ");
      printf_filtered ("Type           ");
      sprintf(tmpbuf,"breakpoint");
      while (rbp_list)
       {
         printf_filtered ("\n%-3s ", rbp_list->break_commands);
         printf_filtered ("%-14s\n",tmpbuf);
         if ((l = rbp_list->commands_list))
          {
            while (l)
              {
#ifdef UI_OUT			  
                print_command_lines (uiout, l, 4);
#else
                print_command_line (l, 4, gdb_stdout);
#endif	
                l = l->next;
              }
          }

         rbp_list = rbp_list->next;
       } 
    
   }

}

/* Print information on breakpoint number BNUM, or -1 if all.
   If WATCHPOINTS is zero, process only breakpoints; if WATCHPOINTS
   is nonzero, process only watchpoints.  */

typedef struct
{
  enum bptype type;
  char *description;
}
ep_type_description_t;

static void
breakpoint_1 (int bnum, int allflag)
{
  register struct breakpoint *b;
  CORE_ADDR last_addr = (CORE_ADDR)(long) - 1;
  int nr_printable_breakpoints = 0;
  int found_a_breakpoint = 0;

#ifdef UI_OUT
  ALL_BREAKPOINTS (b)
    if (bnum == -1
	  || bnum == b->number)
	{
/*  We only print out user settable breakpoints unless the allflag is set. */
      if (!allflag
	  && b->type != bp_breakpoint
	  && b->type != bp_catch_load
	  && b->type != bp_catch_unload
	  && b->type != bp_catch_nomem
	  && b->type != bp_hw_catch_nomem
	  && b->type != bp_catch_fork
	  && b->type != bp_catch_vfork
	  && b->type != bp_catch_exec
	  && b->type != bp_catch_catch
	  && b->type != bp_catch_throw
	  && b->type != bp_hardware_breakpoint
	  && b->type != bp_watchpoint
	  && b->type != bp_read_watchpoint
	  && b->type != bp_access_watchpoint
	  && b->type != bp_hardware_watchpoint)
	    continue;
	  else
	   nr_printable_breakpoints++;
	  }
      /*In MI mode the header is printed even when there are no breakpoints or watchpoints.*/	
	  if (nr_printable_breakpoints > 0 || ui_out_is_mi_like_p (uiout))
	    {
	  	  if (addressprint)
	        ui_out_table_begin (uiout, 6, nr_printable_breakpoints, "BreakpointTable");
	      else
	        ui_out_table_begin (uiout, 5, nr_printable_breakpoints, "BreakpointTable");
	  	  annotate_breakpoints_headers ();
	      annotate_field (0);
	      ui_out_table_header (uiout, 3, ui_left, "number", "Num");	/* 1 */
	      annotate_field (1);
	      ui_out_table_header (uiout, 14, ui_left, "type", "Type");	/* 2 */
	      annotate_field (2);
	      ui_out_table_header (uiout, 4, ui_left, "disp", "Disp");	/* 3 */
	      annotate_field (3);
	      ui_out_table_header (uiout, 3, ui_left, "enabled", "Enb");	/* 4 */
	      if (addressprint)
	        {
	            annotate_field (4);
		        ui_out_table_header (uiout, 10, ui_left, "addr", "Address");	/* 5 */
	        }
	      annotate_field (5);
	      ui_out_table_header (uiout, 40, ui_noalign, "what", "What");	/* 6 */
	      ui_out_table_body (uiout);
	      annotate_breakpoints_table ();
		}
#endif /* UI_OUT */

  ALL_BREAKPOINTS (b)
    if (bnum == -1
	|| bnum == b->number)
    {
/*  We only print out user settable breakpoints unless the allflag is set. */
      if (!allflag
	  && b->type != bp_breakpoint
	  && b->type != bp_catch_load
	  && b->type != bp_catch_unload
	  && b->type != bp_catch_nomem
	  && b->type != bp_hw_catch_nomem
	  && b->type != bp_catch_fork
	  && b->type != bp_catch_vfork
	  && b->type != bp_catch_exec
	  && b->type != bp_catch_catch
	  && b->type != bp_catch_throw
	  && b->type != bp_hardware_breakpoint
	  && b->type != bp_watchpoint
	  && b->type != bp_read_watchpoint
	  && b->type != bp_access_watchpoint
	  && b->type != bp_hardware_watchpoint)
	continue;

      if (!found_a_breakpoint++)
	{

#ifndef UI_OUT
	  annotate_breakpoints_table ();
	  annotate_field (0);
	  printf_filtered ("Num ");
	  annotate_field (1);
	  printf_filtered ("Type           ");
	  annotate_field (2);
	  printf_filtered ("Disp ");
	  annotate_field (3);
	  printf_filtered ("Enb ");
	  if (addressprint)
	    {
	      annotate_field (4);
	      printf_filtered ("Address    ");
	    }
	  annotate_field (5);
	  printf_filtered ("What\n");
#endif /* UI_OUT */
	}

      print_one_breakpoint (b, &last_addr);

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
      /* When breakpoints are placed on inline functions, all inline instances are 
         represented using related_breakpoints. If we are displaying a 
         "single-breakpoint" behavior, then skip printing of these related breakpoints.
      */ 
      if (inline_debugging == BP_ALL && b->related_breakpoint)
        while (b->related_breakpoint)
          b = b->related_breakpoint;
#endif
    }  

#ifdef UI_OUT
	if (nr_printable_breakpoints > 0
			|| ui_out_is_mi_like_p (uiout) )
	  ui_out_table_end (uiout);
#endif /* UI_OUT */

if (!found_a_breakpoint)
    {
#ifdef UI_OUT
      if (bnum == -1)
	ui_out_message (uiout, 0, "No breakpoints or watchpoints.\n");
      else
	ui_out_message (uiout, 0, "No breakpoint or watchpoint number %d.\n",
			bnum);
#else
      if (bnum == -1)
	printf_filtered ("No breakpoints or watchpoints.\n");
      else
	printf_filtered ("No breakpoint or watchpoint number %d.\n", bnum);
#endif /* UI_OUT */
    }
  else
    /* Compare against (CORE_ADDR)-1 in case some compiler decides
       that a comparison of an unsigned with -1 is always false.  */
  if (last_addr != (CORE_ADDR)(long) - 1)
    set_next_address (last_addr);


  /* FIXME? Should this be moved up so that it is only called when
     there have been breakpoints? */
  annotate_breakpoints_table_end ();

}

/* ARGSUSED */
static void
breakpoints_info (char *bnum_exp, int from_tty)
{
  int bnum = -1;

  if (bnum_exp)
    bnum = (int) parse_and_eval_address (bnum_exp);

  breakpoint_1 (bnum, 0);
  procedure_bp (bnum);
}

#if MAINTENANCE_CMDS

/* ARGSUSED */
void
maintenance_info_breakpoints (char *bnum_exp, int from_tty)
{
  int bnum = -1;

  if (bnum_exp)
    bnum = (int) parse_and_eval_address (bnum_exp);

  breakpoint_1 (bnum, 1);
}

#endif

/* Print a message describing any breakpoints set at PC.  */

static void
describe_other_breakpoints (CORE_ADDR pc, asection *section)
{
  register int others = 0;
  register struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    if ((are_addrs_equal (b->address, pc)) && (b->number > 0))
    if (overlay_debugging == 0 ||
	b->section == section)
      others++;
  if (others > 0)
    {
      printf_filtered ("Note: breakpoint%s ", (others > 1) ? "s" : "");
      ALL_BREAKPOINTS (b)
	if ((are_addrs_equal (b->address, pc)) && (b->number > 0))
	if (overlay_debugging == 0 ||
	    b->section == section)
	  {
	    others--;
	    printf_filtered
	      ("%d%s%s ",
	       b->number,
	       ((b->enable == disabled || b->enable == shlib_disabled || b->enable == call_disabled)
		? " (disabled)" : ""),
	       (others > 1) ? "," : ((others == 1) ? " and" : ""));
	  }
      printf_filtered ("also set at pc ");
      print_address_numeric (pc, 1, gdb_stdout);
      printf_filtered (".\n");
    }
}

/* Set the default place to put a breakpoint
   for the `break' command with no arguments.  */

void
set_default_breakpoint (int valid, CORE_ADDR addr, struct symtab *symtab, int line)
{
  default_breakpoint_valid = valid;
  default_breakpoint_address = addr;
  default_breakpoint_symtab = symtab;
  default_breakpoint_line = line;
}

struct symtab*
get_default_breakpoint_symtab ()
{
  if (default_breakpoint_valid)
    return default_breakpoint_symtab;
  else
    return (struct symtab*) NULL;
}

/* Rescan breakpoints at address ADDRESS,
   marking the first one as "first" and any others as "duplicates".
   This is so that the bpt instruction is only inserted once.  */

static void
check_duplicates (CORE_ADDR address, asection *section)
{
  register struct breakpoint *b;
  register int count = 0;
  struct breakpoint *perm_bp = 0;
  if (address == 0)		/* Watchpoints are uninteresting */
    return;

  ALL_BREAKPOINTS (b)
    if (b->enable != disabled
	&& b->enable != user_disabled
	&& b->enable != shlib_disabled
	&& b->enable != call_disabled
	&& are_addrs_equal (b->address, address)
	&& (overlay_debugging == 0 || b->section == section))
    {
      /* Have we found a permanent breakpoint?  */
      if (b->enable == permanent)
	{
	  perm_bp = b;
	  break;
	}
      count++;
      b->duplicate = count > 1;
    }
  
  /* If we found a permanent breakpoint at this address, go over the
     list again and declare all the other breakpoints there to be the
     duplicates.  */

   if (perm_bp)
    {
      perm_bp->duplicate = 0;

      /* Permanent breakpoint should always be inserted.  */
      if (! perm_bp->inserted)
	internal_error ("allegedly permanent breakpoint is not "
			"actually inserted");

      ALL_BREAKPOINTS (b)
	if (b != perm_bp)
	  {
	    if (b->inserted)
	      internal_error ("another breakpoint was inserted on top of "
			      "a permanent breakpoint");

	    if (b->enable != disabled
		&& b->enable != user_disabled
		&& b->enable != shlib_disabled
		&& b->enable != call_disabled
		&& are_addrs_equal (b->address, address)
		&& (overlay_debugging == 0 || b->section == section))
	      b->duplicate = 1;
	  }
    }
}
/* Low level routine to set a breakpoint.
   Takes as args the three things that every breakpoint must have.
   Returns the breakpoint object so caller can set other things.
   Does not set the breakpoint number!
   Does not print anything.

   ==> This routine should not be called if there is a chance of later
   error(); otherwise it leaves a bogus breakpoint on the chain.  Validate
   your arguments BEFORE calling this routine!  */

struct breakpoint *
set_raw_breakpoint (struct symtab_and_line sal)
{
  register struct breakpoint *b, *b1;

  b = (struct breakpoint *) xmalloc (sizeof (struct breakpoint));
  memset (b, 0, sizeof (*b));
  b->address = SWIZZLE(sal.pc);
  if (sal.symtab == NULL)
    b->source_file = NULL;
  else
    /* Use full path for annotation_level > 1 */
    b->source_file = annotation_level > 1?
      savestring (symtab_to_filename (sal.symtab), (int) strlen (symtab_to_filename(sal.symtab))):
      savestring (sal.symtab->filename, (int) strlen (sal.symtab->filename));
#ifdef PC_SOLIB
  if (b->address)
    {
      char* module_name = solib_and_mainfile_address(b->address);
      if (module_name)
        b->module_name = savestring(module_name, (int) strlen(module_name));
    }
#endif
  b->shadow_contents_known_to_be_valid = 0;
  b->section = sal.section;
  b->language = current_language->la_language;
  b->input_radix = input_radix;
  b->thread = -1;
  b->line_number = sal.line;
  b->inline_idx = sal.inline_idx;
  b->enable = enabled;
  b->next = 0;
  b->silent = 0;
  b->ignore_count = 0;
  b->commands = NULL;
  b->frame = 0;
#ifdef REGISTER_STACK_ENGINE_FP
  b->rse_fp= 0;
#endif
  b->dll_pathname = NULL;
  b->triggered_dll_pathname = NULL;
  b->forked_inferior_pid = 0;
  b->exec_pathname = NULL;

  /* Add this breakpoint to the end of the chain
     so that a list of breakpoints will come out in order
     of increasing numbers.  */

  b1 = breakpoint_chain;
  if (b1 == 0)
    breakpoint_chain = b;
  else
    {
      while (b1->next)
	b1 = b1->next;
      b1->next = b;
    }

  check_duplicates (sal.pc, sal.section);
  breakpoints_changed ();

  return b;
}

/* Note that the breakpoint object B describes a permanent breakpoint
   instruction, hard-wired into the inferior's code.  */
void
make_breakpoint_permanent (struct breakpoint *b)
{
  b->enable = permanent;

  /* By definition, permanent breakpoints are already present in the code.  */
  b->inserted = 1;
}

#ifdef GET_LONGJMP_TARGET

static void
create_longjmp_breakpoint (char *func_name)
{
  struct symtab_and_line sal;
  struct breakpoint *b;

  INIT_SAL (&sal);		/* initialize to zeroes */
  if (func_name != NULL)
    {
      struct minimal_symbol *m;

      m = lookup_minimal_symbol_text (func_name, NULL, (struct objfile *) NULL);
      if (m)
	sal.pc = SYMBOL_VALUE_ADDRESS (m);
      else
	return;
    }
  sal.pc = SWIZZLE(sal.pc);
  sal.section = find_pc_overlay (sal.pc);
  b = set_raw_breakpoint (sal);
  if (!b)
    return;

  b->type = func_name != NULL ? bp_longjmp : bp_longjmp_resume;
  b->disposition = donttouch;
  b->enable = disabled;
  b->silent = 1;
  if (func_name)
    b->addr_string = strsave (func_name);
  b->number = internal_breakpoint_number--;
}

#endif /* #ifdef GET_LONGJMP_TARGET */

/* Call this routine when stepping and nexting to enable a breakpoint if we do
   a longjmp().  When we hit that breakpoint, call
   set_longjmp_resume_breakpoint() to figure out where we are going. */

#ifdef GET_LONGJMP_TARGET

void
enable_longjmp_breakpoint ()
{
  register struct breakpoint *b = breakpoint_chain;

  while (b)
    {  
      if (   b->type == bp_longjmp
          || b->type == bp_hw_longjmp)
        {
    /* If the shared libs are not mapped privately, we can handle longjmp breakpoints in
      2 ways, either establish an internal hardware breakpoint on IA, or delete the breakpoint.
      Since the hardware support is limited for hardware bpts and also that not always user
      might want to step over longjmp calls, we have opted for the latter option of just deleting 
      the internal bpt that gdb places in libc::longjmp. */ 
          if (mapshared || (attach_flag && !is_solib_mapped_private()))
            {
               /* store b->next to avoid free memory read for b->next*/
               struct breakpoint *tmp = b->next;
               delete_breakpoint (b);
               b = tmp; 
               continue;
            }
          b->enable = enabled;
          check_duplicates (b->address, b->section);
        }
      b = b->next;
    }
}
#endif /* GET_LONGJMP_TARGET */

#ifdef GET_LONGJMP_TARGET

void
disable_longjmp_breakpoint ()
{
  register struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    if (b->type == bp_longjmp
        || b->type == bp_hw_longjmp
	|| b->type == bp_longjmp_resume
	|| b->type == bp_hw_longjmp_resume)
    {
      b->enable = disabled;
      check_duplicates (b->address, b->section);
    }
}
#endif /* GET_LONGJMP_TARGET */

struct breakpoint *
create_thread_event_breakpoint (CORE_ADDR address)
{
  struct breakpoint *b;
  struct symtab_and_line sal;
  char addr_string[80];		/* Surely an addr can't be longer than that. */

  INIT_SAL (&sal);		/* initialize to zeroes */
  sal.pc = SWIZZLE(address);
  sal.section = find_pc_overlay (sal.pc);
  if ((b = set_raw_breakpoint (sal)) == NULL)
    return NULL;
  
  b->number = internal_breakpoint_number--;
  b->disposition = donttouch;
  b->type = bp_thread_event;	/* XXX: do we need a new type? 
				   bp_thread_event */
  b->enable = enabled;
  /* addr_string has to be used or breakpoint_re_set will delete me.  */
  sprintf (addr_string, "*0x%s", paddr (b->address));
  b->addr_string = strsave (addr_string);

  return b;
}

void
remove_thread_event_breakpoints (void)
{
  struct breakpoint *b, *temp = 0; /* initialize for compiler warning */

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (b->type == bp_thread_event)
      delete_breakpoint (b);
}

#ifdef SOLIB_ADD
void
remove_solib_event_breakpoints ()
{
  register struct breakpoint *b, *temp = NULL;

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (b->type == bp_shlib_event)
    delete_breakpoint (b);
}

#ifdef RTC

/* srikanth, 991020, Insert a breakpoint in rtc_event(). This is called
   by the RTC library for internal errors and external errors such as
   calling free() with a non allocated object. */

struct breakpoint *
create_rtc_event_breakpoint (CORE_ADDR address, enum bptype event_type)
{
  struct breakpoint *b;
  struct symtab_and_line sal;

  INIT_SAL (&sal);              /* initialize to zeroes */
  sal.pc = SWIZZLE(address);
  sal.section = find_pc_overlay (sal.pc);
  b = set_raw_breakpoint (sal);
  b->number = internal_breakpoint_number--;
  b->disposition = donttouch;
  b->type = event_type;

  return b;
}

#ifdef HPPA_FIX_AND_CONTINUE
void
create_bp_fix_event_breakpoint (CORE_ADDR address)
{
  struct breakpoint *b;
  struct symtab_and_line sal;

  INIT_SAL (&sal);              /* initialize to zeroes */
  sal.pc = address;
  sal.section = find_pc_overlay (sal.pc);
  b = set_raw_breakpoint (sal);
  b->number = internal_breakpoint_number--;
  b->disposition = donttouch;
  b->type = bp_fix_event;
}

void
remove_bp_fix_event_breakpoints ()
{
  register struct breakpoint *b, *temp = NULL;

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (b->type == bp_fix_event)
      {
        remove_breakpoint (b, mark_uninserted);
        delete_breakpoint (b);
      }
}
#endif

void
remove_rtc_event_breakpoints ()
{
  register struct breakpoint *b, *temp = NULL;

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (b->type == bp_rtc_event || b->type == bp_hw_rtc_event || 
        b->type == bp_rtc_compiler_event || b->type == bp_hw_rtc_compiler_event)
      delete_breakpoint (b);
}

#endif

struct breakpoint *
create_rantomain_event_breakpoint (int sigwait)
{
  CORE_ADDR address;
  struct breakpoint *b, *temp = 0;
  struct symtab_and_line sal;
  struct minimal_symbol * m;

  /* Delete the existing ones first */
  ALL_BREAKPOINTS_SAFE (b, temp)
    {
      if (   (sigwait && b->type == bp_rantomain_sigwait_event)
          || (!sigwait && b->type == bp_rantomain_event))
        delete_breakpoint (b);
    }

  m = lookup_minimal_symbol_text ("main", 0, 0);
  if (m)
  {
    address = SYMBOL_VALUE_ADDRESS(m);
    address = SKIP_PROLOGUE (address);
    INIT_SAL (&sal);
    sal.pc = SWIZZLE(address);
    sal.section = find_pc_overlay (sal.pc);
    b = set_raw_breakpoint (sal);
    b->number = internal_breakpoint_number--;
    b->disposition = donttouch;
    if (sigwait)
      b->type = bp_rantomain_sigwait_event;
    else
      b->type = bp_rantomain_event;
    return b;
  }

  return 0;
}

void
remove_rantomain_event_breakpoint (int sigwait)
{
  register struct breakpoint *b, *temp = NULL;

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (   (!sigwait && b->type == bp_rantomain_event)
	|| (sigwait && b->type == bp_rantomain_sigwait_event))
      delete_breakpoint (b);
}

struct breakpoint *
create_solib_event_breakpoint (CORE_ADDR address)
{
  struct breakpoint *b;
  struct symtab_and_line sal;

  INIT_SAL (&sal);		/* initialize to zeroes */
  sal.pc = SWIZZLE(address);
  sal.section = find_pc_overlay (sal.pc);
  b = set_raw_breakpoint (sal);
  b->number = internal_breakpoint_number--;
  b->disposition = donttouch;
  b->type = bp_shlib_event;

  return b;
}

static void
create_deferred_event_breakpoint (char* arg, enum bptype type)
{
  struct symtabs_and_lines sals;
  int tmpnelts;
  register struct expression **cond = 0;
  /* Pointers in arg to the start, and one past the end, of the
     condition.  */
  char **cond_string = (char **) NULL;
  char *addr_start = arg;
  char **addr_string = NULL;
  struct cleanup *old_chain;
  struct cleanup *breakpoint_chain = NULL;
  int i;
  int thread = -1;
  int ignore_count = 0;
  struct breakpoint *b;


  sals.sals = NULL;
  sals.nelts = 0;
  sals.is_file_and_line = 0;

  parse_breakpoint_sals (&arg, &sals, &addr_string, ALL_SYMBOLS);

  if (!sals.nelts)
    {
#ifdef SOLIB_ADD    
      /* RM: location may be in a shared library. Do not return. We
	 can place deferred breakpoint. */
      if (! addr_string)
#endif
	return;
    }

#ifdef SOLIB_ADD    
  if (!sals.nelts)
    tmpnelts = 1;
  else
#endif
    tmpnelts = sals.nelts;

  /* Create a chain of things that always need to be cleaned up. */
  old_chain = make_cleanup (null_cleanup, 0);

  /* Make sure that all storage allocated to SALS gets freed.  */
  make_cleanup (free, sals.sals);

  /* Cleanup the addr_string array but not its contents. */
  make_cleanup (free, addr_string);

  /* Allocate space for all the cond expressions. */
  cond = xcalloc (tmpnelts, sizeof (struct expression **));
  make_cleanup (free, cond);

  /* Allocate space for all the cond strings. */
  cond_string = xcalloc (tmpnelts, sizeof (char **));
  make_cleanup (free, cond_string);

  /* ----------------------------- SNIP -----------------------------
     Anything added to the cleanup chain beyond this point is assumed
     to be part of a breakpoint.  If the breakpoint create succeeds
     then the memory is not reclaimed. */
  breakpoint_chain = make_cleanup (null_cleanup, 0);

  /* Mark the contents of the addr_string for cleanup.  These go on
     the breakpoint_chain and only occure if the breakpoint create
     fails. */
  for (i = 0; i < tmpnelts; i++)
    {
      if (addr_string[i] != NULL)
	make_cleanup (free, addr_string[i]);
    }

  b = set_raw_breakpoint (sals.sals[0]);
  b->number = internal_breakpoint_number--;
  b->type = type;
  b->addr_string = addr_string[0];
  b->func_string = NULL;
  if (sals.nelts && sals.is_file_and_line)
    {
      struct symbol *sym;
      sym = find_pc_sect_function (sals.sals[0].pc, sals.sals[0].section);
      if (sym)
        b->func_string = strsave (SYMBOL_SOURCE_NAME (sym));
    }
#ifdef SOLIB_ADD
        if (!sals.nelts)
          b->enable = shlib_disabled;
        else
#endif
	  b->enable = enabled;
  b->disposition = donttouch;


  /* That's it. Discard the cleanups for data inserted into the
     breakpoint. */
  discard_cleanups (breakpoint_chain);
  /* But cleanup everything else. */
  do_cleanups (old_chain);

}

/* Create a sbrk_event_breakpoint. This is an internal breakpoint. The code
   is similar to breakpoint_1(). It takes care of this breakpoint being in
   yet unloaded shared library. arg is function name like sbrk, brk and mmap. 
   We place a breakpoint at start of sbrk, brk and mmap so that we get an
   immediate control after new memory is added to process address space.
   We would like to replace the deferred watchpoints at this point.  */
static void
create_sbrk_event_breakpoint (char* arg)
{
  create_deferred_event_breakpoint(arg, bp_sbrk_event);
}

/* Make the hardware watchpoints deferred during each rerun.
   insert_breakpoints () will take care of resetting them after rerun. */
void
disable_hw_watchpoints ()
{
  struct breakpoint *b, *temp = 0; /* initialize for compiler warning */

  ALL_BREAKPOINTS_SAFE (b, temp)
    {
      if (   (b->type == bp_hardware_watchpoint)
          && (b->enable == enabled))
	{
          b->enable = heap_disabled;
	  /* Set the brk_event_breakpoints here since we have heap_disabled
	     watchpoints now. */
	  if (!sbrk_breakpoint_set)
            CREATE_SBRK_BREAKPOINTS;
	}
    }
#ifdef GDB_TARGET_IS_HPUX
  reset_memory_page_protections ();
#endif
}


/* JAGaf54650: Defferred breakpoints are taken care by the existing
   functionality for shared library breakpoints. For serial debugging,
   the breakpoints set for parent process need to be deffered when
   the child is being debugged and vise versa. 'shlib_disabled' itself
   is reused to avoid redundant coding. The complete usage of shlib_disabled
   is applicable for serial debugging also. 
 */

void defer_breakpoints (void);

void
defer_breakpoints (void)
{
  struct breakpoint *b, *temp = 0;

  ALL_BREAKPOINTS_SAFE (b, temp)
    {
      if ((b->type == bp_breakpoint) && (b->enable == enabled))
	{
	  b->enable = shlib_disabled;

	  if (delete_hook)
	  delete_hook (b);

	  b->address = 0;
	  free (b->source_file);
	  b->source_file = 0;
          if (b->module_name)
            {
              free (b->module_name);
              b->module_name = 0;
            }
	} 
    }
}

/* Disable any breakpoints that are on code in shared libraries.  Only
   apply to enabled breakpoints, disabled ones can just stay disabled.  */

void
disable_breakpoints_in_shlibs (int silent, void* solib)
{
  struct breakpoint *b, *temp = 0, *b1; /* initialize for compiler warning */
  int disabled_shlib_breaks = 0;
  char *cond_string; 
  args_for_parse_exp_1 args;

  /* See also: insert_breakpoints, under DISABLE_UNSETTABLE_BREAK. */
  ALL_BREAKPOINTS_SAFE (b, temp)
  {
#if defined (ADDR_THIS_SOLIB)
    if (solib != NULL && !ADDR_THIS_SOLIB(solib, b->address)) continue;
#else
    if (solib != NULL) return;
#endif

    /* JAGaf51525: Need not disable bp_catch_nomem. Anyway it is going
       to be deleted. Change it once we decide to disable on exit and 
       re-enable on rerun (shared library re-load).
    */
    if (b->type == bp_catch_nomem || b->type == bp_hw_catch_nomem)
      continue;
    b->shadow_contents_known_to_be_valid = 0;
#if defined (PC_SOLIB)
    if (((b->type == bp_sbrk_event) ||
         (b->type == bp_breakpoint) ||
	 (b->type == bp_procedure) ||
	 (b->type == bp_hardware_breakpoint)) &&
	b->enable == enabled &&
	PC_SOLIB (b->address))
      {
	b->enable = shlib_disabled;
        b->inserted = false;

        if (delete_hook)
          delete_hook (b);
	
	b->address = 0;
	free (b->source_file);
	b->source_file = 0;

	if (!silent)
	  {
	    if (b->number >= 0)
	      {
	        if (!disabled_shlib_breaks)
	          {
		    target_terminal_ours_for_output ();
                    warning ("Temporarily disabling or deleting shared library breakpoints:");
	          }
	        disabled_shlib_breaks = 1;
                warning ("Disabling breakpoint #%d ", b->number);
	      }
	  }
      }
#endif
  }
}

/* cover routine for parse_exp_1 */
int 
cover_parse_exp_1 (args_for_parse_exp_1 *args)
{
  args->result = parse_exp_1 (args->exp_string_p, args->block, 0);
  return 1;
}

/* cover routine for decode_line_1 */
int 
cover_decode_line_1 (args_for_decode_line_1 *args)
{
  args->result = decode_line_1 (args->addr_string_p,
				1,
				(struct symtab *) NULL,
				0,
				args->canonical_p,
				ALL_SYMBOLS);
  return 1;
}

/* Try to reenable any breakpoints in shared libraries.  */
void
re_enable_breakpoints_in_shlibs ()
{
  struct breakpoint *b, *cur_bp;
  struct breakpoint *prev = NULL;
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  char *addr_string, *addr_string2;
  char *cond_string;
  int i;
  args_for_parse_exp_1 args;
  args_for_decode_line_1 args2;
  char **canonical = (char **) NULL;
  int multi_message = 0;
  int disabled_cond_bpt = 0; /* JAGaf35513 */

  ALL_BREAKPOINTS (b)
    {
      /* update expressions for watchpoints on shlib re-runs/unload */
      if (b->exp)
        {
          bpstat bs = (bpstat) xcalloc (1, sizeof (struct bpstats));
          bs->breakpoint_at = b;
          watchpoint_check ((char *)((void *)bs));     
          free (bs);
        }

      if (b->enable == shlib_disabled)
        {
          char buf[1];
          b->shadow_contents_known_to_be_valid = 0;
          if (!b->address)
	    {
	      /* RM: see if the breakpoint location is now known */
	      sals.sals = NULL;
	      sals.nelts = 0;
              sals.is_file_and_line = 0;

	      addr_string = b->addr_string;
	      addr_string2 = addr_string;
	      args2.result.nelts = 0;
	      args2.addr_string_p = &addr_string2;
	      canonical = 0;
	      args2.canonical_p = &canonical;
	      if (!catch_errors ((catch_errors_ftype *)
	         		     cover_decode_line_1,
		        	     (char *) &args2,
			             "",
			             RETURN_MASK_ALL))
	      continue;

	      sals = args2.result;

	      if (sals.nelts == 0)
	        continue;

	      /* do we have hardware breakpoints available? */
	      if (b->type == bp_hardware_breakpoint)
	        {
	          int i, target_resources_ok;

	          i = next_hw_breakpoint (0);
	          target_resources_ok =
		    TARGET_CAN_USE_HARDWARE_WATCHPOINT (bp_hardware_breakpoint,
						    i + sals.nelts, 0);
	          if (target_resources_ok <= 0)
		    b->type = bp_breakpoint;
	        }


	      /* RM: if so, set the address field of the breakpoint */
	      /* might expand to more than one breakpoint. Just set a
	       * breakpoint on all instances. */
	      /* iterate over all instances */
	      for (i = 0; i < sals.nelts; i++)
	        {
	           sal = sals.sals[i];
	           catch_errors ((catch_errors_ftype *)
		        	    resolve_sal_pc,
			           (char *) &sal,
			           "",
			           RETURN_MASK_ALL);

#ifdef ADDR_LOADED_SOLIB
	           if (!sal.pc || !ADDR_LOADED_SOLIB(sal.pc))
#else
                   if (!sal.pc)
#endif
		     continue;

                   sal.pc = SWIZZLE(sal.pc);
#ifdef PC_SOLIB
                   /* The name of the breakpoint load module, if it's set, 
                      must match that in the sal. If it doesn't, skip setting 
                      breakpoint. */
                   if (b->module_name &&
                     strcmp(solib_and_mainfile_address(sal.pc), b->module_name))
                   continue;
#endif              

	           /* first time around the loop, we use the existing breakpoint
	            * structure. Subsequent iterations require new breakpoint
	            * structures */
	           if (i)
		     {
		       if ((i == 1) && (b->number >0) && !prev)
		         {
		           printf_filtered ("Multiple breakpoints were set at \"%s\".\n", b->addr_string);
		           if (!multi_message)
			     {
      		               printf_filtered ("Use the \"delete\" command to delete unwanted breakpoints.\n");
			       multi_message = 1;
			     }
		         }
		       /* need more breakpoint structures */
		       cur_bp = set_raw_breakpoint (sal);
		       /* If it is an internal breakpoint, use internal
		          breakpoint numbers for additional breakpoints as well. */
		       if (b->number < 0)
		         cur_bp->number = internal_breakpoint_number--;
		       else
		         {
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
                           if (!prev)
#endif
		             set_breakpoint_count (breakpoint_count + 1);
		           cur_bp->number = breakpoint_count;
		         }
		       cur_bp->type = b->type;
		       cur_bp->cond = NULL;
		       cur_bp->thread = b->thread;
		       if (canonical != (char **) NULL &&
		           canonical[i] != NULL)
		         cur_bp->addr_string = canonical[i];
		       else
		         cur_bp->addr_string = savestring (addr_string,
						      (int) strlen (addr_string));
		       if (b->cond_string)
		         cur_bp->cond_string = savestring (b->cond_string,
						   (int) strlen (b->cond_string));
		       cur_bp->enable = shlib_disabled;
		       cur_bp->disposition = b->disposition;
		     }
	           else
		     {
		       cur_bp = b;
		       if (canonical != (char **) NULL &&
		           canonical[i] != NULL)
		         cur_bp->addr_string = canonical[i];
		       else
		         cur_bp->addr_string = savestring (addr_string,
						      (int) strlen (addr_string));
		     }

	           cur_bp->address = sal.pc;
#ifdef PC_SOLIB
                   if (cur_bp->address && cur_bp->module_name == NULL)
                     {
                       char* module_name = 
                        solib_and_mainfile_address(cur_bp->address);
                       if (module_name)
                         cur_bp->module_name = 
                        savestring(module_name, (int) strlen(module_name));
                     }
#endif
	           if (sal.symtab == NULL)
                     cur_bp->source_file = NULL;
	           else
		     cur_bp->source_file =
		     savestring (sal.symtab->filename,
			      (int) strlen (sal.symtab->filename));
	           cur_bp->line_number = sal.line;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
                   /* When re enabling inline breakpoints, save the inline_idx also */
                   if (sal.inline_idx)
                     cur_bp->inline_idx = sal.inline_idx;
#endif

                   /*
                    * JAGag36201 - several places in this file do something
                    * similar.  Note that the assignment args.result to 
                    * bp->cond should only happen if the parsing is 
                    * successful.  There will be cases where parsing is 
                    * unsuccessful here but will be handled properly later
                    * so simply emit a message but don't turn the breakpoint
                    * to unconditional.  A friendlier message is needed 
                    * because the parsing routine can only emit a general 
                    * message No symbol "XYZ" in current context.
                    */

	           if (cur_bp->cond_string)
		     {
		       cond_string = cur_bp->cond_string;
		       args.exp_string_p = &cond_string;
		       args.block = block_for_pc (sal.pc);
		       args.result = 0;

		       if (!catch_errors ((catch_errors_ftype *)
				     cover_parse_exp_1,
				     (char *) &args,
				     "",
				     RETURN_MASK_ALL))

                         printf_filtered ("\nUnable to parse condition for "
                                          "breakpoint %d at this point.\nThis "
                                          "condition will be re-evaluated "
                                          "in the future.\n",
                                          cur_bp->number);
                       else
                         cur_bp->cond = args.result;

		    }

	           if (cur_bp->address &&
		     target_read_memory (cur_bp->address, buf, 1) == 0)
		   cur_bp->enable = enabled;

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
                   /* Suppose user has placed a deferred breakpoint 
                      on an inline function, then chain all 
                      the inline instance breakpoints using the related
                      breakpont field while resolving them.
                    */
                       
                   if (inline_debugging == BP_ALL && sals.nelts > 1 && 
                        sals.sals[i].inline_idx > 0)
                     {
                       if (prev)
                         prev->related_breakpoint = cur_bp; 
                       prev = cur_bp;
                     }
                   else
                     prev = NULL; 
#endif
	         }

	       if (b->enable == enabled)
	         {
	           /* Since the breakpoint is effectively modified,
	              inform those that might want to know. */
	           if (modify_breakpoint_hook)
	             modify_breakpoint_hook (b);

                   if (break_hook)
                     break_hook (b);

	           safe_free (addr_string);
	         }
	       check_duplicates (sal.pc, b->section);
	       breakpoints_changed ();

               /* srikanth, 991001, deallocate memory for objects created by 
                  the call to decode_line_1() above.
                */
               free (sals.sals);  
               if (canonical)
                 free (canonical);
	     }
           else
	     {
	       /* Do not reenable the breakpoint if the shared library
	          is still not mapped in.  */
	       /* It so happens that a shared library is already mapped into
		  its address space but we have not read yet.
		  So while enabling the deffered breakpoint at an address 
		  check whether the address is falling in address range of
		  any shared library that we have read. That information
		  is there in so_list chain. 
		  If we blindly enable the breakpoint just by checking that 
		  could we read the memory at the address given, 
	          creates trouble in insert_breakpoints().
		  HOW?? read down
		  We have enabled the brkpt that is there in a shlib that is
		  mapped but we have no details about that in our so_list yet.
		  While inerting breakpoint we try s/w breakpoint first.
		  we fail because it is in shlib that is not mapped private
		  then, we try h/w brkpt only if we have info about that library
		  in our so_list otherwise, we dont put h/w brkpt at the addr.
		  we fall back saying can not insert the breakpoint and 
		  again we try to insert the brkpt when we continue the 
		  application but, 
	   	  before reading/getting load events we insert brkpts.
		  While inserting this brkpt we fail again and will stop
		  again. This creates an endless loop.. we never allow the 
		  application to proceed until we insert this brkpt,
		  we do not insert hw brkpt until we have info about this
		  shlib in our so_list.
		 
	  	  So do not enable this until we info about this shlib in our
		  so_list that check is done by PC_SOLIB(b->address).	
	       */ 
		if (b->address && target_read_memory (b->address, buf, 1) == 0
		   && PC_SOLIB(b->address) != NULL)
	         {
	           b->enable = enabled;
	           /* JAGae87253 - Conditional breakpoint */
	           if (b->cond_string != NULL)
	             {
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
	               if (shlib_load_completed == 1) /* JAGaf35513 */
	                 {
#endif
	                   cond_string = b->cond_string;
		           args.exp_string_p = &cond_string;
		           args.block = block_for_pc (b->address);
		           args.result = 0;
		           if (!catch_errors ( (catch_errors_ftype *)
                                                 cover_parse_exp_1,
                                                  (char *) &args,
                                                  "",
                                                  RETURN_MASK_ALL))
		             { 
		               /* JAGaf35513 - Invalid conditional breakpoints are disabled */ 
			       b->enable = disabled;
                               warning ("Disabled conditional breakpoint #%d ", b->number);
                               if (!disabled_cond_bpt)
                                 warning ("Use \"enable\" command to enable breakpoints at correct context.");
                               disabled_cond_bpt = 1;
	                 }
                       else 
		         b->cond = args.result;
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
	             }
		   else
	             b->enable = shlib_disabled; /* JAGaf35513 - ends */
#endif
	         }
	         /* JAGae87253 - ends */
               if (break_hook)
                 break_hook (b);
             }
           }
         }
       else
         b->shadow_contents_known_to_be_valid = 0;  /* undue paranoia ? */
     }
}

#endif

void
remove_mmap_breakpoints ()
{
  register struct breakpoint *b, *temp = 0; /* initialize for compiler warning*/

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (b->type == bp_mmap)
    delete_breakpoint (b);
}

struct breakpoint *
create_mmap_breakpoint (CORE_ADDR address)
{
  struct breakpoint *b;
  struct symtab_and_line sal = {0};

  sal.pc = SWIZZLE(address);
  sal.symtab = NULL;
  sal.section = NULL;
  sal.line = 0;
  b = set_raw_breakpoint (sal);
  b->number = internal_breakpoint_number--;
  b->disposition = donttouch;
  b->type = bp_mmap;

  return b;
}

static void
create_solib_load_unload_event_breakpoint (char *hookname, int tempflag, char *dll_pathname,
		                           char *cond_string, enum bptype bp_kind
#ifdef DYNLINK_HAS_BREAK_HOOK
					   ,void *dynlink_event
#endif
)
{
  struct breakpoint *b;
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  struct cleanup *old_chain;
  struct cleanup *canonical_strings_chain = NULL;
  char *addr_start = hookname;
  char *addr_end = NULL;
  char **canonical = (char **) NULL;
  int thread = -1;		/* All threads. */

#ifndef DYNLINK_HAS_BREAK_HOOK
  /* Set a breakpoint on the specified hook. */
  sals = decode_line_1 (&hookname, 1, (struct symtab *) NULL, 0, &canonical, USER_CHOICE);
  addr_end = hookname;

  if (sals.nelts == 0)
    {
      warning ("Unable to set a breakpoint on dynamic linker callback.");
      warning ("Suggest linking with /opt/langtools/lib/end.o.");
      warning ("GDB will be unable to track shl_load/shl_unload calls");
      return;
    }
  if (sals.nelts != 1)
    {
      warning ("Unable to set a unique breakpoint on dynamic linker callback.");
      warning ("GDB will be unable to track shl_load/shl_unload calls");
      return;
    }

  /* Make sure that all storage allocated in decode_line_1 gets freed in case
     the following errors out.  */
  old_chain = make_cleanup (free, sals.sals);
  if (canonical != (char **) NULL)
    {
      make_cleanup (free, canonical);
      canonical_strings_chain = make_cleanup (null_cleanup, 0);
      if (canonical[0] != NULL)
	make_cleanup (free, canonical[0]);
    }

  resolve_sal_pc (&sals.sals[0]);

  /* Remove the canonical strings from the cleanup, they are needed below.  */
  if (canonical != (char **) NULL)
    discard_cleanups (canonical_strings_chain);
  b = set_raw_breakpoint (sals.sals[0]);
#else /* ifndef DYNLINK_HAS_BREAK_HOOK */
  addr_end = hookname;
  INIT_SAL (&sal);		/* initialize to zeroes */
  b = set_raw_breakpoint (sal);
#endif /* else ifndef DYNLINK_HAS_BREAK_HOOK */

  set_breakpoint_count (breakpoint_count + 1);
  b->number = breakpoint_count;
  b->cond = NULL;
  b->cond_string = (cond_string == NULL) ? NULL : savestring (cond_string, (int) strlen (cond_string));
  b->thread = thread;

  if (canonical != (char **) NULL && canonical[0] != NULL)
    b->addr_string = canonical[0];
  else if (addr_start)
    b->addr_string = savestring (addr_start, (int)(addr_end - addr_start));

  b->enable = enabled;
  b->disposition = tempflag ? del : donttouch;

  if (dll_pathname == NULL)
    b->dll_pathname = NULL;
  else
    {
      b->dll_pathname = (char *) xmalloc (strlen (dll_pathname) + 1);
      strcpy (b->dll_pathname, dll_pathname);
    }
  b->type = bp_kind;
#ifdef DYNLINK_HAS_BREAK_HOOK
  b->dynlink_event = dynlink_event;
  DYNLINK_REGISTER_BP (dynlink_event, b);
#endif

  mention (b);
#ifndef DYNLINK_HAS_BREAK_HOOK
  do_cleanups (old_chain);
#endif
}

void
create_solib_load_event_breakpoint (char *hookname, int tempflag, char *dll_pathname, char *cond_string
#ifdef DYNLINK_HAS_BREAK_HOOK
				    ,void *dynlink_event
#endif
)

{
  create_solib_load_unload_event_breakpoint (hookname,
					     tempflag,
					     dll_pathname,
					     cond_string,
					     bp_catch_load
#ifdef DYNLINK_HAS_BREAK_HOOK
					     ,dynlink_event
#endif
    );
}

void
create_solib_unload_event_breakpoint (char *hookname, int tempflag, char *dll_pathname, char *cond_string
#ifdef DYNLINK_HAS_BREAK_HOOK
				      , void *dynlink_event
#endif
)

{
  create_solib_load_unload_event_breakpoint (hookname,
					     tempflag,
					     dll_pathname,
					     cond_string,
					     bp_catch_unload
#ifdef DYNLINK_HAS_BREAK_HOOK
					     ,dynlink_event
#endif
    );
}

static void
create_fork_vfork_event_catchpoint (int tempflag, char *cond_string, enum bptype bp_kind)
{
  struct symtab_and_line sal = { 0 };
  struct breakpoint *b;
  int thread = -1;		/* All threads. */

  INIT_SAL (&sal);
  sal.pc = 0;
  sal.symtab = NULL;
  sal.line = 0;

  b = set_raw_breakpoint (sal);
  set_breakpoint_count (breakpoint_count + 1);
  b->number = breakpoint_count;
  b->cond = NULL;
  b->cond_string = (cond_string == NULL) ? NULL : savestring (cond_string, (int) strlen (cond_string));
  b->thread = thread;
  b->addr_string = NULL;
  b->enable = enabled;
  b->disposition = tempflag ? del : donttouch;
  b->forked_inferior_pid = 0;

  b->type = bp_kind;

  mention (b);
}

void
create_fork_event_catchpoint (int tempflag, char *cond_string)
{
  create_fork_vfork_event_catchpoint (tempflag, cond_string, bp_catch_fork);
}

void
create_vfork_event_catchpoint (int tempflag, char *cond_string)
{
  create_fork_vfork_event_catchpoint (tempflag, cond_string, bp_catch_vfork);
}

void
create_exec_event_catchpoint (int tempflag, char *cond_string)
{
  struct symtab_and_line sal = { 0 };
  struct breakpoint *b;
  int thread = -1;		/* All threads. */

  INIT_SAL (&sal);
  sal.pc = 0;
  sal.symtab = NULL;
  sal.line = 0;

  b = set_raw_breakpoint (sal);
  set_breakpoint_count (breakpoint_count + 1);
  b->number = breakpoint_count;
  b->cond = NULL;
  b->cond_string = (cond_string == NULL) ? NULL : savestring (cond_string, (int) strlen (cond_string));
  b->thread = thread;
  b->addr_string = NULL;
  b->enable = enabled;
  b->disposition = tempflag ? del : donttouch;

  b->type = bp_catch_exec;

  mention (b);
}

/* Given a normal breakpoint TYPE, return its coresponding hardware breakpoint
   type. */
static enum bptype
convert_to_hardware_breakpoint (enum bptype type)
{
  switch (type)
    {
      case bp_breakpoint:
        return bp_hardware_breakpoint;
      case bp_step_resume:
    	return bp_hw_step_resume;
      case bp_finish:
    	return bp_hw_finish;
      case bp_until:
    	return bp_hw_until;
      case bp_longjmp:
    	return bp_hw_longjmp;
      case bp_longjmp_resume:
    	return bp_hw_longjmp_resume;
      case bp_through_sigtramp:
        return bp_hw_through_sigtramp;
      case bp_watchpoint_scope:
        return bp_hw_watchpoint_scope;
      case bp_rtc_event:
        return bp_hw_rtc_event;
      case bp_rtc_compiler_event:
        return bp_hw_rtc_compiler_event;
      case bp_catch_nomem:
        return bp_hw_catch_nomem;
    }
  return 0;
}

/* Return TRUE if TYPE is a hardware breakpoint type. */
boolean
is_hw_break (enum bptype type)
{
  if (   type == bp_hw_step_resume
      || type == bp_hw_until
      || type == bp_hw_longjmp
      || type == bp_hw_longjmp_resume
      || type == bp_hw_through_sigtramp
      || type == bp_hw_watchpoint_scope
      || type == bp_hw_rtc_event
      || type == bp_hw_catch_nomem
      || type == bp_hardware_breakpoint
      || type == bp_hw_finish)
    return 1;
  return 0;
}

/* Return the number of hardware breakpoints used.  Include the reserved
   breakpoints as well if INTERNAL is FALSE. On IPF we currently reserve
   2 breakpoints for internal use. This function is called with INTERNAL
   set to TRUE when placing an internal hw breakpoint. */
int
next_hw_breakpoint (boolean internal)
{
  const int num_reserved = 2;
  int retval;
  int i = hw_breakpoint_used_count ();
  int i1 = hw_internal_bp_used_count ();

  retval = i;
  if (i1 >= num_reserved || internal)
    /* Already used internal hw breakpoints. */
    retval = i + i1;
  else
    retval = i + num_reserved;
  return retval;
}

static int
hw_breakpoint_used_count ()
{
  register struct breakpoint *b;
  int i = 0;

  ALL_BREAKPOINTS (b)
  {
    if (   (   b->type == bp_hardware_breakpoint
            || b->type == bp_hw_catch_nomem)
        && b->enable == enabled)
      i++;
  }

  return i;
}

static int
hw_internal_bp_used_count ()
{
  register struct breakpoint *b;
  int i = 0;

  ALL_BREAKPOINTS (b)
  {
    if (   is_hw_break (b->type)
        && b->type != bp_hardware_breakpoint
        && b->type != bp_hw_catch_nomem
	&& b->enable == enabled)
      i++;
  }

  return i;
}

static int
hw_watchpoint_used_count (enum bptype type, int *other_type_used)
{
  register struct breakpoint *b;
  int i = 0;

  *other_type_used = 0;
  ALL_BREAKPOINTS (b)
  {
    if (b->enable == enabled)
      {
	if (b->type == type)
	  i++;
	else if ((b->type == bp_hardware_watchpoint ||
		  b->type == bp_read_watchpoint ||
		  b->type == bp_access_watchpoint)
		 && b->enable == enabled)
	  *other_type_used = 1;
      }
  }
  return i;
}

/* Call this after hitting the longjmp() breakpoint.  Use this to set a new
   breakpoint at the target of the jmp_buf.

   FIXME - This ought to be done by setting a temporary breakpoint that gets
   deleted automatically...
 */

#ifdef GET_LONGJMP_TARGET

void
set_longjmp_resume_breakpoint (CORE_ADDR pc, struct frame_info *frame)
{
  register struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    if (   b->type == bp_longjmp_resume
        || b->type == bp_hw_longjmp_resume)
    {
      /* If it is a hw_longjmp_resume breakpoint, reset it to
	 normal one. insert_breakpoints will change it back if
	 required. */
      if (b->type == bp_hw_longjmp_resume)
	b->type = bp_longjmp_resume;
      b->address = pc;
      b->enable = enabled;
      if (frame != NULL)
	{
	  b->frame = frame->frame;
#ifdef REGISTER_STACK_ENGINE_FP
	  b->rse_fp = frame->rse_fp;
#endif
	}
      else
	{
	  b->frame = 0;
#ifdef REGISTER_STACK_ENGINE_FP
	  b->rse_fp = 0;
#endif
	}
      check_duplicates (b->address, b->section);
      return;
    }
}

#endif /* GET_LONGJMP_TARGET */

void
disable_watchpoints_before_interactive_call_start ()
{
  struct breakpoint *b;

  ALL_BREAKPOINTS (b)
  {
    if (((b->type == bp_watchpoint)
	 || (b->type == bp_hardware_watchpoint)
	 || (b->type == bp_read_watchpoint)
	 || (b->type == bp_access_watchpoint)
	 || ep_is_exception_catchpoint (b))
	&& (b->enable == enabled))
      {
	b->enable = call_disabled;
	check_duplicates (b->address, b->section);
      }
  }
}

void
enable_watchpoints_after_interactive_call_stop ()
{
  struct breakpoint *b;

  ALL_BREAKPOINTS (b)
  {
    if (((b->type == bp_watchpoint)
	 || (b->type == bp_hardware_watchpoint)
	 || (b->type == bp_read_watchpoint)
	 || (b->type == bp_access_watchpoint)
	 || ep_is_exception_catchpoint (b))
	&& (b->enable == call_disabled))
      {
	b->enable = enabled;
	check_duplicates (b->address, b->section);
      }
  }
}


/* Set a breakpoint that will evaporate an end of command
   at address specified by SAL.
   Restrict it to frame FRAME if FRAME is nonzero.  */

struct breakpoint *
set_momentary_breakpoint (struct symtab_and_line sal, struct frame_info *frame, enum bptype type)
{
  register struct breakpoint *b;
  b = set_raw_breakpoint (sal);
  b->type = type;
  b->enable = enabled;
  b->disposition = donttouch;
  b->frame = (frame ? frame->frame : 0);
#ifdef REGISTER_STACK_ENGINE_FP
  b->rse_fp = (frame ? frame->rse_fp : 0);
#endif

  /* We also want to capture the inline index, No ?? */
  b->inline_idx = sal.inline_idx;

  /* If we're debugging a multi-threaded program, then we
     want momentary breakpoints to be active in only a 
     single thread of control.  */
  if (in_thread_list (inferior_pid))
    {
#ifdef HP_MXN
      if (is_mxn)
	{
	  int utid;
          utid = get_pthread_for (inferior_pid);
	  if (utid == -1)
	    b->thread = - (get_lwp_for (inferior_pid));
	  else
	    b->thread = utid;
	}
      else
#endif
        b->thread = pid_to_thread_id (inferior_pid);
    }

  return b;
}


/* Tell the user we have just set a breakpoint B.  */

static void
mention (struct breakpoint *b)
{
  int say_where = 0;
#ifdef UI_OUT
  struct cleanup *old_chain;
  struct ui_stream *stb;

  stb = ui_out_stream_new (uiout);
  old_chain = make_cleanup_ui_out_stream_delete (stb);
#endif /* UI_OUT */

  /* FIXME: This is misplaced; mention() is called by things (like hitting a
     watchpoint) other than breakpoint creation.  It should be possible to
     clean this up and at the same time replace the random calls to
     breakpoint_changed with this hook, as has already been done for
     delete_breakpoint_hook and so on.  */
  if (create_breakpoint_hook)
    create_breakpoint_hook (b);

  if (break_hook)
    break_hook (b);

 breakpoint_create_event (b->number);

  switch (b->type)
    {
    case bp_none:
      printf_filtered ("(apparently deleted?) Eventpoint %d: ", b->number);
      break;
#ifdef UI_OUT
    case bp_watchpoint:
      ui_out_text (uiout, "Watchpoint ");
      ui_out_tuple_begin (uiout, "wpt");
      ui_out_field_int (uiout, "number", b->number);
      ui_out_text (uiout, ": ");
      print_expression (b->exp, stb->stream);
      ui_out_field_stream (uiout, "exp", stb);
      ui_out_tuple_end (uiout);
      break;
    case bp_hardware_watchpoint:
      ui_out_text (uiout, "Hardware watchpoint ");
      ui_out_tuple_begin (uiout, "wpt");
      ui_out_field_int (uiout, "number", b->number);
      ui_out_text (uiout, ": ");
      print_expression (b->exp, stb->stream);
      ui_out_field_stream (uiout, "exp", stb);
      ui_out_tuple_end (uiout);
      break;
#else
    case bp_watchpoint:
      printf_filtered ("Watchpoint %d: ", b->number);
      print_expression (b->exp, gdb_stdout);
      break;
    case bp_hardware_watchpoint:
      printf_filtered ("Hardware watchpoint %d: ", b->number);
      print_expression (b->exp, gdb_stdout);
      break;
#endif
#ifdef UI_OUT
    case bp_read_watchpoint:
      ui_out_text (uiout, "Hardware read watchpoint ");
      ui_out_tuple_begin (uiout, "hw-rwpt");
      ui_out_field_int (uiout, "number", b->number);
      ui_out_text (uiout, ": ");
      print_expression (b->exp, stb->stream);
      ui_out_field_stream (uiout, "exp", stb);
      ui_out_tuple_end (uiout);
      break;
    case bp_access_watchpoint:
      ui_out_text (uiout, "Hardware access (read/write) watchpoint ");
      ui_out_tuple_begin (uiout, "hw-awpt");
      ui_out_field_int (uiout, "number", b->number);
      ui_out_text (uiout, ": ");
      print_expression (b->exp, stb->stream);
      ui_out_field_stream (uiout, "exp", stb);
      ui_out_tuple_end (uiout);
      break;
#else
    case bp_read_watchpoint:
      printf_filtered ("Hardware read watchpoint %d: ", b->number);
      print_expression (b->exp, gdb_stdout);
      break;
    case bp_access_watchpoint:
      printf_filtered ("Hardware access (read/write) watchpoint %d: ", b->number);
      print_expression (b->exp, gdb_stdout);
      break;
#endif
    case bp_breakpoint:
#ifdef UI_OUT
      if (ui_out_is_mi_like_p (uiout))
	{
	  say_where = 0;
	  break;
	}
#endif
      printf_filtered ("Breakpoint %d", b->number);
      say_where = 1;
      break;
    case bp_hardware_breakpoint:
#ifdef UI_OUT
      if (ui_out_is_mi_like_p (uiout))
	{
	  say_where = 0;
	  break;
	}
#endif
      printf_filtered ("Hardware assisted breakpoint %d", b->number);
      say_where = 1;
      break;
    case bp_catch_load:
    case bp_catch_unload:
      printf_filtered ("Catchpoint %d (%s %s)",
		       b->number,
		       (b->type == bp_catch_load) ? "load" : "unload",
	     (b->dll_pathname != NULL) ? b->dll_pathname : "<any library>");
      break;
    case bp_catch_nomem:
    case bp_hw_catch_nomem:
      printf_filtered ("Catchpoint %d (%s)",
		       b->number, "nomem");
      break;
    case bp_catch_fork:
    case bp_catch_vfork:
      printf_filtered ("Catchpoint %d (%s)",
		       b->number,
		       (b->type == bp_catch_fork) ? "fork" : "vfork");
      break;
    case bp_catch_exec:
      printf_filtered ("Catchpoint %d (exec)",
		       b->number);
      break;
    case bp_catch_catch:
    case bp_catch_throw:
      printf_filtered ("Catchpoint %d (%s)",
		       b->number,
		       (b->type == bp_catch_catch) ? "catch" : "throw");
      break;

    case bp_until:
    case bp_hw_until:
    case bp_finish:
    case bp_hw_finish:
#ifdef GET_LONGJMP_TARGET
    case bp_longjmp:
    case bp_hw_longjmp:
    case bp_longjmp_resume:
    case bp_hw_longjmp_resume:
#endif /* GET_LONGJMP_TARGET */
    case bp_step_resume:
    case bp_hw_step_resume:
    case bp_through_sigtramp:
    case bp_hw_through_sigtramp:
    case bp_call_dummy:
    case bp_watchpoint_scope:
    case bp_hw_watchpoint_scope:
    case bp_rtc_event:
    case bp_hw_rtc_event:
    case bp_rtc_compiler_event:
    case bp_hw_rtc_compiler_event:
    case bp_fix_event:
    case bp_shlib_event:
    case bp_thread_event:
    case bp_procedure:
      break;
    default:
      break;
    }
  if (say_where)
    {
      char *so_name = NULL;
      if (addressprint || b->source_file == NULL)
	{
	  printf_filtered (" at ");
	  print_address_numeric (b->address, 1, gdb_stdout);
	}
      if (b->source_file)
	{
	  printf_filtered (": file %s, line %d",
			   b->source_file, b->line_number);
	  if (hp_ia64_testsuite || hp_wdb_testsuite)
	    printf_filtered (".");
	}

#ifdef PC_SOLIB
      if (!hp_ia64_testsuite && !hp_wdb_testsuite)
	if ((so_name = solib_and_mainfile_address (b->address)) != NULL)
	  printf_filtered (" from %s.", so_name);
#endif /* PC_SOLIB */
	
      /* 
         ** tuiAllSetHasBreakAt does no screen updates, so it doesn't
         ** need a tuiDo()
       */
      TUIDOWITHOUTTERMTRANSITION (((TuiOpaqueFuncPtr) tui_vAllSetHasBreakAt,
				   b,
				   TRUE));
      TUIDO (((TuiOpaqueFuncPtr) tuiUpdateAllExecInfos));
    }
#ifdef UI_OUT
  do_cleanups (old_chain);
#endif
#ifdef UI_OUT
  if (ui_out_is_mi_like_p (uiout))
    return;
#endif
  if ( b->type != bp_procedure )
    printf_filtered ("\n");
}

/*  JAGae31194 */

int
is_file_colon_line (char *arg)
{
 char *argptr = arg;
 int hasColon=0;
 int lineno=0;
 if(argptr != (char *) NULL)
  {
   while (*argptr && !hasColon)
        {
          hasColon = (*argptr == ':');
          argptr++;
        }
   if(hasColon)
   	{
          lineno=isdigit(*argptr);
        }
   else
	{
	 lineno=isdigit(*arg);
    }
   }
  if(lineno)
  return lineno;
  else
  return 0;
}

/* Add SALS.nelts breakpoints to the breakpoint table.  For each
   SALS.sal[i] breakpoint, include the corresponding ADDR_STRING[i],
   COND[i] and COND_STRING[i] values.

   NOTE: If the function succeeds, the caller is expected to cleanup
   the arrays ADDR_STRING, COND_STRING, COND and SALS (but not the
   array contents).  If the function fails (error() is called), the
   caller is expected to cleanups both the ADDR_STRING, COND_STRING,
   COND and SALS arrays and each of those arrays contents. */

static void
create_breakpoints (struct symtabs_and_lines sals, char **addr_string,
		    struct expression **cond, char **cond_string,
		    enum bptype type, enum bpdisp disposition,
		    int thread, int ignore_count, int from_tty)
{
  int tmpnelts;
  struct breakpoint *b1, *prev = NULL;
#ifdef SOLIB_ADD
  if (sals.nelts)
    {
#endif
      if (is_hw_break (type))
        {
	  int i = next_hw_breakpoint (   type != bp_hardware_breakpoint
				      && type != bp_hw_catch_nomem);
	  int target_resources_ok = 
	    TARGET_CAN_USE_HARDWARE_WATCHPOINT (bp_hardware_breakpoint, 
						i + sals.nelts, 0);
	  if (target_resources_ok == 0)
	    error ("No hardware breakpoint support in the target.");
	  else if (target_resources_ok < 0)
	    error ("Hardware breakpoints used exceeds limit.");
	}
      tmpnelts = sals.nelts;
#ifdef SOLIB_ADD
    }
  else
    {
      tmpnelts = 1;
    }
#endif

  /* Now set all the breakpoints.  */
  {
    int i;
    for (i = 0; i < tmpnelts; i++)
      {
	struct breakpoint *b;
	struct symtab_and_line sal = sals.sals[i];

        sal.pc = SWIZZLE(sal.pc);

#ifdef SOLIB_ADD
	if (sals.nelts)
  	  {
#endif
	    if (   from_tty
		&& procedure_flag == 0
		&& (   type != bp_catch_nomem
		    || type != bp_hw_catch_nomem))
	      describe_other_breakpoints (sal.pc, sal.section);
#ifdef SOLIB_ADD
	  }
#endif

	b = set_raw_breakpoint (sal);
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
        if (!prev || sals.sals[i].inline_idx == 0)
#endif
        set_breakpoint_count (breakpoint_count + 1);
	b->number = breakpoint_count;
        /* Here we set a new type of breakpoint called 
           procedure breakpoint */
        if ( procedure_flag && (type == bp_breakpoint) && annotation_level < 1 )
	b->type = bp_procedure;
        else
	b->type = type;
	b->cond = cond[i];
	b->thread = thread;
	b->addr_string = addr_string[i];
	b->cond_string = cond_string[i];
	b->ignore_count = ignore_count;
	b->func_string = NULL;
 
       /* Set breakall flags to indicate that the user has
          requested for persistent breakpoints */
       if (breakall)
         {
           b->break_exp = (char *) xmalloc (strlen(input)+1);
           strcpy(b->break_exp, input);
           b->breakall=1;
         }
       if (xbreakall)
         {
           b->break_exp = (char *) xmalloc (strlen(input)+1); 
           strcpy(b->break_exp, input);
           b->xbreakall=1;
         } 
       if (rbreakall)
         {
           b->break_exp = (char *) xmalloc (strlen(input)+1);
           strcpy(b->break_exp, input);
           b->rbreakall=1;
         } 

	if (sals.nelts && sals.is_file_and_line)
	  {
	    struct symbol *sym;
	    sym = find_pc_sect_function (sal.pc, sal.section);
	    if (sym)
	      b->func_string = strsave (SYMBOL_SOURCE_NAME (sym));
	  }
#ifdef SOLIB_ADD
        /* Diwakar JAGaf47328 01/14/2005
           If the sal's pc is equal to zero, do not attempt to create
           breakpoint or if contents at the address is not readable,
	   instead create a deferred breakpoint.
         */
        char buf = 0;
	if ((!sals.nelts) || (sals.nelts && sals.sals[i].pc == 0)
	    || (target_read_memory (b->address, &buf, 1) != 0))
	  b->enable = shlib_disabled;
	else
#endif
	  b->enable = enabled;
	b->disposition = disposition;
#ifdef SOLIB_ADD
        if ((!sals.nelts) || (sals.nelts && sals.sals[i].pc == 0)
	    ||(target_read_memory (b->address, &buf, 1) != 0))

	  {
	    /* This is typically called from mention, but we return early */
	    if (create_breakpoint_hook)
	      create_breakpoint_hook (b);

            if (break_hook)
              break_hook (b);

	    breakpoint_create_event (b->number);

	    if (b->address != 0)
	     {
               if (b->line_number > 0 && b->source_file != NULL)
	        {
		  mention (b);
		  printf_filtered
                  ("\nBreakpoint %d (deferred) at address \"0x%llx\"\n"
		   "(\"0x%llx\" was not found).\n",
                   b->number,
                   b->address,
                   b->address);
		 }
	       else
		 printf_filtered	
                 ("Breakpoint %d (deferred) at address \"0x%llx\"\n(\"0x%llx\""
		  " was not found).\n",
                  b->number,
                  b->address,
                  b->address);
	     }
	    else
	     {
	       printf_filtered
	       ("Breakpoint %d (deferred) at \"%s\" (\"%s\" was not found).\n",
	        b->number,
	        b->addr_string,
	        b->addr_string);
	       printf_filtered
	        ("Breakpoint deferred until a shared library containing \"%s\"" 
		 " is loaded.\n", b->addr_string);
             } 
	  }
	else
	  {
	    /* Try to insert the breakpoint to check if the breakpoints needs
	       to be changed to hardware breakpoint. Remove it later. */
	    if (   b->type == bp_breakpoint
                 || b->type == bp_finish
                 || b->type == bp_until
                 || b->type == bp_hw_longjmp
                 || b->type == bp_hw_longjmp_resume
                 || b->type == bp_through_sigtramp
                 || b->type == bp_watchpoint_scope
                 || b->type == bp_rtc_event
                 || b->type == bp_rtc_compiler_event
                 || b->type == bp_catch_nomem
                 || b->type == bp_step_resume)
	      {
	        int val = target_insert_breakpoint (b->address,
                                                  b->shadow_contents);
#if defined (DISABLE_UNSETTABLE_BREAK)
                if (val  && DISABLE_UNSETTABLE_BREAK (b->address))
                  {
		    int i = next_hw_breakpoint (   b->type == bp_finish
                                        || b->type == bp_until
					|| b->type == bp_hw_longjmp
                 			|| b->type == bp_hw_longjmp_resume
                 			|| b->type == bp_through_sigtramp
                 			|| b->type == bp_watchpoint_scope
                 			|| b->type == bp_rtc_event
                 			|| b->type == bp_rtc_compiler_event
                                        || b->type == bp_step_resume);
                    int target_resources_ok =
                      TARGET_CAN_USE_HARDWARE_WATCHPOINT (
                                                bp_hardware_breakpoint,
                                                i + 1, 0);
                    if (target_resources_ok == 0)
                      {
                        /* No HW breakpoints on this target. */
                        /* If this is a batch RTC run, error out. We cannot
                           proceed now. */
                        if (batch_rtc)
                          error ("To run Batch RTC,\nDebug enable the"
                           " executable using\n\t\"chatr +dbg enable"
                           " <executable>\" or \n\t\""
                           "\"/opt/langtools/bin/pxdb -s on <executable>\".\n");
                        else
                          {
                            warning ("Breakpoint address is in a library that"
                                     " is not mapped private");
			    warning ("Prepare the program with ");
                            warning ("\"chatr +dbg enable <executable> \".");
			    warning ("Cannot set breakpoint %d", b->number);
			    delete_breakpoint (b);
                            continue;
                          }
                      }
		    if (target_resources_ok > 0)
		      {
                        /* Inform user that we are changing this to HW break. */
                        if (!batch_rtc)
                          warning ("Breakpoint address is in a library that is"
                                   " not mapped private."
                                   " Changing to Hardware breakpoint.");
			b->type = convert_to_hardware_breakpoint (b->type);
		      }
                    if (target_resources_ok < 0)
		      {
                        /* No more HW breakpoints left. */
                        warning ("Breakpoint address is in a library that is"
                                 " not mapped private.");
		        warning ("Hardware breakpoints used exceeds limit.");
			warning ("Prepare the program with ");
                        warning ("\"chatr +dbg enable <executable> \".");
			warning ("Cannot set breakpoint %d", b->number);
			delete_breakpoint (b);
                        continue;
		      }
	          }
	      if (!val)
	        target_remove_breakpoint (b->address, b->shadow_contents);
	    }
#endif
#endif
          if (!batch_rtc)
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
            if (!prev || sals.sals[i].inline_idx == 0)
#endif
	    mention (b);

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
            /* Chain all the inline instances using the related_breakpoint
               field present in the breakpoint structure 
            */
          if (inline_debugging == BP_ALL && sals.nelts > 1 && sals.sals[i].inline_idx > 0)
            {  
              if (prev)
                prev->related_breakpoint = b;
              prev = b;
            }                    
#endif

             
#ifdef SOLIB_ADD
	  }
#endif
      }
  }    
}

/* Parse ARG which is assumed to be a SAL specification possibly
   followed by conditionals.  On return, SALS contains an array of SAL
   addresses found. ADDR_STRING contains a vector of (canonical)
   address strings. ARG points to the end of the SAL. */

void
parse_breakpoint_sals (char **address,
		       struct symtabs_and_lines *sals,
		       char ***addr_string,
                       enum how_to_resolve_multiple_symbols resolve)
{

  char *addr_start = NULL;
  char *addr_end = NULL;
  char buf[20]; /* number of hex digits in an address + 4 */

  *addr_string = NULL;
  /* If no arg given, or if first arg is 'if ', use the default
     breakpoint. */
  if ((*address) == NULL
      || (strncmp ((*address), "if", 2) == 0 && isspace ((*address)[2])))
    {
      if (default_breakpoint_valid)
	{
	  struct symtab_and_line sal;
	  INIT_SAL (&sal);		/* initialize to zeroes */
	  sals->sals = (struct symtab_and_line *)

          xmalloc (sizeof (struct symtab_and_line));
	  sal.pc = default_breakpoint_address;
	  sal.line = default_breakpoint_line;
	  sal.symtab = default_breakpoint_symtab;
	  sal.section = find_pc_overlay (sal.pc);
	  sals->sals[0] = sal;
	  sals->nelts = 1;
          /* RM: set addr_start and addr_end, or this breakpoint won't
             have an addr_string, and will get deleted when we do a re-run. */
          sprintf(buf, "*0x%08llx", default_breakpoint_address);
          addr_start = buf;
          addr_end = buf+sizeof(buf)-1;
	}
      else
	error ("No default breakpoint address now.");
    }
  else
    {
      addr_start = *address;
      

      /* Force almost all breakpoints to be in terms of the
         current_source_symtab (which is decode_line_1's default).  This
         should produce the results we want almost all of the time while
         leaving default_breakpoint_* alone.  */
      if (default_breakpoint_valid
	  && (!current_source_symtab
	      || (strchr ("+-", (*address)[0]) != NULL)))
	*sals = decode_line_1 (address, 1, default_breakpoint_symtab,
			       default_breakpoint_line, addr_string,
			       resolve);
      else
	*sals = decode_line_1 (address, 1, (struct symtab *) NULL, 0,
			       addr_string, resolve);

      addr_end = *address;
    }

#ifdef SOLIB_ADD
  /* RM: location may be in a shared library. keep the breakpoint around */
  if (! sals->nelts)
    {
      if (! addr_start)
        {
	  *addr_string = NULL;
	}
      else
	{
	  struct symtab_and_line sal;
	  INIT_SAL (&sal);		/* initialize to zeroes */
	  sals->sals = (struct symtab_and_line *)
	    xmalloc (sizeof (struct symtab_and_line));
	  sals->sals[0] = sal;

	  while (addr_end && (addr_end > addr_start) &&
		 (*(addr_end-1) == ' ' || *(addr_end-1) == '\t'))
	    addr_end--;

	  *addr_string = xcalloc (1, sizeof (char **));      
	  (*addr_string)[0] = savestring (addr_start, (int)(addr_end - addr_start));
	}
      return;
    }
#endif
    
  /* For any SAL that didn't have a canonical string, fill one in. */
  if (sals->nelts > 0 && *addr_string == NULL)
    *addr_string = xcalloc (sals->nelts, sizeof (char **));
  if (addr_start != (*address))
    {
      int i;
      for (i = 0; i < sals->nelts; i++)
	{
	  /* Add the string if not present. */
	  if ((*addr_string)[i] == NULL)
	    (*addr_string)[i] = savestring (addr_start, (int)(addr_end - addr_start));
	}
    }
}

/* Convert each SAL into a real PC.  Verify that the PC can be
   inserted as a breakpoint.  If it can't throw an error. */

void
breakpoint_sals_to_pc (struct symtabs_and_lines *sals,
		       char *address)
{    
  int i;
  for (i = 0; i < sals->nelts; i++)
    {
      resolve_sal_pc (&sals->sals[i]);
      /* It's possible for the PC to be nonzero, but still an illegal
         value on some targets.

         For example, on HP-UX if you start gdb, and before running the
         inferior you try to set a breakpoint on a shared library function
         "foo" where the inferior doesn't call "foo" directly but does
         pass its address to another function call, then we do find a
         minimal symbol for the "foo", but it's address is invalid.
         (Appears to be an index into a table that the loader sets up
         when the inferior is run.)

         Give the target a chance to bless sals.sals[i].pc before we
         try to make a breakpoint for it. */
      if (PC_REQUIRES_RUN_BEFORE_USE (sals->sals[i].pc))
	{
	  if (address == NULL)
	    error ("Cannot break without a running program.");
	  else
	    error ("Cannot break on %s without a running program.", 
		   address);
	}
    }
}

/* Parse ARG which is assumed to be breakpoint condition.
   On return, COND contains breakpoint condition, COND_STRING contains
   the condition string, *THREAD contains the thread ID */

static void
parse_breakpoint_cond (char *arg, CORE_ADDR pc,
		struct expression **cond, char **cond_string, int *thread,
		int nelts)
{
  char *tok = arg;

  while (tok && *tok)
    {
      char *end_tok;
      int toklen;
      char *cond_start = NULL;
      char *cond_end = NULL;

      while (*tok == ' ' || *tok == '\t')
	tok++;

      end_tok = tok;

      while (*end_tok != ' ' && *end_tok != '\t' && *end_tok != '\000')
	end_tok++;

      toklen = (int)(end_tok - tok);

      if (toklen >= 1 && strncmp (tok, "if", toklen) == 0)
        {
	  tok = cond_start = end_tok + 1;
	  if (nelts)
  	    {
	      *cond = parse_exp_1 (&tok, block_for_pc (pc), 0);
	      make_cleanup (free, *cond);
	    }
	  else
	    {
              while (tok && *tok)
                tok++;
	      *cond = NULL;
	    }
	  cond_end = tok;
	  *cond_string = savestring (cond_start, (int)(cond_end - cond_start));
	  make_cleanup (free, *cond_string);
	}
      else if (toklen >= 1 && strncmp (tok, "thread", toklen) == 0)
        {
	  char *tmptok;

	  tok = end_tok + 1;
	  tmptok = tok;
	  *thread = (int)strtol (tok, &tok, 0);
	  if (tok == tmptok)
	    error ("Junk after thread keyword.");
#ifdef HP_MXN
	      /* Use the user thread id to put the breakpoint. User uses 
		 -ve ktid to put breakpoints on threads with no user thread 
		 associated. */
	      if (is_mxn)
                {
		  int gdb_tid;
                  if (*thread < 0)
		    {
		      gdb_tid = get_gdb_tid_from_ktid (-(*thread));
		      if (gdb_tid == -1)
                        error ("Unknown thread %d\n", *thread);
		    }
                  gdb_tid = get_gdb_tid_from_utid (*thread);
                  if (gdb_tid == -1)
                    error ("Unknown user thread %d\n", *thread);
                }
	      else
#endif
	  if (!valid_thread_id (*thread))
	    error ("Unknown thread %d\n", *thread);
	}
      else
	error ("Junk at end of arguments.");
    }
}

/* Set a breakpoint according to ARG (function, linenum or *address)
   flag: first bit  : 0 non-temporary, 1 temporary.
   second bit : 0 normal breakpoint, 1 hardware breakpoint. */

/* Diwakar JAGaf47591 01/14/2005
The following flag is used by parse_breakpoint_eval to decide iit needs 
to get the sals for all matching lines in each symbol table. For detail
description of this issue, see the comments for count_exact_line_match
in symtab.c
*/

int match_one_line_flag=1;

static void
break_command_1 (char *arg, int flag, int from_tty)
{
  struct symtabs_and_lines sals;
  int tmpnelts;
  register struct expression **cond = 0;
  /* Pointers in arg to the start, and one past the end, of the
     condition.  */
  char **cond_string = (char **) NULL;
  char *addr_start = arg;
  char **addr_string = NULL;
  struct cleanup *old_chain;
  struct cleanup *breakpoint_chain = NULL;
  int i;
  int thread = -1;
  int ignore_count = 0;
  char *args=arg;
  int hardwareflag = flag & BP_HARDWAREFLAG;
  int nomemflag = flag & BP_NOMEMFLAG;
  int tempflag = flag & BP_TEMPFLAG;
  int matchoneflag = flag & BP_MATCH_ONE; /* match only one sym regardless? */

  sals.sals = NULL;
  sals.nelts = 0;
  sals.is_file_and_line = 0;

  if ((matchoneflag == 0) &&
      (current_language->la_language != language_fortran))
    match_one_line_flag = 0;
  parse_breakpoint_sals (&arg, &sals, &addr_string, matchoneflag ? ALL_SYMBOLS :
								   USER_CHOICE);
  match_one_line_flag = 1;

  if (!sals.nelts)
    {
#ifdef SOLIB_ADD    
      /* RM: location may be in a shared library */
      if (! addr_string)
#endif
	return;
    }

#ifdef SOLIB_ADD    
  if (!sals.nelts)
    tmpnelts = 1;
  else
#endif
    tmpnelts = sals.nelts;

  /* Create a chain of things that always need to be cleaned up. */
  old_chain = make_cleanup (null_cleanup, 0);

  /* Make sure that all storage allocated to SALS gets freed.  */
  make_cleanup (free, sals.sals);

  /* Cleanup the addr_string array but not its contents. */
  make_cleanup (free, addr_string);

  /* Allocate space for all the cond expressions. */
  cond = xcalloc (tmpnelts, sizeof (struct expression **));
  make_cleanup (free, cond);

  /* Allocate space for all the cond strings. */
  cond_string = xcalloc (tmpnelts, sizeof (char **));
  make_cleanup (free, cond_string);

  /* ----------------------------- SNIP -----------------------------
     Anything added to the cleanup chain beyond this point is assumed
     to be part of a breakpoint.  If the breakpoint create succeeds
     then the memory is not reclaimed. */
  breakpoint_chain = make_cleanup (null_cleanup, 0);

  /* Mark the contents of the addr_string for cleanup.  These go on
     the breakpoint_chain and only occure if the breakpoint create
     fails. */
  for (i = 0; i < tmpnelts; i++)
    {
      if (addr_string[i] != NULL)
	make_cleanup (free, addr_string[i]);
    }

#ifdef SOLIB_ADD
  /* RM: location may be in a shared library */
  if (sals.nelts)
#endif
    /* Resolve all line numbers to PC's and verify that the addresses
       are ok for the target.  */
    breakpoint_sals_to_pc (&sals, addr_start);

/*  JAGae31194*/ 
  if (sals.nelts != 0 && is_file_colon_line (args))
  {
      struct minimal_symbol * m;
      
      m = lookup_minimal_symbol_by_pc (sals.sals[0].pc);
      if (m && SYMBOL_VALUE_ADDRESS(m) == sals.sals[0].pc && 
      #ifdef HP_IA64

	      !strncmp (SYMBOL_NAME(m), "_ZN", 3)&& sals.nelts==2)
               {
               sals.nelts=1;
               sals.sals[0].pc = SKIP_PROLOGUE (sals.sals[0].pc);
              }
      #else
	      !strncmp (SYMBOL_NAME(m), "__ct__", 6))
               sals.sals[0].pc = SKIP_PROLOGUE (sals.sals[0].pc);
      #endif

       
  }

  /* Verify that condition can be parsed, before setting any
     breakpoints.  Allocate a separate condition expression for each
     breakpoint. */
  thread = -1;			/* No specific thread yet */
  for (i = 0; i < tmpnelts; i++)
    {
      parse_breakpoint_cond (arg, sals.sals[i].pc, &(cond[i]),
			     &(cond_string[i]), &thread, sals.nelts);
    }

  create_breakpoints (sals, addr_string, cond, cond_string,
		      hardwareflag ? bp_hardware_breakpoint : 
                        nomemflag ? bp_catch_nomem : bp_breakpoint,
		      tempflag ? del : donttouch,
		      thread, ignore_count, from_tty);

  if (sals.nelts > 1 && !procedure_flag )
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
        if (sals.sals[sals.nelts - 1].inline_idx == 0
           || inline_debugging == BP_IND)
#endif
    {
      printf_filtered ("Multiple breakpoints were set.\n");
      printf_filtered ("Use the \"delete\" command to delete unwanted breakpoints.\n");
    }
  /* That's it. Discard the cleanups for data inserted into the
     breakpoint. */
  discard_cleanups (breakpoint_chain);
  /* But cleanup everything else. */
  do_cleanups (old_chain);
}

/* Set a breakpoint of TYPE/DISPOSITION according to ARG (function,
   linenum or *address) with COND and IGNORE_COUNT. */

struct captured_breakpoint_args
  {
    char *address;
    char *condition;
    int hardwareflag;
    int tempflag;
    int thread;
    int ignore_count;
  };

static int
do_captured_breakpoint (void *data)
{
  struct captured_breakpoint_args *args = data;
  struct symtabs_and_lines sals;
  int tmpnelts;
  register struct expression **cond;
  struct cleanup *old_chain;
  struct cleanup *breakpoint_chain = NULL;
  int i;
  char **addr_string;
  char **cond_string;

  char *address_end;

  /* Parse the source and lines spec.  Delay check that the expression
     didn't contain trailing garbage until after cleanups are in
     place. */
  sals.sals = NULL;
  sals.nelts = 0;
  sals.is_file_and_line = 0;
  address_end = args->address;
  addr_string = NULL;
  if (current_language->la_language != language_fortran)
    match_one_line_flag = 0;
  parse_breakpoint_sals (&address_end, &sals, &addr_string, USER_CHOICE);
  match_one_line_flag = 1;

  if (!sals.nelts)
    {
#ifdef SOLIB_ADD    
      /* RM: location may be in a shared library */
      if (! addr_string)
#endif
	return GDB_RC_NONE;
    }

#ifdef SOLIB_ADD    
  if (!sals.nelts)
    tmpnelts = 1;
  else
#endif
    tmpnelts = sals.nelts;

  /* Create a chain of things at always need to be cleaned up. */
  old_chain = make_cleanup (null_cleanup, 0);

  /* Always have a addr_string array, even if it is empty. */
  make_cleanup (free, addr_string);

  /* Make sure that all storage allocated to SALS gets freed.  */
  make_cleanup (free, sals.sals);

  /* Allocate space for all the cond expressions. */
  cond = xcalloc (tmpnelts, sizeof (struct expression *));
  make_cleanup (free, cond);

  /* Allocate space for all the cond strings. */
  cond_string = xcalloc (tmpnelts, sizeof (char **));
  make_cleanup (free, cond_string);

  /* ----------------------------- SNIP -----------------------------
     Anything added to the cleanup chain beyond this point is assumed
     to be part of a breakpoint.  If the breakpoint create goes
     through then that memory is not cleaned up. */
  breakpoint_chain = make_cleanup (null_cleanup, 0);

  /* Mark the contents of the addr_string for cleanup.  These go on
     the breakpoint_chain and only occure if the breakpoint create
     fails. */
  for (i = 0; i < tmpnelts; i++)
    {
      if (addr_string[i] != NULL)
	make_cleanup (free, addr_string[i]);
    }

  /* Wait until now before checking for garbage at the end of the
     address. That way cleanups can take care of freeing any
     memory. */
  if (*address_end != '\0')
    error ("Garbage %s following breakpoint address", address_end);

#ifdef SOLIB_ADD
  /* RM: location may be in a shared library */
  if (sals.nelts)
#endif
    /* Resolve all line numbers to PC's.  */
    breakpoint_sals_to_pc (&sals, args->address);

  /* Verify that conditions can be parsed, before setting any
     breakpoints.  */
  for (i = 0; i < tmpnelts; i++)
    {
      if (args->condition != NULL)
	{
	  char *tok = args->condition;
#ifdef SOLIB_ADD
	  if (sals.nelts)
	    {
#endif
	      cond[i] = parse_exp_1 (&tok, block_for_pc (sals.sals[i].pc), 0);
	      if (*tok != '\0')
		error ("Garbage %s follows condition", tok);
	      make_cleanup (free, cond[i]);
#ifdef SOLIB_ADD
	    }
	  else
	    {
	      cond[i] = NULL;
	    }
#endif
	  cond_string[i] = xstrdup (args->condition);
	}
    }

  create_breakpoints (sals, addr_string, cond, cond_string,
		      args->hardwareflag ? bp_hardware_breakpoint : bp_breakpoint,
		      args->tempflag ? del : donttouch,
		      args->thread, args->ignore_count, 0/*from-tty*/);

  /* That's it. Discard the cleanups for data inserted into the
     breakpoint. */
  discard_cleanups (breakpoint_chain);
  /* But cleanup everything else. */
  do_cleanups (old_chain);
  return GDB_RC_OK;
}

enum gdb_rc
gdb_breakpoint (char *address, char *condition,
		int hardwareflag, int tempflag,
		int thread, int ignore_count)
{
  struct captured_breakpoint_args args;
  args.address = address;
  args.condition = condition;
  args.hardwareflag = hardwareflag;
  args.tempflag = tempflag;
  args.thread = thread;
  args.ignore_count = ignore_count;
  return catch_errors (do_captured_breakpoint, &args,
		       NULL, RETURN_MASK_ALL);
}


static void
break_at_finish_at_depth_command_1 (char *arg, int flag, int from_tty)
{
  struct frame_info *frame = NULL;
  CORE_ADDR low, high, selected_pc = 0;
  char *extra_args = 0; /* initialize for compiler warning */
  char *level_arg, *addr_string; 
  int extra_args_len = 0, if_arg = 0;

  if (!arg ||
      (arg[0] == 'i' && arg[1] == 'f' && (arg[2] == ' ' || arg[2] == '\t')))
    {

      if (default_breakpoint_valid)
	{
	  if (selected_frame)
	    {
	      selected_pc = selected_frame->pc;
	      if (arg)
		if_arg = 1;
	    }
	  else
	    error ("No selected frame.");
	}
      else
	error ("No default breakpoint address now.");
    }
  else
    {
      extra_args = strchr (arg, ' ');
      if (extra_args)
	{
	  extra_args++;
	  extra_args_len = (int)strlen (extra_args);
	  level_arg = (char *) xmalloc (extra_args - arg);
	  strncpy (level_arg, arg, extra_args - arg - 1);
	  level_arg[extra_args - arg - 1] = '\0';
	}
      else
	{
	  level_arg = (char *) xmalloc (strlen (arg) + 1);
	  strcpy (level_arg, arg);
	}

      frame = parse_frame_specification (level_arg);
      if (frame)
	selected_pc = frame->pc;
      else
	selected_pc = 0;
    }
  if (if_arg)
    {
      extra_args = arg;
      extra_args_len = (int) strlen (arg);
    }

  if (selected_pc)
    {
      if (find_pc_partial_function (selected_pc, (char **) NULL, &low, &high))
	{
#ifdef HP_IA64
	  /* High is the first address past the end of the function, so
	     adjust high; if high is on a bundle boundary, it should be 
	     adjusted to the second 2nd slot of the the previous bundle; 
	     otherwise, it's adjusted to the previous slot */
          high -= (high % BUNDLE_SIZE) > 0 ? 1 : BUNDLE_SIZE - 2;
#endif
	  addr_string = (char *) xmalloc (26 + extra_args_len);
	  if (extra_args_len)
	    sprintf (addr_string, (sizeof (high) > 4) ?
	      "*0x%llx %s" : "*0x%lx %s", high, extra_args);
	  else
	    sprintf (addr_string, (sizeof (high) > 4) ?
	      "*0x%llx" : "*0x%lx", high);
	  break_command_1 (addr_string, flag, from_tty);
	  free (addr_string);
	}
      else
	error ("No function contains the specified address");
    }
  else
    error ("Unable to set breakpoint at procedure exit");
}


/* set_mult_exit_bp - Set a breakpoint on each line which is marked as
   an exit.  Return the number of breakpoints set.

   Arguments:

     extra_args
     flag, from_tty: These are passed to break_command_1 to set each breakpoint
		     along with a breakstring which specifies the address.
     low	     the lowest address of the procedure
     high	     the first address past the end of the procedure.
 */

int
set_mult_exit_bp (CORE_ADDR 	low, 
		  CORE_ADDR 	limit, 
		  char *	extra_args, 
		  int 	flag,
		  int 	from_tty)
{
    char * 		break_string;
    int			bp_set_count = 0;
    int			count;
    int 		extra_args_len;

    struct funcexit_linetable *	
			funcexit_linetab;
    CORE_ADDR		inst_addr;
    int 		len;

    register struct funcexit_linetable_entry* 
			line;
    struct symtab *	symtable;

    /* Use the low address to find the symtab which contains the pc and
       the funcexit line table for the pc.
       For each line in the funcexit line table, set a breakpoit at that line.
     */

    if (!(symtable = find_pc_sect_symtab (low, NULL)))
      return 0;
    extra_args_len = (extra_args ? (int) strlen (extra_args) : 0);

    /* Since we got the blockvector for the start of the procedure, it
       won't be nested in any other blocvectors.  symtable->next is NULL.
     */

    while (symtable->next)
      symtable = symtable->next;

    funcexit_linetab = FUNCEXIT_LINETABLE (symtable);
    if (!funcexit_linetab)
      {
#ifdef HP_IA64  /* JAGae99491 
		   Gdb is failing to read the actuals line table for objdebug.
		 */
	warning ("No exit lines found.  Only the return at the end of the procedure will get a breakpoint.");
#endif
	return 0;
      }

    len = funcexit_linetab->nitems;
    line = funcexit_linetab->item;	
    /* The first line does not represent a true statement, skip until
       we find one 
     */

    count = 0;
    break_string = (char *) xmalloc (extra_args_len + 26);
    while (count < len)
      {
	/* Set a breakpoint on before statement.  It is an exit. */
	
	inst_addr = SWIZZLE((line)->funcexit_line_pc);
	if (inst_addr >= low && inst_addr < limit)
	  {
	    if (extra_args_len)
	      if (sizeof (inst_addr) > sizeof(int))
		sprintf (break_string, "*0x%llx %s", inst_addr, extra_args);
	      else
		sprintf (break_string, "*0x%08llx %s", inst_addr, extra_args);
	    else if (sizeof (inst_addr) > sizeof(int))
	      sprintf (break_string, "*0x%llx", inst_addr);
	    else
	      sprintf (break_string, "*0x%08llx", inst_addr);
	    break_command_1 (break_string, flag, from_tty);
	    bp_set_count++;
	  }

	  line++;  count++;
      } /* for each line */
    free (break_string);
  return bp_set_count;
} /* end set_mult_exit_bp */


void
break_at_finish_command_1 (char *arg, int flag, int from_tty)
{
  char *symbol = arg;
  int bp_set_count = 0;
  char *addr_string, *break_string, *beg_addr_string;
  CORE_ADDR limit;
  CORE_ADDR low, high;
  int nbr_bp_set;
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  struct cleanup *old_chain;
  char *extra_args = 0;
  int extra_args_len = 0;
  int i, if_arg = 0;

  if (!arg ||
      (arg[0] == 'i' && arg[1] == 'f' && (arg[2] == ' ' || arg[2] == '\t')))
    {
      if (default_breakpoint_valid)
	{
	  if (selected_frame)
	    {
	      addr_string = (char *) xmalloc (strlen ("*0x")
					   + 16 /* 64-bit hex addr */  + 1);
	      if (sizeof (selected_frame->pc) > 4)
		sprintf (addr_string, "*0x%llx", selected_frame->pc);
	      else
		sprintf (addr_string, "*0x%08llx", selected_frame->pc);
	      if (arg)
		if_arg = 1;
	    }
	  else
	    error ("No selected frame.");
	}
      else
	error ("No default breakpoint address now.");
    }
  else
    {
      addr_string = (char *) xmalloc (strlen (arg) + 1);
      strcpy (addr_string, arg);
    }

  if (if_arg)
    {
      extra_args = arg;
      extra_args_len = (int) strlen (arg);
    }
  else if (arg)
    {
      /* get the stuff after the function name or address */
      extra_args = strchr (arg, ' ');
      if (extra_args)
	{
	  extra_args++;
	  extra_args_len = (int) strlen (extra_args);
	}
    }

  sals.sals = NULL;
  sals.nelts = 0;
  sals.is_file_and_line = 0;

  beg_addr_string = addr_string;
  sals = decode_line_1 (&addr_string, 1, (struct symtab *) NULL, 0,
			(char ***) NULL, USER_CHOICE);

  free (beg_addr_string);

  /* srikanth, 981001, CLLbs15582 decode_line_1 used not to return
   * if arg is bogus. However this has changed ...
   */
  if (sals.nelts == 0)
    {
      /*JAGae68084 For xbreak If the function is not defined
      then we will reset this flag*/
      xbreakall = 0;  
      error ("Function \"%s\" not defined.", symbol);
    }
  old_chain = make_cleanup (free, sals.sals);
  for (i = 0; (i < sals.nelts); i++)
    {
      sal = sals.sals[i];
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
       /* Right now xbreak on inline functions are not supported,
          so just continue.
       */
      if (inline_debugging  == BP_ALL || inline_debugging == BP_IND
          && sal.inline_idx)
        {
          continue;
        }
#endif
      if (find_pc_partial_function (sal.pc, (char **) NULL, &low, &high))
	{
	  limit = high;

	  /* See JAGaa80623.  This subracted 4 from high if there was no
	     symtab.  It turns out that we want to set the breakpoint
	     at high - INSTRUCTION_SIZE in any case, because high
	     is the first address past the end of the function.  This
	     is working on HPPA because there is a nop after the bv at
	     the end of the function, but we really should break on the
	     bv.  This is critical on architectures like IA64 which don't
	     have a delay slot.
	   */
#ifdef HP_IA64
	  /* See JAGad45388.  INSTRUCTION_SIZE is a bundle size, so if high
	     is on a bundle boundary, it should be adjusted to the second
	     2nd slot of the the previous bundle; otherwise, it's
	     adjusted to the previous slot */
	  high -= (high % BUNDLE_SIZE) > 0 ? 1 : BUNDLE_SIZE - 2;
#else
	  high -= INSTRUCTION_SIZE;
#endif
	  break_string = (char *) xmalloc (extra_args_len + 26);
	  if (extra_args_len)
	    sprintf (break_string, (sizeof (high) > 4) ?
	      "*0x%llx %s" : "*0x%lx %s", high, extra_args);
	  else
	    sprintf (break_string, (sizeof (high) > 4) ?
	      "*0x%llx" : "*0x%lx", high);

	  nbr_bp_set = set_mult_exit_bp (low, 
					 limit, 
					 extra_args, 
					 flag, 
					 from_tty);
	  if (nbr_bp_set)
	    bp_set_count+= nbr_bp_set;
	  else
	    {
	      break_command_1 (break_string, flag, from_tty);
	      bp_set_count++;
	    }

	  free (break_string);
	}
      else
	error ("No function contains the specified address");
    }
  if (bp_set_count > 1 && !procedure_flag)
    {
      printf_filtered ("Multiple breakpoints were set.\n");
      printf_filtered ("Use the \"delete\" command to delete unwanted breakpoints.\n");
    }
  do_cleanups (old_chain);
}


/* Helper function for break_command_1 and disassemble_command.  */

void
resolve_sal_pc (struct symtab_and_line *sal)
{
  struct symtab_and_line other_sal;
  CORE_ADDR pc;
  int line = sal->line;
  int inline_idx;

  if (sal->pc == 0 && sal->symtab != NULL)
    {
      if (!find_line_pc (sal->symtab, &line, &pc, &inline_idx))
	error ("No line %d in file \"%s\".",
	       sal->line,
               annotation_level > 1 ? symtab_to_filename (sal->symtab)
                                    : sal->symtab->filename);

      /* Warn if the line number is not the same. */
      if (line > sal->line)
	{
	  warning ("Line %d in file \"%s\" does not have "
		     "instructions. Placing breakpoint on the next available "
		     "source line.",
               sal->line,
               sal->symtab->filename,
	       line);
	}
      sal->pc = pc;
      sal->line = line;
      sal->inline_idx = inline_idx;
    }

  if (sal->section == 0 && sal->symtab != NULL)
    {
      struct blockvector *bv;
      struct block *b;
      struct symbol *sym;
      int index;

      bv = blockvector_for_pc_sect (sal->pc, 0, &index, sal->symtab);
      if (bv != NULL)
	{
	  b = BLOCKVECTOR_BLOCK (bv, index);
	  sym = block_function (b);
	  if (sym != NULL)
	    {
	      fixup_symbol_section (sym, sal->symtab->objfile);
	      sal->section = SYMBOL_BFD_SECTION (block_function (b));
	    }
         else
	    {
	      /* It really is worthwhile to have the section, so we'll just
	         have to look harder. This case can be executed if we have 
	         line numbers but no functions (as can happen in assembly 
	         source).  */

	      struct minimal_symbol *msym;

	      msym = lookup_minimal_symbol_by_pc (sal->pc);
	      if (msym)
		sal->section = SYMBOL_BFD_SECTION (msym);
	    }
	}
    }
}

void
break_command (char *arg, int from_tty)
{
  break_command_1 (arg, 0, from_tty);
}

/*JAGae68084 -This function is called when shared libraies are loaded.
  Places breakpoints for all the functions which the
  user has requested to be persistent*/

void
place_breakpoints_in_shlib()
{
  struct breakpoint *b;
 
  /* On re run if you see a deferred breakpoint ,
    then just leave as it will be resolved by re_enable_breakpoints_in_shllib*/

  ALL_BREAKPOINTS (b)
    {
      if (b->enable == shlib_disabled && !STRCMP(b->module_name,shllib_name)) 
        return;
    }  

  ALL_BREAKPOINTS (b)
    {
      if (b->breakall && !(b->enable == shlib_disabled))
         break_command_1(b->break_exp,0,0 );
      if (b->xbreakall && !(b->enable == shlib_disabled))
         break_at_finish_command(b->break_exp,0);
      if (b->rbreakall && !(b->enable == shlib_disabled))
        rbreak_all_command(input,0);
    }
}

/* JAGae68084 - Breakall_command, rbreakall_command and xbreakall_command
  places a persistent breakpoint , ie breakall foo, will
  place a breakpoint if the symbol is present in the current symtab,
  later it places breakpoints on foo for all the subsequent
  shared libraries which are loaded */

void 
breakall_command(char *arg, int from_tty)
{
  if (isdigit(*arg))
    error("Please specify a function name");
  if_yes_to_all_commands = query("Make breakpoint pending on future shared library load?");
  if (if_yes_to_all_commands)
    {
      breakall = 1;
      input = arg;
    }
  break_command_1(arg,0,from_tty);
  breakall=0;
}

void
xbreakall_command(char *arg, int from_tty)
{
  if (isdigit(*arg))
    error("Please specify a function name");
  if_yes_to_all_commands = query("Make breakpoint pending on future shared library load?");
  if (if_yes_to_all_commands)
    {
     xbreakall = 1;
     input = arg;
    }
  break_at_finish_command(arg,from_tty);
  xbreakall=0;
}

void 
rbreakall_command(char *arg, int from_tty)
{
  if (isdigit(*arg))
    error("Please specify a function name");
  if_yes_to_all_commands = query("Make breakpoint pending on future shared library load?");
  if (if_yes_to_all_commands)
    {
     rbreakall = 1;
     input = arg;
    }
  rbreak_all_command(arg,from_tty);
  rbreakall=0;
}


static void
break_at_finish_command (char *arg, int from_tty)
{
  break_at_finish_command_1 (arg, 0, from_tty);
}

static void
break_at_finish_at_depth_command (char *arg, int from_tty)
{
  break_at_finish_at_depth_command_1 (arg, 0, from_tty);
}

void
tbreak_command (char *arg, int from_tty)
{
  break_command_1 (arg, BP_TEMPFLAG, from_tty);
}

static void
tbreak_at_finish_command (char *arg, int from_tty)
{
  break_at_finish_command_1 (arg, BP_TEMPFLAG, from_tty);
}

static void
hbreak_command (char *arg, int from_tty)
{
  break_command_1 (arg, BP_HARDWAREFLAG, from_tty);
}

static void
thbreak_command (char *arg, int from_tty)
{
  break_command_1 (arg, (BP_TEMPFLAG | BP_HARDWAREFLAG), from_tty);
}

static void
stop_command (char *arg, int from_tty)
{
  /* JAGaf54451 - breakpoint not triggered using <stop in/at> command in non-dbx mode. 
     If the stop command has a hook defined , the help message need not be displayed. */
  if (!stop_hook_command->hook)
    { 
       printf_filtered ("Specify the type of breakpoint to set.\n\
Usage: stop in <function | address>\n\
       stop at <line>\n\
Hint : You can define hook-stop to set a list of commands to \n\
       be run each time execution of the program stops.\n");
    }
  /* JAGaf54451 - END */  
}

static void
stopin_command (char *arg, int from_tty)
{
  int badInput = 0;

  if (arg == (char *) NULL)
    badInput = 1;
  else if (*arg != '*')
    {
      char *argptr = arg;
      int hasColon = 0;

      /* 
         ** look for a ':'.  If this is a line number specification, then say
         ** it is bad, otherwise, it should be an address or function/method name
       */
      while (*argptr && !hasColon)
	{
	  hasColon = (*argptr == ':');
	  argptr++;
	}

      if (hasColon)
	badInput = (*argptr != ':');	/* Not a class::method */
      else
	badInput = isdigit (*arg);	/* a simple line number */
    }

  if (badInput)
    printf_filtered ("Usage: stop in <function | address>\n");
  else
    break_command_1 (arg, 0, from_tty);
}

static void
stopat_command (char *arg, int from_tty)
{
  int badInput = 0;

  if (arg == (char *) NULL || *arg == '*')	/* no line number */
    badInput = 1;
  else
    {
      char *argptr = arg;
      int hasColon = 0;

      /* 
         ** look for a ':'.  If there is a '::' then get out, otherwise
         ** it is probably a line number.
       */
      while (*argptr && !hasColon)
	{
	  hasColon = (*argptr == ':');
	  argptr++;
	}

      if (hasColon)
	badInput = (*argptr == ':');	/* we have class::method */
      else
	badInput = !isdigit (*arg);	/* not a line number */
    }

  if (badInput)
    printf_filtered ("Usage: stop at <line>\n");
  else
    break_command_1 (arg, 0, from_tty);
}

/* ARGSUSED */
/* accessflag:  0: watch write, 1: watch read, 2: watch access(read or write)
 */
static void
watch_command_1 (char *arg, int accessflag, int from_tty)
{
  struct breakpoint *b;
  struct symtab_and_line sal;
  struct expression *exp;
  struct block *exp_valid_block;
  struct value *val, *mark;
  struct frame_info *frame;
  struct frame_info *prev_frame = NULL;
  char *exp_start = NULL;
  char *exp_end = NULL;
  char *tok, *end_tok;
  int toklen;
  char *cond_start = NULL;
  char *cond_end = NULL;
  struct expression *cond = NULL;
  int i, other_type_used, target_resources_ok = 0;
  enum bptype bp_type;
  int mem_cnt = 0;
  args_for_evaluate_expression args;

  INIT_SAL (&sal);		/* initialize to zeroes */

  /* Parse arguments.  */
  innermost_block = NULL;
  exp_start = arg;
  exp = parse_exp_1 (&arg, 0, 0);
  exp_end = arg;
  exp_valid_block = innermost_block;
  mark = value_mark ();
  /* QXCR1000581688: Ajey: Calling all_values_to_null()
     after call to parse_exp_1(), to avoid appending of values
     which will get create in evaluate_expression()
     to 'all_values' list.
     To point 'all_values' to NULL before calling evaluate_expression()
     to avoid an incorrect search and mark is holding the value of 
     all_values. mark is assigned to all_values at the end of the procedure.
  */
  all_values_to_null ();

  args.exp = exp;
  args.valp = NULL;
  donot_print_errors = 1;
  if (!catch_errors ((catch_errors_ftype *) evaluate_expression_1,
			   (char *) &args,
			   NULL, RETURN_MASK_ALL))
    {
      /* Place a deferred watchpoint. */
      donot_print_errors = 0;
      printf_filtered ("Cannot evaluate expression, placing a deferred watchpoint.\n");
      if (!sbrk_breakpoint_set)
	CREATE_SBRK_BREAKPOINTS;
      val = NULL;
    }
  else 
    {
      val = args.valp;
      release_value (val);
      if (VALUE_LAZY (args.valp))
		  if (!catch_errors ((catch_errors_ftype *) value_fetch_lazy_1,
				(char*) args.valp,
				NULL, RETURN_MASK_ALL))
	{
	  donot_print_errors = 0;
          printf_filtered ("Cannot evaluate expression, placing a deferred watchpoint.\n");
	  if (!sbrk_breakpoint_set)
	    CREATE_SBRK_BREAKPOINTS;
          val = NULL;
	}
      donot_print_errors = 0;
    }

  tok = arg;
  while (*tok == ' ' || *tok == '\t')
    tok++;
  end_tok = tok;

  while (*end_tok != ' ' && *end_tok != '\t' && *end_tok != '\000')
    end_tok++;

  toklen = (int)(end_tok - tok);
  if (toklen >= 1 && strncmp (tok, "if", toklen) == 0)
    {
      tok = cond_start = end_tok + 1;
      cond = parse_exp_1 (&tok, 0, 0);
      cond_end = tok;
    }
  if (*tok)
    error ("Junk at end of command.");

  if (accessflag == 1)
    bp_type = bp_read_watchpoint;
  else if (accessflag == 2)
    bp_type = bp_access_watchpoint;
  else
    bp_type = bp_hardware_watchpoint;

  mem_cnt = can_use_hardware_watchpoint (val);
  if (mem_cnt == 0 && bp_type != bp_hardware_watchpoint)
    error ("Expression cannot be implemented with read/access watchpoint.");
  if (mem_cnt != 0)
    {
      i = hw_watchpoint_used_count (bp_type, &other_type_used);
      target_resources_ok = TARGET_CAN_USE_HARDWARE_WATCHPOINT (
				     bp_type, i + mem_cnt, other_type_used);
      if (target_resources_ok == 0 && bp_type != bp_hardware_watchpoint)
	error ("Target does not support this type of hardware watchpoint.");
      if (target_resources_ok < 0 && bp_type != bp_hardware_watchpoint)
	error ("Target resources have been allocated for other types of watchpoints.");
    }

  /* ??rehrauer: DTS #CHFts23014 notes that on HP-UX if you set a h/w
     watchpoint before the "run" command, the inferior dies with a e.g.,
     SIGILL once you start it.  I initially believed this was due to a
     bad interaction between page protection traps and the initial
     startup sequence by the dynamic linker.

     However, I tried avoiding that by having HP-UX's implementation of
     TARGET_CAN_USE_HW_WATCHPOINT return FALSE if there was no inferior_pid
     yet, which forced slow watches before a "run" or "attach", and it
     still fails somewhere in the startup code.

     Until I figure out what's happening, I'm disallowing watches altogether
     before the "run" or "attach" command.  We'll tell the user they must
     set watches after getting the program started.
   */
  if (!target_has_execution)
    {
      warning ("can't do that without a running program; try \"break main\", \"run\" first");
      return;
    }


  /* Now set up the breakpoint.  */
  b = set_raw_breakpoint (sal);
  set_breakpoint_count (breakpoint_count + 1);
  b->number = breakpoint_count;
  b->disposition = donttouch;
  b->exp = exp;
  b->exp_valid_block = exp_valid_block;
  b->exp_string = savestring (exp_start, (int)(exp_end - exp_start));
  b->val = val;
  if (!val)
    b->enable = heap_disabled;
  b->cond = cond;
  if (cond_start)
    b->cond_string = savestring (cond_start, (int)(cond_end - cond_start));
  else
    b->cond_string = 0;

  frame = block_innermost_frame (exp_valid_block);
  if (frame)
    {
      prev_frame = get_prev_frame (frame);
#ifdef REGISTER_STACK_ENGINE_FP
      b->watchpoint_frame = frame->rse_fp;
#else
      b->watchpoint_frame = frame->frame;
#endif
    }
  else
    b->watchpoint_frame = (CORE_ADDR) 0;

  /* RM: It's not useful to use hardware watchpoints for "local"
   * expressions, since local variables are on the stack, and we
   * write to the stack all the time.
   */
  if ((mem_cnt && target_resources_ok > 0) && (!frame))
    b->type = bp_type;
  else
    b->type = bp_watchpoint;

  /* If the expression is "local", then set up a "watchpoint scope"
     breakpoint at the point where we've left the scope of the watchpoint
     expression.  */
  if (innermost_block)
    {
      if (prev_frame)
	{
	  struct breakpoint *scope_breakpoint;
	  struct symtab_and_line scope_sal;

	  INIT_SAL (&scope_sal);	/* initialize to zeroes */
	  scope_sal.pc = get_frame_pc (prev_frame);
          scope_sal.pc = SWIZZLE(scope_sal.pc);
	  scope_sal.section = find_pc_overlay (scope_sal.pc);

	  scope_breakpoint = set_raw_breakpoint (scope_sal);
	  set_breakpoint_count (breakpoint_count + 1);
	  scope_breakpoint->number = breakpoint_count;

	  scope_breakpoint->type = bp_watchpoint_scope;
	  scope_breakpoint->enable = enabled;

	  /* Automatically delete the breakpoint when it hits.  */
	  scope_breakpoint->disposition = del;

	  /* Only break in the proper frame (help with recursion).  */
	  scope_breakpoint->frame = prev_frame->frame;
#ifdef REGISTER_STACK_ENGINE_FP
	  scope_breakpoint->rse_fp = prev_frame->rse_fp;
#endif

	  /* Set the address at which we will stop.  */
	  scope_breakpoint->address = get_frame_pc (prev_frame);

	  /* The scope breakpoint is related to the watchpoint.  We
	     will need to act on them together.  */
	  b->related_breakpoint = scope_breakpoint;
	}
    }
  /* QXCR1000581688: Ajey: Call free_all_values() to free the values 
     created by evaluate_expression() before pointing 'all_values'
     to previous value chain by calling all_values_to_mark (mark).
     Assign the value of mark to all_values so that values created
     before call to evaluate_expression() are freed up properly.
  */
  free_all_values ();
  all_values_to_mark (mark);
  mention (b);
}

/* Return count of locations need to be watched and can be handled
   in hardware.  If the watchpoint can not be handled
   in hardware return zero.  */

#if !defined(TARGET_REGION_SIZE_OK_FOR_HW_WATCHPOINT)
#define TARGET_REGION_SIZE_OK_FOR_HW_WATCHPOINT(byte_size) \
    ((byte_size) <= (REGISTER_SIZE))
#endif

#if !defined(TARGET_REGION_OK_FOR_HW_WATCHPOINT)
#define TARGET_REGION_OK_FOR_HW_WATCHPOINT(ADDR,LEN) \
     TARGET_REGION_SIZE_OK_FOR_HW_WATCHPOINT(LEN)
#endif

static int
can_use_hardware_watchpoint (struct value *v)
{
  int found_memory_cnt = 0;
  struct value *head = v;

  /* Did the user specifically forbid us to use hardware watchpoints? */
  if (!can_use_hw_watchpoints)
    return 0;

   /* Make sure that the value of the expression depends only upon
     memory contents, and values computed from them within GDB.  If we
     find any register references or function calls, we can't use a
     hardware watchpoint.

     The idea here is that evaluating an expression generates a series
     of values, one holding the value of every subexpression.  (The
     expression a*b+c has five subexpressions: a, b, a*b, c, and
     a*b+c.)  GDB's values hold almost enough information to establish
     the criteria given above --- they identify memory lvalues,
     register lvalues, computed values, etcetera.  So we can evaluate
     the expression, and then scan the chain of values that leaves
     behind to decide whether we can detect any possible change to the
     expression's final value using only hardware watchpoints.

     However, I don't think that the values returned by inferior
     function calls are special in any way.  So this function may not
     notice that an expression involving an inferior function call
     can't be watched with hardware watchpoints.  FIXME.  */
  for (; v; v = v->next)
    {
      if (VALUE_LVAL (v) == lval_memory)
	{
	  if (VALUE_LAZY (v))
	    /* A lazy memory lvalue is one that GDB never needed to fetch;
	      we either just used its address (e.g., `a' in `a.b') or
	      we never needed it at all (e.g., `a' in `a,b').  */
	      ;
	  else
	    {
	      /* Ahh, memory we actually used!  Check if we can cover
                 it with hardware watchpoints.  */
	      struct type *vtype = check_typedef (VALUE_TYPE (v));

	      /* We only watch structs and arrays if user asked for it
		 explicitly, never if they just happen to appear in a
		 middle of some value chain.  */
	      if (v == head
		  || (TYPE_CODE (vtype) != TYPE_CODE_STRUCT
	        && TYPE_CODE (vtype) != TYPE_CODE_ARRAY))
		{
		  CORE_ADDR vaddr = VALUE_ADDRESS (v) + VALUE_OFFSET (v);
		  int       len   = TYPE_LENGTH (VALUE_TYPE (v));

		  if (!TARGET_REGION_OK_FOR_HW_WATCHPOINT (vaddr, len))
		    return 0;
		  else
		    found_memory_cnt++;
		}
	    }
	}
      else if (v->lval != not_lval && v->modifiable == 0)
	return 0;       /* ??? What does this represent? */
      else if (v->lval == lval_register)
	return 0;       /* cannot watch a register with a HW watchpoint */
    }

  /* The expression itself looks suitable for using a hardware
     watchpoint, but give the target machine a chance to reject it.  */
  return found_memory_cnt;
}

#ifdef UI_OUT
void
watch_command_wrapper (char *arg, int from_tty)
{
  watch_command (arg, from_tty);
}
#endif
static void 
watch_command (char *arg, int from_tty)
{
  watch_command_1 (arg, 0, from_tty);
}

/* Bindu 021005
   Watch_target commad.
	(gdb) watch_target current_frame
   Equivalent to executing:
	(gdb) print current_frame
   	$1 = (struct frame_info *) 0x7fdf78
	(gdb) watch *(struct frame_info *) 0x7fdf78
 */
static void
watch_target_command (char *arg, int from_tty)
{
  struct expression *exp;
  struct value *val = NULL, *mark;
  args_for_evaluate_expression args_eval;
  char* exp_end;
  char* new_arg;
  char* the_val;
  struct ui_file *stb;
  struct cleanup *old_chain;
  long length;

  int found_an_addr = 0;
  char *arg_dup, *arg_dup_free;

  arg_dup = xstrdup (arg);
  arg_dup_free = arg_dup;  /* Remember to free */
  
  /* Check whether an expression is an address or a variable.
     If an address is prefixed with "*" and followed by 
     a type "(type *)" and if an address is in hex/octal/
     binary/decimal formats, then set found_an_addr to 1. 
     otherwise treat it as a variable and follow up with 1st 
     parsing to fetch address of the variable.
  */

  if ((arg_dup = strchr (arg_dup, '*')) && (arg_dup = strchr (arg_dup, ')'))
       && !found_an_addr)
    { 
      /* arg_dup is pointing at last ")" just before an address 
         in the expression.
      */

      while (*arg_dup != '\0' && !found_an_addr)
       {
         char *hex_check, *decimal_check, *binary_check, *octal_check;
         hex_check = decimal_check = binary_check = octal_check = arg_dup;

         /* Next check is for hex format */ 
         if (*hex_check == '0' && (*(hex_check+1) == 'x' || *(hex_check+1)
              == 'X'))
	   { 
             hex_check += 2; /* skip "0x" or "0X" */
	     while (isxdigit ((int) *hex_check) && *arg_dup != '\0')
	      {
	        if (*(hex_check + 1) == '\0')
	        found_an_addr = 1;
	        hex_check++;
  	      }
	   }
	 
	 /* Next check is for octal format */
         if (*octal_check == '0')
	   {
	     while (*octal_check >= '0' &&  *octal_check <= '7')
	      {
	        if (*(octal_check + 1) == '\0')
	        found_an_addr = 1;
	        octal_check++;
	      }
	   }

	 /* Next check is for binary format */
	 if (*binary_check == '0' ||
	     *binary_check == '1')
    	   {
	     while ((*binary_check == '0' || 
	             *binary_check == '1')
	             && *binary_check != '\0')
	      {
	        if (*(binary_check + 1) == '\0')
	        found_an_addr = 1;
	        binary_check++;
	      }
	   }

	 /* Next check is for decimal format*/
	 if (isdigit ((int) *decimal_check))
           {
             while (isdigit ((int)*decimal_check)
                             && *decimal_check != '\0')
              {
                if (*(decimal_check + 1) == '\0')
                found_an_addr = 1;
                decimal_check++;
              }
           }
          arg_dup++; /* Increment and reassign */
	}
    }
  if (found_an_addr)
    {
      new_arg = (char*) alloca (strlen (arg) + 2);
      sprintf (new_arg, "*%s", arg);
      watch_command_1 (new_arg, 0, from_tty);
      free (arg_dup_free);
      return;	
    }
  free (arg_dup_free);
 
  /* Prase the expression and save the string after the expression in
     exp_end. For example watch_target exp if (a==b), "if (a==b)" will
     get into exp_end. 
  */  
  exp = parse_exp_1 (&arg, 0, 0);
  exp_end = arg;

  /* Evaluate this expression and fetch the value. */
  mark = value_mark ();
  args_eval.exp = exp;
  args_eval.valp = NULL;
  if (!catch_errors ((catch_errors_ftype *) evaluate_expression_1,
                           (char *) &args_eval,
                           NULL, RETURN_MASK_ALL))
    {
      error ("Cannot evaluate expression"); 
    }
  else 
    {
      val = args_eval.valp;
      release_value (val);
      if (VALUE_LAZY (args_eval.valp))
                  if (!catch_errors ((catch_errors_ftype *) value_fetch_lazy_1,
                                (char*) args_eval.valp,
                                NULL, RETURN_MASK_ALL))
        {
          error ("Cannot evaluate expression"); 
        }
    }

  /* Print the value into the buffer the_val. */
  stb = mem_fileopen ();
  old_chain = make_cleanup_ui_file_delete (stb);

  value_print (val, stb, 0, Val_pretty_default);
  the_val = ui_file_xstrdup (stb, &length);
  make_cleanup (free, the_val);
  
  value_release_to_mark (mark);

  /* Now create a new arg with *(value printed) if (a==b) */
  new_arg = (char*) alloca (length + strlen (exp_end) + 2);
  sprintf (new_arg, "*%s %s", the_val, exp_end);

  /* Execute the normal watch_command with new_arg. */
  watch_command_1 (new_arg, 0, from_tty);

  do_cleanups (old_chain);
}

#ifdef UI_OUT
void
rwatch_command_wrapper (char *arg, int from_tty)
{
  rwatch_command (arg, from_tty);
}
#endif
static void 
rwatch_command (char *arg, int from_tty)
{
  watch_command_1 (arg, 1, from_tty);
}


#ifdef UI_OUT
void
awatch_command_wrapper (char *arg, int from_tty)
{
  awatch_command (arg, from_tty);
}
#endif
static void 
awatch_command (char *arg, int from_tty)
{
  watch_command_1 (arg, 2, from_tty);
}


/* Helper routine for the until_command routine in infcmd.c.  Here
   because it uses the mechanisms of breakpoints.  */

/* This function is called by fetch_inferior_event via the
   cmd_continuation pointer, to complete the until command. It takes
   care of cleaning up the temporary breakpoints set up by the until
   command. */
static void
until_break_command_continuation (struct continuation_arg *arg)
{
  struct cleanup *cleanups;

  cleanups = (struct cleanup *) arg->data.pointer;
  do_exec_cleanups (cleanups);
}

/* ARGSUSED */
void
until_break_command (char *arg, int from_tty)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  struct frame_info *prev_frame = get_prev_frame (selected_frame);
  struct breakpoint *breakpoint;
  struct cleanup *old_chain;
  struct continuation_arg *arg1;

  clear_proceed_status ();

  /* Set a breakpoint where the user wants it and at return from
     this function */

  if (default_breakpoint_valid)
    sals = decode_line_1 (&arg, 1, default_breakpoint_symtab,
			  default_breakpoint_line, (char ***) NULL,
			  USER_CHOICE);
  else
    sals = decode_line_1 (&arg, 1, (struct symtab *) NULL, 0, (char ***) NULL,
			  USER_CHOICE);

  if (sals.nelts != 1)
    error ("Couldn't get information on specified line.");

  sal = sals.sals[0];
  free ((PTR) sals.sals);	/* malloc'd, so freed */

  if (*arg)
    error ("Junk at end of arguments.");

  resolve_sal_pc (&sal);

  breakpoint = set_momentary_breakpoint (sal, selected_frame, bp_until);

  old_chain = make_cleanup ((make_cleanup_ftype *) delete_breakpoint, 
			    breakpoint);
     /* If we are running asynchronously, and the target supports async
     execution, we are not waiting for the target to stop, in the call
     tp proceed, below. This means that we cannot delete the
     brekpoints until the target has actually stopped. The only place
     where we get a chance to do that is in fetch_inferior_event, so
     we must set things up for that. */

  if (event_loop_p && target_can_async_p ())
    {
      /* In this case the arg for the continuation is just the point
         in the exec_cleanups chain from where to start doing
         cleanups, because all the continuation does is the cleanups in
         the exec_cleanup_chain. */
      arg1 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg1->next         = NULL;
      arg1->data.pointer = old_chain;

      add_continuation (until_break_command_continuation, arg1);
    }
  /* Keep within the current frame */

  if (prev_frame)
    {
      sal = find_pc_line (prev_frame->pc, 0);
      sal.pc = prev_frame->pc;
      breakpoint = set_momentary_breakpoint (sal, prev_frame, bp_until);
      make_cleanup ((make_cleanup_ftype *) delete_breakpoint, breakpoint);
    }

  proceed (((CORE_ADDR)(long) -1), TARGET_SIGNAL_DEFAULT, 0);
  /* Do the cleanups now, only if we are not running asynchronously,
     or if we are, but the target is still synchronous. */
  if (!event_loop_p || !target_can_async_p ())
    do_cleanups (old_chain);
}


struct sal_chain
{
  struct sal_chain *next;
  struct symtab_and_line sal;
};

/* This shares a lot of code with `print_frame_label_vars' from stack.c.  */

static struct symtabs_and_lines
get_catch_sals (int this_level_only)
{
  register struct blockvector *bl;
  register struct block *block;
  int index, have_default = 0;
  CORE_ADDR pc;
  struct symtabs_and_lines sals;
  struct sal_chain *sal_chain = 0;
  char *blocks_searched;

  /* Not sure whether an error message is always the correct response,
     but it's better than a core dump.  */
  if (selected_frame == NULL)
    error ("No selected frame.");
  block = get_frame_block (selected_frame);
  pc = selected_frame->pc;

  sals.nelts = 0;
  sals.is_file_and_line = 0;
  sals.sals = NULL;

  if (block == 0)
    error ("No symbol table info available.\n");

  bl = blockvector_for_pc (BLOCK_END (block) - 4, &index);
  blocks_searched = (char *) alloca (BLOCKVECTOR_NBLOCKS (bl) * sizeof (char));
  memset (blocks_searched, 0, BLOCKVECTOR_NBLOCKS (bl) * sizeof (char));

  while (block != 0)
    {
      CORE_ADDR end = BLOCK_END (block) - 4;
      int last_index;

      if (bl != blockvector_for_pc (end, &index))
	error ("blockvector blotch");
      if (BLOCKVECTOR_BLOCK (bl, index) != block)
	error ("blockvector botch");
      last_index = BLOCKVECTOR_NBLOCKS (bl);
      index += 1;

      /* Don't print out blocks that have gone by.  */
      while (index < last_index
	     && BLOCK_END (BLOCKVECTOR_BLOCK (bl, index)) < pc)
	index++;

      while (index < last_index
	     && BLOCK_END (BLOCKVECTOR_BLOCK (bl, index)) < end)
	{
	  if (blocks_searched[index] == 0)
	    {
	      struct block *b = BLOCKVECTOR_BLOCK (bl, index);
	      int nsyms;
	      register int i;
	      register struct symbol *sym;

	      nsyms = BLOCK_NSYMS (b);

	      for (i = 0; i < nsyms; i++)
		{
		  sym = BLOCK_SYM (b, i);
		  if (STREQ (SYMBOL_NAME (sym), "default"))
		    {
		      if (have_default)
			continue;
		      have_default = 1;
		    }
		  if (SYMBOL_CLASS (sym) == LOC_LABEL)
		    {
		      struct sal_chain *next = (struct sal_chain *)
		      alloca (sizeof (struct sal_chain));
		      next->next = sal_chain;
		      next->sal = find_pc_line (SYMBOL_VALUE_ADDRESS (sym), 0);
		      sal_chain = next;
		    }
		}
	      blocks_searched[index] = 1;
	    }
	  index++;
	}
      if (have_default)
	break;
      if (sal_chain && this_level_only)
	break;

      /* After handling the function's top-level block, stop.
         Don't continue to its superblock, the block of
         per-file symbols.  */
      if (BLOCK_FUNCTION (block))
	break;
      block = BLOCK_SUPERBLOCK (block);
    }

  if (sal_chain)
    {
      struct sal_chain *tmp_chain;

      /* Count the number of entries.  */
      for (index = 0, tmp_chain = sal_chain; tmp_chain;
	   tmp_chain = tmp_chain->next)
	index++;

      sals.nelts = index;
      sals.sals = (struct symtab_and_line *)
	xmalloc (index * sizeof (struct symtab_and_line));
      for (index = 0; sal_chain; sal_chain = sal_chain->next, index++)
	sals.sals[index] = sal_chain->sal;
    }

  return sals;
}

static void
ep_skip_leading_whitespace (char **s)
{
  if ((s == NULL) || (*s == NULL))
    return;
  while (isspace (**s))
    *s += 1;
}

/* This function examines a string, and attempts to find a token
   that might be an event name in the leading characters.  If a
   possible match is found, a pointer to the last character of
   the token is returned.  Else, NULL is returned.
 */
static char *
ep_find_event_name_end (char *arg)
{
  char *s = arg;
  char *event_name_end = NULL;

  /* If we could depend upon the presense of strrpbrk, we'd use that... */
  if (arg == NULL)
    return NULL;

  /* We break out of the loop when we find a token delimiter.
     Basically, we're looking for alphanumerics and underscores;
     anything else delimites the token.
   */
  while (*s != '\0')
    {
      if (!isalnum (*s) && (*s != '_'))
	break;
      event_name_end = s;
      s++;
    }

  return event_name_end;
}


/* This function attempts to parse an optional "if <cond>" clause
   from the arg string.  If one is not found, it returns NULL.

   Else, it returns a pointer to the condition string.  (It does not
   attempt to evaluate the string against a particular block.)  And,
   it updates arg to point to the first character following the parsed
   if clause in the arg string.
 */
static char *
ep_parse_optional_if_clause (char **arg)
{
  char *cond_string;

  if (((*arg)[0] != 'i') || ((*arg)[1] != 'f') || !isspace ((*arg)[2]))
    return NULL;

  /* Skip the "if" keyword. */
  (*arg) += 2;

  /* Skip any extra leading whitespace, and record the start of the
     condition string.
   */
  ep_skip_leading_whitespace (arg);
  cond_string = *arg;

  /* Assume that the condition occupies the remainder of the arg string. */
  (*arg) += strlen (cond_string);

  return cond_string;
}

/* This function attempts to parse an optional filename from the arg
   string.  If one is not found, it returns NULL.

   Else, it returns a pointer to the parsed filename.  (This function
   makes no attempt to verify that a file of that name exists, or is
   accessible.)  And, it updates arg to point to the first character
   following the parsed filename in the arg string.

   Note that clients needing to preserve the returned filename for
   future access should copy it to their own buffers.
 */
static char *
ep_parse_optional_filename (char **arg)
{
  static char filename[1024];
  char *arg_p = *arg;
  int i;
  char c;

  if ((*arg_p == '\0') || isspace (*arg_p))
    return NULL;

  for (i = 0;; i++)
    {
      c = *arg_p;
      if (isspace (c))
	c = '\0';
      filename[i] = c;
      if (c == '\0')
	break;
      arg_p++;
    }
  *arg = arg_p;

  return filename;
}

/* Commands to deal with catching events, such as signals, exceptions,
   process start/exit, etc.  */

typedef enum
{
  catch_fork, catch_vfork
}
catch_fork_kind;

#if defined(CHILD_INSERT_FORK_CATCHPOINT) || defined(CHILD_INSERT_VFORK_CATCHPOINT)
static void catch_fork_command_1 (catch_fork_kind fork_kind,
				  char *arg, int tempflag, int from_tty);

static void
catch_fork_command_1 (catch_fork_kind fork_kind, char *arg, int tempflag, int from_tty)
{
  char *cond_string = NULL;

  ep_skip_leading_whitespace (&arg);

  /* The allowed syntax is:
     catch [v]fork
     catch [v]fork if <cond>

     First, check if there's an if clause.
   */
  cond_string = ep_parse_optional_if_clause (&arg);

  if ((*arg != '\0') && !isspace (*arg))
    error ("Junk at end of arguments.");

  /* If this target supports it, create a fork or vfork catchpoint
     and enable reporting of such events.
   */
  switch (fork_kind)
    {
    case catch_fork:
      create_fork_event_catchpoint (tempflag, cond_string);
      break;
    case catch_vfork:
      create_vfork_event_catchpoint (tempflag, cond_string);
      break;
    default:
      error ("unsupported or unknown fork kind; cannot catch it");
      break;
    }
}
#endif

#if defined(CHILD_INSERT_EXEC_CATCHPOINT)
static void
catch_exec_command_1 (char *arg, int tempflag, int from_tty)
{
  char *cond_string = NULL;

  ep_skip_leading_whitespace (&arg);

  /* The allowed syntax is:
     catch exec
     catch exec if <cond>

     First, check if there's an if clause. */
  cond_string = ep_parse_optional_if_clause (&arg);

  if ((*arg != '\0') && !isspace (*arg))
    error ("Junk at end of arguments.");

  /* If this target supports it, create an exec catchpoint
     and enable reporting of such events. */
  create_exec_event_catchpoint (tempflag, cond_string);
}
#endif

#if defined(SOLIB_ADD)
static void
catch_load_command_1 (char *arg, int tempflag, int from_tty)
{
  char *dll_pathname = NULL;
  char *cond_string = NULL;

  ep_skip_leading_whitespace (&arg);

  /* The allowed syntax is:
     catch load
     catch load if <cond>
     catch load <filename>
     catch load <filename> if <cond>

     The user is not allowed to specify the <filename> after an
     if clause.

     We'll ignore the pathological case of a file named "if".

     First, check if there's an if clause.  If so, then there
     cannot be a filename.
   */
  cond_string = ep_parse_optional_if_clause (&arg);

  /* If there was an if clause, then there cannot be a filename.

     Else, there might be a filename and an if clause.
   */
  if (cond_string == NULL)
    {
      dll_pathname = ep_parse_optional_filename (&arg);
      ep_skip_leading_whitespace (&arg);
      cond_string = ep_parse_optional_if_clause (&arg);
    }

  if ((*arg != '\0') && !isspace (*arg))
    error ("Junk at end of arguments.");

  /* Create a load breakpoint that only triggers when a load of
     the specified dll (or any dll, if no pathname was specified)
     occurs.
   */
  SOLIB_CREATE_CATCH_LOAD_HOOK (inferior_pid, tempflag, dll_pathname, cond_string);
}

static void
catch_unload_command_1 (char *arg, int tempflag, int from_tty)
{
  char *dll_pathname = NULL;
  char *cond_string = NULL;

  ep_skip_leading_whitespace (&arg);

  /* The allowed syntax is:
     catch unload
     catch unload if <cond>
     catch unload <filename>
     catch unload <filename> if <cond>

     The user is not allowed to specify the <filename> after an
     if clause.

     We'll ignore the pathological case of a file named "if".

     First, check if there's an if clause.  If so, then there
     cannot be a filename.
   */
  cond_string = ep_parse_optional_if_clause (&arg);

  /* If there was an if clause, then there cannot be a filename.

     Else, there might be a filename and an if clause.
   */
  if (cond_string == NULL)
    {
      dll_pathname = ep_parse_optional_filename (&arg);
      ep_skip_leading_whitespace (&arg);
      cond_string = ep_parse_optional_if_clause (&arg);
    }

  if ((*arg != '\0') && !isspace (*arg))
    error ("Junk at end of arguments.");

  /* Create an unload breakpoint that only triggers when an unload of
     the specified dll (or any dll, if no pathname was specified)
     occurs.
   */
  SOLIB_CREATE_CATCH_UNLOAD_HOOK (inferior_pid, tempflag, dll_pathname, cond_string);
}

static void
catch_nomem_command_1 (char *arg, int tempflag, int from_tty)
{
  int saved_inferior_pid = inferior_pid;
  tempflag |= (BP_NOMEMFLAG | BP_MATCH_ONE);
  break_command_1 ("__rtc_nomem_event", tempflag, from_tty);
  set_rtc_catch_nomem();
  inferior_pid = saved_inferior_pid;
}
#endif /* SOLIB_ADD */

/* Commands to deal with catching exceptions.  */

/* Set a breakpoint at the specified callback routine for an
   exception event callback */

void
create_exception_catchpoint (int tempflag, char *cond_string,
                             enum exception_event_kind ex_event, 
                             struct symtab_and_line *sal)
{
  struct breakpoint *b;
  int thread = -1;		/* All threads. */

  if (!sal)			/* no exception support? */
    return;

  b = set_raw_breakpoint (*sal);
  b->cond = NULL;
  b->cond_string = (cond_string == NULL) ? NULL : savestring (cond_string, (int) strlen (cond_string));
  b->thread = thread;
  b->addr_string = NULL;
  b->enable = enabled;
  b->disposition = tempflag ? del : donttouch;
  switch (ex_event)
    {
    case EX_EVENT_THROW:
      b->type = bp_catch_throw;
      set_breakpoint_count (breakpoint_count + 1);
      b->number = breakpoint_count;
      mention (b);
      break;
    case EX_EVENT_CATCH:
      b->type = bp_catch_catch;
      set_breakpoint_count (breakpoint_count + 1);
      b->number = breakpoint_count;
      mention (b);
      break;
    case EX_EVENT_INTERNAL:
      b->number = internal_breakpoint_number--;
      b->type = bp_catch_ex_internal;
      break;
    default:			/* error condition */
      b->type = bp_none;
      b->enable = disabled;
      set_breakpoint_count (breakpoint_count + 1);
      b->number = breakpoint_count;
      error ("Internal error -- invalid catchpoint kind");
    }
}

/* Deal with "catch catch" and "catch throw" commands */

static void
catch_exception_command_1 (enum exception_event_kind ex_event, char *arg, int tempflag, int from_tty)
{
  char *cond_string = NULL;
  struct symtab_and_line *sal = NULL;

  ep_skip_leading_whitespace (&arg);

  cond_string = ep_parse_optional_if_clause (&arg);

  if ((*arg != '\0') && !isspace (*arg))
    error ("Junk at end of arguments.");

  if ((ex_event != EX_EVENT_THROW) &&
      (ex_event != EX_EVENT_CATCH))
    error ("Unsupported or unknown exception event; cannot catch it");

  /* See if we can find a callback routine */
  sal = target_enable_exception_callback (ex_event, 1);

  if (sal)
    {
      /* We have callbacks from the runtime system for exceptions.
         Set a breakpoint on the sal found, if no errors */
      if (sal != (struct symtab_and_line *) -1L)
	create_exception_catchpoint (tempflag, cond_string, ex_event, sal);
      else
	return;			/* something went wrong with setting up callbacks */
    }
  else
    {
      /* No callbacks from runtime system for exceptions.
         Try GNU C++ exception breakpoints using labels in debug info. */
      if (ex_event == EX_EVENT_CATCH)
	{
	  handle_gnu_4_16_catch_command (arg, tempflag, from_tty);
	}
      else if (ex_event == EX_EVENT_THROW)
	{
	  /* Set a breakpoint on __raise_exception () */

	  fprintf_filtered (gdb_stderr, "Unsupported with this platform/compiler combination.\n");
	  fprintf_filtered (gdb_stderr, "Perhaps you can achieve the effect you want by setting\n");
	  fprintf_filtered (gdb_stderr, "a breakpoint on __raise_exception().\n");
	}
    }
}

/* Cover routine to allow wrapping target_enable_exception_catchpoints
   inside a catch_errors */
static struct symtab_and_line *
cover_target_enable_exception_callback (args_for_catchpoint_enable *args)
{
  return target_enable_exception_callback (args->kind, args->enable);
}



/* This is the original v.4.16 and earlier version of the
   catch_command_1() function.  Now that other flavours of "catch"
   have been introduced, and since exception handling can be handled
   in other ways (through target ops) also, this is used only for the
   GNU C++ exception handling system.
   Note: Only the "catch" flavour of GDB 4.16 is handled here.  The
   "catch NAME" is now no longer allowed in catch_command_1().  Also,
   there was no code in GDB 4.16 for "catch throw". 

   Called from catch_exception_command_1 () */


static void
handle_gnu_4_16_catch_command (char *arg, int tempflag, int from_tty)
{
  /* First, translate ARG into something we can deal with in terms
     of breakpoints.  */

  struct symtabs_and_lines sals = {0}; /* initialize for compiler warning */
  struct symtab_and_line sal;
  register struct expression *cond = 0;
  register struct breakpoint *b;
  char *save_arg;
  int i;

  INIT_SAL (&sal);		/* initialize to zeroes */

  /* If no arg given, or if first arg is 'if ', all active catch clauses
     are breakpointed. */

  if (!arg || (arg[0] == 'i' && arg[1] == 'f'
	       && (arg[2] == ' ' || arg[2] == '\t')))
    {
      /* Grab all active catch clauses.  */
      sals = get_catch_sals (0);
    }
  else
    {
      /* Grab selected catch clauses.  */
      error ("catch NAME not implemented");
    }

  if (!sals.nelts)
    return;

  save_arg = arg;
  for (i = 0; i < sals.nelts; i++)
    {
      resolve_sal_pc (&sals.sals[i]);

      while (arg && *arg)
	{
	  if (arg[0] == 'i' && arg[1] == 'f'
	      && (arg[2] == ' ' || arg[2] == '\t'))
	    cond = parse_exp_1 ((arg += 2, &arg),
				block_for_pc (sals.sals[i].pc), 0);
	  else
	    error ("Junk at end of arguments.");
	}
      arg = save_arg;
    }

  for (i = 0; i < sals.nelts; i++)
    {
      sal = sals.sals[i];

      sal.pc = SWIZZLE(sal.pc);

      if (from_tty)
	describe_other_breakpoints (sal.pc, sal.section);

      b = set_raw_breakpoint (sal);
      set_breakpoint_count (breakpoint_count + 1);
      b->number = breakpoint_count;
      b->type = bp_breakpoint;	/* Important -- this is an ordinary breakpoint.
				   For platforms with callback support for exceptions,
				   create_exception_catchpoint() will create special
				   bp types (bp_catch_catch and bp_catch_throw), and
				   there is code in insert_breakpoints() and elsewhere
				   that depends on that. */

      b->cond = cond;
      b->enable = enabled;
      b->disposition = tempflag ? del : donttouch;

      mention (b);
    }

  if (sals.nelts > 1 && !procedure_flag)
    {
      printf_unfiltered ("Multiple breakpoints were set.\n");
      printf_unfiltered ("Use the \"delete\" command to delete unwanted breakpoints.\n");
    }
  free ((PTR) sals.sals);
}

static void
catch_command_1 (char *arg, int tempflag, int from_tty)
{

  /* The first argument may be an event name, such as "start" or "load".
     If so, then handle it as such.  If it doesn't match an event name,
     then attempt to interpret it as an exception name.  (This latter is
     the v4.16-and-earlier GDB meaning of the "catch" command.)

     First, try to find the bounds of what might be an event name.
   */
  char *arg1_start = arg;
  char *arg1_end;
  int arg1_length;

  if (arg1_start == NULL)
    {
      /* Old behaviour was to use pre-v-4.16 syntax */
      /* catch_throw_command_1 (arg1_start, tempflag, from_tty); */
      /* return; */
      /* Now, this is not allowed */
      error ("Catch requires an event name");

    }
  arg1_end = ep_find_event_name_end (arg1_start);
  if (arg1_end == NULL)
    error ("catch requires an event");
  arg1_length = (int) (arg1_end + 1 - arg1_start);

  /* Try to match what we found against known event names. */
  if (strncmp (arg1_start, "signal", arg1_length) == 0)
    {
      error ("Catch of signal not yet implemented");
    }
  else if (strncmp (arg1_start, "catch", arg1_length) == 0)
    {
      catch_exception_command_1 (EX_EVENT_CATCH, arg1_end + 1, tempflag, from_tty);
    }
  else if (strncmp (arg1_start, "throw", arg1_length) == 0)
    {
      catch_exception_command_1 (EX_EVENT_THROW, arg1_end + 1, tempflag, from_tty);
    }
  else if (strncmp (arg1_start, "thread_start", arg1_length) == 0)
    {
      error ("Catch of thread_start not yet implemented");
    }
  else if (strncmp (arg1_start, "thread_exit", arg1_length) == 0)
    {
      error ("Catch of thread_exit not yet implemented");
    }
  else if (strncmp (arg1_start, "thread_join", arg1_length) == 0)
    {
      error ("Catch of thread_join not yet implemented");
    }
  else if (strncmp (arg1_start, "start", arg1_length) == 0)
    {
      error ("Catch of start not yet implemented");
    }
  else if (strncmp (arg1_start, "exit", arg1_length) == 0)
    {
      error ("Catch of exit not yet implemented");
    }
  else if (strncmp (arg1_start, "fork", arg1_length) == 0)
    {
#ifdef HP_IA64_GAMBIT
      error ("Catch of fork events not supported on HP-UX IA64-gambit\n");
#else
      catch_fork_command_1 (catch_fork, arg1_end + 1, tempflag, from_tty);
#endif
    }
  else if (strncmp (arg1_start, "vfork", arg1_length) == 0)
    {
#ifdef HPUX_1020
      error ("Catch of vfork events not supported on HP-UX 10.20\n");
#else
#ifdef HP_IA64_GAMBIT
      error ("Catch of vfork events not supported on HP-UX IA64-gambit\n");
#else
      catch_fork_command_1 (catch_vfork, arg1_end + 1, tempflag, from_tty);
#endif
#endif
    }
  else if (strncmp (arg1_start, "exec", arg1_length) == 0)
    {
#ifdef HP_IA64_GAMBIT
      error ("Catch of exec events not supported on HP-UX IA64-gambit\n");
#else
      catch_exec_command_1 (arg1_end + 1, tempflag, from_tty);
#endif
    }
  else if (strncmp (arg1_start, "load", arg1_length) == 0)
    {
      catch_load_command_1 (arg1_end + 1, tempflag, from_tty);
    }
  else if (strncmp (arg1_start, "unload", arg1_length) == 0)
    {
      catch_unload_command_1 (arg1_end + 1, tempflag, from_tty);
    }
  else if (strncmp (arg1_start, "nomem", arg1_length) == 0)
    {
      catch_nomem_command_1 (arg1_end + 1, tempflag, from_tty);
    }
  else if (strncmp (arg1_start, "stop", arg1_length) == 0)
    {
      error ("Catch of stop not yet implemented");
    }

  /* This doesn't appear to be an event name */

  else
    {
      /* Pre-v.4.16 behaviour was to treat the argument
         as the name of an exception */
      /* catch_throw_command_1 (arg1_start, tempflag, from_tty); */
      /* Now this is not allowed */
      error ("Unknown event kind specified for catch");

    }
}

/* Used by the gui, could be made a worker for other things. */

struct breakpoint *
set_breakpoint_sal (struct symtab_and_line sal)
{
  struct breakpoint *b;
  b = set_raw_breakpoint (sal);
  set_breakpoint_count (breakpoint_count + 1);
  b->number = breakpoint_count;
  b->type = bp_breakpoint;
  b->cond = 0;
  b->thread = -1;
  return b;
}


static void
catch_command (char *arg, int from_tty)
{
  catch_command_1 (arg, 0, from_tty);
}


static void
tcatch_command (char *arg, int from_tty)
{
  catch_command_1 (arg, 1, from_tty);
}


static void
clear_command (char *arg, int from_tty)
{
  register struct breakpoint *b, *b1;
  int default_match;
  struct cleanup *old_chain;
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  register struct breakpoint *found;
  int i;

  if (arg)
    {
      sals = decode_line_spec (arg, 1);

      /* srikanth, 981001, CLLbs15582, decode_line_spec() used not to
       * return if arg is not found, but now it does....
       */
      if (sals.nelts == 0)
	error ("Location not found.");

      default_match = 0;
    }
  else
    {
      sals.sals = (struct symtab_and_line *)
	xmalloc (sizeof (struct symtab_and_line));
      INIT_SAL (&sal);		/* initialize to zeroes */
      sal.line = default_breakpoint_line;
      sal.symtab = default_breakpoint_symtab;
      sal.pc = default_breakpoint_address;
      if (sal.symtab == 0)
        { 
          free ((PTR) sals.sals);	  
	  error ("No source file specified.");
	}

      sals.sals[0] = sal;
      sals.nelts = 1;

      default_match = 1;
    }

   old_chain = make_cleanup (free, sals.sals);

  /* For each line spec given, delete bps which correspond
   * to it.  We do this in two loops: the first loop looks at
   * the initial bp(s) in the chain which should be deleted,
   * the second goes down the rest of the chain looking ahead
   * one so it can take those bps off the chain without messing
   * up the chain.
   */


  for (i = 0; i < sals.nelts; i++)
    {
      /* If exact pc given, clear bpts at that pc.
       * If line given (pc == 0), clear all bpts on specified line.
       * If defaulting, clear all bpts on default line
       * or at default pc.
       *
       *       defaulting    sal.pc != 0    tests to do
       *
       *         0              1             pc
       *         1              1             pc _and_ line
       *         0              0             line
       *         1              0             <can't happen>
       */

      sal = sals.sals[i];
      found = (struct breakpoint *) 0;


      while (breakpoint_chain
      /* Why don't we check here that this is not
       * a watchpoint, etc., as we do below?
       * I can't make it fail, but don't know
       * what's stopping the failure: a watchpoint
       * of the same address as "sal.pc" should
       * wind up being deleted.
       */

	     && (((sal.pc && are_addrs_equal (breakpoint_chain->address, sal.pc)) &&
		  (overlay_debugging == 0 ||
		   breakpoint_chain->section == sal.section))
		 || ((default_match || (0 == sal.pc))
		     && breakpoint_chain->source_file != NULL
		     && sal.symtab != NULL
	      && (STREQ (breakpoint_chain->source_file, sal.symtab->filename) ||
	          STREQ (breakpoint_chain->source_file, sal.symtab->fullname))
		     && breakpoint_chain->line_number == sal.line)))

	{
	  b1 = breakpoint_chain;
	  breakpoint_chain = b1->next;
	  b1->next = found;
	  found = b1;
	}

      ALL_BREAKPOINTS (b)

	while (b->next
	       && b->next->type != bp_none
	       && b->next->type != bp_watchpoint
	       && b->next->type != bp_hardware_watchpoint
	       && b->next->type != bp_read_watchpoint
	       && b->next->type != bp_access_watchpoint
	       && (((sal.pc && are_addrs_equal (b->next->address, sal.pc)) &&
		    (overlay_debugging == 0 ||
		     b->next->section == sal.section))
		   || ((default_match || (0 == sal.pc))
		       && b->next->source_file != NULL
		       && sal.symtab != NULL
		       && (STREQ (b->next->source_file, sal.symtab->filename) ||
                           STREQ (b->next->source_file, sal.symtab->fullname))
		       && b->next->line_number == sal.line)))


	{
	  b1 = b->next;
	  b->next = b1->next;
	  b1->next = found;
	  found = b1;
	}

      if (found == 0)
	{
	  if (arg)
	    error ("No breakpoint at %s.", arg);
	  else
	    error ("No breakpoint at this line.");
	}

      if (found->next)
	from_tty = 1;		/* Always report if deleted more than one */
      if (from_tty)
	printf_unfiltered ("Deleted breakpoint%s ", found->next ? "s" : "");
      breakpoints_changed ();
      while (found)
	{
	  if (from_tty)
	    printf_unfiltered ("%d ", found->number);
	  b1 = found->next;
	  delete_breakpoint (found);
	  found = b1;
	}
      if (from_tty)
	putchar_unfiltered ('\n');
    }
  do_cleanups (old_chain);
}


/* Suspend a breakpoint given a line spec. This is used in Nimbus. 
 */
static void
suspend_command (char *arg, int from_tty)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  int disabled_some;
  struct breakpoint * b;
  int i;
  int addr_arg = 0;

  if (!arg)
    error ("Line number or address should be specified\n");

  sals = decode_line_spec (arg, 1);

  if (sals.nelts == 0)
      error("Location not found.");

  make_cleanup (free, sals.sals);

  if (*arg == '*')
    {
      addr_arg = 1;
    }

  disabled_some = 0;
  for (i = 0; i < sals.nelts; i++)
    {
      sal = sals.sals[i];
      ALL_BREAKPOINTS (b)
        {
          if ( ( (b->source_file && sal.symtab 
		  && (STREQ (b->source_file, sal.symtab->filename)
		      || STREQ (b->source_file, sal.symtab->fullname))
		  && !addr_arg && b->line_number == sal.line)
		 || (addr_arg && are_addrs_equal (b->address, sal.pc)))
	       && b->enable != disabled)
            {
              disable_breakpoint (b);
              disabled_some = 1;
            }
        }
    }

    if (!disabled_some)
      if (arg)
        error ("No active breakpoint at %s.", arg);
      else
        error ("No active breakpoint at this line.");

    breakpoints_changed ();
}

/* Activate a breakpoint given a line spec. This is used in Nimbus.
*/
static void
activate_command (char *arg, int from_tty)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  int activated_some;
  struct breakpoint * b;
  int i;
  int addr_arg = 0;

  if (!arg)
    error ("Line number or address should be specified\n");

  sals = decode_line_spec (arg, 1);

  if (sals.nelts == 0)
      error("Location not found.");

  make_cleanup (free, sals.sals);

  if (*arg == '*')
    {
      addr_arg = 1;
    }

  activated_some = 0;
  for (i = 0; i < sals.nelts; i++)
    {
      sal = sals.sals[i];
      ALL_BREAKPOINTS (b)
        {
          if ( ( (b->source_file && sal.symtab
		  && (STREQ (b->source_file, sal.symtab->filename)
		      || STREQ (b->source_file, sal.symtab->fullname))
		  && !addr_arg && b->line_number == sal.line)
		 || (addr_arg && are_addrs_equal (b->address, sal.pc)))
	       && b->enable == disabled)
	    {
              enable_breakpoint (b);
              activated_some = 1;
            }
        }
    }

    if (!activated_some)
      if (arg)
        error ("No inactive breakpoint at %s.", arg);
      else
        error ("No inactive breakpoint at this line.");

    breakpoints_changed ();
}


/* Delete breakpoint in BS if they are `delete' breakpoints and
   all breakpoints that are marked for deletion, whether hit or not.
   This is called after any breakpoint is hit, or after errors.  */

void
breakpoint_auto_delete (bpstat bs)
{
  struct breakpoint *b, *temp = 0; /* initialize for compiler warning */

  for (; bs; bs = bs->next)
    if (bs->breakpoint_at && bs->breakpoint_at->disposition == del
	&& bs->stop)
      delete_breakpoint (bs->breakpoint_at);

  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    if (b->disposition == del_at_next_stop)
      delete_breakpoint (b);
  }
}

/* Delete a breakpoint and clean up all traces of it in the data structures. */

void
delete_breakpoint (struct breakpoint *bpt)
{
  register struct breakpoint *b;
  register struct deleted_breakpoint * old_bp;
  register bpstat bs;

  if (bpt == NULL)
    error ("Internal error (attempted to delete a NULL breakpoint)");


  /* Has this bp already been deleted?  This can happen because multiple
     lists can hold pointers to bp's.  bpstat lists are especial culprits.

     One example of this happening is a watchpoint's scope bp.  When the
     scope bp triggers, we notice that the watchpoint is out of scope, and
     delete it.  We also delete its scope bp.  But the scope bp is marked
     "auto-deleting", and is already on a bpstat.  That bpstat is then
     checked for auto-deleting bp's, which are deleted.

     A real solution to this problem might involve reference counts in bp's,
     and/or giving them pointers back to their referencing bpstat's, and
     teaching delete_breakpoint to only free a bp's storage when no more
     references were extent.  A cheaper bandaid was chosen. */
  if (bpt->type == bp_none)
    return;

#ifdef DYNLINK_DELETE_EVENT
  DYNLINK_DELETE_EVENT (bpt->dynlink_event);
#endif

  if (delete_breakpoint_hook)
    delete_breakpoint_hook (bpt);

  if (delete_hook)
    delete_hook (bpt);

  breakpoint_delete_event (bpt->number);

  if (bpt->inserted)
    remove_breakpoint (bpt, mark_uninserted);

  if (breakpoint_chain == bpt)
    breakpoint_chain = bpt->next;

  /* If we have callback-style exception catchpoints, don't go through
     the adjustments to the C++ runtime library etc. if the inferior
     isn't actually running.  target_enable_exception_callback for a
     null target ops vector gives an undesirable error message, so we
     check here and avoid it. Since currently (1997-09-17) only HP-UX aCC's
     exceptions are supported in this way, it's OK for now. FIXME */
  if (ep_is_exception_catchpoint (bpt) && target_has_execution)
    {
      static char message1[] = "Error in deleting catchpoint %d:\n";
      static char message[sizeof (message1) + 30];
      args_for_catchpoint_enable args;

      sprintf (message, message1, bpt->number);		/* Format possible error msg */
      args.kind = bpt->type == bp_catch_catch ? EX_EVENT_CATCH : EX_EVENT_THROW;
      args.enable = 0;
      (void) catch_errors ((catch_errors_ftype *) cover_target_enable_exception_callback,
			   (char *) &args,
			   message, RETURN_MASK_ALL);
    }


  ALL_BREAKPOINTS (b)
    if (b->next == bpt)
    {
      b->next = bpt->next;
      break;
    }

  /*
     ** Before turning off the visuals for the bp, check to see that
     ** there are no other bps at the same address.
   */
  if (tui_version)
    {
      int clearIt = 1;		/* srikanth, CHFts24260, 981102 */

      ALL_BREAKPOINTS (b)
      {
	clearIt = !are_addrs_equal (b->address, bpt->address);
	if (!clearIt)
	  break;
      }

      if (clearIt)
	{
	  TUIDOWITHOUTTERMTRANSITION (((TuiOpaqueFuncPtr) tui_vAllSetHasBreakAt,
				       bpt,
				       FALSE));
	  TUIDO (((TuiOpaqueFuncPtr) tuiUpdateAllExecInfos));
	}
    }

  check_duplicates (bpt->address, bpt->section);
  /* If this breakpoint was inserted, and there is another breakpoint
     at the same address, we need to insert the other breakpoint.  */
  if (bpt->inserted
      && bpt->type != bp_hardware_watchpoint
      && bpt->type != bp_read_watchpoint
      && bpt->type != bp_access_watchpoint
      && bpt->type != bp_catch_fork
      && bpt->type != bp_catch_vfork
#ifdef DYNLINK_HAS_BREAK_HOOK
      && bpt->type != bp_catch_load
      && bpt->type != bp_catch_unload
#endif
      && bpt->type != bp_catch_exec)
    {
      ALL_BREAKPOINTS (b)
	if (are_addrs_equal (b->address, bpt->address)
	    && b->section == bpt->section
	    && !b->duplicate
	    && b->enable != disabled
	    && b->enable != shlib_disabled
	    && b->enable != call_disabled)
	{
	  int val;
#ifdef HP_IA64
	  val = ia64_insert_breakpoint_1 (b->address, b->shadow_contents, b);
#else
	  val = target_insert_breakpoint (b->address, b->shadow_contents);
#endif
	  if (val != 0)
	    {
	      target_terminal_ours_for_output ();
	      fprintf_unfiltered (gdb_stderr, "Cannot insert breakpoint %d:\n", b->number);
	      memory_error (val, b->address);	/* which bombs us out */
	    }
	  else
	    b->inserted = 1;
	}
    }
 
  /* srikanth, JAGaa80394, remember the deleted breakpoint */
  old_bp = xmalloc (sizeof (struct deleted_breakpoint));
  old_bp->address = bpt->address;
  old_bp->next = deleted_breakpoint_chain;
  deleted_breakpoint_chain = old_bp;

  if (bpt->cond)
    free (bpt->cond);
  if (bpt->cond_string != NULL)
    free (bpt->cond_string);
  if (bpt->addr_string != NULL)
    free (bpt->addr_string);
  if (bpt->func_string != NULL)
    free (bpt->func_string);
  if (bpt->exp != NULL)
    free (bpt->exp);
  if (bpt->exp_string != NULL)
    free (bpt->exp_string);
  if (bpt->val != NULL)
    value_free (bpt->val);
  if (bpt->source_file != NULL)
    free (bpt->source_file);
  if (bpt->module_name != NULL)
    free (bpt->module_name);
  if (bpt->dll_pathname != NULL)
    free (bpt->dll_pathname);
  if (bpt->triggered_dll_pathname != NULL)
    free (bpt->triggered_dll_pathname);
  if (bpt->exec_pathname != NULL)
    free (bpt->exec_pathname);

  /* Be sure no bpstat's are pointing at it after it's been freed.  */
  /* FIXME, how can we find all bpstat's?
     We just check stop_bpstat for now.  */
  for (bs = stop_bpstat; bs; bs = bs->next)
    if (bs->breakpoint_at == bpt)
      {
	bs->breakpoint_at = NULL;

	/* we'd call bpstat_clear_actions, but that free's stuff and due
	   to the multiple pointers pointing to one item with no
	   reference counts found anywhere through out the bpstat's (how
	   do you spell fragile?), we don't want to free things twice --
	   better a memory leak than a corrupt malloc pool! */
	bs->old_val = NULL;
      }
  /* On the chance that someone will soon try again to delete this same
     bp, we mark it as deleted before freeing its storage. */
  bpt->type = bp_none;

  free ((PTR) bpt);
}

static void
do_delete_breakpoint_cleanup (void *b)
{
  delete_breakpoint (b);
}

struct cleanup *
make_cleanup_delete_breakpoint (struct breakpoint *b)
{
  return make_cleanup (do_delete_breakpoint_cleanup, b);
}

struct cleanup *
make_exec_cleanup_delete_breakpoint (struct breakpoint *b)
{
  return make_exec_cleanup (do_delete_breakpoint_cleanup, b);
}

/* This function is used to delete all the breakpoints set 
   by the rbp , Dinesh , 04/15/02 */

void
rbreak_delete_commands (char *args, int from_tty)
{
  struct break_commands_list *rbp_list, *next;
  rbp_list = break_commands_chain;
  if (args)
    error ("The \"delete_rbreak_commands\" command does not take any arguments."
);

  while (rbp_list)
  {
    delete_command (rbp_list->break_commands,from_tty);
    next = rbp_list->next;
    if (rbp_list->break_commands != NULL)
    free (rbp_list->break_commands);
    free_command_lines (&rbp_list->commands_list);
    free (rbp_list);
    rbp_list = next;
    break_commands_chain = next;
  }
  
  break_commands_chain = 0;
}

void
delete_command (char *arg, int from_tty)
{
  struct breakpoint *b, *temp = 0; /* initialize for compiler warning */

  if (arg == 0)
    {
      int breaks_to_delete = 0;

      /* Delete all breakpoints if no argument.
         Do not delete internal or call-dummy breakpoints, these
         have to be deleted with an explicit breakpoint number argument.  */
      ALL_BREAKPOINTS (b)
      {
	if (b->type != bp_call_dummy &&
	    b->type != bp_shlib_event &&
	    b->type != bp_rtc_event &&
	    b->type != bp_hw_rtc_event &&
	    b->type != bp_rtc_compiler_event &&
	    b->type != bp_hw_rtc_compiler_event &&
	    b->type != bp_fix_event &&
	    b->type != bp_thread_event &&
	    b->number >= 0)
	  breaks_to_delete = 1;
      }

      /* Ask user only if there are some breakpoints to delete.  */
      if (!from_tty
	  || (breaks_to_delete && query ("Delete all breakpoints? ")))
	{
	  ALL_BREAKPOINTS_SAFE (b, temp)
	  {
	    if (b->type != bp_call_dummy &&
		b->type != bp_shlib_event &&
		b->type != bp_rtc_event &&
		b->type != bp_hw_rtc_event &&
		b->type != bp_rtc_compiler_event &&
		b->type != bp_hw_rtc_compiler_event &&
		b->type != bp_fix_event &&
		b->type != bp_thread_event &&

		b->number >= 0)
	      delete_breakpoint (b);
	  }
	}
    }
  else
    map_breakpoint_numbers (arg, delete_breakpoint);
}

int reset_all_breakpoints;

/* Reset a breakpoint given it's struct breakpoint * BINT.
   The value we return ends up being the return value from catch_errors.
   Unused in this case.  */

static int
breakpoint_re_set_one (char *bint)
{
  struct breakpoint *b = (struct breakpoint *)(void *) bint;	/* get past catch_errs */
  struct value *mark;
  int i;
  struct symtabs_and_lines sals;
  char *s;
  enum enable save_enable;
  args_for_evaluate_expression args;
  args_for_parse_exp_1 parse_args;

  switch (b->type)
    {
    case bp_none:
      warning ("attempted to reset apparently deleted breakpoint #%d?\n", b->number);
      return 0;
    case bp_breakpoint:
    case bp_sbrk_event:
    case bp_thread_tracing_event:
    case bp_catch_nomem:
    case bp_hw_catch_nomem:
    case bp_procedure:
    case bp_hardware_breakpoint:
#ifndef DYNLINK_HAS_BREAK_HOOK
    case bp_catch_load:
    case bp_catch_unload:
#endif

     /* JAGaf70508 - Enable the breakpoints when loading 
        more than one file concurrently provided the 
        breakpoints are not disabled or user_disabled
     */
         
     if (b->enable == user_disabled 
         || (b->enable == disabled &&
             strcmp (b->module_name, symfile_objfile->name) != 0)
         || (b->enable == disabled && b->disposition == disable))
       return 0;

      if (b->addr_string == NULL)
	{
	  /* Anything without a string can't be re-set. */
	  delete_breakpoint (b);
	  return 0;
	}

      /* RM: no point in trying to reset shlib_disabled breakpoints, they
       * haven't yet been mapped in.
       */
      /* JAGae87253 - Conditional breakpoint */
      if ((!b->address || b->cond_string) && (b->enable == shlib_disabled))
	break;

      /* srikanth, there is no point in resetting already enabled/inserted
         breakpoints. This causes the breakpoint menu to be repeatedly
         shown each time a new library is loaded if there are two or more
         symbols with the same name in different load modules. There are
         apps out there that load hundreds of shared libraries.

         However, we do want to do this if the main module symbol file
         has changed as in follow-forked child, exec situations.
      */

      if (b->enable == enabled && !reset_all_breakpoints)
        break;

      /* In case we have a problem, disable this breakpoint.  We'll restore
         its status if we succeed.  */
      save_enable = b->enable;
      b->enable = disabled;

      set_language (b->language);
      input_radix = b->input_radix;
      s = b->addr_string;

      sals = decode_line_1_reuse (&s, 1, (struct symtab *) NULL, 0, 
				  (char ***) NULL, USER_CHOICE, b->source_file,
				   b->line_number, b->inline_idx);
      if (sals.is_file_and_line)
        {
	  /* The FILENAME:LINENUM type of breakpoint spec
	     inside multiple instantiated C++ template functions.
	     In this case we need to make sure we pick the right
	     one to use. */
	  /* FIXME: Do we want to handle the fact that with the loading
	     of shared libs there could be more sals coming from these
	     shared libs? */
	  struct symbol *sym;

	  for (i = 0; i < sals.nelts; i++)
  	    {
	      resolve_sal_pc (&sals.sals[i]);
	      sym = find_pc_sect_function (sals.sals[i].pc,
					   sals.sals[i].section);
	      if (sym && b->func_string &&
		  ! strcmp (b->func_string, SYMBOL_SOURCE_NAME (sym)))
		break;
	    }
	  if (i < sals.nelts)
	    {
	      sals.sals[0] = sals.sals[i];
	      sals.nelts = 1;
	    }
	  else
	    {
	      /* Cannot find the function, probably not yet mapped in
	      	 at rerun.  Reset sals.nelts to keep it disabled instead
		 of deleting it. */
	      free ((PTR) sals.sals);
	      sals.nelts = 0;
	    }
	}
      
      for (i = 0; i < sals.nelts; i++)
	{
	  resolve_sal_pc (&sals.sals[i]);

	  /* Reparse conditions, they might contain references to the
	     old symtab.  */
	  if (b->cond_string != NULL)
	    {
	      s = b->cond_string;
	      /* JAGae87253 - Temporarily disabling conditional breakpoint to re-set after loading shared libraries */
	      parse_args.exp_string_p = &s;
	      parse_args.block = block_for_pc (sals.sals[i].pc);
              parse_args.result = 0;
	      if (!catch_errors ((catch_errors_ftype *)
                                        cover_parse_exp_1,
                                        (char *) &parse_args,
                                        "",
                                        RETURN_MASK_ALL))
	      {
		warning("Temporarily disabling conditional breakpoint #%d", b->number);
              	b->enable = shlib_disabled;
		return 0;
	      }
	      else
	        b->cond = parse_args.result;
	    }

	  /* We need to re-set the breakpoint if the address changes... */

          if ((!are_addrs_equal (b->address, sals.sals[i].pc) )
	  /* ...or new and old breakpoints both have source files, and
	     the source file name or the line number changes...  */
	      || (b->source_file != NULL
		  && sals.sals[i].symtab != NULL
		  && (!(STREQ (b->source_file, sals.sals[i].symtab->filename)
		        || STREQ (b->source_file, sals.sals[i].symtab->fullname))
		       || b->line_number != sals.sals[i].line)
	      )
	  /* ...or we switch between having a source file and not having
	     one.  */
	      || ((b->source_file == NULL) != (sals.sals[i].symtab == NULL))
	    )
	    {
	      /* Before changing the address of the breakpoint, if the
		 breakpoint has been inserted with the current address,
		 remove it
		 */
	      if (b->inserted)
		remove_breakpoint (b, mark_uninserted);


	      if (b->source_file != NULL)
		free (b->source_file);
	      if (sals.sals[i].symtab == NULL)
		b->source_file = NULL;
	      else
                b->source_file = annotation_level > 1 ?
                  savestring (symtab_to_filename (sals.sals[i].symtab), (int) strlen (symtab_to_filename(sals.sals[i].symtab))) :
                  savestring (sals.sals[i].symtab->filename, (int) strlen (sals.sals[i].symtab->filename));
	      b->line_number = sals.sals[i].line;
	      b->address = sals.sals[i].pc;

	      /* Used to check for duplicates here, but that can
	         * cause trouble, as it doesn't check for disable
	         * breakpoints.
	       */

	      /* srikanth, if the breakpoint is disabled we don't want
	         to mention it nor emit any annotations : suffices to
	         update the notion of address and spos, so it could be
	         re-enabled later if the user wishes.
	       */

	      if (save_enable != disabled && save_enable != shlib_disabled)
		{
		  mention (b);
		  /* Might be better to do this just once per breakpoint_re_set,
		     rather than once for every breakpoint.  */
		  breakpoints_changed ();
		}
	    }
	  b->section = sals.sals[i].section;

          /* JAGaf70508 - Set the enable flag appropriately 
             when enabling the already disabled breakpoint.
          */
          if (save_enable == disabled)
            b->enable = enabled;
          else
	    b->enable = save_enable;	/* Restore it, this worked. */


	  /* Now that this is re-enabled, check_duplicates
	   * can be used.
	   */
	  check_duplicates (b->address, b->section);

	}

      /* srikanth, if we fail to find the breakpoint location, it could be
         that the corresponding shared library is not mapped in or that the
         debuggee program is not the same (via fork/exec or file new-file)
         or perhaps because, the function is no longer a part of the program.
         If the original enable status indicates that the bp is from a shlib
         make it shlib_disabled. Otherwise, simply disable the breakpoint.
       */
      if (sals.nelts == 0)
	{
	  if (save_enable == shlib_disabled)
	    b->enable = shlib_disabled;
	  else
	    b->enable = disabled;
	}
      else
	free ((PTR) sals.sals);
      break;
       
    case bp_watchpoint:
    case bp_hardware_watchpoint:
    case bp_read_watchpoint:
    case bp_access_watchpoint:
      innermost_block = NULL;
      /* The issue arises of what context to evaluate this in.  The same
         one as when it was set, but what does that mean when symbols have
         been re-read?  We could save the filename and functionname, but
         if the context is more local than that, the best we could do would
         be something like how many levels deep and which index at that
         particular level, but that's going to be less stable than filenames
         or functionnames.  */
      /* So for now, just use a global context.  */
      if (b->exp)
	free ((PTR) b->exp);
      b->exp = 0; /* JAGae60524 */

      args_for_parse_expression parse_args1;
      parse_args1.exp_string = b->exp_string;
      parse_args1.exp = NULL;
      donot_print_errors = 1;
      if (!catch_errors ((catch_errors_ftype *) parse_expression_1,
                         (char*) &parse_args1, NULL, RETURN_MASK_ALL))
      {
	  /* Could not evaluate on re-set. Make this a deferred watchpoint. */
          b->val = NULL;
          if (b->enable != user_disabled)
	    b->enable = heap_disabled;
          donot_print_errors = 0;
          return 0;
      }
      b->exp = parse_args1.exp;

      b->exp_valid_block = innermost_block;
      mark = value_mark ();
      if (b->val)
	value_free (b->val);
      b->val = 0; /* JAGae60524 */
      args.exp = b->exp;
      args.valp = NULL;
      donot_print_errors = 1;
      if (!catch_errors ((catch_errors_ftype *) evaluate_expression_1,
                           (char *) &args,
                           NULL, RETURN_MASK_ALL))
        {
	  /* Could not evaluate on re-set. Make this a deferred watchpoint. */
          donot_print_errors = 0;
          b->val = NULL;
          if (b->enable != user_disabled)
	    b->enable = heap_disabled;
        }
      else
        {
	  b->val = args.valp;
          release_value (b->val);
	  if (!catch_errors ((catch_errors_ftype *) value_fetch_lazy_1,
                           (char *) args.valp,
                           NULL, RETURN_MASK_ALL))
            {
	      /* Could not evaluate on re-set. Make this a deferred watchpoint. */
              donot_print_errors = 0;
              b->val = NULL;
              if (b->enable != user_disabled)
	        b->enable = heap_disabled;
            }
	}

      donot_print_errors = 0;

      if (b->cond_string != NULL)
	{
	  s = b->cond_string;
	  if (b->cond)
	    free ((PTR) b->cond);
	  b->cond = parse_exp_1 (&s, (struct block *) 0, 0);
	}
      if (b->enable == enabled)
	mention (b);
      value_free_to_mark (mark);
      break;
    case bp_catch_catch:
    case bp_catch_throw:
    case bp_catch_ex_internal:
    case bp_mmap:
      break;
      /* We needn't really do anything to reset these, since the mask
         that requests them is unaffected by e.g., new libraries being
         loaded.
       */
    case bp_catch_fork:
    case bp_catch_vfork:
    case bp_catch_exec:
#ifdef DYNLINK_HAS_BREAK_HOOK
    case bp_catch_load:
    case bp_catch_unload:
#endif
      break;

    default:
      printf_filtered ("Deleting unknown breakpoint type %d\n", b->type);
      /* fall through */
      /* Delete longjmp breakpoints, they will be reset later by
         breakpoint_re_set.  */
#ifdef GET_LONGJMP_TARGET
    case bp_longjmp:
    case bp_hw_longjmp:
    case bp_longjmp_resume:
    case bp_hw_longjmp_resume:
      delete_breakpoint (b);
      break;
#endif /* GET_LONGJMP_TARGET */

      /* This breakpoint is special, it's set up when the inferior
         starts and we really don't want to touch it.  */
    case bp_shlib_event:
    case bp_rtc_event:
    case bp_hw_rtc_event:
    case bp_rtc_compiler_event:
    case bp_hw_rtc_compiler_event:
    case bp_rantomain_event:
    case bp_rantomain_sigwait_event:
    case bp_fix_event:

      /* Like bp_shlib_event, this breakpoint type is special.
	 Once it is set up, we do not want to touch it.  */
    case bp_thread_event:

      /* Keep temporary breakpoints, which can be encountered when we step
         over a dlopen call and SOLIB_ADD is resetting the breakpoints.
         Otherwise these should have been blown away via the cleanup chain
         or by breakpoint_init_inferior when we rerun the executable.  */

      /* Srikanth, bp_through_sigtramp should qualify as a temporary
         breakpoint, no ? Jul 29th 2002.
      */

    case bp_until:
    case bp_hw_until:
    case bp_finish:
    case bp_hw_finish:
    case bp_watchpoint_scope:
    case bp_hw_watchpoint_scope:
    case bp_call_dummy:
    case bp_step_resume:
    case bp_hw_step_resume:
    case bp_through_sigtramp:
    case bp_hw_through_sigtramp:
      break;
    }

  return 0;
}

/* Re-set all breakpoints after symbols have been re-loaded.  */
void
breakpoint_re_set ()
{
  struct breakpoint *b, *temp = NULL;
  enum language save_language;
  int save_input_radix;
  static char message1[] = "Error in re-setting breakpoint %d:\n";
  char message[sizeof (message1) + 30 /* slop */ ];

  save_language = current_language->la_language;
  save_input_radix = input_radix;
  ALL_BREAKPOINTS_SAFE (b, temp)
  {
    sprintf (message, message1, b->number);	/* Format possible error msg */
    catch_errors ((catch_errors_ftype *) breakpoint_re_set_one, (char *) b, message,
		  RETURN_MASK_ALL);
  }
  set_language (save_language);
  input_radix = save_input_radix;

#ifdef GET_LONGJMP_TARGET
  create_longjmp_breakpoint ("longjmp");
  create_longjmp_breakpoint ("_longjmp");
  create_longjmp_breakpoint ("siglongjmp");
  create_longjmp_breakpoint ("_siglongjmp");
  create_longjmp_breakpoint (NULL);
#endif

}

/* Set ignore-count of breakpoint number BPTNUM to COUNT.
   If from_tty is nonzero, it prints a message to that effect,
   which ends with a period (no newline).  */

/* Reset the thread number of this breakpoint:

   - If the breakpoint is for all threads, leave it as-is.
   - Else, reset it to the current thread for inferior_pid.
 */
void
breakpoint_re_set_thread (struct breakpoint *b)
{
  if (b->thread != -1)
    {
      if (in_thread_list (inferior_pid))
        {
#ifdef HP_MXN
          if (is_mxn)
	    {
	      int utid;
	      utid = get_pthread_for (inferior_pid);
	      if (utid == -1)
		b->thread = - (get_lwp_for (inferior_pid));
	      else
		b->thread = utid;
	    }
          else
#endif
	    b->thread = pid_to_thread_id (inferior_pid);
        }
    }
}

void
set_ignore_count (int bptnum, int count, int from_tty)
{
  register struct breakpoint *b;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  register struct breakpoint *related_breakpoint;
#endif

  if (count < 0)
    count = 0;

  ALL_BREAKPOINTS (b)
    if (b->number == bptnum)
    {
      b->ignore_count = count;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
      /* If inline_debugging == BP_ALL, then apply the "ignore" command on all
         related breakpoints.
      */
      if (inline_debugging == BP_ALL && b->inline_idx != 0)
        {
          for (related_breakpoint = b->related_breakpoint; related_breakpoint != NULL;
               related_breakpoint = related_breakpoint->related_breakpoint)
            {
              related_breakpoint->ignore_count = count;
            } 
        } 
#endif  
      if (!from_tty)
	return;
      else if (count == 0)
	printf_filtered ("Will stop next time breakpoint %d is reached.",
			 bptnum);
      else if (count == 1)
	printf_filtered ("Will ignore next crossing of breakpoint %d.",
			 bptnum);
      else
	printf_filtered ("Will ignore next %d crossings of breakpoint %d.",
			 count, bptnum);
      breakpoints_changed ();
      return;
    }

  error ("No breakpoint number %d.", bptnum);
}

/* Clear the ignore counts of all breakpoints.  */
void
breakpoint_clear_ignore_counts ()
{
  struct breakpoint *b;

  ALL_BREAKPOINTS (b)
    b->ignore_count = 0;
}

/* Command to set ignore-count of breakpoint N to COUNT.  */

static void
ignore_command (char *args, int from_tty)
{
  char *p = args;
  register int num;

  if (p == 0)
    error_no_arg ("a breakpoint number");

  num = get_number (&p);

  if (*p == 0)
    error ("Second argument (specified ignore-count) is missing.");

  set_ignore_count (num,
		    longest_to_int (value_as_long (parse_and_eval (p))),
		    from_tty);
  printf_filtered ("\n");
  breakpoints_changed ();
}

/* Call FUNCTION on each of the breakpoints
   whose numbers are given in ARGS.  */

static void
map_breakpoint_numbers (char *args, void (*function) PARAMS ((struct breakpoint *)))
{
  register char *p = args;
  char *p1;
  register int num;
  register struct breakpoint *b , *tmp = 0; /* initialize for compiler warning*/
  int match;

  if (p == 0)
    error_no_arg ("one or more breakpoint numbers");

  while (*p)
    {
      match = 0; 
      p1 = p;

      num = get_number_or_range (&p1);
      if (num == 0)
	{
	  warning ("bad breakpoint number at or near '%s'", p);
	}
      else
	{
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
          /* If inline_debugging == BP_ALL, then 
             delete, enable and disable all related breakpoints.
          */
          if (inline_debugging == BP_ALL) 
            {
              ALL_BREAKPOINTS_SAFE (b, tmp)
              if (b->number == num)
                {
                  struct breakpoint *related_breakpoint = b->related_breakpoint;
                  struct breakpoint *next_bp;
                  match = 1;
                  if (related_breakpoint)
                    for (; related_breakpoint != NULL; 
                         related_breakpoint = next_bp)
                      {
                        next_bp = related_breakpoint->related_breakpoint;
                        function (related_breakpoint);
                      }
                  function (b);
                  break;
                }
            }
          else
#endif
          
	  ALL_BREAKPOINTS_SAFE (b, tmp)
	    if (b->number == num)
	      {
		struct breakpoint *related_breakpoint = b->related_breakpoint;
		match = 1;
		function (b);
		if (related_breakpoint)
		  function (related_breakpoint);
		break;
	      }
	  if (match == 0)
	    printf_unfiltered ("No breakpoint number %d.\n", num);
	}
      p = p1;
    }
}

void
disable_breakpoint (struct breakpoint *bpt)
{
  bpt->shadow_contents_known_to_be_valid = 0;
  /* Never disable a watchpoint scope breakpoint; we want to
     hit them when we leave scope so we can delete both the
     watchpoint and its scope breakpoint at that time.  */
  if (bpt->type == bp_watchpoint_scope || bpt->type == bp_hw_watchpoint_scope)
    return;

  /* Never disable a deferred breakpoint */
  if (bpt->enable == shlib_disabled)
    return;

  /* You can't disable permanent breakpoints.  */
  if (bpt->enable == permanent)
    return;

  /* JAGaf70508 - If the user has diabled the breakpoint,
     then set the breakpoint_type as user_disabled.
  */
  /* QXCR1001055861 : Mark the breakpoint user_disabled
     for watchpoints also. Should the catchpoints be treated
     the same way ? Why not mark all bpt->enable to user_disabled
     irrespective of bpt->type ?
  */
  if (   (bpt->type == bp_breakpoint)
      || (bpt->type == bp_hardware_watchpoint)
      || (bpt->type == bp_read_watchpoint)
      || (bpt->type == bp_access_watchpoint)
      || (bpt->type == bp_watchpoint))
    bpt->enable = user_disabled;
  else
    bpt->enable = disabled;

  check_duplicates (bpt->address, bpt->section);

  if (modify_breakpoint_hook)
    modify_breakpoint_hook (bpt);

  if (disable_hook)
    disable_hook (bpt);

  breakpoint_modify_event (bpt->number);
}

/* ARGSUSED */
static void
disable_command (char *args, int from_tty)
{
  register struct breakpoint *bpt;
  if (args == 0)
    ALL_BREAKPOINTS (bpt)
      switch (bpt->type)
      {
      case bp_none:
	warning ("attempted to disable apparently deleted breakpoint #%d?\n", bpt->number);
	continue;
      case bp_breakpoint:
      case bp_catch_load:
      case bp_catch_unload:
      case bp_catch_fork:
      case bp_catch_vfork:
      case bp_catch_exec:
      case bp_catch_catch:
      case bp_catch_throw:
      case bp_hardware_breakpoint:
      case bp_watchpoint:
      case bp_hardware_watchpoint:
      case bp_read_watchpoint:
      case bp_access_watchpoint:
	disable_breakpoint (bpt);
      default:
	continue;
      }
  else
    map_breakpoint_numbers (args, disable_breakpoint);
}

static void
do_enable_breakpoint (struct breakpoint *bpt, enum bpdisp disposition)
{
  struct frame_info *save_selected_frame = NULL;
  int save_selected_frame_level = -1;
  int target_resources_ok, other_type_used;
  struct value *mark;
  char *cond_string;
  args_for_parse_exp_1 args;
  char buf[1];

  /* Don't let any enable action occur on a deferred breakpoint */
  if (bpt->enable == shlib_disabled)
  {
     /* JAGaf35513 - Allow user to enable conditional breakpoint, if valid */
     if (bpt->cond_string && bpt->address && (target_read_memory (bpt->address, buf, 1) == 0))
  	{
    	    /*continue*/
        }
     else 
       return;
  }

  if (is_hw_break (bpt->type))
    {
      int i = next_hw_breakpoint (   bpt->type != bp_hardware_breakpoint
				  && bpt->type != bp_hw_catch_nomem);
      target_resources_ok = TARGET_CAN_USE_HARDWARE_WATCHPOINT (
					  bp_hardware_breakpoint, i + 1, 0);
      if (target_resources_ok == 0)
	error ("No hardware breakpoint support in the target.");
      else if (target_resources_ok < 0)
	error ("Hardware breakpoints used exceeds limit.");
    }
  if (bpt->type == bp_catch_nomem || bpt->type == bp_hw_catch_nomem)
      set_rtc_catch_nomem();

  /* JAGaf35513 - validate the condition to enable the breakpoint */
  if ((bpt->cond_string != NULL) && (bpt->enable == disabled) || (bpt->enable == shlib_disabled))
    {
	cond_string = bpt->cond_string;
	args.exp_string_p = &cond_string;
	args.block = block_for_pc (bpt->address);
	args.result = 0;
	if (!catch_errors ( (catch_errors_ftype *)
                              cover_parse_exp_1,
                              (char *) &args,
                              "",
                              RETURN_MASK_ALL))
	{	
		warning ("Error in parsing condition. Cannot enable "\
				"breakpoint #%d.", bpt->number);
		return;
	}
	else
		bpt->cond = args.result;
    }   

  if (bpt->enable != permanent)
    bpt->enable = enabled;
  bpt->disposition = disposition;
  check_duplicates (bpt->address, bpt->section);
  breakpoints_changed ();

  if (bpt->type == bp_watchpoint || bpt->type == bp_hardware_watchpoint ||
      bpt->type == bp_read_watchpoint || bpt->type == bp_access_watchpoint)
    {
      if (bpt->exp_valid_block != NULL)
	{
	  struct frame_info *fr =

	  /* Ensure that we have the current frame.  Else, this
	     next query may pessimistically be answered as, "No,
	     not within current scope". */
	  get_current_frame ();
	  fr = find_frame_addr_in_frame_chain (bpt->watchpoint_frame, NULL);
	  if (fr == NULL)
	    {
	      printf_filtered ("\
Cannot enable watchpoint %d because the block in which its expression\n\
is valid is not currently in scope.\n", bpt->number);
	      bpt->enable = disabled;
	      return;
	    }

	  save_selected_frame = selected_frame;
	  save_selected_frame_level = selected_frame_level;
	  select_frame (fr, -1);
	}

      value_free (bpt->val);
      mark = value_mark ();
      /* make sure that bpt->exp is not null because, it will be reset to null
	 in watchpoint_check() if bpt->exp cannot be evaluated.
      */ 
      char *exp_string = bpt->exp_string;
      if (bpt->exp == NULL)
        bpt->exp = parse_exp_1 (&exp_string, 0, 0);
      bpt->val = evaluate_expression (bpt->exp);
      release_value (bpt->val);
      if (VALUE_LAZY (bpt->val))
	value_fetch_lazy (bpt->val);

      if (bpt->type == bp_hardware_watchpoint ||
	  bpt->type == bp_read_watchpoint ||
	  bpt->type == bp_access_watchpoint)
	{
	  int i = hw_watchpoint_used_count (bpt->type, &other_type_used);
	  int mem_cnt = can_use_hardware_watchpoint (bpt->val);

	  target_resources_ok = TARGET_CAN_USE_HARDWARE_WATCHPOINT (
				   bpt->type, i + mem_cnt, other_type_used);
	  /* we can consider of type is bp_hardware_watchpoint, convert to 
	     bp_watchpoint in the following condition */
	  if (target_resources_ok < 0)
	    {
	      printf_filtered ("\
Cannot enable watchpoint %d because target watch resources\n\
have been allocated for other watchpoints.\n", bpt->number);
	      bpt->enable = disabled;
	      value_free_to_mark (mark);
	      return;
	    }
	}

      if (save_selected_frame_level >= 0)
	select_and_print_frame (save_selected_frame, save_selected_frame_level);
      value_free_to_mark (mark);
    }
  if (modify_breakpoint_hook)
    modify_breakpoint_hook (bpt);

  if (enable_hook)
    enable_hook (bpt);

  breakpoint_modify_event (bpt->number);
}

void
enable_breakpoint (struct breakpoint *bpt)
{
  do_enable_breakpoint (bpt, bpt->disposition);
}

/* The enable command enables the specified breakpoints (or all defined
   breakpoints) so they once again become (or continue to be) effective
   in stopping the inferior. */

/* ARGSUSED */
static void
enable_command (char *args, int from_tty)
{
  register struct breakpoint *bpt;
  if (args == 0)
    ALL_BREAKPOINTS (bpt)
      switch (bpt->type)
      {
      case bp_none:
	warning ("attempted to enable apparently deleted breakpoint #%d?\n", bpt->number);
	continue;
      case bp_breakpoint:
      case bp_procedure:
      case bp_catch_load:
      case bp_catch_unload:
      case bp_catch_fork:
      case bp_catch_vfork:
      case bp_catch_nomem:
      case bp_hw_catch_nomem:
      case bp_catch_exec:
      case bp_catch_catch:
      case bp_catch_throw:
      case bp_hardware_breakpoint:
      case bp_watchpoint:
      case bp_hardware_watchpoint:
      case bp_read_watchpoint:
      case bp_access_watchpoint:
	enable_breakpoint (bpt);
      default:
	continue;
      }
  else
    map_breakpoint_numbers (args, enable_breakpoint);
}

static void
enable_once_breakpoint (struct breakpoint *bpt)
{
  do_enable_breakpoint (bpt, disable);
}

/* ARGSUSED */
static void
enable_once_command (char *args, int from_tty)
{
  map_breakpoint_numbers (args, enable_once_breakpoint);
}

static void
enable_delete_breakpoint (struct breakpoint *bpt)
{
  do_enable_breakpoint (bpt, del);
}

/* ARGSUSED */
static void
enable_delete_command (char *args, int from_tty)
{
  map_breakpoint_numbers (args, enable_delete_breakpoint);
}

/* Use default_breakpoint_'s, or nothing if they aren't valid.  */

struct symtabs_and_lines
decode_line_spec_1 (char *string, int funfirstline)
{
  struct symtabs_and_lines sals;
  if (string == 0)
    error ("Empty line specification.");
  if (default_breakpoint_valid)
    sals = decode_line_1 (&string, funfirstline,
			  default_breakpoint_symtab, default_breakpoint_line,
			  (char ***) NULL,
			  USER_CHOICE);
  else
    sals = decode_line_1 (&string, funfirstline,
			  (struct symtab *) NULL, 0, (char ***) NULL,
			  USER_CHOICE);
  if (*string)
    error ("Junk at end of line specification: %s", string);
  return sals;
}

/* Check if there is a catch exec enabled */
int
catch_exec_enabled ()
{
  struct breakpoint *b;
  ALL_BREAKPOINTS (b)
    {
      if ((b->type == bp_catch_exec) && (b->enable == enabled))
	return 1;
    }
  return 0;
}

void
_initialize_breakpoint ()
{
  struct cmd_list_element *c;

  breakpoint_chain = 0;
  /* Don't bother to call set_breakpoint_count.  $bpnum isn't useful
     before a breakpoint is set.  */
  breakpoint_count = 0;

  add_com ("ignore", class_breakpoint, ignore_command,
	   "Set ignore-count of breakpoint number N to COUNT.\n\n\
Usage:\n\tignore <N> <COUNT>\n");
  if (xdb_commands)
    add_com_alias ("bc", "ignore", class_breakpoint, 1);

  add_com ("commands", class_breakpoint, commands_command,
	   "Set commands to be executed when the breakpoint 'N' is hit.\n\n\
Usage:\n\tcommands [<N>]\n\t[silent]\n\t[<CMD1>]\n\t[<CMD2>]\n\t[...]\n\t\
end\n\nWith no argument 'N', the targeted breakpoint is the last one set.\n\
The commands themselves follow starting on the next line 'CMD1','CMD2'...\n\
Type a line containing \"end\" to indicate the end of them.\n\
Give \"silent\" as the first line to make the breakpoint silent;\n\
then no output is printed when it is hit, except what the commands print.");
/* Command Added for the Purpose of setting breakpoints
   every debuggable procedure and associating a set of
   commands to it ,Dinesh 04/03/02 */
  add_com ("rbp", class_breakpoint, rbreak_commands,
           "Set a breakpoint for all functions and associating a set of \
commands for the breakpoints.\n\n\
Usage:\n\trbp\n\t[COMMAND1]\n\t[COMMAND2]\n\t[...]\n\tend\n");

  add_com ("xbp", class_breakpoint, xbreak_command,
           "Set a breakpoint at the exit of all functions.\n\n\
Usage:\n\txbp\n");

/* Command Added for the Purpose of deleting all breakpoins set
   by rbreak_commands ,Dinesh 04/15/02 */
  add_com ("rdp", class_breakpoint, rbreak_delete_commands,
           "Delete breakpoints for all functions set by the rbp.\n\n\
Usage:\n\trdp\n");

  add_com ("xdp", class_breakpoint, rbreak_delete_commands,
           "Delete breakpoints for all functions set by the xbp.\n\n\
Usage:\n\txdp\n");

  add_com ("condition", class_breakpoint, condition_command,
	   "Specify breakpoint number N to break only if COND is true.\n\n\
Usage:\n\tcondition <N> <COND>\n\n\
N is an integer and COND is an expression to be evaluated\n\
whenever breakpoint N is reached.");

  add_com ("tbreak", class_breakpoint, tbreak_command,
	   "Set a temporary breakpoint.\n\n\
Usage:\n\tSimilar to 'break' command.\n\n\
Args like \"break\" command.\n\
Behaves like \"break\" except the breakpoint is only temporary,\n\
so it will be deleted when hit.  Equivalent to \"break\" followed\n\
by using \"enable delete\" on the breakpoint number.");
  add_com ("txbreak", class_breakpoint, tbreak_at_finish_command,
	   "Set temporary breakpoint at procedure exit.\n\n\
Usage:\n\tSimilar to 'xbreak' command.\n\n\
Args like \"xbreak\" command.\n\
Behaves like \"xbreak\" except the breakpoint is only temporary,\n\
so it will be deleted when hit.  Equivalent to \"xbreak\" followed\n\
by using \"enable delete\" on the breakpoint number.\n");

  add_com ("hbreak", class_breakpoint, hbreak_command,
	   "Set a hardware assisted  breakpoint.\n\n\
Usage:\n\tSimilar to 'break' command.\n\n\
Args like \"break\" command.\n\
Behaves like \"break\" except the breakpoint requires hardware support,\n\
some target hardware may not have this support.");

  add_com ("thbreak", class_breakpoint, thbreak_command,
	   "Set a temporary hardware assisted breakpoint.\n\n\
Usage:\n\tSimilar to 'break' command.\n\n\
Args like \"break\" command.\n\
Behaves like \"hbreak\" except the breakpoint is only temporary,\n\
so it will be deleted when hit.");

  add_com ("suspend", class_breakpoint, suspend_command,
           "Suspend a breakpoint at line specified by argument.");

  add_com ("activate", class_breakpoint, activate_command,
           "Activate a breakpoint at line specified by argument.");

  add_prefix_cmd ("enable", class_breakpoint, enable_command,
		  "Enable some breakpoints.\n\n\
Usage:\n\tenable [breakpoints] [once | delete] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
With no subcommand, breakpoints are enabled until you command otherwise.\n\
This is used to cancel the effect of the \"disable\" command.\n\
With a subcommand you can enable temporarily.",
		  &enablelist, "enable ", 1, &cmdlist);
  if (xdb_commands)
    add_com ("ab", class_breakpoint, enable_command,
             "Enable some breakpoints.\n\n\
Usage:\n\tenable [breakpoints] [once | delete] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
With no subcommand, breakpoints are enabled until you command otherwise.\n\
This is used to cancel the effect of the \"disable\" command.\n\
With a subcommand you can enable temporarily.");

  add_com_alias ("en", "enable", class_breakpoint, 1);

  add_abbrev_prefix_cmd ("breakpoints", class_breakpoint, enable_command,
			 "Enable some breakpoints.\n\n\
Usage:\n\tenable [breakpoints] [once | delete] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
This is used to cancel the effect of the \"disable\" command.\n\
May be abbreviated to simply \"enable\".\n",
		   &enablebreaklist, "enable breakpoints ", 1, &enablelist);

  add_cmd ("once", no_class, enable_once_command,
	   "Enable breakpoints for one hit.\n\n\
Usage:\n\tenable [breakpoints] once [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
If a breakpoint is hit while enabled in this fashion, it becomes disabled.",
	   &enablebreaklist);

  add_cmd ("delete", no_class, enable_delete_command,
	   "Enable breakpoints and delete when hit.\n\n\
Usage:\n\tenable [breakpoints] delete [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
If a breakpoint is hit while enabled in this fashion, it is deleted.",
	   &enablebreaklist);

  add_cmd ("delete", no_class, enable_delete_command,
	   "Enable breakpoints and delete when hit.\n\n\
Usage:\n\tenable [breakpoints] delete [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
If a breakpoint is hit while enabled in this fashion, it is deleted.",
	   &enablelist);

  add_cmd ("once", no_class, enable_once_command,
	   "Enable breakpoints for one hit.\n\n\
Usage:\n\tenable [breakpoints] once [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
If a breakpoint is hit while enabled in this fashion, it becomes disabled.",
	   &enablelist);

  add_prefix_cmd ("disable", class_breakpoint, disable_command,
		  "Disable some breakpoints.\n\n\
Usage:\n\tdisable [breakpoints] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
To disable all breakpoints, give no argument.\n\
A disabled breakpoint is not forgotten, but has no effect until reenabled.",
		  &disablelist, "disable ", 1, &cmdlist);
  add_com_alias ("dis", "disable", class_breakpoint, 1);
  add_com_alias ("disa", "disable", class_breakpoint, 1);
  if (xdb_commands)
    add_com ("sb", class_breakpoint, disable_command,
	     "Disable some breakpoints.\n\n\
Usage:\n\tdisable [breakpoints] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
To disable all breakpoints, give no argument.\n\
A disabled breakpoint is not forgotten, but has no effect until reenabled.");

  add_cmd ("breakpoints", class_alias, disable_command,
	   "Disable some breakpoints.\n\n\
Usage:\n\tdisable [breakpoints] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
To disable all breakpoints, give no argument.\n\
A disabled breakpoint is not forgotten, but has no effect until reenabled.\n\
This command may be abbreviated \"disable\".",
	   &disablelist);

  add_prefix_cmd ("delete", class_breakpoint, delete_command,
		  "Delete some breakpoints or auto-display expressions.\n\n\
Usage:\n\tdelete [breakpoints] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
To delete all breakpoints, give no argument.\n\n\
Also a prefix command for deletion of other GDB objects.\n\
The \"unset\" command is also an alias for \"delete\".",
		  &deletelist, "delete ", 1, &cmdlist);
  add_com_alias ("d", "delete", class_breakpoint, 1);
  if (xdb_commands)
    add_com ("db", class_breakpoint, delete_command,
	     "Delete some breakpoints.\n\
Arguments are breakpoint numbers with spaces in between.\n\
To delete all breakpoints, give no argument.\n");

  add_cmd ("breakpoints", class_alias, delete_command,
           "Delete some breakpoints or auto-display expressions.\n\n\
Usage:\n\tdelete [breakpoints] [<N1>] [<N2>] [...]\n\n\
Arguments are breakpoint numbers with spaces in between.\n\
To delete all breakpoints, give no argument.\n\
This command may be abbreviated \"delete\".",
	   &deletelist);

  add_com ("clear", class_breakpoint, clear_command,
	   concat ("Clear breakpoint at specified line or function.\n\n\
Usage:\n\tclear [[<FILE>:]<LINE> | [<FILE>:]<FUNC> | [* <ADDR>]]\n\n\
Argument may be line number, function name, or \"*\" and an address.\n\
If line number is specified, all breakpoints in that line are cleared.\n\
If function is specified, breakpoints at beginning of function are cleared.\n\
If an address is specified, breakpoints at that address are cleared.\n\n",
		   "With no argument, clears all breakpoints in the line that the selected frame\n\
is executing in.\n\
\n\
See also the \"delete\" command which clears breakpoints by number.", NULL));

  add_com ("break", class_breakpoint, break_command,
	   concat ("Set breakpoint at specified line or function.\n\n\
Usage: \n\tbreak [[<FILE>:]<LINE> | [<FILE>:]<FUNC> | [*<ADDR>]]\n\
\t or \n\tbreak [+ <OFFSET> | - <OFFSET>]\n\t or \n\
\tbreak ... if <CONDITION>\n\n\
Argument may be line number, function name, or \"*\" and an address.\n\
If line number is specified, break at start of code for that line.\n\
If function is specified, break at the first source line (may not \n\
be the first assembly instruction) for that function.\n\
If an address is specified, break at that exact address.\n",
		   "With no arg, uses current execution address of selected stack frame.\n\
This is useful for breaking on return to a stack frame.\n\
\n\
Multiple breakpoints at one place are permitted, and useful if conditional.\n\
\n\
Do \"help breakpoints\" for info on other commands dealing with breakpoints.", NULL));
  add_com_alias ("b", "break", class_run, 1);
  add_com_alias ("br", "break", class_run, 1);
  add_com_alias ("bre", "break", class_run, 1);
  add_com_alias ("brea", "break", class_run, 1);

  add_com ("xbreak", class_breakpoint, break_at_finish_command,
	   concat ("Set breakpoint at procedure exit. \n\n\
Usage:\n\txbreak [[<FILE>:]<FUNC>] | [*<ADDR>]\n\n\
Argument may be function name, or \"*\" and an address.\n\
If function is specified, break at end of code for that function.\n\
If an address is specified, break at the end of the function that contains \n\
that exact address.\n",
		   "With no arg, uses current execution address of selected stack frame.\n\
This is useful for breaking on return to a stack frame.\n\
\n\
Multiple breakpoints at one place are permitted, and useful if conditional.\n\
\n\
Do \"help breakpoints\" for info on other commands dealing with breakpoints.", NULL));
  add_com_alias ("xb", "xbreak", class_breakpoint, 1);
  add_com_alias ("xbr", "xbreak", class_breakpoint, 1);
  add_com_alias ("xbre", "xbreak", class_breakpoint, 1);
  add_com_alias ("xbrea", "xbreak", class_breakpoint, 1);

  if (xdb_commands)
    {
      add_com_alias ("ba", "break", class_breakpoint, 1);
      add_com_alias ("bu", "ubreak", class_breakpoint, 1);
      add_com ("bx", class_breakpoint, break_at_finish_at_depth_command,
	       "Set breakpoint at procedure exit.  Either there should\n\
be no argument or the argument must be a depth.\n");
    }

  if (dbx_commands)
    {
      add_com ("status", class_info, breakpoints_info,
	       concat ("Status of user-settable breakpoints, or breakpoint number NUMBER.\n\
The \"Type\" column indicates one of:\n\
\tbreakpoint     - normal breakpoint\n\
\twatchpoint     - watchpoint\n\
The \"Disp\" column contains one of \"keep\", \"del\", or \"dis\" to indicate\n\
the disposition of the breakpoint after it gets hit.  \"dis\" means that the\n\
breakpoint will be disabled.  The \"Address\" and \"What\" columns indicate the\n\
address and file/line number respectively.\n\n",
		       "Convenience variable \"$_\" and default examine address for \"x\"\n\
are set to the address of the last breakpoint listed.\n\n\
Convenience variable \"$bpnum\" contains the number of the last\n\
breakpoint set.", NULL));
    }
/* JAGae68084 - added breakall command */
/*add_com ("breakall", class_breakpoint, breakall_command,
           concat ("Set persisitent breakpoint at specified function.\n\
 .These breakpoints are placed for any subsequent shared library loads\n\
If function is specified, break at the first source line (may not \n\
be the first assembly instruction) for that function.\n\
Do \"help breakpoints\" for info on other commands dealing with breakpoints.", NULL));

add_com_alias ("ball", "breakall", class_run, 1);
*/
 /* JAGae68084 - added rbreakall command */
 /* add_com ("rbreakall", class_breakpoint, rbreakall_command,
             concat ("Set persisitent regular expression breakpoint at specified function.\n\
 .These breakpoints are placed for any subsequent shared library loads\n\
If function is specified, break at the first source line (may not \n\
be the first assembly instruction) for that function.\n\
Do \"help breakpoints\" for info on other commands dealing with breakpoints.", NULL));

add_com_alias ("rball", "rbreakall", class_run, 1);
*/
/* JAGae68084- added xbreakall command */
/*add_com ("xbreakall", class_breakpoint, xbreakall_command,
           concat ("Set persistent breakpoint at procedure exit. \n\
Argument is a function name.\n\
If function is specified, break at end of code for that function.\n\
Do \"help breakpoints\" for info on other commands dealing with breakpoints.", NULL));

  add_com_alias ("xball", "xbreakall", class_run, 1);
*/
  /* JAGaf54451 - BEG
     <stop in/at> command was 'dbx' specific till now. Its functionality is the
     same as 'break' command. It is enabled for both dbx and non-dbx modes */
     stop_hook_command = add_abbrev_prefix_cmd ("stop", class_breakpoint, stop_command,
     "Break in function/address or break at a line in the current file.\n\n\
Hint : You can define hook-stop to set a list of commands to \n\
       be run each time execution of the program stops.",
                                             &stoplist, "stop ", 1, &cmdlist);
      add_cmd ("in", class_breakpoint, stopin_command,
	       "Break in function or address.\n", &stoplist);
      add_cmd ("at", class_breakpoint, stopat_command,
	       "Break at a line in the current file.\n", &stoplist);
  /* JAGaf54451 - END */	       

  add_info ("breakpoints", breakpoints_info,
	    concat ("Status of user-settable breakpoints, or breakpoint number 'N'.\n\n\
Usage:\n\tinfo breakpoints [<N>]\n\t or\n\tinfo watchpoints [<N>]\n\n\
The \"Type\" column indicates one of:\n\
\tbreakpoint     - normal breakpoint\n\
\twatchpoint     - watchpoint\n\
The \"Disp\" column contains one of \"keep\", \"del\", or \"dis\" to indicate\n\
the disposition of the breakpoint after it gets hit.  \"dis\" means that the\n\
breakpoint will be disabled.  The \"Address\" and \"What\" columns indicate the\n\
address and file/line number respectively.\n\n",
		    "Convenience variable \"$_\" and default examine address for \"x\"\n\
are set to the address of the last breakpoint listed.\n\n\
Convenience variable \"$bpnum\" contains the number of the last\n\
breakpoint set.", NULL));

  if (xdb_commands)
    add_com ("lb", class_breakpoint, breakpoints_info,
	     concat ("Status of user-settable breakpoints, or breakpoint number NUMBER.\n\
The \"Type\" column indicates one of:\n\
\tbreakpoint     - normal breakpoint\n\
\twatchpoint     - watchpoint\n\
The \"Disp\" column contains one of \"keep\", \"del\", or \"dis\" to indicate\n\
the disposition of the breakpoint after it gets hit.  \"dis\" means that the\n\
breakpoint will be disabled.  The \"Address\" and \"What\" columns indicate the\n\
address and file/line number respectively.\n\n",
		     "Convenience variable \"$_\" and default examine address for \"x\"\n\
are set to the address of the last breakpoint listed.\n\n\
Convenience variable \"$bpnum\" contains the number of the last\n\
breakpoint set.", NULL));

#if MAINTENANCE_CMDS

  add_cmd ("breakpoints", class_maintenance, maintenance_info_breakpoints,
	   concat ("Status of all breakpoints or of breakpoint number 'N'.\n\n\
Usage:\n\tmaintenance info breakpoints [<N>]\n\n\
The \"Type\" column indicates one of:\n\
\tbreakpoint     - normal breakpoint\n\
\twatchpoint     - watchpoint\n\
\tlongjmp        - internal breakpoint used to step through longjmp()\n\
\tlongjmp resume - internal breakpoint at the target of longjmp()\n\
\tuntil          - internal breakpoint used by the \"until\" command\n\
\tfinish         - internal breakpoint used by the \"finish\" command\n",
		   "The \"Disp\" column contains one of \"keep\", \"del\", or \"dis\" to indicate\n\
the disposition of the breakpoint after it gets hit.  \"dis\" means that the\n\
breakpoint will be disabled.  The \"Address\" and \"What\" columns indicate the\n\
address and file/line number respectively.\n\n",
		   "Convenience variable \"$_\" and default examine address for \"x\"\n\
are set to the address of the last breakpoint listed.\n\n\
Convenience variable \"$bpnum\" contains the number of the last\n\
breakpoint set.", NULL),
	   &maintenanceinfolist);

#endif /* MAINTENANCE_CMDS */

  add_com ("catch", class_breakpoint, catch_command,
	   "Set catchpoints to catch events.\n\nUsage:\n\
Raised signals may be caught:\n\
\tcatch signal              - all signals\n\
\tcatch signal <SIGNAL>     - a particular signal\n\
Raised exceptions may be caught:\n\
\tcatch throw               - all exceptions, when thrown\n\
\tcatch throw <EXCEPTION>   - a particular exception, when thrown\n\
\tcatch catch               - all exceptions, when caught\n\
\tcatch catch <EXCEPTION>   - a particular exception, when caught\n\
Thread or process events may be caught:\n\
\tcatch thread_start        - any threads, just after creation\n\
\tcatch thread_exit         - any threads, just before expiration\n\
\tcatch thread_join         - any threads, just after joins\n\
Process events may be caught:\n\
\tcatch start               - any processes, just after creation\n\
\tcatch exit                - any processes, just before expiration\n\
\tcatch fork                - calls to fork()\n\
\tcatch vfork               - calls to vfork()\n\
\tcatch exec                - calls to exec()\n\
Dynamically-linked library events may be caught:\n\
\tcatch load                - loads of any library\n\
\tcatch load <LIB>          - loads of a particular library\n\
\tcatch unload              - unloads of any library\n\
\tcatch unload <LIB>        - unloads of a particular library\n\
The act of your program's execution stopping may also be caught:\n\
\tcatch stop\n\n\
Memory leaks may be caught:\n\
\tcatch leaks               - all leaks (with info leaks)\n\
C++ exceptions may be caught:\n\
\tcatch throw               - all exceptions, when thrown\n\
\tcatch catch               - all exceptions, when caught\n\
Memory allocation failure may be caught:\n\
\tcatch nomem               - NULL pointer return by memory allocators;\n\
\t                            used with set heap-check on, with/without\n\
\t                            null-check enabled\n\
\n\
Do \"help set follow-fork-mode\" for info on debugging your program\n\
after a fork or vfork is caught.\n\n\
Do \"help breakpoints\" for info on other commands dealing with breakpoints.");

  add_com ("tcatch", class_breakpoint, tcatch_command,
	   "Set temporary catchpoints to catch events.\n\n\
Usage:\n\tSimilar to 'catch' command.\n\n\
Args like \"catch\" command.\n\
Like \"catch\" except the catchpoint is only temporary,\n\
so it will be deleted when hit.  Equivalent to \"catch\" followed\n\
by using \"enable delete\" on the catchpoint number.");

  add_com ("watch", class_breakpoint, watch_command,
	   "Set a watchpoint for an expression, EXP.\n\n\
Usage:\n\twatch <EXP>\n\n\
A watchpoint stops execution of your program whenever the value of\n\
an expression changes.");

add_com ("watch_target", class_breakpoint, watch_target_command,
           "Set a watchpoint on the target memory location of an expression, EXP.\n\n\
Usage:\n\twatch_target <EXP>\n\n\
Expression is assumed to be of a pointer type.\n\
A watchpoint stops execution of your program whenever the value at\n\
the target memory location changes.");

  add_com ("rwatch", class_breakpoint, rwatch_command,
	   "Set a read watchpoint for an expression, EXP.\n\n\
Usage:\n\trwatch <EXP>\n\n\
A watchpoint stops execution of your program whenever the value of\n\
an expression is read.");

  add_com ("awatch", class_breakpoint, awatch_command,
	   "Set a watchpoint for an expression, EXP.\n\n\
Usage:\n\tawatch <EXP>\n\n\
A watchpoint stops execution of your program whenever the value of\n\
an expression is either read or written.");

  add_info ("watchpoints", breakpoints_info,
	    "Synonym for ``info breakpoints''.");


  c = add_set_cmd ("can-use-hw-watchpoints", class_support, var_zinteger,
		   (char *) &can_use_hw_watchpoints,
		   "Set debugger's willingness to use watchpoint hardware.\n\
Usage:\nTo set new value:\n\tset can-use-hw-watchpoints <INTEGER>\nTo see current\
 value:\n\tshow can-use-hw-watchpoints\n\n\
If zero, gdb will not use hardware for new watchpoints, even if\n\
such is available.  (However, any hardware watchpoints that were\n\
created before setting this to nonzero, will continue to use watchpoint\n\
hardware.)",
		   &setlist);
  add_show_from_set (c, &showlist);

  can_use_hw_watchpoints = 1;

  c = add_set_cmd ("debug_trace", class_support, var_zinteger,
		   (char *) &debug_traces,
		   "Set printing of  debug traces. If non-zero, gdb will \n\
print information about its internal workings as it works.",
		   &setlist);
  add_show_from_set (c, &showlist);

  debug_traces = 0;
}

/* JAGaf54436: Removed functions create_break_after_load_bp() and
   create_one_break_shared_load_bp()
*/

/* Return TRUE (1) if we should stop for this breakpoint, otherwise
   return 0.  If there is a breakpoint condition it will be evaluated.
   If the ignore count is decremented, the routine annotate_ignore_count_change
   is called.
 */

int is_stop_for_bp (struct breakpoint * bp)
{
  int value_is_zero = 0;

  if (bp->cond)
    {
      /* Need to select the frame, with all that implies
	 so that the conditions will have the right context.  */
      select_frame (get_current_frame (), 0);
      value_is_zero
	= catch_errors ((catch_errors_ftype *) breakpoint_cond_eval, (char *) (bp->cond),
			"Error in testing breakpoint condition:\n",
			RETURN_MASK_ALL);
      /* FIXME-someday, should give breakpoint # */
      free_all_values ();
    }
  if (bp->cond && value_is_zero)
    {
      return 0;  /* FALSE condition, don't stop. */
    }
  else if (bp->ignore_count > 0)
    {
      bp->ignore_count--;
      annotate_ignore_count_change ();
      return 0;
    }
  else
    {
      /* We will stop here */

      return 1;
    }
} /* end is_stop_for_bp */


/* Remove thread tracing event breakpoints */
void
remove_thread_tracing_breakpoints()
{
  register struct breakpoint *b, *temp = NULL;

  ALL_BREAKPOINTS_SAFE (b, temp)
    if (b->type == bp_thread_tracing_event) 
      delete_breakpoint (b);
}


/* Create a thread tracing event breakpoint */
void
create_thread_tracing_breakpoint(char* fname)
{
  create_deferred_event_breakpoint(fname, bp_thread_tracing_event);
}
