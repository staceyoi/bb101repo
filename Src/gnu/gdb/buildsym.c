/* Support routines for building symbol tables in GDB's internal format.
   Copyright 1986-2000 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* This module provides subroutines used for creating and adding to
   the symbol table.  These routines are called from various symbol-
   file-reading routines.

   Routines to support specific debugging information formats (stabs,
   DWARF, etc) belong somewhere else. */

#include "defs.h"
#include "demangle.h"
#include "bfd.h"
#include "obstack.h"
#include "symtab.h"
#include "symfile.h"		/* Needed for "struct complaint" */
#include "objfiles.h"
#include "gdbtypes.h"
#include "complaints.h"
#include "gdb_string.h"
#include "expression.h"		/* For "enum exp_opcode" used by... */
#include "language.h"		/* For "longest_local_hex_string_custom" */
#include "bcache.h"
#ifdef HP_IA64
#include "macrotab.h"
#include <libgen.h>
#endif

/* Ask buildsym.h to define the vars it normally declares `extern'.  */
/* This is incredibly gross.  FIXME. */
#ifdef EXTERN
#undef EXTERN
#endif
#define	EXTERN
/**/
#include "buildsym.h"		/* Our own declarations */
#undef	EXTERN
#define EXTERN extern

/* For cleanup_undefined_types and finish_global_stabs (somewhat
   questionable--see comment where we call them).  */

#include "stabsread.h"

#define DEBUG(x)  

/* List of free `struct pending' structures for reuse.  */

struct pending *free_pendings;

/* Non-zero if symtab has line number info.  This prevents an
   otherwise empty symtab from being tossed.  */
struct pending_block *pending_blocks = NULL;

static int have_line_numbers;

/* Non-zero if the debug info has special entries to perform steplast for C++*/
int is_steplast_eligible = 0;

static int compare_line_numbers (const void *ln1p, const void *ln2p);


/* Initial sizes of data structures.  These are realloc'd larger if
   needed, and realloc'd down to the size actually used, when
   completed.  */

#define	INITIAL_CONTEXT_STACK_SIZE	10
#define	INITIAL_LINE_VECTOR_LENGTH	1000

/* aCC sets the 6th bit & 11th bit in line nos. for marking special
   SLT entries for normal and DOC entries */
#define STEPLAST_MARKER_CONST 0x4000000
#define STEPLAST_MARKER_CONST_DOC2 0x200000

#define	INITIAL_FUNCEXIT_LINE_VECTOR_LENGTH	20
#define	INITIAL_STEPLAST_LINE_VECTOR_LENGTH	100


/* msort is in sort.c in libiberty.  Added to have a stable sort to use
 * in place of qsort.
 */

extern void msort(void *base,
                  size_t nel,
                  size_t size,
                  int (*compar)(const void *, const void *)
                 );

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
/* From hpux-inline.c */
extern void get_index_given_pc(CORE_ADDR, int, int *);
extern int dwarf2_read_minimal;
#endif

/* Complaints about the symbols we have encountered.  */

struct complaint block_end_complaint =
{"block end address less than block start address in %s (patched it)", 0, 0};

struct complaint anon_block_end_complaint =
{"block end address 0x%lx less than block start address 0x%lx (patched it)", 0, 0};

struct complaint innerblock_complaint =
{"inner block not inside outer block in %s", 0, 0};

struct complaint innerblock_anon_complaint =
{"inner block (0x%lx-0x%lx) not inside outer block (0x%lx-0x%lx)", 0, 0};

struct complaint blockvector_complaint =
{"block at %s out of order", 0, 0};

/* maintain the lists of symbols and blocks */

/* Add a pending list to free_pendings. */
void
add_free_pendings (struct pending *list)
{
  register struct pending *link = list;

  if (list)
    {
      while (link->next) link = link->next;
      link->next = free_pendings;
      free_pendings = list;
    }
}
      
/* Add a symbol to one of the lists of symbols.  */

void
add_symbol_to_list (struct symbol *symbol, struct pending **listhead)
{
  register struct pending *link;

  /* If this is an alias for another symbol, don't add it.  */
#if 0
  /* srikanth, 000208, this unabashed gccism that came in with 4.17,
     is causing problems for the Wildebeest. HP aCC compiler emits an
     internal parameter called #free for destructors to signal whether
     they should call free on the this pointer or not. For instance, if
     you say delete p, the destructor must call free, while for auto
     objects, the destructor should cleanup but not attempt to free the
     space. (JAGac42748) */
  /* RM: #aggretx is not an alias but an implicit parameter added by C++ */
  if (symbol->ginfo.name && symbol->ginfo.name[0] == '#' &&
      strncmp(symbol->ginfo.name, "#aggretx", 8))
    return;
#endif

  /* We keep PENDINGSIZE symbols in each link of the list. If we
     don't have a link with room in it, add a new link.  */
  if (*listhead == NULL || (*listhead)->nsyms == PENDINGSIZE)
    {
      if (free_pendings)
	{
	  link = free_pendings;
	  free_pendings = link->next;
	}
      else
	{
	  link = (struct pending *) xmalloc (sizeof (struct pending));
	}

      link->next = *listhead;
      *listhead = link;
      link->nsyms = 0;
    }

  (*listhead)->symbol[(*listhead)->nsyms++] = symbol;

  SYMBOL_OBSOLETED (symbol) = false;
}

/* Find a symbol named NAME on a LIST.  NAME need not be
   '\0'-terminated; LENGTH is the length of the name.  */

struct symbol *
find_symbol_in_list (struct pending *list, char *name, int length)
{
  int j;
  char *pp;

  while (list != NULL)
    {
      for (j = list->nsyms; --j >= 0;)
	{
	  pp = SYMBOL_NAME (list->symbol[j]);
	  if (*pp == *name && strncmp (pp, name, length) == 0 &&
	      pp[length] == '\0')
	    {
	      return (list->symbol[j]);
	    }
	}
      list = list->next;
    }
  return (NULL);
}

/* At end of reading syms, or in case of quit, really free as many
   `struct pending's as we can easily find. */

/* ARGSUSED */
void
really_free_pendings (PTR dummy)
{
  struct pending *next, *next1;

  for (next = free_pendings; next; next = next1)
    {
      next1 = next->next;
      free ((void *) next);
    }
  free_pendings = NULL;

  free_pending_blocks ();

  for (next = file_symbols; next != NULL; next = next1)
    {
      next1 = next->next;
      free ((void *) next);
    }
  file_symbols = NULL;

  for (next = global_symbols; next != NULL; next = next1)
    {
      next1 = next->next;
      free ((void *) next);
    }
  global_symbols = NULL;

#ifdef HP_IA64
  if (pending_macros)
    free_macro_table (pending_macros);
  pending_macros = NULL;
#endif
}

/* This function is called to discard any pending blocks. */

void
free_pending_blocks (void)
{
#if 0				/* Now we make the links in the
				   symbol_obstack, so don't free
				   them.  */
  struct pending_block *bnext, *bnext1;

  for (bnext = pending_blocks; bnext; bnext = bnext1)
    {
      bnext1 = bnext->next;
      free ((void *) bnext);
    }
#endif
  pending_blocks = NULL;
}

static int block_inline_index = 0;
/* Take one of the lists of symbols and make a block from it.  Keep
   the order the symbols have in the list (reversed from the input
   file).  Put the block on the list of pending blocks.  */

void
finish_block (struct symbol *symbol, struct pending **listhead,
	      struct pending_block *old_blocks,
	      CORE_ADDR start, CORE_ADDR end,
	      struct objfile *objfile, int is_inlined)
{
  register struct pending *next, *next1;
  register struct block *block;
  register struct pending_block *pblock;
  struct pending_block *opblock;
  register int i = 0;
  register int j;
  struct type *type;

  /* Dwarf for ILP32 treats addresses as 32-bits.  Adjust here to be
     real addresses.
     */

  start = SWIZZLE (start);
  end = SWIZZLE (end);

  type = symbol ? SYMBOL_TYPE(symbol) : NULL;
  
  /* Count the length of the list of symbols.  */
  if ((type? TYPE_CODE (type):NULL) != TYPE_CODE_USINGDIR)
    for (next = *listhead, i = 0;
	 next;
	 i += next->nsyms, next = next->next)
      {
	/*EMPTY */ ;
      }

 
  /* Can have a block with no symbols, if we have an inlined instance
     with no arguments or locals. */ 
  
  if (i == 0)
    block = (struct block *)(void *) obstack_alloc (&objfile->symbol_obstack,
	    (sizeof (struct block) ));
  else
    block = (struct block *)(void *) obstack_alloc (&objfile->symbol_obstack,
	    (sizeof (struct block) + ((i - 1) * sizeof (struct symbol *))));

  if (is_inlined)/* Check if the pending block is inlined block */
    block->is_inlined = 1;;

  block->inline_idx = block_inline_index;

  /* Copy the symbols into the block.  */

  BLOCK_NSYMS (block) = i;
  if ((type? TYPE_CODE (type):NULL)!=TYPE_CODE_USINGDIR)
    for (next = *listhead; next; next = next->next)
      {
	for (j = next->nsyms - 1; j >= 0; j--)
	  {
	    BLOCK_SYM (block, --i) = next->symbol[j];
	  }
      }

  BLOCK_START (block) = SWIZZLE(start);
  BLOCK_END (block) = SWIZZLE(end);
  /* Superblock filled in when containing block is made */
  BLOCK_SUPERBLOCK (block) = NULL;
  BLOCK_ALIASES (block) = NULL;
  BLOCK_NAMESPACE (block) = NULL; 
  BLOCK_FUNCTION (block) = NULL;

  BLOCK_GCC_COMPILED (block) = processing_gcc_compilation;

  /* Put the block in as the value of the symbol that names it.  */

  if (symbol)
    {
      struct type *ftype = SYMBOL_TYPE (symbol);
      SYMBOL_BLOCK_VALUE (symbol) = block;
      if (TYPE_CODE (ftype) != TYPE_CODE_USINGDIR)
	{
	  BLOCK_FUNCTION (block) = symbol;
	  
	  if (TYPE_NFIELDS (ftype) <= 0)
	    {
	      /* No parameter type information is recorded with the function's
		 type.  Set that from the type of the parameter symbols. */
	      int nparams = 0, iparams;
	      struct symbol *sym;
	      for (i = 0; i < BLOCK_NSYMS (block); i++)
	  /* No parameter type information is recorded with the
	     function's type.  Set that from the type of the
	     parameter symbols. */
		{
		  sym = BLOCK_SYM (block, i);
		  switch (SYMBOL_CLASS (sym))
		    {
		    case LOC_ARG:
		    case LOC_REF_ARG:
		    case LOC_REGPARM:
		    case LOC_REGPARM_ADDR:
		    case LOC_BASEREG_ARG:
		    case LOC_BASEREG_REF_ARG:
		    case LOC_LOCAL_ARG:
		    case LOC_COMPUTED_ARG:
		      nparams++;
		      break;
		    case LOC_UNDEF:
		    case LOC_CONST:
		    case LOC_STATIC:
		    case LOC_INDIRECT:
		    case LOC_REGISTER:
		    case LOC_LOCAL:
		    case LOC_TYPEDEF:
		    case LOC_LABEL:
		    case LOC_BLOCK:
		    case LOC_CONST_BYTES:
		    case LOC_BASEREG:
		    case LOC_UNRESOLVED:
		    case LOC_OPTIMIZED_OUT:
		    case LOC_COMPUTED:
		    default:
		      break;
		    }
		}
	    
	      if (nparams > 0)
		{
		  TYPE_NFIELDS (ftype) = nparams;
		  TYPE_FIELDS (ftype) = (struct field *)
		    TYPE_ALLOC (ftype, nparams * sizeof (struct field));
		  
		  for (i = iparams = 0; iparams < nparams; i++)
		    {
		      sym = BLOCK_SYM (block, i);
		      switch (SYMBOL_CLASS (sym))
			{
			case LOC_ARG:
			case LOC_REF_ARG:
			case LOC_REGPARM:
			case LOC_REGPARM_ADDR:
			case LOC_BASEREG_ARG:
			case LOC_BASEREG_REF_ARG:
			case LOC_LOCAL_ARG:
		        case LOC_COMPUTED_ARG:
			  TYPE_FIELD_TYPE (ftype, iparams) = SYMBOL_TYPE (sym);
			  iparams++;
			  break;
			case LOC_UNDEF:
			case LOC_CONST:
			case LOC_STATIC:
			case LOC_INDIRECT:
			case LOC_REGISTER:
			case LOC_LOCAL:
			case LOC_TYPEDEF:
			case LOC_LABEL:
			case LOC_BLOCK:
			case LOC_CONST_BYTES:
			case LOC_BASEREG:
			case LOC_UNRESOLVED:
			case LOC_OPTIMIZED_OUT:
			case LOC_COMPUTED:
			default:
			  break;
			}
		    }
		}
	    }
	}
      else
	{
	  BLOCK_NAMESPACE (block) = symbol;
	  BLOCK_FUNCTION (block) = NULL;
	}
    }
  else
    {
      BLOCK_FUNCTION (block) = NULL;
    }
  
  /* Now "free" the links of the list, and empty the list.  */
  if ((type? TYPE_CODE (type):NULL) != TYPE_CODE_USINGDIR)
    {
      for (next = *listhead; next; next = next1)
	{
	  next1 = next->next;
	  next->next = free_pendings;
	  free_pendings = next;
	}
      *listhead = NULL;
    }

#if 1
  /* Check to be sure that the blocks have an end address that is
     greater than starting address */

  if (BLOCK_END (block) < BLOCK_START (block))
    {
      if (symbol)
	{
	  complain (&block_end_complaint, SYMBOL_SOURCE_NAME (symbol));
	}
      else
	{
	  complain (&anon_block_end_complaint, BLOCK_END (block), BLOCK_START (block));
	}
      /* Better than nothing */
      BLOCK_END (block) = BLOCK_START (block);
    }
#endif

  /* Install this block as the superblock of all blocks made since the
     start of this scope that don't have superblocks yet.  */

  opblock = NULL;
  for (pblock = pending_blocks; pblock != old_blocks; pblock = pblock->next)
    {
      if (BLOCK_SUPERBLOCK (pblock->block) == NULL)
	{
          /* Check to be sure the blocks are nested as we receive
             them. Check to see if the pending block is completely within the
             the current block. If the compiler/assembler/linker work, this just
             burns a small amount of time.  */
          if (SWIZZLE(BLOCK_START (pblock->block)) < BLOCK_START (block) ||
              SWIZZLE(BLOCK_END (pblock->block)) > BLOCK_END (block))
	    {
	      if (symbol)
		{
		  complain (&innerblock_complaint,
			    SYMBOL_SOURCE_NAME (symbol));
		}
	      else
		{
		  complain (&innerblock_anon_complaint, SWIZZLE(BLOCK_START (pblock->block)),
			    SWIZZLE(BLOCK_END (pblock->block)), BLOCK_START (block),
			    BLOCK_END (block));
		}
	    }
	  BLOCK_SUPERBLOCK (pblock->block) = block;
	}
      opblock = pblock;
    }

  record_pending_block (objfile, block, opblock);
}

void
do_finish_block (struct symbol *symbol, struct pending **listhead,
	      struct pending_block *old_blocks,
	      CORE_ADDR start, CORE_ADDR end,
	      struct objfile *objfile, int is_inlined, int inline_index)
{
    block_inline_index = inline_index;
    finish_block (symbol, listhead, old_blocks, start, end, objfile, is_inlined);
    block_inline_index = 0;
}

/* Record BLOCK on the list of all blocks in the file.  Put it after
   OPBLOCK, or at the beginning if opblock is NULL.  This puts the
   block in the list after all its subblocks.

   Allocate the pending block struct in the symbol_obstack to save
   time.  This wastes a little space.  FIXME: Is it worth it?  */

void
record_pending_block (struct objfile *objfile, struct block *block,
		      struct pending_block *opblock)
{
  register struct pending_block *pblock;

  pblock = (struct pending_block *)(void *)
    obstack_alloc (&objfile->symbol_obstack, (int) sizeof (struct pending_block));
  pblock->block = block;
  if (opblock)
    {
      pblock->next = opblock->next;
      opblock->next = pblock;
    }
  else
    {
      pblock->next = pending_blocks;
      pending_blocks = pblock;
    }
}

/* Note that this is only used in this file and in dstread.c, which
   should be fixed to not need direct access to this function.  When
   that is done, it can be made static again. */

struct blockvector *
make_blockvector (struct objfile *objfile)
{
  extern char *longest_local_hex_string (LONGEST num);

  register struct pending_block *next;
  register struct blockvector *blockvector;
  register int i;

  /* Count the length of the list of blocks.  */

  for (next = pending_blocks, i = 0; next; next = next->next, i++)
    {;
    }

  blockvector = (struct blockvector *)(void *)
    obstack_alloc (&objfile->symbol_obstack,
		   (sizeof (struct blockvector)
		    + (i - 1) * sizeof (struct block *)));

  /* Copy the blocks into the blockvector. This is done in reverse
     order, which happens to put the blocks into the proper order
     (ascending starting address). finish_block has hair to insert
     each block into the list after its subblocks in order to make
     sure this is true.  */

  BLOCKVECTOR_NBLOCKS (blockvector) = i;
  for (next = pending_blocks; next; next = next->next)
    {
      BLOCKVECTOR_BLOCK (blockvector, --i) = next->block;
    }

#if 0				/* Now we make the links in the
				   obstack, so don't free them.  */
  /* Now free the links of the list, and empty the list.  */

  for (next = pending_blocks; next; next = next1)
    {
      next1 = next->next;
      free (next);
    }
#endif
  pending_blocks = NULL;

#if 1				/* FIXME, shut this off after a while
				   to speed up symbol reading.  */
  /* Some compilers output blocks in the wrong order, but we depend on
     their being in the right order so we can binary search. Check the
     order and moan about it.  FIXME.  */
  if (BLOCKVECTOR_NBLOCKS (blockvector) > 1)
    {
      for (i = 1; i < BLOCKVECTOR_NBLOCKS (blockvector); i++)
	{
	  if (SWIZZLE(BLOCK_START (BLOCKVECTOR_BLOCK (blockvector, i - 1)))
	      > SWIZZLE(BLOCK_START (BLOCKVECTOR_BLOCK (blockvector, i))))
	    {
	      CORE_ADDR start
		= SWIZZLE(BLOCK_START (BLOCKVECTOR_BLOCK (blockvector, i)));

	      complain (&blockvector_complaint,
			longest_local_hex_string ((LONGEST) start));
	    }
	}
    }
#endif

  return (blockvector);
}

#ifdef HP_IA64
#define MAX_PATH_SIZE 1024
/* JAGag44406: helper function used in start_subfile.
   Given an input file name and an input directory name, get the real path
   for the file. split the real path into a basename and directory name and
   pass back.
*/
void
get_file_and_dir_name (char  *input_file_name,
                       char  *input_dir_name,
                       char **output_file_name,
                       char **output_dir_name)
{
  char dup_name [MAX_PATH_SIZE];
  char *real_path1 = NULL;
  char *real_path2 = NULL;

  if (input_file_name [0] == '/')
    {
      strcpy (dup_name, input_file_name);
    }
  else
    {
      /* If this is a relative path, concatenate input_dir_name and
         input_file_name to get an absolute path. */
      int len = 0;
      if (input_dir_name)
        {
          len = strlen (input_dir_name);
          strcpy (dup_name, input_dir_name);
          dup_name [len] = '/';
        }
      strcpy (dup_name + len + (input_dir_name == NULL? 0: 1), input_file_name);
    }

  real_path1 = pathopt (dup_name);
  real_path2 = strdup (real_path1);

  *output_file_name = strdup (getbasename (real_path1));
  *output_dir_name = strdup (dirname (real_path2));

  free (real_path1);
  free (real_path2);
}
#endif


/* Start recording information about source code that came from an
   included (or otherwise merged-in) source file with a different
   name.  NAME is the name of the file (cannot be NULL), DIRNAME is
   the directory in which it resides (or NULL if not known).  */

void
start_subfile (char *name, char *dirname1)
{
  register struct subfile *subfile;

  /* See if this subfile is already known as a subfile of the current
     main source file.  */

  for (subfile = subfiles; subfile; subfile = subfile->next)
    {
      if (STREQ (subfile->name, name))
	{
	  current_subfile = subfile;
	  return;
	}
    }

#ifdef HP_IA64
  /* JAGag44406: f90 +U77 gdb line number not found while setting breakpoint. */
  /* For absolute or relative paths, ensure that we do an apples to apples
     comparison. For the passed name and the names of all the subfiles, get the
     real path, get the basenames and directory names and compare them. 
   */
  if (    subfiles
      && language_fortran == deduce_language_from_filename (name))
    {
      char *basename = NULL;
      char *dir_name = NULL;

      get_file_and_dir_name (name, dirname1, &basename, &dir_name);

      for (subfile = subfiles; subfile; subfile = subfile->next)
        {
          char *subfile_basename = NULL;
          char *subfile_dirname = NULL;

          if (language_fortran != subfile->language)
            {
              continue;
            }

          get_file_and_dir_name (name, dirname1,
                                 &subfile_basename, &subfile_dirname);

          if (    STREQ (subfile_basename, basename) 
               && STREQ (subfile_dirname, dir_name))
	    {
	      current_subfile = subfile;
              free (basename);
              free (dir_name);
              free (subfile_basename);
              free (subfile_dirname);
	      return;
	    }

          free (subfile_basename);
          free (subfile_dirname);
        }

      free (basename);
      free (dir_name);
    }
#endif

  /* This subfile is not known.  Add an entry for it. Make an entry
     for this subfile in the list of all subfiles of the current main
     source file.  */

  subfile = (struct subfile *) xmalloc (sizeof (struct subfile));
  memset ((char *) subfile, 0, sizeof (struct subfile));
  subfile->next = subfiles;
  subfiles = subfile;
  current_subfile = subfile;

  /* Save its name and compilation directory name */
  subfile->name = (name == NULL) ? NULL : savestring (name, (int) strlen (name));
  subfile->dirname =
    (dirname1 == NULL) ? NULL : savestring (dirname1, (int) strlen (dirname1));

  /* Initialize line-number recording for this subfile.  */
  subfile->line_vector = NULL;
  subfile->doc_line_vector = NULL;
  subfile->funcexit_line_vector = NULL;
  subfile->steplast_line_vector = NULL;

  /* Default the source language to whatever can be deduced from the
     filename.  If nothing can be deduced (such as for a C/C++ include
     file with a ".h" extension), then inherit whatever language the
     previous subfile had.  This kludgery is necessary because there
     is no standard way in some object formats to record the source
     language.  Also, when symtabs are allocated we try to deduce a
     language then as well, but it is too late for us to use that
     information while reading symbols, since symtabs aren't allocated
     until after all the symbols have been processed for a given
     source file. */

  subfile->language = deduce_language_from_filename (subfile->name);
  if (subfile->language == language_unknown &&
      subfile->next != NULL)
    {
      subfile->language = subfile->next->language;
    }

  /* Initialize the debug format string to NULL.  We may supply it
     later via a call to record_debugformat. */
  subfile->debugformat = NULL;

  /* cfront output is a C program, so in most ways it looks like a C
     program.  But to demangle we need to set the language to C++.  We
     can distinguish cfront code by the fact that it has #line
     directives which specify a file name ending in .C.

     So if the filename of this subfile ends in .C, then change the
     language of any pending subfiles from C to C++.  We also accept
     any other C++ suffixes accepted by deduce_language_from_filename
     (in particular, some people use .cxx with cfront).  */
  /* Likewise for f2c.  */

  if (subfile->name)
    {
      struct subfile *s;
      enum language sublang = deduce_language_from_filename (subfile->name);

      if (sublang == language_cplus || sublang == language_fortran)
	for (s = subfiles; s != NULL; s = s->next)
	  if (s->language == language_c)
	    s->language = sublang;
    }

  /* And patch up this file if necessary.  */
  if (subfile->language == language_c
      && subfile->next != NULL
      && (subfile->next->language == language_cplus
	  || subfile->next->language == language_fortran))
    {
      subfile->language = subfile->next->language;
    }
}

/* For stabs readers, the first N_SO symbol is assumed to be the
   source file name, and the subfile struct is initialized using that
   assumption.  If another N_SO symbol is later seen, immediately
   following the first one, then the first one is assumed to be the
   directory name and the second one is really the source file name.

   So we have to patch up the subfile struct by moving the old name
   value to dirname and remembering the new name.  Some sanity
   checking is performed to ensure that the state of the subfile
   struct is reasonable and that the old name we are assuming to be a
   directory name actually is (by checking for a trailing '/'). */

void
patch_subfile_names (struct subfile *subfile, char *name)
{
  if (subfile != NULL && subfile->dirname == NULL && subfile->name != NULL
      && subfile->name[strlen (subfile->name) - 1] == '/')
    {
      subfile->dirname = subfile->name;
      subfile->name = savestring (name, (int) strlen (name));
      last_source_file = name;

      /* Default the source language to whatever can be deduced from
         the filename.  If nothing can be deduced (such as for a C/C++
         include file with a ".h" extension), then inherit whatever
         language the previous subfile had.  This kludgery is
         necessary because there is no standard way in some object
         formats to record the source language.  Also, when symtabs
         are allocated we try to deduce a language then as well, but
         it is too late for us to use that information while reading
         symbols, since symtabs aren't allocated until after all the
         symbols have been processed for a given source file. */

      subfile->language = deduce_language_from_filename (subfile->name);
      if (subfile->language == language_unknown &&
	  subfile->next != NULL)
	{
	  subfile->language = subfile->next->language;
	}
    }
}

/* Handle the N_BINCL and N_EINCL symbol types that act like N_SOL for
   switching source files (different subfiles, as we call them) within
   one object file, but using a stack rather than in an arbitrary
   order.  */

void
push_subfile (void)
{
  register struct subfile_stack *tem
  = (struct subfile_stack *) xmalloc (sizeof (struct subfile_stack));

  tem->next = subfile_stack;
  subfile_stack = tem;
  if (current_subfile == NULL || current_subfile->name == NULL)
    {
      abort ();
    }
  tem->name = current_subfile->name;
}

char *
pop_subfile (void)
{
  register char *name;
  register struct subfile_stack *link = subfile_stack;

  if (link == NULL)
    {
      abort ();
    }
  name = link->name;
  subfile_stack = link->next;
  free ((void *) link);
  return (name);
}


/* Add a funcexit linetable entry for the address real_call_pc to the
   funcexit line vector for SUBFILE.  */

void 
record_funcexit_line (register struct subfile *subfile, 
		      CORE_ADDR funcexit_line_pc)
{
    struct funcexit_linetable_entry *fele;

  /* Make sure funcexit line vector exists and is big enough.  */
  if (!subfile->funcexit_line_vector)
    {
      subfile->funcexit_line_vector_length = INITIAL_FUNCEXIT_LINE_VECTOR_LENGTH;
      subfile->funcexit_line_vector = (struct funcexit_linetable *)
	xmalloc (sizeof (struct funcexit_linetable)
	   + subfile->funcexit_line_vector_length 
            * sizeof (struct funcexit_linetable_entry));
      subfile->funcexit_line_vector->nitems = 0;
    }

  if (subfile->funcexit_line_vector->nitems + 1 
      >= subfile->funcexit_line_vector_length)
    {
      subfile->funcexit_line_vector_length *= 2;
      subfile->funcexit_line_vector = (struct funcexit_linetable *)
	xrealloc ((char *) subfile->funcexit_line_vector,
		  (sizeof (struct funcexit_linetable)
		   + (subfile->funcexit_line_vector_length
		      * sizeof (struct funcexit_linetable_entry))));
    }

  fele = subfile->funcexit_line_vector->item 
        + subfile->funcexit_line_vector->nitems++;

  /* Clear entry to zeros */
  memset(fele, 0, sizeof (struct funcexit_linetable_entry));
  fele->funcexit_line_pc = funcexit_line_pc;
}

/* Add a steplast linetable entry for the address real_call_pc to the
   steplast line vector for SUBFILE.  */

void 
record_steplast_line (register struct subfile *subfile, 
		      CORE_ADDR real_call_pc)
{
    struct steplast_linetable_entry *sle;

  /* Make sure steplast line vector exists and is big enough.  */
  if (!subfile->steplast_line_vector)
    {
      subfile->steplast_line_vector_length = INITIAL_STEPLAST_LINE_VECTOR_LENGTH;
      subfile->steplast_line_vector = (struct steplast_linetable *)
	xmalloc (sizeof (struct steplast_linetable)
	   + subfile->steplast_line_vector_length 
            * sizeof (struct steplast_linetable_entry));
      subfile->steplast_line_vector->nitems = 0;
      is_steplast_eligible = 1;
    }

  if (subfile->steplast_line_vector->nitems + 1 
      >= subfile->steplast_line_vector_length)
    {
      subfile->steplast_line_vector_length *= 2;
      subfile->steplast_line_vector = (struct steplast_linetable *)
	xrealloc ((char *) subfile->steplast_line_vector,
		  (sizeof (struct steplast_linetable)
		   + (subfile->steplast_line_vector_length
		      * sizeof (struct steplast_linetable_entry))));
    }

  sle = subfile->steplast_line_vector->item 
        + subfile->steplast_line_vector->nitems++;

  /* Clear entry to zeros */
  memset(sle, 0, sizeof (struct steplast_linetable_entry));
  sle->real_call_pc = real_call_pc;
}

/* Add a linetable entry for line number LINE and address PC to the
   line vector for SUBFILE.  
   Added to new args  for inline support -  1) context_ref --> context ref 
   associated with the line, 2) inline_idx --> index for inline routine 
   name judy array */

CORE_ADDR last_record_line_real_pc = 0;

void
record_line_elim (register struct subfile *subfile,
		  int line,
	 	  CORE_ADDR pc,
		  boolean is_address_set,
		  int column,
		  int is_stmt,	
                  int context,
		  CORE_ADDR context_ref,
		  int inline_idx)
{
  static boolean in_eliminated_proc = FALSE;

  struct linetable_entry *e;
  /* Ignore the dummy line number in libg.o */

  pc = SWIZZLE (pc);  /* Dwarf2 thinks pc is 32 bits */


  DEBUG(printf("record_line_elim A: subfile = %s, line = %d pc = %x\n", subfile->name, line, pc));
  DEBUG(printf("record_line_elim B: column = %d, is_stmt = %d context_ref = %d\n", column, is_stmt, context_ref));

  if ( (line == 0xffff) ) 
    {
      return;
    }
  if ( ( line & STEPLAST_MARKER_CONST ) || ( line & STEPLAST_MARKER_CONST_DOC2 ) )
    {
     /* Compiler sets 6th bit in line no. for steplast to differentiate
        from normal SLT_NORMALS */
     record_steplast_line (subfile, pc);
     return;
  }

  /* If a line address is set to zero, we are at the start of an eliminated
     procedure which continues until the next address is set, which may or
     may not be eliminated.
     */

  if (is_address_set)
    if (pc == 0)
      in_eliminated_proc = TRUE;
    else
      in_eliminated_proc = FALSE;


  /* Make sure line vector exists and is big enough.  */
  if (!subfile->line_vector)
    {
      subfile->line_vector_length = INITIAL_LINE_VECTOR_LENGTH;
      subfile->line_vector = (struct linetable *)
	xmalloc (sizeof (struct linetable)
	   + subfile->line_vector_length * sizeof (struct linetable_entry));
      subfile->line_vector->nitems = 0;
      have_line_numbers = 1;
    }

  if (subfile->line_vector->nitems + 1 >= subfile->line_vector_length)
    {
      subfile->line_vector_length *= 2;
      subfile->line_vector = (struct linetable *)
	xrealloc ((char *) subfile->line_vector,
		  (sizeof (struct linetable)
		   + (subfile->line_vector_length
		      * sizeof (struct linetable_entry))));
    }

  e = subfile->line_vector->item + subfile->line_vector->nitems++;
  e->line = line;
  e->pc = pc;
  e->column = column;
  e->is_stmt = is_stmt;
  e->inline_idx = 0;
  e->context_ref = context_ref;

#ifdef INLINE_SUPPORT
/* if context is not zero then we have an inline instance, 
   So we need to find the inline_idx associated with it.
   some times for eg: if you have an inline function which 
   has a call to another inline function as its first 
   instruction, then both these function have the 
   same context_ref(context_ref_pc actually), so you have to
   use "context" as the distinguishing factor between the inlines.
*/
#ifdef HP_IA64
  if (dwarf2_read_minimal && context)
    get_index_given_pc(pc, context, &inline_idx);
#endif
  e->context_ref = 0;
  e->inline_idx = inline_idx;
  e->context_ref = context_ref;
#endif

  if (in_eliminated_proc)
    {
      e->is_stmt = FALSE;
      if (INSTRUCTION_SIZE >= 4)
	e->pc = last_record_line_real_pc + 3;
      else
	e->pc = last_record_line_real_pc + 1;
    }
  else
    {
      last_record_line_real_pc = pc;
    }

} /* end record_line_elim */


void
record_doc_line (struct subfile *subfile,
		 unsigned int line,
		 unsigned int column_pos,
		 unsigned int column_pos_length,
		 CORE_ADDR pc,
		 int is_critical,
		 int has_column,
		 int is_funcexit,
		 int file_opt_level)
{
  struct doc_linetable_entry *e;

  if (line && is_funcexit)
    record_funcexit_line (subfile, pc);

  if (file_opt_level <= 1)
    return;

  if (( line & STEPLAST_MARKER_CONST_DOC2 ) || (line & STEPLAST_MARKER_CONST ))
   {
     /* Compiler sets 11th bit in "special" line no. for steplast to 
        differentiate from normal SLT_NORMALS for DOC2 */
     record_steplast_line (subfile, pc);
     return;
   }


  pc = SWIZZLE (pc);  /* Dwarf2 thinks pc is 32 bits */

  /* Make sure line vector exists and is big enough.  */
  if (!subfile->doc_line_vector)
    {
      subfile->doc_line_vector_length = INITIAL_LINE_VECTOR_LENGTH;
      subfile->doc_line_vector = (struct doc_linetable *)
        xmalloc (sizeof (struct doc_linetable) +
                 subfile->doc_line_vector_length *
                 sizeof (struct doc_linetable_entry));
      subfile->doc_line_vector->nitems = 0;
    }
  if (subfile->doc_line_vector->nitems + 1 >= subfile->doc_line_vector_length)
    {
      subfile->doc_line_vector_length *= 2;
      subfile->doc_line_vector = (struct doc_linetable *)
      xrealloc ((char *) subfile->doc_line_vector,
        (sizeof (struct doc_linetable) + subfile->doc_line_vector_length *
        sizeof (struct doc_linetable_entry)));
    }

  e = subfile->doc_line_vector->item + subfile->doc_line_vector->nitems++;

  /* Clear entry to zeros */
  memset(e, 0, sizeof (struct doc_linetable_entry));

  /* Initialize entry */
  /* On IA Actuals point to corresponding logicals */
  if ((((int) line) >=0) && (line < subfile->line_vector->nitems))
    {
      e->line = subfile->line_vector->item[line].line;
      e->column_pos = subfile->line_vector->item[line].column;
    }
  e->pc = pc;
  e->flags.flag.is_critical = 0;
  e->column_pos_length = 0;
  e->flags.flag.has_column = 1;
}

/* Add a linetable entry for line number LINE and address PC to the
   line vector for SUBFILE.  */

void
record_line (register struct subfile *subfile, int line, CORE_ADDR pc)
{
  record_line_elim (subfile, line, pc, FALSE, 1, 1, 0, 0, 0);
}


/* Needed in order to sort line tables from IBM xcoff files.  Sigh!  */

static int
compare_line_numbers (const void *ln1p, const void *ln2p)
{
  struct linetable_entry *ln1 = (struct linetable_entry *) ln1p;
  struct linetable_entry *ln2 = (struct linetable_entry *) ln2p;

  /* Note: this code does not assume that CORE_ADDRs can fit in ints.
     Please keep it that way.  */
  if (ln1->pc < ln2->pc)
    return -1;

  if (ln1->pc > ln2->pc)
    return 1;

#ifdef INLINE_SUPPORT
  /* if pc are same, line comning first in the linetable
  should come first. This keesp the relationsship of caller and
  callee intact when the pcs are same.  */

  if (ln1->inline_idx || ln2->inline_idx)
    return -1; 
  else
#endif
    {
      if (ln1->line < ln2->line)
	return -1;
      if (ln1->line > ln2->line)
	return 1;
      
      return ln1->column - ln2->column;
    }
}

int
compare_doc_line_pc_values (const void *n1, const void *n2)
{
  struct doc_linetable_entry *e1 = (struct doc_linetable_entry *) n1;
  struct doc_linetable_entry *e2 = (struct doc_linetable_entry *) n2;

  /* Sort by PC value, then by line number. PC values are unsigned types, so
     simply subtracting may not be entirely correct. */
  return ((e1->pc < e2->pc) ?
          -1 :
          ((e1->pc == e2->pc) ?
           ((e1->line < e2->line) ?
            -1 :
            ((e1->line == e2->line) ?
             0 :
             1)) :
           1));
}


/* Start a new symtab for a new source file.  Called, for example,
   when a stabs symbol of type N_SO is seen, or when a DWARF
   TAG_compile_unit DIE is seen.  It indicates the start of data for
   one original source file.  */

void
start_symtab (char *name, char *dirname, CORE_ADDR start_addr)
{

  last_source_file = name;
  last_source_start_addr = start_addr;
  file_symbols = NULL;
  global_symbols = NULL;
  within_function = 0;
  have_line_numbers = 0;

  /* Context stack is initially empty.  Allocate first one with room
     for 10 levels; reuse it forever afterward.  */
  if (context_stack == NULL)
    {
      context_stack_size = INITIAL_CONTEXT_STACK_SIZE;
      context_stack = (struct context_stack *)
	xmalloc (context_stack_size * sizeof (struct context_stack));
    }
  context_stack_depth = 0;

  /* Initialize the list of sub source files with one entry for this
     file (the top-level source file).  */

  subfiles = NULL;
  current_subfile = NULL;
  start_subfile (name, dirname);
}

// Sunil JAGaf49959
extern int display_full_path;

/* Finish the symbol definitions for one main source file, close off
   all the lexical contexts for that file (creating struct block's for
   them), then make the struct symtab for that file and put it in the
   list of all such.

   END_ADDR is the address of the end of the file's text.  SECTION is
   the section number (in objfile->section_offsets) of the blockvector
   and linetable.

   Note that it is possible for end_symtab() to return NULL.  In
   particular, for the DWARF case at least, it will return NULL when
   it finds a compilation unit that has exactly one DIE, a
   TAG_compile_unit DIE.  This can happen when we link in an object
   file that was compiled from an empty source file.  Returning NULL
   is probably not the correct thing to do, because then gdb will
   never know about this empty file (FIXME). */

struct symtab *
end_symtab (CORE_ADDR end_addr, struct objfile *objfile, int section)
{
  register struct symtab *symtab = NULL;
  register struct blockvector *blockvector;
  register struct subfile *subfile;
  register struct context_stack *cstk;
  struct subfile *nextsub;
  struct symbol *mod_sym;
  int mod_sym_idx;
  struct pending *next;

  /* Finish the lexical context of the last function in the file; pop
     the context stack.  */

  if (context_stack_depth > 0)
    {
      cstk = pop_context ();
      /* Make a block for the local symbols within.  */
      finish_block (cstk->name, &local_symbols, cstk->old_blocks,
		    cstk->start_addr, end_addr, objfile, cstk->is_inlined);

      if (context_stack_depth > 0)
	{
	  /* This is said to happen with SCO.  The old coffread.c
	     code simply emptied the context stack, so we do the
	     same.  FIXME: Find out why it is happening.  This is not
	     believed to happen in most cases (even for coffread.c);
	     it used to be an abort().  */
	  static struct complaint msg =
	  {"Context stack not empty in end_symtab", 0, 0};
	  complain (&msg);
	  context_stack_depth = 0;
	}
    }

  /* Reordered executables may have out of order pending blocks; if
     OBJF_REORDERED is true, then sort the pending blocks.  */
  if ((objfile->flags & OBJF_REORDERED) && pending_blocks)
    {
      /* FIXME!  Remove this horrid bubble sort and use merge sort!!! */
      int swapped;
      do
	{
	  struct pending_block *pb, *pbnext;

	  pb = pending_blocks;
	  pbnext = pb->next;
	  swapped = 0;

	  while (pbnext)
	    {
	      /* swap blocks if unordered! */

	      if (SWIZZLE(BLOCK_START (pb->block)) < SWIZZLE(BLOCK_START (pbnext->block)))
		{
		  struct block *tmp = pb->block;
		  pb->block = pbnext->block;
		  pbnext->block = tmp;
		  swapped = 1;
		}
	      pb = pbnext;
	      pbnext = pbnext->next;
	    }
	}
      while (swapped);
    }

  /* Cleanup any undefined types that have been left hanging around
     (this needs to be done before the finish_blocks so that
     file_symbols is still good).

     Both cleanup_undefined_types and finish_global_stabs are stabs
     specific, but harmless for other symbol readers, since on gdb
     startup or when finished reading stabs, the state is set so these
     are no-ops.  FIXME: Is this handled right in case of QUIT?  Can
     we make this cleaner?  */

  cleanup_undefined_types ();
  finish_global_stabs (objfile);

  if (pending_blocks == NULL
      && file_symbols == NULL
      && global_symbols == NULL
      && have_line_numbers == 0
#ifdef HP_IA64
      && pending_macros == NULL
#endif
     )
    {
      /* Ignore symtabs that have no functions with real debugging
         info.  */
      blockvector = NULL;
    }
  else
    {
      extern int num_globusings;
      extern struct symbol **global_using_dirs;
      if (num_globusings)
	{
	  struct symbol *sym;
	  int i =0;
	  for (i = 0; i < num_globusings; ++i)
	    {
	      sym = global_using_dirs[i];
	      finish_block (sym, &file_symbols, 0, last_source_start_addr, 
			    end_addr, objfile, 0); /* global block */
	    }
	  free (global_using_dirs);
	  global_using_dirs = NULL;
	}
      /* Define the STATIC_BLOCK & GLOBAL_BLOCK, and build the
         blockvector.  */
      finish_block (0, &file_symbols, 0, last_source_start_addr, end_addr,
		    objfile, 0); /* file blocks cannot be inlined */
      finish_block (0, &global_symbols, 0, last_source_start_addr, end_addr,
		    objfile, 0); /* file blocks cannot be inlined */
      blockvector = make_blockvector (objfile);
    }

#ifndef PROCESS_LINENUMBER_HOOK
#define PROCESS_LINENUMBER_HOOK()
#endif
  PROCESS_LINENUMBER_HOOK ();	/* Needed for xcoff. */

  /* Now create the symtab objects proper, one for each subfile.  */
  /* (The main file is the last one on the chain.)  */

  for (subfile = subfiles; subfile; subfile = nextsub)
    {
      int linetablesize = 0;
      int funcexit_linetablesize = 0;
      int steplast_linetablesize = 0;
      symtab = NULL;

      /* If we have blocks of symbols, make a symtab. Otherwise, just
         ignore this file and any line number info in it.  */
      if (blockvector)
	{
	  if (subfile->line_vector)
	    {
	      linetablesize = (int) (sizeof (struct linetable) +
	        subfile->line_vector->nitems * sizeof (struct linetable_entry));
#if 0
	      /* I think this is artifact from before it went on the
	         obstack. I doubt we'll need the memory between now
	         and when we free it later in this function.  */
	      /* First, shrink the linetable to make more memory.  */
	      subfile->line_vector = (struct linetable *)
		xrealloc ((char *) subfile->line_vector, linetablesize);
#endif

	      /* Like the pending blocks, the line table may be
	         scrambled in reordered executables.  Sort it if
	         OBJF_REORDERED is true.  */
	      if (objfile->flags & OBJF_REORDERED)
		msort (subfile->line_vector->item,
		       subfile->line_vector->nitems,
		       sizeof (struct linetable_entry),
		       compare_line_numbers);
	    }
	  if (subfile->funcexit_line_vector)
	    {
	      funcexit_linetablesize = (int) (sizeof (struct funcexit_linetable) +
	        subfile->funcexit_line_vector->nitems 
                 * sizeof (struct funcexit_linetable_entry));
            }
	  if (subfile->steplast_line_vector)
	    {
	      steplast_linetablesize = (int) (sizeof (struct steplast_linetable) +
	        subfile->steplast_line_vector->nitems 
                 * sizeof (struct steplast_linetable_entry));
            }
	  /* Now, allocate a symbol table.  */
	  symtab = allocate_symtab (subfile->name, objfile);

	  /* Fill in its components.  */
	  symtab->blockvector = blockvector;
#ifdef HP_IA64
	  symtab->macro_table = pending_macros;
#endif
	  if (subfile->line_vector)
	    {
	      /* Reallocate the line table on the symbol obstack */
	      symtab->linetable = (struct linetable *)(void *)
		obstack_alloc (&objfile->symbol_obstack, linetablesize);
	      memcpy (symtab->linetable, subfile->line_vector, linetablesize);
	    }
	  else
	    {
	      symtab->linetable = NULL;
	    }

          if (subfile->doc_line_vector)
            {
              size_t doclinetablesize;

              /* Sort it by PC value */
              qsort (subfile->doc_line_vector->item,
                     subfile->doc_line_vector->nitems,
                     sizeof(struct doc_linetable_entry),
                     compare_doc_line_pc_values);

              /* Reallocate the doc line table on the symbol obstack */
              doclinetablesize = sizeof (struct doc_linetable) +
                subfile->doc_line_vector->nitems *
                sizeof (struct doc_linetable_entry);
              symtab->doc_linetable = (struct doc_linetable *)(void *)
                obstack_alloc (&objfile -> symbol_obstack, doclinetablesize);
              memcpy (symtab->doc_linetable, subfile->doc_line_vector,
                      doclinetablesize);
            }
          else
            {
              symtab->doc_linetable = NULL;
            }
	  if (subfile->funcexit_line_vector)
	    {
	      /* Reallocate the funcexit line table on the symbol obstack */
	      symtab->funcexit_linetable = (struct funcexit_linetable *)(void *)
		obstack_alloc (&objfile->symbol_obstack, funcexit_linetablesize);
	      memcpy (symtab->funcexit_linetable, 
                      subfile->funcexit_line_vector, 
                      funcexit_linetablesize);
	    }
	  else
	    {
	      symtab->funcexit_linetable = NULL;
	    }
	  if (subfile->steplast_line_vector)
	    {
	      /* Reallocate the steplast line table on the symbol obstack */
	      symtab->steplast_linetable = (struct steplast_linetable *)(void *)
		obstack_alloc (&objfile->symbol_obstack, steplast_linetablesize);
	      memcpy (symtab->steplast_linetable, 
                      subfile->steplast_line_vector, 
                      steplast_linetablesize);
	    }
	  else
	    {
	      symtab->steplast_linetable = NULL;
	    }
	  symtab->block_line_section = section;
	  if (subfile->dirname)
	    {
	      /* Sunil JAGaf49959 07/03/06
	         Get the absolute pathname for the source file by searching the
		 directories mentioned in source_path.
	      */
	      char *full_pathname;
	      if (find_full_path_of (subfile->name, &full_pathname) && display_full_path)
		{
	          /* Reallocate the dirname on the symbol obstack */
	          symtab->dirname = (char *)
	     			    obstack_alloc (&objfile->symbol_obstack,
			        	           strlen (full_pathname) + 1);
	          strcpy (symtab->dirname, full_pathname);
		}
	      else
		{
	          /* Reallocate the dirname on the symbol obstack */
	          symtab->dirname = (char *)
				obstack_alloc (&objfile->symbol_obstack,
			        strlen (subfile->dirname) + 1);
	          strcpy (symtab->dirname, subfile->dirname);
	        }
	     }
	  else
	    {
	      symtab->dirname = NULL;
	    }
	  symtab->free_code = free_linetable;
	  symtab->free_ptr = NULL;

	  /* Use whatever language we have been using for this
	     subfile, not the one that was deduced in allocate_symtab
	     from the filename.  We already did our own deducing when
	     we created the subfile, and we may have altered our
	     opinion of what language it is from things we found in
	     the symbols. */
	  symtab->language = subfile->language;

	  /* Save the debug format string (if any) in the symtab */
	  if (subfile->debugformat != NULL)
	    {
	      symtab->debugformat = obsavestring (subfile->debugformat,
					      (int) strlen (subfile->debugformat),
						  &objfile->symbol_obstack);
	    }

	  /* All symtabs for the main file and the subfiles share a
	     blockvector, so we need to clear primary for everything
	     but the main file.  */

	  symtab->primary = 0;
	}
      if (subfile->name != NULL)
	{
	  free ((void *) subfile->name);
	}
      if (subfile->dirname != NULL)
	{
	  free ((void *) subfile->dirname);
	}
      if (subfile->line_vector != NULL)
	{
	  free ((void *) subfile->line_vector);
	}
      if (subfile->doc_line_vector != NULL)
        {
          free ((PTR) subfile->doc_line_vector);
        }
      if (subfile->funcexit_line_vector != NULL)
        {
          free ((PTR) subfile->funcexit_line_vector);
        }
      if (subfile->steplast_line_vector != NULL)
        {
          free ((PTR) subfile->steplast_line_vector);
        }
      if (subfile->debugformat != NULL)
	{
	  free ((void *) subfile->debugformat);
	}

      nextsub = subfile->next;
      if (subfiles == subfile)
        subfiles = nextsub;
      free ((void *) subfile);
    }

  /* Set this for the main source file.  */
  if (symtab)
    {
      symtab->primary = 1;
    }

  last_source_file = NULL;
  current_subfile = NULL;
#ifdef HP_IA64
  pending_macros = NULL;
#endif

  return symtab;
}

/* Push a context block.  Args are an identifying nesting level
   (checkable when you pop it), and the starting PC address of this
   context.  */

struct context_stack *
push_context (int desc, CORE_ADDR valu, int is_inlined)
{
  register struct context_stack *new;

  if (context_stack_depth == context_stack_size)
    {
      context_stack_size *= 2;
      context_stack = (struct context_stack *)
	xrealloc ((char *) context_stack,
		  (context_stack_size * sizeof (struct context_stack)));
    }

  new = &context_stack[context_stack_depth++];
  new->is_inlined = is_inlined;
  new->depth = desc;
  new->locals = local_symbols;
  new->params = param_symbols;
  new->old_blocks = pending_blocks;
  new->start_addr = valu;
  new->name = NULL;
  new->inline_relative_depth = 0;
  new->end_addr = 0;
  /* In hp-symtab-read.c, we have overloaded the ``is_inlined'' parameter
     to contain the inline index if this scope corresponds to an inline
     function. However the subscopes of the inline have to "inherit" the
     inline_index from the enclosing scope.
  */

  /* Now always set inline_idx on scopes. This is used to discard a
     scope from a block vector, if gdb is invoked without --inline.
  */
  /* JAGag41955: incorrect inheritance of inline indices thus leading
     to wrong scope definitions for symbols.
     Just check before inheriting whether the entity is really the
     enclosing scope of the context under consideration.
  */
#ifdef HP_IA64
  new->inline_index = is_inlined ? is_inlined : 0;
  if (!is_inlined && context_stack_depth > 1)
    if (context_stack[context_stack_depth - 2].end_addr == 0
        || (valu >= context_stack[context_stack_depth - 2].start_addr
             && valu <= context_stack[context_stack_depth - 2].end_addr)) 
      new->inline_index = context_stack[context_stack_depth - 2].inline_index;
#else
  new->inline_index = is_inlined ? is_inlined :
                      (context_stack_depth > 1 ? context_stack[context_stack_depth - 2].inline_index : 0);
#endif 
  local_symbols = NULL;
  param_symbols = NULL;

  return new;
}


/* Compute a small integer hash code for the given name. */

int
hashname (char *name)
{
    return (int) (hash(name,(int)strlen(name)) % HASHSIZE);
}


void
record_debugformat (char *format)
{
  if (current_subfile)
    {
      if (current_subfile->debugformat)
	free (current_subfile->debugformat);
      current_subfile->debugformat = savestring (format, (int) strlen (format));
    }
}

/* Merge the first symbol list SRCLIST into the second symbol list
   TARGETLIST by repeated calls to add_symbol_to_list().  This
   procedure "frees" each link of SRCLIST by adding it to the
   free_pendings list.  Caller must set SRCLIST to a null list after
   calling this function.

   Void return. */

void
merge_symbol_lists (struct pending **srclist, struct pending **targetlist)
{
  register int i;

  if (!srclist || !*srclist)
    return;

  /* Merge in elements from current link.  */
  for (i = 0; i < (*srclist)->nsyms; i++)
    add_symbol_to_list ((*srclist)->symbol[i], targetlist);

  /* Recurse on next.  */
  merge_symbol_lists (&(*srclist)->next, targetlist);

  /* "Free" the current link.  */
  (*srclist)->next = free_pendings;
  free_pendings = (*srclist);
}

/* Initialize anything that needs initializing when starting to read a
   fresh piece of a symbol file, e.g. reading in the stuff
   corresponding to a psymtab.  */


void
buildsym_init ()
{
  free_pendings = NULL;
  file_symbols = NULL;
  global_symbols = NULL;
  pending_blocks = NULL;
#ifdef HP_IA64
  pending_macros = NULL;
#endif
}

/* Initialize anything that needs initializing when a completely new
   symbol file is specified (not just adding some symbols from another
   file, e.g. a shared library).  */

void
buildsym_new_init ()
{
  buildsym_init ();
}
