/* Support for printing C values for GDB, the GNU debugger.
   Copyright 1986, 1988, 1989, 1991-1997, 2000
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "expression.h"
#include "value.h"
#include "demangle.h"
#include "valprint.h"
#include "language.h"
#include "c-lang.h"
#include "gdbcore.h"


extern int deref_char;

static int composite_enum (struct type *type, LONGEST val);

/* Print data of type TYPE located at VALADDR (within GDB), which came from
   the inferior at address ADDRESS, onto stdio stream STREAM according to
   FORMAT (a letter or 0 for natural format).  The data at VALADDR is in
   target byte order.

   If the data are a string pointer, returns the number of string characters
   printed.

   If DEREF_REF is nonzero, then dereference references, otherwise just print
   them like pointers.

   The PRETTY parameter controls prettyprinting.  */

int
c_val_print (struct type *type, char *valaddr, int embedded_offset,
     	     CORE_ADDR address, struct ui_file *stream,
     	     char format[], int deref_ref, int recurse,
             enum val_prettyprint pretty)
{
  register unsigned int i = 0;	/* Number of characters printed */
  unsigned len = 0;
  struct type *elttype;
  unsigned eltlen;
  LONGEST val;
  CORE_ADDR addr;
  extern void f77_get_dynamic_length_of_aggregate(struct type *);
  extern void print_function_address_demangle(CORE_ADDR, struct ui_file *, int);

  CHECK_TYPEDEF (type);
  switch (TYPE_CODE (type))
    {
    case TYPE_CODE_ARRAY:
      elttype = check_typedef (TYPE_TARGET_TYPE (type));
      if (TYPE_ARRAY_UPPER_BOUND_TYPE (type) &&
	  TYPE_ARRAY_UPPER_BOUND_TYPE (type) != BOUND_CANNOT_BE_DETERMINED)
	f77_get_dynamic_length_of_aggregate (type);
      if (TYPE_LENGTH (type) > 0 && TYPE_LENGTH (TYPE_TARGET_TYPE (type)) > 0)
	{
	  eltlen = TYPE_LENGTH (elttype);
	  len = TYPE_LENGTH (type) / eltlen;
	  if (prettyprint_arrays)
	    {
	      print_spaces_filtered (2 + 2 * recurse, stream);
	    }
	  /* For an array of chars, print with string syntax.  */
	  if (eltlen == 1 &&
	      ((TYPE_CODE (elttype) == TYPE_CODE_INT)
	       || ((current_language->la_language == language_m2)
		   && (TYPE_CODE (elttype) == TYPE_CODE_CHAR)))
	           && (format[0] == 0 || format[0] == 's'))
                        
	    {
	      /* If requested, look for the first null char and only print
	         elements up to it.  */
	      if (stop_print_at_null)
		{
		  unsigned int temp_len;

		  /* Look for a NULL char. */
		  for (temp_len = 0;
		       (valaddr + embedded_offset)[temp_len]
		       && temp_len < len && temp_len < print_max;
		       temp_len++);
		  len = temp_len;
		}

	      LA_PRINT_STRING (stream, valaddr + embedded_offset, len, eltlen, 0);
	      i = len;
	    }
	  else
	    {
	      fprintf_filtered (stream, "{");
	      /* If this is a virtual function table, print the 0th
	         entry specially, and the rest of the members normally.  */
	      if (cp_is_vtbl_ptr_type (elttype))
		{
		  i = 1;
		  fprintf_filtered (stream, "%d vtable entries", len - 1);
		}
	      else
		{
		  i = 0;
		}
	      val_print_array_elements (type, valaddr + embedded_offset, address, stream,
				     format, deref_ref, recurse, pretty, i);
	      fprintf_filtered (stream, "}");
	    }
	  break;
	}
      /* Array of unspecified length: treat like pointer to first elt.  */
      addr = address;
      goto print_unpacked_pointer;

    case TYPE_CODE_PTR:
      /* JAGaf75858 - handling wide chars */
      if (format[0] && format[0] != 's' && format[0] != 'W')
	{
	  print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
	  break;
	}
      if (vtblprint && cp_is_vtbl_ptr_type (type))
	{
	  /* Print the unmangled name if desired.  */
	  /* Print vtable entry - we only get here if we ARE using
	     -fvtable_thunks.  (Otherwise, look under TYPE_CODE_STRUCT.) */
	  CORE_ADDR addr
	    = extract_typed_address (valaddr + embedded_offset, type);
	  print_address_demangle (addr, stream, demangle);
	  break;
	}
      elttype = check_typedef (TYPE_TARGET_TYPE (type));
      if (TYPE_CODE (elttype) == TYPE_CODE_METHOD)
	{
	  cp_print_class_method (valaddr + embedded_offset, type, stream);
	}
      else if (TYPE_CODE (elttype) == TYPE_CODE_MEMBER)
	{
	  cp_print_class_member (valaddr + embedded_offset,
				 TYPE_DOMAIN_TYPE (TYPE_TARGET_TYPE (type)),
				 stream, "&");
	}
      else
	{
	addr = unpack_pointer (type, valaddr + embedded_offset);

        /* We can not unswizzle the addr here, as symbol addresses > 1 GB
           will not work properly in print_function_address_demangle for
           unswizzled addresses.
	   // addr = UNSWIZZLE (addr);
                                  // unpack_pointer does a swizzle we don't 
				  // want because we don't want to display 
				  // the swizzled value.
        */
	print_unpacked_pointer:
	  elttype = check_typedef (TYPE_TARGET_TYPE (type));

	  if (TYPE_CODE (elttype) == TYPE_CODE_FUNC)
	    {
#ifdef HP_IA64
              /*
                 QXCR1001060458: Enhance gdb to decode function pointers better.
                 On IA, the pointer to function points to a structure of two 64
                 bit words. First word being the entry address for the function
                 and second word the gp value.
              */
              struct obj_section *section;
              section = find_pc_section (SWIZZLE (addr));
              if (!section ||
                  ((section->the_bfd_section) &&
                   !strcmp(section->the_bfd_section->name, ".opd")))
                addr = (CORE_ADDR) read_memory_integer (SWIZZLE(addr),
                                                      sizeof(CORE_ADDR));
#endif
	      /* Try to print what function it points to.  */
	      print_function_address_demangle (addr, stream, demangle);
	      /* Return value is irrelevant except for string pointers.  */
	      return (0);
	    }

	  if (addressprint && format[0] != 's')
	    {
	      print_address_numeric (addr, 1, stream);
	    }

	  /* For a pointer to char or unsigned char, also print the string
	     pointed to, unless pointer is null.  */
	  /* FIXME: need to handle wchar_t here... */
          /* JAGaf75858 - handle wide chars via format = 'W' */
          /* JAGaf98189 - do not dereference char * if deref_char is off */
	  if (((TYPE_LENGTH (elttype) == 1
	      && TYPE_CODE (elttype) == TYPE_CODE_INT
	      && (format[0] == 0 || format[0] == 's')) 
              || (TYPE_LENGTH (elttype) == sizeof(unsigned int) 
              && TYPE_CODE (elttype) == TYPE_CODE_INT && format[0] == 'W'))
	      && addr != 0
              && deref_char)
	    {
	      i = val_print_string (addr, -1, TYPE_LENGTH (elttype), stream);
	    }
	  else if (cp_is_vtbl_member (type))
	    {
	      /* print vtbl's nicely */
	      CORE_ADDR vt_address = unpack_pointer (type, valaddr + embedded_offset);

	      struct minimal_symbol *msymbol =
	      lookup_minimal_symbol_by_pc (vt_address);
	      if ((msymbol != NULL) &&
		  (vt_address == SYMBOL_VALUE_ADDRESS (msymbol)))
		{
		  fputs_filtered (" <", stream);
		  fputs_filtered (SYMBOL_SOURCE_NAME (msymbol), stream);
		  fputs_filtered (">", stream);
		}
	      if (vt_address && vtblprint)
		{
		  value_ptr vt_val;
		  struct symbol *wsym = (struct symbol *) NULL;
		  struct type *wtype;
		  struct symtab *s;
		  struct block *block = (struct block *) NULL;
		  int is_this_fld;

		  if (msymbol != NULL)
		    wsym = lookup_symbol (SYMBOL_NAME (msymbol), block,
					  VAR_NAMESPACE, &is_this_fld, &s);

		  if (wsym)
		    {
		      wtype = SYMBOL_TYPE (wsym);
		    }
		  else
		    {
		      wtype = TYPE_TARGET_TYPE (type);
		    }
		  vt_val = value_at (wtype, vt_address, NULL);
		  val_print (VALUE_TYPE (vt_val), VALUE_CONTENTS (vt_val), 0,
			     VALUE_ADDRESS (vt_val), stream, format,
			     deref_ref, recurse + 1, pretty);
		  if (pretty)
		    {
		      fprintf_filtered (stream, "\n");
		      print_spaces_filtered (2 + 2 * recurse, stream);
		    }
		}
	    }

	  /* Return number of characters printed, including the terminating
	     '\0' if we reached the end.  val_print_string takes care including
	     the terminating '\0' if necessary.  */
	  return i;
	}
      break;

    case TYPE_CODE_MEMBER:
      error ("not implemented: member type in c_val_print");
      break;

    case TYPE_CODE_REF:
      elttype = check_typedef (TYPE_TARGET_TYPE (type));
      if (TYPE_CODE (elttype) == TYPE_CODE_ARRAY &&
	  TYPE_ARRAY_UPPER_BOUND_TYPE (elttype)&&
          TYPE_ARRAY_UPPER_BOUND_TYPE (type) != BOUND_CANNOT_BE_DETERMINED)
         f77_get_dynamic_length_of_aggregate (elttype);
      if (TYPE_CODE (elttype) == TYPE_CODE_MEMBER)
	{
	  cp_print_class_member (valaddr + embedded_offset,
				 TYPE_DOMAIN_TYPE (elttype),
				 stream, "");
	  break;
	}
      if (addressprint)
	{
	  CORE_ADDR addr
	    = extract_typed_address (valaddr + embedded_offset, type);
	  fprintf_filtered (stream, "@");
	  print_address_numeric (addr, 1, stream);
	  if (deref_ref)
	    fputs_filtered (": ", stream);
	}
      /* De-reference the reference.  */
      if (deref_ref)
	{
	  if (TYPE_CODE (elttype) != TYPE_CODE_UNDEF)
	    {
	      value_ptr deref_val =
	      value_at
	      (TYPE_TARGET_TYPE (type),
	       unpack_pointer (lookup_pointer_type (builtin_type_void),
			       valaddr + embedded_offset),
	       NULL);
	      val_print (VALUE_TYPE (deref_val),
			 VALUE_CONTENTS (deref_val),
			 0,
			 VALUE_ADDRESS (deref_val),
			 stream,
			 format,
			 deref_ref,
			 recurse,
			 pretty);
	    }
	  else
	    fputs_filtered ("???", stream);
	}
      break;

    case TYPE_CODE_UNION:
      if (recurse && !unionprint)
	{
	  fprintf_filtered (stream, "{...}");
	  break;
	}
      /* Fall through.  */
    case TYPE_CODE_STRUCT:
#ifndef TYPE_CODE_CLASS
    case TYPE_CODE_CLASS:
#endif
      if (vtblprint && cp_is_vtbl_ptr_type (type))
	{
	  /* Print the unmangled name if desired.  */
	  /* Print vtable entry - we only get here if NOT using
	     -fvtable_thunks.  (Otherwise, look under TYPE_CODE_PTR.) */
	  int offset = (int) (embedded_offset +
			TYPE_FIELD_BITPOS (type, VTBL_FNADDR_OFFSET) / 8);
	  struct type *field_type = TYPE_FIELD_TYPE (type, VTBL_FNADDR_OFFSET);
	  CORE_ADDR addr
	    = extract_typed_address (valaddr + offset, field_type);

	  print_address_demangle (addr, stream, demangle);
	}
      else
	cp_print_value_fields (type, type, valaddr, embedded_offset, address, stream, format,
			       recurse, pretty, NULL, 0);
      break;

    case TYPE_CODE_ENUM:
      if (format[0])
	{
	  print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
	  break;
	}
      len = TYPE_NFIELDS (type);
      val = unpack_long (type, valaddr + embedded_offset);
      for (i = 0; i < len; i++)
	{
	  QUIT;
	  if (val == TYPE_FIELD_BITPOS (type, i))
	    {
	      break;
	    }
	}
      if (i < len)
	{
	  fputs_filtered (TYPE_FIELD_NAME (type, i), stream);
	}
      else
	{
	  if (!composite_enum (type, val))
	    {
	      print_longest (stream, 'd', 0, val);
	      break;
	    }
	  /* Stacey
	     If we get here, then we are trying to print out a number that
	     greater than all the constants for this given enumeration
	     type, however it might be a combination of smaller enumeration
	     constants.  Print out symbolic names of all enumeration
	     constants present in the value from biggest to smallest.  Else,
	     if there are none, just print the decimal value.  This is
	     adapted from KWDB defect fix JAGad34047 
	     
	     Declare all variables and make a local copy of the number so
	     we can modify it.  We'll need to use the variables below to
	     store both the enum constant and value.  They might not equal
	     each other because enumeration constants can correspond to any
	     numerical value the user specifies.  The 1rst might be 2, the
	     second 4, etc ... The order might not even be ascending */

	  LONGEST local_val = val;
	  int first = 1;
	  int biggest_enum;
	  char *member_name = 0; /* initialize for compiler warning */
	  LONGEST member_val;
	  int biggest_val;

	  do
	    {
	      biggest_val = 0;
	      biggest_enum = -1;

	      /* Iterate over all the enumeration constants.  Each
		 time we find one that is a component of our number,
		 store useful data about it.  Replace the data each time
		 we find a bigger enumeration constant that is a
		 component of our number.  

		 An enumeration constant is a component of our number
		 if the following holds:

		 constant & number = number.  
		 
		 For example, 5 (101 in binary) contains 1 and 4 (100)
		 because:
		 1 & 101 = 1
		 100 & 101 = 100 */
	      

	      for (i = 0; i < len; i++)
		{
		  QUIT;
		  member_val = TYPE_FIELD_BITPOS (type, i);
		  if ((local_val & member_val) == member_val
		      && member_val > biggest_val)
		    {
		      biggest_enum = i;
		      biggest_val = (int) member_val;
		      member_name = TYPE_FIELD_NAME (type, i);
		    }
		}

	      /* Print the name of the largest enumeration constant that
		 we've found.  Then remove this value from our number
		 via a bit-wise & operation and continue.  Eventually,
		 we'll print the second largest number ... and so on ...
		 until there aren't any more numbers to be printed.  */

	      if (biggest_enum >= 0)
		{
		  if (!first)
		    fputs_filtered ("|", stream);
		  first = 0;
		  fputs_filtered (member_name, stream);
		  local_val &= ~biggest_val;
		}
	    }
	  while ((biggest_enum != 0) && (biggest_enum != -1));

	  /* val does not correspond to any value in the enum:
	     just print out the decimal value */
	  if (biggest_enum == -1 && first)
	    print_longest (stream, 'd', 0, val);
	}
      break;

    case TYPE_CODE_FUNC:
      if (format[0])
	{
	  print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
	  break;
	}
      /* FIXME, we should consider, at least for ANSI C language, eliminating
         the distinction made between FUNCs and POINTERs to FUNCs.  */
      fprintf_filtered (stream, "{");
      type_print (type, "", stream, -1);
      fprintf_filtered (stream, "} ");
      /* Try to print what function it points to, and its address.  */
      print_function_address_demangle (address, stream, demangle);
      break;

    case TYPE_CODE_BOOL:
      if (output_format)
	format[0] = format[0] ? format[0] : output_format;
      if (format[0])
	print_scalar_formatted (valaddr + embedded_offset, 
                                  type, format, 0, stream);
      else
	{
	  val = unpack_long (type, valaddr + embedded_offset);
	  if (val == 0)
	    fputs_filtered ("false", stream);
	  else if (val == 1)
	    fputs_filtered ("true", stream);
	  else
	    print_longest (stream, 'd', 0, val);
	}
      break;

    case TYPE_CODE_RANGE:
      /* FIXME: create_range_type does not set the unsigned bit in a
         range type (I think it probably should copy it from the target
         type), so we won't print values which are too large to
         fit in a signed integer correctly.  */
      /* FIXME: Doesn't handle ranges of enums correctly.  (Can't just
         print with the target type, though, because the size of our type
         and the target type might differ).  */
      /* FALLTHROUGH */

    case TYPE_CODE_INT:
      if (output_format)
        format[0] = format[0] ? format[0] : output_format;
      /* JAGaf75858 - handle wide chars */
      if (format[0] && format[0] != 'W')
	{
	  print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
	}
      else
	{
	  val_print_type_code_int (type, valaddr + embedded_offset, stream);
	  /* C and C++ has no single byte int type, char is used instead.
	     Since we don't know whether the value is really intended to
	     be used as an integer or a character, print the character
	     equivalent as well. */
	  if (TYPE_LENGTH (type) == 1)
	    {
	      fputs_filtered (" ", stream);
	      LA_PRINT_CHAR ((unsigned char) unpack_long (type, valaddr + embedded_offset),
			     stream);
	    }
           /* JAGaf75858 - handle wide chars */
	  if (TYPE_LENGTH (type) == sizeof(unsigned int) &&  format[0] == 'W')
	    {
	      fputs_filtered (" ", stream);
	      fprintf_filtered(stream,"'%lc'",
              unpack_long (type, valaddr + embedded_offset));
	    }
	}
      break;

    case TYPE_CODE_CHAR:
      if (output_format)
        format[0] = format[0] ? format[0] : output_format;
      if (format[0])
	print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
      else
	{
	  val = unpack_long (type, valaddr + embedded_offset);
	  if (TYPE_UNSIGNED (type))
	    fprintf_filtered (stream, "%u", (unsigned int) val);
	  else
	    fprintf_filtered (stream, "%d", (int) val);
	  fputs_filtered (" ", stream);
	  LA_PRINT_CHAR ((unsigned char) val, stream);
	}
      break;

    case TYPE_CODE_FLT:
      /* JAGad01835 - radix.exp: print floating value incorrectly 
         when output radix is 8 or 16 */
      if (output_format)
        format[0] = format[0] ? format[0] : output_format;
      if (format[0])
	print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
      else
	{
	  print_floating (valaddr + embedded_offset, type, stream, 0);
	}
      break;

#ifdef HP_IA64
    case TYPE_CODE_DECFLOAT:
      if (output_format)
        format[0] = format[0] ? format[0] : output_format;
      if (format[0])
        print_scalar_formatted (valaddr + embedded_offset, type, format, 0, stream);
      else
        print_decimal_floating (valaddr + embedded_offset, type, stream, 0);
      break;
#endif

    case TYPE_CODE_METHOD:
      cp_print_class_method (valaddr + embedded_offset, lookup_pointer_type (type), stream);
      break;

    case TYPE_CODE_VOID:
      fprintf_filtered (stream, "void");
      break;

    case TYPE_CODE_ERROR:
      fprintf_filtered (stream, "<error type>");
      break;

    case TYPE_CODE_UNDEF:
      /* This happens (without TYPE_FLAG_STUB set) on systems which don't use
         dbx xrefs (NO_DBX_XREFS in gcc) if a file has a "struct foo *bar"
         and no complete type for struct foo in that file.  */
      fprintf_filtered (stream, "<incomplete type>");
      break;

    case TYPE_CODE_COMPLEX:
      print_floating (valaddr + embedded_offset, 
		      TYPE_TARGET_TYPE (type), 
		      stream, 0);
      fprintf_filtered (stream, " + ");
      print_floating (  valaddr + embedded_offset 
		      + TYPE_LENGTH(TYPE_TARGET_TYPE (type)), 
		      TYPE_TARGET_TYPE (type), 
		      stream, 0);
      fprintf_filtered (stream, " * " IMAGINARY_I);
      break;
    
#ifdef TARGET_FLOAT80_BIT
    case TYPE_CODE_FLOAT80:
      {
	__float80	float80_value;
	float80_value = 0; /* initialize for compiler warning */
	memcpy (&float80_value, valaddr, sizeof(__float80));
	print_formatted (long_double_val_from_float80 (float80_value), 
			 format, 
			 0, stream);/* JAGag16624 */
      }
      break;

   case TYPE_CODE_FLOATHPINTEL:
     {
       /* Convert it into long double. 
	  __fpreg has the same format as a fp register except it is 16 byte
	  long rather than 12 bytes. Just use any Floating
	  point register to pass to REGISTER_CONVERT_TO_VIRTUAL. */
       value_ptr long_double_val = allocate_value (builtin_type_long_double);
       REGISTER_CONVERT_TO_VIRTUAL (FR0_REGNUM, builtin_type_long_double,
	 valaddr+4, VALUE_CONTENTS (long_double_val));
       print_formatted (long_double_val,
                         format,
                         0, stream);/* JAGag16624 */
     }
     break;

    case TYPE_CODE_FLOAT80_COMPLEX:
      {
	/* Create a __float80 _Complex gdb value, then cast it to a long
	   double complex and print that.
	   */
	value_ptr temp_val;

	temp_val = allocate_value (builtin_type_float80_complex);
	memcpy (VALUE_CONTENTS_RAW (temp_val), 
				    valaddr, 
				    sizeof (__float80 _Complex));
	temp_val = value_cast (builtin_type_long_double_complex, temp_val);
	print_formatted (temp_val, format, 0, stream);/* JAGag16624 */
      }
      break;
#endif

    default:
      error ("Invalid C/C++ type code %d in symbol table.", TYPE_CODE (type));
    }
  gdb_flush (stream);
  return (0);
}

int
c_value_print (value_ptr val, struct ui_file *stream, char format[],
     	       enum val_prettyprint pretty)
{
  struct type *type = VALUE_TYPE (val);
  struct type *real_type;
  int full, top, using_enc;

  /* If it is a pointer, indicate what it points to.

     Print type also if it is a reference.

     C++: if it is a member pointer, we will take care
     of that when we print it.  */
  if (TYPE_CODE (type) == TYPE_CODE_PTR ||
      TYPE_CODE (type) == TYPE_CODE_REF)
    {
      /* Hack:  remove (char *) for char strings.  Their
         type is indicated by the quoted string anyway. */
      if (TYPE_CODE (type) == TYPE_CODE_PTR &&
	  TYPE_NAME (type) == NULL &&
	  TYPE_NAME (TYPE_TARGET_TYPE (type)) != NULL &&
	  STREQ (TYPE_NAME (TYPE_TARGET_TYPE (type)), "char"))
	{
	  /* Print nothing */
	}
      else if (objectprint 
	       && (   (TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_CLASS)
		   || (   TYPE_CODE (TYPE_TARGET_TYPE (type)) 
		       == TYPE_CODE_STRUCT)))
	{

	  if (TYPE_CODE(type) == TYPE_CODE_REF)
	    {
	      /* Copy value, change to pointer, so we don't get an
	       * error about a non-pointer type in value_rtti_target_type
	       */
	      value_ptr temparg;
	      temparg=value_copy(val);
	      VALUE_TYPE (temparg) = lookup_pointer_type(TYPE_TARGET_TYPE(type));
	      val=temparg;
	    }
	  /* Pointer to class, check real type of object */
	  fprintf_filtered (stream, "(");
          
          /* pes,JAGae12279 - If the address is null , don't do 
	   * value_rtti_target_type .This is to fix the problem ,where 
	   * "info locals" fails to show all the variables when the 
	   * first base class pointer is a NULL value. The diff in 
  	   * value_as_pointer(val) and VALUE_POINTED_TO_OFFSET(val) will
           * give the pointer(say "ptr") to base/derived class.With this 
	   * "ptr" , GDB tries to dereference "ptr",in order to find out 
	   * the vtable and then typeinfo for determining the runtime 
	   * type of class (i.e., to find out whether it is a base class 
	   * or derived class pointer). We need not do this, if the 
	   * "ptr" is NULL,in which case we can't find the vtable .
           */
          if ((value_as_pointer (val) - VALUE_POINTED_TO_OFFSET (val)) != 0)
	    {
              real_type = value_rtti_target_type (val, &full, &top, &using_enc);
              if (real_type)
    	        {
	          /* RTTI entry found */
                  if (TYPE_CODE (type) == TYPE_CODE_PTR)
                    {
                      /* create a pointer type pointing to the real type */
                      type = lookup_pointer_type (real_type);
                    }
                  else
                    {
                      /* create a reference type referencing the real type */
                      type = lookup_reference_type (real_type);
                    }
	          /* JYG: Need to adjust pointer value. */
		  /* Poorva - top is already a negative value for the new C++
		     abi */
		  if (new_cxx_abi)
		    {
#ifdef BFD_IS_SWIZZLED
		      if (!is_swizzled) /* LP64 */
			val->aligner.contents[0] += top;
		      else  /* ILP 32 */
			(*(unsigned int *)(void *) val->aligner.contents)
			  += top;
#else
		      (*(unsigned int *) (void *) val->aligner.contents)
			+= top;
#endif
		    }
		  else
                    val->aligner.contents[0] -= top;

                  /* Note: When we look up RTTI entries, we don't get any 
                     information on const or volatile attributes */
              }
           }
          type_print (type, "", stream, -1);
	  fprintf_filtered (stream, ") ");
	}
      else
	{
	  /* normal case */
	  fprintf_filtered (stream, "(");
	  type_print (type, "", stream, -1);
	  fprintf_filtered (stream, ") ");
	}
    }
  if (objectprint && (   (TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_CLASS)
		      || (TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_STRUCT)))
    {
      /* Attempt to determine real type of object */
      real_type = value_rtti_type (val, &full, &top, &using_enc);
      if (real_type)
	{
	  /* We have RTTI information, so use it */
	  val = value_full_object (val, real_type, full, top, using_enc);
	  fprintf_filtered (stream, "(%s%s) ",
			    TYPE_NAME (real_type),
			    full ? "" : " [incomplete object]");
	  /* Print out object: enclosing type is same as real_type if full */
	  return val_print (VALUE_ENCLOSING_TYPE (val), VALUE_CONTENTS_ALL (val), 0,
			 VALUE_ADDRESS (val), stream, format, 1, 0, pretty);
          /* Note: When we look up RTTI entries, we don't get any information on
             const or volatile attributes */
	}
    }

  return val_print (type, VALUE_CONTENTS_ALL (val), VALUE_EMBEDDED_OFFSET (val),
		    VALUE_ADDRESS (val),
		    stream, format, 1, 0, pretty);
}

/* Description: This function returns a boolean; true if the enum type and the
 * 		value arguments qualify for composite enum printing.  To 
 *		qualify for this the enum should be a set of flags each of 
 *		which is a power of two and the enum should have all the 
 *		elements required to represent val (i.e., each bit in val 
 *		should have one element in the enum to represent it).
 */
static int
composite_enum (struct type *type, LONGEST val)
{
  int i, j;
  int len; 
  int bit_flag; 
  const int num_bits = sizeof (LONGEST) * CHAR_BIT;
  LONGEST member_val;
  LONGEST composite_val = 0;

  /* 1. Check if all elements of the enum are binary flags, each being 
   *    equal to some power of 2.
   */
  len = TYPE_NFIELDS (type);
  for (i = 0; i < len; i++)
    {
      bit_flag = 0;
      member_val = TYPE_FIELD_BITPOS (type, i);

      for (j = 0; j < num_bits; j++)
	if (member_val == (1ull << j))
	  {
	    bit_flag = 1;
	    break;
	  }

      if (bit_flag)
	composite_val |= member_val;
      else
        return 0;
    }

  /* 2. Check if the enum has all the elements necessary to print val
   *    as a composite, i.e., are there enough enum elements to represent
   *	all bits in val.
   */
  if (~composite_val & val)
    return 0;

  return 1;
}
