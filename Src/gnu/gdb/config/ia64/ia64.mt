# Target: IA64 running on HP-UX 11.20
# hp-ia64-linkmap.o contains stubs for linkmap.a; remove this when one 
# becomes available
#TDEPFILES= ia64-tdep.o hp-ia64-solib.o hp-ia64-linkmap.o
# jini: JAGag21714: Added somsolib.o and somread.o for 32 bit mixed
# mode support
# Added macrocmd.o. macroexp.o, macroscope.o & macrotab.o for
# Macro support.
# jini: mixed mode corefile debugging support: Added mixed-mode-tdep.o
# QXCR1000809804 - Instead of using "+e symbol", "+ee,symbol" is used to export 
# the symbol and does not resolve other symbols internally.
TDEPFILES= ia64-tdep.o hp-ia64-solib.o ia64_asm.o gdbrtc.o fastsym.o fastsym-helper.o somsolib.o somread.o macrocmd.o macroexp.o macroscope.o macrotab.o mixed-mode-tdep.o dfp.o
TM_FILE= tm-ia6411.h
MT_LIBRTC_EX= 

RTC_TDEPFILES= hp-ia64-rtc.o
RTC_TDEP64FILES= hp-ia64-rtc64.o
RTC_TDEP_SLFILES= hp-ia64-rtc_sl.o
RTC_TDEP_SL64FILES= hp-ia64-rtc_sl64.o
RTC_TARGETS= librtc.so.1 librtc64.so.1
RTC_LCL= -L../../../../../IPF_BE/usr/lib/hpux32 -lcl +b /usr/lib/hpux32
RTC_LCL64= -L../../../../../IPF_BE/usr/lib/hpux64 -lcl +b /usr/lib/hpux64

EMBEDDED_PATH= -Wl,+b/usr/lib/hpux32

LOADLIBES=-Wl,+ee,mixed_mode_read_memory -lunwind -l:libdld.so -lCsup -l:libuca.so -ldas -L$(srcdir)/../../IPF_BE/usr/lib/$(HP_LIB_SUBDIR) -lJudy -L$(srcdir)/../../IPF_BE/usr/lib/hpux64 -l:libm.so
MT_CFLAGS = -D__HP_CURSES ${POSIX_REVISION_LEVEL}
EXPORT_SYMBOLS = 
INLINE_FLAG=+d
