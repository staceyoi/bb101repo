/* Parameters for execution on any Hewlett-Packard IA-64 machine.
   Copyright 1986, 1987, 1989, 1990, 1991, 1992, 1993, 1995
   Free Software Foundation, Inc. 

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#ifdef __STDC__
struct type;
struct symbol;
#endif

#define GDB_TARGET_IS_HPUX

#include "hp-ia64-solib.h"
#include "hpux-inline.h"
#include <signal.h>

#ifdef _SIGRTMIN
#define REALTIME_LO _SIGRTMIN
#define REALTIME_HI (_SIGRTMAX + 1)
#else
#define REALTIME_LO 37
#define REALTIME_HI 44
#endif

#define GET_LONGJMP_TARGET(ADDR)  get_longjmp_target(ADDR)
extern int get_longjmp_target (CORE_ADDR *);

#define PURIFY_SIGNATURE_SYMBOL               "_p_command_options_arg"

/* Java Stack unwind. */
extern char* get_java_func_name_ipf (struct frame_info *);
#define DO_JAVA_STACK_UNWIND(next_frame, prev)\
	do_java_stack_unwind((next_frame), (prev))
#define GET_JAVA_FUNC_NAME(frame) \
	get_java_func_name_ipf((frame))

extern CORE_ADDR call_dummy_begin;
extern CORE_ADDR call_dummy_end;

/* TARGET_BYTE_ORDER_SELECTABLE means we can change the byte order dynamically*/
#define TARGET_BYTE_ORDER_SELECTABLE 1

/* Target system byte order. */
#define	TARGET_BYTE_ORDER	BIG_ENDIAN

/* srikanth, 071900, ttrace_wait should not be called after
   ttrace TT_PROC_ATTACH.
*/
#define ATTACH_NO_WAIT 1

#define PATHMAP 1 
#define CORE_TRUNCATION_DETECTION 1 
#define USING_MMAP 1 
#define HASH_ALL

/* By default assume we don't have to worry about software floating point.  */
#ifndef SOFT_FLOAT
#define SOFT_FLOAT 0
#endif

/* Get at various relevent fields of an instruction word. */

#define MASK_5 0x1f
#define MASK_11 0x7ff
#define MASK_14 0x3fff
#define MASK_21 0x1fffff

/* This macro gets bit fields using HP's numbering (MSB = 0) */

#define GET_FIELD(X, FROM, TO) \
  ((X) >> 31 - (TO) & (1 << ((TO) - (FROM) + 1)) - 1)

/* Watch out for NaNs */

#define IEEE_FLOAT (1)

#define REG_STRUCT_HAS_ADDR(gcc_p,type) 0

/* Offset from address of function to start of its code.
   Zero on most machines.  */

#define FUNCTION_START_OFFSET 0

/* Advance PC across any function entry prologue instructions
   to reach some "real" code.  */
#define SKIP_PROLOGUE(pc) (skip_prologue (pc))
extern CORE_ADDR skip_prologue (CORE_ADDR);

/* If PC is in some function-call trampoline code, return the PC
   where the function itself actually starts.  If not, return NULL.  */

#define	SKIP_TRAMPOLINE_CODE(pc) skip_trampoline_code (pc, NULL)
extern CORE_ADDR skip_trampoline_code (CORE_ADDR, char *);

/* If PC is in function/thunk/stub for which the target address can't
 * be found until reaching the branch (i.e., using SKIP_TRAMPOLINE_CODE)
 * find the location of the first branch and return it.  For now this
 * is needed only for covariant return thunks on IA. - Bharath, 30 June 2004.
 */
#define	DYNAMIC_TRAMPOLINE_NEXTPC(pc) dynamic_trampoline_nextpc (pc)
extern CORE_ADDR dynamic_trampoline_nextpc (CORE_ADDR pc);

/* Return non-zero if we are in an appropriate trampoline. */

#define IN_SOLIB_CALL_TRAMPOLINE(pc, name) \
   in_solib_call_trampoline (pc, name)
extern int in_solib_call_trampoline (CORE_ADDR, char *);

#define IN_SOLIB_RETURN_TRAMPOLINE(pc, name) \
  in_solib_return_trampoline (pc, name)
extern int in_solib_return_trampoline (CORE_ADDR, char *);

#define IN_SIGTRAMP(pc, name) \
  pc_in_user_sendsig(pc)
extern int pc_in_user_sendsig(CORE_ADDR pc);

#define COVARIANT_SUPPORT

/* Fixme: Uncomment it after fixing FC3 problem - Diwakar */
#define INLINE_SUPPORT

/* If pc is in a covariant return thunk (either outbound or in and outbound
 * return 1
 */
#define IS_PC_IN_COVARIANT_THUNK(pc) is_pc_in_covariant_thunk (pc)
extern int is_pc_in_covariant_thunk (CORE_ADDR pc);

/* Immediately after a function call, return the saved pc.
   Can't go through the frames for this because on some machines
   the new frame is not set up until the new function executes
   some instructions.  */

struct frame_info;
CORE_ADDR saved_pc_after_call PARAMS ((struct frame_info *));
#undef	SAVED_PC_AFTER_CALL
#define SAVED_PC_AFTER_CALL(frame) saved_pc_after_call (frame)

/* Stack grows downward */

#define INNER_THAN(lhs,rhs) ((lhs) < (rhs))

#define STACK_ALIGN(arg) ( ((arg)%16) ? (((arg + 15)&-16)) : (arg))
#define NO_EXTRA_ALIGNMENT_NEEDED

/* Sequence of bytes for breakpoint instruction.  */

/* ia64-gambit: 
 * BREAKPOINT is not used for implementing breakpoints.
 * Low-level breakpoint implemented using the Gambit function
 * gb_brkp(). 
 */
#define BREAKPOINT {0x00, 0x00, 0x00, 0x00}

/* Amount PC must be decremented by after a breakpoint.
   This is often the number of bytes in BREAKPOINT
   but not always.

   Not on the PA-RISC */

#define DECR_PC_AFTER_BREAK 0

/* return instruction is bv r0(rp) or bv,n r0(rp) */

#define ABOUT_TO_RETURN(pc) ((read_memory_integer (pc, 4) | 0x2) == 0xE840C002)

/* Does the RSE bsp exist */
#define REGISTER_STACK_ENGINE_FP

/* Say how long (ordinary) registers are.  This is a piece of bogosity
   used in push_word and a few other places; REGISTER_RAW_SIZE is the
   real way to know how big a register is.  */

/* Tahoe -> FIXME */
#define REGISTER_SIZE 8


/* Number of machine registers */
#define NUM_REGS 588

/* Number of static GRs */
#define NUM_ST_GRS 32

/* Number of BRs */
#define NUM_BRS 8

/* Tahoe */
/* These are needed to determine register type */

/* Floating Point registers (FRs) */
#define FR0_REGNUM       0
#define FRLAST_REGNUM  127

/* NaT registers (NATs) */
#define NR0_REGNUM     128
#define NRLAST_REGNUM  255

/* Predicate registers */
#define PR_REGNUM      588
#define PR0_REGNUM     256
#define PRLAST_REGNUM  319

/* General registers (GRs) */
#define GR0_REGNUM     320
#define GRLAST_REGNUM  447
/* fake - to go at some point */
#define GRFAKE         400

/* Branch registers (BRs) */
#define BR0_REGNUM     448
#define BRLAST_REGNUM  455

/* Application registers (ARs) */
#define AR0_REGNUM     456
#define ARLAST_REGNUM  583

/* PC */
#define PC_REGNUM      584
/* This crap is a rollover from PA - has to go at some point */
#define NPC_REGNUM 584

/* Current Frame Marker (CFM) */
#define CFM_REGNUM     585

#define PSR_REGNUM  586
#define REASON_REGNUM  587

/* Initializer for an array of names of registers.
   There should be NUM_REGS strings in this initializer.  */
#define REGISTER_NAMES  \
{  "fr0",   "fr1",   "fr2",   "fr3",   "fr4",   "fr5",   "fr6",   "fr7", \
   "fr8",   "fr9",  "fr10",  "fr11",  "fr12",  "fr13",  "fr14",  "fr15", \
  "fr16",  "fr17",  "fr18",  "fr19",  "fr20",  "fr21",  "fr22",  "fr23", \
  "fr24",  "fr25",  "fr26",  "fr27",  "fr28",  "fr29",  "fr30",  "fr31", \
  "fr32",  "fr33",  "fr34",  "fr35",  "fr36",  "fr37",  "fr38",  "fr39", \
  "fr40",  "fr41",  "fr42",  "fr43",  "fr44",  "fr45",  "fr46",  "fr47", \
  "fr48",  "fr49",  "fr50",  "fr51",  "fr52",  "fr53",  "fr54",  "fr55", \
  "fr56",  "fr57",  "fr58",  "fr59",  "fr60",  "fr61",  "fr62",  "fr63", \
  "fr64",  "fr65",  "fr66",  "fr67",  "fr68",  "fr69",  "fr70",  "fr71", \
  "fr72",  "fr73",  "fr74",  "fr75",  "fr76",  "fr77",  "fr78",  "fr79", \
  "fr80",  "fr81",  "fr82",  "fr83",  "fr84",  "fr85",  "fr86",  "fr87", \
  "fr88",  "fr89",  "fr90",  "fr91",  "fr92",  "fr93",  "fr94",  "fr95", \
  "fr96",  "fr97",  "fr98",  "fr99", "fr100", "fr101", "fr102", "fr103", \
 "fr104", "fr105", "fr106", "fr107", "fr108", "fr109", "fr110", "fr111", \
 "fr112", "fr113", "fr114", "fr115", "fr116", "fr117", "fr118", "fr119", \
 "fr120", "fr121", "fr122", "fr123", "fr124", "fr125", "fr126", "fr127", \
   "natr0",  "natr1",  "natr2",  "natr3",  "natr4",  "natr5",  "natr6",  \
     "natr7", \
   "natr8",  "natr9", "natr10", "natr11", "natr12", "natr13", "natr14",  \
    "natr15", \
  "natr16", "natr17", "natr18", "natr19", "natr20", "natr21", "natr22",  \
    "natr23", \
  "natr24", "natr25", "natr26", "natr27", "natr28", "natr29", "natr30",  \
    "natr31", \
  "natr32", "natr33", "natr34", "natr35", "natr36", "natr37", "natr38",  \
    "natr39", \
  "natr40", "natr41", "natr42", "natr43", "natr44", "natr45", "natr46",  \
    "natr47", \
  "natr48", "natr49", "natr50", "natr51", "natr52", "natr53", "natr54",  \
    "natr55", \
  "natr56", "natr57", "natr58", "natr59", "natr60", "natr61", "natr62",  \
    "natr63", \
  "natr64", "natr65", "natr66", "natr67", "natr68", "natr69", "natr70",  \
    "natr71", \
  "natr72", "natr73", "natr74", "natr75", "natr76", "natr77", "natr78",  \
    "natr79", \
  "natr80", "natr81", "natr82", "natr83", "natr84", "natr85", "natr86",  \
    "natr87", \
  "natr88", "natr89", "natr90", "natr91", "natr92", "natr93", "natr94",  \
    "natr95", \
  "natr96", "natr97", "natr98", "natr99","natr100","natr101","natr102",  \
   "natr103", \
 "natr104","natr105","natr106","natr107","natr108","natr109","natr110",  \
   "natr111", \
 "natr112","natr113","natr114","natr115","natr116","natr117","natr118",  \
   "natr119", \
 "natr120","natr121","natr122","natr123","natr124","natr125","natr126",  \
   "natr127", \
   "pr0",   "pr1",   "pr2",   "pr3",   "pr4",   "pr5",   "pr6",   "pr7", \
   "pr8",   "pr9",  "pr10",  "pr11",  "pr12",  "pr13",  "pr14",  "pr15", \
  "pr16",  "pr17",  "pr18",  "pr19",  "pr20",  "pr21",  "pr22",  "pr23", \
  "pr24",  "pr25",  "pr26",  "pr27",  "pr28",  "pr29",  "pr30",  "pr31", \
  "pr32",  "pr33",  "pr34",  "pr35",  "pr36",  "pr37",  "pr38",  "pr39", \
  "pr40",  "pr41",  "pr42",  "pr43",  "pr44",  "pr45",  "pr46",  "pr47", \
  "pr48",  "pr49",  "pr50",  "pr51",  "pr52",  "pr53",  "pr54",  "pr55", \
  "pr56",  "pr57",  "pr58",  "pr59",  "pr60",  "pr61",  "pr62",  "pr63", \
   "gr0",   "gr1",   "gr2",   "gr3",   "gr4",   "gr5",   "gr6",   "gr7", \
   "gr8",   "gr9",  "gr10",  "gr11",  "gr12",  "gr13",  "gr14",  "gr15", \
  "gr16",  "gr17",  "gr18",  "gr19",  "gr20",  "gr21",  "gr22",  "gr23", \
  "gr24",  "gr25",  "gr26",  "gr27",  "gr28",  "gr29",  "gr30",  "gr31", \
  "gr32",  "gr33",  "gr34",  "gr35",  "gr36",  "gr37",  "gr38",  "gr39", \
  "gr40",  "gr41",  "gr42",  "gr43",  "gr44",  "gr45",  "gr46",  "gr47", \
  "gr48",  "gr49",  "gr50",  "gr51",  "gr52",  "gr53",  "gr54",  "gr55", \
  "gr56",  "gr57",  "gr58",  "gr59",  "gr60",  "gr61",  "gr62",  "gr63", \
  "gr64",  "gr65",  "gr66",  "gr67",  "gr68",  "gr69",  "gr70",  "gr71", \
  "gr72",  "gr73",  "gr74",  "gr75",  "gr76",  "gr77",  "gr78",  "gr79", \
  "gr80",  "gr81",  "gr82",  "gr83",  "gr84",  "gr85",  "gr86",  "gr87", \
  "gr88",  "gr89",  "gr90",  "gr91",  "gr92",  "gr93",  "gr94",  "gr95", \
  "gr96",  "gr97",  "gr98",  "gr99", "gr100", "gr101", "gr102", "gr103", \
 "gr104", "gr105", "gr106", "gr107", "gr108", "gr109", "gr110", "gr111", \
 "gr112", "gr113", "gr114", "gr115", "gr116", "gr117", "gr118", "gr119", \
 "gr120", "gr121", "gr122", "gr123", "gr124", "gr125", "gr126", "gr127", \
   "br0",   "br1",   "br2",   "br3",   "br4",   "br5",   "br6",   "br7", \
   "kr0",   "kr1",   "kr2",   "kr3",   "kr4",   "kr5",   "kr6",   "kr7", \
   "",   "",  "",  "",  "",  "",  "",  "", \
  "rsc",  "bsp",  "bspstore",  "rnat",  "",  "fcr",  "",  "", \
  "eflag",  "csd",  "ssd",  "cflg",  "fsr",  "fir",  "fdr",  "", \
  "ccv",  "",  "",  "",  "unat",  "",  "",  "", \
  "fpsr",  "",  "",  "",  "itc",  "",  "",  "", \
  "",  "",  "",  "",  "",  "",  "",  "", \
  "",  "",  "",  "",  "",  "",  "",  "", \
  "pfs",  "lc",  "ec",  "",  "",  "",  "",  "", \
  "",  "",  "",  "",  "",  "",  "",  "", \
  "",  "",  "",  "",  "",  "",  "",  "", \
  "",  "",  "",  "",  "",  "",  "",  "", \
  "",  "",  "",  "", "", "", "", "", \
 "", "", "", "", "", "", "", "", \
 "", "", "", "", "", "", "", "", \
 "", "", "", "", "", "", "", "", \
 "ip", "cfm", "psr", "reason" }


/* Register numbers of various important registers.
   Note that some of these values are "real" register numbers,
   and correspond to the general registers of the machine,
   and some are "phony" register numbers which are too large
   to be actual register numbers as far as the user is concerned
   but do serve to get the desired values when passed to read_register.  */

#define R0_REGNUM GR0_REGNUM	/* Doesn't actually exist, used as base for
				   other r registers.  */
#define FLAGS_REGNUM GR0_REGNUM	/* Various status flags */
#define FP_REGNUM (GR0_REGNUM+4)	/* Contains address of stack frame for
					   call dummies */
#define RP_REGNUM BR0_REGNUM	/* return pointer */
#define DP_REGNUM (GR0_REGNUM+1)
#define SP_REGNUM (GR0_REGNUM+12)	/* Contains address of top of stack */
#define TP_REGNUM (GR0_REGNUM+13)	/* Thread local data base register */


/* ??? RM: Is all this really bogus? */
#define SAR_REGNUM (GRFAKE+3)	/* Shift Amount Register */
#define IPSW_REGNUM (GRFAKE+4)	/* Interrupt Processor Status Word */
#define PCOQ_HEAD_REGNUM (GRFAKE+5)	/* instruction offset queue head */
#define PCSQ_HEAD_REGNUM (GRFAKE+6)	/* instruction space queue head */
#define PCOQ_TAIL_REGNUM (GRFAKE+7)	/* instruction offset queue tail */
#define PCSQ_TAIL_REGNUM (GRFAKE+8)	/* instruction space queue tail */
#define EIEM_REGNUM (GRFAKE+9)	/* External Interrupt Enable Mask */
#define IIR_REGNUM (GRFAKE+10)	/* Interrupt Instruction Register */
#define IOR_REGNUM (GRFAKE+11)	/* Interrupt Offset Register */
#define SR4_REGNUM (GRFAKE+12)	/* space register 4 */
#define RCR_REGNUM (GRFAKE+13)	/* Recover Counter (also known as cr0) */
#define CCR_REGNUM (GRFAKE+14)	/* Coprocessor Configuration Register */
#define TR0_REGNUM (GRFAKE+15)	/* Temporary Registers (cr24 -> cr31) */
#define FP0_REGNUM FR0_REGNUM	/* floating point reg. 0 */
#define FP4_REGNUM (FR0_REGNUM+4)

#define ARG0_REGNUM (GR0_REGNUM+32)	/* The first argument of a callee. */
#define ARG1_REGNUM (GR0_REGNUM+33)	/* The second argument of a callee. */
#define ARG2_REGNUM (GR0_REGNUM+34)	/* The third argument of a callee. */
#define ARG3_REGNUM (GR0_REGNUM+35)	/* The fourth argument of a callee. */

/*
 * Processor Status Word Masks
 */

#define PSW_T   0x01000000	/* Taken Branch Trap Enable */
#define PSW_H   0x00800000	/* Higher-Privilege Transfer Trap Enable */
#define PSW_L   0x00400000	/* Lower-Privilege Transfer Trap Enable */
#define PSW_N   0x00200000	/* PC Queue Front Instruction Nullified */
#define PSW_X   0x00100000	/* Data Memory Break Disable */
#define PSW_B   0x00080000	/* Taken Branch in Previous Cycle */
#define PSW_C   0x00040000	/* Code Address Translation Enable */
#define PSW_V   0x00020000	/* Divide Step Correction */
#define PSW_M   0x00010000	/* High-Priority Machine Check Disable */
#define PSW_CB  0x0000ff00	/* Carry/Borrow Bits */
#define PSW_R   0x00000010	/* Recovery Counter Enable */
#define PSW_Q   0x00000008	/* Interruption State Collection Enable */
#define PSW_P   0x00000004	/* Protection ID Validation Enable */
#define PSW_D   0x00000002	/* Data Address Translation Enable */
#define PSW_I   0x00000001	/* External, Power Failure, Low-Priority */
				/* Machine Check Interruption Enable */

/* When fetching register values from an inferior or a core file,
   clean them up using this macro.  BUF is a char pointer to
   the raw value of the register in the registers[] array.  */
/* Tahoe : empty macro */
#define CLEAN_UP_REGISTER_VALUE(regno, buf) ;

/* Define DO_REGISTERS_INFO() to do machine-specific formatting
   of register dumps. */

#define DO_REGISTERS_INFO(_regnum, fp) ia64_do_registers_info (_regnum, fp)
extern void ia64_do_registers_info (int, int);

/* Define STRCAT_REGISTER() to do machine-specific concatenation
   of a register string into a buffer. */

#define STRCAT_REGISTER(buf, bufLen, regnum, raw_buf, precision) \
            ia64_strcat_register(buf, bufLen, regnum, raw_buf, precision)

/* Define PRINT_REGISTER() to do machine-specific printing
   of a register. */

#define PRINT_REGISTER(regnum, raw_buf, precision) \
            ia64_print_register(regnum, raw_buf, precision)

/* Define IS_64BIT() to answer whether the machine is 64 bit or not
   of a register - IA64 always is. */

#define IS_64BIT        1

/* Number of bytes of storage in the actual machine representation
   for register N. */
/* ia64-gambit:
   Floating point regs : 12 bytes, NATs : 1 byte, everybody else : 8 bytes 
   Note: eventhough 11 bytes are sufficient to hold a Floating point
   register, Gambit demands Floating point register size to be 12 bytes.
 */
#define REGISTER_RAW_SIZE(N) \
    ((N) <= FRLAST_REGNUM ? 12 \
      : (N) <= PRLAST_REGNUM ? 1 \
      : 8)


/* Total amount of space needed to store our copies of the machine's
   register state, the array `registers'.  */
#define REGISTER_BYTES ((FRLAST_REGNUM+1)*12 + (NUM_REGS - FRLAST_REGNUM)*8)

/* Index within `registers' of the first byte of the space for
   register N.  */
#define REGISTER_BYTE(N) \
    ((N) <= FRLAST_REGNUM ? (N) * 12    \
     : (FRLAST_REGNUM+1) * 12 + ((N)-FRLAST_REGNUM-1) * 8)

/* Number of bytes of storage in the program's representation
   for register N. */

#define REGISTER_VIRTUAL_SIZE(N) REGISTER_RAW_SIZE(N)

/* Largest value REGISTER_RAW_SIZE can have.  */
#define MAX_REGISTER_RAW_SIZE 12

#define FLOAT_REG_RAW_SIZE 12

/* Largest value REGISTER_VIRTUAL_SIZE can have.  */
#define MAX_REGISTER_VIRTUAL_SIZE 12

/* Return the GDB type object for the "standard" data type
   of data in register N.  */
/* Sunil 151105 JAGaf73984 - FRLAST_REGNUM has value 127. 
   So while computing the register virtual type, check 
   has to be made for N <= FRLAST_REGNUM.  */ 
#define REGISTER_VIRTUAL_TYPE(N) \
 ((N) <= FRLAST_REGNUM ? builtin_type_floathpintel \
   : (N) <= PRLAST_REGNUM ? builtin_type_unsigned_char \
   : builtin_type_long_long)

/* Nonzero if register N requires conversion
   from raw format to virtual format.  */

#undef  REGISTER_CONVERTIBLE
#define REGISTER_CONVERTIBLE(N) (((unsigned)((N) - FR0_REGNUM)) < 128)

/* Convert data from raw format for register REGNUM in buffer FROM
   to virtual format with type TYPE in buffer TO.  */

#undef  REGISTER_CONVERT_TO_VIRTUAL
#define REGISTER_CONVERT_TO_VIRTUAL(REGNUM,TYPE,FROM,TO) \
  ia64_convert_to_virtual ((TYPE), (FROM), (TO));
extern void ia64_convert_to_virtual (struct type *, char *, char *);

/* Convert data from virtual format with type TYPE in buffer FROM
   to raw format for register REGNUM in buffer TO.  */

extern void
doublest_to_ia64 (char *from, char *to);

#undef  REGISTER_CONVERT_TO_RAW
#ifdef TARGET_FLOAT80_BIT
#define REGISTER_CONVERT_TO_RAW(TYPE,REGNUM,FROM,TO) \
{ \
  if ((TYPE)->code == TYPE_CODE_FLOAT80) \
    {  \
      __float80	from_val; \
      DOUBLEST	doublest_val; \
       \
      from_val = 0; memcpy (&from_val, (FROM), sizeof(__float80)); \
      doublest_val = from_val; \
      doublest_to_ia64((char *)&doublest_val, (TO)); \
    } \
  else if ((TYPE)->code == TYPE_CODE_FLOATHPINTEL) \
    { \
      memcpy ((TO), ((char*)(FROM)) + 4, TYPE_LENGTH ((TYPE))); \
    } \
  else \
    { \
      DOUBLEST val = extract_floating ((FROM), TYPE_LENGTH (TYPE)); \
      doublest_to_ia64((char *)&val, (TO)); \
    } \
}
#else /* TARGET_FLOAT80_BIT */
#define REGISTER_CONVERT_TO_RAW(TYPE,REGNUM,FROM,TO) \
{ \
  DOUBLEST val = extract_floating ((FROM), TYPE_LENGTH (TYPE)); \
  doublest_to_ia64((char *)&val, (TO)); \
} \

#endif /* else TARGET_FLOAT80_BIT */

/* Store the address of the place in which to copy the structure the
   subroutine will return.  This is called from call_function. */

#define STORE_STRUCT_RETURN(ADDR, SP) {write_register (GR0_REGNUM+8, (ADDR)); }

#undef EXTRACT_RETURN_VALUE
/* RM: floats are returned in FR8, always as 96 bit values. This case
 *     is handled specially in values.c - not here.
 *     integral values are in r8, padded on the left
 *     32-bit addresses are in r8, padded on the left
 *     aggregates less than 65 bits are in r8, right padded
 *     aggregates up to 256 bits (32 bytes) are in r8 - r11, right padded
 * MC: For LRE, for integer stuff,
 * the value is not preceeded by pad bytes, it is followed by pad bytes.
 */

#define EXTRACT_RETURN_VALUE(TYPE,REGBUF,VALBUF) 			\
  { 									\
    char *__from_ptr = (char *)(REGBUF) + REGISTER_BYTE (GR0_REGNUM+8);	\
									\
    if ((!IS_TARGET_LRE) && 						\
	 (is_integral_type(TYPE) || SOFT_FLOAT ||			\
	 (is_swizzled && is_pointer_type(TYPE))))			\
      /* bump the __from_ptr by size of pad at the left of register */	\
      __from_ptr += (REGISTER_SIZE - TYPE_LENGTH (TYPE));		\
									\
    else if (TYPE_LENGTH (TYPE) > 32)   				\
      /* this macro only handles return values that fit in registers */	\
      internal_error ("EXTRACT_RETURN_VALUE : fell through all cases.");\
									\
    memcpy ((VALBUF), __from_ptr, TYPE_LENGTH (TYPE)); 			\
  }

/* RM: structs up to 256 bits are returned in registers */
#undef USE_STRUCT_CONVENTION
#define USE_STRUCT_CONVENTION(gcc_p, value_type)\
  (TYPE_LENGTH (value_type) > 32)

/* coulter 8/17/01 Returning a float is handled in STORE_RETURN_GDB_VAL
   all other cases are handled by STORE_RETURN_VALUE
   */

#undef STORE_RETURN_VALUE
#define STORE_RETURN_VALUE(TYPE,VALBUF) \
  { \
    if (is_integral_type(TYPE) || SOFT_FLOAT)   \
       write_register_bytes \
              (REGISTER_BYTE (GR0_REGNUM + 8) + \
                 (REGISTER_SIZE - TYPE_LENGTH (TYPE)), \
               (VALBUF), \
               TYPE_LENGTH (TYPE)); \
    else if (TYPE_LENGTH (TYPE) <= 32)   \
       write_register_bytes \
             ( REGISTER_BYTE (28), \
               (VALBUF), \
               TYPE_LENGTH (TYPE)); \
  }

#define STORE_RETURN_GDB_VAL(VAL, TYPE, VALBUF) \
  { \
    if (TYPE_CODE (type) == TYPE_CODE_FLT && !SOFT_FLOAT) \
      value_assign (value_of_register (8), VAL); \
    else \
      STORE_RETURN_VALUE (TYPE, VALBUF); \
  }


/* Return a value_ptr pointing to a structure that was returned on the
   stack at address addr.  On HP-IA64 we cannot call
   EXTRACT_STRUCT_VALUE_ADDRESS because by the time the return is made
   the address is no longer in the registers.  We must be given the
   address.
   */

#define VALUE_RETURNED_FROM_STACK(valtype,addr) \
  ia64_value_returned_from_stack (valtype, addr)

/*
 * This macro defines the register numbers (from REGISTER_NAMES) that
 * are effectively unavailable to the user through ptrace().  It allows
 * us to include the whole register set in REGISTER_NAMES (inorder to
 * better support remote debugging).  If it is used in
 * fetch/store_inferior_registers() gdb will not complain about I/O errors
 * on fetching these registers.  If all registers in REGISTER_NAMES
 * are available, then return false (0).
 * It should really be defined in nm-ia64h.h, where linux gets its definition.
 */
/* Tahoe */
#undef CANNOT_STORE_REGISTER
/* ia64: Took PC_REGNUM out of the list of un-storable register */
/* Bindu: Steve V says 16, 18, 19, 32, 36, 40, 64-66 r the only 
   ARs that can be written.
   Cannot write to CFM*/
/*Bindu: cannot write/read __ar_rnat from IC5*/
extern int is_11_23_or_greater;

/* PSR is partially writable.  It is treated as writable in
 * CANNOT_STORE_REGISTER and there is special case code which deals with
 * setting the user mask (__um)
 */

#define CANNOT_STORE_REGISTER(regno) (((regno) == GR0_REGNUM) \
	|| ((regno) == REASON_REGNUM) \
        || ((regno) == AR0_REGNUM + 17) || ((regno) == FR0_REGNUM) \
	|| ((regno) == (FR0_REGNUM + 1)) || ((regno) == NR0_REGNUM) \
	|| (((regno) >= AR0_REGNUM) && ((regno) <= AR0_REGNUM+15)) \
	|| ((regno) == AR0_REGNUM + 18) \
	|| (((regno) >= AR0_REGNUM + 20) && ((regno) <= AR0_REGNUM + 24)) \
	|| (((regno) >= AR0_REGNUM + 27) && ((regno) <= AR0_REGNUM + 31)) \
	|| (((regno) >= AR0_REGNUM + 33) && ((regno) <= AR0_REGNUM + 35)) \
	|| (((regno) >= AR0_REGNUM + 37) && ((regno) <= AR0_REGNUM + 39)) \
	|| ((regno) == AR0_REGNUM + 40 && IS_TARGET_LRE)                  \
	|| (((regno) >= AR0_REGNUM + 41) && ((regno) <= AR0_REGNUM + 63)) \
	|| (((regno) >= AR0_REGNUM + 67) && ((regno) <= AR0_REGNUM + 127)) \
	|| (   !is_11_23_or_greater \
	    && (((regno) == AR0_REGNUM + 25) || ((regno) == AR0_REGNUM + 26))) \
	|| (   cur_thread_in_syscall () \
	    && (   (((regno) == GR0_REGNUM + 2)) \
		|| (((regno) == GR0_REGNUM + 3)) \
		|| (   ((regno) >= GR0_REGNUM + 14) \
		    && ((regno) <= GR0_REGNUM + 31)) \
		|| (   ((regno) >= FR0_REGNUM + 6) \
		    && ((regno) <= FR0_REGNUM + 15)) \
		|| (   ((regno) >= FR0_REGNUM + 32) \
		    && ((regno) <= FR0_REGNUM + 127)) \
		|| (((regno) == BR0_REGNUM + 6)) \
		|| (((regno) == BR0_REGNUM + 7)) \
		|| (((regno) == AR0_REGNUM + 25)) \
		|| (((regno) == AR0_REGNUM + 26)) \
		|| (((regno) == AR0_REGNUM + 32)) \
		|| (((regno) == AR0_REGNUM + 64)))))

/* Tahoe */

/* Save FRs (128), static GRs (32), BRs (8), 
   PFS, BSPSTORE, RNAT, UNAT, FPSR, LC (6), 
   PRs (64 bits), static NRs (32 bits) in the frame. */
#define EXTRA_FRAME_INFO  \
  int n_rse_regs; \
  CORE_ADDR rse_fp; \
  boolean regs_filled; \
  char registers[(FRLAST_REGNUM + 1) * 12 + \
                 (NUM_ST_GRS + NUM_BRS + 6 + 1) * 8 + 32]; \
  CORE_ADDR cfm; \
  int inline_idx;\
  int frame_type;\
  struct exec_path_tab* local_ept; \
  void *java_ptr;

#define NBR_FRAME_REGS  	(FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 1)

#define LAST_FRAME_REGNO	(NBR_FRAME_REGS - 1)

extern int frame_regno_to_regno (int frame_regno);

#define INIT_EXTRA_FRAME_INFO(fromleaf, frame) init_extra_frame_info (fromleaf, frame)

void destroy_extra_frame_info (struct frame_info *);
#define DESTROY_EXTRA_FRAME_INFO(frame) destroy_extra_frame_info(frame);

void ia64_print_extra_frame_info (struct frame_info *);
#define PRINT_EXTRA_FRAME_INFO(frame) ia64_print_extra_frame_info(frame);

/* Describe the pointer in each stack frame to the previous stack frame
   (its caller).  */

/* FRAME_CHAIN takes a frame's nominal address
   and produces the frame's chain-pointer.

   FRAME_CHAIN_COMBINE takes the chain pointer and the frame's nominal address
   and produces the nominal address of the caller frame.

   However, if FRAME_CHAIN_VALID returns zero,
   it means the given frame is the outermost one and has no caller.
   In that case, FRAME_CHAIN_COMBINE is not used.  */

/* In the case of the PA-RISC, the frame's nominal address
   is the address of a 4-byte word containing the calling frame's
   address (previous FP).  */

CORE_ADDR frame_chain PARAMS ((struct frame_info *));
#define FRAME_CHAIN(thisframe) frame_chain (thisframe)

#define FRAME_CHAIN_VALID(chain, thisframe) \
  frame_chain_valid (chain, thisframe)

#define FRAME_CHAIN_COMBINE(chain, thisframe) (chain)

/* Define other aspects of the stack frame.  */

/* A macro that tells us whether the function invocation represented
   by FI does not have a frame on the stack associated with it.  If it
   does not, FRAMELESS is set to 1, else 0.  */
#define FRAMELESS_FUNCTION_INVOCATION(FI) \
  (frameless_function_invocation(FI))
extern int frameless_function_invocation (struct frame_info *);

CORE_ADDR frame_saved_pc PARAMS ((struct frame_info *));
#define FRAME_SAVED_PC(FRAME) frame_saved_pc (FRAME)

#define GET_SAVED_REGISTER(raw_buffer, optimized, addrp, frame, regnum, lval) ia64_get_saved_register(raw_buffer, optimized, addrp, frame, regnum, lval)
extern void ia64_get_saved_register (char *, int *, CORE_ADDR *, 
				struct frame_info *, int, enum lval_type *);

/* Args address and locals address on IPF is the rse stack address where
   gr32 (may have been) is saved. */
#define FRAME_ARGS_ADDRESS(fi) ((fi)->rse_fp)

#define FRAME_LOCALS_ADDRESS(fi) ((fi)->rse_fp)

/* Set VAL to the number of args passed to frame described by FI.
   Can set VAL to -1, meaning no way to tell.  */

/* We can't tell how many args there are
   now that the C compiler delays popping them.  */
#define FRAME_NUM_ARGS(fi) (-1)

/* Return number of bytes at start of arglist that are not really args.  */

#define FRAME_ARGS_SKIP 0

#define FRAME_FIND_SAVED_REGS(frame_info, frame_saved_regs) \
  ia64_frame_find_saved_regs (frame_info, &frame_saved_regs)


/* Things needed for making the inferior call functions.  */

/* Push an empty stack frame, to record the current PC, etc. */

#define PUSH_DUMMY_FRAME push_dummy_frame (inf_status)

/* Discard from the stack the innermost frame, 
   restoring all saved registers.  */
#define POP_FRAME  ia64_pop_frame ()
extern void ia64_pop_frame (void);

/* use 16 so that it makes sense to change an address by this amount */
#define INSTRUCTION_SIZE 	16

#define BUNDLE_SIZE 		16
#define INSTR_PER_BUNDLE	3
#define BUNDLE_NBR_INTS		4

/* call_dummy:
   ;; save non-preserved branch registers in preserved general registers
   nop.m 
   mov r5=b0
   mov r6=b6 

   nop.m 
   nop.f
   mov r7=b7;;

   ;; call call_dummy_proc to alloc a new frame
   nop.m
   nop.m
   br.call.pp.few       b0=.+64 ;;

   ;; restore non-preserved branch registers
   nop.m
   mov b0=r5
   mov b6=r6

   nop.m
   nop.f
   mov b7=r7;;

   ;; nop bundle -- call dummy breakpoint will be set here
   nop.m
   nop.i
   nop.i ;;

   call_dummy_proc:
   ;; alloc a new frame and move args into place 
   alloc r32=ar.pfs,2,0,8,0
   mov b6 = r11
   or r34 = r21,r0

   or r35 = r22,r0
   or r36 = r23,r0
   or r37 = r24,r0

   or r38 = r25,r0
   or r39 = r26,r0
   or r40 = r27,r0

   or r41 = r28,r0 
   nop.m
   mov r33=b0 ;;     

   ;; actual call
   nop.m
   nop.m
   br.call.dptk.few rp=b6 ;;

   nop.m
   mov.i ar.pfs=r32
   mov b0=r33 ;;

   nop.m
   nop.f
   br.ret.pp.few b0;;    
 */
#define CALL_DUMMY {0x0000000001005000LL, \
                    0x006200c06000c400LL, \
                                \
                    0x0d00000001000000LL, \
                    0x000200e07000c400LL, \
                                \
                    0x1900000001000000LL, \
                    0x0002000040000052LL, \
                                \
                    0x0000000001000028LL, \
                    0x048003c060080007LL, \
                                \
                    0x0d00000001000000LL, \
                    0x000200e070080007LL, \
                                \
                    0x0100000001000000LL, \
                    0x0002000000000400LL, \
                                \
                    0x0000290480056058LL, \
                    0x0480034054013880LL, \
                                \
                    0x081859000e2040baLL, \
                    0x001c40a084013880LL, \
                                \
                    0x083065000e2070d2LL, \
                    0x001c4000b5013880LL, \
                                \
                    0x094871000e200000LL, \
                    0x000200200400c400LL, \
                                \
                    0x1900000001000000LL, \
                    0x0002000060008012LL, \
                                \
                    0x0100000001000000LL, \
                    0x01550000100a0007LL, \
                                \
                    0x1d00000001000000LL, \
                    0x0002008000008402LL  \
}

/* RM: switch to #if 1 when call dummy cn exist on the stack. See
   comment in #else part */
/* switched to HP_IA64_NATIVE : bindu */
#ifdef HP_IA64_NATIVE
/*#if 0 */
/* RM: ??? need a better check? */

#if INNER_THAN(1, 2)
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) \
                             ((call_dummy_begin != 0) && \
			      (swizzle (pc) <  call_dummy_begin) && \
                              (swizzle (pc) >= call_dummy_end))
#else
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) \
                             ((call_dummy_begin != 0) && \
                              (swizzle (pc) >=  call_dummy_begin) && \
                              (swizzle (pc) < call_dummy_end))
#endif

#define CALL_DUMMY_LOCATION ON_STACK
#else
/* RM: GamBit seems to cache bundles around the current pc. This means
   that we can't overwrite existing bundles with new ones. This
   effectively means that the call dummy has to be at a fixed position
   (else successive command line calls with call dummies at slightly
   different offsets fail) */
#define CALL_DUMMY_LOCATION AFTER_TEXT_END
#define NEED_TEXT_START_END 1

/* RM: for the same reason, we can't write out the call dummy right
   after text end -- those bundles may already be cached. Write at a
   safe distance */
#define AFTER_TEXT_END_OFFSET 4800

extern CORE_ADDR text_end;
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) \
  ((pc) >= text_end + AFTER_TEXT_END_OFFSET  \
   && (pc) <= text_end + AFTER_TEXT_END_OFFSET + CALL_DUMMY_LENGTH + DECR_PC_AFTER_BREAK)
#endif

#define CALL_DUMMY_LENGTH (INSTRUCTION_SIZE * 13)

#define CALL_DUMMY_START_OFFSET 0

#define CALL_DUMMY_BREAKPOINT_OFFSET 0x50

/*
 * Insert the specified number of args and function address
 * into a call sequence of the above form stored at DUMMYNAME.
 */

struct value;

#define FIX_CALL_DUMMY ia64_fix_call_dummy

CORE_ADDR ia64_fix_call_dummy (char *, CORE_ADDR, CORE_ADDR, int, struct value **, struct type *, int);  

CORE_ADDR ia64_push_arguments (int, struct value **, CORE_ADDR, int, CORE_ADDR);

#define PUSH_ARGUMENTS(nargs, args, sp, struct_return, struct_addr) \
    sp = ia64_push_arguments(nargs, args, sp, struct_return, struct_addr)

/* Tahoe - empty macro */
#define SMASH_TEXT_ADDRESS(addr) ;

#define BELIEVE_PCC_PROMOTION 1

/* ia64-gambit work     : copied from ../pa/tm-hppa.h */
/* Info about the unwind table associated with an object file.

 * This is hung off of the "objfile->obj_private" pointer, and
 * is allocated in the objfile's psymbol obstack.  This allows
 * us to have unique unwind info for each executable and shared
 * library that we are debugging.
 */
struct unwind_table_entry
  {
    CORE_ADDR region_start;
    CORE_ADDR region_end;
    CORE_ADDR prologue_end;
    char *info_block;
    int scale_factor;        /* 4 - 32 bit entry, 8 - 64 bit entry */
  };

struct obj_unwind_info
  {
    void *table;		/* Pointer to unwind info */
    CORE_ADDR table_size;	/* size in bytes of table */
    struct unwind_table_entry *entries;
    struct unwind_table_entry *cache;	/* Pointer to last entry we found */
    int last;			/* Index of last entry */
  };

typedef struct data
  {
    CORE_ADDR dummy[2];
    CORE_ADDR func_addr;
    CORE_ADDR dp;
  }
opd_data;

typedef struct obj_private_struct
  {
    void *lmdp;			/* really a load_module_desc * */
    struct obj_unwind_info *unwind_info;	/* a pointer */
    struct so_list *so_info;	/* a pointer  */
    /* GDB_TARGET_HAS_OPD - defined so the next two opd items are present */
    opd_data *opd;		/* a pointer  */
    int n_opd_entries;
  }
obj_private_data_t;

#define GDB_TARGET_HAS_OPD

extern CORE_ADDR target_read_pc PARAMS ((int));
extern void target_write_pc PARAMS ((CORE_ADDR, int));
extern CORE_ADDR skip_trampoline_code PARAMS ((CORE_ADDR, char *));

#define TARGET_PID_TO_EXEC_FILE(pid) hppa_pid_to_exec_file(pid)
extern char *hppa_pid_to_exec_file PARAMS ((int));

#define TARGET_ACKNOWLEDGE_FORKED_CHILD(pid) hppa_acknowledge_forked_child(pid)
extern void hppa_acknowledge_forked_child PARAMS ((int));

#define REQUIRE_ATTACH(pid) hppa_require_attach(pid)
extern int hppa_require_attach PARAMS ((int));

#define REQUIRE_DETACH(pid,signal) hppa_require_detach(pid,signal)
extern int hppa_require_detach PARAMS ((int, int));

/* This operation is basically a no-op for a ptrace-based HPUX target.
   It is required for a ttrace-based port, though.  See infttrace.c.
 */
#define TARGET_POST_STARTUP_INFERIOR(pid) hppa_post_startup_inferior(pid)
extern void hppa_post_startup_inferior PARAMS ((int));

/* This operation is intended to be used as the last in a sequence of
   steps taken when following both parent and child of a fork.  This
   is used by a clone of the debugger, which will follow the child.
   The original debugger has detached from this process, and the
   clone has attached to it.  On HPUX, this requires a bit of cleanup
   to make it work correctly.
 */
#define TARGET_POST_FOLLOW_INFERIOR() hppa_post_follow_inferior()
extern void hppa_post_follow_inferior PARAMS ((void));

#define proc_wait(pid,status) process_wait(pid,status)
extern int process_wait PARAMS ((int, int *));

#define TARGET_READ_PC(pid) target_read_pc (pid)
#define TARGET_WRITE_PC(v,pid) target_write_pc (v,pid)

/* For a number of horrible reasons we may have to adjust the location
   of variables on the stack.  Ugh.  */
#define HPREAD_ADJUST_STACK_ADDRESS(ADDR) hpread_adjust_stack_address(ADDR)

extern int hpread_adjust_stack_address PARAMS ((CORE_ADDR));


/* If the current gcc for for this target does not produce correct debugging
   information for float parameters, both prototyped and unprototyped, then
   define this macro.  This forces gdb to  always assume that floats are
   passed as doubles and then converted in the callee.

   For ia64, it appears that the debug info marks the parameters as
   floats regardless of whether the function is prototyped, but the
   actual values are passed as doubles for the non-prototyped case and
   floats for the prototyped case.  Thus we choose to make the
   prototyped case work for C. This is the opposite from what we do
   for PA (Because _I_ (RM) think this is better.
 */

#define COERCE_FLOAT_TO_DOUBLE(formal, actual) 0


/* IS_RELOC_ONLY_SYMBOL is used to identify symbols which used to be excluded
   from the minimal symbol table but which are now included because they
   are needed for linker fixups for DOOM.
 */

#define IS_RELOC_ONLY_SYMBOL(symname)					\
                 (strcmp(symname,"$START$")                             \
		 && (((symname)[0] == 'L' && (symname)[1] == '$')	\
		 || ((symname)[0] == '$'				\
		     && (symname)[strlen((symname)) - 1] == '$')	\
		 || ((symname)[0] == 'D' && (symname)[1] == '$')	\
		 || (strncmp ((symname), "$PIC", 4) == 0)               \
		 || ((symname)[0] == '$' && isdigit ((symname)[1]))     \
		 ))

/* ??? RM: dummy definition for now
   Sequence of bytes for breakpoint instruction.  */
#define BREAKPOINT32 0x0

#define HP_IA64

/* CC_HAS_LONG_LONG is defined so gdb will know what to do with long
   long's.  */
/*FIXME: this #define does not belong here; it should be automatically
 * added to ../../config.h during configuration.  It is defined for native
 * builds, but apparently not for gambit builds.
 */

#ifndef CC_HAS_LONG_LONG
#define CC_HAS_LONG_LONG
#endif

/* PRINTF_HAS_LONG_LONG lets gdb know that printf can handle LONG LONG
 */
/*FIXME: this #define does not belong here; it should be automatically
 * added to ../../config.h during configuration.
 */
#define PRINTF_HAS_LONG_LONG  1

/* HP uses an extended version of Dwarf2 */
#define HP_DWARF2_EXTENSIONS

/* HP dwarf2 is pretty buggy at the moment. Enable workarounds where possible
 */
#define HP_DWARF2_WORKAROUND_BUGS
#define GAMBIT_WORKAROUND_BUGS
/* C++ server code may not want to include the following. */

#ifndef PARTIAL_TM_INCLUDE
#define MSYMBOL_DOUBLES_AS_PSYMBOL
#endif /* #ifndef PARTIAL_TM_INCLUDE */

/* #define FAT_FREE_PSYMTABS */

/*-------------------------------------------------------------------
hp-ia64-solib related stuff.
-------------------------------------------------------------------*/

/* The feature DYNLINK_HAS_BREAK_HOOK is for implementations like PA64
   and Tahoe where the dynamic linker has a routine with a BREAK statement
   that is called for various events which are of interest to the debugger.
   The arguments passed to the routine distinguish the various events and
   provide the relevant information.  If DYNLINK_HAS_BREAK_HOOK is
   defined, the implementation needs to provide SOLIB_AT_DYNLINK_HOOK
   which returns TRUE if we are at the breakpoint in the dynamic linker
   and SOLIB_HANDLE_DYNLINK_EVENT.
 */

#define DYNLINK_HAS_BREAK_HOOK

#define DYNLINK_REGISTER_BP 	hp_ia64_dynlink_register_bp
#define DYNLINK_DELETE_EVENT	hp_ia64_dynlink_delete_event

/* The feature DP_SAVED_IN_OBJFILE indicates that there is a slot in each
   objfile which stores the data base register value (DP).
 */

#define DP_SAVED_IN_OBJFILE

/* The feature OBJFILE_HAS_IS_ARCHIVE indicates that there is a slot in each
   objfile which indicates whether the OBJFILE is archive bound.  The
   define IS_OBJFILE_ARCHIVE should also be defined.
 */

#define  OBJFILE_HAS_IS_ARCHIVE

#define IS_OBJFILE_ARCHIVE(objfile)					\
	(bfd_get_section_by_name ((objfile)->obfd, ".dynamic") == 0)

/* If there is a replacement for default_symfile_offsets in elf_sym_fns
   define SOLIB_SECTION_OFFSETS
 */

#define SOLIB_SECTION_OFFSETS   hp_ia64_solib_section_offsets
struct section_addr_info;
extern void
  hp_ia64_solib_section_offsets PARAMS ((struct objfile *, 
					 struct section_addr_info *));

#define BFD_IS_SWIZZLED	ia64_is_swizzled
extern boolean
  ia64_is_swizzled PARAMS ((bfd *));

#define SWIZZLE	swizzle
extern CORE_ADDR
  swizzle PARAMS ((CORE_ADDR));

#define UNSWIZZLE	unswizzle
extern CORE_ADDR
  unswizzle PARAMS ((CORE_ADDR));

extern boolean at_svc_load_break PARAMS ((CORE_ADDR));

/* FTN_DIMENSIONS_REVERSED is used when accessing an element of an array;  
   the debug information for the bounds of a Fortran array are emitted in
   reverse order and read in in the order emitted, so we need to know that
   this is the case to correctly determine the actual address of an element
   in an array.
 */
#define FTN_DIMENSIONS_REVERSED 1

#define F_CHECK_ARRAY_DESC(address) f_check_array_desc (address)
extern void f_check_array_desc (CORE_ADDR address);

#define F_GET_ARRAY_DESC_ARRAY_LBOUND(type, lower_bound) \
    f_get_array_desc_array_lbound (type, lower_bound)
extern int f_get_array_desc_array_lbound
    PARAMS((struct type *, long long *));

#define F_GET_ARRAY_DESC_ARRAY_UBOUND(type, upper_bound) \
    f_get_array_desc_array_ubound (type, upper_bound)
extern int f_get_array_desc_array_ubound
    PARAMS((struct type *, long long *));

#define F_SET_ARRAY_DESC_ARRAY_STRIDE(type) \
    f_set_array_desc_array_stride (type)
extern int f_set_array_desc_array_stride
    PARAMS((struct type *));

#define F_GET_ARRAY_DESC_ARRAY_ADDR(addr, offset) \
    f_get_array_desc_array_addr (addr, offset)
extern CORE_ADDR f_get_array_desc_array_addr PARAMS((CORE_ADDR, int));

#define F_ADJ_ARRAY_ADDR_W_NEG_STRIDE(addr, type) \
    f_adj_array_addr_w_neg_stride (addr, type)
extern CORE_ADDR f_adj_array_addr_w_neg_stride
    PARAMS((CORE_ADDR, struct type*));

#define F_ADJ_ARRAY_VALADDR_W_NEG_STRIDE(valaddr, type) \
    f_adj_array_valaddr_w_neg_stride (valaddr, type)
extern char * f_adj_array_valaddr_w_neg_stride
    PARAMS((char*, struct type*));

/* In the following AR registers, certaing bit fields are reserved:
   AR64		bits 61-58	51-38
   AR40		Bits 63-58	12
   */

#define AR64_RESERVED_MASK	0xc3f0003fffffffffLL
#define AR40_RESERVED_MASK	0x03ffffffffffefffLL

/* PRINTF_HAS_LONG_DOUBLE should be defined by the configure process, but
   since we are building IA64 with cross compilers, it isn't being defined
   for us.  Define it here.
   */

#ifndef PRINTF_HAS_LONG_DOUBLE
#define PRINTF_HAS_LONG_DOUBLE 1
#endif

#define SET_TRACE_BIT	set_trace_bit

#ifndef HP_XMODE
#define HP_XMODE
#endif

#define START_BOR_CALL(target_addr)	start_bor_call (target_addr)
extern boolean start_bor_call (CORE_ADDR target_addr);

#define SOLIB_BOR_EVENT()	solib_bor_event ()
extern CORE_ADDR solib_bor_event (void);

/* Define this so that the gdb_crash_handler functionality in main.c
   gets turned on. */
#ifndef GDB_CRASH_HANDLER
#define GDB_CRASH_HANDLER
#endif

extern struct unwind_table_entry *find_unwind_entry (CORE_ADDR);

#define OBJFILE_HAS_DLT

#define OBJFILE_HAS_LINKMAP

/* For IA64 we don't have mangled names in the debug info, so we keep a
   pointer to the struct symbol.  See dwarf2read.c struct fn_field.
   */

#define FN_FIELD_HAS_SYM

/* Define this to be able to backtrace from corrupted PC frame. */
#define HANDLE_BAD_PC handle_bad_pc
extern int handle_bad_pc (struct frame_info * frame);

/* Target specific RTC definitions. */
/*-------------------------------------- */

#define PATCHUP_DEFINITIONS(o) ia64_patchup_definitions(o)
/* JAGae75847 - Allow heap checking to work with third party malloc.
   Define to obtain the rtc_dot_sl_got_value of the librtc.sl */
#define SOLIB_GET_GOT_BY_PC(a) hp_ia64_solib_get_got_by_pc(a)
/* GR1 - GR 31, 96 dirty registers & TP. */
#define RTC_REGISTER_BUFFER_SIZE (128 * 8)
/* There is a minor difference in the startup model between PA32 and
     IA64 which needs to be dealt with: on PA32, when we receive the
     first notification from dld about shared libraries being mapped,
     all implicitly linked shared libraries have been mapped in. On
     IA64, we receive one call back per library. */
#define NOTIFCTN_PER_LIB_LOAD

#define SOLIB_ADJUST_INFERIOR_HOOK(pid) hp_ia64_solib_adjust_inferior_hook(pid)
extern void hp_ia64_solib_adjust_inferior_hook ( int );

/* On IPF, F90 adds an underscore to the linker symbol for a function,
   e.g. bar appears as bar_ to the linker.  The user, however, wants to
   interact with the debugger in terms of bar not bar_.
   */

#define FORTRAN_ADDS_UNDERSCORE

/* For HP-UX on IPF we have an implementation
   for the exception handling target op (in ia64-tdep.c) */
#define CHILD_ENABLE_EXCEPTION_CALLBACK
#define CHILD_GET_CURRENT_EXCEPTION_EVENT
#define TARGET_CATCH_INFO(arg, from_tty) \
 ia64_catch_info ((arg), (from_tty))
extern void ia64_catch_info ( char *, int );

/* start ------------------------------------------------- LRE defines vv */

/* Macro to decide for a paricular register whether we need to flip it.
   We only flip if the TARGET_BYTE_ORDER == LITTL_ENDIAN and only for
   particular registers.  The register should have an even number of bytes
   if it is to be flipped. 
 */

#define IS_TARGET_LRE	(TARGET_BYTE_ORDER == LITTLE_ENDIAN)

#define IS_FLIP_REG(regno) 						\
    (   (TARGET_BYTE_ORDER == LITTLE_ENDIAN) 				\
     && (REGISTER_RAW_SIZE (regno) == 8)				\
     && (   (   (regno) < AR0_REGNUM || regno > ARLAST_REGNUM		\
	    )								\
	 && ((regno) != CFM_REGNUM)					\
	 ))

#define IS_REG_VAL_FLIPPED(regno)					\
    (IS_TARGET_LRE && REGISTER_RAW_SIZE (regno) == 8)

/* For code which needs to be revisited */
#define IS_LRE_HACK	IS_TARGET_LRE

/* If the LRE_UNW_HDR_VERSION had a high-order bit set it would
   indicate a 32-bit unwind table, e.g. 0x8000000000000002 
 */

#define LRE_UNW_HDR_VERSION	2

/* end ------------------------------------------------- LRE defines ^^ */


/* Pragmas for better optimization */

/* function name list derived from gcc NORETURN keywords in defs.h */
#pragma NO_RETURN perror_with_name, verror, error, error_stream
#pragma NO_RETURN internal_verror, internal_error, return_to_top_level
#pragma NO_RETURN nomem, longjmp, siglongjmp, _longjmp, exit, _exit

#pragma RARELY_CALLED perror_with_name, verror, error, error_begin, error_stream
#pragma RARELY_CALLED internal_verror, internal_error, return_to_top_level
#pragma RARELY_CALLED nomem, longjmp, siglongjmp, _longjmp, exit, _exit

extern CORE_ADDR add_to_bsp (CORE_ADDR bsp, long size);

/* On IPF, SP is always 16 byte aligned */

#define SP_INCR		16

extern void get_load_info (void);

extern int 		have_read_load_info;
extern CORE_ADDR	load_info_addr;

