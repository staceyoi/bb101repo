/* Parameters for execution on an HP SPPA-RISC machine, running under Gambit, 
   for GDB.
   Copyright 1991, 1992 Free Software Foundation, Inc. 

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


/* Redefine some target bit sizes from the default.  */
		/* tjc -- this may not be required */
#define TARGET_LONG_BIT ((is_swizzled ? 32 : 64))
#define TARGET_LONG_LONG_BIT 64
#define TARGET_PTR_BIT 64


/* For most targets, a pointer on the target and its representation as an
   address in GDB have the same size and "look the same".  For such a
   target, you need only set TARGET_PTR_BIT / ptr_bit and TARGET_ADDR_BIT
   / addr_bit will be set from it.
  
   If TARGET_PTR_BIT and TARGET_ADDR_BIT are different, you'll probably
   also need to set POINTER_TO_ADDRESS and ADDRESS_TO_POINTER as well.
  
   ptr_bit is the size of a pointer on the target */

/* It would be nice if TARGET_PTR_BIT == TARGET_ADDR_BIT, but they're used
   differently in our older drop from FSF and newer FSF edits that use 
   TARGET_ADDR_BIT. */
#define TARGET_ADDR_BIT ((is_swizzled ? 32 : 64))

#define HP_IA64_NATIVE

#define HP_MXN

/* For the moment we are building for DEV5 but our sources have been updated
   for DEV6.  HP_IA64_NATIVE_DEV5 lets us have different code where we
   have to.

   8/7/00 - Now we are compiling for IC4 == DEV6 and later.  We would need
   to turn this on for DEV5 (IC3 or IC2 )
#define HP_IA64_NATIVE_DEV5
   */

#define U_REGS_OFFSET 0		/* supposed to be in nm.h */

#define GDB_TARGET_IS_PA_ELF

#define HP_COMPILED_TARGET 3

/* FLOAT80 is only supported on the native IA64 gdb, not gambit */

#define TARGET_FLOAT80_BIT 128

/* Mostly it's common to all ia64's.  */
#include "ia64/tm-ia64.h"
