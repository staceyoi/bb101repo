/* Parameters for execution on an HP SPPA-RISC machine, running under Gambit, 
   for GDB.
   Copyright 1991, 1992 Free Software Foundation, Inc. 

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* Redefine some target bit sizes from the default.  */
		/* tjc -- this may not be required */
#define TARGET_LONG_BIT 64
#define TARGET_LONG_LONG_BIT 64
#define TARGET_PTR_BIT 64


/* Mostly it's common to all ia64's.  */
#include "ia64/tm-ia64.h"
#define U_REGS_OFFSET 0		/* supposed to be in nm.h */
#define HP_IA64_GAMBIT

#define GDB_TARGET_IS_PA_ELF

#define HP_COMPILED_TARGET 3

/* Function to flush RSE as required by the unwind library.
   Returns nonzero if flush_RSE succeeds in the remote ia64 system.  
 */
extern int remote_ia64_flush_RSE PARAMS ((void));
