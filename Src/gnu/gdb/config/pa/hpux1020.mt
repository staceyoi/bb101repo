# Target: HP PA-RISC running HP-UX 10.20
MT_CFLAGS = -D__HP_CURSES
TDEPFILES= corelow.o hppa-tdep.o hpux-thread.o somread.o somsolib.o gdbrtc.o
TM_FILE= tm-hpux1020.h
MT_LIBRTC_EX= +e U_get_shLib_text_addr +e U_get_shLib_unw_tbl

RTC_TDEPFILES= hppa-rtc.o Ugetfram.o hppa-readable.o
RTC_TDEP_SLFILES= hppa-rtc_sl.o Ugetfram.o hppa-readable.o
RTC_TARGETS= librtc32.sl
