# Target: HP PA-RISC running HPUX 11.00
MT_CFLAGS = -D__HP_CURSES -DSEEK_64_BITS
TDEPFILES= corelow.o hppa-tdep.o hpux-thread.o somread.o somsolib.o gdbrtc.o
TM_FILE= tm-hpux11.h
MT_LIBRTC_EX = +e U_get_shLib_recv_tbl +e U_get_shLib_text_addr +e U_get_shLib_unw_tbl

RTC_TDEPFILES= hppa-rtc.o Ugetfram.o hppa-readable.o
RTC_TDEP_SLFILES= hppa-rtc_sl.o Ugetfram.o hppa-readable.o
RTC_TARGETS= librtc32.sl
INLINE_FLAG=

RTC_LCL= -L../../../../../BUILD_ENV/usr/lib -lcl +cdp ../../../../../BUILD_ENV/usr/lib:/usr/lib
RTC_FILE_OFFSET_BITS= -U_FILE_OFFSET_BITS
