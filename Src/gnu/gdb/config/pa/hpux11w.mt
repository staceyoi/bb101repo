# Target: HP PA-RISC 2.0 running HPUX 11.00 in wide mode
MT_CFLAGS = -D__HP_CURSES -DSEEK_64_BITS
TDEPFILES= corelow.o hppa-tdep.o pa64solib.o gdbrtc.o
TM_FILE= tm-hpux11w.h
MT_LIBRTC_EX = +e U_get_shLib_text_addr +e U_get_shLib_unw_tbl

RTC_TDEPFILES= hppa-rtc.o Ugetfram.o hppa-readable.o
RTC_TDEP_SLFILES= hppa-rtc_sl.o Ugetfram.o hppa-readable.o
RTC_TARGETS= librtc64.sl
INLINE_FLAG=

RTC_LCL64= -L../../../../../BUILD_ENV/usr/lib/pa20_64 -lcl +cdp ../../../../../BUILD_ENV/usr/lib/pa20_64:/usr/lib/pa20_64
RTC_LCL= $(RTC_LCL64)
RTC_FILE_OFFSET_BITS= -U_FILE_OFFSET_BITS
