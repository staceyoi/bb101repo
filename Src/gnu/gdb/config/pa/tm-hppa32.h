/* tm-hppa32.h Target specific declarations common to all of the HPPA-32
   targets.
 */

/* Bindu: dl_header is present in SOM debug format. Not available for
   elf format. This is useful in initializing the import list. */
#define TARGET_HAS_DL_HEADER

/* srikanth, 980830, CLLbs14756 dld32 does not invoke the callbacks
   for implictly loaded libraries */
#define IMPLICIT_SHLIB_EVENT_NOTIFICATION_LACKING

#define AUTO_LOAD_DEBUG_INFO

/* srikanth, 020423, On PA32, shared library long branch stubs come in two
   flavors : ones branching to millicode (link register r31) and ones
   branching to regular functions (link register r2). Both have a stub
   type of 14 in the unwind table !!!! This is because they ran out of
   bits to encode a new stub type !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   Gdb is always using r2 as the link register and this causes next to
   turn into a continue when the code path passes through a long branch
   stub leading to milli routine. We now resort to AI.
*/
#define OVERLOADED_LONG_BRANCH_STUBS

/*------------------- Target specific RTC functions -------------------*/

#define PATCHUP_PLT_IN_THIS_OBJFILE(o) som_patchup_plt_in_this_objfile (o)
#define PATCHUP_DEFINITIONS(o) som_patchup_definitions(o)
#define SOLIB_GET_GOT_BY_PC(a) som_solib_get_got_by_pc(a)

#define RTC_REGISTER_BUFFER_SIZE (32 * 4) /* GR1 - GR31 & TP */

/*---------------------------------------------------------------------*/

#define SOLIB_ADJUST_INFERIOR_HOOK(pid) som_solib_adjust_inferior_hook(pid)
#define CONTINUE_AT_DYNLINK_HOOK_RETURN_PC 1

/*---------------------------------------------------------------
    CMA threads declarations
  CMA_THREAD_FLAG	Mask for the bit which distinguishes a gdb PID for
			a CMA thread.

  FIND_NEW_THREADS	Routine to call to notify GDB about new threads.  Our
			implementation also removes threads which are gone.
			There is also a declaration for hppa_find_new_threads
			here.

  FIND_ACTIVE_THREAD	Routine to return the gdb PID of the active thread.

  FORCE_CMA_THREAD_SWITCH	Routine to make the selected thread the
			next CMA thread to schedule and to zero out the
			quantum of the current thread.

  PIDGET		Extract an OS process number from a gdb PID.

  IS_CMA_PID		Macro to say if a given PID is a CMA PID

  THREAD_FETCH_REGISTERS	Routine to get the register state of a
			thread.

  THREAD_IDX		Extract the thread index from a gdb thread id.

  target_new_objfile	Macro to call a CMA thread implementation routine
			to let it know about object files as they are read
			in and to notify when all are unloaded.
  */

#define CMA_THREAD_FLAG		0x20000000

#define FIND_NEW_THREADS 	hppa_find_new_threads
void hppa_find_new_threads (void);

#define FIND_ACTIVE_THREAD	hpux_find_active_thread
int hpux_find_active_thread (void);

#define FORCE_CMA_THREAD_SWITCH hpux_force_cma_thread_switch
void hpux_force_cma_thread_switch (int);

#define IS_CMA_PID(pid) (is_prog_using_cma_threads && ((pid) & CMA_THREAD_FLAG))

extern int is_prog_using_cma_threads;
extern int is_pid_larger;
extern int cma_process_pid;


#define PIDGET(pid)  ((is_prog_using_cma_threads && (pid) != -1) 	\
                       ? (((pid) & CMA_THREAD_FLAG) ? (cma_process_pid) \
                                                       : (pid))\
		       : (pid))

#define THREAD_FETCH_REGISTERS hpux_thread_fetch_registers
void hpux_thread_fetch_registers (int);

#define THREAD_IDX(pid)	((pid) & ~CMA_THREAD_FLAG)

#define target_new_objfile(OBJFILE) hpux_thread_new_objfile (OBJFILE)

/*---------------------------------------------------------------
    end CMA threads declarations
  */

#define CORE_TRUNCATION_DETECTION 1 /* Core file truncation detection */

/* For inline debugging support*/
#define EXTRA_FRAME_INFO \
   int inline_idx; \
   int frame_type;
