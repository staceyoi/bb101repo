#ifndef TM_HPUX1020
#define TM_HPUX1020

#define HPUX_1020 1

#undef HP_XMODE

#include "pa/tm-hppah.h"
#include "pa/tm-hppa32.h"

#define NO_KERNEL_THREADS 1

#endif
