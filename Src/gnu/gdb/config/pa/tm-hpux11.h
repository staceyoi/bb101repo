#ifndef TM_HPUX11
#define TM_HPUX11

#define HPUX_1100 1

#ifndef HP_XMODE
/*  
 *  This definition exists to enable cross mode support 
 *  between PA32 and PA64.  It is defined here because we need
 *  to have this capability when we build gdb32 as well as gdb64.
 */

#define HP_XMODE 1

#endif

/* srikanth, 071900, ttrace_wait should not be called after
   ttrace TT_PROC_ATTACH.
*/
#define ATTACH_NO_WAIT 1

#define HP_MXN

#include "pa/tm-hppah.h"
#include "pa/tm-hppa32.h"

/* Java Stack unwind. */
#define DO_JAVA_STACK_UNWIND(next_frame, prev)\
        do_java_stack_unwind_pa((next_frame), (prev))
#define GET_JAVA_FUNC_NAME(frame) \
        get_java_func_name_pa((frame))

#endif
