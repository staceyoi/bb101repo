/* Extract registers from a "standard" core file, for GDB.
   Copyright (C) 1988-1998  Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* Typically used on systems that have a.out format executables.
   corefile.c is supposed to contain the more machine-independent
   aspects of reading registers from core files, while this file is
   more machine specific.  */

#include "defs.h"

#ifdef HAVE_PTRACE_H
#include <ptrace.h>
#else
#ifdef HAVE_SYS_PTRACE_H
#include <sys/ptrace.h>
#endif
#endif

#include <sys/types.h>
#include <sys/param.h>
#include "gdbcore.h"
#include "gdbthread.h"
#include "value.h"		/* For supply_register.  */
#include "inferior.h"		/* For ARCH_NUM_REGS. */

/* These are needed on various systems to expand REGISTER_U_ADDR.  */
#ifndef USG
#include "gdb_dirent.h"
#include <sys/file.h>
#include "gdb_stat.h"
#include <sys/user.h>
#endif

#ifndef CORE_REGISTER_ADDR
#define CORE_REGISTER_ADDR(regno, regptr) register_addr(regno, regptr)
#endif /* CORE_REGISTER_ADDR */

#ifdef NEED_SYS_CORE_H
#include <sys/core.h>
#endif

static void fetch_core_registers (char *, unsigned, int, CORE_ADDR);
extern char *core_save_state_gen_reg;
extern int core_save_state_flags[1];
void _initialize_core_aout (void);

/* Extract the register values out of the core file and store
   them where `read_register' will find them.

   CORE_REG_SECT points to the register values themselves, read into memory.
   CORE_REG_SIZE is the size of that area.
   WHICH says which set of registers we are handling (0 = int, 2 = float
   on machines where they are discontiguous).
   REG_ADDR is the offset from u.u_ar0 to the register values relative to
   core_reg_sect.  This is used with old-fashioned core files to
   locate the registers in a large upage-plus-stack ".reg" section.
   Original upage address X is at location core_reg_sect+x+reg_addr.
 */

static void
fetch_core_registers (char *core_reg_sect,
		      unsigned core_reg_size,
		      int which,
		      CORE_ADDR reg_addr)
{
  int regno;
  CORE_ADDR addr;
  int bad_reg = -1;
  extern CORE_ADDR register64_offset;
  CORE_ADDR reg_ptr = -reg_addr;	/* Original u.u_ar0 is -reg_addr. */
  int numregs = ARCH_NUM_REGS;

#ifdef FIND_ACTIVE_THREAD
  /* If we need to deal with CMA threads then if the CMA_THREAD_FLAG is
     set and the current pid is not the active thread, then call 
     hpux_thread_fetch_registers to restore the registers.
     */
  
  if (   IS_CMA_PID (inferior_pid)
      && inferior_pid != FIND_ACTIVE_THREAD ())
    {
      THREAD_FETCH_REGISTERS (-1);  /* -1 means fetch all registers. */
      return;
    }
#endif

  /* If u.u_ar0 was an absolute address in the core file, relativize it now,
     so we can use it as an offset into core_reg_sect.  When we're done,
     "register 0" will be at core_reg_sect+reg_ptr, and we can use
     CORE_REGISTER_ADDR to offset to the other registers.  If this is a modern
     core file without a upage, reg_ptr will be zero and this is all a big
     NOP.  */
  if (reg_ptr > core_reg_size)
    reg_ptr -= KERNEL_U_ADDR;

#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
  /* Stacey 10/8/2001
     Save offsets of 64 bit register values into global variables
     so that we can use it to access 64 bit values of registers 
     from the save state structure in a core file on a PA2.0 machine. */

  register64_offset = (unsigned long) &((save_state_t *) 0)->ss_wide + reg_ptr;
#endif

  for (regno = 0; regno < numregs; regno++)
    {
      addr = CORE_REGISTER_ADDR (regno, reg_ptr);
      if (addr >= core_reg_size
	  && bad_reg < 0)
	bad_reg = regno;
      else
	supply_register (regno, core_reg_sect + addr);
    }

#ifdef GET_CORE_STACKED_REGS
  GET_CORE_STACKED_REGS (core_reg_sect, reg_ptr);
#endif

  if (bad_reg >= 0)
    error ("Register %s not found in core file.", REGISTER_NAME (bad_reg));
  /* save general_purpose_registers section of core file for doing sanity check */
  if (which == 0) 
   {
     core_save_state_gen_reg = core_reg_sect + register64_offset;
     memcpy (core_save_state_flags, core_reg_sect + reg_ptr, sizeof (int));
    }
}


#ifdef REGISTER_U_ADDR

/* Return the address in the core dump or inferior of register REGNO.
   BLOCKEND is the address of the end of the user structure.  */

CORE_ADDR
register_addr (int regno, CORE_ADDR blockend)
{
  CORE_ADDR addr;

  if (regno < 0 || regno >= ARCH_NUM_REGS)
    error ("Invalid register number %d.", regno);

  REGISTER_U_ADDR (addr, blockend, regno);

  return addr;
}

#endif /* REGISTER_U_ADDR */


/* Register that we are able to handle aout (trad-core) file formats.  */

static struct core_fns aout_core_fns =
{
  bfd_target_unknown_flavour,		/* core_flavour */
  default_check_format,			/* check_format */
  default_core_sniffer,			/* core_sniffer */
  fetch_core_registers,			/* core_read_registers */
  NULL					/* next */
};

void
_initialize_core_aout ()
{
  add_core_fns (&aout_core_fns);
}
