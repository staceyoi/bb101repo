/* Core dump and executable file functions below target vector, for GDB.
   Copyright 1986, 87, 89, 91, 92, 93, 94, 95, 96, 97, 1998, 2000
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "gdb_string.h"
#include <errno.h>
#include <signal.h>

#ifdef HP_IA64
#include <sys/utsname.h>
#endif

#define _LARGEFILE64_SOURCE /* Required for using stat64/open64 */
#include <fcntl.h>
#include "defs.h"
#include <sys/stat.h>
#include "elf-bfd.h"
#include "frame.h"		/* required by inferior.h */
#include "inferior.h"
#include "symtab.h"
#include "command.h"
#include "bfd.h"
#include "target.h"
#include "gdbcore.h"
#include "gdbthread.h"
#include "gdbcmd.h"

#include <sys/core.h>
#ifndef IS_CMA_PID
#define IS_CMA_PID(pid)		0
#endif

/* Current bfd of the core. */
bfd *current_core_bfd;

/* For debugging a core produced using dumpcore command */
int live_core = 0;

#ifdef HP_IA64
/* QXCR1000579474: Aries core file debugging. */
/* The following are variables used to signify the runtime text and data
   start addresses of the aries load module. These get filled when the user
   executes the 'aries-core' command at the gdb prompt. These are used
   by gdb to be able to map the addresses to actual symbols within the aries
   load module. */
CORE_ADDR aries_runtime_text_start;
CORE_ADDR aries_linktime_text_start = 0;
CORE_ADDR aries_runtime_data_start;
CORE_ADDR aries_linktime_unwind_start;
CORE_ADDR aries_runtime_unwind_start;
unsigned long aries_text_size;
/* This is used to signify that we are debugging a aries core file. Gets set
   in aries_core_command. */
bool aries_core_debug = false;
bool add_aries_symbol_file = false;
char *aries_module_name = NULL;
static void aries_core_command (char *args, int from_tty);
extern void add_symbol_file_command (char *, int);

static void mmapfile_command (char *arg, int from_tty);

/* Mixed mode corefile debug support. */
int         mixed_mode_debug_aries = 0;
extern void invalidate_rse_info (void);
extern void mixed_mode_cleanup (void);
extern int total_stack_frames;
#endif


/* List of all available core_fns.  On gdb startup, each core file register
   reader calls add_core_fns() to register information on each core format it
   is prepared to read. */

static struct core_fns *core_file_fns = NULL;

/* The core_fns for a core file handler that is prepared to read the core
   file currently open on core_bfd. */

static struct core_fns *core_vec = NULL;

static void core_files_info (struct target_ops *);

#ifdef SOLIB_ADD
static int solib_add_stub (PTR);
#endif

static struct core_fns *sniff_core_bfd (bfd *);

static boolean gdb_check_format (bfd *);

static void core_open (char *, int);

void core_detach (char *, int);

static void core_close (int);

static void core_close_cleanup (void *ignore);

static void get_core_registers (int);

static void add_to_thread_list (bfd *, asection *, PTR);

static int ignore (CORE_ADDR, char *);

static char *core_file_to_sym_file (char *);

static int core_file_thread_alive (int tid);

static void init_core_ops (void);

void _initialize_corelow (void);

#ifdef HP_IA64
void print_core_sysname (void);
extern struct utsname *elf_core_file_sysname (bfd *);
#endif

struct target_ops core_ops;
char *core_save_state_gen_reg;
int core_save_state_flags[1];
void core_file_sanity_check(int);

#ifdef HP_XMODE
/* 
 * External functions defined in top.c to check the executable format
 * of the program to exec and to launch a different gdb if necessary.
 */
extern int xmode_exec_format_is_different (char *executable);
extern void xmode_launch_other_gdb (char *new_file, enum xmode_cause reason);
#endif

#ifdef CORE_TRUNCATION_DETECTION
  #if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
         void check_core_truncation (void);
  #else
         void check_core_truncation_pa32 (void);
  #endif
#endif  /* CORE_TRUNCATION_DETECTION */

CORE_ADDR core_rse_stack;
extern boolean elf_read_core_registers (bfd *abfd);

/* Link a new core_fns into the global core_file_fns list.  Called on gdb
   startup by the _initialize routine in each core file register reader, to
   register information about each format the the reader is prepared to
   handle. */

void
add_core_fns (struct core_fns *cf)
{
  cf->next = core_file_fns;
  core_file_fns = cf;
}

/* The default function that core file handlers can use to examine a
   core file BFD and decide whether or not to accept the job of
   reading the core file. */

int
default_core_sniffer (struct core_fns *our_fns, bfd *abfd)
{
  int result;

  result = (bfd_get_flavour (abfd) == our_fns -> core_flavour);
  return (result);
}

/* Walk through the list of core functions to find a set that can
   handle the core file open on ABFD.  Default to the first one in the
   list of nothing matches.  Returns pointer to set that is
   selected. */

static struct core_fns *
sniff_core_bfd (bfd *abfd)
{
  struct core_fns *cf;
  struct core_fns *yummy = NULL;
  int matches = 0;;

  for (cf = core_file_fns; cf != NULL; cf = cf->next)
    {
      if (cf->core_sniffer (cf, abfd))
	{
	  yummy = cf;
	  matches++;
	}
    }
  if (matches > 1)
    {
      warning ("\"%s\": ambiguous core format, %d handlers match",
	       bfd_get_filename (abfd), matches);
    }
  else if (matches == 0)
    {
      warning ("\"%s\": no core file handler recognizes format, using default",
	       bfd_get_filename (abfd));
    }
  if (yummy == NULL)
    {
      yummy = core_file_fns;
    }
  return (yummy);
}

/* The default is to reject every core file format we see.  Either
   BFD has to recognize it, or we have to provide a function in the
   core file handler that recognizes it. */

int
default_check_format (bfd *abfd)
{
  return (0);
}

/* Attempt to recognize core file formats that BFD rejects. */

static boolean 
gdb_check_format (bfd *abfd)
{
  struct core_fns *cf;

  for (cf = core_file_fns; cf != NULL; cf = cf->next)
    {
      if (cf->check_format (abfd))
	{
	  return (true);
	}
    }
  return (false);
}

/* Discard all vestiges of any previous core file and mark data and stack
   spaces as empty.  */

/* ARGSUSED */
static void
core_close (int quitting)
{
  char *name;

  if (core_bfd)
    {
      inferior_pid = 0;		/* Avoid confusion from thread stuff */

      /* Clear out solib state while the bfd is still open. See
         comments in clear_solib in solib.c. */
#ifdef CLEAR_SOLIB
      CLEAR_SOLIB ();
#endif

      name = bfd_get_filename (core_bfd);
      if (!bfd_close (core_bfd))
	warning ("cannot close \"%s\": %s",
		 name, bfd_errmsg (bfd_get_error ()));
      free (name);
      core_bfd = NULL;
      if (core_ops.to_sections)
	{
	  free ((PTR) core_ops.to_sections);
	  core_ops.to_sections = NULL;
	  core_ops.to_sections_end = NULL;
	}
    }
  core_vec = NULL;
}

static void
core_close_cleanup (void *ignore)
{
  core_close (0/*ignored*/);
}

#ifdef SOLIB_ADD
/* Stub function for catch_errors around shared library hacking.  FROM_TTYP
   is really an int * which points to from_tty.  */

static int
solib_add_stub (PTR from_ttyp)
{
  /* Fixed DTS CLLbs14321 - auto-solib-add doesn't work for core files.
     The second parameter to SOLIB_ADD should be zero to indicate that
     it is not from tty.

     The same thing is done in  wait_for_inferior when it adds shared
     libraries automatically.
  */
  SOLIB_ADD (NULL, 0, &current_target);
  re_enable_breakpoints_in_shlibs ();
  return 0;
}
#endif /* SOLIB_ADD */

/* Look for sections whose names start with `.reg/' so that we can extract the
   list of threads in a core file.  */

static void
add_to_thread_list (bfd *abfd, asection *asect, PTR reg_sect_arg)
{
  int thread_id;
  asection *reg_sect = (asection *) reg_sect_arg;

  if (strncmp (bfd_section_name (abfd, asect), ".reg/", 5) != 0)
    return;

  thread_id = atoi (bfd_section_name (abfd, asect) + 5);

  add_thread (thread_id);

/* Warning, Will Robinson, looking at BFD private data! */

  if (reg_sect != NULL
      && asect->filepos == reg_sect->filepos)	/* Did we find .reg? */
    inferior_pid = thread_id;	/* Yes, make it current */
}

#ifdef HP_MXN

/* bindu 101601: For MxN threads, gdb needs to get both the kernel thread list and the 
 * thread list maintained by the pthread library. libmxndbg will help us collate this
 * list. libmxndbg will ask us for the kernel thread list and gets the pthreads 
 * thread list directly by reading the data of libpthread. 
 */

static bfd *the_bfd;
static void
read_thread_list(bfd *abfd)
{
  the_bfd = abfd;
  update_thread_state(0);
}

/* bindu 101601: Callback provided for libmxndbg to get the kernel thread list. */

struct thr_list* 
read_kthread_list()
{
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
  return bfd_kthread_list(the_bfd); 
#else
  return hpux_bfd_kthread_list(the_bfd); 
#endif
}

#endif
extern char* map_si_code_to_str (int signal, int si_code);

/* This routine opens and sets up the core file bfd.  */

static void
core_open (char *filename, int from_tty)
{
  extern bfd_vma bfd_core_rse_stack;

  const char *p;
  int siggy;
  int si_code;
  struct cleanup *old_chain;
  char *temp;
  bfd *temp_bfd;
  int ontop;
  int scratch_chan;

  target_preopen (from_tty);
  if (!filename)
    {
      error (core_bfd ?
	     "No core file specified.  (Use `detach' to stop debugging a core file.)"
	     : "No core file specified.");
    }

  filename = tilde_expand (filename);
  if (filename[0] != '/')
    {
      temp = concat (current_directory, "/", filename, NULL);
      free (filename);
      filename = temp;
    }

  old_chain = make_cleanup (free, filename);

  /* srikanth, open core files in large file mode for PA64. */
#ifndef SEEK_64_BITS
  scratch_chan = open (filename, write_files ? O_RDWR : O_RDONLY, 0);
#else 
  scratch_chan = open64 (filename, write_files ? O_RDWR : O_RDONLY, 0);
#endif
  if (scratch_chan < 0)
    perror_with_name (filename);

#ifdef HP_XMODE
  /* For cross mode support between 32-bit and 64-bit applications.
     If the core file format is different from what this
     gdb understands, launch the gdb that will understand
     this core file. */
      
  if (xmode_exec_format_is_different (filename))
    {
      if (exec_bfd == NULL)
        xmode_launch_other_gdb (filename, xmode_corefile);
      else
        {
	  printf ("Core file is in different format from executable\n");
	  error ("To unload the executable use the \"file\" command with no argument\n");
	}
    }
#endif

  temp_bfd = bfd_fdopenr (filename, gnutarget, scratch_chan);
  if (temp_bfd == NULL)
    perror_with_name (filename);

  if (!bfd_check_format (temp_bfd, bfd_core) &&
      !gdb_check_format (temp_bfd))
    {
      /* Do it after the err msg */
      /* FIXME: should be checking for errors from bfd_close (for one thing,
         on error it does not free all the storage associated with the
         bfd).  */
      make_cleanup_bfd_close (temp_bfd);
      error ("\"%s\" is not a core dump: %s",
	     filename, bfd_errmsg (bfd_get_error ()));
    }

  /* Looks semi-reasonable.  Toss the old core file and work on the new.  */

  discard_cleanups (old_chain);	/* Don't free filename any more */
  unpush_target (&core_ops);
  core_bfd = temp_bfd;
  core_rse_stack = SWIZZLE (bfd_core_rse_stack);

#ifdef BFD_IS_SWIZZLED
  is_swizzled = BFD_IS_SWIZZLED (core_bfd);
#endif

  old_chain = make_cleanup (core_close_cleanup, 0 /*ignore*/);

  /* Find a suitable core file handler to munch on core_bfd */
  core_vec = sniff_core_bfd (core_bfd);

#ifdef CORE_TRUNCATION_DETECTION
  #if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
     check_core_truncation ();
  #else
    check_core_truncation_pa32 ();
  #endif
#endif  /* CORE_TRUNCATION_DETECTION */

  validate_files ();

  /* Find the data section */
  if (build_section_table (core_bfd, &core_ops.to_sections,
			   &core_ops.to_sections_end))
    error ("\"%s\": Can't find sections: %s",
	   bfd_get_filename (core_bfd), bfd_errmsg (bfd_get_error ()));

  set_gdbarch_from_file (core_bfd);

  ontop = !push_target (&core_ops);
  discard_cleanups (old_chain);

#ifdef HP_IA64
  /* Read the registers after the core target has been created. We need to
     do it this way because on IPF some registers may reside in memory and
     we can read it only after target creation is complete. */
  if (elf_read_core_registers (core_bfd) == false)
    {
      warning ("failed to read core registers.");
    }
#endif

  p = bfd_core_file_failing_command (core_bfd);
  if (p)
    {
      printf_filtered ("Core was generated by `%s'.\n", p);
      if (strlen(p) == 14)	/* Workaround for JAGaf40286 */
        warning ("%s is 14 characters in length.  Due to a limitation \n\
	in the HP-UX kernel, core files contain only the first 14 characters \n\
	of an executable's name.  Check if %s is a truncated name. \n\
	If it is so, core-file, packcore and other commands dealing with \n\
	core files will exhibit incorrect behavior.  To avoid this, issue \n\
	exec-file and symbol-file commands with the full name of the executable \n\
	that produced the core; then issue the core-file, packcore or other \n\
	core file command of interest.\n", p, p);
     }

#ifdef HP_IA64
  /* QXCR1000579474: Aries core file debugging. Figure out the aries load
     module name. This has to be passed to the implicit call
     to add-symbol-file. */
  p = basename ((char *)p);
  if (p[0] == 'a' && strncmp (p, "aries", 5) == 0)
    {
      aries_module_name = xstrdup (p);
    }
#endif

  siggy = bfd_core_file_failing_signal (core_bfd);
  if (siggy > 0)
    /* NOTE: target_signal_from_host() converts a target signal value
       into gdb's internal signal value.  Unfortunatly gdb's internal
       value is called ``target_signal'' and this function got the
       name ..._from_host(). */
    printf_filtered ("Program terminated with signal %d, %s.\n", siggy,
		     target_signal_to_string (target_signal_from_host (siggy)));

#ifdef GDB_TARGET_IS_HPUX
  si_code = bfd_core_file_failing_si_code (core_bfd);
  printf_filtered ("%s\n", map_si_code_to_str (siggy, si_code));
#endif

  /* Build up thread list from BFD sections. */

  init_thread_list ();
  bfd_map_over_sections (core_bfd, add_to_thread_list,
			 bfd_get_section_by_name (core_bfd, ".reg"));

  current_core_bfd = core_bfd;
#ifdef HP_IA64
  have_read_load_info = 0;
  /* Mixed mode corefile debugging support: Reset a few mixed mode related
     globals since we are dealing with a new corefile and a possibly new
     debug-aries setting.
   */
  mixed_mode_cleanup ();

  /* jini: QXCR1000875632: (Re)initialize total_stack_frames to 0 since
     we are dealing with a new corefile here. */
  total_stack_frames = 0;
#endif
  if (ontop)
    {
      /* Fetch all registers from core file.  */
      target_fetch_registers (-1);

      /* Add symbols and section mappings for any shared libraries.  */
#ifdef SOLIB_ADD
      catch_errors (solib_add_stub, &from_tty, (char *) 0,
		    RETURN_MASK_ALL);
#endif


#ifdef HP_MXN
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
      /* If libpthread init is not done, do not go to libmxndbg to get the
	 the thread list. Consider this as non-mxn case. */
      if (is_mxn && !mxn_libpthread_init_done ())
	is_mxn = 0;
#endif

      if (is_mxn)
        {
          errno = 0;
          init_thread_list ();
          read_thread_list(core_bfd);

          /* If any errors occured during MxN thread list creation, re-read 
             it from the core file. We'll lose all MxN info, but at least
             we'll be able to provide some debugging functionality */
          if (errno)
            {
              warning("failed to read MxN thread list; possible causes:\n"
                      "- libpthread mismatch\n- core file corruption\n"
                      "- application memory corruption before core dump\n"
                      "reverting to kernel thread list\n");
              is_mxn = 0;
              init_thread_list ();
              bfd_map_over_sections (core_bfd, add_to_thread_list,
                                 bfd_get_section_by_name (core_bfd, ".reg"));
            }
        }
#endif

      /* Now, set up the frame cache, and print the top of stack.  */
      flush_cached_frames ();
#ifdef HP_IA64
      /* jini: Added invalidate_rse_info() to avoid an unwind failure
         when a backtrace is attempted after switching threads and reloading
         a core file. */  
      invalidate_rse_info();
#endif
      select_frame (get_current_frame (), 0);
      selected_frame->pc = SWIZZLE(selected_frame->pc);
      print_stack_frame (selected_frame, selected_frame_level, 1);
    }
  else
    {
      warning (
		"you won't be able to access this core file until you terminate\n\
your %s; do ``info files''", target_longname);
    }
    core_file_sanity_check(siggy);   
}
/* JAGae74207: sanity check for core-files */ 
void
core_file_sanity_check(int sig)
{
#ifndef HP_IA64
   int is_corrupt = 0;
   unsigned int reg;
   /* Checks are valid only for hardware generated signals */
   if (!( sig == 4 || sig == 8 || sig == 10 || sig == 11)) return;
   /* check for contents of PC with IIR value */
   if(!in_syscall(read_register(FLAGS_REGNUM)))  
     { 
       read_memory(read_register(33),(char *)&reg,4);
       if( reg != read_register(38))
         is_corrupt = 1;
     }
   /* validate PC space with space register values */
   if((*(core_save_state_flags) & 0x40) && sig == 11)
     {
 	long long pc_val = *((long long *)(core_save_state_gen_reg + 33 * 8));
        long long pc_space = *((long long *)(core_save_state_gen_reg + 34 * 8)); 
    	switch(pc_val & 0x3)
         {
          case 0 : if(pc_space != *((long long *)(core_save_state_gen_reg + 42 * 8))) 
		   is_corrupt = 1; break;
	  case 1 : if(pc_space != *((long long *)(core_save_state_gen_reg + 47 * 8))) 
		   is_corrupt = 1; break;
	  case 2 : if (pc_space != *((long long *)(core_save_state_gen_reg + 48 * 8))) 
                   is_corrupt = 1; break;
	  case 3 : if (pc_space != *((long long *)(core_save_state_gen_reg + 49 * 8))) 
		   is_corrupt = 1; 
         }
     }
   if (is_corrupt)
      warning("core file might be corrupted.");
#endif
}

void
core_detach (char *args, int from_tty)
{
  if (args)
    error ("Too many arguments");
  unpush_target (&core_ops);
  reinit_frame_cache ();
  if (from_tty)
    printf_filtered ("No core file now.\n");
}


/* Try to retrieve registers from a section in core_bfd, and supply
   them to core_vec->core_read_registers, as the register set numbered
   WHICH.

   If inferior_pid is zero, do the single-threaded thing: look for a
   section named NAME.  If inferior_pid is non-zero, do the
   multi-threaded thing: look for a section named "NAME/PID", where
   PID is the shortest ASCII decimal representation of inferior_pid.

   HUMAN_NAME is a human-readable name for the kind of registers the
   NAME section contains, for use in error messages.

   If REQUIRED is non-zero, print an error if the core file doesn't
   have a section by the appropriate name.  Otherwise, just do nothing.  */

/* Stacey 10/8/2001   
   The following two global variables are part of a work around for 
   the fact that GDB cannot handle 64 bit register values from PA2.0 
   machines - to allow GDB to read the true 64 bit values of registers
   from a core file if necessary.  They represent the following quantities:

   1.  Contents of save state structure from core file.  This data structure
       contains the 32 and 64 bit values of all registers from the moment
       the program died.  
   2.  Offset of start of 64 bit register values in save state structure.
   
   So core_save_state_contents + register64_offset is the address of 
   the beginning of 64 bit register values in the save state_structure.  
   For more info on the save state structure, see 
   
   /usr/include/machine/save_state.h 
   
   These variables should be removed and the code below should be adjusted
   in the event that GDB become able to handle 64 bit register values from 
   PA2.0 machines.  */

char *core_save_state_contents=NULL;
CORE_ADDR register64_offset=0x0;


#ifndef HP_IA64
static void
get_core_register_section (char *name,
			   int which,
			   char *human_name,
			   int required)
{
  char section_name[100];
  sec_ptr section;
  bfd_size_type size;
  char *contents;
#ifdef HP_MXN
/* bindu 101601: If no associated kernel thread, ask for registers from
   libmxndbg. Check infttrace.c for the use of TID_MARKER. */
  if (inferior_pid & TID_MARKER)
    {
      fetch_inferior_registers(-1);
      registers_fetched ();
      return;
    }
#endif


  if (inferior_pid && (IS_CMA_PID(inferior_pid)) == 0)
    sprintf (section_name, "%s/%d", name, inferior_pid);
  else
    strcpy (section_name, name);

  section = bfd_get_section_by_name (core_bfd, section_name);
  if (! section)
    {
      if (required)
	warning ("Couldn't find %s registers in core file. If this corefile "
		 "is generated using dumpcore or gcore, you may try "
		 "'set live-core 1' or run fixcore on the corefile and "
		 "reload the corefile.\n", human_name);
      return;
    }

  size = bfd_section_size (core_bfd, section);

  /* Stacey 10/8/2001
     Since 'contents' is now a global variable: core_save_state_contents
     the memory it points to should be allocated on the heap, 
     not on the stack because we don't wish to have it freed automatically 
     after this function returns.  

     contents = alloca (size); */

   core_save_state_contents = (char *) xmalloc (size);

  if (! bfd_get_section_contents (core_bfd, section, core_save_state_contents,
				  (file_ptr) 0, size))
    {
      warning ("Couldn't read %s registers from `%s' section in core file.\n",
	       human_name, name);
      return;
    }

  core_vec->core_read_registers (core_save_state_contents, size, which, 
				 ((CORE_ADDR)
				  bfd_section_vma (core_bfd, section)));
}
#endif	/* ! HP_IA64 */

/* ia64_get_core_registers() - cloned from get_core_registers on ia64 
   branch.

   Get the registers out of a core file.  This is the machine-
   independent part.  Fetch_core_registers is the machine-dependent
   part, typically implemented in the xm-file for each architecture.  */

/* We just get all the registers, so we don't use regno.  */

static void
ia64_get_core_registers()
{
  sec_ptr reg_sec;
  unsigned size;
  char *the_regs;
  char secname[30];
  enum bfd_flavour our_flavour = bfd_get_flavour (core_bfd);
  struct core_fns *cf = NULL;

  if (core_file_fns == NULL)
    {
      fprintf_filtered (gdb_stderr,
		     "Can't fetch registers from this type of core file\n");
      return;
    }

#ifdef HP_MXN
/* bindu 101601: If no associated kernel thread, ask for registers from
   libmxndbg. Check infttrace.c for the use of TID_MARKER. */
  if (inferior_pid & TID_MARKER)
    {
      fetch_inferior_registers(-1);
      registers_fetched ();
      return;
    }
#endif

  /* Thread support.  If inferior_pid is non-zero, then we have found a core
     file with threads (or multiple processes).  In that case, we need to
     use the appropriate register section, else we just use `.reg'. */

  /* XXX - same thing needs to be done for floating-point (.reg2) sections. */

  if ((inferior_pid != 0) && (IS_CMA_PID(inferior_pid)) == 0)
    sprintf (secname, ".reg/%d", inferior_pid);
  else
    strcpy (secname, ".reg");

  reg_sec = bfd_get_section_by_name (core_bfd, secname);
  if (!reg_sec)
    goto cant;
  size = (unsigned int) bfd_section_size (core_bfd, reg_sec);
  the_regs = (char *) alloca (size);
  /* Look for the core functions that match this flavor.  Default to the
     first one if nothing matches. */
  for (cf = core_file_fns; cf != NULL; cf = cf->next)
    {
      if (our_flavour == cf->core_flavour)
	{
	  break;
	}
    }
  if (cf == NULL)
    {
      cf = core_file_fns;
    }
  if (cf != NULL &&
      bfd_get_section_contents (core_bfd, reg_sec, the_regs, (file_ptr) 0, size) &&
      cf->core_read_registers != NULL)
    {
      (cf->core_read_registers (the_regs, size, 0,
			      (CORE_ADDR) bfd_section_vma (abfd, reg_sec)));
    }
  else
    {
    cant:
      fprintf_filtered (gdb_stderr,
			"Couldn't fetch registers from core file: %s\n",
			bfd_errmsg (bfd_get_error ()));
    }

  /* Now do it again for the float registers, if they exist.  */
  reg_sec = bfd_get_section_by_name (core_bfd, ".reg2");
  if (reg_sec)
    {
      size = (unsigned int) bfd_section_size (core_bfd, reg_sec);
      the_regs = (char *) alloca (size);
      if (cf != NULL &&
	  bfd_get_section_contents (core_bfd, reg_sec, the_regs, (file_ptr) 0, size) &&
	  cf->core_read_registers != NULL)
	{
	  (cf->core_read_registers (the_regs, size, 2,
			       (unsigned) bfd_section_vma (abfd, reg_sec)));
	}
      else
	{
	  fprintf_filtered (gdb_stderr,
		       "Couldn't fetch register set 2 from core file: %s\n",
			    bfd_errmsg (bfd_get_error ()));
	}
    }
  registers_fetched ();
} /* end ia64_get_core_registers */


/* Get the registers out of a core file.  This is the machine-
   independent part.  Fetch_core_registers is the machine-dependent
   part, typically implemented in the xm-file for each architecture.  */

/* We just get all the registers, so we don't use regno.  */

/* ARGSUSED */

static void
get_core_registers (int regno)
{
  int status;

  if (core_vec == NULL
      || core_vec->core_read_registers == NULL)
    {
      fprintf_filtered (gdb_stderr,
		     "Can't fetch registers from this type of core file\n");
      return;
    }

#ifdef HP_MXN
/* bindu 101601: If no associated kernel thread, ask for registers from 
   libmxndbg. Check infttrace.c for the use of TID_MARKER. */
  if (inferior_pid & TID_MARKER)
    {
      fetch_inferior_registers(-1);
      registers_fetched ();
      return;
    }
#endif

#ifndef HP_IA64
  get_core_register_section (".reg", 0, "general-purpose", 1);
  get_core_register_section (".reg2", 2, "floating-point", 0);
  get_core_register_section (".reg-xfp", 3, "extended floating-point", 0);
#else
  ia64_get_core_registers();
#endif

  registers_fetched ();
}

static char *
core_file_to_sym_file (char *core)
{
  CONST char *failing_command;
  char *p;
  char *temp;
  bfd *temp_bfd;
  int scratch_chan;

  if (!core)
    error ("No core file specified.");

  core = tilde_expand (core);
  make_cleanup (free, core);
  if (core[0] != '/')
    {
      temp = concat (current_directory, "/", core, NULL);
      core = temp;
    }

  scratch_chan = open (core, write_files ? O_RDWR : O_RDONLY, 0);
  if (scratch_chan < 0)
    perror_with_name (core);

#ifdef HP_XMODE
  /* For cross mode support between 32-bit and 64-bit applications.
     If the core file format is different from what this
     gdb understands, launch the gdb that will understand
     this core file. */
      
  if (xmode_exec_format_is_different (core))
    {
      if (exec_bfd == NULL)
        xmode_launch_other_gdb (core, xmode_corefile);
      else
        {
	  printf ("Core file is in different format from executable\n");
	  error ("To unload the executable use the \"file\" command with no argument\n");
	}
    }
#endif

  temp_bfd = bfd_fdopenr (core, gnutarget, scratch_chan);
  if (temp_bfd == NULL)
    perror_with_name (core);

  if (!bfd_check_format (temp_bfd, bfd_core))
    {
      /* Do it after the err msg */
      /* FIXME: should be checking for errors from bfd_close (for one thing,
         on error it does not free all the storage associated with the
         bfd).  */
      make_cleanup_bfd_close (temp_bfd);
      error ("\"%s\" is not a core dump: %s",
	     core, bfd_errmsg (bfd_get_error ()));
    }

  /* Find the data section */
  if (build_section_table (temp_bfd, &core_ops.to_sections,
			   &core_ops.to_sections_end))
    error ("\"%s\": Can't find sections: %s",
	   bfd_get_filename (temp_bfd), bfd_errmsg (bfd_get_error ()));

  failing_command = bfd_core_file_failing_command (temp_bfd);

  /* If we found a filename, remember that it is probably saved
     relative to the executable that created it.  If working directory
     isn't there now, we may not be able to find the executable.  Rather
     than trying to be sauve about finding it, just check if the file
     exists where we are now.  If not, then punt and tell our client
     we couldn't find the sym file.
   */
  p = (char *) failing_command;
  if ((p != NULL) && (access (p, F_OK) != 0))
    p = NULL;

  /* bfd__close would free up failing_command storage! */
  bfd_close (temp_bfd);

  return p;
}

static void
core_files_info (struct target_ops *t)
{
  print_section_info (t, core_bfd);
}

/* If mourn is being called in all the right places, this could be say
   `gdb internal error' (since generic_mourn calls breakpoint_init_inferior).  */

static int
ignore (CORE_ADDR addr, char *contents)
{
  return 0;
}


/* Okay, let's be honest: threads gleaned from a core file aren't
   exactly lively, are they?  On the other hand, if we don't claim
   that each & every one is alive, then we don't get any of them
   to appear in an "info thread" command, which is quite a useful
   behaviour.
 */
static int
core_file_thread_alive (int tid)
{
  return 1;
}

/* Fill in core_ops with its defined operations and properties.  */

static void
init_core_ops ()
{
  core_ops.to_shortname = "core";
  core_ops.to_longname = "Local core dump file";
  core_ops.to_doc =
    "Use a core file as a target.\n\nUsage:\n\ttarget core <CORE-FILE>\n\n"
    "Specify the filename of the core file.";
  core_ops.to_open = core_open;
  core_ops.to_close = core_close;
  core_ops.to_attach = find_default_attach;
  core_ops.to_require_attach = find_default_require_attach;
  core_ops.to_detach = core_detach;
  core_ops.to_require_detach = find_default_require_detach;
  core_ops.to_fetch_registers = get_core_registers;
  core_ops.to_xfer_memory = xfer_memory;
  core_ops.to_files_info = core_files_info;
  core_ops.to_insert_breakpoint = ignore;
  core_ops.to_remove_breakpoint = ignore;
  core_ops.to_create_inferior = find_default_create_inferior;
  core_ops.to_clone_and_follow_inferior = find_default_clone_and_follow_inferior;
  core_ops.to_thread_alive = core_file_thread_alive;
  core_ops.to_core_file_to_sym_file = core_file_to_sym_file;
  core_ops.to_stratum = core_stratum;
  core_ops.to_has_memory = 1;
  core_ops.to_has_stack = 1;
  core_ops.to_has_registers = 1;
  core_ops.to_magic = OPS_MAGIC;
}

#ifdef HP_IA64
static void
set_mixed_mode_debug_aries (char *args,
                            int from_tty,
                            struct cmd_list_element *c)
{
  if (target_has_execution)
    {
      error ("Setting debug-aries is valid only for corefile "
             "debugging sessions.");

    }
  if (target_has_stack)
    {
      error ("Please reload the corefile for the newly specified debug-aries\n"
             "setting to take effect.");
    }
}


static void
show_mixed_mode_debug_aries (char *args,
                             int from_tty,
                             struct cmd_list_element *c)
{
  if (!mixed_mode_debug_aries)
    {
      printf_filtered ("debug-aries is not enabled.\n");
    }
}
#endif


/* non-zero if we should not do the add_target call in
   _initialize_corelow; not initialized (i.e., bss) so that
   the target can initialize it (i.e., data) if appropriate.
   This needs to be set at compile time because we don't know
   for sure whether the target's initialize routine is called
   before us or after us. */
int coreops_suppress_target;

void
_initialize_corelow ()
{
#ifdef HP_IA64
  struct cmd_list_element *set, *show;
  struct cmd_list_element *c; /* For aries-core option. */
#endif

  init_core_ops ();

  if (!coreops_suppress_target)
    add_target (&core_ops);

/* dumpcore command - JAGae75018*/
  add_show_from_set
    (add_set_cmd ("live-core", class_support, var_zinteger,
                  (char *) &live_core,
                  "Set to debug core generated by dumpcore command\n\n\
Usage:\nTo set new value:\n\tset live-core <INTEGER>\nTo see current value:\
\n\tshow live-core\n", &setlist), &showlist);

#ifdef HP_IA64
  set = add_set_cmd ("debug-aries", class_support, var_boolean,
          (char *) &mixed_mode_debug_aries,
          "Set to debug and display aries frames in a mixed mode core.\n\n"
          "Usage:\nTo set new value:\n\tset debug-aries [on | off]\n"
          "To see current value:\n\tshow debug-aries\n\n"
          "Default is off.",
          &setlist);
  set->function.sfunc = set_mixed_mode_debug_aries;
  show = add_show_from_set (set, &showlist);
  show->function.sfunc = show_mixed_mode_debug_aries;

  /* QXCR1000579474: Aries core file debugging. Added aries-core command. This
     command needs to be executed so that gdb gets to know where the text and
     data segments of the aries load module have been mapped -- so that the
     addresses can be attributed to the right symbols. */
  c = add_cmd ("aries-core", class_support, aries_core_command,
        "Feed in the runtime aries text and data segment addresses to gdb\n"
        "so that an aries corefile can be debugged.\n\n"
        "Usage:\n\taries-core <ARIES-TEXT-RUNTIME-START-ADDR> "
        "<ARIES-DATA-RUNTIME-START-ADDR>\n",
        &cmdlist);

  c = add_cmd ("mmapfile", class_support, mmapfile_command,
        "Add contents of memory mapped files missing from core dump.\n\n"
        "Usage:\nmmapfile <FILENAME> <MAP-ADDRESS> <FILE-OFFSET> <LENGTH>\n\n"
        "This can be used if the core file does not contain mmap-ed regions\n"
        "from the live run. This can happen for files mmap-ed with\n"
        "MMAP_PRIVATE with RO permissions, or for files mmap-ed with\n"
        "MMAP_SHARED. With this command, the user can access data from\n"
        "the mmap-ed files, which are missing from the core file. The\n"
        "``info target'' command displays all the segments from the\n"
        "core file.\n",
        &cmdlist);
#endif
}

/* QXCR1000579474: Aries core file debugging. */
#ifdef HP_IA64
static void
aries_core_command (char *args, int from_tty)
{
  char *arg = NULL;
  int argcnt = 0;
  char add_symbol_file_args [PATH_MAX + 100];

  if (NULL == args)
    error ("aries-core takes 2 arguments.");

  while (*args != '\000')
    {
      /* Any leading spaces? */
      while (isspace (*args))
	args++;

      /* Point arg to the beginning of the argument. */
      arg = args;

      /* Move args pointer over the argument. */
      while ((*args != '\000') && !isspace (*args))
	args++;

      /* If there are more arguments, terminate arg and
         proceed past it. */
      if (*args != '\000')
	*args++ = '\000';

      /* Now process the argument. */
      if (argcnt == 0)
	{
	  /* The first argument is the aries runtime text start address. */
          if (arg[0] == '0' && arg[1] == 'x')
            sscanf (arg + 2, "%llx", &aries_runtime_text_start);
          else
            sscanf (arg, "%lld", &aries_runtime_text_start);
        }
      else if (argcnt == 1)
        {
          /* The second argument is the aries runtime data start address. */
          if (arg[0] == '0' && arg[1] == 'x')
            sscanf (arg + 2, "%llx", &aries_runtime_data_start);
          else
            sscanf (arg, "%lld", &aries_runtime_data_start);
        }
      else
        {
          error ("Incorrect number of arguments specified for aries-core.");
        }
      argcnt++;
    }
    if (2 != argcnt)
      {
        error ("Incorrect number of arguments specified for aries-core.");
      }
    aries_core_debug = true;

    /* Encapsulate the call to add-symbol-file of the aries load module
       in aries-core command itself. */
    sprintf (add_symbol_file_args, 
             "%s 0x%llx -s .data 0x%llx\n",
             aries_module_name,
             aries_runtime_text_start, aries_runtime_data_start);

    /* Setting a variable 'add_aries_symbol_file' to true so that
       add_symbol_file_command can distinguish that it has been called
       to add the aries symbol file and hence not prompt for a reconfirmation
       of the symbol file addition */
    add_aries_symbol_file = true;
    add_symbol_file_command (add_symbol_file_args, from_tty);
    add_aries_symbol_file = false;
}

/* Add a mmaped section missing from core file */
static void
mmapfile_command (char *arg, int from_tty)
{
  struct cleanup    *old_chain = (struct cleanup *)NULL;
  char              **argv = (char **)NULL;
  char              *mmaped_file = NULL;
  CORE_ADDR         address = 0;
  CORE_ADDR         offset  = 0;
  CORE_ADDR         length  = 0; 

  if (target_has_execution || !target_has_stack)
    {
      error ("Setting mmapfile is valid only for corefile "
             "debugging sessions.");

    }

  if (arg != (char *)NULL)
    {
      argv = buildargv(arg);
      old_chain = make_cleanup((make_cleanup_ftype *) freeargv, (char *)argv);

      if (!argv[0] || !argv[1] || !argv[2] || !argv[3])
        {
          printf("Usage:\nmmapfile <FILENAME> <MAP-ADDRESS> "
               "<FILE-OFFSET> <LENGTH>\n");
          goto end_of_stuff;
        }

      if (argv[0] != (char *)NULL)
        mmaped_file = argv[0];
      if (argv[1] != (char *)NULL)
        address = parse_and_eval_address (argv[1]);
      if (argv[2] != (char *)NULL)
        offset = parse_and_eval_address (argv[2]);
      if (argv[3] != (char *)NULL)
        length = parse_and_eval_address (argv[3]);
    }

  if (!mmaped_file || !address || !length)
    {
      printf("Usage:\nmmapfile <FILENAME> <MAP-ADDRESS> "
             "<FILE-OFFSET> <LENGTH>\n");
    }
  else
    {
      address = SWIZZLE(address);

      if (debug_traces)
        printf_filtered("mmapfile = %s, address = %lx, "
                        "offset = %lx, length = %lx\n",
                        mmaped_file, address, offset, length);

      int fd = open(mmaped_file, O_RDONLY);
      if ( fd == -1 )
        {
          printf("mmapfile = %s can not be opened (errno = %d)\n",
                 mmaped_file, errno);
          goto end_of_stuff;
        }
      else
        {
          close(fd);
        }

      if (!bfd_add_mmap_section(core_bfd, mmaped_file, address,
                                offset, length))
        {
          printf("mmapfile = %s can not be added\n", mmaped_file);
          goto end_of_stuff;
        }

      /* Add the last section from the bfd to the current target */
      add_target_core_bfd_last_section(core_bfd);

      core_ops.to_sections = current_target.to_sections;
      core_ops.to_sections_end = current_target.to_sections_end;

      current_core_bfd = core_bfd;
    }

end_of_stuff:
  if (old_chain)
    do_cleanups(old_chain);
}

#endif

int
is_live_core ()
{
  return live_core;
}

#ifdef HP_IA64
void
print_core_sysname ()
{
  struct utsname *un = NULL;
  char *buf = NULL;

  if (core_bfd)
    un = (struct utsname *) elf_core_file_sysname (core_bfd);
  if (un)
    {
      buf = (char *) xmalloc (100);
      printf_filtered ("Operating System Information:\n");
      printf_filtered ("  sysname  : %s\n", un->sysname);
      printf_filtered ("  nodename : %s\n", un->nodename);
      printf_filtered ("  release  : %s\n", un->release);
      switch (un->version[0])
        {
          case 'A':
	    strcpy (buf,"A (Two-user system)");
            break;
          case 'B':
	    strcpy (buf,"B (16-user system)");
            break;
          case 'C':
	    strcpy (buf,"C (32-user system)");
            break;
          case 'D':
	    strcpy (buf,"D (64-user system)");
            break;
          case 'E':
	    strcpy (buf,"E (8-user system)");
            break;
          case 'U':
	    strcpy (buf,"U (128-user, 256-user or"
                         " Unlimited-user system)");
            break;
        }
      printf_filtered ("  version  : %s\n", buf);
      printf_filtered ("  machine  : %s\n", un->machine);
      printf_filtered ("  idnumber : %s\n", un->__idnumber);
      free(buf);
    }
}
#endif

#ifdef CORE_TRUNCATION_DETECTION 
/* Function:	void check_core_truncation (void)
 * Description:	Checks if a core is truncated.  On IA and PA64, the kernel dumps each
 *		loaded memory segment (data, mmf, stack, etc.) into separate
 *		program segments in an ELF file.  This routine checks if any
 *		program segment's offset and size in the core file exceeds
 *		the size of the core file.  If so a warning is issued.
 * Input:	None.
 * Output:	None.
 * Globals:	core_bfd needs to be valid for this function.
 * Notes:	There is no need to detect a truncated ELF header or a truncated
 *		program header table because it is already detected for all 
 *		files.  Also, section truncation isn't detected here because 
 *		there is already functionality in gdb that will detect missing
 *		or incomplete sections.  
 *		JAGaf03652.	- Bharath, 6 Feb 2004.
 *              Fix for JAGaf09230 for wdb.pa
 */ 
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)

void 
check_core_truncation (void)
{
  int i;
  struct stat64 st_buf; /* Use stat64 as stat cannot open file with size > 2GB */
  Elf_Internal_Phdr *phdr;

  /* Read in ELF program headers if they haven't been read in already. */

  elf_text_start_vma (core_bfd);

  /* Find out the size of the core file. */
  if (stat64 (core_bfd->filename, &st_buf) == -1)
  {
    warning ("Unable to proceed with determining if the core file is\n\
truncated because: stat() on %s failed - %s.\n\
If you have problems with debugging, it could be due to a truncated core",
	     core_bfd->filename, safe_strerror (errno));
    return;
  }

  /* If any program header's location and size go beyond the core file size
   * issue a warning about the core being truncated.
   */
  phdr = elf_tdata (core_bfd)->phdr;
  for (i = 0; i < elf_elfheader (core_bfd)->e_phnum; i++, phdr++)
  {
    if ((phdr->p_offset + phdr->p_filesz) > st_buf.st_size)
    {
      warning ("%s is truncated.\n\
      Proceeding anyway, but you may run into problems.  To avoid problems\n\
      ensure that core file is complete.  Common reasons for truncation\n\
      include system limit on core file size and lack of disk space.", 
      core_bfd->filename);
      break;
    }
  }
}

#else

/* Function:	void check_core_truncation_pa32 (void)
 * Description:	Checks if a core is truncated.  On PA32, the 
 * 		total size of each header and its corresponding 
 * 		section is calculated and compared with the size 
 * 		of the corefile. If it exceeds, then a warning is 
 *		issued.
 * Input:	None.
 * Output:	None.
 * Globals:	core_bfd needs to be valid for this function.

                This is a fix for JAGaf09230
 */

   void
   check_core_truncation_pa32 (void)
   {
      int val;
      struct stat64 st_buf; /* Use stat64 as stat cannot open file with size > 2GB */
      struct corehead chdr;
      long core_size = 0;
   
      /* Find out the size of the core file. */
      if (stat64 (core_bfd->filename, &st_buf) == -1)
      {
        error ("Unable to stat() %s.  Reason: %s.\n", 
               core_bfd->filename, safe_strerror (errno));
        return;
      }
      if (bfd_seek (core_bfd, 0, SEEK_SET) != 0)
        error ("Error in bfd_seek:  \n", safe_strerror (errno));
      while (1)
       {
         val = bfd_read ((void *) &chdr, 1, sizeof (struct corehead), core_bfd);
         if (val <= 0)
            break;
         core_size += sizeof (struct corehead) + chdr.len;
         bfd_seek (core_bfd, chdr.len, SEEK_CUR);
       }
         if (core_size > st_buf.st_size)
              {
                 warning ("%s is truncated.\n\nProceeding anyway, but you may run into problems. \nTo avoid problems ensure that core file is not truncated. \nCommon reasons for truncation include system limit on core file size\nand lack of disk space.\n", core_bfd->filename);
              }
       }
     #endif
#endif  /* CORE_TRUNCATION_DETECTION */
