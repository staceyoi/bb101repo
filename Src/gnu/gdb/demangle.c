/* Basic C++ mangling and demangling support for GDB.
   Copyright 1991, 1992, 1996, 1999 Free Software Foundation, Inc.
   Written by Fred Fish at Cygnus Support.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


/*  This file contains support code for C++ demangling that is common
   to a styles of demangling, and GDB specific. */

#include <ctype.h>
#include "defs.h"
#include "command.h"
#include "gdbcmd.h"
#include "demangle.h"
#include "gdb_string.h"
#include "symtab.h"
#include "symfile.h"

/* Select the default C++ demangling style to use.  The default is "auto",
   which allows gdb to attempt to pick an appropriate demangling style for
   the executable it has loaded.  It can be set to a specific style ("gnu",
   "lucid", "arm", "hp", etc.) in which case gdb will never attempt to do auto
   selection of the style unless you do an explicit "set demangle auto".
   To select one of these as the default, set DEFAULT_DEMANGLING_STYLE in
   the appropriate target configuration file. */

#ifndef DEFAULT_DEMANGLING_STYLE
#define DEFAULT_DEMANGLING_STYLE AUTO_DEMANGLING_STYLE_STRING
#endif

extern void _initialize_demangler (void);

/* String name for the current demangling style.  Set by the
   "set demangle-style" command, printed as part of the output by the
   "show demangle-style" command. */

static char *current_demangling_style_string;

/* List of supported demangling styles.  Contains the name of the style as
   seen by the user, and the enum value that corresponds to that style. */

static const struct demangler
  {
    char *demangling_style_name;
    enum demangling_styles demangling_style;
    char *demangling_style_doc;
  }
demanglers[] =
{
  {
    AUTO_DEMANGLING_STYLE_STRING,
      auto_demangling,
      "Automatic selection based on executable"
  }
  ,
  {
    GNU_DEMANGLING_STYLE_STRING,
      gnu_demangling,
      "GNU (g++) style demangling"
  }
  ,
  {
    LUCID_DEMANGLING_STYLE_STRING,
      lucid_demangling,
      "Lucid (lcc) style demangling"
  }
  ,
  {
    ARM_DEMANGLING_STYLE_STRING,
      arm_demangling,
      "ARM style demangling"
  }
  ,
  {
    HP_DEMANGLING_STYLE_STRING,
      hp_demangling,
      "HP (aCC) style demangling"
  }
  ,
  {
    EDG_DEMANGLING_STYLE_STRING,
      edg_demangling,
      "EDG style demangling"
  }
  ,
  {
    GNU_NEW_ABI_DEMANGLING_STYLE_STRING,
    gnu_new_abi_demangling,
    "GNU (g++) new-ABI-style demangling"
  }
  ,
  {
    NULL, unknown_demangling, NULL
  }
};

static void set_demangling_command (char *, int, struct cmd_list_element *);

/* Set current demangling style.  Called by the "set demangle-style"
   command after it has updated the current_demangling_style_string to
   match what the user has entered.

   If the user has entered a string that matches a known demangling style
   name in the demanglers[] array then just leave the string alone and update
   the current_demangling_style enum value to match.

   If the user has entered a string that doesn't match, including an empty
   string, then print a list of the currently known styles and restore
   the current_demangling_style_string to match the current_demangling_style
   enum value.

   Note:  Assumes that current_demangling_style_string always points to
   a malloc'd string, even if it is a null-string. */

static void
set_demangling_command (char *ignore, int from_tty, struct cmd_list_element *c)
{
  const struct demangler *dem;

  /*  First just try to match whatever style name the user supplied with
     one of the known ones.  Don't bother special casing for an empty
     name, we just treat it as any other style name that doesn't match.
     If we match, update the current demangling style enum. */

  for (dem = demanglers; dem->demangling_style_name != NULL; dem++)
    {
      if (STREQ (current_demangling_style_string,
		 dem->demangling_style_name))
	{
	  current_demangling_style = dem->demangling_style;
	  break;
	}
    }

  /* Check to see if we found a match.  If not, gripe about any non-empty
     style name and supply a list of valid ones.  FIXME:  This should
     probably be done with some sort of completion and with help. */

  if (dem->demangling_style_name == NULL)
    {
      if (*current_demangling_style_string != '\0')
	{
	  printf_unfiltered ("Unknown demangling style `%s'.\n",
			     current_demangling_style_string);
	}
      printf_unfiltered ("The currently understood settings are:\n\n");
      for (dem = demanglers; dem->demangling_style_name != NULL; dem++)
	{
	  printf_unfiltered ("%-10s %s\n", dem->demangling_style_name,
			     dem->demangling_style_doc);
	  if (dem->demangling_style == current_demangling_style)
	    {
	      free (current_demangling_style_string);
	      current_demangling_style_string =
		savestring (dem->demangling_style_name,
			    (int)strlen (dem->demangling_style_name));
	    }
	}
      if (current_demangling_style == unknown_demangling)
	{
	  /* This can happen during initialization if gdb is compiled with
	     a DEMANGLING_STYLE value that is unknown, so pick the first
	     one as the default. */
	  current_demangling_style = demanglers[0].demangling_style;
	  current_demangling_style_string =
	    savestring (demanglers[0].demangling_style_name,
			(int) strlen (demanglers[0].demangling_style_name));
	  warning ("`%s' style demangling chosen as the default.\n",
		   current_demangling_style_string);
	}
    }
}

/* Fake a "set demangle-style" command. */

void
set_demangling_style (char *style)
{
  if (current_demangling_style_string != NULL)
    {
      free (current_demangling_style_string);
    }
  current_demangling_style_string = savestring (style, (int) strlen (style));
  set_demangling_command ((char *) NULL, 0, (struct cmd_list_element *) NULL);
}

/* In order to allow a single demangler executable to demangle strings
   using various common values of CPLUS_MARKER, as well as any specific
   one set at compile time, we maintain a string containing all the
   commonly used ones, and check to see if the marker we are looking for
   is in that string.  CPLUS_MARKER is usually '$' on systems where the
   assembler can deal with that.  Where the assembler can't, it's usually
   '.' (but on many systems '.' is used for other things).  We put the
   current defined CPLUS_MARKER first (which defaults to '$'), followed
   by the next most common value, followed by an explicit '$' in case
   the value of CPLUS_MARKER is not '$'.

   We could avoid this if we could just get g++ to tell us what the actual
   cplus marker character is as part of the debug information, perhaps by
   ensuring that it is the character that terminates the gcc<n>_compiled
   marker symbol (FIXME). */

static char cplus_markers[] =
{CPLUS_MARKER, '.', '$', '\0'};

int
is_cplus_marker (int c)
{
  return c && strchr (cplus_markers, c) != NULL;
}

/* grep_word : search for a word in input where a word is [a-zA-Z0-9_]+
   Return pointer if found, NULL otherwise. */

static char * 
grep_word (const char * text, char * word)
{
  char * pattern;
  int word_len = (int)strlen(word);

  while ((pattern = (text ? strstr(text, word) : NULL)))
    {
      if (pattern != text && (isalnum(pattern[-1]) || pattern[-1] == '_'))
        {
          text = pattern + word_len;
          continue;
        }

      if (isalnum(pattern[word_len]) || pattern[word_len] == '_')
        {
          text = pattern + word_len;
          continue;
        }

      return pattern;
    }

  return 0;
}

static char * 
eat_leading_white_space(char *input)
{
  while(*input && isspace(*input))
    input++;

  return input;
}

static void
discard_trailing_white_space(char * signature)
{
  char * end = signature + strlen (signature) - 1;

  while (end >= signature && isspace(*end))
    end--;
  
  *(end + 1) = 0;
}

/* mangle_conversion_operators : this function should deal with user defined
   type conversion operators. Right now it doesn't do much. If the signature
   happens to be for a conversion operator function, it simply replaces the
   signature with the pattern "__op" which is the prefix used by aCC for these
   things. This would result in (in the worst case) all names with the prefix 
   "__op" being demangled by the outer layers. Hopefully this isn't a big deal.
   In particular, observe that the overloaded operators do not end up with the
   same prefix. If need be this could be revisited in the future... */

static void 
mangle_conversion_operators(char * signature)
{
    char * text;
    if (grep_word(signature, "operator"))
      {
        /* lest we confuse it with a non C++ function, look for some
           text beyond the "operator" 
        */
      
        text = eat_leading_white_space (signature + 8);
        if (text[0])
          strcpy(signature, "__op");
      }
}

static void
discard_qualifier(char * signature, char * qualifier)
{
  char * keyword;
  int qual_len = (int) strlen(qualifier);
  int i;

  while ((keyword = grep_word(signature, qualifier)))
    {
      for (i=0; i < qual_len; i++)
        keyword[i] = ' ';

      signature = keyword + qual_len;
    }
}

static void
form_mangled_prefix(char * signature)
{
  int sign_len = (int) strlen(signature);
  char * most_ambiguous_name = (char *) alloca(sign_len + 1);
  char * enclosing_scope = (char *) alloca (sign_len + 1);
  int scope_levels = 0;
  char *name, *scopename, *basename;
 
  name = scopename = basename = signature;
    
  while ((name = strstr (basename, "::")) != NULL)
    {
      scope_levels++;
      name[0] = 0;
      scopename = basename;
      basename = name + 2;
    }

  /* Nothing to do if not a C++ qualified name */
  if (scope_levels == 0)
    return;
    
  strcpy (most_ambiguous_name, basename);
  strcpy (enclosing_scope, scopename);

  if (!strcmp(most_ambiguous_name, enclosing_scope))
    strcpy(most_ambiguous_name, "__ct");
  else if (most_ambiguous_name[0] == '~')
    if (!strcmp(most_ambiguous_name + 1, enclosing_scope))
      strcpy(most_ambiguous_name, "__dt");

  strcpy(enclosing_scope, signature);
  if (scope_levels == 1)
    sprintf(signature, "%s__%ld%s", 
	    most_ambiguous_name, (long) strlen(enclosing_scope),
	    enclosing_scope);
  else 
    sprintf(signature, "%s__Q%d_%ld%s", most_ambiguous_name,
	    scope_levels,
	    (long) strlen(enclosing_scope),
	    enclosing_scope);
}


/* discard_argument_lists : delete eveything between and including open_ch
   and close_ch : useful for throwing away template arguments and function
   parameters. We don't care to analyze them as these get encoded in the 
   latter part of the signature. Before calling this function, overloaded 
   operators must be mangled. This would ensure that overloaded <, >, () etc 
   are not part of the input signature. So if we see a '<' it must begin 
   template instantiation arguments and '(' and ')' enclose parameters ... 
*/
   
static void
discard_argument_lists(char * signature, int open_ch, int close_ch)
{
    int nest_level = 0;
    char * arg_begin, *arg_end;

  do {

    /* are there any argument lists ? */

    if ((arg_begin = strchr(signature, open_ch)) == 0)  /* nope */
      return;

    arg_end = arg_begin + 1;
    nest_level = 1;

    while(*arg_end && nest_level)
      {
        if (*arg_end == open_ch)
          nest_level++;
        else if (*arg_end == close_ch)
          nest_level--;
        
        arg_end++;
      }
       
    if (nest_level == 0)   /* what if otherwise ??? */
      while (*arg_begin++ = *arg_end++); /* strcpy(arg_begin, arg_end); */
    else 
      return;   /* garbage in, garbage out */

  } while (1);
}

/* operators ordered carefully for greedy scan, new/delete treated specially */

static struct op_info {
  char * plain_op;
  char * mangled_op;
} mangled_operators[] = {

  "+=",    "__apl",     "++",           "__pp",     "+",        "__pl", 
  "-=",    "__ami",     "--",           "__mm",     "->*",      "__rm",
  "->",    "__rf",      "-",            "__mi",     "*=",       "__amu", 
  "*",     "__ml",      "/=",           "__adv",    "/",        "__dv",
  "%=",    "__amd",     "%",            "__md",     "^=",       "__aer",
  "^",     "__er",      "&=",           "__aad",    "&&",       "__aa",
  "&",     "__ad",      "|=",           "__aor",    "||",       "__oo",
  "|",     "__or",      "<<=",          "__als",    "<<",       "__ls",
  "<=",    "__le",      "<",            "__lt",     ">>=",      "__ars",
  ">>",    "__rs",      ">=",           "__ge",     ">",        "__gt",
  "==",    "__eq",      "=",            "__as",     "!=",       "__ne",
  "!",     "__nt",      ",",            "__cm",     "()",       "__cl",
  "[]",    "__vc",      "~",            "__co",
};

/* srikanth, do a quick check to see if the input string *could* be a
   mangled C++ operator function name. Works only for HP ANSI C++ compiler.
   All HP aCC operator functions start with two leading underscores. The
   caller is expected to have matched these and the input here is past
   those two characters.

   We check only two additional characters here rather than the three that
   would be needed for some names. Should be good enough ...
*/

int special_cplusplus_fn(char * name)
{
  switch(name[0]) {

    case 'a' : /* aa | aad | ad  | adv | aer | als | amd | ami | 
                 amu | aor | apl | ars | as ? */
               if (name[1] == 'a' || name[1] == 'd'  
                                  || name[1] == 'e' || name[1] == 'l' 
                                  || name[1] == 'm' || name[1] == 'o'
                                  || name[1] == 'p' || name[1] == 'r'
                                  || name[1] == 's')
                 return 1;
               break;
  
    case 'c' : /*  cl | cm | co ? */
               if (name[1] == 'l' || name[1] == 'm' || name[1] == 'o')
                 return 1;
               break;


    case 'd' : /*  dl | dla | dv ? */
               if (name[1] == 'l' || name[1] == 'v')
                 return 1;
               break;

    case 'e' : /*  eq | er ? */
               if (name[1] == 'q' || name[1] == 'r')
                 return 1;
               break;

    case 'g' : /*  ge | gt  ? */
               if (name[1] == 'e' || name[1] == 't')
                 return 1;
               break;

    case 'l' : /*  le | ls | lt ? */
               if (name[1] == 'e' || name[1] == 's' || name[1] == 't')
                 return 1;
               break;
    case 'm' : /*  md | mi | ml | mm ? */
               if (name[1] == 'd' || name [1] == 'i' || 
                              name[1] == 'l' || name[1] == 'm') 
                 return 1;
               break;

    case 'n' : /*  ne | nt | nw | nwa ? */
               if (name[1] == 'e' || name[1] == 't' || name[1] == 'w')
                 return 1;
               break;

    case 'o' : /*  oo | op | or ? */
               if (name[1] == 'o' || name[1] == 'p' || name[1] == 'r')
                 return 1;
               break;

    case 'p' : /*  pl | pp ? */
               if (name[1] == 'l' || name[1] == 'p')
                 return 1;
               break;

    case 'r' : /*  rf | rm | rs ? */
               if (name[1] == 'f' || name[1] == 'm' || name[1] == 's')
                 return 1;
               break;

    case 'v' : /*  vc ? */
               if (name[1] == 'c')
                 return 1;
               break;

    default : 
              return 0;
  }

  return 0;
}

static void
mangle_overloaded_operators(const char * input, char * output)
{
  char *keyword;
  int i;
  static int total_ops = sizeof(mangled_operators) / sizeof (struct op_info);
  char * operator;

  output[0] = 0;
  while (keyword = grep_word(input, "operator"))
    {
      operator = eat_leading_white_space (keyword + 8);

      strncat(output, input, keyword - input);
      input = keyword;

      for (i=0; i < total_ops; i++)
        {
          if(!strncmp(operator, mangled_operators[i].plain_op,
                                strlen(mangled_operators[i].plain_op)))
            {
              /* catenate the mangled operator name */
              strcat(output, mangled_operators[i].mangled_op);
              input = operator + strlen(mangled_operators[i].plain_op);
              break;
            }
        }

      if (i == total_ops)   /* check for free store operators */
        {
          if (grep_word(operator, "new"))
            {
              /* reach past the "new" to see if it is "new []" */
              operator = eat_leading_white_space (operator + 3);
    
              if (operator[0] == '[' && operator[1] == ']')
                {
                  strcat(output, "__nwa");
                  input = operator + 2;
                }
              else   
                {
                  strcat(output, "__nw");
                  input = operator;
                } 
            }
          else 
          if (grep_word(operator, "delete"))
            {
              /* reach past the "delete" to see if it is "delete []" */
              operator = eat_leading_white_space (operator + 6);
    
              if (operator[0] == '[' && operator[1] == ']')
                {
                  strcat(output, "__dla");
                  input = operator + 2;
                } 
              else   
                {
                  strcat(output, "__dl");
                  input = operator;
                }
            }
            else 
              {
                strncat(output, input, operator - input);
                input = operator;
              }
        }
      }
  if (input)
    strcat(output, input);
}

/* srikanth, 990610, a quick and dirty HP ANSI C++ style name mangler. As of 
   WDB 1.1 based on gdb 4.17, we demangle all minimal symbols and all partial
   symbols in anticipation of use. This "demangle the whole universe" approach 
   involves a very high penalty in terms of startup time and memory usage. 
   Anecdotal evidence as well as empirical results suggest that we could speed 
   up startup times by as much as 30 - 40% if we turn off anticipatory 
   demangling.

   Furthermore, it stands to reason to believe that a very tiny fraction of the
   set of all symbols from the application feature in any dialog between a user
   and the debugger in a typical session. This set of symbols get "spoken" 
   either by the debugger or by the user. Regardless of the direction of the 
   communication, the channel has some very slow speed links (the human hand 
   and eyes) that limit the bandwidth.

   Turning off preparatory demangling means that we must be capable of 
   demangling names just in time. For output originating from the debugger, 
   this is straightforward. We just need to call cplus_demangle before spitting
   out the name. These demangled versions could also be cached by wdb, thereby 
   incrementally expanding its vocabulary.

   The set of symbols that originate from the user, pose a more difficult 
   problem. These may be partial in nature and may involve regular expressions.
   Rather than deal with all the vagaries of a name mangler, the approach 
   taken is a simple one : 

       o mangle the name just enough to obtain a reasonably long prefix.
       o binary search the prefix thereby zooming in on the relevant part of
         the the symbol table.
       o locate the left most symbol with the prefix of interest.
       o linear search from this point, demangling symbols that are not
         already demangled, caching the names for future use.
       o we are done when we find what we are looking for or the prefix has
         changed.

   Let us first characterize the input : The input is the name of a symbol.
   The set of symbols we are dealing with here contains functions and 
   variables. Types are irrelevant as they have no linkage and thus don't
   find their way into our search space (minimal symbol table or pxdb's quick 
   procedure lookup table.)

   Second, we are going to assume that the input is correctly formed. 
   Should this prove to be untrue (say mismatched or unbalanced brackets,
   parentheses and such much,) we would return the input as is.

   The set of all legal inputs to the mangler is identical to the set of all
   legal outputs of a demangler.
       
   Currently does not handle local classes. */

char * 
obtain_aCC_mangled_prefix(const char * input)
{
  static char * output = 0;
  static int out_len = 0;
  int in_len = (input ? (int)strlen(input) : 0);

  if (out_len == 0)
    output = xmalloc (out_len = 1024);

  /* just to be safe, make output double the size of input */
  if (out_len <= (in_len * 2 ))
    output = xrealloc (output, out_len = (in_len * 2));
    

  /* stepwise simplification of the input :

     1. Mangle overloaded operator names : The *only* legal uses of the
        symbols '<' and '>' in a C++ program are :

        (a) in operators : <, <<, <=, <<=, >, >>, >=, >>=. These uses are
            strictly in the expression grammar and hence are out of context
            here.

        (b) new style cast operators (dynamic_cast, static_cast...) : Again
            our legal input space cannot contain these uses.
              
        (c) overloaded operator functions : This is what we are mangling 
            here. 

        (d) in template argument list : we will defer processing these until
            later. (c) can be distinguished from (d) by the fact that (c) 
            will be preceded by the keyword 'operator'

   */

	mangle_overloaded_operators(input, output);

   /* 
        2. Discard template details if any. The instantiation details
           get encoded quiet later that we could get a reasonably long
           prefix without them. E.g., List<Instruction *>::firstItem(void)
           gets mangled into firstItem__4ListXTP11Instruction_Fv

           Overloaded operators *must* be mangled before attempting this, or
           else we could confuse operator<< et al with templates. 
    */

        discard_argument_lists(output, '<', '>'); /* template arguments */

   /* 3. Discard function parameters if any. These get encoded quiet later
         that we could get a reasonably long prefix without them.
   */
        discard_argument_lists(output, '(', ')'); /* function arguments */

        mangle_conversion_operators(output);

        discard_qualifier(output, "const");
        discard_qualifier(output, "volatile");
        discard_qualifier(output, "static");

        discard_trailing_white_space(output);
        form_mangled_prefix(output);
        return output;
}

int lazy_demangling = 0;

static void
set_lazy_demangling (char *args, int from_tty, struct cmd_list_element *c)
{
  /* When we get here, the variable lazy_demangling has already been
     updated by the outer layer (do_setshow_command) to refelect the
     user's wish. If it was off and turned on now, there is nothing 
     to do. If it was on and turned off now, we need to demangle the
     whole world.
  */

  if (!lazy_demangling)
    demangle_the_whole_world();
}

void
_initialize_demangler ()
{
  struct cmd_list_element *set;

  set = add_set_cmd ("demangle-style", class_support, var_string_noescape,
		     (char *) &current_demangling_style_string,
		     "Set the current C++ demangling style.\n\n\
Usage:\nTo set new value:\n\tset demangle-style <STYLE>\nTo see current value:\
\n\tshow demangle-style\n\n\
Use `set demangle-style' without arguments for a list of demangling styles.",
		     &setlist);
  (void) add_show_from_set (set, &showlist);
  set->function.sfunc = set_demangling_command;


  /* srikanth, add a new set/show command to toggle lazy-demangling mode.
     By default it is on for WDB. This would be needed for those customers
     who use HP's gdb to debug g++ or HP C Front C++ applications. */

   set = add_set_cmd ("lazy-demangling", class_support, var_boolean,
                      (char *) &lazy_demangling,
                      "Set just in time demangling mode (applicable only to WDB)\n\n\
Usage:\nTo set new value:\n\tset lazy-demangling [on | off]\nTo see current value:\
\n\tshow lazy-demangling\n", &setlist);

   (void) add_show_from_set (set, &showlist);
   set -> function.sfunc = set_lazy_demangling; 

#ifdef LAZY_DEMANGLING
   lazy_demangling = 1;
#endif

  /* Set the default demangling style chosen at compilation time. */
  set_demangling_style (DEFAULT_DEMANGLING_STYLE);
  set_cplus_marker_for_demangling (CPLUS_MARKER);
}
