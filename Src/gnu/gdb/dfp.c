/* Implementation of Decimal floating point data type support in GDB.
   Copyright 1986, 1987, 1989-1996, 1999-2000 Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "dfp.h"

boolean dfp_avail = false;

/* API's to libm DFP functions. */
struct libm_wrapper {
  char * name;
  void * func_ptr;
} dfp_wrapper[] =
{
        /* String to decimal conversion functions. */
	{"__strtod32", (void *) &dfp_strto_d32},
	{"__strtod64", (void *) &dfp_strto_d64},
	{"__strtod128", (void *) &dfp_strto_d128},
        /* Decimal to string conversion functions. */
	{"__snprintdec32", (void *) &dfp_d32to_str},
	{"__snprintdec64", (void *) &dfp_d64to_str},
	{"__snprintdec128", (void *) &dfp_d128to_str},
        /* DFP Negation functions. */
	{"_DEC_Fnegd32", (void *) &dfp_neg_d32},
	{"_DEC_Fnegd64", (void *) &dfp_neg_d64},
	{"_DEC_Fnegd128", (void *) &dfp_neg_d128},
        /* DFP conversion functions for decimal32. */
	{"_DEC_Fcnvxd_dbl_to_sgl", (void *) &int64_to_d32},
	{"_DEC_Fcnvxd_sgl_to_sgl", (void *) &int32_to_d32},
	{"_DEC_Fcnvxud_dbl_to_sgl", (void *) &uint64_to_d32},
	{"_DEC_Fcnvxud_sgl_to_sgl", (void *) &uint32_to_d32},
	{"_DEC_Fcnvdxt_sgl_to_dbl", (void *) &d32_to_int64},
	{"_DEC_Fcnvdxt_sgl_to_sgl", (void *) &d32_to_int32},
	{"_DEC_Fcnvdxut_sgl_to_dbl", (void *) &d32_to_uint64},
	{"_DEC_Fcnvdxut_sgl_to_sgl", (void *) &d32_to_uint32},
	{"_DEC_Fcnvfd_sgl_to_sgl", (void *) &flt_to_d32},
	{"_DEC_Fcnvfd_dbl_to_sgl", (void *) &dbl_to_d32},
	{"_DEC_Fcnvfd_quad_to_sgl", (void *) &quad_to_d32},
	{"_DEC_Fcnvdf_sgl_to_sgl", (void *) &d32_to_flt},
	{"_DEC_Fcnvdf_sgl_to_dbl", (void *) &d32_to_dbl},
	{"_DEC_Fcnvdf_sgl_to_quad", (void *) &d32_to_quad},
        /* DFP conversion functions for decimal64. */
	{"_DEC_Fcnvxd_dbl_to_dbl", (void *) &int64_to_d64},
	{"_DEC_Fcnvxd_sgl_to_dbl", (void *) &int32_to_d64},
	{"_DEC_Fcnvxud_dbl_to_dbl", (void *) &uint64_to_d64},
	{"_DEC_Fcnvxud_sgl_to_dbl", (void *) &uint32_to_d64},
	{"_DEC_Fcnvdxt_dbl_to_dbl", (void *) &d64_to_int64},
	{"_DEC_Fcnvdxt_dbl_to_sgl", (void *) &d64_to_int32},
	{"_DEC_Fcnvdxut_dbl_to_dbl", (void *) &d64_to_uint64},
	{"_DEC_Fcnvdxut_dbl_to_sgl", (void *) &d64_to_uint32},
	{"_DEC_Fcnvfd_sgl_to_dbl", (void *) &flt_to_d64},
	{"_DEC_Fcnvfd_dbl_to_dbl", (void *) &dbl_to_d64},
	{"_DEC_Fcnvfd_quad_to_dbl", (void *) &quad_to_d64},
	{"_DEC_Fcnvdf_dbl_to_sgl", (void *) &d64_to_flt},
	{"_DEC_Fcnvdf_dbl_to_dbl", (void *) &d64_to_dbl},
	{"_DEC_Fcnvdf_dbl_to_quad", (void *) &d64_to_quad},
        /* DFP conversion functions for decimal128. */
	{"_DEC_Fcnvxd_dbl_to_quad", (void *) &int64_to_d128},
	{"_DEC_Fcnvxd_sgl_to_quad", (void *) &int32_to_d128},
	{"_DEC_Fcnvxud_dbl_to_quad", (void *) &uint64_to_d128},
	{"_DEC_Fcnvxud_sgl_to_quad", (void *) &uint32_to_d128},
	{"_DEC_Fcnvdxt_quad_to_dbl", (void *) &d128_to_int64},
	{"_DEC_Fcnvdxt_quad_to_sgl", (void *) &d128_to_int32},
	{"_DEC_Fcnvdxut_quad_to_dbl", (void *) &d128_to_uint64},
	{"_DEC_Fcnvdxut_quad_to_sgl", (void *) &d128_to_uint32},
	{"_DEC_Fcnvfd_sgl_to_quad", (void *) &flt_to_d128},
	{"_DEC_Fcnvfd_dbl_to_quad", (void *) &dbl_to_d128},
	{"_DEC_Fcnvfd_quad_to_quad", (void *) &quad_to_d128},
	{"_DEC_Fcnvdf_quad_to_sgl", (void *) &d128_to_flt},
	{"_DEC_Fcnvdf_quad_to_dbl", (void *) &d128_to_dbl},
	{"_DEC_Fcnvdf_quad_to_quad", (void *) &d128_to_quad},
#ifdef TARGET_FLOAT80_BIT
        /* DFP conversion functions for float80. */
	{"_DEC_Fcnvfd_f80_to_sgl", (void *) &f80_to_d32},
	{"_DEC_Fcnvdf_sgl_to_f80", (void *) &d32_to_f80},
	{"_DEC_Fcnvfd_f80_to_dbl", (void *) &f80_to_d64},
	{"_DEC_Fcnvdf_dbl_to_f80", (void *) &d64_to_f80},
	{"_DEC_Fcnvfd_f80_to_quad", (void *) &f80_to_d128},
	{"_DEC_Fcnvdf_quad_to_f80", (void *) &d128_to_f80},
#endif
        /* DFP conversion functions between decimal FP types. */
	{"_DEC_Fcnvdd_sgl_to_dbl", (void *) &d32_to_d64},
	{"_DEC_Fcnvdd_sgl_to_quad", (void *) &d32_to_d128},
	{"_DEC_Fcnvdd_dbl_to_sgl", (void *) &d64_to_d32},
	{"_DEC_Fcnvdd_dbl_to_quad", (void *) &d64_to_d128},
	{"_DEC_Fcnvdd_quad_to_dbl", (void *) &d128_to_d64},
	{"_DEC_Fcnvdd_quad_to_sgl", (void *) &d128_to_d32},
	/* Basic arithmetic operations. */
	{"_DEC_Faddd32", (void *) &dfp_add_d32},
	{"_DEC_Faddd64", (void *) &dfp_add_d64},
	{"_DEC_Faddd128", (void *) &dfp_add_d128},
	{"_DEC_Fsubd32", (void *) &dfp_sub_d32},
	{"_DEC_Fsubd64", (void *) &dfp_sub_d64},
	{"_DEC_Fsubd128", (void *) &dfp_sub_d128},
	{"_DEC_Fmuld32", (void *) &dfp_mul_d32},
	{"_DEC_Fmuld64", (void *) &dfp_mul_d64},
	{"_DEC_Fmuld128", (void *) &dfp_mul_d128},
	{"_DEC_Fdivd32", (void *) &dfp_div_d32},
	{"_DEC_Fdivd64", (void *) &dfp_div_d64},
	{"_DEC_Fdivd128", (void *) &dfp_div_d128},
	/* Comparision operations. */
	{"_DEC_Fcmpd32", (void *) &dfp_cmp_d32},
	{"_DEC_Fcmpd64", (void *) &dfp_cmp_d64},
	{"_DEC_Fcmpd128", (void *) &dfp_cmp_d128}

};

/* Stores the decimal value obtained from the decimal constant.
   string is the decimal constant input. *_strto_d* function 
   handler converts the input string to the DECIMAL32/64/128 value
   */ 
DECFLOAT *
store_decimal_value (DECFLOAT *dval, int len, char *string)
{
  if (!dfp_avail)
    dfp_avail = chk_dfp_avail();  
  switch (len)
    {
      case 4:
	dval->d32 = dfp_strto_d32 (string, 0);
	break;

      case 8:
	dval->d64 = dfp_strto_d64 (string, 0);
	break;

      case 16:
	dval->d128 = dfp_strto_d128 (string, 0);
	break;
    }
  return dval;
}

/* This function checks whether the libm API functions are available or not. */
boolean 
chk_dfp_avail ()
{
  struct shl_descriptor *desc;
  int index = 1;
  int shl_stat = 0;

  while (shl_get (index, &desc) != -1)
    {
      index++;
      if (strstr (desc->filename, "/libm."))
        {
          int i;
          for (i = 0; i < sizeof (dfp_wrapper)/sizeof(dfp_wrapper[0]); i++)
	    {
	      shl_stat = (shl_findsym (&desc->handle, dfp_wrapper[i].name, TYPE_PROCEDURE,
		          	       dfp_wrapper[i].func_ptr));
              if (shl_stat != 0)
                {
                  error ("Decimal Floating Point is not supported with this version of libm: %s\n",
		          desc->filename);
                  return false;
                }
            }
        }
    }
  return true;
}

/* Negates the given decimal value depending upon its decimal float type. */
DECFLOAT *
decfloat_negation (DECFLOAT *dval, int len)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));
  switch (len)
    {
      case 4:
	decval->d32 = dfp_neg_d32 (dval->d32);
	break;

      case 8:
	decval->d64 = dfp_neg_d64 (dval->d64);
	break;

      case 16:
	decval->d128 = dfp_neg_d128 (dval->d128);
	break;
    }
  free (dval);
  return decval;
}

/* Converts unsigned integer to DFP value depending upon target decimal
   float type.*/
DECFLOAT *
dfp_from_unsigned_int (ULONGEST usi_val, int len, int tlen)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));

  if (tlen * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (len == sizeof (uint32_t))
        decval->d32 = uint32_to_d32 ((uint32_t) usi_val);
      else if (len == sizeof (uint64_t))
        decval->d32 = uint64_to_d32 ((uint64_t) usi_val);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (len == sizeof (uint32_t))
        decval->d64 = uint32_to_d64 ((uint32_t) usi_val);
      else if (len == sizeof (uint64_t))
        decval->d64 = uint64_to_d64 ((uint64_t) usi_val);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (len == sizeof (uint32_t))
        decval->d128 = uint32_to_d128 ((uint32_t) usi_val);
      else if (len == sizeof (uint64_t))
        decval->d128 = uint64_to_d128 ((uint64_t) usi_val);
    }
  else
    {
      error ("Can't deal with a decimal floating point number of %d bytes.", tlen);
    }

  return decval;
}

/* Obtains decimal float value from the signed integer depending
   upon its target decimal type length. */
DECFLOAT *
dfp_from_signed_int (LONGEST si_val, int len, int tlen)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));

  if (tlen * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (len == sizeof (int32_t))
        decval->d32 = int32_to_d32 ((int32_t) si_val);
      else if (len == sizeof (int64_t))
        decval->d32 = int64_to_d32 ((int64_t) si_val);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (len == sizeof (int32_t))
        decval->d64 = int32_to_d64 ((int32_t) si_val);
      else if (len == sizeof (int64_t))
        decval->d64 = int64_to_d64 ((int64_t) si_val);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (len == sizeof (int32_t))
        decval->d128 = int32_to_d128 ((int32_t) si_val);
      else if (len == sizeof (int64_t))
        decval->d128 = int64_to_d128 ((int64_t) si_val);
    }
  else
    {
      error ("Can't deal with a decimal floating point number of %d bytes.", tlen);
    }

  return decval;
}

/* Obtain decimal float value from the binary floating point depending
   upon its length and target length. */
DECFLOAT *
dfp_from_bfp (DOUBLEST dval, int len, int tlen)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));

  if (tlen * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (len == sizeof (float))
        decval->d32 = flt_to_d32 ((float) dval);
      else if (len == sizeof (double))
        decval->d32 = dbl_to_d32 ((double) dval);
      else if (len == sizeof (long double))
	decval->d32 = quad_to_d32 ((long double) dval);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (len == sizeof (float))
        decval->d64 = flt_to_d64 ((float) dval);
      else if (len == sizeof (double))
        decval->d64 = dbl_to_d64 ((double) dval);
      else if (len == sizeof (long double))
	decval->d64 = quad_to_d64 ((long double) dval);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (len == sizeof (float))
        decval->d128 = flt_to_d128 ((float) dval);
      else if (len == sizeof (double))
        decval->d128 = dbl_to_d128 ((double) dval);
      else if (len == sizeof (long double))
	decval->d128 = quad_to_d128 ((long double) dval);
    }
  else
    {
      error ("Can't deal with a decimal floating point number of %d bytes.", tlen);
    }

  return decval;
}

#ifdef TARGET_FLOAT80_BIT
/* Obtains decimal float value of target length tlen, from the
   float80 value. */
DECFLOAT *
dfp_from_f80 (__float80 fval, int tlen)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));

  if (tlen * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    decval->d32 = f80_to_d32 (fval);
  else if (tlen * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    decval->d64 = f80_to_d64 (fval);
  else if (tlen * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    decval->d128 = f80_to_d128 (fval);
  else
    {
      error ("Can't deal with a decimal floating point number of %d bytes.", tlen);
    }

  return decval;
}

/* Returns float80 value from the decimal float value. */
__float80
f80_from_dfp (DECFLOAT *dval, int len)
{
  __float80 f80val = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();
  
  if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    f80val = d32_to_f80 (dval->d32);
  else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    f80val = d64_to_f80 (dval->d64);
  else if (len *  TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    f80val = d128_to_f80 (dval->d128);
  return f80val;

}
#endif

/* Return decimal floating point value depending upon its target length. */
DECFLOAT *
dfp_from_dfp (DECFLOAT *dval, int len, int tlen)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  if (len == tlen || tlen == 0)
    return dval;

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));

  if (tlen * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
        decval->d32 = d64_to_d32 (dval->d64);
      else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
        decval->d32 = d128_to_d32 (dval->d128);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
        decval->d64 = d32_to_d64 (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
        decval->d64 = d128_to_d64 (dval->d128);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
        decval->d128 = d32_to_d128 (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
        decval->d128 = d64_to_d128 (dval->d64);
    }
  else
    {
      error ("Can't deal with a decimal floating point number of %d bytes.", tlen);
    }

  return decval;
}

/* Returns unsigned int value from the decimal floating value, depending
   upon its length and target length. */
ULONGEST
unsigned_int_from_dfp (DECFLOAT *dval, int len, int tlen)
{
  ULONGEST ulval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  if (tlen == sizeof (uint64_t))
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
        ulval = d32_to_uint64 (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
	ulval = d64_to_uint64 (dval->d64);
      else if (len *  TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	ulval = d128_to_uint64 (dval->d128);
    }
  else if (tlen == sizeof (uint32_t))
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
        ulval = d32_to_uint32 (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
	ulval = d64_to_uint32 (dval->d64);
      else if (len *  TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	ulval = d128_to_uint32 (dval->d128);
    }
  else
    {
      error ("Can't convert unsigned integer of size %d bytes to decimal float.", tlen);
    }

  return ulval;
}

/* Returns signed integer value from the decimal floating point value,
   depending upon its length and target length. */
LONGEST
signed_int_from_dfp (DECFLOAT *dval, int len, int tlen)
{
  LONGEST lval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  if (tlen == sizeof (int64_t))
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
        lval = d32_to_int64 (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
	lval = d64_to_int64 (dval->d64);
      else if (len *  TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	lval = d128_to_int64 (dval->d128);
    }
  else if (tlen == sizeof (int32_t))
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
        lval = d32_to_int32 (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
	lval = d64_to_int32 (dval->d64);
      else if (len *  TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	lval = d128_to_int32 (dval->d128);
    }
  else
    {
      error ("Can't convert signed integer of size %d bytes to decimal float.", tlen);
    }

  return lval;
}

/* Returns binary floating point value from decimal floating point value
   depending upon its length and target length. */
DOUBLEST
bfp_from_dfp (DECFLOAT *dval, int len, int tlen)
{
  DOUBLEST bval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  if (tlen * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
	bval = d32_to_flt (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
        bval = d64_to_flt (dval->d64);
      else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	bval = d128_to_flt (dval->d128);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
	bval = d32_to_dbl (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
        bval = d64_to_dbl (dval->d64);
      else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	bval = d128_to_dbl (dval->d128);
    }
  else if (tlen * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
	bval = d32_to_quad (dval->d32);
      else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
        bval = d64_to_quad (dval->d64);
      else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
	bval = d128_to_quad (dval->d128);
    }
  else
    {
      error ("Can't convert decimal floating point number to binary floating point number.");
    }

  return bval;
}

/* Add decimal floating point values depending upon its type. */
DECFLOAT *
decfloat_add (DECFLOAT *dval1, DECFLOAT *dval2, int len)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));
  switch (len)
    {
      case 4:
	decval->d32 = dfp_add_d32 (dval1->d32, dval2->d32);
	break;

      case 8:
	decval->d64 = dfp_add_d64 (dval1->d64, dval2->d64);
	break;

      case 16:
	decval->d128 = dfp_add_d128 (dval1->d128, dval2->d128);
	break;

      default:
	error ("Can't perform addition for the given decimal floating point type.");
		 
    }
  return decval;
}

/* Subtract decimal floating point values depending upon its type. */
DECFLOAT *
decfloat_sub (DECFLOAT *dval1, DECFLOAT *dval2, int len)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));
  switch (len)
    {
      case 4:
	decval->d32 = dfp_sub_d32 (dval1->d32, dval2->d32);
	break;

      case 8:
	decval->d64 = dfp_sub_d64 (dval1->d64, dval2->d64);
	break;

      case 16:
	decval->d128 = dfp_sub_d128 (dval1->d128, dval2->d128);
	break;

      default:
	error ("Can't perform subtraction for the given decimal floating point type.");
    }
  return decval;
}

/* Multiply decimal floating point values depending upon its type. */
DECFLOAT *
decfloat_mul (DECFLOAT *dval1, DECFLOAT *dval2, int len)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));
  switch (len)
    {
      case 4:
	decval->d32 = dfp_mul_d32 (dval1->d32, dval2->d32);
	break;

      case 8:
	decval->d64 = dfp_mul_d64 (dval1->d64, dval2->d64);
	break;

      case 16:
	decval->d128 = dfp_mul_d128 (dval1->d128, dval2->d128);
	break;

      default:
	error ("Can't perform multiplication for the given decimal floating point type.");
    }
  return decval;
}

/* Divide decimal floating point values depending upon its type. */
DECFLOAT *
decfloat_div (DECFLOAT *dval1, DECFLOAT *dval2, int len)
{
  DECFLOAT *decval = 0;

  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  decval = (DECFLOAT *) xmalloc (sizeof (DECFLOAT));
  switch (len)
    {
      case 4:
	decval->d32 = dfp_div_d32 (dval1->d32, dval2->d32);
	break;

      case 8:
	decval->d64 = dfp_div_d64 (dval1->d64, dval2->d64);
	break;

      case 16:
	decval->d128 = dfp_div_d128 (dval1->d128, dval2->d128);
	break;

      default:
	error ("Can't perform division for the given decimal floating point type.");
    }
  return decval;
}

/* Compare the decimal float values depending upon its type and comparsion operator. */
int
decfloat_cmp (DECFLOAT *dval1, DECFLOAT *dval2, int len, int cmp_op)
{
  if (!dfp_avail)
    dfp_avail = chk_dfp_avail ();

  switch (len)
    {
      case 4:
	return dfp_cmp_d32 (dval1->d32, dval2->d32, cmp_op);

      case 8:
	return dfp_cmp_d64 (dval1->d64, dval2->d64, cmp_op);

      case 16:
	return dfp_cmp_d128 (dval1->d128, dval2->d128, cmp_op);

      default:
	error ("Can't perform comparision for the given decimal floating point type.");
    }
  return 0;
}

/* Check validity of the decimal float constant.
   This function is called only for decimal constants.
   This function checks whether the given decimal constant is correct. */
boolean
valid_decimal_const (char *str)
{
  char *p, *head;
  int got_dot = 0;
  int got_e = 0;
  int valid_exp = 0;
  int len = strlen (str);

  head = p = strdup (str);
  /* Remove the last two characters in the string. Last two characters
     represent specifier of decimal float constant like 'df' or 'DF',
     (or) 'dd' or 'DD' (or) 'dl' or 'DL'. */
  p[len - 2] = '\0';
  /* Remaining string is validated. */
  while (*p != NULL) 
     {
       /* Check whether it contains dot, so that it is
          valid decimal floating point number. */
       if (!got_dot && *p == '.')
	 got_dot = 1;
       /* If it contains dot, then check whether it is has e or E for
          exponentiation. */
       else if (got_dot && !got_e && (*p == 'e' || *p == 'E'))
	 got_e = 1;
       /* Check whether it is valid exponentiation number.
          It should contain either '+' sign or '-' sign 
          followed by numbers an dnot alphabets. */
       else if (got_e && (p[-1] == 'e' || p[-1] == 'E') &&
		(*p == '-' || *p == '+' || (*p>= '0' &&
                 *p <= '9')) && !valid_exp)
	 valid_exp = 1;
       else if (*p >= '0' && *p <= '9')
	 {
	   p++;
           continue;
         }
       /* If it contains extra alphabets other than format
          specifiers at end, then it is invalid. */
       else if ((*p >= 'a' || *p <= 'z') &&
		(*p >= 'A' || *p <= 'Z') &&
                !(*p == 'e' || *p == 'E'))
	 {
	   free (head);
	   return false;
	 }
       p++;
     }
  /* If we have got 'e' or 'E' letter, but not valid exponentiation
     then return false. */
  if (got_e && !valid_exp)
    {
      free (head);
      return false;
    }
  free (head);
  return true;
}		
