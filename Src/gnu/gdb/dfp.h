/* Decimal floating point data type support header file for GDB.
   Copyright 1986, 1987, 1989-1996, 1999-2000 Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include <inttypes.h>
#include <dl.h>
#include <strings.h>

/* Internal representation for _Decimal32 types. */
#ifndef DECIMAL32
#define DECIMAL32 uint32_t
#endif

/* Internal representation for _Decimal64 types. */
#ifndef DECIMAL64
#define DECIMAL64 uint64_t
#endif

/* Internal representation for _Decimal128 types. */
#ifndef DECIMAL128
#ifndef UINT128
typedef struct {
#if defined (__hpux)
	uint64_t hi;
	uint64_t lo;
#else
	uint64_t lo;
	uint64_t hi;
#endif
}
UINT128;
#define DECIMAL128 UINT128
#endif
#endif
/* Macros for comparision from dec_arith.h file. */
#define DEC_EQ	4	/* For equality */
#define DEC_LT	8	/* For Less Than */
#define DEC_GT	16	/* For Greater Than */
#define DEC_INV	1	/* For Inversion */

#define DEC_LESS_THAN	DEC_LT | DEC_INV	/* for < use */
#define DEC_LT_EQ	DEC_LT | DEC_EQ | DEC_INV	/* for <= use */
#define DEC_GRE_THAN	DEC_GT | DEC_INV	/* for > use */
#define DEC_GRE_EQ	DEC_GT | DEC_EQ | DEC_INV	/* for >= */

/* Internal storage  for decimal floating point value of all DFP length. */
union decval {
  DECIMAL32 d32;
  DECIMAL64 d64;
  DECIMAL128 d128;
};
typedef union decval DECFLOAT;

/* Pointer for all API's DFP functions. */
/* Pointer function for string conversion functions. */
DECIMAL32 (*dfp_strto_d32) (const char * , char **);
DECIMAL64 (*dfp_strto_d64) (const char * , char **);
DECIMAL128 (*dfp_strto_d128) (const char * , char **);

int (*dfp_d32to_str) (char *, size_t, DECIMAL32);
int (*dfp_d64to_str) (char *, size_t, DECIMAL64);
int (*dfp_d128to_str) (char *, size_t, DECIMAL128);

/* Pointer function for negation functions. */
DECIMAL32 (*dfp_neg_d32) (DECIMAL32);
DECIMAL64 (*dfp_neg_d64) (DECIMAL64);
DECIMAL128 (*dfp_neg_d128) (DECIMAL128);

/* Pointer functions for Arithmetic Operations functions. */
DECIMAL32 (*dfp_add_d32) (DECIMAL32, DECIMAL32);   // add
DECIMAL32 (*dfp_sub_d32) (DECIMAL32, DECIMAL32);   // subtract arg1 - arg2
DECIMAL32 (*dfp_mul_d32) (DECIMAL32, DECIMAL32);   // multiply
DECIMAL32 (*dfp_div_d32) (DECIMAL32, DECIMAL32);   // divide arg1 / arg2

DECIMAL64 (*dfp_add_d64) (DECIMAL64, DECIMAL64);   // add
DECIMAL64 (*dfp_sub_d64) (DECIMAL64, DECIMAL64);   // subtract arg1 - arg2
DECIMAL64 (*dfp_mul_d64) (DECIMAL64, DECIMAL64);   // multiply
DECIMAL64 (*dfp_div_d64) (DECIMAL64, DECIMAL64);   // divide arg1 / arg2

DECIMAL128 (*dfp_add_d128) (DECIMAL128, DECIMAL128); // add
DECIMAL128 (*dfp_sub_d128) (DECIMAL128, DECIMAL128); // subtract arg1 - arg2
DECIMAL128 (*dfp_mul_d128) (DECIMAL128, DECIMAL128); // multiply
DECIMAL128 (*dfp_div_d128) (DECIMAL128, DECIMAL128); // divide arg1 / arg2

/* Pointer functions for Comparision functions. */
int (*dfp_cmp_d32) (DECIMAL32, DECIMAL32, int);
int (*dfp_cmp_d64) (DECIMAL64, DECIMAL64, int);
int (*dfp_cmp_d128) (DECIMAL128, DECIMAL128, int);

/* Pointer functions for Conversions functions. */
DECIMAL32 (*int64_to_d32) (int64_t);      // from int64
DECIMAL32 (*int32_to_d32) (int32_t);      // from int32
DECIMAL32 (*uint64_to_d32) (uint64_t);    // from uint64
DECIMAL32 (*uint32_to_d32) (uint32_t);    // from uint32
int64_t (*d32_to_int64) (DECIMAL32);     // to int64
int32_t (*d32_to_int32) (DECIMAL32);     // to int32
uint64_t (*d32_to_uint64) (DECIMAL32);   // to uint64
uint32_t (*d32_to_uint32) (DECIMAL32);   // to uint32
DECIMAL32 (*flt_to_d32) (float);        // from float
DECIMAL32 (*dbl_to_d32) (double);       // from double
DECIMAL32 (*quad_to_d32) (long double);  // from __float128
float (*d32_to_flt) (DECIMAL32);        // to float
double (*d32_to_dbl) (DECIMAL32);       // to double
long double (*d32_to_quad) (DECIMAL32);  // to __float128

DECIMAL64 (*int64_to_d64) (int64_t);      // from int64
DECIMAL64 (*int32_to_d64) (int32_t);      // from int32
DECIMAL64 (*uint64_to_d64) (uint64_t);    // from uint64
DECIMAL64 (*uint32_to_d64) (uint32_t);    // from uint32
int64_t (*d64_to_int64) (DECIMAL64);     // to int64
int32_t (*d64_to_int32) (DECIMAL64);     // to int32
uint64_t (*d64_to_uint64) (DECIMAL64);   // to uint64
uint32_t (*d64_to_uint32) (DECIMAL64);   // to uint32
DECIMAL64 (*flt_to_d64) (float);        // from float
DECIMAL64 (*dbl_to_d64) (double);       // from double
DECIMAL64 (*quad_to_d64) (long double);  // from __float128
float (*d64_to_flt) (DECIMAL64);        // to float
double (*d64_to_dbl) (DECIMAL64);       // to double
long double (*d64_to_quad) (DECIMAL64);  // to __float128

DECIMAL128 (*int64_to_d128) (int64_t);    // from int64
DECIMAL128 (*int32_to_d128) (int32_t);    // from int32
DECIMAL128 (*uint64_to_d128) (uint64_t);  // from uint64
DECIMAL128 (*uint32_to_d128) (uint32_t);  // from uint32
int64_t (*d128_to_int64) (DECIMAL128);   // to int64
int32_t (*d128_to_int32) (DECIMAL128);   // to int32
uint64_t (*d128_to_uint64) (DECIMAL128); // to uint64
uint32_t (*d128_to_uint32) (DECIMAL128); // to uint32

DECIMAL128 (*flt_to_d128) (float);      // from float
DECIMAL128 (*dbl_to_d128) (double);     // from double
DECIMAL128 (*quad_to_d128) (long double);// from __float128
float (*d128_to_flt) (DECIMAL128);      // to float
double (*d128_to_dbl) (DECIMAL128);     // to double
long double (*d128_to_quad) (DECIMAL128); // to __float128

#ifdef TARGET_FLOAT80_BIT
DECIMAL32 (*f80_to_d32) (long double);    // from __float80
long double (*d32_to_f80) (DECIMAL32);    // to __float80
DECIMAL64 (*f80_to_d64) (long double);    // from __float80
long double (*d64_to_f80) (DECIMAL64);    // to __float80
DECIMAL128 (*f80_to_d128) (long double);    // from __float80
long double (*d128_to_f80) (DECIMAL128);    // to __float80
#endif

DECIMAL64 (*d32_to_d64) (DECIMAL32);    // decimal32 to decimal64
DECIMAL128 (*d32_to_d128) (DECIMAL32);  // decimal32 to decimal128
DECIMAL32 (*d64_to_d32) (DECIMAL64);    // decimal64 to decimal32
DECIMAL128 (*d64_to_d128) (DECIMAL64);  // decimal64 to decimal128
DECIMAL64 (*d128_to_d32) (DECIMAL128);  // decimal128 to decimal32
DECIMAL32 (*d128_to_d64) (DECIMAL128);  // decimal128 to decimal64


extern DECFLOAT *store_decimal_value (DECFLOAT *, int, char *);

extern boolean chk_dfp_avail ();

extern DECFLOAT *decfloat_negation (DECFLOAT *, int);

extern DECFLOAT *dfp_from_unsigned_int (ULONGEST, int, int);

extern DECFLOAT *dfp_from_signed_int (LONGEST, int, int);

extern DECFLOAT *dfp_from_bfp (DOUBLEST, int, int);

extern DECFLOAT *dfp_from_dfp (DECFLOAT *, int, int);

extern ULONGEST unsigned_int_from_dfp (DECFLOAT *, int, int);
 
extern LONGEST signed_int_from_dfp (DECFLOAT *, int, int);

extern DOUBLEST bfp_from_dfp (DECFLOAT *, int, int);

#ifdef TARGET_FLOAT80_BIT
extern __float80 f80_from_dfp (DECFLOAT *, int);

extern DECFLOAT *dfp_from_f80 (__float80, int);
#endif

extern DECFLOAT *decfloat_div (DECFLOAT *, DECFLOAT *, int);

extern DECFLOAT *decfloat_mul (DECFLOAT *, DECFLOAT *, int);

extern DECFLOAT *decfloat_sub (DECFLOAT *, DECFLOAT *, int);

extern DECFLOAT *decfloat_add (DECFLOAT *, DECFLOAT *, int);

extern int decfloat_cmp (DECFLOAT *, DECFLOAT *, int, int);

extern boolean valid_decimal_const (char *);
