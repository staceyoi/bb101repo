/* Debug malloc stuff 
 * ------------------
 *
 * This header file is provided by libc for heap arena feature.
 */
#ifndef DM_HEADER_H
#define DM_HEADER_H


#define DEBUG_MALLOC_VER1 0x1    /* Changes with major fixes */
#define DM_ERR (-1)              /* Error return value */
#define DM_BUSY (-2)             /* Return value indicating arena is busy */
#if 0
#define DM_SUCCESS (0)           /* Success return value */
#endif
#define DM_BLOCK_OVERHEAD (header_size + arena_pad) /* Distance from allocptr
			          * to beginning of returned address from
				  * malloc()
				  */
#ifndef DM_SUCCESS
/* Parameters to get_arena_info() */
enum arena_request {
 AR_ARENA_OPEN=0,            /* open a session for obtaining memory stats */
 AR_ARENA_CLOSE,             /* Close a session and return resources */
 AR_ARENA_PARAMS,            /* Return arena params */
 AR_PROC_LEVEL,              /* Return total heap params */
 AR_ARENA_INFO,               /* Return arena info including the block list */
 AR_REAL_MALLINFO=999
};
#endif
/* Debug malloc arena pointers */
struct debug_malloc_arena_pointers {
  uint64_t arena_start;      /* Arena start for an arena */
  uint64_t arena_end;        /* Arena end for an arena */
  uint32_t arena_busy;       /* Denotes whether malloc is keeping the arena busy */
};

/* Memory allocator statistics */
struct malloc_debug_data {
  uint32_t malloc_dbg_magic; /* syncs debugger/debuggee */
  uint16_t is_32; /* Set if this process is 32 bit */
  uint16_t is_threaded; /* Set if this process is pthread enabled */
  uint64_t brkval; /* Process brk value */
  uint16_t q3_q4_private; /* Is this process q3 private? */
  uint32_t no_of_arenas; /* Number of arenas in the process */
  uint64_t expansion_factor; /* arena expansion factor in bytes */
  uint32_t is_sba_enabled; /* Set if SBA is enabled */
  uint64_t maxfast; /* Max block size in SBA */
  uint32_t numblks; /* Number of blocks a particular size */
  uint32_t grain; /* Incremental sizes stored in the SBA */
  uint32_t is_cache_enabled; /* Set if thread local cache is enabled */
  uint32_t no_of_cache_miss; /* Total number of cache misses for this process*/
  uint32_t bucket_size; /* Max power of 2 size in the cache */
  uint32_t no_of_buckets; /* Number of buckets */
  uint32_t  retirement; /* Retirement age in minutes */
  struct debug_malloc_arena_pointers base_arena; /* arena 0's data */
  struct debug_malloc_arena_pointers *ptr; /* Start chaining arena addresses */
#ifndef __LP64__
  char pad[4]; /* So that we can read a 64 bit struct from a 32 bit process */
#endif
};

/*** 
trangto:  this file came directly from libc.  
libc and gdb RTC share some common structures for info
heap arena implementation.  We initially had our own
declaration of the same things based on a spec.  Later,
libc changed but gdb did not know about it.  We had
nasty memory corruption bugs that took days to figure
out.  We could enhance the interface so we know we
are using the same structures.  libc folks are very
busy so this solution will do.  Basically when they
update there dm.h they give us a copy to drop here.
Removing this code makes it harder to merge
in the future.

static struct malloc_debug_data debug_data;
***/ 

/* Exported symbol malloc_debug - this is the starting point for any 
 * tool to latch onto malloc.
 */
struct malloc_debug_data *malloc_debug;
#ifndef DM_SUCCESS
enum heap_block_type {
  HEAP_USR_SBRK = 0, /* Block of memory allocated by calling brk/sbrk */
  HEAP_NODE_BLK,     /* Block of memory allocated for tree nodes */
  HEAP_USED_BLK,     /* Block of memory allocated by the application */
  HEAP_FREE_BLK,     /* Block of memory available for allocation */
  HEAP_HLDHEAD_BLK,  /* Block of memory allocated for SBA Holding header */
  HEAP_HLD_BLK,      /* Block of memory allocd for SBA block holding block */
  HEAP_CACHED_BLK    /* Cached block which is marked as used, actually it is free in the cache */
};
#endif
struct heap_block {
  uint64_t addr; /* virtual address less block header */
#ifndef DM_SUCCESS
  enum heap_block_type type; /* Type of block */
#else
  enum _dm_heap_block_type type;
#endif
  uint64_t size; /* Size of block */
};

/* Handle that will be passed after 'opening' an arena for getting debug
 * info.
 */
struct handle {
  uint32_t num_arenas;
  uint32_t expansion_factor;
  uint32_t maxfast;
  uint32_t grain;
  uint32_t is_threaded;
  uint32_t is_32bit;
  uint32_t malloc_header_size;
  uint64_t (**callbacks)();
  uint64_t debug_data_ptr;
  uint64_t arena_id;
  struct heap_block cur_block;
  char buf[256];
};

/* Macros to access the callback handlers */
#define lookup_addr(x) ( (unsigned long long (*) (char *)) hdl->callbacks[0] ) (x)
#define read_tgt_mem(s,d,l) ( (int (*) (unsigned long long, unsigned long long, int)) hdl->callbacks[1] ) (s,d,l)
#define isilp32() ( (int (*) (void)) hdl->callbacks[2] ) ()

/* Debug malloc service macros */
/* Used to update arena start in the malloc_debug structure */
#define DMarenastart()                                                   \
{                                                                        \
  if(arena_id)                                                           \
     debug_data.ptr[arena_id].arena_start = (unsigned long) ACarenastart;\
  else                                                                   \
     debug_data.base_arena.arena_start = (unsigned long) ACarenastart;   \
}

/* Used to update arena end in the malloc_debug structure */
#define DMarenaend()                                                 \
{                                                                    \
  if(arena_id)                                                       \
     debug_data.ptr[arena_id].arena_end = (unsigned long) ACarenaend;\
  else                                                               \
     debug_data.base_arena.arena_end = (unsigned long) ACarenaend;   \
}

/* Used to update cache miss statistics in malloc_debug structure */
#define DMcache_update()                                    \
{                                                           \
  debug_data.no_of_cache_miss += __m_tls->cache_misses;     \
}

/* In order to ensure that the debuggee's malloc data strucures
 * are in a consistent state we mark the arenas that are currently
 * being worked on. We assume that an arena's data structures are
 * busy when a mutex is taken on that arena, and the arena is in
 * a consistent state when the mutex is unlocked. Though there are
 * other approaches we choose this because of its simplicity.
 *
 * A boolean could well have described the toggle state of busy 
 * arena or not-busy arena. But since we are linking the busy
 * state with recursive mutex locks, it could happen that we mark
 * an arena not-busy in a recursive function (real_malloc) while
 * a calling function would have meant the arena to be busy. Hence
 * we go with the integer incrementing approach below.
 */
/* Used to mark an arena as being used by malloc family. */
#define DMarena_busy()                               \
{                                                    \
  if(arena_id)                                       \
    debug_data.ptr[arena_id].arena_busy += 1;        \
  else                                               \
    debug_data.base_arena.arena_busy += 1;           \
}

/* Used to mark an arena as being in a consistent state */
#define DMarena_not_busy()                           \
{                                                    \
  if(arena_id)                                       \
    debug_data.ptr[arena_id].arena_busy -= 1;        \
  else                                               \
    debug_data.base_arena.arena_busy -= 1;           \
}

struct heap_sba_param {
  int is_sba_enabled; /* flag to determine whether the SBA is enabled */
  uint64_t maxfast;  /* Value of max fast */
  uint64_t numblks;  /* No. of small blocks allocated at a time */
  uint64_t grain;    /* small blocks whose size will be rounded up to the nearest multiple of grain */
};

struct malloc_thread_cache_info {
  int is_cache_enabled; /* flag to determine whether the cache is enabled */
  uint64_t no_of_cache_miss; /* # of cache misses happened in each row */
  uint64_t bucketsize;  /* average # of pointers that can be cached */
  uint64_t buckets;     /* # of buckets in the cache */
  uint64_t retirement;  /* cache retirement age */
};

struct total_arena_info {
  uint64_t total_space;  /* Total space in arenas */
  uint64_t num_bytes_FSB; /* Number of bytes in free small blocks */
  uint64_t num_bytes_USB; /* Number of bytes in used small blocks */
  uint64_t num_bytes_FOB; /* Number of bytes in free Ordinary blocks */
  uint64_t num_bytes_UOB; /* Number of bytes in used Ordinary blocks */
  uint64_t num_bytes_HBH; /* Number of bytes in holding block headers */
  uint64_t num_small_blocks;   /* Number of small blocks */
  uint64_t num_ordinary_blocks;/* Number of ordinary blocks */
  uint64_t num_holding_blocks; /* Number of holding blocks */
  uint64_t num_free_ordinary_blocks; /* Number of free ordinary blocks */
  uint64_t num_free_small_blocks; /* Number of free small blocks */
  struct heap_sba_param SBA_params;
  struct malloc_thread_cache_info cache_info;
};

struct arena_info {
  int num_blocks; /* total number of block in the arena */
  struct heap_block* block_list; /* array of heap_blocks */
  uint64_t start_addr; /* Start address of arena */
  uint64_t end_addr; /* End address of arena */
  uint64_t total_space;  /* Total space in arenas */
  uint64_t num_bytes_FSB; /* Number of bytes in free small blocks */
  uint64_t num_bytes_USB; /* Number of bytes in used small blocks */
  uint64_t num_bytes_FOB; /* Number of bytes in free Ordinary blocks */
  uint64_t num_bytes_UOB; /* Number of bytes in used Ordinary blocks */
  uint64_t num_bytes_HBH; /* Number of bytes in holding block headers */
  uint64_t num_small_blocks;   /* Number of small blocks */
  uint64_t num_ordinary_blocks;/* Number of ordinary blocks */
  uint64_t num_holding_blocks; /* Number of holding block headers */
  uint64_t num_free_ordinary_blocks; /* Number of free ordinary blocks */
  uint64_t num_free_small_blocks; /* Number of free small blocks */
};

struct arena_params {
  int num_arenas; /* Total number of arenas in this process */
  uint64_t expansion_factor; /* Expansion factor in bytes */
};

/* Debug malloc routines - declaration */
static void setup_debug(void);
#ifndef DM_SUCCESS
int libc_get_arena_info(int, enum arena_request, void *, int, void **);
#endif
static int traverse(struct total_arena_info *, struct debug_malloc_arena_pointers *, struct handle *, struct heap_block *, int);

/* x------------------------------------------------------------------x 
 * End of debug malloc declarations and macros
 * x------------------------------------------------------------------x
 */

#endif 
