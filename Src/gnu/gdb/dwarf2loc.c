/* DWARF 2 location expression support for GDB.
   Copyright 2003 Free Software Foundation, Inc.
   Contributed by Daniel Jacobowitz, MontaVista Software, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "ui-out.h"
#include "value.h"
#include "language.h"
#include "frame.h"
#include "gdbcore.h"
#include "target.h"
#include "inferior.h"
#include "ax.h"
#include "ax-gdb.h"

#include "elf/dwarf2.h"
#include "dwarf2expr.h"
#include "dwarf2loc.h"

#include "gdb_string.h"

#ifndef TARGET_ADDR_BIT
#define TARGET_ADDR_BIT TARGET_PTR_BIT
#endif

#ifndef DWARF2_REG_TO_REGNUM
#define DWARF2_REG_TO_REGNUM(REG) (REG)
#endif

/* This is the baton used when performing dwarf2 expression
   evaluation.  */
struct dwarf_expr_baton
{
  struct frame_info *frame;
  struct objfile *objfile;
};

struct needs_frame_baton
{
  int needs_frame;
};

static struct value *
loclist_read_variable (struct symbol *symbol, struct frame_info *frame);
static int
loclist_read_needs_frame (struct symbol *symbol);
static int
loclist_describe_location (struct symbol *symbol, struct ui_file *stream);

static struct value *
locexpr_read_variable (struct symbol *symbol, struct frame_info *frame);
static int
locexpr_read_needs_frame (struct symbol *symbol);
static int
locexpr_describe_location (struct symbol *symbol, struct ui_file *stream);
static int
dwarf_describe_one_locexpr(unsigned char *data, unsigned char *end, struct ui_file *stream,
			   struct objfile *objfile, struct symbol *symbol);

/* The set of location functions used with the DWARF-2 expression
   evaluator and location lists.  */
struct location_funcs dwarf2_loclist_funcs = {
  loclist_read_variable,
  loclist_read_needs_frame,
  loclist_describe_location,
};

/* The set of location functions used with the DWARF-2 expression
   evaluator.  */
struct location_funcs dwarf2_locexpr_funcs = {
  locexpr_read_variable,
  locexpr_read_needs_frame,
  locexpr_describe_location,
};


/* A helper function for dealing with location lists.  Given a
   symbol baton (BATON) and a pc value (PC), find the appropriate
   location expression, set *LOCEXPR_LENGTH, and return a pointer
   to the beginning of the expression.  Returns NULL on failure.

   For now, only return the first matching location expression; there
   can be more than one in the list.  */

static unsigned char *
find_location_expression (struct dwarf2_loclist_baton *baton,
			  int *locexpr_length, CORE_ADDR pc)
{
  CORE_ADDR base_address = baton->base_address;
  CORE_ADDR low, high;
  unsigned char *loc_ptr, *buf_end;
  int length;
  unsigned int addr_size = TARGET_ADDR_BIT / TARGET_CHAR_BIT;

  /* Base mask: The value of the largest representable address.
     This value is 0xffffffff in the 32-bit DWARF format and
     0xffffffffffffffff in the 64-bit DWARF format. */
  CORE_ADDR base_mask = ~(~(CORE_ADDR)1 << (addr_size * 8 - 1));

  loc_ptr = (unsigned char *) baton->data;
  buf_end = (unsigned char *) baton->data + baton->size;

  while (1)
    {
      low = dwarf2_read_address (loc_ptr, buf_end, &length);
      loc_ptr += length;
      high = dwarf2_read_address (loc_ptr, buf_end, &length);
      loc_ptr += length;

      /* An end-of-list entry.  */
      if (low == 0 && high == 0)
	return NULL;

      /* A base-address-selection entry.  */
      if ((low & base_mask) == base_mask)
	{
	  base_address = high;
	  continue;
	}

      /* Otherwise, a location expression entry.  */
      low += base_address;
      high += base_address;

      /* JAGag45745: WDB 5.7 stilll has DWARF issues; Why does gdb not work
         properly with gcc.
         The low and the high addresses read in from the ".debug_loc" section
         correspond to the link time addresses. Convert them to the run time
         addresses before comparing with the pc.
         jini: 13 Aug 2007
      */ 
      if (baton->objfile->flags & OBJF_SHARED)
        {
          low -= baton->objfile->obfd->text_start_address;
          high -= baton->objfile->obfd->text_start_address;

          low += baton->objfile->text_low;
          high += baton->objfile->text_low;
        }

      length = extract_unsigned_integer ((char *) loc_ptr, 2);
      loc_ptr += 2;

      if (pc >= ADJUST_IA64_SLOT_ENCODING(low) &&
	  pc < ADJUST_IA64_SLOT_ENCODING(high))
	{
	  *locexpr_length = length;
	  return loc_ptr;
	}

      loc_ptr += length;
    }
}

/* Helper functions for dwarf2_evaluate_loc_desc.  */

/* Return contents of register REGNUM in frame FRAME as address,
   interpreted as value of type TYPE.   Will abort if register
   value is not available.  */

static CORE_ADDR
address_from_register (struct type *type, int regnum, struct frame_info *frame)
{
  value_ptr value;
  CORE_ADDR result;

  value = value_from_register (type, regnum, frame);

  result = value_as_pointer (value);
  release_value (value);
  value_free (value);

  return result;
}

/* Using the frame specified in BATON,  return the value of register
   REGNUM, treated as a pointer.  */
static CORE_ADDR
dwarf_expr_read_reg (void *baton, int dwarf_regnum)
{
  struct dwarf_expr_baton *debaton = (struct dwarf_expr_baton *) baton;
  CORE_ADDR result;
  int regnum;

  regnum = DWARF2_REG_TO_REGNUM (dwarf_regnum);
  result = address_from_register (builtin_type_ptr,
				  regnum, debaton->frame);
  return result;
}

/* Read memory at ADDR (length LEN) into BUF.  */

static void
dwarf_expr_read_mem (void *baton, char *buf, CORE_ADDR addr, size_t len)
{
  read_memory (addr, buf, len);
}

/* Using the frame specified in BATON, find the location expression
   describing the frame base.  Return a pointer to it in START and
   its length in LENGTH.  */
static void
dwarf_expr_frame_base (void *baton, unsigned char **start, size_t * length)
{
  /* FIXME: cagney/2003-03-26: This code should be using
     get_frame_base_address(), and then implement a dwarf2 specific
     this_base method.  */
  struct symbol *framefunc;
  struct dwarf_expr_baton *debaton = (struct dwarf_expr_baton *) baton;

  /* 
   * First try and look through our list of blocks to find the 
   * appropriate function.
   */

  framefunc = get_frame_function (debaton->frame);

  /*
   * If that does not work, then try looking through the symbol 
   * tables to find the function.
   */

  if (framefunc == NULL) {
    struct minimal_symbol *msymbol 
		= lookup_minimal_symbol_by_pc (debaton->frame->pc);
    if ((msymbol != NULL) &&
        (SYMBOL_VALUE_ADDRESS (msymbol) >= debaton->frame->pc ))
      framefunc = lookup_symbol(SYMBOL_NAME (msymbol), NULL,
			        VAR_NAMESPACE, NULL, NULL);

    /*
     * If we still cannot find the function, error out because 
     * something is wrong.
     */

    if (framefunc == NULL)
      if (msymbol != NULL)
        error("dwarf_expr_frame_base: Cannot find symbol for %s\n",
	      SYMBOL_NAME (msymbol));
      else
        error("dwarf_expr_frame_base: Cannot find symbol for PC=0x%ullx.\n",
	      debaton->frame->pc);
    }

  if (SYMBOL_LOCATION_FUNCS (framefunc) == &dwarf2_loclist_funcs)
    {
      struct dwarf2_loclist_baton *symbaton;
      int size;
      symbaton = SYMBOL_LOCATION_BATON (framefunc);
      *start = find_location_expression (symbaton, &size,
					 get_frame_pc (debaton->frame));
      *length = size;
    }
  else
    {
      struct dwarf2_locexpr_baton *symbaton;
      symbaton = SYMBOL_LOCATION_BATON (framefunc);

      /*
       * If we did not find the right baton in the function we are in,
       * return an error that is hopefully meaningful to the user.
       */

      if (symbaton == NULL)
	{
	  error("Could not find the variable value in \"%s\".",
		SYMBOL_NAME (framefunc));
	}
      else
	{
      	  *length = symbaton->size;
      	  *start = symbaton->data;
    	}
    }

  if (*start == NULL)
    error ("Could not find the frame base for \"%s\".",
	   SYMBOL_NAME (framefunc));
}

/* Evaluate a location description, starting at DATA and with length
   SIZE, to find the current location of variable VAR in the context
   of FRAME.  */
static struct value *
dwarf2_evaluate_loc_desc (struct symbol *var, struct frame_info *frame,
			  unsigned char *data, unsigned short size,
			  struct objfile *objfile)
{
  CORE_ADDR result;
  struct value *retval;
  struct dwarf_expr_baton baton;
  struct dwarf_expr_context *ctx;

  if (size == 0)
    {
      /* The location expression is empty. */
      retval = allocate_value (SYMBOL_TYPE (var));
      VALUE_LVAL (retval) = not_lval;
      VALUE_AVAILABILITY (retval) = VA_OPTIMIZED_OUT;
      return retval;
    }

  baton.frame = frame;
  baton.objfile = objfile;

  ctx = new_dwarf_expr_context ();
  ctx->baton = &baton;
  ctx->read_reg = dwarf_expr_read_reg;
  ctx->read_mem = dwarf_expr_read_mem;
  ctx->get_frame_base = dwarf_expr_frame_base;

  dwarf_expr_eval (ctx, data, size);
  result = dwarf_expr_fetch (ctx, 0);

  if (ctx->in_reg)
    {
      struct symbol *sym = NULL;
      int regnum = DWARF2_REG_TO_REGNUM (result);
      /* JAGag44937: 32-bit debugging fails with gdb/wdb 5.5 and later.
         and
         JAGag45745: WDB 5.7 stilll has DWARF issues; Why does gdb not work
         properly with gcc.
         Inorder to retrieve the frame base address from the register, use
         the type of the pseudo symbol created (TYPE_CODE_PTR). Before this
         fix, an incorrect type (TYPE_CODE_FUNC) was being passed which
         resulted in gdb ending up with an incorrect frame base address. 
         jini. 7 Aug 2007 */
         
      sym = (var->aux_value.loc.aux_symbol) ?
            (var->aux_value.loc.aux_symbol): var;
      retval = value_from_register (SYMBOL_TYPE (sym), regnum, frame);
    }
  else
    {
      retval = allocate_value (SYMBOL_TYPE (var));
      VALUE_BFD_SECTION (retval) = SYMBOL_BFD_SECTION (var);

      VALUE_LVAL (retval) = lval_memory;
      VALUE_LAZY (retval) = 1;
      VALUE_ADDRESS (retval) = result;

    }

  free_dwarf_expr_context (ctx);

  return retval;
}





/* Helper functions and baton for dwarf2_loc_desc_needs_frame.  */

/* Reads from registers do require a frame.  */
static CORE_ADDR
needs_frame_read_reg (void *baton, int regnum)
{
  struct needs_frame_baton *nf_baton = baton;
  nf_baton->needs_frame = 1;
  return 1;
}

/* Reads from memory do not require a frame.  */
static void
needs_frame_read_mem (void *baton, char *buf, CORE_ADDR addr, size_t len)
{
  memset (buf, 0, len);
}

/* Frame-relative accesses do require a frame.  */
static void
needs_frame_frame_base (void *baton, unsigned char **start, size_t * length)
{
  static unsigned char lit0 = DW_OP_lit0;
  struct needs_frame_baton *nf_baton = baton;

  *start = &lit0;
  *length = 1;

  nf_baton->needs_frame = 1;
}

/* Return non-zero iff the location expression at DATA (length SIZE)
   requires a frame to evaluate.  */

static int
dwarf2_loc_desc_needs_frame (unsigned char *data, unsigned short size)
{
  struct needs_frame_baton baton;
  struct dwarf_expr_context *ctx;
  int in_reg;

  baton.needs_frame = 0;

  ctx = new_dwarf_expr_context ();
  ctx->baton = &baton;
  ctx->read_reg = needs_frame_read_reg;
  ctx->read_mem = needs_frame_read_mem;
  ctx->get_frame_base = needs_frame_frame_base;

  dwarf_expr_eval (ctx, data, size);

  in_reg = ctx->in_reg;

  free_dwarf_expr_context (ctx);

  return baton.needs_frame || in_reg;
}


/* Return the value of SYMBOL in FRAME using the DWARF-2 expression
   evaluator to calculate the location.  */
static struct value *
locexpr_read_variable (struct symbol *symbol, struct frame_info *frame)
{
  struct dwarf2_locexpr_baton *dlbaton = SYMBOL_LOCATION_BATON (symbol);
  struct value *val;
  val = dwarf2_evaluate_loc_desc (symbol, frame, dlbaton->data, dlbaton->size,
				  dlbaton->objfile);

  return val;
}

/* Return non-zero iff we need a frame to evaluate SYMBOL.  */
static int
locexpr_read_needs_frame (struct symbol *symbol)
{
  struct dwarf2_locexpr_baton *dlbaton = SYMBOL_LOCATION_BATON (symbol);
  return dwarf2_loc_desc_needs_frame (dlbaton->data, dlbaton->size);
}

/*
 * Print a natural-language description of SYMBOL to STREAM.
 * This is a new implementation of locexpr_describe_location() for 
 * HP.  It is more extensive than the one provided by FSF which
 * only handles limited cases.
 */

static int
locexpr_describe_location (struct symbol *symbol, struct ui_file *stream)
{
  struct dwarf2_locexpr_baton *dlbaton = SYMBOL_LOCATION_BATON (symbol);
  unsigned char *start = dlbaton->data;
  unsigned char *end = dlbaton->data + dlbaton->size;
  
  return dwarf_describe_one_locexpr(start, end, stream, dlbaton->objfile,
				    symbol);
}

/*
 * This function takes, in as input, a pointer to raw data of a
 * location list entry, a pointer to the end of the data, and a 
 * a specification of where to print the output.
 * This function decodes the raw data in the location list entry
 * and prints out the information into a much more descriptive form.
 */

static int
dwarf_describe_one_locexpr(unsigned char *data, unsigned char *end, struct ui_file *stream,
			   struct objfile *objfile, struct symbol *symbol)
{
  unsigned char *ptr = data;
  CORE_ADDR result;
  int bytes_read;
  LONGEST offset;
  ULONGEST reg;
  int regno;
  enum dwarf_location_atom op = *ptr++;

  switch (op)
    {
    case DW_OP_addr:
      /*
       * We have a straight address, simply print out the address 
       * remembering to add in any offset if the loadfile where the
       * symbol is defined has been rebased.
       *.
       * The address is the first argument in the location description.
       */

      result = dwarf2_read_address (ptr, end, &bytes_read);
      result += ANOFFSET(objfile->section_offsets,
			 SYMBOL_SECTION(symbol));
      fprintf_filtered(stream,"at address 0x%x\n", result);
      ptr += bytes_read;
      break;

    case DW_OP_reg0:
    case DW_OP_reg1:
    case DW_OP_reg2:
    case DW_OP_reg3:
    case DW_OP_reg4:
    case DW_OP_reg5:
    case DW_OP_reg6:
    case DW_OP_reg7:
    case DW_OP_reg8:
    case DW_OP_reg9:
    case DW_OP_reg10:
    case DW_OP_reg11:
    case DW_OP_reg12:
    case DW_OP_reg13:
    case DW_OP_reg14:
    case DW_OP_reg15:
    case DW_OP_reg16:
    case DW_OP_reg17:
    case DW_OP_reg18:
    case DW_OP_reg19:
    case DW_OP_reg20:
    case DW_OP_reg21:
    case DW_OP_reg22:
    case DW_OP_reg23:
    case DW_OP_reg24:
    case DW_OP_reg25:
    case DW_OP_reg26:
    case DW_OP_reg27:
    case DW_OP_reg28:
    case DW_OP_reg29:
    case DW_OP_reg30:
    case DW_OP_reg31:

      /*
       * The value for a variable is stored in a register.
       * Print out the name of the register.  
       */

      regno = DWARF2_REG_TO_REGNUM (op - DW_OP_reg0);
      fprintf_filtered(stream,
		       "in register $%s", REGISTER_NAME(regno));
      break;
    case DW_OP_regx:

      /*
       * The value is again stored in a register, but the
       * register number is specified as the first argument in the
       * location description.
       */

      ptr = read_uleb128 (ptr, end, &reg);
      regno = DWARF2_REG_TO_REGNUM (reg);
      fprintf_filtered(stream,
		       "in register $%s", REGISTER_NAME(regno));
      break;
    case DW_OP_breg0:
    case DW_OP_breg1:
    case DW_OP_breg2:
    case DW_OP_breg3:
    case DW_OP_breg4:
    case DW_OP_breg5:
    case DW_OP_breg6:
    case DW_OP_breg7:
    case DW_OP_breg8:
    case DW_OP_breg9:
    case DW_OP_breg10:
    case DW_OP_breg11:
    case DW_OP_breg12:
    case DW_OP_breg13:
    case DW_OP_breg14:
    case DW_OP_breg15:
    case DW_OP_breg16:
    case DW_OP_breg17:
    case DW_OP_breg18:
    case DW_OP_breg19:
    case DW_OP_breg20:
    case DW_OP_breg21:
    case DW_OP_breg22:
    case DW_OP_breg23:
    case DW_OP_breg24:
    case DW_OP_breg25:
    case DW_OP_breg26:
    case DW_OP_breg27:
    case DW_OP_breg28:
    case DW_OP_breg29:
    case DW_OP_breg30:
    case DW_OP_breg31:
      /*
       * The variable is stored at an offset from base 
       * register 1 - 31.  The offset is the first argument
       * in the location description.
       */

      ptr = read_sleb128 (ptr, end, &offset);
      regno = DWARF2_REG_TO_REGNUM (op - DW_OP_breg0);
      fprintf_filtered(stream,
		       "at offset %d from register $%s",
		       offset, REGISTER_NAME(regno));
      break;
    case DW_OP_bregx:
      /*
       * The variable is again stored at an offset from a 
       * base register, but the base register is specified 
       * as the first argument in the location description.
       */

      ptr = read_uleb128 (ptr, end, &reg);
      ptr = read_sleb128 (ptr, end, &offset);
      regno = DWARF2_REG_TO_REGNUM (reg);
      fprintf_filtered(stream,
		       "at offset %d from register $%s",
		       offset, REGISTER_NAME(regno));
      break;
      
    case DW_OP_fbreg:
      /*
       * The variable is stored at an offset from the
       * frame register.  The offset is the first argument
       * in the location description.
       */

      ptr = read_sleb128 (ptr, end, &offset);
      fprintf_filtered(stream,
		       "at frame offset %d",offset);
      break;
    default:
      /*
       * The expression in the location description is far too
       * complex to decipher without creating a state machine.
       * Simply print out this meesage.
       */

      fputs_filtered ("with a complex DWARF2 location expression\n", stream);
      break;
    }
  return 1;
}


/* Wrapper functions for location lists.  These generally find
   the appropriate location expression and call something above.  */

/* Return the value of SYMBOL in FRAME using the DWARF-2 expression
   evaluator to calculate the location.  */
static struct value *
loclist_read_variable (struct symbol *symbol, struct frame_info *frame)
{
  struct dwarf2_loclist_baton *dlbaton = SYMBOL_LOCATION_BATON (symbol);
  struct value *val;
  unsigned char *data;
  int size = 0;
  CORE_ADDR pc = frame ? get_frame_pc (frame) : 0;

  data = find_location_expression (dlbaton, &size, pc);
  if (data == NULL)
  {
    /* The variable is not available at the current PC address */
    val = allocate_value (SYMBOL_TYPE (symbol));
    VALUE_AVAILABILITY(val) = VA_NOT_AVAILABLE_HERE;
    VALUE_ADDRESS(val) = pc;
    return val;
  }

  val = dwarf2_evaluate_loc_desc (symbol, frame, data, size, dlbaton->objfile);

  return val;
}

/* Return non-zero iff we need a frame to evaluate SYMBOL.  */
static int
loclist_read_needs_frame (struct symbol *symbol)
{
  /* If there's a location list, then assume we need to have a frame
     to choose the appropriate location expression.  With tracking of
     global variables this is not necessarily true, but such tracking
     is disabled in GCC at the moment until we figure out how to
     represent it.  */

  return 1;
}


/*
 * Print a natural-language description of SYMBOL to STREAM.
 * Given a symbol, this function looks up the the information stored at
 * a particular index into the .debug_loc table and prints out all of the
 * information stored at that index.  The structure of the location
 * list entry is the following:
 *
 *     start_address   end_address   size  location description
 *     start_address   end_address   size  location description
 *          .             .
 *          .             .
 *     0               0
 */

static int
loclist_describe_location (struct symbol *symbol, struct ui_file *stream)
{
  struct dwarf2_loclist_baton *baton = SYMBOL_LOCATION_BATON (symbol);
  unsigned char *ptr;
  unsigned char *end;
  int length;
  CORE_ADDR low, high;
  CORE_ADDR rebased_low, rebased_high;


  fputs_filtered ("with the following locations:\n", stream);
  fputs_filtered ("\tFrom\t\t\tTo\t\t\tLocation\n", stream);

  /* Get the start and end of the data in the location list entry */

  ptr = baton->data;
  end = baton->data + baton->size;

  while (1)
    {
      char *format;

      /* Get the low pc and high pc of the next item in the location list */

      low = dwarf2_read_address (ptr, end, &length);
      ptr += length;
      high = dwarf2_read_address (ptr, end, &length);
      ptr += length;

      /* A low pc and high pc of 0 indicates that we're
	 the end of the location list entry, so break out of the loop. */
      if (low == 0 && high == 0)
	break;

      /* 
       * Format the addresses.  If we have a 64-bit address, align 
       * addresses with one tab between the low and high addresses.
       * Otherwise, add another tab.
       */
#ifdef HP_NSK
      format = (low >> 63 != 0) ? "\t0x%llx\t0x%llx\t" :
				  "\t0x%lx\t\t0x%lx\t\t";

      /* NSK has relocatable addresses, so adjust them here. */
      low += ANOFFSET(baton->objfile->section_offsets,
				   SYMBOL_SECTION(symbol));
      high += ANOFFSET(baton->objfile->section_offsets,
				     SYMBOL_SECTION(symbol));
#else
      format = (sizeof(low) < 8 || is_swizzled) ? "\t0x%llx\t0x%llx\t" :
				  		  "\t0x%lx\t\t0x%lx\t\t";
#endif

      fprintf_filtered (stream, format, low, high);
      
      /* get the size of the location data */
      length = extract_unsigned_integer(ptr, 2);
      ptr += 2;

      /* Print out the location information */
      dwarf_describe_one_locexpr (ptr, end, stream, baton->objfile, symbol);

      /* Get to next item in the location list entry. */
      ptr += length;

      fputs_filtered ("\n", stream);
    }
  return 1;
}
