/* Dwarf2 location expression and location list support for GDB.
   Copyright 2003 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#if !defined (DWARF2LOC_H)
#define DWARF2LOC_H

/* This header is private to the DWARF-2 reader.  It is shared between
   dwarf2read.c and dwarf2loc.c.  */

/* RM: GDB and the debug infos encoding of register
 * numbers (which numbers are the GRs, which numbers are the FRs, etc)
 * differ. Adjust here.
 */
#ifdef HP_IA64
#define ADJUST_IA64_REGNUM(reg) \
  (((reg) <= FRLAST_REGNUM) ? ((reg)+GR0_REGNUM) : ((reg)-FRLAST_REGNUM-1))
#else
#define ADJUST_IA64_REGNUM(reg) (reg)
#endif /* HP_IA64 */

/* RM: HP debug information on IA64 encodes the slot number in bits 3
 * and 2 of the address. Gambit expects it in bits 1 and 0 of the
 * address. The following macro converts from the debug info encoding
 * to the Gambit encoding
 *
 * MC: gcc and the rest of the world encode this in bits 0 and 1.  Accomodate
 * gcc.  Eventually the HP compilers will change and we should stop using
 * bits 3 and 2.  8/25/00.   Fixme.  Change perhaps 03/01.
 */
#ifdef HP_IA64
#define ADJUST_IA64_SLOT_ENCODING(addr) \
  (((addr) & 3) ? (addr) :		\
		  (((addr) & ~((CORE_ADDR) 0xf)) | (((addr) & 0xc) >> 2)))
#else
#define ADJUST_IA64_SLOT_ENCODING(addr) (addr)
#endif

/* rename for the FSF code that uses generic name. */
#define DWARF2_REG_TO_REGNUM(reg) ADJUST_IA64_REGNUM(reg)

/* The symbol location baton types used by the DWARF-2 reader (i.e.
   SYMBOL_LOCATION_BATON for a LOC_COMPUTED symbol).  "struct
   dwarf2_locexpr_baton" is for a symbol with a single location
   expression; "struct dwarf2_loclist_baton" is for a symbol with a
   location list.  */

struct dwarf2_locexpr_baton
{
  /* Pointer to the start of the location expression.  */
  unsigned char *data;

  /* Length of the location expression.  */
  unsigned long size;

  /* The objfile containing the symbol whose location we're computing.  */
  struct objfile *objfile;
};

struct dwarf2_loclist_baton
{
  /* The initial base address for the location list, based on the compilation
     unit.  */
  CORE_ADDR base_address;

  /* Pointer to the start of the location list.  */
  unsigned char *data;

  /* Length of the location list.  */
  unsigned long size;

  /* The objfile containing the symbol whose location we're computing.  */
  struct objfile *objfile;
};

extern struct location_funcs dwarf2_locexpr_funcs;
extern struct location_funcs dwarf2_loclist_funcs;

#endif
