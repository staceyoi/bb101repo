/* DWARF 2 debugging format support for GDB.

   Copyright 1994, 1995, 1996, 1997, 1998 Free Software Foundation, Inc.

   Adapted by Gary Funck (gary@intrepid.com), Intrepid Technology,
   Inc.  with support from Florida State University (under contract
   with the Ada Joint Program Office), and Silicon Graphics, Inc.
   Initial contribution by Brent Benson, Harris Computer Systems, Inc.,
   based on Fred Fish's (Cygnus Support) implementation of DWARF 1
   support in dwarfread.c

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>
#include <ctype.h>
#include "limits.h"
#include "defs.h"
#include "bfd.h"
#include "elf-bfd.h" 
#include "symtab.h"
#include "gdbtypes.h"
#include "symfile.h"
#include "objfiles.h"
#include "elf/dwarf2.h"
#include "buildsym.h"
#include "demangle.h"
#include "expression.h"

#include "language.h"
#include "complaints.h"
#include "hpread.h"
#include "bcache.h"
#include "dwarf2expr.h"
#include "dwarf2loc.h"
#include <fcntl.h>
#include "gdb_string.h"
#include "gdbcmd.h"
#include <sys/types.h>

#include "unwind_dld.h"
#include <sys/mman.h>
#include "libbfd.h"
#include "symtab.h"
#include "top.h"
#include "target.h"

#if defined(HP_IA64)
#include "machine/sys/inline.h" /* _Asm_lfetch_excl */

/* Macro support. */
#include "macrotab.h"
#endif

#define DEBUG(x)

#ifdef HP_IA64
  /* set to _main_ for compatibility with PA */
  char fortran_main_string[] = "_main_";
#else
  char fortran_main_string[] = "";
#endif

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
/* Used by dwarf2_decode_minimal. */
extern void start_subfile (char *, char *);
extern char *elf_symtab_read_notes (bfd *, int *);
#endif
 extern int is_purified_program;

extern void hpread_build_doom_psymtabs
  PARAMS ((struct objfile *, struct section_offsets *, int));
extern void parse_filename
  PARAMS ((char *, struct parse_filename_info *));
extern void build_doom_tables(struct objfile *);

#ifdef PATHMAP
extern void pathmap_set_cdir (char *cdir);
#endif /* PATHMAP */

/* set/show variable for source level debugging without -g */
static char *src_no_g_mode_kind_names[] =
  {
    "none", "no_sys_libs", "all" , NULL
  };

static char *src_no_g_mode_string = "no_sys_libs";

/* JAGae23887 - add old-vtable flag to allow users to use old
   DW_VIRTUALITY values. */
static char *old_vtable_mode_kind_names[] =
  {
    "off", "on", NULL
  };

static char *old_vtable = "off";

/* JAGag02054 - Switching inline debugging on by default
   on both PA and IA. */

char *inline_debug_type[] = 
  {
    "off",
    "on",
    "inline_bp_all",
    "inline_bp_individual",
    NULL
  };

enum inline_type inline_debugging = ON; 
char *inl_debug = "on";

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
boolean dwarf2_read_minimal = false;
boolean reading_MLT = false;
#endif

/* Remove this when JAGae34124 is fixed by open source g++. */
#define JAGae34124

int hp_dwarf2_object_present = 0;
/* Required for namespaces */
#define INIT_MAX_ALIASES 20
#define INIT_GLOBAL_USINGS 20
char *global_var_name = NULL;
char *import_var_name = NULL;
int num_globusings = 0;
int max_globusings = 0;
int num_anon_ns = 0;
struct symbol **global_using_dirs = NULL;
struct aliases *aliases = NULL;

#define ANON_NS "<unnamed namespace>"

#ifndef GCC_PRODUCER
#define GCC_PRODUCER "GNU C "
#endif

#ifndef GPLUS_PRODUCER
#define GPLUS_PRODUCER "GNU C++ "
#endif

/* .debug_pubnames header
   Because of alignment constraints, this structure has padding and cannot
   be mapped directly onto the beginning of the .debug_info section.  */
typedef struct pubnames_header
  {
    unsigned int length;	/* length of the .debug_pubnames
				   contribution  */
    unsigned char version;	/* version number -- 2 for DWARF
				   version 2 */
    unsigned int info_offset;	/* offset into .debug_info section */
    unsigned int info_size;	/* byte size of .debug_info section
				   portion */
  }
_PUBNAMES_HEADER;
#define _ACTUAL_PUBNAMES_HEADER_SIZE 13

/* .debug_pubnames header
   Because of alignment constraints, this structure has padding and cannot
   be mapped directly onto the beginning of the .debug_info section.  */
typedef struct aranges_header
  {
    unsigned int length;	/* byte len of the .debug_aranges
				   contribution */
    unsigned short version;	/* version number -- 2 for DWARF
				   version 2 */
    unsigned int info_offset;	/* offset into .debug_info section */
    unsigned char addr_size;	/* byte size of an address */
    unsigned char seg_size;	/* byte size of segment descriptor */
  }
_ARANGES_HEADER;
#define _ACTUAL_ARANGES_HEADER_SIZE 12

/* .debug_line statement program prologue
   Because of alignment constraints, this structure has padding and cannot
   be mapped directly onto the beginning of the .debug_info section.  */
typedef struct statement_prologue
  {
    unsigned int total_length;	/* byte length of the statement
				   information */
    unsigned short version;	/* version number -- 2 for DWARF
				   version 2 */
    unsigned int prologue_length;	/* # bytes between prologue &
					   stmt program */
    unsigned char minimum_instruction_length;	/* byte size of
						   smallest instr */
    unsigned char default_is_stmt;	/* initial value of is_stmt
					   register */
    char line_base;
    unsigned char line_range;
    unsigned char opcode_base;	/* number assigned to first special
				   opcode */
    unsigned char *standard_opcode_lengths;
  }
_STATEMENT_PROLOGUE;

/* The dwarf2 spec states that the length of the line section is
   given by the total length field and does not include the length
   of this field. Since the total length of the debug line section is
   the length specified by this field + length of the field we need to
   add the length of the field to the value of the field.
   The length is currently 4 */
#define  TOTAL_LENGTH_FIELD_LENGTH  4

/* When we read in the partial die for the comp unit we have the 2
   dwarf attributes DW_AT_stmt_list and the DW_AT_HP_actuals_stmt_list.
   We don't want to create extra storage for each die so we keep these
   variables and then set the offsets in the psymtab from these.
*/
unsigned long dwarf_line_offset_pst;
unsigned long dwarf_line_offset_actual_pst;

/* offsets and sizes of debugging sections */

static file_ptr dwarf_info_offset;
static file_ptr dwarf_abbrev_offset;
static file_ptr dwarf_info_offset_procs;
static file_ptr dwarf_abbrev_offset_procs;
static file_ptr dwarf_line_offset;
static file_ptr dwarf_line_offset_actual;
static file_ptr dwarf_loc_offset;
static file_ptr dwarf_str_offset;
#ifdef HP_IA64
static file_ptr dwarf_macinfo_offset;
#endif

static unsigned int dwarf_info_size;
static unsigned int dwarf_abbrev_size;
static unsigned int dwarf_info_size_procs;
static unsigned int dwarf_abbrev_size_procs;
static unsigned int dwarf_line_size;
static unsigned int dwarf_line_size_actual;
static unsigned int dwarf_loc_size;
static unsigned int dwarf_str_size;
#ifdef HP_IA64
static unsigned int dwarf_macinfo_size;
#endif

static CORE_ADDR import_lowpc;
static CORE_ADDR comp_unit_lowpc;

/* names of the debugging sections */

#define INFO_SECTION     ".debug_info"
#define ABBREV_SECTION   ".debug_abbrev"
#define INFO_SECTION_PROCS     ".debug_procs_info"
#define ABBREV_SECTION_PROCS   ".debug_procs_abbrev"
#define LINE_SECTION     ".debug_line"
#define PUBNAMES_SECTION ".debug_pubnames"
#define ARANGES_SECTION  ".debug_aranges"
#define LOC_SECTION      ".debug_loc"
#define MACINFO_SECTION  ".debug_macinfo"
#define STR_SECTION      ".debug_str"
#define ACTUAL_LINE_SECTION ".debug_actual"

/* local data types */

/* The data in a compilation unit header looks like this.  */
struct comp_unit_head
  {
    unsigned int length;
    short version;
    unsigned int abbrev_offset;
    unsigned char addr_size;

    /* Base address of this compilation unit.  */
    CORE_ADDR base_address;

    /* Non-zero if base_address has been set.  */
    int base_known;
  };

/* The data in the .debug_line statement prologue looks like this.  */
struct line_head
  {
    unsigned int total_length;
    unsigned short version;
    unsigned int prologue_length;
    unsigned char minimum_instruction_length;
    unsigned char default_is_stmt;
    int line_base;
    unsigned char line_range;
    unsigned char opcode_base;
    unsigned char *standard_opcode_lengths;
  };

/* When we construct a partial symbol table entry we only
   need this much information. */
struct partial_die_info
  {
    enum dwarf_tag tag : 16;
    unsigned char has_children : 1;
    char is_external : 2;
    unsigned char is_declaration : 1;
    unsigned char has_type : 1;
    unsigned char language : 4;
    unsigned char calling_convention;
    unsigned int entry_count;
    unsigned int offset;
    unsigned int abbrev;
    unsigned int type_length;
    unsigned int dies_length;
    char *name;
    char *dirname;
    char *prof_flags; /* used to indicate if comp unit with -g */
    struct attribute *locdesc;
    char *sibling;
    CORE_ADDR lowpc;
    CORE_ADDR highpc;
  };

/* This data structure holds the information of an abbrev. */
struct abbrev_info
  {
    unsigned int number;	/* number identifying abbrev */
    enum dwarf_tag tag:16;	/* dwarf tag */
    unsigned char has_children:1;	/* boolean */
    unsigned int num_attrs;	/* number of attributes */
    struct attr_abbrev *attrs;	/* an array of attribute descriptions */
    struct abbrev_info *next;	/* next in chain */
  };

struct attr_abbrev
  {
    enum dwarf_attribute name;
    enum dwarf_form form;
  };

/* This data structure holds a complete die structure. */
struct die_info
  {
    enum dwarf_tag tag : 16;	/* Tag indicating type of die */
    unsigned short has_children:1;	/* Does the die have children */
    unsigned int abbrev;	/* Abbrev number */
    unsigned int offset;	/* Offset in .debug_info section */
    unsigned int num_attrs;	/* Number of attributes */
    struct attribute *attrs;	/* An array of attributes */
    struct die_info *next_ref;	/* Next die in ref hash table */
    struct die_info *next;	/* Next die in linked list */
    struct type *type;		/* Cached type information */
  };

/* Attributes have a name and a value */
struct attribute
  {
    enum dwarf_attribute name;
    enum dwarf_form form;
    union
      {
	char *str;
	struct dwarf_block *blk;
	unsigned long unsnd;
	long int snd;
	CORE_ADDR addr;
      }
    u;
  };

/* Get at parts of an attribute structure */

#define DW_STRING(attr)    ((attr)->u.str)
#define DW_UNSND(attr)     ((attr)->u.unsnd)
#define DW_BLOCK(attr)     ((attr)->u.blk)
#define DW_SND(attr)       ((attr)->u.snd)
#define DW_ADDR(attr)	   ((attr)->u.addr)

/* Blocks are a bunch of untyped bytes. */
struct dwarf_block
  {
    unsigned int size;
    char *data;
  };

/* We only hold one compilation unit's abbrevs in
   memory at any one time.  */
#ifndef ABBREV_HASH_SIZE
#define ABBREV_HASH_SIZE 121
#endif
#ifndef ATTR_ALLOC_CHUNK
#define ATTR_ALLOC_CHUNK 4
#endif
#ifndef ATTR_ALLOC_LARGE_CHUNK
#define ATTR_ALLOC_LARGE_CHUNK 25
#endif

static struct abbrev_info *dwarf2_abbrevs[ABBREV_HASH_SIZE];

/* A hash table of die offsets for following references.  */
#ifndef REF_HASH_SIZE
#define REF_HASH_SIZE 1021
#endif

/* This points to the current die_ref_table that has to be used for 
   finding dies from offsets. 
   The PA64 GCC is same as was before, and cross-compilation unit
   references are not implemented for that.
*/
#ifdef HP_IA64
static struct die_info **die_ref_table;
#else
static struct die_info *die_ref_table[REF_HASH_SIZE];
#endif

/* Obstack for allocating temporary storage used during symbol reading.  */
static struct obstack dwarf2_tmp_obstack;
static struct obstack dwarf2_tmp_obstack_2;

/* Offset to the first byte of the current compilation unit header,
   for resolving relative reference dies. */
static unsigned int cu_header_offset;

/* Allocate fields for structs, unions and enums in this size.  */
#ifndef DW_FIELD_ALLOC_CHUNK
#define DW_FIELD_ALLOC_CHUNK 4
#endif

/* The language we are debugging.  */
static enum language cu_language;
static const struct language_defn *cu_language_defn;

/* Actually data from the sections.  */
static char *dwarf_info_buffer;
static char *dwarf_abbrev_buffer;
static char *dwarf_info_buffer_procs;
static char *dwarf_abbrev_buffer_procs;
static char *dwarf_line_buffer;
static char *dwarf_line_buffer_actual;
static char *dwarf_str_buffer;
static char *dwarf_loc_buffer;
#ifdef HP_IA64
static char *dwarf_macinfo_buffer;
#endif

static struct debug_section 
  {
    char * info;
    char * abbrev;
    char * line;
    char * line_actual;
    char * loc;
    unsigned int abbrev_size;
    unsigned int reloc_abbrev_size;
    unsigned long info_size;
    unsigned long line_size;
    unsigned long line_size_actual;
    unsigned long loc_size;
    unsigned long reloc_info_size;
    unsigned long reloc_line_size;
    unsigned long reloc_line_size_actual;
    asection * info_section;
    asection * abbrev_section;
    asection * line_section;
    asection * line_section_actual;
    asection * loc_section;
#ifdef HP_IA64
    char * mac;
    unsigned long mac_size;
    unsigned long reloc_mac_size;
    asection * dwarf_macinfo_section;
#endif
    CORE_ADDR lowaddr;

    /* Each section will require different die refs, abbrevs, obstack
       and corresponding to each dwarf_info section there should
       be a comp_unit, hence a cu_header_offset */

    struct die_info *die_ref_table[REF_HASH_SIZE];
    struct abbrev_info *dwarf2_abbrevs[ABBREV_HASH_SIZE];
    unsigned int cu_header_offset;
    enum language cu_language;
    const struct language_defn *cu_language_defn;

    /* flags */
    union
    {
      struct
      {
	unsigned int f_skip:1;
	unsigned int f_nofunc:1;
	unsigned int f_principal:1;
	unsigned int f_reserve:29;
      }
      flag;
      int f_all;
    }
    flags;    
    
}  *debug_sections;

/* set debug-dwutils-processed on to enable debugging of
   dwutils processed binaries. */
int debug_dwutils_processed = 0;

static struct psymtabs_cache {
  struct partial_symtab *pst;
  int this_offset;
  int next_offset;
  struct objfile *obj;
} *cached_psymtabs;
/* This has the count of cached psymtabs in the entire cache. */
static int cached_psymtabs_count = 0;
/* This is the size of the cache at any point of time. */
static int cached_psymtabs_size = 100;

extern struct pending *free_pendings;
struct psymtab_expansion_history
{
  struct partial_symtab *pst; 
  struct die_info **die_ref_table;
  char *dwarf_info_buffer;
  char *dwarf_abbrev_buffer;
  char *dwarf_line_buffer;
  char *dwarf_line_buffer_actual;
  char *dwarf_info_buffer_procs;
  char *dwarf_abbrev_buffer_procs;
  char *dwarf_str_buffer;
  char *dwarf_loc_buffer;
  char *dwarf_macinfo_buffer;
  CORE_ADDR baseaddr;
  CORE_ADDR data_baseaddr;
  struct obstack dwarf2_tmp_obstack;
  struct obstack dwarf2_tmp_obstack_2;
  unsigned long dwarf_line_offset_pst;
  unsigned long dwarf_line_offset_actual_pst;
  struct context_stack *context_stack;
  unsigned int context_stack_depth;
  unsigned int context_stack_size;
  char *last_source_file;
  CORE_ADDR last_source_start_addr;
  struct subfile *subfiles;
  struct subfile *current_subfile;
  unsigned int symnum;
  unsigned char processing_gcc_compilation;
  unsigned char processing_acc_compilation;
  unsigned char processing_hp_compilation;
  struct pending *file_symbols;
  struct pending *local_symbols; 
  struct pending *global_symbols;
  struct pending *param_symbols;
  char *global_var_name;
  char *import_var_name;
  int num_globusings;
  int max_globusings;
  int num_anon_ns;
  struct symbol **global_using_dirs;
  struct aliases *aliases;

  int within_function;
  struct pending_block *pending_blocks;
  struct pending *free_pendings;

  struct subfile_stack *subfile_stack;
  char *(*next_symbol_text_func) (struct objfile *);
  struct type **type_vector;
  int type_vector_length;
  struct macro_table *pending_macros;

  /* offsets and sizes of debugging sections */

  file_ptr dwarf_info_offset;
  file_ptr dwarf_abbrev_offset;
  file_ptr dwarf_info_offset_procs;
  file_ptr dwarf_abbrev_offset_procs;
  file_ptr dwarf_line_offset;
  file_ptr dwarf_line_offset_actual;
  file_ptr dwarf_loc_offset;
  file_ptr dwarf_str_offset;
  file_ptr dwarf_macinfo_offset;

  unsigned int dwarf_info_size;
  unsigned int dwarf_abbrev_size;
  unsigned int dwarf_info_size_procs;
  unsigned int dwarf_abbrev_size_procs;
  unsigned int dwarf_line_size;
  unsigned int dwarf_line_size_actual;
  unsigned int dwarf_loc_size;
  unsigned int dwarf_str_size;
  unsigned int dwarf_macinfo_size;

  CORE_ADDR import_lowpc;
  CORE_ADDR comp_unit_lowpc;
  struct type *ftypes[FT_NUM_MEMBERS];	
  struct pending **list_in_scope;
  CORE_ADDR current_func_lowpc;
  CORE_ADDR current_func_highpc;
  struct type *current_func_type;
  struct symbol *current_funcsym;
  struct try_block *current_t_block;

  int func_opt_level;
  int file_opt_level;

};

static int sectn;
static int processing_doom_objfile = 0; /* boolean to tell us whether we are 
				      processing mainline or processing the
				      objfile for DOOM */

/* The generic symbol table building routines have separate lists for
   file scope symbols and all all other scopes (local scopes).  So
   we need to select the right one to pass to add_symbol_to_list().
   We do it by keeping a pointer to the correct list in list_in_scope.

   FIXME:  The original dwarf code just treated the file scope as the first
   local scope, and all other local scopes as nested local scopes, and worked
   fine.  Check to see if we really need to distinguish these
   in buildsym.c.  */
static struct pending **list_in_scope = &file_symbols;

/* FIXME: decode_locdesc sets these variables to describe the location
   to the caller.  These ought to be a structure or something.   If
   none of the flags are set, the object lives at the address returned
   by decode_locdesc.  */

static int optimized_out;	/* No ops in location in expression,
				   so object was optimized out.  */
static int isreg;		/* Object lives in register.
				   decode_locdesc's return value is
				   the register number.  */
static int offreg;		/* Object's address is the sum of the
				   register specified by basereg, plus
				   the offset returned.  */
static int basereg;		/* See `offreg'.  */
static int isderef;		/* Value described by flags above is
				   the address of a pointer to the object.  */
static int islocal;		/* Variable is at the returned offset
				   from the frame start, but there's
				   no identified frame pointer for
				   this function, so we can't say
				   which register it's relative to;
				   use LOC_LOCAL.  */
static int ispiece;	        /* if not 0, then stack up to stacki contains
				 * information about the pieces (see
				 * DW_OP_piece)
				 */
static int isreference;         /* Kludge to identify reference parameters */
#ifdef HP_DWARF2_EXTENSIONS
static int istls;		/* Kludge to identify TLS variables */
#endif /* HP_DWARF2_EXTENSIONS */
#ifdef HP_IA64
 /* Continuing kludging for HP_IA64 ... */
static int no_of_mod_ranges;	/* Kludge to keep count of modifiability 
				   ranges */
/* Who wants to alloc/dealloc dynamic arrays? */
#define INITIAL_MOD_RANGES_PER_VARIABLE 10
/* Kludge to pass around modifiability ranges */
static struct gdb_hp_mod_unmod_range* mod_ranges = NULL;
static int allocated_mod_ranges = 0;
#endif /* HP_IA64 */

/* DW_AT_frame_base values for the current function.
   frame_base_reg is -1 if DW_AT_frame_base is missing, otherwise it
   contains the register number for the frame register.
   frame_base_offset is the offset from the frame register to the
   virtual stack frame. */
static int frame_base_reg;
static CORE_ADDR frame_base_offset;

/* This value is added to each symbol value.  FIXME:  Generalize to
   the section_offsets structure used by dbxread (once this is done,
   pass the appropriate section number to end_symtab).  */
static CORE_ADDR baseaddr;	/* Add to each symbol value */

/* srikanth, text and data symbols need to be relocated by different
   amounts. Otherwise, we will print variables from shared libraries
   incorrectly. For a.out this is usually not a problem since the
   link time addresses are identical to load time addresses.
*/

static CORE_ADDR data_baseaddr; /* Add to each data symbol value. */


CORE_ADDR current_func_lowpc = 0;
CORE_ADDR current_func_highpc = 0;
static struct type *	current_func_type;

#ifdef HP_DWARF2_EXTENSIONS
static int func_opt_level = 0;
#endif

/* JAGag33388 */
int file_opt_level = 0;
/* We put a pointer to this structure in the read_symtab_private field
   of the psymtab.
   The complete dwarf information for an objfile is kept in the
   psymbol_obstack, so that absolute die references can be handled.
   Most of the information in this structure is related to an entire
   object file and could be passed via the sym_private field of the objfile.
   It is however conceivable that dwarf2 might not be the only type
   of symbols read from an object file.  */
/* Poorva - Since we were keeping things around in memory we used to
   store pointers to where the debug sections were. Now we need to store
   2 things - the first is the offset of the debug sections from the
   beginning of the file and the second is the 
   the offsets of the debug sections for these psts  (or .o's)
   from the beginning of the debug info for the objfile

   We then add this offset to the offsets we store in the objfile
   that tell us where the debug sections for the objfile start from.
   Addition of these 2 offsets tells us the offset at which the
   debug section for this .o starts from relative to the beginning
   of the file (exe or shared library)
   */
/* Poorva: USING_MMAP is a feature define and is defined for IA64. 
   We use mmap to map in the debug info sections when we build the 
   psymtabs etc. We then unmap these sections after we are done building
   psymtabs. When we build our symtabs we map in the parts we need 
   for that symtab.
   Without mmap if the number of host_char_bits == 8 we do not malloc
   space on the obstack instead we just materialize a pointer to
   it from the debug info that has been malloc'd in. We then try to 
   write back to this space but it is mmap'd read only and we get into trouble.
*/

struct dwarf2_pinfo
  {
#ifndef USING_MMAP
    /* Pointer to start of dwarf info buffer for the objfile.  */

    char *dwarf_info_buffer;

    /* Offset in dwarf_info_buffer for this compilation unit. */

    unsigned long dwarf_info_offset;

    /* Pointer to start of dwarf abbreviation buffer for the objfile.  */

    char *dwarf_abbrev_buffer;

    /* Size of dwarf abbreviation section for the objfile.  */

    unsigned int dwarf_abbrev_size;

    /* Pointer to start of dwarf procs info buffer for the objfile.  */

    char *dwarf_info_buffer_procs;

    /* Offset in dwarf_info_buffer_procs for this compilation unit. */

    unsigned long dwarf_info_offset_procs;

    /* Pointer to start of dwarf procs abbreviation buffer for the objfile.  */

    char *dwarf_abbrev_buffer_procs;

    /* Size of dwarf abbreviation procs section for the objfile.  */

    unsigned int dwarf_abbrev_size_procs;

    /* Pointer to start of dwarf line buffer for the objfile.  */

    char *dwarf_line_buffer;

    /* Pointer to start of dwarf actual line buffer for the objfile.  */

    char *dwarf_line_buffer_actual;

    /* Pointer to start of dwarf string buffer for the objfile.  */

    char *dwarf_str_buffer;

    /* Size of dwarf string section for the objfile.  */

    unsigned int dwarf_str_size;

    /* Offset to start of compile_unit tag to handle multiple compile
       units within a compilation unit */

    unsigned long dwarf_comp_unit_offset;
#else
        /* Offset of dwarf info buffer for this psymtab from the
       beginning of the dwarf info buffer for the objfile.  */
    unsigned long dwarf_info_offset;

    /* Size of dwarf_info_buffer for this compilation unit. */

    unsigned long dwarf_info_size;

    /* Offset of dwarf abbrev buffer for this psymtab from the
       beginning of the dwarf abbrev buffer for the objfile.  */

    unsigned long dwarf_abbrev_offset;

    /* Size of dwarf abbreviation section for the objfile.  */

    unsigned int dwarf_abbrev_size;

    /* Offset of dwarf info procs buffer for this psymtab from the
       beginning of the dwarf info procs buffer for the objfile.  */

    unsigned long dwarf_info_offset_procs;

    /* Size of dwarf_info_buffer_procs for this compilation unit. */

    unsigned long dwarf_info_size_procs;

    /* Offset of dwarf abbrev procs buffer for this psymtab from the
       beginning of the dwarf abbrev procs buffer for the objfile.  */

    unsigned long dwarf_abbrev_offset_procs;

    /* Size of dwarf abbreviation procs section for the objfile.  */

    unsigned int dwarf_abbrev_size_procs;

    /* Offset of dwarf line buffer for this psymtab from the
       beginning of the dwarf line buffer for the objfile.  */

    unsigned long dwarf_line_offset;

    /* Size of dwarf line buffer for the objfile.  */

    unsigned long dwarf_line_size;

    /* Offset of dwarf line actual buffer for this psymtab from the
       beginning of the dwarf line actual buffer for the objfile.  */

    unsigned long dwarf_line_offset_actual;

    /* Size of dwarf line buffer actual for the psymtab.  */

    unsigned long dwarf_line_size_actual;

    /* Pointer to start of dwarf string buffer for the objfile.  */

    char * dwarf_str_buffer;

    /* Size of dwarf string section for the objfile.  */

    unsigned int dwarf_str_size;

#ifdef HP_IA64
    /* Pointer to start of dwarf macinfo buffer for the objfile.  */

    char *dwarf_macinfo_buffer;

    /* Size of dwarf macinfo section for the objfile.  */

    unsigned int dwarf_macinfo_size;
#endif

    /* Offset to start of compile_unit tag to handle multiple compile
       units within a compilation unit */

    unsigned long dwarf_comp_unit_offset;

    /* Offsets of the debug_info/ debug_procs_info from the start of the
       file. Depends on which section is used to make this psymtab */

    unsigned long dwarf_info_file_start;

    /* Offsets of the debug_abbrev/ debug_procs_abbrev from the start of the
       file. Depends on which section is used to make this psymtab */

    unsigned long dwarf_abbrev_file_start;
#endif

    /* Pointer to start of dwarf locations buffer for the objfile.  */

    char *dwarf_loc_buffer;

    /* Size of dwarf locations buffer for the objfile.  */

    unsigned int dwarf_loc_size;
  };

#define PST_PRIVATE(p) ((struct dwarf2_pinfo *)(void *)(p)->read_symtab_private)

#ifndef USING_MMAP

#define DWARF_INFO_BUFFER(p) (PST_PRIVATE(p)->dwarf_info_buffer)
#define DWARF_INFO_OFFSET(p) (PST_PRIVATE(p)->dwarf_info_offset)
#define DWARF_ABBREV_BUFFER(p) (PST_PRIVATE(p)->dwarf_abbrev_buffer)
#define DWARF_ABBREV_SIZE(p) (PST_PRIVATE(p)->dwarf_abbrev_size)
#define DWARF_INFO_BUFFER_PROCS(p) (PST_PRIVATE(p)->dwarf_info_buffer_procs)
#define DWARF_INFO_OFFSET_PROCS(p) (PST_PRIVATE(p)->dwarf_info_offset_procs)
#define DWARF_ABBREV_BUFFER_PROCS(p) (PST_PRIVATE(p)->dwarf_abbrev_buffer_procs)
#define DWARF_ABBREV_SIZE_PROCS(p) (PST_PRIVATE(p)->dwarf_abbrev_size_procs)
#define DWARF_LINE_BUFFER(p) (PST_PRIVATE(p)->dwarf_line_buffer)
#define DWARF_LINE_BUFFER_ACTUAL(p) (PST_PRIVATE(p)->dwarf_line_buffer_actual)
#define DWARF_STR_BUFFER(p) (PST_PRIVATE(p)->dwarf_str_buffer)
#define DWARF_STR_SIZE(p) (PST_PRIVATE(p)->dwarf_str_size)
#ifdef HP_IA64
#define DWARF_MACINFO_BUFFER(p) (PST_PRIVATE(p)->dwarf_macinfo_buffer)
#define DWARF_MACINFO_SIZE(p) (PST_PRIVATE(p)->dwarf_macinfo_size)
#endif
#define DWARF_COMP_UNIT_OFFSET(p) (PST_PRIVATE(p)->dwarf_comp_unit_offset)

#else

#define DWARF_INFO_OFFSET(p) (PST_PRIVATE(p)->dwarf_info_offset)
#define DWARF_INFO_SIZE(p) (PST_PRIVATE(p)->dwarf_info_size)
#define DWARF_ABBREV_OFFSET(p) (PST_PRIVATE(p)->dwarf_abbrev_offset)
#define DWARF_ABBREV_SIZE(p) (PST_PRIVATE(p)->dwarf_abbrev_size)
#define DWARF_INFO_OFFSET_PROCS(p) (PST_PRIVATE(p)->dwarf_info_offset_procs)
#define DWARF_INFO_SIZE_PROCS(p) (PST_PRIVATE(p)->dwarf_info_line_procs)
#define DWARF_ABBREV_OFFSET_PROCS(p) (PST_PRIVATE(p)->dwarf_abbrev_offset_procs)
#define DWARF_ABBREV_SIZE_PROCS(p) (PST_PRIVATE(p)->dwarf_abbrev_size_procs)
#define DWARF_LINE_OFFSET(p) (PST_PRIVATE(p)->dwarf_line_offset)
#define DWARF_LINE_SIZE(p) (PST_PRIVATE(p)->dwarf_line_size)
#define DWARF_LINE_OFFSET_ACTUAL(p) (PST_PRIVATE(p)->dwarf_line_offset_actual)
#define DWARF_LINE_SIZE_ACTUAL(p) (PST_PRIVATE(p)->dwarf_line_size_actual)
#define DWARF_STR_BUFFER(p) (PST_PRIVATE(p)->dwarf_str_buffer)
#define DWARF_STR_SIZE(p) (PST_PRIVATE(p)->dwarf_str_size)
#define DWARF_COMP_UNIT_OFFSET(p) (PST_PRIVATE(p)->dwarf_comp_unit_offset)
#define DEBUG_INFO_FILE_START(p) (PST_PRIVATE(p)->dwarf_info_file_start)
#define DEBUG_ABBREV_FILE_START(p) (PST_PRIVATE(p)->dwarf_abbrev_file_start)
#ifdef HP_IA64
#define DWARF_MACINFO_BUFFER(p) (PST_PRIVATE(p)->dwarf_macinfo_buffer)
#define DWARF_MACINFO_SIZE(p) (PST_PRIVATE(p)->dwarf_macinfo_size)
#endif

#endif

#define DWARF_LOC_BUFFER(p)     (PST_PRIVATE(p)->dwarf_loc_buffer)
#define DWARF_LOC_SIZE(p)       (PST_PRIVATE(p)->dwarf_loc_size)


/* Maintain an array of referenced fundamental types for the current
   compilation unit being read.  For DWARF version 1, we have to construct
   the fundamental types on the fly, since no information about the
   fundamental types is supplied.  Each such fundamental type is created by
   calling a language dependent routine to create the type, and then a
   pointer to that type is then placed in the array at the index specified
   by it's FT_<TYPENAME> value.  The array has a fixed size set by the
   FT_NUM_MEMBERS compile time constant, which is the number of predefined
   fundamental types gdb knows how to construct.  */
static struct type *ftypes[FT_NUM_MEMBERS];	/* Fundamental types */

/* FIXME: We might want to set this from BFD via bfd_arch_bits_per_byte,
   but this would require a corresponding change in unpack_field_as_long
   and friends.  */
static int bits_per_byte = 8;

/* The routines that read and process dies for a C struct or C++ class
   pass lists of data member fields and lists of member function fields
   in an instance of a field_info structure, as defined below.  */
struct field_info
  {
    /* List of data member and baseclasses fields. */
    struct nextfield
      {
	struct nextfield *next;
	int accessibility;
	int virtuality;
	struct field field;
      }
     *fields;

    /* Number of fields.  */
    int nfields;

    /* Number of baseclasses.  */
    int nbaseclasses;

    /* Set if the accesibility of one of the fields is not public.  */
    int non_public_fields;

    /* Member function fields array, entries are allocated in the order they
       are encountered in the object file.  */
    struct nextfnfield
      {
	struct nextfnfield *next;
	struct fn_field fnfield;
      }
     *fnfields;

    /* Member function fieldlist array, contains name of possibly overloaded
       member function, number of overloaded member functions and a pointer
       to the head of the member function field chain.  */
    struct fnfieldlist
      {
	char *name;
	int length;
	struct nextfnfield *head;
      }
     *fnfieldlists;

    /* Number of entries in the fnfieldlists array.  */
    int nfnfields;
  };

/* FIXME: Kludge to mark a varargs function type for C++ member function
   argument processing.  */
#define TYPE_FLAG_VARARGS	(1 << 10)

/* Dwarf2 has no clean way to discern C++ static and non-static member
   functions. G++ helps GDB by marking the first parameter for non-static
   member functions (which is the this pointer) as artificial.
   We pass this information between dwarf2_add_member_fn and
   read_subroutine_type via TYPE_FIELD_ARTIFICIAL.  */
#define TYPE_FIELD_ARTIFICIAL	TYPE_FIELD_BITPOS

/* Various complaints about symbol reading that don't abort the process */

static struct complaint dwarf2_invalid_attrib_location_description =
{
  "invalid attribute class or form for location description in", 0, 0
};
static struct complaint dwarf2_loc_list_but_no_cu_base_addr =
{
  "Location list used without specifying the CU base address.", 0, 0
};
static struct complaint dwarf2_func_lowpc_not_set_for_mod_range =
{
  "'current_func_lowpc' not set when DW_OP_HP_mod_range was seen", 0, 0
};
static struct complaint dwarf2_non_const_array_attr_ignored =
{
  "non-constant array attribute form '%s' ignored", 0, 0
};
static struct complaint dwarf2_missing_line_number_section =
{
  "missing .debug_line section", 0, 0
};
static struct complaint dwarf2_mangled_line_number_section =
{
  "mangled .debug_line section", 0, 0
};
static struct complaint dwarf2_missing_line_number_section_actual =
{
  "missing .debug_actuals section", 0, 0
};
static struct complaint dwarf2_unsupported_die_ref_attr =
{
  "unsupported die ref attribute form: '%s'", 0, 0
};
static struct complaint dwarf2_unsupported_stack_op =
{
  "unsupported stack op: '%s'", 0, 0
};

/* These variable were not being used and TOHECC
   with +wlint emitted warnings, so commenting them for now */
/*
static struct complaint dwarf2_complex_location_expr =
{
  "location expression too complex", 0, 0
};
*/

static struct complaint dwarf2_unsupported_tag =
{
  "unsupported tag: '%s'", 0, 0
};
static struct complaint dwarf2_unsupported_at_encoding =
{
  "unsupported DW_AT_encoding: '%s'", 0, 0
};
static struct complaint dwarf2_unsupported_at_frame_base =
{
  "unsupported DW_AT_frame_base for function '%s'", 0, 0
};
static struct complaint dwarf2_unexpected_tag =
{
  "unexepected tag in read_type_die: '%s'", 0, 0
};
static struct complaint dwarf2_missing_at_frame_base =
{
  "DW_AT_frame_base missing for DW_OP_fbreg", 0, 0
};

/* These variable were not being used and TOHECC
   with +wlint emitted warnings, so commenting them for now */

/*
static struct complaint dwarf2_bad_static_member_name =
{
  "unrecognized static data member name '%s'", 0, 0
};
*/
static struct complaint dwarf2_unsupported_accessibility =
{
  "unsupported accessibility %d", 0, 0
};

#ifndef HP_IA64
static struct complaint dwarf2_bad_member_name_complaint =
{
  "cannot extract member name from '%s'", 0, 0
};
#endif

static struct complaint dwarf2_missing_member_fn_type_complaint =
{
  "member function type missing for '%s'", 0, 0
};
static struct complaint dwarf2_vtbl_not_found_complaint =
{
  "virtual function table pointer not found when defining class '%s'", 0, 0
};
static struct complaint dwarf2_absolute_sibling_complaint =
{
  "ignoring absolute DW_AT_sibling", 0, 0
};
static struct complaint dwarf2_const_value_length_mismatch =
{
  "const value length mismatch for '%s', got %d, expected %d", 0, 0
};
static struct complaint dwarf2_unsupported_const_value_attr =
{
  "unsupported const value attribute form: '%s'", 0, 0
};
static struct complaint dwarf2_unexpected_entry_point_count =
{
  "unexpected count for subroutine with entry points: '%d'", 0, 0
};

/* These variable were not being used and TOHECC
   with +wlint emitted warnings, so commenting them for now */

/*
static struct complaint dwarf2_unexpected_symbol_class_for_array_bound =
{
  "unexpected symbol class for array bound: '%d'", 0, 0
};
static struct complaint dwarf2_unexpected_symbol_class_for_array_count =
{
  "unexpected symbol class for array count: '%d'", 0, 0
};
*/

/* Remember the addr_size read from the dwarf.
   If a target expects to link compilation units with differing address
   sizes, gdb needs to be sure that the appropriate size is here for
   whatever scope is currently getting read. */
static int address_size;

#ifndef HP_IA64
/* Some elf32 object file formats while linked for a 32 bit address
   space contain debug information that has assumed 64 bit
   addresses. Eg 64 bit MIPS target produced by GCC/GAS/LD where the
   symbol table contains 32bit address values while its .debug_info
   section contains 64 bit address values.
   ADDRESS_SIGNIFICANT_SIZE specifies the number significant bits in
   the ADDRESS_SIZE bytes read from the file */
static int address_significant_size;
#endif

static boolean processing_bad_pieces;

/* Externals references.  */
extern int info_verbose;	/* From main.c; nonzero => verbose */

/* local function prototypes */

static int attr_form_is_block (struct attribute *);

static void dwarf2_locate_sections (bfd *, asection *, PTR);

static void dwarf2_locate_section_indices PARAMS ((bfd *, asection *, PTR));

static void dwarf2_build_psymtabs_hard (struct objfile *, int);

static char *scan_partial_symbols (char *, struct objfile *,
				   CORE_ADDR *, CORE_ADDR *,
			           const struct comp_unit_head *);

static void add_partial_symbol (struct partial_die_info *, struct objfile *);

static void dwarf2_psymtab_to_symtab (struct partial_symtab *);

static void psymtab_to_symtab_1 (struct partial_symtab *);

static char *dwarf2_read_section (struct objfile *, file_ptr, unsigned int);

static void dwarf2_empty_abbrev_table (PTR);

static struct abbrev_info *dwarf2_lookup_abbrev (unsigned int,
				 const struct comp_unit_head *);

static char *read_partial_die (struct partial_die_info *,
			       bfd *, char *, int *,
			       const struct comp_unit_head *);

static char *read_full_die (struct die_info **, bfd *, char *,
			    const struct comp_unit_head *);

static char *read_attribute (struct attribute *, struct attr_abbrev *,
			     bfd *, char *,
			     const struct comp_unit_head *);

static unsigned int read_1_byte (bfd *, char *);

static int read_1_signed_byte (bfd *, char *);

static unsigned int read_2_bytes (bfd *, char *);

static unsigned int read_4_bytes (bfd *, char *);

static unsigned long read_8_bytes (bfd *, char *);

static CORE_ADDR read_address (bfd *, char *);

static char *read_n_bytes (bfd *, char *, unsigned int);

static LONGEST read_offset (bfd *, char *, int *);

static char *read_string (bfd *, char *, unsigned int *);

static char *read_indirect_string (bfd *, char *, unsigned int *);

static unsigned long read_unsigned_leb128 (bfd *, char *, unsigned int *);

static long read_signed_leb128 (bfd *, char *, unsigned int *);

static void set_cu_language (unsigned int);

static struct attribute *dwarf_attr (struct die_info *, unsigned int);

static void dwarf_decode_lines (unsigned int, char *, bfd *);

static void dwarf2_start_subfile (char *, char *);

static struct subfile * get_main_subfile (void);

static struct symbol *new_symbol (struct die_info *, struct type *,
				  struct objfile *,
				  const struct comp_unit_head *);

static void dwarf2_const_value (struct attribute *, struct symbol *,
				struct objfile *);

static void dwarf2_const_value_data (struct attribute *attr,
				     struct symbol *sym,
				     int bits);

static struct type *die_type (struct die_info *, struct objfile *,
			   const struct comp_unit_head *);

static struct type *die_containing_type (struct die_info *, struct objfile *,
			   const struct comp_unit_head *);


static struct type *tag_type_to_type (struct die_info *, struct objfile *,
			   const struct comp_unit_head *);

static void read_type_die (struct die_info *, struct objfile *,
			   const struct comp_unit_head *);

static void read_typedef (struct die_info *, struct objfile *,
			   const struct comp_unit_head *);

static void read_base_type (struct die_info *, struct objfile *);

static void read_file_scope (struct die_info *, struct objfile *,
			     const struct comp_unit_head *);

static void read_func_scope (struct die_info *, struct objfile *,
			     const struct comp_unit_head *);

static void read_lexical_block_scope (struct die_info *, struct objfile *,
				      const struct comp_unit_head *);

enum PC_bounds_rv
{
  PCB_ok,
  PCB_invalid,
  PCB_ignore
};
static enum PC_bounds_rv dwarf2_get_pc_bounds (struct die_info *,
                                               CORE_ADDR *, CORE_ADDR *, 
                                               struct objfile *);

static void dwarf2_add_field (struct field_info *, struct die_info *,
			      struct objfile *, struct die_info *,
			      const struct comp_unit_head *);

static void dwarf2_attach_fields_to_type (struct field_info *,
					  struct type *, struct objfile *);

static void dwarf2_add_member_fn (struct field_info *,
				  struct die_info *, struct type *,
				  struct objfile *objfile,
			          const struct comp_unit_head *);

static void dwarf2_attach_fn_fields_to_type (struct field_info *,
					     struct type *, struct objfile *);

static void read_structure_scope (struct die_info *, struct objfile *,
				  const struct comp_unit_head *);

static int is_die_declaration (struct die_info *);

static void read_common_block (struct die_info *, struct objfile *,
		   	       const struct comp_unit_head *);

static void read_enumeration (struct die_info *, struct objfile *,
                  	      const struct comp_unit_head *);

static struct type *dwarf_base_type (int, int, struct objfile *);

static CORE_ADDR decode_locdesc (struct symbol *sym, struct attribute *, 
				 struct objfile *, unsigned int *offset);
static CORE_ADDR decode_locexpr (struct symbol *sym, struct dwarf_block *, 
				 struct objfile *, unsigned int *offset);

static void read_array_type (struct die_info *, struct objfile *,
		 const struct comp_unit_head *);

static void read_tag_pointer_type (struct die_info *, struct objfile *,
		 const struct comp_unit_head *);

static void read_tag_ptr_to_member_type (struct die_info *, struct objfile *,
		 const struct comp_unit_head *);

static void read_tag_reference_type (struct die_info *, struct objfile *,
		 const struct comp_unit_head *);

static void read_tag_const_type (struct die_info *, struct objfile *,
		 const struct comp_unit_head *);

static void read_tag_volatile_type (struct die_info *, struct objfile *,
		 const struct comp_unit_head *);

static void read_tag_string_type (struct die_info *, struct objfile *,
		      const struct comp_unit_head *);

static void read_subroutine_type (struct die_info *, struct objfile *,
		      const struct comp_unit_head *);

struct die_info *read_comp_unit (char *, bfd *,
			 	 const struct comp_unit_head *);

static void process_die (struct die_info *, struct objfile *,
			 const struct comp_unit_head *);
static void process_try_block (struct die_info *, struct symbol *,
			       struct objfile *,
			       const struct comp_unit_head *);
static void process_catch_block (struct die_info *, struct symbol *,
				 struct objfile *,
				 const struct comp_unit_head *);

static char *dwarf2_linkage_name (struct die_info *);

static char *dwarf_tag_name (unsigned int);

static char *dwarf_attr_name (unsigned int);

static char *dwarf_form_name (unsigned int);

static char *dwarf_stack_op_name (unsigned int);

static char *dwarf_bool_name (unsigned int);

static char *dwarf_type_encoding_name (unsigned int);


struct die_info *sibling_die (struct die_info *);

void dump_die (struct die_info *);

void dump_die_list (struct die_info *);

void store_in_ref_table (unsigned int, struct die_info *);

static void dwarf2_empty_die_ref_table (void);

static unsigned int dwarf2_get_ref_die_offset (struct attribute *);

struct die_info *follow_die_ref (unsigned int);

static struct type *dwarf2_fundamental_type (struct objfile *, int);
#ifdef HP_IA64
void dwarf2_symfile_init (struct objfile *);
void dwarf2_doom_psymtab_to_symtab PARAMS ((struct partial_symtab *));
#endif


#ifdef USING_MMAP
static void dwarf2_unmap_section PARAMS ((char *, file_ptr,
                                           unsigned int));
#endif

static int dwarf2_read_abbrevs PARAMS ((bfd *, 
		     struct comp_unit_head *cu_header));
static void dwarf2_empty_section_abbrev_table PARAMS ((void));

static void dwarf2_init_section_abbrev_table PARAMS ((void));

static LONGEST read_offset (bfd *, char *, int *);

static char *read_indirect_string (bfd *, char *, unsigned int *);

static void dwarf_decode_lines_actual (unsigned int, char *, bfd *, int);

struct partial_symtab *procs_start_psymtab_common PARAMS ((struct objfile *,
	struct section_offsets *,   struct partial_die_info *,
	const struct comp_unit_head *));


#ifdef HP_DWARF2_EXTENSIONS
static void read_array_descriptor_type  PARAMS ((struct die_info *,
						 struct objfile *,
					         const struct comp_unit_head *));
#endif

static void read_namespace_scope PARAMS ((struct die_info *, struct objfile *,
					  const struct comp_unit_head *));

static void read_imported_declaration PARAMS ((struct die_info *,
					       struct objfile *,
					       const struct comp_unit_head *));

static struct symbol*  read_imported_module PARAMS ((struct die_info *, 
					  struct objfile *,
					  const struct comp_unit_head *));

static void handle_producer PARAMS ((char *));


static void dwarf2_empty_section_die_ref_table PARAMS ((void));


/* memory allocation interface */

static void dwarf2_free_tmp_obstack (PTR);

static struct dwarf_block *dwarf_alloc_block (void);

static struct abbrev_info *dwarf_alloc_abbrev (void);

static struct die_info *dwarf_alloc_die (void);

#ifdef HP_IA64
static void dwarf_decode_macros (unsigned int, char *, bfd *, struct objfile *);
#endif

static void
dwarf2_symbol_mark_computed (struct attribute *attr, struct symbol *sym,
			     const struct comp_unit_head *,
			     struct objfile *objfile);

static void dwarf2_free_tmp_obstack_2 PARAMS ((PTR));


static struct aliases *process_alias PARAMS ((struct die_info *,
					      struct aliases *,
					      struct objfile *,
					      const struct comp_unit_head *));

static char * generate_anon_name PARAMS ((struct objfile *));

#ifdef HP_DWARF2_EXTENSIONS
static void set_modifiability_information PARAMS ((struct symbol *));
#endif /* HP_DWARF2_EXTENSIONS */

#ifdef HP_IA64
static struct psymtab_expansion_history *
save_psymtab_expansion_history (struct partial_symtab *);

void
restore_psymtab_expansion_history (struct psymtab_expansion_history *,
                                   struct objfile *);

static struct partial_symtab *
find_pst_for_die_ref_addr (unsigned int);

static struct die_info *
follow_die_ref_in_table (struct partial_symtab *, unsigned int );

static struct die_info **
find_die_ref_table (struct objfile *, struct die_info *);


void free_cached_psymtabs (struct objfile *);
#endif

void free_die_ref_table (void **);
static void free_die_list (struct die_info *);

static struct die_info *
find_die_from_ref (struct die_info *, 
                   struct objfile *, 
                   struct attribute *);


static const char * dwarf_error_mesg = 
  "DWARF Error: wrong version (0x%lx, expected 2) in\n"
  "compilation unit header of the load module %s.\n"
  "This error might be caused due to this load module being"
  " built\nwith older compilers. You might be able to "
  "workaround this\nproblem by stripping the debug information "
  "from this module at\nthe cost of not being able to "
  "debug it. This can be obtained\nby executing 'strip -l %s'.\n";

/* A symbol with a location description involving DW_OP_piece has been
 * encountered.  Sym is the symbol whose SYMBOL_PIECE_LIST is to be 
 * initialized.  See piece_list in symtab.h.
 *
 * Stacki is an index to the top-of-stack in
 * the stack array.  From the top, the stack contains:
 *	 total_size_of_all_pieces
 *       number_of_pieces
 *	   type, one of PIECE_LOC_REGISTER, PIECE_LOC_BASEREG, PIECE_LOC_STATIC
 *	   size of this piece in bytes
 *	   address_or_register_number
 *	   for PIECE_LOC_BASEREG one more word for the offset
 *    ... repeat 3-4 words for each piece
 */

void 
init_sym_pieces(struct objfile * objfile,
	        struct symbol * sym, 
	        int stacki, 
	        CORE_ADDR* stack)
{
   enum 		piece_address_class 	piece_type;
   CORE_ADDR 		arg_word_1;
   CORE_ADDR 		arg_word_2;
   int 			pieces_left;
   int			piece_size;


   stacki--;  /* We aren't using total_size */
   pieces_left = (int) stack[stacki--];
   assert (pieces_left <= MAX_NBR_PIECES);

   SYMBOL_VALUE_ADDRESS(sym) = 0;  /* initialize full CORE_ADDR */
   SYMBOL_PIECE_LIST(sym) = (symbol_piece_t*) (void *) obstack_alloc(
		   &objfile->symbol_obstack,
		   (int) (sizeof(symbol_piece_t) * pieces_left));  
   SYMBOL_NBR_PIECES(sym) = pieces_left;
   assert (pieces_left > 0 && stacki > 0);

   while (pieces_left > 0)
     {
       assert (stacki > 0);
       pieces_left--;

       piece_type = (enum piece_address_class) stack[stacki--];
       piece_size = (int) stack[stacki--];
       arg_word_1 = stack[stacki--];
       arg_word_2 = 0;
       if (piece_type == PIECE_LOC_BASEREG) 
	 {
	   arg_word_2 = stack[stacki--];
	 }

       SYMBOL_PIECE_LIST(sym)[pieces_left].piece_type = piece_type;
       SYMBOL_PIECE_LIST(sym)[pieces_left].piece_size = piece_size;
       if (piece_type != PIECE_LOC_STATIC)
	 arg_word_1 = ADJUST_IA64_REGNUM(arg_word_1);
       SYMBOL_PIECE_LIST(sym)[pieces_left].arg1 = arg_word_1;
       SYMBOL_PIECE_LIST(sym)[pieces_left].arg2 = arg_word_2;
     }

  if (   processing_bad_pieces 
      && SYMBOL_NBR_PIECES(sym) == 2
      && SYMBOL_PIECE_LIST(sym)[0].piece_type == PIECE_LOC_REGISTER
      && SYMBOL_PIECE_LIST(sym)[1].piece_type == PIECE_LOC_REGISTER)
    {
      int i;
      i = (int) SYMBOL_PIECE_LIST(sym)[0].arg1;
      SYMBOL_PIECE_LIST(sym)[0].arg1 = SYMBOL_PIECE_LIST(sym)[1].arg1;
      SYMBOL_PIECE_LIST(sym)[1].arg1 = i;
    }
  assert (stacki == 0);
} // end init_sym_pieces

/* Try to locate the sections we need for DWARF 2 debugging
   information and return true if we have enough to do something.  */

int
dwarf2_has_info (bfd *abfd)
{
  dwarf_info_offset = dwarf_abbrev_offset = dwarf_line_offset 
		= dwarf_info_offset_procs = dwarf_abbrev_offset_procs
		= dwarf_line_offset_actual
#ifdef HP_IA64
		= dwarf_macinfo_offset 
#endif
		= 0;
  dwarf_str_offset = 0;
  dwarf_loc_offset = 0;
  dwarf_line_size = dwarf_line_size_actual = 0;
  bfd_map_over_sections (abfd, dwarf2_locate_sections, NULL);
  if ((dwarf_info_offset && dwarf_abbrev_offset) ||
	(dwarf_info_offset_procs && dwarf_abbrev_offset_procs))
    {
      return 1;
    }
  else
    {
      return 0;
    }
}


static void
dwarf2_get_section_offsets (bfd *abfd)
{ 
  int i = -1; 
  int * ptr = &i;
  bfd_map_over_sections (abfd, dwarf2_locate_section_indices, (char *) ptr);
}

/* As a space optimization some of the debug_info and debug_abbrev
   sections with associated line sections were removed. The line
   sections were left in. 

   n_dbg_sects in dwarf2_doom_psymtab_to_symtab calculates 
   number of debug info sections correctly. We put the value 
   into sectn (a global since the dwarf2_locate_section_indices 
   is passed as a function pointer to bfd_map_over_sections so 
   we can't pass in a parameter to it) 

   We make sure that the array index - i lies between 0
   and sectn. Also the debug_line section following the 
   debug_abbrev section is the one that needs to be added 
   to the array. Hence we add only if it is NULL.

   11/29/04 - Venke pointed out that the abbrev section can come after
     the debug_info.  The standard says it can be shared by several
     debug_info sections.

 */

static void
dwarf2_locate_section_indices (bfd *ignore_abfd, asection *sectp, PTR ptr)
{
  int i = *((int *)ptr);

/* Increment (*(int *)ptr) and save the result in i.  Initialize
 * debug_sections[i].  If we have reached the last section, return.
 */
#define INCREMENT_I    						\
	{							\
	  if (i >= (sectn - 1))					\
	    return;						\
	  i = ++(*(int *)ptr);					\
	  debug_sections[i].abbrev_section = NULL;		\
	  debug_sections[i].abbrev_size = 0;			\
	  debug_sections[i].line_size = 0;			\
	  debug_sections[i].line_section = NULL;		\
	  debug_sections[i].loc_size = 0;			\
	  debug_sections[i].loc_section = NULL;			\
	  debug_sections[i].line_size_actual = 0;		\
	  debug_sections[i].line_section_actual = NULL;		\
	}


/* If i is -1, increment it.  If there is something in sect_ptr, 
 * increment i to make room for the new section pointer.  If we have 
 * reached the last section, return.
 *
 * Note that there is a theoretical possiblity if a .o is missing one
 * of the debug sections and the next .o has that one first, we will 
 * mis-assign that section.  It will be a bit ugly to resolve if it comes
 * up.  I think the sectn limit saves us from this problem. - Michael Coulter
 */
#define CHECK_I(sect_ptr) 						\
    if (i == -1) 							\
      {									\
	INCREMENT_I;							\
      } 								\
    else 								\
      {									\
	if ((sect_ptr)  != NULL) 					\
	  {								\
	    INCREMENT_I;						\
	  }								\
      }

  /* Poorva - check the debugging flag before doing the strcmp's */
  if (sectp->flags & SEC_DEBUGGING)
    {
      if (STREQ (sectp->name, INFO_SECTION))
	{
	  CHECK_I(debug_sections[i].info_section);
	  debug_sections[i].info_section = sectp;
	  debug_sections[i].info_size = 
	  bfd_get_section_size_before_reloc (sectp);
	} 
      else if (STREQ (sectp->name, ABBREV_SECTION))
	{
	  CHECK_I(debug_sections[i].abbrev_section);
	  debug_sections[i].abbrev_section = sectp;
	  debug_sections[i].abbrev_size = 
	    (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if (STREQ (sectp->name, LINE_SECTION))
	{
	  if (i >= 0 && i < sectn && debug_sections[i].line_section == NULL)
	    {
	      debug_sections[i].line_section = sectp;
	      debug_sections[i].line_size = 
	        bfd_get_section_size_before_reloc (sectp);
	    }
	}
      else if (STREQ (sectp->name, LOC_SECTION))
	{
	  if (i >= 0 && i < sectn)
	    {
	      debug_sections[i].loc_section = sectp;
	      debug_sections[i].loc_size = 
		bfd_get_section_size_before_reloc (sectp);
	    }
	}
      else if (STREQ (sectp->name, ACTUAL_LINE_SECTION))
	{
	  if (i >= 0 && i < sectn)
	    {
	      debug_sections[i].line_section_actual = sectp;
	      debug_sections[i].line_size_actual = 
	        bfd_get_section_size_before_reloc (sectp);
	    }
	}
#ifdef HP_IA64
      else if (STREQ (sectp->name, MACINFO_SECTION))
        {
            {
	      CHECK_I(debug_sections[i].dwarf_macinfo_section);
              debug_sections[i].dwarf_macinfo_section = sectp;
              debug_sections[i].mac_size =
                bfd_get_section_size_before_reloc (sectp);
            }
        }
#endif
    }
}


/* This function is mapped across the sections and remembers the
   offset and size of each of the debugging sections we are interested
   in.  */

static void
dwarf2_locate_sections (bfd *ignore_abfd, asection *sectp, PTR ignore_ptr)
{

  /* Poorva - check the debugging flag before doing the strcmp's */
  if (sectp->flags & SEC_DEBUGGING)
    {
      if (STREQ (sectp->name, INFO_SECTION))
	{
	  dwarf_info_offset = sectp->filepos;
	  dwarf_info_size = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if (STREQ (sectp->name, ABBREV_SECTION))
	{
	  dwarf_abbrev_offset = sectp->filepos;
	  dwarf_abbrev_size = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if ((STREQ (sectp->name, INFO_SECTION_PROCS))
	       && (!STREQ (src_no_g_mode_string, "none")))
	{
	  dwarf_info_offset_procs = sectp->filepos;
	  dwarf_info_size_procs = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if ((STREQ (sectp->name, ABBREV_SECTION_PROCS))
	       && (!STREQ (src_no_g_mode_string, "none")))
	{
	  dwarf_abbrev_offset_procs = sectp->filepos;
	  dwarf_abbrev_size_procs = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if (STREQ (sectp->name, LINE_SECTION))
	{
	  dwarf_line_offset = sectp->filepos;
	  dwarf_line_size = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if (STREQ (sectp->name, ACTUAL_LINE_SECTION))
	{
	  dwarf_line_offset_actual = sectp->filepos;
	  dwarf_line_size_actual = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
      else if (STREQ (sectp->name, LOC_SECTION))
	{
	  dwarf_loc_offset = sectp->filepos;
	  dwarf_loc_size = bfd_get_section_size_before_reloc (sectp);
	}
      else if (STREQ (sectp->name, STR_SECTION))
	{
	  dwarf_str_offset = sectp->filepos;
	  dwarf_str_size = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
#ifdef HP_IA64
      else if (STREQ (sectp->name, MACINFO_SECTION))
	{
	  dwarf_macinfo_offset = sectp->filepos;
	  dwarf_macinfo_size = (unsigned int) bfd_get_section_size_before_reloc (sectp);
	}
#endif
    }
}

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
/* This function read the .notes section of an object file 
   and returns the optimization level asscociated with it.
   Input is : filename and compilation dir.
   Included comparison of directories to avoid incorrectly
   matching dir1/a.c and dir1/dir2/a.c 
*/   
static boolean
get_compile_line (char * filename, char *compdir, struct objfile * objfile)
{
  char * stringtab, *p, * basefilename;
  int i, j, k = 0, size, basefilenamelen, index, compdirlen;
  boolean found = false;

  /* The string table contains information for each compilation unit,
     including the base name of a file, its directory, ecom_option and
     driver_command.

     The driver_command records the compiler and options used to create the
     original executables.

     For example :
     224          t.c
     /tmp/hmc
     ecom options =  -g -Oq00,al,ag,cn
     driver_command =   /opt/ansic/bin/cc -g
  */
  stringtab = elf_symtab_read_notes (objfile->obfd, &size);

  if (p = strrchr (filename, '/'))
    {
      basefilename = p + 1;
    }
  else
    basefilename = filename;
 
  basefilenamelen = strlen (basefilename);

  compdirlen = strlen (compdir);

  for (i = 0; i < size && !found; i++)
    {
      /* Look for the file name in the string table. */
      if ((stringtab[i] == basefilename[0]) && 
          (!strncmp (&stringtab[i], basefilename, basefilenamelen))
           && stringtab[i+ basefilenamelen] == '\n')
        {
          if (!strncmp (&stringtab[i + basefilenamelen + 1], 
              compdir, compdirlen))
            {
              found = true;
              break;
            }
        }
    }

  if (!found && (i == size))
    goto done;

  for (j = i - 1; j >= 0; j--)
    {
      if (stringtab[j] == '-' && stringtab[j+1] == 'O')
        {
          if (stringtab[j+2] == 'q' && stringtab[j+3] == '0')
            if (stringtab[j+4] == '1' || stringtab[j+4] == '0')
              {
                found = true;
                break;
              }
            else
              {
                found = false;
                break;
              }
        }
    }
  done:
    free (stringtab); 
    return found;
}

/* To enable us place breakpoints on inline functions, 
   read the minimal line table associated with a.out. 
   Process them and update the data structure called "inlinee_table" 
   which is maintained by GDB to keep track of inlined instances
   Right now we dont read the minimal line table for any
   shared libraries due to relocation issues.
*/

void dwarf2_decode_minimal (struct objfile *objfile)
{
  /* Saved values for dwarf variables */
  char *dwarf_info_buffer_saved;
  char *dwarf_abbrev_buffer_saved;
  unsigned int dwarf_info_size_saved;
  unsigned int dwarf_abbrev_size_saved;
  file_ptr dwarf_info_offset_saved;
  file_ptr dwarf_abbrev_offset_saved;
  
  char *sibling;
  struct die_info *dies, *child_die;
  bfd *abfd = objfile->obfd;
  char *info_ptr;
  char *beg_of_comp_unit = 0; /* initialize for compiler warning */
  struct comp_unit_head cu_header_struct = {0}; /* init for compiler warning */
  struct partial_die_info comp_unit_die;
  struct partial_die_info dummy;

  struct cleanup *back_to;
  int comp_unit_has_pc_info;
  CORE_ADDR lowpc, highpc;
  int comp_unit_die_has_sibling = 0;
  unsigned int dwarf_abbrev_size_pst = 0;
  char * line_ptr = 0;
  int i = 0;

  /* For decode lines */
  char *name = "<unknown>";
  char *prof_flag;
  char *comp_dir = NULL;
  unsigned int line_offset = 0;
  struct attribute *attr;
  CORE_ADDR baseaddr_saved; 
  /* Have a local temp die_ref_table only for use in this function. */
  struct die_info **local_die_ref_table = (struct die_info **) 
                         xmalloc (REF_HASH_SIZE * sizeof (struct die_info *));
  die_ref_table = local_die_ref_table;

  /* Sanity check for JAGag045062. */
  memset (&dummy, 0, sizeof (struct partial_die_info));
  /* The is_external field is always set to -1 before reading 
     a partial die, and here as we want to compare the null
     sibling comp_unit_die, set this field to -1 in dummy. */ 
  dummy.is_external = -1;

  dwarf2_read_minimal = false;

  /* JAGag26806 - For shared libraries, baseaddr
     contains the relocated value. 
     This indicates that every symbol which is read 
     in must be relocated by this much.
 */
     
  baseaddr_saved = baseaddr;
  baseaddr = ANOFFSET (objfile->section_offsets, 0);

  /* Read in the .debug_procs_info and .debug_procs_abbrev */
  if (dwarf_info_offset_procs) /* check if section present */
    dwarf_info_buffer_procs = dwarf2_read_section (objfile,
                                                   dwarf_info_offset_procs,
                                                   dwarf_info_size_procs);
  if (dwarf_abbrev_offset_procs) /* check if section present */
    dwarf_abbrev_buffer_procs = dwarf2_read_section (objfile,
                                                     dwarf_abbrev_offset_procs,
                                                     dwarf_abbrev_size_procs);

  if (dwarf_line_offset != 0) /*read in only if we have line section */
    dwarf_line_buffer = dwarf2_read_section (objfile,
					     dwarf_line_offset,
					     dwarf_line_size);

  /* save dwarf info and dwarf abbrev variables */
  dwarf_info_buffer_saved = dwarf_info_buffer;
  dwarf_abbrev_buffer_saved = dwarf_abbrev_buffer;
  dwarf_info_size_saved = dwarf_info_size;
  dwarf_abbrev_size_saved = dwarf_abbrev_size;
  dwarf_info_offset_saved = dwarf_info_offset;
  dwarf_abbrev_offset_saved = dwarf_abbrev_offset;

  /* use procs sections as dwarf info and abbrev sections */
  dwarf_info_buffer = dwarf_info_buffer_procs;
  dwarf_abbrev_buffer = dwarf_abbrev_buffer_procs;
  dwarf_info_size = dwarf_info_size_procs;
  dwarf_abbrev_size = dwarf_abbrev_size_procs;
  dwarf_info_offset = dwarf_info_offset_procs;
  dwarf_abbrev_offset = dwarf_abbrev_offset_procs;

  info_ptr = dwarf_info_buffer;

  obstack_init (&dwarf2_tmp_obstack);
  back_to = make_cleanup (dwarf2_free_tmp_obstack, NULL);

  while ((unsigned int) (info_ptr - dwarf_info_buffer)
	 + ((info_ptr - dwarf_info_buffer) % 4) < dwarf_info_size)
    {
      subfiles = NULL;   //Assign subfiles to Null every time you read MLT
      if (!comp_unit_die_has_sibling)
        {
          beg_of_comp_unit = info_ptr;
          cu_header_struct.length = read_4_bytes (abfd, info_ptr);
          info_ptr += 4;
          cu_header_struct.version = read_2_bytes (abfd, info_ptr);
          info_ptr += 2;
          cu_header_struct.abbrev_offset = read_4_bytes (abfd, info_ptr);
          info_ptr += 4;
          cu_header_struct.addr_size = read_1_byte (abfd, info_ptr);
          info_ptr += 1;
          address_size = cu_header_struct.addr_size;

          if (cu_header_struct.version != 2)
	    {
              warning (dwarf_error_mesg,  (long) cu_header_struct.version,
                       objfile->name, objfile->name);
	      goto done;
	    }
          if (cu_header_struct.abbrev_offset >= dwarf_abbrev_size)
	    {
	      warning ("DWARF Error: bad offset (0x%lx) in compilation unit "
                       "header (offset 0x%lx + 6) of %s.",
		    (long) cu_header_struct.abbrev_offset,
		    (long) (beg_of_comp_unit - dwarf_info_buffer),
		    objfile->name);
	      goto done;
	    }
          if (beg_of_comp_unit + cu_header_struct.length + 4
	      > dwarf_info_buffer + dwarf_info_size)
	    {
	      warning ("DWARF Error: bad length (0x%lx) in compilation unit "
                       "header (offset 0x%lx + 0) of %s.",
		   (long) cu_header_struct.length,
		   (long) (beg_of_comp_unit - dwarf_info_buffer),
		   objfile->name);
	      goto done;
	    }
            
          dwarf_abbrev_size_pst =  dwarf2_read_abbrevs (abfd, &cu_header_struct);
          make_cleanup (dwarf2_empty_abbrev_table, NULL);
        }

#ifdef USING_MMAP
      /* These variables are set in read_partial_die by looking at the
         DW_AT_stmt_list and DW_AT_HP_actuals_stmt_list of comp_unit_die.
         We shall be using them to find the offset of the line section in
         the file and then reading in that part of the line sections that
         pertain to this .o only */
      dwarf_line_offset_pst = 0;
      dwarf_line_offset_actual_pst = 0;
#endif
      /* Set cu_header_offset before calling read_partial_die where it is
      used. */
      cu_header_offset = (unsigned int) (beg_of_comp_unit - dwarf_info_buffer);

      /* Read the compilation unit die */
      read_partial_die (&comp_unit_die, abfd, info_ptr, &comp_unit_has_pc_info,
							&cu_header_struct);
      /* compare the sibling compilation unit die to see whether it is null. 
         Sanity check for JAGag045062. 
      */
      if (comp_unit_die_has_sibling &&
          memcmp (&comp_unit_die, &dummy, sizeof (struct partial_die_info)) == 0)
        {
          comp_unit_die_has_sibling = 0;
          info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
          continue;
        }

      dies = read_comp_unit (info_ptr, abfd, &cu_header_struct);

      sibling = NULL;
      do
        {

          /* If it is a non -g executable, then dont take the pain of reading the
             minimal line table as inline information are not emitted without -g
          */
          attr = dwarf_attr (dies, DW_AT_producer);
          if (attr)
            {
              prof_flag = DW_STRING (attr);
              if (prof_flag[0] == 'G' && prof_flag[1] == '\0')
                continue;
            }

          attr = dwarf_attr (dies, DW_AT_name);
          if (attr)
            {
              name = DW_STRING (attr);
            }

          attr = dwarf_attr (dies, DW_AT_comp_dir);
          if (attr)
            {
              comp_dir = DW_STRING (attr);
            }

          /* If your executable is compiled with +O2 and above, just continue without
             reading the line table because right now we dont support them.
          */

          if (!get_compile_line (name, comp_dir, objfile)) 
            continue; 

          start_subfile (name, comp_dir);

          attr = dwarf_attr (dies, DW_AT_stmt_list);
          if (attr)
            {
              reading_MLT = true;
              line_offset = (unsigned int) DW_UNSND (attr);
              dwarf_decode_lines (line_offset, comp_dir, abfd);
            }
       
          if (processing_doom_objfile)
            {
              sibling = comp_unit_die.sibling;
              if (sibling)
                {
                  comp_unit_die_has_sibling = 1;
                  continue;
                }
            }
        }
      while (sibling);

      comp_unit_die_has_sibling = 0;
      info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
    }

  if (reading_MLT == false)
    {
      inline_debugging = ON;
      inl_debug = "on";
      printf_filtered ("\nWarning: The executable is either not compiled with -g or compiled with +O2 and above\n"
               "Inline breakpoint is not available "
               "Setting inline-debug to 'on' now\n");
    }

  done: 
    current_subfile = NULL;
    dwarf_info_buffer = dwarf_info_buffer_saved;
    dwarf_abbrev_buffer = dwarf_abbrev_buffer_saved;
    dwarf_info_size = dwarf_info_size_saved;
    dwarf_abbrev_size = dwarf_abbrev_size_saved;
    dwarf_info_offset = dwarf_info_offset_saved;
    dwarf_abbrev_offset = dwarf_abbrev_offset_saved;
    current_objfile = NULL;

/* Unmap */
#ifdef USING_MMAP
    /* we can now unmap the debug sections of the executable */
    dwarf2_unmap_section (dwarf_info_buffer_procs, dwarf_info_offset_procs, dwarf_info_size_procs);
    dwarf2_unmap_section (dwarf_abbrev_buffer_procs, dwarf_abbrev_offset_procs, 
                          dwarf_abbrev_size_procs);
    dwarf2_unmap_section (dwarf_line_buffer, dwarf_line_offset, dwarf_line_size);
#endif

    free (subfiles);
    free_die_ref_table ((void **) local_die_ref_table);
    die_ref_table = NULL;
    dwarf2_read_minimal = true;
    reading_MLT = false;
    baseaddr = baseaddr_saved;
    do_cleanups (back_to);
}
#endif

/* Build a partial symbol table.  */

void
dwarf2_build_psymtabs (struct objfile *objfile, int mainline)
{
  /* Saved values for dwarf variables */
  char *dwarf_info_buffer_saved;
  char *dwarf_abbrev_buffer_saved;
  unsigned int dwarf_info_size_saved;
  unsigned int dwarf_abbrev_size_saved;
  file_ptr dwarf_info_offset_saved;
  file_ptr dwarf_abbrev_offset_saved;

  /* Initialize all the buffers to 0. */
  dwarf_info_buffer = 0;
  dwarf_abbrev_buffer = 0;
  dwarf_line_buffer = 0;
  dwarf_line_buffer_actual = 0;
  dwarf_str_buffer = 0;
  dwarf_info_buffer_procs = 0;
  dwarf_abbrev_buffer_procs = 0;
#ifdef HP_IA64
  dwarf_macinfo_buffer = 0;
#endif
  
  /* We have already built psymtabs for this - go back */
  if (objfile->psymtabs)
    return;

  current_objfile = objfile;  

  /* We definitely need the .debug_info and .debug_abbrev sections */
  if (dwarf_info_offset && dwarf_abbrev_offset)
    {
#ifdef USING_MMAP
      if (!objfile->sym_private)
	{
	  /* We need to do this here since loading a shared library does not
	     call dwarf2_symfile_init which is where the malloc is done
	     for the exe. We store the offsets of the debug sections from the
	     beginning of the file in the objfile->sym_private structure and
	     hence need to have space malloc'd for it
	     allocate_objfile memsets objfile to 0 so we can check first*/
	  objfile->sym_private = (PTR)
	    xmmalloc (objfile->md, sizeof (struct hpread_symfile_info));
      
	  memset (objfile->sym_private, 0, sizeof (struct hpread_symfile_info));
	}
#endif

      processing_doom_objfile = 0;

      dwarf_info_buffer = dwarf2_read_section (objfile,
					       dwarf_info_offset,
					       dwarf_info_size);
      dwarf_abbrev_buffer = dwarf2_read_section (objfile,
						 dwarf_abbrev_offset,
						 dwarf_abbrev_size);

      if (dwarf_line_offset != 0) /*read in only if we have line section */
        dwarf_line_buffer = dwarf2_read_section (objfile,
					         dwarf_line_offset,
					         dwarf_line_size);

      /* The actual line table is HP specific and does not exist for
         GCC/G++ executables, thus only look for it if the offset is 
         nonzero. - JAGae15364 */
      if (dwarf_line_offset_actual != 0)
        {
          dwarf_line_buffer_actual = dwarf2_read_section (objfile,
					       dwarf_line_offset_actual,
					       dwarf_line_size_actual);
        }

      if (dwarf_str_offset)
         dwarf_str_buffer = dwarf2_read_section (objfile,
                                                 dwarf_str_offset,
                                                 dwarf_str_size);
      else
         dwarf_str_buffer = NULL;

#ifdef HP_IA64
      if (dwarf_macinfo_offset)
        dwarf_macinfo_buffer = dwarf2_read_section (objfile, 
						    dwarf_macinfo_offset,
						    dwarf_macinfo_size);
      else
	dwarf_macinfo_buffer = NULL;
#endif

      if (dwarf_loc_offset)
        dwarf_loc_buffer = dwarf2_read_section (objfile,
    					    dwarf_loc_offset,
    					    dwarf_loc_size);
      else
        dwarf_loc_buffer = NULL;

      if (mainline || objfile->global_psymbols.size == 0 ||
	  objfile->static_psymbols.size == 0)
	{
	  init_psymbol_list (objfile, 1024);
	}

	/* only test this case for now */
	{
	  /* In this case we have to work a bit harder */
	  dwarf2_build_psymtabs_hard (objfile, mainline);
	}
    }

  if (doom_executable)
    {
      build_doom_tables(objfile);
      /* Poorva: We should be able to use the same routine */
      hpread_build_doom_psymtabs(objfile, objfile->section_offsets, mainline);
    }

  current_objfile = NULL;
  /* rven: Build small psymtabs for nodebug case */
  /* section_offsets is a bogus parameter */
  /* save and restore dwarf_info_buffer and dwarf_abbrev_buffer */
  /* Lots of info is passed through globals, we need to clean up this
     routine NOW */
  if (!(dwarf_info_offset_procs && dwarf_abbrev_offset_procs))
    return; /* No procs sections */
  if (STREQ (src_no_g_mode_string, "none"))
    return;	/* No source level debugging without -g */
  if ((STREQ (src_no_g_mode_string, "no_sys_libs"))
    && (strstr(objfile->name,"/usr/lib")))
    return; /* No source level debugging without -g for system libraries */

  current_objfile = objfile;

  /* Poorva: Read in these sections only if we want to do src-no-g debugging.
     They will be freed in dwarf2_build_psymtabs_hard */
  if (dwarf_info_offset_procs) /* check if section present */
    dwarf_info_buffer_procs = dwarf2_read_section (objfile,
                                                   dwarf_info_offset_procs,
                                                   dwarf_info_size_procs);
  if (dwarf_abbrev_offset_procs) /* check if section present */
    dwarf_abbrev_buffer_procs = dwarf2_read_section (objfile,
                                                     dwarf_abbrev_offset_procs,
                                                     dwarf_abbrev_size_procs);

  if (dwarf_line_offset != 0) /*read in only if we have line section */
    dwarf_line_buffer = dwarf2_read_section (objfile,
					     dwarf_line_offset,
					     dwarf_line_size);

  /* The actual line table is HP specific and does not exist for
     GCC/G++ executables, thus only look for it if the offset is 
     nonzero. - JAGae15364 */
  if (dwarf_line_offset_actual != 0)
    {
      dwarf_line_buffer_actual = dwarf2_read_section (objfile,
			                              dwarf_line_offset_actual,
					              dwarf_line_size_actual);
    }

#ifdef HP_IA64
      if (dwarf_macinfo_offset != 0)
        dwarf_macinfo_buffer = dwarf2_read_section (objfile, 
						    dwarf_macinfo_offset,
						    dwarf_macinfo_size);
      else
	dwarf_macinfo_buffer = NULL;
#endif

  /* save dwarf info and dwarf abbrev variables */
  dwarf_info_buffer_saved = dwarf_info_buffer;
  dwarf_abbrev_buffer_saved = dwarf_abbrev_buffer;
  dwarf_info_size_saved = dwarf_info_size;
  dwarf_abbrev_size_saved = dwarf_abbrev_size;
  dwarf_info_offset_saved = dwarf_info_offset;
  dwarf_abbrev_offset_saved = dwarf_abbrev_offset;

  /* use procs sections as dwarf info and abbrev sections */
  dwarf_info_buffer = dwarf_info_buffer_procs ;
  dwarf_abbrev_buffer = dwarf_abbrev_buffer_procs ;
  dwarf_info_size = dwarf_info_size_procs ;
  dwarf_abbrev_size = dwarf_abbrev_size_procs ;
  dwarf_info_offset = dwarf_info_offset_procs;
  dwarf_abbrev_offset = dwarf_abbrev_offset_procs;

  /* Build psymtabs, for a procs section (mainline = -1) */
  dwarf2_build_psymtabs_hard (objfile, -1);

  /* restore dwarf info and abbrev variables from saved values : needed ? */
  dwarf_info_buffer = dwarf_info_buffer_saved ;
  dwarf_abbrev_buffer = dwarf_abbrev_buffer_saved ;
  dwarf_info_size = dwarf_info_size_saved ;
  dwarf_abbrev_size = dwarf_abbrev_size_saved ;
  dwarf_info_offset = dwarf_info_offset_saved;
  dwarf_abbrev_offset = dwarf_abbrev_offset_saved;
  current_objfile = NULL;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  /* Reinitialize this variable to false every time you build a psymtab. */
  dwarf2_read_minimal = false;
  if ((inline_debugging == BP_ALL || 
      inline_debugging == BP_IND))
      dwarf2_decode_minimal (objfile);
#endif
}

/* Build the partial symbol table by doing a quick pass through the
   .debug_info and .debug_abbrev sections.  */
/* Poorva: For DOOM in HP_IA64 code added here should be replicated in 
   find_address_in_debug_section, but taking care that scan_partial_symbols
   doesn't add partial_symbols for DOOM. */

static void
dwarf2_build_psymtabs_hard (struct objfile *objfile, int mainline)
{
  /* Instead of reading this into a big buffer, we should probably use
     mmap()  on architectures that support it. (FIXME) */
  bfd *abfd = objfile->obfd;
  char *info_ptr;
  char *beg_of_comp_unit = 0; /* initialize for compiler warning */
  struct comp_unit_head cu_header_struct = {0}; /* init for compiler warning */
  struct partial_die_info comp_unit_die;
  struct partial_die_info dummy;
  struct partial_symtab *pst;
  struct cleanup *back_to;
  int comp_unit_has_pc_info;
  CORE_ADDR lowpc, highpc;
  int comp_unit_die_has_sibling = 0;
  unsigned int dwarf_abbrev_size_pst = 0;
  char * line_ptr = 0;
  
  memset (&dummy, 0, sizeof (struct partial_die_info));
  /* The is_external field is always set to -1 before reading 
     a partial die, and here as we want to compare the null
     sibling comp_unit_die, set this field to -1 in dummy. */ 
  dummy.is_external = -1;
#ifdef USING_MMAP
  /* Make sure that sym_private is allocated. */
  if (!objfile->sym_private)
    {
      objfile->sym_private = (PTR)
        xmmalloc (objfile->md, sizeof (struct hpread_symfile_info));

      memset (objfile->sym_private, 0, sizeof (struct hpread_symfile_info));
    }
  /* Set the offsets of the debug sections for the objfile (exe/shlib) here */
  DEBUG_LINE_FILE_START (objfile) = dwarf_line_offset;
  DEBUG_LINE_ACTUAL_FILE_START (objfile) = dwarf_line_offset_actual;
#endif
  info_ptr = dwarf_info_buffer;

  obstack_init (&dwarf2_tmp_obstack);
  back_to = make_cleanup (dwarf2_free_tmp_obstack, NULL);

  while ((unsigned int) (info_ptr - dwarf_info_buffer)
	 + ((info_ptr - dwarf_info_buffer) % 4) < dwarf_info_size)
    {
      if (!comp_unit_die_has_sibling)
      {
      beg_of_comp_unit = info_ptr;
      cu_header_struct.length = read_4_bytes (abfd, info_ptr);
      info_ptr += 4;
      cu_header_struct.version = read_2_bytes (abfd, info_ptr);
      info_ptr += 2;
      cu_header_struct.abbrev_offset = read_4_bytes (abfd, info_ptr);
      info_ptr += 4;
      cu_header_struct.addr_size = read_1_byte (abfd, info_ptr);
      info_ptr += 1;
      address_size = cu_header_struct.addr_size;

      if (cu_header_struct.version != 2)
	{
          warning (dwarf_error_mesg,  (long) cu_header_struct.version,
                   objfile->name, objfile->name);
	  return;
	}
      if (cu_header_struct.abbrev_offset >= dwarf_abbrev_size)
	{
	  warning ("DWARF Error: bad offset (0x%lx) in compilation unit header (offset 0x%lx + 6) of %s.",
		 (long) cu_header_struct.abbrev_offset,
		 (long) (beg_of_comp_unit - dwarf_info_buffer),
		 objfile->name);
	  return;
	}
      if (beg_of_comp_unit + cu_header_struct.length + 4
	  > dwarf_info_buffer + dwarf_info_size)
	{
	  warning ("DWARF Error: bad length (0x%lx) in compilation unit header (offset 0x%lx + 0) of %s.",
		 (long) cu_header_struct.length,
		 (long) (beg_of_comp_unit - dwarf_info_buffer),
		 objfile->name);
	  return;
	}
      /* For ILP32 IA64, address_size is 4, but address_significant_size is 8 */
      /* Read the abbrevs for this compilation unit into a table */
      /* dwarf2_read_abbrevs now returns the size of the abbrevs it read in
         for this comp unit so we can store the value in the pst. None of the
         comp unit fields have this information.*/

      dwarf_abbrev_size_pst =  dwarf2_read_abbrevs (abfd, &cu_header_struct);
      
      make_cleanup (dwarf2_empty_abbrev_table, NULL);
      }

#ifdef USING_MMAP
      /* These variables are set in read_partial_die by looking at the
         DW_AT_stmt_list and DW_AT_HP_actuals_stmt_list of comp_unit_die.
         We shall be using them to find the offset of the line section in
         the file and then reading in that part of the line sections that
         pertain to this .o only */
      dwarf_line_offset_pst = 0;
      dwarf_line_offset_actual_pst = 0;
#endif
      /* Set cu_header_offset before calling read_partial_die where it is
	 used. */
      cu_header_offset = (unsigned int) (beg_of_comp_unit - dwarf_info_buffer);
      /* Read the compilation unit die */
      info_ptr = read_partial_die (&comp_unit_die, abfd, info_ptr, 
				   &comp_unit_has_pc_info, &cu_header_struct);

      /* compare the sibling compilation unit die to see whether it is null. 
         Sanity check for JAGag045062. 
      */
      if (comp_unit_die_has_sibling &&
          memcmp (&comp_unit_die, &dummy, sizeof (struct partial_die_info)) == 0)
        {
          comp_unit_die_has_sibling = 0;
          info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
          continue;
        }

      /* Set the language we're debugging */
      set_cu_language (comp_unit_die.language);

      /* Allocate a new partial symbol table structure */
      /* rven: Need to add in file names if we are doing procs_info, get it
	from the logical line table */
      if (mainline != -1)
        pst = start_psymtab_common (objfile, objfile->section_offsets,
				  comp_unit_die.name ? comp_unit_die.name : "",
				  comp_unit_die.dirname ? comp_unit_die.dirname : "",
				  comp_unit_die.lowpc,
				  objfile->global_psymbols.next,
				  objfile->static_psymbols.next);
      else
      { /* Use procs info to build psymtab if file name exists and is unique */
        if ((pst = procs_start_psymtab_common(objfile, objfile->section_offsets,
				&comp_unit_die, &cu_header_struct)) == NULL) 
          {
	    comp_unit_die_has_sibling = 0;
            info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
            continue;
          }
      }
      pst->language = cu_language;
      objfile->current_pst = pst;
#ifdef HP_IA64
      pst->die_ref_table = NULL;
#endif
      pst->read_symtab_private = (char *)
	obstack_alloc (&objfile->psymbol_obstack, (int) sizeof (struct dwarf2_pinfo));
      memset (pst->read_symtab_private, 0, sizeof (struct dwarf2_pinfo *));


#ifndef USING_MMAP
      DWARF_INFO_BUFFER (pst) = dwarf_info_buffer;
      DWARF_INFO_OFFSET (pst) = beg_of_comp_unit - dwarf_info_buffer;
      DWARF_ABBREV_BUFFER (pst) = dwarf_abbrev_buffer;
      DWARF_ABBREV_SIZE (pst) = dwarf_abbrev_size;
      DWARF_LINE_BUFFER (pst) = dwarf_line_buffer;
      DWARF_LINE_BUFFER_ACTUAL (pst) = dwarf_line_buffer_actual;
      DWARF_COMP_UNIT_OFFSET (pst) = comp_unit_die.offset;
#else
      DEBUG_INFO_FILE_START (pst) = dwarf_info_offset;
      DEBUG_ABBREV_FILE_START (pst) = dwarf_abbrev_offset;
      DWARF_INFO_OFFSET (pst) = beg_of_comp_unit - dwarf_info_buffer;
      /* We get the dwarf info size and abbrev offset from the comp
         unit header. The comp_unit_head does not include the size
         of the field that contains the length so add that to be accurate.*/
      DWARF_INFO_SIZE (pst) = cu_header_struct.length + 4;
      DWARF_ABBREV_OFFSET (pst) = cu_header_struct.abbrev_offset;
      DWARF_ABBREV_SIZE (pst) = dwarf_abbrev_size_pst;

      /* Decode line number information if present.  */
      /* The size of the line table is more involved. Read the first 4 bytes
         of the line table to get the size of it and add the size of the
         field that contains it. */
      DWARF_LINE_OFFSET (pst) = dwarf_line_offset_pst;
      if (dwarf_line_buffer)
        {
          line_ptr = dwarf_line_buffer
                     + DWARF_LINE_OFFSET (pst);
          DWARF_LINE_SIZE (pst) = read_4_bytes (abfd, line_ptr)
                                  + TOTAL_LENGTH_FIELD_LENGTH;
        }

      /* Decode actuals line number information if present.  */
      DWARF_LINE_SIZE_ACTUAL (pst) = 0;
      DWARF_LINE_OFFSET_ACTUAL (pst) = dwarf_line_offset_actual_pst;
      if (dwarf_line_buffer_actual)
        {
          line_ptr = dwarf_line_buffer_actual
	    + DWARF_LINE_OFFSET_ACTUAL (pst);
          DWARF_LINE_SIZE_ACTUAL (pst) = read_4_bytes (abfd, line_ptr)
                                         + TOTAL_LENGTH_FIELD_LENGTH;
        }

      /* The offset of the comp unit is the offset from the start of this
       comp unit. For fortran one may have many comp units nested within
       each other or just siblings of each other. These extra comp units
       do not have associated comp unit headers. So their offset needs to be
       calculated from the cu_header_struct that they fall under.
       */
      DWARF_COMP_UNIT_OFFSET (pst) = comp_unit_die.offset - cu_header_offset;
#endif

      DWARF_STR_BUFFER (pst) = dwarf_str_buffer;
      DWARF_STR_SIZE (pst) = dwarf_str_size;
#ifdef HP_IA64
      DWARF_MACINFO_BUFFER(pst) = dwarf_macinfo_buffer;
      DWARF_MACINFO_SIZE(pst) = dwarf_macinfo_size;
#endif
      DWARF_LOC_BUFFER (pst) = dwarf_loc_buffer;
      DWARF_LOC_SIZE (pst) = dwarf_loc_size;
      baseaddr = ANOFFSET (objfile->section_offsets, 0);
      data_baseaddr = ANOFFSET (objfile->section_offsets, 1);

      /* Store the function that reads in the rest of the symbol table */
      pst->read_symtab = dwarf2_psymtab_to_symtab;

      /* Check if comp unit has_children.
         If so, read the rest of the partial symbols from this comp unit.
         If not, there's no more debug_info for this comp unit. */
      if (comp_unit_die.has_children)
	{
	  info_ptr = scan_partial_symbols (info_ptr, objfile, &lowpc,
					   &highpc, &cu_header_struct);

	  /* If the compilation unit didn't have an explicit address range,
	     then use the information extracted from its child dies.  */
	  if (!comp_unit_has_pc_info)
	    {
	      comp_unit_die.lowpc = lowpc;
	      comp_unit_die.highpc = highpc;
	    }
	}
      pst->textlow = SWIZZLE(comp_unit_die.lowpc + baseaddr);
      pst->texthigh = SWIZZLE(comp_unit_die.highpc + baseaddr);

      pst->n_global_syms = (int) (objfile->global_psymbols.next -
	(objfile->global_psymbols.list + pst->globals_offset));
      pst->n_static_syms = (int) (objfile->static_psymbols.next -
	(objfile->static_psymbols.list + pst->statics_offset));
      sort_pst_symbols (pst);

      /* If there is already a psymtab or symtab for a file of this
         name, remove it. (If there is a symtab, more drastic things
         also happen.) This happens in VxWorks.  */
      free_named_symtabs (pst->filename);

      if (comp_unit_die.sibling)
	{
	  comp_unit_die_has_sibling = 1;
	  continue;
	}

      comp_unit_die_has_sibling = 0;
      info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
    }

#ifdef USING_MMAP
  /* Since we are done building our psymtabs - we can now unmap the
     debug sections of the executable */
  dwarf2_unmap_section (dwarf_info_buffer, dwarf_info_offset, dwarf_info_size);
  dwarf2_unmap_section (dwarf_abbrev_buffer, dwarf_abbrev_offset,
                        dwarf_abbrev_size);
  dwarf2_unmap_section (dwarf_line_buffer, dwarf_line_offset, dwarf_line_size);
  if (dwarf_line_size_actual)
    dwarf2_unmap_section (dwarf_line_buffer_actual, dwarf_line_offset_actual,
                          dwarf_line_size_actual);
#endif
  objfile->current_pst = NULL;
  do_cleanups (back_to);
}


/* Used by DOOM to find a text address inside each debug section */

static void
find_address_in_debug_section (struct objfile *objfile, struct section_offsets *section_offsets,
			       struct partial_symtab *pst)
{
  /* Instead of reading this into a big buffer, we should probably use
     mmap()  on architectures that support it. (FIXME) */
  bfd *abfd = objfile->obfd;
  char *info_ptr;
  char *beg_of_comp_unit = 0; /* initialize for compiler warning */
  struct comp_unit_head cu_header_struct = {0}; /* init for compiler warning */
  struct partial_die_info comp_unit_die;
  struct partial_die_info dummy;
  struct cleanup *back_to;
  int comp_unit_has_pc_info;
  CORE_ADDR lowpc, highpc;
  int comp_unit_die_has_sibling = 0;

  memset (&dummy, 0, sizeof (struct partial_die_info));
  /* The is_external field is always set to -1 before reading 
     a partial die, and here as we want to compare the null
     sibling comp_unit_die, set this field to -1 in dummy. */ 
  dummy.is_external = -1;

#ifndef HP_IA64
  /* Number of bytes of any addresses that are signficant */
  address_significant_size = get_elf_backend_data (abfd)->s->arch_size / 8;
#endif
 
  info_ptr = dwarf_info_buffer;
  
  obstack_init (&dwarf2_tmp_obstack);
  back_to = make_cleanup (dwarf2_free_tmp_obstack, NULL);
  
  while ((unsigned int) (info_ptr - dwarf_info_buffer)
	 + ((info_ptr - dwarf_info_buffer) % 4) < dwarf_info_size)
    {
      if (!comp_unit_die_has_sibling)
	{
	  beg_of_comp_unit = info_ptr;
	  cu_header_struct.length = read_4_bytes (abfd, info_ptr);
	  info_ptr += 4;
	  cu_header_struct.version = read_2_bytes (abfd, info_ptr);
	  info_ptr += 2;
	  cu_header_struct.abbrev_offset = read_4_bytes (abfd, info_ptr);
	  info_ptr += 4;
	  cu_header_struct.addr_size = read_1_byte (abfd, info_ptr);
	  info_ptr += 1;
	  address_size = cu_header_struct.addr_size;
	  
	  if (cu_header_struct.version != 2)
	    {
              warning (dwarf_error_mesg,  (long) cu_header_struct.version,
                       objfile->name, objfile->name);
	      return;
	    }
	  if (cu_header_struct.abbrev_offset >= dwarf_abbrev_size)
	    {
	      warning ("DWARF Error: bad offset (0x%lx) in compilation unit header (offset 0x%lx + 6) of %s.",
		 (long) cu_header_struct.abbrev_offset,
		 (long) (beg_of_comp_unit - dwarf_info_buffer),
		 objfile->name);
	      return;
	    }
	  if (beg_of_comp_unit + cu_header_struct.length + 4
	      > dwarf_info_buffer + dwarf_info_size)
	    {
	      warning ("DWARF Error: bad length (0x%lx) in compilation unit header (offset 0x%lx + 0) of %s.",
		 (long) cu_header_struct.length,
		 (long) (beg_of_comp_unit - dwarf_info_buffer),
		 objfile->name);
	      return;
	    }
	  /* For ILP32 IA64, address_size is 4, but address_significant_size is 8 */
#ifndef HP_IA64
	  if (address_size < address_significant_size)
	    {
	      warning ("DWARF Error: bad address size (%ld) in compilation unit header (offset 0x%lx + 11) of %s.",
		     (long) cu_header_struct.addr_size,
		 (long) (beg_of_comp_unit - dwarf_info_buffer),
		 objfile->name);
	    }
#endif
	  
	  /* Read the abbrevs for this compilation unit into a table */
	  dwarf2_read_abbrevs (abfd, &cu_header_struct);
	  
	}
      /* Set cu_header_offset before calling read_partial_die where it is
	 used. */
      cu_header_offset = (unsigned int) (beg_of_comp_unit - dwarf_info_buffer);
      
      /* Read the compilation unit die */
      info_ptr = read_partial_die (&comp_unit_die, abfd, info_ptr, 
				   &comp_unit_has_pc_info, &cu_header_struct);

      /* compare the sibling compilation unit die to see whether it is null. 
         Sanity check for JAGag045062.
      */ 
      if (comp_unit_die_has_sibling &&
          memcmp (&comp_unit_die, &dummy, sizeof (struct partial_die_info)) == 0)
        {
          comp_unit_die_has_sibling = 0;
          info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
          continue;
        }

      /* Set the language we're debugging */
      set_cu_language (comp_unit_die.language);
      pst->read_symtab_private = (char *)
        obstack_alloc (&objfile->psymbol_obstack, 
		       (int)(sizeof (struct dwarf2_pinfo)));
      memset (pst->read_symtab_private, 0, sizeof (struct dwarf2_pinfo *));
      DWARF_COMP_UNIT_OFFSET (pst) = comp_unit_die.offset;
      debug_sections[sectn].cu_header_offset = cu_header_offset;      

      baseaddr = ANOFFSET (section_offsets, 0);
      data_baseaddr = ANOFFSET (section_offsets, 1);

      /* Check if comp unit has_children.
         If so, read the rest of the partial symbols from this comp unit.
         If not, there's no more debug_info for this comp unit. */
      if (comp_unit_die.has_children)
	info_ptr = scan_partial_symbols (info_ptr, objfile, &lowpc,
					 &highpc, &cu_header_struct);

      /* If the compilation unit didn't have an explicit address range,
         then use the information extracted from its child dies.  */
      /* RM: On HP, we have low and high pc's for compilation units,
         but they are buggy. Ignore them. */
#ifndef HP_DWARF2_WORKAROUND_BUGS
      if (!comp_unit_has_pc_info)
#endif
	{
	  comp_unit_die.lowpc = lowpc;
	  comp_unit_die.highpc = highpc;
	}
       if (comp_unit_die.sibling)
        {
	  info_ptr = comp_unit_die.sibling;
          comp_unit_die_has_sibling = 1;
          continue;
        }

      comp_unit_die_has_sibling = 0;
      info_ptr = beg_of_comp_unit + cu_header_struct.length + 4;
    }
  do_cleanups (back_to);
}

static void 
set_lowaddr (CORE_ADDR *lowpc, CORE_ADDR *highpc)
{
       if (((*lowpc == 0) && (*highpc == 0)) ||  
	  (*highpc < *lowpc))
	{
	  /* Skip reading. lowaddr should never be used. */
	  debug_sections[sectn].lowaddr = MAXINT;
	  /* Never skip the principal debug section */
	  if (debug_sections[sectn].flags.flag.f_principal)
	    debug_sections[sectn].flags.flag.f_nofunc = 1;
	  else
	    debug_sections[sectn].flags.flag.f_skip = 1;
	}    
      else if (*lowpc == 0)
	{
	  /* No functions. lowaddr should never be used. */
	  debug_sections[sectn].lowaddr = MAXINT;
	  debug_sections[sectn].flags.flag.f_nofunc = 1;
	}
      else
	debug_sections[sectn].lowaddr = SWIZZLE(*lowpc); /* really 64-bits */

       if (*lowpc == 0)
	 *lowpc = *highpc;
}


/* Read in all interesting dies to the end of the compilation unit.  */

static char *
scan_partial_symbols (char *info_ptr, struct objfile *objfile, 
		      CORE_ADDR *lowpc, CORE_ADDR *highpc,
		      const struct comp_unit_head *cu_header)
{
  bfd *abfd = objfile->obfd;
  struct partial_die_info pdi, nest_pdi;
  char *nest_ptr = NULL;

  /* This function is called after we've read in the comp_unit_die in
     order to read its children.  We start the nesting level at 1 since
     we have pushed 1 level down in order to read the comp unit's children.
     The comp unit itself is at level 0, so we stop reading when we pop
     back to that level. */

  int nesting_level = 1;
  int has_pc_info;
  short in_module;

#ifdef HP_IA64
  /* prefetch "info_ptr", will be used in read_unsigned_leb128() when
     called from read_partial_die() */
  _Asm_lfetch_excl(_LFTYPE_NONE,_LFHINT_NONE,info_ptr+64);
#endif

  /*  Poorva: 24th Aug, 2000. HP_IA64  We don't want to enter functions 
     (global or otherwise into the psymtab because the way 
     lookups work is that we first look in the symtab, then the 
     psymtab and then the linker symbol table. We find the 
     corresponding text lo, text hi and then expand the 
     psymtab into a symtab. */
  /* RM: HP generates a module entry after a subprogram entry, causing
   * everything to be nested one level deeper than what this code
   * originally expected. Keep track of whether one of our levels of
   * nesting is a module entry
   */
  in_module = 0;
  memset (&nest_pdi, 0, sizeof (struct partial_die_info));

  *lowpc = ((CORE_ADDR) (long) -1);
  *highpc = ((CORE_ADDR) 0);

  while (nesting_level)
    {
      info_ptr = read_partial_die (&pdi, abfd, info_ptr,
				   &has_pc_info, cu_header);
      {
	switch (pdi.tag)
	  {
	  case DW_TAG_subprogram:
	  case DW_TAG_entry_point:
	    if (processing_doom_objfile)
	      {
		if (debug_sections[sectn].flags.flag.f_principal 
		    && pdi.lowpc == 0)
		  break;
		else
		  {
		    set_lowaddr(&pdi.lowpc, &pdi.highpc);
		    return info_ptr;
		  }
	      }
	    if (has_pc_info)
	      {		  
		if (pdi.lowpc < *lowpc)
		  {
		    *lowpc = pdi.lowpc;
		  }
		if (pdi.highpc > *highpc)
		  {
		    *highpc = pdi.highpc;
		  }
		/* Poorva: 112602: Remove the check for is_external 
		 * and nesting levels
		 * Compiler doesn't set is_external flag correctly.
		 * And for src_no_g the nesting level is not present
		 * since it has no modules only comp-units
		 */
	        add_partial_symbol(&pdi, objfile);
	      }
	    break;
	  case DW_TAG_module:
	    if (processing_doom_objfile)
	      {
		debug_sections[sectn].flags.flag.f_principal = 1;
	      }
	    /* RM: Warning won't work for nested modules */
	    if (pdi.has_children)
	      in_module = nesting_level;
	    break;
	  case DW_TAG_variable:
	  case DW_TAG_typedef:
	  case DW_TAG_namespace:
	    if (processing_doom_objfile)
	      break;
	    if ((pdi.is_external || nesting_level == 1
		 || (in_module && (nesting_level == 2)))
		&& !pdi.is_declaration)
	      {
		add_partial_symbol (&pdi, objfile);
	      }
	    break;
	  case DW_TAG_class_type:
	  case DW_TAG_structure_type:
	  case DW_TAG_union_type:
	  case DW_TAG_enumeration_type:
	    if (processing_doom_objfile)
	      break;
	    if ((pdi.is_external || nesting_level == 1
		 || (in_module && (nesting_level == 2)))
		&& !pdi.is_declaration)
	      {
		if (pdi.type_length && pdi.sibling)
		  {
		    pdi.dies_length = (unsigned int) (pdi.sibling - info_ptr);
		  }
		add_partial_symbol (&pdi, objfile);
	      }
	    break;
	  case DW_TAG_enumerator:
	    if (processing_doom_objfile)
	      break;
	    /* File scope enumerators are added to the partial symbol
	       table.  */
	    if ((nesting_level == 2)
		|| (in_module && (nesting_level == 3)))
	      add_partial_symbol (&pdi, objfile);
	    break;
	  case DW_TAG_base_type:
	    if (processing_doom_objfile)
	      break;
	    /* File scope base type definitions are added to the partial
	       symbol table.  */
	    if ((nesting_level == 1)
		|| (in_module && (nesting_level == 2)))
	      add_partial_symbol (&pdi, objfile);
	    break;
            /* I have added the following to dwarf2.h so that is follows
               the DWARF2 standard, but more investigation is required 
               to properly implement them here. - JAGae17215 */
	  case DW_TAG_dwarf_procedure:
	  case DW_TAG_restrict_type:
	  case DW_TAG_interface_type:
	  case DW_TAG_unspecified_type:
	  case DW_TAG_partial_unit:
	  case DW_TAG_imported_unit:
	  default:
	    break;
	  }
      }
      
      /* If the die has a sibling, skip to the sibling.
         Do not skip enumeration types, we want to record their
         enumerators.  */
      /* RM: Don't skip modules either */
      /* Poorva: 12th April - 2002  Don't skip class member functions 
	 or namespace functions either. We set the range of the text
	 addresses in the psymtab by iterating over all functions 
	 in it and taking the lowest and highest addresses of these funcs.

	 All the functions in the psymtab should definitely include
	 class member functions and namespace functions. 
	 
	 The sibling of the class/namespace would point to another 
	 class/namespace but we want to visit each member function 
	 (child) of it and hence we should not set info_ptr to the sibling. 
	 */

      if (pdi.sibling && pdi.tag != DW_TAG_enumeration_type
	  && pdi.tag != DW_TAG_module && pdi.tag != DW_TAG_class_type
	  && pdi.tag != DW_TAG_namespace 
#ifdef KGDB
          )
#else /* !KGDB */
	  && ((pdi.tag == DW_TAG_subprogram 
	       && (pdi.is_declaration
		   || (!pdi.is_declaration 
		       && pdi.entry_count < 1)))))
#endif /* !KGDB */
	{
          if (pdi.tag == DW_TAG_subprogram && 
              cu_language == language_fortran && 
              pdi.has_children)
            {
              nest_ptr = read_partial_die (&nest_pdi, abfd,
                                           info_ptr, &has_pc_info,
                                           cu_header);
            }
          if (!(nest_pdi.tag == DW_TAG_subprogram))
             info_ptr = pdi.sibling;
	}
      else if (pdi.has_children)
	{
	  /* Die has children, but the optional DW_AT_sibling attribute
	     is missing.  */
	  nesting_level++;
	}

#ifdef HP_IA64
	/* prefetch new value of "info_ptr", will be used in next call to
	   read_unsigned_leb128() from read_partial_die() */
      _Asm_lfetch_excl(_LFTYPE_NONE,_LFHINT_NONE,info_ptr+128);
#endif

      if (pdi.tag == 0)
	{
	  nesting_level--;
	  if (in_module && (in_module == nesting_level))
	    in_module = 0;
	}
    }
  nest_ptr = NULL;

  /* If while processing an objfile for doom we haven't returned from
     the middle then we didn't find a lowpc for this comp_unit which means
     this comp_unit has no functions so set appropriate flags */
  if (processing_doom_objfile)
    {
        /* No functions. lowaddr should never be used. */
        debug_sections[sectn].lowaddr = MAXINT;
        debug_sections[sectn].flags.flag.f_nofunc = 1;
	return info_ptr;
    }
  
  /* If we didn't find a lowpc, set it to highpc to avoid complaints
     from `maint check'.  */
  if (*lowpc == ((CORE_ADDR) (long) -1))
    *lowpc = *highpc;
  return info_ptr;
}

static void
add_partial_symbol (struct partial_die_info *pdi, struct objfile *objfile)
{
  CORE_ADDR addr = 0;
  unsigned int locdesc_offset;
  int len = 0, is_decl = 0;
  enum type_code type_codes = TYPE_CODE_UNDEF; 
  /* initialize to -1. See use below - we don't want to break out of the
     case statements but we do need to set the type_code according 
     to specific cases. */

  /* Poorva: Jun 24, 2002. All the prim_record_minimal_symbols have been
     commented out - because they are a _big_ memory leak. prim_record does
     not attach anything to the objfile. It just allocates bunches of space.
     To attach these to the objfile's minimal symbol table (which we use
     to access the minsyms) one needs to call
     install_minimal_symbols after calling prim_record....
     Also don't call strlen n times for the the same thing. Store the len.
     */
  if (!pdi)
   return;
  switch (pdi->tag)
    {
      /* Poorva: 24th Aug, 2000. We don't add subprograms to the psymtabs 
	 anymore, hence for HP_IA64 we comment out add_psymbol_to_list */
    case DW_TAG_subprogram:
      len = (int) (pdi->name ? strlen (pdi->name) : 0);
      if (cu_language == language_fortran)
	{
	  /* save name in lower case, because case_insensitive search
	     (see lookup_symbol) expects names to be in lower case */
	  int i;
	  for (i = 0; i < len; i++)
	    pdi->name[i] = tolower (pdi->name[i]);
	}
      if (pdi->is_external || (pdi->is_external == -1))
	{
          if (cu_language == language_fortran &&
              pdi->calling_convention == DW_CC_program)
            {
	      /* For fortran we now need to malloc space for the
                 default_main string instead of just using a pointer into
                 the debug info section since we don't keep the debug
                 info section around anymore.
                 */
              default_main = obsavestring (pdi->name, len,
                                           &objfile->psymbol_obstack);
              add_psymbol_with_dem_name_to_list_1 (
                pdi->name, len,
                fortran_main_string, (int) (strlen (fortran_main_string)),
                VAR_NAMESPACE, LOC_BLOCK, &objfile->global_psymbols,
                0, pdi->lowpc + baseaddr, cu_language, objfile, 
		TYPE_GLOBAL, TYPE_CODE_FUNC, 0, 0, 0);
            }
#ifndef FAT_FREE_PSYMTABS
	  else
	    add_psymbol_to_list_1 (pdi->name, len,
				   VAR_NAMESPACE, LOC_BLOCK,
				   &objfile->global_psymbols,
				   0, pdi->lowpc + baseaddr, cu_language, 
				   objfile, TYPE_GLOBAL, TYPE_CODE_FUNC, 
				   0, 0, 0, (unsigned) -1);
#endif

        }
      else
	{

#ifndef FAT_FREE_PSYMTABS
	  add_psymbol_to_list_1 (pdi->name, len,
				 VAR_NAMESPACE, LOC_BLOCK,
				 &objfile->static_psymbols,
				 0, pdi->lowpc + baseaddr, cu_language, 
				 objfile, TYPE_STATIC, TYPE_CODE_FUNC, 
				 0, 0, 0, (unsigned) -1);
#endif
	}
      break;
    case DW_TAG_entry_point:
      len = (int) (strlen(pdi->name));
      if (cu_language == language_fortran) {
      /* save name in lower case, because case_insensitive search
	 (see lookup_symbol) expects names to be in lower case */
      int i;
      for (i = 0; i < len; i++)
	pdi->name[i] = tolower (pdi->name[i]);
      }
      if (cu_language == language_fortran || cu_language == language_cplus) {
	if (pdi->is_external || (pdi->is_external == -1))
	  {
#ifndef FAT_FREE_PSYMTABS
	    add_psymbol_to_list_1 (pdi->name, len,
				   VAR_NAMESPACE, LOC_BLOCK,
				   &objfile->global_psymbols,
				   0, pdi->lowpc + baseaddr, cu_language, 
				   objfile, TYPE_GLOBAL, TYPE_CODE_FUNC, 
				   0, 0, 0, (unsigned) -1);
#endif
	  }
	else
	  {
#ifndef FAT_FREE_PSYMTABS
	    add_psymbol_to_list_1 (pdi->name, len,
				   VAR_NAMESPACE, LOC_BLOCK,
				   &objfile->static_psymbols,
				   0, pdi->lowpc + baseaddr, cu_language, 
				   objfile, TYPE_STATIC, TYPE_CODE_FUNC, 
				   0, 0, 0, (unsigned) -1);
#endif
	  }
      }
      break;
    case DW_TAG_variable:
      if (pdi->is_external)
	{
	  /* Global Variable.
	     Don't enter into the minimal symbol tables as there is
	     a minimal symbol table entry from the ELF symbols already.
	     Enter into partial symbol table if it has a location
	     descriptor or a type.
	     If the location descriptor is missing, new_symbol will create
	     a LOC_UNRESOLVED symbol, the address of the variable will then
	     be determined from the minimal symbol table whenever the variable
	     is referenced.
	     The address for the partial symbol table entry is not
	     used by GDB, but it comes in handy for debugging partial symbol
	     table building.  */

	  if (pdi->locdesc) 
	    {
	      addr = decode_locdesc (NULL, pdi->locdesc, objfile, &locdesc_offset);
	    }
	  if (pdi->locdesc || pdi->has_type)
	    add_psymbol_to_list_1 (pdi->name, (int)(strlen (pdi->name)),
				   VAR_NAMESPACE,
				   optimized_out? LOC_OPTIMIZED_OUT:LOC_STATIC,
				   &objfile->global_psymbols,
				   0, addr + data_baseaddr, cu_language, 
				   objfile, TYPE_GLOBAL, TYPE_CODE_UNDEF, 
				   0, 0, 0, (unsigned) -1);
	}
      else
	{
	  /* Static Variable. Skip symbols without location descriptors.  */
	  if (pdi->locdesc == NULL)
	    return;
	  addr = decode_locdesc (NULL, pdi->locdesc, objfile, &locdesc_offset);
	  add_psymbol_to_list_1 (pdi->name, (int) (strlen (pdi->name)),
				 VAR_NAMESPACE, 
				 optimized_out? LOC_OPTIMIZED_OUT:LOC_STATIC,
				 &objfile->static_psymbols,
				 0, addr + data_baseaddr, cu_language, 
				 objfile, TYPE_STATIC, TYPE_CODE_UNDEF, 
				 0, 0, 0, (unsigned) -1);
	}
      break;
    case DW_TAG_typedef:
      if (pdi->is_declaration)
        is_decl = 1;

      add_psymbol_to_list_1 (pdi->name, (int) (strlen (pdi->name)),
                             VAR_NAMESPACE, LOC_TYPEDEF,
                             &objfile->static_psymbols, 0, (CORE_ADDR) 0,
                             cu_language, objfile, TYPE_STATIC,
                             TYPE_CODE_UNDEF, is_decl, 0, 0, (unsigned) -1);
      break;
    case DW_TAG_base_type:
      if (pdi->is_declaration)
	is_decl = 1;
      
      add_psymbol_to_list_1 (pdi->name, (int) (strlen (pdi->name)),
			     VAR_NAMESPACE, LOC_TYPEDEF,
			     &objfile->static_psymbols, 0, (CORE_ADDR) 0, 
			     cu_language, objfile, TYPE_GLOBAL, 
			     TYPE_CODE_UNDEF, is_decl, 0, 0, (unsigned) -1);
      break;
    case DW_TAG_class_type:
      type_codes = TYPE_CODE_CLASS;
    case DW_TAG_structure_type:
      type_codes = TYPE_CODE_STRUCT;
    case DW_TAG_union_type:
      type_codes = TYPE_CODE_UNION;
    case DW_TAG_enumeration_type:
      type_codes = TYPE_CODE_ENUM;
    case DW_TAG_namespace:
      type_codes = TYPE_CODE_NAMESPACE;

      /* Skip aggregate types without children, these are external
         references.  */
      if (pdi->has_children == 0)
	return;
      if (pdi->is_declaration)
	is_decl = 1;

      add_psymbol_to_list_1 (pdi->name, (int) (strlen (pdi->name)),
			     STRUCT_NAMESPACE, LOC_TYPEDEF,
			     &objfile->static_psymbols,
			     0, (CORE_ADDR) 0, cu_language, 
			     objfile, TYPE_STATIC, type_codes, 
			     is_decl, pdi->dies_length, 
			     pdi->type_length, (unsigned) -1);
      
      if ((cu_language == language_cplus) && (pdi->tag != DW_TAG_namespace))
	{
	  /* For C++, aggregates (but not namespaces) also implicitly act 
	     as typedefs. */
	  add_psymbol_to_list_1 (pdi->name, (int) (strlen (pdi->name)),
				 VAR_NAMESPACE, LOC_TYPEDEF,
				 &objfile->static_psymbols,
				 0, (CORE_ADDR) 0, cu_language, 
				 objfile, TYPE_STATIC, type_codes, 
				 is_decl, pdi->dies_length, pdi->type_length,
				 (unsigned) -1);
	}
      break;
    case DW_TAG_enumerator:
      add_psymbol_to_list_1 (pdi->name, (int) (strlen (pdi->name)),
			     VAR_NAMESPACE, LOC_CONST,
			     &objfile->static_psymbols,
			     0, (CORE_ADDR) 0, cu_language, 
			     objfile, TYPE_STATIC, TYPE_CODE_UNDEF, 0, 0, 0,
			     (unsigned) -1);
      break;
    default:
      break;
    }
}

/* Expand this partial symbol table into a full symbol table.  */

static void
dwarf2_psymtab_to_symtab (struct partial_symtab *pst)
{
  processing_doom_objfile = 0;
  /* FIXME: This is barely more than a stub.  */
  if (pst != NULL)
    {
      if (pst->readin)
	{
	  warning ("bug: psymtab for %s is already read in.", pst->filename);
	}
      else
	{
	  if (info_verbose)
	    {
	      printf_filtered ("Reading in symbols for %s...", pst->filename);
	      gdb_flush (gdb_stdout);
	    }

	  current_objfile = pst->objfile;
          current_objfile->current_pst = pst;
	  hp_dwarf2_object_present = 1;
	  psymtab_to_symtab_1 (pst);

	  /* Finish up the debug error message.  */
	  if (info_verbose)
	    printf_filtered ("done.\n");
	}
    }
}

static void
psymtab_to_symtab_1 (struct partial_symtab *pst)
{
  struct objfile *objfile = pst->objfile;
  bfd *abfd = objfile->obfd;
  struct comp_unit_head cu_header_struct;
  struct die_info *dies;
  unsigned long offset;
  unsigned long dwarf_comp_unit_offset = 0;
  CORE_ADDR lowpc = 0, highpc = 0;
  struct die_info *child_die;
  char *info_ptr;
  struct symtab *symtab;
  struct cleanup *back_to;
  struct attribute *attr;
  struct partial_die_info comp_unit_die;
  char *sibling;
  int dummy;

  dwarf_comp_unit_offset = DWARF_COMP_UNIT_OFFSET (pst);

  /* Set local variables from the partial symbol table info.  */
  if (!processing_doom_objfile)
    {
      offset = DWARF_INFO_OFFSET (pst);
#ifndef USING_MMAP
      dwarf_info_buffer = DWARF_INFO_BUFFER (pst);
      dwarf_abbrev_buffer = DWARF_ABBREV_BUFFER (pst);
      dwarf_abbrev_size = DWARF_ABBREV_SIZE (pst);
      dwarf_line_buffer = DWARF_LINE_BUFFER (pst);
      dwarf_line_buffer_actual = DWARF_LINE_BUFFER_ACTUAL (pst);
      dwarf_str_buffer = DWARF_STR_BUFFER (pst);
      dwarf_str_size = DWARF_STR_SIZE (pst);
#ifdef HP_IA64
      dwarf_macinfo_buffer = DWARF_MACINFO_BUFFER (pst);
      dwarf_macinfo_size = DWARF_MACINFO_SIZE (pst);
#endif
#else
      dwarf_line_buffer_actual = 0;
      dwarf_abbrev_size = DWARF_ABBREV_SIZE (pst);
      /* Mmap that part of the debug sections that belongs to the .o we
         want to expand the psymtab for.
         Calculate this offset by using the beginning of the debug info
         section in the file e.g DEBUG_INFO_FILE_START and then adding
         to that offset the offset of the debug section of this .o
         within that of the load module.
         */

      dwarf_info_buffer = 
        dwarf2_read_section (objfile,
                             DEBUG_INFO_FILE_START(pst)
                             + DWARF_INFO_OFFSET(pst),
                             (unsigned int) DWARF_INFO_SIZE(pst));
      dwarf_abbrev_buffer = 
        dwarf2_read_section (objfile,
                             DEBUG_ABBREV_FILE_START(pst)
                             + DWARF_ABBREV_OFFSET(pst),
                             DWARF_ABBREV_SIZE(pst));
      dwarf_line_buffer = 
        dwarf2_read_section (objfile,
                             DEBUG_LINE_FILE_START(objfile)
                             + DWARF_LINE_OFFSET(pst),
                             (unsigned int) DWARF_LINE_SIZE(pst));
      if (DWARF_LINE_SIZE_ACTUAL(pst))
        dwarf_line_buffer_actual = 
          dwarf2_read_section (objfile,
                               DEBUG_LINE_ACTUAL_FILE_START
                               (objfile)
                               + DWARF_LINE_OFFSET_ACTUAL(pst),
                               (unsigned int) DWARF_LINE_SIZE_ACTUAL(pst));
      dwarf_str_buffer = (char *) DWARF_STR_BUFFER (pst);
      dwarf_str_size = DWARF_STR_SIZE (pst);
#ifdef HP_IA64
      dwarf_macinfo_buffer = (char *) DWARF_MACINFO_BUFFER (pst);
      dwarf_macinfo_size = DWARF_MACINFO_SIZE (pst);
#endif
      /* info_ptr should point to the beginning of the dwarf info buffer for
       this .o which is just dwarf_info_buffer now. No need to add offset
       to it. */
      info_ptr = dwarf_info_buffer;
      /* There is a psymtab for every comp unit hence this offset is 0 */
      cu_header_offset = 0;
#endif
      dwarf_loc_buffer = DWARF_LOC_BUFFER (pst);
      dwarf_loc_size = DWARF_LOC_SIZE (pst);
      baseaddr = ANOFFSET (pst->section_offsets, 0);
      data_baseaddr = ANOFFSET (pst->section_offsets, 1);
    }
  else
    {
      offset = debug_sections[sectn].cu_header_offset;
      /* For DOOM while doing the relocations for text section
	 objects and data section objects we have already 
	 added baseaddr and data_baseaddr to them.
	 The general functions we use like read_func_scope etc add 
	 these values to the lowpc, highpc etc and for DOOM we don't want
	 them adding these values again to the lowpc etc. 
      */
      /* For purified programs, there are no relocations to apply since
         purify on ia64 writes out debug sections to contains fully relocated
         post-link, post-instrumentation addresses. As a result, we would
         have skipped the apply-relocations-step and so would NOT have already
         added baseaddr and data_baseaddr. So, we have to do it now.
      */
      if (is_purified_program)
        {
          baseaddr = ANOFFSET (pst->section_offsets, 0);
          data_baseaddr = ANOFFSET (pst->section_offsets, 1);
        }
      else
        {
          baseaddr = 0;
          data_baseaddr = 0;
        }

#ifdef USING_MMAP
      info_ptr = dwarf_info_buffer + offset;
      cu_header_offset = (unsigned int) offset;
#endif
    }

#ifndef USING_MMAP
  cu_header_offset = offset;
  info_ptr = dwarf_info_buffer + offset;
#endif

  obstack_init (&dwarf2_tmp_obstack);
  back_to = make_cleanup (dwarf2_free_tmp_obstack, pst);

  if (!processing_doom_objfile)
    {
      buildsym_init ();
      make_cleanup (really_free_pendings, NULL);
    }
  
  /* read in the comp_unit header  */
  cu_header_struct.length = read_4_bytes (abfd, info_ptr);
  info_ptr += 4;
  cu_header_struct.version = read_2_bytes (abfd, info_ptr);
  info_ptr += 2;
  cu_header_struct.abbrev_offset = read_4_bytes (abfd, info_ptr);
  info_ptr += 4;
  cu_header_struct.addr_size = read_1_byte (abfd, info_ptr);
  info_ptr += 1;

  if (!processing_doom_objfile)
  {
    /* Read the abbrevs for this compilation unit  */
#ifdef USING_MMAP
    /* Again we point to the dwarf_abbrev_buffer for this .o and don't need
       to add offset to it hence the abbrev_offset field should be 0 now. */
    unsigned int save_abbrev_offset = cu_header_struct.abbrev_offset;
    cu_header_struct.abbrev_offset = 0;
    dwarf2_read_abbrevs (abfd, &cu_header_struct);
    cu_header_struct.abbrev_offset = save_abbrev_offset;
#else
    dwarf2_read_abbrevs (abfd, &cu_header_struct);
#endif
    info_ptr = dwarf_info_buffer + dwarf_comp_unit_offset; 

    make_cleanup (dwarf2_empty_abbrev_table, NULL); 
  }

  read_partial_die (&comp_unit_die, abfd, info_ptr, &dummy, &cu_header_struct);
#ifdef HP_IA64
  if (!debug_dwutils_processed && !die_ref_table)
    die_ref_table = (struct die_info **) 
                    xmalloc (REF_HASH_SIZE * sizeof (struct die_info *));
  else if (debug_dwutils_processed)
    {
      /* Load the die_ref_table for the psymtab. */
      pst->die_ref_table = (struct die_info **) 
                    xmalloc (REF_HASH_SIZE * sizeof (struct die_info *));
      die_ref_table = pst->die_ref_table;
    }
#endif

  dies = read_comp_unit (info_ptr, abfd, &cu_header_struct);

  /* Find the base address of the compilation unit for range lists and
     location lists.  It will normally be specified by DW_AT_low_pc.
     In DWARF-3 draft 4, the base address could be overridden by
     DW_AT_entry_pc.  It's been removed, but GCC still uses this for
     compilation units with discontinuous ranges.  */

  cu_header_struct.base_known = 0;
  cu_header_struct.base_address = 0;

  attr = dwarf_attr (dies, DW_AT_entry_pc);
  if (attr)
    {
      cu_header_struct.base_address = DW_ADDR (attr);
      cu_header_struct.base_known = 1;
    }
  else
    {
      attr = dwarf_attr (dies, DW_AT_low_pc);
      if (attr)
	{
	  cu_header_struct.base_address = DW_ADDR (attr);
	  cu_header_struct.base_known = 1;
	}
    }

  sibling = NULL;
  do
    {
      if (processing_doom_objfile)
        make_cleanup ((make_cleanup_ftype *) free_die_list, dies);
      objfile->current_pst = pst;
      process_die (dies, objfile, &cu_header_struct);
      if (processing_doom_objfile)
	{
	  sibling = comp_unit_die.sibling;
	  if (sibling)
	    {
	      read_partial_die (&comp_unit_die, abfd, sibling, 
                                &dummy, &cu_header_struct);
	      dies = read_comp_unit (sibling, abfd, &cu_header_struct);
	    }
	}
    }
  while (sibling);

  if (!processing_doom_objfile)
    {
      if (dwarf2_get_pc_bounds (dies, &lowpc, &highpc, objfile) != PCB_ok)
	{
	  /* Some compilers don't define a DW_AT_high_pc attribute for
	     the compilation unit. If the DW_AT_high_pc is missing, synthesize
	     it, by scanning the DIE's below the compilation unit.  */
	  highpc = 0;
	  if (dies->has_children)
	    {
	      child_die = dies->next;
	      while (child_die && child_die->tag)
		{
		  if (child_die->tag == DW_TAG_subprogram)
		    {
		      CORE_ADDR low = 0, high = 0;
		      if (dwarf2_get_pc_bounds (child_die, &low, 
                                                &high, objfile) == PCB_ok)
			{
			  highpc = max (highpc, high);
			}
		    }
		  child_die = sibling_die (child_die);
		}
	    }
	}
      
      symtab = end_symtab (highpc + baseaddr, objfile, 0);

      /* aliases is a global array of alias names for namespaces. It
	 contains all file scoped aliases. We add it to the global 
	 block of the symtab */
      if (aliases && symtab)
	{
	  struct block *block = NULL;
	  block = BLOCKVECTOR_BLOCK (BLOCKVECTOR(symtab), GLOBAL_BLOCK);
	  block->aliases = aliases;
	}
      

      /* Set symtab language to language from DW_AT_language. */
#ifdef HP_IA64
      if (symtab != NULL)
#else
      /* If the compilation is from a C/C++ file generated by language 
	 preprocessors, do not set the language if it was already 
	 deduced by start_subfile.  */
      if (symtab != NULL && 
	  !((cu_language == language_c && symtab->language != language_c) ||
	    (cu_language == language_cplus && 
             symtab->language != language_cplus)))
#endif
	{
	  symtab->language = cu_language;
	}
      
      pst->symtab = symtab;
      pst->readin = 1;
      sort_symtab_syms (pst->symtab);
      hp_dwarf2_object_present = 1;
  
#ifdef USING_MMAP
      /* Finally unmap the sections we have used to build the symtab for
         this .o */
      dwarf2_unmap_section (dwarf_info_buffer,
                            DEBUG_INFO_FILE_START(pst)
                            + DWARF_INFO_OFFSET(pst),
                            (unsigned int) DWARF_INFO_SIZE(pst));
      dwarf2_unmap_section (dwarf_abbrev_buffer,
                            DEBUG_ABBREV_FILE_START(pst)
                            + DWARF_ABBREV_OFFSET(pst),
                            DWARF_ABBREV_SIZE(pst));
      dwarf2_unmap_section (dwarf_line_buffer,
                            DEBUG_LINE_FILE_START(objfile)
                            + DWARF_LINE_OFFSET(pst),
                            (unsigned int) DWARF_LINE_SIZE(pst));
      if (DWARF_LINE_SIZE_ACTUAL(pst))
        dwarf2_unmap_section (dwarf_line_buffer_actual,
                              DEBUG_LINE_ACTUAL_FILE_START
                              (objfile)
                              + DWARF_LINE_OFFSET_ACTUAL(pst),
                              (unsigned int) DWARF_LINE_SIZE_ACTUAL(pst));
#endif
    }
#ifdef HP_IA64
  if (!processing_doom_objfile && 
      !debug_dwutils_processed && die_ref_table)
    {
      free_die_ref_table ((void **) die_ref_table);
      die_ref_table = NULL;
    }
#else
  if (!processing_doom_objfile)
    free_die_ref_table ((void **) die_ref_table);
#endif
  do_cleanups (back_to);
}

#ifdef HP_IA64

void 
dwarf2_symfile_init (struct objfile *objfile)
{
  objfile->sym_private = (PTR)
    xmmalloc (objfile->md, sizeof (struct hpread_symfile_info));

  memset (objfile->sym_private, 0, sizeof (struct hpread_symfile_info));
}

/*  Is called by bfd_map_over_sections. In dwarf2_doom_psymtab_to_symtab
    we need to find out how many .debug_info sections we have so we call
    map_over_sections which calls this function in turn.

    The parameters are sent in by bfd_map_over_sections. 
    sect is a pointer to the sections, sect_count_ptr is a 
    user  given char * that can be modified by the user in this function. 
    ignore_abfd is a bfd pointer but is ignored.    
    */

static void 
compare_sections (bfd *ignore_abfd, asection *sectp, PTR sect_count_ptr)
{
  /* Poorva: Check the debugging flag before looking for the 
     .debug_info section.
     This change reduces the number of strcmp calls by 7 million. */

  if (sectp->flags & SEC_DEBUGGING)
    if (STREQSPACE (sectp->name, INFO_SECTION))
      {
	(*((int *)sect_count_ptr))++;
      }
}


/* For DOOM expand this partial symbol table into a full symbol table.  */

void
dwarf2_doom_psymtab_to_symtab (struct partial_symtab *pst)
{
  /* in elfread.c */
  extern bfd_size_type relocated_section_size (bfd *, asection *);
  extern void get_relocated_section_contents
    (struct objfile *, bfd *, int, asection *, char *, int);
  extern int hpread_compare_debug_sects(void *, void *);
  
  /* From minsyms.c */
  extern void hpread_dump_debug_relocations 
    ( struct objfile *, bfd *, asection *, asection *, asection *);

  extern char* object_path;
  bfd *abfd;
  struct objfile *objfile;  
  int objid;
  int n_dbg_sects;
  int i, length;
  struct cleanup *back_to;

  struct partial_symtab *cur_pst, *last_pst, *default_pst;
  struct cleanup *old_chain = NULL; 
  struct parse_filename_info obj_filename_parse;
  char *file_name, *tmp_name, *file_path = NULL;
  char *path;
  char *cdir;
  char *p;

  char *archive_name = NULL;
  char *arch_member_name = NULL;
  bfd *ar_bfd = NULL;
  
  enum language cu_language_saved ;
  unsigned int cu_header_offset_saved;
  CORE_ADDR baseaddr_saved, data_baseaddr_saved;
  
  char *info_saved;
  char *abbrev_saved;
  char * line_saved;
  char * line_saved_actual;
  char * loc_saved;
  unsigned int abbrev_size_saved;
  unsigned int info_size_saved;
  unsigned int line_size_saved;
  unsigned int loc_size_saved;
  unsigned int line_size_saved_actual;
  file_ptr info_offset_saved;
  file_ptr abbrev_offset_saved;
  file_ptr line_offset_saved;
  file_ptr line_offset_saved_actual;
#ifdef HP_IA64
  char * mac_saved;
  unsigned int macinfo_size_saved;
  file_ptr macinfo_offset_saved;
#endif

    /* Save away various globals. We'll temporarily use them for
     object file information */

  info_saved = dwarf_info_buffer;
  info_offset_saved = dwarf_info_offset;
  info_size_saved = dwarf_info_size;
  abbrev_saved = dwarf_abbrev_buffer;
  abbrev_offset_saved = dwarf_abbrev_offset;
  abbrev_size_saved = dwarf_abbrev_size ;
  line_saved = dwarf_line_buffer;
  line_offset_saved = dwarf_line_offset;
  line_size_saved = dwarf_line_size;
  loc_size_saved = dwarf_loc_size;
  loc_saved = dwarf_loc_buffer;
  line_saved_actual = dwarf_line_buffer_actual;
  line_offset_saved_actual = dwarf_line_offset_actual;
  line_size_saved_actual = dwarf_line_size_actual;
#ifdef HP_IA64
  mac_saved = dwarf_macinfo_buffer;
  macinfo_size_saved = dwarf_macinfo_size;
  macinfo_offset_saved = dwarf_macinfo_offset;
#endif
  cu_language_saved = cu_language;
  cu_header_offset_saved = cu_header_offset;
  baseaddr_saved = baseaddr;
  data_baseaddr_saved = data_baseaddr;

  if (!pst)
    return;
  
  if (pst->readin)
    {
      warning ("bug: psymtab for %s is already read in.", pst->filename);
      return;
    }
  
  if (info_verbose)
    {
      printf_filtered ("Reading in symbols for %s...", pst->filename);
      gdb_flush (gdb_stdout);
    }
  
  if (OFILE_LOAD_STATUS (pst) != DOOM_OFILE_DO_DEMAND_LOAD)
    {
      /* we already tried to read this psymtab. Maybe we couldn't find
       * the object file or maybe we didn't find any debug. Either
       * way, there's no point in trying again
       */
      return;
    }
  
  objfile = pst->objfile;
  obstack_init (&dwarf2_tmp_obstack_2);
  back_to = make_cleanup (dwarf2_free_tmp_obstack_2, NULL);

  /* Store objfile attributes and replace them (later) with attributes of this
     objfile */
 
  /* If the COMPMAP does not have the name give a warning and return */
  if (COMPMAP (objfile)->cr[OBJID (pst)].objname == (unsigned int) -1)
    {
      warning ("Warning: Notes section and linkmap file entries mismatch"
	       "for file %s\n"
	       "Will act like a non -g compiled binary"
	       "Please let your service representative know about this\n",
		pst->filename);

      return;
    } 
  tmp_name = STRINGS (objfile) + COMPMAP (objfile)->cr[OBJID (pst)].objname;
  file_name = (char *) obstack_alloc (&dwarf2_tmp_obstack_2, 
				      strlen (tmp_name) + 1);
  strcpy (file_name, tmp_name);

  /* Remove any leading directory names so we have just the filename. Any
  extra directory name will cause objectdir to be ignored. */	
  
  length = (int) (strlen (file_name) - strlen (SLASH_STRING));  
  if ((length > 0) && (strcmp((file_name + length), SLASH_STRING) == 0))
    *(file_name + length) = '\0';
  tmp_name = strrstr (file_name, SLASH_STRING);
  length = 0;
  if (tmp_name)
    {
      file_path = file_name;
      file_name = tmp_name + 1;
      *tmp_name = '\0';
      length = (int) strlen (file_path);
   }
 

  if (debug_traces)
    printf_filtered ("Reading symbols from %s...\n", file_name);

  
  /* Construct the pathname by replacing $cdir with the
   * compilation directory */
  path = object_path;
  cdir = STRINGS (objfile) + COMPMAP (objfile)->cr[OBJID (pst)].dirname;
  /* Replace a path entry of $cdir with the compilation directory
     name */
#define	cdir_len	5
  p = (char *) strstr (object_path, "$cdir");
  if (p && (p == path
	    || p[-1] == DIRNAME_SEPARATOR)
	&& (p[cdir_len] == DIRNAME_SEPARATOR || p[cdir_len] == '\0'))
    {
      int len, buf_size;

      if (cdir && *cdir)
	buf_size = (int) strlen (object_path) + 1 + (int) strlen (cdir) + 1 + length + 1;
      else
        buf_size = (int) strlen (object_path) + 1 + length + 1;

      path = (char *) obstack_alloc (&dwarf2_tmp_obstack_2, buf_size);

      len = (int) (p - object_path);
      strncpy (path, object_path, len);	/* Before $cdir */
      path [len] = 0;
      /* Add file_path: before $cdir. JAGae71279 */
      if (length)
	{
	  strcat (path, file_path);
	  if (cdir && *cdir)
	    strcat (path, ":");
	}
      if (cdir && *cdir)
	{
          strcat (path, cdir);	/* new stuff */
#ifdef PATHMAP
	  if (length)
            pathmap_set_cdir (path + len + length + 1);
	  else
            pathmap_set_cdir (path + len);
#endif /* PATHMAP */
	}
      else
	{
#ifdef PATHMAP
          pathmap_set_cdir (path + len);
#endif /* PATHMAP */
	}
        strcat (path, object_path + len + cdir_len); /* After $cdir */
    }
  else
    {
      error ("Object directory appears to be incorrect.  If you are using objectdir commands, check to see if they are correctly specified.  If the problem still persists, contact HP.");
    }

  /* CM: Parse the filename string */
  parse_filename (file_name, &obj_filename_parse);
  if (obj_filename_parse.archive_name_length > 0)
    {
      /* CM: Read archive file and get a bfd for a member */
      int found_member;

      archive_name = (char *) 
	obstack_alloc (&dwarf2_tmp_obstack_2, 
		       obj_filename_parse.archive_name_length + 1);
      arch_member_name = obstack_alloc (&dwarf2_tmp_obstack_2, 
				obj_filename_parse.o_file_name_length + 1);

      memcpy (archive_name, file_name, obj_filename_parse.archive_name_length);
      archive_name[obj_filename_parse.archive_name_length] = '\0';

      memcpy (arch_member_name, obj_filename_parse.o_file_name_start,
	      obj_filename_parse.o_file_name_length);
      arch_member_name[obj_filename_parse.o_file_name_length] = '\0';

      found_member = 0;
      /* Do not try cwd first here. Always follow the 'path' to search. */
      /* JAGae71279 */
      ar_bfd = symfile_bfd_open_with_path_type (archive_name,
						path,
						bfd_archive,
						0, 0);
      abfd = NULL;
      if (ar_bfd != NULL)
	{
	  do
	    {
	      abfd = bfd_openr_next_archived_file (ar_bfd, abfd);
	      if (abfd != NULL)
		{
		  if (STREQ (abfd->filename, arch_member_name))
		    {
		      if (bfd_check_format (abfd, bfd_object))
			{
			  found_member = 1;
			}
		    }
		}
	    }
	  while ((abfd != NULL) && (!found_member));
	}
      /* CM: "abfd" now holds a bfd to the archive member or NULL
         if the file was not found in the archive. */
    }
  else
    {
      /* RM: read in debug sections of the object file, and put then in
       * the objfile. This is so that the existing code in this file can
       * read it in without much tweaking.
       */
      /* Do not try cwd first here. Always follow the 'path' to search. */
      /* JAGae71279 */
      abfd = symfile_bfd_open_with_path_type (file_name, path,
					      bfd_object, 0, 0);
    }

  if (!abfd)
    {
      if (!batch_rtc
          && !objfile->separate_debug_objfile) 
        {
          if (annotation_level > 1)
	     warning ("ERROR: Use \"objectdir\" command for path of %s.\n", 
		 file_name);
          else
	     warning ("\nERROR: Use the \"objectdir\" command to specify the search\n"
		 "path for objectfile %s.\nIf NOT specified will "
		 "behave as a non -g compiled binary.\n",
		 file_name);
        }

      OFILE_LOAD_STATUS (pst) = DOOM_OFILE_OPEN_ERROR;
      return;
    }

  if (is_swizzled != BFD_IS_SWIZZLED(abfd))
    {
      printf ("%s ", abfd->filename);
      if (is_swizzled)
	error("is 64 bit while executable is 32 bit.");
      else
	error("is 32 bit while executable is 64 bit.");
    }


  /* count number of debug sections */
  /* With COMDAT, many sections may have the same name. Hence we
   * need to know the number of debug sections.
   */
  /* Use bfd_map_over_sections instead of the below since this 
     can use a function that can check the flag SEC_DEBUGGING before
     doing a strcmp whereas bfd_get_section_by_name_and_id
     does a strcmp - Removed 7 million strcmp from IA64
   */

  n_dbg_sects = 0;
  bfd_map_over_sections (abfd, compare_sections, &n_dbg_sects);


  /* Does this file have debug information? */
  if (!n_dbg_sects)
    {
      if (debug_traces)
	{
	  printf_filtered ("no debug information information found\n");
	}
      OFILE_LOAD_STATUS (pst) = DOOM_OFILE_LOADED_NO_DEBUG;
      return;
    }
  else
    {
      if (debug_traces)
	{
	  printf_filtered ("  found %d debug section(s)\n", n_dbg_sects);
	}
    }
  
  OFILE_LOAD_STATUS (pst) = DOOM_OFILE_LOADED_WITH_DEBUG;
  objid = OBJID (pst);
  
  /* read all the debug sections */
  
  debug_sections = (struct debug_section *) 
		 (void *) obstack_alloc (&dwarf2_tmp_obstack_2, 
			        n_dbg_sects * (sizeof (struct debug_section)));
  memset (debug_sections, 0, sizeof (struct debug_section) * n_dbg_sects);

  /* Fills in the debug_sections[i] array with offsets and sizes. */

  /* Using the variable sectn which is already a global to hold the 
     number of debug sections for now. Should be part of a structure though. */

  sectn = n_dbg_sects;
  processing_doom_objfile = 1;
  dwarf2_get_section_offsets (abfd);
  sectn = -1; 
  
  for (i = 0; i < n_dbg_sects; i++)
    {
      if (debug_traces >= 1000)
        hpread_dump_debug_relocations (objfile,
                                       abfd,
                                       debug_sections[i].info_section,
                                       debug_sections[i].abbrev_section,
                                       debug_sections[i].line_section);
      
	debug_sections[i].reloc_info_size = 
	  (debug_sections[i].info_size ?  
	   relocated_section_size(abfd, debug_sections[i].info_section) : 0 );
	debug_sections[i].reloc_abbrev_size = (unsigned int) 
	  (debug_sections[i].abbrev_size ?  
	   relocated_section_size(abfd, debug_sections[i].abbrev_section) : 0);
	debug_sections[i].reloc_line_size = 
	  (debug_sections[i].line_size ?
	   relocated_section_size(abfd, debug_sections[i].line_section) : 0);
	debug_sections[i].reloc_line_size_actual = 
	  (debug_sections[i].line_size_actual ?
	   relocated_section_size(abfd, debug_sections[i].line_section_actual) : 0);
#ifdef HP_IA64
	debug_sections[i].reloc_mac_size = 
	  (debug_sections[i].mac_size?
	   relocated_section_size(abfd, debug_sections[i].dwarf_macinfo_section) : 0);
#endif

	    /* The relocated size of the .debug_loc section is known to be
	       the same size as its input size on ELF platforms, since none
	       of the fixups used in the .debug_loc file expand the output
	       from the input size.  This is probably true on other
	       platforms (and of other sections), but it's difficult to
	       prove.  Rather than add code here that's known to be useless
	       on platforms we can test, we've decided to go with an assert
	       to check it on every platform - in case it becomes untrue for
	       some case where this code might be used in the future.
	       JAGag33690, "DOC:  +objdebug not supported with location
	       lists".  Carl Burch, March 14, 2007.
	     */
        assert (debug_sections[i].loc_size == 
	  relocated_section_size(abfd, debug_sections[i].loc_section));
   


	if (debug_traces >= 100)
         {
           printf_filtered ("Raw INFO size: %d\n", (int) debug_sections[i].info_size);
           printf_filtered ("Relocated INFO size: %d\n",
                   (int) debug_sections[i].reloc_info_size);
           printf_filtered ("Raw ABBREV size: %d\n",
                   (int) debug_sections[i].abbrev_size);
           printf_filtered ("Relocated ABBREV size: %d\n",
                   (int) debug_sections[i].reloc_abbrev_size);
           printf_filtered ("Raw LINE size: %d\n",
                   (int) debug_sections[i].line_size);
           printf_filtered ("Relocated LINE size: %d\n",
                   (int) debug_sections[i].reloc_line_size);
           printf_filtered ("Raw LOCATION LISTS size: %d\n",
                   (int) debug_sections[i].loc_size);
           printf_filtered ("Raw LINE size actual: %d\n",
                   (int) debug_sections[i].line_size_actual);
           printf_filtered ("Relocated LINE size actual: %d\n",
                   (int) debug_sections[i].reloc_line_size_actual);
         }

	/* Check the sizes first */
	if (debug_sections[i].abbrev_size)
	 {
	   debug_sections[i].abbrev =(char *) obstack_alloc 
	    (&dwarf2_tmp_obstack_2, 
	     debug_sections[i].abbrev_size);
	   if (debug_sections[i].abbrev)
	     get_relocated_section_contents (objfile, abfd, objid,
					     debug_sections[i].abbrev_section,
					     debug_sections[i].abbrev,
					 debug_sections[i].reloc_abbrev_size);
	 }

	if (debug_sections[i].info_size)
	  {
	    debug_sections[i].info = (char *) obstack_alloc 
	      (&dwarf2_tmp_obstack_2, 
	       debug_sections[i].info_size);

	    if (debug_sections[i].info)
	      get_relocated_section_contents (objfile, abfd, objid,
					    debug_sections[i].info_section,
					    debug_sections[i].info,
					    (int) debug_sections[i].reloc_info_size);
	  }

	if (debug_sections[i].line_size)
	  {	
	    debug_sections[i].line = (char *) obstack_alloc 
	      (&dwarf2_tmp_obstack_2, 
	       debug_sections[i].line_size);

	    if (debug_sections[i].line)
	      get_relocated_section_contents (objfile, abfd, objid,
					    debug_sections[i].line_section,
					    debug_sections[i].line,
					    (int) debug_sections[i].reloc_line_size);
	  }

	if (debug_sections[i].loc_size)
	  {	
	    /* Note that the relocated copy of the .debug_loc section is
	       allocated on a different obstack than the other sections
	       processed by this function, to make it persistent for later
	       symbol value lookups (as opposed to being deallocated at the
	       end of this function).  Using the psymbol_obstack ties it to
	       the partial symbol table for this .o file and ensures it will
	       be deallocated with the rest of the pst's data (e.g., on a
	       rerun or exec).  JAGag33690, "DOC:  +objdebug not supported
	       with location lists".  Carl Burch, March 14, 2007.
	     */

	    debug_sections[i].loc = (char *) obstack_alloc 
	      (&objfile->psymbol_obstack, 
	       debug_sections[i].loc_size);
	    if (debug_sections[i].loc)
	      get_relocated_section_contents (objfile, abfd, objid,
					    debug_sections[i].loc_section,
					    debug_sections[i].loc,
					    (int) debug_sections[i].loc_size);
	  }

	if (debug_sections[i].line_size_actual)
	  {
	    debug_sections[i].line_actual = (char *) obstack_alloc 
	      (&dwarf2_tmp_obstack_2, debug_sections[i].line_size_actual);

	    if (debug_sections[i].line_actual)
	      get_relocated_section_contents (objfile, abfd, objid,
					    debug_sections[i].line_section_actual,
					    debug_sections[i].line_actual,
					    (int) debug_sections[i].reloc_line_size_actual);
	  }

#ifdef HP_IA64
	if (debug_sections[i].mac_size)
	  {	
	    debug_sections[i].mac= (char *) obstack_alloc 
	      (&dwarf2_tmp_obstack_2, 
	       debug_sections[i].mac_size);

	    if (debug_sections[i].mac)
	      get_relocated_section_contents (objfile, abfd, objid,
					    debug_sections[i].dwarf_macinfo_section,
					    debug_sections[i].mac,
					    (int) debug_sections[i].reloc_mac_size);
	  }
#endif

	debug_sections[i].flags.f_all = 0;
	dwarf_info_buffer = debug_sections[i].info;
	dwarf_abbrev_buffer = debug_sections[i].abbrev;
	dwarf_line_buffer = debug_sections[i].line;
	dwarf_line_buffer_actual = debug_sections[i].line_actual;
	dwarf_loc_buffer = debug_sections[i].loc;
	dwarf_abbrev_size = debug_sections[i].abbrev_size;
	dwarf_info_size = (unsigned int) debug_sections[i].info_size;
	dwarf_line_size = (unsigned int) debug_sections[i].line_size;
	dwarf_loc_size = (unsigned int) debug_sections[i].loc_size;
	dwarf_line_size_actual = (unsigned int) debug_sections[i].line_size_actual;
#ifdef HP_IA64
	dwarf_macinfo_buffer = debug_sections[i].mac;
	dwarf_macinfo_size = (unsigned int) debug_sections[i].mac_size;
#endif
	sectn = i;
	
	/* find an address inside each debug section */
	if (debug_sections[i].reloc_info_size)
	  find_address_in_debug_section (objfile, objfile->section_offsets, pst);
	else
	  {
	    debug_sections[i].lowaddr = MAXINT;
	    debug_sections[i].flags.flag.f_skip = 1;
	  }

	if (debug_sections[i].loc_size)
	  {	
	    /* Fix for JAGag36594, "DOC:  signal 11 in find_line_symtab
	       setting blt in 254.gap +O2 -g".  These fields were using the
	       extension psymtab struct dwarf2_pinfo before it was
	       (inexplicably) allocated by find_address_in_debug_section()
	       above for +objdebug sections.  Carl Burch, April 4, 2007.  */
            DWARF_LOC_SIZE (pst) = debug_sections[i].loc_size;
            DWARF_LOC_BUFFER (pst) = debug_sections[i].loc;
	  }	
    }

  /* sort the debug sections */
  qsort((char *) debug_sections, n_dbg_sects, sizeof (struct debug_section),
	(int (*)(const void *,const void *)) hpread_compare_debug_sects);
  
  /* find the default pst for this object file */
  default_pst = pst;
    
  /* Now read in the symbols */
  last_pst =0;
  for (i = 0; i < n_dbg_sects; i++) 
    {
      if (debug_sections[i].flags.flag.f_skip) 
	{
	  continue;
	}
      else if (debug_sections[i].flags.flag.f_nofunc) 
	{
	  /* debug section has no functions, read into default psymtab */
	  cur_pst = default_pst;
	}
      else 
	{
	  cur_pst = find_pc_psymtab(debug_sections[i].lowaddr);
	  if (!cur_pst)
	    cur_pst = default_pst;
      	}
    
      
      if (cur_pst != last_pst)
	{
	  /* ??? Check to see for "dependent" psymtabs. We don't
	   *  handle these yet, since we don't seem to use them.
	   */
	  if (cur_pst->number_of_dependencies != 0)
	    error("We don't handle dependent psymtabs yet\n");
	  last_source_file = 0;
	  subfile_stack = 0;
	  buildsym_init ();
	  old_chain = make_cleanup (really_free_pendings, 0);
	}

      /* if here than hp compilation, since gcc does not have doom */
      processing_hp_compilation = HP_COMPILED_TARGET;
      processing_gcc_compilation = HP_COMPILED_TARGET;

      dwarf_info_buffer = debug_sections[i].info ;
      dwarf_abbrev_buffer = debug_sections[i].abbrev ;
      dwarf_line_buffer = debug_sections[i].line ;
      dwarf_line_buffer_actual = debug_sections[i].line_actual ;
      dwarf_loc_buffer = debug_sections[i].loc;
      dwarf_abbrev_size = debug_sections[i].abbrev_size;
      dwarf_info_size = (unsigned int) debug_sections[i].info_size;
      dwarf_line_size = (unsigned int) debug_sections[i].line_size;
      dwarf_loc_size = (unsigned int) debug_sections[i].loc_size;
      dwarf_line_size_actual = (unsigned int) debug_sections[i].line_size_actual;
#ifdef HP_IA64
      dwarf_macinfo_buffer = debug_sections[i].mac;
      dwarf_macinfo_size = (unsigned int) debug_sections[i].mac_size;
#endif
      sectn = i;
      cu_header_offset = debug_sections[i].cu_header_offset;
     
      current_objfile = cur_pst->objfile; 
      psymtab_to_symtab_1(cur_pst);
      current_objfile = NULL; 
      
      last_pst = cur_pst;
    }
  

    /* RM: free all the debug stuff, leave the line table in since we do have
     pointers into it */
  for (i = 0; i < n_dbg_sects; i++) 
    {
      sectn = i;
      dwarf2_empty_section_abbrev_table();
    }

  if (last_pst)
    {
      last_pst->readin = 1;
      last_pst->symtab = end_symtab (last_pst->texthigh, objfile, 0);
     
      /* aliases is a global array of alias names for namespaces. It
	 contains all file scoped aliases. We add it to the global 
	 block of the symtab. This must be done after end_symtab 
	 is called and the blockvector is made.*/

      if (aliases && last_pst->symtab)
	{
	  struct block *block = NULL;
          block = BLOCKVECTOR_BLOCK (BLOCKVECTOR(last_pst->symtab), 
				     GLOBAL_BLOCK);
          block->aliases = aliases;
	}
      /* Set symtab language to language from DW_AT_language. */
#ifdef HP_IA64
      if (last_pst->symtab)
#else
      /* If the compilation is from a C/C++ file generated by language
         preprocessors, do not set the language if it was already
         deduced by start_subfile.  */
      if (last_pst->symtab &&
	  !((cu_language == language_c && last_pst->symtab->language != language_c) ||
	    (cu_language == language_cplus && last_pst->symtab->language != language_cplus)))
#endif
        {
          last_pst->symtab->language = cu_language;
        }
      sort_symtab_syms (last_pst->symtab);
      do_cleanups (old_chain);     
    }
  
  current_objfile = NULL;
  hp_dwarf2_object_present = 1;
  bfd_close(abfd);
  if (ar_bfd)
    bfd_close(ar_bfd);

  /* Restore all the globals */
  dwarf_info_buffer = info_saved;
  dwarf_info_offset = info_offset_saved;
  dwarf_info_size = info_size_saved;
  dwarf_abbrev_buffer = abbrev_saved;
  dwarf_abbrev_offset = abbrev_offset_saved;
  dwarf_abbrev_size = abbrev_size_saved;
  dwarf_line_buffer = line_saved;
  dwarf_line_offset = line_offset_saved;
  dwarf_line_size = line_size_saved;
  dwarf_loc_size = loc_size_saved;
  dwarf_loc_buffer = loc_saved;
  dwarf_line_buffer_actual = line_saved_actual;
  dwarf_line_offset_actual = line_offset_saved_actual;
  dwarf_line_size_actual = line_size_saved_actual;
#ifdef HP_IA64
  dwarf_macinfo_size = macinfo_size_saved;
  dwarf_macinfo_offset = macinfo_offset_saved;
  dwarf_macinfo_buffer = mac_saved;
#endif

  cu_language = cu_language_saved;
  cu_language_defn = language_def (cu_language);
  cu_header_offset = cu_header_offset_saved;
  baseaddr = baseaddr_saved;
  data_baseaddr = data_baseaddr_saved;
  
  processing_doom_objfile =0;
  
  do_cleanups (back_to);

 /* Finish up the debug error message.  */
  if (info_verbose)
    printf_filtered ("done.\n");
  
}
#endif /* HP_IA64 */

/* To be used by process_try_block and process_catch_block. */
struct symbol *current_funcsym;
struct try_block *current_t_block;

/* Process a die and its children.  */

static void
process_die (struct die_info *die, struct objfile *objfile,
	     const struct comp_unit_head *cu_header)
{
  struct die_info *child_die;

  switch (die->tag)
    {
    case DW_TAG_compile_unit:
      read_file_scope (die, objfile, cu_header);
      break;
    case DW_TAG_subprogram:
      read_subroutine_type (die, objfile, cu_header);
      read_func_scope (die, objfile, cu_header);
      break;
    case DW_TAG_try_block:
      process_try_block (die, current_funcsym, objfile, cu_header);
      break;
    case DW_TAG_catch_block:
      process_catch_block (die, current_funcsym, objfile, cu_header);
      break;
    case DW_TAG_entry_point:
      /* We need to process entry point dies and make symbols out 
	 of them and add them to the global block so we can find them
	 and set breakpoints at them */
      if ((cu_language == language_fortran) 
	  || (cu_language == language_cplus)) {
        read_subroutine_type (die, objfile, cu_header);
        read_func_scope (die, objfile, cu_header);
      }
      break;
      /* RM: HP uses modules, just read the children */
    case DW_TAG_module:
      if (die->has_children)
	{
	  child_die = die->next;
	  while (child_die && child_die->tag)
	    {
	      /* import_lowpc etc are globals - a using directive
		 is made into a block and gets the pc values (from 
		 import_lowpc etc.) of the the containing unit - 
		 be it in comp unit scope or function scope. Needs
		 to be in the loop since process_die could process a 
		 child function and that would change its value. */
	      import_lowpc = comp_unit_lowpc + baseaddr;
	      process_die (child_die, objfile, cu_header);
	      child_die = sibling_die (child_die);
	    }
	}
	break;
    case DW_TAG_inlined_subroutine:
      if (inline_debugging)
        read_lexical_block_scope (die, objfile, cu_header);
      break;
    case DW_TAG_lexical_block:
      read_lexical_block_scope (die, objfile, cu_header);
      break;
    case DW_TAG_namespace:
      read_namespace_scope (die, objfile, cu_header);
      break;
    case DW_TAG_imported_declaration:
      if (dwarf_attr (die, DW_AT_name))
	{
	  aliases = process_alias (die, aliases, objfile, cu_header);
	}
      else
	read_imported_declaration (die, objfile, cu_header);
      break;
    case DW_TAG_imported_module:
      (void) read_imported_module (die, objfile, cu_header);
      break;
    case DW_TAG_class_type:
    case DW_TAG_structure_type:
    case DW_TAG_union_type:
      read_structure_scope (die, objfile, cu_header);
      break;
    case DW_TAG_enumeration_type:
      read_enumeration (die, objfile, cu_header);
      break;
    case DW_TAG_subroutine_type:
      read_subroutine_type (die, objfile, cu_header);
      break;
    case DW_TAG_array_type:
      read_array_type (die, objfile, cu_header);
      break;
    case DW_TAG_pointer_type:
      read_tag_pointer_type (die, objfile, cu_header);
      break;
    case DW_TAG_ptr_to_member_type:
      read_tag_ptr_to_member_type (die, objfile, cu_header);
      break;
    case DW_TAG_reference_type:
      read_tag_reference_type (die, objfile, cu_header);
      break;
    case DW_TAG_string_type:
      read_tag_string_type (die, objfile, cu_header);
      break;
    case DW_TAG_base_type:
      read_base_type (die, objfile);
      if (dwarf_attr (die, DW_AT_name))
	{
	  /* Add a typedef symbol for the base type definition.  */
	  new_symbol (die, die->type, objfile, cu_header);
	}
      break;
    case DW_TAG_common_block:
      read_common_block (die, objfile, cu_header);
      break;
    case DW_TAG_common_inclusion:
      break;
#ifdef HP_DWARF2_EXTENSIONS
    case DW_TAG_HP_array_descriptor:
      read_array_descriptor_type (die, objfile, cu_header);
      break;
#endif
    case DW_TAG_typedef:
      if (TYPE_DECLARATION(die->type) != 1)
         new_symbol (die, NULL, objfile, cu_header);
      break;
    default:
      new_symbol (die, NULL, objfile, cu_header);
      break;
    }
}

/* Decode the line number information for the compilation unit whose
   line number info is at OFFSET in the .debug_line section.
   The compilation directory of the file is passed in COMP_DIR.  */

struct filenames
{
  unsigned int num_files;
  struct fileinfo
    {
      char *name;
      unsigned int dir;
      unsigned int time;
      unsigned int size;
    }
   *files;
};

struct directories
  {
    unsigned int num_dirs;
    char **dirs;
  };

#ifdef HP_IA64
/*
 * Global variables used to hold the file and directory table information.
 * Table information will be copied into in these structures before they are destroyed.
 * The table information is required while internalising the macro info to map the 
 * the file indexes to absolute path name.
 */
struct filenames gfiles;
struct directories gdirs;

#endif

#define LOW_PC_UNDEF ((CORE_ADDR) (long) -1)
#define HIGH_PC_UNDEF ((CORE_ADDR) 0)

static void
read_file_scope (struct die_info *die, struct objfile *objfile,
		 const struct comp_unit_head *cu_header)
{
  unsigned int line_offset = 0;
  unsigned int line_offset_actual = 0;
  CORE_ADDR lowpc = LOW_PC_UNDEF;
  CORE_ADDR highpc = HIGH_PC_UNDEF;
  struct attribute *attr;
  char *name = "<unknown>";
  char *comp_dir = NULL;
  struct die_info *child_die;
  bfd *abfd = objfile->obfd;
  /* initialize all namespace globals to 0 */
  num_globusings = 0;
  max_globusings = 0;
  global_using_dirs = NULL;
  num_anon_ns = 0;

  /* Stacey 10/26/2001 - JAGad87549
     Make sure gcc_compiled is set properly so that we will able to
     detect if we are running gcc compiled code.  This will allow us to
     Disable HP-specific functions when running on gcc compiled
     code later on, allowing gdb to work better with gcc and g++ */

  processing_gcc_compilation = HP_COMPILED_TARGET;

  attr = dwarf_attr (die, DW_AT_producer);
  if (attr)
    {
      if (strstr(DW_STRING(attr),"GNU C") == DW_STRING(attr))
        {
          processing_gcc_compilation = 2;
          /* removed some g++ version specific checks here. */
          if (strstr(DW_STRING(attr),"GNU C++") == DW_STRING(attr)) 
	    {
              set_demangling_style (GNU_NEW_ABI_DEMANGLING_STYLE_STRING);
	      demangle_the_whole_world();
	    }
        }
    }

  enum PC_bounds_rv rv = dwarf2_get_pc_bounds (die, &lowpc, &highpc, objfile);
  if (rv == PCB_ignore)
    {
      return;
    }
  else if (rv == PCB_invalid)
    {
      if (die->has_children)
	{
	  child_die = die->next;
	  while (child_die && child_die->tag)
	    {
	      if (child_die->tag == DW_TAG_subprogram)
		{
		  CORE_ADDR low, high;

		  /* Find the highest optimization level in the file */
  		  attr = dwarf_attr (child_die, DW_AT_HP_opt_level);
  		  if (attr)
    		    file_opt_level = (int) (max (DW_UNSND (attr), 
                                                 file_opt_level));

		  if (dwarf2_get_pc_bounds (child_die, &low, &high, 
                                            objfile) == PCB_ok)
		    {
		      lowpc = min (lowpc, low);
		      highpc = max (highpc, high);
		    }
		}
	      child_die = sibling_die (child_die);
	    }
	}
    }

  if (lowpc != LOW_PC_UNDEF)
    lowpc += baseaddr;
  if (highpc != HIGH_PC_UNDEF)
    highpc += baseaddr;

  lowpc = SWIZZLE(lowpc);
  highpc = SWIZZLE(highpc);

  comp_unit_lowpc = lowpc;

  attr = dwarf_attr (die, DW_AT_name);
  if (attr)
    {
      name = DW_STRING (attr);
    }
  attr = dwarf_attr (die, DW_AT_comp_dir);
  if (attr)
    {
      comp_dir = DW_STRING (attr);
      if (comp_dir)
	{
	  /* Irix 6.2 native cc prepends <machine>.: to the compilation
	     directory, get rid of it.  */
	  char *cp = strchr (comp_dir, ':');

	  if (cp && cp != comp_dir && cp[-1] == '.' && cp[1] == '/')
	    comp_dir = cp + 1;
	}
    }

  if (SWIZZLE(objfile->ei.entry_point) >= lowpc &&
      SWIZZLE(objfile->ei.entry_point) < highpc)
    {
      objfile->ei.entry_file_lowpc = lowpc;
      objfile->ei.entry_file_highpc = highpc;
    }

  attr = dwarf_attr (die, DW_AT_language);
  if (attr)
    {
      set_cu_language ((unsigned int) DW_UNSND (attr));
    }

  /* Determine if compiler was the HP or GCC compiler */
  attr = dwarf_attr (die, DW_AT_producer);
  if (attr)
    {
      handle_producer (attr->u.str);
    }

  /* The compilation unit may be in a different language or objfile,
     zero out all remembered fundamental types.  */
  memset (ftypes, 0, FT_NUM_MEMBERS * sizeof (struct type *));

  if (!last_source_file)
    {
      start_symtab (name, comp_dir, lowpc);
      record_debugformat ("DWARF 2");
    }

  /* Decode line number information if present.  */
  attr = dwarf_attr (die, DW_AT_stmt_list);
  if (attr)
    {
      line_offset = (unsigned int) DW_UNSND (attr);
      dwarf_decode_lines (line_offset, comp_dir, abfd);
    }

  /* Decode line number information if present.  */
  attr = dwarf_attr (die, DW_AT_HP_actuals_stmt_list);
  if (attr)
    {
      line_offset_actual = (unsigned int) DW_UNSND (attr);
      dwarf_decode_lines_actual (line_offset_actual, 
      				 comp_dir, 
				 abfd, 
				 file_opt_level);
    }
    
  /* Decode macro information, if present.  Dwarf 2 macro information
     refers to information in the line number info statement program
     header, so we can only read it if we've read the header
     successfully.  */
#ifdef HP_IA64
  attr = dwarf_attr (die, DW_AT_macro_info);
  if (attr)
    {
      unsigned int macro_offset = DW_UNSND (attr);
      dwarf_decode_macros (macro_offset, comp_dir, abfd, objfile);
    }
#endif

  /* Process all dies in compilation unit.  */
  if (die->has_children)
    {
      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  process_die (child_die, objfile, cu_header);
	  child_die = sibling_die (child_die);
	}
    }
}

/* We add namespace aliases to an array of aliases */

static struct aliases *
process_alias (struct die_info *die, struct aliases *aliases, 
		struct objfile *objfile, const struct comp_unit_head *cu_header)
{
  char *name;
  char *alias;
  struct attribute *attr;
  struct die_info *ref_die;

  attr = dwarf_attr (die, DW_AT_name);
  alias = obsavestring (DW_STRING (attr),
			(int) strlen (DW_STRING (attr)),
			&objfile->type_obstack);

  attr = dwarf_attr (die, DW_AT_import);
  ref_die = find_die_from_ref (die, objfile, attr);
  
  attr = dwarf_attr (ref_die, DW_AT_name);
  name = obsavestring (DW_STRING (attr),
			(int) strlen (DW_STRING (attr)),
			&objfile->type_obstack);

  if (ref_die->tag != DW_TAG_namespace)
    {
      /* This is an Open MP alias variable, not a namespace object;
	 create a new symbol using the ref_die, and replace the name
	 with the alias name */
      struct symbol *sym = new_symbol (ref_die, NULL, objfile, cu_header);
      SYMBOL_NAME (sym) = alias;
      return aliases;
    }

  if (!aliases || aliases->num_aliases >= aliases->max_aliases)
    {
      if (aliases == NULL)
	{
	  aliases = (struct aliases *) xmalloc ( sizeof (struct aliases));
	  aliases->max_aliases = INIT_MAX_ALIASES;
	  aliases->num_aliases = 0;
	  aliases->alias = (char **) xmalloc(sizeof(char *) * INIT_MAX_ALIASES);
	  aliases->name  = (char **) xmalloc(sizeof(char *) * INIT_MAX_ALIASES);
	}
      else
	{
	  aliases->alias = (char **) xrealloc (aliases->alias,
						((sizeof (char *)) *
						 (aliases->max_aliases * 2)));
	  aliases->name  = (char **) xrealloc (aliases->name,
						((sizeof (char *)) *
						 (aliases->max_aliases * 2)));
	  aliases->max_aliases *= 2;
	}
    }
  aliases->alias[aliases->num_aliases] = alias;
  aliases->name[aliases->num_aliases++] = name;
  return aliases;
}

/* Bindu 072204: Process a try_block die which contains low and highpc attributes */
static void
process_try_block (struct die_info* die, struct symbol *sym,
                   struct objfile* objfile,
		   const struct comp_unit_head *cu_header)
{
  CORE_ADDR lowpc;
  CORE_ADDR highpc;
  struct die_info *child_die;
  struct try_block *t_block;

  if (die->tag != DW_TAG_try_block)
    return;

  enum PC_bounds_rv rv = dwarf2_get_pc_bounds (die, &lowpc, &highpc, objfile);
  if (rv == PCB_invalid)
    {
      warning ("No pc bounds in try block\n");
      current_t_block = NULL;
      return;
    }
  else if (rv == PCB_ignore)
    {
      current_t_block = NULL;
      return;
    }
  lowpc += baseaddr;
  highpc += baseaddr;

  t_block = (struct try_block *) (void *) obstack_alloc
                        (&objfile->symbol_obstack, sizeof (struct try_block));
  memset (t_block, 0, sizeof (struct try_block));
  t_block->low_pc = lowpc;
  t_block->high_pc = highpc;

  /* Add the new try block to the top of the list. */
  t_block->next = SYMBOL_TRY_BLOCK (sym);
  SYMBOL_TRY_BLOCK (sym) = t_block;
  t_block->c_block = NULL;

  if (die->has_children)
    {
      child_die = die->next;
      while (child_die && child_die->tag)
        {
          process_die (child_die, objfile, cu_header);
          child_die = sibling_die (child_die);
        }
    }

  /* Note the current t_block so that the sibling c_blocks can attach to this. */
  current_t_block = t_block;
  return;
}

/* Bindu 072204: Process a catch block die which contains lowpc, highpc, type and
   selector attributes. */
static void
process_catch_block (struct die_info* die, struct symbol *sym,
                   struct objfile* objfile,
		   const struct comp_unit_head *cu_header)
{
  CORE_ADDR lowpc;
  CORE_ADDR highpc;
  struct catch_block *c_block;
  struct attribute *attr;
  struct die_info *child_die;

  if (die->tag != DW_TAG_catch_block)
    return;

  enum PC_bounds_rv rv = dwarf2_get_pc_bounds (die, &lowpc, &highpc, objfile);
  if (rv == PCB_invalid)
    {
      warning ("No pc bounds in catch block\n");
      return;
    }
  else if (rv == PCB_ignore)
    {
      return;
    }
  else if (!current_t_block)
    {
      warning ("No associated try block exists\n");
      return;
    }

  lowpc += baseaddr;
  highpc += baseaddr;

  read_subroutine_type (die, objfile, cu_header);

  c_block = (struct catch_block *) (void *) obstack_alloc
                        (&objfile->symbol_obstack, sizeof (struct catch_block));
  memset (c_block, 0, sizeof (struct catch_block));
  c_block->low_pc = lowpc;
  c_block->high_pc = highpc;
  c_block->type = die->type;
  attr = dwarf_attr (die, DW_AT_HP_exception_selector);
  if (attr)
    {
      c_block->selector_val = DW_UNSND (attr);
    }
  else
    {
      warning ("No DW_AT_HP_exception_selector attribute in catch_block.");
    }

  /* JAGaf70489 - convert formal_parm to variable.  the parm to catch
   * block is not really a parm it's an exception declaration. 
   */

  if (die->has_children)
  {
     child_die = die->next;
     while (child_die && child_die->tag)
     {
        if (child_die->tag == DW_TAG_formal_parameter)
           child_die->tag = DW_TAG_variable;

        child_die = sibling_die (child_die);
     }
  }

  /* process children block as lexical block */
  read_lexical_block_scope(die, objfile, cu_header); 

  c_block->next = current_t_block->c_block;
  current_t_block->c_block = c_block;
  return;
}


static void
read_func_scope (struct die_info *die, struct objfile *objfile,
                 const struct comp_unit_head *cu_header)
{
  register struct context_stack *new;
  CORE_ADDR lowpc;
  CORE_ADDR highpc;
  struct die_info *child_die;
  struct attribute *attr;
  struct attribute *frame_base_attr;  /* "attr" from DW_AT_frame_base DIE */
  char *name;
  unsigned int locdesc_offset;
  struct die_info *ref_die = 0; /* initialize for compiler warning */
  int entry_count = 0;
  int is_virtual;
  struct pending **old_list_in_scope;
  int nparams;
  struct aliases *aliases = NULL;
  struct block *block = NULL;
  int num_locusings = 0;
  int max_locusings = 0;
  struct symbol ** local_using_dirs = NULL;

  name = dwarf2_linkage_name (die);

  /* bindu: check if this is a pure virtual function. */
  attr = dwarf_attr (die, DW_AT_virtuality);
  if (attr)
    {
      if (STREQ(old_vtable,"on"))
	is_virtual = (int) DW_UNSND (attr) - 1;
      else
	is_virtual = (int) DW_UNSND (attr);
      if (is_virtual == DW_VIRTUALITY_pure_virtual) /* pure virtual*/
        new_symbol(die, die->type, objfile, cu_header);
    }          
 
  if (dwarf2_get_pc_bounds (die, &lowpc, &highpc, objfile) != PCB_ok)
    return;

#ifdef HP_IA64
  /* A subroutine will have a DW_AT_count attribute whose value
     is > 1 if there are entry points.  In this case, ignore the
     subroutine die so that we can avoid duplicates symbols for
     the subroutine and its associated entry point.  We need to 
     traverse the subroutine's children to get the entry points. */

  attr = dwarf_attr (die, DW_AT_count);
  if (attr)
    entry_count = (int) DW_UNSND (attr);
  if (cu_language == language_fortran && entry_count > 1)
    {
      child_die = die->next;
      /* The entry points follow the formal parameter list 
         so we need to bypass the paramters. */
      while (child_die && child_die->tag == DW_TAG_formal_parameter)
        child_die = sibling_die (child_die);
      while (child_die && child_die->tag)
        {
          process_die (child_die, objfile, cu_header);
          child_die = sibling_die (child_die);
          entry_count--;
        }
      if (entry_count != 0)
        complain (&dwarf2_unexpected_entry_point_count, entry_count); 
      return;
    }
#endif

  /* Ignore functions with missing or empty names and functions with
     missing or invalid low and high pc attributes.  */
  /* Poorva: Can't ignore funcs with missing name since it may be
     the definition for a function which has been declared. This
     func will have a spec attribute pointing back to the declaration.
     It is the declaration die which contains the children dies like formal
     parameters to the function. 

     So when we find a function with the hi_pc, lo_pc bounds we
     check to see if it has a DW_AT_specification attribute. If it
     does have the spec attribute we get a handle to the declaration
     die (ref_die). We then install the ref_die's type in the symbol table
     since that will have the correct count of the fields, and a pointer
     to the correct fields. We also go ahead and process the children
     attached to the declaration die and then process children attached to
     this die*/
     
  lowpc += baseaddr;
  highpc += baseaddr;
  
  lowpc = SWIZZLE(lowpc);
  highpc = SWIZZLE(highpc);

  if (SWIZZLE(objfile->ei.entry_point) >= lowpc &&
      SWIZZLE(objfile->ei.entry_point) < highpc)
    {
      objfile->ei.entry_func_lowpc = lowpc;
      objfile->ei.entry_func_highpc = highpc;
    }
  
#ifdef HP_IA64
  current_func_lowpc = lowpc;
  current_func_highpc = highpc;
#endif /* HP_IA64 */

#ifdef HP_DWARF2_EXTENSIONS
  func_opt_level = 0;
  attr = dwarf_attr (die, DW_AT_HP_opt_level);
  if (attr)
    func_opt_level = (int) DW_UNSND (attr);
  else
    func_opt_level = 0;
#endif
  
  /* Decode DW_AT_frame_base location descriptor if present, keep result
     for DW_OP_fbreg operands in decode_locdesc.  */
  frame_base_reg = -1;
  frame_base_offset = 0;
  attr = dwarf_attr (die, DW_AT_frame_base);
  frame_base_attr = attr;
  if (attr)
    {
      CORE_ADDR addr = decode_locdesc (NULL, attr, objfile,
				       &locdesc_offset);
      if (isderef)
	complain (&dwarf2_unsupported_at_frame_base, name);
      if (ispiece)
	complain (&dwarf2_unsupported_at_frame_base, name);
      else if (isreg)
	frame_base_reg = (int) addr;
      else if (offreg)
	{
	  frame_base_reg = basereg;
	  frame_base_offset = addr;
	}
      else
	complain (&dwarf2_unsupported_at_frame_base, name);
    }
  
  new = push_context (0, lowpc, 0);
  
  /* Poorva: if a specification attribute is present then
     it has already been entered as a symbol. We want to 
     get the type previously entered with the name since that type
     will have the correct number of fields etc. 
     enter that to the new_symbol routine. We need to get
     a handle to the symbol since we need to call finish_block 
     to enter block information with the low and high pc values 
     that we have now  */
  
  attr = dwarf_attr( die, DW_AT_specification );

  if (attr)
    {
      ref_die = find_die_from_ref (die, objfile, attr);

      name = dwarf2_linkage_name (ref_die);
      if (ref_die->type == NULL)
       process_die (ref_die, objfile, cu_header);
       
    }  

  /* FIXME Poorva Needs to be removed when we have mangled names */
  if (cu_language == language_cplus && !(name[0] == ' ') 
      && !is_cplus_name(name, DEMANG))     /* JAGaf49121 */
    {
      struct minimal_symbol *msymbol;
      /* Find the minimal_symbol by the pc address and get the 
	 mangled name from there */
      msymbol = lookup_minimal_symbol_by_pc (lowpc); 
      if (msymbol && msymbol->ginfo.name) 
	{
	  struct attribute* name_attr = dwarf_attr(die, DW_AT_name);
	  if (name_attr != 0) /* attribute for name */
	    {
	      name_attr->u.str = 
	        (char *) xmalloc (strlen (msymbol->ginfo.name) + 1);
	      strcpy (name_attr->u.str, msymbol->ginfo.name);
	    }
	}	 
    } 

  if ( attr )
    {
      /* Poorva: Get  handle to the declaration die - done above
	 - we call it ref_die and install the type of ref_die in 
	 the symbol table. */
      new->name = new_symbol (die, ref_die->type, objfile, cu_header);
      /* JAGae39990 - If the type is not in the reference die,
         get it from the current die.  This occurs for g++. */
      if (ref_die->type)
         current_func_type = ref_die->type;
      else if (die->type)
         current_func_type = die->type;
    }
  else    
    {
      new->name = new_symbol (die, die->type, objfile, cu_header);
      current_func_type = die->type;
    }
   
  if (!new->name)
    return;

  /* If there was a location expression for DW_AT_frame_base above,
     record it.  We still need to decode it above because not all
     symbols use location expressions exclusively.  */
  if (frame_base_attr)
    dwarf2_symbol_mark_computed (frame_base_attr, new->name, cu_header, objfile);

#ifdef HP_DWARF2_EXTENSIONS
  new->name->ginfo.opt_level = func_opt_level;
#endif
  old_list_in_scope = list_in_scope;
  list_in_scope = &local_symbols;
#ifdef HP_IA64
  current_func_lowpc = lowpc;
  current_func_highpc = highpc;
  /* The lowpc of the Fortran internal function is < the lowpc of 
     its enclosing function so we need to set last_source_start_addr 
     in order that the symtab has the lowpc of the internal function */
  if (context_stack_depth > 0 && lowpc < last_source_start_addr)
    last_source_start_addr = lowpc;
  /* Changes the global so that using directives in this function can make
     blocks with the pc values of this fucntion */
  import_lowpc = lowpc;
#endif /* HP_IA64 */  
 
  if (attr)
    {
      if (ref_die->has_children)
	{
          struct die_info *first_child_die = NULL;
	  child_die = ref_die->next;
          nparams = 0;
	  while (child_die && child_die->tag)
	    {
#ifdef HP_IA64
	      /* If the subroutine has space for an aggregate return
		 passed in a parameter, under the IPF C++ ABI that
		 pointer is the first parameter, followed by the
		 "this" pointer.  This is the only case where the 
		 "this" pointer can be the second parameter.  
		 In this case, set retval_ptr_idx to
		 that parameter number (zero in practice).  */

    	      if (child_die->tag == DW_TAG_formal_parameter)
    	        {
    	          attr = dwarf_attr (child_die, DW_AT_name);
    	          if (attr && DW_STRING (attr) &&
      		      dwarf_attr (child_die, DW_AT_artificial))
    		    {
    		      if ((nparams == 1)  &&
			  (!strncmp(DW_STRING (attr), "this", 4)))
    		        {
			  /* Don't check that the function has a
			     return type like the code below (that
			     apparently works for g++).  aC++ tells us
			     member functions using this IPF C++ ABI
			     return void.  Change the function's
			     return type to that of its first
			     argument.*/
			  if (ref_die->type == NULL)
			    read_subroutine_type (ref_die, objfile, cu_header);
			  if (ref_die->type == NULL)
			    internal_error ("Cannot get type of ref_die\n");

	                  TYPE_TARGET_TYPE (ref_die->type) = 
			    TYPE_TARGET_TYPE (die_type(first_child_die, objfile, cu_header));
    		          TYPE_RETVAL_PTR_IDX(ref_die->type) = 0;
    		        }
    		    }
		  /* save the previous param's pointer, as it may be the 
		     return value pointer */
		  if (nparams == 0)
    		    first_child_die = child_die;
    	          nparams++;
                }
#endif /* HP_IA64 */  
	      process_die (child_die, objfile, cu_header);
	      import_lowpc = lowpc;
	      child_die = sibling_die (child_die);
	    }
	}
    }
  
  if (die->has_children)
    {
      child_die = die->next;
      /* Remember current symbol to be used by try blocks if any. */
      current_funcsym = new->name;
      nparams = 0;
      while (child_die && child_die->tag)
	{
	  /* If the subroutine has space for an aggregate return passed
	     in a parameter, that parameter is named "#aggretx" followed
	     by something, e.g. "#aggretxform#2".  In this case, 
	     set retval_ptr_idx to that parameter number (zero in
	     practice).  aCC6/EDG uses a prefix of "No.Identifier_" for
	     the same purpose - JAGag28716, "bad return value printed for
	     command line call to fn w/struct return val".
	     */
	  if (child_die->tag == DW_TAG_formal_parameter)
	    {
	      attr = dwarf_attr (child_die, DW_AT_name);
	      if (attr && DW_STRING (attr))
		{
		  if ((!strncmp(DW_STRING (attr), "#aggretx", 8)) ||
		      (!strncmp(DW_STRING (attr), "No.Identifier_", 14)))
		    {
                      if (current_func_type) /* JAGae39990 */
		         TYPE_RETVAL_PTR_IDX(current_func_type) = nparams;
		    }
                  /* QXCR1000835513: ptype of fortran character-valued 
                     function is shown as VOID.
                     The type of fortran character-valued function
                     is stored in the compiler generated variable
                     "_original_function_result" as formal parameter. */
                  if ((cu_language == language_fortran) &&
                      (!(strncmp(DW_STRING (attr), 
                                 "_original_function_result", 25))))
                    {
                      if (current_func_type)
                        TYPE_TARGET_TYPE (current_func_type) =
                             die_type (child_die, objfile, cu_header);
                    }
		}
	      nparams++;
            }
#ifdef HP_IA64
	  if (child_die->tag == DW_TAG_imported_module)
	    {
	      if (num_locusings == max_locusings)
		{
		  if (max_locusings == 0)
		    max_locusings = INIT_GLOBAL_USINGS;
		  else
		    max_locusings *= 2;
		  local_using_dirs = (struct symbol **) xrealloc 
		    (local_using_dirs, 
		     max_locusings * sizeof (struct symbol *));
		}
	      local_using_dirs[num_locusings++] = 
		read_imported_module (child_die, objfile, cu_header);
	    }
	  else if (child_die->tag == DW_TAG_imported_declaration &&
		   (dwarf_attr (child_die, DW_AT_name)))
	    {
	      aliases = process_alias (child_die, aliases, objfile, cu_header);
	    }
	  else if (child_die->tag == DW_TAG_subprogram)
	    {
              /* for inlined routine this arg will be defined and would
                 have non-zero value.
                 Read DWARf3 specs.
              */
	      attr = dwarf_attr (child_die, DW_AT_inline);
              /* QXCR1000549293 : "Mismatch between bkpt confirmation line # and
                 bkpt arrival line number". "IF" condition is modified so that
                 "list_in_scope" is updated whenever not a case of inline
                 function.
              */
  	      if (((attr) && (DW_UNSND (attr) == 0)) || !(attr))
                /* not an inlined function */
	        list_in_scope = old_list_in_scope; /* start scope for subprogram */
              /* TODO: Else part needs to be handled" */

	      process_die (child_die, objfile, cu_header);
	    }
	  else
#endif
	    process_die (child_die, objfile, cu_header);

	  import_lowpc = lowpc;
	  child_die = sibling_die (child_die);
	}
    }
  
  new = pop_context ();

  if (num_locusings)
    {
      struct symbol *sym;
      int i =0;
      for (i = 0; i < num_locusings; ++i)
	{
	  sym = local_using_dirs[i];
	  do_finish_block (sym, &local_symbols, new->old_blocks, lowpc,
			highpc, objfile, new->is_inlined, new->inline_index);
	}
      num_locusings = 0;
      max_locusings = 0;
      free (local_using_dirs);
      local_using_dirs = NULL;
    }
  
  /* Make a block for the local symbols within.  */
  do_finish_block (new->name, &local_symbols, new->old_blocks,
		lowpc, highpc, objfile, new->is_inlined, new->inline_index);

  /* We need to add aliases to the block formed by this function. 
     finish_block attaches the block to the symbol new->name (misnomer)
     and we get block from it. 
     Also we attach the symbol to the type - for handling using declarations
     of functions. When we see a using declaration for a function 
     we can get the symbol from the type of the die. We then clone 
     this symbol and change its name 
     */
 
  block = SYMBOL_BLOCK_VALUE (new->name);
  block->aliases = aliases;

#ifdef HP_IA64
  current_func_lowpc = 0;
  current_func_highpc = 0;
#endif /* HP_IA64 */

/* un-set the func_opt_level before exiting. */
#ifdef HP_DWARF2_EXTENSIONS
  func_opt_level = 0;
#endif /* HP_DWARF2_EXTENSIONS */

#ifdef HP_IA64
  if (context_stack_depth > 0)
    {
      local_symbols = new->locals;
      list_in_scope = &local_symbols;
    }
  else
#endif
  list_in_scope = &file_symbols;

}

/* Process all the DIES contained within a lexical block scope.  Start
   a new scope, process the dies, and then close the scope.  */

static void
read_lexical_block_scope (struct die_info *die, struct objfile *objfile,
			   const struct comp_unit_head *cu_header)
{
  register struct context_stack *new;
  CORE_ADDR lowpc, highpc;
  struct die_info *child_die;
  struct pending **old_list_in_scope;
  struct aliases *aliases = NULL;
  struct block *block = NULL;
  int num_locusings = 0;
  int max_locusings = 0;
  struct symbol **local_using_dirs = NULL;

  /* Ignore blocks with missing or invalid low and high pc attributes.  */
  if (dwarf2_get_pc_bounds (die, &lowpc, &highpc, objfile) != PCB_ok)
    return;

  lowpc += baseaddr;
  highpc += baseaddr;

  lowpc = SWIZZLE(lowpc);
  highpc = SWIZZLE(highpc);

  import_lowpc = lowpc;

  if (inline_debugging && die->tag == DW_TAG_inlined_subroutine)
    {
      int idx = get_inline_index (lowpc, highpc);
      /* If idx is 0 here, we have a problem with either debug info or
	 the gdb's digestion of debug info. */
      struct context_stack *p = push_context (0, lowpc, idx);
      p->end_addr = highpc;
    }
   /* JAGag41955: assign the end_addr of the context to help in check
      in push_context. */
  else if (inline_debugging && die->tag == DW_TAG_lexical_block)
    {
      struct context_stack *p = push_context (0, lowpc, 0);
      p->end_addr = highpc;
    }
  else
    {
      push_context (0, lowpc, 0);
    }

  old_list_in_scope = list_in_scope;
  list_in_scope = &local_symbols;
  if (die->has_children)
    {
      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  if (child_die->tag == DW_TAG_imported_module)
            {
              if (num_locusings == max_locusings)
                {
                  if (max_locusings == 0)
                    max_locusings = INIT_GLOBAL_USINGS;
                  else
                    max_locusings *= 2;
                  local_using_dirs = (struct symbol **) xrealloc
                    (local_using_dirs,
                     max_locusings * sizeof (struct symbol *));
                }
              local_using_dirs[num_locusings++] =
                read_imported_module (child_die, objfile, cu_header);
            }
	  else if (child_die->tag == DW_TAG_imported_declaration)
	    {
	      if (dwarf_attr (child_die, DW_AT_name))
		{
		  aliases = process_alias (child_die, aliases, objfile, 
								cu_header);
		}
	    }
	  else
	    process_die (child_die, objfile, cu_header);
	  
	  import_lowpc = lowpc;
	  child_die = sibling_die (child_die);
	}
    }
  new = pop_context ();
  list_in_scope = old_list_in_scope;

  if (num_locusings)
    {
      struct symbol *sym;
      int i =0;
      for (i = 0; i < num_locusings; ++i)
	{
	  sym = local_using_dirs[i];
	  do_finish_block (sym, &local_symbols, new->old_blocks, new->start_addr,
			highpc, objfile, new->is_inlined, new->inline_index);
	  local_using_dirs[i] = NULL;
	}
      num_locusings = 0;
      max_locusings = 0;
      free (local_using_dirs);
      local_using_dirs = NULL;
    }

  /* Add block even when there are no local symbols for inlined
     instances. */
  if (   (local_symbols != NULL)
      || (inline_debugging && die->tag == DW_TAG_inlined_subroutine))
    {
      extern struct pending_block *pending_blocks;
      do_finish_block (0, &local_symbols, new->old_blocks, new->start_addr,
		    highpc, objfile, new->is_inlined, new->inline_index);
      /* The latest block in the list of pending_blocks is this one */
      block = pending_blocks->block;
      if (block)
	block->aliases = aliases;
    }
  local_symbols = new->locals;
}

static void
handle_producer (char *producer)
{
    processing_bad_pieces = false;

    /* Set the correct processing_*_compilation flag */
    if (STREQN (producer, GCC_PRODUCER, strlen (GCC_PRODUCER)))
      {
        processing_gcc_compilation = producer[strlen (GCC_PRODUCER)]; 
        processing_hp_compilation = 0;
      }
    else if (STREQN (producer, GPLUS_PRODUCER, strlen (GPLUS_PRODUCER)))
      {
        processing_gcc_compilation = producer[strlen (GPLUS_PRODUCER)];
        processing_hp_compilation = 0;
      }
    else
      {
        processing_gcc_compilation = 0;
        processing_hp_compilation = HP_COMPILED_TARGET;

	/* Prior to A.06.01, HP's compilers would list the GR's in a pair
	 * in the wrong order.  
	 */

	if (strncmp (producer, "A.06.00", 7) <= 0)
	  processing_bad_pieces = true;
      }
}

/* Get low and high pc attributes from a die.
   Return 1 if the attributes are present and valid, otherwise, return 0.  */

static enum PC_bounds_rv
dwarf2_get_pc_bounds (struct die_info *die, CORE_ADDR *lowpc, 
                      CORE_ADDR *highpc, struct objfile *objfile)
{
  struct attribute *attr;
  CORE_ADDR low;
  CORE_ADDR high;

  attr = dwarf_attr (die, DW_AT_low_pc);
  if (attr)
    low = DW_ADDR (attr);
  else
    return PCB_invalid;
  attr = dwarf_attr (die, DW_AT_high_pc);
  if (attr)
    high = DW_ADDR (attr);
  else
    return PCB_invalid;

  if (high < low)
    return PCB_invalid;

  /* Both low and high must fall into the objfile's text segment. If they 
     dont't, it's an unrelocated die left over from +Oprocelim. Check both
     link time and run time addresses. */
  if ((low < objfile->obfd->text_start_address || 
       low > objfile->obfd->text_start_address + objfile->obfd->text_size) &&
      (SWIZZLE(low) < objfile->text_low || 
       SWIZZLE(low) > objfile->text_low + objfile->obfd->text_size))
    return PCB_ignore;

  /* When using the GNU linker, .gnu.linkonce. sections are used to
     eliminate duplicate copies of functions and vtables and such.
     The linker will arbitrarily choose one and discard the others.
     The AT_*_pc values for such functions refer to local labels in
     these sections.  If the section from that file was discarded, the
     labels are not in the output, so the relocs get a value of 0.
     If this is a discarded function, mark the pc bounds as invalid,
     so that GDB will ignore it.  */
  if (low == 0 && (bfd_get_file_flags (objfile->obfd) & HAS_RELOC) == 0)
    return PCB_invalid;

  low = ADJUST_IA64_SLOT_ENCODING (low);
  high = ADJUST_IA64_SLOT_ENCODING (high);

  *lowpc = SWIZZLE(low);
  *highpc = SWIZZLE(high);
  return PCB_ok;
}

/* Add an aggregate field to the field list.  */

static void
dwarf2_add_field (struct field_info *fip, struct die_info *die,
		  struct objfile *objfile,
		  struct die_info *parent_die,
		  const struct comp_unit_head *cu_header)
{
  struct nextfield *tmp_field;
  struct nextfield *new_field;
  struct attribute *attr;
  struct field *fp;
  char *fieldname = "";
  unsigned int locdesc_offset;
  int i;

  /* Allocate a new field list entry and link it in.  */
  /* RM: Add base classes after data members. gdb relies on this
     ordering (actually the reverse of it, but this list will get
     flipped later), but Dwarf2 doesn't guarantee it */
  new_field = (struct nextfield *) xcalloc (1, sizeof (struct nextfield));
  make_cleanup (free, new_field);
  if (die->tag != DW_TAG_inheritance)
    {
      new_field->next = fip->fields;
      fip->fields = new_field;
    }
  else
    {
      i = fip->nfields - fip->nbaseclasses - 1;
      if (i < 0)
	{
	  new_field->next = fip->fields;
	  fip->fields = new_field;
	}
      else
	{
	  tmp_field = fip->fields;
	  while (i--)
	    tmp_field = tmp_field->next;
	  new_field->next = tmp_field->next;
	  tmp_field->next = new_field;
	}
    }
  fip->nfields++;

  /* Handle accessibility and virtuality of field.
     The default accessibility for members is public, the default
     accessibility for inheritance is private.  */
  if (die->tag != DW_TAG_inheritance)
    new_field->accessibility = DW_ACCESS_public;
  else
    new_field->accessibility = DW_ACCESS_private;
  new_field->virtuality = DW_VIRTUALITY_none;

  attr = dwarf_attr (die, DW_AT_accessibility);
  if (attr)
    new_field->accessibility = (int) DW_UNSND (attr);
  if (new_field->accessibility != DW_ACCESS_public)
    fip->non_public_fields = 1;
  attr = dwarf_attr (die, DW_AT_virtuality);
  if (attr) {
    /* JAGae23887 - if old-vtable set to on, use the old DW_VIRTUALITY
       values */
    if (STREQ(old_vtable,"on"))
      new_field->virtuality = (int) DW_UNSND (attr) - 1;
    else
      new_field->virtuality = (int) DW_UNSND (attr);
  }

  fp = &new_field->field;
  if (die->tag == DW_TAG_member)
    {
      /* Get type of field.  */
      fp->type = die_type (die, objfile, cu_header);

      /* Get bit size of field (zero if none).  */
      attr = dwarf_attr (die, DW_AT_bit_size);
      if (attr)
	{
	  FIELD_BITSIZE (*fp) = (int) DW_UNSND (attr);
	}
      else
	{
	  FIELD_BITSIZE (*fp) = 0;
	}

      /* Get bit offset of field.  */
      attr = dwarf_attr (die, DW_AT_data_member_location);
      if (attr)
	{
	  FIELD_BITPOS (*fp) =
	    decode_locdesc (NULL, attr, objfile, &locdesc_offset) * 
	    bits_per_byte;
	}
      else
	FIELD_BITPOS (*fp) = 0;

      attr = dwarf_attr (die, DW_AT_bit_offset);
      if (attr)
	{
	  if (BITS_BIG_ENDIAN)
	    {
	      /* For big endian bits, the DW_AT_bit_offset gives the
	         additional bit offset from the MSB of the containing
	         anonymous object to the MSB of the field.  We don't
	         have to do anything special since we don't need to
	         know the size of the anonymous object.  */
	      FIELD_BITPOS (*fp) += DW_UNSND (attr);
	    }
	  else
	    {
	      /* For little endian bits, compute the bit offset to the
	         MSB of the anonymous object, subtract off the number of
	         bits from the MSB of the field to the MSB of the
	         object, and then subtract off the number of bits of
	         the field itself.  The result is the bit offset of
	         the LSB of the field.  */
	      int anonymous_size;
	      int bit_offset = (int) DW_UNSND (attr);

	      attr = dwarf_attr (die, DW_AT_byte_size);
	      if (attr)
		{
		  /* The size of the anonymous object containing
		     the bit field is explicit, so use the
		     indicated size (in bytes).  */
		  anonymous_size = (int) DW_UNSND (attr);
		}
	      else
		{
		  /* The size of the anonymous object containing
		     the bit field must be inferred from the type
		     attribute of the data member containing the
		     bit field.  */
		  anonymous_size = TYPE_LENGTH (fp->type);
		}
	      FIELD_BITPOS (*fp) += anonymous_size * bits_per_byte
		- bit_offset - FIELD_BITSIZE (*fp);
	    }
	}      

      /* Get name of field.  */
      attr = dwarf_attr (die, DW_AT_name);
      if (attr && DW_STRING (attr))
	fieldname = DW_STRING (attr);

      /*  Acc6 Compiler generates names for anonymous types.
          Gdb needs to ignore it like we ignore anonymous
          variable names; JAGaf23121 Diwakar 07/28
       */
      if ((strncmp (fieldname, "{anonymous", 10) == 0))  //}
        fieldname = NULL;

      fp->name = obsavestring (fieldname, (int) strlen (fieldname),
			       &objfile->type_obstack);

#ifdef HP_IA64
      attr = dwarf_attr (die, DW_AT_data_member_location);
      if (!attr && TYPE_CODE (parent_die->type) != TYPE_CODE_UNION)
      {
	/* Look for the last :: and make fp->name equal the name after that */
	char *doub_colon = strrstr (fieldname, "::");
	if (doub_colon)
	  {
	    doub_colon += 2;
	    fp->name = obsavestring (doub_colon, (int) strlen (doub_colon),
				     &objfile->type_obstack);
	  }
	SET_FIELD_PHYSNAME (*fp, obsavestring (fieldname, (int) strlen (fieldname),
					       &objfile->type_obstack));
      }
#endif

      /* Change accessibility for artificial fields (e.g. virtual table
         pointer or virtual base class pointer) to private.  */
      if (dwarf_attr (die, DW_AT_artificial))
	{
	  new_field->accessibility = DW_ACCESS_private;
	  fip->non_public_fields = 1;
	}
    }
  else if (die->tag == DW_TAG_variable)
    {
      char *physname;

      /* C++ static member.
	 Get name of field.  */
      attr = dwarf_attr (die, DW_AT_name);
      if (attr && DW_STRING (attr))
	fieldname = DW_STRING (attr);
      else
	return;

      /* Get physical name.  */
      physname = dwarf2_linkage_name (die);

      SET_FIELD_PHYSNAME (*fp, obsavestring (physname, (int) strlen (physname),
					     &objfile->type_obstack));
      FIELD_TYPE (*fp) = die_type (die, objfile, cu_header);
      FIELD_NAME (*fp) = obsavestring (fieldname, (int) strlen (fieldname),
				       &objfile->type_obstack);
    }
  else if (die->tag == DW_TAG_inheritance)
    {
      /* C++ base class field.  */
      attr = dwarf_attr (die, DW_AT_data_member_location);
      if (new_cxx_abi && new_field->virtuality == DW_VIRTUALITY_virtual)
	{
	  if (attr) {
             /* JAGae40522 - value passed by g++ is in bits not bytes. */
             if (processing_gcc_compilation) { 
	        FIELD_VIRTUAL_BASE_INDEX (*fp) = 
		    (int) decode_locdesc (NULL, attr, 
		    		    objfile, &locdesc_offset) 
		  / bits_per_byte;
             } else {
	        FIELD_VIRTUAL_BASE_INDEX (*fp) = 
		   (int) decode_locdesc (NULL, attr, 
		   		   objfile, &locdesc_offset);
             }
          }
	}
      else
	{
	  if (attr)
	    FIELD_BITPOS (*fp) = 
		  decode_locdesc (NULL, attr, objfile, &locdesc_offset) 
		* bits_per_byte;
	}
      FIELD_BITSIZE (*fp) = 0;
      FIELD_TYPE (*fp) = die_type (die, objfile, cu_header);
      FIELD_NAME (*fp) = type_name_no_tag (fp->type);
      fip->nbaseclasses++;
    }
}

/* Create the vector of fields, and attach it to the type.  */

static void
dwarf2_attach_fields_to_type (struct field_info *fip, struct type *type, struct objfile *objfile)
{
  int nfields = fip->nfields;

  /* Record the field count, allocate space for the array of fields,
     and create blank accessibility bitfields if necessary.  */
  TYPE_NFIELDS (type) = nfields;
  TYPE_FIELDS (type) = (struct field *)
    TYPE_ALLOC (type, sizeof (struct field) * nfields);
  memset (TYPE_FIELDS (type), 0, sizeof (struct field) * nfields);

  if (fip->non_public_fields)
    {
      ALLOCATE_CPLUS_STRUCT_TYPE (type);

      TYPE_FIELD_PRIVATE_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_PRIVATE_BITS (type), nfields);

      TYPE_FIELD_PROTECTED_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_PROTECTED_BITS (type), nfields);

      TYPE_FIELD_IGNORE_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_IGNORE_BITS (type), nfields);
    }

  /* If the type has baseclasses, allocate and clear a bit vector for
     TYPE_FIELD_VIRTUAL_BITS.  */
  if (fip->nbaseclasses)
    {
      int num_bytes = B_BYTES (fip->nbaseclasses);
      char *pointer;

      ALLOCATE_CPLUS_STRUCT_TYPE (type);
      pointer = (char *) TYPE_ALLOC (type, num_bytes);
      TYPE_FIELD_VIRTUAL_BITS (type) = (B_TYPE *) pointer;
      B_CLRALL (TYPE_FIELD_VIRTUAL_BITS (type), fip->nbaseclasses);
      TYPE_N_BASECLASSES (type) = fip->nbaseclasses;
    }

  /* Copy the saved-up fields into the field vector.  Start from the head
     of the list, adding to the tail of the field array, so that they end
     up in the same order in the array in which they were added to the list.  */
  while (nfields-- > 0)
    {
      TYPE_FIELD (type, nfields) = fip->fields->field;
      switch (fip->fields->accessibility)
	{
	case DW_ACCESS_private:
	  SET_TYPE_FIELD_PRIVATE (type, nfields);
	  break;

	case DW_ACCESS_protected:
	  SET_TYPE_FIELD_PROTECTED (type, nfields);
	  break;

	case DW_ACCESS_public:
	  break;

	default:
	  /* Unknown accessibility.  Complain and treat it as public.  */
	  {
	    complain (&dwarf2_unsupported_accessibility,
		      fip->fields->accessibility);
	  }
	  break;
	}
      if (nfields < fip->nbaseclasses)
	{
	  switch (fip->fields->virtuality)
	    {
	    case DW_VIRTUALITY_virtual:
	    case DW_VIRTUALITY_pure_virtual:
	      SET_TYPE_FIELD_VIRTUAL (type, nfields);
	      break;
	    }
	}
      fip->fields = fip->fields->next;
    }
}

#ifndef HP_IA64
/* Skip to the end of a member function name in a mangled name.  */

static char *
skip_member_fn_name (char *physname)
{
  char *endname = physname;

  /* Skip over leading underscores.  */
  while (*endname == '_')
    endname++;

  /* Find two succesive underscores.  */
  do
    endname = strchr (endname, '_');
  while (endname != NULL && *++endname != '_');

  if (endname == NULL)
    {
      complain (&dwarf2_bad_member_name_complaint, physname);
      endname = physname;
    }
  else
    {
      /* Take care of trailing underscores.  */
      if (endname[1] != '_')
	endname--;
    }
  return endname;
}
#endif /* HP_IA64 */

/* Add a member function to the proper fieldlist.  */

static void
dwarf2_add_member_fn (struct field_info *fip, struct die_info *die, struct type *type,
		      struct objfile *objfile,
		      const struct comp_unit_head *cu_header)
{
  struct attribute *attr;
  struct fnfieldlist *flp;
  int i;
  struct fn_field *fnp;
  char *fieldname;
  char *physname;
  struct nextfnfield *new_fnfield;
  unsigned int locdesc_offset;

  /* Extract member function name from mangled name.  */
  physname = dwarf2_linkage_name (die);

#ifndef HP_IA64
  if (physname == NULL)
    return;
  if ((physname[0] == '_' && physname[1] == '_'
       && strchr ("0123456789Qt", physname[2]))
      || DESTRUCTOR_PREFIX_P (physname))
      {
	/* Constructor and destructor field names are set to the name
	   of the class, but without template parameter lists.
	   The name might be missing for anonymous aggregates.  */
	if (TYPE_TAG_NAME (type))
	  {
	    char *p = strchr (TYPE_TAG_NAME (type), '<');
	    
	    if (p == NULL)
	      fieldname = TYPE_TAG_NAME (type);
	    else
	      fieldname = obsavestring (TYPE_TAG_NAME (type),
					p - TYPE_TAG_NAME (type),
					&objfile->type_obstack);
	  }
	else
	  {
	    char *anon_name = "";
	    fieldname = obsavestring (anon_name, strlen (anon_name),
				      &objfile->type_obstack);
	  }
      }
    else
      {
	char *endname = skip_member_fn_name (physname);
	
	/* Ignore member function if we were unable not extract the member
	   function name.  */
	
	if (endname == physname)
	  return;
	fieldname = obsavestring (physname, endname - physname,
				  &objfile->type_obstack);
      }
#else	/* HP_IA64 */
  if (physname[0] == '~')
    return;

      {
	/* Either the mangled name or the demangled name can be in the
	   DWARF2 now.  Hence endname can be obtained by demangling the
	   physname and the physname contains the mangled name - if it
	   demangles.  If it doesn't demangle, then it might already be a
	   demangled name and we still need to check it for the "::'
	   class/function separator.  */

	char *endname = NULL;
	char *demangled_name = cplus_demangle (physname, DMGL_ANSI);
	if (demangled_name == NULL)
	  endname = physname;
	else
	  endname = demangled_name;

	endname = strrchr(endname, ':');
	/* Skip over last ':' */
	++endname;

	if (endname[0] == '~') 
	  {
	    /* Poorva: After proc_elim the linker may actually keep 
	       any 1 of the 3 destructors. Hence it is necessary that
	       we add all 3 destructors to the field list.	       
	       If we find all 3 destructors we set breakpoints on all of
	       them and also if the linker has eliminated 2 we need to be 
	       able to set a breakpoint on the one remaining.

	       Previously we were not adding all 3 to the field list.
	       Now we need to add them hence have if 0'd the code.
	       */
	    ++endname;
	  }
	fieldname = obsavestring (endname, (int) strlen(endname),
				  &objfile->type_obstack);
        if (demangled_name)
	  free (demangled_name);
      }
#endif /* HP_IA64 */
  
  /* Look up member function name in fieldlist.  */
  /* Poorva - Make it a strcmp instead of a strcmp_iw */
  for (i = 0; i < fip->nfnfields; i++)
    {
      if (!strcmp (fip->fnfieldlists[i].name, fieldname))
	break;
    }
  
  /* Create new list element if necessary.  */
  if (i < fip->nfnfields)
    flp = &fip->fnfieldlists[i];
  else
    {
      if ((fip->nfnfields % DW_FIELD_ALLOC_CHUNK) == 0)
	{
	  fip->fnfieldlists = (struct fnfieldlist *)
	    xrealloc (fip->fnfieldlists,
		      (fip->nfnfields + DW_FIELD_ALLOC_CHUNK)
		      * sizeof (struct fnfieldlist));
	  if (fip->nfnfields == 0)
	    make_cleanup (free_current_contents, &fip->fnfieldlists);
	}
      flp = &fip->fnfieldlists[fip->nfnfields];
      flp->name = fieldname;
      flp->length = 0;
      flp->head = NULL;
      fip->nfnfields++;
    }

  /* Create a new member function field and chain it to the field list
     entry. */
  new_fnfield = (struct nextfnfield *) xcalloc (1, sizeof (struct nextfnfield));
  make_cleanup (free, new_fnfield);
  new_fnfield->next = flp->head;
  flp->head = new_fnfield;
  flp->length++;

  /* Fill in the member function field info.  */
  fnp = &new_fnfield->fnfield;
  fnp->physname = obsavestring (physname, (int) strlen (physname),
				&objfile->type_obstack);
  fnp->type = alloc_type (objfile);

  if (die->type && TYPE_CODE (die->type) == TYPE_CODE_FUNC)
    {
      struct type *return_type = TYPE_TARGET_TYPE (die->type);
      struct type **arg_types;
      int nparams = TYPE_NFIELDS (die->type);
      int iparams;
      
      /* Copy argument types from the subroutine type.  */
      arg_types = (struct type **)
	TYPE_ALLOC (fnp->type, (nparams + 1) * sizeof (struct type *));
      for (iparams = 0; iparams < nparams; iparams++)
	arg_types[iparams] = TYPE_FIELD_TYPE (die->type, iparams);

      /* Set last entry in argument type vector.  */
      if (TYPE_FLAGS (die->type) & TYPE_FLAG_VARARGS)
	arg_types[nparams] = NULL;
      else
	arg_types[nparams] = dwarf2_fundamental_type (objfile, FT_VOID);

      smash_to_method_type (fnp->type, type, return_type, arg_types);
      if (nparams >0)
	{
	  TYPE_NFIELDS (fnp->type) = nparams;
	  TYPE_FIELDS (fnp->type) = TYPE_FIELDS (die->type);
	}
      
      /* Handle static member functions.
         Dwarf2 has no clean way to discern C++ static and non-static
         member functions. G++ helps GDB by marking the first
         parameter for non-static member functions (which is the
         this pointer) as artificial. We obtain this information
         from read_subroutine_type via TYPE_FIELD_ARTIFICIAL.  */

      /* Poorva: The parameter list for constructors is on the entry points
	 so we think we have none and mark all constructors as static. For
	 destructors for IA64 we do not have the ~ so the field name 
	 matches the class name and so those don't get marked static too. */
      /* Carl:  The "this" pointer for member functions returning aggregates
	 using the Itanium C++ ABI is the second param, not the first (which
	 is the hidden return value pointer, not marked as "artificial" by
	 aC++).  This was causing member functions using this convention to
	 be erroneously marked "static" by ptype and getting parameter count
	 mismatches for command-line calls.  JAGaf48452, "Cannot use c++
	 operators in gdb expressions" */

      if (strcmp_iw (type->tag_name, fieldname))
	if ((nparams == 0) || 
	    (( ! TYPE_FIELD_ARTIFICIAL (die->type, 0))
#ifdef HP_IA64
	    && (!((nparams > 1) && (TYPE_FIELD_ARTIFICIAL (die->type, 1))))
#endif
	    ))
	  fnp->voffset = VOFFSET_STATIC;
    }
else
    complain (&dwarf2_missing_member_fn_type_complaint, physname);

  /* Get fcontext from DW_AT_containing_type if present.  */
  if (dwarf_attr (die, DW_AT_containing_type) != NULL)
    fnp->fcontext = die_containing_type (die, objfile, cu_header);

  /* dwarf2 doesn't have stubbed physical names, so the setting of is_const
   and is_volatile is irrelevant, as it is needed by gdb_mangle_name only. */
  
  /* Get accessibility.  */
  attr = dwarf_attr (die, DW_AT_accessibility);
  if (attr)
    {
      switch (DW_UNSND (attr))
	{
	case DW_ACCESS_private:
	  fnp->is_private = 1;
	  break;
	case DW_ACCESS_protected:
	  fnp->is_protected = 1;
	  break;
	}
    }

  /* Get index in virtual function table if it is a virtual member function.  */
  attr = dwarf_attr (die, DW_AT_vtable_elem_location);
  if (attr)
    fnp->voffset = (unsigned int) decode_locdesc (NULL, attr, objfile, &locdesc_offset) 
      + 2;
  if (fnp->voffset != 0 && fnp->fcontext == NULL)
    fnp->fcontext = type;
}

/* Create the vector of member function fields, and attach it to the type. */

static void
dwarf2_attach_fn_fields_to_type (struct field_info *fip, struct type *type,
				 struct objfile *objfile)
{
  struct fnfieldlist *flp;
  int total_length = 0;
  int i;

  TYPE_FN_FIELDLISTS (type) = (struct fn_fieldlist *)
    TYPE_ALLOC (type, sizeof (struct fn_fieldlist) * fip->nfnfields);

  for (i = 0, flp = fip->fnfieldlists; i < fip->nfnfields; i++, flp++)
    {
      struct nextfnfield *nfp = flp->head;
      struct fn_fieldlist *fn_flp = &TYPE_FN_FIELDLIST (type, i);
      int k;

      TYPE_FN_FIELDLIST_NAME (type, i) = flp->name;
      TYPE_FN_FIELDLIST_LENGTH (type, i) = flp->length;
      fn_flp->fn_fields = (struct fn_field *)
	TYPE_ALLOC (type, sizeof (struct fn_field) * flp->length);
      for (k = flp->length; (k--, nfp); nfp = nfp->next)
	fn_flp->fn_fields[k] = nfp->fnfield;

      total_length += flp->length;
    }

  TYPE_NFN_FIELDS (type) = fip->nfnfields;
  TYPE_NFN_FIELDS_TOTAL (type) = total_length;
}

/* Called when we find the DIE that starts a structure or union scope
   (definition) to process all dies that define the members of the
   structure or union.

   NOTE: we need to call struct_type regardless of whether or not the
   DIE has an at_name attribute, since it might be an anonymous
   structure or union.  This gets the type entered into our set of
   user defined types.

   However, if the structure is incomplete (an opaque struct/union)
   then suppress creating a symbol table entry for it since gdb only
   wants to find the one with the complete definition.  Note that if
   it is complete, we just call new_symbol, which does it's own
   checking about whether the struct/union is anonymous or not (and
   suppresses creating a symbol table entry itself).  */

static void
read_structure_scope (struct die_info *die, struct objfile *objfile,
		      const struct comp_unit_head *cu_header)
{
  struct type *type;
  struct attribute *attr;
  int decl = 0;
  struct symbol *ssym = NULL;

  /* Don't need to do anything if the type's already set */
  if (die->type) 
    return;

  type = alloc_type (objfile);

  INIT_CPLUS_SPECIFIC (type);
  attr = dwarf_attr (die, DW_AT_name);
  if (attr && DW_STRING (attr))
    {
      TYPE_TAG_NAME (type) = obsavestring (DW_STRING (attr),
					   (int) strlen (DW_STRING (attr)),
					   &objfile->type_obstack);
    }
  
  if (die->tag == DW_TAG_structure_type)
    {
      TYPE_CODE (type) = TYPE_CODE_STRUCT;
    }
  else if (die->tag == DW_TAG_union_type)
    {
      TYPE_CODE (type) = TYPE_CODE_UNION;
    }
  else
    {
      /* FIXME: TYPE_CODE_CLASS is currently defined to TYPE_CODE_STRUCT
         in gdbtypes.h.  */
      TYPE_CODE (type) = TYPE_CODE_CLASS;
    }

  attr = dwarf_attr (die, DW_AT_byte_size);
  if (attr)
    {
      TYPE_LENGTH (type) = (unsigned int) DW_UNSND (attr);
    }
  else
    {
      TYPE_LENGTH (type) = 0;
    }

  /* QXCR1000743340: mis use of DW_TAG_structure_type's DW_CHILDREN field */
  decl = is_die_declaration (die); 

  /* Poorva: We need to set TYPE_DECLARATION to 1 here irrespective of whether
     the die actually is a declaration or truly a definition. 
     TYPE_DECLARATION is used by TYPE_IS_OPAQUE to determine if a 
     type is opaque and if it is 1 then it goes looking for the
     real type.
     This is because the type field of this die is used to set the 
     type field of pointers to the structure type withn the structure 
     e.g the this pointer. If the member function the this pointer
     is associated with is a const, volatile function then in 
     make_cv_type etc we make a copy of this type and mark it as 
     a const. But later we check to see if it is an opaque type
     and then memcpy the fields and number of fields of the real
     type to it. Since this type has not been completely made 
     when pointers within this structure/class will reference it
     we need to make sure that it is marked as opaque (which
     happens with the TYPE_DECLARATION = 1. This then ensures
     that they will later be completed with the fields etc. 
     If not done then any references within the structure using the
     this pointers will fail. - Yet another subtlety of gdb  */
  
  TYPE_DECLARATION (type) = 1;
  
  /* We need to add the type field to the die immediately so we don't
     infinitely recurse when dealing with pointers to the structure
     type within the structure itself. */
  die->type = type;

  if (die->has_children)
    {
      struct field_info fi;
      struct die_info *child_die;
      struct cleanup *back_to = make_cleanup (null_cleanup, NULL);
      int flag_has_vpf = 0;

      memset (&fi, 0, sizeof (struct field_info));

      child_die = die->next;

      while (child_die && child_die->tag)
	{
	  if (child_die->tag == DW_TAG_member)
	    {
	      dwarf2_add_field (&fi, child_die, objfile, die, cu_header);
              /* Set the flag "flag_has_vpf" when virtual-table pointer
                 handshake ("__vfp") is present. For details please see 
                 Lab-Notes of "QXCR1000807286" */
              if (fi.fields && !strcmp ("__vfp", fi.fields->field.name))
                {
                  flag_has_vpf = 1; 
                }
	    }
	  else if (child_die->tag == DW_TAG_subprogram 
		   || (child_die->tag == DW_TAG_entry_point && cu_language == language_fortran))
	    {
	      /* C++ member function. */
	      process_die (child_die, objfile, cu_header);
              /* g++ will create dwarf entries with the artificial attribute.
                 A check is needed for it here, otherwise a struct will be
                 incorrectly identified as a class - JAGae15127 */
	      if (!dwarf_attr(child_die, DW_AT_specification)
                  && !dwarf_attr(child_die, DW_AT_artificial))
                {
		  dwarf2_add_member_fn (&fi, child_die, type, objfile, cu_header);
                }
	    }
	  else if (child_die->tag == DW_TAG_inheritance)
	    {
	      /* C++ base class field.  */
	      dwarf2_add_field (&fi, child_die, objfile, die, cu_header);
	    }
	  else
	    {
	      process_die (child_die, objfile, cu_header);
	    }
	  child_die = sibling_die (child_die);
	}

      /* QXCR1000910443: Polymorphic type recognition fails when there is a
         nested class. Always allocate the c++ structure type whenever either
         the virtual table is present or there are non-zero member functions. */
      if (flag_has_vpf || fi.nfnfields)
        ALLOCATE_CPLUS_STRUCT_TYPE (type);

      /* Attach fields and member functions to the type.  */
      if (fi.nfields)
	dwarf2_attach_fields_to_type (&fi, type, objfile);
      if (fi.nfnfields)
	{
	  dwarf2_attach_fn_fields_to_type (&fi, type, objfile);

	  /* Get the type which refers to the base class (possibly this
	     class itself) which contains the vtable pointer for the current
	     class from the DW_AT_containing_type attribute.  */

	  if (dwarf_attr (die, DW_AT_containing_type) != NULL)
	    {
	      struct type *t = die_containing_type (die, objfile, cu_header);

	      TYPE_VPTR_BASETYPE (type) = t;
	      if (type == t)
		{
		  static const char vptr_name[] =
		  {'_', 'v', 'p', 't', 'r', '\0'};
		  int i;

		  /* Our own class provides vtbl ptr.  */
		  for (i = TYPE_NFIELDS (t) - 1;
		       i >= TYPE_N_BASECLASSES (t);
		       --i)
		    {
		      char *fieldname = TYPE_FIELD_NAME (t, i);

		      if (STREQN (fieldname, vptr_name, strlen (vptr_name) - 1)
			  && is_cplus_marker (fieldname[strlen (vptr_name)]))
			{
			  TYPE_VPTR_FIELDNO (type) = i;
			  break;
			}
		    }

		  /* Complain if virtual function table field not found.  */
		  if (i < TYPE_N_BASECLASSES (t))
		    complain (&dwarf2_vtbl_not_found_complaint,
			  TYPE_TAG_NAME (type) ? TYPE_TAG_NAME (type) : "");
		}
	      else
		{
		  TYPE_VPTR_FIELDNO (type) = TYPE_VPTR_FIELDNO (t);
		}
	    }
	}
      
#ifdef HP_IA64
          /* Virtual table is present if "flag_has_vpf" flag is set. */
          if (has_vtable (type) || flag_has_vpf)
            {
              /* Reset "flag_has_vpf" flag. */
              flag_has_vpf = 0;
              /* Allocate space for class runtime information */
              TYPE_RUNTIME_PTR (type) = (struct runtime_info *)
                xmalloc (sizeof (struct runtime_info));
              /* Set flag for vtable */
              TYPE_VTABLE (type) = 1;
              /* The first non-virtual base class with a vtable. */
              TYPE_PRIMARY_BASE (type) = primary_base_class (type);
              /* The virtual base list. */
              TYPE_VIRTUAL_BASE_LIST (type) = virtual_base_list (type);
            }
          else
            TYPE_RUNTIME_PTR (type) = NULL;
#endif
	  if (!decl)
	    {
	      ssym = new_symbol (die, type, objfile, cu_header);
	      if (ssym != NULL && SYMBOL_TYPE (ssym) != NULL )
		die->type = SYMBOL_TYPE (ssym);
	    }
          else
	    die->type = type;
	  	    
	  do_cleanups (back_to);
    }
  else
    {
      /* Incomplete type when byte size is zero and it is declared. */
      if (decl && (TYPE_LENGTH (type) == 0))
        TYPE_FLAGS (type) |= TYPE_FLAG_STUB;
      die->type = type;
    }

  if (!decl)
    TYPE_DECLARATION (type) = 0;
  die->type = type;
}

/* Check whether the given die is of type "declaration".
   A DIE is a "declaration" if it has a non-zero attribute 
   value of "DW_AT_declaration". We need to check that this doesnt
   have the "DW_AT_specification" attribute. If it has, then it has 
   reference to different DIE. 

   "FSF GDB comment : However, we have to be careful with
   DIEs having a DW_AT_specification attribute. So we may end up
   accidently finding a declaration attribute that belongs
   to a different DIE referenced by the specification attribute,
   even though the given DIE does not have a declaration attribute."*/
static int
is_die_declaration (struct die_info *die)
{
  struct attribute *attr = NULL;
  attr = dwarf_attr (die, DW_AT_declaration);
  return ( (attr && DW_UNSND (attr)) &&
	   (dwarf_attr (die, DW_AT_specification) == NULL));
}
   
/* Routine to handle using declarations. It does not read all
   the DW_TAG_imported_declaration. Tags with a name attribute are
   aliases and those are processed using process_alias.
   */

static void 
read_imported_declaration (struct die_info *die, struct objfile *objfile,
			   const struct comp_unit_head *cu_header)
{
  char *ref_name = NULL;
  struct die_info *ref_die = 0;
  struct attribute *attr, *name_attr;

  name_attr = dwarf_attr (die, DW_AT_name);
  if (name_attr)
    return;
  attr = dwarf_attr (die, DW_AT_import);
  if (attr)
    {
      ref_die = find_die_from_ref (die, objfile, attr);
      ref_name = dwarf2_linkage_name (ref_die);
      if (!ref_die->type)
	{
	  struct attribute* type_attr = dwarf_attr (ref_die, DW_AT_type);
  	  if (!type_attr)
   	    {
     	      /* A missing DW_AT_type. Must be a type die. */
	      ref_die->type = tag_type_to_type (ref_die, objfile, cu_header);
    	    }
	  else
	    ref_die->type = die_type (ref_die, objfile, cu_header);
	}
    }
	
  /* This is a using declaration - Add 2 new symbols to the current 
     scope if the scope is not file scope - the first as A::B::i 
     and the second as i.
     If the current scope is file scope just add "i" since A::B::i
     will be in the global block.
     new_symbol checks to find a name and if it doesn't it checks 
     import_var_name - this is set to the name we want the symbol 
     to have e.g even though ref_die has the name "A::B::i" we may 
     want the name of this new symbol to be "i".
     */

  import_var_name = ref_name;
  if (!(list_in_scope == &file_symbols))
    new_symbol (die, ref_die->type, objfile, cu_header);

  if (ref_name)
    {
      char *ptr;
      char *name = ref_name;
      /* If this is mangled demangle it first without the argument list*/
      if (is_cplus_name (ref_name, 1)) /* JAGaf49121 */
	name = cplus_demangled_name;

      ptr = strrstr (name, "::");
      if (ptr) 
	{
	  import_var_name = ptr + 2;
	}
      new_symbol (die, ref_die->type, objfile, cu_header);
    }
  import_var_name = NULL;
}


/* This is for reading using directives. Forms a block with the namespace
   field filled in for the block - done in finish_block.
   If the using directive is inside a function then go ahead and create
   a block for it now. 
   If it is in file scope then store it's symbol. The blocks needs to be made
   after all local blocks have been made and before the global and static
   blocks are made. 
   This is done in end_symtab where we create a block for all the symbols in
   global_using_dirs and add those to the blockvector for that symtab.
 */

static struct symbol *
read_imported_module (struct die_info *die, struct objfile *objfile,
		      const struct comp_unit_head *cu_header)
{
  struct die_info *ref_die;
  struct attribute *attr;
  struct type *type;
  struct symbol *sym;
  char *name = 0; /* initialize for compiler warning */

  attr = dwarf_attr (die, DW_AT_import);
  if (attr)
    {
      while (1)
	{
	  ref_die = find_die_from_ref (die, objfile, attr);
	  if (ref_die->tag == DW_TAG_imported_declaration)
	    {
	      /* If it is pointing to a DW_TAG_imported_declaration
		 then it is pointing to an alias. Keep following
		 the reference till you find the actual namespace
		 die. Also need to keep going if we have found
		 an extension to a namespace */
	      attr = dwarf_attr (ref_die, DW_AT_import);
	    }
	  else if (ref_die->tag == DW_TAG_namespace)
	    {
	      if (dwarf_attr (ref_die, DW_AT_extension))
		attr = dwarf_attr (ref_die, DW_AT_extension);
	      else
		{
		  name = dwarf2_linkage_name (ref_die);
		  if (name == NULL)
		    {
		      /* We have reached an anonymous namespace */
		      /* The fact that this anonymous namespace has not been
			 named by us already means that we are seeing the 
			 using directive for it before we see the namespace */
		      struct attribute *n_attr = 
			dwarf_attr (ref_die, DW_AT_name);
		      if (n_attr)
			{ 
			  char *die_name = DW_STRING (n_attr);
			  die_name = generate_anon_name (objfile);
			  DW_STRING (n_attr) = obsavestring 
			    (die_name, (int) strlen (die_name), 
			     &objfile->type_obstack);
			  /* HACK - We change the form to 0. 
			     so that we can later identify that this
			     was an anonymous namespace. 
			     2 other solutions - 
			     a) Check to see if we generated this name.
			     b) dwarf2 needs to be modified so that the
			     DW_TAG_namespace also takes a DW_AT_external
			     attribute which is FALSE for anon ns 
			     as they can't be seen outside the comp unit. */
			  n_attr->form = DW_FORM_not_set;
			}
		    }
		  break;
		}
	    }
	  else
	    {
	      warning ("Incorrect reference for using directive\n");
	      break;
	    }
	}
    }
	
  /* This is a using directive - Put it into the using's list
     We need to start a scope for this */

  attr = dwarf_attr (die, DW_AT_start_scope);
  if (attr)
    {
      import_lowpc = DW_UNSND(attr);
      import_lowpc += baseaddr;
    }

  /* if DW_AT_start_scope is not present then import_lowpc 
     is the lowpc of the block it is in - import_lowpc/import_higpc  
     are globals set everytime we enter a new scope */

  type = alloc_type (objfile);
  INIT_CPLUS_SPECIFIC (type);
  TYPE_CODE (type) = TYPE_CODE_USINGDIR;
  TYPE_NFIELDS (type) = 0; 

  import_var_name = name;
  sym = new_symbol (die, type, objfile, cu_header);
  import_var_name = NULL;
      
  if (list_in_scope != &local_symbols)
    {
      if (num_globusings == max_globusings)
	{
	  if (max_globusings == 0)
	    max_globusings = INIT_GLOBAL_USINGS;
	  else
	    max_globusings *= 2;
	  global_using_dirs = (struct symbol **) xrealloc 
	    (global_using_dirs, 
	     max_globusings * sizeof (struct symbol *));
	}
      global_using_dirs[num_globusings++] = sym;
    }
  return sym;
}

/* Routine to generate a name for anonymous namespaces. It takes 
   the name of the file and adds an increasing number to it.*/

static char *
generate_anon_name (struct objfile *objfile)
{
  char *anon_name, *tmp_str;
  char *num = ltoa (num_anon_ns);
  char *obj_name = strrstr (objfile->name, SLASH_STRING);
  obj_name ++;
  tmp_str = (char *) alloca (strlen (obj_name) + 2 + strlen (num));
  sprintf (tmp_str, "%s_%s", obj_name, num);
  anon_name = obsavestring (tmp_str, (int) strlen (tmp_str), 
			    &objfile->type_obstack);
  num_anon_ns++;
  return anon_name;
}

static struct field *
alloc_fields (int num_fields, struct field *fields)
{
  if ((num_fields % DW_FIELD_ALLOC_CHUNK) == 0)
    {
      fields = (struct field *) xrealloc 
	(fields, (num_fields + DW_FIELD_ALLOC_CHUNK) 
	 * sizeof (struct field));
    } 
  
  FIELD_NAME (fields[num_fields]) = NULL;
  FIELD_TYPE (fields[num_fields]) = NULL;
  FIELD_BITPOS (fields[num_fields]) = 0;
  FIELD_BITSIZE (fields[num_fields]) = 0;
  
  return fields;
}


/* Reads the namespace die and processes all it children. It gets
   added as a symbol to the list and as a typedef */

static void
read_namespace_scope (struct die_info *die, struct objfile *objfile,
		      const struct comp_unit_head *cu_header)
{
  struct type *type;
  struct attribute *attr = NULL, *name_attr = NULL;
  struct attribute *decl = NULL;
  struct cleanup *back_to = make_cleanup (null_cleanup, NULL);
  struct pending **old_list_in_scope;
  struct field *fields;
  int num_fields;
  char *die_name = NULL;
  char *ref_name = NULL;
  struct die_info *ref_die = NULL;
  int is_anon = 0;
  type = alloc_type (objfile);
  old_list_in_scope = list_in_scope;
  
  INIT_CPLUS_SPECIFIC (type);
  name_attr = dwarf_attr (die, DW_AT_name);
  if (name_attr)
    {    
      die_name = DW_STRING (name_attr);
      if (!name_attr->form == DW_FORM_not_set)
	{
	  is_anon = 1;
	  list_in_scope = &file_symbols;
	  name_attr->form = DW_FORM_string;
	}
      else 
	list_in_scope = &global_symbols;
    }

  if (!die_name)
    {
      attr = dwarf_attr (die, DW_AT_extension);
      while (attr)
	{
	  /* Follow the reference till you come to
	     a namespace with a name / or to a nonextension.
	     The compiler currently groups them. The last one could 
	     also be an anonymous ns */

	  struct attribute *n_attr = dwarf_attr (die, DW_AT_name);
	  if (n_attr)
	    {    
	      die_name = DW_STRING (n_attr);
	    }
	  if (n_attr && !die_name)
	    break;
	  if (!n_attr)
	    {
	      attr = dwarf_attr (die, DW_AT_extension);
	    }
	}
    }
  
  if (!die_name)
    {
      /* This is an anonymous namespace */
      die_name = generate_anon_name (objfile);
      is_anon = 1;
      if (name_attr)
	DW_STRING (name_attr) = obsavestring (die_name,
					      (int) strlen (die_name),
					      &objfile->type_obstack);
      list_in_scope = &file_symbols;
    }  
  
  if (die_name)
    TYPE_TAG_NAME (type) = obsavestring (die_name,
					 (int) strlen (die_name),
					 &objfile->type_obstack);
  
  TYPE_CODE (type) = TYPE_CODE_NAMESPACE;

  /* Read other attrbiutes too */
  decl = dwarf_attr (die, DW_AT_declaration);
  num_fields = 0;
  fields = NULL;
  
  if (die->has_children)
    {
      struct die_info *child_die;
      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  /* Poorva - We need to make list_in_scope point to global 
	     symbols(for namespaces) in this loop since read_func_scope 
	     called by process_die for subprogram dies-changes the 
	     list_in_scope to file_symbols. */
	  if (!is_anon)
	    list_in_scope = &global_symbols;

	  /* If the child is a using declaration */
	  if (child_die->tag == DW_TAG_imported_declaration)
	    { 
	      /* Before adding it as a global/file static symbol
		 we need to change the name of the child_die. 
		 e.g using B::C::i
		 we need to add a new variable A::i to global scope.
		 */
	      
	      struct attribute *name_attr = dwarf_attr (child_die, DW_AT_name);
	      if (!name_attr)
		{
		  char *ptr = NULL, *var_name = NULL, *name, *tmp = NULL;
		  char *ptr2 = NULL;
		  extern char * go_to_matching (char*, char, char, char*);

		  attr = dwarf_attr (child_die, DW_AT_import);
		  if (attr)
		    {
		      ref_die = find_die_from_ref 
			(child_die, objfile, attr);
		      ref_name = dwarf2_linkage_name (ref_die);
		      if (!ref_die->type)
			{
			  struct attribute* type_attr = 
				dwarf_attr (ref_die, DW_AT_type);
			  if (!type_attr)
			    {
			      /* A missing DW_AT_type. Must be a type die. */
			      ref_die->type = 
				tag_type_to_type (ref_die, objfile, cu_header);
			    }
			  else
			    ref_die->type = die_type (ref_die, objfile, cu_header);
			}
		    }
		  var_name = name = ref_name;
		  /* Get the last part of the name */
		  if (is_cplus_name (ref_name, 1))    /* JAGaf49121 */
		    {
		      var_name = cplus_demangled_name;
		      tmp = go_to_matching (var_name + strlen (var_name),
					    '(', ')', var_name);
		      name = strdup (var_name);
		      name [tmp - var_name + 1] = '\0';
		    }
		  
		  ptr = strrstr (name, "::");
		  if (ptr == NULL)
		    {
		      ptr2 = (char *) xmalloc (3);
		      strcpy(ptr2, "::");
		      ptr = var_name;
		    }
		  else 
		    ptr = var_name + (ptr - name);
		  if (die_name)
		    {
		      char *tmp_str = (char *) xmalloc (strlen (die_name) + 
							strlen (ptr2) +
							strlen (ptr) + 1);
		      sprintf (tmp_str, "%s%s%s", die_name, ptr2, ptr);
		      import_var_name = obsavestring (tmp_str, 
						      (int) strlen (tmp_str),
						      &objfile->type_obstack);
		      free (tmp_str);
		    }
		  list_in_scope = &global_symbols;
		  new_symbol (child_die, ref_die->type, objfile, cu_header);
		  import_var_name = NULL;
		}
	      else 
		{
		  /* Add to alias list */
		  extern const struct cplus_struct_type cplus_struct_default;
		  if (TYPE_CPLUS_SPECIFIC(type) == &cplus_struct_default)
		    ALLOCATE_CPLUS_STRUCT_TYPE (type);
		  
		  (TYPE_CPLUS_SPECIFIC(type))->aliases = process_alias 
		    (child_die, 
		     (TYPE_CPLUS_SPECIFIC(type))->aliases,
		     objfile, cu_header);
		}
	    }
	  else if (child_die->tag == DW_TAG_imported_module)
	    {
	      fields = alloc_fields (num_fields, fields);
	      if (attr = dwarf_attr (child_die, DW_AT_import))
		{
		  char *name = NULL;
		  struct die_info *ref_die;
		  while (1)
		    {
		      ref_die = find_die_from_ref 
			(child_die, objfile, attr);
		      if (ref_die->tag == DW_TAG_namespace)
			{
			  if (dwarf_attr (ref_die, DW_AT_extension))
			    attr = dwarf_attr (ref_die, DW_AT_extension);
			  else
			    {
			      struct attribute *name_attr = 
				dwarf_attr (ref_die, DW_AT_name);
			      name = DW_STRING (name_attr);
			      break;
			    }
			}
		      else
			{
			  /* warning ("Incorrect reference for using 
			     directive\n"); */
			  break;
			}
		    }
		  
		  if (name)
		  {
		    FIELD_NAME (fields[num_fields++])  = obsavestring
		      (name, (int) strlen (name), &objfile->type_obstack);
		  }
		}
	    }
	  else
	    {
	      /* If you find the string ANON_NS strip what the 
		 compiler gives to you. We do not need to look 
		 at subprogram since we get the mangled name
		 for that and it is handled in cplus-dem.c */

	      if (child_die->tag != DW_TAG_subprogram)
		{
		  char *child_name, *anon_string_ptr, *new_name;
		  char *end_child_name;
		  int anon_ns_stringlen = 0;
		  attr = dwarf_attr (child_die, DW_AT_name);
		  if (attr)
		    {
		      child_name = DW_STRING (attr);
		      anon_ns_stringlen = (int) strlen (ANON_NS);
		      end_child_name = child_name + strlen (child_name);
		      new_name = (char *) obstack_alloc 
			(&objfile->symbol_obstack, strlen (child_name) + 1);
		      new_name[0] = '\0';
		      while (1) 
			{
			  anon_string_ptr = strstr (child_name, ANON_NS);
			  if (anon_string_ptr)
			    {
			      strncat (new_name, child_name, 
				       anon_string_ptr - child_name);
			      child_name =  anon_string_ptr + anon_ns_stringlen;
			      /* The +2 is for "::" */
			      if (child_name[0] == ':' 
				  && child_name[1] == ':')
				child_name += 2;
			    }
			  else 
			    break;
			}
		      strncat (new_name, child_name, 
			       end_child_name - child_name);
		      DW_STRING (attr) = new_name;
		    }
		}
	      process_die (child_die, objfile, cu_header);
	    }
	  child_die = sibling_die (child_die);
	}
    }
  
  if (num_fields)
    {
      TYPE_NFIELDS (type) = num_fields;
      TYPE_FIELDS (type) = (struct field *)
	TYPE_ALLOC (type, sizeof (struct field) * num_fields);
      memcpy (TYPE_FIELDS (type), fields,
                  sizeof (struct field) * num_fields);
      free (fields);
    }

  die->type = type;
  global_var_name = die_name;
  if (!decl)
    new_symbol (die, type, objfile, cu_header);
  global_var_name = NULL;

  list_in_scope = old_list_in_scope;
  do_cleanups (back_to);
  die->type = type;
}

/* Given a pointer to a die which begins an enumeration, process all
   the dies that define the members of the enumeration.

   This will be much nicer in draft 6 of the DWARF spec when our
   members will be dies instead squished into the DW_AT_element_list
   attribute.

   NOTE: We reverse the order of the element list.  */

static void
read_enumeration (struct die_info *die, struct objfile *objfile,
                  const struct comp_unit_head *cu_header)
{
  struct die_info *child_die;
  struct type *type;
  struct field *fields;
  struct attribute *attr;
  struct symbol *sym;
  int num_fields;
  int unsigned_enum = 1;

  type = alloc_type (objfile);

  TYPE_CODE (type) = TYPE_CODE_ENUM;
  attr = dwarf_attr (die, DW_AT_name);
  if (attr && DW_STRING (attr))
    {
      TYPE_TAG_NAME (type) = obsavestring (DW_STRING (attr),
					   (int) strlen (DW_STRING (attr)),
					   &objfile->type_obstack);
    }

  attr = dwarf_attr (die, DW_AT_byte_size);
  if (attr)
    {
      TYPE_LENGTH (type) = (unsigned int) DW_UNSND (attr);
    }
  else
    {
      TYPE_LENGTH (type) = 0;
    }

  num_fields = 0;
  fields = NULL;
  if (die->has_children)
    {
      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  if (child_die->tag != DW_TAG_enumerator &&
	      child_die->tag != DW_TAG_constant)
	    {
	      process_die (child_die, objfile, cu_header);
	    }
	  else
	    {
	      attr = dwarf_attr (child_die, DW_AT_name);
	      if (attr)
		{
                  attr = dwarf_attr (child_die, DW_AT_const_value);
                  if (attr->form == DW_FORM_sdata)
                    /* signed enumerator */
		    unsigned_enum = 0;

		  if ((num_fields % DW_FIELD_ALLOC_CHUNK) == 0)
		    {
		      fields = (struct field *)
			xrealloc (fields,
				  (num_fields + DW_FIELD_ALLOC_CHUNK)
				  * sizeof (struct field));
		    }

		  sym = new_symbol (child_die, type, objfile, cu_header);
		  FIELD_NAME (fields[num_fields]) = SYMBOL_NAME (sym);
		  FIELD_TYPE (fields[num_fields]) = NULL;
		  FIELD_BITPOS (fields[num_fields]) = SYMBOL_VALUE (sym);
		  FIELD_BITSIZE (fields[num_fields]) = 0;

		  num_fields++;
		}
	    }

	  child_die = sibling_die (child_die);
	}

      if (num_fields)
	{
	  TYPE_NFIELDS (type) = num_fields;
	  TYPE_FIELDS (type) = (struct field *)
	    TYPE_ALLOC (type, sizeof (struct field) * num_fields);
	  memcpy (TYPE_FIELDS (type), fields,
		  sizeof (struct field) * num_fields);
	  free (fields);
	}
      if (unsigned_enum)
	TYPE_FLAGS (type) |= TYPE_FLAG_UNSIGNED;
    }

  /* Fix for JAGae84607.  enum declarations and definitions weren't distinguished from
   * each other, thereby creating confusion.  Even though declaration DIEs are processed
   * no symbols should be created for them.	- Bharath, August 5 2003.
   */
  if (dwarf_attr (die, DW_AT_declaration) == NULL)	/* A definition */
    {
      TYPE_DECLARATION (type) = 0;
      die->type = type;
      new_symbol (die, type, objfile, cu_header);
    }
  else	/* A declaration */
    {
      TYPE_DECLARATION (type) = 1;
      die->type = type;
    }
}

/* Extract all information from a DW_TAG_array_type DIE and put it in
   the DIE's type field. */

static void
read_array_type (struct die_info *die, struct objfile *objfile,
		 const struct comp_unit_head *cu_header)
{
  struct die_info *child_die;
  struct type *type = NULL;
  struct type *element_type, *range_type, *index_type;
  struct type **range_types = NULL;
  struct attribute *attr;
  int ndim = 0;
  struct cleanup *back_to;
  unsigned int low, high;
  LONGEST array_size = 0;
  int has_lower_bound_attr = 0;
  struct die_info *ref_die;
  int lbound_offset, ubound_offset, stride_offset;
  struct symbol *lbound_sym, *ubound_sym, *stride_sym;
  struct type *stride_type = 0; /* initialize for compiler warning */
  struct type *lbound_type, *ubound_type; 
  enum bound_types {bound_is_unknown = -1, bound_is_constant = 0, 
		    bound_is_variable, bound_is_offset};
  enum bound_types *lbound_types = NULL, *ubound_types = NULL;
  int ubound_is_extent;
  enum stride_types {stride_is_default = 0, stride_is_variable,
		     stride_is_offset};
  enum stride_types stride_size_type = stride_is_default;
  int dynamic_bounds = 0; 
  unsigned int locdesc_offset;

  /* Return if we've already decoded this type. */
  if (die->type)
    {
      return;
    }

  element_type = die_type (die, objfile, cu_header);

  /* Default bounds to an array with unspecified length.  */
  low = 0;
  high = (unsigned int) -1;
  if (cu_language == language_fortran)
    {
      /* FORTRAN implies a lower bound of 1, if not given.  */
      low = 1;
    }

  /* Irix 6.2 native cc creates array types without children for
     arrays with unspecified length.  */
  if (die->has_children == 0)
    {
      index_type = dwarf2_fundamental_type (objfile, FT_INTEGER);
      range_type = create_range_type (NULL, index_type, low, high);
      die->type = create_array_type (NULL, element_type, range_type);
      TYPE_ARRAY_UPPER_BOUND_TYPE (die->type) = BOUND_CANNOT_BE_DETERMINED;
      return;
    }


  attr = dwarf_attr (die, DW_AT_byte_size);
  if (attr)
    {
      if (attr->form == DW_FORM_sdata)
	{
	  array_size = DW_SND (attr);
	}
      else if (attr->form == DW_FORM_udata
	       || attr->form == DW_FORM_data1
	       || attr->form == DW_FORM_data2
	       || attr->form == DW_FORM_data4)
	{
	  array_size = DW_UNSND (attr);
	}
      else
	{
	  complain (&dwarf2_non_const_array_attr_ignored,
		    dwarf_form_name (attr->form));
	}
    }

  back_to = make_cleanup (null_cleanup, NULL);
  child_die = die->next;
  while (child_die && child_die->tag)
    {
      if (child_die->tag == DW_TAG_subrange_type)
	{
	  /* Create structs to save bounds information for array type creation.
	     We arbitrarily allocate these in chunks of DW_FIELD_ALLOC_CHUNK 
	     size; chunk size needs to be at least 1  */
	  if ((ndim % DW_FIELD_ALLOC_CHUNK) == 0)
	    {
	      range_types = (struct type **)
		xrealloc (range_types, (ndim + DW_FIELD_ALLOC_CHUNK)
			  * sizeof (struct type *));
	      lbound_types = (enum bound_types *)
		xrealloc (lbound_types, (ndim + DW_FIELD_ALLOC_CHUNK)
			  * sizeof (enum bound_types));
	      ubound_types = (enum bound_types *)
		xrealloc (ubound_types, (ndim + DW_FIELD_ALLOC_CHUNK)
			  * sizeof (enum bound_types));
	      if (ndim == 0)
		{
		  make_cleanup (free_current_contents, &range_types);
		  make_cleanup (free_current_contents, &lbound_types);
		  make_cleanup (free_current_contents, &ubound_types);
		}
	    }

	  index_type = die_type (child_die, objfile, cu_header);

	  lbound_types[ndim] = bound_is_constant;
	  lbound_sym = NULL;
	  lbound_offset = 0;
	  lbound_type = NULL;
	  attr = dwarf_attr (child_die, DW_AT_lower_bound);
	  if (attr)
	    {
	      has_lower_bound_attr = 1;
	      if (attr->form == DW_FORM_sdata)
		{
		  low = (unsigned int) DW_SND (attr);
		}
	      else if (attr->form == DW_FORM_udata
		       || attr->form == DW_FORM_data1
		       || attr->form == DW_FORM_data2
		       || attr->form == DW_FORM_data4)
		{
		  low = (unsigned int) DW_UNSND (attr);
		}
	      else if (attr->form == DW_FORM_ref4)
		{
		  ref_die  = follow_die_ref (dwarf2_get_ref_die_offset (attr));
		  if (ref_die->tag == DW_TAG_variable)
		    {
		      lbound_sym = 
			new_symbol (ref_die, ref_die->type, objfile, cu_header);
		      lbound_type = ref_die->type;
	  	      lbound_types[ndim] = bound_is_variable;
		      dynamic_bounds = 1;
		    }
		  else if (ref_die->tag == DW_TAG_member)
		    {
		      attr = dwarf_attr (ref_die, DW_AT_data_member_location);
		      lbound_offset = 
			(int) decode_locdesc (NULL, attr, 
						  objfile,
						  &locdesc_offset);
		      lbound_type = die_type (ref_die, objfile, cu_header);
	  	      lbound_types[ndim] = bound_is_offset;
		      dynamic_bounds = 1;
		    }
		  else
		    complain (&dwarf2_non_const_array_attr_ignored,
                              dwarf_form_name (attr->form));
		}
	      else
		{
		  complain (&dwarf2_non_const_array_attr_ignored,
			    dwarf_form_name (attr->form));
#ifdef FORTRAN_HACK
		  die->type = lookup_pointer_type (element_type);
		  return;
#endif
		}
	    }

	  ubound_sym = NULL;
	  ubound_offset = 0;
	  ubound_type = NULL;
	  ubound_types[ndim] = bound_is_unknown;
	  attr = dwarf_attr (child_die, DW_AT_upper_bound);
	  if (attr)
	    {
	      ubound_types[ndim] = bound_is_constant;
	      if (attr->form == DW_FORM_sdata)
		{
		  high = (unsigned int) DW_SND (attr);
		}
	      else if (attr->form == DW_FORM_udata
		       || attr->form == DW_FORM_data1
		       || attr->form == DW_FORM_data2
		       || attr->form == DW_FORM_data4)
		{
		  high = (unsigned int) DW_UNSND (attr);
		}
	      else if (attr->form == DW_FORM_block1)
		{
		  /* GCC encodes arrays with unspecified or dynamic length
		     with a DW_FORM_block1 attribute.
		     FIXME: GDB does not yet know how to handle dynamic
		     arrays properly, treat them as arrays with unspecified
		     length for now.  */
		  high = (unsigned int) -1;
		}
	      else
		{
		  complain (&dwarf2_non_const_array_attr_ignored,
			    dwarf_form_name (attr->form));
#ifdef FORTRAN_HACK
		  die->type = lookup_pointer_type (element_type);
		  return;
#else
		  high = 1;
#endif
		}
	    }

	  ubound_is_extent = 0;
	  attr = dwarf_attr (child_die, DW_AT_count);
	  if (attr)
	    {
	      ubound_is_extent = 1;
	      ubound_types[ndim] = bound_is_constant;
	      if (attr->form == DW_FORM_sdata)
		{
		  high = (unsigned int) DW_SND (attr);
        	}
	      else if (attr->form == DW_FORM_udata
		       || attr->form == DW_FORM_data1
		       || attr->form == DW_FORM_data2
		       || attr->form == DW_FORM_data4)
		{
		  high = (unsigned int) DW_UNSND (attr);
		}
	      else
		{
		  ref_die  = find_die_from_ref (die, objfile, attr);
		  if (ref_die->tag == DW_TAG_variable)
		    {
		      ubound_sym= 
			new_symbol (ref_die, ref_die->type, objfile, cu_header);
		      ubound_type = ref_die->type;
		      ubound_types[ndim] = bound_is_variable;
		      dynamic_bounds = 1;
		    }
		  else if (ref_die->tag == DW_TAG_member)
		    {
		      attr = dwarf_attr (ref_die, DW_AT_data_member_location);
		      ubound_offset = (int) decode_locdesc (NULL, attr, 
						      objfile, 
						      &locdesc_offset);
		      ubound_type = die_type (ref_die, objfile, cu_header);
		      ubound_types[ndim] = bound_is_offset;
		      dynamic_bounds = 1;
		    }
		  else
		    complain (&dwarf2_non_const_array_attr_ignored,
			  dwarf_form_name (attr->form));
		}
	    }

	  /* In some languages, for example, Fortran, the lower bound
	     may be explicitly declared, so we need to calculate it if
	     the lower bound is not explicitly provided but the array
	     size and upper bounds are known.  */
	  if (!has_lower_bound_attr && array_size > 0 &&
	      ubound_types[ndim] == bound_is_constant)
	    {
	      /* check_typedef will not find the transparent type if we are 
		 currently reading the symtab which in this case we are. 
		 So if the type is pointing to an opaque type the size may be 0.
		 Avoid a divide by zero error by checking here.
		 Also for C and C++ the low has to be 0 which it has been initialized to.
	       */

 	      if (TYPE_LENGTH (CHECK_TYPEDEF (element_type)))
	        low = (unsigned int) (high -
		  array_size / TYPE_LENGTH(CHECK_TYPEDEF (element_type)) + 1);
	    }

          stride_size_type = stride_is_default;
	  stride_sym = NULL;
	  stride_offset = 0;
	  attr = dwarf_attr (child_die, DW_AT_stride_size);
	  if (attr)
	    {
              if (attr->form == DW_FORM_ref4)
		{
		  ref_die  = follow_die_ref (dwarf2_get_ref_die_offset (attr));
		  if (ref_die->tag == DW_TAG_variable)
		    {
		      stride_sym=
			new_symbol (ref_die, ref_die->type, objfile, cu_header);
		      stride_type = ref_die->type;
		      stride_size_type = stride_is_variable;
		    }
		  else if (ref_die->tag == DW_TAG_member)
		    {
		      attr = dwarf_attr (ref_die, DW_AT_data_member_location);
		      stride_offset = (int) decode_locdesc (NULL, attr, 
						      objfile, 
						      &locdesc_offset);
		      stride_type = die_type (ref_die, objfile, cu_header);
                      stride_size_type = stride_is_offset;
		    }
	          else
		    complain (&dwarf2_non_const_array_attr_ignored,
			      dwarf_form_name (attr->form));
		      
		}
	      else
		complain (&dwarf2_non_const_array_attr_ignored,
			  dwarf_form_name (attr->form));
	    }

	  /* Save range_type for array type creation.  */
	  if (stride_size_type == stride_is_default)
	    range_types[ndim] = create_range_type (NULL, index_type, low, high);
	  else
	    {
	      /* create a range type with 3 fields, the third field to store 
		 the stride information */
	      struct type *new_type = alloc_type (objfile);
	      TYPE_CODE (new_type) = TYPE_CODE_RANGE;
	      TYPE_TARGET_TYPE (new_type) = index_type;
	      TYPE_LENGTH (new_type) = TYPE_LENGTH (check_typedef (index_type));
	      TYPE_NFIELDS (new_type) = 3;
	      TYPE_FIELDS (new_type) = (struct field *)
		TYPE_ALLOC (new_type, 3 * sizeof (struct field));
	      memset (TYPE_FIELDS (new_type), 0, 3 * sizeof (struct field));
	      /* set range information with some default values */
	      TYPE_FIELD_BITPOS (new_type, 0) = low;
	      TYPE_FIELD_TYPE   (new_type, 0) = index_type;
	      TYPE_FIELD_BITPOS (new_type, 1) = high;
	      TYPE_FIELD_TYPE   (new_type, 1) = index_type;
	      range_types[ndim] = new_type;
	    }

	  /* Adjust the lower bound information, if necessary */
	  if (lbound_types[ndim] == bound_is_variable)
	    {
	      /* the bitpos field holds the address of the symbol;
		 this information is used at runtime to retrieve the 
	         array's lower bound */
	      TYPE_FIELD_BITPOS (range_types[ndim], 0) = (long) lbound_sym;
	      TYPE_FIELD_TYPE (range_types[ndim], 0) = lbound_type;
	    }
	  else if (lbound_types[ndim] == bound_is_offset)
	    {
	      /* the bitpos field holds the offset from base of the array 
		 descriptor which contains the lower bound; this information 
		 is used at runtime to retrieve the array's lower bound */
	      TYPE_FIELD_BITPOS (range_types[ndim], 0) = lbound_offset;
	      TYPE_FIELD_TYPE (range_types[ndim], 0) = lbound_type;
	    }

	  /* Adjust the upper bound information, if necessary */
	  if (ubound_types[ndim] == bound_is_variable)
	    {
	      /* the bitpos field holds the address of the symbol;
		 this information is used at runtime to retrieve the
		 array's upper bound */
	      TYPE_FIELD_BITPOS (range_types[ndim], 1) = (long) ubound_sym;
	      TYPE_FIELD_TYPE (range_types[ndim], 1) = ubound_type;
	    }
	  else if (ubound_types[ndim] == bound_is_offset)
	    {
	      /* the bitpos field holds the offset from base of the array
		 descriptor which contains the upper bound; this information
		 is used at runtime to retrieve the array's upper bound */
	      TYPE_FIELD_BITPOS (range_types[ndim], 1) = ubound_offset;
	      TYPE_FIELD_TYPE (range_types[ndim], 1) = ubound_type;
	    }
	  if (ubound_is_extent)
	    TYPE_FLAGS (range_types[ndim]) |= TYPE_FLAG_EXTENT;

	  /* set the stride information */
	  if (stride_size_type == stride_is_variable)
	    {
	      TYPE_FIELD_BITPOS (range_types[ndim], 2) = (long) stride_sym;
	      TYPE_FLAGS (range_types[ndim]) |= TYPE_FLAG_STRIDE_VARIABLE;
	    }
	  else if (stride_size_type == stride_is_offset)
	    {
	      TYPE_FIELD_BITPOS (range_types[ndim], 2) = stride_offset;
	      TYPE_FLAGS (range_types[ndim]) |= TYPE_FLAG_STRIDE_OFFSET;
	    }
	  if (stride_size_type != stride_is_default)
	    TYPE_FIELD_TYPE (range_types[ndim], 2) = stride_type;
          ndim++;
	}
      child_die = sibling_die (child_die);
    }

  /* Dwarf2 dimensions are output from left to right, create the
     necessary array types in backwards order.  */
  type = element_type;
  while (ndim-- > 0)
    {
      if (dynamic_bounds)
        type = create_dynamic_array_type (NULL, type, range_types[ndim]);
      else
        type = create_array_type (NULL, type, range_types[ndim]);

    /* Create fortran_struct_type to hold extra information for
       Fortran arrays */
    if (cu_language == language_fortran)
      ALLOCATE_FORTRAN_STRUCT_TYPE (type); 

    /* Set TYPE_ARRAY_LOWER_BOUND_TYPE and TYPE_ARRAY_UPPER_BOUND_TYPE
       based on information saved while processing bounds */
    if (lbound_types[ndim] == bound_is_variable)
      {
	TYPE_ARRAY_LOWER_BOUND_TYPE (type) = BOUND_BY_VARIABLE;
      }
    else if (lbound_types[ndim] == bound_is_offset)
      TYPE_ARRAY_LOWER_BOUND_TYPE (type) = BOUND_BY_VARIABLE_WITH_OFFSET;

    if (ubound_types[ndim] == bound_is_variable)
      {
	TYPE_ARRAY_UPPER_BOUND_TYPE (type) = BOUND_BY_VARIABLE;
      }
    else if (ubound_types[ndim] == bound_is_offset)
      {
	TYPE_ARRAY_UPPER_BOUND_TYPE (type) = BOUND_BY_VARIABLE_WITH_OFFSET;
      }
    else if (ubound_types[ndim] == bound_is_unknown)
      {
	TYPE_ARRAY_UPPER_BOUND_TYPE (type) = BOUND_CANNOT_BE_DETERMINED;
      }
    }
#ifdef HP_IA64
  /* For a Fortran character*n type, Dwarf2 emits array of int whose 
     element is name "CHARACTER", so create a string type here instead
     of an array type */
  if (cu_language == language_fortran
      && TYPE_CODE (element_type) == TYPE_CODE_INT
      && ((strcmp (TYPE_NAME (element_type), "CHARACTER")  == 0)
           || (strcmp (TYPE_NAME (element_type), "character")  == 0)))
    TYPE_CODE(type) = TYPE_CODE_STRING;
#endif

  do_cleanups (back_to);

  /* Install the type in the die. */
  die->type = type;
}

/* First cut: install each common block member as a global variable.  */

static void
read_common_block (struct die_info *die, struct objfile *objfile,
		   const struct comp_unit_head *cu_header)
{
  struct die_info *child_die;
  struct attribute *attr;
  struct symbol *sym;
  unsigned int locdesc_offset;

  CORE_ADDR base = (CORE_ADDR) 0;

  attr = dwarf_attr (die, DW_AT_location);
  if (attr)
    {
      base = decode_locdesc (NULL, attr, objfile, &locdesc_offset);
    }
  if (die->has_children)
    {
      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  sym = new_symbol (child_die, NULL, objfile, cu_header);
	  attr = dwarf_attr (child_die, DW_AT_data_member_location);
	  if (attr)
	    {
	      SYMBOL_VALUE_ADDRESS (sym) =
		base + decode_locdesc (sym, attr, objfile, 
				       &locdesc_offset);
	      add_symbol_to_list (sym, &global_symbols);
	    }
	  child_die = sibling_die (child_die);
	}
    }
}

/* Extract all information from a DW_TAG_pointer_type DIE and add to
   the user defined type vector.  */

static void
read_tag_pointer_type (struct die_info *die, struct objfile *objfile,
		       const struct comp_unit_head *cu_header)
{
  struct type *type;
  struct attribute *attr;

  if (die->type)
    {
      return;
    }

  type = lookup_pointer_type (die_type (die, objfile, cu_header));
  attr = dwarf_attr (die, DW_AT_byte_size);
  if (attr)
    {
      TYPE_LENGTH (type) = (unsigned int) DW_UNSND (attr);
    }
  else
    {
      TYPE_LENGTH (type) = address_size;
    }
  die->type = type;
}

/* Extract all information from a DW_TAG_ptr_to_member_type DIE and add to
   the user defined type vector.  */

static void
read_tag_ptr_to_member_type (struct die_info *die, struct objfile *objfile,
			     const struct comp_unit_head *cu_header)
{
  struct type *type;
  struct type *to_type;
  struct type *domain, *retvaltype, **args_type;
  int nargs = 0, i = 0;

  if (die->type)
    {
      return;
    }

  type = alloc_type (objfile);
  to_type = die_type (die, objfile, cu_header);
  domain = die_containing_type (die, objfile, cu_header);
  /* Poorva: for ptr to member functions create a method type 
     by taking the arguments and return type of the function 
     from the die and calling smashing to method_type. After that
     make a pointer type from it. */
  if (TYPE_CODE (to_type) == TYPE_CODE_FUNC)
    {
      retvaltype = TYPE_TARGET_TYPE (to_type);
      nargs = TYPE_NFIELDS (to_type);
      if (nargs > 0)
	{
	  args_type = (struct type **) xmalloc ((nargs + 1) * 
						sizeof (struct type *));
	  for (i = 0; i < nargs; i++)
	    {
	      args_type[i] = TYPE_FIELD_TYPE (to_type, i);
	    }
	  args_type[nargs] = NULL;
          }
      else
	{
	  args_type = NULL;
	}

      smash_to_method_type (type, domain, retvaltype, args_type);
      type = make_pointer_type (type, NULL);
      /* 
	 A pointer to member function is a pair as follows: 
	ptr: 
	 For a non-virtual function, this field is a simple function pointer. 
	 (Under current base Itanium psABI conventions, that is a pointer to a
	 GP/function address pair.) For a virtual function, it is 1 plus the 
	 virtual table offset (in bytes) of the function, represented as a 
	 ptrdiff_t. The value zero represents a NULL pointer, independent of 
	 the adjustment field value below. 
	 
	adj: 
	 The required adjustment to this, represented as a ptrdiff_t. 
	 It has the size, data size, and alignment of a class containing those
	 two members, in that order. (For 64-bit Itanium, that will be 16, 16,
	 and 8 bytes respectively.) 
	 
	 Whether or not it is virtual it has a size of 2 * sizeof (ptrdiff_t)
	 */
      if (is_swizzled)
	TYPE_LENGTH (type) = 4;
      else
	TYPE_LENGTH (type) = 8;
    }
  else
    {
       smash_to_member_type (type, domain, to_type);
       type = make_pointer_type (type, NULL);
    }
  
  die->type = type;
}

/* Extract all information from a DW_TAG_reference_type DIE and add to
   the user defined type vector.  */

static void
read_tag_reference_type (struct die_info *die, struct objfile *objfile,
			 const struct comp_unit_head *cu_header)
{
  struct type *type;
  struct attribute *attr;

  if (die->type)
    {
      return;
    }

  type = lookup_reference_type (die_type (die, objfile, cu_header));
  attr = dwarf_attr (die, DW_AT_byte_size);
  if (attr)
    {
      TYPE_LENGTH (type) = (unsigned int) DW_UNSND (attr);
    }
  else
    {
      TYPE_LENGTH (type) = address_size;
    }
  die->type = type;
}

static void
read_tag_const_type (struct die_info *die, struct objfile *objfile,
		     const struct comp_unit_head *cu_header)
{
  if (die->type)
    {
      return;
    }

  die->type = die_type (die, objfile, cu_header);
  die->type = make_cv_type (1, 
			    (TYPE_FLAGS (die->type) & TYPE_FLAG_VOLATILE) != 0, 
			    die->type, 
			    0);
}

static void
read_tag_volatile_type (struct die_info *die, struct objfile *objfile,
		     const struct comp_unit_head *cu_header)
{
  if (die->type)
    {
      return;
    }

  die->type = die_type (die, objfile, cu_header);
  die->type = make_cv_type ((TYPE_FLAGS (die->type) & TYPE_FLAG_CONST) != 0, 
			    1, 
			    die->type, 
			    0);
}

/* Extract all information from a DW_TAG_string_type DIE and add to
   the user defined type vector.  It isn't really a user defined type,
   but it behaves like one, with other DIE's using an AT_user_def_type
   attribute to reference it.  */

static void
read_tag_string_type (struct die_info *die, struct objfile *objfile,
		      const struct comp_unit_head *cu_header)
{
  struct type *type, *range_type, *index_type, *char_type;
  struct attribute *attr;
  unsigned int length;

  if (die->type)
    {
      return;
    }

#ifdef HP_IA64
  /* read_array_type handles string type */
  read_array_type (die, objfile, cu_header);
  return;
#else

  attr = dwarf_attr (die, DW_AT_string_length);
  if (attr)
    {
      length = DW_UNSND (attr);
    }
  else
    {
      length = 1;
    }
  index_type = dwarf2_fundamental_type (objfile, FT_INTEGER);
  range_type = create_range_type (NULL, index_type, 1, length);
  char_type = dwarf2_fundamental_type (objfile, FT_CHAR);
  type = create_string_type (char_type, range_type);
  die->type = type;
#endif
}

/* Handle DIES due to C code like:

   struct foo
   {
   int (*funcp)(int a, long l);
   int b;
   };

   ('funcp' generates a DW_TAG_subroutine_type DIE)
 */

static void
read_subroutine_type (struct die_info *die, struct objfile *objfile,
		      const struct comp_unit_head *cu_header)
{
  struct type *type;		/* Type that this function returns */
  struct type *ftype;		/* Function that returns above type */
  struct attribute *attr;

  /* Bindu 072204: We come here to read the catch_block type as well. Assume the return
     type to be void in this case. */
  if (die->tag == DW_TAG_catch_block)
    {
      type = dwarf2_fundamental_type (objfile, FT_VOID);;
    }
  else
    {
      /* Decode the type that this subroutine returns */
      if (die->type)
        {
          return;
        }
      type = die_type (die, objfile, cu_header);
    }
  ftype = lookup_function_type (type);

  /* All functions in C++ have prototypes.  */
  attr = dwarf_attr (die, DW_AT_prototyped);
  if ((attr && (DW_UNSND (attr) != 0))
      || cu_language == language_cplus)
    TYPE_FLAGS (ftype) |= TYPE_FLAG_PROTOTYPED;

  if (die->has_children)
    {
      struct die_info *child_die;
      int nparams = 0;
      int iparams = 0;

      if (cu_language == language_cplus)
	{
          /* Bindu 041702: Get the name of the contructors from the entrypoint
	     die. */
          char* entry_name = NULL;
          int entry_count = 0;
          struct attribute *entry_attr = dwarf_attr (die, DW_AT_count);
          if (entry_attr)
            entry_count = (int) DW_UNSND (entry_attr);

          if (entry_count > 1)
            {
              struct attribute* name_attr = dwarf_attr(die, DW_AT_name);
              child_die = die->next;
              while (child_die && child_die->tag)
                {
                  if (child_die->tag == DW_TAG_entry_point)
		    {
                      entry_name = dwarf2_linkage_name (child_die);
		      break;
		    }
                  child_die = sibling_die (child_die);
                }
              if (entry_name && (name_attr != 0)) /* attribute for name */
                {
	          if (!(is_cplus_name (name_attr->u.str, DEMANG))) /* JAGaf49121 */
                    name_attr->u.str = entry_name;
                }
            }
        }



      /* Count the number of parameters.
         FIXME: GDB currently ignores vararg functions, but knows about
         vararg member functions.  */
      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  if (child_die->tag == DW_TAG_formal_parameter)
	    nparams++;
	  else if (child_die->tag == DW_TAG_unspecified_parameters)
	    TYPE_FLAGS (ftype) |= TYPE_FLAG_VARARGS;
	  child_die = sibling_die (child_die);
	}

      /* Allocate storage for parameters and fill them in.  */
      TYPE_NFIELDS (ftype) = nparams;
      TYPE_FIELDS (ftype) = (struct field *)
	TYPE_ALLOC (ftype, nparams * sizeof (struct field));

      child_die = die->next;
      while (child_die && child_die->tag)
	{
	  if (child_die->tag == DW_TAG_formal_parameter)
	    {
	      /* Dwarf2 has no clean way to discern C++ static and non-static
	         member functions. G++ helps GDB by marking the first
	         parameter for non-static member functions (which is the
	         this pointer) as artificial. We pass this information
	         to dwarf2_add_member_fn via TYPE_FIELD_ARTIFICIAL.  */

	      attr = dwarf_attr (child_die, DW_AT_artificial);
	      if (attr)
		TYPE_FIELD_ARTIFICIAL (ftype, iparams) = DW_UNSND (attr);
	      else
		TYPE_FIELD_ARTIFICIAL (ftype, iparams) = 0;
	      TYPE_FIELD_TYPE (ftype, iparams) =
				die_type (child_die, objfile, cu_header);
	      iparams++;
	    }
	  child_die = sibling_die (child_die);
	}
    }
    
#ifdef HP_IA64
    /* This code is to check in DWARF and hold the info of how, the function 
       is expecting its parameters. It could be decided by the function's
       parameter's location attribute value.
       if it is:
       <location (0x0002)       block2 (0x03)    DW_OP_breg 33>
       then the function expects its parameter's value in reg 33.
       If it is:
       <location (0x0002)       block2 (0x03)    DW_OP_bregx 33 0>
       then the function expects its parameter's address in reg 33.
       
       So here we're are storing this information in type structure of 
       each function by reading DWARF. This information is used ONLY for 
       command line function call in hand_function_call() & ia64_push_arguments(). 
       If pass_by_addr set to 1 means WDB should pass an address of 
       aggregate parameter through registers to this function when this 
       function is called by command line call.
       else
       contents of aggregate parameter through registers to this function.
       For details Ps see: CR1000921083. 
    */
    /* Make sure that you read this location information of parameters of this
       function/sub routine only when OPT level is less than 2.
       At OPT level 2 the location information attribute is slightly different 
       and you can not depend on it. So it is better to skip if this function
       and parameters are compiled at OPT level 2 & above.
    */
    ftype->pass_by_addr = false;
    attr = dwarf_attr (die, DW_AT_HP_opt_level);
    if (attr)
     {
       if (DW_UNSND(attr) < 2) 
        if (die->has_children)
         {
           struct die_info *child_die = die->next;
           while (child_die && child_die->tag) 
            {
	      attr = NULL;
	      if (child_die->tag == DW_TAG_formal_parameter)
	       { 
	         attr = dwarf_attr (child_die, DW_AT_location);
                 struct dwarf_block *blk = DW_BLOCK (attr);
                 if (attr && blk->data)
	          {
                    char *data = blk->data;
                    unsigned char op = *data++;
                    if (DW_OP_bregx == op) 
                     {
	               ftype->pass_by_addr = true;
	               break;
	             }  
                  }
	       }
              child_die = sibling_die (child_die);
            }  
        }
     }
#endif
    die->type = ftype;
}

static void
read_typedef (struct die_info *die, struct objfile *objfile,
	      const struct comp_unit_head *cu_header)
{
  int decl_flag = 0;
  struct type *type;
  struct attribute *type_attr;

  if (!die->type)
    {
      struct attribute *attr;
      struct type *xtype;

      type_attr = dwarf_attr (die, DW_AT_type);

      if (!type_attr)
        decl_flag = 1;

      /* A missing DW_AT_type represents a void type.  */
      xtype = die_type (die, objfile, cu_header);

      attr = dwarf_attr (die, DW_AT_name);
      if (attr && DW_STRING (attr) == 0)
	{
	  /* Workaround for JAGab78312 - spurious typedef entries */
	  die->type = xtype;
	  return;
	}

      type = alloc_type (objfile);
      TYPE_CODE (type) = TYPE_CODE_TYPEDEF;
      TYPE_FLAGS (type) |= TYPE_FLAG_TARGET_STUB;
      TYPE_TARGET_TYPE (type) = xtype;

      if (decl_flag) {
         TYPE_DECLARATION (type) = 1;
         TYPE_TARGET_TYPE (type) = NULL;
      }

      if (attr && DW_STRING (attr))
	TYPE_NAME (type) = obsavestring (DW_STRING (attr),
					 (int) strlen (DW_STRING (attr)),
					 &objfile->type_obstack);

      die->type = type;
    }
}

#ifdef HP_DWARF2_EXTENSIONS
static void
read_array_descriptor_type (struct die_info *die, struct objfile *objfile,
	      const struct comp_unit_head *cu_header)
{
  struct type *type, *stype;
  struct attribute *attr;
  struct die_info *ref_die;
  unsigned int locdesc_offset;

  if (die->type)
    return;

  type = alloc_type (objfile);
  TYPE_CODE (type) = TYPE_CODE_ARRAY_DESC;

  attr = dwarf_attr (die, DW_AT_name);
  if (attr && DW_STRING (attr))
    TYPE_NAME (type) = obsavestring (DW_STRING (attr),
      (int) strlen (DW_STRING (attr)), &objfile->type_obstack);

  stype = die_type (die, objfile, cu_header);
  TYPE_LENGTH (type) = TYPE_LENGTH (stype);

  attr = dwarf_attr (die, DW_AT_base_types);

  ref_die = find_die_from_ref (die, objfile, attr);
  TYPE_TARGET_TYPE (type) = tag_type_to_type (ref_die, objfile, cu_header);

  /* Create two fields to hold the byte offsets to the base address
     of the array and to the allocated (i.e., exits) flag */

  TYPE_NFIELDS(type) = 2;
  TYPE_FIELDS (type) = (struct field *)
    TYPE_ALLOC (type, sizeof (struct field) * 2);
  memset (TYPE_FIELDS (type), 0, sizeof (struct field) * 2);

  /* DW_AT_HP_raw_data_ptr is a ref4 to a field of the array descriptor 
     structure where the address of the array resides */
  attr = dwarf_attr (die, DW_AT_HP_raw_data_ptr);
  ref_die  = find_die_from_ref (die, objfile, attr);
  attr = dwarf_attr (ref_die, DW_AT_data_member_location);
  if (attr)
    TYPE_ARRAY_DESC_BASE_ADDR_OFFSET (type) = 
      decode_locdesc (NULL, attr, objfile, &locdesc_offset);
  else
    TYPE_ARRAY_DESC_BASE_ADDR_OFFSET (type) = 0;

  /* DW_AT_allocated is a ref4 to a field of the array descriptor structure
     where the allocated flag resides */
  attr = dwarf_attr (die, DW_AT_allocated);
  ref_die  = find_die_from_ref (die, objfile, attr);
  attr = dwarf_attr (ref_die, DW_AT_data_member_location);
  if (attr)
    TYPE_ARRAY_DESC_ALLOCATED_OFFSET (type) = 
      decode_locdesc (NULL, attr, objfile, &locdesc_offset);
  else
    TYPE_ARRAY_DESC_ALLOCATED_OFFSET (type) = is_swizzled ? 9 : 17;

  die->type = type;
}
#endif

/* Find a representation of a given base type and install
   it in the TYPE field of the die.  */

static void
read_base_type (struct die_info *die, struct objfile *objfile)
{
  struct type *type;
  struct attribute *attr;
  int encoding = 0, size = 0;
  char *name;

  /* If we've already decoded this die, this is a no-op. */
  if (die->type)
    {
      return;
    }

  attr = dwarf_attr (die, DW_AT_encoding);
  if (attr)
    {
      encoding = (int) DW_UNSND (attr);
    }
  attr = dwarf_attr (die, DW_AT_byte_size);
  if (attr)
    {
      size = (int) DW_UNSND (attr);
    }
  attr = dwarf_attr (die, DW_AT_name);
  name = DW_STRING (attr);
  if (attr && name)
    {
      enum type_code code = TYPE_CODE_INT;
      int is_unsigned = 0;
      int typeid = -1;

      switch (encoding)
	{
	case DW_ATE_address:
	  /* Turn DW_ATE_address into a void * pointer.  */
	  code = TYPE_CODE_PTR;
	  is_unsigned = 1;
	  break;
	case DW_ATE_boolean:
	  code = TYPE_CODE_BOOL;
	  is_unsigned = 1;
	  break;
	case DW_ATE_complex_float:
	case DW_ATE_complex_float128:
	  code = TYPE_CODE_COMPLEX;
          switch (size)
            {
            case 8:
              typeid = FT_FLOAT;
              break;
            case 16:
              typeid = FT_DBL_PREC_FLOAT;
              break;
	    case 32:
	      typeid = FT_EXT_PREC_FLOAT;
	      break;

            default:
              complain (&dwarf2_unsupported_at_encoding,
                        dwarf_type_encoding_name (encoding));
            }
	  break;
	case DW_ATE_float:
	case DW_ATE_float128:
	  code = TYPE_CODE_FLT;
	  break;
	case DW_ATE_signed:
	case DW_ATE_signed_char:
#ifdef HP_IA64
          if (!strcmp(name, "char"))
	     is_unsigned |= TYPE_FLAG_NOSIGN;
#endif
	  break;
	case DW_ATE_unsigned:
	case DW_ATE_unsigned_char:
	  is_unsigned = 1;
	  break;
	case DW_ATE_imaginary_float:
	case DW_ATE_imaginary_float128:
	  /* FIXME Poorva: This code has not been implemented yet. This is 
	     here just to avoid a warning. Should properly be handled like
	     a complex number with the real part as 0. Similarly for the
	     DW_ATE_imaginary_float80 */
	  code = TYPE_CODE_FLT;
	  break;
	case DW_ATE_float80:
	case DW_ATE_imaginary_float80:
	  code = TYPE_CODE_FLOAT80;
	  break;
	case DW_ATE_complex_float80:
	  code = TYPE_CODE_FLOAT80_COMPLEX;
	  break;
	case DW_ATE_floathpintel:
	  code = TYPE_CODE_FLOATHPINTEL;
	  break;
	case DW_ATE_decimal_float:
	  code = TYPE_CODE_DECFLOAT;
          break;

	default:
	  complain (&dwarf2_unsupported_at_encoding,
		    dwarf_type_encoding_name (encoding));
	  break;
	}
      
#ifdef HP_IA64
      /* Workaround for f90 compiler which emits type names in upper case --
	 lower case them so that we print them consistently in lower case when 
	 printing type names. */
      if (cu_language == language_fortran)
	{
	  int i;
	  for (i = 0; i < strlen (name); i++)
	    name[i] = tolower (name[i]);
	}
#endif
      type = init_type (code, size, is_unsigned, name, objfile);
      if (encoding == DW_ATE_address)
	TYPE_TARGET_TYPE (type) = dwarf2_fundamental_type (objfile, FT_VOID);
      if (   (   encoding == DW_ATE_complex_float 
	      || encoding == DW_ATE_complex_float128) 
	  && typeid != -1)
	TYPE_TARGET_TYPE (type) = dwarf2_fundamental_type (objfile, typeid);
    }
  else
    {
      type = dwarf_base_type (encoding, size, objfile);
    }
  die->type = type;
}

/* Read a whole compilation unit into a linked list of dies.  */

struct die_info *
read_comp_unit (char *info_ptr, bfd *abfd,
		const struct comp_unit_head *cu_header)
{
  struct die_info *first_die, *last_die, *die;
  char *cur_ptr;
  int nesting_level;

  /* Reset die reference table, we are building a new one now. */
  if  (!processing_doom_objfile)
    dwarf2_empty_die_ref_table ();
  else
    dwarf2_empty_section_die_ref_table ();
  
  cur_ptr = info_ptr;
  nesting_level = 0;
  first_die = last_die = NULL;
  do
    {
      cur_ptr = read_full_die (&die, abfd, cur_ptr, cu_header);
      if (die->has_children)
	{
	  nesting_level++;
	}
      if (die->tag == 0)
	{
	  nesting_level--;
	}

      die->next = NULL;

      /* Enter die in reference hash table */
      store_in_ref_table (die->offset, die);

      if (!first_die)
	{
	  first_die = last_die = die;
	}
      else
	{
	  last_die->next = die;
	  last_die = die;
	}
    }
  while (nesting_level > 0);
  return first_die;
}

/* Free a linked list of dies.  */

static void
free_die_list (struct die_info *dies)
{
  struct die_info *die, *next;

  die = dies;
  while (die)
    {
      next = die->next;
      free (die);
      die = next;
    }
}

/* Free a die_ref_table while freeing objfile psymtabs. 
   This accepts void ** just because I did not want to add 
   many code changes for the caller in objfiles.c to know the 
   struct die_info type.
*/

void
free_die_ref_table (void **dt)
{
  struct die_info *die, *next;
  struct die_info **dtable = (struct die_info **) dt;
  int i;
  
  for (i = 0; i < REF_HASH_SIZE; i++)
    if (dtable [i])
      for (die = dtable [i]; die; die = next)
        {
          next = die->next_ref;
          free (die);
        }
#ifdef HP_IA64
  free (dtable);
#endif
}

#ifdef HP_IA64
/* Do a pass over the cached list and remove all the psymtabs of
   the particular objfile. */ 
void
free_cached_psymtabs (struct objfile *obj)
{
  int i;

  if (!debug_dwutils_processed)
    return;

  for (i = 0; i < cached_psymtabs_count; i++)
    if (cached_psymtabs[i].obj == obj)
      {
        /* copy the last element from the list and delete the last one. */
        memcpy (&cached_psymtabs[i], &cached_psymtabs [cached_psymtabs_count - 1], 
                sizeof (struct psymtabs_cache));    
        cached_psymtabs_count --;
        i --;
      }

}
#endif
/* Read the contents of the section at OFFSET and of size SIZE from the
   object file specified by OBJFILE into the psymbol_obstack and return it.  */

static char *
dwarf2_read_section (struct objfile *objfile, file_ptr offset, unsigned int size)
{
  bfd *abfd = objfile->obfd;
  char *buf;
  FILE *f;
  int fd;
  off_t offset_aligned;
  static size_t pagesize;
  size_t real_size;

  if (size == 0)
    return NULL;

#ifdef USING_MMAP
    /* Find the real file and the real offset into it.  */
  while (abfd->my_archive != NULL)
    {
      offset += abfd->origin;
      abfd = abfd->my_archive;
    }

  f = bfd_cache_lookup(abfd);
  fd = fileno (f);
  /* mmap requires the offset to be page aligned.
     Also align the page - after subtracting the differences we made to the
     offset */
  pagesize = getpagesize ();
  offset_aligned = offset - (offset % pagesize);
  real_size = offset + size - offset_aligned;
  real_size = real_size + pagesize - 1;
  real_size -= real_size % pagesize;

  buf = mmap (0, real_size, PROT_READ, MAP_PRIVATE, fd, offset_aligned);
  if (buf == (PTR) -1L)
    {
      buf = 0;
      error ("mmap for debug section failed");
      return buf;
    }

  /* Step forward in the buffer to the actual offset we required */
  buf = buf + (offset % pagesize);
#else
  buf = (char *) obstack_alloc (&objfile->psymbol_obstack, size);
  if ((bfd_seek (abfd, offset, SEEK_SET) != 0) ||
      (bfd_read (buf, size, 1, abfd) != size))
    {
      buf = NULL;
      error ("DWARF Error: Can't read DWARF data from '%s'",
	     bfd_get_filename (abfd));
    }
#endif
  return buf;
}


/* Poorva: Unmap all sections that have been mapped. Make sure that the 
   offset and size calculations are the same as when we did the mmap.*/

static void
dwarf2_unmap_section (char *buf, file_ptr offset, unsigned int size)
{
  off_t offset_aligned;
  static size_t pagesize;
  size_t real_size;
  void *real_buf;
  int retval;

  if (size == 0 || buf == 0)
    return;
  /* mmap requires the offset to be page aligned */
  pagesize = getpagesize ();
  offset_aligned = offset - (offset % pagesize);
  real_size = offset + size - offset_aligned;
  real_size = real_size + pagesize - 1;
  real_size -= real_size % pagesize;
  real_buf = buf - (offset % pagesize);
  retval = munmap (real_buf, real_size);
  if (retval == -1)
    {
      warning ("munmap failed with errno = %d", errno);
    }
}

/* In DWARF version 2, the description of the debugging information is
   stored in a separate .debug_abbrev section.  Before we read any
   dies from a section we read in all abbreviations and install them
   in a hash table.  */

static int
dwarf2_read_abbrevs (bfd *abfd, struct comp_unit_head *cu_header)
{
  char *abbrev_ptr;
  struct abbrev_info *cur_abbrev;
  unsigned int abbrev_number, bytes_read, abbrev_name;
  unsigned int abbrev_form, hash_number;
  unsigned int offset = cu_header->abbrev_offset;

  /* empty the table */
  if (!processing_doom_objfile)
    dwarf2_empty_abbrev_table (NULL);
  else
    dwarf2_init_section_abbrev_table ();
  
  abbrev_ptr = dwarf_abbrev_buffer + offset;
  abbrev_number = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
  abbrev_ptr += bytes_read;

  /* loop until we reach an abbrev number of 0 */
  while (abbrev_number)
    {
      cur_abbrev = dwarf_alloc_abbrev ();

      /* read in abbrev header */
      cur_abbrev->number = abbrev_number;
      cur_abbrev->tag = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
      abbrev_ptr += bytes_read;
      cur_abbrev->has_children = read_1_byte (abfd, abbrev_ptr);
      abbrev_ptr += 1;

      /* now read in declarations */
      abbrev_name = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
      abbrev_ptr += bytes_read;
      abbrev_form = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
      abbrev_ptr += bytes_read;
      
      while (abbrev_name)
	{
	  if ((cur_abbrev->num_attrs % ATTR_ALLOC_CHUNK) == 0)
	    {
	      cur_abbrev->attrs = (struct attr_abbrev *)
		xrealloc (cur_abbrev->attrs,
			  (cur_abbrev->num_attrs + ATTR_ALLOC_CHUNK)
			  * sizeof (struct attr_abbrev));
	    }

	  cur_abbrev->attrs[cur_abbrev->num_attrs].name = abbrev_name;
	  cur_abbrev->attrs[cur_abbrev->num_attrs++].form = abbrev_form;
	  abbrev_name = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
	  abbrev_ptr += bytes_read;
	  abbrev_form = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
	  abbrev_ptr += bytes_read;
	}

      hash_number = abbrev_number % ABBREV_HASH_SIZE;
      if (!processing_doom_objfile)
	{
	  cur_abbrev->next = dwarf2_abbrevs[hash_number];
	  dwarf2_abbrevs[hash_number] = cur_abbrev;
	}
      else
	{
	  cur_abbrev->next = debug_sections[sectn].dwarf2_abbrevs[hash_number];
	  debug_sections[sectn].dwarf2_abbrevs[hash_number] = cur_abbrev;
	}
      
      /* Get next abbreviation.
         Under Irix6 the abbreviations for a compilation unit are not
         always properly terminated with an abbrev number of 0.
         Exit loop if we encounter an abbreviation which we have
         already read (which means we are about to read the abbreviations
         for the next compile unit) or if the end of the abbreviation
         table is reached.  */
      if ((unsigned int) (abbrev_ptr - dwarf_abbrev_buffer)
	  >= dwarf_abbrev_size)
	break;
      abbrev_number = (unsigned int) read_unsigned_leb128 (abfd, abbrev_ptr, &bytes_read);
      abbrev_ptr += bytes_read;
#ifdef HP_IA64
      /* Under HP_IA64 duplicates may be an error but definitely not 
         an end of the abbreviations for a compilation unit.
       */
      if (dwarf2_lookup_abbrev (abbrev_number, cu_header) != NULL)
	warning ("Duplicate abbreviation code %d(0x%x) seen.\n", abbrev_number,
		 abbrev_number);
#else
      if (dwarf2_lookup_abbrev (abbrev_number, cu_header) != NULL)
	break;
#endif /* HP_IA64 */
    }
  /* This returns the size of the abbrev buffer that we just read in */
  return ((int) (abbrev_ptr - (dwarf_abbrev_buffer + offset)));
}

/* Empty the abbrev table for a new compilation unit.  */

/* ARGSUSED */
static void
dwarf2_empty_abbrev_table (PTR ignore)
{
  int i;
  struct abbrev_info *abbrev, *next;

  for (i = 0; i < ABBREV_HASH_SIZE; ++i)
    {
      next = NULL;
      abbrev = dwarf2_abbrevs[i];
      while (abbrev)
	{
	  next = abbrev->next;
	  free (abbrev->attrs);
	  free (abbrev);
	  abbrev = next;
	}
      dwarf2_abbrevs[i] = NULL;
    }
}

static void
dwarf2_empty_section_abbrev_table ()
{
  int i;
  struct abbrev_info *abbrev, *next;

  /* Allocating on the tmp_obstack - don't free.
   */
  for (i = 0; i < ABBREV_HASH_SIZE; ++i)
    {
      next = NULL;
      abbrev = debug_sections[sectn].dwarf2_abbrevs[i];
      while (abbrev)
	{
	  next = abbrev->next;
	  free (abbrev->attrs);
	  free (abbrev);
	  abbrev = next;
	}
      debug_sections[sectn].dwarf2_abbrevs[i] = NULL;
    }
}

static void
dwarf2_init_section_abbrev_table ()
{
  memset (debug_sections[sectn].dwarf2_abbrevs, 0, 
	  sizeof (debug_sections[sectn].dwarf2_abbrevs));
}

/* Lookup an abbrev_info structure in the abbrev hash table.  */

static struct abbrev_info *
dwarf2_lookup_abbrev (unsigned int number,
		      const struct comp_unit_head *cu_header)
{
  unsigned int hash_number;
  struct abbrev_info *abbrev;

  hash_number = number % ABBREV_HASH_SIZE;
  if (!processing_doom_objfile)
    abbrev = dwarf2_abbrevs[hash_number];
  else
    abbrev = debug_sections[sectn].dwarf2_abbrevs[hash_number];

  while (abbrev)
    {
      if (abbrev->number == number)
	return abbrev;
      else
	abbrev = abbrev->next;
    }
  return NULL;
}

/* Read a minimal amount of information into the minimal die structure.  */

static char *
read_partial_die (struct partial_die_info *part_die, bfd *abfd, char *info_ptr,
		  int *has_pc_info, const struct comp_unit_head *cu_header)
{
  unsigned int abbrev_number, bytes_read, i;
  struct abbrev_info *abbrev;
  static struct attribute attr;
  static struct attribute loc_attr;
  static struct attribute spec_attr;
  int found_spec_attr = 0;
  int has_low_pc_attr = 0;
  int has_high_pc_attr = 0;
  unsigned int sibling_offset;

  memset (part_die, 0, sizeof (struct partial_die_info));
  part_die->is_external = -1;
  *has_pc_info = 0;
  abbrev_number = (unsigned int) read_unsigned_leb128 (abfd, info_ptr, &bytes_read);
  info_ptr += bytes_read;
  if (!abbrev_number)
    return info_ptr;

  abbrev = dwarf2_lookup_abbrev (abbrev_number, cu_header);
  if (!abbrev)
    {
      error ("DWARF Error: Could not find abbrev number %d.", abbrev_number);
    }
  part_die->offset = (unsigned int) (info_ptr - dwarf_info_buffer - bytes_read);
  part_die->tag = abbrev->tag;
  part_die->has_children = abbrev->has_children;
  part_die->abbrev = abbrev_number;

  for (i = 0; i < abbrev->num_attrs; ++i)
    {
      enum dwarf_attribute attr_name;
      info_ptr = read_attribute (&attr, &abbrev->attrs[i], abfd,
				 info_ptr, cu_header);
      attr_name = attr.name;

      /* handle DW_AT_HP items separately to not perturb the switch 
       * See comment below dated 112602
       */
      if ((attr_name == DW_AT_HP_linkage_name) || 
	  (attr_name == DW_AT_MIPS_linkage_name))
        {
	  part_die->name = DW_STRING (&attr);
        }
      else if (attr_name == DW_AT_HP_actuals_stmt_list)
	{
          dwarf_line_offset_actual_pst = DW_UNSND (&attr);
	}
		
      else 
      /* Store the data if it is of an attribute we want to keep in a
         partial symbol table.  */
      switch (attr_name)
	{
	case DW_AT_name:

	  /* Prefer DW_AT_MIPS_linkage_name over DW_AT_name.  */
	  if (part_die->name == NULL)
	    part_die->name = DW_STRING (&attr);
	  break;
	case DW_AT_comp_dir:
	  part_die->dirname = DW_STRING (&attr);
	  break;
	case DW_AT_low_pc:
	  has_low_pc_attr = 1;
	  part_die->lowpc = DW_ADDR (&attr);
	  break;
	case DW_AT_high_pc:
	  has_high_pc_attr = 1;
	  part_die->highpc = DW_ADDR (&attr);
	  break;
	case DW_AT_location:
	  loc_attr = attr;
	  part_die->locdesc = &loc_attr;
	  break;
	case DW_AT_language:
	  part_die->language = DW_UNSND (&attr);
	  break;
	case DW_AT_count:
	  part_die->entry_count = (unsigned int) DW_UNSND (&attr);
	  break;
	case DW_AT_external:
	  part_die->is_external = (DW_UNSND (&attr) != 0);
	  break;
	case DW_AT_declaration:
	  part_die->is_declaration = (DW_UNSND (&attr) != 0);
	  break;
	case DW_AT_calling_convention:
	  part_die->calling_convention = DW_UNSND (&attr);
	  break;
	case DW_AT_type:
	  part_die->has_type = 1;
	  break;
	case DW_AT_abstract_origin:
	case DW_AT_specification:
	  found_spec_attr = 1;
	  spec_attr = attr;
	  break;
        case DW_AT_stmt_list:
          dwarf_line_offset_pst = DW_UNSND (&attr);
          break;
        case DW_AT_producer:
          part_die->prof_flags = DW_STRING (&attr);
          break;
	case DW_AT_byte_size:
	  part_die->type_length = (unsigned int) DW_UNSND (&attr);
	  break;
	case DW_AT_sibling:
	  /* Ignore absolute siblings, they might point outside of
	     the current compile unit.  */
	  if (attr.form == DW_FORM_ref_addr)
	    complain (&dwarf2_absolute_sibling_complaint);
	  else
	    {
	      /* JAGab77765 - error in debug info;  Ignore a sibling attribute
		 with an offset of zero. */
	      sibling_offset =   dwarf2_get_ref_die_offset (&attr) 
			       - cu_header_offset;
	      if (sibling_offset == 0)
		part_die->sibling = 0;
	      else
		part_die->sibling =
		  dwarf_info_buffer + dwarf2_get_ref_die_offset (&attr);
	    }
	  break;
	default:
	  break;
	}
    }

  /* If we found a reference attribute and the die has no name, try
     to find a name in the referred to die.  */

  if (found_spec_attr && part_die->name == NULL)
    {
      struct partial_die_info spec_die;
      char *spec_ptr;
      int dummy;

      spec_ptr = dwarf_info_buffer + dwarf2_get_ref_die_offset (&spec_attr);
      read_partial_die (&spec_die, abfd, spec_ptr, &dummy, cu_header);
      if (spec_die.name)
	{
	  part_die->name = spec_die.name;

	  /* Copy DW_AT_external attribute if it is set.  */
	  if (spec_die.is_external)
	    part_die->is_external = spec_die.is_external;
	}
    }

  /* When using the GNU linker, .gnu.linkonce. sections are used to
     eliminate duplicate copies of functions and vtables and such.
     The linker will arbitrarily choose one and discard the others.
     The AT_*_pc values for such functions refer to local labels in
     these sections.  If the section from that file was discarded, the
     labels are not in the output, so the relocs get a value of 0.
     If this is a discarded function, mark the pc bounds as invalid,
     so that GDB will ignore it.  */
  if (has_low_pc_attr && has_high_pc_attr
      && part_die->lowpc < part_die->highpc
      && (part_die->lowpc != 0
	  || (bfd_get_file_flags (abfd) & HAS_RELOC)))
    *has_pc_info = 1;
  return info_ptr;
}

/* Read the die from the .debug_info section buffer.  And set diep to
   point to a newly allocated die with its information.  */

static char *
read_full_die (struct die_info **diep, bfd *abfd, char *info_ptr,
	       const struct comp_unit_head *cu_header)
{
  unsigned int abbrev_number, bytes_read, i, offset;
  struct abbrev_info *abbrev;
  struct die_info *die;

  offset = (unsigned int) (info_ptr - dwarf_info_buffer);
  abbrev_number = (unsigned int) read_unsigned_leb128 (abfd, info_ptr, &bytes_read);
  info_ptr += bytes_read;
  if (!abbrev_number)
    {
      die = dwarf_alloc_die ();
      die->tag = 0;
      die->abbrev = abbrev_number;
      die->type = NULL;
      *diep = die;
      return info_ptr;
    }

  abbrev = dwarf2_lookup_abbrev (abbrev_number, cu_header);
  if (!abbrev)
    {
      error ("DWARF Error: could not find abbrev number %d.", abbrev_number);
    }

  /* Allocate "struct die_info" together with the "struct attribute" 
     array, reducing the number of malloc/free calls here by half.
     Since the "struct attribute" array has to be 8-byte aligned,
     also reserve space for the alignment hole (if any) after the
     "struct die_info". */
  die = (struct die_info *) 
	xmalloc (sizeof (struct die_info) + 
		     (sizeof(struct die_info) % 8) +
  		     (abbrev->num_attrs * sizeof (struct attribute)));
  die->offset = offset;
  die->tag = abbrev->tag;
  die->has_children = abbrev->has_children;
  die->abbrev = abbrev_number;
  die->type = NULL;

  die->num_attrs = abbrev->num_attrs;
  die->next_ref = NULL;
  die->next = NULL;
  /* Point "attrs" at the struct attribute array we allocated together with 
     the struct die_info above, allowing alignment hole between if necessary. */
  die->attrs = (struct attribute *) 
		(void *) (((char *)(&die[1])) + (sizeof(struct die_info) % 8));
				/* allow for 8-byte alignment of CORE_ADDR */
  for (i = 0; i < abbrev->num_attrs; ++i)
    {
      info_ptr = read_attribute (&die->attrs[i], &abbrev->attrs[i],
				 abfd, info_ptr, cu_header);
    }

  *diep = die;
  return info_ptr;
}

/* Read an attribute described by an abbreviated attribute.  */

static char *
read_attribute (struct attribute *attr, struct attr_abbrev *abbrev, bfd *abfd,
		char *info_ptr, const struct comp_unit_head *cu_header)
{
  unsigned int bytes_read;
  struct dwarf_block *blk;

  attr->name = abbrev->name;
  attr->form = abbrev->form;
  switch (abbrev->form)
    {
    case DW_FORM_addr:
    case DW_FORM_ref_addr:
      DW_ADDR (attr) = read_address (abfd, info_ptr);
      info_ptr += address_size;
      break;
    case DW_FORM_block2:
      blk = dwarf_alloc_block ();
      blk->size = read_2_bytes (abfd, info_ptr);
      info_ptr += 2;
      blk->data = read_n_bytes (abfd, info_ptr, blk->size);
      info_ptr += blk->size;
      DW_BLOCK (attr) = blk;
      break;
    case DW_FORM_block4:
      blk = dwarf_alloc_block ();
      blk->size = read_4_bytes (abfd, info_ptr);
      info_ptr += 4;
      blk->data = read_n_bytes (abfd, info_ptr, blk->size);
      info_ptr += blk->size;
      DW_BLOCK (attr) = blk;
      break;
    case DW_FORM_data2:
      DW_UNSND (attr) = read_2_bytes (abfd, info_ptr);
      info_ptr += 2;
      break;
    case DW_FORM_data4:
      DW_UNSND (attr) = read_4_bytes (abfd, info_ptr);
      info_ptr += 4;
      break;
    case DW_FORM_data8:
      DW_UNSND (attr) = read_8_bytes (abfd, info_ptr);
      info_ptr += 8;
      break;
    case DW_FORM_string:
#ifdef HP_IA64
      /* prefetch two cache lines off "info_ptr", for strlen inside read_string() */
      _Asm_lfetch_excl(_LFTYPE_NONE,_LFHINT_NONE,info_ptr+64);
      _Asm_lfetch_excl(_LFTYPE_NONE,_LFHINT_NONE,info_ptr+128);
#endif
      DW_STRING (attr) = read_string (abfd, info_ptr, &bytes_read);
      info_ptr += bytes_read;
      break;
    case DW_FORM_strp:
      DW_STRING (attr) = read_indirect_string (abfd, info_ptr, &bytes_read);
      info_ptr += bytes_read;
      break;
    case DW_FORM_block:
      blk = dwarf_alloc_block ();
      blk->size = (unsigned int) read_unsigned_leb128 (abfd, info_ptr, &bytes_read);
      info_ptr += bytes_read;
      blk->data = read_n_bytes (abfd, info_ptr, blk->size);
      info_ptr += blk->size;
      DW_BLOCK (attr) = blk;
      break;
    case DW_FORM_block1:
      blk = dwarf_alloc_block ();
      blk->size = read_1_byte (abfd, info_ptr);
      info_ptr += 1;
      blk->data = read_n_bytes (abfd, info_ptr, blk->size);
      info_ptr += blk->size;
      DW_BLOCK (attr) = blk;
      break;
    case DW_FORM_data1:
      DW_UNSND (attr) = read_1_byte (abfd, info_ptr);
      info_ptr += 1;
      break;
    case DW_FORM_flag:
      DW_UNSND (attr) = read_1_byte (abfd, info_ptr);
      info_ptr += 1;
      break;
    case DW_FORM_sdata:
      DW_SND (attr) = read_signed_leb128 (abfd, info_ptr, &bytes_read);
      info_ptr += bytes_read;
      break;
    case DW_FORM_udata:
      DW_UNSND (attr) = read_unsigned_leb128 (abfd, info_ptr, &bytes_read);
      info_ptr += bytes_read;
      break;
    case DW_FORM_ref1:
      DW_UNSND (attr) = read_1_byte (abfd, info_ptr);
      info_ptr += 1;
      break;
    case DW_FORM_ref2:
      DW_UNSND (attr) = read_2_bytes (abfd, info_ptr);
      info_ptr += 2;
      break;
    case DW_FORM_ref4:
      DW_UNSND (attr) = read_4_bytes (abfd, info_ptr);
      info_ptr += 4;
      break;
    case DW_FORM_ref_udata:
      DW_UNSND (attr) = read_unsigned_leb128 (abfd, info_ptr, &bytes_read);
      info_ptr += bytes_read;
      break;
    case DW_FORM_indirect:
    default:
      error ("DWARF Error: Cannot handle %s in DWARF reader.",
	     dwarf_form_name (abbrev->form));
    }
  return info_ptr;
}

/* read dwarf information from a buffer */

static unsigned int
read_1_byte (bfd *abfd, char *buf)
{
  return bfd_get_8 (abfd, (bfd_byte *) buf);
}

static int
read_1_signed_byte (bfd *abfd, char *buf)
{
  return bfd_get_signed_8 (abfd, (bfd_byte *) buf);
}

static unsigned int
read_2_bytes (bfd *abfd, char *buf)
{
  return (unsigned int) (bfd_get_16 (abfd, (bfd_byte *) buf));
}

static int
read_2_signed_bytes (bfd *abfd, char *buf)
{
  return (unsigned int) (bfd_get_signed_16 (abfd, (bfd_byte *) buf));
}

static unsigned int
read_4_bytes (bfd *abfd, char *buf)
{
  return (unsigned int ) (bfd_get_32 (abfd, (bfd_byte *) buf));
}

static int
read_4_signed_bytes (bfd *abfd, char *buf)
{
  return (unsigned int)(bfd_get_signed_32 (abfd, (bfd_byte *) buf));
}

static unsigned long
read_8_bytes (bfd *abfd, char *buf)
{
  return bfd_get_64 (abfd, (bfd_byte *) buf);
}

static CORE_ADDR
read_address (bfd *abfd, char *buf)
{
  CORE_ADDR retval = 0;

  switch (address_size)
    {
    case 2:
      retval = bfd_get_16 (abfd, (bfd_byte *) buf);
      break;
    case 4:
      retval = bfd_get_32 (abfd, (bfd_byte *) buf);
      break;
    case 8:
      retval = bfd_get_64 (abfd, (bfd_byte *) buf);
      break;
    default:
      /* *THE* alternative is 8, right? */
      abort ();
    }

 return retval;
}

static char *
read_n_bytes (bfd *abfd, char *buf, unsigned int size)
{
  /* Poorva - For fortran we try to make all strings lower case but since
     we mmap stuff and don't open it for writing we get into trouble so
     instead allocate memory for the string on the tmp obstack which
     is freed later. */
  /* If the size of a host char is 8 bits, we can return a pointer
     to the buffer, otherwise we have to copy the data to a buffer
     allocated on the temporary obstack.  */
#if !defined(USING_MMAP) && HOST_CHAR_BIT == 8
  return buf;
#else
  char *ret;
  unsigned int i;
 
  ret = obstack_alloc (&dwarf2_tmp_obstack, size);
  for (i = 0; i < size; ++i)
    {
      ret[i] = bfd_get_8 (abfd, (bfd_byte *) buf);
      buf++;
    }
  return ret;
#endif
}

/* Read an offset from the data stream.  The size of the offset is
   given by cu_header->offset_size. */

static LONGEST
read_offset (bfd *abfd, char *buf, int *bytes_read)
{
   LONGEST retval = 0;

   retval = bfd_get_32 (abfd, (bfd_byte *) buf);
   *bytes_read = 4;

   return retval;
}

static char *
read_string (bfd *abfd, char *buf, unsigned int *bytes_read_ptr)
{
  int len = 0;

  /* Poorva - With us freeing the debug info we can't just materialize
     a pointer to inside it. We need to actually malloc space for it */
  /* If the size of a host char is 8 bits, we can return a pointer
     to the string, otherwise we have to copy the string to a buffer
     allocated on the temporary obstack.  */
#if !defined(USING_MMAP) && HOST_CHAR_BIT == 8
  if (*buf == '\0')
    {
      *bytes_read_ptr = 1;
      return NULL;
    }
  *bytes_read_ptr = strlen (buf) + 1;
  return buf;
#else

/* Poorva: 3rd September, 2002 - takes too long since obstack_grow
   memcpy's a byte at a time - get the len and grow it all at once.
   Performance difference of taking 18.8% of total time vs. taking only
   2.2 % of the total time as measured on Java. */

  if ((buf == NULL) || (*buf == 0))
  {
    *bytes_read_ptr = 1;
    return NULL; 
  }
  len = (int) strlen (buf);
  obstack_grow (&dwarf2_tmp_obstack, (bfd_byte *) buf, len + 1);
  *bytes_read_ptr = len + 1;

  return obstack_finish (&dwarf2_tmp_obstack);
#endif
}

static char *
read_indirect_string (bfd *abfd, char *buf, unsigned int *bytes_read_ptr)
{
  LONGEST str_offset = read_offset (abfd, buf, (int *) bytes_read_ptr);

  if (dwarf_str_buffer == NULL)
    {
      error ("DW_FORM_strp used without .debug_str section");
      return NULL;
    }
  if (str_offset >= dwarf_str_size)
    {
      error ("DW_FORM_strp pointing outside of .debug_str section");
      return NULL;
    }
  if (dwarf_str_buffer[str_offset] == '\0')
    return NULL;
  return dwarf_str_buffer + str_offset;
}

static unsigned long
read_unsigned_leb128 (bfd *abfd, char *buf, unsigned int *bytes_read_ptr)
{
  unsigned char byte;
  unsigned long result = 0;
  int shift = 0;
  char *orig_buf = buf;

  while (1)
    {
      byte = bfd_get_8 (abfd, (bfd_byte *) buf);
      buf++;
      result |= ((unsigned long)(byte & (long) 127) << shift);
      if ((byte & 128) == 0)
	{
	  break;
	}
      shift += 7;
    }
  *bytes_read_ptr = (unsigned int) (buf - orig_buf);
  return result;
}

static long
read_signed_leb128 (bfd *abfd, char *buf,  unsigned int *bytes_read_ptr)
{
  long result;
  int shift, size, num_read;
  unsigned char byte;

  result = 0;
  shift = 0;
  size = 32;
  num_read = 0;
  byte = 0;  /* initialize for compiler warning */
  while (1)
    {
      byte = bfd_get_8 (abfd, (bfd_byte *) buf);
      buf++;
      num_read++;
      result |= ((long)(byte & 127) << shift);
      shift += 7;
      if ((byte & 128) == 0)
	{
	  break;
	}
    }
  if ((shift < size) && (byte & 0x40))
    {
      result |= -(1 << shift);
    }
  *bytes_read_ptr = num_read;
  return result;
}

static void
set_cu_language (unsigned int lang)
{
  switch (lang)
    {
    case DW_LANG_C89:
    case DW_LANG_C:
      cu_language = language_c;
      break;
    case DW_LANG_C_plus_plus:
      cu_language = language_cplus;
      break;
    case DW_LANG_Fortran77:
    case DW_LANG_Fortran90:
      cu_language = language_fortran;
      break;
    case DW_LANG_Mips_Assembler:
      cu_language = language_asm;
      break;
    case DW_LANG_Ada83:
    case DW_LANG_Cobol74:
    case DW_LANG_Cobol85:
    case DW_LANG_Pascal83:
    case DW_LANG_Modula2:
    case DW_LANG_Java:
    case DW_LANG_C99:
    case DW_LANG_Ada95:
    case DW_LANG_FORTRAN95:
    default:
      cu_language = language_unknown;
      break;
    }
  cu_language_defn = language_def (cu_language);
}

/* Return the named attribute or NULL if not there.  */

static struct attribute *
dwarf_attr (struct die_info *die, unsigned int name)
{
  unsigned int i;
  struct attribute *spec = NULL;

  for (i = 0; i < die->num_attrs; ++i)
    {
      if (die->attrs[i].name == name)
	{
	  return &die->attrs[i];
	}
      if (die->attrs[i].name == DW_AT_specification
	  || die->attrs[i].name == DW_AT_abstract_origin)
	spec = &die->attrs[i];
    }
  if (spec)
    {
      struct die_info *ref_die =
      find_die_from_ref (die, current_objfile, spec);

      if (ref_die)
	return dwarf_attr (ref_die, name);
    }

  return NULL;
}

/* return pc at context_ref + base index of line table for the subfile */
static CORE_ADDR 
find_context_ref_pc (struct subfile *subfile, int context_ref, int base)
{
    struct linetable *l = subfile->line_vector;

    if (!l)
      {
         warning("No line table found for subfile. Inline support may not work correctly.");
         return NULL;
      }

    if (context_ref + base - 1 > l->nitems)
      {
         warning("Bad context ref in line table. Inline support may not work correctly.");
         return NULL;
      }
    return l->item[context_ref + base - 1].pc;
}

/* return line at context_ref + base index of line table for the subfile */
static int
find_context_ref_line (struct subfile *subfile, int context_ref, int base)
{
  struct linetable *l = subfile->line_vector;

  if (!l)
    {
      warning("No line table found for subfile. Inline support may not work correctly.");
      return NULL;
    }

  if (context_ref + base - 1 > l->nitems)
    {
      warning("Bad context ref in line table. Inline support may not work correctly.");
      return NULL;
    }
  return l->item[context_ref + base - 1].line;
}

/* return inline_idx at context_ref + base index of line table for the subfile */
static int
find_context_ref_idx (struct subfile *subfile, int context_ref, int base)
{
  struct linetable *l = subfile->line_vector;

  if (!l)
    {
      warning("No line table found for subfile. Inline support may not work correctly.");
      return NULL;
    }

  if (context_ref + base - 1 > l->nitems)
    {
      warning("Bad context ref in line table. Inline support may not work correctly.");
      return NULL;
    }
  return l->item[context_ref + base - 1].inline_idx;
}

static int
find_context_ref_column (struct subfile *subfile, int context_ref, int base)
{
  struct linetable *l = subfile->line_vector;

  if (!l) {
    warning ("No line table found for subfile. Inline support may not work correctly.");
    return NULL;
  }

  if (context_ref + base - 1 > l->nitems) {
    warning ("Bad context ref in line table. Inline support may not work correctly.");
    return NULL;
  }
  return l->item[context_ref + base - 1].column;
}


/* Reads the logical line table. Has been cloned by 
   get_filename_from_linetable */
static void
dwarf_decode_lines (unsigned int offset, char *comp_dir, bfd *abfd)
{
  char *line_ptr;
  char *line_end;
  struct line_head lh;
  struct cleanup *back_to;
  unsigned int i;
  unsigned int bytes_read;
  /* Func name index of the current line table entry. */
  unsigned int current_line_idx = 0;

  /* context_ref of the current line table entry. */
  int context_ref = 0;
  /* Previously read in context_ref different from the current one. */
  int prev_context_ref = 0;
  /* context_ref of the previous line table entry. */
  int prev_line_context_ref = 0;
  /* After a call, we got a new inline instance, so the
       prev_line_context_ref != context_ref
     and the new context_ref will be greater than prev_context_ref
     i.e., caller's context ref. 
       context_ref > prev_context_ref

     After the return, we are back in the caller, so the
       prev_line_context_ref != context_ref again
     and the new context_ref will be less than prev_context_ref
       context_ref < prev_context_ref
    */

  char *cur_file, *cur_dir;
  char *proc_name;
  unsigned char op_code, extended_op, adj_opcode;
#ifdef HP_DWARF2_EXTENSIONS
  int context_stack = 0;  /* use counter until implemented */
#endif

  int inline_idx = 0;
  int prev_inline_idx = 0;
  CORE_ADDR context_ref_pc = 0;
  int context_ref_line = 0;
  int context_ref_idx = 0;
  char* context_ref_file = 0;
  int context_ref_base = 0;

#define FILE_ALLOC_CHUNK 5
#define DIR_ALLOC_CHUNK 5

  struct filenames files;
  struct directories dirs;

  boolean is_address_set = false;
  /* JAGae39163 - prev_line used to handle gcc's different line tables. */
  int prev_line = 0;
  /* Save main subfile so that we get the actuals correct. */
  struct subfile *main_subfile = get_main_subfile();

  int context_ref_column = 0;

  if (dwarf_line_buffer == NULL)
    {
      complain (&dwarf2_missing_line_number_section);
      return;
    }

  files.num_files = 0;
  files.files = NULL;

  dirs.num_dirs = 0;
  dirs.dirs = NULL;

#ifndef USING_MMAP
    line_ptr = dwarf_line_buffer + offset;
#else
    /* dwarf_line_buffer now points to the buffer for this .o. Don't
     need to add any offset to it */
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  if (reading_MLT) 
    {
      if (offset > 0)
        line_ptr = dwarf_line_buffer + offset;
      else
        line_ptr = dwarf_line_buffer;
    }
  else 
#endif
    {
      if (!processing_doom_objfile)
        line_ptr = dwarf_line_buffer;
      else
        line_ptr = dwarf_line_buffer + offset;
    }
#endif

  /* read in the prologue */
  lh.total_length = read_4_bytes (abfd, line_ptr);
  if (!lh.total_length)
    return;
  line_ptr += 4;
  line_end = line_ptr + lh.total_length;
  lh.version = read_2_bytes (abfd, line_ptr);
  line_ptr += 2;
  lh.prologue_length = read_4_bytes (abfd, line_ptr);
  line_ptr += 4;
  lh.minimum_instruction_length = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.default_is_stmt = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.line_base = read_1_signed_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.line_range = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.opcode_base = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.standard_opcode_lengths = (unsigned char *)
    xmalloc (lh.opcode_base * sizeof (unsigned char));
  back_to = make_cleanup (free_current_contents, &lh.standard_opcode_lengths);

  lh.standard_opcode_lengths[0] = 1;
  for (i = 1; i < lh.opcode_base; ++i)
    {
      lh.standard_opcode_lengths[i] = read_1_byte (abfd, line_ptr);
      line_ptr += 1;
    }

  /* Read directory table  */
  while ((cur_dir = read_string (abfd, line_ptr, &bytes_read)) != NULL)
    {
      line_ptr += bytes_read;
      if ((dirs.num_dirs % DIR_ALLOC_CHUNK) == 0)
	{
	  dirs.dirs = (char **)
	    xrealloc (dirs.dirs,
		      (dirs.num_dirs + DIR_ALLOC_CHUNK) * sizeof (char *));
	  if (dirs.num_dirs == 0)
	    make_cleanup (free_current_contents, &dirs.dirs);
	}
      dirs.dirs[dirs.num_dirs++] = cur_dir;
    }

  line_ptr += bytes_read;

  /* Read file name table */
  while ((cur_file = read_string (abfd, line_ptr, &bytes_read)) != NULL)
    {
      line_ptr += bytes_read;
      if ((files.num_files % FILE_ALLOC_CHUNK) == 0)
	{
	  files.files = (struct fileinfo *)
	    xrealloc (files.files,
		      (files.num_files + FILE_ALLOC_CHUNK)
		      * sizeof (struct fileinfo));
	  if (files.num_files == 0)
	    make_cleanup (free_current_contents, &files.files);
	}
      files.files[files.num_files].name = cur_file;
      files.files[files.num_files].dir =
	(unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
      line_ptr += bytes_read;
      files.files[files.num_files].time =
	(unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
      line_ptr += bytes_read;
      files.files[files.num_files].size =
	(unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
      line_ptr += bytes_read;
      files.num_files++;
    }
  line_ptr += bytes_read;

  context_ref_base = main_subfile->line_vector->nitems;

  /* Read the statement sequences until there's nothing left.  */
  while (line_ptr < line_end)
    {
      /* state machine registers  */
      CORE_ADDR address = 0;
      unsigned int file = 1;
      unsigned int line = 1;
      unsigned int column = 0;
      int is_stmt = lh.default_is_stmt;
      int end_sequence = 0;
#ifdef HP_DWARF2_EXTENSIONS
      int is_UV_update = 0;
      int post_semantics = 0;
      int function_exit = 0;
      int is_front_end_logical = 0; 
#endif

      /* Start a subfile for the current file of the state machine.  */
      if (files.num_files >= file)
	{
	  /* The file and directory tables are 0 based, the references
	     are 1 based.  */
	  dwarf2_start_subfile (files.files[file - 1].name,
				(files.files[file - 1].dir
				 ? dirs.dirs[files.files[file - 1].dir - 1]
				 : comp_dir));
	}

      /* Decode the table. */
      /* Should this be while (line_ptr < line_end) */
      while (!end_sequence)
	{
	  op_code = read_1_byte (abfd, line_ptr);
	  line_ptr += 1;
	  switch (op_code)
	    {
	    case DW_LNS_extended_op:
	      /* skip length field */
	      (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      extended_op = read_1_byte (abfd, line_ptr);
	      line_ptr += 1;
	      switch (extended_op)
		{
		case DW_LNE_end_sequence:
		  end_sequence = 1;
		  /* Don't call record_line here.  The end_sequence
		     instruction provides the address of the first byte
		     *after* the last line in the sequence; it's not the
		     address of any real source line.  However, the GDB
		     linetable structure only records the starts of lines,
		     not the ends.  This is a weakness of GDB.  */

		  break;
		case DW_LNE_set_address:
		  address = read_address (abfd, line_ptr) + baseaddr;
		  is_address_set = true;
		  line_ptr += address_size;
		  break;
		case DW_LNE_define_file:
		  cur_file = read_string (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  if ((files.num_files % FILE_ALLOC_CHUNK) == 0)
		    {
		      files.files = (struct fileinfo *)
			xrealloc (files.files,
				  (files.num_files + FILE_ALLOC_CHUNK)
				  * sizeof (struct fileinfo));
		      if (files.num_files == 0)
			make_cleanup (free_current_contents, &files.files);
		    }
		  files.files[files.num_files].name = cur_file;
		  files.files[files.num_files].dir =
		    (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  files.files[files.num_files].time =
		    (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  files.files[files.num_files].size =
		    (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  files.num_files++;
		  break;
#ifdef HP_DWARF2_EXTENSIONS
		/* FIXME: this is just a skeletal implementation of the
		   linetable changes to support inlining and optimized code;
		   these changes allow these new opcodes to be recognized so
		   that gdb will not crash. */
		case DW_LNE_HP_negate_is_UV_update:
		  is_UV_update = (!is_UV_update);
		  break;
		case DW_LNE_HP_push_context:
                  context_stack += 1;
		  break;
		case DW_LNE_HP_pop_context:
                  context_stack -= 1;
		  break;
		case DW_LNE_HP_set_file_line_column:
		  file = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  line = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  column = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_set_routine_name:
		  current_line_idx = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_set_sequence:
		  (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_negate_post_semantics:
		  post_semantics = (!post_semantics);
		  break;

		/* The DW_LNE_HP_negate_function_exit opcode should not
		   appear in the logical line table, so it is perhaps
		   misleading to have this code here.
		 */
		case DW_LNE_HP_negate_function_exit:
		  function_exit = (!function_exit);
		  break;
		case DW_LNE_HP_negate_front_end_logical:
		  is_front_end_logical = (!is_front_end_logical);
		  break;
		case DW_LNE_HP_define_proc:
		  proc_name = read_string (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
            
#ifdef INLINE_SUPPORT
		  if (inline_debugging)
		    {
		      insert_name_into_inline_map (proc_name);
		    }
#endif
		  break;

                /* Compilers use this op to provide inline context 
                   instead of push/pop context - Diwakar 07/27 */
		case DW_LNE_HP_set_context_ref:
		  context_ref = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
                  if (prev_line_context_ref != context_ref)
                    prev_context_ref = prev_line_context_ref;
		  line_ptr += bytes_read;
		  break;
#endif /* HP_DWARF2_EXTENSIONS */
		default:
		  complain (&dwarf2_mangled_line_number_section);
		  goto done;
		}
	      break;
	    case DW_LNS_copy:
              /* JAGae39163 - Modify gdb to compensate for gcc's
               * different line tables.  An enhancement request has
               * been filed against gcc - JAGae38927.
               * The difference is that gcc generates multiple rows 
               * that are flagged as statements.  Change all duplicate 
               * rows to be flagged as not statements. */
              if (processing_gcc_compilation) {
                 if (line == prev_line) {
                    is_stmt = 0;
                 } else {
                    prev_line = line;
                    is_stmt = 1;
                 }
              }

#ifdef INLINE_SUPPORT
                  /* - If we see a new context_ref that is greater than the previously
                       known context_ref, we have entered into new Inline Instance.
                     - If we see a new context_ref that is less than the prev
                       known context_ref, we just finished an Inline Instance
                       and got back to the caller. Otherwise, we are in the
                       same context, so leave the inline_idx as is. */
                  if (inline_debugging && current_line_idx > 0)
                    {
                      if (   context_ref > prev_context_ref
                          && context_ref != prev_line_context_ref)
                        {
                          /* New Inline Instance */
                          inline_idx = insert_name_in_inline_table (
                                get_inline_map_entry (current_line_idx -1));
                        }
		      else if (   context_ref < prev_context_ref 
                               && context_ref != prev_line_context_ref)
                        {
                           /*
			     Set inline_idx to caller (previous line's
			     context).
			     Consider the following case :-
                             Functions B and C inlined inside function  A.
                             A() 
                             {
			   
                               6 instructions from A.
                               4 instructions from B.
                               4 instructions from A.
                               6 instructions from C
                               6 instructions from B
                               ...
                             }                             
			     Now initially the context_ref will be 0.
			     After 6 instructions, the context_ref will be set
			     to the index in the logical table where  inlining
			     of B  begins in A. Say this is x.  Now after 4
			     instructions, the context ref will again be set to
			     0 indicating that the  address corresponds to
			     instructions from A.  Now after 4 more
			     instructions,the context ref will again point to
			     the index in the logical table, where the inlining
			     of C begins.  Say this is y.  After 6 more
			     instructions the context ref will again be set
			     back to x (because from this address again
			     instructions from function B begin).  Now since
			     x < y ,this part of the code is entered.  Here the
			     context_ref_idx will be 0 because A() is a 
			     noninlined function.  So in this case a new inline
			     entry should be added and inline_idx should be set
			     appropriately.  Also the high_pc value for the
			     inline entry corresponding to prev_inline_idx
			     should be set to address.
                            
			     Now suppose in the above scenario, A is inlined
			     inside another function D.
                             D() 
                             {
                                4 instructions from D
                                6 instructions from A.
                                4 instructions from B.
                                4 instructions from A.
                                6 instructions from C
                                6 instructions from B
                                ...
                             }  
                             
			     Now initially the context_ref will be 0.  After 4
			     instructions, the context_ref will be set to the
			     index in the logical table where inlining of A
			     begins in D.  Say this is x.  Now after 6
			     instructions, the context ref will again be set to
			     the index in the logical table where inlining of B
			     begins in A.  Let this be y.  Now after 4 more
			     instructions,the context ref will again be set to
			     x.  After 4 more instructions the context ref will
			     be set to index in the logical table where the
			     inlining of C begins in A.  Let this be z.  Now
			     after 6 more instructions,  the context_ref will be
			     set back to y.  At this point the context_ref_idx
			     will be greater than zero and will point to the
			     entry in the inlinee_table corresponding to the
			     point where C is inline into A.  The context_ref
			     now got will be y and this should be compared
			     against the context_ref for the context_ref_idx
			     and if matching only then inline_idx should be set
			     to context_ref_idx.  Otherwise a new inline entry
			     should be started. 
                                                          
			     Also the high_pc value for the inline entry
			     corresponding to prev_inline_idx should be set to
			     address.
                            */

			if (context_ref_idx && (context_ref ==
			    get_actual_context_ref (context_ref_idx)))
			  {
			    /* We've reached the caller now.  Set inline_idx
			       to caller's idx (previous line's context). */
                          inline_idx =  context_ref_idx;
			  /* Make sure the current line's context ref matches the
                             context ref of the inline_idx we have till now. If not
			     pop to the caller's context till we find one. */
			  while (   inline_idx
				 && (find_context_ref_idx (main_subfile,
					 context_ref, context_ref_base)
                                     != get_inline_contextref_idx (inline_idx)))
			    {
			      /* Finished with this inline instance. pop to the caller. */
			      add_pc_to_idx (inline_idx,
				ADJUST_IA64_SLOT_ENCODING (address), 0);
			      inline_idx = get_inline_contextref_idx (inline_idx);
			    }
			  } else {
				/* We got a split inlined instance.  Add this 
				   split part to the inline table. */
				inline_idx = insert_name_in_inline_table(
						get_inline_map_entry(
						current_line_idx -1));
				add_pc_to_idx (prev_inline_idx,
				ADJUST_IA64_SLOT_ENCODING (address), 0);
			  }
                      }
                    }
                  else
                    inline_idx = 0;
                  prev_line_context_ref = context_ref;
#endif

	      if (   (current_subfile != main_subfile)
		  && (   inline_debugging
		      || (!inline_debugging && !context_ref)))
	        record_line_elim (main_subfile, line,
			          ADJUST_IA64_SLOT_ENCODING (address), 
				  is_address_set, column, FALSE, context_ref, 
                                  0, inline_idx);
	      if (inline_idx > prev_inline_idx)
		{
		  /* We just started a new inlined instance.
                     Save the start address. */
#ifdef HP_IA64
		  add_pc_to_idx (inline_idx,
                                 ADJUST_IA64_SLOT_ENCODING (address), 1);
#endif
		}

	      if (inline_idx < prev_inline_idx)
		{
		  /* We just moved from callee to caller. Save the address
		     as end of previous inlined instance. Here, the assumption
                     is that the first line of the caller after the inlined
                     instance ends has the end address of the inlined instance.
                     This is true on HPUX IPF. */
#ifdef HP_IA64
		  add_pc_to_idx (prev_inline_idx,
                                 ADJUST_IA64_SLOT_ENCODING (address), 0);
#endif
		}
	      prev_inline_idx = inline_idx;

              if (inline_debugging && context_ref)
		{
                  context_ref_pc = find_context_ref_pc (main_subfile,
					 context_ref, context_ref_base);
                  context_ref_line = find_context_ref_line (main_subfile,
					 context_ref, context_ref_base);
                  context_ref_idx = find_context_ref_idx (main_subfile,
					 context_ref, context_ref_base);
		  context_ref_file = main_subfile->name;
#if defined HP_IA64
                  context_ref_column =  find_context_ref_column( main_subfile,
                                        context_ref, context_ref_base);
                  /* If we have already read in the minimal table,
                     then dont issue the cannot insert message
                  */
                  if (!dwarf2_read_minimal)
		    if (!add_refs_to_idx (inline_idx, context_ref_pc,
		   		          context_ref_line, context_ref_file,
					  context_ref_idx, context_ref_column,
					  context_ref))
		      warning ("Cannot insert inlined instance\n");
#endif
	        }
              else
		{
                  context_ref_pc = 0;
		  context_ref_idx = 0;
		}

	      /* Record all lines from the main subfile, so that the
	      actuals stay in sync with the logicals.*/

	      if (   inline_debugging
                      || (!inline_debugging && !context_ref))
	        record_line_elim (current_subfile, line,
			          ADJUST_IA64_SLOT_ENCODING (address), 
				  is_address_set, column,
				  is_stmt,
                                  context_ref,
                                  ADJUST_IA64_SLOT_ENCODING(context_ref_pc),
                                  inline_idx);
	      is_address_set = false;
	      break;
	    case DW_LNS_advance_pc:
	      address += lh.minimum_instruction_length
		* read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_advance_line:
	      line += read_signed_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_set_file:
	      /* The file and directory tables are 0 based, the references
	         are 1 based.  */
	      file = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
              if (file > files.num_files)
                  break; /* temporary fix JAGaf00352 till we resolve dwarf issue */
	      dwarf2_start_subfile
		(files.files[file - 1].name,
		 (files.files[file - 1].dir
		  ? dirs.dirs[files.files[file - 1].dir - 1]
		  : comp_dir));
	      break;
	    case DW_LNS_set_column:
	      column = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_negate_stmt:
	      is_stmt = (!is_stmt);
	      break;
	    case DW_LNS_set_basic_block:
	      break;
	    /* Add to the address register of the state machine the
	       address increment value corresponding to special opcode
	       255.  Ie, this value is scaled by the minimum instruction
	       length since special opcode 255 would have scaled the
	       the increment.  */
	    case DW_LNS_const_add_pc:
	      address += (lh.minimum_instruction_length
			  * ((255 - lh.opcode_base) / lh.line_range));
	      break;
	    case DW_LNS_fixed_advance_pc:
	      address += read_2_bytes (abfd, line_ptr);
	      line_ptr += 2;
	      break;
            /* I have added the following to dwarf2.h so that is follows
               the DWARF2 standard, but more investigation is required 
               to properly implement them here. - JAGae17215 */
            case DW_LNS_set_prologue_end:
            case DW_LNS_set_epilogue_begin:
            case DW_LNS_set_isa:
	    default:		/* special operand */
	      adj_opcode = op_code - lh.opcode_base;
	      address += (adj_opcode / lh.line_range)
		* lh.minimum_instruction_length;
	      line += lh.line_base + (adj_opcode % lh.line_range);
              /* JAGae39163 - Modify gdb to compensate for gcc's
               * different line tables.  An enhancement request has
               * been filed against gcc - JAGae38927.
               * The difference is that gcc generates multiple rows 
               * that are flagged as statements.  Change all duplicate 
               * rows to be flagged as not statements. */
              if (processing_gcc_compilation) {
                 if (line == prev_line) {
                    is_stmt = 0;
                 } else {
                    prev_line = line;
                    is_stmt = 1;
                 }
              }

#ifdef INLINE_SUPPORT
                  if (inline_debugging && current_line_idx > 0)
                    {
                      if (   context_ref > prev_context_ref
                          && context_ref != prev_line_context_ref)
                        {
                          inline_idx = insert_name_in_inline_table (
                                get_inline_map_entry (current_line_idx -1));
                        }
                      else if (   context_ref < prev_context_ref
                               && context_ref != prev_line_context_ref)
			{
                          if (context_ref_idx &&
			      (context_ref == get_actual_context_ref(
							context_ref_idx)))
			    {

			      /* We've reached the caller now.  Set
				 inline_idx to caller's idx */

                          inline_idx = context_ref_idx;
			  /* Make sure the current line's context ref matches the
                             context ref of the inline_idx we have till now. If not
                             pop to the caller's context till we find one. */
			  while (   inline_idx
                                 && (find_context_ref_idx (main_subfile,
                                         context_ref, context_ref_base)
                                     != get_inline_contextref_idx (inline_idx)))
                            {
                              add_pc_to_idx (inline_idx,
                                ADJUST_IA64_SLOT_ENCODING (address), 0);
                              inline_idx = get_inline_contextref_idx (inline_idx);
                            }
			} else {
				inline_idx = insert_name_in_inline_table (
					get_inline_map_entry (
					current_line_idx -1));
				add_pc_to_idx (prev_inline_idx,
				ADJUST_IA64_SLOT_ENCODING (address), 0);
			}
		      }
                    }
                  else
                    inline_idx = 0;
                  prev_line_context_ref = context_ref;
#endif

	      if (   current_subfile != main_subfile
		  && (   inline_debugging
                      || (!inline_debugging && !context_ref)))
	        record_line_elim (main_subfile, line,
			          ADJUST_IA64_SLOT_ENCODING (address), 
				  is_address_set, column, FALSE,
                                  context_ref,
                                  0, inline_idx);
	      if (inline_idx > prev_inline_idx)
                {
#ifdef HP_IA64
                  add_pc_to_idx (inline_idx,
                                 ADJUST_IA64_SLOT_ENCODING (address), 1);
#endif
                }

              if (inline_idx < prev_inline_idx)
                {
#ifdef HP_IA64
                  add_pc_to_idx (prev_inline_idx,
                                 ADJUST_IA64_SLOT_ENCODING (address), 0);
#endif
                }
              prev_inline_idx = inline_idx;

              if (inline_debugging && context_ref)
	        {
                  context_ref_pc = find_context_ref_pc (main_subfile,
					 context_ref, context_ref_base);
		  context_ref_line = find_context_ref_line (main_subfile,
                                         context_ref, context_ref_base);
                  context_ref_idx = find_context_ref_idx (main_subfile,
                                         context_ref, context_ref_base);
                  context_ref_file = main_subfile->name;
#if defined HP_IA64
                  context_ref_column = find_context_ref_column( main_subfile,
						context_ref, context_ref_base);
                  if (!dwarf2_read_minimal)
                    if (!add_refs_to_idx (inline_idx, context_ref_pc,
                                          context_ref_line, context_ref_file,
                                          context_ref_idx, context_ref_column,
		   		          context_ref))
                      warning ("Cannot insert inlined instance\n");;
#endif
		}
              else
		{
                  context_ref_pc = 0;
		  context_ref_idx = 0;
		}

	      /* We now record non-statement lines for making
		 sure that logicals and actuals match up. 
	      */
	      if (   inline_debugging
                  || (!inline_debugging && !context_ref))
	      record_line_elim (current_subfile, line,
			        ADJUST_IA64_SLOT_ENCODING (address), 
				is_address_set, column, 
				is_stmt,
                                context_ref,
                                ADJUST_IA64_SLOT_ENCODING(context_ref_pc),
                                inline_idx);
	      is_address_set = false;
	    }
	}
    }

#ifdef HP_IA64
  /* Copy the file and directory table into the global datastructures gfiles
   * and gdirs respectively.
   */
  {
    /* Free the global variables if they were allocated. */
    if (gdirs.dirs && gdirs.num_dirs)
      {
        for (int i = 0; i < gdirs.num_dirs; i++)
          free (gdirs.dirs[i]);
        free (gdirs.dirs);

        gdirs.dirs = NULL;
        gdirs.num_dirs = 0;
      }
    gdirs.num_dirs = dirs.num_dirs;
    gdirs.dirs = (char **) xmalloc (gdirs.num_dirs * sizeof(char *));
    for (int i = 0; i < dirs.num_dirs; i++) 
      {
        int len = strlen (dirs.dirs[i]);
        gdirs.dirs[i] = (char *) xmalloc (len + 1);
        memcpy (gdirs.dirs[i], dirs.dirs[i], (len + 1));
      }
  
    if (gfiles.files && gfiles.num_files)
      {
        for (int i = 0; i < gfiles.num_files; i++)
          free (gfiles.files[i].name);
        free (gfiles.files);

        gfiles.files = NULL;
        gfiles.num_files = 0;
      }
    gfiles.num_files = files.num_files;
    gfiles.files = (struct fileinfo *) xmalloc 
                   (gfiles.num_files * sizeof (struct fileinfo));
    for (int i = 0; i < files.num_files; i++) 
      {
        gfiles.files[i].name = xstrdup (files.files[i].name);
        gfiles.files[i].dir = files.files[i].dir;
	gfiles.files[i].time = files.files[i].time;
	gfiles.files[i].size = files.files[i].size;
      }
  }
#endif

#if defined (INLINE_SUPPORT) && defined (HP_IA64)
    if (inline_debugging)
      free_inline_map();
#endif

#ifdef HP_DWARF2_EXTENSIONS 
  if (context_stack > 0)
    complain (&dwarf2_mangled_line_number_section);
#endif
done:
  do_cleanups (back_to);
}

/* Decode the actual line table information for the compilation unit whose
   actual line table info is at OFFSET in the .debug_actual section.
   The compilation directory of the file is passed in COMP_DIR.  */

static void
dwarf_decode_lines_actual (unsigned int offset, char *comp_dir, bfd *abfd, int file_opt_level)
{
  char *line_ptr;
  char *line_end;
  struct line_head lh;
  struct cleanup *back_to;
  unsigned int i, bytes_read;
  unsigned char op_code, extended_op, adj_opcode;
#ifdef HP_DWARF2_EXTENSIONS
  int context_stack = 0;  /* use counter until implemented */
#endif

#define FILE_ALLOC_CHUNK 5
#define DIR_ALLOC_CHUNK 5

  if (dwarf_line_buffer_actual == NULL)
    {
      complain (&dwarf2_missing_line_number_section_actual);
      return;
    }

#ifndef USING_MMAP
    line_ptr = dwarf_line_buffer_actual + offset;
#else
    /* dwarf_line_buffer now points to the buffer for this .o. Don't
     need to add any offset to it */
  if (!processing_doom_objfile)
    line_ptr = dwarf_line_buffer_actual;
  else
    line_ptr = dwarf_line_buffer_actual + offset;
#endif
  
  /* read in the prologue */
  lh.total_length = read_4_bytes (abfd, line_ptr);
  if (!lh.total_length)
    return;
  line_ptr += 4;
  line_end = line_ptr + lh.total_length;
  lh.version = read_2_bytes (abfd, line_ptr);
  line_ptr += 2;
  lh.prologue_length = read_4_bytes (abfd, line_ptr);
  line_ptr += 4;
  lh.minimum_instruction_length = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.default_is_stmt = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.line_base = read_1_signed_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.line_range = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.opcode_base = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.standard_opcode_lengths = (unsigned char *)
    xmalloc (lh.opcode_base * sizeof (unsigned char));
  back_to = make_cleanup (free_current_contents, &lh.standard_opcode_lengths);

  lh.standard_opcode_lengths[0] = 1;
  for (i = 1; i < lh.opcode_base; ++i)
    {
      lh.standard_opcode_lengths[i] = read_1_byte (abfd, line_ptr);
      line_ptr += 1;
    }
  /* Read the statement sequences until there's nothing left.  */
  while (line_ptr < line_end)
    {
      /* state machine registers  */
      CORE_ADDR address = 0;
      unsigned int line = 0;
      int is_stmt = lh.default_is_stmt;
      int end_sequence = 0;
      int function_exit = 0;
#ifdef HP_DWARF2_EXTENSIONS
      int is_UV_update = 0;
      int post_semantics = 0;
      int is_front_end_logical = 0; 
#endif

      /* Decode the table. */
      while (!end_sequence)
	{
	  op_code = read_1_byte (abfd, line_ptr);
	  line_ptr += 1;
	  switch (op_code)
	    {
	    case DW_LNS_extended_op:
	      (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      extended_op = read_1_byte (abfd, line_ptr);
	      line_ptr += 1;
	      switch (extended_op)
		{
		case DW_LNE_end_sequence:
		  end_sequence = 1;
		  break;
		case DW_LNE_set_address:
		  address = read_address (abfd, line_ptr) + baseaddr;
		  line_ptr += address_size;
		  break;
#ifdef HP_DWARF2_EXTENSIONS
		/* FIXME: this is just a skeletal implementation of the
		   linetable changes to support inlining and optimized code;
		   these changes allow these new opcodes to be recognized so
		   that gdb will not crash. We could do more by reading
		   in and interpreting them. */
		case DW_LNE_HP_negate_is_UV_update:
		  is_UV_update = (!is_UV_update);
		  break;
		case DW_LNE_HP_push_context:
                  context_stack += 1;
		  break;
		case DW_LNE_HP_pop_context:
                  context_stack -= 1;
		  break;
		case DW_LNE_HP_set_routine_name:
		  (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_set_sequence:
		  (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_negate_post_semantics:
		  post_semantics = (!post_semantics);
		  break;
		case DW_LNE_HP_negate_function_exit:
		  function_exit = (!function_exit);
		  break;
		case DW_LNE_HP_negate_front_end_logical:
		  is_front_end_logical = (!is_front_end_logical);
		  break;
		case DW_LNE_HP_define_proc:
		  (void) read_string (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
#endif /* HP_DWARF2_EXTENSIONS */
		default:
		  complain (&dwarf2_mangled_line_number_section);
		  goto done;
		}
	      break;
	    case DW_LNS_copy:
#ifdef HP_DWARF2_EXTENSIONS 
	      if (is_stmt && context_stack == 0)
#endif
		record_doc_line (current_subfile, line-1,
			     0,0,ADJUST_IA64_SLOT_ENCODING (address), 0, 0,
			     function_exit, file_opt_level);
	      break;
	    case DW_LNS_advance_pc:
	      address += lh.minimum_instruction_length
		* read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_advance_line:
	      line += read_signed_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_negate_stmt:
	      is_stmt = (!is_stmt);
	      break;
	    case DW_LNS_set_basic_block:
	      break;
	    case DW_LNS_const_add_pc:
	      address += (lh.minimum_instruction_length
			  * ((255 - lh.opcode_base) / lh.line_range));
	      break;
	    case DW_LNS_fixed_advance_pc:
	      address += read_2_bytes (abfd, line_ptr);
	      line_ptr += 2;
	      break;
	    default:		/* special operand */
	      adj_opcode = op_code - lh.opcode_base;
	      address += (adj_opcode / lh.line_range)
		* lh.minimum_instruction_length;
	      line += lh.line_base + (adj_opcode % lh.line_range);
	      /* append row to matrix using current values */
#ifdef HP_DWARF2_EXTENSIONS 
	      if (is_stmt && context_stack == 0)
#endif
	      /* indexing differences between logicals and actuals cause
		 a difference of 1*/
	      record_doc_line (current_subfile, line-1,
			   0,0,ADJUST_IA64_SLOT_ENCODING (address), 0, 0,
			   function_exit, file_opt_level);
	      function_exit = 0;
	    }
	}
    }
done:
  do_cleanups (back_to);
}

/* Start a subfile for DWARF.  FILENAME is the name of the file and
   DIRNAME the name of the source directory which contains FILENAME
   or NULL if not known.
   This routine tries to keep line numbers from identical absolute and
   relative file names in a common subfile.

   Using the `list' example from the GDB testsuite, which resides in
   /srcdir and compiling it with Irix6.2 cc in /compdir using a filename
   of /srcdir/list0.c yields the following debugging information for list0.c:

   DW_AT_name:          /srcdir/list0.c
   DW_AT_comp_dir:              /compdir
   files.files[0].name: list0.h
   files.files[0].dir:  /srcdir
   files.files[1].name: list0.c
   files.files[1].dir:  /srcdir

   The line number information for list0.c has to end up in a single
   subfile, so that `break /srcdir/list0.c:1' works as expected.  */

static void
dwarf2_start_subfile (char *filename, char *dirname)
{
  /* If the filename isn't absolute, try to match an existing subfile
     with the full pathname.  */

  if (*filename != '/' && dirname != NULL)
    {
      struct subfile *subfile;
      char *fullname = concat (dirname, "/", filename, NULL);

      for (subfile = subfiles; subfile; subfile = subfile->next)
	{
	  if (STREQ (subfile->name, fullname))
	    {
	      current_subfile = subfile;
	      free (fullname);
	      return;
	    }
	}
      free (fullname);
    }
  start_subfile (filename, dirname);
}

/* get the name of the original file which generated this comp unit */
static struct subfile *
get_main_subfile ()
{
      struct subfile *subfile;
      for (subfile = subfiles; subfile->next != 0; subfile = subfile->next);
      return subfile;
}

/* decode location expression or list into the corresponding symbol 
   and value, if any. */
static void
var_decode_location (struct attribute *attr, struct symbol *sym,
		     struct objfile *objfile,
		     const struct comp_unit_head *cu_header)
{

  /* A DW_AT_location attribute with no contents indicates that a
     variable has been optimized away.  */
  if (attr_form_is_block (attr) && DW_BLOCK (attr)->size == 0)
    {
      SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
      return;
    }

  /* Handle location expressions that aren't location lists here.  The FSF
     version defers everything to LOC_COMPUTED.  */

  if (!(attr->form == DW_FORM_data4 || attr->form == DW_FORM_data8))
    {
      unsigned int locdesc_offset;
      SYMBOL_VALUE(sym) = decode_locdesc (sym, attr,
      					  objfile, &locdesc_offset);
      return;
    }

  /* NOTE drow/2002-01-30: It might be worthwhile to have a static
     expression evaluator, and use LOC_COMPUTED only when necessary
     (i.e. when the value of a register or memory location is
     referenced, or a thread-local block, etc.).  Then again, it might
     not be worthwhile.  I'm assuming that it isn't unless performance
     or memory numbers show me otherwise.  */

  dwarf2_symbol_mark_computed (attr, sym, cu_header, objfile);
  SYMBOL_CLASS (sym) = LOC_COMPUTED;
}

/* Makes a copy of the symbol and gives it a new name */

struct symbol *
make_copy_of_symbol (const char *name, struct symbol *sym)
{
  struct symbol *symbol = (struct symbol *) xmalloc (sizeof (struct symbol));
  memcpy (symbol, sym, sizeof (struct symbol));
  
  /* Where do I free this - can't allocate on the obstack since I
     don't have objfile here (it isn't passed into lookup_symbol) */

  SYMBOL_NAME (symbol) = strdup(name);
  SYMBOL_VAR_OFFSET(symbol) = 0;
  if (SYMBOL_LANGUAGE (symbol) == language_cplus)
    SYMBOL_CPLUS_DEMANGLED_NAME (symbol) = SYMBOL_CPLUS_DEMANGLED_NAME (sym);
  return symbol;
}



/* Given a pointer to a DWARF information entry, figure out if we need
   to make a symbol table entry for it, and if so, create a new entry
   and return a pointer to it.
   If TYPE is NULL, determine symbol type from the die, otherwise
   used the passed type.  */

static struct symbol *
new_symbol (struct die_info *die, struct type *type, struct objfile *objfile,
            const struct comp_unit_head *cu_header)
{
  struct symbol *sym = NULL;
  char *name;
  struct attribute *attr = NULL;
  struct attribute *attr2 = NULL;
  CORE_ADDR addr;
  unsigned int locdesc_offset;

  name = dwarf2_linkage_name (die);
  if (name || global_var_name || import_var_name)
    {
      sym = (struct symbol *) (void *) obstack_alloc (&objfile->symbol_obstack,
					     sizeof (struct symbol));
      OBJSTAT (objfile, n_syms++);
      memset (sym, 0, sizeof (struct symbol));
      /* Poorva: If name is null then we need to make sure that we 
	 pick import_var_name before global_var_name since the
	 global one is used for namespaces and the import one
	 is for imported declarations inside the namespace */

      if (name != NULL)
	SYMBOL_NAME (sym) = obsavestring (name, (int) strlen (name),
					  &objfile->symbol_obstack);
      else if (import_var_name)
	SYMBOL_NAME (sym) = obsavestring (import_var_name, 
					  (int) strlen (import_var_name),
					  &objfile->symbol_obstack);
      else
	SYMBOL_NAME (sym) = obsavestring (global_var_name, 
					  (int) strlen (global_var_name),
					  &objfile->symbol_obstack);
	
      /* Default assumptions.
         Use the passed type or decode it from the die.  */
      SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
      SYMBOL_CLASS (sym) = LOC_STATIC;
#ifdef HP_DWARF2_EXTENSIONS
      /* 01/22/02 coulter
	 The compilers recently started doing at +O1 what they were doing
	 at +O0 - e.g. variables are always available for modification.
	 They are marked as HP_selectively_unmodifiable and the list of
	 places where they are unmodifiable is empty.
      */
      /* Setting the default modifiability to "unmodifiable" 
         At BL4: For executables compiled with "-g" [== "-g +O1"]
         1] Globals are supposed to be un-modifiable; no modifiability
         information expected.
         FIXME: if you combine .o's compiled with  "-g +O0" 
         with .o's compiled with "-g +O1" then strange situation
         may result for global. All the globals will be marked
         unmodifiable by default but they should not be if all
         the defs and uses of the globals are in the .o's compiled
         with "-g -O0".
         2] Locals are supposed to be modifiable everywhere unless
         modifiability ranges are present in which case the locals
         are modifiable only in those ranges.
       */
      SYMBOL_MODIFIABILITY (sym) = HP_selectively_unmodifiable;
      SYMBOL_MOD_UNMOD_RANGES (sym) = NULL;
#endif /* HP_DWARF2_EXTENSIONS */
      if (type != NULL)
	SYMBOL_TYPE (sym) = type;
      else
	SYMBOL_TYPE (sym) = die_type (die, objfile, cu_header);
      attr = dwarf_attr (die, DW_AT_decl_line);
      if (attr)
	{
	  SYMBOL_LINE (sym) = DW_UNSND (attr);
	}

      /* If this symbol is from a C++ compilation, then attempt to
         cache the demangled form for future reference.  This is a
         typical time versus space tradeoff, that was decided in favor
         of time because it sped up C++ symbol lookups by a factor of
         about 20. */

      SYMBOL_LANGUAGE (sym) = cu_language;
      SYMBOL_INIT_DEMANGLED_NAME (sym, &objfile->symbol_obstack);
      switch (die->tag)
	{
	case DW_TAG_label:
	  attr = dwarf_attr (die, DW_AT_low_pc);
	  if (attr)
	    {
	      SYMBOL_VALUE_ADDRESS (sym) = DW_ADDR (attr) + baseaddr;
	    }
	  SYMBOL_CLASS (sym) = LOC_LABEL;
	  break;
	case DW_TAG_subprogram:
	  /* SYMBOL_BLOCK_VALUE (sym) will be filled in later by
	     finish_block.  */
	  SYMBOL_CLASS (sym) = LOC_BLOCK;
	  if (SYMBOL_LANGUAGE (sym) == language_fortran)
	    {
	      int i;
	      for (i = 0; i < strlen (SYMBOL_NAME (sym)); i++)
		(SYMBOL_NAME (sym))[i] = tolower ((SYMBOL_NAME (sym))[i]);
              attr = dwarf_attr (die, DW_AT_calling_convention);
              if (attr && DW_UNSND (attr)  == DW_CC_program)
                SYMBOL_FORTRAN_DEMANGLED_NAME(sym) = fortran_main_string;
	    }

	  attr2 = dwarf_attr (die, DW_AT_external);
	  if (attr2 && (DW_UNSND (attr2) != 0))
	    {
	      add_symbol_to_list (sym, &global_symbols);
	    }
	  else
	    {
	      add_symbol_to_list (sym, list_in_scope);
	    }
	  break;
	case DW_TAG_imported_module:
	  /* SYMBOL_BLOCK_VALUE (sym) will be filled in later by
	     finish_block.  */
	  SYMBOL_CLASS (sym) = LOC_BLOCK;
	  break;
        case DW_TAG_entry_point:
	  /* Create a new symbol out of it for C++ too */
          if ((cu_language == language_fortran) 
	      || (cu_language == language_cplus)) {
          /* SYMBOL_BLOCK_VALUE (sym) will be filled in later by
             finish_block.  */
          SYMBOL_CLASS (sym) = LOC_BLOCK;
	  if (SYMBOL_LANGUAGE (sym) == language_fortran)
	    {
	      int i;
  	      SYMBOL_TYPE(sym)->flags |= TYPE_FLAG_ENTRY_POINT;
	      for (i = 0; i < strlen (SYMBOL_NAME (sym)); i++)
		(SYMBOL_NAME (sym))[i] = tolower ((SYMBOL_NAME (sym))[i]);
	    }
          attr2 = dwarf_attr (die, DW_AT_external);
          if (attr2 && (DW_UNSND (attr2) != 0))
            {
              add_symbol_to_list (sym, &global_symbols);
            }
          else
            {
              add_symbol_to_list (sym, list_in_scope);
            }
          }
          break;
	case DW_TAG_variable:
	  /* Compilation with minimal debug info may result in variables
	     with missing type entries. Change the misleading `void' type
	     to something sensible.  */
	  if (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_VOID)
	    SYMBOL_TYPE (sym) = init_type (TYPE_CODE_INT,
					   TARGET_INT_BIT / HOST_CHAR_BIT, 0,
					   "<variable, no debug info>",
					   objfile);
	  attr = dwarf_attr (die, DW_AT_const_value);
	  if (attr)
	    {
	      dwarf2_const_value (attr, sym, objfile);
	      if (SYMBOL_VALUE (sym) == 0 && SYMBOL_VALUE_BYTES (sym) == 0)
		SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
	      attr2 = dwarf_attr (die, DW_AT_external);
	      if (attr2 && (DW_UNSND (attr2) != 0))
		add_symbol_to_list (sym, &global_symbols);
	      else
		add_symbol_to_list (sym, list_in_scope);
	      break;
	    }
	  attr = dwarf_attr (die, DW_AT_location);
	  if (attr)
	    {
#ifdef HP_IA64
	      no_of_mod_ranges = 0;
#endif
	      var_decode_location (attr, sym, objfile, cu_header);
	      if ((SYMBOL_CLASS (sym) == LOC_COMPUTED) ||
	      	  (SYMBOL_CLASS (sym) == LOC_COMPUTED_ARG))
		{
		  /* If this symbol is a DWARF location list, don't
		     try to evaluate its address now, defer to later.
		     For now, assume it is a local scope - we'll see
		     location lists for register-promoted globals later. */
		  add_symbol_to_list (sym, list_in_scope);
		  break;
		}
	      attr2 = dwarf_attr (die, DW_AT_external);
	      if (attr2 && (DW_UNSND (attr2) != 0))
		{
		  SYMBOL_VALUE_ADDRESS (sym) =
		    decode_locdesc (sym, attr, objfile, &locdesc_offset);

#ifdef HP_DWARF2_EXTENSIONS
		  if (!istls && !offreg && SYMBOL_VALUE_ADDRESS (sym) == 0)
		    {
		      SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
		      add_symbol_to_list (sym, &global_symbols);
		      break;
		    }
		  else
		    SYMBOL_CLASS (sym) = LOC_STATIC;
		  add_symbol_to_list (sym, &global_symbols);
		  /* In shared libraries the address of the variable
		     in the location descriptor might still be relocatable,
		     so its value could be zero.
		     Enter the symbol as a LOC_UNRESOLVED symbol, if its
		     value is zero, the address of the variable will then
		     be determined from the minimal symbol table whenever
		     the variable is referenced.  */
                  /* srikanth, for globals in shared libraries, the address
                     in the debug information is not the address of the
                     variable, but is typically the address of the dlt slot.
                     This needs to be dereferenced to access the real
                     variable. Take a short cut and pick up the address from
                     the linker symbol table.
                  */
		  /* Bindu: Don't do all the above if the variable is a 
		     TLS variable. 
		  */
		  if (!istls) 
		    {
#endif /* HP_DWARF2_EXTENSIONS */
		      if (SYMBOL_VALUE_ADDRESS (sym))
		        {
                          struct minimal_symbol * m = 0;

                          m = lookup_minimal_symbol (SYMBOL_NAME(sym),0,objfile);
                          if (m)
          		    SYMBOL_VALUE_ADDRESS (sym) = UNSWIZZLE (SYMBOL_VALUE_ADDRESS (m));
                          else
		            SYMBOL_VALUE_ADDRESS (sym) += data_baseaddr;
		          SYMBOL_CLASS (sym) = LOC_STATIC;
		        }
		      else
		        SYMBOL_CLASS (sym) = LOC_UNRESOLVED;
#ifdef HP_DWARF2_EXTENSIONS
		      if (no_of_mod_ranges)
		        set_modifiability_information (sym);
#endif /* HP_DWARF2_EXTENSIONS */
#ifndef HP_IA64
		      /* Record this info onto the GLOBAL_BLOCK. */	
		      add_symbol_to_list (sym, &global_symbols); 	
#endif
#ifdef HP_IA64
		    }
#if !defined(KGDB) || defined(HOST_IA64)
		  else
		    /* Bindu 112801: For dtls, tls_start_addr is -1.
		     * Save the tls_index in SYMBOL_VAR_OFFSET.
		     * checkout the "IA64 Runtime Architecture Supplement:
		     *                Thread Local Storage"
		     */
                    {
		      if (((struct load_module_desc*) ((obj_private_data_t *)
			   objfile->obj_private)->lmdp)->tls_start_addr !=
			   (CORE_ADDR) (long) -1)
			{
		      	  SYMBOL_VALUE_ADDRESS (sym) += 
		           ((struct load_module_desc*) ((obj_private_data_t *)
			     objfile->obj_private)->lmdp)->tls_start_addr;
                          SYMBOL_CLASS (sym) = LOC_THREAD_LOCAL_STATIC;
			} 
		      else
			{
			  SYMBOL_VAR_OFFSET (sym) =
			    (int)((struct load_module_desc*) ((obj_private_data_t *)
			      objfile->obj_private)->lmdp)->tls_index;
			  SYMBOL_CLASS (sym) = LOC_THREAD_LOCAL_DYNAMIC;
			}
  	              SYMBOL_BASEREG (sym) = TP_REGNUM;
  		      istls = 0;
                    }
#endif /* !defined(KGDB) || defined(HOST_IA64) */
#endif
		}
	      else
		{
		  addr =
		    decode_locdesc (sym, attr, objfile, &locdesc_offset);
		  SYMBOL_VALUE (sym) = addr;

		  /* if opcode = DW_OP_fbreg, the variable is local to the
		     function, thus SYMBOL_VALUE (sym) may equal zero and
		     the variable has not been optimized out. - JAGae19212 */
#ifdef HP_DWARF2_EXTENSIONS
		  if (   !optimized_out
		      && !istls
		      && !offreg
		      && SYMBOL_VALUE (sym) == 0
		      && *DW_BLOCK(attr)->data != ((signed char) DW_OP_fbreg)
		      && !ispiece)
		    {
		      SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
		      add_symbol_to_list (sym, list_in_scope);
		      break;
		    }
#endif
		  add_symbol_to_list (sym, list_in_scope);
#ifdef HP_DWARF2_EXTENSIONS
		  /* Locals are supposed to be modifiable by default */
		  SYMBOL_MODIFIABILITY (sym) = ((func_opt_level <= 1) 
		    ? HP_selectively_unmodifiable 
		    : HP_selectively_modifiable);
#endif /* HP_DWARF2_EXTENSIONS */
		  if (optimized_out || 
#ifdef HP_DWARF2_EXTENSIONS
		      func_opt_level > 0 && 
#endif /* HP_DWARF2_EXTENSIONS */
		      !offreg &&
		      addr == 0)
		    {
		      SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
		    }
		  else if (isreg)
		    {
		      SYMBOL_CLASS (sym) = LOC_REGISTER;
		      if (!ispiece)
			SYMBOL_VALUE (sym) =
			  ADJUST_IA64_REGNUM (SYMBOL_VALUE (sym));
		    }
		  else if (offreg)
		    {
#ifdef HP_IA64_NATIVE_DEV5
		      /* RM: Hack: the debug information tells us that
		       *  locals are offset from SP, when they are
		       *  actually offset from the frame pointer.
		       * MC: This bug was fixed in DEV6.
		       */
		      if (basereg == 12)
			{
			  SYMBOL_CLASS (sym) = LOC_LOCAL;
			}
		      else
#endif
			{
			  SYMBOL_CLASS (sym) = LOC_BASEREG;
			  SYMBOL_BASEREG (sym) = ADJUST_IA64_REGNUM (basereg);
			}
		    }
		  else if (islocal)
		    {
		      SYMBOL_CLASS (sym) = LOC_LOCAL;
		    }
#ifdef HP_DWARF2_EXTENSIONS
#if !defined(KGDB) || defined(HOST_IA64)
		  else if(istls)
		    {
		      if (((struct load_module_desc*) ((obj_private_data_t *)
			   objfile->obj_private)->lmdp)->tls_start_addr != 
							(CORE_ADDR) (long)-1)
			{
                          SYMBOL_VALUE_ADDRESS (sym) +=
                             ((struct load_module_desc*) ((obj_private_data_t *)
                               objfile->obj_private)->lmdp)->tls_start_addr;
                          SYMBOL_CLASS (sym) = LOC_THREAD_LOCAL_STATIC;
			}
		      else
			{
			  SYMBOL_VAR_OFFSET (sym) =
			    (int)((struct load_module_desc*) ((obj_private_data_t *)
			      objfile->obj_private)->lmdp)->tls_index;
			  SYMBOL_CLASS (sym) = LOC_THREAD_LOCAL_DYNAMIC;
			}
                      SYMBOL_BASEREG (sym) = TP_REGNUM;
		      istls = 0;
                    }
#endif /* !defined(KGDB) || defined(HOST_IA64) */
#endif /* HP_DWARF2_EXTENSIONS */
		  else
		    {
		      /* section this symbol is allocated in */
		      struct obj_section *section_ptr = find_pc_section (SWIZZLE(addr));

		      SYMBOL_CLASS (sym) = LOC_STATIC;
		      if ((section_ptr != NULL) &&
		          (section_ptr->the_bfd_section->flags & SEC_READONLY))
		        {
			  /* JAGag26515, "incorrect value printed for .rodata
			     array in shared library".  Actually, the important
			     part was that the const array was of local scope,
			     so we ended up here instead of using the minimal
			     symbol table to get its address.  */
			  /* sym is in readonly memory, in the text segment */
		          SYMBOL_VALUE_ADDRESS (sym) = addr + 
			    ANOFFSET (objfile->section_offsets,
			              SECT_OFF_RODATA (objfile));
		        }
		      else
		        {
			  /* sym is in the regular data segment */
		          SYMBOL_VALUE_ADDRESS (sym) = addr + data_baseaddr;
		        }
		    }
#ifdef HP_DWARF2_EXTENSIONS
		  if (no_of_mod_ranges)
		    set_modifiability_information (sym);
#endif /* DWARF2_EXTENSIONS */
		}
	    }
	  else
	    {
	      /* We do not know the address of this symbol.
	         If it is an external symbol and we have type information
	         for it, enter the symbol as a LOC_UNRESOLVED symbol.
	         The address of the variable will then be determined from
	         the minimal symbol table whenever the variable is
	         referenced.  */
	      attr2 = dwarf_attr (die, DW_AT_external);
	      if (attr2 && (DW_UNSND (attr2) != 0)
		  && dwarf_attr (die, DW_AT_type) != NULL)
		{
		  SYMBOL_CLASS (sym) = LOC_UNRESOLVED;
		  add_symbol_to_list (sym, &global_symbols);
		}
	      else
		{
		  /* If we don't know the location of this symbol and it's
		     not an external symbol, consider this as optimized out. */
		  SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
                  add_symbol_to_list (sym, list_in_scope);
		}
	    }
	  break;
	case DW_TAG_formal_parameter:
	  attr = dwarf_attr (die, DW_AT_location);
	  if (attr)
	    {
#ifdef HP_IA64
	      no_of_mod_ranges = 0;
#endif
	      var_decode_location (attr, sym, objfile, cu_header);
	      /* FIXME drow/2003-07-31: Is LOC_COMPUTED_ARG necessary?  */
	      if (SYMBOL_CLASS (sym) == LOC_COMPUTED)
		{
		  SYMBOL_CLASS (sym) = LOC_COMPUTED_ARG;
                }
	      else if (optimized_out)
		{
                  SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
                }
	      else if (
#ifdef HP_DWARF2_EXTENSIONS
   		          !istls &&
#endif
		          !offreg
		       && SYMBOL_VALUE (sym) == 0
		       && *DW_BLOCK(attr)->data != ((signed char) DW_OP_fbreg)
		       && !ispiece)
		{
		  /* if addr is 0, the variable is optimized. */
		  SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
		}
	      else if (isreg)
		{
		  if (isreference)
		    SYMBOL_CLASS (sym) = LOC_REGPARM_ADDR;
		  else
		    SYMBOL_CLASS (sym) = LOC_REGPARM;
		  if (!ispiece)
		    SYMBOL_VALUE (sym) =
		      ADJUST_IA64_REGNUM (SYMBOL_VALUE (sym));
		}
	      else if (offreg)
		{
		  if (isreference)
		    SYMBOL_CLASS (sym) = LOC_BASEREG_REF_ARG;
		  else
		    SYMBOL_CLASS (sym) = LOC_BASEREG_ARG;
		  SYMBOL_BASEREG (sym) = ADJUST_IA64_REGNUM (basereg);
		}
	      else
		{
		  if (isreference)
		    SYMBOL_CLASS (sym) = LOC_REF_ARG;
		  else
		    SYMBOL_CLASS (sym) = LOC_ARG;
		}
#ifdef HP_DWARF2_EXTENSIONS
	      if (no_of_mod_ranges)
		set_modifiability_information (sym);
#endif /* HP_DWARF2_EXTENSIONS */
	    }
	  attr = dwarf_attr (die, DW_AT_const_value);
	  if (attr)
	    {
	      dwarf2_const_value (attr, sym, objfile);
	    }
          sym->is_argument = 1; /* mark it as argument type symbol */
	  add_symbol_to_list (sym, list_in_scope);
	  break;
	case DW_TAG_unspecified_parameters:
	  /* From varargs functions; gdb doesn't seem to have any
	     interest in this information, so just ignore it for now.
	     (FIXME?) */
	  break;
	case DW_TAG_class_type:
	case DW_TAG_structure_type:
	case DW_TAG_union_type:
	case DW_TAG_enumeration_type:
	  SYMBOL_CLASS (sym) = LOC_TYPEDEF;
	  SYMBOL_NAMESPACE (sym) = STRUCT_NAMESPACE;

	  
	   add_symbol_to_list (sym, list_in_scope);

	  /* The semantics of C++ state that "struct foo { ... }" also
	     defines a typedef for "foo". Synthesize a typedef symbol so
	     that "ptype foo" works as expected.  
	     Now that aCC is replacing cc on IA64, we do this for
	     language_c as well.
	     */
	  /* Bindu 112503: We do not want to add a duplicate symbol into
	     VAR_NAMESPACE. This will create problems when we have
	     a symbol with the same name as struct name. "struct foo foo;"
	     When we use lookup_symbol to lookup the variable foo, we
	     end up getting typedef foo symbol.
	     The above semantics of C++ have been handled in parsing phase
	     where if we do not find a symbol in VAR_NAMESPACE, we go ahead
	     and look it up in STRUCT_NAMESPACE. Fix for JAGae97453. */
	  if (cu_language == language_cplus)
	    {

	      if (TYPE_NAME (SYMBOL_TYPE (sym)) == 0)
		{
		  switch (die->tag)
		    {
		    case DW_TAG_class_type:
		      TYPE_DECLARED_TYPE (SYMBOL_TYPE(sym)) = DECLARED_TYPE_CLASS;
		      break;

		    case DW_TAG_structure_type:
#ifdef JAGae34124
		      /* gcc/g++ emits DW_TAG_structure_type for
		         structs/classes and unions. */
		      /* This can be removed when JAGae34124 is fixed by
			 open source g++. */
		      if (processing_gcc_compilation)
		        TYPE_DECLARED_TYPE (SYMBOL_TYPE (sym)) = DECLARED_TYPE_NONE;
		      else
#endif
		        TYPE_DECLARED_TYPE (SYMBOL_TYPE (sym)) = DECLARED_TYPE_STRUCT;
		      break;

		    case DW_TAG_union_type:
		      TYPE_DECLARED_TYPE (SYMBOL_TYPE (sym)) = DECLARED_TYPE_UNION;
		      break;
		    };
		  TYPE_NAME (SYMBOL_TYPE (sym)) = SYMBOL_NAME (sym);
		}
	    }

	  break;
	case DW_TAG_namespace:
	  {
	    char *prefix;
	    char *type_name;
	    struct symbol *typedef_sym = (struct symbol *)
	      (void *) obstack_alloc (&objfile->symbol_obstack,
			     sizeof (struct symbol));
	    
	    SYMBOL_CLASS (sym) = LOC_TYPEDEF;
	    SYMBOL_NAMESPACE (sym) = STRUCT_NAMESPACE;
	    add_symbol_to_list (sym, list_in_scope);
	    
	    /* The semantics of C++ state that "struct foo { ... }" also
	       defines a typedef for "foo". Synthesize a typedef symbol so
	       that "ptype foo" works as expected.  */
	    *typedef_sym = *sym;
	    SYMBOL_NAMESPACE (typedef_sym) = VAR_NAMESPACE;	      
	    if (TYPE_NAME (SYMBOL_TYPE (sym)) == 0)
	      {
		prefix = "namespace ";
		type_name = obstack_alloc (&objfile->type_obstack,
					   strlen (prefix)
					   + strlen (SYMBOL_NAME (sym)) + 1);
		sprintf (type_name, "%s%s%s", prefix, " ", SYMBOL_NAME (sym));
		TYPE_NAME (SYMBOL_TYPE (sym)) = type_name;
	      }
	    add_symbol_to_list (typedef_sym, list_in_scope);
	  }
	  break;
	case DW_TAG_imported_declaration:
	  if (import_var_name)
	    {
	      struct die_info *ref_die;
	      char *ref_die_name = NULL;
	      struct attribute *attr = dwarf_attr (die, DW_AT_import);
	      ref_die = find_die_from_ref (die, objfile, attr);
	      ref_die_name = dwarf2_linkage_name (ref_die);
	      ref_die_name = obsavestring (ref_die_name, (int) strlen (ref_die_name),
					    &objfile->symbol_obstack);
	      if (ref_die->tag == DW_TAG_subprogram)
		{
		  /* extern struct symbol *copy_symbol (const char *, 
						     struct symbol *); */
		  struct symbol *symbol;
		  if (!ref_die->type)
		    {
		      struct attribute* type_attr =
			 dwarf_attr (ref_die, DW_AT_type);
		      if (!type_attr)
			{
			  /* A missing DW_AT_type. Must be a type die. */
			  ref_die->type = tag_type_to_type (ref_die, objfile, cu_header);
			}
		      else
		        ref_die->type = die_type (ref_die, objfile, cu_header);
		    }
		  symbol = ref_die->type->sym;
		  if (symbol)
		    {	
		      sym = make_copy_of_symbol (import_var_name, symbol);
		      SYMBOL_CPLUS_DEMANGLED_NAME (sym) = 
			obsavestring (import_var_name, 
				      (int) strlen (import_var_name), 
				      &objfile->symbol_obstack);
		      SYMBOL_USING_DECL_NAME (sym) = ref_die_name;
		      add_symbol_to_list (sym, list_in_scope);
		    }
		  else
		    {
		      /* This is to work around the problem that a using decl
			 for a function may be seen before the function. The 
			 symbol for the using_decl has to have aclass as 
			 LOC_BLOCK else things in find_n_block_functions etc 
			 will not work. It also needs a block. 
			 Since we have not yet processed the debug info for the
			 function and so do not have a block - we need to 
			 synthesize a dummy block for the using decl. We do 
			 this by getting the lowpc value from the minimal
			 symbol. We also make the highpcof the block =
			 lowpc. This works but if the highpc creates problems
			 then we need to look for the next minsym (sorted by 
			 pc) and get its lowpc which could be used as the 
			 highpc of this block.
			*/

		      char *name = dwarf2_linkage_name (ref_die);
		      struct minimal_symbol *msym = 
			lookup_minimal_symbol (name, NULL, objfile);
		      if (msym)
			{
			  struct block *block = (struct block *) 
			    (void *) obstack_alloc (&objfile->symbol_obstack,
					   (sizeof (struct block)));
			  BLOCK_START (block) = SWIZZLE(SYMBOL_VALUE(msym));
			  BLOCK_END (block) = SWIZZLE(SYMBOL_VALUE(msym));
			  BLOCK_SUPERBLOCK (block) = NULL;
			  BLOCK_ALIASES (block) = NULL;
			  BLOCK_NAMESPACE (block) = NULL;
			  BLOCK_FUNCTION (block) = NULL;
			  BLOCK_GCC_COMPILED (block) = processing_gcc_compilation;
			  BLOCK_NSYMS (block) = 0;
			  SYMBOL_BLOCK_VALUE(sym) = block;
			  SYMBOL_SECTION(sym) = SYMBOL_SECTION(msym);
			  SYMBOL_BFD_SECTION(sym) = SYMBOL_BFD_SECTION(msym);
			  SYMBOL_CLASS(sym) = LOC_BLOCK;
			  SYMBOL_CPLUS_DEMANGLED_NAME (sym) = 
			    obsavestring (import_var_name, 
					  (int) strlen (import_var_name), 
					  &objfile->symbol_obstack);
			  SYMBOL_USING_DECL_NAME (sym) = ref_die_name;
			  add_symbol_to_list (sym, list_in_scope);
			}
		    }
		}
	      else
		{
		  if (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_VOID)
		    SYMBOL_TYPE (sym) = init_type (TYPE_CODE_INT,
						   TARGET_INT_BIT / 
						   HOST_CHAR_BIT,
						   0,
						   "<variable, no debug info>",
						   objfile);
		  SYMBOL_USING_DECL_NAME (sym) = ref_die_name;
		  attr = dwarf_attr (ref_die, DW_AT_const_value);
		  if (attr)
		    {
		      dwarf2_const_value (attr, sym, objfile);
		      add_symbol_to_list (sym, list_in_scope);
		      break;
		    }
		  attr = dwarf_attr (ref_die, DW_AT_location);
		  if (attr)
		    {
		      attr2 = dwarf_attr (ref_die, DW_AT_external);
		      if (attr2 && DW_UNSND (attr2) != 0)
			{
			  SYMBOL_VALUE_ADDRESS (sym) =
			    decode_locdesc (sym, attr, 
					    objfile, &locdesc_offset);
			  add_symbol_to_list (sym, &global_symbols);
			}		
		      else
			{
			  SYMBOL_VALUE (sym) = addr =
			    decode_locdesc (sym, attr, 
					    objfile, 
					    &locdesc_offset);
			  add_symbol_to_list (sym, list_in_scope);
#ifdef HP_DWARF2_EXTENSIONS
			  /* Locals are supposed to be modifiable by default */
			  SYMBOL_MODIFIABILITY (sym) = ((func_opt_level <= 1) 
			    ? HP_selectively_unmodifiable 
			    : HP_selectively_modifiable);
#endif /* HP_DWARF2_EXTENSIONS */
			  if (optimized_out)
			    {
			      SYMBOL_CLASS (sym) = LOC_OPTIMIZED_OUT;
			    }
			  else if (isreg)
			    {
			      SYMBOL_CLASS (sym) = LOC_REGISTER;
			      if (!ispiece)
				SYMBOL_VALUE (sym) =
				  ADJUST_IA64_REGNUM (SYMBOL_VALUE (sym));
			      }
			  else if (offreg)
			    {
			      SYMBOL_CLASS (sym) = LOC_BASEREG;
			      SYMBOL_BASEREG (sym) = ADJUST_IA64_REGNUM (basereg);
			    }
			  else if (islocal)
			    {
			      SYMBOL_CLASS (sym) = LOC_LOCAL;
			    }
			  
#ifdef HP_DWARF2_EXTENSIONS
			  if (no_of_mod_ranges)
			    set_modifiability_information (sym);
#endif /* DWARF2_EXTENSIONS */
			}
		    }
		  else
		    {
		      /* If this is a function we need to enter the class as
			 LOC_BLOCK. If this is a variable then we do not know 
			 the address of this symbol.
			 If it is an external symbol and we have type information
			 for it, enter the symbol as a LOC_UNRESOLVED symbol.
			 The address of the variable will then be determined from
			 the minimal symbol table whenever the variable is
			 referenced.  */
		      
		      SYMBOL_CLASS (sym) = LOC_UNRESOLVED;
		      add_symbol_to_list (sym, list_in_scope); 
		    }
		}
	    }
	  break;
	case DW_TAG_typedef: 
	case DW_TAG_base_type:
	  SYMBOL_CLASS (sym) = LOC_TYPEDEF;
	  SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;

	  add_symbol_to_list (sym, list_in_scope);
	  break;
	case DW_TAG_enumerator:
	case DW_TAG_constant:
	  attr = dwarf_attr (die, DW_AT_const_value);
	  if (attr)
	    {
	      dwarf2_const_value (attr, sym, objfile);
	    }
	  add_symbol_to_list (sym, list_in_scope);
	  break;
	default:
	  /* Not a tag we recognize.  Hopefully we aren't processing
	     trash data, but since we must specifically ignore things
	     we don't recognize, there is nothing else we should do at
	     this point. */
	  complain (&dwarf2_unsupported_tag, dwarf_tag_name (die->tag));
	  break;
	}
    }
#ifdef GDB_TARGET_IS_HPPA_20W	
  /* Make sure that this gets set even if locdesc_offset = 0 */
  if (sym)
    SYMBOL_DEBUG_LOCATION_OFFSET (sym) = locdesc_offset;
#endif

  /* Bindu 02/02/04: JAGad88954
     Check if this an anonymous union type variable. If so, go through all the
     fields in the type and add each of them as symbols. We should be able
     to reference each member of anonymous union structure directly by their
     name without any qualification. We also reuse the current symbol for the
     last member of the union. Current symbol is not anymore useful to us. */
  if (   (SYMBOL_NAME (sym) [0] == '{')
      && (strncmp (SYMBOL_NAME (sym), "{anonymous", 10) == 0)
      && (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_UNION)
      && (TYPE_NAME (SYMBOL_TYPE (sym)) == 0)
      && (TYPE_TAG_NAME (SYMBOL_TYPE (sym)) == 0))
    {
      struct symbol *sym2 = NULL;
      struct type *sym_type = SYMBOL_TYPE (sym);
      int field_no;
      attr2 = dwarf_attr (die, DW_AT_external);
      for (field_no = 0; field_no < sym_type->nfields - 1; field_no++)
        {
          sym2 = (struct symbol *) (void *) obstack_alloc (&objfile->symbol_obstack,
					     sizeof (struct symbol));
          OBJSTAT (objfile, n_syms++);
          memcpy (sym2, sym, sizeof (struct symbol));
          SYMBOL_NAME (sym2) = sym_type->fields[field_no].name;
          SYMBOL_TYPE (sym2) = sym_type->fields[field_no].type;
          if (attr2 && (DW_UNSND (attr2) != 0))
            add_symbol_to_list (sym2, &global_symbols);
          else
            add_symbol_to_list (sym2, list_in_scope);
        }
      SYMBOL_NAME (sym) = sym_type->fields[field_no].name;
      SYMBOL_TYPE (sym) = sym_type->fields[field_no].type;
      
    }
  return (sym);
}

/* Copy constant value from an attribute to a symbol.  */

static void
dwarf2_const_value (struct attribute *attr, struct symbol *sym, struct objfile *objfile)
{
  struct dwarf_block *blk;

  switch (attr->form)
    {
    case DW_FORM_addr:
      if (TYPE_LENGTH (SYMBOL_TYPE (sym)) != (unsigned int) address_size)
	complain (&dwarf2_const_value_length_mismatch, SYMBOL_NAME (sym),
		  address_size, TYPE_LENGTH (SYMBOL_TYPE (sym)));
      SYMBOL_VALUE_BYTES (sym) = (char *)
	obstack_alloc (&objfile->symbol_obstack, address_size);
      store_address (SYMBOL_VALUE_BYTES (sym), address_size, DW_ADDR (attr));
      SYMBOL_CLASS (sym) = LOC_CONST_BYTES;
      break;
    case DW_FORM_block1:
    case DW_FORM_block2:
    case DW_FORM_block4:
    case DW_FORM_block:
      blk = DW_BLOCK (attr);
      if (TYPE_LENGTH (SYMBOL_TYPE (sym)) != blk->size)
	complain (&dwarf2_const_value_length_mismatch, SYMBOL_NAME (sym),
		  blk->size, TYPE_LENGTH (SYMBOL_TYPE (sym)));
      SYMBOL_VALUE_BYTES (sym) = (char *)
	obstack_alloc (&objfile->symbol_obstack, blk->size);
      memcpy (SYMBOL_VALUE_BYTES (sym), blk->data, blk->size);
      SYMBOL_CLASS (sym) = LOC_CONST_BYTES;
      break;

      /* The DW_AT_const_value attributes are supposed to carry the
	 symbol's value "represented as it would be on the target
	 architecture."  By the time we get here, it's already been
	 converted to host endianness, so we just need to sign- or
	 zero-extend it as appropriate.  */
    case DW_FORM_data1:
      dwarf2_const_value_data (attr, sym, 8);
      break;
    case DW_FORM_data2:
      dwarf2_const_value_data (attr, sym, 16);
      break;
    case DW_FORM_data4:
      dwarf2_const_value_data (attr, sym, 32);
      break;
    case DW_FORM_data8:
      dwarf2_const_value_data (attr, sym, 64);
      break;

    case DW_FORM_sdata:
      SYMBOL_VALUE (sym) = DW_SND (attr);
      SYMBOL_CLASS (sym) = LOC_CONST;
      break;

    case DW_FORM_udata:
      SYMBOL_VALUE (sym) = DW_UNSND (attr);
      SYMBOL_CLASS (sym) = LOC_CONST;
      break;

    default:
      complain (&dwarf2_unsupported_const_value_attr,
		dwarf_form_name (attr->form));
      SYMBOL_VALUE (sym) = 0;
      SYMBOL_CLASS (sym) = LOC_CONST;
      break;
    }
}


/* Given an attr with a DW_FORM_dataN value in host byte order, sign-
   or zero-extend it as appropriate for the symbol's type.  */
static void
dwarf2_const_value_data (struct attribute *attr,
			 struct symbol *sym,
			 int bits)
{
  LONGEST l = DW_UNSND (attr);

  if (bits < sizeof (l) * 8)
    {
      if (TYPE_UNSIGNED (SYMBOL_TYPE (sym)))
	l &= ((LONGEST) 1 << bits) - 1;
      else
	l = (l << (sizeof (l) * 8 - bits)) >> (sizeof (l) * 8 - bits);
    }

  SYMBOL_VALUE (sym) = l;
  SYMBOL_CLASS (sym) = LOC_CONST;
}

#ifdef HP_IA64
static struct psymtab_expansion_history *
save_psymtab_expansion_history (struct partial_symtab *pst)
{
  struct psymtab_expansion_history *old = (struct psymtab_expansion_history *)
                                          xmalloc (sizeof (struct psymtab_expansion_history));
  old->pst = pst; 
  old->die_ref_table = die_ref_table;
  old->dwarf_info_buffer = dwarf_info_buffer;
  old->dwarf_abbrev_buffer = dwarf_abbrev_buffer;
  old->dwarf_line_buffer = dwarf_line_buffer;
  old->dwarf_line_buffer_actual = dwarf_line_buffer_actual;
  old->dwarf_info_buffer_procs = dwarf_info_buffer_procs;
  old->dwarf_abbrev_buffer_procs = dwarf_abbrev_buffer_procs;
  old->dwarf_str_buffer = dwarf_str_buffer;
  old->dwarf_loc_buffer = dwarf_loc_buffer;
  old->dwarf_macinfo_buffer = dwarf_macinfo_buffer;
  old->baseaddr = baseaddr;
  baseaddr = 0;
  old->data_baseaddr = data_baseaddr;
  data_baseaddr = 0;
  memcpy (&old->dwarf2_tmp_obstack, &dwarf2_tmp_obstack, 
          sizeof (struct obstack));
  memcpy (&old->dwarf2_tmp_obstack_2, &dwarf2_tmp_obstack_2, 
          sizeof (struct obstack));
  old->dwarf_line_offset_pst = dwarf_line_offset_pst;
  old->dwarf_line_offset_actual_pst = dwarf_line_offset_actual_pst;
  old->context_stack = context_stack;
  context_stack = NULL;
  old->context_stack_depth = context_stack_depth;
  context_stack_depth = 0;
  old->context_stack_size = context_stack_size;
  old->last_source_file = last_source_file;
  last_source_file = NULL;
  old->last_source_start_addr = last_source_start_addr;
  last_source_start_addr = 0;
  old->subfiles = subfiles;
  subfiles = NULL;
  old->current_subfile = current_subfile;
  current_subfile = NULL;
  old->symnum = symnum;
  symnum = 0;
  old->processing_gcc_compilation = processing_gcc_compilation;
  old->processing_acc_compilation = processing_acc_compilation;
  old->processing_hp_compilation = processing_hp_compilation;
  old->file_symbols = file_symbols;
  file_symbols = NULL;
  old->local_symbols = local_symbols;
  local_symbols = NULL;
  old->global_symbols = global_symbols;
  global_symbols = NULL;
  old->param_symbols = param_symbols;
  param_symbols = NULL;
  old->global_var_name = global_var_name;
  global_var_name = NULL;
  old->import_var_name = import_var_name;
  import_var_name = NULL;
  old->num_globusings = num_globusings;
  num_globusings = 0;
  old->max_globusings = max_globusings;
  max_globusings = 0;
  old->num_anon_ns = num_anon_ns;
  num_anon_ns = 0;
  old->global_using_dirs = global_using_dirs;
  global_using_dirs = NULL;
  old->aliases = aliases;
  aliases = NULL;
  old->within_function = within_function;
  within_function = 0;
  old->pending_blocks = pending_blocks;
  pending_blocks = NULL;
  old->free_pendings = free_pendings;
  free_pendings = NULL;
  old->subfile_stack = subfile_stack;
  subfile_stack = NULL;
  old->next_symbol_text_func = next_symbol_text_func;
  next_symbol_text_func = NULL;
  old->type_vector = type_vector;
  type_vector = NULL;
  old->type_vector_length = type_vector_length;
  type_vector_length = 0;
  old->pending_macros = pending_macros;
  pending_macros = NULL;

  /* offsets and sizes of debugging sections */

  old->dwarf_info_offset = dwarf_info_offset;
  old->dwarf_abbrev_offset = dwarf_abbrev_offset;
  old->dwarf_info_offset_procs = dwarf_info_offset_procs; 
  old->dwarf_abbrev_offset_procs = dwarf_abbrev_offset_procs;
  old->dwarf_line_offset = dwarf_line_offset;
  old->dwarf_line_offset_actual = dwarf_line_offset_actual;
  old->dwarf_loc_offset = dwarf_loc_offset;
  old->dwarf_str_offset = dwarf_str_offset;
  old->dwarf_macinfo_offset = dwarf_macinfo_offset;

  old->dwarf_info_size = dwarf_info_size;
  old->dwarf_abbrev_size = dwarf_abbrev_size;
  old->dwarf_info_size_procs = dwarf_info_size_procs;
  old->dwarf_abbrev_size_procs = dwarf_abbrev_size_procs;
  old->dwarf_line_size = dwarf_line_size;
  old->dwarf_line_size_actual = dwarf_line_size_actual;
  old->dwarf_loc_size = dwarf_loc_size;
  old->dwarf_str_size = dwarf_str_size;
  old->dwarf_macinfo_size = dwarf_macinfo_size;

  old->import_lowpc = import_lowpc;
  old->comp_unit_lowpc = comp_unit_lowpc;
  memcpy (old->ftypes, ftypes, sizeof (ftypes)); 
  old->list_in_scope = list_in_scope;
  list_in_scope = &file_symbols;
  old->current_func_lowpc = current_func_lowpc;
  current_func_lowpc = 0;
  old->current_func_highpc = current_func_highpc;
  current_func_highpc = 0;
  old->current_func_type = current_func_type;
  current_func_type = NULL;
  old->current_funcsym = current_funcsym;
  current_funcsym = NULL;
  old->func_opt_level = func_opt_level;
  old->file_opt_level = file_opt_level;

  return old;
}

void
restore_psymtab_expansion_history (struct psymtab_expansion_history *old,
                                   struct objfile *objfile)
{
  objfile->current_pst = old->pst; 
  die_ref_table = old->die_ref_table;
  dwarf_info_buffer = old->dwarf_info_buffer;
  dwarf_abbrev_buffer = old->dwarf_abbrev_buffer;
  dwarf_line_buffer = old->dwarf_line_buffer;
  dwarf_line_buffer_actual = old->dwarf_line_buffer_actual;
  dwarf_info_buffer_procs =  old->dwarf_info_buffer_procs;
  dwarf_abbrev_buffer_procs = old->dwarf_abbrev_buffer_procs;
  dwarf_str_buffer = old->dwarf_str_buffer;
  dwarf_loc_buffer = old->dwarf_loc_buffer;
  dwarf_macinfo_buffer = old->dwarf_macinfo_buffer;
  baseaddr = old->baseaddr;
  data_baseaddr = old->data_baseaddr;
  memcpy (&dwarf2_tmp_obstack, &old->dwarf2_tmp_obstack, 
          sizeof (struct obstack));
  memcpy (&dwarf2_tmp_obstack_2, &old->dwarf2_tmp_obstack_2, 
         sizeof (struct obstack));
  dwarf_line_offset_pst = old->dwarf_line_offset_pst;
  dwarf_line_offset_actual_pst = old->dwarf_line_offset_actual_pst;
  context_stack = old->context_stack;
  context_stack_depth = old->context_stack_depth;
  context_stack_size = old->context_stack_size;
  last_source_file = old->last_source_file;
  last_source_start_addr = old->last_source_start_addr;
  subfiles = old->subfiles;
  current_subfile = old->current_subfile;
  symnum = old->symnum;
  processing_gcc_compilation = old->processing_gcc_compilation;
  processing_acc_compilation = old->processing_acc_compilation;
  processing_hp_compilation = old->processing_hp_compilation;
  file_symbols = old->file_symbols;
  local_symbols = old->local_symbols;
  global_symbols = old->global_symbols;
  param_symbols = old->param_symbols;
  global_var_name = old->global_var_name;
  import_var_name = old->import_var_name;
  num_globusings = old->num_globusings;
  max_globusings = old->max_globusings;
  num_anon_ns = old->num_anon_ns;
  global_using_dirs = old->global_using_dirs;
  aliases = old->aliases;

  within_function =  old->within_function;
  pending_blocks = old->pending_blocks;
  free_pendings = old->free_pendings;
  subfile_stack = old->subfile_stack;
  next_symbol_text_func = old->next_symbol_text_func;
  type_vector = old->type_vector;
  type_vector_length = old->type_vector_length;
  pending_macros = old->pending_macros;

  /* offsets and sizes of debugging sections */

  dwarf_info_offset = old->dwarf_info_offset;
  dwarf_abbrev_offset = old->dwarf_abbrev_offset;
  dwarf_info_offset_procs = old-> dwarf_info_offset_procs; 
  dwarf_abbrev_offset_procs = old->dwarf_abbrev_offset_procs;
  dwarf_line_offset = old->dwarf_line_offset;
  dwarf_line_offset_actual = old->dwarf_line_offset_actual;
  dwarf_loc_offset = old->dwarf_loc_offset;
  dwarf_str_offset = old->dwarf_str_offset;
  dwarf_macinfo_offset = old->dwarf_macinfo_offset;

  dwarf_info_size = old->dwarf_info_size;
  dwarf_abbrev_size = old->dwarf_abbrev_size;
  dwarf_info_size_procs = old->dwarf_info_size_procs;
  dwarf_abbrev_size_procs = old->dwarf_abbrev_size_procs;
  dwarf_line_size =  old->dwarf_line_size;
  dwarf_line_size_actual = old->dwarf_line_size_actual;
  dwarf_loc_size = old->dwarf_loc_size;
  dwarf_str_size = old->dwarf_str_size;
  dwarf_macinfo_size = old->dwarf_macinfo_size;

  import_lowpc = old->import_lowpc;
  comp_unit_lowpc = old->comp_unit_lowpc;
  memcpy (ftypes, old->ftypes, sizeof (ftypes)); 
  list_in_scope = old->list_in_scope;

  current_func_lowpc = old->current_func_lowpc;
  current_func_highpc = old->current_func_highpc;
  current_func_type = old->current_func_type;
  func_opt_level = old->func_opt_level;
  file_opt_level = old->file_opt_level;

  free (old);
}

/* Given an absolute offset into the debug_info, this function
   returns the corresponding psymtab object.
*/
static struct partial_symtab *
find_pst_for_die_ref_addr (unsigned int ref)
{
  struct partial_symtab *p, *tmp, *ret = NULL;
  int i = 0, j, k;
  int this_offset = 0, next_offset = 0;
  int off1 = 0, off2 = 0;

  if (!cached_psymtabs_count)
    cached_psymtabs = (struct psymtabs_cache *) xmalloc 
                      (cached_psymtabs_size * sizeof (struct psymtabs_cache));
  if (cached_psymtabs_count)
    {
      for (i = 0; i < cached_psymtabs_count; i ++)
        if (ref > cached_psymtabs[i].this_offset && 
            ref < cached_psymtabs[i].next_offset &&
            cached_psymtabs[i].obj == current_objfile)
          ret = cached_psymtabs[i].pst;

    }
  
  if (ret)
    return ret;
  /* We rely on the last psymtab matched and do not break for the first match.
     This is because, we want to ignore the psymtabs from debug_procs, 
     see dwarf2_build_psymtabs_hard (, mainline = -1).
  */
  ALL_OBJFILE_PSYMTABS (current_objfile, p)
    {
      tmp = p->next;
      if (!tmp && 
          DWARF_INFO_OFFSET(p) < ref &&
          ref < off2)
        {
          ret = p;
          this_offset = DWARF_INFO_OFFSET(p);
          next_offset = off2;
          break; 
        }

      off1 = DWARF_INFO_OFFSET(tmp);
      off2 = DWARF_INFO_OFFSET(p);    

      if (off1 == off2 && off1)
        {
          p = p->next;
          off1 = DWARF_INFO_OFFSET(p->next);
        } 

      if (ref > off1 &&
          ref < off2)
        {
          ret = p->next;
          this_offset = off1;
          next_offset = off2;
        } 
     }

  /* Increment the psymtab count and then check whether the cache has
     to be expanded. */
  cached_psymtabs_count ++;
  if (cached_psymtabs_count >= cached_psymtabs_size)
    {
      cached_psymtabs_size *= 2;    
      cached_psymtabs = (struct psymtabs_cache *) xrealloc 
                        (cached_psymtabs, 
                         cached_psymtabs_size * sizeof (struct psymtabs_cache));
           
    } 
  cached_psymtabs [cached_psymtabs_count - 1].pst = ret; 
  cached_psymtabs [cached_psymtabs_count - 1].this_offset = this_offset;
  cached_psymtabs [cached_psymtabs_count - 1].next_offset = next_offset;
  cached_psymtabs [cached_psymtabs_count - 1].obj = current_objfile;
  return ret;
}

/* This function fetches the die_info object from the hash chain
   die_ref_table of the corresponding psymtab. This is called when
   we want to know which psymtab should be recursively expanded.
   This is similar to follow_die_ref ().
*/
static struct die_info *
follow_die_ref_in_table (struct partial_symtab *p, unsigned int offset)
{
  struct die_info *die;
  int h;

  h = (offset % REF_HASH_SIZE);
  
  die = p->die_ref_table[h];

  while (die)
    {
      if (die->offset == offset)
	{
	  return die;
	}
      die = die->next_ref;
    }
  return NULL;

}

/* This function returns the die_ref_table that corresponds to a
   particular die. We use the die->offset to index into the table.
   Once we get a die_info object from the die->offset, we validate 
   whether the object retreived is matching to the die_info argment. 
*/
static struct die_info **
find_die_ref_table (struct objfile *objfile, struct die_info *die)
{
  struct die_info *tmp = NULL;
  struct partial_symtab *p;
  /* If the die is in the current die_ref_table, return the same. */
  tmp = follow_die_ref (die->offset);

  if (tmp && tmp == die)
    return die_ref_table;
  /* Go through the psymtab list and return the corresponding 
     die_ref_table. */
  ALL_OBJFILE_PSYMTABS(objfile, p)
    {
      if (p->die_ref_table)
        tmp = follow_die_ref_in_table (p, die->offset);
      if (tmp && tmp == die)
        return p->die_ref_table;
    }

  return NULL;
}
#endif

/* This routine resolves the DW_FORM_ref* attributes and returns
   the die_info object that is referred to.
   It recursively expand psymtabs as and when we find a cross-compilation
   unit reference. The state of the previous psymtab expansion is stored on the
   stack to be restored after the callee psymtab_to_symtab () returns.
   The cross compilation unit references are uni-directional, that is
   there can be a reference from compilation unit N to compilation unit N-X,
   and not both ways. If there exists bi-directional references, then this
   routine might go into a deadlock. For this routine to work, the direction of 
   references needs to be either backward or forward, not both.
   This was added to support debugging executables processed by dwutils, which
   is generating backward references only. Making debugger understand
   bi-directional references would be quite a task, as we need to handle
   unsatisfied references separately for every psymtab and also determine the
   complete symbol/type information separately, doing recursive calls like done here
   will not work out as the psymtabs are partially expanded. 
   (see JAGag43873 labnotes for a probable method to handle bi-directional references)
   
   This routine also ensures that at whatever point of processing dies,
   we refer the correct die_ref_table by setting the current die_ref_table to
   the die_ref_table that holds the die "die". We need to ensure this because
   after we return a die from another compilation unit, that die might have 
   local references to dies which have to be fetched from the die_ref_table for
   the referred compilation unit. 

   This routine replaced some calls to follow_die_ref () as this resolves
   the refernces correctly. We call follow_die_ref () only when we are sure
   that the references are local.
   
   Cross-compilation unit references are only emitted for non DOOM exes for now,
   and hence the current support is only for exes built with +noobjdebug.
   - mithun 071507
*/
static struct die_info *
find_die_from_ref      (struct die_info *die, 
                        struct objfile *objfile, 
                        struct attribute *attr) 
{
#ifdef HP_IA64
  struct die_info *ret_die = NULL;
  struct attribute *name_attr;
  struct psymtab_expansion_history *old = NULL;
  struct partial_symtab *p;
  unsigned int ref = dwarf2_get_ref_die_offset (attr);
  
  if (!debug_dwutils_processed)
    return follow_die_ref (ref);

  /* Ensure that the current die_ref_table is the correct one. 
     It should be the die_ref_table corresponding to the "die"
  */
  if (!processing_doom_objfile)
    die_ref_table = find_die_ref_table (objfile, die);

  /* Do the usual fetching of dies if the reference is local to the
     compilation unit, ie not DW_FORM_ref_addr.
  */
  if (attr->form != DW_FORM_ref_addr)
    return follow_die_ref (ref);

  if (processing_doom_objfile)
    error ("DWARF error: cross compilation unit references for DOOM executable.");


  /* From the absolute offset "ref", get the corresponding psymtab. */
  p = find_pst_for_die_ref_addr (ref);
  if (!p->readin)
    {  
       /* Store the state of the psymtab expansion before  recursively 
          expanding another psymtab. 
       */
       old = save_psymtab_expansion_history (objfile->current_pst);

       /* Expand the new psymtab which contains the referred die info. */
       PSYMTAB_TO_SYMTAB(p);

       /* The second argument to follow_die_ref_in_table should be the 
          offset relative to the begin of the compilation unit. */
       ret_die = follow_die_ref_in_table (p, 
                                           ref - DWARF_INFO_OFFSET(p));
       if (old)
       /* Restore the older psymtab expansion state stored on stack after
          recursively expanding another psymtab. */
         restore_psymtab_expansion_history (old, objfile);

     }
  else
     ret_die = follow_die_ref_in_table (p, ref - DWARF_INFO_OFFSET(p));

  if (!ret_die)
    {  
       error ("DWARF Error: Cannot find referrent at offset %d.", ref);
       return NULL;
    } 
  if (ret_die == die)
    {
      name_attr = dwarf_attr (die, DW_AT_name);
      if (name_attr && DW_STRING (name_attr))
        {
           error ("DWARF Error: Type '%s' at offset %d points to itself.", 
        	     DW_STRING (name_attr),
	          ref);
	}
      else
        {
          error ("DWARF Error: Type at offset %d points to itself.", ref);
        }
       return NULL;

    }

  return ret_die;
#else
  /* Handle PA64 GCC emitted debug_info as was handled before. */
  return follow_die_ref (dwarf2_get_ref_die_offset (attr));
#endif
}

/* Return the type of the die in question using its DW_AT_type attribute.  */

static struct type *
die_type (struct die_info *die, struct objfile *objfile,
	  const struct comp_unit_head *cu_header)
{
  struct type *type;
  struct attribute *name_attr;
  struct attribute *type_attr;
  struct die_info *type_die = NULL;
  unsigned int ref;

  type_attr = dwarf_attr (die, DW_AT_type);
  if (!type_attr)
    {
      /* A missing DW_AT_type represents a void type.  */
#ifdef KGDB
      if (die->tag == DW_TAG_subprogram)
         return dwarf2_fundamental_type (objfile,  FT_INTEGER);
      else
         return dwarf2_fundamental_type (objfile,  FT_VOID);
#else /* KGDB */
      return dwarf2_fundamental_type (objfile,  FT_VOID);
#endif /* KGDB */
    }
  else
    {
      ref = dwarf2_get_ref_die_offset (type_attr);
      
      type_die = find_die_from_ref (die, objfile, type_attr);

      if (!type_die)
        {  
          error ("DWARF Error: Cannot find referrent at offset %d.\nIf the executable is processed by dwutils, then set debug-dwutils-processed on in a new debugging session.", ref);
          return NULL;
        } 
      if (type_die == die)
	{
	  name_attr = dwarf_attr (die, DW_AT_name);
	  if (name_attr && DW_STRING (name_attr))
	    {
	      error ("DWARF Error: Type '%s' at offset %d points to itself.", 
    		     DW_STRING (name_attr),
		     ref);
	    }
	  else
	    {
	      error ("DWARF Error: Type at offset %d points to itself.", ref);
	    }
	  return NULL;

	}
    }
  type = tag_type_to_type (type_die, objfile, cu_header);
  if (!type)
    {
      dump_die (type_die);
      error ("DWARF Error: Problem turning type die at offset into gdb type.");
    }
  return type;
}

/* Return the containing type of the die in question using its
   DW_AT_containing_type attribute.  */

static struct type *
die_containing_type (struct die_info *die, struct objfile *objfile,
		     const struct comp_unit_head *cu_header)
{
  struct type *type = NULL;
  struct attribute *type_attr;
  struct die_info *type_die = NULL;
  unsigned int ref;

  type_attr = dwarf_attr (die, DW_AT_containing_type);
  if (type_attr)
    {
      type_die = find_die_from_ref (die, objfile, type_attr);
      type = tag_type_to_type (type_die, objfile, cu_header);
    }
  if (!type)
    {
      if (type_die)
	dump_die (type_die);
      error ("DWARF Error: Problem turning containing type into gdb type.");
    }
  return type;
}


static struct type *
tag_type_to_type (struct die_info *die, struct objfile *objfile,
		  const struct comp_unit_head *cu_header)
{
  if (die->type)
    {
      return die->type;
    }
  else
    {
      read_type_die (die, objfile, cu_header);
      if (!die->type)
	{
	  dump_die (die);
	  error ("DWARF Error: Cannot find type of die.");
	}
      return die->type;
    }
}

static void
read_type_die (struct die_info *die, struct objfile *objfile,
	       const struct comp_unit_head *cu_header)
{
  switch (die->tag)
    {
    case DW_TAG_class_type:
    case DW_TAG_structure_type:
    case DW_TAG_union_type:
      read_structure_scope (die, objfile, cu_header);
      break;
    case DW_TAG_enumeration_type:
      read_enumeration (die, objfile, cu_header);
      break;
    case DW_TAG_subprogram:
    case DW_TAG_subroutine_type:
      read_subroutine_type (die, objfile, cu_header);
      break;
    case DW_TAG_entry_point:
    if (cu_language == language_fortran)
      read_subroutine_type (die, objfile, cu_header);
      break;
    case DW_TAG_array_type:
      read_array_type (die, objfile, cu_header);
      break;
    case DW_TAG_pointer_type:
      read_tag_pointer_type (die, objfile, cu_header);
      break;
    case DW_TAG_ptr_to_member_type:
      read_tag_ptr_to_member_type (die, objfile, cu_header);
      break;
    case DW_TAG_reference_type:
      read_tag_reference_type (die, objfile, cu_header);
      break;
    case DW_TAG_const_type:
      read_tag_const_type (die, objfile, cu_header);
      break;
    case DW_TAG_volatile_type:
      read_tag_volatile_type (die, objfile, cu_header);
      break;
    case DW_TAG_string_type:
      read_tag_string_type (die, objfile, cu_header);
      break;
    case DW_TAG_typedef:
      read_typedef (die, objfile, cu_header);
      break;
    case DW_TAG_base_type:
      read_base_type (die, objfile);
      break;
#ifdef HP_DWARF2_EXTENSIONS
    case DW_TAG_HP_array_descriptor:
      read_array_descriptor_type (die, objfile, cu_header);
      break;
#endif
    default:
      complain (&dwarf2_unexpected_tag, dwarf_tag_name (die->tag));
      break;
    }
}

static struct type *
dwarf_base_type (int encoding, int size, struct objfile *objfile)
{
  /* FIXME - this should not produce a new (struct type *)
     every time.  It should cache base types.  */
  struct type *type;
  switch (encoding)
    {
    case DW_ATE_address:
      type = dwarf2_fundamental_type (objfile, FT_VOID);
      return type;
    case DW_ATE_boolean:
      type = dwarf2_fundamental_type (objfile, FT_BOOLEAN);
      return type;
    case DW_ATE_complex_float128:
    case DW_ATE_complex_float:
      if (size == 32)
	{
	  type = dwarf2_fundamental_type (objfile, FT_EXT_PREC_COMPLEX);
	}
      else if (size == 16)
	{
	  type = dwarf2_fundamental_type (objfile, FT_DBL_PREC_COMPLEX);
	}
      else
	{
	  type = dwarf2_fundamental_type (objfile, FT_COMPLEX);
	}
      return type;
    case DW_ATE_float:
      if (size == 16)
	{
	  type = dwarf2_fundamental_type (objfile, FT_EXT_PREC_FLOAT);
	}
      else if (size == 8)
	{
	  type = dwarf2_fundamental_type (objfile, FT_DBL_PREC_FLOAT);
	}
      else
	{
	  type = dwarf2_fundamental_type (objfile, FT_FLOAT);
	}
      return type;
    case DW_ATE_signed:
      switch (size)
	{
	case 1:
	  type = dwarf2_fundamental_type (objfile, FT_SIGNED_CHAR);
	  break;
	case 2:
	  type = dwarf2_fundamental_type (objfile, FT_SIGNED_SHORT);
	  break;
	default:
	case 4:
	  type = dwarf2_fundamental_type (objfile, FT_SIGNED_INTEGER);
	  break;
	}
      return type;
    case DW_ATE_signed_char:
      type = dwarf2_fundamental_type (objfile, FT_SIGNED_CHAR);
      return type;
    case DW_ATE_unsigned:
      switch (size)
	{
	case 1:
	  type = dwarf2_fundamental_type (objfile, FT_UNSIGNED_CHAR);
	  break;
	case 2:
	  type = dwarf2_fundamental_type (objfile, FT_UNSIGNED_SHORT);
	  break;
	default:
	case 4:
	  type = dwarf2_fundamental_type (objfile, FT_UNSIGNED_INTEGER);
	  break;
	}
      return type;
    case DW_ATE_unsigned_char:
      type = dwarf2_fundamental_type (objfile, FT_UNSIGNED_CHAR);
      return type;
    case  DW_ATE_float128:
      type = dwarf2_fundamental_type (objfile, FT_EXT_PREC_FLOAT);
      return type;
    case DW_ATE_decimal_float:
      if (size == 16)
	{
	  type = dwarf2_fundamental_type (objfile, FT_EXT_PREC_DECFLOAT);
	}
      else if (size == 8)
	{
	  type = dwarf2_fundamental_type (objfile, FT_DBL_PREC_DECFLOAT);
	}
      else
	{
	  type = dwarf2_fundamental_type (objfile, FT_DECFLOAT);
	}
      return type;
    default:
      type = dwarf2_fundamental_type (objfile, FT_SIGNED_INTEGER);
      return type;
    }
}


/* Return sibling of die, NULL if no sibling.  */

struct die_info *
sibling_die (struct die_info *die)
{
  int nesting_level = 0;

  if (!die->has_children)
    {
      if (die->next && (die->next->tag == 0))
	{
	  return NULL;
	}
      else
	{
	  return die->next;
	}
    }
  else
    {
      do
	{
	  if (die->has_children)
	    {
	      nesting_level++;
	    }
	  if (die->tag == 0)
	    {
	      nesting_level--;
	    }
	  die = die->next;
	}
      while (nesting_level);
      if (die && (die->tag == 0))
	{
	  return NULL;
	}
      else
	{
	  return die;
	}
    }
}

/* Get linkage name of a die, return NULL if not found.  */

static char *
dwarf2_linkage_name (struct die_info *die)
{
  struct attribute *attr;


#ifdef FORTRAN_ADDS_UNDERSCORE

  /* For FORTRAN, we prefer DW_AT_name to DW_AT_HP_linkage_name */

  if (cu_language == language_fortran)
    {
      attr = dwarf_attr (die, DW_AT_name);
      if (attr && DW_STRING (attr))
	return DW_STRING (attr);
    }
#endif

  attr = dwarf_attr (die, DW_AT_HP_linkage_name);
  if (attr && DW_STRING (attr))
    return DW_STRING (attr);

  attr = dwarf_attr (die, DW_AT_MIPS_linkage_name);
  if (attr && DW_STRING (attr))
    return DW_STRING (attr);

  attr = dwarf_attr (die, DW_AT_name);
  if (attr && DW_STRING (attr))
    return DW_STRING (attr);

  /* Couldn't find a function name in the dwarf , use minimal symbol instead */
  if (die->tag == DW_TAG_subprogram)
    {
      struct minimal_symbol *msymbol ;
      CORE_ADDR lowpc;
      attr = dwarf_attr (die, DW_AT_low_pc);
      if ((attr) && ((lowpc = DW_ADDR (attr))!= NULL))
	{
          msymbol = lookup_minimal_symbol_by_pc (lowpc+baseaddr);
          if (msymbol != NULL 
	    && (SYMBOL_VALUE_ADDRESS (msymbol) >= lowpc+baseaddr ))
	    return SYMBOL_NAME (msymbol);
        }
    }

  return NULL;
}

/* Convert a DIE tag into its string name.  */

static char *
dwarf_tag_name (register unsigned tag)
{
  switch (tag)
    {
    case DW_TAG_array_type:
      return "DW_TAG_array_type";
    case DW_TAG_class_type:
      return "DW_TAG_class_type";
    case DW_TAG_entry_point:
      return "DW_TAG_entry_point";
    case DW_TAG_enumeration_type:
      return "DW_TAG_enumeration_type";
    case DW_TAG_formal_parameter:
      return "DW_TAG_formal_parameter";
    case DW_TAG_imported_declaration:
      return "DW_TAG_imported_declaration";
    case DW_TAG_label:
      return "DW_TAG_label";
    case DW_TAG_lexical_block:
      return "DW_TAG_lexical_block";
    case DW_TAG_member:
      return "DW_TAG_member";
    case DW_TAG_pointer_type:
      return "DW_TAG_pointer_type";
    case DW_TAG_reference_type:
      return "DW_TAG_reference_type";
    case DW_TAG_compile_unit:
      return "DW_TAG_compile_unit";
    case DW_TAG_string_type:
      return "DW_TAG_string_type";
    case DW_TAG_structure_type:
      return "DW_TAG_structure_type";
    case DW_TAG_subroutine_type:
      return "DW_TAG_subroutine_type";
    case DW_TAG_typedef:
      return "DW_TAG_typedef";
    case DW_TAG_union_type:
      return "DW_TAG_union_type";
    case DW_TAG_unspecified_parameters:
      return "DW_TAG_unspecified_parameters";
    case DW_TAG_variant:
      return "DW_TAG_variant";
    case DW_TAG_common_block:
      return "DW_TAG_common_block";
    case DW_TAG_common_inclusion:
      return "DW_TAG_common_inclusion";
    case DW_TAG_inheritance:
      return "DW_TAG_inheritance";
    case DW_TAG_inlined_subroutine:
      return "DW_TAG_inlined_subroutine";
    case DW_TAG_module:
      return "DW_TAG_module";
    case DW_TAG_ptr_to_member_type:
      return "DW_TAG_ptr_to_member_type";
    case DW_TAG_set_type:
      return "DW_TAG_set_type";
    case DW_TAG_subrange_type:
      return "DW_TAG_subrange_type";
    case DW_TAG_with_stmt:
      return "DW_TAG_with_stmt";
    case DW_TAG_access_declaration:
      return "DW_TAG_access_declaration";
    case DW_TAG_base_type:
      return "DW_TAG_base_type";
    case DW_TAG_catch_block:
      return "DW_TAG_catch_block";
    case DW_TAG_const_type:
      return "DW_TAG_const_type";
    case DW_TAG_constant:
      return "DW_TAG_constant";
    case DW_TAG_enumerator:
      return "DW_TAG_enumerator";
    case DW_TAG_file_type:
      return "DW_TAG_file_type";
    case DW_TAG_friend:
      return "DW_TAG_friend";
    case DW_TAG_namelist:
      return "DW_TAG_namelist";
    case DW_TAG_namelist_item:
      return "DW_TAG_namelist_item";
    case DW_TAG_packed_type:
      return "DW_TAG_packed_type";
    case DW_TAG_subprogram:
      return "DW_TAG_subprogram";
    case DW_TAG_template_type_param:
      return "DW_TAG_template_type_param";
    case DW_TAG_template_value_param:
      return "DW_TAG_template_value_param";
    case DW_TAG_thrown_type:
      return "DW_TAG_thrown_type";
    case DW_TAG_try_block:
      return "DW_TAG_try_block";
    case DW_TAG_variant_part:
      return "DW_TAG_variant_part";
    case DW_TAG_variable:
      return "DW_TAG_variable";
    case DW_TAG_volatile_type:
      return "DW_TAG_volatile_type";
    case DW_TAG_MIPS_loop:
      return "DW_TAG_MIPS_loop";
#ifdef HP_DWARF2_EXTENSIONS
    case DW_TAG_HP_array_descriptor:
      return "DW_TAG_HP_array_descriptor";
#endif
    case DW_TAG_format_label:
      return "DW_TAG_format_label";
    case DW_TAG_function_template:
      return "DW_TAG_function_template";
    case DW_TAG_class_template:
      return "DW_TAG_class_template";
    default:
      return "DW_TAG_<unknown>";
    }
}

/* Convert a DWARF attribute code into its string name.  */

static char *
dwarf_attr_name (register unsigned attr)
{
  switch (attr)
    {
    case DW_AT_sibling:
      return "DW_AT_sibling";
    case DW_AT_location:
      return "DW_AT_location";
    case DW_AT_name:
      return "DW_AT_name";
    case DW_AT_ordering:
      return "DW_AT_ordering";
    case DW_AT_byte_size:
      return "DW_AT_byte_size";
    case DW_AT_bit_offset:
      return "DW_AT_bit_offset";
    case DW_AT_bit_size:
      return "DW_AT_bit_size";
    case DW_AT_stmt_list:
      return "DW_AT_stmt_list";
    case DW_AT_low_pc:
      return "DW_AT_low_pc";
    case DW_AT_high_pc:
      return "DW_AT_high_pc";
    case DW_AT_language:
      return "DW_AT_language";
    case DW_AT_discr:
      return "DW_AT_discr";
    case DW_AT_discr_value:
      return "DW_AT_discr_value";
    case DW_AT_visibility:
      return "DW_AT_visibility";
    case DW_AT_import:
      return "DW_AT_import";
    case DW_AT_string_length:
      return "DW_AT_string_length";
    case DW_AT_common_reference:
      return "DW_AT_common_reference";
    case DW_AT_comp_dir:
      return "DW_AT_comp_dir";
    case DW_AT_const_value:
      return "DW_AT_const_value";
    case DW_AT_containing_type:
      return "DW_AT_containing_type";
    case DW_AT_default_value:
      return "DW_AT_default_value";
    case DW_AT_inline:
      return "DW_AT_inline";
    case DW_AT_is_optional:
      return "DW_AT_is_optional";
    case DW_AT_lower_bound:
      return "DW_AT_lower_bound";
    case DW_AT_producer:
      return "DW_AT_producer";
    case DW_AT_prototyped:
      return "DW_AT_prototyped";
    case DW_AT_return_addr:
      return "DW_AT_return_addr";
    case DW_AT_start_scope:
      return "DW_AT_start_scope";
    case DW_AT_stride_size:
      return "DW_AT_stride_size";
    case DW_AT_upper_bound:
      return "DW_AT_upper_bound";
    case DW_AT_abstract_origin:
      return "DW_AT_abstract_origin";
    case DW_AT_accessibility:
      return "DW_AT_accessibility";
    case DW_AT_address_class:
      return "DW_AT_address_class";
    case DW_AT_artificial:
      return "DW_AT_artificial";
    case DW_AT_base_types:
      return "DW_AT_base_types";
    case DW_AT_calling_convention:
      return "DW_AT_calling_convention";
    case DW_AT_count:
      return "DW_AT_count";
    case DW_AT_data_member_location:
      return "DW_AT_data_member_location";
    case DW_AT_decl_column:
      return "DW_AT_decl_column";
    case DW_AT_decl_file:
      return "DW_AT_decl_file";
    case DW_AT_decl_line:
      return "DW_AT_decl_line";
    case DW_AT_declaration:
      return "DW_AT_declaration";
    case DW_AT_discr_list:
      return "DW_AT_discr_list";
    case DW_AT_encoding:
      return "DW_AT_encoding";
    case DW_AT_external:
      return "DW_AT_external";
    case DW_AT_frame_base:
      return "DW_AT_frame_base";
    case DW_AT_friend:
      return "DW_AT_friend";
    case DW_AT_identifier_case:
      return "DW_AT_identifier_case";
    case DW_AT_macro_info:
      return "DW_AT_macro_info";
    case DW_AT_priority:
      return "DW_AT_priority";
    case DW_AT_segment:
      return "DW_AT_segment";
    case DW_AT_specification:
      return "DW_AT_specification";
    case DW_AT_static_link:
      return "DW_AT_static_link";
    case DW_AT_type:
      return "DW_AT_type";
    case DW_AT_use_location:
      return "DW_AT_use_location";
    case DW_AT_variable_parameter:
      return "DW_AT_variable_parameter";
    case DW_AT_virtuality:
      return "DW_AT_virtuality";
    case DW_AT_vtable_elem_location:
      return "DW_AT_vtable_elem_location";
    case DW_AT_allocated:
      return "DW_AT_allocated";
    case DW_AT_associated:
      return "DW_AT_associated";
    case DW_AT_data_location:
      return "DW_AT_data_location";
    case DW_AT_stride:
      return "DW_AT_stride";
    case DW_AT_entry_pc:
      return "DW_AT_entry_pc";
    case DW_AT_use_UTF8:
      return "DW_AT_use_UTF8";
    case DW_AT_ranges:
      return "DW_AT_ranges";
    case DW_AT_trampoline:
      return "DW_AT_trampoline";
    case DW_AT_call_column:
      return "DW_AT_call_column";
    case DW_AT_call_file:
      return "DW_AT_call_file";
    case DW_AT_call_line:
      return "DW_AT_call_line";

#ifdef MIPS
    case DW_AT_MIPS_fde:
      return "DW_AT_MIPS_fde";
    case DW_AT_MIPS_loop_begin:
      return "DW_AT_MIPS_loop_begin";
    case DW_AT_MIPS_tail_loop_begin:
      return "DW_AT_MIPS_tail_loop_begin";
    case DW_AT_MIPS_epilog_begin:
      return "DW_AT_MIPS_epilog_begin";
    case DW_AT_MIPS_loop_unroll_factor:
      return "DW_AT_MIPS_loop_unroll_factor";
    case DW_AT_MIPS_software_pipeline_depth:
      return "DW_AT_MIPS_software_pipeline_depth";
    case DW_AT_MIPS_linkage_name:
      return "DW_AT_MIPS_linkage_name";
#endif
#ifdef HP_DWARF2_EXTENSIONS
      /* HP extensions */
    case DW_AT_HP_block_index:
      return "DW_AT_HP_block_index";
    case DW_AT_HP_unmodifiable:
      return "DW_AT_HP_unmodifiable";
    case DW_AT_HP_actuals_stmt_list:
      return "DW_AT_HP_actuals_stmt_list";
    case DW_AT_HP_pass_by_reference:
      return "DW_AT_HP_pass_by_reference";
    case DW_AT_HP_proc_per_section:
      return "DW_AT_HP_proc_per_section";
    case DW_AT_HP_raw_data_ptr:
      return "DW_AT_HP_raw_data_ptr";
    case DW_AT_HP_opt_level:
      return "DW_AT_HP_opt_level";
#endif

    case DW_AT_sf_names:
      return "DW_AT_sf_names";
    case DW_AT_src_info:
      return "DW_AT_src_info";
    case DW_AT_mac_info:
      return "DW_AT_mac_info";
    case DW_AT_src_coords:
      return "DW_AT_src_coords";
    case DW_AT_body_begin:
      return "DW_AT_body_begin";
    case DW_AT_body_end:
      return "DW_AT_body_end";
    default:
      return "DW_AT_<unknown>";
    }
}

/* Convert a DWARF value form code into its string name.  */

static char *
dwarf_form_name (register unsigned form)
{
  switch (form)
    {
    case DW_FORM_addr:
      return "DW_FORM_addr";
    case DW_FORM_block2:
      return "DW_FORM_block2";
    case DW_FORM_block4:
      return "DW_FORM_block4";
    case DW_FORM_data2:
      return "DW_FORM_data2";
    case DW_FORM_data4:
      return "DW_FORM_data4";
    case DW_FORM_data8:
      return "DW_FORM_data8";
    case DW_FORM_string:
      return "DW_FORM_string";
    case DW_FORM_block:
      return "DW_FORM_block";
    case DW_FORM_block1:
      return "DW_FORM_block1";
    case DW_FORM_data1:
      return "DW_FORM_data1";
    case DW_FORM_flag:
      return "DW_FORM_flag";
    case DW_FORM_sdata:
      return "DW_FORM_sdata";
    case DW_FORM_strp:
      return "DW_FORM_strp";
    case DW_FORM_udata:
      return "DW_FORM_udata";
    case DW_FORM_ref_addr:
      return "DW_FORM_ref_addr";
    case DW_FORM_ref1:
      return "DW_FORM_ref1";
    case DW_FORM_ref2:
      return "DW_FORM_ref2";
    case DW_FORM_ref4:
      return "DW_FORM_ref4";
    case DW_FORM_ref8:
      return "DW_FORM_ref8";
    case DW_FORM_ref_udata:
      return "DW_FORM_ref_udata";
    case DW_FORM_indirect:
      return "DW_FORM_indirect";
    default:
      return "DW_FORM_<unknown>";
    }
}

/* Convert a DWARF stack opcode into its string name.  */

static char *
dwarf_stack_op_name (register unsigned op)
{
  switch (op)
    {
    case DW_OP_addr:
      return "DW_OP_addr";
    case DW_OP_deref:
      return "DW_OP_deref";
    case DW_OP_const1u:
      return "DW_OP_const1u";
    case DW_OP_const1s:
      return "DW_OP_const1s";
    case DW_OP_const2u:
      return "DW_OP_const2u";
    case DW_OP_const2s:
      return "DW_OP_const2s";
    case DW_OP_const4u:
      return "DW_OP_const4u";
    case DW_OP_const4s:
      return "DW_OP_const4s";
    case DW_OP_const8u:
      return "DW_OP_const8u";
    case DW_OP_const8s:
      return "DW_OP_const8s";
    case DW_OP_constu:
      return "DW_OP_constu";
    case DW_OP_consts:
      return "DW_OP_consts";
    case DW_OP_dup:
      return "DW_OP_dup";
    case DW_OP_drop:
      return "DW_OP_drop";
    case DW_OP_over:
      return "DW_OP_over";
    case DW_OP_pick:
      return "DW_OP_pick";
    case DW_OP_swap:
      return "DW_OP_swap";
    case DW_OP_rot:
      return "DW_OP_rot";
    case DW_OP_xderef:
      return "DW_OP_xderef";
    case DW_OP_abs:
      return "DW_OP_abs";
    case DW_OP_and:
      return "DW_OP_and";
    case DW_OP_div:
      return "DW_OP_div";
    case DW_OP_minus:
      return "DW_OP_minus";
    case DW_OP_mod:
      return "DW_OP_mod";
    case DW_OP_mul:
      return "DW_OP_mul";
    case DW_OP_neg:
      return "DW_OP_neg";
    case DW_OP_not:
      return "DW_OP_not";
    case DW_OP_or:
      return "DW_OP_or";
    case DW_OP_plus:
      return "DW_OP_plus";
    case DW_OP_plus_uconst:
      return "DW_OP_plus_uconst";
    case DW_OP_shl:
      return "DW_OP_shl";
    case DW_OP_shr:
      return "DW_OP_shr";
    case DW_OP_shra:
      return "DW_OP_shra";
    case DW_OP_xor:
      return "DW_OP_xor";
    case DW_OP_bra:
      return "DW_OP_bra";
    case DW_OP_eq:
      return "DW_OP_eq";
    case DW_OP_ge:
      return "DW_OP_ge";
    case DW_OP_gt:
      return "DW_OP_gt";
    case DW_OP_le:
      return "DW_OP_le";
    case DW_OP_lt:
      return "DW_OP_lt";
    case DW_OP_ne:
      return "DW_OP_ne";
    case DW_OP_skip:
      return "DW_OP_skip";
    case DW_OP_lit0:
      return "DW_OP_lit0";
    case DW_OP_lit1:
      return "DW_OP_lit1";
    case DW_OP_lit2:
      return "DW_OP_lit2";
    case DW_OP_lit3:
      return "DW_OP_lit3";
    case DW_OP_lit4:
      return "DW_OP_lit4";
    case DW_OP_lit5:
      return "DW_OP_lit5";
    case DW_OP_lit6:
      return "DW_OP_lit6";
    case DW_OP_lit7:
      return "DW_OP_lit7";
    case DW_OP_lit8:
      return "DW_OP_lit8";
    case DW_OP_lit9:
      return "DW_OP_lit9";
    case DW_OP_lit10:
      return "DW_OP_lit10";
    case DW_OP_lit11:
      return "DW_OP_lit11";
    case DW_OP_lit12:
      return "DW_OP_lit12";
    case DW_OP_lit13:
      return "DW_OP_lit13";
    case DW_OP_lit14:
      return "DW_OP_lit14";
    case DW_OP_lit15:
      return "DW_OP_lit15";
    case DW_OP_lit16:
      return "DW_OP_lit16";
    case DW_OP_lit17:
      return "DW_OP_lit17";
    case DW_OP_lit18:
      return "DW_OP_lit18";
    case DW_OP_lit19:
      return "DW_OP_lit19";
    case DW_OP_lit20:
      return "DW_OP_lit20";
    case DW_OP_lit21:
      return "DW_OP_lit21";
    case DW_OP_lit22:
      return "DW_OP_lit22";
    case DW_OP_lit23:
      return "DW_OP_lit23";
    case DW_OP_lit24:
      return "DW_OP_lit24";
    case DW_OP_lit25:
      return "DW_OP_lit25";
    case DW_OP_lit26:
      return "DW_OP_lit26";
    case DW_OP_lit27:
      return "DW_OP_lit27";
    case DW_OP_lit28:
      return "DW_OP_lit28";
    case DW_OP_lit29:
      return "DW_OP_lit29";
    case DW_OP_lit30:
      return "DW_OP_lit30";
    case DW_OP_lit31:
      return "DW_OP_lit31";
    case DW_OP_reg0:
      return "DW_OP_reg0";
    case DW_OP_reg1:
      return "DW_OP_reg1";
    case DW_OP_reg2:
      return "DW_OP_reg2";
    case DW_OP_reg3:
      return "DW_OP_reg3";
    case DW_OP_reg4:
      return "DW_OP_reg4";
    case DW_OP_reg5:
      return "DW_OP_reg5";
    case DW_OP_reg6:
      return "DW_OP_reg6";
    case DW_OP_reg7:
      return "DW_OP_reg7";
    case DW_OP_reg8:
      return "DW_OP_reg8";
    case DW_OP_reg9:
      return "DW_OP_reg9";
    case DW_OP_reg10:
      return "DW_OP_reg10";
    case DW_OP_reg11:
      return "DW_OP_reg11";
    case DW_OP_reg12:
      return "DW_OP_reg12";
    case DW_OP_reg13:
      return "DW_OP_reg13";
    case DW_OP_reg14:
      return "DW_OP_reg14";
    case DW_OP_reg15:
      return "DW_OP_reg15";
    case DW_OP_reg16:
      return "DW_OP_reg16";
    case DW_OP_reg17:
      return "DW_OP_reg17";
    case DW_OP_reg18:
      return "DW_OP_reg18";
    case DW_OP_reg19:
      return "DW_OP_reg19";
    case DW_OP_reg20:
      return "DW_OP_reg20";
    case DW_OP_reg21:
      return "DW_OP_reg21";
    case DW_OP_reg22:
      return "DW_OP_reg22";
    case DW_OP_reg23:
      return "DW_OP_reg23";
    case DW_OP_reg24:
      return "DW_OP_reg24";
    case DW_OP_reg25:
      return "DW_OP_reg25";
    case DW_OP_reg26:
      return "DW_OP_reg26";
    case DW_OP_reg27:
      return "DW_OP_reg27";
    case DW_OP_reg28:
      return "DW_OP_reg28";
    case DW_OP_reg29:
      return "DW_OP_reg29";
    case DW_OP_reg30:
      return "DW_OP_reg30";
    case DW_OP_reg31:
      return "DW_OP_reg31";
    case DW_OP_breg0:
      return "DW_OP_breg0";
    case DW_OP_breg1:
      return "DW_OP_breg1";
    case DW_OP_breg2:
      return "DW_OP_breg2";
    case DW_OP_breg3:
      return "DW_OP_breg3";
    case DW_OP_breg4:
      return "DW_OP_breg4";
    case DW_OP_breg5:
      return "DW_OP_breg5";
    case DW_OP_breg6:
      return "DW_OP_breg6";
    case DW_OP_breg7:
      return "DW_OP_breg7";
    case DW_OP_breg8:
      return "DW_OP_breg8";
    case DW_OP_breg9:
      return "DW_OP_breg9";
    case DW_OP_breg10:
      return "DW_OP_breg10";
    case DW_OP_breg11:
      return "DW_OP_breg11";
    case DW_OP_breg12:
      return "DW_OP_breg12";
    case DW_OP_breg13:
      return "DW_OP_breg13";
    case DW_OP_breg14:
      return "DW_OP_breg14";
    case DW_OP_breg15:
      return "DW_OP_breg15";
    case DW_OP_breg16:
      return "DW_OP_breg16";
    case DW_OP_breg17:
      return "DW_OP_breg17";
    case DW_OP_breg18:
      return "DW_OP_breg18";
    case DW_OP_breg19:
      return "DW_OP_breg19";
    case DW_OP_breg20:
      return "DW_OP_breg20";
    case DW_OP_breg21:
      return "DW_OP_breg21";
    case DW_OP_breg22:
      return "DW_OP_breg22";
    case DW_OP_breg23:
      return "DW_OP_breg23";
    case DW_OP_breg24:
      return "DW_OP_breg24";
    case DW_OP_breg25:
      return "DW_OP_breg25";
    case DW_OP_breg26:
      return "DW_OP_breg26";
    case DW_OP_breg27:
      return "DW_OP_breg27";
    case DW_OP_breg28:
      return "DW_OP_breg28";
    case DW_OP_breg29:
      return "DW_OP_breg29";
    case DW_OP_breg30:
      return "DW_OP_breg30";
    case DW_OP_breg31:
      return "DW_OP_breg31";
    case DW_OP_regx:
      return "DW_OP_regx";
    case DW_OP_fbreg:
      return "DW_OP_fbreg";
    case DW_OP_bregx:
      return "DW_OP_bregx";
    case DW_OP_piece:
      return "DW_OP_piece";
    case DW_OP_deref_size:
      return "DW_OP_deref_size";
    case DW_OP_xderef_size:
      return "DW_OP_xderef_size";
    case DW_OP_nop:
      return "DW_OP_nop";
    case DW_OP_push_object_address:
      return "DW_OP_push_object_address";
    case DW_OP_call2:
      return "DW_OP_call2";
    case DW_OP_call4:
      return "DW_OP_call4";
    case DW_OP_calli:
      return "DW_OP_calli";
#ifdef HP_DWARF2_EXTENSIONS
      /* HP extensions */
    case DW_OP_HP_unknown:
      return "DW_OP_HP_unknown";
    case DW_OP_HP_is_value:
      return "DW_OP_HP_is_value";
    case DW_OP_HP_fltconst4:
      return "DW_OP_HP_fltconst4";
    case DW_OP_HP_fltconst8:
      return "DW_OP_HP_fltconst8";
    case DW_OP_HP_mod_range:
      return "DW_OP_HP_mod_range";
    case DW_OP_HP_unmod_range:
      return "DW_OP_HP_unmod_range";
#endif
    default:
      return "OP_<unknown>";
    }
}

static char *
dwarf_bool_name (unsigned boolval)
{
  if (boolval)
    return "TRUE";
  else
    return "FALSE";
}

/* Convert a DWARF type code into its string name.  */

static char *
dwarf_type_encoding_name (register unsigned enc)
{
  switch (enc)
    {
    case DW_ATE_address:
      return "DW_ATE_address";
    case DW_ATE_boolean:
      return "DW_ATE_boolean";
    case DW_ATE_complex_float128:
      return "DW_ATE_complex_float128";
    case DW_ATE_complex_float:
      return "DW_ATE_complex_float";
    case DW_ATE_float:
      return "DW_ATE_float";
    case DW_ATE_signed:
      return "DW_ATE_signed";
    case DW_ATE_signed_char:
      return "DW_ATE_signed_char";
    case DW_ATE_unsigned:
      return "DW_ATE_unsigned";
    case DW_ATE_unsigned_char:
      return "DW_ATE_unsigned_char";
    default:
      return "DW_ATE_<unknown>";
    }
}

/* Convert a DWARF call frame info operation to its string name. */


void
dump_die (struct die_info *die)
{
  unsigned int i;

  fprintf (stderr, "Die: %s (abbrev = 0x%x, offset = 0x%x)\n",
	   dwarf_tag_name (die->tag), die->abbrev, die->offset);
  fprintf (stderr, "\thas children: %s\n",
	   dwarf_bool_name (die->has_children));

  fprintf (stderr, "\tattributes:\n");
  for (i = 0; i < die->num_attrs; ++i)
    {
      fprintf (stderr, "\t\t%s (%s) ",
	       dwarf_attr_name (die->attrs[i].name),
	       dwarf_form_name (die->attrs[i].form));
      switch (die->attrs[i].form)
	{
	case DW_FORM_ref_addr:
	case DW_FORM_addr:
	  fprintf (stderr, "address: ");
	  print_address_numeric (DW_ADDR (&die->attrs[i]), 1, gdb_stderr);
	  break;
	case DW_FORM_block2:
	case DW_FORM_block4:
	case DW_FORM_block:
	case DW_FORM_block1:
	  fprintf (stderr, "block: size %d", DW_BLOCK (&die->attrs[i])->size);
	  break;
	case DW_FORM_data1:
	case DW_FORM_data2:
	case DW_FORM_data4:
	case DW_FORM_data8:
	case DW_FORM_ref1:
	case DW_FORM_ref2:
	case DW_FORM_ref4:
	case DW_FORM_udata:
	case DW_FORM_sdata:
	  fprintf (stderr, "constant: %ld", DW_UNSND (&die->attrs[i]));
	  break;
	case DW_FORM_string:
	case DW_FORM_strp:
	  fprintf (stderr, "string: \"%s\"",
		   DW_STRING (&die->attrs[i])
		   ? DW_STRING (&die->attrs[i]) : "");
	  break;
	case DW_FORM_flag:
	  if (DW_UNSND (&die->attrs[i]))
	    fprintf (stderr, "flag: TRUE");
	  else
	    fprintf (stderr, "flag: FALSE");
	  break;
	case DW_FORM_indirect:	/* we do not handle indirect yet */
	default:
	  fprintf (stderr, "unsupported attribute form: %d.",
		   die->attrs[i].form);
	}
      fprintf (stderr, "\n");
    }
}

void
dump_die_list (struct die_info *die)
{
  while (die)
    {
      dump_die (die);
      die = die->next;
    }
}

void
store_in_ref_table (unsigned int offset, struct die_info *die)
{
  int h;
  struct die_info *old;

  h = (offset % REF_HASH_SIZE);
  if (!processing_doom_objfile)
    {
      old = die_ref_table[h];
      die->next_ref = old;
      die_ref_table[h] = die;
    }
  else
    {
      old = debug_sections[sectn].die_ref_table[h];
      die->next_ref = old;
      debug_sections[sectn].die_ref_table[h] = die;
    }
  
}

static void
dwarf2_empty_die_ref_table ()
{
  memset (die_ref_table, 0, REF_HASH_SIZE * sizeof (struct die_info *));
}

static void
dwarf2_empty_section_die_ref_table ()
{
  memset (debug_sections[sectn].die_ref_table, 0, 
	  sizeof (debug_sections[sectn].die_ref_table));
}

static unsigned int
dwarf2_get_ref_die_offset (struct attribute *attr)
{
  unsigned int result = 0;

  switch (attr->form)
    {
    case DW_FORM_ref_addr:
      result = (unsigned int) DW_ADDR (attr);
      break;
    case DW_FORM_ref1:
    case DW_FORM_ref2:
    case DW_FORM_ref4:
    case DW_FORM_ref_udata:
      result = cu_header_offset + (unsigned int) DW_UNSND (attr);
      break;
    default:
      complain (&dwarf2_unsupported_die_ref_attr, dwarf_form_name (attr->form));
    }
  return result;
}

struct die_info *
follow_die_ref (unsigned int offset)
{
  struct die_info *die;
  int h;

  h = (offset % REF_HASH_SIZE);
  if (!processing_doom_objfile)
    die = die_ref_table[h];
  else
    die = debug_sections[sectn].die_ref_table[h];

  while (die)
    {
      if (die->offset == offset)
	{
	  return die;
	}
      die = die->next_ref;
    }
  return NULL;
}

static struct type *
dwarf2_fundamental_type (struct objfile *objfile, int typeid)
{
  if (typeid < 0 || typeid >= FT_NUM_MEMBERS)
    {
      error ("DWARF Error: internal error - invalid fundamental type id %d.",
	     typeid);
    }

  /* Look for this particular type in the fundamental type vector.  If
     one is not found, create and install one appropriate for the
     current language and the current target machine. */

  if (ftypes[typeid] == NULL)
    {
      ftypes[typeid] = cu_language_defn->la_fund_type (objfile, typeid);
    }

  return (ftypes[typeid]);
}

/* Location descriptor can be an expression or a location list.  We handle
   location lists now, by deferring evaluating them to printing time.  */

static CORE_ADDR
decode_locdesc (struct symbol *sym, struct attribute *attr, struct objfile *objfile,
		unsigned int *debug_location_offset)
{
  if (   attr->form == DW_FORM_data4
      || attr->form == DW_FORM_data8)
    {
      /* is a location list, defer computing address here */
      ispiece = 0;
      isreg = 0;
      offreg = 0;
      isderef = 0;
      islocal = 0;
      *debug_location_offset = 0;
      isreference = 0;
#ifdef HP_DWARF2_EXTENSIONS
      istls = 0;
#endif
      optimized_out = 0;
      return 0;
    }
  else
   return decode_locexpr (sym, DW_BLOCK (attr), objfile, debug_location_offset);
  
}

/* Decode simple location descriptions.
   Given a pointer to a dwarf block that defines a location, compute
   the location and return the value.

   FIXME: This is a kludge until we figure out a better
   way to handle the location descriptions.
   Gdb's design does not mesh well with the DWARF2 notion of a location
   computing interpreter, which is a shame because the flexibility goes unused.
   FIXME: Implement more operations as necessary.

   A location description containing no operations indicates that the
   object is optimized out. The global optimized_out flag is set for
   those, the return value is meaningless.

   When the result is a register number, the global isreg flag is set,
   otherwise it is cleared.

   When the result is a base register offset, the global offreg flag is set
   and the register number is returned in basereg, otherwise it is cleared.

   When the DW_OP_fbreg operation is encountered without a corresponding
   DW_AT_frame_base attribute, the global islocal flag is set.
   Hopefully the machine dependent code knows how to set up a virtual
   frame pointer for the local references.

   Note that stack[0] is unused except as a default error return.
   Note that stack overflow is not yet handled.  */

static CORE_ADDR
decode_locexpr (struct symbol *sym, struct dwarf_block *blk, struct objfile *objfile,
		unsigned int *debug_location_offset)
{
  int i;
  int size = blk->size;
  char *data = blk->data;
  unsigned int bytes_read;
  unsigned char op;
  int is_piece_on_stack;	/* boolean */

  int 		nbr_pieces;
  int 		piece_size;
  int 		total_size;
  CORE_ADDR 	unsnd;
  CORE_ADDR 	value;
  static int    stacki;		/* index of the top-of-stack, stack grows up */
  static CORE_ADDR stack[64];	/* Used to return pieces */

  ispiece = 0;
  is_piece_on_stack = 0;
  i = 0;
  stacki = 0;
  stack[stacki] = 0;
  isreg = 0;
  offreg = 0;
  isderef = 0;
  islocal = 0;
  optimized_out = 1;
  *debug_location_offset = 0;
  isreference = 0;
#ifdef HP_DWARF2_EXTENSIONS
  istls = 0;
#endif

  while (i < size)
    {
      optimized_out = 0;
      op = data[i++];
      switch (op)
	{
        case DW_OP_lit0:
        case DW_OP_lit1:
        case DW_OP_lit2:
        case DW_OP_lit3:
        case DW_OP_lit4:
        case DW_OP_lit5:
        case DW_OP_lit6:
        case DW_OP_lit7:
        case DW_OP_lit8:
        case DW_OP_lit9:
        case DW_OP_lit10:
        case DW_OP_lit11:
        case DW_OP_lit12:
        case DW_OP_lit13:
        case DW_OP_lit14:
        case DW_OP_lit15:
        case DW_OP_lit16:
        case DW_OP_lit17:
        case DW_OP_lit18:
        case DW_OP_lit19:
        case DW_OP_lit20:
        case DW_OP_lit21:
        case DW_OP_lit22:
        case DW_OP_lit23:
        case DW_OP_lit24:
        case DW_OP_lit25:
        case DW_OP_lit26:
        case DW_OP_lit27:
        case DW_OP_lit28:
        case DW_OP_lit29:
        case DW_OP_lit30:
        case DW_OP_lit31:
          stack[++stacki] = op - DW_OP_lit0;
          break;

	case DW_OP_reg0:
	case DW_OP_reg1:
	case DW_OP_reg2:
	case DW_OP_reg3:
	case DW_OP_reg4:
	case DW_OP_reg5:
	case DW_OP_reg6:
	case DW_OP_reg7:
	case DW_OP_reg8:
	case DW_OP_reg9:
	case DW_OP_reg10:
	case DW_OP_reg11:
	case DW_OP_reg12:
	case DW_OP_reg13:
	case DW_OP_reg14:
	case DW_OP_reg15:
	case DW_OP_reg16:
	case DW_OP_reg17:
	case DW_OP_reg18:
	case DW_OP_reg19:
	case DW_OP_reg20:
	case DW_OP_reg21:
	case DW_OP_reg22:
	case DW_OP_reg23:
	case DW_OP_reg24:
	case DW_OP_reg25:
	case DW_OP_reg26:
	case DW_OP_reg27:
	case DW_OP_reg28:
	case DW_OP_reg29:
	case DW_OP_reg30:
	case DW_OP_reg31:
	  isreg = 1;
	  stack[++stacki] = op - DW_OP_reg0;
	  break;

	case DW_OP_regx:
	  isreg = 1;
	  unsnd = read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;
#if defined(HARRIS_TARGET) && defined(_M88K)
	  /* The Harris 88110 gdb ports have long kept their special reg
	     numbers between their gp-regs and their x-regs.  This is
	     not how our dwarf is generated.  Punt. */
	  unsnd += 6;
#endif
	  stack[++stacki] = unsnd;
	  break;


	case DW_OP_breg0:
	case DW_OP_breg1:
	case DW_OP_breg2:
	case DW_OP_breg3:
	case DW_OP_breg4:
	case DW_OP_breg5:
	case DW_OP_breg6:
	case DW_OP_breg7:
	case DW_OP_breg8:
	case DW_OP_breg9:
	case DW_OP_breg10:
	case DW_OP_breg11:
	case DW_OP_breg12:
	case DW_OP_breg13:
	case DW_OP_breg14:
	case DW_OP_breg15:
	case DW_OP_breg16:
	case DW_OP_breg17:
	case DW_OP_breg18:
	case DW_OP_breg19:
	case DW_OP_breg20:
	case DW_OP_breg21:
	case DW_OP_breg22:
	case DW_OP_breg23:
	case DW_OP_breg24:
	case DW_OP_breg25:
	case DW_OP_breg26:
	case DW_OP_breg27:
	case DW_OP_breg28:
	case DW_OP_breg29:
	case DW_OP_breg30:
	case DW_OP_breg31:
	  offreg = 1;
	  basereg = op - DW_OP_breg0;
	  stack[++stacki] = read_signed_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;
	  break;

	case DW_OP_bregx:
	  offreg = 1;
	  basereg = (unsigned int) read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;
	  stack[++stacki] = read_signed_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;
	  break;

	case DW_OP_fbreg:
	  stack[++stacki] = read_signed_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;
	  if (frame_base_reg >= 0)
	    {
	      offreg = 1;
	      basereg = frame_base_reg;
	      stack[stacki] += frame_base_offset;
	    }
	  else
	    {
	      complain (&dwarf2_missing_at_frame_base);
	      islocal = 1;
	    }
	  break;

	case DW_OP_addr:
	  stack[++stacki] = read_address (objfile->obfd, &data[i]);
	  i += address_size;
	  break;

	case DW_OP_const1u:
	  stack[++stacki] = read_1_byte (objfile->obfd, &data[i]);
	  i += 1;
	  break;

	case DW_OP_const1s:
	  stack[++stacki] = read_1_signed_byte (objfile->obfd, &data[i]);
	  i += 1;
	  break;

	case DW_OP_const2u:
	  stack[++stacki] = read_2_bytes (objfile->obfd, &data[i]);
	  i += 2;
	  break;

	case DW_OP_const2s:
	  stack[++stacki] = read_2_signed_bytes (objfile->obfd, &data[i]);
	  i += 2;
	  break;

	case DW_OP_const4u:
	  stack[++stacki] = read_4_bytes (objfile->obfd, &data[i]);
	  i += 4;
	  break;

	case DW_OP_const4s:
	  stack[++stacki] = read_4_signed_bytes (objfile->obfd, &data[i]);
	  i += 4;
	  break;

	case DW_OP_constu:
	  stack[++stacki] = read_unsigned_leb128 (NULL, (data + i),
						  &bytes_read);
	  i += bytes_read;
	  break;

	case DW_OP_consts:
	  stack[++stacki] = read_signed_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;
	  break;

        case DW_OP_dup:
          stack[stacki + 1] = stack[stacki];
          stacki++;
          break;

	case DW_OP_plus:
	  stack[stacki - 1] += stack[stacki];
	  stacki--;
	  break;

	case DW_OP_plus_uconst:
	  if (isderef) 
	    {
	      *debug_location_offset = 	  
		(unsigned int) read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	      i += bytes_read;
	    }
	  else
	    {
	      stack[stacki] += 
		read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	      i += bytes_read;
	    }
	  break;

	case DW_OP_minus:
	  stack[stacki - 1] -= stack[stacki];
	  stacki--;
	  break;
	
	case DW_OP_nop:
	  break;
	
	case DW_OP_piece:

	  /* top-of-stack is something that we are taking a piece of.  Pop
	   * it off for the moment.  
	   */

	  value = stack[stacki];  
	  stacki--;

	  /* DW_OP_piece takes one argument an uLEB128 size of the piece */

	  piece_size = (unsigned int) read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	  i += bytes_read;

	  /* If a piece is on the stack the stack will look like:
	   *	total_size_of_pieces  [ top-of-stack ]
	   *    number_of_pieces
	   *	   type, one of PIECE_LOC_REGISTER, PIECE_LOC_BASEADDR, 
	   *	     PIECE_LOC_STATIC
	   *	   size of this piece in bytes
	   *	   address_or_register_number
	   *	   for PIECE_LOC_BASEADDR one more word for the offset
	   *    ... repeat 3-4 words for each piece
	   */

	  if (is_piece_on_stack)
	    { /* Pop off the total size and the total number of pieces */
	      total_size = (int) stack[stacki];
	      stacki--;
	      nbr_pieces = (int) stack[stacki];
	      stacki--;
	    }
	  else {
	    total_size = 0;
	    nbr_pieces = 0;
	  }
	  is_piece_on_stack = 1;

	  /* Push this piece onto the stack.  If offreg is true, then
	   * we push value and then basereg, otherwise just the value.
	   */

	  stack[++stacki] = value;
	  if (offreg)
	    stack[++stacki] = basereg;

	  stack[++stacki] = piece_size;  
	  if (offreg)
	    stack[++stacki] = PIECE_LOC_BASEREG;
	  else if (isreg)
	    stack[++stacki] = PIECE_LOC_REGISTER;
	  else 
	    stack[++stacki] = PIECE_LOC_STATIC;
	  offreg = 0;	
	  isreg = 0;


	  /* Add this piece to the total, increment the number of pieces
	   * and store them on the stack 
	   */


	  stack[++stacki] = ++nbr_pieces;
	  stack[++stacki] = total_size + piece_size;  
	  ispiece = 1;
	  break;

	case DW_OP_deref:
	  isreference = 1;
	  isderef = 1;
	  break;
	  
#ifdef HP_DWARF2_EXTENSIONS
	  /* HP extensions */
	case DW_OP_HP_mod_range:
	  {
	    unsigned int from_offset, to_offset;
	    from_offset = (unsigned int) read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	    i += bytes_read;
	    to_offset = (unsigned int) read_unsigned_leb128 (NULL, (data + i), &bytes_read);
	    i += bytes_read;
	    if (current_func_lowpc)
	      {
		CORE_ADDR mod_range_startaddr;
		CORE_ADDR mod_range_endaddr;
		mod_range_startaddr =
		  SWIZZLE(ADJUST_IA64_SLOT_ENCODING (current_func_lowpc + from_offset));
		mod_range_endaddr =
		  SWIZZLE(ADJUST_IA64_SLOT_ENCODING (current_func_lowpc + to_offset));
		if (no_of_mod_ranges >= allocated_mod_ranges)
		  { 
		    allocated_mod_ranges =   2 * allocated_mod_ranges 
				  + INITIAL_MOD_RANGES_PER_VARIABLE;
		    mod_ranges = xrealloc (mod_ranges, 
					    allocated_mod_ranges 
					  * sizeof (struct 
						      gdb_hp_mod_unmod_range));
		  }
		  no_of_mod_ranges += 1;
		  mod_ranges[no_of_mod_ranges - 1].low_addr = mod_range_startaddr;
		  mod_ranges[no_of_mod_ranges - 1].hi_addr = mod_range_endaddr;
	      }
	    else
	      {
		/* FIXME: Poorva: Changed to a complaint for now */
		complain (&dwarf2_func_lowpc_not_set_for_mod_range);
		/* warning ("Dwarf2reader: 'current_func_lowpc' not set when \
DW_OP_HP_mod_range was seen."); */
	      }
	    break;
	  }
	case DW_OP_HP_tls:
	  {
	    istls = 1;
	    stack[++stacki] = read_address(objfile->obfd, &data[i]);
            i += address_size;
            break;
	  }
	  /* Let the following cases fall into default for now */
	case DW_OP_HP_unknown:
	case DW_OP_HP_is_value:
	case DW_OP_HP_fltconst4:
	case DW_OP_HP_fltconst8:
	case DW_OP_HP_unmod_range:
#endif /* HP_DWARF2_EXTENSIONS */

	default:
	  complain (&dwarf2_unsupported_stack_op, dwarf_stack_op_name (op));
	  return (stack[stacki]);
	}
    }
  assert (stacki >= 0);
  if (!ispiece)
    return (stack[stacki--]);
  else
    {
      if (sym)
	init_sym_pieces (objfile, sym, stacki, stack);
      stacki = 0;
      isreg = 1;
      offreg = 1; /* if 0 and istls, then though optimized out */
      basereg = 0;
      if (sym)
	return SYMBOL_VALUE_ADDRESS(sym);   /* Return full CORE_ADDR */
      else
	return 0;
    }
}
#ifdef HP_DWARF2_EXTENSIONS
static void
set_modifiability_information (struct symbol *sym)
{
  int i;

  SYMBOL_MODIFIABILITY (sym) = HP_selectively_modifiable;

  SYMBOL_MOD_UNMOD_RANGES (sym) =
    (struct gdb_hp_mod_unmod_ranges *)
    xmalloc (sizeof (struct gdb_hp_mod_unmod_ranges) +
	       (no_of_mod_ranges - 1) *
	     sizeof (struct gdb_hp_mod_unmod_range));
  (SYMBOL_MOD_UNMOD_RANGES (sym))->nitems = no_of_mod_ranges;
  for (i = 0; i < no_of_mod_ranges; i++)
    {
      (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i] = mod_ranges[i];
    }
}

void
print_hp_symbol_modifiability (struct symbol *sym, struct ui_file *stream)
{
  int i;
  int nitems = (SYMBOL_MOD_UNMOD_RANGES (sym))->nitems;
  if (!nitems)
    return;
  fprintf_filtered (stream, "Modifiability ranges for '%s': \n",
		    SYMBOL_NAME (sym));
  for (i = 0; i < nitems; i++)
    {
      CORE_ADDR low_addr;
      CORE_ADDR hi_addr;

      low_addr = (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i].low_addr;
      hi_addr = (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i].hi_addr;
      fprintf_filtered (stream, "\tStart addr: ");
      print_address_numeric (low_addr, 1, stream);
      fprintf_filtered (stream, "\n");
      fprintf_filtered (stream, "\tEnd addr: ");
      print_address_numeric (hi_addr, 1, stream);
      fprintf_filtered (stream, "\n");
    }
}
#endif /* HP_DWARF2_EXTENSIONS */

/* memory allocation interface */

/* ARGSUSED */
static void
dwarf2_free_tmp_obstack (PTR ptr)
{
#ifdef HP_IA64
  if (ptr)
    memcpy (&((struct partial_symtab *)(void *)ptr)->dwarf2_tmp_obstack, 
            &dwarf2_tmp_obstack, sizeof (struct obstack));
  else
#endif
    obstack_free (&dwarf2_tmp_obstack, NULL);
}

/* ARGSUSED */
static void
dwarf2_free_tmp_obstack_2 (PTR ignore)
{
  obstack_free (&dwarf2_tmp_obstack_2, NULL);
}

static struct dwarf_block *
dwarf_alloc_block ()
{
  struct dwarf_block *blk;

  blk = (struct dwarf_block *)
    (void *) obstack_alloc (&dwarf2_tmp_obstack, sizeof (struct dwarf_block));
  return (blk);
}

static struct abbrev_info *
dwarf_alloc_abbrev ()
{
  struct abbrev_info *abbrev;

  abbrev = (struct abbrev_info *) xcalloc (1, sizeof (struct abbrev_info));

  return (abbrev);
}

static struct die_info *
dwarf_alloc_die ()
{
  struct die_info *die;

  die = (struct die_info *) xcalloc (1, sizeof (struct die_info));
  return (die);
}


/* Check if the attribute's form is a DW_FORM_block*
   if so return true else false. */
static int
attr_form_is_block (struct attribute *attr)
{
  return (attr == NULL ? 0 :
      attr->form == DW_FORM_block1
      || attr->form == DW_FORM_block2
      || attr->form == DW_FORM_block4
      || attr->form == DW_FORM_block);
}


static void
dwarf2_symbol_mark_computed (struct attribute *attr, struct symbol *sym,
			     const struct comp_unit_head *cu_header,
			     struct objfile *objfile)
{

  /* Save the master objfile, so that we can report and look up the
     correct file containing this variable.  */
  if (objfile->separate_debug_objfile_backlink)
    objfile = objfile->separate_debug_objfile_backlink;

  if ((attr->form == DW_FORM_data4 || attr->form == DW_FORM_data8)
      /* ".debug_loc" may not exist at all, or the offset may be outside
	 the section.  If so, fall through to the complaint in the
	 other branch.  */
      && (DW_UNSND (attr) < dwarf_loc_size))
    {
      struct dwarf2_loclist_baton *baton;

      baton = (struct dwarf2_loclist_baton *)
		obstack_alloc (&objfile->symbol_obstack,
			       sizeof (struct dwarf2_loclist_baton));
      baton->objfile = objfile;

      /* We don't know how long the location list is, but make sure we
	 don't run off the edge of the section.  */
      baton->size = dwarf_loc_size - DW_UNSND (attr);
      baton->data = (unsigned char *) dwarf_loc_buffer + DW_UNSND (attr);
      baton->base_address = cu_header->base_address;
      if (cu_header->base_known == 0)
	complain (&dwarf2_loc_list_but_no_cu_base_addr);

      SYMBOL_LOCATION_FUNCS (sym) = &dwarf2_loclist_funcs;
      SYMBOL_LOCATION_BATON (sym) = baton;
    }
  else
    {
      struct dwarf2_locexpr_baton *baton;

      baton = (struct dwarf2_locexpr_baton *) 
		obstack_alloc (&objfile->symbol_obstack,
			       sizeof (struct dwarf2_locexpr_baton));
      baton->objfile = objfile;

      if (attr_form_is_block (attr))
	{
	  /* Note that we're just copying the block's data pointer
	     here, not the actual data.  We're still pointing into the
	     dwarf_info_buffer for SYM's objfile; right now we never
	     release that buffer, but when we do clean up properly
	     this may need to change.  */
	  baton->size = DW_BLOCK (attr)->size;
	  baton->data = (unsigned char *) DW_BLOCK (attr)->data;
	}
      else
	{
	  complain (&dwarf2_invalid_attrib_location_description,
						 SYMBOL_NAME (sym));
	  baton->size = 0;
	  baton->data = NULL;
	}
      
      SYMBOL_LOCATION_FUNCS (sym) = &dwarf2_locexpr_funcs;
      SYMBOL_LOCATION_BATON (sym) = baton;
    }
}

/* Get line table attribute from comp unit die */
int
get_line_offset (struct partial_die_info *part_die, bfd *abfd,
		 unsigned int *offset, const struct comp_unit_head *cu_header)
{
  char *info_ptr=part_die->offset + dwarf_info_buffer_procs;
	/* Uses the pointer of the procs_info buffer */
  unsigned int abbrev_number, bytes_read;
  struct abbrev_info *abbrev;
  struct attribute attr;
  int i;

  abbrev_number = (unsigned int) read_unsigned_leb128 (abfd, info_ptr, &bytes_read);
  info_ptr += bytes_read;
  if (!abbrev_number)
    return 0;

  abbrev = dwarf2_lookup_abbrev (abbrev_number, cu_header);
  if (!abbrev)
    {
      error ("DWARF Error: Could not find abbrev number %d.", abbrev_number);
    }

  for (i = 0; i < abbrev->num_attrs; ++i)
    {
      info_ptr = read_attribute (&attr, &abbrev->attrs[i], abfd,
				 info_ptr, cu_header);

      /* Store the data if it is of an attribute we want to keep in a
         partial symbol table.  */
      switch (attr.name)
	{
	case DW_AT_stmt_list:
	  *offset = (unsigned int) DW_UNSND (&attr);
	  return 1;
	default:
	  break;
	}
    }
  return 0;

}

/* Clone of dwarf_decode_lines used to get file name from line table */
static char *
get_filename_from_linetable (unsigned int offset, struct objfile *objfile)
{
  char *line_ptr;
  char *line_end;
  struct line_head lh;
  struct cleanup *back_to;
  unsigned int i, bytes_read;
  char *cur_file, *cur_dir;
  unsigned char op_code, extended_op, adj_opcode;
  bfd *abfd = objfile->obfd ;

#define FILE_ALLOC_CHUNK 5
#define DIR_ALLOC_CHUNK 5

  struct filenames files;
  struct directories dirs;

  if (dwarf_line_buffer == NULL)
    {
      return NULL;
    }

  files.num_files = 0;
  files.files = NULL;

  dirs.num_dirs = 0;
  dirs.dirs = NULL;

  line_ptr = dwarf_line_buffer + offset;

  /* read in the prologue */
  lh.total_length = read_4_bytes (abfd, line_ptr);
  line_ptr += 4;
  line_end = line_ptr + lh.total_length;
  lh.version = read_2_bytes (abfd, line_ptr);
  line_ptr += 2;
  lh.prologue_length = read_4_bytes (abfd, line_ptr);
  line_ptr += 4;
  lh.minimum_instruction_length = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.default_is_stmt = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.line_base = read_1_signed_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.line_range = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.opcode_base = read_1_byte (abfd, line_ptr);
  line_ptr += 1;
  lh.standard_opcode_lengths = (unsigned char *)
    xmalloc (lh.opcode_base * sizeof (unsigned char));
  back_to = make_cleanup (free_current_contents, &lh.standard_opcode_lengths);

  lh.standard_opcode_lengths[0] = 1;
  for (i = 1; i < lh.opcode_base; ++i)
    {
      lh.standard_opcode_lengths[i] = read_1_byte (abfd, line_ptr);
      line_ptr += 1;
    }

  /* Read directory table  */
  while ((cur_dir = read_string (abfd, line_ptr, &bytes_read)) != NULL)
    {
      line_ptr += bytes_read;
      if ((dirs.num_dirs % DIR_ALLOC_CHUNK) == 0)
	{
	  dirs.dirs = (char **)
	    xrealloc (dirs.dirs,
		      (dirs.num_dirs + DIR_ALLOC_CHUNK) * sizeof (char *));
	  if (dirs.num_dirs == 0)
	    make_cleanup (free_current_contents, &dirs.dirs);
	}
      dirs.dirs[dirs.num_dirs++] = cur_dir;
    }
  line_ptr += bytes_read;

  /* Read file name table */
  while ((cur_file = read_string (abfd, line_ptr, &bytes_read)) != NULL)
    {
      line_ptr += bytes_read;
      if ((files.num_files % FILE_ALLOC_CHUNK) == 0)
	{
	  files.files = (struct fileinfo *)
	    xrealloc (files.files,
		      (files.num_files + FILE_ALLOC_CHUNK)
		      * sizeof (struct fileinfo));
	  if (files.num_files == 0)
	    make_cleanup (free_current_contents, &files.files);
	}
      files.files[files.num_files].name = cur_file;
      files.files[files.num_files].dir =
	(unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
      line_ptr += bytes_read;
      files.files[files.num_files].time =
	(unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
      line_ptr += bytes_read;
      files.files[files.num_files].size =
	(unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
      line_ptr += bytes_read;
      files.num_files++;
    }
  line_ptr += bytes_read;

  /* Read the statement sequences until there's nothing left.  */
  while (line_ptr < line_end)
    {
      /* state machine registers  */
      CORE_ADDR address = 0;
      unsigned int file = 1;
      unsigned int line = 1;
      int is_stmt = lh.default_is_stmt;
      int end_sequence = 0;
#ifdef HP_DWARF2_EXTENSIONS
      int is_UV_update = 0;
      int post_semantics = 0;
      int function_exit = 0;
      int is_front_end_logical = 0; 
#endif

      /* Start a subfile for the current file of the state machine.  */
      if (files.num_files >= file)
	{
	  /* The file and directory tables are 0 based, the references
	     are 1 based.  */
              do_cleanups (back_to);
	      return obsavestring(files.files[file - 1].name,
		(int) strlen(files.files[file - 1].name), &objfile->psymbol_obstack);
	}

      /* Decode the table. */
      while (!end_sequence)
	{
	  op_code = read_1_byte (abfd, line_ptr);
	  line_ptr += 1;
	  switch (op_code)
	    {
	    case DW_LNS_extended_op:
	      (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      extended_op = read_1_byte (abfd, line_ptr);
	      line_ptr += 1;
	      switch (extended_op)
		{
		case DW_LNE_end_sequence:
		  end_sequence = 1;
		  break;
		case DW_LNE_set_address:
		  address = read_address (abfd, line_ptr) + baseaddr;
		  line_ptr += address_size;
		  break;
		case DW_LNE_define_file:
		  cur_file = read_string (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  if ((files.num_files % FILE_ALLOC_CHUNK) == 0)
		    {
		      files.files = (struct fileinfo *)
			xrealloc (files.files,
				  (files.num_files + FILE_ALLOC_CHUNK)
				  * sizeof (struct fileinfo));
		      if (files.num_files == 0)
			make_cleanup (free_current_contents, &files.files);
		    }
		  files.files[files.num_files].name = cur_file;
		  files.files[files.num_files].dir =
		    (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  files.files[files.num_files].time =
		    (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  files.files[files.num_files].size =
		    (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  files.num_files++;
		  break;
#ifdef HP_DWARF2_EXTENSIONS
		/* FIXME: this is just a skeletal implementation of the
		   linetable changes to support inlining and optimized code;
		   these changes allow these new opcodes to be recognized so
		   that gdb will not crash. */
		case DW_LNE_HP_negate_is_UV_update:
		  is_UV_update = (!is_UV_update);
		  break;
		case DW_LNE_HP_push_context:
                  context_stack += 1;
		  break;
		case DW_LNE_HP_pop_context:
                  context_stack -= 1;
		  break;
		case DW_LNE_HP_set_file_line_column:
		  file = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  line = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_set_routine_name:
		  (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_set_sequence:
		  (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
		case DW_LNE_HP_negate_post_semantics:
		  post_semantics = (!post_semantics);
		  break;
		case DW_LNE_HP_negate_function_exit:
		  function_exit = (!function_exit);
		  break;
		case DW_LNE_HP_negate_front_end_logical:
		  is_front_end_logical = (!is_front_end_logical);
		  break;
		case DW_LNE_HP_define_proc:
		  (void) read_string (abfd, line_ptr, &bytes_read);
		  line_ptr += bytes_read;
		  break;
#endif /* HP_DWARF2_EXTENSIONS */
		default:
  		  do_cleanups (back_to);
		  return NULL;
		}
	      break;
	    case DW_LNS_copy:
	      break;
	    case DW_LNS_advance_pc:
	      address += lh.minimum_instruction_length
		* read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_advance_line:
	      line += read_signed_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_set_file:
	      /* The file and directory tables are 0 based, the references
	         are 1 based.  */
	      file = (unsigned int) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
              do_cleanups (back_to);
	      return obsavestring(files.files[file - 1].name,
		(int) strlen(files.files[file - 1].name), &objfile->psymbol_obstack);
	    case DW_LNS_set_column:
	      (void) read_unsigned_leb128 (abfd, line_ptr, &bytes_read);
	      line_ptr += bytes_read;
	      break;
	    case DW_LNS_negate_stmt:
	      is_stmt = (!is_stmt);
	      break;
	    case DW_LNS_set_basic_block:
	      break;
	    case DW_LNS_const_add_pc:
	      address += (lh.minimum_instruction_length
			  * ((255 - lh.opcode_base) / lh.line_range));
	      break;
	    case DW_LNS_fixed_advance_pc:
	      address += read_2_bytes (abfd, line_ptr);
	      line_ptr += 2;
	      break;
	    default:		/* special operand */
	      adj_opcode = op_code - lh.opcode_base;
	      address += (adj_opcode / lh.line_range)
		* lh.minimum_instruction_length;
	      line += lh.line_base + (adj_opcode % lh.line_range);
	      /* append row to matrix using current values */
	    }
	}
    }
  do_cleanups (back_to);
  return NULL;
}

/* make a partial symbol table using procs */
struct partial_symtab *
procs_start_psymtab_common (struct objfile *objfile,
	struct section_offsets * section_offsets,   
	struct partial_die_info *comp_unit_die,
	const struct comp_unit_head *cu_header)
{
  struct partial_symtab *ps;
  char *name;
  unsigned int offset;

  /* Look for the name and whether the file was compiled with -g */
  if (comp_unit_die->name)
    {
      name = obsavestring ( comp_unit_die->name, (int) strlen (comp_unit_die->name),
		&objfile->psymbol_obstack);
      if (strstr (comp_unit_die->prof_flags, "g"))
	return NULL; /* compiled with -g, ignore procs_info */
    }
  else
    {
      /* old compiler, use indirect way */
      /* Get start of line table for this file */
      if ( get_line_offset( comp_unit_die, objfile->obfd, &offset, cu_header)
									== 0)
        return NULL;
      name = get_filename_from_linetable( offset, objfile);
      if (!name)
        return NULL;
      /* Check if we already have a psymtab for this file */
      for (ps = objfile->psymtabs; ps ; ps = ps->next)
      if (STREQ (name, ps->filename))
	{
          return NULL;
        }
    }
  /* Build a psymtab */
  ps = start_psymtab_common (objfile, section_offsets, name ,
				  comp_unit_die->dirname , 0,
				  objfile->global_psymbols.next,
				  objfile->static_psymbols.next);
  return(ps);
}

/* function to set src_no_g option */
void 
set_src_no_g (char * arg)
{
  int i;
  for (i=0; src_no_g_mode_kind_names[i]!=NULL; i++)
    if (!strcmp(arg, src_no_g_mode_kind_names[i]))
      {
        src_no_g_mode_string = src_no_g_mode_kind_names[i];
	return;
      }
  return;
}

void
set_src_no_g_sfunc (char * arg, int from_tty, struct cmd_list_element *c)
{
  /* This function gets called for both set and show commands.
     arg will be NULL for show command. */
  if (!arg)
    return;
  if (!strcmp (arg, "no_sys_libs") || !strcmp (arg, "all"))
    printf_filtered ("Source information will be available for the libraries and executables that\n"
     "will be loaded from now on. If you want it to be available for all load modules,\n"
     "issue this command before the file command, or use the -src_no_g option on the command line\n");
  return;
}

/* Function to set inline-debug from command line */
void set_inline_debug (char * arg)
{

  if (!strcmp (arg, "off"))
    inline_debugging = OFF;
  else if (!strcmp (arg, "on"))
    inline_debugging = ON;
#ifdef HP_IA64  
  else if (!strcmp (arg, "inline_bp_all"))
    inline_debugging = BP_ALL;
  else if (!strcmp (arg, "inline_bp_individual")) 
    inline_debugging = BP_IND;
#else 
  else if ((!strcmp (arg, "inline_bp_all")) || 
           (!strcmp (arg, "inline_bp_individual")))
    {
      inline_debugging = ON;
      inl_debug = "on";
      printf_filtered ("inline-debug inline_bp_all/inline_bp_individual "
                       "is not supported on PA \n"
                       "Setting inline-debug to 'on' now");
    }
#endif
  return;
}    

/* Function to set inline-debug */
void set_inline_debug_sfunc (char * arg, int from_tty, struct cmd_list_element *c)
{
  if (!arg)
    return;
#ifdef HP_IA64
  /* When command completion is used then we get an extra space
     appended to the argument, so we need to check 
     with space and without space.
  */
  if ((!strcmp (arg, "inline_bp_all") || !strcmp (arg, "inline_bp_all ")
       || !strcmp (arg, "inline_bp_individual") || !strcmp (arg, "inline_bp_individual "))
       && (have_full_symbols () || have_partial_symbols ()))
    {
      warning ("inline-debug inline_bp_all/inline_bp_individual can be set only "
                       "before loading the program.\n"
                       "Setting inline-debug to 'on' now\n");
      inline_debugging = ON;
      inl_debug = "on";
      return;
    }
#else
   if ((!strcmp (arg, "inline_bp_all")) || (!strcmp (arg, "inline_bp_all "))
        || (!strcmp (arg, "inline_bp_individual")) || 
       (!strcmp (arg, "inline_bp_individual ")))
     {
       warning ("inline-debug inline_bp_all/inline_bp_individual is "
                "not supported on PA\n"
                "Setting inline-debug to 'on' now");
       inline_debugging = ON;
       inl_debug = "on";
     }
#endif
  
  if (!strcmp (arg, "off"))
    inline_debugging = OFF;
  else if (!strcmp (arg, "on"))
    inline_debugging = ON;
#ifdef HP_IA64
  else if ((!strcmp (arg, "inline_bp_all")) ||
           (!strcmp (arg, "inline_bp_all ")))
    inline_debugging = BP_ALL;
  else if (!strcmp (arg, "inline_bp_individual") || !strcmp (arg, "inline_bp_individual "))
    inline_debugging = BP_IND;
#endif
  return;
}

/* Initialize set variable for src-no-g */
void
_initialize_dwarf2read ()
{
  struct cmd_list_element *c;
  c = add_set_enum_cmd ("src-no-g",
                        class_support,
                        (const char **) src_no_g_mode_kind_names,
                        (const char **) &src_no_g_mode_string,
/* rven: Enable source level debugging without compiling with -g
   using minimal line tables. Several levels possible.
 */
   "Set debugger response to source level debugging without compiling with -g .\n\n\
Usage:\nTo set new value:\n\tset src-no-g <OPTION>\n\
To see current value:\n\tshow src-no-g\n\n\
Limited source level debugging is available using minimal line tables.\n\
<OPTION> can be:\n\
  none        - No source level debugging possible without -g.\n\
  no_sys_libs - For all source files except those in system libraries.\n\
  all         - For all source files.\n\
By default, the debugger will provide source level debugging without \n\
compiling with -g for all source files except those in system libraries.",
                        &setlist);
  c->function.sfunc = set_src_no_g_sfunc;
  add_show_from_set (c, &showlist);

/* JAGae23887 - add old-vtable flag to allow users to use gdb with files 
   that have been compiled with an older compiler that use the older values
   for DW_VIRTUALITY */
  c = add_set_enum_cmd ("old-vtable",
                        class_support,
                        (const char **) old_vtable_mode_kind_names,
                        (const char **) &old_vtable,
                        "Set debugger to use old DW_VIRTUALITY values (on/off).\n\n"
                        "Usage:\nTo set new value:\n\tset old-vtable on | off\nTo see"
                        " current value:\n\tshow old-vtable\n",
                        &setlist);
  add_show_from_set (c, &showlist);

  c = add_set_enum_cmd ("inline-debug", class_support, (const char **) inline_debug_type,
                        (const char **) &inl_debug,
                  "Set debugging of inline code. Default is on.\n\n\
Usage:\nTo set new value:\n\tset inline-debug <OPTION>\nTo see current value:\
\n\tshow inline-debug\n\n<OPTION> can be:\n\
      off                  - Inline debugging is off. \n\
      on	           - Inline debugging is on. You can perform step, next,\n\
                             backtrace, etc, but you cannot place breakpoints \n\
                             on inline functions. \n\
      inline_bp_all        - Valid only on Itanium2 platforms. You can perform step, next, \n\
                             backtrace, etc and place breakpoints on inline functions. \n\
                             The breakpoints on all the instances of an inline function \n\
                             are treated as a single breakpoint. \n\
      inline_bp_individual - Valid only on Itanium2 platforms. You can perform step, next, \n\
                             backtrace, etc and place breakpoints on inline functions. \n\
                             The breakpoints on all the instances of an inline function \n\
                             are treated as individual breakpoints.",
                       &setlist);
  c->function.sfunc = set_inline_debug_sfunc;
  add_show_from_set (c, &showlist);

}

/* Macro support implementation. */
#ifdef HP_IA64

struct macro_table *pending_macros = NULL;

/* Return the full name of file number I in *LH's file name table.
   Use COMP_DIR as the name of the current directory of the
   compilation.  The result is allocated using xmalloc; the caller is
   responsible for freeing it.  */
static char *
file_full_name (int file, const char *comp_dir)
{
  /* Is the file number a valid index into the line header's file name
     table?  Remember that file numbers start with one, not zero.  */
  if (1 <= file && file <= gfiles.num_files)
    {
      struct fileinfo fe = gfiles.files[file-1];
      const char *dir;
      int dir_len;
      char *full_name;

      /* Fetch the dir name using the directory index. */
      if (fe.dir)
        dir = gdirs.dirs[fe.dir - 1];
      else
        dir = comp_dir;

      /* Generate the absolute path name of the file. */
      if (dir)
        {
          dir_len = strlen (dir);
          full_name = xmalloc (dir_len + 1 + strlen (fe.name) + 1);
          strcpy (full_name, dir);
          full_name[dir_len] = '/';
          strcpy (full_name + dir_len + 1, fe.name);
          return full_name;
        }
      else
        return xstrdup (fe.name);
    }
  else
    {
      /* The compiler produced a bogus file number.  We can at least
         record the macro definitions made in the file, even if we
         won't be able to find the file by name.  */
      char fake_name[80];
      sprintf (fake_name, "<bad macro file number %d>", file);

      complain (&macro_general_complaint,
                "bad file number in macro information (%d)",
                 file); 

      return xstrdup (fake_name);
    }
}

static struct macro_source_file *
macro_start_file (int file, int line,
                  struct macro_source_file *current_file,
                  const char *comp_dir,
                  struct objfile *objfile)
{
  /* We don't create a macro table for this compilation unit
     at all until we actually get a filename.  */

  /* Find the full name of this source file.  */
  char *full_name = file_full_name (file, comp_dir);

  /* Check if its a new compilation unit, create new table and add macro info. */
  if (! pending_macros)
    pending_macros = new_macro_table (objfile);

  if (! current_file)
    /* If we have no current file, then this must be the start_file
       directive for the compilation unit's main source file.  */
    current_file = macro_set_main (pending_macros, full_name);
  else
    /* File is a header file which has to be included in the current_file
       data structure. */
    current_file = macro_include (current_file, line, full_name);

  free (full_name);

  return current_file;
}

/* Copy the LEN characters at BUF to a xmalloc'ed block of memory,
   followed by a null byte.  */
static char *
copy_string (char *buf, int len)
{
  char *s = xmalloc (len + 1);
  memcpy (s, buf, len);
  s[len] = '\0';

  return s;
}

static char *
consume_improper_spaces (char *p, const char *body)
{
  if (*p == ' ')
    {
      complain (&macro_general_complaint,
		 "macro definition contains spaces in formal argument list:\n`%s'",
		 body);

      while (*p == ' ')
        p++;
    }

  return p;
}

static void
parse_macro_definition (struct macro_source_file *file, int line,
                        char *body)
{
  char *p;
  /* The body string takes one of two forms.  For object-like macro
     definitions, it should be:

        <macro name> " " <definition>

     For function-like macro definitions, it should be:

        <macro name> "() " <definition>
     or
        <macro name> "(" <arg name> ( "," <arg name> ) * ") " <definition>

     Spaces may appear only where explicitly indicated, and in the
     <definition>.
  */

  /* Find the extent of the macro name.  The macro name is terminated
     by either a space or null character (for an object-like macro) or
     an opening paren (for a function-like macro).  */
  for (p = body; *p; p++)
    if (*p == ' ' || *p == '(')
      break;

  if (*p == ' ' || *p == '\0')
    {
      /* It's an object-like macro.  */
      int name_len = p - body;
      char *name = copy_string (body, name_len);
      char *replacement;

      if (*p == ' ')
        replacement = body + name_len + 1;
      else
        {
	  complain (&macro_general_complaint, "Malformed macro definition %s;", body);
          replacement = body + name_len;
        }
      
      add_macro_definition (file, line, name, 0, 0, replacement, macro_object_like);

      free (name);
    }
  else if (*p == '(')
    {
      /* It's a function-like macro.  */
      char *name = copy_string (body, p - body);
      int argc = 0;
      int argv_size = 1;
      char **argv = xmalloc (argv_size * sizeof (*argv));

      p++;

      p = consume_improper_spaces (p, body);

      /* Parse the formal argument list.  */
      while (*p && *p != ')')
        {
          /* Find the extent of the current argument name.  */
          char *arg_start = p;

          while (*p && *p != ',' && *p != ')' && *p != ' ')
            p++;

          if (! *p || p == arg_start)
	    complain (&macro_general_complaint, "Error in the macro definition argument list %s;");
          else
            {
              /* Make sure argv has room for the new argument.  */
              if (argc >= argv_size)
                {
                  argv_size *= 2;
                  argv = xrealloc (argv, argv_size * sizeof (*argv));
                }

              argv[argc++] = copy_string (arg_start, p - arg_start);
            }

          p = consume_improper_spaces (p, body);

          /* Consume the comma, if present.  */
          if (*p == ',')
            {
              p++;

              p = consume_improper_spaces (p, body);
            }
        }

      if (*p == ')')
        {
          p++;

          if (*p == ' ')
            /* Perfectly formed definition, no complaints.  */
            add_macro_definition (file, line, name,
                                  argc, (char **) argv, 
                                  p + 1, macro_function_like);
          else if (*p == '\0')
            {
              /* Complain, but do define it.  */
	      complain (&macro_general_complaint, 
			"Improper function definition; but still defining it %s", body);
              add_macro_definition (file, line, name,
                                    argc, (char **) argv, 
                                    p, macro_function_like);
            }
          else
            /* Just complain.  */
	    complain (&macro_general_complaint, 
		      "Improper macro definition %s; not defining it", body);
        }
      else
        /* Just complain.  */
	  complain (&macro_general_complaint, 
		    "Improper macro definition %s; not defining it", body);
    
    }
  else
    complain (&macro_general_complaint, 
		    "Just a general complaint");
}

static void
dwarf_decode_macros (unsigned int macro_offset, 
		     char *comp_dir, bfd *abfd, struct objfile *objfile)
{
  char *mac_ptr, *mac_end;
  struct macro_source_file *current_file = NULL;

  if (dwarf_macinfo_buffer == NULL)
    {
      complain (&macro_general_complaint, "missing '.debug_macinfo' section");
      return;
    }

  mac_ptr = dwarf_macinfo_buffer + macro_offset;
  mac_end = dwarf_macinfo_buffer + dwarf_macinfo_size;

  for (;;)
    {
      enum dwarf_macinfo_record_type macinfo_type;

      /* Do we at least have room for a macinfo type byte?  */
      if (mac_ptr >= mac_end)
        {
	   /* Complain & return. */
	   complain (&macro_general_complaint, 
		     "mac ptr runs off end of '.debug_macinfo' section");
           return;
        }

      macinfo_type = read_1_byte (abfd, mac_ptr);
      mac_ptr++;

      switch (macinfo_type)
        {
          /* A zero macinfo type indicates the end of the macro
             information.  */
        case 0:
          return;

        case DW_MACINFO_define:
        case DW_MACINFO_undef:
          {
            unsigned int bytes_read;
            int line;
            char *body;

            line = read_unsigned_leb128 (abfd, mac_ptr, &bytes_read);
            mac_ptr += bytes_read;
            body = read_string (abfd, mac_ptr, &bytes_read);
            mac_ptr += bytes_read;

            if (! current_file)
	      {
	        warning (".debug_macinfo section contains macro definition/s "\
		         "outside the scope of any file. As a result macinfo "\
		         "section for the current compilation unit won't be read.");
	        return;
	      }
            else
              {
                if (macinfo_type == DW_MACINFO_define)
                  parse_macro_definition (current_file, line, body);
                else
                  macro_undef (current_file, line, body);
              }
          }
          break;

        case DW_MACINFO_start_file:
          {
            unsigned int bytes_read;
            int line, file;

            line = read_unsigned_leb128 (abfd, mac_ptr, &bytes_read);
            mac_ptr += bytes_read;
            file = read_unsigned_leb128 (abfd, mac_ptr, &bytes_read);
            mac_ptr += bytes_read;

            current_file = macro_start_file (file, line,
                                             current_file, comp_dir, objfile); 
          }
          break;

        case DW_MACINFO_end_file:
          if (! current_file)
	    complain (&macro_general_complaint,
		      "macro debug info has an unmatched `close_file' directive"); 
          else
            {
              current_file = current_file->included_by;
              if (! current_file)
                {
                  enum dwarf_macinfo_record_type next_type;

                  /* GCC circa March 2002 doesn't produce the zero
                     type byte marking the end of the compilation
                     unit.  Complain if it's not there, but exit no
                     matter what.  */

                  /* Do we at least have room for a macinfo type byte?  */
                  if (mac_ptr >= mac_end)
                    {
			 /* Complain & return. */
	   		complain (&macro_general_complaint, 
		     		  "macro info runs off end of '.debug_macinfo' section");
          		return;
                    }

                  /* We don't increment mac_ptr here, so this is just
                     a look-ahead.  */
                  next_type = read_1_byte (abfd, mac_ptr);
                  if (next_type != 0)
		     complain (&macro_general_complaint,
			       "no terminating 0-type entry for macros in '.debug_macinfo' section"); 

                  return;
                }
            }
          break;

        case DW_MACINFO_vendor_ext:
          {
            unsigned int bytes_read;
            int constant;
            char *string;

            constant = read_unsigned_leb128 (abfd, mac_ptr, &bytes_read);
            mac_ptr += bytes_read;
            string = read_string (abfd, mac_ptr, &bytes_read);
            mac_ptr += bytes_read;

            /* We don't recognize any vendor extensions.  */
          }
          break;
        }
    }
}

#endif
