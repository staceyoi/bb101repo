/* Read ELF (Executable and Linking Format) object files for GDB.
   Copyright 1991, 92, 93, 94, 95, 96, 1998 Free Software Foundation, Inc.
   Written by Fred Fish at Cygnus Support.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"

/* Once all of the BE's have the right ELF files in them, we can include
    #include <elf.h>
    #include <elf_parisc.h>
   instead of "hp_elf.h" and "hp_elf_parisc.h"
   I think we should eventually have <elf_ia64.h> instead of "hp_elf_ia64.h".
   */
#ifdef GDB_TARGET_IS_HPUX
# include <elf.h>
# include <elf_parisc.h>
# include "hp_elf_ia64.h"
#endif

#include "bfd.h"
#include "gdb_string.h"
#include "elf-bfd.h"
/* #include "elf/mips.h" */
#include "symtab.h"
#include "symfile.h"
#include "objfiles.h"
#include "buildsym.h"
#include "stabsread.h"
#include "gdb-stabs.h"
#include "complaints.h"
#include "demangle.h"
#include <assert.h>
#include <strings.h>

#ifdef GDB_TARGET_IS_HPUX
# include "hpread.h"
# ifdef GDB_TARGET_IS_HPPA_20W
#  include "pa64solib.h"
# endif
#endif

#include <sys/mman.h>

#ifdef GDB_TARGET_IS_HPUX
#include "linkmap.h"
#endif

#include <unwind_dld.h> /* For load_module_info struct */

#ifndef MAP_FAILED
#define MAP_FAILED (void *) -1L
#endif

/* Baskar.. function to load export list entries.. */
#ifdef GDB_TARGET_IS_HPPA_20W
int init_export_symbols (struct objfile *);
#endif

extern void _initialize_elfread (void);
extern void dwarf2_symfile_init (struct objfile *);
extern void hpread_symfile_finish (struct objfile *);
extern void elf_spit_symbol_table ( bfd *);

/* The struct elfinfo is available only during ELF symbol table and
   psymtab reading.  It is destroyed at the completion of psymtab-reading.
   It's local to elf_symfile_read.  */

struct elfinfo
  {
    file_ptr dboffset;		/* Offset to dwarf debug section */
    unsigned int dbsize;	/* Size of dwarf debug section */
    file_ptr lnoffset;		/* Offset to dwarf line number section */
    unsigned int lnsize;	/* Size of dwarf line number section */
    asection *stabsect;		/* Section pointer for .stab section */
    asection *stabindexsect;	/* Section pointer for .stab.index section */
    asection *mdebugsect;	/* Section pointer for .mdebug section */
    asection *hp_debug_sect;    /* Section pointer for HP debug info header */
    asection *hp_doom_debug_sect; /* Section pointer for HP debug info header */
  };

/* Various things we might complain about... */

struct complaint section_info_complaint =
{"elf/stab section information %s without a preceding file symbol", 0, 0};

struct complaint section_info_dup_complaint =
{"duplicated elf/stab section information for %s", 0, 0};

struct complaint stab_info_mismatch_complaint =
{"elf/stab section information missing for %s", 0, 0};

struct complaint stab_info_questionable_complaint =
{"elf/stab section information questionable for %s", 0, 0};

static void elf_symfile_init (struct objfile *, int);

static void elf_new_init (struct objfile *);

static void elf_symfile_finish (struct objfile *);

static void elf_symtab_read (struct objfile *, int);

char * elf_symtab_read_notes (bfd *, int *);

/* For JAGaf16859 - Gdb to list source files
 * even when the executable is moved from the 
 * compilation dirctory 
 */
void * elf_set_current_compdir(struct partial_symtab *);

static void free_elfinfo (void *);

static struct minimal_symbol *record_minimal_symbol_and_info (char *,
							      CORE_ADDR,
							      enum
							      minimal_symbol_type,
							      char *,
							      asection *
							      bfd_section,
							      struct objfile
							      *);

extern void mfree (PTR md, PTR ptr);

static void elf_locate_sections (bfd *, asection *, void *);

static uint64_t 
get_start_address PARAMS ((struct objfile *, int sym_section_index, int objid));

/* We are called once per section from elf_symfile_read.  We
   need to examine each section we are passed, check to see
   if it is something we are interested in processing, and
   if so, stash away some access information for the section.

   For now we recognize the dwarf debug information sections and
   line number sections from matching their section names.  The
   ELF definition is no real help here since it has no direct
   knowledge of DWARF (by design, so any debugging format can be
   used).

   We also recognize the ".stab" sections used by the Sun compilers
   released with Solaris 2.

   FIXME: The section names should not be hardwired strings (what
   should they be?  I don't think most object file formats have enough
   section flags to specify what kind of debug section it is
   -kingdon).  */

static void
elf_locate_sections (bfd *ignore_abfd, asection *sectp, PTR eip)
{
  register struct elfinfo *ei;

  ei = (struct elfinfo *) eip;
  if (STREQ (sectp->name, ".debug"))
    {
      ei->dboffset = sectp->filepos;
      ei->dbsize = (unsigned int) bfd_get_section_size_before_reloc (sectp);
    }
  else if (STREQ (sectp->name, ".line"))
    {
      ei->lnoffset = sectp->filepos;
      ei->lnsize = (unsigned int) bfd_get_section_size_before_reloc (sectp);
    }
  else if (STREQ (sectp->name, ".stab"))
    {
      ei->stabsect = sectp;
    }
  else if (STREQ (sectp->name, ".stab.index"))
    {
      ei->stabindexsect = sectp;
    }
  else if (STREQ (sectp->name, ".mdebug"))
    {
      ei->mdebugsect = sectp;
    }
  else if (STREQ (sectp -> name, ".debug_header"))
    {
      ei->hp_debug_sect = sectp;
    }
  else if (STREQ (sectp -> name, ".linkmap") ||
           STREQ (sectp -> name, ".hp.objdict"))
    {
      ei->hp_doom_debug_sect = sectp;
    }
}

#if 0				/* Currently unused */

char *
elf_interpreter (abfd)
     bfd *abfd;
{
  sec_ptr interp_sec;
  unsigned size;
  char *interp = NULL;

  interp_sec = bfd_get_section_by_name (abfd, ".interp");
  if (interp_sec)
    {
      size = bfd_section_size (abfd, interp_sec);
      interp = alloca (size);
      if (bfd_get_section_contents (abfd, interp_sec, interp, (file_ptr) 0,
				    size))
	{
	  interp = savestring (interp, size - 1);
	}
      else
	{
	  interp = NULL;
	}
    }
  return (interp);
}

#endif

static struct minimal_symbol *
record_minimal_symbol_and_info (char *name,
     				CORE_ADDR address,
				enum minimal_symbol_type ms_type,
				char *info,	/* FIXME, is this really char *? */
				asection *bfd_section,
				struct objfile *objfile)
{
  int section;

  /* Guess the section from the type.  This is likely to be wrong in
     some cases.  */
  switch (ms_type)
    {
    case mst_text:
    case mst_file_text:
      /* JYG: MERGE FIXME: The Cygnus change here breaks
	 DOOM PA64 shared library support.  Since SECT_OFF_* are
	 no longer simple macros anymore, put in the hack
	 (constant index value) instead which corresponds to
	 what SECT_OFF_* used to have. */
      /* section = bfd_section->index; */
      section = 0;
#ifdef SMASH_TEXT_ADDRESS
      SMASH_TEXT_ADDRESS (address);
#endif
      break;
    case mst_data:
    case mst_file_data:
      /* JYG: MERGE FIXME: The Cygnus change here breaks
	 DOOM PA64 shared library support.  Since SECT_OFF_* are
	 no longer simple macros anymore, put in the hack
	 (constant index value) instead which corresponds to
	 what SECT_OFF_* used to have. */ 
      section = 1;
      break;
    case mst_bss:
    case mst_file_bss:
      /* JYG: MERGE FIXME: The Cygnus change here breaks
	 DOOM PA64 shared library support.  Since SECT_OFF_* are
	 no longer simple macros anymore, put in the hack
	 (constant index value) instead which corresponds to
	 what SECT_OFF_* used to have. */
      /* section = bfd_section->index; */
      section = 2;
      break;
    default:
      section = -1;
      break;
    }

#ifdef DP_SAVED_IN_OBJFILE
  if (IS_TARGET_LRE)
    {
      if (strcmp (name, "_GLOBAL_OFFSET_TABLE_") == 0)
	{
          objfile->saved_dp_reg = address;
        }
    }
  else
    if (strcmp (name, "__gp") == 0) 
      {
        objfile->saved_dp_reg = address;
      }
#endif

  return prim_record_minimal_symbol_and_info
    (name, address, ms_type, info, section, bfd_section, objfile);
}

/*

   LOCAL FUNCTION

   elf_symtab_read -- read the symbol table of an ELF file

   SYNOPSIS

   void elf_symtab_read (struct objfile *objfile, int dynamic)

   DESCRIPTION

   Given an objfile and a flag that specifies whether or not the objfile
   is for an executable or not (may be shared library for example), add
   all the global function and data symbols to the minimal symbol table.

   In stabs-in-ELF, as implemented by Sun, there are some local symbols
   defined in the ELF symbol table, which can be used to locate
   the beginnings of sections from each ".o" file that was linked to
   form the executable objfile.  We gather any such info and record it
   in data structures hung off the objfile's private data.

 */

static void
elf_symtab_read (struct objfile *objfile, int dynamic)
{
  long storage_needed;
  asymbol *sym;
  asymbol **symbol_table;
  long number_of_symbols;
  long i;
  int index;
  struct cleanup *back_to;
  CORE_ADDR symaddr;
  CORE_ADDR text_offset;
  CORE_ADDR data_offset;
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
  CORE_ADDR text_vma;
  CORE_ADDR data_vma;
#endif
  enum minimal_symbol_type ms_type = mst_unknown;
  /* If sectinfo is nonNULL, it contains section info that should end up
     filed in the objfile.  */
  struct stab_section_info *sectinfo = NULL;
  /* If filesym is nonzero, it points to a file symbol, but we haven't
     seen any section info for it yet.  */
  asymbol *filesym = 0;
#ifdef SOFUN_ADDRESS_MAYBE_MISSING
  /* Name of filesym, as saved on the symbol_obstack.  */
  char *filesymname = obsavestring ("", 0, &objfile->symbol_obstack);
#endif
  struct dbx_symfile_info *dbx = objfile->sym_stab_info;
  unsigned long size;
  int stripped = (bfd_get_symcount (objfile->obfd) == 0);

  text_offset = ANOFFSET(objfile->section_offsets, SECT_OFF_TEXT (objfile));
  data_offset = ANOFFSET(objfile->section_offsets, SECT_OFF_DATA (objfile));

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
  text_vma = elf_text_start_vma(objfile->obfd);
  data_vma = elf_data_start_vma(objfile->obfd);
#endif
  
  if (dynamic)
    {
      storage_needed = bfd_get_dynamic_symtab_upper_bound (objfile->obfd);

      /* Nothing to be done if there is no dynamic symtab.  */
      if (storage_needed < 0)
	return;
    }
  else
    {
      storage_needed = bfd_get_symtab_upper_bound (objfile->obfd);
      if (storage_needed < 0)
	error ("Can't read symbols from %s: %s", bfd_get_filename (objfile->obfd),
	       bfd_errmsg (bfd_get_error ()));
    }
  if (storage_needed > 0)
    {
      symbol_table = (asymbol **) xmalloc (storage_needed);
      back_to = make_cleanup (free, symbol_table);
      if (dynamic)
	number_of_symbols = bfd_canonicalize_dynamic_symtab (objfile->obfd,
							     symbol_table);
      else
	number_of_symbols = bfd_canonicalize_symtab (objfile->obfd, symbol_table);
      if (number_of_symbols < 0)
	error ("Can't read symbols from %s: %s", bfd_get_filename (objfile->obfd),
	       bfd_errmsg (bfd_get_error ()));

      for (i = 0; i < number_of_symbols; i++)
	{
	  sym = symbol_table[i];
	  if (sym->name == NULL || *sym->name == '\0')
	    {
	      /* Skip names that don't exist (shouldn't happen), or names
	         that are null strings (may happen). */
	      continue;
	    }

#ifdef IS_RELOC_ONLY_SYMBOL
          /* skip internal (compiler generated) symbols if in non-DOOM mode */
          if (!doom_executable)
            if (IS_RELOC_ONLY_SYMBOL(sym->name))
              continue;
#endif 

	  if (dynamic
	      && sym->section == &bfd_und_section
	      && (sym->flags & BSF_FUNCTION))
	    {
	      struct minimal_symbol *msym;

	      /* Symbol is a reference to a function defined in
	         a shared library.
	         If its value is non zero then it is usually the address
	         of the corresponding entry in the procedure linkage table,
	         plus the desired section offset.
	         If its value is zero then the dynamic linker has to resolve
	         the symbol. We are unable to find any meaningful address
	         for this symbol in the executable file, so we skip it.  */

              /* If the symbol is from an undefined section, it is a
                 reference, yes, but its value is meaningless. Don't
                 touch it with a barge pole. See JAGad29300 for a case
                 where this messes up things.-- srikanth, 092100.
              */
	      continue;
	    }

	  /* If it is a nonstripped executable, do not enter dynamic
	     symbols, as the dynamic symbol table is usually a subset
	     of the main symbol table.  */
#ifdef HP_IA64
	  /* Bindu: 012601 Enter __thread_specific_seg symbol into 
	     msymtab. The offset for the TLS variables is the difference
	     between the address of the TLS variable in msymtab and
             __thread_specific_seg address.
	     The real address of the TLS variable is (offset + 
	     tls_start_addr + value of TP_REGNUM on IA64).
	  */
	  if (dynamic && !stripped && strcmp("__thread_specific_seg", sym->name))
#else
	  if (dynamic && !stripped)
#endif
	    continue;
	  if (sym->flags & BSF_FILE)
	    {
	      /* STT_FILE debugging symbol that helps stabs-in-elf debugging.
	         Chain any old one onto the objfile; remember new sym.  */
	      if (sectinfo != NULL)
		{
		  sectinfo->next = dbx->stab_section_info;
		  dbx->stab_section_info = sectinfo;
		  sectinfo = NULL;
		}
	      filesym = sym;
#ifdef SOFUN_ADDRESS_MAYBE_MISSING
	      filesymname =
		obsavestring ((char *) filesym->name, strlen (filesym->name),
			      &objfile->symbol_obstack);
#endif
	    }
	  else if (sym->flags & (BSF_GLOBAL | BSF_LOCAL | BSF_WEAK))
	    {
	      struct minimal_symbol *msym;

	      /* Select global/local/weak symbols.  Note that bfd puts abs
	         symbols in their own section, so all symbols we are
	         interested in will have a section. */
	      /* Bfd symbols are section relative. */
	      symaddr = sym->value + sym->section->vma;
	      /* Relocate all non-absolute symbols by the section offset.  */
	      if (sym->section != &bfd_abs_section)
		{
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
                  if (((text_vma <= data_vma) && (symaddr < data_vma)) ||
                      ((text_vma >= data_vma) && (symaddr >= text_vma)))
                    symaddr += text_offset;
                  else if (((text_vma <= data_vma) && (symaddr >= data_vma)) ||
                           ((text_vma >= data_vma) && (symaddr < text_vma)))
                    symaddr += data_offset;
#else                  
		  symaddr += text_offset;
#endif 
		}
	      /* For non-absolute symbols, use the type of the section
	         they are relative to, to intuit text/data.  Bfd provides
	         no way of figuring this out for absolute symbols. */
	      if (sym->section == &bfd_abs_section)
		{
		  /* This is a hack to get the minimal symbol type
		     right for Irix 5, which has absolute addresses
		     with special section indices for dynamic symbols. */
		  unsigned short shndx =
		  ((elf_symbol_type *) sym)->internal_elf_sym.st_shndx;

		  switch (shndx)
		    {
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
		      /* This only works for HP ELF.
			 It shouldn't be necessary to do so. */
                    case SHN_ABS :
                      ms_type = mst_abs;
                      break;
                    case SHN_COMMON :
                      ms_type = mst_bss;
                      break;
                    default :

		      /* If a symbol is in an absolute section that we
			 don't understand, all we can do is ignore it.
			 At one time, we gave an error message about 
			 unexpected sections, but more just kept showing
			 up.

                      error("Unknown section %s, index %d", sym->name, shndx);
			 */
		      ms_type = mst_abs;
		      break;
#else

#ifdef SHN_MIPS_TEXT
		    case SHN_MIPS_TEXT:
		      ms_type = mst_text;
		      break;
#endif

#ifdef SHN_MIPS_DATA
		    case SHN_MIPS_DATA:
		      ms_type = mst_data;
		      break;
#endif

#ifdef SHN_MIPS_ACOMMON
		    case SHN_MIPS_ACOMMON:
		      ms_type = mst_bss;
		      break;
#endif

		    default:
		      ms_type = mst_abs;
#endif /* !GDB_TARGET_IS_HPPA_20W */
		    }

		  /* If it is an Irix dynamic symbol, skip section name
		     symbols, relocate all others by section offset. */
		  if (ms_type != mst_abs)
		    {
		      if (sym->name[0] == '.')
			continue;
		      symaddr += text_offset;
		    }
		}
	      else if (sym->section->flags & SEC_CODE)
		{
		  if (sym->flags & BSF_GLOBAL)
		    {
		      ms_type = mst_text;
		    }
		  else if ((sym->name[0] == '.' && sym->name[1] == 'L')
			   || ((sym->flags & BSF_LOCAL)
			       && sym->name[0] == '$'
			       && sym->name[1] == 'L'))
		    /* Looks like a compiler-generated label.  Skip it.
		       The assembler should be skipping these (to keep
		       executables small), but apparently with gcc on the
		       delta m88k SVR4, it loses.  So to have us check too
		       should be harmless (but I encourage people to fix this
		       in the assembler instead of adding checks here).  */
		    continue;
#ifdef HARRIS_TARGET
		  else if (sym->name[0] == '.' && sym->name[1] == '.')
		    {
		      /* Looks like a Harris compiler generated label for the
		         purpose of marking instructions that are relevant to
		         DWARF dies.  The assembler can't get rid of these 
		         because they are relocatable addresses that the
		         linker needs to resolve. */
		      continue;
		    }
#endif
		  else
		    {
		      ms_type = mst_file_text;
                    }

		}
	      else if (sym->section->flags & SEC_ALLOC)
		{
		  if (sym->flags & BSF_GLOBAL)
		    {
                      if (sym->flags & BSF_TLS)
                        ms_type = mst_tls;
		      else if (sym->section->flags & SEC_LOAD)
                        ms_type = mst_data;
		      else
                        ms_type = mst_bss;
		    }
		  else if (sym->flags & BSF_LOCAL)
		    {
		      /* Named Local variable in a Data section.  Check its
		         name for stabs-in-elf.  The STREQ macro checks the
		         first character inline, so we only actually do a
		         strcmp function call on names that start with 'B'
		         or 'D' */
		      index = SECT_OFF_MAX;
		      if (STREQ ("Bbss.bss", sym->name))
			{
			  index = SECT_OFF_BSS (objfile);
			}
		      else if (STREQ ("Ddata.data", sym->name))
			{
			  index = SECT_OFF_DATA (objfile);
			}
		      else if (STREQ ("Drodata.rodata", sym->name))
			{
			  index = SECT_OFF_RODATA (objfile);
			}
		      if (index != SECT_OFF_MAX)
			{
			  /* Found a special local symbol.  Allocate a
			     sectinfo, if needed, and fill it in.  */
			  if (sectinfo == NULL)
			    {
			      sectinfo = (struct stab_section_info *)
				xmmalloc (objfile->md, sizeof (*sectinfo));
			      memset ((PTR) sectinfo, 0, sizeof (*sectinfo));
			      if (filesym == NULL)
				{
				  complain (&section_info_complaint,
					    sym->name);
				}
			      else
				{
				  sectinfo->filename =
				    (char *) filesym->name;
				}
			    }
			  if (sectinfo->sections[index] != 0)
			    {
			      complain (&section_info_dup_complaint,
					sectinfo->filename);
			    }
			  /* Bfd symbols are section relative. */
			  symaddr = sym->value + sym->section->vma;
			  /* Relocate non-absolute symbols by the section offset. */
			  if (sym->section != &bfd_abs_section)
			    {
			      symaddr += text_offset;
			    }
			  symaddr = SWIZZLE (symaddr);
			  sectinfo->sections[index] = symaddr;
			  /* The special local symbols don't go in the
			     minimal symbol table, so ignore this one. */
			  continue;
			}
		      /* Not a special stabs-in-elf symbol, do regular
		         symbol processing. */
                      if (sym->flags & BSF_TLS)
                        {
                          ms_type = mst_file_tls;
                        }
		      else if (sym->section->flags & SEC_LOAD)
			{
			  ms_type = mst_file_data;
			  /* Bindu 121401: Add these MxN specific symbols
                           * so that lookup_address_of_variable can get
                           * these addresses. lookup_address_of_variable is a
                           * callback function provided for the libmxn to
                           * lookup these symbols.
                           */
			  if (strcmp (sym->name, "__active_pthreads") == 0
                              || strcmp (sym->name, "__mxn_magic") == 0
                              || strcmp (sym->name, "__pthread_startup_done") == 0)
			    ms_type = mst_data;
			}
		      else
			{
			  ms_type = mst_file_bss;
			}
		    }
		  else
		    {
		      ms_type = mst_unknown;
		    }
		}
	      else
		{
		  /* FIXME:  Solaris2 shared libraries include lots of
		     odd "absolute" and "undefined" symbols, that play 
		     hob with actions like finding what function the PC
		     is in.  Ignore them if they aren't text, data, or bss.  */
		  /* ms_type = mst_unknown; */
		  continue;	/* Skip this symbol. */
		}
	      /* Pass symbol size field in via BFD.  FIXME!!!  */
	      size = ((elf_symbol_type *) sym)->internal_elf_sym.st_size;
	      symaddr = SWIZZLE (symaddr);
	      msym = record_minimal_symbol_and_info
		((char *) sym->name, symaddr,
		 ms_type, (PTR) size, sym->section, objfile);
#ifdef SOFUN_ADDRESS_MAYBE_MISSING
	      if (msym != NULL)
		msym->filename = filesymname;
#endif
#ifdef ELF_MAKE_MSYMBOL_SPECIAL
	      ELF_MAKE_MSYMBOL_SPECIAL (sym, msym);
#endif
	    /* unconditional use to silence aCC6 set but not used message. */
	    msym++;
	    }
	}
      do_cleanups (back_to);
    }
  else if (storage_needed == 0 && !dynamic)
    { /* Apparently this object module has been stripped. Give warning 
       * JAGaf20680
       */
      if (!profile_on)
        warning ("Load module %s has been stripped.  \012\
Debugging information is not available.\n", objfile->name);
    }

#ifdef OBJFILE_HAS_IS_ARCHIVE
      objfile->is_archive_bound = IS_OBJFILE_ARCHIVE(objfile);
#endif

    /* FIXME. This ifdef is a band-aid to let GDB32 link */ 
#if ! defined(GDB_TARGET_IS_HPPA) || defined(GDB_TARGET_IS_HPPA_20W)
  elf_spit_symbol_table (objfile->obfd);  /* do it and do it now ! */
#endif
}

char *
elf_symtab_read_notes (bfd * abfd, int * size)
{
  asection * notes;
  char * notes_buf = 0;

  notes = bfd_get_section_by_name (abfd, ".note");
  *size = (int) bfd_section_size (abfd, notes);

  notes_buf = xmalloc (*size);

  if (!bfd_get_section_contents (abfd, notes, notes_buf, (file_ptr)0, *size))
    error ("bfd_get_section_contents\n");
  return notes_buf;
}

/* Scan and build partial symbols for a symbol file.
   We have been initialized by a call to elf_symfile_init, which 
   currently does nothing.

   SECTION_OFFSETS is a set of offsets to apply to relocate the symbols
   in each section.  We simplify it down to a single offset for all
   symbols.  FIXME.

   MAINLINE is true if we are reading the main symbol
   table (as opposed to a shared lib or dynamically loaded file).

   This function only does the minimum work necessary for letting the
   user "name" things symbolically; it does not read the entire symtab.
   Instead, it reads the external and static symbols and puts them in partial
   symbol tables.  When more extensive information is requested of a
   file, the corresponding partial symbol table is mutated into a full
   fledged symbol table by going back and reading the symbols
   for real.

   We look for sections with specific names, to tell us what debug
   format to look for:  FIXME!!!

   dwarf_build_psymtabs() builds psymtabs for DWARF symbols;
   elfstab_build_psymtabs() handles STABS symbols;
   mdebug_build_psymtabs() handles ECOFF debugging information.

   Note that ELF files have a "minimal" symbol table, which looks a lot
   like a COFF symbol table, but has only the minimal information necessary
   for linking.  We process this also, and use the information to
   build gdb's minimal symbol table.  This gives us some minimal debugging
   capability even for files compiled without -g.  */

void
elf_symfile_read (struct objfile *objfile, int mainline, int threshold_exceeded)
{
  bfd *abfd = objfile->obfd;
  struct elfinfo ei;
  struct cleanup *back_to = 0;
  int saved_mainline = 0;
  asection * dlt_section;

  /* Set objfile's dlt start and end */
  dlt_section = bfd_get_section_by_name (objfile->obfd, HP_DLT);
  if (!dlt_section)
    {
      objfile->dlt_start_addr = 0;
      objfile->dlt_end_addr = 0;
    }
  else
    {
      objfile->dlt_start_addr =  dlt_section->vma +
                                        ANOFFSET(objfile->section_offsets,
                                                   SECT_OFF_DATA (objfile));
      objfile->dlt_end_addr = objfile->dlt_start_addr +
                                                bfd_section_size (objfile->obfd,
                                                                  dlt_section);
    }

  init_minimal_symbol_collection ();
  back_to = make_cleanup_discard_minimal_symbols ();

  memset ((char *) &ei, 0, sizeof (ei));

  /* Allocate struct to keep track of the symfile */
  objfile->sym_stab_info = (struct dbx_symfile_info *)
    xmmalloc (objfile->md, sizeof (struct dbx_symfile_info));
  memset ((char *) objfile->sym_stab_info, 0, sizeof (struct dbx_symfile_info));
  make_cleanup (free_elfinfo, (PTR) objfile);
  

  /* coulter - we need to set the correct demangling style before we read the 
     symbol table.
     */
  /* RM: This used to be done _after_ we built the psymtab.
     For DOOM, though, I need the minimal symbol tables to be available
     _before_ I build the psymtabs. I think moving this up should be okay.

     RM: but we do need to explicitly set the demangling style to hp.
     This used to be done by hpread_build_psymtabs (it still is, but it's
     too late by then). */

  /* Demangling style -- if EDG style already set, don't change it,
     as HP style causes some problems with the KAI EDG compiler */ 
  /* CM: Force it to HP only if we see any spaces with the following
     names. These are the names of the spaces that contain debug
     information produced by HP compilers. In case we have a non-debuggable
     shared library or program, also look for a couple of HP specific
     sections. */
  if (bfd_get_section_by_name (abfd, ".HP.opt_annot") ||
      bfd_get_section_by_name (abfd, ".HP.init") ||
      bfd_get_section_by_name (abfd, ".objdebug_type") ||
      bfd_get_section_by_name (abfd, ".debug_header") ||
      bfd_get_section_by_name (abfd, ".debug_pinfo")) {
    if (current_demangling_style != edg_demangling) {
#ifdef GNU_NEW_ABI_DEMANGLING_STYLE_STRING
      if (new_cxx_abi)
	{
	  set_demangling_style (GNU_NEW_ABI_DEMANGLING_STYLE_STRING);
	}
      else
#endif
	{
	  /* Otherwise, ensure that we are using HP style demangling */ 
	  set_demangling_style (HP_DEMANGLING_STYLE_STRING);
	}
    }
  }

  /* Process the normal ELF symbol table first.  This may write some 
     chain of info into the dbx_symfile_info in objfile->sym_stab_info,
     which can later be used by elfstab_offset_sections.  */

  /* FIXME, should take a section_offsets param, not just an offset.  */
  /* RM: @#$%!* so why wasn't it? */
  elf_symtab_read (objfile, 0);

  /* Add the dynamic symbols.  */

  elf_symtab_read (objfile, 1);

  install_minimal_symbols (objfile);

  /* Now process debugging information, which is contained in
     special ELF sections. */

  /* If we are reinitializing, or if we have never loaded syms yet,
     set table to empty.  MAINLINE is cleared so that *_read_psymtab
     functions do not all also re-initialize the psymbol table. */
  if (mainline)
    {
      init_psymbol_list (objfile, 0);
      saved_mainline = mainline;
      mainline = 0;
    }

  /* We first have to find them... */
  if (!threshold_exceeded)
    {
      bfd_map_over_sections (abfd, elf_locate_sections, (PTR) & ei);

      /* ELF debugging information is inserted into the psymtab in the
	 order of least informative first - most informative last.  Since
	 the psymtab table is searched `most recent insertion first' this
	 increases the probability that more detailed debug information
	 for a section is found.

	 For instance, an object file might contain both .mdebug (XCOFF)
	 and .debug_info (DWARF2) sections then .mdebug is inserted first
	 (searched last) and DWARF2 is inserted last (searched first).  If
	 we don't do this then the XCOFF info is found first - for code in
	 an included file XCOFF info is useless. */

      if (ei.mdebugsect)
        {
	  const struct ecoff_debug_swap *swap;

	  /* .mdebug section, presumably holding ECOFF debugging
	     information.  */
	  swap = get_elf_backend_data (abfd)->elf_backend_ecoff_debug_swap;
	  if (swap)
	    elfmdebug_build_psymtabs (objfile, swap, ei.mdebugsect);
	}
      if (ei.stabsect)
        {
	  asection *str_sect;

	  /* Stab sections have an associated string table that looks like
	     a separate section.  */
	  str_sect = bfd_get_section_by_name (abfd, ".stabstr");

	  /* FIXME should probably warn about a stab section without a stabstr.  */
	  if (str_sect)
	    elfstab_build_psymtabs (objfile,
				    mainline,
				    ei.stabsect->filepos,
				    (unsigned int) bfd_section_size (abfd, ei.stabsect),
				    str_sect->filepos,
				    (unsigned int) bfd_section_size (abfd, str_sect));
	}
      if (dwarf2_has_info (abfd))
        {
	  /* DWARF 2 sections */
	  dwarf2_build_psymtabs (objfile, saved_mainline);
	}
      else if (ei.dboffset && ei.lnoffset)
        {
	  /* DWARF sections */
	  dwarf_build_psymtabs (objfile,
				mainline,
				ei.dboffset, ei.dbsize,
				ei.lnoffset, ei.lnsize);
	}

#if defined GDB_TARGET_IS_HPUX
      if (ei.hp_debug_sect || ei.hp_doom_debug_sect)
        {
#ifndef HP_IA64
	  /* HP debug sections */
	  hpread_build_psymtabs (objfile, mainline);
#else
	  /* Dwarf2 debug sections */
	  if (!dwarf2_has_info (abfd))
	    dwarf2_build_psymtabs (objfile, saved_mainline);
#endif
	}

#endif
    }
  
  do_cleanups (back_to);
  /* Force hppa-tdep.c to re-read the unwind descriptors.  */
  objfile->obj_private = NULL;
}

static void
elf_symfile_add_psymtabs (struct objfile *objfile, int mainline)
{
  bfd *abfd = objfile->obfd;
  struct elfinfo ei;

  /* For HP, there may not be a .debug section, dbsize in particular might
     not be initialized.  As in elf_symfile_read, initialize the structure
     to zero. */
  memset ((char *) &ei, 0, sizeof (ei));

#ifdef GDB_TARGET_IS_HPPA
  hpread_symfile_init (objfile);
#endif
  bfd_map_over_sections (abfd, elf_locate_sections, (PTR) &ei);
  if (dwarf2_has_info (abfd))
    {
      /* DWARF 2 sections */
      dwarf2_build_psymtabs (objfile, mainline);
    }
  if (ei.dboffset && ei.lnoffset)
    {
      /* DWARF sections */
      dwarf_build_psymtabs (objfile, mainline,
                            ei.dboffset, ei.dbsize,
                            ei.lnoffset, ei.lnsize);
    }
  if (ei.stabsect)
    {
      asection *str_sect;
      
      /* Stab sections have an associated string table that looks like
         a separate section.  */
      str_sect = bfd_get_section_by_name (abfd, ".stabstr");
      
      /* FIXME should probably warn about a stab section without a stabstr.  */
      if (str_sect)
        elfstab_build_psymtabs (objfile, mainline,
                                ei.stabsect->filepos,
                                (unsigned int) bfd_section_size (abfd, ei.stabsect),
                                str_sect->filepos,
                                (unsigned int) bfd_section_size (abfd, str_sect));
    }
  if (ei.mdebugsect)
    {
      const struct ecoff_debug_swap *swap;
      
      /* .mdebug section, presumably holding ECOFF debugging
         information.  */
      swap = get_elf_backend_data (abfd)->elf_backend_ecoff_debug_swap;
      if (swap)
        elfmdebug_build_psymtabs (objfile, swap, ei.mdebugsect);
    }

#ifdef GDB_TARGET_IS_HPPA
  if (ei.hp_debug_sect || ei.hp_doom_debug_sect)
    {
      /* HP debug sections */
      hpread_build_psymtabs (objfile, mainline);
    }
#endif
}

/* This cleans up the objfile's sym_stab_info pointer, and the chain of
   stab_section_info's, that might be dangling from it.  */

static void
free_elfinfo (PTR objp)
{
  struct objfile *objfile = (struct objfile *) objp;
  struct dbx_symfile_info *dbxinfo = objfile->sym_stab_info;
  struct stab_section_info *ssi, *nssi;

  ssi = dbxinfo->stab_section_info;
  while (ssi)
    {
      nssi = ssi->next;
      mfree (objfile->md, ssi);
      ssi = nssi;
    }

  dbxinfo->stab_section_info = 0;	/* Just say No mo info about this.  */
}


/* Initialize anything that needs initializing when a completely new symbol
   file is specified (not just adding some symbols from another file, e.g. a
   shared library).

   We reinitialize buildsym, since we may be reading stabs from an ELF file.  */

static void
elf_new_init (struct objfile *ignore)
{
  stabsread_new_init ();
  buildsym_new_init ();
}

/* Perform any local cleanups required when we are done with a particular
   objfile.  I.E, we are in the process of discarding all symbol information
   for an objfile, freeing up all memory held for it, and unlinking the
   objfile struct from the global list of known objfiles. */

static void
elf_symfile_finish (struct objfile *objfile)
{
  if (objfile->sym_stab_info != NULL)
    {
      mfree (objfile->md, objfile->sym_stab_info);
    }
#if defined GDB_TARGET_IS_HPUX
  hpread_symfile_finish (objfile);
#endif
}

/* ELF specific initialization routine for reading symbols.

   It is passed a pointer to a struct sym_fns which contains, among other
   things, the BFD for the file whose symbols are being read, and a slot for
   a pointer to "private data" which we can fill with goodies.

   For now at least, we have nothing in particular to do, so this function is
   just a stub. */

static void
elf_symfile_init (struct objfile *objfile, int threshold_exceeded)
{
  /* ELF objects may be reordered, so set OBJF_REORDERED.  If we
     find this causes a significant slowdown in gdb then we could
     set it in the debug symbol readers only when necessary.  */
  objfile->flags |= OBJF_REORDERED;
#ifdef GDB_TARGET_IS_HPPA_20W
  if (!threshold_exceeded)
    {
      /* hook for access to HP-UX 2.0W debug info */
      hpread_symfile_init(objfile);
    }
#endif
#ifdef HP_IA64
  if (!threshold_exceeded)
    {
      /* hook for access to HP_IA64 debug info */
      dwarf2_symfile_init (objfile);
    }
#endif
}

/* When handling an ELF file that contains Sun STABS debug info,
   some of the debug info is relative to the particular chunk of the
   section that was generated in its individual .o file.  E.g.
   offsets to static variables are relative to the start of the data
   segment *for that module before linking*.  This information is
   painfully squirreled away in the ELF symbol table as local symbols
   with wierd names.  Go get 'em when needed.  */

void
elfstab_offset_sections (struct objfile *objfile, struct partial_symtab *pst)
{
  char *filename = pst->filename;
  struct dbx_symfile_info *dbx = objfile->sym_stab_info;
  struct stab_section_info *maybe = dbx->stab_section_info;
  struct stab_section_info *questionable = 0;
  int i;
  char *p;

  /* The ELF symbol info doesn't include path names, so strip the path
     (if any) from the psymtab filename.  */
  while (0 != (p = strchr (filename, '/')))
    filename = p + 1;

  /* FIXME:  This linear search could speed up significantly
     if it was chained in the right order to match how we search it,
     and if we unchained when we found a match. */
  for (; maybe; maybe = maybe->next)
    {
      if (filename[0] == maybe->filename[0]
	  && STREQ (filename, maybe->filename))
	{
	  /* We found a match.  But there might be several source files
	     (from different directories) with the same name.  */
	  if (0 == maybe->found)
	    break;
	  questionable = maybe;	/* Might use it later.  */
	}
    }

  if (maybe == 0 && questionable != 0)
    {
      complain (&stab_info_questionable_complaint, filename);
      maybe = questionable;
    }

  if (maybe)
    {
      /* Found it!  Allocate a new psymtab struct, and fill it in.  */
      maybe->found++;
      pst->section_offsets = (struct section_offsets *)(void *)
	obstack_alloc (&objfile->psymbol_obstack, SIZEOF_SECTION_OFFSETS);
      for (i = 0; i < SECT_OFF_MAX; i++)
	ANOFFSET (pst->section_offsets, i) = maybe->sections[i];
      return;
    }

  /* We were unable to find any offsets for this file.  Complain.  */
  if (dbx->stab_section_info)	/* If there *is* any info, */
    complain (&stab_info_mismatch_complaint, filename);
}


#define INIT_COMP_MAP_ENTRIES 256
/* For ia64 the subspace map contains entries that is data 
   but isn't in the objdebug tables.*/
#define INIT_SUBSPACE_MAP_ENTRIES 1024
#define INIT_STRING_TABLE_SIZE 4096
#define STRING_TABLE_DELTA (1024 * 500)

/* We now have a note map which holds all entries from the
   notes section instead of having being lumped with the all the 
   other section entries. */
#define INIT_NOTE_MAP_ENTRIES 256
/* We also have the objdebug type and const sections entries 
   in a different structure */
#define INIT_DEBUG_MAP_ENTRIES 256
/* The section map contains the text entries as well as objdebug 
   const, type, file entries and data entries. This is a short lived
   data structure and is freed after the psymtabs are made.
   For an executable like i2 - there are a total of 1 million, 200
   thousand entries. The approx. break up of these is as follows. 
   The notes section has as many entries as there are .o's in that 
   objfile. For the i2 exe it was ~1200.
   text -    120,000
   data -     30,000
   other -   800,000
   debug -   220,000
   Total - 1,200,000
 */
#define INIT_SECTION_MAP_ENTRIES 256
/* The section map now has buckets - one bucket for each .o 
   object file of that objfile.
*/
#define INIT_SECTION_MAP_BUCKET_ENTRIES 256


/* RM: this should be in elf_parisc.h, but hasn't made it there yet */
#define NOTE_HP_SRC_FILE_INFO 0x4
#ifndef NOTE_HP_LINKER
#define NOTE_HP_LINKER        0x5
#endif

#define TEXT_SECTION 0
#define DATA_SECTION 1
#define DEBUG_SECTION 2
#define NOTE_SECTION 4
#define OTHER_SECTION 5

int string_table_limit;
int string_table_size;

int notestart;
int noteend;

struct note
{
  long long namesz;
  long long descsz;
  long long type;
  char contents[1];           /* actually contains name and desc */
};

struct hp_note
{
  int32_t namesz;
  int32_t descsz;
  int32_t type;
  char contents[1];      /* actually contains name and desc */
};

#ifdef GDB_TARGET_IS_HPUX

void
elfread_dump_compile_map (struct objfile *objfile)
{
  int i;
  char *language;
  char *dirname;

  printf_filtered ("Compile map for %s:\n", objfile->name);
  for (i = 0; i < COMPMAP (objfile)->n_cr; i++)
    {
      if (COMPMAP (objfile)->cr[i].lang == language_c)
	language = "C";
      else if (COMPMAP (objfile)->cr[i].lang == language_cplus)
	language = "C++";
      else if (COMPMAP (objfile)->cr[i].lang == language_fortran)
	language = "FORTRAN";
      else
	language = "Unknown language";

      dirname = STRINGS (objfile) + COMPMAP (objfile)->cr[i].dirname;
      if (!(*dirname))
	dirname = "Unknown compilation directory";

      printf_filtered ("  %s\n    %s\n    %s\n    %s\n",
	      STRINGS (objfile) + COMPMAP (objfile)->cr[i].objname,
	      STRINGS (objfile) + COMPMAP (objfile)->cr[i].srcname,
	      dirname,
	      language);
    }
}

static struct subspace_to_cr
 {
   int name;
   int objid;
 } * subspace_to_cr = 0;

static int subspace_to_cr_index = 0;

/* Poorva: For ld -r links - links in which one creates a .o 
   e.g ld -r a.o b.o -o c.o 
   aCC -g c.o d.o   
   the notes section from the compiler does not contain information
   about the intermediate .o files e.g c.o.
   The linkmap too does not contain entries for c.o. 
   However the routine LM_get_file_entries does contain information
   about these intermediate .o's.
   Since we use this routine to determine the number of elements
   in the arrays SECTIONMAP, NOTEMAP etc. we are left with holes
   in these data structures. 

   For maintaining a one to one correspondence between objids in 
   the SECTIONMAP and array indices of the COMPMAP (from the compiler 
   notes section) we would like to have the array index of the COMPMAP 
   to be the same as that of the NOTEMAP even though it leaves us 
   with holes in the COMPMAP.

   For IA64 we use another parameter idx which decides what array index
   to add the entry to in the COMPMAP. We decide this based on the 
   objname_idx (string index of the file) and the objid of the NOTEMAP
   that the objname_idx belongs to. It is also the index of the entry 
   in the file_info_to_name array that corresponds to objname_idx.
   We nevertheless increment n_cr to tell us how many actual number 
   of entries are present. 
   Note that the there will be holes in the NOTEMAP depending on if the
   objname_idx belongs to the resulting file of an ld -r link.
   e.g if an ld -r link happened and 
   file_info_to_name[4] = resulting .o file
   file_info_to_name[5] = constituent .o file

   The notes section of the exe will not contain information about the
   resulting .o file. It has information only about the constituent files.
   So COMPMAP (objfile)->cr[4] will be empty.
   It has been memset to -1 in the build_compile_map routine.
   
   The SECTIONMAP contains a bucket for each .o. The index of the 
   bucket gives us an index into the COMPMAP array of cr's 
   (compilation records) 
   
   The SUBSPACEMAP contains entries - each of which has an 
   objid field. We want to use that objid to index into the 
   COMPMAP array. Hence we use idx to determine which array
   index does the srcname etc go into. 
   
   The NOTEMAP which is information about the .o files that
   we get from the linkmap entries. Note - not from the 
   LM_get_file_entry routine. 
   When we build the NOTEMAP in elfread_build_partial_subspace_map
   which is done before this - we add the entries to the 
   index we get from the linkmap's subspace map record. 
   These entries may not be contiguous due to the presence
   of ld -r links. 
   We also want information about the source files (obtained
   from the notes section of the exe to be at the same array 
   index as the array index of the NOTEMAP from which the 
   .o information is obtained. 
   
   So we use the array index of the NOTEMAP as the idx parameter
   to this routine and add the source file information
   to the same index as the array index in the NOTEMAP that the 
   .o information got added to. 
   And also at the same index that the SECTIONMAP's bucket index.
   and the SUBSPACEMAP's entries have the objid as. 

   Later in hp-psymtab-read.c there is a temprorary structure 
   partial_symbol_map and the index of that array too can be used
   to index into the COMPMAP
*/

void
elfread_add_comp_map_entry (struct objfile *objfile,
			    int objname_idx,
			    int idx,
			    char *srcname,
			    char *dirname,
			    char *language)
{
  int n, len;
  int objidx, srcidx, diridx;
  char *s;

  /* If there isn't a filename we don't want to add it to the strings
     table or to the comp map entries. */
  if (!strlen(srcname))
   return;

  /* add srcname and dirname to the strings table */
  len = (int) (strlen (srcname) + strlen (dirname) + 2);
  if (STRINGS (objfile) == NULL)
    {
      n = (len > INIT_STRING_TABLE_SIZE) ? len : INIT_STRING_TABLE_SIZE;
      STRINGS (objfile) = xmalloc (n);
      if (!STRINGS (objfile))
	error ("Out of memory\n");
      string_table_limit = 0;
      string_table_size = n;
    }
  else if (string_table_limit + len > string_table_size)
    {
      n = (len > string_table_size) ?
	string_table_size + len :
        string_table_size + STRING_TABLE_DELTA;
      STRINGS (objfile) = xrealloc (STRINGS (objfile), n);
      if (!STRINGS (objfile))
	error ("Out of memory\n");
      string_table_size = n;
    }

  strcpy (STRINGS (objfile) + string_table_limit, srcname);
  srcidx = string_table_limit;
  string_table_limit += strlen (srcname) + 1;
  strcpy (STRINGS (objfile) + string_table_limit, dirname);
  diridx = string_table_limit;
  string_table_limit += strlen (dirname) + 1;

#ifndef HP_IA64
  if (COMPMAP (objfile)->n_cr >= COMPMAP (objfile)->max_cr)
    {
      n = 2 * COMPMAP (objfile)->max_cr;
      COMPMAP (objfile) =
	(struct comp_map *) xrealloc (COMPMAP (objfile),
				      sizeof (struct comp_map) +
				      sizeof (struct cr) * (n - 1));
      COMPMAP (objfile)->max_cr = n;
    }
  idx = COMPMAP (objfile)->n_cr;
#else
  if ((COMPMAP (objfile)->n_cr >= COMPMAP (objfile)->max_cr) 
      || (idx >= COMPMAP (objfile)->max_cr))
    {
      warning ("ERROR: Mismatch in number of entries in linkmap and notes"
	       "section in file %s.\n Please let your service rep know.",
	       objfile->name);
      return;
    }
#endif  

  /* which language is this? */
  /* Poorva: Make sure to check the longest first e.g ANSI C++
     before ANSI C */
  s = language;
  if (!strncasecmp (s, "HPC", 3))
    COMPMAP (objfile)->cr[idx].lang = language_c;
  else if (!strncasecmp (s, "ANSI C++", 8))
    COMPMAP (objfile)->cr[idx].lang = language_cplus;
  else if (!strncasecmp (s, "ANSI C", 6))
    COMPMAP (objfile)->cr[idx].lang = language_c;
  else if (!strncasecmp (s, "FORTRAN", 7))
    COMPMAP (objfile)->cr[idx].lang = language_fortran;
  else
    COMPMAP (objfile)->cr[idx].lang = language_unknown;
  COMPMAP (objfile)->cr[idx].objname = objname_idx;
  COMPMAP (objfile)->cr[idx].srcname = srcidx;
  COMPMAP (objfile)->cr[idx].dirname = diridx;

#ifndef HP_IA64
  subspace_to_cr[subspace_to_cr_index].name = objname_idx;
  subspace_to_cr[subspace_to_cr_index].objid = (COMPMAP (objfile)->n_cr);
  subspace_to_cr_index ++;
#endif

  (COMPMAP (objfile)->n_cr)++;
}

void
elfread_build_compile_map_64 (struct objfile *objfile,
			      char *notes_buf,
			      bfd_size_type notes_size)
{
  struct comp_map *tmp;
  long i;
  int l, n, j, idx = 0;
  char *s;
  long size;
  struct note *note;
  char *desc;
  char *src_name, *dir_name, *language = "";
  int objname_idx;
  int notemapentry;
#ifndef HP_IA64
  boolean added_end_obj = false;
#endif
  if (debug_traces)
    printf_filtered ("\nBuilding compile map...\n");

#ifndef HP_IA64
  /* Changes made by Srikanth and Priya for performance tuning*/
  subspace_to_cr = xmalloc (sizeof (struct subspace_to_cr) *
                                   SUBSPACEMAP (objfile)->n_sr);
  for (i=0; i < SUBSPACEMAP (objfile)->n_sr; i++)
    subspace_to_cr[i].name = subspace_to_cr[i].objid = 0;
  subspace_to_cr_index = 0;
  n = INIT_COMP_MAP_ENTRIES;
  COMPMAP (objfile) =
    (struct comp_map *) xmalloc (sizeof (struct comp_map) +
				 sizeof (struct cr) * (n - 1));
  COMPMAP (objfile)->max_cr = n;
  COMPMAP (objfile)->n_cr = 0;
  notemapentry = notestart;
#else
  /* Since the note map is different from the subspace map
     the notestart = 0 for IA64 */
  notemapentry = notestart = 0;
  /* Since there may be holes in the NOTEMAP we need to take the 
     largest value that we allocated and not the actual number of entries*/
  noteend = (NOTEMAP (objfile)->max_sr) - 1;
  /* We would like to memset entries to -1 (except for the 
     language entry which we want as language_unknown).
     Hence we would like for a realloc not to happen and
     so we malloc everything at one go. We know the size.
     Since the 0th entry is malloc'd when we do sizeof (struct comp_map)
     we need to malloc noteend more entries for the rest of
     the struct cr (which is an array).
     */
  COMPMAP (objfile) =
    (struct comp_map *) xmalloc (sizeof (struct comp_map) +
				 sizeof (struct cr) * (noteend));

   /* JAGae79180: memseting the allocate location to prevent garbage 
      derefrencing 
   */ 
  memset(COMPMAP(objfile),
	 0,
	 (sizeof (struct comp_map) + (sizeof (struct cr) * (noteend))));
  COMPMAP (objfile)->max_cr = noteend + 1;
  COMPMAP (objfile)->n_cr = 0;
  memset (COMPMAP (objfile)->cr, -1, sizeof (struct cr) * (noteend + 1));
  for (j=0; j < (noteend +1); ++j)
    COMPMAP (objfile)->cr[j].lang = language_unknown;
#endif

  i = 0;
  while (i < notes_size)
    {
      /* RM: skip over any portions of the .notes that aren't mapped
         in the subspace info -- these are useless to us anyway */
      if ((SUBSPACEMAP (objfile)->sr[notemapentry].end <= i))
        {
          notemapentry++;
          if (notemapentry > noteend)
            break;
          else
            i = SUBSPACEMAP (objfile)->sr[notemapentry].start;
        }

      note = (struct note *)(void *) (notes_buf + i);
      if (note->type == NOTE_HP_LINKER && note->namesz == 0 &&
	  note->descsz == 40)
	{
	  /* old incorrect buggy linker foot print detected !!!.
	   * Skip current note descriptor
	   */
	  i += 64;
	  continue;
	}

      size = 3 * sizeof (long long) + note->namesz;
#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
      size += 1;
#endif
      /* desc must be on 8 byte boundary, so round up */
      if (size % 8)
	size = (size + 8) & (~0x7);
      desc = ((char *) note) + size;
      size += note->descsz;
#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
      size += 1;
#endif
      /* next note must be on an 8 byte boundary, so round up */
      if (size % 8)
	size = (size + 8) & (~0x7);

      switch ((long) note->type)
	{
	case NOTE_HP_SRC_FILE_INFO:
#ifdef HP_IA64
	  /* Use NOTEMAP instead of SUBSPACEMAP */
	  while ((notemapentry <= noteend) &&
		 (NOTEMAP (objfile)->sr[notemapentry].end <= i))
	    notemapentry++;
#else
	  while ((notemapentry <= noteend) &&
		 (SUBSPACEMAP (objfile)->sr[notemapentry].end <= i))
	    notemapentry++;
#endif
	  if (notemapentry > noteend)
	    /* RM: Can happen, there's no guarantee that there is a subspace
	     * entry for non-DOOM files
	     */
	    {
	      break;
	    }
	  
#ifdef HP_IA64
	  if (NOTEMAP (objfile)->sr[notemapentry].start > i)
	    break;
	  objname_idx = NOTEMAP (objfile)->sr[notemapentry].objname;
#else
	  if (SUBSPACEMAP (objfile)->sr[notemapentry].start > i)
	    break;
	  objname_idx = SUBSPACEMAP (objfile)->sr[notemapentry].objid;
#endif
	  src_name = desc;
	  /* adjust source and directory names */
	  s = src_name;
	  while (s && *s && (*s != '\n'))
	    s++;
	  if (*s)
	    {
	      *s = 0;
	      dir_name = ++s;
	      while (s && *s && (*s != '\n'))
		s++;
	      if (*s)
		{
		  *s = 0;
		  language = ++s;
		  while (s && *s && (*s != '\n'))
		    s++;
		  if (*s)
		    *s = 0;
		}
	      else
		{
		  language = "";
		}
	    }
	  else
	    {
	      dir_name = "";
	    }

	  if (debug_traces >= 1000)
	    {
	      printf_filtered ("  %s\n", STRINGS (objfile) + objname_idx);
	      printf_filtered ("    %s\n", src_name);
	      printf_filtered ("    %s\n", dir_name);
	      printf_filtered ("    %s\n", language);
	    }

	  
#ifndef HP_IA64
	  idx = 0;
#else
	  /* We would like to have a correspondence between the index
	     of the COMPMAP that this file makes it to and the index/objid
	     field of the psymtabs. Hence we add this new parameter to this
	     routine. The parameter about the objid determines what
	     index of the COMPMAP is information about this source file 
	     added to */
	  idx = NOTEMAP (objfile)->sr[notemapentry].objid;
#endif
	  
	  elfread_add_comp_map_entry (objfile,
				      objname_idx,
				      idx,
				      src_name,
				      dir_name,
				      language);
#ifndef HP_IA64
          if (STREQ (src_name, "end.c"))
            added_end_obj = true;
#endif
	  break;

	default:
	  break;
	}

      i += size;      
    }

#ifndef HP_IA64
  /* if /opt/langtools/lib/pa20_64/end.o has .note section, we need not look through
     strings table below. */
  if (added_end_obj) 
    return;
  /* RM: Add an entry for end.o -- it doesn't have a .note section, so we
   * don't find it in the loop above
   */
  /* Look through the strings table looking for
   * /opt/langtools/lib/pa20_64/end.o. Ugh, gross! */
  for (objname_idx = 0;
       objname_idx < string_table_limit;
       objname_idx += l + 1)
    {
      l = strlen (STRINGS (objfile) + objname_idx);
      if ((l >= 5) &&
	  STREQ (STRINGS (objfile) + objname_idx + l - 5, "end.o"))
	break;
    }

  if (objname_idx < string_table_limit)
    {
      src_name = "end.c";
      language = "";
      dir_name = "";

      if (debug_traces >= 1000)
	{
	  printf_filtered ("  %s\n", STRINGS (objfile) + objname_idx);
	  printf_filtered ("    %s\n", src_name);
	  printf_filtered ("    %s\n", dir_name);
	  printf_filtered ("    %s\n", language);
	}

      elfread_add_comp_map_entry (objfile,
				  objname_idx,
				  idx,
				  src_name,
				  dir_name,
				  language);
    }
#endif

  if (debug_traces > 1)
    printf_filtered ("...done\n");
}

void
elfread_build_compile_map_32 (struct objfile *objfile,
			      char *notes_buf,
			      bfd_size_type notes_size)
{
  struct comp_map *tmp;
  long i;
  int l, n, j, idx;
  char *s;
  long size;
  char *desc;
  char *src_name, *dir_name, *language = "";
  int objname_idx;
  int notemapentry;
  struct hp_note *note;

  if (debug_traces)
    printf_filtered ("\nBuilding compile map...\n");

#ifndef HP_IA64
  /* Changes made by Srikanth and Priya for performance tuning*/
  subspace_to_cr = xmalloc (sizeof (struct subspace_to_cr) *
                                   SUBSPACEMAP (objfile)->n_sr);
  for (i=0; i < SUBSPACEMAP (objfile)->n_sr; i++)
    subspace_to_cr[i].name = subspace_to_cr[i].objid = 0;
  subspace_to_cr_index = 0;
  n = INIT_COMP_MAP_ENTRIES;
  COMPMAP (objfile) =
    (struct comp_map *) xmalloc (sizeof (struct comp_map) +
				 sizeof (struct cr) * (n - 1));
  COMPMAP (objfile)->max_cr = n;
  COMPMAP (objfile)->n_cr = 0;
  notemapentry = notestart;
#else
  notemapentry = notestart = 0;
  noteend = (NOTEMAP (objfile)->max_sr) - 1;
  /* We would like to memset entries to -1 (except for the 
     language entry which we want as language_unknown).
     Hence we would like for a realloc not to happen and
     so we malloc everything at one go. We know the size.
     Since the 0th entry is malloc'd when we do sizeof (struct comp_map)
     we need to malloc noteend more entries for the rest of
     the struct cr (which is an array).
     */
  COMPMAP (objfile) =
    (struct comp_map *) xmalloc (sizeof (struct comp_map) +
				 sizeof (struct cr) * (noteend));
  COMPMAP (objfile)->max_cr = noteend + 1;
  COMPMAP (objfile)->n_cr = 0;
  memset (COMPMAP (objfile)->cr, -1, sizeof (struct cr) * (noteend + 1));
  for (j=0; j < (noteend +1); ++j)
    COMPMAP (objfile)->cr[j].lang = language_unknown;
#endif

  i = 0;
  while (i < notes_size)
    {
      note = (struct hp_note *)(void *) (notes_buf + i);
	            
      if (note->type == NOTE_HP_LINKER && note->namesz == 0 &&
	  note->descsz == 40)
	{
	  /* old incorrect buggy linker foot print detected !!!.
	   * Skip current note descriptor
	   */
	  i += 64;
	  continue;
	}

      size = 3 * sizeof (int32_t) + note->namesz;
#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
      size += 1;
#endif
      /* desc must be on 4 byte boundary, so round up */
      if (size % 4)
	size = (size + 4) & (~0x3);
      desc = ((char *) note) + size;
      
      size += note->descsz;
#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
      size += 1;
#endif
      /* next note must be on a 4 byte boundary, so round up */
      if (size % 4)
	size = (size + 4) & (~0x3);
      
      switch ((long) note->type)
	{
	case NOTE_HP_SRC_FILE_INFO:
#ifdef HP_IA64
	  while ((notemapentry <= noteend) &&
		 (NOTEMAP (objfile)->sr[notemapentry].end <= i))
	    notemapentry++;
#else
	  while ((notemapentry <= noteend) &&
		 (SUBSPACEMAP (objfile)->sr[notemapentry].end <= i))
	    notemapentry++;
#endif
	  if (notemapentry > noteend)
	    /* RM: Can happen, there's no guarantee that there is a subspace
	     * entry for non-DOOM files
	     */
	    {
	      break;
	    }
#ifdef HP_IA64
	  if (NOTEMAP (objfile)->sr[notemapentry].start > i)
	    break;
	  objname_idx = NOTEMAP (objfile)->sr[notemapentry].objname;
#else
	  if (SUBSPACEMAP (objfile)->sr[notemapentry].start > i)
	    break;
	  objname_idx = SUBSPACEMAP (objfile)->sr[notemapentry].objid;
#endif
	  src_name = desc;
	  /* adjust source and directory names */
	  s = src_name;
	  while (s && *s && (*s != '\n'))
	    s++;
	  if (*s)
	    {
	      *s = 0;
	      dir_name = ++s;
	      while (s && *s && (*s != '\n'))
		s++;
	      if (*s)
		{
		  *s = 0;
		  language = ++s;
		  while (s && *s && (*s != '\n'))
		    s++;
		  if (*s)
		    *s = 0;
		}
	      else
		{
		  language = "";
		}
	    }
	  else
	    {
	      dir_name = "";
	    }

	  if (debug_traces >= 1000)
	    {
	      printf_filtered ("  %s\n", STRINGS (objfile) + objname_idx);
	      printf_filtered ("    %s\n", src_name);
	      printf_filtered ("    %s\n", dir_name);
	      printf_filtered ("    %s\n", language);
	    }

#ifndef HP_IA64
	  idx = 0;
#else
	  /* We would like to have a correspondence between the index
	     of the COMPMAP that this file makes it to and the index/objid
	     field of the psymtabs. Hence we add this new parameter to this
	     routine. The parameter about the objid determines what
	     index of the COMPMAP is information about this source file 
	     added to */
	  idx = NOTEMAP (objfile)->sr[notemapentry].objid;
#endif

	  elfread_add_comp_map_entry (objfile,
				      objname_idx,
				      idx,
				      src_name,
				      dir_name,
				      language);
	  break;

	default:
	  break;
	}
      i += size;
    }
  
  if (debug_traces > 1)
    printf_filtered ("...done\n");
}


/* elfread_add_partial_subspace_map_entry */

void 
elfread_add_partial_subspace_map_entry (struct objfile *objfile,
				        unsigned int sectionname,
				        int objid,		
					int objname,
					CORE_ADDR start,
				        CORE_ADDR end,
					int info,
					unsigned int input_section_index)
{
  char *s;
  int n;

  /* Added objid - which is the object file index we get from the 
     linkmap given to us by the linker */
  if (SUBSPACEMAP (objfile)->n_sr >= SUBSPACEMAP (objfile)->max_sr)
    {
      n = 2 * SUBSPACEMAP (objfile)->max_sr;
      SUBSPACEMAP (objfile) =
	(struct subspace_map *) xrealloc (SUBSPACEMAP (objfile),
					  sizeof (struct subspace_map) +
					  sizeof (struct sr) * (n - 1));
      SUBSPACEMAP (objfile)->max_sr = n;
    }

  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].spacename = -1;

  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].subspacename =
    sectionname;
  /* RM: hack: we'll later replace objname with an objid (an index
   * into the compilation map) */
#ifdef HP_IA64
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].objname = objname;
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].objid = objid;
#else
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].objid = objname;
#endif
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].start = start;
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].end = end;
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].info = info;
  SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].input_section_index =
    input_section_index;

  (SUBSPACEMAP (objfile)->n_sr)++;
}

/* Adds an entry to the debug map - which contains entries from the
 objdebug type and const tables */

void
elfread_add_partial_debug_map_entry (struct objfile *objfile,
				     unsigned int sectionname,
				     int objid,
				     int objname,
				     CORE_ADDR start,
				     CORE_ADDR end,
				     int info,
				     unsigned int input_section_index)
{
  char *s;
  int n;

  if (DEBUGMAP (objfile)->n_sr >= DEBUGMAP (objfile)->max_sr)
    {
      n = 2 * DEBUGMAP (objfile)->max_sr;
      DEBUGMAP (objfile) =
        (struct subspace_map *) xrealloc (DEBUGMAP (objfile),
                                          sizeof (struct subspace_map) +
                                          sizeof (struct sr) * (n - 1));
      DEBUGMAP (objfile)->max_sr = n;
    }

  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].spacename = -1;

  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].subspacename =
    sectionname;
  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].objid = objid;
#ifdef OBJFILE_HAS_LINKMAP
  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].objname = objname;
#endif
  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].start = start;
  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].end = end;
  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].info = info;
  DEBUGMAP (objfile)->sr[DEBUGMAP (objfile)->n_sr].input_section_index =
    input_section_index;
 
  (DEBUGMAP (objfile)->n_sr)++;
}

/* Adds an entry to the note map - which contains entries from the
   notes section */

void 
elfread_add_partial_note_map_entry (struct objfile *objfile,
				    unsigned int sectionname,
				    int objid,
				    int objname,
				    CORE_ADDR start,
				    CORE_ADDR end,
				    int info,
				    unsigned int input_section_index)
{
  char *s;
  int n;

  if (NOTEMAP (objfile)->n_sr >= NOTEMAP (objfile)->max_sr)
    {
      n = 2 * NOTEMAP (objfile)->max_sr;
      NOTEMAP (objfile) =
	(struct subspace_map *) xrealloc (NOTEMAP (objfile),
					  sizeof (struct subspace_map) +
					  sizeof (struct sr) * (n - 1));
      NOTEMAP (objfile)->max_sr = n;
    }

  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].spacename = -1;

  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].subspacename =
    sectionname;
#ifdef OBJFILE_HAS_LINKMAP
  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].objname = objname;
#endif
  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].objid = objid;
  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].start = start;
  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].end = end;
  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].info = info;
  NOTEMAP (objfile)->sr[NOTEMAP (objfile)->n_sr].input_section_index =
    input_section_index;

  (NOTEMAP (objfile)->n_sr)++;
}

/* Check - is the first objid a 0 or a 1 */
/* Adds an entry to the bucket that the entry belongs to.
   If the entry is a text section entry then also determines
   if it changes the textlow and text high.
*/

void 
elfread_add_partial_section_map_entry (struct objfile *objfile,
				       unsigned int sectionname,
				       int objid,	
				       int objname,
				       CORE_ADDR start,
				       CORE_ADDR end,	
				       int info,
				       unsigned int input_section_index)
{
  char *s;
  int n;

  if (SECTIONMAP (objfile)->bucket[objid]->n_sr >= 
      SECTIONMAP (objfile)->bucket[objid]->max_sr)
    {
      n = 2 * SECTIONMAP (objfile)->bucket[objid]->max_sr;
      SECTIONMAP (objfile)->bucket[objid] = 
	(struct bucket *) xrealloc (SECTIONMAP (objfile)->bucket[objid],
				    sizeof (struct bucket) +
				    sizeof (struct sr) * (n - 1));
      SECTIONMAP (objfile)->bucket[objid]->max_sr = n;
    }

  n = SECTIONMAP (objfile)->bucket[objid]->n_sr;
  SECTIONMAP (objfile)->bucket[objid]->sr[n].spacename = -1;

  SECTIONMAP (objfile)->bucket[objid]->sr[n].subspacename =
    sectionname;
#ifdef OBJFILE_HAS_LINKMAP
  SECTIONMAP (objfile)->bucket[objid]->sr[n].objname = objname;
#endif
  SECTIONMAP (objfile)->bucket[objid]->sr[n].objid = objid;
  SECTIONMAP (objfile)->bucket[objid]->sr[n].start = start;
  SECTIONMAP (objfile)->bucket[objid]->sr[n].end = end;
  SECTIONMAP (objfile)->bucket[objid]->sr[n].info = info;
  SECTIONMAP (objfile)->bucket[objid]->sr[n].input_section_index =
    input_section_index;

  if (info == TEXT_SECTION)
    {
      if (start < SECTIONMAP (objfile)->bucket[objid]->textlow)
	SECTIONMAP (objfile)->bucket[objid]->textlow = start;
      else if (SECTIONMAP (objfile)->bucket[objid]->textlow == 0)
	SECTIONMAP (objfile)->bucket[objid]->textlow = start;
      
      if (end > SECTIONMAP (objfile)->bucket[objid]->texthigh)
	SECTIONMAP (objfile)->bucket[objid]->texthigh = end;
    }
  (SECTIONMAP (objfile)->bucket[objid]->n_sr)++;
}
#endif


/* For IA64 compare two subspace entries by starting offset of the 
   section. The linkmap is a mapping of which section of the executable
   did the section of the .o make it to. The start address is the 
   link time starting address of the section in the executable. 
   For PA compare two subspace entries - first by section type, 
   and then by starting offset.
 */

int 
elfread_compare_subspace_entries (struct sr *s1, struct sr *s2)
{
#ifdef HP_IA64
  if (s1->start < s2->start)
    return -1;
  else if (s1->start > s2->start)
    return 1;
  else
    return 0;
#else
  if (s1->info < s2->info)
    return -1;
  else if (s1->info > s2->info)
    return 1;
  else if (s1->start < s2->start)
    return -1;
  else if (s1->start > s2->start)
    return 1;
  else
    return 0;
#endif
}


/* Compare two section map entries by the input section index which
   is the index of section in the .o. When we apply relocations to 
   each symbol in the .o we know the which section did it come from 
   in the .o. We then find what is the starting address in the executable 
   of this section. We thus need these sorted on the input section. 
 */

int 
elfread_compare_section_entries (struct sr *s1, struct sr *s2)
{
  if (s1->input_section_index < s2->input_section_index)
    return -1;
  else if (s1->input_section_index > s2->input_section_index)
    return 1;
  else
    return 0;
}

int info, objid = 0;
char *linkmap_buf;
char *linkmap_bss_buf;
char *linkmap_file_buf;
int linkmap_size;
int linkmap_bss_size;
int linkmap_file_size;
int linkmap_next_byte;
int linkmap_bss_next_byte;
int linkmap_file_next_byte;

void
elfread_build_partial_subspace_map (struct objfile *objfile,
				    bfd *abfd,
				    char *linkmap_strings,
				    int type_table_size,
				    int const_table_size)
{
  Elf_Internal_Shdr *hdr;
  CORE_ADDR start = 0;
  CORE_ADDR end = 0;
  int i;
  int section;
  Elf64_Internal_Shdr *sect_ptr;
  int sectionname, objname, last_objname = -1, last_objid = -1;
  unsigned int input_section_index;
  unsigned int l, m, n, p;
  int textstart, datastart, textend, dataend;
  int debugstart, debugend;
  CORE_ADDR section_start;
  CORE_ADDR text_offset, data_offset;
  char *current_section;
  int info;
  LM_mapping_info subspace_map_record;
  LM_sym_info bss_map_record;
  LM_file_entry_info objinfo;
  LM_errcode err;
  int *file_info_to_name;
  Elf64_External_Sym *syms;
  struct subspace_map *tmp;
  int file_index = -1, prev_file_index = -1, n1, n2;
  int *ptr_to_file_index = NULL;

  text_offset = ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
  data_offset = ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile));

  if (debug_traces)
    printf_filtered ("Building partial subspace map...\n");

  n = INIT_SUBSPACE_MAP_ENTRIES;
  SUBSPACEMAP (objfile) = (struct subspace_map *)
    xmalloc (sizeof (struct subspace_map) +
	     sizeof (struct sr) * (n - 1));
  bzero (SUBSPACEMAP (objfile), 
	 sizeof (struct subspace_map) + sizeof (struct sr) * (n - 1));
  SUBSPACEMAP (objfile)->max_sr = n;
  SUBSPACEMAP (objfile)->n_sr = 0;

  /* RM: convert file info entries to old style DOOM file  names */
  err = LM_get_num_files (&m);
  if (err)
    error ("Reading file info from link map\n");

#ifdef HP_IA64
  /* Allocate space for the debug map */
  n1 = INIT_DEBUG_MAP_ENTRIES;
  DEBUGMAP (objfile) = (struct subspace_map *)
    xmalloc (sizeof (struct subspace_map) +
	     sizeof (struct sr) * (n1 - 1));
  DEBUGMAP (objfile)->max_sr = n1;
  DEBUGMAP (objfile)->n_sr = 0;
  

  /* Allocate space for the note map - m tells us the exact number 
     of .o's in that objfile which is what we use to allocate space.*/
  NOTEMAP (objfile) = (struct subspace_map *)
    xmalloc (sizeof (struct subspace_map) +
	     sizeof (struct sr) * (m - 1));
  NOTEMAP (objfile)->max_sr = m;
  NOTEMAP (objfile)->n_sr = 0;

  /* Allocate space for the section map and its buckets. The number of
     buckets is equal to the number of .o's. m tells us the number 
     of .o's in that objfile which is what we use to allocate space.*/
  SECTIONMAP (objfile) = (struct section_map *)
    xmalloc (sizeof (struct section_map) +
	     sizeof (struct bucket) * (m - 1));
#else
  SECTIONMAP (objfile) = (struct section_map *)
    xmalloc (sizeof (struct section_map) +
	     sizeof (CORE_ADDR) * (m - 1));
#endif

  SECTIONMAP (objfile)->n_sr = m;
  for (i = 0; i < m; ++i)
    {
      n = INIT_SECTION_MAP_BUCKET_ENTRIES;
      SECTIONMAP (objfile)->bucket[i] = 
	(struct bucket *) xmalloc ( sizeof (struct bucket) +
				    sizeof (struct sr) * (n - 1));
      memset (SECTIONMAP (objfile)->bucket[i], 0, sizeof (struct bucket)); 
      SECTIONMAP (objfile)->bucket[i]->max_sr = n;
    }

  /* file_info_to_name - Array of integers. 
   * The integers are offsets into the string table.
   * and contain file names. We create this array for objdebug only by
   * calling a linkmap library routine LM_get_file_entry.
   * For ld -r links this will contain information about both the
   * constituent files and the final .o created. The notes sections 
   * however do not contain entries for the .o resulting from the 
   * ld -r link. 
   *
   * We use this array to decide the index to add things to in the 
   * COMPMAP and store the index in the objid field of the NOTEMAP,
   * SUBSPACEMAP, SECTIONMAP etc.
   */

  file_info_to_name = (int *) xmalloc (m * sizeof (int));
  for (i = 0; i < m; i++)
    {
      err = LM_get_file_entry (i, &objinfo);
      if (err)
	error ("Reading file info from link map\n");
      if (!objinfo.is_archive)
	{
	  /* RM: non-archive case */

	  l = 0;
	  if ((objinfo.is_basename &&
	       (*(linkmap_strings + objinfo.dir_name) != '/')) ||
	      (!objinfo.is_basename &&
	       (*(linkmap_strings + objinfo.file_name) != '/')))
	    l += strlen (linkmap_strings + objinfo.link_dir) + 1;
	  if (objinfo.is_basename)
	    l += strlen (linkmap_strings + objinfo.dir_name) + 1;
	  l += strlen (linkmap_strings + objinfo.file_name) + 1;

	  /* RM: make sure we have enough space in the strings table
	   * for this filename */

	  if (STRINGS (objfile) == NULL)
	    {
	      p = (l > INIT_STRING_TABLE_SIZE) ? l :
		INIT_STRING_TABLE_SIZE;
	      STRINGS (objfile) = xmalloc (p);
	      if (!STRINGS (objfile))
		error ("Out of memory\n");
	      string_table_limit = 0;
	      string_table_size = p;
	    }
	  else if (string_table_limit + l > string_table_size)
	    {
	      p = (l > string_table_size) ?
		string_table_size + l :
                string_table_size + STRING_TABLE_DELTA;
	      STRINGS (objfile) = xrealloc (STRINGS (objfile), p);
	      if (!STRINGS (objfile))
		error ("Out of memory\n");
	      string_table_size = p;
	    }

	  file_info_to_name[i] = string_table_limit;
	  if ((objinfo.is_basename &&
	       (*(linkmap_strings + objinfo.dir_name) != '/')) ||
	      (!objinfo.is_basename &&
	       (*(linkmap_strings + objinfo.file_name) != '/')))
	    {
	      strcpy (STRINGS (objfile) + string_table_limit,
		      linkmap_strings + objinfo.link_dir);
	      string_table_limit +=
		strlen (linkmap_strings + objinfo.link_dir) + 1;
	      *(STRINGS (objfile) + string_table_limit - 1) = '/';
	    }
	  if (objinfo.is_basename)
	    {
	      strcpy (STRINGS (objfile) + string_table_limit,
		      linkmap_strings + objinfo.dir_name);
	      string_table_limit +=
		strlen (linkmap_strings + objinfo.dir_name) + 1;
	      *(STRINGS (objfile) + string_table_limit - 1) = '/';
	    }
	  strcpy (STRINGS (objfile) + string_table_limit,
		  linkmap_strings + objinfo.file_name);
	  string_table_limit +=
	    strlen (linkmap_strings + objinfo.file_name) + 1;

	  if (debug_traces >= 10000)
	    printf_filtered ("Constructed strings entry for file: %s\n",
		    STRINGS (objfile) + file_info_to_name[i]);
	}
      else
	{
	  /* RM: archive case */
	  l = 0;
	  if (*(linkmap_strings + objinfo.dir_name) != '/')
	    l += strlen (linkmap_strings + objinfo.link_dir) + 1;
	  l += strlen (linkmap_strings + objinfo.dir_name);
	  l += strlen (linkmap_strings + objinfo.file_name) + 3;

	  /* RM: make sure we have enough space in the strings table
	   * for this filename */

	  if (STRINGS (objfile) == NULL)
	    {
	      p = (l > INIT_STRING_TABLE_SIZE) ? l :
		INIT_STRING_TABLE_SIZE;
	      STRINGS (objfile) = xmalloc (p);
	      if (!STRINGS (objfile))
		error ("Out of memory\n");
	      string_table_limit = 0;
	      string_table_size = p;
	    }
	  else if (string_table_limit + l > string_table_size)
	    {
	      p = (l > string_table_size) ?
		string_table_size + l :
		2 * string_table_size;
	      STRINGS (objfile) = xrealloc (STRINGS (objfile), p);
	      if (!STRINGS (objfile))
		error ("Out of memory\n");
	      string_table_size = p;
	    }

	  file_info_to_name[i] = string_table_limit;
	  if (*(linkmap_strings + objinfo.dir_name) != '/')
	    {
	      strcpy (STRINGS (objfile) + string_table_limit,
		      linkmap_strings + objinfo.link_dir);
	      string_table_limit +=
		strlen (linkmap_strings + objinfo.link_dir) + 1;
	      *(STRINGS (objfile) + string_table_limit - 1) = '/';
	    }
	  strcpy (STRINGS (objfile) + string_table_limit,
		  linkmap_strings + objinfo.dir_name);
	  string_table_limit +=
	    strlen (linkmap_strings + objinfo.dir_name) + 1;
	  *(STRINGS (objfile) + string_table_limit - 1) = '(';
	  strcpy (STRINGS (objfile) + string_table_limit,
		  linkmap_strings + objinfo.file_name);
	  string_table_limit +=
	    strlen (linkmap_strings + objinfo.file_name) + 2;
	  *(STRINGS (objfile) + string_table_limit - 2) = ')';
	  *(STRINGS (objfile) + string_table_limit - 1) = 0;

	  if (debug_traces >= 10000)
	    printf_filtered ("Constructed strings entry for file: %s\n",
		    STRINGS (objfile) + file_info_to_name[i]);

	}
    }

  while (1)
    {
      err = LM_get_next_mapping (&subspace_map_record);
      /* RM: ??? A particular error code indicates no more
       * entries. Don't know which, so just break on all of them. Check
       * with Dmitry, so we can detect problems other than end of
       * entries
       */
      if (err)
	break;

      /* RM: not interested in things other than input sections and
         linker bss's */
      if ((subspace_map_record.mapping_type != LM_INPUT_SECTION) &&
	  (subspace_map_record.mapping_type != LM_LINKER_BSS))
	continue;

      section = subspace_map_record.output_info;
      sect_ptr = elf_elfsections (abfd)[section];
      sectionname = sect_ptr->sh_name;
      objname = file_info_to_name[subspace_map_record.file_index];
      start = subspace_map_record.offset;
      end = start + subspace_map_record.size;
      input_section_index = subspace_map_record.input_info;

#ifdef HP_IA64
      /* Record the .o index from the file_info_to_name array */
      file_index = subspace_map_record.file_index;
#endif

      /* Poorva: 
	 Also use sect_ptr instead of elf_elfsections (abfd)[section]
	 Helps the optimizer from doing the aray indexing 
	 addition multiple times. 
      */

      current_section = STRINGS (objfile) + sectionname;

      if (sect_ptr->sh_flags & (Elf64_Xword) SHF_EXECINSTR)
	{
	  info = TEXT_SECTION;
	  section_start = sect_ptr->sh_addr;
/* MERGE: Adding the offset is not required on PA. */	  
	  start += UNSWIZZLE (section_start) 
#ifdef HP_IA64
		+ UNSWIZZLE (text_offset)
#endif
		;
	  end += UNSWIZZLE (section_start) 
#ifdef HP_IA64
		+ UNSWIZZLE (text_offset)
#endif
		;
	  start = SWIZZLE (start);
	  end = SWIZZLE (end);
	  /* For the performance gdb we require all text symbols like
	     functions etc to also be added to the partial subspace 
	     map entry since we no longer have fat free psymtabs */
#ifdef HASH_ALL
	  elfread_add_partial_subspace_map_entry (objfile,
						  sectionname,
						  file_index,
						  objname,
						  (CORE_ADDR) start,
						  (CORE_ADDR) end,
						  info,
						  input_section_index);
#endif	  
	}
      else if (sect_ptr->sh_flags & (Elf64_Xword) SHF_WRITE)
	{
	  info = DATA_SECTION;
	  section_start = sect_ptr->sh_addr;
	  if (sect_ptr->sh_flags & SHF_HP_TLS)
	    {
	      uint64_t tptr_value = section_start +
				    ANOFFSET (objfile->section_offsets, 1);
	      /* To determine the tptr_value, gdb should really be
		 looking into program headers and not section headers.
		 The following should also work.
	      */
	      if (objfile->tptr_value == 0 || 
                  tptr_value < objfile->tptr_value)
                objfile->tptr_value = tptr_value;
	    }

/* MERGE: Adding the offset is not required on PA. */
	  start += UNSWIZZLE (section_start) 
#ifdef HP_IA64
	  + UNSWIZZLE (data_offset)
#endif
	  ;
	  end += UNSWIZZLE (section_start) 
#ifdef HP_IA64
	  + UNSWIZZLE (data_offset)
#endif
	  ;
	  start = SWIZZLE (start);
	  end = SWIZZLE (end);
#ifdef HP_IA64
	  elfread_add_partial_subspace_map_entry (objfile,
						  sectionname,
						  file_index,
						  objname,
						  (CORE_ADDR) start,
						  (CORE_ADDR) end,
						  info,
						  input_section_index);
#endif
	}
      else if (sect_ptr->sh_type == SHT_NOTE)
	{ /* Poorva: Check flag for notes section instead of doing strcmp */
	  info = NOTE_SECTION;
#ifdef HP_IA64
	  elfread_add_partial_note_map_entry (objfile,
					      sectionname,
					      file_index,
					      objname,
					      (CORE_ADDR) start,
					      (CORE_ADDR) end,
					      info,
					      input_section_index);
#endif
	}
      else if (   (sect_ptr->sh_type == SHT_PROGBITS)
	       && (sect_ptr->sh_flags == 0)
	       && STREQSPACE (current_section, ".objdebug_type"))
	    {
	      info = DEBUG_SECTION;
#ifdef HP_IA64
	      elfread_add_partial_debug_map_entry (objfile,
						   sectionname,
						   file_index,
						   objname,
						   (CORE_ADDR) start,
						   (CORE_ADDR) end,
						   info,
						   input_section_index);
#endif
	    }
	  else if (   (sect_ptr->sh_type == SHT_PROGBITS)
		   && (sect_ptr->sh_flags == 0)
	           && STREQSPACE (current_section, ".objdebug_const"))
	    {
	      info = DEBUG_SECTION;
	      start += type_table_size;
	      end += type_table_size;
#ifdef HP_IA64
	      elfread_add_partial_debug_map_entry (objfile,
						   sectionname,
						   file_index,
						   objname,
						   (CORE_ADDR) start,
						   (CORE_ADDR) end,
						   info,
						   input_section_index);
#endif
	    }
	  else if (   (sect_ptr->sh_type == SHT_PROGBITS)
		   && (sect_ptr->sh_flags == 0)
	           && STREQSPACE (current_section, ".objdebug_file"))
	    {
	      info = DEBUG_SECTION;
	      start += type_table_size + const_table_size;
	      end += type_table_size + const_table_size;
	    }
      else
	{
	  info = OTHER_SECTION;
	  section_start = sect_ptr->sh_addr;
	  start += section_start;
	  end += section_start;
	}

      if (debug_traces >= 1000)
	{
	  printf_filtered ("  %s [%016llx-%016llx]: %s\n",
		  STRINGS (objfile) + sectionname,
		  (long long) start,
		  (long long) end,
		  STRINGS (objfile) + objname);
	}
#ifdef HP_IA64
      if (info != NOTE_SECTION)
	elfread_add_partial_section_map_entry (objfile,
					       sectionname,
					       file_index,
					       objname,
					       (CORE_ADDR) start,
					       (CORE_ADDR) end,
					       info,
					       input_section_index);
#else
     elfread_add_partial_subspace_map_entry (objfile,
					      sectionname,
					      file_index,
					      objname,
					      (CORE_ADDR) start,
					      (CORE_ADDR) end,
					      info,
					      input_section_index); 
#endif
    }



/* RM: Hmmm, I think these are only used in ld -r links, and never
 * make it to the final execuatble
 */
#if 0
  /* RM: Deal with BSS symbols */
  /* ??? Completely untested */
  if (linkmap_bss_buf)
    {
      /* first read symbol table */
      hdr = &elf_tdata (abfd)->symtab_hdr;
      if (bfd_seek (abfd, hdr->sh_offset, SEEK_SET) == -1)
	error ("error reading symbol table");

      /* Temporarily allocate room for the raw ELF symbols.  */
      syms = (Elf64_External_Sym *) xmalloc (hdr->sh_size);

      if (!syms)
	error ("Out of memory\n");

      if (bfd_read (syms, 1, hdr->sh_size, abfd) != hdr->sh_size)
	error ("error reading symbol table");

      while (1)
	{
	  err = LM_get_next_bss_symbol (&bss_map_record);
	  /* RM: ??? A particular error code indicates no more
	   * entries. Don't know which, so just break on all of them. Check
	   * with Dmitry, so we can detect problems other than end of
	   * entries
	   */
	  if (err)
	    break;

	  objname = file_info_to_name[bss_map_record.file_index];

	  start = syms[bss_map_record.sym_index].st_value;
	  end = start + syms[bss_map_record.sym_index].st_size;
	  info = DATA_SECTION;

	  if (debug_traces >= 1000)
	    {
	      printf_filtered ("  %s [%016llx-%016llx]: %s\n",
		      "bss symbol",
		      (long long) start,
		      (long long) end,
		      STRINGS (objfile) + objname);
	    }
	  elfread_add_partial_subspace_map_entry (objfile,
						  -1,
						  objid,
						  objname,
						  (CORE_ADDR) start,
						  (CORE_ADDR) end,
						  info);
	}

      free (syms);
    }
#endif

  n = SUBSPACEMAP (objfile)->n_sr;
  n1 = (NOTEMAP (objfile) ? NOTEMAP (objfile)->n_sr : NULL);
  n2 = (DEBUGMAP (objfile) ? DEBUGMAP (objfile)->n_sr : NULL);
  
  /* Now sort the subspace map so we can perform binary searches on it */
  /* which contains only the dataspace entries */
  qsort (&(SUBSPACEMAP (objfile)->sr[0]), n, sizeof (struct sr),
    (int (*)(const void *, const void *)) elfread_compare_subspace_entries);
  qsort (&(NOTEMAP (objfile)->sr[0]), n1, sizeof (struct sr),
    (int (*)(const void *, const void *)) elfread_compare_subspace_entries);
  qsort (&(DEBUGMAP (objfile)->sr[0]), n2, sizeof (struct sr),
    (int (*)(const void *, const void *)) elfread_compare_subspace_entries);

#ifndef HP_IA64
  /* Identify text and data portions of the subspace map, so we have
   * something to perform binary searches on. Also patch virtal
   * addresses with section offsets
   */
  /* We don't do this for IA64 - since we have different data structures
     for each portion of the subspace map. For a large app like i2 which 
     could easily have 1.5 million entries in the subspace map this would 
     mean one more iteration over that many entries. Also we swizzle and 
     unswizzle when we enter the entries into the map.
     */

  textstart = textend = datastart = dataend = -1;
  debugstart = debugend = -1;
  notestart = noteend = -1;
  for (i = 0; i < n; i++)
    {
      if (SUBSPACEMAP (objfile)->sr[i].info == TEXT_SECTION)
	{
	  if (textstart == -1)
	    textstart = i;
	  textend = i;
	  SUBSPACEMAP (objfile)->sr[i].start += text_offset;
	  SUBSPACEMAP (objfile)->sr[i].end += text_offset;
	}
      else if (SUBSPACEMAP (objfile)->sr[i].info == DATA_SECTION)
	{
	  CORE_ADDR addr;
	  if (datastart == -1)
	    datastart = i;
	  dataend = i;
	  addr = 
	    UNSWIZZLE (SUBSPACEMAP (objfile)->sr[i].start) + UNSWIZZLE(data_offset);
	  SUBSPACEMAP (objfile)->sr[i].start = SWIZZLE (addr);
	  addr = 
	    UNSWIZZLE (SUBSPACEMAP (objfile)->sr[i].end) + UNSWIZZLE (data_offset);
	  SUBSPACEMAP (objfile)->sr[i].end = SWIZZLE (addr);
	}
      else if (SUBSPACEMAP (objfile)->sr[i].info == DEBUG_SECTION)
	{
	  if (debugstart == -1)
	    debugstart = i;
	  debugend = i;
	}
      else if (SUBSPACEMAP (objfile)->sr[i].info == NOTE_SECTION)
	{
	  if (notestart == -1)
	    notestart = i;
	  noteend = i;
	}
    }

  SUBSPACEMAP (objfile)->textstart = textstart;
  SUBSPACEMAP (objfile)->textend = textend;
  SUBSPACEMAP (objfile)->datastart = datastart;
  SUBSPACEMAP (objfile)->dataend = dataend;
  SUBSPACEMAP (objfile)->debugstart = debugstart;
  SUBSPACEMAP (objfile)->debugend = debugend;
#endif
  
  free (file_info_to_name);

  if (debug_traces > 1)
    printf_filtered ("...done\n");
}

char *
linkmap_buffer (unsigned int *size_p)
{
  if (linkmap_next_byte == 0)
    {
      *size_p = linkmap_size;
      linkmap_next_byte = linkmap_size;
      return linkmap_buf;
    }
  else
    {
      *size_p = 0;
      return 0;
    }
}

char *
linkmap_bss_buffer (unsigned int *size_p)
{
  if (linkmap_bss_next_byte == 0)
    {
      *size_p = linkmap_bss_size;
      linkmap_bss_next_byte = linkmap_bss_size;
      return linkmap_bss_buf;
    }
  else
    {
      *size_p = 0;
      return 0;
    }
}

char *
linkmap_file_buffer (unsigned int *size_p)
{
  if (linkmap_file_next_byte == 0)
    {
      *size_p = linkmap_file_size;
      linkmap_file_next_byte = linkmap_file_size;
      return linkmap_file_buf;
    }
  else
    {
      *size_p = 0;
      return 0;
    }
}

void 
linkmap_read_finish (void *ignore)
{
  LM_end_read_linkmap ();
  LM_end_read_linkmap_bss ();
  LM_end_read_linkmap_file ();
}

static int
compare_subspace_to_cr_entries (void *p1, void *p2)
{
  struct subspace_to_cr * s1, *s2;

  s1 = (struct subspace_to_cr *) p1;
  s2 = (struct subspace_to_cr *) p2;

  if (s1->name < s2->name)
    return -1;
  else
  if (s1->name > s2->name)
    return 1;

  return 0;
}

/* replace objnames (offset into strings table) with objids (index
   compilation map)
 */
void
elfread_complete_subspace_map (struct objfile *objfile)
{
  int i;
  int j;

  char filename[1024];
  struct subspace_to_cr key, *result;

  /* srikanth, 000207, we spend a significant amount of the scp engine
     startup time here. This loop is simplified now.
  */

  qsort (subspace_to_cr, subspace_to_cr_index,
               sizeof (struct subspace_to_cr),
               (int (*)(const void *,const void *))
		 compare_subspace_to_cr_entries);

  for (i = 0; i < SUBSPACEMAP (objfile)->n_sr; i++)
    {
      key.name = SUBSPACEMAP (objfile)->sr[i].objid;
      result = bsearch (&key, subspace_to_cr,
                              subspace_to_cr_index,
                              sizeof (struct subspace_to_cr),
                              (int (*)(const void *,const void *))
				compare_subspace_to_cr_entries);
      if (result)
        SUBSPACEMAP (objfile)->sr[i].objid = result->objid;
      else 
        SUBSPACEMAP (objfile)->sr[i].objid = -1;
    }
}

void
elfread_print_subspace_map_entry (struct objfile *objfile, int i)
{
  printf ("    %s [%016llx-%016llx]: %s\n",
	  (SUBSPACEMAP (objfile)->sr[i].subspacename == -1) ?
	  "" :
	  STRINGS (objfile) + SUBSPACEMAP (objfile)->sr[i].subspacename,
	  SUBSPACEMAP (objfile)->sr[i].start,
	  SUBSPACEMAP (objfile)->sr[i].end,
	  (SUBSPACEMAP (objfile)->sr[i].objid == -1) ?
	  "" :
	  STRINGS (objfile) +
       (COMPMAP (objfile)->cr[SUBSPACEMAP (objfile)->sr[i].objid].objname));
}

void
elfread_dump_subspace_map (struct objfile *objfile)
{
  int i;

  printf_filtered ("Subspace map for %s:\n", objfile->name);

#ifndef HP_IA64
  printf_filtered ("  Text section map:\n");
  /* First print map for text space */
  for (i = SUBSPACEMAP (objfile)->textstart;
       i <= SUBSPACEMAP (objfile)->textend;
       i++)
    {
      elfread_print_subspace_map_entry (objfile, i);
    }

  printf_filtered ("\n  Data section map:\n");
  /* then print map for data space */
  for (i = SUBSPACEMAP (objfile)->datastart;
       i <= SUBSPACEMAP (objfile)->dataend;
       i++)
    {
      elfread_print_subspace_map_entry (objfile, i);
    }

  printf_filtered ("\n  Space map for remaining sections:\n");
  /* then the rest */
  for (i = 0; i < SUBSPACEMAP (objfile)->n_sr; i++)
    {
      if ((i >= SUBSPACEMAP (objfile)->textstart) &&
	  (i <= SUBSPACEMAP (objfile)->textend))
	continue;
      if ((i >= SUBSPACEMAP (objfile)->datastart) &&
	  (i <= SUBSPACEMAP (objfile)->dataend))
	continue;
      elfread_print_subspace_map_entry (objfile, i);
    }
#endif
}


void
elfread_build_doom_tables (struct objfile *objfile)
{
  bfd *abfd;
  asection *notes, *section_strings;
  bfd_size_type notes_size, section_strings_size;
  asection *linkmap_strings, *linkmap, *linkmap_bss, *linkmap_file;
  char *linkmap_strings_buf = 0; /* initialize for compiler warning */
  bfd_size_type linkmap_strings_size;
  char *notes_buf = 0;
  int sectionname;
  char *tmp;

  asection *doom_type_table;
  bfd_size_type doom_type_table_size = 0;
  asection *doom_const_table;
  bfd_size_type doom_const_table_size = 0;
  char *buf8, *buf9, *buf10;

  struct cleanup *old_chain = 0;

  LM_errcode err;

  SYMFILE_HT (objfile) = NULL;
  abfd = symfile_bfd_open (objfile->name);

  TYPETABLEMAP (objfile).table = NULL;
  CONSTTABLEMAP (objfile).table = NULL;

  doom_type_table = bfd_get_section_by_name (abfd, ".objdebug_type");
  if (doom_type_table)
    {
      doom_type_table_size = bfd_section_size (abfd, doom_type_table);
      if (doom_type_table_size > 0)
	{
          /* srikanth, allocate short lived huge objects via mmap */
          buf8 = mmap(0, doom_type_table_size,
                         PROT_READ | PROT_WRITE,
                         MAP_ANONYMOUS | MAP_PRIVATE,
                         -1,
                         0);

          if (buf8 == MAP_FAILED)
            error ("Out of memory");

	  if (!bfd_get_section_contents (abfd,
					 doom_type_table,
					 buf8, 0,
					 doom_type_table_size))
	    error ("bfd_get_section_contents\n");
	  TYPETABLEMAP (objfile).table = buf8;
	  TYPETABLEMAP (objfile).table_size = doom_type_table_size;
	  TYPETABLEMAP (objfile).subspacemap_base = 0;
	}
    }

  doom_const_table = bfd_get_section_by_name (abfd, ".objdebug_const");
  if (doom_const_table)
    {
      doom_const_table_size = bfd_section_size (abfd, doom_const_table);
      if (doom_const_table_size > 0)
	{
          buf9 = mmap(0, doom_const_table_size,
                         PROT_READ | PROT_WRITE,
                         MAP_ANONYMOUS | MAP_PRIVATE,
                         -1,
                         0);

          if (buf9 == MAP_FAILED)
            error ("Out of memory");

	  if (!bfd_get_section_contents (abfd,
					 doom_const_table,
					 buf9, 0,
					 doom_const_table_size))
	    error ("bfd_get_section_contents\n");
	  CONSTTABLEMAP (objfile).table = buf9;
	  CONSTTABLEMAP (objfile).table_size = doom_const_table_size;
	  CONSTTABLEMAP (objfile).subspacemap_base = (int) doom_type_table_size;
	}
    }

  notes = bfd_get_section_by_name (abfd, ".note");
  notes_size = bfd_section_size (abfd, notes);

  notes_buf = xmalloc (notes_size);

  if (!bfd_get_section_contents (abfd,
				 notes,
				 notes_buf,
				 0,
				 notes_size))
    error ("bfd_get_section_contents\n");

  /* Read in link map sections */
  linkmap = bfd_get_section_by_name (abfd, ".linkmap");
  if (linkmap)
    {
      linkmap_size = (int) bfd_section_size (abfd, linkmap);
      linkmap_buf = xmalloc (linkmap_size);
      if (!bfd_get_section_contents (abfd,
				     linkmap,
				     linkmap_buf,
				     0,
				     linkmap_size))
	error ("bfd_get_section_contents\n");
    }
  else
    {
      linkmap_buf = 0;
      linkmap_size = 0;
    }

  linkmap_bss = bfd_get_section_by_name (abfd, ".linkmap_bss");
  if (linkmap_bss)
    {
      linkmap_bss_size = (int) bfd_section_size (abfd, linkmap_bss);
      linkmap_bss_buf = xmalloc (linkmap_bss_size);
      if (!bfd_get_section_contents (abfd,
				     linkmap_bss,
				     linkmap_bss_buf,
				     0,
				     linkmap_bss_size))
	error ("bfd_get_section_contents\n");
    }
  else
    {
      linkmap_bss_buf = 0;
      linkmap_bss_size = 0;
    }

  linkmap_file = bfd_get_section_by_name (abfd, ".linkmap_file");
  if (linkmap_file)
    {
      linkmap_file_size = (int) bfd_section_size (abfd, linkmap_file);
      linkmap_file_buf = xmalloc (linkmap_file_size);
      if (!bfd_get_section_contents (abfd,
				     linkmap_file,
				     linkmap_file_buf,
				     0,
				     linkmap_file_size))
	error ("bfd_get_section_contents\n");
    }
  else
    {
      linkmap_file_buf = 0;
      linkmap_file_size = 0;
    }

  linkmap_strings = bfd_get_section_by_name (abfd, ".linkmap_str");
  if (linkmap_strings)
    {
      linkmap_strings_size = bfd_section_size (abfd, linkmap_strings);
      linkmap_strings_buf = xmalloc (linkmap_strings_size);
      if (!bfd_get_section_contents (abfd,
				     linkmap_strings,
				     linkmap_strings_buf,
				     0,
				     linkmap_strings_size))
	error ("bfd_get_section_contents\n");
    }
  else
    {
      error ("No linkmap strings?");
    }

  linkmap_next_byte = 0;
  linkmap_bss_next_byte = 0;
  linkmap_file_next_byte = 0;

  old_chain = make_cleanup (linkmap_read_finish, NULL);

  if (linkmap_buf)
    {
      err = LM_begin_read_linkmap (linkmap_buffer);
      if (err)
	error ("Error in reading linkmap information\n");
    }
  if (linkmap_bss_buf)
    {
      err = LM_begin_read_linkmap_bss (linkmap_bss_buffer);
      if (err)
	error ("Error in reading linkmap information\n");
    }
  if (linkmap_file_buf)
    {
      err = LM_begin_read_linkmap_file (linkmap_file_buffer);
      if (err)
	error ("Error in reading linkmap information\n");
    }

  /* read section strings into string table */
  section_strings = bfd_get_section_by_name (abfd, ".strtab");
  /* RM: bfd tries to hide the string table from us. Don't let it */
  if (!section_strings)
    _bfd_elf_make_section_from_shdr (abfd,
				   bfd_elf_find_section (abfd, ".shstrtab"),
				     ".shstrtab");
  section_strings = bfd_get_section_by_name (abfd, ".shstrtab");
  section_strings_size = bfd_section_size (abfd, section_strings);
  string_table_size = (int) (section_strings_size + STRING_TABLE_DELTA);
  string_table_limit = (int) section_strings_size;

  STRINGS (objfile) = xmalloc (string_table_size);

  if (!bfd_get_section_contents (abfd,
				 section_strings,
				 STRINGS (objfile),
				 0,
				 section_strings_size))
    error ("bfd_get_section_contents\n");

  elfread_build_partial_subspace_map (objfile, abfd, linkmap_strings_buf,
				      (int) doom_type_table_size,
				      (int) doom_const_table_size);

  if (!is_swizzled)
    elfread_build_compile_map_64 (objfile, notes_buf, notes_size);
  else
    elfread_build_compile_map_32 (objfile, notes_buf, notes_size);

  if (debug_traces >= 10)
    elfread_dump_compile_map (objfile);

  /* Since we don't use the replacing of string names with index into
     the compilation map we don't need to do this. */
#ifndef HP_IA64
  elfread_complete_subspace_map (objfile);
#endif
  
  if (debug_traces >= 10)
    elfread_dump_subspace_map (objfile);

  free (notes_buf);
  do_cleanups (old_chain);
  free (linkmap_buf);
  free (linkmap_bss_buf);
  free (linkmap_file_buf);
  free (linkmap_strings_buf);

  bfd_close (abfd);
}

void
elfread_dump_relocations (struct objfile *objfile, bfd *abfd, asection *section)
{
  printf ("Dumping elf relocations not supported yet\n");
}

bfd_size_type
elfread_relocated_section_size (bfd *abfd, asection *section)
{
  return bfd_section_size (abfd, section);
}

/* CM: Create a hashed symbol table for the minimal symbols. This
   should speed the doing relocations on object files. */

struct Hashed_minsymtab_item_t
{
  struct minimal_symbol *sym;
  int objid;
  struct Hashed_minsymtab_item_t *next;
};

struct Hashed_minsymtab_t
  {
    struct Hashed_minsymtab_item_t **table;
    struct Hashed_minsymtab_item_t *syms;
    size_t num_syms;
    size_t table_size;
  };

/* CM: Hash function */
#define ELFREAD_MINSYMTAB_HASHER(hash_val, ht, name) \
do { \
  const char *name_walker = (name); \
  (hash_val) = 0; \
  for (name_walker = (name); *name_walker != '\0'; name_walker++) { \
    (hash_val) = (hash_val) + ((hash_val) << 3) + (int) (*name_walker); \
  } \
  (hash_val) = (int) ((hash_val) % ((ht)->table_size)); \
} while(0)

/* CM: Create a hash table */
static struct Hashed_minsymtab_t *
elfread_create_minsymtab (struct objfile *objfile)
{
  struct Hashed_minsymtab_t *ht;
  int i;
  int count_msyms;
  int num_ht_entries;

  count_msyms = objfile->minimal_symbol_count;

  /* CM: Size of the hash table ... a prime number. Since we already know
     how many symbols there are, choose a table size to match. I just made
     best guesses at what the mapping should be. If emperical measurements
     or queuing theory show that different numbers are better, by all
     means, replace these. */
  if (count_msyms < 100)
    {
      num_ht_entries = 97;
    }
  else if (count_msyms < 1000)
    {
      num_ht_entries = 241;
    }
  else if (count_msyms < 5000)
    {
      num_ht_entries = 499;
    }
  else if (count_msyms < 20000)
    {
      num_ht_entries = 997;
    }
  else if (count_msyms < 80000)
    {
      num_ht_entries = 1999;
    }
  else if (count_msyms < 160000)
    {
      num_ht_entries = 7993;
    }
  else if (count_msyms < 320000)
    {
      num_ht_entries = 16001;
    }
  else
    {
      num_ht_entries = 32003;
    }

  ht = (struct Hashed_minsymtab_t *)(void *)
    obstack_alloc (&objfile->symbol_obstack,
		   sizeof (struct Hashed_minsymtab_t) + 
		   (num_ht_entries *
		    sizeof (struct Hashed_minsymtab_item_t *)) +
		   (count_msyms * sizeof (struct Hashed_minsymtab_item_t)));
  ht->table = (struct Hashed_minsymtab_item_t **)
    ((unsigned long) ht + sizeof (struct Hashed_minsymtab_t));
  ht->syms = (struct Hashed_minsymtab_item_t *)
    ((unsigned long) ht +
     sizeof (struct Hashed_minsymtab_t) +
       (num_ht_entries * sizeof (struct Hashed_minsymtab_item_t *)));

  for (i = 0; i < num_ht_entries; i++)
    {
      ht->table[i] = NULL;
    }
  ht->num_syms = 0;
  ht->table_size = num_ht_entries;

  return ht;
}

/* CM: Determine if two symbol types match. Return non-zero
   for match, zero if not match. mst_unknown and mst_abs match
   with everything. This matrix is square and reflective across
   the diagonal, so it does not matter in what order the indicies
   are given. */
static int matchsymtab[mst_num_types][mst_num_types] =
{
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
  {1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
  {1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
  {1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
  {1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0},
  {1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0},
  {1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1},
  {1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1}
};

#define ELFREAD_MATCH_SYMTAB(sym1, sym2) \
  (matchsymtab[MSYMBOL_TYPE(sym1)][MSYMBOL_TYPE(sym2)])

/* CM: Determine if a symbol shold replace another in the
   symbol table. The first index is the old symbol. The
   second is the new symbol. This means that the new
   symbol is along the X-axis of the matrix and the old is
   along the Y-axis. So, a fixed row corresponds to a fixed
   old symbol type.

   Return values:
   0: don't replace
   1: always replace
   2: replace if old symbol is an ST_ENTRY
 */
static int replacesymtab[mst_num_types][mst_num_types] =
{
  {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0},
  {0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

#define ELFREAD_REPLACE_SYMTAB(sym1, sym2) \
  (replacesymtab[MSYMBOL_TYPE(sym1)][MSYMBOL_TYPE(sym2)])

/* CM: Add a symbol to the hash table. If a matching symbol is
   already present, determine if it should be replaced. The
   objid is the object the symbol was defined in. */
static void 
elfread_add_minsymtab (struct Hashed_minsymtab_t *ht,
		       struct minimal_symbol *sym,
		       int objid)
{
  int hash_val;
  struct Hashed_minsymtab_item_t *syms;
  int found = 0;
  char *sym_name = SYMBOL_NAME (sym);

  /* CM: Search for symbol. Match name, objid, and type. */
  ELFREAD_MINSYMTAB_HASHER (hash_val, ht, sym_name);
  syms = ht->table[hash_val];
  while ((syms != NULL) && (!found))
    {
      if ((objid == syms->objid) &&
	  ELFREAD_MATCH_SYMTAB (sym, syms->sym) &&
	  (STREQ (sym_name, SYMBOL_NAME (syms->sym))))
	{
	  found = 1;
	}
      else
	{
	  syms = syms->next;
	}
    }
  if (found)
    {
      /* CM: Replace this found symbol or dump the added symbol */
      switch (ELFREAD_REPLACE_SYMTAB (syms->sym, sym))
	{
	case 1:
	  syms->sym = sym;
	  syms->objid = objid;
	  break;
#ifndef BFD64
	case 2:
	  if ((((unsigned int) (MSYMBOL_INFO (syms->sym))) == ST_ENTRY))
	    {
	      syms->sym = sym;
	      syms->objid = objid;
	    }
	  break;
#endif
	}
    }
  else
    {
      /* CM: Add symbol */
      struct Hashed_minsymtab_item_t *new_sym = &ht->syms[ht->num_syms];
      new_sym->sym = sym;
      new_sym->objid = objid;
      new_sym->next = ht->table[hash_val];
      ht->table[hash_val] = new_sym;
      (ht->num_syms)++;
    }
}

/* CM: Convert a symbol type to a boolean if the symbol is global or local */
static int convtype2isglobal[mst_num_types] =
{1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0};

/* CM: Search the symbol table for a symbol name in a specific
   objid. If objid == -1, look for a global symbol in any objids.
   If objid >= 0, look for a local symbol with that objid */
static struct minimal_symbol *
elfread_search_minsymtab (struct Hashed_minsymtab_t *ht,
			  const char *sym_name,
			  int objid,
			  int do_match_objid,
			  int isglobaltype,
			  int do_match_isglobaltype)
{
  int hash_val;
  struct Hashed_minsymtab_item_t *syms;

  /* CM: Search for symbol. Match name and objid. */
  ELFREAD_MINSYMTAB_HASHER (hash_val, ht, sym_name);
  syms = ht->table[hash_val];
  while (syms != NULL)
    {
      if (STREQ (sym_name, SYMBOL_NAME (syms->sym)))
	{
	  int result = 1;
	  if (do_match_objid)
	    {
	      result = result && (objid == syms->objid);
	    }
	  if (do_match_isglobaltype)
	    {
	      result = result &&
		(convtype2isglobal[MSYMBOL_TYPE (syms->sym)] == isglobaltype);
	    }
	  if (result)
	    {
	      return syms->sym;
	    }
	}
      syms = syms->next;
    }
  return NULL;
}

/* CM: Convert a symbol type to a particular space to look. */
static enum spaces convtype2space[mst_num_types] =
{textspace, textspace, dataspace, dataspace, dataspace,
 textspace, textspace, dataspace, dataspace, dataspace, dataspace};

/* just like lookup_minimal_symbol, but must locate statics in the
 * right object file. Needed for DOOM for performing linker fixups
 * relative to static symbols.
 */
struct minimal_symbol *
elfread_lookup_minimal_symbol_within_object (register const char *name,
					     int objid,
					     struct objfile *objf,
					     int is_global_type,
					     int is_comdat_type)
{
  struct Hashed_minsymtab_t *ht;

  ht = SYMFILE_HT (objf);
  if (!ht)
    {
      /* Lazily build hash table of minimal symbols */
      struct minimal_symbol *msymbol;
      int sym_objid;

      ht = elfread_create_minsymtab (objf);
      for (msymbol = objf->msymbols;
	   msymbol != NULL && SYMBOL_NAME (msymbol) != NULL;
	   msymbol++)
	{
	  char *symname = SYMBOL_NAME (msymbol);

	  /* CM: If the symbol name starts with "D$", it is a
	     special compiler generated symbol. Treat it specially.
	     This needs to be done because an ambiguity exists. Symnbols
	     can appear at the end or beginning of a subspace created
	     by a compiler.  If it does, in the executable the symbol
	     will sit at the border between two subspaces. As a result,
	     the debugger cannot tell to which subspace the symbol
	     belongs: the end of the first subspace or the beginning
	     of the second. */
	  if (symname[0] == 'D' && symname[1] == '$')
	    {
	      /* CM: For "D$" symbols, perfer it to end a subspace */
	      sym_objid =
		hpread_find_objid (objf,
				   SYMBOL_VALUE (msymbol) - 1,
				   convtype2space[SYMBOL_TYPE (msymbol)]);
	      if (sym_objid == -1)
		{
		  sym_objid =
		    hpread_find_objid (objf,
				       SYMBOL_VALUE (msymbol),
				     convtype2space[SYMBOL_TYPE (msymbol)]);
		}
	    }
	  else
	    {
	      /* CM: For normal symbols, perfer it to begin a subspace */
	      sym_objid =
		hpread_find_objid (objf,
				   SYMBOL_VALUE (msymbol),
				   convtype2space[SYMBOL_TYPE (msymbol)]);
	      if (sym_objid == -1)
		{
		  sym_objid =
		    hpread_find_objid (objf,
				       SYMBOL_VALUE (msymbol) - 1,
				     convtype2space[SYMBOL_TYPE (msymbol)]);
		}
	    }

	  elfread_add_minsymtab (ht, msymbol, sym_objid);
	}
      SYMFILE_HT (objf) = ht;
    }

  /* CM: Find the symbol. */
  if (is_comdat_type)
    {
      return elfread_search_minsymtab (ht, name, objid, 1, is_global_type, 0);
    }
  else
    {
      return elfread_search_minsymtab (ht, name, objid, (is_global_type ? 0 : 1),
				       is_global_type, 1);
    }

#if 0				/* CM: too slow */
  struct objfile *objfile;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *found_symbol = NULL;
  struct minimal_symbol *found_entry_symbol = NULL;
  struct minimal_symbol *found_file_symbol = NULL;
  struct minimal_symbol *found_adjusted_file_symbol = NULL;
  struct minimal_symbol *found_non_file_symbol = NULL;
  unsigned int found_non_file_symbol_distance = MAXINT;
  int symbol_distance;
  struct minimal_symbol *trampoline_symbol = NULL;
  CORE_ADDR start, end;

  for (objfile = object_files;
       objfile != NULL && found_symbol == NULL;
       objfile = objfile->next)
    {
      if (objf == NULL || objf == objfile)
	{
	  for (msymbol = objfile->msymbols;
	       msymbol != NULL && SYMBOL_NAME (msymbol) != NULL &&
	       found_symbol == NULL;
	       msymbol++)
	    {
	      if (SYMBOL_MATCHES_NAME (msymbol, name))
		{
		  switch (MSYMBOL_TYPE (msymbol))
		    {
		    case mst_file_text:
		      /* CM: This following if makes the assumption that
		         found_file_symbol has a higher priority than
		         found_adjusted_file_symbol. If the priorities are
		         reversed, do:
		         if (!found_adjusted_file_symbol) { ... }
		         instead. */
		      if (!found_file_symbol)
			{
			  if (hpread_find_objid (objfile,
						 SYMBOL_VALUE (msymbol),
						 textspace) == objid)
			    {
			      found_file_symbol = msymbol;
			    }
			  /* if it doesn't match, it may be a symbol
			   * just off the end of this object file's
			   * contribution */
			  else
			    {
			      if (hpread_find_objid (objfile,
						 SYMBOL_VALUE (msymbol) - 1,
						     textspace) == objid)
				{
				  found_adjusted_file_symbol = msymbol;
				}
			    }
			}
		      break;

		    case mst_file_data:
		    case mst_file_bss:
		      if (!found_file_symbol)
			{
			  if (hpread_find_objid (objfile,
						 SYMBOL_VALUE (msymbol),
						 dataspace) ==
			      objid)
			    {
			      found_file_symbol = msymbol;
			    }
			  /* RM: ??? Do we need to look for close data
			   * symbols too?
			   */
			}
		      break;

		    case mst_solib_trampoline:
		      /* If a trampoline symbol is found, we prefer to
		         keep looking for the *real* symbol. If the
		         actual symbol is not found, then we'll use the
		         trampoline entry. */
		      if (trampoline_symbol == NULL)
			trampoline_symbol = msymbol;
		      break;

		    case mst_unknown:
		    default:
		      /* RM: ??? Code symbols are better than entry
		       * symbols?
		       */
#ifndef BFD64
		      if ((((unsigned int) (MSYMBOL_INFO (msymbol))) ==
			   ST_ENTRY))
			found_entry_symbol = msymbol;
		      else
#endif
			found_symbol = msymbol;
		      break;
		    }
		}
	    }
	}
    }

  /* External symbols are best.  */
  if (found_symbol)
    {
      return found_symbol;
    }
  /* Entry symbols are next best.  */
  if (found_entry_symbol)
    {
      return found_entry_symbol;
    }
  /* File-local symbols are next best.  */
  if (found_file_symbol)
    {
      return found_file_symbol;
    }
  if (found_adjusted_file_symbol)
    {
      return found_adjusted_file_symbol;
    }
  /* Symbols for shared library trampolines are next best.  */
  if (trampoline_symbol)
    {
      return trampoline_symbol;
    }
  /* "close" non file symbols are a last resort */
  if (found_non_file_symbol)
    {
      return found_non_file_symbol;
    }
  /* Nothing found */
  return NULL;
#endif /* CM: too slow */
}


/* Given an objid and sym_section_index we want to find the enclosing
   entry in the subspace map. We return the start address of the section.
   For a COMDAT section the combination of the objid and 
   sym_section_index will not appear in the linkmap if that section
   did not make it to the executable */

static uint64_t 
get_start_address (struct objfile * objfile, int sym_section_index, int objid)
{
  int start = 0, middle, end = SECTIONMAP (objfile)->bucket[objid]->n_sr;

  /* If the bucket for that .o is not soreted already sort it on the 
     input section index now. */
  if (!SECTIONMAP (objfile)->bucket[objid]->sorted)
    {
      qsort (&(SECTIONMAP (objfile)->bucket[objid]->sr[0]), end, 
	     sizeof (struct sr),
	     (int (*)(const void *, const void *)) 
	     elfread_compare_section_entries);
      SECTIONMAP (objfile)->bucket[objid]->sorted = 1;
    }
  
  /* Find what input section index it went into and retrieve the 
     start address of that input section */
  while (start <= end)
    {
      middle = (start + end) /2;
      if (SECTIONMAP (objfile)->bucket[objid]->sr[middle].input_section_index
	  == sym_section_index)
	return SECTIONMAP (objfile)->bucket[objid]->sr[middle].start;
      else if (sym_section_index < 
	       SECTIONMAP (objfile)->bucket[objid]->sr[middle].
	       input_section_index)
	end = middle - 1;
      else if (sym_section_index > 
	       SECTIONMAP (objfile)->bucket[objid]->sr[middle].
	       input_section_index)
	start = middle + 1;
    }
  return 0;
}



/* For information on what relocations the compiler uses, see the
 * file: /CLO/Components/SLLIC/Src/output/elf.c@@/main/current/LATEST
 * Search for "case i_pa64_debug_data:". This is for data symbols.
 * Function symbols should always generate a DIR64 relocation.  Looks
 * like the compiler could emit SEGREL64 relocations, but in practice,
 * it doesn't seem to. I'll try to handle them anyway.
 */
#define SET_UNALIGNED_DWORD_TO_UNALIGNED_DWORD(dest, src) \
  ((char *) dest)[0] = ((char *) &(src))[0]; \
  ((char *) dest)[1] = ((char *) &(src))[1]; \
  ((char *) dest)[2] = ((char *) &(src))[2]; \
  ((char *) dest)[3] = ((char *) &(src))[3]; \
  ((char *) dest)[4] = ((char *) &(src))[4]; \
  ((char *) dest)[5] = ((char *) &(src))[5]; \
  ((char *) dest)[6] = ((char *) &(src))[6]; \
  ((char *) dest)[7] = ((char *) &(src))[7]

void
elfread_apply_elf64_rela_relocation (struct objfile *objfile,
				     bfd *abfd,
				     Elf64_Rela *reloc,
				     char *buffer,
				     Elf64_Sym *symbol_table,
				     char *symtab_strings,
				     char *special_symtab_buffer,
				     int objid)
{
  Elf64_Xword reloc_info;
  Elf64_Sword reloc_sym_index;
  Elf64_Word reloc_type;
  char *sym_name;
  uint64_t sym_value = 0;
  uint64_t reloc_value;
  uint64_t tp_value;
  uint64_t seg_start_value;
  void *dest;
  struct minimal_symbol *msym;
  struct minimal_symbol *tbss;
  struct minimal_symbol *tdata;
  int is_global_sym;
  int is_comdat_sym;
  int i;
  CORE_ADDR start_address;

  /* LST  int sym_section_index; */
  long sym_section_index;
  
  reloc_info = reloc->r_info;
  reloc_sym_index = ELF64_R_SYM (reloc_info);
  reloc_type = ELF64_R_TYPE (reloc_info);

  /* sym_value = the value of symbol at symbol table index
   * reloc_sym_index
   */
  /* RM: find symbol value in executable */

  /* Poorva: Added stuff for elf large section tables. If the st_shndx 
     contains the escape value SHN_XINDEX then we need to look in the 
     SHT_SYMTAB_SHNDX  section associated with the symtab section. This 
     section is an array of Elf32_Word which contain the section index value. 
  */
  
  /* SHN_XINDEX should be  0xffff in the header file*/
  if (symbol_table[reloc_sym_index].st_shndx == SHN_XINDEX)
    {
      /* Find section SHT_SYMTAB_SINDEX associated with 
	 this SHT_SYMTAB section */

      sym_section_index
        = ((Elf32_Word *) special_symtab_buffer)[reloc_sym_index];
    }
  else	
    sym_section_index = symbol_table[reloc_sym_index].st_shndx;

#ifndef HP_IA64
  is_global_sym =
    ELF64_ST_BIND (symbol_table[reloc_sym_index].st_info) == STB_GLOBAL;

  /* RM: SHF_COMDAT is deprecated, but still used it seems */
  if (sym_section_index < SHN_LOPROC)
    {
      is_comdat_sym = elf_elfsections (abfd)[sym_section_index]->sh_flags 
		      & SHF_HP_COMDAT
                      ||
		      elf_elfsections (abfd)[sym_section_index]->sh_flags 
		      & SHF_COMDAT;
    }
  else
    is_comdat_sym = FALSE;
#else
  is_comdat_sym = (sym_section_index < SHN_LOPROC) &&
    elf_elfsections (abfd)[sym_section_index]->sh_flags
    & SHF_HP_COMDAT;
  /* RM: SHF_COMDAT is deprecated, but still used it seems */
  is_comdat_sym |= (sym_section_index < SHN_LOPROC) &&
    elf_elfsections (abfd)[sym_section_index]->sh_flags
    & SHF_COMDAT;
#endif
  sym_name = symtab_strings + symbol_table[reloc_sym_index].st_name;
#ifdef HP_IA64
  /* Poorva: For IA64 we get the input section index of the 
     section in the .o that the symbol belongs to. We then use the 
     input section index to find the start address of the section
     in the executable that the symbol made it to. 
     get_start_address does a binary search over the input section
     of that particular .o only given by the objid since the 
     information is bucketised. 
     */  

  if (sym_section_index < SHN_LORESERVE)
    {
      sym_value = get_start_address (objfile, (int) sym_section_index, objid);
      if (sym_value) 
	sym_value += symbol_table[reloc_sym_index].st_value;
      else
	return;  /* procedure has been eliminated */
    }
  else
    {
      /* Poorva - The symbol section indices in the linkmap for all 
	 sections above SHN_LORESERVE are -1. e.g for Common symbols, 
	 absolute symbols and others we can't lookup the linkmap the way 
	 it is today. All the common symbols have the section index 
	 as -1. To be able to represent common symbols differently
	 would require a runtime architecture change. 
	 */
      struct minimal_symbol *msym = NULL;
      msym = lookup_minimal_symbol (sym_name, NULL, objfile);
      if (msym)
	sym_value = SYMBOL_VALUE_ADDRESS (msym);
      if (!sym_value)
        return;
    }
#else
    {
      msym = elfread_lookup_minimal_symbol_within_object
	(sym_name,
	 objid,
	 objfile,
	 is_global_sym,
	 is_comdat_sym);
      if (!msym)
	{
	  if (debug_traces > 1)
	    warning ("Symbol `%s' not found in linker symbol table. "
		     "This symbol may not be locatable later.", sym_name);
	  sym_value = 0;
	}
      else
	{
	  /* Calculate the fixup */
	  /* WTF? */
	  /* if (sym_value) */
	  sym_value = SYMBOL_VALUE_ADDRESS (msym) -
	    ANOFFSET (objfile->section_offsets, SYMBOL_SECTION (msym));
	}
    }
#endif
  
  /* The destination of the relocation. In a REL style relocation,
   * this holds the addend to use. But we're just handling RELAs, so we
   * can ignore any value there.
   */
  dest = (void *) (buffer + reloc->r_offset);

  switch (reloc_type)
    {
#ifndef HP_IA64
    case R_PARISC_LTOFF64:
      /* RM: Carleton's comments follow. If the symbol isn't an UNDEF,
       * we'll fix it up to have its actual final address. If not, we'll
       * fix it up to zero and play games with __d_shl_get later. This
       * means that the code that reads this stuff (reading of SVARs in
       * hp-symtab-read.c) has to be intimately familiar with this
       * weirdness.

       * Okay. Instead of fixing this up to a linkage table slot,
       * let's just relocate this to the actual symbol address.  The
       * reason is that it would be difficult to find the linkage table
       * slot in the executable (linear search of the .dlt
       * section--which can be huge-- looking for the linkage table
       * entry with the value "sym_value + addend").  What happens if
       * the DLT slot is not there for some reason (though it is mighty
       * strange if its not there!)? Doing this can be tricky. Note that
       * by not finding the proper DLT slot, the functions which use
       * this information may need to behave differently for the DOOM
       * and non-DOOM case! The only time this may be a problem is in
       * the shared library case.  In that case, you may be able to play
       * the same trick as in 32-bit DOOM and do a "dlsym()" to get the
       * value of the symbol from the 64-bit dld.sl later.

       * I'm not sure how DDE uses this LTOFF stuff.  It may not work in
       * Wdb's case because we could attempt to print a variable before
       * dld.sl executes and places the correct value in the linkage
       * table slot. Does any one know why DDE does it this way and
       * doesn't just call "dlsym"? The only time LTOFFs can give more
       * information than DIR's is when shared libs are involved. */
      reloc_value = sym_value ? sym_value + reloc->r_addend : 0;
      break;
    case R_PARISC_DIR64:
      reloc_value = sym_value + reloc->r_addend;
      break;
    case R_PARISC_LTOFF_TP64:
      /* Note: See previous note about LTOFF relocations */
      /* Get the TP segment start. The real way to do this is to go into
       * the ELF program header and look for the segment with the type
       * PT_HP_TLS and take its starting virtual address. A way to fake
       * this would be to just look for the section name ".tbss" and
       * report its starting virtual address. Also, we don't need to
       * calculate this each time for each relocation. Once per executable
       * we're relocating is probably enough.
       */
      tp_value = 0;
      tdata = lookup_minimal_symbol (".tdata", 0, objfile);
      if (tdata)
        {
          tp_value = SYMBOL_VALUE_ADDRESS (tdata) - 
            ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile));
        }
      else
        {
          tbss = lookup_minimal_symbol (".tbss", 0, objfile);
          if (tbss)
	    tp_value = SYMBOL_VALUE_ADDRESS (tbss) -
	       ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile));
        }
      reloc_value = sym_value ? sym_value + reloc->r_addend - tp_value : 0;
      break;
    case R_PARISC_TPREL64:
      tbss = lookup_minimal_symbol (".tbss", 0, objfile);
      if (tbss)
	tp_value = SYMBOL_VALUE_ADDRESS (tbss) -
	  ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile));
      else
	tp_value = 0;
      reloc_value = sym_value + reloc->r_addend - tp_value;
      break;
    case R_PARISC_SEGREL64:
      /* This is the starting virtual address of the segment (in the
       * executable) in which this section (from the .o) ended up. The
       * proper way to find this would be to use the section index (from
       * get_relocation_contents()), look in the linker tools mapping to
       * find out in what segment of the executable this section was
       * placed. The quick and dirty way is that if you look in the C
       * compiler elf.c file, the SEGREL64 can only be generated for data
       * relocations. Assuming only a single data segment (I know we
       * cannot assume a single TEXT segment!), look in the ELF program
       * header for the start of the data segment and use that. There may
       * already be a function for this which Hui-May and Michael
       * C. use. Another quick and dirty way is to use the value of this
       * symbol and determine directly from that to which segment in the
       * executable the symbol belongs.
       */
      /* Even though the compiler elf.c file suggests that SEGREL's can be
       * generated, I've never seen them. Perhaps we can eliminate the
       * segrel support? I've only tried C, though; what about using aCC?
       * RM: segrel's are indeed generated, for file and procedure statics
       */
      /* RM: There's another way to do this: go through the program
       * header entries, looking for on that includes the current
       * symbols' address. That's the one we want.
       */
      for (i = 0, seg_start_value = 0;
	   i < elf_elfheader (objfile->obfd)->e_phnum;
	   i++)
	if ((elf_tdata (objfile->obfd)->phdr[i].p_type == PT_LOAD) &&
	    (sym_value >= elf_tdata (objfile->obfd)->phdr[i].p_vaddr) &&
	    (sym_value < elf_tdata (objfile->obfd)->phdr[i].p_vaddr +
	     elf_tdata (objfile->obfd)->phdr[i].p_memsz))
	  {
	    seg_start_value = elf_tdata (objfile->obfd)->phdr[i].p_vaddr;
	    break;
	  }

      reloc_value = sym_value + reloc->r_addend - seg_start_value;
      break;
#ifndef HPUX_1020
      case R_PARISC_LTOFF_DTPMOD64: /* New Reloc for DTLS */
        tbss = lookup_minimal_symbol (".tbss", 0, objfile);
        if (tbss)
          tp_value = SYMBOL_VALUE_ADDRESS (tbss) -
            ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile));
        else
          tp_value = 0;
        reloc_value = sym_value + reloc->r_addend - tp_value;
        break;
#endif
#else
    case R_IA_64_DIR64MSB:
      reloc_value = sym_value + reloc->r_addend;
      break;
    case R_IA_64_DTPREL64MSB: /* TLS */
      reloc_value = sym_value + reloc->r_addend - objfile->tptr_value;
      break;
    case R_IA_64_SECREL32MSB:
    case R_IA_64_SECREL64MSB:
      /*
         These relocations can be safely ignored. They are intended
         to deal with offset changes incurred when .debug_* sections
         are glommed together by the linker when creating the
         executable. There's no linker glomming with +objdebug,
         hence no relocation is needed.
      */
	return;
#endif
    default:
      SET_UNALIGNED_DWORD_TO_UNALIGNED_DWORD (&reloc_value, *((uint64_t *) dest));
      warning ("Unknown elf relocation of type %d at offset 0x%llx ignored.",
	       reloc_type, reloc->r_addend);
    }
  SET_UNALIGNED_DWORD_TO_UNALIGNED_DWORD (dest, reloc_value);
}



#define SET_UNALIGNED_WORD_TO_UNALIGNED_WORD(dest, src) \
  ((char *) dest)[0] = ((char *) &(src))[0]; \
  ((char *) dest)[1] = ((char *) &(src))[1]; \
  ((char *) dest)[2] = ((char *) &(src))[2]; \
  ((char *) dest)[3] = ((char *) &(src))[3]


void
elfread_apply_elf32_rela_relocation (struct objfile *objfile,
				     bfd *abfd,
				     Elf32_Rela *reloc,
				     char *buffer,
				     Elf32_Sym *symbol_table,
				     char *symtab_strings,	
				     char *special_symtab_buffer,
				     int objid)
{
  Elf32_Xword reloc_info;
  Elf32_Sword reloc_sym_index;
  Elf32_Word reloc_type;
  char *sym_name;
  uint32_t sym_value = 0;
  uint32_t reloc_value;
  uint32_t tp_value;
  uint32_t seg_start_value;
  void *dest;
  struct minimal_symbol *msym;
  struct minimal_symbol *tbss;
  int is_global_sym;
  int is_comdat_sym;
  int i;
  int sym_section_index;
  CORE_ADDR start_address;

  reloc_info = reloc->r_info;
  reloc_sym_index = ELF32_R_SYM (reloc_info);
  reloc_type = ELF32_R_TYPE (reloc_info);

  /* sym_value = the value of symbol at symbol table index
   * reloc_sym_index
   */
  /* RM: find symbol value in executable */

  if (symbol_table[reloc_sym_index].st_shndx == SHN_XINDEX)
    {
      /* Find section SHT_SYMTAB_SINDEX associated with 
	 this SHT_SYMTAB section */
      sym_section_index = ((int*) special_symtab_buffer)[reloc_sym_index];
    }
  else	
    sym_section_index = symbol_table[reloc_sym_index].st_shndx;


#ifndef HP_IA64
  is_global_sym =
    ELF32_ST_BIND (symbol_table[reloc_sym_index].st_info) == STB_GLOBAL;

  is_comdat_sym = (sym_section_index != SHN_PARISC_ANSI_COMMON) &&
    (sym_section_index != SHN_PARISC_HUGE_COMMON) &&
    elf_elfsections (abfd)[sym_section_index]->sh_flags
    & SHF_HP_COMDAT;
  /* RM: SHF_COMDAT is deprecated, but still used it seems */
  is_comdat_sym |= (sym_section_index != SHN_PARISC_ANSI_COMMON) &&
    (sym_section_index != SHN_PARISC_HUGE_COMMON) &&
    elf_elfsections (abfd)[sym_section_index]->sh_flags
    & SHF_COMDAT;
#else
  is_comdat_sym = (sym_section_index < SHN_LOPROC) &&
    elf_elfsections (abfd)[sym_section_index]->sh_flags
    & SHF_HP_COMDAT;
  /* RM: SHF_COMDAT is deprecated, but still used it seems */
  is_comdat_sym |= (sym_section_index < SHN_LOPROC) &&
    elf_elfsections (abfd)[sym_section_index]->sh_flags
    & SHF_COMDAT;
#endif
  sym_name = symtab_strings + symbol_table[reloc_sym_index].st_name;
#ifdef HP_IA64
  /* Poorva: For IA64 we get the input section index of the 
     section in the .o that the symbol belongs to. We then use the 
     input section index to find the start address of the section
     in the executable that the symbol made it to. 
     get_start_address does a binary search over the input section
     of that particular .o only given by the objid since the 
     information is bucketised. 
     */  
  if (sym_section_index < SHN_LORESERVE)
    {
      sym_value = (uint32_t) get_start_address (objfile, sym_section_index, objid);
      if (sym_value) 
	sym_value += symbol_table[reloc_sym_index].st_value;
      else
	return;  /* procedure has been eliminated */
    }
  else
    {
      /* Poorva - The symbol section indices in the linkmap for all 
	 sections above SHN_LORESERVE are -1. e.g for Common symbols, 
	 absolute symbols and others we can't lookup the linkmap the way 
	 it is today. All the common symbols have the section index 
	 as -1. To be able to represent common symbols differently
	 would require a runtime architecture change. 
	 */
      struct minimal_symbol *msym = NULL;
      msym = lookup_minimal_symbol (sym_name, NULL, objfile);
      if (msym)
	sym_value = (uint32_t) SYMBOL_VALUE_ADDRESS (msym);
      if (!sym_value)
        return;
    }
#else
    {
      msym = elfread_lookup_minimal_symbol_within_object
	(sym_name,
	 objid,
	 objfile,
	 is_global_sym,
	 is_comdat_sym);
      if (!msym)
      {
	if (debug_traces > 1)
	  warning ("Symbol `%s' not found in linker symbol table. "
		   "This symbol may not be locatable later.", sym_name);
	sym_value = 0;
      }
      else
	{
	  /* Calculate the fixup */
	  /* WTF? */
	  /* if (sym_value) */
	  sym_value = SYMBOL_VALUE_ADDRESS (msym) -
	    ANOFFSET (objfile->section_offsets, SYMBOL_SECTION (msym));
	}
    }
#endif
  
  /* The destination of the relocation. In a REL style relocation,
   * this holds the addend to use. But we're just handling RELAs, so we
   * can ignore any value there.
   */
  dest = (void *) (buffer + reloc->r_offset);

  switch (reloc_type)
    {
      /* relocation expression is (sym + addend) */
    case R_IA_64_DIR32MSB:
      reloc_value = sym_value + reloc->r_addend;
      break;
    case R_IA_64_DTPREL32MSB: /* TLS */
      reloc_value = sym_value + reloc->r_addend - 
	(uint32_t)(objfile->tptr_value);
      break;
    case R_IA_64_SECREL64MSB:
    case R_IA_64_SECREL32MSB:
      /*
	These relocations can be safely ignored. They are intended
	to deal with offset changes incurred when .debug_* sections
	are glommed together by the linker when creating the
	executable. There's no linker glomming with +objdebug,
	hence no relocation is needed.
      */
      return;
    default:
      SET_UNALIGNED_WORD_TO_UNALIGNED_WORD (&reloc_value, *((uint32_t *) dest));
      warning ("Unknown elf relocation of type %d at offset 0x%llx ignored.",
	       reloc_type, reloc->r_addend);
    }
  SET_UNALIGNED_WORD_TO_UNALIGNED_WORD (dest, reloc_value);
}



void
elfread_get_relocated_section_contents (struct objfile *objfile,
					bfd *abfd,
					int objid,
				        asection *section,
				        char *obuf,
				        int count)
{
  Elf64_Internal_Shdr **section_table;
  Elf64_Internal_Shdr *sec_ptr;
  char *shstrtab;
  int section_index;
  int section_table_entries;
  char *sect_contents_buf;
  char *relocs_buf;
  char *reloc_symtab_buf;
  char *special_symtab_buf;
  int reloc_section_index;
  int reloc_section_sym_index;
  int special_sym_sect_index = 0;
  asection *reloc_section;
  asection *reloc_section_sym;
  asection *special_section_sym;
  char *reloc_symtab_strings = NULL;
  unsigned int i;
  
  section_table = elf_elfsections (abfd);
  section_table_entries = (int) elf_elfheader (abfd)->e_shnum;

  /* find the elf section indices for the section and for the section
   * containing its relocations
   */
  /* RM: ??? Linear search for now, should be done better */
  section_index = 0;
  /* RM: allocated on abfd's obstack */
  shstrtab = bfd_elf_get_str_section (abfd,
				      (unsigned int) elf_elfheader (abfd)->e_shstrndx);

  /* Poorva: We have now repaired the section->index field so it points
     to the correct indx in the array. Use it instead. 20 % improvement.
     Tried all the pragmas down below to see if they helped but nothing
     seemed to help the optimizer. But have left them in there for future.
   */
  if (section->index)
    section_index = section->index;
  else
    {
      if (shstrtab != NULL)
	{
	  for (i = 1; i < section_table_entries; i++)
	    {
	      /* expected trip count of loop is 200 iterations -- 
	       * this loop is hot! 
	       */
#ifdef HP_IA64
#pragma estimated_frequency 200.0
#endif
	      sec_ptr = section_table[i];
	      if (sec_ptr->bfd_section == section)
		{
		  /* this branch is seldom taken */
#ifdef HP_IA64
#pragma estimated_frequency 0.02
#endif
		  
		  if (!strcmp (&shstrtab[sec_ptr->sh_name], section->name))
		    {
		      section_index = i;
		  break;
		    }
		}
	    }
	}
    }

  /* Error checking */
  if (section_index > section_table_entries)
    error ("Bad elf debug information");
     
  /* RM: ??? Unfortunately, in the ELF section headers, there is only
   * a pointer from the relocation section back to the contents
   * section. We have a contents section and want the relocation
   * section. Do a linear search for now.  This should be done better!
   */
  /* Poorva: While reading in the sections elfcode.h we make the section_index 
     point to the relocation section */
  reloc_section_index = (int) section_table[section_index]->sh_info;

  /* Error checking */
  if (reloc_section_index > section_table_entries)
    error ("Bad elf debug information");
#if 0
  reloc_section_index = 0;
      for (i = 0; i < section_table_entries; i++)
	{
	  /* Look for RELA style relocation sections that are non-alloc.
	   * Alloc relocation sections are dynamic relocations used by
	   * dld.sl The sh_info field of the relocation section header entry
	   * contains the index of the contents sections these relocations
	   * should be applied to. The sh_link field contains the section
	   * index of the symbol table to use for the relocations.  We can
	   * also try to handle ELF REL style relocations, but the HP
	   * compilers and linker don't output them.
	   */
	  sec_ptr = section_table[i]; 
	  if ((sec_ptr->sh_info == section_index) && 
	      (sec_ptr->sh_type == SHT_RELA) &&
	      ((sec_ptr->sh_flags & SHF_ALLOC) == 0))
	    {
	      reloc_section_index = i;
	      break;
	    }
	}
#endif
  
  if (reloc_section_index != 0)
    {
      reloc_section_sym_index = (int) section_table[reloc_section_index]->sh_link;
      if (reloc_section_sym_index == 0)
	{
	  /* Relocations but no symbol table - error ! */
	  error ("No symbol table for relocations?");
	}
    }
  else
    {
      /* No relocations required for this section */
      reloc_section_sym_index = 0;
    }

  /* Error checking */
  if (reloc_section_sym_index > section_table_entries)
    error ("Bad elf debug information");

  if (reloc_section_sym_index != 0)
    {
      /* Poorva: While reading in the sections we make the sh_info field
	 point to the relocation section */
      special_sym_sect_index = (int) section_table[reloc_section_sym_index]->sh_info;

      /* Error checking */
      if (special_sym_sect_index > section_table_entries)
        error ("Bad elf debug information");

#if 0
      if (special_sym_sect_index == 0)
	for (i = 0; i < section_table_entries; i++)
	  {
	    /* Look for SHT_SYMTAB_SHNDX  sections that have the same value
	       in the alloc field as the symtab section they are associated 
	       with. The sh_info field has the section index of the
	       symtab section this section is associated with. Unfortuantely
	       in the elf section headers there isn't a pointer form the
	       symtab section to this section so we have to do a linear
	       search. SHT_SYMTAB_SHNDX should be 18 in the header file.
	       */
	    sec_ptr = section_table[i]; 
	    if ((sec_ptr->sh_type == SHT_SYMTAB_SHNDX) &&
		(sec_ptr->sh_info == reloc_section_sym_index))
	      {
		/* The sections should either both be allocated or both 
		   be non-allocated. */
		
		assert (!(section_table[reloc_section_sym_index]->sh_flags & 
			  (Elf64_Xword) SHF_ALLOC) 
			== (sec_ptr->sh_flags & (Elf64_Xword) SHF_ALLOC));
		
		special_sym_sect_index = i;
		break;
	      }
	  }
#endif
    }
  
  sect_contents_buf = obuf;
  relocs_buf = (reloc_section_index != 0) ?
    (char *) xmalloc (section_table[reloc_section_index]->sh_size) : NULL;
  reloc_symtab_buf = (reloc_section_sym_index != 0) ?
    (char *) xmalloc (section_table[reloc_section_sym_index]->sh_size) : NULL;
  special_symtab_buf = (special_sym_sect_index != 0) ?
    (char *) xmalloc (section_table[special_sym_sect_index]->sh_size) : NULL;
    
  /* Read in section contents from "section" into "sect_contents_buf" */
  bfd_get_section_contents (abfd, section, sect_contents_buf, 0, count);

  if (relocs_buf)
    {
      char *name;

      name = &shstrtab[section_table[reloc_section_index]->sh_name];
      /* Read in section contents from section index
       * "reloc_section_index" into "relocs_buf"
       */
      /* reloc_section = bfd_get_section_by_name(abfd, name); */
      reloc_section = section_table[reloc_section_index]->bfd_section;
      /* RM: bfd tries to hide some sections from us. Don't let it */
      if (!reloc_section)
	_bfd_elf_make_section_from_shdr (abfd,
					 section_table[reloc_section_index],
					 name);
      /* reloc_section = bfd_get_section_by_name(abfd, name); */
      reloc_section = section_table[reloc_section_index]->bfd_section;
      bfd_get_section_contents (abfd, reloc_section, relocs_buf, 0,
				bfd_section_size (abfd, reloc_section));
    }

  if (reloc_symtab_buf)
    {
      char *name;

      name = &shstrtab[section_table[reloc_section_sym_index]->sh_name];
      /* Read in section contents from section index
       * "reloc_section_sym_index" into "reloc_symtab_buf"
       */
      /* reloc_section_sym = bfd_get_section_by_name(abfd, name); */
      reloc_section_sym = section_table[reloc_section_sym_index]->bfd_section;
      /* RM: bfd tries to hide some sections from us. Don't let it */
      if (!reloc_section_sym)
	_bfd_elf_make_section_from_shdr (abfd,
				     section_table[reloc_section_sym_index],
					 name);
      /* reloc_section_sym = bfd_get_section_by_name(abfd, name); */
      reloc_section_sym = section_table[reloc_section_sym_index]->bfd_section;
      bfd_get_section_contents (abfd, reloc_section_sym, reloc_symtab_buf, 0,
				bfd_section_size (abfd, reloc_section_sym));

      /* RM: allocated in abfd's obstack, will be freed when we close it */
      reloc_symtab_strings =
	bfd_elf_get_str_section (
				  abfd,
			   (unsigned int) section_table[reloc_section_sym_index]->sh_link);
    }
  
  if (special_symtab_buf)
    {
      char *name;

      name = &shstrtab[section_table[special_sym_sect_index]->sh_name];
      /* Read in section contents from section index
       * "special_sym_sect_index" into "special_symtab_buf"
       */

      special_section_sym = section_table[special_sym_sect_index]->bfd_section;
      /* RM: bfd tries to hide some sections from us. Don't let it */
      if (!special_section_sym)
	_bfd_elf_make_section_from_shdr (abfd,
				     section_table[special_sym_sect_index],
					 name);

      special_section_sym = section_table[special_sym_sect_index]->bfd_section;
      bfd_get_section_contents (abfd, special_section_sym, 
				special_symtab_buf, 0, 
				bfd_section_size (abfd, special_section_sym));

    }

  /* RM: ??? The above can be made much better. Can we get the raw
   * symbol table from bfd?  We still need to verify here that the
   * relocation section is using that symbol table. In any case, we
   * should not be reading in the symbol table for each section being
   * relocated.  Can we really only count on there being just a single
   * .symtab symbol table? In practice, probably yes, in general, no.
   */

  if ((relocs_buf != NULL) && (reloc_symtab_buf != NULL))
    {
      if (!is_swizzled) 
	{
	  Elf64_Xword reloc_limit;
	  Elf64_Xword reloc_current_loc;
	  
	  /* Apply relocations */
	  reloc_limit = (unsigned long) relocs_buf +
	    section_table[reloc_section_index]->sh_size;
	  for (reloc_current_loc = (Elf64_Xword) relocs_buf;
	       reloc_current_loc < reloc_limit;
	       reloc_current_loc += sizeof (Elf64_Rela))
	    {
	      elfread_apply_elf64_rela_relocation (objfile,
						   abfd,
						   (Elf64_Rela *)
							 (unsigned long)
							 reloc_current_loc,
						   sect_contents_buf,
						   (Elf64_Sym *)(void *)
							reloc_symtab_buf,
						   reloc_symtab_strings,
						   special_symtab_buf,
						   objid);
	    }
	}
      else
	{
	  unsigned long reloc_limit;
	  unsigned long reloc_current_loc;
	  
	  /* Apply relocations */
	  reloc_limit = (unsigned long) relocs_buf +
	    section_table[reloc_section_index]->sh_size;
	  for (reloc_current_loc = (unsigned long) relocs_buf;
	       reloc_current_loc < reloc_limit;
	       reloc_current_loc += sizeof (Elf32_Rela))
	    {
	      elfread_apply_elf32_rela_relocation (objfile,
						   abfd,
						   (Elf32_Rela *) reloc_current_loc,
						   sect_contents_buf,
						   (Elf32_Sym *)(void *) reloc_symtab_buf,
						   reloc_symtab_strings,
						   special_symtab_buf,
						   objid);
	    }
	}
    }
  
  /* Clean up */
  if (relocs_buf)
    {
      free (relocs_buf);
    }
  if (reloc_symtab_buf)
    {
      free (reloc_symtab_buf);
    }
  if (special_symtab_buf)
    {
      free (special_symtab_buf);
    }
}

/* Register that we are able to handle ELF object file formats.  */

static struct sym_fns elf_sym_fns =
{
  bfd_target_elf_flavour,
  elf_new_init,			/* sym_new_init: init anything gbl to entire symtab */
  elf_symfile_init,		/* sym_init: read initial info, setup for sym_read() */
  elf_symfile_read,		/* sym_read: read a symbol file into symtab */
  elf_symfile_add_psymtabs,
  elf_symfile_finish,		/* sym_finish: finished with file, cleanup */
#ifdef SOLIB_SECTION_OFFSETS
  SOLIB_SECTION_OFFSETS,
#else
  default_symfile_offsets, /* sym_offsets:  Translate ext. to int. relocation */
#endif

  NULL				/* next: pointer to next struct sym_fns */
};

/* jini: Mixed mode support: JAGag21714 */
#ifdef HP_IA64
extern struct sym_fns som_sym_fns;
#endif

/* Routine to initialize some fields of (symfile_objfile->lmdp) for
   LRE.  It is here because we need all of the elf includes.
   It is only called for the main program.
   */

void
elf_init_lmdp (struct objfile * objfile, struct load_module_desc* lmdp)
{
  int i;

  /* IS_LRE_HACK;  With a proper dlmodinfo interface, this routine would
                   not be needed.  Remove it after LRE gives us dlmodinfo.
                   */

  lmdp->data_base = 0;
  lmdp->data_size = 0;
  lmdp->text_base = 0;
  lmdp->text_size = 0;
  lmdp->unwind_base = 0;
  lmdp->unwind_size = 0;
  for (i = 0;
       i < elf_elfheader (objfile->obfd)->e_phnum;
       i++)
    {
      if (elf_tdata (objfile->obfd)->phdr[i].p_type == PT_IA_64_UNWIND)
        {
          lmdp->unwind_base = elf_tdata (objfile->obfd)->phdr[i].p_vaddr;
          lmdp->unwind_size = elf_tdata (objfile->obfd)->phdr[i].p_memsz;
          return;
        }
      else if (elf_tdata (objfile->obfd)->phdr[i].p_type == PT_LOAD
               && ! (elf_tdata (objfile->obfd)->phdr[i].p_flags & PF_W))
        {
          /* Text is loadable but not writable */
          lmdp->text_base = elf_tdata (objfile->obfd)->phdr[i].p_vaddr;
          lmdp->text_size = elf_tdata (objfile->obfd)->phdr[i].p_memsz;
        }
      else if (elf_tdata (objfile->obfd)->phdr[i].p_type == PT_LOAD
               && (elf_tdata (objfile->obfd)->phdr[i].p_flags & PF_W))
        {
          /* Data is loadable and writable */
          lmdp->data_base = elf_tdata (objfile->obfd)->phdr[i].p_vaddr;
          lmdp->data_size = elf_tdata (objfile->obfd)->phdr[i].p_memsz;
        }
    } /* for each phdr i */
  return;
} /* end elf_init_lmdp */

/* Baskar  stripped exec */

#ifdef GDB_TARGET_IS_HPPA_20W
int
init_export_symbols (struct objfile *objfile)
{
   unsigned int export_list_size;

   asection *dyn_symtab;
   long dyn_symcount;
   Elf64_Sym *dyn_symbols = 0;

   asection * strtab;
   long strsize;
   char * strings = 0;
   int index, k;

   /* Initialize objfile variables */
   objfile->export_list = NULL;
   objfile->export_list_size = 0;

   /* Readin .dynsym contents */
   dyn_symtab = bfd_get_section_by_name (objfile->obfd, ".dynsym");
   if (!dyn_symtab)
     goto cleanup;

   dyn_symcount = bfd_section_size (objfile->obfd, dyn_symtab) / sizeof (Elf64_Sym);

   if (!dyn_symcount)
     goto cleanup;

   dyn_symbols = xmalloc (dyn_symcount * sizeof (Elf64_Sym));
   bfd_get_section_contents (objfile->obfd, dyn_symtab, dyn_symbols,
				0, dyn_symcount * sizeof (Elf64_Sym));

   strtab = bfd_get_section_by_name (objfile->obfd, ".dynstr");
   if (!strtab)
     goto cleanup;

   strsize = bfd_section_size (objfile->obfd, strtab);
   if (!strsize)
     goto cleanup;

   strings = xmalloc (strsize);
   bfd_get_section_contents (objfile->obfd, strtab, strings,
                              0, strsize);

   /* allocate mem for export list in objstack... */
   export_list_size = dyn_symcount;

   objfile->export_list = (ExportEntry *)
	obstack_alloc (&objfile->psymbol_obstack, export_list_size * sizeof (ExportEntry));

   for (index=0,k=0; index<dyn_symcount; index++)
     {
	Elf64_Sym *symbol;
	char *name;

	symbol = dyn_symbols + index;
	name = &strings[symbol->st_name];

	/* storing only the fiunctions & objects */

	if (ELF64_ST_TYPE (symbol->st_info) == STT_FUNC ||
	    ELF64_ST_TYPE (symbol->st_info) == STT_OBJECT )
	  {

	    objfile->export_list[k].name = (char *) obstack_alloc (&objfile->psymbol_obstack,
			strlen (name) + 1);
	    strcpy (objfile->export_list[k].name, name);
	    objfile->export_list[k].address = symbol->st_value;
	    k++;
	  }
     }
	
   objfile->export_list_size = k;

cleanup :
   free (dyn_symbols);
   free (strings);

   return objfile->export_list_size;
}
#endif

void
_initialize_elfread ()
{
  add_symtab_fns (&elf_sym_fns);
#ifdef HP_IA64
  /* jini: Mixed mode support: Sep 2006: JAGag21714: Add som_sym_fns to
     the list of sym_fns. */
  add_symtab_fns (&som_sym_fns);
#endif
}

/* For JAGaf16859 - Gdb to list source files
 * even when the executable is moved from the 
 * compilation dirctory 
 */

void *
elf_set_current_compdir (struct partial_symtab *pst)
{
	bfd *abfd;
	asection *notes;
	bfd_size_type notes_size;
	char *notes_buf;
	long size;
	long i=0;
	char *desc;
	char srcfilename[1024];
	char *src_name;
	char *s;
	char *dir_name;
	char *pst_file;
	char *current_file;
	struct note *note;
	void *compilation_dir=NULL;

	abfd = symfile_bfd_open (pst->objfile->name);

	notes = bfd_get_section_by_name (abfd, ".note");
  	notes_size = bfd_section_size (abfd, notes);
        /* current_compilation_directory will be pointing to this buffer,
           hence we cannot make this alloca. */
  	/* notes_buf = (char *)alloca (notes_size);*/
        notes_buf = (char *)xmalloc (notes_size);

  	if (!bfd_get_section_contents (abfd, notes, notes_buf, 0, notes_size))
    		error ("bfd_get_section_contents\n");

   	while (i < notes_size)
   	{
      		note = (struct note *)(void *) (notes_buf + i);

      		if (note->type == NOTE_HP_LINKER && note->namesz == 0 
		    && note->descsz == 40)
	  	{
	    /* old incorrect buggy linker foot print detected !!!.
	     * Skip current note descriptor */
	  		i += 64;
	  		continue;
	  	}
      		size = 3 * sizeof (long long) + note->namesz;
#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
      		size += 1;
#endif
      /* desc must be on 8 byte boundary, so round up */
      		if (size % 8)
	  		size = (size + 8) & (~0x7);
      		desc = ((char *) note) + size;
	  	size += note->descsz;
#if defined(GDB_TARGET_IS_HPUX) && !defined(HP_IA64)
      		size += 1;
#endif
      /* next note must be on a 4 byte boundary, so round up */
      		if (size % 8)
	  		size = (size + 8) & (~0x7);

       		switch ((long) note->type)
		{
	   	  case NOTE_HP_SRC_FILE_INFO:
      			int name_size =  3 * sizeof(long long);
                        bool strcmp_res;
      			src_name = desc;
      /* adjust source and directory names */
			s = src_name;
	  		while (s && *s && (*s != '\n'))
	    			s++;
	  		if (*s)
	    		{
	      			*s = 0;
	      			dir_name = ++s;
	      			while (s && *s && (*s != '\n'))
				s++;
	      			if (*s)
				{
	  				*s = 0;
	  				while (s && *s && (*s != '\n'))
	    					s++;
	  				if (*s)
	    					*s = 0;
				}
	    		}
	  		else
	    		{
	      			dir_name = "";
	    		}

			pst_file = strdup(basename(pst->filename));
			current_file = strdup(basename(src_name));

                        strcmp_res = (strcmp (pst_file, current_file) == 0);
                        free (current_file);
                        free (pst_file);
	    		if (strcmp_res)
			{
      				compilation_dir = dir_name;
               			return compilation_dir;
	    		}

			break;
      		}
	  	i = i + size;
   	}

	return NULL;
}
