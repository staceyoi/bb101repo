/* Evaluate expressions for GDB.
   Copyright 1986, 87, 89, 91, 92, 93, 94, 95, 96, 97, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <ctype.h>		/* for tolower(3C) */

#include "defs.h"
#include "gdb_string.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "value.h"
#include "expression.h"
#include "target.h"
#include "frame.h"
#include "demangle.h"
#include "language.h"		/* For CAST_IS_CONVERSION */
#include "f-lang.h"		/* for array bound stuff */
#include "parser-defs.h"        /* For Fortran intrinsics */
#include "complaints.h"
#include "symfile.h"		/* for print_hp_symbol_modifiability() */
#include "command.h"		/* for error_no_arg() */

/* Objects to store the slice/range info for fortran array slice */
slicevector *slice_vector;
offset_table *offset_tbl;
boolean print_ftn_array_slice;
LONGEST upper_bound[MAX_FORTRAN_DIMS+1];
LONGEST lower_bound[MAX_FORTRAN_DIMS+1];
/* JAGag24558: jini: For PA32, setting values in registers representing
   'long long' data on wide mode resulted in setting the next register.
   8 was being passed as 'intlen' to write_register_bytes. But due to
   REGISTER_RAW_SIZE and related macros being incorrectly defined to
   4 bytes instead of 8 bytes for wide mode, it resulted in incorrect
   comparisons -- inturn resulting in the next register getting set.
   A complete fix for this requires REGISTER_RAW_SIZE and related macros
   to be defined to have 4 bytes or 8 bytes depending on the mode
   (narrow/ wide). The defect ID for this is JAGag26694. FIXME.
   A temporary workaround is being done so as to not have 8 being
   passed as 'intlen' to write_register_bytes. This would avoid
   data being written into the next register. A variable called
   'set_register' is being introduced. Since currently we anyways do not
   have the support to write 'long long' data into registers through
   the print command, this variable is being used to check if we are
   reading from or writing into registers. If we are writing into registers,
   avoid having reg_val created with a size of 8 bytes in 'value_of_register'.
*/
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
boolean set_register = false;
#endif

static void construct_slice_vector (int, LONGEST *, LONGEST *, LONGEST *,
				    slicevector *, int);


/* Defined in symtab.c */
extern int hp_som_som_object_present;
extern int hp_dwarf2_object_present;
extern int namespace_enabled;
extern int ambiguity_detected;
extern struct type *msym_data_symbol_type;

extern char *
class_of PARAMS ((struct type *));

/* This is defined in valops.c */
extern int overload_resolution;

/* JYG: lookup rtti type of STRUCTOP_PTR when this is set to continue
   on with successful lookup for member/method of the rtti type. */
extern int objectprint;

/* JAGaf49121 */
extern char *cplus_demangled_name;

/* Prototypes for local functions. */

static value_ptr evaluate_subexp_for_sizeof (struct expression *, int *);

static value_ptr evaluate_subexp_for_address (struct expression *,
					      int *, enum noside);

static value_ptr
evaluate_subexp_for_ftn_intrinsic (value_ptr, int,
				   struct expression *exp, int *,
				   enum noside);

static value_ptr evaluate_subexp (struct type *, struct expression *,
				  int *, enum noside);

static char *get_label (struct expression *, int *);

static value_ptr
evaluate_struct_tuple (value_ptr, struct expression *, int *,
		       enum noside, int);

static LONGEST
init_array_element (value_ptr, value_ptr, struct expression *,
		    int *, enum noside, LONGEST, LONGEST);
#ifdef HP_IA64
static void check_hp_modifiability PARAMS ((struct expression *, int));
#endif /* HP_IA64 */

static value_ptr evaluate_subexp_for_ftn_intrinsic PARAMS ((value_ptr, int, struct expression *exp, int *, enum noside));

extern int
find_overload_match (struct type **arg_types, int nargs, char *name,
		     enum call_type method, int lax, value_ptr obj,
		     struct symbol *fsym, value_ptr *valp,
		     struct symbol **symp, int *staticp);

struct complaint print_as_int_complaint = 
{"No debug info - printing as integer" ,0, 0};

#if defined (__GNUC__) && !__STDC__
inline
#endif
static value_ptr
evaluate_subexp ( struct type *expect_type,
                  register struct expression *exp,
                  register int *pos,
                  enum noside noside)
{
  return (*exp->language_defn->evaluate_exp) (expect_type, exp, pos, noside);
}

/* Parse the string EXP as a C expression, evaluate it,
   and return the result as a number.  */

CORE_ADDR
parse_and_eval_address ( char *exp)
{
  struct expression *expr = parse_expression (exp);
  register CORE_ADDR addr;
  register struct cleanup *old_chain =
  make_cleanup (free_current_contents, &expr);

  addr = value_as_pointer (evaluate_expression (expr));
  do_cleanups (old_chain);
  return addr;
}

/* Like parse_and_eval_address but takes a pointer to a char * variable
   and advanced that variable across the characters parsed.  */

CORE_ADDR
parse_and_eval_address_1 ( char **expptr)
{
  struct expression *expr = parse_exp_1 (expptr, (struct block *) 0, 0);
  register CORE_ADDR addr;
  register struct cleanup *old_chain =
  make_cleanup (free_current_contents, &expr);

  addr = value_as_pointer (evaluate_expression (expr));
  do_cleanups (old_chain);
  return addr;
}

value_ptr
parse_and_eval ( char *exp)
{
  struct expression *expr = parse_expression (exp);
  register value_ptr val;
  register struct cleanup *old_chain
  = make_cleanup (free_current_contents, &expr);

  val = evaluate_expression (expr);
  do_cleanups (old_chain);
  return val;
}

/* Parse up to a comma (or to a closeparen)
   in the string EXPP as an expression, evaluate it, and return the value.
   EXPP is advanced to point to the comma.  */

value_ptr
parse_to_comma_and_eval ( char **expp)
{
  struct expression *expr = parse_exp_1 (expp, (struct block *) 0, 1);
  register value_ptr val;
  register struct cleanup *old_chain
  = make_cleanup (free_current_contents, &expr);

  val = evaluate_expression (expr);
  do_cleanups (old_chain);
  return val;
}

/* Evaluate an expression in internal prefix form
   such as is constructed by parse.y.

   See expression.h for info on the format of an expression.  */

value_ptr
evaluate_expression ( struct expression *exp)
{
  int pc = 0;
  return evaluate_subexp (NULL_TYPE, exp, &pc, EVAL_NORMAL);
}

/* Evaluate an expression, avoiding all memory references
   and getting a value whose type alone is correct.  */

value_ptr
evaluate_type ( struct expression *exp)
{
  int pc = 0;
  return evaluate_subexp (NULL_TYPE, exp, &pc, EVAL_AVOID_SIDE_EFFECTS);
}

/* If the next expression is an OP_LABELED, skips past it,
   returning the label.  Otherwise, does nothing and returns NULL. */

static char *
get_label ( register struct expression *exp, int *pos)
{
  if (exp->elts[*pos].opcode == OP_LABELED)
    {
      int pc = (*pos)++;
      char *name = &exp->elts[pc + 2].string;
      int tem = longest_to_int (exp->elts[pc + 1].longconst);
      (*pos) += 3 + BYTES_TO_EXP_ELEM (tem + 1);
      return name;
    }
  else
    return NULL;
}

/* This function evaluates tupes (in Chill) or brace-initializers
   (in C/C++) for structure types.  */

static value_ptr
evaluate_struct_tuple ( value_ptr struct_val,
                        register struct expression *exp,
                        register int *pos,
                        enum noside noside,
                        int nargs)
{
  struct type *struct_type = check_typedef (VALUE_TYPE (struct_val));
  struct type *substruct_type = struct_type;
  struct type *field_type;
  int fieldno = -1;
  int variantno = -1;
  int subfieldno = -1;
  while (--nargs >= 0)
    {
      int pc = *pos;
      value_ptr val = NULL;
      int nlabels = 0;
      int bitpos, bitsize;
      char *addr;

      /* Skip past the labels, and count them. */
      while (get_label (exp, pos) != NULL)
	nlabels++;

      do
	{
	  char *label = get_label (exp, &pc);
	  if (label)
	    {
	      for (fieldno = 0; fieldno < TYPE_NFIELDS (struct_type);
		   fieldno++)
		{
		  char *field_name = TYPE_FIELD_NAME (struct_type, fieldno);
		  if (field_name != NULL && STREQ (field_name, label))
		    {
		      variantno = -1;
		      subfieldno = fieldno;
		      substruct_type = struct_type;
		      goto found;
		    }
		}
	      for (fieldno = 0; fieldno < TYPE_NFIELDS (struct_type);
		   fieldno++)
		{
		  char *field_name = TYPE_FIELD_NAME (struct_type, fieldno);
		  field_type = TYPE_FIELD_TYPE (struct_type, fieldno);
		  if ((field_name == 0 || *field_name == '\0')
		      && TYPE_CODE (field_type) == TYPE_CODE_UNION)
		    {
		      variantno = 0;
		      for (; variantno < TYPE_NFIELDS (field_type);
			   variantno++)
			{
			  substruct_type
			    = TYPE_FIELD_TYPE (field_type, variantno);
			  if (TYPE_CODE (substruct_type) == TYPE_CODE_STRUCT
			      || TYPE_CODE (substruct_type) == TYPE_CODE_CLASS)
			    {
			      for (subfieldno = 0;
				 subfieldno < TYPE_NFIELDS (substruct_type);
				   subfieldno++)
				{
				  if (STREQ (TYPE_FIELD_NAME (substruct_type,
							      subfieldno),
					     label))
				    {
				      goto found;
				    }
				}
			    }
			}
		    }
		}
	      error ("there is no field named %s", label);
	    found:
	      ;
	    }
	  else
	    {
	      /* Unlabelled tuple element - go to next field. */
	      if (variantno >= 0)
		{
		  subfieldno++;
		  if (subfieldno >= TYPE_NFIELDS (substruct_type))
		    {
		      variantno = -1;
		      substruct_type = struct_type;
		    }
		}
	      if (variantno < 0)
		{
		  fieldno++;
		  subfieldno = fieldno;
		  if (fieldno >= TYPE_NFIELDS (struct_type))
		    error ("too many initializers");
		  field_type = TYPE_FIELD_TYPE (struct_type, fieldno);
		  if (TYPE_CODE (field_type) == TYPE_CODE_UNION
		      && TYPE_FIELD_NAME (struct_type, fieldno)[0] == '0')
		    error ("don't know which variant you want to set");
		}
	    }

	  /* Here, struct_type is the type of the inner struct,
	     while substruct_type is the type of the inner struct.
	     These are the same for normal structures, but a variant struct
	     contains anonymous union fields that contain substruct fields.
	     The value fieldno is the index of the top-level (normal or
	     anonymous union) field in struct_field, while the value
	     subfieldno is the index of the actual real (named inner) field
	     in substruct_type. */

	  field_type = TYPE_FIELD_TYPE (substruct_type, subfieldno);
	  if (val == 0)
	    val = evaluate_subexp (field_type, exp, pos, noside);

	  /* Now actually set the field in struct_val. */

	  /* Assign val to field fieldno. */
	  if (VALUE_TYPE (val) != field_type)
	    val = value_cast (field_type, val);

	  bitsize = TYPE_FIELD_BITSIZE (substruct_type, subfieldno);
	  bitpos = (int) TYPE_FIELD_BITPOS (struct_type, fieldno);
	  if (variantno >= 0)
	    bitpos += TYPE_FIELD_BITPOS (substruct_type, subfieldno);
	  addr = VALUE_CONTENTS (struct_val) + bitpos / 8;
	  if (bitsize)
	    modify_field (addr, value_as_long (val),
			  bitpos % 8, bitsize);
	  else
	    memcpy (addr, VALUE_CONTENTS (val),
		    TYPE_LENGTH (VALUE_TYPE (val)));
	}
      while (--nlabels > 0);
    }
  return struct_val;
}

/* Recursive helper function for setting elements of array tuples for Chill.
   The target is ARRAY (which has bounds LOW_BOUND to HIGH_BOUND);
   the element value is ELEMENT;
   EXP, POS and NOSIDE are as usual.
   Evaluates index expresions and sets the specified element(s) of
   ARRAY to ELEMENT.
   Returns last index value.  */

static LONGEST
init_array_element ( value_ptr array, 
                     value_ptr element,
                     register struct expression *exp,
                     register int *pos,
                     enum noside noside,
                     LONGEST low_bound, 
                     LONGEST high_bound)
{
  LONGEST index;
  int element_size = TYPE_LENGTH (VALUE_TYPE (element));
  if (exp->elts[*pos].opcode == BINOP_COMMA)
    {
      (*pos)++;
      init_array_element (array, element, exp, pos, noside,
			  low_bound, high_bound);
      return init_array_element (array, element,
				 exp, pos, noside, low_bound, high_bound);
    }
  else if (exp->elts[*pos].opcode == BINOP_RANGE)
    {
      LONGEST low, high;
      (*pos)++;
      low = value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
      high = value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
      if (low < low_bound || high > high_bound)
	error ("tuple range index out of range");
      for (index = low; index <= high; index++)
	{
	  memcpy (VALUE_CONTENTS_RAW (array)
		  + (index - low_bound) * element_size,
		  VALUE_CONTENTS (element), element_size);
	}
    }
  else
    {
      index = value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
      if (index < low_bound || index > high_bound)
	error ("tuple index out of range");
      memcpy (VALUE_CONTENTS_RAW (array) + (index - low_bound) * element_size,
	      VALUE_CONTENTS (element), element_size);
    }
  return index;
}
#ifdef HP_IA64
static void
check_hp_modifiability ( struct expression *exp, int pc)
{
  enum exp_opcode op;
  struct symbol *sym;
  int warning_needed = 1;
  CORE_ADDR current_ip;
  char buf[100];		/* For strcat_address_numeric() */
  extern CORE_ADDR read_pc PARAMS ((void));

  current_ip = read_pc ();
  op = exp->elts[pc].opcode;
  if (op != OP_VAR_VALUE)
    return;
  sym = exp->elts[pc + 2].symbol;

  if (SYMBOL_MODIFIABILITY (sym) == HP_selectively_modifiable)
    {
      if (SYMBOL_MOD_UNMOD_RANGES (sym) == NULL)
	{
	  /* Modifiable nowhere */
	  warning_needed = 1;
	}
      else
	{
	  /* Modifiable only in specified code ranges */
	  int i;
	  int nitems = (SYMBOL_MOD_UNMOD_RANGES (sym))->nitems;

	  for (i = 0; i < nitems; i++)
	    {
	      CORE_ADDR low_addr;
	      CORE_ADDR hi_addr;
	      int ip_in_mod_range;

	      low_addr = (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i].low_addr;
	      hi_addr = (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i].hi_addr;
	      /* CAUTION: Comparing slot-encoded instruction addresses */
	      ip_in_mod_range = (current_ip >= low_addr && current_ip < hi_addr);
	      if (ip_in_mod_range)
		{
		  warning_needed = 0;
		  break;
		}
	    }
	}
    }
  else if (SYMBOL_MODIFIABILITY (sym) == HP_selectively_unmodifiable)
    {
      if (SYMBOL_MOD_UNMOD_RANGES (sym) == NULL)
	{
	  /* Modifiable everywhere */
	  warning_needed = 0;
	}
      else
	{
	  /* Unmodifiable only in specified code ranges */
	  int i;
	  int nitems = (SYMBOL_MOD_UNMOD_RANGES (sym))->nitems;

	  warning_needed = 0;
	  for (i = 0; i < nitems; i++)
	    {
	      CORE_ADDR low_addr;
	      CORE_ADDR hi_addr;
	      int ip_in_mod_range;

	      low_addr = (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i].low_addr;
	      hi_addr = (SYMBOL_MOD_UNMOD_RANGES (sym))->item[i].hi_addr;
	      /* CAUTION: Comparing slot-encoded instruction addresses */
	      ip_in_mod_range = (current_ip >= low_addr && current_ip < hi_addr);
	      if (ip_in_mod_range)
		{
		  warning_needed = 1;
		  break;
		}
	    }
	}
    }

  if (warning_needed)
    {
      buf[0] = 0;
      strcat_address_numeric (current_ip, 1, buf, 100);
      warning ("Due to optimizations variable '%s' can not be safely \n\t modified at %s.",
	       SYMBOL_NAME (sym), buf);
      print_hp_symbol_modifiability (sym, gdb_stderr);
    }
}
#endif /* HP_IA64 */

value_ptr
evaluate_subexp_standard ( struct type *expect_type,
			   register struct expression *exp,
                           register int *pos,
                           enum noside noside)
{
  enum exp_opcode op;
  int tem, tem2, tem3;
  register int pc, pc2 = 0, oldpos;
  register value_ptr arg1 = NULL, arg2 = NULL, arg3;
  struct type *type;
  int nargs;
  value_ptr *argvec;
  LONGEST upper, lower, retcode;
  int code;
  int ix;
  CORE_ADDR val_arg1, mem_offset;

  struct type **arg_types;
  int save_pos1, i;

  /* JAGaf63770 - see explanation for these where they are being used */
  int second_pos;
  int special_field;
  char *current_field;
  struct type *scope_type1, *scope_type2; 

  pc = (*pos)++;
  op = exp->elts[pc].opcode;

  switch (op)
    {
    case OP_SCOPE:
      {
	int look_for_this = 0;

	/* get the length of the variable-length string "field" name from
	   elts[pc + 2] (skipping over the type pointer element at pc=1)
	   and use it to find the corresponding closing OP_SCOPE entry. */
	tem = longest_to_int (exp->elts[pc + 2].longconst);
	(*pos) += (4 + BYTES_TO_EXP_ELEM (tem + 1));

	/* Handle class_name::member for HP aCC. If it is a static data member,
	   the compiler generates a fully qualified symbol name for it. 'yylex'
	   routine has already looked ahead in order to get a symbol for it.
	   However, if it is not a static data member, it is simply a field of
	   its class. To get around the problem, a flag 'look_for_this' is
	   used to tell the callees to check if it is a data member of 'this'.
	*/
#ifdef GDB_TARGET_IS_HPUX 
	if ((hp_som_som_object_present || hp_dwarf2_object_present ) && 
	    current_language->la_language == language_cplus)
          {
	    if (pc && exp->elts[pc - 1].opcode == UNOP_ADDR)
              {
		/* If we see 'object.class_name::member', it is not a member of
		   'this'. */
                look_for_this = 0;                                 
	      }
	    else
              {
		char * class_name = exp->elts[pc + 1].type->tag_name;
		if (selected_frame)
                  {
		    /* If pc is in a member function of its class or the class
		       is dervied from virtual class, it can be member of
		       'this'. */ 
		    struct symbol * func_sym =
		      find_pc_function (selected_frame->pc);

		    if (func_sym)
                      {
			extern char *class_of (struct type *);

			char * func_class_name =
			  class_of (SYMBOL_TYPE (func_sym));

			/* it could be a data member in which case we
			   will just save some time digging up info for
			   class functions. */
			if (func_class_name != NULL)
			  {
			    if (!strcmp(class_name, func_class_name))
			      look_for_this = 1;
			    else
			      {
				struct symbol * func_class_sym = 
				  lookup_symbol (func_class_name, 0,
						 VAR_NAMESPACE, 0, 0);

				if (func_class_sym && 
				    has_vtable (SYMBOL_TYPE (func_class_sym)))
				  look_for_this = 1;
			      }
			  }
		      }
		  }
	      }
	  }
#endif

	arg1 = value_struct_elt_for_reference (exp->elts[pc + 1].type,
					       0,
					       exp->elts[pc + 1].type,
					       &exp->elts[pc + 3].string,
					       expect_type,
					       look_for_this);
	if (arg1 == NULL)
	  error ("There is no field named %s", &exp->elts[pc + 3].string);
	return arg1;
      }

    case OP_LONG:	/* Fix for JAGaf04904. */
      (*pos) += 3;

      /* Fix for JAGaf42530. */
      if ((noside == EVAL_SKIP) && (!exp->elts[pc + 1].type))
        goto nosideret;

      if (!exp->elts[pc + 1].type)
	error ("Unable to evaluate expression: No type");

      return value_from_longest (exp->elts[pc + 1].type,
				 exp->elts[pc + 2].longconst);

    case OP_DOUBLE:
      (*pos) += 3;
      return value_from_double (exp->elts[pc + 1].type,
				exp->elts[pc + 2].doubleconst);

    /* Information stored in the expression is passed to obtain value.
       Pointer is incremented by 3 to readin successive values. */
#ifdef HP_IA64
    case OP_DECFLOAT:
      (*pos) += 3;
      return value_from_decfloat (exp->elts[pc + 1].type,
				  exp->elts[pc + 2].decfloatconst);
#endif

    case OP_VAR_VALUE:

      /* Poorva: Merged this in from 10.20 branch since calling a 
	 member function through a pointer to the object was not working */

       /* JYG: We used to just return value_zero of the symbol type
	 if we're asked to avoid side effects.  Otherwise we return
	 value_of_variable (...).  However I'm not sure if
	 value_of_variable () has any side effect.
	 We need a full value object returned here for whatis_exp ()
	 to call evaluate_type () and then pass the full value to
	 value_rtti_target_type () if we are dealing with a pointer
	 or reference to a base class and print object is on. */
     
      /* JYG: JAGac20375
         What side effort would there be to call value_of_variable?
         The code for ptype / whatis command calls evaluate_type
         which asks for no side effects.  Yet if the expression yields
         a pointer / reference type to a class and objectprint is on,
         we need a more than just value_zero of the base type in order
         to call value_rtti_target_type.
	 
         For now, we will simply ignore the difference between
         EVAL_AVOID_SIDE_EFFECTS and EVAL_NORMAL */
      
      (*pos) += 3;
      /* evaluate_type calls with EVAL_AVOID_SIDE_EFFECTS. If this is a
	 function type, do ahead and look for overload instances. */
      if (noside == EVAL_AVOID_SIDE_EFFECTS 
	  && TYPE_CODE (SYMBOL_TYPE (exp->elts[pc + 2].symbol)) == TYPE_CODE_FUNC)
	{
	  int nelts;
	  char* copy_name;
	  struct symbol **sym_arr;
          struct symtab_and_block *symtab_and_block_arr;
	  nelts = 0;                   /*  counter for the symbol array */

	  /* Allocate sym_arr and symtab_and_block_arr. We don't use
	     symtab_and_block_arr here, but oh well, find_functions wants it. */
          sym_arr = (struct symbol **)
             alloca (max_synonyms_seen * sizeof (struct symbol *));
          symtab_and_block_arr = (struct symtab_and_block *)
             alloca (max_synonyms_seen * sizeof (struct symtab_and_block));

	  /* Discard argument list from symbol name. */
	  copy_name = (char*) alloca (strlen (SYMBOL_SOURCE_NAME (exp->elts[pc + 2].symbol)) +1);
	  strcpy (copy_name, SYMBOL_SOURCE_NAME (exp->elts[pc + 2].symbol));
	  strtok (copy_name, "(");

	  /* We set the look_for_all parameter as 1 here since we
             know that we need to look for all possible instantiations.
           */
          nelts = find_functions (copy_name,
				 sym_arr, symtab_and_block_arr, 1);

	  /* if 0 or 1, use whatever we had. */
	  if (nelts <= 1)
            arg1 = value_of_variable (exp->elts[pc + 2].symbol,
                                exp->elts[pc + 1].block);
	  else
	    {
	      char* inarg, *inarg1;
              char *prompt;
	      struct symtab_and_line sal;
              int choice = 0;

              /* Choices are [0],[1],..,[nelts+1] */
              printf_filtered ("[0] cancel\n");
              while (choice < nelts)
                {
		  sal = find_function_start_sal (sym_arr[choice], 1);
                  printf_filtered ("[%d] %s at %s:%d\n",
                             (choice + 1),
			     SYMBOL_SOURCE_NAME (sym_arr[choice]),
			     sal.symtab->filename,
			     sal.line);
                  choice++;
                }
              if ((prompt = getenv ("PS2")) == NULL)
                {
                  prompt = "> ";
                }
              inarg = command_line_input (prompt, 0, "overload-choice");
              if (inarg == 0 || *inarg == 0)
                error_no_arg ("Select a choice number");

              inarg1 = inarg;
              while (*inarg1 >= '0' && *inarg1 <= '9')
                inarg1++;
              if (*inarg1 && *inarg1 != ' ' && *inarg1 != '\t')
                error ("Argument must be choice number.");

              choice = atoi (inarg);
              if (choice == 0)
                error ("cancelled");
              if (choice > nelts)
                error ("Argument must be given choice numbers");
	      arg1 = value_of_variable (sym_arr[choice-1], 0);
	    }
	}
      else
      arg1 = value_of_variable (exp->elts[pc + 2].symbol,
                                exp->elts[pc + 1].block);
      /* We need to know the address of the variable of an array
         descriptor type because the bounds are determined at runtime,
         so treat it like a variable */
      if (TYPE_CODE (VALUE_TYPE (arg1)) == TYPE_CODE_ARRAY_DESC)
        f_fixup_array_desc_type_array_addr (VALUE_TYPE (arg1),
                                            VALUE_ADDRESS (arg1));
      /* If ambiguity_detected (from symtab.c) is true then
	 the name of the symbol is different form the name
	 of the symbol that we are printing out - warn the user. 
      */
      
      if (namespace_enabled && (current_language->la_language == language_cplus) && ambiguity_detected)
	{
	  struct symbol *sym = exp->elts[pc + 2].symbol;
	  /* If using_decl_name is present use that since that has 
	     the actual name of the symbol. This symbol is just a clone
	     of the actual one. 
	  */
	  if ((SYMBOL_LANGUAGE(sym) == language_cplus) 
	      && SYMBOL_USING_DECL_NAME(sym))
	    printf_filtered ("variable is: %s\n", SYMBOL_USING_DECL_NAME(sym));
	  else
	    {
	      if (is_cplus_name (sym->ginfo.name, 1))  /* JAGaf49121 */
		printf_filtered ("variable is: %s\n", 
				 cplus_demangled_name);
	      else
		printf_filtered ("variable is: %s\n", sym->ginfo.name);
	    }
	}
      return arg1;
      
    case OP_LAST:
      (*pos) += 2;
      return
	access_value_history (longest_to_int (exp->elts[pc + 1].longconst));

    case OP_REGISTER:
      {
	int regno = longest_to_int (exp->elts[pc + 1].longconst);
	value_ptr val = value_of_register (regno);

	(*pos) += 2;
	if (val == NULL)
	  error ("Value of register %s not available.", REGISTER_NAME (regno));
	else
	  return val;
      }
    case OP_BOOL:
      (*pos) += 2;
      return value_from_longest (LA_BOOL_TYPE,
				 exp->elts[pc + 1].longconst);

    case OP_INTERNALVAR:
      (*pos) += 2;
      return value_of_internalvar (exp->elts[pc + 1].internalvar);

    case OP_STRING:
      tem = longest_to_int (exp->elts[pc + 1].longconst);
      (*pos) += 3 + BYTES_TO_EXP_ELEM (tem + 1);
      if (noside == EVAL_SKIP)
	goto nosideret;
      return value_string (&exp->elts[pc + 2].string, tem);

    case OP_BITSTRING:
      tem = longest_to_int (exp->elts[pc + 1].longconst);
      (*pos)
	+= 3 + BYTES_TO_EXP_ELEM ((tem + HOST_CHAR_BIT - 1) / HOST_CHAR_BIT);
      if (noside == EVAL_SKIP)
	goto nosideret;
      return value_bitstring (&exp->elts[pc + 2].string, tem);

    case OP_ARRAY:
      (*pos) += 3;
      tem2 = longest_to_int (exp->elts[pc + 1].longconst);
      tem3 = longest_to_int (exp->elts[pc + 2].longconst);
      nargs = tem3 - tem2 + 1;
      type = expect_type ? check_typedef (expect_type) : NULL_TYPE;

      if (expect_type != NULL_TYPE && noside != EVAL_SKIP
	  && (TYPE_CODE (type) == TYPE_CODE_STRUCT
	      || TYPE_CODE (type) == TYPE_CODE_CLASS))
	{
	  value_ptr rec = allocate_value (expect_type);
	  memset (VALUE_CONTENTS_RAW (rec), '\0', TYPE_LENGTH (type));
	  return evaluate_struct_tuple (rec, exp, pos, noside, nargs);
	}

      if (expect_type != NULL_TYPE && noside != EVAL_SKIP
	  && TYPE_CODE (type) == TYPE_CODE_ARRAY)
	{
	  struct type *range_type = TYPE_FIELD_TYPE (type, 0);
	  struct type *element_type = TYPE_TARGET_TYPE (type);
	  value_ptr array = allocate_value (expect_type);
	  int element_size = TYPE_LENGTH (check_typedef (element_type));
	  LONGEST low_bound, high_bound, index;
	  if (get_discrete_bounds (range_type, &low_bound, &high_bound) < 0)
	    {
	      low_bound = 0;
	      high_bound = (TYPE_LENGTH (type) / element_size) - 1;
	    }
	  index = low_bound;
	  memset (VALUE_CONTENTS_RAW (array), 0, TYPE_LENGTH (expect_type));
	  for (tem = nargs; --nargs >= 0;)
	    {
	      value_ptr element;
	      int index_pc = 0;
	      if (exp->elts[*pos].opcode == BINOP_RANGE)
		{
		  index_pc = ++(*pos);
		  evaluate_subexp (NULL_TYPE, exp, pos, EVAL_SKIP);
		}
	      element = evaluate_subexp (element_type, exp, pos, noside);
	      if (VALUE_TYPE (element) != element_type)
		element = value_cast (element_type, element);
	      if (index_pc)
		{
		  int continue_pc = *pos;
		  *pos = index_pc;
		  index = init_array_element (array, element, exp, pos, noside,
					      low_bound, high_bound);
		  *pos = continue_pc;
		}
	      else
		{
		  if (index > high_bound)
		    /* to avoid memory corruption */
		    error ("Too many array elements");
		  memcpy (VALUE_CONTENTS_RAW (array)
			  + (index - low_bound) * element_size,
			  VALUE_CONTENTS (element),
			  element_size);
		}
	      index++;
	    }
	  return array;
	}

      if (expect_type != NULL_TYPE && noside != EVAL_SKIP
	  && TYPE_CODE (type) == TYPE_CODE_SET)
	{
	  value_ptr set = allocate_value (expect_type);
	  char *valaddr = VALUE_CONTENTS_RAW (set);
	  struct type *element_type = TYPE_INDEX_TYPE (type);
	  struct type *check_type = element_type;
	  LONGEST low_bound, high_bound;

	  /* get targettype of elementtype */
	  while (TYPE_CODE (check_type) == TYPE_CODE_RANGE ||
		 TYPE_CODE (check_type) == TYPE_CODE_TYPEDEF)
	    check_type = TYPE_TARGET_TYPE (check_type);

	  if (get_discrete_bounds (element_type, &low_bound, &high_bound) < 0)
	    error ("(power)set type with unknown size");
	  memset (valaddr, '\0', TYPE_LENGTH (type));
	  for (tem = 0; tem < nargs; tem++)
	    {
	      LONGEST range_low, range_high;
	      struct type *range_low_type, *range_high_type;
	      value_ptr elem_val;
	      if (exp->elts[*pos].opcode == BINOP_RANGE)
		{
		  (*pos)++;
		  elem_val = evaluate_subexp (element_type, exp, pos, noside);
		  range_low_type = VALUE_TYPE (elem_val);
		  range_low = value_as_long (elem_val);
		  elem_val = evaluate_subexp (element_type, exp, pos, noside);
		  range_high_type = VALUE_TYPE (elem_val);
		  range_high = value_as_long (elem_val);
		}
	      else
		{
		  elem_val = evaluate_subexp (element_type, exp, pos, noside);
		  range_low_type = range_high_type = VALUE_TYPE (elem_val);
		  range_low = range_high = value_as_long (elem_val);
		}
	      /* check types of elements to avoid mixture of elements from
	         different types. Also check if type of element is "compatible"
	         with element type of powerset */
	      if (TYPE_CODE (range_low_type) == TYPE_CODE_RANGE)
		range_low_type = TYPE_TARGET_TYPE (range_low_type);
	      if (TYPE_CODE (range_high_type) == TYPE_CODE_RANGE)
		range_high_type = TYPE_TARGET_TYPE (range_high_type);
	      if ((TYPE_CODE (range_low_type) != TYPE_CODE (range_high_type)) ||
		  (TYPE_CODE (range_low_type) == TYPE_CODE_ENUM &&
		   (range_low_type != range_high_type)))
		/* different element modes */
		error ("POWERSET tuple elements of different mode");
	      if ((TYPE_CODE (check_type) != TYPE_CODE (range_low_type)) ||
		  (TYPE_CODE (check_type) == TYPE_CODE_ENUM &&
		   range_low_type != check_type))
		error ("incompatible POWERSET tuple elements");
	      if (range_low > range_high)
		{
		  warning ("empty POWERSET tuple range");
		  continue;
		}
	      if (range_low < low_bound || range_high > high_bound)
		error ("POWERSET tuple element out of range");
	      range_low -= low_bound;
	      range_high -= low_bound;
	      for (; range_low <= range_high; range_low++)
		{
		  int bit_index = (unsigned) range_low % TARGET_CHAR_BIT;
		  if (BITS_BIG_ENDIAN)
		    bit_index = TARGET_CHAR_BIT - 1 - bit_index;
		  valaddr[(unsigned) range_low / TARGET_CHAR_BIT]
		    |= 1 << bit_index;
		}
	    }
	  return set;
	}

      argvec = (value_ptr *) alloca (sizeof (value_ptr) * nargs);
      for (tem = 0; tem < nargs; tem++)
	{
	  /* Ensure that array expressions are coerced into pointer objects. */
	  argvec[tem] = evaluate_subexp_with_coercion (exp, pos, noside);
	}
      if (noside == EVAL_SKIP)
	goto nosideret;
#ifdef HP_IA64
      /* QXCR1000838914 -- unfriendly behavior from "print" during core
         file debugging session.
         If this is a corefile debugging session, we cannot use value_array
         since this routine tries to allocate memory in the inferior with a
         command line call, but there is no inferior running. Introducing
         a new routine called 'value_from_array' here which uses memory from
         gdb itself rather than from the inferior.
       */
      if (!target_has_execution)
        {
          return value_from_array (tem2, tem3, argvec);
        }
#endif
      return value_array (tem2, tem3, argvec);

    case TERNOP_SLICE:
      {
	value_ptr array = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	int lowbound
	= (int) value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
	int upper
	= (int) value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
	if (noside == EVAL_SKIP)
	  goto nosideret;
	return value_slice (array, lowbound, upper - lowbound + 1);
      }

    case TERNOP_SLICE_COUNT:
      {
	value_ptr array = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	int lowbound
	= (int) value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
	int length
	= (int) value_as_long (evaluate_subexp (NULL_TYPE, exp, pos, noside));
	return value_slice (array, lowbound, length);
      }

    case TERNOP_COND:
      /* Skip third and second args to evaluate the first one.  */
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (value_logical_not (arg1))
	{
	  evaluate_subexp (NULL_TYPE, exp, pos, EVAL_SKIP);
	  return evaluate_subexp (NULL_TYPE, exp, pos, noside);
	}
      else
	{
	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	  evaluate_subexp (NULL_TYPE, exp, pos, EVAL_SKIP);
	  return arg2;
	}

    case OP_FUNCALL:
      (*pos) += 2;
      op = exp->elts[*pos].opcode;
      nargs = longest_to_int (exp->elts[pc + 1].longconst);
      /* Allocate arg vector, including space for the function to be
         called in argvec[0] and a terminating NULL */
      argvec = (value_ptr *) alloca (sizeof (value_ptr) * (nargs + 4));
      if (op == STRUCTOP_MEMBER || op == STRUCTOP_MPTR)
	{
	  LONGEST fnptr;

	  /* 1997-08-01 Currently we do not support function invocation
	     via pointers-to-methods with HP aCC. Pointer does not point
	     to the function, but possibly to some thunk. */
	  if (hp_som_som_object_present || hp_dwarf2_object_present)
	    {
	      error ("Not implemented: function invocation through pointer to method with HP aCC");
	    }

	  nargs++;
	  /* First, evaluate the structure into arg2 */
	  pc2 = (*pos)++;

	  if (op == STRUCTOP_MEMBER)
	    {
	      arg2 = evaluate_subexp_for_address (exp, pos, noside);
	    }
	  else
	    {
	      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	    }

	  /* If the function is a virtual function, then the
	     aggregate value (providing the structure) plays
	     its part by providing the vtable.  Otherwise,
	     it is just along for the ride: call the function
	     directly.  */

	  arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);

	  fnptr = value_as_long (arg1);

	  if (METHOD_PTR_IS_VIRTUAL (fnptr))
	    {
	      int fnoffset = (int) METHOD_PTR_TO_VOFFSET (fnptr);
	      struct type *basetype;
	      struct type *domain_type =
	      TYPE_DOMAIN_TYPE (TYPE_TARGET_TYPE (VALUE_TYPE (arg1)));
	      int i, j;
	      basetype = TYPE_TARGET_TYPE (VALUE_TYPE (arg2));
	      if (domain_type != basetype)
		arg2 = value_cast (lookup_pointer_type (domain_type), arg2);
	      basetype = TYPE_VPTR_BASETYPE (domain_type);
	      for (i = TYPE_NFN_FIELDS (basetype) - 1; i >= 0; i--)
		{
		  struct fn_field *f = TYPE_FN_FIELDLIST1 (basetype, i);
		  /* If one is virtual, then all are virtual.  */
		  if (TYPE_FN_FIELD_VIRTUAL_P (f, 0))
		    for (j = TYPE_FN_FIELDLIST_LENGTH (basetype, i) - 1; j >= 0; --j)
		      if ((int) TYPE_FN_FIELD_VOFFSET (f, j) == fnoffset)
			{
			  value_ptr temp = value_ind (arg2);
			  arg1 = value_virtual_fn_field (&temp, f, j, domain_type, 0);
			  arg2 = value_addr (temp);
			  goto got_it;
			}
		}
	      if (i < 0)
		error ("virtual function at index %d not found", fnoffset);
	    }
	  else
	    {
	      VALUE_TYPE (arg1) = lookup_pointer_type (TYPE_TARGET_TYPE (VALUE_TYPE (arg1)));
	    }
	got_it:

	  /* Now, say which argument to start evaluating from */
	  tem = 2;
	}
      else if (op == STRUCTOP_STRUCT || op == STRUCTOP_PTR)
	{
	  /* Hair for method invocations */
	  int tem2;

	  nargs++;
	  /* First, evaluate the structure into arg2 */
	  pc2 = (*pos)++;
	  tem2 = longest_to_int (exp->elts[pc2 + 1].longconst);
	  *pos += 3 + BYTES_TO_EXP_ELEM (tem2 + 1);

	  if (op == STRUCTOP_STRUCT)
	    {
	      /* If v is a variable in a register, and the user types
	         v.method (), this will produce an error, because v has
	         no address.

	         A possible way around this would be to allocate a
	         copy of the variable on the stack, copy in the
	         contents, call the function, and copy out the
	         contents.  I.e. convert this from call by reference
	         to call by copy-return (or whatever it's called).
	         However, this does not work because it is not the
	         same: the method being called could stash a copy of
	         the address, and then future uses through that address
	         (after the method returns) would be expected to
	         use the variable itself, not some copy of it.  */
	      arg2 = evaluate_subexp_for_address (exp, pos, noside);
	    }
	  else
	    {
	      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);

              type=VALUE_TYPE (arg2);
	      /* CR1000800549   command line calls through pointer
                 to reference do not work. */

	      if (TYPE_TARGET_TYPE(type))
                {
		  struct type* target_type = check_typedef 
					      (TYPE_TARGET_TYPE (type));
		  /* Check whether it is pointer to reference. */
	          if (type && (TYPE_CODE(type) == TYPE_CODE_REF) &&
                      (TYPE_CODE(target_type) == TYPE_CODE_PTR))
                    {
		      /* Store the value from the reference pointer.*/
		      arg2 = store_reference_pointer (arg2);
	              type = VALUE_TYPE (arg2);
                    }
                }

	      /* Error out if it's still reference type */
              if (type && (TYPE_CODE(type) == TYPE_CODE_REF))
                error ("Expression must have pointer type");
	    }
#ifdef HP_IA64
            /* QXCR1001037183: WDB hangs when trying to call a member function using this
             * pointer. Return from here w/o evaluating any further if the argument is
             * optimized-out.
             */
             if (arg2 && (arg2->availability == VA_OPTIMIZED_OUT))
               return (arg2);
#endif   /* HP_IA64 */
	  /* Now, say which argument to start evaluating from */
	  tem = 2;
	}
      /* RM: handle calls of the form "A::foo()" */
      else if (op == OP_SCOPE)
       {
	/* Get the length of the variable-length string "field" name from
	   elts[pc + 2] (skipping over the type pointer element at pc=1) and
	   use it to find the element AFTER the corresponding closing
	   OP_SCOPE entry.  Magic number 5 includes the fixed parts of the
	   OP_SCOPE sandwich :  OP_SCOPE/typeptr/field-name-len/
	   <variable-length field-name>/field-name-len/OP_SCOPE.  Yes, the
	   length field is in there twice.  Carl Burch, 12/21/05 fix for
	   JAGaf86027, "gdb crash evaluating expression while debugging
	   native compiler". */
	 tem = longest_to_int (exp->elts[*pos + 2].longconst);
	 *pos += (5 + BYTES_TO_EXP_ELEM (tem + 1));
	 tem = 1;
       }
      else
	{
	  /* Non-method function call */
	  save_pos1 = *pos;
	  argvec[0] = evaluate_subexp_with_coercion (exp, pos, noside);
	  tem = 1;
	  type = VALUE_TYPE (argvec[0]);
	  if (type && TYPE_CODE (type) == TYPE_CODE_PTR)
	    type = TYPE_TARGET_TYPE (type);
	  if (type && TYPE_CODE (type) == TYPE_CODE_FUNC)
	    {
	      for (; tem <= nargs && tem <= TYPE_NFIELDS (type); tem++)
		{
		  /* Pass in NULL_TYPE so that the coersion of the
		     user given arguments will not happen. */
		  argvec[tem] = evaluate_subexp (NULL_TYPE,
						 exp, pos, noside);
		}
	    }
	}

      /* Evaluate arguments */
      for (; tem <= nargs; tem++)
	{
	  /* Ensure that array expressions are coerced into pointer objects. */
	  argvec[tem] = evaluate_subexp_with_coercion (exp, pos, noside);
	}

      /* signal end of arglist */
      argvec[tem] = 0;

      if (op == STRUCTOP_STRUCT || op == STRUCTOP_PTR)
	{
	  int static_memfuncp;
	  value_ptr temp = arg2;
	  char tstr[256];

	  /* Method invocation : stuff "this" as first parameter */
	  /* pai: this used to have lookup_pointer_type for some reason,
	   * but temp is already a pointer to the object */
	
          /*JAGaf63290, Usha: Method call with typedef'd pointer fails*/
          struct type *target_type = check_typedef (VALUE_TYPE (temp));
	  if (op == STRUCTOP_PTR
              && TYPE_CODE (target_type) != TYPE_CODE_PTR)
            error ("expression must have pointer type.");

	  argvec[1]
	    = value_from_pointer (VALUE_TYPE (temp),
				  VALUE_ADDRESS (temp) + VALUE_OFFSET (temp));
	  /* Name of method from expression */
	  strcpy (tstr, &exp->elts[pc2 + 2].string);

	  if (overload_resolution && (exp->language_defn->la_language == language_cplus))
	    {
	      /* Language is C++, do some overload resolution before evaluation */
	      value_ptr valp = NULL;

	      /* Prepare list of argument types for overload resolution */
	      arg_types = (struct type **) xmalloc (nargs * (sizeof (struct type *)));
	      for (ix = 1; ix <= nargs; ix++)
		arg_types[ix - 1] = VALUE_TYPE (argvec[ix]);

	      if (noside == EVAL_SKIP)
		goto nosideret;

              /* JAGae28186 291105 Sunil & Kavya, If the virtual function is called through a 
	       pointer or reference, it has to consult vtable to execute the function 
	       else it as to be handled as a normal function. */

	      if ( op == STRUCTOP_STRUCT && TYPE_CODE(arg2->enclosing_type) != TYPE_CODE_REF )
              	(void) find_overload_match (arg_types, nargs, tstr,
                                  	   NON_POLYMORPHIC_CALL /* method called through structure */ ,
				           0 /* strict match */ ,
                                           arg2 /* the object */ , NULL,
                                           &valp, NULL, &static_memfuncp); 
	      else
                (void) find_overload_match (arg_types, nargs, tstr,
                                           POLYMORPHIC_CALL /* method called through ptr/reference */ , 
					   0 /* strict match */ ,
                                           arg2 /* the object */ , NULL,
                                           &valp, NULL, &static_memfuncp);

	      argvec[1] = arg2;	/* the ``this'' pointer */
	      argvec[0] = valp;	/* use the method found after overload resolution */
	    }
	  else
	    /* Non-C++ case -- or no overload resolution */
	    {
	      temp = arg2;
	      argvec[0] = value_struct_elt (&temp, argvec + 1, tstr,
					    &static_memfuncp,
					    op == STRUCTOP_STRUCT
				       ? "structure" : "structure pointer");
	      argvec[1] = arg2;	/* the ``this'' pointer */
	    }

	  if (static_memfuncp)
	    {
	      argvec[1] = argvec[0];
	      nargs--;
	      argvec++;
	    }
	}
      /* RM: handle calls of the form "A::foo()" */
      else if (op == OP_SCOPE)
        {
          char tstr[256];
          value_ptr temp;
          struct type *t;
          
          /* Name of method from expression */ 
          strcpy(tstr, &exp->elts[pc+6].string);

          /* RM: We need an object in which to find a method. However,
             we only have a type ("A") in this situation. Create a
             dummy object of the right type */
          t = exp->elts[pc+4].type;
          temp = allocate_value(t);
          
          if (overload_resolution && (exp->language_defn->la_language == language_cplus))
            {
              /* Language is C++, do some overload resolution before evaluation */
              value_ptr valp = NULL;
              
              /* Prepare list of argument types for overload resolution */ 
              arg_types = (struct type **) xmalloc (nargs * (sizeof (struct type *)));
              for (ix=1; ix <= nargs; ix++)
                arg_types[ix-1] = VALUE_TYPE (argvec[ix]);

             (void) find_overload_match (arg_types, nargs, tstr,
                                         NON_POLYMORPHIC_CALL/* method */, 0 /* strict match */,
                                         temp /* the object */, NULL,
                                         &valp, NULL, NULL);

              argvec[0] = valp;  /* use the method found after overload resolution */ 
            }
          else /* no overload resolution */ 
            {
              argvec[0] = value_struct_elt (&temp, argvec+1, tstr,
                                            NULL,
                                            "structure");
            }
        }
      else if (op == STRUCTOP_MEMBER || op == STRUCTOP_MPTR)
	{
	  argvec[1] = arg2;
	  argvec[0] = arg1;
	}
      else if (op == OP_VAR_VALUE)
	{
	  /* Non-member function being called */
          /* fn: This can only be done for C++ functions.  A C-style function
             in a C++ program, for instance, does not have the fields that 
             are expected here */

	  if (overload_resolution && (exp->language_defn->la_language == language_cplus))
	    {
	      /* Language is C++, do some overload resolution before evaluation */
	      struct symbol *symp;

	      /* Prepare list of argument types for overload resolution */
	      arg_types = (struct type **) xmalloc (nargs * (sizeof (struct type *)));
	      for (ix = 1; ix <= nargs; ix++)
		arg_types[ix - 1] = VALUE_TYPE (argvec[ix]);

	      (void) find_overload_match (arg_types, nargs, NULL /* no need for name */ ,
		       	  NON_METHOD_CALL/* not method */ , 0 /* strict match */ ,
		 	  NULL, exp->elts[save_pos1+2].symbol /* the function */ ,
			  NULL, &symp, NULL);

	      /* Now fix the expression being evaluated */
	      exp->elts[save_pos1+2].symbol = symp;
	      argvec[0] = evaluate_subexp_with_coercion (exp, &save_pos1, noside);
	    }
	  else
	    {
	      /* Not C++, or no overload resolution allowed */
	      /* nothing to be done; argvec already correctly set up */
	    }
	}
      else
	{
	  /* It is probably a C-style function */
	  /* nothing to be done; argvec already correctly set up */
	}

    do_call_it:

      if (noside == EVAL_SKIP)
	goto nosideret;
      if (noside == EVAL_AVOID_SIDE_EFFECTS)
	{
	  /* If the return type doesn't look like a function type, call an
	     error.  This can happen if somebody tries to turn a variable into
	     a function call. This is here because people often want to
	     call, eg, strcmp, which gdb doesn't know is a function.  If
	     gdb isn't asked for it's opinion (ie. through "whatis"),
	     it won't offer it. */

	  struct type *ftype =
	  TYPE_TARGET_TYPE (VALUE_TYPE (argvec[0]));

	  if (ftype)
	    return allocate_value (TYPE_TARGET_TYPE (VALUE_TYPE (argvec[0])));
	  else
	    error ("Expression of type other than \"Function returning ...\" used as function");
	}
      if (argvec[0] == NULL)
	error ("Cannot evaluate function -- may be inlined");
      
/* RM: Do we need to allocate space for return value? */
      {
        struct type *ftype = VALUE_TYPE (argvec[0]);
        int aggretx_pos = -1;
        int jj;

        aggretx_pos = TYPE_RETVAL_PTR_IDX(ftype);
        if (aggretx_pos != -1)
          {
            for (jj = nargs; jj > aggretx_pos; jj--)
              argvec[jj+1] = argvec[jj];
            argvec[aggretx_pos+1] =
              value_allocate_space_in_inferior(
                TYPE_LENGTH(TYPE_TARGET_TYPE(VALUE_TYPE (argvec[0]))));
            nargs++;
          }
      }

      return call_function_by_hand (argvec[0], nargs, argvec + 1);

    case OP_F77_UNDETERMINED_ARGLIST:

      /* Remember that in F77, functions, substring ops and 
         array subscript operations cannot be disambiguated 
         at parse time.  We have made all array subscript operations, 
         substring operations as well as function calls  come here 
         and we now have to discover what the heck this thing actually was.  
         If it is a function, we process just as if we got an OP_FUNCALL. */

      nargs = longest_to_int (exp->elts[pc + 1].longconst);
      (*pos) += 2;

      /* First determine the type code we are dealing with.  */
      op = exp->elts[(*pos)].opcode;
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      type = check_typedef (VALUE_TYPE (arg1));
      code = TYPE_CODE (type);

      if (code == TYPE_CODE_REF || code == TYPE_CODE_ARRAY_DESC)
        code = TYPE_CODE (TYPE_TARGET_TYPE(type));

      switch (code)
	{
	case TYPE_CODE_ARRAY:
	  goto multi_f77_subscript;

	case TYPE_CODE_STRING:
	  goto op_f77_substr;

	case TYPE_CODE_PTR:
	case TYPE_CODE_FUNC:
	  /* It's a function call. */
	  /* Allocate arg vector, including space for the function to be
	     called in argvec[0] and a terminating NULL */
	  argvec = (value_ptr *) alloca (sizeof (value_ptr) * (nargs + 2));
	  argvec[0] = arg1;
	  tem = 1;
	  for (; tem <= nargs; tem++)
	    argvec[tem] = evaluate_subexp_with_coercion (exp, pos, noside);
	  argvec[tem] = 0;	/* signal end of arglist */
	  goto do_call_it;

	default:
	  error ("Cannot perform substring on this type");
	}

    op_f77_substr:
      /* Check whether this is a Fortran intrinsic  -- the only expected op's
       * are OP_STRING created in f-exp.y when an intrinsic name is recognized
       * or OP_VAR_VALUE for a string variable. Unknown symbols are caught
       * when parsing the expression. 
       */
      if (op == OP_STRING) 
        return evaluate_subexp_for_ftn_intrinsic (arg1, nargs, exp, pos,
						  noside);

      /* We have a substring operation on our hands here, 
         let us get the string we will be dealing with */

      /* Now evaluate the 'from' and 'to' */

      switch (exp->elts[*pos].opcode)
	{
        /* Process the new opcode TERNOP_FTN_ARRAY_SLICE to evaluate
	   'from' and 'to' */
	case TERNOP_FTN_ARRAY_SLICE:
	{
	  LONGEST retcode, upper, lower;
	  struct type *tmp_type;
	  enum noside no_side;

	  (*pos)++;
	  tmp_type = check_typedef (VALUE_TYPE (arg1));

	  /* Evaluate lower and upper bound */
	  retcode = f77_get_dynamic_lowerbound (tmp_type, &lower);
	  if (retcode == BOUND_FETCH_ERROR)
            error ("Cannot obtain dynamic lower bound");

          retcode = f77_get_dynamic_upperbound (tmp_type, &upper);
          if (retcode == BOUND_FETCH_ERROR)
            error ("Cannot obtain dynamic upper bound");

	  /* Evaluate the subscript and range value */
	  no_side = exp->elts[*pos + 1].type ? EVAL_NORMAL : EVAL_SKIP;
	  arg2 = evaluate_subexp_with_coercion (exp, pos, no_side);
	  tem2 = (int)((no_side == EVAL_NORMAL) ? value_as_long (arg2) : lower);

	  no_side = exp->elts[*pos + 1].type ? EVAL_NORMAL : EVAL_SKIP;
	  arg3 = evaluate_subexp_with_coercion (exp, pos, no_side);
	  tem3 = (int)((no_side == EVAL_NORMAL) ? (int) value_as_long (arg3) : upper);

	  /* skip the third OP_LONG expression - the 'stride' */
	  evaluate_subexp_with_coercion (exp, pos, EVAL_SKIP);

          return value_slice (arg1, tem2, tem3 - tem2 + 1);
	}
	default: /* OP_LONG */
          arg2 = evaluate_subexp_with_coercion (exp, pos, noside);

	  if (nargs < 2)
	    return value_subscript (arg1, arg2);

          arg3 = evaluate_subexp_with_coercion (exp, pos, noside);

          if (noside == EVAL_SKIP)
            goto nosideret;

          tem2 = (int) value_as_long (arg2);
          tem3 = (int) value_as_long (arg3);

          return value_slice (arg1, tem2, tem3 - tem2 + 1);
	}

    case OP_COMPLEX:
      /* We have a complex number, There should be 2 floating 
         point numbers that compose it */
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);

      return value_literal_complex (arg1, arg2, builtin_type_f_complex_s16);

    case STRUCTOP_STRUCT:
      tem = longest_to_int (exp->elts[pc + 1].longconst);
      (*pos) += 3 + BYTES_TO_EXP_ELEM (tem + 1);
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (noside == EVAL_AVOID_SIDE_EFFECTS)
        {
          struct type * fieldtype = lookup_struct_elt_type (VALUE_TYPE (arg1),
                                                   &exp->elts[pc + 2].string,
                                                   0);
          /* We need to know the address of the variable of an array
           * descriptor type because the bounds are determined at runtime
           * so treat it like a variable
           */
          if (TYPE_CODE (fieldtype) == TYPE_CODE_ARRAY_DESC)
            {
              value_ptr temp = arg1;
              arg1 = value_struct_elt (&temp, NULL, &exp->elts[pc + 2].string,
                                       NULL, "structure");
              f_fixup_array_desc_type_array_addr (VALUE_TYPE (arg1),
                VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1));
              return arg1;
            }
          else 
            return value_zero (fieldtype, lval_memory);
        }
      else
        {
          value_ptr temp = arg1;
          arg1 = value_struct_elt (&temp, NULL, &exp->elts[pc + 2].string,
                                   NULL, "structure");
          if (TYPE_CODE (VALUE_TYPE (arg1)) == TYPE_CODE_ARRAY_DESC)
            f_fixup_array_desc_type_array_addr (VALUE_TYPE (arg1),
              VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1));
          return arg1;
	}

    case STRUCTOP_PTR:
      tem = longest_to_int (exp->elts[pc + 1].longconst);
      (*pos) += 3 + BYTES_TO_EXP_ELEM (tem + 1);
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
        goto nosideret;

      /* JYG: if print object is on we need to replace the base type
	 with rtti type in order to continue on with successful
	 lookup of member / method only available in the rtti type. */
      {
        struct type *type = VALUE_TYPE (arg1);
        struct type *real_type;
        int full, top, using_enc;
        
        if (   objectprint && TYPE_TARGET_TYPE(type) 
	    && ( (TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_CLASS)
	        || (TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_STRUCT)))
          {
            real_type = value_rtti_target_type (arg1, &full, &top, &using_enc);
            if (real_type)
              {
                if (TYPE_CODE (type) == TYPE_CODE_PTR)
                  real_type = lookup_pointer_type (real_type);
                else
                  real_type = lookup_reference_type (real_type);

                arg1 = value_cast (real_type, arg1);
              }
          }
      }

      if (noside == EVAL_AVOID_SIDE_EFFECTS)
	return value_zero (lookup_struct_elt_type (VALUE_TYPE (arg1),
						   &exp->elts[pc + 2].string,
						   0),
			   lval_memory);
      else
	{
	  value_ptr temp = arg1;
	  return value_struct_elt (&temp, NULL, &exp->elts[pc + 2].string,
				   NULL, "structure pointer");
	}

    case STRUCTOP_MEMBER:
      arg1 = evaluate_subexp_for_address (exp, pos, noside); 
      second_pos = *pos;
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);

      /* With HP aCC, pointers to methods do not point to the function code */
      if ((hp_som_som_object_present || hp_dwarf2_object_present ) &&
	  (TYPE_CODE (VALUE_TYPE (arg2)) == TYPE_CODE_PTR) &&
      (TYPE_CODE (TYPE_TARGET_TYPE (VALUE_TYPE (arg2))) == TYPE_CODE_METHOD))
	error ("Pointers to methods not supported with HP aCC");	/* 1997-08-19 */

      /* Poorva we need to read both words instead of just the first
	 word. Pointers are 32 bits in gdb but data could be 64 */

#ifndef HP_IA64
      mem_offset = value_as_long (arg2);
#else
      if (!is_swizzled) /* LP64 */
	mem_offset = value_as_longest (arg2);
      else
	mem_offset = value_as_long (arg2);
#endif
      
      goto handle_pointer_to_member;

    case STRUCTOP_MPTR:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      second_pos = *pos;
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);

      /* With HP aCC, pointers to methods do not point to the function code */
      if ((hp_som_som_object_present || hp_dwarf2_object_present ) &&
	  (TYPE_CODE (VALUE_TYPE (arg2)) == TYPE_CODE_PTR) &&
      (TYPE_CODE (TYPE_TARGET_TYPE (VALUE_TYPE (arg2))) == TYPE_CODE_METHOD))
	error ("Pointers to methods not supported with HP aCC");	/* 1997-08-19 */


#ifndef HP_IA64
      mem_offset = value_as_long (arg2);
#else
      if (!is_swizzled) /* LP64 */
	mem_offset = value_as_longest (arg2);
      else
	mem_offset = value_as_long (arg2);
#endif

    handle_pointer_to_member:
      /* HP aCC generates offsets that have bit #29 set; turn it off to get
         a real offset to the member. */
      if (hp_som_som_object_present || hp_dwarf2_object_present)
	{
          /* For IPF a null pointer is represented as -1 - C++ ABI */
	  if (new_cxx_abi)
	    {
	      if (is_swizzled)
		{
		  if (mem_offset == ((unsigned int) (-1)))
		    error ("Attempted dereference of null pointer-to-member");
		}
	      else
		if (mem_offset == ((unsigned long)(long) (-1)))
		  error ("Attempted dereference of null pointer-to-member");
	    }
	  else
	    /* For PA mem_offset is zero for null pointer */
	    if (!mem_offset && !IS_TARGET_LRE)	/* no bias -> really null */
	      error ("Attempted dereference of null pointer-to-member");

	  if (hp_som_som_object_present)
	    mem_offset &= ~0x20000000;
	}
      if (noside == EVAL_SKIP)
	goto nosideret;
      type = check_typedef (VALUE_TYPE (arg2));
      if (TYPE_CODE (type) != TYPE_CODE_PTR)
	goto bad_pointer_to_member;
      type = check_typedef (TYPE_TARGET_TYPE (type));
      if (TYPE_CODE (type) == TYPE_CODE_METHOD)
	error ("not implemented: pointer-to-method in pointer-to-member construct");
      if (TYPE_CODE (type) != TYPE_CODE_MEMBER)
	goto bad_pointer_to_member;

      /* JAGaf63770: 
         Analysis: we have a situation where classX.classY::fieldZ 
         returns incorrect value.  value_cast below sends classY to
         search_struct_field and rtti resolution to get the address and 
         add mem_offset.  It does not have any information about fieldZ.
         By luck, if fieldZ belongs to classY it returns the correct value.  
         When fieldZ does not belong to classY value_cast still returns
         address of classY.  JAG contains different examples to illustrate
         cases where fieldZ belongs to a parent of classY.  Virtual,
         multiple inheritance is a beast.  There are comments FIXME
         everywhere in gdb, there are several JAGs in gdb.c++.  This
         new fix below is not suppose to fix ALL problems.  

         Fix: When fieldZ does not belong to classY, search_struct_field
         and rtti resolution must have knowledge about the field or 
         else it will not be able to select the correct class in the 
         inheritance chain.  First, find out if fieldZ belongs to classY.  
         If it does not then pass fieldZ along so it can look up properly.  
         A similar situation that gdb can handle correctly already is 
         classX.fieldZ.  In this case gdb passes information about 
         classX and fieldZ along.  
         
         Tests: Besides the test case provided by the submitter, 
         inherit.exp and member-ptr.exp are good tests to exercise 
         this part of the code.  

         Variables:
         second_pos - the pos before doing evaluate_subexp on arg2 
         special_field - flag to indicate whether to do the old way or not
         current_field - field in question like fieldZ (could be NULL)
         scope_type1, scope_type2 - extract the type to get the class
         name (classY).  For readability, have 2 of them but it can be
         done with just one variable.    
      */

      /* 
       * Starting at the second position to the end (nelts)
       * search for the field string.  String is at 
       *   if exp->elts[n].opcode == UNOP_ADDR
       *   and exp->elts[n+1].opcode == OP_SCOPE
       *   then field = &exp->elts[n+4].string.  
       * This might look like a hack but it is not.  OP_SCOPE case 
       * in this file has similar detection for object.class_name::member.
       * If opcode for n is UNOP_ADDR and opcode for n+1 is OP_SCOPE,
       * expect the string to be at n+4.
       */ 
      current_field = NULL;

      for (i = second_pos; i < exp->nelts; i++)
        {
          if ((exp->elts[i].opcode == UNOP_ADDR) &&
              (exp->elts[i+1].opcode == OP_SCOPE))
            {
              current_field =  &exp->elts[i + 4].string;
              break; 
            }
        }

      /* 
       * if there is a field go look to find if it belongs in this class. 
       * if it does not then do something special.  If it does keep
       * doing the same thing as before.
       */
      if (current_field)
        {
          scope_type1 = lookup_pointer_type (TYPE_DOMAIN_TYPE (type));
          scope_type2 = check_typedef (TYPE_TARGET_TYPE (scope_type1));
          special_field = TRUE;

          for (i = TYPE_NFIELDS (scope_type2) - 1; i >= 0 ; i--)
            {
              char *t_field_name = TYPE_FIELD_NAME (scope_type2, i);
              if (t_field_name && 
                  ((strcmp_iw (t_field_name, current_field) == 0)))
                {
                  special_field = FALSE;
                  break; 
                } /* if */
            }     /* for */

          /* there exists a field but it does not belong to the class */
          if (special_field)
            {
              /* arg1 is in a reg, introduce temparg to avoid warning */
              value_ptr temparg = arg1;
              arg3 = value_struct_elt (&temparg, NULL, current_field,
                                       NULL, "structure");
              return arg3; 
            } /* if special_field */
        }     /* if current_field */

      /* Now, convert these values to an address.  */
      arg1 = value_cast (lookup_pointer_type (TYPE_DOMAIN_TYPE (type)),
			 arg1);

#ifndef HP_IA64
      arg3 = value_from_pointer (lookup_pointer_type (TYPE_TARGET_TYPE (type)),
				 value_as_long (arg1) + mem_offset);
#else
      val_arg1 = is_swizzled ? value_as_long (arg1) : value_as_longest (arg1);

      arg3 = value_from_pointer (lookup_pointer_type (TYPE_TARGET_TYPE (type)),
				  val_arg1 + mem_offset );
#endif
      return value_ind (arg3);
    bad_pointer_to_member:
      error ("non-pointer-to-member value used in pointer-to-member construct");

    case BINOP_CONCAT:
      arg1 = evaluate_subexp_with_coercion (exp, pos, noside);
      arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, op, OP_NULL, noside);
      else
	return value_concat (arg1, arg2);

    case BINOP_ASSIGN:
#ifdef HP_IA64
      check_hp_modifiability (exp, *pos);
#endif /* HP_IA64 */
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = true;
#endif
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);

#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = false;
#endif

      /* Do special stuff for HP aCC pointers to members */
      if (hp_som_som_object_present || hp_dwarf2_object_present)
	{
	  struct type *arg1_type = VALUE_TYPE (arg1);

	  while (TYPE_CODE (arg1_type) == TYPE_CODE_TYPEDEF)	/* Fix for JAGaf22634 */
	    arg1_type = TYPE_TARGET_TYPE (arg1_type);

	  /* 1997-08-19 Can't assign HP aCC pointers to methods. No details of
	     the implementation yet; but the pointer appears to point to a code
	     sequence (thunk) in memory -- in any case it is *not* the address
	     of the function as it would be in a naive implementation. */
	  if ((TYPE_CODE (arg1_type) == TYPE_CODE_PTR) &&
	      (TYPE_CODE (TYPE_TARGET_TYPE (arg1_type)) == TYPE_CODE_METHOD))
	    error ("Assignment to pointers to methods not implemented with HP aCC");
	  if (hp_som_som_object_present)
	    /* HP aCC pointers to data members require a constant bias */
	    if ((TYPE_CODE (arg1_type) == TYPE_CODE_PTR) &&
		(TYPE_CODE (TYPE_TARGET_TYPE (arg1_type)) == TYPE_CODE_MEMBER))
	      {
		/* forces evaluation */ 
		/* RM: changed int to CORE_ADDR */
		CORE_ADDR * ptr = (CORE_ADDR *)(void *) VALUE_CONTENTS (arg2);
		*ptr |= 0x20000000;	/* set 29th bit */
	    }
	}

      if (noside == EVAL_SKIP || noside == EVAL_AVOID_SIDE_EFFECTS)
	return arg1;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, op, OP_NULL, noside);
      else
	return value_assign (arg1, arg2);

    case BINOP_ASSIGN_MODIFY:
      (*pos) += 2;
#ifdef HP_IA64
      check_hp_modifiability (exp, *pos);
#endif /* HP_IA64 */
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = true;
#endif
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = false;
#endif
      if (noside == EVAL_SKIP || noside == EVAL_AVOID_SIDE_EFFECTS)
	return arg1;
      op = exp->elts[pc + 1].opcode;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, BINOP_ASSIGN_MODIFY, op, noside);
      else if (op == BINOP_ADD)
	arg2 = value_add (arg1, arg2);
      else if (op == BINOP_SUB)
	arg2 = value_sub (arg1, arg2);
      else
	arg2 = value_binop (arg1, arg2, op);
      return value_assign (arg1, arg2);

    case BINOP_ADD:
      arg1 = evaluate_subexp_with_coercion (exp, pos, noside);
      arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, op, OP_NULL, noside);
      else
	return value_add (arg1, arg2);

    case BINOP_SUB:
      arg1 = evaluate_subexp_with_coercion (exp, pos, noside);
      arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, op, OP_NULL, noside);
      else
	return value_sub (arg1, arg2);

    case BINOP_MUL:
    case BINOP_DIV:
    case BINOP_REM:
    case BINOP_MOD:
    case BINOP_EXP:
    case BINOP_LSH:
    case BINOP_RSH:
    case BINOP_BITWISE_AND:
    case BINOP_BITWISE_IOR:
    case BINOP_BITWISE_XOR:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, op, OP_NULL, noside);
      else if (noside == EVAL_AVOID_SIDE_EFFECTS
	       && (op == BINOP_DIV || op == BINOP_REM || op == BINOP_MOD))
	return value_zero (VALUE_TYPE (arg1), not_lval);
      else
	return value_binop (arg1, arg2, op);

    case BINOP_RANGE:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      error ("':' operator used in invalid context");

    case BINOP_SUBSCRIPT:
      arg1 = evaluate_subexp_with_coercion (exp, pos, noside);
      arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	return value_x_binop (arg1, arg2, op, OP_NULL, noside);
      else
	{
	  /* If the user attempts to subscript something that is not an
	     array or pointer type (like a plain int variable for example),
	     then report this as an error. */

	  COERCE_REF (arg1);
	  type = check_typedef (VALUE_TYPE (arg1));
	  if (TYPE_CODE (type) != TYPE_CODE_ARRAY
	      && TYPE_CODE (type) != TYPE_CODE_PTR)
	    {
	      if (TYPE_NAME (type))
		error ("cannot subscript something of type `%s'",
		       TYPE_NAME (type));
	      else
		error ("cannot subscript requested type");
	    }

	  if (noside == EVAL_AVOID_SIDE_EFFECTS)
	    return value_zero (TYPE_TARGET_TYPE (type), lval_memory);
	  else
	    return value_subscript (arg1, arg2);
	}

    case BINOP_IN:
      arg1 = evaluate_subexp_with_coercion (exp, pos, noside);
      arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      return value_in (arg1, arg2);

    case MULTI_SUBSCRIPT:
      (*pos) += 2;
      nargs = longest_to_int (exp->elts[pc + 1].longconst);
      arg1 = evaluate_subexp_with_coercion (exp, pos, noside);
      while (nargs-- > 0)
	{
	  arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
	  /* FIXME:  EVAL_SKIP handling may not be correct. */
	  if (noside == EVAL_SKIP)
	    {
	      if (nargs > 0)
		{
		  continue;
		}
	      else
		{
		  goto nosideret;
		}
	    }
	  /* FIXME:  EVAL_AVOID_SIDE_EFFECTS handling may not be correct. */
	  if (noside == EVAL_AVOID_SIDE_EFFECTS)
	    {
	      /* If the user attempts to subscript something that has no target
	         type (like a plain int variable for example), then report this
	         as an error. */

	      type = TYPE_TARGET_TYPE (check_typedef (VALUE_TYPE (arg1)));
	      if (type != NULL)
		{
		  arg1 = value_zero (type, VALUE_LVAL (arg1));
		  noside = EVAL_SKIP;
		  continue;
		}
	      else
		{
		  error ("cannot subscript something of type `%s'",
			 TYPE_NAME (VALUE_TYPE (arg1)));
		}
	    }

	  if (binop_user_defined_p (op, arg1, arg2))
	    {
	      arg1 = value_x_binop (arg1, arg2, op, OP_NULL, noside);
	    }
	  else
	    {
	      arg1 = value_subscript (arg1, arg2);
	    }
	}
      return (arg1);

    multi_f77_subscript:
      {
	LONGEST subscript_array[MAX_FORTRAN_DIMS + 1];	/* 1-based array of 
							   subscripts, max == 7 */
	LONGEST array_size_array[MAX_FORTRAN_DIMS + 1];
	int ndimensions = 1, i,ind;
	struct type *tmp_type;
	int offset_item;	/* The array offset where the item lives */
        LONGEST stride[MAX_FORTRAN_DIMS+1];
        int is_array_desc = 0;

	boolean ftn_slice_flag = FALSE;
	int nelms = 1;
	LONGEST range_array[MAX_FORTRAN_DIMS + 1];
	LONGEST stride_array[MAX_FORTRAN_DIMS + 1];
	value_ptr range_val, stride_val, tmp_val = NULL;

	/* initialize subscript, slice and stride arrays */
	for (i=0; i<=MAX_FORTRAN_DIMS; i++)
	  subscript_array[i] = range_array[i] = 0;
	for (i=0; i<=MAX_FORTRAN_DIMS; i++)
	  stride_array[i] = 1;
	
	if (nargs > MAX_FORTRAN_DIMS)
	  error ("Too many subscripts for Fortran (%d Max)", MAX_FORTRAN_DIMS);

	tmp_type = check_typedef (VALUE_TYPE (arg1));

        if (TYPE_CODE (tmp_type) == TYPE_CODE_REF)
          {
            COERCE_REF (arg1);
            tmp_type = TYPE_TARGET_TYPE (tmp_type);
          }

        if (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY_DESC)
          {
            is_array_desc = 1;
#ifdef F_CHECK_ARRAY_DESC
            /* check if variable is initialized */
            F_CHECK_ARRAY_DESC (TYPE_ARRAY_ADDR (TYPE_TARGET_TYPE(tmp_type)) +
                                TYPE_ARRAY_DESC_ALLOCATED_OFFSET (type));
#endif
            tmp_type = TYPE_TARGET_TYPE (tmp_type);
            f77_get_dynamic_length_of_aggregate (tmp_type);
            /* Dereference the array descriptor to get the address of the 
             * actual array
             */
#ifdef F_GET_ARRAY_DESC_ARRAY_ADDR
#  ifndef HP_IA64
            VALUE_ADDRESS (arg1) = 
              F_GET_ARRAY_DESC_ARRAY_ADDR (TYPE_ARRAY_ADDR (tmp_type));
#  else
            VALUE_ADDRESS (arg1) = 
	      F_GET_ARRAY_DESC_ARRAY_ADDR (TYPE_ARRAY_ADDR (tmp_type),
		     (int) TYPE_ARRAY_DESC_BASE_ADDR_OFFSET (type));
#  endif
#endif
          }

        ndimensions = calc_f77_array_dims (tmp_type);

	if (nargs != ndimensions)
	  error ("Wrong number of subscripts");

	/* Evaluate the lower and upper bounds for all subscripts */
	for (i = 1; i <= nargs; i++)
	  {
	    /* Evaluate lower and upper bound */
	    retcode = f77_get_dynamic_upperbound (tmp_type, &upper);
	    if (retcode == BOUND_FETCH_ERROR)
	      error ("Cannot obtain dynamic upper bound");

	    retcode = f77_get_dynamic_lowerbound (tmp_type, &lower);
	    if (retcode == BOUND_FETCH_ERROR)
	      error ("Cannot obtain dynamic lower bound");

#ifdef FTN_DIMENSIONS_REVERSED
	    /* Bounds are fetched in reverse order of the subscripts */
	    upper_bound[nargs + 1 - i] = upper;
	    lower_bound[nargs + 1 - i] = lower;

	    if (is_array_desc)
	      stride[nargs + 1 - i] = TYPE_ARRAY_STRIDE (tmp_type);
#else
	    upper_bound[i] = upper;
	    lower_bound[i] = lower;
#endif
	    /* If we are at the bottom of a multidimensional 
	       array type then keep a ptr to the last ARRAY
	       type around for use when calling value_subscript()
	       below. This is done because we pretend to value_subscript
	       that we actually have a one-dimensional array 
	       of base element type that we apply a simple 
	       offset to. */

	    if (i < nargs)
	      tmp_type = check_typedef (TYPE_TARGET_TYPE (tmp_type));
	  }


	/* Now that we know we have a legal array subscript expression 
	   let us actually find out where this element exists in the array. */

	/* Iterate over the dimensions of the array subscript expression. */
	for (i = 1; i <= nargs; i++)
	  {
	    /* process TERNOP_FTN_ARRAY_SLICE to evaluate subscript, range and stride
	       Fill in subscript, range and stride arrays */

	    print_ftn_array_slice = FALSE;
	    switch (exp->elts[*pos].opcode)
	      {
	      case TERNOP_FTN_ARRAY_SLICE:
	      {
		enum noside no_side;
		(*pos)++;
		ftn_slice_flag = TRUE;

		/* Evaluate Subscript */
		no_side = exp->elts[*pos + 1].type ? EVAL_NORMAL : EVAL_SKIP;
		arg2 = evaluate_subexp_with_coercion (exp, pos, no_side);

		if (no_side == EVAL_NORMAL)
		  {
		    subscript_array[i] = value_as_long (arg2);

		    if (subscript_array[i] > upper_bound[i] || subscript_array[i] < lower_bound[i])
			error ("In dimension %d, array subscript (%lld) is out of range (%lld...%lld)",
				  i, subscript_array[i], lower_bound[i], upper_bound[i]);
		  }
		else
		  subscript_array[i] = lower_bound[i];

                /* Zero-normalize subscripts so that offsetting will work. */ 
                subscript_array[i] -= lower_bound[i];

		/* Evaluate Range */
		no_side = exp->elts[*pos + 1].type ? EVAL_NORMAL : EVAL_SKIP;
		range_val = evaluate_subexp_with_coercion (exp, pos, no_side);

		if (no_side == EVAL_NORMAL)
		  {
		    range_array[i] = value_as_long (range_val);
		    if (range_array[i] > upper_bound[i] || range_array[i] < lower_bound[i])
			error ("In dimension %d, range specifier (%lld) is out of range (%lld...%lld)",
				  i, range_array[i], lower_bound[i], upper_bound[i]);
		  }
		else
		  range_array[i] = upper_bound[i];

	        /* Zero normalize range array - only if range specified explicitly */
		if (range_array[i] >= lower_bound[i]) 
	          range_array[i] -= lower_bound[i];

		/* Evaluate stride */
		no_side = exp->elts[*pos + 1].type ? EVAL_NORMAL : EVAL_SKIP;
		stride_val = evaluate_subexp_with_coercion (exp, pos, no_side);
		stride_array[i] = (no_side == EVAL_NORMAL) ? value_as_long (stride_val) : 1;

		/* stride can be a negative integer, only if subscript is greater than range.
		   this enables to print in reverse order */

		if ((subscript_array[i] < range_array[i]) && stride_array[i] < 0)
		  error ("Invalid stride");

		if ((subscript_array[i] > range_array[i]) && stride_array[i] > 0)
		  error ("Invalid stride");

		break;
	      }
	      default: /* OP_LONG */
		/* Evaluate subscript and fill in subscirpt array */
		arg2 = evaluate_subexp_with_coercion (exp, pos, noside);
		subscript_array[i] = value_as_long (arg2);

                /* Zero-normalize subscripts so that offsetting will work. */ 
                subscript_array[i] -= lower_bound[i];

		break;
	      }

	    /* Calculate size of each dimension */
	    array_size_array[i] = upper_bound[i] - lower_bound[i] + 1;
	  }

	/* if ftn_slice_flag is set then construct slicevector - contains
	   index for all the elements within the slice to be printed. And
	   calculate offset */

	offset_item = 0;
	if (ftn_slice_flag)
	  {
	    /* Evaluate number of elements
	       alloc the reqd memory
	       construct slice vector
	       Evaluate offset for all the elements in slice vector
	     */
            unsigned int elt_len = 0;
	    for (i=1; i<=nargs; i++)
	      {
		int lo_val = (int) subscript_array[i]; /* Zero normalized */
		int hi_val = (int) range_array[i]; /* Zero normalized */
		int stride = (int) stride_array[i]; /* No Zero normalized */
		
		/* In an expression like <expr1:expr2:expr3>
		   stride (expr3) can be -ve, if the low_bound (expr1) is 
		   greater than range (expr2). This is to print the elements
		   in reverse order */
		if (stride < 0)
		  nelms *= (lo_val - hi_val)/(stride * -1) + 1;
		else if (hi_val && (hi_val >= lo_val))
		  nelms *= (hi_val - lo_val)/stride + 1;
	      }

	    /* allocate reqd memory */
	    slice_vector = (slicevector *) xmalloc (FTN_SIZEOF_SLICE_VECTOR + 
		 	    (nelms * sizeof(subscript_array)));
            /* Store no of elements in the slice and dimensions
               of the array */ 
	    FTN_SLICE_VECTOR_NELMS(slice_vector) = nelms;
	    FTN_SLICE_VECTOR_DIMS(slice_vector) = ndimensions;

	    /* create slice vector */
	    construct_slice_vector (nargs, subscript_array, range_array, stride_array,
				    slice_vector, 1);

	    /* calculate offset value for all the elements */
	    offset_tbl = (offset_table *) xmalloc (FTN_SIZEOF_OFFSET_TBL +
			  (nelms * sizeof(LONGEST)));
	    FTN_OFFSET_TBL_NELMS(offset_tbl) = nelms;

	    /* Find the length of an element */
	    tmp_type = VALUE_TYPE(arg1);
	    while ( TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY ||
		    TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY_DESC )
	      tmp_type = TYPE_TARGET_TYPE (tmp_type);
	    elt_len = TYPE_LENGTH (tmp_type);

	    /* Iterate over the elements of the array slice. */
	    for (ind=1; ind<=nelms; ind++)
	      {
	        LONGEST *slice_elm = FTN_SLICE_VECTOR_ELM(slice_vector, ind);
		
		if (is_array_desc)
		  {
		    offset_item = (int) (slice_elm[ndimensions] * stride[ndimensions]);
		    for(i=ndimensions - 1; i>= 1; i--)
		      offset_item = offset_item + (int) (slice_elm[i] * stride[i]);
		  }
		else
		  {
		    offset_item = (int) slice_elm[ndimensions];
		    for (i = ndimensions - 1; i>= 1; i--)
		      offset_item = (int)(array_size_array[i] * offset_item + slice_elm[i]);
		    offset_item *= elt_len;
		  }
		FTN_OFFSET_TBL_ELM(offset_tbl, ind) = offset_item;
	      }

	    print_ftn_array_slice = TRUE;
	    return arg1;
	  }

	  /* handle normal multi-dim fortran array element printing */
          offset_item = (int) subscript_array[ndimensions];

          if (is_array_desc)
            {
              offset_item = (int) (subscript_array[ndimensions] * stride[ndimensions]);
              for (i = ndimensions - 1; i >= 1; i--)
                offset_item = offset_item + (int)(subscript_array[i] * stride[i]);
            }
          else
	    {
	      offset_item = (int) subscript_array[ndimensions];
		
	      for (i = ndimensions - 1; i>= 1; i--)
	        offset_item = (int) array_size_array[i] * offset_item + (int) subscript_array[i];
	    }

          /* Create value_ptr node for this slice element with the value of the offset */
	  arg2 = value_from_longest (builtin_type_f_integer, offset_item);

	  /* Let us now play a dirty trick: we will take arg1 
	     which is a value node pointing to the topmost level
	     of the multidimensional array-set and pretend
	     that it is actually a array of the final element 
	     type, this will ensure that value_subscript()
	     returns the correct type value */

	  VALUE_TYPE (arg1) = tmp_type;
          if (is_array_desc)
            {
              /* offset_item is a byte offset relative to the base address
	         of the array, so we need to adjust VALUE_ADDRESS which
                 holds the address of the array */
              VALUE_ADDRESS (arg1) += offset_item;
              return (value_ind (value_coerce_array (arg1)));
            }
          else
	    return (value_ind (value_add (value_coerce_array (arg1), arg2)));
      }

    case BINOP_LOGICAL_AND:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	{
	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	  goto nosideret;
	}

      oldpos = *pos;
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, EVAL_AVOID_SIDE_EFFECTS);
      *pos = oldpos;

      if (binop_user_defined_p (op, arg1, arg2))
	{
	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
	  tem = value_logical_not (arg1);
	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos,
				  (tem ? EVAL_SKIP : noside));
	  return value_from_longest (LA_BOOL_TYPE,
			     (LONGEST) (!tem && !value_logical_not (arg2)));
	}

    case BINOP_LOGICAL_OR:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	{
	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	  goto nosideret;
	}

      oldpos = *pos;
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, EVAL_AVOID_SIDE_EFFECTS);
      *pos = oldpos;

      if (binop_user_defined_p (op, arg1, arg2))
	{
	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
	  tem = value_logical_not (arg1);

	  arg2 = evaluate_subexp (NULL_TYPE, exp, pos,
				  (!tem ? EVAL_SKIP : noside));
	  return value_from_longest (LA_BOOL_TYPE,
			     (LONGEST) (!tem || !value_logical_not (arg2)));
	}

    case BINOP_EQUAL:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	{
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
#ifdef HP_IA64
	  if (is_dfp_arg (arg1, arg2))
	    tem = dec_value_compare (op, arg1, arg2);
	  else
#endif
	    tem = value_equal (arg1, arg2);
	  return value_from_longest (LA_BOOL_TYPE, (LONGEST) tem);
	}

    case BINOP_NOTEQUAL:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	{
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
#ifdef HP_IA64
	  if (is_dfp_arg (arg1, arg2))
	    tem = dec_value_compare (op, arg1, arg2);
	  else
#endif
	    tem = value_equal (arg1, arg2);
	  return value_from_longest (LA_BOOL_TYPE, (LONGEST) ! tem);
	}

    case BINOP_LESS:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	{
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
#ifdef HP_IA64
          if (is_dfp_arg (arg1, arg2))
            tem = dec_value_compare (op, arg1, arg2);
          else
#endif
            tem = value_less (arg1, arg2);
	  return value_from_longest (LA_BOOL_TYPE, (LONGEST) tem);
	}

    case BINOP_GTR:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	{
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
#ifdef HP_IA64
          if (is_dfp_arg (arg1, arg2))
            tem = dec_value_compare (op, arg1, arg2);
          else
#endif
	    tem = value_less (arg2, arg1);
	  return value_from_longest (LA_BOOL_TYPE, (LONGEST) tem);
	}

    case BINOP_GEQ:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	{
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
#ifdef HP_IA64
          if (is_dfp_arg (arg1, arg2))
            tem = dec_value_compare (op, arg1, arg2);
          else
#endif
	    tem = value_less (arg2, arg1) || value_equal (arg1, arg2);
	  return value_from_longest (LA_BOOL_TYPE, (LONGEST) tem);
	}

    case BINOP_LEQ:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (VALUE_TYPE (arg1), exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (binop_user_defined_p (op, arg1, arg2))
	{
	  return value_x_binop (arg1, arg2, op, OP_NULL, noside);
	}
      else
	{
#ifdef HP_IA64
          if (is_dfp_arg (arg1, arg2))
            tem = dec_value_compare (op, arg1, arg2);
          else
#endif
	    tem = value_less (arg1, arg2) || value_equal (arg1, arg2);
	  return value_from_longest (LA_BOOL_TYPE, (LONGEST) tem);
	}

    case BINOP_REPEAT:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      type = check_typedef (VALUE_TYPE (arg2));
      if (TYPE_CODE (type) != TYPE_CODE_INT)
	error ("Non-integral right operand for \"@\" operator.");
      if (noside == EVAL_AVOID_SIDE_EFFECTS)
	{
	  return allocate_repeat_value (VALUE_TYPE (arg1),
				     longest_to_int (value_as_long (arg2)));
	}
      else
	return value_repeat (arg1, longest_to_int (value_as_long (arg2)));

    case BINOP_COMMA:
      evaluate_subexp (NULL_TYPE, exp, pos, noside);
      return evaluate_subexp (NULL_TYPE, exp, pos, noside);

    case UNOP_NEG:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (unop_user_defined_p (op, arg1))
	return value_x_unop (arg1, op, noside);
      else
	return value_neg (arg1);

    case UNOP_COMPLEMENT:
      /* C++: check for and handle destructor names.  */
      op = exp->elts[*pos].opcode;

      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (unop_user_defined_p (UNOP_COMPLEMENT, arg1))
	return value_x_unop (arg1, UNOP_COMPLEMENT, noside);
      else
	return value_complement (arg1);

    case UNOP_LOGICAL_NOT:
      arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (unop_user_defined_p (op, arg1))
	return value_x_unop (arg1, op, noside);
      else
	return value_from_longest (LA_BOOL_TYPE,
				   (LONGEST) value_logical_not (arg1));

    case UNOP_IND:
      if (expect_type && TYPE_CODE (expect_type) == TYPE_CODE_PTR)
	expect_type = TYPE_TARGET_TYPE (check_typedef (expect_type));
      arg1 = evaluate_subexp (expect_type, exp, pos, noside);
      /* mithun 113005: JAGac40928, do not dereference a non-pointer int. */
      type = check_typedef (VALUE_TYPE (arg1));
      if (TYPE_CODE (type) == TYPE_CODE_INT)
         error ("Attempt to dereference a non-pointer value.");
      if ((TYPE_TARGET_TYPE (VALUE_TYPE (arg1))) &&
	  ((TYPE_CODE (TYPE_TARGET_TYPE (check_typedef (VALUE_TYPE (arg1)))) == TYPE_CODE_METHOD) ||
	   (TYPE_CODE (TYPE_TARGET_TYPE (check_typedef (VALUE_TYPE (arg1)))) == TYPE_CODE_MEMBER)))
	error ("Attempt to dereference pointer to member without an object");
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (unop_user_defined_p (op, arg1))
	return value_x_unop (arg1, op, noside);
      else if (noside == EVAL_AVOID_SIDE_EFFECTS)
	{
	  type = check_typedef (VALUE_TYPE (arg1));
	  if (TYPE_CODE (type) == TYPE_CODE_PTR
	      || TYPE_CODE (type) == TYPE_CODE_REF
	  /* In C you can dereference an array to get the 1st elt.  */
	      || TYPE_CODE (type) == TYPE_CODE_ARRAY
	    )
	    return value_zero (TYPE_TARGET_TYPE (type),
			       lval_memory);
	  else if (TYPE_CODE (type) == TYPE_CODE_INT)
	    /* GDB allows dereferencing an int.  */
	    return value_zero (builtin_type_int, lval_memory);
	  else
	    error ("Attempt to take contents of a non-pointer value.");
	}
      return value_ind (arg1);

    case UNOP_ADDR:
      /* C++: check for and handle pointer to members.  */

      op = exp->elts[*pos].opcode;

      if (noside == EVAL_SKIP)
	{
	  if (op == OP_SCOPE)
	    {
	      int temm = longest_to_int (exp->elts[pc + 3].longconst);
	      (*pos) += 3 + BYTES_TO_EXP_ELEM (temm + 1);
	    }
	  else
	    evaluate_subexp (NULL_TYPE, exp, pos, EVAL_SKIP);
	  goto nosideret;
	}
      else
	{
	  value_ptr retvalp = evaluate_subexp_for_address (exp, pos, noside);
	  /* If HP aCC object, use bias for pointers to members */
	  if (hp_som_som_object_present &&
	      (TYPE_CODE (VALUE_TYPE (retvalp)) == TYPE_CODE_PTR) &&
	      (TYPE_CODE (TYPE_TARGET_TYPE (VALUE_TYPE (retvalp))) == 
               TYPE_CODE_MEMBER))
	    {
	      /* forces evaluation */
              /* RM: experiment, changed int to CORE_ADDR */
              CORE_ADDR * ptr = (CORE_ADDR *)(void *) VALUE_CONTENTS (retvalp);
	      *ptr |= 0x20000000;	/* set 29th bit */
	    }

          /* Changes for JAGad01740. The value of 'code' in the struct 
             type is TYPE_CODE_ARRAY which has to be coerced into 
             TYPE_CODE_PTR before returning retvalp */
              if (TYPE_CODE (VALUE_TYPE (retvalp)) == TYPE_CODE_ARRAY)
                  COERCE_ARRAY(retvalp);

	  return retvalp;
	}

    case UNOP_SIZEOF:
      if (noside == EVAL_SKIP)
	{
	  evaluate_subexp (NULL_TYPE, exp, pos, EVAL_SKIP);
	  goto nosideret;
	}
      return evaluate_subexp_for_sizeof (exp, pos);

    case UNOP_CAST:
      (*pos) += 2;
      type = exp->elts[pc + 1].type;
      arg1 = evaluate_subexp (type, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (type != VALUE_TYPE (arg1))
	arg1 = value_cast (type, arg1);
      return arg1;

    case UNOP_MEMVAL:
      (*pos) += 2;
      /* If it has this type then it isn't something with one of the valid 
	 int sizes so print out a warning. If it had been a valid int size 
	 the type would have been e.g msym_data_symbol_type_int
       */
      if (exp->elts[pc + 1].type == msym_data_symbol_type)
	warning ("No debug info available - Trying to print as integer");
      else 
	complain (&print_as_int_complaint, 0);
      arg1 = evaluate_subexp (expect_type, exp, pos, noside);
      if (noside == EVAL_SKIP)
	goto nosideret;
      if (noside == EVAL_AVOID_SIDE_EFFECTS)
	return value_zero (exp->elts[pc + 1].type, lval_memory);
      else
	return value_at_lazy (exp->elts[pc + 1].type,
			      value_as_pointer (arg1),
			      NULL);

    case UNOP_PREINCREMENT:
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = true;
#endif
      arg1 = evaluate_subexp (expect_type, exp, pos, noside);
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = false;
#endif
      if (noside == EVAL_SKIP || noside == EVAL_AVOID_SIDE_EFFECTS)
	return arg1;
      else if (unop_user_defined_p (op, arg1))
	{
	  return value_x_unop (arg1, op, noside);
	}
      else
	{
	  arg2 = value_add (arg1, value_from_longest (builtin_type_char,
						      (LONGEST) 1));
	  return value_assign (arg1, arg2);
	}

    case UNOP_PREDECREMENT:
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = true;
#endif
      arg1 = evaluate_subexp (expect_type, exp, pos, noside);
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = false;
#endif
      if (noside == EVAL_SKIP || noside == EVAL_AVOID_SIDE_EFFECTS)
	return arg1;
      else if (unop_user_defined_p (op, arg1))
	{
	  return value_x_unop (arg1, op, noside);
	}
      else
	{
	  arg2 = value_sub (arg1, value_from_longest (builtin_type_char,
						      (LONGEST) 1));
	  return value_assign (arg1, arg2);
	}

    case UNOP_POSTINCREMENT:
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = true;
#endif
      arg1 = evaluate_subexp (expect_type, exp, pos, noside);
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = false;
#endif
      if (noside == EVAL_SKIP || noside == EVAL_AVOID_SIDE_EFFECTS)
	return arg1;
      else if (unop_user_defined_p (op, arg1))
	{
	  return value_x_unop (arg1, op, noside);
	}
      else
	{
	  arg2 = value_add (arg1, value_from_longest (builtin_type_char,
						      (LONGEST) 1));
	  value_assign (arg1, arg2);
	  return arg1;
	}

    case UNOP_POSTDECREMENT:
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = true;
#endif
      arg1 = evaluate_subexp (expect_type, exp, pos, noside);
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
      set_register = false;
#endif
      if (noside == EVAL_SKIP || noside == EVAL_AVOID_SIDE_EFFECTS)
	return arg1;
      else if (unop_user_defined_p (op, arg1))
	{
	  return value_x_unop (arg1, op, noside);
	}
      else
	{
	  arg2 = value_sub (arg1, value_from_longest (builtin_type_char,
						      (LONGEST) 1));
	  value_assign (arg1, arg2);
	  return arg1;
	}

    case OP_THIS:
      (*pos) += 1;
      return value_of_this (1);

    case OP_TYPE:
      error ("Attempt to use a type name as an expression");

    case OP_IMAGINARY_I_VAL:
      return value_of_imaginary_i(1);

    default:
      /* Removing this case and compiling with gcc -Wall reveals that
         a lot of cases are hitting this case.  Some of these should
         probably be removed from expression.h; others are legitimate
         expressions which are (apparently) not fully implemented.

         If there are any cases landing here which mean a user error,
         then they should be separate cases, with more descriptive
         error messages.  */

      error ("\
GDB does not (yet) know how to evaluate that kind of expression");
    }

nosideret:
  return value_from_longest (builtin_type_long, (LONGEST) 1);
}

/* Evaluate a subexpression of EXP, at index *POS,
   and return the address of that subexpression.
   Advance *POS over the subexpression.
   If the subexpression isn't an lvalue, get an error.
   NOSIDE may be EVAL_AVOID_SIDE_EFFECTS;
   then only the type of the result need be correct.  */

static value_ptr
evaluate_subexp_for_address ( register struct expression *exp,
                              register int *pos,
                              enum noside noside)
{
  enum exp_opcode op;
  register int pc;
  struct symbol *var;
  struct type *tmp_type = NULL;
  struct type *target_type = NULL;

  pc = (*pos);
  op = exp->elts[pc].opcode;

  switch (op)
    {
    case UNOP_IND:
      (*pos)++;
      return evaluate_subexp (NULL_TYPE, exp, pos, noside);

    case UNOP_MEMVAL:
      (*pos) += 3;
      return value_cast (lookup_pointer_type (exp->elts[pc + 1].type),
			 evaluate_subexp (NULL_TYPE, exp, pos, noside));

    case OP_VAR_VALUE:
      var = exp->elts[pc + 2].symbol;

      tmp_type = SYMBOL_TYPE (var);

      /* Error out if reference contains pointer type for
         command line calls. */
      if ((exp->elts[0].opcode == OP_FUNCALL) &&
          (TYPE_CODE(tmp_type) == TYPE_CODE_PTR))
         error ("Expression must have class type.");

      if (TYPE_TARGET_TYPE (tmp_type))
        target_type = TYPE_TARGET_TYPE (tmp_type);

     /* Error out if reference contains pointer type. */
      if ((TYPE_CODE (tmp_type) == TYPE_CODE_REF) && target_type &&
          (TYPE_CODE (target_type) == TYPE_CODE_PTR))
        error ("Expression must have class type.");

      /* C++: The "address" of a reference should yield the address
       * of the object pointed to. Let value_addr() deal with it. */
      if (TYPE_CODE (SYMBOL_TYPE (var)) == TYPE_CODE_REF)
	goto default_case;

      (*pos) += 4;
      if (noside == EVAL_AVOID_SIDE_EFFECTS)
	{
	  struct type *type =
	  lookup_pointer_type (SYMBOL_TYPE (var));
	  enum address_class sym_class = SYMBOL_CLASS (var);

	  if (sym_class == LOC_CONST
	      || sym_class == LOC_CONST_BYTES
	      || sym_class == LOC_REGISTER
	      || sym_class == LOC_REGPARM)
	    error ("Attempt to take address of register or constant.");

	  return
	    value_zero (type, not_lval);
	}
      else
	return
	  locate_var_value
	  (var,
	   block_innermost_frame (exp->elts[pc + 1].block));
     
     /* JAGag16297:Inconsistencies in expression evaluation */
     case OP_LONG:
      if (noside == EVAL_NORMAL)
	{
	  value_ptr x = evaluate_subexp (NULL_TYPE, exp, pos, noside);
	  if ((VALUE_LVAL (x) == lval_register 
	      && TYPE_CODE (VALUE_TYPE (x)) == TYPE_CODE_REF) 
                  || (VALUE_LVAL (x) == lval_memory))
	      return value_zero (VALUE_TYPE(x), VALUE_LVAL (x));
	  else
	    error ("Attempt to take address of non-lval");
	}
    
    default:
    default_case:
      if (noside == EVAL_AVOID_SIDE_EFFECTS)
	{
	  value_ptr x = evaluate_subexp (NULL_TYPE, exp, pos, noside);
          
          /* JAGaf14767 - For both the cases - value in register or 
             value in memory do the same! */

	  if ((VALUE_LVAL (x) == lval_register 
	      && TYPE_CODE (VALUE_TYPE (x)) == TYPE_CODE_REF) 
                  || (VALUE_LVAL (x) == lval_memory))
	      return value_zero (VALUE_TYPE(x), VALUE_LVAL (x));
	  else
	    error ("Attempt to take address of non-lval");
	}
      return value_addr (evaluate_subexp (NULL_TYPE, exp, pos, noside));
    }
}

/* Evaluate like `evaluate_subexp' except coercing arrays to pointers.
   When used in contexts where arrays will be coerced anyway, this is
   equivalent to `evaluate_subexp' but much faster because it avoids
   actually fetching array contents (perhaps obsolete now that we have
   VALUE_LAZY).

   Note that we currently only do the coercion for C expressions, where
   arrays are zero based and the coercion is correct.  For other languages,
   with nonzero based arrays, coercion loses.  Use CAST_IS_CONVERSION
   to decide if coercion is appropriate.

 */

value_ptr
evaluate_subexp_with_coercion ( register struct expression *exp,
                                register int *pos,
                                enum noside noside)
{
  register enum exp_opcode op;
  register int pc;
  register value_ptr val;
  struct symbol *var;

  pc = (*pos);
  op = exp->elts[pc].opcode;

  switch (op)
    {
    case OP_VAR_VALUE:
      var = exp->elts[pc + 2].symbol;
      if (TYPE_CODE (check_typedef (SYMBOL_TYPE (var))) == TYPE_CODE_ARRAY
	  && CAST_IS_CONVERSION)
	{
	  (*pos) += 4;
	  val =
	    locate_var_value
	    (var, block_innermost_frame (exp->elts[pc + 1].block));
          return value_cast (lookup_pointer_type (TYPE_TARGET_TYPE (check_typedef (SYMBOL_TYPE (var)))),
			     val);
	}
      /* FALLTHROUGH */

    default:
      return evaluate_subexp (NULL_TYPE, exp, pos, noside);
    }
}

/* Evaluate a subexpression of EXP, at index *POS,
   and return a value for the size of that subexpression.
   Advance *POS over the subexpression.  */

static value_ptr
evaluate_subexp_for_sizeof ( register struct expression *exp,
                             register int *pos)
{
  enum exp_opcode op;
  register int pc;
  struct type *type;
  value_ptr val;

  pc = (*pos);
  op = exp->elts[pc].opcode;

  switch (op)
    {
      /* This case is handled specially
         so that we avoid creating a value for the result type.
         If the result type is very big, it's desirable not to
         create a value unnecessarily.  */
    case UNOP_IND:
      (*pos)++;
      val = evaluate_subexp (NULL_TYPE, exp, pos, EVAL_AVOID_SIDE_EFFECTS);
      type = check_typedef (VALUE_TYPE (val));
      if (TYPE_CODE (type) != TYPE_CODE_PTR
	  && TYPE_CODE (type) != TYPE_CODE_REF
	  && TYPE_CODE (type) != TYPE_CODE_ARRAY)
	error ("Attempt to take contents of a non-pointer value.");
      type = check_typedef (TYPE_TARGET_TYPE (type));
      return value_from_longest (builtin_type_int, (LONGEST)
				 TYPE_LENGTH (type));

    case UNOP_MEMVAL:
      (*pos) += 3;
      type = check_typedef (exp->elts[pc + 1].type);
      return value_from_longest (builtin_type_int,
				 (LONGEST) TYPE_LENGTH (type));

    case OP_VAR_VALUE:
      (*pos) += 4;
      type = check_typedef (SYMBOL_TYPE (exp->elts[pc + 2].symbol));
      if (TYPE_CODE (type) == TYPE_CODE_REF)
        {
          struct type *elttype;
          elttype = check_typedef (TYPE_TARGET_TYPE (type));
          if (TYPE_CODE (elttype) == TYPE_CODE_ARRAY &&
	      TYPE_ARRAY_UPPER_BOUND_TYPE (elttype) &&
              TYPE_ARRAY_UPPER_BOUND_TYPE (type) != BOUND_CANNOT_BE_DETERMINED)
            f77_get_dynamic_length_of_aggregate (elttype);
          return
            value_from_longest (builtin_type_int, (LONGEST) TYPE_LENGTH (elttype));
        }
      return
	value_from_longest (builtin_type_int, (LONGEST) TYPE_LENGTH (type));

    default:
      val = evaluate_subexp (NULL_TYPE, exp, pos, EVAL_AVOID_SIDE_EFFECTS);
      return value_from_longest (builtin_type_int,
				 (LONGEST) TYPE_LENGTH (VALUE_TYPE (val)));
    }
}

/* Evaluate a fortran intrinsic */

static value_ptr
evaluate_subexp_for_ftn_intrinsic ( value_ptr sval,
                                    int nargs,
                                    struct expression *exp,
                                    int *pos,
                                    enum noside noside)
{
  int slen;
  char *string;
  struct symbol *sym;
  enum exp_opcode op = OP_NULL;
  int i;
  extern const struct op_print f_op_print_tab[];
  value_ptr arg1, arg2;
  value_ptr val = NULL;

  slen = TYPE_LENGTH (check_typedef (VALUE_TYPE (sval)));
  string = (char *) alloca (slen + 1);
  memcpy (string, VALUE_CONTENTS (sval), slen);
  string[slen] = 0;
  if (case_sensitivity == case_sensitive_off)
    for (i = 0; i < slen; i++)
      string[i] = tolower (string[i]);

  /* Find the intrinsic */ 
 
  for (i=0; f_op_print_tab[i].string != NULL; i++)  
    {
      if (f_op_print_tab[i].precedence == PREC_BUILTIN_FUNCTION  &&
          slen == strlen (f_op_print_tab[i].string) &&
          strcmp (string, f_op_print_tab[i].string) == 0)
        {
          op = f_op_print_tab[i].opcode;
          break;
        }
    }

  if (op == OP_NULL)
    {
      error ("Unrecognized Fortran intrinsic \"%s\"", string);
      return NULL;
    }

  /* Verify that the correct number of arguments are passed */
  if (!(nargs == 1 ||
       ((op == BINOP_FTN_MOD || op == BINOP_FTN_AMOD || op == BINOP_FTN_DMOD)
        && nargs == 2)))
    error ("Wrong number of arguments");

  if (op ==  UNOP_FTN_LOC && noside != EVAL_SKIP)
    return evaluate_subexp_for_address (exp, pos, noside);

  arg1 = evaluate_subexp (NULL_TYPE, exp, pos, noside);

  if (noside == EVAL_SKIP)
    return value_from_longest (builtin_type_long, (LONGEST) 1);

  switch (op)
    {
    case UNOP_FTN_ABS:  /* absolute value */
    case UNOP_FTN_DABS: /* absolute value for real*8 argument */
    case UNOP_FTN_IABS: /* absolute value for integer*4 argument */
      val =  value_abs (arg1, op, noside);
      break;

    case UNOP_FTN_ANINT: /* nearest whole number */
    case UNOP_FTN_DNINT: /* nearest whole number for real*8 */
      val = value_round (arg1, op, noside);
      break;

    case UNOP_FTN_EXP: /* exponential */
      val = value_exp (arg1, noside);
      break;

    case UNOP_FTN_IMAG:  /* imaginary part of complex */
    case UNOP_FTN_AIMAG: /* imaginary part of complex*8 */
      val = value_imaginary (arg1, op, noside);
      break;

    case BINOP_FTN_MOD:  /* remainder */
    case BINOP_FTN_AMOD: /* remainder for real*4 argument */
    case BINOP_FTN_DMOD: /* remainder for real*8 arguments*/
      arg2 = evaluate_subexp (NULL_TYPE, exp, pos, noside);
      val =  value_remainder (arg1, arg2, op, noside);
      break;

    case UNOP_FTN_NINT:   /* nearest integer */
    case UNOP_FTN_IDNINT: /* nearest integer for real*8 */
      val = value_round (arg1, op, noside);
      break;

    case UNOP_FTN_AINT:  /* truncation */
    case UNOP_FTN_DINT:  /* truncation for real*8 */
    case UNOP_FTN_CHAR:  /* convert integer to character */
    case UNOP_FTN_CMPLX: /* convert numeric to complex */
    case UNOP_FTN_DBLE:  /* convert numeric to real*8 */
    case UNOP_FTN_FLOAT: /* convert integer to real*4 */
    case UNOP_FTN_ICHAR: /* convert character to integer */
    case UNOP_FTN_IDINT: /* convert real*8 to integer */
    case UNOP_FTN_IFIX:  /* convert real to integer */
    case UNOP_FTN_INT:   /* convert numeric to integer */
    case UNOP_FTN_REAL:  /* convert numeric to real */
    case UNOP_FTN_SNGL:  /* convert real*8 to integer */
      val = value_ftn_cast (arg1, op, noside);
      break;
    
    default:
      error ("Unimplemented Fortran intrinsic \"%s\"", string);
    }

  return val;
}

/* Parse a type expression in the string [P..P+LENGTH). */

struct type *
parse_and_eval_type ( char *p, int length)
{
  char *tmp = (char *) alloca (length + 4);
  struct expression *expr;
  tmp[0] = '(';
  memcpy (tmp + 1, p, length);
  tmp[length + 1] = ')';
  tmp[length + 2] = '0';
  tmp[length + 3] = '\0';
  expr = parse_expression (tmp);
  if (expr->elts[0].opcode != UNOP_CAST)
    error ("Internal error in eval_type.");
  return expr->elts[1].type;
}

int
calc_f77_array_dims ( struct type *array_type)
{
  int ndimen = 1;
  struct type *tmp_type;

  if ((TYPE_CODE (array_type) != TYPE_CODE_ARRAY))
    error ("Can't get dimensions for a non-array type");

  tmp_type = array_type;

  while ((tmp_type = TYPE_TARGET_TYPE (tmp_type)))
    {
      if (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY)
	++ndimen;
    }
  return ndimen;
}

/* Construct slice vector - a recursive function to
   evaluate the indices of the array elements
   within the array slice

   nargs - Number of args in the array, I/P
   low_bound - subsript array, I/P
   high_bound - range array, I/P
   stride_array - stride array, I/P
   slice_vector - slice vector, O/P
*/
   
static void 
construct_slice_vector ( int nargs,
                         LONGEST *low_bound,
                         LONGEST *high_bound,
                         LONGEST *stride_array,
                         slicevector *slice_vector,
                         int index)
{
  LONGEST hi_val, lo_val, stride;
  int cnt = 1,i,j;

  lo_val = low_bound[nargs];
  stride = stride_array[nargs];

  /* if stride is -ve, than the subscript (lo_val) will be > range (hi_val) */
  if (stride < 0)
    cnt = (int)((lo_val - high_bound[nargs])/(stride * -1)) + 1;
  else if (high_bound[nargs] > lo_val)
    cnt = (int)((high_bound[nargs] - lo_val)/stride) + 1;

  switch (nargs)
    {
    case 0:
      break;
    case 1:
      {
        for (i=1;i<=cnt;i++)
	  {
	    FTN_SLICE_VECTOR(slice_vector, index, nargs) = lo_val;
	    index++;
	    lo_val += stride;
	  }
        break;
      }
    default:
      {
        for (i=1; i<=cnt;i++)
    	  {   
            int elms = 1;
            for (j=1; j<nargs; j++)
	      {
                if (stride_array[j] < 0)
                  elms *= (low_bound[j] - high_bound[j])/(stride_array[j] * -1) + 1;
                else if (high_bound[j] > low_bound[j])
                  elms *= (high_bound[j] - low_bound[j])/stride_array[j] + 1;
	      }

            for (j=1; j<=elms; j++)
	      {
	        FTN_SLICE_VECTOR(slice_vector, index, nargs) = lo_val;
	        index++;
	      }

            construct_slice_vector (nargs-1, low_bound, high_bound, stride_array, slice_vector, index-elms);

            lo_val += stride;
    	  }   
      }
    }
}
