#ifndef _EXEC_PATH_H
#define _EXEC_PATH_H

#include "defs.h"

/* Defined by the compiler. This value should not change. */
#define GEPT_BUFFER_FIX_SIZE (65536)
/* Magic numbers for different tables */
#define _CPR_LEPT_MAGIC 	 0x10ca1ba747ab1e
#define _CPR_GEPT_MAGIC         0x910ba1ba747ab1e
#define _CPR_FIXSIZE_GEPT_MAGIC 0xf910ba1ba747ab1e

/* The following definitions are copied from 
   /CLO/Components/SYZYGY/Src/rtc_aux/rtc_cpr.h
   They are here for our reference to make code easier to read

   typedef void* PathInfo;

   typedef struct _CPR_GEPT {
     UInt64      magic;              _CPR_GEPT_MAGIC
     UInt32      buffer_length;      the length of the circular_buf
     UInt32      version;            the version number of the GEPT
     PathInfo    **current_pos_ptr;  pointer to current_pos variable
     PathInfo    *circular_buf;      the buffer is allocated having
     buffer_length items at runtime
     pthread_t   thread_no;          the thread number of the table
     FILE        *file;              file to write
     UInt32      format:3;           the format of data wrote to file
     UInt32      buffer_reset:1;     1 if the buffer is wrapped around
   } _CPR_GEPT;

   the fix sized global path table has no way to initailize with thread local
   object addresses, and it cannot get the thread number for the table, the
   analysis tool to need find out the thread number with pthread calls (may
   be need to ask pthread team to provide some thing like
      pthread_t pthread_mapping_address_to_thread_number(void *addr);

   typedef struct _CPR_FIXSZ_GEPT {
     UInt64      magic;         _CPR_FIXSIZE_GEPT_MAGIC
     UInt16      current_idx;   the index to the circular_buf for next PathInfo
     UInt16      padding;
     UInt32      version;       version number of the GEPT
     PathInfo    circular_buf[GEPT_BUFFER_FIX_SIZE]; 
   } _CPR_FIXSZ_GEPT;

   typedef struct _CPR_LEPT {
     UInt64 magic;              _CPR_LEPT_MAGIC 
     UInt64 buffer_length;
     PathInfo *path_buf; 
   } _CPR_LEPT;
*/

/* Size of the pointer in the inferior. */
#ifdef HP_IA64
#define PTR_SIZE (is_swizzled ? 4 : 8)
#else
#define PTR_SIZE (sizeof (CORE_ADDR))
#endif /* !HP_IA64 */



#define MAGIC_OFF 0
#define GEPT_BUFLEN_OFF 8
#define GEPT_CURPOS_OFF 16
#define GEPT_BUFPTR_OFF (GEPT_CURPOS_OFF + PTR_SIZE)
#define FSZ_GEPT_CURPOS_OFF 8
#define FSZ_GEPT_BUF_OFF 16
#define LEPT_BUFLEN_OFF 8
#define LEPT_BUFPTR_OFF 16

#define SCAN_BLOCK_SZ 1024

struct exec_path_tab
{
  int is_global;        /* is this a global table */
  unsigned int end_idx; /* end marker for global circular buffer */
  unsigned int cur_pos; /* currently selected entry in this table */
  unsigned int buffer_length; /* length of the path_buf */
  char* path_buf;        /* array of pc values */
};


extern void invalidate_current_ept (void);
extern void invalidate_global_epts (void);
extern struct exec_path_tab* get_current_ept (void);
extern void set_current_ept (struct exec_path_tab* ept);
extern void ept_free (struct exec_path_tab* ept);

extern struct thread_info* find_thread_pid (int pid); /* from thread.c */
extern void* frame_obstack_alloc (unsigned long size); /* blockframe.c */
extern struct thread_info* get_thread_list (void); /* threads.c */
extern struct frame_info* parse_frame_specification (char *frame_exp);
extern int print_insn (CORE_ADDR memaddr, struct ui_file *stream);

/* from ia64-tdep.c */
extern long long slot0_contents (unsigned char *bundle);
extern long long slot1_contents (unsigned char *bundle);
extern long long slot2_contents (unsigned char *bundle);
extern boolean is_mov_ip (long long slot);

/* used for printing ept detail. */
#define INCR_PC_BUNDLE(PC) \
PC += (PC % BUNDLE_SIZE) == 2 ? BUNDLE_SIZE - 2 : 1;

enum ept_detail {
  STOP,
  STEP_INTO,
  CONTINUE
};
#endif /* _EXEC_PATH_H */
