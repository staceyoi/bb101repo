/* Work with executable files, for GDB. 
   Copyright 1988, 1989, 1991, 1992, 1993, 1994, 1997, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#define _LARGEFILE64_SOURCE
#include <fcntl.h>      /* for open64(2) prototype */

#include "defs.h"
#include "frame.h"
#include "inferior.h"
#include "target.h"
#include "gdbcmd.h"
#include "language.h"
#include "symfile.h"
#include "objfiles.h"
#include "gdbtypes.h"
#ifdef HP_IA64
#include "hp-ia64-solib.h"
#include "solist.h"
#endif
#include <stdlib.h>

#ifdef HP_IA64
#include "elf.h"
#include "elf_hp.h"
#include <sys/utsname.h>
#endif

#ifdef USG
#include <sys/types.h>
#endif

#include <fcntl.h>
#include "gdb_string.h"

#include "gdbcore.h"

#include <ctype.h>
#include "gdb_stat.h"

#ifdef LOG_BETA_LOAD_TIME
#ifdef HAVE_TIME_H
#include <time.h>
#endif
#endif

#ifndef O_BINARY
#define O_BINARY 0
#endif

#include "xcoffsolib.h"

#ifdef HP_IA64
/* By default, 32 bit since compiler default id 32 bit. */
boolean is_swizzled = TRUE;
#else
boolean is_swizzled = FALSE;
#endif

extern int debugging_aries, debugging_aries64;

#ifdef HP_IA64
/* QXCR1000579474: Aries core file debugging. */
extern CORE_ADDR aries_linktime_unwind_start;
extern CORE_ADDR aries_runtime_unwind_start;
extern CORE_ADDR aries_runtime_text_start;
extern boolean aries_core_debug;

/* For getting type info of core file */
struct types_core
{
  int type_val;
  char * type_name;
} core_types[] =
{
	{ PT_HP_CORE_NONE,	"PT_HP_CORE_NONE" },
	{ PT_HP_CORE_VERSION,	"PT_HP_CORE_VERSION" },
	{ PT_HP_CORE_KERNEL,	"PT_HP_CORE_KERNEL" },
	{ PT_HP_CORE_COMM,	"PT_HP_CORE_COMM" },
	{ PT_HP_CORE_PROC,	"PT_HP_CORE_PROC" },
	{ PT_HP_CORE_LOADABLE,	"PT_HP_CORE_LOADABLE" },
	{ PT_HP_CORE_STACK,	"PT_HP_CORE_STACK" },
	{ PT_HP_CORE_SHM,	"PT_HP_CORE_SHM" },
	{ PT_HP_CORE_MMF,	"PT_HP_CORE_MMF" },
	{ 0,			NULL },
};

static char * coretype_name (unsigned long);
static void print_heap_corefile (void);
#endif

struct vmap *map_vmap (bfd *, bfd *);
CORE_ADDR cached_pc;
void *cached_minsym;

#ifdef HP_IA64_NATIVE
/* Remove when sendsig is dumped in the core file */
extern void *ia64_UNW_read_local_mem_kernel_pages PARAMS 
	((void *dst, const uint64_t src, size_t length, uint32_t ident));
extern int address_in_user_sendsig_unw PARAMS ((CORE_ADDR address));

/* Read from RSE if the address is in RSE range. */
extern int address_in_rsebs PARAMS ((CORE_ADDR address, int len));
extern int read_inferior_ideal_memory PARAMS ((CORE_ADDR, char *, int));
extern void *ia64_read_local_mem_kernel_pages ( void *, const uint64_t , size_t );
#endif

void (*file_changed_hook) (char *);

#ifdef HP_XMODE
/* External functions defined in top.c to check the executable format
   of the program to exec and to launch a different gdb if necessary. */
extern int xmode_exec_format_is_different (char *executable);
extern void xmode_launch_other_gdb (char *new_file, enum xmode_cause reason);
#endif

/* Prototypes for local functions */

static void add_to_section_table (bfd *, sec_ptr, PTR);

/* JAGaf54650: This function needs to be used in infrun.c.
   So making it as non-static function */
void exec_close (int);

void file_command (char *, int);

static void set_section_command (char *, int);

static void exec_files_info (struct target_ops *);

static void bfdsec_to_vmap (bfd *, sec_ptr, PTR);

static int ignore (CORE_ADDR, char *);

static void init_exec_ops (void);

void _initialize_exec (void);

extern int info_verbose;

extern int is_11_23_or_greater;

/* The target vector for executable files.  */

struct target_ops exec_ops;

/* The Binary File Descriptor handle for the executable file.  */

bfd *exec_bfd = NULL;

/* Whether to open exec and core files read-only or read-write.  */

int write_files = 0;

/* Text start and end addresses (KLUDGE) if needed */

#ifndef NEED_TEXT_START_END
#define NEED_TEXT_START_END (0)
#endif
CORE_ADDR text_start = 0;
CORE_ADDR text_end = 0;

struct vmap *vmap;

/* ARGSUSED */

/* JAGaf54650 */
void
exec_close (int quitting)
{
  struct vmap *vp, *nxt;

  for (nxt = vmap; nxt != NULL;)
    {
      vp = nxt;
      nxt = vp->nxt;

      /* if there is an objfile associated with this bfd,
         free_objfile() will do proper cleanup of objfile *and* bfd. */

      if (vp->objfile)
	{
	  free_objfile (vp->objfile);
	}
      else if (vp->bfd != exec_bfd)
	/* FIXME-leak: We should be freeing vp->name too, I think.  */
	if (!bfd_close (vp->bfd))
	  warning ("cannot close \"%s\": %s",
		   vp->name, bfd_errmsg (bfd_get_error ()));

      /* FIXME: This routine is #if 0'd in symfile.c.  What should we
         be doing here?  Should we just free everything in
         vp->objfile->symtabs?  Should free_objfile do that?
         FIXME-as-well: free_objfile already free'd vp->name, so it isn't
         valid here.  */
      free_named_symtabs (vp->name);
      free (vp);
    }

  vmap = NULL;

  if (exec_bfd)
    {
      char *name = bfd_get_filename (exec_bfd);

      if (!bfd_close (exec_bfd))
	warning ("cannot close \"%s\": %s",
		 name, bfd_errmsg (bfd_get_error ()));
      free (name);
      exec_bfd = NULL;
    }

  if (exec_ops.to_sections)
    {
      free ((PTR) exec_ops.to_sections);
      exec_ops.to_sections = NULL;
      exec_ops.to_sections_end = NULL;
    }
}

/*  Process the first arg in ARGS as the new exec file.

   This function is intended to be behave essentially the same
   as exec_file_command, except that the latter will detect when
   a target is being debugged, and will ask the user whether it
   should be shut down first.  (If the answer is "no", then the
   new file is ignored.)

   This file is used by exec_file_command, to do the work of opening
   and processing the exec file after any prompting has happened.

   And, it is used by child_attach, when the attach command was
   given a pid but not a exec pathname, and the attach command could
   figure out the pathname from the pid.  (In this case, we shouldn't
   ask the user whether the current target should be shut down --
   we're supplying the exec pathname late for good reason.) */

void
exec_file_attach (char *args, int from_tty)
{
  char **argv;
  char *filename;

#ifdef HP_IA64
  is_swizzled = TRUE;  /* set in case of early exit */
#else
  is_swizzled = FALSE;  /* set in case of early exit */
#endif

  /* Remove any previous exec file.  */
  unpush_target (&exec_ops);
  /* Remove any previous core file. */
  unpush_target (&core_ops);

  if (debugging_aries)
    args = "/usr/lib/hpux32/pa_boot32.so";    
  else if (debugging_aries64)
    args = "/usr/lib/hpux64/pa_boot64.so";

  cached_pc = 0;
  cached_minsym = 0;

  /* Now open and digest the file the user requested, if any.  */

  if (args)
    {
      char *scratch_pathname;
      int scratch_chan;

      /* Scan through the args and pick up the first non option arg
         as the filename.  */

      argv = buildargv (args);
      if (argv == NULL)
	nomem (0);

      make_cleanup_freeargv (argv);

      for (; (*argv != NULL) && (**argv == '-'); argv++)
	{;
	}
      if (*argv == NULL)
	error ("No executable file name was specified");

      filename = tilde_expand (*argv);
      make_cleanup (free, filename);

      scratch_chan = openp (getenv ("PATH"), 1, filename,
		   write_files ? O_RDWR | O_BINARY : O_RDONLY | O_BINARY, 0,
			    &scratch_pathname);

#ifdef HP_XMODE
      /* For cross mode support between 32-bit and 64-bit applications.
         If the executable to attach is different from what this
         gdb understands, launch the gdb that will understand
         this program. */
      if (scratch_pathname != NULL) {
        if (xmode_exec_format_is_different (scratch_pathname)) 
          {
            xmode_launch_other_gdb (scratch_pathname, xmode_executable);
          }
      }
#endif

#if defined(__GO32__) || defined(_WIN32) || defined(__CYGWIN__)
      if (scratch_chan < 0)
	{
	  char *exename = alloca (strlen (filename) + 5);
	  strcat (strcpy (exename, filename), ".exe");
	  scratch_chan = openp (getenv ("PATH"), 1, exename, write_files ?
	     O_RDWR | O_BINARY : O_RDONLY | O_BINARY, 0, &scratch_pathname);
	}
#endif
      if (scratch_chan < 0)
	perror_with_name (filename);

#ifdef SEEK_64_BITS
      close (scratch_chan);
      scratch_chan = open64 (scratch_pathname, O_RDONLY);
#endif

      exec_bfd = bfd_fdopenr (scratch_pathname, gnutarget, scratch_chan);

      if (!exec_bfd)
	error ("\"%s\": could not open as an executable file: %s",
	       scratch_pathname, bfd_errmsg (bfd_get_error ()));

      /* At this point, scratch_pathname and exec_bfd->name both point to the
         same malloc'd string.  However exec_close() will attempt to free it
         via the exec_bfd->name pointer, so we need to make another copy and
         leave exec_bfd as the new owner of the original copy. */
      scratch_pathname = xstrdup (scratch_pathname);
      make_cleanup (free, scratch_pathname);

      if (!bfd_check_format (exec_bfd, bfd_object))
	{
	  /* Make sure to close exec_bfd, or else "run" might try to use
	     it.  */
	  exec_close (0);
	  error ("\"%s\": not in executable format: %s",
		 scratch_pathname, bfd_errmsg (bfd_get_error ()));
	}

      if (! is_11_23_or_greater && IS_TARGET_LRE)
	{
	  exec_close (0);
	  error ("Linux Runtime Environment debugging is not allowed prior to 11.23");
	}

#ifdef BFD_IS_SWIZZLED
      is_swizzled = BFD_IS_SWIZZLED(exec_bfd);

      if (is_swizzled) 
        {
          /* is_sizzld -> ILP32 */
          /* long and unsigned long are 32-bit */
          TYPE_LENGTH (builtin_type_long) = 4;
          TYPE_LENGTH (builtin_type_unsigned_long) = 4;
        }
      else
        {
          /* !is_sizzld -> ILP64 */
          /* long and unsigned long are 64-bit */
          /* reset values when a 64-bit file is exec'ed after a 32-bit file */
          TYPE_LENGTH (builtin_type_long) 
            = TARGET_LONG_BIT / TARGET_CHAR_BIT;
          TYPE_LENGTH (builtin_type_unsigned_long) 
            = TARGET_LONG_BIT / TARGET_CHAR_BIT;
        }

#endif

      /* FIXME - This should only be run for RS6000, but the ifdef is a poor
         way to accomplish.  */
#ifdef IBM6000_TARGET
      /* Setup initial vmap. */

      map_vmap (exec_bfd, 0);
      if (vmap == NULL)
	{
	  /* Make sure to close exec_bfd, or else "run" might try to use
	     it.  */
	  exec_close (0);
	  error ("\"%s\": can't find the file sections: %s",
		 scratch_pathname, bfd_errmsg (bfd_get_error ()));
	}
#endif /* IBM6000_TARGET */

      if (build_section_table (exec_bfd, &exec_ops.to_sections,
			       &exec_ops.to_sections_end))
	{
	  /* Make sure to close exec_bfd, or else "run" might try to use
	     it.  */
	  exec_close (0);
	  error ("\"%s\": can't find the file sections: %s",
		 scratch_pathname, bfd_errmsg (bfd_get_error ()));
	}

      /* text_end is sometimes used for where to put call dummies.  A
         few ports use these for other purposes too.  */
      if (NEED_TEXT_START_END)
	{
	  struct section_table *p;

	  /* Set text_start to the lowest address of the start of any
	     readonly code section and set text_end to the highest
	     address of the end of any readonly code section.  */
	  /* FIXME: The comment above does not match the code.  The
	     code checks for sections with are either code *or*
	     readonly.  */
	  text_start = ~(CORE_ADDR) 0;
	  text_end = (CORE_ADDR) 0;
	  for (p = exec_ops.to_sections; p < exec_ops.to_sections_end; p++)
	    if (bfd_get_section_flags (p->bfd, p->the_bfd_section)
		& (SEC_CODE | SEC_READONLY))
	      {
		if (text_start > p->addr)
		  text_start = p->addr;
		if (text_end < p->endaddr)
		  text_end = p->endaddr;
	      }
	}

      validate_files ();

      set_gdbarch_from_file (exec_bfd);

      push_target (&exec_ops);

      /* Tell display code (if any) about the changed file name.  */
      if (exec_file_display_hook)
	(*exec_file_display_hook) (filename);
    }
  else if (from_tty)
    {
      printf_unfiltered ("No executable file now.\n");
      if (core_bfd)
        core_detach (0, from_tty);
    }
}

/*  Process the first arg in ARGS as the new exec file.

   Note that we have to explicitly ignore additional args, since we can
   be called from file_command(), which also calls symbol_file_command()
   which can take multiple args. */

void
exec_file_command (char *args, int from_tty)
{
  target_preopen (from_tty);
  exec_file_attach (args, from_tty);
}

/* Set both the exec file and the symbol file, in one command.  
   What a novelty.  Why did GDB go through four major releases before this
   command was added?  */

void
file_command (char *arg, int from_tty)
{
#ifdef LOG_BETA_LOAD_TIME
  time_t start_time, elapsed_time;

  start_time = time (NULL);
#endif

  /* FIXME, if we lose on reading the symbol file, we should revert
     the exec file, but that's rough.  */
  exec_file_command (arg, from_tty);
  symbol_file_command (arg, from_tty);

#ifdef LOG_BETA_LOAD_TIME
  elapsed_time = time (NULL) - start_time;
  if (elapsed_time > 5)
    log_test_event (LOG_BETA_LOAD_TIME, (int) elapsed_time);
#endif

  if (file_changed_hook)
    file_changed_hook (arg);
}


/* Locate all mappable sections of a BFD file. 
   table_pp_char is a char * to get it through bfd_map_over_sections;
   we cast it back to its proper type.  */

static void
add_to_section_table (bfd *abfd,
                      sec_ptr asect,
                      PTR table_pp_char)
{
  struct section_table **table_pp = (struct section_table **) table_pp_char;
  flagword aflag;

  aflag = bfd_get_section_flags (abfd, asect);
  if (!(aflag & SEC_ALLOC))
    return;
  if (0 == bfd_section_size (abfd, asect))
    return;
  (*table_pp)->bfd = abfd;
  (*table_pp)->the_bfd_section = asect;
  (*table_pp)->addr = SWIZZLE (bfd_section_vma (abfd, asect));
  (*table_pp)->endaddr =
      SWIZZLE ((*table_pp)->addr + bfd_section_size (abfd, asect));
#ifdef HP_IA64
  if (is_swizzled) {
      if (((*table_pp)->endaddr & 0xffffffff) == 0) {
	  /* At the end of the 32-bit address space, the endaddr might be 
	   * beyond the end of the address space.  An address that cannot 
	   * be gotten by swizzle.  It is size bytes beyond the start.  
	   * Last 32-bits are zero.  */
	  (*table_pp)->endaddr = 
	    (*table_pp)->addr + bfd_section_size (abfd, asect);
      }
    }
#endif
  (*table_pp)++;
}

/* Builds a section table, given args BFD, SECTABLE_PTR, SECEND_PTR.
   Returns 0 if OK, 1 on error.  */

int
build_section_table (bfd *some_bfd,
                     struct section_table **start,
                     struct section_table **end)
{
  unsigned count;

  count = bfd_count_sections (some_bfd);
  if (*start)
    free ((PTR) * start);
  *start = (struct section_table *) xmalloc (count * sizeof (struct section_table));
  *end = *start;
  bfd_map_over_sections (some_bfd, add_to_section_table, (char *) end);
  if (*end > *start + count)
    abort ();
  /* We could realloc the table, but it probably loses for most files.  */
  return 0;
}

static void
bfdsec_to_vmap (bfd *abfd,
                sec_ptr sect,
                PTR arg3)
{
  struct vmap_and_bfd *vmap_bfd = (struct vmap_and_bfd *) arg3;
  struct vmap *vp;

  vp = vmap_bfd->pvmap;

  if ((bfd_get_section_flags (abfd, sect) & SEC_LOAD) == 0)
    return;

  if (STREQ (bfd_section_name (abfd, sect), ".text"))
    {
      vp->tstart = bfd_section_vma (abfd, sect);
      vp->tend = SWIZZLE (vp->tstart + bfd_section_size (abfd, sect));
      vp->tvma = bfd_section_vma (abfd, sect);
      vp->toffs = sect->filepos;
    }
  else if (STREQ (bfd_section_name (abfd, sect), ".data"))
    {
      vp->dstart = bfd_section_vma (abfd, sect);
      vp->dend = SWIZZLE (vp->dstart + bfd_section_size (abfd, sect));
      vp->dvma = bfd_section_vma (abfd, sect);
    }
  /* Silently ignore other types of sections. (FIXME?)  */
}

/* Make a vmap for ABFD which might be a member of the archive ARCH.
   Return the new vmap.  */

struct vmap *
map_vmap (bfd *abfd,
          bfd *arch)
{
  struct vmap_and_bfd vmap_bfd;
  struct vmap *vp, **vpp;

  vp = (struct vmap *) xmalloc (sizeof (*vp));
  memset ((char *) vp, '\0', sizeof (*vp));
  vp->nxt = 0;
  vp->bfd = abfd;
  vp->name = bfd_get_filename (arch ? arch : abfd);
  vp->member = arch ? bfd_get_filename (abfd) : "";

  vmap_bfd.pbfd = arch;
  vmap_bfd.pvmap = vp;
  bfd_map_over_sections (abfd, bfdsec_to_vmap, &vmap_bfd);

  /* Find the end of the list and append. */
  for (vpp = &vmap; *vpp; vpp = &(*vpp)->nxt)
    ;
  *vpp = vp;

  return vp;
}

/* Read or write the exec file.

   Args are address within a BFD file, address within gdb address-space,
   length, and a flag indicating whether to read or write.

   Result is a length:

   0:    We cannot handle this address and length.
   > 0:  We have handled N bytes starting at this address.
   (If N == length, we did it all.)  We might be able
   to handle more bytes beyond this length, but no
   promises.
   < 0:  We cannot handle this address, but if somebody
   else handles (-N) bytes, we can start from there.

   The same routine is used to handle both core and exec files;
   we just tail-call it with more arguments to select between them.  */

int
xfer_memory (CORE_ADDR memaddr,
             char *myaddr,
             int len,
             int write,
             struct target_ops *target)
{
  boolean res;
  struct section_table *p;
  CORE_ADDR nextsectaddr, memend;
  boolean (*xfer_fn) (bfd *, sec_ptr, PTR, file_ptr, bfd_size_type);
  asection *section = 0; /* initialize for compiler warning */

  /* Aries corefile debugging. */
#ifdef HP_IA64
  static bool unwind_sec = false;
  static bool unwind_hdr_sec = false;
  static bool unwind_info_sec = false;
#endif

  if (len <= 0)
    abort ();

#ifdef HP_IA64_NATIVE
  /* If the address falls in rse, read using read_inferior_ideal_memory,
     which handles reading rse.
   */
  /* We might come in with an unswizzled address. Don't swizzle if we
     are refering to a kernel address. */
  memaddr = address_in_user_sendsig_unw (memaddr)? memaddr: SWIZZLE (memaddr);
  if (   !write 
      && !(inferior_pid & TID_MARKER) 
      && address_in_rsebs (memaddr, len)
     )
    {
      if (!read_inferior_ideal_memory (memaddr, myaddr, len))
        return len;
      else
        return 0;
    }
#endif

  if (overlay_debugging)
    {
      section = find_pc_overlay (memaddr);
      if (pc_in_unmapped_range (memaddr, section))
	memaddr = overlay_mapped_address (memaddr, section);
    }

  memend = memaddr + len;
  xfer_fn = write ? bfd_set_section_contents : bfd_get_section_contents;
  nextsectaddr = memend;

  
  for (p = target->to_sections; p < target->to_sections_end; p++)
    {
#ifdef HP_IA64
      /* QXCR1000579474: Aries core file debugging. */
      /* Apply the runtime relocation offset to the unwind sections. */
      if (   (aries_core_debug)
          && ((0 == strncmp (p->the_bfd_section->name, ".IA_64.unwind", 13))))
        {
          /* Subtract the unwind link time address and add the run time
             address. */
          if (   (false == unwind_sec)
              && (13 == strlen (p->the_bfd_section->name)))
            {
              p->addr += (aries_runtime_unwind_start
                          - aries_linktime_unwind_start);
              p->endaddr += (aries_runtime_unwind_start
                          - aries_linktime_unwind_start);
              unwind_sec = true;
            }
          if (   (false == unwind_hdr_sec)
              && (0 == strcmp (p->the_bfd_section->name + 13, "_hdr")))
            {
              p->addr += (aries_runtime_unwind_start -
                          aries_linktime_unwind_start);
              p->endaddr += (aries_runtime_unwind_start -
                             aries_linktime_unwind_start);
              unwind_hdr_sec = true;
            }
          if (   (false == unwind_info_sec)
              && (0 == strcmp (p->the_bfd_section->name + 13, "_info")))
            {
              p->addr += (aries_runtime_unwind_start -
                          aries_linktime_unwind_start);
              p->endaddr += (aries_runtime_unwind_start -
                             aries_linktime_unwind_start);
              unwind_info_sec = true;
            }
        }
#endif

      if (overlay_debugging && section && p->the_bfd_section &&
	  strcmp (section->name, p->the_bfd_section->name) != 0)
	continue;		/* not the section we need */
      if (memaddr >= p->addr)
	if (memend <= p->endaddr)
	  {
#ifdef HP_IA64
            /* QXCR1000903217: Check if we have a mismatch */
            if (target_has_stack && !target_has_execution)
              {
                /* The following code tries to skip a section, if it finds that
                   the current section belongs to a mismatched library and if 
                   the memaddr belongs to a different library. The solib list
                   maintained by gdb has the text start address and size from
                   the core file. So, an address, which is supposed to belong to
                   the text section of some library, will find the right library
                   (as available in the core file) if the solib maintained by
                   gdb is searched. On the other hand, the bfd sections for the
                   libraries are added from the mismatched library available.

                   If we know that we are expecting the memaddr to fall within
                   the text section of a shared library, then we can verify here
                   if we got misled by a mismatched library section here. If we
                   detect it, we can skip this mismatched section and continue
                   the search to get to the right section from the right 
                   library.
                */

                struct so_list* p_solib
                  = (struct so_list*)
                       hp_ia64_solib_get_solib_ptr_by_pc (p->addr);

                struct so_list* memaddr_solib
                  = (struct so_list*)
                       hp_ia64_solib_get_solib_ptr_by_pc (memaddr);

                if (p_solib && p_solib->mismatch && (p_solib != memaddr_solib))
                {
                  continue;
                }

                if (p_solib && p_solib->mismatch)
                {
                  mismatched_so_name = hp_ia64_solib_address (p->addr);
                }
             }
#endif // HP_IA64

	    /* Entire transfer is within this section.  */
	    res = xfer_fn (p->bfd, p->the_bfd_section, myaddr,
			   UNSWIZZLE (memaddr) - UNSWIZZLE (p->addr), len);
	    return (res != 0) ? len : 0;
	  }
	else if (memaddr >= p->endaddr)
	  {
	    /* This section ends before the transfer starts.  */
	    continue;
	  }
	else
	  {
	    /* This section overlaps the transfer.  Just do half.  */
	    len = (int)(p->endaddr - memaddr);
	    res = xfer_fn (p->bfd, p->the_bfd_section, myaddr,
			   UNSWIZZLE (memaddr) - UNSWIZZLE (p->addr), len);
	    return (res != 0) ? len : 0;
	  }
      else
	nextsectaddr = min (nextsectaddr, p->addr);
    }

  if (nextsectaddr > memend)
    {
#ifdef HP_IA64_NATIVE
/* Remove when sendsig's unwind info is dumped */
    if (address_in_user_sendsig_unw(memaddr)) {
	ia64_read_local_mem_kernel_pages(myaddr, memaddr, len);
	return len;
	}
#endif /* HP_IA64_NATIVE */
    return 0;			/* We can't help */
    }
  else
    {
      /* Next boundary where we can help */
      return (int)(-(UNSWIZZLE (nextsectaddr) - UNSWIZZLE (memaddr)));	
    }
}


void
print_section_info (struct target_ops *t,
                    bfd *abfd)
{
  struct section_table *p;
#ifdef HP_IA64
  int cnt = 0;
#endif

  printf_filtered ("\t`%s', ", bfd_get_filename (abfd));
  wrap_here ("        ");
  printf_filtered ("file type %s.\n", bfd_get_target (abfd));
  if (abfd == exec_bfd)
    {
      printf_filtered ("\tEntry point: ");
      print_address_numeric (bfd_get_start_address (abfd), 1, gdb_stdout);
      printf_filtered ("\n");
    }
  for (p = t->to_sections; p < t->to_sections_end; p++)
    {
      printf_filtered ("\t%s",
		       longest_local_hex_string_custom ((LONGEST) p->addr,
							"08l"));
      printf_filtered (" - %s",
		       longest_local_hex_string_custom ((LONGEST) p->endaddr,
							"08l"));

      if (info_verbose)
	printf_filtered (" @ %s",
			 local_hex_string_custom ((unsigned long) p->the_bfd_section->filepos, "08l"));
      printf_filtered (" is %s", bfd_section_name (p->bfd, p->the_bfd_section));

      /* Print type info only for core file sections. */
#ifdef HP_IA64
      if ((abfd == core_bfd) && (bfd_get_format (p->bfd) == bfd_core))
        {
          if ((p->the_bfd_section->type_info == PT_HP_CORE_STACK))
            {
              /* First stack is RSE. */
              if (cnt == 0)
                {
                  printf_filtered (" (%s - RSE)\n",
                     coretype_name (p->the_bfd_section->type_info));
                  cnt++;
                }
              else if (cnt == 1)
                {
                  printf_filtered (" (%s)\n",
                     coretype_name (p->the_bfd_section->type_info));
                  cnt++;
                  /* Print heap segment after main stack. */
                  print_heap_corefile ();
                }
            }
#ifdef HP_IA64
          else if (   (p->the_bfd_section->type_info == PT_HP_CORE_MMF)
                   && (p->the_bfd_section->flags & SEC_MMAPFILE_ADD))
            {
              printf_filtered (" (Added with mmapfile \'%s\'))",
                                p->the_bfd_section->mmap_sect_ptr->filename);
            }
#endif
          else
            printf_filtered (" (%s)",  
                             coretype_name (p->the_bfd_section->type_info));
        }
#endif

      if (p->bfd != abfd)
	{
	  printf_filtered (" in %s", bfd_get_filename (p->bfd));
	}
      printf_filtered ("\n");
    }
}

#ifdef HP_IA64
/* Print core type name matching the type value. */
static char * 
coretype_name (unsigned long core_t)
{
  static char buf[24];

  for (int i = 0; i < sizeof (core_types)/sizeof (core_types[0]); i++)
    {
      if (core_types[i].type_val == core_t)
        return core_types[i].type_name;
    }
  (void) sprintf(buf, "UNKNOWN TYPE (%#x)", (int) core_t);
  return buf;
}

/* Print the heap segment of the corefile.
   _minbrk contains start address and
   _curbrk contains end address of the core dump. */
static void
print_heap_corefile ()
{
  struct minimal_symbol *m;
  CORE_ADDR start_addr, end_addr;
  int status;

  m = lookup_minimal_symbol ("_minbrk", NULL, NULL);
  if (m == NULL)
    start_addr = NULL;
  else
    {
      status = target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                                     (char *) &start_addr, sizeof(CORE_ADDR));
      if (status != 0)
        start_addr = NULL;
      else
        if (is_swizzled)
          {
            start_addr = start_addr >> 32;
            start_addr = swizzle (start_addr);
          }
    }

  m = lookup_minimal_symbol ("_curbrk", NULL, NULL);
  if (m == NULL)
    end_addr = NULL;
  else
    {
      status = target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                                     (char *) &end_addr, sizeof(CORE_ADDR));
      if (status != 0)
        end_addr = NULL;
      else
        if (is_swizzled)
          {
            end_addr = end_addr >> 32;
            end_addr = swizzle (end_addr);
          }
    }
  if (start_addr && end_addr)
    {
      printf_filtered ("\t%s",
		       longest_local_hex_string_custom ((LONGEST) start_addr,
							"08l"));
      printf_filtered (" - %s",
		       longest_local_hex_string_custom ((LONGEST) end_addr,
							"08l"));
      printf_filtered (" is heap segment");
    }
}
#endif

static void
exec_files_info (struct target_ops *t)
{
  print_section_info (t, exec_bfd);

  if (vmap)
    {
      struct vmap *vp;

      printf_unfiltered ("\tMapping info for file `%s'.\n", vmap->name);
      printf_unfiltered ("\t  %*s   %*s   %*s   %*s %8.8s %s\n",
			 strlen_paddr (), "tstart",
			 strlen_paddr (), "tend",
			 strlen_paddr (), "dstart",
			 strlen_paddr (), "dend",
			 "section",
			 "file(member)");

      for (vp = vmap; vp; vp = vp->nxt)
	printf_unfiltered ("\t0x%s 0x%s 0x%s 0x%s %s%s%s%s\n",
			   paddr (vp->tstart),
			   paddr (vp->tend),
			   paddr (vp->dstart),
			   paddr (vp->dend),
			   vp->name,
			   *vp->member ? "(" : "", vp->member,
			   *vp->member ? ")" : "");
    }
}

/* msnyder 5/21/99:
   exec_set_section_offsets sets the offsets of all the sections
   in the exec objfile.  */

void
exec_set_section_offsets (bfd_signed_vma text_off,
                          bfd_signed_vma data_off,
                          bfd_signed_vma bss_off)
{
  struct section_table *sect;

  for (sect = exec_ops.to_sections;
       sect < exec_ops.to_sections_end;
       sect++)
    {
      flagword flags;

      flags = bfd_get_section_flags (exec_bfd, sect->the_bfd_section);

      if (flags & SEC_CODE)
	{
	  sect->addr += text_off;
	  sect->endaddr += text_off;
	}
      else if (flags & (SEC_DATA | SEC_LOAD))
	{
	  sect->addr += data_off;
	  sect->endaddr += data_off;
	}
      else if (flags & SEC_ALLOC)
	{
	  sect->addr += bss_off;
	  sect->endaddr += bss_off;
	}
    }
}

static void
set_section_command (char *args,
                     int from_tty)
{
  struct section_table *p;
  char *secname;
  unsigned seclen;
  CORE_ADDR secaddr;
  char secprint[100];
  CORE_ADDR offset;

  if (args == 0)
    error ("Must specify section name and its virtual address");

  /* Parse out section name */
  for (secname = args; !isspace (*args); args++);
  seclen = (unsigned int)(args - secname);

  /* Parse out new virtual address */
  secaddr = parse_and_eval_address (args);

  for (p = exec_ops.to_sections; p < exec_ops.to_sections_end; p++)
    {
      if (!strncmp (secname, bfd_section_name (exec_bfd, p->the_bfd_section), seclen)
	  && bfd_section_name (exec_bfd, p->the_bfd_section)[seclen] == '\0')
	{
	  offset = secaddr - p->addr;
	  p->addr += offset;
	  p->endaddr = SWIZZLE (p->endaddr + offset);
	  if (from_tty)
	    exec_files_info (&exec_ops);
	  return;
	}
    }
  if (seclen >= sizeof (secprint))
    seclen = sizeof (secprint) - 1;
  strncpy (secprint, secname, seclen);
  secprint[seclen] = '\0';
  error ("Section %s not found", secprint);
}

/* If mourn is being called in all the right places, this could be say
   `gdb internal error' (since generic_mourn calls
   breakpoint_init_inferior).  */

static int
ignore (CORE_ADDR addr,
        char *contents)
{
  return 0;
}

/* Fill in the exec file target vector.  Very few entries need to be
   defined.  */

static void
init_exec_ops ()
{
  exec_ops.to_shortname = "exec";
  exec_ops.to_longname = "Local exec file";
  exec_ops.to_doc = "Use an executable file as a target.\n\n\
Usage:\n\ttarget exec <EXEC-FILE>\n\n\
Specify the filename of the executable file.";
  exec_ops.to_open = exec_file_command;
  exec_ops.to_close = exec_close;
  exec_ops.to_attach = find_default_attach;
  exec_ops.to_require_attach = find_default_require_attach;
  exec_ops.to_require_detach = find_default_require_detach;
  exec_ops.to_xfer_memory = xfer_memory;
  exec_ops.to_files_info = exec_files_info;
  exec_ops.to_insert_breakpoint = ignore;
  exec_ops.to_remove_breakpoint = ignore;
  exec_ops.to_create_inferior = find_default_create_inferior;
  exec_ops.to_clone_and_follow_inferior = find_default_clone_and_follow_inferior;
  exec_ops.to_stratum = file_stratum;
  exec_ops.to_has_memory = 1;
  exec_ops.to_magic = OPS_MAGIC;
}

void
_initialize_exec ()
{
  struct cmd_list_element *c;

  init_exec_ops ();

  if (!dbx_commands)
    {
      c = add_cmd ("file", class_files, file_command,
		   "Use FILE as program to be debugged.\n\n\
Usage:\n\tfile [<FILE>]\n\n\
FILE is read for its symbols, for getting the contents of pure memory,\n\
and it is the program executed when you use the `run' command.\n\
If FILE cannot be found as specified, your execution directory path\n\
($PATH) is searched for a command of that name.\n\
No arg means to have no executable file and no symbols.", &cmdlist);
      c->completer = filename_completer;
    }

  c = add_cmd ("exec-file", class_files, exec_file_command,
	       "Use FILE as program for getting contents of pure memory.\n\n\
Usage:\n\texec-file [<FILE>]\n\n\
If FILE cannot be found as specified, your execution directory path\n\
is searched for a command of that name.\n\
No arg means have no executable file.", &cmdlist);
  c->completer = filename_completer;

  add_com ("section", class_files, set_section_command,
	   "Change the base address of section SECTION of the exec file to ADDR.\n\n\
Usage:\n\tsection <SECTION> <ADDR>\n\n\
This can be used if the exec file does not contain section addresses,\n\
(such as in the a.out format), or when the addresses specified in the\n\
file itself are wrong.  Each section must be changed separately.  The\n\
``info files'' command lists all the sections and their addresses.");

  add_show_from_set
    (add_set_cmd ("write", class_support, var_boolean, (char *) &write_files,
		  "Set writing into executable and core files.\n\n"
                  "Usage:\nTo set new value:\n\tset write [on | off]\n"
                  "To see current value:\n\tshow write\n",
		  &setlist),
     &showlist);

  add_target (&exec_ops);
}
