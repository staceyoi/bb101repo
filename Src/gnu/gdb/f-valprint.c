/* Support for printing Fortran values for GDB, the GNU debugger.
   copyright 1993-1995, 2000 Free Software Foundation, Inc.
   Contributed by Motorola.  Adapted from the C definitions by Farooq Butt
   (fmbutt@engage.sps.mot.com), additionally worked over by Stan Shebs.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdb_string.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "expression.h"
#include "value.h"
#include "demangle.h"
#include "valprint.h"
#include "language.h"
#include "f-lang.h"
#include "c-lang.h"
#include "frame.h"
#include "gdbcore.h"
#include "command.h"
#include "annotate.h"
#include "target.h" /* for target_has_stack */

extern struct frame_info *currently_printing_frame;  /* JAGae92961 */
extern CORE_ADDR skip_prologue_hard_way (CORE_ADDR pc);
extern void _initialize_f_valprint (void);
static void info_common_command (char *, int);
static void list_all_visible_commons (char *);
static void f77_print_array (struct type *, char *, CORE_ADDR,
			     struct ui_file *, char[], int, int,
			     enum val_prettyprint);
static void f77_print_array_1 (int, int, struct type *, char *,
			       CORE_ADDR, struct ui_file *, char[], int, int,
			       enum val_prettyprint,
			       unsigned int *how_many);
void f77_create_arrayprint_offset_tbl (struct type *, struct ui_file *,int); /* JAGaf56705 */

int f77_array_offset_tbl[MAX_FORTRAN_DIMS + 1][3];

extern int dont_print_value;

/* Array which holds offsets to be applied to get a row's elements
   for a given array. Array also holds the size of each subarray.  */

/* The following macro gives us the size of the nth dimension, Where 
   n is 1 based. */

#define F77_DIM_SIZE(n) (f77_array_offset_tbl[n][1])

/* The following gives us the offset for row n where n is 1-based. */

#define F77_DIM_OFFSET(n) (f77_array_offset_tbl[n][0])

/* The following gives us the stride for row n where n is 1-based */

#define F77_DIM_STRIDE(n) (f77_array_offset_tbl[n][2])

/* Functions and external objects required to print Fortran array slice */

static void ftn_print_array_slice_1 (int, struct type *, char *, CORE_ADDR,
				     struct ui_file *, char[], int, int, enum val_prettyprint);
static void ftn_print_array_slice (struct type *, char *, CORE_ADDR, struct ui_file *,
			           char[], int, int, enum val_prettyprint);
/* JAGag29592 */
extern void set_currently_printing_frame (void);
extern slicevector *slice_vector;
extern offset_table *offset_tbl;
extern boolean print_ftn_array_slice;
extern LONGEST lower_bound[MAX_FORTRAN_DIMS + 1];

int
f77_get_dynamic_lowerbound (struct type *type, LONGEST *lower_bound)
{
  CORE_ADDR current_frame_addr;
  CORE_ADDR ptr_to_lower_bound;

  /* JAGag29592: jini: The cached frames would have got flushed previously.
     Have 'currently_printing_frame' and 'selected_frame' in sync with the
     'selected_frame_level'. */
  if (NULL == currently_printing_frame && target_has_stack)
    {
      set_currently_printing_frame();
    }

  switch (TYPE_ARRAY_LOWER_BOUND_TYPE (type))
    {
    case BOUND_BY_VARIABLE:
      {
        struct symbol *sym
          = (struct symbol*)TYPE_ARRAY_LOWER_BOUND_VALUE (type);
        value_ptr value = read_var_value (sym, currently_printing_frame); /* JAGae92961 - take value from current frame being printed */

        *lower_bound = value_as_pointer (value);
        return BOUND_FETCH_OK;
      }

    case BOUND_BY_VALUE_ON_STACK:
      current_frame_addr = currently_printing_frame->frame;
      if (current_frame_addr > 0)
	{
	  *lower_bound =
	    read_memory_integer (current_frame_addr +
				 TYPE_ARRAY_LOWER_BOUND_VALUE (type),
                               TYPE_LENGTH (TYPE_ARRAY_LOW_BOUND_TYPE (type)));
	}
      else
	{
	  *lower_bound = DEFAULT_LOWER_BOUND;
	  return BOUND_FETCH_ERROR;
	}
      break;

    case BOUND_SIMPLE:
      *lower_bound = TYPE_ARRAY_LOWER_BOUND_VALUE (type);
      break;

    case BOUND_CANNOT_BE_DETERMINED:
      error ("Lower bound may not be '*' in F77");
      break;

    case BOUND_BY_REF_ON_STACK:
      current_frame_addr = currently_printing_frame->frame;
      if (current_frame_addr > 0)
	{
	  ptr_to_lower_bound =
	    read_memory_integer (current_frame_addr +
				 TYPE_ARRAY_LOWER_BOUND_VALUE (type),
                                 TARGET_PTR_BIT / TARGET_CHAR_BIT);
          *lower_bound = read_memory_integer (ptr_to_lower_bound,
                                            TARGET_INT_BIT / TARGET_CHAR_BIT); 
	}
      else
	{
	  *lower_bound = DEFAULT_LOWER_BOUND;
	  return BOUND_FETCH_ERROR;
	}
      break;

    case BOUND_BY_VARIABLE_WITH_OFFSET:
      {
#ifdef F_GET_ARRAY_DESC_ARRAY_LBOUND 
        return F_GET_ARRAY_DESC_ARRAY_LBOUND (type, lower_bound);
#else
        error ("Unhandled dynamic array lower bound type: %d",
               TYPE_ARRAY_LOWER_BOUND_TYPE (type));
        break;
#endif
      } 

    case BOUND_BY_REF_IN_REG:
    case BOUND_BY_VALUE_IN_REG:
    default:
      error ("Unhandled dynamic array lower bound type: %d",
             TYPE_ARRAY_LOWER_BOUND_TYPE (type));
      break;
    }
  return BOUND_FETCH_OK;
}

int
f77_get_dynamic_upperbound (struct type *type, LONGEST *upper_bound)
{
  CORE_ADDR current_frame_addr = 0;
  CORE_ADDR ptr_to_upper_bound;
  LONGEST lower_bound; 
  int retval;
  
  /* JAGag29592: jini: The cached frames would have got flushed previously.
     Have 'currently_printing_frame' and 'selected_frame' in sync with the
     'selected_frame_level'. */
  if (NULL == currently_printing_frame && target_has_stack)
    {
      set_currently_printing_frame();
    }

  switch (TYPE_ARRAY_UPPER_BOUND_TYPE (type))
    {
    case BOUND_BY_VARIABLE:
      {
        struct symbol *sym
          = (struct symbol*)TYPE_ARRAY_UPPER_BOUND_VALUE (type);
        value_ptr value = read_var_value (sym, currently_printing_frame); /* JAGae92961 - take value from current frame being printed */

        *upper_bound = value_as_pointer (value);
       /* f90, TYPE_EXTENT is set if sym actually holds the extent,
	   not the upper bound, of the array; thus the upper bound
	   must be calculated */ 
	if (TYPE_EXTENT (TYPE_FIELD_TYPE (type,0)))
	  {
	    retval = f77_get_dynamic_lowerbound (type, &lower_bound);
	    if (retval != BOUND_FETCH_OK)
	      return retval;
	    *upper_bound = *upper_bound + lower_bound - 1;

	    /* If C, add 1 which will be taken off below */
	    if (current_language->la_language == language_c)
	      *upper_bound += 1;
	  }

        /* upper bound is interpreted as number of elements for C variable
           length arrays */
        if (current_language->la_language == language_c)
          *upper_bound -= 1;
        return BOUND_FETCH_OK;
      }

    case BOUND_BY_VALUE_ON_STACK:
      current_frame_addr = FRAME_LOCALS_ADDRESS (currently_printing_frame);
       /* f90, TYPE_EXTENT is set if sym actually holds the extent,
     not the upper bound, of the array; thus the upper bound
     must be calculated */
      if (TYPE_EXTENT (TYPE_FIELD_TYPE (type,0)))
	  {
	    retval = f77_get_dynamic_lowerbound (type, &lower_bound);
	    if (retval != BOUND_FETCH_OK)
	      return retval;
	    *upper_bound = *upper_bound + lower_bound - 1;
	  }
      if (current_frame_addr > 0)
	{
	  *upper_bound =
	    read_memory_integer (current_frame_addr +
				 TYPE_ARRAY_UPPER_BOUND_VALUE (type),
                             TYPE_LENGTH (TYPE_ARRAY_HIGH_BOUND_TYPE (type)));
           			     
	  /* upper bound is interpreted as number of elements for C variable
	     length arrays */
	  if (current_language->la_language == language_c)
	    *upper_bound -= 1;
	}
      else
	{
	  *upper_bound = DEFAULT_UPPER_BOUND;
	  return BOUND_FETCH_ERROR;
	}
      break;

    case BOUND_SIMPLE:
      *upper_bound = TYPE_ARRAY_UPPER_BOUND_VALUE (type);
      if (*upper_bound == INT_MAX)
        {
          /* we have an assumed size array on our hands. Assume that 
             upper_bound == lower_bound so that we show at least 
             1 element.If the user wants to see more elements, let 
             him manually ask for 'em and we'll subscript the 
             array and show him */
          f77_get_dynamic_lowerbound (type, upper_bound);
        }
      break;

    case BOUND_CANNOT_BE_DETERMINED:
      /* we have an assumed size array on our hands. Assume that 
         upper_bound == lower_bound so that we show at least 
         1 element.If the user wants to see more elements, let 
         him manually ask for 'em and we'll subscript the 
         array and show him */
      f77_get_dynamic_lowerbound (type, upper_bound);
      break;

    case BOUND_BY_REF_ON_STACK:
      current_frame_addr = FRAME_ARGS_ADDRESS (currently_printing_frame);
      if (current_frame_addr > 0)
	{
	  ptr_to_upper_bound =
	    read_memory_integer (current_frame_addr +
				 TYPE_ARRAY_UPPER_BOUND_VALUE (type),
                                 TARGET_PTR_BIT / TARGET_CHAR_BIT);
	  *upper_bound = read_memory_integer(ptr_to_upper_bound,
                                           TARGET_INT_BIT / TARGET_CHAR_BIT); 
	  /* upper bound is interpreted as number of elements for C variable
	     length arrays */
	  if (current_language->la_language == language_c)
	    *upper_bound -= 1;
	}
      else
	{
	  *upper_bound = DEFAULT_UPPER_BOUND;
	  return BOUND_FETCH_ERROR;
	}
      break;

    case BOUND_BY_VARIABLE_WITH_OFFSET:
      {
#ifdef F_GET_ARRAY_DESC_ARRAY_UBOUND
        return F_GET_ARRAY_DESC_ARRAY_UBOUND (type, upper_bound);
#else
        error ("Unhandled dynamic array upper bound type: %d",
               TYPE_ARRAY_UPPER_BOUND_TYPE (type));
        break;
#endif
      }

    case BOUND_BY_REF_IN_REG:
    case BOUND_BY_VALUE_IN_REG:
    default:
      error ("Unhandled dynamic array upper bound type: %d",
             TYPE_ARRAY_UPPER_BOUND_TYPE (type));
      break;
    }
  return BOUND_FETCH_OK;
}

/* Set the stride in the Fortran struct field */
void
f_set_array_stride (struct type *type)
{
  if (TYPE_STRIDE_VARIABLE (TYPE_FIELD_TYPE (type, 0)))
    {
      struct symbol *sym = (struct symbol*) TYPE_ARRAY_STRIDE_LOC (type);
      value_ptr value = read_var_value (sym, selected_frame);
      TYPE_ARRAY_STRIDE(type) = (int) value_as_pointer (value);
    }
#ifdef F_SET_ARRAY_DESC_ARRAY_STRIDE
  else if (TYPE_STRIDE_OFFSET (TYPE_FIELD_TYPE (type, 0)))
    F_SET_ARRAY_DESC_ARRAY_STRIDE (type);
#endif
  else
    {
      if (!TYPE_FORTRAN_SPECIFIC (type))
	ALLOCATE_FORTRAN_STRUCT_TYPE (type);
      TYPE_ARRAY_STRIDE(type) = TYPE_LENGTH (get_element_type (type));
    }
}
/* Obtain F77 adjustable array dimensions */

void
f77_get_dynamic_length_of_aggregate (struct type *type)
{
  LONGEST upper_bound = -1;
  LONGEST lower_bound = current_language->la_language == language_fortran ? 1 :0;
  int retcode;

  /* Recursively go all the way down into a possibly multi-dimensional
     F77 array and get the bounds.  For simple arrays, this is pretty
     easy but when the bounds are dynamic, we must be very careful 
     to add up all the lengths correctly.  Not doing this right 
     will lead to horrendous-looking arrays in parameter lists.

     This function also works for strings which behave very 
     similarly to arrays.  */

  if (TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_ARRAY
      || TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_STRING)
    f77_get_dynamic_length_of_aggregate (TYPE_TARGET_TYPE (type));

  /* Recursion ends here, start setting up lengths.  */
  retcode = f77_get_dynamic_lowerbound (type, &lower_bound);
  if (retcode == BOUND_FETCH_ERROR)
    error ("Cannot obtain valid array lower bound");

  retcode = f77_get_dynamic_upperbound (type, &upper_bound);
  if (retcode == BOUND_FETCH_ERROR)
    error ("Cannot obtain valid array upper bound");

/* MERGE: On IPF calculation of stride for FORTRAN arrays is done seperately.
   This is not required on PA and making this call leads to printing of 
   wrong array values. ifdef added during PA-IPF source merge. */ 
#ifdef HP_IA64
  f_set_array_stride (type);
#endif

  /* Patch in a valid length value. */

  if (TYPE_ARRAY_UPPER_BOUND_TYPE (type) == BOUND_BY_VARIABLE_WITH_OFFSET)
    {
      TYPE_LENGTH (type) = (int) 
	((upper_bound - lower_bound) * abs(TYPE_ARRAY_STRIDE(type))
        + TYPE_LENGTH (TYPE_TARGET_TYPE (type)));
    }
  else
    { 
      TYPE_LENGTH (type) = (int)
	((upper_bound - lower_bound + 1)
	* TYPE_LENGTH (check_typedef (TYPE_TARGET_TYPE (type))));
    } 
}

/* Function that sets up the array offset-size-stride table for the array 
   type "type".  */
/* JAGaf56705 - <gdb has problems in printing fortran array> - START
   Suppose <type> is an array and <type->target_type> is also an array.  
   In this case, if we want to reset the offset table values of <type> ,
   we need the `ndimen` (dimension) of the current array.  
   JAGaf56705 - END */
void
f77_create_arrayprint_offset_tbl (struct type *type, 
				  struct ui_file *stream,
				  int ndimen)
{
  struct type *tmp_type;
  int eltlen;
  LONGEST upper, lower;
  int retcode;

  tmp_type = type;

  while ((TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY))
    {
      if (TYPE_ARRAY_UPPER_BOUND_TYPE (tmp_type) == BOUND_CANNOT_BE_DETERMINED)
	fprintf_filtered (stream, "<assumed size array> ");

      retcode = f77_get_dynamic_upperbound (tmp_type, &upper);
      if (retcode == BOUND_FETCH_ERROR)
	error ("Cannot obtain dynamic upper bound");

      retcode = f77_get_dynamic_lowerbound (tmp_type, &lower);
      if (retcode == BOUND_FETCH_ERROR)
	error ("Cannot obtain dynamic lower bound");

      F77_DIM_SIZE (ndimen) = (int) (upper - lower + 1);

      /* The upper bound of an array descriptor array is always 
       * BOUND_BY_VARIABLE_WITH_OFFSET.  Since an array's stride cannot 
       * be 0, we set F77_DIM_STRIDE (ndimen) to 0 if we are not dealing
       * with an array descriptor array and use this as a flag when calculating 
       * the offsets.
       */
      F77_DIM_STRIDE (ndimen) =
        TYPE_ARRAY_UPPER_BOUND_TYPE (tmp_type) == BOUND_BY_VARIABLE_WITH_OFFSET
        ? TYPE_ARRAY_STRIDE (tmp_type) : 0;

      tmp_type = TYPE_TARGET_TYPE (tmp_type);
      ndimen++;
    }

  /* Now we multiply eltlen by all the offsets, so that later we 
     can print out array elements correctly.  Up till now we 
     know an offset to apply to get the item but we also 
     have to know how much to add to get to the next item */

  ndimen--;
  eltlen = F77_DIM_STRIDE (ndimen) ?
	   F77_DIM_STRIDE (ndimen) : TYPE_LENGTH (tmp_type);
  F77_DIM_OFFSET (ndimen) = eltlen;
  while (--ndimen > 0)
    {
      if (F77_DIM_STRIDE (ndimen))
         eltlen = F77_DIM_STRIDE (ndimen);
      else 
	eltlen *= F77_DIM_SIZE (ndimen + 1);
      F77_DIM_OFFSET (ndimen) = eltlen;
    }
}

/* Set the address of the array descriptor variable in the array type nodes.
 * This allows us to access the array descriptor via array type node
 * in f77_get_dynamic_lowerbound f77_get_dynamic_upperbound
 */
void
f_fixup_array_desc_type_array_addr (struct type *array_desc_type,
				    CORE_ADDR address)
{
  struct type *array_type = TYPE_TARGET_TYPE (array_desc_type);
  while (TYPE_CODE (array_type) == TYPE_CODE_ARRAY)
    {
      TYPE_ARRAY_ADDR (array_type) = address;
      array_type = TYPE_TARGET_TYPE (array_type);
    }
}

/* Returns element type of an array; it assumes that array_type is a valid
   array type node */

struct type *
get_element_type (struct type *array_type)
{

  struct type *target_type = TYPE_TARGET_TYPE (check_typedef (array_type));
  while (TYPE_CODE (target_type)  == TYPE_CODE_ARRAY)
    target_type = TYPE_TARGET_TYPE (check_typedef (target_type));
  return target_type;
}

/* Actual function which prints out F77 arrays, Valaddr == address in 
   the superior.  Address == the address in the inferior.  */

static void
f77_print_array_1 (int nss,
		   int ndimensions,
		   struct type *type,
		   char *valaddr,
		   CORE_ADDR address,
		   struct ui_file *stream,
		   char format[],
		   int deref_ref,
		   int recurse,
		   enum val_prettyprint pretty,
		   unsigned int *how_many /* Elements printed so far */ )
{
  int i;
  extern unsigned int print_max;
  int retcode; 
  LONGEST lower_bound;
  int max_printed = 0;
 
  retcode = f77_get_dynamic_lowerbound (type, &lower_bound);
  if (retcode == BOUND_FETCH_ERROR)
    error ("Cannot obtain valid array lower bound");
  annotate_array_section_begin ((int) lower_bound,
                                check_typedef (TYPE_TARGET_TYPE (type)));

  if (!dont_print_value)
    {
      if (nss != ndimensions)
	{
	  for (i = 0; i < F77_DIM_SIZE (nss); i++)
	    {
	      fprintf_filtered (stream, "(");
	      f77_print_array_1 (nss + 1, ndimensions, TYPE_TARGET_TYPE (type),
				 valaddr + i * F77_DIM_OFFSET (nss),
				 address + i * F77_DIM_OFFSET (nss),
				 stream, format, deref_ref, recurse, pretty,
				 how_many);
              /* JAGaf86891 - <gdb shows data structures incorrectly>
                  gdb was printing wrong results for fortran arrays with structures
                  of fortran array types. If there is multi-dimensional array
                  with structure of multidimensional array, offset table values are
                  not updated. So the offset value, size of nth dimension and stride
                  changes when it is called recursively and doesnt change for dimensions.
                  So offset table values are updated each time for the particular 
		  dimension. JAGaf56705 had fixed only one part and had missed to check for
		  multidimensional arrays. */
	      if (TYPE_CODE(type) == TYPE_CODE_ARRAY)
		f77_create_arrayprint_offset_tbl(type, stream, nss);
	      fprintf_filtered (stream, ")");
	      annotate_elt ();
	      if (*how_many == print_max && i < F77_DIM_SIZE(nss) - 1)
		{
		  max_printed = 1;
		  break;
		}
	      if (i < F77_DIM_SIZE(nss) - 1)
		fprintf_filtered (stream, ", ");
	    }
	}
      else
	{
	  for (i = 0; (i < F77_DIM_SIZE (nss)); i++)
	    {
	      val_print (TYPE_TARGET_TYPE (type),
			 valaddr + i * F77_DIM_OFFSET (ndimensions),
			 0,
			 address + i * F77_DIM_OFFSET (ndimensions),
			 stream, format, deref_ref, recurse, pretty);
	      /* JAGaf56705 - <gdb has problems printing f90 array contents> - START 
	         If <type> is an array and <type->target_type> is also an array,
		 then <type>'s offset table values may have changed.
		 Therefore, <type>'s offset table values are set again.*/	 
	      if (TYPE_CODE(type) == TYPE_CODE_ARRAY)
	        f77_create_arrayprint_offset_tbl(type, stream, ndimensions); 
	      /* JAGaf56705 - END */
	      annotate_elt ();
	      (*how_many)++;
	      if (*how_many == print_max && i < F77_DIM_SIZE (nss) - 1)
		{
		  max_printed = 1;
		  break;
		}
	      if (i < F77_DIM_SIZE (nss) - 1)
		fprintf_filtered (stream, ", "); 
	      
	    }
	}
    }
  annotate_array_section_end ();
  
  if (max_printed)
    fprintf_filtered (stream, "...");
}

/* This function gets called to print an F77 array, we set up some 
   stuff and then immediately call f77_print_array_1() */

static void
f77_print_array (struct type *type,
		 char *valaddr,
		 CORE_ADDR address,
		 struct ui_file *stream,
		 char format[],
		 int deref_ref,
	         int recurse,
		 enum val_prettyprint pretty)
{
  int ndimensions;
  unsigned int how_many = 0;

  ndimensions = calc_f77_array_dims (type);

  if (ndimensions > MAX_FORTRAN_DIMS || ndimensions < 0)
    error ("Type node corrupt! F77 arrays cannot have %d subscripts (%d Max)",
	   ndimensions, MAX_FORTRAN_DIMS);

  /* Since F77 arrays are stored column-major, we set up an 
     offset table to get at the various row's elements. The 
     offset table contains entries for offset, size and stride for each
     subarray. */

  f77_create_arrayprint_offset_tbl (type, stream, 1);  /* JAGaf56705 */

  f77_print_array_1 (1, ndimensions, type, valaddr, address, stream, format,
		     deref_ref, recurse, pretty, &how_many);
}


/* Print data of type TYPE located at VALADDR (within GDB), which came from
   the inferior at address ADDRESS, onto stdio stream STREAM according to
   FORMAT (a letter or 0 for natural format).  The data at VALADDR is in
   target byte order.

   If the data are a string pointer, returns the number of string characters
   printed.

   If DEREF_REF is nonzero, then dereference references, otherwise just print
   them like pointers.

   The PRETTY parameter controls prettyprinting.  */

int
f_val_print (struct type *type,
	     char *valaddr,
	     int embedded_offset,
	     CORE_ADDR address,
	     struct ui_file *stream,
	     char format[],
	     int deref_ref,
	     int recurse,
	     enum val_prettyprint pretty)
{
  register unsigned int i = 0;	/* Number of characters printed */
  struct type *elttype;
  LONGEST val;
  CORE_ADDR addr;
  CHECK_TYPEDEF (type);
  extern CORE_ADDR stop_pc;
  CORE_ADDR prologue_begin, prologue_end; 
  boolean inside_prologue = 0;
 
  /* Are we stopped inside the prologue of some function? if yes, for Fortran
     strings do not try to get the length of the string from the stack by 
     calling f77_get_dynamic_length_of_aggregate (type); because it will be 
     some garbage value.
     For other types of paramteres check, can we access the memory location.
     If yes, then print the parameter value, it may be junk :(, depends on
     where we are stopped inside the prologue. */
      
  char buf[1];
  find_pc_partial_function (stop_pc, 0, &prologue_begin, 0);
  prologue_end = SKIP_PROLOGUE (prologue_begin); 
  if (stop_pc < prologue_end) 
   {
     /* Use this var for more fine grain level checking, if needed.. */
     inside_prologue = 1;
     /* The length check is required to distinguish between string paramter of
        a function and normal character, or normal string.
        eg- (gdb) print 'a' 
        eg- (gdb) print "abcd" */

     if (type->length == 0)
      {
       	printf_filtered ("<Uninitialized>\n");
        warning ("You have stopped inside prologue of the current function."
                 "\nThe function parameters may not be initialized yet."
                 "\nYou can print the parameters after going past the" 
                 "\nfunction prologue end at address %#x." 
                 , prologue_end);
        return 0;
      }
     
     else if (type->length != 0 && address != NULL 
              && (target_read_memory (address, buf, 1) != 0))
      {
       	printf_filtered ("<Uninitialized>\n");
        warning ("You have stopped inside prologue of the current function."
                 "\nThe function parameters may not be initialized yet."
                 "\nYou can print the parameters after going past the" 
                 "\nfunction prologue end at address %#x." 
                 , prologue_end);
        return 0;
      }
   }
  
  switch (TYPE_CODE (type))
    {
    case TYPE_CODE_STRING:
      f77_get_dynamic_length_of_aggregate (type);
      LA_PRINT_STRING (stream, valaddr+embedded_offset, TYPE_LENGTH (type), 1, 0);
      break;

    case TYPE_CODE_ARRAY:
      if (print_ftn_array_slice)
	{
          ftn_print_array_slice (type, valaddr+embedded_offset, address, stream, format,
		                 deref_ref, recurse, pretty);
	  print_ftn_array_slice = FALSE;
	}
      else
	{
          fprintf_filtered (stream, "(");
          f77_print_array (type, valaddr+embedded_offset, address, stream, format,
			   deref_ref, recurse, pretty);
          fprintf_filtered (stream, ")");
	}
      break;

    case TYPE_CODE_PTR:
      if (format[0] && format[0] != 's')
	{
          print_scalar_formatted (valaddr+embedded_offset, type, format,
				  0, stream);
	  break;
	}
      else
	{
          addr = unpack_pointer (type, valaddr+embedded_offset);
	  elttype = check_typedef (TYPE_TARGET_TYPE (type));

	  if (TYPE_CODE (elttype) == TYPE_CODE_FUNC)
	    {
	      /* Try to print what function it points to.  */
	      print_address_demangle (addr, stream, demangle);
	      /* Return value is irrelevant except for string pointers.  */
	      return 0;
	    }

	  if (addressprint && format[0] != 's')
	    fprintf_filtered (stream, "0x%s", paddr_nz (addr));

	  /* Return number of characters printed, plus one for the
	     terminating null if we have "reached the end".  */
	  return (i + (print_max && i != print_max));
	}

    case TYPE_CODE_UNION:
      if (recurse && !unionprint)
        {
          fprintf_filtered (stream, "{...}");
          break;
        }
      /* Fall through.  */

    case TYPE_CODE_STRUCT:
#ifdef LOG_BETA_F90_STRUCT_VAL
      /* Logs TYPE_CODE_UNION and TYPE_CODE_STRUCT */
      log_test_event (LOG_BETA_F90_STRUCT_VAL, 1);
#endif
      cp_print_value_fields (type, type, valaddr, embedded_offset, address,
                            stream, format, recurse, pretty, NULL, 0);
      break;

    case TYPE_CODE_FUNC:
      if (format[0])
	{
          print_scalar_formatted (valaddr+embedded_offset, type, format,
				  0, stream);
	  break;
	}
      /* FIXME, we should consider, at least for ANSI C language, eliminating
         the distinction made between FUNCs and POINTERs to FUNCs.  */
      fprintf_filtered (stream, "{");
      type_print (type, "", stream, -1);
      fprintf_filtered (stream, "} ");
      /* Try to print what function it points to, and its address.  */
      print_address_demangle (address, stream, demangle);
      break;

    case TYPE_CODE_INT:
      if (output_format)
        format[0] = format[0] ? format[0] : output_format;
      if (type == builtin_type_f_character)
        LA_PRINT_STRING (stream, valaddr+embedded_offset, 1, 1, 0);
      else if (format[0])
        print_scalar_formatted (valaddr+embedded_offset, type, format,
				0, stream);
      else
	{
          val_print_type_code_int (type, valaddr+embedded_offset, stream);
#ifndef GDB_TARGET_IS_HPUX
	  /* C and C++ has no single byte int type, char is used instead.
	     Since we don't know whether the value is really intended to
	     be used as an integer or a character, print the character
	     equivalent as well. */
	  if (TYPE_LENGTH (type) == 1)
	    {
	      fputs_filtered (" ", stream);
              LA_PRINT_CHAR ((unsigned char) unpack_long (type, valaddr+embedded_offset),
			     stream);
	    }
#endif
	}
      break;

    case TYPE_CODE_FLT:
      if (format[0])
        print_scalar_formatted (valaddr+embedded_offset, type, format,
				0, stream);
      else
        print_floating (valaddr+embedded_offset, type, stream, 0);
      break;

    /* QXCR1000745819: error displaying floating point regs. 
       Type of FP register is FLOATHPINTEL and hence converted
       to long double to handle it. */
    case TYPE_CODE_FLOATHPINTEL:
      {
        value_ptr long_double_val = allocate_value (builtin_type_long_double);
        REGISTER_CONVERT_TO_VIRTUAL (FR0_REGNUM,
	                             builtin_type_long_double, 
                                     valaddr+4, 
                                     VALUE_CONTENTS (long_double_val));
        print_formatted (long_double_val, format, 0,
                         stream);
      }
      break;

    case TYPE_CODE_VOID:
      fprintf_filtered (stream, "VOID");
      break;

    case TYPE_CODE_ERROR:
      fprintf_filtered (stream, "<error type>");
      break;

    case TYPE_CODE_RANGE:
      /* FIXME, we should not ever have to print one of these yet.  */
      fprintf_filtered (stream, "<range type>");
      break;

    case TYPE_CODE_BOOL:
      if (output_format)
        format[0] = format[0] ? format[0] : output_format;
      if (format[0])
        print_scalar_formatted (valaddr+embedded_offset, type, format,
	 			0, stream);
      else
	{
	  val = 0;
	  switch (TYPE_LENGTH (type))
	    {
	    case 1:
              val = unpack_long (builtin_type_f_logical_s1,
				 valaddr+embedded_offset);
	      break;

	    case 2:
              val = unpack_long (builtin_type_f_logical_s2,
				 valaddr+embedded_offset);
	      break;

	    case 4:
              val = unpack_long (builtin_type_f_logical,
				 valaddr+embedded_offset);
	      break;

	    default:
	      error ("Logicals of length %d bytes not supported",
		     TYPE_LENGTH (type));

	    }

	  if (val == 0)
	    fprintf_filtered (stream, ".FALSE.");
	  else
#ifdef GDB_TARGET_IS_HPUX
            /* HP debuggers print .TRUE. for any non-zero value */
            fprintf_filtered (stream, ".TRUE.");
#else
	  if (val == 1)
	    fprintf_filtered (stream, ".TRUE.");
	  else
	    /* Not a legitimate logical type, print as an integer.  */
	    {
	      /* Bash the type code temporarily.  */
	      TYPE_CODE (type) = TYPE_CODE_INT;
              f_val_print (type, valaddr+embedded_offset, 0, address, stream,
                           format, deref_ref, recurse, pretty);
	      /* Restore the type code so later uses work as intended. */
	      TYPE_CODE (type) = TYPE_CODE_BOOL;
	    }
#endif
	}
      break;

    case TYPE_CODE_COMPLEX:
      switch (TYPE_LENGTH (type))
	{
	case 8:
	  type = builtin_type_f_real;
	  break;
	case 16:
	  type = builtin_type_f_real_s8;
	  break;
	case 32:
	  type = builtin_type_f_real_s16;
	  break;
	default:
	  error ("Cannot print out complex*%d variables", TYPE_LENGTH (type));
	}
      fputs_filtered ("(", stream);
      print_floating (valaddr+embedded_offset, type, stream, 0);
      fputs_filtered (",", stream);
      print_floating (valaddr+embedded_offset + TYPE_LENGTH (type), type,
		      stream, 0);
      fputs_filtered (")", stream);
      break;

    case TYPE_CODE_UNDEF:
      /* This happens (without TYPE_FLAG_STUB set) on systems which don't use
         dbx xrefs (NO_DBX_XREFS in gcc) if a file has a "struct foo *bar"
         and no complete type for struct foo in that file.  */
      fprintf_filtered (stream, "<incomplete type>");
      break;

    case TYPE_CODE_REF:
      elttype = check_typedef (TYPE_TARGET_TYPE (type));
      if (TYPE_CODE (elttype) == TYPE_CODE_ARRAY)
         f77_get_dynamic_length_of_aggregate (elttype);
      if (addressprint)
        {
          fprintf_filtered (stream, "LOC (");
          print_address_numeric
            (extract_address (valaddr + embedded_offset,
                              TARGET_PTR_BIT / HOST_CHAR_BIT), 1, stream);
          fputs_filtered (")", stream);
          if (deref_ref)
            fputs_filtered (": ", stream);
        }
      /* De-reference the reference.  */
      if (deref_ref)
        {
          if (TYPE_CODE (elttype) != TYPE_CODE_UNDEF)
            {
              value_ptr deref_val =
                value_at
                  (TYPE_TARGET_TYPE (type),
                   unpack_pointer (lookup_pointer_type (builtin_type_void),
                                   valaddr + embedded_offset),
                   NULL);
              val_print(VALUE_TYPE (deref_val),
                        VALUE_CONTENTS (deref_val),
                        0,
                        VALUE_ADDRESS (deref_val),
                        stream,
                        format,
                        deref_ref,
                        recurse,
                        pretty);
            }
          else
            fputs_filtered ("???", stream);
        } 
      break;
      
    case TYPE_CODE_ARRAY_DESC:
      {
        /* This type is used to support the f90 array descriptor, a runtime
         * structure used for certain types of arrays and pointers.  See
         * hp-symtab-read.c for a more complete description of the runtime
         * structure and the gdb type created to support it.
         */
        struct type *target_type = TYPE_TARGET_TYPE (type);
        value_ptr value;
        CORE_ADDR original_addr = 0; /* initialize for compiler warning */
	unsigned char flag;      /*JAGae77839*/ 
	CORE_ADDR addr;   	 /*JAGae77839*/
   /*  JAGaf34053 - Pointer to an array */
#ifdef GDB_TARGET_IS_HPPA
        CORE_ADDR arr_addr;
        int status;
        char buf[sizeof (ULONGEST)];
#endif

#ifdef LOG_BETA_F90_ARRAY_VAL
        log_test_event (LOG_BETA_F90_ARRAY_VAL, 1);
#endif

        /* Check for uninitialized array descriptor */
#ifdef F_CHECK_ARRAY_DESC
       /*JAGae77839 - gdb shouldn't error out while printing 
	  info locals instead should print the error message and 
	  continue printing other local variables. So, removed 
	  F_CHECK_ARRAY_DESC call and inlined the function*/
#ifndef HP_IA64
	addr = TYPE_ARRAY_ADDR (target_type); 
#else
	addr = TYPE_ARRAY_ADDR (target_type) + 
			TYPE_ARRAY_DESC_ALLOCATED_OFFSET(type);
#endif
	if (!addr)
        {
            printf_filtered("Unable to access dynamic array address");
            break;
        }
   /*  JAGaf34053 - Pointer to an array */
#ifdef GDB_TARGET_IS_HPPA
        if (TYPE_ARRAY_IS_PTR(type) == 1)
	{
	    arr_addr = read_memory_unsigned_integer (addr, (TARGET_PTR_BIT/TARGET_CHAR_BIT));
	    status = target_read_memory (arr_addr, buf, 1);
	    if (status != 0)
	       {
	         printf_filtered ("Uninitialized dynamic array");
	         break;
	       }
         } 
#endif       
     
#ifndef HP_IA64
	flag = read_memory_unsigned_integer (addr +
				             2 * (TARGET_PTR_BIT/TARGET_CHAR_BIT) +
					     1, 1 /* byte */);
#else
	flag = read_memory_unsigned_integer (addr, 1 /* byte */);  
#endif
	if (!flag)
        {
            printf_filtered("Uninitialized dynamic array");
            break;
        }
	/*JAGae77839 - ends*/
#endif

        /* Make sure the length of the array is calculated */
        f77_get_dynamic_length_of_aggregate (target_type);

        /* Get the address of the actual array contents */
#ifndef HP_IA64
#ifdef F_GET_ARRAY_DESC_ARRAY_ADDR
        addr = F_GET_ARRAY_DESC_ARRAY_ADDR (TYPE_ARRAY_ADDR (target_type));
#endif
#else
#ifdef F_GET_ARRAY_DESC_ARRAY_ADDR
        addr = F_GET_ARRAY_DESC_ARRAY_ADDR (TYPE_ARRAY_ADDR (target_type),
			 (int) TYPE_ARRAY_DESC_BASE_ADDR_OFFSET (type));
#endif
#endif

        /* If stride is negative, may need to recalculate addr. */
        if (TYPE_ARRAY_STRIDE (target_type) < 0)
          {
            original_addr = addr;
#ifdef F_ADJ_ARRAY_ADDR_W_NEG_STRIDE
            addr = F_ADJ_ARRAY_ADDR_W_NEG_STRIDE (addr, target_type);
#endif
          }

        /* Dereference the array to get the contents 
         *
         * FIXME: we're reading in the contents of a section of the original
         * array specified by array descriptor, not just the elements 
         * described by it.  If the stride is large, we're reading in a 
         * lot more than we need to.
         */
        value = value_at (target_type, addr, NULL);
        valaddr = VALUE_CONTENTS (value);

        /* If stride is negative, may need to reset address 
           of inferior's contents (addr) and adjust address of 
           superior's contents (valaddr).
         */
        if (TYPE_ARRAY_STRIDE (target_type) < 0)
          {
            addr = original_addr;
#ifdef F_ADJ_ARRAY_VALADDR_W_NEG_STRIDE
            valaddr = F_ADJ_ARRAY_VALADDR_W_NEG_STRIDE (valaddr, target_type);
#endif
          }

        /* Finally, print it !
	   Ignore embedded_offset since we've already calculated
	   the correct valaddr */
        f_val_print (target_type, valaddr, /* embedded_offset = */ 0, 
                     addr, stream, format, deref_ref, recurse, pretty);
        break;
      }

    default:
      error ("Invalid Fortran type code %d in symbol table.",
	     TYPE_CODE (type));
    }
  
  /* We are inside the prologue but user have printed other than func parameter,
     eg- (gdb) print 'abcd' or (gdb) p 50 
     then, the address would be NULL, so do not print the below warning msg.
     similarly for the parameter address may be null depending
     on where we are inside the prologue.
     So, if the memory location at the address is accessible but we are inside
     the prologue then, print the below msg.
     
     If the addresss is null for the function parameter whose length is non null
     then, we do not come here, because the val_lazy would be true for the func
     param and we would try to fetch it in the record_latest_value() before  
     trying to print the value by coming here. */
  if (inside_prologue && address != NULL
      && (target_read_memory (address, buf, 1) == 0)) 
   {
     printf_filtered ("\n");
     warning ("You have stopped inside prologue of the current function."
              "\nThe function parameters may not be initialized yet."
              "\nYou can print the parameters after going past the" 
              "\nfunction prologue end at address %#x." 
              , prologue_end);
   }
  gdb_flush (stream);
  return 0;
}


int
f_value_printOrStrcat (char *buf,
		       int bufLen,
		       value_ptr val,
		       struct ui_file *stream,
		       char format[],
		       enum val_prettyprint pretty)
{
  struct type * type = VALUE_TYPE (val);

  /* If it is a pointer, indicate what it points to. */
  if (TYPE_CODE (type) == TYPE_CODE_PTR)
    {
      fputs_filtered ("(", stream);
      type_print (type, "", stream, -1);
      fputs_filtered (") ", stream);
    }
  return val_print (type, VALUE_CONTENTS_ALL (val),
                            VALUE_EMBEDDED_OFFSET (val), VALUE_ADDRESS (val),
                            stream, format, 1, 0, pretty);
}

int
f_value_print ( value_ptr val, struct ui_file *stream, char format[],
		enum val_prettyprint pretty)
{
  return f_value_printOrStrcat ((char *)NULL, 0, val, stream, format, pretty);
}

int
f_value_strcat ( char *buf, int bufLen, value_ptr val, char format[],
		 enum val_prettyprint pretty)
{
  return f_value_printOrStrcat (buf, bufLen, val, (struct ui_file *)NULL, format, pretty);
}

static void
list_all_visible_commons (char *funname)
{
  SAVED_F77_COMMON_PTR tmp;

  tmp = head_common_list;

  printf_filtered ("All COMMON blocks visible at this level:\n\n");

  while (tmp != NULL)
    {
      if (STREQ (tmp->owning_function, funname))
	printf_filtered ("%s\n", tmp->name);

      tmp = tmp->next;
    }
}

/* This function is used to print out the values in a given COMMON 
   block. It will always use the most local common block of the 
   given name */

static void
info_common_command (char *comname, int from_tty)
{
  SAVED_F77_COMMON_PTR the_common;
  COMMON_ENTRY_PTR entry;
  struct frame_info *fi;
  register char *funname = 0;
  struct symbol *func;

  /* We have been told to display the contents of F77 COMMON 
     block supposedly visible in this function.  Let us 
     first make sure that it is visible and if so, let 
     us display its contents */

  fi = selected_frame;

  if (fi == NULL)
    error ("No frame selected");

  /* The following is generally ripped off from stack.c's routine 
     print_frame_info() */

  func = find_pc_function (fi->pc);
  if (func)
    {
      /* In certain pathological cases, the symtabs give the wrong
         function (when we are in the first function in a file which
         is compiled without debugging symbols, the previous function
         is compiled with debugging symbols, and the "foo.o" symbol
         that is supposed to tell us where the file with debugging symbols
         ends has been truncated by ar because it is longer than 15
         characters).

         So look in the minimal symbol tables as well, and if it comes
         up with a larger address for the function use that instead.
         I don't think this can ever cause any problems; there shouldn't
         be any minimal symbols in the middle of a function.
         FIXME:  (Not necessarily true.  What about text labels) */

      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);

      if (msymbol != NULL
	  && (SYMBOL_VALUE_ADDRESS (msymbol)
	      > BLOCK_START (SYMBOL_BLOCK_VALUE (func))))
	funname = SYMBOL_NAME (msymbol);
      else
	funname = SYMBOL_NAME (func);
    }
  else
    {
      register struct minimal_symbol *msymbol =
      lookup_minimal_symbol_by_pc (fi->pc);

      if (msymbol != NULL)
	funname = SYMBOL_NAME (msymbol);
    }

  /* If comname is NULL, we assume the user wishes to see the 
     which COMMON blocks are visible here and then return */

  if (comname == 0)
    {
      list_all_visible_commons (funname);
      return;
    }

  the_common = find_common_for_function (comname, funname);

  if (the_common)
    {
      if (STREQ (comname, BLANK_COMMON_NAME_LOCAL))
	printf_filtered ("Contents of blank COMMON block:\n");
      else
	printf_filtered ("Contents of COMMON block '%s':\n", comname);

      printf_filtered ("\n");
      entry = the_common->entries;

      while (entry != NULL)
	{
	  printf_filtered ("%s = ", SYMBOL_NAME (entry->symbol));
	  print_variable_value (entry->symbol, fi, gdb_stdout);
	  printf_filtered ("\n");
	  entry = entry->next;
	}
    }
  else
    printf_filtered ("Cannot locate the common block %s in function '%s'\n",
		     comname, funname);
}


/* Actual function which prints a Fortran array slice */

static void
ftn_print_array_slice_1 (int ndimensions, struct type *type,
			 char *valaddr, CORE_ADDR address,
			 struct ui_file *stream,
			 char format[], int deref_ref, int recurse,
			 enum val_prettyprint pretty)
{
   int i,j;

   if (!dont_print_value)
     {
       /* skip the "type" that describes an array type. we only need 
          the "type" describing the array element. */
       if ( (TYPE_CODE (TYPE_TARGET_TYPE (type) ) == TYPE_CODE_ARRAY) )
         {
           ftn_print_array_slice_1 (ndimensions, TYPE_TARGET_TYPE(type),
				    valaddr, address, stream, format,
			            deref_ref, recurse, pretty);
         }
       else
	 {
	   for (i=1; i<=offset_tbl->nelms; i++)
	     {    
	       fprintf_filtered (stream, "(");
	       for (j=1; j<=ndimensions; j++)
	         {
	           int k = (int) (FTN_SLICE_VECTOR(slice_vector, i, j) + lower_bound[j]);
	           printf_filtered ((j!=ndimensions) ? "%d," : "%d) = ", k);
	         }

	       val_print (TYPE_TARGET_TYPE (type),
		          valaddr + FTN_OFFSET_TBL_ELM(offset_tbl, i),
		          0,
		          address + FTN_OFFSET_TBL_ELM(offset_tbl,i),
		          stream, format, deref_ref, recurse, pretty);
	       fprintf_filtered (stream, "\n");
	     }
	 }
     }
}

/* This function gets called to print an Fortran slice array */

static void
ftn_print_array_slice (struct type *type, char *valaddr, CORE_ADDR address,
		       struct ui_file *stream,
		       char format[],
		       int deref_ref,
		       int recurse,
		       enum val_prettyprint pretty)
{
   int ndimensions;

   ndimensions = calc_f77_array_dims (type);

   if (ndimensions > MAX_FORTRAN_DIMS || ndimensions < 0)
     error ("Type node corrupt! F77 arrays cannot have %d subscripts (%d Max)",
            ndimensions, MAX_FORTRAN_DIMS);

   ftn_print_array_slice_1 (ndimensions, type, valaddr, address, stream, format,
			    deref_ref, recurse, pretty);

   FTN_FREE_SLICE_VECTOR(slice_vector);
   FTN_FREE_OFFSET_TBL(offset_tbl);
}

void
_initialize_f_valprint ()
{
  add_info ("common", info_common_command,
	    "Print out the values contained in a Fortran COMMON block.\n\nUsage:"
            "\n\tinfo common [<BLK>]\nIf block-name is not given as argument "
            "then display info of all the common-blocks visible\nelse display "
            "info only for specified block.\n");
  if (xdb_commands)
    add_com ("lc", class_info, info_common_command,
	     "Print out the values contained in a Fortran COMMON block.");
}
