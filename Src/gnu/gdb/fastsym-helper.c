/* Helper file and interface to gdb for new symbol storage model. 

   Copyright 1994, 1995, 1996, 1997, 2001 Free Software Foundation, Inc.
   
   Written by Poorva Gupta (poorva@cup.hp.com)

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


#include <ctype.h>
#include <assert.h>
#include "fastsym.h"
#include "defs.h"
#include "gdbtypes.h"
#include "gdb_string.h"
#include "symtab.h"
#include "bfd.h"
#include "symfile.h"
#include "objfiles.h"
#include "demangle.h"
#include "gdb-stabs.h"
#include "target.h"         

/* These are prefixes to an anonymous namespace's mangled name
 * ACC_ANON_PREFIX_LENGTH  is GLOBAL_N_ 
 * GCC_ANON_PREFIX_LENGTH  is GLOBAL_
 */
#define ACC_ANON_PREFIX_LENGTH 9
#define GCC_ANON_PREFIX_LENGTH 7
extern char *add_string(char *);

#ifdef HP_IA64
extern CORE_ADDR inferior_aries_text_start;
extern CORE_ADDR inferior_aries_text_end;
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
#endif

/* This allocates space on the obstack and is used to allocate space for the 
 * the Judy arrays, the demangled bucket, synonym chain etc.
 */

void *
current_pst_obstack_alloc (long size)
{
  if (!current_objfile)
    error ("No current objfile found");
  
  return obstack_alloc (&current_objfile->psymbol_obstack, size);
}

/* We keep a global buffer for names sent to Judy.
 * This a buffer of large size and gets reallocated when more is
 * needed. Used for extract_hash_key and to iterate over the Judy array.
 * Saves time in that we don't need to malloc everytime.
 */
#define EIGHTK 8192
int string_size = EIGHTK; /* Largest size required. Also STRING_SIZE */
static char* local_long_name; /* longest string buffer */
static int current_string_size = EIGHTK; /* size of the currently allocated
                                           local_long_name */

/* Returns a pointer to the local_long_name */
/* Bindu: The consumers should always call this with the total size of the
   string they want to have in this to make sure they have enough room. */
char *
long_name (int size)
{
  int adjust_size;
  if (size > STRING_SIZE)
    adjust_size = size;
  else
    adjust_size = STRING_SIZE;

  if (adjust_size > current_string_size)
    {
      /* Reallocate in increments of 8k */
      current_string_size = ((adjust_size + EIGHTK - 1)/EIGHTK)*EIGHTK;
      if (local_long_name)
        {
          /* If a local_long_name is already allocated, realloc now. */
          local_long_name =
            (char *) xrealloc (local_long_name, current_string_size);
        }
    }
  /* If the long_name is not yet been allocated, allocate now. */
  if (!local_long_name)
    {
      local_long_name =
        (char *) xmalloc (current_string_size);
    }
 
  STRING_SIZE = current_string_size; 
  return local_long_name;
}

/* Check to see if this minsym's pc matches the pc passed in */
int
check_pc (void *minsym, void *addr)
{
  struct minimal_symbol *msymbol = (struct minimal_symbol *) minsym;
  CORE_ADDR pc = *(CORE_ADDR *) addr;

  if (msymbol)
    {
      if (SYMBOL_VALUE_ADDRESS (msymbol) == pc)
	return 1;
    }
  return 0;
}

/* Check to see if this mpsym's namespace matches the namespace passed in */

int
check_namespace (void *minsym, int namesp)
{
  struct minimal_symbol *msymbol = (struct minimal_symbol *) minsym;
  if (msymbol)
    {
      if (PSYMBOL_NAMESPACE (msymbol) == (namespace_enum) namesp)
	return 1;
    }
  return 0;
}

/* Allocate memory on the psymbol_obstack for the mpsym passed in
 * Also allocate memory for it's name, demangled name if present 
 * and fill in the psymtab that it belongs to. 
 * If dies_length and type_length are not NULL then use them and
 * allocate and fill in the elem_dups structure attached to the mpsym.
 * It returns the partial_symbol it allocated and filled in. 
 */

void *
alloc_mpsym (void *value, 
	     enum dups_enum is_duplicate, char *dem_name,
	     int dies_length, int type_length)
{
  struct partial_symbol *psym, *psym_arg;
  struct objfile *objfile = current_objfile;
  int dem_namelength = 0;
  struct partial_symtab *pst = NULL;
  enum language language;
  unsigned int hash_val;
  unsigned int str_length;

  if (!objfile)
    error ("No current objfile found");

  psym_arg = (struct partial_symbol *) value;
  psym =   (struct partial_symbol *) (void *)
    obstack_alloc (&objfile->psymbol_obstack, 
		   sizeof (struct partial_symbol));
  
  *psym = *psym_arg;

  if (is_duplicate)
    {
      pst = objfile->current_pst;
      
      SYMBOL_NAME (psym) = add_string(SYMBOL_NAME (psym_arg));
      if (dem_name)
	{
	  dem_namelength = (int) strlen (dem_name);
	  language = SYMBOL_LANGUAGE (psym);
	  switch (language)
	    {
	      case language_c:
	      case language_cplus:
		SYMBOL_CPLUS_DEMANGLED_NAME (psym) = (char *)
		  obstack_alloc(&objfile->psymbol_obstack,
				  dem_namelength + 1);
		strcpy (SYMBOL_CPLUS_DEMANGLED_NAME (psym), dem_name);
		break;
	      case language_chill:
		SYMBOL_CHILL_DEMANGLED_NAME (psym) = (char *)
		    obstack_alloc (&objfile->psymbol_obstack,
				   dem_namelength + 1);
		strcpy (SYMBOL_CHILL_DEMANGLED_NAME (psym), dem_name);
		break;
	      case language_fortran:
		SYMBOL_FORTRAN_DEMANGLED_NAME (psym) = (char *)
		    obstack_alloc(&objfile->psymbol_obstack,
				  dem_namelength + 1);
		strcpy (SYMBOL_FORTRAN_DEMANGLED_NAME (psym), dem_name);
	      default:
		break;
	    }
        }

      if (dies_length || type_length)
        {
          /* Need to alloc space for elem_dups struct */
          (psym)->elem_dups = (struct elem_dups *)(void *) obstack_alloc
            (&objfile->psymbol_obstack, sizeof (struct elem_dups));
          (psym)->elem_dups->dies_length = dies_length;
          (psym)->elem_dups->type_length = type_length;
        }
    }
  psym->pst = pst;
  
  return (void *) psym;
}

/* Add the value given to the synonym_chain (arg_list). Also 
 * increment the syncount for that bucket. Return the new link that
 * you created.
 */


void * 
add_to_syn_chain (void *value, void **arg_list, int *count)
{
  struct objfile *objfile = current_objfile;
  struct msym_list *new_link = NULL;
  struct msym_list *list = *(struct msym_list **) arg_list;

  if (!objfile) 
    error ("No current objfile found");

  new_link = (struct msym_list *)(void *)
    obstack_alloc (&objfile->psymbol_obstack, sizeof (struct msym_list));
  new_link->msym = (struct minimal_symbol *) value;
  
  new_link->next = list;
  *arg_list = new_link;
  (*count) = (*count) + 1;
  if ((*count) > max_synonyms_seen)
     max_synonyms_seen = *count;
  return new_link;
}


/* value - is the new one mpsym to be added.
 * value_old is the one already at that position. 
 * 
 * -- they share the name but are not true duplicates of
 *    each other e.g struct foo foo where the 2 foos are
 *    different but have the same name or they could be
 *    static functions in different files.
 *    We then add the second one to the synonym chain
 * 
 * -- One is a psym and the previous one is the corresponding msym.
 *    In this case we want to add the information about the psym
 *    to the msym esp which psymtab did it come from.
 * 
 */

void
add_to_pst_list (void *value, void *value_old, void **arg_list,
		 int *count,
		 int dies_length, 
		 int type_length)
{
  struct minimal_symbol *psym_new, *psym_old, *psym = NULL;
  struct objfile *objfile = current_objfile;
  CORE_ADDR addr;
  struct msym_list *new_link;
  struct partial_symtab *pst = NULL;
  
  if (!objfile) 
    error ("No current objfile found");

  /* Always prefer minimal_symbol to partial_symbol since the msym has the
     linker type and the psym has padding instead of it */

  psym_new = (struct minimal_symbol *) value;
  psym_old = value_old ? ((struct minimal_symbol *) value_old) : NULL;

  pst = objfile->current_pst;

  /* If the previous one at this position did not have a pst 
   * it came from the minimal symbol table. If the current one has 
   * the same address as the older one then add the extra information 
   * present in the new one to the older one. It means that we have the
   * partial symbol corresponding the minimal symbol present in the
   * table. 
   */

  if (psym_old 
      && ((psym_old == psym_new) 
	  || (swizzle (SYMBOL_VALUE_ADDRESS (psym_old)) 
	      == swizzle (SYMBOL_VALUE_ADDRESS (psym_new)))))
    {
      if (psym_old->pst == NULL)
	{
	  /* The previous sym was most probably a minimal symbol - we 
	   * now have more information since we are a partial_symbol hence 
	   * change the previous sym's fields to this psyms 
	   */
	  
	  SYMBOL_LANGUAGE (psym_old) = SYMBOL_LANGUAGE (psym_new);
	  PSYMBOL_NAMESPACE (psym_old) = PSYMBOL_NAMESPACE (psym_new);
	  PSYMBOL_CLASS (psym_old) = PSYMBOL_CLASS (psym_new);
	  SYMBOL_INIT_LANGUAGE_SPECIFIC (psym_old, SYMBOL_LANGUAGE (psym_new));
	  MSYMBOL_IS_GLOBAL (psym_old) = MSYMBOL_IS_GLOBAL (psym_new);
	  MSYMBOL_TYPE_CODE (psym_old) = MSYMBOL_TYPE_CODE (psym_new);
	  MSYMBOL_IS_DECL (psym_old) = MSYMBOL_IS_DECL (psym_new);
	}
      psym = psym_old;
    }
  
  /* If psym is still NULL we need to add the new one to the synonym chain. 
   * We don't have a duplicate but just something that shares a name
   */

  if (!psym) 
  {
    new_link = (struct msym_list *) add_to_syn_chain (value, arg_list, count);
    psym = (struct minimal_symbol *) 
      ((struct msym_list *) new_link)->msym;
  }

  psym->pst = pst;
  if ((dies_length || type_length) && psym->elem_dups == NULL)
    {
      /* Need to alloc space for elem_dups struct */
      psym->elem_dups = (struct elem_dups *)(void *) obstack_alloc
	(&objfile->psymbol_obstack, sizeof (struct elem_dups));
      psym->elem_dups->dies_length = dies_length;
      psym->elem_dups->type_length = type_length;
    }    
}

/* Allocates space for, demangles the name and sets the demangled 
 * name of the minsymbol
 */

void
set_demangled_name (void *value)
{
  char *dem_name = NULL;
  struct minimal_symbol *minsym = (struct minimal_symbol *) value;

  if (is_cplus_name (SYMBOL_NAME (minsym), 1))  /* JAGaf49121 */
    dem_name = cplus_demangled_name;

  if (dem_name) 
    {
      SYMBOL_INIT_LANGUAGE_SPECIFIC (minsym, language_cplus);
      if (current_objfile)
	{
	  SYMBOL_CPLUS_DEMANGLED_NAME (minsym) = obsavestring (dem_name, 
					    (int) strlen (dem_name), 
			  (&current_objfile->psymbol_obstack));
	  free (dem_name);                                        
	}
      else
	SYMBOL_CPLUS_DEMANGLED_NAME (minsym) = dem_name;
    }
  else
    {
      if (SYMBOL_LANGUAGE (minsym) != language_c)
	{
	  SYMBOL_INIT_LANGUAGE_SPECIFIC (minsym, SYMBOL_LANGUAGE (minsym));
	  SYMBOL_INIT_DEMANGLED_NAME (minsym, 
				      (&current_objfile->psymbol_obstack));
	}
    }
}


/* is_dup returns :
   NOT_DUP if there isn't a duplicate 
   DUP they have the same name but are not true duplicates (e.g., static funcs)
   TRUE_DUP if it is a true duplicate 
*/

enum dups_enum 
is_dup (void *value1, void *value2, int dies_length, int type_length)
{
  struct minimal_symbol *minsym, *ins_minsym;
  CORE_ADDR newaddr = 0, oldaddr = 0;

  minsym = (struct minimal_symbol *) value1;
  ins_minsym = (struct minimal_symbol *) value2;

  /* If we have the same pointers which we will have for DOOM 
     since we build our psymbols from the msymbols
     */
  if (minsym == ins_minsym)
    return DUP;

  oldaddr = SYMBOL_VALUE_ADDRESS (minsym);
  newaddr = SYMBOL_VALUE_ADDRESS (ins_minsym);

  /* Seems like this check before calling swizzle gets us 9% improvement */
  if (is_swizzled) 
    {
      oldaddr = SWIZZLE (oldaddr);
      newaddr = SWIZZLE (newaddr);
    }

  /* Base Types (int, char etc), 
     User defined types (structs, classes) etc,
     typedefs, 
     enumerators do not have addresses */

  if (oldaddr && newaddr)
    {
      if (oldaddr == newaddr)
	{
	  /* If they have the same address but the old one does not
	   * have a psymtab associated with it then the new one is
	   * probably the psymbol and the old one is the msymbol.
	   * Need to return that the new one is a DUP of the old one
	   * but not a TRUE_DUP so that add_to_pst_list will be called.
	   */

	  if (minsym->pst)
	    return TRUE_DUP;
	  else
	    return DUP;
	}
      return NOT_DUP;
    }
  
  /* Only if the already inserted sym has a list of psymtabs attached 
   * to it check the namespace - since it means that the minsym is
   * a partial symbol. The namespace makes sense only in psyms - minsyms 
   * have namespace set to UNDEF_NAMESPACE.
   */
  if (minsym->pst && current_objfile->current_pst)
    if (PSYMBOL_NAMESPACE (ins_minsym) != PSYMBOL_NAMESPACE (minsym))
      return NOT_DUP;

  if ((MSYMBOL_TYPE_CODE(minsym) == TYPE_CODE_UNDEF) ||
      (MSYMBOL_TYPE_CODE(ins_minsym) == TYPE_CODE_UNDEF))
    {
      /* If one's a static and one's a global they can't be dups of each other.
       * 
       * If both are global check to see that there namespace and address
       * class match too so we don't return TRUE_DUP for the case of
       * struct foo foo - where one is VAR_NAMESPACE and the other is
       * STRUCT_NAMESPACE
       * 
       * If neither of the above then they are dups of each other. 
       *
       */
      if (MSYMBOL_IS_GLOBAL(minsym) != MSYMBOL_IS_GLOBAL(ins_minsym))
	return NOT_DUP;      
      else if (((MSYMBOL_IS_GLOBAL(minsym) == TYPE_GLOBAL) 
		&& (MSYMBOL_IS_GLOBAL(ins_minsym) == TYPE_GLOBAL))
	     && (PSYMBOL_NAMESPACE (ins_minsym) == PSYMBOL_NAMESPACE (minsym))
	     && (PSYMBOL_CLASS(ins_minsym) == PSYMBOL_CLASS(minsym)))
	return TRUE_DUP;
      else 
	return DUP;
    }
  
  /* If the type_codes and whether they are global/static 
   * differs then we do not need to check further.
   */
  
  if (MSYMBOL_TYPE_CODE(minsym) != MSYMBOL_TYPE_CODE(ins_minsym))
    return NOT_DUP;
  
  if (MSYMBOL_IS_GLOBAL(minsym) != MSYMBOL_IS_GLOBAL(ins_minsym))    
    return NOT_DUP;
  
  /* Else check to see if they are user defined types in which case
   * we need to check the elem_dups structure to make sure that 
   * they really are dups / true dups of each other. 
   */

  if (MSYMBOL_TYPE_CODE(ins_minsym) == TYPE_CODE_CLASS 
      || MSYMBOL_TYPE_CODE(ins_minsym) == TYPE_CODE_STRUCT
      || MSYMBOL_TYPE_CODE(ins_minsym) == TYPE_CODE_UNION
      || MSYMBOL_TYPE_CODE(ins_minsym) == TYPE_CODE_ENUM)
    {
      if (MSYMBOL_NUM_FIELDS (minsym) == dies_length
	  && MSYMBOL_TYPE_LENGTH (minsym) == type_length)
	{
	  if (!MSYMBOL_IS_DECL (ins_minsym) && MSYMBOL_IS_DECL (minsym))
	    return DUP;
	  else if (MSYMBOL_IS_DECL (ins_minsym) ==  MSYMBOL_IS_DECL (minsym))
	    return TRUE_DUP;
	  else if (MSYMBOL_IS_DECL (ins_minsym) && !MSYMBOL_IS_DECL (minsym))
	    return TRUE_DUP;
	  else 
	    return TRUE_DUP;
	}
      else 
	return NOT_DUP;
    }

  /* This should come after the above check else we will return for 
     classes too without making at least a sanity check */
  if (MSYMBOL_TYPE_CODE(minsym) == MSYMBOL_TYPE_CODE(ins_minsym))
    {
      if (MSYMBOL_IS_GLOBAL(minsym) == TYPE_GLOBAL
	  && MSYMBOL_IS_GLOBAL(ins_minsym) == TYPE_GLOBAL)
	return TRUE_DUP; /* true duplicate */
      else if (MSYMBOL_IS_GLOBAL(ins_minsym) == TYPE_STATIC
	       && MSYMBOL_IS_GLOBAL(minsym) == TYPE_GLOBAL)
	return NOT_DUP; /* not a duplicate */
      else if (MSYMBOL_IS_GLOBAL(ins_minsym) == TYPE_STATIC
	       && MSYMBOL_IS_GLOBAL(minsym) == TYPE_STATIC)
	return DUP;  /* duplicate but not true */	       
      else
	return NOT_DUP; /* not a duplicate */
    }

  return NOT_DUP;
}

/* Used to sort the demangled chain and since we know we have
 * complete names from the demangler and we will be comparing names 
 * from the demangler against each other and not comparing user given 
 * names against demangler names we can use strcmp which is much faster 
 * and not the ignore whitespace and templates version 
 */

int
compare_minimal_symbols_by_demangled_name (const void *msym1,
					   const void *msym2)
{

  struct minimal_symbol *minsym1 = *(struct minimal_symbol **) msym1;
  struct minimal_symbol *minsym2 = *(struct minimal_symbol **) msym2;

  char *name1 = SYMBOL_SOURCE_NAME (minsym1);
  char *name2 = SYMBOL_SOURCE_NAME (minsym2);

  return strcmp (name1, name2);
}

/* Used to bsearch the demangled chain. Since here we are comparing a
 * a user given name against one from the demangler we must ignore 
 * whitespaces etc so we use strcmp_iwt. 
 */

int
compare_name_and_minimal_symbols_demangled_name (const void *msym_name1,
					         const void *msym2)
{
  
  struct minimal_symbol *m2 = *(struct minimal_symbol **) msym2;
  int ret = 0;

  char *name1 = (char *) msym_name1;
  char *name2 = SYMBOL_SOURCE_NAME (m2);

  return strcmp_iwt_void (name2, name1);
}

/* Used to find the category the mpsym belongs to. global,
 * static, trampoline etc.
 * key is the name we are supposed to look for.
 * namesp is the namespace we want to look for i.e VAR_NAMESPACE etc.
 * mst_sought is the type of the msymbol sought.
 * not_decl tells us whether it is a declaration or not. 
 * found_symbol is filled in if we find a global symbol.
 * found_file_symbol is filled in if we find a static.
 * trampoline symbol is filled in if we find a trampoline.
 *
 * close_match is filled in if we have been asked to compare the 
 * namespace of a symbol (VAR_NAMESPACE, STRUCT_NAMESPACE etc) 
 * and find that the partial_symbols namespace does not match. 
 * 
 */

static void
find_sym (struct minimal_symbol *msymbol,
          char *key,
          int len,
          int namesp,
          int mst_sought,
          int not_decl,
          struct minimal_symbol **found_symbol,
          struct minimal_symbol **found_file_symbol,
          struct minimal_symbol **trampoline_symbol,
          struct minimal_symbol **close_match)
{      
  /* Since this routine has been invoked we know that
     the key can't be a mangled name and also that the 
     demangled name has been filled in */
    
  if (namesp && (PSYMBOL_NAMESPACE (msymbol) != namesp))
    {
      if (*close_match)
	{
	  if (namesp == VAR_NAMESPACE 
	      && (PSYMBOL_NAMESPACE (msymbol) == UNDEF_NAMESPACE))
            {
	      *close_match = msymbol;
#ifdef HP_IA64
              /* For the mixed mode PA32 lib, the msymbol namespace remains
                 UNDEF_NAMESPACE. Hence the found_symbol was not getting set.
                 This causes further searches to happen down the line and
                 sometimes resulted in an incorrect match to the following
                 stub. Fix this by setting found_symbol also based on some
                 checks for the mixed mode PA32 case. */
              if (   inferior_aries_text_start
                  && inferior_aries_text_end && is_swizzled)
                {
                  if (in_mixed_mode_pa_lib
                       (SYMBOL_VALUE_ADDRESS (msymbol), NULL))
                    {
                      *found_symbol = msymbol;
                    }
                }
#endif
            }
	  else if (namesp == STRUCT_NAMESPACE 
		   && (PSYMBOL_NAMESPACE (msymbol) == VAR_OR_STRUCT_NAMESPACE))
	    *close_match = msymbol;
	  return;
	}
      else
        {
	  *close_match = msymbol;
#ifdef HP_IA64
          if (  inferior_aries_text_start
              && inferior_aries_text_end && is_swizzled)
            {
              if (in_mixed_mode_pa_lib
                    (SYMBOL_VALUE_ADDRESS (msymbol), NULL))
                {
                  *found_symbol = msymbol;
                }
            }
#endif
         }
      return;
    }

  
  /* The symbol_matches_name macro tests first the name and then
     the demangled name to find if there is a match 
     We could also use this below instead
     if (strncmp (SYMBOL_DEMANGLED_NAME (msymbol), key, len))
     */
  
  if (SYMBOL_MATCHES_NAME (msymbol, key))
    {
      switch (MSYMBOL_TYPE (msymbol))
	{
	case mst_file_data:
	case mst_file_bss:
	  
	  if (mst_sought == mst_text)
	    break;
	  /* else fall through ... */
	  
	case mst_file_text:
	  if (mst_sought == mst_solib_trampoline)
	    break;
	  *found_file_symbol = msymbol;
	  break;
	  
	case mst_solib_trampoline:
	  /* if we are specifically searching for a trampoline
	     symbol, we are done. Otherwise, we will continue
	     to look for the real symbol. If none is found, we 
	     will use the trampoline if appropriate.
	     */
	  
	  if (mst_sought == mst_solib_trampoline)
	    *found_symbol = msymbol;
	  else if (mst_sought != mst_text)
	    if (*trampoline_symbol == NULL && target_has_stack)
	      *trampoline_symbol = msymbol;
	  break;
	      
	case mst_text:
	  if (mst_sought != mst_solib_trampoline)
	    *found_symbol = msymbol;
	  break;

	case mst_unknown:
	default:

	  /* If we don't want to look for declarations/opaque types */
	  /* If the msymbol does not have a pst put in close match instead of
	     giving it a real place in found_symbol etc. This will help to 
	     prefer an msymbol with a pst over one without. */
	  
	  if (not_decl == 1)
	    {
	      if (!MSYMBOL_IS_DECL (msymbol))
		{
		  if (!msymbol->pst)
		    *close_match = msymbol;		    
		  else
		    *found_symbol = msymbol;
		}
	      else
		break;
	    }
	  
	  if (mst_sought != mst_solib_trampoline &&
	      mst_sought != mst_text)
	    {
	      if (!msymbol->pst)
		*close_match = msymbol;
	      else if (MSYMBOL_IS_GLOBAL(msymbol) == TYPE_STATIC)
		*found_file_symbol = msymbol;
	      else
		*found_symbol = msymbol;
	    }
#ifdef HP_IA64
          /* jini: Sep 08: QXCR1000863944. */
          /* We don't seem to be dealing with mst_tls and mst_file_tls. For the 'aries_tp' symbol,
             the msymbol type would be either of these.  */
	  if ((   MSYMBOL_TYPE (msymbol) == mst_tls
               || MSYMBOL_TYPE (msymbol) == mst_file_tls)
              && mst_sought == mst_text
              && inferior_aries_text_start
              && inferior_aries_text_end
              && key[0] == 'a' && (strcmp (key, "aries_tp") == 0))
            {
	      if (!msymbol->pst)
		*close_match = msymbol;
	      else if (MSYMBOL_IS_GLOBAL(msymbol) == TYPE_STATIC)
		*found_file_symbol = msymbol;
	      else
		*found_symbol = msymbol;
            }
#endif
	  break;
	}
    }
  return;
} /* End of find_sym */

/* 
 * Used to iterate over the demangled chain and retrieve a matching 
 * entry from it. Once we finish iterating over the demangled chain 
 * we check the synonym_chain for a match
 *
 * key is the name we are supposed to look for.
 * ptr is the demangled chain of the relevant array holder judy arrray
 * namesp is the namespace we want to look for i.e VAR_NAMESPACE etc.
 * mst_sought is the type of the msymbol sought.
 * not_decl tells us whether it is a declaration or not. 
 * syn_chain is the synonym chain that is associated with the 1st level 
 * array
 * ptr_count is the number of elements in this array.
 * msym is the msymbol found in the demangled chain not necessarily the
 * first. 
 * found_type is what we fill in depending on the type we find i.e GLOBAL,
 * STATIC or trampoline.
 * 
 * Returns the matching mpsym found
 */

void *
find_best_match (void **msym,
                 char *key,
                 void **ptr,
                 void *syn_chain,
                 int namesp,
                 int mst_sought,
                 int *found_type,
                 int ptr_count,
                 int not_decl)
{
  struct minimal_symbol *found_symbol = NULL;
  struct minimal_symbol *found_file_symbol = NULL;
  struct minimal_symbol *trampoline_symbol = NULL;
  struct minimal_symbol *close_match = NULL;
  struct minimal_symbol **minsym, **ptr_list, **ptr_end;
  struct minimal_symbol *msymbol = NULL;
  struct msym_list *list;
  int len = 0;

  minsym = (struct minimal_symbol **) msym;
  ptr_list = (struct minimal_symbol **) ptr;
  ptr_end = ptr_list + ptr_count;

  /* Obtain a pointer to the first matching entry */
  len = (int) strlen (key);
  while (minsym > ptr_list)
    if (!strncmp (SYMBOL_SOURCE_NAME (*(minsym - 1)), key, len))
      minsym--;
    else
      break;
  
  /* Make sure not to go over the end of the list */

  /* Remember you could be handling a mix of psyms and minsyms */
  for (;(minsym < ptr_end) && *minsym && found_symbol == NULL; minsym++)
    {
      msymbol = *minsym;
      if (!msymbol)
	return NULL;

      if (!SYMBOL_MATCHES_NAME(msymbol, key))
	break;

      find_sym (msymbol, key, len, namesp, mst_sought, not_decl,
		&found_symbol, &found_file_symbol, 
		&trampoline_symbol, &close_match);
    }

 if (syn_chain)
   {
     list = (struct msym_list *) syn_chain;
     while (list && found_symbol == NULL)
       {
	 msymbol = list->msym;

	 if (SYMBOL_MATCHES_NAME(msymbol, key))
	   find_sym (msymbol, key, len, namesp, mst_sought, 
		     not_decl, &found_symbol, &found_file_symbol, 
		     &trampoline_symbol, &close_match);
	 list = list->next;
       }
   }

  
  /* External symbols are best.  */
  if (found_symbol)
    {
      *found_type = GLOB;
      return (void *) found_symbol;
    }

  /* File-local symbols are next best.  */
  if (found_file_symbol)
    {
      *found_type = STAT;
      return (void *) found_file_symbol;
    }

  /* Symbols for shared library trampolines are next best.  */
  if (trampoline_symbol)
    {
      *found_type = TRAMP;
      return (void *) trampoline_symbol;
    }
  
  if (close_match)
    {
      *found_type = CL_MTCH;
      return (void *) close_match;
    }

  return NULL;
}

/* Find all the mpsyms matching this name
 * key is the name we are supposed to look for.
 * ptr is the list we have been looking in to find the msym.
 * ptr_count is the number of elements in this array.
 * Either action or minsym_and_obj_arr will be present. 
 * action is a function pointer and we need to perform this action
 * on it.
 * If action is NULL then we need to place it in the array. 
 * minsym_count is the index at which we need to place it in the 
 * minsym_and_obj array
 * 
 * Since this function is called in a loop for all objfiles and we 
 * could have filled in the minsym_and_obj_arr from other objfiles 
 * we need to know the index at which we need to start filling things 
 * in for this objfile.
 *
 * look_for_all if true (1) means that we need to look from the 
 * beginning of the bucket and for all matching patterns. It is used for 
 * things like break Foo::bar where Foo is a templated class
 * Reason for this is we 
 * Foo<char> , Foo<char>::bar, Foo<char>::baz, 
 * Foo<int>, Foo<int>::bar, Foo<int>::baz
 * all make it to the same bukcet with the new C++ abi demangling scheme.
 * They get sorted in the order above because we use strcmp. 
 * An alternative could be to use a differnt algoritm to sort them so they 
 * get sorted as Foo<chr>, Foo<int>,  Foo<char>::bar,Foo<int>::bar, 
 * Foo<char>::baz, Foo<int>::baz
 * but all bukets will have to pay the penalty of using this new function 
 * instead of strcmp to sort. Whereas we know that we are doing something 
 * like Foo::bar when we start with it and so it better to have the user 
 * pay the penalty of looking through the whole bucket instead
 *
 * Returns the number of mathching mpsyms found
 */

int
find_matching_syms (void **msym,
                    char *key,
                    void **ptr,
                    void *syn_chain,
                    int syn_count,
                    void (*action) (struct minimal_symbol *),
                    struct minsym_and_objfile *minsym_and_obj_arr,
                    int minsym_count,
                    int ptr_count,
                    int look_for_all)
{
  struct minimal_symbol **minsym, **ptr_list, **ptr_end;
  struct minimal_symbol *msymbol = NULL;
  struct msym_list *list;
  int len = 0, count = 0, cnt = 0;
  
  minsym = (struct minimal_symbol **) msym;
  ptr_list = (struct minimal_symbol **) ptr;
  ptr_end = ptr_list + ptr_count;

  if (!look_for_all)
    {
      /* Obtain a pointer to the first matching entry */
      len = (int) strlen (key);
      while (minsym > ptr_list)
	if (!strncmp (SYMBOL_DEMANGLED_NAME (*(minsym - 1)), key, len))
	  minsym--;
	else
	  break;
    }
  else
    minsym = ptr_list;
  
  /* Remember you could be handling a mix of psyms and minsyms */
  for (;(minsym < ptr_end) && *minsym; minsym++)
    {
      struct minimal_symbol *msymbol = *minsym;
      if (!msymbol)
	return NULL;

      if (SYMBOL_MATCHES_NAME (msymbol, key))
	{
	  switch (MSYMBOL_TYPE (msymbol))
	    {
	    case mst_text:
	    case mst_file_text:
	    case mst_unknown:
	      if (action)
		action (msymbol);
	      else
		{
		  if (minsym_count < max_synonyms_seen)
		    /* populate the minsyms_obj array */
		    minsym_and_obj_arr[minsym_count++].minsym = msymbol;
		  else
		    {
		      warning (" We have exceeded the max number of" 
      "overloaded or static function definitions - %d - "
      "that we would like to store."
      "Talk to your closest customer rep to increase the limit\n", 
		      max_synonyms_seen);
		      return count;
		    }
		}
	      ++count;
	      break;
	    default:
	      break;
	    }
	}
      else if (!look_for_all) 
	/* If we are looking for all don't break till you don't get to the 
	   end of the whole list irrespective if this name does not match
	   The next name might coz this maybe Foo<int> whereas the next 
	   may be Foo<int>::bar. */
	break;

    }
  
  if (syn_chain)
    {
      list = (struct msym_list *) syn_chain;
      while (list && cnt < syn_count)
	{
	  msymbol = list->msym;	  
	  cnt++;
	  if (SYMBOL_MATCHES_NAME(msymbol, key))
	    {	    
	      switch (MSYMBOL_TYPE (msymbol))
		{
		case mst_text:
		case mst_file_text:
		  if (action)
		    action (msymbol);
		  else
		    {
		      if (minsym_count < max_synonyms_seen)
			/* populate the minsyms_obj array */
			minsym_and_obj_arr[minsym_count++].minsym = msymbol;
		      else
			{
		  warning (" We have exceeded the max number of" 
			   "overloaded or static function definitions - %d - "
			   "that we would like to store."
			   "Talk to your closest customer rep to increase " 
                           "the limit\n", 
		max_synonyms_seen);
		            return count;
			}
		    }
		  ++count;
		  break;
		default:
		  break;
		}
	    }	  
	  list = list->next;
	}
    }

  return count;
} /* End of find_matching_syms */

/* Stores in the minsym_and_objfile_array if provided */

int
store_in_minsym_arr (struct minsym_and_objfile *minsym_and_obj_arr, 
		     int minsym_count, void **minsym)
{
  struct minimal_symbol *msymbol = *(struct minimal_symbol **) minsym;

  if (minsym_count < max_synonyms_seen)
    {
      minsym_and_obj_arr[minsym_count++].minsym = msymbol;
      return 1;
    }
  return 0;
}

/* Check if the mpsym is a declaration */

int
sym_not_decl (void **minsym)
{
  struct minimal_symbol *msymbol = *(struct minimal_symbol **) minsym;
  if (!MSYMBOL_IS_DECL (msymbol))
    return TRUE;
  return 0;
}

/* Check if the mpsym is global, static, trampoline  etc. */

int
sym_type (void **minsym,
         int mst_sought)
{
  /* 
  global_symbol = 1;
  file_symbol = 2;
  trampoline_symbol = 3;
  */

  struct minimal_symbol *msymbol = *(struct minimal_symbol **) minsym;
  switch (MSYMBOL_TYPE (msymbol))
    {
    case mst_file_data:
    case mst_file_bss:
      
      if (mst_sought == mst_text)
	break;
      /* else fall through ... */
      
    case mst_file_text:
      if (mst_sought == mst_solib_trampoline)
	if (target_has_stack)
	  return TRAMP;
      return STAT;
	
    case mst_solib_trampoline:
      /* if we are specifically searching for a trampoline
	 symbol, we are done. Otherwise, we will continue
	 to look for the real symbol. If none is found, we 
	 will use the trampoline if appropriate.
	 */
      
      if (mst_sought == mst_solib_trampoline)
	return GLOB;
      else if (mst_sought != mst_text)
	if (target_has_stack)
	  return TRAMP;
      break;
      
    case mst_text:
      if (mst_sought != mst_solib_trampoline)
	return GLOB; 
      break;
      
    case mst_unknown:
    default:
      if (mst_sought != mst_solib_trampoline &&
	  mst_sought != mst_text)
	if (MSYMBOL_IS_GLOBAL(msymbol) == TYPE_STATIC)
	  return STAT;
	else
	  return GLOB;
      break;
    }
 
  if (MSYMBOL_IS_GLOBAL (msymbol) == TYPE_STATIC)
    return STAT;
  else
    return GLOB;
} /* End sym_type */

/* Used to skip over the number offsets associated with Thunks
   
   Check if this is a C++ thunk.
   It will be of the form _ZT<call-offset> where 
   
             <call-offset> ::= h <nv-offset> _
                           ::= v <v-offset> _
	     <nv-offset> ::= n<offset number>
	                     # non-virtual base override
			     
	     <v-offset>  ::= <number> _ <virtual offset number>
	     <number>    ::= <nv-offset>
	                 ::= <offset number>
	     <virtual offset number> ABI doesn't say whether it is positive
	     or negative so assume it can be both.
			 
	     e.g _ZThn20061_
	         or _ZTv200_2000_
		 or _Tvn2000_20002_

		 Tchn20061_v200_2000_
		 or Tcv200_2000_hv20061_

   We check for 'T' and 'c' following it in extract_hash_key
   This function is called twice if a 'c' is seen else called once. 

 */

static int
skip_thunk_offsets (const char *input, int idx)
{
  if (input[idx] == 'h')
    {
      if (input[idx + 1] == 'n')
	idx = idx + 2; /* Skip over "hn" */
      else
	idx++; /* Skip over the "h" */
      
      /* Skip over the decimals following "hn or h" */
      while (input[idx] && isdigit(input[idx]))
	idx++;
      /* Skip over the following '_' */
      idx++;
    }
  else if (input[idx] == 'v')
    {
      if (input[idx + 1] == 'n')
	idx = idx + 2; /* Skip over "vn" */
      else
	idx++; /* Skip over "v" */
      
      /* Skip over 1 set of digits following "v" */
      while (input[idx] && isdigit(input[idx]))
	idx++;
      /* Skip over the following '_' */
      idx++;

      if (input[idx] == 'n')
	idx++; /* Skip over the "n" */

      /* Skip over 2nd set of digits following 
	 1st set of digits */
      while (input[idx] && isdigit(input[idx]))
	idx++;
      /* Skip over the following '_' */
      idx++;
    }
  return idx;
}


/* Handle the namespace std mangling specially
   <substitution> ::= St # ::std::
   <substitution> ::= Sa # ::std::allocator
   <substitution> ::= Sb # ::std::basic_string
   <substitution> ::= Ss # ::std::basic_string < char,
                                                 ::std::char_traits<char>,
                                                 ::std::allocator<char> >
   <substitution> ::= Si # ::std::basic_istream<char,  std::char_traits<char> >
   <substitution> ::= So # ::std::basic_ostream<char,  std::char_traits<char> >
   <substitution> ::= Sd # ::std::basic_iostream<char, std::char_traits<char> >
   
   libCsup's demangle demangles the following string as 
   "_ZNSs6appendERKSsmm"

   "std::string::append(std::string const&,unsigned long,unsigned long)"
   So we prefix std::string:: to the append
   
   Looked at /CLO/Components/HPCXX/Src/U2Lib/Parser.C in
   IA64ABIParser::retrieveStdAllocator, 
   IA64ABIParser::retrieveStdString
   IA64ABIParser::retrieveStdBasicString
   */

static int
add_std_demangling (const char *input, int idx, char **std_prefix, int *length)
{
  switch (input[idx])
    {
    case 't':
      strcpy (*std_prefix, "std");
      *length = 3;
      break;
    case 'a':
      strcpy (*std_prefix, "std::allocator");
      *length = 14;
      break;
    case 'b':
      strcpy (*std_prefix, "std::basic_string");
      *length = 17;
      break;
    case 's':
      strcpy (*std_prefix, "std::string");
      *length = 11;
      break;
    case 'i':
      strcpy (*std_prefix, "std::istream");
      *length = 12;
      break;
    case 'o':
      strcpy (*std_prefix, "std::ostream");
      *length = 12;
      break;
    case 'd':
      strcpy (*std_prefix, "std::iostream");
      *length = 13;
      break;
    default:
      *length = 0;
      break;
    }
  ++idx;
  return idx;
}


/*  Poorva - This function helps extract the key that we will
    hash to find the bucket to either insert in or lookup.
    It is used both while creating the hash table from the
    minimal symbols/ partial symbols and also when we are trying
    to lookup a symbol provided by the user.

    FOR DEMANGLED NAMES
    We want to extract the name such that we can hash it e.g
    for foo we want to return foo
    for Foo::bar we want to return Foo
    for Foo<char>::bar we want to return Foo
    for foo ( Bar::baz, Z<char>::w) we want to return foo.

    FOR MANGLED NAMES
    What we want to do here is extract the class name
    or the id1::id2 name
    where id1: <namespace name>
             : <class name>

     and  id2: <namespace name>
             : <class name>
             : <namespace data member>
             : <namespace function>

   from the mangled name
   e.g _ZN6GarplyIS_IcEE6garplyEiS0_
   From the above mangled name we want to get Garply
   Garply< Garply< char > >::garply( int, Garply< char > )
   We don't care about how the template has been instantiated
   they all get thrown into the same bucket.

   For demangled names we want to extract the leading name e.g
   foo(int, char) => foo
   foo<...>() => foo
   Foo<...>::foo() => Foo
   Foo::Bar<...>::foo() => Foo
   Foo<...> => Foo

   All namespace names and nested classes are prepended with N somewhere
   after _Z and before the digit. 
   For namespaces - since all namespace names are prepended with
   the namespace name e.g __rw::rwerrno is mangled as  _ZN4__rw7rwerrno
   What we want to hash is the whole __rw::rwerrno so check for N
   If we have a namespace or a nested class we take the identifier
   after the first one and concatenate ::2ndidentifier to the first
   and then hash the whole thing.

   If the first class is a templated class then we ignore the N
   e.g We have the name Garply<char>::NestedGarply::foo
   _ZN6GarplyIcE12NestedGarply3foo
   We see the N and so think we need to get the identifier after the
   first one but while going along we see the I after Garply and
   know that it is a templated class and just hash by Garply.

   A more uniform hash would have been if we could have hashed by
   the member function/data name. This would have prevented everything
   in a single class to get thrown into the same bucket.
   On PA this is possible since the mangling puts the member data first.
   On IPF with the new ABI mangling scheme - we would need to extract
   it from the right(end) and trying to decode a mangled name from right to
   left is fraught with peril.
*/

const char *
extract_hash_key (const char *input, int *opening_brace_start, 
		  int *is_mangled, char **dot_dot_pos, 
		  char** template_begin_pos, char** demangled_name_ptr)
{
  char *output = NULL, *outputOld = NULL;
  char *dotdot = NULL, *template_begin = NULL, *anon_ns = NULL;
  int j = 0, k = 0, end_digit = 0, i = 0, index = 0;
  int nPresent = 0, oldLength = 0, idx = 0;
  int length = 0;
  bool set_demangled_name = FALSE;
  *dot_dot_pos = 0; 
  *is_mangled = 0;
  *template_begin_pos= 0;
  *opening_brace_start = 0;

  if (!input || input[0] == '\0') 
    return NULL;

  if (demangled_name_ptr && (*demangled_name_ptr == NULL))
    {
      set_demangled_name = TRUE;
    }

  /* FOR DEMANGLED NAME */
  if (!(is_cplus_name (input, DEMANG)))  /* JAGaf49121 */
    {
      /* long_name has been malloc'd already - won't need to malloc here */
      output = long_name (STRING_SIZE);
      output[0] = '\0';
#if 0
      /* Not a mangled name. For C structs e.g for Foo::y
         Thelinker symbol table does not have the Foo::y.
         It just has the Foo class. Also we do not add these
         to the partial symbls either.
         Hence we do not encounter these. If in the future
         we encounter these add code here to extract the
         Foo by recognising the ::. (Below is untested code) */

         idx = 0;
         *is_mangled = 0;
         while (input[idx])
         {

           if (input [idx] == ':' && input [idx+1] == ':')
             break;
           idx ++;
         }
         length = idx;
	 	 
         strncpy (output, input, length);
	 output[length] = '\0';
         return output;
#endif

      /* The name is demangled - Discard all argument lists e.g
       * foo(int) should be output as foo. Start from the back and
       * find the matching paren
       */

      int len = (int) strlen (input), count = 0;
      int index = len - 1;

      /* Ignore any whitespace at the end */
      while (isspace (input [index]))
        index --;

      if (input[index] == ')')
        {
          count = 0;
          while (index >= 0)
            {
              if (input[index] == '(')
                count --;
              else if (input[index] == ')')
                count ++;
              if ((count == 0) && (index > 0))
		{
		  /* Ignore any whitespace before the first "(" */
		  while (isspace (input [index - 1]))
		    index --;
		  break;
		}
              index --;
            }
        }
      else
	index = len;

      /* JAGag37540, 'gdb crash with "list (1.c:1)"'.  A set of parens with
         no identifier ahead of it leaves -1 in "index" at this point. */
      if (index == -1) 
        return NULL;

      *opening_brace_start = index;
      dotdot = strstr (input, "::");
      *dot_dot_pos = dotdot;
      template_begin = strchr (input, '<');
      *template_begin_pos = template_begin;

      /* If the '<' is at the very start of the input 
	 string you aren't seeing a template but another string
	 of the form <unnamed namespace>::foo::bar etc. so set
	 template begin to NULL.
	 Also we need to look up the name <unnamed namespace>::foo
	 hence modify dotdot to point to the 2nd :: if there is 
	 a second one.
	 */

      if (template_begin == input)
	{
	  template_begin = NULL;
	  *template_begin_pos = template_begin;
	  if (dotdot)
	    {
	      dotdot += 2;
	      dotdot = strstr (dotdot, "::");
	      /* We set dot_dot_pos to NULL since get_more_of_name will use it
	       * to see how much more of the name does it need to get. 
	       * We don't want it getting more than it has so we set it to NULL
	       */
	      *dot_dot_pos = NULL;
	    }
	  else
	    return input;
	}
      
      /* index here specifies the point at which you have the '('
         if there is one.
         If there is an index but either there is a '<' or "::"
         before it then we need to extract stuff from before this.

         The reason we have to make sure that the dotdot / template begin
         lie before the the index of the '(' is because
         we may have a string that looks like this
         foo (A::bar, A<char>::baz)

         */

      if (!index || (index &&
                     ((template_begin && (template_begin < &input[index]))
                      || (dotdot && (dotdot < &input[index])))))
        {
          /* If Case: Pick the string until the '<' since that is less than
             the dotdot (checked for below) and the '(' (checked for above)
             e.g Foo<char>::bar(...)
             Else Case: Pick the string till the dotdot since the template
             args and the '(' come after it.
             e.g Foo::Bar<char>::baz(...)
             */

          if (template_begin && (!dotdot || (dotdot &&
                                             (template_begin < dotdot))))
            {
              length = (int)(template_begin - input);
              /* Make sure that you have enough room */
              output = long_name (length + 1);
              output = strncpy (output, input, length);
              output[length] = '\0';
              return output;
            }
          else if (dotdot && (!template_begin
                              || (template_begin
                                  && (dotdot < template_begin))))
            {
              length = (int)(dotdot - input);
              /* Make sure that you have enough room */
              output = long_name (length + 1);
              output = strncpy (output, input, length);
              output[length] = '\0';
              return output;
            }
        }
      else
        {
          /* Case for which we don't have template angle brackets or
             dotdot before the '('
             e.g foo (...)
             or foo(Bar<char>::baz, Z::w)
           */
          /* Make sure that you have enough room */
          output = long_name (index + 1);
          output = strncpy(output, input, index);
          output[index] = '\0';
          length = index;
          return output;
        }

      /* If we have neither '(' nor '<' nor "::" we need to just return
         the input string e.g foo
       */

      length = len;
      return (char *) input;
    } /* End of !(is_cplus_name (input, 1)) */
  else if (is_cplus_name (input, 1))    /* JAGaf49121 */
    {
      if (cplus_demangled_name && set_demangled_name)
        {
          /* JAGaf33468: Since the name has already been demangled, cache it. */
          *demangled_name_ptr = cplus_demangled_name;
        }

      if (IS_TARGET_LRE)
        {
          char *demangled_name;
	  char *position;

	  /* The code here is a work-in-progress.  Unclear what it should
	   * really be.  Empirically this is doing OK.
	   */

	  *is_mangled = 1;
	  demangled_name = cplus_demangled_name;   /* JAGaf49121 */
	  if (position = strchr(demangled_name, '(' /* ) */ ))
	    *position = 0;  /* Throw away anything from the first '(' */
	
	  /* If namespace given, use that */
	  if (position = strstr (demangled_name, "::"))
	    *position = 0;
          /* Make sure that there is enough room */
          output = long_name (strlen (demangled_name) + 1);
	  strcpy (output, demangled_name);

          /* JAGaf33468: Free cplus_demangled_name in the caller if
             demangled_name_ptr is not NULL. */ 
          if (!set_demangled_name)
            {
	      free (cplus_demangled_name);
              cplus_demangled_name = NULL;
            }
	  return output;
        }
      else if (!set_demangled_name)
        {
  	  /* After fix for JAGaf49121, is_cplus_name() now allocates memory
	     into global variable "cplus_demangled_name" if its second arg is
	     1.  That's only needed in LRE case above, but need to free it.  */
          /* JAGaf33468: Free cplus_demangled_name in the caller if
             demangled_name_ptr is not NULL. */ 
	  free (cplus_demangled_name);
	  cplus_demangled_name = NULL;
        }

      /* FOR MANGLED NAME */
      
      output = NULL;
      *is_mangled = 1;
      length = 0;
      idx = 2; /* ignore the "_Z" */

      /* We first look at the prefix before the number in the 
	 mangled name e.g _ZNG3foo where foo is the symbol and _ZNG
	 is the prefix.
	 Look in it for N - which means nested/namespace
	 After which scan it for numbers and ignore those numbers. 
	 e.g for anonymous namespace entries the compiler outputs a
	 GLOBAL_N_number_ 
	 We need to ignore this number so look for the following patterns
	 Look for C++ thunks - T
	 Look for G - anonymous namespaces 
	 Look for S - the std namespace and treat it specially.
	 */
      while (input[idx] && !isdigit(input[idx]))
	{
	  if (input[idx] == 'N')
	    {
	      nPresent = 1;
	      ++idx;
	    }
	  /* Check if this is a C++ thunk.
	     It will be of the form 
	     _ZT<call-offset> or 
	     _ZTc <call-offset> <call-offset>

	     We need to skip offsets. 
	     See skip_thunk_offset for informations 

	                For c <call-offset> <call-offset> 
			       # first call-offset is 'this' adjustment
			       # second call-offset is result adjustment
	     */
	  else if (input[idx] == 'T')
	    {	      
	      idx++;
	      if (input[idx +1] == 'c')	      
		{
		  idx++;
		  idx = skip_thunk_offsets (input, idx);
		  idx = skip_thunk_offsets (input, idx);
		}
	      else
		idx = skip_thunk_offsets (input, idx);      
	    }
	  else if (   input[idx] == 'G' ) 
	    {
	      /* For g++ the name looks like `_GLOBAL_[_.$]N.'
		 For aCC it looks like "GLOBAL_N_" followed by 
		 a number and then a _ 
		 e.g _ZNGLOBAL_N_4773472_3fooEv = <unnamed namespace>::foo
		 If anon_ns is alreday set, look for next GLOBAL_.
		 */
	      if (!anon_ns)
		anon_ns = strstr (input, "GLOBAL_");
	      else
		anon_ns = strstr (input + idx, "GLOBAL_");
	      /* For the pattern _ZGVGLOBAL_  make sure that the GLOBAL_
		 does match the first G */
	      if ((input + idx) != anon_ns)
		{
		  ++idx;
		  continue;
		}
	      idx += GCC_ANON_PREFIX_LENGTH;
	      if (anon_ns)
		{
		  if (input[idx] == '_' || input[idx] == '.' || input[idx] == '$')
		    ++idx;
		  if (input[idx] == 'N')
		    ++idx;
		 /* For an hp compilation the code in braces is necessary 
		  * since we want to get past the number after the _ZNGLOBAL_N
		  * for an eg. expr _ZNGLOBAL_N_4773472_3fooEv
		  * For g++ we don't need this - while processing 
		  * psymtabs in dwarf2read.c check for compilation type 
		  * and set g++compiled and then don't do this for g++
		  */ 
		    {
		      if (input[idx] == '_')
			++idx;
		      while (input[idx] != '_')
			++idx;	      
		    }
		} /* End of if (anon_ns) */	      
	      ++idx;
	    } /* End of else if (input[idx] == 'G') */
	  else if (input[idx] == 'S')
	    {
	      ++idx;
	      /* For the namespace std we have substitions in the new abi. 
		 "std::basic_string<char,std::char_traits<char>,
		 std::allocator<char> >::append"
		 is mangled as
		 "_ZNSs6appendERKSsmm"
		 */
              /* Make sure that there is enough room */
	      output = long_name (STRING_SIZE);
              output[0] = '\0';
	      idx = add_std_demangling (input, idx, &output, &oldLength);
	      
	      /* For the std::string::append case the std_prefix would 
		 be std::string and in keeping with the other keys 
		 we never extract more than 1 :: so we are done at
		 this point and can return whatever output now contains. 
		 which will be std::allocator, std::basic_string,
		 std::string, std::istream, std::ostream or std::iostream.
	       */
	      if (oldLength > 3)
		return output;
	    }
	  else
	    ++idx;

	} /* End of while (input[idx] && !isdigit(input[idx])) */
      
      while (1)
	{
	  if (input[idx] == 'S' && (input[idx+1] == '_' || input[idx+2] == '_'))
	    {
	      /* input can be S_ only if you are coming into this loop the
		 second time around. If the next 2 characters
		 are S_ then we have the same name repeated*/
	      /* The new C++ abi has a concept of compression where every
		 identifier that it mangles is entered into a table with
		 a short form e.g S_, S0_, S1_ etc.
		 The second time it sees the same identifier it uses the
		 S_ substitutions instead of mangling the name.
		 If we have a nested class with the same name then this might
		 be true e.g Foo::Foo where the 2nd Foo is a class of Foo.
		 */
	      /* Make sure that there is enough room */
              output = long_name (((2 * length) + 3));
	      strcat (output, "::");
	      strncat (output, output, length);
	      length = 2*(length) + 2;
	      break;
	    }
	  
	  j = 0;
	  length = 0;
	  
	  /* Form the number by extracting the digits from the string
	     and making the number */
	  
	  while (isdigit(input[idx]))
	    {
	      length *= 10;
	      length += input[idx] - '0';
	      idx++;
	    }
	  
	  /* If length > 1200 then return input */
	  /* If the length == 0 then we have nothing to do */
	  if (length == 0)
	    {
	      if (output)
		{
		  return output;
		}
	      else 
		return input;
	    }
	  else if ((idx + length) > strlen (input))	
  	    return input;

	  /* Store the index at which the digits end e.g _Z3Foo4Bars - hence
	     for this case end_digit = 3 since idx points to the F of Foo. */
	  
	  end_digit = idx;
	  
	  /* Increment the index to beyond the number that is get past Foo  */
	  
	  idx = end_digit + length;
	  
	  if (output)
	    {
              /* Make sure that there is enough room */
              output = long_name ((oldLength + 3));

	      strcat (output, "::");
	      length = oldLength + length + 2;
	      j = oldLength + 2;
	    }
	  else
	    {
	      output = long_name (STRING_SIZE);
              *output = NULL;
	    }
	  /* Concatenate or copy the string to the output string. Note that
	   * j has been set to where to point in the output string. Also if
	   * an output string is already present then a :: was added above.
	   */
	  
	  if (output)
	    {
              /* Make sure that there is enough room */
              output = long_name ((j + idx - end_digit + 1));

	      strncpy (output + j, input + end_digit, idx - end_digit);
	      output[ j + idx - end_digit ] = '\0';
	    }
	  else
	    return input;
	  
	  /* If you have found a template then you want to break irrespective
	     of the fact that it may have a nested class or not */

	  if (input[idx] == 'I')
	    {
	      break;
	    }
	  
	  /* nPresent tells us if we found an N in the beginning and
	     hence if we need to go around the loop again */
	  
	  if (!nPresent)
	    break;
	  else
	    oldLength = length;
	  /* If there is a member of the form, NSTail::Set::image(),
	     where NSTail is either a namespace or an outer of the nested class,
	     we only want to extract NSTail::Set, not the whole of
	     NSTail::Set::image. */
	  nPresent = 0;
	} /* End of while (1) */

      return (output);
    }
  return input;
}


/* return_more_of_name is a buffer used to return a value from get_more_of_name.
   When necessary, we allocate a larger buffer.
   */

static char * return_more_of_name = NULL;
static int sizeof_return_more_of_name = 0;


/* get_more_of_name [ need summary explanation ]

   Note: The return value may be a pointer to a static buffer which may
   be re-used on a subsequent call.
 */

const char *
get_more_of_name (const char *input, const char *prev_hash_key, 
		  int *opening_brace_start, char *dot_dot_pos,
		  char *template_begin_pos)
{
  char *output = NULL, *second_dot_dot = NULL;
  char *dotdot = NULL, *template_begin = NULL;
  const char *index = 0;
  int length = 0;
  const char *end = NULL;
  int needed_len;

  if (dot_dot_pos == NULL)
    return input;
  if (!input) 
    return NULL;

  /* FOR DEMANGLED NAME 
   * FIX_FOR_OLD_CXX_ABI 
   */
  if (!(is_cplus_name (input, DEMANG)))   /* JAGaf49121 */
    {
      int len = (int) strlen (input), count = 0;
      index = input + *opening_brace_start;
	
      dotdot = dot_dot_pos;

      template_begin = template_begin_pos;

      /* If the '<' is at the very start of the input 
	 string you aren't seeing a template but another string
	 of the form <unnamed namespace>::foo::bar etc. and so 
	 extract_hash_key already returned the correct stuff.
	 
	 No need to go looking for more.
	 */

      if (template_begin == input)
	return input;

      /* index here specifies the point at which you have the '('
         if there is one.
         If there is an index but either there is a '<' or "::"
         before it then we need to extract stuff from before this.

         The reason we have to make sure that the dotdot / template begin
         lie before the the index of the '(' is because
         we may have a string that looks like this
         foo (A::bar, A<char>::baz)

         */

      if (!index || (index &&
                     ((template_begin && (template_begin < index))
                      || (dotdot && (dotdot < index)))))
        {
          /* If Case: Pick the string until the '<' since that is less than
             the dotdot (checked for below) and the '(' (checked for above)
             e.g Foo<char>::bar(...)
             Else Case: Pick the string till the dotdot since the template
             args and the '(' come after it.
             e.g Foo::Bar<char>::baz(...)
             */

          if (template_begin && (!dotdot || (dotdot &&
                                             (template_begin < dotdot))))
            {
	      /* If you have a template before the dotdot then 
	       * extract_hash_key would have already returned this and 
	       * we don't want retrieve to be doing more work so we return 
	       * input itself. 
	       */
	      return input;
	    }
	  else if (dotdot && (!template_begin
                              || (template_begin
                                  && (dotdot < template_begin))))
            {
	      second_dot_dot = strstr (dotdot + 2, "::");
	      if (second_dot_dot)
		{
		  if (template_begin && (template_begin < second_dot_dot))
		    end = template_begin;
		  else
		    end = second_dot_dot;
		  
		  end = (index < end) ? index : end;
		}
	      else
		end = (template_begin && (template_begin < index)) ? template_begin : index;
	      
	      length = (int)(end - dotdot);
	      needed_len = (int) strlen(prev_hash_key) + length + 1;
	      if (needed_len > sizeof_return_more_of_name)
		return_more_of_name = xrealloc (return_more_of_name, 
						needed_len + 4000);
	      sizeof_return_more_of_name = needed_len + 4000;
	      strcpy (return_more_of_name, prev_hash_key);
	      strncat (return_more_of_name, dotdot, length);
	      return return_more_of_name;
            }
        }
      else
	return input;

      /* If we have neither '(' nor '<' nor "::" we need to just return
         the input string e.g foo
       */

      return input;
    } /* End of !(is_cplus_name (input, 1)) */
  
  return input;
}
