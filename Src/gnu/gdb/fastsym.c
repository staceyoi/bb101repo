/* New symbol storage model (actually partial and minimal symbols) for gdb 
   Improves performance, scalability and helps debug profiled binaries
   and comdats.

   Copyright 1994, 1995, 1996, 1997, 2001 Free Software Foundation, Inc.
   
   Written by Poorva Gupta (poorva@cup.hp.com)

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <string.h>
#include "../../../Src/gnu/Judy/src/Judy.h"
#include "defs.h"
#include "fastsym.h"
#include "symtab.h"

/* #define STANDALONE 1*/

#ifdef STANDALONE
#include "gdbtypes.h"
#include "gdb_string.h"
#include "bfd.h"
#include "symfile.h"
#include "objfiles.h"
#include "demangle.h"
#include "gdb-stabs.h"
#include "target.h"
#endif

#if defined(HP_IA64)
#include "machine/sys/inline.h" /* _Asm_lfetch_excl */
#endif

/* Returns the name of the minimal symbol */
#define GET_MINSYM_NAME(value)	(SYMBOL_NAME((struct minimal_symbol *) value))

/* Make a copy of the string at PTR with SIZE characters in the symbol obstack
   (and add a null character at the end in the copy).
   Returns the address of the copy.  */
                                                                            
extern char *obsavestring (char *, int, struct obstack *);

static JError_t jerror_t = {0};
static PJError_t PJError = &jerror_t;

/*** 

Our data structure looks like this.

  1st              2nd level Judy array 
  level            (may also have list sorted on demangled names)
  Judy             (may also have a synonym chain)
  array
 ______
|      |---->  _Z3Foo3barEi,  _Z3Foo3barEc  
| Foo  |  
|      |---->  Foo::bar(int) , Foo::bar(char) (lazily formed demangled chain)
 ------
|      |   2 bars exact same name but static in different files
|      |----> _Z3barEi (2nd level Judy array)
| bar  |----> _Z3barEi / bar (int) (synonym chain has both names) 
|      |----> bar(int) demangled chain
 ------
|      |
| intI |----> intI (global - assuming it isn't mangled hence only 
|      |             a 2nd level Judy array) 
 ------
|      |
| baz  |----> baz (for struct baz baz; 
 ------		   1 baz in 2nd level Judy array
                   2nd baz as part of the synonym chain.)

 
This diagram can be likened to a hash table with buckets and 
overflow chains.

The 1st level Judy array can be thought of as the hash table.
Each bucket will have one more Judy array attached to it and 
may have a synonym chain and demangled chain.

The 2nd level Judy array can be thought of as the overflow chain 
of the buckets of the hash table. The difference is that the 
overflow chain has unique strings only. 

Any non unique strings go into the synonym chain attached to each
bucket.

e.g we have struct baz baz - 
one baz goes into the 2nd level Judy array and the 2nd baz goes
into the synonym chain for that bucket.
OR
static int bar(int) in 2 different files. 
1 bar goes into the 2nd level Judy array and the 2nd bar goes
into the synony chain. 
 
Besides that we lazily and on demand form a linked list sorted
by demangled names. 

The struct array_holder can be thought of as the bucket. 

It contains the
2nd level Judy array ---> PPvoid_t mangled;
synonym chain        ---> void *synonym_chain; 
demangled chain      ---> void **demangled;  
           (linked list sorted by demangled name - the entries 
            are formed from the 2nd level Judy array and will be
            equal to the number of entries in the 2nd level Judy array.
            When we form this linked list we also go ahead and demangle
            the names of the mpsyms in the synonym_chain.) 

When searching for something we also have to search the synonym chain.

Besides that it contains the 

count          ----> count of number of things in the 2nd level Judy array
dem_count      ----> count of number of things in demangled array 
                     should be equal to count if demangled array present
syncount       ----> count of number of things in synonym chain.


----------------------------------------------------------------------------
JudySL is an "unbounded" dynamic word array with arbitrary string indexes 
and sorting features

The Judy functions used in this file are as follows. The description has
been taken from man JudySL.

PPvoid_t JudySLIns (PPvoid_t PPArray, const char * Index, PJError_t PJError);
PPvoid_t JudySLGet (Pcvoid_t  PArray, const char * Index, PJError_t PJError);
PPvoid_t JudySLFirst (Pcvoid_t  PArray, char * Index, PJError_t PJError);
PPvoid_t JudySLNext (Pcvoid_t  PArray, char * Index, PJError_t PJError);
ulong_t  JudySLFreeArray (PPvoid_t PPArray, PJError_t PJError);

Given a pointer to a pointer to a JudySL string array, the latter of 
which must be initialized to (Pvoid_t) NULL before the first call of 
JudySLIns(), and an array index to insert (store) that is a null-terminated
character string, return a pointer to the value area (array element) 
corresponding to the specified index, or PPJERR in case of serious error.
The caller must save the desired value in the value area, which is set to 
zero upon insertion of a new index.  If the JudySL array is initially empty, 
it is created. *PPArray may be modified whenever a new Index is inserted.

----------------------------------------------------------------------------
PPvoid_t JudySLGet (Pcvoid_t  PArray, const char * Index, PJError_t PJError);

JudySLGet retrieves a value from the Judy table. Index is the key you
are searching for.  Given a pointer to a JudySL string array and an array
index to locate that is a null-terminated character string, return a pointer 
to the value area (array element) corresponding to the specified index, a null
pointer if Index is not stored (valid), or PPJERR in case of serious error 
                     void * Array[char *];       // theoretical native C
                     char * Index = "Kansas";
                     Array[Index] = Value;
                     Value = Array[Index];

                     Pvoid_t PArray = (Pvoid_t) NULL; // JudySL array
                     char *  Index  = "Kansas";
                     *JudySLIns(&PArray, Index, PJE0) = Value;
                     Value = *JudySLGet(PArray, Index, PJE0);

--------------------------------------------------------------------------
PPvoid_t JudySLFirst (Pcvoid_t  PArray, char * Index, PJError_t PJError);

Given a pointer to a JudySL string array and a pointer to an array index 
that is a null-terminated character string, locate the specified or next 
(higher-valued) index stored in the JudySL array (start with a null string 
to look up the first index in the array) and return a pointer to its value 
area, a null pointer if there is no valid index at or above Index, or PPJERR 
in case of serious error. If successful, *PIndex is also modified to the new
index.

Note:  The caller's Index buffer must be at least as large as the largest 
string stored in the array, although the passed Index string might be shorter.

--------------------------------------------------------------------------
PPvoid_t JudySLNext (Pcvoid_t  PArray, char * Index, PJError_t PJError);

Given a pointer to a JudySL string array and a pointer to an array index 
that is a null-terminated character string, locate the next (higher-valued) 
index stored in the JudySL array and return a pointer to its value area, 
a null pointer if there is no valid index above Index, or PPJERR in case 
of serious error. If successful, *PIndex is also modified to the new index.

--------------------------------------------------------------------------
ulong_t  JudySLFreeArray (PPvoid_t PPArray, PJError_t PJError);

Given a pointer to a JudySL string array, free the entire array 
(much faster than using JudySLNext() and JudySLDel()), set *PPArray to NULL, 
and return the number of bytes freed, including 0 for an already empty
array or JERR in case of a serious error.
Note:  JudySLFreeArray() does not return memory to the operating system, 
but holds it for other Judy use.

--------------------------------------------------------------------------

This is the Error struct of Judy. 
typedef struct _JUDY_ERROR_STRUCT
{
        JU_Errno_t je_Errno;         // one of the enums above.
        int        je_ErrID;         // often an internal source line number.
        Word_t     je_reserved[4];   // for future backward compatibility.

} JError_t, * PJError_t;

For more information on Judy use man JudySL 
or look at Src/gnu/Judy/src/Judy.h
Downloaded from sourceforge.net 
****/

struct array_holder 
{
  int count;
  int dem_count;
  int syncount;
  PPvoid_t mangled;
  void *synonym_chain;
  void **demangled;
};


/* We handle Judy errors in this routine. */

static void
handle_error (PJError_t PJError)
{
  JU_Errno_t err_num = JU_ERRNO (PJError);

#if 0
  /* For HP internal use only */
  /* often an internal source line number in Judy sources.*/
  int errid = JU_ERRID(PJError); 
#endif

  error ("Judy Error errno = %d", err_num);
}


/* This routine returns a pointer to a malloc'd area.
 * This malloc'd area is then used to keep a pointer to the Judy table.
 * The pointer to the table changes with every insert 
 * that you make into Judy.
 * 
 * In the objfile data structure we have a field long_name
 * which is a malloced area of large size and we use it for extract_hash_key
 * and to iterate over the Judy array. Saves time in that we don't need to
 * malloc everytime.
 *
 * We have a size if we ever want to make this a hash table instead we could
 * use the number of minsyms passed in to determine the number of buckets in 
 * it. 
 */

void **
init_table(long init_size)
{
  PPvoid_t PPArray = (void **) current_pst_obstack_alloc (sizeof (void **));
  *PPArray = NULL;

  return PPArray;
}


/* Used to iterate over the synonym chain. 
 * pptr_minsym is the last visited node in the synonym_chain. 
 * Use it to get to the next node and return the minsym from the 
 * next node. 
 * If *pptr_minsym is NULL use the synonym_chain.
 * Also set pptr_minsym to point to the most recently visited node.
 */

inline void *
get_next_synonym (void **pptr_minsym, void *arg_list)
{
  struct msym_list *node = (struct msym_list *) (*pptr_minsym);
  struct msym_list *list = (struct msym_list *) arg_list;
  
  node = (node) ? node->next : list;
  *pptr_minsym = node;
  return (node ? node->msym : NULL);
}


/* 
 * This is used to insert the minimal and partial symbols into the 
 * Judy array. It checks for duplicates and adds to the synonym chain 
 * when required. It also adds the psymtab that an mpsym (minimal or 
 * partial symbol - they are now the same) belongs to. 
 * 
 * It returns what it entered into the table. If it found a DUP and 
 * did not create a unique entry - it returns NULL. The return value is 
 * used by add_psymbol_to_list
 *
 * Given an mpsym (the value argument) we retrieve the name from it. 
 * We use the the name to extract a key. 
 * (look at extract_hash_key for comments on what the key should be)
 * We use this key to find the bucket to insert it into 
 * (creating one if it isn't already present). 
 * If the bucket is already present then 
 * a) If check duplicates turned on  
 *    - we try to retrieve an item of the same name from the 2nd level
 *      Judy array. 
 *          - If we don't find one then we insert it. 
 *          - if we find one then we call is_dup which tells us if they
 *            are TRUE_DUP, DUP or NOT_DUP.
 *          - If it is a TRUE_DUP we do not want to enter this one too.
 *          - If it is a NOT_DUP then we add it to the 2nd level Judy array.
 *          - If is a DUP which could mean 2 things 
 *             -- they share the name but are not true duplicates of 
 *                each other e.g struct foo foo where the 2 foos are 
 *                different but have the same name or they could be 
 *                static functions in different files. 
 *                We then add the second one to the synonym chain 
 *                (taken care of in add_to_pst_list) 
 *             -- One is a psym and the previous one is the corresponding msym.
 *                In this case we want to add the information about the psym 
 *                to the msym esp which psymtab did it come from. 
 *                (taken care of in add_to_pst_list) 
 * 
 * b) If check duplicates is not on we 
 *    - we try to retrieve an item of the same name from the 2nd level
 *      Judy array by calling insert which tells us if it is empty or not. 
 *        - If we don't find anything present at that location we insert
 *          what we have
 *        - If we do find something present at that location then we 
 *           i) Either have the psym for that msym (hence update msym) 
 *          ii) Or we add to the synony chain if they aren't the same.
 *          Either case we call add_to_pst_list which takes care of it. 
 *
 *  The other parameters to the function are 
 *  value_name - For partial symbols we do not want to allocate space on 
 *               the obstack for it without first doing duplicate checking
 *               hence we pass in the pointer for the name.
 *  is_allocated - For partial symbols we do not want to allocate the 
 *                 psym on the obstack without first dup checking so 
 *                 only after we make sure that it isn't a duplicate
 *                 do we call alloc_mpsym to allocate space for it. 
 *                 is_allocated is true for minimal symbols since those
 *                 are allocated before we come in here. Duplicates have 
 *                 already been thrown out. 
 *  dem_name - For some fortran symbols we add the demangled name too 
 *             which is passed in too.
 *  type_length - required for dup_checking of user defined types. 
 *                tells us the length of the type e.g number of bytes in
 *                a class. 
 *  dies_length - required for dup_checking of user defined types. 
 *                tells us the number of dies between this die and its 
 *                sibling and in essence provides us with a method of 
 *                identifying whether a class is the same as another. 
 *                ( Used an alternative to number of fields in class.
 *                  At the time that we are building the class psymbol we
 *                  don't want to count the number of fields (expensive) 
 *                  but we do have the distance between this die and its
 *                  sibling so we use that.) 
 *
 */

void *
insert_into_table (void **table, void *value, char *value_name, 
		   char *dem_name, int is_allocated, 
		   int dies_length, int type_length,
                   struct obstack *obstack)
{
  void *minsym = NULL;
  int is_mangled = 0;
  char *name = NULL, *minsym_name = NULL, *demangled_name = NULL;
  const char * key = NULL;
  int opening_brace_start, name_length = 0;
  struct array_holder *array_holder, **ptr;  
  Pvoid_t PArray = (Pvoid_t) (*table); 
  int is_duplicate = -1;
  void **ret_value = NULL; /* value retrieved from Judy */
  void *pptr_minsym = NULL, *old_value = NULL;
  char *dot_dot_pos, *template_begin_pos;
  /* Flag that tells us that we checked the synonym chain and hence
     we may have a null value for old_value but it still needs to 
     be added to the syn_chain and not to the mangled Judy array */
  int checked_syn_chain = 0;
  bool demangling_done = FALSE;

  minsym_name = (value_name) ? value_name : GET_MINSYM_NAME(value);
  if (!minsym_name || minsym_name[0] == '\0')
    return NULL;

  demangled_name = SYMBOL_CPLUS_DEMANGLED_NAME ((struct minimal_symbol *)value);
  if (demangled_name)
    {
      demangling_done = TRUE;
    }
  key = extract_hash_key (minsym_name, &opening_brace_start,
                          &is_mangled, &dot_dot_pos,
                          &template_begin_pos, &demangled_name);
  if (!key)
    key = minsym_name;
  if (!key)
    return NULL;

  ptr = (struct array_holder **) JudySLIns (&PArray, key, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);

#ifdef HP_IA64
  /* prefetch "ptr" pointer, used below */
  _Asm_lfetch_excl(_LFTYPE_NONE,_LFHINT_NONE,ptr);
#endif

  /* JAGaf33468: jini: Since the C++ names have already been demangled
     in the call to is_cplus_name, cache it for later use. */
  if (!demangling_done && obstack && demangled_name)
    {
      SYMBOL_CPLUS_DEMANGLED_NAME ((struct minimal_symbol *)value) =
        obsavestring (demangled_name,
                      (int) strlen (demangled_name),
                      obstack);
    }
  if (cplus_demangled_name)
    {
      free (cplus_demangled_name);
      cplus_demangled_name = NULL;
      if (!demangling_done)
        {
          demangled_name = NULL;
        }
    }

  *table = PArray;

  /* Judy insert initializes the value area with a NULL Pointer 
   if nothing already present */

  if (ptr && (*ptr == NULL))
    {
      /* No bucket present for this key - creating one
       * We will always insert into the mangled list
       * hence we initialize another Judy array. 
       */

      array_holder = (struct array_holder *) current_pst_obstack_alloc 
	                             (sizeof (struct array_holder));
      *ptr = array_holder;
      memset (array_holder, 0, sizeof (struct array_holder));
      array_holder->mangled = (PPvoid_t) init_table (0);

      if (is_allocated) 
	*JudySLIns ((*ptr)->mangled, minsym_name, PJError) = value;
      else
	{
	  value = alloc_mpsym (value, NOT_DUP, dem_name,
			       dies_length, type_length);
	  *JudySLIns ((*ptr)->mangled, minsym_name, PJError) = value;
	}

      if (JU_ERRNO(PJError))
	handle_error (PJError);
      (*ptr)->count ++;
      return value;
    }

  /* Retrieve a symbol of the same name if one is present. Note the 
   * use of minsym_name and not key to retrieve. 
   */

  ret_value = JudySLIns ((*ptr)->mangled, minsym_name, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);    
  old_value = (ret_value && *ret_value) ? (*ret_value) : NULL;

#ifdef CHECK_DUPLICATE  
  /* Here we want to remove duplicates - we check in the function is_dup 
   * if we have a TRUE_DUP (do nothing - return NULL) 
   * NOT_DUP - in which case check the synonym chain for other items that 
   * share the same name but have different properties. 
   * DUP - in which case we call add_to_pst_list which after performing
   * some checks adds the psymtab that the mpsym came from to it. 
   * 
   * For comments about the significance of the values DUP, NOT_DUP etc 
   * refer to the beginning of this function
   */

#ifdef HP_IA64
  /* prefetch "old_value" pointer, used in is_dup() */
  _Asm_lfetch_excl(_LFTYPE_NONE,_LFHINT_NONE,old_value);
#endif

  while (old_value)
    {
      is_duplicate = is_dup (old_value, value, dies_length, type_length);
      
      if (is_duplicate == TRUE_DUP)
	return NULL;
      
      if (is_duplicate == DUP) 
	{
	  /* We need to allocate it before adding to pst. */
	  if (!is_allocated)
            value = alloc_mpsym (value, DUP, dem_name, dies_length,
				 type_length);

	  add_to_pst_list (value, old_value, 
			   &((*ptr)->synonym_chain),	   
			   &((*ptr)->syncount),
			   dies_length, type_length);

	  return NULL;
	}
      
      if (is_duplicate == NOT_DUP)
	{
	  /* check all in the synonym chain to see if you find a dup there.*/
	  if (((*ptr)->syncount) && ((*ptr)->synonym_chain))
	    {
	      old_value = get_next_synonym (&pptr_minsym,
					    ((*ptr)->synonym_chain));
	      checked_syn_chain = 1;
	    }
	  else
	    break;
	}
    } 
#endif /* CHECK_DUPLICATE */ 

  /* If dup_checking is enabled and we still got here means that 
   * it is NOT_DUP. If we were checking duplicates in the above ifdef
   * we would have returned if we had found a DUP after adding to the 
   * pst_list. The fact that we got out of the while and got to this
   * point means that we got a NOT_DUP in the code above. 
   */
  

  /* If it isn't already allocated (won't be allocated in case of 
   * psymbols then allocate it
   */

  if (!is_allocated) 
    value = alloc_mpsym (value, NOT_DUP, dem_name, dies_length, type_length);

  /* If you find something is present at already at that index in the 
   * Judy array then 
   * a) you have been checking for dups but you are here means that it is 
   *    not a dup.
   * b) you haven't been checking for dups and *ret_value is still set 
   *    means it is either the psym for the msym or a synonym. 
   */

  if (old_value || checked_syn_chain)
    {      
      /* Do not enter .stub symbols into the synonym list, they aren't
       * needed and they get in the way.  JAGaf39474, "executable takes 
       * too long to load".
       */
      if ((minsym_name[0] != '.') ||
          (minsym_name[1] != 's') ||
          (minsym_name[2] != 't') ||
          (minsym_name[3] != 'u') ||
          (minsym_name[4] != 'b') ||
          (minsym_name[5] != 0))
        add_to_pst_list (value, old_value, 
		       &((*ptr)->synonym_chain),	   
		       &((*ptr)->syncount),
		       dies_length, type_length);
      /* (*ptr)->count is not incremented here since that is the count
       * of the things in the mangled list - or a count of the number of 
       * unique items. Once we call add_to_pst_list we add either
       * to the synonym chain which is why the syncount parameter is
       * passed in or we modify an already present item
       */
    }
  else
    { 
      *ret_value = value;
      (*ptr)->count ++;
    }

  return value;
} /* End of insert_into_table */


/* Used by retrieve routines to demangle the bucket if required. 
 * Iterate over the whole mangled Judy array and insert into
 * the demangled array. The demangled array can't be a Judy
 * array since we need to do a strcmp_iw or strcmp_iwt on 
 * the elements when we are looking for names in it. 
 * We also might have only a partial name that we are looking for.
 *
 * demangle bucket returns a pointer to pointer to minsym if found. 
 * It demangles the names , qsorts them and then bsearches the bucket. 
 */

void **
demangle_bucket (struct array_holder *ptr, char *key)
{
  void *msymbol, *node_synchain = NULL, **pptr_minsym = NULL, 
    **ret_pptr_minsym = NULL;
  char *name, *minsym_name;
  int i = 0, count = 0, size = sizeof (void *);
  /* size is actually size of struct minimal symbol * for us */

  if (ptr->demangled == NULL)
    {      
      ptr->demangled = (void **) current_pst_obstack_alloc (ptr->count * size);
      
      /* To be able to use the Judy routines to iterate over the 
       * Judy array we have to have a buffer passed in to Judy. 
       * The buffer allocated must be as long as the longest string
       * but must contain the last index used. Since malloc is expensive
       * we allocate a buffer when we create the bucket and use it now. 
       */

      name =  long_name(STRING_SIZE); 
      strcpy (name, "");

      /* Iterate over mangled array and demangle the names in 
	 the mangled array */
      pptr_minsym = JudySLFirst (*(ptr->mangled), name, PJError);
      if (JU_ERRNO(PJError))
	handle_error (PJError);
      while (pptr_minsym && *pptr_minsym) 
	{
	  set_demangled_name (*pptr_minsym);
	  ptr->demangled[count++] = *pptr_minsym;
	  minsym_name = GET_MINSYM_NAME(*pptr_minsym);
          name =  long_name(strlen(minsym_name) + 1); 
	  strcpy (name, minsym_name);
	  pptr_minsym = JudySLNext(*(ptr->mangled), name, PJError);
	  if (JU_ERRNO(PJError))
	    handle_error (PJError);
	}
      ptr->dem_count = count;

      if (count == 0)
	ptr->demangled = NULL;

      if (ptr->dem_count)
	{
	  /* Now that you have the array filled in sort it */
	  qsort (ptr->demangled, 
		 ptr->dem_count, 
		 size,
		 compare_minimal_symbols_by_demangled_name);
	}

      /* Also demangle all names in the synonym chain */
      if (ptr->synonym_chain)
	{
	  for (i = 0; i < ptr->syncount; ++i)
	    {
	      msymbol = get_next_synonym (&node_synchain,
					  ptr->synonym_chain);
	      set_demangled_name (msymbol);
	    }
	}
    }

  /* Now that you have it do a bsearch on the demangled chain.
   * If bsearch doesn't find something there is no need to look
   * into the synonym chain since there has to an entry with that
   * name in the 2nd level Judy array before it makes it into the 
   * synonym chain.
   */

  if (ptr->dem_count)
    ret_pptr_minsym = (void **) 
      bsearch ((char *) key,
	       ptr->demangled, 
	       ptr->dem_count,
	       size,
	       compare_name_and_minimal_symbols_demangled_name);
  else
    return NULL;

  return ret_pptr_minsym;
}

static struct array_holder **
get_ptr_holder_with_new_hash (void *table, char *key, const char *hash_key, 
		   	      int *opening_brace_start, char *dot_dot_pos,
			      char * template_begin_pos)
{
  struct array_holder **ptr_holder = NULL;
  
  if (hash_key != key)
    hash_key = get_more_of_name (key, hash_key, opening_brace_start, 
				 dot_dot_pos, template_begin_pos);
  
  if (hash_key == key)
    return NULL;
  ptr_holder = (struct array_holder **) 
    JudySLGet(table, hash_key, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);
  return ptr_holder;
}


/* Retrieves elements from the table. Prefers global over static etc if 
 * nothing specified by user. Replaces all different routines e.g 
 * lookup_minimal_symbol_text, lookup_minimal_symbol_solib_trampoline,
 * lookup_symbol_resolved, lookup_minimal_symbol_by_pc 
 * 
 * The parameters to the function
 * table - is a pointer to the 1st level Judy array.
 * key - is the key to search for. 
 * search_mangled - specified by user - says search only mangled 
 * mst_sought - user can specify that he is looking for a trampoline
 *              symbol, a static symbol, in which case we would prefer a
 *              trampoline symbol over the other symbols. 
 * not_decl - Tells us to look for a symbol that is not marked as 
 *            a declaration (lookup_symbol_resolved) 
 * namesp - tells us to look in VAR_NAMESPACE, STRUCT_NAMESPACE etc.
 * addr - is the pc value we want to look for along with the name.
 * found_type - is filled in by this function to tell the caller if
 *              we found a global, static, trampoline etc. 
 *              It is an enum sym_type_enum defined in fastsym.h
 * 
 * The function returns a NULL if nothing found else returns the symbol 
 * that matches the criteria the user provided e.g matches not_decl, 
 * namesp etc. If it didn't find an exact match for the criteria it 
 * returns a close_match if it found one. 
 */


void *
retrieve_from_table (void *table, char *key, int search_mangled,     
		     int mst_sought, int *found_type, int not_decl,
		     int namesp, void *addr)
{
  int is_mangled = 0;
  void *msymbol, **pptr_minsym = NULL, *close_match = NULL,
    *node_synchain = NULL;
  const char *hash_key;
  int opening_brace_start;
  struct array_holder **ptr_holder, *ptr;
  char *dot_dot_pos, *template_begin_pos;

  if (!key)
    return NULL;
  hash_key = extract_hash_key ((char *) key, 
			       &opening_brace_start, &is_mangled,
			       &dot_dot_pos, &template_begin_pos, NULL);
  if (!hash_key)
    hash_key = key;

  /* Get the bucket using the key */

  ptr_holder = (struct array_holder **) JudySLGet (table, hash_key, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);

  if (!ptr_holder && !is_mangled)
    ptr_holder = get_ptr_holder_with_new_hash 
      (table, key, hash_key, &opening_brace_start, 
       dot_dot_pos, template_begin_pos);
  
  if (!ptr_holder)
    return NULL;
  
  ptr = (struct array_holder *) *ptr_holder;
  if (!ptr)
    return NULL;

  /* If the user has specified to search only the mangled array 
   * don't bother to demangle names in this bucket - be as 
   * lazy as you can. 
   */
  if (!search_mangled && !is_mangled)
    {
      pptr_minsym = demangle_bucket (ptr, key);
      
      if (pptr_minsym == NULL)
	{
	  /* Try with the new hash_key */
	  ptr_holder = get_ptr_holder_with_new_hash 
	    (table, key, hash_key, &opening_brace_start,
	     dot_dot_pos, template_begin_pos);
	  if (!ptr_holder)
	    return 0;	  
	  ptr = (struct array_holder *) *ptr_holder;
	  if (!ptr)
	    return 0;
	  pptr_minsym = demangle_bucket (ptr, key);
	  if (pptr_minsym == NULL)
	    return NULL;
	}
      
      /* After demangling and searching the bucket find_best_match
       * finds which of the minsyms fits best - prefers global over static
       * etc. Also checks the synony_chain to find mpsyms
       */
      msymbol = find_best_match (pptr_minsym, key, ptr->demangled, 
				 ptr->synonym_chain, namesp,
				 mst_sought, found_type, ptr->count, not_decl);
      
      return msymbol;
    }
  else 
    {
      void *found_symbol = NULL;
      void *found_file_symbol = NULL;
      void *trampoline_symbol = NULL;

      /* If you had even one mangled name we would be looking in the
	 demangled array.
	 So the names in here are all demangled. 
      */

      pptr_minsym = NULL;
      while (found_symbol == NULL)
	{
	  if (pptr_minsym)
	    {
	      /* If you already found one in the Judy array look 
		 in the synonym chain too */
	      if (ptr->syncount && ptr->synonym_chain)
		{
		  msymbol = get_next_synonym (&node_synchain,
					      ptr->synonym_chain);
		  pptr_minsym = &msymbol;
		}
	       else
		 pptr_minsym = NULL;
	    }
	  else
	    {
	      /* Look in the 2nd level Judy array */
	      pptr_minsym = (void **) JudySLGet (*(ptr->mangled),key, PJError);
	      if (JU_ERRNO(PJError))
		handle_error (PJError);
	    }

	  if (pptr_minsym && *pptr_minsym)
	    {
	      /* If user has specified a pc check if the pc matches */
	      if (addr) 
		{
		  if (check_pc (*pptr_minsym, addr))
		    return *pptr_minsym;
		  else
		    continue;
		}

	      /* If user has specified that he isn't looking for a 
		 declaration make sure we don't return a declaration.
		 Is marked a declaration if it is declared extern */
	      if (not_decl)
		{
		  if (sym_not_decl (pptr_minsym))
		    {	
		      found_symbol = *pptr_minsym;
		      break;
		    }
		  else
		    continue;
		}
	      
	      /* If we need to look in VAR_NAMESPACE etc make sure 
		 we return only what is required */
	      if (namesp)
		{
		  if (check_namespace (*pptr_minsym, namesp) == 0)
		    {
		      close_match = *pptr_minsym;
		      continue;
		    }
		}

	      /* Else fill in either the global / static etc 
		 since we like to prefer global over static */
	      switch (sym_type (pptr_minsym, mst_sought))
		{
		case GLOB:
		  found_symbol = *pptr_minsym;
		  break;
		case STAT:
		  found_file_symbol = *pptr_minsym;
		  break;
		case TRAMP:
		  trampoline_symbol = *pptr_minsym;
		  break;
		default:
		  found_symbol = *pptr_minsym;
		  break;
		}  
	    }
	  else
	    break;
	}
      

      if (found_symbol == NULL && found_file_symbol == NULL && 
	  trampoline_symbol == NULL)
        {
	  *found_type = CL_MTCH;
	  return close_match;
	}

      /* External symbols are best.  */
      if (found_symbol)
	{
	  *found_type = GLOB;
	  return found_symbol;
	}
      
      /* File-local symbols are next best.  */
      if (found_file_symbol)
	{
	  *found_type = STAT;
	  return found_file_symbol;
	}
      
      /* Symbols for shared library trampolines are next best.  */
      if (trampoline_symbol)
	{
	  *found_type = TRAMP;
	  return trampoline_symbol;
	}
    }

  return NULL;
}

	
/* Retrieve all symbols matching that key and either put them into the 
 * minsym_and_obj_arr or perform 'action' on them. Return the number 
 * of matching symbols found.
 * look_for_all indicates we have to look in the whole demangled bucket for all 
 * matching symbols
 */


int
retrieve_all_from_table (void *table, char *key, retr_action_fn_ptr_t action,
			 struct minsym_and_objfile *minsym_and_obj_arr, int minsym_count,
			 int look_for_all)
{
  int is_mangled = 0;
  void *msymbol = NULL, *node_synchain = NULL, **pptr_minsym = NULL;
  const char *hash_key;
  int opening_brace_start;
  struct array_holder **ptr_holder, *ptr;
  int count = 0, number_stored = 0;
  char *dot_dot_pos, *template_begin_pos;

  if (!key)
    return NULL;
  hash_key = extract_hash_key ((char *) key, &opening_brace_start, 
			       &is_mangled, &dot_dot_pos,
                               &template_begin_pos, NULL);
  if (!hash_key)
    hash_key = key;

  /* Get the bucket using the key */

  ptr_holder = (struct array_holder **) JudySLGet(table, hash_key, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);
  
  if (!ptr_holder && !is_mangled)
    ptr_holder = 
      get_ptr_holder_with_new_hash 
      (table, key, hash_key, &opening_brace_start,
       dot_dot_pos, template_begin_pos);
  
  if (!ptr_holder)
    return 0;

  ptr = (struct array_holder *) *ptr_holder;
  if (!ptr)
    return 0;

  /* If the key that you have is not mangled then search the 
   * demangle the bucket else don't bother to demangle names in this 
   * bucket - be as lazy as you can.
   */

  if (!is_mangled) 
    {
      pptr_minsym = demangle_bucket (ptr, key);
      
      if (pptr_minsym == NULL)
	{
	  /* Try with the new hash_key */
	  ptr_holder = 
	    get_ptr_holder_with_new_hash 
	    (table, key, hash_key, &opening_brace_start,
	     dot_dot_pos, template_begin_pos);
	  if (!ptr_holder)
	    return 0;
	  
	  ptr = (struct array_holder *) *ptr_holder;
	  if (!ptr)
	    return 0;
	  pptr_minsym = demangle_bucket (ptr, key);
	  if (pptr_minsym == NULL)
	    return NULL;
	}
      
      /* Iterate over the demangled array to find all the matching minsyms 
       * If there are more than one entry with the same name; bsearch will 
       * return any of those. We need to get to the first of those matching 
       * entries and then for all matching minsyms either store them in the 
       * minsym_and_obj_arr or perform action on them.
       */

      count = find_matching_syms (pptr_minsym, key, ptr->demangled, 
			          ptr->synonym_chain, ptr->syncount,
				  action, minsym_and_obj_arr, 
				  minsym_count, ptr->count, look_for_all);
    }
  else 
    {
      pptr_minsym = NULL;
      while (1) 
	{
	  if (pptr_minsym)
	    {
	      /* If you did find one entry in the Judy array then the 
	       * else part of this if statement would have filled in 
	       * pptr_minsym. There may be others in the synnonym chain 
	       * Check those too and for all matching minsyms either add
	       * them to the minsym_and_obj_arr or perform action on them.
	       * Increment count too and if not found then break. 
	       */

	      if (ptr->syncount && ptr->synonym_chain)
		{
		  msymbol = get_next_synonym (&node_synchain,
					      ptr->synonym_chain);
		  pptr_minsym = &msymbol;
		}
	       else
		 pptr_minsym = NULL;
	    }
	  else
	    {
	      /* Look in the Judy array first */ 
	      pptr_minsym = (void **) JudySLGet (*(ptr->mangled),key, PJError);
	      if (JU_ERRNO(PJError))
		handle_error (PJError);
	    }
	  
	  if (pptr_minsym && *pptr_minsym)
	    {
	      if (minsym_and_obj_arr)
		{
		  number_stored = store_in_minsym_arr (minsym_and_obj_arr, 
						       minsym_count, pptr_minsym);
		  if (number_stored == 0)
		    return count;
		}
	      else if (action)
		action (*pptr_minsym);
	      ++count;
	    }
	  else
	    break;
	}
    }
  return count;
}


/* Frees the Judy arrays, the synonym chain, the demangled bucket */
 
void
free_table (void **table, long size)
{ 
  struct array_holder **buckets = NULL;
  char *key = NULL, *prev_key = NULL;
  void *ptr, *ptr_old;

  key = long_name (STRING_SIZE);
  strcpy (key, "");

  /* Free the 2nd level Judy arrays */
  buckets = (struct array_holder **) JudySLFirst (*table, key, PJError);  
  if (JU_ERRNO(PJError))
    handle_error (PJError);
  while (buckets && *buckets)
    {
      (void) JudySLFreeArray (((*buckets)->mangled), PJError);
      if (JU_ERRNO(PJError))
	handle_error (PJError);

      ((*buckets)->demangled) = NULL;
      ((*buckets)->mangled) = NULL;
      if ((*buckets)->synonym_chain)
	((*buckets)->synonym_chain) = NULL;

      ((*buckets)->count) = 0;
      ((*buckets)->dem_count) = 0;
      ((*buckets)->syncount) = 0;

      buckets = (struct array_holder **) JudySLNext (*table, key, PJError);
      if (JU_ERRNO(PJError))
	handle_error (PJError);
    }

  /* JudySLFreeArray() does not return memory to the
     operating system, but holds it for other Judy use. */
  /* Free the 1st level Judy array */
  if (table)
    (void) JudySLFreeArray (table, PJError);
  table = NULL;
}

#ifdef STANDALONE
int main ()
{
  PPvoid_t table;
  struct minimal_symbol *minsym;
  char *name = (char *) xmalloc (12);
  int is_duplicate = -1;
  table = init_table (0);
  
  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "A::i");
  insert_into_table (table, minsym, NULL, &is_duplicate, 0, NULL);


  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "A::foo");
  insert_into_table (table, minsym, , NULL, &is_duplicate, 0, NULL);

  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "A::bar(int)");
  insert_into_table (table, minsym, , NULL, &is_duplicate, 0, NULL);

  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "B::i");
  insert_into_table (table, minsym, , NULL, &is_duplicate, 1, NULL);


  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "B::j");
  insert_into_table (table, minsym, , NULL, &is_duplicate, 1, NULL);


  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "B::foo");
  insert_into_table (table, minsym, , NULL, &is_duplicate, 1, NULL);


  minsym = (struct minimal_symbol *) xmalloc (sizeof 
					      (struct minimal_symbol *));
  SYMBOL_NAME (minsym) = (char *) xmalloc (12);
  strcpy (SYMBOL_NAME (minsym), "intI");
  insert_into_table (table, minsym, , NULL, &is_duplicate, 0, NULL);

  strcpy (name, "A::i"); 
  retrieve_from_table (*table, name, 0, 0);
  strcpy (name, "A::i"); 
  retrieve_from_table (*table, name, 1, 0);
  strcpy (name, "A::foo"); 
  retrieve_from_table (*table, name, 0, 0);

  free_table (table, 0);

}
#endif

