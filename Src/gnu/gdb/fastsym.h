#include "obstack.h"

#define CHECK_DUPLICATE 1
#define STRING_SIZE string_size
#define UPDATE_STRING_SIZE(size) \
  { \
    if (size > STRING_SIZE) \
      STRING_SIZE = size; \
  }

extern int string_size;
enum dups_enum
{
  /* Is a true duplicate  = 0 */
  TRUE_DUP, 
  
  /* Is a duplicate by name but is probably a static and
     so it is legally possible to have more than one 
     (in this case we need to add the psymtab that the sym 
     belongs to the list of psymtabs) = 1
     */
  DUP, 
  
  /* Not a duplicate - one of a kind = 2 */
  NOT_DUP
};

signed enum sym_type_enum
{
  CL_MTCH = -1,
  UNDEF   =  0,
  GLOB    =  1, 
  STAT    =  2,
  TRAMP   =  3
};


/* In fastsym*.c minimal_symbol and partial_symbol are interchangeable
 * Sometimes use mpsym which stands for minimal /partial symbol. 
 * msym_list is the structure for the synonym_chain.  
 */

struct msym_list 
{
  struct msym_list *next;
  struct minimal_symbol *msym;
};


/* Defined on Judy side */

/* This routine returns a pointer to a malloc'd area.
 * This malloc'd area is then used to keep a pointer to the Judy table.
 * On the gdb side the objfile is the holder of this judy table. 
 * objfile->hash_table holds it. 
 */

void **
init_table(long init_size);

/* 
 * Inserts the minsyms / psyms into the Judy table. 
 */

void *
insert_into_table (void **table, void *value, char *name,
		   char *dem_name, int is_allocated,
		   int dies_length, int type_length,
                   struct obstack *obstack);

/* Retrieves symbol from the table depending upon requirements 
 * e.g not_decl (don't want a declaration only mpsym) - used for  
 * a lookup_symbol_resolved kind of lookup
 * namesp - what are we looking for VAR_NAMESPACE, STRUCT_NAMESPACE etc.
 * a sym with a particular address - used for a lookup_minsym_by_pc
 * kind of lookup
 * If there is a static and a global present retrieve_from_table 
 * prefers the global over the static 
 */

void *
retrieve_from_table (void *table, char *key, int search_mangled, 
		     int mst_sought, int *found_type, int not_decl,
		     int namesp, void *addr);

struct minsym_and_objfile; /* declare for following declaration */

/* Returns the number of symbols it found. Used to search for 
 * multiple symbols of the same name. Used by decode_line_1_reuse to 
 * set breakpoints.
 * key is the name we need to lookup
 * action is a function pointer that is passed in if we would like 
 * a certain action performed (e.g expanding the psymtab the mpsym 
 * belongs to. 
 * minsym_and_objfile is an array to store the mpsym and the objfile 
 * it was found in. 
 * minsym_count gives us the index to start storing at. 
 */

struct minimal_symbol;
typedef void (*retr_action_fn_ptr_t) (struct minimal_symbol *);
int
retrieve_all_from_table (void *table, char *key, 
			 retr_action_fn_ptr_t action,
			 struct minsym_and_objfile *, int minsym_count,
			 int look_for_all);

/* 
 * Frees the table and all the baggage with it. 
 */

void
free_table (void **table, long size);


/* Defined on gdb side */

/* Takes a size and allocates space on the objfiles psymbol obstack 
 */

/* From fastsym-helper.c */

void *
current_pst_obstack_alloc (long size);

/* Returns the pointer to the area allocated for a global
 * buffer
 * It returns local_long_name
 */

char *
long_name (int size);

/* 
 * Checks to see if this minsym's pc matches the pc passed in
 */

int
check_pc (void *minsym, void *addr);

/* 
 * is_dup returns NOT_DUP if there isn't a duplicate
 * DUP if it is a duplicate but not a true duplicate
 * TRUE_DUP if it is a true duplicate
 * A DUP is returned if we find the partial_symbol for a minimal
 * symbol.
 * Minimal symbols are created and entered first into the table
 * When we enter partial symbols for variables and functions 
 * we will find the corresponding minimal symbols. We need to 
 * add additional information that the psyms carry to these already
 * present msyms. The additional info that is most important for this 
 * concept to work is - Which partial symtab (.o / .c) do I belong to. 
 *
 * Besides that a DUP is returned for synonyms e.g statics in different
 * files. 
 * struct foo foo; where foo is both a struct and a variable etc. 
 * struct bar { int i; int k; };  struct bar { int d[2]; }  etc.
 */

enum dups_enum 
is_dup (void *minsym, void *ins_minsym, int num_fields, int type_length);


/* Used to sort the demangled chain and since we know we have
 * complete names we can use strcmp and not the ignore whitespace and
 * templates version
 */

int
compare_minimal_symbols_by_demangled_name (const void *p1, const void *p2);

/* Used to bsearch the demangled chain */

int
compare_name_and_minimal_symbols_demangled_name (const void *p1, 
						 const void *p2);

/* Returns the name of the minimal symbol */

char *
get_minsym_name (void *value);

/* 
 * Allocates space for, demangles the name and sets the demangled
 * name of the minsymbol
 */

void
set_demangled_name (void *value);

/* Allocate memory on the psymbol_obstack for the psymbol passed in
 * Also allocate memory for it's name, demangled name if present
 * and fill in the psymtab that it belongs to.
 * If dies_length and type_length are not NULL then use them and
 * allocate and fill in the elem_dups structure attached to the mpsym.
 */

void *
alloc_mpsym (void *value, int is_duplicate, char *dem_name,
	     int dies_length, int type_length);

/* 
 * Add the value given to the synonym_chain (arg_list). Also
 * increment the syncount for that bucket. Return the new link that
 * you created.
 */

void *
add_to_syn_chain (void *value, void **arg_list, int *count);

/* value - is the new one mpsym to be added.
 * value_old is the one already at that position.
 *
 * -- they share the name but are not true duplicates of
 *    each other e.g struct foo foo where the 2 foos are
 *    different but have the same name or they could be
 *    static functions in different files.
 *    We then add the second one to the synonym chain
 *
 * -- One is a psym and the previous one is the corresponding msym.
 *    In this case we want to add the information about the psym
 *    to the msym esp which psymtab did it come from.
 *
 */

void
add_to_pst_list (void *value, void *value_old, void **arg_list, 
		 int *count,
		 int dies_length, 
		 int type_length);

/* Check if the mpsym is a declaration */
int
sym_not_decl (void **minsym);

/* Check if the mpsym is global, static, trampoline  etc. */
 
int
sym_type (void **minsym, int mst_sought);

/*
 * Used to iterate over the demangled chain and retrieve a matching
 * entry from it. Once we finish iterating over the demangled chain
 * we check the synonym_chain for a match
 *
 */

void * 
find_best_match (void **minsym, char *key, void **list, void *syn_chain, 
		 int namesp, int mst_sought, int *found_type, int ptr_count, 
		 int not_decl);

struct minimal_symbol; /* declare for following declaration */

/* Find all the mpsyms matching this name */

int 
find_matching_syms (void **minsym, char *key, void **list,
		    void *syn_chain, int syn_count,
		    void (*action) (struct minimal_symbol *), 
		    struct minsym_and_objfile *, int minsym_count, 
		    int ptr_count, int look_for_all);

/* Stores in the minsym_and_objfile_array provided */

int
store_in_minsym_arr (struct minsym_and_objfile *, 
		     int minsym_count, void **minsym);

/* This function helps extract the key that we will
 * use to find the bucket to either insert in or lookup.
 * It is used both while creating the judy table from the
 * minimal symbols/ partial symbols and also when we are trying
 * to lookup a symbol provided by the user.
 */

const char *
extract_hash_key (const char *input, int *length, int *is_mangled,
		  char **dot_dot_pos, char **template_begin_pos,
                  char **demangled_name_ptr);

/* This function helps extract more of the key that we will
 * use to find the bucket in lookup.
 * 
 */

const char *
get_more_of_name (const char *input, const char *prev_hash_key, 
		  int *opening_brace_start, char *dot_dot_pos, 
		  char *template_begin_pos);

extern int check_namespace ( void *, int );
