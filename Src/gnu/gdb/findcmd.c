/* Implementation of find command.
   Copyright 1986, 1987, 1989-1996, 1999-2000 Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include <ctype.h>
#include <string.h>
#include "assert.h"
#include "gdbcmd.h"
#include "value.h"
#include "target.h"

#define PATTERN_BUF_SIZE 100
#define CORE_ADDR_MAX (~ (CORE_ADDR) 0)
#define SEARCH_CHUNK_SIZE 16000

#define FIND_USAGE_ERROR "Usage :\tfind [/size-char] [/max-count] " \
                         "start-address, end-address,\n" \
			  "\texpr1 [, expr2 ...]\n" \
			  "\tfind [/size-char] [/max-count] " \
                          "start-address, +length,\n" \
			  "\texpr1 [, expr2 ...]"

extern void *two_way_short_needle (const unsigned char *, size_t,
 				    const unsigned char *, size_t);

extern void *two_way_long_needle (const unsigned char *, size_t,
 				   const unsigned char *, size_t);

/* Function similar to FSF's memmem() function.*/
extern void *pattern_match (const unsigned char *, size_t,
                            const unsigned char *, size_t);

static boolean validate_address (value_ptr);

static boolean validate_strOrchar (value_ptr);

static int
simple_search_memory (CORE_ADDR, ULONGEST, const unsigned char *,
		      ULONGEST, CORE_ADDR *); 

/*  Parse the arguments of the "find" command.  */
/* Copied from bfd_put_bits.  */
static void
put_bits (uint64_t data, char *buf, int bits, int big_p)
{
  int i;
  int bytes;

  assert (bits % 8 == 0);

  bytes = bits / 8;
  for (i = 0; i < bytes; i++)
    {
      int index = big_p ? bytes - i - 1 : i;

      buf[index] = data & 0xff;
      data >>= 8;
    }
}

/* This function parses the "find command" arguments.
   It validates each argument and then returns back
   the start_addr, end_addr, pattern_len,
   address_space_len and max_cnt */
static void
parse_find_args (char *args, ULONGEST *max_cnt_ptr,
		 char **pattern_buf_ptr, ULONGEST *pattern_len_ptr,
		 CORE_ADDR *start_addr_ptr, ULONGEST *addr_space_len_ptr)
{
  /* Default to using the specified type.  */
  char size = '\0';
  ULONGEST max_count = ~(ULONGEST) 0;
  /* Buffer to hold the search pattern.  */
  char *pattern_buf;
  /* Current size of search pattern buffer.
     We realloc space as needed.  */
  ULONGEST pattern_buf_size = PATTERN_BUF_SIZE;
  /* Pointer to one past the last in-use part of pattern_buf.  */
  char *pattern_buf_end;
  ULONGEST pattern_len;
  CORE_ADDR start_addr = 0;
  ULONGEST search_space_len = 0;
  char *s = args;
  int big_p = gdbarch_byte_order (current_gdbarch) == BIG_ENDIAN;
  struct cleanup *old_cleanups;
  struct value *val;
  struct type *type;
  int arg_count = 0;
  /* Used to validate arguments */
  boolean valid_val;

  if (args == NULL)
    error ("Arguments required. Provide arguments to find the pattern.");

  pattern_buf = xmalloc (pattern_buf_size);
  pattern_buf_end = pattern_buf;
  old_cleanups = make_cleanup (free_current_contents, &pattern_buf);

  /* Parse string to get the arguments and format values. */
  while (*s == '/')
    {
      ++s;

      while (*s != '\0' && *s != '/' && !isspace (*s))
	{
	  if (isdigit (*s))
	    {
	      max_count = atoi (s);
	      while (isdigit (*s))
		++s;
	      continue;
	    }

	  switch (*s)
	    {
	    case 'b':
	    case 'h':
	    case 'w':
	    case 'g':
	      size = *s++;
	      break;
	    default:
	      error ("Invalid format");
	    }
	}

      while (isspace (*s))
	++s;
    }

  /* For corefiles if the first argument(start addr) is string,
     then issue out usage error */
  if (*s == '"' && (arg_count < 2) &&
      (!target_has_execution && target_has_stack))
    {
      printf_filtered ("Wrong start address for find command in core file.\n"
                       "Please provide proper address.\n");
      error (FIND_USAGE_ERROR);
    }
  else
    {
      /* Get the search range.  */
      val = parse_to_comma_and_eval (&s);
      /* Validate the start address. */
      valid_val = validate_address (val);
      if (valid_val)
        {
          start_addr = value_as_pointer (val);
          arg_count++;
        }
      else
        {
          printf_filtered ("Invalid start address. Please provide "
                           "proper address.\n");
          error (FIND_USAGE_ERROR); 
        }
     }

  if (*s == ',')
    ++s;
  while (isspace (*s))
    ++s;

  if (*s == '+')
    {
      LONGEST len;
      ++s;
      /* For corefiles if the second argument is string,
         then issue out usage error. */
      if (*s == '"' && (arg_count < 2) &&
          (!target_has_execution && target_has_stack))
        {
          printf_filtered ("Wrong length for find command in core file.\n"
                           "Please provide proper length.\n");
          error (FIND_USAGE_ERROR);
        }
      else
        {
          val = parse_to_comma_and_eval (&s);
          len = value_as_long (val);
          if (len == 0)
	    {
	      printf_filtered ("Empty search range.\n");
	      return;
	    }
          if (len < 0)
	    error ("Invalid length.");
          /* Watch for overflows.  */
          if (len > CORE_ADDR_MAX
	      || (start_addr + len - 1) < start_addr)
	    error ("Search space too large.");
          search_space_len = len;
          arg_count++;
        }
    }
  else
    {
      /* For corefiles if the second argument is string,
         then issue out usage error. */
      if (*s == '"' && (arg_count < 2) &&
          (!target_has_execution && target_has_stack))
        {
          printf_filtered ("Wrong end address for find command in core file.\n"
                           "Please provide proper address.\n");
          error (FIND_USAGE_ERROR);
        }
      else
        {
          CORE_ADDR end_addr;
          val = parse_to_comma_and_eval (&s);
          valid_val = validate_address (val);
          if (valid_val)
            {
              end_addr = value_as_pointer (val);
              if (start_addr > end_addr)
	        error ("Invalid search space, end preceeds start.");
              search_space_len = end_addr - start_addr + 1;
              /* We don't support searching all of memory
	         (i.e. start=0, end = 0xff..ff).
	         Bail to avoid overflows later on.  */
              if (search_space_len == 0)
	        error ("Overflow in address range computation, "
                       "choose smaller range.");
              arg_count++;
            }
          else
            {
              printf_filtered ("Invalid end address. "
                               "Please provide proper address.\n");
              error (FIND_USAGE_ERROR); 
            }
        }
    }

  if (*s == ',')
    ++s;

  /* Fetch the search string.  */
  while (*s != '\0')
    {
      LONGEST x;
      int val_bytes;

      while (isspace (*s))
	++s;

      /* For corefiles, instead of parsing and evaluating the
         argument, simple parsing is done to store the pattern.
         This is done to overcome commandline call made for
         strings while evaluating the expression. No issues
         for character. */
      if (*s == '"' && (arg_count < 3) &&
          (!target_has_execution && target_has_stack))
        {
           if (size != '\0')
             error ("Invalid size-char format for find "
                    "command in core file.\nDo \"help find\" "
		    "for command usage and more details.");
           ++s;
           int buf_size = 0;
           while (isalnum (*s) && (*s != '"'))
             {
               if (buf_size == pattern_buf_size)
	         {
                   size_t current_offset = pattern_buf_end - pattern_buf;
	           pattern_buf_size *= 2;
	           pattern_buf = xrealloc (pattern_buf, pattern_buf_size);
	           pattern_buf_end = pattern_buf + current_offset;
	         }
               *pattern_buf_end = *s;
               pattern_buf_end++;
               s++;
               buf_size++;
             }

           if (*s == '"')
             ++s;
           else
             {
               printf_filtered ("Invalid search pattern\n");
               error (FIND_USAGE_ERROR);
             }
        }
      else
        {
          val = parse_to_comma_and_eval (&s);
          valid_val = validate_strOrchar (val);
          if (valid_val)
            {
              val_bytes = TYPE_LENGTH (VALUE_TYPE (val));

              /* This check is made for strings alone.
                 Usually string stores newline character also.
                 If user gives string pattern like str1, str2 then
                 the formed search pattern will be wrong without removal
                 of newline character. So for string, length is reduced
                 by 1. */
              if ((TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_ARRAY) &&
                  (val_bytes > 1))
                {
                  /* If pattern is string and contains size-char, then
                     issue out error message. */
                  if (size != '\0')
                    error ("Invalid size-char format for find "
                           "command in core file.\nDo \"help find\" "
		           "for command usage and more details.");
                  else
                    val_bytes--;
                }

              /* Keep it simple and assume size == 'g' when
	         watching for when we need to grow the 
                 pattern buf.  */
              if ((pattern_buf_end - pattern_buf + 
                  max (val_bytes, sizeof (int64_t))) > pattern_buf_size)
	        {
                  size_t current_offset = pattern_buf_end - pattern_buf;
	          pattern_buf_size *= 2;
	          pattern_buf = xrealloc (pattern_buf, pattern_buf_size);
	          pattern_buf_end = pattern_buf + current_offset;
	        }

              if (size != '\0')
	        {
	          x = value_as_long (val);
	          switch (size)
	            {
	              case 'b':
	                *pattern_buf_end++ = x;
	                break;
	              case 'h':
	                put_bits (x, pattern_buf_end, 16, big_p);
	                pattern_buf_end += sizeof (int16_t);
	                break;
	              case 'w':
	                put_bits (x, pattern_buf_end, 32, big_p);
	                pattern_buf_end += sizeof (int32_t);
	                break;
	              case 'g':
	                put_bits (x, pattern_buf_end, 64, big_p);
	                pattern_buf_end += sizeof (int64_t);
	                break;
	            }
	        }
              else
	        {
	          memcpy (pattern_buf_end, VALUE_CONTENTS_ALL (val),
                          val_bytes);
	          pattern_buf_end += val_bytes;
	        }
            }
          else
            {
              printf_filtered ("Invalid search pattern. Please provide"
                               "proper search pattern.\n");
              error (FIND_USAGE_ERROR);
            }
        }
      if (*s == ',')
	++s;
      while (isspace (*s))
        ++s;
    }

  if (pattern_buf_end == pattern_buf)
    error ("Missing search pattern.");

  pattern_len = pattern_buf_end - pattern_buf;
  arg_count++;

  if (search_space_len < pattern_len)
    error ("Search space too small to contain pattern.");

  if (arg_count != 3)
    error (FIND_USAGE_ERROR);

  *max_cnt_ptr = max_count;
  *pattern_buf_ptr = pattern_buf;
  *pattern_len_ptr = pattern_len;
  *start_addr_ptr = start_addr;
  *addr_space_len_ptr = search_space_len;

  /* We successfully parsed the arguments, leave the freeing of PATTERN_BUF
     to the caller now.  */
  discard_cleanups (old_cleanups);
}

/* Searches for a pattern in the specified address range. */
static void
find_command (char *args, int from_tty)
{
  /* Arguments of the find command */
  ULONGEST max_count = 0;
  char *pattern_buf = 0;
  ULONGEST pattern_len = 0;
  CORE_ADDR start_addr = 0;
  ULONGEST addr_space_len = 0;
  
  unsigned int found_count;
  CORE_ADDR last_found_addr;
  struct cleanup *old_cleanups;

  parse_find_args (args, &max_count, &pattern_buf, &pattern_len, 
		     &start_addr, &addr_space_len);

  old_cleanups = make_cleanup (free_current_contents, &pattern_buf);

  /* Perform the search.  */

  found_count = 0;
  last_found_addr = 0;

  while (addr_space_len >= pattern_len
	 && found_count < max_count)
    {
      /* Offset from start of this iteration to the next iteration.  */
      ULONGEST next_iter_incr;
      CORE_ADDR found_addr = 0;
      int found = simple_search_memory (start_addr, addr_space_len,
					(const unsigned char *) pattern_buf,
					pattern_len, &found_addr);

      if (found <= 0)
	break;

      print_address (found_addr, gdb_stdout);
      printf_filtered ("\n");
      ++found_count;
      last_found_addr = found_addr;

      /* Begin next iteration at one byte past this match.  */
      next_iter_incr = (found_addr - start_addr) + 1;

      /* For robustness, we don't let search_space_len go -ve here.  */
      if (addr_space_len >= next_iter_incr)
	addr_space_len -= next_iter_incr;
      else
	addr_space_len = 0;
      start_addr += next_iter_incr;
    }

  /* Record and print the results.  */

  set_internalvar (lookup_internalvar ("numfound"),
		   value_from_longest (builtin_type_int,
       		   (LONGEST) found_count));
  if (found_count == 0)
    printf_filtered ("Pattern not found.\n");
  else
    printf_filtered ("%d pattern%s found.\n", found_count,
		     found_count > 1 ? "s" : "");

  do_cleanups (old_cleanups);
}

/* The default implementation of to_search_memory.
   This implements a basic search of memory, reading target memory and
   performing the search here (as opposed to performing the search in on the
   target side with, for example, gdbserver).  */

static int
simple_search_memory (CORE_ADDR start_addr, ULONGEST search_space_len,
		      const unsigned char *pattern, ULONGEST pattern_len, 
		      CORE_ADDR *found_addrp) 
{
  const unsigned chunk_size = SEARCH_CHUNK_SIZE;
  /* Buffer to hold memory contents for searching.  */
  char *search_buf;
  unsigned search_buf_size;
  struct cleanup *old_cleanups;

  search_buf_size = chunk_size + pattern_len - 1;

  /* No point in trying to allocate a buffer larger than the search space.  */
  if (search_space_len < search_buf_size)
    search_buf_size = search_space_len;

  search_buf = xmalloc (search_buf_size);
  if (search_buf == NULL)
    error ("Unable to allocate memory to perform the search.");
  old_cleanups = make_cleanup (free_current_contents, &search_buf);

  /* Prime the search buffer.  */

  if (target_read_memory (start_addr, search_buf, search_buf_size) != 0)
    {
      warning ("Unable to access target memory at 0x%x, halting search.",
	       start_addr);
      do_cleanups (old_cleanups);
      return -1;
    }

  /* Perform the search.

     The loop is kept simple by allocating [N + pattern-length - 1] bytes.
     When we've scanned N bytes we copy the trailing bytes to the start and
     read in another N bytes.  */

  while (search_space_len >= pattern_len)
    {
      unsigned char *found_ptr = NULL;
      unsigned nr_search_bytes = min (search_space_len, search_buf_size);

      found_ptr = pattern_match ((const unsigned char *)search_buf, nr_search_bytes,
			         pattern, pattern_len);

      if (found_ptr != NULL)
	{
	  CORE_ADDR found_addr = start_addr + 
                                 (CORE_ADDR) (found_ptr - 
                                 (unsigned char *) search_buf);
	  *found_addrp = found_addr;
	  do_cleanups (old_cleanups);
	  return 1;
	}

      /* Not found in this chunk, skip to next chunk.  */
      /* Don't let search_space_len wrap here, it's unsigned.  */
      if (search_space_len >= chunk_size)
	search_space_len -= chunk_size;
      else
	search_space_len = 0;

      if (search_space_len >= pattern_len)
	{
	  unsigned keep_len = search_buf_size - chunk_size;
	  start_addr += chunk_size;
	  CORE_ADDR read_addr = start_addr + keep_len;
	  int nr_to_read;

	  /* Copy the trailing part of the previous iteration to the front
	     of the buffer for the next iteration.  */
	  assert (keep_len == pattern_len - 1);
	  memcpy (search_buf, search_buf + chunk_size, keep_len);

	  nr_to_read = min (search_space_len - keep_len, chunk_size);

	  if (target_read_memory (read_addr, search_buf + keep_len,
			          nr_to_read) != 0)
	    {
	      warning ("Unable to access target memory at 0x%x, halting search.",
		        read_addr);
	      do_cleanups (old_cleanups);
	      return -1;
	    }

	}
    }

  /* Not found.  */

  do_cleanups (old_cleanups);
  return 0;
}

/* Function to validate whether given value is a valid address. 
   Return true for structures, unions, pointers, references,
   integer and array. It returns false for character, string and 
   all other data types. Refer gdbtypes.h to know about type codes. */
static boolean
validate_address (value_ptr val)
{
  struct type *type = VALUE_TYPE (val);
  enum type_code code = TYPE_CODE (type);
  struct type *tar_type = NULL;
  int eltlen = 0;

  switch (code)
    {
      case TYPE_CODE_UNION:
      case TYPE_CODE_STRUCT:
      case TYPE_CODE_REF:
      case TYPE_CODE_PTR:
        return true;

      /* Returns false if it is character type. */
      case TYPE_CODE_INT:
        if (TYPE_LENGTH (type) == 1)
          return false;
        else
          return true;

      /* Returns false if it is string type. */
      case TYPE_CODE_ARRAY: 
        tar_type = check_typedef (TYPE_TARGET_TYPE (type));
        eltlen = TYPE_LENGTH (tar_type);
        if (eltlen == 1 &&
            TYPE_CODE (tar_type) == TYPE_CODE_INT)
          return false;
        else
          return true;

      default:
        return false;
    }
}

/* Function validates whether the value is string or character 
   or integer(hexadecimal). */
static boolean
validate_strOrchar (value_ptr val)
{
  struct type *type = VALUE_TYPE (val);
  enum type_code code = TYPE_CODE (type);
  struct type *tar_type = NULL;
  int eltlen = 0;

  switch (code)
    {
      /* If integer or character type, return true. */
      case TYPE_CODE_INT:
          return true;

      /* For string type, return true. */
      case TYPE_CODE_ARRAY: 
        tar_type = check_typedef (TYPE_TARGET_TYPE (type));
        eltlen = TYPE_LENGTH (tar_type);
        if (eltlen == 1 &&
            TYPE_CODE (tar_type) == TYPE_CODE_INT)
          return true;
        else
          return false;

      default:
        return false;
    }
}

void
_initialize_memory_find (void)
{
  add_cmd ("find", class_vars, find_command, ("\
Search memory for the given pattern.\n\n\
Usage:\n\
find [/<SIZE-CHAR>] [/<MAX-COUNT>] <START-ADDR>, <END-ADDR>, <EXP1> [, <EXP2> ...]\n\
find [/<SIZE-CHAR>] [/<MAX-COUNT>] <START-ADDR>, +<LENGTH>, <EXP1> [, <EXP2> ...]\n\n\
size-char is one of b,h,w,g for 8,16,32,64 bit values respectively,\n\
and if not specified the size is taken from the type of the expression in the\n\
current language. size-char is applicable only for hexadecimal patterns.\n\
Note that this means for example that in the case of C-like languages\n\
a search for an untyped 0x42 will search for \"(int) 0x42\"\n\
which is typically four bytes.\n\n\
Convenience variable \"$numfound\" is set to the number of matches."),
	   &cmdlist);
}
