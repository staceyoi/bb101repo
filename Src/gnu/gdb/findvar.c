/* Find a variable's value in memory, for GDB, the GNU debugger.
   Copyright 1986, 87, 89, 91, 94, 95, 96, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>
#include "defs.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "frame.h"
#include "value.h"
#include "gdbcore.h"
#include "inferior.h"
#include "target.h"
#include "gdb_string.h"
#include "objfiles.h"
#include "floatformat.h"
#include "symfile.h"		/* for overlay functions */
#include "gdbarch.h"
#include "f-lang.h"
#ifdef HP_IA64
#include "mixed_mode.h"
#endif
#define DTV_OFFSET 8 /* for DTLS */

/* This is used to indicate that we don't know the format of the floating point
   number.  Typically, this is useful for native ports, where the actual format
   is irrelevant, since no conversions will be taking place.  */

const struct floatformat floatformat_unknown;

/* Registers we shouldn't try to store.  */
#if !defined (CANNOT_STORE_REGISTER)
#define CANNOT_STORE_REGISTER(regno) 0
#endif

/* Forward declarations */

#ifdef HP_IA64
extern void ia64_read_register_from_frame (char* , struct frame_info*, int);
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
extern CORE_ADDR fetch_ap_for_gcc_frame (char buf[], struct frame_info *frame, int regnum);
#endif
extern int hppa_pc_in_gcc_function (CORE_ADDR);

/* JAGag24558: jini */
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
extern boolean set_register;
#endif

void write_register_gen (int, char *);

int 
read_relative_register_raw_bytes_for_frame (int regnum, 
					    char *myaddr, 
					    struct frame_info *frame);

extern CORE_ADDR pre_interrupt_bsp (void);
#ifndef HP_IA64
extern void free_dummy_register_array (void);
#endif
static value_ptr
value_from_pieces (struct symbol *sym, struct frame_info *frame);

/* Basic byte-swapping routines.  GDB has needed these for a long time...
   All extract a target-format integer at ADDR which is LEN bytes long.  */

#if TARGET_CHAR_BIT != 8 || HOST_CHAR_BIT != 8
  /* 8 bit characters are a pretty safe assumption these days, so we
     assume it throughout all these swapping routines.  If we had to deal with
     9 bit characters, we would need to make len be in bits and would have
     to re-write these routines...  */
you lose
#endif

LONGEST
extract_signed_integer (void *addr, int len)
{
  LONGEST retval;
  unsigned char *p;
  unsigned char *startaddr = (unsigned char *) SWIZZLE((CORE_ADDR)addr);
  unsigned char *endaddr   = startaddr + len;

  endaddr = (unsigned char *) SWIZZLE((CORE_ADDR)endaddr);

  if (len > (int) sizeof (LONGEST))
    error ("\
That operation is not available on integers of more than %d bytes.",
	   sizeof (LONGEST));

  /* Start at the most significant end of the integer, and work towards
     the least significant.  */
  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    {
      p = startaddr;
      /* Do the sign extension once at the start.  */
      retval = ((LONGEST) * p ^ 0x80) - 0x80;
      for (++p; p < endaddr; ++p)
	retval = (retval << 8) | *p;
    }
  else
    {
      p = endaddr - 1;
      /* Do the sign extension once at the start.  */
      retval = ((LONGEST) * p ^ 0x80) - 0x80;
      for (--p; p >= startaddr; --p)
	retval = (retval << 8) | *p;
    }
  return retval;
}

ULONGEST
extract_unsigned_integer (void *addr, int len)
{
  ULONGEST retval;
  unsigned char *p;
  unsigned char *startaddr = (unsigned char *) SWIZZLE((CORE_ADDR)addr);
  unsigned char *endaddr = startaddr + len;

  endaddr   = (unsigned char *) SWIZZLE((CORE_ADDR)endaddr);

  if (len > (int) sizeof (ULONGEST))
    error ("\
That operation is not available on integers of more than %d bytes.",
	   sizeof (ULONGEST));

  /* Start at the most significant end of the integer, and work towards
     the least significant.  */
  retval = 0;
  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    {
      for (p = startaddr; p < endaddr; ++p)
	retval = (retval << 8) | *p;
    }
  else
    {
      for (p = endaddr - 1; p >= startaddr; --p)
	retval = (retval << 8) | *p;
    }
  return retval;
}

/* Sometimes a long long unsigned integer can be extracted as a
   LONGEST value.  This is done so that we can print these values
   better.  If this integer can be converted to a LONGEST, this
   function returns 1 and sets *PVAL.  Otherwise it returns 0.  */

int
extract_long_unsigned_integer (void *addr, int orig_len, LONGEST *pval)
{
  char *p, *first_addr;
  int len;

  len = orig_len;
  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    {
      for (p = (char *) addr;
	   len > (int) sizeof (LONGEST) && p < (char *) addr + orig_len;
	   p++)
	{
	  if (*p == 0)
	    len--;
	  else
	    break;
	}
      first_addr = p;
    }
  else
    {
      first_addr = (char *) addr;
      for (p = (char *) addr + orig_len - 1;
	   len > (int) sizeof (LONGEST) && p >= (char *) addr;
	   p--)
	{
	  if (*p == 0)
	    len--;
	  else
	    break;
	}
    }

  if (len <= (int) sizeof (LONGEST))
    {
      *pval = (LONGEST) extract_unsigned_integer (first_addr,
						  sizeof (LONGEST));
      return 1;
    }

  return 0;
}


/* Treat the LEN bytes at ADDR as a target-format address, and return
   that address.  ADDR is a buffer in the GDB process, not in the
   inferior.

   This function should only be used by target-specific code.  It
   assumes that a pointer has the same representation as that thing's
   address represented as an integer.  Some machines use word
   addresses, or similarly munged things, for certain types of
   pointers, so that assumption doesn't hold everywhere.

   Common code should use extract_typed_address instead, or something
   else based on POINTER_TO_ADDRESS.  */

CORE_ADDR
extract_address (void *addr, int len)
{
  /* Assume a CORE_ADDR can fit in a LONGEST (for now).  Not sure
     whether we want this to be true eventually.  */
  return (CORE_ADDR) extract_unsigned_integer (addr, len);
}


/* Treat the bytes at BUF as a pointer of type TYPE, and return the
   address it represents.  */
CORE_ADDR
extract_typed_address (void *buf, struct type *type)
{
  /*JAGaf63290, Usha: Method call with typedef'd pointer fails*/
  struct type *target_type = check_typedef (TYPE_TARGET_TYPE (type));

  if (TYPE_CODE (type) != TYPE_CODE_PTR
      && TYPE_CODE (type) != TYPE_CODE_REF
      && TYPE_CODE (target_type) != TYPE_CODE_PTR
      && TYPE_CODE (target_type) != TYPE_CODE_REF)
    internal_error ("findvar.c (extract_typed_address): "
		    "type is not a pointer or reference");

  return POINTER_TO_ADDRESS (type, buf);
}


void
store_signed_integer (void *addr, int len, LONGEST val)
{
  unsigned char *p;
  unsigned char *startaddr = (unsigned char *) SWIZZLE((CORE_ADDR)addr);
  unsigned char *endaddr = startaddr + len;

  endaddr   = (unsigned char *) SWIZZLE((CORE_ADDR)endaddr);

  /* Start at the least significant end of the integer, and work towards
     the most significant.  */
  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    {
      for (p = endaddr - 1; p >= startaddr; --p)
	{
	  *p = val & 0xff;
	  val >>= 8;
	}
    }
  else
    {
      for (p = startaddr; p < endaddr; ++p)
	{
	  *p = val & 0xff;
	  val >>= 8;
	}
    }
}

void
store_unsigned_integer (void *addr, int len, ULONGEST val)
{
  unsigned char *p;
  unsigned char *startaddr = (unsigned char *) SWIZZLE((CORE_ADDR)addr);
  unsigned char *endaddr = startaddr + len;

  endaddr   = (unsigned char *) SWIZZLE((CORE_ADDR)endaddr);

  /* Start at the least significant end of the integer, and work towards
     the most significant.  */
  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    {
      for (p = endaddr - 1; p >= startaddr; --p)
	{
	  *p = val & 0xff;
	  val >>= 8;
	}
    }
  else
    {
      for (p = startaddr; p < endaddr; ++p)
	{
	  *p = val & 0xff;
	  val >>= 8;
	}
    }
}

/* Store the address VAL as a LEN-byte value in target byte order at
   ADDR.  ADDR is a buffer in the GDB process, not in the inferior.

   This function should only be used by target-specific code.  It
   assumes that a pointer has the same representation as that thing's
   address represented as an integer.  Some machines use word
   addresses, or similarly munged things, for certain types of
   pointers, so that assumption doesn't hold everywhere.

   Common code should use store_typed_address instead, or something else
   based on ADDRESS_TO_POINTER.  */
void
store_address (void *addr, int len, LONGEST val)
{
  store_unsigned_integer (addr, len, val);
}


/* Store the address ADDR as a pointer of type TYPE at BUF, in target
   form.  */
void
store_typed_address (void *buf, struct type *type, CORE_ADDR addr)
{ /* QXCR1000874270:gdb crash on C++ expression print.
     FIXME-locate where the target_type is set to null and why, as dereferencing null *target_type causes gdb to crash so returning if target_type is null.It is suspected that it is set during evaluation of an expression*/

   if (type->target_type==NULL)
       return ;

  /*JAGaf63290, Usha: Method call with typedef'd pointer fails*/
  struct type *target_type = check_typedef (TYPE_TARGET_TYPE (type));
  
  if (TYPE_CODE (type) != TYPE_CODE_PTR
      && TYPE_CODE (type) != TYPE_CODE_REF
      && TYPE_CODE (target_type) != TYPE_CODE_PTR
      && TYPE_CODE (target_type) != TYPE_CODE_REF)
    internal_error ("findvar.c (store_typed_address): "
		    "type is not a pointer or reference");

  ADDRESS_TO_POINTER (type, buf, addr);
}

/* Extract the value from the reference pointer and store it as
   pointer value. */
value_ptr
store_reference_pointer (value_ptr arg1)
{
  struct type *type = check_typedef (VALUE_TYPE (arg1));
  struct type *target_type = check_typedef (TYPE_TARGET_TYPE (type));
  register value_ptr v1;
  CORE_ADDR addr = 0;

  if (target_type && (TYPE_CODE (target_type) == TYPE_CODE_PTR) &&
      (TYPE_CODE(TYPE_TARGET_TYPE(target_type)) == TYPE_CODE_CLASS))
    {
      v1 = allocate_value (target_type);
      VALUE_LVAL (v1) = lval_memory;
      VALUE_BFD_SECTION (v1) = VALUE_BFD_SECTION (arg1);
      if (VALUE_LAZY (arg1))
        value_fetch_lazy (arg1);
      addr = extract_address (VALUE_CONTENTS_RAW (arg1),
                                TYPE_LENGTH (type));
      VALUE_ADDRESS (v1) = addr;
      VALUE_LAZY (v1) = 1;
      return v1;
    }
  return 0;

}

/* Extract a floating-point number from a target-order byte-stream at ADDR.
   Returns the value as type DOUBLEST.

   If the host and target formats agree, we just copy the raw data into the
   appropriate type of variable and return, letting the host increase precision
   as necessary.  Otherwise, we call the conversion routine and let it do the
   dirty work.  */

DOUBLEST
extract_floating (void *addr, int len)
{
  DOUBLEST dretval;

  if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (HOST_FLOAT_FORMAT == TARGET_FLOAT_FORMAT)
	{
	  float retval;

	  retval = 0;  /* initialize for compiler warning */
	  memcpy (&retval, addr, sizeof (retval));
	  return retval;
	}
      else
	floatformat_to_doublest (TARGET_FLOAT_FORMAT, addr, &dretval);
    }
  else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (HOST_DOUBLE_FORMAT == TARGET_DOUBLE_FORMAT)
	{
	  double retval = 0;  /* initialize for compiler warning */

	  memcpy (&retval, addr, sizeof (retval));
	  return retval;
	}
      else
	floatformat_to_doublest (TARGET_DOUBLE_FORMAT, addr, &dretval);
    }
  else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (HOST_LONG_DOUBLE_FORMAT == TARGET_LONG_DOUBLE_FORMAT)
	{
	  DOUBLEST retval = 0;  /* initialize for compiler warning */

	  memcpy (&retval, addr, sizeof (retval));
	  return retval;
	}
      else
	floatformat_to_doublest (TARGET_LONG_DOUBLE_FORMAT, addr, &dretval);
    }
  else
    {
      error ("Can't deal with a floating point number of %d bytes.", len);
    }

  return dretval;
}

#ifdef HP_IA64
/* Extract the decimal float value stored at valaddr depending upon
   its size of decimal float.
 */
DECFLOAT *
extract_decfloat (char *valaddr, int len)
{
  DECFLOAT *dval;

  dval = (DECFLOAT *) xmalloc (sizeof(DECFLOAT));

  if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      memcpy (&dval->d32, valaddr, sizeof(dval->d32));
    }
  else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      memcpy (&dval->d64, valaddr, sizeof(dval->d64)); 
    }
  else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      /* If HPUX then extract bits first from higher and followed by lower bits. */
      if (TARGET_BYTE_ORDER == BIG_ENDIAN)
        {
          memcpy (&dval->d128.hi, valaddr, sizeof(dval->d128.hi));
          memcpy (&dval->d128.lo, valaddr + 8, sizeof(dval->d128.lo));
        }
      /* If linux or incl x86 then extract bits stored at lower followed by higher bits. */
      else
	{
          memcpy (&dval->d128.lo, valaddr, sizeof(dval->d128.lo));
          memcpy (&dval->d128.hi, valaddr + 8, sizeof(dval->d128.hi));
	}
    }
  else
    {
      error ("Can't deal with a decimal floating point number of %d bytes.", len);
    }
  return dval;
}
#endif

void
store_floating (void *addr, int len, DOUBLEST val)
{
  if (len * TARGET_CHAR_BIT == TARGET_FLOAT_BIT)
    {
      if (HOST_FLOAT_FORMAT == TARGET_FLOAT_FORMAT)
	{
	  float floatval = val;

	  memcpy (addr, &floatval, sizeof (floatval));
	}
      else
	floatformat_from_doublest (TARGET_FLOAT_FORMAT, &val, addr);
    }
  else if (len * TARGET_CHAR_BIT == TARGET_DOUBLE_BIT)
    {
      if (HOST_DOUBLE_FORMAT == TARGET_DOUBLE_FORMAT)
	{
	  double doubleval = val;

	  memcpy (addr, &doubleval, sizeof (doubleval));
	}
      else
	floatformat_from_doublest (TARGET_DOUBLE_FORMAT, &val, addr);
    }
  else if (len * TARGET_CHAR_BIT == TARGET_LONG_DOUBLE_BIT)
    {
      if (HOST_LONG_DOUBLE_FORMAT == TARGET_LONG_DOUBLE_FORMAT)
	memcpy (addr, &val, sizeof (val));
      else
	floatformat_from_doublest (TARGET_LONG_DOUBLE_FORMAT, &val, addr);
    }
  else
    {
      error ("Can't deal with a floating point number of %d bytes.", len);
    }
}


/* Return the address in which frame FRAME's value of register REGNUM
   has been saved in memory.  Or return zero if it has not been saved.
   If REGNUM specifies the SP, the value we return is actually
   the SP value, not an address where it was saved.  */

CORE_ADDR
find_saved_register (struct frame_info *frame, int regnum)
{
  register struct frame_info *frame1 = NULL;
  register CORE_ADDR addr = 0;
  register int hitonce = 0;

  if (frame == NULL)		/* No regs saved if want current frame */
    return 0;

#ifdef HAVE_REGISTER_WINDOWS
  /* We assume that a register in a register window will only be saved
     in one place (since the name changes and/or disappears as you go
     towards inner frames), so we only call get_frame_saved_regs on
     the current frame.  This is directly in contradiction to the
     usage below, which assumes that registers used in a frame must be
     saved in a lower (more interior) frame.  This change is a result
     of working on a register window machine; get_frame_saved_regs
     always returns the registers saved within a frame, within the
     context (register namespace) of that frame. */

  /* However, note that we don't want this to return anything if
     nothing is saved (if there's a frame inside of this one).  Also,
     callers to this routine asking for the stack pointer want the
     stack pointer saved for *this* frame; this is returned from the
     next frame.  */

  if (REGISTER_IN_WINDOW_P (regnum))
    {
      frame1 = get_next_frame (frame);
      if (!frame1)
	return 0;		/* Registers of this frame are active.  */

      /* Get the SP from the next frame in; it will be this
         current frame.  */
      if (regnum != SP_REGNUM)
	frame1 = frame;

      FRAME_INIT_SAVED_REGS (frame1);
      return frame1->saved_regs[regnum];	/* ... which might be zero */
    }
#endif /* HAVE_REGISTER_WINDOWS */

  /* jini: JAGag24558: Gdb was wrongly populating and returning addr
     for the current frame. This resulted in lval being incorrectly set
     to lval_memory for values to be read in from registers. Avoid this. */
  if (get_prev_frame (frame1) == frame)
    {
      return 0;
    }

  /* Note that this next routine assumes that registers used in
     frame x will be saved only in the frame that x calls and
     frames interior to it.  This is not true on the sparc, but the
     above macro takes care of it, so we should be all right. */
  while (1)
    {
      QUIT;
      frame1 = get_prev_frame (frame1);
#if defined(HP_IA64)
      if (frame1 == 0 || frame1 == frame)
        break;
#else
#ifdef GDB_TARGET_IS_HPPA_20W
      if (frame1 == 0 || hitonce || (frame1 == frame && !(regnum <= 26 && regnum >= 19)))
#else
      if (frame1 == 0 || hitonce || (frame1 == frame && !(regnum <= 26 && regnum >= 23)))
#endif
	break;
#endif

#if !defined(HP_IA64)
#ifdef GDB_TARGET_IS_HPPA_20W
      if(frame1 == frame && regnum <= 26 && regnum >= 19)
#else
      if(frame1 == frame && regnum <= 26 && regnum >= 23)
#endif
        hitonce = 1;
#endif
      FRAME_INIT_SAVED_REGS (frame1);
      if (frame1->saved_regs[regnum])
	addr = frame1->saved_regs[regnum];
    }

  return addr;
}

/* Find register number REGNUM relative to FRAME and put its (raw,
   target format) contents in *RAW_BUFFER.  Set *OPTIMIZED if the
   variable was optimized out (and thus can't be fetched).  Set *LVAL
   to lval_memory, lval_register, or not_lval, depending on whether
   the value was fetched from memory, from a register, or in a strange
   and non-modifiable way (e.g. a frame pointer which was calculated
   rather than fetched).  Set *ADDRP to the address, either in memory
   on as a REGISTER_BYTE offset into the registers array.

   Note that this implementation never sets *LVAL to not_lval.  But
   it can be replaced by defining GET_SAVED_REGISTER and supplying
   your own.

   The argument RAW_BUFFER must point to aligned memory.  */

void
default_get_saved_register (char *raw_buffer, int *optimized,
	                    CORE_ADDR *addrp,
     		            struct frame_info *frame,
		            int regnum, enum lval_type *lval)
{
  CORE_ADDR addr;

  if (!target_has_registers)
    error ("No registers.");

  /* Normal systems don't optimize out things with register numbers.  */
  if (optimized != NULL)
    *optimized = 0;
  addr = find_saved_register (frame, regnum);
  if (addr != 0)
    {
      if (lval != NULL)
	*lval = lval_memory;
      if (regnum == SP_REGNUM)
	{
	  if (raw_buffer != NULL)
	    {
	      /* Put it back in target format.  */
	      store_address (raw_buffer, REGISTER_RAW_SIZE (regnum),
			     (LONGEST) addr);
	    }
	  if (addrp != NULL)
	    *addrp = 0;
	  return;
	}
      if (raw_buffer != NULL)
	read_memory (addr, raw_buffer, REGISTER_RAW_SIZE (regnum));
    }
  else
    {
      if (lval != NULL)
	*lval = lval_register;
      addr = REGISTER_BYTE (regnum);
      if (raw_buffer != NULL)
#ifdef HP_IA64
        /* Bindu 05/18/01: For IA64 we have a problem with getting the
           address of a floating point register in the upper frames. 
    	   So, we get the value of the floating point registers from the 
           frame's unwind context. */
        /* Also get grs and brs from unwind context. PFS and CFM too. */
        if ((frame != 0) && (get_next_frame (frame))
             && (((regnum > FR0_REGNUM + 1) && (regnum <= FRLAST_REGNUM))
                 || ((regnum > GR0_REGNUM + 1) && (regnum <= GR0_REGNUM + 31))
		 || ((regnum > BR0_REGNUM) && (regnum <= BR0_REGNUM + 7))
		 || (regnum == AR0_REGNUM + 64)
		 || (regnum == CFM_REGNUM))
	   )
          {
            if (lval != NULL)
              *lval = not_lval; /* don't allow people to write for now.
				   FIXME!!! */
 	    /* As of now, unwind library does not give us the address
	       at which this register resides. So it's not possible to fix
	       this. */
            ia64_read_register_from_frame (raw_buffer, frame, regnum);
          }
        else
#endif
          read_register_gen (regnum, raw_buffer);
    }
  if (addrp != NULL)
    *addrp = addr;
}

#if !defined (GET_SAVED_REGISTER)
#define GET_SAVED_REGISTER(raw_buffer, optimized, addrp, frame, regnum, lval) \
  default_get_saved_register(raw_buffer, optimized, addrp, frame, regnum, lval)
#endif
void
get_saved_register (char *raw_buffer, int *optimized, CORE_ADDR *addrp,
                    struct frame_info *frame, int regnum,
                    enum lval_type *lval)
{
  GET_SAVED_REGISTER (raw_buffer, optimized, addrp, frame, regnum, lval);
}

/* Copy the bytes of register REGNUM, relative to the input stack frame,
   into our memory at MYADDR, in target byte order.
   The number of bytes copied is REGISTER_RAW_SIZE (REGNUM).

   Returns 1 if could not be read, 0 if could.  */

int
read_relative_register_raw_bytes_for_frame (int regnum, char *myaddr,
                                             struct frame_info *frame)
{
  int optim;

  get_saved_register (myaddr, &optim, (CORE_ADDR *) NULL, frame,
		      regnum, (enum lval_type *) NULL);

  if (register_valid[regnum] < 0)
    return 1;			/* register value not available */

  return optim;
}

/* Copy the bytes of register REGNUM, relative to the current stack frame,
   into our memory at MYADDR, in target byte order.
   The number of bytes copied is REGISTER_RAW_SIZE (REGNUM).

   Returns 1 if could not be read, 0 if could.  */

int
read_relative_register_raw_bytes (int regnum, char *myaddr)
{
  return read_relative_register_raw_bytes_for_frame (regnum, myaddr,
						     selected_frame);
}

#if !defined(HP_IA64)
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64) 
extern int arg_size[4];
#else 
extern int arg_size[8];
#endif
#endif

#ifdef HP_IA64
/* Mixed mode equivalent of 'value_of_register'. */
value_ptr
mixed_mode_value_of_register (int regnum)
{
  CORE_ADDR addr;
  int optim = 0, is_nat = 0;
  register value_ptr reg_val;
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  enum lval_type lval;
  struct type *regnum_type = NULL;

  mixed_mode_get_saved_register (raw_buffer, &optim, &addr,
		      selected_frame, regnum, &lval);

  if (is_swizzled)
    regnum_type = (regnum < PA32_FP4_REGNUM)? builtin_type_int:
                                              builtin_type_double;
  else
    regnum_type = (regnum < PA64_FP4_REGNUM)? builtin_type_unsigned_long_long:
                                              builtin_type_double;

  reg_val = allocate_value (regnum_type);
  memcpy ((void *)VALUE_CONTENTS_RAW (reg_val),
          (void *)raw_buffer,
          TYPE_LENGTH (regnum_type));
  VALUE_LVAL (reg_val) = lval;
  VALUE_ADDRESS (reg_val) = addr;
  VALUE_REGNO (reg_val) = regnum;
  VALUE_AVAILABILITY (reg_val) = VA_AVAILABLE;

  /* Setting modifiable to be zero since this is
     corefile debugging. */
  reg_val->modifiable = 0;
  return reg_val;

}
#endif

/* Return a `value' with the contents of register REGNUM
   in its virtual format, with the type specified by
   REGISTER_VIRTUAL_TYPE.  

   NOTE: returns NULL if register value is not available.
   Caller will check return value or die!  */

value_ptr
value_of_register (int regnum)
{
  CORE_ADDR addr;
  int optim = 0, is_nat = 0;
  register value_ptr reg_val;
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  enum lval_type lval;
  /* For argument registers, argsize will specify the number of 
     bytes to be read. */
  int argsize = 0;

#ifdef HP_IA64
  if (selected_frame && selected_frame->pa_save_state_ptr)
    {
      return mixed_mode_value_of_register (regnum);
    }
#endif

/* Sunil 051216 JAGaf71194  On a PA 2.0 machine, the register set is
   64 bits wide (even on a narrow chip.) gdb thinks registers are 4 
   bytes wide on a PA2.0N machine (see config/pa/tm-hppa.h). A 32 
   bit program normally references only the 32 bit register file 
   unless it is compiled with the DA2.0N compiler option. 
	As a result, the following fix has been made to work on 64
   bit machines in general.*/

#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
  /* jini: JAGag24558: Read from the save state and concatenate only
     if we are reading in the register value. Setting a value in the
     register results in the code under 'handle_it_normal_way' getting
     executed. */
  if (   !set_register
       && pa_is_64bit()
       && regnum != 30
       && regnum != 33
       && selected_frame_level == 0)
    {
      int regaddr;
      unsigned int offset;
      register int i;
      long raw_val[2] = {0,0};
      long long reg_value = 0;
      char reg_val_string[32];
      int size = 0;

      extern CORE_ADDR register64_offset;
      extern char *core_save_state_contents;

      /* when doing corefile target_has_execution=0, target_has_stack=1.  
       * With a corefile do not call ptrace instead read the data from 
       * core_save_state_contents.
       */
      if(!target_has_execution && target_has_stack)
	{
	   /* Sunil 25/1/2005 
   	      For now I am putting in a temporary work around to print only the
	      narrow mode register from a core file.  */
           char raw_regs[REGISTER_BYTES];
   	   int i;

  	   /* Make a copy of gdb's save area (may cause actual
              reads from the target). */
  	   for (i = 0; i < NUM_REGS; i++)
    	     read_relative_register_raw_bytes (i, 
                                               raw_regs + REGISTER_BYTE (i));

 	   if ( (selected_frame == get_current_frame ()) && 
	        ( (regnum >= 1) && (regnum <= 63)))
	     {
	       raw_val[0] = *(long *) (core_save_state_contents + 
				       register64_offset + 8*regnum);
	       raw_val[1] = *(long *) (core_save_state_contents + 
				       register64_offset + 8*regnum+4);
	     }
	    raw_val[1] = *(long *) (raw_regs + REGISTER_BYTE (regnum));
	    goto read_core_save_state_contents;
	}

      /* We are running on PA2.0N. */
      if (!target_has_registers)
        error ("No registers.");
 
      /* Essentially we are reading the 2 halves from the save_state 
         structure (gdb save state area) and assemble the result into 64 
         bit value.
      */
  
#if HAVE_STRUCT_SAVE_STATE_T == 1 && HAVE_STRUCT_MEMBER_SS_WIDE == 1
      save_state_t temp;

      offset = ((int) &temp.ss_wide) - ((int) &temp);
      regaddr = offset + regnum * 8;
#endif

      for (i = 0; i < 2; i++)
        {
          errno = 0;
          raw_val[i] = call_ptrace (PT_RUREGS, inferior_pid,
                                   (PTRACE_ARG3_TYPE) regaddr, 0);
          if (errno != 0)
            {
              /* Warning, not error, in case we are attached; sometimes the
                 kernel doesn't let us at the registers. In this case we 
                 cant read from the save state structure. FIXME */
              goto handle_it_normal_way;
            }

          regaddr += sizeof (long);
        }

read_core_save_state_contents: 
      if (raw_val[0] == 0)
        {
          sprintf(reg_val_string, "%x", raw_val[1]);
          sscanf(reg_val_string, "%lx", &reg_value);
          size = sizeof(long);
        }
      else
        {
          sprintf(reg_val_string, "%x%8.8x", raw_val[0], raw_val[1]);
          sscanf(reg_val_string, "%llx", &reg_value);
          size = 2 * sizeof(long);
        }

      reg_val = allocate_value ((size == 8)? builtin_type_int64:
                                             builtin_type_int);

      memcpy (VALUE_CONTENTS_RAW (reg_val), (char*) &reg_value, size);
   
      /* Normal systems don't optimize out things with register numbers.  */
      /* JAGag24558: jini: the 'addr' value was incorrectly getting
         assigned to the address of reg_value. 

         Fix this to have 'addr', 'lval' and 'optim' obtained from
         get_saved_register. Note: raw_buffer is NOT used to populate
         VALUE_CONTENTS_RAW (reg_val). */
      get_saved_register (raw_buffer, &optim, &addr,
                          selected_frame, regnum, &lval);

      VALUE_LVAL (reg_val) = lval;
      VALUE_ADDRESS (reg_val) = addr;
      VALUE_REGNO (reg_val) = regnum;
      /* optim is set to 2 by get_saved_register() to indicate that the 
         register is  a NaT */
      VALUE_AVAILABILITY (reg_val) = optim ? 
        (optim == NOT_A_THING ? VA_NOT_A_THING : VA_OPTIMIZED_OUT) :
        VA_AVAILABLE;
      reg_val->modifiable = ! CANNOT_STORE_REGISTER (regnum);
      return reg_val;
    }
handle_it_normal_way:

#endif /* end !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)*/

  get_saved_register (raw_buffer, &optim, &addr,
		      selected_frame, regnum, &lval);

  if (register_valid[regnum] < 0)
    return NULL;		/* register value not available */

#if !defined(HP_IA64)
#if defined(GDB_TARGET_IS_HPPA_20W)
  if ((regnum <= 26 && regnum >= 19) && 
      !hppa_pc_in_gcc_function (selected_frame->pc)) 
#else
  if (selected_frame_level != 0 && regnum <= 26 && regnum >= 23)
#endif
    {
      switch (regnum)
        {
          case 26:
		  argsize = arg_size[0];
		  break;
          case 25:
		  argsize = arg_size[1];
		  break;
          case 24:
		  argsize = arg_size[2];
		  break;
          case 23:
		  argsize = arg_size[3];
		  break;
#if defined(GDB_TARGET_IS_HPPA_20W)
          case 22:
		  argsize = arg_size[4];
		  break;
          case 21:
		  argsize = arg_size[5];
		  break;
          case 20:
		  argsize = arg_size[6];
		  break;
          case 19:
		  argsize = arg_size[7];
		  break;
#endif
        }

      if (argsize == 1)
  	reg_val = allocate_value (builtin_type_char);
      else if (argsize == 2)
	reg_val = allocate_value (builtin_type_short);
      else if (argsize == 4)
	reg_val = allocate_value (builtin_type_int);
      else if(argsize == 8)
	reg_val = allocate_value (builtin_type_int64);
      else
  	reg_val = allocate_value (REGISTER_VIRTUAL_TYPE (regnum));
    }
  else
#endif
    reg_val = allocate_value (REGISTER_VIRTUAL_TYPE (regnum));

  /* Convert raw data to virtual format if necessary.  */

  if (REGISTER_CONVERTIBLE (regnum))
    {
      REGISTER_CONVERT_TO_VIRTUAL (regnum, REGISTER_VIRTUAL_TYPE (regnum),
				   raw_buffer, VALUE_CONTENTS_RAW (reg_val));
    }
  else if (REGISTER_RAW_SIZE (regnum) == REGISTER_VIRTUAL_SIZE (regnum))
    {
      memcpy (VALUE_CONTENTS_RAW (reg_val), raw_buffer,
	      REGISTER_RAW_SIZE (regnum));
      if (   IS_TARGET_LRE 
	  && !IS_FLIP_REG (regnum) 
	  && REGISTER_RAW_SIZE (regnum) == 8)
	{
	  /* The register is not flipped, but the vlalue is */
	  endian_flip (VALUE_CONTENTS_RAW (reg_val), 
					   REGISTER_RAW_SIZE (regnum));
	}
    }
  else
    internal_error ("Register \"%s\" (%d) has conflicting raw (%d) "
                    "and virtual (%d) size",
		    REGISTER_NAME (regnum),
		    regnum,
		    REGISTER_RAW_SIZE (regnum),
		    REGISTER_VIRTUAL_SIZE (regnum));
#ifdef HP_IA64
  if (regnum == AR0_REGNUM + 17){
	* ((CORE_ADDR *)(void *) raw_buffer) = selected_frame->rse_fp;
	memcpy (VALUE_CONTENTS_RAW (reg_val), raw_buffer,
		    REGISTER_RAW_SIZE (regnum));
	if (   IS_TARGET_LRE 
	    && !IS_FLIP_REG (regnum) 
	    && REGISTER_RAW_SIZE (regnum) == 8)
	    {
	      /* The register is not flipped, but the vlalue is */
	      endian_flip (VALUE_CONTENTS_RAW (reg_val), 
					       REGISTER_RAW_SIZE (regnum));
	    }
  }

#endif
  VALUE_LVAL (reg_val) = lval;
  VALUE_ADDRESS (reg_val) = addr;
  VALUE_REGNO (reg_val) = regnum;
  /* optim is set to 2 by get_saved_register() to indicate that the 
     register is  a NaT */
  VALUE_AVAILABILITY (reg_val) = optim ? 
    (optim == NOT_A_THING ? VA_NOT_A_THING : VA_OPTIMIZED_OUT) :
    VA_AVAILABLE;
  reg_val->modifiable = ! CANNOT_STORE_REGISTER (regnum);
  return reg_val;
}

/* Low level examining and depositing of registers.

   The caller is responsible for making
   sure that the inferior is stopped before calling the fetching routines,
   or it will get garbage.  (a change from GDB version 3, in which
   the caller got the value from the last stop).  */

/* Contents and state of the registers (in target byte order). */

char *registers;

//For JAGaf13527
#ifndef HP_IA64
char *dummy_register;
/* sizeof dummy_register array */
int dummy_register_size;
int dummyno;
int dummyread;
extern int dummy_frame_read;
extern int call_dummy_frame;
#endif

/* VALID_REGISTER is non-zero if it has been fetched, -1 if the
   register value was not available. */

signed char *register_valid;

/* The thread/process associated with the current set of registers.  For now,
   -1 is special, and means `no current process'.  */
int registers_pid = -1;

/* Indicate that registers may have changed, so invalidate the cache.  */

void
registers_changed ()
{
  int i;
  int numregs = ARCH_NUM_REGS;

  registers_pid = -1;

  /* Force cleanup of any alloca areas if using C alloca instead of
     a builtin alloca.  This particular call is used to clean up
     areas allocated by low level target code which may build up
     during lengthy interactions between gdb and the target before
     gdb gives control to the user (ie watchpoints).  */
  alloca (0);

  for (i = 0; i < numregs; i++)
    register_valid[i] = 0;

  if (registers_changed_hook)
    registers_changed_hook ();
}

/* Indicate that all registers have been fetched, so mark them all valid.  */
void
registers_fetched ()
{
  int i;
  int numregs = ARCH_NUM_REGS;
  for (i = 0; i < numregs; i++)
    register_valid[i] = 1;
}

/* read_register_bytes and write_register_bytes are generally a *BAD*
   idea.  They are inefficient because they need to check for partial
   updates, which can only be done by scanning through all of the
   registers and seeing if the bytes that are being read/written fall
   inside of an invalid register.  [The main reason this is necessary
   is that register sizes can vary, so a simple index won't suffice.]
   It is far better to call read_register_gen and write_register_gen
   if you want to get at the raw register contents, as it only takes a
   regno as an argument, and therefore can't do a partial register
   update.

   Prior to the recent fixes to check for partial updates, both read
   and write_register_bytes always checked to see if any registers
   were stale, and then called target_fetch_registers (-1) to update
   the whole set.  This caused really slowed things down for remote
   targets.  */

/* Copy INLEN bytes of consecutive data from registers
   starting with the INREGBYTE'th byte of register data
   into memory at MYADDR.  */

void
read_register_bytes (int inregbyte, char *myaddr, int inlen)
{
  int inregend = inregbyte + inlen;
  int regno;

  if (registers_pid != inferior_pid)
    {
      registers_changed ();
      registers_pid = inferior_pid;
    }

  /* See if we are trying to read bytes from out-of-date registers.  If so,
     update just those registers.  */

  for (regno = 0; regno < NUM_REGS; regno++)
    {
      int regstart, regend;

      if (register_valid[regno])
	continue;

      if (REGISTER_NAME (regno) == NULL || *REGISTER_NAME (regno) == '\0')
	continue;

      regstart = REGISTER_BYTE (regno);
      regend = regstart + REGISTER_RAW_SIZE (regno);

      if (regend <= inregbyte || inregend <= regstart)
	/* The range the user wants to read doesn't overlap with regno.  */
	continue;

      /* We've found an invalid register where at least one byte will be read.
         Update it from the target.  */
      target_fetch_registers (regno);

      if (!register_valid[regno])
	error ("read_register_bytes:  Couldn't update register %d.", regno);
    }

  if (myaddr != NULL)
    memcpy (myaddr, &registers[inregbyte], inlen);
}

/* Read register REGNO into memory at MYADDR, which must be large enough
   for REGISTER_RAW_BYTES (REGNO).  Target byte-order.
   If the register is known to be the size of a CORE_ADDR or smaller,
   read_register can be used instead.  */
void
read_register_gen (int regno, char *myaddr)
{
  if (registers_pid != inferior_pid)
    {
      registers_changed ();
      registers_pid = inferior_pid;
    }

  if (!register_valid[regno])
    target_fetch_registers (regno);
  memcpy (myaddr, &registers[REGISTER_BYTE (regno)],
	  REGISTER_RAW_SIZE (regno));
}

/* Write register REGNO at MYADDR to the target.  MYADDR points at
   REGISTER_RAW_BYTES(REGNO), which must be in target byte-order.  */

void
write_register_gen (int regno, char *myaddr)
{
  char flip_buf[12];
  int size;

  /* On the sparc, writing %g0 is a no-op, so we don't even want to change
     the registers array if something writes to this register.  */
  if (CANNOT_STORE_REGISTER (regno))
    return;

#ifdef HP_MXN

  /* bindu 020502: pulled in from PA. */
  /* srikanth, If the thread is unbound and not running currently,
     do not allow register writes. Allowing register writes into
     the user space save state structure could interfere with the
     thread's capabaility to switch itself back in.

     Yes, this sounds rather arbitrary and authoritarian. But I think
     this is no biggie. See that by definition a thread that is
     switched out is stopped deep inside libpthread and is no where
     near user code. If users ever complain about not being to
     write to the registers of threads that are switched out, we'll
     revisit this issue. Until then, no siree. 001212.
  */

  if (get_lwp_for (inferior_pid) == 0)
    error ("Thread is switched out by the user space scheduler. "
           "Operation disallowed.");
#endif

  if (registers_pid != inferior_pid)
    {
      registers_changed ();
      registers_pid = inferior_pid;
    }

  size = REGISTER_RAW_SIZE (regno);

  if (IS_FLIP_REG(regno))
    {
      memcpy (flip_buf, myaddr, size);
      endian_flip (flip_buf, size);
    }

  /* If we have a valid copy of the register, and new value == old value,
     then don't bother doing the actual store. */

  if (IS_FLIP_REG(regno))
    {
      if (register_valid[regno]
	  && memcmp (flip_buf, myaddr, size) == 0)
	return;
    }
  else
    {
      if (register_valid[regno]
	  && memcmp (&registers[REGISTER_BYTE (regno)], myaddr, size) == 0)
	return;
    }

  target_prepare_to_store ();

  if (IS_FLIP_REG(regno))
    {
      memcpy (&registers[REGISTER_BYTE (regno)], flip_buf, size);
    }
  else
    memcpy (&registers[REGISTER_BYTE (regno)], myaddr, size);

  register_valid[regno] = 1;

  target_store_registers (regno);

  /* ttrace needs the write in big-endian, but when we read it again,
     we need it to be in little-endian, so we store it the other way.
   */

  if (IS_FLIP_REG(regno))
    memcpy (&registers[REGISTER_BYTE (regno)], myaddr, size);

}

/* Copy INLEN bytes of consecutive data from memory at MYADDR
   into registers starting with the MYREGSTART'th byte of register data.  */

void
write_register_bytes (int myregstart, char *myaddr, int inlen)
{
  int myregend = myregstart + inlen;
  int regno;

  target_prepare_to_store ();

  /* Scan through the registers updating any that are covered by the range
     myregstart<=>myregend using write_register_gen, which does nice things
     like handling threads, and avoiding updates when the new and old contents
     are the same.  */

  for (regno = 0; regno < NUM_REGS; regno++)
    {
      int regstart, regend;

      regstart = REGISTER_BYTE (regno);
      regend = regstart + REGISTER_RAW_SIZE (regno);

      /* Is this register completely outside the range the user is writing?  */
      if (myregend <= regstart || regend <= myregstart)
	/* do nothing */ ;		

      /* Is this register completely within the range the user is writing?  */
      else if (myregstart <= regstart && regend <= myregend)
	write_register_gen (regno, myaddr + (regstart - myregstart));

      /* The register partially overlaps the range being written.  */
      else
	{
	  char regbuf[MAX_REGISTER_RAW_SIZE];
	  /* What's the overlap between this register's bytes and
             those the caller wants to write?  */
	  int overlapstart = max (regstart, myregstart);
	  int overlapend   = min (regend,   myregend);

	  /* We may be doing a partial update of an invalid register.
	     Update it from the target before scribbling on it.  */
	  read_register_gen (regno, regbuf);

	  memcpy (registers + overlapstart,
		  myaddr + (overlapstart - myregstart),
		  overlapend - overlapstart);

	  target_store_registers (regno);
	}
    }
}


/* Return the raw contents of register REGNO, regarding it as an integer.  */
/* This probably should be returning LONGEST rather than CORE_ADDR.  */

CORE_ADDR
read_register (int regno)
{
  CORE_ADDR result;

  if (registers_pid != inferior_pid)
    {
      registers_changed ();
      registers_pid = inferior_pid;
    }

  if (!register_valid[regno])
    target_fetch_registers (regno);

  if (!IS_TARGET_LRE || IS_FLIP_REG (regno)){
#ifndef HP_IA64 //For JAGaf13527
   if((regno == 0x3) && (call_dummy_frame==1)){
    return ((CORE_ADDR)
	    extract_unsigned_integer (&dummy_register[REGISTER_BYTE (--dummyread)], REGISTER_RAW_SIZE (regno)));}
   else 
#endif  
    return ((CORE_ADDR)
	    extract_unsigned_integer (&registers[REGISTER_BYTE (regno)],
				      REGISTER_RAW_SIZE (regno)));}
  else
    {
      memcpy (&result, 
	      &registers[REGISTER_BYTE (regno)],
	      REGISTER_RAW_SIZE (regno));
      return result;
    }
}


#ifdef DP_SAVED_IN_OBJFILE

/* Return the current DP value.  If we aren't running, then return the DP
   value for the main program.
   */

CORE_ADDR
get_dp_reg (void)
{
  CORE_ADDR     dp_reg_value;

  dp_reg_value = read_register(DP_REGNUM);
  if (dp_reg_value == 0)
    {
      dp_reg_value = symfile_objfile->saved_dp_reg;
    }
  return dp_reg_value;
} /* end get_dp_reg */
#endif

CORE_ADDR
read_register_pid (int regno, int pid)
{
  int save_pid;
  CORE_ADDR retval;

  if (pid == inferior_pid)
    return read_register (regno);

  save_pid = inferior_pid;

  inferior_pid = pid;

  retval = read_register (regno);

  inferior_pid = save_pid;

  return retval;
}

/* Store VALUE, into the raw contents of register number REGNO.
   This should probably write a LONGEST rather than a CORE_ADDR */

void
write_register (int regno, LONGEST val)
{
  PTR buf;
  int size;

  /* On the sparc, writing %g0 is a no-op, so we don't even want to change
     the registers array if something writes to this register.  */
  if (CANNOT_STORE_REGISTER (regno))
    return;

  if (registers_pid != inferior_pid)
    {
      registers_changed ();
      registers_pid = inferior_pid;
    }

  size = REGISTER_RAW_SIZE (regno);
  buf = (PTR) alloca (size);
  store_signed_integer (buf, size, (LONGEST) val);

  /* If we have a valid copy of the register, and new value == old value,
     then don't bother doing the actual store. */

  if (register_valid[regno]
      && memcmp (&registers[REGISTER_BYTE (regno)], buf, size) == 0)
    return;

  target_prepare_to_store ();

  if (IS_FLIP_REG(regno))
   {
     char flip_buf[8];
     memcpy (flip_buf, buf, 8);
     endian_flip (flip_buf, 8);
     memcpy (&registers[REGISTER_BYTE (regno)], flip_buf, size);
   }
  else
    memcpy (&registers[REGISTER_BYTE (regno)], buf, size);

//For JAGaf13527
#ifndef HP_IA64
   if ((regno == 0x3) && (call_dummy_frame == 1))
     { 
       /* JAGag04959: To avoid array overflow. */
       if (REGISTER_BYTE(dummyno) >= dummy_register_size)
         { 
           free_dummy_register_array ();
           dummy_register_size = REGISTER_BYTE(dummyno + 10);
           dummy_register = (char *) xrealloc (dummy_register, dummy_register_size * sizeof(char));
         }
       memcpy (&dummy_register[REGISTER_BYTE(dummyno)],buf,size);
       dummyno = dummyno + 1;
       dummyread = dummyno;
     } 
#endif

  target_store_registers (regno);

  /* ttrace needs the write in big-endian, but when we read it again,
     we need it to be in little-endian, so we store it the other way.
     Note that store_signed_integer flips the value in buf around for
     LRE.
   */

  if (IS_FLIP_REG(regno))
    memcpy (&registers[REGISTER_BYTE (regno)], buf, size);

  register_valid[regno] = 1;
}

void
write_register_pid (int regno, CORE_ADDR val, int pid)
{
  int save_pid;

  if (pid == inferior_pid)
    {
      write_register (regno, val);
      return;
    }

  save_pid = inferior_pid;

  inferior_pid = pid;

  write_register (regno, val);

  inferior_pid = save_pid;
}

/* Flip the endianness of an integer */
void
endian_flip(void * buf, int sizeof_buf)
{
  int i,j,k;
  char *ptr=buf;
  for (i = 0; i < sizeof_buf/2 ; i++) 
    {
      j = ptr[i];
      ptr[i] = ptr[sizeof_buf -1 -i];
      ptr[sizeof_buf -i - 1] = j;
    }
}     

/* Flip a structure.  A description is a set of { count, size } flip_desc_t
   descriptors ending with { 0, 0 }.  A descriptor is saying the next
   thing is count fields each of size size.  Size should be a power of 2.  
   It is 1 for things like strings which are not to be flipped.
 */

void flip_struct(void* buf, flip_desc_t* buf_desc_p)
{
  char *	ptr;
  int		count;
  int		size;

  ptr = (char*) buf;
  for (count = buf_desc_p->count;  count > 0;  count = (++buf_desc_p)->count)
    {
      size = buf_desc_p->size;
      if (size > 1)
	for (; count > 0;  count--)
	  {
	    endian_flip (ptr, size);
	    ptr+= size;
	  }
      else
	ptr += size * count;
    } 
} /* end flip_struct */

/* Record that register REGNO contains VAL.
   This is used when the value is obtained from the inferior or core dump,
   so there is no need to store the value there.

   If VAL is a NULL pointer, then it's probably an unsupported register.  We
   just set it's value to all zeros.  We might want to record this fact, and
   report it to the users of read_register and friends.
 */

void
supply_register (int regno, char *val)
{
#if 1
  if (registers_pid != inferior_pid)
    {
      registers_changed ();
      registers_pid = inferior_pid;
    }
#endif

  /* The HP-UX kernel is BIG ENDIAN and gives us registers in BIG ENDIAN
     format. Flip it to make it appear as LITTLE_ENDIAN for LRE apps */
  if (IS_FLIP_REG(regno))
    endian_flip (val, 8);

  register_valid[regno] = 1;
  if (val)
    memcpy (&registers[REGISTER_BYTE (regno)], val, REGISTER_RAW_SIZE (regno));
  else
    memset (&registers[REGISTER_BYTE (regno)], '\000', REGISTER_RAW_SIZE (regno));

  /* On some architectures, e.g. HPPA, there are a few stray bits in some
     registers, that the rest of the code would like to ignore.  */
#ifdef CLEAN_UP_REGISTER_VALUE
  CLEAN_UP_REGISTER_VALUE (regno, &registers[REGISTER_BYTE (regno)]);
#endif
}


/* This routine is getting awfully cluttered with #if's.  It's probably
   time to turn this into READ_PC and define it in the tm.h file.
   Ditto for write_pc.

   1999-06-08: The following were re-written so that it assumes the
   existance of a TARGET_READ_PC et.al. macro.  A default generic
   version of that macro is made available where needed.

   Since the ``TARGET_READ_PC'' et.al. macro is going to be controlled
   by the multi-arch framework, it will eventually be possible to
   eliminate the intermediate read_pc_pid().  The client would call
   TARGET_READ_PC directly. (cagney). */

#ifndef TARGET_READ_PC
#define TARGET_READ_PC generic_target_read_pc
#endif

CORE_ADDR
generic_target_read_pc (int pid)
{
#ifdef PC_REGNUM
  if (PC_REGNUM >= 0)
    {
      CORE_ADDR pc_val = ADDR_BITS_REMOVE ((CORE_ADDR) read_register_pid (PC_REGNUM, pid));
      return pc_val;
    }
#else
  internal_error ("generic_target_read_pc");
  return 0;
#endif
}

CORE_ADDR
read_pc_pid (int pid)
{
  int saved_inferior_pid;
  CORE_ADDR pc_val;

  /* In case pid != inferior_pid. */
  saved_inferior_pid = inferior_pid;
  inferior_pid = pid;

  pc_val = TARGET_READ_PC (pid);

  inferior_pid = saved_inferior_pid;
  return SWIZZLE(pc_val);
}

CORE_ADDR
read_pc ()
{
  return read_pc_pid (inferior_pid);
}

#ifndef TARGET_WRITE_PC
#define TARGET_WRITE_PC generic_target_write_pc
#endif

void
generic_target_write_pc (CORE_ADDR pc, int pid)
{
#ifdef PC_REGNUM
  if (PC_REGNUM >= 0)
    write_register_pid (PC_REGNUM, pc, pid);
  if (NPC_REGNUM >= 0)
    write_register_pid (NPC_REGNUM, pc + 4, pid);
  if (NNPC_REGNUM >= 0)
    write_register_pid (NNPC_REGNUM, pc + 8, pid);
#else
  internal_error ("generic_target_write_pc");
#endif
}

void
write_pc_pid (CORE_ADDR pc, int pid)
{
  int saved_inferior_pid;

  /* In case pid != inferior_pid. */
  saved_inferior_pid = inferior_pid;
  inferior_pid = pid;

  TARGET_WRITE_PC (pc, pid);

  inferior_pid = saved_inferior_pid;
}

void
write_pc (CORE_ADDR pc)
{
  write_pc_pid (pc, inferior_pid);
}

/* Cope with strage ways of getting to the stack and frame pointers */

#ifndef TARGET_READ_SP
#define TARGET_READ_SP generic_target_read_sp
#endif

CORE_ADDR
generic_target_read_sp ()
{
  CORE_ADDR value = 0;
#ifdef SP_REGNUM
  if (SP_REGNUM >= 0)
    value = read_register (SP_REGNUM);
#else
  internal_error ("generic_target_read_sp");
#endif
  return value;
}

CORE_ADDR
read_sp ()
{
  return TARGET_READ_SP ();
}

#ifndef TARGET_WRITE_SP
#define TARGET_WRITE_SP generic_target_write_sp
#endif

void
generic_target_write_sp (CORE_ADDR val)
{
#ifdef SP_REGNUM
  if (SP_REGNUM >= 0)
    {
      write_register (SP_REGNUM, val);
      return;
    }
#else
  internal_error ("generic_target_write_sp");
#endif
}

void
write_sp (CORE_ADDR val)
{
  TARGET_WRITE_SP (val);
}

#ifndef TARGET_READ_FP
#define TARGET_READ_FP generic_target_read_fp
#endif

CORE_ADDR
generic_target_read_fp ()
{
#ifdef FP_REGNUM
  if (FP_REGNUM >= 0)
    return read_register (FP_REGNUM);
#else
  internal_error ("generic_target_read_fp");
  return NULL;
#endif
}

CORE_ADDR
read_fp ()
{
  return TARGET_READ_FP ();
}

#ifndef TARGET_WRITE_FP
#define TARGET_WRITE_FP generic_target_write_fp
#endif

void
generic_target_write_fp (CORE_ADDR val)
{
#ifdef FP_REGNUM
  if (FP_REGNUM >= 0)
    {
      write_register (FP_REGNUM, val);
      return;
    }
#else
  internal_error ("generic_target_write_fp");
#endif
}

void
write_fp (CORE_ADDR val)
{
  TARGET_WRITE_FP (val);
}


/* Given a pointer of type TYPE in target form in BUF, return the
   address it represents.  */
CORE_ADDR
unsigned_pointer_to_address (struct type *type, void *buf)
{
  return extract_address (buf, TYPE_LENGTH (type));
}

CORE_ADDR
signed_pointer_to_address (struct type *type, void *buf)
{
  return extract_signed_integer (buf, TYPE_LENGTH (type));
}

/* Given an address, store it as a pointer of type TYPE in target
   format in BUF.  */
void
unsigned_address_to_pointer (struct type *type, void *buf, CORE_ADDR addr)
{
  store_address (buf, TYPE_LENGTH (type), addr);
}

void
address_to_signed_pointer (struct type *type, void *buf, CORE_ADDR addr)
{
  store_signed_integer (buf, TYPE_LENGTH (type), addr);
}

/* Will calling read_var_value or locate_var_value on SYM end
   up caring what frame it is being evaluated relative to?  SYM must
   be non-NULL.  */
int
symbol_read_needs_frame (struct symbol *sym)
{
  switch (SYMBOL_CLASS (sym))
    {
      /* All cases listed explicitly so that gcc -Wall will detect it if
         we failed to consider one.  */

    case LOC_COMPUTED:
    case LOC_COMPUTED_ARG:
      {
	/* Does this location expression need a frame? - find out. */
	struct location_funcs *symfuncs = SYMBOL_LOCATION_FUNCS (sym);
	return (symfuncs->read_needs_frame) (sym);
      }
      break;

    case LOC_REGISTER:
    case LOC_ARG:
    case LOC_REF_ARG:
    case LOC_REGPARM:
    case LOC_REGPARM_ADDR:
    case LOC_LOCAL:
    case LOC_LOCAL_ARG:
    case LOC_BASEREG:
    case LOC_BASEREG_ARG:
    case LOC_BASEREG_REF_ARG:
    case LOC_THREAD_LOCAL_STATIC:
    case LOC_THREAD_LOCAL_DYNAMIC:
    case LOC_THREAD_LOCAL_STATIC_DPREL:
    case LOC_THREAD_LOCAL_DYNAMIC_DPREL:
#ifdef HPPA_DOC
    case LOC_REG_ADD_CONST:
    case LOC_REG_MUL_CONST:
    case LOC_CONST_SUB_REG:
#endif
      return 1;

    case LOC_STATIC:
      if (SYMBOL_LANGUAGE (sym) == language_fortran)
	return SYMBOL_IS_LOCAL (sym);

#ifdef HPPA_DOC
    case LOC_MEM_ADD_CONST:
    case LOC_MEM_MUL_CONST: 
#endif
    case LOC_UNDEF:
    case LOC_CONST:
    case LOC_INDIRECT:
    case LOC_TYPEDEF:

    case LOC_LABEL:
      /* Getting the address of a label can be done independently of the block,
         even if some *uses* of that address wouldn't work so well without
         the right frame.  */

    case LOC_BLOCK:
    case LOC_CONST_BYTES:
    case LOC_UNRESOLVED:
    case LOC_OPTIMIZED_OUT:
      return 0;
    }
  return 1;
}

CORE_ADDR
read_var_addr (register struct symbol *var, struct frame_info *frame)
{
  register value_ptr v;
  CORE_ADDR addr;

  v = read_var_value (var, frame);
  if (v)
    addr = VALUE_ADDRESS (v);
  else
    addr = 0;
  return addr;
}

#ifdef HPPA_DOC
/* CM: Copy an unsigned integer from a char buffer into another char buffer.
   Respect endianness, buffer sizes, and integer overflow. */

static void
assign_unsigned_integer(char *dest, size_t dest_len, char *src, size_t src_len)
{
  union {
    char is_little;
    int set_to_one;
  } endian;
  size_t copy_len;

  endian.set_to_one = 1;

  /* Zero destination */
  memset (dest, 0, dest_len);

  /* Length to copy is minimum of buffer lengths */
  copy_len = (dest_len < src_len) ? dest_len : src_len;

  /* If big endian, must adjust src and dest */
  if (!endian.is_little) {
    if (dest_len < src_len) {
      src = src + (src_len - dest_len);
    } else {
      dest = dest + (dest_len - src_len);
    }
  }

  /* Copy */
  memcpy (dest, src, copy_len);
}
#endif /* HPPA_DOC */


/* Retrieve the frame pointer, regardless if it is represented by a
   location list or is in the usual location.  */

static CORE_ADDR
get_frame_ptr (struct frame_info *frame, CORE_ADDR norm_fp, struct symbol *aux_sym)
{
  CORE_ADDR addr;

  struct symbol *cur_func = find_pc_function (frame->pc);

  if ((cur_func == NULL) || (SYMBOL_LOCATION_BATON(cur_func) == NULL))
    {
      /* no location list, eval frame base address normally */
      addr = norm_fp;
    }
  else
    {
      /* frame base address is a location list, compute from location
	 list evaluator functions squirreled away in the current
         function's symbol struct.  JAGag32281, "gdb cannot print right
         arguments with gcc 4.1". Carl Burch, 03/22/2007 */

      /* JAGag44937: 32-bit debugging fails with gdb/wdb 5.5 and later. 
         and
         JAGag45745: WDB 5.7 stilll has DWARF issues; Why does gdb not work
         properly with gcc.
         Since frame base address is an 'address', a symbol with type code
         TYPE_CODE_PTR should be used to retrieve the frame base address.
         Hence, create a pseudo symbol with type code as TYPE_CODE_PTR, and
         have the newly introduced field 'aux_symbol' pointing to it, so that
         routines down the line can use this pseudo symbol to retrieve the
         correct address.
         jini. 7 Aug 2007 */
      struct symbol *pseudo_sym = NULL;
      struct symbol sym = *cur_func;
      register value_ptr v;
      if (aux_sym)
        {
          pseudo_sym = (struct symbol*) alloca (sizeof (struct symbol));
          sym.aux_value.loc.aux_symbol = pseudo_sym;
          SYMBOL_TYPE (pseudo_sym) = lookup_pointer_type (SYMBOL_TYPE (aux_sym));
        }
      SYMBOL_CLASS (&sym) = LOC_COMPUTED;
      v = read_var_value (&sym, frame);
      addr = 0;
#ifdef HP_IA64
      if (   (v)
          && aux_sym
          && (   (   (lval_register == VALUE_LVAL (v))
                  && (REGISTER_BYTE (VALUE_REGNO (v)) == VALUE_ADDRESS (v)))
              || (   (lval_memory == VALUE_LVAL (v)
                  && (VALUE_REGNO (v) >= GR0_REGNUM + NUM_ST_GRS)
                  && (VALUE_REGNO (v) < frame->n_rse_regs
                                        + GR0_REGNUM + NUM_ST_GRS)
                  && (add_to_bsp (frame->rse_fp,
                                  VALUE_REGNO (v) - GR0_REGNUM - NUM_ST_GRS) ==
                      VALUE_ADDRESS (v))))))
        {
          /* VALUE_ADDRESS contains the offset representing the relevant 
             register. Use VALUE_CONTENTS_RAW where the frame base address
             is actually stored. */
          int len = TYPE_LENGTH (SYMBOL_TYPE (pseudo_sym));
          memcpy (((char *) &addr) + sizeof (addr) - len,
                   VALUE_CONTENTS_RAW (v), len);
        }
      else 
#endif
        if (v)
          {
            addr = VALUE_ADDRESS (v);
          }
      sym.aux_value.loc.aux_symbol = NULL;
    }
  return addr;
}

/* Given a struct symbol for a variable,
   and a stack frame id, read the value of the variable
   and return a (pointer to a) struct value containing the value. 
   If the variable cannot be found, return a zero pointer.
   If FRAME is NULL, use the selected_frame.  */

value_ptr
read_var_value2 (register struct symbol *var, struct frame_info *frame,
                 int from_tty)
{
  register value_ptr v;
  struct type *type = SYMBOL_TYPE (var);
  CORE_ADDR addr = 0;
  CORE_ADDR addr1 = 0;
  register int len;

  if (!var)
    return 0;

  v = allocate_value (type);
  VALUE_LVAL (v) = lval_memory;	/* The most likely possibility.  */
  VALUE_BFD_SECTION (v) = SYMBOL_BFD_SECTION (var);
 
  len = TYPE_LENGTH (type); 
 
  if (frame == NULL)
    frame = selected_frame;

  if (SYMBOL_NBR_PIECES(var))
    return value_from_pieces (var, frame);

  switch (SYMBOL_CLASS (var))
    {
    case LOC_CONST:
      /* Put the constant back in target format.  */
      store_signed_integer (VALUE_CONTENTS_RAW (v), len,
			    (LONGEST) SYMBOL_VALUE (var));
      VALUE_LVAL (v) = not_lval;
      return v;

    case LOC_LABEL:
      /* Put the constant back in target format.  */
      if (overlay_debugging)
	{
	  CORE_ADDR addr
	    = symbol_overlayed_address (SYMBOL_VALUE_ADDRESS (var),
					SYMBOL_BFD_SECTION (var));
	  store_typed_address (VALUE_CONTENTS_RAW (v), type, addr);
	}
      else
	store_typed_address (VALUE_CONTENTS_RAW (v), type,
			      SYMBOL_VALUE_ADDRESS (var));
      VALUE_LVAL (v) = not_lval;
      return v;

    case LOC_CONST_BYTES:
      {
	char *bytes_addr;
	bytes_addr = SYMBOL_VALUE_BYTES (var);
	memcpy (VALUE_CONTENTS_RAW (v), bytes_addr, len);
	VALUE_LVAL (v) = not_lval;
	return v;
      }

    case LOC_STATIC:
      if (overlay_debugging)
	addr = symbol_overlayed_address (SYMBOL_VALUE_ADDRESS (var),
					 SYMBOL_BFD_SECTION (var));
      else
	addr = SYMBOL_VALUE_ADDRESS (var);
      break;

    case LOC_INDIRECT:
      /* The import slot does not have a real address in it from the
         dynamic loader (dld.sl on HP-UX), if the target hasn't begun
         execution yet, so check for that. */
      /* pes,JAGae13566 The above stmt is partially true for corefiles. Because shared libs
	  gets mapped to a real address when a corefile is added.
	  So check  target_has_execution and also check whether it
	  is corefile.If not corefile(!target_has_stack), don't emit this error.
       */
      if ( !target_has_execution && !target_has_stack )
	{
	  if (!from_tty)
	    return NULL;
	  error ("\
  Attempt to access variable defined in different shared object or load module when\n\
  addresses have not been bound by the dynamic loader. Try again when executable is running.");
	}

      addr = SYMBOL_VALUE_ADDRESS (var);
      if (addr == 0)
        {
          /* RM: this indicates that we don't have an address for the
	     import slot. Go find the real address. */
#ifdef FIND_VAR_IN_SOLIB
          addr = FIND_VAR_IN_SOLIB(SYMBOL_NAME(var));
          if (debug_traces)
            printf ("%s found at address %08lx\n", SYMBOL_NAME(var), addr);
#endif          
        }
      else
        {
          addr = read_memory_unsigned_integer
            (addr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
        }
#ifdef GDB_TARGET_IS_HPPA_20W
      /* The offset of variable in a Fortran common block needs to be added 
       * after the base address of the variable has been read in
       */
      if (SYMBOL_LANGUAGE (var) == language_fortran)
        {
          addr += SYMBOL_VAR_OFFSET (var);
        }
#endif
      break;

    case LOC_ARG:
      if (frame == NULL)
	return 0;
      addr = get_frame_ptr (frame, FRAME_ARGS_ADDRESS (frame), var);
      if (!addr)
	return 0;
      addr += SYMBOL_VALUE (var);
      break;

    case LOC_REF_ARG:
      if (frame == NULL)
	return 0;
      addr = get_frame_ptr (frame, FRAME_ARGS_ADDRESS (frame), var);
      if (!addr)
	return 0;
      addr += SYMBOL_VALUE (var);
      addr = read_memory_unsigned_integer
	(addr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
      break;

    case LOC_LOCAL:
    case LOC_LOCAL_ARG:
      if (frame == NULL)
	return 0;
      addr = get_frame_ptr (frame, FRAME_LOCALS_ADDRESS (frame), var);
      addr += SYMBOL_VALUE (var);
      break;

    case LOC_BASEREG:
    case LOC_BASEREG_ARG:
    case LOC_BASEREG_REF_ARG:
      {
	char buf[MAX_REGISTER_RAW_SIZE];
#ifdef GDB_TARGET_IS_HPPA_20W
        /* GCC does not store previous ap if it is not required to call another function. 
           So, for the innermost frame, fetch ap from GR 29. */
	addr = fetch_ap_for_gcc_frame (buf, frame, SYMBOL_BASEREG (var));
	if (!addr && ((SYMBOL_BASEREG (var) >= 3 && SYMBOL_BASEREG (var) < 19) ||
             frame == get_current_frame ()))
#endif
	get_saved_register (buf, NULL, NULL, frame, SYMBOL_BASEREG (var),
			    NULL);
#ifdef GDB_TARGET_IS_HPPA_20W
        else if (!((SYMBOL_BASEREG (var) >= 3 && SYMBOL_BASEREG (var) < 19) ||
             frame == get_current_frame ()))
          {
            printf_filtered ("Unable to retrieve argument value.");
            return NULL;
          }
#endif
	addr = extract_address (buf, REGISTER_RAW_SIZE (SYMBOL_BASEREG (var)));
	addr += SYMBOL_VALUE (var);
#ifdef GDB_TARGET_IS_HPPA_20W
	if (SYMBOL_DEBUG_LOCATION_OFFSET (var) != 0) 
	  {
	    addr = read_memory_unsigned_integer (addr,
						 TARGET_PTR_BIT / 
						 TARGET_CHAR_BIT);
	    addr += SYMBOL_DEBUG_LOCATION_OFFSET (var);
	  }
#endif
  	if (SYMBOL_CLASS (var) == LOC_BASEREG_REF_ARG)
	  addr = read_memory_unsigned_integer
		  (addr, 
		   (int)(is_swizzled ? 4 : TARGET_PTR_BIT / TARGET_CHAR_BIT ));
	break;
      }

    /* Bindu:
     * If static tls,
     * TP
     *  |
     *  ----------------------------------------
     *  |<- tls_start_addr->| load module |
     *  |                   |-------------|
     *  |                   |   | a |     |
     *  ----------------------------------------
     */

    case LOC_THREAD_LOCAL_STATIC:
      {
        char buf[MAX_REGISTER_RAW_SIZE];

        get_saved_register (buf, NULL, NULL, frame, SYMBOL_BASEREG (var),
                            NULL);
        addr = extract_address (buf, REGISTER_RAW_SIZE (SYMBOL_BASEREG (var)));
        addr += SYMBOL_VALUE (var);
        break;
      }

    /* Bindu 112801:
     * If dtls,
     * checkout the "IA64 Runtime Architecture Supplement:
     *                Thread Local Storage"
     * If tls_index < 0
     *   addr = *(TP + tls_index) + symbol_addr
     * else
     *   addr = *(*TP + tls_index) + symbol_addr
     */

    case LOC_THREAD_LOCAL_DYNAMIC:
      {
        char buf[MAX_REGISTER_RAW_SIZE];

        get_saved_register (buf, NULL, NULL, frame, SYMBOL_BASEREG (var),
                            NULL);
        addr = extract_address (buf, REGISTER_RAW_SIZE (SYMBOL_BASEREG (var)));

        if ((int)SYMBOL_VAR_OFFSET (var) < 0)
          {
	    addr += SYMBOL_VAR_OFFSET (var) * sizeof(CORE_ADDR);
	  }
	else
	  {
#ifndef HP_IA64 
            /* Dinesh 10/25/02, The 16 byte thread control
               block is allocated at the negative offset 
               relative to TP(CR27), at TP - 16 */

            addr = addr - 16; /* To reach the dynmaic DTV */
#endif /* !HP_IA64 */
	    addr = read_memory_unsigned_integer (addr,
                                TARGET_PTR_BIT / TARGET_CHAR_BIT);
            addr = addr + SYMBOL_VAR_OFFSET (var) * sizeof(CORE_ADDR);
	  }
	addr = read_memory_unsigned_integer (addr,
                                TARGET_PTR_BIT / TARGET_CHAR_BIT);
#ifndef HP_IA64
        if (addr == 0)
            addr = find_tls_get_addr ( (int)SYMBOL_VAR_OFFSET (var),
                                        SYMBOL_VALUE(var));
        else
#endif /* !HP_IA64 */
           addr += SYMBOL_VALUE(var);
        break;
      }


    case LOC_THREAD_LOCAL_STATIC_DPREL:
      {
        char buf[MAX_REGISTER_RAW_SIZE];

        get_saved_register (buf, NULL, NULL, frame, SYMBOL_BASEREG (var),
                            NULL);
        addr = extract_address (buf, REGISTER_RAW_SIZE (SYMBOL_BASEREG (var)));
        /* A thread local storage is accessed by first loading an offset
           from a dp-relatvie offset, then adding the offset to the
           tp-register (CR27).

           MFCTL        CR27, tmp1
           ...
           ADDIL        LT'var,dp,r1
           LDD          RT'var(r1),tmp2
           LDD          tmp2(tmp1),tmp3
        */
        /* Dinesh 05/22/02 , DP_REGNUM is already added to
           the SYMBOL_VALUE of the variable in the function
           hpread_process_one_debug_symbol hp-symtab-read.c
           so no need to read register now.
        */

        addr += read_memory_unsigned_integer
                 ( SYMBOL_VALUE(var),
                   TARGET_PTR_BIT / TARGET_CHAR_BIT);

        break;
      }
    
    case LOC_THREAD_LOCAL_DYNAMIC_DPREL:
      {

        char buf[MAX_REGISTER_RAW_SIZE];

        get_saved_register (buf, NULL, NULL, frame, SYMBOL_BASEREG (var),
                            NULL);
        addr = extract_address (buf, REGISTER_RAW_SIZE (SYMBOL_BASEREG (var)));
        if ((int)SYMBOL_VAR_OFFSET (var) < 0)
          {
	    addr1 = addr + SYMBOL_VAR_OFFSET (var) * sizeof(CORE_ADDR);
	  }
	else
	  {

            /* Dinesh 10/25/02, The 16 byte thread control
               block is allocated at the negative offset 
               relative to TP(CR27), at TP - 16 */

            addr = addr - 16; 
	    addr = read_memory_unsigned_integer (addr,
                                TARGET_PTR_BIT / TARGET_CHAR_BIT);
	    addr1 = addr + SYMBOL_VAR_OFFSET (var) * sizeof(CORE_ADDR);
	  }
	addr = read_memory_unsigned_integer (addr1,
                             TARGET_PTR_BIT / TARGET_CHAR_BIT);

        if (addr == 0)
          {
#ifndef HP_IA64
            addr = find_tls_get_addr ( SYMBOL_VAR_OFFSET (var),
                                           SYMBOL_VALUE(var));
#endif
            addr = read_memory_unsigned_integer (addr1,
                                TARGET_PTR_BIT / TARGET_CHAR_BIT);
          }
         /* Dinesh, we need to add DTV_OFFSET(8 bytes) because for
            each DTLS symbol, there are two DLT slots. The first
            one contains the module index and the second one contains
            the symbol address and for static TLS, there was only one
            DLT slot. For more information,see
            http://cllweb.cup.hp.com/linker/cross_projects/Dyn_TLS
                                                        /index.html
         */
        addr += read_memory_unsigned_integer
                 ( SYMBOL_VALUE(var) + DTV_OFFSET,
                   TARGET_PTR_BIT / TARGET_CHAR_BIT);
        break;
      }


    case LOC_TYPEDEF:
      if (!from_tty)
        return NULL;
      error ("Cannot look up value of a typedef");
      break;

    case LOC_BLOCK:
      if (overlay_debugging)
	VALUE_ADDRESS (v) = symbol_overlayed_address
	  (SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (var))), SYMBOL_BFD_SECTION (var));
      else
	VALUE_ADDRESS (v) = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (var)));
      return v;

    case LOC_REGISTER:
    case LOC_REGPARM:
    case LOC_REGPARM_ADDR:
      {
	int regno = (int)SYMBOL_VALUE (var);
	value_ptr regval;

	if (frame == NULL)
	  return 0;

	if (SYMBOL_CLASS (var) == LOC_REGPARM_ADDR)
	  {
	    regval = value_from_register (lookup_pointer_type (type),
					  regno,
					  frame);

	    if (regval == NULL)
	      {
		if (!from_tty)
		  return NULL;
		error ("Value of register variable not available.");
	      }

	    addr = value_as_pointer (regval);
	    VALUE_LVAL (v) = lval_memory;
	  }
	else
	  {
	    regval = value_from_register (type, regno, frame);

	    if (regval == NULL)
	      {
		if (!from_tty)
		  return NULL;
		error ("Value of register variable not available.");
	      }
	    return regval;
	  }
      }
      break;

    case LOC_COMPUTED:
    case LOC_COMPUTED_ARG:
      {
	/* Read in the value of this location expression. */
	struct location_funcs *funcs = SYMBOL_LOCATION_FUNCS (var);

	if (frame == 0 && (funcs->read_needs_frame) (var))
	  return 0;
	return (funcs->read_variable) (var, frame);

      }
      break;

    case LOC_UNRESOLVED:
      {
	struct minimal_symbol *msym;
	struct symbol *sym_var = NULL;

	/* First look for a resolved symbol with this name in global blocks. */
  	sym_var = lookup_symbol_resolved (SYMBOL_NAME (var));
	if (sym_var)
	  {
	    var = sym_var;
	    return read_var_value (var, frame);
	  }

	/* If no resolved symbol is found in symbol tables, check in minimal symbol
	   table. This happens when the real variable is declared in a shared library
	   which does not have debug info. */
	msym = lookup_minimal_symbol (SYMBOL_NAME (var), NULL, NULL);
	if (msym == NULL)
	  return 0;
	if (overlay_debugging)
	  addr = symbol_overlayed_address (SYMBOL_VALUE_ADDRESS (msym),
					   SYMBOL_BFD_SECTION (msym));
	else
	  addr = SYMBOL_VALUE_ADDRESS (msym);
      }
      break;

#ifdef HPPA_DOC
    case LOC_REG_ADD_CONST:
    case LOC_REG_MUL_CONST:
    case LOC_CONST_SUB_REG:
      {
        char buf[MAX_REGISTER_RAW_SIZE];

        get_saved_register (buf, NULL, NULL, frame, SYMBOL_BASEREG (var),
                            NULL);
        addr = extract_address (buf, REGISTER_RAW_SIZE (SYMBOL_BASEREG (var)));

        if (SYMBOL_CLASS(var) == LOC_REG_ADD_CONST)
          addr += SYMBOL_VALUE (var);
        else if (SYMBOL_CLASS(var) == LOC_REG_MUL_CONST)
          addr *= SYMBOL_VALUE (var);
        else
          addr =  SYMBOL_VALUE(var) - addr;

        assign_unsigned_integer((char *) VALUE_CONTENTS_RAW (v),
                                len,
                                (char *) &addr,
                                sizeof(addr));
        VALUE_LVAL (v) = not_lval;
        return v;
      }
      break;

    case LOC_MEM_ADD_CONST:
    case LOC_MEM_MUL_CONST:
      {
        addr = read_memory_unsigned_integer
                  (SYMBOL_MEMADDR(var), TARGET_PTR_BIT / TARGET_CHAR_BIT);

        if (SYMBOL_CLASS(var) == LOC_MEM_ADD_CONST)
          addr += SYMBOL_VALUE(var);
        else
          addr *= SYMBOL_VALUE(var); 

        assign_unsigned_integer((char *) VALUE_CONTENTS_RAW (v),
                                len,
                                (char *) &addr,
                                sizeof(addr));
        VALUE_LVAL (v) = not_lval;
        return v;
      }
      break;
#endif /* HPPA_DOC */

    case LOC_OPTIMIZED_OUT:
      VALUE_LVAL (v) = not_lval;
      VALUE_AVAILABILITY (v) = VA_OPTIMIZED_OUT;
      return v;

    default:
      if (!from_tty)
	return NULL;
      error ("Cannot look up value of a botched symbol.");
      break;
    }

  /* JAGae68862: To fixup the address of the array descriptor
   * in the type specific fortran stuff.
   */

  if (TYPE_CODE(type) == TYPE_CODE_ARRAY_DESC )
  {
    f_fixup_array_desc_type_array_addr (type, addr);
  }

  VALUE_ADDRESS (v) = addr;
  VALUE_LAZY (v) = 1;
  return v;
}

/* Old, default version, IS from_tty */
value_ptr
read_var_value (register struct symbol *var, struct frame_info *frame)
{
#ifdef INLINE_SUPPORT
/* if we are in an inlined frame, do not 
   attempt to read variables from that frames
   saved registers as it is a dummy frame and 
   there are no saved registers. To get the
   registers, go down the frame stack till you
   find a non-inlined frame. All variables which
   are visible in inlined functions will be saved
   in the first non-inlined frame.
*/
  struct frame_info *temp_fp = NULL;

  for (temp_fp = frame; temp_fp && temp_fp->inline_idx > 0;
       temp_fp = get_prev_frame(temp_fp))
    ;
  return read_var_value2 (var, temp_fp, 1);
#else
  return read_var_value2 (var, frame, 1);
#endif
}

/* Copy bytes from a symbol piece to its value destination.
   Return FALSE on failure, TRUE on success.
 */

static int 
copy_from_piece (int has_float80,
	         struct frame_info *frame, 
	         char* to_addr, 
		 symbol_piece_t* piece)
{
  CORE_ADDR		addr;
  CORE_ADDR 		from_addr;
  enum lval_type 	lval;
  int 			optim;
  char*			reg_val_ptr;
  char 			raw_buffer[MAX_REGISTER_RAW_SIZE];
  struct type*		piece_type;
  double		val;

  switch (piece->piece_size)
    {
      case TARGET_FLOAT_BIT / TARGET_CHAR_BIT:	
		piece_type = builtin_type_float;
		break;

      case TARGET_DOUBLE_BIT / TARGET_CHAR_BIT:	
		piece_type = builtin_type_double;
		break;

      case TARGET_LONG_DOUBLE_BIT / TARGET_CHAR_BIT:
		if (has_float80)
		  piece_type = builtin_type_float80;
		else
		  piece_type = builtin_type_long_double;
		break;

      default:	warning ("Report to HP Internal error in copy_from_piece.");
		piece_type = builtin_type_float;
		break;
    }

  if (piece->piece_type == PIECE_LOC_REGISTER)
    { // Copy bytes out of the regster value 
       get_saved_register (raw_buffer, 
			   &optim, 
			   &addr, 
			   frame, 
			   (int)piece->arg1, 
			   &lval);
#ifdef REGISTER_CONVERT_TO_VIRTUAL
       if (REGISTER_CONVERTIBLE (piece->arg1))
	 {
	   REGISTER_CONVERT_TO_VIRTUAL (piece->arg1, 
					piece_type, 
					raw_buffer,
					to_addr);
	 }
       else
#endif
       memcpy (to_addr, raw_buffer, piece->piece_size);
       return 1; // true, success
    }

  /* The remaining types copy memory, calculate from_addr */

  if (piece->piece_type == PIECE_LOC_STATIC)
    {  // Copy bytes from the given address
      from_addr = piece->arg1;
    }
  else if (piece->piece_type == PIECE_LOC_BASEREG)
    { // The address is the value of register arg1 plus the offset arg2
      get_saved_register (raw_buffer, 
			   &optim, 
			   &addr, 
			   frame, 
			   (int)piece->arg1, 
			   &lval);
      memcpy (&from_addr, raw_buffer, sizeof(CORE_ADDR));
      if (IS_TARGET_LRE && IS_FLIP_REG (piece->arg1))
	{ // the address in the register was fipped.
	  endian_flip (&from_addr, sizeof(CORE_ADDR));
	}
      from_addr += piece->arg2;
    }
  else
    {
      error ("Cannot get value of a botched symbol.");
    }
  return ! target_read_memory (from_addr, to_addr, piece->piece_size);
} // end copy_from_piece

/* Return a value of type TYPE where the value was stored in pieces.
   See piece_list in symtab.h.  Patterned on value_from_register.
   Returns NULL if the value is not available.
   The caller needs to check the return value.
 */

static value_ptr
value_from_pieces (struct symbol *sym, struct frame_info *frame)
{
  int			bytes_copied = 0;
  int 			idx;
  int 			len;
  char*			to_addr;
  struct type *		type;
  value_ptr 		value;

  type = SYMBOL_TYPE(sym);
  value = allocate_value (type);
  CHECK_TYPEDEF (type);
  len = TYPE_LENGTH (type);

  to_addr = VALUE_CONTENTS_RAW (value);
  for (idx = 0;  idx < SYMBOL_NBR_PIECES(sym);  idx++)
    {
      if (! copy_from_piece (
		   TYPE_CODE (SYMBOL_TYPE(sym)) 
		== TYPE_CODE (builtin_type_float80_complex),
		frame, to_addr, 
		&SYMBOL_PIECE_LIST(sym)[idx]))
	return NULL; // failure 
      to_addr += SYMBOL_PIECE_LIST(sym)[idx].piece_size;
      bytes_copied += SYMBOL_PIECE_LIST(sym)[idx].piece_size;
    }
  if (len != bytes_copied)
    {
      warning ("JAGaf49324 encountered - wrong size");
      if (len > bytes_copied)
	error ("JAGaf49324 caused memory corruption.");
    }

  VALUE_FRAME (value) = FRAME_FP (frame);
  VALUE_LVAL (value) = lval_register;   // sort of
  VALUE_NBR_PIECES (value) = SYMBOL_NBR_PIECES(sym);
  VALUE_PIECE_LIST (value) = SYMBOL_PIECE_LIST(sym);

  return value;
} // end value_from_pieces

/* Return a value of type TYPE, stored in register REGNUM, in frame
   FRAME.

   NOTE: returns NULL if register value is not available.
   Caller will check return value or die!  */

value_ptr
value_from_register (struct type *type, int regnum,
                     struct frame_info *frame)
{
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  CORE_ADDR addr;
  int optim;
  value_ptr v = allocate_value (type);
  char *value_bytes = 0;
  int value_bytes_copied = 0;
  int num_storage_locs;
  enum lval_type lval;
  int len;

  CHECK_TYPEDEF (type);
  len = TYPE_LENGTH (type);

  VALUE_REGNO (v) = regnum;

  num_storage_locs = (len > REGISTER_VIRTUAL_SIZE (regnum) ?
		      ((len - 1) / REGISTER_RAW_SIZE (regnum)) + 1 :
		      1);
#ifdef TARGET_FLOAT80_BIT
  if (   (   type->code == TYPE_CODE_FLOAT80
          || type->code == TYPE_CODE_FLOATHPINTEL)
      && REGISTER_RAW_SIZE (regnum) == 12)
    num_storage_locs = 1;  /* The value is just in 1 FP register */
#endif

  if (num_storage_locs > 1
#ifdef GDB_TARGET_IS_H8500
      || TYPE_CODE (type) == TYPE_CODE_PTR
#endif
    )
    {
      /* Value spread across multiple storage locations.  */

      int local_regnum;
      int mem_stor = 0, reg_stor = 0;
      int mem_tracking = 1;
      CORE_ADDR last_addr = 0;
      CORE_ADDR first_addr = 0;

      value_bytes = (char *) alloca (len + MAX_REGISTER_RAW_SIZE);

      /* Copy all of the data out, whereever it may be.  */

#ifdef GDB_TARGET_IS_H8500
/* This piece of hideosity is required because the H8500 treats registers
   differently depending upon whether they are used as pointers or not.  As a
   pointer, a register needs to have a page register tacked onto the front.
   An alternate way to do this would be to have gcc output different register
   numbers for the pointer & non-pointer form of the register.  But, it
   doesn't, so we're stuck with this.  */

      if (TYPE_CODE (type) == TYPE_CODE_PTR
	  && len > 2)
	{
	  int page_regnum;

	  switch (regnum)
	    {
	    case R0_REGNUM:
	    case R1_REGNUM:
	    case R2_REGNUM:
	    case R3_REGNUM:
	      page_regnum = SEG_D_REGNUM;
	      break;
	    case R4_REGNUM:
	    case R5_REGNUM:
	      page_regnum = SEG_E_REGNUM;
	      break;
	    case R6_REGNUM:
	    case R7_REGNUM:
	      page_regnum = SEG_T_REGNUM;
	      break;
	    }

	  value_bytes[0] = 0;
	  get_saved_register (value_bytes + 1,
			      &optim,
			      &addr,
			      frame,
			      page_regnum,
			      &lval);

	  if (register_valid[page_regnum] == -1)
	    return NULL;	/* register value not available */

	  if (lval == lval_register)
	    reg_stor++;
	  else
	    mem_stor++;
	  first_addr = addr;
	  last_addr = addr;

	  get_saved_register (value_bytes + 2,
			      &optim,
			      &addr,
			      frame,
			      regnum,
			      &lval);

	  if (register_valid[regnum] == -1)
	    return NULL;	/* register value not available */

	  if (lval == lval_register)
	    reg_stor++;
	  else
	    {
	      mem_stor++;
	      mem_tracking = mem_tracking && (addr == last_addr);
	    }
	  last_addr = addr;
	}
      else
#endif /* GDB_TARGET_IS_H8500 */
	for (local_regnum = regnum;
	     value_bytes_copied < len;
	     (value_bytes_copied += REGISTER_RAW_SIZE (local_regnum),
	      ++local_regnum))
	  {
	    get_saved_register (value_bytes + value_bytes_copied,
				&optim,
				&addr,
				frame,
				local_regnum,
				&lval);

	    if (register_valid[local_regnum] == -1)
	      return NULL;	/* register value not available */

	    if (regnum == local_regnum)
	      first_addr = addr;
	    if (lval == lval_register)
	      reg_stor++;
	    else
	      {
		mem_stor++;

		mem_tracking =
		  (mem_tracking
		   && (regnum == local_regnum
		       || addr == last_addr));
	      }
	    last_addr = addr;
	  }

      if ((reg_stor && mem_stor)
	  || (mem_stor && !mem_tracking))
	/* Mixed storage; all of the hassle we just went through was
	   for some good purpose.  */
	{
	  VALUE_LVAL (v) = lval_reg_frame_relative;
	  VALUE_FRAME (v) = FRAME_FP (frame);
	  VALUE_FRAME_REGNUM (v) = regnum;
	}
      else if (mem_stor)
	{
	  VALUE_LVAL (v) = lval_memory;
	  VALUE_ADDRESS (v) = first_addr;
	}
      else if (reg_stor)
	{
	  VALUE_LVAL (v) = lval_register;
	  VALUE_ADDRESS (v) = first_addr;
	}
      else
	internal_error ("value_from_register: Value not stored anywhere!");

      /* optim is set to 2 by get_saved_register() to indicate that the 
         register is  a NaT */
      VALUE_AVAILABILITY (v) = optim ? 
        (optim == NOT_A_THING ? VA_NOT_A_THING : VA_OPTIMIZED_OUT) :
        VA_AVAILABLE;

      /* Any structure stored in more than one register will always be
         an integral number of registers.  Otherwise, you'd need to do
         some fiddling with the last register copied here for little
         endian machines.  */

      /* Copy into the contents section of the value.  */
      memcpy (VALUE_CONTENTS_RAW (v), value_bytes, len);

      /* Finally do any conversion necessary when extracting this
         type from more than one register.  */
#ifdef REGISTER_CONVERT_TO_TYPE
      REGISTER_CONVERT_TO_TYPE (regnum, type, VALUE_CONTENTS_RAW (v));
#endif
      return v;
    }

  /* Data is completely contained within a single register.  Locate the
     register's contents in a real register or in core;
     read the data in raw format.  */

  get_saved_register (raw_buffer, &optim, &addr, frame, regnum, &lval);

  if (register_valid[regnum] == -1)
    return NULL;		/* register value not available */

  /* optim is set to 2 by get_saved_register() to indicate that the 
     register is  a NaT */
  VALUE_AVAILABILITY (v) = optim ? 
    (optim == NOT_A_THING ? VA_NOT_A_THING : VA_OPTIMIZED_OUT) :
    VA_AVAILABLE;
  VALUE_LVAL (v) = lval;
  VALUE_ADDRESS (v) = addr;

  /* Convert raw data to virtual format if necessary.  */

  if (REGISTER_CONVERTIBLE (regnum) && TYPE_CODE (type) != TYPE_CODE_FLOATHPINTEL)
    {
      REGISTER_CONVERT_TO_VIRTUAL (regnum, type,
				   raw_buffer, VALUE_CONTENTS_RAW (v));
    }
  else
    {
      /* Raw and virtual formats are the same for this register.  */

      if (TARGET_BYTE_ORDER == BIG_ENDIAN && len < REGISTER_RAW_SIZE (regnum))
	{
	  /* Big-endian, and we want less than full size.  */
	  VALUE_OFFSET (v) = REGISTER_RAW_SIZE (regnum) - len;
	}
      if (TYPE_CODE (type) == TYPE_CODE_FLOATHPINTEL)
        {
	  /* __fpreg has the same format as a fp register except it is 16 byte
          long rather than 12 bytes. */
	  memset (VALUE_CONTENTS_RAW (v), 0, len);
          memcpy (VALUE_CONTENTS_RAW (v) + 4, raw_buffer + VALUE_OFFSET (v), len-4);
        }
      else
        memcpy (VALUE_CONTENTS_RAW (v), raw_buffer + VALUE_OFFSET (v), len);
    }

  return v;
}

/* Given a struct symbol for a variable or function,
 * and a stack frame id, 
 * return a (pointer to a) struct value containing the properly typed
 * address.  
 * 
 * If from_tty is FALSE, do not give warnings or errors and return NULL
 * to indicate failure.  locate_var_value2 was derrived from locate_var_value
 * which now sets from_tty to TRUE and calls locate_var_value2.
 */

value_ptr
locate_var_value2 (register struct symbol *var, struct frame_info *frame,
                   int from_tty)
{
  CORE_ADDR addr = 0;
  struct type *type = SYMBOL_TYPE (var);
  value_ptr lazy_value;

  /* Evaluate it first; if the result is a memory address, we're fine.
     Lazy evaluation pays off here. */

  lazy_value = read_var_value2 (var, frame, 0);
  if (lazy_value == 0) {
    if (!from_tty)
      return NULL;
    error ("Address of \"%s\" is unknown.", SYMBOL_SOURCE_NAME (var));
  }

  if (VALUE_LAZY (lazy_value)
      || TYPE_CODE (type) == TYPE_CODE_FUNC)
    {
      value_ptr val;

      addr = VALUE_ADDRESS (lazy_value);
      val = value_from_pointer (lookup_pointer_type (type), addr);
      VALUE_BFD_SECTION (val) = VALUE_BFD_SECTION (lazy_value);
      return val;
    }

  if (!from_tty)
    return NULL;

  /* Not a memory address; check what the problem was.  */
  switch (VALUE_LVAL (lazy_value))
    {

    case lval_register:
    case lval_reg_frame_relative:

      error ("Address requested for identifier \"%s\" which is in a register.",
	     SYMBOL_SOURCE_NAME (var));
      break;

    default:
      error ("Can't take address of \"%s\" which isn't an lvalue.",
	     SYMBOL_SOURCE_NAME (var));
      break;
    }
  return 0;			/* For lint -- never reached */
}


value_ptr
locate_var_value (register struct symbol *var, struct frame_info *frame)
{
  return locate_var_value2 (var, frame, 1);
}


static void build_findvar (void);
static void
build_findvar ()
{
  /* We allocate some extra slop since we do a lot of memcpy's around
     `registers', and failing-soft is better than failing hard.  */
  int sizeof_registers = REGISTER_BYTES + /* SLOP */ 256;
  int sizeof_register_valid = NUM_REGS * sizeof (*register_valid);
  registers = xmalloc (sizeof_registers);
  memset (registers, 0, sizeof_registers);
  register_valid = xmalloc (sizeof_register_valid);
  memset (register_valid, 0, sizeof_register_valid);

//For JAGaf13527
 #ifndef HP_IA64 
  /* Initialize the size of dummy_register here. */
  dummy_register_size = sizeof_registers;
  dummy_register = xmalloc (sizeof_registers);
  memset (dummy_register, 0, sizeof_registers);
 #endif 
}

#ifndef HP_IA64
/* Resize dummy_register and discard all non-used dummy-frame addresses.
   We realloc the array keeping frame_count as a max limit for valid
   dummy_frames. */
void
free_dummy_register_array ()
{
  int sizeof_registers = REGISTER_BYTES + /* SLOP */ 256;
  int frame_count = 0;
  struct frame_info *fi = get_current_frame ();
  for (; fi; fi = get_prev_frame (fi), frame_count++);

  if (dummy_register_size > sizeof_registers && dummyno > frame_count)
    {
      char *temp = xcalloc (sizeof_registers, sizeof (char));
      memcpy (temp, &dummy_register [dummy_register_size - REGISTER_BYTE(frame_count)], 
              REGISTER_BYTE(frame_count));

      free (dummy_register);
      dummy_register = temp;
      dummy_register_size = sizeof_registers;
      dummyno = frame_count + 1;
      dummyread = dummyno;
    }
}
#endif
                       
void _initialize_findvar (void);
void
_initialize_findvar ()
{
  build_findvar ();

  register_gdbarch_swap (&registers, sizeof (registers), NULL);
  register_gdbarch_swap (&register_valid, sizeof (register_valid), NULL);
  register_gdbarch_swap (NULL, 0, build_findvar);
}
