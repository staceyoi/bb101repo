#include "defs.h"
#include "symtab.h"
#include "gdb-stabs.h"       
#include "objfiles.h"
#include "target.h"         
#include "command.h"
#include "gdbcmd.h"
#include "gdbthread.h"
#include "bfd.h"
#include "value.h"
#include "breakpoint.h"
#include "inferior.h"
#include "language.h"
#include "rtc.h"
#include "top.h"
#include "demangle.h"
#include "annotate.h"
#include <strings.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "dm.h" 
#include <dl.h>
#include <stdlib.h>
#if defined (GDB_TARGET_IS_HPPA) || defined (GDB_TARGET_IS_HPPA_20W)
#include <som.h>
#endif 

#ifdef HP_IA64
#include "hp-ia64-solib.h"
#endif

#if defined (RTC)

#include <shl.h>
#ifndef HPUX_1020
#include <sys/ttrace.h>
#endif /* HPUX_1020 */


/* Bindu 101702: On IA64, we will need to do swizzling of certain addresses.
   This should be a nop for other platforms. You don't want to put
   ifdef's all around..... */
#ifdef SWIZZLE
#define SWZ SWIZZLE
#else
#define SWZ
#endif /* !SWIZZLE */

/* Size of the pointer in the inferior. */
#ifdef HP_IA64
#define PTR_SIZE \
          (is_swizzled? 4: 8)
#else
#define PTR_SIZE (sizeof (CORE_ADDR))
#endif /* !HP_IA64 */

typedef struct tmp_dangling_ptrs_struct {
  enum root_set location;
  int ptr;
} tmp_dangling_ptrs_struct;

/* This array definition is tied to enum roo_set in rtc.h.
   Used for determining  dangling pointer locations. 
*/
char *root_set_loc[] = {
	"Process main thread stack",
        "data space",
        "Register",
	"Small block allocator area in heap",
	"Heap",
	"might be Mmap`d region"
};

#define SIZEOF_DANGLING_STRUCT \
         (is_swizzled ? sizeof (tmp_dangling_ptrs_struct) : sizeof (dangling_ptrs_struct)

/* JAGaf49121 */
extern char *cplus_demangled_name;

#define DEMANGLE_NEW(nm) \
    if (new_cxx_abi && is_cplus_name (nm, 1))  \
      { \
         nm = cplus_demangled_name;\
         free_function = 1;\
      } \
      else\
      {\
         if (!strcmp (nm, "__nwa(unsigned int)"))\
           nm = "operator new[](unsigned int)";\
         else if (!strcmp (nm, "__nwa(unsigned long)"))\
           nm = "operator new[](unsigned long)";\
      }\

/* JAGae75847 - Allow heap checking to work with third party malloc 
 * Macros to put instruction pieces into place in an instruction slot
 * The names correspond mostly to the field names used in the
 * instruction set reference manual.
 */

#define P_OP(x) ((uint64_t)(x)<<37)
#define P_R1(x) ((uint64_t)(x)<<6)
#define P_R2(x) ((uint64_t)(x)<<13)
#define P_B1(x) ((uint64_t)(x)<<6)
#define P_B2(x) ((uint64_t)(x)<<13)
#define P_X1(x) ((uint64_t)(x)<<22)
#define P_X2M(x) ((uint64_t)(x)<<31)
#define P_X3(x) ((uint64_t)(x)<<33)
#define P_X4M(x) ((uint64_t)(x)<<27)
#define P_X6(x) ((uint64_t)(x)<<27)
#define P_Y(x) ((uint64_t)(x)<<26)
#define P_IH(x) ((uint64_t)(x)<<23)
#define P_WHI(x) ((uint64_t)(x)<<20)
#define P_WHB(x) ((uint64_t)(x)<<33)
#define P_P(x) ((uint64_t)(x)<<12)
#define P_BTYPE(x) ((uint64_t)(x)<<6)
#define P_IMM1(x) ((uint64_t)((x)&0x1)<<36)
#define P_IMM1C(x) ((uint64_t)((x)&0x1)<<21)
#define P_IMM5C(x) ((uint64_t)((x)&0x1f)<<22)
#define P_IMM7B(x) ((uint64_t)((x)&0x7f)<<13)
#define P_TIMM9C(x) ((uint64_t)((x)&0x1ff)<<24)
#define P_IMM9D(x) ((uint64_t)((x)&0x1ff)<<27)

/* Macros to assemble various 41-bit instructions */

#define I21_MOV_TO_BR(b1,r2,tag) ( \
	P_OP(0) | P_X3(7) | P_TIMM9C((tag)>>4) | P_IH(0) | \
	P_X1(0) | P_WHI(1) | P_R2(r2) | P_B1(b1))
#define M48_NOP(imm) ( \
	P_OP(0) | P_X3(0) | P_X2M(0) | P_X4M(1) | P_Y(0))
#define B4_BR_COND_SPTK_FEW(b2) ( \
	P_OP(0) | P_WHB(0) | P_X6(0x20) | P_B2(b2) | P_P(0) | P_BTYPE(0))
#define X2_MOV_IMM_LONG(r1,imm) ( \
	P_OP(6) | P_IMM1((uint64_t)(imm)>>63) | \
	P_IMM9D((uint64_t)(imm)>>7) | P_IMM5C((uint64_t)(imm)>>16) | \
	P_IMM1C((uint64_t)(imm)>>21) | P_IMM7B(imm) | P_R1(r1))

/* Assembles three instructions into a bundle */
static void bundle_asm(char[], int , uint64_t , uint64_t , uint64_t );

static char *error_text;

/* Flags for various error detection rules. Defined in thread-check.c */
extern int thr_non_recursive_relock;
extern int thr_unlock_not_own;
extern int thr_mixed_sched_policy;
extern int thr_condvar_multiple_mutexes;
extern int thr_condvar_wait_no_mutex;
extern int thr_deleted_obj_used;
extern int thr_thread_exit_own_mutex;
extern int thr_thread_exit_no_join_detach;
extern int thr_stack_utilization; /* % */
extern int thr_num_waiters;
extern int thr_ignore_sys_objects;

extern int addressprint;	/* Print addresses, or stay symbolic only? */

/* error checking for info heap arena.  Introduce this 
 * macro so do_arena does not look bloated.
 */
#define CHECK_ERROR_CONDITION(status, text) \
      if (status == DM_ERR) \
        error (text); \
      if (status == DM_BUSY) \
        error ("Heap info temporarily unavailable");\

#define HEAP_INFO	 1   /* to print memory allocations only */
#define BOUNDS_INFO	 2   /* to print memory overruns */
#define PROCESS_INFO     3   /* for info heap process */
#define ARENA_INFO       4   /* for info heap arena */
#define ARENA_NUM_INFO   5   /* for info heap arena[s] [1|2..] */
#define BLOCK_INFO       6   /* for info heap arena[s] [1|2].. block  */
#define BLOCK_NUM_INFO   7   /* for info heap arena[s] [1|2].. block [1|2].. */
#define BLOCK_STACK_INFO 8   /* info heap arena [1|2..] blocks stack */
#define CORRUPTION_INFO  9   /* for info corruption */
#define HIGH_MEM_INFO   10   /* info heap high-mem */
#define LEAKS_INFO	11   /* to print leaks information/to GC */

#define PAGE_FROM_POINTER(pointer) \
           ((((unsigned long) (pointer)) & 0xfffffffful) >> 12)

int rtc_in_progress = 0;
int dld_can_preload = 0;
int check_heap_in_this_run = 0;
extern int trace_threads_in_this_run;
extern int check_heap;  /* next run */
extern struct objfile* get_rtc_dot_sl (void);
extern int sym_info_rtc (char *, int, FILE*);
extern void sym_module_rtc (char *, int, FILE*);

extern void switch_to_thread (int pid);
extern void foreach_live_thread ( void (*function )(void *), void *);
extern void decode_thread_event (enum rtc_event ecode);
extern void add_symbol_file_command (char *, int);

#ifdef GDB_TARGET_IS_HPUX
extern char * wdb_version_nbr;
#endif

/* Bindu 040303 attach & rtc: to save the information about if we are
   debugging an attached process. */
int att_flag = 0;

/* various tunable knobs for RTC. The mother of all knobs is the
   variable check_heap_in_this_run. If this is off, none of the others
   matter.
*/
int rtc_initialized = 0;
int rtc_frame_count_set = 0;
int frame_count = DEFAULT_FRAME_COUNT;
static int check_leaks = 1;
static int check_bounds = 0;
static int check_string = 0;
static int scramble_blocks = 0;
static int min_leak_size = DEFAULT_MIN_LEAK_SIZE;
static int min_heap_size = 0;  /* JAGaf89828 */
static CORE_ADDR watch_address = 0;
static int high_mem_count = 0;

static void heap_info_command (char *, int );
static void leaks_info_command (char *leaknum_exp, int from_tty);

/* use compiler support to heap check facility */
static int compiler_heap_check = 0; 

/* null-check variables */
#define DEBUG(x) 
#define DEFAULT_NULL_CHECK_RANDOM_RANGE  100
#define DEFAULT_NULL_CHECK_SEED_VALUE    5 
static int null_check_count = -1; 
static int null_check_size = -1;  
static int null_check_random_range = DEFAULT_NULL_CHECK_RANDOM_RANGE;
static int null_check_seed_value = DEFAULT_NULL_CHECK_SEED_VALUE;

/* incremental heap analysis */
#define TIME_STR_LEN 25

struct interval {
char  start_time[TIME_STR_LEN]; /* dyn: get the correct size */
char  end_time[TIME_STR_LEN]; /* dyn: get the correct size */
int   interval;
CORE_ADDR heap_start;
CORE_ADDR heap_end;
int   num_blocks;
int   unq_blocks;
int   total_bytes;
struct memory_info *block_info;
}; 

/* heap_idx is used to hold the heap records for each interval.
   The size of this array is determined by the number of records
   int "/tmp/__heap_interval_info.idx" file.
 */
static struct interval *heap_idx;
static int check_heap_interval_value = 0;
static int check_heap_interval_reset_value = 0;
static int retain_freed_blocks = 0;
static int rtc_header_size = DEFAULT_HEADER_SIZE;
static int rtc_footer_size = DEFAULT_FOOTER_SIZE;
static int check_heap_interval_repeat_value = DEFAULT_REPEAT_CNT;
static FILE * heap_interval_idxfile_fp = NULL;
static FILE * heap_interval_filename_fp = NULL;

/* big_block_size : If this value is non-zero, we will stop
   the inferior whenever an allocation size exceeds this size.
*/
static long big_block_size = 0;  

/* heap_growth_size : If this value is non-zero, we will stop
   the inferior whenever an allocation causes the process
   address space to grow by a size greater than this value.
   The difference between this option and the former is
   subtle but important. 

   p = malloc (huge_size);
   ...
   free (p);
   p = malloc (huge_size);

   will trigger two events in one the former case, but in the 
   latter, may trigger zero, one or two event(s). 
*/

static long heap_growth_size = 0;
static int check_free = 0;
static int is_threaded = 0;
static int is_cma_threaded = 0;
static int is_dce_threaded = 0;

/* Suresh: Jan 08: This is the equivalent of pc_tuple defined in rtc.h
*/
struct gdb_pc_tuple {
    CORE_ADDR pc;
    int library_index_of_pc;
};

/* The structure gdb_chunk_info is a clone of rtc_chunk_info defined in
   rtc.h. Sharing this structure between a 32 bit gdb and a potentially
   64 bit librtc does not work well. Anytime you touch one, make
   sure the other one is updated too.
*/

typedef struct gdb_chunk_info {

    unsigned scanned     : 1;
    unsigned do_not_scan  :1;
    unsigned old_leak    : 1;
    unsigned heap_block  : 1;
    unsigned padded_block  : 1;
    unsigned preinit_mmap  : 1;   /* added for info corruption core */
    unsigned new  : 1;            /* added to be consistent with rtc.h */ 
    unsigned freed_block  : 1;    /* added to retain freed blocks */

    CORE_ADDR next_leak;
    CORE_ADDR next;
    CORE_ADDR base;        
    CORE_ADDR end;         
    CORE_ADDR pc_tuple; /*it should NOT be struct gdb_pc_tuple* here!! */
    dangling_ptrs *dptr;

} gdb_chunk_info;

/* We use a slightly different structure for maintaining leak info
   in gdb, compared to the data structures used by the inferior.
   The structure here is influenced by presentation needs i.e., the
   need to collate multiple leaks from the same allocating stack and
   the need to sort the leaks by size. The memory pressure in
   the inferior is intense since there is one instance of rtc_chunk_info
   per allocated block. In gdb, the memory pressure is expected to be
   very low as we store only the (collated) leaks.
*/

static struct memory_info {

  /* where art thou ? */
  CORE_ADDR address;

  long long size;

  long long min;
  long long max;

  unsigned count;

  struct gdb_pc_tuple *pc_tuple;

  int	corrupted; /* set to 1 if the block is corrupted 
                      at header or footer by librtc */

  /* The field pointer_to_stack_trace_tuple is the pointer to an array of 
     PC values in the inferior. We use it here to identify the leaks from
     the same place. Confusingly it is called pc in rtc_chunk_info.
  */

  CORE_ADDR pointer_to_stack_trace_tuple;

  int high_mem_cnt;  /* JAGaf87040 */

  int leaked_block; /* Leak (1) or a Dangling block (0) */

  dangling_ptrs *dptr;

} * leak_info, *block_info;

/* To keep track of start and end of heap summary */

static CORE_ADDR heap_start;
static CORE_ADDR heap_end;

/* 
 * Begin of heap arena related declaration.  enum and structs 
 * that we share with libc are in dm.h.  When they update theirs
 * we get a new drop to be in sync.  If not you will encounter
 * very difficult bug.   
 */


/* 
 * introduce this to ease the extraction of bit fields 
 * This is a clone of rtc_chunk_info defined in rtc.h and gdb_chunk_info. 
 * It's a clone except for the union part.  Anytime you touch one, make
 * sure the other one is updated too.
 */
typedef struct tmp_chunk_info {
  union bitfields {
    LONGEST flags;              /* which is long long int */
    struct thebits {
      unsigned scanned     : 1;
      unsigned do_not_scan  : 1;
      unsigned old_leak    : 1;
      unsigned heap_block  : 1;
      unsigned padded_block  : 1;
      unsigned preinit_mmap  : 1; 
      unsigned new  : 1;          
      unsigned freed_block  : 1;
    } bits;
  } theUnion;

    CORE_ADDR next_leak;
    CORE_ADDR next;
    CORE_ADDR base;        
    CORE_ADDR end;         
    CORE_ADDR pc_tuple; /*it should NOT be struct gdb_pc_tuple* here!! */
    dangling_ptrs *dptr;

} tmp_chunk_info;


/* equiv to infrtc.c chunks_in_page */
static gdb_chunk_info ** chunks_n_page;

/* equiv to infrtc.c total_pages 1048576=0x100000 */
static long gdb_total_pages;              

/* Gdb's equiv to infrtc.c's shlib_info */
/* Need not have all the fields matching.. */
static struct gdb_shlib_info {
    struct gdb_shlib_info* next_unloaded; /* chain for faster access */
    CORE_ADDR text_start;
    CORE_ADDR text_end;
    int library_index;
    char library_name[MAXPATHLEN+1];
    bool is_it_loaded;
} *gdb_shlib_info = 0;

/* This strucure doesn't have pointers and is used for easy structure
   copy from the librtc side.. This is the real equivalent to shlib_info
   and any changes here should be reflected in librtc and vice versa
*/
struct clone_shlib_info {
    CORE_ADDR next_unloaded; /* chain for faster access */
    CORE_ADDR text_start;
    CORE_ADDR text_end;
    CORE_ADDR data_start;
    CORE_ADDR data_end;
    int library_index;
    char library_name[MAXPATHLEN+1];
    bool is_it_loaded;
};
static int gdb_shlib_info_count = 0;

/* head of unloaded shlib_info chain */
static struct gdb_shlib_info *head_unloaded_gdb_shlib_info = NULL;

/* To supress warnings from add_symbol_file_command() */
bool add_unloaded_library_symbols = false;

/* Hack to handle symbol resolution for unloaded libs */
struct objfile *first_added_unloaded_module_objfile = NULL;

/* This is the address at which we start loading the unloaded 
   object files; Doesn't matter if this address clashes with any 
   address in the process address space, because all the
   unloaded libraries are considered to be part of a
   "disjoint" address space from the real process address space
   and is used for symbol resoultion of only unloaded library 
   symbols...
  
   For both IA 32 and PA 32, this address is very sensitive. We
   need to chose an address which is in the 1st quadrant of the
   address space; if we choose address in 2nd or 3rd quadrant,
   though we will be able to add the symbol file, symbol lookups
   fail!
 */ 
#if defined(HP_IA64)
#define FICT_START_ADDRESS (is_swizzled ? 0x4000000 : 0x4000000000002000)
#elif defined(GDB_TARGET_IS_HPPA_20W)
#define FICT_START_ADDRESS  0x4000000000002000 /* PA 64 */
#else 
#define FICT_START_ADDRESS 0x4000000 /* PA 32 */
#endif

CORE_ADDR load_address_for_next_library;

#define ALL_UNLOADED_LIBRARIES -1
#define ABSOLUTE_ADDRESS_INDEX -1
#define SIZEOF_LIBRTC_PC_TUPLE 8
#define RTC_PC_SOLIB(pc_addr, pc_lib_index) \
      ( pc_lib_index == ABSOLUTE_ADDRESS_INDEX ) ? (PC_SOLIB(pc_addr)) : \
            (gdb_shlib_info[pc_lib_index].library_name)

/* 
 * This is set to the correct value depends on IA/PA or 32/64 bit 
 * All of chunk reading is based on these values.  
 */
static int trueSize;      /* size of each address entity */
static int trueChunkSz;   /* sizeof chunk_info */


/* MAGIC_COOKIE set differently for 64bit just for 
 * convenience in process_c_i.  If not process_c_i has 
 * to extract the header differently.  PA64 is a 32bit
 * program.  LL after the constant is needed.
 */ 
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
#define GDB_MAGIC_COOKIE  0xfeedfacefeedfaceLL
#else 
#define GDB_MAGIC_COOKIE  0xfeedface
#endif

/* request a list of x block_list each time  
 * suppose there are total of 152 blocks and
 * BLK_LIST_SIZE=50 there will be 4 calls to 
 * libc_get_arena_info.  The 4th one will have 
 * just 2 blocks block_list[2].addr will be zero.
 * After the initial testing will discuss with
 * libc and do some performance analysis to 
 * determine what this value should be. 
 */
#define BLK_LIST_SIZE  2000 

static void restore_objfile_head(int, struct objfile*);
static struct objfile* set_objfile_head_to_unloaded_modules_list(int);
static struct minimal_symbol* msymbol_of_rtc_stack_pc_address(struct gdb_pc_tuple*, CORE_ADDR*);
static void add_symbols_of_unloaded_libraries (int);
static void bounds_info_command (char *blocknum_exp, int from_tty);
static void read_all_chunks(char *request_type, int fd);
static void free_all_chunks(void);
static void leaks_dangling_common_command (char *leaknum_exp, int from_tty, boolean leak_detection, boolean internal_gc);

/*
 * This came from infttrace.c.  libc needs to treat everything as 
 * unsigned long long for consistency so for PA32 we need to do
 * some conversion.
 */
static int
read_tgt_mem2 (unsigned long long memaddr, unsigned long long myaddr, int len)
{
#if defined (GDB_TARGET_IS_HPPA) && !defined (GDB_TARGET_IS_HPPA_20W)
  unsigned long memaddr_pa32=(unsigned long) memaddr;
  char *myaddr_pa32=(char *) (unsigned long) myaddr;

  return target_read_memory (memaddr_pa32, myaddr_pa32, len);
#else
  return target_read_memory (memaddr, (char *) (unsigned long) myaddr, len);
#endif 

}

/*
 * as mentioned before libc needs the callback interface to be 
 * consistent.  PA32 CORE_ADDR is unsigned long so change the return
 * value of lookup to return unsigned long long.
 * When we want to use the debuggable version of libc, calling 
 * lookup_address_of_variable is a bad idea since lookup_symbol
 * will return a valid sym.  But this is not the correct address.
 * We want the address from lookup_minimal_symbol. 
 * Even if we're not using a debuggable version of libc it's more
 * efficient to call lookup_minimal_symbol right away rather than
 * go through all the logic of lookup_symbol and have it return
 * null then call minimal_symbol.  
 */ 
static unsigned long long 
lookup_addr_of_variable (char *str)
{
  CORE_ADDR symaddr = 0;
  struct minimal_symbol *msymbol;

  msymbol = lookup_minimal_symbol (str, NULL, NULL);
 
  if (msymbol)
    symaddr = SYMBOL_VALUE_ADDRESS (msymbol);

  if (symaddr)
  {

#ifdef SWIZZLE
    /* libmxndbg requires that this address be swizzled always. */
    symaddr = SWIZZLE (symaddr);
#endif

    symaddr = (unsigned long long) symaddr;
    return symaddr;
  }
  else
  {
    error ("Unable to look up address of variable: %s", str); 
    return NULL;
  }
}

/*
 * This is one of the 3 important routines set up for libc to use.
 * Here's how libc uses this routine 
 *
 * ** If debugging a 32 bit application then the real address will be
 * ** only the first 4 bytes.
 *
 *   if (isilp32())
 *     address = address >> 32;
 *
 * The address it's shifting is an address of a variable of the test
 * program (debuggee) not gdb.  libc relies on gdb to tell it when to
 * shift.  If this is not correct, horrible things will happen.
 */ 

static boolean
is_ilp32 (void)
{

#ifdef HP_IA64
  return is_swizzled;
#elif defined (GDB_TARGET_IS_HPPA_20W)
  return false; 
#else
  /* PA32 */
  return true;
#endif 

}


void *debug_callbacks[] = { (void*) lookup_addr_of_variable,
                            (void*) read_tgt_mem2,
                            (void*) is_ilp32,
                            (void*) 0, /* set breakpoint */
                          };


/* for more descriptive printing */
char *heap_type_string[10]={"USER_SBRK","NODE_BLOCK","USED_BLOCK","FREE_BLOCK","HOLDING_HEADER_BLOCK","HOLDING_BLOCK","HEAP_CACHED_BLOCK"};
char *exec_type_string[6]={"SHARE_MAGIC","EXEC_MAGIC","SHMEM_MAGIC","MPAS"};

/* interface to libc.  use dlsym to call libc routine.  
 * This is useful to check if libc is the correct version.
 */
#ifndef DM_SUCCESS
int (*libc_get_arena_hdl)(int, enum arena_request, void *, int, void **);
#else
int (*libc_get_arena_hdl)(int, enum _dm_arena_request, void *, int, void **);
#endif



/* End of heap arena related declarations */ 


static long long total_dangling_bytes = 0;
static long total_dangling_blocks = 0;
static long long total_leak_bytes = 0;
static long total_leak_blocks = 0;
static long long total_bytes = 0;
static long total_blocks = 0;

static int leak_info_capacity = 0;  /* how big is leak_info[] ? */

CORE_ADDR leaks_info_plabel;
CORE_ADDR heap_info_plabel;
CORE_ADDR high_mem_info_plabel;
CORE_ADDR interval_info_plabel;
CORE_ADDR threads_info_plabel;

struct objfile * rtc_dot_sl;
static struct objfile * libc_dot_sl = 0;
static struct objfile * libdld_dot_sl = 0;
/* To hold LTP(Linkage Table Pointer) for the helper library */
static CORE_ADDR rtc_dot_sl_got_value;

static int leak_count;
static int block_count;

/* To remember if we already patched the a.out's definations. */
static int symfile_objfile_patched = 0;

boolean threaded_program (void);

typedef struct wrappers_t {
    char * name;
    CORE_ADDR function_pointer;
} wrappers_t;


static wrappers_t wrappers_all[] = {
   { "malloc",  0 }, { "calloc",   0 },
   { "valloc",  0 }, { "realloc",  0 },
   { "free",    0 }, { "mmap",     0 },
   { "munmap",  0 }, { "sbrk",     0 }, 
   { "brk",     0 }, { "shmat",    0 },
   { "shmdt",   0 }, { "mprotect", 0 },
   { "shl_unload",  0 }, { "dlclose", 0 },
   { "memcpy",   0 }, { "memccpy",  0 },
   { "memset",   0 }, { "memmove",  0 },
   { "strcpy",   0 }, { "strncpy",  0 },
   { "bcopy",    0 }, { "bzero",    0 },
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
   { "__mmap64", 0 },
   { "U_get_shLib_text_addr",   0 },
   { "U_get_shLib_unw_tbl",     0 },
   { "U_get_shLib_recv_tbl",    0 },
#endif
};


static wrappers_t wrappers_nostring[] = { 
   { "malloc",  0 }, { "calloc",   0 },
   { "valloc",  0 }, { "realloc",  0 },
   { "free",    0 }, { "mmap",     0 },
   { "munmap",  0 }, { "sbrk",     0 }, 
   { "brk",     0 }, { "shmat",    0 },
   { "shmdt",   0 }, { "mprotect", 0 },
   { "shl_unload",  0 }, { "dlclose", 0 },
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
   { "__mmap64", 0 },
   { "U_get_shLib_text_addr",   0 },
   { "U_get_shLib_unw_tbl",     0 },
   { "U_get_shLib_recv_tbl",    0 },
#endif

};

static wrappers_t *wrappers;
static int wrappers_size; 

int pending_dld_relocations = 0;


/* JAGaf56372: sometimes the function name has () and 
 * sometimes not.  When it doesn't then add in ().
 * If we always print () then there will be too many ().
 */
char *parenth;

static int
dld_understands_preload (void)
{
  char * file;
  char * version;
  int minor;
  char buffer[BUFSIZ + 1] = "";
  FILE * fp;

  /* srikanth, we have no clean way of knowing if dld on the user system
     supports LD_PRELOAD. Rather than grep for version strings, just
     say yes. Here are the cases :

     (1) dld actually supports LD_PRELOAD and the user has not linked with
         librtc. This library will be preloaded : fine.
     (2) dld actually supports LD_PRELOAD and the user has already linked
         in librtc : This is ok as dld guarantees that preload would
         happen anyway and the implicitly linked in copy would not
         additionally be loaded.
     (3) dld does not support LD_PRELOAD and the user has not linked in
         librtc. Gdb would export LD_PRELOAD environment variable
         but this would be ignored by dld. Later on gdb would discover that
         librtc is not linked in and complain about it. This is the
         required behavior.
     (4) dld does not support LD_PRELOAD and the user has linked in
         librtc. Gdb would export LD_PRELOAD environment variable
         but this would be ignored by dld. But leak detection can progress.

     The only minor issue with (3) and (4) is the environment pollution
     that happens. This happens only when RTC is turned on. I think this
     is no big deal as we do this anyway if dld version is > 11.19.
   */

  return 1;

}

/* Reset all the globals to prepare for runtime-checking. This function is
   called everytime gdb starts debugging a process.  */
void
prepare_for_rtc (int attached)
{
  struct objfile * objfile;
  int i;
  struct minimal_symbol * m;

  libdld_dot_sl = libc_dot_sl = rtc_dot_sl = 0;
  rtc_dot_sl_got_value = 0;

  for (i=0; i < leak_count; i++)
    free (leak_info[i].pc_tuple);

  for (i=0; i < block_count; i++)
    free (block_info[i].pc_tuple);

  free (leak_info);
  free (block_info);
  is_threaded = 0;
  is_cma_threaded = 0;
  is_dce_threaded = 0;
  block_count = 0;
  leak_count = 0;
  leak_info_capacity = 0;
  leak_info = 0;
  block_info = 0;
  total_dangling_bytes = total_leak_bytes = total_bytes = 0;
  total_leak_blocks = 0;
  total_blocks = 0;
  total_dangling_blocks = 0;
  heap_info_plabel = 0;
  high_mem_info_plabel = 0;
  leaks_info_plabel = 0;
  interval_info_plabel = 0;
  threads_info_plabel = 0;
  att_flag = 0;

  /* Enable RTC after attach */
#if 0
  if (attached && check_heap)
    {
      warning ("GDB cannot check the heap in a process already running.");
      warning ("You will have to restart the program for heap debugging.");
      check_heap_in_this_run = 0;
    }
  else
#endif

#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
/* JAGaf37695 - RTC_INIT should be turned 'on' to collect heap information 
   from startup of the process for PA32 application. 
   $ LD_PRELOAD=/opt/langtools/lib/librtc.sl RTC_INIT=on <executable>
   
   Caution : If RTC_INIT is turned 'on', librtc starts recording 
   heap information by default for PA32 process. So, user should 
   avoid export-ing this environment variable for shell and set 
   only when appropriate. */

  if (attached && check_heap)
    {
      m = lookup_minimal_symbol ("__pthreads_not_ready", 0, rtc_dot_sl);
      if (m)
        {
          int librtc_not_initialized = 1;
          target_read_memory (SYMBOL_VALUE_ADDRESS (m), 
                              (char *)&librtc_not_initialized, 4);
          if (librtc_not_initialized) /* set to 0 by librtc once initialized */
            warning ("Heap information provided may not be accurate for the attached process.\nHint: To get complete heap information from startup of the attached process set environment variable RTC_INIT to `on' in command line.");
        }
    }
#endif

  check_heap_in_this_run = check_heap;

  /* Remove any RTC event breakpoint we inserted in the previous run */
  remove_rtc_event_breakpoints ();
  rtc_initialized = rtc_frame_count_set = 0;


  if (check_heap_in_this_run)
    dld_can_preload = dld_understands_preload ();

  symfile_objfile_patched = 0;
  att_flag = attached; /* is debuggee an attached process. */
}

/* Check if the debugee is a threaded program. */
boolean
threaded_program (void)
{
  struct objfile * objfile;
  int is_threaded = 0;

  ALL_OBJFILES (objfile)
    {
      if (strstr (objfile->name, "/libcma."))
        is_threaded = is_cma_threaded = 1;
      else
      if (strstr (objfile->name, "/libdce."))
        is_threaded = is_dce_threaded = 1;
      else
      if (strstr (objfile->name, "/libpthread."))
        is_threaded = 1;
    }

  return is_threaded;
}

/* Return the objfile associated with libc. */
static struct objfile * 
locate_libc (void)
{
  struct objfile * objfile;
  struct minimal_symbol * m;

  ALL_OBJFILES (objfile)
    {
      if (strstr (objfile->name, "/libc."))
        {
          /* paranoia could destroya ... */
          m = lookup_minimal_symbol_text ("fopen", NULL, objfile);
          if (m)
            return objfile;
        }
    }

  return 0;
}

/* Return the librtc's objfile. */
static struct objfile * 
locate_libdld (void)
{
  struct objfile * objfile;
  struct minimal_symbol * m;

  ALL_OBJFILES (objfile)
    {
      if (strstr(objfile -> name,"/libdl.")||strstr(objfile -> name,"/libdld."))
        {
          /* paranoia could destroya ... */
          m = lookup_minimal_symbol_text ("shl_unload", NULL, objfile);
          if (m)
            return objfile;
        }
    }
    return 0;
}


struct objfile * 
locate_librtc (void)
{
  struct objfile * objfile;
  struct minimal_symbol *m=NULL;
  int i;

  ALL_OBJFILES (objfile)
    {
      /* in future we could make it a full path name */
      if (!strstr (objfile->name, "/librtc"))
        continue;
      
      for (i=0; i < (wrappers_size / sizeof(wrappers[0])); i++)
        {
          m = lookup_minimal_symbol_text
                           (wrappers[i].name, 0, objfile);
          wrappers[i].function_pointer = SYMBOL_VALUE_ADDRESS (m);
        }
 
      if (m)
        rtc_dot_sl_got_value = 
                    SOLIB_GET_GOT_BY_PC (SYMBOL_VALUE_ADDRESS (m));
      return objfile;
    }
    return 0;
}

#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
static int
som_patchup_definitions (struct objfile * objfile)
{
  int i, wrapper_patched=0;
  struct minimal_symbol * m;
  CORE_ADDR address;
  int inst;

  for (i=0; i < (wrappers_size / sizeof(wrappers[0])); i++)
    {
      m = lookup_minimal_symbol_text (wrappers[i].name, 0, objfile);

      /* ignore local definitions, if any */
      if (!m || MSYMBOL_TYPE (m) == mst_file_text)
        continue;
      else
	wrapper_patched=1;

      /* Now for the dirty work ... bulk of the code below is
         courtesy of f&c effort. We need room for seven instructions.
         Since we are going by the assumption that any exported function
         by the name of malloc/free (and so on) is actually malloc and
         free (and so on), it is reasonable to expect that there will
         be atleast seven instructions in it.

         Even where malloc simply forwards the call to a "real_malloc",
         this has to be true. For, we need one instruction each to store
         the return pointer in the caller's frame, set up a stack frame,
         branch to the real routine, idle in the delay slot, retrieve
         the return address, branch to the caller, and finally idle in
         the delay slot.
      */

      address = SYMBOL_VALUE_ADDRESS (m);

      /* LDIL rtc_dot_sl_got_value , r19 */
      inst = 0x22600000;
      inst = deposit_21 (rtc_dot_sl_got_value >> 11, inst);
      target_write_memory (address, (char *)&inst, 4);

      /* LDO rtc_dot_sl_got_value(r19), r19 */
      inst = 0x36730000;
      inst = deposit_14 (rtc_dot_sl_got_value & MASK_11, inst);
      target_write_memory (address + 4, (char *)&inst, 4);

      /* LDIL rtc_wrapper() , r22 */
      inst = 0x22c00000;
      inst = deposit_21 (wrappers[i].function_pointer >> 11, inst);
      target_write_memory (address + 8, (char *)&inst, 4);

      /* LDO rtc_wrapper(r22), r22 */
      inst = 0x36d60000;
      inst = deposit_14 (wrappers[i].function_pointer & MASK_11, inst);
      target_write_memory (address + 12, (char *)&inst, 4);

      /* let us work with the least common denominator (1.1) */
      inst = 0x02c010bf; /*  ldsid 0(r22), r31 */
      target_write_memory (address + 16, (char *)&inst, 4);

      inst = 0x001f1820; /*  mtsp r31,sr0 */
      target_write_memory (address + 20, (char *)&inst, 4);

      inst = 0xe2c00002; /* BE,n (sr0, r22) */
      target_write_memory (address + 24, (char *)&inst, 4);
    }
    return wrapper_patched;
}
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
static int
pa64_patchup_definitions (struct objfile * objfile)
{
  int i, wrapper_patched=0;
  struct minimal_symbol * m;
  CORE_ADDR address;
  unsigned int inst, wrapper_func_uvalue, wrapper_func_lvalue;
  unsigned int gp_uvalue, gp_lvalue, imm21_value, imm11_value; 

  for (i=0; i < (wrappers_size / sizeof(wrappers[0])); i++)
    {
      int temp;
      m = lookup_minimal_symbol_text (wrappers[i].name, 0, objfile);

      /* ignore local definitions, if any */
      if (!m || MSYMBOL_TYPE (m) == mst_file_text)
        continue;
      else
	wrapper_patched=1;

      /* The following instructions are patched up at the allocator definition.
       * These instructions are used to transfer the control to the wrapper 
       * functions in the helper library. This is done by:
       * Loading the register r1 with the address of the wrapper function.
       * Loading r27 with the gp value of the helper library.
       * Branching to the wrapper function.
       */

      address = SYMBOL_VALUE_ADDRESS (m);
      
      wrapper_func_uvalue = wrappers[i].function_pointer >> 32;
      wrapper_func_lvalue = wrappers[i].function_pointer & 0xffffffffLL;
     
      inst = 0x22800000; /* LDIL (upper 21 bits), %r20 */
      imm21_value = wrapper_func_uvalue >> 11;
      inst = deposit_21(imm21_value, inst);
      target_write_memory(address, (char *)&inst, 4);

      inst = 0x36940000; /* LDO (upper 14 bits), %r20 */
      imm11_value = wrapper_func_uvalue & MASK_11;
      inst = deposit_14(imm11_value, inst);
      target_write_memory(address+4, (char *)&inst, 4);

      inst = 0x22a00000; /* LDIL (lower 21 bits), %r21 */
      imm21_value = wrapper_func_lvalue >> 11;
      inst = deposit_21(imm21_value, inst);
      target_write_memory(address+8, (char *)&inst, 4);

      inst = 0x36b50000; /* LDO (lower 14 bits), %r21 */
      imm11_value = wrapper_func_lvalue & MASK_11;
      inst = deposit_14(imm11_value, inst);
      target_write_memory(address+12, (char *)&inst, 4);

      inst = 0xfab4c001; /* MIXW,R %r20, %r21, %r1 */
      target_write_memory(address+16, (char *)&inst, 4);

      gp_uvalue = rtc_dot_sl_got_value >> 32;
      gp_lvalue = rtc_dot_sl_got_value & 0xffffffffLL;
     
      inst = 0x22800000; /* LDIL (upper 21 bits), %r20 */
      imm21_value = gp_uvalue >> 11;
      inst = deposit_21(imm21_value, inst);
      target_write_memory(address+20, (char *)&inst, 4);

      inst = 0x36940000; /* LDO (upper 14 bits), %r20 */
      imm11_value = gp_uvalue & MASK_11;
      inst = deposit_14(imm11_value, inst);
      target_write_memory(address+24, (char *)&inst, 4);

      inst = 0x22a00000; /* LDIL (lower 21 bits), %r21 */
      imm21_value = gp_lvalue >> 11;
      inst = deposit_21(imm21_value, inst);
      target_write_memory(address+28, (char *)&inst, 4);

      inst = 0x36b50000; /* LDO (lower 14 bits), %r21 */
      imm11_value = gp_lvalue & MASK_11;
      inst = deposit_14(imm11_value, inst);
      target_write_memory(address+32, (char *)&inst, 4);

      inst = 0xfab4c01b; /* MIXW,R %r20, %r21, %gp */
      target_write_memory(address+36, (char *)&inst, 4);

      inst = 0xe820d002; /* BVE,n %r1 */
      target_write_memory(address+40, (char *)&inst, 4);
    }

    return wrapper_patched;
}
#endif

#ifdef HP_IA64
static int
ia64_patchup_definitions (struct objfile * objfile)
{
  int i, wrapper_patched=0;
  struct minimal_symbol * m;
  CORE_ADDR address;
  uint64_t s0,s1,s2,tp;
  char istub[16];
  
for (i=0; i < (wrappers_size / sizeof(wrappers[0])); i++)
    {
      int temp;
      m = lookup_minimal_symbol_text (wrappers[i].name, 0, objfile);

      /* ignore local definitions, if any */
      if (!m || MSYMBOL_TYPE (m) == mst_file_text)
        continue;
      else
	wrapper_patched=1;

      /* The following instructions are patched up at the allocator definition.
       * These instructions are used to transfer the control to the wrapper 
       * functions in the helper library. This is done by:
       * Loading the register gr16 with the address of the wrapper function.
       * Loading gr1 with the gp value of the helper library.
       * Branching to the wrapper function.
       */

      address = SYMBOL_VALUE_ADDRESS (m);

      s0 = M48_NOP(0); /* nop.m */
      /* s1 contains the imm41 bits of rtc_wrapper value.
       * s2 is movl r16,imm64 of rtc_wrapper value
       */
      s1 = (wrappers[i].function_pointer) >> 22;
      s2 = X2_MOV_IMM_LONG(16,wrappers[i].function_pointer);
      bundle_asm(istub,5,s0,s1,s2);
      target_write_memory(address, istub, 16);
      s0 = M48_NOP(0); /* nop.m */
      /* s1 contains the imm41 bits of rtc_dot_sl_got_value.
       * s2 is movl r1,imm64 of rtc_dot_sl_got_value
       */
      s1 = (rtc_dot_sl_got_value) >> 22;
      s2 = X2_MOV_IMM_LONG(1,rtc_dot_sl_got_value);
      bundle_asm(istub,5,s0,s1,s2);
      target_write_memory(address+16, istub, 16);
      s0 = M48_NOP(0); /* nop.m */
      /* QXCR1000591248: Application with custom allocator crashes
         when "set heap-check on".
         b6 is used instead of b1 since b6 is scratch register,
         and it is caller-saved register which could be used safely. */
      /* mov b6=r16 */
      s1 = I21_MOV_TO_BR(6,16,0);
      /* br b6 */
      s2 = B4_BR_COND_SPTK_FEW(6);
      bundle_asm(istub,17,s0,s1,s2);
      target_write_memory(address+32, istub, 16);
    }
    return wrapper_patched;
}
#endif

/* Assembles three instructions into a bundle with the given template,
 * as a stream of 16 bytes.  This produces an endian-neutral
 * representation for the bundle.
 */

static void bundle_asm(char istub[], int tem, uint64_t s0, uint64_t s1, 
                       uint64_t s2)
{
        istub[0] = tem | ((s0<<5) & 0xe0);
        istub[1] = ((s0>>3) & 0xff);
        istub[2] = ((s0>>11) & 0xff);
        istub[3] = ((s0>>19) & 0xff);
        istub[4] = ((s0>>27) & 0xff);
        istub[5] = ((s0>>35) & 0xff) | ((s1<<6) & 0xc0);
        istub[6] = ((s1>>2) & 0xff);
        istub[7] = ((s1>>10) & 0xff);
        istub[8] = ((s1>>18) & 0xff);
        istub[9] = ((s1>>26) & 0xff);
        istub[10] = ((s1>>34) & 0x7f) | ((s2<<7) & 0x80);
        istub[11] = ((s2>>1) & 0xff);
        istub[12] = ((s2>>9) & 0xff);
        istub[13] = ((s2>>17) & 0xff);
        istub[14] = ((s2>>25) & 0xff);
        istub[15] = ((s2>>33) & 0xff);
}

/* Set the value of the variable (in librtc) whose type is 
   pointer in the inferior */
static void
set_inferior_pointer_var_value (char * name, CORE_ADDR value)
{
  struct minimal_symbol * m;
  CORE_ADDR anaddr;
  char buf[8];
  int status;

  if (rtc_dot_sl == 0)  /* Can happen for PA64 or IA64 */
    return;

  m = lookup_minimal_symbol (name, NULL, rtc_dot_sl);
  if (!m)
#ifdef GDB_TARGET_IS_HPUX
    error ("Internal error : %s missing in librtc!\nIt might be due to "
           "version mismatch of librtc and gdb(Version %s).\nHint: Use "
           "environment variable LIBRTC_SERVER to pick correct version "
           "of librtc.", name, wdb_version_nbr);
#else 
    error ("Internal error : %s missing in librtc!", name);
#endif

  anaddr = SYMBOL_VALUE_ADDRESS (m);

  if (PTR_SIZE == 8)
    store_address (buf, PTR_SIZE, value);
  else
    {
      /* If the ptr size is 4, just write it as an interger.
         This is important for ilp32 when the value is swizzled. */
      store_unsigned_integer (buf, PTR_SIZE, value);
    }

  status = target_write_memory (anaddr, buf, PTR_SIZE);

  /* JAGaf87040 - we need to have a friendlier message.  One common 
   * reason why we can't update a variable is because the librtc
   * has been unloaded.  After a program finished running, gdb unloads
   * the librtc.  If user supplies a RTC command too late, he/she will 
   * encounter this message.  Sometimes it's not clear what happened.
   * Let's add additional explanation.  
   */ 
  if (status != 0)
    error ("Unable to update %s!.  It might be due to librtc no longer "
           "being loaded.", name);

}


/* Set the value of a variable with the given integer value. */
void
set_inferior_var_value (char * name, int value)
{
  struct minimal_symbol * m;
  CORE_ADDR anaddr;
  char buf[4];
  int status;

  if (rtc_dot_sl == 0)  /* Can happen for PA64 or IA64 */
    return;

  m = lookup_minimal_symbol (name, NULL, rtc_dot_sl);
  if (!m)
#ifdef GDB_TARGET_IS_HPUX
    error ("Internal error : %s missing in librtc!\nIt might be due to "
           "version mismatch of librtc and gdb(Version %s).\nHint: Use "
           "environment variable LIBRTC_SERVER to pick correct version "
           "of librtc.", name, wdb_version_nbr);
#else
    error ("Internal error : %s missing in librtc!", name);
#endif  

  anaddr = SYMBOL_VALUE_ADDRESS (m);
  store_unsigned_integer (buf, 4, value);
  status = target_write_memory (anaddr, buf, 4);
  if (status != 0)
    error ("Internal error : Unable to update %s!", name);
}

/* Get the value of a variable */
/* Pass the size of the variable to be read */
int64_t
get_inferior_var_value (char* name, int size)
{
  struct minimal_symbol* m;
  CORE_ADDR anaddr;
  char buf[8];
  int status;

  if (rtc_dot_sl == 0)  /* Can happen for PA64 or IA64 */
    return -1;

  m = lookup_minimal_symbol (name, NULL, rtc_dot_sl);
  if (!m)
#ifdef GDB_TARGET_IS_HPUX
    error ("Internal error : %s missing in librtc!\nIt might be due to "
           "version mismatch of librtc and gdb(Version %s).\nHint: Use "
           "environment variable LIBRTC_SERVER to pick correct version "
           "of librtc.", name, wdb_version_nbr);
#else
    error ("Internal error : %s missing in librtc!", name);
#endif  

  anaddr = SYMBOL_VALUE_ADDRESS (m);
  status = target_read_memory (anaddr, buf, size);
  if (status != 0)
    error ("Internal error : Unable to read %s!", name);

  return extract_signed_integer(buf, size);
}


/* This function is called at startup and everytime there is a library load.
   It initializes the librtc and makes it ready to start memory checking.
 */
void 
snoop_on_the_heap (void)
{
  struct minimal_symbol *m;
  CORE_ADDR pc;

  if (!target_has_stack)
    return;

  /* 
   * JAGaf74522 - in the original design, all functions RTC wants to
   * intercept are added in the wrappers array.  strcpy, memcpy... were
   * handled the same way.  A customer came along with an application
   * that redefined strcpy, bzero etc..  he encountered the following 
   * warnings...
   *   warning ("Your program seems to override the C library functions.");
   *   warning ("This behavior is not supported yet for this platform !");
   * 
   * we had a discussion that we should separate the string related calls.  
   * check_string flag is used because it's set when user turns on 
   * set heap-check string on (we announced this option in our documentation
   * in 5.4 therefore it's important to be consistent)
   * Implementation notes:  Introduce 2 different wrappers and assign 
   * it to wrappers so existing functions can continue to use wrappers.
   */ 
  if (check_string)
    {
      wrappers = wrappers_all;
      wrappers_size = sizeof(wrappers_all);
    }
  else
    {
      wrappers = wrappers_nostring;
      wrappers_size = sizeof(wrappers_nostring);
    }

  /* Set __rtc_frame_count here for the benefit of thread checking */
  if ((trace_threads_in_this_run || check_heap_in_this_run) &&
      !rtc_frame_count_set && !batch_rtc)
    {
      rtc_dot_sl = get_rtc_dot_sl ();
      /* If we attached, user might have set the frame_count by calling
         the __rtc_init_leaks. We don't want to overwrite that. */
      if (att_flag)
        {
          struct minimal_symbol *m = lookup_minimal_symbol 
              ("__rtc_frame_count", 0, rtc_dot_sl);
          if (m)
            {
              int fc;
              target_read_memory (SYMBOL_VALUE_ADDRESS (m), (char *)&fc, 4);
              frame_count = fc;
            }
        }
      set_inferior_var_value ("__rtc_frame_count", frame_count);
      rtc_frame_count_set = 1;
    }

  if (!check_heap_in_this_run && !trace_threads_in_this_run)
    return;

  if (check_heap_in_this_run && !is_threaded)
    {
      is_threaded = threaded_program ();
#ifdef HPUX_1020
      /* Cannot do heap checking on userspace thread. */
      if (is_threaded)
        {
          check_heap = check_heap_in_this_run = 0;
          warning ("GDB cannot detect leaks in a user space "
                   "threaded program.");
          return;
        }
#else
      if (is_cma_threaded || is_dce_threaded)
        {
          check_heap = check_heap_in_this_run = 0;
          warning ("GDB cannot detect leaks in a user space "
                   "threaded program.");
          return;
        }
#endif
    }

  /* Make sure that librtc and libc.sl are linked in. */


  /* There is a minor difference in the startup model between PA32 and
     PA64 which needs to be dealt with: on PA32, when we receive the
     first notification from dld about shared libraries being mapped,
     all implicitly linked shared libraries have been mapped in. On
     PA64 and IPF, we receive one call back per library. So if a library
     we are looking for is missing, we cannot complain and disable RTC.

     Fix for +check=malloc errors in Integration testing by adding test for
     what the "rtc_dot_sl" points to being NULL, not just "rtc_dot_sl"
     itself.  Now that we clean up the symbol table after a shared library
     unload, the unload event for the RTC library was causing the call of
     	"set_inferior_var_value ("shlib_info_invalid", 1);" 
     setting the variable in librtc.so* telling it to reread the shared
     library list to fail, with messages suggesting a mismatch between gdb
     and librtc.* versions.  The HASH_ALL code below causes
     snoop_on_the_heap() to now return silently if the RTC library's symbol
     table has been unloaded.
  */
  if (get_rtc_dot_sl() == NULL) 
      return;

  if (libc_dot_sl == 0)
    if ((libc_dot_sl = locate_libc ()) == 0)
      return;

  if (libdld_dot_sl == 0)
    if ((libdld_dot_sl = locate_libdld ()) == 0)
      return;

  DEBUG(printf("calling snoop_on_the_heap\n");)

  /* The way we intercept and reroute calls to [de]allocation
     functions is by  preloading the librtc before any other
     libraries get loaded and forcing the dld to bind any calls to
     these symbols to the symbols in librtc.

     However, this would not work if the a.out *defines* any of the
     symbols that we want to intercept.

     This situation is not uncommon. There are numerous programs
     including gdb itself that have there own homegrown versions
     of mallocators. Many products in CLL (compilers) also have
     a private implementation of malloc package that gets bound
     into the a.out.
     
     We will cope with these bad programs as follows : We will
     simply patch the definitions and turn them into a special
     kind of import stub : one that does not consult the PLT, but
     in effect does the same : Jump to the appropriate wrapper
     routine after updating the linkage table pointer with the
     correct value.
  */

  if (check_heap_in_this_run && symfile_objfile && !symfile_objfile_patched)
    {
      if (PATCHUP_DEFINITIONS (symfile_objfile))
        {
          warning ("Your program seems to override the default C "
                   "library memory functions.");
          warning ("To support memory checking capabilities all memory "
                   "functions will be routed to default C library(libc)!");
        }
      symfile_objfile_patched = 1;
    }

  /* The leak detector used to determine the list of currently mapped
     libraries by using shl_get (). This is unsafe in a threaded
     program, as the call could block. Since a library has been loaded
     or unloaded just now, let the RTC module know that its library
     list is not valid. The list will be automatically updated on the
     next call to malloc () or free ().  Garbage collection is not
     possible in the interim. This should not be an issue as most
     programs don't shl_load () anyway and cannot shl_unload unless
     they shl_load (). Even in the case where libraries are dynamically
     loaded, this interval should be negligible.

     The alternative to this scheme is ugly, requiring gdb to update
     the data structures in the inferior and it is not always doable
     without advancing the inferior and that could trigger breakpoints
     and really mess up things.
  */

  set_inferior_var_value ("shlib_info_invalid", 1);

#ifdef HP_IA64
  /*
    If the application is bind immediate and not BOR, set the value
    of __rtc_control_mxn_sa_switch in inferior to 1. This will enable
    controlling of SA switch during heap/leak detection in the app.
    (Refer to QXCR1000873676).

    When thread check is on, do not control SA switch, as it may
    lead to other complications.
  */
  if (trace_threads_in_this_run)
    {
      set_inferior_var_value ("__rtc_control_mxn_sa_switch", 0);
    }
  else
    {
      /*
         This code would make the librtc use siginhibit/sigenable
         during heap/leak detection. The right solution would be to
         have a libpthread API to do this (QXCR1000907337). Till then,
         one can use RTC_NO_MXN_SA_CONTROL to avoid the use of
         siginhibit/sigenable, in case any problem is found
      */
      if (getenv("RTC_NO_MXN_SA_CONTROL") == NULL)
        set_inferior_var_value ("__rtc_control_mxn_sa_switch",
                                hp_ia64_is_bor () ? 0 : 1);
    }
#endif

  if (rtc_initialized) 
    return;

  if (check_heap_in_this_run)
    {
       /* Added for batch rtc support. Following initializations are 
          not needed for batch rtc as they have been already been done 
          thru config file in librtc. Also, when gdb is invoked in batch
          rtc, you are at the very end of the program - libc.so.1 is being 
          unloaded by dynamic loade; Nothing much other than printing the 
          leaks information would happen before the a.out is unloaded. So, 
          there is no need to do that. We still need to get plabel info of
          commands which are executed to get leaks and heap info.
       */
 
       if (!batch_rtc) 
         {
           /* Insert an internal breakpoint in rtc_event () to trap sos
              signals and calls to free with non heap objects. */
           m = lookup_minimal_symbol_text ("__rtc_event", 0, rtc_dot_sl);
           if (m)
             {
                pc = SYMBOL_VALUE_ADDRESS (m);
                /* JAGaf36306
                   We want to have breakpoint at the absolute 0 of the
                   function because, later, we read arg registers of 
		   __rtc_event. If the control is stopped after the 
		   prologue, it is possible that some of the arg 
                   registers have been trashed by the compiler. Since
                   the user is not going to do source debugging of this
                   function, it is ok not to skip the prologue. The side
                   effect is thta the arguments of the top frame will not
                   show correctly but this is true for a lot of places
                   where we stop at the absolute 0 of a function. */
                create_rtc_event_breakpoint (pc, bp_rtc_event);
             }
 
             if (compiler_heap_check)
               {
                  m = lookup_minimal_symbol_text ("_rtc_raise_fault", 0, 0);
                  if (m)
                    {
                       pc = SYMBOL_VALUE_ADDRESS (m);
                       create_rtc_event_breakpoint (pc, bp_rtc_compiler_event);
                    }
                  else 
                    {
                       warning("Cannot find _rtc_raise_fault function. "
                               "Application may not be compiled with "
                               "+check=all option. Set heap-check compiler "
                               "not enabled.");
                    }
               }

           /* Tune the knobs in the inferior per user's request ... */
           set_inferior_var_value ("__rtc_check_bounds", check_bounds);
           set_inferior_var_value ("__rtc_check_free", check_free);
           set_inferior_var_value ("__rtc_check_string", check_string);
           set_inferior_var_value ("__rtc_scramble_blocks", scramble_blocks);

           set_inferior_var_value ("__rtc_min_leak_size", min_leak_size);
           /* JAGaf89828 */
           set_inferior_var_value ("__rtc_min_heap_size", min_heap_size);

           /* null check related inits */
           set_inferior_var_value ("__rtc_null_check_size", null_check_size);
           set_inferior_var_value ("__rtc_null_check_count", null_check_count);
           set_inferior_var_value ("__rtc_null_check_seed_value", 
                                     null_check_seed_value);
           set_inferior_var_value ("__rtc_null_check_random_range", 
                                     null_check_random_range);

           /* interval check related inits */
           set_inferior_var_value ("__rtc_check_heap_interval", 
                                     check_heap_interval_value);
           set_inferior_var_value ("__rtc_check_heap_repeat", 
                                   check_heap_interval_repeat_value);
           set_inferior_var_value ("__rtc_check_heap_reset", 
                                     check_heap_interval_reset_value);
           check_heap_interval_reset_value = 0;

           set_inferior_var_value ("__rtc_big_block_size", 
                                   (int) big_block_size);
           set_inferior_var_value ("__rtc_heap_growth_size", 
                                   (int) heap_growth_size);
           set_inferior_pointer_var_value ("__rtc_watch_address", 
                                           watch_address);

	   set_inferior_var_value ("__rtc_high_mem_val", high_mem_count);
           /* By default we do not enable retaining freed blocks, as this would have
              a huge memory overload on the application
            */
	   set_inferior_var_value ("__rtc_retain_freed_blocks",retain_freed_blocks);

           set_inferior_var_value ("__rtc_header_size", rtc_header_size);
           set_inferior_var_value ("__rtc_footer_size", rtc_footer_size);

           /* Initialize the stack pointer in librtc. We should be stopped 
              at _start (or dld on PA64.) This SP value is not the same as the 
              stack base, but the RTC module will recompute it. The reason we 
              want to do it here is to prevent the LIBCL bug from from rearing 
              its ugly head right at startup. See comments in infrtc.c
            */

           set_inferior_pointer_var_value ("stack_base",
                                  read_register (SP_REGNUM));
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
           set_inferior_var_value ("__pthreads_not_ready", 0);
#endif
        } /* if (!batch_rtc) ... */

      /* Locate plabels to debugger hooks. This is to avoid doing it
         when making the command line call. It is not safe to postpone
         it till then since shl_findsym uses mutexes and a deadlock may
         result. (See find_stub_with_shl_get in hppa-tdep.c)
      */
      m = lookup_minimal_symbol ("leaks_info_plabel", 0, rtc_dot_sl);
      if (!m)
        error ("Internal error : Leak detector missing !\n");
      target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                              (char *) &leaks_info_plabel,
                              sizeof (CORE_ADDR));
                      
      m = lookup_minimal_symbol ("heap_info_plabel", 0, rtc_dot_sl);
      if (!m)
        error ("Internal error : Heap analyzer missing !\n");
      target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                              (char *) &heap_info_plabel,
                              sizeof (CORE_ADDR));
                      
      m = lookup_minimal_symbol ("high_mem_info_plabel", 0, rtc_dot_sl);
      if (!m)
        error ("Internal error : Heap analyzer missing !\n");
      target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                              (char *) &high_mem_info_plabel,
                              sizeof (CORE_ADDR));

      m = lookup_minimal_symbol ("interval_info_plabel", 0, rtc_dot_sl);
      if (!m)
        error ("Internal error : Heap analyzer missing !\n");
      target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                              (char *) &interval_info_plabel,
                              sizeof (CORE_ADDR));
      
    }

  if (trace_threads_in_this_run)
    {
      /* Locate plabels to debugger hooks. This is to avoid doing it
         when making the command line call. It is not safe to postpone
         it till then since shl_findsym uses mutexes and a deadlock may
         result. (See find_stub_with_shl_get in hppa-tdep.c)
      */
      m = lookup_minimal_symbol ("threads_info_plabel", 0, rtc_dot_sl);
      if (!m)
        error ("Internal error : Thread checker missing !\n");
      target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                              (char *) &threads_info_plabel,
                              sizeof (CORE_ADDR));
                      
       /* Insert an internal breakpoint in rtc_thread_event () to trap
          thread-related programming errors. */
       m = lookup_minimal_symbol_text ("__rtc_thread_event", 
                                       0, rtc_dot_sl);
       if (m)
         {
            pc = SYMBOL_VALUE_ADDRESS (m);
            /* JAGaf36306
               We want to have breakpoint at the absolute 0 of the
               function because, later, we read arg registers of 
               __rtc_thread_event. If the control is stopped after the 
               prologue, it is possible that some of the arg 
               registers have been trashed by the compiler. Since
               the user is not going to do source debugging of this
               function, it is ok not to skip the prologue. The side
               effect is that the arguments of the top frame will not
               show correctly but this is true for a lot of places
               where we stop at the absolute 0 of a function. */
            create_rtc_event_breakpoint (pc, bp_rtc_event);
         }

       if (!batch_rtc)
         {
           /* For the batch mode case, set the variables directly
              in librtc. */
           set_inferior_var_value ("__rtc_non_recursive_relock", 
                                   thr_non_recursive_relock);
           set_inferior_var_value ("__rtc_unlock_not_own", 
                                   thr_unlock_not_own);
           set_inferior_var_value ("__rtc_mixed_sched_policy", 
                                   thr_mixed_sched_policy);
           set_inferior_var_value ("__rtc_condvar_multiple_mutexes", 
                                   thr_condvar_multiple_mutexes);
           set_inferior_var_value ("__rtc_condvar_wait_no_mutex", 
                                   thr_condvar_wait_no_mutex);
           set_inferior_var_value ("__rtc_deleted_obj_used", 
                                   thr_deleted_obj_used);
           set_inferior_var_value ("__rtc_thread_exit_own_mutex", 
                                   thr_thread_exit_own_mutex);
           set_inferior_var_value ("__rtc_thread_exit_no_join_detach", 
                                   thr_thread_exit_no_join_detach);
           set_inferior_var_value ("__rtc_stack_utilization", 
                                   thr_stack_utilization);
           set_inferior_var_value ("__rtc_num_waiters", 
                                   thr_num_waiters);
           set_inferior_var_value ("__rtc_ignore_sys_objects", 
                                   thr_ignore_sys_objects);
         } // !batch_rtc
    }

  rtc_initialized = 1;
}

void
decode_nomem_event (void)
{
  enum rtc_event ecode = 0;;
  CORE_ADDR pointer = 0;;
  char address[128];

  ecode = (enum rtc_event) (CORE_ADDR) read_register (ARG0_REGNUM);
  pointer = (CORE_ADDR) read_register (ARG1_REGNUM);
  strcpy (address,
              longest_local_hex_string_custom ((LONGEST) pointer,"08l"));

  switch (ecode)
    {
    case RTC_NOMEM : 
      warning ("Malloc is returning simulated %s value", address);
      return;
    /* JAGaf48255 - gdb should report if malloc returns null pointer */
    case RTC_MEM_NULL :  
      warning ("Memory Allocation is returning %s value", address);
      return;
    default : 
      warning ("Unknown RTC null-check event at %s", address);
      return;
    }
}


void
print_inferior_stack_frame (CORE_ADDR pc_tuple_list)
{
  int i, status;
  int free_function = 0;
  CORE_ADDR addr;
  struct gdb_pc_tuple temp_pc_tuple;
  char *function = "???";
  struct minimal_symbol *m;
  struct symtab_and_line sal;

  /* get the frames for this pc_tuple_list */
  for (i = 0; pc_tuple_list && i < frame_count; i++)
    {
     /* Suresh: Jan 08 - Is the size arithmetic OK for pc_tuple_list ? */
     /* IPF 32 bit target case */
     if ( PTR_SIZE != sizeof (CORE_ADDR))
       {

         /* Read the PC first */
	 if ( target_read_memory ((CORE_ADDR) pc_tuple_list +
                                  (i * SIZEOF_LIBRTC_PC_TUPLE),
                                    (char *) &temp_pc_tuple.pc,
                                        sizeof(PTR_SIZE )))
                error ("Error downloading pc_tuple data !");
         /* Shift as pointer size is not equal to the sizeof CORE_ADDR */
	 temp_pc_tuple.pc = temp_pc_tuple.pc >>
				  ((sizeof (CORE_ADDR) - PTR_SIZE) * 8);
         /* Now read the index */
         if ( target_read_memory ( (CORE_ADDR) pc_tuple_list + (i * SIZEOF_LIBRTC_PC_TUPLE) + PTR_SIZE, (char *) &temp_pc_tuple.library_index_of_pc, sizeof(int)))
                error ("Error downloading pc_tuple data !");

       }
       /* On all other targets sizeof(gdb_pc_tuple) and librtc's pc_tuple
          match, so you can increment the debuggee's pc_tuple pointer
          with sizeof(gdb_pc_tuple) */
     else
       {

          if ( target_read_memory (pc_tuple_list + (sizeof(struct gdb_pc_tuple)* i), (char*) &temp_pc_tuple, sizeof(struct gdb_pc_tuple)))
               error ("Error downloading pc_tuple date !");
       }
      /* Suresh: Jan 08: Since this routine is used only for reporting 
         corruption events which happens in the context when the app 
         is trying do the incorrect operation, the stack context should 
         contain *only* pc values that are from currently loaded 
         modules, so we don't need any 'offset address' correction here...
      */
      if ( temp_pc_tuple.library_index_of_pc != ABSOLUTE_ADDRESS_INDEX)
        error ("Unloaded library PCs in live stack context \n");
      addr = temp_pc_tuple.pc;

      /* if PC is zero, then we have reached the end of call stack
         and so we get out of the for loop
       */
      if (addr == 0)
          break;

      if (m = lookup_minimal_symbol_by_pc ((CORE_ADDR) SWZ (addr)))
        {
          function = SYMBOL_SOURCE_NAME (m);
          DEMANGLE_NEW(function);
          sal = find_pc_line (addr, 0);
          
          /* JAGaf56372: check if () is missing add them in 
           * don't automatically put them in the printf */
          parenth = (strchr(function,'(') == NULL &&
                     strchr(function,')') == NULL ) ? "()" : "  ";

          printf_filtered ("#%-2d %s%s", i+1, function, parenth);

          if (sal.symtab && sal.symtab->filename && sal.symtab->filename[0])
            printf_filtered (" at %s:%d\n", sal.symtab->filename, sal.line);
          else 
            printf_filtered (" from %s\n", PC_SOLIB(addr));
          
          if (free_function)
            {
              free (function);
              free_function = 0;
            }
        }
      else 
        {
          if (i == 0)
            printf_filtered("#%-2d %s()", i+1, "<unknown_procedure>");
        }
      addr = 0;
    }
}


/*
 * JAGaf74522: print context for header/footer/memcpy/strcpy/...
 * corruption.
 * decode_rtc_event writes a pointer to a string address before
 * calling this print routine.  To print blkp use %s. 
 */
void
print_context_common(char *blkp, CORE_ADDR pc_tuple_list, size_t sz, 
                     int type, int ecode)
{
   int num_ignore_frames = 0;
   char local_buf[4096]; 

   /* type is important for BAD_HEADER/FOOTER but not for other mem calls */
   switch (ecode)
     {
       case RTC_FREED_BLOCK_CORRUPTED :
       	 sprintf(local_buf,
          "Memory block (size = %ld address = %s) appears to be corrupted "
	  "after the block has been freed\nAllocation context might not be correct.\n\t",
	  (long) sz, blkp);
         warning(local_buf); 
         break;

       case RTC_BAD_HEADER :
       case RTC_BAD_FOOTER :
         /* JAGag41467: correct the warning message about allocation context. */
       	 sprintf(local_buf,
          "Memory block (size = %ld address = %s) appears to be corrupted "
	  "at the %s.\nAllocation context might not be correct.\n\t",
	  (long) sz, blkp, type ? "beginning" : "end");
         warning(local_buf); 
         break;

       case RTC_BAD_MEMCPY:
         sprintf(local_buf, 
            "memcpy corrupted (address = %s size = %ld)", blkp,  (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_MEMCCPY:
         sprintf(local_buf, 
            "memccpy corrupted (address = %s size = %ld)", blkp,  (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_MEMSET:
         sprintf(local_buf, 
            "memset corrupted (address = %s size = %ld)", blkp,  (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_MEMMOVE:
         sprintf(local_buf, 
            "memmove corrupted (address = %s size = %ld)", blkp, (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_BZERO:
         sprintf(local_buf, 
            "bzero corrupted (address = %s size = %ld)", blkp, (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_BCOPY:
         sprintf(local_buf, 
            "bcopy corrupted (address = %s size = %ld)", blkp, (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_STRCPY:
         sprintf(local_buf, 
            "strcpy corrupted (address = %s size = %ld)", blkp, (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_STRNCPY:
         sprintf(local_buf, 
            "strncpy corrupted (address = %s size = %ld)", blkp,  (long) sz);
         warning(local_buf); 
         break;

       case RTC_BAD_STRCAT:
         sprintf(local_buf,
            "strcat corrupted (address = %s size = %ld)", blkp, (long) sz);
         warning(local_buf);
         break;

       case RTC_BAD_STRNCAT:
         sprintf(local_buf,
            "strncat corrupted (address = %s size = %ld)", blkp,  (long) sz);
         warning(local_buf);
         break;

       case RTC_HIGH_MEM:  /* JAGaf87040 */
         sprintf(local_buf, 
                 "High water mark (address = %s total memory per call "
                 "site = %ld)\n", blkp, (long) sz);
         warning(local_buf); 
         break;


     } /* switch */

   if (!pc_tuple_list) return; 

   print_inferior_stack_frame (pc_tuple_list);

   /* As frame count starts from zero, this should be pointing to frame 4. */
   num_ignore_frames = 4;

   /* The underlying comment for GUI must not be altered without letting 
      the wdb-gui team know about the changes. This is used by the gui 
      to select the frame with frame number 'num_ignore_frames' (frame 
      count starts at zero) and display the same on the source window, 
      instead of displaying librtc sources. Fix for JAGag10057. */
   if (annotation_level == 2)
     sprintf(local_buf, "Use command backtrace (bt) to see the current "
             "context.\n\nIgnore top %d frames belonging to leak detection "
             "library of gdb.\nThe GUI is switching to frame %d ...\n", 
             num_ignore_frames, num_ignore_frames);
   else
     sprintf(local_buf, "Use command backtrace (bt) to see the current "
             "context.\n\nIgnore top %d frames belonging to leak detection "
             "library of gdb.\n",
             num_ignore_frames);
   
   warning(local_buf); 
   
   return;
}

void
decode_rtc_compiler_event (void)
{
 /* I am not trying read args and provide a detailed informatiob
    like decode_rtc_event because this part is already being done
    by _rtc_raise_fault. The defines used in this function are 
    conttolled by compiler code; I do not want to create a new
    dependency on compiler header file. 
 */
  warning("Corruption found by compiler generated code (+check option). "
          "Step through _rtc_raise_fault to find details.");
}

void
decode_memory_event (enum rtc_event ecode)
{
  size_t size;
  CORE_ADDR pointer = 0, pcl;
  char address[128];
  char *arg = NULL;
  /* At this point, the inferior is stopped in __rtc_event (). The first
     parameter is the event code and second one is a pointer, which
     may not make sense for all event codes. While reporting the event
     use warnings rather than printf_* since warning automatically
     annotates the message suitably. */
  pointer = (CORE_ADDR) read_register (ARG1_REGNUM);
  pcl = (CORE_ADDR) read_register (ARG2_REGNUM);
  size = (size_t) (CORE_ADDR) read_register (ARG3_REGNUM);

  strcpy (address,
          longest_local_hex_string_custom ((LONGEST) pointer, "08l"));

  switch (ecode)
    {
    case RTC_BAD_FREE : 
      warning ("Attempt to free unallocated or already freed object at %s", 
               address);
      return;

    case RTC_HEAP_GROWTH :
      warning ("Attempt to grow the heap at %s", address);
      return;

    case RTC_HUGE_BLOCK :
      warning ("Attempt to allocate a large object at %s", address);
      return;

    case RTC_BAD_REALLOC :
      warning ("Attempt to realloc an unallocated object at %s",
                                                 address);
      return;

    case RTC_ALLOCATED_WATCHED_BLOCK :
      warning ("Watched address %s allocated", address);
      return;

    case RTC_DEALLOCATED_WATCHED_BLOCK :
      warning ("Watch address %s deallocated", address);
      return;

    case RTC_FREED_BLOCK_CORRUPTED :
      print_context_common(address, pcl, size, 2, ecode);
      return;

    case RTC_BAD_HEADER :
      print_context_common(address, pcl, size, 1, ecode);
      return;

    case RTC_BAD_FOOTER :
      print_context_common(address, pcl, size, 0, ecode);
      return;


    /* As of now all other event codes indicate some kind of internal
       failure. We cannot continue and the RTC module will disable 
       itself. The king is dead, long live the king !
    */

    case RTC_NO_MEMORY : 
      warning ("The child process ran out of memory.");
      return;

    case RTC_SBRK_NEGATIVE :
      warning ("Attempt to shrink the heap ! Heap debugging disabled.");
      return;

    /* If for some reason, we had trouble determining the stack base or
       the C library entry points, let us not wash the dirty linen in
       public, just quit with a generic "internal error" message.
    */

    case RTC_MALLOC_MISSING :
    case RTC_STACK_MISSING :
      warning ("Internal error in GDB. Please notify HP.");
      warning ("Heap debugging disabled. Process otherwise debuggable.");
      return;

    case RTC_BAD_MEMCPY:
    case RTC_BAD_MEMCCPY:
    case RTC_BAD_MEMSET:
    case RTC_BAD_MEMMOVE:
    case RTC_BAD_BZERO:
    case RTC_BAD_BCOPY:
    case RTC_BAD_STRCPY:
    case RTC_BAD_STRNCPY:
    case RTC_BAD_STRCAT:
    case RTC_BAD_STRNCAT:
    case RTC_HIGH_MEM: /* JAGaf87040 */
      print_context_common(address, pcl, size, 0, ecode);
      return;  
    case RTC_INTERNAL_GC:
      /* Internal GC case, we call the function with NULL and 0 as the 1 and 2 argument*/
      leaks_dangling_common_command(NULL, 0, FALSE, TRUE);
      return;
    case RTC_MUTEX_LOCK_FAILED:
      /* We have reached a safe position to call "info heap" and 
         "Info corruption" commands again.
      */
      if (get_inferior_var_value ("failed_to_acquire_mutex", 4))
        {
          if (get_inferior_var_value("__rtc_bounds_or_heap", 4) == HEAP_INFO)
            heap_info_command (arg, 1);
          else if (get_inferior_var_value("__rtc_bounds_or_heap", 4) == BOUNDS_INFO)
            bounds_info_command (arg, 1);
	  else if (get_inferior_var_value("__rtc_bounds_or_heap", 4) == LEAKS_INFO)
            leaks_info_command (arg, 1);
        }
      return;
    }
}

void
decode_rtc_event (void)
{
  size_t size;
  enum rtc_event ecode;

  /* At this point, the inferior is stopped in __rtc_event() or in 
     __rtc_thread_event(). The first parameter is the event code */
  ecode = (enum rtc_event) (CORE_ADDR) read_register (ARG0_REGNUM);
  DEBUG(printf ("ecode = %x reg =%x\n", ecode, ARG0_REGNUM););

  if (ecode > RTC_THREAD_EVENT_START && ecode < RTC_THREAD_EVENT_END)
    decode_thread_event (ecode);
  else 
    decode_memory_event (ecode);
}

 
/* Print the information about a leak (leak_no). Write it either to
   stdout or to fp if provided. */ 
static void
report_this_leak (int leak_no, FILE * fp, boolean leak_detection)
{
    int i;
    int free_function = 0;
    struct memory_info * leak;
    char local_buf[4096];
    int counter = 0;

    if (leak_no < 0 || leak_no >= leak_count)
      leak_detection ? error ("Not a valid leak number.") :  error (" Not a valid dangling block number.");

    /* Sep 07, Suresh: Now the given number cannot be used to directly index into
       the array, as both dangling and leak records co-exist in the same array. 
       We need to iterate through the array and find "leak_no"th record of given type.
     */
    counter = leak_no;
    for(i=0; i < leak_count && counter >= 0; i++)
       if (leak_info[i].leaked_block == leak_detection ) counter--;
    if ( i != 0 && counter == -1)
       leak = leak_info + (i -1);
    else 
      {
      /* Wrong index, there are not that many records of given type in the array */
      leak_detection ? error ("Not a valid leak number.") :  error (" Not a valid dangling block number.");
      return; /* NOT reached */
      }
    if (leak->count == 1)
      {
        /* Only 1 block. Print the address then. */
        /* srikanth, 000221, for some reason, printf_filtered ()
           crashed when we try to print long long values. This
           does not always happen, only sometimes. Just be safe
           and use sprintf () and puts_filtered ().
        */
        leak_detection ? sprintf (local_buf, "%lld bytes leaked at %s "
                       "(%2.2f%% of all bytes leaked)\n",
                               leak->size,
           longest_local_hex_string_custom ((LONGEST) leak->address, "08l"),
                          ((((float) leak->size) / total_leak_bytes) * 100)) :
                         sprintf (local_buf, "%lld bytes dangling at %s "
                       "(%2.2f%% of all dangling blocks)\n",
                               leak->size,
           longest_local_hex_string_custom ((LONGEST) leak->address, "08l"),
                          ((((float) leak->size) / total_dangling_bytes) * 100)) ;

                           
        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);
      }
    else 
      {
        leak_detection ? sprintf (local_buf, "%lld bytes leaked in %d blocks "
                      "(%2.2f%% of all bytes leaked)\n",
                                  leak->size, leak->count,
                          ((((float) leak->size) / total_leak_bytes) * 100)):
                         sprintf (local_buf, "%lld bytes dangling in %d blocks "
                      "(%2.2f%% of all dangling blocks)\n",
                                  leak->size, leak->count,
                          ((((float) leak->size) / total_dangling_bytes) * 100));
        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);

        sprintf (local_buf, "These range in size from "
                            "%lld to %lld bytes %s\n",
                                  leak->min, leak->max,
                                  leak->pc_tuple ? "and are allocated" : "");
        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);
      }

    if (!leak->pc_tuple)
      return;

    /* Print the frame stack. */
    for (i=0; i < frame_count; i++)
      {
        struct symtab_and_line sal;
        struct minimal_symbol *m;
        char * name = "???";
        CORE_ADDR pc_addr = leak->pc_tuple[i].pc;
        int pc_lib_index;
        struct objfile* temp_head;

        /* For some reason when the stack is completely unwound
           we end up with a pc value of 0xfffffffc. This is not
           the documented behavior of libcl. Just deal with it ...
        */
        /* Suresh: Would work fine for live addresses.. What about for 
           offset addresses ?? and we are checking the unswizzled address
           here, whereas previosly it was the swizzled one..
        */
        if (pc_addr == 0 || pc_addr == (CORE_ADDR) 0xfffffffc)
          break;

        pc_lib_index = leak->pc_tuple[i].library_index_of_pc;
        m = msymbol_of_rtc_stack_pc_address( &leak->pc_tuple[i],
                                                              &pc_addr);
        temp_head = set_objfile_head_to_unloaded_modules_list(pc_lib_index);

        if (m)
          name = SYMBOL_SOURCE_NAME (m);

	/* JAGae74363 Diwakar Nag */
        DEMANGLE_NEW(name)

        sal = find_pc_line (pc_addr, 0);

        parenth = ( strchr(name,'(') == NULL &&
                    strchr(name,')') == NULL ) ? "()" : "  ";

	/* JAGae78845 Diwakar Nag */
        /* QXCR1000931438: WDB doesn't have an option to print the hex address
           for leaks. Currently annotation level = 2 (GUI) is kept out of scope
           as WDB-GUI parsing purely depends on the WDB o/p and this change 
           would require multiple changes in the WDB-GUI code.*/
        if (!addressprint || annotation_level > 1)
          {
            if (fp)
              fprintf (fp, "#%-2d %s%s", i, name, parenth);
            else 
              printf_filtered("#%-2d %s%s", i, name, parenth);
          }
        else
          {
            if (fp)
              fprintf (fp, "#%-2d %s in %s%s", i,
                  longest_local_hex_string_custom ((LONGEST)SWZ (pc_addr),
                      "08l"),
                  name, parenth);
            else 
              printf_filtered("#%-2d %s in %s%s", i,
                  longest_local_hex_string_custom ((LONGEST)SWZ (pc_addr),
                      "08l"),
                  name, parenth);
          }

        if (free_function) 
          {
            free(name);
	    free_function = 0;
          }

        if (sal.symtab && sal.symtab->filename && sal.symtab->filename[0])
          {
            if (fp)
              fprintf (fp, " at %s:%d\n", sal.symtab->filename, sal.line);
            else 
              printf_filtered (" at %s:%d\n", sal.symtab->filename, sal.line);
          }
        else 
          {
            if (fp)
              fprintf (fp, " from %s\n",RTC_PC_SOLIB(pc_addr, pc_lib_index));
            else 
              printf_filtered (" from %s\n",RTC_PC_SOLIB(pc_addr, pc_lib_index));
          }
        restore_objfile_head(pc_lib_index, temp_head);
      }
    if (!leak_detection)
    {
      int k = 0;
      if (fp)
        fprintf (fp, "\nDangling pointers to this block are:\n");
      else
        printf_filtered ("\nDangling pointers to this block are:\n");
      for (; k < leak->dptr->count; k++)
        { 
          
          if (fp)
            fprintf (fp, "\nPointer %d: Located in %s.\n", k + 1,
                           root_set_loc [leak->dptr->ptrs[k].location]);   
          else
            printf_filtered ("\nPointer %d: Located in %s.\n", k + 1,
                           root_set_loc [leak->dptr->ptrs[k].location]);   
          if (leak->dptr->ptrs[k].location != REGISTER_SET)
            {
              if (sym_info_rtc(leak->dptr->ptrs[k].ptr, 2, fp))
              {
               
               /* TODO: Later - Suresh:Oct 07: Catching the culprit, when he does "write 
                  access" using this dangling pointer, by overwriting with the address
                  of a special page (setup by RTC) that has no RW permission, if we are
                  sure if its a "actual" pointer and not just a potential pointer. We
                  can find this out by looking up the type information to see if its
                  of pointer type(?) in certain cases..

                  Also we need to book-keep these locations+old value, when we 
                  overwrite with a page address that has no RW access, so that when 
                  a SEGV happens inside the debugger session, we can see if its 
                  because of GDB inducing the fault to catch the "culprit" or a 
                  genuine fault.

                  Open issues: How do we handle cases, when the user detaches from the
                  debugger after we have "patched" these dangling pointers. Could result
                  in a core dump that could be misleading to the user. May be we need
                  to restore all these locations back to "the original value" before
                  detach
                 */

                sym_module_rtc (leak->dptr->ptrs[k].ptr, 2, fp);
              }
           }   
       }
    }
}

void
print_rtc_stack_frame (CORE_ADDR* pcs, int count, FILE *fp)
{
   int i;
   int free_function = 0;
   char local_buf[4096];

   for (i=0; i < count; i++)
     {
        struct symtab_and_line sal;
        struct minimal_symbol *m;
        char * name = "???";
        CORE_ADDR pc_addr = (CORE_ADDR) SWZ (pcs[i]);

        /* For some reason when the stack is completely unwound
           we end up with a pc value of 0xfffffffc. This is not
           the documented behavior of libcl. Just deal with it ...
        */
        if (pcs[i] == 0 ||
            pcs[i] == (CORE_ADDR) 0xfffffffc)
          break;

        m = lookup_minimal_symbol_by_pc (pc_addr);
        if (m)
          name = SYMBOL_SOURCE_NAME (m);

        /* JAGae74363 Diwakar Nag */
        DEMANGLE_NEW(name);

        /* JAGae78845 Diwakar Nag */
        sal = find_pc_line (pc_addr, 0);

        parenth = ( strchr(name,'(') == NULL &&
                    strchr(name,')') == NULL ) ? "()" : "  ";


        if (fp)
          fprintf (fp, "#%-2d %s%s", i, name, parenth);
        else
          printf_filtered("#%-2d %s%s", i, name, parenth);

        if (free_function)
          {
             free(name);
             free_function = 0;
          }

       if (sal.symtab && sal.symtab->filename && sal.symtab->filename[0])
         {
            if (fp)
              fprintf (fp, " at %s:%d\n", sal.symtab->filename, sal.line);
            else
              printf_filtered (" at %s:%d\n", sal.symtab->filename, sal.line)
;
         }
       else
         {
            if (fp)
              fprintf (fp, " from %s\n",PC_SOLIB(pc_addr));
            else
              printf_filtered (" from %s\n",PC_SOLIB(pc_addr));
         }
      }
   return;
}

/* Suresh: Jan 08: This function is used by thread check code as well 
   to print the stack trace. But they still collect stack traces as
   a PC array and they are unaware of the 2-tuple stack trace we have
   now gone for... 

   To keep them happy :)-, we will have 2 versions of this function - 
   one the old one, and the other this new variant that is for 
   memory RTC, that can handle 2-tuple stack traces..

*/
void
print_rtc_stack_frame_variant1 (struct gdb_pc_tuple* pc_tuple, int count, FILE *fp)
{
   int i;
   int free_function = 0;
   char local_buf[4096];

   for (i=0; i < count; i++)
     {
        struct symtab_and_line sal;
        struct minimal_symbol *m;
        char * name = "???";
        CORE_ADDR pc_addr = pc_tuple[i].pc;
        int pc_lib_index = pc_tuple[i].library_index_of_pc;
        struct objfile* temp_head;

        /* For some reason when the stack is completely unwound
           we end up with a pc value of 0xfffffffc. This is not
           the documented behavior of libcl. Just deal with it ...
        */
        /* Suresh: Would work fine for live addresses.. What about for 
          offset addresses ??
        */
        if (pc_addr == 0 || pc_addr == (CORE_ADDR) 0xfffffffc)
          break;

        m = msymbol_of_rtc_stack_pc_address( &pc_tuple[i],&pc_addr);
        temp_head = set_objfile_head_to_unloaded_modules_list(pc_lib_index);

        if (m)
          name = SYMBOL_SOURCE_NAME (m);

	/* JAGae74363 Diwakar Nag */
        DEMANGLE_NEW(name);

        /* JAGae78845 Diwakar Nag */
        sal = find_pc_line (pc_addr, 0);

        parenth = ( strchr(name,'(') == NULL &&
                    strchr(name,')') == NULL ) ? "()" : "  ";

        if (!addressprint || annotation_level > 1)
          {
            if (fp)
              fprintf (fp, "#%-2d %s%s", i, name, parenth);
            else
              printf_filtered("#%-2d %s%s", i, name, parenth);
          }
        else
          {
            if (fp)
              fprintf (fp, "#%-2d %s in %s%s", i,
                  longest_local_hex_string_custom ((LONGEST)SWZ (pc_addr),
                      "08l"),
                  name, parenth);
            else 
              printf_filtered("#%-2d %s in %s%s", i,
                  longest_local_hex_string_custom ((LONGEST)SWZ (pc_addr),
                      "08l"),
                  name, parenth);
          }

        if (free_function) 
          {
             free(name);
	     free_function = 0;
          }

       if (sal.symtab && sal.symtab->filename && sal.symtab->filename[0])
	 {
            if (fp)
              fprintf (fp, " at %s:%d\n", sal.symtab->filename, sal.line);
            else 
              printf_filtered (" at %s:%d\n", sal.symtab->filename, sal.line);
	 }
       else 
         {
            if (fp)
              fprintf (fp, " from %s\n",RTC_PC_SOLIB(pc_addr, pc_lib_index));
            else 
              printf_filtered (" from %s\n",RTC_PC_SOLIB(pc_addr, pc_lib_index));
         }
       restore_objfile_head(pc_lib_index, temp_head);
     }
   return;
}


static void
report_this_interval_block (struct memory_info *block, FILE * fp, int tot_bytes)
{
    char local_buf[200];
    int free_function = 0;

    if (block->count == 1)
      {
        sprintf (local_buf, "%lld bytes at %s "
                    "(%2.2f%% of all bytes allocated)\n",
                  block->size, longest_local_hex_string_custom (
                        (LONGEST) block->address, "08l"),
               ((((float) block->size) / tot_bytes) * 100));
        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);
      }
    else 
      {
        sprintf (local_buf, "%lld bytes in %d blocks "
                      "(%2.2f%% of all bytes allocated)\n",
                             block->size, block->count,
                       ((((float) block->size) / tot_bytes) * 100));
        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);

        sprintf (local_buf, "These range in size from "
                            "%lld to %lld bytes %s\n",
                             block->min, block->max,
                             block->pc_tuple ? "and are allocated" : "");

        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);
      }

    if (!block->pc_tuple)
      return;

    print_rtc_stack_frame_variant1(block->pc_tuple, frame_count, fp);
    return;
}

/* Print out the information about a heap block (block_no). Write it
   either to stdout or to fp if provided. 
*/

static void
report_this_block (int block_no, FILE * fp, int type)
{
    int i;
    char local_buf[200];
    int free_function = 0;
    struct memory_info * block;

    if (block_no < 0 || block_no >= block_count)
      error ("Not a valid block number.");
  
    block = block_info + block_no;

    if (type == BOUNDS_INFO && !block->corrupted)
      error ("Block number not corrupted.");

    if (block->count == 1)
      {
        sprintf (local_buf, "%lld bytes at %s "
                    "(%2.2f%% of all bytes allocated)\n",
                  block->size, longest_local_hex_string_custom (
                        (LONGEST) block->address, "08l"),
               ((((float) block->size) / total_bytes) * 100));
        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);
      }
    else 
      {
        sprintf (local_buf, "%lld bytes in %d blocks "
                      "(%2.2f%% of all bytes allocated)\n",
                             block->size, block->count,
                       ((((float) block->size) / total_bytes) * 100));

        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);

        sprintf (local_buf, "These range in size from "
                            "%lld to %lld bytes %s\n",
                             block->min, block->max,
                             block->pc_tuple ? "and are allocated" : "");

        if (fp)
          fputs (local_buf, fp);
        else 
          puts_filtered (local_buf);
      }

    if (!block->pc_tuple)
      return;

    print_rtc_stack_frame_variant1(block->pc_tuple, frame_count, fp);
    return;
}

/* Print out a report on all the leaks provided by leaks_info.
   If fp is provided print a detailed report. If not write to
   stdout a brief desc of all the leaks. */
static void
print_leaks (struct memory_info *leak_info, FILE * fp, boolean leak_detection)
{
  int i = 0;
  int free_function = 0;
  char local_buf[4096];
  int actual_records_of_same_type = 0;

  if (!leak_count)
    return;

  leak_detection ?  sprintf (local_buf, "\n%lld bytes leaked in %ld blocks\n\n",
                      total_leak_bytes, total_leak_blocks) :
                    sprintf (local_buf, "\n%lld bytes dangling in %ld blocks\n\n",
                      total_dangling_bytes, total_dangling_blocks);
  if (fp)
    fputs (local_buf, fp);
  else
    puts_filtered (local_buf);

#ifdef GDB_TARGET_IS_HPPA_20W
  if (!fp)
    printf_filtered ("No.   Total bytes     Blocks         Address        "
                                             " Function\n");
  else 
    fprintf (fp, "No.   Total bytes     Blocks         Address        "
                                             " Function\n");
#else
  if (!fp)
    printf_filtered ("No.   Total bytes     Blocks     Address     Function\n");
  else 
    fprintf (fp, "No.   Total bytes     Blocks     Address     Function\n");
#endif

  for (i=0; i < leak_count; i++, leak_info++)
    {
      struct minimal_symbol * m;
      char * function = "???";
      CORE_ADDR pc_addr;
      char * parenth = "  ";

      /* Skip this block, if its of a different type */
     if ( leak_info->leaked_block == leak_detection )
      {
      if (leak_info->pc_tuple)
        {
         /* Suresh: Jan 08:Convert to absolute addr if its offset address*/
          m = msymbol_of_rtc_stack_pc_address( leak_info->pc_tuple, &pc_addr );
          if (m)
            function = SYMBOL_SOURCE_NAME (m);

	  /* JAGae74363 Diwakar Nag */
          DEMANGLE_NEW(function);
        }

        if (strncmp(function,"???",3))
          parenth = ( strchr(function,'(') == NULL &&
                      strchr(function,')') == NULL ) ? "()" : "  ";

        sprintf (local_buf, "%-3d      %-10lld     %-6d  %s   %s%s\n", 
                                        actual_records_of_same_type, leak_info->size,
                                             leak_info->count,
        longest_local_hex_string_custom ((LONGEST) leak_info->address,
                                            "08l"),
                                             function, parenth);
        /* We increment this records of same type so that we print a 
           running sequential number for each record in the output
         */
        actual_records_of_same_type++;
        if (free_function)
          {
             free (function);
             free_function = 0;
          }

        if (!fp)
          puts_filtered (local_buf);
        else 
          fputs (local_buf, fp);
       }
     } /* end of for */

  /* QXCR1001069334, Display this note in the report to help user understand
     it better. */
  if (fp)
    fprintf (fp, "\n\n-------------------------------------------------------"
             "------------------\n");
  sprintf (local_buf, "\nNOTE:\n Function name \"???\" implies that user has set"
           " min-leak-size; function names\nare not fetched for leaks of smaller"
           " sizes for better performance.\n");
  if (!fp)
    printf ("%s", local_buf);
  else
    fprintf (fp, "%s", local_buf);

  if (fp)
    {
        fprintf (fp, "\n\n-------------------------------------------------------------------------\n");
        fprintf (fp, "\n"
"                              Detailed Report\n");
        fprintf (fp, "\n-------------------------------------------------------------------------\n");

        for (i=0; i < leak_count; i++)
          {
            report_this_leak (i, fp, leak_detection);
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
          }
    }
  return;
}

static void
print_interval_blocks (FILE * fp, int total_records)
{
  int i = 0, idx;
  int free_function = 0;

  long long heap_size;
  char local_buf[4096];

  struct memory_info *block_ptr;

  for (idx = 0; idx < total_records; idx++)
    {
       heap_size = (heap_idx[idx].heap_end - heap_idx[idx].heap_start);
       if (fp)
         {
           fprintf (fp, "\n======================================"
                        "=====================\n\n");
           fprintf (fp, "Start Time: %s End Time: %s\nInterval: %d\n",
                         heap_idx[idx].start_time, heap_idx[idx].end_time,
		         heap_idx[idx].interval);
           fprintf (fp, "Actual Heap Usage :\n");
           fprintf (fp, "\tHeap Start\t:\t%s\n",
                         longest_local_hex_string_custom (
                          (LONGEST) heap_idx[idx].heap_start, "08l"));
           fprintf (fp, "\tHeap End\t:\t%s\n",
                         longest_local_hex_string_custom (
                          (LONGEST) heap_idx[idx].heap_end, "08l"));
           fprintf (fp, "\tHeap Size\t:\t%lld bytes\n\n", heap_size);
           fprintf (fp, "Allocations happened during this interval:\n");
           fprintf (fp, "%d bytes allocated in %d blocks\n this n\n",
                         heap_idx[idx].total_bytes, heap_idx[idx].num_blocks);
         }
       else
         {
           printf_filtered ("\n======================================"
                        "=====================\n\n");
           printf_filtered ("Start Time: %s End Time: %s\nInterval: %d\n",
                         heap_idx[idx].start_time, heap_idx[idx].end_time,
		         heap_idx[idx].interval);
           printf_filtered ("Actual Heap Usage :\n");
           printf_filtered ("\tHeap Start\t:\t%s\n",
                              longest_local_hex_string_custom (
                               (LONGEST) heap_idx[idx].heap_start, "08l"));
           printf_filtered ("\tHeap End\t:\t%s\n",
                              longest_local_hex_string_custom (
                               (LONGEST) heap_idx[idx].heap_end, "08l"));
           printf_filtered ("\tHeap Size\t:\t%lld bytes\n\n", heap_size);
           printf_filtered ("Allocations happened during this interval:\n");
           printf_filtered ("%d bytes allocated in %d blocks\n this n\n",
                         heap_idx[idx].total_bytes, heap_idx[idx].num_blocks);
         }

#ifdef GDB_TARGET_IS_HPPA_20W
       if (fp)
         fprintf (fp, "No.   Total bytes     Blocks         Address        "
                                             " Function\n");
       else 
         printf_filtered ("No.   Total bytes     Blocks         Address        "
                                             " Function\n");
#else
       if (fp)
         fprintf (fp, "No.   Total bytes     Blocks     Address     Function\n");
       else 
         printf_filtered ("No.   Total bytes     Blocks     Address     Function\n");
#endif
       for (i=0, block_ptr = heap_idx[idx].block_info; 
             i < heap_idx[idx].unq_blocks; i++, block_ptr++)
         {
            struct minimal_symbol * m;
            char * function = "???";
            CORE_ADDR pc_addr;

            if (block_ptr->pc_tuple)
              {
                /* Suresh: Jan 08:Convert to absolute addr if its 
                   offset address */
                 m = msymbol_of_rtc_stack_pc_address( block_ptr->pc_tuple, &pc_addr );
                 if (m)
                   function = SYMBOL_SOURCE_NAME (m);

                 DEMANGLE_NEW(function);
              }

            parenth = ( strchr(function,'(') == NULL &&
                        strchr(function,')') == NULL ) ? "()" : "  ";

            if (fp)
              fprintf (fp, "%-3d      %-10lld     %-6d  %s   %s%s\n", i, 
                 block_ptr->size, block_ptr->count,
                 longest_local_hex_string_custom (
                   (LONGEST) block_ptr->address, "08l"), function, parenth);
            else 
              {
                 sprintf (local_buf, "%-3d      %-10lld     %-6d  %s   %s%s\n",
                   i, block_ptr->size, block_ptr->count,
                   longest_local_hex_string_custom (
                     (LONGEST) block_ptr->address, "08l"), function, parenth);
                 puts_filtered (local_buf);
              }

           if (free_function) 
             {
                free(function);
	        free_function = 0;
             }
         } /* for */

      if (fp)
        {
            fprintf (fp, "\n\n-------------------------------------------------------------------------\n");
            fprintf (fp, "\n"
"                              Detailed Report\n");
            fprintf (fp, "\n-------------------------------------------------------------------------\n");
            for (i=0; i < heap_idx[idx].unq_blocks; i++)
               {
                  report_this_interval_block (
                      &(heap_idx[idx].block_info[i]), fp, heap_idx[i].total_bytes);
                  fprintf (fp, "\n-------------------------------------------------------------------------\n");
               }
         }
    }
}

/* Helper function for resolving PC addresses in RTC stack trace */
/* Hack!!; Remove all the new objfiles correspoding
   to the newly added unloaded modules by short-circuiting
   objfile chain, if the address is a absolute address 
   corresponding to any of the "live" loaded modules

   Do the converse - if the address is a relative address,
   set the objfile chain(head) to point to the first objfile
   we added for the unloaded objects..

   This hack is required, because i am not able to resolve
   overlapping address between the existing loaded live 
   modules address and the 'fictious' address i gave while 
   adding the objfile for unloaded libs
               
   What is required is a relative lookup of a address(pc), 
   qualified with section/objfile to which the lookup should 
   restrict itself. But i am not able to find such a 
   function -  lookup_minimal_symbol_by_pc_section() 
   doesn't seem to work ??
*/
static struct minimal_symbol * 
msymbol_of_rtc_stack_pc_address( struct gdb_pc_tuple *pc_tuple, CORE_ADDR* swizzled_pc_addr )
{
  int pc_lib_index;
  struct objfile* temp_head;
  struct minimal_symbol *min_sym;
  CORE_ADDR temp_addr;

  /* Suresh: Jan 08: Is it a absolute or offset address ? */
  if ((pc_lib_index = pc_tuple[0].library_index_of_pc) != 
                                          ABSOLUTE_ADDRESS_INDEX)
   {
    if( pc_lib_index >= gdb_shlib_info_count)
       error("PC library index > than the no of library entries \n");

    if (get_inferior_var_value ("gdb_has_read_shlib_info",sizeof(int)) == 0)
       error(" Unloaded library symbols may not have been loaded \n");

    if( first_added_unloaded_module_objfile )
     {
       temp_head = object_files;
       object_files = first_added_unloaded_module_objfile;
       temp_addr =(CORE_ADDR) SWZ (pc_tuple[0].pc + (CORE_ADDR) gdb_shlib_info[pc_lib_index].text_start);
       min_sym = lookup_minimal_symbol_by_pc (temp_addr);
       *swizzled_pc_addr = temp_addr;
       object_files = temp_head;
     }
    else
     { /* Special case; relative address, but library loaded again by
          application !!, so since its already loaded, we would
          not have loaded it explicitly using add_symbol_file() and
          so we need to do the lookup on the main/primary object_files 
          chain 
       */
       temp_addr =(CORE_ADDR) SWZ (pc_tuple[0].pc + (CORE_ADDR) gdb_shlib_info[pc_lib_index].text_start);
       min_sym = lookup_minimal_symbol_by_pc (temp_addr);
       *swizzled_pc_addr = temp_addr;
     }
   }
  else 
   { /* Live modules case */
     /* Operate on the main/primary object_files chain */
     temp_addr = (CORE_ADDR) SWZ (pc_tuple[0].pc);
     min_sym = lookup_minimal_symbol_by_pc (temp_addr);
     *swizzled_pc_addr = temp_addr;
   }
  return(min_sym);
}
static struct objfile*
set_objfile_head_to_unloaded_modules_list(int pc_lib_index)
{
  struct objfile *temp_head = NULL ;

  if( pc_lib_index >= gdb_shlib_info_count)
    error("PC library index > than the no of library entries \n");

  if (pc_lib_index != ABSOLUTE_ADDRESS_INDEX)
   {
    if (get_inferior_var_value ("gdb_has_read_shlib_info",sizeof(int)) == 0)
       error(" Unloaded library symbols may not have been loaded \n");

    if( first_added_unloaded_module_objfile )
     {
        temp_head = object_files;
        object_files = first_added_unloaded_module_objfile;
     }
    /* Special case; relative address, but library loaded again by
       application !!, so since its already loaded, we would
       not have loaded it explicitly using add_symbol_file() and
       so there is no alternate objfile list , so we return NULL
       in this case..
     */
   }
  return temp_head;
}
static void 
restore_objfile_head(int pc_lib_index, struct objfile* temp_head)
{
  if (pc_lib_index != ABSOLUTE_ADDRESS_INDEX && temp_head)
   {
    /* restore back the global object_files */
    object_files = temp_head;
   }
}

/* Print out a report on all the heap blocks provided by block_info.
   If fp is provided print a detailed report. If not write to
   stdout a brief desc of all the blocks. */
static void
print_blocks (struct memory_info *block_info, FILE * fp, int type)
{
  int i = 0;
  int free_function = 0;
  char local_buf[4096];
  long long heap_size;

  heap_size = (heap_end - heap_start);
  /* 
   * JAGaf87040 - this routine used to take care of info heap (HEAP_INFO)
   * or info corruption.  We now have one more type for info heap high-mem
   * (HIGH_MEM_INFO) 
   */
  if (type == HEAP_INFO)
    {
      if (fp)
        {
          fprintf (fp, "Actual Heap Usage:\n");
          fprintf (fp, "\tHeap Start\t=\t%s\n",
                        longest_local_hex_string_custom (
                               (LONGEST) heap_start, "08l"));
          fprintf (fp, "\tHeap End\t=\t%s\n",
                        longest_local_hex_string_custom (
                               (LONGEST) heap_end, "08l"));
          fprintf (fp, "\tHeap Size\t=\t%lld bytes\n\n", heap_size);
          fprintf (fp, "Outstanding Allocations:\n");
          fprintf (fp, "\t%lld bytes allocated in %ld blocks\n\n",
                       total_bytes, total_blocks);
        }
      else
        {
          printf_filtered ("Actual Heap Usage:\n");
          printf_filtered ("\tHeap Start\t=\t%s\n",
                           longest_local_hex_string_custom (
                                   (LONGEST) heap_start, "08l"));
          printf_filtered ("\tHeap End\t=\t%s\n",
                            longest_local_hex_string_custom (
                                   (LONGEST) heap_end, "08l"));
          printf_filtered ("\tHeap Size\t=\t%lld bytes\n\n", heap_size);
          printf_filtered ("Outstanding Allocations:\n");
          printf_filtered ("\t%lld bytes allocated in %ld blocks\n\n",
                               total_bytes, total_blocks);
        }
    }
  else
    {
      char *p;
      p = ((type == HIGH_MEM_INFO) ?
                "High water mark statistic\n" :
                "Following blocks appear to be corrupted\n");

      if (fp)
        fputs (p, fp);
      else
        puts_filtered (p);
    }


  /* JAGaf87040 */
  if (type == HIGH_MEM_INFO)
    {
      if (fp)
        fprintf (fp, "High water mark updated count: %d\n\n", block_info->high_mem_cnt);
      else 
        printf_filtered ("High water mark updated count: %d\n\n", block_info->high_mem_cnt);
    }

#ifdef GDB_TARGET_IS_HPPA_20W
  /* Why is the fprintf different for PA64??*/
  if (fp)
    if (type == BOUNDS_INFO)
        fprintf (fp, "No.   Total bytes     Blocks         Corruption     "
                                             "     Address         Function\n");
    else
        fprintf (fp, "No.   Total bytes     Blocks         Address        "
                                             " Function\n");
  else 
    if (type == BOUNDS_INFO)
        printf_filtered ("No.   Total bytes     Blocks         Corruption    "
                                             "     Address         Function\n");
    else
        printf_filtered ("No.   Total bytes     Blocks         Address        "
                                             " Function\n");
#else
  if (fp)
    if (type == BOUNDS_INFO)
     fprintf (fp, "No.   Total bytes     Blocks     Corruption        Address         Function\n");
    else
     fprintf (fp, "No.   Total bytes     Blocks     Address        Function\n");
  else 
    if (type == BOUNDS_INFO)
     printf_filtered("No.   Total bytes     Blocks     Corruption      Address        Function\n");
    else
     printf_filtered("No.   Total bytes     Blocks     Address        Function\n");
#endif

  for (i=0; i < block_count; i++, block_info++)
    {
      struct minimal_symbol * m;
      char * function = "???";
      char corrupted[15]="None";

      if (type == BOUNDS_INFO && block_info->corrupted == 0)
      {
        continue;
      }

      /* All blocks below min_leak_size will not have stack frames
         captured and we need to handle this. As even in this case 
         pc_tuple will not be '0' and hence we need to explicitly
         check for 'pc' not being '0'
       */
      if (block_info->pc_tuple && block_info->pc_tuple[0].pc != 0)
        {
         CORE_ADDR pc_addr;

         m = msymbol_of_rtc_stack_pc_address(block_info->pc_tuple,&pc_addr);
         if (m)
           function = SYMBOL_SOURCE_NAME (m);

	 /* JAGae74363 Diwakar Nag */
          DEMANGLE_NEW(function);
        }

      parenth = ( strchr(function,'(') == NULL &&
                  strchr(function,')') == NULL ) ? "()" : "  ";

      if(block_info->corrupted == RTC_BAD_HEADER)
         strcpy(corrupted, "Beginning    ");
      else if (block_info->corrupted == RTC_BAD_FOOTER)
         strcpy(corrupted, "End of block ");
      else if (block_info->corrupted == RTC_FREED_BLOCK_CORRUPTED)
         strcpy(corrupted, "Inside block ");

      if (fp)
      {
        if (type == BOUNDS_INFO)
          fprintf (fp, "%-3d      %-10lld     %-6d  %s   %s %s%s\n", i, 
                                             block_info->size,
                                             block_info->count,
                                             corrupted,
           longest_local_hex_string_custom ((LONGEST) block_info->address,
                                            "08l"),
                                             function, parenth);
        else
          fprintf (fp, "%-3d      %-10lld     %-6d  %s   %s%s\n", i, 
                                             block_info->size,
                                             block_info->count,
           longest_local_hex_string_custom ((LONGEST) block_info->address,
                                            "08l"),
                                             function, parenth);
      }
      else 
        {
        if (type == BOUNDS_INFO)
          sprintf (local_buf, "%-3d      %-10lld     %-6d  %s   %s %s%s\n",
                                   i, block_info->size,
                                             block_info->count,
                                             corrupted,
           longest_local_hex_string_custom ((LONGEST) block_info->address,
                                            "08l"), 
                                             function, parenth);
        else
          sprintf (local_buf, "%-3d      %-10lld     %-6d  %s   %s%s\n",
                                   i, block_info->size,
                                             block_info->count,
           longest_local_hex_string_custom ((LONGEST) block_info->address,
                                            "08l"),
                                             function, parenth);
         puts_filtered (local_buf);
        }

        if (free_function) {
          free(function);
	  free_function = 0;
        }
    }

    if (fp)
      {
        fprintf (fp, "\n\n-------------------------------------------------------------------------\n");
        fprintf (fp, "\n"
"                              Detailed Report\n");
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
        for (i=0; i < block_count; i++)
          {
            report_this_block (i, fp, type);
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
          }
       }
}


/* Return true if the size of p2 is greater than size of p1.
   Used by qsort. */
static int
compare_leaks (const void *p1, const void *p2)
{
  struct memory_info * l1, *l2;
  
  l1 = (struct memory_info *) p1;
  l2 = (struct memory_info *) p2;

  return (int) (l2->size - l1->size);
}

/* Suresh: Jan 08: This function reads in the shlib_info table from 
   the inferior 
*/
void
read_gdb_shlib_info ()
{

  int k;
  struct minimal_symbol *m; 
  CORE_ADDR shlib_info_value;
  value_ptr val;

  /* Read in the no of entries in the RTC table ; the size of 
     shlib_info_count is 'int' and if at all it changes in librtc, we 
     should change here as well
   */
  if (target_has_execution)
    {
      gdb_shlib_info_count
        = get_inferior_var_value ("shlib_info_count",sizeof(int));
    }
  else
    {
      /* QXCR1000751009: Read shlib_info_count for core files */
      m = lookup_minimal_symbol ("shlib_info_count", 0, rtc_dot_sl);
      if (!m)
        error ("Internal error : RTC gdb_shlib_info_count missing !\n");

      if ( target_read_memory (SYMBOL_VALUE_ADDRESS (m), (char *) 
                                      &gdb_shlib_info_count, PTR_SIZE) != 0)
        error("Error downloading gdb_shlib_info_count !");

      /* Let's shift if the pointer size is not equal
         to the sizeof CORE_ADDR. */
      if (PTR_SIZE != sizeof (CORE_ADDR))
        gdb_shlib_info_count = gdb_shlib_info_count >> 
                                    (sizeof (CORE_ADDR) - PTR_SIZE) * 8;
    }

  DEBUG( printf(" value of gdb_shlib_info_count is %llu \n",
                gdb_shlib_info_count);)

  if ( gdb_shlib_info_count < 0 )
    error ("Internal error : RTC shlib_info_count is -ve !\n");

  gdb_shlib_info = calloc( gdb_shlib_info_count, 
                           sizeof(struct gdb_shlib_info));
  if( !gdb_shlib_info)
        error ("Out of memory !");

  /* Read the full table into gdb_shlib_info */ 
  m = lookup_minimal_symbol ("shlib_info", 0, rtc_dot_sl);
  if (!m)
    error ("Internal error : RTC shlib_info missing !\n");

  if ( target_read_memory (SYMBOL_VALUE_ADDRESS (m), (char *) 
                                  &shlib_info_value, PTR_SIZE) != 0)
    error("Error downloading shlib_info !");

  DEBUG( printf(" value of shlib_info is %llu \n", shlib_info_value);)

  /* Let's shift if the pointer size is not equal to the sizeof CORE_ADDR. */
  if (PTR_SIZE != sizeof (CORE_ADDR))
    shlib_info_value = shlib_info_value >> 
                                (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

  DEBUG( printf(" value of shlib_info after adjustment is %llu \n", shlib_info_value);)

  /* Loop and read the entire shlib_info table . We need to read member
     fields in shlib_info 1 by 1 and do this for gdb_shlib_info_count of
     structures
  */
  for( k = 0; k < gdb_shlib_info_count; k++)
  {
    struct clone_shlib_info dummy;
    struct clone_shlib_info* clone_shlib_info_ptr=&dummy;

    /* IPF 32 bit target case */
    if (PTR_SIZE != sizeof (CORE_ADDR))
    {
       int field_itr;
     
       /* Read in the individual pointers 1 by 1 */
       for (field_itr = 0; field_itr < 6; field_itr++)
        {
         if( target_read_memory (shlib_info_value + (field_itr * PTR_SIZE), (char *)clone_shlib_info_ptr + (field_itr * sizeof(CORE_ADDR)), PTR_SIZE) != 0 )
             error("Internal error: Not able to read shlib_info from librtc ! \n");
        }	
         /* We will do the adjustments only for the 2 pointers of interest */
         clone_shlib_info_ptr->text_start = clone_shlib_info_ptr->text_start >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;
         clone_shlib_info_ptr->text_end = clone_shlib_info_ptr->text_end >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

       /* We have read the first 5 pointers of the shlib_info structure 
          Now read in the next 3 non-pointer fields of the structure 
       */
       if (target_read_memory (shlib_info_value + (5 * PTR_SIZE), 
         (char *) clone_shlib_info_ptr + (5 * sizeof(CORE_ADDR)) , 
          (sizeof(int) + sizeof(char[MAXPATHLEN+1]) + sizeof(bool))) != 0 )
         error("Internal error: Unable to read shlib_info from librtc!\n");

       /* Advance the pointer of shlib_info. This is UGLY, but should work, 
          as there is just a 2 byte padding in the structure and the 
          size of the member fields add up to the sizeof(struct shlib_info) +          2 bytes = 32*5 + 32 + 8200 + 8 + 16(pad) = 8416 bits = 1052 bytes
          in IA 32 target
          CAUTION: All this will go haywire if the size of librtc's 
          shlib_info changes !! please take care of this if you make
          changes to the other structure...
       */
       shlib_info_value = shlib_info_value + (5 * PTR_SIZE) + sizeof(int) + 
         sizeof(char[MAXPATHLEN+1]) + sizeof(bool) + 2 * sizeof(char);
     }
   else
     {
       /* On all other targets sizeof(clone_shlib_info) and librtc's 
          shlib_info match, so you can increment the debuggee's 
          shlib_info pointer with sizeof(clone_shlib_info) 
        */
       if( target_read_memory (shlib_info_value, (char *) clone_shlib_info_ptr, (sizeof(struct clone_shlib_info))) != 0 )
         error("Internal error: Unable to read shlib_info from librtc!\n");

       /* Advance the pointer of shlib_info */
       shlib_info_value = shlib_info_value + sizeof(struct clone_shlib_info);
      
     }
    /* Lets do member-by-member copy of the fields into gdb_shlib_info[] 
       At this point, clone_shlib_info_ptr should point to start of &dummy 
     */
    gdb_shlib_info[k].text_start = clone_shlib_info_ptr->text_start;
    gdb_shlib_info[k].text_end = clone_shlib_info_ptr->text_end;
    gdb_shlib_info[k].is_it_loaded = clone_shlib_info_ptr->is_it_loaded;
    gdb_shlib_info[k].library_index = clone_shlib_info_ptr->library_index;
    memcpy ((char*)(gdb_shlib_info[k].library_name),(char*)clone_shlib_info_ptr->library_name, sizeof(char[MAXPATHLEN+1]));

  } /* end of for */

  /*
     All the values read are OK, except next_unloaded ptr because it points
     to entries in RTC table(debuggee address space), which are meaningless 
     here in gdb's address space, and it needs to be re-populated..
   */
  head_unloaded_gdb_shlib_info = NULL;
  for (k=0; k < gdb_shlib_info_count ; k++)
  {
   if (gdb_shlib_info[k].is_it_loaded == FALSE)
     {
      gdb_shlib_info[k].next_unloaded = head_unloaded_gdb_shlib_info;
      head_unloaded_gdb_shlib_info = &gdb_shlib_info[k];
     }
  }

  /* To indicate to librtc side saying we have read in the table */
  if ((target_has_execution && target_has_stack))
    set_inferior_var_value ("gdb_has_read_shlib_info", 1);
}

/* Add this leak to our leak_info list. */
static void
add_a_leak (struct gdb_chunk_info * a_leak, boolean leak_detection)
{

  int i;
  long long this_leak_size;

  this_leak_size = a_leak->end - a_leak->base;
  if (leak_detection)
  {
    total_leak_bytes += this_leak_size;
    total_leak_blocks++;
  } else
  {
    total_dangling_bytes += this_leak_size;
    total_dangling_blocks++;
  }

  /* Have we met Mr. Leaky already ? If this is Mr. Anonymous (i.e.,
     no associated stack trace,) let us not lump it with such others.
  */

  /* Sep 07,Suresh: we lump the block, only if the matching block is also of 
     the same type of block( leaked block )

     Also Do not lump the dangling blocks, so that its easier to
     show the dangling pointers corresponding to each dangling block
   */
  if (a_leak->pc_tuple && leak_detection)
    {
      for (i=0; i < leak_count; i++)
        {
          if ((leak_info[i].pointer_to_stack_trace_tuple == (CORE_ADDR) a_leak->pc_tuple) &&
              (leak_info[i].leaked_block == leak_detection))
            {
              leak_info[i].count++;
              leak_info[i].size += this_leak_size;

              if (leak_info[i].min > this_leak_size)
                leak_info[i].min = this_leak_size;
              if (leak_info[i].max < this_leak_size) 
                leak_info[i].max = this_leak_size;

              return;
            }
        }
    }

  i = leak_count;
  /* expand leak info if needed */

  if (i == leak_info_capacity)
    {
      leak_info_capacity += 4096;
      leak_info = xrealloc (leak_info, leak_info_capacity * 
                                  sizeof (struct memory_info));
    }

  leak_info[i].address = a_leak->base;
  leak_info[i].size = this_leak_size;
  leak_info[i].count = 1;
  leak_info[i].pointer_to_stack_trace_tuple = a_leak->pc_tuple;
  leak_info[i].min = leak_info[i].max = this_leak_size;
  /* Suresh : Oct 07:We use the same array to house both leaks as well 
     as dangling blocks, so to distinguish we have this new field */
  leak_info[i].leaked_block = leak_detection; 
  leak_info[i].dptr = a_leak->dptr;

  if (a_leak->pc_tuple)
    {
      int j;

      /* srikanth, 000317, allocate an extra NULL word at the end.
         This is necessary to know where the `pc' array ends. We can't
         rely on frame_count since the user could change it.
      */
      leak_info[i].pc_tuple = calloc (frame_count + 1, sizeof (struct gdb_pc_tuple));

      if (!leak_info[i].pc_tuple)
        error ("Out of memory !");

      for (j=0; j < frame_count; j++)
        {
          /* Suresh: Jan 08 - Is the size arithmetic OK for pc_tuple ? */
          /* IPF 32 bit target case */
          if ( PTR_SIZE != sizeof (CORE_ADDR))
           {

             /* Read the PC first */
	     if ( target_read_memory ((CORE_ADDR) a_leak->pc_tuple +
                                      (j * SIZEOF_LIBRTC_PC_TUPLE),
                                       (char *) &leak_info[i].pc_tuple[j].pc,
                                        sizeof(PTR_SIZE )))
                error ("Error downloading pc_tuple data !");
	     leak_info[i].pc_tuple[j].pc = leak_info[i].pc_tuple[j].pc >>
				  ((sizeof (CORE_ADDR) - PTR_SIZE) * 8);
             /* Now read the index */
	     if ( target_read_memory ( (CORE_ADDR) a_leak->pc_tuple + (j * SIZEOF_LIBRTC_PC_TUPLE) + PTR_SIZE, (char *) &leak_info[i].pc_tuple[j].library_index_of_pc, sizeof(int)))
                error ("Error downloading pc_tuple data !");

           }
          /* On all other targets sizeof(gdb_pc_tuple) and librtc's pc_tuple
             match, so you can increment the debuggee's pc_tuple pointer
             with sizeof(gdb_pc_tuple) */
          else
           {

	     if ( target_read_memory ((CORE_ADDR) a_leak->pc_tuple +
                                      (j * (sizeof(struct gdb_pc_tuple))),
                                       (char *) &leak_info[i].pc_tuple[j],
                                        sizeof(struct gdb_pc_tuple)))
                error ("Error downloading pc_tuple data !");

           }
          if (leak_info[i].pc_tuple[j].pc == 0)
            break;
        }
      }
    else 
      leak_info[i].pc_tuple = 0;

  leak_count ++;
}



/* if fname is "stderr" return stderr
   if fname starts with '+', open the file for append,
   otherwise open the file for write
*/
static FILE *
open_log_file(char *fname)
{
   FILE *fp = stderr;
   if (fname != NULL && strcmp(fname, "stderr") !=0) {
      if (*fname == '+')
        {
          fp = fopen(fname+1, "a+");
        }
      else
        {
          int fd;
          unlink (fname);
          if ((fd = open (fname, O_RDWR | O_EXCL | O_CREAT, 0777)) != -1)
            {
              fp = fdopen(fd, "w");
            }
        }
   }
   if (fp == NULL)
      fp = stderr; 
   return fp;
}

/* Save current thread's registers onto the inferior space.
   Input is the pointer to a CORE_ADDR, which says where the
   next register value should go. CORE_ADDR will be advanced
   after save.
*/
  
static void
save_thread_registers (void * registers)
{

  register int regnum;
  char buffer[MAX_REGISTER_RAW_SIZE];

  CORE_ADDR * reg_file;

  reg_file = (CORE_ADDR*) registers;

  /* Dump the register values onto a malloc block allocated by librtc
     specifically for this purpose.
  */
#ifndef HP_IA64
  for (regnum = 1; regnum < 32; regnum++)
#else
  /* Bindu 101702: On IPF, the general registers are gr0 to gr31. */
  for (regnum = GR0_REGNUM + 1; regnum < GR0_REGNUM + 32; regnum++)
#endif
    {
      read_register_gen (regnum, buffer);
      target_write_memory (*reg_file, buffer, REGISTER_RAW_SIZE (regnum));
      *reg_file = *reg_file + REGISTER_RAW_SIZE (regnum);
    }

  /* Now for the TP register */
  regnum = TP_REGNUM;
  read_register_gen (regnum, buffer);
  target_write_memory (*reg_file, buffer, REGISTER_RAW_SIZE (regnum));
  *reg_file = *reg_file + REGISTER_RAW_SIZE (regnum);

  /* Is there any need to push floating point registers, space
     registers, control registers other than CR27, PSW, PC Q
     etc ... ?
   */

#ifdef HP_IA64
  /* Bindu 101702: For IA64, lets save all the dirty registers too.
     On IA64 (upto Madison) there can be a maximum of 96 dirty registers
     as the number of stacked physical registers are 96. */
  {
    CORE_ADDR addr_bsp, addr_bspstore;

    addr_bsp = read_register (AR0_REGNUM + 17);
    addr_bspstore = read_register (AR0_REGNUM + 18);

    if (addr_bsp > addr_bspstore)
      {
        char rse_buffer[REGISTER_RAW_SIZE (GR0_REGNUM) * 96];
        int dirty_size = (int) (addr_bsp - addr_bspstore);

        /* Make sure not to write beyond 96 in the reg_file. So set the 
           size to 96 if it is > 96. This can never happen though. */
	if (dirty_size > 96 * REGISTER_RAW_SIZE (GR0_REGNUM))
	  dirty_size = 96 * REGISTER_RAW_SIZE (GR0_REGNUM);

        target_read_memory (addr_bspstore, rse_buffer, dirty_size);
        target_write_memory (*reg_file, rse_buffer, dirty_size);
        *reg_file = *reg_file + dirty_size;
      }
  }
#endif

}


#ifdef HP_IA64
/* Bindu 101702: On IA64, we need to save the bottom/top(BSPSTORE) of
   RSE whose info is already flushed into memory for each thread.
   Librtc can use these pointers to scan this memory. 
   This function takes as an argument a pointer to the memory location
   where an inferior memory addr(CORE_ADDR) is saved. (__rtc_RSE_file).
 */
static void
save_thread_rse_state (void *rse_file)
{
  CORE_ADDR *rse_addr = (CORE_ADDR *) rse_file;
  int regnum;
  char buffer[MAX_REGISTER_RAW_SIZE];
  struct frame_info *frame = NULL, *prev_frame = NULL;

  /* Get the bottom most frame. */
  prev_frame = get_current_frame ();
  while ((prev_frame != NULL))
    {
      frame = prev_frame;
      prev_frame = get_prev_frame (frame);
      if (frame == prev_frame)
        break;
    }
  if (frame != NULL)
    {
      /* rse_bottom is bottom most frame's rse_fp. */
      CORE_ADDR rse_bottom = frame->rse_fp;
      CORE_ADDR rse_top;
      regnum = AR0_REGNUM + 18; /* BSPSTORE */
      rse_top = read_register (regnum);
      if (rse_top > rse_bottom)
      {
        /* Write the bottom of rse */
        target_write_memory (*rse_addr, 
			     (char*) &rse_bottom, 
			     sizeof (CORE_ADDR));
	*rse_addr = *rse_addr + sizeof (CORE_ADDR);


        /* Write the top of rse. */
        target_write_memory (*rse_addr, (char*) &rse_top, sizeof (CORE_ADDR));
        *rse_addr = *rse_addr + sizeof (CORE_ADDR);
      }
    }
}
#endif

/* Suresh: Sep 07 If we return FALSE, then no sorting happens between the 2 records 
   and the order is maintained?
   Return false if the p1 is a leaked block and p2 is dangling block
   Return false if the p1 is a leaked block and p2 is leaked block
   Return false if the p1 is a dangling block and p2 is dangling block

   Return True if p1 is dangling and P2 is leaked block

   Used by qsort to order all the leaked blocks first followed by dangling blocks. 
*/
static int
compare_leaks_and_dangling_records (const void *p1, const void *p2)
{
  struct memory_info * l1, *l2;
  
  l1 = (struct memory_info *) p1;
  l2 = (struct memory_info *) p2;

  return (l1->leaked_block == 0 && l2->leaked_block == 1);

}

void
cleanup_dangling_entries()
{
    int i;

    qsort (leak_info, leak_count, sizeof (struct memory_info), compare_leaks_and_dangling_records);

     /* Sep 07, Suresh: Browse thru the leak_info list which now has all leaked 
        blocks at the top and dangling records at the end, We find out the first 
        dangling record, and reset the end pointer to this record, which mimics 
        deleting all dandling records
     */
      for (i=0; i < leak_count; i++)
        {
          /* Find the first dangling record and break */ 
          if (leak_info[i].leaked_block != 1)
             break;
        }

      /* reset the leak_count field to include the last of the leaked records */
      if ( i != leak_count )
         leak_count = i;
        
      /* reset the cumulative totals when we cleanup the entries..*/
      total_dangling_blocks = total_dangling_bytes = 0;
      return;
}

/* Common command for both info leaks and info dangling .*/
static void  
leaks_dangling_common_command (char *leaknum_exp, int from_tty, boolean leak_detection, boolean internal_gc)
{
  value_ptr funcval, val;
  struct gdb_chunk_info a_leak;
  CORE_ADDR new_leaks;
  int new_leaks_error = 0;
  int status; 
  int leak_num = -1;
  int leaked_more = 0;
  FILE * fp = 0;
  struct cleanup * old_chain =0; /* initialized for cmpiler warning */
  CORE_ADDR register_file = 0;
  struct minimal_symbol * m;
  tid_t main_tid, gc_tid, saved_tid;
  CORE_ADDR main_stack_pointer;
  CORE_ADDR thread_stack_pointer = 0; /* initialized for cmpiler warning */
  int thread_state;
  boolean stack_switched = 0;

  if (!check_heap_in_this_run)
    error ("Leak detection is not enabled now.");

  /* On PA64, it is possible that we get here without the necessary libs */ 
  if (!rtc_dot_sl)
    error ("The heap debugging library (librtc) is not linked in !\nHint: Use environment variable LIBRTC_SERVER to pick correct version of librtc and upgrade to the latest linker version (greater than B.11.19);  Or link in librtc with your application to enable gdb support for heap debugging.");

  if (!libc_dot_sl)
    error ("The shared C library is not linked in !");

#ifdef HPUX_1020
  if (is_threaded)
    error ("GDB cannot detect leaks in a user space threads program.\n");
#endif 

#ifdef LOG_BETA_RTC_USED
  log_test_event (LOG_BETA_RTC_USED, 1);
#endif

  dont_repeat ();

  if (leaknum_exp)
    {
      if (could_be_filename(leaknum_exp))
        {
          /* treat it as a filename ... */
          fp = open_log_file (leaknum_exp);
          if (!fp)
            error ("Cannot open file %s\n", leaknum_exp);
          if (fp != stderr)
             old_chain = make_cleanup ((make_cleanup_ftype *) fclose, fp);
        }
      else 
        leak_num = atoi (leaknum_exp);

      /* If a user asks for the n'th leak and we only know about
         m leaks, where m < n, don't complain and quit. Look for
         more leaks if the process is still running.
      */
 
      if (!fp)
        {
          if(leak_detection)
            { if (leak_num < leak_count || !target_has_stack)
              {
                report_this_leak (leak_num, 0, leak_detection);
                return;
              }
              /* else fall through ... */
            }
          else
            { 
                /* For dangling case don't fall through if the requested n'th
                   dangling block is not within the bounds
                */
                report_this_leak (leak_num, 0, leak_detection);
                return;
            }
        }
    }

  if (!target_has_stack)
    {
      if (leak_info)
        print_leaks (leak_info, fp, leak_detection);
      else
        error ("There is no running process.");
      goto cleanup;
    }

#ifdef HAS_END_DOT_O
  /* JAGae05770: the comment below is no longer valid. Now, we try to handle
     signals */
  /* Making *any* command line call after a Ctrl-C confuses end.o so
     thoroughly, it sigsegvs (only on 11.00.) Even when we don't pass
     the signal. No idea why. Just bail out.
  */ 
  if (stop_signal && stop_signal != TARGET_SIGNAL_TRAP) {
    struct target_waitstatus sw;
    char target_was_single_stepped = 1;
    fprintf (stderr,"There are pending signals.");
      
    if(stop_signal == TARGET_SIGNAL_INT) {
      fprintf (stderr," Single Stepping now.\n");
      target_resume(-1, 1, TARGET_SIGNAL_0);
    }

    /* Block some signals which are not commonly handled by the user apps. 
       This list needs to be updated based on need. */
    else if(stop_signal != TARGET_SIGNAL_BUS && 
            stop_signal != TARGET_SIGNAL_SEGV && 
            stop_signal != TARGET_SIGNAL_ILL && 
            stop_signal != TARGET_SIGNAL_QUIT) {
      fprintf (stderr," Single Stepping now.\n");
      target_resume(-1, 1, stop_signal);
    }
    else { 
      fprintf (stderr,"\n");
      target_was_single_stepped = 0;
    }
    stop_signal = 0;
    if(target_was_single_stepped) {
      registers_changed ();
      target_wait (inferior_pid, &sw);
    }
  }
#endif /* HAS_END_DOT_O */

#ifndef HPUX_1020

  /* srikanth, we may be garbage collecting in the context of some
     thread other than the main thread. This poses two problems:
     Firstly, we may overrun the smaller thread stacks by using
     recursion in mark(). We cannot predict how recursive it could get.

     Secondly, the function __rtc_leaks_info () scans only the main
     stack explicitly. All thread stacks are scanned only implicitly.
     These are allocated by using mmap and there is somewhere in the
     user space a pointer to the different thread stacks. This works
     fortunately for us because pthreads is essentially a user threads
     package. The fact that these user space threads are bound 1 to 1
     to a kernel lwp is incidental.

     What we will do is this : if we are not collecting in the
     context of the main thread, then we will simply switch the
     stack to a main thread's stack and switch it back once we are done.
  */

  /* srikanth, 000928, we used to refuse to oblige if there are pending
     events for other threads. With the K threads overhaul, this
     is no longer necessary. The only condition is that the current
     thread should not have unhandled event. This is checked for
     by thread_could_run_gc() below.
  */ 

  /* JAGag12946 
     We used to find the leaks on supposedly "main" thread. But if the 
     main thread is blocked we issued an error message. Instead of that
     we will search for any "good thread -any thread which is running" 
     to initiate leak detection.
  */ 
  gc_tid = select_thread_could_run_gc(&thread_state);
 
  if (gc_tid == 0)
    {
      switch (thread_state) {

        case TTS_WASSUSPENDED : 
          error ("Current thread is suspended. Cannot detect leaks now.");
        case TTS_WASSLEEPING :
          error ("Current thread is blocked. Cannot detect leaks now.\n"
                 "Issuing the 'finish' command might help.");
        case TTS_INSYSCALL :
          error ("Current thread is inside a syscall. Cannot detect leaks "
                 "now.\nIssuing the 'finish' command might help.");
        case TTS_ATEXIT :
          /* After the above signal handling stuff (JAGae05770), gdb hangs 
             in certain situations if the target is not explicitly killed */
          target_kill();
          error ("Current thread is about to terminate. Cannot detect "
                 "leaks now.");
        case 0 :
          error ("Current thread has pending events. Cannot detect "
                 "leaks now.\nIssuing a single step command might help.");
       }
     }

  /* So far so good. Save the registers. */
  m = lookup_minimal_symbol ("__rtc_register_file", NULL, rtc_dot_sl);
  if (!m)
    error ("Internal error : __rtc_register_file missing !\n");
  target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                      (char *) &register_file,
                      sizeof (CORE_ADDR));

  if (PTR_SIZE != sizeof (CORE_ADDR))
    register_file = register_file >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

  rtc_in_progress = 1;
  foreach_live_thread (save_thread_registers, &register_file);

#ifdef HP_IA64
  /* Let's fill up __rtc_RSE_file with a set of bottom of rse stack
     and top of rse stack (bspstore) for each thread. */
  m = lookup_minimal_symbol ("__rtc_RSE_file", NULL, rtc_dot_sl);
  if (!m)
    error ("Internal error : __rtc_RSE_file missing !\n");
  target_read_memory (SYMBOL_VALUE_ADDRESS (m),
                      (char *) &register_file,
                      sizeof (CORE_ADDR));

  if (PTR_SIZE != sizeof (CORE_ADDR))
    register_file = register_file >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

  rtc_in_progress = 1;
  foreach_live_thread (save_thread_rse_state, &register_file);
#endif /* HP_IA64 */

  /* Get the pointer to the main thread stack. We always try to execute
     on the main thread stack if the main thread is present. */
  main_tid = get_main_thread_tid ();
  saved_tid = inferior_pid;

  /* If we are running in the context of the main thread already,
     just get the stack pointer. */
  if (inferior_pid == main_tid)
    {
      main_stack_pointer = read_register (SP_REGNUM);
    }
  /* Otherwise, if main thread is in thread list (hasn't exited), switch
     to it and get the stack pointer. */
  else if (in_thread_list (main_tid))
    {
      switch_to_thread (main_tid);
      main_stack_pointer = read_register (SP_REGNUM);
    }
  /* If main thread doesn't exist, there is no main stack. */
  else
    {
      main_stack_pointer = NULL;
    }

  /* If the current thread is not the one we selected to run GC... */
  if (inferior_pid != gc_tid)
    {
      /* Switch to that thread */
      switch_to_thread (gc_tid);
      /* If there is a main stack, switch to it */
      if (main_stack_pointer)
        {
          stack_switched = 1;
          thread_stack_pointer = read_register (SP_REGNUM);
          write_register (SP_REGNUM, main_stack_pointer);
          registers_changed ();
        }
    }

  /* Set it to 1 if the main thread exied. */
  if (!in_thread_list (main_tid))
    set_inferior_var_value ("__main_thread_exited", 1);

#endif /* !HP_1020 */

  if (!batch_rtc && !internal_gc )
      leak_detection ? printf_filtered ("Scanning for memory leaks...\n\n"):
                       printf_filtered ("Scanning for dangling blocks...\n\n");

  if( leak_detection )
    funcval = find_function_in_inferior ("__rtc_leaks_info");
  else
    funcval = find_function_in_inferior ("__rtc_dangling_info");
  if ( !leak_detection && internal_gc)
    funcval = find_function_in_inferior ("__rtc_dangling_info_internal");

  rtc_in_progress = 1;
  /* Suresh: Oct 07 - FIXME:To suppress a warning in command-line call when we 
     invoke command-line calls implicitly for running Internal GC, we set
     profile_on, which suppresses the warning "Unable to restore previously 
     selected frame" in restore_selected_frame()
     Not too sure of any side-effects of this...
  */
  if (internal_gc )profile_on = 1;
  val = call_function_by_hand (funcval, 0, 0);
  if (internal_gc )profile_on = 0;
  rtc_in_progress = 0;
  new_leaks = value_as_pointer (val);

  /* Switch the threads back if necessary */
  if (saved_tid != inferior_pid)
    switch_to_thread (saved_tid);

#ifndef HPUX_1020
  /* If we switched stacks above, restore the original SP */
  if (stack_switched)
    {
      write_register (SP_REGNUM, thread_stack_pointer);
      registers_changed ();
    }
#endif 
  /* Check whether inferior is ilp32. */
  if (PTR_SIZE != sizeof (CORE_ADDR))
    {
      /* JAGag42742: The error messages are actually communicated as a pointer
       in this case, and the pointer returned from the 32-bit librtc is 
       interpreted as a pointer entity and is later swizzled.
       As we know that the errors are signed int in this case, we can interpret
       the (special case) errors as int and do integer comparison.
      */
      new_leaks_error = (int) new_leaks;
      new_leaks = SWZ ((long) new_leaks);

      if (new_leaks_error == RTC_NOT_RUNNING)
        error ("Heap debugging has not commenced or has been disabled.\n");
      else 
      if (new_leaks_error == RTC_MUTEX_LOCK_FAILED)
       {
      /* Aquiring mutex failed because some other thread had
         acquired the same mutex. It is a good idea to give the heap or corruption
         outputs rather than bailing out even if the mutex is locked.
         The idea is to catch the thread which is about to unlock the mutex
         and call the "info heap" or info corruption or info leaks" commands again.
         since we can be sure that the data structures are sane at the unlock time.
      */
        if (leak_detection && !batch_rtc)
          {
            //struct minimal_symbol * m;
            //CORE_ADDR rtc_mutex_unlock_addr = 0;
            set_inferior_var_value ("failed_to_acquire_mutex", 1);

            /* Warn the user that there might be some slight differences. */
            warning ("Some other thread has locked the mutex. Attempting to move to safe place.");
            warning ("The application execution might be stopped at a slightly different point as"); 
            warning ("compared to the point at which info leak was issued.\n");
            /* Continue the threads */
            proceed ((CORE_ADDR)(long) - 1, TARGET_SIGNAL_0, 0);
            goto cleanup;
          }
        else
          error ("Some other thread has locked the mutex. Try again later.\n");
       }
      else 
      if (new_leaks_error == RTC_UNSAFE_NOW)
        error ("The current thread is inside the allocator. Try again later.\n");
    }
  else
    {
      if (new_leaks == (CORE_ADDR) (long) RTC_NOT_RUNNING)
        error ("Heap debugging has not commenced or has been disabled.\n");
      else
      if (new_leaks == (CORE_ADDR) (long) RTC_MUTEX_LOCK_FAILED)
       {
      /* Aquiring mutex failed because some other thread had
         acquired the same mutex. It is a good idea to give the heap or corruption
         outputs rather than bailing out even if the mutex is locked.
         The idea is to catch the thread which is about to unlock the mutex
         and call the "info heap" or info corruption or info leaks" commands again.
         since we can be sure that the data structures are sane at the unlock time.
      */
        if (leak_detection && !batch_rtc)
          {
            //struct minimal_symbol * m;
            //CORE_ADDR rtc_mutex_unlock_addr = 0;
            set_inferior_var_value ("failed_to_acquire_mutex", 1);

            /* Warn the user that there might be some slight differences. */
            warning ("Some other thread has locked the mutex. Attempting to move to safe place.");
            warning ("The application execution might be stopped at a slightly different point as");
            warning ("compared to the point at which info leak was issued.\n");
            /* Continue the threads */
            proceed ((CORE_ADDR)(long) - 1, TARGET_SIGNAL_0, 0);
            goto cleanup;
          }
        else
          error ("Some other thread has locked the mutex. Try again later.\n");
       }
      else
      if (new_leaks == (CORE_ADDR) (long) RTC_UNSAFE_NOW)
        error ("The current thread is inside the allocator. Try again later.\n");
    }

  if (!new_leaks)
    { 
      if (batch_rtc)
         goto cleanup;

      if (leak_num != -1)
       if (leak_detection) 
            error ("Not a valid leak number.\n");
       else
            error ("Not a valid dangling block number.\n");

      if (!leak_info) 
       {
         if (leak_detection) 
            error ("No new leaks.\n");
//         else
            //error ("No dangling blocks.\n");
       }
      else if (leak_detection )
      {
             if(!query ("No new leaks. Would you like to look at the previous leak(s)?"))
             goto cleanup;
      } 
      else if (!leak_detection)
      {
        /* if its dangling case and there are no blocks, discard the old blocks */
        /* kind off reduntant.. We will do the clean up the next time when more new 
           dangling blocks are found
         */
            cleanup_dangling_entries();
            error ("No dangling blocks.\n");
      }
         
    }

  /* Need to clean-up dangling blocks entries from previous run of the command as they 
     could be stale */
  if(!leak_detection && new_leaks )
            cleanup_dangling_entries();

  while (new_leaks)
    {
      CORE_ADDR addr = 0;
      int field_itr;

      QUIT;

      /* First 4 fields are 1-bit fields. Remaining 5 are pointers in
	 inferior. See rtc.h for rtc_chunk_info structure.*/

      /* Read-in the bit fields first. */
      status = target_read_memory (new_leaks, (char *) &a_leak,
                                           PTR_SIZE);
      if (status != 0)
        error("Error downloading data !");

      /* Read in the next 5 pointer fields into our CORE_ADDR fields. */
      /* Its 6 pointer fields now after dangling_ptrs. */
      for (field_itr = 0; field_itr < 6; field_itr++)
        {
          status = target_read_memory (new_leaks + PTR_SIZE + field_itr * PTR_SIZE,
					 (char*) &addr, PTR_SIZE);
	  if (status != 0)
            error("Error downloading data !");

	  /* Let's shift if the pointer size is not equal to the 
	     sizeof CORE_ADDR. */
          if (PTR_SIZE != sizeof (CORE_ADDR))
	    addr = addr >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

          memcpy ((char*)(&a_leak) + ((field_itr + 1) * sizeof (CORE_ADDR)),
		  (char*)&addr, sizeof (CORE_ADDR));

	  addr = 0;
        }
      if (a_leak.dptr != 0 && !leak_detection)
        {
          dangling_ptrs_struct *p;
          int k = 0; 
          dangling_ptrs *dp;
          CORE_ADDR addr1 = 0;
          addr = 0; 
          dp = (dangling_ptrs *) xmalloc (2 * sizeof (int) + PTR_SIZE);

          target_read_memory ((CORE_ADDR) a_leak.dptr, (char *) dp, 
                              2 * sizeof (int));

          target_read_memory ((CORE_ADDR) a_leak.dptr + 2 * sizeof (int), 
                              (char *) &addr, PTR_SIZE);

          if (PTR_SIZE != sizeof (CORE_ADDR))
	    addr = addr >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

          dp->ptrs = (void *) addr;
          a_leak.dptr = dp;
          p = (dangling_ptrs_struct *) xmalloc (sizeof (dangling_ptrs_struct) 
                                                * a_leak.dptr->count);
          
          addr = 0;

          if (PTR_SIZE != sizeof (CORE_ADDR))
            {
              tmp_dangling_ptrs_struct *p32;
              p32 = (tmp_dangling_ptrs_struct *) xmalloc (sizeof (tmp_dangling_ptrs_struct)
                                                         * a_leak.dptr->count);    
              target_read_memory ((CORE_ADDR) a_leak.dptr->ptrs, (char *) p32,
                                  sizeof (tmp_dangling_ptrs_struct) * a_leak.dptr->count);
              for (k = 0; k < a_leak.dptr->count; k++)
                {
                  p[k].location = p32[k].location;
                  p[k].ptr = (char *) p32[k].ptr;   
                }
              free (p32);
            }
          else
            target_read_memory ((CORE_ADDR) a_leak.dptr->ptrs, (char *) p,
                                sizeof (dangling_ptrs_struct) * a_leak.dptr->count);
               
#if 0                  
          for (; k < a_leak.dptr->count; k++)
            {
              target_read_memory ((CORE_ADDR) a_leak.dptr->ptrs + 
                                  k * SIZEOF_DANGLING_STRUCT, 
                                  (char *) &p[k].location,
                                  sizeof (int));

              target_read_memory ((CORE_ADDR) a_leak.dptr->ptrs +
                                   k * SIZEOF_DANGLING_STRUCT + sizeof (int),
                                   (char *) &addr, PTR_SIZE);
  
              if (PTR_SIZE != sizeof (CORE_ADDR))
                addr = addr >> (sizeof (CORE_ADDR) - PTR_SIZE) * 8;

              p[k].ptr = (void *) addr;
            }
#endif          
          a_leak.dptr->ptrs = p;
        }
 	
      add_a_leak (&a_leak, leak_detection);
      leaked_more = 1;
      new_leaks = a_leak.next_leak;
    }

  if (leaked_more)
    qsort (leak_info, leak_count, sizeof (struct memory_info),
                                    compare_leaks);

  if (!internal_gc)
    if (leak_num != -1)
      report_this_leak (leak_num, 0, leak_detection);
    else 
      print_leaks (leak_info, fp, leak_detection);

cleanup :

  if (fp && fp != stderr)
    {
      int empty_file = (ftell(fp) == 0l);
      if (fclose (fp) == -1)
        error ("Error writing to file\n");
      if (leaknum_exp && could_be_filename(leaknum_exp) && batch_rtc) {
          if (empty_file) {
             /* remove the empty file in batch rtc mode */
             unlink(leaknum_exp);
          }
          else {
             /* remind there is a leak file on the directory */
             if (leaknum_exp[0] != '+')
               warning("Memory leak info is written to \"%s\".", leaknum_exp);
             else
               warning("Memory leak info is appended to \"%s\".", leaknum_exp+1);
          }
      }
      if (old_chain)
        discard_cleanups (old_chain);
    }
}

/* This gets invoked when info dangling is called by the user.*/

/* Suresh, Oct 07: We use the same leak_info array in gdb side for caching 
   the result from the inferior for both leaks and dangling blocks.
*/
static void  
dangling_info_command (char *leaknum_exp, int from_tty)
{
  /* Suresh, Jan 08 - Add unload library symbols before printing 
     stack trace..
   */
  add_symbols_of_unloaded_libraries(ALL_UNLOADED_LIBRARIES);
  leaks_dangling_common_command(leaknum_exp, from_tty, FALSE, FALSE);
}

/* This gets invoked when info leaks is called by the user.*/
static void  
leaks_info_command (char *leaknum_exp, int from_tty)
{
  /* Suresh, Jan 08 - Add unload library symbols before printing 
     stack trace.. 
   */
  add_symbols_of_unloaded_libraries(ALL_UNLOADED_LIBRARIES);
  set_inferior_var_value("__rtc_bounds_or_heap", LEAKS_INFO);
  leaks_dangling_common_command(leaknum_exp, from_tty, TRUE, FALSE);
}

/* Return 1 if the p1's pointer_to_stack_trace is greater than
   to that of p2, -1 other way round. 0 if equal. */
static int
compare_blocks (const void *p1, const void *p2)
{
  struct memory_info * b1, *b2;
  
  b1 = (struct memory_info *) p1;
  b2 = (struct memory_info *) p2;

  if (b1->pointer_to_stack_trace_tuple < b2->pointer_to_stack_trace_tuple)
    return -1;
  else if (b1->pointer_to_stack_trace_tuple > b2->pointer_to_stack_trace_tuple)
    return 1;

  return 0;
}

/* Returns > 0 if p2's size is greater than that of p1, 0 if equal, else
   returns < 0. Used to sort heap blocks by size. */
static int
compare_blocks_by_size (const void *p1, const void *p2)
{
  struct memory_info * b1, *b2;
  
  b1 = (struct memory_info *) p1;
  b2 = (struct memory_info *) p2;

  /* QXCR1000883689 : Heap profile lists biggest allocation last. 
     The value which was getting returned previously, was causing
     an integer over-flow. */
  if (b1->size < b2->size)
    return 1;
  else if (b1->size > b2->size)
    return -1;

  return 0;

}

/* At this point the inferior has gathered the profile statistics
   and has dumped it to a file named /tmp/__heap_info.<inferior_pid>.
   This binary file has the following format : For each allocated block
   there is one record of output. (The field separator is not
   actually there.)

   size|address|pointer-to-stack-trace-tuple|pc_tuple[0]|pc_tuple[1]|...|pc_tuple[n-1]

   where size is the size of the block.
         address is the base address of the block.
         pointer-to-stack-trace is an inferior pointer which points to
                 the stack trace collected for the allocation. The
                 reason we are interested in it is because, if two
                 or more blocks are from the same allocation call
                 stack, they will have the same value for this field.
                 Thus it is useful for collating blocks based on
                 the allocation stack.
         pc_tuple[x] is the PC & libray index value of the x'th caller 
                 in the call stack.

  There will be a total of 'frame-count' number of these.
*/
                 
static int
read_interval_idx_file (void)
{
  FILE *fp;
  int i = 0;

  if (heap_interval_idxfile_fp == NULL)
    {
      char heap_interval_idxfile[100];
      int real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
    					PIDGET(inferior_pid) :
    					get_pid_for(inferior_pid));
    
      sprintf(heap_interval_idxfile, "/tmp/__heap_interval_info.idx.%u", 
    						real_inferior_pid);
      if ((fp = fopen (heap_interval_idxfile, "r")) == NULL)
        return 0;
      heap_interval_idxfile_fp = fp;

      /* Clean up file after it's opened by this process (and the librtc,
	 which creates it).  Will disappear on everyone's fclose.  */
      unlink(heap_interval_idxfile);
    }
    else
      fp = heap_interval_idxfile_fp;

  while (fread (&heap_idx[i].start_time, TIME_STR_LEN, 1, fp))
    {
       heap_idx[i].start_time[TIME_STR_LEN -1] = NULL;
       fread (&heap_idx[i].end_time, TIME_STR_LEN, 1, fp);
       heap_idx[i].end_time[TIME_STR_LEN -1] = NULL;
       fread (&heap_idx[i].interval, sizeof (heap_idx[i].interval), 1, fp);
       fread (&heap_idx[i].num_blocks, sizeof (heap_idx[i].num_blocks), 1, fp);
       i++;
     }

   if (!feof(fp))
       error("Heap interval index file is corrupted");

   /* reposition file so if we read it again it will be like we reopened it. */
   rewind(fp);
   return i;
}

#define FREAD_IDX_DATA_FILE(a,b,c,d) if (!fread(a, b, c, d)) \
                         error("Error reading heap interval data file");
static int
down_load_interval_data (void)
{
  int i, idx;
  int j;
  int tmp_flag = 0;
  int total_records = 0;
  int unique_allocations = 0;

  FILE * fp;
  struct memory_info * interval_data;

  CORE_ADDR tmp_size;
  CORE_ADDR last_allocation_address;

  if ((total_records = read_interval_idx_file()) == 0)
    error ("No heap interval index file.");

  if (heap_interval_filename_fp == NULL)
    {
      char heap_interval_filename[100];
      int real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
    					PIDGET(inferior_pid) :
    					get_pid_for(inferior_pid));

      sprintf(heap_interval_filename, "/tmp/__heap_interval_info.%u", 
    						real_inferior_pid);
      if ((fp = fopen (heap_interval_filename, "r")) == NULL)
        error ("No heap interval data file.");
      heap_interval_filename_fp = fp;

      /* Clean up file after it's opened by this process (and the librtc,
	 which creates it).  Will disappear on everyone's fclose.  */
      unlink(heap_interval_filename);
    }
  else
    {
      fp = heap_interval_filename_fp;
      /* rewind to read file from the beginning */
      rewind (fp);
    }

  for (idx = 0; idx < total_records; idx++)
    {
       interval_data = xmalloc (heap_idx[idx].num_blocks * sizeof (struct memory_info));

       FREAD_IDX_DATA_FILE (&heap_idx[idx].heap_start, sizeof (CORE_ADDR), 1, fp);

       FREAD_IDX_DATA_FILE (&heap_idx[idx].heap_end, sizeof (CORE_ADDR), 1, fp);

       for (i = 0; i < heap_idx[idx].num_blocks; i++)
         {
            int j;

            interval_data[i].count = 1;
            interval_data[i].pc_tuple = 0;

            FREAD_IDX_DATA_FILE (&tmp_size, sizeof (CORE_ADDR), 1, fp);

            interval_data[i].size = tmp_size;

            heap_idx[idx].total_bytes += interval_data[i].size;

            FREAD_IDX_DATA_FILE (&interval_data[i].address, sizeof (CORE_ADDR), 1, fp);
            FREAD_IDX_DATA_FILE (&interval_data[i].pointer_to_stack_trace_tuple, sizeof (CORE_ADDR), 1, fp);
            FREAD_IDX_DATA_FILE (&tmp_flag, sizeof (int), 1, fp);

            interval_data[i].corrupted = tmp_flag;

            interval_data[i].pc_tuple = calloc (frame_count + 1, sizeof (struct gdb_pc_tuple));
            if (!interval_data[i].pc_tuple)
              error ("Out of memory reading heap interval data!");

            for( j =0; j < frame_count ; j++)
             {
                FREAD_IDX_DATA_FILE (&interval_data[i].pc_tuple[j].pc, sizeof (CORE_ADDR), 1, fp);
                FREAD_IDX_DATA_FILE (&interval_data[i].pc_tuple[j].library_index_of_pc, sizeof (int), 1, fp);
             }
            DEBUG(printf("i= %d count=%d pc=%x add=%x size=%d\n", i, interval_data[i].count, interval_data[i].pc_tuple, interval_data[i].address, interval_data[i].size);)
         }

      /* Sort on the 'pointer to stack trace' field.  */
      qsort (interval_data, heap_idx[idx].num_blocks, sizeof (struct memory_info),
        compare_blocks);

      /* Find total unique blocks */
      unique_allocations = 0;
      last_allocation_address = (CORE_ADDR) (long) -1;
      for (i=0; i < heap_idx[idx].num_blocks; i++)
        {
           if (interval_data[i].pointer_to_stack_trace_tuple != last_allocation_address)
             {
                unique_allocations++;
                last_allocation_address = interval_data[i].pointer_to_stack_trace_tuple;
             }
        }

     heap_idx[idx].block_info = xmalloc (unique_allocations * 
                           sizeof (struct memory_info));
     heap_idx[idx].unq_blocks = unique_allocations;

     /* Aggregate the blocks */
     unique_allocations = 0;
     last_allocation_address = (CORE_ADDR) (long) -1;
     for (i=0, j=-1; i < heap_idx[idx].num_blocks; i++)
       {
          if (interval_data[i].pointer_to_stack_trace_tuple != last_allocation_address)
            {
               heap_idx[idx].block_info[++j] = interval_data[i];
               heap_idx[idx].block_info[j].min = 
               heap_idx[idx].block_info[j].max = interval_data[i].size;

               last_allocation_address = interval_data[i].pointer_to_stack_trace_tuple;
            }
          else 
            {
               heap_idx[idx].block_info[j].count++;
               heap_idx[idx].block_info[j].size += interval_data[i].size;

               if (heap_idx[idx].block_info[j].min > interval_data[i].size)
                  heap_idx[idx].block_info[j].min = interval_data[i].size;
               if (heap_idx[idx].block_info[j].max < interval_data[i].size)
                  heap_idx[idx].block_info[j].max = interval_data[i].size;

               free (interval_data[i].pc_tuple);
             }
       } /* for */
     free (interval_data);

     /* Sort based on the size */
     qsort (heap_idx[idx].block_info, heap_idx[idx].unq_blocks,
       sizeof (struct memory_info), compare_blocks_by_size);
   } /* for */

   /* reposition file to be reread from scratch */
   rewind (fp);
   return total_records;
}


/*
 * JAGaf87040 - add a subrequest to read from /tmp/__heap_info.<inferior_pid>
 * for info heap or /tmp/__high_mem_info.<inferior_pid> for info heap high-mem
 */
static void
down_load_data (int subrequest)
{
  struct memory_info * data;
  int i;
  FILE * fp = NULL;
  int j;
  int unique_allocations = 0;
  int tmp_flag = 0;
  CORE_ADDR last_allocation_address;
  CORE_ADDR tmp_size;
  char filename[100];	/* pathname for communication file to librtc */

  int real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
					PIDGET(inferior_pid) :
					get_pid_for(inferior_pid));

  /* construct temp file name */
  sprintf (filename, "/tmp/%s.%u", (subrequest == HIGH_MEM_INFO) ?
					"__high_mem_info" :
					"__heap_info",
				   real_inferior_pid);

  DEBUG(printf ("down_load_data(): using %s as communication file from librtc.\n",
		filename));

  fp = fopen (filename, "r");
  if (!fp)
  {
    perror_with_name (filename);
    DEBUG(printf ("down_load_data(): Heap analysis file %s missing!\n", filename);)
    fflush (stdout);
    return;
  }

  DEBUG(printf ("down_load_data(): Opened %s file successfully!\n", filename));

  /* Clean up file after it's opened by this process (and the librtc,
	 which creates it).  Will disappear on everyone's fclose.  */
  unlink(filename);

  total_bytes = 0;
  total_blocks = block_count;

  data = xmalloc (block_count * sizeof (struct memory_info));

  /* Read in heap start and end address for info heap command */
  /* QXCR1000751009: Handle bounds info. Heap start and end is not
     written for corruption information */
  if (subrequest != HIGH_MEM_INFO && subrequest != BOUNDS_INFO)
    {
      fread(&heap_start, sizeof (CORE_ADDR), 1, fp);
      fread(&heap_end, sizeof (CORE_ADDR), 1, fp);
    }

  /* Read in the raw data ... */
  for (i = 0; i < block_count; i++)
  {
      data[i].count = 1;
      data[i].pc_tuple = 0;

      /* librtc writes 'size' of size of CORE_ADDR. */
      fread (&tmp_size, sizeof (CORE_ADDR), 1, fp);
      data[i].size = tmp_size;

      total_bytes += data[i].size;

      fread (&data[i].address, sizeof (CORE_ADDR), 1, fp);
      fread (&data[i].pointer_to_stack_trace_tuple, sizeof (CORE_ADDR), 1, fp);
      fread (&tmp_flag, sizeof (int), 1, fp);
      data[i].corrupted = tmp_flag;

      data[i].pc_tuple = calloc (frame_count + 1, sizeof (struct gdb_pc_tuple));
      if (!data[i].pc_tuple)
        error ("Out of memory!");
      for( j =0; j < frame_count ; j++)
       {
        fread (&data[i].pc_tuple[j].pc, sizeof (CORE_ADDR), 1, fp);
        fread (&data[i].pc_tuple[j].library_index_of_pc, sizeof (int), 1, fp);
       }

      /* JAGaf87040 - download the count */
      if (subrequest == HIGH_MEM_INFO)
        fread (&data[i].high_mem_cnt, sizeof (int), 1, fp);
    }
  fclose (fp);
  qsort (data, block_count, sizeof (struct memory_info),
                                    compare_blocks);

  /* At this point the blocks are sorted on the 'pointer to
     stack trace' field. So all blocks originating in the
     same call stack are adjacent to each other ...
  */
  /* Sep 2007, Suresh: We don't need to collate the blocks for a "info corruption" report
     as the user would like to see all the corrupted block addresses
     explicitly, to better debug and find the route cause of the corruption..
   */
  if(subrequest == BOUNDS_INFO)
  {
     unique_allocations = block_count;
  }
  else
  {
     unique_allocations = 0;
     last_allocation_address = (CORE_ADDR) (long) -1;
     for (i=0; i < block_count; i++)
       {
         if (data[i].pointer_to_stack_trace_tuple != last_allocation_address)
           {
             unique_allocations++;
             last_allocation_address = data[i].pointer_to_stack_trace_tuple;
           }
       }
  }
  block_info = xmalloc (unique_allocations *
                           sizeof (struct memory_info));

  /* Collate the blocks now so that allocations from the same
     call stack show up together.
  */
  unique_allocations = 0;
  last_allocation_address = (CORE_ADDR) (long) -1;
  for (i=0, j=-1; i < block_count; i++)
    {
      /* No block collating for info corruption report.. */
      if ((subrequest == BOUNDS_INFO) || (data[i].pointer_to_stack_trace_tuple != last_allocation_address))
        {
          block_info[++j] = data[i];
          block_info[j].min = block_info[j].max = data[i].size;
          last_allocation_address = data[i].pointer_to_stack_trace_tuple;
        }
      else
        {
          block_info[j].count++;
          block_info[j].size += data[i].size;

          if (block_info[j].min > data[i].size)
            block_info[j].min = data[i].size;
          if (block_info[j].max < data[i].size)
            block_info[j].max = data[i].size;

          free (data[i].pc_tuple);

        }

      /* JAGaf87040 - transfer the count from i data to j block info */
      block_info[j].high_mem_cnt = data[i].high_mem_cnt;  

    }

  free (data);
  block_count = j + 1;

  /* Now sort based on the size ... */
  qsort (block_info, block_count, sizeof (struct memory_info),
                                    compare_blocks_by_size);

  for (i = block_count - 1; i >= 0 && block_info[i].size < min_heap_size ; i--)
    free (block_info[i].pc_tuple);   /* release memory since we are going to filter out this */

  block_count = i+1;  /* We will reclaim any excess memory in heap_info_command_common */
}

static void
allocate_heap_idx (void)
{
   int i;
  
   /* Data is freed only when user resets the interval slot so that
      he can perform heap_interval_info as many time as he wants.
    */
   for (i = 0; heap_idx[i].block_info; i++) 
        if (!heap_idx[i].block_info)
           free(heap_idx[i].block_info);
   free(heap_idx);

   heap_idx = xmalloc (check_heap_interval_repeat_value * 
                sizeof (struct interval));
   memset(heap_idx, NULL, check_heap_interval_repeat_value *sizeof (struct interval));
   return;
}

static void
heap_interval_info_command (char *output_file, int from_tty)
{
  int i;
  long ret_val;
  int thread_state;
  int block_num = -1;

  FILE * fp = 0;
  value_ptr funcval, val;
  struct cleanup * old_chain = 0; /* initialized for compiler warning */

  if (!check_heap_in_this_run)
    error ("Heap analysis is not enabled now.");

  if (!rtc_dot_sl)
#ifndef HP_IA64
    error ("The heap debugging library (librtc) is not linked in !\nHint:\
            Use environment variable LIBRTC_SERVER to pick correct version \
            of librtc and upgrade to the latest linker version (greater \
            than B.11.19);  Or link in librtc with your application to \
            enable gdb support for heap debugging.");
#else /* HP_IA64 */
    error ("The heap debugging library (librtc) is not loaded!\nHint:\
            Use environment variable LIBRTC_SERVER to pick correct version \
            of librtc.");
#endif /* HP_IA64 */

  if (!libc_dot_sl)
    error ("The shared C library is not linked in !");

  dont_repeat ();

  if (output_file)
    {
      int fd;
      unlink (output_file);
      fd = open (output_file, O_RDWR | O_EXCL | O_CREAT, 0777);
      if (fd == -1 || (fp = fdopen (fd, "w")) == NULL)
        error ("Cannot open file %s\n", output_file);
    }

  if (!target_has_stack)
    {
        error ("There is no running process.");
        goto cleanup;
    }

#ifdef HAS_END_DOT_O
  /* JAGae05770: Now, we try to handle signals */
  if (stop_signal && stop_signal != TARGET_SIGNAL_TRAP) {
    struct target_waitstatus sw;
    char target_was_single_stepped = 1;
    fprintf (stderr,"There are pending signals.");
      
    if(stop_signal == TARGET_SIGNAL_INT) {
      fprintf (stderr," Single Stepping now.\n");
      target_resume(-1, 1, TARGET_SIGNAL_0);
    }
    /*Block some signals which are not commonly handled by the user apps. This
      list needs to be updated based on need
     */
    else if(stop_signal != TARGET_SIGNAL_BUS && 
            stop_signal != TARGET_SIGNAL_SEGV && 
            stop_signal != TARGET_SIGNAL_ILL && 
            stop_signal != TARGET_SIGNAL_QUIT) {
      fprintf (stderr," Single Stepping now.\n");
      target_resume(-1, 1, stop_signal);
    }
    else { 
      fprintf (stderr,"\n");
      target_was_single_stepped = 0;
    }
    stop_signal = 0;
    if(target_was_single_stepped) {
      registers_changed ();
      target_wait (inferior_pid, &sw);
    }
  }
#endif /* HAS_END_DOT_O */

#ifndef HPUX_1020
  thread_state = thread_could_run_gc (inferior_pid);
  switch (thread_state) {

    case TTS_WASSUSPENDED : 
      error ("Current thread is suspended. Cannot profile now.");
    case TTS_WASSLEEPING :
      error ("Current thread is blocked. Cannot profile now.");
    case TTS_INSYSCALL :
      error ("Current thread is inside a syscall. Cannot profile now.");
    case TTS_ATEXIT :
      /*After the above signal handling stuff (JAGae05770), gdb hangs in certain
        situations if the target is not explicitly killed */
      target_kill();
      error ("Current thread is about to terminate. Cannot profile now.");
    case 0 :
      error ("Current thread has pending events. Cannot profile now.");
  }
#endif

  allocate_heap_idx();

  if (!batch_rtc)
    /* JAGaf87040 */
    if (fp)
      printf_filtered ("Analyzing heap and writing results to %s...\n\n", output_file);
    else 
      printf_filtered ("Analyzing heap ...\n\n");

  funcval = find_function_in_inferior ("__rtc_heap_interval_info");
  rtc_in_progress = 1;
  val = call_function_by_hand (funcval, 0, 0);
  rtc_in_progress = 0;

  ret_val = (long) value_as_long (val);
  
  if (ret_val == RTC_NOT_RUNNING)
    error ("Heap debugging has not commenced or has been disabled.\n");
  else 
  if (ret_val == RTC_MUTEX_LOCK_FAILED)
    error ("Some other thread has locked the mutex. Try again later.\n");
  else 
  if (ret_val == RTC_UNSAFE_NOW)
    error ("Current thread is inside the allocator. Try again later.\n");
  else
  if (ret_val == RTC_FOPEN_FAILED)
    error ("Failed to open temporary file.\n");
  else 
  if (block_count == RTC_FCLOSE_FAILED)
    error ("Failed to close temporary file.\n");
  print_interval_blocks (fp, down_load_interval_data ());

cleanup :
  if ((fp) && (fclose (fp) == -1))
    error ("Error writing to output file\n");
}


/*
 * do_arena routine implements info heap process by calling
 * libc routine.  Print information libc routine sends back  
 * here.  
 */ 
static void 
print_process_mem (struct total_arena_info *total_ar, FILE *fp)
{

#if  defined(HP_IA64)
  asection *notes  = NULL;
  struct objfile *obj;
  int note_size;
  boolean section_stat;
  unsigned int myloc[100];

  /* 
   * read in address_space_model 
   * for (obj=object_files; obj != NULL; obj=obj->next)
   *
   * this is for ia only.  do elfdump -s a.out. the hpux_options 
   * is at the top.  The exec_type is at the 8th position of 
   * hpux_options henc myloc[7].  
   */  
           
  notes =  bfd_get_section_by_name (object_files->obfd, ".note.hpux_options"); 
  note_size =  (int) bfd_section_size (object_files->obfd, notes);
  section_stat = bfd_get_section_contents(object_files->obfd,notes,
                                          myloc,0,note_size);

#endif 


  /* GDB_TARGET_IS_HPPA is for both PA32 and PA64 so the test case
   * for PA64 was crashing trying to process SOM stuff.  Use the 
   * correct defines to make sure it's only PA32. 
   */
#if defined (GDB_TARGET_IS_HPPA) && !defined(GDB_TARGET_IS_HPPA_20W)
  struct som_exec_data *exec_dat; 
  char prt_string[200];

  /* 
   * important files to get magic information
   * som.c, som.h, /usr/include/filehdr.h, /usr/include/sys/magic.h 
   * 
   * #define RELOC_MAGIC     0x106           * relocatable only *
   * #define EXEC_MAGIC      0x107           * normal executable *
   * #define SHARE_MAGIC     0x108           * shared executable *
   * #define SHMEM_MAGIC     0x109           * Quad2 used for shared memory *
   * #define AR_MAGIC        0xFF65
   *
   * optional (implementation-dependent) file types:
   * #define DEMAND_MAGIC    0x10B           * demand-load executable *
   * #define DL_MAGIC        0x10D           * dynamic load library *
   * #define SHL_MAGIC       0x10E
   *
   *
   * /usr/include/aouthdr.h describe the bits in exec_flag 
   *   bit 10 - Third quadrant of address space used as private 
   *            data space when set. 
   *
   *   bit  9 - Fourth quadrant of address space 
   *   0x00200004  q3p enable 
   *   0x00600004  both 3 & 4 enable
   *   0x00000004  both are disable
   *   note if 3 is disable and 4 is enable you can't run the a.out
   *   so there won't a case 0x00400004.  use odump to see the quadrant
   *   info. 
   */

   exec_dat = obj_som_exec_data(object_files->obfd);

   switch (exec_dat->the_magic)
   {
      case  RELOC_MAGIC:           /* 0x106  relocatable only */
        strcpy(prt_string, "Executable type: RELOC_MAGIC, "); 
        break; 
      case  EXEC_MAGIC:            /* 0x107  normal executable */
        strcpy(prt_string, "Executable type: EXEC_MAGIC, "); 
        break; 
      case  SHARE_MAGIC:           /* 0x108  shared executable */
        strcpy(prt_string, "Executable type: SHARE_MAGIC, "); 
        break; 
      case  SHMEM_MAGIC:           /* 0x109  Quad2 used for shared memory */
        strcpy(prt_string, "Executable type: SHMEM_MAGIC, "); 
        break; 
      case  DEMAND_MAGIC:          /* 0x10B  demand-load executable */
        strcpy(prt_string, "Executable type: DEMAND_MAGIC, "); 
        break; 
      case DL_MAGIC:               /* 0x10D  dynamic load library */
        strcpy(prt_string, "Executable type: DL_MAGIC, "); 
        break; 
      case  SHL_MAGIC:             /* 0x10E  shared library */
        strcpy(prt_string, "Executable type: SHL_MAGIC, "); 
        break; 
      default:
        strcpy(prt_string, "Executable type: Unknown magic, "); 
        break; 
   } /* switch */


   /* the order of the if below is important */
   if ((exec_dat->exec_flags & 0x00600000) == 0x00600000)
     strcat (prt_string, "q3p: enable, q4p: enable");
   else 
     if ((exec_dat->exec_flags & 0x00200000) == 0x00200000)
        strcat (prt_string, "q3p: enable, q4p: disable");
     else 
        strcat (prt_string, "q3p: disable, q4p: disable");

#endif 


  if (fp)
    {
       fprintf(fp,"Total space in arenas: %llu\n", total_ar->total_space);
       fprintf(fp,"Number of bytes in free small blocks: %llu\n", 
               total_ar->num_bytes_FSB);
       fprintf(fp,"Number of bytes in used small blocks: %llu\n", 
               total_ar->num_bytes_USB);
       fprintf(fp,"Number of bytes in free ordinary blocks: %llu\n", 
               total_ar->num_bytes_FOB);
       fprintf(fp,"Number of bytes in used ordinary blocks: %llu\n", 
               total_ar->num_bytes_UOB);
       fprintf(fp,"Number of bytes in holding block header:  %llu\n", 
               total_ar->num_bytes_HBH);
       fprintf(fp,"Number of small blocks: %llu\n", total_ar->num_small_blocks);     
       fprintf(fp,"Number of used ordinary blocks: %llu\n", 
               total_ar->num_ordinary_blocks);
       fprintf(fp,"Number of holding blocks: %llu\n", 
               total_ar->num_holding_blocks);
       fprintf(fp,"Number of free ordinary blocks: %llu\n", 
               total_ar->num_free_ordinary_blocks);
       fprintf(fp,"Number of free small blocks: %llu\n", 
               total_ar->num_free_small_blocks);

       fprintf(fp,"Small block allocator parameters\n");

       if (total_ar->SBA_params.is_sba_enabled)
         fprintf(fp,"\tenabled: yes\n");
       else
         fprintf(fp,"\tenabled: no\n");

       fprintf(fp,"\tmaxfast: %llu\n",  total_ar->SBA_params.maxfast);
       fprintf(fp,"\tnumblks: %llu\n",  total_ar->SBA_params.numblks);
       fprintf(fp,"\tgrain: %llu\n",  total_ar->SBA_params.grain);
       fprintf(fp,"cache\n");
       fprintf(fp,"\tenabled: %d\n",  total_ar->cache_info.is_cache_enabled);
       fprintf(fp,"\tmiss: %llu\n",  total_ar->cache_info.no_of_cache_miss);
       fprintf(fp,"\tbucketsize: %llu\n",  total_ar->cache_info.bucketsize);
       fprintf(fp,"\tbuckets: %llu\n",  total_ar->cache_info.buckets);
       fprintf(fp,"\tretirement: %llu\n", total_ar->cache_info.retirement);

#if  defined(HP_IA64)
       if (section_stat == true)
         {
            fprintf(fp,"Exec type: %s\n", exec_type_string[myloc[7]]);
         }
#endif 

#if defined (GDB_TARGET_IS_HPPA) && !defined(GDB_TARGET_IS_HPPA_20W)
       fprintf(fp,"%s\n", prt_string);
#endif 

    }
  else
    {
       printf_filtered("Total space in arenas: %llu\n", total_ar->total_space);
       printf_filtered("Number of bytes in free small blocks: %llu\n", 
                       total_ar->num_bytes_FSB);
       printf_filtered("Number of bytes in used small blocks: %llu\n", 
                       total_ar->num_bytes_USB);
       printf_filtered("Number of bytes in free ordinary blocks: %llu\n", 
                       total_ar->num_bytes_FOB);
       printf_filtered("Number of bytes in used ordinary blocks: %llu\n", 
                       total_ar->num_bytes_UOB);
       printf_filtered("Number of bytes in holding block header:  %llu\n", 
                       total_ar->num_bytes_HBH);
       printf_filtered("Number of small blocks: %llu\n", 
                       total_ar->num_small_blocks);     
       printf_filtered("Number of used ordinary blocks: %llu\n", 
                       total_ar->num_ordinary_blocks);
       printf_filtered("Number of holding blocks: %llu\n", 
                       total_ar->num_holding_blocks);
       printf_filtered("Number of free ordinary blocks: %llu\n", 
                       total_ar->num_free_ordinary_blocks);
       printf_filtered("Number of free small blocks: %llu\n", 
                       total_ar->num_free_small_blocks);

       printf_filtered("Small block allocator parameters\n");
       
       if ( total_ar->SBA_params.is_sba_enabled)
         printf_filtered("\tenabled: yes\n");
       else
         printf_filtered("\tenabled: no\n");

       printf_filtered("\tmaxfast: %llu\n",  total_ar->SBA_params.maxfast);
       printf_filtered("\tnumblks: %llu\n",  total_ar->SBA_params.numblks);
       printf_filtered("\tgrain: %llu\n",  total_ar->SBA_params.grain);
       printf_filtered("cache\n");
       printf_filtered("\tenabled: %d\n",  total_ar->cache_info.is_cache_enabled);
       printf_filtered("\tmiss: %llu\n",  total_ar->cache_info.no_of_cache_miss);
       printf_filtered("\tbucketsize: %llu\n",  total_ar->cache_info.bucketsize);
       printf_filtered("\tbuckets: %llu\n",  total_ar->cache_info.buckets);
       printf_filtered("\tretirement: %llu\n", total_ar->cache_info.retirement);

#if  defined(HP_IA64)
       if (section_stat == true)
         {
            printf_filtered("Exec type: %s\n", exec_type_string[myloc[7]]);
         }
#endif 

#if defined (GDB_TARGET_IS_HPPA) && !defined(GDB_TARGET_IS_HPPA_20W)
       printf_filtered("%s\n", prt_string);
#endif 

  }
}


/*
 * This routine is responsible for printing info heap arena info
 * The serious work is in do_arena.  do_arena calls this routine to print.  
 */ 

static void 
print_arena_mem (struct arena_info *ar_info, FILE *fp)
{

  if (fp)
    {
       fprintf(fp,"\n");
       fprintf(fp,"Total number of blocks in arena: %d\n",ar_info->num_blocks);
       fprintf(fp,"Start address: 0x%llx\n",ar_info->start_addr);
       fprintf(fp,"Ending address: 0x%llx\n",ar_info->end_addr);
       fprintf(fp,"Total space: 0x%llu\n",ar_info->total_space);

       fprintf(fp,"Number of bytes in free small blocks: %llu\n", 
               ar_info->num_bytes_FSB);
       fprintf(fp,"Number of bytes in used small blocks: %llu\n", 
               ar_info->num_bytes_USB);     
       fprintf(fp,"Number of bytes in free ordinary blocks: %llu\n", 
               ar_info->num_bytes_FOB);
       fprintf(fp,"Number of bytes in used ordinary blocks: %llu\n", 
               ar_info->num_bytes_UOB);
       fprintf(fp,"Number of bytes in holding block header: %llu\n", 
               ar_info->num_bytes_HBH);
       fprintf(fp,"Number of small blocks: %llu\n",ar_info->num_small_blocks);
       fprintf(fp,"Number of used ordinary blocks: %llu\n",
               ar_info->num_ordinary_blocks);
       fprintf(fp,"Number of holding blocks: %llu\n",ar_info->num_holding_blocks);
       fprintf(fp,"Number of free ordinary blocks: %llu\n", 
               ar_info->num_free_ordinary_blocks);
       fprintf(fp,"Number of free small blocks: %llu\n", 
               ar_info->num_free_small_blocks);

    }
  else 
    {
       printf_filtered("\n");
       printf_filtered("Total number of blocks in arena: %d\n",
                       ar_info->num_blocks);
       printf_filtered("Start address: 0x%llx\n",ar_info->start_addr);
       printf_filtered("Ending address: 0x%llx\n",ar_info->end_addr);
       printf_filtered("Total space: %llu\n",ar_info->total_space);

       printf_filtered("Number of bytes in free small blocks: %llu\n", 
                       ar_info->num_bytes_FSB);
       printf_filtered("Number of bytes in used small blocks: %llu\n", 
                       ar_info->num_bytes_USB);     
       printf_filtered("Number of bytes in free ordinary blocks: %llu\n", 
                       ar_info->num_bytes_FOB);
       printf_filtered("Number of bytes in used ordinary blocks: %llu\n", 
                       ar_info->num_bytes_UOB);
       printf_filtered("Number of bytes in holding block header: %llu\n", 
                       ar_info->num_bytes_HBH);
       printf_filtered("Number of small blocks: %llu\n",ar_info->num_small_blocks);
       printf_filtered("Number of used ordinary blocks: %llu\n",
                       ar_info->num_ordinary_blocks);
       printf_filtered("Number of holding blocks: %llu\n",ar_info->num_holding_blocks);
       printf_filtered("Number of free ordinary blocks: %llu\n", 
                       ar_info->num_free_ordinary_blocks);
       printf_filtered("Number of free small blocks: %llu\n", 
                       ar_info->num_free_small_blocks);

    }  /* else */

}


/*
 * Print stack trace.  Call this for info heap arena # blocks # 
 * If block # exists in our chunk_info print stack trace. 
 */ 

static void 
print_virtual_addr (struct arena_info *ar_info, int addr_idx, int idx, 
                    FILE *fp, gdb_chunk_info * c_i, boolean prt_stack)
{

  int i;
  int free_function = 0;
  CORE_ADDR cur_addr;

  if (fp) 
    {
      fprintf(fp,"Virtual address %d: 0x%llx, ", 
              addr_idx, ar_info->block_list[idx].addr);
      fprintf(fp,"Block Type: %s,\t",
              heap_type_string[ar_info->block_list[idx].type]);
      fprintf(fp,"Size: %llu\n", ar_info->block_list[idx].size); 
    }
  else 
    {
      printf_filtered("Virtual address %d: 0x%llx, ", 
                      addr_idx, ar_info->block_list[idx].addr);
      printf_filtered("Block Type: %s,\t",
                      heap_type_string[ar_info->block_list[idx].type]);
      printf_filtered("Size: %llu\n", ar_info->block_list[idx].size); 
    }

  if (!prt_stack) return; 

  /* Suresh, Jan 08: For handling pc_tuples, the easiest way is to call
     the below generic routinue to print the stack trace. Not too sure
     why 'info heap arena block (11.31 only) used extract_address() to
     print the stack trace ?

     We need to change this a lot to handle the 2 tuple pc array, instead
     removed all the earlier code and calling the generic routinue
     print_stack_frame_variant1() that should do the job
  */
  print_rtc_stack_frame_variant1 ((struct gdb_pc_tuple *)c_i->pc_tuple, frame_count, fp);

}  /* print_stack */



/*
 * info heap arena # blocks.  If block_list_size=100 and total
 * number of blocks=500, there will be 5 calls to libc interface.
 * sum them all up here.
 */ 

static void 
save_ar_info (struct arena_info *sum_ar_info, struct arena_info *ar_info)
{
  sum_ar_info->start_addr = ar_info->start_addr;
  sum_ar_info->end_addr = ar_info->end_addr ;
  sum_ar_info->num_blocks += ar_info->num_blocks;
  sum_ar_info->total_space += ar_info->total_space ;
  sum_ar_info->num_bytes_FSB += ar_info->num_bytes_FSB ;
  sum_ar_info->num_bytes_USB += ar_info->num_bytes_USB ;
  sum_ar_info->num_bytes_FOB += ar_info->num_bytes_FOB ;
  sum_ar_info->num_bytes_UOB += ar_info->num_bytes_UOB ;
  sum_ar_info->num_bytes_HBH += ar_info->num_bytes_HBH ;
  sum_ar_info->num_small_blocks += ar_info->num_small_blocks ;
  sum_ar_info->num_ordinary_blocks += ar_info->num_ordinary_blocks ;
  sum_ar_info->num_holding_blocks += ar_info->num_holding_blocks ;
  sum_ar_info->num_free_ordinary_blocks += ar_info->num_free_ordinary_blocks ;
  sum_ar_info->num_free_small_blocks += ar_info->num_free_small_blocks;
}



/*
 * libc_get_arena returns a block_list which contains addresses.
 * take each address and search through our chunk_info.  If found
 * print the stack info for that address.  
 */

static void
search_chunk_info(FILE *fp, int virtual_idx, int actual_idx, 
                  struct arena_info *ar_info)
{
  long page_no;
  boolean done = false; 
  CORE_ADDR addr;  
  gdb_chunk_info * c_i;  /* RTC commonly used name for chunk_info */    

  /* 
   * Must always add rtc_header_size to the address in block_list 
   * returned from libc becuase of the padding.  __rtc_malloc
   * adds rtc_header_size & rtc_footer_size padding before calling 
   * malloc.  It records the base addr without the header
   * padding.  libc malloc would have no way of knowing 
   * our padding business.  
   */
  addr = ar_info->block_list[actual_idx].addr+rtc_header_size;

  /* 
   * before printing search through chunk_info to get 
   * stack trace 
   */

  if (chunks_n_page)
    {
      page_no = PAGE_FROM_POINTER (addr);
      c_i = chunks_n_page[page_no];
      while (c_i)
        {
          /*
           * libc took some pains to return in block_list.addr what 
           * malloc would have returned to the application
           */ 
          if (addr == c_i->base)
            {
              print_virtual_addr (ar_info, virtual_idx, actual_idx, fp, c_i, true);
              done = true;
              break;                      /* get out of while */
            } 

            c_i = (gdb_chunk_info *) (unsigned long) c_i->next;
        }  /* while chunk_info list */

    }  /* if chunks_n_page */

  /* 
   * addr returned from libc won't always be in gdb chunk_list.
   * it will be if ordinary block(HEAP_USED_BLK)
   * it won't be if it's a small block.  In any case even if
   * it's not in the list to print stack trace, print the 
   * virtual address here. 
   */
  if (!done)
    print_virtual_addr (ar_info, virtual_idx, actual_idx, fp, NULL, false);
}


/*
 * for an arena_num, call libc_get_arena to read in arena
 * info and block_list. 
 *
 */
static void 
read_arena_info(FILE *fp, int arena_num, void **arena_handle)
{
  void *data = NULL;     /* ptr to send to libc. could be total_arena or 
                          * arena_info
                          */
  struct arena_info ar_info;
  struct arena_info sum_ar_info;      /* for AR_INFO request, the end of each 
                                       * call to libc, libc returns stat
                                       * about # small blk, # used blk etc...
                                       * This is only relevant for one 
                                       * iteration.  introduce sum to add
                                       * them all up to present to user
                                       */  
  boolean done = false;    /* use for various purposes to exit out of loops */
  int status = 0;   

  memset(&sum_ar_info, 0, sizeof(struct arena_info));
  ar_info.block_list = calloc (BLK_LIST_SIZE,sizeof(struct heap_block)); 
  while (!done)
    {
      memset(ar_info.block_list, 0, BLK_LIST_SIZE);
      data = (void *) &ar_info; 
      status = libc_get_arena_hdl(arena_num, AR_ARENA_INFO, data, 
                                  BLK_LIST_SIZE, arena_handle);
      error_text = "Unable to request for info heap arena"; 
      CHECK_ERROR_CONDITION(status, error_text);
      save_ar_info (&sum_ar_info, &ar_info); 
 
      if (ar_info.num_blocks != BLK_LIST_SIZE && 
          ar_info.block_list[ar_info.num_blocks].addr == 0)
        done = true;
         

    }  /* while !done */

  free (ar_info.block_list); 

  if (fp)
     fprintf(fp, "\nArena ID: %d\n", arena_num);
  else
     printf_filtered("\nArena ID: %d\n", arena_num);

  print_arena_mem (&sum_ar_info, fp);

}


/*
 * case 7) info heap arena [0|1|...] block [0|1|..] [filename]
 *  - call libc routine to get specified block and arena info
 *  _ call read_all_chunks to read chunk_info from the inferior
 *    if the block virtual address matches with chunk_info->base
 *    address print the stack info (chunk_info->pc_tuple)
 *
 *    get stack trace if block address matches chunk_info->base 
 *    retrieve all chunk_info using read_all_chunks.  This command
 *    and info corruption of corefile call read_all_chunks.  
 *    Expect block_list to be longer so for each
 *    address in block_list search through chunk_info list
 */

static void 
do_arena_block_num (FILE *fp, int arena_n, int blk_num, void **arena_handle)
{
  int status = -1;       /* return status from libc calls */
  void *data = NULL;     /* ptr to send to libc. could be total_arena or 
                          * arena_info
                          */
  int i = 0;             /* index */   

  gdb_chunk_info * c_i;     /* chunk_info to compare address for
                             * stack trace 
                             */ 
  struct total_arena_info total_ar;
  struct arena_info ar_info;
  struct arena_info sum_ar_info; 
  int adjustBlk_num;
  int numIteration;   /* number of times need to call libc_get_arena
                       * before getting to the list for the blk_num
                       * suppose user enters blocks 102.  BLK_LIST_SIZE
                       * is 50.  numIteration=3.  adjustBlk_num=2.
                       */  

  memset(&sum_ar_info, 0, sizeof(struct arena_info));


  /* 
   * If this is a live proc we expect rtc_do_sl and check_heap_in_this_run. 
   * If this is a corefile, check_heap_in_this_run and rtc_dot_sl don't
   * need to be set.  In read_all_chunks, we have a way to detect if the 
   * corefile was linked properly
   */
  if ((target_has_execution || !target_has_stack) && !rtc_dot_sl)
    error ("This command requires heap debugging library (librtc) to be linked in !\nHint: Use environment variable LIBRTC_SERVER to pick correct version of librtc and upgrade to the latest linker version (greater than B.11.19);  Or link in librtc with your application to enable gdb support for heap debugging.");

  if ((target_has_execution || !target_has_stack) && !check_heap_in_this_run)  
    error ("This command requires set heap-check on");

  printf_filtered ("Executing heap arena command....\n\n"); 
  read_all_chunks ("ARENA_INFO", 0);

  if (fp)
    fprintf (fp, "Arena ID: %d\n", arena_n);
  else
    printf_filtered ("Arena ID: %d\n", arena_n);

  if (blk_num >= BLK_LIST_SIZE)
    {
      adjustBlk_num = (blk_num % BLK_LIST_SIZE);
      numIteration = (blk_num / BLK_LIST_SIZE)+1;
    }
  else
    { 
      adjustBlk_num = blk_num; 
      numIteration = 1;    /* must read at least one */
    }

  ar_info.block_list = calloc (BLK_LIST_SIZE, sizeof(struct heap_block)); 
  for (i=0; i < numIteration; i++)
    {
      memset(ar_info.block_list, 0, BLK_LIST_SIZE);
      data = (void *) &ar_info; 
      status = libc_get_arena_hdl (arena_n, AR_ARENA_INFO, data, 
                                   BLK_LIST_SIZE, arena_handle);

      error_text = "Unable to request for info heap arena # block # (1)";   
      CHECK_ERROR_CONDITION(status, error_text);
      save_ar_info (&sum_ar_info, &ar_info); 
    }  /* for */            

  if (blk_num > sum_ar_info.num_blocks)
    error ("Block %d is out of range", blk_num);

  /* the last iteration should contain block we're interested in */
  if (ar_info.block_list && ar_info.block_list[adjustBlk_num].addr )
    search_chunk_info(fp, blk_num, adjustBlk_num, &ar_info);
  else 
    error ("invalid block number for info heap arena # block #");

  /* continue reading the rest for summary purposes */
  while (ar_info.num_blocks == BLK_LIST_SIZE ||
         ar_info.block_list[ar_info.num_blocks].addr != 0)
    {
       memset(ar_info.block_list, 0, BLK_LIST_SIZE);
       data = (void *) &ar_info; 
       status = libc_get_arena_hdl (arena_n, AR_ARENA_INFO, data, 
                                    BLK_LIST_SIZE, arena_handle);
       error_text = "Unable to request for info heap arena # block # (2)";    
       CHECK_ERROR_CONDITION(status, error_text);
       save_ar_info (&sum_ar_info, &ar_info); 

    }  /* while searching through block_list */

  print_arena_mem (&sum_ar_info, fp); 
  free (ar_info.block_list);
  free_all_chunks(); 

}


/*
 * case 6) info heap arena [0|1|..] blocks [filename]
 *  - call libc routine to get arena and blocks info only for the
 *    specified arena number
 *
 * case 8) info heap arena [0|1|...] block stack [filename]
 *  - similar to case 7 but print stack info for all blocks
 *    not just specified block 
 */  
static void
do_arena_blocks (FILE *fp, int request_type, int arena_n, int blk_num, 
                 void **arena_handle)
{

  int status = -1;       /* return status from libc calls */
  boolean done = false;  /* use for various purposes to exit out of loops */

  int virtual_idx = 0;   /* use for printing.  suppose there are 152 blocks
                          * and BLK_LIST_SIZE=50 there will be 4 calls to 
                          * libc_get_arena_info.  index i will start at 0 
                          * increment to 49 (BLK_LIST_SIZE-1) for every call.  
                          * virtual_idx starts at zero and keep incr for 
                          * subsequent  calls
                          */ 
  void *data = NULL;     /* ptr to send to libc. could be total_arena or 
                          * arena_info
                          */
  int i = 0;             /* index */   

  gdb_chunk_info * c_i;  /* chunk_info to compare address for
                          * stack trace 
                          */ 
  struct total_arena_info total_ar;
  struct arena_info ar_info;
  struct arena_info sum_ar_info;      /* for AR_INFO request, the end of each 
                                       * call to libc, libc returns stat
                                       * about # small blk, # used blk etc...
                                       * This is only relevant for one 
                                       * iteration.  introduce sum to add
                                       * them all up to present to user
                                       */  

  memset(&sum_ar_info, 0, sizeof(struct arena_info));

  /*
   * if the request is BLOCK_STACK_INFO (users wants to see
   * stack info) + this is a live process + linked with RTC
   * then call read_all_chunks to get c_i for stack printing.
   * Otherwise use libc interface and provide to user just
   * the virtual address info but no stacks info
   * For BLOCK_INFO request, user doesn't care about stacks
   * so don't need to call read_all_chunks.
   */ 
  if ( (request_type == BLOCK_STACK_INFO) &&
       (target_has_execution || !target_has_stack) && 
        check_heap_in_this_run && 
        rtc_dot_sl 
     )
    {
      /* for live process, read_all_chunks does take time
       * so add this print to let user know we're doing something
       */
      printf_filtered ("Executing heap arena command....\n\n"); 
      read_all_chunks ("ARENA_INFO", 0);
    }

  if (fp)
    fprintf(fp, "Arena ID: %d\n", arena_n);
  else
    printf_filtered("Arena ID: %d\n", arena_n);

  virtual_idx = 0;
  done = false;  /* start out fresh */

  ar_info.block_list = calloc (BLK_LIST_SIZE, sizeof (struct heap_block)); 
  while (!done)
    {
      memset(ar_info.block_list, 0, BLK_LIST_SIZE);
      data = (void *) &ar_info; 
      status = libc_get_arena_hdl (arena_n, AR_ARENA_INFO, data, 
                                   BLK_LIST_SIZE, arena_handle);

      error_text = "Unable to request for info heap arena blocks"; 
      CHECK_ERROR_CONDITION(status, error_text);
      save_ar_info(&sum_ar_info, &ar_info); 

      i = 0;
      /* traverse while i < BLK_LIST_SIZE and addr !is 0 */
      while (i < BLK_LIST_SIZE && ar_info.block_list[i].addr != 0)
        {
          if (request_type == BLOCK_STACK_INFO)
            search_chunk_info(fp, virtual_idx, i, &ar_info);
          else
            print_virtual_addr (&ar_info, virtual_idx, i, fp, NULL, 0); 

          i++;
          virtual_idx++; 
        } /* while */

      if (ar_info.num_blocks != BLK_LIST_SIZE && 
          ar_info.block_list[ar_info.num_blocks].addr == 0)
        done = true; 
 

    }  /* while searching through block_list */

  free (ar_info.block_list);
  print_arena_mem (&sum_ar_info, fp); 

  if ( (request_type == BLOCK_STACK_INFO) &&
       (target_has_execution || !target_has_stack) && 
        check_heap_in_this_run && 
        rtc_dot_sl 
     )
    free_all_chunks();

}



/* 
 * main implementation of info heap arena commands 
 * 
 * case 3) info heap process [filename]
 *  - call libc routine to get info, print out info for the process
 *
 * case 4) info heap arena[s] [filename]
 *  - call libc routine to get arena info, display information
 *
 * case 5) info heap arena [0|1|2..] [filename]
 *  - call libc routine to get arena info only for specified arena,
 *
 * case 6) info heap arena [0|1|..] blocks [filename]
 *  - call libc routine to get arena and blocks info only for the
 *    specified arena number
 *
 * case 7) info heap arena [0|1|...] block [0|1|..] [filename]
 *  - call libc routine to get specified block and arena info
 *  _ call read_all_chunks to read chunk_info from the inferior
 *    if the block virtual address matches with chunk_info->base
 *    address print the stack info (chunk_info->pc_tuple)
 * 
 * case 8) info heap arena [0|1|...] block stack [filename]
 *  - similar to case 7 but print stack info for all blocks
 *    not just specified block 
 *  
 * Basically for each case call libc_get_arena_handle with 
 * appropriate request.   * call print_* to display the information 
 * libc sends back. 
 */

static void 
do_arena (int request_type, int arena_n, int blk_num, char *filename)
{
  int status = -1;       /* return status from libc calls */
  boolean done = false;  /* use for various purposes to exit out of loops */
  int virtual_idx = 0;   /* use for printing.  suppose there are 152 blocks
                          * and BLK_LIST_SIZE=50 there will be 4 calls to 
                          * libc_get_arena_info.  index i will start at 0 
                          * increment to 49 (BLK_LIST_SIZE-1) for every call.  
                          * virtual_idx starts at zero and keep incr for 
                          * subsequent  calls
                          */ 
  void *data = NULL;     /* ptr to send to libc. could be total_arena or 
                          * arena_info
                          */
  int i = 0;             /* index */   
  struct arena_params ar_params;
  struct total_arena_info total_ar;
  struct arena_info ar_info;
  struct arena_info sum_ar_info;   /* for AR_INFO request, the end of each 
                                    * call to libc, libc returns stat
                                    * about # small blk, # used blk etc...
                                    * This is only relevant for one 
                                    * iteration.  introduce sum to add
                                    * them all up to present to user
                                    */  
  void ** arena_handle;      /* for libc use,  they request us to allocate */ 
  FILE *fp = NULL;           /* absolutely must be NULL if no file specified */

  if (filename)
    {
      int fd;
      unlink (filename);
      fd = open (filename, O_RDWR | O_EXCL | O_CREAT, 0777);
      if (fd == -1 || (fp = fdopen (fd, "w")) == NULL)
        error("Unable to open %s", filename); 
    } 

  /* JAGag09507 get a handle to an important libc routine */ 
  if (!libc_get_arena_hdl)
    {
      struct shl_descriptor *desc;
      int index = 1;
      int shl_stat=0;
      while (shl_get (index, &desc) != -1)
       {
         index++;
         if (strstr (desc->filename, "/libc."))
           {
             shl_stat = shl_findsym (&desc->handle, "libc_get_arena_info", 
                                     TYPE_PROCEDURE, &libc_get_arena_hdl);

             /*
              * to use info heap process, info heap arena... commands
              * version of libc must have libc_get_arena_info interface.
              * If there's an error do nm on the libc version it's looking
              * at below to see if libc_get_arena_info is there. 
              */
             if (shl_stat != 0)
               {
                 printf_filtered("This command is not supported with this version of libc: %s\n", desc->filename);
                 return;  
               } /* check shl_stat */
           }     /* if libc found */
       }         /* while shl_get */  
    }            /* !libc_get_arena_hdl */


  /* alloc arena handle */
  arena_handle = (void **) malloc (sizeof(void **));

  status = libc_get_arena_hdl (0, AR_ARENA_OPEN, debug_callbacks, 
                               0, arena_handle);

  error_text = "Unable to request for opening of arena handle";
  CHECK_ERROR_CONDITION(status, error_text);
  switch (request_type)
  {

    /* case 3) info heap process: fill up total_arena_info */
    case PROCESS_INFO:
      data = (void *) &total_ar;
      status = libc_get_arena_hdl (0, AR_PROC_LEVEL, data, 0, arena_handle);
      error_text = "Unable to request for info heap process";  
      CHECK_ERROR_CONDITION(status, error_text);
      print_process_mem(&total_ar, fp);
      break;


    /*
     * case 4) info heap arenas: 
     *    request AR_ARENA_PARAMS to fill up arena_params
     *  
     *    request AR_ARENA_INFO to fill up arena_info for each 
     *    arena_num from arena_params
     *    block info is not needed.  pass in 0 for num_blks
     */
    case ARENA_INFO:
      data = (void *) &ar_params;
      status = libc_get_arena_hdl (0, AR_ARENA_PARAMS, data, 0, arena_handle);
      error_text = "Unable to request for info heap arenas";
      CHECK_ERROR_CONDITION(status, error_text);

      if (fp)
        {
          fprintf(fp, "num_arenas: %d\n", ar_params.num_arenas);
          fprintf(fp, "expansion: %llu\n",  ar_params.expansion_factor);
        }
      else
        {  
          printf_filtered("num_arenas: %d\n", ar_params.num_arenas);
          printf_filtered("expansion: %llu\n",  ar_params.expansion_factor);
        }

      /* arena starts at 0 */
      for (i=0; i < ar_params.num_arenas; i++)
        {
          read_arena_info(fp, i, arena_handle); 
        }  /* for num_arenas */ 

      break;


    /*
     * case 5) info heap arena 6:
     *    arena_info for arena 6, conceptually block list is 
     *    not needed here however if call libc_get_arena_info
     *    with block_list==null malloc.c will return ERROR
     *    so just allocate it.
     */
    case ARENA_NUM_INFO:
      read_arena_info(fp, arena_n, arena_handle);    
      break;

    /* case 6) info heap arena 8 block 3 */
    case BLOCK_NUM_INFO:
      do_arena_block_num (fp, arena_n, blk_num, arena_handle);   
      break;

    /* case 6 & 8) print stack info for all virtual address (if available) */
    case BLOCK_STACK_INFO:
    case BLOCK_INFO:
      do_arena_blocks (fp, request_type, arena_n, blk_num, arena_handle);
      break;

  } /* switch */

  if (fp) 
    fclose(fp);

  status = libc_get_arena_hdl (0, AR_ARENA_CLOSE, NULL, 0, arena_handle);
  error_text = "Unable to request for closing of arena handle";
  CHECK_ERROR_CONDITION(status, error_text);
}


/*
 * case 1) info heap
 *  - old command to display heap info
 *
 * case 2) info heap #
 *  _ old command to display heap/stack info for block #
 *
 * case 3) info heap process [filename]
 *  - call libc routine to get info, print out info for the process
 *
 * case 4) info heap arena[s] [filename]
 *  - call libc routine to get arena info, display information
 *
 * case 5) info heap arena [0|1|2..] [filename]
 *  - call libc routine to get arena info only for specified arena,
 *
 * case 6) info heap arena [0|1|..] blocks [filename]
 *  - call libc routine to get arena and blocks info only for the
 *    specified arena number
 *
 * case 7) info heap arena [0|1|...] block [0|1|..] [filename]
 *  - call libc routine to get specified block and arena info
 *  _ call read_all_chunks to read chunk_info from the inferior
 *    if the block virtual address matches with chunk_info->base
 *    address print the stack info (chunk_info->pc_tuple)
 * 
 * case 8) info heap arena [0|1|...] block stack [filename]
 *  - similar to case 7 but print stack info for all blocks
 *    not just specified block 
 *  
 */

static void
heap_info_command_common (char *blocknum_exp, int from_tty, int type)
{
  value_ptr funcval, val;
  FILE * fp = 0;
  int block_num = -1;
  int i;
  struct cleanup * old_chain = 0; /* initialized for cmpiler warning */
  int thread_state;

  char *token[20] = {0};  /* for parsing info heap commands 
                           * token[0]=arena,process,filename 
                           * token[1]=arena number 
                           * token[2]=block[s] 
                           * token[3]=block num 
                           * declare to be 20 so user can accidently
                           * type up to 20 tokens.  
                           */
  char *lower_block_exp;  /* do lower case the entire blocknum_exp */

  int arena_num=0;        /* info heap arena 8, arena_num=8 */
  int toke_idx=0;         /* index for reading tokens in */
  char *filename;         /* generous size for filename */
  int subrequest_type=type; /* JAGaf87040-add support for info heap
                             * high-mem. Initially sets subrequest_type
                             * to incoming type.  If further parsing 
                             * results in high-mem then change the 
                             * subrequest.  This is important for 
                             * find_function, down_load_data and 
                             * print_blocks
                             */


  /* case 1) info heap */
  if (!blocknum_exp) 
    {
      subrequest_type = type;
      goto start_heap;
    }

  /* case 2) info heap 80 */
  if (could_be_number (blocknum_exp))
    {
       block_num = atoi (blocknum_exp);

       if (block_num < 0 || block_num >= block_count)
         error ("Bad block number.\n");
       else  
         report_this_block (block_num, 0, type);  /* always to the screen */

       return;
    }

  /* do tolower for the whole string */
  lower_block_exp=convert_lower(blocknum_exp);

  i=0;
  token[0] = strtok(lower_block_exp," ");
  while (token[++toke_idx]=strtok(NULL, " "))

  /* case 8) trailing junk  */
  if (toke_idx > 5 )
    {
      error("Invalid heap command.  Unexpected token at the end\n");
    }

  /* case 4-7, parse arena command */
  if (!strcmp(token[0], "arena") ||
      !strcmp(token[0],"arenas") )
    {
      if (token[1] && could_be_number(token[1])) 
        {  
          /* 
           * first part of case 5-7
           * atoi returns 0 for an invalid string.  0 is a valid
           * number in this case so must check for a valid number first
           */
          /* get arena_num  but can not call do_arena yet  
           * because we might still have block to parse 
           */
          arena_num=atoi(token[1]);
        }
      else
        {
          /* case: 4
             enter here either token[1] is NULL or 
             it's not a number.  If it's NULL then
             the cmd=info heap arena[s].  If it's not
             NULL and not a number then token[1] is 
             a filename.  cmd=info heap arena filename
           */
          do_arena(ARENA_INFO, -1, -1, token[1]);
          return; 
        }
      

      /* case 6-7 */
      if ( token[2] && (!strcmp(token[2],"blocks") || 
                        !strcmp(token[2],"block")))
        {  
          /* case 7 */
          if (token[3] && could_be_number(token[3])) 
            {
              block_num=atoi(token[3]);

              if (block_num < 0)
                error ("Negative block number is invalid");

              if (toke_idx == 5)
                do_arena(BLOCK_NUM_INFO,arena_num,block_num, token[4]);
              else
                do_arena(BLOCK_NUM_INFO,arena_num,block_num, NULL);
              return;
            }
          /* case 8 */
          if (token[3] && (!strcmp(token[3],"stacks") || 
                           !strcmp(token[3],"stack")))
            {
              do_arena(BLOCK_STACK_INFO,arena_num,-1,token[4]);
              return; 
            }
          else
           {
              /* case 6: enter here if token[3] is NULL or not
               * a number.  If NULL cmd: info heap arena 7 blocks.
               * if not a number cmd: info heap arena 7 blocks filename
               * token[3] contains the filename
               * arena_num has been set.  block_num is not used here. 
               */ 
               do_arena(BLOCK_INFO,arena_num,-1,token[3]);
               return;  
            }
        }   /* if blocks */
      else 
        {
          /*
           * case: 5
           * Enter here if token[2] is NULL cmd: info heap arena 7
           * or token[2] is not blocks cmd: info heap arena 7 filename
           * arena_num has been set.  block_num is not needed 
           */ 
           do_arena(ARENA_NUM_INFO, arena_num, -1, token[2]); 
           return;
        }
    } /* if arenas */


  /* at this point it should be info heap filename/process 
   * and nothing else
   */

  if (toke_idx>2)
    {
      error("Invalid info heap process command\n");
    }

  if (!strcmp(token[0],"process"))
    {
       do_arena(PROCESS_INFO,-1,-1, token[1]); 
       return;
    }

  /* JAGaf87040 - there are 2 usage of this cmd 
   * info heap high-mem to emit the high water mark number
   * set heap-check high-mem-count <num> - to stop when the HWM number 
   * has been reached
   */
  if (!strcmp(token[0], "high-mem"))
    {
      subrequest_type = HIGH_MEM_INFO; 
      goto start_heap;
    }

  if (could_be_filename (blocknum_exp))
    {
       /* treat it as a filename ... */
       fp = open_log_file (blocknum_exp);
       if (!fp)
         error ("Cannot open file %s\n", blocknum_exp);
     
       if (fp != stderr)
         old_chain = make_cleanup ((make_cleanup_ftype *) fclose, fp);

    }  /* filename */


start_heap:

    /*
     * for non info heap process/arena commands require apps to be linked 
     * with librtc.  
     */
     if (!check_heap_in_this_run)
       error ("Heap analysis is not enabled now.");

     /* On PA64 and IPF, it is possible that we get here without the
        necessary libs */ 
     if (!rtc_dot_sl)
       error ("The heap debugging library (librtc) is not linked in !\nHint: Use environment variable LIBRTC_SERVER to pick correct version of librtc and upgrade to the latest linker version (greater than B.11.19);  Or link in librtc with your application to enable gdb support for heap debugging.");

     if (!libc_dot_sl)
       error ("The shared C library is not linked in !");

#ifdef LOG_BETA_RTC_USED
  log_test_event (LOG_BETA_RTC_USED, 1);
#endif

  dont_repeat ();


  if (!target_has_stack)
    {
      if (block_count)
        print_blocks (block_info, fp, type);
      else 
        error ("There is no running process.");
      goto cleanup;
    }

#ifdef HAS_END_DOT_O
  /* JAGae05770: the comment below is no longer valid. Now, we try to handle
     signals */
  /* Making *any* command line call after a Ctrl-C confuses end.o so
     thoroughly, it sigsegvs (only on 11.00.) Even when we don't pass
     the signal. No idea why. Just bail out  -- srikanth, 000311.
  */ 
  if (stop_signal && stop_signal != TARGET_SIGNAL_TRAP) {
    struct target_waitstatus sw;
    char target_was_single_stepped = 1;
    fprintf (stderr,"There are pending signals.");
      
    if(stop_signal == TARGET_SIGNAL_INT) {
      fprintf (stderr," Single Stepping now.\n");
      target_resume(-1, 1, TARGET_SIGNAL_0);
    }
    /*Block some signals which are not commonly handled by the user apps. This
      list needs to be updated based on need
     */
    else if(stop_signal != TARGET_SIGNAL_BUS && 
            stop_signal != TARGET_SIGNAL_SEGV && 
            stop_signal != TARGET_SIGNAL_ILL && 
            stop_signal != TARGET_SIGNAL_QUIT) {
      fprintf (stderr," Single Stepping now.\n");
      target_resume(-1, 1, stop_signal);
    }
    else { 
      fprintf (stderr,"\n");
      target_was_single_stepped = 0;
    }
    stop_signal = 0;
    if(target_was_single_stepped) {
      registers_changed ();
      target_wait (inferior_pid, &sw);
    }
  }
#endif /* HAS_END_DOT_O */

#ifndef HPUX_1020

  /* srikanth, 000928, we used to refuse to oblige if there are pending
     events for other threads. With the K threads overhaul, this
     is no longer necessary. The only condition is that the current
     thread should not have unhandled event. This is checked for
     by thread_could_run_gc() below.
  */ 

  thread_state = thread_could_run_gc (inferior_pid);

  switch (thread_state) {

    case TTS_WASSUSPENDED : 
      error ("Current thread is suspended. Cannot profile now.");
    case TTS_WASSLEEPING :
      error ("Current thread is blocked. Cannot profile now.");
    case TTS_INSYSCALL :
      error ("Current thread is inside a syscall. Cannot profile now.");
    case TTS_ATEXIT :
      /*After the above signal handling stuff (JAGae05770), gdb hangs in certain
        situations if the target is not explicitly killed */
      target_kill();
      error ("Current thread is about to terminate. Cannot profile now.");
    case 0 :
      error ("Current thread has pending events. Cannot profile now.");
  }
  
#endif

  /* release any old data .. */
  for (i=0; i < block_count; i++)
    free (block_info[i].pc_tuple);

  free (block_info);
  block_info = 0;
  block_count = 0;
  total_blocks = 0;
  total_bytes = 0;

  if (!batch_rtc)
      printf_filtered ("Analyzing heap ...\n\n");


  /* JAGaf87040 */
  if (subrequest_type == HIGH_MEM_INFO)
    funcval = find_function_in_inferior ("__rtc_high_mem_info");
  else
    funcval = find_function_in_inferior ("__rtc_heap_info");

  rtc_in_progress = 1;
  val = call_function_by_hand (funcval, 0, 0);
  rtc_in_progress = 0;

  block_count = (int) value_as_long (val);
  
  if (block_count == RTC_NOT_RUNNING)
    error ("Heap debugging has not commenced or has been disabled.\n");
  else 
  if (block_count == RTC_MUTEX_LOCK_FAILED)
    {
      /* Aquiring mutex failed because some other thread had
         acquired the same mutex. It is a good idea to give the heap or corruption 
         outputs rather than bailing out even if the mutex is locked.
         The idea is to catch the thread which is about to unlock the mutex 
         and call the "info heap" or info corruption" commands again. 
         since we can be sure that the data structures are sane at the unlock time. 
         This can be extended to leaks and other commands as well.
      */
      if (subrequest_type == 1 || subrequest_type == 2
          && !batch_rtc)
        {
          struct minimal_symbol * m;
          CORE_ADDR rtc_mutex_unlock_addr = 0;
          set_inferior_var_value ("failed_to_acquire_mutex", 1);

          /* Warn the user that there might be some slight differences. */
          warning ("Some other thread has locked the mutex. Attempting to move to safe place.");
          warning ("The application execution might be stopped at a slightly different point as"); 
          warning ("compared to the point at which info heap/corruption was issued.\n");
          /* Continue the threads */
          proceed ((CORE_ADDR)(long) - 1, TARGET_SIGNAL_0, 0);
          goto cleanup;
        }
      else
        error ("Some other thread has locked the mutex. Try again later.\n");
    }
  else 
  if (block_count == RTC_UNSAFE_NOW)
    error ("Current thread is inside the allocator. Try again later.\n");
  else
  if (block_count == RTC_FOPEN_FAILED)
    error ("Failed to open temporary file.\n");
  else 
  if (block_count == RTC_FCLOSE_FAILED)
    error ("Failed to close temporary file.\n");
  
  if (block_count > 0)
    {
      down_load_data (subrequest_type);
      if (block_count == 0)   /* down_load_data changes block_count with min_heap_size set */
        error ("No blocks were found.\n");
      print_blocks (block_info, fp, subrequest_type);
    }  
  else   /* do not leave 0 byte output file hanging around ... */
    {
      char heap_info_filename[100];
      int real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
					PIDGET(inferior_pid) :
					get_pid_for(inferior_pid));

      sprintf (heap_info_filename, "/tmp/__heap_info.%u", real_inferior_pid);
      unlink (heap_info_filename);
      if (!batch_rtc)
        error ("No blocks were found.\n");
    }

cleanup :


  if (fp && fp != stderr)
    {
      int empty_file = (ftell(fp) == 0l);
      if (fclose (fp) == -1)
        error ("Error writing to file\n");
      if (empty_file && blocknum_exp && batch_rtc) 
        {
           /* remove the empty file in batch rtc mode */
           unlink(blocknum_exp);
        }
      else
        {
          /* JAGag40239 - remind the user that there is 
             a heap file on the directory */
          if (blocknum_exp[0] != '+')
            printf_filtered ("Memory heap info is written to \"%s\".\n", 
                             blocknum_exp);
          else
            printf_filtered ("Memory heap info is appended to \"%s\".\n", 
                             blocknum_exp+1);
        }

      if (old_chain)
        discard_cleanups (old_chain);
    }
}



/* 
 * This function is equivalent to _rtc_heap_info in infrtc.c
 * It's part of info corruption implementation. 
 * This routine writes to /tmp/__heap_info.<inferior_pid> file so we can
 * reuse down_load_data and print_blocks.  If performance
 * is a problem then we need to save to block_info directly
 */
/* Suresh, Jan 08: TODO - Need to completely fix this for pc_tuple..*/
static void 
process_c_i(gdb_chunk_info *c_i, int fd)
{
   /* same usage as in _rtc_heap_info */
   int corrupted = 0;
   CORE_ADDR size;
   CORE_ADDR zero = 0;
   int libindex_zero = 0;

   int j, status;            /* for index and status */

   char buf[10];             /* local buf for target_read */

   CORE_ADDR tmp_adr;        /* address of c_i->base minus the header area 
                              * when malloc_padded allocates memory, it 
                              * adds in the padding but returns a pointer
                              * pointing after the header.  This pointer
                              * is stored in the base of each chunk_info
                              * when restoring the base here, header needs
                              * to be read in as well.
                              */ 

   int adjustSize;           /* if rtc_header_size is 8 adjustSize is 2
                              * which means MAGIC_COOKIE was stored in
                              * 2 slots of the header area.  In  
                              * malloc_padded, MAGIC_COOKIE was stored 
                              * in a certain way.  We're just trying to
                              * extract it here.  
                              */

   char * tmp_base;          /* use to read in the memory area pointed by
                              * c_i->base including padded area.
                              * declared it like pointer in malloc_padded 
                              */ 

   char *tmp_pc;             /* use to convert c_i->pc_tuple back to char *
                              * to use extract_address 
                              */  

   /* temp variable to extract address to detect header/footer corruption */
   CORE_ADDR head1, head2, foot, curAddr;

   size = c_i->end - c_i->base;
   /* skip if its a c_i representing a freed block */
   if (c_i->padded_block && !c_i->freed_block)
     {
        tmp_adr = c_i->base-rtc_header_size;
        tmp_base = malloc((size_t) (size+rtc_header_size+rtc_footer_size)); 
        status = target_read_memory (tmp_adr, tmp_base, 
                                     (int) (size+rtc_header_size+rtc_footer_size));

        if (status != 0)
           error("Error reading tmp_base");


        /* 
         * for PA64, IA64 default rtc_header_size is 16.  
         * 1) move the tmp_base pass the head padding that is move 
         *    it to the actual base.
         * 2) for head1 move it back 1 slot before extract_address
         * 3) for head2 move it back another slot 
         */

        tmp_base += rtc_header_size;
        head1 = extract_address (tmp_base-trueSize, trueSize);
        head2 = extract_address (tmp_base-(trueSize*2), trueSize);    
        foot = extract_address (tmp_base+size, rtc_footer_size);

        CORE_ADDR magic_cookie;
        if (trueSize == 4)
          magic_cookie = 0xfeedface;
        else
          magic_cookie = 0xfeedfacefeedfaceLL;

        if (head1 != magic_cookie || head2 != magic_cookie)
          {
             corrupted = RTC_BAD_HEADER;
          }
        else if (foot != (unsigned char) 0xff)
          { 
             corrupted = RTC_BAD_FOOTER;
          }

     }  /* if padded_block */


   /* skip __rtc_initialize */
   if (c_i->preinit_mmap == 1)
      return;

   /* This is used for reporting corrupted blocks only. So, return
      if the current block is not corrupted. */
   if (!corrupted)
      return;

   /*
    * this is mostly similar to _rtc_heap_info except for the pc.
    * down_load_data sorts by the pc.  pc address here is different
    * because pc array is allocated in the debugger 
    * space (read_all_chunks).  The content pc[0], pc[1].. are
    * the same the best thing I can think of is sort base on pc[0] 
    */
   write (fd, &size, sizeof (CORE_ADDR));
   write (fd, &c_i->base, sizeof (CORE_ADDR));
   /* send the pc[0] over */  
   tmp_pc = (char *) (unsigned long) c_i->pc_tuple; 
   curAddr = extract_address (tmp_pc, trueSize); 
   write (fd, &curAddr, sizeof (CORE_ADDR));
   write (fd, &corrupted, sizeof (int));

   if (c_i->pc_tuple)
     {
        tmp_pc = (char *) (unsigned long) c_i->pc_tuple;
        curAddr = extract_address (tmp_pc, sizeof (CORE_ADDR)); 
        /* QXCR1000751009: Write both PC and library index. */
        for (j=0; j < frame_count && curAddr; j++)
          {
             write (fd, &curAddr, sizeof (CORE_ADDR));
             tmp_pc += sizeof (CORE_ADDR);
             // Library index is an integer */
             int lib_index = extract_address (tmp_pc, sizeof (int));
             write (fd, &lib_index, sizeof (int));
             tmp_pc += sizeof (CORE_ADDR);
             curAddr=extract_address (tmp_pc, sizeof (CORE_ADDR)); 
          }

        /* Fill up the remaining by zeros. */
        /* QXCR1000751009: Write both PC and library index. */
        for (; j < frame_count; j++)
          {
            write (fd, &zero, sizeof (CORE_ADDR));
            write (fd, &libindex_zero, sizeof (int));
          }

     } /* if c_i->pc */
   else 
     {
        for (j=0; j < frame_count; j++)
          {
            /* QXCR1000751009: Write both PC and library index. */
            write (fd, &zero, sizeof (CORE_ADDR));
            write (fd, &libindex_zero, sizeof (int));
          }
     }

   /*
    * block_count is for down_load_data and print_block
    * can't increment block_count at the beginning of this
    * function.  Similar to _rtc_heap_info, it doesn't incr
    * count for preinit_mmap blocks 
    */
   block_count++;

}  /* process_c_i */



/*
 *  Free all the chunks allocated in read_all_chunks 
 */ 
static void
free_all_chunks (void)
{
   int i;
   gdb_chunk_info *delete_c_i;
   char *tmp_pc;

   for (i=0; i < gdb_total_pages; i++)
     {
        delete_c_i = chunks_n_page[i];   
        while (delete_c_i)
          {
             if (delete_c_i->pc_tuple)
               {
                  /* convert before delete to avoid warnings 
                   * as explained in print_virtual_address and 
                   * read_all_chunks, pc is really a pointer not
                   * a CORE_ADDR  
                   */

                  tmp_pc = (char *) (unsigned long) delete_c_i->pc_tuple;
                  free (tmp_pc);
               }

             delete_c_i = (gdb_chunk_info *) (unsigned long) delete_c_i->next;
             free (delete_c_i);

          }  /* while */
     }       /* for */

   free (chunks_n_page);

   /* 
    * free won't set it to NULL. 
    */ 
   chunks_n_page = NULL;

} /* free_all_chunks */



/*
 * info corruption for core-file & info heap arena # block # will
 * use this routine.  This routine reads all the link lists of 
 * chunk_info from core-file or live process (for info heap arena)
 * This is written to work for IA/PA 32/64 conbination.  
 * Read everything into char * buffer.  Extract information out
 * carefully based on the size.  
 */ 

/* Suresh, Nov 07: TODO 1: the newly added prog guard bytes don't work for
   corefiles, if the user has modified the header and footer padding 
   size to be different from the default values of 16 and 1. We need to 
   read this from the coredump, before we begin processing for info 
   corruption in  a corefile.

   TODO 2: Also we don't do check for in-place corruptions of freed blocks
   we just skip the c_i if its corresponds to freed blocks.

   TODO 3: Need to fix the reading of 2 member pc tuples from core files

*/

static void 
read_all_chunks (char *request_type, int fd)
{

  char *pageArray;     /* contains address from debuggee 
                        * for each slot it adr exist then there are
                        * c_i hanging off of it.
                        */
  char *origPageArray; /* traversing page_array frequently so keep
                        * the original  
                        */
 
  char *chunkArray;    /* initial array to store chunk_info 
                        * after this it will extract certain fields
                        * and place in real chunk  
                        */
  char *chunkArrayPtr; /* points to chunkArray,  theptr will get incr */

  CORE_ADDR chunk_page_addr;  /* addr of array chunks_n_page.  First
                               * it is set at the beginning.  Then it
                               * incr by ptrsize to read chunk_info_addr
                               */


  CORE_ADDR curAddr;
  struct minimal_symbol *m = 0;
  char buf[10];                       
  int i,j;
  int status;
  gdb_chunk_info *c_i, *c_i_new;
  tmp_chunk_info tmp_c_i;    /* This is used to ease the bit extraction */


  /*
   * Before performing any extraction, set the proper size. 
   */ 

  trueChunkSz = sizeof(gdb_chunk_info); /* size of gdb_chunk_info */
#if defined(HP_IA64)
  if (!is_swizzled)
  {
     trueSize = 8; 
  }
  else
  {
     trueSize = 4; 
  }
#else
#if defined(GDB_TARGET_IS_HPPA_20W)
     trueSize = 8; 
#else
     trueSize = 4; 
#endif
#endif  



  /*  
   * NOTE: It is crucial the lookup of total_pages and chunks_in_page
   * works.  If it fails there are couple of possibilities.
   * 1) a.out that generated this corefile was not linked 
   *    with librtc.so/sl correctly.  If it did link, it might
   *    have linked with incorrect 32 or 64 version. 
   * 2) these symbols are not exported correctly.  On PA,
   *    static symbols are not visible.  total_pages and 
   *    chunks_in_page had to be changed to not static.
   */ 


  /* 
   * might not need to do target_read_mem for total_pages.
   * It's safer to do but if we want to save a target_read
   * consider not doing this.  
   */   
  m = lookup_minimal_symbol ("total_pages", 0, rtc_dot_sl); 
  if (!m)
    {
      /* do this detail error checking once.  Could do it for chunks_in_page
       * also but this should be sufficient.  total_pages and chunks_in_page
       * are together forever.              
       */
      if ((!target_has_execution && target_has_stack))
        error("If this is a corefile, the program that generated this core did not link with librtc correctly.  Unable to find librtc symbol total_pages");
      else
        error("Detected incorrect version of librtc.  Unable to find librtc symbol total_pages");
    }

  status = target_read_memory (SYMBOL_VALUE_ADDRESS (m), buf, 
                               trueSize);
  if (status != 0)
     error("Error occurred during reading total pages!");

  gdb_total_pages = (long) extract_signed_integer (buf,trueSize);

  pageArray = calloc (gdb_total_pages, trueSize); 

  /* read the address of the beginning of chunks_n_page */
  m = lookup_minimal_symbol ("chunks_in_page", 0, rtc_dot_sl);
  if (!m)
     error("Detected incorrect version of librtc.  Unable to find librtc symbol chunks_in_page");

  status = target_read_memory (SYMBOL_VALUE_ADDRESS (m), buf, 
                               trueSize);  

  if (status != 0)
     error("Error reading chunks_in_page");

  chunk_page_addr = extract_address (buf, trueSize);

  if (chunk_page_addr == 0)
    error("incorrect page address");

  status = target_read_memory (chunk_page_addr, pageArray, 
                                (int) gdb_total_pages*trueSize);

  if (status != 0)
     error("Error reading pageArray");


  origPageArray = pageArray;

  chunks_n_page = (gdb_chunk_info **) 
                   calloc(gdb_total_pages, sizeof (gdb_chunk_info *));

  /* alloc this once and reuse for all c_i */
  chunkArray = calloc (1, trueChunkSz);
  pageArray = origPageArray;
  for (i=0; i < gdb_total_pages; i++, pageArray += trueSize)
  {
    curAddr = extract_address (pageArray, trueSize);
    if (curAddr)
    {
       status = target_read_memory (curAddr, chunkArray, trueChunkSz); 
       if (status != 0)
          error("Reading a chunk_array");

       c_i = calloc (1, sizeof(gdb_chunk_info)); 
       chunks_n_page[i] = c_i;

       chunkArrayPtr = chunkArray;


       /*
        * at this point chunkArray contains a chunk_info from core or
        * a live process.  It's 28 or 56 bytes of bit patterns depends on
        * if it's 32/64 bit.  Extract the fields and populate gdbrtc.c
        * structure gdb_chunk_info.  For fields base, end, next_leak
        * just extract and assign to those fields.   For bit fields
        * first put in a temp structure with the union then extract the 
        * the bitfields one by one.  This is the easiest and safest way.
        * For pc and next, initially extract and assign them as the
        * other fields but the their usage are different.  The next 
        * field initially contains the target address of the next
        * chunk_info.  If this is not null take this address and 
        * read in the bit pattern of the next chunk_info of the 
        * target.  Set the next field to point to the chunk_info of gdb.
        * After the initial reading here the next field is to 
        * link up all the gdb_chunk_info.
        * The pc field initially contains the pointer to array of 
        * pc of the target.  If pc is not null, allocate an array
        * of size framecount and read the entire pc array from the target.
        * The pc field is reset to point to this array which is now
        * in gdb space.  
        */ 

       extract_long_unsigned_integer (chunkArrayPtr, trueSize, 
                                      &tmp_c_i.theUnion.flags);  
       c_i->scanned = tmp_c_i.theUnion.bits.scanned;
       c_i->do_not_scan = tmp_c_i.theUnion.bits.do_not_scan;
       c_i->old_leak = tmp_c_i.theUnion.bits.old_leak;
       c_i->heap_block = tmp_c_i.theUnion.bits.heap_block;
       c_i->padded_block = tmp_c_i.theUnion.bits.padded_block;
       c_i->preinit_mmap = tmp_c_i.theUnion.bits.preinit_mmap;
       c_i->new = tmp_c_i.theUnion.bits.new;
       c_i->freed_block = tmp_c_i.theUnion.bits.freed_block;
       chunkArrayPtr += trueSize;
       c_i->next_leak = extract_address (chunkArrayPtr, trueSize);
       chunkArrayPtr += trueSize;
       c_i->next = extract_address (chunkArrayPtr, trueSize);
       chunkArrayPtr += trueSize;
       c_i->base = extract_address (chunkArrayPtr, trueSize);
       chunkArrayPtr += trueSize;
       c_i->end = extract_address (chunkArrayPtr, trueSize);
       chunkArrayPtr += trueSize;
       c_i->pc_tuple = extract_address (chunkArrayPtr, trueSize);

       /* Suresh: Jan 08; read in the pc tuple array */
       if (c_i->pc_tuple)
         {
            CORE_ADDR pc_tuple_list = c_i->pc_tuple;
            struct gdb_pc_tuple *temp_pc_tuple = calloc (frame_count, sizeof(struct gdb_pc_tuple));
            /* get the frames for this pc_tuple_list */
            for (int i = 0; pc_tuple_list && i < frame_count; i++)
              {
                 /* IPF 32 bit target case */
                 if ( PTR_SIZE != sizeof (CORE_ADDR))
                  {

                   /* Read the PC first */
                    if ( target_read_memory ((CORE_ADDR) pc_tuple_list +
                                            (i * SIZEOF_LIBRTC_PC_TUPLE),
                                           (char *) &temp_pc_tuple[i].pc,
                                                      sizeof(PTR_SIZE )))
                       error ("Error downloading pc_tuple data !");
                   /* Shift as pointer size is not equal to the sizeof CORE_ADDR */
                   temp_pc_tuple[i].pc = temp_pc_tuple[i].pc >>
                                  ((sizeof (CORE_ADDR) - PTR_SIZE) * 8);
                   /* Now read the index */
                   if ( target_read_memory ( (CORE_ADDR) pc_tuple_list + (i * SIZEOF_LIBRTC_PC_TUPLE) + PTR_SIZE, (char *) &temp_pc_tuple[i].library_index_of_pc, sizeof(int)))
                       error ("Error downloading pc_tuple data !");

                  }
                /* On all other targets sizeof(gdb_pc_tuple) and librtc's pc_tuple
                   match, so you can increment the debuggee's pc_tuple pointer
                   with sizeof(gdb_pc_tuple) */
                else
                  {

                    if ( target_read_memory (pc_tuple_list + ( i * sizeof(struct gdb_pc_tuple)), (char*) &temp_pc_tuple[i], sizeof(struct gdb_pc_tuple)))
                       error ("Error downloading pc_tuple date !");
                  }

              }
            c_i->pc_tuple = (CORE_ADDR)temp_pc_tuple; 

         } 


       /* 
        * since this routine is shared for info corruption  
        * and info heap arena # block #, do process_c_i only 
        * for info corruption. 
        */
       if (strcmp (request_type, "ARENA_INFO"))
          process_c_i (c_i, fd); 

       /* read in the rest of the list */
       while (c_i->next)
       {
          curAddr = c_i->next;
          status = target_read_memory (curAddr, chunkArray, trueChunkSz);

          if (status != 0)
             error("Error reading a next chunk_info");

          c_i_new = calloc (1, sizeof(gdb_chunk_info));
          chunkArrayPtr = chunkArray;

          /* extract all fields */

          extract_long_unsigned_integer (chunkArrayPtr, trueSize, 
                                         &tmp_c_i.theUnion.flags);  
          c_i_new->scanned = tmp_c_i.theUnion.bits.scanned;
          c_i_new->do_not_scan = tmp_c_i.theUnion.bits.do_not_scan;
          c_i_new->old_leak = tmp_c_i.theUnion.bits.old_leak;
          c_i_new->heap_block = tmp_c_i.theUnion.bits.heap_block;
          c_i_new->padded_block = tmp_c_i.theUnion.bits.padded_block;
          c_i_new->preinit_mmap = tmp_c_i.theUnion.bits.preinit_mmap;
          c_i_new->new = tmp_c_i.theUnion.bits.new;
          c_i->freed_block = tmp_c_i.theUnion.bits.freed_block;

          chunkArrayPtr += trueSize;
          c_i_new->next_leak = extract_address (chunkArrayPtr, trueSize);
          chunkArrayPtr += trueSize;
          c_i_new->next = extract_address (chunkArrayPtr, trueSize);
          chunkArrayPtr += trueSize;
          c_i_new->base = extract_address (chunkArrayPtr, trueSize);
          chunkArrayPtr += trueSize;
          c_i_new->end = extract_address (chunkArrayPtr, trueSize);
          chunkArrayPtr += trueSize;
          c_i_new->pc_tuple = extract_address (chunkArrayPtr, trueSize);

          c_i->next = (CORE_ADDR) c_i_new; 
          c_i = c_i_new;

          /* Suresh: Jan 08: read in the pc tuple array */
          if (c_i->pc_tuple)
          {
            CORE_ADDR pc_tuple_list = c_i->pc_tuple;
            struct gdb_pc_tuple *temp_pc_tuple = calloc (frame_count, sizeof(struct gdb_pc_tuple));
            /* get the frames for this pc_tuple_list */
            for (int i = 0; pc_tuple_list && i < frame_count; i++)
              {
                 /* IPF 32 bit target case */
                 if ( PTR_SIZE != sizeof (CORE_ADDR))
                  {

                   /* Read the PC first */
                    if ( target_read_memory ((CORE_ADDR) pc_tuple_list +
                                            (i * SIZEOF_LIBRTC_PC_TUPLE),
                                           (char *) &temp_pc_tuple[i].pc,
                                                      sizeof(PTR_SIZE )))
                       error ("Error downloading pc_tuple data !");
                   /* Shift as pointer size is not equal to the sizeof CORE_ADDR */
                   temp_pc_tuple[i].pc = temp_pc_tuple[i].pc >>
                                  ((sizeof (CORE_ADDR) - PTR_SIZE) * 8);
                   /* Now read the index */
                   if ( target_read_memory ( (CORE_ADDR) pc_tuple_list + (i * SIZEOF_LIBRTC_PC_TUPLE) + PTR_SIZE, (char *) &temp_pc_tuple[i].library_index_of_pc, sizeof(int)))
                       error ("Error downloading pc_tuple data !");

                  }
                /* On all other targets sizeof(gdb_pc_tuple) and librtc's pc_tuple
                   match, so you can increment the debuggee's pc_tuple pointer
                   with sizeof(gdb_pc_tuple) */
                else
                  {

                    if ( target_read_memory (pc_tuple_list + ( i * sizeof(struct gdb_pc_tuple)), (char*) &temp_pc_tuple[i], sizeof(struct gdb_pc_tuple)))
                       error ("Error downloading pc_tuple date !");
                  }

              }
            c_i->pc_tuple = (CORE_ADDR)temp_pc_tuple; 

          } 

          /* if not equal to ARENA_INFO then do process */
          if (strcmp (request_type, "ARENA_INFO"))
             process_c_i (c_i, fd); 

       } /* while read the rest of c_i */


    }  /* if curAddr exists */
  }    /* for */

  free (chunkArray);
  free (pageArray);

}  /* read_all_chunks */




/* 
 * This function is called when user does an 'info corruption' 
 * For live process, set bounds checking on, infrtc does
 *    the work and saves information to /tmp/__heap_info.<inferior_pid>.
 *    heap_info_command_common reads from /tmp/__heap_info.<inferior_pid>
 *    and display the information.
 * 
 * For corefile, read in the chunks similar to how infrtc does it.
 *    Saves information to /tmp/__heap_info.<inferior_pid>.  Use
 *    existing routines down_load_data() and print_blocks() to display. 
 */
static void
bounds_info_command (char *blocknum_exp, int from_tty)
{

  int status, i, j;           /* status and indexes */
  int   fd;                   /* for /tmp/__heap_info.<inferior_pid> */


  /* Suresh, Jan 08 - Add unload library symbols before printing 
     stack trace.. 
   */
  add_symbols_of_unloaded_libraries(ALL_UNLOADED_LIBRARIES);

  /* 
   * Enter this if the user do info corruption and it is a corefile 
   */
  if ((!target_has_execution && target_has_stack))
  {
     char heap_info_filename[100];
     int real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
					PIDGET(inferior_pid) :
					get_pid_for(inferior_pid));

     sprintf (heap_info_filename, "/tmp/__heap_info.%u", real_inferior_pid);
     (void) unlink (heap_info_filename);
     fd = open (heap_info_filename, O_RDWR | O_CREAT | O_EXCL, 0777);
     if (fd == -1)
        perror_with_name (heap_info_filename);
     /* 
      * Allow anyone to write, preventing permissions problems if left around.
      * Needed in case umask makes 0777 in creat(2) call ineffective.
      */
     (void) fchmod (fd, 0777);

     /* release any old data.. down_load_data will allocate block_info */
     for (i=0; i < block_count; i++)
        free (block_info[i].pc_tuple);

     free (block_info);
     block_info = 0;
     block_count = 0;
     total_blocks = 0;
     total_bytes = 0;

     read_all_chunks ("COREFILE_INFO", fd); 

     if (close (fd) == EOF)
     {
        error ("FCLOSE_FAILED");
     }

 
     /* 
      * instead of calling heap_info_command_common which has 
      * a lot of extra stuff, just call down_load_data and print_blocks  
      */
     if (block_count > 0)
     {
        FILE *fp;

        /* QXCR1000751009: Download data for bounds info */
        down_load_data (BOUNDS_INFO);
        if (could_be_filename (blocknum_exp))
          {
            /* treat it as a filename ... */
            fp = open_log_file (blocknum_exp);
            if (!fp)
              error ("Cannot open file %s\n", blocknum_exp);

            print_blocks (block_info, fp, BOUNDS_INFO); 
          }
        /*
         * QXCR1000751009: Support "info corruption n" to show n-th
         * corruption info from core file
         */
        else if (blocknum_exp && could_be_number (blocknum_exp))
          {
            int block_num = atoi (blocknum_exp);

            if (block_num < 0 || block_num >= block_count)
              error ("Bad block number.\n");
            else  
              /* always to the screen */
              report_this_block (block_num, 0, BOUNDS_INFO);
          }
        else 
           print_blocks (block_info, 0, BOUNDS_INFO); 
      }  /* block_count > 0 */

     /* should we remove after printing?? */
     unlink (heap_info_filename);
 
     free_all_chunks();

  }    /* if corefile  */
  else 
  {
     /* for live process, bounds checking should be enabled 
      * for this command to work. 
      */
    if (check_bounds == 1)
    {
       set_inferior_var_value ("__rtc_bounds_or_heap", BOUNDS_INFO);
       heap_info_command_common(blocknum_exp, from_tty, BOUNDS_INFO);
    }
    else
       error ("Bounds checking not enabled. Use help heap-check for syntax");
  }    /* live process */
}


/* Suresh: Jan 08: QXCR1000573545 - Incorrect stack trace after a dlclose 

   This function is used to manualy add the struct objfile and the symbol info
   of all unloaded libraries to the gdb data structures. The trick here is we 
   add these symbols, but these are not present in the address space of the 
   debuggee any longer, as they have been unloaded by dlclose/shl_unload() call

   Care needs to be excercised if a user tries to place a breakpoint/print 
   these sysmbols, because it may result in an error as gdb will not be 
   able to breakpoints or say print these symbols, as they are really 
   not present in the address space.

   The way we do this is as follow: We find out all libs that are unloaded from 
   gbd_shlib_info() and if NOT manually added earlier, we will add the 
   objfile/symbol info using the gdb mechanism of add_symbol_file_command(), 
   giving a start address that should not clash with any used address. We give 
   a start address of 0x40000000 00002000 in IA64 (close to the default link 
   time address) and add the objfile for the 1st unloaded library. We find out 
   the size of the just added text symbols (rather the size of text segment 
   as if the library was really loaded in the address space), by looking at 
   "this.objfile->obfd->test_size". All other fields like text_high in 
   obfd/objfile, seem to be bogus and cannot be trusted. We add the 2nd 
   unloaded symbols @0x40000* + <above> text_size; and so on, to guarantee 
   that the addresses of all unloaded library symbols don't overlap.

   We maintain 2 distinct 'objfile' lists that are "disjoint" - the main 
   (primary) object_files list that GDB is already is aware off(the one that 
   has the current 'live and loaded' modules. Whenever we add a objfile 
   manually for an unloaded module, we add it our alternate list.(except for
   the 1st unloaded module, where we add to the primary list and then take it
   out of primary list and create the alternate secondary list, to avoid any 
   side effects of creating a objfile when the "object_files" head in NULL) 

   The idea is to have all unloaded modules in a seperate list, so that text 
   start/end doesn't need to be unique between these lists( they can be 
   overlapping).  This is reqd, because otherwise later symbol lookups would be 
   incorrect and we will not achieve our objective of resolving unloaded 
   library PCs correctly. Figuring out a non-overlapping text start is a 
   *non-trivial* task, as its typically done by Mr.kernel, whenever program 
   segments are loaded!  We are not loading the library as such, but just 
   digesting the symbol info in the library and construcing kind off 
   'link time' addresses that are non-overlapping only 
   **among the unloaded libraries**

   Q 1: What will happen, if after we do this manual load of thes symbols at
   an address(say 0x400000000002000) the application tries to load the same 
   library again?

   There will be 2 objfiles for this library. One is each objfile list. All 
   previous PC references to this library would be 'offset' addresses and would
   get resolved by using the 'alt' objfile list and any PC ref's to the current
   loaded instance would be "absolute address' and be resolved by using the 
   primary(live) objfile list.  

   TODO : Performance improvement: (not a correctness issue, would still work)
   We would need to handle cases when the same library is loaded/unloaded 
   multiple times during an interactive session - we should not to do 
   add_symbol_file*() mutiple times for the same unloaded library. This is not
   an issue for B mode, as this table is fetched only once at the end of the 
   application run.
*/
static void
add_symbols_of_unloaded_libraries ( int library_index_no)
{
    static struct objfile *just_added_objfile = 0;
    struct objfile* l;
    char temp_str[MAXPATHLEN+sizeof(CORE_ADDR)] = { "0" }; /* space for max lib name + start address */
    int i;

    if (!target_has_stack)
        error ("There is no running process.\n");
       
    /*We need to read again from RTC side, only if it has changed from 
      last time we read in. This flag is defined in librtc and reset 
      by librtc whenever there is a new update to the shlib_info.
      The size of this flag is 'int' and if at all it changes to 
      someother type, we should change here as well..
     */
    /* QXCR1000751009: Handle core file */
    if (!target_has_execution ||
        get_inferior_var_value ("gdb_has_read_shlib_info",sizeof(int)) == 0)
      {
         read_gdb_shlib_info();
      }

    /* Given index should be within the bounds 0 to gdb_shlib_info_count OR
       the special value 'ALL_UNLOADED_LIBRARIES'( -1)
    */
    if ( !(library_index_no == ALL_UNLOADED_LIBRARIES || (library_index_no >= 0 && 
                              library_index_no < gdb_shlib_info_count)))
      error( "Internal Error: unloaded library index is outside range \n");

    /* We add the symbol file of all unloaded libraries at one shot if
       the index passed is ALL_UNLOADED_LIBRARIES
     */
    if( library_index_no == ALL_UNLOADED_LIBRARIES )
      i = 0;
    else
      i = library_index_no;
    for( ; i < gdb_shlib_info_count; i++)
      {
       if (gdb_shlib_info[i].is_it_loaded == FALSE)
        {
          struct objfile* temp_head = NULL;
          struct objfile *cur, *prev;

          /* Make object_files point to the (alternate) list of 
             objfiles for the unloaded modules added by us if
             we are coming here for 2nd or later time. Only we
             add symfile for the first time, we add it to
             the main object_files list.
           
          */
          if( first_added_unloaded_module_objfile != NULL)
            {
            temp_head = object_files;
            object_files = first_added_unloaded_module_objfile;
            }
          else
            load_address_for_next_library = FICT_START_ADDRESS;

          add_unloaded_library_symbols = true; 
/* PA 32 */
#if defined (GDB_TARGET_IS_HPPA) && !defined(GDB_TARGET_IS_HPPA_20W)
          sprintf(temp_str,"%s %lu",gdb_shlib_info[i].library_name, load_address_for_next_library);
#else 
          sprintf(temp_str,"%s %llu",gdb_shlib_info[i].library_name, load_address_for_next_library);
#endif
          add_symbol_file_command (temp_str,0);
          add_unloaded_library_symbols = false;
        
 
          /* cycle thru the objfile structures and find the text_size 
             of the just added objfile.  struct objfile stores the 
             absolute path of the library name whereas gdb_shlib_info 
             can at times have relative paths and its safer to match 
             the substring rather than do an exact match

             Remember the objfile for the unloaded library that is 
             added first, only once when the first of the 
             add_symbol_file_command is called by us..
 
             Also need to find the objfile for the last of the live 
             modules, that point to the just added objfile(first) 
             corresponding to the unloaded library , and makes it next 
             ptr NULL;
          */
          prev = NULL;
          cur  = object_files;
          just_added_objfile = NULL;

          /* restore the global object_files */
          if( first_added_unloaded_module_objfile != NULL && temp_head )
            object_files = temp_head;

          /* cycles thru one of the object files list - either the
             main list of "live" modules or the alternate list of
             unloaded modules, to find out 2 things: the just loaded
             object file and also check to see if its was the first
             of the object file added by us.
          */
          while ( cur != NULL )
           { 
            if (strstr( cur->name, gdb_shlib_info[i].library_name)) 
             {
              just_added_objfile = cur;
              if( first_added_unloaded_module_objfile == NULL)
                {
                 /* make the last objfile in the live list NULL */
                  prev->next = 0; 
                  first_added_unloaded_module_objfile = cur;
                }
               break;
              }
            prev = cur;
            cur=cur->next;
           }
          if (just_added_objfile == NULL)
            error ("Internal error:No objfile found for unloaded lib!\n");

          DEBUG(printf ("RTC : Last added lib start = %llu ; size = %lld \n", load_address_for_next_library, just_added_objfile->obfd->text_size);)

          /* Once loaded make a note, so that we don't load it again */
          gdb_shlib_info[i].is_it_loaded = TRUE;
          gdb_shlib_info[i].text_start = load_address_for_next_library;
          gdb_shlib_info[i].text_end = (load_address_for_next_library + just_added_objfile->obfd->text_size);
          load_address_for_next_library +=just_added_objfile->obfd->text_size;
        }
      /* If we came here for loading a specific library, get out of the 
         loop
       */
      if( library_index_no != ALL_UNLOADED_LIBRARIES)
        break;
     }
}

/* This function is called when user does an 'info heap'. */
static void
heap_info_command (char *blocknum_exp, int from_tty)
{


   /* 
    * when doing corefile target_has_execution=0, target_has_stack=1.  
    * With a corefile do not call set_inferior_var_value
    * 
    */
   /* When user says info heap after the program has exited 
      then we need not go and set the __rtc_bounds_or_heap as 
      librtc.sl will be unloaded. Instead we must issue a warning 
      that the program is no longer running. */
     /*
     1>live process target_has_execution =1 && target_has_stack=1
     2> After the program exit target_has_execution = 0 &&
      target_has_stack = 0
     3>For corefile target_has_execution =0 target_has_stack = 1
     So for 2 and 3rd case we need not set the __rtc_bounds_or_heap
    */

   /* Suresh, Jan 08 - Add unload library symbols before printing 
      stack trace.. 
    */
   add_symbols_of_unloaded_libraries(ALL_UNLOADED_LIBRARIES);

   if ((target_has_execution && target_has_stack))
      set_inferior_var_value ("__rtc_bounds_or_heap", HEAP_INFO);

   heap_info_command_common(blocknum_exp, from_tty, HEAP_INFO);

}


/* this will be automatically updated by the `set heap-check' command */
static char * heap_check_args;

static void
set_heap_check_command (char *args, int from_tty, struct cmd_list_element *c)
{
  int found = 0, switching_heap_checking_on = 0;
  struct objfile * objfile;
  int value;

  if (target_has_execution)
    {
       ALL_OBJFILES (objfile)
         {
            if (strstr (objfile->name, "/librtc")) 
              {
                 found = 1;
                 break;
              }
         }
         if (!found) 
              error ("librtc is not loaded: Either use -leaks command line option, set heap-check before starting the program, or link librtc explicitly");
     }
      
   /* First of all cause to emit an `rtc-parameters-invalid' annotation.
      This is necessary so that the GUI and GDB speak the same language
      (have the same values for parameters.) In response to the
      annotation, the GUI will issue the command, "show heap-check"
      which dumps out all the values.
   */

   annotate_rtc_parameter_change ();

   /* The input args is useless, we need to look in heap_check_args.
      GDB's set command handler leaves this pointer pointing after
      the "set heap-check". For example, if the user issued the
      command "set heap-check leaks on", when control reaches this
      point, the char * heap_check_args points to the substring
      "leaks on".
   */

   args = heap_check_args;

   if (args == 0)
     error ("Incomplete command : try `help set heap-check'");

   /* We will only do minimal validation of input arguments. For 
      example the code below would accept "set heap-check leaks ontario"
      happily as though the user said "set heap-check leaks on". This 
      should be good enough.

      Many of the heap checking features are controlled by variables in
      the inferior. Update the variables suitably. There is no variable
      for toggling leak detection however. For this case, we will simply
      update the call stack frame count to be zero. The thing is even 
      when the user does not care for a leak report, we still need to 
      maintain the exact same data structures to be able to issue any 
      other heap debugging diagonostic.
   */

   while (*args == ' ' || *args == '\t')
     args++;

   if (!strncmp (args, "leaks ", 6) || !strncmp (args, "leaks\t", 6))
     {
       args += 6;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         {
           switching_heap_checking_on = check_leaks == 0 ? 1 : 0; /* leaks is being changed ot on from off */
           check_heap = check_leaks = 1;
         }
       else if (!strncmp (args, "off", 3))
         check_leaks = 0;
     }
   else if (!strncmp (args, "bounds ", 7) ||
            !strncmp (args, "bounds\t", 7))
     {
       args += 7;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         {
           switching_heap_checking_on = check_bounds == 0 ? 1 : 0; /* bounds is being changed to on from off */
           check_heap = check_bounds = 1;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_check_bounds", 1);
         }
       else if (!strncmp (args, "off", 3))
         {
           check_bounds = 0;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_check_bounds", 0);
         }
     }
   else if (!strncmp (args, "free ", 5) || !strncmp (args, "free\t", 5))
     {
       args += 5;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         {
           switching_heap_checking_on = check_free == 0 ? 1 : 0; /* free is being changed to on from off */
           check_heap = check_free = 1;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_check_free", 1);
         }
       else if (!strncmp (args, "off", 3))
         {
           check_free = 0;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_check_free", 0);
         }
     }
   else if (!strncmp (args, "string ", 7) || !strncmp (args, "string\t", 7))
     {
       args += 7;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         {
           check_string = 1;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_check_string", 1);
         }
       else if (!strncmp (args, "off", 3))
         {
           check_string = 0;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_check_string", 0);
         }
     }
   else if (!strncmp (args, "compiler ", 9) || 
            !strncmp (args, "compiler\t", 9))
     {
       args += 9;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         check_heap = compiler_heap_check = 1;
       else if (!strncmp (args, "off", 3))
         compiler_heap_check = 0;
     }
   else if (!strncmp (args, "scramble ", 9) || 
            !strncmp (args, "scramble\t", 9))
     {
       args += 9;
       while (*args == ' ' || *args == '\t')
         args++;
       /* Emits a warning message to user when scrambling is turned on
        * since scrambling sometimes results in crash or misbehavior.
        */
       if (!strncmp (args, "on", 2))
         {
	   warning("Scrambling feature of memory debugging is turned on.\n"
                   "This feature may result in unexpected crash or unpredictable\n"
                   "behavior sometimes.");  
           switching_heap_checking_on = scramble_blocks == 0 ? 1 : 0; /* scramble_blocks is being changed to on from off */
           check_heap = scramble_blocks = 1;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_scramble_blocks", 1);
         }
       else if (!strncmp (args, "off", 3))
         {
           scramble_blocks = 0;
           if (target_has_stack && check_heap_in_this_run)
             set_inferior_var_value ("__rtc_scramble_blocks", 0);
         }
     }
   else if (!strncmp (args, "frame-count ", 12) ||
            !strncmp (args, "frame-count\t",12))
     {
       args += 12;
       while (*args == ' ' || *args == '\t')
         args++;

       frame_count = atoi (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         {
           set_inferior_var_value ("__rtc_frame_count", frame_count);
           /* Invalidate the buffers allocated using old parameters. */
           set_inferior_var_value ("__rtc_stack_trace_count", 0);
         }
     }
   else if (!strncmp (args, "high-mem-count ", 15) ||
            !strncmp (args, "high-mem-count\t",15))
     {
       args += 15;
       while (*args == ' ' || *args == '\t')
         args++;

       high_mem_count = atoi (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_high_mem_val", high_mem_count);
     }
   else if (!strncmp (args, "min-leak-size ",14) ||
            !strncmp (args, "min-leak-size\t",14))
     {
       args += 14;
       while (*args == ' ' || *args == '\t')
         args++;

       min_leak_size = atoi (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_min_leak_size", min_leak_size);
     }
   else if (!strncmp (args, "min-heap-size ",14) ||
            !strncmp (args, "min-heap-size\t",14))
     {
       args += 14;
       while (*args == ' ' || *args == '\t')
         args++;

       min_heap_size = atoi (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_min_heap_size", min_heap_size);
     }
   else if (!strncmp (args, "watch ",6) ||
            !strncmp (args, "watch\t",6))
     {
       args += 6;
       while (*args == ' ' || *args == '\t')
         args++;

       watch_address = parse_and_eval_address (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_pointer_var_value ("__rtc_watch_address", 
                                                     watch_address);
     }
   else if (!strncmp (args, "block-size ",11) ||
            !strncmp (args, "block-size\t",11))
     {
       args += 11;
       while (*args == ' ' || *args == '\t')
         args++;

       big_block_size = atoi (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_big_block_size", 
                                                  (int) big_block_size);
     }
   else if (!strncmp (args, "heap-size ",10) ||
            !strncmp (args, "heap-size\t",10))
     {
       args += 10;
       while (*args == ' ' || *args == '\t')
         args++;

       heap_growth_size = atoi (args);
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_heap_growth_size", 
                                                    (int) heap_growth_size);
     }
   else if (!strncmp (args, "null-check-size ",16) ||
            !strncmp (args, "null-check-size\t",16))
     {
       args += 16;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value <= 0)
           error("Invalid value for null-check-size\n");

       if (null_check_count != -1)
           error("null-check-count already specified. Ignoring null-check-size value\n");

       null_check_size = value;
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_null_check_size", 
                                   null_check_size);
     }
   else if (!strncmp (args, "seed-value ",11) ||
            !strncmp (args, "seed-value\t",11))
     {
       args += 11;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value <= 0)
           error("Invalid value for seed-value\n");

       null_check_seed_value = value;

       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_null_check_seed_value", 
                                   null_check_seed_value);
     }
   else if (!strncmp (args, "random-range ",13) ||
            !strncmp (args, "random-range\t",13))
     {
       args += 13;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value <= 0)
           error("Invalid value for random-range\n");

       null_check_random_range = value;

       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_null_check_random_range", 
                                    null_check_random_range);
     }
   else if (!strncmp (args, "null-check ",11) ||
            !strncmp (args, "null-check\t",11))
     {
       args += 11;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp(args, "random", 6))
         {
            check_heap = 1;
            null_check_count = 0;
            if (target_has_stack && check_heap_in_this_run)
              set_inferior_var_value ("__rtc_null_check_count", 
                                        null_check_count);
         }
       else if ((value = atoi(args)))
         {
            check_heap = 1;
            null_check_count = value;

            if (target_has_stack && check_heap_in_this_run)
              set_inferior_var_value ("__rtc_null_check_count", 
                                         null_check_count);
         }
       else
           error("Invalid value for null-check\n");
     }
   else if (!strncmp (args, "interval ",9) ||
            !strncmp (args, "interval\t",9))
     {
       args += 9;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value < 0)
           error("Invalid value for interval\n");

       check_heap_interval_value = value;
       check_heap = 1;

       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_check_heap_interval", 
                                   check_heap_interval_value);
     }
   else if (!strncmp (args, "header-size ",9) ||
            !strncmp (args, "header-size\t",9))
     {
       args += 12;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value < 0)
           error("Invalid value for header-size\n");

       /* Only integral multiples of the default header size*/
       /* If not we take the nearest rounded integral */

       if (value > DEFAULT_HEADER_SIZE) 
         if (value % DEFAULT_HEADER_SIZE == 0)
           rtc_header_size = value;
         else
           rtc_header_size = ( value / DEFAULT_HEADER_SIZE) * DEFAULT_HEADER_SIZE;

       check_heap = 1;

       /* We don't allow modifications to the running program. This newly set value
          will take effect when the user re-runs the program again under GDB
        */
       if (target_has_stack )
           error("The newly set header-size value will take effect on the next run");
     }
   else if (!strncmp (args, "footer-size ",9) ||
            !strncmp (args, "footer-size\t",9))
     {
       args += 12;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value < 0)
           error("Invalid value for footer-size\n");

       /* Only integral multiples of the default footer size*/
       /* If not we take the nearest rounded integral */

       if (value > DEFAULT_FOOTER_SIZE) 
         if (value % DEFAULT_FOOTER_SIZE == 0)
           rtc_footer_size = value;
         else
           rtc_footer_size = ( value / DEFAULT_FOOTER_SIZE) * DEFAULT_FOOTER_SIZE;

       check_heap = 1;

       /* We don't allow modifications to the running program. This newly set value
          will take effect when the user re-runs the program again under GDB
        */
       if (target_has_stack )
           error("The newly set header-size value will take effect on the next run");
     }
   else if (!strncmp (args, "repeat ",7) ||
            !strncmp (args, "repeat\t",7))
     {
       args += 7;
       while (*args == ' ' || *args == '\t')
         args++;

       value = atoi (args);
       if (value < 0)
           error("Invalid value for interval repetition\n");

       check_heap_interval_repeat_value = value;
       check_heap = 1;
       if (target_has_stack && check_heap_in_this_run)
         set_inferior_var_value ("__rtc_check_heap_repeat", 
                                   check_heap_interval_repeat_value);
     }
   else if (!strncmp (args, "reset",5))
     {
       check_heap_interval_reset_value = 1;
       check_heap = 1;
       if (target_has_stack && check_heap_in_this_run)
         {
            set_inferior_var_value ("__rtc_check_heap_reset", 
                                   check_heap_interval_reset_value);
            check_heap_interval_reset_value = 0;
         }
     }
   else if (!strncmp (args, "retain-freed-blocks ",20) || 
            !strncmp (args, "retain-freed-blocks\t",20 ))
     {
       args += 20;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         {
           switching_heap_checking_on = retain_freed_blocks == 0 ? 1 : 0; /* retain_freed_blocks is being changed to on from off */
           check_heap = retain_freed_blocks = 1;
           if (target_has_stack && check_heap_in_this_run)
              set_inferior_var_value ("__rtc_retain_freed_blocks", 1);
         }
       else if (!strncmp (args, "off", 3))
         {
           retain_freed_blocks = 0;
           if (target_has_stack && check_heap_in_this_run)
              set_inferior_var_value ("__rtc_retain_freed_blocks", 0);
         }
     }
   else if (!strncmp (args, "on",2))
     {
       /* The command "set heap-check on" is provided as a short hand
          for the following settings : leak check on, bounds check on,
          free check on.
       */
          
       switching_heap_checking_on = check_heap == 0 ? 1 : 0; /* check_heap is being changed to on from off */
       check_heap = check_leaks = check_bounds =
           check_free = 1;
       if (target_has_stack && check_heap_in_this_run)
         {
           set_inferior_var_value ("__rtc_check_bounds", 1);
           set_inferior_var_value ("__rtc_check_free", 1);
         }
     }
   else if (!strncmp (args, "off",3))
     {
       /* The command "set heap-check off" is the master switch */
          
       check_heap = check_leaks = check_bounds =
           check_free = 0;
       if (target_has_stack && check_heap_in_this_run)
         {
           set_inferior_var_value ("__rtc_check_bounds", 0);
           set_inferior_var_value ("__rtc_check_free", 0);
         }
     }
   else
     error ("Malformed command, try `help set heap-check'.");

   if (found && switching_heap_checking_on)
     warning("Program is already running. Heap information provided may not be accurate.");

  /* Bindu 040303 attach & rtc: If the user started the heap check in the
     middle of execution (or attach). Let's start-up the stuff. In the
     normal case, this is done when the program starts up. */
  if (target_has_stack && check_heap && !check_heap_in_this_run)
    {
      prepare_for_rtc (1);
      snoop_on_the_heap ();
    }
}

static void
show_heap_check_command (char *args, int from_tty, struct cmd_list_element *c)
{
  if (!check_heap)
    {
      printf_filtered ("Heap checking is not enabled now.\n");
      return;
    }

  printf_filtered ("Current heap check settings are :\n");
  printf_filtered ("\tCheck leaks      : %s\n", check_leaks ? "on" : "off");
  printf_filtered ("\tCheck bounds     : %s\n", check_bounds ? "on" : "off");
  printf_filtered ("\tCheck free()     : %s\n", check_free ? "on" : "off");
  printf_filtered ("\tCheck string     : %s\n", check_string ? "on" : "off");
  printf_filtered ("\tScrambling       : %s\n", scramble_blocks ? "on" : "off");
  printf_filtered ("\tFrame count      : %d\n", frame_count);
  printf_filtered ("\tMin-leak-size    : %d\n", min_leak_size);
  printf_filtered ("\tMin-heap-size    : %d\n", min_heap_size);
  printf_filtered ("\tBlock-size       : %d\n", big_block_size);
  printf_filtered ("\tHeader-size      : %d\n", rtc_header_size);
  printf_filtered ("\tFooter-size      : %d\n", rtc_footer_size);
  printf_filtered ("\tSeed-value       : %d\n", null_check_seed_value);
  printf_filtered ("\tRandom-range     : %d\n", null_check_random_range);
  printf_filtered ("\tNull-check       : %d\n", null_check_count);
  printf_filtered ("\tNull-check-size  : %d\n", null_check_size);
  printf_filtered ("\tHeap-size        : %d\n", heap_growth_size);
  printf_filtered ("\tHeap Interval    : %d\n", check_heap_interval_value);
  printf_filtered ("\tRepeat Count     : %d\n", 
                                         check_heap_interval_repeat_value);
  printf_filtered ("\tHigh-mem Count   : %d\n", high_mem_count); 
  printf_filtered ("\tWatch Address    : %s\n", 
              longest_local_hex_string_custom ((LONGEST) watch_address, "08l"));
  printf_filtered ("\tretain-freed-blocks : %s\n", retain_freed_blocks ? "on" : "off");
}

void 
set_rtc_catch_nomem (void)
{
    if (target_has_stack && check_heap_in_this_run)
      set_inferior_var_value ("__rtc_catch_nomem", 1);
    return;
}

void
_initialize_gdbrtc (void)
{
  struct cmd_list_element *set, *show;

  add_info ("dangling", dangling_info_command, 
        "Dangling block report for all dangling blocks, or for a specified block"
        ".\n\nUsage:\n\tinfo dangling [<FILE>] | [<BLK>]\n\nIf a file name is "
        "provided the output of the command is written to the given file.");
  add_info ("leaks", leaks_info_command, 
        "Memory leak report for all leaks, or for a specified LEAK.\n\nUsage:\n"
        "\tinfo leak [<FILE>] | [<LEAK>]\n\nIf a file name is provided the "
        "output of the command is written to the given file.");
  add_info ("heap", heap_info_command, 
	"Memory profile report for all allocations, or for specified ALLOCATION."
        "\n\nUsage:\ninfo heap [<FILE>]\n\t- Displays heap report or writes "
        "report to <FILE>.\ninfo heap <HEAP-ID>\n\t- Produces detailed "
        "information on the specified heap allocation.\ninfo heap process\n\t- "
        "High level memory usage of a process.\ninfo heap arenas\n\t- High level "
        "memory usage for all arenas.\ninfo heap arenas <ARENA-NUM> blocks\n\t- "
        "Block level and overall memory usage.\ninfo heap arenas <ARENA-NUM> "
        "blocks <BLOCK-NUM>\n\t- Block level and overall memory usage details with "
        "stack trace for specified arena and block.\ninfo heap arena "
        "<ARENA-NUM> blocks stacks\n\t- Block level and overall memory usage with "
        "stack trace where applicable\ninfo heap high-mem\n\t- Displays number of"
        " times brk() value changes for a given run along with the highest brk "
        "value in the run.\n");
  add_info ("corruption", bounds_info_command, 
	"Memory profile report of all allocations which appear to be corrupted.\n"
        "\nUsage:\n\tinfo corruption [<FILE>]\n\nIf a file name is provided the "
        "output of the commonad is written to the given file.");

  add_info ("heap-interval", heap_interval_info_command, 
	"Incremental memory profile report of all allocations.\n\nUsage:\n\tinfo "
        "heap-interval [<FILE>]\n");

  set = add_set_cmd ("heap-check", class_support, var_string_noescape,
         (char *) &heap_check_args,
      "Set thread debugging commands\n"
      "Usage:\n\tSet heap-check [ [on|off] | <OPTION> on|off | <OPTION> <NUM> ].\n\
    where <OPTION> on|off or <OPTION> <NUM> can be one of the following:\n\n\
        leaks on|off        : Toggle the leak detection capability.\n\
        bounds on|off       : Toggle bounds check on heap blocks.\n\
        free on|off         : Toggle validation of calls to free()\n\
        scramble on|off     : Specify whether freed blocks should be scrambled\n\
        compiler on|off     : Stop whenever compiler detects corruption in variables\n\
                              allocated by compiler. The application should be\n\
                              compiled with +check=all option.\n\
        string on|off       : Toggle validation of calls to strcpy, strncpy, memcpy,\n\
                              memccpy, memset, memmove, bzero, bcopy\n\
        frame-count <NUM>   : Specify the depth of call stack to be captured\n\
        header-size <NUM>   : Specify the Header guard bytes for detecting boundary\n\
                              corruptions\n\
        footer-size <NUM>   : Specify the Footer guard bytes for detecting boundary\n\
                              corruptions\n\
        min-leak-size <NUM> : Specify the minimum size of a block for stack\n\
                              trace collection. GDB will still be able to \n\
                              report leaks of blocks smaller than this value.\n\
                              However, no stack trace would be provided.\n\
        min-heap-size <NUM> : Specify the minimum size of a block for stack\n\
                              trace collection. GDB will report blocks of size \n\
                              greater than or equal to <num>.\n\
        block-size <NUM>    : Stop the program whenever it makes an allocation\n\
                              request exceeding <num> bytes (0 to disable).\n\
        heap-size <NUM>     : Stop target program whenever it's heap grows by\n\
                              <num> bytes (0 to disable).\n\
        watch <ADDR>        : Instruct GDB to stop whenever a block at the\n\
                              given address gets allocated or deallocated.\n\n\
	high-mem-count <NUM>: stops when brk() value has moved <num> times\n\
        null-check <NUM>    : Instructs malloc to return null after <num>\n\
			      invocations of malloc\n\
	null-check random   : Instructs malloc to return null after a random\n\
			      number of invocations of malloc\n\
        null-check-size <NUM> : Instructs malloc to return null after <num>\n\
			      bytes have been allocated by malloc\n\
        seed-value <NUM>    : Specifies the seed value to be used for\n\
                              generating random null-check-count\n\
        random-range <NUM>  : Specifies the random range to be used by random\n\
			      range\n\
        interval <NUM>      : Specifies the time interval to be used for\n\
                              incremental memory profile\n\
        repeat <NUM>        : Perform incremental profile for <num>\n\
                              interval periods where each period duration\n\
                              is defined by set heap-check interval command.\n\
                              The default value is 100.\n\
        reset               : Discard the heap interval data from the\n\
                              internal data file.\n\
        retain-freed-blocks on|off: Specify whether freed blocks have to be retained\n\
                                    which enables advanced corruption detection.\n\
Simply typing `set heap-check on' uses the following defaults :\n\
    leak check on, bounds check on, free check on\n",
                     &setlist);

    set->function.sfunc = set_heap_check_command;
    show = add_show_from_set (set, &showlist);
    show->function.sfunc = show_heap_check_command;
}

#else   /* RTC */

  /* just to keep the linkers happy */
snoop_on_the_heap (void) {} 
prepare_for_rtc (int) {} 
decode_rtc_event (void) {}
_initialize_gdbrtc (void) {}

#endif 
