/* Low level interface to Gambit simulator, for the remote server for GDB.
   Copyright (C) 1995 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* 
 * Tahoe. These 2 headers are needed for :
 *     a. Gambit (igb_api.h)
 *     b. to be able to read initial PC (from ELF executable - libelf.h)
 * They are placed early to avoid redifnition problems.
 */
#include <assert.h>
#include "igb_api.h"
#include "libelf.h"

#include "defs.h"
#include <sys/wait.h>
#include "frame.h"
#include "inferior.h"

#include <stdio.h>
#include <sys/param.h>
#include <sys/dir.h>
#include <sys/user.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sgtty.h>
#include <fcntl.h>
#include <stdlib.h>
#include <alloca.h>
#include <string.h>
#include <libelf.h>
#include <unistd.h>
/* We want the IA64 version of load_info even though the gambit server
   is an HPPA executable.
   */
#define __ia64
#include "/CLO/TAHOE_BE/usr/include/crt0.h"
#undef __ia64

#include "elf_em.h"

/* 
 * Tahoe. This is to receive argv[0] from main.c. It is needed for some 
 * reason to initiallize Gambit.
 */
#include "server.h"

int is_ilp32 = 0;		/* If the flag bit EF_IA_64_ABI64 is not set, this will
				   be set to TRUE.  EM-32 programs do set this bit.
				 */

/***************Begin MY defs*********************/
int quit_flag = 0;
char registers_array[REGISTER_BYTES];
char *registers = registers_array;

char buf2[MAX_REGISTER_RAW_SIZE];
/*harish_tahoe_investigation:
 * The following definition is needed for the April 98 version 
 * of the Gambit library.
 */
int exFlag = 0;

static gb_t gb_result = {GB_STATUS_OK, 
			 GB_EVENT_NOTIFICATION,
 			 GB_EVENT_EXEC_DONE};
/***************End MY defs*********************/

#include <sys/ptrace.h>
#include <machine/reg.h>

extern char **environ;
extern int errno;
extern int inferior_pid;
void quit (), perror_with_name ();


#define BAD_VALUE	-1

/* break_in_dld_break - If this is a shared bound program, place a Gambit
   breakpoint on top of the break instruction in _asm_break to simulate 
   hitting the break instruction.  This routine also has an important
   side effect of setting is_swizzled, to indicate if this is an ILP32
   program.

   With the microloader change, things are more complicated.  The .interp
   section contains a colon separated list of shared library paths.  The
   first is the mocroloader.  The following ones are services to be loaded
   by the microloader.  For the duration of this gdbserver, there is only
   one service, dld.so.  The variable __load_info_ptr is defined in the
   microloader.  It points at a load_info_t.  At the moment that 
   uld_invoke_startup_service is called, __load_info_ptr->li_uld_taddr contains
   the text address where dld.so was mapped.  The link-time presumed
   text address for dld is zero.  At the time uld.so is invoked, 
   R35 contains the address of the load_info structure.
 */

/***************Begin Forward Declarations *******/
static void fetch_register PARAMS ((int));

int read_inferior_memory PARAMS ((CORE_ADDR, char *, int));

CORE_ADDR swizzle PARAMS ((CORE_ADDR));

CORE_ADDR unswizzle PARAMS ((CORE_ADDR));
/***************End   Forward Declarations *******/

void
break_in_dld_break32 (unsigned long long uld_start_addr, 
		    char *progname,
		    int  filedes,
		    Elf *elf_desc)
{
  unsigned long long addr_dld_break;
  Elf32_Addr addr_start;
  U64 brkpt_addr;
  char* colon_pos;
  char *dld_path;
  Elf_Data *elf_data;
  Elf_Scn *elf_scn;
  Elf32_Shdr *elf32_shdr;
  Elf32_Ehdr *elf32_ehdr;
  Elf32_Sym *elf32_sym;
  int ev_current;
  U64 gb_attr =
  {GB_BKPT_TYPE_FETCH, 0};
  gb_t gb_local_result;
  unsigned long long load_info_addr;
  int ident_offset;
  size_t ident_size;
  char *ident_str;
  load_info_t load_info;
  int nbr_active;
  int nbr_syms;
  char *sec_name;
  int scn_idx;
  char *sym_name;
  int sym_count;
  Elf_Scn *symtab_scn;
  int strtab_scn_idx;
  unsigned long long addr_uld_invoke_startup_service;
  char *uload_path;
  U64 zero_u64 = {0, 0};

  /* Get the program header */

  elf32_ehdr = elf32_getehdr (elf_desc);
  if (elf32_ehdr == NULL)
    {
      error ("Unable to get elf header of %s.", progname);
    }

  is_ilp32 = ((elf32_ehdr->e_flags & EF_IA_64_ABI64) == 0);

  /* Scan the section table looking for the .interp section (SHT_PROGBITS) */

  for (scn_idx = 0; scn_idx < elf32_ehdr->e_shnum; scn_idx++)
    {
      elf_scn = elf_getscn (elf_desc, scn_idx);
      if (elf_scn == NULL)
	{
	  error ("Unable to read section of %s.", progname);
	}
      elf32_shdr = elf32_getshdr (elf_scn);
      if (elf32_shdr == NULL)
	{
	  error ("Unable to read section header of %s.", progname);
	}

      if (elf32_shdr->sh_type != SHT_PROGBITS)
	continue;

      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */
      sec_name = (char *) elf_strptr (elf_desc,
				      elf32_ehdr->e_shstrndx,
				      elf32_shdr->sh_name);
      if (sec_name && strcmp (sec_name, ".interp") == 0)
	break;

    }				/* for each section, scan for .interp until found */

  if (scn_idx >= elf32_ehdr->e_shnum)
    {				/* Did not find .interp section.  Return. */
      nbr_active = elf_end (elf_desc);
      if (nbr_active != 0)
	{
	  error ("Error with elf_end for %s.", progname);
	}
      close (filedes);
      return;
    }

  /* We found the .interp section.  Get the path. */

  elf_data = elf_getdata (elf_scn, NULL);
  uload_path = strdup (elf_data->d_buf);

  /* With the micro-loader changes, .interp is now a colon separated
     list of file paths.  The first is the micro-loader.  The others are
     the services which is loads.  For the time when we expect to use
     gdbserver, the only service is dld.so, so we just move dld_path
     to the character past the first colon.
     */
  
  colon_pos = strchr(uload_path, ':');
  if (!colon_pos)
    {
      error ("Error expected .interp with microloader and dld.", progname);
    }
  *colon_pos = 0;  /* Terminate the string for uload_path. */
  dld_path = colon_pos + 1;
  if (strchr(dld_path, ':'))
    {
      error ("Error expected .interp with only dld service.", progname);
    }

  nbr_active = elf_end (elf_desc);
  if (nbr_active != 0)
    {
      error ("Error with elf_end for %s.", progname);
    }

  /* At startup, r35 points at the load_info structure.  We get the value
     of r35 before anything changes because we will need the address
     of this structure later.
     */

  fetch_register (GR0_REGNUM + 35); /* put R35 into the registers buffer */
  memcpy (&load_info_addr, 
	  &registers[REGISTER_BYTE (GR0_REGNUM + 35)],
  	  sizeof(CORE_ADDR));

  /* For debugging purposes, it is useful to read the initial load_info
     struct which gives uld's text and data segment addresses.

     Now, for some reason, gambit has stopped setting LI_TRACE, so we 
     do it here.
     */

  read_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));
  load_info.li_flags |= LI_TRACE;
  write_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));

  /* Now, examine the microloader and look for the symbol 
     uld_invoke_startup_service.  We will set a breakpoint there and
     run, at which time we can set a breakpoint on _asm_break.
     */

  filedes = open (uload_path, O_RDONLY);
  if (filedes == BAD_VALUE)
    {
      error ("Unable to open interpreter file %s.", uload_path);
    }

  elf_desc = elf_begin (filedes, ELF_C_READ, NULL);
  if (elf_desc == NULL)
    {
      error ("Error, elf_begin on %s.\n", uload_path);
    }

  /* Verify that we have an ia64 executable */

  ident_str = elf_getident (elf_desc, &ident_size);
  if (ident_str == NULL || ident_size < EI_VERSION)
    {
      error ("Bad elf header in %s.", uload_path);
    }

  if (ident_str[EI_MAG0] != 0x7f
      || ident_str[EI_MAG1] != 'E'
      || ident_str[EI_MAG2] != 'L'
      || ident_str[EI_MAG3] != 'F'
      || ident_str[EI_CLASS] != 1	/* ELF32 programs will have 1 here */
      || ident_str[EI_VERSION] != EV_CURRENT)
    {
      error ("Bad elf identifier information in %s.", uload_path);
    }

  /* Get the program header */

  elf32_ehdr = elf32_getehdr (elf_desc);
  if (elf32_ehdr == NULL)
    {
      error ("Unable to get elf header of %s.", uload_path);
    }

  /* Scan the section table looking for the .symtab section (SHT_SYMTAB) 
     and the .strtab section (SHT_STRTAB) 
   */

  symtab_scn = NULL;
  strtab_scn_idx = BAD_VALUE;
  for (scn_idx = 0; scn_idx < elf32_ehdr->e_shnum; scn_idx++)
    {
      elf_scn = elf_getscn (elf_desc, scn_idx);
      if (elf_scn == NULL)
	{
	  error ("Unable to read section of %s.", uload_path);
	}

      elf32_shdr = elf32_getshdr (elf_scn);
      if (elf32_shdr == NULL)
	{
	  error ("Unable to read section header of %s.", uload_path);
	}

      if (elf32_shdr->sh_type != SHT_SYMTAB
	  && elf32_shdr->sh_type != SHT_STRTAB)
	continue;

      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */
      sec_name = (char *) elf_strptr (elf_desc,
				      elf32_ehdr->e_shstrndx,
				      elf32_shdr->sh_name);
      if (elf32_shdr->sh_type == SHT_SYMTAB
	  && sec_name
	  && strcmp (sec_name, ".symtab") == 0)
	{
	  symtab_scn = elf_scn;
	  if (strtab_scn_idx != BAD_VALUE)
	    break;
	}

      if (elf32_shdr->sh_type == SHT_STRTAB
	  && sec_name
	  && strcmp (sec_name, ".strtab") == 0)
	{
	  strtab_scn_idx = scn_idx;
	  if (symtab_scn)
	    break;
	}
    }				/* end for scan sections for .symtab */

  if (scn_idx >= elf32_ehdr->e_shnum)
    {				/* Did not find everything.  Return. */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find symbol table of %s.", uload_path);
      return;
    }

  /* Search the symbol table for "uld_invoke_startup_service" and "$START$".
     $START$ is the link-time presumed address of where we start execution.
     uld_start_addr is the actual run-time address.  The difference between
     these values tells us how to adjust link time addresses to get run-time
     addresses.
     */

  elf_data = elf_getdata (symtab_scn, NULL);
  elf32_sym = (Elf32_Sym *) elf_data->d_buf;
  nbr_syms = elf_data->d_size / sizeof (Elf32_Sym);
  addr_uld_invoke_startup_service = BAD_VALUE;
  addr_start = BAD_VALUE;
  for (sym_count = 0; sym_count < nbr_syms; elf32_sym++, sym_count++)
    {
      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */

      sym_name = (char *) elf_strptr (elf_desc,
				      strtab_scn_idx,
				      elf32_sym->st_name);

      if (sym_name && strcmp (sym_name, "uld_invoke_startup_service") == 0)
	{
	  addr_uld_invoke_startup_service = elf32_sym->st_value;
	  if (addr_start != BAD_VALUE)
	    break;
	}

      if (sym_name && strcmp (sym_name, "$START$") == 0)
	{
	  addr_start = elf32_sym->st_value;
	  if (addr_uld_invoke_startup_service != BAD_VALUE)
	    break;
	}
    }	/* scan symbol table for _asm_break and $START$ */

  if (sym_count >= nbr_syms)
    {				/* did not find both symbols */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find uld_invoke_startup_service in %s.", uload_path);
      return;
    }

  /* Convert the link time address of addr_uld_invoke_startup_service to 
     the run-time address.
     uld_start_addr is the run-time address of $START$.
   */

  addr_uld_invoke_startup_service = 
	addr_uld_invoke_startup_service - addr_start + uld_start_addr;

  /* Set a gambit breakpoint at uld_invoke_startup_service */

  memcpy (&brkpt_addr, &addr_uld_invoke_startup_service, sizeof (brkpt_addr));
  gb_local_result = gb_brkp (GB_BKPT_SET, brkpt_addr, gb_attr);

  nbr_active = elf_end (elf_desc);
  if (nbr_active != 0)
    {
      error ("Error with elf_end for %s.", uload_path);
    }

  close (filedes);

  /* Run the program until we hit the breakpoint we just set.  At
     that point, the load_info.li_svc_taddr will hold the text address
     at which dld was mapped and we can set the breakpoint at _asm_break. 
     */

  gb_result = gb_exec (zero_u64);

  /* To be clean, remove the breakpont at uld_invoke_startup_service. */

  gb_local_result = gb_brkp (GB_BKPT_REMOVE, brkpt_addr, gb_attr);

  /* To find out where dld was mapped, we need to read the load_info
     structure which we know is at address load_info_addr.
     */
  
  read_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));

  /* Now, examine dld and look for the symbol _asm_break.
     Set a breakpoint at that address plus 2.  (The break instruction
     is in the third instruction of the first bundle.)
   */

  filedes = open (dld_path, O_RDONLY);
  if (filedes == BAD_VALUE)
    {
      error ("Unable to open interpreter file %s.", dld_path);
    }

  elf_desc = elf_begin (filedes, ELF_C_READ, NULL);
  if (elf_desc == NULL)
    {
      error ("Error, elf_begin on %s.\n", dld_path);
    }

  /* Verify that we have an ia64 executable */

  ident_str = elf_getident (elf_desc, &ident_size);
  if (ident_str == NULL || ident_size < EI_VERSION)
    {
      error ("Bad elf header in %s.", dld_path);
    }

  if (ident_str[EI_MAG0] != 0x7f
      || ident_str[EI_MAG1] != 'E'
      || ident_str[EI_MAG2] != 'L'
      || ident_str[EI_MAG3] != 'F'
      || ident_str[EI_CLASS] != 1	/* IA32 programs will have 1 here */
      || ident_str[EI_VERSION] != EV_CURRENT)
    {
      error ("Bad elf identifier information in %s.", dld_path);
    }

  /* Get the program header */

  elf32_ehdr = elf32_getehdr (elf_desc);
  if (elf32_ehdr == NULL)
    {
      error ("Unable to get elf header of %s.", dld_path);
    }

  /* Scan the section table looking for the .symtab section (SHT_SYMTAB) 
     and the .strtab section (SHT_STRTAB) 
   */

  symtab_scn = NULL;
  strtab_scn_idx = BAD_VALUE;
  for (scn_idx = 0; scn_idx < elf32_ehdr->e_shnum; scn_idx++)
    {
      elf_scn = elf_getscn (elf_desc, scn_idx);
      if (elf_scn == NULL)
	{
	  error ("Unable to read section of %s.", dld_path);
	}

      elf32_shdr = elf32_getshdr (elf_scn);
      if (elf32_shdr == NULL)
	{
	  error ("Unable to read section header of %s.", dld_path);
	}

      if (elf32_shdr->sh_type != SHT_SYMTAB
	  && elf32_shdr->sh_type != SHT_STRTAB)
	continue;

      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */
      sec_name = (char *) elf_strptr (elf_desc,
				      elf32_ehdr->e_shstrndx,
				      elf32_shdr->sh_name);
      if (elf32_shdr->sh_type == SHT_SYMTAB
	  && sec_name
	  && strcmp (sec_name, ".symtab") == 0)
	{
	  symtab_scn = elf_scn;
	  if (strtab_scn_idx != BAD_VALUE)
	    break;
	}

      if (elf32_shdr->sh_type == SHT_STRTAB
	  && sec_name
	  && strcmp (sec_name, ".strtab") == 0)
	{
	  strtab_scn_idx = scn_idx;
	  if (symtab_scn)
	    break;
	}
    }				/* end for scan sections for .symtab */

  if (scn_idx >= elf32_ehdr->e_shnum)
    {				/* Did not find everything.  Return. */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find symbol table of %s.", dld_path);
      return;
    }

  /* Search the symbol table for "_asm_break" */

  elf_data = elf_getdata (symtab_scn, NULL);
  elf32_sym = (Elf32_Sym *) elf_data->d_buf;
  nbr_syms = elf_data->d_size / sizeof (Elf32_Sym);
  addr_dld_break = BAD_VALUE;
  for (sym_count = 0; sym_count < nbr_syms; elf32_sym++, sym_count++)
    {
      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */

      sym_name = (char *) elf_strptr (elf_desc,
				      strtab_scn_idx,
				      elf32_sym->st_name);

      if (sym_name && strcmp (sym_name, "_asm_break") == 0)
	{
	  addr_dld_break = elf32_sym->st_value;
	  break;
	}
    }				/* scan symbol table for _asm_break and $START$ */

  if (sym_count >= nbr_syms)
    {				/* did not find _asm_break */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find _asm_break in %s.", dld_path);
      return;
    }

  /* Convert the link time address of dld_break to the run-time address.
     The presumed link-time text address is zero, so we just have to
     add the address of the text segment, load_info.li_svc_taddr.
   */

  addr_dld_break = addr_dld_break + load_info.li_svc_taddr;
  addr_dld_break = swizzle (addr_dld_break);

  /* The break instruction is at the third instruction of the first bundle
     in _asm_break.
   */

  addr_dld_break += 2;
  memcpy (&brkpt_addr, &addr_dld_break, sizeof (brkpt_addr));
  gb_local_result = gb_brkp (GB_BKPT_SET, brkpt_addr, gb_attr);

  nbr_active = elf_end (elf_desc);
  if (nbr_active != 0)
    {
      error ("Error with elf_end for %s.", dld_path);
    }

  close (filedes);
  free (uload_path);
}  /* break_in_dld_break32 */

void
break_in_dld_break64 (unsigned long long uld_start_addr, 
		    char *progname,
		    int  filedes,
		    Elf *elf_desc)
{
  unsigned long long addr_dld_break;
  Elf64_Addr addr_start;
  U64 brkpt_addr;
  char* colon_pos;
  char *dld_path;
  Elf_Data *elf_data;
  Elf_Scn *elf_scn;
  Elf64_Shdr *elf64_shdr;
  Elf64_Ehdr *elf64_ehdr;
  Elf64_Sym *elf64_sym;
  int ev_current;
  U64 gb_attr =
  {GB_BKPT_TYPE_FETCH, 0};
  gb_t gb_local_result;
  unsigned long long load_info_addr;
  int ident_offset;
  size_t ident_size;
  char *ident_str;
  load_info_t load_info;
  int nbr_active;
  int nbr_syms;
  char *sec_name;
  int scn_idx;
  char *sym_name;
  int sym_count;
  Elf_Scn *symtab_scn;
  int strtab_scn_idx;
  unsigned long long addr_uld_invoke_startup_service;
  char *uload_path;
  U64 zero_u64 = {0, 0};

  /* Get the program header */

  elf64_ehdr = elf64_getehdr (elf_desc);
  if (elf64_ehdr == NULL)
    {
      error ("Unable to get elf header of %s.", progname);
    }

  is_ilp32 = ((elf64_ehdr->e_flags & EF_IA_64_ABI64) == 0);

  /* Scan the section table looking for the .interp section (SHT_PROGBITS) */

  for (scn_idx = 0; scn_idx < elf64_ehdr->e_shnum; scn_idx++)
    {
      elf_scn = elf_getscn (elf_desc, scn_idx);
      if (elf_scn == NULL)
	{
	  error ("Unable to read section of %s.", progname);
	}
      elf64_shdr = elf64_getshdr (elf_scn);
      if (elf64_shdr == NULL)
	{
	  error ("Unable to read section header of %s.", progname);
	}

      if (elf64_shdr->sh_type != SHT_PROGBITS)
	continue;

      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */
      sec_name = (char *) elf_strptr (elf_desc,
				      elf64_ehdr->e_shstrndx,
				      elf64_shdr->sh_name);
      if (sec_name && strcmp (sec_name, ".interp") == 0)
	break;

    }				/* for each section, scan for .interp until found */

  if (scn_idx >= elf64_ehdr->e_shnum)
    {				/* Did not find .interp section.  Return. */
      nbr_active = elf_end (elf_desc);
      if (nbr_active != 0)
	{
	  error ("Error with elf_end for %s.", progname);
	}
      close (filedes);
      return;
    }

  /* We found the .interp section.  Get the path. */

  elf_data = elf_getdata (elf_scn, NULL);
  uload_path = strdup (elf_data->d_buf);

  /* With the micro-loader changes, .interp is now a colon separated
     list of file paths.  The first is the micro-loader.  The others are
     the services which is loads.  For the time when we expect to use
     gdbserver, the only service is dld.so, so we just move dld_path
     to the character past the first colon.
     */
  
  colon_pos = strchr(uload_path, ':');
  if (!colon_pos)
    {
      error ("Error expected .interp with microloader and dld.", progname);
    }
  *colon_pos = 0;  /* Terminate the string for uload_path. */
  dld_path = colon_pos + 1;
  if (strchr(dld_path, ':'))
    {
      error ("Error expected .interp with only dld service.", progname);
    }

  nbr_active = elf_end (elf_desc);
  if (nbr_active != 0)
    {
      error ("Error with elf_end for %s.", progname);
    }

  /* At startup, r35 points at the load_info structure.  We get the value
     of r35 before anything changes because we will need the address
     of this structure later.
     */

  fetch_register (GR0_REGNUM + 35); /* put R35 into the registers buffer */
  memcpy (&load_info_addr, 
	  &registers[REGISTER_BYTE (GR0_REGNUM + 35)],
  	  sizeof(CORE_ADDR));

  /* For debugging purposes, it is useful to read the initial load_info
     struct which gives uld's text and data segment addresses.

     Now, for some reason, gambit has stopped setting LI_TRACE, so we 
     do it here.
     */

  read_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));
  load_info.li_flags |= LI_TRACE;
  write_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));

  /* Now, examine the microloader and look for the symbol 
     uld_invoke_startup_service.  We will set a breakpoint there and
     run, at which time we can set a breakpoint on _asm_break.
     */

  filedes = open (uload_path, O_RDONLY);
  if (filedes == BAD_VALUE)
    {
      error ("Unable to open interpreter file %s.", uload_path);
    }

  elf_desc = elf_begin (filedes, ELF_C_READ, NULL);
  if (elf_desc == NULL)
    {
      error ("Error, elf_begin on %s.\n", uload_path);
    }

  /* Verify that we have an ia64 executable */

  ident_str = elf_getident (elf_desc, &ident_size);
  if (ident_str == NULL || ident_size < EI_VERSION)
    {
      error ("Bad elf header in %s.", uload_path);
    }

  if (ident_str[EI_MAG0] != 0x7f
      || ident_str[EI_MAG1] != 'E'
      || ident_str[EI_MAG2] != 'L'
      || ident_str[EI_MAG3] != 'F'
      || ident_str[EI_CLASS] != 2	/* IA32 programs will have 1 here */
      || ident_str[EI_VERSION] != EV_CURRENT)
    {
      error ("Bad elf identifier information in %s.", uload_path);
    }

  /* Get the program header */

  elf64_ehdr = elf64_getehdr (elf_desc);
  if (elf64_ehdr == NULL)
    {
      error ("Unable to get elf header of %s.", uload_path);
    }

  /* Scan the section table looking for the .symtab section (SHT_SYMTAB) 
     and the .strtab section (SHT_STRTAB) 
   */

  symtab_scn = NULL;
  strtab_scn_idx = BAD_VALUE;
  for (scn_idx = 0; scn_idx < elf64_ehdr->e_shnum; scn_idx++)
    {
      elf_scn = elf_getscn (elf_desc, scn_idx);
      if (elf_scn == NULL)
	{
	  error ("Unable to read section of %s.", uload_path);
	}

      elf64_shdr = elf64_getshdr (elf_scn);
      if (elf64_shdr == NULL)
	{
	  error ("Unable to read section header of %s.", uload_path);
	}

      if (elf64_shdr->sh_type != SHT_SYMTAB
	  && elf64_shdr->sh_type != SHT_STRTAB)
	continue;

      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */
      sec_name = (char *) elf_strptr (elf_desc,
				      elf64_ehdr->e_shstrndx,
				      elf64_shdr->sh_name);
      if (elf64_shdr->sh_type == SHT_SYMTAB
	  && sec_name
	  && strcmp (sec_name, ".symtab") == 0)
	{
	  symtab_scn = elf_scn;
	  if (strtab_scn_idx != BAD_VALUE)
	    break;
	}

      if (elf64_shdr->sh_type == SHT_STRTAB
	  && sec_name
	  && strcmp (sec_name, ".strtab") == 0)
	{
	  strtab_scn_idx = scn_idx;
	  if (symtab_scn)
	    break;
	}
    }				/* end for scan sections for .symtab */

  if (scn_idx >= elf64_ehdr->e_shnum)
    {				/* Did not find everything.  Return. */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find symbol table of %s.", uload_path);
      return;
    }

  /* Search the symbol table for "uld_invoke_startup_service" and "$START$".
     $START$ is the link-time presumed address of where we start execution.
     uld_start_addr is the actual run-time address.  The difference between
     these values tells us how to adjust link time addresses to get run-time
     addresses.
     */

  elf_data = elf_getdata (symtab_scn, NULL);
  elf64_sym = (Elf64_Sym *) elf_data->d_buf;
  nbr_syms = elf_data->d_size / sizeof (Elf64_Sym);
  addr_uld_invoke_startup_service = BAD_VALUE;
  addr_start = BAD_VALUE;
  for (sym_count = 0; sym_count < nbr_syms; elf64_sym++, sym_count++)
    {
      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */

      sym_name = (char *) elf_strptr (elf_desc,
				      strtab_scn_idx,
				      elf64_sym->st_name);

      if (sym_name && strcmp (sym_name, "uld_invoke_startup_service") == 0)
	{
	  addr_uld_invoke_startup_service = elf64_sym->st_value;
	  if (addr_start != BAD_VALUE)
	    break;
	}

      if (sym_name && strcmp (sym_name, "$START$") == 0)
	{
	  addr_start = elf64_sym->st_value;
	  if (addr_uld_invoke_startup_service != BAD_VALUE)
	    break;
	}
    }	/* scan symbol table for _asm_break and $START$ */

  if (sym_count >= nbr_syms)
    {				/* did not find both symbols */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find uld_invoke_startup_service in %s.", uload_path);
      return;
    }

  /* Convert the link time address of addr_uld_invoke_startup_service to 
     the run-time address.
     uld_start_addr is the run-time address of $START$.
   */

  addr_uld_invoke_startup_service = 
	addr_uld_invoke_startup_service - addr_start + uld_start_addr;

  /* Set a gambit breakpoint at uld_invoke_startup_service */

  memcpy (&brkpt_addr, &addr_uld_invoke_startup_service, sizeof (brkpt_addr));
  gb_local_result = gb_brkp (GB_BKPT_SET, brkpt_addr, gb_attr);

  nbr_active = elf_end (elf_desc);
  if (nbr_active != 0)
    {
      error ("Error with elf_end for %s.", uload_path);
    }

  close (filedes);

  /* Run the program until we hit the breakpoint we just set.  At
     that point, the load_info.li_svc_taddr will hold the text address
     at which dld was mapped and we can set the breakpoint at _asm_break. 
     */

  gb_result = gb_exec (zero_u64);

  /* To be clean, remove the breakpont at uld_invoke_startup_service. */

  gb_local_result = gb_brkp (GB_BKPT_REMOVE, brkpt_addr, gb_attr);

  /* To find out where dld was mapped, we need to read the load_info
     structure which we know is at address load_info_addr.
     */
  
  read_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));

  /* Now, examine dld and look for the symbol _asm_break.
     Set a breakpoint at that address plus 2.  (The break instruction
     is in the third instruction of the first bundle.)
   */

  filedes = open (dld_path, O_RDONLY);
  if (filedes == BAD_VALUE)
    {
      error ("Unable to open interpreter file %s.", dld_path);
    }

  elf_desc = elf_begin (filedes, ELF_C_READ, NULL);
  if (elf_desc == NULL)
    {
      error ("Error, elf_begin on %s.\n", dld_path);
    }

  /* Verify that we have an ia64 executable */

  ident_str = elf_getident (elf_desc, &ident_size);
  if (ident_str == NULL || ident_size < EI_VERSION)
    {
      error ("Bad elf header in %s.", dld_path);
    }

  if (ident_str[EI_MAG0] != 0x7f
      || ident_str[EI_MAG1] != 'E'
      || ident_str[EI_MAG2] != 'L'
      || ident_str[EI_MAG3] != 'F'
      || ident_str[EI_CLASS] != 2	/* IA32 programs will have 1 here */
      || ident_str[EI_VERSION] != EV_CURRENT)
    {
      error ("Bad elf identifier information in %s.", dld_path);
    }

  /* Get the program header */

  elf64_ehdr = elf64_getehdr (elf_desc);
  if (elf64_ehdr == NULL)
    {
      error ("Unable to get elf header of %s.", dld_path);
    }

  /* Scan the section table looking for the .symtab section (SHT_SYMTAB) 
     and the .strtab section (SHT_STRTAB) 
   */

  symtab_scn = NULL;
  strtab_scn_idx = BAD_VALUE;
  for (scn_idx = 0; scn_idx < elf64_ehdr->e_shnum; scn_idx++)
    {
      elf_scn = elf_getscn (elf_desc, scn_idx);
      if (elf_scn == NULL)
	{
	  error ("Unable to read section of %s.", dld_path);
	}

      elf64_shdr = elf64_getshdr (elf_scn);
      if (elf64_shdr == NULL)
	{
	  error ("Unable to read section header of %s.", dld_path);
	}

      if (elf64_shdr->sh_type != SHT_SYMTAB
	  && elf64_shdr->sh_type != SHT_STRTAB)
	continue;

      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */
      sec_name = (char *) elf_strptr (elf_desc,
				      elf64_ehdr->e_shstrndx,
				      elf64_shdr->sh_name);
      if (elf64_shdr->sh_type == SHT_SYMTAB
	  && sec_name
	  && strcmp (sec_name, ".symtab") == 0)
	{
	  symtab_scn = elf_scn;
	  if (strtab_scn_idx != BAD_VALUE)
	    break;
	}

      if (elf64_shdr->sh_type == SHT_STRTAB
	  && sec_name
	  && strcmp (sec_name, ".strtab") == 0)
	{
	  strtab_scn_idx = scn_idx;
	  if (symtab_scn)
	    break;
	}
    }				/* end for scan sections for .symtab */

  if (scn_idx >= elf64_ehdr->e_shnum)
    {				/* Did not find everything.  Return. */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find symbol table of %s.", dld_path);
      return;
    }

  /* Search the symbol table for "_asm_break" */

  elf_data = elf_getdata (symtab_scn, NULL);
  elf64_sym = (Elf64_Sym *) elf_data->d_buf;
  nbr_syms = elf_data->d_size / sizeof (Elf64_Sym);
  addr_dld_break = BAD_VALUE;
  for (sym_count = 0; sym_count < nbr_syms; elf64_sym++, sym_count++)
    {
      /* The (char*) cast shouldn't be needed here, but the compiler
         complained. */

      sym_name = (char *) elf_strptr (elf_desc,
				      strtab_scn_idx,
				      elf64_sym->st_name);

      if (sym_name && strcmp (sym_name, "_asm_break") == 0)
	{
	  addr_dld_break = elf64_sym->st_value;
	  break;
	}
    }				/* scan symbol table for _asm_break and $START$ */

  if (sym_count >= nbr_syms)
    {				/* did not find _asm_break */
      nbr_active = elf_end (elf_desc);
      close (filedes);
      error ("Unable find _asm_break in %s.", dld_path);
      return;
    }

  /* Convert the link time address of dld_break to the run-time address.
     The presumed link-time text address is zero, so we just have to
     add the address of the text segment, load_info.li_svc_taddr.
   */

  addr_dld_break = addr_dld_break + load_info.li_svc_taddr;
  addr_dld_break = swizzle (addr_dld_break);

  /* The break instruction is at the third instruction of the first bundle
     in _asm_break.
   */

  addr_dld_break += 2;
  memcpy (&brkpt_addr, &addr_dld_break, sizeof (brkpt_addr));
  gb_local_result = gb_brkp (GB_BKPT_SET, brkpt_addr, gb_attr);

  nbr_active = elf_end (elf_desc);
  if (nbr_active != 0)
    {
      error ("Error with elf_end for %s.", dld_path);
    }

  close (filedes);
  free (uload_path);
}  /* break_in_dld_break64 */

void
break_in_dld_break (unsigned long long uld_start_addr, char *progname)
{
  unsigned long long addr_dld_break;
  Elf64_Addr addr_start;
  U64 brkpt_addr;
  char* colon_pos;
  char *dld_path;
  Elf_Data *elf_data;
  Elf *elf_desc;
  Elf_Scn *elf_scn;
  Elf64_Shdr *elf64_shdr;
  Elf64_Ehdr *elf64_ehdr;
  Elf64_Sym *elf64_sym;
  int ev_current;
  int filedes;
  U64 gb_attr =
  {GB_BKPT_TYPE_FETCH, 0};
  gb_t gb_local_result;
  unsigned long long load_info_addr;
  int ident_offset;
  size_t ident_size;
  char *ident_str;
  load_info_t load_info;
  int nbr_active;
  int nbr_syms;
  char *sec_name;
  int scn_idx;
  char *sym_name;
  int sym_count;
  Elf_Scn *symtab_scn;
  int strtab_scn_idx;
  unsigned long long addr_uld_invoke_startup_service;
  char *uload_path;
  U64 zero_u64 = {0, 0};

  if (elf_version (EV_CURRENT) == EV_NONE)
    {
      error ("Error from elf_version.");
    }
  filedes = open (progname, O_RDONLY);
  if (filedes == BAD_VALUE)
    {
      error ("Unable to open program file %s.", progname);
    }

  elf_desc = elf_begin (filedes, ELF_C_READ, NULL);
  if (elf_desc == NULL)
    {
      error ("Error, elf_begin on %s.\n", progname);
    }

  /* Verify that we have an ia64 executable */

  ident_str = elf_getident (elf_desc, &ident_size);
  if (ident_str == NULL || ident_size < EI_VERSION)
    {
      error ("Bad elf header in %s.", progname);
    }

  if (ident_str[EI_MAG0] != 0x7f
      || ident_str[EI_MAG1] != 'E'
      || ident_str[EI_MAG2] != 'L'
      || ident_str[EI_MAG3] != 'F'
      || (   ident_str[EI_CLASS] != 2   /* ELF64 */
	  && ident_str[EI_CLASS] != 1)  /* ELF32 */
      || ident_str[EI_VERSION] != EV_CURRENT)
    {
      error ("Bad elf identifier information in %s.", progname);
    }

  if (ident_str[EI_CLASS] == 2)
    break_in_dld_break64 (uld_start_addr, progname, filedes, elf_desc);
  else
    break_in_dld_break32 (uld_start_addr, progname, filedes, elf_desc);
}				/* break_in_dld_break */


/*
 * Tahoe. Provide STDIO functions for Gambit simulator.
 * These entry points are passed into gb_init().
 */
static int
gb_stdin (char *buf, int count)
{
  return read (fileno (stdin), buf, count);
}

static int
gb_stdout (char *buf, int count)
{
  return write (fileno (stdout), buf, count);
}

static int
gb_stderr (char *buf, int count)
{
  return write (fileno (stderr), buf, count);
}

/*
 * Print gambit error message.
 */
static void
print_gb_err_msg (gb_t gb_result)
{
#define MSG_BUF_SIZE 1024
  char msg_buf[MSG_BUF_SIZE + 1];
  int msg_result;

  if (gb_result.status == GB_STATUS_OK)
    return;

  /* Get an error message, if there was trouble. */
  msg_result = gb_err_msg (gb_result, 1, "tctest_target: ", msg_buf,
			   MSG_BUF_SIZE);
  fprintf (stderr, msg_buf);

}

/*
 * Tahoe. Get Entry point for ELF executable, since Gambit doesn't pre-load 
 * the IP.
 */
static unsigned long long
get_entry_addr (target_fname)

     char *target_fname;

{
  int fd;
  Elf *elf;
  Elf64_Ehdr *elfhdr;
  unsigned long long entry_addr;

  /* Open the file, and use ELF library to get the header with the 
   * entry address. */
  if ((fd = open (target_fname, O_RDONLY)) == -1)
    {
      error ("Error opening file: %s.", target_fname);
    }

  if (elf_version (EV_CURRENT) != EV_CURRENT)
    {
      error ("Invalid ELF library version.");
    }
  if (!(elf = elf_begin (fd, ELF_C_READ, (Elf *) 0)))
    {
      error ("Error beginning ELF access: %s.", target_fname);
    }

  if (elf_kind (elf) != ELF_K_ELF)
    {
      error ("Not a valid ELF executable: %s.", target_fname);
    }

  if (!(elfhdr = elf64_getehdr (elf)))
    {
      error ("Could not get ELF header: %s.", target_fname);
    }

  /* Finally, get what we want. */
  entry_addr = elfhdr->e_entry;

  elf_end (elf);

  /* Now, close the file, we don't need ELF access anymore. */
  close (fd);

  return entry_addr;
}

U64 exec_num =
{
  {0, 0}};

/* Start an inferior process and return its pid.
   ALLARGS is a vector of program-name and args.
   ENV is the environment vector to pass.  */

int
create_inferior (program, allargs)
     char *program;
     char **allargs;
{
  int pid;
  /* 
   * Tahoe.
   * init_IP : initial IP (PC) pre-loaded when we start up executable
   * config_parm : used to configure Gambit
   * local_result : result of Gambit operations
   */
  unsigned long long init_IP = 0;
  gb_status config_parm;
  gb_status *config_parm_p;
  gb_t gb_local_result;

  EX86_vaddr_t dummy_addr =
  {
    {0, 0}
  };
  int i, arg_count;
  U64 gb_attr =
  {GB_BKPT_TYPE_FETCH, 0};
  U64 entry_addr;
  U64 zero_u64 = {0, 0};
  U64 gambit_addr;
  unsigned long long long_long_addr;


  {
    /* To debug, set do_debug to 1 and then attach to the server 
       and manually set do_debug to zero. 
     */
    int do_debug = 0;
    while (do_debug)
      ;
  }

  TPRINTF ("Tahoe executable : %s\n", program);

  /* 
   * configure Gambit 
   */
  SET_PARSE_ST (config_parm, GB_CONF_INIT, GB_CONFIG_ENABLE, ul, 0, ul, 0,
		ul, 0, ul, 0);
  gb_config (&config_parm);
  SET_PARSE_ST (config_parm, GB_CONF_DEBUG, GB_CONFIG_ENABLE, ul, 0, ul, 0,
		ul, 0, ul, 0);
  gb_config (&config_parm);

  /* 000128 coulter - If we don't set GB_CONF_PROTECT_SEG_OFF, then
     gdb_exec does not return. 
     */
  SET_PARSE_ST (config_parm, GB_CONF_PROTECT_SEG_OFF, GB_CONFIG_ENABLE,
		ul, 0, ul, 0, ul, 0, ul, 0);
  gb_config (&config_parm);

  /* I am not sure what this GB_CONF_EXECF stuff does and why it needs */
  /* the name of this executable, but this is what DDE did and so this */
  /* is what I'll do */
  SET_PARSE_ST (config_parm, GB_CONF_EXECF, GB_CONFIG_SET,
		str, (char *) strdup (my_name), ul, 0, ul, 0, ul, 0);
  gb_config (&config_parm);

  /* The dbgshl option tells gambit to set the DT_HP_DLD_FLAGS bits
     DT_HP_DEBUG_PRIVATE and DT_HP_DEBUG_CALLBACK to load shared
     libraries private and to call the callback routine.  Gambit won't
     let us set those bits because of protection issues.  
   */

  config_parm_p = gb_parse ("dbgshl");
  if (config_parm_p->Config != GB_CONF_INVALID)
    {
      gb_config (config_parm_p);
    }

  /* Turn NAT on for a higher fidelity simulation at some cost in execution
     speed.
     */
  
  config_parm_p = gb_parse("nat_on");
  if (config_parm_p->Config != GB_CONF_INVALID)
    {
      if (gb_config(config_parm_p).status != GB_STATUS_OK )
	{
	  error ("Unable to turn nat_on in libgambit.");
	}
    }
  else
    error ("Unable to turn nat_on in libgambit.");

  /* Turn on alignment check */

  config_parm_p = gb_parse("ac_on");
  if (config_parm_p->Config != GB_CONF_INVALID)
    {
      if (gb_config(config_parm_p).status != GB_STATUS_OK )
	{
	  error ("Unable to turn ac_on in libgambit.");
	}
    }
  else
    error ("Unable to turn ac_on in libgambit.");

  /* Turn on -asig */
  config_parm_p = gb_parse("asig");
  if (config_parm_p->Config != GB_CONF_INVALID)
    {
      if (gb_config(config_parm_p).status != GB_STATUS_OK )
	{
	  error ("Unable to turn on -asig in libgambit.");
	}
    }
  else
    error ("Unable to turn on -asig in libgambit.");

  /* 
   * initialize Gambit 
   */
  /*harish_tahoe_investigation: supplying extra parameters */
  /*gb_init (NULL, NULL, NULL, gb_stdin, gb_stdout, gb_stderr); */
  gb_init (NULL, NULL, NULL, gb_stdin, gb_stdout, gb_stderr, /*exflg= */ 0,
  /*filname= */ program);


  /* 
   * Load target program 
   */
  /*harish_tahoe_investigation: supplying extra parameters */
  /* gb_local_result = gb_load (program, GB_LOAD_FORMAT_MOFL); */
  gb_local_result = gb_load (program, GB_LOAD_FORMAT_MOFL, /*exflg= */ 0);
  print_gb_err_msg (gb_local_result);


  /* 
   * Load the initial IP and stack 
   */
  /* gb_local_result.opt.il is the entry point address.  For a shared
     bound program, this is an address in dld.sl.  For an archive bound
     program, it is $START$.
   */
  memcpy (&init_IP, &gb_local_result.opt.il, sizeof (init_IP));

  TPRINTF ("Initializing IP to 0x%016llx\n", init_IP);
  fflush (stderr);
  gb_local_result = gb_obj (GB_OBJ_SET, GB_OBJ_IL, 0, (char *) &init_IP,
			    sizeof (init_IP));
  print_gb_err_msg (gb_local_result);


  /*
   * Set the context - cmd line parms & env (which we set to NULL)
   */
  i = 0;
  arg_count = 0;
  while (allargs[i++] != NULL)
    arg_count++;
  gb_local_result = gb_context (dummy_addr, /* target_argc */ arg_count,
  /* target_argv */ allargs, NULL);
  print_gb_err_msg (gb_local_result);

  pid = 1997;			/* fake PID */

  TPRINTF ("Faked pid : %d\n", pid);

  /* Set a gambit breakpoint on top of the break instruction in
     _asm_break to simulate hitting the break instruction there.
     It will also set the global is_ipl32;
   */

  break_in_dld_break (init_IP, program);

  return pid;

#ifdef NONDEF
  pid = fork ();
  if (pid < 0)
    perror_with_name ("fork");

  if (pid == 0)
    {
      ptrace (PT_SETTRC, 0, 0, 0, 0);

      execv (program, allargs);

      fprintf (stderr, "Cannot exec %s: %s.\n", program,
	       strerror (errno));
      fflush (stderr);
      _exit (0177);
    }
  return pid;
#endif
}

/* Kill the inferior process.  Make us have no inferior.  */

void
kill_inferior ()
{
  if (inferior_pid == 0)
    return;

  gb_quit ();
  TPRINTF ("GAMBIT: executed gb_quit\n");

#ifdef NONDEF
  ptrace (8, inferior_pid, 0, 0, 0);
  wait (0);
#endif
/*************inferior_died ();****VK**************/
}

/* Return nonzero if the given thread is still alive.  */
int
mythread_alive (pid)
     int pid;
{
  return 1;
}

/* Wait for process, returns status */

unsigned char
mywait (status)
     char *status;
{
  int pid;
  int w;
  unsigned char exit_status;

  /* no need to wait in Gambit - since we are here, we must have stopped */

  switch (gb_result.status)
    {
    case GB_STATUS_OK:
      switch (gb_result.event_type)
	{
	case GB_EVENT_NOTIFICATION:
	  switch (gb_result.event_code)
	    {
	    case GB_EVENT_ABORT:
	    case GB_EVENT_EXIT:
	      /* all this corresponds to WIFEXITED */
	      exit_status = (unsigned char)
		(gb_result.opt.info.exit_status.dw[1]);
	      fprintf (stderr,
		       "GAMBIT: Child exited with retcode = %d\n",
		       exit_status);
	      *status = 'W';

	      return exit_status;

	      break;

	    case GB_EVENT_CTRL_C:
	      /* this corresponds to WIFSTOPPED */
	      /* fetch_inferior_registers (0); */
	      *status = 'T';
	      fprintf (stderr,
		       "GAMBIT: CTRL-C pressed\n");

	      return (unsigned char) SIGINT;

	      break;

	    case GB_EVENT_BKPT:
	    case GB_EVENT_EXEC_DONE:
	    case GB_EVENT_HW_EVENT:
	    default:

/* FIXME : what does correspond to WTERMSIG (terminated with a signal) ? */

	      /* all this corresponds to WIFSTOPPED */
	      fetch_inferior_registers (0);
	      *status = 'T';
	      return (unsigned char) SIGTRAP;

	      break;
	    }			/* switch (gb_result.event_code) */
	  break;		/* case GB_EVENT_NOTIFICATION */

	default:		/* status is GB_STATUS_OK but event is other */
	  /* than GB_EVENT_NOTIFICATION - bad news */
	  fprintf (stderr,
		   "GAMBIT: did not get GB_EVENT_NOTIFICATION");
	  break;
	}			/* switch (gb_result.event_type) */
      break;			/* GB_STATUS_OK: */

    default:			/* status is other than GB_STATUS_OK - bad news */
      fprintf (stderr, "GAMBIT : did not get GB_STATUS_OK");
      break;
    }				/* switch (gb_result.status) */

#ifdef NONDEF
  pid = wait (&w);

  if (pid != inferior_pid)
    perror_with_name ("wait");

  if (WIFEXITED (w))
    {
      fprintf (stderr, "\nChild exited with retcode = %x \n", WEXITSTATUS (w));
      *status = 'W';
      return ((unsigned char) WEXITSTATUS (w));
    }
  else if (!WIFSTOPPED (w))
    {
      fprintf (stderr, "\nChild terminated with signal = %x \n", WTERMSIG (w));
      *status = 'X';
      return ((unsigned char) WTERMSIG (w));
    }

  fetch_inferior_registers (0);

  *status = 'T';
  return ((unsigned char) WSTOPSIG (w));
#endif
}

/* Resume execution of the inferior process.
   If STEP is nonzero, single-step it.
   If SIGNAL is nonzero, give it that signal.  */

void
myresume (step, signal)
     int step;
     int signal;
{

  /* 
   * Tahoe : FIXME : how do I tell Gambit to continue with/without a signal ?
   */

  if (step == 1)
    exec_num.dw[1] = 1;
  else
    exec_num.dw[1] = 0;

  gb_result = gb_exec (exec_num);
  print_gb_err_msg (gb_result);

#ifdef NONDEF
  errno = 0;
  ptrace (step ? PT_SINGLE : PT_CONTIN, inferior_pid, 1, signal, 0);
  if (errno)
    perror_with_name ("ptrace");
#endif
}


#if !defined (offsetof)
#define offsetof(TYPE, MEMBER) ((unsigned long) &((TYPE *)0)->MEMBER)
#endif

/* U_REGS_OFFSET is the offset of the registers within the u area.  */
#if !defined (U_REGS_OFFSET)
#define U_REGS_OFFSET \
  ptrace (PT_READ_U, inferior_pid, \
          (PTRACE_ARG3_TYPE) (offsetof (struct user, u_ar0)), 0) \
    - KERNEL_U_ADDR
#endif

unsigned int
register_addr (regno, blockend)
     int regno;
     int blockend;
{
  int addr;

  if (regno < 0 || regno >= ARCH_NUM_REGS)
    error ("Invalid register number %d.", regno);

  REGISTER_U_ADDR (addr, blockend, regno);

  return addr;
}

/* Fetch one register.  */

static void
fetch_register (regno)
     int regno;
{
  register unsigned int regaddr;
  char buf[MAX_REGISTER_RAW_SIZE];
  register int i;

  /* Offset of registers within the u area.  */
  unsigned int offset;

#ifdef NONDEF
  offset = U_REGS_OFFSET;

  regaddr = register_addr (regno, offset);
  for (i = 0; i < REGISTER_RAW_SIZE (regno); i += sizeof (int))
    {
      errno = 0;
      *(int *) &registers[regno * 4 + i] = ptrace (PT_RUREGS, inferior_pid,
					  (PTRACE_ARG3_TYPE) regaddr, 0, 0);
      regaddr += sizeof (int);
      if (errno != 0)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = strerror (errno);
	  char *msg = alloca (strlen (err) + 128);
	  sprintf (msg, "reading register %d: %s", regno, err);
	  error (msg);
	  goto error_exit;
	}
    }
error_exit:;
#endif

  /* Tahoe : general registers */
  if ((regno >= GR0_REGNUM) && (regno <= GRLAST_REGNUM))
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_GREG, regno - GR0_REGNUM,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
      print_gb_err_msg (gb_result);
    }

  /* Tahoe - float regs */
  if ((regno >= FR0_REGNUM) && (regno <= FRLAST_REGNUM))
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_FPREG, regno - FR0_REGNUM,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
    }

  /* NaTs */
  if ((regno >= NR0_REGNUM) && (regno <= NRLAST_REGNUM))
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_GREG_NaT, regno - NR0_REGNUM,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
    }

  /* Predicate regs */
  if ((regno >= PR0_REGNUM) && (regno <= PRLAST_REGNUM))
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_PREG, regno - PR0_REGNUM,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
    }

  /* Branch regs */
  if ((regno >= BR0_REGNUM) && (regno <= BRLAST_REGNUM))
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_BREG, regno - BR0_REGNUM,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
    }

  /* App regs */
  if ((regno >= AR0_REGNUM) && (regno <= ARLAST_REGNUM))
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_AREG, regno - AR0_REGNUM,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
    }


  /* Tahoe - PC */
  if (regno == PC_REGNUM)
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_IL, 0,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
      *((long long *) &registers[REGISTER_BYTE (regno)]) =
	*((long long *) &registers[REGISTER_BYTE (regno)]);
      print_gb_err_msg (gb_result);
      TPRINTF ("Read PC : 0x%016llx\n",
	       *((long long *) &registers[REGISTER_BYTE (regno)]));
    }

  /* Tahoe - PSR */
  if (regno == PSR_REGNUM)
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_PSR, 0,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
      print_gb_err_msg (gb_result);
      TPRINTF ("Read PSR : 0x%016llx\n",
	       *((long long *) &registers[REGISTER_BYTE (regno)]));
    }

  /* Tahoe - CFM */
  if (regno == CFM_REGNUM)
    {
      gb_result = gb_obj (GB_OBJ_GET, GB_OBJ_CFM, 0,
			  (char *) &(registers[REGISTER_BYTE (regno)]),
			  REGISTER_RAW_SIZE (regno));
      print_gb_err_msg (gb_result);
      TPRINTF ("Read CFM : 0x%016llx\n",
	       *((long long *) &registers[REGISTER_BYTE (regno)]));
    }
}

/* Fetch all registers, or just one, from the child process.  */

void
fetch_inferior_registers (regno)
     int regno;
{
  if (regno == -1 || regno == 0)
    for (regno = 0; regno < NUM_REGS; regno++)
      fetch_register (regno);
  else
    fetch_register (regno);
}

/* Store our register values back into the inferior.
   If REGNO is -1, do this for all registers.
   Otherwise, REGNO specifies which register (so we can save time).  */

void
store_inferior_registers (regno)
     int regno;
{
  register unsigned int regaddr;
  char buf[80];
  extern char *registers;
  register int i;
  unsigned int offset = U_REGS_OFFSET;
  int scratch;

#ifdef NONDEF
  if (regno >= 0)
    {
      if (CANNOT_STORE_REGISTER (regno))
	return;
      regaddr = register_addr (regno, offset);
      errno = 0;
      if (regno == PCOQ_HEAD_REGNUM || regno == PCOQ_TAIL_REGNUM)
	{
	  scratch = *(int *) &registers[REGISTER_BYTE (regno)] | 0x3;
	  ptrace (PT_WUREGS, inferior_pid, (PTRACE_ARG3_TYPE) regaddr,
		  scratch, 0);
	  if (errno != 0)
	    {
	      /* Error, even if attached.  Failing to write these two
	         registers is pretty serious.  */
	      sprintf (buf, "writing register number %d", regno);
	      perror_with_name (buf);
	    }
	}
      else
	for (i = 0; i < REGISTER_RAW_SIZE (regno); i += sizeof (int))
	  {
	    errno = 0;
	    ptrace (PT_WUREGS, inferior_pid, (PTRACE_ARG3_TYPE) regaddr,
		    *(int *) &registers[REGISTER_BYTE (regno) + i], 0);
	    if (errno != 0)
	      {
		/* Warning, not error, in case we are attached; sometimes the
		   kernel doesn't let us at the registers.  */
		char *err = strerror (errno);
		char *msg = alloca (strlen (err) + 128);
		sprintf (msg, "writing register %d: %s",
			 regno, err);
		error (msg);
		return;
	      }
	    regaddr += sizeof (int);
	  }
    }
  else
    for (regno = 0; regno < NUM_REGS; regno++)
      store_inferior_registers (regno);
#endif

  if (regno >= 0)
    {
      if (CANNOT_STORE_REGISTER (regno))
	return;

      /* Tahoe : general registers */
      if ((regno >= GR0_REGNUM) && (regno <= GRLAST_REGNUM))
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_GREG, regno - GR0_REGNUM,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	  print_gb_err_msg (gb_result);
	}

      /* Tahoe - float regs */
      if ((regno >= FR0_REGNUM) && (regno <= FRLAST_REGNUM))
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_FPREG, regno - FR0_REGNUM,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	}

      /* NaTs */
      if ((regno >= NR0_REGNUM) && (regno <= NRLAST_REGNUM))
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_GREG_NaT, regno - NR0_REGNUM,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	}

      /* Predicate regs */
      if ((regno >= PR0_REGNUM) && (regno <= PRLAST_REGNUM))
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_PREG, regno - PR0_REGNUM,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	}

      /* Branch regs */
      if ((regno >= BR0_REGNUM) && (regno <= BRLAST_REGNUM))
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_BREG, regno - BR0_REGNUM,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	}

      /* App regs */
      if (regno == (AR0_REGNUM + 40) || regno == (AR0_REGNUM + 64))
	{
	  CORE_ADDR new_value;

	  assert (REGISTER_RAW_SIZE (regno) == sizeof (CORE_ADDR));
	  memcpy ((char*) &new_value, &(registers[REGISTER_BYTE (regno)]), 
		  REGISTER_RAW_SIZE (regno));
	  if (regno == (AR0_REGNUM + 64))
	    {
	      /* Must not set reserved fields 61-58 and 51-38 */
	      new_value = new_value & AR64_RESERVED_MASK;
	    }
	  else 
	    {
	      /* Bits 63-58 are reserved in AR40 = Floating-point Status 
		 Register.  Also bit 12 is td (Traps disabled) of FPSR.sf0, 
		 which is reserved.
		 */
	      new_value = new_value & AR40_RESERVED_MASK;
	    }
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_AREG, regno - AR0_REGNUM,
			      (char *) &new_value,
			      REGISTER_RAW_SIZE (regno));
	}


      /* Tahoe - PC */
      if (regno == PC_REGNUM)
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_IL, 0,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	  print_gb_err_msg (gb_result);
	  TPRINTF ("Wrote PC : 0x%016llx\n",
		   *((long long *) &registers[REGISTER_BYTE (regno)]));
	}

      /* Tahoe - PSR */
      if (regno == PSR_REGNUM)
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_PSR, 0,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	  print_gb_err_msg (gb_result);
	  TPRINTF ("Wrote PSR : 0x%016llx\n",
		   *((long long *) &registers[REGISTER_BYTE (regno)]));
	}

      /* Tahoe - CFM */
      if (regno == CFM_REGNUM)
	{
	  gb_result = gb_obj (GB_OBJ_SET, GB_OBJ_CFM, 0,
			      (char *) &(registers[REGISTER_BYTE (regno)]),
			      REGISTER_RAW_SIZE (regno));
	  print_gb_err_msg (gb_result);
	  TPRINTF ("Wrote CFM : 0x%016llx\n",
		   *((long long *) &registers[REGISTER_BYTE (regno)]));
	}
    }
  else
    {
      for (regno = 0; regno < NUM_REGS; regno++)
	store_inferior_registers (regno);
    }
}

/* NOTE! I tried using PTRACE_READDATA, etc., to read and write memory
   in the NEW_SUN_PTRACE case.
   It ought to be straightforward.  But it appears that writing did
   not write the data that I specified.  I cannot understand where
   it got the data that it actually did write.  */

/* Copy LEN bytes from inferior's memory starting at MEMADDR
   to debugger memory starting at MYADDR.  */

int
read_inferior_memory (memaddr, myaddr, len)
     CORE_ADDR memaddr;
     char *myaddr;
     int len;
{
  register int i;

  /* Round starting address down to long-long-word (8 bytes) boundary.  */
  CORE_ADDR addr = memaddr & (-((LONGEST) sizeof (long long)));

  /* 
   * Round ending address up; get number of long-long-words (8-byte chunks) 
   * that makes.  
   */
  register int count
  = (((memaddr + len) - addr) + sizeof (long long) - 1) / sizeof (long long);

  /* Allocate buffer of that many long-long-words.  */
  long long *buffer = (long long *) alloca (count * sizeof (long long));

  /* Gambit stuff */
  gb_t gb_local_result;
  MEMORY_ADDRESS mem_parm;
  CORE_ADDR loc_addr;
  int status;

  TPRINTF ("Asked to read %016llx, start at %016llx, will read %d times\n",
	   memaddr, addr, count);

  /* Read all the long-long-words */
  for (i = 0; i < count; i++, addr += sizeof (long long))
    {
      memcpy (&mem_parm.addr, &addr, sizeof (addr));
      mem_parm.type = GB_MEM_TYPE_VIRTUAL;
      gb_local_result = gb_mem (GB_MEM_DATA_READ, mem_parm, &buffer[i],
				sizeof (long long));
      TPRINTF ("Reading %016llx : %016llx\n", addr, buffer[i]);
      {
	int j;
	unsigned char *ptr = (unsigned char *) &buffer[i];

	for (j = 0; j < 8; j++)
	  TPRINTF ("    reading %016llx + %d : %x\n", addr, j, ptr[j]);

      }
      print_gb_err_msg (gb_local_result);
      status = (gb_local_result.status == GB_STATUS_OK ? 0 : -1);
      if (status != 0)
	return status;
    }

  /* Copy appropriate bytes out of the buffer.  */
  memcpy (myaddr, (char *) buffer + (memaddr & (sizeof (long long) - 1)),
	  len);
  return 0;
}


/* Copy LEN bytes of data from debugger memory at MYADDR
   to inferior's memory at MEMADDR.
   On failure (cannot write the inferior)
   returns the value of errno.  */

int
write_inferior_memory (memaddr, myaddr, len)
     CORE_ADDR memaddr;
     char *myaddr;
     int len;
{
  register int i;
  /* Round starting address down to long-long-word (8 bytes) boundary.  */
  CORE_ADDR addr = memaddr & (-((LONGEST) sizeof (long long)));

  /*
   * Round ending address up; get number of long-long-words (8-byte chunks)
   * that makes.
   */
  register int count
  = (((memaddr + len) - addr) + sizeof (long long) - 1) / sizeof (long long);

  /* Allocate buffer of that many long-long-words.  */
  long long *buffer = (long long *) alloca (count * sizeof (long long));

  extern int errno;
  gb_t gb_local_result;
  MEMORY_ADDRESS mem_parm;
  CORE_ADDR loc_addr;
  int status;


  TPRINTF ("Asked to write %016llx, start at %016llx, will write %d times\n",
	   memaddr, addr, count);

  {
    int j;

    for (j = 0; j < len; j++)
      {
	TPRINTF ("    asked-to-write-byte %d : %x\n", j,
		 (unsigned char) myaddr[j]);
      }
  }

  /* Fill start and end extra bytes of buffer with existing memory data.  */
  TPRINTF ("Reading initial bytes at %016llx\n", addr);

  memcpy (&mem_parm.addr, &addr, sizeof (addr));
  mem_parm.type = GB_MEM_TYPE_VIRTUAL;
  gb_local_result = gb_mem (GB_MEM_DATA_READ, mem_parm, &buffer[0],
			    sizeof (long long));
  print_gb_err_msg (gb_local_result);
  status = (gb_local_result.status == GB_STATUS_OK ? 0 : -1);
  if (status != 0)
    return status;

#ifdef NONDEF
  buffer[0] = ptrace (1, inferior_pid, addr, 0, 0);
#endif

  if (count > 1)
    {
      loc_addr = addr + (count - 1) * sizeof (long long);

      TPRINTF ("Reading last bytes at %016llx\n", loc_addr);

      memcpy (&mem_parm.addr, &loc_addr, sizeof (loc_addr));
      mem_parm.type = GB_MEM_TYPE_VIRTUAL;
      gb_local_result = gb_mem (GB_MEM_DATA_WRITE, mem_parm,
				&buffer[count - 1], sizeof (long long));
      print_gb_err_msg (gb_local_result);
      status = (gb_local_result.status == GB_STATUS_OK ? 0 : -1);
      if (status != 0)
	return status;
    }

#ifdef NONDEF
  if (count > 1)
    {
      buffer[count - 1]
	= ptrace (1, inferior_pid,
		  addr + (count - 1) * sizeof (int), 0, 0);
    }
#endif

  /* Copy data to be written over corresponding part of buffer */

  memcpy ((char *) buffer + (memaddr & (sizeof (long long) - 1)), myaddr, len);

  /* Write the entire buffer.  */

  for (i = 0; i < count; i++, addr += sizeof (long long))
    {
      TPRINTF ("Writing bytes at %016llx (%d) - %016llx\n",
	       addr, i, buffer[i]);
      {
	int j;
	unsigned char *ptr = (unsigned char *) &buffer[i];

	for (j = 0; j < 8; j++)
	  TPRINTF ("    writing %016llx + %d : %x\n", addr, j, ptr[j]);

      }
      memcpy (&mem_parm.addr, &addr, sizeof (addr));
      mem_parm.type = GB_MEM_TYPE_VIRTUAL;

      gb_local_result = gb_mem (GB_MEM_DATA_WRITE, mem_parm,
				&buffer[i], sizeof (long long));
      print_gb_err_msg (gb_local_result);
      status = (gb_local_result.status == GB_STATUS_OK ? 0 : -1);
      if (status != 0)
	return status;


#ifdef NONDEF
      errno = 0;
      ptrace (4, inferior_pid, addr, buffer[i], 0);
      if (errno)
	return errno;
#endif
    }


  return 0;
}
/* Added for supporting breakpoint commands in WDB-gambit. 
   Called after receiving a packet of the form: <CMD> <ADDR> <MASK> <FLAGS>
   Where <CMD> is one of: `B' to set, or `b' to clear a breakpoint.
   <ADDR> is the address of the breakpoint.  <MASK> is a don't care mask 
   for addresses.
   <FLAGS> is any combination of `r', `w', or `f' for read/write/or fetch.  
 */
int
inferior_common_breakpoint (cmd, addr, mask, break_flags)
     int cmd;
     CORE_ADDR addr;
     CORE_ADDR mask;
     char *break_flags;
{
  gb_t gb_local_result;
  EX86_vaddr_t gb_addr;

  if (addr == (CORE_ADDR) 0)
    error ("NULL breakpoint address\n");

  if (strcmp (break_flags, "f") != 0)
    error ("Un-implemented breakpoint kind %s\n", break_flags);

  /* Copy parameters to Gambit's "clumsy" parameter types. */
  memcpy (&gb_addr, &addr, sizeof (addr));
  addr &= mask;
  switch (cmd)
    {
    case 'B':
      {
	U64 gb_attr =
	{GB_BKPT_TYPE_FETCH, 0};
	TPRINTF ("Asked to set a  %s breakpoint at %016llx\n",
		 break_flags, addr);
	/* Ask Gambit to set a breakpoint at addr. */
	gb_local_result = gb_brkp (GB_BKPT_SET, gb_addr, gb_attr);
	break;
      }
    case 'b':
      {
	U64 gb_attr =
	{GB_BKPT_TYPE_FETCH, 0};
	TPRINTF ("Asked to remove a  %s breakpoint at %016llx\n",
		 break_flags, addr);
	/* Ask Gambit to remove a breakpoint at addr. */
	gb_local_result = gb_brkp (GB_BKPT_REMOVE, gb_addr, gb_attr);
	break;
      }
    default:
      error ("Unrecognized breakpoint command %c\n", cmd);
    }
  /* Success? */
  print_gb_err_msg (gb_local_result);
  return (gb_local_result.status == GB_STATUS_OK ? 0 : -1);
}
/* Added for supporting the unwind library in WDB-gambit. 
   Called after receiving a packet of the form: 'F'
   Returns 0 on success; -1 otherwise.
 */
int
inferior_flush_RSE ()
{
  gb_t gb_local_result;

  gb_local_result = gb_flush_rs ();
  /* Success? */
  print_gb_err_msg (gb_local_result);
  return (gb_local_result.status == GB_STATUS_OK ? 0 : -1);
}

void
initialize ()
{
  inferior_pid = 0;
}

int
have_inferior_p ()
{
  return inferior_pid != 0;
}

/*
 * Tahoe. Dummy routines to satisfy Gambit.
 */
int
gb_dbg_symbol_name_to_addr ()
{
  return 0;
}

int
gb_dbg_addr_to_symbol_name ()
{
  return 0;
}

int
gb_dbg_loadsymbols ()
{
  return 0;
}

int
gb_dbg_process_exit ()
{
  return 0;
}

int
gb_dbg_load_process_symbols ()
{
  return 0;
}

/*harish_tahoe_investigation:
 * More dummy routines needed for the April 98 version 
 * of the Gambit library.
 */
int 
msl_start ()
{
  error ("Called dummy stub msl_start():low-ia64g.c\n");
}
int 
dis64 ()
{
  error ("Called dummy stub dis64():low-ia64g.c\n");
}

/* When we use the dbgshl gambit option, it will call this routine.  This
   isn't really what we have in mind, so we just return and ignore the call.
   We will be setting a gambit breakpoint on top of the _asm_break break
   instruction to get our notification.
 */
int 
load_shl_symbols ()
{
  return 0;
}
int 
msl_label_addr ()
{
  error ("Called dummy stub msl_label_addr():low-ia64g.c\n");
}
int 
msl_done ()
{
  error ("Called dummy stub msl_done():low-ia64g.c\n");
}
/* int fstat64(){ error("Called dummy stub fstat64():low-ia64g.c\n"); } */
int 
msl_label_name ()
{
  error ("Called dummy stub msl_label_name():low-ia64g.c\n");
}
int 
msl_load_symbols ()
{
  error ("Called dummy stub msl_load_symbols():low-ia64g.c\n");
}

/* swizzle
   input_addr is copied to the result.  
   If the current program is ilp32, then bits 32 and 33 of input_addr are
   copied to bits  1 and 2 of the result.  (high order bit is numbered zero 
   here).
 */

CORE_ADDR
swizzle (input_addr)
     CORE_ADDR input_addr;
{
  CORE_ADDR result;
  if (is_ilp32)
    {
      result = (input_addr & ~0x6000000000000000LL)
	| ((input_addr & 0xc0000000) << 31);
    }
  else
    {
      result = input_addr;
    }
  return result;
}				/* end swizzle */

/* unswizzle
   input_addr is copied to the result.  
   If the current program is ilp32, then only the low 32 bits are copied,
   otherwise, the input is just copied to the output.
 */

CORE_ADDR
unswizzle (input_addr)
     CORE_ADDR input_addr;
{
  CORE_ADDR result;

  if (is_ilp32)
    {
      result = input_addr & 0xFFFFFFFFLL;
    }
  else
    {
      result = input_addr;
    }
  return result;
}				/* end unswizzle */

void
initialize_low ()
{
}
