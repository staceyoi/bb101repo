
#define NONDEF

/* Low level interface to Gambit simulator, for the remote server for GDB.
   Copyright (C) 1995 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* 
 * Tahoe. These 2 headers are needed for :
 *     a. Gambit (igb_api.h)
 *     b. to be able to read initial PC (from ELF executable - libelf.h)
 * They are placed early to avoid redifnition problems.
 */

#include "defs.h"
int read_inferior_memory PARAMS ((CORE_ADDR, char *, int));
int read_inferior_ideal_memory PARAMS ((CORE_ADDR, char *, int));
#include <sys/wait.h>
#include "frame.h"
#include "inferior.h"

#include <stdio.h>
#include <sys/param.h>
#include <sys/dir.h>
#include <sys/user.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sgtty.h>
#include <fcntl.h>
#include <stdlib.h>
#include <alloca.h>
#include <string.h>
#include <libelf.h>
#include <unistd.h>

#include <elf_em.h>
#include <assert.h>
#include <sys/scall_define.h>

/* 
 * Tahoe. This is to receive argv[0] from main.c. It is needed for some 
 * reason to initiallize Gambit.
 */
#include "server.h"

int is_ilp32 = 0;		/* If the flag bit EF_IA_64_ABI64 is not set, this will
				   be set to TRUE.  EM-32 programs do set this bit.
				 */

/***************Begin MY defs*********************/
int quit_flag = 0;
char registers[REGISTER_BYTES];

/* Index within `registers' of the first byte of the space for
   register N.  */


char buf2[MAX_REGISTER_RAW_SIZE];

#define RSE_RDWR_SIZE	8
#define SEM_TALK	(1)
#define SEM_LISTEN	(0)
#define TTRACE_ARG_TYPE uint64_t
#define TT_NIL ((TTRACE_ARG_TYPE) TT_NULLARG)

typedef struct {
  int		have_read_rse_info;  /* Set to FALSE before each call to 
					ttrace_wait */
  CORE_ADDR	ar_bsp;
  CORE_ADDR	ar_bspstore;
  CORE_ADDR	cfm;
  CORE_ADDR	rnat;
  CORE_ADDR	bsp_stored_nat;
  CORE_ADDR 	bsp_stored_nat_addr; /* -1 if bsp_stored_nat is undefined */
} rse_state_info_t;

rse_state_info_t rse_state_info;

#define GET_RSE_STATE_INFO  if (!rse_state_info.have_read_rse_info) \
    get_rse_state_info ();

/* When bits 8:3 of BPSTORE are all one, RNAT is stored.  Given the BS
   address of a regisger, it's RNAT collection is at that address oring
   in 0x1f8.
   */

#define NAT_COLLECTION_ADDR(bs_addr) ((bs_addr) | 0x1f8)

/* Bits 6:0 of CFM_REGNUM is sof - size of frame - the number of stacked
   registers seen by the procedure.
   */
#define SIZE_OF_FRAME	(rse_state_info.cfm & 0x7f)

  /* From 11.5.2 of EAS 2.6, p 11-8, "RNAT{x} corresponds to the register
     saved at concatenate (BSPSTORE{63:9}, x(5:0), 0{2:0}).  So take 8:3 of
     gr_bs_addr, shift nat_collection right by that many bits and AND the
     result with 1.  That is the desired NaT bit/byte.
     */
#define NAT_BITPOS(bs_addr) (((bs_addr) >> 3) & 0x3f)
  
/***************End MY defs*********************/

#include <sys/ttrace.h>

#include "../ia64-regs.h"

extern char **environ;
extern int errno;
extern int inferior_pid;
void quit (), perror_with_name ();


#define BAD_VALUE	-1

/***************Begin Forward Declarations *******/
static void fetch_register PARAMS ((int));

int read_inferior_memory PARAMS ((CORE_ADDR, char *, int));

CORE_ADDR swizzle PARAMS ((CORE_ADDR));

CORE_ADDR unswizzle PARAMS ((CORE_ADDR));

static void get_rse_state_info PARAMS ((void));
/***************End   Forward Declarations *******/

/***************Begin Static Declarations *******/

typedef struct {
  int  parent_channel[2];  /* Parent "talks" to [1], child "listens" to [0] */
  int  child_channel[2];   /* Child "talks" to [1], parent "listens" to [0] */
} startup_semaphore_t;

static startup_semaphore_t startup_semaphore;

static ttstate_t	thread_state;

/***************End   Static Declarations *******/

static int fr0_value[3] = { 0x00000000, 0x00000000, 0x00000000 };
static int fr1_value[3] = { 0x0000ffff, 0x80000000, 0x00000000 };
static int gr0_value[2] = { 0x00000000, 0x00000000 };

/* This function simply calls ttrace with the given arguments.  
 * It exists so that all calls to ttrace are isolated.  All
 * parameters should be as specified by "man 2 ttrace".
 *
 * No other "raw" calls to ttrace should exist in this module.
 */
static int
call_real_ttrace (request, pid, tid, addr, data, addr2)
     ttreq_t request;
     pid_t pid;
     lwpid_t tid;
     TTRACE_ARG_TYPE addr, data, addr2;
{
  int tt_status;

  errno = 0;
  TFPRINTF (stderr, "call ttrace with request %d\n", request);
  if (trace_flag) 
    fflush (stdout);
  tt_status = ttrace (request, PIDGET (pid), PIDGET (tid), addr, data, addr2);

#ifdef THREAD_DEBUG
  if (errno)
    {
      /* Don't bother for a known benign error: if you ask for the
       * first thread state, but there is only one thread and it's
       * not stopped, ttrace complains.
       *
       * We have this inside the #ifdef because our caller will do
       * this check for real.
       */
      if (request != TT_PROC_GET_FIRST_LWP_STATE
	  || errno != EPROTO)
	{
	  if (debug_on)
	    printf ("TT fail for %s, with pid %d, tid %d, status %d \n",
		    get_printable_name_of_ttrace_request (request),
		    pid, tid, tt_status);
	}
    }
#endif

#if 0
  /* ??rehrauer: It would probably be most robust to catch and report
   * failed requests here.  However, some clients of this interface
   * seem to expect to catch & deal with them, so we'd best not.
   */
  if (errno)
    {
      strcpy (reason_for_failure, "ttrace (");
      strcat (reason_for_failure, get_printable_name_of_ttrace_request (request));
      strcat (reason_for_failure, ")");
      printf ("ttrace error, errno = %d\n", errno);
      perror_with_name (reason_for_failure);
    }
#endif

  return tt_status;
}

/* This function simply calls ttrace_wait with the given arguments.  
 * It exists so that all calls to ttrace_wait are isolated.
 *
 * No "raw" calls to ttrace_wait should exist elsewhere.
 */
static int
call_real_ttrace_wait (pid, tid, option, tsp, tsp_size)
     int pid;
     lwpid_t tid;
     ttwopt_t option;
     ttstate_t *tsp;
     size_t tsp_size;
{
  int ttw_status;

  errno = 0;
  TFPRINTF (stderr, "About to call ttrace_wait\n");
  if (trace_flag) 
    fflush (stdout);
  ttw_status = ttrace_wait (pid, tid, option, tsp, tsp_size);

#ifdef THREAD_DEBUG
  if (ttw_status != 1)
    {
      if (debug_on)
	fprintf (stderr, "TW fail with pid %d, tid %d \n", pid, tid);
    }
#endif
  TFPRINTF (stderr, "after call to ttrace_wait, status is %d\n", ttw_status);
  TFPRINTF (stderr, "tts_event is 0x%x\n", tsp->tts_event);

  /* Here is where we do anything we want to do after every call to
     ttrace_wait.

     o Set rse_state_info.have_read_rse_info to FALSE
     */
  
  rse_state_info.have_read_rse_info = FALSE;
  return ttw_status;
}



#define TT_NIL ((TTRACE_ARG_TYPE) TT_NULLARG)
#define TT_USE_CURRENT_PC ((TTRACE_ARG_TYPE) TT_NOPC)

/* A process may have one or more kernel threads, of which all or
   none may be stopped.  This function returns the ID of the first
   kernel thread in a stopped state, or 0 if none are stopped.
 */
static lwpid_t
get_first_tid (void)
{
  ttstate_t thread_state;
  int tt_status;

  tt_status = call_real_ttrace (
				 TT_PROC_GET_FIRST_LWP_STATE,
				 (pid_t) inferior_pid,
				 (lwpid_t) TT_NIL,
				 (TTRACE_ARG_TYPE) &thread_state,
				 (TTRACE_ARG_TYPE) sizeof (thread_state),
				 TT_NIL);

  if (errno)
    {
      if (errno == EPROTO)
	{
	  /* This is an error we can handle: there isn't any stopped
	   * thread.  This happens when we're re-starting the application
	   * and it has only one thread.  GET_NEXT handles the case of
	   * no more stopped threads well; GET_FIRST doesn't.  (A ttrace
	   * "feature".)
	   */
	  tt_status = 1;
	  errno = 0;
	  return 0;
	}
      else
	perror_with_name ("ttrace");
    }

  if (tt_status < 0)
    /* Failed somehow.
     */
    return 0;

  return thread_state.tts_lwpid;
}

/* Read in the RSE state informatin that we need to read stacked registers
   and the RSE backing store that hasn't been flushed.  Set
   rse_state_info.have_read_rse_info to TRUE.
   */

static void 
get_rse_state_info (void)
{
  int tt_status;

  fetch_register (AR0_REGNUM + 17); /* AR17 is BSP */
  memcpy (&rse_state_info.ar_bsp, 
	  &registers[REGISTER_BYTE (AR0_REGNUM + 17)],
          REGISTER_SIZE);
  fetch_register (AR0_REGNUM + 18); /* AR18 is BSPSTORE */
  memcpy (&rse_state_info.ar_bspstore, 
	  &registers[REGISTER_BYTE (AR0_REGNUM + 18)],
          REGISTER_SIZE);
  fetch_register (CFM_REGNUM); 
  memcpy (&rse_state_info.cfm, 
	  &registers[REGISTER_BYTE (CFM_REGNUM)],
          REGISTER_SIZE);
  rse_state_info.bsp_stored_nat_addr = -1;

  /* Getting the final NAT collection is a special case of
     TT_LWP_RDRSEBS, addr == ar.bsp | 0x1f8
     */
  tt_status = call_real_ttrace ( TT_LWP_RDRSEBS,
				 (pid_t) inferior_pid,
				 (lwpid_t) get_first_tid (),
				 (TTRACE_ARG_TYPE) 
				   NAT_COLLECTION_ADDR(rse_state_info.ar_bsp),
				 (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
				 (TTRACE_ARG_TYPE) &rse_state_info.rnat);
  rse_state_info.have_read_rse_info = TRUE;
} /* end get_rse_state_info */

/*
 * Tahoe. Provide STDIO functions for Gambit simulator.
 * These entry points are passed into gb_init().
 */
static int
gb_stdin (char *buf, int count)
{
  return read (fileno (stdin), buf, count);
}

static int
gb_stdout (char *buf, int count)
{
  return write (fileno (stdout), buf, count);
}

static int
gb_stderr (char *buf, int count)
{
  return write (fileno (stderr), buf, count);
}

/* Start an inferior process and return its pid.
   ALLARGS is a vector of program-name and args.
   ENV is the environment vector to pass.  */

int
create_inferior (program, allargs)
     char *program;
     char **allargs;
{
  int pid;
  /* 
   * Tahoe.
   * init_IP : initial IP (PC) pre-loaded when we start up executable
   * config_parm : used to configure Gambit
   * local_result : result of Gambit operations
   */
  int 			arg_count;
  Elf64_Ehdr 		elf_header;
  int 			filedes;
  unsigned long long 	init_IP = 0;
  int 			i;
  unsigned long long 	long_long_addr;
  ttevent_t 		notifiable_events;
  int			status;
  uint64_t 		tc_magic_child = TT_VERSION;
  uint64_t 		tc_magic_parent = 0;

  {
    /* To debug, set do_debug to 1 and then attach to the server 
       and manually set do_debug to zero. 
     */
    int do_debug = 0;
    while (do_debug)
      ;
  }

  TPRINTF ("Tahoe executable : %s\n", program);

  filedes = open (program, O_RDONLY);
  if (filedes == BAD_VALUE)
    {
      error ("Unable to open program file %s.", program);
    }

  /* Read in the elf header to use below in setting is_ilp32 */

  if (read (filedes, &elf_header, sizeof(Elf64_Ehdr)) != sizeof(Elf64_Ehdr))
    {
      error ("Unable to read elf header of %s.", program);
    }
  lseek (filedes, 0, SEEK_SET);

  if (   elf_header.e_ident[EI_CLASS] != ELFCLASS64
      || elf_header.e_ident[EI_DATA] != ELFDATA2MSB
      || elf_header.e_machine != EM_IA_64)
    {
      error ("Unable to unexpected elf header in %s.", program);
    }

  is_ilp32 = ((elf_header.e_flags & EF_EM_ABI64) == 0);

  /* Create the semaphore used to coordinate the startup between the child
     and the parent.
     */
  
  status = pipe (startup_semaphore.parent_channel);
  if (status < 0)
    {
      error ("Unable to create parent pipe for startup semaphore.");
    }
  status = pipe (startup_semaphore.child_channel);
  if (status < 0)
    {
      error ("Unable to create child pipe for startup semaphore.");
    }

#ifdef NONDEF
  pid = fork ();
  if (pid < 0)
    perror_with_name ("fork");

  if (pid == 0)
    {
      ttstate_t thread_state;

      /* ptrace (PT_SETTRC, 0, 0, 0, 0); */
      status = call_real_ttrace (TT_PROC_SETTRC, 
			         (pid_t) 0,
			         (lwpid_t) 0, 
			         0,
			         (TTRACE_ARG_TYPE) TT_VERSION,
			         0);
      if (status < 0)
	error ("child: failed ttrace TT_PROC_SETTRC, errno is %d", errno);
      
      /* Notify the parent that we're potentially ready to be traced */
      write (startup_semaphore.child_channel[SEM_TALK],
	     &tc_magic_child,
	     sizeof (tc_magic_child));

      /* Wait for acknowledgement from the parent. */
      read (startup_semaphore.parent_channel[SEM_LISTEN],
	    &tc_magic_parent,
	    sizeof (tc_magic_parent));
      
      /* Discard our copy of the semaphore. */
      (void) close (startup_semaphore.parent_channel[SEM_LISTEN]);
      (void) close (startup_semaphore.parent_channel[SEM_TALK]);
      (void) close (startup_semaphore.child_channel[SEM_LISTEN]);
      (void) close (startup_semaphore.child_channel[SEM_TALK]);

      /* Exec the program */
      execv (program, allargs);

      fprintf (stderr, "Cannot exec %s: %s.\n", program,
	       strerror (errno));
      fflush (stderr);
      _exit (0177);
    }

  /* Wait for the child to tell us that it has forked. */
  read (startup_semaphore.child_channel[SEM_LISTEN],
	&tc_magic_child,
	sizeof(tc_magic_child));
  
  /* We can now set the child's ttrace event mask. */
  sigemptyset (&notifiable_events.tte_signals);
  notifiable_events.tte_opts = TTEO_NONE;
  notifiable_events.tte_opts |= TTEO_PROC_INHERIT;
  notifiable_events.tte_opts |= TTEO_NOSTRCCHLD; /* Do not trace child */

  notifiable_events.tte_events  = TTEVT_DEFAULT;
  notifiable_events.tte_events |= TTEVT_SIGNAL;
  notifiable_events.tte_events |= TTEVT_EXIT;
  notifiable_events.tte_events |= TTEVT_EXEC;
#if 0
/* Ignore certain events for the bootstrap debugger */
  notifiable_events.tte_events |= TTEVT_FORK;
#endif
  notifiable_events.tte_events |= TTEVT_VFORK;
  notifiable_events.tte_events |= TTEVT_LWP_CREATE;
  notifiable_events.tte_events |= TTEVT_LWP_EXIT;
  notifiable_events.tte_events |= TTEVT_LWP_TERMINATE;
  notifiable_events.tte_events |= TTEVT_BPT_SSTEP;

  status = call_real_ttrace (TT_PROC_SET_EVENT_MASK,
			     pid,
			     (lwpid_t) TT_NIL,
			     (TTRACE_ARG_TYPE) & notifiable_events,
			     (TTRACE_ARG_TYPE) sizeof (notifiable_events),
			     TT_NIL);
  if (status < 0)
	error ("parent: failed ttrace TT_PROC_SET_EVENT_MASK, errno is %d", 
		errno);
  
  write (startup_semaphore.parent_channel[SEM_TALK],
	 &tc_magic_parent,
	 sizeof (tc_magic_parent));

  /* Discard our copy of the semaphore. */
  (void) close (startup_semaphore.parent_channel[SEM_LISTEN]);
  (void) close (startup_semaphore.parent_channel[SEM_TALK]);
  (void) close (startup_semaphore.child_channel[SEM_LISTEN]);
  (void) close (startup_semaphore.child_channel[SEM_TALK]);

  return pid;
#endif
}

/* Kill the inferior process.  Make us have no inferior.  */

void
kill_inferior ()
{
  int		status;

  if (inferior_pid == 0)
    return;

#ifdef NONDEF
  /* ptrace (PT_EXIT, inferior_pid, 0, 0, 0); */
  call_real_ttrace (TT_PROC_EXIT, 
		    (pid_t) inferior_pid,
		    (lwpid_t) 0, 
		    (uint64_t) 0,
		    (uint64_t) 0,
		    (uint64_t) 0);

  status = call_real_ttrace_wait (inferior_pid, 
				  0,  /* any thread */
				  TTRACE_WAITOK, 
				  &thread_state, 
				  sizeof(thread_state));
  if (status != 1)
    {
      fprintf (stderr, "Error from ttrace_wait after kill, errno = %d\n", 
	       errno);
    }
#endif
/*************inferior_died ();****VK**************/
}

/* Return nonzero if the given thread is still alive.  */
int
mythread_alive (pid)
     int pid;
{
  return 1;
}

/* Wait for process, returns status */

unsigned char
mywait (status)
     char *status;
{
  int pid;
  int w;
  unsigned char exit_status;
  int wait_status;

#ifdef NONDEF

  wait_status = call_real_ttrace_wait (inferior_pid, 
				       0,  /* any thread */
				       TTRACE_WAITOK, 
				       &thread_state, 
				       sizeof(thread_state));
  if (wait_status != 1)
    {
      fprintf (stderr, "Error from ttrace_wait, errno = %d\n", 
	       errno);
    }
  
  if (thread_state.tts_event & TTEVT_EXIT)
    {
      fprintf (stderr, "\nChild exited with retcode = %x \n", 
	       thread_state.tts_u.tts_exit.tts_exitcode);
      *status = 'W';
      return ((unsigned char) thread_state.tts_u.tts_exit.tts_exitcode);
    }
  else if (thread_state.tts_event & TTEVT_SIGNAL)
    {
      fetch_inferior_registers (0);
      *status = 'T';
      return ((unsigned char) thread_state.tts_u.tts_signal.tts_signo);
    }

  /* I'm not sure this bit is correct.  particutlarly the return signal */
  fetch_inferior_registers (0);

  *status = 'T';
  return ((unsigned char) _SIGTRAP);
#endif
}

/* Resume execution of the inferior process.
   If STEP is nonzero, single-step it.
   If SIGNAL is nonzero, give it that signal.  */

void
myresume (step, signal)
     int step;
     int signal;
{
  int		status;

#ifdef NONDEF
  errno = 0;
  /* ptrace (step ? PT_SINGLE : PT_CONTIN, inferior_pid, 1, signal, 0); */
  if (step)
    {
      status = call_real_ttrace (TT_LWP_SINGLE,
				 (pid_t) inferior_pid,
				 (lwpid_t) get_first_tid (), 
				 TT_USE_CURRENT_PC,
				 (uint64_t) signal,
				 (uint64_t) 0);
    }
  else
    {
      status = call_real_ttrace (TT_LWP_CONTINUE,
				 (pid_t) inferior_pid,
				 (lwpid_t) get_first_tid (),
				 TT_USE_CURRENT_PC,
				 (TTRACE_ARG_TYPE) signal,
				 TT_NIL);
    }

  if (status == -1 && errno)
    perror_with_name ("ttrace");
#endif
}


#if !defined (offsetof)
#define offsetof(TYPE, MEMBER) ((unsigned long) &((TYPE *)0)->MEMBER)
#endif

/* reg_bs_addr - given a GDB register number, return the backing store
   address of the register or zero if it does not have a BS address.
   */

static CORE_ADDR 
reg_bs_addr (regno)
  int regno;
{
  int 		last_reg;
  CORE_ADDR	last_reg_addr;
  CORE_ADDR	nat_addr;
  CORE_ADDR	reg_addr;

  if (regno < GR0_REGNUM + 32)
    return 0;  /* only stacked registers GR32 .. GR127 have GBS addresses */

  GET_RSE_STATE_INFO;

  last_reg = 31 + SIZE_OF_FRAME + GR0_REGNUM;
  if (regno > last_reg)
    return 0;

  /* The last register is 31 + SIZE_OF_FRAME.  It would be stored at
     BSP - REGISTER_SIZE, unless that is a NaT collection, in which case
     it would be the previous slot.
     */
  
  last_reg_addr = rse_state_info.ar_bsp - REGISTER_SIZE;
  if (last_reg_addr == NAT_COLLECTION_ADDR (last_reg_addr))
    last_reg_addr -= REGISTER_SIZE;
  reg_addr = last_reg_addr - ((last_reg - regno) * REGISTER_SIZE);

  /* We need to reduce reg_addr by one slot for every NaT collection stored
     between it and last_reg_addr.  nat_addr starts out as the first
     nat collection before last_reg_addr.
     */
    
  nat_addr = NAT_COLLECTION_ADDR (last_reg_addr) - (64 * REGISTER_SIZE);
  while (nat_addr >= reg_addr)
    {
      nat_addr -= (64 * REGISTER_SIZE);
      reg_addr -= REGISTER_SIZE;
    }
  return reg_addr;
} /* end reg_bs_addr */


unsigned int
register_addr (regno, blockend)
     int regno;
     int blockend;
{
  int addr;

  if (regno < 0 || regno >= ARCH_NUM_REGS)
    error ("Invalid register number %d.", regno);

  REGISTER_U_ADDR (addr, blockend, regno);

  return addr;
}

/* get_stacked_nat - Set the NaT byte in reg_buf_p for regno which is a
   NaT register NR32 .. NR127.
   If the register is not present, set the NaT byte to 1.

   Return 0 on success, otherwise errno.
   */

static int
get_stacked_nat (regno)
  int		regno;
{
  int		i;
  CORE_ADDR	gr_bs_addr;
  int		gr_regno;
  uint64_t	nat_collection;
  CORE_ADDR	nat_collection_addr;
  int		shift_size;
  int		status;

  GET_RSE_STATE_INFO;

  gr_regno = regno - NR0_REGNUM + GR0_REGNUM;
  if (!(gr_bs_addr = reg_bs_addr (gr_regno)))
    { /* The register isn't there.  Set its NaT byte to 1. */
      registers[REGISTER_BYTE (regno)] = 1;
      return 0;
    }
  nat_collection_addr = NAT_COLLECTION_ADDR (gr_bs_addr);
  if (nat_collection_addr > rse_state_info.ar_bsp)
    nat_collection = rse_state_info.rnat;
  else
    {
      if (nat_collection_addr == rse_state_info.bsp_stored_nat_addr)
	 nat_collection = rse_state_info.bsp_stored_nat;
      else
	{
	  status = read_inferior_ideal_memory (nat_collection_addr, 
					       (char*) &nat_collection,
	  				       REGISTER_SIZE);
	  if (status)
	    return status;
	  rse_state_info.bsp_stored_nat_addr = nat_collection_addr;
	  rse_state_info.bsp_stored_nat = nat_collection;
	}
    }

  /* From 11.5.2 of EAS 2.6, p 11-8, "RNAT{x} corresponds to the register
     saved at concatenate (BSPSTORE{63:9}, x(5:0), 0{2:0}).  So take 8:3 of
     gr_bs_addr, shift nat_collection right by that many bits and AND the
     result with 1.  That is the desired NaT bit/byte.
     */
  
  shift_size = NAT_BITPOS(gr_bs_addr);
  registers[REGISTER_BYTE (regno)] = (nat_collection >> shift_size) & 1;
  return 0;
} /* end get_stacked_nat */

/* get_stacked_register - copy the contents of gdb register regno to
   reg_buf_p (8 bytes).  Regno should be in the rang 32 .. 127.  If the
   register is not present, the buffer is zeroed out.

   Return 0 on success, otherwise errno.
   */

static int
get_stacked_register (regno, reg_buf_p)
  int		regno;
  char *	reg_buf_p;
{
  int		i;
  CORE_ADDR	reg_addr;
  int		status;

  GET_RSE_STATE_INFO;

  if (!(reg_addr = reg_bs_addr (regno)))
    { /* The register isn't there.  Zero out the buffer. */
      memset (reg_buf_p, 0, REGISTER_SIZE);
      return 0;
    }
  status = read_inferior_ideal_memory (reg_addr, reg_buf_p, REGISTER_SIZE);
  return status;
} /* end get_stacked_register */


/* Fetch one register.  */

static void
fetch_register (regno)
     int regno;
{
  char 		buf[MAX_REGISTER_RAW_SIZE];
  register int 	i;
  unsigned int 	regaddr;
  int		slot_nbr;
  int		status;

  struct {
    uint64_t 	pad63 		: 21;
    uint64_t 	slot_nbr	:  2;
    uint64_t 	pad40		:  9;
    uint64_t 	pad31		: 32;
  } psr;

  /* Offset of registers within the u area.  */
  unsigned int offset;

  regaddr = map_gdb_reg_to_ureg[regno];
  if (regno == NR0_REGNUM)
    { /* GR0 is not readable.  It's NAT byte is zero */
      registers[REGISTER_BYTE (regno)] = 0;  
      return;
    }
  if (regno >= NR0_REGNUM + 1 && regno <= NR0_REGNUM + 31)
    { /* Fetching NaT bits is weird.  Fetch 9 bytes of the GR, last byte is the
	 NaT.
	 */
      char reg_buffer[9];

      regaddr = map_gdb_reg_to_ureg[GR0_REGNUM + regno - NR0_REGNUM];
      status = call_real_ttrace (TT_LWP_RUREGS,
				 inferior_pid,
				 get_first_tid (),
				 (TTRACE_ARG_TYPE) regaddr,
				 9, /* GR plus NaT byte*/
				 (TTRACE_ARG_TYPE)
				   &reg_buffer[0]);
      if (status == -1 && errno)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = strerror (errno);
	  char *msg = alloca (strlen (err) + 128);

	  if (errno == EINVAL && regno >= NR0_REGNUM + 32 )
	    { /* After 32, the GR might not exist.  NAT is 0 */
	      registers[REGISTER_BYTE (regno)] = 0;
	      return;
	    }

	  sprintf (msg, "reading register %d: %s", regno, err);
	  fprintf (stderr, "gdb: %s\n", msg);

	  registers[REGISTER_BYTE (regno)] = 0;
	  return;
	}
      registers[REGISTER_BYTE (regno)] = reg_buffer[8];  /* transfer NaT byte */
      return;
    }
  else if (regno >= NR0_REGNUM + 32 && regno <= NRLAST_REGNUM)
    {
      status = get_stacked_nat (regno);
      if (status)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = strerror (status);
	  char *msg = alloca (strlen (err) + 128);

	  sprintf (msg, "reading register %d: %s", regno, err);
	  fprintf (stderr, "gdb: %s\n", msg);

	  registers[REGISTER_BYTE (regno)] = 0;
	  return;
	}
      return;
    }
  else if (regno == FR0_REGNUM)
    { /* FR0 always reads as zeroes */
      memcpy(&registers[REGISTER_BYTE (regno)], &fr0_value, 12);
      return;
    }
  else if (regno == FR0_REGNUM + 1)
    { /* FR1 reads as +1.0: 0000 ffff 8000 0000 0000 */
      memcpy(&registers[REGISTER_BYTE (regno)], &fr1_value, 12);
      return;
    }
  else if (regno >= FR0_REGNUM + 2 && regno <= FRLAST_REGNUM)
    { /* Gdb keeps FR's as 12-byte values, but ttrace uses a 16 byte buffer */
      char reg_buffer[16];

      status = call_real_ttrace (TT_LWP_RUREGS,
				 inferior_pid,
				 get_first_tid (),
				 (TTRACE_ARG_TYPE) regaddr,
				 16, /* FR's are written with 16 bytes */
				 (TTRACE_ARG_TYPE)
				   &reg_buffer[0]);
      if (status == -1 && errno)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = strerror (errno);
	  char *msg = alloca (strlen (err) + 128);

	  sprintf (msg, "reading register %d: %s", regno, err);
	  fprintf (stderr, "gdb: %s\n", msg);
	  return;
	}
      memcpy(&registers[REGISTER_BYTE (regno)], &reg_buffer[4], 12);
      return;
    }
  else if (regno == GR0_REGNUM)
    {
      memcpy(&registers[REGISTER_BYTE (regno)], &gr0_value, 16);
      return;
    }
  else if (regno > GR0_REGNUM + 31 && regno <= GRLAST_REGNUM)
    { /* Get a stacked register out of the RSE BS */
      get_stacked_register (regno, &registers[REGISTER_BYTE (regno)]);
      return;
    }
  else if (regno == PC_REGNUM)
    { /* We must compute an alternate regaddr for PC_REGNUM */
      if (thread_state.tts_flags & TTS_INSYSCALL)
	{
	  regaddr = __b6;
	  slot_nbr = 0;
	}
      else
	{
	  regaddr = __cr_iip;
	  /* The slot number is bits 42:41 of PSR which is copied to IPSR */
	  fetch_register (PSR_REGNUM);
	  memcpy (&psr, &registers[REGISTER_BYTE (PSR_REGNUM)], sizeof (psr));
	  slot_nbr = psr.slot_nbr;
	}

      /* Fall through now and do the usual read with the new regaddr */
    }

  status = call_real_ttrace (TT_LWP_RUREGS,
		             inferior_pid,
		             get_first_tid (),
		             (TTRACE_ARG_TYPE) regaddr,
		             8, /* All these writes are 8 bytes */
		             (TTRACE_ARG_TYPE)
		               &registers[REGISTER_BYTE (regno)]);
  if (status == -1 && errno)
    {
      /* Warning, not error, in case we are attached; sometimes the
	 kernel doesn't let us at the registers.  */
      char *err = strerror (errno);
      char *msg = alloca (strlen (err) + 128);

      if (   errno == EINVAL 
	  && (   (regno >= GR0_REGNUM + 32 && regno <= GR0_REGNUM + 127)
	      || (regno >= AR0_REGNUM && regno <= ARLAST_REGNUM)))
      { /* GR's after GR31 might not exist.  Treat them as zero */
	/* Only some of the AR registers can be read.  Treat the others
	   as zero.
	   */

	long long zero_value = 0;

	memcpy (&registers[REGISTER_BYTE (regno)], 
		(char*) &zero_value, 
		sizeof(long long));
      }
      else
	{
	  sprintf (msg, "reading register %d: %s", regno, err);
	  fprintf (stderr, "gdb: %s\n", msg);
	  return;
	}
    }
  if (regno == PC_REGNUM)
    { /* Add in the slot_nbr extracted above */

      registers[REGISTER_BYTE (PC_REGNUM) + 7] = 
	registers[REGISTER_BYTE (PC_REGNUM) + 7] + slot_nbr;
    }
  
} /* end fetch_register */

/* Fetch all registers, or just one, from the child process.  */

void
fetch_inferior_registers (regno)
     int regno;
{
  if (regno == -1 || regno == 0)
    for (regno = 0; regno < NUM_REGS; regno++)
      fetch_register (regno);
  else
    fetch_register (regno);
}

/* put_stacked_nat - Set the NaT bit of a stacked GR.  Regno should
   be a NaT register number NR32 .. NR127.
   If the register does not exist, do nothing and return 0.
   Nat_value should be 0 or 1.

   Return 0 on success, otherwise errno.
   */

static int
put_stacked_nat (regno, nat_value)
  int		regno;
  int		nat_value;
{
  uint64_t	bit_value;
  CORE_ADDR	gr_bs_addr;
  int		gr_regno;
  uint64_t	nat_collection;
  CORE_ADDR	nat_collection_addr;
  uint64_t	mask;
  int		shift_size;
  int		status;
  int 		tt_status;


  GET_RSE_STATE_INFO;

  nat_value = nat_value != 0;   /* Make sure nat_value is 0 or 1 */

  gr_regno = regno - NR0_REGNUM + GR0_REGNUM;
  if (!(gr_bs_addr = reg_bs_addr (gr_regno)))
    return 0; /* The register isn't there.  Do nothing. */

  nat_collection_addr = NAT_COLLECTION_ADDR (gr_bs_addr);
  if (nat_collection_addr > rse_state_info.ar_bsp)
    nat_collection = rse_state_info.rnat;
  else
    {
      if (nat_collection_addr == rse_state_info.bsp_stored_nat_addr)
	 nat_collection = rse_state_info.bsp_stored_nat;
      else
	{
	  status = read_inferior_ideal_memory (nat_collection_addr, 
					       (char*) &nat_collection,
	  				       REGISTER_SIZE);
	  if (status)
	    return status;
	  rse_state_info.bsp_stored_nat_addr = nat_collection_addr;
	  rse_state_info.bsp_stored_nat = nat_collection;
	}
    }

  /* From 11.5.2 of EAS 2.6, p 11-8, "RNAT{x} corresponds to the register
     saved at concatenate (BSPSTORE{63:9}, x(5:0), 0{2:0}).  So take 8:3 of
     gr_bs_addr, shift nat_collection right by that many bits and AND the
     result with 1.  That is the desired NaT bit/byte.
     */
  
  shift_size = NAT_BITPOS(gr_bs_addr);
  mask = 1 << shift_size;
  bit_value = nat_value;
  bit_value = bit_value << shift_size;  /* Shift bit to correct position */
  nat_collection = (nat_collection & (~mask)) | bit_value;

  if (nat_collection_addr == rse_state_info.bsp_stored_nat_addr)
      rse_state_info.bsp_stored_nat = nat_collection;
  else
      rse_state_info.rnat = nat_collection;
  tt_status = call_real_ttrace ( TT_LWP_WRRSEBS,
				 (pid_t) inferior_pid,
				 (lwpid_t) get_first_tid (),
				 (TTRACE_ARG_TYPE) nat_collection_addr,
				 (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
				 (TTRACE_ARG_TYPE) &nat_collection);
  if (tt_status == -1 && errno)
      return errno;
  return 0;
} /* end put_stacked_nat */

/* put_stacked_register - copy the contents of gdb register regno to
   the RSE backing store for the register.  Return 0 on success, otherwise
   the value of errno.
   */

static int
put_stacked_register (regno, reg_buf_p)
  int		regno;
  char *	reg_buf_p;
{
  int		i;
  CORE_ADDR	reg_addr;
  int		tt_status;

  GET_RSE_STATE_INFO;

  if (!(reg_addr = reg_bs_addr (regno)))
    { /* The register isn't there.  Do nothing. */
      return 0;
    }

  if (reg_addr < rse_state_info.ar_bspstore)
    return write_inferior_memory (reg_addr, reg_buf_p, REGISTER_SIZE);

  tt_status = call_real_ttrace ( TT_LWP_WRRSEBS,
				 (pid_t) inferior_pid,
				 (lwpid_t) get_first_tid (),
				 (TTRACE_ARG_TYPE) reg_addr,
				 (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
				 (TTRACE_ARG_TYPE) reg_buf_p);
  if (tt_status == -1 && errno)
    {
      return errno;
    }

  return 0;
} /* end put_stacked_register */

/* Store our register values back into the inferior.
   If REGNO is -1, do this for all registers.
   Otherwise, REGNO specifies which register (so we can save time).  */

void
store_inferior_registers (regno)
     int regno;
{
  register unsigned int regaddr;
  char buf[80];
  extern char registers[];
  register int i;
  int scratch;
  int write_size;
  int	status;

#ifdef NONDEF
  if (regno >= 0)
    {
      if (CANNOT_STORE_REGISTER (regno))
	return;
      assert (regno < NUM_REGS);
      regaddr = map_gdb_reg_to_ureg[regno];
      if (regaddr == -1)
	{ /* The register does not have a simple mapping.  Handle special
	     cases.
	     */
	  if (regno >= NR0_REGNUM && regno <= NR0_REGNUM + 31)
	    {  /* Storing NaT bits is weird.  We read the related GR and then
		  do a 9-byte store including a byte for the NaT, bit zero
		  of which is the NaT bit.
		  */
	      char reg_buffer[9];

	      regaddr = map_gdb_reg_to_ureg[GR0_REGNUM + regno - NR0_REGNUM];
	      status = call_real_ttrace (TT_LWP_RUREGS,
					 inferior_pid,
					 get_first_tid (),
					 (TTRACE_ARG_TYPE) regaddr,
					 9, /* GR plus NaT byte*/
					 (TTRACE_ARG_TYPE)
					   &reg_buffer[0]);
	      if (status == -1 && errno)
		{
		  /* Warning, not error, in case we are attached; sometimes the
		     kernel doesn't let us at the registers.  */
		  char *err = strerror (errno);
		  char *msg = alloca (strlen (err) + 128);
		  sprintf (msg, "reading register %d: %s", regno, err);
		  fprintf (stderr, "gdb: %s\n", msg);
		  return;
		}

	      reg_buffer[8] = registers[REGISTER_BYTE (regno)];
	      status = call_real_ttrace (TT_LWP_WUREGS,
					 inferior_pid,
					 get_first_tid (),
					 (TTRACE_ARG_TYPE) regaddr, 
					 9,  /* GR + NaT byte */
					 (TTRACE_ARG_TYPE) 
					   &reg_buffer[0]);
	      if (status == -1 && errno != 0)
		{
		  /* Warning, not error, in case we are attached; sometimes the
		     kernel doesn't let us at the registers.  */
		  char *err = strerror (errno);
		  char *msg = alloca (strlen (err) + 128);
		  sprintf (msg, "writing register %d: %s",
			   regno, err);
		  fprintf (stderr, "gdb: %s\n", msg);
		  return;
		}
	      return;
	    }
	  else if (regno >= NR0_REGNUM + 32 && regno <= NRLAST_REGNUM)
	    {
	      status = put_stacked_nat (regno, 
					registers[REGISTER_BYTE (regno)]);
	      if (status)
		{
		  /* Warning, not error, in case we are attached; sometimes the
		     kernel doesn't let us at the registers.  */
		  char *err = strerror (status);
		  char *msg = alloca (strlen (err) + 128);
		  sprintf (msg, "writing register %d: %s",
			   regno, err);
		  fprintf (stderr, "gdb: %s\n", msg);
		}
	      return;
	    }
	  else if (regno == PC_REGNUM)
	    { /* If the process is in a system call, then store the address
		 in BR6, otherwise, store it in __cr_iip.  Otherwise it is
		 like storing any other 64-bit register.
		 */
	      if (thread_state.tts_flags & TTS_INSYSCALL)
		regaddr = __b6;
	      else
		regaddr = __cr_iip;

	      /* Fall through and read with new regaddr */
	    }
	} /* if (regaddr == -1) */

      if (regno >= FR0_REGNUM && regno <= FRLAST_REGNUM)
	{ /* Storing FR's is special because gdb keeps them in 12 bytes but
	     we write them in ttrace using 16 bytes.
	     */
	  char reg_buffer[16];

	  reg_buffer[0] = reg_buffer[1] = reg_buffer[2] = reg_buffer[3] =  0;
	  memcpy (&reg_buffer[4], &registers[REGISTER_BYTE (regno)], 12);
	  status = call_real_ttrace (TT_LWP_WUREGS,
				     inferior_pid,
				     get_first_tid (),
				     (TTRACE_ARG_TYPE) regaddr, 
				     16,  /* FR's are 16 bytes in ttrace */
				     (TTRACE_ARG_TYPE) 
				       &reg_buffer[0]);
	  if (status == -1 && errno != 0)
	    {
	      /* Warning, not error, in case we are attached; sometimes the
		 kernel doesn't let us at the registers.  */
	      char *err = strerror (errno);
	      char *msg = alloca (strlen (err) + 128);
	      sprintf (msg, "writing register %d: %s",
		       regno, err);
	      fprintf (stderr, "gdb: %s\n", msg);
	      return;
	    }
	  return;
	}
      else if (regno > GR0_REGNUM + 31 && regno <= GRLAST_REGNUM)
	{
	  put_stacked_register (regno, &registers[REGISTER_BYTE (regno)]);
	  return;
	}

      errno = 0;
      status = call_real_ttrace (TT_LWP_WUREGS,
			         inferior_pid,
			         get_first_tid (),
			         (TTRACE_ARG_TYPE) regaddr, 
			         8,  /* All these writes are 8 bytes */
			         (TTRACE_ARG_TYPE) 
			           &registers[REGISTER_BYTE (regno)]);
      if (status == -1 && errno != 0)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = strerror (errno);
	  char *msg = alloca (strlen (err) + 128);
	  sprintf (msg, "writing register %d: %s",
		   regno, err);
	  fprintf (stderr, "gdb: %s\n", msg);
	  return;
	}
    }
  else
    for (regno = 0; regno < NUM_REGS; regno++)
      store_inferior_registers (regno);
#endif

}

/* NOTE! I tried using PTRACE_READDATA, etc., to read and write memory
   in the NEW_SUN_PTRACE case.
   It ought to be straightforward.  But it appears that writing did
   not write the data that I specified.  I cannot understand where
   it got the data that it actually did write.  */

/* Copy LEN bytes from inferior's memory starting at MEMADDR
   to debugger memory starting at MYADDR.  */

int
read_inferior_memory (memaddr, myaddr, len)
     CORE_ADDR memaddr;
     char *myaddr;
     int len;
{
  register int i;

  /* Round starting address down to long-long-word (8 bytes) boundary.  */
  CORE_ADDR addr = memaddr & (-((LONGEST) sizeof (long long)));

  /* 
   * Round ending address up; get number of long-long-words (8-byte chunks) 
   * that makes.  
   */
  register int count
  = (((memaddr + len) - addr) + sizeof (long long) - 1) / sizeof (long long);

  /* Allocate buffer of that many long-long-words.  */
  long long *buffer = (long long *) alloca (count * sizeof (long long));

  CORE_ADDR loc_addr;
  int status;

  TPRINTF ("Asked to read %016llx, start at %016llx, will read %d times\n",
	   memaddr, addr, count);

  /* Read all the long-long-words */
  for (i = 0; i < count; i++, addr += sizeof (long long))
    {
      CORE_ADDR swizzle_addr;

      swizzle_addr = addr;
#if 0 /* 02/26/00 coulter - remove 06/01/00 */
      buffer[i] = ptrace (PT_RIUSER, inferior_pid, swizzle_addr, 0, 0);
#endif
      status = call_real_ttrace (TT_PROC_RDTEXT,
		                 inferior_pid,
		                 TT_NIL,
			         (TTRACE_ARG_TYPE) swizzle_addr,
			         sizeof (long long),
			         & buffer[i]);
      if (status == -1 && errno)
	{ /* Try to read the data as data instead of text */
	  status = call_real_ttrace (TT_PROC_RDDATA,
				     inferior_pid,
				     TT_NIL,
				     (TTRACE_ARG_TYPE) swizzle_addr,
				     sizeof (long long),
				     & buffer[i]);
	}
    }

  /* Copy appropriate bytes out of the buffer.  */
  memcpy (myaddr, (char *) buffer + (memaddr & (sizeof (long long) - 1)),
	  len);
  return 0;
}


/* Copy LEN bytes from inferior's memory starting at MEMADDR
   to debugger memory starting at MYADDR.  Use read_inferior_memory
   to read everything except addresses between BSPSTORE and BSP.  For
   these, we use TT_LWP_RDRSEBS to read the memory.

   Return 0 on success, otherwise, errno 
   */

int
read_inferior_ideal_memory (memaddr, myaddr, len)
     CORE_ADDR memaddr;
     char *myaddr;
     int len;
{
  char			buffer[RSE_RDWR_SIZE];
  int			bytes_left;
  int			bytes_left_in_buffer;
  CORE_ADDR		end_addr;
  char*			from_ptr;
  CORE_ADDR		read_addr;
  int			read_len;
  CORE_ADDR		start_addr;
  int			status;
  CORE_ADDR 		swizzle_addr;
  char*			to_ptr;
  int 			tt_status;


  GET_RSE_STATE_INFO;

  swizzle_addr = memaddr;

  /* Break the read into up to three parts, use read_inferior_memory
     to read any memory before the [BSPSTORE, BSP) region and any
     memory after the region.  Here we will handle reads inside the region.
     */

  if (swizzle_addr < rse_state_info.ar_bspstore)
    { /* Read before BSPSTORE? */
      end_addr = swizzle_addr + len;
      if (end_addr > rse_state_info.ar_bspstore)
	end_addr = rse_state_info.ar_bspstore;
      read_len = end_addr - swizzle_addr;
      status = read_inferior_memory (swizzle_addr, myaddr, read_len);
      if (status)
	return status;
    }
  if (swizzle_addr + len > rse_state_info.ar_bsp)
    { /* Read after BSPSTORE? */
      end_addr = swizzle_addr + len;
      start_addr = swizzle_addr;
      if (start_addr < rse_state_info.ar_bsp)
	start_addr = rse_state_info.ar_bsp;
      read_len = end_addr - start_addr;
      read_inferior_memory (start_addr, 
			    myaddr + start_addr - swizzle_addr,
			    read_len);
      if (status)
	return status;
    }

  /* Read anything in the region of [ar_bspstore .. ar_bsp) */
  start_addr = swizzle_addr;
  if (start_addr < rse_state_info.ar_bspstore)
    start_addr = rse_state_info.ar_bspstore;
  end_addr = swizzle_addr + len;
  if (end_addr > rse_state_info.ar_bsp)
    end_addr = rse_state_info.ar_bsp;
  
  if (start_addr < end_addr)
    { /* Read the regsion from start_addr to end_addr */

      read_addr = start_addr & ~0xf;  /* Round down to bundle boundary */
      /* Read the first 8 bytes;  We might only use part of this */
      tt_status = call_real_ttrace ( TT_LWP_RDRSEBS,
				     (pid_t) inferior_pid,
				     (lwpid_t) get_first_tid (),
				     (TTRACE_ARG_TYPE) read_addr,
				     (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
				     (TTRACE_ARG_TYPE) &buffer);
      if ((tt_status == -1) && errno)
	return errno;

      to_ptr = myaddr + (start_addr - swizzle_addr);
      from_ptr = buffer + start_addr - read_addr;
      bytes_left_in_buffer = RSE_RDWR_SIZE - (start_addr - read_addr);
      bytes_left = end_addr - start_addr;

      while (bytes_left)
	{
	  while (bytes_left && bytes_left_in_buffer)
	    {
	      *to_ptr++ = *from_ptr++;
	      bytes_left--;
	      bytes_left_in_buffer--;
	    }

	  if (bytes_left)
	    {
	      read_addr += RSE_RDWR_SIZE;
	      tt_status = call_real_ttrace ( TT_LWP_RDRSEBS,
					     (pid_t) inferior_pid,
					     (lwpid_t) get_first_tid (),
					     (TTRACE_ARG_TYPE) read_addr,
					     (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
					     (TTRACE_ARG_TYPE) &buffer);
	      if (tt_status == -1 && errno)
		return errno;
	      bytes_left_in_buffer = RSE_RDWR_SIZE;
	      from_ptr = buffer;
	    }
	} /* while bytes_left */
       
    } /* Read the regsion from start_addr to end_addr */
  return 0;
} /* end read_inferior_ideal_memory */

/* Copy LEN bytes of data from debugger memory at MYADDR
   to inferior's memory at MEMADDR.
   On failure (cannot write the inferior)
   returns the value of errno.  

   Because we can only write bundles as entire 16-byte writes, we do all
   writes with as a multiple of 16-bytes.  To read and write smaller areas
   we read in the preceeding and following bytes to round the region
   out to a multiple of 16 bytes.
   
   */

int
write_inferior_memory (memaddr, myaddr, len)
     CORE_ADDR memaddr;
     char *myaddr;
     int len;
{
  register int i;
  /* Round starting address down to bundle (16 bytes) boundary.  */
  CORE_ADDR addr = memaddr & (-((LONGEST) sizeof (bundle_t)));
  CORE_ADDR swizzle_addr;
  int  first_write_failed;

  /*
   * Round ending address up; get number of bundles (16-byte chunks)
   * that makes.
   */
  register int count
  = (((memaddr + len) - addr) + sizeof (bundle_t) - 1) / sizeof (bundle_t);

  /* Allocate buffer of that many long-long-words.  */
  bundle_t *buffer = (bundle_t *) alloca (count * sizeof (bundle_t));

  extern int errno;
  int status;

  TPRINTF ("Asked to write %016llx, start at %016llx, will write %d times\n",
	   memaddr, addr, count);

  {
    int j;

    for (j = 0; j < len; j++)
      {
	TPRINTF ("    asked-to-write-byte %d : %x\n", j,
		 (unsigned char) myaddr[j]);
      }
  }

  /* Fill start and end extra bytes of buffer with existing memory data.  */
  TPRINTF ("Reading initial bytes at %016llx\n", addr);

  swizzle_addr = addr;

#ifdef NONDEF
#if 0 /* 02/26/00 coulter - remove 06/01/00 */
  buffer[0] = ptrace (PT_RIUSER, inferior_pid, swizzle_addr, 0, 0);
#endif
  status = call_real_ttrace (TT_PROC_RDTEXT,
			     inferior_pid,
			     TT_NIL,
			     (TTRACE_ARG_TYPE) swizzle_addr,
			     (TTRACE_ARG_TYPE) sizeof (bundle_t),
			     & buffer[0]);
  if (status == -1 && errno)
    { /* Try using RDDATA instead */
      status = call_real_ttrace (TT_PROC_RDDATA,
				 inferior_pid,
				 TT_NIL,
				 (TTRACE_ARG_TYPE) swizzle_addr,
				 (TTRACE_ARG_TYPE) sizeof (bundle_t),
				 & buffer[0]);
      if (status == -1 && errno)
	return errno;
    }

#endif

#ifdef NONDEF
  if (count > 1)
    {
#if 0 /* 02/26/00 coulter - remove 06/01/00 */
      buffer[count - 1]
	= ptrace (PT_RIUSER, inferior_pid,
		  swizzle_addr + (count - 1) * sizeof (int), 0, 0);
#endif
      status = call_real_ttrace (TT_PROC_RDTEXT,
		                 inferior_pid,
		                 TT_NIL,
			         (TTRACE_ARG_TYPE) 
			             swizzle_addr 
				   + (count - 1) * sizeof (bundle_t),
			         (TTRACE_ARG_TYPE) sizeof (bundle_t),
			         & buffer[count - 1]);
      if (status == -1 && errno)
	{ /* Try using TT_PROC_RDDATA instead */
	  status = call_real_ttrace (TT_PROC_RDDATA,
				     inferior_pid,
				     TT_NIL,
				     (TTRACE_ARG_TYPE) 
					 swizzle_addr 
				       + (count - 1) * sizeof (bundle_t),
				     (TTRACE_ARG_TYPE) sizeof (bundle_t),
				     & buffer[count - 1]);
	  if (status == -1 && errno)
	    return errno;
	}
    }
#endif

  /* Copy data to be written over corresponding part of buffer */

  memcpy ((char *) buffer + (memaddr & (sizeof (bundle_t) - 1)), myaddr, len);

  /* Write the entire buffer.  */

  for (i = 0; i < count; i++, addr += sizeof (bundle_t))
    {
      TPRINTF ("Writing bytes at %016llx (%d) - %016llx  %016llx\n",
	       addr, i, buffer[i].halves.h, buffer[i].halves.l);
      {
	int j;
	unsigned char *ptr = (unsigned char *) &buffer[i];

	for (j = 0; j < sizeof (bundle_t); j++)
	  TPRINTF ("    writing %016llx + %d : %x\n", addr, j, ptr[j]);

      }
      swizzle_addr = addr;

#ifdef NONDEF
      errno = 0;
#if 0 /* 02/26/00 coulter - remove 06/01/00 */
      ptrace (4, inferior_pid, swizzle_addr, buffer[i], 0);
#endif
      /* To be sure that caches are updated, write both text and data.
	 If either works, assume it is OK.
	 */

      status = call_real_ttrace (TT_PROC_WRTEXT,
		                 inferior_pid,
		                 TT_NIL,
			         (TTRACE_ARG_TYPE) 
			           swizzle_addr,
				 (TTRACE_ARG_TYPE) sizeof (bundle_t),
			         (TTRACE_ARG_TYPE) &buffer[i]);
      first_write_failed = (status == -1 && errno);

      /* Try using TT_PROC_WRDATA instead */
      status = call_real_ttrace (TT_PROC_WRDATA,
				 inferior_pid,
				 TT_NIL,
				 (TTRACE_ARG_TYPE) 
				   swizzle_addr,
				 (TTRACE_ARG_TYPE) sizeof (bundle_t),
				 (TTRACE_ARG_TYPE) &buffer[i]);
      if (status == -1 && errno && first_write_failed)
	{
	  TPRINTF ("Write failed, errno is %d, memaddr is 0x%llx \n",
		   errno, memaddr);
	  return errno;
	}
#endif
    }
  return 0;
}

void
initialize ()
{
  inferior_pid = 0;
}

int
have_inferior_p ()
{
  return inferior_pid != 0;
}

/*
 * Tahoe. Dummy routines to satisfy Gambit.
 */
int
gb_dbg_symbol_name_to_addr ()
{
  return 0;
}

int
gb_dbg_addr_to_symbol_name ()
{
  return 0;
}

int
gb_dbg_loadsymbols ()
{
  return 0;
}

int
gb_dbg_process_exit ()
{
  return 0;
}

int
gb_dbg_load_process_symbols ()
{
  return 0;
}


/* When we use the dbgshl gambit option, it will call this routine.  This
   isn't really what we have in mind, so we just return and ignore the call.
   We will be setting a gambit breakpoint on top of the _asm_break break
   instruction to get our notification.
 */
int 
load_shl_symbols ()
{
  return 0;
}

/* swizzle
   input_addr is copied to the result.  
   If the current program is ilp32, then bits 32 and 33 of input_addr are
   copied to bits  1 and 2 of the result.  (high order bit is numbered zero 
   here).
 */

CORE_ADDR
swizzle (input_addr)
     CORE_ADDR input_addr;
{
  CORE_ADDR result;
  if (is_ilp32)
    {
      result = (input_addr & ~0x6000000000000000LL)
	| ((input_addr & 0xc0000000) << 31);
    }
  else
    {
      result = input_addr;
    }
  return result;
}				/* end swizzle */

/* unswizzle
   input_addr is copied to the result.  
   If the current program is ilp32, then only the low 32 bits are copied,
   otherwise, the input is just copied to the output.
 */

CORE_ADDR
unswizzle (input_addr)
     CORE_ADDR input_addr;
{
  CORE_ADDR result;

  if (is_ilp32)
    {
      result = input_addr & 0xFFFFFFFFLL;
    }
  else
    {
      result = input_addr;
    }
  return result;
}				/* end unswizzle */
