/* Temporary hack for ia64 -- stubs to satisfy unsats in link */

#include "defs.h"
#include "linkmap.h"

LM_errcode LM_begin_read_linkmap (char* (*linkmap_buffer)  
				  (unsigned int* size))
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}

LM_errcode LM_begin_read_linkmap_bss (char* (*linkmap_bss_buffer) 
				  (unsigned int* size))
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}

LM_errcode LM_begin_read_linkmap_file (char* (*linkmap_file_buffer) 
				  (unsigned int* size))
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}

LM_errcode LM_end_read_linkmap (void)
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}

LM_errcode LM_end_read_linkmap_bss (void)
{
  return LM_INVALID_STREAM;
}

LM_errcode LM_end_read_linkmap_file (void)
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}


LM_errcode LM_get_file_entry (unsigned int index, LM_file_entry_info* file)
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}

LM_errcode LM_get_next_mapping (LM_mapping_info* mapping)
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}

LM_errcode LM_get_num_files (unsigned int* num)
{
  error ("debug only in object modules (DOOM) not supported");
  return LM_INVALID_STREAM;
}
