/* Target specific file to be compiled along with infrtc.c to generate
 * librtc.sl.
 * This file has to define the functions:
 *      int get_frame_pc_list (void** pc_list)
 * Target: hp-ia64
 *
 * We use uwx routines to do the unwinding.
 * See man uwx or uwx.h and uwx_self.h header files for more info.
 * Result from the uwx routines if < UWX_OK means an error.
 */


#include <uwx.h> /* Unwind express */
#include <dlfcn.h> /* Unwind express */
#include <uwx_self.h> /* For self unwinding */
#include "defs.h"
#include "rtc.h"
#include "infrtc.h"
#include "demangle.h"
#include <string.h> 


#ifdef HP_IA64
#include <sys/pstat.h>
#include <sys/uc_access.h>
#include <ucontext.h>
lwpid_t _lwp_self();
void _lwp_suspend_all(void);
void _lwp_resume_all(void);
int _lwp_getstate(long tid, long scope, void* ucp); 
struct lwp_status *lwpbuf;
uint64_t *bspstore_regs;
#endif

/* Globals to hold the uwx data structures, unw enviornment and
   callback enviornment used with libc malloc. */
__thread struct uwx_env *uenv;
__thread struct uwx_self_info *cbinfo;
__thread int uenv_initted = 0;

/* Globals to hold the uwx data structures, unw enviornment and
   callback enviornment used with thread tracing alloc. */
__thread struct uwx_env *uenv_thr;
__thread struct uwx_self_info *cbinfo_thr;
__thread int uenv_initted_thr = 0;

/* Symbol lookup cache used in backtrace */
static struct uwx_symbol_cache *RTC_cachep = NULL;

extern void* utl_malloc (unsigned int);
extern void utl_free (void*);
extern unsigned int hash (int64_t key);

int debug_syms_from_unload_libs = 0;

int (*uwx_set_nofr_ptr) (struct uwx_env *env) = NULL;
int (*uwx_self_init_state_cache_ptr) (struct uwx_self_info *info,
                                      int nrows,
                                      int assoc,
                                      int entsz) = NULL;

/* Returns frame_count number of frame pc's by doing a stack trace. */
static int
get_frame_pc_list_common (void** pc_list, int frame_count,
                          struct uwx_env** u_env, 
                          struct uwx_self_info** cb_info,
                          int* u_env_initted)
{
  int result;
  uint64_t pc_val;
  int slot_no;
  int level = 0, j;

  /* Initialize the uwx data structures. */
  if (!(*u_env_initted))
    {
      *u_env = uwx_init();
      if (uwx_set_nofr_ptr) 
        uwx_set_nofr_ptr(*u_env);
      *cb_info = uwx_self_init_info(*u_env);
      if (uwx_self_init_state_cache_ptr) 
        uwx_self_init_state_cache_ptr(*cb_info, 
                                      1000, /* number of rows in the cache */
                                      8,    /* associativity of the cache */
                                      128); /* size of each cache entry */
      /* Register the standard callbacks provided by libunwind. */
      result = uwx_register_callbacks(
                        *u_env,
                        (intptr_t) *cb_info,
                        uwx_self_copyin,
                        uwx_self_lookupip);
      if (result < UWX_OK)
        {
          return level;
        }
      /* Set u_env_initted so that we don't have to do these
         initialization again. */
      *u_env_initted = 1;
    }

  /* Initialize the current context */
  result = uwx_self_init_context (*u_env);
  if (result < UWX_OK)
    {
      return level;
    }

  /* Skip 4 frames. These are the librtc frames. */
  for (j=0; ((j < 4) && (result == UWX_OK)); j++)
    {
      result = uwx_step (*u_env);
    }
  if ((j != 3) && (result != UWX_OK))
    {
      return level;
    }
  
  /* Now get the stack trace and collect the PCs. */
  do
    {
      /* Get the PC for this frame. */
      result = uwx_get_reg (*u_env, UWX_REG_IP, &pc_val);
      if (result < UWX_OK)
        {
	  return level;
        }

      /* Get the pc that's just before return_pc. */
      slot_no = (int) (pc_val & 0xF);
      pc_val = pc_val & ~0xFLL;
      switch (slot_no)
      {
      case 0:
	pc_val = pc_val - 14;
	break;
      case 1:
	pc_val = pc_val;
	break;
      case 2:
	pc_val = pc_val + 1;
	break;
      default:
	return level;
      }

      pc_list[level++] = (void *) (unsigned long) pc_val;
       
      /* Get the previous frame. */
      if ((result = uwx_step (*u_env)) < UWX_OK)
        {
	  return level;
	}
    } while ((level < frame_count) && (result != UWX_BOTTOM));
  
  return level;
}


#define PTR_HASH_SLOTS 0x1000
typedef struct ptr_hash_tag
{
  void* ret_ptr;
  void* alloc_ptr;
  struct ptr_hash_tag* next;
} ptr_hash_t;

static ptr_hash_t* malloc_ptr_hash[PTR_HASH_SLOTS] = {0};

static void* 
unwind_malloc (size_t size)
{
  uint64_t malloc_ptr = (uint64_t) malloc (size + 15);
  uint64_t ret_ptr = ((malloc_ptr + 15) & (~15));

  /* If the address we're about to return is different from that returned
     by malloc(), we need to cache it in order to pass the right address
     to free() */
  if (ret_ptr != malloc_ptr)
    {
      ptr_hash_t* entry = (ptr_hash_t*) malloc (sizeof (ptr_hash_t));
      entry->alloc_ptr = (void*) malloc_ptr;
      entry->ret_ptr = (void*) ret_ptr;
      unsigned int idx = hash(ret_ptr) % PTR_HASH_SLOTS;
      entry->next = malloc_ptr_hash[idx];
      malloc_ptr_hash[idx] = entry;
    }
  return (void*) ret_ptr;
}

static void 
unwind_free (void* ptr)
{
  /* Lookup the address in the hash table */
  unsigned int idx = hash ((int64_t) ptr) % PTR_HASH_SLOTS;
  ptr_hash_t* entry = malloc_ptr_hash[idx];
  ptr_hash_t* prev_entry = NULL;
  while (entry)
    {
      if (entry->ret_ptr == ptr)
        {
          /* If we find the address in the hash table, make sure
             that's the one we pass to free() */
          ptr = entry->alloc_ptr;

          /* Remove this entry from the list */
          if (prev_entry)
            prev_entry->next = entry->next;
          else
            malloc_ptr_hash[idx] = entry->next;

          free (entry);
          break;
        }
      prev_entry = entry;
      entry = entry->next;
    }
  free (ptr);
}

/* Returns frame_count number of frame pc's by doing a stack trace. 
   Entry point for most of RTC heap-check related functions */
int
get_frame_pc_list (void** pc_list, int frame_count)
{
  if (!uenv_initted)
    uwx_register_alloc_cb((alloc_cb) unwind_malloc, (free_cb) unwind_free);
  return get_frame_pc_list_common(pc_list, frame_count, &uenv, &cbinfo,
                                  &uenv_initted);
}

/* Returns frame_count number of frame pc's by doing a stack trace.
   Entry point for the thread tracer. Uses custom malloc */
int
get_frame_pc_list_thr (void** pc_list, int frame_count)
{
  /* Use the malloc package from the thread tracer because we can't use
     libc malloc from inside pthread tracing routines */
  if (!uenv_initted_thr)
    uwx_register_alloc_cb((alloc_cb) utl_malloc, (free_cb) utl_free);
  return get_frame_pc_list_common(pc_list, frame_count, &uenv_thr, &cbinfo_thr,
                                  &uenv_initted_thr);
}

/* Returns the symbol information (name, file, line etc.), given a IP address
   and a library index. If fp is not NULL, it prints the information to fp.

   This is HP-UX specific, as it relies on the HP-UX unwind APIs to get
   symbol information. For other platforms, other mechanisms might be reqd
   to get the information on symbols.

   Return 0 on error, non-zero on success.
*/
int backtrace (FILE *fp, int i, uint64_t ip, int index,
                struct symbol_from_unload_lib **symbol_info)
{
  int ident_parm = 0, status;
  uint64_t  addr = ip;
  uint64_t load_map_parm =0;
  struct load_module_desc desc;
  size_t desc_size = sizeof(struct load_module_desc);
  char *name = NULL, *mod_name = NULL;
  struct symbol_from_unload_lib *fetch_symbol = NULL;
  char *funcp = NULL;
  uint64_t offsetp;
  uint64_t relfuncstart;
  char *ifuncp = NULL;
  char *srcfilep = NULL;
  int linenump;
  int inline_contextp;
  char no_sym[5] = "????";
  int  demang_stauts = 0;

  status = dlmodinfo (addr, &desc, desc_size, NULL,
                      ident_parm, load_map_parm);
  if (!status)
   {
     if (debug_syms_from_unload_libs)
      {
        DEBUG(printf ("\nERROR: dlmodinfo failed \n %d ",dlerrno());)
        DEBUG(printf ("\n %#llx \n", (uint64_t)ip);)
      }

     if (index != 0)
      fetch_symbol
        = load_or_store_symbol_for_unloading_library (0,
                                                      UNSWIZZLE_PTR(addr),
                                                      index);
     /* At this point, return 0, as an error status */
     if (!fetch_symbol)
       return 0;
   }

  if (status)
   {
     name = dlgetname (&desc, sizeof(desc), NULL, ident_parm, load_map_parm);
     if (!name)
       {
         DEBUG(printf ("ERROR: dlgetname()\n error no: %d ", dlerrno());)
       }
     else
       mod_name = strdup (name); 

     relfuncstart = ((uint64_t)UNSWIZZLE_PTR(ip)) - ((uint64_t)UNSWIZZLE_PTR(desc.text_base)); 

     /* QXCR1000976492: HP debug information on IA64 encodes the slot number
      * in bits 3 and 2 of the address. The IP address passed to the API
      * uwx_find_source_info() needs to be in this format. The code below
      * moves the slot information from bits 0 and 1 to bits 2 and 3.
      */
     uint64_t relfunc_bundle = (relfuncstart & ~0x3); 
     uint8_t  relfunc_slot = (relfuncstart & 0x3);
     relfuncstart = (relfunc_bundle + (relfunc_slot << 2));

     /* Get the symbol information using unwind API */
     status = uwx_find_source_info(uenv, &RTC_cachep, mod_name,
                           relfuncstart, relfuncstart, 0,
                           &ifuncp, &funcp, &offsetp, &srcfilep, &linenump,
                           &inline_contextp);

      if (status != UWX_OK && debug_syms_from_unload_libs)
        DEBUG(printf ("ERROR: uwx_find_source_info\n");)
   
      if (status == UWX_ERR_NOSYM && fp != NULL)
      {
        if (funcp == NULL || srcfilep == NULL || linenump == 0)
          if (addressprint)
            {
              fprintf (fp, "#%-2d %"PRIx64" in %s() at %s",i ,addr, no_sym,
                  srcfilep ? srcfilep : " ");
            }
          else
            {
              fprintf (fp, "#%-2d %s() at %s",i, no_sym,
                  srcfilep ? srcfilep : " ");
            }

          if (linenump)
	    fprintf (fp, ":%d", linenump);

          fprintf (fp, "\n");
            
      }
     else
      { 
        if (status == UWX_OK && fp != NULL && symbol_info == NULL)
         {
           if (addressprint)
             {
               fprintf (fp, "#%-2d 0x%"PRIx64" in %s()",i ,addr,
                        funcp ? __cxa_demangle ( funcp, 0, 0, &demang_stauts)
                              : "????");
             }
           else
             {
               fprintf (fp, "#%-2d %s()",i,
                        funcp ? __cxa_demangle ( funcp, 0, 0, &demang_stauts)
                              : "????");
             }
           if (srcfilep)
	    fprintf (fp, " at %s", srcfilep);  
  	   else
	    if (name)
 	     fprintf (fp, " from %s", mod_name);
	   
	   if (linenump)
	    fprintf (fp, ":%d", linenump);
	   if (inline_contextp)
	    fprintf (fp, ":%d", inline_contextp);
           fprintf (fp, "\n");
	}
        
	if (symbol_info != NULL && ip != 0 && index != 0)
         {
           (*symbol_info)->symbol_name
              = funcp ? __cxa_demangle(funcp, 0, 0, &demang_stauts) : NULL;
           /* QXCR1000985463 : strdup the file name so that it is
              not overwritten later. __cxa_demangle above does a malloc,
              so no need to do strdup there. */
           /* QXCR1000989650: uwx_find_source_info() may retuen NULL
              file name, even though the return status is UWX_OK. This can
              happen if the application is compiled with +O2. Till unwind
              library has a fix for it, we need to expect empty file name.
              To be on the safer size, check if the file name is NULL before
              caling strdup, as a NULL argument to strdup causes abort.
           */
           (*symbol_info)->src_file_name = srcfilep ? strdup(srcfilep) : NULL;
           if (linenump)
            (*symbol_info)->line_number = linenump;
           else
            (*symbol_info)->line_number = inline_contextp;
           (*symbol_info)->offset = offsetp; 
         }
       // TBD: We need to release the symbol cache at the end.. when ?
       // certainly not here, because unwind needs to have the symbol cache for
       // future calls to uwx_find_source_info, otherwise, the calls will be
       // expensive.
       // uwx_release_symbol_cache(uenv, RTC_cachep);
       free (mod_name);		
       return 1;
     }
   }
  else
   { 
     /* convert offset to absolute pc address */
     if (fp != NULL)
       {
         if (addressprint)
           {
             fprintf (fp, "#%-2d 0x%"PRIx32" in %s()", i,
                      fetch_symbol->unswizzled_addr, 
                      fetch_symbol->symbol_name
                        ? __cxa_demangle(fetch_symbol->symbol_name,
                                         0, 0, &demang_stauts)
                        : "????");
           }
         else
           {
             DEBUG(printf("symbol name = %s\n", fetch_symbol->symbol_name);)
             fprintf (fp, "#%-2d %s()", i,
                      fetch_symbol->symbol_name
                        ? __cxa_demangle(fetch_symbol->symbol_name,
                                         0, 0, &demang_stauts)
                        : "????");
           }

         if (fetch_symbol->src_file_name)
          fprintf (fp, " at %s", fetch_symbol->src_file_name);
         if (fetch_symbol->line_number)
          fprintf (fp, ":%d", fetch_symbol->line_number);
         fprintf (fp, "\n");
       }
     else if (symbol_info != NULL)
       {
         (*symbol_info)->symbol_name = fetch_symbol->symbol_name
            ? __cxa_demangle((fetch_symbol->symbol_name), 0, 0, &demang_stauts )
            : NULL;
         /* QXCR1000985463 : strdup the file name so that it is
            not overwritten later. __cxa_demangle above does a malloc,
            so no need to do strdup there. */
         /* QXCR1000989650: uwx_find_source_info() may retuen NULL
            file name, even though the return status is UWX_OK. This can
            happen if the application is compiled with +O2. Till unwind
            library has a fix for it, we need to expect empty file name.
            To be on the safer size, check if the file name is NULL before
            calling strdup, as a NULL argument to strdup causes abort.
         */
         (*symbol_info)->src_file_name = fetch_symbol->src_file_name
            ? strdup(fetch_symbol->src_file_name) : NULL;

         if (fetch_symbol->line_number)
            (*symbol_info)->line_number = fetch_symbol->line_number;
         else
           (*symbol_info)->line_number = inline_contextp;
         (*symbol_info)->offset = fetch_symbol->offset;
       } 
   }

   return 1;
}


/* Thread control APIs - vvv HP-UX/IPF specific */

/* Suspend all threads, except the calling thread.

   This function is vvv HP-UX/IPF specific. Other platforms will need
   different mechanisms.
*/
void suspend_all_threads()
{
  /* Undocumented API from 11.23 onwards */
  _lwp_suspend_all();
} 

/* Resume all suspended threads.

   This function is vvv HP-UX/IPF specific. Other platforms will need
   different mechanisms.
*/
void resume_all_threads()
{
  /* Undocumented API from 11.23 onwards */
  _lwp_resume_all();
}

#define BURST   (1)

/* Find out the number of threads in the process currently.
   This gets called after suspending all other threads, so
   that the number of threads is consistent.

   This function uses pstat interface on HP-UX. On other OS,
   either /proc/<pid>/tasks (Linux) or something equivalent
   can be used.
*/
static int
get_nbr_threads(pid_t mypid, struct lwp_status *lwpbuf_ptr)
{
  int num_threads = 0;
  int lwp_idx     = 0;
  int lwp_count   = 0;

  while (lwp_count = pstat_getlwp(lwpbuf_ptr + num_threads,
                                  sizeof(struct lwp_status) * BURST, BURST, 
                                  lwp_idx, mypid) > 0)
    {
      DEBUG(printf("Got thread count %d\n", lwp_count);)
      for (int li = num_threads; li < num_threads + lwp_count; li++)
        DEBUG(printf("[%d] : lwp_id = %"PRId64" lwp_flag = %"PRIx64"\n", li,
                     (lwpbuf_ptr + li)->lwp_lwpid,
                     (lwpbuf_ptr + li)->lwp_flag);)
      lwp_idx = (lwpbuf_ptr + (lwp_count + num_threads - 1))->lwp_idx + 1;
      num_threads += lwp_count;
    };

  return num_threads;
}


/* Discover RSE regions for all threads, given their bspstore register
   value.

   This is required only on IPF, since it deals with RSE.

   This function is HP-UX specific. It uses pstat interface to get the 
   mmap-ed regions.

   The RSE backing store for threads are mmap-ed pages. These pages are
   mmaped, with rw_ permissions. This function find all such mmap-ed regions
   and checks if the bspstore pointer of any thread lies inside the mmap-ed
   region. If so, we found the RSE backing store for the thread whose
   bspstore was in the mmap-ed region. The RSE region expands from the start
   of the mmap-ed region to the bspstore. We note them here for scanning
   this region during mark().

   For main thread, kernel allocates a seperate RSE region. This can be
   identified with the PS_RSESTACK flag. 
 */
static void
discover_rse_regions_for_threads(
  uint64_t *bspstore_regs,
  int      bspstore_count,
  uint64_t main_thread_bspstore
)
{
  int i = 0, count = 0;
  struct pst_static pst;
  struct pst_vm_status buf;
  char *rtc_RSE_ptr = (char *)__rtc_RSE_file;

  if (pstat_getstatic(&pst, sizeof(pst), (size_t)1, 0) == -1)
    perror("pstat_getstatic");

  count = pstat_getprocvm (&buf, sizeof(buf), 0, i);

  while (count > 0) 
    {
      /* The RSE backing store for threads are mmap-ed pages. These pages
         are mmaped, with rw_ permissions.
      */
      if (   buf.pst_vaddr != NULL
          && buf.pst_type == PS_MMF
          && (   (buf.pst_permission & (PS_PROT_READ | PS_PROT_WRITE))
              && !(buf.pst_permission & PS_PROT_EXECUTE))
          && !(buf.pst_flags & PS_SHARED_LIBRARY))
        {
#if 0
          /* Following changes will be allowed once we get the
             kernel patch and is available for building librtc.
             At that point of time we should unifdef this piece 
             of code. Remove this comment. Once the patch is 
             available we need to make sure it works for PA and
             IA.
          */
          /* kernel does not give size correctly if there
             is hole in mmapped segment because of partial
             munmap earlier.
           */
          if (buf.pst_flags & PS_HOLES)
            continue; 
#endif
          uint64_t mmap_start = SWIZZLE_PTR(buf.pst_vaddr);
          /* pstat_getprocvm returns size in the native page size */
          uint64_t mmap_end   = buf.pst_vaddr
                                + (long)(buf.pst_length * pst.page_size);

          mmap_end = SWIZZLE_PTR(mmap_end);
          DEBUG(printf("mmap-ed region - start = %llx, end = %llx\n",
                       mmap_start, mmap_end);)

          /* Check if any of the bspstore values are within the
             mmap-ed region
          */
          for (int bc = 0; bc < bspstore_count; bc++)
            {
               if (   (mmap_start < bspstore_regs[bc])
                   && (bspstore_regs[bc] <= mmap_end))
                 {
                   /* Found a valid RSE region */
                   DEBUG(printf("Found RSE : start = %llx, end = %llx\n",
                                mmap_start, bspstore_regs[bc]);)

                   *rtc_RSE_ptr = mmap_start;
                   rtc_RSE_ptr  = rtc_RSE_ptr + REGISTER_RAW_SIZE(GR0_REGNUM);
                   *rtc_RSE_ptr = bspstore_regs[bc];
                   rtc_RSE_ptr  = rtc_RSE_ptr + REGISTER_RAW_SIZE(GR0_REGNUM);
                   break;
                 }
            }
        }
      else
      /* If main thread is alive, discover the RSE region for it */
      if (buf.pst_vaddr != NULL && buf.pst_type == PS_RSESTACK)
        {
          if (!__main_thread_exited)
            {
              uint64_t mmap_start = SWIZZLE_PTR(buf.pst_vaddr);
              /* pstat_getprocvm returns size in the native page size */
              uint64_t mmap_end   = buf.pst_vaddr
                                + (long)(buf.pst_length * pst.page_size);

              mmap_end = SWIZZLE_PTR(mmap_end);
              DEBUG(printf("mmap-ed region - start = %llx, end = %llx\n",
                           mmap_start, mmap_end);)

              /* TBD: The check before is required ? */
              if (   (mmap_start < main_thread_bspstore)
                  && (main_thread_bspstore <= mmap_end))
                {
                   /* Found valid main thread RSE region */
                   DEBUG(printf("Found main thread RSE : start = %llx, "
                                "end = %llx\n",
                                mmap_start, main_thread_bspstore);)

                   *rtc_RSE_ptr = mmap_start;
                   rtc_RSE_ptr  = rtc_RSE_ptr + REGISTER_RAW_SIZE(GR0_REGNUM);
                   *rtc_RSE_ptr = main_thread_bspstore;
                   rtc_RSE_ptr  = rtc_RSE_ptr + REGISTER_RAW_SIZE(GR0_REGNUM);
                }
            }
        }

      i++;
      count = pstat_getprocvm (&buf, sizeof(buf), 0, i);

    } /* while */
}

/* Get context (registers, RSE contents) of all threads in the process.

   This function is vvv HP-UX/IPF specific. Other platforms will need
   different mechanisms.
*/
int get_thread_contexts (
  char **stack_end_ptr,
  int *is_this_main_thr_ptr
)
{
  int num_threads = 0;

  pid_t mypid = getpid(); 

  int      bspstore_count = 0;

  char *rtc_context_ptr = (char *)__rtc_register_file;

  *is_this_main_thr_ptr = 0;

  /* Task 2: How many threads ? Their status.. */
  num_threads = get_nbr_threads(mypid, lwpbuf);

  /* Task 3 : Get register of other threads. */
  /* Task 4 : Get RSE contents of other threads. */

#define LWP_ALL_STATE (0) /* From /ux/vbe/em/usr/include/sys/lwp.h */

/* Reason value from __uc_get_reason() */
#define CTXT_CREATED_IN_SYSCALL (0)

  ucontext_t ucp;
  int ret = 0;
  lwpid_t my_lwpid = _lwp_self();
  uint64_t main_thread_bspstore = 0;

  __main_thread_exited = 1;

  for (int li = 0; li < num_threads; li++)
    {
      bool is_main_thread = false;
      DEBUG(printf("[%d] : Getting state for lwp_id = %"PRId64"\n", li,
                   lwpbuf[li].lwp_lwpid);)
      if (lwpbuf[li].lwp_flag & LWP_FIRSTLWP)
        {
          DEBUG(printf("main thread found!");)

          /* TBD: Should I check if the lwp is active? */
          if (!(lwpbuf[li].lwp_flag & LWP_ACTIVE))
            {
              DEBUG(printf("But the main thread isn't active - "
                    "ignoring it!");)
              continue;
            }
          __main_thread_exited = 0;
          is_main_thread = true;
        }

      if (lwpbuf[li].lwp_flag & LWP_USER_KERNEL_DAEMON)
        {
          DEBUG(printf("It's a kernel daemon thread, don't scan this");)
          continue;
        }

      int getstate_ret;
      if (lwpbuf[li].lwp_lwpid == my_lwpid)
        {
          DEBUG(printf("It's the current thread %d\n", my_lwpid);)
          getstate_ret = getcontext(&ucp);
          if (getstate_ret != 0)
            {
              DEBUG(printf("getcontext failed with error %d\n",
                    getstate_ret);)
              continue;
            }
        }
      else
        {
          /* Undocumented API from 11.23 onwards */
          getstate_ret = _lwp_getstate(lwpbuf[li].lwp_lwpid,
                                       LWP_ALL_STATE,
                                       &ucp);
          if (getstate_ret != 0)
          {
            DEBUG(printf("_lwp_getstate failed %d\n", getstate_ret);)
            continue;
          }
        }

      /* Get the reason of context (syscall/interrupt)
       * reason == 0 -> context was created in a syscall.
       * reason != 0 -> context was created while handling an
                      interruption that occurred while running
                      user code.
      */
      uint16_t reason;

      ret = __uc_get_reason(&ucp, &reason);
      if (ret != 0)
        {
          DEBUG(printf("__uc_get_reason failed with error %d\n", ret);)
        }
      else
        {
          DEBUG(printf("reason = %d\n", reason);)
        }

      /* Read all registers */

      /* General registers */
      uint64_t gr_values[32];
      unsigned int gr_NAT = 0;
      register int regnum;

      DEBUG(printf("Getting register values for %"PRId64"\n",
                   lwpbuf[li].lwp_lwpid);)
      ret = __uc_get_grs(&ucp, 1, 31, gr_values, &gr_NAT);
      if (ret != 0)
        {
          DEBUG(printf("__uc_get_grs failed with error %d\n", ret);)
          continue;
        }

      /* Copy over to the __rtc_register_file */
      for (regnum = 1; regnum < 32; regnum++)
        {
          /* In syscall context, scratch registers (GR2, GR3 and
             GR14-GR31) are not saved in context, and will have value
             0 from _uc_get_grs(). They will be ignored for mark()
          */
          if (reason == CTXT_CREATED_IN_SYSCALL)
            {
              /* Skip GR2 and GR3, as they are 0 */
              if ((regnum == 2) || (regnum == 3))
                continue;
              /* Skip the whole range from GR14-GR31, as they are 0 */
              if (14 == regnum)
                break;
            }
          DEBUG(printf("Register[%d] = %"PRIx64"\n",
                       regnum, gr_values[regnum]);)
          *rtc_context_ptr = gr_values[regnum];
          rtc_context_ptr
            = rtc_context_ptr + REGISTER_RAW_SIZE(GR0_REGNUM + regnum);
        } 

      /* Update the main threads stack address */
      if (is_main_thread)
        {
          *stack_end_ptr = (char *)(gr_values[11]);

          if (lwpbuf[li].lwp_lwpid == my_lwpid)
            *is_this_main_thr_ptr = 1;

          DEBUG(printf("main thread stack end = %p\n",
                       *stack_end_ptr);)
        }

      /* Read the dirty registers.
         bspstore to bspstore+max(bsp, 96) */
      uint64_t addr_bsp, addr_bspstore;
      ret = __uc_get_ar_bsp(&ucp, &addr_bsp);
      /* check return value */
      if (ret != 0)
        {
          DEBUG(printf("__uc_get_ar_bsp failed with error %d\n", ret);)
          continue;
        }

      DEBUG(printf("bsp = %"PRIx64"\n", addr_bsp);)

      ret = __uc_get_ar_bspstore(&ucp, &addr_bspstore);
      /* check return value */
      if (ret != 0)
        {
          DEBUG(printf("__uc_get_ar_bspstore failed with error %d\n", ret);)
          continue;
        }

      DEBUG(printf("bspstore = %"PRIx64"\n", addr_bspstore);)
      /* Store this seperately for main thread */
      if (is_main_thread)
        main_thread_bspstore = addr_bspstore;
      else
        bspstore_regs[bspstore_count++] = addr_bspstore;

      if (addr_bsp > addr_bspstore)
        {
          int dirty_size = (int) (addr_bsp - addr_bspstore);
          if (dirty_size > 96 * REGISTER_RAW_SIZE(GR0_REGNUM))
            dirty_size = 96 * REGISTER_RAW_SIZE(GR0_REGNUM);

#ifdef __LP64__
#define UC_GET_RSEBS __uc_get_rsebs
#define UC_ARG2_TYPE   uint64_t *
#else
#define UC_GET_RSEBS __uc_get_rsebs64
#define UC_ARG2_TYPE   ptr64_t
#endif
          uint64_t* bsp_ptr = (uint64_t*)(void*)addr_bspstore;
          /* Copy over to the __rtc_register_file */
          /* Copy from addr_bspstore to addr_bspstore+dirty_size */
          for (regnum = 0;
               regnum < dirty_size / REGISTER_RAW_SIZE(GR0_REGNUM);
               regnum++)
            {
              ret = UC_GET_RSEBS(&ucp, (UC_ARG2_TYPE)SWIZZLE_PTR(bsp_ptr), 1,
                                 (uint64_t *)rtc_context_ptr);
              if (ret != 0)
                {
                  DEBUG(printf("__uc_get_rsebs failed with error %d\n", ret);)
                  bsp_ptr++;
                  continue;
                }
              DEBUG(printf("bspstore[%d] value thru rsebs = %x\n",
                           regnum, *rtc_context_ptr);)
              bsp_ptr++;
              rtc_context_ptr
                = rtc_context_ptr + REGISTER_RAW_SIZE(GR0_REGNUM);
            } 
        }
    } // For all threads

  /*
    Read the RSE context for all threads.
  */

  discover_rse_regions_for_threads(bspstore_regs,
                                   bspstore_count,
                                   main_thread_bspstore);
  return 1;
}

#undef BURST
