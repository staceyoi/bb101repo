/* Handle HP-UX IA64 ELF shared libraries for GDB, the GNU Debugger.
   Copyright 1998 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Written by Hewlett-Packard.  */

/* This file was largely derrived from pa64-solib.c */

/*--------------------------------------------------------------------------
   Includes
--------------------------------------------------------------------------*/

/* System file includes */

#include <elf.h>
#include <elf_hp.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>	/* for bzero() prototype */
#include <sys/param.h>
#include <sys/pthread.h>
#include <sys/ttrace.h>
#include <sys/stat.h>
#include <dl.h>        /* For shl_findsym */
#include <sys/dyntune.h> /* For gettune() */
#include <time.h>

/* Gdb includes used by following includes */

#include "solist.h"
#include "hp-ia64-solib.h"
#include "pa_save_state.h" /* for pa_save_state_t */
#include <a.out.h>
#include "objfiles.h"
#include "som.h"

/* Gdb includes */

#include <assert.h>
#include "elf-bfd.h"
#include "breakpoint.h"
#include "gdb-stabs.h"
#include "gdbcmd.h"
#include "gdbcore.h"
#include "inferior.h"
#include "inttypes.h"
#include "symfile.h"
#include "value.h"
#include "language.h"
#include "breakpoint.h"
#include "top.h"	/* for mapshared extern */
#include "gnu-regex.h"	/* for re_exec() prototype */
#include "rtc.h"	/* for snoop_on_the_heap() prototype */
/* We want the IA64 version of load_info even though the gambit server
   is an HPPA executable.
   */

#ifndef __ia64
#define __ia64
#endif
#include "crt0.h"
#undef __ia64

/***************Begin MY defs*********************/

#define TTRACE_ARG_TYPE uint64_t

/* --------- defines which depend on previous defines -------*/

#define TT_NIL ((TTRACE_ARG_TYPE) TT_NULLARG)

/***************End   MY defs*********************/

#define  EM_IA64 50
/*--------------------------------------------------------------------------
   Other external declarations 
--------------------------------------------------------------------------*/
void ia64_request_bor_callback (int needed);

extern int in_startup_no_prev_fp; 

extern CORE_ADDR text_start;
extern CORE_ADDR text_end;

/* jini: Mixed mode changes for corefile debugging. */
CORE_ADDR   inferior_aries_text_start;
CORE_ADDR   inferior_aries_text_end;
bool        aries_helper_lib_loaded;
extern int  mixed_mode_debug_aries;
shl_t       aries_shlib_handle;

#ifdef RTC
extern int check_heap_in_this_run, trace_threads_in_this_run;
#endif /* RTC */

/* Defined in gdbrtc.c; Used for supressing warnings to end-user */
extern bool add_unloaded_library_symbols;

/* Defined in exec.c; used to prevent dangling pointer bug.
 */
extern struct target_ops exec_ops;
extern int debugging_aries, debugging_aries64;

/* jini: Mixed mode core file debugging. */ 
int (*ptr_aries_get_save_state) (CORE_ADDR        aries_tp_addr,
                                 int              context_nbr,
                                 pa_save_state_t *pa_save_state_ptr,
                                 bool             is_32bit);

int (*ptr_aries_get_nbr_contexts) (CORE_ADDR        aries_tp_addr,
                                   int             *nbr_contexts,
                                   bool             is_32bit);

int (*ptr_aries_in_signal_handler) (CORE_ADDR        aries_tp_addr,
                                    char            *signal_handler_name,
                                    int             *in_signal_handler,
                                    bool             is_32bit);

int (*ptr_aries_get_pa2ia_export_stub_size) ();


extern enum exec_format xmode_exec_format;
bool objfile_is_mixed_mode (struct objfile *objfile);
extern char* dld_name;

/*JAGae68084- persistent deferred breakpoint */
extern int if_yes_to_all_commands;
int in_shl_load;
char *shllib_name;
int now_re_enable_bp;
extern void place_breakpoints_in_shlib(void);
static int strt_loaded;
extern void disable_break_exp_chain (struct objfile *);
extern boolean bfd_is_elf32 PARAMS ((bfd*));

/* From bfd/bfd-in.h */

bfd_vma bfd_getl64 PARAMS ((const unsigned char *));

boolean
elf_is_header_big_endian PARAMS ((bfd *));

#ifdef HP_MXN
void clear_mxn_info(void);
#endif

/* Support for batch mode thread check. */
extern void mark_librtc_start_end (CORE_ADDR text_start, CORE_ADDR text_end);
extern void mark_libpthread_start_end (CORE_ADDR text_start, CORE_ADDR text_end);

/* Defined in libxpdl.a from the linker project. */

uint64_t
dlgetmodinfo (int index,
	      struct load_module_desc *desc,
	      size_t desc_size,
	      void *(*read_tgt_mem) (void *buffer,
				     uint64_t ptr,
				     size_t bufsiz,
				     int ident),
	      int ident_parm,
	      uint64_t load_map_parm);

#if 0
static void
hp_ia64_core_exe_mismatch_detection (
  struct load_module_desc *exe_lmdp,
  CORE_ADDR exe_timestamp,
  CORE_ADDR exe_checksum
);
#endif

static void
hp_ia64_core_so_mismatch_detection (struct so_list *so_ptr);

static flip_desc_t desc_load_module_desc[] = { { 11, 8 }, {0, 0}};

/* This are from gdbrtc.c */
extern struct objfile* rtc_dot_sl;

/* Variables for core library mismatch detection. */
char *so_pathname_g;

/* Need to disable core library mismatch detection if the gdb or the core used
 * an older dld.
 */
static int core_so_mismatch_detection_disabled = 0;

int shlib_load_completed = 0; /* JAGaf35513 */

CORE_ADDR saved_load_info_addr = 0;

extern load_info_t 		load_info;
extern int read_inferior_memory (CORE_ADDR, char *, int);
extern int write_inferior_memory (CORE_ADDR, char *, int);

/* jini: Mixed mode support for PA32 libraries. JAGag21714 */
extern void
som_solib_load_symbols (struct so_list *so,
                        char *name,
                        int from_tty,
                        CORE_ADDR text_addr,
                        struct target_ops *target);

extern int
som_solib_sizeof_symbol_table (char *filename);

extern CORE_ADDR
get_load_info_dld_data_start();

/* is_executable
   Called from dlgetmodinfo1 to determine if the load module descriptor
   belongs to the executable 

   Check if this is the executable by comparing with the entry at index -2
   If there is no entry at index -2 compare the text_base with the
   default text base of an executable.
 */

int
is_executable (struct load_module_desc *desc,
	      size_t desc_size,
	      void *(*read_tgt_mem) (void *buffer,
				     uint64_t ptr,
				     size_t bufsiz,
				     int ident),
	      int ident_parm,
	      uint64_t load_map_parm)
{
  struct load_module_desc desc1;
  if (dlgetmodinfo (-2, &desc1, desc_size, read_tgt_mem, ident_parm,
                    load_map_parm) == NULL)
    /* Check if the text base corresponds to the executable */
    if ((desc->text_base == 0x4000000000000000 ) ||
	(desc->text_base == 0x4000000))
	return 1;
    else
	return 0;
  if (desc1.text_base == desc->text_base)
       return 1;
  return 0;
}

bool
in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile)
{

  if (exec_ia64 != xmode_exec_format)
    {
      return FALSE;
    }

  if (objfile && objfile->is_mixed_mode_pa_lib)
    {
      return TRUE;
    }

  addr = SWIZZLE (addr);
  if (NULL == objfile)
    {
      ALL_OBJFILES (objfile)
        {
          if (   (   addr >= SWIZZLE (objfile->text_low) 
	          && addr < SWIZZLE (objfile->text_high))
              || (   addr >= SWIZZLE (objfile->data_start)
                  && addr < SWIZZLE (objfile->data_start + objfile->data_size)))
	    {
              break;
            }
        }
    } /* for every objfile */

  return objfile_is_mixed_mode (objfile);
}


bool objfile_is_mixed_mode (struct objfile *objfile)
{
  struct som_exec_data  *somdata = NULL;
  unsigned short         e_machine = 0;

  if (NULL == objfile)
    {
      return FALSE;
    }

  if (TRUE == objfile->is_mixed_mode_pa_lib)
    {
      return TRUE;
    }

  e_machine = elf_elfheader(objfile->obfd)->e_machine;
  if (e_machine == EM_IA64)
    {
      return FALSE;
    }

  /* jini: 060511: Check for PA64 libraries. */
  if (e_machine == EM_PARISC)
    {
      /* Yes, mixed mode PA64 lib.*/
      objfile->is_mixed_mode_pa_lib = TRUE;
      return TRUE;
    }

  /* JAGag21715:
     jini: Mixed mode support: Sept 2006: Check for SOM PA32 libaries. */
  somdata = obj_som_exec_data (objfile->obfd);
  if ((somdata->the_magic == DL_MAGIC) || (somdata->the_magic == SHL_MAGIC))
    {
      /* Yes, mixed mode SOM lib.*/
      objfile->is_mixed_mode_pa_lib = TRUE;
      return TRUE;
    }

  return FALSE;
}


/* dlgetmodinfo1
   wrapper for dlgetmodinfo to provide old documented behaviour of
   providing executable at position 0 and libraries at positions 1 thru n
   with dld at -1.

   Only provides different behaviour from dlgetmodinfo if BIND_FIRST is used.
   Check if this is the executable by comparing with the entry at index -2
   If there is no entry at index -2 compare the text_base with the
   default text base of an executable.
 */

uint64_t
dlgetmodinfo1 (int index,
	      struct load_module_desc *desc,
	      size_t desc_size,
	      void *(*read_tgt_mem) (void *buffer,
				     uint64_t ptr,
				     size_t bufsiz,
				     int ident),
	      int ident_parm,
	      uint64_t load_map_parm)
{
  int i;

  if (debug_traces > 200)
  {
    printf_filtered("dlgetmodinfo1 - index = %d, desc = %llx, "
                    "desc_size = %d, ident_parm = %d, "
                    "load_map_parm = %llx\n",
                    index, desc, desc_size, ident_parm, load_map_parm);
  }

  /* Is BIND_FIRST used ? */
  if (dlgetmodinfo (0, desc, desc_size, read_tgt_mem, ident_parm,
                    load_map_parm) == NULL)
    /* call to dlgetmodinfo failed, default to its return value */
    return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, ident_parm, 
                         load_map_parm);
  if (is_executable (desc, desc_size, read_tgt_mem, ident_parm, load_map_parm))
     return dlgetmodinfo(index, desc, desc_size, read_tgt_mem, ident_parm, 
                         load_map_parm);

   /* dld's index */
    if (index == -1)
         return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, 
	                      ident_parm, load_map_parm);

   /* find location of executable */
   for (i=0; ; i++)
      if (dlgetmodinfo (i, desc, desc_size, read_tgt_mem, ident_parm,
	                load_map_parm) == NULL)
	  return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, 
	                       ident_parm, load_map_parm);
       else if (is_executable (desc, desc_size, read_tgt_mem, ident_parm, 
                               load_map_parm))
	 break;

    /* Asking for executable */
    if (index == 0)
         return dlgetmodinfo (i, desc, desc_size, read_tgt_mem, 
	                      ident_parm, load_map_parm);
	
    /* Shared library is before executable on list */
    if (i>=index)
	  return dlgetmodinfo (index-1, desc, desc_size, read_tgt_mem, 
	                       ident_parm, load_map_parm);
    else
      /* Shared library is after executable on list */
      return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, 
	                   ident_parm, load_map_parm);
}

char *
  dlgetname (struct load_module_desc *desc,
	     size_t desc_size,
	     void *(*read_tgt_mem) (void *buffer,
				    uint64_t ptr,
				    size_t bufsiz,
				    int ident),
	     int ident_parm,
	     uint64_t load_map_parm);

extern CORE_ADDR uld_text_addr;

/*--------------------------------------------------------------------------
   Defines
     BREAK_DE_SVC_LOADED	"service loaded" event - e.g. loaded dld
     BREAK_DE_LIB_LOADED	Library loaded, but initializers not executed
     BREAK_DE_LIB_UNLOADED	library unloaded
     BREAK_DE_LOAD_COMPLETE	library loaded and initializers executed.
     BREAK_DE_BOR		bind on reference symbol has been bound
--------------------------------------------------------------------------*/

#define BREAK_DE_SVC_LOADED      1  /* "service loaded" event */
#define BREAK_DE_LIB_LOADED      2  /* "library loaded" event */
#define BREAK_DE_LIB_UNLOADED    3  /* "library unloaded" event */
#define BREAK_DE_LOAD_COMPLETE   4  /* "loading complete" event */
#define BREAK_DE_BOR		 5

#ifndef O_BINARY
#define O_BINARY 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/*--------------------------------------------------------------------------
   Type Declarations
--------------------------------------------------------------------------*/

typedef union 
    {
      int int_buf[BUNDLE_NBR_INTS];
      unsigned char char_buf[BUNDLE_SIZE];
    } inst_union_t;

typedef union
    {
	struct {
		uint64_t	h;
		uint64_t	l;
	} halves;
	struct {
		uint64_t	slot2:		41;
		uint64_t	slot1hi:	23;
		uint64_t	slot1lo:	18;
		uint64_t	slot0:		41;
		uint64_t	template:	 5;
	} slots;
    } bundle_t;

/*--------------------------------------------------------------------------
   Data Declarations
--------------------------------------------------------------------------*/


typedef struct catch_load_unload_event
  {
    struct breakpoint *breakpt;
    char *filename;
    struct catch_load_unload_event *next;
  }
catch_load_unload_event_t;

catch_load_unload_event_t *catch_load_list = NULL;
catch_load_unload_event_t *catch_unload_list = NULL;

/* Mixed mode support: Have so_list_head accessible from somsolib.c too.*/
struct so_list *so_list_head;

static struct so_list *so_list_tail;

/* This is the cumulative size in bytes of the symbol tables of all
   shared objects on the so_list_head list.  (When we say size, here
   we mean of the information before it is brought into memory and
   potentially expanded by GDB.)  When adding a new shlib, this value
   is compared against the threshold size, held by auto_solib_add
   (in megabytes).  If adding symbols for the new shlib would cause
   the total size to exceed the threshold, then the new shlib's symbols
   are not loaded.
 */
static LONGEST hp_ia64_solib_total_st_size;

/* These addresses should be filled in by hp_ia64_solib_create_inferior_hook.
   They are also used elsewhere in this module.
 */
typedef struct
  {
    CORE_ADDR address;
    struct unwind_table_entry *unwind;
  }
addr_and_unwind_t;

/* When adding fields, be sure to clear them in _initialize_hp_ia64_solib. 
   or really hp_ia64_solib_restart which is called by it.
 */
typedef struct
  {
    CORE_ADDR dld_break_addr;
    CORE_ADDR dld_flags_addr;
    long long dld_flags;
    sec_ptr dyninfo_sect;
    boolean have_read_dld_descriptor;
    boolean is_archive_bound;
    boolean is_bor;
    boolean is_valid;
    CORE_ADDR load_map;
    CORE_ADDR load_map_addr;
    CORE_ADDR lre_debug_base_addr;
    CORE_ADDR lre_debug_base;
#if 0
    CORE_ADDR exe_timestamp;
    CORE_ADDR exe_checksum;
#endif
    struct load_module_desc dld_desc;
    struct load_module_desc main_prog_desc;
  }
dld_cache_t;

static dld_cache_t dld_cache;

static boolean threshhold_warning_given = FALSE;

/* When examining the shared library for debugging information we have to
   look for HP debug symbols, stabs and dwarf2 debug symbols.  */
static char *ia64_debug_section_names[] = {
  ".debug_info", ".debug_abbrev", ".debug_line", ".debug_pubnames", 
  ".debug_pubnames", ".debug_aranges", ".debug_loc", ".debug_macinfo", 
  ".debug_actuals", ".debug_str", NULL
};

static char *gdb_shlib_root = 0;
char **gdb_shlib_path_list = 0;
int gdb_shlib_path_length = 0;

/*--------------------------------------------------------------------------
   Forward declarations 
--------------------------------------------------------------------------*/

static void
hp_ia64_solib_add_solib_objfile PARAMS ((struct so_list *, boolean, boolean));

static void
hp_ia64_solib_create_catch_load_unload_hook PARAMS ((int,
						     char *,
						     char *,
					       catch_load_unload_event_t **,
						     const char *));

static void
hp_ia64_solib_sharedlibrary_command PARAMS ((char *args, int from_tty));

static LONGEST
  hp_ia64_solib_sizeof_symbol_table PARAMS ((char *, enum mixed_mode_type_t*));

static void
hp_ia64_sharedlibrary_info_command PARAMS ((char *, int));

static void
hp_ia64_solib_load_symbols PARAMS ((struct so_list *,
				    boolean,
				    struct target_ops *));

static boolean
  read_dynamic_info PARAMS ((asection * shlib_info, dld_cache_t * dld_cache_p));

static boolean
  read_dld_descriptor PARAMS ((struct target_ops * target));


static void *
  read_tgt_mem (void *buffer, uint64_t ptr, size_t bufsiz, int ident);

void
set_trace_bit (int);

int 
is_solib_mapped_private (void);

/*--------------------------------------------------------------------------
   Code Definitions
--------------------------------------------------------------------------*/

/* The followig routines are found in alphabetical order:
   _initialize_hp_ia64_solib
   add_to_solist
   add_to_solist_1
   handle_dynlink_load_event
   handle_dynlink_unload_event
   hp_ia64_dynlink_delete_event
   hp_ia64_dynlink_register_bp
   hp_ia64_solib_add
   hp_ia64_solib_add_solib_objfile
   hp_ia64_solib_address   
   hp_ia64_solib_at_dynlink_hook
   hp_ia64_solib_create_catch_load_hook
   hp_ia64_solib_create_catch_load_unload_hook
   hp_ia64_solib_create_catch_unload_hook
   hp_ia64_solib_create_inferior_hook
   hp_ia64_solib_get_got_by_pc
   hp_ia64_solib_get_solib_by_pc
   hp_ia64_solib_handle_dynlink_event
   hp_ia64_solib_have_load_event
   hp_ia64_solib_have_unload_event
   hp_ia64_solib_in_dynamic_linker
   hp_ia64_solib_load_symbols
   hp_ia64_solib_loaded_library_pathname
   hp_ia64_solib_remove_inferior_hook
   hp_ia64_solib_restart
   hp_ia64_solib_section_offsets
   hp_ia64_solib_sharedlibrary_command
   hp_ia64_solib_sizeof_symbol_table
   hp_ia64_sharedlibrary_info_command
   hp_ia64_solib_unloaded_library_pathname
   read_dld_descriptor
   read_dynamic_info
   read_tgt_mem
   set_trace_bit
   solib_bor_event
   start_bor_call
 */

/* _initialize_hp_ia64_solib */

void
_initialize_hp_ia64_solib ()
{
  char *gdb_shlib_path = NULL;
  int i;
  struct rlimit rlp;
  char *s;
  const char *token;

  add_com ("sharedlibrary", class_files, hp_ia64_solib_sharedlibrary_command,
	   "Load shared object library symbols for files matching REGEXP.\n\n\
Usage:\n\tsharedlibrary [<REGEXP>]\n\n");
  add_info ("sharedlibrary", hp_ia64_sharedlibrary_info_command,
	    "Status of loaded shared object libraries.\n\nUsage:\n\tinfo share\
\n\t or\n\tinfo sharedlibrary\n");
  add_show_from_set
    (add_set_cmd ("auto-solib-add", class_support, var_zinteger,
		  (char *) &auto_solib_add,
		  "Set autoloading size threshold (in megabytes) of shared library symbols.\n\n\
Usage:\nTo set new value:\n\tset auto-solib-add <INTEGER>\nTo see current value:\n\tshow \
auto-solib-add\n\n\
If nonzero, symbols from all shared object libraries will be loaded\n\
automatically when the inferior begins execution or when the dynamic linker\n\
informs gdb that a new library has been loaded, until the symbol table\n\
of the program and libraries exceeds this threshold.\n\
Otherwise, symbols must be loaded manually, using `sharedlibrary'.",
		  &setlist),
     &showlist);

  /* ??rehrauer: On HP-UX, the kernel parameter MAXDSIZ limits how much
     data space a process can use.  We ought to be reading MAXDSIZ and
     setting auto_solib_add to some large fraction of that value.  If
     not that, we maybe ought to be setting it smaller than the default
     for MAXDSIZ (that being 64Mb, I believe).  However, [1] this threshold
     is only crudely approximated rather than actually measured, and [2]
     50 Mbytes is too small for debugging gdb itself.  Thus, the arbitrary
     100 figure.
   */
  /* RM: Using MAXDSIZ to set the default threshold now */
  /* auto_solib_add = 100;  */

  getrlimit (RLIMIT_DATA, &rlp);
  auto_solib_add = (int) (((rlp.rlim_cur / 1000000) * 3) / 4);

  /* RM: initialize gdb_shlib_root */
  s = getenv("GDB_SHLIB_ROOT");
  if (s)
    {
      gdb_shlib_root = xmalloc(strlen(s) + 1);
      strcpy(gdb_shlib_root, s);
    }
  else
    {
      gdb_shlib_root = 0;
    }

  /* srikanth, 001117, Consult environment for GDB_SHLIB_PATH and
     if defined use that as the directory where the libraries will
     be found. This is useful for debugging core files produced on
     a different system. (JAGab25944)

     stacey, 6/29/2001 GDB_SHLIB_PATH can be either a single directory
     as mentioned above or a colon separated list of directories where
     the libraries will be found.  If the user enters a colon separated
     list, gdb will search the directories in the order the user has 
     entered them from first to last.  The code below parses the user's
     colon separated text into separated library names and stores them 
     in gdb_shlib_path - a pointer to a dynamically allocated array of 
     directory names (JAGab25944)  */

  s = getenv("GDB_SHLIB_PATH");
  gdb_shlib_path = s ? strdup (s) : s;

  if (gdb_shlib_path != NULL)
    {
      gdb_shlib_path_length = 1;

      /* At this point, we know the user has entered at least one
	 shared library path.  Count the colons in the gdb_shlib_path 
	 string to find out how many additional directory paths the 
	 user has entered.  */

      for (i = 0; gdb_shlib_path[i] != NULL; i++)
	if (gdb_shlib_path[i] == ':')
	  gdb_shlib_path_length++;      

      /* Now parse the gdb_shlib_path and separate into different directories
	 Treat ':'s as separators */

      gdb_shlib_path_list = xmalloc (gdb_shlib_path_length * sizeof (char *));

      for (token = strtok (gdb_shlib_path, ":"), i=0;
	   token != NULL;
	   token = strtok (NULL, ":"), i++)
	gdb_shlib_path_list[i] = strdup (token);

      free (gdb_shlib_path);
    }

  hp_ia64_solib_restart ();
}				/* _initialize_hp_ia64_solib */

/* add_to_solist
   Called from handle_dynlink_load_event and hp_ia64_solib_add to add
   a shared library to so_list_head list and possibly to read in the
   debug information for the library.  

   If load_module_desc_p is NULL, then the load module descriptor must
   be read from the inferior process at the address load_module_desc_addr.
 */

static void
add_to_solist (boolean from_tty,
	       char *dll_path,
	       struct load_module_desc *load_module_desc_p,
	       CORE_ADDR load_module_desc_addr,
	       struct target_ops *target)
{
  static char altname[MAXPATHLEN + 1];
  static char copy[MAXPATHLEN + 1];
  struct so_list *new_so;
  int hp_ia64_solib_st_size_threshhold_exceeded;
  int gdb_shlib_list_index = 0;
  struct stat statbuf;
  int status;
  LONGEST st_size;
  struct so_list *next_solist, *saved_so;
  /*jini: mixed mode support: JAGag21714 */
  enum mixed_mode_type_t mixed_mode_type = MIXED_MODE_NONE;

  /* We come here many time to add symbols from dyamicaly loaded libraries.
     If user told gdb not to read in symbols from a.out then, symfile_objfile
     will be null.
     one command line option is "-e" which does not set symarg variable in
     captured_main(). which causes gdb not to read symbols from a.out.
     Printing warning message for this has been taken care in 
     hp_ia64_solib_create_inferior_hook().
  */
  if (symfile_objfile == NULL || symfile_objfile->obfd == NULL)
   return;

 /* JAGae68084 capture the name of the shared library loaded*/
  if (shllib_name)
    free (shllib_name);
  shllib_name=pathopt(dll_path);

  /* see if the entry for this lib is already in the solist */
  saved_so = NULL;
  next_solist = so_list_head;
  while (next_solist && !saved_so)
    {
      if (strcmp (next_solist->name, dll_path) == 0)
	{
          saved_so = next_solist;
        }
      next_solist = next_solist->next;
    }

  /* Add the shared library to the so_list_head list */
  if (saved_so == NULL)
    {
      new_so = (struct so_list *) xmalloc (sizeof (struct so_list));
      memset ((char *) new_so, 0, sizeof (struct so_list));
      new_so->solib_desc = xcalloc (1, sizeof (struct load_module_desc));
      if (so_list_head == NULL)
        {
          so_list_head = new_so;
          so_list_tail = new_so;
        }
      else
        {
          so_list_tail->next = new_so;
          so_list_tail = new_so;
        }
    }
  else
    {
      new_so = saved_so;
    }

  /* Initialize the new_so */

  if (load_module_desc_p)
    {
      *((struct load_module_desc *)(new_so->solib_desc)) = *load_module_desc_p;
    }
  else
    {
      if (target_read_memory (load_module_desc_addr,
			      (char *) new_so->solib_desc,
			      sizeof (struct load_module_desc))
	  != 0)
	{
	  error ("Error while reading in dynamic library %s", dll_path);
	}
    }				/* else load_module_desc_p is NULL */

  LRE_FLIP_STRUCT (new_so->solib_desc, desc_load_module_desc);

  new_so->desc_addr = load_module_desc_addr;
  new_so->loaded = TRUE;

  /* srikanth, JAGab25944, 001117. If gdb_shlib_path_list is defined and
     we are debugging core files, treat that directory as the place
     where the library will be found. This is useful for analyzing
     core files on a system other than the one that produced the
     core without having to create a hierarchy as the gdb_shlib_root
     method requires.

     stacey, JAGab25944, 6/29/2001
     the for loop below enhances gdb to search multiple directories
     for the appropriately named shared library.  The gdb_shlib_path_list
     variable is a pointer to an array of path names.  Which are obtained
     from the user's GDB_SHLIB_PATH environment variable.  GDB will search
     the paths in the order in which they are entered by the user: from
     first to last.  So if the user types:

     export GDB_SHLIB_PATH=libs1:libs2:libs3
     
     GDB will search libs1, then libs2, and finally libs3 and will stop
     as soon as it finds the library it wants.  Note: this implementation
     assumes that all library names are unique. */

  if (gdb_shlib_path_list && !target_has_execution)
    {

      for (gdb_shlib_list_index=0; 
	   gdb_shlib_list_index < gdb_shlib_path_length; 
	   gdb_shlib_list_index++)
	{
	  sprintf (copy, "%s%s", 
		   gdb_shlib_path_list[gdb_shlib_list_index], 
		   strrchr (dll_path, '/'));

	  if (stat (copy, &statbuf) != -1)
	    {
	      dll_path = copy;
	      break;
	    }
	}
    }
  else if (gdb_shlib_root && !target_has_execution)
    {
      /* RM: If we find this library in gdb_shlib_root _and_ we are debugging
	 core files, use that version instead. Useful when analyzing core files
	 on a system other than the one that produced the core
      */
      strcpy (altname, gdb_shlib_root);
      strcat (altname, dll_path);
      status = stat (altname, &statbuf);
      if (status != -1) {
	dll_path = altname;
      }
    }
  new_so->name = obsavestring (dll_path, (int) strlen (dll_path),
			       &symfile_objfile->symbol_obstack);

  /* If we are not going to load the library, tell the user if we
     haven't already and return.  
   */
  /* JAGag21714:
     jini: mixed mode changes: hp_ia64_solib_sizeof_symbol_table would call
     som_solib_sizeof_symbol_table or pa64_solib_sizeof_symbol_table
     if the library is a PA library */

  st_size = hp_ia64_solib_sizeof_symbol_table (dll_path, &mixed_mode_type);
  /* MIXED_MODE_64 is unused currently. */
  new_so->is_mixed_mode_pa_lib = (    MIXED_MODE_32 == mixed_mode_type
                                   || MIXED_MODE_64 == mixed_mode_type);

  hp_ia64_solib_st_size_threshhold_exceeded =
    !from_tty && 
      ((auto_solib_add == 0) || 
       ((st_size + hp_ia64_solib_total_st_size)
        > (auto_solib_add * (LONGEST) 1000000)));
  /* FIXME: jini: mixed mode changes: Enable mixed mode support for
     cases where the threshold has exceeded. */
  if (hp_ia64_solib_st_size_threshhold_exceeded &&
      !new_so->is_mixed_mode_pa_lib)
    {
      if (auto_solib_add && !threshhold_warning_given)
	warning ("Symbols for some libraries have not been loaded, because\ndoing so would exceed the size threshold specified by auto-solib-add.\nTo manually load symbols, use the 'sharedlibrary' command.\nTo raise the threshold, set auto-solib-add to a larger value and rerun\nthe program.\n");
      threshhold_warning_given = TRUE;

      hp_ia64_solib_add_solib_objfile (new_so, from_tty, 1);
      return;
    }

  /* Now read in debug info. */

  hp_ia64_solib_total_st_size += st_size;

  /* This fills in new_so->objfile, among others. */
  if (new_so->is_mixed_mode_pa_lib && MIXED_MODE_32 == mixed_mode_type)
    {
       /* jini: mixed mode changes: JAGag21714: Load symbols from the PA32
          library */
        som_solib_load_symbols (
                          new_so,
                          dll_path,
                          from_tty,
                          ((struct load_module_desc *)
                            (new_so->solib_desc))->text_base,
                          target);
    }
  else 
    {
      hp_ia64_solib_load_symbols (new_so,
			          from_tty,
			          target);
    }
  return;
}				/* end add_to_solist */

/* Use add_to_solist_1 with catch_errors instead. */
typedef struct {
  boolean from_tty;
  char *dll_path;
  struct load_module_desc *load_module_desc_p;
  CORE_ADDR load_module_desc_addr;
  struct target_ops *target;
} args_for_add_to_solist;

int
add_to_solist_1 (PTR args)
{
  args_for_add_to_solist *args1 = args;
  add_to_solist (args1->from_tty, args1->dll_path, args1->load_module_desc_p,
                 args1->load_module_desc_addr, args1->target);
  return 1;
}


/* check whether shared libs are privately mapped */
int 
is_solib_mapped_private (void)
{
   return (dld_cache.dld_flags & DT_HP_DEBUG_PRIVATE);
}

/* at_svc_load_break - return TRUE if stop_pc is a break instruction with
   an immediate value that indicates that this is a service load breakpoint.

   Read in the instruction bundle and examine the instruction.

   Otherwize, return FALSE.
   */

boolean
at_svc_load_break (CORE_ADDR stop_pc)
{
  char		bundle_buf[BUNDLE_SIZE];
  CORE_ADDR 	bundle_addr;
  char 		flip_buf[BUNDLE_SIZE];
  int		i;
  int 		slot_no;
  inst_union_t	inst_buf;
  bundle_t	bundle;
  unsigned long long	instruction;
  struct minimal_symbol * m;

  bundle_addr = (stop_pc & (~0xF));
  slot_no = (int) (stop_pc % 16);

  if (target_read_memory (bundle_addr, (char*) &bundle_buf, BUNDLE_SIZE) != 0)
    return FALSE;

  /* To examine the instruction, we must first flip the little-endian bytes */

  for (i = 0;  i < BUNDLE_SIZE;  i++)
    inst_buf.char_buf[BUNDLE_SIZE - 1 - i] = bundle_buf[i];
  memcpy (&bundle, &inst_buf, BUNDLE_SIZE);
  instruction = 0; /* initialize for compiler warning */
  switch (slot_no)
    {
    case 0:
      instruction = bundle.slots.slot0;
      break;

    case 1:
      instruction = (bundle.slots.slot1hi << 18) | bundle.slots.slot1lo;
      break;

    case 2:
      instruction = bundle.slots.slot2;
      break;
    }
  
  if (instruction == 0x1c0c9c0LL)
    return TRUE;

  /* In IC8 uld started using:  break.i          0xf0327 
     which gives us 0x3c0c9c0
  */

  if (instruction == 0x3c0c9c0)
    return TRUE;
  else
    {
     /* Hack : FIXME : is the slot number computation above correct ???*/
      m = lookup_minimal_symbol_by_pc (stop_pc);
      if (m && !strcmp (SYMBOL_NAME(m), "_asm_break"))
        return TRUE;
    }
    return FALSE;

} /* end at_svc_load_break */


static void clear_solib_info(struct so_list *solib)
{
  struct symtab *s;

  /* Diable breakpoints in this shlib */
  disable_breakpoints_in_shlibs(!info_verbose, (void*) solib);

  /* Clear current_source_symtab, if applicable */
  ALL_OBJFILE_SYMTABS(solib->objfile, s)
    {
      if (current_source_symtab == s) current_source_symtab = NULL;
    }
}


/* handle_dynlink_load_event
   Called by hp_ia64_solib_handle_dynlink_event to handle a load event.
   Extract the relevant information from the break routine parameters
   and then process loading the shared library.

   Return 1 if gdb should stop for input, 0 if not. 
 */

static int
handle_dynlink_load_event (int pid)
{
  catch_load_unload_event_t *catch_event;
  char dll_path[MAXPATHLEN];
  CORE_ADDR dll_path_addr;
  CORE_ADDR load_module_desc_addr;
  CORE_ADDR load_module_desc_size;
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;

  /* extract the various args, ARG0 had the load event indicator
     ARG1       &(struct load_module_desc)
     ARG2       sizeof(struct load_module_desc)
     dll_path_addr      dll_path_address
   */

  load_module_desc_addr = read_register (ARG1_REGNUM);
  load_module_desc_size = read_register (ARG2_REGNUM);
  dll_path_addr = read_register (ARG3_REGNUM);
  dll_path_addr = SWIZZLE (dll_path_addr);

  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);
  if (   info_verbose
      && (load_module_desc_size > sizeof (struct load_module_desc)))
    {
      warning ("Version of shared library %s not understood; there may be new features in the library that gdb does not support\n", dll_path);
    }
  add_to_solist (FALSE, dll_path, NULL, load_module_desc_addr, NULL);

  /* If the user has requested load notifications, then report it now. */
  catch_event = catch_load_list;
  while (catch_event)
    {
      /* if filename is NULL, the user wants notification for all libraries */
      if (   (   catch_event->filename == NULL 
              || strcmp (catch_event->filename, dll_path) == 0)
	  && catch_event->breakpt->enable != disabled
	  && is_stop_for_bp (catch_event->breakpt)
	 )
	{
	  printf_unfiltered ("Catchpoint %d (loaded %s)\n",
			     catch_event->breakpt->number,
			     dll_path);
	  inferior_pid = saved_inferior_pid;
	  stop_bpstat = bpstat_stop_status (&stop_pc, 0);
	  stop_for_this_bp (catch_event->breakpt, stop_bpstat);
	  return 1;		/* stop for input */
	}			/* end if */
      catch_event = catch_event->next;
    }				/* end while catch_event */

  inferior_pid = saved_inferior_pid;
  return 0;			/* don't stop for input */
}				/* end handle_dynlink_load_event */


/* handle_dynlink_unload_event
   Called by hp_ia64_solib_handle_dynlink_event to handle an unload event.
   Extract the relevant information from the break routine parameters
   and then process unloading the shared library.
 */

static int
handle_dynlink_unload_event (int pid)
{
  catch_load_unload_event_t *catch_event;
  char dll_path[MAXPATHLEN];
  CORE_ADDR dll_path_addr;
  boolean found_solib;
  CORE_ADDR load_module_desc_size;
  static struct so_list *next_solist;
  static struct so_list *prev_solist_p;
  static struct so_list **prev_solist_pp;
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;

  /* extract the various args, ARG0 had the unload event indicator
     ARG1       &(struct load_module_desc)
     ARG2       sizeof(struct load_module_desc)
     dll_path_addr      dll_path_address
   */

  /* skip the "load_module_desc_addr */
  (void) read_register (ARG1_REGNUM);
  load_module_desc_size = read_register (ARG2_REGNUM);
  dll_path_addr = read_register (ARG3_REGNUM);

  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);
  if (   info_verbose
      && (load_module_desc_size > sizeof (struct load_module_desc)))
    {
      warning ("Version of shared library %s not understood; there may be new features in the library that gdb does not support\n", dll_path);
    }

  /* Mark the shared library as unloaded by setting loaded to FALSE.
     Also set desc_addr to NULL. */

  found_solib = FALSE;
  next_solist = so_list_head;
  prev_solist_p = NULL;
  prev_solist_pp = &so_list_head;
  while (next_solist && !found_solib)
    {
      if (strcmp (next_solist->name, dll_path) == 0)
	{
	  found_solib = TRUE;
	  next_solist->desc_addr = NULL;
	  next_solist->loaded = FALSE;

	  /* If we never loaded debug info for this library, we can
	     forget about it and remove it from the so_list_head list.
	   */

	  if (next_solist->objfile == NULL)
	    {
	      /* This might update so_list_head */
	      *prev_solist_pp = next_solist->next;
	      if (so_list_tail == next_solist)
		so_list_tail = prev_solist_p;
	    }
	}			/* if strcmp == 0 */
      prev_solist_pp = &next_solist->next;
      prev_solist_p = next_solist;
      next_solist = next_solist->next;
    }				/* end while */

  if (found_solib)
    {
      if (!prev_solist_p->objfile)
        {
          free (prev_solist_p->solib_desc);
          free (prev_solist_p);
        }
      else
        {
         /* Disable the watchpoint expressions(watch on shared lib space)
            that aren`t valid after unloading shlib and removing debug information */
          disable_break_exp_chain (prev_solist_p->objfile);
          /* clear debug info, breakpoints, etc. */
          clear_solib_info(prev_solist_p);
          if (rtc_dot_sl == prev_solist_p->objfile)
            rtc_dot_sl = NULL;
          free_objfile(prev_solist_p->objfile);
          prev_solist_p->objfile = NULL;
        }
    }

  /* If the user has requested unload notifications, then report it now. */

  catch_event = catch_unload_list;
  while (catch_event)
    {
      /* if filename is NULL, the user wants notification for all libraries */
      if (   (   catch_event->filename == NULL
	      || strcmp (catch_event->filename, dll_path) == 0)
	  && catch_event->breakpt->enable != disabled
	  && is_stop_for_bp (catch_event->breakpt))
	{
	  printf_unfiltered ("Catchpoint %d (unloaded %s)\n",
			     catch_event->breakpt->number,
			     dll_path);
	  inferior_pid = saved_inferior_pid;
	  stop_bpstat = bpstat_stop_status (&stop_pc, 0);
	  stop_for_this_bp (catch_event->breakpt, stop_bpstat);
	  return 1;		/* stop for input */
	}			/* end if */
      catch_event = catch_event->next;
    }				/* end while catch_event */

  inferior_pid = saved_inferior_pid;
  return 0;			/* don't stop for input */
}				/* end handle_dynlink_unload_event */


/* hp_ia64_dynlink_delete_event DYNLINK_DELETE_EVENT
   Remove the indicated catch_load_unload_event_t from the appropriate list
 */
void 
hp_ia64_dynlink_delete_event (void *void_event_p)
{
  catch_load_unload_event_t *event_p;
  catch_load_unload_event_t **event_list;

  if (void_event_p == NULL)
    return;
  event_p = (catch_load_unload_event_t *) void_event_p;
  if (event_p->breakpt->type == bp_catch_load)
    {
      event_list = &catch_load_list;
    }
  else
    {
      assert (event_p->breakpt->type == bp_catch_unload);
      event_list = &catch_unload_list;
    }

  /* Remove the event from the event list */

  if (*event_list == event_p)
    {
      *event_list = event_p->next;
    }
  else
    {
      while (*event_list && (*event_list)->next != event_p)
	{
	  event_list = &((*event_list)->next);
	}
      if (*event_list)
	{
	  (*event_list)->next = event_p->next;
	}
    }

  free (event_p->filename);
  free (event_p);
}				/* end hp_ia64_dynlink_delete_event */

/* hp_ia64_dynlink_register_bp DYNLINK_REGISTER_BP
   Place a pointer to the breakpoint structure in the catch_load_unload_event_t
   structure pointer passed in the void* pointer
 */
void 
hp_ia64_dynlink_register_bp (void *void_event_p, struct breakpoint *breakpt)
{
  catch_load_unload_event_t *event_p;

  event_p = (catch_load_unload_event_t *) void_event_p;
  event_p->breakpt = breakpt;
}				/* end hp_ia64_dynlink_register_bp */


/* jini: Mixed mode changes. Load the libaries library in gdb so that
   we can use it for reading in the values of the registers.
 */
static void
init_and_load_aries (struct load_module_desc *dll_desc)
{
  char *aries_helper_lib = NULL;
  struct stat buf;
  int ret_value;

  inferior_aries_text_start = dll_desc->text_base;
  inferior_aries_text_end = dll_desc->text_base + dll_desc->text_size;

  if (mixed_mode_debug_aries || aries_helper_lib_loaded)
    {
      /* We are interested in debugging only the libaries library. So, there is
       * no need to load the aries helper library in gdb. */
      return;
    }

  aries_helper_lib = getenv ("GDB_ARIES_LIB");
  if (aries_helper_lib && stat (aries_helper_lib, &buf) == -1)
    {
      printf_filtered ("Bad aries helper library %s, errno %#d. The PA library "
                       "cannot be debugged.\n",
                        aries_helper_lib, errno);
      return;
    }
  else if (!aries_helper_lib)
    {
      aries_helper_lib = "/usr/lib/hpux64/libaries64.so";
    }

  /* Shload the Aries Helper library. */
  aries_shlib_handle = shl_load (aries_helper_lib,
    			         BIND_IMMEDIATE|BIND_VERBOSE, 0L);
  if (aries_shlib_handle == 0)
  {
    printf_filtered ("errno = %#d\n", errno);
    printf_filtered ("Bad shlib %s\n", aries_shlib_handle);
    printf_filtered ("Unable to load the aries helper library. The PA stack\n"
                     "frames will not be displayed.\n");
    mixed_mode_debug_aries = true;
    return;
  }

  aries_helper_lib_loaded = true;
  /* Initialize function pointers */
  ret_value = shl_findsym (&aries_shlib_handle,
    "aries_get_save_state", TYPE_PROCEDURE, &ptr_aries_get_save_state);
  if (ret_value == -1)
  {
    printf_filtered ("Incorrect aries helper library %s used.\n"
                     "The procedure aries_get_save_state could not "
                     "be found.\nThe PA stack frames will not be displayed.\n",
                      aries_helper_lib);
    mixed_mode_debug_aries = true;
    return;
  }

  ret_value = shl_findsym (&aries_shlib_handle,
    "aries_get_nbr_contexts", TYPE_PROCEDURE,
    &ptr_aries_get_nbr_contexts);
  if (ret_value == -1)
  {
    printf_filtered ("Incorrect aries helper library %s used.\n"
                     "The procedure aries_get_nbr_contexts could not "
                     "be found.\nThe PA stack frames will not be displayed.\n",
                      aries_helper_lib);
    mixed_mode_debug_aries = true;
    return;
  }

  ret_value = shl_findsym (&aries_shlib_handle,
    "aries_in_signal_handler", TYPE_PROCEDURE,
    &ptr_aries_in_signal_handler);
  if (ret_value == -1)
  {
    printf_filtered ("Incorrect aries helper library %s used.\n"
                     "The \nprocedure aries_in_signal_handler could not "
                     "be found.\nThe PA stack frames will not be displayed.\n",
                      aries_helper_lib);

    mixed_mode_debug_aries = true;
    return;
  }

  ret_value = shl_findsym (&aries_shlib_handle,
     "aries_get_pa2ia_export_stub_size", TYPE_PROCEDURE,
     &ptr_aries_get_pa2ia_export_stub_size);
  return;
}

/* hp_ia64_solib_add - SOLIB_ADD
   Add symbols from shared libraries into the symtab list, unless the
   size threshold (specified by auto_solib_add, in megabytes) would
   be exceeded.

   This routine is called when gdb attaches to a running program or when
   it has a core file.  It uses dlgetmodinfo1 and dlgetname to get 
   the load module descriptor and name of each library and then calls
   add_to_solist.

   Parameters:
   arg_string   - NULL.  In the HP-UX IA64 implementatation, the
   sharedlibrary command does not call this routine.
   See hp_ia64_solib_sharedlibrary_command.

   from_tty     - TRUE if the call is the result of a user command
   such as sharedlibrary.  Presumably FALSE.

   target               - array of target operation routines;  NULL if
   we are working with an inferior process.  
   &current_target if called for attach or
   a core file.
 */
extern int rtc_initialized;

void
hp_ia64_solib_add (char *arg_string, int from_tty, struct target_ops *target)
{
  struct load_module_desc dll_desc;
  struct minimal_symbol *m;
  int dll_index;
  char *dll_path;
  args_for_add_to_solist args;

  /* In HP-UX IA64, hp_ia64_solib_sharedlibrary_command does not call hp_ia64_solib_add.
     arg_string should be NULL.
   */
  assert (arg_string == NULL);

  /* If we haven't read in the load map pointer, do it now. */

  if (!dld_cache.have_read_dld_descriptor)
    if (!read_dld_descriptor (target))
      return;			/* done.  archive bound.  no shared libraries */

   /* Bindu 081503: Don't do this during attach. Otherwise, it will cause gdb
      to ignore library loading immediately after attach. */

   /* JAGaf15615 */
  if (!attach_flag && !rtc_initialized && check_heap_in_this_run && target_has_stack)
    snoop_on_the_heap ();


  /* srikanth, JAGaa93423, 990416, Alert the user if the shared libraries
     are not mapped private.  
     cdb, 041026, only if we've attached.
   */

  if (!batch_rtc && attach_flag && (!dld_cache.dld_flags & DT_HP_DEBUG_PRIVATE))
    if (profile_on)
      {
            /* We used to exit here with an error message. This was because
               command line calls didn't work quite well when shared libraries
               are not privately mapped. But with prospect v2.6, we don't
               rely on command line calls any more, so I got rid of the
               block here. The `if' is still needed to avoid the warning
               message below which is not useful for profiling mode.

                         - Srikanth, Nov 16th 2005.
            */
      }
    else
      {
        struct utsname uts;
        int sys_ver = 0;
        uname (&uts);
        if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
          warning ("uname: Couldn't resolve the release identifier of the "
                   "Operating system.");
        if (sys_ver < 31)
          warning ("The shared libraries were not privately mapped; setting "
                   "a\nbreakpoint in a shared library will not work until you"
                   " rerun the program;\nstepping over longjmp calls will " 
                   "not work as expected.\n Use the following command to "
                   "enable debugging of shared libraries.\n"
                   "chatr +dbg enable <executable>");
        else if (sys_ver == 31)
       	  {
            uint64_t value = 0;
            /* Check whether the varibale is set. If not present
               ask user to install the patches.
            */
            if (gettune((const char *)"shlib_debug_enable", &value) == 
		-1 && errno == ENOENT)
              {
	        warning ("The shared libraries were not privately mapped; "
		         "setting a\nbreakpoint in a shared library will not" 
                         " work until you rerun the program;\nstepping over"
			 " longjmp calls will not work as expected.\n"
                         "Use the following command to enable debugging of "
                         "shared libraries.\n"
                         "\"chatr +dbg enable <executable>\"\n or install the "
                         "patches PHKL_38651 and PHKL_38778");
    	      }
            else if (value == 0)
	      warning ("The shared libraries were not privately mapped; "
                       "setting a\nbreakpoint in a shared library will not"
                       " work until you rerun the program;\nstepping over"
                       " longjmp calls will not work as expected.\n"
		       "Please set the kernel variable \"shlib_debug_enable\""
                       " to 1 to enable the shared library debugging\n");
	  }	
      }
  /* For each shared library (the first has index 1), call add_to_solist.
     We must first obtain the load module descriptor and path.
  */

  for (dll_index = 1;; dll_index++)
    {
      unsigned long long lib_entry_p;

      /* Read in the load module descriptor */
      lib_entry_p = dlgetmodinfo1 (dll_index, &dll_desc, sizeof (dll_desc),
				   read_tgt_mem, 0, dld_cache.load_map);
      if (lib_entry_p  == NULL)
	  return;	/* Presumably we have reached the end of the list and are done. */

      /* Get the name of the shared library */

      dll_path = dlgetname (&dll_desc,
			    sizeof (dll_desc),
			    read_tgt_mem,
			    0,
			    dld_cache.load_map);
      if (!dll_path)
	{
	  /* FIXME - we are currently getting a problem which libraries
	     that were loaded with shl_load and then unloaded with shl_unload.
	     They should not be in the list, but dlgetmodinfo1 is returning
	     a descriptor but dlgetname is not finding the name.  We will
	     just continue past this one until dlgetmodinfo1 returns NULL 
	     instead of generating an error.
	     error ("hp_ia64_solib_add, error reading shared library path.");

	     Well, that didn't work well at Bloomberg.  Their executable
	     appeared on that list after this test was NULL, but with bad
	     data from a corefile that caused gdb to SIGSEGV.  Let's
	     take this as a hint that dlgetmodinfo() should have returned
	     NULL itself and bail now, while we're waiting for the linkoids
	     to fix this situation.
	   */

	  return; /* Presumably we have reached the end of the list and are done. */
	}

      /* If a core is being loaded, store the name of the library used in the core. */
      if (target_has_stack && !target_has_execution)
	  so_pathname_g = dll_path;

      args.from_tty = from_tty;
      args.dll_path = dll_path;
      args.load_module_desc_p = &dll_desc;
      args.load_module_desc_addr = 0;
      args.target = target;

      /* jini: Mixed mode corefile debug support.
       * Check if this is the libaries library. If so, store the text start
       * and the text end addresses. */
      if (    (0 == strncmp (basename (dll_path), "libaries", 8))
           && !target_has_execution)
        {
          init_and_load_aries (args.load_module_desc_p);
        }

      if (!catch_errors ((catch_errors_ftype *) add_to_solist_1,
                                        (char *) &args,
                                        NULL, RETURN_MASK_ALL))
	{
	  warning ("Error in reading symbols from %s... skipping\n", dll_path);
	}

    }				/* end for dll_index */
}				/* hp_ia64_solib_add */


/* hp_ia64_solib_add_solib_objfile - called by hp_ia64_solib_load_symbols
   Called to add symbols from a shared library to gdb's 
   symbol table. 
 */

static void
hp_ia64_solib_add_solib_objfile (struct so_list *so_ptr, int from_tty, boolean threshold_exceeded)
{
  char *name = so_ptr->name;
  obj_private_data_t *obj_private;
  struct load_module_desc *solib_desc = (struct load_module_desc *)
                                        (so_ptr->solib_desc);
  CORE_ADDR text_addr = solib_desc->text_base;
  struct section_addr_info section_addrs;
  asection *sec;
  bfd *tmp_bfd;
  bool objfile_is_pa = false;

  memset (&section_addrs, 0, sizeof (section_addrs));
  section_addrs.other[0].name = ".text";
  section_addrs.other[0].addr = solib_desc->text_base;
  section_addrs.other[1].name = ".data";
  section_addrs.other[1].addr = solib_desc->data_base;
  section_addrs.other[2].name = ".bss";
  section_addrs.other[2].addr = solib_desc->data_base;

  /* We need the BFD so that we can look at its sections.  We open up the
     file temporarily, then close it when we are done.  */
  tmp_bfd = symfile_bfd_open (so_ptr->name);
  if (tmp_bfd == NULL)
    {
      perror_with_name (so_ptr->name);
      return;
    }

  if (!bfd_check_format (tmp_bfd, bfd_object))
    {
      bfd_close (tmp_bfd);
      error ("\"%s\" is not an object file: %s", so_ptr->name,
             bfd_errmsg (bfd_get_error ()));
    }


  /* Undo some braindamage from symfile.c.

     First, symfile.c will subtract the VMA of the first .text section
     in the shared library that it finds.  Undo that.

     JYG: symfile.c will also subtract the VMA of the first .data / .bss
     section in the shared library that it finds.  Also pre-adjust
     to offset the 'braindamage' */
  sec = bfd_get_section_by_name (tmp_bfd, ".text");
  if (sec)
    {
      section_addrs.other[0].addr += bfd_section_vma (tmp_bfd, sec);
    }
  section_addrs.other[0].addr -= elf_text_start_vma (tmp_bfd);
  
  sec = bfd_get_section_by_name (tmp_bfd, ".data");
  if (sec)
    section_addrs.other[1].addr += bfd_section_vma (tmp_bfd, sec);
  section_addrs.other[1].addr -= elf_data_start_vma (tmp_bfd);
  
  sec = bfd_get_section_by_name (tmp_bfd, ".bss");
  if (sec)
    section_addrs.other[2].addr += bfd_section_vma (tmp_bfd, sec);
  section_addrs.other[2].addr -= elf_data_start_vma (tmp_bfd);

  /* We are done with the temporary bfd.  Get rid of it and make sure
     nobody else can us it.  */
  bfd_close (tmp_bfd);
  tmp_bfd = NULL;

  so_ptr->objfile = maybe_symbol_file_add (name, from_tty, &section_addrs, 
					   0, OBJF_SHARED, threshold_exceeded);
  so_ptr->objfile->text_low = SWIZZLE (solib_desc->text_base);
  so_ptr->objfile->text_high =  
    SWIZZLE (solib_desc->text_base + solib_desc->text_size);

  so_ptr->objfile->data_start = SWIZZLE (solib_desc->data_base);
  so_ptr->objfile->data_size = solib_desc->data_size;
  so_ptr->objfile->saved_dp_reg = solib_desc->linkage_ptr;
  so_ptr->objfile->is_archive_bound = FALSE;
  so_ptr->abfd = so_ptr->objfile->obfd;
  

  /* Mark this as a shared library and save private data.
   */
  so_ptr->objfile->flags |= OBJF_SHARED;

  objfile_is_pa = objfile_is_mixed_mode (so_ptr->objfile);
  if (    so_ptr->objfile->obj_private == NULL
       && !objfile_is_pa )
    {
      obj_private = (obj_private_data_t *)
	(void *) obstack_alloc (&so_ptr->objfile->psymbol_obstack,
		       sizeof (obj_private_data_t));
      obj_private->unwind_info = NULL;
      obj_private->opd = NULL;
      obj_private->so_info = NULL;
      obj_private->lmdp = NULL;
      so_ptr->objfile->obj_private = (PTR) obj_private;
    }
  else
    {
      obj_private = so_ptr->objfile->obj_private;
    }
  if (!objfile_is_pa && !obj_private->lmdp)
    {
      obj_private->lmdp = (struct load_module_desc *)
	(void *) obstack_alloc (&so_ptr->objfile->psymbol_obstack,
		       sizeof (struct load_module_desc));
      memcpy (obj_private->lmdp,
	      solib_desc,
	      sizeof (struct load_module_desc));
    }				/* if( so_ptr->objfile->obj_private == NULL ) */

  obj_private = (obj_private_data_t *) so_ptr->objfile->obj_private;
  if (!objfile_is_pa)
    obj_private->so_info = so_ptr;

  if (!bfd_check_format (so_ptr->abfd, bfd_object))
    {
      error ("\"%s\": not in executable format: %s.",
	     name, bfd_errmsg (bfd_get_error ()));
    }

  if (batch_rtc)
    {
      /* Store the text start and end addresses of librtc.so and libpthread.so
         so that these addresses can be compared against when we display the
         backtrace at event occurrence for batch mode thread check. */
      /* FIXFIX: Check if we can enable this only for the thread check case.
         trace_threads_in_this_run is not set at this point. */
      char *base_name = basename (name);
      if (0 == strncmp (base_name, "librtc", 6))
        mark_librtc_start_end (so_ptr->objfile->text_low,
                               so_ptr->objfile->text_high);
      else if (0 == strncmp (base_name, "libpthread", 10))
        mark_libpthread_start_end (so_ptr->objfile->text_low,
                                   so_ptr->objfile->text_high);
    }

}				/* hp_ia64_solib_add_solib_objfile */


boolean hp_ia64_this_solib_address (void *solib, CORE_ADDR addr)
{
  struct so_list *so = (struct so_list *) solib;
  struct load_module_desc *solib_desc = 
         (struct load_module_desc *)(so->solib_desc);

  /* Is this address within this shlib's text range? */
  return ((addr >= solib_desc->text_base)
          && (addr < solib_desc->text_base + solib_desc->text_size));
}


boolean hp_ia64_loaded_solib_address (CORE_ADDR addr)
{
  struct so_list *so = so_list_head;
  while (so)
   {
     if (hp_ia64_this_solib_address((void*) so, addr) && so->loaded)
       return true;
     so = so->next;
   }

  if (addr >= text_start && addr < text_end)
    return true;

  return false;
}


/* hp_ia64_solib_address - called by DISABLE_UNSETTABLE_BREAK
 */

char *
hp_ia64_solib_address (CORE_ADDR addr)
{
  struct so_list *so = so_list_head;

  while (so)
    {
      /* Is this address within this shlib's text range?  If so,
         return the shlib's name.
       */
      struct load_module_desc *solib_desc = 
         (struct load_module_desc *)(so->solib_desc);
      if ((addr >= solib_desc->text_base)
	  && (addr < solib_desc->text_base + solib_desc->text_size))
	return so->name;

      /* Nope, keep looking... */
      so = so->next;
    }

  /* No, we couldn't prove that the address is within a shlib. */
  return NULL;
}				/* hp_ia64_solib_address */

/* Checks the text low and text high of all shared libraries to find 
   if a given address is present in a shared library. 

   If not found it checks the main executable to see if address is part
   of that.

   Returns the name of the load module the address was found in
   else returns NULL.
 */

#ifdef PC_SOLIB
char *
solib_and_mainfile_address (CORE_ADDR addr)
{
  char *so_name = NULL;
  /* Check to see if the address is present in a shared library */
  if ((so_name = PC_SOLIB (addr)) != NULL)
    return so_name;
  else if (symfile_objfile) /* If main exe present */
    {
      CORE_ADDR text_base = 0, text_size = 0, text_high = 0;
      CORE_ADDR data_start = 0, data_end = 0;

      /* We cache the text low and text high for an objfile in it.
	 Check cached values first.*/
      if (symfile_objfile->text_low
	  && symfile_objfile->text_high)
	{
	  text_base = symfile_objfile->text_low;
	  text_high = symfile_objfile->text_high;
	}
      /* If the executable has been run under the debugger the objfiles 
	 load module descriptor will have values of text low and text high 
	 Also cache them for future use. 
       */
      else if ((symfile_objfile->obj_private)
	       && ((obj_private_data_t *)
		   symfile_objfile->obj_private)->lmdp)
	{
	  text_base = ((struct load_module_desc *)
		       ((obj_private_data_t *)
			symfile_objfile->obj_private)->lmdp)->text_base;
	  text_high = text_base +
	    ((struct load_module_desc *)
	     ((obj_private_data_t *)
	      symfile_objfile->obj_private)->lmdp)->text_size;
	  symfile_objfile->text_low = text_base;
	  symfile_objfile->text_high = text_high;
	  symfile_objfile->data_start = 
	    elf_data_start_vma (symfile_objfile->obfd);
	  symfile_objfile->data_size = elf_data_size (symfile_objfile->obfd);

	}
      /* If the load module descriptor not present yet then look for the 
	 minimal symbols __text_start and _extext which signify start and
	 end of text and cache the values.  Ditto for __data_start and _end
       */
      else
	{
	  text_base = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
					    ("__text_start",
					     0, symfile_objfile));
	  text_high = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
					    ("_etext",
					     0, symfile_objfile));
	  data_start = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
					    ("__data_start",
					     0, symfile_objfile));
	  data_end = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
					    ("_end",
					     0, symfile_objfile));
	  symfile_objfile->text_low = text_base;
	  symfile_objfile->text_high = text_high;
	  symfile_objfile->data_start = data_start;
	  symfile_objfile->data_size =   UNSWIZZLE(data_end) 
	                               - UNSWIZZLE(data_start);
	}
      
      if (text_base && text_high
	  && text_base <= addr
	  && addr <= text_high)
	return symfile_objfile->name;
    }
  
  return (0);
}
#endif

/* hp_ia64_solib_at_dynlink_hook - SOLIB_AT_DYNLINK_HOOK
   Return TRUE if the breakpoint is the dld.sl breakpoint for notifying
   the debugger about events.
 */

/* THis version should work, but we are getting the wrong address for
   _asm_break when we look it up.
 */

boolean
hp_ia64_solib_at_dynlink_hook (int pid, CORE_ADDR stop_pc)
{
  unsigned int instructions[4];
  boolean status;
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;

  if (!dld_cache.have_read_dld_descriptor)
    read_dld_descriptor (&current_target);
  status = at_svc_load_break (stop_pc);
  inferior_pid = saved_inferior_pid;
  return status;
}				/* end hp_ia64_solib_at_dynlink_hook */

/* hp_ia64_solib_create_catch_load_hook - SOLIB_CREATE_CATCH_LOAD_HOOK

   This function adds something to the end of the catch_load_list.
   If the filename is NULL, then loads of any dll will be caught.  Else,
   only loads of the file whose pathname is the string contained by
   filename will be caught.

   Only file filename argument is used.
 */

void
hp_ia64_solib_create_catch_load_hook (int pid, int tempflag, char *filename, char *cond_string)
{
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;
  hp_ia64_solib_create_catch_load_unload_hook (tempflag,
					       cond_string,
					       filename,
					       &catch_load_list,
					       "load");
  inferior_pid = saved_inferior_pid;
}				/* hp_ia64_solib_create_catch_load_hook */


/* hp_ia64_solib_create_catch_load_unload_hook
   Routine called by hp_ia64_solib_create_catch_load_hook and
   hp_ia64_solib_create_catch_load_hook to do the work.  

   tempflag     TRUE for tcatch command.
   filename     NULL if all events are to be caught, or the path of the file
   otherwise to just catch one file.
   event_list_pp        Either &catch_load_list or &catch_unload_list.
   The new event is addes to the indicated list.
   type         Either "load" or "unload"
 */


static void
hp_ia64_solib_create_catch_load_unload_hook (int tempflag,
					     char *cond_string,
					     char *filename,
					     catch_load_unload_event_t **event_list_pp,
					     const char *type)
{
  catch_load_unload_event_t *new_event;
  catch_load_unload_event_t *last_event;

  new_event = (catch_load_unload_event_t *)
    xmalloc (sizeof (catch_load_unload_event_t));
  memset ((char *) new_event, 0, sizeof (catch_load_unload_event_t));

  new_event->filename = filename ? savestring (filename, (int) strlen (filename))
    : NULL;
  if (*event_list_pp)
    {
      last_event = *event_list_pp;
      while (last_event->next)
	last_event = last_event->next;
      last_event->next = new_event;
    }
  else
    {
      *event_list_pp = new_event;
    }

  if (strcmp ("load", type) == 0)
    {
      create_solib_load_event_breakpoint ("", tempflag, filename, cond_string,
					  new_event);
    }
  else
    {
      create_solib_unload_event_breakpoint ("", tempflag, filename, cond_string,
					    new_event);
    }
  assert (new_event->breakpt);
  assert (new_event->breakpt->dynlink_event == (void *) new_event);
  assert (new_event->breakpt->type == bp_catch_load
	  || new_event->breakpt->type == bp_catch_unload);

}				/* hp_ia64_solib_create_catch_load_unload_hook */


/* hp_ia64_solib_create_catch_unload_hook - SOLIB_CREATE_CATCH_UNLOAD_HOOK

   This function adds something to the end of the catch_unload_list.
   If the filename is NULL, then unloads of any dll will be caught.  Else,
   only unloads of the file whose pathname is the string contained by
   filename will be caught.

   Only file filename argument is used.
 */

void
hp_ia64_solib_create_catch_unload_hook (int pid, int tempflag, char *filename, char *cond_string)
{
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;
  hp_ia64_solib_create_catch_load_unload_hook (tempflag,
					       cond_string,
					       filename,
					       &catch_unload_list,
					       "unload");
  inferior_pid = saved_inferior_pid;
}				/* hp_ia64_solib_create_catch_unload_hook */


/* hp_ia64_solib_create_inferior_hook - SOLIB_CREATE_INFERIOR_HOOK
   This hook gets called just before the first instruction in the
   inferior process is executed.

   This is our opportunity to set magic flags in the inferior so
   that GDB can be notified when a shared library is mapped in and
   to tell the dynamic linker that a private copy of the library is
   needed (so GDB can set breakpoints in the library).

   See read_dynamic_info for how the flags word is found and read.
   We set two flag bits here:
   LI_TRACE		To get shared libraries mapped private, unless we set
   LI_FORCE_MAP_SHARED  To get shared libraries mapped shared
   Set load_info.li_flags to get the shared library notifications
   and BOR callbacks.
 */

#define LRE_NBR_LOAD_INFO_DWORDS	18
flip_desc_t load_info_desc[] = { { LRE_NBR_LOAD_INFO_DWORDS, 8 }, 
					{ 0, 0 } };

void
hp_ia64_solib_create_inferior_hook ()
{
  struct minimal_symbol *msymbol;
  unsigned int dld_flags, have_endo;
  int status;
  asection *shlib_info;
  char shadow_contents[BREAKPOINT_MAX], buf[sizeof (CORE_ADDR)];
  struct objfile *objfile;

  CORE_ADDR 		anaddr;
  CORE_ADDR 		bsp;
  CORE_ADDR 		load_info_addr;
  load_info_t 		load_info;
  int 			real_pid;
  lwpid_t 		real_tid;
  ttstate_t 		tsp;

  /* First, remove all the solib event breakpoints.  Their addresses
     may have changed since the last time we ran the program.  */
  remove_solib_event_breakpoints ();

  /* The symfile_objfile points to the main objfile I.e a.out's objfile.
     If user told gdb not to read in symbols from a.out then, symfile_objfile
     will be null.
     one command line option is "-e" which does not set symarg variable in
     captured_main(). which causes gdb not to read symbols from a.out.
  */
  if (symfile_objfile == NULL)
   {
     warning ("No object file symbols. use file command."
              "\nIf you have used \"-e\" command line option, you need to"
              "\nuse \"-s <symbol_file>\" option to load symbol file.\n");
     return;
   }

  /* srikanth, 000601, if we are debugging a PA a.out on IA64,
     the intent is that we want to debug the emulator (aries.)
     In this case, much of the code executed by this function
     is not relevant. We will do the required initialization/
     cleanup and get out. 
  */

  if (debugging_aries || debugging_aries64)
    {
      /* Wipe out all knowledge of old shared libraries since their
         mapping can change from one exec to another!  */
      while (so_list_head)
        {
          struct so_list *temp;

          temp = so_list_head->next;
          free (so_list_head->solib_desc);
          free (so_list_head);
          so_list_head = temp;
        }
      so_list_tail = NULL;
      solib_clear_symtab_users ();
      return;
    }

  /* First see if the objfile was dynamically linked.  Here we make sure
     there is a non empty .dynamic section.
   */
  shlib_info = bfd_get_section_by_name (symfile_objfile->obfd, ".dynamic");
  if (!shlib_info)
    return;

  /* It's got a .dynamic section, make sure it's not empty.  */
  if (bfd_section_size (symfile_objfile->obfd, shlib_info) == 0)
    return;


  /* Read in relavant information from the .dynamic section.  
   */

  if (!read_dynamic_info (shlib_info, &dld_cache))
    {
      if (IS_LRE_HACK)
        warning ("Unable read in the .dynamic section.");
      else
        error ("Unable read in the .dynamic section.");
    }

#ifndef HP_IA64_GAMBIT
  /* We have to find the address of the load_info structure which is passed
     to the microloader.  After initialization, the global variable,
     __load_info will contain the address of the structure.  If we
     are attaching, we will get the address from __load_info.  If __load_info
     is zero, then presumably we are at startup and the address of the
     structure is in the 4th (and last) argument to the micro loader and
     has been stored at BSP - 8.  BSP and BSPSTORE should be the same.

     Under Gambit, there is no attaching and gabmit sets the load_info flag.
     */

  msymbol = lookup_minimal_symbol ("__load_info", NULL, symfile_objfile);
  if (msymbol == NULL)
    load_info_addr = NULL;
  else
    {
      anaddr = SYMBOL_VALUE_ADDRESS (msymbol);
      status = read_inferior_memory (anaddr, (char*) &load_info_addr, 8);
      if (status != 0)
	load_info_addr = 0;
      else
	if (is_swizzled)
	  { /* We have read in a 32-bit pointer.  Shift as swizzle */
	    load_info_addr = load_info_addr >> 32;
	    load_info_addr = swizzle (load_info_addr);
	  }
    }
  
  saved_load_info_addr = load_info_addr;
  if (!load_info_addr && target_has_execution)
    { /* Get load_info_addr from 4th argument passed by the kernel */
      CORE_ADDR arg_list_addr;

      status = call_ttrace (TT_PROC_GET_ARGS,
			    inferior_pid,
			    (TTRACE_ARG_TYPE) &arg_list_addr,
			    (TTRACE_ARG_TYPE) sizeof (CORE_ADDR),
			    (TTRACE_ARG_TYPE) TT_NIL);
      if (status == -1 && errno)
	  error ("Unable to find the load_info structure.");

      status = read_inferior_memory (arg_list_addr + 3 * sizeof (CORE_ADDR),
				     (char *) &load_info_addr,
				     sizeof (CORE_ADDR));
      if (status != 0)
	  error ("Unable to read the load_info structure address.");
    }

  saved_load_info_addr = load_info_addr;
  read_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));
  /* description matches load_info decl */
  assert (sizeof(load_info) == LRE_NBR_LOAD_INFO_DWORDS * sizeof(long long) );  
  LRE_FLIP_STRUCT (&load_info, load_info_desc);
  if (sizeof(load_info) != load_info.li_length)
    warning (
	"load_info structure has unexpected size.  Possible version problem.");
  /* Enable BOR call-backs only while stepping. */
  load_info.li_flags |= LI_TRACE;
  if (mapshared)
    load_info.li_flags |= LI_FORCE_MAP_SHARED;
  uld_text_addr = load_info.li_uld_taddr;
  LRE_FLIP_STRUCT (&load_info, load_info_desc);
  write_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));

  /* Note, if LRE, load_info is in little-endian at this point, but we 
     don't use it again, so it isn't worth flipping back.
   */

#endif /* ifndef HP_IA64_GAMBIT */

#ifdef RTC
  /* srikanth, 000328, If we are running in RTC mode, create an
     internal breakpoint in main. This will be the point at which
     gdb will intercept calls to malloc and free. We were intercepting
     earlier than this, just after libc.sl and librtc.sl are mapped in
     but this causes problems in PA64. If the C library initialization
     code calls malloc, (the newer versions of C libraries do) then
     control winds up in librtc, even before the thread subsystem is
     initialized. CR27 is not initialized properly at this point
     and this leads a segfault when librtc.sl references its TLS. */

  if (check_heap_in_this_run)
    {
      struct minimal_symbol * m;
      m = lookup_minimal_symbol ("main", 0, symfile_objfile);
      if (m)
        create_solib_event_breakpoint (SYMBOL_VALUE_ADDRESS(m));
    }
#endif /* RTC */

  /* There used to be code here in the PA32 version to set a breakpoint
     at _start.  Two reasons were given and neither is true for HP-UX IA64, so
     the breakpoint isn't being set.  The reasons were:

     * Not all sites have /opt/langtools/lib/end.o, so it's not always
     possible to track the dynamic linker's events.

     * At this time no events are triggered for shared libraries
     loaded at startup time (what a crock).  
   */

  /* Wipe out all knowledge of old shared libraries since their
     mapping can change from one exec to another!  */
  while (so_list_head)
    {
      struct so_list *temp;

      temp = so_list_head->next;
      free (so_list_head->solib_desc);
      free (so_list_head);
      so_list_head = temp;
    }
  so_list_tail = NULL;
  solib_clear_symtab_users ();
}				/* hp_ia64_solib_create_inferior_hook */


/* hp_ia64_solib_get_got_by_pc 
   Return the GOT value for the shared library in which ADDR belongs.  If
   ADDR isn't in any known shared library, return zero.  
   GOT is the value of the linkage pointer.
 */

CORE_ADDR
hp_ia64_solib_get_got_by_pc (CORE_ADDR addr)
{
  struct so_list *so_list = so_list_head;
  CORE_ADDR got_value = 0;

  while (so_list)
    {
      struct load_module_desc *solib_desc = (struct load_module_desc *)
                                            (so_list->solib_desc);
      if (solib_desc->text_base <= addr
	  && (solib_desc->text_base + solib_desc->text_size) > addr)
	{
	  got_value = solib_desc->linkage_ptr;
	  break;
	}
      so_list = so_list->next;
    }
  return got_value;
}				/* hp_ia64_solib_get_got_by_pc */


/* hp_ia64_solib_get_solib_by_pc
   Return the address of the handle of the shared library in which ADDR 
   belongs.  If ADDR isn't in any known shared library, return zero.  

   This function is used in hppa_fix_call_dummy in hppa-tdep.c
 */

CORE_ADDR
hp_ia64_solib_get_solib_by_pc (CORE_ADDR addr)
{
  struct so_list *so_list = so_list_head;
  CORE_ADDR solib_handle_addr = 0;

  while (so_list)
    {
      struct load_module_desc *solib_desc = (struct load_module_desc *)
                                            (so_list->solib_desc);
      if (solib_desc->text_base <= addr
	  && (solib_desc->text_base + solib_desc->text_size) > addr)
	{
	  solib_handle_addr = so_list->desc_addr;
	  break;
	}
      so_list = so_list->next;
    }
  return solib_handle_addr;
}				/* hp_ia64_solib_get_solib_by_pc */

CORE_ADDR
hp_ia64_solib_get_solib_ptr_by_pc (CORE_ADDR addr)
{
  struct so_list *so_list = so_list_head;

  while (so_list)
    {
      struct load_module_desc *solib_desc = (struct load_module_desc *)
                                            (so_list->solib_desc);
      if (solib_desc->text_base <= addr
          && (solib_desc->text_base + solib_desc->text_size) > addr)
        {
          return (CORE_ADDR)so_list;
        }
      so_list = so_list->next;
    }
  return NULL;
}                               /* hp_ia64_solib_get_solib_ptr_by_pc */

/*JAGae68084 - This function check's if we  have a shl_load event*/
void see_if_shl_load(int pid)
{

if (target_has_execution)
  {
    struct minimal_symbol * m;

    m = lookup_minimal_symbol_by_pc (stop_pc);
    if (m && !strcmp (SYMBOL_NAME (m),  "_asm_break"))
        if (hp_ia64_solib_have_load_event (pid))
               in_shl_load=1;

  }
}

/* hp_ia64_solib_handle_dynlink_event - SOLIB_HANDLE_DYNLINK_EVENT
   Process the event signaled by hitting the break instruction in dld.
   Return 0 if gdb should not stop for input, non-zero otherwise;
 */
int
hp_ia64_solib_handle_dynlink_event (int pid, CORE_ADDR stop_pc)
{
  CORE_ADDR arg0;
  int return_value;
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;
  struct so_list *next_solist;

  /* The first argument (in ARG0_REGNUM) to the break routine is the reason 
     for the notification.  Extract it and act accordingly. */

  return_value = 0;
  arg0 = read_register (ARG0_REGNUM);

  /* Switch terminal for any messages produced. */
  target_terminal_ours_for_output ();

  /* We can't use a switch statement with a 64-bit value */
  if (arg0 == BREAK_DE_LIB_LOADED)
    {
      strt_loaded=1; /*JAGae68084*/
      return_value = handle_dynlink_load_event (pid);
#ifdef RTC
      snoop_on_the_heap ();
#endif /* RTC */
    }
  else if (arg0 == BREAK_DE_SVC_LOADED && 
                       (debugging_aries || debugging_aries64))
    {
      return_value = handle_dynlink_load_event (pid);
#ifdef RTC
      snoop_on_the_heap ();
#endif /* RTC */
    }
  else if (arg0 == BREAK_DE_LIB_UNLOADED)
    {
      return_value = handle_dynlink_unload_event (pid);
#ifdef RTC
      snoop_on_the_heap ();
#endif /* RTC */
    }
  else if (arg0 == BREAK_DE_BOR)
    {
      /* Silently ignore this, handled through SOLIB_BOR_CALL */
    }
  else if (   arg0 == BREAK_DE_SVC_LOADED 
	   || arg0 == BREAK_DE_LOAD_COMPLETE)
    {
      /* Silently ignore these.  Currently the only service is dld, which
	 is handled specially. */
      if (arg0 == BREAK_DE_LOAD_COMPLETE)
        {
          shlib_load_completed = 1; /* JAGaf35513 */

          if (check_heap_in_this_run || trace_threads_in_this_run)
            {
              /* Warn if we're doing thread or heap checking and
                 librtc is not loaded */
              next_solist = so_list_head;
              while (next_solist)
                {
                  if (strstr (next_solist->name, "librtc") != 0)
                    break;
                  next_solist = next_solist->next;
                }

              if (next_solist == NULL)
                warning ("Cannot find librtc. Heap and/or thread checking "
                         "infomation will not be available. Set LIBRTC_SERVER "
                         "to point to the right librtc or set GDB_ROOT to "
                         "point to your wdb installation directory.");
            }
        }
    }
  else
    {
      warning ("Unexpected shared library event ignored.");
    }
 
  /* JAGae68084- If user says "Yes" to place breakpoints on 
     subsequent libraries loaded,and if loading of libraries are 
     complete, then place breakpoints in shared libraries*/
  if(if_yes_to_all_commands && strt_loaded && shlib_load_completed)
       {
        strt_loaded=0;
        now_re_enable_bp = 1;
        see_if_shl_load(inferior_pid);
        place_breakpoints_in_shlib();
        in_shl_load = 0;
        free(shllib_name);
        shllib_name = NULL;
       }

  target_terminal_inferior ();

  inferior_pid = saved_inferior_pid;
  return return_value;
}				/* end hp_ia64_solib_handle_dynlink_event */

/* hp_ia64_solib_have_load_event - SOLIB_HAVE_LOAD_EVENT
   This function returns TRUE if the dynamic linker has just reported
   a load of a library.

   This function must be used only when the inferior has stopped in
   the dynamic linker hook, or undefined results are guaranteed.

   This probably isn't called for HP-UX IA64.
 */

int
hp_ia64_solib_have_load_event (int pid)
{
  CORE_ADDR arg0;
  int saved_inferior_pid = inferior_pid;

  /* skip the "stop_pc_pid" */
  (void) read_pc_pid (pid);
  if (!hp_ia64_solib_at_dynlink_hook (pid, stop_pc))
    return FALSE;
  inferior_pid = pid;
  arg0 = read_register (ARG0_REGNUM);
  inferior_pid = saved_inferior_pid;
  return (arg0 == BREAK_DE_LIB_LOADED);
}				/* hp_ia64_solib_have_load_event */


/* hp_ia64_solib_have_unload_event - SOLIB_HAVE_UNLOAD_EVENT
   This function returns TRUE if the dynamic linker has just reported
   an unload of a library.

   This function must be used only when the inferior has stopped in
   the dynamic linker hook, or undefined results are guaranteed.

   This probably isn't called for HP-UX IA64.
 */

int
hp_ia64_solib_have_unload_event (int pid)
{
  CORE_ADDR arg0;
  int saved_inferior_pid = inferior_pid;

  /* skip the "stop_pc_pid" */
  (void) read_pc_pid (pid);
  if (!hp_ia64_solib_at_dynlink_hook (pid, stop_pc))
    return FALSE;
  inferior_pid = pid;
  arg0 = read_register (ARG0_REGNUM);
  inferior_pid = saved_inferior_pid;
  return (arg0 == BREAK_DE_LIB_UNLOADED);
}				/* hp_ia64_solib_have_unload_event */


/* hp_ia64_solib_in_dynamic_linker - SOLIB_IN_DYNAMIC_LINKER
   This function returns TRUE if pc is the address of an instruction that
   lies within the dynamic linker (such as the event hook, or the dld
   itself).

   This function must be used only when a dynamic linker event has been
   caught, and the inferior is being stepped out of the hook, or undefined
   results are guaranteed.
 */
int
hp_ia64_solib_in_dynamic_linker (int pid, CORE_ADDR pc)
{
  asection *shlib_info;
  int saved_inferior_pid = inferior_pid;

  if (symfile_objfile == NULL)
    return FALSE;		/* I'm not sure this will ever happen. */

  inferior_pid = pid;
  if (!dld_cache.have_read_dld_descriptor)
    if (!read_dld_descriptor (&current_target))
      {
        inferior_pid = saved_inferior_pid;
        return FALSE;		/* archive bound.  Can't be in dynamic linker. */
      }
  inferior_pid = saved_inferior_pid;

  if (pc >= dld_cache.dld_desc.text_base
      && pc < dld_cache.dld_desc.text_base
      + dld_cache.dld_desc.text_size)
    return TRUE;
  else
    return FALSE;
}				/* hp_ia64_solib_in_dynamic_linker */


/* hp_ia64_solib_load_symbols
   Load the debug information for a shared library.  Target is NULL
   if we are not attaching to a process or reading a CORE file.
 */

static void
hp_ia64_solib_load_symbols 
PARAMS ((struct so_list * so_ptr,
	 boolean from_tty,
	 struct target_ops * target))
{
  char buf[4];
  char *name = so_ptr->name;
  CORE_ADDR presumed_data_start;
  int status;
  struct section_table *sec_ptr;
  CORE_ADDR text_addr = ((struct load_module_desc *)
                         (so_ptr->solib_desc))->text_base;

#ifdef SOLIB_DEBUG
  printf ("--Adding symbols for shared library \"%s\"\n", name);
#endif

  so_ptr->symbols_loaded = TRUE;

  hp_ia64_solib_add_solib_objfile (so_ptr, from_tty, 0);

  /* Now we need to build a section table for this library since
     we might be debugging a core file from a dynamically linked
     executable in which the libraries were not privately mapped.  */
  if (build_section_table (so_ptr->abfd,
			   &so_ptr->sections,
			   &so_ptr->sections_end))
    {
      error ("Unable to build section table for shared library.");
      return;
    }

  /* For core files detect if there is a mismatch between the libraries
   * on the system and the ones in the core.
   */
  if (target_has_stack && !target_has_execution)
    hp_ia64_core_so_mismatch_detection (so_ptr);

  /* Relocate all the sections based on where they got loaded.  */
  for (sec_ptr = so_ptr->sections; sec_ptr < so_ptr->sections_end; sec_ptr++)
    {
      if (sec_ptr->the_bfd_section->flags & SEC_CODE)
	{
	  sec_ptr->addr += ANOFFSET (so_ptr->objfile->section_offsets,
				     SECT_OFF_TEXT(so_ptr->objfile));
	  sec_ptr->endaddr += ANOFFSET (so_ptr->objfile->section_offsets,
					SECT_OFF_TEXT(so_ptr->objfile));
	}
      else if (sec_ptr->the_bfd_section->flags & SEC_DATA)
	{
	  sec_ptr->addr += ANOFFSET (so_ptr->objfile->section_offsets,
				     SECT_OFF_DATA(so_ptr->objfile));
	  sec_ptr->endaddr += ANOFFSET (so_ptr->objfile->section_offsets,
					SECT_OFF_DATA(so_ptr->objfile));
	}
      sec_ptr->addr = SWIZZLE (sec_ptr->addr);
      sec_ptr->endaddr = SWIZZLE (sec_ptr->endaddr);
    }

  /* Now see if we need to map in the text and data for this shared
     library (for example debugging a core file which does not use
     private shared libraries.). 

     Carefully peek at the first text address in the library.  If the
     read succeeds, then the libraries were privately mapped and were
     included in the core dump file.

     If the peek failed, then the libraries were not privately mapped
     and are not in the core file, we'll have to read them in ourselves.  
   */


  status = target_read_memory (text_addr, buf, 4);
  /* RM: for good measure, try and read the last byte of the shared
     library too. Gene Bradley found a weird core file in which the
     regions from libc.2 and libcl.2 overlap somewhat. There's not
     much we can do about the overlapping region, but we should at
     least try to handle things outside the overlap
   */
  if (status == 0)
    {
      struct load_module_desc *solib_desc = (struct load_module_desc *)
                                            (so_ptr->solib_desc);
      status = target_read_memory (solib_desc->text_base +
				   solib_desc->text_size - 4,
				   buf, 4);
    }
  if (status != 0 && target == 0)
    {
      if (IS_LRE_HACK && hp_ia64_testsuite)
	return;  /* Suppress warning message for test suite */

      warning ("Error encountered reading starting text of shared library");
      return;
    }

  if (status != 0)
    {
      int old, new;
      int update_coreops;
      int update_execops;

      /* We must update the to_sections field in the core_ops structure
         here, otherwise we dereference a potential dangling pointer
         for each call to target_read/write_memory within this routine.  */
      update_coreops = core_ops.to_sections == target->to_sections;

      /* Ditto exec_ops (this was a bug).
       */
      update_execops = exec_ops.to_sections == target->to_sections;

      new = (int) (so_ptr->sections_end - so_ptr->sections);
      /* Add sections from the shared library to the core target.  */
      if (target->to_sections)
	{
	  old = (int) (target->to_sections_end - target->to_sections);
	  target->to_sections = (struct section_table *)
	    xrealloc ((char *) target->to_sections,
		      ((sizeof (struct section_table)) * (old + new)));
	}
      else
	{
	  old = 0;
	  target->to_sections = (struct section_table *)
	    xmalloc ((sizeof (struct section_table)) * new);
	}
      target->to_sections_end = (target->to_sections + old + new);

      /* Update the to_sections field in the core_ops structure
         if needed, ditto exec_ops.  */
      if (update_coreops)
	{
	  core_ops.to_sections = target->to_sections;
	  core_ops.to_sections_end = target->to_sections_end;
	}

      if (update_execops)
	{
	  exec_ops.to_sections = target->to_sections;
	  exec_ops.to_sections_end = target->to_sections_end;
	}

      /* Copy over the old data before it gets clobbered.  */
      memcpy ((char *) (target->to_sections + old),
	      so_ptr->sections,
	      ((sizeof (struct section_table)) * new));
    }

}				/* end hp_ia64_solib_load_symbols */


/* hp_ia64_solib_loaded_library_pathname - SOLIB_LOADED_LIBRARY_PATHNAME
   This function returns a pointer to the string representation of the
   pathname of the dynamically-linked library that has just been loaded.

   This function must be used only when SOLIB_HAVE_LOAD_EVENT is TRUE,
   or undefined results are guaranteed.

   This string's contents are only valid immediately after the inferior
   has stopped in the dynamic linker hook, and becomes invalid as soon
   as the inferior is continued.  Clients should make a copy of this
   string if they wish to continue the inferior and then access the string.

   I believe this routine will not be invoked on HP-UX IA64.
 */

char *
hp_ia64_solib_loaded_library_pathname (int pid)
{
  CORE_ADDR arg0;
  CORE_ADDR dll_path_addr;
  static char dll_path[MAXPATHLEN];
  int saved_inferior_pid = inferior_pid;

  /* skip the "stop_pc_pid" */
  (void) read_pc_pid (pid);
  if (!hp_ia64_solib_at_dynlink_hook (pid, stop_pc))
    {
      inferior_pid = saved_inferior_pid;
      return "";
    }
  inferior_pid = pid;
  dll_path_addr = read_register (ARG3_REGNUM);
  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);
  inferior_pid = saved_inferior_pid;
  return dll_path;
}				/* hp_ia64_solib_loaded_library_pathname */


/* hp_ia64_solib_remove_inferior_hook - SOLIB_REMOVE_INFERIOR_HOOK
   This operation removes the "hook" between GDB and the dynamic linker,
   which causes the dld to notify GDB of shared library events.

   After this operation completes, the dld will no longer notify GDB of
   shared library events.  To resume notifications, GDB must call
   hp_ia64_solib_create_inferior_hook.

   This operation does not remove any knowledge of shared libraries which
   GDB may already have been notified of.

   This is typically used after a fork so the child won't get these signals.
 */
void
hp_ia64_solib_remove_inferior_hook (int pid)
{
  int saved_inferior_pid = inferior_pid;
  /* Ensure that we're really operating on the specified process. */
  inferior_pid = pid;
  set_trace_bit (FALSE);
  inferior_pid = saved_inferior_pid;

}				/* hp_ia64_solib_remove_inferior_hook */

/* hp_ia64_solib_remove_inferior_hook - SOLIB_REMOVE_INFERIOR_HOOK
   This operation resets the "hook" between GDB and the dynamic linker,
   which causes the dld to notify GDB of shared library events.

   This is typically used after a vfork so the child get these signals.
 */
void
hp_ia64_solib_adjust_inferior_hook (int pid)
{
  int saved_inferior_pid = inferior_pid;
  /* Ensure that we're really operating on the specified process. */
  inferior_pid = pid;
  set_trace_bit (TRUE);
  inferior_pid = saved_inferior_pid;
}


/* hp_ia64_solib_restart - SOLIB_RESTART
   This function must be called when the inferior is killed, and the program
   restarted.  This is not the same as CLEAR_SOLIB, in that it doesn't discard
   any symbol tables.

 */

void
hp_ia64_solib_restart ()
{
  struct so_list *sl = so_list_head;

  /* Before the shlib info vanishes, use it to disable any breakpoints
     that may still be active in those shlibs.
   */
  disable_breakpoints_in_shlibs (!info_verbose, NULL);
  shlib_load_completed = 0; /* JAGaf35513 */

  /* Discard all the shlib descriptors.
   */
  while (sl)
    {
      struct so_list *next_sl = sl->next;
      if(sl->sections)
        free (sl->sections); /* For fixing memory leak NEC(JAGaf79121) JAG */
      free (sl->solib_desc);
      free (sl);
      sl = next_sl;
    }
  so_list_head = NULL;
  so_list_tail = NULL;

  hp_ia64_solib_total_st_size = (LONGEST) 0;
  threshhold_warning_given = FALSE;

  /* Set is_valid to zero and also the elements checked in read_dynamic_info */

  dld_cache.is_valid = 0;
  dld_cache.have_read_dld_descriptor = 0;
  dld_cache.is_archive_bound = 0;
  dld_cache.dld_break_addr = 0;
  dld_cache.dld_flags_addr = 0;
  dld_cache.load_map = 0;
  dld_cache.load_map_addr = 0;
  dld_cache.dld_desc.data_base = 0;
  dld_cache.dld_flags = 0;
  dld_cache.dyninfo_sect = 0;

#if 0
  dld_cache.exe_timestamp = 0;
  dld_cache.exe_checksum = 0;
#endif

  /* Cleanup all mxn related structures before restart. */
#ifdef HP_MXN
  clear_mxn_info();
#endif


}				/* hp_ia64_solib_restart */


/* hp_ia64_solib_section_offsets
   For a given objfile, fill in a struct section_offsets with
   offsets between the link-time addresses and the runtime addresses of the
   segments for:
   text
   data
   bss   (the same as data)

   For the main program, the addr passed is zero, and the offsets are all
   set to zero.  For anything else (i.e. shared libraries), addr is
   the the run-time address of the text section minus the 
   link-time presumed text address.

   Call error if something goes wrong.
 */

void
hp_ia64_solib_section_offsets (struct objfile *objfile, struct section_addr_info *addrs)
{

  int i;
  struct so_list *so_list = so_list_head;

  /* Sunil S - JAGag07432 */
  CORE_ADDR text_addr, data_addr;

  objfile->num_sections = SECT_OFF_MAX;
  objfile->section_offsets = (struct section_offsets *)
			     (void *) obstack_alloc (&objfile->psymbol_obstack,
					    SIZEOF_SECTION_OFFSETS);

  /* JYG: MERGE FIXME: hack to initialize these here */
  objfile->sect_index_text = 0;
  objfile->sect_index_data = 1;
  objfile->sect_index_bss = 2;
  objfile->sect_index_rodata = 3;

  if (addrs->other[0].addr == 0)
    {
      /* For the main program, all the offsets are zero. */
      for (i = 0; i < SECT_OFF_MAX; i++)
	ANOFFSET (objfile->section_offsets, i) = 0;
      return;
    }

  while (so_list)
    {
      struct load_module_desc *solib_desc = (struct load_module_desc *)
                                            (so_list->solib_desc);

      /* Oh what a pain!  We need the offsets before so_list->objfile
         is valid.  The BFDs will never match.  Make a best guess.  */
      if (addrs->other[0].addr == 
          solib_desc->text_base - elf_text_start_vma(objfile->obfd))
	{
	  /* The text offset is easy.  */
	  ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile))
	    = ANOFFSET (objfile->section_offsets, SECT_OFF_RODATA (objfile))
	    = addrs->other[0].addr;

	  /* The data segment is very similar */

	  ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile))
	    = ANOFFSET (objfile->section_offsets, SECT_OFF_BSS (objfile))
	    = addrs->other[1].addr;

	  return;
	}
      so_list = so_list->next;
    }

  /* Skip this warning for the internal use of add_symbol_file_command
     during RTC processing of unloaded shared libraries
   */
  if ( !add_unloaded_library_symbols )
    {
       printf_unfiltered ("\n");
       warning ("Unknown shared object. You are trying to load a shared"
          " library/object file that is previously not loaded.");
    }

  ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile))
    = ANOFFSET (objfile->section_offsets, SECT_OFF_RODATA (objfile))
    = addrs->other[0].addr;

  if (addrs->other[1].name)
    ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile))
      = ANOFFSET (objfile->section_offsets, SECT_OFF_BSS (objfile))
      = addrs->other[1].addr;
  return;
}				/* hp_ia64_solib_section_offsets */


/* Implement the sharedlibrary command - list information about the 
   shared libraries in use.
 */

static void
hp_ia64_solib_sharedlibrary_command (char *arg_string, int from_tty)
{
  struct so_list *so_list;

  dont_repeat ();		/* If the user types return, don't repeat the command */
  if ((re_comp (arg_string ? arg_string : ".")) != NULL)
    {
      error ("Invalid regexp: '%s'", arg_string);
    }

  /* Go through the list of libraries.  If a library matches the args pattern
     and we haven't loaded its debug info, load the info now.
   */
  for (so_list = so_list_head; so_list; so_list = so_list->next)
    {
      if ((so_list->objfile) && (so_list->objfile->psymtabs != NULL))
	continue;		/* debug info already loaded */

      if (!re_exec (so_list->name))
	continue;		/* Name does not match regular expression. */

      printf_unfiltered ("Loading  %s\n", so_list->name);

      hp_ia64_solib_total_st_size +=
	hp_ia64_solib_sizeof_symbol_table (so_list->name, NULL);

      if (so_list->objfile == NULL)
	{
	  hp_ia64_solib_load_symbols (so_list,
				      from_tty,
				      (struct target_ops *) 0);
	}
      else if ((so_list->objfile != NULL) &&
	       (so_list->objfile->psymtabs == NULL) &&
	       (so_list->objfile->sf->sym_add_psymtabs != NULL))
	{
	  /* RM: we now do a fair amount of processing (reading
	     unwind info, linker symbol table) even if the
	     threshold is exceeded. We only want to read in the
	     actual debug information when we come here, the
	     rest should already have been done */
	  printf ("--Adding symbols for shared library \"%s\"\n",
		  so_list->name);
	  (*so_list->objfile->sf->sym_add_psymtabs)
	    (so_list->objfile, 0);
	}
    }				/* end for so_list */

  re_enable_breakpoints_in_shlibs ();	/* Fixed CLLbs16090 */
}

/* Get some HPUX-specific data from a shared lib.
 */
CORE_ADDR
so_lib_thread_start_addr (struct so_list *so)
{
  /* FIXME: jini: Mixed mode changes: JAGag21714: Return NULL for
     PA SOM libs */
  if (so->is_mixed_mode_pa_lib)
    {
      return NULL;
    }

  return ((struct load_module_desc *)(so->solib_desc))->tls_start_addr;
}				/* so_lib_thread_start_addr */

/* hp_ia64_solib_sizeof_symbol_table
   Return an estimate of the heap space required for the debug information
   from filename.
 */

static LONGEST
hp_ia64_solib_sizeof_symbol_table (char *filename,
                                   enum mixed_mode_type_t* mixed_mode_type)
{
  bfd *abfd;
  int section_idx;
  int desc;
  char *absolute_name;
  LONGEST st_size = (LONGEST) 0;
  asection *sect;

  if (mixed_mode_type)
    {
      *mixed_mode_type = MIXED_MODE_NONE;
    }
  /* We believe that filename was handed to us by the dynamic linker, and
     is therefore always an absolute path.
   */
  desc = openp (getenv ("PATH"), 1, filename, O_RDONLY | O_BINARY, 0, &absolute_name);
  if (desc < 0)
    {
      perror_with_name (filename);
    }
  filename = absolute_name;

  abfd = bfd_fdopenr (filename, gnutarget, desc);
  if (!abfd)
    {
      close (desc);
      make_cleanup (free, filename);
      error ("\"%s\": can't open to read symbols: %s.", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

  if (!bfd_check_format (abfd, bfd_object))	/* Reads in section info */
    {
      /* jini: mixed mode support: JAGag21714: If this is a SOM file,
         this is a mixed mode binary. Just return in this case. */
      if (mixed_mode_type && bfd_get_flavour(abfd) == bfd_target_som_flavour)
        {
          *mixed_mode_type = MIXED_MODE_32;
          bfd_close (abfd);		/* This also closes desc */
          make_cleanup (free, filename);
          return som_solib_sizeof_symbol_table (filename);
        }
      bfd_close (abfd);		/* This also closes desc */
      make_cleanup (free, filename);
      error ("\"%s\": can't read symbols: %s.", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

  /* Sum the sizes of the various sections that compose debug info. */
  for (section_idx = 0; ia64_debug_section_names[section_idx] != NULL; section_idx++)
    {
      asection *sect;

      sect = bfd_get_section_by_name (abfd, ia64_debug_section_names[section_idx]);
      if (sect)
        st_size += (LONGEST)bfd_section_size (abfd, sect);
    }

  bfd_close (abfd);		/* This also closes desc */
  free (filename);		/* fee the filename returned by openp */

  /* Unfortunately, just summing the sizes of various debug info
     sections isn't a very accurate measurement of how much heap
     space the debugger will need to hold them.  It also doesn't
     account for space needed by linker (aka "minimal") symbols.

     Anecdotal evidence suggests that just summing the sizes of
     debug-info-related sections understates the heap space needed
     to represent it internally by about an order of magnitude.

     Since it's not exactly brain surgery we're doing here, rather
     than attempt to more accurately measure the size of a shlib's
     symbol table in GDB's heap, we'll just apply a 10x fudge-
     factor to the debug info sections' size-sum.  No, this doesn't
     account for minimal symbols in non-debuggable shlibs.  But it
     all roughly washes out in the end.

     Jan 15 2002 - When we try to load in large shared libraries 
     with say 82 MegaBytes of debug info we exceed the max size given 
     to us by the Kernel with the 10x fudge factor. With a 2x fudge
     factor we still have a chance. Tested with the jvm. If someone
     finds a better fudge factor - by all means replace this.
   */
  return st_size * (LONGEST) 2;
}				/* end hp_ia64_solib_sizeof_symbol_table */

/* hp_ia64_sharedlibrary_info_command
   Dump information about all the currently loaded shared libraries.  
 */

static void
hp_ia64_sharedlibrary_info_command (char *ignore, int from_tty)
{
  struct so_list *so_list = so_list_head;
  unsigned int num_shlibs = 0;

  if (!dld_cache.load_map)
    { /* We need to call read_dld_descriptor until it reads load_map */
      read_dld_descriptor (&current_target);
    }

  if (exec_bfd == NULL)
    {
      printf_unfiltered ("no exec file.\n");
      return;
    }

  if (so_list == NULL)
    {
      printf_unfiltered ("No shared libraries loaded at this time.\n");
      return;
    }

  printf_unfiltered ("Shared Object Libraries\n");
  printf_unfiltered ("    %-19s%-19s%-19s%-19s%-19s\n",
		     "    tstart", "     tend", "    dstart",
		     "     dend",  "     gp");
  while (so_list)
    {
      unsigned int flags;
      struct load_module_desc *solib_desc = (struct load_module_desc *)
                                            (so_list->solib_desc);

      printf_unfiltered ("%s", so_list->name);
      if (! so_list->symbols_loaded)
	printf_unfiltered ("  (symbols not loaded)");
      if (so_list->loaded == FALSE)
	printf_unfiltered ("  (shared library unloaded)");
      printf_unfiltered ("\n");
      printf_unfiltered (" %-18s",
			 longest_local_hex_string_custom (
			                    solib_desc->text_base,
							   "016l"));
      printf_unfiltered (" %-18s",
			 longest_local_hex_string_custom (
			                     solib_desc->text_base
				           + solib_desc->text_size,
							   "016l"));
      printf_unfiltered (" %-18s",
			 longest_local_hex_string_custom (
			                    solib_desc->data_base,
							   "016l"));
      printf_unfiltered (" %-18s",
			 longest_local_hex_string_custom (
			                     solib_desc->data_base
				           + solib_desc->data_size,
							   "016l"));
      printf_unfiltered (" %-18s",
                         longest_local_hex_string_custom (
                                             solib_desc->linkage_ptr,
                                                           "016l"));
      printf_unfiltered ("\n");
      so_list = so_list->next;
      num_shlibs++;
    }
    printf_unfiltered ("Total of %d shared libraries.\n", num_shlibs);
}				/* hp_ia64_sharedlibrary_info_command */


/* hp_ia64_solib_unloaded_library_pathname - SOLIB_UNLOADED_LIBRARY_PATHNAME(
   This function returns a pointer to the string representation of the
   pathname of the dynamically-linked library that has just been unloaded.

   This function must be used only when SOLIB_HAVE_UNLOAD_EVENT is TRUE,
   or undefined results are guaranteed.

   This string's contents are only valid immediately after the inferior
   has stopped in the dynamic linker hook, and becomes invalid as soon
   as the inferior is continued.  Clients should make a copy of this
   string if they wish to continue the inferior and then access the string.
 */

char *
hp_ia64_solib_unloaded_library_pathname (int pid)
{
  CORE_ADDR arg0;
  CORE_ADDR dll_path_addr;
  static char dll_path[MAXPATHLEN];
  int saved_inferior_pid = inferior_pid;

  /* skip the "stop_pc_pid" */
  (void) read_pc_pid (pid);
  if (!hp_ia64_solib_at_dynlink_hook (pid, stop_pc))
    {
      inferior_pid = saved_inferior_pid;
      return "";
    }
  inferior_pid = pid;
  dll_path_addr = read_register (ARG3_REGNUM);
  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);
  inferior_pid = saved_inferior_pid;
  return dll_path;
}				/* hp_ia64_solib_unloaded_library_pathname */

/* Bit hackish.. the core file we have seems to have corrupt
   load map address. We are trying to get it from the system dld
   and use that instead.
*/
static int
find_global_liblist_from_installed_dld (
  CORE_ADDR *load_ptr,
  unsigned long *timestamp_ptr,
  char** dld_used_ptr)
{
  char *dld_path = getenv("GDB_MATCH_DLD");
  if (NULL == dld_path)
  {
    if (is_swizzled)
      dld_path = "/usr/lib/hpux32/dld.so";
    else
      dld_path = "/usr/lib/hpux64/dld.so";
  }

  *dld_used_ptr = dld_path;

  char *tmp_file = tempnam(NULL, ".gdb");
  if (!tmp_file)
    tmp_file = "/tmp/.gdb_tmp";

  CORE_ADDR dld_data_seg = 0;

  char sys_cmd[1024];
  sprintf(sys_cmd, "elfdump -s -n .dynsym %s | grep __data_seg "
                   "| awk '{ print $6 '} > %s", dld_path, tmp_file);

  system(sys_cmd);

  sprintf(sys_cmd, "elfdump -t %s | grep dld_global_liblist "
                   "| awk '{ print $6 '} >> %s", dld_path, tmp_file);

  system(sys_cmd);

  sprintf(sys_cmd, "elfdump -L -H %s | grep HPTime "
                   " | awk '{ print $3 '} >> %s", dld_path, tmp_file); 
  system(sys_cmd);

  FILE *fp = fopen(tmp_file, "r");
  if (!fp)
    return -1;

  fscanf(fp, "%lx", &dld_data_seg);
  fscanf(fp, "%lx", load_ptr);
  fscanf(fp, "%lx", timestamp_ptr);

  fclose(fp);

  if (debug_traces)
    printf_filtered("dld_data_seg = %lx, load_ptr = %lx, timestamp = %lx\n",
                    dld_data_seg, *load_ptr, *timestamp_ptr);

  *load_ptr -= dld_data_seg;

  *load_ptr += get_load_info_dld_data_start();

  if (debug_traces)
    printf_filtered("load_ptr = %lx\n", *load_ptr);

  unlink(tmp_file);

  return 0;
}

/* read_dld_descriptor
   Read dld_cache.dld_desc.  This must happen after dld starts running,
   so we can't do it in read_dynamic_info.  Set have_read_dld_descriptor
   to TRUE.  Also set load_map to the value in load_map_addr;

   If the program is archive bound, return FALSE and set is_archive_bound
   to TRUE so that we won't try to do the read again.
   If the program is shared bound, return TRUE.
 */

static boolean
read_dld_descriptor (struct target_ops *target)
{
  char *dll_path;
  asection *dyninfo_sect;
  struct minimal_symbol *msymbol;
  obj_private_data_t *obj_private;
  struct objfile *objfile;
  char *prev_so_pathname_g = NULL;
  int ret;

  if (dld_cache.is_archive_bound)
    {
      return FALSE;
    }

  if (debugging_aries || debugging_aries64)
    {
      in_startup_no_prev_fp = FALSE;
      dld_cache.is_archive_bound = TRUE;
      return FALSE;
    }

  if (!dld_cache.is_valid)
    {
      if (symfile_objfile == NULL)
       {
         return 0;
       } 	 
      dyninfo_sect = bfd_get_section_by_name (symfile_objfile->obfd,
					      ".dynamic");
      if (!dyninfo_sect)
	{
	  in_startup_no_prev_fp = FALSE;
	  dld_cache.is_archive_bound = TRUE;
	  return FALSE;
	}
      if (!read_dynamic_info (dyninfo_sect, &dld_cache))
	error ("Unable to read in .dynamic section information.");
    }				/* if (!dld_cache.is_valid) */

  if (IS_TARGET_LRE)
    {
      if (target_read_memory (dld_cache.lre_debug_base_addr,
			      (char *) &dld_cache.lre_debug_base,
			      sizeof (dld_cache.lre_debug_base))
	  != 0)
	{
	  error ("Error while reading in LRE debug base pointer.");
	}

      /* Need to decide what to do with lre_debug_base, etc. */
      if (IS_LRE_HACK)
	{
	  /* Need dlmodinfo stuff to do lmdp right */
	  if (symfile_objfile->obj_private == NULL)
	    {
	      obj_private = (obj_private_data_t *)
		(void *) obstack_alloc (&symfile_objfile->psymbol_obstack,
			       sizeof (obj_private_data_t));

	      bzero (obj_private, sizeof(obj_private_data_t));
	      symfile_objfile->obj_private = (PTR) obj_private;
	    }
	  else
	    {
	      obj_private = symfile_objfile->obj_private;
	    }
	  if (!obj_private->lmdp)
	    {
	      obj_private->lmdp = (struct load_module_desc *)
		(void *) obstack_alloc (&symfile_objfile->psymbol_obstack,
			       sizeof (struct load_module_desc));
	      bzero (obj_private->lmdp, sizeof (struct load_module_desc));



	      /* Set stuff in lmdp of the main program */

	      ((struct load_module_desc*)obj_private->lmdp)->linkage_ptr = 
		  read_register (GR0_REGNUM + 1);

	      /* Set unwind_base and unwind_size */
	      elf_init_lmdp (symfile_objfile, 
			     (struct load_module_desc*) obj_private->lmdp);
	    }

	  /* Other stuff we need to do */

	  in_startup_no_prev_fp = FALSE;  /* It is OK to try to unwind */
	  dld_name = xstrdup ("/lib/ld-linux-ia64.so.2");
	  return FALSE;
	}
    }

  if (debug_traces > 200)
  {
    printf_filtered("read_dld_descriptor, dld_cache.load_map_addr = %llx\n",
                    dld_cache.load_map_addr);
  }

  if (target_read_memory (dld_cache.load_map_addr,
			  (char *) &dld_cache.load_map,
			  sizeof (dld_cache.load_map))
      != 0)
    {
      error ("Error while reading in load map pointer.");
    }

  /* If we are dealing with an ILP32 program, is_swizzled will be TRUE
     and the address stored is only 4 bytes, not 8 bytes.
   */
  if (is_swizzled)
    {
      unsigned long long temp_addr;
      temp_addr = dld_cache.load_map;
      temp_addr = temp_addr >> 32;
      dld_cache.load_map = swizzle (temp_addr);
    }

  if (debug_traces > 200)
  {
    printf_filtered("read_dld_descriptor, dld_cache.load_map = %llx\n",
                    dld_cache.load_map);
  }

  if (!dld_cache.load_map)
    {
      /* When we hit the notification in uld.so we haven't yet read in
	 dld.so.  We try again after dld.so has been loaded and dld has
	 placed the address we need in load_map.
	 */
      return TRUE;
    }


  msymbol = lookup_minimal_symbol ("_asm_break", NULL, so_list_tail->objfile);
  if (msymbol == NULL)
    {
      dld_cache.dld_break_addr = 0;
    }
  else
    {
      dld_cache.dld_break_addr = SYMBOL_VALUE_ADDRESS (msymbol);
    }

  re_enable_breakpoints_in_shlibs ();

  if (!dld_cache.load_map)
    {
      /* When we hit the notification in uld.so we haven't yet read in
	 dld.so.  We try again after dld.so has been loaded and dld has
	 placed the address we need in load_map.
	 */
      return TRUE;
    }
  /* Read in dld_cache.dld_desc and add dld to the shared library list */
  

  /* Let us check that we have a valid dld_cache.load_map */

  CORE_ADDR check_version;

  CORE_ADDR installed_dld_global_liblist_ptr = 0;
  CORE_ADDR installed_dld_global_liblist = 0;
  unsigned long install_dld_timestamp = 0;
  char *dld_used = NULL;

  ret = target_read_memory (dld_cache.load_map,
                            (char *) &check_version,
                            sizeof (CORE_ADDR));
          // dld_cache.load_map not readble
  if (   ret != 0
          // dld_cache.load_map readble, but doesn't have
          // proper data, probably we have a wrong
          // dld_cache.load_map
          // The first two 4 byte fields read from the
          // dld_cache.load_map are version and is_narrow_mode.
          // For 32 bit, they should be 0 and 1.
      || (!ret && is_swizzled && check_version != 1)
          // For 64 bit, they should be 0 and 0.
      || (!ret && !is_swizzled && check_version != 0))
  {
    if (!target_has_execution)
    {
      ret = find_global_liblist_from_installed_dld(
                    &installed_dld_global_liblist_ptr,
                    &install_dld_timestamp,
                    &dld_used);
      if (ret == 0)
      {
        if (target_read_memory (installed_dld_global_liblist_ptr,
                                (char *) &installed_dld_global_liblist,
                                sizeof (CORE_ADDR))
            != 0)
        {
          error("Can not read the load map pointer %llx.\n"
                "Potential dld library list pointer %llx found from "
                "alternate dld could not be read.\n"
                "Check for possible mismatch between the executable "
                "and core file.",
                dld_cache.load_map,
                installed_dld_global_liblist_ptr);
        }

        if (is_swizzled)
          {
	    /* We have read in a 32-bit pointer.  Shift as swizzle */
	    installed_dld_global_liblist
              = installed_dld_global_liblist >> 32;
	    installed_dld_global_liblist
              = swizzle (installed_dld_global_liblist);
          }

        if (target_read_memory (installed_dld_global_liblist,
                               (char *) &check_version,
                               sizeof (CORE_ADDR))
            != 0)
        {
          error("Can not read the load map pointer %llx.\n"
                "Potential load map pointer %llx found from alternate "
                "dld could not be read.\n"
                "Check for possible mismatch between the executable "
                "and core file.",
                dld_cache.load_map,
                installed_dld_global_liblist);
        }
        else if (   (is_swizzled && check_version != 1)
                 || (!is_swizzled && check_version != 0))
        {
          error("Can not read the load map pointer %llx.\n"
                "Potential load map pointer %llx found from alternate "
                "dld is not valid.\n"
                "Check for possible mismatch between the executable "
                "and core file.",
                dld_cache.load_map,
                installed_dld_global_liblist);
        }
        else
        {
          time_t   time_val = (time_t) (uint64_t) install_dld_timestamp;
          char*    time_str = ctime(&time_val);

          warning("Core file load map pointer %llx is corrupted\n"
                  "or the executable does not match the core file.\n"
                  "Using the load map pointer %llx from\n"
                  "alternate dld %s with timestamp = %s\n",
                  dld_cache.load_map,
                  installed_dld_global_liblist,
                  dld_used,
                  time_str);

          dld_cache.load_map = installed_dld_global_liblist;
        }
      }
      else
      {
        error("Can not read the load map pointer %llx.\nCheck for "
              "possible mismatch between the executable and core file.",
              dld_cache.load_map);
      }
    }
    else
    {
      error("Can not read the load map pointer %llx.\n",
            dld_cache.load_map);
    }
  }

  /* Read in the dld load module descriptor */

  if (dlgetmodinfo1 (-1,
		    &dld_cache.dld_desc,
		    sizeof (dld_cache.dld_desc),
		    read_tgt_mem,
		    0,
		    dld_cache.load_map)
      == NULL)
    {
      error ("Error trying to get information about dynamic linker.");
    }

  dld_cache.have_read_dld_descriptor = TRUE;

  in_startup_no_prev_fp = FALSE;  /* It is OK to try to unwind */

  /* Add dld.sl to the list of known shared libraries so that we can
     do unwind, etc.
   */

  if (IS_LRE_HACK)
    {
      dll_path = xstrdup ("/lib/ld-linux-ia64.so.2");
    }
  else
    dll_path = dlgetname (&dld_cache.dld_desc,
			  sizeof (dld_cache.dld_desc),
			  read_tgt_mem,
			  0,
			  dld_cache.load_map);

  /* With the microloader change, dll_path might be something like:
     "/usr/lib/hpux64/uld.so:/usr/lib/hpux64/dld.so".  If we find "/uld.so:",
     then move past the first colon.
     */
  if (strstr (dll_path, "/uld.so:"))
    dll_path = strchr (dll_path, ':') + 1;

  dld_name = dll_path;

  /* QXCR1000890610 : IA64 debugger will not correctly unwind core if dld.so
     used is not the same. The global variable "so_pathname_g" is updated just
     before the "add_to_solist" function call so that correct "dld.so" library
     path gets printed in the warning message. */ 
  prev_so_pathname_g = so_pathname_g;
  so_pathname_g = dll_path;
  add_to_solist (0, dll_path, &dld_cache.dld_desc, 0, target);
  so_pathname_g = prev_so_pathname_g;

  if (!dld_cache.load_map)
    {
      /* When we hit the notification in uld.so we haven't yet read in
	 dld.so.  We try again after dld.so has been loaded and dld has
	 placed the address we need in load_map.
	 */
      return TRUE;
    }

  ALL_OBJFILES (objfile)
  {
    /* Skip the separate debug object , look for the actual one. */
    if (objfile->separate_debug_objfile_backlink)
      continue;

    if (!(objfile->flags & OBJF_SHARED))
      break;
  }
  if (objfile)
    {
      /* RM: Set lmdp for main program */
      bool objfile_is_pa = false;
      if (dlgetmodinfo1 (0,
			&dld_cache.main_prog_desc,
			sizeof (dld_cache.main_prog_desc),
			read_tgt_mem,
			0,
			dld_cache.load_map)
	  == NULL)
	{
	  error ("Error trying to get information about main program.");
	}

#if 0
      if (target_has_stack && !target_has_execution)
        hp_ia64_core_exe_mismatch_detection(&dld_cache.main_prog_desc,
                                            dld_cache.exe_timestamp,
                                            dld_cache.exe_checksum);
#endif

      objfile_is_pa = objfile_is_mixed_mode (objfile);
      if (!objfile_is_pa && objfile->obj_private == NULL)
	{
	  obj_private = (obj_private_data_t *)
	    (void *) obstack_alloc (&objfile->psymbol_obstack,
			   sizeof (obj_private_data_t));
	  obj_private->unwind_info = NULL;
	  obj_private->opd = NULL;
	  obj_private->so_info = NULL;
	  obj_private->lmdp = NULL;
	  objfile->obj_private = (PTR) obj_private;
	}
      else
	{
	  obj_private = objfile->obj_private;
	}
      if (!objfile_is_pa && !obj_private->lmdp)
	{
	  obj_private->lmdp = (struct load_module_desc *)
	    (void *) obstack_alloc (&objfile->psymbol_obstack,
			   sizeof (struct load_module_desc));
	  memcpy (obj_private->lmdp, &dld_cache.main_prog_desc,
		  sizeof (struct load_module_desc));
	}
    }

  return TRUE;
}				/* end read_dld_descriptor */

/* read_dynamic_info
   Read the .dynamic section and extract the information of interest,
   which is stored in dld_cache.  The routine elf_locate_base in solib.c 
   was used as a model for this. 
 */
static boolean
read_dynamic_info32 (asection * dyninfo_sect, dld_cache_t * dld_cache_p)
{
  char *buf;
  char *bufend;
  CORE_ADDR dyninfo_addr;
  int dyninfo_sect_size;
  CORE_ADDR entry_addr;

  /* Read in .dynamic section, silently ignore errors.  */
  dyninfo_addr = bfd_section_vma (symfile_objfile->obfd, dyninfo_sect);
  dyninfo_sect_size = (int) bfd_section_size (exec_bfd, dyninfo_sect);
  buf = alloca (dyninfo_sect_size);
  if (target_read_memory (dyninfo_addr, buf, dyninfo_sect_size))
    return FALSE;

  /* Scan the .dynamic section and record the items of interest. 
     In particular, DT_HP_DLD_FLAGS */
  for (bufend = buf + dyninfo_sect_size, entry_addr = dyninfo_addr;
       buf < bufend;
       buf += sizeof (Elf32_Dyn), entry_addr += sizeof (Elf32_Dyn))
    {
      Elf32_Dyn *x_dynp = (Elf32_Dyn *) (void *) buf;
      Elf32_Sword dyn_tag;
      CORE_ADDR dyn_ptr;
      char pbuf[TARGET_PTR_BIT / HOST_CHAR_BIT];

      dyn_tag = (Elf32_Sword) bfd_h_get_32 (symfile_objfile->obfd,
			      (bfd_byte *) & x_dynp->d_tag);

      if (dyn_tag == DT_NULL)
	{
	  break;		/* end of dynamic array */
	}
      else if (dyn_tag == DT_HP_DLD_FLAGS)
	{
	  /* Set dld_flags_addr and dld_flags in *dld_cache_p */
	  dld_cache_p->dld_flags_addr = entry_addr + offsetof (Elf32_Dyn, d_un);
	  if (target_read_memory (dld_cache_p->dld_flags_addr,
				  (char *) &dld_cache_p->dld_flags,
				  sizeof (dld_cache_p->dld_flags))
	      != 0)
	    {
	      error ("Error while reading in .dynamic section of the program.");
	    }
	  /* Flip endian for 32-bit value */
	  if (!elf_is_header_big_endian (symfile_objfile->obfd))
	    dld_cache_p->dld_flags =
	      bfd_getl32 ((unsigned char *) &dld_cache_p->dld_flags);
	}
      else if (dyn_tag == DT_HP_LOAD_MAP)
	{
	  /* Dld will place the address of the load map at load_map_addr
	     after it starts running.
	   */
	  if (target_read_memory (entry_addr + offsetof (Elf32_Dyn,
							 d_un.d_ptr),
				  (char *) &dld_cache_p->load_map_addr,
				  sizeof (dld_cache_p->load_map_addr))
	      != 0)
	    {
	      error ("Error while reading in .dynamic section of the program.");
	    }
	  /* Flip endian for 64-bit elf data */
	  if (!elf_is_header_big_endian (symfile_objfile->obfd))
	    dld_cache_p->load_map_addr =
	      bfd_getl32 ((unsigned char *) &dld_cache_p->load_map_addr);

	  if (bfd_is_elf32 (symfile_objfile->obfd))
	  {
	    unsigned long long temp_addr;

	    temp_addr = dld_cache.load_map_addr;
	    temp_addr = temp_addr >> 32;
	    dld_cache.load_map_addr = swizzle (temp_addr);
	  }
	}
      else if (dyn_tag == DT_HP_TIME_STAMP)
	{
	  /* If the dld.so in the core is old, i.e., it doesn't have the
	   * new capability of dlgetmodinfo(), disable core library mismatch
	   * detection.  Otherwise results will be bogus.
	   */
  	  /* The last dld without the new functionality was for 11.23 which 
	   * was build on May 15th, 2003.  This date corresponds to May 16h.
	   */
  	  const int new_dld_date = 0x3ec50000;
          if (x_dynp->d_un.d_val < new_dld_date)
	    core_so_mismatch_detection_disabled = 1;
#if 0
          dld_cache.exe_timestamp = x_dynp->d_un.d_val;
#endif
	}
#if 0
      else if (dyn_tag == DT_HP_CHECKSUM)
        {
          dld_cache.exe_checksum = x_dynp->d_un.d_val;
        }
#endif
      else if (dyn_tag == DT_FLAGS)
        {
          /* Find if this is BOR/bind-immediate */
          dld_cache_p->is_bor = !(x_dynp->d_un.d_val & DF_BIND_NOW);
        }
      else
	{
	  /* tag is not of interest */
	}
    }				/* end for buf < bufend */

  /* Record other information and set is_valid to TRUE. */

  dld_cache_p->dyninfo_sect = dyninfo_sect;

  /* Verify that we read in required info.  These fields are re-set to zero
     in hp_ia64_solib_restart.
   */

  if (dld_cache_p->dld_flags_addr != 0 && dld_cache_p->load_map_addr != 0)
    dld_cache_p->is_valid = TRUE;
  else
    return FALSE;		/* something was not read in */

  return TRUE;
}				/* read_dynamic_info */

static boolean
read_dynamic_info64 (asection * dyninfo_sect, dld_cache_t * dld_cache_p)
{
  char *buf;
  char *bufend;
  CORE_ADDR dyninfo_addr;
  int dyninfo_sect_size;
  CORE_ADDR entry_addr;
  CORE_ADDR lre_debug_base;

  /* Read in .dynamic section, silently ignore errors.  */
  dyninfo_addr = bfd_section_vma (symfile_objfile->obfd, dyninfo_sect);
  dyninfo_sect_size = (int) bfd_section_size (exec_bfd, dyninfo_sect);
  buf = alloca (dyninfo_sect_size);
  if (target_read_memory (dyninfo_addr, buf, dyninfo_sect_size))
    return FALSE;

  /* Scan the .dynamic section and record the items of interest. 
     In particular, DT_HP_DLD_FLAGS */
  for (bufend = buf + dyninfo_sect_size, entry_addr = dyninfo_addr;
       buf < bufend;
       buf += sizeof (Elf64_Dyn), entry_addr += sizeof (Elf64_Dyn))
    {
      Elf64_Dyn *x_dynp = (Elf64_Dyn *) (void *) buf;
      Elf64_Sxword dyn_tag;
      CORE_ADDR dyn_ptr;
      char pbuf[TARGET_PTR_BIT / HOST_CHAR_BIT];

      dyn_tag = bfd_h_get_64 (symfile_objfile->obfd,
			      (bfd_byte *) & x_dynp->d_tag);

      /* We can't use a switch here because dyn_tag is 64 bits */
      if (dyn_tag == DT_NULL)
	{
	  break;		/* end of dynamic array */
	}
      else if (dyn_tag == DT_HP_DLD_FLAGS)
	{
	  /* Set dld_flags_addr and dld_flags in *dld_cache_p */
	  dld_cache_p->dld_flags_addr = entry_addr + offsetof (Elf64_Dyn, d_un);
	  if (target_read_memory (dld_cache_p->dld_flags_addr,
				  (char *) &dld_cache_p->dld_flags,
				  sizeof (dld_cache_p->dld_flags))
	      != 0)
	    {
	      error ("Error while reading in .dynamic section of the program.");
	    }
	  /* Flip endian for 64-bit value */
	  if (!elf_is_header_big_endian (symfile_objfile->obfd))
	    dld_cache_p->dld_flags =
	      bfd_getl64 ((unsigned char *) &dld_cache_p->dld_flags);
	}
      else if (dyn_tag == DT_HP_LOAD_MAP)
	{
	  /* Dld will place the address of the load map at load_map_addr
	     after it starts running.
	   */
	  if (target_read_memory (entry_addr + offsetof (Elf64_Dyn,
							 d_un.d_ptr),
				  (char *) &dld_cache_p->load_map_addr,
				  sizeof (dld_cache_p->load_map_addr))
	      != 0)
	    {
	      error ("Error while reading in .dynamic section of the program.");
	    }
	  /* Flip endian for 64-bit elf data */
	  if (!elf_is_header_big_endian (symfile_objfile->obfd))
	    dld_cache_p->load_map_addr =
	      bfd_getl64 ((unsigned char *) &dld_cache_p->load_map_addr);
	}
      else if (dyn_tag == 0x15)  /* Linux DT_DEBUG */
	{
	  /* Remember the address where LRE dld will put debug_base */
	  dld_cache_p->lre_debug_base_addr = 
		entry_addr + offsetof (Elf64_Dyn, d_un.d_ptr);
	}
      else if (dyn_tag == DT_HP_TIME_STAMP)
	{
	  /* If the dld.so in the core is old, i.e., it doesn't have the
	   * new capability of dlgetmodinfo(), disable core library mismatch
	   * detection.  Otherwise results will be bogus.
	   */
  	  /* The last dld without the new functionality was for 11.23 which 
	   * was build on May 15th, 2003.  This date corresponds to May 16h.
	   */
  	  const int new_dld_date = 0x3ec50000;
          if (x_dynp->d_un.d_val < new_dld_date)
	    core_so_mismatch_detection_disabled = 1;
	}
      else if (dyn_tag == DT_FLAGS)
        {
          /* Find if this is BOR/bind-immediate */
          dld_cache_p->is_bor = !(x_dynp->d_un.d_val & DF_BIND_NOW);
        }
      else
	{
	  /* tag is not of interest */
	}
    }				/* end for buf < bufend */

  /* Record other information and set is_valid to TRUE. */

  dld_cache_p->dyninfo_sect = dyninfo_sect;

  /* Verify that we read in required info.  These fields are re-set to zero
     in hp_ia64_solib_restart.
   */
  if (IS_LRE_HACK)
    {
      dld_cache_p->is_valid = TRUE;
      return TRUE;  /* Nothing to read in at the moment for Linux binary */
    }

  if (dld_cache_p->dld_flags_addr != 0 && dld_cache_p->load_map_addr != 0)
    dld_cache_p->is_valid = TRUE;
  else
    return FALSE;		/* something was not read in */

  return TRUE;
}				/* read_dynamic_info */

static boolean
read_dynamic_info (asection * dyninfo_sect, dld_cache_t * dld_cache_p)
{
  if (bfd_is_elf32 (exec_bfd))
    return read_dynamic_info32 (dyninfo_sect, dld_cache_p);
  else
    return read_dynamic_info64 (dyninfo_sect, dld_cache_p);
} /* read_dynamic_info */

/* read_tgt_mem
   This is just a wrapper so that dlgetmodinfo1 can call a read routine
   with the calling sequence it expects.  The ident parameter is
   ignored.
 */


static void *
read_tgt_mem (void *buffer, uint64_t ptr, size_t bufsiz, int ident)
{
  if (target_read_memory (ptr, (char *) buffer, (int) bufsiz) != 0)
    return 0;			/* failure */
  else
    return buffer;
}				/* end read_tgt_mem */

/* Set the load_info.li_flags bit in the load_info structure to the
   indicated value (0 or 1).
   value == ENABLE_BOR indicates that we want to enable BOR callbacks.
   Side effect: The text address of the micro-loader is placed in 
     uld_text_addr.

   A pointer to the load_info structure is the fourth argument passed by
   the kernel.
   */

void
set_trace_bit (int value)
{
#ifndef HP_IA64_GAMBIT
  
  CORE_ADDR 		arg_list_addr;
  int			status;

  status = call_ttrace (TT_PROC_GET_ARGS,
			inferior_pid,
			(TTRACE_ARG_TYPE) &arg_list_addr,
			(TTRACE_ARG_TYPE) sizeof (CORE_ADDR),
			(TTRACE_ARG_TYPE) TT_NIL);
  if (status == -1 && errno)
      error ("Unable to find the load_info structure.");

  status = read_inferior_memory (arg_list_addr + 3 * sizeof (CORE_ADDR),
				 (char *) &load_info_addr,
				 sizeof (CORE_ADDR));
  if (status != 0)
      error ("Unable to read the load_info structure address.");

  read_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));
  load_info.li_flags &= ~(LI_TRACE | LI_BOR | LI_FORCE_MAP_SHARED);
  if (value)
    load_info.li_flags |= (mapshared ?
      (LI_TRACE | LI_FORCE_MAP_SHARED) : (LI_TRACE));
  /* Enable BOR callbacks */
  if (value == ENABLE_BOR)
    load_info.li_flags |= LI_BOR;
  uld_text_addr = load_info.li_uld_taddr;
  write_inferior_memory (load_info_addr, (char*) &load_info, sizeof(load_info));
#endif
}

/* start_bor_call START_BOR_CALL
   
   start_bor_call is called in wait_for_inferior when we are handling a 
   subroutine call.  It is given the destination address of 
   SKIP_TRAMPOLINE_CODE.  If that address is .bortext then return TRUE,
   else return FALSE.
 */

boolean 
start_bor_call (CORE_ADDR target_addr)
{
  struct minimal_symbol *msymbol;

  msymbol = lookup_minimal_symbol_by_pc (target_addr);
  if (msymbol && strcmp (SYMBOL_NAME (msymbol), ".bortext") == 0)
    return TRUE;

  return FALSE;
} 

/* solib_bor_event
   
   solib_bor_event should be called when we are in the notification routine.
   If the event is  BREAK_DE_BOR, return the bound address which is 
   in $arg1 == $gr33, otherwise return 0.
 */

CORE_ADDR 
solib_bor_event ()
{
  CORE_ADDR arg0;
  CORE_ADDR bound_addr;

  /* The first argument (in ARG0_REGNUM) to the break routine is the reason 
     for the notification.  It should be BREAK_DE_BOR 
   */

  arg0 = read_register (ARG0_REGNUM);
  if (arg0 != BREAK_DE_BOR)
    return FALSE;

  bound_addr = read_register (ARG1_REGNUM);
  return bound_addr;
}

#if 0
static void
hp_ia64_core_exe_mismatch_detection (
  struct load_module_desc *exe_lmdp,
  CORE_ADDR exe_timestamp,
  CORE_ADDR exe_checksum
)
{
  if (core_so_mismatch_detection_disabled)
    return;

  if (exe_lmdp->time_stamp == 0)
    return;

  if (exe_lmdp->checksum != exe_checksum)
       warning ("The executable checksum does not match the one recorded \n"
                "in the core file, a possible mismatch with the core and exe.\n"
                "This can lead to wrong debugging output with the core file.");

  if (exe_lmdp->time_stamp != exe_timestamp)
       warning ("The executable timestamp does not match the one recorded \n"
                "in the core file, a possible mismatch with the core and exe");

  return;
}
#endif

/*----------------------------------------------------------------------------*/
/* Description: This function takes a so_list* argument, reads the link time for
 * the associated shared library from its dynamic section and compares that to 
 * the one returned by dlgetmodinfo() for the same shared library in the core.
 * If they don't match a warning message is issued.
 */
static void
hp_ia64_core_so_mismatch_detection (struct so_list *so_ptr)
{
  static int printed_once;

  char *buf;
  asection *dyn_sect;
  int dyn_sect_size;
  unsigned long so_time_stamp;
  struct load_module_desc *solib_desc = (struct load_module_desc *)
                                        (so_ptr->solib_desc);

  /* This functionality depends upon a new version of dlgetmodinfo(), i.e., dld.so.
   * If this dld.so isn't out there, this needs to be disabled to prevent wrong
   * information from being given out.  Once the new dld.so has been out for a 
   * while the follwing check and the two that are in read_dynamic_info32 & 64
   * should be removed.		Bharath, 25 Nov 2003.
   */
  if (core_so_mismatch_detection_disabled)
    return;

  /* If the gdb is using an old dld.so, irrespective of whether the core used the new
   * or the old dld.so, disable mismatch detection.
   */
  if (solib_desc->time_stamp == 0)
    {
      core_so_mismatch_detection_disabled = 1;
      return;
    }

  /* QXCR1000890610 : IA64 debugger will not correctly unwind core if dld.so
     used is not the same. Since a dld.so mismatch caused issues with
     unwinding some threads beyond some frames within libc.so, the following
     portion is being commented out so that "dld.so" library mismatch between
     that of the core and the system gets detected. */
  /* Skip the check for dld; it isn't accurate or relevant.
  if (strcmp (basename (so_ptr->name), "dld.so") == 0)
    return; */

  dyn_sect = bfd_get_section_by_name (so_ptr->abfd, ".dynamic");

  if (dyn_sect == NULL)
    error ("\n%s doesn't have a .dynamic section.  Could be a bad library.\n", so_ptr->name);

  /* Read in .dynamic section.  */
  dyn_sect_size = (int) bfd_section_size (so_ptr->abfd, dyn_sect);
  buf = alloca (dyn_sect_size);
  if (!bfd_get_section_contents (so_ptr->abfd, dyn_sect, buf, 0, dyn_sect_size))
    error ("\nCan't read the .dynamic section for %s.\n", so_ptr->name);

  /* Read in the time stamp from the .dynamic section. */
  if (bfd_is_elf32 (so_ptr->abfd))
    {
      Elf32_Dyn *dyn_sect_p = (Elf32_Dyn *) (void *) buf; 

      while (dyn_sect_p->d_tag != DT_NULL && dyn_sect_p->d_tag != DT_HP_TIME_STAMP)
	dyn_sect_p++;

      so_time_stamp = dyn_sect_p->d_un.d_val;
    }
  else	/* Assuming that the alternate is 64-bit ELF! */
    {
      Elf64_Dyn *dyn_sect_p = (Elf64_Dyn *) (void *) buf;

      while (dyn_sect_p->d_tag != DT_NULL && dyn_sect_p->d_tag != DT_HP_TIME_STAMP)
	dyn_sect_p++;

      so_time_stamp = (unsigned long) dyn_sect_p->d_un.d_val;
    }

  if ((solib_desc->time_stamp != so_time_stamp) && !printed_once)
    {
      warning ("Some of the libraries in the core file are different "
               "from the libraries on this computer.  It might be "
               "possible to proceed with your debugging process "
               "successfully.  However, if you run into problems you "
               "must use packcore command or use the versions of the "
               "libraries used by the core.  The mismatches are: \n");
      printed_once = 1;
    }

  so_ptr->mismatch = 0;

  if (solib_desc->time_stamp != so_time_stamp)
    {
       printf ("  %s in the core file is different from\n  %s used by gdb\n\n",
               so_pathname_g, so_ptr->name);
       time_t   time_val = (time_t) (uint64_t) solib_desc->time_stamp;
       char*    time_str = ctime(&time_val);

       printf ("  core file %s timestamp = %s\n",
               so_pathname_g, time_str);

       time_val = (time_t) (uint64_t) so_time_stamp;
       time_str = ctime(&time_val);

       printf ("  %s used by gdb timestamp = %s\n",
               so_ptr->name, time_str);
       so_ptr->mismatch = 1;
    }
}

void
ia64_request_bor_callback (int needed)
{
  /* JAGag12140: Enabling should be done only while step operations */
  if (needed)
    SET_TRACE_BIT (ENABLE_BOR);
  else
    SET_TRACE_BIT (SET_FLAGS);
}

boolean
hp_ia64_is_bor(void)
{
  return dld_cache.is_bor;
}
/*----------------------------------------------------------------------------*/
