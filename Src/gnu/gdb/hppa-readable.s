;----- hppa_location_readable
;
;	Input :		arg0 contains an address to check for read access
;	on exit		ret0 non-zero if readable, zero otherwise
;
;--------------------------------------------------------------

	.space		$TEXT$
	.export		hppa_location_readable,entry,priv_lev=3
	.align		4

	.proc
hppa_location_readable
	.callinfo
	.enter
	ldi		3,%r1				;lowest privilege.
	dep		r0,31,2,%arg0			;word align %arg0.
	ldsid		(0,%arg0),%r31			;get space-id for
	mtsp		%r31,%sarg			;quick cmp.
	prober		(%sarg,%arg0),%r1,%ret0
	.leave
	.procend
