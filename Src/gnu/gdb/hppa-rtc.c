/* Target specific file to be compiled along with infrtc.c to generate
 * librtc.sl. 
 * This file has to define the functions:
 *	int get_frame_pc_list (void** pc_list)
 *	void libcl_stuff ()
 * Target: hppa
 */

#include <dl.h>
#include "defs.h"
#include "rtc.h"

extern void obtain_backdoor_entry (void);
#define OBTAIN_BACKDOOR_ENTRY() if (!libc_malloc) obtain_backdoor_entry ()
extern void * (*libc_malloc)  (size_t size);
extern int rtc_started;
extern int rtc_disabled;
extern char * stack_base;


/* On PA32, we end up triggering a nasty bug in libcl that causes the
   process stack to grow arbitrarily. (Search for probe in this file
   for a description of the problem.) As a result we need to intercept
   and sanitize a few calls to libcl unwind routines. Under the
   LD_PRELOAD mechanism we cannot call the libcl functions by name as
   the calls would end up right within this module leading to infinite
   recursion. So we need to materialize pointers using shl_findsym.
*/
typedef struct unwind_info {
    void * unw_start;
    void * unw_end;
} unwind_info;

typedef struct rtable
{
  void * start;
  void * end;
} rtable;

/* Data structures for stack unwind mechanism  on PA*/

typedef struct cframe_info {
  unsigned long cur_frsize; /* frame size */
  unsigned long cursp; /* stack pointer */
#ifndef GDB_TARGET_IS_HPPA_20W
  unsigned long currls; /* PC-space of CALLING routine */
#endif
  unsigned long currlo; /* PC-offset of CALLING routine */
  unsigned long curdp; /* data pointer */
  unsigned long toprp; /* return pointer */
  unsigned long topmrp; /* millicode return pointer */
#ifndef GDB_TARGET_IS_HPPA_20W
  unsigned long topsr0; /* sr0 */
  unsigned long topsr4; /* sr4 */
#endif
  unsigned long r3; /* gr3 */
#ifndef GDB_TARGET_IS_HPPA_20W
  unsigned long cur_r19; /* linkage-table pointer (gr19) - for PIC code */
#endif
  unsigned long r4; /* gr4 */ /* Added for alloca unwind interface */
#ifdef GDB_TARGET_IS_HPPA_20W
  unsigned long reserved[4];
#else
  unsigned long reserved[8];  /* Added for alloca unwind interface */
#endif
} cframe_info;


typedef struct pframe_info {
  unsigned long prev_frsize; /* frame size */
  unsigned long prevsp; /* stack pointer */
#ifndef GDB_TARGET_IS_HPPA_20W
  unsigned long prevrls; /* PC-space of CALLING routine */
#endif
  unsigned long prevrlo; /* PC-offset of CALLING routine */
  unsigned long prevdp; /* data pointer */
  unsigned int udescr0; /* first half of unwind descriptor */
  unsigned int udescr1; /* second half of unwind descriptor */
  unsigned int ustart; /* start of the unwind region */
  unsigned int uend; /* end of the unwind region */
  unsigned long uw_index; /* index into the unwind table */
#ifndef GDB_TARGET_IS_HPPA_20W
  unsigned long prev_r19; /* linkage-table pointer (gr19) - for PIC code */
#endif
  unsigned long PFIinitR3;  /* Added for alloca unwind interface */
  unsigned long PFIinitR4;  /* Added for alloca unwind interface */
#ifdef GDB_TARGET_IS_HPPA_20W
  unsigned long reserved[4];
#else 
  unsigned long reserved[8];
#endif
} pframe_info;

extern int U_get_previous_frame_x(struct cframe_info*, struct pframe_info*, int size);
extern void U_prep_frame_rec_for_unwind(struct cframe_info*);
extern void U_init_frame_record(struct cframe_info*);

#ifndef GDB_TARGET_IS_HPPA_20W

void U_copy_frame_info(struct cframe_info *current, struct pframe_info *previous);

#pragma HP_NO_RELOCATION U_get_shLib_text_addr
#pragma HP_NO_RELOCATION U_get_shLib_unw_tbl
#pragma HP_NO_RELOCATION U_get_shLib_recv_tbl

#pragma HP_LONG_RETURN U_get_shLib_text_addr
#pragma HP_LONG_RETURN U_get_shLib_unw_tbl
#pragma HP_LONG_RETURN U_get_shLib_recv_tbl

void * (*libcl_U_get_shLib_text_addr) (char *);
unwind_info (*libcl_U_get_shLib_unw_tbl) (char *);
rtable (*libcl_U_get_shLib_recv_tbl) (char *);

void
libcl_stuff ()
{
  int index;
  index = 1;
  struct shl_descriptor *desc;
  while (shl_get(index, &desc) != -1)
    {
      index++;

      if (strstr(desc->filename, "libcl."))
        {
          shl_findsym (&desc -> handle, "U_get_shLib_text_addr",
                       TYPE_PROCEDURE, &libcl_U_get_shLib_text_addr);
          shl_findsym (&desc -> handle, "U_get_shLib_unw_tbl",
                       TYPE_PROCEDURE, &libcl_U_get_shLib_unw_tbl);
          shl_findsym (&desc -> handle, "U_get_shLib_recv_tbl",
                       TYPE_PROCEDURE, &libcl_U_get_shLib_recv_tbl);
        }
    }
}

/* Obscurity alert ! The unwind library (libcl.sl) uses these ugly
   routines called U_get_shLib_text_addr (), U_get_shLib_unw_tbl()
   and U_get_shLib_recv_tbl () to determine the text_start address
   of a shared library and its unwind tables given its R19 value.
   They accomplish this by internal knowledge of what R19 itself
   points to. There is a way to reach into the some dld data structures
   for a library given its R19. (See Sec 6.3.10) The value of R19 itself
   is materialized by the caller by reading the current context or
   by picking it from the frame marker area.

   These ugly functions do not trust the input parameters as it could
   have come from anywhere and accessing that location with impunity
   could result in a sigsegv. So they do two things :

       o Make sure that the *R19 is readable. This is accomplished
         via the PA-RISC probe instruction. This instruction lets
         a program query whether it can actually access a location
         without generating a fault.
         
       o If the location is accessible, read value at some offset
         and see if it matches some magic cookie value left there
         by dld.

    If both tests pass, these functions use their knowledge of the
    layout of the shl_descriptor structure to retrieve the text_start
    and unwind tables.

    When the probe instruction is executed, if the address probed
    is not currently mapped in, but is in a automatically growing region
    like the stack, the kernel extends the process space !

    Assume that the process SP is 200 MB away from the end of Q2.

  Process : Mr. Kernel, can I access this location at 0x7fffffff ?
  Kernel  : Hmm, let us see. That address is in the second quadrant
            and is really your stack. Those pages are not mapped in
            right now, but looks like you need more stack space. 
            Ok, lemme grow your stack.

  After this imaginary conversation, the system begins crawling ...
  This problems exists only on 32 bit systems as on 64 bit runtime
  they don't probe all over the place : they ask dld.

  So what we will do is intercept these calls and inspect the address.
  If this address is above the stack_base we have computed, we will
  return failure. See that RTC is disabled after an attach, so this is
  not an issue. When the program is started under the debugger all
  shared libraries are mapped before the stack base. No valid R19
  can be greater than the stack base.

  Stack base is initialized by gdb with whatever it thinks is the
  correct value. We will override it with what we compute. This is
  needed so as to avoid triggering this bug even we as compute the
  stack base in determine_stack_base ();

  References : /CLO/Components/UNWIND/Src/UX/utable_lookups.c
               /CLO/Components/UNWIND/Src/UX/unwind.s
               PA Runtime Architecture Document v3.1 P 8-250.
               PA Runtime Architecture Document Sec 6.3.10.
*/

void *
U_get_shLib_text_addr (char * r19)
{
  char * dld_info;

  OBTAIN_BACKDOOR_ENTRY();

  /* Now we could get here while computing the stack base ... */
  if (rtc_started == 0 || rtc_disabled == 1 || !stack_base)
    return libcl_U_get_shLib_text_addr (r19);

  r19 = (char *) (((long) r19) & ~3L);
  if (r19 < stack_base)
    {
      if (hppa_location_readable (r19))
        {
          dld_info = r19 + ((*((long *) r19)) & ~3L);
          if (dld_info < stack_base)
            return libcl_U_get_shLib_text_addr (r19);
        }
    }

  return (void *) -1;
}


unwind_info
U_get_shLib_unw_tbl (char * r19)
{
  unwind_info retval, U_get_shLib_unw_tbl (char *);
  char * dld_info;

  OBTAIN_BACKDOOR_ENTRY();

  /* Now we could get here while computing the stack base ... */
  if (rtc_started == 0 || rtc_disabled == 1 || !stack_base)
    return libcl_U_get_shLib_unw_tbl (r19);

  r19 = (char *) (((long) r19) & ~3L);
  if (r19 < stack_base)
    {
      if (hppa_location_readable (r19))
        {
          dld_info = r19 + ((*((long *) r19)) & ~3L);
          if (dld_info < stack_base)
            return libcl_U_get_shLib_unw_tbl (r19);
        }
    }
  retval.unw_start = (void *) ~0L;
  return retval;
}

struct rtable
U_get_shLib_recv_tbl (char * r19)
{
  rtable retval, U_get_shLib_recv_tbl (char *);
  char * dld_info;

  OBTAIN_BACKDOOR_ENTRY();

  /* Now we could get here while computing the stack base ... */
  if (rtc_started == 0 || rtc_disabled == 1 || !stack_base)
    return libcl_U_get_shLib_recv_tbl (r19);

  r19 = (char *) (((long) r19) & ~3L);
  if (r19 < stack_base)
    {
      if (hppa_location_readable (r19))
        {
          dld_info = r19 + ((*((long *) r19)) & ~3L);
          if (dld_info < stack_base)
            return libcl_U_get_shLib_recv_tbl (r19);
        }
    }
  retval.start = (void *) ~0L;
  return;
}

/* The trap and unwind facility in libcl.sl hides some of the
   routines in the PA32 incarnation. For reasons not obvious to
   me. Here is a local version donated by Dennis Handly.
*/

void 
U_copy_frame_info (cframe_info *curr_frame, pframe_info *prev_frame)
{
  /* Update curr_frame with values returned in prev_frame */
  curr_frame->cur_frsize = prev_frame->prev_frsize;
  curr_frame->cursp = prev_frame->prevsp;
  curr_frame->currls = prev_frame->prevrls;
  curr_frame->currlo = prev_frame->prevrlo;
  curr_frame->curdp = prev_frame->prevdp;
  curr_frame->r3 = prev_frame->PFIinitR3;
  curr_frame->r4 = prev_frame->PFIinitR4;

  /* don't update curr_frame.cur_r19 because U_get_previous_frame does
   * it directly.
   */
   
#ifndef HPUX_1020
  curr_frame->cur_r19 = prev_frame->prev_r19;
#endif
}

#endif /* !GDB_TARGET_IS_HPPA_20W */

int
get_frame_pc_list (void** pc_list, int frame_count)
{
  cframe_info curr_frame;
  pframe_info prev_frame;
  int level = 0, j;
  int U_error;

  /* PA stack unwinding. */
#ifndef HPUX_1020
  U_init_frame_record (&curr_frame);
  U_prep_frame_rec_for_unwind (&curr_frame);
#ifndef GDB_TARGET_IS_HPPA_20W
  curr_frame.topsr0 = 0;
  curr_frame.topsr4 = 0;
#endif /* !GDB_TARGET_IS_HPPA_20W */
#else /* HPUX_1020 */
    U_get_frame_info(&curr_frame);
#endif /* HPUX_1020 */

  /* At this point curr_frame is pointing to the frame corresponding
     to get_frame_pc_list, which is called by rtc_record_malloc (),
     which is called by rtc_*alloc() which is called by the application.
     Pop two levels above to reach into user space code ...
   */ 


  for (j=0; j < 3; j++)
    {
      U_error = U_get_previous_frame_x (&curr_frame,
                                      &prev_frame,
                                      sizeof (cframe_info));
      if (U_error)
        return level;

      U_copy_frame_info (&curr_frame, &prev_frame);
    }

  /* curr_frame has the stack frame details pertaining to 
     the caller of rtc_*alloc : record frame_count number of
     frames from there. 
  */

  do 
    {
      if (curr_frame.currlo)
        {
          /* U_get_previous_frame_x () has the side effect of
             adjusting the current frame if it is pointing to a stub.
             So don't record the PC right now. Obtain the previous 
             frame and record current PC after any adjustmnents.
          */
          U_error = U_get_previous_frame_x(&curr_frame,
                                         &prev_frame,
                                         sizeof(cframe_info));
          if (U_error)
            return level;

          /* reach for the call and not the return address ... */
          pc_list[level++] = (void *) ((curr_frame.currlo & ~3L) - 4);
          U_copy_frame_info (&curr_frame, &prev_frame);
        }
     } while ( level < frame_count && curr_frame.currlo);

  return level;
}
