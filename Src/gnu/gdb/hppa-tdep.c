/* Target-dependent code for the HP PA architecture, for GDB.
   Copyright 1986, 1987, 1989-1996, 1999-2000 Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>

#include "defs.h"
#include "frame.h"
#include "bfd.h"
#include "elf-bfd.h"
#include "inferior.h"
#include "value.h"

/* For argument passing to the inferior */
#include "symtab.h"

/* For Fortran array descriptor routines */
#include "f-lang.h"

#ifdef USG
#include <sys/types.h>
#endif

#include <dl.h>
#include <sys/param.h>
#include <signal.h>
#include <siginfo.h>

#include <sys/ptrace.h>
#include <sys/syscall.h>
#include <machine/save_state.h>

#ifdef COFF_ENCAPSULATE
#include "a.out.encap.h"
#else
#endif

/*#include <sys/user.h>         After a.out.h  */
#include <sys/file.h>
#include "gdb_stat.h"
#include "gdb_wait.h"

#include "gdbcore.h"
#include "gdbcmd.h"
#include "target.h"
#include "symfile.h"
#include "objfiles.h"
#include "command.h"

#include <memory.h>
#include "hpread.h"
#include "javalib.h"	/* For Java Stack trace */

#include "gdbtypes.h"

/* To support detection of the pseudo-initial frame
   that threads have. */
#define THREAD_INITIAL_FRAME_SYMBOL  "__pthread_exit"
#define THREAD_INITIAL_FRAME_SYM_LEN  sizeof(THREAD_INITIAL_FRAME_SYMBOL)

#ifdef HP_MXN
#define MXN_THREAD_INITIAL_FRAME_SYMBOL  "__pthread_unbound_body"
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
typedef enum {
        STUB_NONE,
        STUB_SOLIB_CALL,
        STUB_LONG_CALL,
	STUB_CXX_ADJUST_THIS
#ifdef COVARIANT_SUPPORT
	,STUB_CXX_ADJUST_COVARIANT_RETURN
#endif
  } stub_type;

/* The most instructions in a stub */
#define MAX_STUB_INST           7
#else
CORE_ADDR redirect_through_dummy_addr = -1;
#define REDIRECT_THROUGH_DUMMY_FRAMESIZE 64
#endif

int call_dummy_frame; //For JAGaf13527
extern boolean print_bad_pc_unwind_msg;

extern asection *bfd_get_section_by_name_and_id (bfd *abfd,
						 const char *name,
						 int id);
#ifdef GDB_TARGET_IS_HPPA_20W
CORE_ADDR fetch_ap_for_gcc_frame (char buf[], struct frame_info *frame, 
                                  int regnum);
int scan_gcc_prologue_for_saved_ap (struct frame_info *frame, 
                                    int *saved_ap_regnum, int *saved_ap_offset);
extern int extract_5_store (unsigned int);
#endif

static int extract_5_load (unsigned int);

static unsigned extract_5R_store (unsigned int);

static unsigned extract_5r_store (unsigned int);

static void find_dummy_frame_regs (struct frame_info *,
				   struct frame_saved_regs *);

static int find_proc_framesize (CORE_ADDR);

static int find_return_regnum (CORE_ADDR);

struct unwind_table_entry *hppa_find_unwind_entry (CORE_ADDR);

static int extract_17 (unsigned int);

unsigned deposit_21 (unsigned int, unsigned int);

static int extract_21 (unsigned);

static int extract_22 (unsigned);

unsigned deposit_14 (int, unsigned int);

int extract_14 (unsigned);

static void unwind_command (char *, int);

static int low_sign_extend (unsigned int, unsigned int);

static int sign_extend (unsigned int, unsigned int);

static int restore_pc_queue_1 (CORE_ADDR, CORE_ADDR);

static int restore_pc_queue (struct frame_saved_regs *);

static int hppa_alignof (struct type *);

/* To support multi-threading and stepping. */
extern int hppa_prepare_to_proceed (void);

CORE_ADDR skip_prologue_hard_way (CORE_ADDR);

CORE_ADDR skip_epilogue_hard_way (CORE_ADDR);

static int prologue_inst_adjust_sp (unsigned long);

static int is_branch (unsigned long);

static int inst_saves_gr (unsigned long);

static int inst_saves_fr (unsigned long);

static int pc_in_interrupt_handler (CORE_ADDR);

int hppa_pc_in_gcc_function (CORE_ADDR);

static int pc_in_linker_stub (CORE_ADDR);

static int compare_unwind_entries (const void *, const void *);

static void read_unwind_info (struct objfile *);

static void internalize_unwinds (struct objfile *,
				 struct unwind_table_entry *,
				 asection *, unsigned int,
				 unsigned int, CORE_ADDR);
static void pa_print_registers (char *, int, int);
static void pa_strcat_registers (char *, int, int, struct ui_file *);
void pa_register_look_aside (char *, int, long * );
static void pa_print_fp_reg (int, enum precision_type);
static void pa_strcat_fp_reg (int, struct ui_file *, enum precision_type);
static void record_text_segment_lowaddr (bfd *, asection *, void *);

typedef struct
  {
    struct minimal_symbol *msym;
    CORE_ADDR solib_handle;
    CORE_ADDR return_val;
  }
args_for_find_stub;

static int cover_find_stub_with_shl_get (PTR);

static int is_pa_2 = 0;		/* False */

/* This is declared in infrun.c. */
extern CORE_ADDR prev_pc;

/* This is declared in symtab.c; set to 1 in hp-symtab-read.c */
extern int hp_som_som_object_present;

/* In breakpoint.c */
extern int exception_catchpoints_are_fragile;

/* This is defined in valops.c. */
extern value_ptr find_function_in_inferior (char *);

#ifdef GDB_TARGET_IS_HPPA_20W
extern int stop_stack_dummy;
#endif

extern int endo_missing_in_symfile_objfile;
extern int found_endo_export_list;

ExportEntry * find_symbol_in_export_list (char *, struct objfile *);

/* Fix for JAGae99882.  Bharath, 9 Feb 2004. */

struct target_si_code_t si_code_map[] =
{
  {SIGILL, ILL_UNKNOWN, "ILL_UNKNOWN - Unknown Error"},
  {SIGILL, ILL_ILLOPC, "ILL_ILLOPC - Illegal Op-Code"},
  {SIGILL, ILL_ILLBRKINST, "ILL_ILLBRKINST - Break instruction trap"},
  {SIGILL, ILL_PRVOPC, "ILL_PRVOPC - Privileged instruction trap "},
  {SIGILL, ILL_PRVREG, "ILL_PRVREG - Privileged register trap"},
  {SIGILL, ILL_ILLOPN, "ILL_ILLOPN - Illegal Operand"},
  {SIGILL, ILL_ILLADR, "ILL_ILLADR - Illegal addressing mode"},
  {SIGILL, ILL_ILLTRP, "ILL_ILLTRP - Illegal trap"},
  {SIGILL, ILL_COPROC, "ILL_COPROC - Coprocessor error"},
  {SIGILL, ILL_BADSTK, "ILL_BADSTK - Internal stack error"},

  {SIGFPE, FPE_UNKNOWN, "FPE_UNKNOWN - Unknown Error"},
  {SIGFPE, FPE_FLTOVF, "FPE_FLTOVF - Floating pt. overflow"},
  {SIGFPE, FPE_COND, "FPE_COND - HP Conditional trap"},
  {SIGFPE, FPE_ASSISTX, "FPE_ASSISTX - HP Assist exception trap"},
  {SIGFPE, FPE_ASSISTEM, "FPE_ASSISTEM - HP Assist emulation trap"},
  {SIGFPE, FPE_INTDIV, "FPE_INTDIV - integer divide by zero"},
  {SIGFPE, FPE_INTOVF, "FPE_INTOVF - integer overflow"},
  {SIGFPE, FPE_FLTDIV, "FPE_FLTDIV - floating point divide by zero"},
  {SIGFPE, FPE_FLTUND, "FPE_FLTUND - floating point underflow"},
  {SIGFPE, FPE_FLTRES, "FPE_FLTRES - floating point inexact result"},
  {SIGFPE, FPE_FLTINV, "FPE_FLTINV - invalid floating point operation"},
  {SIGFPE, FPE_FLTSUB, "FPE_FLTSUB - subscript out of range"},

  {SIGSEGV, SEGV_UNKNOWN, "SEGV_UNKNOWN - Unknown Error"},
  {SIGSEGV, SEGV_MAPERR, "SEGV_MAPERR - Address not mapped to object"},
  {SIGSEGV, SEGV_ACCERR, "SEGV_ACCERR - Invalid Permissions for object"},

  {SIGBUS, BUS_UNKNOWN, "BUS_UNKNOWN - Unknown Error"}, 
  {SIGBUS, BUS_ADRALN, "BUS_ADRALN - Invalid address alignment. Please refer to the following link that helps in handling unaligned data: http://docs.hp.com/en/7730/newhelp0610/pragmas.htm#pragma-pack-ex3"},
  {SIGBUS, BUS_ADRERR, "BUS_ADRERR - Non-existant physical address"},
  {SIGBUS, BUS_OBJERR, "BUS_OBJERR - Object specific hardware error"},
  
  {SIGTRAP, TRAP_UNKNOWN, "TRAP_UNKNOWN - Unknown Error"},
  {SIGTRAP, TRAP_BRKPT, "TRAP_BRKPT - Process breakpoint"},
  {SIGTRAP, TRAP_TRACE, "TRAP_TRACE - Process trace trap"},

  {SIGCHLD, CLD_EXITED, "CLD_EXITED - Child exited "},
  {SIGCHLD, CLD_KILLED, "CLD_KILLED - Abnormal termination, no core file"},
  {SIGCHLD, CLD_DUMPED, "CLD_DUMPED - Abnormal termination w/core file"},
  {SIGCHLD, CLD_TRAPPED, "CLD_TRAPPED - Traced child has trapped"},
  {SIGCHLD, CLD_STOPPED, "CLD_STOPPED - Child has stopped"},
  {SIGCHLD, CLD_CONTINUED, "CLD_CONTINUED - Stopped child has continued"},

  {SIGPOLL, POLL_IN, "POLL_IN - Data input available"},
  {SIGPOLL, POLL_OUT, "POLL_OUT - Output buffers available"},
  {SIGPOLL, POLL_MSG, "POLL_MSG - Input message available"},
  {SIGPOLL, POLL_ERR, "POLL_ERR - I/O Error"},
  {SIGPOLL, POLL_PRI, "POLL_PRI - High priority input available"},
  {SIGPOLL, POLL_HUP, "POLL_HUP - Device disconnected"},

  {SIGGFAULT, GFAULT_EXPLICIT, "GFAULT_EXPLICIT - SIGGFAULT caused by GCLOCK"},
  {SIGGFAULT, GFAULT_IMPLICIT, "GFAULT_IMPLICIT - SIGGFAULT caused by fault on graphics device"}
};

const int si_code_map_size = sizeof (si_code_map) /
			     sizeof (struct target_si_code_t);

#ifdef GET_LONGJMP_TARGET
int
get_longjmp_target(CORE_ADDR *pc)
{
  CORE_ADDR jb_addr;
  char buf[TARGET_PTR_BIT / TARGET_CHAR_BIT];
 
  jb_addr = read_register (ARG0_REGNUM);
  /* the pc where the step-over of a longjmp call should result in is the one that follows the
     corresponding setjmp call in the program. We get this value from the jump-buffer structure,
     the address of which is got from the argument register. The address of jump-buffer 
     is the first argument to a longjmp call */
#ifdef GDB_TARGET_IS_HPPA_20W
  if (target_read_memory ((CORE_ADDR) jb_addr,
			  buf,
			  TARGET_PTR_BIT / TARGET_CHAR_BIT))
     return 0;
#else
  if (target_read_memory ((CORE_ADDR) (jb_addr + 176),
			  buf,
			  TARGET_PTR_BIT / TARGET_CHAR_BIT))
     return 0;
#endif
  *pc = extract_address (buf, TARGET_PTR_BIT / TARGET_CHAR_BIT) & ~0x3;

  return 1; 
}
#endif

/* indicates if the given frame is the topmost frame in the stack. */
int
is_topmost_frame (frame)
  	struct basic_frame_info *frame;
{
  struct frame_info *topmost;

  topmost = get_current_frame(); /* Get top most frame */

  /* Verify if the frame's SP matches the value of the SP register
     and the top most frame's pc matches the argument frame's pc */
  if ( (frame->sp == read_sp()) && (frame->pc == topmost->pc) )
    return 1;

  return 0;
}

/* Given a basic_frame, get the basic frame information for the next frame */
int
java_get_next_frame (struct basic_frame_info *frame, struct basic_frame_info *nextfrm)
{
  struct frame_info *next;
  next=get_current_frame(); /* Start from top frame */
  while (next!=0) 
  {
    if (next->frame == frame->sp) /* found */
    {
      /* fill up the frame information */
      nextfrm->pc = next->pc;
      nextfrm->fp = next->frame; 
      if (next->next)
        nextfrm->sp = next->next->frame;
      else
        nextfrm->sp = read_sp(); /* Top frame */
      return 1;
     } 
     else 
       next=next->prev;
  }

  /* Not found. */
  nextfrm->pc = nextfrm->sp = nextfrm->fp = 0;
  return 0;
}

/* indicate if the given frame is the caller of a signal handler. */
int is_next_frame_signal_handler (frame)
        struct basic_frame_info *frame;
{
  struct frame_info *next;
  next = get_current_frame();
  while (next) 
  {
    if ( next->frame == frame->sp ) 
    { /* Found the next frame. */
      if ( next->signal_handler_caller ) 
	return 1;
      return 0;
    } 
    next = next->prev;
  }
  return 0;
}

/* return register regnum for frame in reg */
int 
java_get_saved_register (struct basic_frame_info *frame, int regnum,
                CORE_ADDR *reg)
{
  extern int read_relative_register_raw_bytes (int regnum, char *myaddr);
  struct frame_info *next,*prev;
  CORE_ADDR addr;

  /* For top frame, read from context */
  if (is_topmost_frame (frame)) {
    if (read_relative_register_raw_bytes(regnum, (char*) &addr))
      return 0;
    *reg= (CORE_ADDR) addr;
    return 1;
  }
  prev=get_current_frame();

  /* Find corresponding frame and read its registers, use sp as handle */
  while (prev!=0) {
    if (prev->frame == frame->sp) /* SP matched */
    {
      if (read_relative_register_raw_bytes_for_frame(regnum, 
	(char *) &addr, prev->prev))
	return 0;
      *reg= (CORE_ADDR) addr;
      return 1;
    }
    prev = prev->prev;
  }
  return 0;
}


extern int java_debugging;

/* Check if we need to handle bad PC values */
int 
handle_bad_pc(struct frame_info * frame)
{
  /* Are we the top frame or the frame calling a signal handler ?*/
  if (frame->next && !frame->next->signal_handler_caller)
    return 0; /* Can only handle bad PC for these cases */

  /* Is it a Good PC ? */
  if (hppa_find_unwind_entry(frame->pc) 	/* HP C function usually */
    || (hppa_pc_in_gcc_function((CORE_ADDR) frame->pc))) /* In GCC code */
    return 0; /* Good PC */

  /* We can handle this bad PC */
  return 1;
}

/* Obtain the previous frame if the current frame has a bad PC */
void
do_handle_bad_pc(struct frame_info * frame, struct frame_info * prev)
{
  struct unwind_table_entry *u;

  /* While profiling, PC samples can fall on BOR code and such
     that don't have unwind entries. We don't want the warning
     messages printed for these. Silently handle if possible,
     if not, tough luck - Srikanth, Oct 10th 2005.
  */
  if (!profile_on && print_bad_pc_unwind_msg)
    warning ("Attempting to unwind past bad PC %s ",
                   longest_local_hex_string ((LONGEST) frame->pc));
  if (frame->next == 0)	/* Top frame */
    frame->frame = read_sp();	/* use value of SP */
  else 
    /* Signal Handler caller */
    /* Get SP saved in the save state of the signal handler caller*/
    if (read_relative_register_raw_bytes_for_frame(SP_REGNUM, 
      (char *) &frame->frame, frame)) 
	/* Couldn't find SP, use callee frame's frame pointer */
	frame->frame=frame->next->frame; 

    /* Initialize PC from rp value */
    prev->pc = FRAME_SAVED_PC (frame);

    if (u = hppa_find_unwind_entry(prev->pc))
      /* Calculate frame pointer using unwind entry */
      prev->frame = frame->frame - (u->Total_frame_size << 3);
    else if (hppa_pc_in_gcc_function((CORE_ADDR) prev->pc)) 
    {
      if (frame->next == 0) /* Top frame */
        prev->frame = TARGET_READ_FP ();
      else
	if (read_relative_register_raw_bytes_for_frame(FP_REGNUM, 
	  (char *) &prev->frame, frame))
	  /* Couldn't find FP, use callee frame's frame pointer */
	     frame->frame=frame->frame;
    }
    else
      prev->frame = frame->frame; /* Not a good PC either, use previous FP */

}

/* Java stack unwind for PA */
void 
do_java_stack_unwind_pa(struct frame_info * next_frame, 
		struct frame_info * prev) 
{
  struct unwind_table_entry *u;
  struct basic_frame_info bprev,bnext;

  /* Initialize basic frame being sent to junwindlib */
  bnext.pc = next_frame->pc;
  bnext.fp = 0;

  if (next_frame->next)
    bnext.sp = next_frame->next->frame;
  else
    bnext.sp = read_sp();

  /* Call Java Unwind library and update prev */
  if (get_prev_frame_info(&bnext,&bprev)) {
    if (bnext.sp==bprev.sp) /* frameless invocation. get saved pc */
      prev->pc = FRAME_SAVED_PC (next_frame);
    else
      prev->pc = bprev.pc;
  next_frame->frame = bprev.sp;	

  if (u=hppa_find_unwind_entry (prev->pc)) { /* Native frame */
    prev->frame = next_frame->frame - (u->Total_frame_size << 3);
    return;
  }

  if (bprev.fp) /* Update frame pointer */
    prev->frame = bprev.fp;
  }
}

/* Get function name (along with additional information) for Java frame */
char *
get_java_func_name_pa(struct frame_info * frame)
{
  struct basic_frame_info fi;
  char* fstr;

  fi.fp = 0;
  fi.pc = frame->pc;
  if (frame->next)
    fi.sp = frame->next->frame;
  else
    fi.sp = read_sp();

  /* Call Java Unwind library to get string */
  fstr = get_java_frame_str(&fi);
  if (fi.fp)
    frame->frame = fi.fp;

  return fstr;
}

/* Should call_function allocate stack space for a struct return?  */
int
hppa_use_struct_convention (gcc_p, type)
     int gcc_p;
     struct type *type;
{
  return (TYPE_LENGTH (type) > 2 * REGISTER_SIZE);
}


int
in_syscall (flags)
     CORE_ADDR flags;
{
  return ((flags & 0x2) && (!(flags & 0x20)));
}

/* RM: SS_INSYSCALL but not SS_DORFI */
#define IN_SYSCALL(flags) (in_syscall((flags)))

/* Routines to extract various sized constants out of hppa 
   instructions. */

/* This assumes that no garbage lies outside of the lower bits of 
   value. */

static int
sign_extend (val, bits)
     unsigned val, bits;
{
  return (int) (val >> (bits - 1) ? (-1 << bits) | val : val);
}

/* For many immediate values the sign bit is the low bit! */

static int
low_sign_extend (val, bits)
     unsigned val, bits;
{
  return (int) ((val & 0x1 ? (-1 << (bits - 1)) : 0) | val >> 1);
}

/* extract the immediate field from a ld{bhw}s instruction */

static int
extract_5_load (word)
     unsigned word;
{
  return low_sign_extend (word >> 16 & MASK_5, 5);
}

/* extract the immediate field from a break instruction */

static unsigned
extract_5r_store (word)
     unsigned word;
{
  return (word & MASK_5);
}

/* extract the immediate field from a {sr}sm instruction */

static unsigned
extract_5R_store (word)
     unsigned word;
{
  return (word >> 16 & MASK_5);
}

/* extract a 14 bit immediate field */

int
extract_14 (word)
     unsigned word;
{
  return low_sign_extend (word & MASK_14, 14);
}

/* deposit a 14 bit constant in a word */

unsigned
deposit_14 (opnd, word)
     int opnd;
     unsigned word;
{
  unsigned sign = (opnd < 0 ? 1 : 0);

  return word | ((unsigned) opnd << 1 & MASK_14) | sign;
}

/* extract a 21 bit constant */

static int
extract_21 (word)
     unsigned word;
{
  int val;

  word &= MASK_21;
  word <<= 11;
  val = GET_FIELD (word, 20, 20);
  val <<= 11;
  val |= GET_FIELD (word, 9, 19);
  val <<= 2;
  val |= GET_FIELD (word, 5, 6);
  val <<= 5;
  val |= GET_FIELD (word, 0, 4);
  val <<= 2;
  val |= GET_FIELD (word, 7, 8);
  return sign_extend (val, 21) << 11;
}

#ifdef COVARIANT_SUPPORT

#ifdef GDB_TARGET_IS_HPPA_20W

/* PA64 - Find out the pc of the Co-variant callee function */
static void 
find_pc_of_covariant_callee_func ( pc, stub_size, stub_kind_p,
				   start_stub_p, unwind_p, target_pc, start_addr_p )
CORE_ADDR pc;
int* stub_size;
stub_type* stub_kind_p;
int* start_stub_p; 
struct unwind_table_entry* unwind_p;
CORE_ADDR* target_pc;
CORE_ADDR* start_addr_p;
{

       /*      Co-variant return type(64bit)
	    If the pc is in the "call" trampoline code of 
	    outbound/inbound_outbound thunk, get on
            to the branch instruction and find the
            next instruction to be executed which 
	    is nothing but the instruction in Co-variant
            callee function.
	    STD     %r2,-16(%r30)   ;offset 0x3a0
            STD,MA  %r27,32(%r30)   ;offset 0x3a4
            --------------------------------------
	   		 case (i)
	    thisadj is less than 16 bits 
            B,L     goo__2TEFv,%r2  ;offset 0x3a8

                        case (ii)
            thisadj is greater than 16 bits
            ADDIL    L'OFFSET,%r26,%r1
            B,L      DESTINATION
            LDO      OFFSET(%r1),%r26
            ------------------------------------- 
        */ 
              
       if (     unwind_p != 0
             && (start_stub_p[0] & 0xffff0000) == 0x73c20000
             && (start_stub_p[1] & 0xffff0000) == 0x73db0000 )
        {
           if ( (start_stub_p[2] & 0xffe00000) == 0xe8400000 
               /*  &&  (start_stub_p[3] & 0xff000000) == 0x37000000 */)
             { 
               /* If sub-opcode of the branch instruction (16-18 bits) is
	          5, extract 22 bits otherwise just extract 17 bits */
               if ( GET_FIELD (start_stub_p[2], 16, 18) != 5 ) 
                 {
                    *target_pc = (*start_addr_p + 8 + 8 + extract_17 (start_stub_p[2])) 
                    & ~0x3;
                 } else {
                    *target_pc = (*start_addr_p + 8 + 8 + extract_22 (start_stub_p[2])) 
                    & ~0x3;
                 }
                 *stub_size = 4 * INSTRUCTION_SIZE;
              } else if ( ( start_stub_p[2] & 0xfe000000) == 0x2a000000
                          && (start_stub_p[3] & 0xfc00e002) == 0xe8000000
                          && (start_stub_p[4] & 0xffff0000 ) == 0x343a0000)
                {
                  if ( GET_FIELD (start_stub_p[3], 16, 18) != 5 ) 
                    {
                      *target_pc = (*start_addr_p + 8 + 12 + extract_17 (start_stub_p[3])) 
                      & ~0x3;
                    } else {
                      *target_pc = (*start_addr_p + 8 + 12 + extract_22 (start_stub_p[3])) 
                      & ~0x3;
                    }
                    *stub_size = 5 * INSTRUCTION_SIZE;
                }
          *stub_kind_p = STUB_CXX_ADJUST_COVARIANT_RETURN;
        }
}

/* PA64 - Find out the pc of the Co-variant caller function */
static void 
find_pc_of_covariant_caller_func ( pc, stub_size, stub_kind_p,
				   start_stub_p, unwind_p, target_pc, start_addr_p )
CORE_ADDR pc;
int stub_size;
stub_type* stub_kind_p;
int* start_stub_p; 
struct unwind_table_entry* unwind_p;
CORE_ADDR* target_pc;
CORE_ADDR* start_addr_p;
{

           /*		Co-variant return type(64bit)
		If the pc is in the "return" trampoline of
	      the Outbound/Inbound_outbound thunks, get the
              saved pc from the previous/current frame.
              In other words, we calculate the target_pc/rp from
              the current_frame, if the current frame's pc is in thunk.
              And we calculate target_pc/rp from the previous_frame,
              if the previous frame is thunk. Later case is added 
              as a special case for "return <expr>" implementation
              to support Co-variant returns 

              case (i)   In multiple inheritance case

              CMPB,=,N        %r0,%r28,$L3    ;offset 0x3b0
              ---------------------------------------------
		      		case (a)
	      retadj const is <16 bits
              LDO     8(%r28),%r28    ;offset 0x3b4

				case (b)
	      retadj const is >16 bits
	      ADDIL L'retadj,%r28,%r1
	      LDO R'retadj(%r1),%r28
	     ---------------------------------------------
              LDD     -48(%r30),%r2   ;offset 0x3b8
              BVE     (%r2)   ;offset 0x3bc 

                             or 

             case (ii)   If virtual co-variant
             CMPB,=,N        %r0,%r28,$L3    ;offset 0x440
	     ----------------------------------------------
		             	case (a)
	     retadj <= 16 bits
             LDD     0(%r28),%r1     ;offset 0x444
             LDD     -16(%r1),%r1    ;offset 0x448
             ADD     %r1,%r28,%r28   ;offset 0x44c

				case (b)
	     retadj > 16 bits
	     LDD      (%ret0), %r1
	     ADDIL    L'retadj,%r1,%r1
	     LDD      R'retadj(%r1), %ret0
	     ADD      %r1,%ret0,%ret0	   
	     -----------------------------------------------
             LDD     -48(%r30),%r2   ;offset 0x450
             BVE     (%r2)   ;offset 0x454 

            */
        if (   unwind_p != 0
		/* Multiple inheritance case*/
                && ( ( (start_stub_p[0] & 0xffff0000)     == 0x83800000
                        && (start_stub_p[1] & 0xffff0000) == 0x379c0000
                        && (start_stub_p[2] & 0xffff0000) == 0x53c20000
                        && (start_stub_p[3] & 0xffffffff) == 0xe840d000)
		     ||
		     ( (start_stub_p[0] & 0xffff0000)     == 0x83800000
                        && (start_stub_p[1] & 0xfe000000) == 0x2a000000
			&& (start_stub_p[2] & 0xffff0000) == 0x343c0000
                        && (start_stub_p[3] & 0xffff0000) == 0x53c20000
                        && (start_stub_p[4] & 0xffffffff) == 0xe840d000)
                     || 
		 /* Virtual Co-variant case */
                        ( (start_stub_p[0] & 0xffff0000)  ==  0x83800000
                        && (start_stub_p[1] & 0xffff0000) == 0x53810000
                        && (start_stub_p[2] & 0xffff0000) == 0x50210000
                        && (start_stub_p[3] & 0xffff000)  == 0xb810000
                        && (start_stub_p[4] & 0xffff0000) == 0x53c20000
                        && (start_stub_p[5] & 0xffffffff) == 0xe840d000)
                     || 
                        ( (start_stub_p[0] & 0xffff0000)  == 0x83800000
                        && (start_stub_p[1] & 0xffff0000) == 0x53810000
                        && (start_stub_p[2] & 0xff000000) == 0x28000000
                        && (start_stub_p[3] & 0xffff0000) == 0x503c0000
                        && (start_stub_p[4] & 0xffff000)  ==  0xb810000
                        && (start_stub_p[5] & 0xffff0000) == 0x53c20000
                        && (start_stub_p[6] & 0xffffffff) == 0xe840d000)))  
        {
          unwind_p = hppa_find_unwind_entry (pc);
          *target_pc = FRAME_SAVED_PC (get_current_frame ());
          if( IS_PC_IN_COVARIANT_THUNK (*target_pc)) 
            {
              *target_pc = FRAME_SAVED_PC (get_prev_frame (get_current_frame ()));
            }
          stub_size = 4 * INSTRUCTION_SIZE;
          *stub_kind_p = STUB_CXX_ADJUST_COVARIANT_RETURN;
        }
}

#endif /* This funcion is only for PA wide mode */

#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W

/* PA32 : Find the pc of the Co-variant caller  function */
int 
find_pc_of_covariant_caller_func ( pc, u ) 
CORE_ADDR* pc;
struct unwind_table_entry* u;
{
          long first_stub_inst, second_stub_inst, third_stub_inst,
               fourth_stub_inst, fifth_stub_inst,sixth_stub_inst,
	       seventh_stub_inst,eigth_stub_inst,ninth_stub_inst;
          int return_val = 0;

          first_stub_inst   = read_memory_integer (*pc,4);
          second_stub_inst  = read_memory_integer (*pc+4,4);
          third_stub_inst   = read_memory_integer (*pc+8,4);
          fourth_stub_inst  = read_memory_integer (*pc+12,4);
          fifth_stub_inst   = read_memory_integer (*pc+16,4);
          sixth_stub_inst   = read_memory_integer (*pc+20,4);
          seventh_stub_inst = read_memory_integer (*pc+24,4);
          eigth_stub_inst   = read_memory_integer (*pc+28,4);
          ninth_stub_inst   = read_memory_integer (*pc+32,4);
           /*		Co-variant return type(32bit)
		If the pc is in the return trampoline of
	      the Outbound/Inbound_outbound thunks, get the
              saved pc from the previous/current frame
              In other words, we calculate the target_pc/rp from
              the current_frame, if the current frame's pc is in thunk.
              And we calculate target_pc/rp from the previous_frame,
              if the previous frame is thunk. Later case is added 
              as a special case for "return <expr>" implementation
              to support Co-variant returns 
               case (i)      Multiple inheritance case
               CMPB,=,N        %r0,%r28,$L4    ;offset 0x20c
	       ---------------------------------------------------
				case (a)
	       retadj<= 14 bits
               LDO     4(%r28),%r28    ;offset 0x210

				case (b)
	       retadj>=14 bits
	       LDIL    L' retadj,%r28
               LDO     R'retadj(%r1),%r28
               ADD     %r1,%r28,%r28
	       ---------------------------------------------------
               LDW     -84(%r30),%r2   ;offset 0x214
               BV      %r0(%r2)        ;offset 0x218

		             OR

	       case (ii)    If virtual Co-variant

               CMPB,=,N        %r0,%r28,$L4    ;offset 0x2a8
	       ---------------------------------------------------
				case (a)
	       retadj<=14 bits
               LDW     0(%r28),%r1     ;offset 0x2ac
               LDSID   (%r1),%r31      ;offset 0x2b0
               MTSP    %r31,%sr2       ;offset 0x2b4
               LDW     -8(%sr2,%r1),%r1        ;offset 0x2b8
               ADD     %r1,%r28,%r28   ;offset 0x2bc

				case (b)
	       retadj>=14 bits
               LDW     0(%r28),%r1     ;offset 0x2ac
               LDSID   (%r1),%r31      ;offset 0x2b0
               MTSP    %r31,%sr2       ;offset 0x2b4
               ADDIL   L'retadj,%r1,%r1
	       LDW     R'retadj(%sr2,%r1),%r1
               ADD     %r1,%r28,%r28   ;offset 0x2bc
               --------------------------------------------------

               LDW     -84(%r30),%r2   ;offset 0x2c0
               BV      %r0(%r2)        ;offset 0x2c4 

				OR

               case (iii) For share libs

              ldw -0x20(%sp),%r19
              cmpb,=,n %r0,%ret0,0x7f4f70a8
              ldo 4(%ret0),%ret0
              ldw -0x54(%sp),%rp
              BVE (%r2)

	                          OR

               case (iv) Virtual Co-variant with Sharelib

             LDW     -32(%r30),%r19  ;offset 0x11c
             CMPB,=,N        %r0,%r28,$L3    ;offset 0x120
             LDW     0(%r28),%r1     ;offset 0x124
             LDSID   (%r1),%r31      ;offset 0x128
             MTSP    %r31,%sr2       ;offset 0x12c
             LDW     -8(%sr2,%r1),%r1        ;offset 0x130
             ADD     %r1,%r28,%r28   ;offset 0x134
             LDW     -84(%r30),%r2   ;offset 0x138
             BVE     (%r2)   ;offset 0x13c



            */

           if (   /* Mulitple inheritance case */
		     ((( first_stub_inst    & 0xffff0000) == 0x83800000)
                     &&((second_stub_inst   & 0xffff0000) == 0x379c0000)
                     &&((third_stub_inst    & 0xffff0000) == 0x4bc20000)
                     &&((fourth_stub_inst   & 0xff000000) == 0xe8000000))
                   ||
		     ((( first_stub_inst    & 0xffff0000) == 0x83800000)
                     &&((second_stub_inst   & 0xff000000) == 0x23000000)
                     &&((third_stub_inst    & 0xffff0000) == 0x343c0000)
                     &&((fourth_stub_inst   & 0xfff0000)  == 0xb810000)
                     &&((fifth_stub_inst    & 0xffff0000) == 0x4bc20000)
                     &&((sixth_stub_inst    & 0xffff0000) == 0xe8400000))
	          /* Virtual covariant case */	 
	         || ((first_stub_inst       & 0xffff0000) == 0x83800000
                      && (second_stub_inst  & 0xffff0000) == 0x4b810000
                      && (third_stub_inst   & 0xe00000) == 0x200000
                      && (fourth_stub_inst  & 0xff0000) == 0x1f0000
		      && (fifth_stub_inst   & 0xffff0000) == 0x48210000
		      && (sixth_stub_inst   & 0xfff0000) == 0xb810000
                      && (seventh_stub_inst & 0xffff0000) == 0x4bc20000
                      && (eigth_stub_inst   & 0xffff0000) == 0xe8400000)
	         || ((first_stub_inst       & 0xffff0000) == 0x83800000
                      && (second_stub_inst  & 0xffff0000) == 0x4b810000
                      && (third_stub_inst   & 0xe00000) == 0x200000
                      && (fourth_stub_inst  & 0xff0000) == 0x1f0000
                      && (fifth_stub_inst   & 0xff000000) == 0x28000000
		      && (sixth_stub_inst   & 0xffff0000) == 0x48210000
		      && (seventh_stub_inst & 0xfff0000) == 0xb810000
                      && (eigth_stub_inst & 0xffff0000) == 0x4bc20000
                      && (ninth_stub_inst   & 0xffff0000) == 0xe8400000)
	         /* For shared libs */
                 || ((first_stub_inst       &  0xffff0000) == 0x4bd30000
                     &&(second_stub_inst    & 0xffff0000) == 0x83800000
                     &&((third_stub_inst    & 0xffff0000) == 0x379c0000)
                     &&((fourth_stub_inst   & 0xffff0000) == 0x4bc20000)
                     &&((fifth_stub_inst    & 0xff000000) == 0xe8000000))
	         ||    ((first_stub_inst    & 0xfff00000) == 0x4bd00000
		      && (second_stub_inst  & 0xffff0000) == 0x83800000
                      && (third_stub_inst   & 0xffff0000) == 0x4b810000
                      && (fourth_stub_inst  & 0xe00000)   == 0x200000
                      && (fifth_stub_inst   & 0xff0000)   == 0x1f0000
		      && (sixth_stub_inst   & 0xffff0000) == 0x48210000
		      && (seventh_stub_inst & 0xfff0000)  == 0xb810000
                      && (eigth_stub_inst   & 0xffff0000) == 0x4bc20000
                      && (ninth_stub_inst   & 0xffff0000) == 0xe8400000))	
             {
                  u = hppa_find_unwind_entry (*pc);
                  *pc = FRAME_SAVED_PC (get_current_frame ());
                  if (IS_PC_IN_COVARIANT_THUNK (*pc)) 
                    {
                      *pc = FRAME_SAVED_PC( get_prev_frame (get_current_frame ()));
                    }
		  return_val = 1;
             }
       return return_val;
}


/* PA32 : Find the pc of the Co-variant callee  function */
int 
find_pc_of_covariant_callee_func ( pc, u ) 
CORE_ADDR* pc;
struct unwind_table_entry* u;
{
          long first_stub_inst, second_stub_inst, third_stub_inst,
               fourth_stub_inst, fifth_stub_inst,sixth_stub_inst,
	       seventh_stub_inst,eigth_stub_inst,ninth_stub_inst,
               tenth_stub_inst,eleventh_stub_inst,twelfth_stub_inst,
               check_inst, thirteenth_stub_inst,fourteenth_stub_inst;
          int return_val = 0;
          
          first_stub_inst   = read_memory_integer (*pc,4);
          second_stub_inst  = read_memory_integer (*pc+4,4);
          third_stub_inst   = read_memory_integer (*pc+8,4);
          fourth_stub_inst  = read_memory_integer (*pc+12,4);
          fifth_stub_inst   = read_memory_integer (*pc+16,4);
          sixth_stub_inst   = read_memory_integer (*pc+20,4);
          seventh_stub_inst = read_memory_integer (*pc+24,4);
          eigth_stub_inst   = read_memory_integer (*pc+28,4);
          ninth_stub_inst   = read_memory_integer (*pc+32,4);
          tenth_stub_inst   = read_memory_integer (*pc+36,4);
          eleventh_stub_inst= read_memory_integer (*pc+40,4);
          twelfth_stub_inst = read_memory_integer (*pc+44,4);
          thirteenth_stub_inst = read_memory_integer (*pc+48,4);
          fourteenth_stub_inst = read_memory_integer (*pc+52,4);

            /* 		Co-variant return type(32 bit)
		If the pc is in the "call" trampoline code of 
	       outbound/inbound_outbound thunk, get on
               to the branch instruction and find the
               next instruction to be executed which 
	       is nothing but the instruction in Co-variant
               callee function
               case (1)   Non-PIC code
               STW     %r2,-20(%r30)   ;offset 0x1e4
	       LDO     64(%r30),%r30   ;offset 0x1e8
       	       LDO     -116(%r30),%r1  ;offset 0x1ec
               LDO     -52(%r30),%r2   ;offset 0x1f0
               LDO     -48(%r30),%r19  ;offset 0x1f4
               LDW,MA  -4(%r1),%r28    ;offset 0x1f8
               CMPB,>=,N       %r2,%r19,$L3    ;offset 0x1fc
               STW,MA  %r28,-4(%r2)    ;offset 0x200
	       -------------------------------------------------
                             case (a)
               Short call && thisadj is less than 14 bits
               B,L     goo__2TEFv,%r2  ;offset 0x204

			     case (b)
               Short call and thisadj is greater than 14 bits
               ADDIL    L'thiadj,%r26,%r1
               B,L     goo__2TEFv,%r2  ;offset 0x204

                             case (c) ------>Unteststed
               Just long call with no thisadj (For outbound thunk)
               LDIL    L'callee,%r1
               BE,L    R'callee(%sr4,%r1)

			     case (d)  ----->Untested
               long call with thisadj < 14bits 
               LDO     thisadj(%arg0),%arg0
               LDIL    L'callee,%r1
               BE,L    R'callee(%sr4,%r1)

                             case (e) ----->Untested
               long call with thisadj > 14bits
               ADDIL    L'thisadj,%arg0,%r1
               LDO      R'thisadj(%r1),%arg0
               LDIL    L'callee,%r1
               BE,L    R'callee(%sr4,%r1)
               
	       ---------------------------------------------------

              case (ii) PIC code
		    If the outbound/inboundoutbound thunk is
	      followed by sharelib stub, the trampoline code (PIC code)
              will be:
              STW     %r2,-20(%r30)   ;offset 0xf4
              LDO     64(%r30),%r30   ;offset 0xf8
              STW     %r19,-32(%r30)  ;offset 0xfc
              LDO     -116(%r30),%r1  ;offset 0x100
              LDO     -52(%r30),%r2   ;offset 0x104
              LDO     -48(%r30),%r21  ;offset 0x108
              LDW,MA  -4(%r1),%r28    ;offset 0x10c
              CMPB,>=,N       %r2,%r21,$L0    ;offset 0x110
              STW,MA  %r28,-4(%r2)    ;offset 0x114
              -----------------------------------------------------
	      case a) thisadj const is < 14 bits
              LDO     -4(%r26),%r26   ;offset 0x118

              case b) thisadj const is > 14 bits
              ADDIL L'thisadj , %arg0,%r1
              LDO R'thisadj(%r1), %arg0
            
              case c) no thisadj const (For outbound thunk)
              -----------------------------------------------------
              B,L     .+8,%r2 ;offset 0x11c
              ADDIL   L'goo__2TEFv-$L1+4,%r2,%r1      ;offset 0x120
              LDO     R'goo__2TEFv-$L2+8(%r1),%r1     ;offset 0x124
              LDSID   (%r1),%r31      ;offset 0x128
              MTSP    %r31,%sr0       ;offset 0x12c
              BE,L    0(%sr0,%r1),%r31        ;offset 0x130
	       
            */ 

           if (   ((first_stub_inst   & 0xffff0000) == 0x6bc20000)
                &&((second_stub_inst  & 0xffff0000) == 0x37de0000)
                &&((third_stub_inst   & 0xffff0000) == 0x37c10000)
                &&((fourth_stub_inst  & 0xffff0000) == 0x37c20000)
                &&((fifth_stub_inst   & 0xfff00000) == 0x37d00000)
                &&((sixth_stub_inst   & 0xff00000)  == 0xc300000)
                &&((seventh_stub_inst & 0xff0f0000) == 0x8a020000)
                &&((eigth_stub_inst   & 0xff00000)  == 0xc500000))
	     {
                if ((ninth_stub_inst   & 0xffe00000) == 0xe8400000) 
                 {
		   /* Case (a) */
                   *pc = (*pc + 32 + 8 + extract_17 (ninth_stub_inst)) & ~0x3;
                   return_val = 1;
                 }else if (    (ninth_stub_inst & 0xff000000) == 0x2b000000 
			    && (tenth_stub_inst & 0xffe00000) == 0xe8400000 ) 
                 {
		   /* Case (b) */
                   *pc = (*pc + 36 + 8 + extract_17 (tenth_stub_inst)) & ~0x3;
                   return_val = 1;
                 } else if (     (ninth_stub_inst & 0xff000000) == 0x20000000
			      && (tenth_stub_inst & 0xff000000) == 0xe4000000 ) 
                 {
		   /* Case (c)  Branch at tenth instn*/	
		   *pc = (extract_21(ninth_stub_inst)+
		          extract_17(tenth_stub_inst))&~0x3;
                 } else if (   (ninth_stub_inst    & 0xffff0000) == 0x375a0000
			    && (tenth_stub_inst    & 0xff000000) == 0x20000000
	                    && (eleventh_stub_inst & 0xff000000) == 0xe4000000 ) 
                 {
		  /* Case (d)  Branch at eleventh instn */
		   *pc = (extract_21(tenth_stub_inst)+
			    extract_17(eleventh_stub_inst))&~0x3;
                   return_val = 1;
                 } else if (   (ninth_stub_inst    & 0xff000000) == 0x2b000000
			    && (tenth_stub_inst    & 0xffff0000) == 0x343a0000
			    && (eleventh_stub_inst & 0xff000000) == 0x20000000
			    && (twelfth_stub_inst  & 0xff000000) == 0xe4000000 ) 
                 {
		  /* Case (e)  Branch at twelfth instn */
		   *pc = (extract_21(eleventh_stub_inst)+
			    extract_17(twelfth_stub_inst))&~0x3;
                   return_val = 1;
                 }  
             } else if (   ((first_stub_inst    & 0xffff0000) == 0x6bc20000)
                          &&((second_stub_inst  & 0xffff0000) == 0x37de0000)
                          &&((third_stub_inst   & 0xffff0000) == 0x6bd30000)
                          &&((fourth_stub_inst  & 0xffff0000) == 0x37c10000)
                          &&((fifth_stub_inst   & 0xffff0000) == 0x37c20000)
                          &&((sixth_stub_inst   & 0xffff0000) == 0x37d50000)
                          &&((seventh_stub_inst & 0xff00000)  == 0xc300000)
                          &&((eigth_stub_inst   & 0xffff0000) == 0x8aa20000)
                          &&((ninth_stub_inst   & 0xff00000)  == 0xc500000)) 
                 {
                      /*  Case (ii) - For PIC code */
                      check_inst = read_memory_integer (*pc+60,4);
                      if (   (tenth_stub_inst    & 0xffff0000) == 0x375a0000
                          && (eleventh_stub_inst & 0xff000000) == 0xe8000000
                          && (twelfth_stub_inst  & 0xff000000) == 0x28000000
			  && (check_inst         & 0xff000000) == 0xe4000000 )
                        {
 			  /* Case (a) - Matching only first few instns 
			     and the branch instn in all the cases.
                             Branch at 16th instn .Find out the contents of $r1
			     which is the pc of callee. Remember that you
			     are at the first instruction of the thunk and
			     haven't executed the instructions till the branch.
			     The contents of $r1 gets updated by the intermediate
			     insns,so we need to find out the contents of $r1 
			     manually at the time of branch insn. 
			     Same with Case (b) and (c) */
                          *pc = (  *pc + 52 - 1 + extract_21 (twelfth_stub_inst) +
			           extract_14 (thirteenth_stub_inst)) & ~0x3;
                           return_val = 1;
                        }else if ((tenth_stub_inst    & 0xff000000) == 0x2b000000
			        &&(eleventh_stub_inst & 0xffff0000) == 0x343a0000
                                &&(twelfth_stub_inst  & 0xff000000) == 0xe8000000)
		        {
                           check_inst = read_memory_integer (*pc+64,4);
                           if ((check_inst & 0xff000000) == 0xe4000000)
 			     {
				/* Case (b) - Branch at 17th instn */
                                *pc = (*pc + 56 - 1+
				      +extract_21 (thirteenth_stub_inst)
				      +extract_14 (fourteenth_stub_inst)) &~0x3;
                               
                                return_val = 1;
                             }
                        }else if ((tenth_stub_inst     & 0xff000000) == 0xe8000000
                                 &&(eleventh_stub_inst & 0xff000000) == 0x28000000)
                        {
                           check_inst = read_memory_integer (*pc+56,4);
			   if ((check_inst & 0xff000000) == 0xe4000000)
			     {
				/* Case (c) - Branch at 15th instn */
                               *pc = ( *pc + 48 - 1+
				      extract_21(eleventh_stub_inst) +
			              extract_14(twelfth_stub_inst)) &~0x3;
                                return_val = 1;
			     }
			}
		           
                  }
            return return_val;
}

#endif
#endif  /* PA 32 bit support for Co-variant return type */

static int
extract_22 (word)
     unsigned word;
{
  return sign_extend (GET_FIELD (word, 19, 28) |
                      GET_FIELD (word, 29, 29) << 10 |
                      GET_FIELD (word, 11, 15) << 11 |
                      GET_FIELD (word, 6, 10) << 16 |
                      (word & 0x1) << 21, 22) << 2;
}

#endif

/* deposit a 21 bit constant in a word. Although 21 bit constants are
   usually the top 21 bits of a 32 bit constant, we assume that only
   the low 21 bits of opnd are relevant */

unsigned
deposit_21 (opnd, word)
     unsigned opnd, word;
{
  unsigned val = 0;

  val |= GET_FIELD (opnd, 11 + 14, 11 + 18);
  val <<= 2;
  val |= GET_FIELD (opnd, 11 + 12, 11 + 13);
  val <<= 2;
  val |= GET_FIELD (opnd, 11 + 19, 11 + 20);
  val <<= 11;
  val |= GET_FIELD (opnd, 11 + 1, 11 + 11);
  val <<= 1;
  val |= GET_FIELD (opnd, 11 + 0, 11 + 0);
  return word | val;
}

/* extract a 17 bit constant from branch instructions, returning the
   19 bit signed value. */

static int
extract_17 (word)
     unsigned word;
{
  return sign_extend (GET_FIELD (word, 19, 28) |
		      GET_FIELD (word, 29, 29) << 10 |
		      GET_FIELD (word, 11, 15) << 11 |
		      (word & 0x1) << 16, 17) << 2;
}

/* Deposit a 17 bit constant in an instruction (like bl). */

unsigned int
deposit_17 (opnd, word)
     unsigned opnd, word;
{
  word |= GET_FIELD (opnd, 15 + 0, 15 + 0); /* w */
  word |= GET_FIELD (opnd, 15 + 1, 15 + 5) << 16; /* w1 */
  word |= GET_FIELD (opnd, 15 + 6, 15 + 6) << 2; /* w2[10] */
  word |= GET_FIELD (opnd, 15 + 7, 15 + 16) << 3; /* w2[0..9] */

  return word;
}

/* Extract an im10a immediate from an instruction like LDD 
   im10a defines 14 bits, the low three bits are zero, plus the sign
   bit makes 14.
*/

int
extract_im10a (inst)
     unsigned inst;
{
  int im10a;
  int sign_bit;
  int s;

/* RM: Different for PA32 and PA64 */
#ifdef GDB_TARGET_IS_HPPA_20W
  sign_bit = inst & 1;
  im10a = (inst >> 4) & 0x03ff;
  im10a <<= 3;
  s = (inst & 0x0000c000) >> 14;
  if (sign_bit)
    {
      im10a |= (~s) << 13;
    }
  else
    {
      im10a |= s << 13;
    }
#else
  sign_bit = inst & 1;
  im10a = (inst >> 4) & 0x03ff;
  im10a <<= 3;
  if (sign_bit)
    im10a |= 0xffffe000;
#endif  
  return im10a;
} /* end extract_ima10a */


/* Compare the start address for two unwind entries returning 1 if 
   the first address is larger than the second, -1 if the second is
   larger than the first, and zero if they are equal.  */

static int
compare_unwind_entries (arg1, arg2)
     const void *arg1;
     const void *arg2;
{
  const struct unwind_table_entry *a = arg1;
  const struct unwind_table_entry *b = arg2;

  if (a->region_start > b->region_start)
    return 1;
  else if (a->region_start < b->region_start)
    return -1;
  else
    return 0;
}

static void
internalize_unwinds (objfile, table, section, entries, size, text_offset)
     struct objfile *objfile;
     struct unwind_table_entry *table;
     asection *section;
     unsigned int entries, size;
     CORE_ADDR text_offset;
{
  /* We will read the unwind entries into temporary memory, then
     fill in the actual unwind table.  */
  if (size > 0)
    {
      unsigned long tmp;
      unsigned i;
      char *buf = (char *) alloca (size);

      bfd_get_section_contents (objfile->obfd, section, buf, 0, size);

      /* Now internalize the information being careful to handle host/target
         endian issues.  */
      for (i = 0; i < entries; i++)
	{
	  table[i].region_start = bfd_get_32 (objfile->obfd,
					      (bfd_byte *) buf);
	  table[i].region_start += text_offset;
	  buf += 4;
	  table[i].region_end = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	  table[i].region_end += text_offset;
	  buf += 4;
	  tmp = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	  buf += 4;
	  table[i].Cannot_unwind = (tmp >> 31) & 0x1;
	  table[i].Millicode = (tmp >> 30) & 0x1;
#ifndef GDB_TARGET_IS_HPPA_20W
	  table[i].Millicode_save_sr0 = (tmp >> 29) & 0x1;
#endif
	  table[i].Region_description = (tmp >> 27) & 0x3;
	  table[i].reserved1 = (tmp >> 26) & 0x1;
	  table[i].Entry_SR = (tmp >> 25) & 0x1;
	  table[i].Entry_FR = (tmp >> 21) & 0xf;
	  table[i].Entry_GR = (tmp >> 16) & 0x1f;
	  table[i].Args_stored = (tmp >> 15) & 0x1;
#ifdef GDB_TARGET_IS_HPPA_20W
          table[i].reserved2 = (tmp >> 12) & 0x7;
#else
	  table[i].Variable_Frame = (tmp >> 14) & 0x1;
	  table[i].Separate_Package_Body = (tmp >> 13) & 0x1;
	  table[i].Frame_Extension_Millicode = (tmp >> 12) & 0x1;
#endif
	  table[i].Stack_Overflow_Check = (tmp >> 11) & 0x1;
	  table[i].Two_Instruction_SP_Increment = (tmp >> 10) & 0x1;
#ifdef GDB_TARGET_IS_HPPA_20W
          table[i].reserved3 = (tmp >> 9) & 0x1;
#else
	  table[i].Ada_Region = (tmp >> 9) & 0x1;
#endif
	  table[i].cxx_info = (tmp >> 8) & 0x1;
	  table[i].cxx_try_catch = (tmp >> 7) & 0x1;
	  table[i].sched_entry_seq = (tmp >> 6) & 0x1;
#ifdef GDB_TARGET_IS_HPPA_20W
          table[i].reserved4 = (tmp >> 5) & 0x1;
#else
	  table[i].reserved2 = (tmp >> 5) & 0x1;
#endif
	  table[i].Save_SP = (tmp >> 4) & 0x1;
	  table[i].Save_RP = (tmp >> 3) & 0x1;
	  table[i].Save_MRP_in_frame = (tmp >> 2) & 0x1;
#ifdef GDB_TARGET_IS_HPPA_20W
          table[i].reserved5 = (tmp >> 1) & 0x1;
#else
	  table[i].extn_ptr_defined = (tmp >> 1) & 0x1;
#endif
	  table[i].Cleanup_defined = tmp & 0x1;
	  tmp = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	  buf += 4;
#ifdef GDB_TARGET_IS_HPPA_20W
          table[i].reserved6 = (tmp >> 31) & 0x1;
#else
	  table[i].MPE_XL_interrupt_marker = (tmp >> 31) & 0x1;
#endif
	  table[i].HP_UX_interrupt_marker = (tmp >> 30) & 0x1;
	  table[i].Large_frame = (tmp >> 29) & 0x1;
#ifdef GDB_TARGET_IS_HPPA_20W
          table[i].alloca_frame = (tmp >> 28) & 0x1;
          table[i].reserved7 = (tmp >> 27) & 0x1;
#else
	  table[i].Pseudo_SP_Set = (tmp >> 28) & 0x1;
	  table[i].reserved4 = (tmp >> 27) & 0x1;
#endif
	  table[i].Total_frame_size = tmp & 0x7ffffff;

	  /* Stub unwinds are handled elsewhere. */
#ifndef GDB_TARGET_IS_HPPA_20W
	  table[i].stub_unwind.stub_type = 0;
	  table[i].stub_unwind.padding = 0;
#endif
	}
    }
}

/* Read in the backtrace information stored in the `$UNWIND_START$' section of
   the object file.  This info is used mainly by hppa_find_unwind_entry() to find
   out the stack frame size and frame pointer used by procedures.  We put
   everything on the psymbol obstack in the objfile so that it automatically
   gets freed when the objfile is destroyed.  */

static void
read_unwind_info (objfile)
     struct objfile *objfile;
{
  asection *unwind_sec, *stub_unwind_sec;
  unsigned unwind_size, stub_unwind_size, total_size;
  unsigned index, unwind_entries;
  unsigned stub_entries, total_entries;
  CORE_ADDR text_offset;
  struct obj_unwind_info *ui;
  obj_private_data_t *obj_private;

#ifdef GDB_TARGET_IS_HPPA_20W
  text_offset = elf_text_start_vma(objfile->obfd)
		+ ANOFFSET (objfile->section_offsets, 0);
#else
  text_offset = ANOFFSET (objfile->section_offsets, 0);
#endif

  ui = (struct obj_unwind_info *) obstack_alloc (&objfile->psymbol_obstack,
					   sizeof (struct obj_unwind_info));

  ui->table = NULL;
  ui->cache = NULL;
  ui->last = -1;

  /* For reasons unknown the HP PA64 tools generate multiple unwinder
     sections in a single executable.  So we just iterate over every
     section in the BFD looking for unwinder sections intead of trying
     to do a lookup with bfd_get_section_by_name. 

     First determine the total size of the unwind tables so that we
     can allocate memory in a nice big hunk.  */
  total_entries = 0;

  for (unwind_sec = objfile->obfd->sections;
       unwind_sec;
       unwind_sec = unwind_sec->next)
    {
      if (strcmp (unwind_sec->name, "$UNWIND_START$") == 0
	  || strcmp (unwind_sec->name, ".PARISC.unwind") == 0)
	{

	  unwind_size = bfd_section_size (objfile->obfd, unwind_sec);
	  unwind_entries = unwind_size / UNWIND_ENTRY_SIZE;

	  total_entries += unwind_entries;
	}
    }

  /* Now compute the size of the stub unwinds.  Note the ELF tools do not
     use stub unwinds at the current time.  */
  stub_unwind_sec = bfd_get_section_by_name (objfile->obfd, "$UNWIND_END$");

  if (stub_unwind_sec)
    {
      stub_unwind_size = bfd_section_size (objfile->obfd, stub_unwind_sec);
      stub_entries = stub_unwind_size / STUB_UNWIND_ENTRY_SIZE;
    }
  else
    {
      stub_unwind_size = 0;
      stub_entries = 0;
    }

  /* Compute total number of unwind entries and their total size.  */
  total_entries += stub_entries;
  total_size = total_entries * sizeof (struct unwind_table_entry);

  /* Allocate memory for the unwind table.  */
  ui->table = (struct unwind_table_entry *)
    obstack_alloc (&objfile->psymbol_obstack, total_size);
  ui->last = total_entries - 1;

  /* Now read in each unwind section and internalize the standard unwind
     entries.  */
  index = 0;
  for (unwind_sec = objfile->obfd->sections;
       unwind_sec;
       unwind_sec = unwind_sec->next)
    {
      if (strcmp (unwind_sec->name, "$UNWIND_START$") == 0
	  || strcmp (unwind_sec->name, ".PARISC.unwind") == 0)
	{
	  unwind_size = bfd_section_size (objfile->obfd, unwind_sec);
	  unwind_entries = unwind_size / UNWIND_ENTRY_SIZE;

	  internalize_unwinds (objfile, &ui->table[index], unwind_sec,
			       unwind_entries, unwind_size, text_offset);
	  index += unwind_entries;
	}
    }

  /* Now read in and internalize the stub unwind entries.  */
  if (stub_unwind_size > 0)
    {
      unsigned int i;
      char *buf = (char *) alloca (stub_unwind_size);

      /* Read in the stub unwind entries.  */
      bfd_get_section_contents (objfile->obfd, stub_unwind_sec, buf,
				0, stub_unwind_size);

      /* Now convert them into regular unwind entries.  */
      for (i = 0; i < stub_entries; i++, index++)
	{
	  /* Clear out the next unwind entry.  */
	  memset (&ui->table[index], 0, sizeof (struct unwind_table_entry));

	  /* Convert offset & size into region_start and region_end.  
	     Stuff away the stub type into "reserved" fields.  */
	  ui->table[index].region_start = bfd_get_32 (objfile->obfd,
						      (bfd_byte *) buf);
	  ui->table[index].region_start += text_offset;
	  buf += 4;
#ifndef GDB_TARGET_IS_HPPA_20W
	  ui->table[index].stub_unwind.stub_type = bfd_get_8 (objfile->obfd,
							  (bfd_byte *) buf);
#endif
	  buf += 2;
	  ui->table[index].region_end
	    = ui->table[index].region_start + 4 *
	    (bfd_get_16 (objfile->obfd, (bfd_byte *) buf) - 1);
	  buf += 2;
	}

    }

  /* Unwind table needs to be kept sorted.  */
  qsort (ui->table, total_entries, sizeof (struct unwind_table_entry),
	 compare_unwind_entries);

  /* Keep a pointer to the unwind information.  */
  if (objfile->obj_private == NULL)
    {
      obj_private = (obj_private_data_t *)
	obstack_alloc (&objfile->psymbol_obstack,
		       sizeof (obj_private_data_t));
      obj_private->unwind_info = NULL;
      obj_private->so_info = NULL;
      obj_private->opd = NULL;

      objfile->obj_private = (PTR) obj_private;
    }
  obj_private = (obj_private_data_t *) objfile->obj_private;
  obj_private->unwind_info = ui;
}

/* Lookup the unwind (stack backtrace) info for the given PC.  We search all
   of the objfiles seeking the unwind table entry for this PC.  Each objfile
   contains a sorted list of struct unwind_table_entry.  Since we do a binary
   search of the unwind tables, we depend upon them to be sorted.  */

struct unwind_table_entry *
hppa_find_unwind_entry (pc)
     CORE_ADDR pc;
{
  int first, middle, last;
  struct objfile *objfile;

  /* A function at address 0?  Not in HP-UX! */
  if (pc == (CORE_ADDR) 0)
    return NULL;

  ALL_OBJFILES (objfile)
  {
    struct obj_unwind_info *ui = NULL;

    if (objfile->obj_private)
      ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;

    if (!ui)
      {
	read_unwind_info (objfile);
	if (objfile->obj_private == NULL)
	  error ("Internal error reading unwind information.");
	ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;

#if 0          
          /* RM: ??? The unwind for $cerror indicates that it is a
             millicode routine, but the disassembly shows that the
             return register is r2 instead of r31. Clear the millicode
             flag */
            {
              struct minimal_symbol *msym;
              struct unwind_table_entry *u;
              
              msym = lookup_minimal_symbol("$cerror", 0, objfile);
              if (msym)
                {
                  u = hppa_find_unwind_entry(SYMBOL_VALUE_ADDRESS(msym));
                  if (u)
                    u->Millicode = 0;
                }
            }
#endif
      }

    /* First, check the cache */

    if (ui->cache
	&& pc >= ui->cache->region_start
	&& pc <= ui->cache->region_end)
      return ui->cache;

    /* Not in the cache, do a binary search */

    first = 0;
    last = ui->last;

    while (first <= last)
      {
	middle = (first + last) / 2;
	if (pc >= ui->table[middle].region_start
	    && pc <= ui->table[middle].region_end)
	  {
	    /* RM: ??? Incremental linker bug: the incremental
	       linker seems to be using non-inclusive end addresses for
	       unwind descriptors that are unused. The following hack
	       works around this */
	    while (middle < last &&
		   pc >= ui->table[middle+1].region_start
		   && pc <= ui->table[middle+1].region_end)
	      middle++;
	    ui->cache = &ui->table[middle];
	    return &ui->table[middle];
	  }

	if (pc < ui->table[middle].region_start)
	  last = middle - 1;
	else
	  first = middle + 1;
      }
  }				/* ALL_OBJFILES() */
  return NULL;
}

/* Return the adjustment necessary to make for addresses on the stack
   as presented by hpread.c.

   This is necessary because of the stack direction on the PA and the
   bizarre way in which someone (?) decided they wanted to handle
   frame pointerless code in GDB.  */
int
hpread_adjust_stack_address (func_addr)
     CORE_ADDR func_addr;
{
  struct unwind_table_entry *u;

  u = hppa_find_unwind_entry (func_addr);
  if (!u)
    return 0;
  else
    return u->Total_frame_size << 3;
}

/* Called to determine if PC is in an interrupt handler of some
   kind.  */

static int
pc_in_interrupt_handler (pc)
     CORE_ADDR pc;
{
  struct unwind_table_entry *u;
  struct minimal_symbol *msym_us;
  
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return 0;

  /* Oh joys.  HPUX sets the interrupt bit for _sigreturn even though
     its frame isn't a pure interrupt frame.  Deal with this.  */
  msym_us = lookup_minimal_symbol_by_pc (pc);

  return u->HP_UX_interrupt_marker && !IN_SIGTRAMP (pc, SYMBOL_NAME (msym_us));
}

/* Called when no unwind descriptor was found for PC.  Returns 1 if it
   appears that PC is in a linker stub.

   ?!? Need to handle stubs which appear in PA64 code.  */

static int
pc_in_linker_stub (pc)
     CORE_ADDR pc;
{
  int found_magic_instruction = 0;
  int i;
  char buf[4];

  /* If unable to read memory, assume pc is not in a linker stub.  */
  if (target_read_memory (pc, buf, 4) != 0)
    return 0;

  /* We are looking for something like

     ; $$dyncall jams RP into this special spot in the frame (RP')
     ; before calling the "call stub"
     ldw     -18(sp),rp

     ldsid   (rp),r1         ; Get space associated with RP into r1
     mtsp    r1,sp           ; Move it into space register 0
     be,n    0(sr0),rp)      ; back to your regularly scheduled program */

  /* Maximum known linker stub size is 4 instructions.  Search forward
     from the given PC, then backward.  */
  for (i = 0; i < 4; i++)
    {
      /* If we hit something with an unwind, stop searching this direction.  */

      if (hppa_find_unwind_entry (pc + i * 4) != 0)
	break;

      /* JAGag38255 - We simply read 4 instructions backward/forward 
         from the given pc to look for a linker stub. In course of 
         doing so we may be reading beyond a page boundary which 
         may not actually belong to the inferior. 
         If target_read_memory() returns a non zero value then the 
         pc is not in linker stub or may likely be in an unmapped 
         memory region. So return 0.
      */
      if (target_read_memory ((pc + i * 4), buf, 4) != 0)
        return 0;


      /* Check for ldsid (rp),r1 which is the magic instruction for a 
         return from a cross-space function call.  */
      if (extract_signed_integer (buf, 4) == 0x004010a1)
	{
	  found_magic_instruction = 1;
	  break;
	}
      /* Add code to handle long call/branch and argument relocation stubs
         here.  */
    }

  if (found_magic_instruction != 0)
    return 1;

  /* Now look backward.  */
  for (i = 0; i < 4; i++)
    {
      /* If we hit something with an unwind, stop searching this direction.  */

      if (hppa_find_unwind_entry (pc - i * 4) != 0)
	break;

      /* JAGag38255 */
      if (target_read_memory ((pc - i * 4), buf, 4) != 0)
        return 0;

      /* Check for ldsid (rp),r1 which is the magic instruction for a 
         return from a cross-space function call.  */
      if (extract_signed_integer (buf, 4)== 0x004010a1)
	{
	  found_magic_instruction = 1;
	  break;
	}
      /* Add code to handle long call/branch and argument relocation stubs
         here.  */
    }
  return found_magic_instruction;
}

static int
find_return_regnum (pc)
     CORE_ADDR pc;
{
  struct unwind_table_entry *u;

  u = hppa_find_unwind_entry (pc);

  if (!u)
    return RP_REGNUM;

/* RM: According to the PA64 RAT, local millicode calls put the return
   pointer in RP and external millicode calls put the return pointer
   in r31. This would be a problem, except that we never generate
   external millicode. */
#ifndef GDB_TARGET_IS_HPPA_20W  
  if (u->Millicode)
    return 31;

  /* RM: If LONG_BRANCH_MRP is what I think it is, then this should return
     31 too. */
  if (u->stub_unwind.stub_type == LONG_BRANCH_MRP)
    return 31;
#endif
  
  return RP_REGNUM;
}

int
hppa_pc_in_gcc_function (pc)
     CORE_ADDR pc;
{
  struct symbol *func_sym;

  func_sym = find_pc_function (pc);
  if (func_sym
      && BLOCK_GCC_COMPILED (SYMBOL_BLOCK_VALUE (func_sym))
      != HP_COMPILED_TARGET)
    return 1;
  return 0;
}

/* Return size of frame, or -1 if we should use a frame pointer.  */
static int
find_proc_framesize (pc)
     CORE_ADDR pc;
{
  struct unwind_table_entry *u;
  struct minimal_symbol *msym_us;

  /* This may indicate a bug in our callers... */
  if (pc == (CORE_ADDR) 0)
    return -1;

  u = hppa_find_unwind_entry (pc);

  if (!u)
    {
      if (pc_in_linker_stub (pc))
	/* Linker stubs have a zero size frame.  */
	return 0;
      else
	return -1;
    }

  msym_us = lookup_minimal_symbol_by_pc (pc);

  /* If Save_SP is set, and we're not in an interrupt or signal caller,
     then we have a frame pointer, ie, information is stored in register 
     r3.

     srk -- 980922.  The above statement is incorrect.  Apparently the 
     Save_SP check is used on PA32 for determining whether we have 
     gcc-compiled code; this check is not sufficient as we have encountered 
     HP-compiled code where Save_SP is set, namely Fortran, resulting in -1 
     being errorneously returned to the caller of find_proc_framesize.  
     The check for returning -1 has been refined to check if the pc is in 
     a function which gdb knows about.  If it is, we check the value of the 
     gcc_compile_flag defined in the function's block node and returns -1 
     if the value is less than HP_COMPILED_TARGET; gcc uses stabs and does 
     not set processing_gcc_compilation, the global variable that is used to 
     set the gcc_compile_flag in the block node, so the only thing we're 
     certain about is that if it's HP compiled code, the gcc_compile_flag 
     will be set to HP_COMPILED_TARGET.

     On PA2.0, u->Save_SP is valid, but we want don't want to return -1 
     for Fortran because it we need a valid frame size. */
  if (u->Save_SP && !pc_in_interrupt_handler (pc)
      && !IN_SIGTRAMP (pc, SYMBOL_NAME (msym_us)))
    {
      if (hppa_pc_in_gcc_function(pc))
        return -1;
    }

  /* JYG: MERGE FIXME: the next conditional seems to bomb out
     gdb.hp/gdb.aCC/exception.exp w.r.t. dynamic executable catch catch */
  /* RM: With HP's compilers, the alloca_frame bit indicates whether
     the frame pointer is saved in a register (either r3 or r4) or not */
  if (!hppa_pc_in_gcc_function(pc) &&
#ifdef GDB_TARGET_IS_HPPA_20W
      u->alloca_frame 
#else
      u->Pseudo_SP_Set
#endif
      && !pc_in_interrupt_handler (pc)      
      && !IN_SIGTRAMP (pc, SYMBOL_NAME (msym_us)))
    return -1;

  /* RM: If we are in __wdb_call_dummy, return -1 -- we should use the
     frame pointer in this case */
  if (!strcmp(SYMBOL_NAME (msym_us), "__wdb_call_dummy"))
    return -1;

  return u->Total_frame_size << 3;
}

/* Return offset from sp at which rp is saved, or 0 if not saved.  */
static int rp_saved (CORE_ADDR);

static int
rp_saved (pc)
     CORE_ADDR pc;
{
  struct unwind_table_entry *u;
  CORE_ADDR prologue_begin, prologue_end, epilogue_begin, epilogue_end;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;

  /* A function at, and thus a return PC from, address 0?  Not in HP-UX! */
  if (pc == (CORE_ADDR) 0)
    return 0;

  /* RM: if we are in the prologue of a function, rp may not yet be
     saved. More hacks. */
  find_pc_partial_function (pc, 0, &prologue_begin, 0);
  prologue_end = skip_prologue_hard_way (prologue_begin);
  if (pc < prologue_end)
    {
      /* Srikanth, Sep 26th 2005. This piece of code used to assume that
         rp will not change within the prologue. OIOW, the assumption is
         that if the process is stopped within the prologue, then the 
         return pointer is readable from a live register.

         We have seen some cases (in PBO'd libtten.sl) where RP does get
         modified within the prologue with an mfia %rp instruction or an
         ldo 0x7fc(%r1),%rp instruction. This ends up sending gdb on a tail
         spin.

         Now, we see if rp has been saved and if so, return the appropriate
         offset in the caller's frame. See that stack adjustment within the
         prologue and the attendant issues with setting up the (pseudo) frame
         pointer are already handled by gdb. See init_extra_frame_info ()
         for details.

         OK, that code is flaky and has problems with fortran. So do this
         only if profiling.
      */

      CORE_ADDR current_pc;
      char buf[4];
      unsigned int inst;

      if (!profile_on)
        return 0;

      u = hppa_find_unwind_entry (pc);

      for (current_pc = prologue_begin; current_pc < pc; current_pc += 4)
        {
	  if (target_read_memory (current_pc, buf, 4) != 0)
            return 0;

	  inst = extract_unsigned_integer (buf, 4);

          /* There are only limited ways of storing rp, opcodes lifted from
             skip_prologue_hard_way_1.
          */
          if (inst == 0x6bc23fd9 || inst == 0x0fc212c1 || inst_saves_gr(inst) == 2)
            {
              if (u && u->Save_RP)
                return (TARGET_PTR_BIT == 64 ? -16 : -20);
              else
                return 0;
            }
        }
      return 0;
    }

  /* RM: similarly for epilogues */
  u = hppa_find_unwind_entry (pc);
  if (u)
    {
      /* RM: Are we in an unwind region with an exit point? */
      if (!(u->Region_description & 0x1))
        {
          epilogue_end = u->region_end;
          epilogue_begin = skip_epilogue_hard_way (epilogue_end);
          /* For the prospect issue with GCC code, where gdb where there was a tail spin 
             while printing call-stacks. Though the pc is greater than epilogue_begin,
             if we find that the RP is saved, we should be returning the offset. */
          if ((pc > epilogue_begin) && hppa_pc_in_gcc_function (pc) && u->Save_RP)
             return (TARGET_PTR_BIT == 64 ? -16 : -20);
          if (pc > epilogue_begin)
            return 0;
        }
    }
  
  /* RM: for _sr4export, pc is saved at offset -24 */
  msym = lookup_minimal_symbol_by_pc (pc);
  msym_sr4export = lookup_minimal_symbol ("_sr4export", NULL, NULL);
  if (msym
      && msym_sr4export
      && SYMBOL_VALUE_ADDRESS (msym) ==
           SYMBOL_VALUE_ADDRESS (msym_sr4export))
    {
      return -24;
    }

  if (!u)
    {
      if (pc_in_linker_stub (pc))
	/* This is the so-called RP'.  */
	return -24;
      else
	return 0;
    }

  if (u->Save_RP)
    return (TARGET_PTR_BIT == 64 ? -16 : -20);
#ifndef GDB_TARGET_IS_HPPA_20W
  else if (u->stub_unwind.stub_type != 0)
    {
      switch (u->stub_unwind.stub_type)
	{
	case EXPORT:
	case IMPORT:
	  return -24;
	case PARAMETER_RELOCATION:
	  return -8;
	default:
	  return 0;
	}
    }
#endif
  else
    return 0;
}

int
frameless_function_invocation (frame)
     struct frame_info *frame;
{
  struct unwind_table_entry *u;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;

  u = hppa_find_unwind_entry (frame->pc);

  if (u == 0)
    return 0;

  /* RM: _sr4export is not frameless */
  msym = lookup_minimal_symbol_by_pc (frame->pc);
  msym_sr4export = lookup_minimal_symbol ("_sr4export", NULL, NULL);
  if (msym
      && msym_sr4export
      && SYMBOL_VALUE_ADDRESS (msym) ==
           SYMBOL_VALUE_ADDRESS (msym_sr4export))
    {
      return 0;
    }
              
  return (u->Total_frame_size == 0
#ifndef GDB_TARGET_IS_HPPA_20W
 && u->stub_unwind.stub_type == 0
#endif
  );
}

CORE_ADDR
saved_pc_after_call (frame)
     struct frame_info *frame;
{
  int ret_regnum;
  CORE_ADDR pc, saved_pc;
  struct unwind_table_entry *u;

#ifdef AT_PURIFY_CALL
  if (pc = AT_PURIFY_CALL(prev_pc)) {
    return pc;
  }
#endif

#ifndef GDB_TARGET_IS_HPPA_20W
  /* JYG: PURIFY COMMENT:
     read_register () call below would destroy frame, so we have to
     record saved_pc for possible use later on. */
  saved_pc = FRAME_SAVED_PC (frame);
#endif

  ret_regnum = find_return_regnum (get_frame_pc (frame));
  pc = read_register (ret_regnum) & ~0x3;

  /* If PC is in a linker stub, then we need to dig the address
     the stub will return to out of the stack.  */
#ifndef GDB_TARGET_IS_HPPA_20W
  u = hppa_find_unwind_entry (pc);
  if (u && u->stub_unwind.stub_type != 0)
    return saved_pc;
  else
#endif    
    return pc;
}

CORE_ADDR
hppa_frame_saved_pc (frame)
     struct frame_info *frame;
{
  CORE_ADDR pc = get_frame_pc (frame);
  struct unwind_table_entry *u;
  CORE_ADDR old_pc = 0;
  int spun_around_loop = 0;
  int rp_offset = 0;
  /* RM: need this purely to get offset of wide mode pc within save_state */
  save_state_t save_state;
  int saved_pc_offset;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;

  /* BSD, HPUX & OSF1 all lay out the hardware state in the same manner
     at the base of the frame in an interrupt handler.  Registers within
     are saved in the exact same order as GDB numbers registers.  How
     convienent.  */
  if (pc_in_interrupt_handler (pc))
    return read_memory_integer (frame->frame + PC_REGNUM * REGISTER_SIZE,
				TARGET_PTR_BIT / 8) & ~0x3;

#ifdef FRAME_SAVED_PC_IN_SIGTRAMP
  /* Deal with signal handler caller frames too.  */
  if (frame->signal_handler_caller)
    {
      CORE_ADDR rp;
      int flags;
      /* Handle case when the signal was raised when the process 
	 was in syscall for PA32*/
      if ((TARGET_PTR_BIT / 8 == 4) && 
	((flags=read_memory_integer (frame->frame +40, 4)) & SS_INSYSCALL)){
		return read_memory_integer ((CORE_ADDR) &((save_state_t *) 
			(frame->frame+40))->ss_narrow.ss_gr31,4) & ~0x3 ;
      }
#ifdef FRAME_SAVED_PC_SYSCALL_IN_SIGTRAMP
     /* JAGaf61946: Handle case when the signal was raised when the process 
	 was in syscall for PA64*/
      else if ((flags=read_memory_integer (frame->frame+96 , 4)) & SS_INSYSCALL){
	FRAME_SAVED_PC_SYSCALL_IN_SIGTRAMP (frame, &rp);
      }
#endif
      else
      	FRAME_SAVED_PC_IN_SIGTRAMP (frame, &rp);

     return rp & ~0x3;
    }
#endif

#ifndef GDB_TARGET_IS_HPPA_20W
  if ((frame->pc >= frame->frame
       && frame->pc <= (frame->frame
                        /* A call dummy is sized in words, but it is
                           actually a series of instructions.  Account
                           for that scaling factor.  */
                        + ((REGISTER_SIZE / INSTRUCTION_SIZE)
                           * CALL_DUMMY_LENGTH)
                        /* Similarly we have to account for 64bit
                           wide register saves.  */
                        + (32 * REGISTER_SIZE)
                        /* We always consider FP regs 8 bytes long.  */
                        + (NUM_REGS - FP0_REGNUM) * 8
                        /* Similarly we have to account for 64bit
                           wide register saves.  */
                        + (6 * REGISTER_SIZE))))
#else
  if (PC_IN_CALL_DUMMY (pc, 0, 0))
#endif
    {
      struct frame_saved_regs saved_regs;

      get_frame_saved_regs (frame, &saved_regs);
      pc = read_memory_integer (saved_regs.regs[PC_REGNUM],
                                TARGET_PTR_BIT / 8) & ~0x3;
    }
#ifndef GDB_TARGET_IS_HPPA_20W
  else if (PC_IN_REDIRECT_THROUGH_DUMMY (pc))
    {
      /* The return pointer is saved on the stack. */
      return read_memory_integer (frame->frame - 20, sizeof (CORE_ADDR)) & ~0x3;
    }
#endif
  else if (frameless_function_invocation (frame))
    {
      int ret_regnum;

      ret_regnum = find_return_regnum (pc);

      /* If the next frame is an interrupt frame or a signal
         handler caller, then we need to look in the saved
         register area to get the return pointer (the values
         in the registers may not correspond to anything useful).  */
      if (frame->next
	  && (frame->next->signal_handler_caller
	      || pc_in_interrupt_handler (frame->next->pc)))
	{
	  struct frame_saved_regs saved_regs;

	  get_frame_saved_regs (frame->next, &saved_regs);
          if (IN_SYSCALL(read_memory_integer (saved_regs.regs[FLAGS_REGNUM],
                                              TARGET_PTR_BIT / 8)))
            {
	      pc = read_memory_integer (saved_regs.regs[31],
					TARGET_PTR_BIT / 8) & ~0x3;

	      /* Syscalls are really two frames.  The syscall stub itself
	         with a return pointer in %rp and the kernel call with
	         a return pointer in %r31.  We return the %rp variant
	         if %r31 is the same as frame->pc.  */
	      if (pc == frame->pc)
		pc = read_memory_integer (saved_regs.regs[RP_REGNUM],
					  TARGET_PTR_BIT / 8) & ~0x3;
	    }
	  else
	    pc = read_memory_integer (saved_regs.regs[RP_REGNUM],
				      TARGET_PTR_BIT / 8) & ~0x3;
	}
      else
	pc = read_register (ret_regnum) & ~0x3;
    }
  else
    {
      spun_around_loop = 0;
      old_pc = pc;

    restart:
      rp_offset = rp_saved (pc);
      /* Unwinding from a frameless invocation. We have already read in prev frame`s RP
         from register, we should not read the same for this frame if Save_RP is set. 
         JAGag20972: Sometimes RP is actually saved in the prologue of the frameless 
         function, but with an offset of caller frame`s frame pointer. 
         We also get rp_offset to zero if the RP falls right in the beginning of the
         prologue even though the Save_RP is set for the caller to frameless invocation. 
         For hprof`s call-stack tail-spin issue.See rp_saved () for another fix.- mithun */
      if (rp_offset == 0 && frame->next && frameless_function_invocation (frame->next))
        {
          struct unwind_table_entry *u;
          u = hppa_find_unwind_entry (pc);
          if (u && u->Save_RP)
            rp_offset = (TARGET_PTR_BIT == 64 ? -16 : -20);
        }  

      /* Similar to code in frameless function case.  If the next
         frame is a signal or interrupt handler, then dig the right
         information out of the saved register info.  */
      if (rp_offset == 0
	  && frame->next
	  && (frame->next->signal_handler_caller
	      || pc_in_interrupt_handler (frame->next->pc)))
	{
	  struct frame_saved_regs saved_regs;

	  get_frame_saved_regs (frame->next, &saved_regs);
          if (IN_SYSCALL(read_memory_integer (saved_regs.regs[FLAGS_REGNUM], 
                                              TARGET_PTR_BIT / 8)))
            {
	      pc = read_memory_integer (saved_regs.regs[31],
					TARGET_PTR_BIT / 8) & ~0x3;

	      /* Syscalls are really two frames.  The syscall stub itself
	         with a return pointer in %rp and the kernel call with
	         a return pointer in %r31.  We return the %rp variant
	         if %r31 is the same as frame->pc.  */
	      if (pc == frame->pc)
		pc = read_memory_integer (saved_regs.regs[RP_REGNUM],
					  TARGET_PTR_BIT / 8) & ~0x3;
	    }
	  else
	    pc = read_memory_integer (saved_regs.regs[RP_REGNUM],
				      TARGET_PTR_BIT / 8) & ~0x3;
	}
      else if (rp_offset == 0)
	{
	  old_pc = pc;
	  pc = read_register (RP_REGNUM) & ~0x3;
	}
      else
	{
	  old_pc = pc;
	  pc = read_memory_integer (frame->frame + rp_offset,
				    TARGET_PTR_BIT / 8) & ~0x3;
	  if ((pc==0) && (java_debugging) )
	  {
	    /* rp in the frame marker is 0             */
	    /* Check if external rp contains a Java PC */
	    pc=read_memory_integer (frame->frame -24, 
		TARGET_PTR_BIT / 8) & ~0x3;
	    if (is_java_frame(pc))
	      return pc;
	    pc=0;
	  }	
	}
    }

  /* If PC is inside a linker stub, then dig out the address the stub
     will return to. 

     Don't do this for long branch stubs.  Why?  For some unknown reason
     _start is marked as a long branch stub in hpux10.  */
#ifndef GDB_TARGET_IS_HPPA_20W
  u = hppa_find_unwind_entry (pc);
  if (u && u->stub_unwind.stub_type != 0
      && u->stub_unwind.stub_type != LONG_BRANCH)
    {
      unsigned int insn;

      /* If this is a dynamic executable, and we're in a signal handler,
         then the call chain will eventually point us into the stub for
         _sigreturn.  Unlike most cases, we'll be pointed to the branch
         to the real sigreturn rather than the code after the real branch!. 

         Else, try to dig the address the stub will return to in the normal
         fashion.  */
      insn = read_memory_integer (pc, 4);
      if ((insn & 0xfc00e000) == 0xe8000000)
	return (pc + extract_17 (insn) + 8) & ~0x3;
      else
	{
	  if (old_pc == pc)
	    spun_around_loop++;

	  if (spun_around_loop > 1)
	    {
	      /* We're just about to go around the loop again with
	         no more hope of success.  Die. */
	      error ("Unable to find return pc for this frame");
	    }
	  else
	    goto restart;
	}
    }
#endif

  return pc;
}

#ifdef CONTINUE_AT_DYNLINK_HOOK_RETURN_PC

/* Srikanth, Aug 20th JAGae39135 On PA32, If we are stoped at __d_trap()
   for shl_load notification, rather than step over the breakpoint, 
   continue at the return address. Code for __d_trap looks like

   0x64dec <__d_trap>:     bv,n %r0(%rp)
   0x64df0 <__d_trap+4>:   nop

   So doing a normal step over breakpoint is really semantically
   the same as continuing at the return address.

   This function is used to compute the PC at which the program
   must be resumed.

   This is needed to avoid a "Internal error : changing continue
   to step" message from gdb at Intel. What happens there is
   Intel's app uses ualarm() calls to request SIGALRM signals
   every 200 milliseconds. When we try to single step over the
   breakpoint and a SIGALRM is pending, the OS actually does a
   single step into the sighandler rather than over the breakpoint
   slot.

   This causes gdb to get into serious fits.

   As an added benefit, this eliminates one context switch
   in shl_load processing.

   This problem should show up in some other contexts too ?
*/

CORE_ADDR
dynlink_hook_return_pc (CORE_ADDR stop_pc)
{
  struct minimal_symbol * m;
  struct minimal_symbol * rm;
  CORE_ADDR return_pc;
  struct unwind_table_entry *u;
  unsigned int insn;

  if (stop_pc == 0)
    return 0;

  m = lookup_minimal_symbol_by_pc (stop_pc);

  /* Are we at the dynlink hook ? */
  if (m && SYMBOL_NAME(m) && !strcmp(SYMBOL_NAME(m), "__d_trap"))
    {
      /* Be absolutely sure we are doing the right thing : */

      /* Are we stopped at bv,n %r0(%rp) ? */

      insn = read_memory_integer (stop_pc, 4);
      if (insn != 0xe840c002)
        return 0;

      return_pc = read_register (2) & ~3;
      rm = lookup_minimal_symbol_by_pc (return_pc);

      /* Additional check : return_pc should actually point
         to the export stub for __d_trap;
      */
      if (rm && SYMBOL_NAME(rm) && !strcmp(SYMBOL_NAME(rm), "__d_trap"))
        {
          u = hppa_find_unwind_entry (return_pc);
          if (u && u->stub_unwind.stub_type != 0
                && u->stub_unwind.stub_type == EXPORT)
            return return_pc;
        }
    } 

  return 0;
}
#endif

/* RM: For PA64 initialize the AP */

/* JAGaf70690 - GDB crashes when invoked via WDB-GUI - <BEGIN> */
#ifdef GDB_TARGET_IS_HPPA_20W
CORE_ADDR saved_ap_value = 0;
#endif /* JAGaf70690 - <END> */

void
init_frame_ap(struct frame_info *frame)
{
  int ap_copy_regnum;
  struct frame_info *tmp_frame;
  struct unwind_table_entry *u;

  /* Stacey 10/26/2001 - JAGad87549
     Disable this HP-specific function when running on gcc compiled
     code in order to avoid getting tons of the following errors:
     error "Unable to find AP register" */

  if (hppa_pc_in_gcc_function(frame->pc))
    return;

#ifdef GDB_TARGET_IS_HPPA_20W
  /* RM: The compilers copy AP over to r3, r4 or r5, depending on the
   * type of frame. We can determine the location of the dedicated AP
   * register for PA64 by examining two bits in the unwind
   * descriptor: large_frame_r3 and alloca_frame.
   *
   *      large_frame_r3    alloca_frame    AP location
   *     -----------------------------------------------
   *            0                 0             r3
   *            0                 1             r4
   *            1                 0             r4
   *            1                 1             r5
   */
  /* RM: ??? We need to handle all kinds of special frames here:
   * signal handlers, interrupts, etc, etc.
   */

  u = hppa_find_unwind_entry(frame->pc);
  /* RM: ??? What if !u? */
  if (u)
    {
      ap_copy_regnum =
        (u->Large_frame && u->alloca_frame) ?
          5 :
          ((u->Large_frame || u->alloca_frame) ? 4 : 3);
      
      tmp_frame = frame->next;
      if (!frame->next)
        { /* Innermost frame */
         
          CORE_ADDR prologue_begin, prologue_end, pc;
          find_pc_partial_function (frame->pc, 0, &prologue_begin, 0);
          prologue_end = skip_prologue_hard_way (prologue_begin);
          /* If pc is in prologue code, read register AP_REGNUM, otherwise,
             read register ap_copy_regnum.
           */
          if (frame->pc <= prologue_end)
            frame->ap = read_register(AP_REGNUM);
          else
	  { 
            frame->ap = read_register(ap_copy_regnum);
          /* JAGaf70690 - gdb aborts when invoked via wdb gui - <BEGIN> -
	     Gdb aborts in case of PA64 DOC, when GR3 gets trashed in assembly 
             level. <frame->ap> is read from reg GR3. While <GR3> still has a
	     value, store it in <saved_ap_value>.*/ 	    
	    if (frame->ap)
	       saved_ap_value = frame->ap;
	    else
	       frame->ap = saved_ap_value;
	   } /* JAGaf70690 - <END> */   
        }
      else
        {
          /* Okay, I copied my AP to ap_copy_regnum before calling my
           * child. ap_copy_regnum is a callee save, so we should be able
           * to dig it out.
           */
	  /* Not valid for gcc frame. We need to check this to handle mixed frames on stack,
	     frames compiled with HP compiler and with gcc. */	
          if (hppa_pc_in_gcc_function (tmp_frame->pc))
	    return;
          while (tmp_frame)
            {
	      struct minimal_symbol * m;

              u = hppa_find_unwind_entry (tmp_frame->pc);
         
              /* JAGaf11924, Sunil & Srikanth : We don't have proper
                 unwind entries emitted for __wdb_call_dummy. See that
                 /CLO/Products/XDB/debug/endasm64.s which defines the
                 space for the call dummy, simply has a sequence of
                 NOPS as place holders which are patched dynamically
                 by gdb with call dummy instructions. As a result, while
                 we do have unwind entries (due to the .callinfo and other
                 directives, the descriptors are bogus.

                 We resort to a check for call dummy by name here and
                 if we are in call dummy, don't use the bogus descriptors.
                 Sep 14th 05.
              */

	      if (u->Entry_GR == 0)
                {
                    m = lookup_minimal_symbol_by_pc (tmp_frame->pc);
                    if (!strcmp (SYMBOL_NAME(m), "__wdb_call_dummy"))
		    {
			if(tmp_frame->next->signal_handler_caller == 1)
				tmp_frame = frame;
                      break; 
		    }
                }
		
              /* RM: ??? What about if !u? */
              if (u)
                {
                  /* Entry_GR specifies the number of callee-saved general
                   * registers saved in the stack.  It starts at r3, so
                   * r3 would be 1.
                   */
                  if (u->Entry_GR >= ap_copy_regnum - 2)
                    break;
                }
              tmp_frame = tmp_frame->next;
            }
	  /* Not valid gor gcc frame. */
          if (hppa_pc_in_gcc_function (tmp_frame->pc))
	    return;

          if (tmp_frame)
            {
              CORE_ADDR prologue_begin, prologue_end, pc;
              find_pc_partial_function (tmp_frame->pc, 0, &prologue_begin, 0);
              prologue_end = skip_prologue_hard_way (prologue_begin);
              if (tmp_frame->pc <= prologue_end)
                {
                  /* The pc is in the prologue code, the value in
                   * ap_copy_regnum was never saved into the
                   * stack (this ap_copy_regnum still holds the value of
                   * the previous argument pointer).
                   */
                  boolean found = false;
                  for (pc = prologue_begin;
                       !found && (pc <= prologue_end);
                       pc += 4) 
                  {
                    long inst = read_memory_integer (pc, 4);
                    /* Look for one of the following instructions :
                       std,ma r3,off(sp)
                       std,ma r4,off(sp)
                       std,ma r5,off(sp)
                    */
                    if (((inst & 0xfff0000f) == 0x73c00008) &&
                         (inst & 0x001f0000) >> 16 == ap_copy_regnum)
                      found = true;
                  }

                  if (found)
                    {
                      /* If the std,ma instruction hasn't been executed yet,
                         read ap from ap_copy_regnum, otherwise read ap
                         from the stack.
                      */
                      if (tmp_frame->pc < pc)
                        frame->ap = read_register (ap_copy_regnum);
                      else
                        frame->ap =
                          read_memory_integer (tmp_frame->frame +
                                               (u ? 8*u->Entry_FR : 0) +
                                               (ap_copy_regnum - 3) * 8,
                                               sizeof(CORE_ADDR));
                    }
                  else
                    error ("Unable to find AP register.");
                }
              else
                {
                  /* We have walked down the chain to a function that saved
                   * ap_copy_regnum.  callee saves starting at r3 are
                   * saved immediately after the callee save FRs
                   */
                  frame->ap =
                    read_memory_integer (tmp_frame->frame +
                                         (u ? 8*u->Entry_FR : 0) +
                                         (ap_copy_regnum - 3) * 8,
                                         sizeof(CORE_ADDR));
                }
            }
          else
            {
              /* The value in ap_copy_regnum was never saved into the
               * stack (thus ap_copy_regnum still holds the value of
               * the previous argument pointer).
               */
              frame->ap = read_register(ap_copy_regnum);
            }
        }
    }
#endif
}  
  
#ifdef GDB_TARGET_IS_HPPA_20W
/* Find the base register with respect to which gcc retrieves the value. 
   Try to find this information from the callee chain. */ 
CORE_ADDR
fetch_ap_for_gcc_frame (char buf[], struct frame_info *frame, int regnum)
{
  CORE_ADDR addr1 = 0;
  int saved_ap_regnum, saved_ap_offset;
  struct frame_info *frame1 = frame->next;

  if (!hppa_pc_in_gcc_function (frame->pc))
    return addr1;
  /* scan the callee chain of this frame. */
  for (;frame1; frame1 = frame1->next)
    {
       if (scan_gcc_prologue_for_saved_ap (frame1, &saved_ap_regnum, &saved_ap_offset))
         {
           addr1 = frame1->frame + saved_ap_offset;
           read_memory (addr1, buf, REGISTER_RAW_SIZE (regnum));
           return addr1;
         }          
     }
  return addr1; 
}

/* Look for particular instructions in prologue, to know whether 
   AP_REGNUM is saved in this frame. saved_ap_regnum will have the preserved register
   in which AP_REGNUM - 64 is saved, and saved_ap_offset gives the offset from SP. */
int 
scan_gcc_prologue_for_saved_ap (struct frame_info *frame, 
                                int *saved_ap_regnum, int *saved_ap_offset)
{
  CORE_ADDR prologue_begin, prologue_end, pc;
  int sp_in_reg = 0;
  *saved_ap_regnum = 0;
  *saved_ap_offset = 0; 
  find_pc_partial_function (frame->pc, 0, &prologue_begin, 0);
  prologue_end = skip_prologue_hard_way (prologue_begin);

  for (pc = prologue_begin; pc <= prologue_end; pc += 4)
    {
      unsigned int inst = read_memory_unsigned_integer (pc, 4);
      /* for instruction like copy %sp,%r3 */
      if (!sp_in_reg && 
          ((inst & 0xffe0ffe0) == 0x8000240)  &&
          GET_FIELD (inst, 11, 15) == SP_REGNUM)
        sp_in_reg = GET_FIELD (inst, 27,31);
      /* for instruction like std %r5,8(%r3) */    
      if (sp_in_reg &&
          (inst & 0xfc00d3c0) == 0xc0012c0 &&
          GET_FIELD (inst, 6, 10) == sp_in_reg)
        {
          *saved_ap_regnum = GET_FIELD (inst, 11, 15);
          *saved_ap_offset = extract_5_store (inst);
        }     
      /* for instruction like ldo -0x40(%ret1),%r5 */
      if (*saved_ap_regnum &&
          (inst & 0xfc000000) == 0x34000000 &&
          GET_FIELD (inst, 6, 10) == AP_REGNUM &&
          GET_FIELD (inst, 11, 15) == *saved_ap_regnum &&
          /*AP is supposed to be saved if it was before in a preserved register*/ 
          (*saved_ap_regnum >=3 && *saved_ap_regnum < 19)) 
        return 1;
          
    }
  return 0;
}
#endif

/* We need to correct the PC and the FP for the outermost frame when we are
   in a system call.  */

void
init_extra_frame_info (fromleaf, frame)
     int fromleaf;
     struct frame_info *frame;
{
  int flags;
  int framesize;
  CORE_ADDR pc;
  CORE_ADDR sp;
  CORE_ADDR epilogue_begin;
  CORE_ADDR prologue_end;
  char buf[4];
  unsigned long inst;
  unsigned long status;
  struct unwind_table_entry *u;
  int inc=0;

  if (frame->next && !fromleaf)
    return;

  /* If the next frame represents a frameless function invocation
     then we have to do some adjustments that are normally done by
     FRAME_CHAIN.  (FRAME_CHAIN is not called in this case.)  */
  if (fromleaf)
    {
      frame->pc = SAVED_PC_AFTER_CALL(frame->next);
      /* Find the framesize of *this* frame without peeking at the PC
         in the current frame structure (it isn't set yet).  */
      framesize = find_proc_framesize (FRAME_SAVED_PC (get_next_frame (frame)));

      /* Now adjust our base frame accordingly.  If we have a frame pointer
         use it, else subtract the size of this frame from the current
         frame.  (we always want frame->frame to point at the lowest address
         in the frame).  */
      if (framesize == -1)
        {
          if (!hppa_pc_in_gcc_function(frame->pc))
            {
              u = hppa_find_unwind_entry (frame->pc);
              /* RM: what to do? */
              if (!u)
                return;
              find_pc_partial_function (frame->pc, 0, &pc, 0);
              prologue_end = skip_prologue_hard_way(pc);
              if (frame->pc >= prologue_end)
                {
                  if (u->Large_frame)
                    frame->frame =
                      read_register(4) - (u->Total_frame_size << 3);
                  else
                    frame->frame =
                      read_register(3) - (u->Total_frame_size << 3);
                  return;
                }
              framesize = u->Total_frame_size << 3;
            }
          else
            {
              frame->frame = TARGET_READ_FP ();
              return;
            }
        }
      frame->frame -= framesize;
      return;
    }

  flags = read_register (FLAGS_REGNUM);
  if (IN_SYSCALL(flags))        /* In system call? */
    frame->pc = read_register (31) & ~0x3;

  /* The outermost frame is always derived from PC-framesize

     One might think frameless innermost frames should have
     a frame->frame that is the same as the parent's frame->frame.
     That is wrong; frame->frame in that case should be the *high*
     address of the parent's frame.  It's complicated as hell to
     explain, but the parent *always* creates some stack space for
     the child.  So the child actually does have a frame of some
     sorts, and its base is the high address in its parent's frame.  */
  framesize = find_proc_framesize (frame->pc);

  /* RM: Are we in the prologue? */
  find_pc_partial_function (frame->pc, 0, &pc, 0);
  prologue_end = skip_prologue_hard_way(pc);
  if (framesize == -1)
    {
      if (!hppa_pc_in_gcc_function(frame->pc))
        {
          u = hppa_find_unwind_entry (frame->pc);
          /* RM: what to do? */
          if (!u)
            return;
          if (frame->pc >= prologue_end)
            {
              if (u->Large_frame)
                frame->frame =
                  read_register(4) - (u->Total_frame_size << 3);
              else
                frame->frame =
                  read_register(3) - (u->Total_frame_size << 3);
              return;
            }
          framesize = u->Total_frame_size << 3;
          /* RM: fall through to prologue analyzer */
        }
      else
        {
          frame->frame = TARGET_READ_FP ();
          return;
        }
    }

  /* RM: If we are in the prologue of a function, we need to do
   * some adjustments to the framesize before we can subtract it
   * from the stack pointer. The current frame is probably junk,
   * but at least we can unwind the stack correctly if we find
   * where the current frame would start if we were past the prologue
   *
   * These are terrible hacks.
   */
  sp = read_sp();
  for (pc=frame->pc; pc < prologue_end; pc+=4)
    {
      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);
      inc += prologue_inst_adjust_sp(inst);
    }
  
  /* RM: similarly for epilogues */
  u = hppa_find_unwind_entry (pc);
  if (u && (!(u->Region_description & 0x1)))
    {
      pc = u->region_end;
      epilogue_begin = skip_epilogue_hard_way (pc);
      for (pc=frame->pc-4; pc >= epilogue_begin; pc-=4)
        {
          status = target_read_memory (pc, buf, 4);
          inst = extract_unsigned_integer (buf, 4);
          inc -= prologue_inst_adjust_sp(inst);
        }
    }
  
  sp+=inc;
  frame->frame = sp - framesize;
}


/* Given a GDB frame, determine the address of the calling function's frame.
   This will be used to create a new GDB frame struct, and then
   INIT_EXTRA_FRAME_INFO and INIT_FRAME_PC will be called for the new frame.

   This may involve searching through prologues for several functions
   at boundaries where GCC calls HP C code, or where code which has
   a frame pointer calls code without a frame pointer.  */

CORE_ADDR
frame_chain (frame)
     struct frame_info *frame;
{
  int my_framesize, caller_framesize;
  struct unwind_table_entry *u = NULL, *u2 = NULL, 
    *callers_unwind_entry = NULL;
  CORE_ADDR frame_base;
  struct frame_info *tmp_frame;

  /* A frame in the current frame list, or zero.  */
  struct frame_info *saved_regs_frame = 0;
  /* Where the registers were saved in saved_regs_frame.
     If saved_regs_frame is zero, this is garbage.  */
  struct frame_saved_regs saved_regs;

  CORE_ADDR caller_pc;
  CORE_ADDR pc;
  CORE_ADDR prologue_end;
  int frame_base_decrement;
  int pseudo_sp_register;

  struct minimal_symbol *min_frame_symbol;
  struct symbol *frame_symbol;
  char *frame_symbol_name = NULL;

  /* If this is a threaded application, and we see the
     routine "__pthread_exit", treat it as the stack root
     for this thread. */

  /* srikanth, for MxN threads on HP-UX, the start routine
     is "__pthread_unbound_body" 
  */

  min_frame_symbol = lookup_minimal_symbol_by_pc (frame->pc);
  frame_symbol = find_pc_function (frame->pc);

  if ((min_frame_symbol != 0) /* && (frame_symbol == 0) */ )
    {
      /* The test above for "no user function name" would defend
         against the slim likelihood that a user might define a
         routine named "__pthread_exit" and then try to debug it.

         If it weren't commented out, and you tried to debug the
         pthread library itself, you'd get errors.

         So for today, we don't make that check. */
      frame_symbol_name = SYMBOL_NAME (min_frame_symbol);
      if (frame_symbol_name != 0)
	{
	  if (0 == strncmp (frame_symbol_name,
			    THREAD_INITIAL_FRAME_SYMBOL,
			    THREAD_INITIAL_FRAME_SYM_LEN))
	    {
	      /* Pretend we've reached the bottom of the stack. */
	      return (CORE_ADDR) 0;
	    }
#ifdef HP_MXN
          if (!strcmp (frame_symbol_name, MXN_THREAD_INITIAL_FRAME_SYMBOL))
            return 0;
#endif
	}
    }				/* End of hacky code for threads. */

    //For JAGaf13527
    if ((frame_symbol_name ? !strcmp (frame_symbol_name, "_sr4export") : NULL))
        call_dummy_frame = 1;
    else	
        call_dummy_frame = 0;

  /* Handle HPUX, BSD, and OSF1 style interrupt frames first.  These
     are easy; at *sp we have a full save state strucutre which we can
     pull the old stack pointer from.  Also see frame_saved_pc for
     code to dig a saved PC out of the save state structure.  */
  if (pc_in_interrupt_handler (frame->pc))
    frame_base = read_memory_integer (frame->frame + SP_REGNUM * 4,
				      TARGET_PTR_BIT / 8);
#ifdef FRAME_BASE_BEFORE_SIGTRAMP
  else if (frame->signal_handler_caller)
    {
      FRAME_BASE_BEFORE_SIGTRAMP (frame, &frame_base);
    }
#endif
  else
    frame_base = frame->frame;

  /* RM: If we are in a command line call, we have reserved some extra
     space for "arguments". Subtract that out to get the real frame
     base */
#ifndef GDB_TARGET_IS_HPPA_20W
  if (frame->pc >= frame->frame
      && frame->pc <= (frame->frame
                       + ((REGISTER_SIZE / INSTRUCTION_SIZE)
                          * CALL_DUMMY_LENGTH)
                       + (32 * REGISTER_SIZE)
                       + (NUM_REGS - FP0_REGNUM) * 8
                       + (6 * REGISTER_SIZE)))
    frame_base -= 48;
#else
  if (PC_IN_CALL_DUMMY (frame->pc, 0, 0))
    frame_base -= 16;
#endif
  
  /* Get frame sizes for the current frame and the frame of the 
     caller.  */
  my_framesize = find_proc_framesize (frame->pc);
  caller_pc = FRAME_SAVED_PC (frame);

  /* If caller_pc is a Java PC, since we don't know the caller Java frame's 
     size, return frame_base. Java functions don't have unwind entries */
  if ((java_debugging)&&(is_java_frame(caller_pc)))
    return frame->frame;

  /* RM: If we are in main and the caller is in dld, just return
     0. frame_chain_valid will indicate that this is an invalid frame
     chain value anyway */
#ifdef GDB_TARGET_IS_HPPA_20W
  if ((min_frame_symbol) && (frame_symbol_name) &&
      (!strcmp(frame_symbol_name, "main")) &&
      (pa64_solib_in_dynamic_linker(inferior_pid, caller_pc)))
    return 0;
#endif  
  
  /* If we can't determine the caller's PC, then it's not likely we can
     really determine anything meaningful about its frame.  We'll consider
     this to be stack bottom. */
  if (caller_pc == (CORE_ADDR) 0)
    return (CORE_ADDR) 0;

  caller_framesize = find_proc_framesize (FRAME_SAVED_PC (frame));

  /* RM: set up some variables for alloca frames */
  u = hppa_find_unwind_entry (caller_pc);
  callers_unwind_entry = u;
  u2 = hppa_find_unwind_entry (frame->pc);

  /* JYG: PURIFY COMMENT: hppa_find_unwind_entry can return NULL. */
  if (!u || hppa_pc_in_gcc_function(caller_pc))
    {
      pseudo_sp_register = 3;
      frame_base_decrement = 0;
    }
  else
    {
      pseudo_sp_register = u->Large_frame ? 4 : 3;
      frame_base_decrement = u->Total_frame_size << 3;
    }

  /* If caller does not have a frame pointer, then its frame
     can be found at current_frame - caller_framesize.  */
  if (caller_framesize != -1)
    {
      return frame_base - caller_framesize;
    }
#ifndef GDB_TARGET_IS_HPPA_20W
  else if (PC_IN_REDIRECT_THROUGH_DUMMY (caller_pc))
    /* caller_framesize is -1 if pc is in __wdb_call_dummy. We know the frame
       size of dummy routine which redirects calls, subtract it from the
       frame_base to get previous frame pointer. */ 
    return frame_base - REDIRECT_THROUGH_DUMMY_FRAMESIZE;
#endif
  /* Both caller and callee have frame pointers and are GCC compiled
     (SAVE_SP bit in unwind descriptor is on for both functions.
     The previous frame pointer is found at the top of the current frame.  */
  if (caller_framesize == -1 && my_framesize == -1)
    {
      return read_memory_integer (frame_base +
				  (u2 ? u2->Entry_FR*8 : 0) +
				  (pseudo_sp_register-3) *
				  (TARGET_PTR_BIT / 8),
                                  TARGET_PTR_BIT / 8) -
	frame_base_decrement;
    }
  /* Caller has a frame pointer, but callee does not.  This is a little
     more difficult as GCC and HP C lay out locals and callee register save
     areas very differently.

     The previous frame pointer could be in a register, or in one of 
     several areas on the stack.

     Walk from the current frame to the innermost frame examining 
     unwind descriptors to determine if %r3 ever gets saved into the
     stack.  If so return whatever value got saved into the stack.
     If it was never saved in the stack, then the value in %r3 is still
     valid, so use it. 

     We use information from unwind descriptors to determine if %r3
     is saved into the stack (Entry_GR field has this information).  */

  tmp_frame = frame;
  while (tmp_frame)
    {
      u = hppa_find_unwind_entry (tmp_frame->pc);
           
      if (!u &&  
      #ifndef GDB_TARGET_IS_HPPA_20W //For JAGaf13527
               !(tmp_frame->pc >= tmp_frame->frame
                 && tmp_frame->pc <= (tmp_frame->frame
                 + ((REGISTER_SIZE / INSTRUCTION_SIZE)
                        * CALL_DUMMY_LENGTH)
                 + (32 * REGISTER_SIZE)
                 + (NUM_REGS - FP0_REGNUM) * 8
                 + (6 * REGISTER_SIZE)))
      #else
              !PC_IN_CALL_DUMMY (tmp_frame->pc, 0, 0)
      #endif
         ){

	  /* We could find this information by examining prologues.  I don't
	     think anyone has actually written any tools (not even "strip")
	     which leave them out of an executable, so maybe this is a moot
	     point.  */
	  /* ??rehrauer: Actually, it's quite possible to stepi your way into
	     code that doesn't have unwind entries.  For example, stepping into
	     the dynamic linker will give you a PC that has none.  Thus, I've
	     disabled this warning. */
#if 0
          warning ("Unable to find unwind for PC %s -- Help!",
                   longest_local_hex_string ((LONGEST) tmp_frame->pc));
#endif
	         return (CORE_ADDR) 0;
            }

      /* Entry_GR specifies the number of callee-saved general registers
         saved in the stack.  It starts at %r3, so %r3 would be 1.  */
      if ( (u ? u->Entry_GR : NULL)  > pseudo_sp_register - 3
          || ((u ? u->Save_SP : 0) && hppa_pc_in_gcc_function(tmp_frame->pc))
	  || tmp_frame->signal_handler_caller
	  || pc_in_interrupt_handler (tmp_frame->pc))
	{
          char buf[4];
          unsigned long inst, status;

          /* RM: If we are in the prologue, make sure the register
             has been saved */
          find_pc_partial_function (tmp_frame->pc, 0, &pc, 0);
          prologue_end = skip_prologue_hard_way(pc);
          for (pc = tmp_frame->pc; pc < prologue_end; pc++)
            {
              status = target_read_memory (pc, buf, 4);
              inst = extract_unsigned_integer (buf, 4);
              if (inst_saves_gr(inst) == pseudo_sp_register)
                {
                  tmp_frame = tmp_frame->next;
                  continue;
                }
            }
          break;  
        }
      else
	tmp_frame = tmp_frame->next;
    }

  if (tmp_frame)
    {
      /* We may have walked down the chain into a function with a frame
         pointer.  */
      if ((u->Save_SP && hppa_pc_in_gcc_function(tmp_frame->pc))
	  && !tmp_frame->signal_handler_caller
	  && !pc_in_interrupt_handler (tmp_frame->pc))
	{
	  return read_memory_integer (tmp_frame->frame, TARGET_PTR_BIT / 8);
	}
      /* %r3 was saved somewhere in the stack.  Dig it out.  */
      else
	{
	  /* Sick.

	     For optimization purposes many kernels don't have the
	     callee saved registers into the save_state structure upon
	     entry into the kernel for a syscall; the optimization
	     is usually turned off if the process is being traced so
	     that the debugger can get full register state for the
	     process.

	     This scheme works well except for two cases:

	     * Attaching to a process when the process is in the
	     kernel performing a system call (debugger can't get
	     full register state for the inferior process since
	     the process wasn't being traced when it entered the
	     system call).

	     * Register state is not complete if the system call
	     causes the process to core dump.

	     The following heinous code is an attempt to deal with
	     the lack of register state in a core dump.  

	     Stacey
	     It checks to see if we have core dumped in a system call.  
	     We can tell this by looking at the flags register.  As
	     indicated above the register state is sometimes not complete
	     when this has happened, so if the flags register doesn't
	     appear to be set then read it again just in case. */

	  /* Abominable hack.  */

	  get_frame_saved_regs (tmp_frame, &saved_regs);

	  if (current_target.to_has_execution == 0 && 
	      ( (saved_regs.regs[FLAGS_REGNUM] && 
		 (IN_SYSCALL (read_memory_integer (saved_regs.regs[FLAGS_REGNUM],
						   TARGET_PTR_BIT / 8))))	 
		|| (saved_regs.regs[FLAGS_REGNUM] == 0
		    && IN_SYSCALL(read_register (FLAGS_REGNUM)))))
	    {
	      u = hppa_find_unwind_entry (FRAME_SAVED_PC (frame));
	      
	      /* If we get here, then we know we have core dumped in a system
		 call.  The following code is in place to handle gcc functions
		 which aren't guaranteed to have an unwind entry and HP C 
		 calls to alloca frames.  In these special cases, we can find
		 the beginning of the frame by using the psuedo stack
		 pointer register. 
		 
		 In all other cases we can simply subtract the frame size
		 from what we believe to be the end of the frame.  */
	      
	      if ( (!u) ||
#ifdef GDB_TARGET_IS_HPPA_20W
		   (callers_unwind_entry->alloca_frame)
#else
		   (callers_unwind_entry->Pseudo_SP_Set)
#endif
		   )
		{
		  return read_memory_integer (saved_regs.regs[pseudo_sp_register],
					      TARGET_PTR_BIT / 8) -
		    frame_base_decrement;
		}
	      else
		{
		  return frame_base - (u->Total_frame_size << 3);
		}
	    }

	  return read_memory_integer (saved_regs.regs[pseudo_sp_register],
				      TARGET_PTR_BIT / 8) -
	    frame_base_decrement;
	}
    }
  else
    {
      /* Get the innermost frame.  */
      tmp_frame = frame;
      while (tmp_frame->next != NULL)
	tmp_frame = tmp_frame->next;

      get_frame_saved_regs (tmp_frame, &saved_regs);

      /* Abominable hack.  See above.  */
      if (current_target.to_has_execution == 0
	  && ((saved_regs.regs[FLAGS_REGNUM]
               && (IN_SYSCALL(read_memory_integer (saved_regs.regs[FLAGS_REGNUM],
						   TARGET_PTR_BIT / 8))))
	      || (saved_regs.regs[FLAGS_REGNUM] == 0
                  && IN_SYSCALL(read_register (FLAGS_REGNUM)))))
	{
	  u = hppa_find_unwind_entry (FRAME_SAVED_PC (frame));
	  if (!u)
	    {
	      return read_memory_integer (saved_regs.regs[pseudo_sp_register],
					  TARGET_PTR_BIT / 8) -
		frame_base_decrement;
	    }
	  else
	    {
	      return frame_base - (u->Total_frame_size << 3);
	    }
	}

      /* The value in %r3 was never saved into the stack (thus %r3 still
         holds the value of the previous frame pointer).  */
      return read_register (pseudo_sp_register) - frame_base_decrement;
    }
}


/* To see if a frame chain is valid, see if the caller looks like it
   was compiled with gcc. */

int
hppa_frame_chain_valid (chain, thisframe)
     CORE_ADDR chain;
     struct frame_info *thisframe;
{
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_us;
  struct minimal_symbol *msym_start;
  struct minimal_symbol *msym_call_dummy;
  struct minimal_symbol *msym_sr4export;
  struct unwind_table_entry *u, *next_u = NULL;
  struct frame_info *next;

  if (!chain)
    return 0;

  u = hppa_find_unwind_entry (thisframe->pc);

  if (u == NULL)
    return 1;

  /* We can't just check that the same of msym_us is "_start", because
     someone idiotically decided that they were going to make a Ltext_end
     symbol with the same address.  This Ltext_end symbol is totally
     indistinguishable (as nearly as I can tell) from the symbol for a function
     which is (legitimately, since it is in the user's namespace)
     named Ltext_end, so we can't just ignore it.  */
  msym_us = lookup_minimal_symbol_by_pc (FRAME_SAVED_PC (thisframe));
  msym_start = lookup_minimal_symbol ("_start", NULL, NULL);
  if (msym_us
      && msym_start
      && SYMBOL_VALUE_ADDRESS (msym_us) == SYMBOL_VALUE_ADDRESS (msym_start))
    /* This additional check for inside_main_func is to allow fortran programs
     * where _start and the main program have the same address.  This check 
     * will return 0 for C and C++ programs where _start is defined in libc.
     */
    return inside_main_func (FRAME_SAVED_PC (thisframe));

#ifdef GDB_TARGET_IS_HPPA_20W
  /* Return 0 we reach "main" followed by a millicode routine. */  
  if ((strcmp(SYMBOL_NAME(lookup_minimal_symbol_by_pc (thisframe->pc)),
      "main") == 0) &&
      (strncmp(SYMBOL_NAME(msym_us), "$$", 2) == 0))
      return 0;
#endif

  /* Grrrr.  Some new idiot decided that they don't want _start for the
     PRO configurations; $START$ calls main directly....  Deal with it.  */
  msym_start = lookup_minimal_symbol ("$START$", NULL, NULL);
  if (msym_us
      && msym_start
      && SYMBOL_VALUE_ADDRESS (msym_us) == SYMBOL_VALUE_ADDRESS (msym_start))
    return 0;

  next = get_next_frame (thisframe);
  if (next)
    next_u = hppa_find_unwind_entry (next->pc);

  /* If this frame does not save SP, has no stack, isn't a stub,
     and doesn't "call" an interrupt routine or signal handler caller,
     then its not valid.  */
  if (u->Save_SP || u->Total_frame_size
#ifndef GDB_TARGET_IS_HPPA_20W
      || u->stub_unwind.stub_type != 0
#endif
      || (thisframe->next && thisframe->next->signal_handler_caller)
      || (next_u && next_u->HP_UX_interrupt_marker))
    return 1;

  if (pc_in_linker_stub (thisframe->pc))
    return 1;

  /* RM: If we are in _sr4export or in __wdb_call_dummy, return 1. We
   * need this to unwind through command line calls
   */
  msym = lookup_minimal_symbol_by_pc (thisframe->pc);
  msym_sr4export = lookup_minimal_symbol ("_sr4export", NULL, NULL);
  if (msym
      && msym_sr4export
      && SYMBOL_VALUE_ADDRESS (msym) == SYMBOL_VALUE_ADDRESS (msym_sr4export))
    return 1;
  msym_call_dummy = lookup_minimal_symbol ("__wdb_call_dummy", NULL, NULL);
  if (msym
      && msym_call_dummy
      && SYMBOL_VALUE_ADDRESS (msym) == SYMBOL_VALUE_ADDRESS (msym_call_dummy))
    return 1;

  return 0;
}

/*
   These functions deal with saving and restoring register state
   around a function call in the inferior. They keep the stack
   double-word aligned; eventually, on an hp700, the stack will have
   to be aligned to a 64-byte boundary. */

void
push_dummy_frame (inf_status)
     struct inferior_status *inf_status;
{
  CORE_ADDR sp, pc, pcspace;
  register int regnum;
  CORE_ADDR int_buffer;
  double freg_buffer;

  /* Oh, what a hack.  If we're trying to perform an inferior call
     while the inferior is asleep, we have to make sure to clear
     the "in system call" bit in the flag register (the call will
     start after the syscall returns, so we're no longer in the system
     call!)  This state is kept in "inf_status", change it there.

     We also need a number of horrid hacks to deal with lossage in the
     PC queue registers (apparently they're not valid when the in syscall
     bit is set).  */
  pc = target_read_pc (inferior_pid);
  int_buffer = read_register (FLAGS_REGNUM);
  if (IN_SYSCALL(int_buffer))
    {
      unsigned int sid;
      int_buffer &= ~0x2;
      write_inferior_status_register (inf_status, 0, int_buffer);
      write_inferior_status_register (inf_status, PCOQ_HEAD_REGNUM, pc + 0);
      write_inferior_status_register (inf_status, PCOQ_TAIL_REGNUM, pc + sizeof (CORE_ADDR));
      sid = (pc >> (REGISTER_SIZE * 8 - 2)) & 0x3;
      if (sid == 0)
	pcspace = read_register (SR4_REGNUM);
      else
	pcspace = read_register (SR4_REGNUM + 4 + sid);
      write_inferior_status_register (inf_status, PCSQ_HEAD_REGNUM, pcspace);
      write_inferior_status_register (inf_status, PCSQ_TAIL_REGNUM, pcspace);
    }
  else
    pcspace = read_register (PCSQ_HEAD_REGNUM);

  /* Space for "arguments"; the RP goes in here. */
  sp = read_register (SP_REGNUM) + (TARGET_PTR_BIT == 64 ? 16 : 48);
  int_buffer = read_register (RP_REGNUM) | 0x3;

  /* The 32bit and 64bit ABIs save the return pointer into different
     stack slots.  */
  write_memory (sp - (REGISTER_SIZE == 8 ? 16 : 20),
		(char *) &int_buffer, REGISTER_SIZE);

  int_buffer = TARGET_READ_FP ();
  call_dummy_frame = 1; //For JAGaf13527
  write_memory (sp, (char *) &int_buffer, REGISTER_SIZE);

  write_register (FP_REGNUM, sp);
  call_dummy_frame = 0; //For JAGaf13527

  sp += 2 * REGISTER_SIZE;

  for (regnum = 1; regnum < 32; regnum++)
    if (regnum != RP_REGNUM && regnum != FP_REGNUM)
      sp = push_word (sp, read_register (regnum));

  /* This is not necessary for the 64bit ABI.  In fact it is dangerous.  */
  if (REGISTER_SIZE == 8)
    sp = STACK_ALIGN (sp);
  else
    sp += 4;

  for (regnum = FP0_REGNUM; regnum < NUM_REGS; regnum++)
    {
      read_register_bytes (REGISTER_BYTE (regnum), (char *) &freg_buffer, 8);
      sp = push_bytes (sp, (char *) &freg_buffer, 8);
    }
  sp = push_word (sp, read_register (IPSW_REGNUM));
  sp = push_word (sp, read_register (SAR_REGNUM));
  sp = push_word (sp, pc);
  sp = push_word (sp, pcspace);
  sp = push_word (sp, pc + 4);
  sp = push_word (sp, pcspace);
  write_register (SP_REGNUM, sp);
}

static void
find_dummy_frame_regs (frame, frame_saved_regs)
     struct frame_info *frame;
     struct frame_saved_regs *frame_saved_regs;
{
  CORE_ADDR fp = frame->frame;
  int i;

  /* The 32bit and 64bit ABIs save RP into different locations.  */
  frame_saved_regs->regs[RP_REGNUM] = (fp - (REGISTER_SIZE == 8 ? 16 : 20))
				      & ~0x3;

  frame_saved_regs->regs[FP_REGNUM] = fp;

  frame_saved_regs->regs[1] = fp + (2 * REGISTER_SIZE);

  for (fp += 3 * REGISTER_SIZE, i = 3; i < 32; i++)
    {
      if (i != FP_REGNUM)
	{
	  frame_saved_regs->regs[i] = fp;
	  fp += REGISTER_SIZE;
	}
    }

  /* This is not necessary or desirable for the 64bit ABI.  */
  if (REGISTER_SIZE == 8)
    fp = STACK_ALIGN (fp);
  else
    fp += 4;

  for (i = FP0_REGNUM; i < NUM_REGS; i++, fp += 8)
    frame_saved_regs->regs[i] = fp;

  frame_saved_regs->regs[IPSW_REGNUM] = fp;
  frame_saved_regs->regs[SAR_REGNUM] = fp + REGISTER_SIZE;
  frame_saved_regs->regs[PCOQ_HEAD_REGNUM] = fp + 2 * REGISTER_SIZE;
  frame_saved_regs->regs[PCSQ_HEAD_REGNUM] = fp + 3 * REGISTER_SIZE;
  frame_saved_regs->regs[PCOQ_TAIL_REGNUM] = fp + 4 * REGISTER_SIZE;
  frame_saved_regs->regs[PCSQ_TAIL_REGNUM] = fp + 5 * REGISTER_SIZE;
}

void
hppa_pop_frame ()
{
  register struct frame_info *frame = get_current_frame ();
  register CORE_ADDR fp, npc, target_pc;
  register int regnum;
  struct frame_saved_regs fsr;
  double freg_buffer;
  extern CORE_ADDR stop_pc;

#ifdef GDB_TARGET_IS_HPPA_20W
  if (stop_stack_dummy)
    {
      /* init_extra_frame_info sets frame->frame to sp - framesize.
	 However, framesize is zero for __wdb_call_dummy. For command
	 line calls, r3 is the frame pointer. */
      frame->frame = read_fp();
    }
#endif  
  fp = FRAME_FP (frame);
  get_frame_saved_regs (frame, &fsr);

#ifndef NO_PC_SPACE_QUEUE_RESTORE
  if (fsr.regs[IPSW_REGNUM])	/* Restoring a call dummy frame */
    {
      /* RM: Only do this if we are stopped in the call dummy. Otherwise
       * this doesn't work anyway (since we assume we can execute
       * instructions in the call duumy). If, for instance, we are stopped
       * in a callee of the call dummy, and executing a return command, we
       * _don't_ (necessarily) want to switch space ids anyway.
       *
       * ??? This procedure is pretty broken if we pop a frame
       * representiing an inter space call. The space id never gets
       * restored in this case. (But see below.)
       */
#ifndef GDB_TARGET_IS_HPPA_20W
      if (stop_pc >= frame->frame
          && stop_pc <= (frame->frame
                         + ((REGISTER_SIZE / INSTRUCTION_SIZE)
                            * CALL_DUMMY_LENGTH)
                         + (32 * REGISTER_SIZE)
                         + (NUM_REGS - FP0_REGNUM) * 8
                         + (6 * REGISTER_SIZE)))
#else
      if (PC_IN_CALL_DUMMY (stop_pc, 0, 0))
#endif
	restore_pc_queue (&fsr);
    }
#endif

  for (regnum = 31; regnum > 0; regnum--)
    if (fsr.regs[regnum])
      write_register (regnum, read_memory_integer (fsr.regs[regnum],
		      REGISTER_SIZE));

#ifndef NO_PC_SPACE_QUEUE_RESTORE
  if (1) /* (!fsr.regs[IPSW_REGNUM]) *//* Not restoring a call dummy frame */
    {
      /* RM: i2 actually wants to use the return command, so we can no
       * longer get away with the half-baked version we have had so
       * far. We need to restore the pc space queue for returns
       * across shared libraries.
       * We want to set the PCSQ to point to the space for the new pc.
       * Unfortunately, HPUX doesn't let us change PCSQ directly. So
       * we need YAGH (yet another gross hack).
       * Don't bother if we are stopped in a syscall. The syscall exit
       * code will set up the space registers on the basis of the
       * value of r31. */
      
      CORE_ADDR pcsqh, pcsqt, pcoqh, pcoqt, sr, r1;
      struct target_waitstatus w;
      int inst1, inst2;
      char buf[4];
      int status;
      int sid;

      /* Find the target pc */
      if (fsr.regs[PCOQ_HEAD_REGNUM])
        npc = read_memory_integer (fsr.regs[PCOQ_HEAD_REGNUM], REGISTER_SIZE);
      else 
        npc = read_register (RP_REGNUM) & ~0x3;
      
      sid = (npc >> (REGISTER_SIZE*8-2)) & 0x3;
      if (sid == 0)
        sr = read_register (SR4_REGNUM);
      else
        sr = read_register (SR4_REGNUM + 4 + sid);
      pcsqh = read_register(PCSQ_HEAD_REGNUM);
      if ((pcsqh != sr) && (!(IN_SYSCALL(read_register(FLAGS_REGNUM)))))
        {
          pcoqh = read_register(PCOQ_HEAD_REGNUM) & ~0x3;
          pcoqt = read_register(PCOQ_TAIL_REGNUM) & ~0x3;

          if (target_read_memory (pcoqh, buf, 4) != 0)
            error("Couldn't modify space queue\n"); 
          inst1 = extract_unsigned_integer (buf, 4);
          
          if (target_read_memory (pcoqt, buf, 4) != 0)
            error("Couldn't modify space queue\n"); 
          inst2 = extract_unsigned_integer (buf, 4);

          /* RM: Use BE instead of the PA2.0 specific BVE instruction. */
          /* BE 0(sr sid+4, r1) */
          *((int *) buf) = 0xe0202000 | (sid << 14);
          if (target_write_memory (pcoqh, buf, 4) != 0)
            error("Couldn't modify space queue\n"); 
          
          /* NOP */
          *((int *) buf) = 0x08000240;
          if (target_write_memory (pcoqt, buf, 4) != 0)
            {
              *((int *) buf) = inst1;
              target_write_memory (pcoqh, buf, 4);
              error("Couldn't modify space queue\n"); 
            }
          
          r1 = read_register(1);
          write_register(1, npc);
          
          /* single step twice */
          target_resume (-1, 1, 0);
          registers_changed ();
          target_wait (inferior_pid, &w);
          target_resume (-1, 1, 0);
          registers_changed ();
          target_wait (inferior_pid, &w);
          
          *((int *) buf) = inst1;
          target_write_memory (pcoqh, buf, 4);
          *((int *) buf) = inst2;
          target_write_memory (pcoqt, buf, 4);
          write_register(1, r1);
        }
    }
#endif

  for (regnum = NUM_REGS - 1; regnum >= FP0_REGNUM; regnum--)
    if (fsr.regs[regnum])
      {
	read_memory (fsr.regs[regnum], (char *) &freg_buffer, 8);
	write_register_bytes (REGISTER_BYTE (regnum), (char *) &freg_buffer, 8);
      }

  if (fsr.regs[IPSW_REGNUM])
    write_register (IPSW_REGNUM,
		    read_memory_integer (fsr.regs[IPSW_REGNUM],
					 REGISTER_SIZE));

  if (fsr.regs[SAR_REGNUM])
    write_register (SAR_REGNUM,
		    read_memory_integer (fsr.regs[SAR_REGNUM],
					 REGISTER_SIZE));

  /* If the PC was explicitly saved, then just restore it.  */
  if (fsr.regs[PCOQ_TAIL_REGNUM])
    {
      npc = read_memory_integer (fsr.regs[PCOQ_TAIL_REGNUM],
				 REGISTER_SIZE);
      write_register (PCOQ_TAIL_REGNUM, npc);
      if (fsr.regs[PCOQ_HEAD_REGNUM])
        {
          npc = read_memory_integer (fsr.regs[PCOQ_HEAD_REGNUM], REGISTER_SIZE);
          write_register (PCOQ_HEAD_REGNUM, npc);
        }
    }
  /* Else use the value in %rp to set the new PC.  */
  else
    {
      npc = read_register (RP_REGNUM);
      write_pc (npc);
    }

  if (fsr.regs[FP_REGNUM])
    write_register (FP_REGNUM, read_memory_integer (fp, REGISTER_SIZE));

  if (fsr.regs[IPSW_REGNUM])	/* call dummy */
    write_register (SP_REGNUM, fp - (TARGET_PTR_BIT == 64 ? 16 : 48));
  else
    write_register (SP_REGNUM, fp);

  /* The PC we just restored may be inside a return trampoline.  If so
     we want to restart the inferior and run it through the trampoline.

     Do this by setting a momentary breakpoint at the location the
     trampoline returns to. 

     Don't skip through the trampoline if we're popping a dummy frame.  */
  target_pc = SKIP_TRAMPOLINE_CODE (npc & ~0x3) & ~0x3;
  if (target_pc && !fsr.regs[IPSW_REGNUM])
    {
      struct symtab_and_line sal;
      struct breakpoint *breakpoint;
      struct cleanup *old_chain;

      /* Set up our breakpoint.   Set it to be silent as the MI code
         for "return_command" will print the frame we returned to.  */
      sal = find_pc_line (target_pc, 0);
      sal.pc = target_pc;
      breakpoint = set_momentary_breakpoint (sal, NULL, bp_finish);
      breakpoint->silent = 1;

      /* So we can clean things up.  */
      old_chain = make_cleanup_delete_breakpoint (breakpoint);

      /* Start up the inferior.  */
      clear_proceed_status ();
      proceed_to_finish = 1;
      proceed ((CORE_ADDR) -1, TARGET_SIGNAL_DEFAULT, 0);

      /* Perform our cleanups.  */
      do_cleanups (old_chain);
    }
  flush_cached_frames ();
}

/* After returning to a dummy on the stack, restore the instruction
   queue space registers. */

static int
restore_pc_queue_1 (CORE_ADDR new_space, CORE_ADDR new_pc)
{
  CORE_ADDR pc = read_pc ();
  struct target_waitstatus w;
  int insn_count;

  /* Advance past break instruction in the call dummy. */
  write_register (PCOQ_HEAD_REGNUM, pc + 4);
  write_register (PCOQ_TAIL_REGNUM, pc + 8);

  /* HPUX doesn't let us set the space registers or the space
     registers of the PC queue through ptrace. Boo, hiss.
     Conveniently, the call dummy has this sequence of instructions
     after the break:
     mtsp r21, sr0
     ble,n 0(sr0, r22)

     So, load up the registers and single step until we are in the
     right place. */

  write_register (21, new_space);
  write_register (22, new_pc);

  for (insn_count = 0; insn_count < 3; insn_count++)
    {
      /* FIXME: What if the inferior gets a signal right now?  Want to
         merge this into wait_for_inferior (as a special kind of
         watchpoint?  By setting a breakpoint at the end?  Is there
         any other choice?  Is there *any* way to do this stuff with
         ptrace() or some equivalent?).  */
      resume (1, 0);
      target_wait (inferior_pid, &w);

      if (w.kind == TARGET_WAITKIND_SIGNALLED)
	{
	  stop_signal = w.value.sig;
	  terminal_ours_for_output ();
	  printf_unfiltered ("\nProgram terminated with signal %s, %s.\n",
			     target_signal_to_name (stop_signal),
			     target_signal_to_string (stop_signal));
	  gdb_flush (gdb_stdout);
	  return 0;
	}
    }
  target_terminal_ours ();
  target_fetch_registers (-1);
  return 1;
}

static int
restore_pc_queue (struct frame_saved_regs *fsr)
{
  CORE_ADDR new_pc = read_memory_integer (fsr->regs[PCOQ_HEAD_REGNUM],
                     REGISTER_SIZE);
  CORE_ADDR new_space = read_memory_integer (fsr->regs[PCSQ_HEAD_REGNUM],
                                             REGISTER_SIZE);
  return restore_pc_queue_1 (new_space, new_pc);
}

/* This function pushes a stack frame with arguments as part of the
   inferior function calling mechanism. */

/* JYG: MERGE NOTE: DON'T use Cygnus version -- that one promotes
   integral / float types to long using value_cast() -- for the
   32-bit gdb (which is how PA20W version is built) the implicit
   conversion just doesn't work (truncated long double return to
   a long). */

/* suppress optimization for this function to fix JAGaf15593, "MERGE :
   release_ux gdb32 fails cmdline calls returning structs > 8 bytes".  Since
   this failure only occurs on 11.0 (not 11.11), this pragma can be removed
   when 11.0 is no longer supported in wdb.  - Carl Burch 04/22/2004.  */
#pragma optimize off

CORE_ADDR
hppa_push_arguments (nargs, args, sp, struct_return, struct_addr)
     int nargs;
     value_ptr *args;
     CORE_ADDR sp;
     int struct_return;
     CORE_ADDR struct_addr;
{
  /* array of arguments' offsets */
  int *offset = (int *) alloca (nargs * sizeof (CORE_ADDR));

  /* array of arguments' lengths: real lengths in bytes, not aligned to
     word size */
  int *lengths = (int *) alloca (nargs * sizeof (CORE_ADDR));

  /* The number of stack bytes occupied by the current argument.  */
  int bytes_reserved;

  /* The total number of bytes reserved for the arguments.  */
  int cum_bytes_reserved = 0;

  /* Similarly, but aligned.  */
  int cum_bytes_aligned = 0;
  int i;

  /* When an arg does not occupy a whole word, for instance in bitfields:
     if the arg is x bits (0<x<32), it must be written
     starting from the (x-1)-th position  down until the 0-th position. 
     It is enough to align it to the word.*/ 
  /* if an arg occupies 8 bytes, it must be aligned on the 64-bits 
     high order word in odd arg word*/
  /* if an arg is larger than 64 bits, we need to pass a pointer to it, and
     copy the actual value on the stack, so that the callee can play with it.
     This is taken care of in valops.c in the call_function_by_hand function.
     The argument that is received in this function here has already be converted
     to a pointer to whatever is needed, so that it just can be pushed
     as a word argument*/
  
  for (i = 0; i < nargs; i++)
    {
      lengths[i] = TYPE_LENGTH (VALUE_TYPE (args[i]));

      if (lengths[i] % sizeof(CORE_ADDR))
        bytes_reserved = (lengths[i] / sizeof(CORE_ADDR)) * sizeof(CORE_ADDR)
                         + sizeof(CORE_ADDR);
      else 
        bytes_reserved = lengths[i];

#ifdef GDB_TARGET_IS_HPPA_20W
      /* Integral scalar values smaller than a register are padded on
         the left. */
      if ((lengths[i] < 8) && (bytes_reserved > lengths[i]) &&
          ((is_integral_type(VALUE_TYPE (args[i]))) ||
           (is_float_type(VALUE_TYPE (args[i])))))
        offset[i] = cum_bytes_reserved + bytes_reserved - lengths[i];
      else
        offset[i] = cum_bytes_reserved;
#else
      offset[i] = cum_bytes_reserved + lengths[i];
#endif

      if ((bytes_reserved == 8) && (offset[i] % 8)) /*if 64-bit arg is not 64 bit aligned*/
      {
        int new_offset=0;
        /* bytes_reserved is already aligned to the word, so we put it at one word
           more down the stack. This will leave one empty word on the
           stack, and one unused register. This is OK, see the calling
           convention doc */
        /* the offset may have to be moved to the corresponding position
           one word down the stack, to maintain 
           alignment.*/
        new_offset = (offset[i] / 8) * 8 + 8;
        if ((new_offset - offset[i]) >= sizeof(CORE_ADDR)) 
          {
	    bytes_reserved += sizeof(CORE_ADDR);
	    offset[i] += sizeof(CORE_ADDR);
	  }
      }

#ifdef GDB_TARGET_IS_HPPA_20W
      /* RM: On PA64, arguments larger than 8 bytes are aligned to a
         16 byte boundary */
      if ((bytes_reserved > 8) && (offset[i] % 16))
        {
          /* must already be 8 byte aligned, just move it up 8 more bytes */
          offset[i] += 8;
          bytes_reserved += 8;
        }
#endif

      cum_bytes_reserved += bytes_reserved;
    }

  /* CUM_BYTES_RESERVED already accounts for all the arguments
     passed by the user.  However, the ABIs mandate minimum stack space
     allocations for outgoing arguments.

     The ABIs also mandate minimum stack alignments which we must
     preserve.  */
  cum_bytes_aligned = STACK_ALIGN (cum_bytes_reserved);
#ifndef GDB_TARGET_IS_HPPA_20W
  sp += max (cum_bytes_aligned, REG_PARM_STACK_SPACE);
#endif

  /* Now write each of the args at the proper offset down the stack.  */
  for (i = 0; i < nargs; i++)
    write_memory (sp + (TARGET_PTR_BIT == 64 ? offset[i] : -offset[i]),
		  VALUE_CONTENTS (args[i]), lengths[i]);

#ifdef GDB_TARGET_IS_HPPA_20W
  /* Let AP_REGNUM point to the first stack parameter, i.e., right after
     the eight register parameters. */
  write_register(AP_REGNUM, sp + REG_PARM_STACK_SPACE);
  /* RM: Also write this value out to r4. If we are blocked in a
   * syscall, ap may get trashed by the time we return. This way we
   * can copy over r4 to ap in __wdb_call_dummy()
   */
  write_register(4, sp + REG_PARM_STACK_SPACE);
  sp += max (cum_bytes_aligned, REG_PARM_STACK_SPACE);

  /* JYG: MERGE FIXME: this is what Cygnus introduced.
     Need to resolve.  ifdef'd out for now */
#if 0
  /* ?!? This needs further work.  We need to set up the global data
     pointer for this procedure.  This assumes the same global pointer
     for every procedure.   The call dummy expects the dp value to
     be passed in register %r6.  */
  write_register (6, read_register (27));
#endif

#endif

  /* If a structure has to be returned, set up register 28 to hold its
     address */
  if (struct_return)
    write_register (28, struct_addr);

  /* JYG: MERGE FIXME: WDB version increments 2 * sizeof (CORE_ADDR)
     for 20W and 8 * sizeof (CORE_ADDR) otherwise.  Use Cygnus'
     code for now */
  /* The stack will have additional space for a frame marker.  */
  return sp + (TARGET_PTR_BIT == 64 ? 64 : 32);
}

/* restore from dropping optimization above */
#pragma optimize on

/* elz: this function returns a value which is built looking at the given address.
   It is called from call_function_by_hand, in case we need to return a 
   value which is larger than 64 bits, and it is stored in the stack rather than 
   in the registers r28 and r29 or fr4.
   This function does the same stuff as value_being_returned in values.c, but
   gets the value from the stack rather than from the buffer where all the
   registers were saved when the function called completed. */
value_ptr
hppa_value_returned_from_stack (valtype, addr)
     register struct type *valtype;
     CORE_ADDR addr;
{
  register value_ptr val;

  val = allocate_value (valtype);
  CHECK_TYPEDEF (valtype);
  target_read_memory (addr, VALUE_CONTENTS_RAW (val), TYPE_LENGTH (valtype));

  return val;
}



/* elz: Used to lookup a symbol in the shared libraries.
   This function calls shl_findsym, indirectly through a
   call to __d_shl_get. __d_shl_get is in end.c, which is always
   linked in by the hp compilers/linkers. 
   The call to shl_findsym cannot be made directly because it needs
   to be active in target address space. 
   inputs: - minimal symbol pointer for the function we want to look up
   - address in target space of the descriptor for the library
   where we want to look the symbol up.
   This address is retrieved using the 
   som_solib_get_solib_by_pc function (somsolib.c). 
   (or pa64_solib_get_solib_by_pc function (pa64-solib.c)).
   output: - real address in the library of the function.          
   note: the handle can be null, in which case shl_findsym will look for
   the symbol in all the loaded shared libraries.
   files to look at if you need reference on this stuff:
   dld.c, dld_shl_findsym.c
   end.c
   man entry for shl_findsym */

extern CORE_ADDR pa64_solib_get_solib_by_pc (CORE_ADDR);
extern int restore_inferior_status_on_call_completion;

CORE_ADDR
find_stub_with_shl_get (function, handle)
     struct minimal_symbol *function;
     CORE_ADDR handle;
{
  struct symbol *get_sym, *symbol2;
  struct minimal_symbol *buff_minsym = 0, *msymbol = 0;
  struct type *ftype = 0;
  value_ptr *args;
  value_ptr funcval, val;

  int x, namelen, err_value = -1;
  CORE_ADDR tmp = -1;
  CORE_ADDR endo_buff_addr = 0, value_return_addr = 0, errno_return_addr = 0;
  CORE_ADDR stub_addr;
  extern int rtc_in_progress;
  extern CORE_ADDR heap_info_plabel;
  extern CORE_ADDR high_mem_info_plabel;
  extern CORE_ADDR leaks_info_plabel;
  extern CORE_ADDR interval_info_plabel;
  extern CORE_ADDR threads_info_plabel;

  int found_debug_info = 0;
  ExportEntry *export_entry;
  CORE_ADDR shldp_addr = 0;

  /* srikanth, 000303, If garbage collection or heap profiling is in
     progress (both set the same flag `rtc_in_progress'), do not
     proceed with the shl_findsym call, as it could lead to deadlock.
     Use the values cached by gdbrtc.c instead. */
  
  if (rtc_in_progress)
    {
      if (!strcmp(SYMBOL_NAME(function), "__rtc_leaks_info"))
        return leaks_info_plabel;

      if (!strcmp(SYMBOL_NAME(function), "__rtc_heap_info"))
        return heap_info_plabel;
      
      if (!strcmp(SYMBOL_NAME(function), "__rtc_high_mem_info"))
        return high_mem_info_plabel;
      
      if (!strcmp(SYMBOL_NAME(function), "__rtc_heap_interval_info"))
        return interval_info_plabel;
      
      if (!strcmp(SYMBOL_NAME(function), "__rtc_pthread_info"))
        return threads_info_plabel;
    }

  args = (value_ptr *) alloca (sizeof (value_ptr) *
			       (TARGET_PTR_BIT == 64 ? 8 : 6));
  funcval = find_function_in_inferior ("__d_shl_get");

  get_sym = lookup_symbol ("__d_shl_get", NULL, VAR_NAMESPACE, NULL, NULL);
  if (get_sym)
    found_debug_info = 1;

  buff_minsym = lookup_minimal_symbol ("__buffer", NULL, NULL);
  if (!buff_minsym)
    {
      /* Baskar, Lookup export list - to handle stripped exec */
      export_entry = find_symbol_in_export_list ("__buffer", NULL);
      if (export_entry)
        endo_buff_addr = export_entry->address;
      else
	error ("Can't find an address for __buffer");
    }
  else
    endo_buff_addr = SYMBOL_VALUE_ADDRESS (buff_minsym);

  msymbol = lookup_minimal_symbol ("__shldp", NULL, NULL);
  if (!msymbol)
    {
      /* Baskar, Lookup export list - to handle stripped exec */
      export_entry = find_symbol_in_export_list ("__shldp", NULL);
      if (export_entry)
        shldp_addr = export_entry->address;
      else
	error ("Can't find an address for __shldp");
    }
  else
    shldp_addr = SYMBOL_VALUE_ADDRESS (msymbol);

  namelen = strlen (SYMBOL_NAME (function));
  value_return_addr = endo_buff_addr + namelen + 1;

  if (found_debug_info)
    ftype = check_typedef (SYMBOL_TYPE (get_sym));

  /* do alignment */
  if ((x = value_return_addr % 64) != 0)
    value_return_addr = value_return_addr + 64 - x;

  errno_return_addr = value_return_addr + 64;


  /* set up stuff needed by __d_shl_get in buffer in end.o */

  target_write_memory (endo_buff_addr, SYMBOL_NAME (function), namelen + 1);

  target_write_memory (value_return_addr, (char *) &tmp, TARGET_PTR_BIT / 8);

  target_write_memory (errno_return_addr, (char *) &tmp, TARGET_PTR_BIT / 8);

  target_write_memory (shldp_addr, (char *) &handle, TARGET_PTR_BIT / 8);

  /* now prepare the arguments for the call */

  if (found_debug_info)
    {
      args[0] = value_from_longest (TYPE_FIELD_TYPE (ftype, 0),
                                    (TARGET_PTR_BIT == 64 ? 10 : 12));
      args[1] = value_from_longest (TYPE_FIELD_TYPE (ftype, 1), shldp_addr);
      args[2] = value_from_longest (TYPE_FIELD_TYPE (ftype, 2), 
                                    endo_buff_addr);
      args[3] = value_from_longest (TYPE_FIELD_TYPE (ftype, 3), 
                                    TYPE_PROCEDURE);
      args[4] = value_from_longest (TYPE_FIELD_TYPE (ftype, 4), 
                                    value_return_addr);
      if (TARGET_PTR_BIT == 64)
        {
          /* RM: PA64's __d_shl_get takes two additional parameters */
          args[5] = value_from_longest (TYPE_FIELD_TYPE(ftype, 6), stop_pc);
          args[6] = value_from_longest (TYPE_FIELD_TYPE(ftype, 5),
                                        errno_return_addr+64);
          args[7] = value_from_longest (TYPE_FIELD_TYPE(ftype, 7),
                                        errno_return_addr);
        }
      else
        {
          args[5] = value_from_longest (TYPE_FIELD_TYPE(ftype, 5),
                                        errno_return_addr);
        }
    }
  else /* stripped exec - no debug info */
    {
      args[0] = 
        value_from_longest (lookup_pointer_type (builtin_type_unsigned_int),
                            (TARGET_PTR_BIT == 64 ? 10 : 12));
      
      args[1] = 
        value_from_longest (lookup_pointer_type (builtin_type_unsigned_int), 
                            shldp_addr);
      args[2] = 
        value_from_longest (lookup_pointer_type (builtin_type_unsigned_int), 
                            endo_buff_addr);
      args[3] = 
        value_from_longest (lookup_pointer_type (builtin_type_unsigned_int), 
                            TYPE_PROCEDURE);
      args[4] = 
        value_from_longest (lookup_pointer_type (builtin_type_unsigned_int), 
                            value_return_addr);
      
      if (!TARGET_PTR_BIT == 64)
        {
          args[5] = value_from_longest (lookup_pointer_type (
                              builtin_type_unsigned_int), errno_return_addr);
        }
      else
        {
          args[5] = value_from_longest (lookup_pointer_type (
                              builtin_type_unsigned_long),stop_pc);
          args[6] = value_from_longest (lookup_pointer_type (
                                           builtin_type_unsigned_long),
                                        errno_return_addr+64);
          args[7] = value_from_longest (lookup_pointer_type (
                                           builtin_type_unsigned_long),
                                        errno_return_addr);
        }
    }
  
  /* now call the function */
  restore_inferior_status_on_call_completion = 0;
  val = call_function_by_hand (funcval, (TARGET_PTR_BIT == 64 ? 8 : 6), args);
  restore_inferior_status_on_call_completion = 1;

  /* now get the results */

  target_read_memory (errno_return_addr, (char *) &err_value, 
                      sizeof (err_value));

  target_read_memory (value_return_addr, (char *) &stub_addr, 
                      sizeof (stub_addr));
  if (stub_addr == (CORE_ADDR) -1L)
    error("call to __d_shl_get failed, could not find function to call\n");
  if (stub_addr <= 0)
    error ("call to __d_shl_get failed, error code is %d", err_value);

  return (stub_addr);
}

/* RM: find address of a variable in a shared library. Used for DOOM,
 * where we don't have the address of the import slot in the debug info
 */

CORE_ADDR
find_var_with_shl_get (var)
     const char *var;
{
  struct symbol *get_sym, *symbol2;
  struct minimal_symbol *buff_minsym, *msymbol;
  struct type *ftype;
  value_ptr *args;
  value_ptr funcval, val;

  int x, namelen, err_value = -1;
  CORE_ADDR tmp = -1;
  CORE_ADDR endo_buff_addr, value_return_addr, errno_return_addr;
  CORE_ADDR stub_addr;


  args = (value_ptr *) alloca (sizeof (value_ptr) *
			       (TARGET_PTR_BIT == 64 ? 8 : 6));
  funcval = find_function_in_inferior ("__d_shl_get");
  get_sym = lookup_symbol ("__d_shl_get", NULL, VAR_NAMESPACE, NULL, NULL);
  buff_minsym = lookup_minimal_symbol ("__buffer", NULL, NULL);
  msymbol = lookup_minimal_symbol ("__shldp", NULL, NULL);
  symbol2 = lookup_symbol ("__shldp", NULL, VAR_NAMESPACE, NULL, NULL);
  endo_buff_addr = SYMBOL_VALUE_ADDRESS (buff_minsym);
  namelen = strlen (var);
  value_return_addr = endo_buff_addr + namelen + 1;
  ftype = check_typedef (SYMBOL_TYPE (get_sym));

  /*do alignment */
  if ((x = value_return_addr % 64) != 0)
    value_return_addr = value_return_addr + 64 - x;

  errno_return_addr = value_return_addr + 64;


  /* set up stuff needed by __d_shl_get in buffer in end.o */

  target_write_memory (endo_buff_addr, (char *) var, namelen + 1);

  target_write_memory (value_return_addr, (char *) &tmp, TARGET_PTR_BIT / 8);

  target_write_memory (errno_return_addr, (char *) &tmp, TARGET_PTR_BIT / 8);

  target_write_memory (SYMBOL_VALUE_ADDRESS (msymbol),
                       (char *) 0, TARGET_PTR_BIT / 8);

  /*now prepare the arguments for the call */

  args[0] = value_from_longest (TYPE_FIELD_TYPE (ftype, 0),
				(TARGET_PTR_BIT == 64 ? 10 : 12));
  args[1] = value_from_longest (TYPE_FIELD_TYPE (ftype, 1), SYMBOL_VALUE_ADDRESS (msymbol));
  args[2] = value_from_longest (TYPE_FIELD_TYPE (ftype, 2), endo_buff_addr);
  args[3] = value_from_longest (TYPE_FIELD_TYPE (ftype, 3), TYPE_DATA);
  args[4] = value_from_longest (TYPE_FIELD_TYPE (ftype, 4), value_return_addr);
  if (TARGET_PTR_BIT == 64)
    {
      /* RM: PA64's __d_shl_get takes two additional parameters */
      args[5] = value_from_longest (TYPE_FIELD_TYPE(ftype, 6), stop_pc);
      args[6] = value_from_longest (TYPE_FIELD_TYPE(ftype, 5),
				    errno_return_addr+64);
      args[7] = value_from_longest (TYPE_FIELD_TYPE(ftype, 7),
				    errno_return_addr);
    }
  else
    {
      args[5] = value_from_longest (TYPE_FIELD_TYPE(ftype, 5),
				    errno_return_addr);
    }

  /* now call the function */

  val = call_function_by_hand (funcval, (TARGET_PTR_BIT == 64 ? 8 : 6), args);

  /*now get the results */

  target_read_memory (errno_return_addr, (char *) &err_value, sizeof (err_value));

  target_read_memory (value_return_addr, (char *) &stub_addr, sizeof (stub_addr));
  if (stub_addr == (CORE_ADDR) - 1L)
    error ("call to __d_shl_get failed, could not find function to call\n");
  if (stub_addr <= 0)
    error ("call to __d_shl_get failed, error code is %d", err_value);

  return (stub_addr);
}

/* Cover routine for find_stub_with_shl_get to pass to catch_errors */
static int
cover_find_stub_with_shl_get (PTR args_untyped)
{
  args_for_find_stub *args = args_untyped;
  args->return_val = find_stub_with_shl_get (args->msym, args->solib_handle);
  return 0;
}


/* 10/25/02 Dinesh, This routine is used to get address
   of the DTLS which is not yet been accessed by the
   Program */

CORE_ADDR
find_tls_get_addr ( module_index, symbol_offset)
     CORE_ADDR module_index;
     CORE_ADDR symbol_offset;

{
  struct symbol *get_sym;
  value_ptr *args;
  value_ptr funcval,val;
  CORE_ADDR retval;

  args = (value_ptr *) alloca (sizeof (value_ptr) * 2);
  funcval = find_function_in_inferior ("__tls_get_addr");
#ifdef GDB_TARGET_IS_HPPA_20W
  args[0] = value_from_longest (builtin_type_long, module_index);
  args[1] = value_from_longest (builtin_type_unsigned_long, symbol_offset);
#else
  args[0] = value_from_longest (builtin_type_int, (LONGEST) module_index);
  args[1] = value_from_longest (builtin_type_long, (LONGEST) symbol_offset);
#endif

  val = call_function_by_hand (funcval, 2 , args);
  retval = value_as_long(val);
  return retval;

}

#ifndef GDB_TARGET_IS_HPPA_20W
CORE_ADDR
locate_sr4export()
{
    unsigned int int_buf[1];
    char *buf = (char*) int_buf;
    unsigned int inst;
    CORE_ADDR base_pc;
    int status;
    CORE_ADDR sr4export_address;
    unsigned sr4_sequence[] = { 0xe6c02000, 0x37e20000, 0x4bc23fd1,
                                0x004010a1, 0x00011820, 0xe0400002,
                                0x08000240
                              };
    int index;
    CORE_ADDR end_pc;
    struct section_table *st;

    extern struct target_ops exec_ops;
    extern bfd *exec_bfd;

    ExportEntry *export_entry;

    /* If the symbol has an entry in export list, skip the sacnning process */
    if ( (export_entry = find_symbol_in_export_list ("_sr4export", NULL)) )
      return export_entry->address;

    /* Srikanth, find the address of _sr4export() in stripped a.outs by
       scanning for the instruction sequence below. This hasn't changed
       atleast since 1996. This is a band aid until a more rigorous
       solution is found.

       0xe6c02000       ble     0(sr4,r22)              ; branch to real entry point
       0x37e20000       copy    r31,rp                  ;   ...return link in rp
       0x4bc23fd1       ldw     -24(sp),rp              ; restore return link from stack
       0x004010a1       ldsid   (rp),r1                 ; get space id for return
       0x00011820       mtsp    r1,sr0
       0xe0400002       be,n    0(sr0,rp)               ; return
       0x08000240       nop
    */

  base_pc = 0;

  /* find the start pc of $CODE$ */
  for ( st = exec_ops.to_sections; st < exec_ops.to_sections_end; st++)
      if (!strcmp ("$CODE$", bfd_section_name (exec_bfd, st->the_bfd_section)))
        {
          base_pc = st->addr;
          break;
        }

  /* find sr4export with in first 1k words of $CODE$ */
  end_pc = base_pc + 0x1000;

  /* Skip over any unreadable words in the beginning ... */
  while ((status = target_read_memory (base_pc, buf, 4)) != 0)
      base_pc += 4;

  /* If we didn't find sr4export within the first 1k words,
     give up.
  */

  sr4export_address = 0;
  index = 0;

  while (base_pc < end_pc && index < 7)
    {
      status = target_read_memory (base_pc, buf, 4);
      if (status != 0)
        error ("Could not determine the address of _sr4export\n");
      inst = extract_unsigned_integer (buf, 4);
      if (inst != sr4_sequence[index])
        {
          index = 0;
          sr4export_address = 0;
        }
      else
       {
         if (index == 0)
           sr4export_address = base_pc;
         index++;
       }
      base_pc += 4;

    }

  if (base_pc >= end_pc && index != 7)
    error ("Could not determine the address of _sr4export (searched 16 pages)\n");

  return sr4export_address;
}

CORE_ADDR
locate_dyncall()
{
    unsigned int int_buf[1];
    char *buf = (char*) int_buf;
    unsigned int inst;
    CORE_ADDR base_pc;
    int status;
    CORE_ADDR dyncall_address;
    unsigned dyncall_sequence[] = { 0xc7d6c012, 0xd6c01c1e, 0x0ec81093,
                                    0x0ec01096 };

    int index;
    CORE_ADDR end_pc = 0;
    struct section_table *st;

    extern struct target_ops exec_ops;
    extern bfd *exec_bfd;

    /* Find the address of $$dyncall() in stripped a.out by scanning for the
       instruction sequence below. 
    
    	0xc7d6c012    <$$dyncall>:     bb,>=,n %r22,0x1e,0x16c8 <$$dyncall+16>
    	0xd6c01c1e    <$$dyncall+4>:   depwi 0,31,2,%r22
    	0x0ec81093    <$$dyncall+8>:   ldw 4(%r22),%r19
    	0x0ec01096    <$$dyncall+12>:  ldw 0(%r22),%r22

    	0x02c010a1    <noshlibs>:      ldsid (%r22),%r1
    	0x00011820    <noshlibs+4>:    mtsp %r1,%sr0
    	0xe2c00000    <noshlibs+8>:    be 0(%sr0,%r22)
    	0x6bc23fd1    <noshlibs+12>:   stw %rp,-0x18(%sp)
    */

  base_pc = 0;

  /* find the start pc of $MILLICODE$ */
  for ( st = exec_ops.to_sections; st < exec_ops.to_sections_end; st++)
      if (!strcmp ("$MILLICODE$", bfd_section_name (exec_bfd, st->the_bfd_section)))
        {
          base_pc = st->addr;
          end_pc = st->endaddr;
          break;
        }

  /* Skip over any unreadable words in the beginning ... */
  while ((status = target_read_memory (base_pc, buf, 4)) != 0)
      base_pc += 4;

  dyncall_address = 0;
  index = 0;

  while (base_pc < end_pc && index < 4)
    {
      status = target_read_memory (base_pc, buf, 4);
      if (status != 0)
        error ("Could not determine the address of $$dyncall\n");
      inst = extract_unsigned_integer (buf, 4);
      if (inst != dyncall_sequence[index])
        {
          index = 0;
          dyncall_address = 0;
        }
      else
       {
         if (index == 0)
           dyncall_address = base_pc;

         index++;
       }
      base_pc += 4;
    }

  if (base_pc >= end_pc && index != 4)
    error ("Could not determine the address of $$dyncall (searched millicode area)\n");

  return dyncall_address;
}
#endif

/* Baskar, stripped exec.. 
   function to search export list 
*/

ExportEntry *
find_symbol_in_export_list (name, objf)
   char *name;
   struct objfile *objf;
{
   struct objfile *objfile;
   ExportEntry *export_entry = NULL;
 
   ALL_OBJFILES (objfile)
     {
       if (objf == NULL || objf == objfile)
	{
          if (!objfile->export_list)
            init_export_symbols (objfile);

          if ((export_entry = find_export_entry (name, objfile)))
            break;
	}
     }

   return export_entry;
}

/* Insert the specified number of args and function address
   into a call sequence of the above form stored at DUMMYNAME.

   On the hppa we need to call the stack dummy through $$dyncall.
   Therefore our version of FIX_CALL_DUMMY takes an extra argument,
   real_pc, which is the location where gdb should start up the
   inferior to do the function call. 

   This has to work across several versions of hpux, bsd, osf1.  It has to
   work regardless of what compiler was used to build the inferior program.
   It should work regardless of whether or not end.o is available.  It has
   to work even if gdb can not call into the dynamic loader in the inferior
   to query it for symbol names and addresses.

   Yes, all those cases should work.  Luckily code exists to handle most
   of them.  The complexity is in selecting exactly what scheme should
   be used to perform the inferior call.

   Please contact Jeff Law (law@cygnus.com) before changing this code.  */

CORE_ADDR
hppa_fix_call_dummy (dummy, pc, fun, nargs, args, type, gcc_p)
     char *dummy;
     CORE_ADDR pc;
     CORE_ADDR fun;
     int nargs;
     value_ptr *args;
     struct type *type;
     int gcc_p;
{
  CORE_ADDR dyncall_addr;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *trampoline;
  int flags = read_register (FLAGS_REGNUM);
  struct unwind_table_entry *u = NULL;
  CORE_ADDR new_stub = 0;
  CORE_ADDR solib_handle = 0;

  /* Nonzero if we will use GCC's PLT call routine.  This routine must be
     passed an import stub, not a PLABEL.  It is also necessary to set %r19
     (the PIC register) before performing the call.

     If zero, then we are using __d_plt_call (HP's PLT call routine) or we
     are calling the target directly.  When using __d_plt_call we want to
     use a PLABEL instead of an import stub.  */
  int using_gcc_plt_call = 1;

#ifdef GDB_TARGET_IS_HPPA_20W
  /* We currently use completely different code for the PA2.0W inferior
     function call sequences.  This needs to be cleaned up.  */
  {
    struct obj_section *funcsect;
    CORE_ADDR pcsqh, pcsqt, pcoqh, pcoqt, sr5;
    struct target_waitstatus w;
    int inst1, inst2;
    char buf[4];
    int status;
    struct objfile *objfile;
    asection *opd_section;
    obj_private_data_t *obj_private;
    int size, i;

    struct minimal_symbol *msymbol;
    CORE_ADDR tmp_addr = 0;

    dyncall_addr = fun;

    /* FUN could be an OPD pointer */
    funcsect = find_pc_section (fun);
    if (funcsect
        && STREQ (funcsect->the_bfd_section->name, ".opd"))
      {
        fun = (CORE_ADDR) read_memory_integer (fun + 16, sizeof (CORE_ADDR));
      }

    /* We can not modify the PC space queues directly, so we start
       up the inferior and execute a couple instructions to set the
       space queues so that they point to the call dummy in the stack.  */
    /* RM: We want to set the PCSQ to point to the space for the main
       image. This is in SR5 (usually, but the code below should work
       wherever it it).
       Unfortunately, HPUX doesn't let us change PCSQ directly. So we
       need YAGH (yet another gross hack)
       Don't bother if we are stopped in a syscall. The syscall exit
       code will set up the space registers on the basis of the value
       of r31. */
   /* JAGaf39799 - Execute the below code only when end.o exist */
   if (!endo_missing_in_symfile_objfile)
    {
     pcsqh = read_register (PCSQ_HEAD_REGNUM);
     sr5 = read_register (SR5_REGNUM);
     if ((pcsqh != sr5) && (!(IN_SYSCALL (read_register (FLAGS_REGNUM)))))
      {
        pcoqh = read_register (PCOQ_HEAD_REGNUM);
        pcoqt = read_register (PCOQ_TAIL_REGNUM);
        if (target_read_memory (pcoqh, buf, 4) != 0)
          error ("Couldn't modify space queue\n");
        inst1 = extract_unsigned_integer (buf, 4);

        if (target_read_memory (pcoqt, buf, 4) != 0)
          error ("Couldn't modify space queue\n");
        inst2 = extract_unsigned_integer (buf, 4);

        /* BVE (r1) */
        *((int *) buf) = 0xe820d000;
        if (target_write_memory (pcoqh, buf, 4) != 0)
          error ("Couldn't modify space queue\n");

        /* NOP */
        *((int *) buf) = 0x08000240;
        if (target_write_memory (pcoqt, buf, 4) != 0)
          {
            *((int *) buf) = inst1;
            target_write_memory (pcoqh, buf, 4);
            error ("Couldn't modify space queue\n");
          }

        /* Baskar */
	msymbol = lookup_minimal_symbol ("__wdb_call_dummy", NULL, NULL);
	if (!msymbol)
	  {
	    ExportEntry *export_entry = 0;
	    export_entry = find_symbol_in_export_list ("__wdb_call_dummy", NULL);
	    if (export_entry)
	      tmp_addr = (CORE_ADDR)export_entry->address;
	    else
	      error ("Couldn't find the address of __wdb_call_dummy");
	  }
	else
	  tmp_addr = SYMBOL_VALUE_ADDRESS (msymbol);

        write_register (1, tmp_addr);

        /* Single step twice, the BVE instruction will set the space queue
	   such that it points to the PC value written immediately above
	   (ie the call dummy).  */
        resume (1, 0);
        target_wait (inferior_pid, &w);
        resume (1, 0);
        target_wait (inferior_pid, &w);

	/* Restore the two instructions at the old PC locations.  */
        *((int *) buf) = inst1;
        target_write_memory (pcoqh, buf, 4);
        *((int *) buf) = inst2;
        target_write_memory (pcoqt, buf, 4);
      }
    }
    /* We need to see if this objfile has a different DP value than our
       own (it could be a shared library for example).  */
    ALL_OBJFILES (objfile)
      {
	struct obj_section *s;

	/* See if FUN is in any section within this shared library.  */
	for (s = objfile->sections; s < objfile->sections_end; s++)
	  if (s->addr <= fun && fun <= s->endaddr)
	    break;

        if (s >= objfile->sections_end)
	  continue;

        /* RM: ??? just use saved dp value in objfile? */
        /* Keep a pointer to the opd information.  */
	if (objfile->obj_private == NULL)
          {
	    obj_private = (obj_private_data_t *)
			  obstack_alloc (&objfile->psymbol_obstack,
					 sizeof (obj_private_data_t));
	    obj_private->unwind_info = NULL;
	    obj_private->so_info = NULL;
	    obj_private->opd = NULL;

	    objfile->obj_private = (PTR) obj_private;
	  }
	else
	  obj_private = (obj_private_data_t *) objfile->obj_private;
	
	/* The DP value may be different for each objfile.  But within an
	   objfile each function uses the same dp value.  Thus we do not need
	   to grope around the opd section looking for dp values.

	   ?!? This is not strictly correct since we may be in a shared library
	   and want to call back into the main program.  To make that case
         work correctly we need to set DP for the main program's objfile,
         then remove this conditional.  */

	if (!obj_private->opd)
          {
	    opd_section =
	      bfd_get_section_by_name (objfile->obfd, ".opd");
	    if (!opd_section)
	      error ("No opd sectionin file %s", objfile->obfd->filename);

	    size = bfd_get_section_size_before_reloc (opd_section);
	    obj_private->opd =
	      (opd_data *) obstack_alloc (&objfile->psymbol_obstack,
					  size);
	    bfd_get_section_contents (objfile->obfd, opd_section,
				      obj_private->opd, 0, size);
	    obj_private->n_opd_entries = size / sizeof (opd_data);
	  }

	/* adjust function address */
	fun -= ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
	for (i = 0; i < obj_private->n_opd_entries; i++)
          {
	    if (obj_private->opd[i].func_addr == fun)
              {
		/* Write the function address to r22, and dp to its register
		   respectively. */
		fun += ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
		write_register (22, fun);
		/* RM: Also write this value out to r5. If we are
		 * blocked in a syscall, r22 may get trashed by the
		 * time we return. This way we can copy over r5 to r22
		 * in __wdb_call_dummy() */
		write_register (5, fun);
#ifdef DP_SAVED_IN_OBJFILE
		write_register (DP_REGNUM, objfile->saved_dp_reg); 

		/* RM: Similarly, make a copy of DP in r6 */
		write_register (6, objfile->saved_dp_reg); 
#else
		write_register (DP_REGNUM, obj_private->opd[i].dp +
				ANOFFSET (objfile->section_offsets,
					  SECT_OFF_DATA (objfile)));
		/* RM: Similarly, make a copy of DP in r6 */
		write_register (6, obj_private->opd[i].dp +
				ANOFFSET (objfile->section_offsets,
					  SECT_OFF_DATA (objfile)));
#endif
	        return fun;
	      }
	  }
	/* re-adjust function address */
	fun += ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));

	/* RM: actually, it's quite possible that a function has no .opd
	   entry -- most static functions don't, for example. */
	/* RM: ??? I think all .opd entries in a single objfile will have
	 * the same dp value anyway, since dp only changes between shared
	 * libraries. So just pick an arbitrary entry. This assumes that
	 * we've already found the right objfile to look in.
	 */
	if (obj_private->n_opd_entries)
          {
	    write_register (22, fun);
	    /* RM: Also write this value out to r5. If we are
	     * blocked in a syscall, r22 may get trashed by the
	     * time we return. This way we can copy over r5 to r22 */
	    write_register (5, fun);
#ifdef DP_SAVED_IN_OBJFILE
	    write_register (DP_REGNUM, objfile->saved_dp_reg); 
	     /*RM: Similarly, make a copy of DP in r6 */
	    write_register (6, objfile->saved_dp_reg); 
#else
	    write_register (DP_REGNUM, obj_private->opd[0].dp +
			    ANOFFSET (objfile->section_offsets,
				      SECT_OFF_DATA (objfile)));
	    /* RM: Similarly, make a copy of DP in r6 */
	    write_register (6, obj_private->opd[0].dp +
			    ANOFFSET (objfile->section_offsets,
				      SECT_OFF_DATA (objfile))); 	
#endif
	    return fun;
	  }
	else
	  error (".opd section has no entries");
      }

    if (IN_SYSCALL (flags))
      return pc;
#ifndef GDB_TARGET_IS_PA_ELF
    else if (pa64_solib_get_got_by_pc (target_read_pc (inferior_pid)))
      return pc;
#endif
    else
      return dyncall_addr;
  }
#endif

#ifndef GDB_TARGET_IS_HPPA_20W
  /* Prefer __gcc_plt_call over the HP supplied routine because
     __gcc_plt_call works for any number of arguments.  */
  trampoline = NULL;
  if (lookup_minimal_symbol ("__gcc_plt_call", NULL, NULL) == NULL)
    using_gcc_plt_call = 0;

  /* Command-line calls in stripped a.out
     If "current-pc" is in a.out then the call to dummy frame should go
     thru $$dyncall. Find the address of $$dyncall in stripped a.out.
   */
  if (endo_missing_in_symfile_objfile || found_endo_export_list) 
    {
      dyncall_addr = locate_dyncall ();
    }
  else
    {
      msymbol = lookup_minimal_symbol ("$$dyncall", NULL, NULL);
      if (msymbol == NULL)
        error ("Can't find an address for $$dyncall trampoline");

      dyncall_addr = SYMBOL_VALUE_ADDRESS (msymbol);
    }

  /* FUN could be a procedure label, in which case we have to get
     its real address and the value of its GOT/DP if we plan to
     call the routine via gcc_plt_call.  */
  if (fun & 0x2)
    {
      /* Get the GOT/DP value for the target function.  It's
         at *(fun+4).  Note the call dummy is *NOT* allowed to
         trash %r19 before calling the target function.  */
      write_register (19, read_memory_integer ((fun & ~0x3) + REGISTER_SIZE,
		      REGISTER_SIZE));

      /* Now get the real address for the function we are calling, it's
         at *fun.  */
      fun = (CORE_ADDR) read_memory_integer (fun & ~0x3,
					     TARGET_PTR_BIT / 8);
    }
  else
    {

#ifndef GDB_TARGET_IS_PA_ELF
      /* FUN could be an export stub, the real address of a function, or
         a PLABEL.  When using gcc's PLT call routine we must call an import
         stub rather than the export stub or real function for lazy binding
         to work correctly

         If we are using the gcc PLT call routine, then we need to
         get the import stub for the target function.  */
      if (using_gcc_plt_call && som_solib_get_got_by_pc (fun))
	{
	  struct objfile *objfile;
	  struct minimal_symbol *funsymbol, *stub_symbol;
	  CORE_ADDR newfun = 0;

	  funsymbol = lookup_minimal_symbol_by_pc (fun);
	  if (!funsymbol)
	    error ("Unable to find minimal symbol for target function.\n");

	  /* Search all the object files for an import symbol with the
	     right name. */
	  ALL_OBJFILES (objfile)
	  {
	    stub_symbol
	      = lookup_minimal_symbol_solib_trampoline
	      (SYMBOL_NAME (funsymbol), NULL, objfile);

	    if (!stub_symbol)
	      stub_symbol = lookup_minimal_symbol (SYMBOL_NAME (funsymbol),
						   NULL, objfile);

	    /* Found a symbol with the right name.  */
	    if (stub_symbol)
	      {
		struct unwind_table_entry *u;
		/* It must be a shared library trampoline.  */
		if (MSYMBOL_TYPE (stub_symbol) != mst_solib_trampoline)
		  continue;

		/* It must also be an import stub.  */
		u = hppa_find_unwind_entry (SYMBOL_VALUE (stub_symbol));
		if (u == NULL
		    || (u->stub_unwind.stub_type != IMPORT
#ifdef GDB_NATIVE_HPUX_11
			/* Sigh.  The hpux 10.20 dynamic linker will blow
			   chunks if we perform a call to an unbound function
			   via the IMPORT_SHLIB stub.  The hpux 11.00 dynamic
			   linker will blow chunks if we do not call the
			   unbound function via the IMPORT_SHLIB stub.

			   We currently have no way to select bevahior on just
			   the target.  However, we only support HPUX/SOM in
			   native mode.  So we conditinalize on a native
			   #ifdef.  Ugly.  Ugly.  Ugly  */
			&& u->stub_unwind.stub_type != IMPORT_SHLIB
#endif
			))
		  continue;

		/* OK.  Looks like the correct import stub.  */
		newfun = SYMBOL_VALUE (stub_symbol);
		fun = newfun;

		/* If we found an IMPORT stub, then we want to stop
		   searching now.  If we found an IMPORT_SHLIB, we want
		   to continue the search in the hopes that we will find
		   an IMPORT stub.  */
		if (u->stub_unwind.stub_type == IMPORT)
		  break;
	      }
	  }

	  /* Ouch.  We did not find an import stub.  Make an attempt to
	     do the right thing instead of just croaking.  Most of the
	     time this will actually work.  */
	  if (newfun == 0)
	    write_register (19, som_solib_get_got_by_pc (fun));

	  u = hppa_find_unwind_entry (fun);
	  if (u
	      && (u->stub_unwind.stub_type == IMPORT
		  || u->stub_unwind.stub_type == IMPORT_SHLIB))
	    trampoline = lookup_minimal_symbol ("__gcc_plt_call", NULL, NULL);

	  /* If we found the import stub in the shared library, then we have
	     to set %r19 before we call the stub.  */
	  if (u && u->stub_unwind.stub_type == IMPORT_SHLIB)
	    write_register (19, som_solib_get_got_by_pc (fun));
	}
#endif
    }

  /* If we are calling into another load module then have sr4export call the
     magic __d_plt_call routine which is linked in from end.o.

     You can't use _sr4export to make the call as the value in sp-24 will get
     fried and you end up returning to the wrong location.  You can't call the
     target as the code to bind the PLT entry to a function can't return to a
     stack address.

     Also, query the dynamic linker in the inferior to provide a suitable
     PLABEL for the target function.  */
  if (!using_gcc_plt_call)
    {
      CORE_ADDR new_fun = 0;

      /* Command-line calls in stripped a.out.

	 The dummy-frame checks if the function to be called is in text or data
	 space. If it's in the data space, dummy simply performs a "normal" procedure
	 call, as there is no problem for the function to return back to dummy
	 (dummy is also in data space).
	 If in text space, dummy calls _sr4export and the call to the function is
	 made thru __d_plt_call.

	 Generally, command line call to a function defined in a shared lib will go
	 thru __d_plt_call. In a normal a.out (not-stripped and end.o linked),
	 there is no problem in finding the address of __d_plt_call.

	 But in a stripped executable, as the linker symbol table is empty
	 we cannot find the addr of __d_plt_call. Also in an executable
	 not linked with end.o, __d_plt_call is not available. In these cases,
	 we can make dummy to call the function directly, as there is no problem
	 because the shared libs are loaded into data segment (if mapped private)
      */

      if (endo_missing_in_symfile_objfile)
        {
          struct objfile *objfile;
          struct minimal_symbol *fun_symbol, *fun_symbol1;
          CORE_ADDR newfun = 0;

          /* find the address of target function */
          fun_symbol = lookup_minimal_symbol_by_pc (fun);
          if (!fun_symbol)
            error ("Unable to find minimal symbol for target function.\n");

	  /* Make sure the dummy-frame calls the function and not the stub;
	     So the search for symbol of type "text" */

          ALL_OBJFILES (objfile)
          {
            fun_symbol1 = lookup_minimal_symbol_text
                          (SYMBOL_NAME (fun_symbol), NULL, objfile);

            if (fun_symbol1)
              {
                newfun = SYMBOL_VALUE (fun_symbol1);
                break;
              }
          }

	  if (newfun)
	    fun = newfun;

          /* if we found the function in shared lib, set r19 to linkage
             table pointer */

          if (som_solib_get_solib_by_pc (fun) )
	    {
              write_register (19, som_solib_get_got_by_pc (fun));
	    }

        } /* endo_missing_in_symfile_objfile */

      else /* Normal non-stripped executables */
      {

      ExportEntry *export_entry;
      CORE_ADDR tmp_addr = 0;

      /* Get a handle for the shared library containing FUN.  Given the
         handle we can query the shared library for a PLABEL.  */
      solib_handle = som_solib_get_solib_by_pc (fun);

      if (solib_handle)
	{
	  struct minimal_symbol *fmsymbol = lookup_minimal_symbol_by_pc (fun);

	  trampoline = lookup_minimal_symbol ("__d_plt_call", NULL, NULL);

	  if (trampoline == NULL)
	    {
	      /* Baskar, Lookup export list - to handle stripped exec */
              export_entry = find_symbol_in_export_list ("__d_plt_call", NULL);
              if (export_entry)
		new_fun = (CORE_ADDR)export_entry->address;
	      else
	        error ("Can't find an address for __d_plt_call or __gcc_plt_call trampoline\nSuggest linking executable with -g or compiling with gcc.");
	    }
	  else
	    /* This is where sr4export will jump to.  */
	    new_fun = SYMBOL_VALUE_ADDRESS (trampoline);

	  /* If the function is in a shared library, then call __d_shl_get to
	     get a PLABEL for the target function.  */

	  new_stub = find_stub_with_shl_get (fmsymbol, solib_handle);

	  if (new_stub == 0)
	    error ("Can't find an import stub for %s", SYMBOL_NAME (fmsymbol));

	  /* We have to store the address of the stub in __shlib_funcptr.  */
	  msymbol = lookup_minimal_symbol ("__shlib_funcptr", NULL,
					   (struct objfile *) NULL);

	  if (msymbol == NULL)
	    {
	      /* Baskar, Lookup export list - to handle stripped exec */
              export_entry = find_symbol_in_export_list ("__shlib_funcptr", NULL);
              if (export_entry)
		tmp_addr = (CORE_ADDR)export_entry->address;
	      else
	        error ("Can't find an address for __shlib_funcptr");
	    }
	  else
	    tmp_addr = SYMBOL_VALUE_ADDRESS (msymbol);

	  target_write_memory (tmp_addr, (char *) &new_stub, 4);

	  /* We want sr4export to call __d_plt_call, so we claim it is
	     the final target.  Clear trampoline.  */
	  fun = new_fun;
	  trampoline = NULL;
	}
      } /* Normal non-stripped executable */
    }

  /* Store upper 21 bits of function address into ldil.  fun will either be
     the final target (most cases) or __d_plt_call when calling into a shared
     library and __gcc_plt_call is not available.  */
  store_unsigned_integer
    (&dummy[FUNC_LDIL_OFFSET],
     INSTRUCTION_SIZE,
     deposit_21 (fun >> 11,
		 extract_unsigned_integer (&dummy[FUNC_LDIL_OFFSET],
					   INSTRUCTION_SIZE)));

  /* Store lower 11 bits of function address into ldo */
  store_unsigned_integer
    (&dummy[FUNC_LDO_OFFSET],
     INSTRUCTION_SIZE,
     deposit_14 (fun & MASK_11,
		 extract_unsigned_integer (&dummy[FUNC_LDO_OFFSET],
					   INSTRUCTION_SIZE)));
#ifdef SR4EXPORT_LDIL_OFFSET

  {
    CORE_ADDR trampoline_addr;

    /* We may still need sr4export's address too.  */

    if (trampoline == NULL)
      {
	msymbol = lookup_minimal_symbol ("_sr4export", NULL, NULL);
	if (msymbol == NULL)
	  {
#ifndef GDB_TARGET_IS_HPPA_20W
          /* Srikanth, _sr4export will be missing if the a.out is
             stripped. Don't give up. Scan for the instruction
             sequence and locate it.
          */
          trampoline_addr = locate_sr4export();

          if (trampoline_addr == NULL)
#endif
	    error ("Can't find an address for _sr4export trampoline");
	  }
	  else
	    trampoline_addr = SYMBOL_VALUE_ADDRESS (msymbol);
      }
    else
      trampoline_addr = SYMBOL_VALUE_ADDRESS (trampoline);


    /* Store upper 21 bits of trampoline's address into ldil */
    store_unsigned_integer
      (&dummy[SR4EXPORT_LDIL_OFFSET],
       INSTRUCTION_SIZE,
       deposit_21 (trampoline_addr >> 11,
		   extract_unsigned_integer (&dummy[SR4EXPORT_LDIL_OFFSET],
					     INSTRUCTION_SIZE)));

    /* Store lower 11 bits of trampoline's address into ldo */
    store_unsigned_integer
      (&dummy[SR4EXPORT_LDO_OFFSET],
       INSTRUCTION_SIZE,
       deposit_14 (trampoline_addr & MASK_11,
		   extract_unsigned_integer (&dummy[SR4EXPORT_LDO_OFFSET],
					     INSTRUCTION_SIZE)));
  }
#endif

  write_register (22, pc);

  /* If we are in a syscall, then we should call the stack dummy
     directly.  $$dyncall is not needed as the kernel sets up the
     space id registers properly based on the value in %r31.  In
     fact calling $$dyncall will not work because the value in %r22
     will be clobbered on the syscall exit path. 

     Similarly if the current PC is in a shared library.  Note however,
     this scheme won't work if the shared library isn't mapped into
     the same space as the stack.  */
  if (IN_SYSCALL (flags))
    return pc;
#ifndef GDB_TARGET_IS_PA_ELF
  else if (som_solib_get_got_by_pc (target_read_pc (inferior_pid)))
    return pc;
#endif
  else
    return dyncall_addr;
#endif
}

static struct breakpoint *mmap_bp = 0;
static struct breakpoint *mmap_ret_bp = 0;
static void mmap_begin_hook(void);
static void mmap_end_hook(void);

#ifndef GDB_TARGET_IS_HPPA_20W
extern CORE_ADDR dld_text_offset;
extern CORE_ADDR dld_data_offset;
extern int dld_mmap_calls;
#endif

void
hppa_create_inferior_hook (pid)
  int pid;
{
#ifndef GDB_TARGET_IS_HPPA_20W
  struct minimal_symbol *msymbol;

  remove_mmap_breakpoints();  
  msymbol = lookup_minimal_symbol ("mmap", 0, 0);
  if (!msymbol)
    {
      mmap_bp = 0;
      return;
    }
  mmap_bp =
    create_mmap_breakpoint(SYMBOL_VALUE_ADDRESS(msymbol));
#endif
}

void mmap_hook(void)
{
#ifndef GDB_TARGET_IS_HPPA_20W
  if (mmap_bp && stop_pc == mmap_bp->address) mmap_begin_hook();
  if (mmap_ret_bp && stop_pc == mmap_ret_bp->address) mmap_end_hook();
#endif
}
  
#ifndef GDB_TARGET_IS_HPPA_20W
static void mmap_begin_hook(void)
{
  CORE_ADDR ret_addr;
  struct symtab_and_line sal;
  struct minimal_symbol *msymbol;

  ret_addr = read_register(2) & ~0x3;

  /* RM Assume first call to mmap from _map_dld maps dld text, and
     second maps dld data */
  msymbol = lookup_minimal_symbol_by_pc(ret_addr);
  if (!msymbol)
    return;
  if (strcmp(SYMBOL_NAME(msymbol), "__map_dld"))
    return;

  dld_mmap_calls++;
  if (mmap_bp && dld_mmap_calls==2)
    {
      delete_breakpoint (mmap_bp);
      mmap_bp = 0;
    }
       
  sal.symtab = NULL;
  sal.section = NULL;
  sal.line = 0;
  sal.pc = ret_addr;
  
  mmap_ret_bp = set_momentary_breakpoint(sal, NULL, bp_mmap);
}

static void mmap_end_hook(void)
{
  if (mmap_ret_bp)
    {
      delete_breakpoint (mmap_ret_bp);
      mmap_ret_bp = 0;
    }

  if (dld_mmap_calls==1)
    dld_text_offset = read_register(28);
  if (dld_mmap_calls==2)
    dld_data_offset = read_register(28);
}
#endif

/* If the pid is in a syscall, then the FP register is not readable.
   We'll return zero in that case, rather than attempting to read it
   and cause a warning. */
/* RM: ??? the kernel seems to save gr3 and gr4 even in a syscall
 * context, so I don't know what the above comment refers to. I am
 * disabling the check for syscall. Let's see what this breaks...
 */
CORE_ADDR
target_read_fp (pid)
     int pid;
{
#if 0
  int flags = read_register (FLAGS_REGNUM);

  if (IN_SYSCALL (flags))
    {
      return (CORE_ADDR) 0;
    }
#endif

  /* This is the only site that may directly read_register () the FP
     register.  All others must use TARGET_READ_FP (). */
  return read_register (FP_REGNUM);
}


/* Get the PC from %r31 if currently in a syscall.  Also mask out privilege
   bits.  */

CORE_ADDR
target_read_pc (pid)
     int pid;
{
  int flags = read_register_pid (FLAGS_REGNUM, pid);
  CORE_ADDR pc;
  
  /* The following test does not belong here.  It is OS-specific, and belongs
     in native code.  */
  /* Test SS_INSYSCALL */
  if (IN_SYSCALL(flags))
    return read_register_pid (31, pid) & ~0x3;

  /* RM: ??? read r31 when at the sys call gate */
  pc = read_register_pid (PC_REGNUM, pid) & ~0x3;
  if (pc == SYSCALLGATE)
    return read_register_pid (31, pid) & ~0x3;

  return pc;
}

/* Write out the PC.  If currently in a syscall, then also write the new
   PC value into %r31.  */

void
target_write_pc (v, pid)
     CORE_ADDR v;
     int pid;
{
  int flags = read_register_pid (FLAGS_REGNUM, pid);

  /* The following test does not belong here.  It is OS-specific, and belongs
     in native code.  */
  /* If in a syscall, then set %r31.  Also make sure to get the 
     privilege bits set correctly.  */
  /* Test SS_INSYSCALL */
  if (IN_SYSCALL(flags))
    write_register_pid (31, (LONGEST)(v | 0x3), pid);

  write_register_pid (PC_REGNUM, v, pid);
  write_register_pid (NPC_REGNUM, v + 4, pid);
}

/* return the alignment of a type in bytes. Structures have the maximum
   alignment required by their fields. */

static int
hppa_alignof (type)
     struct type *type;
{
  int max_align, align, i;
  CHECK_TYPEDEF (type);
  switch (TYPE_CODE (type))
    {
    case TYPE_CODE_PTR:
    case TYPE_CODE_INT:
    case TYPE_CODE_FLT:
      return TYPE_LENGTH (type);
    case TYPE_CODE_ARRAY:
      return hppa_alignof (TYPE_FIELD_TYPE (type, 0));
    case TYPE_CODE_STRUCT:
    case TYPE_CODE_UNION:
      max_align = 1;
      for (i = 0; i < TYPE_NFIELDS (type); i++)
	{
	  /* Bit fields have no real alignment. */
	  /* if (!TYPE_FIELD_BITPOS (type, i)) */
	  if (!TYPE_FIELD_BITSIZE (type, i))	/* elz: this should be bitsize */
	    {
	      align = hppa_alignof (TYPE_FIELD_TYPE (type, i));
	      max_align = max (max_align, align);
	    }
	}
      return max_align;
    default:
      return 4;
    }
}

/*
 * pa_is_64bit()
 *   Answer whether the architecture is 64 bit or not
 */
int
pa_is_64bit(void)
{
  static int know_which = 0;  /* False */

  if( !know_which ) {
     if (target_has_stack)
       {
         int flags;
         int old_pid;

         /* The target may be a corefile which came from a different kind
            of system.  Look at the SS_WIDEREGS bit in the save state flags.
            */

         old_pid = inferior_pid;
#ifdef FIND_ACTIVE_THREAD
         inferior_pid = FIND_ACTIVE_THREAD();
#endif
         flags = read_register (FLAGS_REGNUM);
         is_pa_2 = flags & SS_WIDEREGS;

#ifdef FIND_ACTIVE_THREAD
        inferior_pid = old_pid;
#endif
       } /* if target_has_stack */
     else
       {
         if( CPU_PA_RISC2_0 == sysconf(_SC_CPU_VERSION)) {
            is_pa_2 = 1;
         }
       } /* else not target_has_stack */
     
     know_which = 1;  /* True */
  }

  return is_pa_2;
}

/* Print the register regnum, or all registers if regnum is -1 */

void
pa_do_registers_info (regnum, fpregs)
     int regnum;
     int fpregs;
{
  char raw_regs[REGISTER_BYTES];
  int i;

  /* Make a copy of gdb's save area (may cause actual
     reads from the target). */
  for (i = 0; i < NUM_REGS; i++)
    read_relative_register_raw_bytes (i, raw_regs + REGISTER_BYTE (i));

  if (regnum == -1)
    pa_print_registers (raw_regs, regnum, fpregs);
  else if (regnum < FP4_REGNUM)
    {
      long reg_val[2];

      /* Why is the value not passed through "extract_signed_integer"
         as in "pa_print_registers" below? */
      pa_register_look_aside (raw_regs, regnum, &reg_val[0]);

      if (!is_pa_2)
	{
	  printf_unfiltered ("%s %x\n", REGISTER_NAME (regnum), reg_val[1]);
	}
      else
	{
	  /* Fancy % formats to prevent leading zeros. */
	  if (reg_val[0] == 0)
	    printf_unfiltered ("%s %x\n", REGISTER_NAME (regnum), reg_val[1]);
	  else if (is_pa_2 && ((regnum >= FP0_REGNUM)
		   && (regnum <= (FP0_REGNUM+3))))
                printf_unfiltered ("%s %x|%8.8x\n", REGISTER_NAME (regnum),
				   reg_val[0], reg_val[1]);
	       else
	        printf_unfiltered ("%s %x%8.8x\n", REGISTER_NAME (regnum),
			       reg_val[0], reg_val[1]);
	}
    }
  else
    {
#ifdef GDB_TARGET_IS_HPPA_20W
      pa_print_fp_reg (regnum, unspecified_precision);
      printf_filtered("\n");
#else
      pa_print_fp_reg (regnum, single_precision);
      printf_filtered("\n");
      pa_print_fp_reg (regnum, double_precision);
      printf_filtered("\n");
#endif
    }
}

/********** new function ********************/
void
pa_do_strcat_registers_info (regnum, fpregs, stream, precision)
     int regnum;
     int fpregs;
     struct ui_file *stream;
     enum precision_type precision;
{
  char raw_regs[REGISTER_BYTES];
  int i;

  /* srikanth, do not read all registers unless regnum is -1 */ 
  /* Make a copy of gdb's save area (may cause actual
     reads from the target). */
  if (regnum == -1)
    {
      for (i = 0; i < NUM_REGS; i++)
        read_relative_register_raw_bytes (i, raw_regs + REGISTER_BYTE (i));
    }
  else 
    {
      i = regnum;
      read_relative_register_raw_bytes (i, raw_regs + REGISTER_BYTE (i));
    }

  if (regnum == -1)
    pa_strcat_registers (raw_regs, regnum, fpregs, stream);

  else if (regnum < FP4_REGNUM)
    {
      long reg_val[2];

      /* Why is the value not passed through "extract_signed_integer"
         as in "pa_print_registers" below? */
      pa_register_look_aside (raw_regs, regnum, &reg_val[0]);

      if (!is_pa_2)
	{
	  fprintf_unfiltered (stream, "%s %x", REGISTER_NAME (regnum), reg_val[1]);
	}
      else
	{
	  /* Fancy % formats to prevent leading zeros. */
	  if (reg_val[0] == 0)
	    fprintf_unfiltered (stream, "%s %x", REGISTER_NAME (regnum),
				reg_val[1]);
	  else
	    fprintf_unfiltered (stream, "%s %x%8.8x", REGISTER_NAME (regnum),
				reg_val[0], reg_val[1]);
	}
    }
  else
    /* Note that real floating point values only start at
       FP4_REGNUM.  FP0 and up are just status and error
       registers, which have integral (bit) values. */
    pa_strcat_fp_reg (regnum, stream, precision);
}

/* If this is a PA2.0 machine, fetch the real 64-bit register
   value.  Otherwise use the info from gdb's saved register area.

   Note that reg_val is really expected to be an array of longs,
   with two elements. */
void
pa_register_look_aside (raw_regs, regnum, raw_val)
     char *raw_regs;
     int regnum;
     long *raw_val;
{
  static int know_which = 0;	/* False */

  int regaddr;
  unsigned int offset;
  register int i;
  int start;
  extern CORE_ADDR register64_offset;
  extern char *core_save_state_contents;

  char buf[MAX_REGISTER_RAW_SIZE];
  long long reg_val;

  /* The array holds offset of floating point registers */
  /* PA save_state.h defines the macro ssoff() to give the
     offset of a given field within the structure */
  static int fp32_offsets[] = {
        ssoff(ss_frstat),
        ssoff(ss_frexcp1),
        ssoff(ss_frexcp2),
        ssoff(ss_frexcp3),
        ssoff(ss_frexcp4),
        ssoff(ss_frexcp5),
        ssoff(ss_frexcp6),
        ssoff(ss_frexcp7),
 };

if (!know_which)
    {
      if (CPU_PA_RISC2_0 == sysconf (_SC_CPU_VERSION))
	{
	  is_pa_2 = (1 == 1);
	}

      know_which = 1;		/* True */
    }

  raw_val[0] = 0;
  raw_val[1] = 0;

/* srikanth, 001116, JAGad02426, "Registers from core file are bad
   on a PA 2.0 machine." On a PA 2.0 machine, the register set is
   64 bits wide (even on a narrow chip.) A 32 bit program normally
   references only the 32 bit register file unless it is compiled
   with the DA2.0N compiler option. In the latter case, compilers
   generate code in such a way that within functions, the 64 bit wide
   registers are made use of where feasible. But the procedure calling
   conventions preserve and restore only the 32 bit register file.

   However, gdb's support for 2.0N chips is only partially in place.
   For instance see that gdb thinks registers are 4 bytes wide on a
   PA2.0N (see config/pa/tm-hppa.h)

   As a result, if it really is a 2.0N machine we read the two halfs
   from the save_state structure (see below) and assemble the results
   into a 64 bit value. This is possible to do for a running program
   but not for a core file. For a core file, we juxtapose adjacent
   registers, which is a complete bogusity.

   The real fix for this problem is to assemble full 64 bit values from
   the core files and alter gdb's notion of the register size. This is
   going to be pretty involed if we want to ship one gdb for 1.1 and 2.0N.
   For now, I am simply fixing the problem by exposing only the
   32 bit regsiters for a core file.

   Stacey 10/8/2001
   
   For now I am putting in a temporary work around for now so that true 
   64 bit values can be viewed from a core file.  This work around should
   be eliminated when we teach gdb to handle the real 64 bit register values
   on PA2.0 machines.  */
   
  if (!is_pa_2)
    {
      /* MERGEBUG: why do we need REGISTER_BYTE(regnum) for? */
      raw_val[1] = *(long *) (raw_regs + REGISTER_BYTE (regnum));
      return;
    }
  else if (!target_has_execution)
    {
#ifdef GDB_TARGET_IS_HPPA_20W
      raw_val[0] = *(long *)(raw_regs + REGISTER_BYTE (regnum));
      raw_val[1] = *(long *)(raw_regs + REGISTER_BYTE (regnum) + 4);
#else
      /* Stacey 10/8/2001
	 If we get here, is_pa_2 and the fact that GDB_TARGET_IS_HPPA_20W
	 is not defined tell us that we are running on PA2.0N.  We also know
	 we're reading registers from a core file.  So, we should restore and
	 print true 64 bit values of registers from the save state structure
	 in the core file where necessary.  

	 According to the documentation in the save state header file
	 /usr/include/sys/machine.h the registers represented by regnum 0 and
	 regnum > 63 (flags and floating point registers) are the same and
	 are stored only once in once place for all PA machines.  Thus,
	 are treatment of them need not change.  

	 Since procedure calling convention saves and restores only 32 bit
	 values and is based on a 32 bit value register, for frames high in 
	 the stack, displaying the 32 bit values is the right thing to do.  
	 
	 So, we only need to be concerned with displaying the 64 bit values of
	 registers 1-63 from the top frame.  GDB's current notion of register 
	 size forces us to store and print the 2 halves of a register from
	 raw_val[0] and raw_val[1].  This is unnecessarily complicated and
	 should be fixed as soon as GDB is able to handle true 64 bit values
	 from core files on PA2.0 machines.  */

      if ( (selected_frame == get_current_frame ()) && 
	   ( (regnum >= 1) && (regnum <= 63)))
	  {
	    raw_val[0] = *(long *) (core_save_state_contents + 
				    register64_offset + 8*regnum);
	    raw_val[1] = *(long *) (core_save_state_contents + 
				    register64_offset + 8*regnum+4);
	  }
      raw_val[1] = *(long *) (raw_regs + REGISTER_BYTE (regnum));

#endif
      return;
    }

  /* Code below copied from hppah-nat.c, with fixes for wide
     registers, using different area of save_state, etc. */
  if (regnum == FLAGS_REGNUM || regnum >= FP0_REGNUM ||
      !HAVE_STRUCT_SAVE_STATE_T || !HAVE_STRUCT_MEMBER_SS_WIDE)
    {
      /* Use narrow regs area of save_state and default macro. */
      offset = U_REGS_OFFSET;
      regaddr = register_addr (regnum, offset);
      start = 1;
    }
  else
    {
      /* Use wide regs area, and calculate registers as 8 bytes wide.

         We'd like to do this, but current version of "C" doesn't
         permit "offsetof":

         offset  = offsetof(save_state_t, ss_wide);

         Note that to avoid "C" doing typed pointer arithmetic, we
         have to cast away the type in our offset calculation:
         otherwise we get an offset of 1! */

      /* NB: save_state_t is not available before HPUX 9.
         The ss_wide field is not available previous to HPUX 10.20,
         so to avoid compile-time warnings, we only compile this for
         PA 2.0 processors.  This control path should only be followed
         if we're debugging a PA 2.0 processor, so this should not cause
         problems. */

      /* #if the following code out so that this file can still be
         compiled on older HPUX boxes (< 10.20) which don't have
         this structure/structure member.  */
#if HAVE_STRUCT_SAVE_STATE_T == 1 && HAVE_STRUCT_MEMBER_SS_WIDE == 1
      save_state_t temp;

      offset = ((int) &temp.ss_wide) - ((int) &temp);
      regaddr = offset + regnum * 8;
      start = 0;
#endif
    }

  for (i = start; i < 2; i++)
    {
      errno = 0;
#ifdef GDB_TARGET_IS_HPPA_20W
      /* If it is PA-2.0 then we should read flaoting ponint status and
         exception registers by reading 8 bytes, so two calls to ptrace.
         Each of them will read in 4 bytes.  */
      if (is_pa_2 && ((regnum >= FP0_REGNUM) && (regnum <= (FP0_REGNUM+3))))
       {
         raw_val[0] = (long) call_ptrace (PT_RUREGS, inferior_pid,
                     (PTRACE_ARG3_TYPE) fp32_offsets[(regnum-FP0_REGNUM)*2], 0);
         raw_val[1] = (long) call_ptrace (PT_RUREGS, inferior_pid, (PTRACE_ARG3_TYPE)
                        fp32_offsets[((regnum-FP0_REGNUM)*2)+1], 0);
       }
      else
#endif
       {
         raw_val[i] = call_ptrace (PT_RUREGS, inferior_pid,
				(PTRACE_ARG3_TYPE) regaddr, 0);
         regaddr += sizeof (long);
       }

      if (errno != 0)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = safe_strerror (errno);
	  char *msg = (char *) alloca (strlen (err) + 128);
	  sprintf (msg, "reading register %s: %s", REGISTER_NAME (regnum), err);
	  warning (msg);
	  goto error_exit;
	}
    }

  if (regnum == PCOQ_HEAD_REGNUM || regnum == PCOQ_TAIL_REGNUM)
    raw_val[1] &= ~0x3;		/* I think we're masking out space bits */

error_exit:
  ;
}

/* Sunil -- To print alias names */
#ifdef GDB_TARGET_IS_HPPA_20W
#define PA_REGS 26
#else
#define PA_REGS 22
#endif

#define ONE 1
struct aliasnames1 {
     char *alias_to;
     char *alias_names[ONE];
};
static const struct aliasnames1 register_aliases[] = {
 	/* Return pointer           */	 { "rp", { "rp/r2" } },
	/* millicode return pointer */	 { "r31", { "mrp/r31" } },
	/* Return value	 	    */	 { "ret0", { "ret0/r28" } },
	/* Return value-High part of 
	   double , Static link,
	   argument pointer	    */	 { "ret1", { "ret1/ap/r29" } },
	/* Stack pointer	    */   { "sp", { "sp/r30" } },
	/* Data/global pointer 	    */   { "dp", { "dp/gp/r27" } },
	/* argument		    */   { "r26", { "arg0/r26" } },
	/* argument or high part
	   of double    	    */   { "r25", { "arg1/r25" } },
	/* argument		    */   { "r24", { "arg2/r24" } },
        /* argument or high part
	   of double		    */   { "r23", { "arg3/r23" } },
#ifdef GDB_TARGET_IS_HPPA_20W
	/* register argument        */   { "r22", { "arg4/r22" } },
	/* register argument        */   { "r21", { "arg5/r21" } },
	/* register argument        */   { "r20", { "arg6/r20" } },
	/* register argument        */   { "r19", { "arg7/r19" } },
#endif
  	/* Recovery counter reg     */   { "cr0", { "rctr/cr0" } },
	/* Protection ID 1 	    */   { "cr8", { "pidr1/cr8" } },
	/* Protection ID 2 	    */   { "cr9", { "pidr2/cr9" } },
	/* Coprocessor config reg   */   { "ccr", { "ccr/cr10" } },
	/* Shift amount reg	    */   { "sar", { "sar/cr11" } },
	/* Protection ID 3	    */   { "cr12", { "pidr3/cr12" } },
	/* Protection ID 4	    */   { "cr13", { "pidr4/cr13" } },
        /* External interrupt enable
	   mask			    */   { "eiem", { "eiem/cr15" } },
	/* Interrupt instruncton reg*/   { "iir", { "iir/cr19" } },
	/* Interrupt space reg	    */   { "isr", { "isr/cr20" } },
	/* Interrupt offset reg     */   { "ior", { "ior/cr21" } },
        /* Interruption processor
	   status word		    */   { "ipsw", { "ipsw/cr22" } }

};

int get_alias (char *reg_name, char **alias1)
{
  int each_group; 
  for (  each_group = 0 ; each_group < PA_REGS ; each_group++ ) {
      /* For each aliases in a group */
          if ( STREQN ( reg_name, 
			register_aliases[each_group].alias_to,
                        strlen(reg_name) ) && 
			( strlen(reg_name) == strlen(register_aliases[each_group].alias_to) ) ) {
  		*alias1 = xmalloc (sizeof(char) * (strlen(register_aliases[each_group].alias_names[0]) + 1));
		sprintf(*alias1, "%s", register_aliases[each_group].alias_names[0]);
		return 1;
	}
  }
  return 0;
}
/* "Info all-reg" command */

static void
pa_print_registers (raw_regs, regnum, fpregs)
     char *raw_regs;
     int regnum;
     int fpregs;
{
  int i, j;
  /* Alas, we are compiled so that "long long" is 32 bits */
  long raw_val[2];
  long long_val;
  enum precision_type precision = unspecified_precision;
  char *reg_name = NULL;

  /* RM: On PA32, each (double precision) floating point register is
   * treated like two (single precision) registers. On PA64, a
   * floating point register really is a floating point register.
   * So on PA32 we report 8 32-bit registers fpe0..fpe7 and on PA64
   * we report 4 64-bit registers fpe0..fpe3.
   */


  /*******************************************************
   *   JAGab15917, JAGad06198, and JAGab16990
   *
   *   Stacey: 6/29/01
   *
   *   The code below has been modified to allow gdb's
   *   info reg command to check to see if the user has
   *   done a set width command and display the registers
   *   in rows accordingly.  This is to avoid printing 4 
   *   registers per row on a 40 character wide terminal.
   *
   *   It takes 30 character spaces to output the info for
   *   for one register.  If the width is 0 or some other 
   *   small number, gdb looks to the user's shell to get 
   *   the user's column width from the appropriate 
   *   environment variable.  
   *
   *   9/21/01
   *   
   *   Since this is only an issue for terminal inferaces,
   *   and this new formatting would actually be a very 
   *   difficult formatting for the GUI to parse, there are
   *   2 sections of code below.  The first is for terminal
   *   interfaces like gdb in line mode, VDB (nimbus) and 
   *   the TUI.  
   *   
   *   Since most GUI's operate at a higher annotation level
   *   than the terminal user interface (because they
   *   require extra printed annotation information from gdb) 
   *   they should will use the second section of code as
   *   and will not be effected by the new formatting below.  
   *
   *   LIMITATION: 
   *   This code assumes that the user will remember to
   *   do a set width command each time he re-sizes his
   *   terminal window. If the user forgets to do a set 
   *   width command, this won't work.  
   *******************************************************/

  if (annotation_level == 0)
    {
#ifdef GDB_TARGET_IS_HPPA_20W
#define REGS 17*4
#else
#define REGS 18*4
#endif

      extern unsigned int get_width ();
      int registers_per_row = (int) ( (int) get_width ()/30);

      if (registers_per_row < 1)
	{
	  registers_per_row = (int) (atoi (getenv ("COLUMNS"))/30);
	  if (registers_per_row < 1)
	    {
	      warning ("Your terminal display isn't wide enough to properly "
		       "display registers so the following output may be "
		       "poorly formatted or difficult to read.\n\n"
		       "To fix this, resize your terminal window and "
		       "execute the gdb 'set width' command to set the "
		       "window width to at least 30 characters\n");
	      registers_per_row=1;
	    }
	}
      
      for (i = 0; i < REGS; i++)
	{
	  pa_register_look_aside (raw_regs, i, &raw_val[0]);

	  /* Even fancier % formats to prevent leading zeros
	   * and still maintain the output in columns.       */
	  if (!is_pa_2)
	    {
	      /* Being big-endian, on this machine the low bits
	       * (the ones we want to look at) are in the second longword.
	       */
	      long_val = extract_signed_integer (&raw_val[1], 4);
	      printf_filtered ("%11.11s: %10x  ",
		       get_alias(REGISTER_NAME (i), &reg_name) ? reg_name : REGISTER_NAME (i),
 		       long_val);
	    }
	  else
	    {
	      /* raw_val = extract_signed_integer (&raw_val, 8); */
	      if (raw_val[0] == 0)
		printf_filtered ("%11.11s:         %8x  ",
		       get_alias (REGISTER_NAME (i), &reg_name) ? reg_name : REGISTER_NAME (i),
		       raw_val[1]);
#ifdef GDB_TARGET_IS_HPPA_20W
              else
		{
 	          if (is_pa_2 && ((i >= FP0_REGNUM) && (i <= (FP0_REGNUM+3))))
                    printf_filtered ("%11.11s: %8x|%8.8x  ",
		       get_alias (REGISTER_NAME (i), &reg_name) ? reg_name : REGISTER_NAME (i),
				 raw_val[0], raw_val[1]);
                  else
 	            printf_filtered ("%11.11s: %8x%8.8x  ",
		       get_alias (REGISTER_NAME (i), &reg_name) ? reg_name : REGISTER_NAME (i),
				 raw_val[0], raw_val[1]);
                }
#endif
	    }
	  if ( (i % registers_per_row) == 0)
	    printf_unfiltered ("\n");
	}
  
      /* just in case there aren't enough registers left to complete
	 the row */
      printf_unfiltered ("\n");
#undef REGS
    }
  else
    {

      /******************************************************
       *   Since most GUI's operate at a higher annotation level
       *   than the terminal user interface (because they
       *   require extra printed annotation information from gdb) 
       *   they should will use the second section of code as
       *   and will not be effected by the new formatting below.  */

#ifdef GDB_TARGET_IS_HPPA_20W
#define REGS 17
#else
#define REGS 18
#endif

      for (i = 0; i < REGS; i++)
	{
	  for (j = 0; j < 4; j++)
	    {
	      /* Q: Why is the value passed through "extract_signed_integer",
	       *    while above, in "pa_do_registers_info" it isn't?
	       * A: ?
	       */
	      pa_register_look_aside (raw_regs, i + (j * REGS), &raw_val[0]);

	      /* Even fancier % formats to prevent leading zeros
	       * and still maintain the output in columns.
	       */
	      if (!is_pa_2)
		{
		  /* Being big-endian, on this machine the low bits
		   * (the ones we want to look at) are in the second longword.
		   */
		  long_val = extract_signed_integer (&raw_val[1], 4);
		  printf_filtered ("%11.11s: %10x  ",
				   REGISTER_NAME (i + (j * REGS)), long_val);
		}
	      else
		{
		  /* raw_val = extract_signed_integer (&raw_val, 8); */
		  if (raw_val[0] == 0)
		    printf_filtered ("%11.11s:         %8x  ",
		     get_alias (REGISTER_NAME (i+(j * REGS)), &reg_name) ? reg_name : REGISTER_NAME (i + (j * REGS)),
				     raw_val[1]);
		  else
		   {
                     if (is_pa_2 && (((i+ (j * REGS)) >= FP0_REGNUM) && ((i+ (j * REGS)) <= (FP0_REGNUM+3))))
                      printf_filtered ("%11.11s: %8x|%8.8x  ",
		       get_alias (REGISTER_NAME (i+(j * REGS)), &reg_name) ? reg_name : REGISTER_NAME (i + (j * REGS)),
				     raw_val[0], raw_val[1]);
                     else
                      printf_filtered ("%11.11s: %8x%8.8x  ",
		        get_alias (REGISTER_NAME (i+(j * REGS)), &reg_name) ? reg_name : REGISTER_NAME (i + (j * REGS)),
				     raw_val[0], raw_val[1]);
                   } 
		}
	    }
	  printf_unfiltered ("\n");
	}
#undef REGS
    }
  if (fpregs)
    /* Note that real floating point values only start at
     * FP4_REGNUM.  FP0 and up are just status and error
     * registers, which have integral (bit) values.
     */
    for (i = FP4_REGNUM; i < NUM_REGS; i++)     /* FP4_REGNUM == 72 */
      {
#ifdef GDB_TARGET_IS_HPPA_20W
        pa_print_fp_reg (i, unspecified_precision);
        printf_filtered ("\n");
#else
        pa_print_fp_reg (i, single_precision);
        printf_filtered ("\n");
        /* if regnum is even, then this register can also be a double-precision FP register */
        if ((i % 2) == 0)
          {
            pa_print_fp_reg (i, double_precision);
            printf_filtered ("\n");
          }
#endif
      }
}

/************* new function ******************/
static void
pa_strcat_registers (raw_regs, regnum, fpregs, stream)
     char *raw_regs;
     int regnum;
     int fpregs;
     struct ui_file *stream;
{
  int i, j;
  long raw_val[2];		/* Alas, we are compiled so that "long long" is 32 bits */
  long long_val;
  enum precision_type precision;

  precision = unspecified_precision;

#ifdef GDB_TARGET_IS_HPPA_20W
#define REGS 17
#else
#define REGS 18
#endif

  for (i = 0; i < REGS; i++)
    {
      for (j = 0; j < 4; j++)
        {
          /* Q: Why is the value passed through "extract_signed_integer",
           *    while above, in "pa_do_registers_info" it isn't?
           * A: ?
           */
          pa_register_look_aside (raw_regs, i + (j * REGS), &raw_val[0]);

          /* Even fancier % formats to prevent leading zeros
           * and still maintain the output in columns.
           */
          if (!is_pa_2)
            {
              /* Being big-endian, on this machine the low bits
               * (the ones we want to look at) are in the second longword.
               */
              long_val = extract_signed_integer (&raw_val[1], 4);
              fprintf_filtered (stream, "%8.8s: %8x  ",
                                REGISTER_NAME (i + (j * REGS)),
                                long_val);
            }
          else
            {
              /* raw_val = extract_signed_integer (&raw_val, 8); */
              if (raw_val[0] == 0)
                fprintf_filtered (stream, "%8.8s:         %8x  ",
                                  REGISTER_NAME (i + (j * REGS)),
                                  raw_val[1]);
              else
                fprintf_filtered (stream, "%8.8s: %8x%8.8x  ",
                                  REGISTER_NAME (i + (j * REGS)),
                                  raw_val[0],
                                  raw_val[1]);
            }
        }
      fprintf_unfiltered (stream, "\n");
    }

  if (fpregs)
    for (i = FP4_REGNUM; i < NUM_REGS; i++)     /* FP4_REGNUM == 72 */
      pa_strcat_fp_reg (i, stream, precision);

#undef REGS
}

static void
pa_print_fp_reg (i, precision)
     int i;
     enum precision_type precision;
{
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];

  /* Get 32bits of data.  */
  read_relative_register_raw_bytes (i, raw_buffer);

  /* Put it in the buffer.  No conversions are ever necessary.  */
  memcpy (virtual_buffer, raw_buffer, REGISTER_RAW_SIZE (i));

#ifdef GDB_TARGET_IS_HPPA_20W
  if ((precision == double_precision) ||
      (precision == unspecified_precision))
#else
  if (precision == double_precision && (i % 2) == 0)
#endif
    {
      /* If the user wants double precision and if the register number is
         even, format it that way.
      */
      char raw_buffer[MAX_REGISTER_RAW_SIZE];
 
#ifndef GDB_TARGET_IS_HPPA_20W
      /* Get the data in raw format for the 2nd half. */
      read_relative_register_raw_bytes (i + 1, raw_buffer);

      /* Copy it into the appropriate part of the virtual buffer. */
      memcpy (virtual_buffer + REGISTER_RAW_SIZE(i), raw_buffer,
              REGISTER_RAW_SIZE(i));
#endif
      fputs_filtered (REGISTER_NAME (i), gdb_stdout);
      print_spaces_filtered (8 - strlen (REGISTER_NAME (i)), gdb_stdout);
      fputs_filtered ("(double precision)     ", gdb_stdout);
      val_print (builtin_type_double, (char *)virtual_buffer, 0, 0,
                 gdb_stdout, 0, 1, 0, Val_pretty_default);
      printf_filtered ("\n");

#ifdef GDB_TARGET_IS_HPPA_20W
      if (precision == unspecified_precision)
        {
          fprintf_filtered (gdb_stdout, "\n%sL", REGISTER_NAME (i));
          print_spaces_filtered (7 - strlen (REGISTER_NAME (i)),
                                 gdb_stdout);
          fputs_filtered ("(single precision)     ", gdb_stdout);
          val_print (builtin_type_float,
                             (char *)virtual_buffer, 0, 0,
                             gdb_stdout, 0, 1, 0, Val_pretty_default);
          fprintf_filtered (gdb_stdout, "\n%sR", REGISTER_NAME (i));
          print_spaces_filtered (7 - strlen (REGISTER_NAME (i)),
                                 gdb_stdout);
          fputs_filtered ("(single precision)    ", gdb_stdout);
          val_print (builtin_type_float,
                             ((char *)virtual_buffer) + sizeof(float),
                             0, 0,
                             gdb_stdout, 0, 1, 0, Val_pretty_default);
        }
#endif
    }
  else
    {
      if (precision != unspecified_precision)
        {
          fputs_filtered (REGISTER_NAME (i), gdb_stdout);
          print_spaces_filtered (8 - strlen (REGISTER_NAME (i)), gdb_stdout);
          fputs_filtered ("(single precision)    ", gdb_stdout);
        }
      else
        fprintf_filtered (gdb_stdout, "%8.8s: ", REGISTER_NAME (i));
   
      val_print (REGISTER_VIRTUAL_TYPE(i),
                 (char *)virtual_buffer, 0, 0, gdb_stdout, 0, 1, 0,
                 Val_pretty_default);
      printf_filtered ("\n");

    }
}

/*************** new function ***********************/
static void
pa_strcat_fp_reg (i, stream, precision)
     int i;
     struct ui_file *stream;
     enum precision_type precision;
{
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];

  fputs_filtered (REGISTER_NAME (i), stream);
  print_spaces_filtered (8 - strlen (REGISTER_NAME (i)), stream);

  /* Get 32bits of data.  */
  read_relative_register_raw_bytes (i, raw_buffer);

  /* Put it in the buffer.  No conversions are ever necessary.  */
  memcpy (virtual_buffer, raw_buffer, REGISTER_RAW_SIZE (i));

  if (precision == double_precision && (i % 2) == 0)
    {

      char raw_buf[MAX_REGISTER_RAW_SIZE];

      /* Get the data in raw format for the 2nd half.  */
      read_relative_register_raw_bytes (i + 1, raw_buf);

      /* Copy it into the appropriate part of the virtual buffer.  */
      memcpy (virtual_buffer + REGISTER_RAW_SIZE (i), raw_buf, REGISTER_RAW_SIZE (i));

      val_print (builtin_type_double, virtual_buffer, 0, 0, stream, 0,
		 1, 0, Val_pretty_default);

    }
  else
    {
      val_print (REGISTER_VIRTUAL_TYPE (i), virtual_buffer, 0, 0, stream, 0,
		 1, 0, Val_pretty_default);
    }

}

#ifdef DP_SAVED_IN_OBJFILE

/* JAGae90956 - To get the value of dp register saved in the objfile (saved_dp_reg) for given PC */

CORE_ADDR
get_dpreg_for_objfile(CORE_ADDR pc)
 {
  struct objfile *obj_file;  
  char *solib_name;

 solib_name = pa64_solib_address (pc);
 
 if (solib_name)
  {
    ALL_OBJFILES(obj_file)
     {
       if(strstr(obj_file->name, solib_name))
         return obj_file->saved_dp_reg;
      }
   }
 
 return NULL;
}  

#endif



#ifdef GDB_TARGET_IS_HPPA_20W

/* analyze_stub - Return 0 if the pc value passed in is not in
   a stub of some sort.  Otherwise, return the target pc of the
   trampoline and set various return variables:

     *stub_kind_p       - what kind of trampoline is it
     *start_addr_p      - the starting address of the trampoline
     *limit_addr_p      - the first address past the end of the
   */

CORE_ADDR
analyze_stub (CORE_ADDR pc, 
              stub_type* stub_kind_p, 
              CORE_ADDR* start_addr_p, 
              CORE_ADDR* limit_addr_p)
{
  CORE_ADDR                     dp_value;
  int                           idx;
  int                           inst_a[(2 * MAX_STUB_INST) -1];
  CORE_ADDR                     read_addr;
  int                           retry_count;
  int*                          start_stub_p;
  int                           stub_size = 0;
  struct minimal_symbol *       stub_sym_p;
  int                           target_dp_offset;
  CORE_ADDR                     target_pc = 0;
  struct unwind_table_entry *   unwind_p;
  CORE_ADDR			objfile_dp_reg; /* JAGae90956 */
  static int is_dp_corrupt_warned = 0;


  assert(sizeof(int) >= INSTRUCTION_SIZE); 
  *stub_kind_p = STUB_NONE;
  *start_addr_p = pc;
  *limit_addr_p = pc;

  /* Get the unwind descriptor corresponding to PC.  A linker stub will
     not have an unwind descriptor.  A C++ thunk will. */

  unwind_p = hppa_find_unwind_entry (pc);

  /* One more santiy check, there should be a ".stub" symbol here. 
     There is also a ".text" symbol at the same place.
     */

  stub_sym_p = lookup_minimal_symbol_by_pc (pc);
  if (!stub_sym_p)
    return 0;

  if (unwind_p)
    {
      if (strncmp(SYMBOL_NAME(stub_sym_p), "__ath_", 6) != 0)
        return 0;  /* Not a C++ thunk */
    }
  else if (strcmp (SYMBOL_NAME(stub_sym_p), ".stub") != 0
	   && strcmp (SYMBOL_NAME(stub_sym_p), ".text") != 0) 
    return 0;


  /* We check for a stub starting at the address passed in, then
     we repeat the check starting at the preceding MAX_STUB_INST -1
     instructions.  We first read in MAX_STUB_INST instructions into
     the inst_a array starting at index MAX_STUB_INST -1.  For each
     repetition, we read in one more preceeding instruction.
     */

  start_stub_p = &inst_a[MAX_STUB_INST -1];
  read_addr = pc;
  for (idx = 0;  idx < MAX_STUB_INST;  idx++) 
    {
      start_stub_p[idx] = 
          read_memory_unsigned_integer (read_addr, INSTRUCTION_SIZE);
      read_addr += INSTRUCTION_SIZE;
    }

  /* We need to check for the stub starting at the initial location or
     the preceeding MAX_STUB_INST -1 instructions.
     */

  for (retry_count = 0;  retry_count < MAX_STUB_INST -1;  retry_count++)
    {
      /* Is this a shared library import stub?
           ldd OFFSET(sr0,dp),r1
           bve (r1)
           ldd OFFSET(sr0,dp),dp
         */
      /* RM: For large enough plabel tables, the space register
         deosn't have to be sr0, it seems. Generalizing to the
         following sequence:
           ldd OFFSET(srN,dp),r1
           bve (r1)
           ldd OFFSET(srN,dp),dp
         */         

      if (    (start_stub_p[0] & 0xffff000e)    == 0x53610000   
           && start_stub_p[1]                   == 0xe820d000
           && (start_stub_p[2] & 0xffff000e)    == 0x537b0000
           && unwind_p == 0)
        {
          target_dp_offset = extract_im10a(start_stub_p[0]);
          dp_value = read_register(DP_REGNUM);
          objfile_dp_reg = get_dpreg_for_objfile (pc); /* JAGae90956 DP value obtained from objfile */

          /* Compare DP value with the value obtained from objfile. If they are different, DP is
             corrupted, so throw a warning message */
          if (objfile_dp_reg && (dp_value != objfile_dp_reg))
             {
              if(!is_dp_corrupt_warned)
               {
         	   warning ("Attempting to unwind using bad DP %s", 
                               longest_local_hex_string ((LONGEST) dp_value));
                   is_dp_corrupt_warned = 1;
               }
             }
          else
          {
             target_pc = read_memory_unsigned_integer (dp_value + target_dp_offset,
                                                    sizeof (CORE_ADDR));
             stub_size = 3 * INSTRUCTION_SIZE;
             *stub_kind_p = STUB_SOLIB_CALL;
          }
          break;
        } /* end if shared library import stub */

      /* Is this a shared library import stub?
           addil OFFSET,dp,r1
           ldd OFFSET(srN,r1),r31
           bve (r31)
           ldd OFFSET(srN,r1),dp
         */         
      if (    (start_stub_p[0] & 0xffe00000) == 0x2b600000
           && (start_stub_p[1] & 0xffff000e)    == 0x503f0000   
           && start_stub_p[2]                   == 0xebe0d000
           && (start_stub_p[3] & 0xffff000e)    == 0x503b0000
           && unwind_p == 0)
        {
          target_dp_offset = extract_21(start_stub_p[0]) +
                             extract_im10a(start_stub_p[1]);
          dp_value = read_register(DP_REGNUM);
          objfile_dp_reg =  get_dpreg_for_objfile (pc); /* JAGae90956 - DP value obtained from objfile */
          /* Compare DP value with the value obtained from objfile. If they are different, DP is
             corrupted, so throw a warning message */
          if (objfile_dp_reg && (dp_value != objfile_dp_reg))
           {
             if(!is_dp_corrupt_warned)
               {
	           warning ("Attempting to unwind using bad DP %s!", 
                               longest_local_hex_string ((LONGEST) dp_value));  
		   is_dp_corrupt_warned = 1;
		}
           }
          else
            {
              target_pc = read_memory_unsigned_integer (dp_value + target_dp_offset,
                                                    sizeof (CORE_ADDR));
              stub_size = 4 * INSTRUCTION_SIZE;
              *stub_kind_p = STUB_SOLIB_CALL;
            }
          break;
        } /* end if shared library import stub */

      /* A long branch stub on PA64 is 4 instructions long. e.g:
            mfia r1
            addil OFFSET,r1
            ldo OFFSET(r1),r1
            bve,n (r1)
         */

      if (    start_stub_p[0]                   == 0x14a1
           && (start_stub_p[1] & 0xffe00000)    == 0x28200000
           && (start_stub_p[2] & 0xffff0000)    == 0x34210000
           && start_stub_p[3]                   == 0xe820d002
           && unwind_p == 0)
        {
          target_pc =   pc 
                      + extract_21 (start_stub_p[1]) 
                      + extract_14 (start_stub_p[2]);
          stub_size = 4 * INSTRUCTION_SIZE;
          *stub_kind_p = STUB_LONG_CALL;
          break;
        } /* end if shared library import stub */

#ifdef COVARIANT_SUPPORT

         /* PA64 : Find out the pc of the Co-variant caller function */
         find_pc_of_covariant_caller_func ( pc, &stub_size, 
	                                    stub_kind_p, start_stub_p, 
					    unwind_p, &target_pc, start_addr_p );

         /* PA64 : Find out the pc of the Co-variant callee function */
         find_pc_of_covariant_callee_func ( pc, &stub_size, 
	                                    stub_kind_p, start_stub_p, 
					    unwind_p, &target_pc, start_addr_p );
         if ( *stub_kind_p == STUB_CXX_ADJUST_COVARIANT_RETURN )
           {
             break;
           }
#endif

      /* Check for the various forms of C++ thunks to adjust the this pointer */

        /* 0xe8000000   b DESTINATION
           0x375a3fe1   ldo OFFSET(%r26),%r26
           */
      if (    unwind_p != 0
           && (start_stub_p[0] & 0xfc00e002) == 0xe8000000
           && (start_stub_p[1] & 0xffff0000) == 0x375a0000)
        {
          target_pc = (*start_addr_p + 8 + extract_17 (start_stub_p[0])) & ~0x3;
          stub_size = 2 * INSTRUCTION_SIZE;
          *stub_kind_p = STUB_CXX_ADJUST_THIS;
          break;
        }

        /* 0x2b5bcfff   addil L'OFFSET,%r26,%r1
           0xe8000000   b DESTINATION
           0x343a0760   ldo OFFSET(%r1),%r26
           */
      if (    unwind_p != 0
           && (start_stub_p[0] & 0xfe000000) == 0x2a000000
           && (start_stub_p[1] & 0xfc00e002) == 0xe8000000
           && (start_stub_p[2] & 0xffff0000 ) == 0x343a0000)
        {
          target_pc =   (*start_addr_p + 4 + 8 + extract_17 (start_stub_p[1])) 
                      & ~0x3;
          stub_size = 3 * INSTRUCTION_SIZE;
          *stub_kind_p = STUB_CXX_ADJUST_THIS;
          break;
        }

      /* Prepare for the next iteration if there is one */

      if (retry_count < MAX_STUB_INST -2) 
        {
          start_stub_p--;
          *start_addr_p = *start_addr_p - INSTRUCTION_SIZE;
          *start_stub_p = 
              read_memory_unsigned_integer (*start_addr_p, INSTRUCTION_SIZE);
        } /* if there is another iteration */
    } /* For each possible stub starting instruction */

    /* did we find a stub? */

    if (retry_count >= MAX_STUB_INST - 1 || *start_addr_p + stub_size < pc)
      { /* No, we did not find a stub */
        *start_addr_p = pc;
        return 0;
      }
    
    /* Yes, we found a stub */

    *limit_addr_p = *start_addr_p + stub_size;
    return target_pc;
} /* end analyze_stub */

#endif /* GDB_TARGET_IS_HPPA_20W */

#ifdef COVARIANT_SUPPORT
/* Return true if pc is in outbound or inbound outbound thunk */
int 
in_covariant_inbound_outbound_thunks (pc) 
CORE_ADDR pc;
{
  struct minimal_symbol* msym;
  int return_val = 0; 
   msym = lookup_minimal_symbol_by_pc (pc);
   if ( msym
        && strncmp ( SYMBOL_NAME( msym ), "__ath_", 6 ) == 0 ) 
     {
       return_val = 1;
     }
   return return_val;
}
#endif

/* Return one if PC is in the call path of a trampoline, else return zero.

   Note we return one for *any* call trampoline (long-call, arg-reloc), not
   just shared library trampolines (import, export).  */

int
in_solib_call_trampoline (pc, name)
     CORE_ADDR pc;
     char *name;
{
#ifdef GDB_TARGET_IS_HPPA_20W

  CORE_ADDR           limit_addr;
  CORE_ADDR           start_addr;
  stub_type           stub_kind;
  CORE_ADDR           stub_target;
#ifdef COVARIANT_SUPPORT
  if (IS_PC_IN_COVARIANT_THUNK (pc)) 
    {
      return 0;
    }
#endif
  stub_target = analyze_stub (pc, &stub_kind, &start_addr, &limit_addr);
  return (stub_target != 0);

#else
  struct minimal_symbol *minsym;
  struct unwind_table_entry *u;
  static CORE_ADDR dyncall = 0;
  static CORE_ADDR sr4export = 0;

  /* FIXME XXX - dyncall and sr4export must be initialized whenever we get a
     new exec file */

  /* First see if PC is in one of the two C-library trampolines.  */
  if (!dyncall)
    {
      minsym = lookup_minimal_symbol ("$$dyncall", NULL, NULL);
      if (minsym)
	dyncall = SYMBOL_VALUE_ADDRESS (minsym);
      else
	dyncall = -1;
    }

  if (!sr4export)
    {
      minsym = lookup_minimal_symbol ("_sr4export", NULL, NULL);
      if (minsym)
	sr4export = SYMBOL_VALUE_ADDRESS (minsym);
      else
	sr4export = -1;
    }

  if (pc == dyncall || pc == sr4export)
    return 1;

  minsym = lookup_minimal_symbol_by_pc (pc);
  if (minsym && strcmp (SYMBOL_NAME (minsym), ".stub") == 0)
    return 1;

  /* Get the unwind descriptor corresponding to PC, return zero
     if no unwind was found.  */
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return 0;

  /* If this isn't a linker stub, then return now.  */
  if (u->stub_unwind.stub_type == 0)
    return 0;

  /* By definition a long-branch stub is a call stub.  */
  if (u->stub_unwind.stub_type == LONG_BRANCH)
    return 1;

  /* RM: LONG_BRANCH_MRP stubs too.  */
  if (u->stub_unwind.stub_type == LONG_BRANCH_MRP)
    return 1;

  /* The call and return path execute the same instructions within
     an IMPORT stub!  So an IMPORT stub is both a call and return
     trampoline.  */
  if (u->stub_unwind.stub_type == IMPORT)
    return 1;

  if (u->stub_unwind.stub_type == LONG_BRANCH_SHLIB)
    return 1;

  /* Parameter relocation stubs always have a call path and may have a
     return path.  */
  if (u->stub_unwind.stub_type == PARAMETER_RELOCATION
      || u->stub_unwind.stub_type == EXPORT)
    {
      CORE_ADDR addr;

      /* Search forward from the current PC until we hit a branch
         or the end of the stub.  */
      for (addr = pc; addr <= u->region_end; addr += 4)
	{
	  unsigned long insn;

	  insn = read_memory_integer (addr, 4);

	  /* Does it look like a bl?  If so then it's the call path, if
	     we find a bv or be first, then we're on the return path.  */
	  if ((insn & 0xfc00e000) == 0xe8000000)
	    return 1;
	  else if ((insn & 0xfc00e001) == 0xe800c000
		   || (insn & 0xfc000000) == 0xe0000000)
	    return 0;
	}

      /* Should never happen.  */
      warning ("Unable to find branch in parameter relocation stub.\n");
      return 0;
    }

  /* Unknown stub type.  For now, just return zero.  */
  return 0;
#endif
}

/* Return one if PC is in the return path of a trampoline, else return zero.

   Note we return one for *any* call trampoline (long-call, arg-reloc), not
   just shared library trampolines (import, export).  */

int
in_solib_return_trampoline (pc, name)
     CORE_ADDR pc;
     char *name;
{
  struct unwind_table_entry *u;

  /* Get the unwind descriptor corresponding to PC, return zero
     if no unwind was found.  */
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return 0;

#ifndef GDB_TARGET_IS_HPPA_20W
  /* If this isn't a linker stub or it's just a long branch stub, then
     return zero.  */
  if (u->stub_unwind.stub_type == 0 || u->stub_unwind.stub_type == LONG_BRANCH)
    return 0;

  /* The call and return path execute the same instructions within
     an IMPORT stub!  So an IMPORT stub is both a call and return
     trampoline.  */
  if (u->stub_unwind.stub_type == IMPORT)
    return 1;

  /* Parameter relocation stubs always have a call path and may have a
     return path.  */
  if (u->stub_unwind.stub_type == PARAMETER_RELOCATION
      || u->stub_unwind.stub_type == EXPORT)
    {
      CORE_ADDR addr;

      /* Search forward from the current PC until we hit a branch
         or the end of the stub.  */
      for (addr = pc; addr <= u->region_end; addr += 4)
	{
	  unsigned long insn;

	  insn = read_memory_integer (addr, 4);

	  /* Does it look like a bl?  If so then it's the call path, if
	     we find a bv or be first, then we're on the return path.  */
	  if ((insn & 0xfc00e000) == 0xe8000000)
	    return 0;
	  else if ((insn & 0xfc00e001) == 0xe800c000
		   || (insn & 0xfc000000) == 0xe0000000)
	    return 1;
	}

      /* Should never happen.  */
      warning ("Unable to find branch in parameter relocation stub.\n");
      return 0;
    }
#endif

  /* Unknown stub type.  For now, just return zero.  */
  return 0;

}

#ifndef GDB_TARGET_IS_HPPA_20W
/* Figure out if PC is in a trampoline, and if so find out where
   the trampoline will jump to.  If not in a trampoline, return zero.

   Simple code examination probably is not a good idea since the code
   sequences in trampolines can also appear in user code.

   We use unwinds and information from the minimal symbol table to
   determine when we're in a trampoline.  This won't work for ELF
   (yet) since it doesn't create stub unwind entries.  Whether or
   not ELF will create stub unwinds or normal unwinds for linker
   stubs is still being debated.  [ The PA64 definition follows. ]

   This should handle simple calls through dyncall or sr4export,
   long calls, argument relocation stubs, and dyncall/sr4export
   calling an argument relocation stub.  It even handles some stubs
   used in dynamic executables.  */

/* We set cache_objfile to -1 so that it won't match zero or any valid objfile
   pointer to inticate that the cached shared library symbol values are
   invalid.

   Each shared library has a private copy of millicode, so we need to look
   up the special millicode symbols in a given shared library, e.g. $$dyncall.
 */

static struct objfile *cache_objfile = (struct objfile *) -1;
static CORE_ADDR solib_dyncall;
static CORE_ADDR solib_dyncall_external;
static CORE_ADDR solib_dyncall_external_20;
static CORE_ADDR solib_sr4export;

CORE_ADDR
skip_trampoline_code_1 (pc, name)
     CORE_ADDR pc;
     char *name;
{
  long orig_pc = pc;
  long prev_inst, curr_inst, loc;
  static CORE_ADDR dyncall = 0;
  static CORE_ADDR dyncall_external = 0;
  static CORE_ADDR dyncall_external_20 = 0;
  static CORE_ADDR sr4export = 0;
  struct minimal_symbol *msym;
  struct objfile* solib_objfile;
  struct unwind_table_entry *u;

  solib_objfile = som_solib_get_objfile_by_pc (pc);
  if (!solib_objfile)
    { /* Initialize the stuff which we initialize for a new objfile */
      cache_objfile = 0;
      solib_dyncall = -1;
      solib_dyncall_external = -1;
      solib_dyncall_external_20 = -1;
      solib_sr4export = -1;
    }
  else if (solib_objfile && solib_objfile != cache_objfile)
    {
      cache_objfile = solib_objfile;
      msym = lookup_minimal_symbol ("$$dyncall", NULL, cache_objfile);
      if (msym)
        solib_dyncall = SYMBOL_VALUE_ADDRESS (msym);
      else
        solib_dyncall = -1;

      msym = lookup_minimal_symbol ("$$dyncall_external", NULL, cache_objfile);
      if (msym)
        solib_dyncall_external = SYMBOL_VALUE_ADDRESS (msym);
      else
        solib_dyncall_external = -1;

      msym = lookup_minimal_symbol ("$$dyncall_external_20",
                                    NULL,
                                    cache_objfile);
      if (msym)
        solib_dyncall_external_20 = SYMBOL_VALUE_ADDRESS (msym);
      else
        solib_dyncall_external_20 = -1;

      msym = lookup_minimal_symbol ("_sr4export", NULL, cache_objfile);
      if (msym)
        solib_sr4export = SYMBOL_VALUE_ADDRESS (msym);
      else
        solib_sr4export = -1;
    }

  /* FIXME XXX - dyncall and sr4export must be initialized whenever we get a
     new exec file */

  if (!dyncall)
    {
      msym = lookup_minimal_symbol ("$$dyncall", NULL, NULL);
      if (msym)
	dyncall = SYMBOL_VALUE_ADDRESS (msym);
      else
	dyncall = -1;
    }

  if (!dyncall_external)
    {
      msym = lookup_minimal_symbol ("$$dyncall_external", NULL, NULL);
      if (msym)
	dyncall_external = SYMBOL_VALUE_ADDRESS (msym);
      else
	dyncall_external = -1;
    }

  if (!dyncall_external_20)
    {
      msym = lookup_minimal_symbol ("$$dyncall_external_20", NULL, NULL);
      if (msym)
        dyncall_external_20 = SYMBOL_VALUE_ADDRESS (msym);
      else
        dyncall_external_20 = -1;
    }

  if (!sr4export)
    {
      msym = lookup_minimal_symbol ("_sr4export", NULL, NULL);
      if (msym)
	sr4export = SYMBOL_VALUE_ADDRESS (msym);
      else
	sr4export = -1;
    }

  /* Addresses passed to dyncall may *NOT* be the actual address
     of the function.  So we may have to do something special.  */
  if (pc == dyncall || (solib_objfile == cache_objfile && pc == solib_dyncall))
    {
      pc = (CORE_ADDR) read_register (22);

      /* If bit 30 (counting from the left) is on, then pc is the address of
         the PLT entry for this function, not the address of the function
         itself.  Bit 31 has meaning too, but only for MPE.  */
      if (pc & 0x2)
	pc = (CORE_ADDR) read_memory_integer (pc & ~0x3, TARGET_PTR_BIT / 8);
    }
  else if (pc == dyncall_external 
           || (solib_objfile == cache_objfile && pc == solib_dyncall_external))
    {
      pc = (CORE_ADDR) read_register (22);
      pc = (CORE_ADDR) read_memory_integer (pc & ~0x3, TARGET_PTR_BIT / 8);
    }
  else if (pc == dyncall_external_20
           || (solib_objfile == cache_objfile && pc == solib_dyncall_external_20))
    {
      pc = (CORE_ADDR) read_register (22);
      pc = (CORE_ADDR) read_memory_integer (pc & ~0x3, TARGET_PTR_BIT / 8);
    }
  else if (pc == sr4export || (solib_objfile == cache_objfile && pc == sr4export))
    pc = (CORE_ADDR) (read_register (22));

  /* Get the unwind descriptor corresponding to PC, return zero
     if no unwind was found.  */
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return 0;

  /* If this isn't a linker stub, then return now.  */
  /* elz: attention here! (FIXME) because of a compiler/linker 
     error, some stubs which should have a non zero stub_unwind.stub_type 
     have unfortunately a value of zero. So this function would return here
     as if we were not in a trampoline. To fix this, we go look at the partial
     symbol information, which reports this guy as a stub.
     (FIXME): Unfortunately, we are not that lucky: it turns out that the 
     partial symbol information is also wrong sometimes. This is because 
     when it is entered (somread.c::som_symtab_read()) it can happen that
     if the type of the symbol (from the som) is Entry, and the symbol is
     in a shared library, then it can also be a trampoline.  This would
     be OK, except that I believe the way they decide if we are ina shared library
     does not work. SOOOO..., even if we have a regular function w/o trampolines
     its minimal symbol can be assigned type mst_solib_trampoline.
     Also, if we find that the symbol is a real stub, then we fix the unwind
     descriptor, and define the stub type to be EXPORT.
     Hopefully this is correct most of the times. */
  if (u->stub_unwind.stub_type == 0)
    {

/* elz: NOTE (FIXME!) once the problem with the unwind information is fixed
   we can delete all the code which appears between the lines */
/*--------------------------------------------------------------------------*/
      msym = lookup_minimal_symbol_by_pc (pc);

    if (   msym 
        && strncmp(msym->ginfo.name, "__ath_", 6) == 0
        && MSYMBOL_TYPE (msym) == mst_file_text) 
      {
        /* We may be at a C++ thunk which adjusts the this pointer.
           There are four known variants:
                  0xe81f15f5    b 0x7a81907c
                  0x375a3ff9    ldo -4(%r26),%r26
           or:
                  0x2b5bcfff    addil L'-0xa000,%r26,%r1
                  0xe81f1fc5    b 0x7ad29ba0
                  0x343a0770    ldo 0x3b8(%r1),%r26
           or:
                  0x23e13000    ldil L'0x3800,%r31
                  0xe3e024d0    be 0x268(%sr4,%r31)
                  0x375a3ff1    ldo -8(%r26),%r26
           or:
                  0x2b5bcfff    addil L'-0xa000,%r26,%r1
                  0x23e13000    ldil L'0x3800,%r31
                  0xe3e02538    be 0x29c(%sr4,%r31)
                  0x343a0770    ldo 0x3b8(%r1),%r26
           If so, update pc to the target of the branch.
           */

        long first_stub_inst, second_stub_inst, third_stub_inst,
             fourth_stub_inst;
            
        first_stub_inst = read_memory_integer(pc, 4);
        second_stub_inst = read_memory_integer(pc + 4, 4);

          /*      0xe81f15f5    b 0x7a81907c
                  0x375a3ff9    ldo -4(%r26),%r26
                  */
          if (   ((first_stub_inst & 0xfc00e002) == 0xe8000000)
              && ((second_stub_inst & 0xffff0000 ) == 0x375a0000))
            {
              pc = (pc + 8 + extract_17 (first_stub_inst)) & ~0x3;
              return pc;
            }

          third_stub_inst = read_memory_integer(pc + 8, 4);
          /*      0x2b5bcfff    addil L'-0xa000,%r26,%r1
                  0xe81f1fc5    b 0x7ad29ba0
                  0x343a0770    ldo 0x3b8(%r1),%r26
                  */
          if (   ((first_stub_inst & 0xfe000000) == 0x2a000000)
              && ((second_stub_inst & 0xfc00e002) == 0xe8000000)
              && ((third_stub_inst & 0xffff0000 ) == 0x343a0000))
            {
              pc = (pc + 4 + 8 + extract_17 (second_stub_inst)) & ~0x3;
              return pc;
            }

          /*      0x23e13000    ldil L'0x3800,%r31
                  0xe3e024d0    be 0x268(%sr4,%r31)
                  0x375a3ff1    ldo -8(%r26),%r26
                  */
          if (   ((first_stub_inst & 0xffe00000) == 0x23e00000)
              && ((second_stub_inst & 0xffe0e002) == 0xe3e02000)
              && ((third_stub_inst & 0xffff0000 ) == 0x375a0000))
            {
              pc =   (  extract_21 (first_stub_inst) 
                      + extract_17 (second_stub_inst)) 
                   & ~0x3;
              return pc;
            }
          

          fourth_stub_inst = read_memory_integer(pc + 12, 4);
          /*      0x2b5bcfff    addil L'-0xa000,%r26,%r1
                  0x23e13000    ldil L'0x3800,%r31
                  0xe3e02538    be 0x29c(%sr4,%r31)
                  0x343a0770    ldo 0x3b8(%r1),%r26
                  */
          if (   ((first_stub_inst & 0xfe000000) == 0x2a000000)
              && ((second_stub_inst & 0xffe00000) == 0x23e00000)
              && ((third_stub_inst & 0xffe0e002) == 0xe3e02000)
              && ((fourth_stub_inst & 0xffff0000 ) == 0x343a0000))
            {
              pc =   (  extract_21 (second_stub_inst) 
                      + extract_17 (third_stub_inst)) 
                   & ~0x3;
              return pc;
            }
#ifdef COVARIANT_SUPPORT

         /* PA32 : Find out the pc of the Co-variant caller & callee function */
         if (   find_pc_of_covariant_caller_func ( &pc, u ) 
             || find_pc_of_covariant_callee_func ( &pc, u ) )
           {
	        return pc;
           }
     
#endif

      } /* end of C++ "this" adjustment thunks */


      if (msym == NULL || MSYMBOL_TYPE (msym) != mst_solib_trampoline)
	return orig_pc == pc ? 0 : pc & ~0x3;

      else if (msym != NULL && MSYMBOL_TYPE (msym) == mst_solib_trampoline)
	{
	  struct objfile *objfile;
	  struct minimal_symbol *msymbol;
	  int function_found = 0;

	  /* go look if there is another minimal symbol with the same name as 
	     this one, but with type mst_text. This would happen if the msym
	     is an actual trampoline, in which case there would be another
	     symbol with the same name corresponding to the real function */

	  ALL_MSYMBOLS (objfile, msymbol)
	  {
	    if (MSYMBOL_TYPE (msymbol) == mst_text
		&& STREQ (SYMBOL_NAME (msymbol) , SYMBOL_NAME (msym))
		&& (SYMBOL_VALUE_ADDRESS (msymbol) != SYMBOL_VALUE_ADDRESS (msym)))
	      {
		function_found = 1;
		goto found;
	      }
	  }

	found:
	  if (function_found)
	    /* the type of msym is correct (mst_solib_trampoline), but
	       the unwind info is wrong, so set it to the correct value */
	    u->stub_unwind.stub_type = EXPORT;
	  else
	    /* the stub type info in the unwind is correct (this is not a
	       trampoline), but the msym type information is wrong, it
	       should be mst_text. So we need to fix the msym, and also
	       get out of this function */
	    {
	      MSYMBOL_TYPE (msym) = mst_text;
	      return orig_pc == pc ? 0 : pc & ~0x3;
	    }
	}

/*--------------------------------------------------------------------------*/
    }

  /* It's a stub.  Search for a branch and figure out where it goes.
     Note we have to handle multi insn branch sequences like ldil;ble.
     Most (all?) other branches can be determined by examining the contents
     of certain registers and the stack.  */

  loc = pc;
  curr_inst = 0;
  prev_inst = 0;
  while (1)
    {
      /* Make sure we haven't walked outside the range of this stub.  */
      if (u != hppa_find_unwind_entry (loc))
	{
	  /* 09/13/01 coulter
	     The PC may not be in a linker stub at all, so we shouldn't
	     print the warning message.  Comment it out.
	  warning ("Unable to find branch in linker stub");
	  */
	  return orig_pc == pc ? 0 : pc & ~0x3;
	}

      prev_inst = curr_inst;
      curr_inst = read_memory_integer (loc, 4);

      /* Does it look like a branch external using %r1?  Then it's the
         branch from the stub to the actual function.  */
      if ((curr_inst & 0xffe0e000) == 0xe0202000)
	{
	  /* Yup.  See if the previous instruction loaded
	     a value into %r1.  If so compute and return the jump address.  */
	  if ((prev_inst & 0xffe00000) == 0x20200000)
            {
              CORE_ADDR tmp_pc;

              /* ??? RM: long branch stubs may branch to one of the dyncall
                 varieties, need to skip that too */
              tmp_pc = (extract_21 (prev_inst) + extract_17 (curr_inst)) & ~0x3;
              if (tmp_pc == dyncall_external_20)
                {
                  tmp_pc = (CORE_ADDR) read_register (22);
                  tmp_pc = (CORE_ADDR) read_memory_integer (tmp_pc & ~0x3, sizeof(CORE_ADDR));
                }
              return tmp_pc;
            }
	  else
	    {
	      warning ("Unable to find ldil X,%%r1 before ble Y(%%sr4,%%r1).");
	      return orig_pc == pc ? 0 : pc & ~0x3;
	    }
	}

      /* Does it look like a be 0(sr0,%r21)? OR 
         Does it look like a be, n 0(sr0,%r21)? OR 
         Does it look like a bve (r21)? (this is on PA2.0)
         Does it look like a bve, n(r21)? (this is also on PA2.0)
         That's the branch from an
         import stub to an export stub.

         It is impossible to determine the target of the branch via
         simple examination of instructions and/or data (consider
         that the address in the plabel may be the address of the
         bind-on-reference routine in the dynamic loader).

         So we have try an alternative approach.

         Get the name of the symbol at our current location; it should
         be a stub symbol with the same name as the symbol in the
         shared library.

         Then lookup a minimal symbol with the same name; we should
         get the minimal symbol for the target routine in the shared
         library as those take precedence of import/export stubs.  */
      if ((curr_inst == 0xe2a00000) ||
	  (curr_inst == 0xe2a00002) ||
	  (curr_inst == 0xeaa0d000) ||
	  (curr_inst == 0xeaa0d002))
	{
	  struct minimal_symbol *stubsym, *libsym;

	  stubsym = lookup_minimal_symbol_by_pc (loc);
	  if (stubsym == NULL)
	    {
              warning ("Unable to find symbol for 0x%s",
                       longest_raw_hex_string ((LONGEST) loc));
	      return orig_pc == pc ? 0 : pc & ~0x3;
	    }

	  libsym = lookup_minimal_symbol (SYMBOL_NAME (stubsym), NULL, NULL);
	  if (libsym == NULL)
	    {
	      warning ("Unable to find library symbol for %s\n",
		       SYMBOL_NAME (stubsym));
	      return orig_pc == pc ? 0 : pc & ~0x3;
	    }

          if (libsym->type == mst_solib_trampoline)
            {
              /* RM: Oh, oh! */
              /* RM: This can happen, for example, when we step into a
                 function in a filter library for which the
                 corresponding implementation library has not yet been
                 loaded. We have no information about the target of
                 the stub yet. */
              return -1;
            }

	  return SYMBOL_VALUE (libsym);
	}

      /* Does it look like bl X,%rp or bl X,%r0?  Another way to do a
         branch from the stub to the actual function.  */
      /*elz */
      else if ((curr_inst & 0xffe0e000) == 0xe8400000
	       || (curr_inst & 0xffe0e000) == 0xe8000000
	       || (curr_inst & 0xffe0e000) == 0xe800A000)
	return (loc + extract_17 (curr_inst) + 8) & ~0x3;

      /* RM: We don't seem to handle a particular kind of shared
       * library long branch stub. This stub has been spotted in
       * Dassault's CATIA v5 application, so it certainly exists. I
       * have no way of testing this, hope it works.
       *  
       * The stub looks like this:
       *   bl .+8, %r1
       *   addil OFFSET,r1
       *   ldo OFFSET(r1),r1
       *   bv,n (r1)
       */
      /* Did we find the bv,n (r1) instruction? */
      else if (curr_inst ==  0xe820c002)
        {
          /* examine the previous instructions */
          long first_stub_inst, second_stub_inst,
               third_stub_inst, fourth_stub_inst;
          
          first_stub_inst = read_memory_integer(loc-12, 4);
          second_stub_inst = read_memory_integer(loc-8, 4);
          third_stub_inst = prev_inst;
          
          if ((first_stub_inst & 0xffe0e000) == 0xe8200000
              && (second_stub_inst & 0xffe00000) == 0x28200000
              && (third_stub_inst & 0xffff0000) == 0x34210000)
            {
              return (loc - 4  /* bl target */
                      + extract_21(second_stub_inst)
                      + extract_14(third_stub_inst));
            } /* end if shared library long branch stub */
        }
      
      /* Does it look like bv (rp)?   Note this depends on the
         current stack pointer being the same as the stack
         pointer in the stub itself!  This is a branch on from the
         stub back to the original caller.  */
      /*else if ((curr_inst & 0xffe0e000) == 0xe840c000) */
      else if ((curr_inst & 0xffe0f000) == 0xe840c000)
	{
	  /* Yup.  See if the previous instruction loaded
	     rp from sp - 8.  */
          /*   0xfd11082 == LDWS -8(r30),r2 */
          if (prev_inst == 0x4bc23ff1 || prev_inst == 0xfd11082)
	    return (read_memory_integer
		    (read_register (SP_REGNUM) - 8, TARGET_PTR_BIT / 8)) & ~0x3;
	  else
	    {
	      warning ("Unable to find restore of %%rp before bv (%%rp).");
	      return orig_pc == pc ? 0 : pc & ~0x3;
	    }
	}

      /* elz: added this case to capture the new instruction
         at the end of the return part of an export stub used by
         the PA2.0: BVE, n (rp) */
      else if ((curr_inst & 0xffe0f000) == 0xe840d000)
	{
	  return (read_memory_integer
		  (read_register (SP_REGNUM) - 24, TARGET_PTR_BIT / 8)) & ~0x3;
	}

      /* What about be,n 0(sr0,%rp)?  It's just another way we return to
         the original caller from the stub.  Used in dynamic executables.  */
      else if (curr_inst == 0xe0400002)
	{
	  /* The value we jump to is sitting in sp - 24.  But that's
	     loaded several instructions before the be instruction.
	     I guess we could check for the previous instruction being
	     mtsp %r1,%sr0 if we want to do sanity checking.  */
	  return (read_memory_integer
		  (read_register (SP_REGNUM) - 24, TARGET_PTR_BIT / 8)) & ~0x3;
	}

      /* Haven't found the branch yet, but we're still in the stub.
         Keep looking.  */
      loc += 4;
    }
}

CORE_ADDR
skip_trampoline_code (pc, name)
     CORE_ADDR pc;
     char *name;
{
    CORE_ADDR target;
    CORE_ADDR real_target;
    struct minimal_symbol * m;
    target = skip_trampoline_code_1 (pc, name);
    if (!target)
        return 0;
    
    m = lookup_minimal_symbol_by_pc (target);
    if (m)
      {
        char * name;
        name = SYMBOL_NAME (m);
        if (name && !strncmp (name, "$$dyncall", 9))
          return skip_trampoline_code_1 (target, 0);
      }
    return target;
}

#else

/* skip_trampoline_code - PA64 definition.  See comment block above for
   general comments.  Return 0 if we are not in a trampoline, otherwise
   return the target pc address.

   We look for the following kinds of stubs:
     Shared library import stub:
       ldd OFFSET(sr0,dp),r1
       bve (r1)
       ldd OFFSET(sr0,dp),dp

     Long branch stubs:
        mfia r1
        addil OFFSET,r1
        ldo OFFSET(r1),r1
        bve,n (r1)
   */

CORE_ADDR
skip_trampoline_code (pc, name)
     CORE_ADDR pc;
     char *name;
{
    CORE_ADDR           limit_addr;
    CORE_ADDR           start_addr;
    stub_type           stub_kind;
    CORE_ADDR           last_target;
    CORE_ADDR           new_target;

    last_target = 0;
    new_target = analyze_stub (pc, &stub_kind, &start_addr, &limit_addr);

    /* A C++ thunk may go to a long branch stub, etc.  Deal with chains 
       of stubs.
       */
    while (new_target)
      {
        last_target = new_target;
        new_target = analyze_stub (last_target, 
                                   &stub_kind, 
                                   &start_addr, 
                                   &limit_addr);
      }
    return last_target;
}
#endif /* GDB_TARGET_IS_HPPA_20W - define PA64 find_solib_trampoline_targe */

/* For the given instruction (INST), return any adjustment it makes
   to the stack pointer or zero for no adjustment. 

   This only handles instructions commonly found in prologues.  */

static int
prologue_inst_adjust_sp (inst)
     unsigned long inst;
{
  /* This must persist across calls.  */
  static int save_high21;

  /* The most common way to perform a stack adjustment ldo X(sp),sp */
  if ((inst & 0xffffc000) == 0x37de0000)
    return extract_14 (inst);

  /* stwm X,D(sp) */
  if ((inst & 0xffe00000) == 0x6fc00000)
    return extract_14 (inst);

  /* RM: STD,MA instruction used in PA 64*/
  if ((inst & 0xffe0000e) == 0x73c00008)
    {
        return extract_im10a(inst);
    }

  /* RM: epilogues use ldw */
  /* ldw X,D(sp) */
  if ((inst & 0xffe00000) == 0x4fc00000)
    return extract_14 (inst);

  /* addil high21,%r1; ldo low11,(%r1),%r30)
     save high bits in save_high21 for later use.  */
  if ((inst & 0xffe00000) == 0x28200000)
    {
      save_high21 = extract_21 (inst);
      return 0;
    }

  if ((inst & 0xffff0000) == 0x343e0000)
    return save_high21 + extract_14 (inst);

  /* fstws as used by the HP compilers.  */
  if ((inst & 0xffffffe0) == 0x2fd01220)
    return extract_5_load (inst);

  /* RM: fldd in epilogues */
  if ((inst & 0xffffffe0) == 0x2fd13020)
    return extract_5_load (inst);
  
  /* No adjustment.  */
  return 0;
}

/* Return nonzero if INST is a branch of some kind, else return zero.  */

static int
is_branch (inst)
     unsigned long inst;
{
  switch (inst >> 26)
    {
    case 0x20:
    case 0x21:
    case 0x22:
    case 0x23:
    case 0x27:
    case 0x28:
    case 0x29:
    case 0x2a:
    case 0x2b:
    case 0x2f:
    case 0x30:
    case 0x31:
    case 0x32:
    case 0x33:
    case 0x38:
    case 0x39:
    case 0x3a:
    case 0x3b:
      return 1;

    default:
      return 0;
    }
}

/* Return the register number for a GR which is saved by INST or
   zero if INST does not save a GR.  */

static int
inst_saves_gr (inst)
     unsigned long inst;
{
  /* JYG: MERGE FIXME: we don't have all mnemonics matched.  Why? */

  /* Does it look like a stw / stwm ?  */
  if ((inst >> 26) == 0x1a
      || (inst >> 26) == 0x1b
      || ((inst >> 26) == 0x1f && ((inst >> 1) & 0x3) == 0x2)
      || ((inst >> 26) == 0x03 && ((inst >> 6) & 0xf) == 0xa))
    return extract_5R_store (inst);

  /* Does it look like a std?  */
  if (((inst >> 26) == 0x1c && ((inst >> 1) & 0x1) == 0x0)
      || ((inst >> 26) == 0x03 && ((inst >> 6) & 0xf) == 0xb))
    return extract_5R_store (inst);

  /* Does it look like sth or stb?  HPC versions 9.0 and later use these
     too.  */
  if ((inst >> 26) == 0x19
      || (inst >> 26) == 0x18
      || ((inst >> 26) == 0x3
	  && ((((inst >> 6) & 0xf) == 0x8 || (inst >> 6) & 0xf) == 0x9)))
    return extract_5R_store (inst);

  return 0;
}

/* Return the register number for a FR which is saved by INST or
   zero it INST does not save a FR.

   Note we only care about full 64bit register stores (that's the only
   kind of stores the prologue will use).

   FIXME: What about argument stores with the HP compiler in ANSI mode? */

static int
inst_saves_fr (inst)
     unsigned long inst;
{
  /* JYG: MERGE FIXME: we don't have all mnemonics matched.  Why? */
  /* is this an FSTD ? */
  if ((inst & 0xfc00dfc0) == 0x2c001200)
    return extract_5r_store (inst);
  if ((inst & 0xfc000002) == 0x70000002)
    return extract_5R_store (inst);
  /* is this an FSTW ? */
  if ((inst & 0xfc00df80) == 0x24001200)
    return extract_5r_store (inst);
  if ((inst & 0xfc000002) == 0x7c000000)
    return extract_5R_store (inst);
  return 0;
}

/* Advance PC across any function entry prologue instructions
   to reach some "real" code. 

   Use information in the unwind table to determine what exactly should
   be in the prologue.  */


CORE_ADDR
skip_prologue_hard_way_1 (pc)
     CORE_ADDR pc;
{
  char buf[4];
  CORE_ADDR orig_pc = pc;
  CORE_ADDR fn_begin;
  unsigned long inst, stack_remaining, save_gr, save_fr, save_rp, save_sp,
    copy_sp;
  unsigned long args_stored, status, i, restart_gr, restart_fr;
  struct unwind_table_entry *u;

  status = target_read_memory (pc, buf, 4);
  inst = extract_unsigned_integer (buf, 4);
#ifdef GDB_TARGET_IS_HPPA_20W
  if (inst & 0xffe00000 == 0x23600000)
    return pc;
#else
  if (inst & 0xffe00000 == 0x22600000)
    return pc;
#endif

  if (inst & 0xffe00000 == 0x22c00000)
    return pc;

  restart_gr = 0;
  restart_fr = 0;

restart:
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return pc;

  /* If we are not at the beginning of a function, then return now. */
  find_pc_partial_function(pc, 0, &fn_begin, 0);
  if (pc != fn_begin)
    return pc;

  /* Skip NOP. */
  while (inst == 0x08000240)
    {
      pc += 4;
      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);
    }

  /* This is how much of a frame adjustment we need to account for.  */
  stack_remaining = u->Total_frame_size << 3;

  /* Magic register saves we want to know about.  */
  save_rp = u->Save_RP;
  save_sp = u->Save_SP;
  copy_sp =
#ifndef GDB_TARGET_IS_HPPA_20W
    u->Pseudo_SP_Set;
#else
    u->alloca_frame;
#endif  
    

  /* An indication that args may be stored into the stack.  Unfortunately
     the HPUX compilers tend to set this in cases where no args were
     stored too!.  */
  args_stored = 1;

  /* Turn the Entry_GR field into a bitmask.  */
  save_gr = 0;
  for (i = 3; i < u->Entry_GR + 3; i++)
    {
      /* Frame pointer gets saved into a special location.  */
      if (u->Save_SP && i == FP_REGNUM)
	continue;

      save_gr |= (1 << i);
    }
  save_gr &= ~restart_gr;

  /* Turn the Entry_FR field into a bitmask too.  */
  save_fr = 0;
  for (i = 12; i < u->Entry_FR + 12; i++)
    save_fr |= (1 << i);
  save_fr &= ~restart_fr;

  /* Loop until we find everything of interest or hit a branch.

     For unoptimized GCC code and for any HP CC code this will never ever
     examine any user instructions.

     For optimzied GCC code we're faced with problems.  GCC will schedule
     its prologue and make prologue instructions available for delay slot
     filling.  The end result is user code gets mixed in with the prologue
     and a prologue instruction may be in the delay slot of the first branch
     or call.

     Some unexpected things are expected with debugging optimized code, so
     we allow this routine to walk past user instructions in optimized
     GCC code.  */
  while (save_gr || save_fr || save_rp || save_sp || stack_remaining > 0
         || copy_sp || args_stored)
    {
      unsigned int reg_num;
      unsigned long old_stack_remaining, old_save_gr, old_save_fr;
      unsigned long old_save_rp, old_save_sp, old_copy_sp, next_inst;

      /* Save copies of all the triggers so we can compare them later
         (only for HPC).  */
      old_save_gr = save_gr;
      old_save_fr = save_fr;
      old_save_rp = save_rp;
      old_save_sp = save_sp;
      old_copy_sp = copy_sp;
      old_stack_remaining = stack_remaining;

      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);

      /* Yow! */
      if (status != 0)
	return pc;

      /* Note the interesting effects of this instruction.  */
      stack_remaining -= prologue_inst_adjust_sp (inst);

      /* There are limited ways to store the return pointer into the
	 stack.  */
      if (inst == 0x6bc23fd9 || inst == 0x0fc212c1 || inst_saves_gr(inst) == 2)
	save_rp = 0;

      /* RM: If we have an alloca frame, is this the instruction that
         copies sp? */
      if (copy_sp &&
          ((u->Large_frame && (inst == 0x37c40000)) ||
           (!u->Large_frame && (inst == 0x37c30000))))
        copy_sp = 0;

      /* These are the only ways we save SP into the stack.  At this time
         the HP compilers never bother to save SP into the stack.  */
      if ((inst & 0xffffc000) == 0x6fc10000
	  || (inst & 0xffffc00c) == 0x73c10008)
	save_sp = 0;

      /* Are we loading some register with an offset from the argument
         pointer?  */
      if ((inst & 0xffe00000) == 0x37a00000
	  || (inst & 0xffffffe0) == 0x081d0240)
	{
	  pc += 4;
	  continue;
	}

      /* Account for general and floating-point register saves.  */
      reg_num = inst_saves_gr (inst);
      save_gr &= ~(1 << reg_num);

      /* Ugh.  Also account for argument stores into the stack.
         Unfortunately args_stored only tells us that some arguments
         where stored into the stack.  Not how many or what kind!

         This is a kludge as on the HP compiler sets this bit and it
         never does prologue scheduling.  So once we see one, skip past
         all of them.   We have similar code for the fp arg stores below.

         FIXME.  Can still die if we have a mix of GR and FR argument
         stores!  */
      if (reg_num >= (TARGET_PTR_BIT == 64 ? 19 : 23) && reg_num <= 26)
	{
	  while (reg_num >= (TARGET_PTR_BIT == 64 ? 19 : 23) && reg_num <= 26)
	    {
	      pc += 4;
	      status = target_read_memory (pc, buf, 4);
	      inst = extract_unsigned_integer (buf, 4);
	      if (status != 0)
		return pc;
	      reg_num = inst_saves_gr (inst);
	    }
	  args_stored = 0;
	  continue;
	}

      reg_num = inst_saves_fr (inst);
      save_fr &= ~(1 << reg_num);

      status = target_read_memory (pc + 4, buf, 4);
      next_inst = extract_unsigned_integer (buf, 4);

      /* Yow! */
      if (status != 0)
	return pc;

      /* We've got to be read to handle the ldo before the fp register
         save.  */
      if ((inst & 0xfc000000) == 0x34000000
	  && inst_saves_fr (next_inst) >= 4
	  && inst_saves_fr (next_inst) <= (TARGET_PTR_BIT == 64 ? 11 : 7))
	{
	  /* So we drop into the code below in a reasonable state.  */
	  reg_num = inst_saves_fr (next_inst);
	  pc -= 4;
	}

      /* Ugh.  Also account for argument stores into the stack.
         This is a kludge as on the HP compiler sets this bit and it
         never does prologue scheduling.  So once we see one, skip past
         all of them.  */
      if (reg_num >= 4 && reg_num <= (TARGET_PTR_BIT == 64 ? 11 : 7))
	{
	  while (reg_num >= 4 && reg_num <= (TARGET_PTR_BIT == 64 ? 11 : 7))
	    {
	      pc += 8;
	      status = target_read_memory (pc, buf, 4);
	      inst = extract_unsigned_integer (buf, 4);
	      if (status != 0)
		return pc;
	      if ((inst & 0xfc000000) != 0x34000000)
		break;
	      status = target_read_memory (pc + 4, buf, 4);
	      next_inst = extract_unsigned_integer (buf, 4);
	      if (status != 0)
		return pc;
	      reg_num = inst_saves_fr (next_inst);
	    }
	  args_stored = 0;
	  continue;
	}

      /* Quit if we hit any kind of branch.  This can happen if a prologue
         instruction is in the delay slot of the first call/branch.  */
      if (is_branch (inst))
	break;

      /* What a crock.  The HP compilers set args_stored even if no
         arguments were stored into the stack (boo hiss).  This could
         cause this code to then skip a bunch of user insns (up to the
         first branch).

         To combat this we try to identify when args_stored was bogusly
         set and clear it.   We only do this when args_stored is nonzero,
         all other resources are accounted for, and nothing changed on
         this pass.  */
      if (args_stored
       && !(save_gr || save_fr || save_rp || save_sp || stack_remaining > 0)
	  && old_save_gr == save_gr && old_save_fr == save_fr
	  && old_save_rp == save_rp && old_save_sp == save_sp
          && old_copy_sp == copy_sp
	  && old_stack_remaining == stack_remaining)
#ifdef GDB_TARGET_IS_HPPA_20W
        /* RM: On PA64, the prologue contains an instruction to copy
         * the ap to one of r3, r4, or r5. Don't skip out on seeing
         * this instruction
         */
        if ((inst & 0xfff8ffff) != 0x37a00000)
#endif
	  break;

      /* Bump the PC.  */
      pc += 4;
    }

  /* We've got a tenative location for the end of the prologue.  However
     because of limitations in the unwind descriptor mechanism we may
     have went too far into user code looking for the save of a register
     that does not exist.  So, if there registers we expected to be saved
     but never were, mask them out and restart.

     This should only happen in optimized code, and should be very rare.  */
  if (save_gr || (save_fr && !(restart_fr || restart_gr)))
    {
      pc = orig_pc;
      restart_gr = save_gr;
      restart_fr = save_fr;
      goto restart;
    }

  return pc;
}

#define PROLOGUE_CACHE_SIZE  64
#define EPILOGUE_CACHE_SIZE  64

struct prologue_cache {   /* sorted on an MRU basis */
    CORE_ADDR pc;
    CORE_ADDR pc_past_prologue;
} prologue_cache[PROLOGUE_CACHE_SIZE];

struct epilogue_cache {   /* sorted on an MRU basis */
    CORE_ADDR pc;
    CORE_ADDR pc_past_epilogue;
} epilogue_cache[EPILOGUE_CACHE_SIZE];

hppa_flush_prologue_cache ()
{
  int i;
  
  for (i=0; i < PROLOGUE_CACHE_SIZE; i++)
    prologue_cache[i].pc = prologue_cache[i].pc_past_prologue = 0;
}

hppa_flush_epilogue_cache ()
{
  int i;
  
  for (i=0; i < EPILOGUE_CACHE_SIZE; i++)
    epilogue_cache[i].pc = epilogue_cache[i].pc_past_epilogue = 0;
}

CORE_ADDR
skip_prologue_hard_way (pc)
     CORE_ADDR pc;
{

  struct prologue_cache this, last;
  int i;

  if (prologue_cache[0].pc == pc)
    return prologue_cache[0].pc_past_prologue;

  /* not so lucky ... */

  last = prologue_cache[0];

  /* Do we have the desired info in cache ?  As we scan the list of
     entries left to right, looking for the pc of interest, we will
     bubble the entries away from the beginning i.e., the "most recently
     used" slot. This would create a vacuum in the very first slot. 
     When we find the pc we are looking for, we will move that entry to
     this slot. Alternately, if the entry is not in the cache and we
     have to determine it the hard way, we will use this slot as the
     destination. This guarentees that the list is sorted on an MRU
     basis at all times.
  */

  for (i=1; i < PROLOGUE_CACHE_SIZE; i++)
    {
      this = prologue_cache[i];
      prologue_cache[i] = last;

      if (this.pc == pc)
        {
          prologue_cache[0] = this;
          return this.pc_past_prologue;
        }

      last = this;
    }

  prologue_cache[0].pc = pc;
  prologue_cache[0].pc_past_prologue = skip_prologue_hard_way_1 (pc);
    
  return prologue_cache[0].pc_past_prologue;
}

/* RM: ??? FIXME: This procedure only finds where the stack decrement
   for a procedure is done. No other epilogue instructions are
   identified */
CORE_ADDR
skip_epilogue_hard_way_1 (pc)
     CORE_ADDR pc;
{
  char buf[4];
  unsigned long inst, stack_remaining;
  struct unwind_table_entry *u;
  unsigned long status;
  CORE_ADDR orig_pc = pc;
  
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return pc;

  /*   If we are not at the end of an unwind region, then return now. */ 
  if ((u->Region_description & 0x1) || (pc != u->region_end))
    return pc;

  /* This is how much of a frame adjustment we need to account for.  */
  stack_remaining = u->Total_frame_size << 3;

  /* Loop until we find where the stack decrement is */
  while (stack_remaining > 0)
    {
      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);
       
      /* Yow! */
      if (status != 0)
        return pc;

      /* Note the interesting effects of this instruction.  */
      stack_remaining += prologue_inst_adjust_sp (inst);

      /* RM: rp is restored before the epilogue begins */
      if ((inst & 0xffffc000 == 0x4bc20000) ||
          (inst & 0xffff0002 == 0x53c20000))
        break;
      
      /* Bump the PC.  */
      pc -= 4;

      /* RM: If we went through the whole region, give up */
      if (pc < u->region_start)
        return orig_pc;
    }

  return pc+4;
}

CORE_ADDR
skip_epilogue_hard_way (pc)
     CORE_ADDR pc;
{

  struct epilogue_cache this, last;
  int i;

  if (epilogue_cache[0].pc == pc)
    return epilogue_cache[0].pc_past_epilogue;

  /* not so lucky ... */

  last = epilogue_cache[0];

  /* Do we have the desired info in cache ?  As we scan the list of
     entries left to right, looking for the pc of interest, we will
     bubble the entries away from the beginning i.e., the "most recently
     used" slot. This would create a vacuum in the very first slot. 
     When we find the pc we are looking for, we will move that entry to
     this slot. Alternately, if the entry is not in the cache and we
     have to determine it the hard way, we will use this slot as the
     destination. This guarentees that the list is sorted on an MRU
     basis at all times.
  */

  for (i=1; i < EPILOGUE_CACHE_SIZE; i++)
    {
      this = epilogue_cache[i];
      epilogue_cache[i] = last;

      if (this.pc == pc)
        {
          epilogue_cache[0] = this;
          return this.pc_past_epilogue;
        }

      last = this;
    }

  epilogue_cache[0].pc = pc;
  epilogue_cache[0].pc_past_epilogue = skip_epilogue_hard_way_1 (pc);
    
  return epilogue_cache[0].pc_past_epilogue;
}

/* Return the address of the PC after the last prologue instruction if
   we can determine it from the debug symbols.  Else return zero.  */

 int parse_args= 0; /* JAGaf60171 */

static CORE_ADDR
after_prologue (pc)
     CORE_ADDR pc;
{
  struct symtab_and_line sal;
  CORE_ADDR func_addr, func_end, start_pc;
  struct symbol *f;
  CORE_ADDR savepc=pc,newpc;
  /* If we can not find the symbol in the partial symbol table, then
     there is no hope we can determine the function's start address
     with this code.  */
  if (!find_pc_partial_function (pc, NULL, &func_addr, &func_end))
    return 0;

  f = find_pc_function (pc);
  /* MERGEBUG: are the two lines below correct? */
  if (!f)
    return 0;			/* no debug info, do it the hard way! */

  sal = find_pc_line (pc, 0);

  /* There are only two cases to consider.  First, the end of the source line
     is within the function bounds.  In that case we return the end of the
     source line.  Second is the end of the source line extends beyond the
     bounds of the current function.  We need to use the slow code to
     examine instructions in that case. 

     Anything else is simply a bug elsewhere.  Fixing it here is absolutely
     the wrong thing to do.  In fact, it should be entirely possible for this
     function to always return zero since the slow instruction scanning code
     is supposed to *always* work.  If it does not, then it is a bug.  */
  if (sal.end < func_end)
    {
/* *INDENT-OFF */
      /* this happens when the function has no prologue, because the way 
	 find_pc_line works: elz. Note: this may not be a very good
	 way to decide whether a function has a prologue or not, but
	 it is the best I can do with the info available
	 Also, this will work for functions like: int f()
	                                          {
                                                    return 2;
                                                  }
	 I.e. the bp will be inserted at the first open brace.
	 For functions where the body is only one line written like this:
	                                          int f()
                                                  { return 2; }
         this will make the breakpoint to be at the last brace, after the body
         has been executed already. What's the point of stepping through a
	 function without any variables anyway??  */
      /* JAGaf60171: For the function like this, 
						  int f()
						  { return 2;
						  }
	 the breakpoint is planted at close brace, which is not correct.
	 Now, the bp is planted at the first instruction after the nop(s).
	 This also changes the behaviour for the function which has only 
	 one line body as above. The bp will be set at the first valid
	 instruction of the line instead of at the last brace. */
/* *INDENT-ON */
    if ((SYMBOL_LINE(f) > 0) && (SYMBOL_LINE(f) < sal.line))
     return pc; /*no adjusment will be made*/
    find_exact_line_pc (sal.symtab, sal.line, &start_pc); 
    if ((current_language->la_language == language_c)
         && (SYMBOL_LINE(f) == sal.line)
         && (start_pc && (start_pc < pc)))
     {
       char buf[4];
       unsigned long status, inst, reg_num,next_inst;
         status = target_read_memory (pc, buf, 4);
         inst = extract_unsigned_integer (buf, 4);
         reg_num = inst_saves_gr (inst);
    
      /* Skip NOP. */
        while (inst != 0x08000240)
        {
    	 if(pc<func_end)
     	 {
      		pc += 4;
      		status = target_read_memory (pc, buf, 4);
      		inst = extract_unsigned_integer (buf, 4);
      		reg_num = inst_saves_gr (inst);
     	 }
    	 else
         {
     		if(pc==func_end)
     		  pc=savepc;
     		else
     		  pc=sal.end;
   	        break;
    	 }
   	}
    
    	if(inst == 0x08000240)
     	{
   		if(pc<func_end && pc<sal.end)
     		{
     		  pc+=4;
    		}
    		else
    		{
    		  if(pc<func_end || pc<sal.end)
     		    pc=sal.end;
    		  else
    		    pc=savepc;
    		}
   	}
        status = target_read_memory (pc, buf, 4);
        inst = extract_unsigned_integer (buf, 4);
        reg_num = inst_saves_gr (inst);

      /* To skip the argument store instruction if the function is like
         int f (arg) {return 0}; so that conditions can be evaluated
         before the breakpoint for conditional breakpoints. */
      
       if (reg_num >= (TARGET_PTR_BIT == 64 ? 19 : 23) && reg_num <= 26)
       {
        while (reg_num >= (TARGET_PTR_BIT == 64 ? 19 : 23) && reg_num <= 26)
        {
          pc += 4;
          status = target_read_memory (pc, buf, 4);
          inst = extract_unsigned_integer (buf, 4);
          if (status != 0)
            return pc;
          reg_num = inst_saves_gr (inst);
        }
        parse_args = 1;
      }
      else
      {
       parse_args = 0;
      }

      if(parse_args==0)
      {
        reg_num = inst_saves_fr (inst);
  
       if (reg_num >= 4 && reg_num <= (TARGET_PTR_BIT == 64 ? 11 : 7))
        {
          while (reg_num >= 4 && reg_num <= (TARGET_PTR_BIT == 64 ? 11 : 7))
            {
              pc += 8;
              status = target_read_memory (pc, buf, 4);
              inst = extract_unsigned_integer (buf, 4);
              if (status != 0)
                return pc;
             /* if ((inst & 0xfc000000) != 0x34000000)
                break; */
              status = target_read_memory (pc + 4, buf, 4);
              next_inst = extract_unsigned_integer (buf, 4);
              if (status != 0)
                return pc;
              reg_num = inst_saves_fr (next_inst);
            }
           parse_args=1;
        }
       else
         parse_args=0;
      }

     if(parse_args!=0)
	    return pc;
     if(pc<=sal.end)  
  	    return pc;
     else
   	    return sal.end;
    }
    else
   	    return sal.end;  
   }
  else
  /* The line after the prologue is after the end of the function.  In this
     case, put the end of the prologue is the beginning of the function.  */
  /* This should happen only when the function is prologueless and has no
     code in it. For instance void dumb(){} Note: this kind of function
     is  used quite a lot in the test system */
    return pc;
}

/* To skip prologues, I use this predicate.  Returns either PC itself
   if the code at PC does not look like a function prologue; otherwise
   returns an address that (if we're lucky) follows the prologue.  If
   LENIENT, then we must skip everything which is involved in setting
   up the frame (it's OK to skip more, just so long as we don't skip
   anything which might clobber the registers which are being saved.
   Currently we must not skip more on the alpha, but we might the lenient
   stuff some day.  */

CORE_ADDR
hppa_skip_prologue (pc)
     CORE_ADDR pc;
{
  unsigned long inst, status;
  int offset;
  CORE_ADDR post_prologue_pc;
  char buf[4];

  /* JYG: PURIFY FIXME: maybe the actual routine behind target_read_memory
     should be fixed to eliminate this bogus purify UMR error once and
     for all (seems like purify is unable to ascertain whether buf is
     touched or not by target_read_memory). */
  /* BEGIN PURIFY */
  memset (buf, 0, 4);
  /* END PURIFY */

    /* Skip NOP. */
    while (1)
      {
        status = target_read_memory (pc, buf, 4);
        inst = extract_unsigned_integer (buf, 4);
        if (inst != 0x08000240)
          break;
        pc += 4;
      }

  /* See if we can determine the end of the prologue via the symbol table.
     If so, then return either PC, or the PC after the prologue, whichever
     is greater.  */

  post_prologue_pc = after_prologue (pc);

  /* If after_prologue returned a useful address, then use it.  Else
     fall back on the instruction skipping code.

     Some folks have claimed this causes problems because the breakpoint
     may be the first instruction of the prologue.  If that happens, then
     the instruction skipping code has a bug that needs to be fixed.  */
  if (post_prologue_pc != 0)
    return max (pc, post_prologue_pc);
  else
    return (skip_prologue_hard_way (pc));
}

/* JAGae85195 - Sunil -- */
#if !defined(GDB_TARGET_IS_HPPA_20W)
int arg_size[4];
#else 
int arg_size[8];
#endif

/* Put here the code to store, into a struct frame_saved_regs,
   the addresses of the saved registers of frame described by FRAME_INFO.
   This includes special registers such as pc and fp saved in special
   ways in the stack frame.  sp is even more special:
   the address we return for it IS the sp for the next frame.  */

void
hppa_frame_find_saved_regs (frame_info, frame_saved_regs)
     struct frame_info *frame_info;
     struct frame_saved_regs *frame_saved_regs;
{
  CORE_ADDR pc;
  struct unwind_table_entry *u, *u2;
  unsigned long inst, stack_remaining, save_gr, save_fr, save_rp, save_sp;
  int status, i, reg;
  char buf[4];
  int fp_loc = -1;
  extern CORE_ADDR text_end;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;
  int final_iteration;

  /* Zero out everything.  */
  memset (frame_saved_regs, '\0', sizeof (struct frame_saved_regs));

  /* Call dummy frames always look the same, so there's no need to
     examine the dummy code to determine locations of saved registers;
     instead, let find_dummy_frame_regs fill in the correct offsets
     for the saved registers.  */
#ifndef GDB_TARGET_IS_HPPA_20W
  if ((frame_info->pc >= frame_info->frame
       && frame_info->pc <= (frame_info->frame
			     /* A call dummy is sized in words, but it is
				actually a series of instructions.  Account
				for that scaling factor.  */
			     + ((REGISTER_SIZE / INSTRUCTION_SIZE)
				* CALL_DUMMY_LENGTH)
			     /* Similarly we have to account for 64bit
				wide register saves.  */
			     + (32 * REGISTER_SIZE)
			     /* We always consider FP regs 8 bytes long.  */
			     + (NUM_REGS - FP0_REGNUM) * 8
			     /* Similarly we have to account for 64bit
				wide register saves.  */
			     + (6 * REGISTER_SIZE))))
#else
  if (PC_IN_CALL_DUMMY (frame_info->pc, 0, 0))
#endif
    find_dummy_frame_regs (frame_info, frame_saved_regs);

  /* Interrupt handlers are special too.  They lay out the register
     state in the exact same order as the register numbers in GDB.  */
  if (pc_in_interrupt_handler (frame_info->pc))
    {
      for (i = 0; i < NUM_REGS; i++)
	{
	  /* SP is a little special.  */
	  if (i == SP_REGNUM)
	    frame_saved_regs->regs[SP_REGNUM]
	      = read_memory_integer (frame_info->frame + SP_REGNUM * REGISTER_SIZE,
				     TARGET_PTR_BIT / 8);
	  else
	    frame_saved_regs->regs[i] = frame_info->frame + i * REGISTER_SIZE;
	}
      return;
    }

  /* RM: _sr4export needs special treatment */
  msym = lookup_minimal_symbol_by_pc (frame_info->pc);
  msym_sr4export = lookup_minimal_symbol ("_sr4export", NULL, NULL);
  if (msym
      && msym_sr4export
      && SYMBOL_VALUE_ADDRESS (msym) ==
      SYMBOL_VALUE_ADDRESS (msym_sr4export))
    {
      frame_saved_regs->regs[RP_REGNUM] = frame_info->frame-0x18;
    }

#ifdef FRAME_FIND_SAVED_REGS_IN_SIGTRAMP
  /* Handle signal handler callers.  */
  if (frame_info->signal_handler_caller)
    {
      FRAME_FIND_SAVED_REGS_IN_SIGTRAMP (frame_info, frame_saved_regs);
      return;
    }
#endif

  /* Get the starting address of the function referred to by the PC
     saved in frame.  */
  pc = get_pc_function_start (frame_info->pc);

  /* Yow! */
  u = hppa_find_unwind_entry (pc);

  /* Stacey 11/8/2001
     In most cases the code above will work just fine ... however in the
     special case where we're trying to retrieve registers after a call to 
     a function with a variable size stack frame, the code can hurt us by:
     
     1.  leading us to an unwind entry with bad or insufficient info or
         the stub of the procedure may have a different unwind
	 entry from the body of the procedure, containing less saved
	 registers.  

     2.  making sure we don't get an unwind entry at all 
         the entry stub of a procedure may not have an unwind entry -
	 in which case we will get here having no unwind entry.  This
	 would be a bug on HP C, but we should handle this case anyways.  

     The if/else condition below is designed to handle these special cases by 
     making sure we get

     1.  a number >= to the number of saved registers - even if we've got the 
         wrong unwind entry.  This helps since we don't really use the unwind
	 entry for anything at this point except register information.  While
	 this isn't perfect, at least we won't lose register information.  

     2.  an unwind entry in the case that there is no unwind entry for the
         entry stub of a procedure.  */

  if (!u)
    {
      u = hppa_find_unwind_entry (frame_info->pc);
      if (!u)
	/* we have no unwind entry !!! */
        /* The frame always represents the value of %sp at entry to the
           current function (and is thus equivalent to the "saved" stack
           pointer.  */
        frame_saved_regs->regs[SP_REGNUM] = frame_info->frame;
	return;
    }
  else 
    {
      u2 = hppa_find_unwind_entry (frame_info->pc);
      if (u2 && (u2->Entry_GR > u->Entry_GR))
	u->Entry_GR = u2->Entry_GR;
    }

  /* This is how much of a frame adjustment we need to account for.  */
  stack_remaining = u->Total_frame_size << 3;

  /* Magic register saves we want to know about.  */
  save_rp = u->Save_RP;
  save_sp = u->Save_SP;

  /* rven: In a conversation with Cary Coutant 10/12/2001, Cary told
     me that on PA, it is architected that the callee save registers
     are contiguously saved starting with register r3 at the beginning 
     of the frame */ 
  /* Turn the Entry_GR field into a bitmask.  */
  save_gr = 0;
  for (i = 3; i < u->Entry_GR + 3; i++)
    {
      /* Frame pointer gets saved into a special location.  */
      if (u->Save_SP && i == FP_REGNUM)
	continue;

      if (i <= 8)
      	frame_saved_regs->regs[i] = frame_info->frame + 
		(u->Entry_FR) * 8 +     /* Skip FP regs which are saved first */
		(i-3) * sizeof (CORE_ADDR);
      else
	save_gr |= (1 << i);
    }

  /* Turn the Entry_FR field into a bitmask too.  */
  save_fr = 0;
  for (i = 12; i < u->Entry_FR + 12; i++)
    save_fr |= (1 << i);

  /* The frame always represents the value of %sp at entry to the
     current function (and is thus equivalent to the "saved" stack
     pointer.  */
  frame_saved_regs->regs[SP_REGNUM] = frame_info->frame;
#ifdef GDB_TARGET_IS_HPPA_20W
  /* This code is HP compiler specific. */
  if (!hppa_pc_in_gcc_function (frame_info->pc)) {
#endif
  /* For JAGae85195 - Sunil, access the frame to get the contents of r23-r26*/
  /* Although a check whether Args_stored bit is set or not is done, it
     is not guaranteed that we can actually get to the arguments as
     the compiler may have optimized the initial store of args
  */
  register struct symbol *sym = NULL;
  register struct symbol *func = get_frame_function (frame_info);
  struct block *b = NULL;
  int nsyms = 0;

#define R26_REGNUM 26
#define R25_REGNUM 25
#define R24_REGNUM 24
#define R23_REGNUM 23
#ifdef GDB_TARGET_IS_HPPA_20W
#define R22_REGNUM 22
#define R21_REGNUM 21
#define R20_REGNUM 20
#define R19_REGNUM 19
#endif

  if (func)
    {
      b = SYMBOL_BLOCK_VALUE (func);
      nsyms = BLOCK_NSYMS (b);
    }

#ifndef GDB_TARGET_IS_HPPA_20W
      unsigned long temp_sp = FRAME_FP(frame_info);
#else
      unsigned long long temp_sp = frame_info->ap;
#endif

  for (i = 0; i < nsyms; i++)
    {
      sym = BLOCK_SYM (b, i);

      if (sym->is_argument != 1) /* do not print locals */
        continue;

      struct type *type = SYMBOL_TYPE (sym);
	
/* JAGae85195 - Sunil S
   The calculation follows a general pattern

   temp_sp + number of bytes to advance to the next word - argument size - number of bytes 
   to be subtracted from temp_sp to get to the posistion of the corresponding argument.
*/
      switch (i)
        {
          /* for r26 */
          case 0:
    	      arg_size[i] = TYPE_LENGTH (type);
#ifndef GDB_TARGET_IS_HPPA_20W
              frame_saved_regs->regs[R26_REGNUM] = temp_sp + 4 - arg_size[i] - 36;
#else
    	      frame_saved_regs->regs[R26_REGNUM] = temp_sp + 8 - arg_size[i] - 64;
#endif
	      break;
	
          /* for r25 */
          case 1:
    	      arg_size[i] = TYPE_LENGTH (type);
#ifndef GDB_TARGET_IS_HPPA_20W
              frame_saved_regs->regs[R25_REGNUM] = temp_sp + 4 - arg_size[i] - 40;
#else
    	      frame_saved_regs->regs[R25_REGNUM] = temp_sp + 8 - arg_size[i] - 56;
#endif
	      break;

          /* for r24 */
          case 2:
    	      arg_size[i] = TYPE_LENGTH (type);
#ifndef GDB_TARGET_IS_HPPA_20W
              frame_saved_regs->regs[R24_REGNUM] = temp_sp + 4 - arg_size[i] - 44;
#else
    	      frame_saved_regs->regs[R24_REGNUM] = temp_sp + 8 - arg_size[i] - 48;
#endif
	      break;

          /* for r23 */
          case 3:
    	      arg_size[i] = TYPE_LENGTH (type);
#ifndef GDB_TARGET_IS_HPPA_20W
              frame_saved_regs->regs[R23_REGNUM] = temp_sp + 4 - arg_size[i] - 48;
#else
    	      frame_saved_regs->regs[R23_REGNUM] = temp_sp + 8 - arg_size[i] - 40;
#endif
	      break;

#ifdef GDB_TARGET_IS_HPPA_20W
	  case 4:
    	      arg_size[i] = TYPE_LENGTH (type);
    	      frame_saved_regs->regs[R22_REGNUM] = temp_sp + 8 - arg_size[i] - 32;
	      break;

	  case 5:
    	      arg_size[i] = TYPE_LENGTH (type);
    	      frame_saved_regs->regs[R21_REGNUM] = temp_sp + 8 - arg_size[i] - 24;
	      break;

	  case 6:
    	      arg_size[i] = TYPE_LENGTH (type);
    	      frame_saved_regs->regs[R20_REGNUM] = temp_sp + 8 - arg_size[i] - 16;
	      break;

	  case 7:
    	      arg_size[i] = TYPE_LENGTH (type);
    	      frame_saved_regs->regs[R19_REGNUM] = temp_sp + 8 - arg_size[i] - 8;
	      break;
#endif
  /* Other types of symbols we just skip over.  */
	  default:
	    continue;
	  }
	}
#ifdef GDB_TARGET_IS_HPPA_20W
    } /* End if (!hppa_pc_in_gcc_function (frame_info)) */
#endif
  /* Loop until we find everything of interest or hit a branch.

     For unoptimized GCC code and for any HP CC code this will never ever
     examine any user instructions.

     For optimized GCC code we're faced with problems.  GCC will schedule
     its prologue and make prologue instructions available for delay slot
     filling.  The end result is user code gets mixed in with the prologue
     and a prologue instruction may be in the delay slot of the first branch
     or call.

     Some unexpected things are expected with debugging optimized code, so
     we allow this routine to walk past user instructions in optimized
     GCC code.  */
  final_iteration = 0;
  while ((save_gr || save_fr || save_rp || save_sp || stack_remaining > 0)
	 && pc <= frame_info->pc)
    {
      status = target_read_memory (pc, buf, TARGET_PTR_BIT / 8);
      inst = extract_unsigned_integer (buf, 4);

      /* Yow! */
      if (status != 0)
	return;

      /* Note the interesting effects of this instruction.  */
      stack_remaining -= prologue_inst_adjust_sp (inst);

      /* There are limited ways to store the return pointer into the
	 stack.  */
      /* stw rp,-0x14(sr0,sp) */
      /* std rp,-0x10(sr0,sp) */
      if (inst == 0x6bc23fd9 || inst == 0x0fc212c1 || inst_saves_gr(inst) == 2)
        {
          save_rp = 0;
          frame_saved_regs->regs[RP_REGNUM] = frame_info->frame - 
					      (TARGET_PTR_BIT == 64 ? 16 : 20);
	}

      /* Note if we saved SP into the stack.  This also happens to indicate
	 the location of the saved frame pointer.  */
      if (   (inst & 0xffffc000) == 0x6fc10000  /* stw,ma r1,N(sr0,sp) */
          || (inst & 0xffffc00c) == 0x73c10008) /* std,ma r1,N(sr0,sp) */
	{
	  frame_saved_regs->regs[FP_REGNUM] = frame_info->frame;
	  save_sp = 0;
	}

      /* Account for general and floating-point register saves.  */
      reg = inst_saves_gr (inst);
      if (reg >= 9 && reg <= 18
	  && (!u->Save_SP || reg != FP_REGNUM))
	{
	  save_gr &= ~(1 << reg);

	  /* stwm with a positive displacement is a *post modify*.  */
	  if ((inst >> 26) == 0x1b
	      && extract_14 (inst) >= 0)
            frame_saved_regs->regs[reg]
              = frame_info->frame + (fp_loc != -1 ? fp_loc : 0) ;
          /* RM: extract_14 should only be used for STW instructions */
          else if (((inst >> 26) == 0x1a) || ((inst >> 26) == 0x1b))
            {
              int basereg = (inst & 0x03e00000) >> 21;
              if (basereg == SP_REGNUM)
	        {
		  /* Handle code with and without frame pointers.  */
		  if (u->Save_SP)
		    frame_saved_regs->regs[reg]
		      = frame_info->frame + extract_14 (inst);
		  else
		    frame_saved_regs->regs[reg]
		      = frame_info->frame + (u->Total_frame_size << 3)
		      + extract_14 (inst);
		}
              else if (basereg == 3)
		frame_saved_regs->regs[reg] = frame_info->frame +
		  extract_14 (inst);
              /* Removed Unexpected instruction warning message.
              From a frame saved registers perspective, the saves will all be
              relative to SP. (R3 is only for gcc/g++, not for HP compilers).*/
            }

          /* RM: Now handle STD instructions */
	  /* A std has explicit post_modify forms.
	     JYG: And so we match for
	     STD long disp form, m-field == 1, a-field == 0, instead of
	     checking whether extract_im10a >= 0 */

	  else if ((inst & 0xfc00000e) == 0x70000008)
	    frame_saved_regs->regs[reg] = frame_info->frame;
          else if ((inst >> 26) == 0x1c)
            {
              /* Handle code with and without frame pointers.  */
              if (u->Save_SP)
                frame_saved_regs->regs[reg]
                  = frame_info->frame + extract_im10a (inst);
              else
                frame_saved_regs->regs[reg]
                  = frame_info->frame + (u->Total_frame_size << 3)
                    + extract_im10a (inst);
            }
        }

      /* GCC handles callee saved FP regs a little differently.  

         It emits an instruction to put the value of the start of
         the FP store area into %r1.  It then uses fstds,ma with
         a basereg of %r1 for the stores.

         HP CC emits them at the current stack pointer modifying
         the stack pointer as it stores each register.  */

      /* ldo X(%r3),%r1 or ldo X(%r30),%r1.  */
      if ((inst & 0xffffc000) == 0x34610000
	  || (inst & 0xffffc000) == 0x37c10000)
	fp_loc = extract_14 (inst);

      reg = inst_saves_fr (inst);
      if (reg >= 12 && reg <= 21)
	{
	  /* Note +4 braindamage below is necessary because the FP status
	     registers are internally 8 registers rather than the expected
	     4 registers.  */
	  save_fr &= ~(1 << reg);
	  if (fp_loc == -1)
	    {
	      /* 1st HP CC FP register store.  After this instruction
	         we've set enough state that the GCC and HPCC code are
	         both handled in the same manner.  */
	      frame_saved_regs->regs[reg + FP4_REGNUM + REGISTER_SIZE] = frame_info->frame;
	      fp_loc = 8;
	    }
	  else
	    {
	      frame_saved_regs->regs[reg + FP0_REGNUM + REGISTER_SIZE]
		= frame_info->frame + fp_loc;
	      fp_loc += 8;
	    }
	}

      /* Quit if we hit any kind of branch the previous iteration. */
      if (final_iteration)
	break;

      /* We want to look precisely one instruction beyond the branch
	 if we have not found everything yet.  */
      if (is_branch (inst))
	final_iteration = 1;

      /* Bump the PC.  */
      pc += 4;
    }
}


/* Exception handling support for the HP-UX ANSI C++ compiler.
   The compiler (aCC) provides a callback for exception events;
   GDB can set a breakpoint on this callback and find out what
   exception event has occurred. */

/* The name of the hook to be set to point to the callback function */
static char HP_ACC_EH_notify_hook[] = "__eh_notify_hook";
/* The name of the function to be used to set the hook value */
static char HP_ACC_EH_set_hook_value[] = "__eh_set_hook_value";
/* The name of the callback function in end.o */
static char HP_ACC_EH_notify_callback[] = "__d_eh_notify_callback";
/* Location with a pointer to __d_eh_notify_callback */ 
static char HP_ACC_EH_notify_callback_ptr[] = "__eh_callback_fptr";
/* Name of function in end.o on which a break is set (called by above) */
static char HP_ACC_EH_break[] = "__d_eh_break";
/* Name of flag (in end.o) that enables catching throws */
static char HP_ACC_EH_catch_throw[] = "__d_eh_catch_throw";
/* Name of flag (in end.o) that enables catching catching */
static char HP_ACC_EH_catch_catch[] = "__d_eh_catch_catch";
/* The enum used by aCC */
typedef enum
  {
    __EH_NOTIFY_THROW,
    __EH_NOTIFY_CATCH
  }
__eh_notification;

/* Is exception-handling support available with this executable? */
static int hp_cxx_exception_support = 0;
/* Has the initialize function been run? */
int hp_cxx_exception_support_initialized = 0;
/* Similar to above, but imported from breakpoint.c -- non-target-specific */
extern int exception_support_initialized;
/* Address of __eh_notify_hook */
static CORE_ADDR eh_notify_hook_addr = 0;
/* Address of __d_eh_notify_callback */
static CORE_ADDR eh_notify_callback_addr = 0;
/* Address of __d_eh_break */
static CORE_ADDR eh_break_addr = 0;
/* Address of __d_eh_catch_catch */
static CORE_ADDR eh_catch_catch_addr = 0;
/* Address of __d_eh_catch_throw */
static CORE_ADDR eh_catch_throw_addr = 0;
/* Sal for __d_eh_break */
static struct symtab_and_line *break_callback_sal = 0;

/* Code in end.c expects __d_pid to be set in the inferior,
   otherwise __d_eh_notify_callback doesn't bother to call
   __d_eh_break!  So we poke the pid into this symbol
   ourselves.
   0 => success
   1 => failure  */
int
setup_d_pid_in_inferior ()
{
  CORE_ADDR anaddr;
  struct minimal_symbol *msymbol;
  char buf[4];

  /* Slam the pid of the process into __d_pid; failing is only a warning!  */
  msymbol = lookup_minimal_symbol ("__d_pid", NULL, symfile_objfile);
  if (msymbol == NULL)
    {
      warning ("Unable to find __d_pid symbol in object file.");
      warning ("Suggest linking executable with -g (links in /opt/langtools/lib/end.o).");
      return 1;
    }

  anaddr = SYMBOL_VALUE_ADDRESS (msymbol);
  store_unsigned_integer (buf, 4, inferior_pid);
  if (target_write_memory (anaddr, buf, 4))
    {
      warning ("Unable to write __d_pid");
      warning ("Suggest linking executable with -g (links in /opt/langtools/lib/end.o).");
      return 1;
    }
  return 0;
}

/* Initialize exception catchpoint support by looking for the
   necessary hooks/callbacks in end.o, etc., and set the hook value to
   point to the required debug function

   Return 0 => failure
   1 => success          */

static int
initialize_hp_cxx_exception_support ()
{
  struct symtabs_and_lines sals;
  struct cleanup *old_chain;
  struct cleanup *canonical_strings_chain = NULL;
  int i;
  char *addr_start;
  char *addr_end = NULL;
  char **canonical = (char **) NULL;
  int thread = -1;
  struct symbol *sym = NULL;
  struct minimal_symbol *msym = NULL;
  struct objfile *objfile;
  asection *shlib_info;

  /* Detect and disallow recursion.  On HP-UX with aCC, infinite
     recursion is a possibility because finding the hook for exception
     callbacks involves making a call in the inferior, which means
     re-inserting breakpoints which can re-invoke this code */

  static int recurse = 0;
  if (recurse > 0)
    {
      hp_cxx_exception_support_initialized = 0;
      exception_support_initialized = 0;
      return 0;
    }

  hp_cxx_exception_support = 0;

  /* First check if we have seen any HP compiled objects; if not,
     it is very unlikely that HP's idiosyncratic callback mechanism
     for exception handling debug support will be available!
     This will percolate back up to breakpoint.c, where our callers
     will decide to try the g++ exception-handling support instead. */
  if (!hp_som_som_object_present)
    return 0;

  /* We have a SOM executable with SOM debug info; find the hooks */

  /* First look for the notify hook provided by aCC runtime libs */
  /* If we find this symbol, we conclude that the executable must
     have HP aCC exception support built in.  If this symbol is not
     found, even though we're a HP SOM-SOM file, we may have been
     built with some other compiler (not aCC).  This results percolates
     back up to our callers in breakpoint.c which can decide to
     try the g++ style of exception support instead.
     If this symbol is found but the other symbols we require are
     not found, there is something weird going on, and g++ support
     should *not* be tried as an alternative.

     ASSUMPTION: Only HP aCC code will have __eh_notify_hook defined.  
     ASSUMPTION: HP aCC and g++ modules cannot be linked together. */

  /* libCsup has this hook; it'll usually be non-debuggable */
  msym = lookup_minimal_symbol (HP_ACC_EH_notify_hook, NULL, NULL);
  if (msym)
    {
      eh_notify_hook_addr = SYMBOL_VALUE_ADDRESS (msym);
      hp_cxx_exception_support = 1;
    }
  else
    {
      warning ("Unable to find exception callback hook (%s).", HP_ACC_EH_notify_hook);
      warning ("Executable may not have been compiled debuggable with HP aCC.");
      warning ("GDB will be unable to intercept exception events.");
      eh_notify_hook_addr = 0;
      hp_cxx_exception_support = 0;
      return 0;
    }

  /* Next look for the notify callback routine in end.o */
  /* This is always available in the SOM symbol dictionary if end.o is linked in */
  msym = lookup_minimal_symbol (HP_ACC_EH_notify_callback, NULL, NULL);
  if (msym)
    {
      eh_notify_callback_addr = SYMBOL_VALUE_ADDRESS (msym);
      hp_cxx_exception_support = 1;
    }
  else
    {
      warning ("Unable to find exception callback routine (%s).", HP_ACC_EH_notify_callback);
      warning ("Suggest linking executable with -g (links in /opt/langtools/lib/end.o).");
      warning ("GDB will be unable to intercept exception events.");
      eh_notify_callback_addr = 0;
      return 0;
    }

  /* Check whether the executable is dynamically linked or archive bound */
  /* With an archive-bound executable we can use the raw addresses we find
     for the callback function, etc. without modification. For an executable
     with shared libraries, we have to do more work to find the plabel, which
     can be the target of a call through $$dyncall from the aCC runtime support
     library (libCsup) which is linked shared by default by aCC. */
  /* This test below was copied from somsolib.c/somread.c.  It may not be a very
     reliable one to test that an executable is linked shared. pai/1997-07-18 */
#ifdef GDB_TARGET_IS_HPPA_20W
  /* RM: HP_ACC_EH_notify_callback_ptr contains a pointer to
   * HP_ACC_EH_notify_callback. No need to muck about with __d_shl_get
   * to find a plabel and other such yucky stuff. I suspect this will
   * work on PA32 too, but let it be for now.
   */
  msym = lookup_minimal_symbol (HP_ACC_EH_notify_callback_ptr, NULL, NULL);
  if (msym)
    {
      if (target_read_memory(SYMBOL_VALUE_ADDRESS(msym),
                             (char*) &eh_notify_callback_addr,
                             sizeof(CORE_ADDR)) != 0)
        {
          warning ("Unable to find exception callback routine pointer (%s).", HP_ACC_EH_notify_callback_ptr);
          warning ("Suggest linking executable with -g (links in /opt/langtools/lib/end.o).");
          warning ("GDB will be unable to intercept exception events.");
          eh_notify_callback_addr = 0;
          return 0;
        }
      hp_cxx_exception_support = 1;
    }  
  else 
    {
      warning ("Unable to find exception callback routine pointer (%s).", HP_ACC_EH_notify_callback_ptr);
      warning ("Suggest linking executable with -g (links in /opt/langtools/lib/end.o).");
      warning ("GDB will be unable to intercept exception events.");
      eh_notify_callback_addr = 0;
      return 0;
    }
#else  
  shlib_info = bfd_get_section_by_name (symfile_objfile->obfd, "$SHLIB_INFO$");
  if (shlib_info && (bfd_section_size (symfile_objfile->obfd, shlib_info) != 0))
    {
      /* The minsym we have has the local code address, but that's not the
         plabel that can be used by an inter-load-module call. */
      /* Find solib handle for main image (which has end.o), and use that
         and the min sym as arguments to __d_shl_get() (which does the equivalent
         of shl_findsym()) to find the plabel. */

      args_for_find_stub args;
      static char message[] = "Error while finding exception callback hook:\n";

      args.solib_handle = som_solib_get_solib_by_pc (eh_notify_callback_addr);
      args.msym = msym;
      args.return_val = 0;

      recurse++;
      catch_errors (cover_find_stub_with_shl_get, (PTR) &args, message,
		    RETURN_MASK_ALL);
      eh_notify_callback_addr = args.return_val;
      recurse--;

      exception_catchpoints_are_fragile = 1;

      if (!eh_notify_callback_addr)
	{
	  /* We can get here either if there is no plabel in the export list
	     for the main image, or if something strange happened (??) */
	  warning ("Couldn't find a plabel (indirect function label) for the exception callback.");
	  warning ("GDB will not be able to intercept exception events.");
	  return 0;
	}
    }
  else
    exception_catchpoints_are_fragile = 0;
#endif

  /* Now, look for the breakpointable routine in end.o */
  /* This should also be available in the SOM symbol dict. if end.o linked in */
  msym = lookup_minimal_symbol (HP_ACC_EH_break, NULL, NULL);
  if (msym)
    {
      eh_break_addr = SYMBOL_VALUE_ADDRESS (msym);
      hp_cxx_exception_support = 1;
    }
  else
    {
      warning ("Unable to find exception callback routine to set breakpoint (%s).", HP_ACC_EH_break);
      warning ("Suggest linking executable with -g (link in /opt/langtools/lib/end.o).");
      warning ("GDB will be unable to intercept exception events.");
      eh_break_addr = 0;
      return 0;
    }

  /* Next look for the catch enable flag provided in end.o */
  sym = lookup_symbol (HP_ACC_EH_catch_catch, (struct block *) NULL,
		       VAR_NAMESPACE, 0, (struct symtab **) NULL);
  if (sym)			/* sometimes present in debug info */
    {
      eh_catch_catch_addr = read_var_addr (sym, NULL);
      if (eh_catch_catch_addr)
	hp_cxx_exception_support = 1;
    }
  else
    /* otherwise look in SOM symbol dict. */
    {
      msym = lookup_minimal_symbol (HP_ACC_EH_catch_catch, NULL, NULL);
      if (msym)
	{
	  eh_catch_catch_addr = SYMBOL_VALUE_ADDRESS (msym);
	  hp_cxx_exception_support = 1;
	}
      else
	{
	  warning ("Unable to enable interception of exception catches.");
	  warning ("Executable may not have been compiled debuggable with HP aCC.");
	  warning ("Suggest linking executable with -g (link in /opt/langtools/lib/end.o).");
	  return 0;
	}
    }

  /* Next look for the catch enable flag provided end.o */
  sym = lookup_symbol (HP_ACC_EH_catch_throw, (struct block *) NULL,
		       VAR_NAMESPACE, 0, (struct symtab **) NULL);
  if (sym)			/* sometimes present in debug info */
    {
      eh_catch_throw_addr = read_var_addr (sym, NULL);
      if (eh_catch_throw_addr)
	hp_cxx_exception_support = 1;
    }
  else
    /* otherwise look in SOM symbol dict. */
    {
      msym = lookup_minimal_symbol (HP_ACC_EH_catch_throw, NULL, NULL);
      if (msym)
	{
	  eh_catch_throw_addr = SYMBOL_VALUE_ADDRESS (msym);
	  hp_cxx_exception_support = 1;
	}
      else
	{
	  warning ("Unable to enable interception of exception throws.");
	  warning ("Executable may not have been compiled debuggable with HP aCC.");
	  warning ("Suggest linking executable with -g (link in /opt/langtools/lib/end.o).");
	  return 0;
	}
    }

  /* Set the flags */
  hp_cxx_exception_support = 2;	/* everything worked so far */
  hp_cxx_exception_support_initialized = 1;
  exception_support_initialized = 1;

  return 1;
}

/* Target operation for enabling or disabling interception of
   exception events.
   KIND is either EX_EVENT_THROW or EX_EVENT_CATCH
   ENABLE is either 0 (disable) or 1 (enable).
   Return value is NULL if no support found;
   -1 if something went wrong,
   or a pointer to a symtab/line struct if the breakpointable
   address was found. */

struct symtab_and_line *
child_enable_exception_callback (kind, enable)
     enum exception_event_kind kind;
     int enable;
{
  char buf[sizeof (CORE_ADDR)];

  if (!exception_support_initialized || !hp_cxx_exception_support_initialized)
    if (!initialize_hp_cxx_exception_support ())
      return NULL;

  switch (hp_cxx_exception_support)
    {
    case 0:
      /* Assuming no HP support at all */
      return NULL;
    case 1:
      /* HP support should be present, but something went wrong */
      return (struct symtab_and_line *) -1;	/* yuck! */
      /* there may be other cases in the future */
    }

  /* Set the EH hook to point to the callback routine */
  store_unsigned_integer (buf, sizeof(CORE_ADDR), enable ? eh_notify_callback_addr : 0); 
  if (target_write_memory (eh_notify_hook_addr, buf, sizeof(CORE_ADDR)))
    {
      warning ("Could not write to target memory for exception event callback.");
      warning ("Interception of exception events may not work.");
      return (struct symtab_and_line *) -1;
    }
  if (enable)
    {
      /* Ensure that __d_pid is set up correctly -- end.c code checks this. :-( */
      if (inferior_pid > 0)
	{
	  if (setup_d_pid_in_inferior ())
	    return (struct symtab_and_line *) -1;
	}
      else
	{
	  warning ("Internal error: Invalid inferior pid?  Cannot intercept exception events.");
	  return (struct symtab_and_line *) -1;
	}
    }

  switch (kind)
    {
    case EX_EVENT_THROW:
      store_unsigned_integer (buf, 4, enable ? 1 : 0);
      if (target_write_memory (eh_catch_throw_addr, buf, 4))
	{
	  warning ("Couldn't enable exception throw interception.");
	  return (struct symtab_and_line *) -1;
	}
      break;
    case EX_EVENT_CATCH:
      store_unsigned_integer (buf, 4, enable ? 1 : 0);
      if (target_write_memory (eh_catch_catch_addr, buf, 4))
	{
	  warning ("Couldn't enable exception catch interception.");
	  return (struct symtab_and_line *) -1;
	}
      break;
    default:
      error ("Request to enable unknown or unsupported exception event.");
    }

  /* Copy break address into new sal struct, malloc'ing if needed. */
  if (!break_callback_sal)
    {
      break_callback_sal = (struct symtab_and_line *) xmalloc (sizeof (struct symtab_and_line));
    }
  INIT_SAL (break_callback_sal);
  break_callback_sal->symtab = NULL;
  break_callback_sal->pc = eh_break_addr;
  break_callback_sal->line = 0;
  break_callback_sal->end = eh_break_addr;

  return break_callback_sal;
}

/* Record some information about the current exception event */
static struct exception_event_record current_ex_event;
/* Convenience struct */
static struct symtab_and_line null_symtab_and_line =
{NULL, 0, 0, 0};

/* Report current exception event.  Returns a pointer to a record
   that describes the kind of the event, where it was thrown from,
   and where it will be caught.  More information may be reported
   in the future */
struct exception_event_record *
child_get_current_exception_event ()
{
  CORE_ADDR event_kind;
  CORE_ADDR throw_addr;
  CORE_ADDR catch_addr;
  struct frame_info *fi, *curr_frame;
  int level = 1;

  curr_frame = get_current_frame ();
  if (!curr_frame)
    return (struct exception_event_record *) NULL;

  /* Go up one frame to __d_eh_notify_callback, because at the
     point when this code is executed, there's garbage in the
     arguments of __d_eh_break. */
  fi = find_relative_frame (curr_frame, &level);
  if (level != 0)
    return (struct exception_event_record *) NULL;

  select_frame (fi, -1);

  /* Read in the arguments */
  /* __d_eh_notify_callback() is called with 3 arguments:
     1. event kind catch or throw
     2. the target address if known
     3. a flag -- not sure what this is. pai/1997-07-17 */
  event_kind = read_register (ARG0_REGNUM);
  catch_addr = read_register (ARG1_REGNUM);

  /* Now go down to a user frame */
  /* For a throw, __d_eh_break is called by
     __d_eh_notify_callback which is called by
     __notify_throw which is called
     from user code.
     For a catch, __d_eh_break is called by
     __d_eh_notify_callback which is called by
     <stackwalking stuff> which is called by
     __throw__<stuff> or __rethrow_<stuff> which is called
     from user code. */
  /* FIXME: Don't use such magic numbers; search for the frames */
  level = (event_kind == EX_EVENT_THROW) ? 3 : 4;
  fi = find_relative_frame (curr_frame, &level);
  if (level != 0)
    return (struct exception_event_record *) NULL;

  select_frame (fi, -1);
  throw_addr = fi->pc;

  /* Go back to original (top) frame */
  select_frame (curr_frame, -1);

  current_ex_event.kind = (enum exception_event_kind) event_kind;
  current_ex_event.throw_sal = find_pc_line (throw_addr, 1);
  current_ex_event.catch_sal = find_pc_line (catch_addr, 1);

  return &current_ex_event;
}

static void
unwind_command (exp, from_tty)
     char *exp;
     int from_tty;
{
  CORE_ADDR address;
  struct unwind_table_entry *u;

  /* If we have an expression, evaluate it and use it as the address.  */

  if (exp != 0 && *exp != 0)
    address = parse_and_eval_address (exp);
  else
    return;

  u = hppa_find_unwind_entry (address);

  if (!u)
    {
      printf_unfiltered ("Can't find unwind table entry for %s\n", exp);
      return;
    }

  printf_unfiltered ("unwind_table_entry (0x%lx):\n", u);

  printf_unfiltered ("\tregion_start = ");
  print_address (u->region_start, gdb_stdout);

  printf_unfiltered ("\n\tregion_end = ");
  print_address (u->region_end, gdb_stdout);

#ifdef __STDC__
#define pif(FLD) if (u->FLD) printf_unfiltered (" "#FLD);
#else
#define pif(FLD) if (u->FLD) printf_unfiltered (" FLD");
#endif

  printf_unfiltered ("\n\tflags =");
  pif (Cannot_unwind);
  pif (Millicode);
#ifndef GDB_TARGET_IS_HPPA_20W
  pif (Millicode_save_sr0);
#endif
  pif (Entry_SR);
  pif (Args_stored);
#ifndef GDB_TARGET_IS_HPPA_20W
  pif (Variable_Frame);
  pif (Separate_Package_Body);
  pif (Frame_Extension_Millicode);
#endif
  pif (Stack_Overflow_Check);
  pif (Two_Instruction_SP_Increment);
#ifndef GDB_TARGET_IS_HPPA_20W
  pif (Ada_Region);
#endif
  pif (Save_SP);
  pif (Save_RP);
  pif (Save_MRP_in_frame);
#ifndef GDB_TARGET_IS_HPPA_20W
  pif (extn_ptr_defined);
#endif
  pif (Cleanup_defined);
#ifndef GDB_TARGET_IS_HPPA_20W
  pif (MPE_XL_interrupt_marker);
#endif
  pif (HP_UX_interrupt_marker);
  pif (Large_frame);
#ifdef GDB_TARGET_IS_HPPA_20W
  pif (alloca_frame);
#endif

  putchar_unfiltered ('\n');

#ifdef __STDC__
#define pin(FLD) printf_unfiltered ("\t"#FLD" = 0x%x\n", u->FLD);
#else
#define pin(FLD) printf_unfiltered ("\tFLD = 0x%x\n", u->FLD);
#endif

  pin (Region_description);
  pin (Entry_FR);
  pin (Entry_GR);
  pin (Total_frame_size);
}

#ifdef PREPARE_TO_PROCEED

/* If the user has switched threads, and there is a breakpoint
   at the old thread's pc location, then switch to that thread
   and return TRUE, else return FALSE and don't do a thread
   switch (or rather, don't seem to have done a thread switch).

   Ptrace-based gdb will always return FALSE to the thread-switch
   query, and thus also to PREPARE_TO_PROCEED.

   The important thing is whether there is a BPT instruction,
   not how many user breakpoints there are.  So we have to worry
   about things like these:

   o  Non-bp stop -- NO

   o  User hits bp, no switch -- NO

   o  User hits bp, switches threads -- YES

   o  User hits bp, deletes bp, switches threads -- NO

   o  User hits bp, deletes one of two or more bps
   at that PC, user switches threads -- YES

   o  Plus, since we're buffering events, the user may have hit a
   breakpoint, deleted the breakpoint and then gotten another
   hit on that same breakpoint on another thread which
   actually hit before the delete. (FIXME in breakpoint.c
   so that "dead" breakpoints are ignored?) -- NO

   For these reasons, we have to violate information hiding and
   call "breakpoint_here_p".  If core gdb thinks there is a bpt
   here, that's what counts, as core gdb is the one which is
   putting the BPT instruction in and taking it out. */
int
hppa_prepare_to_proceed ()
{
  pid_t old_thread;
  pid_t current_thread;

  old_thread = hppa_switched_threads (inferior_pid);
  if (old_thread != 0)
    {
      /* Switched over from "old_thread".  Try to do
         as little work as possible, 'cause mostly
         we're going to switch back. */
      CORE_ADDR new_pc;
      CORE_ADDR old_pc = read_pc ();

      /* Yuk, shouldn't use global to specify current
         thread.  But that's how gdb does it. */
      current_thread = inferior_pid;
      inferior_pid = old_thread;

      new_pc = read_pc ();
      if (new_pc != old_pc	/* If at same pc, no need */
	  && breakpoint_here_p (new_pc))
	{
	  /* User hasn't deleted the BP.
	     Return TRUE, finishing switch to "old_thread". */
	  flush_cached_frames ();
	  registers_changed ();
#if 0
	  printf ("---> PREPARE_TO_PROCEED (was %d, now %d)!\n",
		  current_thread, inferior_pid);
#endif

	  return 1;
	}

      /* Otherwise switch back to the user-chosen thread. */
      inferior_pid = current_thread;
      new_pc = read_pc ();	/* Re-prime register cache */
    }

  return 0;
}
#endif /* PREPARE_TO_PROCEED */

void
hppa_skip_permanent_breakpoint ()
{
  /* To step over a breakpoint instruction on the PA takes some
     fiddling with the instruction address queue.

     When we stop at a breakpoint, the IA queue front (the instruction
     we're executing now) points at the breakpoint instruction, and
     the IA queue back (the next instruction to execute) points to
     whatever instruction we would execute after the breakpoint, if it
     were an ordinary instruction.  This is the case even if the
     breakpoint is in the delay slot of a branch instruction.

     Clearly, to step past the breakpoint, we need to set the queue
     front to the back.  But what do we put in the back?  What
     instruction comes after that one?  Because of the branch delay
     slot, the next insn is always at the back + 4.  */
  write_register (PCOQ_HEAD_REGNUM, read_register (PCOQ_TAIL_REGNUM));
  write_register (PCSQ_HEAD_REGNUM, read_register (PCSQ_TAIL_REGNUM));

  write_register (PCOQ_TAIL_REGNUM, read_register (PCOQ_TAIL_REGNUM) + 4);
  /* We can leave the tail's space the same, since there's no jump.  */
}

static void
objects_info (arg, from_tty)
     char *arg;
     int from_tty;
{
  struct partial_symtab *ps;
  struct objfile *objfile;
  int index;

  dont_repeat ();
  printf_filtered ("\n%9.9s  %12.12s  %s\n", "Index", "Status", "File");
  printf_filtered ("%9.9s  %12.12s  %s\n", "-----", "------", "----");
  index = 0;
  ALL_PSYMTABS (objfile, ps) {
    char *status_str;

    index++;
    switch (OFILE_LOAD_STATUS(ps)) {
    case DOOM_OFILE_PRELOADED:
      status_str = "In Exec";
      break;
    case DOOM_OFILE_DO_NOT_DEMAND_LOAD:
      status_str = "Not Loaded";
      break;
    case DOOM_OFILE_DO_DEMAND_LOAD:
      status_str = "Demand Load";
      break;
    case DOOM_OFILE_LOADED_WITH_DEBUG:
      status_str = "Loaded";
      break;
    case DOOM_OFILE_LOADED_NO_DEBUG:
      status_str = "No Debug";
      break;
    case DOOM_OFILE_OPEN_ERROR:
      status_str = "File Error";
      break;
    default:
      status_str = "<Unknown>";
    }
    printf_filtered ("%9d  %12.12s  %s\n", index, status_str, ps->filename);
  }
  printf_filtered ("\n");
}

static void
objectload_command (arg, from_tty)
     char *arg;
     int from_tty;
{
  struct partial_symtab *ps;
  struct objfile *objfile;
  int found = 0;

  dont_repeat ();

  if (arg) {
    ALL_PSYMTABS (objfile, ps) {
      if (STREQ(ps->filename, arg)) {
        if (from_tty) {
          printf_filtered ("Loading %s...\n", ps->filename);
        }
        PSYMTAB_TO_SYMTAB(ps);
        found = 1;
      }
    }
    if (!found) {
      int given_index = atoi(arg);
      if (given_index > 0) {
        int index = 0;
        ALL_PSYMTABS (objfile, ps) {
          index++;
          if (given_index == index) {
            if (from_tty) {
              printf_filtered ("Loading debug information from %s...\n",
                               ps->filename);
            }
            PSYMTAB_TO_SYMTAB(ps);
            found = 1;
          }
        }
      }
    }
    if (!found) {
      error("%s: Unknown object file. Use \"info objects\" to list the files.",
            arg);
    }
  } else {
    int ask_user = 1;
    
    if (from_tty) {
      ask_user = query("Load all of the debug information from all object"
                       " files? ");
    }
    if (ask_user) {
      ALL_PSYMTABS (objfile, ps) {
        if (OFILE_LOAD_STATUS(ps) != DOOM_OFILE_LOADED_WITH_DEBUG) {
          if (from_tty) {
            printf_filtered ("Loading %s...\n", ps->filename);
          }
          PSYMTAB_TO_SYMTAB(ps);
        }
      }
    }
  }
}

void
objectretry_command (arg, from_tty)
     char *arg;
     int from_tty;
{
  struct partial_symtab *ps;
  struct objfile *objfile;
  int at_least_one_reset = 0;

  dont_repeat ();

  if (from_tty) {
    printf_filtered ("\nSetting objects to load on demand:\n");
  }
  ALL_PSYMTABS (objfile, ps) {
    if (OFILE_LOAD_STATUS_IS_RETRYABLE(ps)) {
      if (from_tty) {
        printf_filtered ("\t%s\n", ps->filename);
      }
      OFILE_LOAD_STATUS(ps) = DOOM_OFILE_DO_DEMAND_LOAD;
      at_least_one_reset = 1;
    }
  }
  if (from_tty) {
    if (!at_least_one_reset) {
      printf_filtered ("\t<none>\n");
    }
    printf_filtered ("\n");
  }

  if (arg) {
    objectdir_command(arg, from_tty);
    if (from_tty) {
      printf_filtered ("\n");
    }
  }
}

void
_initialize_hppa_tdep ()
{
  tm_print_insn = print_insn_hppa;

  add_cmd ("unwind", class_maintenance, unwind_command,
	   "Print unwind table entry at given address.\n\nUsage:\n\tmaintenance "
           "info unwind <FUNC> | <ADDR>\n",
	   &maintenanceprintlist);

  add_info("objects", objects_info,
           "Load status of debug information in each object file.\n\nUsage:\n\t"
           "info objects\n");
  add_cmd ("objectload", class_files, objectload_command,
           "Force an object file to be loaded and its debug information read.\n\n"
           "Usage:\n\tobjectload <FILE> | <INDEX>\n\n"
           "Either the name or the index number reported by \"info objects\"\n"
           "can be provided. If an object has already been successfully loaded,\n"
           "it will not be loaded again.",
           &cmdlist);
  add_cmd ("objectretry", class_files, objectretry_command,
           "Retry loading object files that failed previously.\n\n"
           "Usage:\n\tobjectretry <DIR1> [[:]<DIR2>] [...]\n\n"
           "If an argument is provided, add it to the list of directories\n"
           "searched for object files. See \"objectdir\".",
           &cmdlist);

}

/*
 * Format register value with constant width. Useful formatting for GUI clients
 * with register displays. Integer register values are filled with leading
 * zeros; floats have form: (+|-)d.ddddddddde(+-)dd (32-bit) or
 * (+|-)d.ddddddddddddddddde(+-)dddd (64-bit). Additionally, for PA 64, floats
 * are returned as 3 values (shown as positive values for ease of example):
 *
 *   +d.ddddddddddddddddde+dddd +d.ddddddddde+dd +d.ddddddddde+dd
 *
 * where the first value is the 64-bit fp register, the second value is the
 * left side, the third the right side of the register.
 */
void
pa_strcat_register_const_width (buf, bufLen, regnum, raw_buf)
     char *buf;
     int bufLen;
     int regnum;
     char *raw_buf;
{
  int i;

  /* Real floating point values start at FP4_REGNUM. */
  if (regnum < FP4_REGNUM) 
    {
      long reg_val[2];

      reg_val[0] = 0;
      reg_val[1] = 0;

      if (!pa_is_64bit ())
        {
          long long_val;

          /* Being big-endian, on this machine the low bits
           * (the ones we want to look at) are in the second longword.
           */
          reg_val[1] = *(long *)(raw_buf);

          long_val = extract_signed_integer (&reg_val[1], 4);
          strcat_to_buf_with_fmt (buf, bufLen, "%.8x", long_val);
        }
      else
        {
          pa_register_look_aside (raw_buf, regnum, (long *)&reg_val);
          strcat_to_buf_with_fmt (buf, bufLen, "%.8x%.8x", reg_val[0], reg_val[1]);
        }
    }
  else
    {
      unsigned char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];
      DOUBLEST doub;
      int inv;
      char * float_fmt = "%#+1.9e";

      /* Put it in the buffer.  No conversions are ever necessary.  */
      memcpy (virtual_buffer, raw_buf, REGISTER_RAW_SIZE(regnum));

#ifndef GDB_TARGET_IS_HPPA_20W
      if ((regnum % 2) != 0)
        {
          doub = unpack_double (builtin_type_float, (char *) virtual_buffer, &inv);
          strcat_to_buf_with_fmt (buf, bufLen, float_fmt, (float) doub);
        }
      else
#endif
        {
          char * double_fmt = "%#+1.17e";
          char * exponent;
#define TMP_BUF_LEN 100
          char tmpbuf[TMP_BUF_LEN];
#ifndef GDB_TARGET_IS_HPPA_20W
          char raw_buffer[MAX_REGISTER_RAW_SIZE];

          /* Get the data in raw format for the 2nd half.  */
          read_relative_register_raw_bytes (regnum + 1, raw_buffer);
       
          /* Copy it into the appropriate part of the virtual buffer.  */
          memcpy (virtual_buffer + REGISTER_RAW_SIZE(regnum), raw_buffer,
                  REGISTER_RAW_SIZE(regnum));
#endif
          doub = unpack_double (builtin_type_double, (char *) virtual_buffer, &inv);
          tmpbuf[0] = '\0';
          strcat_to_buf_with_fmt (tmpbuf, TMP_BUF_LEN, double_fmt, (double) doub);

          /* printf() and family don't have a way to specify how many digits follow
           * the exponent, so we force it to 4 digits for 64-bit regs (e(+|-)dddd).
           * At least two digits are guaranteed (and two digits are enough for the
           * 32-bit case).
           */
          exponent = strchr (tmpbuf, 'e');
          if (exponent != NULL)
            {
              if (strlen (exponent) == 4)
                strcat (tmpbuf, "00");
              else if (strlen (exponent) == 5)
                  strcat (tmpbuf, "0");
            }
          strcat_to_buf_with_fmt (buf, bufLen, "%s", tmpbuf);
#ifdef GDB_TARGET_IS_HPPA_20W
          /* Cat left half. */
          strcat_to_buf (buf, bufLen, " ");
          doub = unpack_double (builtin_type_float, (char *) virtual_buffer, &inv);
          strcat_to_buf_with_fmt (buf, bufLen, float_fmt, (float) doub);
          /* Cat right half. */
          strcat_to_buf (buf, bufLen, " ");
          doub = unpack_double (builtin_type_float, ((char *) virtual_buffer)
                                + sizeof (float), &inv);
          strcat_to_buf_with_fmt (buf, bufLen, float_fmt, (float) doub);
#endif
        }
    }
}

#ifdef GDB_TARGET_IS_HPPA_20W
/* at_purify_call - PA64
   If this is a purified program and $R15 is equal to prev_pc and
   the instruction at prev_pc is "mfia %r15" and the instruction at
   prev_pc -4 is "bve (%r15)", then we have stepped into a purify call.
   We return the return address which is $R15 + 4.  Otherwise we return 0.
   */

CORE_ADDR
at_purify_call (prev_pc)
  CORE_ADDR     prev_pc;
{
    extern int  is_purified_program;

    int         first_inst;
    int         second_inst;
    LONGEST     r15_value;

    if (!is_purified_program)
      return 0;

    r15_value = read_register (15);
    if (r15_value != prev_pc)
      return 0;

    first_inst = read_memory_unsigned_integer (prev_pc - INSTRUCTION_SIZE,
                                               INSTRUCTION_SIZE);
    if (first_inst != 0xe9e0d000)       /* bve (%r15) */
      return 0;

    second_inst = read_memory_unsigned_integer (prev_pc, INSTRUCTION_SIZE);
    if (second_inst != 0x14af)          /* mfia %r15 */
      return 0;

    return(prev_pc + INSTRUCTION_SIZE);  /* Purify return address */
} /* end at_purify_call */
#endif

/* The following f_xxx routines are for support of the f90 array descriptor,
 * a runtime structure used for certain types of arrays and pointers.  See 
 * hp-symtab-read.c for a more complete description of the runtime structure
 * and the gdb type created to support it.
 */

/* Get the byte where the initialized flag resides -- it's at byte
 * offset 9 (32-bit addresses) or 17 (64-bit addresses) from addr.
 * Issue an error if the initialized bit is not set (any non-zero value is
 * ok).  This check is necessary to avoid reading in garbage for the array
 * bounds.
 */
void 
f_check_array_desc (addr)
     CORE_ADDR addr;
{
  unsigned char flag;
  if (!addr)
    error ("Unable to access dynamic array address");
  flag = read_memory_unsigned_integer (addr +
				       2 * (TARGET_PTR_BIT/TARGET_CHAR_BIT) +
				       1, 1 /* byte */);
  if (!flag)
    error ("Uninitialized dynamic array");
}

/* Retrieve the lower bound of array descriptor array
 */
int
f_get_array_desc_array_lbound (type, lower_bound)
  struct type *type;
  LONGEST *lower_bound;
{
  /* Calculate address of lower bound by adding the offset to the array 
   * info structure that describes the array, i.e, TYPE_ARRAY_LOWER_BOUND_VALUE
   * and incrementing it by the offset to the lower bound, in this case, 0.
   */
  CORE_ADDR addr = TYPE_ARRAY_ADDR (type) +
		   TYPE_ARRAY_LOWER_BOUND_VALUE (type);
  *lower_bound = read_memory_integer (addr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
  return BOUND_FETCH_OK;
}

/* Calculate upper bound of array descriptor array and retrieve the stride.
 * The descriptor array gives the extent of an array (the number of elements),
 * so the upper bound must be calculated using the lower bound and the extent.
 * Save the stride information in the type node.
 */
int
f_get_array_desc_array_ubound_and_stride (type, upper_bound)
  struct type * type;
  LONGEST *upper_bound;
{
  LONGEST lower_bound, extent;
  CORE_ADDR addr;
  int retval = f77_get_dynamic_lowerbound (type, &lower_bound);
  if (retval != BOUND_FETCH_OK)
      return retval;
  /* Calculate the address of the extent by adding the offset to the array info
   * structure that describes the array, i.e, TYPE_ARRAY_UPPER_BOUND_VALUE,
   * and incrementing it by the offset to the extent. 
   */
  addr = TYPE_ARRAY_ADDR (type)
         + TYPE_ARRAY_UPPER_BOUND_VALUE (type)
	 + TARGET_PTR_BIT / TARGET_CHAR_BIT;
  extent = read_memory_integer (addr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
  /* Calculate upper bound */
  *upper_bound = extent + lower_bound - 1;
  /* Get the address of the stride which follows the extent, then
   * read in the stride and save it in the array type node
   */
  addr += TARGET_PTR_BIT / TARGET_CHAR_BIT;
  TYPE_ARRAY_STRIDE(type)
    = read_memory_integer (addr, TARGET_PTR_BIT / TARGET_CHAR_BIT); 
  return BOUND_FETCH_OK;
}

/* Get the address of the actual array contents given the address of
 * the array descriptor -- it's at byte offset 0 of the array descriptor.
 */
CORE_ADDR 
f_get_array_desc_array_addr (addr)
  CORE_ADDR addr;
{
  return read_memory_integer (addr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
}

/* Adjust address of array when stride is negative. The address, addr,
 * currently points to first element of the reverse order.  It needs to
 * point to the last element of the reverse order.
 */

CORE_ADDR f_adj_array_addr_w_neg_stride (addr, target_type)
  CORE_ADDR addr;
  struct type *target_type;
{
  addr = addr 
         - TYPE_LENGTH (target_type)
         + TYPE_LENGTH (get_element_type (target_type));
  return addr;
}

/* Adjust the address of valaddr when stride is negative;  valaddr is 
 * the address of the buffer into which the array contents 
 * has been read into; it currently points to the last element of the 
 * reverse order; it needs to point to the first element of the reverse order.
 */
char * 
f_adj_array_valaddr_w_neg_stride (valaddr, target_type)
  char * valaddr;
  struct type *target_type;
  
{
  valaddr = valaddr
            + TYPE_LENGTH (target_type)
            - TYPE_LENGTH (get_element_type (target_type));
  return valaddr;
}

#ifdef HPPA_FIX_AND_CONTINUE
struct activefunc {
  struct symbol * sym;
  struct activefunc * next;
};

struct fixeddatum {
  CORE_ADDR addr;
  int size;
  int oldval, newval;       /* old and new values of the datum */
  struct fixeddatum * next;
};

struct obsoletedsym {
  struct minimal_symbol * oldmsym, *newmsym;
  struct symbol * oldsym, *newsym;
  struct obsoletedsym * next;
};

struct fixedlib {
  struct objfile * objfile;
  value_ptr val;
  char * name;                    /* shared library name */
  struct fixeddatum *firstdatum, *lastdatum;/* modified data */ 
  struct obsoletedsym * obsoletedsym;
  struct fixedlib * prev;
  struct fixedlib * next;
};

/* Data structure to keep track of files being fixed. */
struct fixinfo {
  char * fullname;                 /* full path source name */
  char * objname;
  struct objfile * oldobj;
  struct partial_symtab * oldps;
  CORE_ADDR r19;
  struct activefunc * active_func; /* chain of active functions */
  int libnum;                   /* last digit of shared library name */
  struct fixedlib * library;       /* chain of shared libraries */
  struct fixinfo * next;
};

static struct fixinfo * fixinfo_chain = NULL;
static boolean reapplyfix = false;

struct fixinfo * get_fixinfo_chain ()
{
  return fixinfo_chain;
}

/* Determine if an object needs be fixed. Called by init_import_symbols
   routine when it assign plt addresses. */  
boolean objfile_needs_fix (objfile)
     struct objfile *objfile;
{
   struct fixinfo *fixinfo;
   boolean fix = false;
   for (fixinfo = fixinfo_chain; fixinfo; fixinfo = fixinfo->next)
     {
     if (!strcmp(fixinfo->library->name, objfile->name))
       return true;
     }
   return false;
}

/* Find the chain of active functions. */
static struct activefunc *
find_active_functions (fixinfo)
     struct fixinfo * fixinfo;
{
  struct activefunc * function_chain = NULL;
  CORE_ADDR pc;
  struct frame_info *fi;
  struct symbol *sym;
  struct activefunc *func;
  struct symtab_and_line sal;
  for (fi = get_current_frame(); fi != 0; fi = get_prev_frame (fi))
    {
      sal = find_pc_line (fi->pc, 0);
      if (sal.symtab == NULL)
        continue;
 
      if ((!strcmp (fixinfo->fullname, sal.symtab->filename)) ||
          (!strcmp (getbasename (fixinfo->fullname),
		    getbasename (sal.symtab->filename))))
        {
          sym = find_pc_function (fi->pc);
          if (sym != 0) 
            {
              func = xmalloc (sizeof (struct activefunc));
              func->sym = sym;
              if (function_chain == NULL)
		func->next = NULL;
              else
		func->next = function_chain;
              function_chain = func;
            }
        }
    }
    return function_chain;
}

/* Is a function in the active chain? */
static boolean
in_active_func(name, func_chain)
     char * name;
     struct activefunc *func_chain;
{
  struct activefunc * func;
  for (func = func_chain; func != 0; func = func->next)
    {
      if (!strcmp(name, SYMBOL_NAME(func->sym)))
        return true;
    }
  return false;
}

void delete_file (name)
    char * name;
{
  char * command;
  command = (char *) alloca (strlen (name) +10);
  strcpy (command, "/bin/rm ");
  strcat (command, name);
  system (command);
}

#define FIXERR "\nFix Failed."
/* When error occurs, delete the fixed information which is no longer necessary.*/
static void delete_fixinfo (fixinfo, message)
    struct fixinfo * fixinfo;
    char * message;
{
  struct fixinfo * curfixinfo, * prevfixinfo = NULL;
  struct activefunc * curactivefunc, * nextactivefunc;
  struct fixedlib * nextlib;
  struct fixeddatum * datum, * nextdatum;
  struct obsoletedsym * obsoletedsym, * nextobsoletedsym;

  for (curfixinfo = fixinfo_chain; curfixinfo; curfixinfo = curfixinfo->next)
    {
      if (curfixinfo == fixinfo && fixinfo->library != NULL)
        {
          delete_file (fixinfo->objname);
          delete_file (fixinfo->library->name);
          free (fixinfo->library->name);

          for (datum = fixinfo->library->firstdatum; datum;)
            {
              if (target_write_memory (datum->addr,
                                       (char *)&(datum->oldval), 
                                       datum->size))
                error ("Fix: \"%s\", Can't restore value back %s ",
                       fixinfo->fullname, FIXERR);
              nextdatum = datum->next;
              free (datum);
              datum = nextdatum;
            }

          for (obsoletedsym = fixinfo->library->obsoletedsym; obsoletedsym;)
            {
              MSYMBOL_OBSOLETED(obsoletedsym->oldmsym) = false;
              MSYMBOL_OBSOLETED(obsoletedsym->newmsym) = true;
              SYMBOL_OBSOLETED(obsoletedsym->oldsym) = false;
              SYMBOL_OBSOLETED(obsoletedsym->newsym) = true;
              nextobsoletedsym = obsoletedsym->next;
              free (obsoletedsym);
              obsoletedsym = nextobsoletedsym;
            }

          if (fixinfo->library->val != NULL)
            {
              value_ptr val, *args;
              val = find_function_in_inferior ("shl_unload");
              args = (value_ptr *) alloca (sizeof (value_ptr));
              args[0] = fixinfo->library->val;
              call_function_by_hand (val, 1, args);
#ifdef GDB_TARGET_IS_HPPA_20W
              pa64_solib_remove (fixinfo->library->objfile);
#else
              som_solib_remove (fixinfo->library->objfile);
#endif
            }
          nextlib = fixinfo->library->next;
          free (fixinfo->library);
          fixinfo->library = nextlib;
          if (nextlib != 0)
            {
              if (message != NULL)
                error ("Fix: \"%s\", %s %s ", fixinfo->fullname, message, FIXERR);
              else
                error ("%s", FIXERR);
              break;
            }
          for (curactivefunc = fixinfo->active_func; curactivefunc;)
            {
              nextactivefunc = curactivefunc->next;
              free (curactivefunc);
              curactivefunc = nextactivefunc;
            }
  
          if (prevfixinfo == NULL)
            fixinfo_chain = curfixinfo->next;
          else
            prevfixinfo->next = curfixinfo->next;
           
          if (message != NULL)
            printf_filtered ("Fix: \"%s\", %s", fixinfo->fullname, message);
          free (fixinfo->objname);
          free (fixinfo->fullname);
          free (curfixinfo);
          error ("%s", FIXERR);
          break;
        }
      else
        prevfixinfo = curfixinfo;
    }
}

/* Record the value of a memory location, and update it with the new value. */
static void
updatedatum (fixinfo, addr, newval, size)
    struct fixinfo * fixinfo;
    CORE_ADDR addr;
    char * newval;
    int size;
{ 
  struct fixeddatum * fixeddatum;
  char buf [8];
  int oldval;
  target_read_memory (addr, buf, size);
  oldval = extract_unsigned_integer (buf, size);
  if (target_write_memory (addr, newval, size))
    delete_fixinfo (fixinfo, "Can't redirect function");
  fixeddatum = xmalloc (sizeof (struct fixeddatum));
  fixeddatum->next = NULL;
  fixeddatum->addr = addr;
  fixeddatum->size = size;
  fixeddatum->oldval = oldval;
  fixeddatum->newval = *newval;
  if (fixinfo->library->firstdatum == NULL)
    fixinfo->library->firstdatum = fixinfo->library->lastdatum = fixeddatum;
  else
    {
       fixinfo->library->lastdatum->next = fixeddatum;
       fixinfo->library->lastdatum = fixeddatum;
    }
}

/* Redirect a function to its new definition. New instructions are written
   out from the first instruction of a function if there is enough space.
   Otherwise, redirect the function to __wdb_call_dummy which will then
   redirect fucntion to the new location. A function has at least three
   instructions in its prologue code to allow the redirction to text_end.
*/   
extern CORE_ADDR wdb_call_dummy_start_addr;
extern CORE_ADDR wdb_call_dummy_avail_addr;

static void
fixup_func (fixinfo, name, new_sym, old_sym, active)
     struct fixinfo * fixinfo;
     char * name;
     struct symbol * new_sym, * old_sym;
     boolean active;
{
  struct unwind_table_entry * hppa_find_unwind_entry (CORE_ADDR);
  struct unwind_table_entry * old_unwind_entry, * new_unwind_entry;
  int i, inst, fixed_inst_size;
  struct fixedfunc * func;
  CORE_ADDR oldfuncstart, oldfuncend, newfuncstart, new_stub, solib_handle,
            fixup_addr, pc,
            prologue_begin, prologue_end;
  struct minimal_symbol * msym;
  extern CORE_ADDR text_end;
#ifndef GDB_TARGET_IS_HPPA_20W
  CORE_ADDR stub_addr;
#endif
  struct symname * symname;
  struct obsoletedsym * obsoletedsym;

  oldfuncstart = BLOCK_START (SYMBOL_BLOCK_VALUE (old_sym));
  oldfuncend = BLOCK_END (SYMBOL_BLOCK_VALUE (old_sym));
#ifndef GDB_TARGET_IS_HPPA_20W
  if ((stub_addr = lookup_minimal_symbol_solib_export_trampoline
                   (name, fixinfo->library->objfile)))
    newfuncstart = stub_addr;
  else
#endif
    newfuncstart = BLOCK_START (SYMBOL_BLOCK_VALUE (new_sym));
  pc = read_pc();

/* Generate BVE for PA 2.0N and PA2.0W, BV for PA1.1N. */
/* Haven't found architecture version information yet, the following code is
   intended for 2.0N and 2.0W. */
#ifdef GDB_TARGET_IS_HPPA_20W
  fixed_inst_size = 44;
#else
  if (stub_addr)
    fixed_inst_size = 32;
  else
    fixed_inst_size = 20;
#endif
  if ((oldfuncend - oldfuncstart <= fixed_inst_size) ||
      (active && pc >= oldfuncstart && pc <= oldfuncend && pc < oldfuncstart + fixed_inst_size)) 
    {
      if (active)
        {
          if (pc <= oldfuncstart + 8)
            {
#ifdef GDB_TARGET_IS_HPPA_20W
              /* An internal breakpoint is created at the starting address of
                 the program when a program is restarted. It is O.K. to fix
                 the function.
              */
              if (!reapplyfix || 
                  (oldfuncstart != bfd_get_start_address (symfile_objfile->obfd)))
#endif
		delete_fixinfo(fixinfo, "Not enough space to redirect function");
	    }
          else
            {
              find_pc_partial_function (pc, 0, &prologue_begin, 0);
              prologue_end = skip_prologue_hard_way (prologue_begin);
              if (prologue_end <= prologue_begin + 4)
                delete_fixinfo (fixinfo, "The prologue code is less than three instructions");
            }
        }

        if (wdb_call_dummy_avail_addr == -1)
          {
            msym = lookup_minimal_symbol ("__wdb_call_dummy", NULL, NULL);
            if (msym)
              {
                wdb_call_dummy_start_addr = SYMBOL_VALUE_ADDRESS(msym);
                wdb_call_dummy_avail_addr = wdb_call_dummy_start_addr;
              }
            else
              delete_fixinfo (fixinfo, "wdb_call_dummy was not found.\n");
          }
        fixup_addr = wdb_call_dummy_avail_addr;
        wdb_call_dummy_avail_addr += fixed_inst_size;

        inst = 0x22c00000; /* LDIL fixup_addr, r22 */
#ifdef GDB_TARGET_IS_HPPA_20W
        i = (int) (fixup_addr & 0xffffffffLL) >> 11;
        inst = deposit_21 (i, inst);
#else
        inst = deposit_21 (fixup_addr >> 11, inst);
#endif
        updatedatum (fixinfo, oldfuncstart, (char *)&inst, 4);
    
        inst = 0x36d60000; /* LDO fixup_addr(r22), r22 */
#ifdef GDB_TARGET_IS_HPPA_20W
        i = (int) (fixup_addr & 0xffffffffLL);
        inst = deposit_14 (i & MASK_11, inst);
#else
        inst = deposit_14 (fixup_addr & MASK_11, inst);
#endif
        updatedatum (fixinfo, oldfuncstart + 4, (char *)&inst, 4);
        
#ifdef GDB_TARGET_IS_HPPA_20W
        inst = 0x23600000; /* LDIL fixup_addr, r27 */
        i = (int)((fixup_addr >> 32) >> 11);
        inst = deposit_21 (i, inst);
        updatedatum (fixinfo, oldfuncstart + 8, (char *)&inst, 4);
        inst = 0x377b0000; /* LDO fixup_addr(r27), r27 */
        i = (int) (fixup_addr >> 32);
        inst = deposit_14 (i & MASK_11, inst);
        updatedatum (fixinfo, oldfuncstart + 12, (char *)&inst, 4);
        inst = 0xf2db0c00; /* DEPD r27,31,32,r22 */
        updatedatum (fixinfo, oldfuncstart + 16, (char *)&inst, 4);
#endif

#ifndef GDB_TARGET_IS_HPPA_20W
        if (stub_addr)
          inst = 0xe2c00002; /* BE,n (sr0, r22) */
        else
#endif
	  inst = 0xeac0d002; /* BVE,n (r22) */
#ifdef GDB_TARGET_IS_HPPA_20W
        updatedatum (fixinfo, oldfuncstart + 20, (char *)&inst, 4);
#else
        updatedatum (fixinfo, oldfuncstart + 8, (char *)&inst, 4);
#endif
    }
  else
    fixup_addr = oldfuncstart; 

  if (!fixinfo->r19)
#ifdef GDB_TARGET_IS_HPPA_20W
   fixinfo->r19 = pa64_solib_get_got_by_pc (newfuncstart);
#else
   fixinfo->r19 = som_solib_get_got_by_pc (newfuncstart);
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
  inst = 0x23600000; /* LDIL fixinfo->r19, r27 */
  i = (int)(fixinfo->r19 & 0xffffffffLL) >> 11;
  inst = deposit_21 (i, inst);
  updatedatum (fixinfo, fixup_addr, (char *)&inst, 4);

  inst = 0x377b0000; /* LDO fixinfo->off(r27), r27 */
  i = (int)(fixinfo->r19 & 0xffffffffLL);
  inst = deposit_14 (i & MASK_11, inst);
  updatedatum (fixinfo, fixup_addr + 4, (char *)&inst, 4);
#else
  inst = 0x22600000; /* LDIL fixinfo->r19, r19 */
  inst = deposit_21 (fixinfo->r19 >> 11, inst);
  updatedatum (fixinfo, fixup_addr, (char *)&inst, 4);
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
  inst = 0x22c00000; /* LDIL fixinfo->r19, r22 */
  i = (int)((fixinfo->r19 >> 32) >> 11);
  inst = deposit_21 (i, inst);
  updatedatum (fixinfo, fixup_addr + 8, (char *)&inst, 4);

  inst = 0x36d60000; /* LDO fixinfo->r22(r22), r22 */
  i = (int)(fixinfo->r19 >> 32);
  inst = deposit_14 (i & MASK_11, inst);
  updatedatum (fixinfo, fixup_addr + 12, (char *)&inst, 4);

  inst = 0xf3760c00; /* DEPD r22,31,32,r27 */
  updatedatum (fixinfo, fixup_addr + 16, (char *)&inst, 4);
#else
  inst = 0x36730000; /* LDO fixinfo->r19(r19), r19 */
  inst = deposit_14 (fixinfo->r19 & MASK_11, inst);
  updatedatum (fixinfo, fixup_addr + 4, (char *)&inst, 4);
#endif

  inst = 0x22c00000; /* LDIL new_func, r22 */
#ifdef GDB_TARGET_IS_HPPA_20W
  i = (int)(newfuncstart & 0xffffffffLL) >> 11;
  inst = deposit_21 (i, inst);
  updatedatum (fixinfo, fixup_addr + 20, (char *)&inst, 4);

  inst = 0x36d60000; /* LDO off(r22), r22 */
  i = (int)(newfuncstart & 0xffffffffLL);
  inst = deposit_14 (i & MASK_11, inst);
  updatedatum (fixinfo, fixup_addr + 24, (char *)&inst, 4);

  inst = 0x23800000; /* LDIL new_func, r28 */
  i = (int)((newfuncstart >> 32) >> 11);
  inst = deposit_21 (i, inst);
  updatedatum (fixinfo, fixup_addr + 28, (char *)&inst, 4);

  inst = 0x379c0000; /* LDO fixinfo->off(r28), r28 */
  i = (int)(newfuncstart >> 32);
  inst = deposit_14 (i & MASK_11, inst);
  updatedatum (fixinfo, fixup_addr + 32, (char *)&inst, 4);

  inst = 0xf2dc0c00; /* DEPD r28,31,32,r22 */
  updatedatum (fixinfo, fixup_addr + 36, (char *)&inst, 4);
#else
  inst = 0x22c00000; /* LDIL new_func, r22 */
  inst = deposit_21 (newfuncstart >> 11, inst);
  updatedatum (fixinfo, fixup_addr + 8, (char *)&inst, 4);

  inst = 0x36d60000; /* LDO new_func(r22), r22 */
  inst = deposit_14 (newfuncstart & MASK_11, inst);
  updatedatum (fixinfo, fixup_addr + 12, (char *)&inst, 4);
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
  inst = 0xeac0d002; /* BVE,n (r22) */
  updatedatum (fixinfo, fixup_addr + 40, (char *)&inst, 4);
#else
  if (stub_addr)
    {
      inst = 0x02c010bf; /*  ldsid 0(r22), r31 */
      updatedatum (fixinfo, fixup_addr + 16, (char *)&inst, 4);
      inst = 0x001f1820; /*  mtsp r31,sr0 */
      updatedatum (fixinfo, fixup_addr + 20, (char *)&inst, 4);
      /* Fixes are reapplied when a program is restarted. Since "main" is
         called from _start+168 instead from not its stub, do not restore
         rp to the stub RP.
      */ 
      if (reapplyfix && (!strcmp (name, "main")))
        {
          inst = 0xe2c00002; /* BE,N (sr0, r22) */
          updatedatum (fixinfo, fixup_addr + 24, (char *)&inst, 4);
        }
      else
        {
          inst = 0xe2c00000; /* BE (sr0, r22) */
          updatedatum (fixinfo, fixup_addr + 24, (char *)&inst, 4);
          inst = 0x6bc23fd1; /* STW rp,-24(sp) */
          updatedatum (fixinfo, fixup_addr + 28, (char *)&inst, 4);
        }
    }
  else
    {
      inst = 0xeac0d002; /* BVE,n (sr0, r22) */
      updatedatum (fixinfo, fixup_addr + 16, (char *)&inst, 4);
    }
#endif

  SYMBOL_OBSOLETED (old_sym) = true;
  msym = lookup_minimal_symbol_by_pc (oldfuncstart);
  if (msym)
    MSYMBOL_OBSOLETED (msym) = true;
  obsoletedsym = xmalloc (sizeof ( struct obsoletedsym)); 
  obsoletedsym->oldsym = old_sym;
  obsoletedsym->newsym = new_sym;
  obsoletedsym->oldmsym = msym;
  msym = lookup_minimal_symbol_by_pc (newfuncstart);
  obsoletedsym->newmsym = msym;
  if (fixinfo->library->obsoletedsym == NULL)
    obsoletedsym->next = NULL; 
  else
    obsoletedsym->next = fixinfo->library->obsoletedsym;
  fixinfo->library->obsoletedsym = obsoletedsym;

  old_unwind_entry = hppa_find_unwind_entry (SYMBOL_VALUE_ADDRESS (old_sym));
  new_unwind_entry = hppa_find_unwind_entry (SYMBOL_VALUE_ADDRESS (new_sym));
  if (old_unwind_entry != 0 && new_unwind_entry != 0 &&
#ifdef GDB_TARGET_IS_HPPA_20W
      old_unwind_entry->alloca_frame == 0 &&
      new_unwind_entry->alloca_frame != 0)
#else
      old_unwind_entry->Pseudo_SP_Set == 0 &&
      new_unwind_entry->Pseudo_SP_Set != 0)
#endif
    {
      printf_filtered ("Fix: \"%s\":%s didn't use \"alloca\" before.\n",
                        fixinfo->fullname, SYMBOL_NAME (old_sym));
      delete_fixinfo (fixinfo, NULL);
    }
}

/* Remove the type prefix - struct, union, class from a type name in order
   to check any modification of types. It is necessary to do so because
   a prefix may be added by hpread_type_lookup routine which may or may not
   be called in some cases.
*/ 

static boolean
remove_type_prefix (name1, name2, prefix)
     char **name1, **name2, *prefix;
{
  char * p1 = strstr (*name1, prefix);
  char * p2 = strstr (*name2, prefix);
  if (p1 != 0 || p2 != 0)
    {
      if (p1)
        *name1 += strlen (prefix);
      if (p2)
        *name2 += strlen (prefix);
      return true;
    }
  return false;
}

int get_func_line (filename, funcname)
     char * filename;
     char * funcname;
{
  struct symtabs_and_lines sals;
  char * tmp = (char *) alloca (strlen (filename) + strlen (funcname) + 10);
  strcpy (tmp, filename);
  strcat (tmp, ":");
  strcat (tmp, funcname);
  sals = decode_line_spec_1 (tmp, 0);
  return sals.sals[0].line;
}

/* Check if any restrictions are violated before applying the fix.
   Fixes are applied as restrictions are checked in order to save time.
   If any error occurs, the fixed data will be reverted to the old data.
*/
static void
check_restrictions (fixinfo, library) 
     struct fixinfo *fixinfo;
     struct fixedlib *library;
{
  struct objfile *oldobj = fixinfo->oldobj;
  struct objfile *newobj = library->objfile;
  struct symtab *oldsymtab, *newsymtab = NULL;
  struct blockvector *oldbv, *newbv;
  struct block *oldblock, *newblock;
  struct symbol *oldsym = 0, *newsym;
  struct minimal_symbol *msym;
  int i, j, k, m;
  char *symname, *funcname, *oldname, *newname, *tmp;
  boolean active;
  struct fixedfunc *fixedfunc = NULL;
  CORE_ADDR symaddr, newstub, solibhandle, r19;
  char buf[8];
  unsigned int dl_header [18];

  fixinfo->active_func = find_active_functions (fixinfo);
  ALL_OBJFILE_SYMTABS (newobj, newsymtab)
    {
      /* global symbols */
      int value;
      newbv = BLOCKVECTOR (newsymtab);
      newblock = BLOCKVECTOR_BLOCK (newbv, GLOBAL_BLOCK);
      for (j = 0; j < BLOCK_NSYMS (newblock); j++)
        {
          symname = SYMBOL_NAME (BLOCK_SYM (newblock, j));
          newsym = lookup_block_symbol (newblock, symname, VAR_NAMESPACE);
          /* Ignore type definitions. */
          if (newsym && (SYMBOL_CLASS (newsym) != LOC_TYPEDEF))
            {
              ALL_OBJFILE_SYMTABS (oldobj, oldsymtab)
                {
                  oldbv = BLOCKVECTOR (oldsymtab);
                  oldblock = BLOCKVECTOR_BLOCK (oldbv, GLOBAL_BLOCK);
                  if (oldblock != newblock)
                    {
                      oldsym = lookup_block_symbol (oldblock, symname, VAR_NAMESPACE);
                      if (oldsym)
                        break;
                    }
                }
              if (!oldsym)
                continue;

              /* Fixup a function. */
              if (TYPE_CODE (SYMBOL_TYPE (newsym)) == TYPE_CODE_FUNC)
                fixup_func (fixinfo, symname, newsym, oldsym,
                             in_active_func (symname, fixinfo->active_func));

              /* Check the type of a global variable. */
              if (strcmp (TYPE_NAME (SYMBOL_TYPE (oldsym)),
                           TYPE_NAME (SYMBOL_TYPE (newsym))))
                {
                  printf_filtered ("Fix error: \"%s\": Changing the type name of a global variable (%s) is not supported.\n",
                                   fixinfo->fullname,
                                   SYMBOL_SOURCE_NAME (BLOCK_SYM (newblock, j)));
                  delete_fixinfo (fixinfo, NULL);
                }
            }
        }

      /* file static symbols */
      newblock = BLOCKVECTOR_BLOCK (newbv, STATIC_BLOCK);
      for (j = 0; j < BLOCK_NSYMS (newblock); j++)
        {
          symname = SYMBOL_NAME (BLOCK_SYM (newblock, j));
          newsym = lookup_block_symbol (newblock, symname, VAR_NAMESPACE);
          if (newsym == NULL)
            {
              printf_filtered ("Fix error: \"%s\": Internal error: No symbol was found for '%s'.\n",
                               fixinfo->fullname,
                               SYMBOL_SOURCE_NAME(BLOCK_SYM(newblock, j)));
              delete_fixinfo (fixinfo, NULL);
            }

          if (SYMBOL_CLASS (newsym) == LOC_CONST)
            continue;
          oldsym = lookup_symbol (symname, NULL, VAR_NAMESPACE, NULL, NULL);
          if (oldsym == NULL)
            {
              printf_filtered ("Fix error: \"%s\": Internal error: No symbol was found for '%s'.\n",
                               fixinfo->fullname,
                               SYMBOL_SOURCE_NAME(BLOCK_SYM(newblock, j)));
              delete_fixinfo (fixinfo, NULL);
            }

          if (TYPE_CODE (SYMBOL_TYPE (newsym)) == TYPE_CODE_FUNC)
            fixup_func (fixinfo, symname, newsym, oldsym,
                         in_active_func (symname, fixinfo->active_func));
          else if ((SYMBOL_CLASS (oldsym) != LOC_CONST) &&
                   (SYMBOL_CLASS (oldsym) != LOC_CONST_BYTES) &&
                   (SYMBOL_CLASS (oldsym) != LOC_REGISTER) &&
                   (SYMBOL_CLASS (oldsym) != LOC_REGPARM) &&
                   (SYMBOL_CLASS (oldsym) != LOC_REGPARM))
            {
              /* Copy the value at the old location to the new
                 location. */
              CORE_ADDR oldaddr, newaddr;
              oldaddr = SYMBOL_VALUE_ADDRESS (oldsym);
              newaddr = SYMBOL_VALUE_ADDRESS (newsym);
              if (SYMBOL_CLASS (oldsym) == LOC_INDIRECT)
                {
                  if (oldaddr == NULL)
#ifdef FIND_VAR_IN_SOLIB
                    oldaddr = FIND_VAR_IN_SOLIB (SYMBOL_SOURCE_NAME (oldsym));
#else
                    oldaddr = read_memory_unsigned_integer
                              (oldaddr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
#endif

                  if (newaddr == NULL)
#ifdef FIND_VAR_IN_SOLIB
                    newaddr = FIND_VAR_IN_SOLIB (SYMBOL_SOURCE_NAME (newsym));
#else
                    newaddr = read_memory_unsigned_integer
                              (newaddr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
#endif
                }

              if (oldaddr != NULL && newaddr != NULL)
                {
                  int size = sizeof (CORE_ADDR);
                  CORE_ADDR oldval = read_memory_unsigned_integer
                                     (oldaddr, size);
                  updatedatum (fixinfo, newaddr,
                               (char *)&oldval,
                               size);
                }
            }

          if ((SYMBOL_CLASS (oldsym) != LOC_CONST) &&
              (SYMBOL_CLASS (oldsym) != LOC_TYPEDEF) &&
              (strcmp (TYPE_NAME (SYMBOL_TYPE (oldsym)),
                       TYPE_NAME (SYMBOL_TYPE (newsym))))) 
            {
              printf_filtered ("Fix error: \"%s\", function %s: Changing the type name of a file static variable (%s) is not supported.",
                               fixinfo->fullname,
                               SYMBOL_SOURCE_NAME (BLOCK_SYM (newblock, j)),
                               SYMBOL_SOURCE_NAME (oldsym));
              delete_fixinfo (fixinfo, NULL);
            }
        }

      /* local symbols */
      for (i = FIRST_LOCAL_BLOCK; i < BLOCKVECTOR_NBLOCKS (newbv); i++)
        {
          newblock = BLOCKVECTOR_BLOCK (newbv, i);
          funcname = SYMBOL_SOURCE_NAME (BLOCK_FUNCTION (newblock));
          if (funcname == NULL)
            continue;

          active = in_active_func (funcname, fixinfo->active_func);

          ALL_OBJFILE_SYMTABS (oldobj, oldsymtab)
            {
              oldbv = BLOCKVECTOR (oldsymtab);
              for (k = FIRST_LOCAL_BLOCK; k < BLOCKVECTOR_NBLOCKS (oldbv); k++)
                {
                  oldblock = BLOCKVECTOR_BLOCK (oldbv, k);
                  if (!strcmp (funcname,
                               SYMBOL_SOURCE_NAME (BLOCK_FUNCTION (oldblock))))                  
                    {
                      for (m = 0; m < BLOCK_NSYMS (oldblock); m++)
                        {
                          symname = SYMBOL_SOURCE_NAME (BLOCK_SYM (oldblock, m));
                          oldsym = lookup_block_symbol (oldblock, symname,
                                   VAR_NAMESPACE);
                          if (oldsym == NULL)
                            {
                              printf_filtered ("Fix error: \"%s\", function %s: Internal error: Can't find symbol for '%s'.\n",
                                               fixinfo->fullname,
                                               funcname,
                                               symname);
                              delete_fixinfo (fixinfo, NULL);
                            }
                          newsym = lookup_block_symbol (newblock, symname,
                                   VAR_NAMESPACE);
                          if (active && newsym == NULL)
                            {
                              printf_filtered ("Fix error: \"%s\", function %s: Deleting a variable (%s) from a function (%s) that is active on the stack is not supported.\n",
                                               fixinfo->fullname, funcname,
                                               symname, funcname);
                              delete_fixinfo (fixinfo, NULL);
                            }
                          else
                            {
                              if (active &&
                                  (SYMBOL_CLASS (oldsym) != LOC_STATIC) &&
                                  (SYMBOL_VALUE_ADDRESS (oldsym) !=
                                   SYMBOL_VALUE_ADDRESS (newsym)))
                                {
                                  printf_filtered ("Fix error: \"%s\", function %s: Changing the address of a variable (%s) from a function (%s) that is active on the stack is not supported.\n",
                                                   fixinfo->fullname,
                                                   funcname, symname, funcname);
                                  delete_fixinfo (fixinfo, NULL);
                                }

                              oldname = TYPE_NAME (SYMBOL_TYPE (oldsym));
                              newname = TYPE_NAME (SYMBOL_TYPE (newsym));
                              if (strcmp (oldname, newname))
                                {
                                   if (remove_type_prefix (&oldname,
                                       &newname, "struct ") ||
                                       remove_type_prefix (&oldname,
                                       &newname, "union ") ||
                                       remove_type_prefix (&oldname,
                                       &newname, "class "))
                                    {
                                     if (strcmp (oldname, newname))
                                       {
                                         printf_filtered ("Fix error: \"%s\", line %d: function %s: Changing the type name of a variable/argument (%s) of a function (%s) is not supported.\n",
                                                          fixinfo->fullname,
                                                          get_func_line(
                                                          fixinfo->fullname,
                                                          funcname),
                                                          funcname, symname, funcname);
                                         delete_fixinfo (fixinfo, NULL);
                         
                                       }
                                     }
                                   else
                                     {
                                       printf_filtered ("Fix error: \"%s\", line %d: function %s: Changing the type name of a variable/argument (%s) of a function (%s) is not supported.\n",
                                              fixinfo->fullname,
                                              get_func_line (fixinfo->fullname,
                                                             funcname),
                                              funcname, symname, funcname);
                                       delete_fixinfo (fixinfo, NULL);
                                     }
                                }
                            }
                        }

		      if (active)
			for (m = 0; m < BLOCK_NSYMS (newblock); m++)
			  {
			    symname = SYMBOL_SOURCE_NAME (BLOCK_SYM (newblock, m));
			    newsym = lookup_block_symbol (newblock, symname,
							  VAR_NAMESPACE);
			    oldsym = lookup_block_symbol (oldblock, symname,
							  VAR_NAMESPACE);
			    if (oldsym == NULL)
			      if (SYMBOL_CLASS (newsym) == LOC_LOCAL)
			        {
				  printf_filtered ("Fix error: \"%s\" line%d:, function %s: Adding a variable (%s) to a function (%s) that is active on the stack is not supported.\n",
						   fixinfo->fullname,
						   get_func_line(fixinfo->fullname, funcname),
						   funcname, symname, funcname);
				  delete_fixinfo (fixinfo, NULL);
				}
			      else
                                {
				  printf_filtered ("Fix error: \"%s\", line %d, function %s: Adding a parameter (%s) to a function (%s) that is active on the stack is not supported.\n",
						   fixinfo->fullname,
						   get_func_line (fixinfo->fullname, funcname),
						   funcname, symname, funcname);
				  delete_fixinfo (fixinfo, NULL);
				}
			  }
                      break;
                    }
                }
            }
        }
    }

#ifndef GDB_TARGET_IS_HPPA_20W
  /* Patch up PLT entries for each imported functions. */
  bfd_get_section_contents (newobj->obfd, 
                            bfd_get_section_by_name (newobj->obfd,
                                                     "$SHLIB_INFO$"),
                            dl_header, 0, 18 * sizeof (int));
  for (m = 0 ; m < (newobj->first_import_index_for_static > 0 ?
                    newobj->first_import_index_for_static :
                    newobj->import_list_size); m++)
    {
      if (newobj->import_list[m].name != 0)
        {
          if (m < dl_header [16])
            {
              msym = lookup_minimal_symbol (newobj->import_list[m].name, NULL,
                                            fixinfo->oldobj);
              if (!msym)
                /* If a datum is not defined in the new object file,
                   look for the all the object files. */
                msym = lookup_minimal_symbol (newobj->import_list[m].name, NULL,
                                            NULL);
            }
          else
            /* Look for a minimal symbol of mst_text type. */
            msym = quick_lookup_minimal_symbol (newobj->import_list[m].name,
                                                NULL, NULL, mst_text);
          if (msym)
            {
              symaddr = SYMBOL_VALUE_ADDRESS (msym);
              if (m < dl_header [16])
                {
                  if (SYMBOL_CLASS (msym) == LOC_INDIRECT)
                    {
                      if (symaddr == 0)
#ifdef FIND_VAR_IN_SOLIB
                        symaddr = FIND_VAR_IN_SOLIB (SYMBOL_SOURCE_NAME (msym)
);
#else
                        symaddr = read_memory_unsigned_integer
                                  (symaddr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
#endif

                    }
                  if (symaddr)
                    {
                    updatedatum (fixinfo,
                                 newobj->import_list[m].addr.dlt_addr,
                                 (char *)&symaddr, sizeof (CORE_ADDR));
                    }
                }
              else
                {
		  if (symaddr)
		    {
                      struct objfile * objfile = som_solib_get_objfile_by_pc
                                                 (symaddr);
                      /* Do not patch up entries in the non user loaded
                         libraries. */
                      if (objfile != NULL &&
			  !(objfile->flags & OBJF_USERLOADED) &&
                          (strstr (objfile->name, "/libc.")))
                        continue;
                      solibhandle = som_solib_get_solib_by_pc (symaddr);
                      if (solibhandle == 0)
                        {
                          /* The imported function is not in a shared library.
                             If the function is called from a shared library,
                             and returns to the caller through a BV instruction,
                             a SIGSEV would occur because of the different
                             space id's.

                             The two words of a plt entry consists of
                             the imported function address and r19.

                             To handle the case, it is assumed that r19 is
                             not used by archived libraries and a dummy
                             routine is created at __wdb_call_dummy area.
                             A call to the imported function is redirected
                             to the dummy routine which in turns calls the
                             imported function. The callee will return
                             through the dummy routine. The scheme works because
                             __wdb_call_dummy resides in end.o which should
                             have the same space id as the imported function.

                             To implement the scheme, the first word of a plt
                             entry points to the dummy routine and r19 contains
                             the imported function address. The dummy routine
                             will call the imported function by using the value
                             of r19.
                          */
                          if (redirect_through_dummy_addr == -1) 
                            {
                              if (wdb_call_dummy_avail_addr == -1)
                                {
                                  msym = lookup_minimal_symbol ("__wdb_call_dummy", NULL, NULL);
                                  if (msym)
                                    {
                                      wdb_call_dummy_start_addr =
                                        SYMBOL_VALUE_ADDRESS(msym);
                                      wdb_call_dummy_avail_addr =
                                        wdb_call_dummy_start_addr;
                                    }
                                  else
                                    delete_fixinfo (fixinfo,
						    "wdb_call_dummy was not found.\n");
                               }
                              redirect_through_dummy_addr =
				wdb_call_dummy_avail_addr;

                              /* ldsid (%r19), %r1 */
                              *((int *) buf) = 0x026010a1;
                              if (target_write_memory (wdb_call_dummy_avail_addr,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");

                              /* mtsp %r1, %sr0 */
                              *((int *) buf) = 0x00011820;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 4,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* stw %rp, -0x14(%sp) */
                              *((int *) buf) = 0x6bc23fd9;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 8,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* ldo 0x40(%sp),%sp */
                              *((int *) buf) = 0x37de0080;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 12,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* be,l 0(%r19) */
                              *((int *) buf) = 0xe6600000;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 16,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* copy %r31,%rp */
                              *((int *) buf) = 0x37e20000;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 20,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* ldw -0x54(%sp), %rp */
                              *((int *) buf) = 0x4bc23f59;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 24,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* ldsid (%rp), %r1 */
                              *((int *) buf) = 0x004010a1;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 28,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* mtsp %r1, %sr0 */
                              *((int *) buf) = 0x00011820;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 32,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* be (rp) */
                              *((int *) buf) = 0xe0400000;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 36,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
    
                              /* ldo 0x40(%sp),%sp */
                              *((int *) buf) = 0x37de3f81;
                              if (target_write_memory (wdb_call_dummy_avail_addr + 40,
                                  buf, 4) != 0)
                                  delete_fixinfo (fixinfo,
                                    "Can't redirect funciton.\n");
                              wdb_call_dummy_avail_addr += 44;
                            }
                            r19 = symaddr;   
                            symaddr = redirect_through_dummy_addr;
                        }
                      else
                        {
                          r19 = som_solib_get_got_by_pc (symaddr);
                        }
                      updatedatum (fixinfo,
                                   newobj->import_list[m].addr.plt_addr,
                                   (char *)&symaddr, sizeof (CORE_ADDR));
                      /* Update r19. */
                      updatedatum (fixinfo,
                                   newobj->import_list[m].addr.plt_addr +
                                   sizeof (CORE_ADDR),
                                   (char *)&r19, sizeof (CORE_ADDR));
                    }
                }
            }
        }
    }

  /* The following piece of code is derived from dld.c from LINKER/Src/dld
     directory. 
     Patch up the location of a dynamic relocation record with it's plt entry
     address.
  */
  {
#define DR_PLABEL_EXT 1
#define DR_PLABEL_INT 2
#define DR_DATA_EXT   3
#define DR_DATA_INT   4
#define DR_PROPAGATE  5
#define DR_INVOKE     6
#define DR_TEXT_INT   7
  struct dreloc_record {
     int shlib;
     int symbol;
     int location;
     int value;
     unsigned char type;
     char reserved;
     short module_index;
  };
  struct dreloc_record *dreloc_ptr;
  struct dreloc_record *dreloc_entry_ptr = (struct dreloc_record *) 
	alloca (sizeof (struct dreloc_record));
  int dreloc_count = dl_header[13];
  asection * data_section, * text_section;
  CORE_ADDR value;
  int static_func_index = newobj->first_import_index_for_static;

  text_section = bfd_get_section_by_name (newobj->obfd, "$SHLIB_INFO$");
  for (m = 0; m < dreloc_count && m < newobj->import_list_size; m++)
    {
      bfd_get_section_contents (newobj->obfd, text_section,
                                dreloc_entry_ptr,
                                dl_header[12] + 
                                m * sizeof (struct dreloc_record),
                                sizeof (struct dreloc_record));

      switch (dreloc_entry_ptr->type) {
        case DR_PLABEL_EXT :
          value = newobj->import_list[dreloc_entry_ptr->symbol].addr.plt_addr + 2;
          target_write_memory (dreloc_entry_ptr->location +
            ANOFFSET (newobj->section_offsets, SECT_OFF_TEXT (newobj)),
            (char *)&value, sizeof (CORE_ADDR));
          break;
        case DR_PLABEL_INT :
          /* The symbol value for a static function is -1. Make use of
             the first import index for static functions recorded in
             init_import_symbols to determine which import entry should
             the relocation entry belongs to.
             There is a one to one correspondence for the static functions
             between the import list and dynamic relocation records. They
             both reside at the end of the list.
          */
          if (static_func_index <= m)
          {
          msym = lookup_minimal_symbol (
                 newobj->import_list[static_func_index].name, NULL, NULL);
          if (msym)
            {
              target_read_memory (dreloc_entry_ptr->location +
                ANOFFSET (newobj->section_offsets, SECT_OFF_TEXT (newobj)),
                buf, 4);
              newstub = extract_unsigned_integer (buf, 4) - 2;
              symaddr = SYMBOL_VALUE_ADDRESS (msym);
              target_write_memory (newstub, (char *)&symaddr, 4);
              r19 = som_solib_get_got_by_pc (symaddr);
              target_write_memory (newstub + 4, (char *) &r19, 4);
              static_func_index++;
            }
          }
          break;
        default :
          break;
      }
    }
  }
#endif
}

/* Construct a library name for a fixed file. For example, the first library for
   t.c is t.fix.sl0, and the second library is t.fix.sl1, etc. */
static char * get_library (fixinfo)
   struct fixinfo * fixinfo;
{
  char *library, *name, *basefilename;
  char strnum [4];
  int basefilenamelen;

  basefilename = getbasename (fixinfo->fullname);
  basefilenamelen = strlen (basefilename); 
  name = (char *) alloca (basefilenamelen + 50);
  library = xmalloc (strlen (current_directory) + basefilenamelen +
		     strlen(name) + 50);
  strcpy (library, current_directory);
  strcat (library, "/");
  strncat (library, basefilename, basefilenamelen);
  sprintf (name, ".fix.sl%d\0", fixinfo->libnum);
  strncat (library, name, strlen (name));
  fixinfo->libnum = fixinfo->libnum + 1;
  return library;
}

static char *current_library = NULL;

boolean
is_current_fixed_library (name)
     char *name;
{
  if (current_library)
    return (!strcmp (name, current_library));
  else
    return 0;
}

boolean is_fixed_library (name)
     char * name;
{
  return ((name ? (strstr (name, ".fix.sl") != 0) : NULL));
}

/* Load a library. */
value_ptr
load_library (name)
     char * name;
{
  value_ptr * libraryvec, val, * args;
  int i, librarylen = strlen (name);
  val = find_function_in_inferior ("shl_load");
  libraryvec = (value_ptr *) alloca (sizeof (value_ptr) * (librarylen + 2));
  for (i = 0; i < librarylen + 1; i++)
    libraryvec [i] = value_from_longest (builtin_type_char, name[i]);
  args = (value_ptr *) alloca (sizeof (value_ptr) * 3);
  args[0] = value_array (0, librarylen, libraryvec);
  args[1] = value_from_longest (builtin_type_int, BIND_DEFERRED |
                                BIND_VERBOSE | BIND_NONFATAL);
  args[2] = value_from_longest (builtin_type_int, 0);
  current_library = name;
  call_function_by_hand (val, 3, args);
  return val;
}

#ifdef GDB_TARGET_IS_HPPA_20W
  extern struct objfile *pa64_solib_get_objfile_by_name (char *);
#else
  extern struct objfile *som_solib_get_objfile_by_name (char *);
#endif

void load_fixed_libraries ()
{
  struct fixinfo * fixinfo;
  struct fixedlib * library;
  struct fixeddatum * datum;
  struct symname * symname;
  char * basefilename;
  struct partial_symtab * ps;

#ifndef GDB_TARGET_IS_HPPA_20W
  redirect_through_dummy_addr = -1;
#endif

  for (fixinfo = fixinfo_chain; fixinfo; fixinfo = fixinfo->next)
    {
      fixinfo->library->val = load_library (fixinfo->library->name);
#ifdef GDB_TARGET_IS_HPPA_20W
      fixinfo->library->objfile = pa64_solib_get_objfile_by_name
                                  (fixinfo->library->name);
#else
      fixinfo->library->objfile = som_solib_get_objfile_by_name 
                                  (fixinfo->library->name);
#endif
      basefilename = strrchr (fixinfo->fullname, '/') + 1;
      ALL_OBJFILE_PSYMTABS (fixinfo->library->objfile, ps)
        if (!strcmp (fixinfo->fullname, ps->filename) ||
            !strcmp (basefilename, ps->filename)) 
          psymtab_to_symtab (ps);
      check_restrictions (fixinfo, fixinfo->library);
    }
}

#define COMPILE_COMMAND_ERROR "Can't fix \"%s\". You must have a compiler that support 'fix' feature and must compile with -g and must not compile with optimization.\n" 

static boolean
get_compile_command (filename, fullname, objfile, ps)
     char * filename;
     char ** fullname;
     struct objfile * objfile;
     struct partial_symtab * ps;
{
  char * stringtab, * compile_command, * dir = 0, * file,
       * basefilename, *notes_buf, *p;
  int i, j, k = 0, size, res, dirlen = 0, basefilenamelen, status, index;
  struct stat source_stat;
  boolean found = false;
#ifdef GDB_TARGET_IS_HPPA_20W
  extern char *elf_symtab_read_notes (bfd *, int *);
#else
  extern char *som_symtab_read_stringtab (bfd *, int *);
#endif
 
  /* For PA32, the string table contains information for each compilation unit,
     including the base name of a file, its directory, ccom_option and 
     driver_command.

     The driver_command records the compiler and options used to create the
     original executables.

     For example :
     224          t.c
     /tmp/hmc
     ccom options =  -g -Oq00,al,ag,cn
     driver_command =   /opt/ansic/bin/cc -g

     For PA64, the simliar information is in .notes section.
  */
#ifdef GDB_TARGET_IS_HPPA_20W
  stringtab = elf_symtab_read_notes (objfile->obfd, &size);
#else
  stringtab = som_symtab_read_stringtab (objfile->obfd, &size);
#endif

  if ((p = strrchr (filename, '/')))
    {
      basefilename = p + 1;
    }
  else
    basefilename = filename;
  basefilenamelen = strlen(basefilename);
  index = 0;
  restart : for (i = index; i < size && !found; i++)
    {
      /* Look for the file name in the string table. */
      if ((stringtab[i] == basefilename[0]) &&
          (!strncmp (&stringtab[i], basefilename, basefilenamelen)))
        {
#ifdef GDB_TARGET_IS_HPPA_20W
          if (stringtab[i - 1] == ' ' || stringtab[i - 1] == '\t' ||
              stringtab[i - 1] == '\n') 
#endif
            {
              found = true;
              break;
            }
        }
    }

  if (!found && (i == size))
    return false;

  found = false;

#ifdef GDB_TARGET_IS_HPPA_20W
  for (j = i - 1; j >= 0; j--)
#else
  for (j = i - 2; j >= 0; j--)
#endif
    {
      if (stringtab[j] == 0)
        {
          found = true;
          for (k = j; k < i; k++)
            if (stringtab[k] == '/' || stringtab[k] == '.')
              {
                if (stringtab [i - 1] != '/')
                  found = false;
                i = k;
                break;
              }
          if (found)
            {
              for (k = i; ; k++)
              if (stringtab[k] == '\n')
                {
                  break;
                }
            }
          break;
        }
    }

  file = (char *) alloca (k  - i + 100);
  for (j = k - 1; j > i; j--)
    if (stringtab[j] != '\t' && stringtab[j] != ' ')
      break;
  strncpy (file, &stringtab[i], j - i + 1);
  file [j - i + 1] = '\0';
  for (i = k + 1; i < size; i++)
    if (stringtab[i] != '\t' && stringtab[i] != ' ')
      break;
  for (j = i; j < size; j++)
    {
      /* Look for the directory of the source file. */
      if (stringtab[j] == '\n')
        {
          dir = (char *) alloca (j - i + 20);
          *fullname = xmalloc (strlen (file) + strlen (current_directory) + 200);
          dirlen = j - i;
          strncpy (dir, &stringtab[i], dirlen);
          dir[dirlen] = '\0';
          if (file[0] != '/')
            {
              strcpy (*fullname, dir);
              strcat (*fullname, "/");
              strncat(*fullname, file, strlen (file));
            }
          else
            *fullname = strsave(file);
          pathopt (*fullname);
          if ((filename [0] != '/') || !strcmp (filename, *fullname))
            {
              found = true;
              break;
            }
          else
            {
              index = j;
              found = false;
              goto restart;
            }   
        }
    }
  if (!found)
    {
      return false;
    }

  found = false;
  for (i = j; i < size; i++)
    {
      /* Look for the driver_command option. */
      if ((stringtab[i] == 'd') &&
          (strncmp (&stringtab[i], "driver_command", 14) == 0))
        {
          found = true;
          break;
        }
    }

  if (!found)
    error (COMPILE_COMMAND_ERROR, filename);

  found = false;
  for (j = i + 14; i < size; j++)
    if (stringtab[j] == '=')
      {
        found = true;
        if (stringtab[j+1] == '\"') /* aCC has an extra double quote. */
          j += 1;
        break;
      }
  if (!found)
    error ("Wrong syntax for driver_command.%s", FIXERR);
  
  for (i = j + 1; i < size; i++)
#ifdef GDB_TARGET_IS_HPPA_20W
    if ((stringtab[i] == '\n')
#else
    if ((stringtab[i] == 0)
#endif
        || (stringtab [i] == '\"')) /* aCC has an extra ending quote. */
      {
        for (k = j + 1 ; k < i; k++)
          {
            if ((stringtab[k] == '-' && stringtab[k+1] == 'O') ||
                (stringtab[k] == '+' && stringtab[k+1] == 'O'))
              {
                error ("The compilation unit was optimized. It cannot be \'fix\'ed.%s", FIXERR); 
                break;
              }
          }
        
        /* Retrieve the compile options from driver_command. */
        compile_command = xmalloc (i - j + dirlen + strlen (current_directory) 
                                   + 200);
        strcpy (compile_command, dir);
        status = chdir (compile_command);
        if (status != 0)
          error ("Can not change to the compilation directory.%s", FIXERR);
        strncpy (compile_command, &stringtab[j+1], i - j);
        compile_command [i - j - 1] = '\0';
        strcat (compile_command, " +z +ESnolit -c ");
        strncat (compile_command, *fullname, strlen (*fullname));
        strcat (compile_command, " -o ");
        strcat (compile_command, current_directory);
        strcat (compile_command, "/");
        strncat (compile_command, getbasename(filename), basefilenamelen);
        strcat (compile_command, ".fix.o");
        printf_filtered ("Compiling %s...\n", *fullname);
        gdb_flush (gdb_stdout);
        free (stringtab);
        status = system (compile_command);
        if (status != 0)
          error ("Compilation errors occurred.%s", FIXERR);
        return true;
      }
  free (stringtab);
  error (COMPILE_COMMAND_ERROR, fullname);
}

void
fix_file (filename)
     char *filename;
{
  int status, basefilenamelen;
  struct partial_symtab *ps, *oldps = 0;
  char *command;
  struct fixinfo *curfixinfo, *prevfixinfo = NULL;
  struct fixedlib * fixedlib;
  struct breakpoint *b, *bp;
  struct symtab_and_line sal;
  struct symbol *symbol;
  struct minimal_symbol * msym;
  extern struct symbol *lookup_func_symbol_from_objfile (struct objfile *,
							 const char *);
  extern struct symtab_and_line find_function_start_sal (struct symbol * sym,
							 int);
  extern struct breakpoint *breakpoint_chain;
  struct objfile *objfile, *oldobj = NULL;
  CORE_ADDR pc;
  char *fullname, *basefilename, *objname, *sourcename, *p;

  /* Compile the source file. */
  if (!source_full_path_of (filename, &fullname))
    sourcename = savestring (filename, strlen (filename));
  else
    sourcename = filename;
  if ((p = strrchr (sourcename, '/')))
    basefilename = p + 1;
  else
    basefilename = sourcename;
  ALL_PSYMTABS (objfile, ps)
    if (!strcmp (sourcename, ps->filename) ||
        !strcmp (basefilename, getbasename (ps->filename)))
      {
        oldobj = objfile;
        oldps = ps;
        if (get_compile_command(filename, &fullname, oldobj, oldps))
          goto done;
      }
  error ("Fix: No object file was found for %s.\nPerhaps the object file is part of the library that hasn't yet been loaded? (See the shared library command.)%s", filename, FIXERR);

  done :
  
  /* Create the shared library. */
  basefilename = getbasename (filename);
  basefilenamelen = strlen (basefilename);
  command = (char *) alloca (strlen (current_directory) * 2 + strlen (fullname) *2 + 100);
  strcpy (command, "/usr/ccs/bin/ld -b ");
  objname = xmalloc (strlen (current_directory) + basefilenamelen + 50);
  strcpy (objname, current_directory);
  strcat (objname, "/");
  strcat (objname, basefilename);
  strcat (objname, ".fix.o");
  strcat (command, objname);
  strcat (command, " -o ");

  for (curfixinfo = fixinfo_chain, prevfixinfo = fixinfo_chain;
       curfixinfo; curfixinfo = curfixinfo->next)
    {
      if (strcmp (fullname, curfixinfo->fullname))
        prevfixinfo = curfixinfo;
      else
        break;
    }

  if (curfixinfo == NULL)
    {
      char * p;
      curfixinfo = xmalloc (sizeof (struct fixinfo));
      curfixinfo->next = NULL;
      curfixinfo->r19 = 0;
      curfixinfo->libnum = 0;
      curfixinfo->fullname = fullname; 
      curfixinfo->objname = objname; 
      curfixinfo->library = NULL;
      curfixinfo->oldobj = oldobj;
      curfixinfo->oldps = oldps;

      if (fixinfo_chain == NULL)
        fixinfo_chain = curfixinfo;
      if (prevfixinfo != NULL)
        prevfixinfo->next = curfixinfo;
    }

  fixedlib = xmalloc (sizeof (struct fixedlib));
  fixedlib->name = get_library (curfixinfo);
  fixedlib->firstdatum = NULL;
  fixedlib->lastdatum = NULL;
  fixedlib->val = NULL;
  fixedlib->obsoletedsym = NULL;
  fixedlib->prev = NULL;
  fixedlib->next = curfixinfo->library; 
  if (curfixinfo->library == NULL)
    curfixinfo->library = fixedlib;
  else
    {
      ALL_OBJFILE_MSYMBOLS (curfixinfo->library->objfile, msym)
	MSYMBOL_OBSOLETED (msym) = true;
      curfixinfo->library->prev = fixedlib;
    }
  strncat (command, fixedlib->name, strlen (fixedlib->name));
  strcat (command, " 2> /dev/null \0");
  printf_filtered ("Linking...\n");
  status = system (command);
  if (status != 0)
    delete_fixinfo (curfixinfo, "Link error occurred."); 
  
  /* Temporarily disable breakpoints so they can be removed by
     normal_stop routine. */
  for (b = breakpoint_chain; b; b = b->next) 
    {
    if (!strcmp (getbasename (b->source_file),
		 getbasename (curfixinfo->fullname)))
      {
        b->enable = shlib_disabled;
        b->address = 0; /* Prevent breakpoint_re_set_one to reset the 
                           breakpoint. */
	b->inserted = false;
      }
    }

  fixedlib->val = load_library (fixedlib->name);

#ifdef GDB_TARGET_IS_HPPA_20W
  fixedlib->objfile = pa64_solib_get_objfile_by_name (fixedlib->name);
#else
  fixedlib->objfile = som_solib_get_objfile_by_name (fixedlib->name);
#endif

  if (fixedlib->objfile == NULL)
    delete_fixinfo (curfixinfo, "Can't find object file for the fixed file."); 
  psymtab_to_symtab (curfixinfo->oldps);
  ALL_OBJFILE_PSYMTABS (fixedlib->objfile, ps)
    if (!strcmp (curfixinfo->fullname, ps->filename) ||
        !strcmp (basefilename, ps->filename))
      {
        psymtab_to_symtab (ps);
      }
  printf_filtered ("Applying code changes to %s.\n", filename);
  curfixinfo->library = fixedlib;
  check_restrictions (curfixinfo, fixedlib);

  /* Move the breakpoints from the source file to the new source file. */ 
  for (b = breakpoint_chain; b; b = b->next)
    {
      if ((!strcmp (getbasename (b->source_file),
		    getbasename (curfixinfo->fullname))) &&
          (b->addr_string[0] != '*') && /* Can't move an address breakpoint. */
          (symbol = 
           lookup_func_symbol_from_objfile (curfixinfo->library->objfile, b->addr_string))
           != 0)
        {
          pc = BLOCK_START (SYMBOL_BLOCK_VALUE (symbol));
          sal = find_function_start_sal (symbol, 1);
          b->enable = enabled;
          b->address = pc;
          b->line_number = sal.line;
          b->section = sal.section;
        }
    }
  strcpy (command, current_directory);
  status = chdir (command);
  if (status != 0)
     error ("Can not change to the working directory.%s", FIXERR);
}

void fix_pc()
{
  CORE_ADDR pcsqh, pcsqt, pcoqh, pcoqt, sr, r1, dp = 0, pc, npc = 0; 
  struct symtab_and_line sal;
  struct target_waitstatus w;
  int inst1, inst2;
  char buf[8], *symfilename;
  int status;
  int sid;
  struct fixinfo * fixinfo;
  struct symtab * symtab = 0;
  struct minimal_symbol * msym;
  struct symbol * sym;
  boolean pc_fixed = false;
#ifndef GDB_TARGET_IS_HPPA_20W
  CORE_ADDR old_stub_addr = 0, new_stub_addr;
  int inc = 0;
#endif

  sal = find_pc_line (read_pc(), 0); 
  if (sal.symtab->filename == NULL)
    error ("Can't move pc for the fixed file.%s", FIXERR);

  for (fixinfo = fixinfo_chain; fixinfo; fixinfo = fixinfo->next)
    {
      ALL_OBJFILE_SYMTABS (fixinfo->library->objfile, symtab)
        {
          symfilename = getbasename (symtab->filename);
          if ((!strcmp (symtab->filename, sal.symtab->filename)) ||
              (!strcmp (symfilename, getbasename (sal.symtab->filename)))) 
            {
              find_line_pc (symtab, &sal.line, &npc, 0);
              msym = lookup_minimal_symbol_by_pc (npc);
              if (msym)
                {
                  sym  = lookup_symbol(SYMBOL_NAME (msym), NULL, 
                                       VAR_NAMESPACE, NULL, NULL);
#ifdef GDB_TARGET_IS_HPPA_20W
                  dp = pa64_solib_get_got_by_pc (npc);
#else
                  dp = som_solib_get_got_by_pc (npc);
                  if ((new_stub_addr = 
                      lookup_minimal_symbol_solib_export_trampoline
                      (SYMBOL_NAME (sym), fixinfo->library->objfile)))
                    {
                      inc = 0;
                      old_stub_addr =
                        lookup_minimal_symbol_solib_export_trampoline
                        (SYMBOL_NAME (sym), fixinfo->oldobj);
                    }
#endif
                  for (pc = BLOCK_START (SYMBOL_BLOCK_VALUE (sym));
                       pc < npc; pc += 4)
                   {
                     if (target_read_memory (pc, buf, 4) != 0)
                       {
                         printf_filtered("Fix: \"%s\", Can't read %s.",
                              fixinfo->fullname, pc); 
                         delete_fixinfo (fixinfo, NULL);
                       }
                     inst1 = extract_unsigned_integer (buf, 4);
#ifdef GDB_TARGET_IS_HPPA_20W
                     /* std dp,off(sp) */
                     if ((inst1 & 0xffffc000) == 0x73db0000)
#else
                     if (new_stub_addr)
                       inc -= prologue_inst_adjust_sp(inst1);
                     /* stw r19,off(sp) */
                     if ((inst1 & 0xffffc000) == 0x6bd30000)
#endif
                       {
#ifdef GDB_TARGET_IS_HPPA_20W
                         *((long long *) buf) = dp;
#else
                         *((int *) buf) = dp;
#endif
                         if (target_write_memory (read_register (SP_REGNUM) +
                                                  extract_14 (inst1), buf,
                                                  sizeof (CORE_ADDR)) != 0)
                           {
                             printf_filtered ("Fix: \"%s\", Can't update dp.",
                                              fixinfo->fullname);
                             delete_fixinfo (fixinfo, NULL);
                           }
#ifndef GDB_TARGET_IS_HPPA_20W
                         if (new_stub_addr)
                           {
                             if (!old_stub_addr)
                               {
                                 /* If there is no stub for the old function,
                                    copy the return pointer to the stub's
                                    return pointer.
                                 */
                                 if (target_read_memory (
                                     read_register (SP_REGNUM) + inc - 20,
                                     buf, sizeof (CORE_ADDR)) != 0)
                                   {
                                     printf_filtered (
                                       "Fix: \"%s\", Can't read rp.",
                                       fixinfo->fullname);
                                     delete_fixinfo (fixinfo, NULL);
                                   }
                                 if (target_write_memory (
                                     read_register (SP_REGNUM) + inc - 24,
                                     buf, sizeof (CORE_ADDR)) != 0)
                                   {
                                     printf_filtered (
                                       "Fix: \"%s\", Can't update rp'.",
                                       fixinfo->fullname);
                                     delete_fixinfo (fixinfo, NULL);
                                   }
                               }

                             /* If there is an export stub for the
                                current function, the current function
                                should return to the third
                                instruction of the stub.
                                An export stub has the following code :
                                BL,N  X,%rp
                                NOP
                                LDW -24(sp), %rp

                                Therefore, the return pointer on the 
                                stack should be updated.
                             */
                             *((int *) buf) = new_stub_addr + 8;
                             if (target_write_memory (read_register (SP_REGNUM) +
                                                      inc - 20, buf,
                                                      sizeof (CORE_ADDR)) != 0)
                               {
                                 printf_filtered ("Fix: \"%s\", Can't update rp.",
                                                  fixinfo->fullname);
                                 delete_fixinfo (fixinfo, NULL);
                               }
                           }
#endif
                         break;
                       }
                   }
		  pc_fixed = true;
                }
              else
                {
                  printf_filtered ("Fix: \"%s\", Can't find symbol for new pc.",
                                   fixinfo->fullname);
                  delete_fixinfo (fixinfo, NULL);
                }
              break;
            }
        }
    }
  if (!pc_fixed)
    return;

  sid = (npc >> (REGISTER_SIZE*8-2)) & 0x3;
  if (sid == 0)
    sr = read_register (SR4_REGNUM);
  else
    sr = read_register (SR4_REGNUM + 4 + sid);
  pcsqh = read_register(PCSQ_HEAD_REGNUM);
  if ((pcsqh != sr) && (!(IN_SYSCALL(read_register(FLAGS_REGNUM)))))
    {
      pcoqh = read_register(PCOQ_HEAD_REGNUM) & ~0x3;
      pcoqt = read_register(PCOQ_TAIL_REGNUM) & ~0x3;

      if (target_read_memory (pcoqh, buf, 4) != 0)
        error("Couldn't modify space queue\n"); 
      inst1 = extract_unsigned_integer (buf, 4);
        
      if (target_read_memory (pcoqt, buf, 4) != 0)
        error("Couldn't modify space queue\n"); 
      inst2 = extract_unsigned_integer (buf, 4);

      /* BE 0(sr sid+4, r1) */
      *((int *) buf) = 0xe0202000 | (sid << 14);
      if (target_write_memory (pcoqh, buf, 4) != 0)
        error("Couldn't modify space queue\n"); 
          
      /* NOP */
      *((int *) buf) = 0x08000240;
      if (target_write_memory (pcoqt, buf, 4) != 0)
        {
          *((int *) buf) = inst1;
          target_write_memory (pcoqh, buf, 4);
          error("Couldn't modify space queue\n"); 
        }
          
      r1 = read_register(1);
      write_register(1, npc);
       
      /* single step twice */
      target_resume (-1, 1, 0);
      registers_changed ();
      target_wait (inferior_pid, &w);
      target_resume (-1, 1, 0);
      registers_changed ();
      target_wait (inferior_pid, &w);
        
      *((int *) buf) = inst1;
      target_write_memory (pcoqh, buf, 4);
      *((int *) buf) = inst2;
      target_write_memory (pcoqt, buf, 4);
      write_register(1, r1);
#ifdef GDB_TARGET_IS_HPPA_20W
      write_register (DP_REGNUM, dp);
#else
      write_register (19, dp);
#endif
    if (symtab && current_source_symtab)
      current_source_symtab = symtab;
    }
}

void cleanup_fixinfo ()
{
  struct fixinfo *fixinfo, *nextfixinfo;
  struct stat source_stat;
  int res;
  char * command = NULL;
  struct fixedlib * fixedlib;
  struct fixeddatum * datum, * nextdatum;
  if (fixinfo_chain == NULL)
    return;

  printf_filtered ("The following modules in %s have been fixed :\n",
                   symfile_objfile->name);
  for (fixinfo = fixinfo_chain; fixinfo;)
    {
      if (command == NULL)
        command = (char *) alloca (strlen (fixinfo->library->name) + 100);
      strcpy (command, "/bin/rm ");
      strncat (command, fixinfo->objname,
               strlen(fixinfo->objname));  
      system (command);

      for (fixedlib = fixinfo->library; fixedlib; fixedlib = fixedlib->next)
        {
          strcpy (command, "/bin/rm ");
          strncat (command, fixedlib->name, strlen (fixedlib->name));
          system (command);
          free (fixedlib->name);
          for (datum = fixedlib->firstdatum; datum;)
            {
              nextdatum = datum->next;
              free (datum);
              datum = nextdatum;
            }
        }
      nextfixinfo = fixinfo->next;
      printf_filtered (" %s\n", fixinfo->fullname);
      free (fixinfo);
      fixinfo = nextfixinfo;
    }
  printf_filtered ("Remember to remake the program.\n");
}

void set_reapply_fix (val)
  boolean val;
{
  if (fixinfo_chain)
    reapplyfix = val;
}

boolean get_reapply_fix ()
{
  return reapplyfix;
}

void check_unapplied_changes ()
{
  struct fixinfo *fixinfo;
  struct stat source_stat, obj_stat;
  int res;
  if (annotation_level == 2 || inferior_pid == 0 || fixinfo_chain == NULL)
    return;
  for (fixinfo = fixinfo_chain; fixinfo; fixinfo = fixinfo->next)
    {
      res = stat (fixinfo->fullname, &source_stat);
      if (res != 0)
        {
          printf_filtered ("Fix: \"%s\", Can't read '%s'.", fixinfo->fullname, fixinfo->fullname);
          delete_fixinfo (fixinfo, NULL);
        }
      else
        {
          res = stat (fixinfo->library->name, &obj_stat);
          if (res != 0)
            {
              printf_filtered ("Fix: \"%s\", '%s' has disappeared.\n", fixinfo->fullname, fixinfo->library->name);
              delete_fixinfo (fixinfo, NULL);
            }
          else
            {
              if (source_stat.st_mtime > obj_stat.st_mtime &&
                  annotation_level < 2 &&
                  query ("Use edited version of '%s'? ",
                         fixinfo->fullname))
                fix_file (fixinfo->fullname);
            }
        }
    }
}
#endif /* HPPA_FIX_AND_CONTINUE */

/* Return the full filename using the compile map information.
 * Return 0 if there is no compile map */

char *
hppa_get_cdir_filename (pst)
    struct partial_symtab * pst;
{
  struct objfile *objfile;
  char *dirname;
  int fd;
  char * pst_full_pathname;
  extern void hpread_psymtab_to_symtab ();

  /* Return 0 if this is not a doom psymtab */
  if (pst->read_symtab == hpread_psymtab_to_symtab)
    return 0;
  
  pst_full_pathname = 0;

  objfile = pst->objfile;
  dirname = STRINGS (objfile) + COMPMAP (objfile)->cr[OBJID (pst)].dirname;
  char buf[1];
  if (dirname && target_read_memory ((CORE_ADDR) dirname, buf, 1) == 0)
    {
      fd = openp (dirname, 0, pst->filename, O_RDONLY, 0, &pst_full_pathname);
      if (fd < 0)
        pst_full_pathname = strdup(pst->filename);
    }

  return pst_full_pathname;
}

/* srikanth, 020423, On PA32, shared library long branch stubs come in two
   flavors : ones branching to millicode (link register r31) and ones
   branching to regular functions (link register r2). Both have a stub
   type of 14 in the unwind table !!!! This is because they ran out of 
   bits to encode a new stub type !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   Gdb is always using r2 as the link register and this causes next to
   turn into a continue when the code path passes through a long branch 
   stub leading to milli routine. We now resort to AI.
*/

#ifndef GDB_TARGET_IS_HPPA_20W
CORE_ADDR
fix_pc_if_in_long_branch_stub (stop_pc, prev_pc, return_pc)
CORE_ADDR stop_pc, prev_pc, return_pc;
{
  struct unwind_table_entry *u;

  u = hppa_find_unwind_entry(stop_pc);
  if (u && u->stub_unwind.stub_type != 0
      && u->stub_unwind.stub_type == LONG_BRANCH_SHLIB)
  {
      if ((return_pc != prev_pc + 4) && (return_pc != prev_pc + 8))
          return read_register (31) & ~0x3;
  }
  return return_pc;
}
#endif


/* It would be nice to have an implementation of this routine on PA as
   well as IPF. However, TT_PROC_GET_ARGS ttrace request used on IPF to
   access program arguments is not supported on PA. */
char* 
get_inferior_cwd ()
{
  return NULL;
}
