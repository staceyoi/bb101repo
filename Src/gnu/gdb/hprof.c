/* Srikanth, May 17th 2004: hprof.c : routines to gather PC samples
   and record the call stack.

   The hprof functionality is implemented in three components :

	(a) Profile engine : This is based on gdb. The profile engine is
            responsible for gathering PC samples and call stacks. It
            simply records these data in ASCII text form in the output
            file specified by the driver.

	(b) Driver/Command : This is the user's interface to this
            capability. The driver (prospect) is responsible for
            command line option processing, invoking the engine
            with suitable options and when raw profile collection is
	    complete, for processing the raw samples and computing the
            hierarchical profile.

	(c) Helper library : This is a small library which is responsible
            for triggering interval timers. It also has a set of variables
            using which profile parameters could be controlled.

	Note : The wrapper would invoke gdb as :

	gdb -nx -profile --quiet [profile options] a.out [pid] --commands=file
	where file would contain

	run [args] || continue
	quit

	based on whether we are starting the process or attaching to it.
*/

#include "defs.h"
#include "gdb_string.h"
#include <ctype.h>
#include "symtab.h"
#include "frame.h"
#include "inferior.h"
#include "breakpoint.h"
#include "gdb_wait.h"
#include "gdbcore.h"
#include "gdbcmd.h"
#include "target.h"
#include "gdbthread.h"
#include "annotate.h"
#include "symfile.h"		/* for overlay functions */
#include "top.h"
#include <signal.h>
#include "inf-loop.h"
#include "objfiles.h"
#include "environ.h"

/*******************  Hierarchical profile : Basics **************

    Basically by executing a setitimer call in the context of the
    target process, we can request the kernel to allow us to sample
    the PC values in the program at periodic intervals. The interval
    itself is specified in the call to setitimer. Anytime the specified
    interval expires, the kernel delivers a signal to the process.
    GDB can now intercept the signal, unwind the stack to discover
    the program context and record it.

    By aggregating samples over the run of the program, we can build
    a "hierarchical profile" that shows the samples attributed
    to various functions in the program and their callers.

*********************************************************************/

int profile_on = 0;   /* Are we to profile the application ? */
int profiler_signal = TARGET_SIGNAL_VTALRM;

/* Sampling interval is set to 50 ms by default. User can change
   this via command line option. Less the interval, more the samples,
   and so greater the accuracy, but slower the program runs.
*/
static int sampling_interval = 50;

static char * profile_output_file = "hprof.out";
static FILE * profile_fp = stdout;

#define IPF32_PROFILE_HELPER_LIBRARY "/opt/prospect/lib/hpux32/libhprof.so"
#define IPF64_PROFILE_HELPER_LIBRARY "/opt/prospect/lib/hpux64/libhprof.so"
#define PA32_PROFILE_HELPER_LIBRARY  "/opt/prospect/lib/libhprof.sl"
#define PA64_PROFILE_HELPER_LIBRARY  "/opt/prospect/lib/pa20_64/libhprof.sl"

static struct objfile * profile_helper_objfile;
static char * profile_helper_library;

extern int profile_test_on;  /* Are we running under a testsuite ? */

static void handle_signals_discretly ()
{
  int i;

  /* While profiling, silently and immediately pass the
     signal to the program. In the case of the profiling
     signal itself, we should not pass it to the target.

     This silent and instant delivery is needed for hiding
     the underlying implementation.
  */
  for (i = 0; i < TARGET_SIGNAL_LAST; i++)
    {
      signal_stop_update (i, 0);
      signal_print_update(i, 0);
      if (i == profiler_signal || i == TARGET_SIGNAL_TRAP)
        signal_pass_update(i, 0); /* not for program's consumption */
      else   
        signal_pass_update(i, 1); /* Pass to program */
    }

  /* If we adopted a running process, alert the kernel to the
     changed signal disposition.
  */
  if (target_has_execution)
    require_notification_of_events (inferior_pid);
}

static char *
identify_helper_library ()
{
  char * override_library;

#ifdef HP_IA64
  if (is_swizzled)
    profile_helper_library = IPF32_PROFILE_HELPER_LIBRARY;
  else
    profile_helper_library = IPF64_PROFILE_HELPER_LIBRARY;
#else
#ifndef GDB_TARGET_IS_HPPA_20W
  profile_helper_library = PA32_PROFILE_HELPER_LIBRARY;
#else 
  profile_helper_library = PA64_PROFILE_HELPER_LIBRARY;
#endif
#endif

  /* Check for override. */
  if ((override_library = getenv("LIBHPROF_SERVER")) != NULL)
    profile_helper_library = strdup (override_library);

  return profile_helper_library;
}

static struct objfile * profile_helper_available (void)
{
    struct objfile * objfile;

    ALL_OBJFILES (objfile)
        if (!strcmp (objfile->name, profile_helper_library))
          return objfile;
        else
        if (strstr (objfile->name, "libhprof.s"))
          return objfile;

    return 0;
}

/* Command line control of profile parameters : There is very little
   validation done here. This is because, gdb is supposed to be invoked
   by the wrapper *after* validating its input. In any case, we default
   to sane values if something is wrong.
*/
void set_sampling_interval(char * optarg)
{
    sampling_interval = atoi (optarg);

    /* hpux allows no less than 10ms period for sampling. */
    if (sampling_interval < 10)
      sampling_interval = 10;
}

void set_profile_output_file(char * optarg)
{
    if (optarg && optarg[0])
      profile_output_file = strdup (optarg);

    /* Open the file right at the time of the option processing
       so as to unblock prospect from its fopen. We really don't
       expect any errors here since prospect would have made
       sure that the FIFO has been created and that it has the
       right permissions.
    */
    if ((profile_fp = fopen (profile_output_file, "w")) == NULL)
      {
        printf_unfiltered ("hprof: error creating output file %s\n",
                                        profile_output_file);
        shutdown_profiling (0);
      }
}

void set_sampling_timer_type(char * optarg)
{
    if (optarg)
      {
        if (!strcmp (optarg, "virtual"))
          profiler_signal = TARGET_SIGNAL_VTALRM;
        else
        if (!strcmp (optarg, "profile"))
          profiler_signal = TARGET_SIGNAL_PROF;
       }
}


/* Prospect signals us to shut down by sending a SIGTERM.
   We also come here when the benchmark is over.

   Signal parameter is ignored.
*/
void shutdown_profiling (int sig)
{
    if (inferior_pid != 0 && target_has_execution)
    {
	if (attach_flag)
        {
            printf_unfiltered ("Detaching from target ...");
            gdb_flush (gdb_stdout);
	    target_detach (0, 0);
            /* Srikanth, this seems to be needed due to some timing issue.
               Without the sleep here, the process gets killed.
            */
            sleep (2);
            printf_unfiltered (" Done.\n");
        }
	else
	    target_kill ();
    }
    gdb_flush (gdb_stdout);
    fclose (profile_fp);
    if (!profile_test_on)  /* This exit confuses dejagnu so thoroghly */
      exit (0);
}

void
initiate_profiling_after_launch ()
{
  static char interval_string [16];
  static char timertype_string [16];
  struct sigaction sigact;

  /* In the new scheme of things, this gets called only in the
     launch case. We don't get here in the attach case.
  */
  sigact.sa_handler = shutdown_profiling;
  sigemptyset (&sigact.sa_mask);
  sigact.sa_flags = 0;
  sigaction (SIGTERM, &sigact, (struct sigaction *)NULL);

  handle_signals_discretly ();

  /* If we are running under the testsuite, then there is
     nothing we have to do to initate the profile.
     The program itself generates alarm signals explicitly
     to avoid nondeterminism that would be associated with
     interval timers.
   */
  if (profile_test_on)
	return;

  set_in_environ (inferior_environ, "LD_PRELOAD", identify_helper_library());
  sprintf (interval_string, "%d", sampling_interval);
  set_in_environ (inferior_environ, "HPROF_SAMPLING_INTERVAL", interval_string);
  sprintf (timertype_string, "%s", 
       (profiler_signal == TARGET_SIGNAL_VTALRM) ? "virtual" : "profile");
  set_in_environ (inferior_environ, "HPROF_SAMPLING_TIMER_TYPE", timertype_string);
}


void initiate_profiling_after_attach (void)
{
    struct minimal_symbol * m;
    CORE_ADDR anaddr;
    char buf[4];
    struct sigaction sigact;
    int using_virtual_timer = 1;

    sigact.sa_handler = shutdown_profiling;
    sigemptyset (&sigact.sa_mask);
    sigact.sa_flags = 0;
    sigaction (SIGTERM, &sigact, (struct sigaction *)NULL);

    identify_helper_library ();

    if ((profile_helper_objfile = profile_helper_available ()) == 0)
    {
	printf_unfiltered ("hprof: profiler helper library not loaded, profiling disabled.\n");
	printf_unfiltered ("You may need to explicitly link with %s\n",
		profile_helper_library);
	gdb_flush (gdb_stdout);
	shutdown_profiling (0);
    }

    /* Grrrr. We have to discover the profile parameters the program
       is invoked with and clue in prospect on the same.
     */
    m = lookup_minimal_symbol ("__use_virtual_timer", NULL,
	    profile_helper_objfile);
    if (!m)
      {
        printf_unfiltered ("Internal error: `__use_virtual_timer' missing in assist library\n");
        shutdown_profiling (0);
      }

    anaddr = SYMBOL_VALUE_ADDRESS (m);
    target_read_memory (anaddr, buf, 4);
    using_virtual_timer = (int) extract_unsigned_integer (buf, 4);
    if (using_virtual_timer)
	profiler_signal = TARGET_SIGNAL_VTALRM;
    else
	profiler_signal = TARGET_SIGNAL_PROF;

    m = lookup_minimal_symbol ("__sampling_interval", NULL,
	    profile_helper_objfile);
    if (!m)
      {
        printf_unfiltered ("Internal error: `__sampling_interval' missing in assist library\n");
        shutdown_profiling (0);
      }

    anaddr = SYMBOL_VALUE_ADDRESS (m);
    target_read_memory (anaddr, buf, 4);
    sampling_interval = (int) extract_unsigned_integer (buf, 4);

    if (profiler_signal == TARGET_SIGNAL_PROF)
        fprintf (profile_fp, "Sampling timer type=profile\n"); 
    if (sampling_interval != 50)
        fprintf (profile_fp, "Sampling interval=%d\n", sampling_interval);

    handle_signals_discretly ();
}


void error_while_profiling (char * message)
{
    printf_unfiltered ("Internal error: %s\n", message);
    printf_unfiltered ("Stop.\n");
    shutdown_profiling (0);
}


static int record_call_stack (PTR unused)
{
  struct frame_info *fi;
  struct minimal_symbol * m;
  static int sample_count;
  unsigned long long offset;
  int level = 0;
  CORE_ADDR last_pc;
  CORE_ADDR last_fp;
  CORE_ADDR entry_point;
  char * symbol_name;

  fprintf (profile_fp, "Sample # : %d\n\n", ++sample_count);

  /* Srikanth, I sped up this loop a bit by folding multiple
     fprintf's into one. Removing SPOS emission which is not
     used by prospect anyway at this point. On programs with
     sevaral threads, this could otherwise take a good bit of
     time.
  */
  last_fp = last_pc = 0;
  for (fi = get_current_frame(); fi; fi = get_prev_frame(fi))
    {
      m = lookup_minimal_symbol_by_pc (fi->pc);

      if (m)
        {
          offset = fi->pc - SYMBOL_VALUE_ADDRESS(m);
          symbol_name = SYMBOL_SOURCE_NAME(m);

          /* For millicode symbols like $$remU, source name comes out as NULL. 
             Use the linkage name instead - Srikanth, Nov 30th 2005.
          */
          if (symbol_name == 0 || symbol_name[0] == 0)
              symbol_name = SYMBOL_NAME(m);

          entry_point = SYMBOL_VALUE_ADDRESS(m);
        }
      else
        {
          offset = 0;
          symbol_name = 0;
          entry_point = fi->pc;
        }

      if (symbol_name == 0 || symbol_name[0] == 0)
          symbol_name = "<unknown>";

      /* don't print the fake frame we insert */
      if (symbol_name[0] == '_' && !strcmp (symbol_name, "__thread_launch_wrapper__"))
          break;

      fprintf (profile_fp,  sizeof (CORE_ADDR) == 4 ? "\tin 0x%08x + %d %s\n" :
                                "\tin 0x%016llx + %d %s\n", entry_point, (int) offset,
                                 symbol_name);
      /* JAGag20972: Because this problem is fixed in gdb, am commenting this code.
         - srikanth, mithun, 061025. */
#if 0     
      /* FIXME: PA debugger goes into tailspin when unwinding
         from sigprocmask. No idea why. (CR to be raised)
      */
      if (m && !strcmp (SYMBOL_SOURCE_NAME(m), "sigprocmask"))
          break;
#endif
      /* Sometimes gdb gets stuck on with the same pc and same
         fp. Silently bail out.
      */
      if (fi->pc == last_pc && fi->frame == last_fp)
        break;

      last_pc = fi->pc;
      last_fp = fi->frame;

      /* We cannot have a call from offset 0 of any function !!!,
         Sometime, when the PC samples falls in the prologue
         gdb goes into an infinite loop.
      */
      if ((level > 0 && offset == 0) || (level++ > 100))
          break;
    }
  fprintf (profile_fp, "\n");
  fflush (profile_fp);

  return 0;
}

void gather_pc_sample ()
{
  extern int donot_print_errors;
  void (*saved_error_hook) (char *);
        
  /* Srikanth, Nov 20th, 2005, We used to iterate over the thread
     list and unwind the call stack for each thread. As of prospect
     2.6, this is changed. We now enable only thread specific timers
     and unwind the call stack of only the thread which triggered
     the timer. This is because TT_PROC_STOP introduces a heavy skew
     in favor of system calls and the resulting profiles greatly
     amplify the time spent inside system calls and are not useful.
  */
  donot_print_errors = 1;
  saved_error_hook = error_hook;
  error_hook = NULL;
  catch_errors (record_call_stack, 0, "", RETURN_MASK_ALL);
  error_hook = saved_error_hook;
  donot_print_errors = 0;
}
