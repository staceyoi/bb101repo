/* All functions needed to support debugging of inlined functions on HP-UX. */

#include "defs.h"
#include "top.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "gdbcore.h"
#include "frame.h"
#include "target.h"
#include "value.h"
#include "symfile.h"
#include "objfiles.h"
#include <string.h>
#include <sys/types.h>
#include <assert.h>
#include "demangle.h"

#define DEBUG(x) 

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
/* This variable indicates if we have already read in MLT or not. 
   If this variable is set, we would have to skip updating the
   inlinee_table when you read the line table again when 
   expanding psymtabs to symtabs.
*/
extern int dwarf2_read_minimal;
#endif

/* JAGag33388 - Variables required to verify if the file
   is compiled with higher optimization levels.
*/
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
extern enum inline_type inline_debugging;
extern char *inl_debug;
extern int file_opt_level;
#endif

/* inline_routine_map is a function name map used for inlining.
   This map is filled up for each DW_LNE_HP_define_proc entry in
   dwarf. The idx corresponding to DW_LNE_HP_set_routine_name tag
   refers to the one of these entries. */
struct inline_routine_map {
    int nums;		/* Number of entries allocated in map_entry. */
    int num_filled;	/* Number of entries filled. */
    char** map_entry;	/* List of function name entries. */
};
struct inline_routine_map inline_routine_map = { 0, 0, 0 };
#define INLINE_MAP_CHUNK 100

/* A struct inlinee_table_entry describes one inlined call site.  One
   function name in the inline_routine_map may be referred to by multiple
   inlined instances in the "inlinee_table" array of struct
   inlinee_table_entry.  In general, "context_ref_*" means the caller.  */
struct inlinee_table_entry
{
    char * name;		/* callee inlined at this call site */
    char * context_ref_file;	/* source file of the call site */
    CORE_ADDR context_ref_pc;	/* addr of last instr of caller before start of
				   this (sequence of) inlined instance(s) */
    CORE_ADDR low_pc;		/* addr of first instr this inlined instance */
    CORE_ADDR high_pc;		/* next address past the last instruction of
				   this inlined instance */
    CORE_ADDR actual_context_ref_pc;	/* context_ref_pc where the first 
					   inlined instance of this
					   split inlined instance started. */
    int context_ref_line;	/* source line in caller's file of call site */
    int context_ref_idx;        /* zero if this is a first-level inline
				   (directly into the non-inlined function),
				   or index in the "inlinee_table" of the
				   outer inlined instance this inner inlined
				   instance nests into */
    int context_ref_column;     /* Column where a routine was inlined
				   into another routine.  Also helps
				   recognize duplicates in inlining info. */
    int actual_context_ref;     /* The value of the opcode HP_set_context_ref
				   as it appears in the line tables.  Used
				   to recognize split inlined instances.  */
};

/* Allocatable array of inlined instances, global to the debugger - all
   objfiles' inlined instances information goes in this one table. 
   Note that element zero of this array is not used, so that index
   zero can be used as a flag value. */
static struct inlinee_table_entry *inlinee_table;
static int inlinee_table_size = 0;
static int inlinee_count = 0;
#define INLINE_TBL_CHUNK 100

/* Clear all data corresponding to inlining */
void
clear_inline_data()
{
  /* Make sure the following scenario works when changing this routine. 
     In a scenario like 

         gdb --inl eon
         break ggUnionBoundingBox3
         run
  */

  inlinee_count = 0;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  if (inlinee_table)
    {
      free (inlinee_table);
      inlinee_table = 0;
      inlinee_table_size = 0;
    }
  dwarf2_read_minimal = 0; 
#endif
  return;
}

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
/* Give the inline function name and retrieve the PC's associated with it.
   This function adopts linear way of searching the inline table
   This needs to be fixed to use Judy.
*/
CORE_ADDR* find_all_inline_instances (char *name, char *source_file, 
                                      int inline_idx, int *inline_count)
{
  int i, count = 0, len;
  char * dem_name, *dem_parms;
  CORE_ADDR *inline_pc = NULL;

  if (source_file != NULL && inline_idx)
    {
      *inline_count = 1;
      inline_pc = (CORE_ADDR *) xmalloc (sizeof(CORE_ADDR));
      *inline_pc = inlinee_table[inline_idx].low_pc;
      return inline_pc;
    }

  inline_pc = (CORE_ADDR *) xmalloc (inlinee_count * sizeof(CORE_ADDR));

  for (i = 1; i <= inlinee_count ; i++)
     {
       dem_name = cplus_demangle ((inlinee_table[i].name), DMGL_ANSI);
       dem_parms = cplus_demangle ((inlinee_table[i].name), DMGL_ANSI | DMGL_PARAMS);

       if (!strcmp (name, dem_name) || !strcmp (name, dem_parms))
         {
           *(inline_pc+count) = (CORE_ADDR )inlinee_table[i].low_pc;   
           count++;
         }
     }
    *inline_count = count;
    return inline_pc;
}

/* This function is called from record_line_elim. When you read the line table 
   when expanding psymtabs, the inline_idx must be properly filled.
   Given the pc and context, retrieve the inline index.
*/
void get_index_given_pc(CORE_ADDR pc, int context, int *index)
{
  for (int i = 1; i <= inlinee_count; i++)
     {
       if (context == inlinee_table[i].actual_context_ref &&
           pc >= inlinee_table[i].low_pc && pc <= inlinee_table[i].high_pc)
         { 
           *index = i;
           return;
         }
     }  
}

#endif 

/* Enlarge the inline_routine_map's list of function names by fixed amount. */
void 
realloc_inline_map()
{
    inline_routine_map.map_entry = (char**)
      xrealloc (inline_routine_map.map_entry,
	        (inline_routine_map.nums + INLINE_MAP_CHUNK)
			 * sizeof (char*));
    inline_routine_map.nums = inline_routine_map.nums + INLINE_MAP_CHUNK;
}

/* Free the inline_routine_map's list of function names. */
void 
free_inline_map()
{
  if (inline_routine_map.map_entry)
    {
      free (inline_routine_map.map_entry);
      inline_routine_map.map_entry = 0;
    }
  inline_routine_map.nums = 0;
  inline_routine_map.num_filled = 0;
}

/* Insert a new function name into the inline_routine_map's list.  Do not
   check for the name already being present, the string table function
   add_string() elides duplicates.  */
void
insert_name_into_inline_map (char* name)
{
  if (inline_routine_map.num_filled == inline_routine_map.nums)
    realloc_inline_map();
  inline_routine_map.map_entry[inline_routine_map.num_filled] =
        add_string (name);
  inline_routine_map.num_filled ++;
}

/* Retrieve a function name by index from the inline_routine_map's list.  */
char*
get_inline_map_entry (int index)
{
  if (!inline_routine_map.map_entry || index >= inline_routine_map.num_filled)
    {
      warning ("No inline routine map_entry for index %d\n", index);
      return NULL;
    }
  return inline_routine_map.map_entry[index];
}

/* Add an inlined instance entry to the inline table. */
int
add_inline_entry (char *inline_name,
                  int context_ref_line,
                  char * context_ref_file,
                  CORE_ADDR context_ref_pc,
                  int context_ref_inlinee_index,
                  CORE_ADDR low_pc,
                  CORE_ADDR high_pc)
{
  int inlinee_index;
  char* name = add_string (inline_name);
  inlinee_index = insert_name_in_inline_table (name);
  if (add_refs_to_idx (inlinee_index,
		   context_ref_pc,
		   context_ref_line,
		   context_ref_file,
		   context_ref_inlinee_index,0,0))
    warning ("Cannot insert inlined instance %d for %s.\n",
		   inlinee_index, name);
  add_pc_to_idx (inlinee_index, low_pc, TRUE);
  add_pc_to_idx (inlinee_index, high_pc, FALSE);
  return inlinee_index;
}

/* Insert a name into inline table and return the corresponding index. */
int
insert_name_in_inline_table (char *name)
{
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  /* If you have already read in the inline table, just return. */
  if (dwarf2_read_minimal)
    return 0;
#endif

  if (inlinee_table_size == 0 || (inlinee_count + 1) == inlinee_table_size)
    {
      inlinee_table = (struct inlinee_table_entry *)
                      xrealloc (inlinee_table,
			        (sizeof (struct inlinee_table_entry))
				 * (inlinee_table_size += INLINE_TBL_CHUNK));
      if (inlinee_table == 0)
        {
	  DEBUG(printf("Insert of %s failed. Cannot realloc inlinee_table.\n",
	               name);)
	  return -1;
        }
    }

  inlinee_count++;
  inlinee_table[inlinee_count].name = name;
  return (inlinee_count);
}

/* gets the name of the inline routine from inline table.
*/
char *
get_inline_name(int index)
{
  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline name lookup failed for idx %d.\n", index);)
      return NULL;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)
      return inlinee_table[index].name;
    }
}

/* Srikanth, 050716, get_inline_return_pc : return the return PC for the inline
   instance identified by index.
   The highpc is always the next instruction of the inlined instance.
   This should suffice as return_pc.
*/
CORE_ADDR
get_inline_return_pc (int index)
{
  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline return_pc lookup failed for idx %d.\n", index);)
      return NULL;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
      /* JAGag33388 - If the executable is compiled with higher optimization
         and high_pc is zero, it indicates that there might be split inline
         instances.  GDB does not handle these currently.
         So warn the user and turn off inline debugging.
      */

       /* JAGag41210 - At any optimization level, GDB should recover
          instead of aborting if high_pc is zero.
       */

      if (inlinee_table[index].high_pc == 0) 
        {
          error ("Inline error: (Possibly due to split inline instances).\n"
                 "Turning off inline debugging.\n");
          inline_debugging = OFF;
          inl_debug = "off";
          return NULL;
        }
      else
        {
#endif
      assert (inlinee_table[index].high_pc != 0);
#ifdef GDB_TARGET_IS_HPPA
      return inlinee_table[index].high_pc + 4;
#else
      return inlinee_table[index].high_pc;
#endif
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
        }
#endif
    }
}

/* Gets the contextref_pc of the inline routine given by index */
CORE_ADDR
get_inline_contextref_pc (int index)
{
  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline context_ref_pc lookup failed for idx %d.\n", index);)
      return NULL;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)
      return inlinee_table[index].context_ref_pc;
    }
}

/* Gets the contextref_idx of the inline routine given by index */
int
get_inline_contextref_idx (int index)
{
  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline context_ref_idx lookup failed for idx %d.\n",
		   index);)
      return NULL;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)
      assert (inlinee_table[index].context_ref_idx >= 0 && 
	      inlinee_table[index].context_ref_idx <= inlinee_count);
      return inlinee_table[index].context_ref_idx;
    }

}

/* Gets the index of the first inline routine matched by the lookup values. */
static int 
lookup_inline_entry( int index, int context_ref_line, int context_ref_idx,
                     int context_ref_column, char *file )
{
    int i;

    assert (context_ref_idx >= 0 && context_ref_idx <= inlinee_count);

    for( i = 1 ; i < inlinee_count ; i++ ) 
    {
        if( index == i ) 
            continue;
        assert (inlinee_table[i].context_ref_idx >= 0 && 
		inlinee_table[i].context_ref_idx <= inlinee_count);
        if( context_ref_line == inlinee_table[ i ].context_ref_line && 
            context_ref_idx  == inlinee_table[ i ].context_ref_idx &&
            context_ref_column ==  inlinee_table[ i ].context_ref_column &&
            ( file && inlinee_table[ i ].context_ref_file &&
              strcmp( file,inlinee_table[ i ].context_ref_file ) == 0 ) 
          )      
            return i;
    }    
   return -1;
}

/* Adds context_ref_pc, context_ref_line, context_ref_file, context_ref_idx,
   context_ref_column, and actual_context_ref to the inline table entry
   identified by "index". */
boolean
add_refs_to_idx (
    int index,
    CORE_ADDR context_ref_pc,
    int context_ref_line,
    char* context_ref_file,
    int context_ref_idx,
    int context_ref_column,
    int actual_context_ref)
{
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  /* If you have already read in the inline table, just return. */
  if (dwarf2_read_minimal)
    return 0;
#endif

  int lookup_index = -1;

  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline context_ref_idx lookup failed for idx %d.\n",
                   index);)
      return 0;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)
      assert (context_ref_idx >= 0 && context_ref_idx <= inlinee_count);

      inlinee_table[index].context_ref_pc = context_ref_pc;
      inlinee_table[index].context_ref_line = context_ref_line;
      inlinee_table[index].context_ref_file =
	savestring (context_ref_file, (int) strlen (context_ref_file));
      inlinee_table[index].context_ref_idx = context_ref_idx;
      inlinee_table[index].context_ref_column = context_ref_column;
      if ((lookup_index = (lookup_inline_entry (index,
		context_ref_line, context_ref_idx, context_ref_column,
		context_ref_file))) != -1)
	{
	  inlinee_table[index].actual_context_ref_pc =
		inlinee_table[lookup_index].context_ref_pc; 
        } else {
	  inlinee_table[index].actual_context_ref_pc = 0;
        }
      inlinee_table[ index ].actual_context_ref = actual_context_ref;          
      return 1;
    }
}

/* Add a high or low PC value to an inline instance. */
int
add_pc_to_idx (
    int index,
    CORE_ADDR pc,
    boolean start)
{
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
/* If you have already read in the inline table, just return. */
  if (dwarf2_read_minimal)
    return 0;
#endif

  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline pc_to_idx lookup failed for idx %d.\n",
                   index);)
      return 0;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)

      if (start)
        inlinee_table[index].low_pc = SWIZZLE(pc);
      else
        inlinee_table[index].high_pc = SWIZZLE(pc);
      return 1;
    }
}

/* Gets the index of the first inline routine matched by the high/low
   PC lookup values.  Zero (the unused index into the "inlinee_table")
   is returned if no match is found. */
int
get_inline_index (CORE_ADDR low_pc, CORE_ADDR high_pc)
{
  int i;

  for (i = inlinee_count; i > 0; i--)
    {
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
      /* JAGag33388 - If the file is compiled with higher optimization
         and high_pc is zero, it indicates that there might be split inline
         instances.  GDB does not handle these currently.
         So warn the user and turn off inline debugging.
      */

      /* JAGag41210 - At any optimization level, GDB should recover
         instead of aborting if high_pc/low_pc is zero or 
         if high_pc is less than low_pc. 
      */

      if (inlinee_table[i].high_pc == 0 ||
          inlinee_table[i].low_pc == 0 ||
         (inlinee_table[i].high_pc < inlinee_table[i].low_pc))
        {
          warning ("Inline error: (Possibly due to split inline instances).\n"
                   "Turning off inline debugging.\n");
          inline_debugging = OFF;
          inl_debug = "off";
          return 0;
        }
      else
        {
#endif
      assert (inlinee_table[i].low_pc <= inlinee_table[i].high_pc);
	/* FIXME :  the above assert should be just "<", change it back when
	   JAGag05455 "Incorrect DWARF for concrete inline instances at +O1"
	   is fixed.  */

      assert (inlinee_table[i].low_pc != 0);
      assert (inlinee_table[i].high_pc != 0);
      if (   inlinee_table[i].low_pc == SWIZZLE(low_pc)
          && inlinee_table[i].high_pc == SWIZZLE(high_pc))
        return i;
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
        } 
#endif
    }

  return 0;
}


/* Gets the value of the opcode HP_set_context_ref as it appeared
   in the line tables.  */
int get_actual_context_ref( int index )
{ 
    if (index < 1 || index > inlinee_count)
    {
        DEBUG(printf("Inline context_ref_pc lookup failed for idx %d.\n", index);)
        return NULL;
    }
    else
    {
        DEBUG(printf("Match found - %d \n", index);)
        return inlinee_table[index].actual_context_ref;
    }
}

/* Get the actual_context_ref_pc field, the context_ref_pc from 
   where the first inlined instance of this split inlined 
   instance started. */
CORE_ADDR
get_inline_actual_contextref_pc (int index)
{
  if (index < 1 || index > inlinee_count)
    {
      DEBUG(printf("Inline context_ref_pc lookup failed for idx %d.\n", index);)
      return NULL;
    }
  else
    {
      DEBUG(printf("Match found - %d \n", index);)
      return inlinee_table[index].actual_context_ref_pc;
    }
}

void
dump_inlinee_table()
{
  FILE*         temp_file;
  int i;
  char header1[] = "\t              \t\t      \t\t       \t\t" \
		   "                 -- cref_ --- " \
		   "actual\n";
  char headers[] = "\tcontext_ref_pc\t\tlow_pc\t\thigh_pc\t\t" \
		   "actual_cref_pc   line idx col " \
		   "_cref  name\n";
  char format[] = "%3d.  %p  %p  %p  %p  " \
		   "%5d %3d %3d " \
		   "%5d   %s\n";

  temp_file = fopen ("dump.inlinee_table", "w");
  if (temp_file == NULL)
      printf("dump_inlinee_table: could not open dump.inlinee_table file.\n");

  fprintf(temp_file, header1);
  fprintf(temp_file, headers);

  for( i = 1 ; i <= inlinee_count ; i++ ) 
  {
    struct inlinee_table_entry *t = &inlinee_table[i];
    fprintf(temp_file, format, i,
      t->context_ref_pc,
      t->low_pc,
      t->high_pc,
      t->actual_context_ref_pc,
      t->context_ref_line,
      t->context_ref_idx,
      t->context_ref_column,
      t->actual_context_ref,
      t->name);
  }

  fclose(temp_file);
}
