#include <sys/types.h>

extern struct inline_routine_map inline_routine_map;

extern void insert_name_into_inline_map (char*);
extern char* get_inline_map_entry (int index);
extern void clear_inline_data(void);
extern void free_inline_map(void);
int add_inline_entry (char*, int, char *, CORE_ADDR, int, CORE_ADDR, CORE_ADDR);
extern int insert_name_in_inline_table (char *name);
extern char * get_inline_name(int);
extern CORE_ADDR get_inline_contextref_pc (int);
extern CORE_ADDR get_inline_return_pc (int);
extern int get_inline_contextref_idx (int);
extern int add_pc_to_idx (int, CORE_ADDR, boolean);
extern int get_inline_index (CORE_ADDR low_pc, CORE_ADDR high_pc);

extern CORE_ADDR get_inline_actual_contextref_pc (int);
extern int get_actual_context_ref ( int inline_idx );
extern boolean add_refs_to_idx (int, CORE_ADDR, int, char*, int, int, int);
