/* Low level interface for debugging HPUX/DCE threads for GDB, the GNU debugger.
   Copyright 1996, 1999 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* This module implements a sort of half target that sits between the
   machine-independent parts of GDB and the ptrace interface (infptrace.c) to
   provide access to the HPUX user-mode thread implementation.

   HPUX threads are true user-mode threads, which are invoked via the cma_*
   and pthread_* (DCE and Posix respectivly) interfaces.  These are mostly
   implemented in user-space, with all thread context kept in various
   structures that live in the user's heap.  For the most part, the kernel has
   no knowlege of these threads.

   A key data structure here is a table with information about all known
   CMA threads.  There is a hash table to find the table index associated
   with a CMA thread handle.  The gdb PID for a CMA thread contains the
   table index for the thread (the "thread index").

   Gdb learns about new threads when the FIND_NEW_THREADS macro is
   invoked by info_threads_command.  FIND_NEW_THREADS ==
   hppa_find_new_threads calls add_thread as it adds entries to the
   thread table.  In this implementation FIND_NEW_THREADS will also
   discover threads that have gone away and will invoke delete_thread
   for those threads.

   The other key aspect is accessing the registers of a thread.  For the
   active thread, this is done with the old ptrace/ttrace interface.
   For the inactive threads the CMA library saves certain registers which
   can be obtained through the thread handle.  The registers which cannot
   be restored are treated as if they are set to zero.

   */

#include "defs.h"

#define _CMA_NOWRAPPERS_

#include <assert.h>
#include <cma_tcb_defs.h>
#include <cma_deb_core.h>
#include "gdbthread.h"
#include "target.h"
#include "inferior.h"
#include <fcntl.h>
#include <sys/stat.h>
#include "gdbcore.h"
#include "objfiles.h"
#include <sys/ttrace.h>

#ifndef TRUE
# define TRUE 1
#endif

#ifndef FALSE
# define FALSE 0
#endif

extern int child_suppress_run;
extern int registers_pid;
extern struct target_ops child_ops;	/* target vector for inftarg.c */

extern void _initialize_hpux_thread (void);
/* If the pid is > 2^16 , gdb needs to handle it properly when
   constructing gdb_pids */
int is_pid_larger = 0;

struct string_map
  {
    int num;
    char *str;
  };

static int hpux_thread_active = 0;
static int nbr_user_threads = 0;

static CORE_ADDR P_cma__g_known_threads = 0;
static CORE_ADDR P_cma__g_current_thread = 0;
static CORE_ADDR P_cma__g_debug_state = 0;

static struct cleanup *save_inferior_pid (void);

static void restore_inferior_pid (int pid);

void hpux_thread_new_objfile (struct objfile *objfile);

static void hpux_thread_resume (int pid, int step, enum target_signal signo);

static void init_hpux_thread_ops (void);

static struct target_ops hpux_thread_ops;

/*========================================================================
  Mapping pthread_t to cma_tnum etc.

  For a CMA thread, as in sol-thread.c, the PID used by gdb for a thread
  is made up of a high-bit on, followed by 15 bits of an internal thread
  index, followed by the pid of the process (16 bits).  The internal thread
  index is an index into a table of thread information.

  We maintain a hash table to facilitate finding a thread by its
  pthread_t handle.  The index of a thread can be extractec directly from 
  a gdb pid.
  
  We maintain information about each known thread in a thread table.
  For each such thread we keep its gdb thread number (e.g. "thread 1"),
  the pthread_t handle (a pointer to the TCB of the thread), the gdb pid,
  the index of the next entry on the pthread_t hash chain and the
  index of the next entry on the gdb pid hash chain.

  These data structures are re-initialized when hpux_thread_new_objfile is
  called with a NULL argument, which indicates the termination of execution.

  We re-use table entries as threads are destroyed and re-created.  When
  a we notice that a thread terminates, we remove it from the hash chains,
  set is_active to FALSE and set next_pid to the index of the next free
  entry and put the index of the entry into thread_info_free_list;
  An entry on the free list has a gdb_thread_num of -1.  The 
  next_handle field is used to link them together.
  */
  /* Minor variation in the above comments, if the O.S. is 11.11
 	On O.S. like 11.11, the pid can be anything less than 2^29. 
  */

#define PID_BITS_ORIG		16
#define PID_BITS_11_11		29

#define THREAD_INFO_HASH_SIZE   1024

#define END_OF_LIST             -1
#define HANDLE_HASH(NBR)        (  (((unsigned int) NBR) >> 3) \
                                 % THREAD_INFO_HASH_SIZE)


#define MAKE_PID(TH_IDX, PID) \
		(cma_process_pid = PID) ? (TH_IDX | CMA_THREAD_FLAG) : PID\

#define NIL_GDB_PID             -1
#define NIL_THREAD_IDX          -1
#define NIL_THREAD_NUM          -1
#define REG_SAVE_OFFSET         160
#define REG_SAVE_OFFSET_PA20    224
#define THREAD_HANDLE(PID)      (thread_info_p[THREAD_IDX(PID)].thread_handle)

typedef struct {
  int           gdb_thread_num; /* The thread number presented to the user. */
  cma__t_int_tcb* thread_handle;  /* The CMA thread handle */
  int           gdb_pid;        /* The pid used internally in gdb */
  int           next_handle;    /* Table index for next handle_hash_tbl, 
                                   -1 for end.*/
  int           is_active;      /* Used for marking entries. */
} t_hpux_thread_info;

static int                      thread_info_entries = 0;
static int                      thread_info_free_list = END_OF_LIST;
static t_hpux_thread_info*      thread_info_p = 0;

static int handle_hash_tbl[THREAD_INFO_HASH_SIZE];

/* Forward declarations */

static int get_th_idx (void);
void hppa_find_new_threads (void);
static int hppa_find_new_threads_search (cma__t_int_tcb *);
static int search_for_handle (cma__t_int_tcb*);
static void set_regmap (void);
    

/* free_th_idx, add the idx'th entry of the thread_info_p table to the 
   free list. 
   */
static void
free_th_idx (int idx)
{
  thread_info_p[idx].gdb_thread_num = -1;
  thread_info_p[idx].is_active = FALSE;
  thread_info_p[idx].next_handle = thread_info_free_list;
  thread_info_free_list = idx;
  nbr_user_threads--;
}

/* Return the index of the thread table that has thread_handle.  If necessary,
   add an entry to the table.
   */

static int
find_th_idx (thread_handle)
  cma__t_int_tcb*       thread_handle;
{
   int  handle_hash;
   int  idx;
   int  new_pid;

   if ((idx = search_for_handle(thread_handle)) != NIL_THREAD_IDX)
     return idx;

   /* We have a new thread.  Add it to our table and gdb's thread list. */

   idx = get_th_idx();
   new_pid = MAKE_PID (idx, PIDGET(inferior_pid));

   /* add to gdb's list of threads */
   if (nbr_user_threads == 1)
     thread_info_p[idx].gdb_thread_num = 
       add_user_thread (new_pid, inferior_pid);
   else
     thread_info_p[idx].gdb_thread_num = (add_thread (new_pid))->num; 

   thread_info_p[idx].thread_handle = thread_handle;
   thread_info_p[idx].gdb_pid = new_pid;
   handle_hash = HANDLE_HASH(thread_handle);
   thread_info_p[idx].next_handle = handle_hash_tbl[handle_hash];
   handle_hash_tbl[handle_hash] = idx;
   thread_info_p[idx].is_active = TRUE;

   return idx;
} /* find_th_idx */

/* Return the index of a thread table entry which can be used.  Take it 
   off the free list.  If the list is empty, reallocate the table to make
   it larger and andd the new entries to the free list.
   */

static int
get_th_idx  ()
{
  int   i;
  int   new_size;
  int   value;


  if (thread_info_free_list == END_OF_LIST)
  {
    /* Grow the thread_info_p table */

    new_size = thread_info_entries + 10 + (thread_info_entries / 4);
    thread_info_p = (t_hpux_thread_info*) 
        xrealloc(thread_info_p, new_size * (sizeof(t_hpux_thread_info)));

    /* Push the new entries onto the free list starting at the end so that
       they will be allocated in order.
       */

    for (i = new_size - 1;  i >= thread_info_entries; i--)
    {
      free_th_idx (i);
      nbr_user_threads++;  /* We didn't free a thread */
    }
    thread_info_entries = new_size;
  } /* if (thread_info_free_list == END_OF_LIST) */

  /* Return the head of the free list */

  value = thread_info_free_list;
  thread_info_free_list = thread_info_p[thread_info_free_list].next_handle;
  nbr_user_threads++;
  return value;

} /* get_th_idx */

/* init_th_idx
   Initialize the thread index table and the handle_hash_tbl.  This
   routine is called during initialization and when all threads have
   been deactivated.

   Initialize handle_hash_tbl and put all elements of thread_info_p[]
   on the free list.
   */

static void
init_th_idx ()
{
  int i;

  for (i = 0;  i < THREAD_INFO_HASH_SIZE;  i++)
  {
    handle_hash_tbl[i] = END_OF_LIST;
  }


  /* Again, we push entries onto the free list from the end so that they 
     will be allocated in order.
     */

  thread_info_free_list = END_OF_LIST;
  for (i = thread_info_entries - 1;  i >= 0;  i--)
  {
    free_th_idx (i);
  }
  nbr_user_threads = 0;
} /* init_th_idx */

/* search_for_handle - look for the thread handle in our table.  If found,
     return the thread index, otherwise, return NIL_THREAD_IDX.
   */

static int
search_for_handle (cma__t_int_tcb* thread_handle)
{
   int  idx;

   for (idx = handle_hash_tbl[HANDLE_HASH(thread_handle)];
        idx != -1;
        idx = thread_info_p[idx].next_handle)
   {
     if (thread_info_p[idx].thread_handle == thread_handle)
       return idx;
   }

   return NIL_THREAD_IDX;  /* thread not found */
} /* end search_for_handle */


/*

   LOCAL FUNCTION

   save_inferior_pid - Save inferior_pid on the cleanup list
   restore_inferior_pid - Restore inferior_pid from the cleanup list

   SYNOPSIS

   struct cleanup *save_inferior_pid ()
   void restore_inferior_pid (int pid)

   DESCRIPTION

   These two functions act in unison to restore inferior_pid in
   case of an error.

   NOTES

   inferior_pid is a global variable that needs to be changed by many of
   these routines before calling functions in procfs.c.  In order to
   guarantee that inferior_pid gets restored (in case of errors), you
   need to call save_inferior_pid before changing it.  At the end of the
   function, you should invoke do_cleanups to restore it.

 */


static struct cleanup *
save_inferior_pid ()
{
  return make_cleanup ((make_cleanup_ftype *) restore_inferior_pid, 
		       (void *) inferior_pid);
}

static void
restore_inferior_pid (pid)
     int pid;
{
  inferior_pid = pid;
}


static int cached_thread;
static cma__t_int_tcb cached_tcb;

/* Return the gdb pid of the active thread. */

int
hpux_find_active_thread ()
{
  int                   idx;
  int                   main_pid;
  struct minimal_symbol* ms;
  struct cleanup *      old_chain;
  cma__t_int_tcb*       tcb_ptr;

  if (is_prog_using_cma_threads == 0)
    return inferior_pid;
  
  if (!target_has_stack)
    return inferior_pid;

  main_pid = PIDGET(inferior_pid);
  old_chain = save_inferior_pid ();
  inferior_pid = PIDGET(inferior_pid);  

  /* If the process has just died, gdb still thinks it has a stack and
     execution, but read_memory would error out and call memory_error, so
     we call target_read_memory directly. */


  ms = lookup_minimal_symbol ("cma__g_current_thread", NULL, NULL);

  if (!ms)
    return inferior_pid;

  P_cma__g_current_thread = SYMBOL_VALUE_ADDRESS (ms);

  if (   target_read_memory ((CORE_ADDR)P_cma__g_current_thread,
                             (char *)&tcb_ptr,
                             sizeof tcb_ptr) 
      != 0)
  {
    do_cleanups (old_chain); /* restore inferior_pid */
    return inferior_pid;
  }

  do_cleanups (old_chain); 
  if ((idx = search_for_handle (tcb_ptr)) != NIL_THREAD_IDX)
    return MAKE_PID(idx, main_pid);

  /* We need to add the thread handle to the thread_info_p table, but
     we want consistent thread numbering, so we call hppa_find_new_threads()
     to go through the thread list in standard order and add the known
     threads.  
     */

  if (!hppa_find_new_threads_search (tcb_ptr))
    return inferior_pid;  /* Thread is not really active yet. */

  return MAKE_PID(find_th_idx (tcb_ptr), main_pid);
} /* end find_active_thread */

/* find_tcb - given a gdb pid, return a pointer to a copy of the TCB of the
   thread.   (Kept in cached_tcb.) */

static cma__t_int_tcb *
find_tcb (thread)
     int thread;
{
  struct cleanup *old_chain;
  int thread_idx;
  cma__t_known_object queue_header;
  cma__t_queue *queue_ptr;
  cma__t_int_tcb *thread_handle;

  if (thread == cached_thread)
    return &cached_tcb;

  thread_idx = THREAD_IDX(thread);
  assert (thread_idx < thread_info_entries &&
	  thread_info_p[thread_idx].is_active &&
	  thread_info_p[thread_idx].gdb_pid == thread);
  old_chain = save_inferior_pid ();
  inferior_pid = PIDGET(inferior_pid);  
  read_memory ((CORE_ADDR)thread_info_p[thread_idx].thread_handle, 
               (char *)&cached_tcb, 
               sizeof cached_tcb);
  do_cleanups (old_chain);

  cached_thread = thread;
  return &cached_tcb;
}

/* hpux_force_cma_thread_switch  called by define FORCE_CMA_THREAD_SWITCH
   Set the quantum on the current thread to zero and set the next thread
   to run to the thread corresponding to gdb_pid.
   */

void
hpux_force_cma_thread_switch (gdb_pid)
  int gdb_pid;
{
  int                   active_pid;
  int                   data;
  struct cleanup *      old_chain;
  cma__t_int_tcb*       tcb_p;
  cma__t_int_tcb*       thread_handle;

  /* Make sure that the CMA library is loaded and initialized. */

  if (is_prog_using_cma_threads == 0)
    return;  /* DCE/CMA subsystem not initialized yet. */

  if ((IS_CMA_PID(gdb_pid)) == 0)
    return;  /* Not a CMA thread */

  if (!target_has_execution)
    return;  /* Not running */

  thread_handle = THREAD_HANDLE(gdb_pid);
  
  /* Set the quantum of the current thread to zer0. */

  active_pid = hpux_find_active_thread();
  tcb_p = find_tcb (active_pid);  /* pointer to copy of tcb */
  thread_handle = THREAD_HANDLE(active_pid);
  data = 0;
  assert(sizeof(int) == sizeof(thread_handle->timer.quanta_remaining));
  old_chain = save_inferior_pid ();
  inferior_pid = PIDGET(inferior_pid);  
  write_memory ((CORE_ADDR) &thread_handle->timer.quanta_remaining,
                (char*) &data,  
                sizeof(thread_handle->timer.quanta_remaining));

  /* Make the desired thread the next thread to run */

  thread_handle = THREAD_HANDLE(gdb_pid);

  assert(   sizeof(int) 
         == sizeof(((cma__t_debug_state *) P_cma__g_debug_state)->next_to_run));
  write_memory ((CORE_ADDR) &((cma__t_debug_state *) 
                                 P_cma__g_debug_state)->next_to_run,
                (char*) &thread_handle,
                sizeof(((cma__t_debug_state *) 
                           P_cma__g_debug_state)->next_to_run));
          
  /* Tell the CMA library that things have chagned. */

  data = 1;
  assert(   sizeof(int) 
         == sizeof(((cma__t_debug_state *) 
                      P_cma__g_debug_state)->is_inconsistency));
  write_memory ((CORE_ADDR) &(((cma__t_debug_state *) 
                                   P_cma__g_debug_state)->is_inconsistency),
                (char*) &data,  
                sizeof(((cma__t_debug_state *) 
                           P_cma__g_debug_state)->is_inconsistency));
  do_cleanups (old_chain);

} /* end hpux_force_cma_thread_switch */


/* Most target vector functions from here on actually just pass through to
   inftarg.c, as they don't need to do anything specific for threads.  */

/* ARGSUSED */
static void
hpux_thread_open (arg, from_tty)
     char *arg;
     int from_tty;
{
  child_ops.to_open (arg, from_tty);
}

/* Attach to process PID, then initialize for debugging it
   and wait for the trace-trap that results from attaching.  */

static void
hpux_thread_attach (args, from_tty)
     char *args;
     int from_tty;
{
  child_ops.to_attach (args, from_tty);

  /* XXX - might want to iterate over all the threads and register them. */
}

static void
hpux_thread_require_attach (args, from_tty)
     char *args;
     int from_tty;
{
  child_ops.to_require_attach (args, from_tty);

  /* XXX - might want to iterate over all the threads and register them. */
}

/* Take a program previously attached to and detaches it.
   The program resumes execution and will no longer stop
   on signals, etc.  We'd better not have left any breakpoints
   in the program or it'll die when it hits one.  For this
   to work, it may be necessary for the process to have been
   previously attached.  It *might* work if the program was
   started via the normal ptrace (PTRACE_TRACEME).  */

static void
hpux_thread_detach (args, from_tty)
     char *args;
     int from_tty;
{
  child_ops.to_detach (args, from_tty);
}

static void
hpux_thread_require_detach (pid, args, from_tty)
     char *args;
     int from_tty;
{
  child_ops.to_require_detach (pid, args, from_tty);
}

/* Resume execution of process PID.  If STEP is nozero, then
   just single step it.  If SIGNAL is nonzero, restart it with that
   signal activated.  We may have to convert pid from a thread-id to an LWP id
   for procfs.  */

static void
hpux_thread_resume (pid, step, signo)
     int pid;
     int step;
     enum target_signal signo;
{
  struct cleanup *old_chain;

  old_chain = save_inferior_pid ();

  inferior_pid = PIDGET(inferior_pid);
  pid = PIDGET(pid);  /* If pid is -1, PIDGET returns -1 */

#if 0
  if (pid != -1)
    {
      pid = thread_to_lwp (pid, -2);
      if (pid == -2)		/* Inactive thread */
	error ("This version of Solaris can't start inactive threads.");
    }
#endif

  child_ops.to_resume (pid, step, signo);

  cached_thread = 0;

  do_cleanups (old_chain);
}

/* Wait for any threads to stop.  We may have to convert PID from a thread id
   to a LWP id, and vice versa on the way out.  */

static int
hpux_thread_wait (pid, ourstatus)
     int pid;
     struct target_waitstatus *ourstatus;
{
  int active_thread;
  int rtnval;
  struct cleanup *old_chain;

  old_chain = save_inferior_pid ();

  inferior_pid = PIDGET(inferior_pid);

  /* srikanth, wait only on the pid specified by wait for inferior.
     It is wrong to arbitrarily wait on inferior_pid when gdb asks
     to wait on something else. 100300.
  */

  if (pid != -1)
    pid = PIDGET(pid);

  rtnval = child_ops.to_wait (pid, ourstatus);

  /* After a fork, we can get an entirely new process.  If the active
     thread is part of the process indicated by rtnval, we want to return
     the thread_id of the thread, otherwise we want to return the pid
     returned by child_ops.to_wait.
     */
  

  active_thread = hpux_find_active_thread ();
  if (PIDGET (active_thread) == PIDGET (rtnval))
    rtnval = active_thread;

  do_cleanups (old_chain);

  return rtnval;
}

static char regmap32[NUM_REGS] =
{
  -2, -1, -1, 0, 4, 8, 12, 16, 20, 24,	/* flags, r1 -> r9 */
  28, 32, 36, 40, 44, 48, 52, 56, 60, -1,	/* r10 -> r19 */
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,	/* r20 -> r29 */

  /* r30, r31, sar, pcoqh, pcsqh, pcoqt, pcsqt, eiem, iir, isr */
  -2, -1, -1, -2, -1, -1, -1, -1, -1, -1,

  /* ior, ipsw, goto, sr4, sr0, sr1, sr2, sr3, sr5, sr6 */
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,

  /* sr7, cr0, cr8, cr9, ccr, cr12, cr13, cr24, cr25, cr26 */
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,

  -1, -1, -1, -1,		/* mpsfu_high, mpsfu_low, mpsfu_ovflo, pad */
  144, -1, -1, -1, -1, -1, -1, -1,	/* fpsr, fpe1 -> fpe7 */
  -1, -1, -1, -1, -1, -1, -1, -1,	/* fr4 -> fr7 */
  -1, -1, -1, -1, -1, -1, -1, -1,	/* fr8 -> fr11 */
  136, -1, 128, -1, 120, -1, 112, -1,	/* fr12 -> fr15 */
  104, -1, 96, -1, 88, -1, 80, -1,	/* fr16 -> fr19 */
  72, -1, 64, -1, -1, -1, -1, -1,	/* fr20 -> fr23 */
  -1, -1, -1, -1, -1, -1, -1, -1,	/* fr24 -> fr27 */
  -1, -1, -1, -1, -1, -1, -1, -1,	/* fr28 -> fr31 */
};

/* This table was largely derrived from looking at DDE code. */

static char regmap64[NUM_REGS] =
{
  -2, -1, -1, 0, 8, 16, 24, 32, 40, 48, /* flags, r1 -> r9 */
  56, 64, 72, 80, 88, 96, 104, 112, 120, -1, /* r10 -> r19 */
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, /* r20 -> r29 */

  /* r30, r31, sar, pcoqh, pcsqh, pcoqt, pcsqt, eiem, iir, isr */
  -2, -1, -1, -2, -1, -1, -1, -1, -1, -1,

  /* ior, ipsw, goto, sr4, sr0, sr1, sr2, sr3, sr5, sr6 */
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,

  /* sr7, cr0, cr8, cr9, ccr, cr12, cr13, cr24, cr25, cr26 */
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,

  -1, -1, -1, -1,               /* mpsfu_high, mpsfu_low, mpsfu_ovflo, pad */
  208, -1, -1, -1, -1, -1, -1, -1, /* fpsr, fpe1 -> fpe7 
                                      Guess 208=144+64 */
  -1, -1, -1, -1, -1, -1, -1, -1, /* fr4 -> fr7 */
  -1, -1, -1, -1, -1, -1, -1, -1, /* fr8 -> fr11 */
  200, -1, 192, -1, 184, -1, 176, -1, /* fr12 -> fr15 */
  168, -1, 160, -1, 152, -1, 144, -1, /* fr16 -> fr19 */
  136, -1, 128, -1, -1, -1, -1, -1, /* fr20 -> fr23 */
  -1, -1, -1, -1, -1, -1, -1, -1, /* fr24 -> fr27 */
  -1, -1, -1, -1, -1, -1, -1, -1, /* fr28 -> fr31 */
};

static char*    regmap          = regmap64;
static int      reg_save_offset = REG_SAVE_OFFSET_PA20;

/* Fetch the indicated register for inferior_pid which is a CMA thread. 
   -1 is a request to fetch all of the registers.
   */

void
hpux_thread_fetch_registers (regno)
     int regno;
{
  cma__t_int_tcb tcb, *tcb_ptr;
  struct cleanup *old_chain;
  int i;
  int first_regno, last_regno;
  int main_pid;
  int old_pid;

  main_pid = PIDGET(inferior_pid);
  old_pid = inferior_pid;
  old_chain = save_inferior_pid ();

  if ((IS_CMA_PID(old_pid)) == 0 ||
      old_pid == hpux_find_active_thread())
    {
      if (main_pid != inferior_pid)
	switch_to_thread(main_pid);
      fetch_inferior_registers (regno);
      do_cleanups (old_chain);
      registers_pid = old_pid;  /* The registers belong to the thread */
      return;
    }

  tcb_ptr = find_tcb (inferior_pid);
  inferior_pid = main_pid;

  if (regno == -1)
    {
      first_regno = 0;
      last_regno = NUM_REGS - 1;
    }
  else
    {
      first_regno = regno;
      last_regno = regno;
    }

  for (regno = first_regno; regno <= last_regno; regno++)
    {
      unsigned char buf[MAX_REGISTER_RAW_SIZE];

      if (regmap[regno] == -1)
        {
          /* Just supply 0 for the register */
          memset (buf, 0, MAX_REGISTER_RAW_SIZE);
          supply_register (regno, (char*) &buf[0]);
        }
      else
	{
	  CORE_ADDR sp;

	  sp = (CORE_ADDR) tcb_ptr->static_ctx.sp - reg_save_offset;

	  if (regno == FLAGS_REGNUM)
	    /* Flags must be 0 to avoid bogus value for SS_INSYSCALL */
	    memset (buf, '\000', REGISTER_RAW_SIZE (regno));
	  else if (regno == SP_REGNUM)
	    store_address (buf, sizeof sp, sp);
	  else if (regno == PC_REGNUM)
	    read_memory (sp - 20, (char *) &buf[0], REGISTER_RAW_SIZE (regno));
	  else
	    read_memory (sp + regmap[regno], (char *) &buf[0],
			 REGISTER_RAW_SIZE (regno));

	  supply_register (regno, (char *) &buf[0]);
	}
    }

  do_cleanups (old_chain);
}

static void
hpux_thread_store_registers (regno)
     int regno;
{
  cma__t_int_tcb tcb, *tcb_ptr;
  struct cleanup *old_chain;
  int i;
  int main_pid;
  int old_pid;
  int first_regno, last_regno;

  main_pid = PIDGET(inferior_pid);
  old_pid = inferior_pid;

  old_chain = save_inferior_pid ();

  if ((IS_CMA_PID(old_pid)) == 0 ||
      old_pid == hpux_find_active_thread())
    {
      if (main_pid != inferior_pid)
	switch_to_thread(main_pid);
      store_inferior_registers (regno);
      do_cleanups (old_chain);
      switch_to_thread(old_pid);
      return;
    }

  tcb_ptr = find_tcb (inferior_pid);
  inferior_pid = main_pid;

  if (regno == -1)
    {
      first_regno = 0;
      last_regno = NUM_REGS - 1;
    }
  else
    {
      first_regno = regno;
      last_regno = regno;
    }

  for (regno = first_regno; regno <= last_regno; regno++)
    {
      if (regmap[regno] == -1)
	child_ops.to_store_registers (regno);
      else
	{
	  unsigned char buf[MAX_REGISTER_RAW_SIZE];
	  CORE_ADDR sp;

          sp = (CORE_ADDR) tcb_ptr->static_ctx.sp - reg_save_offset;

	  if (regno == FLAGS_REGNUM)
	    child_ops.to_store_registers (regno);	/* Let lower layer handle this... */
	  else if (regno == SP_REGNUM)
	    {
	      write_memory ((CORE_ADDR) & tcb_ptr->static_ctx.sp,
			    registers + REGISTER_BYTE (regno),
			    REGISTER_RAW_SIZE (regno));
	      tcb_ptr->static_ctx.sp = (cma__t_hppa_regs *)
                (extract_address (registers + REGISTER_BYTE (regno), REGISTER_RAW_SIZE (regno)) + reg_save_offset);
	    }
	  else if (regno == PC_REGNUM)
	    write_memory (sp - 20,
			  registers + REGISTER_BYTE (regno),
			  REGISTER_RAW_SIZE (regno));
	  else
	    write_memory (sp + regmap[regno],
			  registers + REGISTER_BYTE (regno),
			  REGISTER_RAW_SIZE (regno));
	}
    }

  do_cleanups (old_chain);
}

/* Get ready to modify the registers array.  On machines which store
   individual registers, this doesn't need to do anything.  On machines
   which store all the registers in one fell swoop, this makes sure
   that registers contains all the registers from the program being
   debugged.  */

static void
hpux_thread_prepare_to_store ()
{
  child_ops.to_prepare_to_store ();
}

static int
hpux_thread_xfer_memory (memaddr, myaddr, len, dowrite, target)
     CORE_ADDR memaddr;
     char *myaddr;
     int len;
     int dowrite;
     struct target_ops *target;	/* ignored */
{
  int retval;
  struct cleanup *old_chain;

  old_chain = save_inferior_pid ();

  inferior_pid = PIDGET(inferior_pid);

  retval = child_ops.to_xfer_memory (memaddr, myaddr, len, dowrite, target);

  do_cleanups (old_chain);

  return retval;
}

/* Print status information about what we're accessing.  */

static void
hpux_thread_files_info (ignore)
     struct target_ops *ignore;
{
  child_ops.to_files_info (ignore);
}

static void
hpux_thread_kill_inferior ()
{
  child_ops.to_kill ();
}

static void
hpux_thread_notice_signals (pid)
     int pid;
{
  child_ops.to_notice_signals (pid);
}

/* Fork an inferior process, and start debugging it.  */

static void
hpux_thread_create_inferior (exec_file, allargs, env)
     char *exec_file;
     char *allargs;
     char **env;
{
  /* run without file or exec-file specification will get us
     NULLs here, i.e. start up gdb, type 'run'.
     FIXME: if exec_file is NULL we will try to obtain it from exec_bfd.
     This assert check is probably misplaced! */
  if (object_files && object_files->name && exec_file)
    assert(strcmp(object_files->name, exec_file) == 0);
  hpux_thread_new_objfile (NULL);  /* forget everything */
  hpux_thread_new_objfile (object_files);  /* process main program */
  child_ops.to_create_inferior (exec_file, allargs, env);
}

/* This routine is called whenever a new symbol table is read in, or when all
   symbol tables are removed.  libthread_db can only be initialized when it
   finds the right variables in libthread.so.  Since it's a shared library,
   those variables don't show up until the library gets mapped and the symbol
   table is read in.  */

/* This new_objfile event is now managed by a chained function pointer. 
 * It is the callee's responsability to call the next client on the chain.
 */

/* Saved pointer to previous owner of the new_objfile event. */
static void (*target_new_objfile_chain) (struct objfile *);

void
hpux_thread_new_objfile (objfile)
     struct objfile *objfile;
{
  struct minimal_symbol *ms;

  if (!objfile)
    {
      hpux_thread_active = 0;
      is_prog_using_cma_threads = 0;
      P_cma__g_known_threads = 0;
      init_th_idx ();
      goto quit;
    }

  ms = lookup_minimal_symbol ("cma__g_known_threads", NULL, objfile);

  if (!ms)
    goto quit;

  P_cma__g_known_threads = SYMBOL_VALUE_ADDRESS (ms);

  ms = lookup_minimal_symbol ("cma__g_current_thread", NULL, objfile);

  if (!ms)
    goto quit;

  P_cma__g_current_thread = SYMBOL_VALUE_ADDRESS (ms);

  ms = lookup_minimal_symbol ("cma__g_debug_state", NULL, objfile);
  if (!ms)
    return;
  P_cma__g_debug_state = SYMBOL_VALUE_ADDRESS (ms);
  if (target_has_stack)
    {
      set_regmap ();
    }


  /* In 11.31 HP-UX allows pids up to 2^30 -1.  (30-bits).  This is
   * being pre-enabled in AR0604.  We disallow CMA debugging if the 
   * PID >= 2^29.  PIDs of 2^23 were supported in 11.11 on PA.
   */

  if (!is_prog_using_cma_threads && inferior_pid >= (1 << PID_BITS_11_11))
    {
      /* The program is apparently a CMA program, but the PID is too
	 large for gdb to support it.  Put out a helpful warning.
       */

      warning (
	"Program is using CMA thread library but PID is > 2^29, so gdb\012\
  will not know about CMA threads.  See kernel tunable parameters\012\
  process_id_min and process_id_max.");  
      goto quit;
    }

  is_prog_using_cma_threads = 1;
  hpux_thread_active = 1;

  if (inferior_pid >= (1 << PID_BITS_ORIG))
    {
      is_pid_larger = 1;
      /*error ("Process ID is greater than 65535.");*/
    }

  /* Don't push the target for a corefile which doesn't have execution */
  if (target_has_execution)
    push_target (&hpux_thread_ops);
  inferior_pid = hpux_find_active_thread ();

quit:
  /* Call predecessor on chain, if any. */
  if (target_new_objfile_chain)
    target_new_objfile_chain (objfile);
}

/* Clean up after the inferior dies.  */

static void
hpux_thread_mourn_inferior ()
{
  unpush_target (&hpux_thread_ops);
  hpux_thread_active = 0;
  child_ops.to_mourn_inferior ();
}

/* Mark our target-struct as eligible for stray "run" and "attach" commands.  */

static int
hpux_thread_can_run ()
{
  /* srikanth, this code is problematic in follow fork/exec situations. We end
     up using the target vector for CMA process even when the process is not
     CMA threaded (for the exec'd child.) Not sure what are all complications
     that would result, but at the least the handle command core dumps when
     issued in the context of the exec'd process. Seen in Java 1.4
 
     I am disabling this for 11.00. Do we even support debugging CMA programs
     on 11.00 ??? I think this is an improvement, not a fix.
  */
#ifdef HPUX_1020
  return child_suppress_run;
#else
  return 0;
#endif
}

static int
hpux_thread_alive (pid)
     int pid;
{
  return 1;
}

static void
hpux_thread_stop ()
{
  child_ops.to_stop ();
}

/*-----------------------------------------------------------------------------
  This section defines simple pass-through routines that call the 
  equivalent child_ops routines.  Originally and in the Solaris 
  implementation, these were all NULL.  It is possible that some 
  of these need more than just pass-through routines.
-----------------------------------------------------------------------------*/

static void
hpux_thread_acknowledge_created_inferior (pid)
{
  child_ops.to_acknowledge_created_inferior (pid);
}

static int
hpux_thread_can_follow_vfork_prior_to_exec ()
{
  return child_ops.to_can_follow_vfork_prior_to_exec ();
} 

static void
hpux_thread_clone_and_follow_inferior (child_pid, followed_child)
  int  child_pid;
  int  *followed_child;
{
  child_ops.to_clone_and_follow_inferior (child_pid, followed_child);
} 

static char *
hpux_thread_core_file_to_sym_file (core)
  char *  core;
{
  return child_ops.to_core_file_to_sym_file (core);
}

static struct symtab_and_line *
hpux_thread_enable_exception_callback (kind, enable)
  enum exception_event_kind kind;
  int enable;
{
  return child_ops.to_enable_exception_callback (kind, enable);
}

static struct exception_event_record *
hpux_thread_get_current_exception_event ()
{
  return child_ops.to_get_current_exception_event ();
}

static int
hpux_thread_has_execd (pid, execd_pathname)
  int  pid;
  char **  execd_pathname;
{
  return child_ops.to_has_execd (pid, execd_pathname);
}

static int
hpux_thread_has_exited (pid, wait_status, exit_status)
  int  pid;
  int  wait_status;
  int *  exit_status;
{
  return child_ops.to_has_exited (pid, wait_status, exit_status);
}

static int
hpux_thread_has_forked (pid, child_pid)
  int  pid;
  int *  child_pid;
{
  return child_ops.to_has_forked (pid, child_pid);
}

static int
hpux_thread_has_syscall_event (pid, kind, syscall_id)
  int  pid;
  enum target_waitkind *  kind;
  int *  syscall_id;
{
  return child_ops.to_has_syscall_event (pid, kind, syscall_id);
}

static int
hpux_thread_has_vforked (pid, child_pid)
  int  pid;
  int *  child_pid;
{
  return child_ops.to_has_vforked (pid, child_pid);
}

static int
hpux_thread_insert_exec_catchpoint (pid)
  int  pid;
{
  return child_ops.to_insert_exec_catchpoint (pid);
}

static int
hpux_thread_insert_fork_catchpoint (pid)
  int  pid;
{
  return child_ops.to_insert_fork_catchpoint (pid);
}

static int
hpux_thread_insert_vfork_catchpoint (pid)
  int  pid;
{
  return child_ops.to_insert_vfork_catchpoint (pid);
}

static char *
hpux_thread_pid_to_exec_file (pid)
  int  pid;
{
  return child_ops.to_pid_to_exec_file (pid);
}

static void
hpux_thread_post_attach (pid)
  int  pid;
{
  child_ops.to_post_attach (pid);
}

static void
hpux_thread_post_follow_inferior_by_clone ()
{
  child_ops.to_post_follow_inferior_by_clone ();
}

static void
hpux_thread_post_follow_vfork (parent_pid, followed_parent, child_pid, 
                               followed_child)
  int  parent_pid;
  int  followed_parent;
  int  child_pid;
  int  followed_child;
{
  child_ops.to_post_follow_vfork (parent_pid, followed_parent, child_pid,
                                 followed_child);
}

static void
hpux_thread_post_startup_inferior (pid)
  int  pid;
{
  child_ops.to_post_startup_inferior (pid);
}

static void
hpux_thread_post_wait (pid, wait_status)
  int  pid;
  int  wait_status;
{
  child_ops.to_post_wait (pid, wait_status);
}

static int
hpux_thread_remove_exec_catchpoint (pid)
  int  pid;
{
  return child_ops.to_remove_exec_catchpoint (pid);
}

static int
hpux_thread_remove_fork_catchpoint (pid)
  int  pid;
{
  return child_ops.to_remove_fork_catchpoint (pid);
}

static int
hpux_thread_remove_vfork_catchpoint (pid)
  int  pid;
{
  return child_ops.to_remove_vfork_catchpoint (pid);
}

static int
hpux_thread_reported_exec_events_per_exec_call ()
{
  return child_ops.to_reported_exec_events_per_exec_call ();
}

/*-----------------------------------------------------------------------------
  End of pass-through target routines.
-----------------------------------------------------------------------------*/

/* hppa_find_new_threads_search
   This is like hppa_find_new_threads except that it returns TRUE if the
   tcb_sought is one of the tcb points on the thread list.

   During startup, the DCE data structure cma__g_current_thread can 
   give a tcb pointer, but if that pointer isn't on the list of threads,
   it really isn't the active thread.  hppa_find_new_threads_search is
   called with the value of cma__g_current_thread, and if it returns
   FALSE, then there really isn't an active thread.
   */

static int
hppa_find_new_threads_search(tcb_sought)
  cma__t_int_tcb *      tcb_sought;
{
  unsigned int                  cma_init_done;
  CORE_ADDR                     cma_known_threads_addr;
  int                           idx;
  struct minimal_symbol *       min_sym;
  struct minimal_symbol         *ms;
  cma__t_known_object           queue_header;
  cma__t_queue *                queue_ptr;
  cma__t_int_tcb                temp_tcb;
  int                           tcb_found = FALSE;

  /* don't do anything if _initialize_hpux_thread didn't set child_suppress_run
     */

  if (!child_suppress_run)
    return tcb_found;

  if (inferior_pid == -1)
    {
      printf_filtered("No process.\n");
      return tcb_found;
    }

  /* Make sure that the CMA library is loaded and initialized. */

  if (is_prog_using_cma_threads == 0)
    return tcb_found;  /* DCE/CMA subsystem not initialized yet. */

  /* Go through the thread_info_p table and mark all entries as not
     active.  As we go through the CMA threads, we will mark the ones
     we find in our table as active.  Any which are marked inactive and
     are not no the free list, will be removed below.
     */

  for (idx = 0;  idx < thread_info_entries; idx++)
  {
    thread_info_p[idx].is_active = FALSE;
  }
    
  /* Iterate through the DCE/CMA threads. */


  ms = lookup_minimal_symbol ("cma__g_known_threads", NULL, NULL);

  if (!ms)
    return tcb_found;

  P_cma__g_known_threads = SYMBOL_VALUE_ADDRESS (ms);

  if (   target_read_memory ((CORE_ADDR)P_cma__g_known_threads,
                             (char *)&queue_header,
                             sizeof queue_header) 
      != 0)
  {
    return tcb_found;
  }
  
  for (queue_ptr = queue_header.queue.flink;
       queue_ptr != 0 && queue_ptr != (cma__t_queue *)P_cma__g_known_threads;
       queue_ptr = temp_tcb.threads.flink)
  {
    cma__t_int_tcb *            tcb_ptr;

    tcb_ptr = cma__base (queue_ptr, threads, cma__t_int_tcb);
    if (   target_read_memory ((CORE_ADDR)tcb_ptr,
                               (char *)&temp_tcb,
                               sizeof temp_tcb) 
        != 0)
    {
      return FALSE;
    }
    if (temp_tcb.header.type == cma__c_obj_tcb)
    {
      idx = find_th_idx (tcb_ptr);  /* Add it to table if not found */
      thread_info_p[idx].is_active = TRUE;
      if (tcb_ptr == tcb_sought)
        tcb_found = TRUE;
    }
  } /* for queue_ptr */

  /* Any threads which are still inactive and not on the free list 
     should be removed. */

  for (idx = 0;  idx < thread_info_entries; idx++)
  {
    if (   thread_info_p[idx].is_active == FALSE
        && thread_info_p[idx].gdb_thread_num != NIL_THREAD_NUM) 
    {
      delete_thread (thread_info_p[idx].gdb_pid);
      free_th_idx (idx);
    }
  }
  return tcb_found;
} /* end hppa_find_new_threads_search */

/* hppa_find_new_threads
   This routine is called to update gdb's knowledge of DCE threads.  It is
   invoked from info_threads_command through the FIND_NEW_THREADS macro.
   */

void
hppa_find_new_threads()
{
  hppa_find_new_threads_search(0);
}

/* set_regmap - set regmap and reg_save_offset to get either 32-bit registers
     or 64 bit registers based on the SS_WIDEREGS bit of the flags register.
   */

static void set_regmap ()
{
  int old_pid;
  int flags;

  old_pid = inferior_pid;
  if (IS_CMA_PID(inferior_pid))
    inferior_pid = PIDGET(inferior_pid);
  flags = read_register (FLAGS_REGNUM);
  inferior_pid = old_pid;

  if (flags & SS_WIDEREGS)
  {
    regmap = regmap64;
    reg_save_offset = REG_SAVE_OFFSET_PA20;
  }
  else 
  {
    regmap = regmap32;
    reg_save_offset = REG_SAVE_OFFSET;
  }
} /* end set_regmap */


static void
init_hpux_thread_ops ()
{
  hpux_thread_ops.to_shortname = "hpux-threads";
  hpux_thread_ops.to_longname = "HPUX threads and pthread.";
  hpux_thread_ops.to_doc = "HPUX threads and pthread support.";
  hpux_thread_ops.to_open = hpux_thread_open;
  hpux_thread_ops.to_attach = hpux_thread_attach;
  hpux_thread_ops.to_require_attach = hpux_thread_require_attach;
  hpux_thread_ops.to_detach = hpux_thread_detach;
  hpux_thread_ops.to_require_detach = hpux_thread_require_detach;
  hpux_thread_ops.to_resume = hpux_thread_resume;
  hpux_thread_ops.to_wait = hpux_thread_wait;
  hpux_thread_ops.to_fetch_registers = hpux_thread_fetch_registers;
  hpux_thread_ops.to_store_registers = hpux_thread_store_registers;
  hpux_thread_ops.to_prepare_to_store = hpux_thread_prepare_to_store;
  hpux_thread_ops.to_xfer_memory = hpux_thread_xfer_memory;
  hpux_thread_ops.to_files_info = hpux_thread_files_info;
  hpux_thread_ops.to_insert_breakpoint = memory_insert_breakpoint;
  hpux_thread_ops.to_remove_breakpoint = memory_remove_breakpoint;
  hpux_thread_ops.to_terminal_init = terminal_init_inferior;
  hpux_thread_ops.to_terminal_inferior = terminal_inferior;
  hpux_thread_ops.to_terminal_ours_for_output = terminal_ours_for_output;
  hpux_thread_ops.to_terminal_ours = terminal_ours;
  hpux_thread_ops.to_terminal_info = child_terminal_info;
  hpux_thread_ops.to_kill = hpux_thread_kill_inferior;
  hpux_thread_ops.to_create_inferior = hpux_thread_create_inferior;
  hpux_thread_ops.to_create_inferior = hpux_thread_create_inferior;
  hpux_thread_ops.to_post_startup_inferior = hpux_thread_post_startup_inferior;
  hpux_thread_ops.to_acknowledge_created_inferior =
    hpux_thread_acknowledge_created_inferior;
  hpux_thread_ops.to_clone_and_follow_inferior =
    hpux_thread_clone_and_follow_inferior;
  hpux_thread_ops.to_post_follow_inferior_by_clone =
    hpux_thread_post_follow_inferior_by_clone;
  hpux_thread_ops.to_insert_fork_catchpoint =
    hpux_thread_insert_fork_catchpoint;
  hpux_thread_ops.to_remove_fork_catchpoint =
    hpux_thread_remove_fork_catchpoint;
  hpux_thread_ops.to_insert_vfork_catchpoint =
    hpux_thread_insert_vfork_catchpoint;
  hpux_thread_ops.to_remove_vfork_catchpoint =
    hpux_thread_remove_vfork_catchpoint;
  hpux_thread_ops.to_has_forked = hpux_thread_has_forked;
  hpux_thread_ops.to_has_vforked = hpux_thread_has_vforked;
/* JAGaf20569 : To handle prefork event for cma in 11.22 and above systems. */
#if TT_FEATURE_LEVEL >= 10
  hpux_thread_ops.to_has_forks_prefork = process_has_forks_prefork;
#endif
  hpux_thread_ops.to_can_follow_vfork_prior_to_exec =
    hpux_thread_can_follow_vfork_prior_to_exec;
  hpux_thread_ops.to_post_follow_vfork = hpux_thread_post_follow_vfork;
  hpux_thread_ops.to_insert_exec_catchpoint = hpux_thread_insert_exec_catchpoint;
  hpux_thread_ops.to_remove_exec_catchpoint = hpux_thread_remove_exec_catchpoint;
  hpux_thread_ops.to_has_execd = hpux_thread_has_execd;
  hpux_thread_ops.to_reported_exec_events_per_exec_call =
    hpux_thread_reported_exec_events_per_exec_call;
  hpux_thread_ops.to_has_syscall_event = hpux_thread_has_syscall_event;
  hpux_thread_ops.to_has_exited = hpux_thread_has_exited;
  hpux_thread_ops.to_mourn_inferior = hpux_thread_mourn_inferior;
  hpux_thread_ops.to_can_run = hpux_thread_can_run;
  hpux_thread_ops.to_notice_signals = hpux_thread_notice_signals;
  hpux_thread_ops.to_thread_alive = hpux_thread_alive;
  hpux_thread_ops.to_stop = hpux_thread_stop;
  hpux_thread_ops.to_enable_exception_callback =
    hpux_thread_enable_exception_callback;
  hpux_thread_ops.to_get_current_exception_event =
    hpux_thread_get_current_exception_event;
  hpux_thread_ops.to_pid_to_exec_file = hpux_thread_pid_to_exec_file;
  hpux_thread_ops.to_core_file_to_sym_file = hpux_thread_core_file_to_sym_file;
  hpux_thread_ops.to_stratum = process_stratum;
  hpux_thread_ops.to_has_all_memory = 1;
  hpux_thread_ops.to_has_memory = 1;
  hpux_thread_ops.to_has_stack = 1;
  hpux_thread_ops.to_has_registers = 1;
  hpux_thread_ops.to_has_execution = 1;
  hpux_thread_ops.to_magic = OPS_MAGIC;
}

void
_initialize_hpux_thread ()
{
  init_hpux_thread_ops ();
  add_target (&hpux_thread_ops);

  child_suppress_run = 1;
  P_cma__g_known_threads = 0;
  is_prog_using_cma_threads = 0;
  init_th_idx ();
  /* Hook into new_objfile notification. */
  target_new_objfile_chain = target_new_objfile_hook;
  target_new_objfile_hook  = hpux_thread_new_objfile;
}
