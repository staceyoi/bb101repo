/* Target-dependent code for the HP PA architecture, for GDB.
   Copyright 1986, 1987, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996
   Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "frame.h"
#include "top.h"
#include "bfd.h"
#include "inferior.h"
#include "value.h"
#include "floatformat.h"
#include "gdbtypes.h"

/* For argument passing to the inferior */
#include "symtab.h"

/* For Fortran array descriptor routines */
#include "f-lang.h"

/* For DOOM specific structs */
#include "hpread.h"

#ifdef USG
#include <sys/types.h>
#endif

#include <assert.h>
#include <dl.h>
#include <sys/param.h>
#include <signal.h>
#include <strings.h>
#include <siginfo.h>
#include <stdlib.h>
#include <langtypes.h>
#include <sys/pthread.h>
#include <sys/ttrace.h> 


/*#include <sys/user.h>         After a.out.h  */
#include "gdb_stat.h"
#include <sys/wait.h>

#include "elf-bfd.h"
#include "gdbcore.h"
#include "gdbcmd.h"
#include "target.h"
#include "symfile.h"
#include "objfiles.h"

#include "langtypes.h"
#include "uwx.h"
#include <dlfcn.h>
#include <unwind_dld.h>
#include "javalib.h"    /* For Java Stack trace */

#ifndef HP_IA64_GAMBIT
#include <sys/uc_access.h>
#endif

#include "crt0.h"
#include "mixed_mode.h"

#define DEBUG(x) 

#define UWX_ARG_ADDR64	uint64_t *

#define UNW_VER(x) ((x) >> 48)
#define UNW_FLAG_MASK(x) ((x >> 32) & 0x0000ffff)
#define UNW_LENGTH(x) (x & 0xffffffff)

#ifdef __LP64__
#define UC_GET_RSEBS   __uc_get_rsebs
#define UC_ARG2_TYPE   uint64_t *
#else
#define UC_GET_RSEBS   __uc_get_rsebs64
#define UC_ARG2_TYPE   ptr64_t
#endif

/* To identify a sigcontext frame. */
#define UWX_ABI_HPUX_SIGCONTEXT 0x0101  /* abi = HP-UX, context = 1 */

#define TTRACE_ARG_TYPE uint64_t
 
extern CORE_ADDR reg_bs_addr (int, CORE_ADDR, CORE_ADDR);
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
extern CORE_ADDR mixed_mode_frame_chain (struct frame_info *frame);
extern int hppa_frame_chain_valid (CORE_ADDR chain, struct frame_info *frame);
extern CORE_ADDR mixed_mode_frame_saved_pc (struct frame_info *frame);
extern CORE_ADDR hppa_saved_pc_after_call (struct frame_info *frame);
extern void mixed_mode_init_extra_frame_info (int fromleaf, struct frame_info *frame);
extern bool mixed_mode_pa_unwind;
extern int mixed_mode_handle_bad_pc (struct frame_info * frame);
extern void mixed_mode_do_handle_bad_pc (struct frame_info * frame,
                                         struct frame_info * prev);
extern struct frame_info *aries_border_frame;
extern CORE_ADDR inferior_aries_text_start;
extern CORE_ADDR inferior_aries_text_end;
extern struct frame_info *mixed_mode_adjust (struct frame_info *fi);
extern CORE_ADDR aries_pa2ia_export_stub_addr;
extern int aries_pa2ia_export_stub_size;

/* --------- defines which depend on previous defines -------*/
 
#define TT_NIL ((TTRACE_ARG_TYPE) TT_NULLARG)

extern unsigned int stop_whining;
#define IN_SYSCALL (read_register (REASON_REGNUM) == 0)

extern int read_nat_addr (CORE_ADDR, char*, int); /* From ia64h-nat.c */

extern struct objfile *symfile_objfile;
extern boolean print_bad_pc_unwind_msg;

extern char* mismatched_so_name;

#ifdef HP_MXN
/* To support detection of the pseudo-initial frame
 * that threads have.
 */
#define THREAD_INITIAL_FRAME_SYMBOL  "__pthread_unbound_body"
#define THREAD_INITIAL_FRAME_SYMBOL2  "__thread_start0"
#define THREAD_INITIAL_FRAME_SYMBOL3  "__block_upcall_handler"
#endif

extern int remote_ia64_flush_RSE PARAMS ((void));
extern void objectdir_command (char *dirname, int from_tty);
extern int check_natval (const struct floatformat *fmt,
     			 char *from);

/* Data structure to maintain the information about the
   unwind session */
struct uwx_info {
    ucontext_t *ucontext; /* pointer to ucontext structure of the sig
				 context frame.*/
    uint64_t rvec[10];
    CORE_ADDR dummy_fp; /* frame pointer for the dummy frame.*/
    int frame_status; /* 0 Started from topmost frame
			 1 did we restart at the sig context frame?
			 2 did we restart at call dummy?  */
};

/* To hold the header fields of ucontext structure. */
struct uc_header {
    uint32_t version;
    uint32_t size_in_bytes;
};

/* Token that we pass to unwind express. This token is passed back to
   us when calling our callback functions. */
struct uwx_info *cbinfo;

/* The bottommost unwind context that we have so far */
struct uwx_env *current_uc = NULL;

extern flip_desc_t load_info_desc;

extern CORE_ADDR saved_load_info_addr;
extern bool objfile_is_mixed_mode (struct objfile *objfile);

/* How far is the bottommost unwind context from the bottommost frame
   we have constructed. If uc_distance is 0, current_uc has current frame's
   unwind_context. If uc_distance is 1, current_uc has prev frame's uc.
   If uc_distance is -1, we have either reached the bottom of stack or
   encountered an error. */
int uc_distance = 0;

load_info_t load_info;
CORE_ADDR load_info_addr;
int have_read_load_info = 0;

// watchpoint
boolean reached_main_in_frame_chain = false;

boolean no_predication_handling = FALSE;
/* Forward declarations. */
void ia64_frame_find_saved_regs (struct frame_info *,
                                struct frame_saved_regs *);
void java_set_native_context (struct uwx_env *, struct frame_info *, CORE_ADDR);
void get_saved_register_from_uc (int , char* , ucontext_t *);
void set_initial_context (struct uwx_env *, CORE_ADDR);
void set_context_from_sigcontext (struct uwx_env *, ucontext_t *);
int ia64_uwx_lookupip ( int, uint64_t, intptr_t, uint64_t**);
int ia64_uwx_copyin ( int, char *, uint64_t, int, intptr_t);
static int loadmap_from_ip (struct load_module_desc *desc, Int64 pc);
//static int ia64_pc_in_gcc_function (CORE_ADDR);

#ifdef COVARIANT_SUPPORT
static CORE_ADDR find_cov_thunk_next_pc (CORE_ADDR pc);
#endif /* COVARIANT_SUPPORT */

#define PC_IN_USER_SENDSIG(pc) \
	((pc >= load_info.li_sendsig_txt) \
	&& (pc < (load_info.li_sendsig_txt + load_info.li_sendsig_tsz)))
static char *reg_names[] = REGISTER_NAMES;

/* Template types along with execution units are needed to correctly identify
 * instructions.  Template encodings come directly from the IA64 manual.
 * Implemented as part of support for covariant return types on IA64, but is
 * relevant for any instruction decoding.
 * - Bharath, 1 July, 2004.
 */
#define MAX_TEMPLATES 32
#define MAX_SLOTS 3
enum exec_unit { undefined = 0, M, I, L, X, F, B };
enum exec_unit inst_type[MAX_TEMPLATES][MAX_SLOTS] = {
  {M, I, I},	/* Template 0x00 */
  {M, I, I},	/* Template 0x01 */
  {M, I, I},	/* Template 0x02 */
  {M, I, I},	/* Template 0x03 */
  {M, L, X},	/* Template 0x04 */
  {M, L, X},	/* Template 0x05 */
  {0, 0, 0},	/* Template 0x06 */
  {0, 0, 0},	/* Template 0x07 */
  {M, M, I},	/* Template 0x08 */
  {M, M, I},	/* Template 0x09 */
  {M, M, I},	/* Template 0x0a */
  {M, M, I},	/* Template 0x0b */
  {M, F, I},	/* Template 0x0c */
  {M, F, I},	/* Template 0x0d */
  {M, M, F},	/* Template 0x0e */
  {M, M, F},	/* Template 0x0f */
  {M, I, B},	/* Template 0x10 */
  {M, I, B},	/* Template 0x11 */
  {M, B, B},	/* Template 0x12 */
  {M, B, B},	/* Template 0x13 */
  {0, 0, 0},	/* Template 0x14 */
  {0, 0, 0},	/* Template 0x15 */
  {B, B, B},	/* Template 0x16 */
  {B, B, B},	/* Template 0x17 */
  {M, M, B},	/* Template 0x18 */
  {M, M, B},	/* Template 0x19 */
  {0, 0, 0},	/* Template 0x1a */
  {0, 0, 0},	/* Template 0x1b */
  {M, F, B},	/* Template 0x1c */
  {M, F, B},	/* Template 0x1d */
  {0, 0, 0},	/* Template 0x1e */
  {0, 0, 0}	/* Template 0x1f */
};

/* Stacey 03/21/2002
   Data structures for saving old register and floating point register
   values in case the user wants to print changed registers when in VDB
   disassembly mode 
   
   FIXME:  We really should be using gdb standard data structures here.  
   It would make the code much cleaner and in some cases faster.  */

static long long *saved_register_values = NULL;
typedef char (*saved_fp_register_values_type)[MAX_REGISTER_VIRTUAL_SIZE];
static char (*saved_fp_register_values) [MAX_REGISTER_VIRTUAL_SIZE] = NULL;

/* JAGaf76770 - support for MCA recovery core file code 
   This define needs to be removed once it makes it into siginfo.h */

#ifndef BUS_MCARECOV
#define BUS_MCARECOV 4
#endif
 
#ifndef ILL_MCARECOV
#define ILL_MCARECOV 12
#endif

/* Fix for JAGae99882.  Bharath, 9 Feb 2004. */
struct target_si_code_t si_code_map[] =
{
  {SIGILL, ILL_UNKNOWN, "ILL_UNKNOWN - Unknown Error"},
  {SIGILL, ILL_ILLOPC, "ILL_ILLOPC - Illegal Op-Code"},
  {SIGILL, ILL_ILLOPN, "ILL_ILLOPN - Illegal Operand"},
  {SIGILL, ILL_ILLADR, "ILL_ILLADR - Illegal addressing mode"},
  {SIGILL, ILL_ILLTRP, "ILL_ILLTRP - Illegal trap"},
  {SIGILL, ILL_PRVOPC, "ILL_PRVOPC - Privileged instruction trap"},
  {SIGILL, ILL_PRVREG, "ILL_PRVREG - Privileged register trap"},
  {SIGILL, ILL_COPROC, "ILL_COPROC - Coprocessor error"},
  {SIGILL, ILL_BADSTK, "ILL_BADSTK - Internal stack error"},
  {SIGILL, ILL_REGNAT, "ILL_REGNAT - Register NaT Consumption"},
  {SIGILL, ILL_BREAK,  "ILL_BREAK  - Application Break instruction"},
  {SIGILL, ILL_ILLDEP, "ILL_ILLDEP - Illegal Dependency fault"},
  {SIGILL, ILL_MCARECOV, "ILL_MCARECOV - Machine Check Abort error - User state lost MCA"},

  {SIGFPE, FPE_UNKNOWN, "FPE_UNKNOWN - Unknown Error"},
  {SIGFPE, FPE_INTDIV, "FPE_INTDIV - integer divide by zero"},
  {SIGFPE, FPE_INTOVF, "FPE_INTOVF - integer overflow"},
  {SIGFPE, FPE_FLTDIV, "FPE_FLTDIV - FP divide by zero"},
  {SIGFPE, FPE_FLTOVF, "FPE_FLTOVF - FP overflow"},
  {SIGFPE, FPE_FLTUND, "FPE_FLTUND - FP underflow"},
  {SIGFPE, FPE_FLTRES, "FPE_FLTRES - FP inexact result"},
  {SIGFPE, FPE_FLTINV, "FPE_FLTINV - invalid FP operation"},
  {SIGFPE, FPE_FLTSUB, "FPE_FLTSUB - subscript out of range"},
  {SIGFPE, FPE_DECOVF, "FPE_DECOVF - Decimal Overflow"},
  {SIGFPE, FPE_DECDIV, "FPE_DECDIV - Decimal Divide by Zero"},
  {SIGFPE, FPE_DECERR, "FPE_DECERR - Packed Decimal Error"},
  {SIGFPE, FPE_INVASC, "FPE_INVASC - Invalid ASCII Digit"},
  {SIGFPE, FPE_INVDEC, "FPE_INVDEC - Invalid Decimal Digit"},

  {SIGSEGV, SEGV_UNKNOWN, "SEGV_UNKNOWN - Unknown Error"},
  {SIGSEGV, SEGV_MAPERR, "SEGV_MAPERR - Address not mapped to object"},
  {SIGSEGV, SEGV_ACCERR, "SEGV_ACCERR - Invalid Permissions for object"},
  {SIGSEGV, SEGV_PSTKOVF, "SEGV_PSTKOVF - Paragraph Stack Overflow"},

  {SIGBUS, BUS_UNKNOWN, "BUS_UNKNOWN - Unknown Error"},
  {SIGBUS, BUS_ADRALN, "BUS_ADRALN - Invalid address alignment. Please refer to the following link that helps in handling unaligned data: http://docs.hp.com/en/7730/newhelp0610/pragmas.htm#pragma-pack-ex3"},
  {SIGBUS, BUS_ADRERR, "BUS_ADRERR - Non-existant physical address"},
  {SIGBUS, BUS_OBJERR, "BUS_OBJERR - Object specific hardware error"},
  {SIGBUS, BUS_MCARECOV, "BUS_MCARECOV - Machine Check Abort error - User memory state lost MCA"},

  {SIGTRAP, TRAP_UNKNOWN, "TRAP_UNKNOWN - Unknown Error"},
  {SIGTRAP, TRAP_BRKPT, "TRAP_BRKPT - Process breakpoint"},
  {SIGTRAP, TRAP_TRACE, "TRAP_TRACE - Process trace trap"},

  {SIGCHLD, CLD_UNKNOWN, "CLD_UNKNOWN - Unknown child status change"},
  {SIGCHLD, CLD_EXITED, "CLD_EXITED - Child exited"},
  {SIGCHLD, CLD_KILLED, "CLD_KILLED - Abnormal termination, no core file"},
  {SIGCHLD, CLD_DUMPED, "CLD_DUMPED - Abnormal termination w/core file"},
  {SIGCHLD, CLD_TRAPPED, "CLD_TRAPPED - Traced child has trapped"},
  {SIGCHLD, CLD_STOPPED, "CLD_STOPPED - Child has stopped"},
  {SIGCHLD, CLD_CONTINUED, "CLD_CONTINUED - Stopped child has continued"},

  {SIGPOLL, POLL_UNKNOWN, "POLL_UNKNOWN - Unknown poll code"},
  {SIGPOLL, POLL_IN, "POLL_IN - Data input available"},
  {SIGPOLL, POLL_OUT, "POLL_OUT - Output buffers available"},
  {SIGPOLL, POLL_MSG, "POLL_MSG - Input message available"},
  {SIGPOLL, POLL_ERR, "POLL_ERR - I/O Error"},
  {SIGPOLL, POLL_PRI, "POLL_PRI - High priority input available"},
  {SIGPOLL, POLL_HUP, "POLL_HUP - Device disconnected"},

  {SIGGFAULT, GFAULT_EXPLICIT, "GFAULT_EXPLICIT - SIGGFAULT caused by GCLOCK"},
  {SIGGFAULT, GFAULT_IMPLICIT, "GFAULT_IMPLICIT - SIGGFAULT caused by fault on graphics device."}
};

const int si_code_map_size = sizeof (si_code_map) /
			     sizeof (struct target_si_code_t);

extern CORE_ADDR backtrace_other_thread_bsp;

/* QXCR1000579474: Aries core file debugging. */
extern CORE_ADDR aries_runtime_text_start;
extern CORE_ADDR aries_linktime_unwind_start;
extern CORE_ADDR aries_runtime_unwind_start;
extern unsigned long aries_text_size;
extern char *aries_module_name;

#ifdef GET_LONGJMP_TARGET
int
get_longjmp_target(CORE_ADDR *pc)
{
  long long jb_addr;
  char buf[TARGET_PTR_BIT / TARGET_CHAR_BIT];

  jb_addr = read_register (GR0_REGNUM + 32);
  /* the pc where the step-over of a longjmp call should result in is the one that follows the
     corresponding setjmp call in the program. We get this value from the jump-buffer structure,
     the address of which is got from the argument register. The address of jump-buffer 
     is the first argument to a longjmp call */
  if (target_read_memory ((CORE_ADDR) (jb_addr + 7 * sizeof (long long)),
			  buf,
			  TARGET_PTR_BIT / TARGET_CHAR_BIT))
     return 0;

  *pc = extract_address (buf, TARGET_PTR_BIT / TARGET_CHAR_BIT);

  return 1; 
}
#endif

int
pc_in_user_sendsig(CORE_ADDR pc)
{
  if (PC_IN_USER_SENDSIG(pc))
     return 1;
  else
     return 0;
}

/* This function reads in the load_info structure provided by the kernal. 
   We can either get it from the process arguments (4th arg) or reading 
   in the __load_info symbol of dld. */
void
get_load_info()
{
  extern CORE_ADDR core_rse_stack;

  CORE_ADDR load_info_addr;
  CORE_ADDR arg_list_addr;
  CORE_ADDR sym_addr;
  int status;
  struct minimal_symbol *msymbol = 0;
#ifdef HP_IA64_NATIVE
  if (target_has_stack && !target_has_execution) /* corefile */
    {
      /* The 4th 64-bit word of the RSE stack has the address of the load_info
       * structure passed to the micro-loader.
       */

      status = target_read_memory (   core_rse_stack + 3 * sizeof (CORE_ADDR),
                                    (char*)&load_info_addr,
                                    sizeof (CORE_ADDR));
      if (status != 0)
	{
          warning ("Unable to read the load_info structure address from %s.",
		 bfd_get_filename ((SYMBOL_BFD_SECTION (msymbol))->owner));
	  return;
	}

      status = target_read_memory (load_info_addr, 
				   (char*) &load_info, 
				   sizeof(struct load_info));
      if (status != 0)
	{
	  warning ("Unable to read the load_info structure");
	  return;
	}
    }
  else
    {
      CORE_ADDR 		arg_list_addr;
      int			status;

      if (IS_TARGET_LRE)
	load_info_addr = saved_load_info_addr;
      else
	{

	  status = call_ttrace (TT_PROC_GET_ARGS,
				inferior_pid,
				(TTRACE_ARG_TYPE) &arg_list_addr,
				(TTRACE_ARG_TYPE) sizeof (CORE_ADDR),
				(TTRACE_ARG_TYPE) TT_NIL);
	  if (status == -1 && errno)
	      error ("Unable to find the load_info structure.");

	  status = target_read_memory (arg_list_addr + 3 * sizeof (CORE_ADDR),
					 (char *) &load_info_addr,
					 sizeof (CORE_ADDR));
	  if (status != 0)
	      error ("Unable to read the load_info structure address.");
	}

      target_read_memory (load_info_addr, (char*) &load_info, sizeof(load_info));
      LRE_FLIP_STRUCT (&load_info, &load_info_desc);
      if (sizeof(load_info) != load_info.li_length)
	warning (
	 "load_info structure has unexpected size.  Possible version problem.");
    }
#else /* gambit */
  msymbol = lookup_minimal_symbol("__load_info", NULL, NULL);
  sym_addr = SYMBOL_VALUE_ADDRESS (msymbol);
  status = target_read_memory ( sym_addr,
                                 (char*)&load_info_addr,
                                 sizeof (CORE_ADDR));
  if (status != 0)
    error ("Unable to read the load_info structure address from %s.",
	   bfd_get_filename ((SYMBOL_BFD_SECTION (msymbol))->owner));
  if (is_swizzled)
    { /* We have read in a 32-bit pointer.  Shift as swizzle */
      load_info_addr = load_info_addr >> 32;
      load_info_addr = swizzle (load_info_addr);
    }

  status = target_read_memory (load_info_addr, 
			       (char*) &load_info, 
			       sizeof(struct load_info));
  if (status != 0)
      error ("Unable to read the load_info structure");
  
#endif

  have_read_load_info = 1;

  set_internalvar (lookup_internalvar ("user_sendsig"),
		   value_from_longest (builtin_type_long_long,
				       (LONGEST) (load_info.li_sendsig_txt)));
}

#ifdef HP_IA64_NATIVE
int 
address_in_user_sendsig_unw(CORE_ADDR address)
{
  if ((have_read_load_info) && (load_info.li_sendsig_unw >0) && (address >= load_info.li_sendsig_unw) && (address <= load_info.li_sendsig_unw + 4096))
	return 1;
  else
	return 0;
}
extern uint32_t ia64_read_kernel_word(uint64_t);

/*  ia64_read_local_mem_kernel_pages:
     Purpose:  This function permits a 32 bit (+DD32) application to read
       user_sendsig()'s unwind table information which lies in the
       gateway page of the kernel.  The gateway page is not accessible
       by a 32 bit (un-swizzled) pointer.
     Inputs:   dst -- destination address which must be word aligned
               src -- src address -- must be word aligned.  May be
                      in the kernel gateway pages (e.g. reachable only by
                      a 64 bit address) even in +DD32 applications.
               length -- length in bytes of data to transfer to *dst.
               ident  -- thread identity.
     Limitations:  Don't use this memory reader if you are a +DD64 bit
      application (and +DD32 aps should only use it for user_sendsig's
      unwind information) since it is not optimized for speed.  Note data
      alignment requirements under "Inputs:" above.
*/
void *
ia64_read_local_mem_kernel_pages (void *dst,
                const uint64_t src,
                size_t length)
{
    extern  struct load_info *__load_info;
    unsigned int * to_ptr_s1 =  (uint32_t *) dst;   //  destination pointer
    unsigned long long from_adr_s1 = src;   //  source address
      /* For the case when we are debugging a core file check if we are
	 debugging on a system different from where it was generated */
      if ((target_has_stack && !target_has_execution) && (is_swizzled) && (load_info.li_sendsig_unw != ((struct load_info *)__load_info)->li_sendsig_unw))
	{
	  error("\nAttempting to debug core generated on a different machine.\n\
Unwinding through signal handler will not work.\n\
Try debugging on the machine where this core was generated\n");
	  return 0;
	}
	
    /* Copy words */
    while (length > 0 )    // words still remain
    {
        *to_ptr_s1 = ia64_read_kernel_word(from_adr_s1);
        to_ptr_s1++;
        from_adr_s1 += 4;
        if (length < 4)   /* Account for partial word at end of requested
                             data.  It does not matter to read all four */
            length = 0;   /* bytes of a partial word since page boundaries */
        else              /* never split a word. */
            length -= 4;
    }
    return dst;
} // ia64_read_local_mem_kernel_pages
#endif  /* HP_IA64_NATIVE */


/* Predefined Register numbers */
enum { BR_RP    = 0 };  /* Return Pointer RP -- branch register 0 */

#define SAVE_SP_REGNUM FP_REGNUM

/* Bindu: The error code for uwx are from -19 to 2. See unw_err_msg below. */
#define UNWIND_FAIL(ERR_NUM)       	\
        {                                       \
	  if (stop_whining)			\
	  if ((ERR_NUM < -19) || (ERR_NUM > 2)) \
	    warning("Error %d while unwinding\n", ERR_NUM);	\
	  else					\
            warning ("Error %s while unwinding\n", 	\
		     unw_err_msg[ERR_NUM + 19]);	\
          uc_distance = -1;                       \
        }

/* bindu: To provide more verbose error message for unwind errors. */

char *unw_err_msg[45] = { "UWX_ERR_NOCONTEXT", /* -19 */
			  "UWX_ERR_NOCALLBACKS",
			  "UWX_ERR_CANTUNWIND",
			  "UWX_ERR_BADREGID",
			  "UWX_ERR_UNDEFLABEL",
			  "UWX_ERR_PROLOG_UF",
			  "UWX_ERR_NOMEM",
			  "UWX_ERR_BADUDESC",
			  "UWX_ERR_NOUDESC",
			  "UWX_ERR_NOUENTRY",
			  "UWX_ERR_COPYIN_REG",
			  "UWX_ERR_COPYIN_RSTK",
			  "UWX_ERR_COPYIN_MSTK",
			  "UWX_ERR_COPYIN_UINFO",
			  "UWX_ERR_COPYIN_UTBL",
			  "UWX_ERR_BADKEY",
			  "UWX_ERR_LOOKUPERR",
			  "UWX_ERR_IPNOTFOUND",
			  "UWX_ERR_NOENV",
			  "UWX_OK",  /* 0 */
			  "UWX_BOTTOM", /* 1 */
			  "UWX_ABI_FRAME" /* 2 */
                         };


/*FIXME */
const struct floatformat floatformat_ia64 =
{
  /*byteorder=) */ floatformat_big /*NEED_TO_VERIFY */ ,
				/*totalsize=) */ 96,
				/*Assumption: floating-pt register size ==12 bytes */
		/* i.e. RAW_REGISTER_SIZE(FR0_REGNUM)==12 */
				/*sign_start= */ 14,
				/* Ignore leading 14 bits (0--13) */
 /*exp_start= */ 15,
 /*exp_len= */ 17,
 /*exp_bias= */ 0xFFFF,
 /*exp_nan= */ 0x1FFFF,
 /*man_start= */ 32,
 /*man_len= */ 64,
 /*intbit= */ floatformat_intbit_yes
};

/* In breakpoint.c */
extern int exception_catchpoints_are_fragile;

/* Has the initialize function been run? */
/* used in symfile.c */
int hp_cxx_exception_support_initialized = 0;

/* For the moment, gdb isn't well informed about uld.so.  We use knowledge
   about where it was loaded in text to suppress error messages.  Look
   for uses of uld_text_addr.
   FIXME: This should be ripped out after we can get infomred about uld.sl
   peoperly.  -- Michael Coulter 5/15/00.
   */

CORE_ADDR uld_text_addr;

/* When debugging aries, there is no dld in the picture. This leads to
   numerous warnings about load module descriptor construction failures
   Silently suppress these warnings for now if debugging aries. FIXME
*/
extern int debugging_aries,debugging_aries64;
static void ia64_print_fp_reg PARAMS ((int, enum precision_type));
void ia64_find_dummy_frame_regs_1 (CORE_ADDR, struct frame_saved_regs *);
CORE_ADDR pre_interrupt_bsp (void);
int read_inferior_ideal_memory PARAMS ((CORE_ADDR memaddr,
				    char *myaddr, int len));

typedef enum
  {
    STUB_NONE,
    STUB_SOLIB_CALL,
    STUB_LONG_CALL,
    STUB_BRL
  }
stub_type;

/* The most instructions in a stub */
#define MAX_STUB_BUNDLES		3

/* A NaT collection is written to the RSE BS when the address bits 8:3
   are zero, i.e. the BS address of a register ored with 0x1f8 has the
   nat collection for that register.
   */

#define NAT_COLLECTION_ADDR(bs_addr) ((bs_addr) | 0x1f8)

/* The low 7 bits of the current frame marker is the size of the frame. 
   13-7 is sol and 17-14 is sor. */

#define SIZE_OF_FRAME(cfm)      ((cfm) & 0x7f)
#define SIZE_OF_LOCALS(cfm)     (((cfm) & 0x3f80) >> 7)
#define SIZE_OF_ROTATING(cfm)   (((cfm)>>14)& 0x0f)

#define MAX_PLAUSIBLE_UNWIND 50000

/* Initialize the token (cbinfo) that we pass to unwind express. */
static void
ia64_init_cbinfo ()
{
  if (!cbinfo)
    {
      cbinfo = (struct uwx_info *)malloc (sizeof (struct uwx_info));
      cbinfo->ucontext = 0;
    }
  cbinfo->frame_status = 0;
}

/* Initialize the current_uc. We keep the cbinfo, ucontext and current_uc
   for the rest of the life of gdb. Do not allocate cbinfo, current_uc
   and ucontext on obstack, as obstack goes away everytime a new session
   is started. */
static void
init_uwx_env ()
{
  int status;

  if (!current_uc)
    {
      /* Allocate the current_uc */
      current_uc = uwx_init();
      ia64_init_cbinfo ();

      /* Pass the callback and token info to the uwx. */
      status = uwx_register_callbacks(
                        current_uc,
                        (intptr_t) cbinfo,
                        ia64_uwx_copyin,
                        ia64_uwx_lookupip);
      if (status != UWX_OK)
        error ("Error unwinding with uwx");
    }
  /* Set the endian type. */
  status = uwx_set_remote (current_uc, !IS_TARGET_LRE);
  if (status != UWX_OK)
    error ("Error unwinding with uwx");
  ia64_init_cbinfo ();
}

/* Add 'size' number of 8 byte words from bsp. Account for 
   any nats in between. */
CORE_ADDR
add_to_bsp (CORE_ADDR bsp, long size)
{
  CORE_ADDR nat_addr, ret_val;
  ret_val = bsp + 8 * size;
  nat_addr = NAT_COLLECTION_ADDR (bsp);
  if (size > 0)
    {
      /* Adjust the nats. */
      while (nat_addr <= ret_val)
        {
          nat_addr += 64 * REGISTER_SIZE;
          ret_val += REGISTER_SIZE;
        }
    }
  else
    {
      /* Adjust the nats. */
      nat_addr = nat_addr - 64 * REGISTER_SIZE;
      while (nat_addr >= ret_val)
        {
          nat_addr -= 64 * REGISTER_SIZE;
          ret_val -= REGISTER_SIZE;
        }
    }
  return ret_val;
}

static void
ia64_to_double (char *from, char *to)
{
  floatformat_to_double (&floatformat_ia64, from, (double *)(void *) to);
}

static void
ia64_to_doublest (char *from, char *to)
{
  floatformat_to_doublest (&floatformat_ia64, from, (DOUBLEST *)(void *) to);
}

void
doublest_to_ia64 (char *from, char *to)
{
  floatformat_from_doublest (&floatformat_ia64, (DOUBLEST *)(void *) from, to);
}

/* ia64_convert_to_float80 - from is a 12-byte buffer holding an fpreg.
   The high-order 14-bits are zero, followd by the 82-bits of data.
   "to" is the 16-byte float80 memory value, of which the first 80 bits
   hold the value.

   Convert the bits from a floating point register to a __float80
   value.  From the EAS 2.4, Table 5-1 on page 5-1, a __float80 has 80 bits:
	1 sign bit, 15 bits of exponent and 64 bits of significand.
	The exponent is biased by 16383.
   From figure 5-1 on page 5-2, an __fpreg has 82 bits:
	1 sign bit, 17 bits of exponent and 64 bits of significand.
	The exponent is biased by 65535;
   */

typedef struct {
    uint64_t	unused:			14;
    uint64_t	sign:			1;
    uint64_t	exponent:		17;
    uint64_t	high_significand:	32;
    uint32_t	low_significand_high:	16;
    uint32_t	low_significand_low:	16;
} fpreg_bits_t;


typedef struct {
    uint64_t	sign:			1;
    uint64_t	exponent:		15;
    uint64_t	high_significand:	32;
    uint64_t	low_significand_high:	16;
    uint32_t	low_significand_low:	16;
    uint32_t	unused1:		16;
    uint32_t	unused2;
} float80_bits_t;

void 
ia64_convert_to_float80 (char * from, char * to)
{
  fpreg_bits_t	fpreg_bits;
  float80_bits_t	float80_bits;

  memcpy (&fpreg_bits, from, sizeof(fpreg_bits_t));
  float80_bits.sign = fpreg_bits.sign;
  /* top two bits of exponent truncated */
  float80_bits.exponent = fpreg_bits.exponent ? 
      fpreg_bits.exponent -65535 + 16383 : 0;

  float80_bits.high_significand = fpreg_bits.high_significand;
  float80_bits.low_significand_high = fpreg_bits.low_significand_high;
  float80_bits.low_significand_low = fpreg_bits.low_significand_low;
  float80_bits.unused1 = 0;
  float80_bits.unused2 = 0;

  memcpy (to, &float80_bits, sizeof(float80_bits_t));
} /* end ia64_convert_to_float80 */


/* from points at the contents of an FP register, to is the value contents
   of a floating point variable.  Convert the bits in the register to
   what is needed by the type.
   */

void 
ia64_convert_to_virtual (struct type * type, char * from, char * to)
{
  if (type->code == TYPE_CODE_FLOAT80)
    ia64_convert_to_float80 (from, to);
  else if (type->code == TYPE_CODE_FLOATHPINTEL)
    {
      memset (to, 0, sizeof(fpreg_bits_t)+4);
      memcpy (to+4, from, sizeof(fpreg_bits_t));
    }
  else
    {
      if (TYPE_LENGTH (type) <= 8)
        {
            double val;
            ia64_to_double (from, (char *)&val);
            store_floating (to, TYPE_LENGTH (type), val);
        }
      else
        {
            DOUBLEST val;
            ia64_to_doublest (from, (char *)&val);
            store_floating (to, TYPE_LENGTH (type), val);
        }
    }
} /* end ia64_convert_to_virtual */
/*
   ** Print or concat into buf, the register indicated by regnum, whose raw value 
   ** is in raw_buf.
   ** First convert the raw value into a double.
   ** If buf is non-null:
   **       value as a double is concatenated into buf.
   ** If buf IS null:
   **       Raw hex value is printed followed by value as a double.
   **
 */
void
ia64_printOrStrcat_fp_register (char *buf, int bufLen, int regnum,
			        char *raw_buf, enum precision_type precision)
{
  DOUBLEST d = 0;
  int natval = 0;
  int printIt = !(buf != (char *) NULL && bufLen > 0);

  unsigned char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];

  /* Put it in the buffer.  No conversions are ever necessary.  */
  memcpy (virtual_buffer, raw_buf, REGISTER_RAW_SIZE (regnum));

  floatformat_to_doublest (&floatformat_ia64,
                           (char *) virtual_buffer,
                           &d);
  /* QXCR1000587032: wdb does not show NaT in FP register
     during core dump analysis. Check whether floating point
     register contains NaTVal and print accordingly.
   */ 
  natval = check_natval (&floatformat_ia64,
                          (char *) virtual_buffer);

  if (natval == 1)
    {
      fprintf_filtered (gdb_stdout, "%s", reg_names[regnum]);
      print_spaces_filtered ((int) (8 - strlen (reg_names[regnum])), gdb_stdout);
      fprintf_filtered (gdb_stdout, "%s", "NaTVal");
    }
  else
    {
      if (printIt)
        {				/* Dump the raw bits as well */
          int i;
          fprintf_filtered (gdb_stdout, "%s", reg_names[regnum]);
          print_spaces_filtered ((int) (8 - strlen (reg_names[regnum])),
                                 gdb_stdout);
          fprintf_filtered (gdb_stdout, "0x");
          for (i = 0; i < REGISTER_RAW_SIZE (regnum); i++)
	    {
	      fprintf_filtered (gdb_stdout, "%02x",
                                (int) virtual_buffer[i]);
	      if (((i + 1) % 4) == 0)
	      fprintf_filtered (gdb_stdout, " ");
	    }
          fprintf_filtered (gdb_stdout, "\t");
        }
      else
        fprintf_filtered (gdb_stdout, "%s", " ");

      val_print (builtin_type_long_double, (char *) &d, 
                 0, 0, gdb_stdout, 0, 1, 0,
	         Val_pretty_default);
    }
  return;
}

/*
   ** precision should indicate what should happen if the register is an fp 
   ** register.  
   ** See comment above ia64_printOrStrcat_fp_register().
 */
void
ia64_printOrStrcat_register (char *buf, int bufLen, int regnum, char *raw_buf,
			     enum precision_type precision, char *format)
{
  int i;
  int printIt = !(buf != (char *) NULL && bufLen > 0);

  if (!((regnum >= FR0_REGNUM) && (regnum <= FRLAST_REGNUM)))
    {
      long long reg_val;

      reg_val =
	extract_signed_integer (raw_buf, REGISTER_RAW_SIZE (regnum));

      if (printIt)
	{
	  if (regnum == PC_REGNUM)
	    {
	      print_address_numeric ((CORE_ADDR) reg_val, /*use_local= */ 1,
				     gdb_stdout);
	      return;
	    }
	  if (format == (char *) NULL)
	    {
	      printf_filtered ("%5.5s: %8llx  ",
			       reg_names[regnum], reg_val);
	    }
	  else
	    {
	      printf_filtered (format, reg_names[regnum], reg_val);
	    }
	}
      else
	{
	  if (regnum == PC_REGNUM)
	    {
	      strcat_address_numeric ((CORE_ADDR) reg_val, /*use_local= */ 1,
				      buf, bufLen);
	      return;
	    }
	  if (format == (char *) NULL)
	    strcat_to_buf_with_fmt (buf, bufLen, "%16llx", reg_val);
	  else
	    strcat_to_buf_with_fmt (buf, bufLen, format, reg_val);
	}
    }
  else
    ia64_printOrStrcat_fp_register (buf, bufLen, regnum, raw_buf, precision);

  return;
}

/*
   ** precision should indicate what should happen if the register is an fp register.  
   ** See comment above pa_printOrStrcat_fp_register().
 */
void
ia64_print_register (int regnum, char *raw_buf, 
		     enum precision_type precision, char *format)
{
  ia64_printOrStrcat_register ((char *) NULL, 0, regnum, raw_buf, precision,
			       format);
}

/*
   ** precision should indicate what should happen if the register is an fp 
   ** register.  
   ** See comment above ia64_printOrStrcat_fp_register().
 */
void
ia64_strcat_register (char *buf, int bufLen, int regnum, char *raw_buf,
		      enum precision_type precision)
{
  ia64_printOrStrcat_register (buf, bufLen, regnum, raw_buf, precision,
			       (char *) NULL);
}

/*
   ** precision should indicate what should happen if the register is an fp 
   ** register.  
   ** See comment above ia64_printOrStrcat_fp_register().
 */
static void
ia64_print_fp_reg (int i, enum precision_type precision)
{
  unsigned char raw_buffer[MAX_REGISTER_RAW_SIZE];

  read_relative_register_raw_bytes (i, (char *) raw_buffer);
  ia64_printOrStrcat_fp_register ((char *) NULL, 0, i, 
				  (char *) raw_buffer, precision);
}

/*
   ** precision should indicate what should happen if the register is an fp 
   ** register.  
   ** See comment above ia64_printOrStrcat_fp_register().
 */
void
ia64_strcat_fp_register (char *buf, int bufLen, int regnum,
			 enum precision_type precision)
{
  char raw_buffer[MAX_REGISTER_RAW_SIZE];

  read_relative_register_raw_bytes (regnum, raw_buffer);
  ia64_printOrStrcat_fp_register (buf, bufLen, regnum, raw_buffer, precision);
}


/* Print the register regnum, or all registers if regnum is -1 */

void
ia64_do_registers_info (int regnum, int fpregs)
{
  if (selected_frame && selected_frame->pa_save_state_ptr)
    {
      /* This is a mixed mode PA frame. Have the registers printed using
         mixed_mode_do_registers_info. */
      mixed_mode_do_registers_info (regnum, fpregs);
      return;
    }

  if (regnum == -1)
    {
      /* Print all the registers */
      unsigned char raw_regs[REGISTER_BYTES];
      int i;
      int num_stacked_regs;

      num_stacked_regs = (int) SIZE_OF_FRAME (selected_frame->cfm);

      for (i = PR0_REGNUM; i < NUM_REGS; i++) 
       {
         /* Skip the unreadable AR registers */
         if (   ((i >= AR0_REGNUM )&& (i < AR0_REGNUM + 16)) 
	     || (   (i > AR0_REGNUM + 19) 
		 && (i != AR0_REGNUM + 32) 
		 && (i != AR0_REGNUM + 36) 
		 && (i != AR0_REGNUM + 40) 
		 && (i != AR0_REGNUM + 64) 
		 && (i != AR0_REGNUM + 65) 
		 && (i != AR0_REGNUM + 66) 
		 && (i <= ARLAST_REGNUM)))
           continue;

	 /* Skip __reason. */
	 if (i == REASON_REGNUM)
	   continue; 

            if (!((i>= GR0_REGNUM) && (i <= GRLAST_REGNUM)))
              {
		/* ia64_print_register doesnot print ip: */
	        if (i == PC_REGNUM) 
		  printf_filtered("%5.5s: ", reg_names[PC_REGNUM]);

      		if (i == AR0_REGNUM + 17)
		  (* ((CORE_ADDR *)(void *) (raw_regs + REGISTER_BYTE (i)))) = 
	            pre_interrupt_bsp();
		else
	          read_relative_register_raw_bytes 
			 (i, (char *) (raw_regs + REGISTER_BYTE (i)));
                ia64_print_register (i, (char *) (raw_regs + REGISTER_BYTE(i)),
			 unspecified_precision, "%5.5s: %18#llx  ");

		/* Stacey 6/10/2002
 		   print a new line after printing each register.  This works
		   for all guis and the command line interface, with the 1
		   exception: the wdb gui (operates at annotation level 2) 
		   expects no newline after the CFM and PFS registers.
  		        So print new lines unless we are printing either the
		   CFM or PFS registers at annotation level 2.  The new line
		   for CFM & PFS will be printed after a special display of
		   their contents below.  */

		if ( ( (annotation_level == 2 ) 
		       && !( (i == CFM_REGNUM)
			     || (i == AR0_REGNUM + 64)))
		     || (annotation_level != 2))
		  printf_filtered ("\n");

		/* Stacey 6/10/2002
		   CFM & PFS registers are printed twice:
		   first as raw values like the other registers above
		   and below in special formats.  */
		if ( (i == CFM_REGNUM) || (i == AR0_REGNUM + 64))
		  {
		    char *raw_addr = (char *) raw_regs + REGISTER_BYTE (i);
		    int len = REGISTER_RAW_SIZE (i);

		    CORE_ADDR cfm =
		      (CORE_ADDR) extract_unsigned_integer (raw_addr, len);
		    printf_filtered ("       (sor:%lld, sol:%lld, sof:%lld)\n",
				     SIZE_OF_ROTATING(cfm), SIZE_OF_LOCALS(cfm),
				     SIZE_OF_FRAME(cfm));		    
		  }
	      }
	    else
	      {
                char nat_buf[MAX_REGISTER_RAW_SIZE];
                long long reg_val;

		/* Print "NaT" instead of the value in a GR if the NaT bit s set
                   for that register */
		if(i >= (GR0_REGNUM + NUM_ST_GRS + num_stacked_regs))
		  continue;
	 	read_relative_register_raw_bytes 
			(i, (char *) (raw_regs + REGISTER_BYTE (i)));

	 	read_relative_register_raw_bytes (i - GR0_REGNUM + NR0_REGNUM, 
                                                  (char *) (nat_buf));
                reg_val =
                  extract_signed_integer (nat_buf, 
                                          REGISTER_RAW_SIZE (i - GR0_REGNUM + 
                                                             NR0_REGNUM));
                if (reg_val == 1)
                  printf_filtered ("%5.5s: %18s", reg_names[i], "NaT");
                else
                  ia64_print_register (i, (char *) (raw_regs + 
                                                    REGISTER_BYTE(i)),
                                       unspecified_precision, 
                                       "%5.5s: %18#llx  ");
                printf_filtered ("\n");
	      }
	  }
	for (i = FR0_REGNUM; i <= FRLAST_REGNUM; i++)
    	  {                           
          /* A floating point register */
	    if (fpregs) 
	      {
                ia64_print_fp_reg (i, double_precision);
                printf_filtered ("\n");
	      }
          }  
  }

  else if (!((regnum >= FR0_REGNUM) && (regnum <= FRLAST_REGNUM)))
    {				/* NOT a floating point register */
      char raw_buf[MAX_REGISTER_RAW_SIZE];

      if (regnum == AR0_REGNUM + 17)
	(* ((CORE_ADDR *)(void *) (raw_buf ))) = 
	  pre_interrupt_bsp();
      else
        read_relative_register_raw_bytes (regnum, (char *) raw_buf);

      /* For general registers print the value of the NaT bit */
      if (regnum >= GR0_REGNUM && regnum <= GRLAST_REGNUM)
        {
          char nat_buf[MAX_REGISTER_RAW_SIZE];
          long long reg_val;
          read_relative_register_raw_bytes (regnum - GR0_REGNUM + NR0_REGNUM, 
                                            (char *) nat_buf);
          reg_val =
            extract_signed_integer (nat_buf, 
                                    REGISTER_RAW_SIZE (regnum - GR0_REGNUM + 
                                                       NR0_REGNUM));
          if (reg_val == 1)
            printf_filtered ("%7.7s: %18s", reg_names[regnum], "NaT");
          else
            ia64_print_register (regnum, raw_buf, unspecified_precision, 
                                 "%7.7s: %18#llx  ");
        }      
      else
        {
          ia64_print_register (regnum, raw_buf, unspecified_precision, 
                               "%7.7s: %18#llx  ");
        }

      /* Stacey 6/10/2002
	 Print a new line after printing each register.  This works
	 for all guis and the command line interface, with 1 exception:
	 the wdb gui (annotation level 2) expects no newline after the
	 CFM and PFS registers.
	 	 So print new lines unless we are printing either the
	 CFM or PFS registers at annotation level 2.  The new line
	 for CFM & PFS will be printed after a special display of
	 their contents below.  */
      if ( ( (annotation_level == 2 ) 
	     && !( (regnum == CFM_REGNUM) || (regnum == AR0_REGNUM + 64)))
	   || (annotation_level != 2))
	printf_filtered ("\n");

      /* Stacey 6/10/2002
	 CFM & PFS registers are printed twice:
	 first as raw values like the other registers above
	 and below in special formats.  */
      if ( (regnum == CFM_REGNUM) || (regnum == AR0_REGNUM + 64))
	{
	  CORE_ADDR cfm =
	    (CORE_ADDR) extract_unsigned_integer (raw_buf, 
						  REGISTER_RAW_SIZE (regnum));

	  printf_filtered ("        (sor:%lld, sol:%lld, sof:%lld)\n",
			   SIZE_OF_ROTATING(cfm), SIZE_OF_LOCALS(cfm),
			   SIZE_OF_FRAME(cfm));		    
	}      
    }
  else
    {				/* A floating point register */
      ia64_print_fp_reg (regnum, double_precision);
      printf_filtered ("\n");
    }
}

CORE_ADDR
target_read_pc (int pid)
{
  /* NOTE: ia64-gambit -  Internal PC value is slot-encoded */
  return read_register (PC_REGNUM);
}

void
target_write_pc (CORE_ADDR v, int pid)
{
  /* NOTE: ia64-gambit -  Internal PC value is slot-encoded */
  write_register (PC_REGNUM, (LONGEST) v);
}

CORE_ADDR
saved_pc_after_call (struct frame_info *frame)
{
  CORE_ADDR return_pc;

  if (mixed_mode_pa_unwind && frame->pa_save_state_ptr)
    {
      return hppa_saved_pc_after_call (frame);
    }

  return_pc = frame_saved_pc (frame);

  /* If for some reason, we couldn't find the return_pc,
     get rp from br0 */
  if (return_pc == 0)
    read_relative_register_raw_bytes_for_frame (BR0_REGNUM,
                                                (char *) &return_pc,
                                                frame);
  return return_pc;
}

/*
 * DUMMY: pa_is_64bit()
 *   Answer whether the architecture is 64 bit or not
 */
int
pa_is_64bit ()
{
  /* warning ("pa_is_64bit() should not be called for IA64.\n"); */

  return (1 == 1);		/* True */
}

/*
 */
CORE_ADDR
ia64_push_arguments (int nargs, value_ptr *args, CORE_ADDR sp,
		     int struct_return, CORE_ADDR struct_addr)
{
  /* array of arguments' offsets */
  int *offset = (int *) alloca (nargs * sizeof (CORE_ADDR));
  /* array of arguments' lengths: real lengths in bytes, not aligned
     to word size */
  int *lengths = (int *) alloca (nargs * sizeof (CORE_ADDR));

  int bytes_reserved;		/* this is the number of bytes on the stack
				   occupied by an argument. This will be always
				   a multiple of 4 */

  int cum_bytes_reserved = 0;	/* this is the total number of bytes
				   reserved by the args seen so far. It
				   is a multiple of 4 always */
  int cum_bytes_aligned = 0;	/* same as above, but aligned on 8 bytes */
  int i, j, k;
  int num_items;
  struct type *base_type;
  value_ptr val;

  for (i = 0; i < nargs; i++)
    {

      lengths[i] = TYPE_LENGTH (VALUE_TYPE (args[i]));

      if (lengths[i] % sizeof (CORE_ADDR))
	bytes_reserved = (int) ((lengths[i] / sizeof (CORE_ADDR)) * sizeof (CORE_ADDR)
	  + sizeof (CORE_ADDR));
      else
	bytes_reserved = lengths[i];

      /* RM: integers and floats less than 8 bytes are padded to the left,
       * aggregates are padded on the right
       */
      /* RM: ??? check double-extended-precision args */
      if ((lengths[i] < 8) && (bytes_reserved > lengths[i]) && !IS_TARGET_LRE &&
	  ((is_integral_type (VALUE_TYPE (args[i]))) ||
	   (is_float_type (VALUE_TYPE (args[i]))) ||
	   (is_pointer_type (VALUE_TYPE (args[i])))))
	offset[i] = cum_bytes_reserved + bytes_reserved - lengths[i];
      else
	offset[i] = cum_bytes_reserved;

      cum_bytes_reserved += bytes_reserved;
    }

  /* RM: now move up the sp to reserve space required for the
   * args. Reserve at least 64 bytes, since we later blindly copy
   * over 64 bytes to registers and decrement the stack pointer by
   * 64. */
  /* we also need to keep the sp aligned to 16 bytes */
  cum_bytes_aligned = (cum_bytes_reserved % 16) ? ((cum_bytes_reserved + 15) & -16) : cum_bytes_reserved;
  sp -= max (64, cum_bytes_aligned);

  /* QXCR1000998213: IPF:DD64 wrong results returned when calling function
     directly from gdb. Initialising the stack with null.*/
  char *init_sp; 
  init_sp = xcalloc(max(64, cum_bytes_aligned), sizeof(char));
  write_memory (sp ,init_sp ,max(64, cum_bytes_aligned));
  make_cleanup (free, init_sp);

  /* now write each of the args at the proper offset down the stack */
  for (i = 0; i < nargs; i++)
    /* RM: args 0 through 7 are passed in the out registers. We write
       them out to memory here and will later copy them to the
       registers */
    write_memory (sp + offset[i], VALUE_CONTENTS (args[i]), lengths[i]);

  /* RM: write out floating point parameters to the fp registers too */
  for (i = 0, j = 8; (i < nargs) && (j < 16); i++)
    {
      if (offset[i] > 64)
	break;

      base_type = NULL;
      if (is_floats_only_type (VALUE_TYPE (args[i]), &base_type, &num_items))
	{
	  char buf[12];

	  val = value_zero (base_type, not_lval);
	  for (k = 0; (k < num_items) && (j < 16); k++)
	    {
	      if (offset[i] + (k + 1) * TYPE_LENGTH (base_type) > 64)
		break;
	      memcpy (VALUE_CONTENTS_RAW (val),
		      VALUE_CONTENTS (args[i]) + k * TYPE_LENGTH (base_type),
		      TYPE_LENGTH (base_type));

	      /* value_from_double really takes a DOBLEST as the third arg.
		 Similarly value_as_double really returns DOUBLEST.
	       */
	      doublest_to_ia64 (VALUE_CONTENTS (value_from_double (builtin_type_long_double,
						    value_as_double (val))),
			      buf);
	      write_register_gen (FR0_REGNUM + j, buf);
	      j++;
	    }
	}
    }

  /* if a structure has to be returned, set up register r8 to hold its
     address */
  if (struct_return)
    write_register (GR0_REGNUM + 8, struct_addr);

  /* RM: copy the register parameters to registers. There may not be
   * enough "out" registers in the current frame to directly put the
   * parameters them. We'll put them in r10-r17. The call dummy will
   * alloc a new frame and copy them over to its out registers.
   */

  for (i = 0; i < 8; i++) {
   /* RM: is this argument slot used? */
   if ((REGISTER_SIZE * (i + 1)) <= cum_bytes_reserved)
    {
      /* This code checks and decides how to pass aggregate parameters
	 . In c by default aggregates are passed to calle by copying 
 	 the elements of aggregates into registers.
	 In C++ this done when there is no dtor/ctor present.
	 If ctor/dtor is present then address of an aggregate is passed
	 through register to calle. For more details Ps see CR1000921083.
      */
#ifdef HP_IA64
	 if (i < nargs)
          {
            struct type *arg_type = VALUE_TYPE (args[i]);
	    if (((TYPE_CODE (arg_type) == TYPE_CODE_STRUCT) ||
               (TYPE_CODE (arg_type) == TYPE_CODE_CLASS) ||
	       (TYPE_CODE (arg_type) == TYPE_CODE_UNION))
                && (arg_type->pass_by_addr == 1))
 	   
	     write_register (GR0_REGNUM + 21 + i, sp + offset[i]);
	    else
    	     write_register (GR0_REGNUM + 21 + i,
                        read_memory_integer (sp + REGISTER_SIZE * i,
                                           REGISTER_SIZE));	
          } 
        else
#endif 
           write_register (GR0_REGNUM + 21 + i,
                        read_memory_integer (sp + REGISTER_SIZE * i,
                                           REGISTER_SIZE));
    }
    write_register (NR0_REGNUM + 21 + i, 0);
  }
 
  sp += 64;
  /* the stack will have 2 more words on top of the args */
  return sp - 2 * sizeof (CORE_ADDR);
}

/* Return a value (typically a struct) which was placed at address, addr
   (typically on the stack).  Called from call_function_by_hand.
   */

value_ptr
ia64_value_returned_from_stack (struct type *valtype, CORE_ADDR addr)
{
  value_ptr val;

  val = allocate_value (valtype);
  CHECK_TYPEDEF (valtype);
  target_read_memory(addr, VALUE_CONTENTS_RAW (val), TYPE_LENGTH (valtype));

  return val;
} /* end ia64_value_returned_from_stack */


/* Bindu 05/18/01: fill in the floating point register values.
   Bindu 030502: fill in gr0-31 and br0-7.
   FIXME!! need to get right nats 0-31 as well.
   Add more registers if gdb needs to get the values of the registers
   in the upper frames from unwind. */
void 
fill_regs_from_uc (struct uwx_env *myuc, struct frame_info *frame)
{
  int frame_regno;
  int regnum;
  int result;
  CORE_ADDR reg_val;
  /* __uc_get_grs () expects unsigned int */
  unsigned int nat_val = 0;
  /* nat value from uwx_get_nat */
  int uwx_nat_val = 0;
  unsigned int nats = 0;
  CORE_ADDR fr_val[2];
  char* regs = frame->registers;

  /* Nothing to do, just return */
  if (frame->regs_filled)
    return;

  /* Get the register from unwind_context. If unwind library returns an
     error, set the register to 0. */
  for (regnum = FR0_REGNUM; regnum <= FRLAST_REGNUM; regnum++)
    {
      result = uwx_get_reg (myuc, UWX_REG_FR(regnum - FR0_REGNUM), 
			    (UWX_ARG_ADDR64) fr_val);
      if (result != UWX_OK)
        memset (&regs[REGISTER_BYTE (regnum)],
                0, REGISTER_RAW_SIZE (regnum));
      else
        memcpy (&regs[REGISTER_BYTE (regnum)],
                ((char*) fr_val)+ 4, REGISTER_RAW_SIZE (regnum));
    }

  /* fill general registers 0-31 */
  for (regnum = 1; regnum <= 31; regnum++)
    {
      result = uwx_get_reg(myuc, UWX_REG_GR(regnum), 
			   (UWX_ARG_ADDR64) &reg_val);
      if (result != UWX_OK)
        memset (&regs[REGISTER_BYTE (FRLAST_REGNUM + regnum + 1)],
                0, REGISTER_RAW_SIZE (GR0_REGNUM + regnum));
      else
        memcpy (&regs[REGISTER_BYTE (FRLAST_REGNUM + regnum + 1)],
  	        &reg_val, REGISTER_RAW_SIZE (GR0_REGNUM + regnum));
      /* Read in the nats as well and collate all 32 bits. */
      result = uwx_get_nat (myuc, UWX_REG_GR(regnum), &uwx_nat_val);
      if (result != UWX_OK)
        uwx_nat_val = 0;
      nats = nats | (uwx_nat_val << regnum);
    
      /* Get the NATS for gr1-gr31 from ucontext for only the frame 
        which receives a signal. Refer :QXCR1000854247 for more
        details.
      */
      if (frame->next->signal_handler_caller == 1)
        {
          __uc_get_grs (cbinfo->ucontext, regnum, 1, 
           (UWX_ARG_ADDR64) &reg_val, &nat_val);
          nats = nats | nat_val;
        }
}
  /* Copy the nats to the last 32 bits of regs array. 
     Storing NATS starting at regs[1912] and regs size is [1944].
     (FRLAST_REGNUM + 1)+ NUM_ST_GRS + NUM_BRS + 7) is to store at
     1912th position.
  */
  memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                               + NUM_ST_GRS + NUM_BRS + 7)],
	  &nats, sizeof (nats));
  /* Fill BRs. */
  for (regnum = 0; regnum <= 7; regnum++)
    {
      result = uwx_get_reg(myuc, UWX_REG_BR(regnum), 
			   (UWX_ARG_ADDR64) &reg_val);
      if (result != UWX_OK)
        memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1) 
					+ NUM_ST_GRS + regnum)],
                0, REGISTER_RAW_SIZE (BR0_REGNUM + regnum));
      else
        memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
					+ NUM_ST_GRS + regnum)],
                &reg_val, REGISTER_RAW_SIZE (BR0_REGNUM + regnum));
    }

  /* PFS (ar64) */
  result = uwx_get_reg(myuc, UWX_REG_PFS, (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS)],
 	    0, REGISTER_RAW_SIZE (AR0_REGNUM + 64));
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS)],
	    &reg_val, REGISTER_RAW_SIZE (AR0_REGNUM + 64));

  /* BSPSTORE (ar18) */
  result = uwx_get_reg(myuc, UWX_REG_BSPSTORE, (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 1)],
 	    0, REGISTER_RAW_SIZE (AR0_REGNUM + 18));
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 1)],
	    &reg_val, REGISTER_RAW_SIZE (AR0_REGNUM + 18));

  /* RNAT (ar19) */
  result = uwx_get_reg(myuc, UWX_REG_RNAT, (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 2)],
 	    0, REGISTER_RAW_SIZE (AR0_REGNUM + 19));
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 2)],
	    &reg_val, REGISTER_RAW_SIZE (AR0_REGNUM + 19));

  /* UNAT (ar36) */
  result = uwx_get_reg(myuc, UWX_REG_UNAT, (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 3)],
 	    0, REGISTER_RAW_SIZE (AR0_REGNUM + 36));
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 3)],
	    &reg_val, REGISTER_RAW_SIZE (AR0_REGNUM + 36));

  /* FPSR (ar40) */
  result = uwx_get_reg(myuc, UWX_REG_FPSR, (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 4)],
 	    0, REGISTER_RAW_SIZE (AR0_REGNUM + 40));
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 4)],
	    &reg_val, REGISTER_RAW_SIZE (AR0_REGNUM + 40));

  /* LC (ar65) */
  result = uwx_get_reg(myuc, UWX_REG_LC, (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 5)],
 	    0, REGISTER_RAW_SIZE (AR0_REGNUM + 65));
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 5)],
	    &reg_val, REGISTER_RAW_SIZE (AR0_REGNUM + 65));

  /* Predicates. */
  result = uwx_get_reg (myuc, UWX_REG_PREDS , (UWX_ARG_ADDR64) &reg_val);
  if (result != UWX_OK)
    memset (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 6)],
 	    0, REGISTER_SIZE);
  else
    memcpy (&regs[REGISTER_BYTE ((FRLAST_REGNUM + 1)
                                 + NUM_ST_GRS + NUM_BRS + 6)],
	    &reg_val, REGISTER_SIZE);

  if (IS_TARGET_LRE)
    { /* At this point, everything in the register buffer is in big-endian
	 because that is what we get from unwind.  Some values need
	 to be flipped.
       */

      for (frame_regno = 0;  frame_regno < NBR_FRAME_REGS;  frame_regno++)
      {
	regnum = frame_regno_to_regno (frame_regno);
	if (IS_REG_VAL_FLIPPED (regnum))
	  {
	    endian_flip (&regs[REGISTER_BYTE (frame_regno)], 
			 REGISTER_RAW_SIZE (frame_regno));
          }
      }
    }

  /* Mark the registers as filled */
  frame->regs_filled = TRUE;
} /* end fill_regs_from_uc */

void *
read_tgt_mem (void *dst, const UInt64 src, size_t length, uint32_t ident)
{
  target_read_memory (src, dst, (int) length);
  return dst;
}

struct proc_data {
  unsigned int unwind_hdr_size, unwind_size, unwind_info_size;
};


/* I don't know if we are guaranteed that all the unwind sections in an
 * executable will be contiguous and consecutive, so I use the vma to
 * determine the overall size of the buffer and a section's offset into
 * the buffer.
 *
 * Previously the unwind_hdr was always loaded at the beginning of the
 * buffer.  If any part of gdb is dependent on that then this code
 * assumes the unwind_hdr will have the lowest vma and will therefore be
 * at the beginning of the buffer. */

struct map_unwind_data {
    long long lower;
    long long upper;
    int is_hdr_present;
    char *buf;
};

static const char IA64_UNWIND_PREFIX[]      = ".IA_64.unwind";
static const char IA64_UNWIND_INFO_PREFIX[] = ".IA_64.unwind_info";
static const char GNU_UNWIND_PREFIX[]       = ".gnu.linkonce.ia64unw.";
static const char GNU_UNWIND_INFO_PREFIX[]  = ".gnu.linkonce.ia64unwi.";

void
get_unwind_size(bfd *abfd, asection *sec, PTR user_data)
{
    struct map_unwind_data *data = (struct map_unwind_data *)user_data;
    const char *name = bfd_get_section_name(abfd, sec);
    long long size;
    CORE_ADDR lower, upper;
    int is_hdr_present;

    is_hdr_present = (strcmp(".IA_64.unwind_hdr", name) == 0);
    if (is_hdr_present)
      data->is_hdr_present = 1;

    if (    is_hdr_present 
	||  strncmp(IA64_UNWIND_PREFIX,
		    name, sizeof(IA64_UNWIND_PREFIX)-1) == 0
	||  strncmp(IA64_UNWIND_INFO_PREFIX,
		    name, sizeof(IA64_UNWIND_INFO_PREFIX)-1) == 0
	||  strncmp(GNU_UNWIND_PREFIX,
		    name, sizeof(GNU_UNWIND_PREFIX)-1) == 0
	||  strncmp(GNU_UNWIND_INFO_PREFIX,
		    name, sizeof(GNU_UNWIND_INFO_PREFIX)-1) == 0)
    {
         size  = bfd_section_size(abfd, sec);
         lower = bfd_section_vma(abfd, sec);
         upper = lower + size;
         if (lower < data->lower) data->lower = lower;
         if (upper > data->upper) data->upper = upper;
    }
}

void
read_unwind_data(bfd *abfd, asection *sec, PTR user_data)
{
    struct map_unwind_data *data = (struct map_unwind_data *)user_data;
    const char *name = bfd_get_section_name(abfd, sec);
    long long size, offset;
    if (strcmp(".IA_64.unwind_hdr", name) == 0
    ||  strncmp(IA64_UNWIND_PREFIX,
                name, sizeof(IA64_UNWIND_PREFIX)-1) == 0
    ||  strncmp(IA64_UNWIND_INFO_PREFIX,
                name, sizeof(IA64_UNWIND_INFO_PREFIX)-1) == 0
    ||  strncmp(GNU_UNWIND_PREFIX,
                name, sizeof(GNU_UNWIND_PREFIX)-1) == 0
    ||  strncmp(GNU_UNWIND_INFO_PREFIX,
                name, sizeof(GNU_UNWIND_INFO_PREFIX)-1) == 0)
    {
         size  = bfd_section_size(abfd, sec);
         offset = bfd_section_vma(abfd, sec) - data->lower;
         bfd_get_section_contents (abfd, sec, &data->buf[offset], 0, size);
    }
}

static void
read_unwind_sections (struct objfile *objfile)
{
  struct map_unwind_data data = { (long long) -1, 0, 0, 0};
  struct obj_unwind_info *ui;
  char *buf;
  int i, index;
  CORE_ADDR *unw_ptr;
  struct load_module_desc* lmdp;
  CORE_ADDR *limit;

  ui = (struct obj_unwind_info *)(void *)
    obstack_alloc (&objfile->psymbol_obstack,
		   sizeof (struct obj_unwind_info));

  memset (ui, 0, sizeof (struct obj_unwind_info));
  ((obj_private_data_t *) objfile->obj_private)->unwind_info = ui;

  bfd_map_over_sections(objfile->obfd,
                        get_unwind_size,
                        (PTR)&data);

  /* The thing at offset 0 in buf should be the unwind header 
     However a user may not have an unwind_hdr section in their .o
     Think hand coded assembly files. 
     If the offset is 0 and the section is not unwind header don't 
     read stuff in since it looks like we may have corrupt unwind info.
     */
  if (   (data.is_hdr_present == 0 && ! IS_TARGET_LRE)
      || (IS_TARGET_LRE && data.upper == 0))
    {
      if (!batch_rtc)
         warning ("No unwind information found.\n Skipping this library %s.\n", objfile->name);
      ui->table = NULL;
      ui->entries = NULL;
      return;
    }

  if (IS_TARGET_LRE)
    {
      /* Linux does not have a .IA_64_unwind_hdr, but we want to alloate
         space for one which we will synthesize in buf.
       */
      data.lower = data.lower - sizeof(struct unwind_header);
    }
  
  buf = obstack_alloc (&objfile->psymbol_obstack, data.upper - data.lower); 
  data.buf = buf;
  ui->table = buf;
  ui->table_size = data.upper - data.lower;
  ui->entries = NULL;

  bfd_map_over_sections(objfile->obfd,
                        read_unwind_data,
                        (PTR)&data);

  if (IS_TARGET_LRE)
    {
      /* Flip the unwind stuff. Fortunatly, everything is a 64-bit word. */

      lmdp = ((obj_private_data_t *) objfile->obj_private)->lmdp;
      limit = (CORE_ADDR *)(void *) (data.buf + data.upper - data.lower);
      unw_ptr = (CORE_ADDR *)(void *) ((char*) limit - lmdp->unwind_size);
      while (unw_ptr < limit)
	endian_flip (unw_ptr++, sizeof (CORE_ADDR));


      /* Synthesize an unwind header */

      unw_ptr = (CORE_ADDR *)(void *) data.buf;
      *(unw_ptr++) = LRE_UNW_HDR_VERSION;  /* version number, dummy */
      *(unw_ptr++) = lmdp->unwind_base;
      *(unw_ptr++) = lmdp->unwind_base + lmdp->unwind_size;
    }
}

struct unwind_header *
read_unwind_header (struct load_module_desc *lmdp, int ident)
{
  obj_private_data_t *obj_private;
  struct objfile *objfile;

  ALL_OBJFILES (objfile)
  {
    if (objfile->is_mixed_mode_pa_lib || objfile_is_mixed_mode (objfile))
      {
        continue;
      }

    /* Skip the separate debug object , look for the actual one. */
    if (objfile->separate_debug_objfile_backlink)
      continue;

    if (!objfile->obj_private ||
	(lmdp != ((obj_private_data_t *) objfile->obj_private)->lmdp))
      continue;

    if (!(((obj_private_data_t *) objfile->obj_private)->unwind_info))
      {
	read_unwind_sections (objfile);
      }

    return
      (struct unwind_header *)
      ((obj_private_data_t *) objfile->obj_private)->unwind_info->table;
  }

  return NULL;
}

struct unwind_table64
{
  CORE_ADDR start;
  CORE_ADDR end;
  long long info_ptr;
};

struct unwind_table32
{
  UInt32 start;
  UInt32 end;
  UInt32 info_ptr;
};

static int
read_unsigned_leb128 (char *buf, int *bytes_read)
{
  unsigned int result;
  int shift;

  result = 0;
  shift = 0;
  *bytes_read = 0;
  while (1)
    {
      (*bytes_read)++;
      result |= (((*buf) & 127) << shift);
      if (((*buf) & 128) == 0)
	{
	  break;
	}
      shift += 7;
      buf++;
    }
  return result;
}

/* This function takes gdb's internal objfile representations, extracts
   the IA64 unwind segment,
   
   __________________________________________________
   |unwind header| unwind table | unwind info blocks|
   --------------------------------------------------

   and creates a gdb specific copy of the unwind table which is then
   stored in gdb's objfile data structure.  

   In the past, this was done mainly so that we could calculate and store
   prologue end addresses and simplify some code in other functions: i.e.
   by taking advantage of the similarity between 32 and 64 bit unwind
   segments & using 1 kind of data structure to store 32 & 64 bit unwind
   info, we could write code to deal with 64 bits only, etc.  Having
   our own copy of the unwind tables also allowed us to improve gdb's
   performance by adding caching.  
   
   Now however, the old bug - causing incorrect prologue info
   to be reported by the unwind library - has been fixed.  Therefore,
   we don't really need our own copy of the unwind info.  In fact it's
   not used.  The unwind library reads unwind info directly from the
   child process by calling read_tgt_mem.  FIXME: So we would probably
   get a bigger performance improvement by just eliminating this
   function completely.  

   But until then, ... the remaining stuff that makes this code complicated:
   1.  The entire unwind segment (including the tables) contains
       pointers expressed as offsets from the debuggee text segment
       which must be changed to absolute addresses.  This is
       simple until we deal with self-referencing offsets.  
*/

static void
internalize_unwinds (struct objfile *objfile)
{
  CORE_ADDR unwind_base = 0, text_base;
  struct obj_unwind_info *ui = NULL;
  struct unwind_header *uh = NULL;
  unsigned char * region_header;
  unsigned char *unwind_buf = NULL;
  struct unwind_table32 *unwind_table32;
  struct unwind_table64 *vunwind_table64;
  long long n_unwind_entries;
  long long i;
  int prologue_size;  /* length of the prologue in instruction slots */
  boolean prologue_found = false;
  int j, k, n, t, r;
  CORE_ADDR u_offset;
  boolean is_unwind_32;
  unsigned char* table_displacement;

  ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;

  /* Get the starting addresses of the child process' unwind & text
     segments.  We later use these to translate the pointer-offsets
     (in gdb's new unwind copy) to absolute addresses.

     If the program has already been run and is not archived bound, then
     we can just get the text_base and unwind_base from the load module
     descriptors.  However archive bound modules and things like core
     files don't have load module descriptor info.  For these we'll
     go through the elf file to calculate the text and unwind base. */

  if ((!objfile->is_archive_bound) &&
      (objfile->obj_private) &&
      ((obj_private_data_t *) objfile->obj_private)->lmdp)
    {
      text_base = ((struct load_module_desc *)
	    ((obj_private_data_t *) objfile->obj_private)->lmdp)->text_base;
      unwind_base = ((struct load_module_desc *)
	  ((obj_private_data_t *) objfile->obj_private)->lmdp)->unwind_base;
      if ((unwind_base == (CORE_ADDR)(long) -1) || 
	  ((is_swizzled) && ((int) unwind_base == -1 )))
	{
	  ui->entries = NULL;
	  ui->cache = NULL;
	  ui->last = 0 - 1;
	  return;  /* nothing to do, no unwind entries */
	}
    }
  else
    {
      asection *unwind_sec;

      unwind_sec = bfd_get_section_by_name (objfile->obfd, ".IA_64.unwind_hdr");
      if (unwind_sec)
	{
	  if ( bfd_section_size (objfile->obfd, unwind_sec) == 0)
	    {
	      ui->entries = NULL;
	      ui->cache = NULL;
	      ui->last = 0 - 1;
	      return;  /* nothing to do, no unwind entries */
	    }
	  unwind_base = unwind_sec->vma +
	    ANOFFSET (objfile->section_offsets, 0);
	}
      else
	warning ("Unwind base not found");

      text_base = elf_text_start_vma (objfile->obfd) +
	ANOFFSET (objfile->section_offsets, 0);
    }

  /* Get ready to copy the unwind entries by:     
     1.  calculating # of entries in the real unwind table
     2.  allocating obstack space for that # of 64 bit entries in
         gdb's unwind table (remember - even 32 bit entries are
	 stored in gdb's 64 bit data structures) 

     Note:
     unwind_buf & ui->table now point to the start of our copy of
     the child's unwind header/segment.  */
        
  unwind_buf = ui->table;
  uh = (struct unwind_header *)(void *) unwind_buf;
  if (!uh)
    return; /* Probably no unwind header */
  is_unwind_32 = (uh->unwind_header_version & 0x8000000000000000LL) != 0;

  unwind_table32 = 0; /* initialize for compiler warning */
  vunwind_table64 = 0; /* initialize for compiler warning */
  if (is_unwind_32)
    {
      unwind_table32 =
	(struct unwind_table32 *)(void *) (unwind_buf + sizeof (*uh));
      n_unwind_entries =
	(uh->unwind_end - uh->unwind_start) / sizeof (struct unwind_table32);
    }
  else
    {
      if (!IS_TARGET_LRE)
	{ /* For HP-UX, the unwind table comes first after the header */
	  vunwind_table64 =
	    (struct unwind_table64 *)(void *) (unwind_buf + sizeof (*uh));
        }
      else
	{
	  /* For LRE, the unwind table is at the end of the buffer */

	  vunwind_table64 = (struct unwind_table64 *)(void *) 
	    (unwind_buf + ui->table_size - (uh->unwind_end - uh->unwind_start));
	}
      n_unwind_entries =
	(uh->unwind_end - uh->unwind_start) / sizeof (struct unwind_table64);
    }

  ui->entries =
    (struct unwind_table_entry *)(void *)
    obstack_alloc (&objfile->psymbol_obstack,
		   n_unwind_entries * sizeof (struct unwind_table_entry));
  ui->cache = NULL;
  ui->last = (int) (n_unwind_entries - 1);

  /*  The loop below takes unwind entries from the real unwind table,
      converts them to absolute addresses, copies them to gdb's
      table, and then calculates the prologue size from the unwind
      descriptors.  

      Unwind region start and end pc's are expressed as offsets from
      the debuggee text segment.  So they can be made into absolute
      addresses like this:
      
      absolute address = pointer + text_base (child's text segment)

      This also makes pointer-offsets to info blocks into absolute
      addresses of those blocks in the child process.  We do some more
      work to make them point instead to the info blocks in gdb's copy
      of the unwind table.  The only address we have that describes
      child's unwind info relative to gdb's address space is the address
      of the unwind segment.  So ...

      absolute address (in debugger space) = 
        unwind segment in debugger space
        + pointer offset (from unwind segment)
      
      We can get the offset as follows:
      pointer offset (from unwind segment) =
        absolute address - unwind_base (unwind segment in debuggee space)

      In the end, it looks like this:
       absolute address in debugger's address space =
         unwind_header_address (w.r.t. debugger)
         + pointer (segment relative to debuggee text base)
	 + text_base (rel to debuggee)
	 - unwind_base (rel to debuggee)
	 
      For details, see Appendix A of the IA64 Stack Unwind Library for
      HP-UX documentation.  */

  if (!IS_TARGET_LRE)
    table_displacement =   (unsigned char*) unwind_buf 
			 + (text_base - unwind_base);
  else
    {
      /* For LRE, unwind_base is the run-time address of the unwind
	 table which is at vunwind_table64 in the buffer.  THis
	 point corresponds to an offset of (unwind_base - text_base).

	table_displacement + unwind_base - text_base = vunwind_table64
	 table_displacement = vunwind_table64 - unwind_base + text_base

       */
	
      table_displacement =   (unsigned char*) vunwind_table64 
			   - unwind_base 
			   + text_base;
    }
  for (i = 0; i < n_unwind_entries; i++)
    {
      if (is_unwind_32)
	{
	  /* RM: ??? ILP32 bug */
	  ui->entries[i].region_start = swizzle(unwind_table32[i].start + text_base);
	  ui->entries[i].region_end = swizzle(unwind_table32[i].end + text_base);
	  u_offset = unwind_table32[i].info_ptr;
	  ui->entries[i].info_block =
	    (char *) (unwind_buf + (text_base + u_offset - unwind_base));
	  ui->entries[i].scale_factor = 4;
          
          /* Since the unwind table is sorted by the procedure start address
             make sure that the next unwind table start address i,e next proce
             -dure start address is greater than the last one.
          */ 
          if ((i+1) < n_unwind_entries)
           {
             if (unwind_table32[i+1].start < unwind_table32[i].start)
              {
                warning ("The unwind information of %s is corrupted."
                         "\nwdb is unable to read the unwind information"
                         "\nThe unwind table entry [%d]'s start address is %#x"
                         " smaller \nthan the unwind table entry [%d]'s start"
                         " address %#x.\n"  
                         , objfile->name, (i+1), unwind_table32[i+1].start
		         , i, unwind_table32[i].start);         
                return;
              }
           } 
          
  	  /* Also check for next unwind table start is not less than 
	     prev unwind table end.
          */
	  if ((i+1) < n_unwind_entries)
           {
             if (unwind_table32[i+1].start < unwind_table32[i].end)
              {
                warning ("The unwind information of %s is corrupted."
                         "\nwdb is unable to read the unwind information"
                         "\nThe unwind table entry [%d]'s start address is %#x"
                         " smaller \nthan the unwind table entry [%d]'s end"
                         " address %#x.\n"  
                         , objfile->name, (i+1), unwind_table32[i+1].start
		         , i, unwind_table32[i].end);         
                return;
              }
           } 
	}
      else
	{
	  /* RM: ??? ILP32 bug */
	  ui->entries[i].region_start = vunwind_table64[i].start + text_base;
	  ui->entries[i].region_end = vunwind_table64[i].end + text_base;
	  u_offset = vunwind_table64[i].info_ptr;
	  /* Compute table_displacement, u_offset + displacement points
	     at the unwind thing in our buffer copied from text_base +
	     u_offset.
	     */

	  ui->entries[i].info_block = (char*) table_displacement + u_offset;
	  ui->entries[i].scale_factor = 8;
          
          /* Since The unwind table is sorted by the procedure start address
             make sure that the next unwind table start address i,e next proce
             -dure start address is greater than the last one.
          */
          if ((i+1) < n_unwind_entries)
           {
             if (vunwind_table64[i+1].start < vunwind_table64[i].start)
              {
                warning ("The unwind information of %s is corrupted."
                         "\nwdb is unable to read the unwind information."
                         "\nThe unwind table entry [%d]'s start address is %#x"
                         " smaller \nthan the unwind table entry [%d]'s start"
                         " address %#x.\n"  
                         , objfile->name, (i+1), vunwind_table64[i+1].start
                         , i, vunwind_table64[i].start);
                return;   
              }
           }
  	  
	  /* Also check for next unwind table start is not less than 
	     prev unwind table end.
          */
          if ((i+1) < n_unwind_entries)
           {
             if (vunwind_table64[i+1].start < vunwind_table64[i].end)
              {
                warning ("The unwind information of %s is corrupted."
                         "\nwdb is unable to read the unwind information."
                         "\nThe unwind table entry [%d]'s start address is %#x"
                         " smaller \nthan the unwind table entry [%d]'s end"
                         " address %#x.\n"  
                         , objfile->name, (i+1), vunwind_table64[i+1].start
                         , i, vunwind_table64[i].end);
                return;   
              }
           }
	
       }

      /* RM: Read first region header, and check if it is a prologue */

      region_header = u_offset + 8 + table_displacement;
      
      /* the prologue header includes a prologue size, extract it. */
      prologue_found = 
        /* format R1 */
        ((((*region_header) & 0xe0) == 0) ||
         /* format R2 */
         (((*region_header) & 0xf8) == 0x40) ||
         /* format R3 */
         ((*region_header) == 0x60));
       
      prologue_size = 0;
      if (prologue_found)
	{
	  /* format R1 */
	  if (((*region_header) & 0xe0) == 0)
	    {
	      prologue_size = (*region_header) & 0x1f;
	    }
	  /* format R2 */
	  if (((*region_header) & 0xf8) == 0x40)
	    {
	      prologue_size =
		read_unsigned_leb128 ((char*) region_header + 2, &n);
	    }
	  /* format R3 */
	  if ((*region_header) == 0x60)
	    {
	      prologue_size =
		read_unsigned_leb128 ((char*) region_header + 1, &n);
	    }
	}

      /* Compute the end of the prologue address by adding into the entry
	 address the number of bytes in "prologue_size" slots (3 slots per
	 bundle and 16 bytes per bundle), plus the slot number of any
	 remainder in the last bundle of the prologue.  */
      ui->entries[i].prologue_end =
	ui->entries[i].region_start + prologue_size / 3 * 16 +
	prologue_size % 3;
    }
}

static void
read_unwind_info (struct objfile *objfile)
{
  struct obj_unwind_info *ui = NULL;

  assert (!objfile->is_mixed_mode_pa_lib);
  if (!objfile->obj_private)
    {
      objfile->obj_private =
	(obj_private_data_t *)(void *)
	obstack_alloc (&objfile->psymbol_obstack,
		       sizeof (obj_private_data_t));
      ((obj_private_data_t *) objfile->obj_private)->unwind_info = NULL;
      ((obj_private_data_t *) objfile->obj_private)->so_info = NULL;
      ((obj_private_data_t *) objfile->obj_private)->lmdp = NULL;
    }

  ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;

  if (!ui)
    {
      read_unwind_sections (objfile);
      ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;
    }

  if (ui && !ui->entries)
    internalize_unwinds (objfile);
}

/* Lookup the unwind info for the given PC.  We search all of the
   objfiles seeking the unwind table entry for this PC.  Each objfile
   contains a sorted list of struct unwind_table_entry.  Since we do a
   binary search of the unwind tables, we depend upon them to be
   sorted.  */

/* MG: Note that for IA64 the 'region_end' field of the unwind descriptor
   is defined to contain the address of the first bundle beyond
   the end of the procedure.
   For this reason, lookups should ensure that the IP of interest lies within
   the range region_start(inclusive) through region_end(exclusive).
   The boundary conditions below now use '<' instead of '<='. */

struct unwind_table_entry *
find_unwind_entry (CORE_ADDR pc)
{
  int first, middle, last;
  struct objfile *objfile = NULL;

  /* A function at address 0?  Not in HP-UX! */
  if (pc == (CORE_ADDR) 0)
    return NULL;

  ALL_OBJFILES (objfile)
  {
    struct obj_unwind_info *ui = NULL;

    if (objfile->is_mixed_mode_pa_lib || objfile_is_mixed_mode (objfile))
      {
        continue;
      }

    /* Skip the separate debug object , look for the actual one. */
    if (objfile->separate_debug_objfile_backlink)
      continue;

    if (objfile->obj_private)
      ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;

    if (!ui || !ui->entries)
      {
	read_unwind_info (objfile);
	ui = ((obj_private_data_t *) (objfile->obj_private))->unwind_info;
      }

    /* First, check the cache */

    if (ui->cache
	&& pc >= ui->cache->region_start
	&& pc <  ui->cache->region_end)  /* was <= (see comment above) */
      return ui->cache;

    /* Not in the cache, do a binary search */

    first = 0;
    last = ui->last;

    while (first <= last)
      {
	middle = (first + last) / 2;
	if (   pc >= ui->entries[middle].region_start
	    && pc <  ui->entries[middle].region_end) /* was <= */
	  {
	    ui->cache = &ui->entries[middle];
	    return &ui->entries[middle];
	  }

	if (pc < ui->entries[middle].region_start)
	  last = middle - 1;
	else
	  first = middle + 1;
      }
  }				/* ALL_OBJFILES() */
  return NULL;
}

/* Given an ip, returns the information about that load module.
   Callback provided to unwind express. */
int 
ia64_uwx_lookupip (int request, uint64_t ip, intptr_t tok,
			 uint64_t **resultp)
{
  struct uwx_info *info = (struct uwx_info *) tok;
  uint64_t *rvec;
  int i;
  if (!debugging_aries && !debugging_aries64)
    if (!have_read_load_info) 
      get_load_info();

  if (request == UWX_LKUP_LOOKUP) 
    {
      rvec = info->rvec;
      i = 0;
      if (PC_IN_USER_SENDSIG(ip))
        {
	  /* if it is a user_sendsig frame, just notify that. */
          rvec[i++] = UWX_KEY_CONTEXT;
          rvec[i++] = UWX_ABI_HPUX_SIGCONTEXT;
          rvec[i++] = 0;
          rvec[i++] = 0;
          *resultp = rvec;
          return UWX_LKUP_FDESC;
	}
      else if (! IS_TARGET_LRE)
        { /* Make unwind table for non LRE 
	     unwind_base is the address of the unwind header section which
	     does not exist on Linux.
	     */
	  struct load_module_desc desc;
	  CORE_ADDR unwind_base;
  	  int ret_val;

	  /* Get the load module desc for this load module. */
	  if (!loadmap_from_ip (&desc, ip))
	    return UWX_LKUP_ERR;

          /* QXCR1000903217: reset the mismatched_so_name here to remove
             the record of any mismatched happened in the past */
          mismatched_so_name = NULL;

	  unwind_base = desc.unwind_base;

	  /* Add the text_base. */
          rvec[i++] = UWX_KEY_TBASE;
          rvec[i++] = desc.text_base;

	  /* Add the unwind flags. First double-word that
	     the unwind_base points to. */
          rvec[i++] = UWX_KEY_UFLAGS;
	  target_read_memory (unwind_base, (char*)&rvec[i], 8);
	  i++;

	  /* Add the unwind_start. Second dword that the
	     unwind_base points to. */
          rvec[i++] = UWX_KEY_USTART;
	  target_read_memory (unwind_base+8, (char*)&rvec[i], 8);
	  rvec[i] += desc.text_base;
	  i++;

	  /* Add the unwind_end. Third dword that the
	     unwind_base points to. */
          rvec[i++] = UWX_KEY_UEND;
	  target_read_memory (unwind_base+16, (char*)&rvec[i], 8);
	  rvec[i] += desc.text_base;
	  i++;

          rvec[i++] = 0;
          rvec[i++] = 0;
          *resultp = rvec;
          return UWX_LKUP_UTABLE;
	}
      else 
        { /* Set up unwind lookup table for LRE. 
	     unwind_base is the address of the unwind section.
	     See elf_init_lmdp.
	   */

	  struct load_module_desc desc;
  	  int ret_val;

	  if (!loadmap_from_ip (&desc, ip))
	    return UWX_LKUP_ERR;
          rvec[i++] = UWX_KEY_TBASE;
          rvec[i++] = desc.text_base;

          rvec[i++] = UWX_KEY_USTART;
	  rvec[i++] = desc.unwind_base;
//          rvec[i++] = desc.unwind_base;
          rvec[i++] = UWX_KEY_UEND;
	  rvec[i++] = desc.unwind_base + desc.unwind_size;
//          rvec[i++] = desc.unwind_base + desc.unwind_size;
          rvec[i++] = 0;
          rvec[i++] = 0;
          *resultp = rvec;
          return UWX_LKUP_UTABLE;
	}
    }
  else if (request == UWX_LKUP_FREE) 
    {
      /* We do not allocate any datastructures above. So, nothing to free. */
      return 0;
    }
  return 0;
}

/* Given an ip, return the load_module descriptor for that load module 
   in desc. Return 0 for failure and 1 for success. This function assumes
   that desc is already allocated by the caller. */
static int
loadmap_from_ip (struct load_module_desc *desc, Int64 pc)
{
  CORE_ADDR start, end;
  struct objfile *objfile;
  CORE_ADDR swiz_pc;
  struct load_module_desc *lmdp;

  swiz_pc = pc;
  if (PC_IN_CALL_DUMMY (pc, 0, 0))
    return 0;

  /* Find objfile for pc */
  ALL_OBJFILES (objfile)
    {
      if (objfile->is_mixed_mode_pa_lib || objfile_is_mixed_mode (objfile))
        {
          continue;
        }  

      /* Skip the separate debug object , look for the actual one. */
      if (objfile->separate_debug_objfile_backlink)
        {
          continue;
        }

      /* QXCR1000579474: Aries core file debugging. */
      boolean is_aries = false;
      if (aries_module_name)
        {
          char *module_name = xstrdup (objfile->name);
          module_name = basename (module_name);

          if ((0 == strcmp (module_name, "aries64_dbg.so")) ||
              (0 == strcmp (module_name, "aries32_dbg.so")) ||
              (0 == strcmp (module_name, "aries64.so")) ||
              (0 == strcmp (module_name, "aries32.so")))
            {
              is_aries = true;
              if ((0 == aries_runtime_text_start) ||
                  (0 == aries_text_size) ||
                  (0 == aries_linktime_unwind_start) ||
                  (0 == aries_runtime_unwind_start))
                {
                  error ("aries-core command needs to be executed to "
                         "debug aries.");
                }
            }
        }

      /* is the PC in this objfile? */
      if ((!objfile->is_archive_bound) &&
          (objfile->obj_private) &&
          (((obj_private_data_t *) objfile->obj_private)->lmdp) || is_aries)
        {
          if (is_aries)
            {
              start = aries_runtime_text_start;
              end = start + aries_text_size;
            }
          else
            {
              start = ((struct load_module_desc *)
                       ((obj_private_data_t *) objfile->obj_private)->lmdp)->text_base;
              end = ((struct load_module_desc *)
                      ((obj_private_data_t *) objfile->obj_private)->lmdp)->text_base +
                    ((struct load_module_desc *)
                      ((obj_private_data_t *) objfile->obj_private)->lmdp)->text_size;
            }
        }
      else
        {
          start = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol ("__text_start",
							       0, objfile));
          end = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol ("_etext",
                                                             0, objfile));
        }
      start = swizzle(start);
      end = swizzle(end);

      if ((pc < start) || (pc >= end))
        continue;

      if (!objfile->obj_private ||
          !(((obj_private_data_t *) objfile->obj_private)->lmdp))
        {
          lmdp = (struct load_module_desc *)(void *)
                 obstack_alloc (&objfile->psymbol_obstack,
                                sizeof (struct load_module_desc));
          
          memset (lmdp, 0, sizeof (*lmdp));
          if (objfile->is_archive_bound)
            {
              /* set the minimum necessary */
              /* RM: The following lines are correct only for
               * archive bound programs
               */
              lmdp->text_base = swizzle(start);
              lmdp->unwind_base =
                swizzle(SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol ("__unwind_header",
                                                             0, objfile)));
	      }
          else
            {
              /* JAGag40885: Improved warning message from a less meaningful
               * "Unwind failed for lack of lmdp" */
              if (target_has_stack && !target_has_execution)
                warning ("Unwind failed since the load module descriptor\n"
                         "of %s is not available. This\n"
                         "might happen due to a mismatch between the "
                         "core file\nand executable.", objfile->name);
              else
                warning ("Unwind failed since the load module descriptor\n"
                         "of %s is not available.", objfile->name);

              lmdp->text_base = swizzle(start);
              if (is_aries)
                {
                  lmdp->unwind_base = swizzle (aries_runtime_unwind_start);
                }
              else
                {
                  lmdp->unwind_base =
                    swizzle(SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol (
                                                      "__unwind_header",
                                                      0, objfile)));
                }
            }
          if (objfile->obj_private == NULL)
            {
              objfile->obj_private =
                (obj_private_data_t *)(void *)
                  obstack_alloc (&objfile->psymbol_obstack,
                                 sizeof (obj_private_data_t));
              ((obj_private_data_t *) objfile->obj_private)->unwind_info =
		 NULL;
	       ((obj_private_data_t *) objfile->obj_private)->so_info =
		 NULL;
            }
          ((obj_private_data_t *) objfile->obj_private)->lmdp = lmdp;
        }

      break;
    }
  
  if (!objfile)
    {
      /* For kernal addresses in user_sendsig() */
      if (PC_IN_USER_SENDSIG(swiz_pc)) 
        {
          lmdp = desc;
          lmdp->text_base = load_info.li_sendsig_txt;
          lmdp->unwind_base = load_info.li_sendsig_unw;
          lmdp->text_size = load_info.li_sendsig_tsz;
          return 1;
        }

      /* Bindu 09/25/03: For stripped archive bound programs, __text_start is not
	 present. Get the text_base and unwind_base from .text and .IA_64.unwind_hdr
	 respectively. */
      if (symfile_objfile && symfile_objfile->is_archive_bound)
        {
	  asection *unwind_sec;
	  lmdp = desc;

          unwind_sec = bfd_get_section_by_name (symfile_objfile->obfd, ".IA_64.unwind_hdr");
          if (unwind_sec)
            {
              if ( bfd_section_size (symfile_objfile->obfd, unwind_sec) == 0)
                {
                  return 0;  /* nothing to do, no unwind entries */
                }
              lmdp->unwind_base = unwind_sec->vma +
                ANOFFSET (symfile_objfile->section_offsets, 0);
            }
          else
	    {
              warning ("Unwind base not found");
	      return 0;
	    }

          lmdp->text_base = elf_text_start_vma (symfile_objfile->obfd) +
            ANOFFSET (symfile_objfile->section_offsets, 0);
	  return 1;
	}

      return 0;
    }
  
 
  lmdp = desc;
  *lmdp = *((struct load_module_desc *)
                    (((obj_private_data_t *) objfile->obj_private)->lmdp));
  lmdp->text_base =  
    swizzle (lmdp->text_base);
  lmdp->unwind_base = 
    swizzle (lmdp->unwind_base);

  return 1;
}

/* Function to read the inferiors memory or registers.*/
int 
ia64_uwx_copyin (
    int request,
    char *loc,
    uint64_t rem,
    int len,
    intptr_t tok)
{
  struct uwx_info *info = (struct uwx_info *) tok;
  int regid, i;
  int status;
  CORE_ADDR regval, fr_val[2];
  CORE_ADDR *ret_val = (CORE_ADDR *)(void *) loc;

  if (request != UWX_COPYIN_REG)
    {
      status = target_read_memory (rem, loc, len);
      if (status != 0)
        return 0;
      return len;
    }
  else
    {
      /* Read registers. */
      regid = (int)rem;
      if (!info->frame_status)
        {
	  /* Read the top-most frame's registers. */
	  if (rem < UWX_REG_GR(0))
	    {
              switch (regid)
	        {
                  case UWX_REG_AR_PFS:
		    *ret_val = read_register (PFS_REGNUM);
                    break;
                  case UWX_REG_PREDS:
		    *ret_val = 0;
		    for (i = PR0_REGNUM; i <= PRLAST_REGNUM; i++)
		      {
			regval = read_register (i);
			regval = (regval << (i - PR0_REGNUM));
			*ret_val = *ret_val | regval;
		      }
                    break;
                  case UWX_REG_AR_RNAT:
		    *ret_val = read_register (AR0_REGNUM + 19);
                    break;
                  case UWX_REG_AR_UNAT:
		    *ret_val = read_register (AR0_REGNUM + 36);
                    break;
                  case UWX_REG_AR_FPSR:
		    *ret_val = read_register (AR0_REGNUM + 40);
                    break;
                  case UWX_REG_AR_LC:
		    *ret_val = read_register (AR0_REGNUM + 65);
                    break;
		  case UWX_REG_BSPSTORE:
		    *ret_val = pre_interrupt_bsp ();
		    break;
                  default:
                    return 0;
                }
            }
          else if (regid >= UWX_REG_GR(1) && regid <= UWX_REG_GR(31))
	    {
	      *ret_val = read_register (GR0_REGNUM + (regid - UWX_REG_GR(0)));
            }
          else if (regid >= UWX_REG_BR(0) && regid <= UWX_REG_BR(7))
	    {
	      *ret_val = read_register (BR0_REGNUM + (regid - UWX_REG_BR(0)));
            }
	  else if (regid >= UWX_REG_FR(2) && regid <= UWX_REG_FR(127))
	    {
	      read_register_gen (FR0_REGNUM +
				 (regid - UWX_REG_FR(0)), (char*)fr_val);
	      memset(ret_val, '\000', 4);
	      memcpy(((char*)ret_val)+4 ,fr_val, 12);
	    }
	  else
	    {
	      printf ("did not implement reading uwx register %d\n", regid);
	      return 0;
	    }
        }
      else if (info->frame_status == 1)
	{
	  /* Read registers from the saved ucontext. */
	  if (rem < UWX_REG_GR(0)) 
	    {
              switch (regid) 
		{
                  case UWX_REG_AR_PFS:
                    status = __uc_get_ar(info->ucontext, 64, 
					 (UWX_ARG_ADDR64) ret_val);
                    break;
                  case UWX_REG_PREDS:
                    status = __uc_get_prs(info->ucontext, 
					  (UWX_ARG_ADDR64) ret_val);
                    break;
                  case UWX_REG_AR_RNAT:
                    status = __uc_get_ar(info->ucontext, 19, 
					  (UWX_ARG_ADDR64) ret_val);
                    break;
                  case UWX_REG_AR_UNAT:
                    status = __uc_get_ar(info->ucontext, 36, 
					  (UWX_ARG_ADDR64) ret_val);
                    break;
                  case UWX_REG_AR_FPSR:
                    status = __uc_get_ar(info->ucontext, 40, 
					  (UWX_ARG_ADDR64) ret_val);
                    break;
                  case UWX_REG_AR_LC:
                    status = __uc_get_ar(info->ucontext, 65, 
					  (UWX_ARG_ADDR64) ret_val);
                    break;
                  default:
                    return 0;
                }
            }
          else if (regid >= UWX_REG_GR(1) && regid <= UWX_REG_GR(31))
	    {
	      unsigned int nat;
              status = __uc_get_grs (info->ucontext,
                                regid - UWX_REG_GR(0), 1, 
				(UWX_ARG_ADDR64) ret_val, &nat);
	     }
          else if (regid >= UWX_REG_BR(0) && regid <= UWX_REG_BR(7)) 
	    {
              status = __uc_get_brs (info->ucontext,
                                regid - UWX_REG_BR(0), 1, 
				(UWX_ARG_ADDR64) ret_val);
            }
	  else if (regid >= UWX_REG_FR(2) && regid <= UWX_REG_FR(127))
	    {
	      status = __uc_get_frs (info->ucontext, regid - UWX_REG_FR(0),
					 1, (fp_regval_t *)(void *) ret_val);
	    }
	  else
	    {
	      printf ("did not implement reading uwx register %d\n", regid);
	      return 0;
	    }

          if (status != 0)
            return 0;
	}
      else /* dummy frame. */
        {
	  struct frame_saved_regs fsr;
	  /* Read the dummy registers saved at dummy_fp. */
	  ia64_find_dummy_frame_regs_1 (info->dummy_fp, &fsr);
  	  if (rem < UWX_REG_GR(0))
	    {
              switch (regid)
	        {
                  case UWX_REG_AR_PFS:
		    *ret_val = read_memory_integer (fsr.regs[PFS_REGNUM], REGISTER_SIZE);
                    break;
                  case UWX_REG_PREDS:
		    *ret_val = 0;
		    for (i = PR0_REGNUM; i <= PRLAST_REGNUM; i++)
		      {
			regval = read_memory_integer (fsr.regs[i],
							REGISTER_SIZE);
			regval = (regval << (i - PR0_REGNUM));
			*ret_val = *ret_val | regval;
		      }
                    break;
                  case UWX_REG_AR_RNAT:
		    *ret_val =  read_memory_integer (fsr.regs[AR0_REGNUM + 19],
							 REGISTER_SIZE);
                    break;
                  case UWX_REG_AR_UNAT:
		    *ret_val = read_memory_integer (fsr.regs[AR0_REGNUM + 36],
							 REGISTER_SIZE);
                    break;
                  case UWX_REG_AR_FPSR:
		    *ret_val = read_memory_integer (fsr.regs[AR0_REGNUM + 40],
							 REGISTER_SIZE);
                    break;
                  case UWX_REG_AR_LC:
		    *ret_val = read_memory_integer (fsr.regs[AR0_REGNUM + 65],
 							REGISTER_SIZE);
                    break;
		  case UWX_REG_BSPSTORE:
		    *ret_val =  read_memory_integer (fsr.regs[AR0_REGNUM + 17], 8);
		    regval =  read_memory_integer (fsr.regs[CFM_REGNUM], 8);
		    *ret_val = add_to_bsp (*ret_val,
				 -SIZE_OF_FRAME (regval));
		    break;
                  default:
                    return 0;
                }
            }
          else if (regid >= UWX_REG_GR(1) && regid <= UWX_REG_GR(31))
	    {
	      *ret_val = read_memory_integer (
		fsr.regs[GR0_REGNUM + (regid - UWX_REG_GR(0))], REGISTER_SIZE);
            }
          else if (regid >= UWX_REG_BR(0) && regid <= UWX_REG_BR(7))
	    {
	      *ret_val = read_memory_integer (
		fsr.regs[BR0_REGNUM + (regid - UWX_REG_BR(0))], REGISTER_SIZE);
            }
	  else if (regid >= UWX_REG_FR(2) && regid <= UWX_REG_FR(127))
	    {
	      target_read_memory (fsr.regs[FR0_REGNUM + 
			(regid - UWX_REG_FR(0))], (char*)fr_val, 12);
	      memset(ret_val, '\000', 4);
	      memcpy(((char*)ret_val)+4 ,fr_val, 12);
	    }
	 else
	    {
	      printf ("did not implement reading uwx register %d\n", regid);
	      return 0;
	    }
        }
      return len;
    }
}

/* Returns the value of a GR read from given uwx. */
UInt64
MyUnwindContext_getGRval (struct uwx_env * myuc, int num)
{
  int result;
  CORE_ADDR reg_val;
  if (!myuc)
    {
      error ("Unable to read GR from null uwx");
    }
  result = uwx_get_reg (myuc, UWX_REG_GR(num), (UWX_ARG_ADDR64) &reg_val);
  if (result < UWX_OK)
    {
      error ("Unable to read GR %d from uwx", num);
    }

  return reg_val;
}

/* Returns the value of PC read from given uwx. */
UInt64
MyUnwindContext_getIPval (struct uwx_env * myuc)
{
  int result;
  CORE_ADDR pc_val;
  if (!myuc)
    {
      error ("Unable to read IP from null uwx");
    }
  result = uwx_get_reg (myuc, UWX_REG_IP, (UWX_ARG_ADDR64) &pc_val);
  if (result < UWX_OK)
    {
      error ("Unable to read IP from uwx");
    }
  return pc_val;
}


/* Returns the value of RP read from given uwx. */
UInt64
MyUnwindContext_getRPval (struct uwx_env * myuc)
{
  int result;
  CORE_ADDR rp_val, pc, fp;
  struct frame_saved_regs fsr;

  if (!myuc)
    {
      error ("Unable to read RP from null uwx");
    }
  pc = MyUnwindContext_getIPval(myuc);
  /* If this is a call_dummy frame, get the rp from saved registers.*/
  if (PC_IN_CALL_DUMMY (pc, 0, 0))
    {
      fp = MyUnwindContext_getGRval (myuc, SAVE_SP_REGNUM - GR0_REGNUM);
      ia64_find_dummy_frame_regs_1 (fp, &fsr);
      return read_memory_integer (fsr.regs[PC_REGNUM], REGISTER_SIZE);
    }

  result = uwx_get_reg (myuc, UWX_REG_RP, (UWX_ARG_ADDR64) &rp_val);
  if (result < UWX_OK)
    {
      /* QXCR1000903217: Report the mismatched library name, if any. This
         will help in pinpointing the source of the unwinding issue. */
      if (mismatched_so_name && target_has_stack && !target_has_execution)
        {
          warning ("This is probably due to the mismatched library %s, "
                   "use packcore command for core files, or use the "
                   "correct version of this library.", mismatched_so_name);
          mismatched_so_name = NULL;
        }
      return 0;
    }

  /* If this is a user_sendsig frame, get the rp from ucontext. */
  if (result == UWX_ABI_FRAME)
    {
      int status;
      CORE_ADDR gr32_val;

      struct uc_header header_fields_in_user_context;

      /* r32 has a pointer to ucontext */
      status = uwx_get_reg(myuc, UWX_REG_GR(32), (UWX_ARG_ADDR64) &gr32_val);
      if (status != 0)
	return status;

      /* Get the size of ucontext structure. */
      target_read_memory (gr32_val, (char*)&header_fields_in_user_context,
                sizeof (header_fields_in_user_context));

      if (0 == header_fields_in_user_context.size_in_bytes)
        {
          return 0;
        }

      /* Read the ucontext. */
      if (!cbinfo -> ucontext)
	{
          /* allocate space for uc. */
          cbinfo -> ucontext = (ucontext_t *)
                   xmalloc (header_fields_in_user_context.size_in_bytes);
        }
      target_read_memory (gr32_val, (char*) cbinfo -> ucontext,
                          header_fields_in_user_context.size_in_bytes);
      get_saved_register_from_uc (PC_REGNUM, (char*)&rp_val, cbinfo->ucontext);
    }

  return rp_val;
}


/* Returns the value of PSP read from given uwx. */
UInt64
MyUnwindContext_getPSPval (struct uwx_env * myuc)
{
  int result;
  CORE_ADDR psp_val, pc;
  if (!myuc)
    {
      error ("Unable to read PSP from null uwx");
    }
  pc = MyUnwindContext_getIPval(myuc);
  /* If this a dummy frame, get psp from SAVE_SP_REGNUM. */
  if (PC_IN_CALL_DUMMY (pc, 0, 0))
    {
      return MyUnwindContext_getGRval (myuc, SAVE_SP_REGNUM - GR0_REGNUM);
    }
  result = uwx_get_reg (myuc, UWX_REG_PSP, (UWX_ARG_ADDR64) &psp_val);
  if (result < UWX_OK)
    {
      /* QXCR1000903217: Report the mismatched library name, if any. This
         will help in pinpointing the source of the unwinding issue. */
        if (mismatched_so_name && target_has_stack && !target_has_execution)
          {
            warning ("Unwind problem encountered.");
            warning ("This is probably due to the mismatched library %s, "
                     "use packcore command for core files, or use the "
                     "correct version of this library.", mismatched_so_name);
            mismatched_so_name = NULL;
          }
      return 0;
    }
  /* If this frame is user_Sendsig frame, get psp from ucontext. */
  if (result == UWX_ABI_FRAME)
    {
      int status;
      CORE_ADDR gr32_val;

      struct uc_header header_fields_in_user_context;

      /* r32 has a pointer to ucontext */
      status = uwx_get_reg(myuc, UWX_REG_GR(32), (UWX_ARG_ADDR64) &gr32_val);
      if (status != 0)
        {
	  warning ("Unwind problem encountered.  Continuing.");

         /* QXCR1000903217: Report the mismatched library name, if any. This
            will help in pinpointing the source of the unwinding issue. */
          if (mismatched_so_name && target_has_stack && !target_has_execution)
            {
              warning ("This is probably due to the mismatched library %s, "
                       "use packcore command for core files, or use the "
                       "correct version of this library.", mismatched_so_name);
              mismatched_so_name = NULL;
            }
	  return 0;
	}

      /* Get the size of ucontext structure. */
      status = target_read_memory (gr32_val, 
                                   (char*)&header_fields_in_user_context,
                		   sizeof (header_fields_in_user_context));
      if (status != 0)
	{
	  warning ("Unwind problem encountered.  Continuing.");
	  return 0;
	}

      if (header_fields_in_user_context.size_in_bytes > MAX_PLAUSIBLE_UNWIND)
	{
	  warning ("Unwind problem encountered.  Continuing.");
	  return 0;
	}

      if (0 == header_fields_in_user_context.size_in_bytes)
        {
          return 0;
        }

      if (!cbinfo -> ucontext)
	{
          /* allocate space for uc. */
          cbinfo -> ucontext = (ucontext_t *)
                   xmalloc (header_fields_in_user_context.size_in_bytes);
        }
      target_read_memory (gr32_val, (char*) cbinfo -> ucontext,
                          header_fields_in_user_context.size_in_bytes);
      get_saved_register_from_uc (SP_REGNUM, (char*)&psp_val, cbinfo->ucontext);
    }
  return psp_val;
}


/* Returns the value of SP read from given uwx. */
UInt64
MyUnwindContext_getSPval (struct uwx_env * myuc)
{
  int result;
  CORE_ADDR sp_val;
  if (!myuc)
    {
      error ("Unable to read SP from null uwx");
    }
  result = uwx_get_reg (myuc, UWX_REG_SP, (UWX_ARG_ADDR64) &sp_val);
  if (result < UWX_OK)
    {
      error ("Unable to read SP from uwx");
    }
  return sp_val;
}


/* Returns the value of CFM read from given uwx. */
UInt64
MyUnwindContext_getCFMval (struct uwx_env * myuc)
{
  int result;
  CORE_ADDR reg_val;
  if (!myuc)
    {
      error ("Unable to read GR from null uwx");
    }
  result = uwx_get_reg (myuc, UWX_REG_CFM, (UWX_ARG_ADDR64) &reg_val);
  if (result < UWX_OK)
    {
      error ("Unable to read CFM from uwx");
    }

  return reg_val;
}

/* Given an unwind context, get the previous frame's unwind context. */
int
MyUnwindContext_step (struct uwx_env * myuc)
{
  CORE_ADDR pc, prev_pc, sp, fp, bsp, cfm, fr_val[2] = {0,0};
  struct frame_saved_regs fsr;
  struct objfile *objfile;
  struct load_module_desc desc;
  int i, result;
  int num_stacked_regs;
  int mitr_export_stub_fixup_done = 0;
  CORE_ADDR rp, prev_sp;

  pc = MyUnwindContext_getIPval (myuc);

  /* If current frame is a dummy frame, retrive the prev frame info from 
     saved dummy context. */
  if (PC_IN_CALL_DUMMY (pc, 0, 0))
    {
      /* prev frame's sp is saved in SAVED_SP_REGNUM. */
      fp = MyUnwindContext_getGRval (myuc, SAVE_SP_REGNUM - GR0_REGNUM);
      /* Get the saved registers from dummy frame. */
      ia64_find_dummy_frame_regs_1 (fp, &fsr);
      /* prev frame's pc */
      prev_pc = read_memory_integer (fsr.regs[PC_REGNUM], REGISTER_SIZE);
      /* CFM */
      cfm = read_memory_integer (fsr.regs[CFM_REGNUM], 8);
      /* AR.BSP. The saved BSP is the after_interrupt_bsp of the frame */
      bsp =  read_memory_integer (fsr.regs[AR0_REGNUM + 17], 8);
      bsp = add_to_bsp (bsp, -SIZE_OF_FRAME (cfm));


      /* Set dummy_fp and frame status to 2 to let the ia64_uwx_copyin
	 know where to read registers from. */
      cbinfo->dummy_fp = fp;
      cbinfo->frame_status = 2;

      uwx_init_context(myuc, prev_pc, fp, bsp, cfm);

      return UWX_OK;
    }
  else
    {
      /* RM: If we have no uc, return now */
      if (!(myuc))
	return UWX_BOTTOM;

      /* If we cannot find a loadmap for this ip, return error. */
      if (!loadmap_from_ip (&desc, pc))
	    return UWX_ERR_IPNOTFOUND;

      prev_pc = MyUnwindContext_getRPval (myuc);
      /* jini: With mixed mode unwinding, there is an issue in unwinding beyond
         the routine aries_pa2ia_arg_trans. This is since this routine is
         called by an assembly routine called aries_pa2ia_export_stub placed in
         the data section and this has no unwind entries. This routine has 
         a register stack frame consisting of 2 locals and 1 output register, and
         has no memory stack frame.
        
         To overcome the unwind issue, we manually adjust the BSP to skip this
         frame and its caller, and the IP to the caller of aries_pa2ia_export_stub.

         The first few instructions of aries_pa2ia_export_stub are as follows:

         alloc            r32=ar.pfs,0,2,1,0;;
         nop.m            0x0
         mov              r33=b0

         This shows us that we have to obtain the CFM from r32 (BSP) and RP
         from r33 (BSP + 8).

         Steps below:
          - Check if we have the RP of the routine falling in 
            aries_pa2ia_export_stub.
          - If so, obtain BSP from unwind context and reduce by the size of
            locals of aries_pa2ia_export_stub to obtain the BSP correspoding
            to that of aries_pa2ia_export_stub.
          - Obtain CFM and RP stored in aries_pa2ia_export_stub by reading in
            the values at BSP and BSP + 8.
          - Obtain the FP of this frame, which would represent the SP of the
            caller of aries_pa2ia_export_stub. 
          - Get the unwind library to give us a new frame.
          - Reduce BSP to represent the BSP of the caller of
            aries_pa2ia_export_stub by subtracting the size of locals
            corresponding to the CFM stored in aries_pa2ia_export_stub frame.
          - Make the following substitutions in the unwind context.
              - The IP with the RP saved in aries_pa2ia_export_stub
              - BSP with the new BSP
              - CFM with the CFM saved in aries_pa2ia_export_stub
              - SP with the FP of the prev routine. 
      */

         
      if (   inferior_aries_text_start
          && (   aries_pa2ia_export_stub_addr < prev_pc
              && prev_pc < aries_pa2ia_export_stub_addr +
                           aries_pa2ia_export_stub_size))
        {
          CORE_ADDR temp_bsp;
          fp = MyUnwindContext_getGRval (myuc, SAVE_SP_REGNUM - GR0_REGNUM);
	  uwx_get_reg(myuc, UWX_REG_BSP, (UWX_ARG_ADDR64) &bsp);
          temp_bsp = add_to_bsp (bsp, -2);

          prev_sp = MyUnwindContext_getPSPval (myuc);
       
          target_read_memory (temp_bsp, (char *)&cfm, 8);

          target_read_memory (temp_bsp + 8, (char *)&rp, 8);
          mitr_export_stub_fixup_done = 1;
       } 
      /* ask unwind to give us a new frame. */
      result = uwx_step(myuc);

      if (mitr_export_stub_fixup_done == 1)
        {
          bsp = add_to_bsp (bsp, -2);
          bsp = add_to_bsp (bsp, -SIZE_OF_LOCALS (cfm));
          result = uwx_init_context (myuc, rp, prev_sp, bsp, cfm);
        }

      /* if the current frame is a user_sendsig frame, get the prev
	 frame from saved ucontext. */
      if (result == UWX_ABI_FRAME)
        {
	  int status;
          ucontext_t *uc;
          CORE_ADDR gr32_val;

	  struct uc_header header_fields_in_user_context;

          /* r32 has a pointer to ucontext */
	  status = uwx_get_reg(myuc, UWX_REG_GR(32), 
			       (UWX_ARG_ADDR64) &gr32_val);
	  if (status != 0)
	    return status;

          /* Get the size of ucontext structure. */
          target_read_memory (gr32_val, (char*)&header_fields_in_user_context,
                sizeof (header_fields_in_user_context));

	  if (header_fields_in_user_context.size_in_bytes > MAX_PLAUSIBLE_UNWIND)
	    {
	      warning ("Unwind problem encountered.  Continuing.");
	      return 0;
	    }

	  if (!cbinfo -> ucontext)
	    {
              /* allocate space for uc. */
              cbinfo -> ucontext = (ucontext_t *)
                   xmalloc (header_fields_in_user_context.size_in_bytes);
            }
          target_read_memory (gr32_val, (char*) cbinfo -> ucontext,
                          header_fields_in_user_context.size_in_bytes);

	  /* Let's remember that we have started unwinding from sigcontext
	     frame. */
	  cbinfo -> frame_status = 1;
	  set_context_from_sigcontext (myuc, cbinfo -> ucontext);

	  return UWX_OK;
        }
      /* If the newly created prev frame is in call dummy, ignore the
	 ignore the falure from uwx. We can handle it later on. */
      pc = MyUnwindContext_getIPval (myuc);
      if (PC_IN_CALL_DUMMY(pc, 0, 0))
	{
	  return UWX_OK;
	}

      return result;
    }
}

/* Indicates if the given frame is the topmost frame in the stack. */
int
is_topmost_frame (struct basic_frame_info * bframe)
{
  struct frame_info *topmost;

  topmost = get_current_frame (); /* Get top most frame */

  /* Verify if the frame's SP matches the value of the SP register
     and the top most frame's pc matches the argument frame's pc.
     Also check the rse_fp. */
  if ((bframe->sp == read_sp ()) 
      && (bframe->pc == topmost->pc) 
      && (bframe->bsp == pre_interrupt_bsp ()))
    return 1;

  return 0;
}

/* Sets the top_bframe to tomost frame. Returns 1 if success. 
  Returns 0 if no topmost frame. */
int
java_get_topmost_frame (struct basic_frame_info *top_bframe)
{
  struct frame_info *top_frame = get_current_frame ();
  if (top_frame)
    {
      top_bframe->pc = top_frame->pc;
      top_bframe->fp = top_frame->frame;
      top_bframe->bsp = top_frame->rse_fp;
      top_bframe->cfm = top_frame->cfm;
      top_bframe->sp = read_register (SP_REGNUM);
      return 1;
    }
  return 0;
  
}
/* Given a basic_frame bprev, get the basic frame information for 
   the next frame bnext. */
int
java_get_next_frame (struct basic_frame_info *bprev, 
			struct basic_frame_info *bnext)
{
  struct frame_info *next;
  next = get_current_frame (); /* Start from top frame */
  if (!next)
    {
      memset (bnext, 0, sizeof (struct basic_frame_info));
      return 0;
    }
  while (next->prev != 0)
    {
      if ((next->frame == bprev->sp)
  	  && (next->prev->rse_fp == bprev->bsp))/* found */
        {
          /* fill up the frame information */
          bnext->pc = next->pc;
          bnext->fp = next->frame;
          bnext->bsp = next->rse_fp;
          bnext->cfm = next->cfm;
          bnext->java_ptr = next->java_ptr;
          if (next->next)
            bnext->sp = next->next->frame;
          else
            bnext->sp = read_sp (); /* Top frame */
          return 1;
        }
      else
       next = next->prev;
    } /* while */

  /* Not found. */
  memset (bnext, 0, sizeof (struct basic_frame_info));
  return 0;
}

/* indicate if the given frame is the caller of a signal handler. */
int is_next_frame_signal_handler (struct basic_frame_info *bframe)
{
  struct frame_info *next;
  next = get_current_frame ();
  while (next)
  {
    if (!next->prev)
      return 0;
    if ((next->frame == bframe->sp) 
        && (next->prev->rse_fp == bframe->bsp))
    { /* Found the next frame. */
      if (next->signal_handler_caller)
        return 1;
      return 0;
    }
    next = next->prev;
  }
  return 0;
}

/* Callback for java to read the registers. */
int
java_get_saved_register (struct basic_frame_info *bframe, int regnum,
                unsigned long long *reg)
{
  extern int read_relative_register_raw_bytes (int regnum, char *myaddr);
  struct frame_info *next;
  CORE_ADDR addr;

  /* For top frame, read from context */
  if (is_topmost_frame (bframe)) {
    if (read_relative_register_raw_bytes (regnum, (char*) &addr))
      return 0;
  *reg= (CORE_ADDR) addr;
  return 1;
  }
  next = get_current_frame ();

  /* Find corresponding frame and read its registers, use sp as handle */
  while (next != 0) {
    if (!next->prev)
        return 0;
    /* Mixed mode corefile debugging: If we encounter mixed mode PA frames,
       use the "aries_border_frame". */
    if (next->pa_save_state_ptr)
      {
        next = aries_border_frame;       
      }
    if ( (next->frame == bframe->sp) /* SP and BSP matched */
        && (next->prev->rse_fp ==  bframe->bsp) )
    {
      if (read_relative_register_raw_bytes_for_frame (regnum, 
						(char *) &addr, 
						next->prev))
        return 0;
      *reg= (CORE_ADDR) addr;
      return 1;
    }
    next = next->prev;
  }
  return 0;
}

/* Used by Java stack unwinding to update an intermediate native frame. 
 * Similar to make_new_frame which is used to make the topmost frame. 
 */
void
java_make_native_frame (struct frame_info *frame, CORE_ADDR sp)
{
  init_uwx_env ();
  uc_distance = 0;
  
  java_set_native_context (current_uc, frame, sp);
  frame->java_ptr = 0;
  return;
} /* java_make_native_frame */


/* Java stack unwind for IA. Given a frame next_frame get previous frame's
   frame_info prev. */
void
do_java_stack_unwind (struct frame_info * next_frame,
                struct frame_info * prev)
{
  struct basic_frame_info bprev, bnext;

  /* Initialize basic frame being sent to junwindlib */
  bnext.pc = next_frame->pc;
  bnext.fp = 0;

  if (next_frame->next && next_frame->next->pa_save_state_ptr)
    {
      if (!aries_border_frame)
        error ("aries_border_frame is NULL. Cannot proceed with mixed-mode\n"
               "unwinding. Please turn debug-aries on to proceed.");
    }

  /* If this the next of next_frame is not a java frame (i.e., next_frame is
     the first java frame after a native frame), get the cfm and bsp from pfs
     and bsp of next of next_frame. If the next of next_frame is a signal
     handler caller, we already have proper values set for cfm and rse_fp
     from ucontext.*/
  if (next_frame->next && !next_frame->next->pa_save_state_ptr &&
      (!is_java_frame (next_frame->next->pc))
      && !next_frame->next->signal_handler_caller)
    {
      read_relative_register_raw_bytes_for_frame (AR0_REGNUM + 64,
                                                  (char *) &next_frame->cfm, 
						  next_frame->next);
      next_frame->cfm &= 0x0000003fffffffffLL;
      next_frame->rse_fp = add_to_bsp (next_frame->next->rse_fp, 
			    -SIZE_OF_LOCALS (next_frame->cfm));
    }

  fill_regs_from_uc (current_uc, next_frame);

  if (!next_frame->next && !next_frame->next->pa_save_state_ptr)
    {
      next_frame->cfm = read_register (AR0_REGNUM + 64);
      next_frame->cfm &= 0x0000003fffffffffLL;
      next_frame->rse_fp = pre_interrupt_bsp();
    }
  else if (next_frame->next->pa_save_state_ptr)
    {
      /* Use the aries border frame. */
      read_relative_register_raw_bytes_for_frame (AR0_REGNUM + 64,
                                                  (char *) &next_frame->cfm, 
						  aries_border_frame);
      next_frame->cfm &= 0x0000003fffffffffLL;
      next_frame->rse_fp = add_to_bsp (aries_border_frame->rse_fp, 
			    -SIZE_OF_LOCALS (next_frame->cfm));
    }

  bnext.bsp = next_frame->rse_fp;
  bnext.cfm = next_frame->cfm;
  bnext.java_ptr = 0;

  if (next_frame->next)
    {
      if (next_frame->next->pa_save_state_ptr)
        {
          bnext.sp = aries_border_frame->frame;
        }
      else
        {
          bnext.sp = next_frame->next->frame;
        }
    }
  else
    {
      bnext.sp = read_sp ();
    }

  /* Call Java Unwind library and update prev */
  if (get_prev_java_frame_info (&bnext, &bprev)) 
    {
      prev->pc = bprev.pc;
      next_frame->frame = bprev.sp;
      prev->cfm = bprev.cfm;

      prev->rse_fp = add_to_bsp (next_frame->rse_fp, -SIZE_OF_LOCALS (bprev.cfm));

      prev->n_rse_regs = (int) SIZE_OF_FRAME (bprev.cfm);
      prev->java_ptr = bprev.java_ptr;

      if (find_unwind_entry (prev->pc)) /* Native frame */
        {
          /* initialize unwind */
          java_make_native_frame (prev, next_frame->frame);
          return;
        }

      if (bprev.fp) /* Update frame pointer */
        prev->frame = bprev.fp;
    }
}

/* Get function name (along with additional information) for Java frame */
char *
get_java_func_name_ipf (struct frame_info * frame)
{
  struct basic_frame_info bframe;
  char* fstr;

  bframe.fp = 0;
  bframe.pc = frame->pc;
  bframe.bsp = frame->rse_fp;
  bframe.cfm = frame->cfm;
  bframe.java_ptr = frame->java_ptr;

  if (frame->next)
    bframe.sp = frame->next->frame;
  else
    bframe.sp = read_sp ();

  /* Call Java Unwind library to get string */
  fstr = get_java_frame_str (&bframe);
  if (bframe.fp)
    frame->frame = bframe.fp;

  return fstr;
} /* get_java_func_name_ipf */

/* Check if we need to handle bad PC values */
int
handle_bad_pc (struct frame_info * frame)
{
  if (   inferior_aries_text_start
      && inferior_aries_text_end
      && in_mixed_mode_pa_lib (frame->pc, NULL))
    return mixed_mode_handle_bad_pc (frame);

  /* Are we the top frame or the frame calling a signal handler ?*/
  /* Can only handle bad PC for these cases */
  if (frame->next && !frame->next->signal_handler_caller)
    return 0; 

  /* Is it a Good PC ? */
  if (find_unwind_entry (frame->pc))
    return 0; 

  /* We can handle this bad PC */
  return 1;
}

#ifndef HP_IA64_GAMBIT
/* Given a pointer to ucontext_t, retrieve registers from them. */
void
get_saved_register_from_uc (int regnum, char* myaddr, ucontext_t *uc)
{
  unsigned int nats;
  CORE_ADDR regvals[1];
  int status;

  /* PC */
  if (regnum == PC_REGNUM)
    {
       status = __uc_get_ip (uc, (uint64_t*)(void *) myaddr);
       if (status)
        warning ("error in getting register %d from ucontext\n", regnum);
    }

  /* __reason */
  if (regnum == REASON_REGNUM)
    {
      status = __uc_get_reason (uc, (uint16_t*)(void *) myaddr);
       if (status)
        warning ("error in getting register %d from ucontext\n", regnum);
    }

  /* gr0 */
  else if (regnum == GR0_REGNUM)
    {
      memset (myaddr, '\000', REGISTER_SIZE);
    }
  /* gr1 - gr31 */
  else if ((regnum >= GR0_REGNUM) && (regnum < GR0_REGNUM + 32))
    {
      status = __uc_get_grs (uc, regnum - GR0_REGNUM, 1, (UWX_ARG_ADDR64) regvals,
			     &nats);
      if (status)
	warning ("error in getting register %d from ucontext\n", regnum);
      memcpy (myaddr, regvals, 8);
    }
  /* br0 - br7 */
  else if ((regnum >= BR0_REGNUM) && (regnum <= BRLAST_REGNUM ))
    {
      status = __uc_get_brs (uc, regnum - BR0_REGNUM, 1, 
			     (UWX_ARG_ADDR64) regvals);
      if (status)
	warning ("error in getting register %d from ucontext\n", regnum);
      memcpy (myaddr, regvals, 8);
    }
  /* fr0 - fr127 */
  else if ((regnum >= FR0_REGNUM) && (regnum <= FRLAST_REGNUM))
    {
      fp_regval_t fpvals[1];
      status = __uc_get_frs(uc, regnum - FR0_REGNUM, 1, fpvals);
      if (status)
	warning ("error in getting register %d from ucontext\n", regnum); 
      memcpy (myaddr, fpvals, 16);
    }
  /* ar0 - ar127. All of these registers are not readable. */
  else if ((regnum >= AR0_REGNUM) && (regnum <= ARLAST_REGNUM))
    {
      status = __uc_get_ar (uc, regnum - AR0_REGNUM, (uint64_t*)(void *) myaddr);
      if (status)
	warning ("error in getting register %d from ucontext\n", regnum);
    }
}
#endif /* !HP_IA64_GAMBIT */

/* Obtain the previous frame if the current frame has a bad PC */
void
do_handle_bad_pc (struct frame_info * frame, struct frame_info * prev)
{
#ifndef HP_IA64_GAMBIT
  int i;
  int result, num_stacked_regs;
  CORE_ADDR reg_addr, regval, fr_val[2] = {0,0};
  CORE_ADDR pfs;
  ucontext_t *uc;

  if (   inferior_aries_text_start
      && inferior_aries_text_end
      && in_mixed_mode_pa_lib (frame->pc, NULL))
    {
      mixed_mode_do_handle_bad_pc (frame, prev);
      return;
    }
 
  if (frame->next == 0)
    {
      /* Top frame */
      if (print_bad_pc_unwind_msg)
        warning ("Attempting to unwind past bad PC %s ",
               longest_local_hex_string ((LONGEST) read_register (PC_REGNUM)));

      /* use value of SP */
      frame->frame = read_sp ();
      prev->frame = frame->frame;

      /* get the ip from rp. */
      prev->pc = read_register (BR0_REGNUM);

      /* BSP has advanced by the sol of previous frame
         (with br.call). Get back to the right BSP. */
      pfs = read_register (AR0_REGNUM + 64);
      prev->rse_fp = add_to_bsp (frame->rse_fp,
		   			-SIZE_OF_LOCALS (pfs));
    }
  else
    {
      /* Signal Handler caller */
      CORE_ADDR bsp;
      CORE_ADDR gr32_val;
      CORE_ADDR pc;
      uint16_t reason;
      /* A variable to hold the header fields of ucontext structure. */
      struct uc_header header_fields_in_user_context;

      /* Information about the frame signal generating frame 
	 is saved in user_sendsig (signal handler caller) frame
         in ucontext structure. */ 
      /* r32 has a pointer to ucontext */
      read_relative_register_raw_bytes_for_frame (GR0_REGNUM + 32,
                (char *) &gr32_val, frame->next);
      /* Get the size of ucontext structure. */
      target_read_memory (gr32_val, (char*)&header_fields_in_user_context,
                sizeof (header_fields_in_user_context));

      /* allocate space for uc. */
      if (!cbinfo -> ucontext
          && header_fields_in_user_context.size_in_bytes != 0)
	{
          /* allocate space for uc. */
          cbinfo -> ucontext = (ucontext_t *)
                   xmalloc (header_fields_in_user_context.size_in_bytes);
        }
      uc = cbinfo->ucontext;
      cbinfo->frame_status = 1;

      if (header_fields_in_user_context.size_in_bytes != 0)
        {
          target_read_memory (gr32_val, (char*) uc,
                	      header_fields_in_user_context.size_in_bytes);

          get_saved_register_from_uc (PC_REGNUM, (char *) &pc, uc);
          if (print_bad_pc_unwind_msg)
            warning ("Attempting to unwind past bad PC %s ",
                     longest_local_hex_string ((LONGEST) pc));

          /* Get SP, PFS, RP and BSP saved from the uc. */
          get_saved_register_from_uc (SP_REGNUM, (char *) &frame->frame,
				      uc);
          get_saved_register_from_uc (AR0_REGNUM + 64, (char*) &pfs, uc);
          get_saved_register_from_uc (BR0_REGNUM, (char*) &prev->pc, uc);
          get_saved_register_from_uc (AR0_REGNUM + 17, (char*) &bsp, uc);

          get_saved_register_from_uc (REASON_REGNUM, (char*) &reason, uc);
        }
      else
        {
          pc = frame->frame = pfs = prev->pc = bsp = reason = 0;
        }

      /* Before we got into the user_sendsig frame, bsp is moved to
	 contain the sof of the bad frame (ie, size of outs of the
	 prev frame). To get to the bsp of the previous frame,
	 reduce the bsp by sof of previous frame. */
      /* If this frame is not a syscall context, then cover has been performed.
         Reduce the bsp by size of frame. */
      if (reason != 0)
        prev->rse_fp = add_to_bsp (bsp, -SIZE_OF_FRAME (pfs));
    }

  /* Get the number of stacked registers for this frame. */
  prev->n_rse_regs = (int) SIZE_OF_FRAME (pfs);

  /* Initialize unwind context. */

  init_uwx_env ();
  uc_distance = 0;

  /* CFM = PFS */
  uwx_init_context (current_uc, prev->pc, frame->frame, prev->rse_fp, pfs);
  result = uwx_set_reg (current_uc, UWX_REG_BSPSTORE, prev->rse_fp);
  if (result != UWX_OK)
    error ("error setting reg in uwx. ");
  read_relative_register_raw_bytes (FR0_REGNUM + 2, ((char*)fr_val)+4);
  result = uwx_set_fr (current_uc, UWX_REG_FR(2), (UWX_ARG_ADDR64) fr_val);
  if (result != UWX_OK)
    error ("error setting reg in uwx. ");
  prev->java_ptr = 0;

  /* Get the fp. */
  prev->frame = MyUnwindContext_getPSPval (current_uc);
#endif /* !HP_IA64_GAMBIT */
  return;
}


/* Given a frame, get this frame's saved_pc which is  previous frame's pc. */
CORE_ADDR
frame_saved_pc (struct frame_info *frame)
{
  CORE_ADDR prev_pc = 0;

  if (mixed_mode_pa_unwind && frame->pa_save_state_ptr)
    {
      prev_pc = mixed_mode_frame_saved_pc (frame);
      /* PA to PA BOR calls might cause the previous frame's pc to be
         in the libaries code. If so, find the corresponding PA rp
         and return it. */
      if (   inferior_aries_text_start <= prev_pc
          && prev_pc < inferior_aries_text_end)
        {
          char *name = NULL;
          /* Due to PA to PA BOR calls, the PA code has to return through
             aries_pa2ia_process() in the libaries code. */
          find_pc_partial_function (prev_pc, &name,
                                   (CORE_ADDR *) NULL, (CORE_ADDR *) NULL);
          if (IS_ARIES_PA2IA_PROCESS (name))
            {
              if (frame->prev)
                {
                  if (   inferior_aries_text_start <= frame->prev->pc
                      && frame->prev->pc < inferior_aries_text_end)
                    {
                      error ("Mismatch in frame data.");
                    }
                  else
                    return frame->prev->pc;
                }
              else
                {
                  struct frame_info *prev_frame = NULL;
                  prev_frame = get_prev_frame (frame);
                  prev_frame = mixed_mode_adjust (prev_frame);
                  return prev_frame->pc;
                }
            }
          error ("Call from an unknown aries routine %s.", name);
        }
        else
        {
          return prev_pc;
        }
    }

  if (frame->prev)
    {
      prev_pc = frame->prev->pc;
    }
  /* Do we own the current uc? */
  else if (uc_distance == 0)
    {
      prev_pc = MyUnwindContext_getRPval (current_uc);
    }
  else if (!current_uc)
    {
      /* bottom of stack */
      return 0;
    }
  /* Does the (so far unconstructed) previous frame own the current_uc? */
  else if (uc_distance == 1)
    prev_pc = MyUnwindContext_getIPval (current_uc);
  /* reached bottom-of-stack or error. return pc of 0. */
  else if (uc_distance == -1)
    return 0;
  else
    error ("Unexpected error in frame_saved_pc()");

  return prev_pc;
}

/* Given a frame return fp of previous frame. */

CORE_ADDR
frame_chain (struct frame_info *frame)
{
  CORE_ADDR prev_fp = 0;
  CORE_ADDR pc;
  int result;

  /* Bindu 041302: The below code is picked up from PA. Libpthread doesn't 
     mark the bottom-of-stack for threads properly (rp == 0). Once this bug
     is fixed in libpthread, we don't need to do the following. */
  /* Bindu 111102: Also put in the code to check if we got to main and stop
     unwinding. */
  struct minimal_symbol *min_frame_symbol;
  /* struct symbol         *frame_symbol; */
  char                  *frame_symbol_name = NULL;

  /* Call the corresponding mixed mode routine if this is a mixed mode PA
     frame. */
  if (mixed_mode_pa_unwind && frame->pa_save_state_ptr)
    {
      return mixed_mode_frame_chain (frame);
    }

  min_frame_symbol  = lookup_minimal_symbol_by_pc (frame->pc);
  /* frame_symbol      = find_pc_function( frame->pc ); */

#ifdef HP_MXN
  /* If this is a threaded application, and we see the
   * routine "__pthread_unbound_body", treat it as the stack root
   * for this thread.
   */
  if ((min_frame_symbol != 0) /* && (frame_symbol == 0) */ )
    {
    /* The test above for "no user function name" would defend
     * against the slim likelihood that a user might define a
     * routine named "__pthread_unbound_body" and then try to debug it.
     *
     * If it weren't commented out, and you tried to debug the
     * pthread library itself, you'd get errors.
     *
     * So for today, we don't make that check.
     */
    frame_symbol_name = SYMBOL_NAME( min_frame_symbol );
    if (frame_symbol_name != 0) {
      if ((0 == strncmp( frame_symbol_name,
                        THREAD_INITIAL_FRAME_SYMBOL,
                        sizeof(THREAD_INITIAL_FRAME_SYMBOL) ))
	  || (0 == strncmp( frame_symbol_name,
                        THREAD_INITIAL_FRAME_SYMBOL2,
                        sizeof(THREAD_INITIAL_FRAME_SYMBOL2) ))
	  || (0 == strncmp( frame_symbol_name,
                        THREAD_INITIAL_FRAME_SYMBOL3,
                        sizeof(THREAD_INITIAL_FRAME_SYMBOL3) ))) {
         /* Pretend we've reached the bottom of the stack.
          */
         reached_main_in_frame_chain = true;
         return (CORE_ADDR) 0;
         }
       }
    }  /* End of hacky code for threads. */
#endif
  /* Check if we reached main. Signal end of stack trace. */
  if (min_frame_symbol != 0)
    {
      frame_symbol_name = SYMBOL_NAME( min_frame_symbol );
      if (frame_symbol_name != 0) 
        {
          if (0 == strcmp( frame_symbol_name, default_main))
            {
              /* Pretend we've reached the bottom of the stack.
               */
              reached_main_in_frame_chain = true;
              return (CORE_ADDR) 0;
            }
       }
    } /* End of hacky code for finding main. */

  reached_main_in_frame_chain = false;

  if (frame->prev)
    {
      prev_fp = frame->prev->frame;
    }
  /* bottom of stack */
  else if (uc_distance == -1)
    return 0;
  /* Do we own the current uc? */
  else if (uc_distance == 0)
    {
      /* To get the previous frame's fp, we need to get the psp of
       * previous frame. As we have the current frame's uc, we need
       * to step once to get to the previous frame's uc. 
       * If this step gets into a java frame, return some fp and 
       * upper layers will handle it. 
       */

      /* Bindu 022406: Copy all the registers out of the unwind context
         into frame before we change current_uc. */
      fill_regs_from_uc (current_uc, frame);

      /* Get the previous uc. */
      result = MyUnwindContext_step (current_uc);
      uc_distance++;

      /* If the prev frame is a java frame, return some fp. */
      if ((result < UWX_OK)
	  && (java_debugging)
	  && (is_java_frame (MyUnwindContext_getIPval (current_uc))))
	return frame->frame;

      if (result < UWX_OK)
        {
	  UNWIND_FAIL(result);
	  return 0;
        }

      if (result == UWX_BOTTOM)
	{
	  uc_distance = -1;
	  return 0;
	}
      /* Previous frame's sp is this frame's fp.*/
      prev_fp = MyUnwindContext_getPSPval (current_uc);
      /* If prev_fp is 0 and this is a java frame, return some fp. Upper
	 layers will handle this. */
      if (   (prev_fp == 0) 
	  && java_debugging 
	  && (is_java_frame (MyUnwindContext_getIPval (current_uc))))
        {
	  return frame->frame;
	}
    }
  /* Does the (so far unconstructed) previous frame own the current_uc? */
  else if (uc_distance == 1)
    {
      pc = MyUnwindContext_getIPval (current_uc);
      if (java_debugging
          && (is_java_frame (pc)))
        return frame->frame;

      /* Previous frame's sp is this frame's fp. */
      prev_fp = MyUnwindContext_getPSPval (current_uc);
      /* If prev_fp is 0 and this is a java frame, return some fp. Upper
         layers will handle this. */
      if (   (prev_fp == 0)
	  && (java_debugging)
	  && (is_java_frame (pc)))
	{
	  return frame->frame;
	}
    }
  else
    error ("Unexpected error in frame_chain()");
  return prev_fp;
}

void
push_dummy_frame (struct inferior_status *inf_status)
{
  CORE_ADDR sp;
  register int regnum;
  CORE_ADDR int_buffer;
  char reg_buffer[MAX_REGISTER_RAW_SIZE];
  int num_stacked_regs;

#ifdef HP_IA64_GAMBIT
  {
    CORE_ADDR pc;

    /* RM: Gambit won't let us set the pc to a non-bundle aligned
    address. Since we eventually have to reset the pc to its current
    value, don't allow command line calls from non-bundle aligned
    addresses */ 
    pc = read_register (PC_REGNUM);
    if (pc & 0x3)
      error ("Command line calls must be made from a bundle aligned\n"
	     "address. Please step to a bundle aligned address and try\n"
	     "again\n");
    
    /* First flush the RSE, so we can unwind past this call */
    if (remote_ia64_flush_RSE () == 0)
      error ("RSE flush failed, cannot unwind\n");
  }
#endif
  registers_changed ();

  /* RM: save the current SP */
  sp = read_register (SP_REGNUM);
  int_buffer = read_register (SAVE_SP_REGNUM);
  write_memory (sp - REGISTER_SIZE, (char *) &int_buffer, REGISTER_SIZE);
  write_register (SAVE_SP_REGNUM, sp);

  sp -= REGISTER_SIZE;

  sp = push_word (sp, read_register (PC_REGNUM));
  sp = push_word (sp, read_register (CFM_REGNUM));
  /* RM: save the static registers */
  for (regnum = GR0_REGNUM + 1; regnum < GR0_REGNUM + NUM_ST_GRS; regnum++)
    if (regnum != SAVE_SP_REGNUM)
      sp = push_word (sp, read_register (regnum));

  /* RM: save the stacked registers */
  /* RM: ??? we only need to store the "out" registers */
  num_stacked_regs = (int) SIZE_OF_FRAME (read_register(CFM_REGNUM));
  for (regnum = GR0_REGNUM + NUM_ST_GRS; regnum < (GR0_REGNUM + NUM_ST_GRS +
                                           num_stacked_regs); regnum++)
  {
    sp = push_word (sp, read_register (regnum));
  }

  /* RM: save the branch registers */
  for (regnum = BR0_REGNUM; regnum <= BRLAST_REGNUM; regnum++)
    sp = push_word (sp, read_register (regnum));

  /* RM: save the predicate registers */
  for (regnum = PR0_REGNUM; regnum <= PRLAST_REGNUM; regnum++)
    {
      read_register_bytes (REGISTER_BYTE (regnum), reg_buffer, 1);
      sp = push_bytes (sp, reg_buffer, 1);
    }

  /* RM: save the NaT registers */
  for (regnum = NR0_REGNUM; regnum < (NR0_REGNUM + NUM_ST_GRS +
				      num_stacked_regs); regnum++)
    {
      read_register_bytes (REGISTER_BYTE (regnum), reg_buffer, 1);
      sp = push_bytes (sp, reg_buffer, 1);
    }

  /* RM: save the floating point registers */
  for (regnum = FR0_REGNUM; regnum <= FRLAST_REGNUM; regnum++)
    {
      read_register_bytes (REGISTER_BYTE (regnum), reg_buffer, 12);
      sp = push_bytes (sp, reg_buffer, 12);
    }

  /* RM: save the application registers */
  for (regnum = AR0_REGNUM; regnum <= ARLAST_REGNUM; regnum++)
    sp = push_word (sp, read_register (regnum));

  sp = push_word (sp, read_register (PSR_REGNUM));
  /* RM: align the stack */
  sp = (sp % 16) ? (sp & ((CORE_ADDR)(long) -16)) : sp;
  write_register (SP_REGNUM, sp);
  return;
}

/* bindu 022602: This function sets the unwind context for an intermediate 
 * frame. This function is used when we enter a native frame from a java 
 * frame while doing Java stack unwinding.
 * Similar to set_initial_context.
 */
void
java_set_native_context (struct uwx_env * uenv, struct frame_info *frame,
			 CORE_ADDR sp)
{
  int result;
  CORE_ADDR pc, bsp, cfm, fr_val[2] = {0,0};
  extern int remote_ia64_flush_RSE PARAMS ((void));

#ifdef HP_IA64_GAMBIT
  /* flush RSE */
  if (remote_ia64_flush_RSE () == 0)
    error ("RSE flush failed, cannot unwind\n");
#endif

  /* If we don't have a real unwind context, return now */
  if (!uenv)
    return;

  result = uwx_init_context(uenv, frame->pc, sp, frame->rse_fp, frame->cfm);
  if (result < UWX_OK)
    {
      error("Incorrect unwind initialization,  %d", result);
    }
  read_relative_register_raw_bytes (FR0_REGNUM + 2, ((char*)fr_val)+4);
  result = uwx_set_fr (uenv, UWX_REG_FR(2), (UWX_ARG_ADDR64) fr_val);
  if (result != UWX_OK)
    error ("Cannot set register in uwx. ");
}

/* Set the initial context for the topmost frame. */
void 
set_initial_context (struct uwx_env * uenv, CORE_ADDR pc)
{
  int result;
  CORE_ADDR bsp, sp, cfm, fr_val[2] = {0,0};

#ifdef HP_IA64_GAMBIT
  /* flush RSE */
  if (remote_ia64_flush_RSE () == 0)
    error ("RSE flush failed, cannot unwind\n");
#endif
  registers_changed ();

  /* RM: If we don't have a real unwind context, return now */
  if (!uenv)
    return;
  
  /* Get sp, bsp and cfm. */
  sp = read_register (SP_REGNUM);
  bsp = pre_interrupt_bsp ();
  cfm = read_register (CFM_REGNUM);

  result = uwx_init_context (uenv, pc, sp, bsp, cfm);
  if (result < UWX_OK)
    {
      error("Incorrect unwind initialization,  %d", result);
    }
  result = uwx_set_reg (uenv, UWX_REG_BSPSTORE, bsp);
  if (result != UWX_OK)
    error ("Cannot set register in uwx. ");

  /* Set atleast one FR. */
  read_relative_register_raw_bytes (FR0_REGNUM + 2, ((char*)fr_val)+4);
  result = uwx_set_fr (uenv, UWX_REG_FR(2), (UWX_ARG_ADDR64) fr_val);
  if (result != UWX_OK)
    error ("Cannot set register in uwx. ");
}

/* Set context from the from ucontext to get the previous frame of
   user_sendsig frame. */
void 
set_context_from_sigcontext (struct uwx_env * uenv, ucontext_t *ucontext)
{
  int result;
  int status;
  uint16_t reason;
  uint64_t ip;
  uint64_t sp;
  uint64_t bsp;
  uint64_t cfm;
  unsigned int nat;
  uint64_t ec;
  CORE_ADDR fr_val[2] = {0,0};
  status = __uc_get_reason(ucontext, &reason);
  status = __uc_get_ip(ucontext, &ip);
  status = __uc_get_grs(ucontext, 12, 1, &sp, &nat);
  status = __uc_get_ar(ucontext, 17, &bsp);
  status = __uc_get_ar(ucontext, 66, &ec);
  status = __uc_get_cfm(ucontext, &cfm);
  if (status)
    warning ("error in getting registers from ucontext\n");
  /* If this frame is not a syscall context, then cover has been performed.
     Reduce the bsp by size of frame. */
  cfm |= ec << 52;
  if (reason != 0)
    {
      bsp = add_to_bsp (bsp, -SIZE_OF_FRAME(cfm));
    }
  uwx_init_context(uenv, ip, sp, bsp, cfm);
  result = uwx_set_reg (uenv, UWX_REG_BSPSTORE, bsp);
  if (result != UWX_OK)
    error ("Cannot set register in uwx. ");
  /* Set atleast one FR. */
  read_relative_register_raw_bytes (FR0_REGNUM + 2, ((char*)fr_val)+4);
  result = uwx_set_fr (uenv, UWX_REG_FR(2), (UWX_ARG_ADDR64) fr_val);
  if (result != UWX_OK)
    error ("Cannot set register in uwx. ");
  return;
}

/* make_new_frame - make a new unwind context based on pc and frame.
   If failure, uc_distance is -1.  On success, current_uc is the unwind
   context of the parent and frame has been initialized. 
 */
void
make_new_frame (CORE_ADDR pc, struct frame_info *frame)
{
  /* initialize the current_uc. */
  init_uwx_env ();
  uc_distance = 0;
  set_initial_context (current_uc, pc);

  frame->pc = MyUnwindContext_getIPval (current_uc);

  frame->cfm = read_register (CFM_REGNUM);
  frame->n_rse_regs = (int) SIZE_OF_FRAME (frame->cfm);
  /* RM: set rse frame pointer to AR.BSP */
  frame->rse_fp = pre_interrupt_bsp ();

  /* Previous frame's sp is this frame's fp. Get the previous frame's uc. */
  frame->frame = MyUnwindContext_getPSPval (current_uc);
  /* If prev_fp is 0 and this is a java frame, return some fp. Upper
     layers will handle this. */
  if (   (frame->frame == 0)
      && (java_debugging)
      && (is_java_frame (pc)))
    {
      frame->frame = read_sp ();
    }
  return;
} /* make_new_frame */

/* Initialize frame fp, rse_fp, no. of stacked regs and frame registers */
void
init_extra_frame_info (int fromleaf, struct frame_info *frame)
{
  int result;
  CORE_ADDR pc, start, end, cfm = 0;
  struct objfile *objfile;

  if (mixed_mode_pa_unwind)
    {
      mixed_mode_init_extra_frame_info (fromleaf, frame);
      return;
    }

  /* Initialize execution path table to NULL */
  frame->local_ept = NULL;

  /* We are initializing the context from prev frame now. if current frame 
     owns the current_uc, get the prev frames uc. */
  if ((frame->next) && (uc_distance == 0))
    {
      /* Bindu 022406: Copy all the registers out of the unwind context
         into frame before we change current_uc. */
      fill_regs_from_uc (current_uc, frame->next);

      result = MyUnwindContext_step (current_uc);
      if (result < UWX_OK)
	{
	  UNWIND_FAIL (result);
	  return;
	}
      uc_distance++;
    }

  /* RM: We have a new frame, so uc_distance has decreased */
  if (uc_distance > 0)
    uc_distance--;
  
  /* If this is the first frame. */
  if (!frame->next)
    {
      /* Bindu: If pc is already set don't set it again */
      if (!frame->pc)
        pc = read_register (PC_REGNUM);
      else
        pc = frame->pc;

      make_new_frame (pc, frame);
      if (!current_uc || (uc_distance == -1))
	return;  /* UNWIND_FAIL has already been called */
    }
  else
    {
      if (current_uc && (uc_distance == 0))
	{
	  frame->pc = MyUnwindContext_getIPval (current_uc);
	  cfm = MyUnwindContext_getCFMval (current_uc);
          frame->java_ptr = 0;
	  /* Get the BSP from unwind context. It's important to get it 
	     from current_uc, as this will take of the frame prev to
	     signal handler caller.*/
	  uwx_get_reg(current_uc, UWX_REG_BSP, 
		      (UWX_ARG_ADDR64) &frame->rse_fp);
	}
      else if (!current_uc)
	{
	  /* RM: Can this happen? */
	  error ("Internal error while unwinding");
	}
      else if (uc_distance == -1)
	{
	  /* Errors should already have been dealt in frame_chain.
	     If we came here even after error, something wrong. */
	  error ("Internal error while unwinding");
	}
      else
	{
	  error ("Unexpected error in init_extra_frame_info()");
	}

      frame->n_rse_regs = (int) SIZE_OF_FRAME (cfm);	/* cfm.sof */
      frame->cfm = cfm;
    }
}

/* Delete all frame related stuff from our data structures. 
 * 'frame' points to the head of the frame_chain (current_frame). 
 */
void
destroy_extra_frame_info (struct frame_info *frame)
{

  /* Clear the java_ptr. It's allocated by the libjunwind and we need to 
     free this while freeing the frame_chain. */
  struct frame_info *frame_itr = frame;
  while (frame_itr != 0)
    {
      /* pa_save_state_ptr has been allocated in the frame_obstack. No need
         to free it separately. */
      if (frame_itr->pa_save_state_ptr)
        frame_itr->pa_save_state_ptr = NULL;

      if (frame_itr->java_ptr)
	{
	  free (frame_itr->java_ptr);
	  frame_itr->java_ptr = NULL;
	}
      frame_itr = frame_itr->prev;
    }
  
  uc_distance = -1;
}

/* Print IA specific frame information. Called by frame_info()
   using PRINT_EXTRA_FRAME_INFO */
void
ia64_print_extra_frame_info (struct frame_info *frame)
{
  CORE_ADDR nat_addr;
  CORE_ADDR final_stacked_gr_addr;
  int sof;

  if (frame->pa_save_state_ptr)
    {
      return;
    }
  // Print cfm (sof, sol, sor)
  sof = SIZE_OF_FRAME (frame->cfm);
  printf_filtered (" Size of frame is %d, ", sof);
  wrap_here ("   ");
  printf_filtered ("Size of locals is %d, ", SIZE_OF_LOCALS(frame->cfm));
  wrap_here ("   ");
  printf_filtered ("Size of rotating is %d.", SIZE_OF_ROTATING(frame->cfm));
  printf_filtered ("\n");

  // Print any nat collection addresses in sof
  nat_addr = NAT_COLLECTION_ADDR (frame->rse_fp);
  final_stacked_gr_addr = frame->rse_fp + 8 * sof;
  if (nat_addr <= final_stacked_gr_addr)
    printf_filtered (" NAT collections saved at ");
  while (nat_addr <= final_stacked_gr_addr)
    {
      print_address_numeric (nat_addr, 1, gdb_stdout);

      /* Get the next NAT collection addr. Every 64th word in RSE is a nat
         collection. */
      nat_addr += 64 * REGISTER_SIZE;

      /* The last stacked gr address has moved by one word since there is a
         nat collection in between. */
      final_stacked_gr_addr += REGISTER_SIZE;

      if (nat_addr <= final_stacked_gr_addr)
        printf_filtered (" ");
      else
        printf_filtered (".\n");
    }
  /* JAGaf05237: Handle signal handler callers.  */
  if (frame->signal_handler_caller)
    {
      int i, status, last_reg, regno; 
      CORE_ADDR GR32, last_reg_addr; 
      struct uc_header header_fields_in_user_context;
      CORE_ADDR save_state_regs[NUM_REGS];
      ucontext_t *uc;
      unsigned int nats[127] = {0};
      fp_regval_t fpregs[FRLAST_REGNUM - FR0_REGNUM];
      int num_rot_regs, rrb_gr;

      memset (save_state_regs, 0, NUM_REGS * sizeof (CORE_ADDR));
      memset (fpregs, 0, (FRLAST_REGNUM - FR0_REGNUM) * sizeof (fp_regval_t));

      read_relative_register_raw_bytes_for_frame (GR0_REGNUM + 32,
                                         (char *) &GR32, (frame));
      target_read_memory (GR32, (char*)&header_fields_in_user_context,
                          sizeof (header_fields_in_user_context));
      uc = (ucontext_t *) 
           xmalloc (header_fields_in_user_context.size_in_bytes);
      target_read_memory (GR32, (char*) uc,
                          header_fields_in_user_context.size_in_bytes);
     
      __uc_get_ip (uc, (uint64_t*)(void *) &save_state_regs[PC_REGNUM]);
      __uc_get_reason (uc, (uint16_t*)(void *) 
                       &save_state_regs[REASON_REGNUM]);
      
      __uc_get_grs (uc, 1, 31, 
                      (UWX_ARG_ADDR64) (save_state_regs + GR0_REGNUM + 1), nats);

      __uc_get_brs (uc, BR0_REGNUM, BRLAST_REGNUM - BR0_REGNUM,
                      (UWX_ARG_ADDR64) (save_state_regs + BR0_REGNUM));

      __uc_get_frs (uc, FR0_REGNUM, FRLAST_REGNUM - FR0_REGNUM, fpregs);

      for (i = AR0_REGNUM; i <= ARLAST_REGNUM; i++)
        __uc_get_ar (uc, i - AR0_REGNUM, 
                     (uint64_t*)(void *) &save_state_regs[i]);
      __uc_get_cfm (uc, (uint64_t*)(void *) &save_state_regs[CFM_REGNUM]);
#define BSP_REGNUM AR0_REGNUM+17
#define BSP_STORE_REGNUM AR0_REGNUM+18      
      /* The stacked registers are saved in the backing store and not in the
         save_state that we read here. We read in the BSP from the save_state to
         get the state where we were before entering the user_sendsig 
         kernel routine.

         The implementation w.r.t reading stacked grs from backing store is
         similar to what being done in reg_bs_addr (). Instead of reading
         rse_state_info we read from the save_state read here. We also need to 
         check for BS overflows here. */
      last_reg = (int)(31 + SIZE_OF_FRAME(save_state_regs [CFM_REGNUM]) + GR0_REGNUM);
      
      for (i = last_reg, regno = i; i > GR0_REGNUM + 32; i--, regno = i)
        {
            CORE_ADDR reg_addr;
            reg_addr = reg_bs_addr (i, save_state_regs [CFM_REGNUM], 
                                    save_state_regs [BSP_REGNUM]);
            /* Check for Backing store overflow, in which case the rest of stacked regs
              have to be read in from save_state`s bs overflow area.
              If BSP_STORE <= reg_addr < BSP (or BSP > BSP_STORE), then use __uc_get_rsebs.
              If reg_addr < BSP_STORE(where BSP == BSP_STORE), then read directly. 
              If there is no BS overflow, BSP will be equal to BSP_STORE as a result of
              flushing of stacked registers onto the backing store. */
            if (save_state_regs [BSP_STORE_REGNUM] <= reg_addr &&
                reg_addr < save_state_regs [BSP_REGNUM])
              UC_GET_RSEBS (uc, (UC_ARG2_TYPE) reg_addr, 1, 
                               (uint64_t *) (void *) &save_state_regs [i]);
            else
              read_inferior_ideal_memory (reg_addr, (char *)(save_state_regs + i), 
                                       REGISTER_SIZE);
         }

      puts_filtered (" Saved registers state:\n");
      puts_filtered (" Registers with value zero are not displayed\n");

      for (i = 0; i < NUM_REGS; i++)
        if (save_state_regs[i] && i != SP_REGNUM)
          {
            if (i >= FR0_REGNUM && i <= FRLAST_REGNUM)
              ia64_printOrStrcat_fp_register ((char *) NULL, 0, i, 
				  (char *) &fpregs [i - FR0_REGNUM], double_precision);
            else 
              {
                 printf_filtered (" %s: ", REGISTER_NAME (i));
                 print_address_numeric (save_state_regs[i], 1, gdb_stdout);
                 if (i < 127 && nats[i])
                   printf_filtered (" NaT");
                 printf_filtered ("   ");
                 wrap_here (" ");
              }
         }    
       puts_filtered ("\n");
   } /* End signal_handler_caller */  
} // print_extra_frame_info

/* Returns 1 if we can get to prev frame, else returns 0. */
int
frame_chain_valid (CORE_ADDR chain, struct frame_info *frame)
{
  int result;

  if (frame->pa_save_state_ptr)
    return hppa_frame_chain_valid (chain, frame);

  if (frame->prev)
    {
      return 1;
    }
  else if (uc_distance == 1)
    return current_uc != 0;
  else if (uc_distance == -1)
    return 0;
  else
    {
      /* Bindu 022406: Copy all the registers out of the unwind context
         into frame before we change current_uc. */
      fill_regs_from_uc (current_uc, frame);

      result = MyUnwindContext_step (current_uc);
      uc_distance++;

      /* If the prev frame is a java frame, return 1. */
      if ((result < UWX_OK)
          && (java_debugging)
          && (is_java_frame (MyUnwindContext_getIPval (current_uc))))
        return 1;

      if (result < UWX_OK)
        {
          UNWIND_FAIL(result);
	  return 0;
	}
      if (result == UWX_BOTTOM)
	{
	  uc_distance = -1;
	  return 0;
	}
    }

  return 1;
}

/* given a pointer to a bundle, extract the instrution in slot
   0. Instructions are stored little endian on IA64, so we need to
   reverse the bytes here */
long long
slot0_contents (unsigned char *bundle)
{
  long long tmp = 0;
  tmp = (unsigned long long) (((bundle[0] >> 5) |
			      (bundle[1] << 3) |
			      (bundle[2] << 11) |
			      (bundle[3] << 19)) |
    ((unsigned long long) bundle[4]) << 27 |
    (((unsigned long long) bundle[5]) & 0x3f) << 35);

  return tmp;
}

/* given a pointer to a bundle, extract the instrution in slot
   1. Instructions are stored little endian on IA64, so we need to
   reverse the bytes here */
long long
slot1_contents (unsigned char *bundle)
{
  long long tmp = 0;
  tmp = (unsigned long long) (((bundle[5] >> 6) |
			      (bundle[6] << 2) |
			      (bundle[7] << 10) |
			      (bundle[8] << 18)) |
    ((unsigned long long) bundle[9]) << 26 |
    (((unsigned long long) bundle[10]) & 0x7f) << 34);

  return tmp;
}

/* given a pointer to a bundle, extract the instrution in slot
   2. Instructions are stored little endian on IA64, so we need to
   reverse the bytes here */
long long
slot2_contents (unsigned char *bundle)
{
  long long tmp = 0;
  tmp = (unsigned long long) (((bundle[10] >> 7) |
			      (bundle[11] << 1) |
			      (bundle[12] << 9) |
			      (bundle[13] << 17)) |
    ((unsigned long long) bundle[14]) << 25 |
    ((unsigned long long) bundle[15]) << 33);

  return tmp;
}

long long
extract_im22 (long long slot)
{
  long long tmp = 0;

  tmp = ((slot & 0x00000000000fe000ULL) >> 13) |
    ((slot & 0x0000000ff8000000ULL) >> 20) |
    ((slot & 0x0000000007c00000ULL) >> 6);
  if (slot & 0x0000001000000000ULL)
    {
      tmp |= 0xffffffffffe00000ULL;
    }
  return tmp;
}

long long
extract_im64 (long long slot, long long im41)
{
  long long tmp = 0;

  tmp = ((slot & 0x00000000000fe000ULL) >> 13) |
    ((slot & 0x0000000ff8000000ULL) >> 20) |
    ((slot & 0x0000000007c00000ULL) >> 6) |
    ((slot & 0x0000000000200000ULL)) |
    ((im41 & 0x000001ffffffffffULL) << 22) |
    ((slot & 0x0000001000000000ULL) << 27);
  return tmp;
}

/* 
   given a pointer to a bundle, extract the immediate value for a 
   brl instruction
   The immediate value is 60 bits long and its extraction is described
   in section 4, Volume 3 of the IA64 Architecture Software Developer's
   Manual. It is obtained as follows:
	Shift bit 36 of slot 2 by 59 bits
	Or the above with  bits 32-13 of slot 2 
	Or the result with bits 40-2 of slot 1 shifted 20 bits 
*/

long long
extract_im60 (unsigned char *bundle)
{
  long long tmp = 0;
  long long l;
  l = slot2_contents(bundle) ;
  tmp = ((l >>36) &1) << 59;
  tmp |= ((l >>13) & 0xfffff);
  tmp |= (long long) ((slot1_contents(bundle) >> 2) & 0x7fffffffff) << 20;
  return tmp;
}

/* For branch instruction B1 in section 4 last page , 
   Volume 3 of the IA64 Architecture Software Developer's Manual. 
   */

int
extract_im21 (long long slot)
{
  int tmp = 0;
  int sign = 0;

  /* Shift bit 36 (the sign bit) of slot 2 */
  sign = (int) ((slot >> 36 ) &1);

  /* Fix for JAGaf33457.  22 July 2004, Bharath.  Sign extension was bogus. */
  /* Shift the imm 20 bits and or them with the sign bit */
  tmp = (int) ((slot >> 13) & 0xfffff);
  tmp |= (sign << 20);		/* Fix for JAGaf33457.  22 July 2004, Bharath */

  /* Sign extend. */
  tmp = (tmp << 11) >> 11;

  /* Bundle align it - make lower 3 bits 0 */
  tmp = tmp << 4;
  return tmp;
}
/*----------------------------------------------------------------------------*/
/* Function	: is_ip_call_br()
 * Description	: Determines if a single IA64 instruction (syllable) is an IP
 *		    relative call branch.
 * Inputs	: Syllable/instruction encoding, template and slot numbers.
 * Outputs	: 0 if false, non-zero otherwise.
 * Globals	: Uses the array inst_type[] to determine the instruction type
 *		    for a given template and slot number.
 * Notes	: 
 */
static int 
is_ip_call_br(unsigned long long inst, int template, int slot)
{
  return (((((inst) >> 37) & 0xf) == 5) &&	/* opcode == 5 */
  	  (inst_type[template][slot] == B)
	 );
}

/*----------------------------------------------------------------------------*/
/* Function	: is_ind_call_br()
 * Description	: Determines if a single IA64 instruction (syllable) is an 
 *		    indirect call branch.
 * Inputs	: Syllable/instruction encoding, template and slot numbers.
 * Outputs	: 0 if false, non-zero otherwise.
 * Globals	: Uses the array inst_type[] to determine the instruction type
 *		    for a given template and slot number.
 * Notes	: 
 */
static int 
is_ind_call_br(unsigned long long inst, int template, int slot)
{
  return (((((inst) >> 37) & 0xf) == 1)	&&	/* opcode == 1 */
  	  (inst_type[template][slot] == B)
	 );
}

/*----------------------------------------------------------------------------*/
/* Function	: is_ind_ret_br()
 * Description	: Determines if a single IA64 instruction (syllable) is a
 *		    return branch.
 * Inputs	: Syllable/instruction encoding, template and slot numbers.
 * Outputs	: 0 if false, non-zero otherwise.
 * Globals	: Uses the array inst_type[] to determine the instruction type
 *		    for a given template and slot number.
 * Notes	: 
 */
static int 
is_ind_ret_br(unsigned long long inst, int template, int slot)
{
  return (((((inst) >> 37) & 0xf) == 0) &&	/* opcode == 0    */
  	  ((((inst) >> 27) & 0x3f) == 0x21) &&	/* x6     == 0x21 */
  	  ((((inst) >> 6) & 0x7) == 4) &&	/* btype  == 4    */
  	  (inst_type[template][slot] == B)
	 );
}

/*----------------------------------------------------------------------------*/
/* Function	: is_mov_ip()
 * Description	: Determines if a single IA64 instruction (syllable) is a
 *		  'mov r1 = ip' instruction.
 * Inputs	: Syllable/instruction encoding, template and slot numbers.
 * Outputs	: 0 if false, non-zero otherwise.
 */
int 
is_mov_ip (long long slot)
{
  return (((((slot) >> 37) & 0xf) == 0) &&	/* opcode == 0    */
  	  ((((slot) >> 27) & 0x3f) == 0x30));	/* x6     == 0x30 */
}


/* A C++ adjustor thunk will start with "_ZTc" "_ZTh" or "_ZTv" */

/* Fixme - need to handle old demangle style starting with "__"
   for Red Hat 7.2   IS_LRE_HACK  (no hack, just a marker)
*/

int is_cxx_stub_symbol (char *name)
{
  if (   ! name
      || name[0] != '_'
      || name[1] != 'Z'
      || name[2] != 'T'
      || (name[3] != 'c' && name[3] != 'h' && name[3] != 'v'))
    return 0;
  return 1;
}

#ifdef COVARIANT_SUPPORT
/*----------------------------------------------------------------------------*/
/* Function	: is_pc_in_covariant_thunk ()
 * Description	: Determines if a given PC address is located inside a covariant
 *		    return adjuster thunk.
 * Inputs	: A pc address.
 * Outputs	: 1 if the address is inside a covariant thunk, 0 otherwise. 
 * Globals	:
 * Notes	:
 */
int 
is_pc_in_covariant_thunk (CORE_ADDR pc)
{
  char *name;
  struct minimal_symbol* msym;

  msym = lookup_minimal_symbol_by_pc (pc);
  name = SYMBOL_NAME (msym);
  if (name && 
      name[0] == '_' &&
      name[1] == 'Z' &&
      name[2] == 'T' &&
      name[3] == 'c')
    return 1;
  return 0;
}
#endif /* COVARIANT_SUPPORT */

/*---------------------------------------------------------------------------
  MAJOR DISCOURSE MODE ON:  How to add a new stub pattern in analyz_stub

  From time to time, you will encounter the need to recognize a new code
  sequence.  A nasty problem.  Here is what to do.
    o Make a minimanl prorgram mystub.c, e.g. return 9;  
    o Compile it with the -S option of cc to make mystub.s: cc -S mystub.c
    o Edit mystub.s to have your code sequence starting after a label
      to force it on a bundle boundary.
    o You will want to make two versions of mystub.c, one with all of the
      variable bits (usually immediates) 0 and one with all of the
      variable bits 1.  Make two a.out files, mystub1 and mystub2.
      Using gdb, examine the stub code, from the differences, you
      can determine a mask to mask out the variable bits.  When the
      variable bits are masked out what is left is a static pattern for
      each word.  The mask and matching bit patterns are used to examine
      a stub word, e.g.
	  && (start_stub_p[1] & 0x00fcffff) == 0x00240041
				^^^^^^^^^^     ^^^^^^^^^^
				mask           match
      use something like:
	disassemble main
	x /8i  ADDRESS
	x /12x ADDRESS

	For a given word, if VER0 is the word with the variable bits off and
 	VER1 is the word with the variable bits on, then the invariant 
	patter is given by VER0 and the mask is given by 
		p /x ~(~VER0 & VER1)

  ---------------------------------------------------------------------------
 */

/* analyze_stub - Return 0 if the pc value passed in is not in
   a stub of some sort.  Otherwise, return the target pc of the
   trampoline and set various return variables:

   *stub_kind_p - what kind of trampoline is it
   *start_addr_p        - the starting address of the trampoline
   *limit_addr_p        - the first address past the end of the
 */

CORE_ADDR
analyze_stub (CORE_ADDR pc,
	      stub_type * stub_kind_p,
	      CORE_ADDR * start_addr_p,
	      CORE_ADDR * limit_addr_p)
{
  CORE_ADDR dp_value;
  int idx;
  int inst_a[(2 * 4 * MAX_STUB_BUNDLES) - 1];
  CORE_ADDR read_addr;
  int retry_count;
  int *start_stub_p;
  int stub_size = 0;
  struct minimal_symbol *stub_sym_p;
  int target_dp_offset;
  CORE_ADDR target_pc = 0;
  struct unwind_table_entry *unwind_p;
  int is_cxx_stub;

  /* INSTRUCTION_SIZE is the size of a bundle */
  assert ((sizeof (int) * 4) == INSTRUCTION_SIZE);
  *stub_kind_p = STUB_NONE;
  *start_addr_p = pc & ~0x3;
  *limit_addr_p = pc & ~0x3;

  /* Get the unwind descriptor corresponding to PC, return zero if
     an unwind descriptor was found because linker stubs don't have unwind
     entries in IA64.
   */

  /* One more sanity check, there should be a ".stub" symbol here. 
     There is also a ".text" symbol at the same place.
   */

  stub_sym_p = lookup_minimal_symbol_by_pc (pc);
  if (!stub_sym_p)
    return 0;

  is_cxx_stub = is_cxx_stub_symbol (SYMBOL_NAME (stub_sym_p));
  unwind_p = find_unwind_entry (pc);
  if (unwind_p && ! is_cxx_stub)
    return 0;

  if (   strcmp (SYMBOL_NAME (stub_sym_p), ".stub") != 0
      && strcmp (SYMBOL_NAME (stub_sym_p), ".text") != 0
      && strcmp (SYMBOL_NAME (stub_sym_p), ".bortext") != 0
      && ! is_cxx_stub
      && (   !IS_TARGET_LRE
	  || (   IS_TARGET_LRE 
	      && strcmp (SYMBOL_NAME (stub_sym_p), "_init") != 0)))
    return 0;


  /* We check for a stub starting at the address passed in, then
     we repeat the check starting at the preceding MAX_STUB_INST -1
     instructions.  We first read in MAX_STUB_INST instructions into
     the inst_a array starting at index MAX_STUB_INST -1.  For each
     repetition, we read in one more preceeding instruction.
   */
  start_stub_p = &inst_a[MAX_STUB_BUNDLES * 4 - 1];
  /* RM: start at current bundle */
  read_addr = pc & ~0x3;
  target_read_memory (read_addr, 
		      (char*) start_stub_p, 
		      MAX_STUB_BUNDLES * 16);

  /* We need to check for the stub starting at the initial location or
     the preceeding MAX_STUB_INST -1 instructions.
   */

  for (retry_count = 0; retry_count < MAX_STUB_BUNDLES; retry_count++)
    {
      /* Is this a shared library import stub?
         addl r15 = @pltoff(__gp), gp;;  // PLTOFF16F
         ld8  r16 = [r15], 8;;
         ld8  gp = [r15];;
         mov  b6 = r16;;
         br   b6;;
       */

      if ((start_stub_p[0] & 0xffff0306) == 0x0b780002
	  && (start_stub_p[1] & 0x00fcffff) == 0x00240041
	  && start_stub_p[2] == 0x3c302800
	  && start_stub_p[3] == 0x00000400
	  && start_stub_p[4] == 0x0b08001e
	  && start_stub_p[5] == 0x18100000
	  && start_stub_p[6] == 0x000200c0
	  && start_stub_p[7] == 0x00090007
	  && start_stub_p[8] == 0x1d000000
	  && start_stub_p[9] == 0x01000000
	  && start_stub_p[10] == 0x00020000
	  && start_stub_p[11] == 0x60008000)
	{
	  target_dp_offset = (int) extract_im22 (slot0_contents ((unsigned char *) start_stub_p));
	  dp_value = read_register (GR0_REGNUM + 1);
	  target_pc = read_memory_unsigned_integer (dp_value + target_dp_offset,
						    sizeof (CORE_ADDR));
	  stub_size = 3 * INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_SOLIB_CALL;
	  break;
	}			/* end if shared library import stub */

      /* In the May 2000 2.6 RAT Roll, a shared library import stub mutated
	 to (2 bundles):
	  addl             r15=-40,r1;;		(-40 will vary)
	  ld8              r16=[r15],8
	  adds             r14=0,r1;;
	  ld8              r1=[r15]
	   mov.few.dc.dc    b6=r16,0x1d51	(where 0x1d51 is "." so this
						 instruction is invariant)
	  br.cond.sptk.few b6;;
	  */

      if (   (start_stub_p[0] & 0xffff0306) == 0x0b780002
	  && (start_stub_p[1] & 0x00fcffff) == 0x00240041
	  && start_stub_p[2] == 0x3c3028c0
	  && start_stub_p[3] == 0x01080084
	  && start_stub_p[4] == 0x1108001e
	  && start_stub_p[5] == 0x18106080
	  && start_stub_p[6] == 0x04800300
	  && start_stub_p[7] == 0x60008000)
	{
	  target_dp_offset = (int) extract_im22 (slot0_contents ((unsigned char *) start_stub_p));
	  dp_value = read_register (GR0_REGNUM + 1);
	  target_pc = read_memory_unsigned_integer (dp_value 
						    + target_dp_offset ,
						    sizeof (CORE_ADDR));
	  stub_size = 2 * INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_SOLIB_CALL;
	  break;
	}			/* end if shared library import stub */

      /* 8/1/03 LRE introduced a new import stub:
	    addl             r15=xxx,r1;;
	    ld8              r16=[r15],8
	    mov              r14=r1;;

	    ld8              r1=[r15]
	    mov              b6=r16
	    br               b6;;
       */


      if (   (start_stub_p[0] & 0xffff0306) == 0x0b780002
	  && (start_stub_p[1] & 0x00fcffff) == 0x00240041
	  && start_stub_p[2] == 0x3c302800
	  && start_stub_p[3] == 0x00000400

	  && start_stub_p[4] == 0x1108001e
	  && start_stub_p[5] == 0x18106080
	  && start_stub_p[6] == 0x04800300
	  && start_stub_p[7] == 0x60008000)
	{
	  target_dp_offset = (int) extract_im22 (slot0_contents ((unsigned char *) start_stub_p));
	  dp_value = read_register (GR0_REGNUM + 1);
	  target_pc = read_memory_unsigned_integer (dp_value + target_dp_offset,
						    sizeof (CORE_ADDR));
	  stub_size = 2 * INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_SOLIB_CALL;
	  break;
	}			/* end if shared library import stub */

      /* JAGaf14047 gdb stepping into shlibs broken by linker import stub
         change.  Like the previous stub, but the ld8 instruction has
	 a .acq modifier:

		addl             r15=xxxx,r1;;
		ld8.acq          r16=[r15],8
		mov              r14=r1;;

		ld8              r1=[r15]
		mov              b6=r16
		br               b6;;


				(only diff      (only non 
				 shown)		 -1 shown)
		VER0		VER1		MASK
		0x0b780002	0x0b78fcfb	0xffff0306
		0x00240041      0xff270041	0xfcffff
		0x3c7029c0	
		0x01080084

		0x1108001e      
		0x18106080      
		0x04800300
		0x60008000
	 */

      if (   (start_stub_p[0] & 0xffff0306) == 0x0b780002
	  && (start_stub_p[1] & 0xfcffff) == 0x00240041
	  && start_stub_p[2] == 0x3c7029c0
	  && start_stub_p[3] == 0x01080084

	  && start_stub_p[4] == 0x1108001e
	  && start_stub_p[5] == 0x18106080
	  && start_stub_p[6] == 0x04800300
	  && start_stub_p[7] == 0x60008000)
	{
	  target_dp_offset = (int) extract_im22 (slot0_contents ((unsigned char *) start_stub_p));
	  dp_value = read_register (GR0_REGNUM + 1);
	  target_pc = read_memory_unsigned_integer (dp_value + target_dp_offset,
						    sizeof (CORE_ADDR));
	  stub_size = 2 * INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_SOLIB_CALL;
	  break;
	}			/* end if shared library import stub */

      /* A long branch stub on IA64 is 3 bundles long. e.g:
         movl r15 = target - (address of "mov r16 = ip" instruction) ;;
         mov  r16 = ip ;;
         add  r16 = r16, r15;;
         mov  b6 = r16;;
         br   b6
       */

      if (start_stub_p[0] == 0x04000000
	  && (start_stub_p[1] & 0xff3f0000) == 0x01000000
	  && (start_stub_p[2] & 0x000080ff) == 0x000000e0
	  && (start_stub_p[3] & 0x0f0000f0) == 0x01000060
	  && start_stub_p[4] == 0x03000000
	  && start_stub_p[5] == 0x01000001
	  && start_stub_p[6] == 0x00600000
	  && start_stub_p[7] == 0x02790080
	  && start_stub_p[8] == 0x10000000
	  && start_stub_p[9] == 0x01006080
	  && start_stub_p[10] == 0x04800300
	  && start_stub_p[11] == 0x60008002)
	{
	  target_pc = *start_addr_p + 16 +
	    +extract_im64 (slot2_contents ((unsigned char *) start_stub_p),
			   slot1_contents ((unsigned char *) start_stub_p));
	  stub_size = 3 * INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_LONG_CALL;
	  break;
	}			/* end if shared library import stub */

      /* Need to check for this kind of stub (.bortext) too. 
	 It is 1 bundle long.
	 
	  addl             r15=1,r1;;		(1 will vary)
	  nop
	  br.cond.sptk.few 0x4001402;;          (0x4001402 will vary)
	  
	  For the add(immediate 22 bits)
	  For the pattern 1c78000 
	  The bits marked with ^ have an explanation below them as
	  to what they are e.g the f is part of r1.

	  1cf  8 f 11  11  f 1  00  1      ff2 01   11
       	    ^    ^ ^^  ^^  ^ ^  ^^  ^      ^^   ^   ^^ 
	    r1   imm   r1  imm  r3  imm    imm      imm  
	    
	 imm has 22 bits, r1 - 7 bits, r3 - 2 bits r0 - r3

	 Extract the 21 bit immediate in the Branch instruction.
	 Is an IP relative branch. 
	    
        */

      if ((  (start_stub_p[0] & 0xffff03fe) == 0x1c780002)
	  && (start_stub_p[1] & 0x00fcffff) == 0x240000
	  && (start_stub_p[2]) == 0x20000 
	  && (start_stub_p[3] & 0x0f0000f7) == 0x00000040)
	{
	  int rem = 0;
	  target_pc = pc + 
	    extract_im21 (slot2_contents ((unsigned char *) start_stub_p));
	  /* The target_pc here should be bundle aligned since the 
	     stub is bundle aligned hence pc is bundle aligned and
	     since we shifted the imm by 4 that is bundle aligned.
	   */
	  stub_size = INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_SOLIB_CALL;
	  break;
	}			/* end of shared library import stub */

      /* Need to check for this kind of stub too. It is 1 bundle long.
          addl             r?=?,r?;;           (? will vary)
          nop
          br.cond.sptk.few 0x4001402;;          (0x4001402 will vary)

          For the add(immediate 22 bits). We also get a 1d78 and a 1c78
          in start_stub_p[0] because the bits in the 5 bit template
          differ and then the byte gets flipped for endian issues.
          */

      if (( (start_stub_p[0] & 0xffff03fe) == 0x1d780000)
	  && (start_stub_p[1] & 0x00fcffff) == 0x240000
	  && (start_stub_p[2]) == 0x20000 
	  && (start_stub_p[3] & 0x0f0000ff) == 0x00000040)
	{
	  int rem = 0;
	  target_pc = pc + 
		extract_im21 (slot2_contents ((unsigned char *) start_stub_p));
	  /* The target_pc here should be bundle aligned since the 
	     stub is bundle aligned hence pc is bundle aligned and
	     since we shifted the imm by 4 that is bundle aligned.
	   */
	  stub_size = INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_SOLIB_CALL;
	  break;
	}			/* end of shared library import stub */

      /* ACC6 adjuster thunk, similar to previous stub:
		adds             r32=??,r32
		nop.m            0x0
		br.cond.dptk.few ???;;
	 */


      if (   (start_stub_p[0] & 0xffff03fe) == 0x19000140
	  && (start_stub_p[1] & 0xc0fdffff) == 0x00210000
	  && (start_stub_p[2]) == 0x20000 
	  && (start_stub_p[3] & 0x0f0000f7) == 0x00000042)
	{
	  int rem = 0;
	  target_pc =  *start_addr_p + 
		extract_im21 (slot2_contents ((unsigned char *) start_stub_p));
	  /* The target_pc here should be bundle aligned since the 
	     stub is bundle aligned hence pc is bundle aligned and
	     since we shifted the imm by 4 that is bundle aligned.
	   */
	  stub_size = INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_LONG_CALL;
	  break;
	}			/* end of shared library import stub */


      /* ACC6 adjuster thunk, 64-bit immediate:
		nop.m            0x0
		movl             r2=0xffffffffffffb1d0;;

		add              r32=r2,r32
		nop.m            0x0
		br.cond.dptk.few C2::f()+0;;
	 */


      if (   (start_stub_p[0]) == 0x05000000 
	  && (start_stub_p[1] & 0xff3f0000) == 0x01000000  
	  && (start_stub_p[2] & 0x80ff) == 0x00000040
	  && (start_stub_p[3] & 0xf0800f0) == 0x00000060
	  && (start_stub_p[4] ) == 0x19000940
	  && (start_stub_p[5] ) == 0x00200000
	  && (start_stub_p[6] ) == 0x00020000 
	  && (start_stub_p[7] & 0xf0000f7) == 0x00000042)
	{
	  target_pc = *start_addr_p + 16 +
	    extract_im21 (slot2_contents ((unsigned char *) start_stub_p + 16));
	  stub_size = 2 * INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_LONG_CALL;
	  break;
	}			/* end of shared library import stub */

      /* Need to check for brl stub. It is 1 bundle long.
	  nop.m             0
	  brl.cond.sptk.few 0x5313532;;          (0x5313532 will vary)
	  The target address is obtained by getting the 60 bit
	  immediate, shifting it 4 bits left,  and adding it to the 
	  current IP. (See IA64 Architecture Software Developer's Manual)
	  */

      if (   (start_stub_p[0]) == 0x05000000
	  && (start_stub_p[1] & 0x01000000) == 0x01000000
	  && (start_stub_p[3] & 0x000000c0) == 0x000000c0)
	{
	  target_pc = *start_addr_p +
	  	(extract_im60 ((unsigned char *) start_stub_p) << 4);
	  stub_size = INSTRUCTION_SIZE;
	  *stub_kind_p = STUB_BRL;
	  break;
	}			/* end of brl stub */
	
#ifdef COVARIANT_SUPPORT
      if (is_pc_in_covariant_thunk (pc))
	  return find_cov_thunk_next_pc (pc);
#endif /* COVARIANT_SUPPORT */

      /* Prepare for the next iteration if there is one */

      if (retry_count < MAX_STUB_BUNDLES - 1)
	{
	  start_stub_p -= 4;
	  *start_addr_p = *start_addr_p - INSTRUCTION_SIZE;
	  for (idx = 0; idx < 4; idx++)
	    {
	      start_stub_p[idx] = (int)
		read_memory_unsigned_integer (*start_addr_p + 4 * idx,
					      sizeof (int));
	    }
	}			/* if there is another iteration */
    }				/* For each possible stub starting instruction */

  /* did we find a stub? */

  if (retry_count >= MAX_STUB_BUNDLES || *start_addr_p + stub_size < pc)
    {				/* No, we did not find a stub */
      *start_addr_p = pc;
      return 0;
    }

  /* Yes, we found a stub */

  *limit_addr_p = *start_addr_p + stub_size;
  return target_pc;
}				/* end analyze_stub */

#ifdef COVARIANT_SUPPORT
/*----------------------------------------------------------------------------*/
/* Function	: find_cov_thunk_next_pc() 
 * Description	: For the covariant thunk associated with a given PC address,
 *		    this function finds the target pc, which is either the 
 *		    callee's address or the caller's return address depending 
 *		    upon which branch instruction the PC is pointing to.
 * Inputs	: A PC address.
 * Outputs	: Target pc to which the covariant thunk will proceed next.  
 *		    If the control is in the call path, this will be address of
 *		    the callee.  If the control is in the return path, this will
 *		    be the address of caller's return site.  If the PC isn't 
 *		    pointing to a branch instruction then a 0 is returned.
 * Globals	:
 * Notes	: Assumes that PC is in a covariant thunk, so prior to calling
 *		    this function, is_pc_in_covariant_thunk() should be called
 *		    to accertain that the PC is inside a covariant thunk.
 *		    Results are unspecified otherwise.
 */
static CORE_ADDR
find_cov_thunk_next_pc (CORE_ADDR pc)
{
  int i, template, breg;
  int slot_no = (int) (pc & 3);
  unsigned long long slot[3];
  char bundle[INSTRUCTION_SIZE];
  CORE_ADDR start_addr = pc & ~0x3ULL;
  CORE_ADDR target_pc;

  assert ((sizeof (int) * 4) == INSTRUCTION_SIZE);

  target_read_memory (start_addr, bundle, INSTRUCTION_SIZE);

  template = bundle[0] & 0x1f;
  slot[0] = slot0_contents ((unsigned char *) bundle);
  slot[1] = slot1_contents ((unsigned char *) bundle);
  slot[2] = slot2_contents ((unsigned char *) bundle);

  /* To skip the thunk and proceed to the callee/caller the target pc must be 
   * found.  That is done by decoding the branch that the pc is pointing to 
   * (can be in any slot, pc will tell which one).  If the pc is pointing to a
   * branch, the target pc is found be decoding the branch instruction.  
   * Otherwise 0 is returned and in handle_inferior_event(), the dynamic 
   * trampoline mechanism will be activated to skip instructions in the
   * covariant thunk till a branch is reached after which this function will 
   * be invoked again to get the target PC.
   */
  if (is_ip_call_br (slot[slot_no], template, slot_no))
    target_pc = pc + extract_im21 (slot[slot_no]);
  else if (is_ind_call_br (slot[slot_no], template, slot_no))
    {
      /* get branch register number & read it from the inferior. */
      breg = (int) ((slot[slot_no] >> 13) & 7);
      target_pc = read_register (BR0_REGNUM + breg);
    }
  else if (is_ind_ret_br (slot[slot_no], template, slot_no))
    /* get rp from the inferior */
    target_pc = read_register (BR0_REGNUM);
  else	/* pc wasn't pointing to a branch instruction. */
    target_pc = 0;

  return target_pc;
}

/*----------------------------------------------------------------------------*/
/* Function	: is_pc_in_covariant_thunK_return_path() 
 * Description	: For the covariant thunk associated with a given PC address,
 *		    this function finds if the control flow in the thunk is the
 *		    return path from the callee to the caller.
 * Inputs	: A PC address.
 * Outputs	: 1 if pc indicates that the control flow is in the return path
 *		    of the thunk back to the caller, 0 otherwise.
 * Globals	:
 * Notes	: Scans through all the instructions starting from the given pc
 *		    address till it finds a br.ret.  If it finds a br.call 
 *		    before that, then it returns 0, 1 otherwise.
 */
int
is_pc_in_covariant_thunk_return_path (CORE_ADDR pc)
{
  int i, template;
  unsigned long long slot[3];
  char bundle[INSTRUCTION_SIZE];
  CORE_ADDR start_addr = pc & ~0x3ULL;
  CORE_ADDR target_pc;

  while (IS_PC_IN_COVARIANT_THUNK (pc))
    {
      assert ((sizeof (int) * 4) == INSTRUCTION_SIZE);

      target_read_memory (start_addr, bundle, INSTRUCTION_SIZE);

      template = bundle[0] & 0x1f;
      slot[0] = slot0_contents ((unsigned char *) bundle);
      slot[1] = slot1_contents ((unsigned char *) bundle);
      slot[2] = slot2_contents ((unsigned char *) bundle);

      for (i = 0; i < 3; i++)
        {
	  if (is_ip_call_br (slot[i], template, i) ||
	      is_ind_call_br (slot[i], template, i))
	    return 0;
	  if (is_ind_ret_br (slot[i], template, i))
	    return 1;
        }

      start_addr += INSTRUCTION_SIZE;
    }
  return 0;
}
#endif /* COVARIANT_SUPPORT */

/* Return one if PC is in the call path of a trampoline, else return zero.

   Note we return one for *any* call trampoline (long-call, arg-reloc), not
   just shared library trampolines (import, export).  */
int
in_solib_call_trampoline (CORE_ADDR pc, char *name)
{
  CORE_ADDR limit_addr;
  CORE_ADDR start_addr;
  stub_type stub_kind;
  CORE_ADDR stub_target;

  stub_target = analyze_stub (pc, &stub_kind, &start_addr, &limit_addr);
  return (stub_target != 0);

}				/* end in_solib_call_trampoline (PA64 definition) */

/* Return one if PC is in the return path of a trampoline, else return zero.

   Note we return one for *any* call trampoline (long-call, arg-reloc), not
   just shared library trampolines (import, export).  */

int
in_solib_return_trampoline (CORE_ADDR pc, char *name)
{
  /* For now, just return zero.  */
  return 0;
}

/* Figure out if PC is in a trampoline, and if so find out where
   the trampoline will jump to.  If not in a trampoline, return zero.

   Simple code examination probably is not a good idea since the code
   sequences in trampolines can also appear in user code.
 */

/* skip_trampoline_code - PA64 definition.  See comment block above for
   general comments.  Return 0 if we are not in a trampoline, otherwise
   return the target pc address.

   We look for the following kinds of stubs:
   Shared library import stub:
   addl r15 = @pltoff(__gp), gp;;  // PLTOFF16F
   ld8 r16 = [r15], 8;;
   ld8 gp = [r15];;
   mov b6 = r16;;
   br b6;;

   Long branch stubs:
   movl r15 = target - (address of "mov r16 = ip" instruction) ;;
   mov  r16 = ip ;;
   add     r16 = r16, r15;;
   mov  b6 = r16;;
   br   b6
 */

CORE_ADDR
skip_trampoline_code (CORE_ADDR pc, char *name)
{
  CORE_ADDR limit_addr;
  CORE_ADDR start_addr;
  stub_type stub_kind;

  return analyze_stub (pc, &stub_kind, &start_addr, &limit_addr);
}


/* Bindu 041205
   This function return the end of prologue for a block containing the
   PC in the given unwind table entry U's uniwnd_info_block. Note that
   a single unwind info block can contain multiple prologue and body
   regions. */
/* The 'last_prologue' parameter is just to doubly ensure that the caller 
   wants the prologue_end of the last prologue region.
   See frameless_function_invocation (). */ 
/* Returns either the address of the end of the prologue code, or zero if
   it can't decide where the unwind table's idea of the prologue code ends. */

CORE_ADDR
get_prologue_end (struct unwind_table_entry *u, CORE_ADDR pc, int last_prologue)
{
  unsigned char *unwind_info_block = (unsigned char*) u->info_block;
  CORE_ADDR next_addr = u->region_start;
  int scale_factor = u->scale_factor;
  CORE_ADDR prologue_end = 0, body_end;

  int header_P = 0, n, r, prologue_size = 0;
  unsigned char *unwind_desc_p = unwind_info_block + sizeof (CORE_ADDR);
  unsigned char *end_desc_p;
  CORE_ADDR vfu;

  /* Read first double word containing the VFU field */

  vfu = ( ( ((CORE_ADDR) *(uint32_t *)(void *) (unwind_info_block)) << 32) |
          ((CORE_ADDR)  *(uint32_t *)(void *) (unwind_info_block + 4) ) );
  
  end_desc_p = unwind_desc_p + (UNW_LENGTH (vfu) * scale_factor);

  /* Now go through the Unwind descriptor records ... */
  while (unwind_desc_p < end_desc_p)
    {
      /* format R1 */
      if (((*unwind_desc_p) & 0xc0) == 0)
	{
	  int rlen = (*unwind_desc_p) & 0x1f;
	  int slot_no = (int)(next_addr & 0xF);
	  header_P = (*unwind_desc_p & 0x20) == 0;
	  if (header_P)
	    {
	      prologue_size = rlen;
	      prologue_end = (next_addr & ~0xFLL) + (rlen + slot_no) / 3 * 16 + (rlen + slot_no) % 3;
	      next_addr = prologue_end;
	    }
	  else
	    {
	      body_end = (next_addr & ~0xFLL) + (rlen + slot_no) / 3 * 16 + (rlen + slot_no) % 3;
	      next_addr = body_end;
	      if (pc < body_end)
		return prologue_end;
	    }
	  unwind_desc_p++;
	  continue;
	}
      /* format R2 */
      if ( ( (*unwind_desc_p) & 0xf8) == 0x40)
	{
	  int rlen = read_unsigned_leb128((char*) unwind_desc_p+2, &n);
	  int slot_no = (int) (next_addr & 0xF);
	  header_P = 1;	/* R2 is always a header */
	  prologue_size = rlen;
	  prologue_end = (next_addr & ~0xFLL) + (rlen + slot_no) / 3 * 16 + (rlen + slot_no) % 3;
          next_addr = prologue_end;
	  unwind_desc_p += n + 2;
	  continue;
	}
      /* format R3 */
      if ( ( (*unwind_desc_p) & 0xfe) == 0x60)
	{
	  int rlen = read_unsigned_leb128((char*) unwind_desc_p+1, &n);
	  int slot_no = (int) (next_addr & 0xF);
	  header_P = (*unwind_desc_p & 0x03) == 0;
	  if (header_P)
	    {
	      prologue_size = rlen;
	      prologue_end = (next_addr & ~0xFLL) + (rlen + slot_no) / 3 * 16 + (rlen + slot_no) % 3;
              next_addr = prologue_end;
	    }
	  else
	    {
	      body_end = (next_addr & ~0xFLL) + (rlen + slot_no) / 3 * 16 + (rlen + slot_no) % 3;
              next_addr = body_end;
              if (pc < body_end)
                return prologue_end;
	    }

	  unwind_desc_p += n + 1;
	  continue;
	}
      /* format B1 */
      if ( ( ( (*unwind_desc_p) & 0xc0) == 0x80) && !header_P)
	{
	  unwind_desc_p += 1;
	  continue;
	}
      /* format P1 */
      if ( ( (*unwind_desc_p) & 0xe0) == 0x80)
	{
	  unwind_desc_p += 1;
	  continue;
	}
      /* format P2 */
      if (((*unwind_desc_p) & 0xf0) == 0xa0)
	{
	  unwind_desc_p += 2;
	  continue;
	}
      /* format P3 */
      if (((*unwind_desc_p) & 0xf8) == 0xb0)
	{
	  unwind_desc_p += 2;
	  continue;
	}
      /* format P4 */
      if ((*unwind_desc_p) == 0xb8)
	{
	  int imask_size = (prologue_size+3)/4;
	  unwind_desc_p += 1;
	  unwind_desc_p += imask_size;
	  continue;
	}
      /* format P5 */
      if ((*unwind_desc_p) == 0xb9)
	{
	  unwind_desc_p += 4;
	  continue;
	}
      /* format P6 or B2 */
      if (((*unwind_desc_p) & 0xe0) == 0xc0)
	{
	  unwind_desc_p++;
	  if (!header_P)
	    {
	      read_unsigned_leb128((char*) unwind_desc_p, &n);
	      unwind_desc_p += n;
	    }
	  continue;
	}
      /* format P7 or B3 */
      if (((*unwind_desc_p) & 0xf0) == 0xe0)
	{
	  r = (*unwind_desc_p) & 0x0f;
	  read_unsigned_leb128((char*) unwind_desc_p+1, &n);
	  unwind_desc_p += 1 + n;
	  if (!header_P || (r == 0))
	    {
	      read_unsigned_leb128((char*) unwind_desc_p, &n);
	      unwind_desc_p += n;
	    }
	  continue;
	}
      /* format P8 or B4 */
      if ((*unwind_desc_p) == 0xf0)
	{
	  unwind_desc_p += ( (header_P) ? 2 : 1 );
	  read_unsigned_leb128((char*) unwind_desc_p, &n);
	  unwind_desc_p += n;
	  continue;
	}
      /* format P9 */
      if ((*unwind_desc_p) == 0xf1)
	{
	  unwind_desc_p += 3;
	  continue;
	}
      /* format P10 */
      if ((*unwind_desc_p) == 0xff)
	{
	  unwind_desc_p += 3;
	  continue;
	}

      /* Unknown format - if we get to the bottom of this loop we're about
	 to go into an infinite loop, since only known opcodes result in
	 incrementing "unwind_desc_p".  Just return zero and let the caller
	 handle it.  Carl Burch, 05/11/2007 */
      return (CORE_ADDR) 0;
    }
  if (last_prologue)
    return prologue_end;
  else
    return (CORE_ADDR) 0;
}

static CORE_ADDR
skip_prologue_hard_way (CORE_ADDR pc)
{
  struct unwind_table_entry *u;
  CORE_ADDR prologue_end = 0;

  u = find_unwind_entry (pc);
  if (!u)
    return pc;

  prologue_end = get_prologue_end (u, pc, 0);
  if (prologue_end == 0)
    return pc;
  if (pc >= prologue_end)
    return pc;
  return prologue_end;
}

#ifdef COVARIANT_SUPPORT
/*----------------------------------------------------------------------------*/
/* Function	: dynamic_trampoline_nextpc ()
 * Description	: Skips trampoline code (stubs, thunks, etc.) till a branch
 *		    instruction is reached in the trampoline and returns the 
 *		    address of this branch instruction.
 * Inputs	: A PC address.
 * Outputs	: Address of the first branch instruction inside the thunk.
 * Globals	:
 * Notes	: Some trampoline code can't be decoded statically to 
 *		    determine its target pc.  In those cases a dynamic 
 *		    mechanism is needed, which will execute instructions 
 *		    in the trampoline one at a time till it reaches a 
 *		    branch instruction in the trampoline code and then 
 *		    decodes the branch to find the target pc.  For IA64, 
 *		    there has been no need for such a dynamic trampoline 
 *		    skipping feature till now; covariant return thunks 
 *		    can't be handled using the static mechanism because 
 *		    the location of the branch inside the thunk and the 
 *		    number of instructions in the thunk are variable.  
 *		    Hence the need for dynamic trampoline. 
 */
CORE_ADDR
dynamic_trampoline_nextpc (CORE_ADDR pc)
{
  int i, template;
  unsigned long long slot[3];
  char bundle[INSTRUCTION_SIZE];
  CORE_ADDR start_addr = pc & ~0x3ULL;

  assert ((sizeof (int) * 4) == INSTRUCTION_SIZE);

  /* Go through the thunk and find out the location of the branch from the 
   * given pc.
   */
  while (is_pc_in_covariant_thunk (pc))
    {
      target_read_memory (start_addr, bundle, INSTRUCTION_SIZE);

      template = bundle[0] & 0x1f;
      slot[0] = slot0_contents ((unsigned char *) bundle);
      slot[1] = slot1_contents ((unsigned char *) bundle);
      slot[2] = slot2_contents ((unsigned char *) bundle);

      for (i = 0; i < 3; i++)
	{
	  if (is_ip_call_br (slot[i], template, i) ||
	      is_ind_call_br (slot[i], template, i) ||
	      is_ind_ret_br (slot[i], template, i))

	    /* Address with slot # of br inst; slot is needed because it won't
	     * be always at the end of the bundle.  This is due to the fact 
	     * that a covariant thunk is in reality a function and can be 
	     * optimized.
	     */
	    return start_addr | i;
	}
      start_addr += INSTRUCTION_SIZE;
    }

  /* If we reach here, it means that we ended up outside the thunk and
   * didn't find a branch. Not finding a branch in a covariant thunk
   * points to a unknown problem or an unknown trampoline code.
   */
  return 0;	
}
#endif /* COVARIANT_SUPPORT */

/* Called with what we believe is a function start using
minimal symbol table and uses unwind information to find
true function start */
CORE_ADDR
handle_label (CORE_ADDR pc)
{
  struct unwind_table_entry *u;

  u = find_unwind_entry (pc);
  if (!u)
    return pc;

  if ((pc > u->region_start) && (pc <= u->region_end))	/* found a label? */
    return u->region_start;

  return pc;
}

/* return 0 if we cannot determine the end of the prologue,
   return the new pc value if we know where the prologue ends */
/* Used only for gcc currently. */

static CORE_ADDR
after_prologue (CORE_ADDR pc)
{
  struct symtab_and_line sal;
  CORE_ADDR func_addr, func_end;
  CORE_ADDR start_pc;

  if (!find_pc_partial_function (pc, NULL, &func_addr, &func_end))
    return 0;			/* Unknown */

  sal = find_pc_line (func_addr, 0);

  /* RM: Didn't find the line? */
  if (sal.line == 0)
    return 0;

  /* Check if the logical line returned isn't really the entry code, in case
     there actually isn't any entry code and skipping the first logical line
     will make us set the breakpoint at the second line of the user code.  */
  if (find_exact_line_pc (sal.symtab, (sal.line - 1), &start_pc))
    {
      /* The previous line number does exist in this symbol table. */
      if (start_pc == sal.pc)
	{
	  /* A logical line before that of the "first" exists and 
	     contains no code. The prologue and all associated cruft
	     instructions in this logical line have been optimized away. */
          return pc;	/* no adjustment to be made */
	}
    }

  if (sal.end < func_end)
    {
      return sal.end;		/* this is the end of the prologue */
    }
  /* The line after the prologue is after the end of the function.  In this
     case, put the end of the prologue is the beginning of the function.  */
  /* This should happen only when the function is prologueless and has no
     code in it. For instance void dumb(){} Note: this kind of function
     is  used quite a lot in the test system */

  else
    return pc;			/* no adjustment will be made */
}

#if 0	/* Bindu, 04/19/2005 */
static int
ia64_pc_in_gcc_function (pc)
     CORE_ADDR pc;
{
  struct symbol *func_sym;

  func_sym = find_pc_function (pc);
  if (func_sym
      && BLOCK_GCC_COMPILED (SYMBOL_BLOCK_VALUE (func_sym))
      != 0)
    return 1;
  return 0;
}
#endif

/* To skip prologues, I use this predicate.  Returns either PC itself
   if the code at PC does not look like a function prologue; otherwise
   returns an address that (if we're lucky) follows the prologue.
 */
CORE_ADDR
skip_prologue (CORE_ADDR pc)
{
  struct unwind_table_entry *u;
  CORE_ADDR post_prologue_pc;

  u = find_unwind_entry (pc);

  /* 0 length prologue, just return PC */
  if (u && u->region_start == u->prologue_end )
    {
      return pc;
    }

  /* See if we can determine the end of the prologue via the symbol table.
     If so, then return either PC, or the PC after the prologue, whichever
     is greater.  */
  post_prologue_pc = after_prologue (pc);

  if (post_prologue_pc != 0)
    return max (pc, post_prologue_pc);
  /* Can't determine prologue from the symbol table */
  return (skip_prologue_hard_way (pc));
}

int
hpread_adjust_stack_address (CORE_ADDR func_addr)
{
  return 0;
}

/* A frameless invocation is one that has no memory stack and
   no register stack allocated to it. The current SP should be 
   same as the SP of the caller frame (no memory stack).
   There should not be any register stack allocations in all
   of the prologue regions. 
   A 'prologue' by definition should be the region where register
   or memory stacks are established and key registers are saved, 
   and if we have no such region, we can term the frame as frameless.
   A procedure can start as a frameless invocation and later allocate 
   register/memory stack as needed, so we need to scan through all of 
   the different prologue regions. We call get_prologue_end () with 
   the last PC in the procedure just to ensure we get the prologue_end
   of the last prologue region in the frame. 
   For a frame that does not start as a frameless invocation but later
   allocates reg/mem stack, we can still say that it is not a frameless
   invocation as it is sure to be not so at some point of time, or else
   if it is really a necessity check for sol from CFM. 
   JAGag37230 -mithun
*/
int
frameless_function_invocation (struct frame_info *frame)
{
  /* RM: Just look for a prologue in this function. If there isn't
     any, assume it isn't frameless */
  CORE_ADDR func_start, after_prologue;
  struct unwind_table_entry *u = 0;
  u = find_unwind_entry (read_register (PC_REGNUM));
  func_start = get_pc_function_start (frame->pc);
  if (func_start)
    {
      func_start = handle_label (func_start);
      /* The region_end in an unwind descriptor accounts for the 
         next PC after the last PC of the frame. */
      after_prologue = get_prologue_end (u, 
                       (u->region_end % 16) ? u->region_end - 1 
                                            : u->region_end - 16 + 2, 1);
      return after_prologue == func_start;
    }
  return 0;
}

CORE_ADDR
ia64_fix_call_dummy (char *dummy, CORE_ADDR pc, CORE_ADDR fun,
		     int nargs, value_ptr *args, struct type *type, 
		     int gcc_p)
{
  struct objfile *objfile;
  struct obj_section *funcsect;

  fun = swizzle (fun);  /* Because pointers are 4 bytes, we often get
			   unswizzled addresses for functions. */
  /* RM: fun could be an OPD pointer */
  funcsect = find_pc_section (fun);
  if (funcsect &&
      STREQ (funcsect->the_bfd_section->name, ".opd"))
    {
      fun = (CORE_ADDR) read_memory_integer (fun, sizeof (CORE_ADDR));
    }

  /* RM: set up DP register */
  ALL_OBJFILES (objfile)
  {
    /* RM: Are we in the right objfile? */
    struct obj_section *s;
    if (objfile->is_mixed_mode_pa_lib || objfile_is_mixed_mode (objfile))
      {
        continue;
      }

    /* Skip the separate debug object , look for the actual one. */
    if (objfile->separate_debug_objfile_backlink)
        continue;

    for (s = objfile->sections; s < objfile->sections_end; ++s)
      if (s->addr <= fun
	  && fun <= s->endaddr)
	break;

    if (s < objfile->sections_end)
      break;
  }

  if (!objfile)
    error ("Can't find load module for function to call");

  write_register (GR0_REGNUM + 1, swizzle(objfile->saved_dp_reg));

  write_register (GR0_REGNUM + 11, swizzle (fun));
  return (CORE_ADDR) fun;
}

/* Bindu: This functions pops all the registers pushed by push_dummy_frame 
   from fp to fsr. This has to be synchronised with push_dummy_frame */
void
ia64_find_dummy_frame_regs_1 (CORE_ADDR fp,
			      struct frame_saved_regs *fsr)
{
  int i;
  int num_stacked_regs;

  /* Decrement fp before assigning. */
  fp = fp - REGISTER_SIZE;
  fsr->regs[SAVE_SP_REGNUM] = fp;
  fp = fp - REGISTER_SIZE;
  fsr->regs[PC_REGNUM] = fp;
  fp = fp - REGISTER_SIZE;
  fsr->regs[CFM_REGNUM] = fp;  

  /* RM: the static registers */
  for (i = GR0_REGNUM + 1;
       i < GR0_REGNUM + NUM_ST_GRS;
       i++)
    if (i != SAVE_SP_REGNUM)
      {
	fp -= REGISTER_SIZE;
	fsr->regs[i] = fp;
      }
  num_stacked_regs = (int) SIZE_OF_FRAME (read_memory_integer(fsr->regs[CFM_REGNUM], 
						       REGISTER_SIZE));
  /* RM: the stacked registers */
  for (i = GR0_REGNUM + NUM_ST_GRS;
       i < GR0_REGNUM + NUM_ST_GRS + num_stacked_regs;
       i++)
    {
      fp -= REGISTER_SIZE;
      fsr->regs[i] = fp;
    }

  /* RM: the branch registers */
  for (i = BR0_REGNUM;
       i <= BRLAST_REGNUM;
       i++)
    {
      fp -= REGISTER_SIZE;
      fsr->regs[i] = fp;
    }

  /* RM: the predicate registers */
  for (i = PR0_REGNUM;
       i <= PRLAST_REGNUM;
       i++)
    {
      fp -= 1;
      fsr->regs[i] = fp;
    }

  /* RM: the NaT registers */
  for (i = NR0_REGNUM;
       i < NR0_REGNUM + NUM_ST_GRS + num_stacked_regs;
       i++)
    {
      fp -= 1;
      fsr->regs[i] = fp;
    }

  /* RM: the floating point registers */
  for (i = FR0_REGNUM;
       i <= FRLAST_REGNUM;
       i++)
    {
      fp -= 12;
      fsr->regs[i] = fp;
    }

  /* RM: the application registers */
  for (i = AR0_REGNUM;
       i <= ARLAST_REGNUM;
       i++)
    {
      fp -= REGISTER_SIZE;
      fsr->regs[i] = fp;
    }

  fp -= REGISTER_SIZE;
  fsr->regs[PSR_REGNUM] = fp;

  return;
}

void
ia64_find_dummy_frame_regs (struct frame_info *frame,
			    struct frame_saved_regs *fsr)
{
  ia64_find_dummy_frame_regs_1 (frame->frame, fsr);
  return;
}

void
ia64_frame_find_saved_regs (struct frame_info *frame_info,
			    struct frame_saved_regs *frame_saved_regs)
{
  int i;
  CORE_ADDR p;

  /* Zero out everything.  */
  memset (frame_saved_regs, '\0', sizeof (struct frame_saved_regs));

  /* Call dummy frames always look the same, so there's no need to
     examine the dummy code to determine locations of saved registers;
     instead, let find_dummy_frame_regs fill in the correct offsets
     for the saved registers.  */
  if (PC_IN_CALL_DUMMY (frame_info->pc, frame_info->sp, frame_info->psp))
    ia64_find_dummy_frame_regs (frame_info, frame_saved_regs);
  else
    {
      /* Bindu JAGaf84254: On IPF, prev frame's stacked reagisters are
         really not saved registers here. Do not consider them so. */

      /* The frame always represents the value of %sp at entry to the
	 current function (and is thus equivalent to the "saved" stack
	 pointer.  */
      frame_saved_regs->regs[SP_REGNUM] = frame_info->frame;

      /* RM: ??? FIXME: rest of ia64_frame_find_saved_regs() not
         implemented yet. */
      return;
    }
  return;
}

/* Bindu 030602: Get the FRs, GR0-31 and BR0-7 from the frame. */
void
ia64_read_register_from_frame (char* raw_buffer,
			       struct frame_info *frame,
			       int regnum)
{
  int frame_regnum = -1;
  CORE_ADDR regval;
  int nats;
  /* CFM */
  if (regnum == CFM_REGNUM)
    {
      memcpy (raw_buffer, &frame->cfm, 
	   REGISTER_RAW_SIZE (regnum));
      return;
    }

  /* PC */
  if (regnum == PC_REGNUM)
    {
      memcpy (raw_buffer, &frame->pc, 
	   REGISTER_RAW_SIZE (regnum));
      return;
    }

  if ((regnum >= FR0_REGNUM) && (regnum <= FRLAST_REGNUM))
    frame_regnum = regnum;
  else if ((regnum >= GR0_REGNUM) && (regnum <= GR0_REGNUM + 31))
    frame_regnum = FRLAST_REGNUM + 1 + (regnum - GR0_REGNUM);
  else if ((regnum >= NR0_REGNUM) && (regnum <= NR0_REGNUM + 31))
  /* To read the Nats of reg1 to reg31 stored at frame->register[1912] */
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 7;
  else if ((regnum >= BR0_REGNUM) && (regnum <= BRLAST_REGNUM))
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + (regnum - BR0_REGNUM);
  else if (regnum == AR0_REGNUM + 64)
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS;
  else if (regnum == AR0_REGNUM + 18)
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 1;
  else if (regnum == AR0_REGNUM + 19)
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 2;
  else if (regnum == AR0_REGNUM + 36)
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 3;
  else if (regnum == AR0_REGNUM + 40)
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 4;
  else if (regnum == AR0_REGNUM + 65)
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 5;
  else if ((regnum >= PR0_REGNUM) && (regnum <= PRLAST_REGNUM))
    frame_regnum = FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 6;


  if (frame_regnum != -1)
    {
      /* Bindu 022406: Read in the registers from unwind context if we haven't
         already done so. */
      fill_regs_from_uc (current_uc, frame);

      if ((regnum >= NR0_REGNUM) && (regnum <= NR0_REGNUM + 31))
        {
          memcpy (&nats, &frame->registers[REGISTER_BYTE (frame_regnum)], 
	        sizeof (nats));
	  *raw_buffer = ((nats >> (regnum - NR0_REGNUM)) & 1);
	}
      else if ((regnum >= PR0_REGNUM) && (regnum <= PRLAST_REGNUM))
	{
          memcpy (&regval, &frame->registers[REGISTER_BYTE (frame_regnum)], 
	        sizeof (regval));
	  *raw_buffer = (regval >> (regnum - PR0_REGNUM)) &1;
	}
      else
        memcpy (raw_buffer, &frame->registers[REGISTER_BYTE (frame_regnum)], 
	        REGISTER_RAW_SIZE (regnum));

      /* frame->registers are all in big-endian.  If we are expecting this
	 register to be in little-endian, flip the buffer.
       */

      if (IS_FLIP_REG (regnum))
	endian_flip (raw_buffer, REGISTER_RAW_SIZE (regnum));
    }
  return;
}


/* Find register number REGNUM relative to FRAME and put its (raw,
   target format) contents in *RAW_BUFFER.  Set *OPTIMIZED if the
   variable was optimized out (and thus can't be fetched).  Set *OPTIMIZED
   to 2 to indicate that the register is a NaT. Set *LVAL
   to lval_memory, lval_register, or not_lval, depending on whether
   the value was fetched from memory, from a register, or in a strange
   and non-modifiable way (e.g. a frame pointer which was calculated
   rather than fetched).  Set *ADDRP to the address, either in memory
   on as a REGISTER_BYTE offset into the registers array.

   The argument RAW_BUFFER must point to aligned memory.  */

void
ia64_get_saved_register (char *raw_buffer, int *optimized, CORE_ADDR *addrp,
			 struct frame_info *frame, int regnum, 
			 enum lval_type *lval)
{
  CORE_ADDR addr = 0;

  if (!target_has_registers)
    error ("No registers.");

  if (frame->pa_save_state_ptr)
    {
      mixed_mode_get_saved_register (raw_buffer, optimized, addrp, frame,
                                     regnum, lval);
      return;
    }

  /* Normal systems don't optimize out things with register numbers.  */
  if (optimized != NULL)
    *optimized = 0;
  /* If this is during backtrace_other_thread_command, read the stacked
     registers using rse_fp of the frame. */
  /* skip the following for inlined routines too. */
  if (!frame || frame->inline_idx > 0 || (   get_current_frame () == frame
		  && backtrace_other_thread_bsp == (CORE_ADDR)(long) -1)
             || (   get_current_frame () == frame
		 && backtrace_other_thread_bsp != (CORE_ADDR)(long) -1
                 && ( regnum < GR0_REGNUM + NUM_ST_GRS
                   || regnum >= frame->n_rse_regs + GR0_REGNUM + NUM_ST_GRS) 
                 && ( regnum < NR0_REGNUM + NUM_ST_GRS
                   || regnum >= frame->n_rse_regs + NR0_REGNUM + NUM_ST_GRS)))
    {
      if (lval != NULL)
	*lval = lval_register;
      addr = REGISTER_BYTE (regnum);
      if (raw_buffer != NULL)
        read_register_gen (regnum, raw_buffer);
    }
  else if (   (   (regnum >= GR0_REGNUM + NUM_ST_GRS)
               && (regnum < frame->n_rse_regs + GR0_REGNUM + NUM_ST_GRS))
	   /* For bot, for the topmost frame, n_rse_regs is not properly
	      set. Consider all 128 as stacked regs. */
	   || (   backtrace_other_thread_bsp != (CORE_ADDR)(long) -1
	       && get_current_frame () == frame
	       && (regnum >= GR0_REGNUM + NUM_ST_GRS)
               && (regnum < 128 + GR0_REGNUM + NUM_ST_GRS)))
    {
      addr = add_to_bsp (frame->rse_fp, 
		(regnum - GR0_REGNUM - NUM_ST_GRS));
      if (lval != NULL)
	*lval = lval_memory;
      if (raw_buffer != NULL)
	read_memory (addr, raw_buffer, REGISTER_RAW_SIZE (regnum));
      if (IS_TARGET_LRE)
        { /* If we read memory belonging to RSE, it was stored in big-endian
	     and we expect memory to be in little endian, so flip the buffer.
	   */

	  endian_flip (raw_buffer, REGISTER_RAW_SIZE (regnum));
	}
    }
  else if (   (   (regnum >= NR0_REGNUM + NUM_ST_GRS)
	       && (regnum < frame->n_rse_regs + NR0_REGNUM + NUM_ST_GRS))
	      /* For bot, for the topmost frame, n_rse_regs is not properly
 	       * set. Consider all 128 as stacked regs. 
	       */
	   || (   backtrace_other_thread_bsp != (CORE_ADDR)(long) -1
	       && get_current_frame () == frame
               && (regnum >= NR0_REGNUM + NUM_ST_GRS)
	       && (regnum < 128 + NR0_REGNUM + NUM_ST_GRS)))
    {
      CORE_ADDR gr_addr;
      gr_addr = add_to_bsp (frame->rse_fp, 
		(regnum - NR0_REGNUM - NUM_ST_GRS));
      addr = NAT_COLLECTION_ADDR (gr_addr);
      /* get the nat bit contents by calling read_nat_addr.
	 See the comment for NAT_BITOPS in ia64h-nat.c for the
	 the shift size. */
      read_nat_addr (addr, raw_buffer, (int)(((gr_addr) >> 3) & 0x3f));
    }
  else
    {

      if (raw_buffer != NULL)
        {
        /* Bindu 05/18/01: For IA64 we have a problem with getting the
           address of a floating point register in the upper frames. 
    	   So, we get the value of the floating point registers from the 
           frame's unwind context. */
        /* Also get grs, nats, prs and brs from unwind context.
	   PC, PFS and CFM too. */
        /* FIXME: we might want to use uwx_get_spill_loc to get the
	   spill location . */
          if ((frame != 0) && (get_next_frame (frame))
             && (   ((regnum >= FR0_REGNUM) && (regnum <= FRLAST_REGNUM))
                 || ((regnum >= GR0_REGNUM) && (regnum <= GR0_REGNUM + 31))
                 || ((regnum >= NR0_REGNUM) && (regnum <= NR0_REGNUM + 31))
                 || ((regnum >= PR0_REGNUM) && (regnum <= PRLAST_REGNUM))
		 || ((regnum >= BR0_REGNUM) && (regnum <= BR0_REGNUM + 7))
                    /* ARs: PFS, BSPSTORE, RNAT, UNAT, FPSR, LC */
		 || (regnum == AR0_REGNUM + 64) || (regnum == AR0_REGNUM + 18)
                 || (regnum == AR0_REGNUM + 19) || (regnum == AR0_REGNUM + 36)
                 || (regnum == AR0_REGNUM + 40) || (regnum == AR0_REGNUM + 65)
		 || (regnum == CFM_REGNUM)
		 || (regnum == PC_REGNUM)))
            {
              if (lval != NULL)
                *lval = not_lval; /* don't allow people to write for now.
                                     FIXME!!! */
              /* As of now, unwind library does not give us the address
                 at which this register resides. So it's not possible to fix
                 this. */
              ia64_read_register_from_frame (raw_buffer, frame, regnum);
            }
          /* Read all other registers from the top most frame */
          else
            {
              if (lval != NULL)
                *lval = not_lval; /* don't allow people to write for now.
                                     FIXME!!! */
              read_register_gen (regnum, raw_buffer);
            }
        }
    }

  /* For general registers get the value of the NaT bit */
  if (regnum >= GR0_REGNUM && regnum <= GRLAST_REGNUM)
    {
      char nat_buf[MAX_REGISTER_RAW_SIZE];
      long long reg_val;
      read_relative_register_raw_bytes (regnum - GR0_REGNUM + NR0_REGNUM, 
                                        (char *) nat_buf);
      reg_val = 
        extract_signed_integer (nat_buf, 
                                REGISTER_RAW_SIZE (regnum - GR0_REGNUM + 
                                                   NR0_REGNUM));
      if (reg_val == 1 && optimized != NULL)
        *optimized = NOT_A_THING;
    }

  if (addrp != NULL)
        *addrp = addr;
}


/* Target operation for enabling or disabling interception of
   exception events.
   KIND is either EX_EVENT_THROW or EX_EVENT_CATCH
   ENABLE is either 0 (disable) or 1 (enable).
   Return value is NULL if no support found;
   -1 if something went wrong,
   or a pointer to a symtab/line struct if the breakpointable
   address was found. */

static char throw_break_sym[] = "__throw__FPvT1";
static char catch_break_sym[] = "__begin_catch";
static struct symtab_and_line *break_callback_sal = NULL;

CORE_ADDR throw_break_addr = 0;
CORE_ADDR catch_break_addr = 0;

static struct exception_event_record current_ex_event;

struct symtab_and_line *
remote_enable_exception_callback (enum exception_event_kind kind, int enable)
{
  struct minimal_symbol *msymbol = 0; /* initialize for compiler warning */

  switch (kind)
    {
    case EX_EVENT_THROW:
      msymbol = lookup_minimal_symbol (throw_break_sym, 0, 0);
      if (!msymbol)
	return (struct symtab_and_line *)(long) -1;
      throw_break_addr = SYMBOL_VALUE_ADDRESS (msymbol);
      break;
    case EX_EVENT_CATCH:
      msymbol = lookup_minimal_symbol (catch_break_sym, 0, 0);
      if (!msymbol)
	return (struct symtab_and_line *)(long) -1;
      catch_break_addr = SYMBOL_VALUE_ADDRESS (msymbol);
      break;
    default:
      error ("Request to enable unknown or unsupported exception event.");
    }

  if (!break_callback_sal)
    {
      break_callback_sal = (struct symtab_and_line *) xmalloc (sizeof (struct symtab_and_line));
    }
  break_callback_sal->symtab = NULL;
  break_callback_sal->pc = SYMBOL_VALUE_ADDRESS (msymbol);
  break_callback_sal->line = 0;
  break_callback_sal->end = SYMBOL_VALUE_ADDRESS (msymbol);

  return break_callback_sal;
}

/* Report current exception event.  Returns a pointer to a record
   that describes the kind of the event, where it was thrown from,
   and where it will be caught.  More information may be reported
   in the future */
struct exception_event_record *
remote_get_current_exception_event ()
{
  CORE_ADDR pc, ret_pc;
  static struct symtab_and_line empty_sal =
  {0, 0, 0, 0};

  pc = read_register (PC_REGNUM);

  if (pc == throw_break_addr)
    {
      ret_pc = read_register (BR0_REGNUM);
      current_ex_event.kind = (enum exception_event_kind) EX_EVENT_THROW;
      current_ex_event.throw_sal = find_pc_line (ret_pc, 1);
      current_ex_event.catch_sal = empty_sal;
    }
  else if (pc == catch_break_addr)
    {
      ret_pc = read_register (BR0_REGNUM);
      current_ex_event.kind = (enum exception_event_kind) EX_EVENT_CATCH;
      current_ex_event.throw_sal = empty_sal;;
      current_ex_event.catch_sal = find_pc_line (ret_pc, 1);
    }
  else
    return 0;

  return &current_ex_event;
}

/* Temporary hack alert!
   pa_do_strcat_registers_info is called from tui/libtui.a(tuiRegs.o).
   A dummy definition is placed here.  The routine should be re-named
   and a version implemented for ia64.
 */

void
pa_do_strcat_registers_info (int regnum, int fpregs, struct ui_file *stream,
			     enum precision_type precision)
{
  warning ("Register display not implemented.  Use info registers command.\n");
}				/* end pa_do_strcat_registers_info */


/* Return the value that BSP had when the interrupted procedure was
   interrupted.  I.E. BSP should be pointing at the spill address of GR32.
   When the interrupt occurred, an RSE cover was performed and BSP was 
   bumped up to be able to store the registers of the interrupted procedure,
   i.e. its last register is spilled to the new BSP minus 8.
   */

CORE_ADDR 
pre_interrupt_bsp ()
{
   CORE_ADDR	after_interrupt_bsp;
   CORE_ADDR	bsp;
   CORE_ADDR	cfm;
   int		size_of_frame;
#ifdef HP_MXN
   int		size_of_locals;
#endif

  if (backtrace_other_thread_bsp != (CORE_ADDR)(long) -1)
    return backtrace_other_thread_bsp;

   after_interrupt_bsp = read_register (AR0_REGNUM + 17);
#ifdef HP_IA64_GAMBIT
   return after_interrupt_bsp;
#else
   cfm = read_register (CFM_REGNUM);
   size_of_locals = (int) SIZE_OF_LOCALS (cfm);
   /* Bindu: If a thread is stopped inside a system call the 
      after_interrupt_bsp is the bsp-sol. */
   if (IN_SYSCALL) 
     {
       bsp = add_to_bsp (after_interrupt_bsp, 
			 -(size_of_locals));
       return bsp;
     }

   size_of_frame = (int) SIZE_OF_FRAME (cfm);
   if (!size_of_frame)
     return after_interrupt_bsp;  /* No registers, cover did nothing */
 
   /* Bindu 012901: For unbound threads, flush is done in the callee. 
      So the BSP is actual BSP + sol. */

#ifdef HP_MXN
   if (is_mxn && (get_lwp_for(inferior_pid) == 0)) /* unbound thread */
     bsp = add_to_bsp (after_interrupt_bsp, -(size_of_locals));
   else 
#endif
     bsp = add_to_bsp (after_interrupt_bsp,
				 -size_of_frame);

   return bsp;
#endif /* #else of HP_IA64_GAMBIT */
} /* end pre_interrupt_bsp */

static void
objects_info (char *arg, int from_tty)
{
  struct partial_symtab *ps;
  struct objfile *objfile;
  int index;

  dont_repeat ();
  printf_filtered ("\n%9.9s  %12.12s  %s\n", "Index", "Status", "File");
  printf_filtered ("%9.9s  %12.12s  %s\n", "-----", "------", "----");
  index = 0;
  ALL_PSYMTABS (objfile, ps) {
    char *status_str;

    index++;
    switch (OFILE_LOAD_STATUS(ps)) {
    case DOOM_OFILE_PRELOADED:
      status_str = "In Exec";
      break;
    case DOOM_OFILE_DO_NOT_DEMAND_LOAD:
      status_str = "Not Loaded";
      break;
    case DOOM_OFILE_DO_DEMAND_LOAD:
      status_str = "Demand Load";
      break;
    case DOOM_OFILE_LOADED_WITH_DEBUG:
      status_str = "Loaded";
      break;
    case DOOM_OFILE_LOADED_NO_DEBUG:
      status_str = "No Debug";
      break;
    case DOOM_OFILE_OPEN_ERROR:
      status_str = "File Error";
      break;
    default:
      status_str = "<Unknown>";
    }
    printf_filtered ("%9d  %12.12s  %s\n", index, status_str, ps->filename);
  }
  printf_filtered ("\n");
}

static void
objectload_command (char *arg, int from_tty)
{
  struct partial_symtab *ps;
  struct objfile *objfile;
  int found = 0;

  dont_repeat ();

  if (arg) {
    ALL_PSYMTABS (objfile, ps) {
      if (STREQ(ps->filename, arg)) {
        if (from_tty) {
          printf_filtered ("Loading %s...\n", ps->filename);
        }
        PSYMTAB_TO_SYMTAB(ps);
        found = 1;
      }
    }
    if (!found) {
      int given_index = atoi(arg);
      if (given_index > 0) {
        int index = 0;
        ALL_PSYMTABS (objfile, ps) {
          index++;
          if (given_index == index) {
            if (from_tty) {
              printf_filtered ("Loading debug information from %s...\n",
                               ps->filename);
            }
            PSYMTAB_TO_SYMTAB(ps);
            found = 1;
          }
        }
      }
    }
    if (!found) {
      error("%s: Unknown object file. Use \"info objects\" to list the files.",
            arg);
    }
  } else {
    int ask_user = 1;

    if (from_tty) {
      ask_user = query("Load all of the debug information from all object"
                       " files? ");
    }
    if (ask_user) {
      ALL_PSYMTABS (objfile, ps) {
        if (OFILE_LOAD_STATUS(ps) != DOOM_OFILE_LOADED_WITH_DEBUG) {
          if (from_tty) {
            printf_filtered ("Loading %s...\n", ps->filename);
          }
          PSYMTAB_TO_SYMTAB(ps);
        }
      }
    }
  }
}


void
objectretry_command (char *arg, int from_tty)
{
  struct partial_symtab *ps;
  struct objfile *objfile;
  int at_least_one_reset = 0;

  dont_repeat ();

  if (from_tty) {
    printf_filtered ("\nSetting objects to load on demand:\n");
  }
  ALL_PSYMTABS (objfile, ps) {
    if (OFILE_LOAD_STATUS_IS_RETRYABLE(ps)) {
      if (from_tty) {
        printf_filtered ("\t%s\n", ps->filename);
      }
      OFILE_LOAD_STATUS(ps) = DOOM_OFILE_DO_DEMAND_LOAD;
      at_least_one_reset = 1;
    }
  }
  if (from_tty) {
    if (!at_least_one_reset) {
      printf_filtered ("\t<none>\n");
    }
    printf_filtered ("\n");
  }

  if (arg) {
    objectdir_command(arg, from_tty);
    if (from_tty) {
      printf_filtered ("\n");
    }
  }
}

void
_initialize_ia64_tdep ()
{
  tm_print_insn = print_insn_ia64;
  tm_strcat_insn = strcat_insn_ia64;

  add_info("objects", objects_info,
           "Load status of debug information in each object file.\n\n"
           "Usage:\n\tinfo objects\n");
  add_cmd ("objectload", class_files, objectload_command,
           "Force an object file to be loaded and its debug information read.\n\n"
           "Usage:\n\tobjectload <FILE> | <INDEX>\n\n"
           "Either the name or the index number reported by \"info objects\"\n"
           "can be provided. If an object has already been successfully loaded,\n"
           "it will not be loaded again.",
           &cmdlist);
  add_cmd ("objectretry", class_files, objectretry_command,
           "Retry loading object files that failed previously.\n\n"
           "Usage:\n\tobjectretry <DIR1> [[:]<DIR2>] [...]\n\n"
           "If an argument is provided, add it to the list of directories\n"
           "searched for object files. See \"objectdir\".",
           &cmdlist);

  add_show_from_set 
    (add_set_cmd ("no-predication-handling", class_support, var_boolean, 
                  (char *) &no_predication_handling, 
                  "Set no special handling for predicated-off instructions.(IA only)" 
                  "\n\nUsage:\nTo set new value:\n\tset no-predication-handling "
                  "[on | off]\nTo see current value:\n\tshow no-predication-handling"
                  "\n\nIf set, gdb will stop in breakpoints at instructions even if "
                  "they are predicated-off.\n", &setlist), 
     &showlist);
}

/* swizzle
   input_addr is copied to the result.  
   If the current program is ilp32, then bits 32 and 33 of input_addr are
   copied to bits  1 and 2 of the result.  (high order bit is numbered zero 
   here).
 */

CORE_ADDR
swizzle (CORE_ADDR input_addr)
{
  CORE_ADDR result;
  if (is_swizzled)
    {
      if (input_addr < 0x100000000LL)
        result = (input_addr & ~0x6000000000000000LL)
	  | ((input_addr & 0xc0000000) << 31);
      else
        result = input_addr;
    }
  else
    {
      result = input_addr;
    }
  return result;
}				/* end swizzle */

/* unswizzle
   input_addr is copied to the result.  
   If the current program is ilp32, then only the low 32 bits are copied,
   otherwise, the input is just copied to the output.
 */

CORE_ADDR
unswizzle (CORE_ADDR input_addr)
{
  CORE_ADDR result;

  if (is_swizzled)
    {
      result = input_addr & 0xFFFFFFFFLL;
    }
  else
    {
      result = input_addr;
    }
  return result;
}				/* end unswizzle */

#ifdef MAINTENANCE_CMDS
/* Job of this routine is to take a pointer to an unwind info block and
   a scale_factor which tells us whether to multiply the length
   field by 4 or 8 (for 32 bit words or 64 bit double words) and
   _______________________________
   |  |  |  |   Unwind    | Other|    V = version, F = flags,
   |V |F |U | Descriptors | Stuff|    U = length of the descriptor
   |__|__|__|_____________|______|        area in words/double words
                                          (32/64 bit) known as 'ulen' 
					  in most unwind documents

   correctly interpret the record formats and print them in a human
   readable format for the user.  Most of the code is dedicated to
   interpreting the unwind descriptors - groups of records encoded in
   variable-length byte strings describing the unwind regions in a
   procedure.  The many unwind descriptor record formats are
   described in Appendix B of the IA64 Run Time Architecture.  The
   1 letter variable names like 'r' and 't' are maintained in order
   to match the documentation as closely as possible.  */

void
dump_unwind_info (unsigned char *unwind_info_block, int scale_factor)
{
  int header_P = 0, r, t = 0, prologue_size = 0, n;
  unsigned char *unwind_desc_p = unwind_info_block + sizeof (CORE_ADDR);
  unsigned char *end_desc_p;
  CORE_ADDR ui;

  /* Read first double word containing the VFU field one word at a time
     so we can handle 32 as well as 64 bit unwind info */

  ui = ( ( ((CORE_ADDR) *(uint32_t *)(void *) (unwind_info_block)) << 32) |
	   ((CORE_ADDR)  *(uint32_t *)(void *) (unwind_info_block + 4) ) );
  
  end_desc_p = unwind_desc_p + (UNW_LENGTH (ui) * scale_factor);

  printf_unfiltered ("Info block version:"
		     "0x%llx, flags:0x%llx, length:%lld * %d == %lld\n",
		     UNW_VER (ui), UNW_FLAG_MASK (ui), UNW_LENGTH (ui),
		     scale_factor, UNW_LENGTH (ui) * scale_factor);

  /* Now print the Unwind descriptor records ... */
  while (unwind_desc_p < end_desc_p)
    {
      printf_unfiltered ("\n0x%03x: (%02x) ", 
			((CORE_ADDR) unwind_desc_p - ui
			 - sizeof(CORE_ADDR)),
			*unwind_desc_p);
      /* format R1 */
      if (((*unwind_desc_p) & 0xc0) == 0)
	{
	  int rlen = (*unwind_desc_p) & 0x1f;
	  header_P = (*unwind_desc_p & 0x20) == 0;
	  if (header_P)
	    {
	      prologue_size = rlen;
	      printf_unfiltered("%-14s", "R1prologue");
	    }
	  else
            printf_unfiltered("%-14s", "R1body");
	  printf_unfiltered("rlen=%d", rlen);
	  unwind_desc_p++;
	  continue;
	}
      /* format R2 */
      if ( ( (*unwind_desc_p) & 0xf8) == 0x40)
	{
	  int mask = ((*unwind_desc_p & 7) << 1) | (*(unwind_desc_p + 1) >> 7);
	  int grsave = *(unwind_desc_p + 1) & 0x7f;
	  int rlen = read_unsigned_leb128((char*) unwind_desc_p+2, &n);
	  printf_unfiltered("%-14s", "R2");
	  printf_unfiltered("rlen=%d mask=%x grsave=%d", rlen, mask, grsave);
	  prologue_size = rlen;
	  unwind_desc_p += n + 2;
	  continue;
	}
      /* format R3 */
      if ( ( (*unwind_desc_p) & 0xfe) == 0x60)
	{
	  int rlen = read_unsigned_leb128((char*) unwind_desc_p+1, &n);
	  header_P = (*unwind_desc_p & 0x03) == 0;
	  if (header_P)
	    {
	      printf_unfiltered("%-14s", "R3prologue");
	      prologue_size = rlen;
	    }
	  else
	    printf_unfiltered("%-14s", "R3body");

	  printf_unfiltered("rlen=%d", rlen);
	  unwind_desc_p += n + 1;
	  continue;
	}
      /* format B1 */
      if ( ( ( (*unwind_desc_p) & 0xc0) == 0x80) && !header_P)
	{
	  unsigned char label = *unwind_desc_p & 0x1f;
	  if ((*unwind_desc_p & 0xe0) == 0x80)
	    printf_unfiltered("%-14s", "B1label_state");
	  else
	    printf_unfiltered("%-14s", "B1copy_state");
	  printf_unfiltered("label=%d", label);
	  unwind_desc_p += 1;
	  continue;
	}
      /* format P1 */
      if ( ( (*unwind_desc_p) & 0xe0) == 0x80)
	{
	  unsigned char brmask = *unwind_desc_p & 0x1f;
	  printf_unfiltered("%-14s", "P1br_mem");
	  printf_unfiltered("brmask=0x%x", brmask);
	  unwind_desc_p += 1;
	  continue;
	}
      /* format P2 */
      if (((*unwind_desc_p) & 0xf0) == 0xa0)
	{
	  unsigned char brmask = ( ( (*unwind_desc_p & 0xf) << 1)
				   | (* (unwind_desc_p + 1) >> 7));
	  unsigned char gr = (*(unwind_desc_p + 1) & 0x7f);
	  printf_unfiltered("%-14s", "P2br_gr");
	  printf_unfiltered("brmask=0x%x gr=%d", brmask, gr);
	  unwind_desc_p += 2;
	  continue;
	}
      /* format P3 */
      if (((*unwind_desc_p) & 0xf8) == 0xb0)
	{
	  unsigned char r = ( ( (*unwind_desc_p & 0x7) << 1)
			      | (* (unwind_desc_p + 1) >> 7));
	    
	  unsigned char grbr = (*(unwind_desc_p + 1) & 0x7f);
	  char g_or_b = 'g';
	  switch (r) 
	    {
	    case 0:
	      printf_unfiltered("%-14s", "P3psp_gr");
	      break;
	    case 1:
	      printf_unfiltered("%-14s", "P3rp_gr");
	      break;
	    case 2:
	      printf_unfiltered("%-14s", "P3pfs_gr");
	      break;
	    case 3:
	      printf_unfiltered("%-14s", "P3preds_gr");
	      break;
	    case 4:
	      printf_unfiltered("%-14s", "P3unat_gr");
	      break;
	    case 5:
	      printf_unfiltered("%-14s", "P3lc_gr");
	      break;
	    case 6:
	      printf_unfiltered("%-14s", "P3rp_br");
	      g_or_b = 'b';
	      break;
	    case 7:
	      printf_unfiltered("%-14s", "P3rnat_gr");
	      break;
	    case 8:
	      printf_unfiltered("%-14s", "P3bsb_gr");
	      break;
	    case 9:
	      printf_unfiltered("%-14s", "P3bspstore_gr");
	      break;
	    case 10:
	      printf_unfiltered("%-14s", "P3fpsr_gr");
	      break;
	    case 11:
	      printf_unfiltered("%-14s", "P3priunat_gr");
	      break;
	    default:
	      printf_unfiltered("%-14s", "P3unknown");
	      break;
	    }
	  printf_unfiltered("%cr=%d", g_or_b, grbr);
	  unwind_desc_p += 2;
	  continue;
	}
      /* format P4 */
      if ((*unwind_desc_p) == 0xb8)
	{
	  int i = 0, imask_size = (prologue_size+3)/4;
	  printf_unfiltered("%-14s", "P4spill_mask");
	  unwind_desc_p += 1;
	  for (i = 0; i < prologue_size; i++)
	    {
	      int pair = (*unwind_desc_p >> (6 - 2 * (i & 3))) & 3;
	      switch (pair) {
	      case 0:
		printf_unfiltered("0");
		break;
	      case 1:
		printf_unfiltered("F");
		break;
	      case 2:
		printf_unfiltered("G");
		break;
	      case 3:
		printf_unfiltered("B");
		break;
	      }
	      if ((i & 3) == 3)
		unwind_desc_p += 1;
	    }
	  if ((i & 3) != 3)
	    unwind_desc_p += 1;
	  continue;
	}
      /* format P5 */
      if ((*unwind_desc_p) == 0xb9)
	{
	  unsigned char grmask = (*(unwind_desc_p + 1)) >> 4;
	  unsigned int frmask = 
	    (*(unwind_desc_p + 3)) |
	    ((*(unwind_desc_p + 2)) << 8) |
	    (((*(unwind_desc_p + 1)) & 0xf) << 16);
	  printf_unfiltered("%-14s", "P5frgr_mem");
	  printf_unfiltered("grmask=%x frmask=%x", grmask, frmask);
	  unwind_desc_p += 4;
	  continue;
	}
      /* format P6 or B2 */
      if (((*unwind_desc_p) & 0xe0) == 0xc0)
	{
	  if (header_P)
	    {
	      int rmask = *unwind_desc_p & 0xf;
	      if ((*unwind_desc_p & 0xf0) == 0xc0)
		printf_unfiltered("%-14s", "P6fr_mem");
	      else if ((*unwind_desc_p & 0xf0) == 0xd0)
		printf_unfiltered("%-14s", "P6gr_mem");
	      else
		printf_unfiltered("%-14s", "?P6orB2?");
	      printf_unfiltered("rmask=%d", rmask);
	      unwind_desc_p += 1;
	    }
	  else
	    {
	      int ecount = *unwind_desc_p & 0x1f;
	      printf_unfiltered("%-14s", "B2epilogue");
	      unwind_desc_p++;
	      t = read_unsigned_leb128((char*) unwind_desc_p, &n);
	      printf_unfiltered("t=%d", t);
	      unwind_desc_p += n;
	    }
	  continue;
	}
      /* format P7 or B3 */
      if (((*unwind_desc_p) & 0xf0) == 0xe0)
	{
	  if (header_P)
	    {
	      int size;
	      r = (*unwind_desc_p) & 0x0f;
	      t = read_unsigned_leb128((char*) unwind_desc_p+1, &n);
	      unwind_desc_p += 1 + n;
	      switch (r) {
	      case 0:
		printf_unfiltered("%-14s", "P7mem_stack_f");
		size = read_unsigned_leb128((char*) unwind_desc_p, &n);
		printf_unfiltered("t=%d size=%d", t, size);
		unwind_desc_p += n;
		break;
	      case 1:
		printf_unfiltered("%-14s", "P7mem_stack_v");
		printf_unfiltered("t=%d", t);
		break;
	      case 2:
		printf_unfiltered("%-14s", "P7spill_base");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 3:
		printf_unfiltered("%-14s", "P7mmem_stack_p_sprelem_stack_v");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 4:
		printf_unfiltered("%-14s", "P7rp_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 5:
		printf_unfiltered("%-14s", "P7rp_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 6:
		printf_unfiltered("%-14s", "P7pfs_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 7:
		printf_unfiltered("%-14s", "P7pfs_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 8:
		printf_unfiltered("%-14s", "P7preds_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 9:
		printf_unfiltered("%-14s", "P7preds_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 10:
		printf_unfiltered("%-14s", "P7lc_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 11:
		printf_unfiltered("%-14s", "P7lc_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 12:
		printf_unfiltered("%-14s", "P7unat_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 13:
		printf_unfiltered("%-14s", "P7unat_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 14:
		printf_unfiltered("%-14s", "P7fpsr_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 15:
		printf_unfiltered("%-14s", "P7fpsr_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      }
	    }
	  else
	    {
	      int ecount;
	      t = read_unsigned_leb128((char*) unwind_desc_p+1, &n);
	      unwind_desc_p += 1 + n;
	      ecount = read_unsigned_leb128((char*) unwind_desc_p, &n);
	      unwind_desc_p += n;
	      printf_unfiltered("%-14s", "B3epilogue");
	      printf_unfiltered("t=%d ecount=%d", t, ecount);
	    }
	  continue;
	}
      /* format P8 or B4 */
      if ((*unwind_desc_p) == 0xf0)
	{
	  if (header_P)
	    {
	      unwind_desc_p += 1;
	      r = *unwind_desc_p;
	      unwind_desc_p += 1;
	      t = read_unsigned_leb128((char*) unwind_desc_p, &n);
	      unwind_desc_p += n;
	      switch (r) {
	      case 1:
		printf_unfiltered("%-14s", "P8rp_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 2:
		printf_unfiltered("%-14s", "P8pfs_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 3:
		printf_unfiltered("%-14s", "P8preds_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 4:
		printf_unfiltered("%-14s", "P8lc_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 5:
		printf_unfiltered("%-14s", "P8unat_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 6:
		printf_unfiltered("%-14s", "P8fpsr_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 7:
		printf_unfiltered("%-14s", "P8bsp_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 8:
		printf_unfiltered("%-14s", "P8bsp_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 9:
		printf_unfiltered("%-14s", "P8sp_psprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 10:
		printf_unfiltered("%-14s", "P8bspstore_when");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 11:
		printf_unfiltered("%-14s", "P8bspstore_psprel");
		printf_unfiltered("t=%d", t);
		break;
	      case 12:
		printf_unfiltered("%-14s", "P8bspstore_sprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 13:
		printf_unfiltered("%-14s", "P8rnat_when");
		printf_unfiltered("t=%d", t);
		break;
	      case 14:
		printf_unfiltered("%-14s", "P8rnat_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 15:
		printf_unfiltered("%-14s", "P8rnat_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 16:
		printf_unfiltered("%-14s", "P8priunat_when_gr");
		printf_unfiltered("t=%d", t);
		break;
	      case 17:
		printf_unfiltered("%-14s", "P8prinat_psprel");
		printf_unfiltered("pspoff=%d", t);
		break;
	      case 18:
		printf_unfiltered("%-14s", "P8prinat_sprel");
		printf_unfiltered("spoff=%d", t);
		break;
	      case 19:
		printf_unfiltered("%-14s", "P8prinat_when_mem");
		printf_unfiltered("t=%d", t);
		break;
		
	      }
	    }
	  else
	    {
	      if (*unwind_desc_p == 0xf0)
		{
		  printf_unfiltered("%-14s", "B4label_state");
		}
	      else
		{
		  printf_unfiltered("%-14s", "B4copy_state");
		}
	      printf_unfiltered("label=%d", t);
	      unwind_desc_p += 1;
	      t = read_unsigned_leb128((char*) unwind_desc_p, &n);
	      unwind_desc_p += n;
	    }
	  continue;
	}
      /* format P9 */
      if ((*unwind_desc_p) == 0xf1)
	{
	  printf_unfiltered("P9\n");
	  unwind_desc_p += 1;
	  unwind_desc_p += 1;
	  unwind_desc_p += 1;
	  continue;
	}
      /* format P10 */
      if ((*unwind_desc_p) == 0xff)
	{
	  unsigned char abi, context;
	  unwind_desc_p += 1;
	  abi = *unwind_desc_p;
	  unwind_desc_p += 1;
	  context = *unwind_desc_p;
	  unwind_desc_p += 1;
	  printf_unfiltered("%-14s", "P10");
	  printf_unfiltered("abi=%d", abi);
	  switch (abi) {
	  case 0:
	    printf_unfiltered(" Unix SVR4,");
	    break;
	  case 1:
	    printf_unfiltered(" HP-UX,");
	    break;
	  case 2:
	    printf_unfiltered(" Windows NT,");
	    break;
	  }
	  printf_unfiltered("context=%d", context);
	  continue;
	}
    }
  printf_unfiltered("\n");
}

/*  GDB normally searches the objfile for all other unwind info, but
    is required get info about user_sendsig unwind info from the gateway
    page and from the load_info structure.  This function tries to find
    and read the user_sendsig unwind info and returns 0 if the read is
    successful, 1 if there is an error.  */
int
read_user_sendsig_unwind (struct unwind_table_entry *u, char *exp)
{
  char buf[sizeof (struct unwind_table64)], vfu_field [sizeof (CORE_ADDR)];
  struct unwind_table64 handler_unw_entry;
  int bytes_to_copy, status = -1;

  /* The gateway page unwind table only has 1 entry - for user_sendsig.
     Get it by starting with the unwind segment and skipping over the
     unwind header:
     __________________________________________________
     |unwind header| unwind table | unwind info blocks|
     --------------------------------------------------  */

  status = target_read_memory ( (load_info.li_sendsig_unw
    				 + sizeof (struct unwind_header)), 
				buf, sizeof (struct unwind_table64));
  if (status != 0)
    {
      printf_unfiltered ("Can't find unwind table entry for %s\n", exp);
      return (1);
    }
	  
  handler_unw_entry = *( (struct unwind_table64 *)(void *) buf);
	  
  u->region_start = handler_unw_entry.start + load_info.li_sendsig_txt;
  u->region_end = handler_unw_entry.end + load_info.li_sendsig_txt;
  u->scale_factor = sizeof (CORE_ADDR);
  /* Thankfully, user_sendsig doesn't have a prologue */
  u->prologue_end = u->region_end;
	  
  /* Now copy the unwind descriptors from the unwind info block.  Get to
     the info block by starting with the unwind segment and skipping over
     both unwind header and table.  

     Unwind Segment
     __________________________________________________
     |unwind header| unwind table | unwind info blocks|
     -------------------------------------------------- 
     
     unwind info blocks can contain any number of descriptors.  In order
     to figure out how much to copy, read the length of the unwind
     descriptor area from the first double word of the unwind info block.
     Anatomy of an Unwind Info Block:
     U = length of unwind descriptor area.  
     _____________________________________
     | other | U |  Unwind       | other |
     | stuff |   |  Descriptors  | stuff |
     -------------------------------------   */

  status =
    target_read_memory (load_info.li_sendsig_unw
			+ sizeof (struct unwind_header)
			+ sizeof (struct unwind_table64),
			vfu_field, sizeof (CORE_ADDR));
  if (status != 0)
    {
      printf_unfiltered ("Can't find unwind table entry for %s\n", exp);
      return (1);
    }

  /* u->scale_factor helps convert the descriptor area length from words
     (32 bit) or double words (64 bit) into bytes.  We copy the descriptors
     and the first double word in the unwind info block.  For details about
     the special double word, see comments above dump_unwind_info ()  */

  bytes_to_copy = (int) 
    ((UNW_LENGTH (*(CORE_ADDR *)(void *) (vfu_field)) * u->scale_factor)
    + sizeof (CORE_ADDR));
	  
  u->info_block = (char *) malloc (bytes_to_copy);
	  
  status =
    target_read_memory (load_info.li_sendsig_unw
			+ sizeof (struct unwind_header)
			+ sizeof (struct unwind_table64),
			u->info_block, bytes_to_copy);
  if (status != 0)
    {
      printf_unfiltered ("Can't read unwind descriptors for %s\n", exp);
      return (1);
    }

  return (0);
}

/* The maint info unwind command ...

   The 'exp' argument can be an address or function name.  

   This function finds the unwind entry corresponding to the address
   from 'exp', stores it in 'u', prints the start and end pc of the
   unwind region and the end of the prologue region, and 
   calls dump_unwind_info () to print the unwind descriptors in the
   unwind info block.  The majority of this code deals with the one special
   case - addresses in user_sendsig - because:

       GDB uses private copies of unwind info for all functions except
   the user_sendsig (because it doesn't belong to any objfile).  Storing
   private copies of unwind is probably wasteful - see the comments above
   internalize_unwinds ().  Until this is fixed, we need to create a fake
   gdb unwind entry for user_sendsig populate it with the right data, and
   pass it to helper functions to be printed out just like all other unwind
   info.  */

static void
unwind_command (char *exp, int from_tty)
{
  CORE_ADDR address;
  struct unwind_table_entry *u = NULL;
  unsigned long long low, high;
  char *name;

  /* If we have an expression, evaluate it and use it as the address.  */
  if (exp != 0 && *exp != 0)
    {
      address = parse_and_eval_address (exp);
    }
  else
    {
      printf_unfiltered ("maint info unwind: specify function or address?\n");
      return;
    }

  if (is_swizzled)
    address = swizzle(address);

  u = find_unwind_entry (address);

  if (!u)
    {
      /* If address is in user_sendsig, read user_sendsig unwind info from
	 the child process & store it into 'u'.  If this fails, return.  All
	 error messages will be printed by read_user_sendsig_unwind(). */
      if (PC_IN_USER_SENDSIG(address))
	{
	  u = 
	    (struct unwind_table_entry *) malloc (sizeof \
						  (struct unwind_table_entry));
	  if (u)
	    {
	      if (read_user_sendsig_unwind (u, exp))
		return;
	    }
	  else
	    {
	      printf_unfiltered ("Couldn't allocate memory to store "
				 "user_sendsig unwind info\n");
	    }
	}
      else
	{
	  printf_unfiltered ("Can't find unwind table entry for %s\n", exp);
	  return;
	}
    }

  /* Finally ... We have a private copy of an unwind entry stored in 'u'.
     Print the info from 'u' in a human readable format.  */
  if (find_pc_partial_function (address, &name, &low, &high))
    printf_unfiltered ("%s:\n", name);
  printf_unfiltered ("0x%llx .. 0x%llx, end_prologue@0x%llx\n",
		     u->region_start, u->region_end, u->prologue_end);

  dump_unwind_info ( (unsigned char *) u->info_block, u->scale_factor);

  if (PC_IN_USER_SENDSIG(address))
    {
      /* Free the fake unwind entry we made for the signal handler */
      free (u);
      u = NULL;
    }
}
#endif /* MAINTENANCE_CMDS */

void
_initialize_hp_ia64_tdep (void)
{
  /* When we run the gdb test suite, if a breakpoint is at an address,
     the tests are confused by the :<instruction_slot> notation as in:
     Breakpoint 4, 0x40000000000065f0:1 in main () at hello_gambit_solib.c:3

     The environment variable HP_IA64_TESTSUITE is set to change the behavior
     of gdb to print:
     Breakpoint 4, 0x40000000000065f1 in main () at hello_gambit_solib.c:3

     The instruction slot replaces the "0" as the last digit of the address.

     See IA64 code in printcmd.c:print_address_numeric.
   */

  if (getenv ("HP_IA64_TESTSUITE") != NULL)
    hp_ia64_testsuite = 1;

#ifdef MAINTENANCE_CMDS
  add_cmd ("unwind", class_maintenance, unwind_command,
	   "Print unwind table entry at given address.\n\nUsage:\n\tmaintenance "
           "info unwind <FUNC> | <ADDR>\n",
	   &maintenanceinfolist);
#endif /* MAINTENANCE_CMDS */

  exception_catchpoints_are_fragile = 1;
}

/* The following f_xxx routines are for support of the f90 array descriptor,
 * a runtime structure used for certain types of arrays and pointers.  See
 * hp-symtab-read.c for a more complete description of the runtime structure
 * and the gdb type created to support it.
 */

/* Get the byte where the initialized flag resides -- the byte offset
 * is calculated into the addr passed to f_check_array_desc.
 * Issue an error if the initialized bit is not set (any non-zero value is
 * ok).  This check is necessary to avoid reading in garbage for the array
 * bounds.
 */
void
f_check_array_desc (CORE_ADDR addr)
{
  unsigned char flag;
  if (!addr)
    error ("Unable to access dynamic array address");
  flag = read_memory_unsigned_integer (addr, 1 /* byte */);
  if (!flag)
    error ("Uninitialized dynamic array");
}

/* Retrieve the lower bound of array descriptor array
 */
int
f_get_array_desc_array_lbound (struct type *type, LONGEST *lower_bound)
{
  /* Calculate the address of lower bound by adding the offset to where the 
     lower bound is stored in the array descriptor, i.e, 
     TYPE_ARRAY_LOWER_BOUND_VALUE, to the address of the array descriptor. */
  CORE_ADDR addr  = TYPE_ARRAY_ADDR (type) +
                    TYPE_ARRAY_LOWER_BOUND_VALUE (type);
  *lower_bound = 
    read_memory_integer (addr, TYPE_LENGTH (TYPE_ARRAY_LOW_BOUND_TYPE (type)));
  return BOUND_FETCH_OK;
}

/* Calculate upper bound of array descriptor array.  The descriptor array 
 * gives the extent of an array (the number of elements), so the upper bound 
 * must be calculated using the lower bound and the extent.
 */
int
f_get_array_desc_array_ubound (struct type * type, LONGEST *upper_bound)
{
  LONGEST lower_bound, extent;
  CORE_ADDR addr;

  int retval = f77_get_dynamic_lowerbound (type, &lower_bound);
  if (retval != BOUND_FETCH_OK)
      return retval;
  /* Calculate the address of the extent by adding the offset to where the 
     extent is stored, i.e, TYPE_ARRAY_UPPER_BOUND_VALUE, to the address 
     of the array descriptor. */
  addr = TYPE_ARRAY_ADDR (type) + TYPE_ARRAY_UPPER_BOUND_VALUE (type);
  extent = 
    read_memory_integer (addr, TYPE_LENGTH (TYPE_ARRAY_HIGH_BOUND_TYPE (type)));
  /* Calculate upper bound */
  *upper_bound = extent + lower_bound - 1;

  return BOUND_FETCH_OK;
}

/* Calculate the stride; use the offset stored in the third field 
 * (TYPE_ARRAY_STRIDE_LOC) the range type to retrieve the stride from 
 * the array descriptor. 
*/
int
f_set_array_desc_array_stride (struct type * type)
{
  CORE_ADDR addr = TYPE_ARRAY_ADDR (type) + TYPE_ARRAY_STRIDE_LOC (type);
  TYPE_ARRAY_STRIDE(type) = (int) 
    read_memory_integer (addr, TYPE_LENGTH (TYPE_ARRAY_STRIDE_TYPE (type)));
  return TYPE_ARRAY_STRIDE(type);
}

/* Get the address of the actual array contents given the address of
 * the array descriptor and the offset from the base of the array descriptor
 * where the address is stored
 */
CORE_ADDR
f_get_array_desc_array_addr (CORE_ADDR addr, int offset)
{
  CORE_ADDR array_addr;
  array_addr = read_memory_integer (addr + offset, 
    (int) (is_swizzled ? sizeof (int) : TARGET_PTR_BIT / TARGET_CHAR_BIT));
  return array_addr;
}

/* Adjust address of array when stride is negative. The address, addr,
 * currently points to first element of the reverse order.  It needs to
 * point to the last element of the reverse order.
 */
CORE_ADDR
f_adj_array_addr_w_neg_stride (CORE_ADDR addr, struct type *target_type)
{
  addr = addr
         - TYPE_LENGTH (target_type)
         + TYPE_LENGTH (get_element_type (target_type));
  return addr;
}

/* Adjust the address of valaddr when stride is negative;  valaddr is
 * the address of the buffer into which the array contents
 * has been read into; it currently points to the last element of the
 * reverse order; it needs to point to the first element of the reverse order.
 */
char *
f_adj_array_valaddr_w_neg_stride (char * valaddr, struct type *target_type)
{
  valaddr = valaddr
            + TYPE_LENGTH (target_type)
            - TYPE_LENGTH (get_element_type (target_type));
  return valaddr;
}

/* ia64_pop_dummy_frame
   Helper routine for ia64_pop_frame.  Pop a call dummy frame off of the 
   stack.  For a call dummy, all of the registers are saved in the stack
   and we can use the frame_saved_registers to get all the values
   */

void
ia64_pop_dummy_frame (CORE_ADDR pc, struct frame_info *frame)
{
  register CORE_ADDR fp, sp, target_pc = 0, r8, old_br, new_br;
  register int regnum;
  struct frame_saved_regs fsr;
  char reg_buf[MAX_REGISTER_RAW_SIZE];
  struct target_waitstatus w;
  unsigned int mov_br_gr8_bundle[] =
  {0x01000000,
   0x01000000,
   0x00020000,
   0x00000400,
   0x03000000,			/* nop.m        */
   0x01000040,			/* mov b0=r8;;  */
   0x04800300,			/* nop.i;;      */
   0x00000400};
  unsigned int save_bundle[8];
  int num_stacked_regs;
  CORE_ADDR cur_psr;
  CORE_ADDR new_psr;
  CORE_ADDR old_psr;

  fp = FRAME_FP (frame);
  get_frame_saved_regs (frame, &fsr);

  /* RM: restore the static registers */
  for (regnum = GR0_REGNUM + 1; regnum < GR0_REGNUM + NUM_ST_GRS; regnum++)
    if ((regnum != SP_REGNUM) && (fsr.regs[regnum]))
      write_register (regnum, read_memory_integer (fsr.regs[regnum],
						   REGISTER_SIZE));

  /* RM: restore the stacked registers */
  /* RM: ??? we only need to restore the "out" registers */
  registers_changed();
  num_stacked_regs = (int) SIZE_OF_FRAME (read_register(CFM_REGNUM));
  
  for (regnum = GR0_REGNUM + NUM_ST_GRS; regnum < (GR0_REGNUM + NUM_ST_GRS + 
					   num_stacked_regs); regnum++) 
    if (fsr.regs[regnum])
    {
      write_register (regnum, read_memory_integer (fsr.regs[regnum],
						   REGISTER_SIZE));
    }

  /* restore the branch registers */
  for (regnum = BR0_REGNUM; regnum <= BRLAST_REGNUM; regnum++)
    if (fsr.regs[regnum])
      write_register (regnum, read_memory_integer (fsr.regs[regnum],
						   REGISTER_SIZE));
  /* RM: restore the predicate registers */
  for (regnum = PR0_REGNUM; regnum <= PRLAST_REGNUM; regnum++)
    if (fsr.regs[regnum])
      write_register (regnum, read_memory_integer (fsr.regs[regnum], 1));

  /* RM: restore the NaT registers */
  for (regnum = NR0_REGNUM; regnum < (NR0_REGNUM + NUM_ST_GRS +
				      num_stacked_regs); regnum++)
    if (fsr.regs[regnum])
      write_register (regnum, read_memory_integer (fsr.regs[regnum], 1));

  /* RM: restore the floating point registers */
  for (regnum = FR0_REGNUM; regnum <= FRLAST_REGNUM; regnum++)
    if (fsr.regs[regnum])
      {
	target_read_memory (fsr.regs[regnum], reg_buf, 12);
	/* write_register() takes a LONGEST, which is only 8
	   bytes. Hack this by writing the register directly */
	memcpy (&registers[REGISTER_BYTE (regnum)], reg_buf, 12);
	register_valid[regnum] = 1;
	target_store_registers (regnum);
      }

  /* RM: restore the application registers */
  for (regnum = AR0_REGNUM; regnum <= ARLAST_REGNUM; regnum++)
    if (fsr.regs[regnum])
      { if (   regnum != (AR0_REGNUM + 18)	/* don't restore BSPSTORE */
            && regnum != (AR0_REGNUM + 16) 	/* RSC Rgister Stack Configruation */
	    && regnum != (AR0_REGNUM + 17))     /* don't restore BSP */
	  {
	    CORE_ADDR new_value;

	    if (regnum == (AR0_REGNUM + 64))    /* Must not set reserved 
						   fields 61-58 and 51-38 */
	      {
		new_value = read_memory_integer (fsr.regs[regnum], 
						 REGISTER_SIZE);
		if (IS_TARGET_LRE)
		  endian_flip (&new_value, sizeof(new_value));
		new_value = new_value & AR64_RESERVED_MASK;
		write_register (regnum, new_value);
	      }
	    else if (regnum == (AR0_REGNUM + 40))
	      { /* Bits 63-58 are reserved in AR40 = Floating-point 
		   Status Register.
		   Also bit 12 is td (Traps disabled) of FPSR.sf0, which 
		     is reserved.
		   */
		
		new_value = read_memory_integer (fsr.regs[regnum], 
						 REGISTER_SIZE);
		new_value = new_value & AR40_RESERVED_MASK;
		write_register (regnum, new_value);
	      }
	    else
	      write_register (regnum, read_memory_integer (fsr.regs[regnum],
							   REGISTER_SIZE));
	    
	  }
      }

  if (fsr.regs[PSR_REGNUM])
    { /* We can only restore the user mask bits (0..4) of the PSR. */

      cur_psr = read_register (PSR_REGNUM);
      old_psr = read_memory_integer (fsr.regs[PSR_REGNUM], REGISTER_SIZE);
      new_psr = cur_psr & ~0x1f | old_psr & 0x1f;

      write_register (PSR_REGNUM, new_psr);
    }

/****** don't need to restore CFM. taken care by hardware *******/
  /*if (fsr.regs[CFM_REGNUM]){
    write_register (CFM_REGNUM,
		 read_memory_integer (fsr.regs[CFM_REGNUM], REGISTER_SIZE));
    }
*/

  write_register (SP_REGNUM, fp);

  /* If the PC was explicitly saved, then just restore it.  */
  if (fsr.regs[PC_REGNUM])
    {
      pc = read_memory_integer (fsr.regs[PC_REGNUM], REGISTER_SIZE);
      write_register (PC_REGNUM, pc);
    }
  /* Else use the value in %rp to set the new PC.  */
  else
    {
      pc = read_register (BR0_REGNUM);
      write_register (PC_REGNUM, pc);
    }

  /* The PC we just restored may be inside a return trampoline.  If so
     we want to restart the inferior and run it through the trampoline.

     Do this by setting a momentary breakpoint at the location the
     trampoline returns to. 

     Don't skip through the trampoline if we're popping a dummy frame.  */
  if (!PC_IN_CALL_DUMMY (frame->pc, frame->sp, frame->psp))
    {
#ifdef COVARIANT_SUPPORT
      /* For supporting covariant return types, dynamic skipping of trampoline
       * code is needed to find the target pc.  This code only does static 
       * analysis to find the target pc.  To fix it, the static analysis has 
       * to be done twice for dynamic skipping (covariant thunks for now).  
       * First analysis determines that a dynamic skip is needed and a dynamic 
       * skip is performed, after which another analysis is done to find the 
       * target pc.  If there is no covariant thunk to do a dynamic skip 
       * over, this loop works only once, i.e., one static analysis to find 
       * the target pc.
       */
      int i;

      for (i = 0; i < 2; i++)
        {
	  if (i == 0)
	    target_pc = SKIP_TRAMPOLINE_CODE (pc & ~0x3) & ~0x3;
          else
	    target_pc = SKIP_TRAMPOLINE_CODE (target_pc);

	  if (IS_PC_IN_COVARIANT_THUNK (pc) && target_pc == 0)
	      target_pc = DYNAMIC_TRAMPOLINE_NEXTPC (pc);
	  else
	    i++;
#else /* COVARIANT_SUPPORT */
	  target_pc = SKIP_TRAMPOLINE_CODE (pc & ~0x3) & ~0x3;
#endif /* COVARIANT_SUPPORT */

	  if (target_pc)
	    {
	      struct symtab_and_line sal;
	      struct breakpoint *breakpoint;
	      struct cleanup *old_chain;

	      /* Set up our breakpoint.   Set it to be silent as the MI code
		 for "return_command" will print the frame we returned to.  */
	      sal = find_pc_line (target_pc, 0);
	      sal.pc = target_pc;
	      breakpoint = set_momentary_breakpoint (sal, NULL, bp_finish);
	      breakpoint->silent = 1;

	      /* So we can clean things up.  */
	      old_chain = make_cleanup ((make_cleanup_ftype *) delete_breakpoint, 
					breakpoint);

	      /* Start up the inferior.  */
	      clear_proceed_status ();
	      proceed_to_finish = 1;
	      proceed ((CORE_ADDR)(long) - 1, TARGET_SIGNAL_DEFAULT, 0);

	      /* Perform our cleanups.  */
	      do_cleanups (old_chain);
	    }
#ifdef COVARIANT_SUPPORT
	}
#endif /* COVARIANT_SUPPORT */
    }
  flush_cached_frames ();
}

  
typedef struct {
  int	ar_enum;
  int 		ar_regnum;
} map_ar_enum_t;

/* We don't need to restore PFS, BSP and BSPSTORE. It will be taken care of 
   the return instruction. 
   uwx allows us to read only 
   UWX_REG_AR_PFS
   UWX_REG_AR_BSPSTORE
   UWX_REG_AR_RNAT
   UWX_REG_AR_UNAT
   UWX_REG_AR_FPSR
   UWX_REG_AR_LC
   For now restore only the relevant with in these. If in future we see a
   requirement to read anything else that uwx doesn't provide, we need to
   talk to them. */
map_ar_enum_t map_ar_enum [] = 
      { 
	{ UWX_REG_AR_RNAT,		AR0_REGNUM + 19},
	{ UWX_REG_AR_UNAT,		AR0_REGNUM + 36},
	{ UWX_REG_AR_FPSR,		AR0_REGNUM + 40},
	{ UWX_REG_AR_LC,		AR0_REGNUM + 65},
	{ -1,			-1}
      };

/* ia64_pop_real_frame
   Helper routine for ia64_pop_frame.  Pop a real call frame (not a call 
   dummy frame) off of the stack.  Use the unwind library to find out 
   the values of the registers to restore for the parent.
   */

void
ia64_pop_real_frame (CORE_ADDR pc, struct frame_info *frame)
{
  CORE_ADDR		bundle_addr;
  CORE_ADDR 		fp;
  CORE_ADDR 		psp;
  map_ar_enum_t* 	map_ar_enum_p;
  CORE_ADDR 		reg_val;
  register int 		regnum;
  char			save_stack_region[BUNDLE_SIZE];
  CORE_ADDR		sv_call_dummy_begin;
  CORE_ADDR		sv_call_dummy_end;
  CORE_ADDR		target_pc = 0;
  struct 
      target_waitstatus w;

  int status;

  /* return_bundle is a big-endian view of a bundle containing:
	br.ret.dptk.few  b0
	nop.b            0
	nop.b            0;;
     */
  const static int return_bundle[] = { 0x17200000, 0xa1000000, 
				       0x00001000, 0x00000020 };

  fp = FRAME_FP (frame);

  make_new_frame(pc, frame);
  if ((uc_distance == -1) || (current_uc == 0))
  {
    error ("Unable to unwind stack at PC 0x%llx\n", (long long) pc);
  }

  /* Get the previous frame's context to which we are trying to return to. */
  if (uc_distance == 0)
    {
      /* Bindu 022406: Copy all the registers out of the unwind context
         into frame before we change current_uc. */
      fill_regs_from_uc (current_uc, frame);

      status = MyUnwindContext_step (current_uc);
      if (status < UWX_OK)
	{
	  uc_distance = -1;
	  error ("Unable to unwind stack at PC 0x%llx\n", (long long) pc);
	}
      uc_distance++;
    }

  /* Restore the static registers and their NaT bits.  
     The stacked registers will be handled by the RSE stack which 
     will be updated by the return command we will execute.
     Just the preserved GRs should be enough.
     */
  for (regnum = 4; regnum <= 7; regnum++)
    {
      int nat_val;

      status = uwx_get_reg (current_uc, UWX_REG_GR(regnum), 
			    (UWX_ARG_ADDR64)
      &reg_val);
      if (status != UWX_OK)
	error ("unable to read reg from current_uc\n");
      write_register (GR0_REGNUM + regnum, reg_val);

      /* uwx doesn't properly read nats now. Uncomment this after
	 uwx is fixed. */
/*
      status = uwx_get_nat (current_uc, UWX_REG_GR(regnum), &nat_val);
      if (status != UWX_OK)
	error ("unable to read reg from current_uc\n");
      write_register (regnum + NR0_REGNUM, nat_val);
*/
    }

  /* restore the branch registers */
  for (regnum = 0; regnum <= 7; regnum++)
    {
      CORE_ADDR reg_val;
      status = uwx_get_reg (current_uc, UWX_REG_BR(regnum), 
			    (UWX_ARG_ADDR64) &reg_val);
      if (status != UWX_OK)
	error ("unable to read reg from current_uc\n");
      write_register (regnum + BR0_REGNUM, reg_val);
    }

  /* restore the predicate registers */
  {
    CORE_ADDR reg_val;
    status = uwx_get_reg (current_uc, UWX_REG_PREDS, 
			  (UWX_ARG_ADDR64) &reg_val);
    if (status != UWX_OK)
      error ("unable to read reg from current_uc\n");
    /* Write it as a single 64 bit register. */
    write_register (PR_REGNUM, reg_val);
  }

  /* restore the floating point registers FR0 and FR1 are note writable */
  for (regnum = FR0_REGNUM + 2; regnum <= FRLAST_REGNUM; regnum++)
    {

      CORE_ADDR	fr_value[2];
      status = uwx_get_reg (current_uc, UWX_REG_FR(regnum - FR0_REGNUM), 
			    (UWX_ARG_ADDR64) fr_value);
      if (status != UWX_OK)
	error ("unable to read reg from current_uc\n");

      /* Our floats are 12 bytes, but fr_value is 16 bytes with the
	 actual value starting at addr + 4.
	 */
     write_register_gen (regnum, 
			  (  (char*) fr_value) 
			   + 16 
			   - FLOAT_REG_RAW_SIZE);
    }

  /* restore the application registers.  Save AR64 to restore after the
     return
     */
  for (map_ar_enum_p = &map_ar_enum[0]; 
       map_ar_enum_p->ar_regnum != -1;
       map_ar_enum_p++)
    {
      int status;
      CORE_ADDR reg_val;
      regnum = map_ar_enum_p->ar_regnum;
      status = uwx_get_reg (current_uc, map_ar_enum_p->ar_enum, 
			    (UWX_ARG_ADDR64) &reg_val);
      if (status != UWX_OK)
	error ("unable to read ar from current_uc");
      write_register (regnum, reg_val);
    } /* for each AR enumeration */

  /* Copy the PC to BR0 so the return command will update PC to that value.
     Save the old parent value of BR0 to restore after the return.
     */

  pc = MyUnwindContext_getIPval (current_uc);
  write_register (BR0_REGNUM, pc);

  /* Before executing the return command, we copy the parent's CFM to
     AR64 (PFS).  The return instruction will copy AR64 to CFM to restore
     the parent's CFM.
     */

  write_register (AR0_REGNUM + 64, 
		  MyUnwindContext_getCFMval (current_uc));

  /* Restore SP, $gr12, SP_REGNUM */
      
  status = uwx_get_reg (current_uc, UWX_REG_SP, (UWX_ARG_ADDR64) &reg_val);
  if (status != UWX_OK)
    error ("unable to read SP from current_uc\n");
  write_register (SP_REGNUM, reg_val);

  /* We write to the stack a bundle with the following instructions:
	br.ret.dptk.few  b0
	nop.b            0
	nop.b            0;;
     We then write the address of the bundle to PC and ask ttrace to step
     one instruction.  Afterwards, we restore the stack locations we overwrote
     with the bundle and B0 which was used in the return.
     */

  /* Stack grows down, make sure bundle_addr > SP */
  bundle_addr = (fp & ~((CORE_ADDR)(BUNDLE_SIZE - 1))) + 2*BUNDLE_SIZE;  

  read_memory (bundle_addr, save_stack_region, BUNDLE_SIZE);
  write_memory (bundle_addr, (char*) &return_bundle[0], BUNDLE_SIZE);

  /* Set PC to point at the bundle we just wrote */

  write_register (PC_REGNUM, bundle_addr);
  stop_pc = bundle_addr;
  psp = MyUnwindContext_getPSPval (current_uc);
  sv_call_dummy_begin = call_dummy_begin;
  sv_call_dummy_end = call_dummy_end;
  call_dummy_begin = bundle_addr;
  call_dummy_end = bundle_addr + BUNDLE_SIZE;
  registers_changed ();
  flush_cached_frames ();

  step_1 (0, 1, 0, NULL);
  call_dummy_end = sv_call_dummy_end;
  call_dummy_begin = sv_call_dummy_begin;
  get_current_frame()->frame = psp;

  /* Restore the stack region where we wrote the bundle */
  write_memory (bundle_addr, save_stack_region, BUNDLE_SIZE);

  /* The PC we just restored may be inside a return trampoline.  If so
     we want to restart the inferior and run it through the trampoline.

     Do this by setting a momentary breakpoint at the location the
     trampoline returns to. 

     Don't skip through the trampoline if we're popping a dummy frame.  */
  if (!PC_IN_CALL_DUMMY (frame->pc, frame->sp, frame->psp))
    {
#ifdef COVARIANT_SUPPORT
      /* For supporting covariant return types, dynamic skipping of trampoline
       * code is needed to find the target pc.  This code only does static 
       * analysis to find the target pc.  To fix it, the static analysis has 
       * to be done twice for dynamic skipping (covariant thunks for now).  
       * First analysis determines that a dynamic skip is needed and a dynamic 
       * skip is performed, after which another analysis is done to find the 
       * target pc.  If there is no covariant thunk to do a dynamic skip 
       * over, this loop works only once, i.e., one static analysis to find 
       * the target pc.
       */
      int i;

      for (i = 0; i < 2; i++)
        {
	  if (i == 0)
	    target_pc = SKIP_TRAMPOLINE_CODE (pc & ~0x3) & ~0x3;
          else
	    target_pc = SKIP_TRAMPOLINE_CODE (target_pc);

	  if (IS_PC_IN_COVARIANT_THUNK (pc) && target_pc == 0)
	    target_pc = DYNAMIC_TRAMPOLINE_NEXTPC (pc);
	  else
	    i++;
#else /* COVARIANT_SUPPORT */
	  target_pc = SKIP_TRAMPOLINE_CODE (pc & ~0x3) & ~0x3;
#endif /* COVARIANT_SUPPORT */

	  if (target_pc)
	    {
	      struct symtab_and_line sal;
	      struct breakpoint *breakpoint;
	      struct cleanup *old_chain;

	      /* Set up our breakpoint.   Set it to be silent as the MI code
		 for "return_command" will print the frame we returned to.  */
	      sal = find_pc_line (target_pc, 0);
	      sal.pc = target_pc;
	      breakpoint = set_momentary_breakpoint (sal, NULL, bp_finish);
	      breakpoint->silent = 1;

	      /* So we can clean things up.  */
	      old_chain = make_cleanup ((make_cleanup_ftype *) delete_breakpoint, 
					breakpoint);

	      /* Start up the inferior.  */
	      clear_proceed_status ();
	      proceed_to_finish = 1;
	      proceed ((CORE_ADDR)(long) - 1, TARGET_SIGNAL_DEFAULT, 0);

	      /* Perform our cleanups.  */
	      do_cleanups (old_chain);
	    }
#ifdef COVARIANT_SUPPORT
	}
#endif /* COVARIANT_SUPPORT */
    }
}


/* Pop the current frame off of the stack.  On IA64, we can't write all of
   the registers we need to, so we do what we can and then execute a
   return instruction which will set the following registers for us:
      CFM	copied from PFS (AR64)
      BSP	updated based on current/old value of CFM and BSP
      BSP.store	updated based on current/old value of CFM BSP.store
      PC	updated from B0 where we write the return address.
   Of these, only PC is writable by gdb.
   */
   
void
ia64_pop_frame ()
{
  CORE_ADDR pc;
  struct frame_info *frame = get_current_frame ();

  pc = frame->pc;
  if (!pc)
    pc = read_register (PC_REGNUM);

  if (PC_IN_CALL_DUMMY (pc, 0, 0))
    ia64_pop_dummy_frame (pc, frame);
  else
    ia64_pop_real_frame (pc, frame);
} /* end ia64_pop_frame */

/* frame_regno_to_regno - map the number of a register which is saved in
   a frame to the general register number.  Only some registers are saved
   in a frame.  See LAST_FRAME_REGNO define.  -1 is returned for an
   invalid frame_regno, which shouldn't happen.
 */

int frame_regno_to_regno (int frame_regno)
{
  if (frame_regno < 0)
    {
      assert (0); /* Should not be called with an invalid regno */
      return -1;
    }

  if (frame_regno < FRLAST_REGNUM + 1)
    return frame_regno;
  if (frame_regno < (FRLAST_REGNUM + 1 + NUM_ST_GRS))
    return GR0_REGNUM + (frame_regno - (FRLAST_REGNUM + 1));
  if (frame_regno < (FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS))
     return BR0_REGNUM + (frame_regno - (FRLAST_REGNUM + 1 + NUM_ST_GRS));
  if (frame_regno < (FRLAST_REGNUM + 1 + NUM_ST_GRS + NUM_BRS + 1))
     return AR0_REGNUM + 64;
  
  assert (0); /* Should not be called with an invalid regno */
  return -1;  /* Not a frame register number! */
} /* end frame_regno_to_regno */

/* Exception Handling. on IPF. We can get the throw address from __cxa_throw/__cxa_rethrow for both catch and throw cases. We can get the catch location from __cxa_begin_catch for catch case and _Unwind_RaiseExceptionHook for throw case. See Exception Handling Design Doc for more information. */

CORE_ADDR saved_throw_addr = 0;

/* To check whether the internal breakpoints at __cxa_throw and __cxa_rethrow
   are already placed or not. */
int ex_internal = 0;

void
reset_ex_internal (void *ignore)
{
  ex_internal = 0;
}

/* Return the low_pc of the catch block with selector value SELECTOR in the
   symbol SYM */
CORE_ADDR
get_catch_addr_from_sym (struct symbol *sym, unsigned long long selector)
{
  struct try_block *t_block = SYMBOL_TRY_BLOCK (sym);
  struct catch_block *c_block;
  while (t_block)
    {
      c_block = t_block->c_block;
      while (c_block)
        {
	  if (c_block->selector_val == selector)
	    return c_block->low_pc;
	  c_block = c_block->next;
        }
      t_block = t_block->next;
    }
  return 0;
}

/* Return sal for __cxa_begin_catch for KIND EX_EVENT_CATCH and
   _Unwind_RaiseExceptionHook for KIND EX_CATCH_THROW. Place
   internal breakpoints at __cxa_throw and __cxa_rethrow for both cases. */
struct symtab_and_line *
child_enable_exception_callback (enum exception_event_kind kind, int ignore)
{
  struct minimal_symbol* minsym;

  if (!break_callback_sal)
    {
      break_callback_sal = (struct symtab_and_line *) xmalloc (sizeof (struct symtab_and_line));
    }
  INIT_SAL (break_callback_sal);
  break_callback_sal->symtab = NULL;
  break_callback_sal->line = 0;

  switch (kind)
    {
      case EX_EVENT_THROW:
      case EX_EVENT_CATCH:
	if (!ex_internal)
  	  {
            /* place a breakpoint at __cxa_throw */
            minsym = lookup_minimal_symbol ("__cxa_throw", NULL, NULL);
            break_callback_sal->pc = SYMBOL_VALUE_ADDRESS (minsym);
            break_callback_sal->end = SYMBOL_VALUE_ADDRESS (minsym);
            create_exception_catchpoint (0, 0, EX_EVENT_INTERNAL,
					 break_callback_sal);

            /* Place a breakpoint at __cxa_rethrow. */
            minsym = lookup_minimal_symbol ("__cxa_rethrow", NULL, NULL);
            break_callback_sal->pc = SYMBOL_VALUE_ADDRESS (minsym);
            break_callback_sal->end = SYMBOL_VALUE_ADDRESS (minsym);
            create_exception_catchpoint (0, 0, EX_EVENT_INTERNAL,
					 break_callback_sal);
            ex_internal = 1;
            make_run_cleanup (reset_ex_internal, (void *)NULL);
	  }

        if (kind == EX_EVENT_CATCH)
	  {
            /* return the sal for __cxa_begin_catch. */
            minsym = lookup_minimal_symbol ("__cxa_begin_catch", NULL, NULL);
	    if (!minsym)
	      {
	        warning ("Unable to find __cxa_begin_catch. No catchpoint set.");
		return NULL;
	      }
	  }
	else
	  {
            /* return the sal for _Unwind_RaiseExceptionHook. */
            minsym = lookup_minimal_symbol ("_Unwind_RaiseExceptionHook",
						 NULL, NULL);
	    if (!minsym)
	      {
		warning ("Unable to find _Unwind_RaiseExceptionHook. No catchpoint set.");
		return NULL;
	      }
	  }
        break_callback_sal->pc = SYMBOL_VALUE_ADDRESS (minsym);
        break_callback_sal->end = SYMBOL_VALUE_ADDRESS (minsym);
        return break_callback_sal;
      default:
        error ("Request to enable unknown or unsupported exception event.");
	/* slience aCC6 warning about no return value */
	return NULL;
    }
}

/* This function is called when a exception catchpoint is triggered. Return
   the exception_event_record decribing the event. */
struct exception_event_record *
child_get_current_exception_event ()
{
  struct frame_info *fi, *curr_frame;
  int level = 1;
  CORE_ADDR catch_addr = 0;
  CORE_ADDR stop_pc = read_pc ();
  struct minimal_symbol* minsym = lookup_minimal_symbol_by_pc (stop_pc);

  curr_frame = get_current_frame ();
  if (!curr_frame)
    return (struct exception_event_record *) NULL;

  if (   (strcmp (SYMBOL_NAME (minsym), "__cxa_throw") == 0)
      || (strcmp (SYMBOL_NAME (minsym), "__cxa_rethrow") == 0))
    {
      /* Move up by one frame to get the throw addr. */
      fi = find_relative_frame (curr_frame, &level);
      if (level != 0)
        return (struct exception_event_record *) NULL;
      saved_throw_addr = fi->pc;
      return &current_ex_event;
    }
  else if (strcmp (SYMBOL_NAME (minsym), "__cxa_begin_catch") == 0)
    {
      /* Move up by one frame to get the catch addr. */
      fi = find_relative_frame (curr_frame, &level);
      if (level != 0)
        return (struct exception_event_record *) NULL;
      catch_addr = fi->pc;
      current_ex_event.kind = EX_EVENT_CATCH;
      current_ex_event.throw_sal = find_pc_line (saved_throw_addr, 1);
      current_ex_event.catch_sal = find_pc_line (catch_addr, 1);
      return &current_ex_event;
    }
  else if (strncmp (SYMBOL_SOURCE_NAME (minsym), "_Unwind_RaiseExceptionHook", 26) == 0)
    {
      /* arg1 is landing_pad address, arg2 is selector value. */
      CORE_ADDR landing_pad = read_register (ARG0_REGNUM);
      unsigned long long selector = read_register (ARG1_REGNUM);
      struct minimal_symbol *msym;
      struct symbol * sym;

      if (landing_pad != 0)
        {
          msym = lookup_minimal_symbol_by_pc (landing_pad);
          if (!msym)
	    warning ("Unable to find the catch address");
          else
	    {
              sym = find_pc_function (SYMBOL_VALUE_ADDRESS(msym));
              if (!sym || !SYMBOL_TRY_BLOCK (sym))
	        warning ("Unable to find the catch address");
	      else
                catch_addr = get_catch_addr_from_sym (sym, selector);
	    }
        }
      current_ex_event.kind = EX_EVENT_THROW;
      current_ex_event.throw_sal = find_pc_line (saved_throw_addr, 1);
      current_ex_event.catch_sal = find_pc_line (catch_addr, 0);
      return &current_ex_event;
    }
  return NULL;
}

/* On ia64, 3 instructions go into one address. gdb encodes each of these 
   instructions by using 1 and 0 bits. i.e., addresses end with 0, 1 and 2.
   Decrement PC_VAL by 1 using this encoding. */
CORE_ADDR
ia64_decr_pc_by_1 (CORE_ADDR pc_val)
{
  int slot_no;
  slot_no = (int) (pc_val & 0xF);
  pc_val = pc_val & ~0xFLL;
  switch (slot_no)
    {
      case 0:
        pc_val = pc_val - 14;
        break;
      case 1:
        pc_val = pc_val;
        break;
      case 2:
        pc_val = pc_val + 1;
        break;
      default:
        error ("Encountered wrong text address.");
      }
  return pc_val;
}

/* info catch command for IPF.
   implementing "info catch"
		"info catch all"
		"info catch *<address>"
		"info catch <filename>:<line_no>"
		"info catch <line_no>"
		"info catch <function_name>"
 */
void
ia64_catch_info (char* arg, int from_tty)
{
  struct minimal_symbol *msym;
  struct symbol *sym = 0;
  CORE_ADDR pc;
  struct symtab_and_line sal;
  struct symtabs_and_lines sals;
  struct try_block *t_block = 0;
  struct catch_block *c_block;
  struct frame_info *fi;
  int printall = 0;
  struct symtab *symtab;
  char *filename;
  char *ptr;
  int line;

  if (!target_has_execution)
    noprocess ();

  if (arg && strcmp (arg, "all") == 0)
    printall =  1;

  if (printall)
    {
      /* Start from the topmost farme. */
      fi = get_current_frame ();
      printf_filtered ("Catch locations are at ...\n");
    }
  else
    {
       /* Use currently selected frame. */
       fi = selected_frame;
       printf_filtered ("Catch locations in this frame are at ...\n");
    }

  while (fi)
    {
      if (printall || !arg)
        {
	  /* For "info catch" and "info catch all" use frame pc. */
	  pc = fi->pc;

	  /* If not current frame, we are looking at return pc.
	     Decrement by 1 to get the previous pc. */
	  if (fi != get_current_frame ())
	    pc = ia64_decr_pc_by_1 (pc);
	}
      else
        {
	  if (arg[0] == '*')
	    {
	      /* info catch *<address> */
	      pc = parse_and_eval_address (arg + 1);
	    }
	  else
	    {
	      if ((ptr = strstr (arg, ":")) && (ptr[1] != ':'))
	        {
		  /* Process filename::line here. Filename ends with a
		     while space or a :. line number starts after a :. */

		  filename = arg;
		  while (arg[0] != ':')
  		    {
		      arg++;
		      if (arg[0] == ' ')
			arg[0] = '\0';
		    }
		  arg[0] = '\0';
		  arg++;
		  if (arg[0] == '\0')
		    error ("No line number specified.");

		  line = atoi (arg);
		  symtab = lookup_symtab (filename);
		  if (!symtab)
		    error ("No file %s", filename);
	          if (!find_line_pc (symtab, &line, &pc, 0))
		    error ("No line %d, in file %s", line, filename);
		}
	      else
		{
		  /* If there is no :, consider arg as function name first.
		     If not a function name, consider it as lineno in the
		     current symtab. */
		  msym = lookup_minimal_symbol (arg, 0, 0);
		  if (!msym)
		    {
		      int line = atoi (arg);
		      if (!find_line_pc (get_default_breakpoint_symtab (), &line, &pc, 0))
		        error ("Function or line %s not found", arg);
		    }
		  else
		    pc = SYMBOL_VALUE_ADDRESS (msym);
		}
	    }
        }
      t_block = 0;
      msym = lookup_minimal_symbol_by_pc (pc);
      if (!msym)
        printf_filtered ("No symbol information for frame with PC value 0x%llx\n", pc);
      else 
        {
          sym = find_pc_function (SYMBOL_VALUE_ADDRESS(msym));
          if (!sym)
            printf_filtered ("No symbol information for frame with PC value 0x%llx\n", pc);
	  else
            t_block = SYMBOL_TRY_BLOCK (sym);
        }

      /* Iterate over all the try blocks for this symbol , check if pc falls
	 in any of these and print all their catch blocks. */
      while (t_block)
        {
          if (pc >= t_block->low_pc && pc < t_block->high_pc)
	    {
	      c_block = t_block->c_block;
              while (c_block)
                {
                  sal = find_pc_line (c_block->low_pc, 0);
	          printf_filtered ("%s:%d", getbasename (sal.symtab->filename),
				   sal.line);
	          /* Pass in show=2, so that type_print will not print
		     void in front of catch. */
	          type_print (c_block->type, "catch", gdb_stdout, 2);
	          printf_filtered (" in %s()\n", SYMBOL_SOURCE_NAME (sym));
	          c_block = c_block->next;
	        }
	    }
          t_block = t_block->next;
        }
      /* If printall, continue in this while loop. Else break out. */
      if (printall)
        fi = get_prev_frame (fi);
      else
        break;
    }
  return;
}

/*************** Start functions for nimbus register printing **************

   Stacey - 3/10/2002

   The several pages of code below are used for printing changed registers 
   from nimbus in disassembly mode.  To see where the code ends, search for
   a comment containing:

   End functions for nimbus register printing 


   FIXME:
   Future Enhancements
   1.  Use copies of standard gdb data structures instead of Faxing's special
       data structures.  It would make the code more efficient and more
       readable in some cases.  */
   


/* This function takes a registers number, old value, and new value, and
   prints them out in a human readable format.  This gets called by 
   print_changed_registers () after a register has been identified as being
   changed.  

   Limitation: This function won't handle floating point registers
               For floating point registers, see ia64_print_fp_reg_value_pair
	       which does exactly the same thing as this function.  Floating
	       point registers are a different size from other registers
	       and therefore need to be handled specially  */

/* The register names in the disassembly library are different from the
   register names gdb uses.  GDB thinks of registers as r1, r3, etc.
   The names from the disassembly library might be gr1.  This array is used
   to help us map back and forth between them.  */

char *orig_reg_names [NUM_REGS] = {
  "f0",    "f1",    "f2",    "f3",    "f4",    "f5",    "f6",    "f7",
  "f8",    "f9",    "f10",   "f11",   "f12",   "f13",   "f14",   "f15",
  "f16",   "f17",   "f18",   "f19",   "f20",   "f21",   "f22",   "f23",
  "f24",   "f25",   "f26",   "f27",   "f28",   "f29",   "f30",   "f31",
  "f32",   "f33",   "f34",   "f35",   "f36",   "f37",   "f38",   "f39",
  "f40",   "f41",   "f42",   "f43",   "f44",   "f45",   "f46",   "f47",
  "f48",   "f49",   "f50",   "f51",   "f52",   "f53",   "f54",   "f55",
  "f56",   "f57",   "f58",   "f59",   "f60",   "f61",   "f62",   "f63",
  "f64",   "f65",   "f66",   "f67",   "f68",   "f69",   "f70",   "f71",
  "f72",   "f73",   "f74",   "f75",   "f76",   "f77",   "f78",   "f79",
  "f80",   "f81",   "f82",   "f83",   "f84",   "f85",   "f86",   "f87",
  "f88",   "f89",   "f90",   "f91",   "f92",   "f93",   "f94",   "f95",
  "f96",   "f97",   "f98",   "f99",   "f100",  "f101",  "f102",  "f103",
  "f104",  "f105",  "f106",  "f107",  "f108",  "f109",  "f110",  "f111",
  "f112",  "f113",  "f114",  "f115",  "f116",  "f117",  "f118",  "f119",
  "f120",  "f121",  "f122",  "f123",  "f124",  "f125",  "f126",  "f127",

  "nr0",   "nr1",   "nr2",   "nr3",   "nr4",   "nr5",   "nr6",   "nr7",
  "nr8",   "nr9",   "nr10",  "nr11",  "nr12",  "nr13",  "nr14",  "nr15",
  "nr16",  "nr17",  "nr18",  "nr19",  "nr20",  "nr21",  "nr22",  "nr23",
  "nr24",  "nr25",  "nr26",  "nr27",  "nr28",  "nr29",  "nr30",  "nr31",
  "nr32",  "nr33",  "nr34",  "nr35",  "nr36",  "nr37",  "nr38",  "nr39",
  "nr40",  "nr41",  "nr42",  "nr43",  "nr44",  "nr45",  "nr46",  "nr47",
  "nr48",  "nr49",  "nr50",  "nr51",  "nr52",  "nr53",  "nr54",  "nr55",
  "nr56",  "nr57",  "nr58",  "nr59",  "nr60",  "nr61",  "nr62",  "nr63",
  "nr64",  "nr65",  "nr66",  "nr67",  "nr68",  "nr69",  "nr70",  "nr71",
  "nr72",  "nr73",  "nr74",  "nr75",  "nr76",  "nr77",  "nr78",  "nr79",
  "nr80",  "nr81",  "nr82",  "nr83",  "nr84",  "nr85",  "nr86",  "nr87",
  "nr88",  "nr89",  "nr90",  "nr91",  "nr92",  "nr93",  "nr94",  "nr95",
  "nr96",  "nr97",  "nr98",  "nr99",  "nr100", "nr101", "nr102", "nr103",
  "nr104", "nr105", "nr106", "nr107", "nr108", "nr109", "nr110", "nr111",
  "nr112", "nr113", "nr114", "nr115", "nr116", "nr117", "nr118", "nr119",
  "nr120", "nr121", "nr122", "nr123", "nr124", "nr125", "nr126", "nr127",

  "p0",    "p1",    "p2",    "p3",    "p4",    "p5",    "p6",    "p7",
  "p8",    "p9",    "p10",   "p11",   "p12",   "p13",   "p14",   "p15",
  "p16",   "p17",   "p18",   "p19",   "p20",   "p21",   "p22",   "p23",
  "p24",   "p25",   "p26",   "p27",   "p28",   "p29",   "p30",   "p31",
  "p32",   "p33",   "p34",   "p35",   "p36",   "p37",   "p38",   "p39",
  "p40",   "p41",   "p42",   "p43",   "p44",   "p45",   "p46",   "p47",
  "p48",   "p49",   "p50",   "p51",   "p52",   "p53",   "p54",   "p55",
  "p56",   "p57",   "p58",   "p59",   "p60",   "p61",   "p62",   "p63",

  "r0",    "r1",    "r2",    "r3",    "r4",    "r5",    "r6",    "r7",
  "r8",    "r9",    "r10",   "r11",   "r12",   "r13",   "r14",   "r15",
  "r16",   "r17",   "r18",   "r19",   "r20",   "r21",   "r22",   "r23",
  "r24",   "r25",   "r26",   "r27",   "r28",   "r29",   "r30",   "r31",
  "r32",   "r33",   "r34",   "r35",   "r36",   "r37",   "r38",   "r39",
  "r40",   "r41",   "r42",   "r43",   "r44",   "r45",   "r46",   "r47",
  "r48",   "r49",   "r50",   "r51",   "r52",   "r53",   "r54",   "r55",
  "r56",   "r57",   "r58",   "r59",   "r60",   "r61",   "r62",   "r63",
  "r64",   "r65",   "r66",   "r67",   "r68",   "r69",   "r70",   "r71",
  "r72",   "r73",   "r74",   "r75",   "r76",   "r77",   "r78",   "r79",
  "r80",   "r81",   "r82",   "r83",   "r84",   "r85",   "r86",   "r87",
  "r88",   "r89",   "r90",   "r91",   "r92",   "r93",   "r94",   "r95",
  "r96",   "r97",   "r98",   "r99",   "r100",  "r101",  "r102",  "r103",
  "r104",  "r105",  "r106",  "r107",  "r108",  "r109",  "r110",  "r111",
  "r112",  "r113",  "r114",  "r115",  "r116",  "r117",  "r118",  "r119",
  "r120",  "r121",  "r122",  "r123",  "r124",  "r125",  "r126",  "r127",

  "b0",    "b1",    "b2",    "b3",    "b4",    "b5",    "b6",    "b7",

  "ar0",   "ar1",   "ar2",   "ar3",   "ar4",   "ar5",   "ar6",   "ar7",
  "ar8",   "ar9",   "ar10",  "ar11",  "ar12",  "ar13",  "ar14",  "ar15",
  "ar16",  "ar17",  "ar18",  "ar19",  "ar20",  "ar21",  "ar22",  "ar23",
  "ar24",  "ar25",  "ar26",  "ar27",  "ar28",  "ar29",  "ar30",  "ar31",
  "ar32",  "ar33",  "ar34",  "ar35",  "ar36",  "ar37",  "ar38",  "ar39",
  "ar40",  "ar41",  "ar42",  "ar43",  "ar44",  "ar45",  "ar46",  "ar47",
  "ar48",  "ar49",  "ar50",  "ar51",  "ar52",  "ar53",  "ar54",  "ar55",
  "ar56",  "ar57",  "ar58",  "ar59",  "ar60",  "ar61",  "ar62",  "ar63",
  "ar64",  "ar65",  "ar66",  "ar67",  "ar68",  "ar69",  "ar70",  "ar71",
  "ar72",  "ar73",  "ar74",  "ar75",  "ar76",  "ar77",  "ar78",  "ar79",
  "ar80",  "ar81",  "ar82",  "ar83",  "ar84",  "ar85",  "ar86",  "ar87",
  "ar88",  "ar89",  "ar90",  "ar91",  "ar92",  "ar93",  "ar94",  "ar95",
  "ar96",  "ar97",  "ar98",  "ar99",  "ar100", "ar101", "ar102", "ar103",
  "ar104", "ar105", "ar106", "ar107", "ar108", "ar109", "ar110", "ar111",
  "ar112", "ar113", "ar114", "ar115", "ar116", "ar117", "ar118", "ar119",
  "ar120", "ar121", "ar122", "ar123", "ar124", "ar125", "ar126", "ar127",

  "ip",    "cr_ifs", "cr_ipsr"
};

static void
ia64_print_register_value_pair(int regnum, long long old_val, long long new_val)
{
  if (!((regnum >= FR0_REGNUM) && (regnum <= FRLAST_REGNUM)))
    {
      printf_unfiltered ("%5.5s:  %16llx  =>  %16llx\n",
			 reg_names[regnum], old_val, new_val);
    }
}

/* This function is a floating point counterpart to 
   ia64_print_register_value_pair

   Floating point registers are different sizes from other registers
   and therefore need to be handled specially.  This function takes a 
   floating point register's number, old value, and new value, and prints 
   them out in a human readable format.  This gets called by
   print_changed_registers () after a floating point register has been 
   identified as being changed.  */

static void
ia64_print_fp_reg_value_pair(int regnum, char *old_val, char *new_val)
{
  double d1, d2;

  floatformat_to_double (&floatformat_ia64, old_val, &d1);
  floatformat_to_double (&floatformat_ia64, new_val, &d2);

  fprintf_unfiltered (gdb_stdout, "%5.5s: ", reg_names[regnum]);
  fputs_filtered ("(double precision) ", gdb_stdout);
    
  val_print (builtin_type_float, (char *) &d1, 0, 0, 
	     gdb_stdout, 0, 1, 0, Val_pretty_default);

  fputs_filtered ("  =>  ", gdb_stdout);

  val_print (builtin_type_float, (char *) &d2, 0, 0, 
	     gdb_stdout, 0, 1, 0, Val_pretty_default);

  printf_filtered ("\n");
}

/* 
   ** ia64_register_parser (char *)
   ** This function maps an original register name to a gdb register name
   ** and does some transformations at the same time.
*/

int
ia64_register_parser (char * arg)
{
  extern char * orig_reg_names[NUM_REGS];
  int i, start, end, dollar_preceding = 0;
  char temp[80];

  /* Return immediately if we're not running under nimbus disassembly
     mode or if the thing we're dealing with is not a register.  */
  if (!nimbus_version || !assembly_level_debugging)
    return 0;

  /* ignore the beginning '*' character and any white space in the register
     name */

  if (*arg == '*')
    arg++;
  while (isspace(*arg))
    arg++;

  /* if there's a dollar sign in front, this is probably not a register
     we should return here */

  if (*arg == '$') {
    dollar_preceding++;
    arg++;
  }

  /* faxing, 07312001, 
     the following code is supposed to make the loop faster,
     * it is specific to ia64 registers. */

  if (strlen (arg) >= 8) /* all ia64 reg names is less that 8 chars */
    return 0;

  /* Check what kind of register this is.  If we have a floating point
     register, we only have to search through the floating point section,
     etc.  */

  switch (arg[0])
    {
    case 'f': 
      start = FR0_REGNUM;	/* 0 */ 
      end = FRLAST_REGNUM;	/* 127 */ 
      break;

    case 'n': 
      start = NR0_REGNUM;	/* 128 */ 
      end = NRLAST_REGNUM;	/* 255 */ 
      break;

    case 'p': 
      if (arg[1] != 's')
	{
	  start = PR0_REGNUM;		/* 256 */ 
	  end = PRLAST_REGNUM;	/* 319 */ 
	}
      else {
	start = PC_REGNUM;		/* 584 */   
	end = PSR_REGNUM;   	/* 586 */ 
      }
      break;

    case 'g':
    case 'r': 
      start = GR0_REGNUM;	/* 320 */ 
      end = GRLAST_REGNUM;	/* 447 */ 
      break;

    case 'b': 
      start = BR0_REGNUM;	/* 448 */ 
      end = BRLAST_REGNUM;	/* 455 */ 
      break;

    case 'a': 
      start = AR0_REGNUM;	/* 456 */ 
      end = ARLAST_REGNUM;	/* 583 */ 
      break;

    default:  
      start = PC_REGNUM;	/* 584 */   
      end = PSR_REGNUM;   	/* 586 */ 
      break;
    }

  for (i = start; i <= end; i++)
    {
      if (!strcmp (arg, orig_reg_names[i]))
    	{
	  if (!dollar_preceding)
	    {
	      sprintf (temp, "$%s", reg_names[i]);
	      strcpy (arg, temp);
	      return 1;
	    }
	  else return 0;
	}
      else 
	if (!strcmp (arg, reg_names[i]))
	  {
    	    if (!dollar_preceding)
	      {
	        sprintf (temp, "$%s", arg);
		strcpy (arg, temp);
		return 1;
	      }
	    else return 0;
	  }
    }
  return 0;
}

/* save_old_register_values()
   This function saves all registers register values so that we can compare
   them with the new values the next time gdb stops.  Registers are only
   saved if the user has specified that he wants to see changed registers
   by setting the print_register_enabled flag to 1.  

   FIXME:
   We should really be using copies of standard gdb data structures here.  
   It would make simpler and faster */

void
save_old_register_values ()
{
  extern int print_changed_registers_enabled;
  unsigned char raw_regs[REGISTER_BYTES];
  unsigned char raw_buffer[MAX_REGISTER_RAW_SIZE];
  int i;
  int num_stacked_regs;

  if (!print_changed_registers_enabled)
    return;

  if (!target_has_registers || selected_frame == NULL)
    return;

  if (saved_register_values) 
    {
      free (saved_register_values);
    }

  if ( (saved_register_values = 
       (long long *) malloc ((NUM_REGS - PR0_REGNUM) * 
			     sizeof (long long))) == NULL)
    {
      error("Error: memory allocation failed.\n");
      return;
    }

  num_stacked_regs = (int) SIZE_OF_FRAME (read_register(CFM_REGNUM));

  for (i = PR0_REGNUM; i < NUM_REGS; i++)
    {
      /* Skip the unreadable AR registers */
      if (   ((i >= AR0_REGNUM )&& (i < AR0_REGNUM + 16))
             || (   (i > AR0_REGNUM + 19)
		    && (i != AR0_REGNUM + 32)
		    && (i != AR0_REGNUM + 36)
		    && (i != AR0_REGNUM + 40)
		    && (i != AR0_REGNUM + 64)
		    && (i != AR0_REGNUM + 65)
		    && (i != AR0_REGNUM + 66)
		    && (i <= ARLAST_REGNUM)))
	continue;

      if (!((i>= GR0_REGNUM) && (i <= GRLAST_REGNUM)))
        {
	  read_relative_register_raw_bytes (i, (char *) (raw_regs +
							 REGISTER_BYTE (i)));
	    
	  saved_register_values[i - PR0_REGNUM] = 
	    extract_signed_integer ((char *) (raw_regs + REGISTER_BYTE (i)), 
				    REGISTER_RAW_SIZE (i));
        }
      else {
	/*
	 * Print the GRs and NRs together in the format
	 * grnum: value natr: natvalue
	 */
	if (i >= (GR0_REGNUM + 32 + num_stacked_regs))
	  continue;

	read_relative_register_raw_bytes (i,
					  (char *) (raw_regs +
						    REGISTER_BYTE (i)));
	saved_register_values[i - PR0_REGNUM] = 
	  extract_signed_integer ((char *) (raw_regs + REGISTER_BYTE (i)), 
				  REGISTER_RAW_SIZE (i));

	read_relative_register_raw_bytes (i - GR0_REGNUM + NR0_REGNUM,
					  (char *) (raw_regs +
						    REGISTER_BYTE (i -
								   GR0_REGNUM+
								   NR0_REGNUM)
						    ));
	saved_register_values[i - GR0_REGNUM + NR0_REGNUM - PR0_REGNUM] =
	  extract_signed_integer ((char *) (raw_regs +
					    REGISTER_BYTE (i - GR0_REGNUM +
							   NR0_REGNUM)),
				  REGISTER_RAW_SIZE (i - GR0_REGNUM +
						     NR0_REGNUM));
      }
    }

  if (saved_fp_register_values)
    free (saved_fp_register_values);

  if (   (saved_fp_register_values = (saved_fp_register_values_type) 
	     xmalloc (  (FRLAST_REGNUM - FR0_REGNUM + 1) 
		      * sizeof(char[MAX_REGISTER_VIRTUAL_SIZE]))) 
      == NULL)
    {
      error("Error: memory allocation failed.\n");
      return;
    }

  for (i = FR0_REGNUM; i <= FRLAST_REGNUM; i++)
    {
      memset (saved_fp_register_values[i - FR0_REGNUM], '\0',
	      MAX_REGISTER_VIRTUAL_SIZE);

      read_relative_register_raw_bytes (i, (char *)raw_buffer);

      /* Put it in the buffer.  No conversions are ever necessary.  */
      memcpy (saved_fp_register_values[i - FR0_REGNUM], raw_buffer,
	      REGISTER_RAW_SIZE (i));
    }
}

/* print_changed_registers()
   This function checks to see if any registers have changed by comparing
   the current values with saved values obtained thru earlier calls to 
   save_old_register_values ().  This function also prints changed register 
   values by making calls to the ia64_print_value_pairs function and its 
   floating point counterpart.  Changed registers are printed with old/new
   value pairs.  */

void
print_changed_registers ()
{
  extern int print_changed_registers_enabled;
  char raw_regs[REGISTER_BYTES];
  long long reg_val;
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];
  int i;
  int num_stacked_regs;

  if (!print_changed_registers_enabled)
    return;

  if (!target_has_registers || selected_frame == NULL)
    return;

  if (saved_register_values == NULL)
    {
      save_old_register_values ();
      return;
    }

  num_stacked_regs = (int) SIZE_OF_FRAME (read_register(CFM_REGNUM));

  for (i = PR0_REGNUM; i < NUM_REGS; i++)
    {
      /* Skip the unreadable AR registers */
      if (   ((i >= AR0_REGNUM )&& (i < AR0_REGNUM + 16))
             || (   (i > AR0_REGNUM + 19)
		    && (i != AR0_REGNUM + 32)
		    && (i != AR0_REGNUM + 36)
		    && (i != AR0_REGNUM + 40)
		    && (i != AR0_REGNUM + 64)
		    && (i != AR0_REGNUM + 65)
		    && (i != AR0_REGNUM + 66)
		    && (i <= ARLAST_REGNUM)))
	continue;

      if (!((i>= GR0_REGNUM) && (i <= GRLAST_REGNUM)))
        {
	  read_relative_register_raw_bytes
	    (i, (char *) (raw_regs + REGISTER_BYTE (i)));
	  reg_val =
	    extract_signed_integer ((char *) (raw_regs + REGISTER_BYTE (i)), 
				    REGISTER_RAW_SIZE (i));

	  if (saved_register_values[i - PR0_REGNUM] != reg_val)
            {
	      ia64_print_register_value_pair(i,
					     saved_register_values[i - 
								  PR0_REGNUM],
					     reg_val);
	      saved_register_values[i - PR0_REGNUM] = reg_val;
            }
        }
      else {
	/*
	 * Print the GRs and NRs together in the format
	 * grnum: value natr: natvalue
	 */
	if (i >= (GR0_REGNUM + 32 + num_stacked_regs))
	  continue;
	read_relative_register_raw_bytes (i,
					  (char *) (raw_regs +
						    REGISTER_BYTE (i)));
	reg_val = 
	  extract_signed_integer ( (char *) (raw_regs + REGISTER_BYTE (i)),
				   REGISTER_RAW_SIZE (i));

	if (saved_register_values[i - PR0_REGNUM] != reg_val)
	  {
	    ia64_print_register_value_pair(i,
					   saved_register_values[i - 
								PR0_REGNUM], 
					   reg_val);
								
	    saved_register_values[i - PR0_REGNUM] = reg_val;
	  }

	read_relative_register_raw_bytes (i -
					  GR0_REGNUM + NR0_REGNUM,
					  (char *) (raw_regs +
						    REGISTER_BYTE (i -
								   GR0_REGNUM+
								   NR0_REGNUM)
						    ));
	reg_val = 
	  extract_signed_integer ((char *) (raw_regs + 
					    REGISTER_BYTE (i - GR0_REGNUM +
							   NR0_REGNUM)),
				  REGISTER_RAW_SIZE (i - GR0_REGNUM + 
						     NR0_REGNUM));
	if (saved_register_values[i - GR0_REGNUM + NR0_REGNUM - PR0_REGNUM] !=
	    reg_val)
	  {
	    ia64_print_register_value_pair (i, 
					    saved_register_values[i - 
								 GR0_REGNUM +
								 NR0_REGNUM -
								 PR0_REGNUM],
					    reg_val);
	    saved_register_values[i - GR0_REGNUM + NR0_REGNUM - PR0_REGNUM] = 
	      reg_val;
	  }
      }
    }

  for (i = FR0_REGNUM; i <= FRLAST_REGNUM; i++)
    {
      memset (virtual_buffer, '\0', MAX_REGISTER_VIRTUAL_SIZE);

      read_relative_register_raw_bytes (i, (char *)raw_buffer);

      /* Put it in the buffer.  No conversions are ever necessary.  */
      memcpy (virtual_buffer, raw_buffer, REGISTER_RAW_SIZE (i));

      if (memcmp (saved_fp_register_values[i - FR0_REGNUM], virtual_buffer,
		  MAX_REGISTER_VIRTUAL_SIZE))
        {
	  ia64_print_fp_reg_value_pair (i, saved_fp_register_values[i -
								   FR0_REGNUM],
					virtual_buffer);

	  memcpy (saved_fp_register_values[i - FR0_REGNUM], virtual_buffer,
		  MAX_REGISTER_VIRTUAL_SIZE);
        }
    }
}

/*************** End functions for nimbus register printing **************/


#define PTR_SIZE (is_swizzled ? 4 : 8)

/* Read the environment of the inferior and return the value of PWD
   environment variable */
char*
get_inferior_cwd()
{
  CORE_ADDR arg_list_addr;
  CORE_ADDR inf_env_addr, env_str_addr;
  char buf[8];
  char str_pwd[] = "PWD=";
  char env_str[sizeof(str_pwd)];
  static char cwd_path[MAXPATHLEN];
  int status;

  if (inferior_pid == 0)
    return NULL;

  /* Read the argumant list */
  status = call_ttrace (TT_PROC_GET_ARGS,
                        inferior_pid,
                        (TTRACE_ARG_TYPE) &arg_list_addr,
                        (TTRACE_ARG_TYPE) sizeof (CORE_ADDR),
                        (TTRACE_ARG_TYPE) TT_NIL);
  if (status == -1)
    return NULL;

  /* Read the envirinment pointer */
  status = target_read_memory (arg_list_addr + 2 * sizeof (CORE_ADDR),
                               (char *) &inf_env_addr,
                               sizeof (CORE_ADDR));
  if (status != 0)
    return NULL;

  /* Read environment variables and locate PWD */
  if (target_read_memory (inf_env_addr, buf, PTR_SIZE))
      return NULL;
  env_str_addr = extract_address(buf, PTR_SIZE);

  /* Iterate over environment variables looking for PWD */
  env_str[sizeof(str_pwd) - 1] = 0;
  while (env_str_addr)
    {
      /* Read individual environment strings until we find PWD */
      status = target_read_memory (env_str_addr, env_str, 
                                   sizeof(str_pwd) - 1);

      /* If we read all the requested bytes for the current environment 
         variable string and it compares to "PWD=", read the rest of the
         string */
      if (status == 0 && strcmp (env_str, str_pwd) == 0)
        {
          int i = 0;
          cwd_path[0] = 0;
          env_str_addr += (sizeof(str_pwd) - 1);

          if (target_read_memory (env_str_addr, &cwd_path[i], 1))
            return NULL;

          while (cwd_path[i] && i < MAXPATHLEN)
            {
              i++;
              env_str_addr++;

              if (target_read_memory (env_str_addr, &cwd_path[i], 1))
                return NULL;
            }
          if (cwd_path[0])
            return cwd_path;
          else
            return NULL;
        }

      /* Move to the next entry in the env[] array */
      inf_env_addr += PTR_SIZE;
      if (target_read_memory (inf_env_addr, buf, PTR_SIZE))
          return NULL;
      env_str_addr = extract_address(buf, PTR_SIZE);
    }

  return NULL;
}

extern CORE_ADDR
get_load_info_dld_data_start ()
{
  if (!have_read_load_info)
    get_load_info();

  return load_info.li_svc_daddr;
}
