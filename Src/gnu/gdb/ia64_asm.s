	.radix C
	.text
        .proc ia64_read_kernel_word
        .prologue
        .body
ia64_read_kernel_word              ::
        // This procedure reads a word from anywhere in the 64 bit
        // address space.

         ld4            r8 = [r32]
         br.ret rp              //return
;;
        .endp ia64_read_kernel_word
