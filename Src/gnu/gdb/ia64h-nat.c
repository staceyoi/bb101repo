/* Native support code for HPUX IA64.
   Copyright 2000
   Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* Here are the externally visible functions that were defined in 
   hppah-nat.c:

    fetch_inferior_registers (regno)
    store_inferior_registers (regno)
    child_xfer_memory (memaddr, myaddr, len, write, target)
    child_post_follow_inferior_by_clone ()
    child_post_follow_vfork (parent_pid, followed_parent, child_pid, followed_child)
    hppa_pid_to_str (pid)
    hppa_tid_to_str (tid)
    _initialize_core_hppa ()
    parent_attach_all (pid, addr, data)
    hppa_require_attach (pid)
    hppa_require_detach (pid, signal)
    hppa_enable_page_protection_events (pid)
    hppa_disable_page_protection_events (pid)
    hppa_insert_hw_watchpoint (pid, start, len, type)
    hppa_remove_hw_watchpoint (pid, start, len, type)
    hppa_can_use_hw_watchpoint (type, cnt, ot)
    hppa_range_profitable_for_hw_watchpoint (pid, start, len)
    hppa_pid_or_tid_to_str (id)
    hppa_switched_threads (pid)
    hppa_ensure_vforking_parent_remains_stopped (pid)
    hppa_resume_execd_vforking_child_to_get_parent_vfork ()
    hppa_get_process_events (pid, wait_status, must_continue_pid_after)
    require_notification_of_events (pid)
    require_notification_of_exec_events (pid)
    child_acknowledge_created_inferior (pid)
    child_post_startup_inferior (pid)
    child_post_attach (pid)
    child_insert_fork_catchpoint (pid)
    child_remove_fork_catchpoint (pid)
    child_insert_vfork_catchpoint (pid)
    child_remove_vfork_catchpoint (pid)
    child_has_forked (pid, childpid)
    child_has_vforked (pid, childpid)
    child_can_follow_vfork_prior_to_exec ()
    child_insert_exec_catchpoint (pid)
    child_remove_exec_catchpoint (pid)
    child_has_execd (pid, execd_pathname)
    child_reported_exec_events_per_exec_call ()
    child_has_syscall_event (pid, kind, syscall_id)
    child_pid_to_exec_file (pid)
    pre_fork_inferior ()
    child_thread_alive (pid)
*/

#include <alloca.h>
#include <assert.h>
#include <elf_em.h>
#include <fcntl.h>
#include <libelf.h>
#include <sgtty.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/dir.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/scall_define.h>
#include <sys/ttrace.h>
#include <sys/user.h>
#include <machine/sys/reg_struct.h>
#include <sys/wait.h>
#include <sys/uc_access.h>
#include <unistd.h>

#include "defs.h"
#include "frame.h"
#include "inferior.h"
#include "ia64-regs.h"
#include "target.h"
#include "gdbcore.h"



/***************Begin MY defs*********************/

#ifndef CORE_REGISTER_ADDR
#define CORE_REGISTER_ADDR(regno, regptr) register_addr(regno, regptr)
#endif /* CORE_REGISTER_ADDR */

#define GET_RSE_STATE_INFO  if (!rse_state_info.have_read_rse_info) \
    get_rse_state_info ();


  /* From 11.5.2 of EAS 2.6, p 11-8, "RNAT{x} corresponds to the register
     saved at concatenate (BSPSTORE{63:9}, x(5:0), 0{2:0}).  So take 8:3 of
     gr_bs_addr, shift nat_collection right by that many bits and AND the
     result with 1.  That is the desired NaT bit/byte.
     */
#define NAT_BITPOS(bs_addr) (((bs_addr) >> 3) & 0x3f)

/* When bits 8:3 of BPSTORE are all one, RNAT is stored.  Given the BS
   address of a regisger, it's RNAT collection is at that address oring
   in 0x1f8.
   */

#define NAT_COLLECTION_ADDR(bs_addr) ((bs_addr) | 0x1f8)

/* We use 16 bytes for the floating point registers;  9 for GR's, ... */

#define REG_BUF_LEN	16
#define RSE_RDWR_SIZE   8

/* Bits 6:0 of CFM_REGNUM is sof - size of frame - the number of stacked
   registers seen by the procedure.
   */
#define SIZE_OF_FRAME	(rse_state_info.cfm & 0x7f)
#define SIZE_OF_LOCALS	( (rse_state_info.cfm & 0x3f80) >> 7 )
#define _UNW_CFM_SOF(value)           ((value>>0) & 0x7f)
#define _UNW_CFM_SOL(value)           ((value>>7) & 0x7f)
#define _UNW_CFM_SOR(value)           ((value>>14)& 0x0f)
#define _UNW_CFM_RRB_GR(value)         ((value>>18)& 0x7f)
#define _UNW_CFM_RRB_FR(value)         ((value>>25)& 0x7f)
#define _UNW_CFM_RRB_PR(value)         ((value>>32)& 0x3f)
#define NAT_COLLECTION_ADDR(bs_addr) ((bs_addr) | 0x1f8)

#define TPRINTF if (trace_flag) printf
#define TTRACE_ARG_TYPE uint64_t

/* --------- defines which depend on previous defines -------*/

#define TT_NIL ((TTRACE_ARG_TYPE) TT_NULLARG)

#define CHECK_WRITE_REG_BODY(regno, errno, warning_given) \
        { \
          /* Warning, not error, in case we are attached; sometimes the \
             kernel doesn't let us at the registers.  */ \
          char *err = safe_strerror (errno); \
          char *msg = alloca (strlen (err) + 128); \
          sprintf (msg, "writing register %d: %s", \
                   regno, err); \
	  if (!warning_given) \
            warning (msg); \
          warning_given = 1; \
        } 

#define CHECK_WRITE_REG(status, regno, errno, warning_given) \
      if (status == -1 && errno != 0) \
	CHECK_WRITE_REG_BODY (regno, errno, warning_given); 

#define CHECK_READ_REG_BODY(regno, errno, warning_given) \
	{ \
	  /* Warning, not error, in case we are attached; sometimes the \
	     kernel doesn't let us at the registers.  */ \
	  char *err = safe_strerror (errno); \
	  char *msg = alloca (strlen (err) + 128); \
	  sprintf (msg, "reading register %d: %s", regno, err); \
	  if (!warning_given) \
	    warning (msg); \
	  warning_given = 1; \
	}

#define CHECK_READ_REG(status, regno, errno, warning_given) \
      if (status == -1 && errno) \
	CHECK_READ_REG_BODY (regno, errno, warning_given);

#define CHECK_READ_REG_SUPPLY(status, regno, errno, warning_given, reg_buffer) \
  { \
    int supply_zero; \
 		     \
    supply_zero = (status == -1 && errno ); \
    CHECK_READ_REG (status, regno, errno, warning_given); \
    if (supply_zero) \
      { \
        supply_register(regno, 0); \
	bzero (reg_buffer, REGISTER_SIZE); \
      } \
    else \
      supply_register (regno, reg_buffer); \
  }

#ifdef __LP64__
#define UC_GET_RSEBS   __uc_get_rsebs
#define UC_ARG2_TYPE   uint64_t *
#else
#define UC_GET_RSEBS   __uc_get_rsebs64
#define UC_ARG2_TYPE   ptr64_t
#endif

/* JAGag06703 */
#define DEBUG(x)
extern boolean no_predication_handling;
/***************End   MY defs*********************/

/***************Begin MY types *******************/

typedef union 
    {
      int int_buf[BUNDLE_NBR_INTS];
      unsigned char char_buf[BUNDLE_SIZE];
    } inst_union_t;

typedef struct {
  int		have_read_rse_info;  /* Set to FALSE before each call to 
					ttrace_wait */
  CORE_ADDR	ar_bsp;
  CORE_ADDR	ar_bspstore;
  CORE_ADDR	cfm;
  CORE_ADDR	rnat;
  CORE_ADDR	bsp_stored_nat;
  CORE_ADDR 	bsp_stored_nat_addr; /* -1 if bsp_stored_nat is undefined */
} rse_state_info_t;

/***************End   MY types *******************/

/***************Begin MY data ********************/
static int 		fr0_value[3] = { 0x00000000, 0x00000000, 0x00000000 };
static int 		fr1_value[3] = { 0x0000ffff, 0x80000000, 0x00000000 };
static int 		gr0_value[2] = { 0x00000000, 0x00000000 };

/* Define masks to extract the instruction bits for each slot.
   This mask is applied to the bundle after the bytes have been re-arranged,
   e.g. after the bytes have been flipped, the leftmost 41 bits belong to
   slot 2.  If you and the mask with the bundle, you will get just the 
   41 bits of the instruction.
   */

/* int instruction_mask[INSTR_PER_BUNDLE][BUNDLE_NBR_INTS] = */
int 			instruction_mask[3][4] =
  { 0x00000000, 0x00000000, 0x00003fff, 0xffffffe0,  /* slot 0 */
    0x00000000, 0x007fffff, 0xffffc000, 0x00000000,  /* slot 1 */
    0xffffffff, 0xff800000, 0x00000000, 0x00000000,  /* slot 2 */
  };

rse_state_info_t 	rse_state_info;  /* All zeroes means have_read_rse_info
					    is FALSE. */
static int 		trace_flag   = 0;
static char *reg_names[] = REGISTER_NAMES;
/***************End   MY data ********************/
/***************Begin External Declarations ******/
extern int		call_ttrace PARAMS ((ttreq_t, int, TTRACE_ARG_TYPE,
			             TTRACE_ARG_TYPE,  TTRACE_ARG_TYPE));
extern struct target_ops child_ops;     /* target vector for inftarg.c */
extern int address_in_user_sendsig_unw ( CORE_ADDR );
CORE_ADDR 	swizzle PARAMS ((CORE_ADDR));
CORE_ADDR 	unswizzle PARAMS ((CORE_ADDR));
extern unsigned int stop_whining;
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
/***************End   External Declarations ******/

/***************Begin Forward Declarations *******/
static void 	fetch_register PARAMS ((int));
static void 	get_rse_state_info PARAMS ((void));
static int	get_stacked_nat PARAMS ((int, char *));
static int	get_stacked_register PARAMS ((int, char *));
extern struct target_ops ia64_target_ops;
int 		read_inferior_ideal_memory PARAMS ((CORE_ADDR, char *, int));
int 		read_inferior_memory PARAMS ((CORE_ADDR, char *, int));
CORE_ADDR reg_bs_addr PARAMS ((int,CORE_ADDR,CORE_ADDR));
int 		write_inferior_memory PARAMS ((CORE_ADDR, char *, int));
int             ia64_insert_breakpoint_1 PARAMS ((CORE_ADDR, char *, 
                                struct breakpoint* ));
boolean 	is_it_a_special_instruction PARAMS ((bundle_t, int));
int address_in_rsebs (CORE_ADDR addr, int len);

/***************End   Forward Declarations *******/


void
child_post_follow_inferior_by_clone ()
{
  /* This function is used when following both the parent and child
     of a fork.  In this case, the debugger clones itself.  The original
     debugger follows the parent, the clone follows the child.  The
     original detaches from the child, delivering a SIGSTOP to it to
     keep it from running away until the clone can attach itself.

     At this point, the clone has attached to the child.  Because of
     the SIGSTOP, we must now deliver a SIGCONT to the child, or it
     won't behave properly. */
  (void) kill (inferior_pid, SIGCONT);
}


void
child_post_follow_vfork (int parent_pid, int followed_parent, int child_pid,
     			 int followed_child)
{

  /* Are we a debugger that followed the parent of a vfork?  If so,
     then recall that the child's vfork event was delivered to us
     first.  And, that the parent was suspended by the OS until the
     child's exec or exit events were received.

     Upon receiving that child vfork, then, we were forced to remove
     all breakpoints in the child and continue it so that it could
     reach the exec or exit point.

     But also recall that the parent and child of a vfork share the
     same address space.  Thus, removing bp's in the child also
     removed them from the parent.

     Now that the child has safely exec'd or exited, we must restore
     the parent's breakpoints before we continue it.  Else, we may
     cause it run past expected stopping points. */
  if (followed_parent)
    {
      reattach_breakpoints (parent_pid);
      /* Bindu 121202, we want to be notified of shl_load events. */

      SOLIB_ADJUST_INFERIOR_HOOK (parent_pid);
    }

  /* Are we a debugger that followed the child of a vfork?  If so,
     then recall that we don't actually acquire control of the child
     until after it has exec'd or exited.
   */
  if (followed_child)
    {
      /* If the child has exited, then there's nothing for us to do.
         In the case of an exec event, we'll let that be handled by
         the normal mechanism that notices and handles exec events, in
         resume(). */

    }
}

/* Copy LEN bytes to or from inferior's memory starting at MEMADDR
   to debugger memory starting at MYADDR.   Copy to inferior if
   WRITE is nonzero.

   Returns the length copied, which is either the LEN argument or zero.
   This xfer function does not do partial moves, since child_ops
   doesn't allow memory operations to cross below us in the target stack
   anyway.  */

int
child_xfer_memory (CORE_ADDR memaddr, char *myaddr, int len,
                   int write,
                    struct target_ops *target) /* ignored */
{
  /* We might come in with an unswizzled memaddr. Swizzle it here.
     Don't swizzle if memaddr is a kernel address. */
  memaddr = address_in_user_sendsig_unw (memaddr)? memaddr: SWIZZLE (memaddr);
  if (write)
    {
      if (!write_inferior_memory (memaddr, myaddr, len))
	return len; /* success */
      return 0; /* failure */
    }
  else
    {
      GET_RSE_STATE_INFO;

      /* if memaddr is between bsp and bspstore call 
         read_inferior_ideal_memory*/
      if (address_in_rsebs(memaddr, len))
        {
          if(!read_inferior_ideal_memory (memaddr, myaddr, len))
            return len; /* success */
          return 0; /* failure */
        }
      if (!read_inferior_memory (memaddr, myaddr, len))
        return len; /* success */
      return 0; /* failure */
    }
} /* end child_xfer_memory */

/* Check if the address range falls in RSE. 
   Used in exec.c */
int
address_in_rsebs (CORE_ADDR addr, int len)
{
  GET_RSE_STATE_INFO;
  /*
   * Pictorially, this is how the memory can be viewed as 3 segments -
   *
   * 	Segment before RSE | BSPSTORE to BSP | Segment after RSE
   *
   * We need to find out if the memory being read falls fully/partially
   * in the RSE region. To do that, we need to make sure of the following -
   *
   * Case 1: Read after BSP
   * ----------------------
   * 
   * The memory region being read falls enritely after RSE ->
   *
   * 	BSP | ... addr to addr+len ... 
   *
   * Case 2: Read before BSPSTORE
   * ----------------------------
   *
   * The memory region being read falls enritely before RSE ->
   *
   * 	... addr to addr+len ... | BSPSTORE
   *
   *
   * In the above two cases, the address range is not in RSE. Otherwise, it
   * would partially or fully overlap with the RSE region.
   *
   */

  /* Check if this is neither case 1 or case 2 */
  if (!(   (addr >= rse_state_info.ar_bsp) // Case 1
        || ((addr + len) <= rse_state_info.ar_bspstore))) // Case 2
    {
      // Neither case 1 or 2, the read overlaps the RSE partially/fully
      return 1;
    }
  else
    {
      // The read is either case 1 or 2, with no RSE overlap
      return 0;
    }
}

/* Fetch all registers, or just one, from the child process.  */

void
fetch_inferior_registers (int regno)
{
  if (regno == -1 || regno == 0)
    for (regno = 0; regno < NUM_REGS; regno++)
      fetch_register (regno);
  else
    fetch_register (regno);
}

/* Fetch one register.  */

static void
fetch_register (int regno)
{
  char 		buf[MAX_REGISTER_RAW_SIZE];
  register int 	i;
  unsigned int 	regaddr;
  int		status;
  ttstate_t*    thread_state_p;
  CORE_ADDR	pc_value;
  CORE_ADDR 	regvalue;
  int		warning_given = 0;
  struct {
    uint64_t 	pad63 		: 21;
    uint64_t 	slot_nbr	:  2;
    uint64_t 	pad40		:  9;
    uint64_t 	pad31		: 32;
  } psr;

  /* Offset of registers within the u area.  */
  unsigned int offset;
  char reg_buffer[REG_BUF_LEN];

  regaddr = map_gdb_reg_to_ureg[regno];
  if (regno == NR0_REGNUM)
    { /* GR0 is not readable.  It's NAT byte is zero */
      supply_register(regno, 0);  
      return;
    }
  if (regno >= NR0_REGNUM + 1 && regno <= NR0_REGNUM + 31)
    { /* Fetching NaT bits is weird.  Fetch 9 bytes of the GR, last byte is the
	 NaT.
	 */
      regaddr = map_gdb_reg_to_ureg[GR0_REGNUM + regno - NR0_REGNUM];
      status = call_ttrace (TT_LWP_RUREGS,
			    inferior_pid,
			    (TTRACE_ARG_TYPE) regaddr,
			    9, /* GR plus NaT byte*/
			    (TTRACE_ARG_TYPE)
			      &reg_buffer[0]);
      if (status == -1 && errno)
	{
	  /* Warning, not error, in case we are attached; sometimes the
	     kernel doesn't let us at the registers.  */
	  char *err = safe_strerror (errno);
	  char *msg = alloca (strlen (err) + 128);

	  if (errno == EINVAL && regno >= NR0_REGNUM + 32 )
	    { /* After 32, the GR might not exist.  NAT is 0 */
	      registers[REGISTER_BYTE (regno)] = 0;
	      return;
	    }

	  sprintf (msg, "reading register %d: %s", regno, err);
	  warning (msg);

	  supply_register(regno, 0);
	  return;
	}

      supply_register(regno, reg_buffer + 8);  /* transfer NaT byte */
      return;
    }
  else if (regno >= NR0_REGNUM + 32 && regno <= NRLAST_REGNUM)
    {
      status = get_stacked_nat (regno, reg_buffer);
      if (status)
	{
	  CHECK_READ_REG_BODY (regno, status, warning_given);
	  bzero (reg_buffer, REG_BUF_LEN);
	}
      supply_register(regno, reg_buffer);  /* transfer NaT byte */
      return;
    }
  else if (regno == FR0_REGNUM)
    { /* FR0 always reads as zeroes */
      supply_register(regno, (char *) &fr0_value);
      return;
    }
  else if (regno == FR0_REGNUM + 1)
    { /* FR1 reads as +1.0: 0000 ffff 8000 0000 0000 */
      supply_register(regno, (char *) &fr1_value);
      return;
    }
  else if (regno >= FR0_REGNUM + 2 && regno <= FRLAST_REGNUM)
    { /* Gdb keeps FR's as 12-byte values, but ttrace uses a 16 byte buffer */
      /* Handle rotation if rotating register base is nonzero */
      /* For information on rotating registers refer to Section 4.5.1,
	 Volume 1 of the IA64 Architecture Software Developer's Manual. */
      char reg_buffer[REG_BUF_LEN];
      int rrb_fr;
      GET_RSE_STATE_INFO;
      rrb_fr = (int)  _UNW_CFM_RRB_FR (rse_state_info.cfm);
      int regno1=regno;
      if ((rrb_fr!=0) && (regno>= FR0_REGNUM + 32) &&
                    (regno < FRLAST_REGNUM))
        {
           regno1 = FR0_REGNUM + 32
                   + ((regno-FR0_REGNUM - 32)+ rrb_fr) % 96;
           regaddr = map_gdb_reg_to_ureg[regno1];
	}

      status = call_ttrace (TT_LWP_RUREGS,
			    inferior_pid,
			    (TTRACE_ARG_TYPE) regaddr,
			    16, /* FR's are written with 16 bytes */
			    (TTRACE_ARG_TYPE)
			      &reg_buffer[0]);
      if (status == -1 && errno)
	{
	  CHECK_READ_REG_BODY (regno, errno, warning_given);
	  bzero (reg_buffer, REG_BUF_LEN);
	}
      supply_register(regno, reg_buffer + 4);
      return;
    }
  else if (regno == GR0_REGNUM)
    {
      supply_register(regno, (char *) &gr0_value);
      return;
    }
  else if (regno > GR0_REGNUM + 31 && regno <= GRLAST_REGNUM)
    { /* Get a stacked register out of the RSE BS */
      status = get_stacked_register (regno, reg_buffer);
      if (status)
        {
	  CHECK_READ_REG_BODY(regno, status, warning_given);
	  bzero (reg_buffer, REGISTER_SIZE);
        }
      supply_register(regno, reg_buffer);
      return;
    }

  /* RM: Is it one of the unreadable ARs? */
  if (regno >= AR0_REGNUM && regno <= ARLAST_REGNUM)
    {
      if ((regno < AR0_REGNUM + 16) ||
          ((regno > AR0_REGNUM + 19) &&
           (regno != AR0_REGNUM + 32) &&
           (regno != AR0_REGNUM + 36) &&
           (regno != AR0_REGNUM + 40) &&
           (regno != AR0_REGNUM + 64) &&
           (regno != AR0_REGNUM + 65) &&
           (regno != AR0_REGNUM + 66)))
        {
          supply_register(regno, 0);
          return;
        }
    }
  /*Bindu: AR19 is reading the value at ar.bsp|0x1f8 using TT_LWP_RDRSEBS*/
  if (regno == AR0_REGNUM + 19)
    {
      /* Read the bsp */
      status = call_ttrace (TT_LWP_RUREGS,
                       inferior_pid,
                       (TTRACE_ARG_TYPE)(long) map_gdb_reg_to_ureg[AR0_REGNUM + 17],
                       8, /* All these writes are 8 bytes */
                       (TTRACE_ARG_TYPE) reg_buffer);
      if (status != -1 || !errno)
        /*Read the nat_collection*/
        status = call_ttrace (TT_LWP_RDRSEBS,
                          inferior_pid,
                          (TTRACE_ARG_TYPE)(long)
                           NAT_COLLECTION_ADDR(* (CORE_ADDR*)(void *) reg_buffer),
                          8, /* All these writes are 8 bytes */
                          (TTRACE_ARG_TYPE) reg_buffer);
      CHECK_READ_REG_SUPPLY(status, regno, errno, warning_given, reg_buffer);
      return;
                                     
    }

  /* Reading predicate registers is reading __pr and getting the 
     corresponding bit. */
  if ((regno >= PR0_REGNUM) && (regno <= PRLAST_REGNUM))
    {
      /* Handle rotation if rotating register base is nonzero */
      /* For information on rotating registers refer to Section 4.5.1,
	 Volume 1 of the IA64 Architecture Software Developer's Manual. */
      int rrb_pr;
      GET_RSE_STATE_INFO;
      rrb_pr = (int) _UNW_CFM_RRB_FR (rse_state_info.cfm);
      int regno1=regno;
      if ((rrb_pr!=0) && (regno>= PR0_REGNUM + 16) &&
                    (regno < PRLAST_REGNUM))
           regno1 = PR0_REGNUM + 16
                   + ((regno-PR0_REGNUM - 16)+ rrb_pr) % 48;
      regaddr = map_gdb_reg_to_ureg[PR0_REGNUM];
      status = call_ttrace (TT_LWP_RUREGS,
                            inferior_pid,
                            (TTRACE_ARG_TYPE) regaddr,
                            8, /* All these writes are 8 bytes */
                            (TTRACE_ARG_TYPE) reg_buffer);
      CHECK_READ_REG_SUPPLY(status, regno, errno, warning_given, reg_buffer);
      regvalue = 0; /* initialize for compiler warning */
      memcpy(&regvalue, reg_buffer, sizeof(regvalue));
      regvalue = (regvalue >> (regno1 - PR0_REGNUM)) & 1;
      regvalue = regvalue << 56; /* predicate registers are only 1 byte long */
      memcpy(reg_buffer, &regvalue, sizeof(regvalue));
      supply_register(regno, reg_buffer);
      return;
    }

#ifdef HP_MXN
  /* bindu 012901: Get CFM from PFS for unbound threads. The registers
   * for __uswtch are saved in __save_context which called by 
   * __uswtch. So __uswtch's CFM is in PFS at the time of saving.
   */

  if ( (regno == CFM_REGNUM) && (is_mxn && get_lwp_for (inferior_pid) == 0) )
    {
      CORE_ADDR reg_val;
      status = call_ttrace (TT_LWP_RUREGS,
                            inferior_pid,
                            (TTRACE_ARG_TYPE)(long) 
     				map_gdb_reg_to_ureg[AR0_REGNUM + 64],
                            8, /* 8 byte read */
                            (TTRACE_ARG_TYPE) reg_buffer);
      CHECK_READ_REG_SUPPLY(status, regno, errno, warning_given, reg_buffer);
      reg_val = 0; /* initialize for compiler warning */
      memcpy(&reg_val, reg_buffer, REGISTER_SIZE);
      reg_val = reg_val & 0x0000003fffffffffLL;
      memcpy(reg_buffer, &reg_val, REGISTER_SIZE);
      supply_register(regno, reg_buffer);
      return;
    }
#endif

  status = call_ttrace (TT_LWP_RUREGS,
		        inferior_pid,
		        (TTRACE_ARG_TYPE) regaddr,
		        8, /* All these writes are 8 bytes */
		        (TTRACE_ARG_TYPE) reg_buffer);

  CHECK_READ_REG_SUPPLY(status, regno, errno, warning_given, reg_buffer);
  
  return;
} /* end fetch_register */


/* Read in the RSE state informatin that we need to read stacked registers
   and the RSE backing store that hasn't been flushed.  Set
   rse_state_info.have_read_rse_info to TRUE.

   For a core file, the core registers have already been read into 
   the registers buffer (in most cases, see JAGaf77658 for an exception).
   We need to fetch certain registers from ucontext_t of that particular
   thread. For an executable, we need to fetch the new register values.  
   */
extern bfd *current_core_bfd;
extern ucontext_t *get_uc_for (bfd *, int);

static void 
get_rse_state_info (void)
{
  int status;
  CORE_ADDR nat_addr;

  ucontext_t *uc = 0;

  /* JAGaf77658: gdb unable to backtrace Caliper core 
     If we're processing a .gdbinit file and are trying to set
     breakpoints dispite the fact we're going to process a corefile,
     then the registers array hasn't been initialized and this routine
     is useless - do nothing successfully.  Carl Burch, 10/18/05.
     */
  if (!target_has_execution && (current_core_bfd == NULL))
     return;

  if (!target_has_execution && !(inferior_pid & TID_MARKER)) {
    uc = get_uc_for (current_core_bfd, inferior_pid);
    /* Suppress spurious error messages when we've looked at a core
       file but did a run command for a process that just exited.
       Follow-on fix for residual problem with JAGaf87220, "gdb 
       crash on run from core file session".
       */
    if ((uc == NULL) && (current_core_bfd != NULL))
       return;
    }

  if ( (target_has_execution) 
#ifdef HP_MXN
      || (is_mxn && get_lwp_for (inferior_pid) == 0)
#endif
     ) {
    fetch_register (AR0_REGNUM + 17); /* AR17 is BSP */
    fetch_register (AR0_REGNUM + 18); /* AR18 is BSPSTORE */
    fetch_register (CFM_REGNUM); 
    }
  memcpy (&rse_state_info.ar_bsp, 
	  &registers[REGISTER_BYTE (AR0_REGNUM + 17)],
          REGISTER_SIZE);
  memcpy (&rse_state_info.ar_bspstore, 
	  &registers[REGISTER_BYTE (AR0_REGNUM + 18)],
          REGISTER_SIZE);
  memcpy (&rse_state_info.cfm, 
	  &registers[REGISTER_BYTE (CFM_REGNUM)],
          REGISTER_SIZE);

#ifdef HP_MXN
  /* bindu 012901: For unbound thread, at the time of saving, cover is 
   * not executed and so BSP does not include outs of __uswtch. Patch 
   * BSP with outs now.
   */
  if (is_mxn && (get_lwp_for (inferior_pid) == 0))
    {
      rse_state_info.ar_bsp = rse_state_info.ar_bsp
             + REGISTER_SIZE * ( SIZE_OF_FRAME - SIZE_OF_LOCALS );
      /* Account for nats. */
      nat_addr = NAT_COLLECTION_ADDR (rse_state_info.ar_bsp
		   - REGISTER_SIZE * ( SIZE_OF_FRAME - SIZE_OF_LOCALS ));
      while (nat_addr <= rse_state_info.ar_bsp)
        {
	  nat_addr +=64 * REGISTER_SIZE;
	  rse_state_info.ar_bsp += REGISTER_SIZE;
        }
      rse_state_info.ar_bspstore = rse_state_info.ar_bsp;
    }
#endif

  rse_state_info.bsp_stored_nat_addr = (CORE_ADDR)(long) -1;

  /* Getting the final NAT collection is a special case of
     TT_LWP_RDRSEBS, addr == ar.bsp | 0x1f8
     */

  if ( (target_has_execution)
#ifdef HP_MXN
      || (is_mxn && get_lwp_for (inferior_pid) == 0)
#endif
     )
    {
      status = call_ttrace (TT_LWP_RDRSEBS,
			     inferior_pid,
			     (TTRACE_ARG_TYPE) 
				NAT_COLLECTION_ADDR(rse_state_info.ar_bsp),
			     (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
			     (TTRACE_ARG_TYPE) &rse_state_info.rnat);
      if (errno && !status)
        perror_with_name ("Error in getting ar.bsp");
    }
  else
    {
      /* __uc_get_rsebs() swizzles the address that we pass it. As we already 
         swizzle the address before, we can call UC_GET_RSEBS which does not 
         swizzle the addr.
       */
      status = UC_GET_RSEBS(
		  uc, 
		  (UC_ARG2_TYPE) NAT_COLLECTION_ADDR(rse_state_info.ar_bsp), 
		  1, 
		  (uint64_t *) &rse_state_info.rnat);
      if (current_core_bfd && status != 0)
        error ("Error in getting ar.bsp");
    }

  rse_state_info.have_read_rse_info = TRUE;
} /* end get_rse_state_info */


/* Given a nat_addr and shift_size, this function reads the nats at this addr
   and copy's the corresponing bit (depending on shift_size) into reg_buf.
   Returns 0 on success. */
int
read_nat_addr  (CORE_ADDR nat_collection_addr, char* reg_buf, int shift_size)
{
  uint64_t	nat_collection;
  int		status;

  GET_RSE_STATE_INFO;

  if (nat_collection_addr > rse_state_info.ar_bsp)
    nat_collection = rse_state_info.rnat;
  else
    {
      if (nat_collection_addr == rse_state_info.bsp_stored_nat_addr)
         nat_collection = rse_state_info.bsp_stored_nat;
      else
        {
          status = read_inferior_ideal_memory (nat_collection_addr,
                                               (char*) &nat_collection,
                                               REGISTER_SIZE);
          if (status)
            return status;
          rse_state_info.bsp_stored_nat_addr = nat_collection_addr;
          rse_state_info.bsp_stored_nat = nat_collection;
        }
    }

  *reg_buf = (nat_collection >> shift_size) & 1;
  return 0;
}

/* get_stacked_nat - Set the NaT byte in reg_buf_p for regno which is a
   NaT register NR32 .. NR127.
   If the register is not present, set the NaT byte to 1.

   Return 0 on success, otherwise errno.
   */

static int
get_stacked_nat (int regno, char * reg_buf_p)
{
  int		i;
  CORE_ADDR	gr_bs_addr;
  int		gr_regno;
  uint64_t	nat_collection;
  CORE_ADDR	nat_collection_addr;
  int		shift_size;
  int		status;

  GET_RSE_STATE_INFO;

  gr_regno = regno - NR0_REGNUM + GR0_REGNUM;
  if (!(gr_bs_addr = reg_bs_addr (gr_regno, 0, 0)))
    { /* The register isn't there.  Set its NaT byte to 1. */
      registers[REGISTER_BYTE (regno)] = 1;
      return 0;
    }
  nat_collection_addr = NAT_COLLECTION_ADDR (gr_bs_addr);
  shift_size = (int) NAT_BITPOS(gr_bs_addr);
  status = read_nat_addr (nat_collection_addr, reg_buf_p, shift_size);
  if (status)
    return status;
  return 0;
} /* end get_stacked_nat */

/* get_stacked_register - copy the contents of gdb register regno to
   reg_buf_p (8 bytes).  Regno should be in the rang 32 .. 127.  If the
   register is not present, the buffer is zeroed out.

   Return 0 on success, otherwise errno.
   */

static int
get_stacked_register (int regno, char *	reg_buf_p)
{
  int		i;
  CORE_ADDR	reg_addr;
  int		status;

  GET_RSE_STATE_INFO;

  if (!(reg_addr = reg_bs_addr (regno, 0, 0)))
    { /* The register isn't there.  Zero out the buffer. */
      memset (reg_buf_p, 0, REGISTER_SIZE);
      return 0;
    }
  status = read_inferior_ideal_memory (reg_addr, reg_buf_p, REGISTER_SIZE);
  return status;
} /* end get_stacked_register */


/* Insert a breakpoint on targets that don't have any better breakpoint
   support.  We read the contents of the target location and stash it,
   then overwrite it with a breakpoint instruction.  ADDR is the target
   location in the target machine.  CONTENTS_CACHE is a pointer to 
   memory allocated for saving the target contents.  It is guaranteed
   by the caller to be long enough to save sizeof BREAKPOINT bytes (this
   is accomplished via BREAKPOINT_MAX).  

   If a request is made to place a breakpoint on the immediate part of
   a 64-bit immediate instruction, then change the slot number to 2 instead
   of 1.  The system, however, reports the breakpoint as happening for
   slot 1, so we don't have to change the owning breakpoint.

   Returns 0 on success and 1 on failure.

   */

/* Suresh,Nov06, JAGag06703: For better support for handling GDB stops at 
   predicated false instructions, the different kind of stops like h/w 
   breakpoint, s/w breakpoint, internal breakpoints etc, are all considered 
   separately.

   Here in this function we handle s/w breakpoints and internal breakpoints..
 
   For s/w bkpt, gdb should NOT stop in the predicated instructions if 
   qualifying predicate (QP) is false. One way to implement this is to 
   extract the predicate register from the original instruction and attach 
   it to 'break.i' instruction that we place in lieu. The idea is to convert 
   the break.i that we have been using so far to a predicated break.i 
   instruction, so that GDB itself will get control, only when QP is true.

   But internal breakpoints(like fini and others) could be placed on instruction 
   that are predicated false and hence NOT stoping in them, would mean say for 
   fini, that it would NOT stop in the end of the current function return and 
   end up executing all the instructions. Hence for all internal breakpoints, we 
   will maintain the existing status quo and NOT emit predicated breaks for them.

   Also there are quite a few instuctions that do modify the processor arch 
   state, even when QP is false and hence for them we don't emit predicated break.
  
   1. To emit predicate breaks, lets find out the predicate register specified 
   in the original inst, which is specified in the last 6 bits of the instruction.
   Lets extract the last 6 bits(0x3f) and OR it to the break instruction that we 
   place in lieu of the original instruction..

   The 41 bits of the inst correspond to "Opcode:registers:...:predicate register"

   2. For Internal breakpoints: We don't want to append the register predicate 
   with the break inst for any thing other than a regular breakpoint. We don't 
   have the struct breakpoint passed to this function as an argument to decide 
   if its a regular bkpt. To handle this..
      
   a. A clean approach would be change the signature of this function to take 
   the breakpoint structure as an argument OR overload this function with 
   different args - but there are lots of places where this function is called
   ( close to 30) and hence we need to change at all those places...

   We take this approach. The function ia64_insert_breakpoint() is mutated
   to ia64_insert_breakpoint_1() that takes 3 args, the extra argument is 
   a pointer to struct breakpoint. Where ever we call this function (6 places
   in breakpoint.c, except create_breakpoints()) with a breakpoint member 
   shadow_contents, all those places should call this - the mutated variant and 
   all the other places would continue to call the original function(with 2 args)
      
   i am checking for valid values like b.type, b.enable, b.disposition 
   and if all this tallies, then the structure is a breakpoint structure. 
        - for a normal breakpoint b.enable = enabled typically...
        - for a normal breakpoint b.disposition can be donttouch(typically), 
          del (for temp breakpoints)..
   Also probable_breakpoint is NULL if this function is called thru the 
   original function(ia64_insert_breakpoint())

   3. To take care of instructions that modify state, when QP is false..
   I am planning to check for those opcodes and if it matches one of the
   few instructions like uncond compare, floating point reciprocal approx etc,
   we will not emit predicated breaks, so they continue to be hit the old way
   even if QP is false..

   4. TODO: to handle stops when an async signal is hit..  experimented with a
   couple of alternatives, but not working as expected. Need more time to debug
   and fix this..  see JAG labnotes for more details..

 */

int
ia64_insert_breakpoint (CORE_ADDR addr, char *contents_cache)
{
	return ia64_insert_breakpoint_1( addr, contents_cache, NULL);
}

int
ia64_insert_breakpoint_1 (CORE_ADDR addr, char *contents_cache, 
                                struct breakpoint* probable_breakpoint)
{
  int slot_no;
  CORE_ADDR bundle_addr;

  bundle_t	bundle;
			/* break.i          0x80000 */
  const long long break_instruction = 0x2000000;
  inst_union_t	ubufs;
  char 		flip_buf[BUNDLE_SIZE];
  int 		i;
  boolean need_predicated_break = FALSE;
  DEBUG(FILE *fd;)

  /* Bindu 032102: For certain breakpoints like catch_vfork, the addr is 0. 
     No need to insert at this address. */
  if (!addr)
    return 0; 

  if (in_mixed_mode_pa_lib (addr, NULL))
    {
      warning ("Cannot insert breakpoints in PA libraries. Breakpoint "
               "addr\n         requested: 0x%llx\n", addr);
      return 1;
    }
  
  bundle_addr = (addr & (~0xF));
  slot_no = (int) (addr % 16);

  if ((slot_no >= 0) && (slot_no <= 2))
    {
      /* Read the entire bundle into contents_cache, but we will only use
	 the 41 bits for this slot instruction.  We reorder the bytes
	 because we are big-endian.
	 */

      if (   read_inferior_memory (bundle_addr, flip_buf, BUNDLE_SIZE) 
	  != 0)
	{
          warning ("Unable to read address from target memory (%016llx:%d) \n", \
               bundle_addr, slot_no);
	  return 1;
	}

      for (i = 0;  i < BUNDLE_SIZE;  i++)
	contents_cache[i] = flip_buf[BUNDLE_SIZE - 1 - i];

      /* We use a breakpoint immediate of  0x80000 which is the first in
	 the range reserved for debugers.  The instruction bit-pattern
	 is 0x2000000.
		break.i          0x80000
	 Replce the instruction at slot_nbr with 0x2000000, the 
	 value of break_instruction.
	 */
      
      assert (sizeof(ubufs) == sizeof(bundle));
      memcpy (&bundle, contents_cache, sizeof(ubufs));
      if (bundle.slots.tmplt == 4  || bundle.slots.tmplt == 5)
	{ 
          /* JAGag32172: Because we have the same instn in two slots, 
             break on addr with slot 2 will actually make us stop at slot 1
             which is not handled as our breakpoint_chain is not 
             updated. */ 
          if (slot_no == 2 && probable_breakpoint)
            {
              struct breakpoint *b;
              extern struct breakpoint *breakpoint_chain;
              for (b = breakpoint_chain; b; b = b->next)
                if (b->address == probable_breakpoint->address - 1)
                  {
                    probable_breakpoint->address -= 1;
                    probable_breakpoint->duplicate = 1;
                    return 0;
                  }
              probable_breakpoint->address -= 1;
            }
          else if (slot_no == 1)
            /* We have a "2" template, MLI.  If the slot is 1, change it to
	       2.  This is an instruction like movl which occupies two
	       instruction slots.
	    */
	    slot_no = 2;
	}

      /*
        JAGag06703: Check for valid values like b.type, b.enable, 
        b.disposition 
        - for a normal breakpoint b.enable = enabled typically...
        - for a normal breakpoint b.disposition can be donttouch(typically), 
          del (for temp breakpoints)..
        Also probable_breakpoint is NULL if this function is called thru the 
        original function.
      */
      DEBUG(fd = fopen("./JAGag06703.gdbout","a");)
      DEBUG(if( probable_breakpoint );)
      DEBUG(fprintf(fd, " The addr = %x, contents_cache = %x, type = %d, enable 
           = %d, disposition = %d, No = %d  \n", addr, contents_cache, 
           probable_breakpoint->type, probable_breakpoint->enable, 
           probable_breakpoint->disposition, probable_breakpoint->number);)

      /* If user doesn't want this special predication handling, then lets not do it */
      if( probable_breakpoint && !no_predication_handling)
      if((probable_breakpoint->type == bp_breakpoint) && 
         (probable_breakpoint->enable == enabled ) && 
         (probable_breakpoint->disposition == del || del_at_next_stop || disable 
          || donttouch ))
	{ 
          /* JAGag06703: Ok, looks like a valid bkpt structure and a regular 
             breakpoint. Now lets check if its one of those special instructions 
             that modify processror state, then we will not emit a predicated break, 
             irrespective of the QP. 
           */
          DEBUG( fprintf(fd,"We have a normal bkpt; Going for predicated bkpt \n");)
          need_predicated_break = ! is_it_a_special_instruction(bundle, slot_no);
        };

     /* JAGag06703: for emitting predicated break, lets find out the 
        predicate register specified in the original inst, which is specified 
        in the last 6 bits of the instruction.  Lets extract the last 
        6 bits(0x3f) and OR it to the break instruction that we place in lieu 
        of the original instruction..

        The 41 bits of the inst correspond to 
         "Opcode:registers:...:predicate register"
      */

      switch (slot_no)
	{
          case 0:
            if (need_predicated_break)
                bundle.slots.slot0 = (bundle.slots.slot0 & 0x3f) | break_instruction;
            else
                bundle.slots.slot0 = break_instruction;
            break;

          case 1:
            bundle.slots.slot1hi = break_instruction >> 18;
            if (need_predicated_break)
                bundle.slots.slot1lo = (bundle.slots.slot1lo & 0x3f) | break_instruction;
            else
                bundle.slots.slot1lo = break_instruction;
            break;

	  case 2:
            if (need_predicated_break)
	        bundle.slots.slot2 = (bundle.slots.slot2 & 0x3f) | break_instruction;
	    else
                bundle.slots.slot2 = break_instruction;
	    break;
	  }
      memcpy (&ubufs, &bundle, sizeof(ubufs));

      for (i = 0;  i < BUNDLE_SIZE;  i++)
	flip_buf[i] = ubufs.char_buf[BUNDLE_SIZE - 1 - i];

      if ( write_inferior_memory (bundle_addr, (char *) flip_buf, BUNDLE_SIZE) != 0)
	{
	  return 1;
	}
    }
  else
    {
      warning ("Invalid breakpoint address (%016llx:%d) \n", 
	       bundle_addr, slot_no);
      return 1;
    }

  return 0;
} /* end ia64_insert_breakpoint */

/* ia64_remove_breakpoint - remove the breakpoint placed at addr.
     contents_cache is the entire instruction bundle flipped around
     so that we can decipher it.
 */

int
ia64_remove_breakpoint (CORE_ADDR addr, char *contents_cache)
{
  CORE_ADDR bundle_addr;
  int cache_buf[BUNDLE_NBR_INTS];
  bundle_t cached_bundle;
  bundle_t current_bundle;
  char flip_buf[BUNDLE_SIZE];
  int i;
  int slot_no;
  union 
    {
      int int_buf[BUNDLE_NBR_INTS];
      char char_buf[BUNDLE_SIZE];
    } ubufs;

  bundle_addr = (addr & (~0xF));
  slot_no = (int) (addr % 16);

  if ((slot_no >= 0) && (slot_no <= 2))
    {
      /* Read the entire bundle, and replace the 41
	 bits of the instruction with the corresponding 41 bits from
	 contents_cache. 
	 */

      if (   read_inferior_memory (bundle_addr, (char*) flip_buf, BUNDLE_SIZE) 
	  != 0)
	{
	  error ("Unable to read bundle at %016llx:%d \n", 
		 bundle_addr, slot_no);
	}

      for (i = 0;  i < BUNDLE_SIZE;  i++)
	ubufs.char_buf[i] = flip_buf[BUNDLE_SIZE - 1 - i];

      memcpy (&current_bundle, &ubufs, BUNDLE_SIZE);
      memcpy (&cached_bundle, contents_cache, BUNDLE_SIZE);
      if (   (cached_bundle.slots.tmplt == 4 || cached_bundle.slots.tmplt == 5) 
	  && slot_no == 1)
	{ /* We have a "2" template, MLI.  If the slot is 1, change it to
	     2.  This is an instruction like movl which occupies two
	     instruction slots.
	     */
	  slot_no = 2;
	}
      switch (slot_no)
	{
	case 0:
	  current_bundle.slots.slot0 = cached_bundle.slots.slot0;
	  break;
	  ;;

	case 1:
	  current_bundle.slots.slot1hi = cached_bundle.slots.slot1hi;
	  current_bundle.slots.slot1lo = cached_bundle.slots.slot1lo;
	  break;
	  ;;

	case 2:
	  current_bundle.slots.slot2 = cached_bundle.slots.slot2;
	  break;
	  ;;
	}

      memcpy (&ubufs, &current_bundle, BUNDLE_SIZE);

      for (i = 0;  i < BUNDLE_SIZE;  i++)
	flip_buf[i] = ubufs.char_buf[BUNDLE_SIZE - 1 - i];

      if (   write_inferior_memory (bundle_addr, (char *) flip_buf, BUNDLE_SIZE)
	  != 0)
	{
	  warning ("Unable to remove breakpoint at %016llx:%d \n", 
		   bundle_addr, slot_no);
	}
    }
  else
    {
      warning ("Invalid remove breakpoint address (%016llx:%d) \n", 
	       bundle_addr, slot_no);
    }
  return 0;
} /* end ia64_remove_breakpoint */


/* JAGag06703: The following instructions modify processor state, even when 
   QP is false - unconditional compare, floating point reciprocal approximations 
   (frsqrta, frcpa) and while loop branches ( br.wtop, br.wexit). We should NOT 
   do a predicated break for these isntruction, as it would result in not 
   executing these instructions at all, and it would result in application 
   correctness issues, when running under the debugger.

   The opcodes of these instructions:( From Intel SDM ver 2.2 vol 3)

   1. cmp.unc - Bits 40-37 - C/D/E ; Bit 36 - 0; 35:34 -00; 33 - 0; 12 -1
              - A unit instruction; 
              - Opcode clashes with long call/branch,hence to uniquely identify 
                this, it should not be slot 1/2 and bundle temp be 04/05
 */

#define CMP_UNC_MASK         0x1FE00001000
#define CMP_UNC_OPCODE1      0x18000001000
#define CMP_UNC_OPCODE2      0x1A000001000
#define CMP_UNC_OPCODE3      0x1C000001000

/*  cmp.imm.unc  - Bits 40-37 - C/D/E ; 35:34 -10; 33 - 0; 12 -1 */

#define CMP_IMM_UNC_MASK     0x1EE00001000
#define CMP_IMM_UNC_OPCODE1  0x18800001000
#define CMP_IMM_UNC_OPCODE2  0x1A800001000
#define CMP_IMM_UNC_OPCODE3  0x1C800001000

/* 2. cmp4.unc - Bits 40-37 - C/D/E ; 36 - 0 ; 35:34 -01; 33 - 0; 12 -1
               - A unit instruction; 
               - Opcode clashes with long call/branch,hence to uniquely identify 
                 this, it should not be slot 1/2 and bundle temp be 04/05
 */
#define CMP4_UNC_MASK        0x1FE00001000
#define CMP4_UNC_OPCODE1     0x18400001000
#define CMP4_UNC_OPCODE2     0x1A400001000
#define CMP4_UNC_OPCODE3     0x1C400001000
 
/*  cmp4.imm.unc -Bits 40-37 - C/D/E ;35:34 -11; 33 - 0; 12 -1 */
#define CMP4_IMM_UNC_MASK    0x1EE00001000
#define CMP4_IMM_UNC_OPCODE1 0x18C00001000
#define CMP4_IMM_UNC_OPCODE2 0x1AC00001000
#define CMP4_IMM_UNC_OPCODE3 0x1CC00001000

/* 3.tbit.unc - Bits 40-37 - 5; Bit 36 - 0; 35/34 -00; 33 -0; 13 - 0; 12 -1
              - I unit inst; cannot come in slot 0 and 
              - bundle template should be from 00 to 11
 */
#define TBIT_UNC_MASK        0x1FE00003000
#define TBIT_UNC_OPCODE      0x0A000001000

/* tnat.unc- Bits 40:37 - 5; Bit 36 - 0; 35:34 -00; 33 -0; 19 - 0;
                      13 -1; 12 - 1*/
#define TNAT_UNC_MASK        0x1FE00083000
#define TNAT_UNC_OPCODE      0x0A000003000

/* tf.unc - Bits 40-37 - 5; Bit 36 - 0; 35/34 -00; 33 -0; 26:20 - 0; 19 - 1; 
                      13 - 1 ; 12 -1; */
#define TF_UNC_MASK          0x1FE07F83000
#define TF_UNC_OPCODE        0x0A000083000

/* Common for 4,5,6,7
	      - f unit inst, only in slots 1 or 2
	      - if in slot 1 - bundle template is 0C/0D/1C/1D
	      - if in slot 2 - bundle template is 0E/0F
    
  4. fcmp.unc - Bits 40-37 - 4; bit 12 - 1;   
 */
#define FCMP_UNC_MASK        0x1E000001000
#define FCMP_UNC_OPCODE      0x08000001000

/* 5. fclass.unc - Bits 40-37 - 5; bit 12 -1; */
#define FCLASS_UNC_MASK      0x1E000001000
#define FCLASS_UNC_OPCODE    0x0A000001000

/* 6. frsqrta/fprsqrta - Bits 40:37 - 0/1 ; 36 - 1; 33 - 1;  */
#define FRCPA_FRSQRTA_MASK   0x1F200000000
#define FRSQRTA_OPCODE       0x01200000000
#define FPRSQRTA_OPCODE      0x03200000000

/* 7. frcpa/fprcpa     - Bits 40:37 - 0/1 ; 36 - 0; 33 - 1;  */
#define FRCPA_FRSQRTA_MASK   0x1F200000000
#define FRCPA_OPCODE         0x00200000000
#define FPRCPA_OPCODE        0x02200000000

/* 8. br.wtop and br.exit
              - bits 40:37 - 4; 8:6 - 011/010; 
              - b-unit inst;
              - 10 <= bundle temp <= 1D
 */
#define BR_WTOP_MASK         0x1E0000001C0
#define BR_WTOP_OPCODE       0x080000000C0
#define BR_WEXIT_MASK        0x1E0000001C0 
#define BR_WEXIT_OPCODE      0x08000000080

/* JAGag29032 : We need to look for br.cond as well and treat it as special because of cod
   e hoisting and resulting in logical SPOS for the source line at the predicated br.cond.    Breakpoints placed in this logical SPOS may not be hit as expected and confuse the 
   user. Hence the decision to handle this as special instruction and let breakpoints 
   stop here unconditionally.  See JAG mailnotes for more details.
*/
/* 9. br.cond 
           Common:   - b-unit inst;
                     - 10 <= bundle temp <= 1D
           IP-relative branch:
              - bits 40:37 - 4; 8:6 - 000; 
           Indirect branch:
              - bits 40:37 - 0; 
              - bits 32:27 - 20 (6 bits)
              - 8:6 - 000; 
 */

#define BR_COND_MASK1        0x1E0000001C0 // IP-relative
#define BR_COND_OPCODE1      0x08000000000
#define BR_COND_MASK2        0x1E1F80001C0 // Indirect
#define BR_COND_OPCODE2      0x00100000000

/* This function checks all the above conditions and returns 1 if its one of
   these special instructions else it returns zero 

   All the conditions below, check for the template match first before checking
   for opcode match. This is an optimization to bail out early in the compares
   for all normal instructions, which is the common case(majority)
 */ 
boolean
is_it_a_special_instruction (bundle_t bundle, int slot_no)
{

   /* Copy into slot0 for easier code manipulation below */
   if (slot_no == 1)
     bundle.slots.slot0 = bundle.slots.slot1hi << 18 | bundle.slots.slot1lo;
   if (slot_no == 2)
     bundle.slots.slot0 = bundle.slots.slot2;

   /* Is it a br.wtop or br.wexit or br.cond instruction ? */
   if ((bundle.slots.tmplt >= 0x10 && bundle.slots.tmplt <= 0x1D) && 
       (((bundle.slots.slot0 & BR_WTOP_MASK ) == BR_WTOP_OPCODE) || 
        ((bundle.slots.slot0 & BR_COND_MASK1) == BR_COND_OPCODE1)|| 
        ((bundle.slots.slot0 & BR_COND_MASK2) == BR_COND_OPCODE2)|| 
        ((bundle.slots.slot0 & BR_WEXIT_MASK) == BR_WEXIT_OPCODE )))
     return(TRUE);
   /* Is it a floating point approx instruction ?*/
   if (((bundle.slots.tmplt >= 0x0C && bundle.slots.tmplt <= 0x0F) || 
        bundle.slots.tmplt == 0x1C || bundle.slots.tmplt == 0x1D) 
                                  && 
       (((bundle.slots.slot0 & FRCPA_FRSQRTA_MASK) == FRCPA_OPCODE) || 
        ((bundle.slots.slot0 & FRCPA_FRSQRTA_MASK) == FPRCPA_OPCODE)|| 
        ((bundle.slots.slot0 & FRCPA_FRSQRTA_MASK) == FRSQRTA_OPCODE) || 
        ((bundle.slots.slot0 & FRCPA_FRSQRTA_MASK) == FPRSQRTA_OPCODE) || 
        ((bundle.slots.slot0 & FCLASS_UNC_MASK) == FCLASS_UNC_OPCODE) || 
        ((bundle.slots.slot0 & FCMP_UNC_MASK) == FCMP_UNC_OPCODE)))
      return(TRUE);
   /* Is it a test bit/nat/feature.unc instruction ?*/
   if ((slot_no != 0) 
                && 
       (bundle.slots.tmplt <= 0x11) 
                && 
       (((bundle.slots.slot0 & TBIT_UNC_MASK) == TBIT_UNC_OPCODE) || 
        ((bundle.slots.slot0 & TNAT_UNC_MASK) == TNAT_UNC_OPCODE) || 
        ((bundle.slots.slot0 & TF_UNC_MASK ) == TF_UNC_OPCODE)))
     return(TRUE);
   /* Is it a comapare.unc instruction ?*/
   if ((((bundle.slots.tmplt != 0x04 && bundle.slots.tmplt != 0x05)) || 
        ((slot_no ==0)&&(bundle.slots.tmplt == 0x04||bundle.slots.tmplt == 0x05))) 
                     && 
       (((bundle.slots.slot0 & CMP4_UNC_MASK) == CMP4_UNC_OPCODE1) || 
        ((bundle.slots.slot0 & CMP4_UNC_MASK) == CMP4_UNC_OPCODE2) || 
        ((bundle.slots.slot0 & CMP4_UNC_MASK) == CMP4_UNC_OPCODE3) || 
        ((bundle.slots.slot0 & CMP4_IMM_UNC_MASK) == CMP4_IMM_UNC_OPCODE1) || 
        ((bundle.slots.slot0 & CMP4_IMM_UNC_MASK) == CMP4_IMM_UNC_OPCODE2) || 
        ((bundle.slots.slot0 & CMP4_IMM_UNC_MASK) == CMP4_IMM_UNC_OPCODE3) || 
        ((bundle.slots.slot0 & CMP_UNC_MASK) == CMP_UNC_OPCODE1) || 
        ((bundle.slots.slot0 & CMP_UNC_MASK) == CMP_UNC_OPCODE2) || 
        ((bundle.slots.slot0 & CMP_UNC_MASK) == CMP_UNC_OPCODE3) || 
        ((bundle.slots.slot0 & CMP_IMM_UNC_MASK) == CMP_IMM_UNC_OPCODE1) || 
        ((bundle.slots.slot0 & CMP_IMM_UNC_MASK) == CMP_IMM_UNC_OPCODE2) || 
        ((bundle.slots.slot0 & CMP_IMM_UNC_MASK) == CMP_IMM_UNC_OPCODE3))) 
      return(TRUE);
   return(FALSE);
}


/* Suresh,JAGag06703: This functions reads the instruction from the inferior 
   and then extracts the predicate register used in the instruction. The 
   predicate register is encoded in the last 6 bits of each instruction.

   Return value:  On error, we still return 0, as predicate zero(p0) is always 
   true and specifying that as a QP in a instruction is harmless. On success, 
   return the predicate register read ( 0 < 63 )

   Caveat: If the instruction is one of the special instructions that modify 
   processor arch state, then return 0(aka P0, which is always 1) so that 
   the caller of this routine will handle it appropriately..In this case it
   really doesn't extract the actual pred register used, and the name of the
   routine is a misnomer!. i couldn't think of a better name :)
 */

int
extract_predicate_register (CORE_ADDR address)
{
  unsigned int slot_no;
  CORE_ADDR bundle_addr;
  bundle_t bundle;
  char flip_buf[BUNDLE_SIZE];
  char contents_cache[BUNDLE_SIZE];
  int i;
  int predicate_used = 0;

  bundle_addr = address & (~0xF);
  slot_no = (unsigned int) (address % 16);

  if (slot_no <= 2)
  {
    /* Read the entire bundle into contents_cache, but we will only use
       the 41 bits for this slot instruction.  We reorder the bytes
       because data is big-endian and these are instructions, which are
       little-endian on IPF.
     */
    if ( read_inferior_memory (bundle_addr, flip_buf, BUNDLE_SIZE) != 0)
    {
     warning ("Unable to read address from target memory (%016llx:%d) \n", \
               bundle_addr, slot_no);
     return(predicate_used); /* predicate_used is 0 here.. */
    }
    for (i = 0;  i < BUNDLE_SIZE;  i++)
      contents_cache[i] = flip_buf[BUNDLE_SIZE - 1 - i];

    memcpy (&bundle, contents_cache, sizeof(bundle));
    if ((bundle.slots.tmplt == 4  || bundle.slots.tmplt == 5) && slot_no == 1)
      {  
        /* We have a "2" template, MLI.  If the slot is 1, change it to 2.  
           This is an instruction like movl which occupies two instruction slots.
         */
        slot_no = 2;
      }
    if ( is_it_a_special_instruction (bundle, slot_no))
      return(predicate_used);  /* predicate_used is 0 here.. */
       
    switch (slot_no)
    {
      case 0:
        predicate_used = bundle.slots.slot0 & 0x3f;
        break;
      case 1:
        predicate_used = bundle.slots.slot1lo & 0x3f;
        break;
      case 2:
        predicate_used = bundle.slots.slot2 & 0x3f;
        break;
     }

     DEBUG(printf_filtered ("predicate used = %d\n", predicate_used );)
     assert ( predicate_used <= 63 );
   }
   else
   {
     warning ("Invalid address (%016llx:%d) \n", bundle_addr, slot_no);
   }
   return(predicate_used); 
}

void
invalidate_rse_info ()
{
  rse_state_info.have_read_rse_info = FALSE;
}

/* put_stacked_nat - Set the NaT bit of a stacked GR.  Regno should
   be a NaT register number NR32 .. NR127.
   If the register does not exist, do nothing and return 0.
   Nat_value should be 0 or 1.

   Return 0 on success, otherwise errno.
   */

static int
put_stacked_nat (int regno, int	nat_value)
{
  uint64_t	bit_value;
  CORE_ADDR	gr_bs_addr;
  int		gr_regno;
  uint64_t	nat_collection;
  CORE_ADDR	nat_collection_addr;
  uint64_t	mask;
  int		shift_size;
  int		status;
  int 		tt_status;
  ttreq_t       request;


  GET_RSE_STATE_INFO;

  nat_value = nat_value != 0;   /* Make sure nat_value is 0 or 1 */

  gr_regno = regno - NR0_REGNUM + GR0_REGNUM;
  if (!(gr_bs_addr = reg_bs_addr (gr_regno, 0, 0)))
    return 0; /* The register isn't there.  Do nothing. */

  nat_collection_addr = NAT_COLLECTION_ADDR (gr_bs_addr);
  if (nat_collection_addr > rse_state_info.ar_bsp)
    nat_collection = rse_state_info.rnat;
  else
    {
      if (nat_collection_addr == rse_state_info.bsp_stored_nat_addr)
	 nat_collection = rse_state_info.bsp_stored_nat;
      else
	{
	  status = read_inferior_ideal_memory (nat_collection_addr, 
					       (char*) &nat_collection,
	  				       REGISTER_SIZE);
	  if (status)
	    return status;
	  rse_state_info.bsp_stored_nat_addr = nat_collection_addr;
	  rse_state_info.bsp_stored_nat = nat_collection;
	}
    }

  /* From 11.5.2 of EAS 2.6, p 11-8, "RNAT{x} corresponds to the register
     saved at concatenate (BSPSTORE{63:9}, x(5:0), 0{2:0}).  So take 8:3 of
     gr_bs_addr, shift nat_collection right by that many bits and AND the
     result with 1.  That is the desired NaT bit/byte.
     */
  
  shift_size = (int) NAT_BITPOS(gr_bs_addr);
  mask = 1LL << shift_size;
  bit_value = nat_value;
  bit_value = bit_value << shift_size;  /* Shift bit to correct position */
  nat_collection = (nat_collection & (~mask)) | bit_value;

  rse_state_info.bsp_stored_nat = nat_collection;
  rse_state_info.bsp_stored_nat_addr = nat_collection_addr;
  if (nat_collection_addr == NAT_COLLECTION_ADDR(rse_state_info.ar_bsp))
    { 
      rse_state_info.rnat = nat_collection;
    }

  /* Bindu 100606: Use WRRSEBS if the nat_collection_addr is ar.rnat or
     if it falls between bspstore and bsp */
  if (   nat_collection_addr == NAT_COLLECTION_ADDR(rse_state_info.ar_bsp)
      || (   (rse_state_info.ar_bspstore <= nat_collection_addr)
          && (nat_collection_addr < rse_state_info.ar_bsp)))
    request = TT_LWP_WRRSEBS;
  else
    request = TT_PROC_WRDATA;

  tt_status = call_ttrace (request,
                           (pid_t) inferior_pid,
                           (TTRACE_ARG_TYPE) nat_collection_addr,
                           (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
                           (TTRACE_ARG_TYPE) &nat_collection);
  if (tt_status == -1 && errno)
    return errno; 

  return 0;
} /* end put_stacked_nat */

/* put_stacked_register - copy the contents of gdb register regno to
   the RSE backing store for the register.  Return 0 on success, otherwise
   the value of errno.
   */

static int
put_stacked_register (int regno, char *	reg_buf_p)
{
  int		i;
  CORE_ADDR	reg_addr;
  int		tt_status;

  GET_RSE_STATE_INFO;

  if (!(reg_addr = reg_bs_addr (regno, 0, 0)))
    { /* The register isn't there.  Do nothing. */
      return 0;
    }

  if (reg_addr < rse_state_info.ar_bspstore)
    return write_inferior_memory (reg_addr, reg_buf_p, REGISTER_SIZE);

  tt_status = call_ttrace (TT_LWP_WRRSEBS,
			   (pid_t) inferior_pid,
			   (TTRACE_ARG_TYPE) reg_addr,
			   (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
			   (TTRACE_ARG_TYPE) reg_buf_p);
  if (tt_status == -1 && errno)
    {
      return errno;
    }

  return 0;
} /* end put_stacked_register */

/* ia64_target_ops is defined at the end of the file. */


/* Copy LEN bytes from inferior's memory starting at MEMADDR
   to debugger memory starting at MYADDR.  Use read_inferior_memory
   to read everything except addresses between BSPSTORE and BSP.  For
   these, we use TT_LWP_RDRSEBS to read the memory.

   Return 0 on success, otherwise, errno 
   */

int
read_inferior_ideal_memory (CORE_ADDR memaddr, char *myaddr, int len)
{
  char			buffer[RSE_RDWR_SIZE];
  int			bytes_left;
  int			bytes_left_in_buffer;
  CORE_ADDR		end_addr;
  char*			from_ptr;
  CORE_ADDR		read_addr;
  int			read_len;
  CORE_ADDR		start_addr;
  int			status;
  char*			to_ptr;
  int 			tt_status;


  ucontext_t *uc = 0;
  if (!target_has_execution && !(inferior_pid & TID_MARKER))
    uc = get_uc_for (current_core_bfd, inferior_pid);

  GET_RSE_STATE_INFO;

  /* Break the read into up to three parts, use read_inferior_memory
     to read any memory before the [BSPSTORE, BSP) region and any
     memory after the region.  Here we will handle reads inside the region.
     */

  if (memaddr < rse_state_info.ar_bspstore)
    { /* Read before BSPSTORE? */
      end_addr = memaddr + len;
      if (end_addr > rse_state_info.ar_bspstore)
	end_addr = rse_state_info.ar_bspstore;
      read_len = (int) (end_addr - memaddr);
      status = target_read_memory (memaddr, myaddr, read_len);
      if (status)
	return status;
    }
  if (memaddr + len > rse_state_info.ar_bsp)
    { /* Read after BSP? */
      end_addr = memaddr + len;
      start_addr = memaddr;
      if (start_addr < rse_state_info.ar_bsp)
	start_addr = rse_state_info.ar_bsp;
      read_len = (int) (end_addr - start_addr);
      status = target_read_memory (start_addr, 
			    myaddr + start_addr - memaddr,
			    read_len);
      if (status)
	return status;
    }

  /* Read anything in the region of [ar_bspstore .. ar_bsp) */
  start_addr = memaddr;
  if (start_addr < rse_state_info.ar_bspstore)
    start_addr = rse_state_info.ar_bspstore;
  end_addr = memaddr + len;
  if (end_addr > rse_state_info.ar_bsp)
    end_addr = rse_state_info.ar_bsp;
  
  if (start_addr < end_addr)
    { /* Read the regsion from start_addr to end_addr */

      read_addr = start_addr & ~0x7; /* Round down to 8 byte boundary */
      
      /* Read the first 8 bytes;  We might only use part of this */
      if ( (target_has_execution)
#ifdef HP_MXN
          || (is_mxn && get_lwp_for (inferior_pid) == 0)
#endif
         )
	{
	  tt_status = call_ttrace (TT_LWP_RDRSEBS,
				   (pid_t) inferior_pid,
				   (TTRACE_ARG_TYPE) read_addr,
				   (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
					 (TTRACE_ARG_TYPE) &buffer);
	  if ((tt_status == -1) && errno)
	    return errno;
	}
      else
	{
	  tt_status = UC_GET_RSEBS(
			uc, 
			(UC_ARG2_TYPE) read_addr,
			1, 
			(uint64_t *) (void *) &buffer);
	  if (tt_status)
	    return tt_status;
	}

      to_ptr = myaddr + (start_addr - memaddr);
      from_ptr = buffer + start_addr - read_addr;
      bytes_left_in_buffer = (int) (RSE_RDWR_SIZE - (start_addr - read_addr));
      bytes_left = (int) (end_addr - start_addr);

      while (bytes_left)
	{
	  while (bytes_left && bytes_left_in_buffer)
	    {
	      *to_ptr++ = *from_ptr++;
	      bytes_left--;
	      bytes_left_in_buffer--;
	    }

	  if (bytes_left)
	    {
	      read_addr += RSE_RDWR_SIZE;
	      if ( (target_has_execution)
#ifdef HP_MXN
                  || (is_mxn && get_lwp_for (inferior_pid) == 0)
#endif
		 )
		{
		  tt_status = call_ttrace (TT_LWP_RDRSEBS,
					   (pid_t) inferior_pid,
					   (TTRACE_ARG_TYPE) read_addr,
					   (TTRACE_ARG_TYPE) RSE_RDWR_SIZE,
					   (TTRACE_ARG_TYPE) &buffer);
		  if (tt_status == -1 && errno)
		    return errno;
		}
	      else
		{
		  tt_status = UC_GET_RSEBS(
				uc, 
				(UC_ARG2_TYPE) read_addr,
				1, 
				(uint64_t *)(void *) &buffer);
		  if (tt_status)
		    return tt_status;
		}
	      bytes_left_in_buffer = RSE_RDWR_SIZE;
	      from_ptr = buffer;
	    }
	} /* while bytes_left */
       
    } /* Read the regsion from start_addr to end_addr */
  return 0;
} /* end read_inferior_ideal_memory */


/* Copy LEN bytes from inferior's memory starting at MEMADDR
   to debugger memory starting at MYADDR.  Return 0 or errno if
   there was an error.

   To read an entire bundle, we must do the read with one read of 16 bytes.
   For that reason, all reads are done as a multiple of 16 bytes.
   */

int
read_inferior_memory (CORE_ADDR memaddr, char *myaddr, int len)
{
  register int i;

  /* QXCR1000571865: Do we have a read across RSE region ?
   *
   * RSE range is ar.bspstore to ar.bsp. If the start address
   * is beyond bsp or the end address less than bspstore, the read
   * is not across RSE. Otherwise, the read is across RSE and the
   * function read_inferior_ideal_memory() is used to read the content.
   */
  if (address_in_rsebs(memaddr, len))
    {
      return read_inferior_ideal_memory (memaddr, myaddr, len);
    }

  /* Round starting address down to a bundle (16 bytes) boundary.  */
  CORE_ADDR addr = memaddr & 
		   ((CORE_ADDR) (~((signed long long) BUNDLE_SIZE -1)));

  /* 
   * Round ending address up; get number of bundles (16-byte chunks) 
   * that makes.  
   */
  register int count
  = (int) ((((memaddr + len) - addr) + BUNDLE_SIZE - 1) / BUNDLE_SIZE);

  /* Allocate buffer of that many bundles.  */
  bundle_t *buffer = (bundle_t *) alloca (count * sizeof (bundle_t));

  CORE_ADDR loc_addr;
  int status;
  int error_seen = 0;

  TPRINTF ("Asked to read %016llx, start at %016llx, will read %d times\n",
	   memaddr, addr, count);

  /* Read all the long-long-words */
  for (i = 0; i < count; i++, addr += sizeof (bundle_t))
    {
      status = call_ttrace (TT_PROC_RDTEXT,
			    inferior_pid,
			    (TTRACE_ARG_TYPE) addr,
			    sizeof (bundle_t),
			    (TTRACE_ARG_TYPE) & buffer[i]);
      if (status == -1 && errno)
	{ /* Try to read the data as data instead of text */
	  status = call_ttrace (TT_PROC_RDDATA,
			        inferior_pid,
			        (TTRACE_ARG_TYPE) addr,
			        sizeof (bundle_t),
			        (TTRACE_ARG_TYPE) & buffer[i]);
	 if (status == -1 && errno)
	    error_seen = errno;
	}
    }
    if (error_seen)
      {
        TPRINTF ("reading address %llx: %s\n", memaddr, safe_strerror (error_seen));
        return error_seen;
      }

  /* Copy appropriate bytes out of the buffer.  */
  memcpy (myaddr, (char *) buffer + (memaddr & (sizeof (bundle_t) - 1)),
	  len);
  return 0;
} /* end read_inferior_memory */
  
/* reg_bs_addr - given a GDB register number, return the backing store
   address of the register or zero if it does not have a BS address.
   */
/* If cfm, bsp is zero read them from rse_state_info, else from their values
   passed as parameters here. */
CORE_ADDR 
reg_bs_addr (int regno, CORE_ADDR cfm, CORE_ADDR bsp)
{
  int 		last_reg;
  CORE_ADDR	last_reg_addr;
  CORE_ADDR	nat_addr;
  CORE_ADDR	reg_addr;
  int num_rot_regs, rrb_gr ;

  if (!cfm)
    {
      GET_RSE_STATE_INFO;
      num_rot_regs = (int) (8 * _UNW_CFM_SOR (rse_state_info.cfm));
      rrb_gr = (int) _UNW_CFM_RRB_GR (rse_state_info.cfm);
    }
  else
    {
      num_rot_regs = (int) (8 * _UNW_CFM_SOR (cfm));
      rrb_gr = (int) _UNW_CFM_RRB_GR (cfm);
    } 
  
  if (regno < GR0_REGNUM + 32)
    return 0;  /* only stacked registers GR32 .. GR127 have GBS addresses */

  if (!cfm)
    GET_RSE_STATE_INFO;

  if (!cfm)
    last_reg = (int)(31 + SIZE_OF_FRAME + GR0_REGNUM);
  else
    last_reg = (int)(31 + ((cfm) & 0x7f) + GR0_REGNUM);

  if (regno > last_reg)
    return 0;
  /* Get the corresponding physical register if  rotation if rotating 
     register base is nonzero */
  if ((num_rot_regs > 0) && (regno>= GR0_REGNUM + NUM_ST_GRS) &&
     (regno < GR0_REGNUM + NUM_ST_GRS + num_rot_regs))
       regno = GR0_REGNUM + NUM_ST_GRS
               + ((regno-GR0_REGNUM - NUM_ST_GRS)+ rrb_gr) % num_rot_regs;

  /* The last register is 31 + SIZE_OF_FRAME.  It would be stored at
     BSP - REGISTER_SIZE, unless that is a NaT collection, in which case
     it would be the previous slot.
     */
  
  if (!cfm)
    last_reg_addr = rse_state_info.ar_bsp - REGISTER_SIZE;
  else
    /* get the last reg addr for the register stack frame from BSP - reg size. */
   last_reg_addr = bsp - REGISTER_SIZE;
   
  if (last_reg_addr == NAT_COLLECTION_ADDR (last_reg_addr))
    last_reg_addr -= REGISTER_SIZE;
  reg_addr = last_reg_addr - ((last_reg - regno) * REGISTER_SIZE);

  /* We need to reduce reg_addr by one slot for every NaT collection stored
     between it and last_reg_addr.  nat_addr starts out as the first
     nat collection before last_reg_addr.
     */
    
  nat_addr = NAT_COLLECTION_ADDR (last_reg_addr) - (64 * REGISTER_SIZE);
  while (nat_addr >= reg_addr)
    {
      nat_addr -= (64 * REGISTER_SIZE);
      reg_addr -= REGISTER_SIZE;
    }
  return reg_addr;
} /* end reg_bs_addr */


/* Store our register values back into the inferior.
   If REGNO is -1, do this for all registers.
   Otherwise, REGNO specifies which register (so we can save time).  */


void
store_inferior_registers (int regno)
{
  register unsigned int regaddr;
  char buf[80];
  register int i;
  int scratch;
  int write_size;
  int	status;
  ttstate_t*    thread_state_p;
  int warning_given;
  CORE_ADDR old_psr;
  CORE_ADDR new_psr;

  warning_given = 0;

  if (regno >= 0)
    {
      if (CANNOT_STORE_REGISTER (regno))
	return;
      assert ((regno < NUM_REGS) || (regno == PR_REGNUM));
      regaddr = map_gdb_reg_to_ureg[regno];
      if (regaddr == ((unsigned int) -1))
	{ /* The register does not have a simple mapping.  Handle special
	     cases.
	     */
	  if (regno > NR0_REGNUM && regno <= NR0_REGNUM + 31)
	    {  /* Storing NaT bits is weird.  We read the related GR and then
		  do a 9-byte store including a byte for the NaT, bit zero
		  of which is the NaT bit.
		  */
	      char reg_buffer[9];

	      regaddr = map_gdb_reg_to_ureg[GR0_REGNUM + regno - NR0_REGNUM];
	      status = call_ttrace (TT_LWP_RUREGS,
				    inferior_pid,
				    (TTRACE_ARG_TYPE) regaddr,
				    9, /* GR plus NaT byte*/
				    (TTRACE_ARG_TYPE) &reg_buffer[0]);
	      CHECK_READ_REG (status, regno, errno, warning_given);

	      reg_buffer[8] = registers[REGISTER_BYTE (regno)];
	      status = call_ttrace (TT_LWP_WUREGS,
				    inferior_pid,
				    (TTRACE_ARG_TYPE) regaddr, 
				    9,  /* GR + NaT byte */
				    (TTRACE_ARG_TYPE) &reg_buffer[0]);
	      CHECK_WRITE_REG (status, regno, errno, warning_given);
	      return;
	    }
	  else if (regno >= NR0_REGNUM + 32 && regno <= NRLAST_REGNUM)
	    {
	      status = put_stacked_nat (regno, 
					registers[REGISTER_BYTE (regno)]);
	      if (status)
		{
                  CHECK_WRITE_REG_BODY (regno, status, warning_given);
		}
	      return;
	    }
	} /* if (regaddr == -1) */

      if (regno >= FR0_REGNUM && regno <= FRLAST_REGNUM)
	{ /* Storing FR's is special because gdb keeps them in 12 bytes but
	     we write them in ttrace using 16 bytes.
	     */
	  char reg_buffer[REG_BUF_LEN];

	  reg_buffer[0] = reg_buffer[1] = reg_buffer[2] = reg_buffer[3] =  0;
	  memcpy (&reg_buffer[4], &registers[REGISTER_BYTE (regno)], 12);
	  status = call_ttrace (TT_LWP_WUREGS,
			        inferior_pid,
			        (TTRACE_ARG_TYPE) regaddr, 
			        16,  /* FR's are 16 bytes in ttrace */
			        (TTRACE_ARG_TYPE) &reg_buffer[0]);
	  /* With Lazy FP we may get EINVAL for f32-f127.  If we get
	     EINVAL, ignore it.
	     */
	  if (regno < FR0_REGNUM + 32 || errno != EINVAL)
	    {
	      CHECK_WRITE_REG (status, regno, errno, warning_given);
	    }
	  return;
	}
      else if (regno > GR0_REGNUM + 31 && regno <= GRLAST_REGNUM)
	{
	  status = put_stacked_register (regno, &registers[REGISTER_BYTE (regno)]);
	  CHECK_WRITE_REG (status, regno, errno, warning_given);
	  return;
	}
      /* Writing to Predicate registers is writing it to the 
	 corresponding bit in __pr */
      else if (regno >= PR0_REGNUM && regno <= PRLAST_REGNUM)
	{
    	  char reg_buffer[8];
	  CORE_ADDR regvalue;
          regaddr = map_gdb_reg_to_ureg[PR0_REGNUM];
          errno = 0;
	  status = call_ttrace (TT_LWP_RUREGS,
                                inferior_pid,
                                (TTRACE_ARG_TYPE) regaddr,
                                8,  /* All these writes are 8 bytes */
                                (TTRACE_ARG_TYPE)
                                  reg_buffer);
	  CHECK_WRITE_REG (status, regno, errno, warning_given);
	  regvalue = 0; /* initialize for compiler warning */
	  memcpy(&regvalue, reg_buffer, sizeof(regvalue));
	  if (registers[REGISTER_BYTE (regno)])
	    {
	      regvalue = regvalue | (1LL << (regno - PR0_REGNUM));
	    }
          else
	    {
	      regvalue = regvalue & ~(1 << (regno - PR0_REGNUM));
	    }
	  memcpy(reg_buffer, &regvalue, sizeof(regvalue));
          status = call_ttrace (TT_LWP_WUREGS,
		   	        inferior_pid,
			        (TTRACE_ARG_TYPE) regaddr, 
			        8,  /* All these writes are 8 bytes */
			        (TTRACE_ARG_TYPE) 
			          reg_buffer);
	  CHECK_WRITE_REG (status, regno, errno, warning_given);
	  return;
	}
      else if (regno == PR_REGNUM)
        {
	  regaddr = map_gdb_reg_to_ureg[PR0_REGNUM];
	  errno = 0;
	  status = call_ttrace (TT_LWP_WUREGS,
                                inferior_pid,
                                (TTRACE_ARG_TYPE) regaddr,
                                8,  /* All these writes are 8 bytes */
                                (TTRACE_ARG_TYPE)
                                &registers[REGISTER_BYTE (regno)]);
          CHECK_WRITE_REG (status, regno, errno, warning_given);
          return;
        }

/* Bindu: I didn't find any need to modify ar18 (bspstore) till now. 
   Writing to BSPSTORE has a side effect of writing to BSP which is a 
   problem when restoring the registers after a command line call.
   If anybody need to write to this register in future, please handle 
   the above case. */

      if (regno == AR0_REGNUM + 18)
        {
	  return;
 	}

      if (regno == AR0_REGNUM + 19)
        {
          errno = 0;
	  GET_RSE_STATE_INFO;

         /* Read the nat_collection */
         status = call_ttrace (TT_LWP_WRRSEBS,
                            inferior_pid,
                            (TTRACE_ARG_TYPE)
                              NAT_COLLECTION_ADDR(rse_state_info.ar_bsp),
                            8, /* All these writes are 8 bytes */
                            (TTRACE_ARG_TYPE) 
	  	  	      &registers[REGISTER_BYTE (regno)]);
	  CHECK_WRITE_REG (status, regno, errno, warning_given);
          return;
        }
      else if (   (regno == AR0_REGNUM + 64)
	       || (regno == AR0_REGNUM + 40))
	{
	  CORE_ADDR new_value;

	  assert (REGISTER_RAW_SIZE (regno) == sizeof (CORE_ADDR));
	  memcpy ((char*) &new_value, &(registers[REGISTER_BYTE (regno)]),
		  sizeof (CORE_ADDR));

	  if (regno == (AR0_REGNUM + 64))
	    {
	      /* Must not set reserved fields 61-58 and 51-38 */
	      new_value = new_value & AR64_RESERVED_MASK;
	    }
	  else if (regno == (AR0_REGNUM + 40))
	    {
	      /* Bits 63-58 are reserved in AR40 = Floating-point Status 
		 Register.  Also bit 12 is td (Traps disabled) of FPSR.sf0, 
		 which is reserved.
		 */
	      new_value = new_value & AR40_RESERVED_MASK;
	    }

	  errno = 0;
	  status = call_ttrace (TT_LWP_WUREGS,
				inferior_pid,
				(TTRACE_ARG_TYPE) regaddr,
				sizeof(CORE_ADDR),  
				(TTRACE_ARG_TYPE) &new_value);
	  CHECK_WRITE_REG (status, regno, errno, warning_given);
          return;
	}
      else if (regno == PSR_REGNUM)
	{ /* The PSR register is not writable in whole, the user mask
	   * (the low 5 bits) is writable with using the __um register.
	   * If the current value of PSR is being written, do nothing.
	   * If the value being written only differs in the user mask,
	   * set the user mask.  Otherwise, give a warning.
	   * Oh! why give a warning? Just write only to the lower 5.
	   */

	  status = call_ttrace (TT_LWP_RUREGS,
				inferior_pid,
				(TTRACE_ARG_TYPE) regaddr,
				sizeof (old_psr),
				(TTRACE_ARG_TYPE) &old_psr);
	  CHECK_READ_REG (status, regno, errno, warning_given);
	  memcpy (&new_psr, 
		  &registers[REGISTER_BYTE (regno)], 
		  sizeof (new_psr));
	  if (old_psr == new_psr)
	    return;  /* no change in value */
	  if ((old_psr & ~0x1f) != (new_psr & ~0x1f))
	    {
	      new_psr = (old_psr & ~0x1f) | (new_psr & 0x1f);
	      memcpy (&registers[REGISTER_BYTE (regno)], 
		      &new_psr,
		      sizeof (new_psr));
	      if (stop_whining)
		warning ("Only low 5 bits of PSR are modified.");
	    }
	  
	  errno = 0;
	  status = call_ttrace (TT_LWP_WUREGS,
				inferior_pid,
				(TTRACE_ARG_TYPE) __um,
				8,  /* All these writes are 8 bytes */
				(TTRACE_ARG_TYPE)
				  &registers[REGISTER_BYTE (regno)]);
	    
	  CHECK_WRITE_REG (status, regno, errno, warning_given);
	  return;
	}
      
      errno = 0;
      status = call_ttrace (TT_LWP_WUREGS,
                            inferior_pid,
                            (TTRACE_ARG_TYPE) regaddr,
                            8,  /* All these writes are 8 bytes */
                            (TTRACE_ARG_TYPE)
                              &registers[REGISTER_BYTE (regno)]);
	
      CHECK_WRITE_REG (status, regno, errno, warning_given);
      return;
    }
  else
    for (regno = 0; regno < NUM_REGS; regno++)
      store_inferior_registers (regno);

} /* end store_inferior_registers */

/* Copy LEN bytes of data from debugger memory at MYADDR
   to inferior's memory at MEMADDR.
   On failure (cannot write the inferior)
   returns the value of errno.  

   Because we can only write bundles as entire 16-byte writes, we do all
   writes with as a multiple of 16-bytes.  To read and write smaller areas
   we read in the preceeding and following bytes to round the region
   out to a multiple of 16 bytes.
   
   */

int
write_inferior_memory (CORE_ADDR memaddr, char *myaddr, int len)
{
  register int i;
  /* Round starting address down to bundle (16 bytes) boundary.  */
  CORE_ADDR addr = memaddr & ((CORE_ADDR) (-((long) sizeof (bundle_t))));
  int  first_write_failed;

  /*
   * Round ending address up; get number of bundles (16-byte chunks)
   * that makes.
   */
  register int count
  = (int) ((((memaddr + len) - addr) + sizeof (bundle_t) - 1) / sizeof (bundle_t));

  /* Allocate buffer of that many long-long-words.  */
  bundle_t *buffer = (bundle_t *) alloca (count * sizeof (bundle_t));

  extern int errno;
  int status;

  TPRINTF ("Asked to write %016llx, start at %016llx, will write %d times\n",
	   memaddr, addr, count);

  {
    int j;

    for (j = 0; j < len; j++)
      {
	TPRINTF ("    asked-to-write-byte %d : %x\n", j,
		 (unsigned char) myaddr[j]);
      }
  }

  /* Fill start and end extra bytes of buffer with existing memory data.  */
  TPRINTF ("Reading initial bytes at %016llx\n", addr);

  /* JAGag16749 - if address is in this special region better use RSEBS */
  if (address_in_rsebs(addr, sizeof (bundle_t)))
    {

      status = call_ttrace (TT_LWP_RDRSEBS, 
	   	            inferior_pid,
		            (TTRACE_ARG_TYPE) addr,
		            (TTRACE_ARG_TYPE) sizeof (bundle_t),
		            (TTRACE_ARG_TYPE) & buffer[0]);

      if (status = -1 && errno)
        return errno; 

    }
  else
    {
      status = call_ttrace (TT_PROC_RDTEXT,
	   	            inferior_pid,
		            (TTRACE_ARG_TYPE) addr,
		            (TTRACE_ARG_TYPE) sizeof (bundle_t),
		            (TTRACE_ARG_TYPE) & buffer[0]);

      if (status == -1 && errno)
        { /* Try using RDDATA instead */
          status = call_ttrace (TT_PROC_RDDATA,
	  		        inferior_pid,
                                (TTRACE_ARG_TYPE) addr,
                                (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                (TTRACE_ARG_TYPE) & buffer[0]);

          if (status == -1 && errno)
	    return errno;
        }

    } /* memaddr is not between bspstore and bsp */

  if (count > 1)
    {
      /* JAGag16749 - if address is in this special region 
       * better use RSEBS 
       */
      if (address_in_rsebs(addr + (count - 1) * sizeof (bundle_t),
                           sizeof (bundle_t)))
        {
           status = call_ttrace (TT_LWP_RDRSEBS, 
                                 inferior_pid,
                                 (TTRACE_ARG_TYPE) 
                                 addr 
                                 + (count - 1) * sizeof (bundle_t),
                                 (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                 (TTRACE_ARG_TYPE) & buffer[count - 1]);

           if (status = -1 && errno)
             return errno; 

        }
      else
        {
           status = call_ttrace (TT_PROC_RDTEXT,
                                 inferior_pid,
                                 (TTRACE_ARG_TYPE) 
                                 addr 
                                 + (count - 1) * sizeof (bundle_t),
                                 (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                 (TTRACE_ARG_TYPE) & buffer[count - 1]);

           if (status == -1 && errno)
             { /* Try using TT_PROC_RDDATA instead */
               status = call_ttrace (TT_PROC_RDDATA,
                                     inferior_pid,
                                     (TTRACE_ARG_TYPE) 
                                     addr 
                                     + (count - 1) * sizeof (bundle_t),
                                     (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                     (TTRACE_ARG_TYPE) & buffer[count - 1]);
               if (status == -1 && errno)
                 return errno;
             }

        } /* memaddr is not between bspstore and bsp */
    } /* count > 1 */

  /* Copy data to be written over corresponding part of buffer */

  memcpy ((char *) buffer + (memaddr & (sizeof (bundle_t) - 1)), myaddr, len);

  /* Write the entire buffer.  */

  for (i = 0; i < count; i++, addr += sizeof (bundle_t))
    {
      TPRINTF ("Writing bytes at %016llx (%d) - %016llx  %016llx\n",
	       addr, i, buffer[i].halves.h, buffer[i].halves.l);
      {
	int j;
	unsigned char *ptr = (unsigned char *) &buffer[i];

	for (j = 0; j < sizeof (bundle_t); j++)
	  TPRINTF ("    writing %016llx + %d : %x\n", addr, j, ptr[j]);

      }

      errno = 0;

      /* JAGag16749 - if address is in this special region 
       * better use RSEBS 
       */
      if (address_in_rsebs(addr, sizeof (bundle_t)))
        {

          status = call_ttrace (TT_LWP_WRRSEBS, 
                                inferior_pid,
                                (TTRACE_ARG_TYPE) addr,
                                (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                (TTRACE_ARG_TYPE) &buffer[i]);
          
          if (status == -1 && errno)
            {
              TPRINTF ("Write failed, errno is %d, memaddr is 0x%llx \n",
                       errno, memaddr);
              return errno;
            }

        }
      else
        {
          /* 
           * To be sure that caches are updated, write both text and data.
           * If either works, assume it is OK.
           */

          status = call_ttrace (TT_PROC_WRTEXT,
                                inferior_pid,
                                (TTRACE_ARG_TYPE) addr,
                                (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                (TTRACE_ARG_TYPE) &buffer[i]);
          first_write_failed = (status == -1 && errno);

          /* Try using TT_PROC_WRDATA instead */
          status = call_ttrace (TT_PROC_WRDATA,
                                inferior_pid,
                                (TTRACE_ARG_TYPE) addr,
                                (TTRACE_ARG_TYPE) sizeof (bundle_t),
                                (TTRACE_ARG_TYPE) &buffer[i]);

          if (status == -1 && errno && first_write_failed)
            {
              TPRINTF ("Write failed, errno is %d, memaddr is 0x%llx \n",
                       errno, memaddr);
              return errno;
            }

        } /* memaddr not between bspstore and bsp */

    }  /* for */

  return 0;
} /* end write_inferior_memory */

/* Move the registers already saved around so that the correct virtual 
   registers rotations are displayed 
   For information on rotating registers refer to Section 4.5.1,
   Volume 1 of the IA64 Architecture Software Developer's Manual. */

static void
handle_rotation(void)
{
  int num_rot_regs, rrb_gr, rrb_pr, rrb_fr, i, regno;
  CORE_ADDR saved_reg[96];
  char saved_pr_reg[48];
  char saved_fp_reg[96][REGISTER_RAW_SIZE(FR0_REGNUM)];

  GET_RSE_STATE_INFO;
  num_rot_regs =(int) (8 * _UNW_CFM_SOR (rse_state_info.cfm));
  rrb_gr =(int)  _UNW_CFM_RRB_GR (rse_state_info.cfm);
  rrb_pr = (int) _UNW_CFM_RRB_PR (rse_state_info.cfm);
  rrb_fr = (int) _UNW_CFM_RRB_FR (rse_state_info.cfm);

  if (rrb_gr != 0)
    {
      for (i = 0; i < num_rot_regs; i++ )
        saved_reg[i] = read_register(GR0_REGNUM + NUM_ST_GRS +i);
      for (i = 0; i < num_rot_regs; i++ )
        {
          regno = (i + rrb_gr) % num_rot_regs;
          supply_register (GR0_REGNUM+NUM_ST_GRS+i,(char *) &saved_reg[regno]);
        }
    }
  
  if (rrb_pr != 0)
    {
      for (i = 0; i < 48; i++ )
        saved_pr_reg[i] = read_register(PR0_REGNUM + 16 + i);
      for (i = 0; i < 48; i++ )
        {
          regno = (i + rrb_pr) % 48;
          supply_register (PR0_REGNUM+16+i, (char *) &saved_pr_reg[regno]);
        }
    }
  
  if (rrb_fr != 0)
    {
      for (i = 0; i < 96; i++ )
        memcpy(saved_fp_reg[i], &registers[REGISTER_BYTE(FR0_REGNUM + 32 + i)],
	  REGISTER_RAW_SIZE (FR0_REGNUM));
      for (i = 0; i < 96; i++ )
        {
          regno = (i + rrb_fr) % 96;
          supply_register (FR0_REGNUM+32+i, (char *) &saved_fp_reg[regno]);
        }
    }
  
}


/* Extract the register values out of the core file and store
   them where `read_register' will find them.

   CORE_REG_SECT points to the register values themselves, read into memory.
   CORE_REG_SIZE is the size of that area.
   WHICH says which set of registers we are handling (0 = int, 2 = float
   on machines where they are discontiguous).
   REG_ADDR is the offset from u.u_ar0 to the register values relative to
   core_reg_sect.  This is used with old-fashioned core files to
   locate the registers in a large upage-plus-stack ".reg" section.
   Original upage address X is at location core_reg_sect+x+reg_addr.
 */

static void
fetch_core_registers (char *core_reg_sect, unsigned core_reg_size, int which,
     		      CORE_ADDR reg_addr)
{
  int regno;
  CORE_ADDR addr;
  int bad_reg = -1;
  CORE_ADDR reg_ptr = -reg_addr;	/* Original u.u_ar0 is -reg_addr. */
  int numregs = ARCH_NUM_REGS;

#ifdef FIND_ACTIVE_THREAD
  /* If we need to deal with CMA threads then if the CMA_THREAD_FLAG is
     set and the current pid is not the active thread, then call 
     hpux_thread_fetch_registers to restore the registers.
   */

  if (IS_CMA_PID(inferior_pid)
      && inferior_pid != FIND_ACTIVE_THREAD ())
    {
      THREAD_FETCH_REGISTERS (-1);	/* -1 means fetch all registers. */
      return;
    }
#endif

  /* If u.u_ar0 was an absolute address in the core file, relativize it now,
     so we can use it as an offset into core_reg_sect.  When we're done,
     "register 0" will be at core_reg_sect+reg_ptr, and we can use
     CORE_REGISTER_ADDR to offset to the other registers.  If this is a modern
     core file without a upage, reg_ptr will be zero and this is all a big
     NOP.  */
  if (reg_ptr > core_reg_size)
    reg_ptr -= KERNEL_U_ADDR;

  for (regno = 0; regno < numregs; regno++)
    {
      addr = CORE_REGISTER_ADDR (regno, reg_ptr);
      if (addr >= core_reg_size
	  && bad_reg < 0)
	bad_reg = regno;
      else
	{
	  supply_register (regno, core_reg_sect + addr);
	}
    }				/* for regno */

#ifdef GET_CORE_STACKED_REGS
  GET_CORE_STACKED_REGS (core_reg_sect, reg_ptr);
#endif
  if (bad_reg >= 0)
    error ("Register %s not found in core file.", reg_names[bad_reg]);
  handle_rotation();

}

static struct core_fns hp_ia64_elf_core_fns =
{
  bfd_target_elf_flavour,               /* core_flavour */
  default_check_format,                 /* check_format */
  default_core_sniffer,                 /* core_sniffer */
  fetch_core_registers,            	/* core_read_registers */
  NULL					/* next */
};

void
_initialize_core_ia64 ()
{
  add_core_fns (&hp_ia64_elf_core_fns);
}
