/* Read hp debug symbols and convert to internal format, for GDB.
   Copyright 1993, 1996, 1998, 1999 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Written by the Center for Software Science at the University of Utah
   and by Cygnus Support.  */

/* Common include file for hp_symtab_read.c and hp_psymtab_read.c.
   This has nested includes of a bunch of stuff. */

#include "sys/types.h"
#include "defs.h"
#include "bfd.h"
#include "hpread.h"
#include "demangle.h"

extern int incremental_expansion;

/* To generate dumping code, uncomment this define.  The dumping
   itself is controlled by routine-local statics called "dumping". */
/* #define DUMPING */

/* To use the quick look-up tables, uncomment this define. */
#define QUICK_LOOK_UP

/* To call PXDB to process un-processed files, uncomment this define. */
/* JYG: MERGE NOTE: this should go away and have the decision made
   at runtime */
#define USE_PXDB

/* Forward procedure declarations */

void hpread_symfile_init (struct objfile *);

void hpread_build_psymtabs (struct objfile *, int);

void hpread_symfile_finish (struct objfile *);

static union dnttentry *hpread_get_gntt (int, struct objfile *);

static CORE_ADDR hpread_get_textlow (int, int, struct objfile *, int);

static struct partial_symtab *hpread_start_psymtab
  (struct objfile *, char *, CORE_ADDR, int,
   struct partial_symbol **, struct partial_symbol **);

static struct partial_symtab *hpread_end_psymtab
  (struct partial_symtab *, char **, int, int, CORE_ADDR,
   struct partial_symtab **, int);

/* End of forward routine declarations */

#define PXDB_SVR4 "/opt/langtools/bin/pxdb"

#ifdef USE_PXDB
/* NOTE use of system files!  May not be portable. */
static int use_pxdb = 1;
#else
static int use_pxdb = 0;
#endif

/* RM: Global to indicate whether debug information is in object
 * files.
 */
int doom_executable = 0;

/* check for the existance of a file, given its full pathname */
int
file_exists (filename)
     char *filename;
{
  if (filename)
    return (access (filename, F_OK) == 0);
  return 0;
}


/* Translate from the "hp_language" enumeration in hp-symtab.h
   used in the debug info to gdb's generic enumeration in defs.h. */
static enum language
trans_lang (in_lang)
     enum hp_language in_lang;
{
  if (in_lang == HP_LANGUAGE_C)
    return language_c;

  else if (in_lang == HP_LANGUAGE_CPLUSPLUS)
    return language_cplus;

  else if (in_lang == HP_LANGUAGE_FORTRAN)
    return language_fortran;

  else
    return language_unknown;
}

char default_fortran_main_string[] = "_main_";

/* Call PXDB to process our file.

   Approach copied from DDE's "dbgk_run_pxdb".  Note: we
   don't check for BSD location of pxdb, nor for existance
   of pxdb itself, etc.

   NOTE: uses system function and string functions directly.

   Return value: 1 if ok, 0 if not */
int
hpread_call_pxdb (file_name)
     char *file_name;
{
  int retval;

  if (file_exists (PXDB_SVR4))
    {
      char *p = alloca (strlen (PXDB_SVR4) + strlen (file_name) + 2);
      sprintf (p, PXDB_SVR4 " %s", file_name);

      warning ("File not processed by pxdb--about to process now.\n");
      retval = (system (p) == 0);
    }
  else
    {
      warning ("pxdb not found at standard location: /opt/langtools/bin\ngdb will not be able to debug %s.\nPlease install pxdb at the above location and then restart gdb.\nYou can also run pxdb on %s with the command\n\"pxdb %s\" and then restart gdb.", file_name, file_name, file_name);

      retval = 0;
    }
  return retval;
}				/* hpread_call_pxdb */

int
hpread_init_debug_in_object_files(sym_bfd)
              bfd  *sym_bfd;
{
    asection *type_section;
    asection *map_section;

    unsigned long tmp;
    unsigned int pxdbed;

    /* RM: if it has a $TYPE$ subspace or a .objdebug_type section, it is a 
     * doom executable. For good measure check for the subspace map as well
     */
    if (TARGET_PTR_BIT == 64)
      {
	type_section = bfd_get_section_by_name( sym_bfd, ".objdebug_type");
	map_section = bfd_get_section_by_name( sym_bfd, ".linkmap");
      }
    else
      {
	type_section = bfd_get_section_by_name( sym_bfd, "$TYPE$");
	map_section = bfd_get_section_by_name( sym_bfd, "$LINKMAP$");
      }

    if(type_section && map_section)
        doom_executable = 1;
    else
        doom_executable = 0;
}


/* Return 1 if the file turns out to need pre-processing
   by PXDB, and we have thus called PXDB to do this processing
   and the file therefore needs to be re-loaded.  Otherwise
   return 0. */
int
hpread_pxdb_needed (sym_bfd)
     bfd *sym_bfd;
{
  asection *pinfo_section, *debug_section, *header_section;
  unsigned int do_pxdb = false;
  char *buf;
  bfd_size_type header_section_size;

  unsigned long tmp;
  unsigned int pxdbed;

  if (! use_pxdb) /* do not use pxdb */
    return 0;

  header_section = bfd_get_section_by_name( sym_bfd, HP_HEADER);
  if (!header_section)
    return 0;			/* No header at all, can't recover... */

  debug_section = bfd_get_section_by_name (sym_bfd, "$DEBUG$");
  pinfo_section = bfd_get_section_by_name (sym_bfd, "$PINFO$");

  if (pinfo_section && !debug_section)
    {
      /* Debug info with DOC, has different header format. 
         this only happens if the file was pxdbed and compiled optimized
         otherwise the PINFO section is not there. */
      header_section_size = bfd_section_size (sym_bfd, header_section);

      if (header_section_size == (bfd_size_type) sizeof (DOC_info_PXDB_header))
	{
	  buf = alloca (sizeof (DOC_info_PXDB_header));

	  if (!bfd_get_section_contents (sym_bfd,
					 header_section,
					 buf, 0,
					 header_section_size))
	    error ("bfd_get_section_contents\n");

	  tmp = bfd_get_32 (sym_bfd, (bfd_byte *) (buf + sizeof (int) * 4));
	  pxdbed = (tmp >> 31) & 0x1;

	  if (!pxdbed)
	    error ("file debug header info invalid\n");
	  do_pxdb = 0;
	}

      else
	error ("invalid $HEADER$ size in executable \n");
    }

  else
    {

      /* this can be three different cases:
         1. pxdbed and not doc
         - DEBUG and HEADER sections are there
         - header is PXDB_header type
         - pxdbed flag is set to 1

         2. not pxdbed and doc
         - DEBUG and HEADER  sections are there
         - header is DOC_info_header type
         - pxdbed flag is set to 0

         3. not pxdbed and not doc
         - DEBUG and HEADER sections are there
         - header is XDB_header type
         - pxdbed flag is set to 0

         NOTE: the pxdbed flag is meaningful also in the not
         already pxdb processed version of the header,
         because in case on non-already processed by pxdb files
         that same bit in the header would be always zero.
         Why? Because the bit is the leftmost bit of a word
         which contains a 'length' which is always a positive value
         so that bit is never set to 1 (otherwise it would be negative)

         Given the above, we have two choices : either we ignore the
         size of the header itself and just look at the pxdbed field,
         or we check the size and then we (for safety and paranoia related
         issues) check the bit.
         The first solution is used by DDE, the second by PXDB itself.
         I am using the second one here, because I already wrote it,
         and it is the end of a long day.
         Also, using the first approach would still involve size issues
         because we need to read in the contents of the header section, and
         give the correct amount of stuff we want to read to the
         get_bfd_section_contents function.  */

      /* decide which case depending on the size of the header section.
         The size is as defined in hp-symtab.h  */

      header_section_size = bfd_section_size (sym_bfd, header_section);

      if (header_section_size == (bfd_size_type) sizeof (PXDB_header))	/* pxdb and not doc */
	{

	  buf = alloca (sizeof (PXDB_header));
	  if (!bfd_get_section_contents (sym_bfd,
					 header_section,
					 buf, 0,
					 header_section_size))
	    error ("bfd_get_section_contents\n");

	  tmp = bfd_get_32 (sym_bfd, (bfd_byte *) (buf + sizeof (int) * 3));
	  pxdbed = (tmp >> 31) & 0x1;

	  if (pxdbed)
	    do_pxdb = 0;
	  else
	    error ("file debug header invalid\n");
	}
      else			/*not pxdbed and doc OR not pxdbed and non doc */
	do_pxdb = 1;
    }

  if (do_pxdb)
    return 1;
  else
    return 0;
}


#ifdef QUICK_LOOK_UP

/* Code to handle quick lookup-tables follows. */


/* Some useful macros */
#define VALID_FILE(i)   ((i) < pxdb_header_p->fd_entries)
#define VALID_MODULE(i) ((i) < pxdb_header_p->md_entries)
#define VALID_PROC(i)   ((i) < pxdb_header_p->pd_entries)
#define VALID_CLASS(i)  ((i) < pxdb_header_p->cd_entries)

#define FILE_START(i)    (qFD[i].adrStart)
#define MODULE_START(i) (qMD[i].adrStart)
#define PROC_START(i)    (qPD[i].adrStart)

#define FILE_END(i)   (qFD[i].adrEnd)
#define MODULE_END(i) (qMD[i].adrEnd)
#define PROC_END(i)   (qPD[i].adrEnd)

#define FILE_ISYM(i)   (qFD[i].isym)
#define MODULE_ISYM(i) (qMD[i].isym)
#define PROC_ISYM(i)   (qPD[i].isym)

#define VALID_CURR_FILE    (curr_fd < pxdb_header_p->fd_entries)
#define VALID_CURR_MODULE  (curr_md < pxdb_header_p->md_entries)
#define VALID_CURR_PROC    (curr_pd < pxdb_header_p->pd_entries)
#define VALID_CURR_CLASS   (curr_cd < pxdb_header_p->cd_entries)

#define CURR_FILE_START     (qFD[curr_fd].adrStart)
#define CURR_MODULE_START   (qMD[curr_md].adrStart)
#define CURR_PROC_START     (qPD[curr_pd].adrStart)

#define CURR_FILE_END    (qFD[curr_fd].adrEnd)
#define CURR_MODULE_END  (qMD[curr_md].adrEnd)
#define CURR_PROC_END    (qPD[curr_pd].adrEnd)

#define CURR_FILE_ISYM    (qFD[curr_fd].isym)
#define CURR_MODULE_ISYM  (qMD[curr_md].isym)
#define CURR_PROC_ISYM    (qPD[curr_pd].isym)

#define TELL_OBJFILE                                      \
            do {                                          \
               if( !told_objfile ) {                      \
                   told_objfile = 1;                      \
                   warning ("\nIn object file \"%s\":\n", \
                            objfile->name);               \
               }                                          \
            } while (0)



/* Keeping track of the start/end symbol table (LNTT) indices of
   psymtabs created so far */

typedef struct
{
  int start;
  int end;
}
pst_syms_struct;

static pst_syms_struct *pst_syms_array = 0;

static pst_syms_count = 0;
static pst_syms_size = 0;

/* used by the TELL_OBJFILE macro */
static boolean told_objfile = 0;

/* Set up psymtab symbol index stuff */
static void
init_pst_syms ()
{
  pst_syms_count = 0;
  pst_syms_size = 20;
  pst_syms_array = (pst_syms_struct *) xmalloc (20 * sizeof (pst_syms_struct));
}

/* Clean up psymtab symbol index stuff */
static void
clear_pst_syms ()
{
  pst_syms_count = 0;
  pst_syms_size = 0;
  free (pst_syms_array);
  pst_syms_array = 0;
}

/* Add information about latest psymtab to symbol index table */
static void
record_pst_syms (start_sym, end_sym)
     int start_sym;
     int end_sym;
{
  if (++pst_syms_count > pst_syms_size)
    {
      pst_syms_array = (pst_syms_struct *) xrealloc (pst_syms_array,
			      2 * pst_syms_size * sizeof (pst_syms_struct));
      pst_syms_size *= 2;
    }
  pst_syms_array[pst_syms_count - 1].start = start_sym;
  pst_syms_array[pst_syms_count - 1].end = end_sym;
}

/* Find a suitable symbol table index which can serve as the upper
   bound of a psymtab that starts at INDEX

   This scans backwards in the psymtab symbol index table to find a
   "hole" in which the given index can fit.  This is a heuristic!!
   We don't search the entire table to check for multiple holes,
   we don't care about overlaps, etc. 

   Return 0 => not found */
static int
find_next_pst_start (index)
     int index;
{
  int i;

  for (i = pst_syms_count - 1; i >= 0; i--)
    if (pst_syms_array[i].end <= index)
      return (i == pst_syms_count - 1) ? 0 : pst_syms_array[i + 1].start - 1;

  if (pst_syms_array[0].start > index)
    return pst_syms_array[0].start - 1;

  return 0;
}



/* Utility functions to find the ending symbol index for a psymtab */

/* Find the next file entry that begins beyond INDEX, and return
   its starting symbol index - 1.
   QFD is the file table, CURR_FD is the file entry from where to start,
   PXDB_HEADER_P as in hpread_quick_traverse (to allow macros to work).

   Return 0 => not found */
static int
find_next_file_isym (index, qFD, curr_fd, pxdb_header_p)
     int index;
     quick_file_entry *qFD;
     int curr_fd;
     PXDB_header_ptr pxdb_header_p;
{
  while (VALID_CURR_FILE)
    {
      if (CURR_FILE_ISYM >= index)
	return CURR_FILE_ISYM - 1;
      curr_fd++;
    }
  return 0;
}

/* Find the next procedure entry that begins beyond INDEX, and return
   its starting symbol index - 1.
   QPD is the procedure table, CURR_PD is the proc entry from where to start,
   PXDB_HEADER_P as in hpread_quick_traverse (to allow macros to work).

   Return 0 => not found */
static int
find_next_proc_isym (index, qPD, curr_pd, pxdb_header_p)
     int index;
     quick_procedure_entry *qPD;
     int curr_pd;
     PXDB_header_ptr pxdb_header_p;
{
  while (VALID_CURR_PROC)
    {
      if (CURR_PROC_ISYM >= index)
	return CURR_PROC_ISYM - 1;
      curr_pd++;
    }
  return 0;
}

/* Find the next module entry that begins beyond INDEX, and return
   its starting symbol index - 1.
   QMD is the module table, CURR_MD is the modue entry from where to start,
   PXDB_HEADER_P as in hpread_quick_traverse (to allow macros to work).

   Return 0 => not found */
static int
find_next_module_isym (index, qMD, curr_md, pxdb_header_p)
     int index;
     quick_module_entry *qMD;
     int curr_md;
     PXDB_header_ptr pxdb_header_p;
{
  while (VALID_CURR_MODULE)
    {
      if (CURR_MODULE_ISYM >= index)
	return CURR_MODULE_ISYM - 1;
      curr_md++;
    }
  return 0;
}

/* Scan and record partial symbols for all functions starting from index
   pointed to by CURR_PD_P, and between code addresses START_ADR and END_ADR.
   Other parameters are explained in comments below. */

/* This used to be inline in hpread_quick_traverse, but now that we do essentially the
   same thing for two different cases (modules and module-less files), it's better
   organized in a separate routine, although it does take lots of arguments. pai/1997-10-08 */

static int
scan_procs (curr_pd_p, qPD, max_procs, start_adr, end_adr_p, pst, vt_bits, objfile, curr_fd_p, qFD, max_files)
     int *curr_pd_p;		/* pointer to current proc index */
     quick_procedure_entry *qPD;	/* the procedure quick lookup table */
     int max_procs;		/* number of entries in proc. table */
     CORE_ADDR start_adr;	/* beginning of code range for current psymtab */
    CORE_ADDR *end_adr_p;   /* pointer to end of code range for current psymtab */ 
     struct partial_symtab *pst;	/* current psymtab */
     char *vt_bits;		/* strings table of SOM debug space */
     struct objfile *objfile;	/* current object file */
     int *curr_fd_p;    /* pointer to current file index */
     quick_file_entry *qFD;          /* file quick lookup table */
     int max_files;     /* number of entries in file table */
{
  union dnttentry *dn_bufp;
  int symbol_count = 0;		/* Total number of symbols in this psymtab */
  CORE_ADDR end_adr = *end_adr_p; /* Convenience variables -- avoid dereferencing pointer all the time */ 
  int curr_pd = *curr_pd_p;
  int curr_fd = *curr_fd_p;

#ifdef DUMPING
  /* Turn this on for lots of debugging information in this routine */
  static int dumping = 0;
#endif

#ifdef DUMPING
  if (dumping)
    {
        printf ("Scan_procs called, addresses ");
        print_address_numeric(start_adr, 1, gdb_stdout);
        printf (" to ");
        print_address_numeric(end_adr, 1, gdb_stdout);
        printf(", proc [%d]\n", curr_pd);
    }
#endif

    /* srikanth, we used to create partial symbols as we iterated over
       the quick procedure lookup table created by pxdb. We don't do 
       this anymore. Instead, we will rely on the linker symbol table to
       locate the symbol, use its address to decide which psymtab would
       house the partial symbol, were one to exist.

       It would be nice if this whole loop could be eliminated. However,
       there is some Fortran specific processing that goes on here that
       could not be removed (sigh.) HP compilers provide _MAIN_ as an 
       alternate name for the (high level) program entry point. So a user 
       could say b _MAIN_ and expect a breakpoint inserted there. This is 
       useful since Fortran programs don't have a unique entry point like
       `main'. Unfortunately, this information is not present in the
       linker symbol table and must be captured here.
    */

  while ((CURR_PROC_START <= end_adr) && (curr_pd < max_procs))
    {

      char *rtn_name;		/* mangled name */
      char *rtn_dem_name;	/* qualified demangled name */
      enum language lang = trans_lang ((enum hp_language) qPD[curr_pd].language);
      int ftn_module_func = 0;

      /* Get the alias name if there is one */
      if (vt_bits[ (long) qPD[curr_pd].sbAlias]) /* not a null string */ 
        {
	  rtn_name = &vt_bits[(long) qPD[curr_pd].sbAlias];
	  rtn_dem_name = &vt_bits[ (long)qPD[curr_pd].sbProc ];
          /* check for Fortran module function in the form of
             module_name$function_name; we want to create partial symbols
             for these functions because the normal search through the
             minimal symbols will not work with these functions */
	  if (lang == language_fortran && strchr (rtn_name, '$') &&
              (strcmp (strchr (rtn_name, '$') + 1, rtn_dem_name)) == 0)
	    ftn_module_func = 1;
	}
      else
	{
	  rtn_name = &vt_bits[(long) qPD[curr_pd].sbProc];
	  rtn_dem_name = NULL;
	}

        /* Hack to get around HP compilers' insistence on providing
         * "_MAIN_" as an alternate name for "main".  
         */
        if (strcmp (rtn_name, "_MAIN_") == 0 || ftn_module_func)
	  {
            /* Set mangled name to "main" and demangled_name to "",
             * except for Fortran, where the mangled name is set to 
             * "_MAIN_" and the demangled name is "" or the program name.
             * We do this for Fortran because "main" is not a special name 
             * and there could be a user subroutine called "main".  If 
             * there is a program name, we want to save it as the user may 
             * reference it to set breakpoints, etc.
             */
          if (!ftn_module_func)
            if (lang == language_fortran) {
                if (strcmp(rtn_dem_name, default_main) == 0)
                    rtn_dem_name = NULL;
                default_main = rtn_name = default_fortran_main_string;

            }
            else {
                rtn_name = default_main;
                rtn_dem_name = NULL;
            }
            add_psymbol_with_dem_name_to_list (
                       rtn_name,
                       strlen ( rtn_name),
                       rtn_dem_name,
                       rtn_dem_name ? strlen (rtn_dem_name) : 0, 
                       VAR_NAMESPACE,
                       LOC_BLOCK, 
                       &objfile->global_psymbols,  
                       (qPD[curr_pd].adrStart +  /* Starting address of rtn */
                        ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile))),
                       0,                         /* core addr?? */
                       lang,
                       objfile);
        }

#ifdef DUMPING
        if (dumping)
	  printf( "..add %s (demangled %s) [%d] to this psymtab\n", rtn_name, rtn_dem_name, curr_pd);
#endif

      /* Check for module-spanning routines. */
      if (CURR_PROC_END > end_adr)
	{
#ifdef DUMPING
	  if (dumping)
	    {
	      printf ("Procedure \"%s\" [%d] spans file or module boundaries.", rtn_name, curr_pd);
	      printf ("CURR_PROC_END = 0x%x, end_adr = 0x%x\n", CURR_PROC_END, end_adr);
	    }
#endif
	  /* Search through the quick file table for multiple entries for 
	   * a given function and adjust the current file entry and the 
	   * the ending address of the function.  ipd is the procedure index 
	   * of the current file entry in the caller.
	   */
	  curr_fd = *curr_fd_p;
	  while (curr_fd <= max_files
		 && qFD[curr_fd].ipd == curr_pd
		 && FILE_END (curr_fd)  <  CURR_PROC_END)
	    ++curr_fd;
	  if (curr_fd > *curr_fd_p) {
	    *curr_fd_p = curr_fd;
	    *end_adr_p = CURR_PROC_END;
	  }
        }

      /* RM: Check that global functions are where we expect them to
         be. If not, that's a good sign that this psymtab has been
         overridden by an incrementally linked module */
      if (CURRENT_CHAPTER (objfile) < N_CHAPTERS (objfile) - 1)
        {
          dn_bufp = hpread_get_lntt (qPD[curr_pd].isym, objfile);
          if (dn_bufp->dfunc.global)
            {
              struct minimal_symbol *m;
              
              m = lookup_minimal_symbol (rtn_name, 0, objfile);
              if (!m ||
                  SYMBOL_VALUE_ADDRESS(m) !=
                  qPD[curr_pd].adrStart +  /* Starting address of rtn */
                  ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile)))
                OVERRIDDEN (pst) = 1;
            }
        }

      symbol_count++;
      *curr_pd_p = ++curr_pd;	/* bump up count & reflect in caller */
    }				/* loop over procedures */

#ifdef DUMPING
  if (dumping)
    {
      if (symbol_count == 0)
	printf ("Scan_procs: no symbols found!\n");
    }
#endif

  return symbol_count;
}


/* Traverse the quick look-up tables, building a set of psymtabs.

   This constructs a psymtab for modules and files in the quick lookup
   tables.

   Mostly, modules correspond to compilation units, so we try to
   create psymtabs that correspond to modules; however, in some cases
   a file can result in a compiled object which does not have a module
   entry for it, so in such cases we create a psymtab for the file.  */

int
hpread_quick_traverse (objfile, gntt_bits, vt_bits, pxdb_header_p)
     struct objfile *objfile;	/* The object file descriptor */
     char *gntt_bits;		/* GNTT entries, loaded in from the file */
     char *vt_bits;		/* VT (string) entries ditto. */
     PXDB_header_ptr pxdb_header_p;	/* Pointer to pxdb header ditto */
{
  struct partial_symtab *pst;

  char *addr;

  quick_procedure_entry *qPD;
  quick_file_entry *qFD;
  quick_module_entry *qMD;
  quick_class_entry *qCD;

  int idx;
  int i;
  CORE_ADDR start_adr;		/* current psymtab's starting code addr   */
  CORE_ADDR end_adr;		/* current psymtab's ending code addr     */
  CORE_ADDR next_mod_adr;	/* next module's starting code addr    */
  int curr_pd;			/* current procedure */
  int curr_fd;			/* current file      */
  int curr_md;			/* current module    */
  int start_sym;		/* current psymtab's starting symbol index */
  int end_sym;			/* current psymtab's ending symbol index   */
  int max_LNTT_sym_index; /* max index of $LNTT$ subspace; */
  /* it's the index of the combined LNTT    */
  /* and GNTT symbols after pxdb has run    */
  int syms_in_pst;
  B_TYPE *class_entered;

  struct partial_symbol **global_syms;	/* We'll be filling in the "global"   */
  struct partial_symbol **static_syms;	/* and "static" tables in the objfile
					   as we go, so we need a pair of     
					   current pointers. */

#ifdef DUMPING
  /* Turn this on for lots of debugging information in this routine.
     You get a blow-by-blow account of quick lookup table reading */
  static int dumping = 0;
#endif

  pst = (struct partial_symtab *) 0;

  /* Clear out some globals */
  init_pst_syms ();
  told_objfile = 0;

  /* Demangling style -- if EDG style already set, don't change it,
     as HP style causes some problems with the KAI EDG compiler */
  if (current_demangling_style != edg_demangling)
    {
      /* Otherwise, ensure that we are using HP style demangling */
      set_demangling_style (HP_DEMANGLING_STYLE_STRING);
    }

  /* First we need to find the starting points of the quick
     look-up tables in the GNTT. */

  addr = gntt_bits;

  qPD = (quick_procedure_entry_ptr) addr;
  addr += pxdb_header_p->pd_entries * sizeof (quick_procedure_entry);

#ifdef DUMPING
  if (dumping)
    {
      printf ("\n Printing routines as we see them\n");
      for (i = 0; VALID_PROC (i); i++)
	{
	  idx = (long) qPD[i].sbProc;
	  printf( "%s ", &vt_bits[idx]);
          print_address_numeric(PROC_START (i), 1, gdb_stdout);
          printf("..");
          print_address_numeric(PROC_END (i), 1, gdb_stdout);
          printf("\n");
	}
    }
#endif

  qFD = (quick_file_entry_ptr) addr;
  addr += pxdb_header_p->fd_entries * sizeof (quick_file_entry);

#ifdef DUMPING
  if (dumping)
    {
      printf ("\n Printing files as we see them\n");
      for (i = 0; VALID_FILE (i); i++)
	{
	  idx = (long) qFD[i].sbFile;
          printf( "%s ", &vt_bits[idx]);
          print_address_numeric(FILE_START(i), 1, gdb_stdout);
          printf("..");
          print_address_numeric(FILE_END(i), 1, gdb_stdout);
          printf("\n");
	}
    }
#endif

  qMD = (quick_module_entry_ptr) addr;
  addr += pxdb_header_p->md_entries * sizeof (quick_module_entry);

#ifdef DUMPING
  if (dumping)
    {
      printf ("\n Printing modules as we see them\n");
      for (i = 0; i < pxdb_header_p->md_entries; i++)
	{
	  idx = (long) qMD[i].sbMod;
	  printf ("%s\n", &vt_bits[idx]);
	}
    }
#endif

  qCD = (quick_class_entry_ptr) addr;
  addr += pxdb_header_p->cd_entries * sizeof (quick_class_entry);

#ifdef DUMPING
  if (dumping)
    {
      printf ("\n Printing classes as we see them\n");
      for (i = 0; VALID_CLASS (i); i++)
	{
	  idx = (long) qCD[i].sbClass;
	  printf ("%s\n", &vt_bits[idx]);
	}

      printf ("\n Done with dump, on to build!\n");
    }
#endif

  /* We need this index only while hp-symtab-read.c expects
     a byte offset to the end of the LNTT entries for a given
     psymtab.  Thus the need for it should go away someday.

     When it goes away, then we won't have any need to load the
     LNTT from the objfile at psymtab-time, and start-up will be
     faster.  To make that work, we'll need some way to create
     a null pst for the "globals" pseudo-module. */
  max_LNTT_sym_index = LNTT_SYMCOUNT (objfile);

  /* Scan the module descriptors and make a psymtab for each.

     We know the MDs, FDs and the PDs are in order by starting
     address.  We use that fact to traverse all three arrays in
     parallel, knowing when the next PD is in a new file
     and we need to create a new psymtab. */
  curr_pd = 0;			/* Current procedure entry */
  curr_fd = 0;			/* Current file entry */
  curr_md = 0;			/* Current module entry */

  start_adr = 0;		/* Current psymtab code range */
  end_adr = 0;

  start_sym = 0;		/* Current psymtab symbol range */
  end_sym = 0;

  syms_in_pst = 0;		/* Symbol count for psymtab */

  /* Psts actually just have pointers into the objfile's
     symbol table, not their own symbol tables. */
  global_syms = objfile->global_psymbols.next;
  static_syms = objfile->static_psymbols.next;


  /* First skip over pseudo-entries with address 0.  These represent inlined
     routines and abstract (uninstantiated) template routines.
     FIXME: These should be read in and available -- even if we can't set
     breakpoints, etc., there's some information that can be presented
     to the user. pai/1997-10-08  */

  while (VALID_CURR_PROC && (CURR_PROC_START == 0))
    curr_pd++;

  /* Loop over files, modules, and procedures in code address order. Each
     time we enter an iteration of this loop, curr_pd points to the first
     unprocessed procedure, curr_fd points to the first unprocessed file, and
     curr_md to the first unprocessed module.  Each iteration of this loop
     updates these as required -- any or all of them may be bumpd up
     each time around.  When we exit this loop, we are done with all files
     and modules in the tables -- there may still be some procedures, however.

     Note: This code used to loop only over module entries, under the assumption
     that files can occur via inclusions and are thus unreliable, while a
     compiled object always corresponds to a module.  With CTTI in the HP aCC
     compiler, it turns out that compiled objects may have only files and no
     modules; so we have to loop over files and modules, creating psymtabs for
     either as appropriate.  Unfortunately there are some problems (notably:
     1. the lack of "SRC_FILE_END" entries in the LNTT, 2. the lack of pointers
     to the ending symbol indices of a module or a file) which make it quite hard
     to do this correctly.  Currently it uses a bunch of heuristics to start and
     end psymtabs; they seem to work well with most objects generated by aCC, but
     who knows when that will change...   */

  while (VALID_CURR_FILE || VALID_CURR_MODULE)
    {

      char *mod_name_string = NULL;
      char *full_name_string;

      /* First check for modules like "version.c", which have no code
         in them but still have qMD entries.  They also have no qFD or
         qPD entries.  Their start address is -1 and their end address
         is 0.  */
      if (VALID_CURR_MODULE && (CURR_MODULE_START == -1) && (CURR_MODULE_END == 0))
	{

	  mod_name_string = &vt_bits[(long) qMD[curr_md].sbMod];

#ifdef DUMPING
	  if (dumping)
	    printf ("Module with data only %s\n", mod_name_string);
#endif

	  /* We'll skip the rest (it makes error-checking easier), and
	     just make an empty pst.  Right now empty psts are not put
	     in the pst chain, so all this is for naught, but later it
	     might help.  */

	  pst = hpread_start_psymtab (objfile,
				      mod_name_string,
				      CURR_MODULE_START,	/* Low text address: bogus! */
		       (CURR_MODULE_ISYM * sizeof (struct dntt_type_block)),
	  /* ldsymoff */
				      global_syms,
				      static_syms);

	  pst = hpread_end_psymtab (pst,
				    NULL,	/* psymtab_include_list */
				    0,	/* includes_used        */
				  end_sym * sizeof (struct dntt_type_block),
	  /* byte index in LNTT of end 
	     = capping symbol offset  
	     = LDSYMOFF of nextfile */
				    0,	/* text high            */
				    NULL,	/* dependency_list      */
				    0);		/* dependencies_used    */

	  global_syms = objfile->global_psymbols.next;
	  static_syms = objfile->static_psymbols.next;

	  curr_md++;
	}
      else if (VALID_CURR_MODULE &&
	       ((CURR_MODULE_START == 0) || (CURR_MODULE_START == -1) ||
		(CURR_MODULE_END == 0) || (CURR_MODULE_END == -1)))
	{
	  TELL_OBJFILE;
          warning_start ("Module \"%s\" [0x%x] has non-standard addresses.  It starts at ",
                        mod_name_string, curr_md);
          print_address_numeric (start_adr, 1, gdb_stderr);
          fprintf_filtered (gdb_stderr, ", ends at ");
          print_address_numeric (end_adr, 1, gdb_stderr);
          fprintf_filtered (gdb_stderr, ", and will be skipped.\n");
	  /* On to next module */
	  curr_md++;
	}
      else
	{
	  /* First check if we are looking at a file with code in it
	     that does not overlap the current module's code range */

	  if (VALID_CURR_FILE ? (VALID_CURR_MODULE ? (CURR_FILE_END < CURR_MODULE_START) : 1) : 0)
	    {

	      /* Looking at file not corresponding to any module,
	         create a psymtab for it */
	      full_name_string = &vt_bits[(long) qFD[curr_fd].sbFile];
	      start_adr = CURR_FILE_START;
	      end_adr = CURR_FILE_END;
	      start_sym = CURR_FILE_ISYM;

	      /* Check if there are any procedures not handled until now, that
	         begin before the start address of this file, and if so, adjust
	         this module's start address to include them.  This handles routines that
	         are in between file or module ranges for some reason (probably
	         indicates a compiler bug */

              if (VALID_CURR_PROC && (CURR_PROC_START < start_adr))
		{
		  TELL_OBJFILE;
		  warning ("Found procedure \"%s\" [0x%x] that is not in any file or module.",
			   &vt_bits[(long) qPD[curr_pd].sbProc], curr_pd);
		  start_adr = CURR_PROC_START;
		  if (CURR_PROC_ISYM < start_sym)
		    start_sym = CURR_PROC_ISYM;
		}

	      /* Sometimes (compiler bug -- COBOL) the module end address is higher
	         than the start address of the next module, so check for that and
	         adjust accordingly */

              if (VALID_FILE (curr_fd + 1) && (FILE_START (curr_fd + 1) < end_adr))
		{
		  TELL_OBJFILE;
                  warning("File \"%s\" [%d] has ending address after starting address of next file; adjusting ending address down.",
			  full_name_string, curr_fd);
		  end_adr = FILE_START (curr_fd + 1) - 1;	/* Is -4 (or -8 for 64-bit) better? */
		}
	      if (VALID_MODULE (curr_md) && (CURR_MODULE_START <= end_adr))
		{
		  TELL_OBJFILE;
                  warning("File \"%s\" [%d] has ending address after starting address of next module; adjusting ending address down.",
			  full_name_string, curr_fd);
		  end_adr = CURR_MODULE_START - 1;	/* Is -4 (or -8 for 64-bit) better? */
		}


#ifdef DUMPING
	      if (dumping)
		{
                  printf( "Make new psymtab for file %s [%d] (", full_name_string, curr_fd);
                  print_address_numeric(start_adr, 1, gdb_stdout);
                  printf(" to ");
                  print_address_numeric(end_adr, 1, gdb_stdout);
                  printf(").\n");
		}
#endif
	      /* Create the basic psymtab, connecting it in the list
	         for this objfile and pointing its symbol entries
	         to the current end of the symbol areas in the objfile.

	         The "ldsymoff" parameter is the byte offset in the LNTT
	         of the first symbol in this file.  Some day we should
	         turn this into an index (fix in hp-symtab-read.c as well).
	         And it's not even the right byte offset, as we're using
	         the size of a union! FIXME!  */
	      pst = hpread_start_psymtab (objfile,
					  full_name_string,
					  start_adr,	/* Low text address */
			      (start_sym * sizeof (struct dntt_type_block)),
	      /* ldsymoff */
					  global_syms,
					  static_syms);

	      /* Set up to only enter each class referenced in this module once.  */
	      class_entered = malloc (B_BYTES (pxdb_header_p->cd_entries));
	      B_CLRALL (class_entered, pxdb_header_p->cd_entries);

	      /* Scan the procedure descriptors for procedures in the current
	         file, based on the starting addresses. */

	      syms_in_pst = scan_procs (&curr_pd, qPD,
					pxdb_header_p->pd_entries,
					start_adr, &end_adr,
					pst, vt_bits, objfile,
                                        &curr_fd, qFD,
					pxdb_header_p->fd_entries);

	      /* Get ending symbol offset */

	      end_sym = 0;
	      /* First check for starting index before previous psymtab */
	      if (pst_syms_count && start_sym < pst_syms_array[pst_syms_count - 1].end)
		{
		  end_sym = find_next_pst_start (start_sym);
		}
	      /* Look for next start index of a file or module, or procedure */
	      if (!end_sym)
		{
		  int next_file_isym = find_next_file_isym (start_sym, qFD, curr_fd + 1, pxdb_header_p);
		  int next_module_isym = find_next_module_isym (start_sym, qMD, curr_md, pxdb_header_p);
		  int next_proc_isym = find_next_proc_isym (start_sym, qPD, curr_pd, pxdb_header_p);

		  if (next_file_isym && next_module_isym)
		    {
		      /* pick lower of next file or module start index */
		      end_sym = min (next_file_isym, next_module_isym);
		    }
		  else
		    {
		      /* one of them is zero, pick the other */
		      end_sym = max (next_file_isym, next_module_isym);
		    }

		  /* As a precaution, check next procedure index too */
		  if (!end_sym)
		    end_sym = next_proc_isym;
		  else
		    end_sym = min (end_sym, next_proc_isym);
		}

	      /* Couldn't find procedure, file, or module, use globals as default */
	      if (!end_sym)
		/* if there are no globals, use max_LNTT_sym_index */
		end_sym = max_LNTT_sym_index - 1 == pxdb_header_p->globals ?
			  max_LNTT_sym_index : pxdb_header_p->globals;

#ifdef DUMPING
              if (dumping)
	        {
                  printf ("File psymtab indices: 0x%x to 0x%x\n",
			  start_sym, end_sym);
		}
#endif

	      pst = hpread_end_psymtab (pst,
					NULL,	/* psymtab_include_list */
					0,	/* includes_used        */
				  end_sym * sizeof (struct dntt_type_block),
	      /* byte index in LNTT of end 
	         = capping symbol offset   
	         = LDSYMOFF of nextfile */
					end_adr,	/* text high */
					NULL,	/* dependency_list */
					0);	/* dependencies_used */

	      record_pst_syms (start_sym, end_sym);

#ifdef DUMPING
              if (dumping)
	        {
                  printf( "Made new psymtab for file %s [%d] (", full_name_string, curr_fd);
                  print_address_numeric(start_adr, 1, gdb_stdout);
                  printf(" to ");
                  print_address_numeric(end_adr, 1, gdb_stdout);
                  printf("), sym 0x%x to 0x%x.\n", CURR_FILE_ISYM, end_sym);
		}
#endif
	      /* Prepare for the next psymtab. */
	      global_syms = objfile->global_psymbols.next;
	      static_syms = objfile->static_psymbols.next;
	      free (class_entered);

	      curr_fd++;
	    }			/* Psymtab for file */
	  else
	    {
	      /* We have a module for which we create a psymtab */

	      mod_name_string = &vt_bits[(long) qMD[curr_md].sbMod];

	      /* We will include the code ranges of any files that happen to
	         overlap with this module */

	      /* So, first pick the lower of the file's and module's start addresses */
	      start_adr = CURR_MODULE_START;
	      if (VALID_CURR_FILE)
		{
		  if (CURR_FILE_START < CURR_MODULE_START)
		    {
		      TELL_OBJFILE;
		      warning ("File \"%s\" [0x%x] crosses beginning of module \"%s\".",
			       &vt_bits[(long) qFD[curr_fd].sbFile],
			       curr_fd, mod_name_string);

		      start_adr = CURR_FILE_START;
		    }
		}

	      /* Also pick the lower of the file's and the module's start symbol indices */
	      start_sym = CURR_MODULE_ISYM;
	      if (VALID_CURR_FILE && (CURR_FILE_ISYM < CURR_MODULE_ISYM))
		start_sym = CURR_FILE_ISYM;

	      /* For the end address, we scan through the files till we find one
	         that overlaps the current module but ends beyond it; if no such file exists we
	         simply use the module's start address.  
	         (Note, if file entries themselves overlap
	         we take the longest overlapping extension beyond the end of the module...)
	         We assume that modules never overlap. */

	      end_adr = CURR_MODULE_END;

	      if (VALID_CURR_FILE)
		{
		  while (VALID_CURR_FILE && (CURR_FILE_START < end_adr))
		    {

#ifdef DUMPING
		      if (dumping)
			printf ("Maybe skipping file %s which overlaps with module %s\n",
				&vt_bits[(long) qFD[curr_fd].sbFile], mod_name_string);
#endif
                      /* The following warning is not valid for f90 programs
			 where each function is enclosed in a module;
			 consequently there may be several modules within
			 a file */
                      if (CURR_FILE_END > end_adr &&
			  qMD[ curr_md].language != HP_LANGUAGE_FORTRAN)
			{
			  TELL_OBJFILE;
			  warning ("File \"%s\" [0x%x] crosses end of module \"%s\".",
				   &vt_bits[(long) qFD[curr_fd].sbFile],
				   curr_fd, mod_name_string);
			  end_adr = CURR_FILE_END;
			}
		      curr_fd++;
		    }
		  curr_fd--;	/* back up after going too far */
		}

	      /* Sometimes (compiler bug -- COBOL) the module end address is higher
	         than the start address of the next module, so check for that and
	         adjust accordingly */

	      if (VALID_MODULE (curr_md + 1) && (MODULE_START (curr_md + 1) <= end_adr))
		{
		  TELL_OBJFILE;
		  warning ("Module \"%s\" [0x%x] has ending address after starting address of next module; adjusting ending address down.",
			   mod_name_string, curr_md);
		  end_adr = MODULE_START (curr_md + 1) - 1;	/* Is -4 (or -8 for 64-bit) better? */
		}
              if (VALID_FILE (curr_fd + 1) &&
		  (FILE_START (curr_fd + 1) < end_adr))
		{
		  TELL_OBJFILE;
		  warning ("Module \"%s\" [0x%x] has ending address after starting address of next file; adjusting ending address down.",
			   mod_name_string, curr_md);
		  end_adr = FILE_START (curr_fd + 1) - 1;	/* Is -4 (or -8 for 64-bit) better? */
		}

	      /* Use one file to get the full name for the module.  This
	         situation can arise if there is executable code in a #include
	         file.  Each file with code in it gets a qFD.  Files which don't
	         contribute code don't get a qFD, even if they include files
	         which do, e.g.: 

	         body.c:                    rtn.h:
	         int x;                     int main() {
	         #include "rtn.h"               return x;
	         }

	         There will a qFD for "rtn.h",and a qMD for "body.c",
	         but no qMD for "rtn.h" or qFD for "body.c"!

	         We pick the name of the last file to overlap with this
	         module.  C convention is to put include files first.  In a
	         perfect world, we could check names and use the file whose full
	         path name ends with the module name. */

	      if (VALID_CURR_FILE)
		full_name_string = &vt_bits[(long) qFD[curr_fd].sbFile];
	      else
		full_name_string = mod_name_string;

	      /* Check if there are any procedures not handled until now, that
	         begin before the start address we have now, and if so, adjust
	         this psymtab's start address to include them.  This handles routines that
	         are in between file or module ranges for some reason (probably
	         indicates a compiler bug */

              if (VALID_CURR_PROC && (CURR_PROC_START < start_adr))
	        {
                  TELL_OBJFILE;
                  warning ("Found procedure \"%s\" [%d] that is not in any file or module.",
			   &vt_bits[(long) qPD[curr_pd].sbProc], curr_pd);
		  start_adr = CURR_PROC_START;
		  if (CURR_PROC_ISYM < start_sym)
		    start_sym = CURR_PROC_ISYM;
		}

#ifdef DUMPING
	      if (dumping)
		{
                  printf( "Make new psymtab for module %s (", mod_name_string);
                  print_address_numeric(start_adr, 1, gdb_stdout);
                  printf(" to ");
                  print_address_numeric(end_adr, 1, gdb_stdout);
                  printf("), using file %s\n", full_name_string);
		}
#endif
	      /* Create the basic psymtab, connecting it in the list
	         for this objfile and pointing its symbol entries
	         to the current end of the symbol areas in the objfile.

	         The "ldsymoff" parameter is the byte offset in the LNTT
	         of the first symbol in this file.  Some day we should
	         turn this into an index (fix in hp-symtab-read.c as well).
	         And it's not even the right byte offset, as we're using
	         the size of a union! FIXME!  */
	      pst = hpread_start_psymtab (objfile,
					  full_name_string,
					  start_adr,	/* Low text address */
			      (start_sym * sizeof (struct dntt_type_block)),
	      /* ldsymoff */
					  global_syms,
					  static_syms);

	      /* Set up to only enter each class referenced in this module once.  */
	      class_entered = malloc (B_BYTES (pxdb_header_p->cd_entries));
	      B_CLRALL (class_entered, pxdb_header_p->cd_entries);

	      /* Scan the procedure descriptors for procedures in the current
	         module, based on the starting addresses. */

	      syms_in_pst = scan_procs (&curr_pd, qPD,
					pxdb_header_p->pd_entries,
					start_adr, &end_adr,
					pst, vt_bits, objfile,
                                        &curr_fd, qFD,
					pxdb_header_p->fd_entries);

	      /* Get ending symbol offset */

	      end_sym = 0;
	      /* First check for starting index before previous psymtab */
	      /* RM: aCC/pxdb seems to incorrectly emit debug info that
		 includes LNTT entries from files outside the current modules
		 address ranges within the current modules LNTT entries.
		 Disabling the check below works around this, but causes
		 certain CTTI functionality to break. Take your pick. */
#if 1
              if (pst_syms_count &&
		  start_sym < pst_syms_array[pst_syms_count - 1].end)
	        {
                  end_sym = find_next_pst_start (start_sym);
		}
#endif

	      /* Look for next start index of a file or module, or procedure */
	      if (!end_sym)
		{
		  int next_file_isym = find_next_file_isym (start_sym, qFD, curr_fd + 1, pxdb_header_p);
		  int next_module_isym = find_next_module_isym (start_sym, qMD, curr_md + 1, pxdb_header_p);
		  int next_proc_isym = find_next_proc_isym (start_sym, qPD, curr_pd, pxdb_header_p);

		  if (next_file_isym && next_module_isym)
		    {
		      /* pick lower of next file or module start index */
		      end_sym = min (next_file_isym, next_module_isym);
		    }
		  else
		    {
		      /* one of them is zero, pick the other */
		      end_sym = max (next_file_isym, next_module_isym);
		    }

		  /* As a precaution, check next procedure index too */
		  if (!end_sym)
		    end_sym = next_proc_isym;
		  else
		    end_sym = min (end_sym, next_proc_isym);
		}

	      /* Couldn't find procedure, file, or module, use globals as default */
	      if (!end_sym)
		end_sym = pxdb_header_p->globals;

#ifdef DUMPING
	      if (dumping)
		{
                  printf ("Module psymtab indices: 0x%x to 0x%x\n",
			  start_sym, end_sym);
		}
#endif

	      pst = hpread_end_psymtab (pst,
					NULL,	/* psymtab_include_list */
					0,	/* includes_used        */
				  end_sym * sizeof (struct dntt_type_block),
	      /* byte index in LNTT of end 
	         = capping symbol offset   
	         = LDSYMOFF of nextfile */
					end_adr,	/* text high */
					NULL,	/* dependency_list      */
					0);	/* dependencies_used    */

	      record_pst_syms (start_sym, end_sym);

#ifdef DUMPING
              if (dumping)
	        {
                  printf( "Made new psymtab for module %s (", mod_name_string);
                  print_address_numeric(start_adr, 1, gdb_stdout);
                  printf(" to ");
                  print_address_numeric(end_adr, 1, gdb_stdout);
                  printf("), sym %x to %x.\n", CURR_MODULE_ISYM, end_sym);
		}
#endif

	      /* Prepare for the next psymtab. */
	      global_syms = objfile->global_psymbols.next;
	      static_syms = objfile->static_psymbols.next;
	      free (class_entered);

	      curr_md++;
	      curr_fd++;
	    }			/* psymtab for module */
	}			/* psymtab for non-bogus file or module */
    }				/* End of while loop over all files & modules */

  /* There may be some routines after all files and modules -- these will get
     inserted in a separate new module of their own */
  if (VALID_CURR_PROC)
    {
      start_adr = CURR_PROC_START;
      end_adr = qPD[pxdb_header_p->pd_entries - 1].adrEnd;
      TELL_OBJFILE;
      warning ("Found functions beyond end of all files and modules [%d].",
	       curr_pd);
#ifdef DUMPING
      if (dumping)
	{
          printf( "Orphan functions at end, PD %d and beyond (", curr_pd);
          print_address_numeric(start_adr, 1, gdb_stdout);
          printf(" to ");
          print_address_numeric(end_adr, 1, gdb_stdout);
          printf(")\n");
	}
#endif
      pst = hpread_start_psymtab (objfile,
				  "orphans",
				  start_adr,	/* Low text address */
			 (CURR_PROC_ISYM * sizeof (struct dntt_type_block)),
      /* ldsymoff */
				  global_syms,
				  static_syms);

      scan_procs (&curr_pd, qPD, pxdb_header_p->pd_entries,
                  start_adr, &end_adr,
                  pst, vt_bits, objfile,
                  &curr_fd, qFD, pxdb_header_p->fd_entries);

      pst = hpread_end_psymtab (pst,
				NULL,	/* psymtab_include_list */
				0,	/* includes_used */
		   pxdb_header_p->globals * sizeof (struct dntt_type_block),
      /* byte index in LNTT of end 
         = capping symbol offset   
         = LDSYMOFF of nextfile */
				end_adr,	/* text high  */
				NULL,	/* dependency_list */
				0);	/* dependencies_used */
    }


#ifdef NEVER_NEVER
  /* Now build psts for non-module things (in the tail of
     the LNTT, after the last END MODULE entry).

     If null psts were kept on the chain, this would be
     a solution.  FIXME */
  pst = hpread_start_psymtab (objfile,
			      "globals",
			      0,
			      (pxdb_header_p->globals
			       * sizeof (struct dntt_type_block)),
			      objfile->global_psymbols.next,
			      objfile->static_psymbols.next);
  hpread_end_psymtab (pst,
		      NULL, 0,
		      (max_LNTT_sym_index * sizeof (struct dntt_type_block)),
		      0,
		      NULL, 0);
#endif

  clear_pst_syms ();

  return 1;

}				/* End of hpread_quick_traverse. */


/* Get appropriate header, based on pxdb type. 
   Return value: 1 if ok, 0 if not */
int
hpread_get_header (objfile, pxdb_header_p)
     struct objfile *objfile;
     PXDB_header_ptr pxdb_header_p;
{
  asection *pinfo_section, *debug_section, *header_section;

#ifdef DUMPING
  /* Turn on for debugging information */
  static int dumping = 0;
#endif

  header_section = bfd_get_section_by_name (objfile->obfd, HP_HEADER);
  if (!header_section)
    {
      /* We don't have either PINFO or DEBUG sections.  But
         stuff like "libc.sl" has no debug info.  There's no
         need to warn the user of this, as it may be ok. The
         caller will figure it out and issue any needed
         messages. */
#ifdef DUMPING
      if (dumping)
	printf ("==No debug info at all for %s.\n", objfile->name);
#endif

      return 0;
    }

  /* We would like either a $DEBUG$ or $PINFO$ section.
     Once we know which, we can understand the header
     data (which we have defined to suit the more common
     $DEBUG$ case). */
  debug_section = bfd_get_section_by_name (objfile->obfd, "$DEBUG$");
  pinfo_section = bfd_get_section_by_name (objfile->obfd, "$PINFO$");

#ifdef GDB_TARGET_IS_HPPA_20W
  if (header_section)
    {
#else
  if (debug_section)
    {
#endif
      /* The expected case: normal pxdb header. */
      bfd_get_section_contents (objfile->obfd, header_section,
				pxdb_header_p, 0, sizeof (PXDB_header));

      if (!pxdb_header_p->pxdbed)
	{
	  /* This shouldn't happen if we check in "symfile.c". */
	  return 0;
	}			/* DEBUG section */
    }

  else if (pinfo_section)
    {
      /* The DOC case; we need to translate this into a
         regular header. */
      DOC_info_PXDB_header doc_header;

#ifdef DUMPING
      if (dumping)
	{
	  printf ("==OOps, PINFO, let's try to handle this, %s.\n", objfile->name);
	}
#endif

      bfd_get_section_contents (objfile->obfd,
				header_section,
				&doc_header, 0,
				sizeof (DOC_info_PXDB_header));

      if (!doc_header.pxdbed)
	{
	  /* This shouldn't happen if we check in "symfile.c". */
	  warning ("File \"%s\" not processed by pxdb!", objfile->name);
	  return 0;
	}

      /* Copy relevent fields to standard header passed in. */
      pxdb_header_p->pd_entries = doc_header.pd_entries;
      pxdb_header_p->fd_entries = doc_header.fd_entries;
      pxdb_header_p->md_entries = doc_header.md_entries;
      pxdb_header_p->pxdbed = doc_header.pxdbed;
      pxdb_header_p->bighdr = doc_header.bighdr;
      pxdb_header_p->sa_header = doc_header.sa_header;
      pxdb_header_p->inlined = doc_header.inlined;
      pxdb_header_p->globals = doc_header.globals;
      pxdb_header_p->time = doc_header.time;
      pxdb_header_p->pg_entries = doc_header.pg_entries;
      pxdb_header_p->functions = doc_header.functions;
      pxdb_header_p->files = doc_header.files;
      pxdb_header_p->cd_entries = doc_header.cd_entries;
      pxdb_header_p->aa_entries = doc_header.aa_entries;
      pxdb_header_p->oi_entries = doc_header.oi_entries;
      pxdb_header_p->version = doc_header.version;
    }				/* PINFO section */

  else
    {
#ifdef DUMPING
      if (dumping)
	printf ("==No debug info at all for %s.\n", objfile->name);
#endif

      return 0;

    }

  return 1;
}				/* End of hpread_get_header */
#endif /* QUICK_LOOK_UP */

/* Given a string say whether it points to VT */
static boolean string_from_vt (objfile, string)
    struct objfile * objfile;
    char * string; 
{
    /* don't touch with a barge pole if it ain't kosher */
    if (!string || !objfile) 
      return 0;

    if ((string >= VT (objfile)) && 
             (string <= (VT (objfile) + VT_SIZE (objfile))))
      return 1;

    return 0;
}


/* Initialization for reading native HP C debug symbols from OBJFILE.

   Its only purpose in life is to set up the symbol reader's private
   per-objfile data structures, and read in the raw contents of the debug
   sections (attaching pointers to the debug info into the private data
   structures).

   Since BFD doesn't know how to read debug symbols in a format-independent
   way (and may never do so...), we have to do it ourselves.  Note we may
   be called on a file without native HP C debugging symbols.

   FIXME, there should be a cleaner peephole into the BFD environment
   here. */
void
hpread_symfile_init (objfile)
     struct objfile *objfile;
{
  asection *vt_section, *slt_section, *lntt_section, *gntt_section;
#ifdef HPPA_DOC
  asection *rt_section;
#endif /* HPPA_DOC */
  bfd_size_type lntt_size, gntt_size, slt_size;
  int i, n;
  asection *lines_section;
  PXDB_header pxdb_header;

  extern boolean (*permanent_copy_exists) (struct objfile * objfile,
					   char * string);

  permanent_copy_exists = string_from_vt;

  /* Allocate struct to keep track of the symfile */
  objfile->sym_private = (PTR)
    xmmalloc (objfile->md, sizeof (struct hpread_symfile_info));
  memset (objfile->sym_private, 0, sizeof (struct hpread_symfile_info));

  /* RM: Count number of debug chapters */
  N_CHAPTERS (objfile) = 0;
  while (bfd_get_section_by_name_and_id (objfile->obfd,
					 HP_HEADER, N_CHAPTERS (objfile)))
    N_CHAPTERS (objfile)++;

  /* RM: allocate at least one of each thing below, for use by DOOM */
  n = (N_CHAPTERS (objfile)) ? (N_CHAPTERS (objfile)) : 1;
  
  /* JYG: disable incremental expansion if in ild mode */
  if (N_CHAPTERS (objfile) > 1)
    incremental_expansion = 0;

  /* RM: Initialize all the chapters */
  HPUX_SYMFILE_INFO (objfile)->gntt =
    (char **) obstack_alloc (&objfile->symbol_obstack,
			     n * sizeof(char *));
  HPUX_SYMFILE_INFO (objfile)->lntt =
    (char **) obstack_alloc (&objfile->symbol_obstack,
			     n * sizeof(char *));
  HPUX_SYMFILE_INFO (objfile)->slt =
    (char **) obstack_alloc (&objfile->symbol_obstack,
			     n * sizeof(char *));
  HPUX_SYMFILE_INFO (objfile)->vt =
    (char **) obstack_alloc (&objfile->symbol_obstack,
			     n * sizeof(char *));
#ifdef HPPA_DOC
  HPUX_SYMFILE_INFO (objfile)->rt =
    (char **) obstack_alloc (&objfile->symbol_obstack,
			     n * sizeof(char *));
  HPUX_SYMFILE_INFO (objfile)->rt_size =
    (unsigned int *) obstack_alloc (&objfile->symbol_obstack,
				    n * sizeof(unsigned int));
#endif /* HPPA_DOC */

  HPUX_SYMFILE_INFO (objfile)->lines =
    (char **) obstack_alloc (&objfile->symbol_obstack,
                             n * sizeof(char *));
  HPUX_SYMFILE_INFO (objfile)->globals_start =
    (unsigned int *) obstack_alloc (&objfile->symbol_obstack,
                                    n * sizeof(unsigned int));

  HPUX_SYMFILE_INFO (objfile)->vt_size =
    (unsigned int *) obstack_alloc (&objfile->symbol_obstack,
				    n * sizeof(unsigned int));
  HPUX_SYMFILE_INFO (objfile)->lines_size =
    (unsigned int *) obstack_alloc (&objfile->symbol_obstack,
                                    n * sizeof(unsigned int));

  HPUX_SYMFILE_INFO (objfile)->lntt_symcount =
    (unsigned int *) obstack_alloc (&objfile->symbol_obstack,
				    n * sizeof(unsigned int));
  HPUX_SYMFILE_INFO (objfile)->gntt_symcount =
    (unsigned int *) obstack_alloc (&objfile->symbol_obstack,
				    n * sizeof(unsigned int));

  HPUX_SYMFILE_INFO (objfile)->type_vector =
    (struct type ***) obstack_alloc (&objfile->symbol_obstack,
				     n * sizeof(struct type **));
  HPUX_SYMFILE_INFO (objfile)->type_vector_length =
    (int *) obstack_alloc (&objfile->symbol_obstack,
			   n * sizeof(int));

  HPUX_SYMFILE_INFO (objfile)->sl_index =
    (sltpointer *) obstack_alloc (&objfile->symbol_obstack,
				  n * sizeof(sltpointer));
  HPUX_SYMFILE_INFO (objfile)->slt_size =
    (int *) obstack_alloc (&objfile->symbol_obstack,
                           n * sizeof(int));

  for (i = 0; i < N_CHAPTERS (objfile); i++)
    {
      CURRENT_CHAPTER (objfile) = i;
      
      /* We haven't read in any types yet.  */
      TYPE_VECTOR (objfile) = 0;
      TYPE_VECTOR_LENGTH (objfile) = 0;

      /* RM: We'll read in the DOOM specific stuff later */
      /* Read in data from the $GNTT$ subspace.  */
      if (! (gntt_section = bfd_get_section_by_name_and_id (objfile->obfd,
							    HP_GNTT, i)))
        return;
      
      if (! (lntt_section = bfd_get_section_by_name_and_id (objfile->obfd,
							    HP_LNTT, i)))
        return;

      /* JYG: for ild mode, we disable SLT demand paging as well */
      if ( (N_CHAPTERS (objfile) > 1) &&
           ! (slt_section = bfd_get_section_by_name_and_id (objfile->obfd,
                                                            HP_SLT, i)))
        return;

      if (i == 0)
        {
	  if (N_CHAPTERS (objfile) <= 1)
	    {
	      /* non ild mode, i == 0 */

	      /* srikanth, 000122, allocate the fake GNTT (pxdb quick lookup tables)
		 directly from the heap, as it not used once hpread_quick_traverse
		 finishes.
	      */
	      GNTT (objfile) = xmalloc (bfd_section_size (objfile->obfd, gntt_section));

	      bfd_get_section_contents (objfile->obfd, gntt_section, GNTT (objfile),
					0, bfd_section_size (objfile->obfd, gntt_section));

	      GNTT_SYMCOUNT (objfile)
		= bfd_section_size (objfile->obfd, gntt_section)
		/ sizeof (struct dntt_type_block);

	      /* srikanth, 000121, don't read in the lntt right now. It is quite
		 likely the user may never set a breakpoint in this load module.
		 If the lntt is needed we will page it in on demand. However, we
		 will continue to preload the (real) GNTT portion, as it would be
		 scanned in its entirety right at startup (by hpread_build_psymtabs()).
	      */

	      LNTT (objfile) = 0;
	      LNTT_SYMCOUNT (objfile)
		= bfd_section_size (objfile->obfd, lntt_section)
		/ sizeof (struct dntt_type_block);

	      hpread_get_header (objfile, &pxdb_header);
	      REAL_GNTT_START (objfile) = pxdb_header.globals;
	      REAL_GNTT_SYMCOUNT (objfile)
		= LNTT_SYMCOUNT (objfile) - pxdb_header.globals;
	      REAL_GNTT (objfile)
		= obstack_alloc (&objfile->symbol_obstack,
				 (REAL_GNTT_SYMCOUNT (objfile) * 
				  sizeof (struct dntt_type_block)));
	      bfd_get_section_contents (objfile->obfd, lntt_section,
					REAL_GNTT (objfile),
					pxdb_header.globals *
					sizeof (struct dntt_type_block),
					REAL_GNTT_SYMCOUNT (objfile) *
					sizeof (struct dntt_type_block));

	      GLOBALS_START(objfile) = 0;
      
	      /* srikanth, 990421, don't read in the slt right now. It is quite likely the 
		 user may never set a breakpoint in this load module. If the slt is needed
		 we will page it in on demand. 
	      */

	      SLT (objfile) = 0;
	    }
	  else
	    {
	      /* ild mode, i == 0 */
	      /* no demand paging for LNTT or SLT */

              gntt_size = bfd_section_size (objfile->obfd, gntt_section);
              lntt_size = bfd_section_size (objfile->obfd, lntt_section);
              slt_size = bfd_section_size (objfile->obfd, slt_section);

	      GNTT (objfile) = obstack_alloc (&objfile->symbol_obstack,
					      gntt_size);
	      bfd_get_section_contents (objfile->obfd, gntt_section,
					GNTT(objfile), 0, gntt_size);
	      GNTT_SYMCOUNT (objfile) = gntt_size /
					sizeof (struct dntt_type_block);
          
	      /* Read in data from the $LNTT$ subspace.  Also keep track
	       * of the number of LNTT symbols.
	       *
	       * FIXME: this could be moved into the psymtab-to-symtab
	       *        expansion code, and save startup time.  At the moment this
	       *        data is still used, though.  We'd need a way to tell
	       *        hp-symtab-read.c whether or not to load the LNTT.
	       */

	      LNTT (objfile) = obstack_alloc (&objfile->symbol_obstack,
					      lntt_size);
	      bfd_get_section_contents (objfile->obfd, lntt_section,
					LNTT(objfile), 0, lntt_size);
	      LNTT_SYMCOUNT (objfile) = lntt_size /
					sizeof (struct dntt_type_block);

	      GLOBALS_START (objfile) = 0;

              SLT (objfile) = obstack_alloc (&objfile->symbol_obstack,
					     slt_size);
              bfd_get_section_contents (objfile->obfd, slt_section,
                                        SLT (objfile), 0, slt_size);
              SL_INDEX (objfile) = 0;
              SLT_SIZE (objfile) = slt_size;
	    }
        }
      else
        {
	  /* ild mode, i != 0 */

          lntt_size = bfd_section_size (abfd, lntt_section);
          gntt_size = bfd_section_size (abfd, gntt_section);
          slt_size = bfd_section_size (abfd, slt_section);

          LNTT(objfile) = obstack_alloc (&objfile->symbol_obstack,
					 lntt_size + gntt_size);
          bfd_get_section_contents (objfile->obfd, lntt_section,
				    LNTT(objfile), 0, lntt_size);
          LNTT_SYMCOUNT (objfile) = (lntt_size + gntt_size) /
				    sizeof (struct dntt_type_block);

          GNTT(objfile) = 0;
          bfd_get_section_contents (objfile->obfd, gntt_section,
                                    LNTT(objfile) + lntt_size,
                                    0, gntt_size);
          GNTT_SYMCOUNT (objfile) = 0;

          GLOBALS_START(objfile) = lntt_size /
				   sizeof (struct dntt_type_block);

          SLT(objfile) = obstack_alloc (&objfile->symbol_obstack,
                                        slt_size);
          bfd_get_section_contents (objfile->obfd, slt_section, SLT(objfile),
                                    0, slt_size);
          SL_INDEX (objfile) = 0;

          /* Adjust the SLT -- pxdb has not been run, so the SLT still
           * contains offsets from functions instead of actual addresses
           */
#ifndef BFD64
	  {
	    bfd_size_type rel_slt_size;
	    
	    rel_slt_size = relocated_section_size (objfile->obfd, slt_section);
	    
	    hpread_adjust_slt_for_doom (objfile, rel_slt_size);
	  }
#else
          /* For PA64, this shouldn't be done -- pxdb leaves SLT entries as
	     offsets, so the non-DOOM code handles this anyway.
	     JYG: record SLT_SIZE for ild mode */
	  SLT_SIZE (objfile) = slt_size;
#endif
        }

      /* Read in data from the $VT$ subspace.  $VT$ contains things like
	 names and constants.  Keep track of the number of symbols in the VT.  */
      if (! (vt_section = bfd_get_section_by_name_and_id (objfile->obfd,
							  HP_VT, i)))
	return;

      VT_SIZE (objfile) = bfd_section_size (objfile->obfd, vt_section);

      VT (objfile) =
	(char *) obstack_alloc (&objfile->symbol_obstack,
				VT_SIZE (objfile));

      bfd_get_section_contents (objfile->obfd, vt_section, VT (objfile),
				0, VT_SIZE (objfile));

#ifdef HPPA_DOC
      rt_section = bfd_get_section_by_name_and_id (objfile->obfd, HP_RT, i);

      /* RM: ??? FIXME: support this!!! */
      if (rt_section && i > 0)
        warning("Incremental linking and DOC cannot be combined for now");

      if (rt_section)
        {
          RT_SIZE(objfile) = bfd_section_size (objfile->obfd, rt_section);
      
          RT (objfile)
            = obstack_alloc (&objfile->symbol_obstack, RT_SIZE(objfile));
          
          bfd_get_section_contents (objfile->obfd, rt_section, RT (objfile),
                                    0, RT_SIZE(objfile));
        }
      else
        {
          RT_SIZE(objfile) = 0;
          RT (objfile) = 0;
        }
#endif /* HPPA_DOC */

      /* Read in data from the LINES subspace. */
      lines_section = bfd_get_section_by_name_and_id (objfile->obfd, HP_LINES, i);
      if (lines_section)
        {
	  LINES_SIZE (objfile) = bfd_section_size (objfile->obfd, lines_section);
	  LINES (objfile) =
	    obstack_alloc (&objfile->symbol_obstack, LINES_SIZE (objfile));
	  bfd_get_section_contents (objfile->obfd,
				    lines_section,
				    LINES (objfile),
				    0,
				    LINES_SIZE (objfile));
	}
      else
        {
	  LINES_SIZE (objfile) = 0;
	  LINES (objfile) = NULL;
	}
    }

  CURRENT_CHAPTER(objfile) = 0;
}


/* given an address or an offset in a known space, find enclosing entry
 * in the subspace map. Return the index of entry found.
 * Return -1 if not found.
 */
int hpread_find_index(objfile, address, space)
     struct objfile         *objfile;
     CORE_ADDR              address;
     enum spaces            space;
{
    int start = 0, end = 0, middle;

    switch (space) {
        case textspace:
            start = SUBSPACEMAP(objfile)->textstart;
            end = SUBSPACEMAP(objfile)->textend;
            break;

        case dataspace:
            start = SUBSPACEMAP(objfile)->datastart;
            end = SUBSPACEMAP(objfile)->dataend;
            break;

        case debugspace:
            start = SUBSPACEMAP(objfile)->debugstart;
            end = SUBSPACEMAP(objfile)->debugend;
            break;

        default:
            error("Request to search for address in unknown space\n");
            break;
    }

    while (start <= end)
        {
            middle = (start+end)/2;
            if ((SUBSPACEMAP(objfile)->sr[middle].start <= address) &&
                (SUBSPACEMAP(objfile)->sr[middle].end > address))
                return middle;
            if (SUBSPACEMAP(objfile)->sr[middle].start <= address)
                start = middle+1;
            else if (SUBSPACEMAP(objfile)->sr[middle].start > address)
                end = middle-1;
        }

    /* If we didn't find a data symbol in dataspace, look it up in
     * textspace. This is needed for literals that are put into the
     * text section
     */
    if (space == dataspace)
        return hpread_find_index(objfile, address, textspace);
    else
        return -1;
}


int hpread_find_objid(objfile, address, space)
     struct objfile         *objfile;
     CORE_ADDR              address;
     enum spaces            space;
{
    int val;

    val = hpread_find_index(objfile, address, space);
    if (val == -1)
        return -1;
    else
        return SUBSPACEMAP(objfile)->sr[val].objid;
}

int hpread_find_subspace_map_info(objfile, address, space)
     struct objfile         *objfile;
     CORE_ADDR              address;
     enum spaces            space;
{
    int val;
    int psymtab_id;

    val = hpread_find_index(objfile, address, space);
    if (val == -1)
        return -1;
    else {
        psymtab_id = SUBSPACEMAP(objfile)->sr[val].info;
        return psymtab_id;
    }
}

/* temporary place to hold symbols before transfering them to psymtabs */
struct partial_symbol_map_entry
{
    int objid;
    int is_default;
    int has_non_doom_debug;
    CORE_ADDR textlow;
    CORE_ADDR texthigh;
    struct psymbol_allocation_list globals;
    struct psymbol_allocation_list statics;
};

void hpread_print_partial_symbols (p, count, what)
     struct partial_symbol **p;
     int count;
     char *what;
{
  printf ("    %s partial symbols:\n", what);
  while (count-- > 0)
    {
      printf ("      `%s'", SYMBOL_NAME(*p));
      if (SYMBOL_DEMANGLED_NAME (*p) != NULL)
        {
          printf ("  `%s'", SYMBOL_DEMANGLED_NAME (*p));
        }
      printf (", ");
      switch (PSYMBOL_NAMESPACE (*p))
        {
        case UNDEF_NAMESPACE:
          printf ("undefined namespace, ");
          break;
        case VAR_NAMESPACE:
          /* This is the usual thing -- don't print it */
          break;
        case STRUCT_NAMESPACE:
          printf ("struct namespace, ");
          break;
        case VAR_OR_STRUCT_NAMESPACE:
          printf ("var or struct namespace, ");
          break;
        case LABEL_NAMESPACE:
          printf ("label namespace, ");
          break;
        default:
          printf ("<invalid namespace>, ");
          break;
        }
      switch (PSYMBOL_CLASS (*p))
        {
        case LOC_UNDEF:
          printf ("undefined");
          break;
        case LOC_CONST:
          printf ("constant int");
          break;
        case LOC_STATIC:
          printf ("static");
          break;
        case LOC_INDIRECT:
          printf ("extern global");
          break;
        case LOC_REGISTER:
          printf ("register");
          break;
        case LOC_ARG:
          printf ("pass by value");
          break;
        case LOC_REF_ARG:
          printf ("pass by reference");
          break;
        case LOC_REGPARM:
          printf ("register parameter");
          break;
        case LOC_REGPARM_ADDR:
          printf ("register address parameter");
          break;
        case LOC_LOCAL:
          printf ("stack parameter");
          break;
        case LOC_TYPEDEF:
          printf ("type");
          break;
        case LOC_LABEL:
          printf ("label");
          break;
        case LOC_BLOCK:
          printf ("function");
          break;
        case LOC_CONST_BYTES:
          printf ("constant bytes");
          break;
        case LOC_LOCAL_ARG:
          printf ("shuffled arg");
          break;
        case LOC_UNRESOLVED:
          printf ("unresolved");
          break;
        case LOC_OPTIMIZED_OUT:
          printf ("optimized out");
          break;
        default:
          printf ("<invalid location>");
          break;
        }
      printf (", ");
      /* FIXME-32x64: Need to use SYMBOL_VALUE_ADDRESS, etc.; this
         could be 32 bits when some of the other fields in the union
         are 64.  */
#ifdef BFD64      
      printf ("0x%llx\n", (long long) SYMBOL_VALUE_ADDRESS (*p));
#else      
      printf ("0x%lx\n", (long) SYMBOL_VALUE (*p));
#endif
      p++;
    }
}

static void
hpread_dump_psymtab (objfile, psymtab)
     struct objfile *objfile;
     struct partial_symtab *psymtab;
{
  int i;

  printf ("\nPartial symtab for source file %s ",
                    psymtab -> filename);
  printf ("(object ");
  printf ("0x%lx", (unsigned long) psymtab);
  printf (")\n\n");
  printf ("  Read from executable file %s (",
                      objfile -> name);
  printf ("0x%lx", (unsigned long) objfile);
  printf(")\n");

  if (psymtab -> readin)
    {
      printf ("  Full symtab was read (at ");
      printf ("0x%lx", (unsigned long) psymtab->symtab);
      printf (" by function at ");
      printf ("0x%lx", (unsigned long)((PTR)psymtab->read_symtab));
      printf (")\n");
    }

  printf ("  Relocate symbols by ");
  for (i = 0; i < psymtab->objfile->num_sections; ++i)
    {
      if (i != 0)
        printf (", ");
#ifdef BFD64      
      printf ("0x%llx",
             (unsigned long long) (ANOFFSET(psymtab->section_offsets, i)));
#else      
      printf ("0x%lx",
             (unsigned long) (ANOFFSET(psymtab->section_offsets, i)));
#endif      
    }
  printf ("\n");

  printf ("  Symbols cover text addresses ");
#ifdef BFD64
  printf ("0x%llx", (unsigned long long) (psymtab->textlow));
#else  
  printf ("0x%lx", (unsigned long) (psymtab->textlow));
#endif
  printf ("-");
#ifdef BFD64
  printf ("0x%llx", (unsigned long long) (psymtab->texthigh));
#else
  printf ("0x%lx", (unsigned long) (psymtab->texthigh));
#endif
  printf ("\n");
  printf ("  Depends on %d other partial symtabs.\n",
                    psymtab->number_of_dependencies);
  for (i = 0; i < psymtab->number_of_dependencies; i++)
    {
      printf ("    %d ", i);
      printf ("0x%lx", (unsigned long) psymtab->dependencies[i]);
      printf (" %s\n",
                        psymtab->dependencies[i]->filename);
    }
  if (psymtab -> n_global_syms > 0)
    {
      hpread_print_partial_symbols (objfile->global_psymbols.list
                                    + psymtab->globals_offset,
                                    psymtab->n_global_syms, "Global");
    }
  if (psymtab -> n_static_syms > 0)
    {
      hpread_print_partial_symbols (objfile->static_psymbols.list
                                    + psymtab->statics_offset,
                                    psymtab->n_static_syms, "Static");
    }
  printf ("\n");
}

/* Scan and build partial symbols for a symbol file when using DOOM
 * (debug only in object modules).
 *
 *  The minimal symbol table (either SOM or HP a.out) and the DOOM
 *  specific tebles have already been read in. We just need to map
 *  this information into psymtabs.
 *
 *  SECTION_OFFSETS contains offsets relative to which the symbols in
 *  the various sections are (depending where the sections were
 *  actually loaded).
 *  MAINLINE is true if we are reading the main symbol table (as
  lntt_section = bfd_get_section_by_name (objfile->obfd, "$LNTT$");
  slt_section = bfd_get_section_by_name (objfile->obfd, "$SLT$");
  vt_section = bfd_get_section_by_name (objfile->obfd, "$VT$");
 *  opposed to a shared lib or dynamically loaded file).
 */

void
hpread_build_doom_psymtabs (objfile, section_offsets, mainline)
     struct objfile         *objfile;
     struct section_offsets *section_offsets;
     int                      mainline;
{
    struct minimal_symbol *m;
    char *name, *dem_name;
    int n_objs, objid, i, j, last_objid, psymtabid;
    int cur_psymtab_id;
    int n_psymtabs;
    int ignore_entry, n;
    struct partial_symbol_map_entry *partial_symbol_map;
    int partial_symbol_map_size, new_partial_symbol_map_size;
    struct partial_symtab *pst;
    extern void hpread_doom_psymtab_to_symtab();
    struct partial_symbol **p;
    struct partial_symbol **global_syms; 
    struct partial_symbol **static_syms; 
    struct psymbol_allocation_list *list;
    CORE_ADDR pc;
    
    /* scan the minimal symbols, and decide which symbol belongs where */
    if (debug_traces)
        printf("\nBuilding DOOM psymtabs...\n");

    /* build a table with partial symbol information. This is indexed
     * by psymtab ids.
     */
    n_objs = COMPMAP(objfile)->n_cr;
    partial_symbol_map_size = n_objs;
    partial_symbol_map =
        xmalloc(partial_symbol_map_size*
                sizeof(struct partial_symbol_map_entry));
    if (!partial_symbol_map)
        error("Out of memory");


    /* assume that each psymbol list is roughly equal in size for
     * initial estimate of size
     */
    n = (n_objs > 0) ? 
        n = objfile->minimal_symbol_count/n_objs/2 :
        0;

    for (i = 0; i < n_objs; i++) {
        partial_symbol_map[i].objid = i;
        partial_symbol_map[i].is_default = 1;
        partial_symbol_map[i].has_non_doom_debug = 0;        
        partial_symbol_map[i].textlow = 0;
        partial_symbol_map[i].texthigh = 0;
        partial_symbol_map[i].globals.list = 
          partial_symbol_map[i].globals.next = 
              (struct partial_symbol **)
                xmalloc (n*sizeof (struct partial_symbol *));
        partial_symbol_map[i].globals.size = n;   /* srikanth, 000207 */
        partial_symbol_map[i].statics.list = 
          partial_symbol_map[i].statics.next = 
              (struct partial_symbol **)
                xmalloc (n*sizeof (struct partial_symbol *));
        partial_symbol_map[i].statics.size = n; 
    }

    /* fill in the psymytab map. Also assign psymtab ids to all
     * elements in the subspace map */
    /* non-textspace elements have psymtab id = objid */
    for (i = 0; i < SUBSPACEMAP(objfile)->n_sr; i++) {
        SUBSPACEMAP(objfile)->sr[i].info =
            SUBSPACEMAP(objfile)->sr[i].objid;
    }
    
    /* each group of contiguous textspace elements from the same
     * object file has a unique psymtab id
     */
    last_objid = -1;
    cur_psymtab_id = -1;
    n_psymtabs = n_objs;
    for (i = SUBSPACEMAP(objfile)->textstart;
         i <= SUBSPACEMAP(objfile)->textend;
         i++) {
        objid = SUBSPACEMAP(objfile)->sr[i].objid;
        if (objid == -1) {
            SUBSPACEMAP(objfile)->sr[i].info = -1;
            last_objid = -1;
            continue;
        }
        if (objid == last_objid) {
            SUBSPACEMAP(objfile)->sr[i].info = cur_psymtab_id;
            partial_symbol_map[cur_psymtab_id].texthigh =
                SUBSPACEMAP(objfile)->sr[i].end;
            continue;
        }

        if (partial_symbol_map[objid].textlow != 0) {
            /* resize psymtab map if necessary */
            if (n_psymtabs >= partial_symbol_map_size)
                {
                    new_partial_symbol_map_size =
                        2*partial_symbol_map_size;
                    partial_symbol_map =
                        xrealloc(partial_symbol_map,
                                 new_partial_symbol_map_size*
                                 sizeof(struct partial_symbol_map_entry));
                    for (j = partial_symbol_map_size;
                         j < new_partial_symbol_map_size;
                         j++) {
                        partial_symbol_map[j].objid = -1;

                        partial_symbol_map[j].has_non_doom_debug = 0;        
                        partial_symbol_map[j].textlow = 0;
                        partial_symbol_map[j].texthigh = 0;
                        partial_symbol_map[j].is_default = 0;
                        partial_symbol_map[j].globals.list = 
                            partial_symbol_map[j].globals.next = 
                            (struct partial_symbol **)
                            xmalloc (n*sizeof (struct partial_symbol *));
                        partial_symbol_map[j].globals.size = n; 
                        partial_symbol_map[j].statics.list = 
                            partial_symbol_map[j].statics.next = 
                            (struct partial_symbol **)
                            xmalloc (n*sizeof (struct partial_symbol *));
                        partial_symbol_map[j].statics.size = n; 
                    }
                    partial_symbol_map_size = new_partial_symbol_map_size;
                }
            cur_psymtab_id = n_psymtabs++;
        }
        else {
            cur_psymtab_id = objid;
        }
        partial_symbol_map[cur_psymtab_id].textlow =
            SUBSPACEMAP(objfile)->sr[i].start;
        partial_symbol_map[cur_psymtab_id].texthigh =
            SUBSPACEMAP(objfile)->sr[i].end;
        partial_symbol_map[cur_psymtab_id].objid = objid;
        SUBSPACEMAP(objfile)->sr[i].info = cur_psymtab_id;

        /* RM: Mark psymtabs for which we already have debug
         * information in the excutable */
        /* ??? We want to check if the range of pc values covered by
         * this psymtab is already covered by an existing
         * psymtab. Unfortunately, there seem to be some slight
         * variation on exactly where the psymtabs start, so using
         * textlow doesn't work. Use the average of textlow and
         * texthigh for now. */
        pc = (partial_symbol_map[cur_psymtab_id].textlow +
              partial_symbol_map[cur_psymtab_id].texthigh) / 2;
        if (find_pc_psymtab(pc)) {
            if (debug_traces > 1) {
                printf("    %s has non-DOOM debug information\n",
                       STRINGS(objfile) +
                       COMPMAP(objfile)->cr[objid].objname);
            }
            partial_symbol_map[cur_psymtab_id].has_non_doom_debug = 1;
        }
        
        last_objid = objid;
    }            
    
    if (debug_traces >= 10000)
        printf("  Symbols:\n");

    /* srikanth, 990325, the doom type and const tables have a high degree 
       of duplication in them. It is profitable to get bcache to eliminate 
       the dups. 
     */
    bcache_eliminate_duplicates();

    ALL_OBJFILE_MSYMBOLS(objfile, m) {

        switch(MSYMBOL_TYPE(m)) {

        /* srikanth, we used to add mst_text, mst_file_text, mst_file_data
           and mst_file_bss symbols to the psymtabs here. Per the new scheme,
           these won't get added anymore and we will fall back on the linker
           symbol table for these. We still need to add mst_data and mst_bss
        */
            case mst_data:   
            case mst_bss:   

                /* Is this a generated symbols ? if yes ignore it */

#ifdef IS_RELOC_ONLY_SYMBOL
                if (IS_RELOC_ONLY_SYMBOL(SYMBOL_NAME(m)))
                  continue;
#endif

                psymtabid = 
                  hpread_find_subspace_map_info(objfile,
                                                m->ginfo.value.address,
                                                dataspace);
                if (psymtabid == -1)
                    break;

                objid = partial_symbol_map[psymtabid].objid;

                MSYMBOL_NAMESPACE (m) = VAR_NAMESPACE;
                MSYMBOL_CLASS (m) = LOC_STATIC;
                SYMBOL_LANGUAGE(m) = COMPMAP(objfile)->cr[objid].lang;

                list = &(partial_symbol_map[psymtabid].globals);
                if (list->next >= list->list + list->size)
                  extend_psymbol_list (list, objfile);
                *list->next++ = (struct partial_symbol *) m;
                OBJSTAT (objfile, n_psyms++);

                if (debug_traces >= 10000)
                    printf("    %s is in %s, psymtab id = %d\n",
                           m->ginfo.name,
                           (objid == -1) ?
                           "unknown file" :
                           STRINGS(objfile) +
                           COMPMAP(objfile)->cr[objid].objname,
                           psymtabid);
                break;

            default:
                if (debug_traces >= 100)
                    printf("    %s ignored: unexpected type\n",
                           m->ginfo.name);
                break;
        }
    }

    /* CM: Types */
    /* for each type entry
       - get psymtabid
       - get objid
                objid = partial_symbol_map[psymtabid].objid;
       - add entry
       add_psymbol_to_list
       (name,
       strlen(name),
       STRUCT_NAMESPACE,
       LOC_TYPEDEF,
       &(partial_symbol_map[psymtabid].globals),
       0,
       0,
       COMPMAP(objfile)->cr[objid].lang,
       objfile, -1);
       */
    if (TYPETABLEMAP(objfile).table) {
        char *buf_start_of_string;
        char *buf_index;
        int count_index;
        char *buf = TYPETABLEMAP(objfile).table;
        char *buf_prev_index;
        char *buf_index_end;

        for (count_index = 0,
                 buf_prev_index = TYPETABLEMAP(objfile).table,
                 buf_index = TYPETABLEMAP(objfile).table + 1,
                 buf_index_end = TYPETABLEMAP(objfile).table + TYPETABLEMAP(objfile).table_size,
                 buf_start_of_string = TYPETABLEMAP(objfile).table;
             buf_index < buf_index_end;
             buf_index++, buf_prev_index++, count_index++) {
            if ((*buf_index == '\0') && (*buf_prev_index != '\0')) {
                psymtabid =
                    hpread_find_subspace_map_info(objfile,
                                                  (CORE_ADDR) (TYPETABLEMAP(objfile).subspacemap_base +
                                                   count_index),
                                                  debugspace);
                if (psymtabid == -1)
                    continue;

                objid = partial_symbol_map[psymtabid].objid;
                
                name = buf_start_of_string;


                /* srikanth, 990323, To avoid having to add the same type
                   twice, I am introducing a new dual or fuzzy namespace 
                   called VAR_OR_STRUCT_NAMESPACE.  Also since the name
                   equals the demangled name,  use add_psymbol_to_list()
                   It makes two calls to bcache() instead of three */

                /* Add as a fuzzy type */
                add_psymbol_to_list
                    (name,
                     strlen(name),
                     VAR_OR_STRUCT_NAMESPACE,
                     LOC_TYPEDEF,
                     &(partial_symbol_map[psymtabid].globals),
                     0,
                     0,                          /* core addr?? */
                     COMPMAP(objfile)->cr[objid].lang,
                     objfile, -1);

                if (debug_traces >= 10000)
                    printf("    %s is in %s, psymtab id = %d\n",
                           name,
                           (objid == -1) ?
                           "unknown file" :
                           STRINGS(objfile) +
                           COMPMAP(objfile)->cr[objid].objname,
                           psymtabid);

                buf_start_of_string = buf_index + 1;
            } else if ((*buf_index == '\0') && (*buf_prev_index == '\0')) {
                buf_start_of_string = buf_index + 1;
            }
        }
      munmap (TYPETABLEMAP (objfile).table,
	      TYPETABLEMAP (objfile).table_size);
    }

    /* CM: Constants */
    /* for each const entry
       - get psymtabid
       - get objid
                objid = partial_symbol_map[psymtabid].objid;
       - add entry
       add_psymbol_to_list
       (name,
       strlen(name),
       VAR_NAMESPACE,
       LOC_CONST,
       &(partial_symbol_map[psymtabid].globals),
       0,
       0,
       COMPMAP(objfile)->cr[objid].lang,
       objfile, -1);
       */
    if (CONSTTABLEMAP(objfile).table) {
        char *buf_start_of_string;
        char *buf_index;
        int count_index;
        char *buf = CONSTTABLEMAP(objfile).table;
        char *buf_prev_index;
        char *buf_index_end;

        for (count_index = 0,
                 buf_prev_index = CONSTTABLEMAP(objfile).table,
                 buf_index = CONSTTABLEMAP(objfile).table + 1,
                 buf_index_end = CONSTTABLEMAP(objfile).table + CONSTTABLEMAP(objfile).table_size,
                 buf_start_of_string = CONSTTABLEMAP(objfile).table;
             buf_index < buf_index_end;
             buf_index++, buf_prev_index++, count_index++) {
            if ((*buf_index == '\0') && (*buf_prev_index != '\0')) {
                psymtabid =
                    hpread_find_subspace_map_info(objfile,
                                                  (CORE_ADDR) (CONSTTABLEMAP(objfile).subspacemap_base +
                                                   count_index),
                                                  debugspace);
                if (psymtabid == -1)
                    continue;

                objid = partial_symbol_map[psymtabid].objid;
                
                name = buf_start_of_string;
                
                add_psymbol_to_list
                    (name,
                     strlen(name),
                     VAR_NAMESPACE,
                     LOC_CONST,
                     &(partial_symbol_map[psymtabid].globals),
                     0,
                     0,                          /* core addr?? */
                     COMPMAP(objfile)->cr[objid].lang,
                     objfile, -1);
                if (debug_traces >= 10000)
                    printf("    %s is in %s, psymtab id = %d\n",
                           name,
                           (objid == -1) ?
                           "unknown file" :
                           STRINGS(objfile) +
                           COMPMAP(objfile)->cr[objid].objname,
                           psymtabid);

                buf_start_of_string = buf_index + 1;
            } else if ((*buf_index == '\0') && (*buf_prev_index == '\0')) {
                buf_start_of_string = buf_index + 1;
            }
        }
      munmap (CONSTTABLEMAP (objfile).table, 
	      CONSTTABLEMAP (objfile).table_size);
    }

    if (debug_traces >= 1000) {
        printf("Partial symbol map:\n");
        for (i = 0; i < n_psymtabs; i++) {
            if (partial_symbol_map[i].objid == -1)
                continue;
#ifdef BFD64
            printf("  %s:\n    textlow = %016llx\n    texthigh = %016llx\n",
                   STRINGS(objfile)+
                   (COMPMAP(objfile)->cr[partial_symbol_map[i].objid].objname),
                   (long long) partial_symbol_map[i].textlow,
                   (long long) partial_symbol_map[i].texthigh);
#else
            printf("  %s:\n    textlow = %08lx\n    texthigh = %08lx\n",
                   STRINGS(objfile)+
                   (COMPMAP(objfile)->cr[partial_symbol_map[i].objid].objname),
                   (long) partial_symbol_map[i].textlow,
                   (long) partial_symbol_map[i].texthigh);
#endif
            hpread_print_partial_symbols (partial_symbol_map[i].globals.list,
                                          partial_symbol_map[i].globals.next-
                                            partial_symbol_map[i].globals.list,
                                          "Global");
    
            hpread_print_partial_symbols (partial_symbol_map[i].statics.list,
                                          partial_symbol_map[i].statics.next-
                                            partial_symbol_map[i].statics.list,
                                          "Static");
        }
    }
    
    global_syms = objfile -> global_psymbols.next;
    static_syms = objfile -> static_psymbols.next;
    for (i = 0; i < n_psymtabs; i++) {
        if (partial_symbol_map[i].objid == -1)
          {
            if (partial_symbol_map[i].globals.list)
              free(partial_symbol_map[i].globals.list);
            if (partial_symbol_map[i].statics.list)
              free(partial_symbol_map[i].statics.list);
            continue;
          }

        if (partial_symbol_map[i].has_non_doom_debug)
          {
            if (partial_symbol_map[i].globals.list)
              free(partial_symbol_map[i].globals.list);
            if (partial_symbol_map[i].statics.list)
              free(partial_symbol_map[i].statics.list);
            continue;
          }
        
        /* Just use the non-DOOM procedures to create the
         * psymtab. We'll munge the fields that need to be different
         * for DOOM once we have a psymtab */

        pst = hpread_start_psymtab(objfile,
                                   STRINGS(objfile) +
                                   COMPMAP(objfile)->cr[partial_symbol_map[i].objid].srcname,
                                   partial_symbol_map[i].textlow -
                                   ANOFFSET(section_offsets, 0), /* Gross!! */
                                   0,
                                   global_syms,
                                   static_syms);
    
        /* We used to call add_psymbol_with_dem_name_to_list() here.
           Now we simply reuse the psymbol pointers from the partial
           symbol map. 
        */
        for (p = partial_symbol_map[i].globals.list;
             p != partial_symbol_map[i].globals.next;
             p++) {
             
             list = &(objfile->global_psymbols);
             if (list->next >= list->list + list->size)
               extend_psymbol_list (list, objfile);
             *list->next++ = *p;
             OBJSTAT (objfile, n_psyms++);
        }

        free(partial_symbol_map[i].globals.list);
        for (p = partial_symbol_map[i].statics.list;
             p != partial_symbol_map[i].statics.next;
             p++) {

                   list = &(objfile->static_psymbols);
                   if (list->next >= list->list + list->size)
                     extend_psymbol_list (list, objfile);
                   *list->next++ = *p;
                   OBJSTAT (objfile, n_psyms++);
        }

        free(partial_symbol_map[i].statics.list);
        pst = hpread_end_psymtab (pst,
                                  NULL,        /* psymtab_include_list */
                                  0,           /* includes_used        */
                                  0,           /* end index in LNTT    */
                                               /* we don't use this    */
                                  partial_symbol_map[i].texthigh - 
                                     ANOFFSET(section_offsets, 0), /* Gross!! */
                                  NULL,        /* dependency_list      */
                                  0);          /* dependencies_used    */

        if (pst) {
            OBJID(pst) = partial_symbol_map[i].objid;
            DEFAULT_PSYMTAB(pst) = partial_symbol_map[i].is_default;
            OFILE_LOAD_STATUS(pst) = DOOM_OFILE_DO_DEMAND_LOAD;
            pst->read_symtab = hpread_doom_psymtab_to_symtab;
            /* RM: We know the language, set it now */
            if ((OBJID(pst) >= 0) &&
                (COMPMAP(objfile)->cr[OBJID(pst)].lang != language_unknown))
                pst->language = COMPMAP(objfile)->cr[OBJID(pst)].lang;
        }


        global_syms = objfile->global_psymbols.next;
        static_syms = objfile->static_psymbols.next;
    }

    /* deal with the left overs - these were never used */
    for ( ; i < partial_symbol_map_size; i++) {
      if (partial_symbol_map[i].globals.list)
        free(partial_symbol_map[i].globals.list);
      if (partial_symbol_map[i].statics.list)
        free(partial_symbol_map[i].statics.list);
    }

    if (debug_traces >= 1000) {
        ALL_PSYMTABS (objfile, pst) {
            hpread_dump_psymtab (objfile, pst);
        }
    }

    free(partial_symbol_map);
    bcache_ignore_duplicates();  /* until the next time */
    if (debug_traces > 1)
        printf("...done\n");
}

/* RM: Lookup a name in "globals" psymtabs from later chapters. If one
   is found, that symbol is considered to override the input symbol */
/* RM: ??? This does a binary search over the "globals" psymtabs,
   making the process of constructing the "globals" psymtabs an O(n
   log(n)) logorithm. A hash table may work better */
int hpread_symbol_overridden (objfile, name, namespace)
  struct objfile *objfile;
  char *name;
  namespace_enum namespace;
{
  struct partial_symtab *tpst;
  struct partial_symbol *
    lookup_partial_symbol(struct partial_symtab *, char *, int, namespace_enum);

  /* JYG: Don't do this if we're not looking at +ild files or
     looking at the latest chapter. */
  if ( (N_CHAPTERS (objfile) <= 1) ||
       (CURRENT_CHAPTER (objfile) == N_CHAPTERS (objfile) - 1))
    return 0;

  ALL_OBJFILE_PSYMTABS (objfile, tpst)
    {
      if ( (LDCHAPTER (tpst) > CURRENT_CHAPTER (objfile)) &&
	   !strcmp (tpst->filename, "globals"))
        {
          if (lookup_partial_symbol (tpst, name, 1, namespace))
            return 1;
        }
    }

  return 0;
}

void
hpread_build_ild_psymtabs (objfile, mainline)
     struct objfile *objfile;
     int mainline;
{
  int within_function = 0;
  int within_function_pst = 0;
  int sym_index_source;
  unsigned int symcount;
  unsigned int hp_symnum;
  union dnttentry *dn_bufp;
  union dnttentry *dn_temp;
  CORE_ADDR valu;
  char *namestring;
  char *dem_name;
  struct partial_symtab *pst;
  CORE_ADDR texthigh;
  enum language lang;
  char *headers, *current_header;
  unsigned int header_size;
  int doc_header;
  asection *header_sect;
  int gntt_offset;
  int lntt_offset;
  int slt_offset;
  int vt_offset;
  int dntt_limit;
  union sltentry *sl_bufp;
  
  symcount = LNTT_SYMCOUNT (objfile);
  pst = 0;

  header_sect =  (asection *)
                  bfd_get_section_by_name_and_id (objfile->obfd, HP_HEADER,
                                                  CURRENT_CHAPTER(objfile));
  headers = (char *) alloca(bfd_section_size(objfile->obfd, header_sect));
  bfd_get_section_contents (objfile->obfd,
                            header_sect,
                            headers, 0,
                            bfd_section_size(objfile->obfd, header_sect));
  gntt_offset = 0;
  lntt_offset = 0;
  slt_offset = 0;
  vt_offset = 0;
  current_header = headers;
  doc_header = ((DOC_info_header *) current_header)->doc_header;
  if (GLOBALS_START(objfile))
    dntt_limit = doc_header ?
                  ((DOC_info_header *) current_header)->lntt_length :
                  ((XDB_header *) current_header)->lntt_length;
  else
    dntt_limit = doc_header ?
                 ((DOC_info_header *) current_header)->gntt_length :
                 ((XDB_header *) current_header)->gntt_length;
   
  for (hp_symnum = 0;  hp_symnum < symcount; hp_symnum++)
    {
      QUIT;

      dn_bufp = hpread_get_lntt (hp_symnum, objfile);

      if (dn_bufp->dblock.extension)
        continue;

      /* RM: We need to adjust the dntt, slt and vt pointers as we go
         along. Calculate offsets for this adjustment here. Normally
         pxdb would have done this for us :( */
      if (hp_symnum * sizeof (struct dntt_type_block) >= dntt_limit)
        {
          doc_header = ((DOC_info_header *) current_header)->doc_header;
          if (hp_symnum == GLOBALS_START(objfile))
            {
              vt_offset = 0;
              slt_offset = 0;
              gntt_offset = 0;
              lntt_offset = 0;
              current_header = headers;
            }
          else
            {
              if (doc_header)
                {
                  vt_offset += ((DOC_info_header *) current_header)->vt_length;
                  slt_offset += ((DOC_info_header *) current_header)->slt_length / sizeof (union sltentry);
                  gntt_offset += ((DOC_info_header *) current_header)->gntt_length / sizeof (struct dntt_type_block);
                  lntt_offset += ((DOC_info_header *) current_header)->lntt_length / sizeof (struct dntt_type_block);
                  current_header += sizeof(DOC_info_header);
                }
              else
                {
                  vt_offset += ((XDB_header *) current_header)->vt_length;
                  slt_offset += ((XDB_header *) current_header)->slt_length / sizeof (union sltentry);
                  gntt_offset += ((((XDB_header *) current_header)->gntt_length) & 0x7fffffff) / sizeof (struct dntt_type_block);
                  lntt_offset += ((XDB_header *) current_header)->lntt_length / sizeof (struct dntt_type_block);
                  current_header += sizeof(XDB_header);
                }
            }
                 

          if (hp_symnum >= GLOBALS_START(objfile))
            dntt_limit += doc_header ?
                          ((DOC_info_header *) current_header)->gntt_length :
                          (((XDB_header *) current_header)->gntt_length &
                           0x7fffffff);
          else
            dntt_limit += doc_header ?
                          ((DOC_info_header *) current_header)->lntt_length :
                          ((XDB_header *) current_header)->lntt_length;
        }


      
#define ADJUST_VT(field) \
   if (field)            \
     (field) += vt_offset;
#define ADJUST_SLT(field)                              \
  (field) += slt_offset;                               \
  /* RM: adjust backpointer too */                     \
  sl_bufp = hpread_get_slt ((field), objfile);         \
  if (sl_bufp->sspec.backptr.dnttp.global)             \
    sl_bufp->sspec.backptr.dnttp.index += gntt_offset; \
  else                                                 \
    sl_bufp->sspec.backptr.dnttp.index += lntt_offset;
#define ADJUST_DNTT(field)                                  \
  if ((field).word != DNTTNIL && !(field).dnttp.immediate)  \
    if ((field).dnttp.global)                               \
      (field).dnttp.index += gntt_offset;                   \
    else                                                    \
      (field).dnttp.index += lntt_offset;


                                                              
      if (hpread_has_name(dn_bufp->dblock.kind)) 
        ADJUST_VT(dn_bufp->dsfile.name)
                                                                   
      /* Only handle things which are necessary partial symbols.
         everything else is ignored (except for dnntponter, sltpinter
         and vtpointer adjustments. */
      switch (dn_bufp->dblock.kind)
        {
          case DNTT_TYPE_SRCFILE:
            ADJUST_SLT(dn_bufp->dsfile.address)
            sym_index_source = hp_symnum;
            continue;
            
          case DNTT_TYPE_MODULE:
            ADJUST_VT(dn_bufp->dmodule.alias)
            ADJUST_SLT(dn_bufp->dmodule.address)

            if (pst)
              {
                hpread_end_psymtab (pst, 0, 0, 
                                    ((hp_symnum - 1)
                                     * sizeof (struct dntt_type_block)),
                                    texthigh,
                                    0, 0);
                pst = (struct partial_symtab *) 0;
                texthigh = 0;
              }

            /* Now begin a new module and a new psymtab for it */
            SET_NAMESTRING (dn_bufp, &namestring, objfile);

            if (STREQ (namestring, "<<<NULL_SYMBOL>>>"))
              {
                union dnttentry *sym_index_string =
                  hpread_get_lntt (sym_index_source, objfile);
                SET_NAMESTRING (sym_index_string, &namestring, objfile);
                dn_bufp->dmodule.name = sym_index_string->dsfile.name;
              }
            
            valu = hpread_get_textlow(0, hp_symnum, objfile, symcount);
            valu += ANOFFSET (objfile->section_offsets,
			      SECT_OFF_TEXT (objfile));
            if (!pst)
              {
                pst = hpread_start_psymtab (objfile,
                                            namestring, valu,
                                            (hp_symnum
                                             * sizeof (struct dntt_type_block)),
                                            objfile->global_psymbols.next,
                                            objfile->static_psymbols.next);
                texthigh = valu;
              }
            continue;

          case DNTT_TYPE_FUNCTION:
          case DNTT_TYPE_DOC_FUNCTION:
            ADJUST_VT(dn_bufp->dfunc.alias)
            ADJUST_SLT(dn_bufp->dfunc.address)
            ADJUST_DNTT(dn_bufp->dfunc.firstparam)
            ADJUST_DNTT(dn_bufp->dfunc.retval)
            if (dn_bufp->dblock.kind == DNTT_TYPE_DOC_FUNCTION)
              ADJUST_DNTT(dn_bufp->ddocfunc.inline_list);

            valu = dn_bufp->dfunc.hiaddr +
		   ANOFFSET (objfile->section_offsets,
			     SECT_OFF_TEXT (objfile));
            if (valu > texthigh)
              texthigh = valu;
            /* RM: Don't need to add functions to the psymtabs per
               Srikanth's fat free psymtabs */
            /* RM: But need to add main for Fortran */
            lang = trans_lang ((enum hp_language) dn_bufp->dfunc.language);
            if (dn_bufp->dfunc.alias)
              {
                namestring = VT(objfile) + dn_bufp->dfunc.alias;
                SET_NAMESTRING (dn_bufp, &dem_name, objfile);
              }
            else
              {
                SET_NAMESTRING (dn_bufp, &namestring, objfile);
                dem_name = NULL;
              }
            valu = dn_bufp->dfunc.lowaddr +
                   ANOFFSET (objfile->section_offsets,
			     SECT_OFF_TEXT (objfile));
            if (dn_bufp->dfunc.global && namestring &&
                strcmp (namestring, "_MAIN_") == 0)
              {
                if (lang == language_fortran)
                  {
                    if (strcmp(dem_name, default_main) == 0)
                      dem_name = NULL;
                    default_main = namestring = default_fortran_main_string;
                  }
                else
                  {
                    namestring = default_main;
                    dem_name = NULL;
                  }
                add_psymbol_with_dem_name_to_list (namestring,
                                                   strlen (namestring),
                                                   dem_name,
                                                   strlen (dem_name),
                                                   VAR_NAMESPACE,
                                                   LOC_BLOCK,
                                                   &objfile->global_psymbols,
                                                   valu,
                                                   0,	/* core addr?? */
                                                   lang,
                                                   objfile);
              }

            /* RM: Check that global functions are where we expect them to
               be. If not, that's a good sign that this psymtab has been
               overridden by an incrementally linked module */
            if (CURRENT_CHAPTER (objfile) < N_CHAPTERS(objfile) - 1)
              {
                if (namestring != default_fortran_main_string &&
                    dn_bufp->dfunc.global)
                  {
                    struct minimal_symbol *m;
                    
                    m = lookup_minimal_symbol(namestring, 0, objfile);
                    if (!m || SYMBOL_VALUE_ADDRESS(m) != valu)
                      OVERRIDDEN (pst) = 1;
                  }
              }
            
            /* RM: Each COMDAT function goes into its own little psymtab */
            if (!pst)
              {
                texthigh = dn_bufp->dfunc.hiaddr +
			   ANOFFSET (objfile->section_offsets,
				     SECT_OFF_TEXT (objfile));
                /* RM: start it at the last srcfile seen */
                dn_temp = hpread_get_lntt (sym_index_source, objfile);
                SET_NAMESTRING (dn_temp, &namestring, objfile);
                pst = hpread_start_psymtab (objfile,
                                            namestring, valu,
                                            (sym_index_source
                                             * sizeof (struct dntt_type_block)),
                                            objfile->global_psymbols.next,
                                            objfile->static_psymbols.next);
                within_function_pst = 1;
              }
            within_function = 1;
            continue;
            
          case DNTT_TYPE_END:
            ADJUST_SLT(dn_bufp->dend.address)
            ADJUST_DNTT(dn_bufp->dend.beginscope)

            dn_temp = hpread_get_lntt (INDEX (objfile,
                                              dn_bufp->dend.beginscope.dnttp),
                                       objfile);
            if ((dn_temp->dblock.kind == DNTT_TYPE_FUNCTION) ||
                (dn_temp->dblock.kind == DNTT_TYPE_DOC_FUNCTION))
              {
                within_function = 0;
                /* RM: If processing a COMDAT function, end the psymtab */
                if (pst && within_function_pst)
                  {
                    hpread_end_psymtab (pst, 0, 0, 
                                        (hp_symnum 
                                         * sizeof (struct dntt_type_block)),
                                        texthigh,
                                        0, 0);
                    pst = (struct partial_symtab *) 0;
                    texthigh = 0;
                    within_function_pst = 0;
                  }
              }
            /* RM: If at the end of a module, end the psymtab */
            if (dn_temp->dblock.kind == DNTT_TYPE_MODULE)
              if (pst)
                {
                  hpread_end_psymtab (pst, 0, 0, 
                                      ((hp_symnum - 1)
                                       * sizeof (struct dntt_type_block)),
                                      texthigh,
                                      0, 0);
                  texthigh = 0;
                  pst = (struct partial_symtab *) 0;
                }
                
            continue;

          case DNTT_TYPE_SVAR:
          case DNTT_TYPE_DVAR:
          case DNTT_TYPE_TYPEDEF:
          case DNTT_TYPE_TAGDEF:
            {
              /* Variables, typedefs and the like.  */
              enum address_class storage;
              namespace_enum namespace;

              if (dn_bufp->dblock.kind == DNTT_TYPE_SVAR)
                {
                  ADJUST_DNTT(dn_bufp->dsvar.type)
                }
              else if (dn_bufp->dblock.kind == DNTT_TYPE_DVAR)
                {
                  ADJUST_DNTT(dn_bufp->ddvar.type)
                }
              else
                {
                  ADJUST_DNTT(dn_bufp->dtype.type)
                }

              /* Don't add locals to the partial symbol table.  */
              if (within_function
                  && (dn_bufp->dblock.kind == DNTT_TYPE_SVAR
                      || dn_bufp->dblock.kind == DNTT_TYPE_DVAR))
                continue;

              /* RM: Don't bother with incomplete types */
              if ((dn_bufp->dblock.kind == DNTT_TYPE_TYPEDEF
                   || dn_bufp->dblock.kind == DNTT_TYPE_TAGDEF) &&
                  (!dn_bufp->dtype.typeinfo))
                continue;
              
              SET_NAMESTRING (dn_bufp, &namestring, objfile);

              /* TAGDEFs go into the structure namespace.  */
              if (dn_bufp->dblock.kind == DNTT_TYPE_TAGDEF)
                namespace = STRUCT_NAMESPACE;
              else
                namespace = VAR_NAMESPACE;

              /* What kind of "storage" does this use?  */
              if (dn_bufp->dblock.kind == DNTT_TYPE_SVAR)
                storage = LOC_STATIC;
              else if (dn_bufp->dblock.kind == DNTT_TYPE_DVAR
                       && dn_bufp->ddvar.regvar)
                storage = LOC_REGISTER;
              else if (dn_bufp->dblock.kind == DNTT_TYPE_DVAR)
                storage = LOC_LOCAL;
              else
                storage = LOC_UNDEF;
              
              if (!pst)
                {
                  pst = hpread_start_psymtab (objfile,
                                              "globals", 0,
                                              (hp_symnum
                                               * sizeof (struct dntt_type_block)),
					      objfile->global_psymbols.next,
                                              objfile->static_psymbols.next);
                }

              /* Compute address of the data symbol */
              valu = dn_bufp->dsvar.location;
              /* Relocate in case it's in a shared library */
              if (storage == LOC_STATIC)
                valu += ANOFFSET (objfile->section_offsets,
				  SECT_OFF_DATA (objfile));

              /* Luckily, dvar, svar, typedef, and tagdef all
               * have their "global" bit in the same place, so it works
               * (though it's bad programming practice) to reference
               * "dsvar.global" even though we may be looking at
               * any of the above four types.
               */
              if (dn_bufp->dsvar.global)
                {
                  /* RM: Check if symbol is already present in a
                     "globals" pst from a later chapter. If so, stomp
                     on this symbol so that psymtab expansion ignores
                     it */
                  if (!hpread_symbol_overridden (objfile, namestring, namespace))
                    add_psymbol_to_list (namestring, strlen (namestring),
                                         namespace, storage,
                                         &objfile->global_psymbols,
                                         valu,
                                         0, language_unknown, objfile,
					 hp_symnum);
                  else
                    dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
                }
              else
		add_psymbol_to_list (namestring, strlen (namestring),
				     namespace, storage,
				     &objfile->static_psymbols,
				     valu,
				     0, language_unknown, objfile,
				     hp_symnum);
              
              /* For TAGDEF's, the above code added the tagname to the
               * struct namespace. This will cause tag "t" to be found
               * on a reference of the form "(struct t) x". But for
               * C++ classes, "t" will also be a typename, which we
               * want to find on a reference of the form "ptype t".
               * Therefore, we also add "t" to the var namespace.
               * Do the same for enum's due to the way aCC generates
               * debug info for these (see more extended comment
               * in hp-symtab-read.c).
               * We do the same for templates, so that "ptype t"
               * where "t" is a template also works.
               */
              if (dn_bufp->dblock.kind == DNTT_TYPE_TAGDEF &&
		  dn_bufp->dtype.type.dnttp.index < LNTT_SYMCOUNT (objfile))
                {
                  int global = dn_bufp->dtag.global;
                  /* Look ahead to see if it's a C++ class */
                  dn_bufp = hpread_get_lntt (dn_bufp->dtype.type.dnttp.index, objfile);
                  if (dn_bufp->dblock.kind == DNTT_TYPE_CLASS ||
                      dn_bufp->dblock.kind == DNTT_TYPE_ENUM ||
                      dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
                    {
                      if (global)
                        {
                          /* RM: Check if symbol is already present in a
                             "globals" pst from a later chapter. If so, stomp
                             on this symbol so that psymtab expansion ignores
                             it */
                          if (!hpread_symbol_overridden (objfile, namestring, VAR_NAMESPACE))
                            add_psymbol_to_list (namestring, strlen (namestring),
                                                 VAR_NAMESPACE, storage,
                                                 &objfile->global_psymbols,
                                                 dn_bufp->dsvar.location,
                                                 0, language_unknown, objfile,
						 hp_symnum);
                          else
                            dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
                        }
                      else
			add_psymbol_to_list (namestring, strlen (namestring),
					     VAR_NAMESPACE, storage,
					     &objfile->static_psymbols,
					     dn_bufp->dsvar.location,
					     0, language_unknown, objfile,
					     hp_symnum);
                    }
                }
            }
            continue;

          case DNTT_TYPE_MEMENUM:
          case DNTT_TYPE_CONST:
            /* Constants and members of enumerated types.  */
            if (dn_bufp->dblock.kind == DNTT_TYPE_CONST)
              {
                  ADJUST_DNTT(dn_bufp->dconst.type)
              }
            else
              {
                ADJUST_DNTT(dn_bufp->dmember.nextmem)
              }
            
            SET_NAMESTRING (dn_bufp, &namestring, objfile);

            if (!pst)
	      pst = hpread_start_psymtab (objfile,
					  "globals", 0,
					  (hp_symnum
					   * sizeof (struct dntt_type_block)),
					  objfile->global_psymbols.next,
					  objfile->static_psymbols.next);

            if (dn_bufp->dconst.global)
	      {
		/* RM: Check if symbol is already present in a
		   "globals" pst from a later chapter. If so, stomp
		   on this symbol so that psymtab expansion ignores
		   it */
		if (!hpread_symbol_overridden (objfile, namestring, VAR_NAMESPACE))
		  add_psymbol_to_list (namestring, strlen (namestring),
				       VAR_NAMESPACE, LOC_CONST,
				       &objfile->global_psymbols, 0,
				       0, language_unknown, objfile,
				       hp_symnum);
		else
		  dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
	      }
            else
              add_psymbol_to_list (namestring, strlen (namestring),
                                   VAR_NAMESPACE, LOC_CONST,
                                   &objfile->static_psymbols, 0,
                                   0, language_unknown, objfile,
				   hp_symnum);
            continue;

          case DNTT_TYPE_FPARAM:
            ADJUST_DNTT(dn_bufp->dfparam.type)
            ADJUST_DNTT(dn_bufp->dfparam.nextparam)
            continue;

          case DNTT_TYPE_POINTER:
          case DNTT_TYPE_REFERENCE:
            ADJUST_DNTT(dn_bufp->dptr.pointsto)
            continue;
            
          case DNTT_TYPE_ENUM:
            ADJUST_DNTT(dn_bufp->denum.firstmem)
            continue;
            
          case DNTT_TYPE_SET:
            ADJUST_DNTT(dn_bufp->dset.subtype)
            continue;
            
          case DNTT_TYPE_SUBRANGE:
            ADJUST_DNTT(dn_bufp->dsubr.subtype)
            continue;

          case DNTT_TYPE_ARRAY:
            ADJUST_DNTT(dn_bufp->darray.indextype)
            ADJUST_DNTT(dn_bufp->darray.elemtype)
            continue;

          case DNTT_TYPE_STRUCT:
            ADJUST_DNTT(dn_bufp->dstruct.firstfield)
            ADJUST_DNTT(dn_bufp->dstruct.vartagfield)
            ADJUST_DNTT(dn_bufp->dstruct.varlist)
            continue;
                      
          case DNTT_TYPE_UNION:
            ADJUST_DNTT(dn_bufp->dunion.firstfield)
            continue;
            
          case DNTT_TYPE_FIELD:
            ADJUST_DNTT(dn_bufp->dfield.type)
            ADJUST_DNTT(dn_bufp->dfield.nextfield)
            continue;

          case DNTT_TYPE_FUNCTYPE:
            ADJUST_DNTT(dn_bufp->dfunctype.firstparam)
            ADJUST_DNTT(dn_bufp->dfunctype.retval)
            continue;

          case DNTT_TYPE_WITH:
            ADJUST_SLT(dn_bufp->dwith.address)
            ADJUST_DNTT(dn_bufp->dwith.type)
            continue;

          /* RM: No common in hp-symtab64.h?? */
#if 0            
          case DNTT_TYPE_COMMON:
            ADJUST_VT(dn_bufp->dcommon.alias)
            continue;
#endif
          case DNTT_TYPE_BLOCKDATA:
            ADJUST_VT(dn_bufp->dblockdata.alias)
            ADJUST_SLT(dn_bufp->dblockdata.address)
            ADJUST_DNTT(dn_bufp->dblockdata.firstparam)
            ADJUST_DNTT(dn_bufp->dblockdata.retval)
            continue;
            
          case DNTT_TYPE_CLASS_SCOPE:
            ADJUST_SLT(dn_bufp->dclass_scope.address)
            ADJUST_DNTT(dn_bufp->dclass_scope.type)
            continue;
            
          case DNTT_TYPE_PTRMEM:
            ADJUST_DNTT(dn_bufp->dptrmem.pointsto)
            ADJUST_DNTT(dn_bufp->dptrmem.memtype)
            continue;

          case DNTT_TYPE_PTRMEMFUNC:
            ADJUST_DNTT(dn_bufp->dptrmemfunc.pointsto)
            ADJUST_DNTT(dn_bufp->dptrmemfunc.memtype)
            continue;

          case DNTT_TYPE_CLASS:
            ADJUST_DNTT(dn_bufp->dclass.memberlist)
            ADJUST_DNTT(dn_bufp->dclass.parentlist)
            ADJUST_DNTT(dn_bufp->dclass.identlist)
            ADJUST_DNTT(dn_bufp->dclass.friendlist)
            ADJUST_DNTT(dn_bufp->dclass.templateptr)
            ADJUST_DNTT(dn_bufp->dclass.nextexp)
            continue;
            
          case DNTT_TYPE_GENFIELD:
            ADJUST_DNTT(dn_bufp->dgenfield.field)
            ADJUST_DNTT(dn_bufp->dgenfield.nextfield)
            continue;
            
          case DNTT_TYPE_VFUNC:
            ADJUST_DNTT(dn_bufp->dvfunc.funcptr)
            continue;
            
          case DNTT_TYPE_MEMACCESS:
            ADJUST_DNTT(dn_bufp->dmemaccess.classptr)
            ADJUST_DNTT(dn_bufp->dmemaccess.field)
            continue;

          case DNTT_TYPE_INHERITANCE:
            ADJUST_DNTT(dn_bufp->dinheritance.classname)
            ADJUST_DNTT(dn_bufp->dinheritance.next)
            continue;

          case DNTT_TYPE_FRIEND_CLASS:
            ADJUST_DNTT(dn_bufp->dfriend_class.classptr)
            ADJUST_DNTT(dn_bufp->dfriend_class.next)
            continue;
            
          case DNTT_TYPE_FRIEND_FUNC:
            ADJUST_DNTT(dn_bufp->dfriend_func.funcptr)
            ADJUST_DNTT(dn_bufp->dfriend_func.classptr)
            ADJUST_DNTT(dn_bufp->dfriend_func.next)
            continue;
            
          case DNTT_TYPE_MODIFIER:
            ADJUST_DNTT(dn_bufp->dmodifier.type)
            continue;

          case DNTT_TYPE_OBJECT_ID:
            ADJUST_DNTT(dn_bufp->dobject_id.next)
            continue;
            
          case DNTT_TYPE_TEMPLATE:
            ADJUST_DNTT(dn_bufp->dtemplate.memberlist)
            ADJUST_DNTT(dn_bufp->dtemplate.parentlist)
            ADJUST_DNTT(dn_bufp->dtemplate.identlist)
            ADJUST_DNTT(dn_bufp->dtemplate.friendlist)
            ADJUST_DNTT(dn_bufp->dtemplate.arglist)
            ADJUST_DNTT(dn_bufp->dtemplate.expansions)
            continue;

          case DNTT_TYPE_TEMPLATE_ARG:
            ADJUST_DNTT(dn_bufp->dtempl_arg.type)
            ADJUST_DNTT(dn_bufp->dtempl_arg.nextarg)
            continue;
            
          case DNTT_TYPE_FUNC_TEMPLATE:
            ADJUST_VT(dn_bufp->dfunc_template.alias)
            ADJUST_DNTT(dn_bufp->dfunc_template.firstparam)
            ADJUST_DNTT(dn_bufp->dfunc_template.retval)
            ADJUST_DNTT(dn_bufp->dfunc_template.arglist)
            continue;

          case DNTT_TYPE_LINK:
            ADJUST_DNTT(dn_bufp->dlink.ptr1)
            ADJUST_DNTT(dn_bufp->dlink.ptr2)
            continue;

          case DNTT_TYPE_DYN_ARRAY_DESC:
            ADJUST_DNTT(dn_bufp->darray_desc.elemtype)
            ADJUST_DNTT(dn_bufp->darray_desc.indextype)
            continue;
            
          case DNTT_TYPE_DESC_SUBRANGE:
            ADJUST_DNTT(dn_bufp->ddesc_subr.subtype)
            ADJUST_DNTT(dn_bufp->ddesc_subr.indextype)
            continue;
            
          case DNTT_TYPE_BEGIN:
            ADJUST_SLT(dn_bufp->dbegin.address)
            continue;

          default:
            continue;
        }
    }

  /* End any pending partial symbol table.
   */
  if (pst)
    {
      hpread_end_psymtab (pst, 0, 0,
			  hp_symnum * sizeof (struct dntt_type_block),
			  texthigh, 0, 0);
    }  
}

/* Scan and build partial symbols for a symbol file.

   The minimal symbol table (either SOM or HP a.out) has already been
   read in; all we need to do is setup partial symbols based on the
   native debugging information.

   Note that the minimal table is produced by the linker, and has
   only global routines in it; the psymtab is based on compiler-
   generated debug information and has non-global
   routines in it as well as files and class information.

   We assume hpread_symfile_init has been called to initialize the
   symbol reader's private data structures.

   MAINLINE is true if we are reading the main symbol table (as
   opposed to a shared lib or dynamically loaded file). */

void
hpread_build_psymtabs (objfile, mainline)
     struct objfile *objfile;
     int mainline;
{
  /* in somread.c */
  extern void build_doom_tables(struct objfile *);

#ifdef DUMPING
  /* Turn this on to get debugging output. */
  static int dumping = 0;
#endif

  char *namestring;
  int past_first_source_file = 0;
  struct cleanup *old_chain;

  int hp_symnum, symcount, i;
  int scan_start = 0;

  union dnttentry *dn_bufp;
  CORE_ADDR valu;
  char *p;
  CORE_ADDR texthigh = 0;
  int have_name = 0;

  /* Current partial symtab */
  struct partial_symtab *pst;

  /* List of current psymtab's include files */
  char **psymtab_include_list;
  int includes_allocated;
  int includes_used;

  /* Index within current psymtab dependency list */
  struct partial_symtab **dependency_list;
  int dependencies_used, dependencies_allocated;

  /* Just in case the stabs reader left turds lying around.  */
  free_pending_blocks ();
  make_cleanup (really_free_pendings, 0);

  pst = (struct partial_symtab *) 0;

  /* We shouldn't use alloca, instead use malloc/free.  Doing so avoids
     a number of problems with cross compilation and creating useless holes
     in the stack when we have to allocate new entries.  FIXME.  */

  includes_allocated = 30;
  includes_used = 0;
  psymtab_include_list = (char **) alloca (includes_allocated *
					   sizeof (char *));

  dependencies_allocated = 30;
  dependencies_used = 0;
  dependency_list =
    (struct partial_symtab **) alloca (dependencies_allocated *
				       sizeof (struct partial_symtab *));

  /* RM: actually this has already been inserted into the cleanup
     chain in syms_from_objfile */
#if 0  
  old_chain = make_cleanup_free_objfile (objfile);
#endif

  last_source_file = 0;

  if (doom_executable || N_CHAPTERS (objfile) > 1)
    build_doom_tables (objfile);
  
  /* Process incrementally linked chapters in reverse order */
  for (i = N_CHAPTERS (objfile) - 1; i > 0; i--)
    {
      CURRENT_CHAPTER (objfile) = i;
      hpread_build_ild_psymtabs (objfile, mainline);
    }

  CURRENT_CHAPTER (objfile) = 0;
      
  if (N_CHAPTERS (objfile) == 0)
    goto make_doom_psymtabs;

#ifdef QUICK_LOOK_UP
  if (! hpread_pxdb_needed (objfile->obfd))
    {
      /* Begin code for new-style loading of quick look-up tables. */

      /* elz: this checks whether the file has beeen processed by pxdb.
	 If not we would like to try to read the psymbols in
	 anyway, but it turns out to be not so easy. So this could 
	 actually be commented out, but I leave it in, just in case
	 we decide to add support for non-pxdb-ed stuff in the future. */
      PXDB_header pxdb_header;
      int found_modules_in_program;

      if (hpread_get_header (objfile, &pxdb_header))
        {
	  /* Build a minimal table.  No types, no global variables,
	     no include files.... */
#ifdef DUMPING
	  if (dumping)
	    printf ("\nNew method for %s\n", objfile->name);
#endif

	  /* elz: quick_traverse returns true if it found
	   some modules in the main source file, other
	   than those in end.c
	   In C and C++, all the files have MODULES entries
	   in the LNTT, and the quick table traverse is all 
	   based on finding these MODULES entries. Without 
	   those it cannot work. 
	   It happens that F77 programs don't have MODULES
	   so the quick traverse gets confused. F90 programs
	   have modules, and the quick method still works.
	   So, if modules (other than those in end.c) are
	   not found we give up on the quick table stuff, 
	   and fall back on the slower method  */
	  found_modules_in_program = hpread_quick_traverse (objfile,
							    GNTT (objfile),
							    VT (objfile),
							    &pxdb_header);

#if 0
	  discard_cleanups (old_chain);
#endif

	  /* Set up to scan the global section of the LNTT.

	   This field is not always correct: if there are
	   no globals, it will point to the last record in
	   the regular LNTT, which is usually an END MODULE.

	   Since it might happen that there could be a file
	   with just one global record, there's no way to
	   tell other than by looking at the record, so that's
	   done below. */
	  if (found_modules_in_program)
	    scan_start = pxdb_header.globals;
	}
#ifdef DUMPING
      else
        {
	  if (dumping)
	    printf ("\nGoing on to old method for %s\n", objfile->name);
	}
#endif
    }
  /* RM: it's now possible that we reach here: if we have a
     non-pxdb'd DOOM executable. We don't want to try and run pxdb
     in this case, because it may be the case that end.o is the only
     non-DOOM object. One of the big reasons for DOOM is that we want
     to avoid pxdb in the first place. */
#endif /* QUICK_LOOK_UP */

  if (N_CHAPTERS (objfile) <= 1)
    {
      /* srikanth, 000122, die, die, die ! fake GNTT */
      free (GNTT (objfile));
      GNTT (objfile) = 0;
    }

  /* Make two passes, one over the GNTT symbols, the other for the
     LNTT symbols.

     JB comment: above isn't true--they only make one pass, over
     the LNTT.

     In reality the lone pass is made over the the GNTT. This
     unfortunate confusion stems from the fact that after pxdb is run,
     in the resultant binary the term GNTT refers to the quick lookup
     tables and LNTT refers to the combined "real" GNTT and LNTT. The 
     original GNTT has been appended to the original LNTT and the combo
     is christened LNTT. -- srikanth, 991111. */

  for (i = 0; i < 1; i++)
    {
      int within_function = 0;
      unsigned dntt_index;

      if (i)
	symcount = GNTT_SYMCOUNT (objfile);
      else
	symcount = LNTT_SYMCOUNT (objfile);


      for (hp_symnum = scan_start; hp_symnum < symcount; hp_symnum++)
	{
	  QUIT;
	  if (i)
	    dn_bufp = hpread_get_gntt (hp_symnum, objfile);
	  else
	    dn_bufp = hpread_get_lntt (hp_symnum, objfile);

	  if (dn_bufp->dblock.extension)
	    continue;

	  /* Only handle things which are necessary for minimal symbols.
	     everything else is ignored.  */
	  switch (dn_bufp->dblock.kind)
	    {
	    case DNTT_TYPE_SRCFILE:
	      {
#ifdef QUICK_LOOK_UP
		if (scan_start == hp_symnum
		    && symcount == hp_symnum + 1)
		  {
		    /* If there are NO globals in an executable,
		       PXDB's index to the globals will point to
		       the last record in the file, which 
		       could be this record. (this happened for F77 libraries)
		       ignore it and be done! */
		    continue;
		  }
#endif /* QUICK_LOOK_UP */

		/* A source file of some kind.  Note this may simply
		   be an included file.  */
		SET_NAMESTRING (dn_bufp, &namestring, objfile);

		/* Check if this is the source file we are already working
		   with.  */
		if (pst && !strcmp (namestring, pst->filename))
		  continue;

		/* Check if this is an include file, if so check if we have
		   already seen it.  Add it to the include list */
		p = strrchr (namestring, '.');
		if (!strcmp (p, ".h"))
		  {
		    int j, found;

		    found = 0;
		    for (j = 0; j < includes_used; j++)
		      if (!strcmp (namestring, psymtab_include_list[j]))
			{
			  found = 1;
			  break;
			}
		    if (found)
		      continue;

		    /* Add it to the list of includes seen so far and
		       allocate more include space if necessary.  */
		    psymtab_include_list[includes_used++] = namestring;
		    if (includes_used >= includes_allocated)
		      {
			char **orig = psymtab_include_list;

			psymtab_include_list = (char **)
			  alloca ((includes_allocated *= 2) *
				  sizeof (char *));
			memcpy ((PTR) psymtab_include_list, (PTR) orig,
				includes_used * sizeof (char *));
		      }
		    continue;
		  }

		if (pst)
		  {
		    if (!have_name)
		      {
			pst->filename = (char *)
			  obstack_alloc (&pst->objfile->psymbol_obstack,
					 strlen (namestring) + 1);
			strcpy (pst->filename, namestring);
			have_name = 1;
			continue;
		      }
		    continue;
		  }

		/* This is a bonafide new source file.
		   End the current partial symtab and start a new one.  */

		if (pst && past_first_source_file)
		  {
		    hpread_end_psymtab (pst, psymtab_include_list,
					includes_used,
					(hp_symnum
					 * sizeof (struct dntt_type_block)),
					texthigh,
					dependency_list, dependencies_used);
		    pst = (struct partial_symtab *) 0;
		    includes_used = 0;
		    dependencies_used = 0;
		  }
		else
		  past_first_source_file = 1;

		valu = hpread_get_textlow (i, hp_symnum, objfile, symcount);
		valu += ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
		pst = hpread_start_psymtab (objfile,
					    namestring, valu,
					    (hp_symnum
					 * sizeof (struct dntt_type_block)),
					    objfile->global_psymbols.next,
					    objfile->static_psymbols.next);
                OFILE_LOAD_STATUS(pst) = DOOM_OFILE_PRELOADED;
		texthigh = valu;
		have_name = 1;
		continue;
	      }

	    case DNTT_TYPE_MODULE:
	      /* A source file.  It's still unclear to me what the
	         real difference between a DNTT_TYPE_SRCFILE and DNTT_TYPE_MODULE
	         is supposed to be.  */

	      /* First end the previous psymtab */
	      if (pst)
		{
		  hpread_end_psymtab (pst, psymtab_include_list, includes_used,
				      ((hp_symnum - 1)
				       * sizeof (struct dntt_type_block)),
				      texthigh,
				      dependency_list, dependencies_used);
		  pst = (struct partial_symtab *) 0;
		  includes_used = 0;
		  dependencies_used = 0;
		  have_name = 0;
		}

	      /* Now begin a new module and a new psymtab for it */
	      SET_NAMESTRING (dn_bufp, &namestring, objfile);
	      valu = hpread_get_textlow (i, hp_symnum, objfile, symcount);
	      valu += ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
	      if (!pst)
		{
		  pst = hpread_start_psymtab (objfile,
					      namestring, valu,
					      (hp_symnum
					 * sizeof (struct dntt_type_block)),
					      objfile->global_psymbols.next,
					      objfile->static_psymbols.next);
		  texthigh = valu;
		  have_name = 0;
		}
	      continue;

	    case DNTT_TYPE_FUNCTION:
	    case DNTT_TYPE_ENTRY:
	      /* The beginning of a function.  DNTT_TYPE_ENTRY may also denote
	         a secondary entry point.  */
	      valu = dn_bufp->dfunc.hiaddr + ANOFFSET (objfile->section_offsets,
						       SECT_OFF_TEXT (objfile));
	      if (valu > texthigh)
		texthigh = valu;
	      valu = dn_bufp->dfunc.lowaddr +
		ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
	      SET_NAMESTRING (dn_bufp, &namestring, objfile);
	      if (dn_bufp->dfunc.global)
	        {
		  /* RM: Check if symbol is already present in a
		     "globals" pst from a later chapter. If so, stomp
		     on this symbol so that psymtab expansion ignores
		     it */
		  if (!hpread_symbol_overridden (objfile, namestring, VAR_NAMESPACE))
		    add_psymbol_to_list (namestring, strlen (namestring),
					 VAR_NAMESPACE, LOC_BLOCK,
					 &objfile->global_psymbols, valu,
					 0, language_unknown, objfile,
					 hp_symnum);
		  else
		    dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
		}
              else
                add_psymbol_to_list (namestring, strlen (namestring),
				     VAR_NAMESPACE, LOC_BLOCK, 
				     &objfile->static_psymbols, valu, 
				     0, language_unknown, objfile,
				     hp_symnum);
	      within_function = 1;
	      continue;

	    case DNTT_TYPE_DOC_FUNCTION:
	      valu = dn_bufp->ddocfunc.hiaddr + ANOFFSET (objfile->section_offsets,
							  SECT_OFF_TEXT (objfile));
	      if (valu > texthigh)
		texthigh = valu;
	      valu = dn_bufp->ddocfunc.lowaddr +
		ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
	      SET_NAMESTRING (dn_bufp, &namestring, objfile);
	      if (dn_bufp->ddocfunc.global)
	        {
		  /* RM: Check if symbol is already present in a
		     "globals" pst from a later chapter. If so, stomp
		     on this symbol so that psymtab expansion ignores
		     it */
		  if (!hpread_symbol_overridden (objfile, namestring, VAR_NAMESPACE))
		    add_psymbol_to_list (namestring, strlen (namestring),
					 VAR_NAMESPACE, LOC_BLOCK,
					 &objfile->global_psymbols, valu,
					 0, language_unknown, objfile,
					 hp_symnum);
		  else
		    dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
		}
              else
                add_psymbol_to_list (namestring, strlen (namestring),
				     VAR_NAMESPACE, LOC_BLOCK,
				     &objfile->static_psymbols, valu,
				     0, language_unknown, objfile,
				     hp_symnum);
	      within_function = 1;
	      continue;

	    case DNTT_TYPE_BEGIN:
	    case DNTT_TYPE_END:
	      /* We don't check MODULE end here, because there can be
	         symbols beyond the module end which properly belong to the
	         current psymtab -- so we wait till the next MODULE start */


#ifdef QUICK_LOOK_UP
	      if (scan_start == hp_symnum
		  && symcount == hp_symnum + 1)
		{
		  /* If there are NO globals in an executable,
		     PXDB's index to the globals will point to
		     the last record in the file, which is
		     probably an END MODULE, i.e. this record.
		     ignore it and be done! */
		  continue;
		}
#endif /* QUICK_LOOK_UP */

	      /* Scope block begin/end.  We only care about function
	         and file blocks right now.  */

	      if ((dn_bufp->dend.endkind == DNTT_TYPE_FUNCTION) ||
		  (dn_bufp->dend.endkind == DNTT_TYPE_DOC_FUNCTION))
		within_function = 0;
	      continue;

	    case DNTT_TYPE_SVAR:
	    case DNTT_TYPE_DVAR:
	    case DNTT_TYPE_TYPEDEF:
	    case DNTT_TYPE_TAGDEF:
	      {
		/* Variables, typedefs and the like.  */
		enum address_class storage;
		namespace_enum namespace;

		/* Don't add locals to the partial symbol table.  */
		if (within_function
		    && (dn_bufp->dblock.kind == DNTT_TYPE_SVAR
			|| dn_bufp->dblock.kind == DNTT_TYPE_DVAR))
		  continue;

		/* TAGDEFs go into the structure namespace.  */
		if (dn_bufp->dblock.kind == DNTT_TYPE_TAGDEF)
		  namespace = STRUCT_NAMESPACE;
		else
		  namespace = VAR_NAMESPACE;

		/* What kind of "storage" does this use?  */
		if (dn_bufp->dblock.kind == DNTT_TYPE_SVAR)
		  storage = LOC_STATIC;
		else if (dn_bufp->dblock.kind == DNTT_TYPE_DVAR
			 && dn_bufp->ddvar.regvar)
		  storage = LOC_REGISTER;
		else if (dn_bufp->dblock.kind == DNTT_TYPE_DVAR)
		  storage = LOC_LOCAL;
		else
		  storage = LOC_UNDEF;

		SET_NAMESTRING (dn_bufp, &namestring, objfile);
		if (!pst)
		  {
		    pst = hpread_start_psymtab (objfile,
						"globals", 0,
						(hp_symnum
					 * sizeof (struct dntt_type_block)),
					      objfile->global_psymbols.next,
					     objfile->static_psymbols.next);
		  }

		/* Compute address of the data symbol */
		valu = dn_bufp->dsvar.location;
		/* Relocate in case it's in a shared library */
		if (storage == LOC_STATIC)
		  valu += ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile));

		/* Luckily, dvar, svar, typedef, and tagdef all
		   have their "global" bit in the same place, so it works
		   (though it's bad programming practice) to reference
		   "dsvar.global" even though we may be looking at
		   any of the above four types. */
		if (dn_bufp->dsvar.global)
		  {
                    /* RM: Check if symbol is already present in a
                       "globals" pst from a later chapter. If so, stomp
                       on this symbol so that psymtab expansion ignores
                       it */
                    if (!hpread_symbol_overridden (objfile, namestring, namespace))
                      add_psymbol_to_list (namestring, strlen (namestring),
                                           namespace, storage,
                                           &objfile->global_psymbols,
                                           valu,
                                           0, language_unknown, objfile,
					   hp_symnum);
                    else
                      dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
		  }
		else
		  add_psymbol_to_list (namestring, strlen (namestring),
				       namespace, storage,
				       &objfile->static_psymbols,
				       valu,
				       0, language_unknown, objfile,
				       hp_symnum);

		/* For TAGDEF's, the above code added the tagname to the
		   struct namespace. This will cause tag "t" to be found
		   on a reference of the form "(struct t) x". But for
		   C++ classes, "t" will also be a typename, which we
		   want to find on a reference of the form "ptype t".
		   Therefore, we also add "t" to the var namespace.
		   Do the same for enum's due to the way aCC generates
		   debug info for these (see more extended comment
		   in hp-symtab-read.c).
		   We do the same for templates, so that "ptype t"
		   where "t" is a template also works. */
		if (dn_bufp->dblock.kind == DNTT_TYPE_TAGDEF &&
		  dn_bufp->dtype.type.dnttp.index < LNTT_SYMCOUNT (objfile))
		  {
		    int global = dn_bufp->dtag.global;
		    /* Look ahead to see if it's a C++ class */
		    dn_bufp = hpread_get_lntt (dn_bufp->dtype.type.dnttp.index, objfile);
		    if (dn_bufp->dblock.kind == DNTT_TYPE_CLASS ||
			dn_bufp->dblock.kind == DNTT_TYPE_ENUM ||
			dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
		      {
			if (global)
			  {
			    /* RM: Check if symbol is already present in a
			       "globals" pst from a later chapter. If so, stomp
			       on this symbol so that psymtab expansion ignores
			       it */
                            if (!hpread_symbol_overridden (objfile, namestring, VAR_NAMESPACE))
                              add_psymbol_to_list (namestring, strlen (namestring),
                                                   VAR_NAMESPACE, storage,
                                                   &objfile->global_psymbols,
                                                   dn_bufp->dsvar.location,
                                                   0, language_unknown,
						   objfile, hp_symnum);
                            else
                              dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
			  }
			else
			  add_psymbol_to_list (namestring,
					       strlen (namestring),
					       VAR_NAMESPACE, storage,
					       &objfile->static_psymbols,
					       dn_bufp->dsvar.location,
					       0, language_unknown, 
					       objfile, hp_symnum);
		      }
		  }
	      }
	      continue;

	    case DNTT_TYPE_ENUM:

              if (!incremental_expansion)
                continue;

	      if (!pst)
                pst = hpread_start_psymtab (objfile,
                                            "globals", 0,
                                            (hp_symnum
                                             * sizeof (struct dntt_type_block)),
                                            objfile->global_psymbols.next,
                                            objfile->static_psymbols.next);
              {
                dnttpointer mem;
                union dnttentry *memp;

                /* srikanth, 000124, for each enumerator, record the
                   dntt index of the enclosing enumeration. This will
                   cause the whole enumeration to be internalized at
                   one stroke.
                */

                mem = dn_bufp->denum.firstmem;
                while (mem.word && mem.word != DNTTNIL)
                  {
                    memp = hpread_get_lntt (mem.dnttp.index, objfile);

	            SET_NAMESTRING (memp, &namestring, objfile);
                    /* enumerators don't have a global bit, so ... */
		    add_psymbol_to_list (namestring, strlen (namestring),
					 VAR_NAMESPACE, LOC_CONST,
					 &objfile->global_psymbols, 0,
					 0, language_unknown, 
					 objfile, hp_symnum);
                    mem = memp->dmember.nextmem;
                  }
              }

	      continue;

	    case DNTT_TYPE_MEMENUM:

              if (incremental_expansion)
                continue;  /* will be processed on seeing the ENUM */

              /* fall thro ... */

	    case DNTT_TYPE_CONST:
	      /* Constants and members of enumerated types.  */
	      SET_NAMESTRING (dn_bufp, &namestring, objfile);
	      if (!pst)
		{
		  pst = hpread_start_psymtab (objfile,
					      "globals", 0,
					      (hp_symnum
					 * sizeof (struct dntt_type_block)),
					      objfile->global_psymbols.next,
					      objfile->static_psymbols.next);
		}
	      if (dn_bufp->dconst.global)
	        {
		  /* RM: Check if symbol is already present in a
		     "globals" pst from a later chapter. If so, stomp
		     on this symbol so that psymtab expansion ignores
		     it */
		  if (!hpread_symbol_overridden (objfile, namestring, VAR_NAMESPACE))
		    add_psymbol_to_list (namestring, strlen (namestring),
					 VAR_NAMESPACE, LOC_CONST,
					 &objfile->global_psymbols, 0,
					 0, language_unknown, objfile,
					 hp_symnum);
		  else
		    dn_bufp->dblock.kind = DNTT_TYPE_HOLE1;
		}
	      else
                add_psymbol_to_list (namestring, strlen (namestring),
				     VAR_NAMESPACE, LOC_CONST,
				     &objfile->static_psymbols, 0,
				     0, language_unknown, 
                                     objfile, hp_symnum);
	      continue;
	    default:
	      continue;
	    }
	}
    }

  /* End any pending partial symbol table. */
  if (pst)
    hpread_end_psymtab (pst, psymtab_include_list, includes_used,
         	        hp_symnum * sizeof (struct dntt_type_block),
			0, dependency_list, dependencies_used);

make_doom_psymtabs:
  /* RM: Are we reading debug from object files?
   */
  if (doom_executable) {
      hpread_build_doom_psymtabs(objfile, objfile->section_offsets, mainline);
  }

#if 0
  discard_cleanups (old_chain);
#endif  
}

/* Perform any local cleanups required when we are done with a particular
   objfile.  I.E, we are in the process of discarding all symbol information
   for an objfile, freeing up all memory held for it, and unlinking the
   objfile struct from the global list of known objfiles. */

void
hpread_symfile_finish (objfile)
     struct objfile *objfile;
{
  int i;
  
  if (objfile->sym_private != NULL)
    {
       /* srikanth, 990311, JAGaa80452, don't forget to forget ... */
      for (i = 0; i < N_CHAPTERS (objfile); i++)
        if (HPUX_SYMFILE_INFO(objfile)->type_vector[i])
          mfree(objfile->md, HPUX_SYMFILE_INFO(objfile)->type_vector[i]);
      if (SUBSPACEMAP(objfile))
        free(SUBSPACEMAP(objfile));
      if (COMPMAP(objfile))
        free(COMPMAP(objfile));
      if (STRINGS(objfile))
        free(STRINGS(objfile));
     mfree (objfile->md, objfile->sym_private);
    }
}


/* The remaining functions are all for internal use only.  */

/* Various small functions to get entries in the debug symbol sections.  */

/* srikanth, 000121, routines to demand page the lntt space */

static void
initialize_lntt_page_table (objfile)
     struct objfile *objfile;
{

  int i;
  lntt_page_frame *page_table;
  asection *lntt_section;

  LNTT_PAGE_TABLE (objfile) =
    page_table = (lntt_page_frame *)
                       obstack_alloc (&objfile->symbol_obstack,
		           LNTT_PAGE_COUNT * sizeof (lntt_page_frame));

  /* allocate the pages upfront, it is no biggie */

  for (i = 0; i < LNTT_PAGE_COUNT; i++)
    {

     /* Obsucrity alert ! We cannot simply allocate as many bytes as
        LNTT_ENTRIES_PER_PAGE * sizeof (struct dntt_type_block). This
        is because the last dntt entry could be the beginning of the
        dntt record followed by one or more "extension blocks". We some
        how have to make sure that a dntt entry is not broken across
        pages. So, let us simply allocate and read in 
        sizeof (union dnttentry) bytes extra.
     */

      page_table[i].page = (struct dntt_type_block *)
	obstack_alloc (&objfile->symbol_obstack,
		       LNTT_ENTRIES_PER_PAGE * sizeof (struct dntt_type_block) 
                        + sizeof (union dnttentry));
      page_table[i].page_number = -1;
    }

  LNTT_SECTION (objfile) = lntt_section =
    bfd_get_section_by_name (objfile->obfd, HP_LNTT);

  LNTT_SIZE (objfile) = bfd_section_size (objfile->obfd, lntt_section);

}


static union dnttentry *
lntt_entry_from_page_table (index, objfile)
     int index;
     struct objfile *objfile;
{
  int i, j;

  lntt_page_frame *page_table;
  lntt_page_frame this_page_frame, last_page_frame;
  asection *lntt_section;
  int bytes_to_skip, bytes_to_read;
  int page_number;
  int entry;

  /* do we have the page table set up ? */

  if (LNTT_PAGE_TABLE (objfile) == 0)
    initialize_lntt_page_table (objfile);

  page_table = LNTT_PAGE_TABLE (objfile);

  page_number = LNTT_PAGE_NUMBER (index);
  entry = LNTT_ENTRY_IN_PAGE (index);

  if (page_table[0].page_number == page_number)	  /* common case */
    return (union dnttentry *) &page_table[0].page[entry];

  /* Do we have the desired page in memory ?  As we scan the list of
     pages left to right, looking for the page of interest, we will
     bubble the page frames away from the beginning i.e., the
     "most recently used" slot. This would create a vacuum in the very
     first slot. When we find the page we are looking for, we will move
     that page to this slot. Alternately, if the page is not in memory
     and we have to page it in from disk, we will use this slot as the
     destination. This guarentees that the list is sorted on a MRU basis
     at all times.
  */

  last_page_frame = page_table[0];

  for (i = 1; i < LNTT_PAGE_COUNT; i++)
    {
      this_page_frame = page_table[i];
      page_table[i] = last_page_frame;

      if (page_number == this_page_frame.page_number)
	{
	  page_table[0] = this_page_frame;	/* Make this MRU */
	  return (union dnttentry *) &this_page_frame.page[entry];
	}

      last_page_frame = this_page_frame;
    }

  /* Page fault. At this point, `last_page_frame' points to the least
     recently used page. Discard its contents by paging in the new data
     on top of it. Make the new page the most recently used one ...
  */

  page_table[0] = last_page_frame;

  bytes_to_skip = page_number *
    LNTT_ENTRIES_PER_PAGE * sizeof (struct dntt_type_block);

  bytes_to_read
    = LNTT_ENTRIES_PER_PAGE * sizeof (struct dntt_type_block) + 
               sizeof (union dnttentry);

  lntt_section = LNTT_SECTION (objfile);

  /* last page needs special handling */

  if (bytes_to_skip + bytes_to_read > LNTT_SIZE (objfile))
    bytes_to_read = LNTT_SIZE (objfile) - bytes_to_skip;

  bfd_get_section_contents (objfile->obfd,
			    lntt_section,
			    page_table[0].page,
			    bytes_to_skip,
			    bytes_to_read);

  page_table[0].page_number = page_number;

  return (union dnttentry *) &page_table[0].page[entry];
}

#define DNTT_CACHE_COUNT 1024

static struct dntt_cache {
    union dnttentry dntt_buffer[DNTT_CACHE_COUNT];
    struct dntt_cache * next;
} * dntt_cache = 0;

static int dntt_count = 0;

static union dnttentry *
cached_dntt_entry(union dnttentry * dn_bufp)
{
  if (!dntt_cache || dntt_count == DNTT_CACHE_COUNT)
    {
      struct dntt_cache * new_cache_buffer;
      new_cache_buffer = (struct dntt_cache *)
                             malloc (sizeof(struct dntt_cache));
      if (!new_cache_buffer)
        error ("Out of memory\n");
      new_cache_buffer->next = dntt_cache;
      dntt_cache = new_cache_buffer;
      dntt_count = 0;
    }

   return memcpy(&dntt_cache -> dntt_buffer[dntt_count++],
                  dn_bufp,
                  sizeof(union dnttentry));
}

discard_dntt_cache ()
{
  struct dntt_cache * next;

  dntt_count = 0;
  while (dntt_cache)
    {
      next = dntt_cache -> next;
      free (dntt_cache);
      dntt_cache = next;
    }
}

union dnttentry *
hpread_get_lntt (index, objfile)
     int index;
     struct objfile *objfile;
{
  static union dnttentry nilentry;

  /* index can atmost be 29 bits. There are places in hp-symtab-read.c
     where a dnttpointer is passed to this function instead of
     a dntt index. Masking the top three bits lets us get the index
     from the dnttpointer.
  */

  index &= 0x1fffffff;
  if (index >= LNTT_SYMCOUNT (objfile))
    {
      nilentry.dblock.kind = DNTT_TYPE_NIL;
      return &nilentry;
    }

  if (LNTT (objfile))  /* demand paging is off */
    return (union dnttentry *)
      &(LNTT (objfile)[(index * sizeof (struct dntt_type_block))]);

  if (index >= REAL_GNTT_START (objfile))
    return (union dnttentry *)
      &(REAL_GNTT (objfile)[((index - REAL_GNTT_START (objfile)) *
                              sizeof (struct dntt_type_block))]);

  return cached_dntt_entry (lntt_entry_from_page_table (index, objfile));
}

static union dnttentry *
hpread_get_gntt (index, objfile)
     int index;
     struct objfile *objfile;
{
  return (union dnttentry *)
    &(GNTT (objfile)[(index * sizeof (struct dntt_type_block))]);
}

static void 
initialize_slt_page_table(objfile)
     struct objfile *objfile;
{

  int i;
  slt_page_frame * page_table;
  asection * slt_section;

  SLT_PAGE_TABLE(objfile) = 
               page_table = (slt_page_frame *) 
                             obstack_alloc(&objfile->symbol_obstack,
                                   SLT_PAGE_COUNT * sizeof(slt_page_frame));

  /* allocate the pages upfront, it is no biggie */

  for (i = 0; i < SLT_PAGE_COUNT; i++)
    {
      page_table[i].page = (union sltentry *)
                      obstack_alloc(&objfile->symbol_obstack,
                          SLT_ENTRIES_PER_PAGE * sizeof(union sltentry));
      page_table[i].page_number = -1;
    }

  SLT_SECTION(objfile) = slt_section = 
           bfd_get_section_by_name (objfile->obfd, HP_SLT);

  SLT_SIZE(objfile) = bfd_section_size (objfile->obfd, slt_section);

}

static union sltentry *
slt_entry_from_page_table(index, objfile)
     int index;
     struct objfile *objfile;
{
  int i, j;

  slt_page_frame * page_table;
  slt_page_frame this_page_frame, last_page_frame;
  asection * slt_section;
  int bytes_to_skip, bytes_to_read;

  int page_number;
  int entry;

  /* do we have the page table set up ? */

  if (SLT_PAGE_TABLE(objfile) == 0)
    initialize_slt_page_table(objfile);

  page_table  = SLT_PAGE_TABLE(objfile);

  page_number = SLT_PAGE_NUMBER(index);
  entry = SLT_ENTRY_IN_PAGE (index);

  if (page_table[0].page_number == page_number)  /* common case */
    return &page_table[0].page[entry];

  /* Do we have the desired page in memory ?  As we scan the list of pages
     left to right, looking for the page of interest, we will bubble the page
     frames away from the beginning i.e., the "most recently used" slot. This
     would create a vacuum in the very first slot. When we find the page we
     are looking for, we will move that page to this slot. Alternately, if the
     page is not in memory and we have to page it in from disk, we will use
     this slot as the destination. This guarentees that the list is sorted on
     a MRU basis at all times.
  */

  last_page_frame = page_table[0];

  for (i = 1; i < SLT_PAGE_COUNT; i++)
    {
      this_page_frame  = page_table[i];
      page_table[i] = last_page_frame;

      if (page_number == this_page_frame.page_number)
        {
          page_table[0] = this_page_frame;  /* Make this MRU */
          return &this_page_frame.page[entry];
        }

      last_page_frame = this_page_frame; 
    }

  /* Page fault. At this point, `last_page_frame' points to the least recently
     used page. Discard its contents by paging in the new data on top of it.
     Make the new page the most recently used one ...
  */ 

   page_table[0] = last_page_frame;

   bytes_to_skip = page_number * 
                     SLT_ENTRIES_PER_PAGE * sizeof(union sltentry);

   bytes_to_read = SLT_ENTRIES_PER_PAGE * sizeof(union sltentry);

   slt_section = SLT_SECTION(objfile);

   /* last page needs special handling */

   if (bytes_to_skip + bytes_to_read > SLT_SIZE(objfile))
     bytes_to_read = SLT_SIZE(objfile) - bytes_to_skip;
    
   bfd_get_section_contents (objfile->obfd, 
                             slt_section, 
                             page_table[0].page,
                             bytes_to_skip,
                             bytes_to_read);

   page_table[0].page_number = page_number;
   
   return &page_table[0].page[entry];   /* unwrap the toffee */
}

union sltentry *
hpread_get_slt (index, objfile)
     int index;
     struct objfile *objfile;
{
  if (SLT(objfile))  /* demand paging is off */
    return (union sltentry *) &(SLT (objfile)[index * sizeof (union sltentry)]);

  return slt_entry_from_page_table(index, objfile);
}

#ifdef HPPA_DOC
struct range *
hpread_get_rt (index, objfile)
     int index;
     struct objfile *objfile;
{
  return (struct range *)
    &(RT (objfile)[(index * sizeof (struct range))]);
}
#endif /* HPPA_DOC */

size_t
hpread_get_slt_size (objfile)
     struct objfile *objfile;
{
  if (SLT (objfile))            /* demand paging is off */
    return SLT_SIZE (objfile);

  if (SLT_PAGE_TABLE (objfile) == 0)
    initialize_slt_page_table (objfile);

  return SLT_SIZE (objfile);
}

dst_ln_entry_t *
hpread_get_lines (index, objfile)
     int index;
     struct objfile *objfile;
{
  return (dst_ln_entry_t *) &(LINES (objfile)[index]);
}

/* Get the low address associated with some symbol (typically the start
   of a particular source file or module).  Since that information is not
   stored as part of the DNTT_TYPE_MODULE or DNTT_TYPE_SRCFILE symbol we must infer it from
   the existance of DNTT_TYPE_FUNCTION symbols.  */

static CORE_ADDR
hpread_get_textlow (global, index, objfile, symcount)
     int global;
     int index;
     struct objfile *objfile;
     int symcount;
{
  union dnttentry *dn_bufp = NULL;
  struct minimal_symbol *msymbol;

  /* Look for a DNTT_TYPE_FUNCTION symbol.  */
  if (index < symcount)		/* symcount is the number of symbols in */
    {				/*   the dbinfo, LNTT table */
      do
	{
	  if (global)
	    dn_bufp = hpread_get_gntt (index++, objfile);
	  else
	    dn_bufp = hpread_get_lntt (index++, objfile);
	}
      while (dn_bufp->dblock.kind != DNTT_TYPE_FUNCTION
	     && dn_bufp->dblock.kind != DNTT_TYPE_DOC_FUNCTION
	     && dn_bufp->dblock.kind != DNTT_TYPE_END
	     && index < symcount);
    }

  /* Avoid going past a DNTT_TYPE_END when looking for a DNTT_TYPE_FUNCTION.  This
     might happen when a sourcefile has no functions.  */
  if (dn_bufp->dblock.kind == DNTT_TYPE_END)
    return 0;

  /* Avoid going past the end of the LNTT file */
  if (index == symcount)
    return 0;

  /* The minimal symbols are typically more accurate for some reason.  */
  if (dn_bufp->dblock.kind == DNTT_TYPE_FUNCTION)
    msymbol = lookup_minimal_symbol (dn_bufp->dfunc.name + VT (objfile), NULL,
				     objfile);
  else				/* must be a DNTT_TYPE_DOC_FUNCTION */
    msymbol = lookup_minimal_symbol (dn_bufp->ddocfunc.name + VT (objfile), NULL,
				     objfile);

  if (msymbol)
    return SYMBOL_VALUE_ADDRESS (msymbol);
  else
    return dn_bufp->dfunc.lowaddr;
}

/* Allocate and partially fill a partial symtab.  It will be
   completely filled at the end of the symbol list.

   SYMFILE_NAME is the name of the symbol-file we are reading from, and ADDR
   is the address relative to which its symbols are (incremental) or 0
   (normal). */

static struct partial_symtab *
hpread_start_psymtab (objfile,
		      filename, textlow, ldsymoff, global_syms, static_syms)
     struct objfile *objfile;
     char *filename;
     CORE_ADDR textlow;
     int ldsymoff;
     struct partial_symbol **global_syms;
     struct partial_symbol **static_syms;
{
  CORE_ADDR offset = ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile));
  extern void hpread_psymtab_to_symtab ();
  struct partial_symtab *result =
  start_psymtab_common (objfile, objfile->section_offsets,
			filename, textlow, global_syms, static_syms);

  result->textlow += offset;
  result->texthigh += offset;
  result->read_symtab_private = (char *)
    obstack_alloc (&objfile->psymbol_obstack, sizeof (struct symloc));
  LDSYMOFF (result) = ldsymoff;
  LDCHAPTER (result) = CURRENT_CHAPTER (objfile);
  OVERRIDDEN (result) = 0;
  result->read_symtab = hpread_psymtab_to_symtab;
  result->psymbol_to_expand = 0;

  return result;
}


/* Close off the current usage of PST.  
   Returns PST or NULL if the partial symtab was empty and thrown away.

   capping_symbol_offset  --Byte index in LNTT or GNTT of the
   last symbol processed during the build
   of the previous pst.

   FIXME:  List variables and peculiarities of same.  */

static struct partial_symtab *
hpread_end_psymtab (pst, include_list, num_includes, capping_symbol_offset,
		    capping_text, dependency_list, number_dependencies)
     struct partial_symtab *pst;
     char **include_list;
     int num_includes;
     int capping_symbol_offset;
     CORE_ADDR capping_text;
     struct partial_symtab **dependency_list;
     int number_dependencies;
{
  int i;
  struct objfile *objfile = pst->objfile;
  CORE_ADDR offset = ANOFFSET (pst->section_offsets, SECT_OFF_TEXT (objfile));
  struct partial_symtab *tpst;
  CORE_ADDR pc;
  int subspace_index;

#ifdef DUMPING
  /* Turn on to see what kind of a psymtab we've built. */
  static int dumping = 0;
#endif

  if (capping_symbol_offset != -1)
    LDSYMLEN (pst) = capping_symbol_offset - LDSYMOFF (pst);
  else
    LDSYMLEN (pst) = 0;
  pst->texthigh = capping_text + offset;

  pst->n_global_syms =
    objfile->global_psymbols.next - (objfile->global_psymbols.list + pst->globals_offset);
  pst->n_static_syms =
    objfile->static_psymbols.next - (objfile->static_psymbols.list + pst->statics_offset);

#ifdef DUMPING
  if (dumping)
    {
      printf ("\nPst %s, LDSYMOFF %x (%x), LDSYMLEN %x (%x), globals %d, statics %d\n",
	      pst->filename,
	      LDSYMOFF (pst),
	      LDSYMOFF (pst) / sizeof (struct dntt_type_block),
	      LDSYMLEN (pst),
	      LDSYMLEN (pst) / sizeof (struct dntt_type_block),
	      pst->n_global_syms, pst->n_static_syms);
    }
#endif

  /* RM: If we just built a psymtab for an overridden module in a
     incrementally linked file, discard it.
     RM: FIXME: ??? This is wasteful! Why did we bother creating the entire
     psymtab if we just want to throw it away? */

  /* RM: Is the space used by this module now unused? */
  if (!OVERRIDDEN (pst))
    {
      pc = (pst->textlow+pst->texthigh)/2;
      if (pc)
        {
          if (LDCHAPTER (pst) < N_CHAPTERS (objfile) - 1)
            {
              int objid;
              char *srcname;
              
              subspace_index =  hpread_find_index (objfile, pc, textspace);
              
              if (subspace_index == -1)
                OVERRIDDEN (pst) = 1;
            }
        }
    }

  /* RM: Or did it get reused by another psymtab? */
  if (!OVERRIDDEN (pst))
    {
      ALL_OBJFILE_PSYMTABS (objfile, tpst)
        {
          if ((LDCHAPTER(tpst) > LDCHAPTER(pst)) &&
              (pc >= tpst->textlow && pc < tpst->texthigh))
            {
              OVERRIDDEN (pst) = 1;
              break;
            }
        }
    }
  
  if (OVERRIDDEN (pst))
    {
      discard_psymtab (pst);
      return NULL;
    }

  pst->number_of_dependencies = number_dependencies;
  if (number_dependencies)
    {
      pst->dependencies = (struct partial_symtab **)
	obstack_alloc (&objfile->psymbol_obstack,
		    number_dependencies * sizeof (struct partial_symtab *));
      memcpy (pst->dependencies, dependency_list,
	      number_dependencies * sizeof (struct partial_symtab *));
    }
  else
    pst->dependencies = 0;

  for (i = 0; i < num_includes; i++)
    {
      struct partial_symtab *subpst =
      allocate_psymtab (include_list[i], objfile);

      subpst->section_offsets = pst->section_offsets;
      subpst->read_symtab_private =
	(char *) obstack_alloc (&objfile->psymbol_obstack,
				sizeof (struct symloc));

      /* RM: ??? Needs to be changed for DOOM, fix someday */
      LDSYMOFF (subpst) =
	LDSYMLEN (subpst) =
	subpst->textlow =
	subpst->texthigh = 0;

      /* We could save slight bits of space by only making one of these,
         shared by the entire set of include files.  FIXME-someday.  */
      subpst->dependencies = (struct partial_symtab **)
	obstack_alloc (&objfile->psymbol_obstack,
		       sizeof (struct partial_symtab *));
      subpst->dependencies[0] = pst;
      subpst->number_of_dependencies = 1;

      subpst->globals_offset =
	subpst->n_global_syms =
	subpst->statics_offset =
	subpst->n_static_syms = 0;

      subpst->readin = 0;
      subpst->symtab = 0;
      subpst->read_symtab = pst->read_symtab;
    }

  sort_pst_symbols (pst);

  /* If there is already a psymtab or symtab for a file of this name, remove it.
     (If there is a symtab, more drastic things also happen.)
     This happens in VxWorks.  */
  free_named_symtabs (pst->filename);

  /* srikanth, 990609, we used to check for and discard empty psymtabs here.
     We can't do this anymore, as currently all psymtabs (except for "globals")
     are empty. The lookup scheme has been changed to search the minimal symbol
     table instead and if a linker symbol is found, use its address to decide
     which psymtab to expand. The textlow and texthigh addresses of a psymtab
     envelop the functions contained ...
  */

  return pst;
}
