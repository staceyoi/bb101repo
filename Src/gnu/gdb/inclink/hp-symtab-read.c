/* Read hp debug symbols and convert to internal format, for GDB.
   Copyright 1993, 1996 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Written by the Center for Software Science at the University of Utah
   and by Cygnus Support.  */

/* Common include for hp-symtab-read.c and hp-psymtab-read.c.
 * Note this has nested includes for a bunch of stuff.
 */
#include "defs.h"
#include "limits.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "language.h"
#include "hpread.h"
#include "demangle.h"
#include "complaints.h"
#include "gdbcore.h"


/* External declarations */

extern bfd_vma elf_data_start_vma (bfd *);
extern bfd_vma elf_text_start_vma (bfd *);

extern boolean (*permanent_copy_exists) (struct objfile * objfile,
					 char * string);

static struct complaint hpread_unhandled_end_common_complaint =
{
  "unhandled symbol in hp-symtab-read.c: DNTT_TYPE_COMMON/DNTT_TYPE_END.\n", 0, 0
};

static struct complaint hpread_unhandled_type_complaint =
{
  "hpread_type_translate: unhandled type code.", 0, 0
};

static struct complaint hpread_struct_complaint =
{
  "hpread_read_struct_type: expected SVAR type...", 0, 0
};

static struct complaint hpread_array_complaint =
{
  "error in hpread_array_type.", 0, 0
};

static struct complaint hpread_type_lookup_complaint =
{
  "error in hpread_type_lookup().", 0, 0
};


static struct complaint hpread_unexpected_end_complaint =
{
  "internal error in hp-symtab-read.c: Unexpected DNTT_TYPE_END kind.", 0, 0
};

static struct complaint hpread_tagdef_complaint =
{
  "error processing class tagdef", 0, 0
};

static struct complaint hpread_unhandled_common_complaint =
{
  "unhandled symbol in hp-symtab-read.c: DNTT_TYPE_COMMON.", 0, 0
};

static struct complaint hpread_unhandled_blockdata_complaint =
{
  "unhandled symbol in hp-symtab-read.c: DNTT_TYPE_BLOCKDATA.", 0, 0
};


/* Forward procedure declarations */

static CORE_ADDR hpread_get_scope_start (sltpointer, struct objfile *);

static unsigned long hpread_get_line (sltpointer, struct objfile *);

static CORE_ADDR hpread_get_location (sltpointer, struct objfile *, CORE_ADDR);

static void hpread_psymtab_to_symtab_1 (struct partial_symtab *);

static void hpread_global_psymtab_to_symtab_1 (struct partial_symtab *);

void hpread_psymtab_to_symtab (struct partial_symtab *);

static struct symtab *hpread_expand_symtab
  (struct objfile *, int, int, CORE_ADDR, int,
   struct section_offsets *, char *);

static struct symtab *hpread_expand_global_symtab (struct objfile *, int,
						   int, CORE_ADDR, int,
						   struct section_offsets *,
						   char *,
						   struct partial_symtab *);

static int hpread_type_translate (dnttpointer);

static struct type **hpread_lookup_type (dnttpointer, struct objfile *);

static struct type *hpread_alloc_type (dnttpointer, struct objfile *);

static struct type *hpread_read_enum_type
  (dnttpointer, union dnttentry *, struct objfile *);

static struct type *hpread_read_function_type
  (dnttpointer, union dnttentry *, struct objfile *, int);

static struct type *hpread_read_doc_function_type
  (dnttpointer, union dnttentry *, struct objfile *, int);

#ifdef HPPA_DOC
void make_range_list (int, struct symbol *, struct objfile *);
#endif

static struct type *hpread_read_struct_type
  (dnttpointer, union dnttentry *, struct objfile *);

static struct type *hpread_get_nth_template_arg (struct objfile *, int);

static struct type *hpread_read_templ_arg_type
  (dnttpointer, union dnttentry *, struct objfile *, char *);

static struct type *hpread_read_set_type
  (dnttpointer, union dnttentry *, struct objfile *);

static struct type *hpread_read_array_type
  (dnttpointer, union dnttentry *dn_bufp, struct objfile *objfile);

static struct type *hpread_read_subrange_type
  (dnttpointer, union dnttentry *, struct objfile *);

static struct type *hpread_type_lookup (dnttpointer, struct objfile *);

static void hpread_process_one_debug_symbol
  (union dnttentry *, char *, struct section_offsets *,
   struct objfile *, CORE_ADDR, int, char *, int, int *, int);

static int hpread_get_scope_depth (union dnttentry *, struct objfile *, int);

static void fix_static_member_physnames
  (struct type *, char *, struct objfile *);

static void fixup_class_method_type
  (struct type *, struct type *, struct objfile *);

static void hpread_adjust_bitoffsets (struct type *, int);

static dnttpointer hpread_get_next_skip_over_anon_unions
  (int, dnttpointer, union dnttentry **, struct objfile *);

void hpread_adjust_slt_for_doom (struct objfile *, int);

static struct type * hpread_read_dyn_array_desc_type (dnttpointer,
						      union dnttentry *dn_bufp,
						      struct objfile *objfile);

static struct type *hpread_read_desc_subrange_type (int, dnttpointer,
						    union dnttentry *,
						    struct objfile *);

/* Global to indicate presence of HP-compiled objects,
   in particular, SOM executable file with SOM debug info 
   Defined in symtab.c, used in hppa-tdep.c. */
extern int hp_som_som_object_present;

/* Global to turn off new incremental psymtab expansion 
   behavior. This would be useful as a escape hatch in
   case of problems ...
*/
    int incremental_expansion = 1;  /* enabled on 000124 */

/* Static used to indicate a class type that requires a
   fix-up of one of its method types */
static struct type *fixup_class = NULL;

/* Static used to indicate the method type that is to be
   used to fix-up the type for fixup_class */
static struct type *fixup_method = NULL;

#ifdef GDB_TARGET_IS_HPPA_20W
static int processing_common_block = 0;
#endif

static int static_analysis_warning = 0;


/* RM: static holding the compilation directory for the current
   symtab. Currently only set by DOOM psymtabs */
static void *current_compilation_directory = NULL;
/* static holding the language for the language for the current symtab */
static enum language current_symtab_language = language_unknown;

/* Get the nesting depth for the source line identified by INDEX.  */

static CORE_ADDR
hpread_get_scope_start (index, objfile)
     sltpointer index;
     struct objfile *objfile;
{
  union sltentry *sl_bufp;

  sl_bufp = hpread_get_slt (index, objfile);
  return INDEX(objfile, sl_bufp->sspec.backptr.dnttp);
}

/* Get the source line number the the line identified by INDEX.  */

static unsigned long
hpread_get_line (index, objfile)
     sltpointer index;
     struct objfile *objfile;
{
  union sltentry *sl_bufp;

  sl_bufp = hpread_get_slt (index, objfile);
  return sl_bufp->snorm.line;
}

/* Find the code address associated with a given sltpointer */

static CORE_ADDR
hpread_get_location (index, objfile, offset)
     sltpointer index;
     struct objfile *objfile;
     CORE_ADDR offset;
{
  union sltentry *sl_bufp;
  int i;

  /* code location of special sltentrys is determined from context */
  sl_bufp = hpread_get_slt (index, objfile);

  if (sl_bufp->snorm.sltdesc == SLT_END)
    {
      /* find previous normal sltentry and get address */
      for (i = 0; ((sl_bufp->snorm.sltdesc != SLT_NORMAL) &&
		   (sl_bufp->snorm.sltdesc != SLT_NORMAL_OFFSET) &&
		   (sl_bufp->snorm.sltdesc != SLT_EXIT)); i++)
	sl_bufp = hpread_get_slt (index - i, objfile);
      if (sl_bufp->snorm.sltdesc == SLT_NORMAL_OFFSET)
        return sl_bufp->snormoff.address
# ifdef GDB_TARGET_IS_HPPA_20W
          /* linetable pc changed to offset for PA ELF */
	  /* JYG: function address is already adjusted for shared library
	     text offset, need to adjust it back to provide consistent
	     handling of both SOM and ELF by its callers. */
          + CURRENT_FUNCTION_VALUE(objfile) - offset
# endif
          ;
      else
        return sl_bufp->snorm.address
# ifdef GDB_TARGET_IS_HPPA_20W
          /* linetable pc changed to offset for PA ELF */
	  /* JYG: function address is already adjusted for shared library
	     text offset, need to adjust it back to provide consistent
	     handling of both SOM and ELF by its callers. */
          + CURRENT_FUNCTION_VALUE(objfile) - offset
# endif
          ;
    }

  /* find next normal sltentry and get address */
  for (i = 0; ((sl_bufp->snorm.sltdesc != SLT_NORMAL) &&
	       (sl_bufp->snorm.sltdesc != SLT_NORMAL_OFFSET) &&
	       (sl_bufp->snorm.sltdesc != SLT_EXIT)); i++)
    sl_bufp = hpread_get_slt (index + i, objfile);
  if (sl_bufp->snorm.sltdesc == SLT_NORMAL_OFFSET)
        return sl_bufp->snormoff.address
# ifdef GDB_TARGET_IS_HPPA_20W
          /* linetable pc changed to offset for PA ELF */
	  /* JYG: function address is already adjusted for shared library
	     text offset, need to adjust it back to provide consistent
	     handling of both SOM and ELF by its callers. */
          + CURRENT_FUNCTION_VALUE(objfile) - offset
# endif
          ;
      else
        return sl_bufp->snorm.address
# ifdef GDB_TARGET_IS_HPPA_20W
          /* linetable pc changed to offset for PA ELF */
	  /* JYG: function address is already adjusted for shared library
	     text offset, need to adjust it back to provide consistent
	     handling of both SOM and ELF by its callers. */
          + CURRENT_FUNCTION_VALUE(objfile) - offset
# endif
          ;
}

/* adjust SLT for DOOM -- object modules have not been pxdb'ed, and
 * contain offsets from the beginning of functions, instead of actual
 * addresses
 */
void
hpread_adjust_slt_for_doom(objfile, slt_size)
     struct objfile *objfile;
     int slt_size;
{
  union sltentry *sl_bufp;
  int i, n;
  CORE_ADDR base;
  union dnttentry *dn_bufp;

  n = slt_size/(sizeof (union sltentry));
  base = 0;
  for (i = 0; i < n ; i++) {
    sl_bufp = hpread_get_slt (i, objfile);
    if (sl_bufp->snorm.sltdesc == SLT_FUNCTION) {
        dn_bufp =
          hpread_get_lntt(INDEX(objfile,
                                sl_bufp->sspec.backptr.dnttp),
                          objfile);
        base = dn_bufp->dfunc.lowaddr;
    }
    else if ((sl_bufp->snorm.sltdesc == SLT_NORMAL) ||
             (sl_bufp->snorm.sltdesc == SLT_EXIT))
        sl_bufp->snorm.address += base;
    else if (sl_bufp->snorm.sltdesc == SLT_NORMAL_OFFSET)
      sl_bufp->snormoff.address += base;
  }
}



/* Return 1 if an HP debug symbol of type KIND has a name associated with
 * it, else return 0. (This function is not currently used, but I'll
 * leave it here in case it proves useful later on. - RT).
 */

int
hpread_has_name (kind)
     enum dntt_entry_type kind;
{
  switch (kind)
    {
    case DNTT_TYPE_SRCFILE:
    case DNTT_TYPE_MODULE:
    case DNTT_TYPE_FUNCTION:
    case DNTT_TYPE_DOC_FUNCTION:
    case DNTT_TYPE_ENTRY:
    case DNTT_TYPE_IMPORT:
    case DNTT_TYPE_LABEL:
    case DNTT_TYPE_FPARAM:
    case DNTT_TYPE_SVAR:
    case DNTT_TYPE_DVAR:
    case DNTT_TYPE_CONST:
    case DNTT_TYPE_TYPEDEF:
    case DNTT_TYPE_TAGDEF:
    case DNTT_TYPE_MEMENUM:
    case DNTT_TYPE_FIELD:
    case DNTT_TYPE_COMMON:
    case DNTT_TYPE_SA:
    case DNTT_TYPE_BLOCKDATA:
    case DNTT_TYPE_MEMFUNC:
    case DNTT_TYPE_DOC_MEMFUNC:
      return 1;

    case DNTT_TYPE_BEGIN:
    case DNTT_TYPE_END:
    case DNTT_TYPE_POINTER:
    case DNTT_TYPE_ENUM:
    case DNTT_TYPE_SET:
    case DNTT_TYPE_ARRAY:
    case DNTT_TYPE_STRUCT:
    case DNTT_TYPE_UNION:
    case DNTT_TYPE_VARIANT:
    case DNTT_TYPE_FILE:
    case DNTT_TYPE_FUNCTYPE:
    case DNTT_TYPE_SUBRANGE:
    case DNTT_TYPE_WITH:
    case DNTT_TYPE_COBSTRUCT:
    case DNTT_TYPE_XREF:
    case DNTT_TYPE_MACRO:
    case DNTT_TYPE_CLASS_SCOPE:
    case DNTT_TYPE_REFERENCE:
    case DNTT_TYPE_PTRMEM:
    case DNTT_TYPE_PTRMEMFUNC:
    case DNTT_TYPE_CLASS:
    case DNTT_TYPE_GENFIELD:
    case DNTT_TYPE_VFUNC:
    case DNTT_TYPE_MEMACCESS:
    case DNTT_TYPE_INHERITANCE:
    case DNTT_TYPE_FRIEND_CLASS:
    case DNTT_TYPE_FRIEND_FUNC:
    case DNTT_TYPE_MODIFIER:
    case DNTT_TYPE_OBJECT_ID:
    case DNTT_TYPE_TEMPLATE:
    case DNTT_TYPE_TEMPLATE_ARG:
    case DNTT_TYPE_FUNC_TEMPLATE:
    case DNTT_TYPE_LINK:
    case DNTT_TYPE_DYN_ARRAY_DESC:
    case DNTT_TYPE_DESC_SUBRANGE:
      /* DNTT_TYPE_BEGIN_EXT ? */
      /* DNTT_TYPE_INLN ? */
      /* DNTT_TYPE_INLN_LIST ? */
      /* DNTT_TYPE_ALIAS ? */
    default:
      return 0;
    }
}

/* Do the dirty work of reading in the full symbol from a partial symbol
   table.  */

static void
hpread_psymtab_to_symtab_1 (pst)
     struct partial_symtab *pst;
{
  struct cleanup *old_chain;
  int i;

  /* Get out quick if passed junk.  */
  if (!pst)
    return;

  /* Complain if we've already read in this symbol table.  */
  if (pst->readin)
    {
      fprintf (stderr, "Psymtab for %s already read in.  Shouldn't happen.\n",
	       pst->filename);
      return;
    }

  /* Read in all partial symtabs on which this one is dependent */
  for (i = 0; i < pst->number_of_dependencies; i++)
    if (!pst->dependencies[i]->readin)
      {
	/* Inform about additional files that need to be read in.  */
	if (info_verbose)
	  {
	    fputs_filtered (" ", gdb_stdout);
	    wrap_here ("");
	    fputs_filtered ("and ", gdb_stdout);
	    wrap_here ("");
	    printf_filtered ("%s...", pst->dependencies[i]->filename);
	    wrap_here ("");	/* Flush output */
	    gdb_flush (gdb_stdout);
	  }
	hpread_psymtab_to_symtab_1 (pst->dependencies[i]);
      }

  /* If it's real...  */
  if (LDSYMLEN (pst))
    {
      /* Init stuff necessary for reading in symbols */
      buildsym_init ();
      old_chain = make_cleanup (really_free_pendings, 0);

      pst->symtab =
	hpread_expand_symtab (pst->objfile, LDSYMOFF (pst), LDSYMLEN (pst),
			      pst->textlow, pst->texthigh - pst->textlow,
			      pst->section_offsets, pst->filename);
      sort_symtab_syms (pst->symtab);

      do_cleanups (old_chain);
    }

  pst->readin = 1;
}

/* srikanth, 991111, new functions to expand the "globals" psymtab
   incrementally.
*/

static void
hpread_global_psymtab_to_symtab_1 (pst)
     struct partial_symtab *pst;
{
  struct cleanup *old_chain;

  /* If it's real...  */
  if (LDSYMLEN (pst))
    {
      /* Init stuff necessary for reading in symbols */
      buildsym_init ();
      old_chain = make_cleanup (really_free_pendings, 0);

      pst->symtab =
        hpread_expand_global_symtab (pst->objfile, LDSYMOFF (pst), 
                                     LDSYMLEN (pst),
                              pst->textlow, pst->texthigh - pst->textlow,
                              pst->section_offsets, pst->filename,
                              pst);
      sort_symtab_syms (pst->symtab);

      do_cleanups (old_chain);
    }
}

void
hpread_dump_debug_relocations(objfile,
                              abfd,
                              lntt_section,
                              gntt_section,
                              slt_section)
  struct objfile *objfile;
  bfd *abfd;
  asection *slt_section, *lntt_section, *gntt_section;
{
  /* in somread.c */
  extern void dump_relocations
    (struct objfile *, bfd *, asection *);
  char *buf;
  
  printf("\nRelocations for LNTT:\n");
  dump_relocations(objfile, abfd, lntt_section);
  printf("\nRelocations for GNTT:\n");
  dump_relocations(objfile, abfd, gntt_section);
  printf("\nRelocations for SLT:\n");
  dump_relocations(objfile, abfd, slt_section);
}


struct debug_sect
{
  asection *gntt_section;
  asection *lntt_section;
  asection *slt_section;
  asection *vt_section;
  char *lntt;
  char *slt;
  char *vt;
  int lntt_size;
  int slt_size;
  int vt_size;
  int globals_start;
  CORE_ADDR lowaddr;
  /* flags */
  union {
    struct {
      int f_skip:1;
      int f_nofunc:1;
      int f_principal:1;
      int f_reserve:29;
    } flag;
    int f_all;
  } flags;
};

int
hpread_compare_debug_sects(p1, p2)
  void *p1, *p2;
{
  struct debug_sect *s1, *s2;
  s1=(struct debug_sect *) p1;
  s2=(struct debug_sect *) p2;

  /* RM: Entries with lowaddr = MAXINT are "smaller" than everything
   * else! This is because MAXINT is used to indicate debug sections
   * without any functions, and these debug sections are associated
   * with the default psymtab, which has the *smallest* address. Fun.
   */
  if (s1->lowaddr == s2->lowaddr)
    return 0;

  /* CM: Use f_* flags instead of MAXINT */
  if (s1->flags.flag.f_skip)
    return -1;
  if (s2->flags.flag.f_skip)
    return 1;
  if (s1->flags.flag.f_nofunc)
    return -1;
  if (s2->flags.flag.f_nofunc)
    return 1;

  return
    (s1->lowaddr < s2->lowaddr) ?
    -1 :
    ((s1->lowaddr > s2->lowaddr) ?
     1 :
     0);
}

struct parse_filename_info {
  size_t archive_name_length;
  char *o_file_name_start;
  size_t o_file_name_length;
};

/* CM: Parse the name and fill out the "struct parse_filename_info"
   structure. This looks for filenames that specify .o files located
   in archive libraries. The form looks like: foo.a(bar.o). This will
   return "info.archive_name_length == 0" if this name does not
   represent an archive library. */
/* RM: $#@! PA64 uses foo.a[bar.o] instead of foo.a(bar.o) */
static void parse_filename(char *name, struct parse_filename_info *info)
{
  char *found_open = NULL;
  char *nameWalker = name;

  while ((*nameWalker != '\0') && (!found_open)) {
    if ((*nameWalker == '(') ||
        (*nameWalker == '['))
      {
        found_open = nameWalker;
      }
    nameWalker++;
  }
  while (*nameWalker != '\0') {
    nameWalker++;
  }
  if ((found_open) && (nameWalker >= (found_open + 2))) {
    info->archive_name_length = found_open - name;
    info->o_file_name_start = found_open + 1;
    info->o_file_name_length = nameWalker - found_open - 2;
  } else {
    info->archive_name_length = 0;
    info->o_file_name_start = 0;
    info->o_file_name_length = nameWalker - name;
  }
}

/* CM: Uncomment out the following to print out a bunch of stuff as
   each GNTT or LNTT gets processed. */
/* #define DEBUG_PRINT_GNTT_AS_PROCESSED */
/* #define DEBUG_PRINT_LNTT_AS_PROCESSED */

extern char *object_path;

void
hpread_doom_psymtab_to_symtab (pst)
     struct partial_symtab *pst;
{
  /* in somread.c */
  extern bfd_size_type relocated_section_size (bfd *, asection *);
  extern void get_relocated_section_contents
    (struct objfile *, bfd *, int, asection *, char *, int);

  bfd *abfd;
  struct objfile *objfile;
  char *buf, *buf2;
  int symbol_count;
  int symsize;
  unsigned int max_symnum;
  bfd_size_type lntt_size, gntt_size, slt_size;
  bfd_size_type rel_lntt_size, rel_gntt_size, rel_slt_size;
  int sym_index;
  int sym_index_first_source;
  int sym_index_source;
  union dnttentry *dn_bufp = 0;
  int objid;
  int n_dbg_sects;
  int i, j, k;
  struct debug_sect *debug_sects;
  int principal_dbg_sect;
  int principal_dbg_sect_gntt_read;
  struct partial_symtab *cur_pst, *last_pst, *default_pst;
  struct cleanup *old_chain = NULL;
  struct parse_filename_info obj_filename_parse;
  char *file_name;
  char *path;
  char *cdir;
  char *p;

  char *archive_name = NULL;
  char *arch_member_name = NULL;
  bfd *ar_bfd = NULL;

  char *lntt_objfile;
  char *gntt_objfile;
  char *slt_objfile;
  size_t slt_size_objfile;
  sltpointer sl_index_objfile;
  unsigned int globals_start_objfile;
  unsigned int lntt_symcount_objfile;
  unsigned int gntt_symcount_objfile;
  int current_chapter_objfile;
  char* vt_objfile;
  unsigned int vt_size_objfile;
  struct type **type_vector_objfile;
  int type_vector_length_objfile;
 
  if (!pst)
    return;

  if (pst->readin)
    {
      fprintf (stderr, "Psymtab for %s already read in.  Shouldn't happen.\n",
	       pst->filename);
      return;
    }

  if (OFILE_LOAD_STATUS(pst) != DOOM_OFILE_DO_DEMAND_LOAD)
    {
      /* we already tried to read this psymtab. Maybe we couldn't find
       * the object file or maybe we didn't find any debug. Either
       * way, there's no point in trying again
       */
      return;
    }

  objfile = pst->objfile;

  /* RM: squirrel away various objfile attributes. We'll temporarily
   * reuse these for our own nefarious purposes */
  lntt_objfile = LNTT(objfile);
  gntt_objfile = GNTT(objfile);
  slt_objfile = SLT(objfile);
  slt_size_objfile = SLT_SIZE (objfile);
  sl_index_objfile = SL_INDEX(objfile);
  globals_start_objfile = GLOBALS_START(objfile);
  lntt_symcount_objfile = LNTT_SYMCOUNT(objfile);
  gntt_symcount_objfile = GNTT_SYMCOUNT(objfile);
  vt_objfile = VT(objfile);
  vt_size_objfile = VT_SIZE(objfile);
  type_vector_objfile = TYPE_VECTOR(objfile);
  type_vector_length_objfile = TYPE_VECTOR_LENGTH(objfile);
  current_chapter_objfile = CURRENT_CHAPTER (objfile);

  CURRENT_CHAPTER (objfile) = 0;

  file_name = STRINGS(objfile) + COMPMAP(objfile)->cr[OBJID(pst)].objname;
  if (debug_traces)
    printf("Reading symbols from %s...\n", file_name);

  /* RM: Construct the pathname by replacing $cdir with the
   * compilation directory */
  path = object_path;
  cdir = STRINGS(objfile) + COMPMAP(objfile)->cr[OBJID(pst)].dirname;
  if (cdir && *cdir)
    {
      /* Replace a path entry of $cdir with the compilation directory
         name */
#define	cdir_len	5
      p = (char *)strstr (object_path, "$cdir");
      if (p && (p == path
                || p[-1] == DIRNAME_SEPARATOR)
          && (p[cdir_len] == DIRNAME_SEPARATOR || p[cdir_len] == '\0'))
	{
	  int len;

	  path = (char *)
	    alloca (strlen (object_path) + 1 + strlen (cdir) + 1);
	  len = p - object_path;
	  strncpy (path, object_path, len);		/* Before $cdir */
	  strcpy (path + len, cdir);		/* new stuff */
	  strcat (path + len, object_path + len + cdir_len); /* After $cdir */
	}
    }

  
  /* CM: Parse the filename string */
  parse_filename(file_name, &obj_filename_parse);
  if (obj_filename_parse.archive_name_length > 0) {
    /* CM: Read archive file and get a bfd for a member */
    int found_member;

    archive_name = malloc(obj_filename_parse.archive_name_length + 1);
    arch_member_name = malloc(obj_filename_parse.o_file_name_length + 1);

    memcpy(archive_name, file_name, obj_filename_parse.archive_name_length);
    archive_name[obj_filename_parse.archive_name_length] = '\0';

    memcpy(arch_member_name, obj_filename_parse.o_file_name_start,
	   obj_filename_parse.o_file_name_length);
    arch_member_name[obj_filename_parse.o_file_name_length] = '\0';

    found_member = 0;
    ar_bfd = symfile_bfd_open_with_path_type (getbasename (archive_name),
					      path,
					      bfd_archive,
					      0);

    /* Try again with archive name, since archive name is a full pathname */
    if (!ar_bfd)
      ar_bfd = symfile_bfd_open_with_path_type (archive_name,
						path,
						bfd_archive,
						0);

    abfd = NULL;
    if (ar_bfd != NULL) {
      do {
	abfd = bfd_openr_next_archived_file(ar_bfd, abfd);
	if (abfd != NULL) {
	  if (STREQ(abfd->filename, arch_member_name)) {
	    if (bfd_check_format(abfd, bfd_object)) {
	      found_member = 1;
	    }
	  }
	}
      } while ((abfd != NULL) && (!found_member));
    }
    /* CM: "abfd" now holds a bfd to the archive member or NULL
       if the file was not found in the archive. */
  } else {
    /* RM: read in debug sections of the object file, and put then in
     * the objfile. This is so that the existing code in this file can
     * read it in without much tweaking.
     */
    abfd = symfile_bfd_open_with_path_type (getbasename (file_name), path,
					    bfd_object, 0);

    /* Try again with file_name which is a full pathname */
    if (!abfd)
      abfd = symfile_bfd_open_with_path_type (file_name, path,
					      bfd_object, 0);
  }

  if (!abfd) {
    warning("%s: Unable to open file to read debug information. Use the "
	    "\"objectretry\" command to try again.",
	    file_name);
    OFILE_LOAD_STATUS(pst) = DOOM_OFILE_OPEN_ERROR;
    return;
  }

  /* count number of debug sections */
  n_dbg_sects = 0;
  while (bfd_get_section_by_name_and_id(abfd, HP_HEADER, n_dbg_sects)) {
    n_dbg_sects++;
  }

  /* Does this file have debug information? */
  if (!n_dbg_sects) {
    if (debug_traces) {
      printf("  no debug information information found\n");
    }
    OFILE_LOAD_STATUS(pst) = DOOM_OFILE_LOADED_NO_DEBUG;
    return;
  }
  else {
    if (debug_traces) {
      printf("  found %d debug section(s)\n", n_dbg_sects);
    }
  }

  OFILE_LOAD_STATUS(pst) = DOOM_OFILE_LOADED_WITH_DEBUG;
  objid = OBJID(pst);

  /* read all the debug sections */
  debug_sects = xmalloc(n_dbg_sects*(sizeof (struct debug_sect)));
  for (i = 0; i < n_dbg_sects; i++) {
    /* Combine GNTT and LNTT into one large section */
    debug_sects[i].lntt_section =
      (void*)bfd_get_section_by_name_and_id (abfd, HP_LNTT, i);
    debug_sects[i].gntt_section =
      (void*)bfd_get_section_by_name_and_id (abfd, HP_GNTT, i);
    debug_sects[i].slt_section =
      (void*)bfd_get_section_by_name_and_id (abfd, HP_SLT, i);

    if (debug_traces >= 1000)
      hpread_dump_debug_relocations(objfile,
                                    abfd,
                                    debug_sects[i].lntt_section,
                                    debug_sects[i].gntt_section,
                                    debug_sects[i].slt_section);

    lntt_size = bfd_section_size (abfd, debug_sects[i].lntt_section);
    gntt_size = bfd_section_size (abfd, debug_sects[i].gntt_section);
    slt_size = bfd_section_size (abfd, debug_sects[i].slt_section);
    rel_lntt_size = relocated_section_size (abfd, debug_sects[i].lntt_section);
    rel_gntt_size = relocated_section_size (abfd, debug_sects[i].gntt_section);
    rel_slt_size = relocated_section_size (abfd, debug_sects[i].slt_section);

    if (debug_traces >= 100) {
      printf("Raw LNTT size: %d\n", (int) lntt_size);
      printf("Relocated LNTT size: %d\n", (int) rel_lntt_size);
      printf("Raw GNTT size: %d\n", (int) gntt_size);
      printf("Relocated GNTT size: %d\n", (int) rel_gntt_size);
      printf("Raw SLT size: %d\n", (int) slt_size);
      printf("Relocated SLT size: %d\n", (int) rel_slt_size);
    }

    debug_sects[i].lntt_size = rel_lntt_size + rel_gntt_size;
    debug_sects[i].slt_size = rel_slt_size;

    debug_sects[i].lntt
      = xmalloc (debug_sects[i].lntt_size);
    get_relocated_section_contents (objfile, abfd, objid,
                                    debug_sects[i].lntt_section,
                                    debug_sects[i].lntt,
                                    rel_lntt_size);
    get_relocated_section_contents (objfile, abfd, objid,
                                    debug_sects[i].gntt_section,
                                    debug_sects[i].lntt +
                                    rel_lntt_size,
                                    rel_gntt_size);
    debug_sects[i].globals_start = (rel_lntt_size /
				    sizeof (struct dntt_type_block));

    debug_sects[i].slt = xmalloc (debug_sects[i].slt_size);
    get_relocated_section_contents (objfile, abfd, objid,
                                    debug_sects[i].slt_section,
                                    debug_sects[i].slt,
                                    rel_slt_size);

    debug_sects[i].vt_section =
      (void*)bfd_get_section_by_name_and_id (abfd, HP_VT, i);
    debug_sects[i].vt_size =
      bfd_section_size (abfd, debug_sects[i].vt_section);
    debug_sects[i].vt = (char *) xmalloc (debug_sects[i].vt_size);
    bfd_get_section_contents (abfd, debug_sects[i].vt_section,
                              debug_sects[i].vt,
                              0, debug_sects[i].vt_size);
    debug_sects[i].flags.f_all = 0;

    /* Adjust the SLT -- pxdb has not been run, so the SLT still
     * contains offsets from functions instead of actual addresses
     */
    /* For PA64, this shouldn't be done -- pxdb leaves SLT entries as
     * offsets, so the non-DOOM code handles this anyway
     */
    /* Since this procedure uses various other procedures that assume
     * that all the debug info is in the executable, we need to do
     * some trickery to pretend that we got these tables from the
     * executable
     */
    LNTT (objfile) = debug_sects[i].lntt;
    GNTT (objfile) = 0;
    SLT (objfile) = debug_sects[i].slt;
    SLT_SIZE (objfile) = debug_sects[i].slt_size;
    SL_INDEX (objfile) = 0;
    GLOBALS_START(objfile) = debug_sects[i].globals_start;
    LNTT_SYMCOUNT (objfile) =
      (rel_lntt_size + rel_gntt_size) / sizeof (struct dntt_type_block);
    GNTT_SYMCOUNT (objfile) = 0;

#ifndef BFD64
    hpread_adjust_slt_for_doom(objfile, rel_slt_size);
#endif
    
    /* find an address inside each debug section */
    lntt_size = rel_lntt_size + rel_gntt_size;
    for (sym_index = 0;
         sym_index < lntt_size / sizeof (struct dntt_type_block);
         sym_index++) {
      dn_bufp = hpread_get_lntt(sym_index, objfile);
      if (dn_bufp->dblock.extension)
	continue;
      if (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_MODULE) {
	debug_sects[i].flags.flag.f_principal = 1;
      } else if (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_FUNCTION)
        if (debug_sects[i].flags.flag.f_principal && (dn_bufp->dfunc.lowaddr == 0))
          /* RM: function in the main debug didn't make it to the
           * executable, continue looking
           */
          continue;
        else
	  break;
    }
    if (sym_index < lntt_size / sizeof (struct dntt_type_block)) {
      if (((dn_bufp->dfunc.lowaddr == 0) && (dn_bufp->dfunc.hiaddr == 0)) ||
	   ((unsigned long) (dn_bufp->dfunc.hiaddr) <
	    (unsigned long) (dn_bufp->dfunc.lowaddr))) {
	/* Skip reading. lowaddr should never be used. */
	debug_sects[i].lowaddr = MAXINT;
	/* Never skip the principal debug section */
	if (debug_sects[i].flags.flag.f_principal) {
	  debug_sects[i].flags.flag.f_nofunc = 1;
	} else {
	  debug_sects[i].flags.flag.f_skip = 1;
	}
      } else {
	debug_sects[i].lowaddr = (dn_bufp->dfunc.lowaddr +
				  ANOFFSET(objfile->section_offsets, 0));
      }
    } else {
      /* No functions. lowaddr should never be used. */
      debug_sects[i].lowaddr = MAXINT;
      debug_sects[i].flags.flag.f_nofunc = 1;
    }
#if 0
    if ((sym_index < lntt_size / sizeof (struct dntt_type_block)) &&
	(dn_bufp->dfunc.hiaddr >= dn_bufp->dfunc.lowaddr) &&
        (!debug_sects[i].flags.flag.f_principal))
      debug_sects[i].lowaddr = dn_bufp->dfunc.lowaddr +
                                 ANOFFSET(objfile->section_offsets, 0);
    else {
      debug_sects[i].lowaddr = MAXINT;
    }
#endif
  }

  /* sort the debug sections */
  qsort((char *) debug_sects, n_dbg_sects, sizeof (struct debug_sect),
        hpread_compare_debug_sects);

  /* Look for principal debug section -- backwards */
  principal_dbg_sect = -1;
  for (i = n_dbg_sects; i--; ) {
    if (debug_sects[i].flags.flag.f_principal) {
      principal_dbg_sect = i;
      break;
    }
  }
#if 0
  for (i = 0; i < n_dbg_sects; i++) {
#if 0 /* !!!!! CM: Look at all the sections in case a principal section doesnt define a funxtion. */
    if ((debug_sects[i].lowaddr > 0) && (debug_sects[i].lowaddr != MAXINT)) {
#endif
      int max_index = (debug_sects[i].lntt_size /
		       sizeof (struct dntt_type_block));
      /* check to see if this is the principal debug section */
      LNTT (objfile) = debug_sects[i].lntt;
      for (sym_index = 0; sym_index < max_index; sym_index++) {
	dn_bufp = hpread_get_lntt(sym_index, objfile);
	if (dn_bufp->dblock.extension)
	  continue;
	if ((dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_MODULE) ||
            (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_FUNCTION))
	  break;
      }

      if ((sym_index < max_index) &&
          (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_MODULE)) {
	principal_dbg_sect = i;
	break;
      }
#if 0
    }
#endif
  }
#endif

  if (current_demangling_style != edg_demangling) {
    set_demangling_style (HP_DEMANGLING_STYLE_STRING);
  }

  /* find the default pst for this object file */
  default_pst = 0;
  ALL_OBJFILE_PSYMTABS (objfile, cur_pst)
    {
      if (DEFAULT_PSYMTAB(cur_pst) &&
          (OBJID(cur_pst) == OBJID(pst))) {
        default_pst = cur_pst;
        break;
      }
    }

  /* RM: set the compilation directory for all symtabs we'll read in */
  current_compilation_directory =
    STRINGS(objfile) + COMPMAP(objfile)->cr[OBJID(pst)].dirname;
  current_symtab_language = COMPMAP(objfile)->cr[OBJID(pst)].lang;
  
  /* Now read in the symbols */
  last_pst = 0;
  principal_dbg_sect_gntt_read = 0;
  for (i = 0; i < n_dbg_sects; i++) {
    if (debug_sects[i].flags.flag.f_skip) {
      /* we never used the VT so it should be safe to free it */
      free(debug_sects[i].vt);
      continue;
    }
    else if (debug_sects[i].flags.flag.f_nofunc) {
      /* debug section has no functions, read into default psymtab */
      cur_pst = default_pst;
    }
    else {
      cur_pst = find_pc_psymtab(debug_sects[i].lowaddr);
    }
    if (!cur_pst)
      continue;

    if (cur_pst != last_pst) {
      /* RM: ??? But check to see for "dependent" psymtabs. We don't
       *  handle these yet, since we don't seem to use them.
       */
      if (cur_pst->number_of_dependencies != 0)
        error("We don't handle dependent psymtabs yet\n");
      last_source_file = 0;
      subfile_stack = 0;
      if (last_pst) {
#if defined(DEBUG_PRINT_GNTT_AS_PROCESSED) || defined(DEBUG_PRINT_LNTT_AS_PROCESSED)
	fflush(stdout);
#endif
        last_pst->readin = 1;
        last_pst->symtab = end_symtab(last_pst->texthigh, objfile, 0);
        if (last_pst->symtab)
          last_pst->symtab->language = current_symtab_language;
        sort_symtab_syms (last_pst->symtab);
        do_cleanups (old_chain);
      }
      buildsym_init ();
      old_chain = make_cleanup (really_free_pendings, 0);
    }

    /* Since we use various procedures that assume that all the debug
     * info is in the executable, we need to do some trickery to
     * pretend that we got these tables from the executable
     */
    LNTT (objfile) = debug_sects[i].lntt;
    GNTT (objfile) = 0;
    SLT (objfile) = debug_sects[i].slt;
    SLT_SIZE (objfile) = debug_sects[i].slt_size;
    SL_INDEX (objfile) = 0;
    GLOBALS_START(objfile) = debug_sects[i].globals_start;
    LNTT_SYMCOUNT (objfile) =
      debug_sects[i].lntt_size / sizeof (struct dntt_type_block);
    GNTT_SYMCOUNT (objfile) = 0;
    VT (objfile) = debug_sects[i].vt;
    VT_SIZE (objfile) = debug_sects[i].vt_size;    
    TYPE_VECTOR(objfile) = 0;
    TYPE_VECTOR_LENGTH(objfile) = 0;

    processing_hp_compilation = HP_COMPILED_TARGET;
    processing_gcc_compilation = HP_COMPILED_TARGET;

    sym_index = 0;
    lntt_size = debug_sects[i].lntt_size;
    sym_index_first_source = -1;
    if (i == principal_dbg_sect) {
      /* Advance LNTT index to start of module */
      for (sym_index = 0;
           sym_index < lntt_size / sizeof (struct dntt_type_block);
           sym_index++)
        {
          dn_bufp = hpread_get_lntt(sym_index, objfile);
          if (dn_bufp->dblock.extension)
            continue;
          if (dn_bufp->dblock.kind ==
              (unsigned char) DNTT_TYPE_MODULE)
            break;
          else if (dn_bufp->dblock.kind ==
              (unsigned char) DNTT_TYPE_SRCFILE)
	    sym_index_first_source = sym_index;
        }
    }
    LDSYMOFF(cur_pst) = sym_index*sizeof (struct dntt_type_block);
    LDSYMLEN(cur_pst) =
          (debug_sects[i].globals_start *
             sizeof (struct dntt_type_block)) -
          LDSYMOFF(cur_pst);

    if (LDSYMLEN (cur_pst)) {
      char            *namestring;
      union dnttentry *dn_bufp;
      int              at_module_boundary = 0;
      int sym_index =
	LDSYMOFF(cur_pst) / sizeof (struct dntt_type_block);
      current_objfile = objfile;

      dn_bufp = hpread_get_lntt(sym_index, objfile);
      max_symnum = LDSYMLEN(cur_pst) / sizeof (struct dntt_type_block);
      sym_index_source = sym_index_first_source;

#ifdef DEBUG_PRINT_LNTT_AS_PROCESSED /* CM: Debugging for reading in the LNTT */
      if (cur_pst == default_pst)
	printf("(Defualt pst) ");
      printf("Processing LNTT (%d of %d) with %d indicies from %s -> ",
	     i, n_dbg_sects, max_symnum, file_name);
#endif
      for (symnum = 0; symnum < max_symnum; symnum++) {
	QUIT;		      /* Allow this to be interruptable */
	dn_bufp = hpread_get_lntt (sym_index + symnum, objfile);

	if (dn_bufp->dblock.extension) {
#ifdef DEBUG_PRINT_LNTT_AS_PROCESSED /* CM: Debugging for reading in the LNTT */
	  printf("x");
#endif
	  continue;
	}
#ifdef DEBUG_PRINT_LNTT_AS_PROCESSED /* CM: Debugging for reading in the LNTT */
	printf(".");
#endif
	SET_NAMESTRING (dn_bufp, &namestring, objfile);
	if (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_SRCFILE) {
	  sym_index_source = sym_index + symnum;
	} else if ((dn_bufp->dblock.kind ==
		    (unsigned char) DNTT_TYPE_MODULE) &&
		   (STREQ(namestring, "<<<NULL_SYMBOL>>>"))) {
	  union dnttentry *sym_index_string =
	    hpread_get_lntt(sym_index_source, objfile);
	  SET_NAMESTRING(sym_index_string, &namestring, objfile);
	}

	hpread_process_one_debug_symbol
	  (dn_bufp, namestring, cur_pst->section_offsets,
	   objfile, cur_pst->textlow,
	   cur_pst->texthigh - cur_pst->textlow,
	   cur_pst->filename, symnum + sym_index,
	   &at_module_boundary,
           1);
#if 0
	if (at_module_boundary == -1)
	  break;
#endif        
      }
#ifdef DEBUG_PRINT_LNTT_AS_PROCESSED /* CM: Debugging for reading in the LNTT */
      printf(" <- %d processed.\n", symnum);
#endif
    }

    free(TYPE_VECTOR(objfile));
    TYPE_VECTOR(objfile) = 0;
    TYPE_VECTOR_LENGTH(objfile) = 0;

    /* RM: if LDSYMLEN is 0, we haven't read an SRCFILE entry, so we
     * may not have started a symtab. Defer reading the GNTT.
     */
    /* RM: if this is the first pst, also read in the GNTTs of _all_
       the debug sections. aCC has started putting out some types in
       COMDATed GNTTs */
    if ((!principal_dbg_sect_gntt_read) && (LDSYMLEN (cur_pst) != 0)) {
      principal_dbg_sect_gntt_read = 1;
      for (j = 0; j < n_dbg_sects; j++) {
        /* RM: This should be okay -- if we don't find a symbol in
         * this object file, we should find it in the object file that
         * contains the comdat that actually made it to the executable
         */
        if (debug_sects[j].flags.flag.f_skip) {
          continue;
        }
        
        LNTT (objfile) = debug_sects[j].lntt;
        GNTT (objfile) = 0;
        SLT (objfile) = debug_sects[j].slt;
	SLT_SIZE (objfile) = debug_sects[j].slt_size;
        /* the GNTT should not cause entries in the line number table to
         * be read in
         */
        SL_INDEX(objfile) = MAXINT;
        GLOBALS_START(objfile) =
          debug_sects[j].globals_start;
        LNTT_SYMCOUNT (objfile) =
          debug_sects[j].lntt_size / sizeof (struct dntt_type_block);
        GNTT_SYMCOUNT (objfile) = 0;
        VT (objfile) = debug_sects[j].vt;
        VT_SIZE (objfile) = debug_sects[j].vt_size;    
        LDSYMOFF(cur_pst) = (debug_sects[j].globals_start *
                            sizeof (struct dntt_type_block));
        LDSYMLEN(cur_pst) = debug_sects[j].lntt_size - 
          LDSYMOFF(cur_pst);

        if (LDSYMLEN (cur_pst)) {
          char            *namestring;
          union dnttentry *dn_bufp;
          int              at_module_boundary = 0;
          int sym_index =
            LDSYMOFF(cur_pst) / sizeof (struct dntt_type_block);
          current_objfile = objfile;
          
          dn_bufp = hpread_get_lntt(sym_index, objfile);
          max_symnum = LDSYMLEN(cur_pst) / sizeof (struct dntt_type_block);
          sym_index_source = sym_index_first_source;
          
#ifdef DEBUG_PRINT_GNTT_AS_PROCESSED /* CM: Debugging for reading in the GNTT */
          if (cur_pst == default_pst)
            printf("(Defualt pst) ");
          printf("Processing GNTT (%d of %d) with %d indicies from %s -> ",
                 j, n_dbg_sects, max_symnum, file_name);
#endif
          
          for (symnum = 0; symnum < max_symnum; symnum++) {
            QUIT;		      /* Allow this to be interruptable */
            dn_bufp = hpread_get_lntt (sym_index + symnum, objfile);
            
            if (dn_bufp->dblock.extension) {
#ifdef DEBUG_PRINT_GNTT_AS_PROCESSED /* CM: Debugging for reading in the GNTT */
              printf("x");
#endif
              continue;
            }
            
#ifdef DEBUG_PRINT_GNTT_AS_PROCESSED /* CM: Debugging for reading in the GNTT */
            printf(".");
#endif
            
            SET_NAMESTRING (dn_bufp, &namestring, objfile);
            if (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_SRCFILE) {
              sym_index_source = sym_index + symnum;
            } else if ((dn_bufp->dblock.kind ==
                        (unsigned char) DNTT_TYPE_MODULE) &&
                       (STREQ(namestring, "<<<NULL_SYMBOL>>>"))) {
              union dnttentry *sym_index_string =
                hpread_get_lntt(sym_index_source, objfile);
              SET_NAMESTRING(sym_index_string, &namestring, objfile);
            }
            
            hpread_process_one_debug_symbol
              (dn_bufp, namestring, cur_pst->section_offsets,
               objfile, cur_pst->textlow,
               cur_pst->texthigh - cur_pst->textlow,
               cur_pst->filename, symnum + sym_index,
               &at_module_boundary,
               1);

            if (at_module_boundary == -1)
              break;
          }
#ifdef DEBUG_PRINT_GNTT_AS_PROCESSED /* CM: Debugging for reading in the GNTT */
          printf(" <- %d processed.\n", symnum);
#endif
        }
        free(TYPE_VECTOR(objfile));
        TYPE_VECTOR(objfile) = 0;
        TYPE_VECTOR_LENGTH(objfile) = 0;
      }
    }
    last_pst = cur_pst;
  }

  /* RM: free all the debug stuff, leave the vt in since we do have
     pointers into it */
  for (i = 0; i < n_dbg_sects; i++) {
    free(debug_sects[i].lntt);
    free(debug_sects[i].slt);
  }

  free (debug_sects);  /* srikanth, 000204 */

  if (last_pst) {
#if defined(DEBUG_PRINT_GNTT_AS_PROCESSED) || defined(DEBUG_PRINT_LNTT_AS_PROCESSED)
    fflush(stdout);
#endif
    last_pst->readin = 1;
    last_pst->symtab = end_symtab(last_pst->texthigh, objfile, 0);
    if (last_pst->symtab)
      last_pst->symtab->language = current_symtab_language;
    sort_symtab_syms (last_pst->symtab);
    do_cleanups (old_chain);
  }

  current_objfile = NULL;
  hp_som_som_object_present = 1;
  bfd_close(abfd);
  if (archive_name)
    free(archive_name);
  if (arch_member_name)
    free(arch_member_name);
  if (ar_bfd)
    bfd_close(ar_bfd);

  /* Reset all attributes of the objfile */

  LNTT (objfile) = lntt_objfile;
  GNTT (objfile) = gntt_objfile;
  SLT (objfile) = slt_objfile;
  SLT_SIZE (objfile) = slt_size_objfile;
  SL_INDEX (objfile) = sl_index_objfile;
  GLOBALS_START (objfile) = globals_start_objfile;
  LNTT_SYMCOUNT (objfile) = lntt_symcount_objfile;
  GNTT_SYMCOUNT (objfile) = gntt_symcount_objfile;
  VT (objfile) = vt_objfile;
  VT_SIZE (objfile) = vt_size_objfile;
  TYPE_VECTOR (objfile) = type_vector_objfile;
  TYPE_VECTOR_LENGTH (objfile) = type_vector_length_objfile;
  CURRENT_CHAPTER(objfile) = current_chapter_objfile;

  if (debug_traces > 1)
    printf_filtered ("...done.\n");
}

/* Read in all of the symbols for a given psymtab for real.
   Be verbose about it if the user wants that.  */

void
hpread_psymtab_to_symtab (pst)
     struct partial_symtab *pst;
{
  /* Get out quick if given junk.  */
  if (!pst)
    return;

  /* Sanity check.  */
  if (pst->readin)
    {
      fprintf (stderr, "Psymtab for %s already read in.  Shouldn't happen.\n",
	       pst->filename);
      return;
    }

  /* elz: setting the flag to indicate that the code of the target
     was compiled using an HP compiler (aCC, cc) 
     the processing_acc_compilation variable is declared in the 
     file buildsym.h, the HP_COMPILED_TARGET is defined to be equal
     to 3 in the file tm_hppa.h */

  processing_hp_compilation = HP_COMPILED_TARGET;
  processing_gcc_compilation = HP_COMPILED_TARGET;

  if (LDSYMLEN (pst) || pst->number_of_dependencies)
    {
      /* Print the message now, before reading the string table,
         to avoid disconcerting pauses.  */
      if (info_verbose)
	{
	  printf_filtered ("Reading in symbols for %s...", pst->filename);
	  gdb_flush (gdb_stdout);
	}

      CURRENT_CHAPTER(pst->objfile) = LDCHAPTER(pst);
      if (!incremental_expansion || 
          strcmp(pst->filename, "globals") || pst->psymbol_to_expand == 0)
	hpread_psymtab_to_symtab_1 (pst);
      else 
        hpread_global_psymtab_to_symtab_1 (pst);
      CURRENT_CHAPTER(pst->objfile) = 0;

      pst->psymbol_to_expand = 0;

      /* Match with global symbols.  This only needs to be done once,
         after all of the symtabs and dependencies have been read in.   */
      scan_file_globals (pst->objfile);

      /* Finish up the debug error message.  */
      if (info_verbose)
	printf_filtered ("done.\n");
    }
}

/* Read in a defined section of a specific object file's symbols.

   DESC is the file descriptor for the file, positioned at the
   beginning of the symtab
   SYM_OFFSET is the offset within the file of
   the beginning of the symbols we want to read
   SYM_SIZE is the size of the symbol info to read in.
   TEXT_OFFSET is the beginning of the text segment we are reading symbols for
   TEXT_SIZE is the size of the text segment read in.
   SECTION_OFFSETS are the relocation offsets which get added to each symbol. */

static struct symtab *
hpread_expand_symtab (objfile, sym_offset, sym_size, text_offset, text_size,
		      section_offsets, filename)
     struct objfile *objfile;
     int sym_offset;
     int sym_size;
     CORE_ADDR text_offset;
     int text_size;
     struct section_offsets *section_offsets;
     char *filename;
{
  char *namestring;
  union dnttentry *dn_bufp;
  unsigned max_symnum;
  int at_module_boundary = 0;
  /* 1 => at end, -1 => at beginning */
  struct symtab *s;

  int sym_index = sym_offset / sizeof (struct dntt_type_block);

  current_objfile = objfile;
  current_symtab_language = language_unknown;
  subfile_stack = 0;

  last_source_file = 0;

  /* Demangling style -- if EDG style already set, don't change it,
     as HP style causes some problems with the KAI EDG compiler */
  if (current_demangling_style != edg_demangling)
    {
      /* Otherwise, ensure that we are using HP style demangling */
      set_demangling_style (HP_DEMANGLING_STYLE_STRING);
    }

  dn_bufp = hpread_get_lntt (sym_index, objfile);
  if (!((dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_SRCFILE) ||
	(dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_MODULE)))
    {
      start_symtab ("globals", NULL, 0);
      record_debugformat ("HP");
    }

  /* The psymtab builder (hp-psymtab-read.c) is the one that
   * determined the "sym_size" argument (i.e. how many DNTT symbols
   * are in this symtab), which we use to compute "max_symnum"
   * (point in DNTT to which we read). 
   *
   * Perhaps this should be changed so that 
   * process_one_debug_symbol() "knows" when
   * to stop reading (based on reading from the MODULE to the matching
   * END), and take out this reliance on a #-syms being passed in...
   * (I'm worried about the reliability of this number). But I'll
   * leave it as-is, for now. - RT
   *
   * The change above has been made. I've left the "for" loop control
   * in to prepare for backing this out again. -JB
   */
  max_symnum = sym_size / sizeof (struct dntt_type_block);
  /* No reason to multiply on pst side and divide on sym side... FIXME */

  /* Read in and process each debug symbol within the specified range.
   */
  for (symnum = 0;
       symnum < max_symnum;
       symnum++)
    {
      QUIT;			/* Allow this to be interruptable */
      dn_bufp = hpread_get_lntt (sym_index + symnum, objfile);

      if (dn_bufp->dblock.extension)
	continue;

      /* Yow!  We call SET_NAMESTRING on things without names!  */
      SET_NAMESTRING (dn_bufp, &namestring, objfile);

      hpread_process_one_debug_symbol (dn_bufp, namestring, section_offsets,
				       objfile, text_offset, text_size,
				       filename, symnum + sym_index,
				       &at_module_boundary, 0);

      discard_dntt_cache ();

      /* OLD COMMENTS: This routine is only called for psts.  All psts
       * correspond to MODULES.  If we ever do lazy-reading of globals
       * from the LNTT, then there will be a pst which ends when the
       * LNTT ends, and not at an END MODULE entry.  Then we'll have
       * to re-visit this break.  

       if( at_end_of_module )
       break;

       */

      /* We no longer break out of the loop when we reach the end of a
         module. The reason is that with CTTI, the compiler can generate
         function symbols (for template function instantiations) which are not
         in any module; typically they show up beyond a module's end, and
         before the next module's start.  We include them in the current
         module.  However, we still don't trust the MAX_SYMNUM value from
         the psymtab, so we break out if we enter a new module. */

      if (at_module_boundary == -1)
	break;
    }

  current_objfile = NULL;
  hp_som_som_object_present = 1;	/* Indicate we've processed an HP SOM SOM file */

  s = end_symtab (text_offset + text_size, objfile, 0);
  if (s && current_symtab_language != language_unknown)
    s->language = current_symtab_language;
  return s;
}


static struct symtab *
hpread_expand_global_symtab (objfile, sym_offset, sym_size, 
                             text_offset, text_size,
                             section_offsets, filename,
                             pst)
     struct objfile *objfile;
     int sym_offset;
     int sym_size;
     CORE_ADDR text_offset;
     int text_size;
     struct section_offsets *section_offsets;
     char *filename;
     struct partial_symtab * pst;
{
  char *namestring;
  union dnttentry *dn_bufp;
  int at_module_boundary = 0;
  struct symtab *s;

  int sym_index = PSYMBOL_NATIVE_INFO (pst->psymbol_to_expand);

  current_objfile = objfile;
  subfile_stack = 0;

  last_source_file = 0;
  current_symtab_language = language_unknown;

  /* Demangling style -- if EDG style already set, don't change it,
     as HP style causes some problems with the KAI EDG compiler */
  if (current_demangling_style != edg_demangling)
    {
      /* Otherwise, ensure that we are using HP style demangling */
      set_demangling_style (HP_DEMANGLING_STYLE_STRING);
    }

  dn_bufp = hpread_get_lntt (sym_index, objfile);
  if (!((dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_SRCFILE) ||
        (dn_bufp->dblock.kind == (unsigned char) DNTT_TYPE_MODULE)))
    {
      start_symtab ("globals", NULL, 0);
      record_debugformat ("HP");
    }

  SET_NAMESTRING (dn_bufp, &namestring, objfile);

  hpread_process_one_debug_symbol (dn_bufp, namestring, section_offsets,
                                       objfile, text_offset, text_size,
                                       filename, sym_index,
                                       &at_module_boundary,
                                       0
        );

  discard_dntt_cache ();
  current_objfile = NULL;
  hp_som_som_object_present = 1;

  s = end_symtab (text_offset + text_size, objfile, 0);
  if (s && current_symtab_language != language_unknown)
    s->language = current_symtab_language;
  return s;
}

/* Convert a fortran string type into GDB internal format */

static struct type *
hpread_ftn_string_type(typep)
     dnttpointer typep;
{
  unsigned long len = typep.dntti.bitlength / 8 ;
  int lowbound = current_language->string_lower_bound;
  struct type *rangetype = create_range_type ((struct type *) NULL,
                                              builtin_type_int,
                                              lowbound, len + lowbound - 1);
  struct type *stringtype
    =   create_string_type ((struct type *) NULL, rangetype);
  return stringtype;
}

/* Convert basic types from HP debug format into GDB internal format.  */

static int
hpread_type_translate (typep)
     dnttpointer typep;
{
  if (!typep.dntti.immediate)
    {
      error ("error in hpread_type_translate\n.");
      return;
    }

  switch (typep.dntti.type)
    {
    case HP_TYPE_BOOLEAN:
    case HP_TYPE_BOOLEAN_S300_COMPAT:
    case HP_TYPE_BOOLEAN_VAX_COMPAT:
      if (typep.dntti.bitlength <= 8)
	return FT_BOOLEAN;
      if (typep.dntti.bitlength <= 16)   /* Fortran logical*2 */
        return FT_UNSIGNED_SHORT;
      if (typep.dntti.bitlength <= 32)   /* Fortran logical*4 */
        return FT_UNSIGNED_INTEGER;
      return FT_UNSIGNED_LONG_LONG;      /* Fortran logical*8 */
    case HP_TYPE_CHAR:		/* C signed char, C++ plain char */

    case HP_TYPE_WIDE_CHAR:
      return FT_CHAR;
    case HP_TYPE_INT:
      if (typep.dntti.bitlength <= 8)
	return FT_SIGNED_CHAR;	/* C++ signed char */
      if (typep.dntti.bitlength <= 16)
	return FT_SHORT;
      if (typep.dntti.bitlength <= 32)
	return FT_INTEGER;
      return FT_LONG_LONG;
    case HP_TYPE_LONG:
      if (typep.dntti.bitlength <= 8)
	return FT_SIGNED_CHAR;	/* C++ signed char. */
      return FT_LONG;
    case HP_TYPE_UNSIGNED_LONG:
      if (typep.dntti.bitlength <= 8)
	return FT_UNSIGNED_CHAR;	/* C/C++ unsigned char */
      if (typep.dntti.bitlength <= 16)
	return FT_UNSIGNED_SHORT;
#ifdef GDB_TARGET_IS_HPPA_20W
      if (typep.dntti.bitlength <= 64)
        return FT_UNSIGNED_LONG;
#else
      if (typep.dntti.bitlength <= 32)
        return FT_UNSIGNED_LONG;
#endif      
      return FT_UNSIGNED_LONG_LONG;
    case HP_TYPE_UNSIGNED_INT:
      if (typep.dntti.bitlength <= 8)
	return FT_UNSIGNED_CHAR;
      if (typep.dntti.bitlength <= 16)
	return FT_UNSIGNED_SHORT;
      if (typep.dntti.bitlength <= 32)
	return FT_UNSIGNED_INTEGER;
      return FT_UNSIGNED_LONG_LONG;
    case HP_TYPE_REAL:
    case HP_TYPE_REAL_3000:
    case HP_TYPE_DOUBLE:
      if (typep.dntti.bitlength == 64)
	return FT_DBL_PREC_FLOAT;
      if (typep.dntti.bitlength == 128)
	return FT_EXT_PREC_FLOAT;
      return FT_FLOAT;
    case HP_TYPE_COMPLEX:
    case HP_TYPE_COMPLEXS3000:
      if (typep.dntti.bitlength == 128)
	return FT_DBL_PREC_COMPLEX;
      if (typep.dntti.bitlength == 192)
	return FT_EXT_PREC_COMPLEX;
      return FT_COMPLEX;
    case HP_TYPE_VOID:
      return FT_VOID;
    case HP_TYPE_STRING200:
    case HP_TYPE_LONGSTRING200:
    case HP_TYPE_MOD_STRING_SPEC:
    case HP_TYPE_MOD_STRING_3000:
    case HP_TYPE_FTN_STRING_S300_COMPAT:
    case HP_TYPE_FTN_STRING_VAX_COMPAT:
      return FT_STRING;
    case HP_TYPE_TEMPLATE_ARG:
      return FT_TEMPLATE_ARG;
    case DNTT_TYPE_XREF:
    case DNTT_TYPE_SA:
      if (!static_analysis_warning) {
        warning ("Static analysis is not supported");
        static_analysis_warning = 1;
      }
      return FT_VOID;
    case HP_TYPE_TEXT:
    case HP_TYPE_FLABEL:
    case HP_TYPE_PACKED_DECIMAL:
    case HP_TYPE_ANYPOINTER:
    case HP_TYPE_GLOBAL_ANYPOINTER:
    case HP_TYPE_LOCAL_ANYPOINTER:
    default:
      warning ("hpread_type_translate: unhandled type code %d.\n",typep.dntti.type);
      return FT_VOID;
    }
}

/* Given a position in the DNTT, return a pointer to the 
 * already-built "struct type" (if any), for the type defined 
 * at that position.
 */

static struct type **
hpread_lookup_type (hp_type, objfile)
     dnttpointer hp_type;
     struct objfile *objfile;
{
  unsigned old_len;
  int index = INDEX(objfile, hp_type.dnttp);
  int size_changed = 0;

  /* The immediate flag indicates this doesn't actually point to
   * a type DNTT.
   */
  if (hp_type.dntti.immediate)
    return NULL;

  /* For each objfile, we maintain a "type vector".
   * This an array of "struct type *"'s with one pointer per DNTT index.
   * Given a DNTT index, we look in this array to see if we have
   * already processed this DNTT and if it is a type definition.
   * If so, then we can locate a pointer to the already-built
   * "struct type", and not build it again.
   * 
   * The need for this arises because our DNTT-walking code wanders
   * around. In particular, it will encounter the same type multiple
   * times (once for each object of that type). We don't want to 
   * built multiple "struct type"'s for the same thing.
   *
   * Having said this, I should point out that this type-vector is
   * an expensive way to keep track of this. If most DNTT entries are 
   * 3 words, the type-vector will be 1/3 the size of the DNTT itself.
   * Alternative solutions:
   * - Keep a compressed or hashed table. Less memory, but more expensive
   *   to search and update.
   * - (Suggested by JB): Overwrite the DNTT entry itself
   *   with the info. Create a new type code "ALREADY_BUILT", and modify
   *   the DNTT to have that type code and point to the already-built entry.
   * -RT
   */

  if (index < LNTT_SYMCOUNT (objfile))
    {
      if (index >= TYPE_VECTOR_LENGTH (objfile))
	{
	  old_len = TYPE_VECTOR_LENGTH (objfile);

	  /* See if we need to allocate a type-vector. */
	  if (old_len == 0)
	    {
              /* srikanth, 000207, we used to take into consideration
                 the GNTT_SYMCOUNT field to decide how big the type
                 vector should be. This is wrong as GNTT_SYMCOUNT is
                 computed based on the pxdb quick lookup tables which
                 have already been blown into smithereens.
              */
	      TYPE_VECTOR_LENGTH (objfile) = LNTT_SYMCOUNT (objfile);
	      TYPE_VECTOR (objfile) = (struct type **)
		xmmalloc (objfile->md, TYPE_VECTOR_LENGTH (objfile) * sizeof (struct type *));
	      memset (&TYPE_VECTOR (objfile)[old_len], 0,
		      (TYPE_VECTOR_LENGTH (objfile) - old_len) *
		      sizeof (struct type *));
	    }
	}
      return &TYPE_VECTOR (objfile)[index];
    }
  else
    return NULL;
}

/* Possibly allocate a GDB internal type so we can internalize HP_TYPE.
   Note we'll just return the address of a GDB internal type if we already
   have it lying around.  */

static struct type *
hpread_alloc_type (hp_type, objfile)
     dnttpointer hp_type;
     struct objfile *objfile;
{
  struct type **type_addr;

  type_addr = hpread_lookup_type (hp_type, objfile);
  if (*type_addr == 0)
    {
      *type_addr = alloc_type (objfile);

      /* A hack - if we really are a C++ class symbol, then this default
       * will get overriden later on.
       */
      TYPE_CPLUS_SPECIFIC (*type_addr)
	= (struct cplus_struct_type *) &cplus_struct_default;
    }

  return *type_addr;
}

/* Read a native enumerated type and return it in GDB internal form.  */

static struct type *
hpread_read_enum_type (hp_type, dn_bufp, objfile)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{
  struct type *type;
  struct pending **symlist, *osyms, *syms;
  struct pending *local_list = NULL;
  int o_nsyms, nsyms = 0;
  dnttpointer mem;
  union dnttentry *memp;
  char *name;
  long n;
  struct symbol *sym;

  /* Allocate a GDB type. If we've already read in this enum type,
   * it'll return the already built GDB type, so stop here.
   * (Note: I added this check, to conform with what's done for 
   *  struct, union, class.
   *  I assume this is OK. - RT)
   */
  type = hpread_alloc_type (hp_type, objfile);
  if (TYPE_CODE (type) == TYPE_CODE_ENUM)
    return type;

  /* HP C supports "sized enums", where a specifier such as "short" or
     "char" can be used to get enums of different sizes. So don't assume
     an enum is always 4 bytes long. pai/1997-08-21 */
  TYPE_LENGTH (type) = dn_bufp->denum.bitlength / 8;

  symlist = &file_symbols;
  osyms = *symlist;
  o_nsyms = osyms ? osyms->nsyms : 0;

  /* Get a name for each member and add it to our list of members.  
   * The list of "mem" SOM records we are walking should all be
   * SOM type DNTT_TYPE_MEMENUM (not checked).
   */
  mem = dn_bufp->denum.firstmem;
  while (mem.word && mem.word != DNTTNIL)
    {
      memp = hpread_get_lntt (INDEX(objfile, mem.dnttp), objfile);

      name = VT (objfile) + memp->dmember.name;
      sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
					     sizeof (struct symbol));
      memset (sym, 0, sizeof (struct symbol));
      SYMBOL_NAME (sym) = name;  /* JAGaa80452 */
      SYMBOL_CLASS (sym) = LOC_CONST;
      SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
      SYMBOL_VALUE (sym) = memp->dmember.value;
      add_symbol_to_list (sym, symlist);
      nsyms++;
      mem = memp->dmember.nextmem;
    }

  /* Now that we know more about the enum, fill in more info.  */
  TYPE_CODE (type) = TYPE_CODE_ENUM;
  TYPE_FLAGS (type) &= ~TYPE_FLAG_STUB;
  TYPE_NFIELDS (type) = nsyms;
  TYPE_FIELDS (type) = (struct field *)
    obstack_alloc (&objfile->type_obstack, sizeof (struct field) * nsyms);

  /* Find the symbols for the members and put them into the type.
     The symbols can be found in the symlist that we put them on
     to cause them to be defined.  osyms contains the old value
     of that symlist; everything up to there was defined by us.

     Note that we preserve the order of the enum constants, so
     that in something like "enum {FOO, LAST_THING=FOO}" we print
     FOO, not LAST_THING.  */
  for (syms = *symlist, n = 0; syms; syms = syms->next)
    {
      int j = 0;
      if (syms == osyms)
	j = o_nsyms;
      for (; j < syms->nsyms; j++, n++)
	{
	  struct symbol *xsym = syms->symbol[j];
	  SYMBOL_TYPE (xsym) = type;
	  TYPE_FIELD_NAME (type, n) = SYMBOL_NAME (xsym);
	  TYPE_FIELD_BITPOS (type, n) = SYMBOL_VALUE (xsym);
	  TYPE_FIELD_BITSIZE (type, n) = 0;
	}
      if (syms == osyms)
	break;
    }

  return type;
}

/* Determine gdb equivalent of hp language type */

static enum language
hpread_language_type (enum hp_language hp_lang)
{
  switch (hp_lang)
    {
    case HP_LANGUAGE_UNKNOWN:
      return language_unknown;

    case HP_LANGUAGE_C:
      return language_c;

    case HP_LANGUAGE_FORTRAN:
      return language_fortran;

    case HP_LANGUAGE_CPLUSPLUS:
      return language_cplus;

    default:
      return language_auto;
    }
}

/* Read and internalize a native function debug symbol.  */

static struct type *
hpread_read_function_type (hp_type, dn_bufp, objfile, newblock)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
     int newblock;
{
  struct type *type, *type1;
  struct pending *syms;
  struct pending *local_list = NULL;
  int nsyms = 0;
  dnttpointer param;
  union dnttentry *paramp;
  char *name;
  long n;
  struct symbol *sym;
  int record_args = 1;
  enum language lang = hpread_language_type (dn_bufp->dfunc.language);
  union dnttentry * dn_tmp;

  /* See if we've already read in this type.  */
  type = hpread_alloc_type (hp_type, objfile);
  if (TYPE_CODE (type) == TYPE_CODE_FUNC)
    {
      record_args = 0;		/* already read in, don't modify type */
    }
  else
    {
      /* Nope, so read it in and store it away.  */
      if (dn_bufp->dblock.kind == DNTT_TYPE_FUNCTION ||
          dn_bufp->dblock.kind == DNTT_TYPE_MEMFUNC ||
          dn_bufp->dblock.kind == DNTT_TYPE_ENTRY)
        /* FORTRAN subroutines can have DNTTNIL return types so we need to
           special case them or else call to hpread_type_lookup will issue
           an "unhandled type code" warning for the return type
        */
        if (lang == language_fortran && dn_bufp->dfunc.retval.word == DNTTNIL)
          type1 = lookup_function_type (lookup_fundamental_type (objfile, FT_VOID));
        else
	  type1 = lookup_function_type (hpread_type_lookup (dn_bufp->dfunc.retval,
							    objfile));
      else if (dn_bufp->dblock.kind == DNTT_TYPE_FUNCTYPE)
	type1 = lookup_function_type (hpread_type_lookup (dn_bufp->dfunctype.retval,
							  objfile));
      else			/* expect DNTT_TYPE_FUNC_TEMPLATE */
	type1 = lookup_function_type (hpread_type_lookup (dn_bufp->dfunc_template.retval,
							  objfile));
      memcpy ((char *) type, (char *) type1, sizeof (struct type));

      /* Mark it -- in the middle of processing */
      TYPE_FLAGS (type) |= TYPE_FLAG_INCOMPLETE;
    }

  /* Now examine each parameter noting its type, location, and a
     wealth of other information.  */
  if (dn_bufp->dblock.kind == DNTT_TYPE_FUNCTION ||
      dn_bufp->dblock.kind == DNTT_TYPE_MEMFUNC)
    param = dn_bufp->dfunc.firstparam;
  else if (dn_bufp->dblock.kind == DNTT_TYPE_ENTRY)
    param = dn_bufp->dentry.firstparam;
  else if (dn_bufp->dblock.kind == DNTT_TYPE_FUNCTYPE)
    param = dn_bufp->dfunctype.firstparam;
  else				/* expect DNTT_TYPE_FUNC_TEMPLATE */
    param = dn_bufp->dfunc_template.firstparam;

  while (param.word && param.word != DNTTNIL)
    {
      paramp = hpread_get_lntt (INDEX(objfile, param.dnttp), objfile);
      nsyms++;
      param = paramp->dfparam.nextparam;

      /* Get the name.  */
      name = VT (objfile) + paramp->dfparam.name;

      sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
					     sizeof (struct symbol));
      (void) memset (sym, 0, sizeof (struct symbol));
      SYMBOL_NAME (sym) = name;
      SYMBOL_LANGUAGE (sym) = lang;

      /* Figure out where it lives.  */
      if (paramp->dfparam.regparam)
	SYMBOL_CLASS (sym) = LOC_REGPARM;
      else if (paramp->dfparam.indirect)
	SYMBOL_CLASS (sym) = LOC_REF_ARG;
      else
	SYMBOL_CLASS (sym) = LOC_ARG;
      SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
      if (paramp->dfparam.copyparam)
	{
	  SYMBOL_VALUE (sym) = paramp->dfparam.location;
#ifdef HPREAD_ADJUST_STACK_ADDRESS
	  SYMBOL_VALUE (sym)
	    += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
	  /* This is likely a pass-by-invisible reference parameter,
	     Hack on the symbol class to make GDB happy.  */
	  /* ??rehrauer: This appears to be broken w/r/t to passing
	     C values of type float and struct.  Perhaps this ought
	     to be highighted as a special case, but for now, just
	     allowing these to be LOC_ARGs seems to work fine.
	   */
#if 0
	  SYMBOL_CLASS (sym) = LOC_REGPARM_ADDR;
#endif
          /* coulter 08/13/99 - On PA64, FRAME_ARGS_ADDRESS is ((fi)->ap)
             as opposed to ((fi)->frame) which is used on PA32.  Since the
             adjusted value is an offset from the frame pointer and not
             the args pointer, we need to change the symbol class to something
             that is relative to fp.
             */
          SYMBOL_CLASS (sym) = LOC_LOCAL_ARG;
        }
      else
        SYMBOL_VALUE (sym) = paramp->dfparam.location;

      /* Get its type.  */
      SYMBOL_TYPE (sym) = hpread_type_lookup (paramp->dfparam.type, objfile);

      if (lang == language_fortran)
      {
	/* Fortran strings have their length passed on stack at
	   runtime.  dfparam.misc gives the offset of this value relative
	   to the current frame, or a dntt pointer to the DVAR that has
	   the offset, depending on the dfparm.misc_kind value.  Set this
	   offset into the UPPER_BOUND_VALUE for the type. */
	if  (paramp->dfparam.misc)
          {
	    struct type *tmp_type = SYMBOL_TYPE (sym);
	    /* it could be a array of strings so get the element type */
	    if (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY_DESC)
	      tmp_type = TYPE_TARGET_TYPE (tmp_type);
	    while (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY)
	      tmp_type = TYPE_TARGET_TYPE (tmp_type);
	    /* we should end up with a string */
	    if (TYPE_CODE (tmp_type) != TYPE_CODE_STRING)
              {
		warning ("unsupported parameter type for %s",
			 SYMBOL_NAME(sym));
	      }
	    else
              {
		TYPE_ARRAY_UPPER_BOUND_TYPE (tmp_type) =
		  BOUND_BY_VALUE_ON_STACK;
		if (paramp->dfparam.misc_kind == 0)
                  {
		    TYPE_ARRAY_UPPER_BOUND_VALUE (tmp_type) =
		      paramp->dfparam.misc;
		  }
		else if (paramp->dfparam.misc_kind == 1)
                  {
		    dn_tmp = hpread_get_lntt (paramp->dfparam.misc, objfile);
		    TYPE_ARRAY_UPPER_BOUND_VALUE (tmp_type) =
		      dn_tmp->ddvar.location;
		    TYPE_ARRAY_HIGH_BOUND_TYPE (tmp_type) =
		      hpread_type_lookup (dn_tmp->ddvar.type, objfile);
#ifdef HPREAD_ADJUST_STACK_ADDRESS
		    TYPE_ARRAY_UPPER_BOUND_VALUE (tmp_type) +=
		      HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
		  }
		else
                  {
		    warning ("unsupported misc_kind (%d) in string %s ",
			     paramp->dfparam.misc_kind, SYMBOL_NAME(sym));
		  }
	      }
	  }
      }

      /* Add it to the symbol list.  */
      /* Note 1 (RT) At the moment, add_symbol_to_list() is also being
       * called on FPARAM symbols from the process_one_debug_symbol()
       * level... so parameters are getting added twice! (this shows
       * up in the symbol dump you get from "maint print symbols ...").
       * Note 2 (RT) I took out the processing of FPARAM from the 
       * process_one_debug_symbol() level, so at the moment parameters are only
       * being processed here. This seems to have no ill effect.
       */
      /* Note 3 (pai/1997-08-11) I removed the add_symbol_to_list() which put
         each fparam on the local_symbols list from here.  Now we use the
         local_list to which fparams are added below, and set the param_symbols
         global to point to that at the end of this routine. */ 
         
      /* elz: I added this new list of symbols which is local to the function.
         this list is the one which is actually used to build the type for the
         function rather than the gloabal list pointed to by symlist.
         Using a global list to keep track of the parameters is wrong, because 
         this function is called recursively if one parameter happend to be
         a function itself with more parameters in it. Adding parameters to the
         same global symbol list would not work!      
         Actually it did work in case of cc compiled programs where you do not check the
         parameter lists of the arguments.  */
      add_symbol_to_list (sym, &local_list);
    }

  /* If type was read in earlier, don't bother with modifying
     the type struct */ 
  if (!record_args)
    goto finish;
  
  /* Note how many parameters we found.  */
  TYPE_NFIELDS (type) = nsyms;
  TYPE_FIELDS (type) = (struct field *)
    obstack_alloc (&objfile->type_obstack,
                   sizeof (struct field) * nsyms);
  /* Note: The param symbols were already allocated on the obstack; maybe this
     allocation of type fields again on the obstack can be avoided? pai/1997-12-10 */ 

  /* Find the symbols for the parameters and 
     use them to fill parameter-type information into the function-type.
     The parameter symbols can be found in the local_list that we just put them on. */
  /* Note that we preserve the order of the parameters, so
     that in something like "enum {FOO, LAST_THING=FOO}" we print
     FOO, not LAST_THING.  */

  /* get the parameters types from the local list not the global list
     so that the type can be correctly constructed for functions which
     have function as parameters */
  for (syms = local_list, n = 0; syms; syms = syms->next)
    {
      int j = 0;
      for (j = 0; j < syms->nsyms; j++, n++)
	{
	  struct symbol *xsym = syms->symbol[j];
	  TYPE_FIELD_NAME (type, n) = SYMBOL_NAME (xsym);
	  TYPE_FIELD_TYPE (type, n) = SYMBOL_TYPE (xsym);
	  TYPE_FIELD_BITPOS (type, n) = n;
	  TYPE_FIELD_BITSIZE (type, n) = 0;

          /* RM: is this the artificial parameter used by aCC to contain a
             pointer to space allocated for the return value
          */
          if (!strncmp(TYPE_FIELD_NAME(type, n), "#aggretx", 8))
            TYPE_RETVAL_PTR_IDX(type) = n;
	}
    }
  /* Mark it as having been processed */
  TYPE_FLAGS (type) &= ~(TYPE_FLAG_INCOMPLETE);

  /* Check whether we need to fix-up a class type with this function's type */
  if (fixup_class && (fixup_method == type))
    {
      fixup_class_method_type (fixup_class, fixup_method, objfile);
      fixup_class = NULL;
      fixup_method = NULL;
    }

  /* Set the param list of this level of the context stack
     to our local list.  Do this only if this function was
     called for creating a new block, and not if it was called
     simply to get the function type. This prevents recursive
     invocations from trashing param_symbols. */
finish:
  if (newblock)
    param_symbols = local_list;
  else  /* srikanth, 990310, JAGaa80452 : dam(n) the leak ! */
    add_free_pendings (local_list);

  return type;
}


/* Read and internalize a native DOC function debug symbol.  */
/* This is almost identical to hpread_read_function_type(), except
 * for references to dn_bufp->ddocfunc instead of db_bufp->dfunc.
 * Since debug information for DOC functions is more likely to be
 * volatile, please leave it this way.
 */
static struct type *
hpread_read_doc_function_type (hp_type, dn_bufp, objfile, newblock)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
     int newblock;
{
  struct type *type, *type1 = NULL;
  struct pending *syms;
  struct pending *local_list = NULL;
  int nsyms = 0;
  dnttpointer param;
  union dnttentry *paramp;
  char *name;
  long n;
  struct symbol *sym;
  int record_args = 1;
  enum language lang = hpread_language_type (dn_bufp->dfunc.language);
  union dnttentry *dn_tmp;

#ifdef LOG_BETA_DOC_FUNCTION
  log_test_event (LOG_BETA_DOC_FUNCTION, 1);
#endif

  /* See if we've already read in this type.  */
  type = hpread_alloc_type (hp_type, objfile);
  if (TYPE_CODE (type) == TYPE_CODE_FUNC)
    {
      record_args = 0;		/* already read in, don't modify type */
    }
  else
    {
      /* Nope, so read it in and store it away.  */
      if (dn_bufp->dblock.kind == DNTT_TYPE_DOC_FUNCTION ||
	  dn_bufp->dblock.kind == DNTT_TYPE_DOC_MEMFUNC)
        /* FORTRAN subroutines can have DNTTNIL return types so we need to
         * special case them or else call to hpread_type_lookup will issue
         * an "unhandled type code" warning for the return type
         */
        if (lang == language_fortran && dn_bufp->dfunc.retval.word == DNTTNIL)
          type1 = lookup_function_type (lookup_fundamental_type (objfile, FT_VOID));
        else
	  type1 = lookup_function_type (hpread_type_lookup (dn_bufp->ddocfunc.retval,
							    objfile));
      memcpy ((char *) type, (char *) type1, sizeof (struct type));

      /* Mark it -- in the middle of processing */
      TYPE_FLAGS (type) |= TYPE_FLAG_INCOMPLETE;
    }

  /* Now examine each parameter noting its type, location, and a
     wealth of other information.  */
  if (dn_bufp->dblock.kind == DNTT_TYPE_DOC_FUNCTION ||
      dn_bufp->dblock.kind == DNTT_TYPE_DOC_MEMFUNC)
    param = dn_bufp->ddocfunc.firstparam;
  while (param.word && param.word != DNTTNIL)
    {
      paramp = hpread_get_lntt (INDEX(objfile,param.dnttp), objfile);
      nsyms++;
      param = paramp->dfparam.nextparam;

      /* Get the name.  */
      name = VT (objfile) + paramp->dfparam.name;
      sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
					     sizeof (struct symbol));
      (void) memset (sym, 0, sizeof (struct symbol));
      SYMBOL_NAME (sym) = name;
      SYMBOL_LANGUAGE (sym) = lang;

      /* Figure out where it lives.  */
      if (paramp->dfparam.regparam)
	SYMBOL_CLASS (sym) = LOC_REGPARM;
      else if (paramp->dfparam.indirect)
	SYMBOL_CLASS (sym) = LOC_REF_ARG;
      else
	SYMBOL_CLASS (sym) = LOC_ARG;
      SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
      if (paramp->dfparam.copyparam)
	{
	  SYMBOL_VALUE (sym) = paramp->dfparam.location;
#ifdef HPREAD_ADJUST_STACK_ADDRESS
	  SYMBOL_VALUE (sym)
	    += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
	  /* This is likely a pass-by-invisible reference parameter,
	     Hack on the symbol class to make GDB happy.  */
	  /* ??rehrauer: This appears to be broken w/r/t to passing
	     C values of type float and struct.  Perhaps this ought
	     to be highighted as a special case, but for now, just
	     allowing these to be LOC_ARGs seems to work fine.
	   */
#if 0
	  SYMBOL_CLASS (sym) = LOC_REGPARM_ADDR;
#endif
          /* coulter 08/13/99 - On PA64, FRAME_ARGS_ADDRESS is ((fi)->ap)
             as opposed to ((fi)->frame) which is used on PA32.  Since the
             adjusted value is an offset from the frame pointer and not
             the args pointer, we need to change the symbol class to something
             that is relative to fp.
             */
          SYMBOL_CLASS (sym) = LOC_LOCAL_ARG;
	}
      else
	SYMBOL_VALUE (sym) = paramp->dfparam.location;

      /* Get its type.  */
      SYMBOL_TYPE (sym) = hpread_type_lookup (paramp->dfparam.type, objfile);

      if (lang == language_fortran)
        {
	  /* Fortran strings have their length passed on stack at
	     runtime.  dfparam.misc gives the offset of this value relative
	     to the current frame, or a dntt pointer to the DVAR that has
	     the offset, depending on the dfparm.misc_kind value.  Set this
	     offset into the UPPER_BOUND_VALUE for the type. */
	  if  (paramp->dfparam.misc)
	    {
	      struct type *tmp_type = SYMBOL_TYPE (sym);
	      /* it could be a array of strings so get the element type */
	      if (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY_DESC)
		tmp_type = TYPE_TARGET_TYPE (tmp_type);
	      while (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY)
		tmp_type = TYPE_TARGET_TYPE (tmp_type);
	      /* we should end up with a string */
	      if (TYPE_CODE (tmp_type) != TYPE_CODE_STRING)
	        {
		  warning ("unsupported parameter type for %s",
			   SYMBOL_NAME (sym));
		}
	      else
	        {
		  TYPE_ARRAY_UPPER_BOUND_TYPE (tmp_type) =
		    BOUND_BY_VALUE_ON_STACK;
		  if (paramp->dfparam.misc_kind == 0)
		    {
		      TYPE_ARRAY_UPPER_BOUND_VALUE (tmp_type) =
			paramp->dfparam.misc;
		    }
		  else if (paramp->dfparam.misc_kind == 1)
		    {
		      dn_tmp = hpread_get_lntt (paramp->dfparam.misc, objfile);
		      TYPE_ARRAY_UPPER_BOUND_VALUE (tmp_type) =
			dn_tmp->ddvar.location;
		      TYPE_ARRAY_HIGH_BOUND_TYPE (tmp_type) =
			hpread_type_lookup (dn_tmp->ddvar.type, objfile);
#ifdef HPREAD_ADJUST_STACK_ADDRESS
		      TYPE_ARRAY_UPPER_BOUND_VALUE (tmp_type) +=
			HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
		    }
		  else
		    {
		      warning ("unsupported misc_kind (%d) in string %s ",
			       paramp->dfparam.misc_kind, SYMBOL_NAME (sym));
		    }
		}
	    }
	}

#ifdef HPPA_DOC
      if (paramp->dfparam.doc_ranges)
        make_range_list(paramp->dfparam.location, sym, objfile);
#endif

      /* Add it to the symbol list.  */
      /* Note 1 (RT) At the moment, add_symbol_to_list() is also being
       * called on FPARAM symbols from the process_one_debug_symbol()
       * level... so parameters are getting added twice! (this shows
       * up in the symbol dump you get from "maint print symbols ...").
       * Note 2 (RT) I took out the processing of FPARAM from the 
       * process_one_debug_symbol() level, so at the moment parameters are only
       * being processed here. This seems to have no ill effect.
       */
      /* Note 3 (pai/1997-08-11) I removed the add_symbol_to_list() which put
         each fparam on the local_symbols list from here.  Now we use the
         local_list to which fparams are added below, and set the param_symbols
         global to point to that at the end of this routine. */

      /* elz: I added this new list of symbols which is local to the function.
         this list is the one which is actually used to build the type for the
         function rather than the gloabal list pointed to by symlist.
         Using a global list to keep track of the parameters is wrong, because 
         this function is called recursively if one parameter happend to be
         a function itself with more parameters in it. Adding parameters to the
         same global symbol list would not work!      
         Actually it did work in case of cc compiled programs where you do not check the
         parameter lists of the arguments.  */
      add_symbol_to_list (sym, &local_list);
    }

  /* If type was read in earlier, don't bother with modifying
     the type struct */
  if (!record_args)
    goto finish;

  /* Note how many parameters we found.  */
  TYPE_NFIELDS (type) = nsyms;
  TYPE_FIELDS (type) = (struct field *)
    obstack_alloc (&objfile->type_obstack,
		   sizeof (struct field) * nsyms);

  /* Find the symbols for the parameters and 
     use them to fill parameter-type information into the function-type.
     The parameter symbols can be found in the local_list that we just put them on. */
  /* Note that we preserve the order of the parameters, so
     that in something like "enum {FOO, LAST_THING=FOO}" we print
     FOO, not LAST_THING.  */

  /* get the parameters types from the local list not the global list
     so that the type can be correctly constructed for functions which
     have function as parameters
   */
  for (syms = local_list, n = 0; syms; syms = syms->next)
    {
      int j = 0;
      for (j = 0; j < syms->nsyms; j++, n++)
	{
	  struct symbol *xsym = syms->symbol[j];
	  TYPE_FIELD_NAME (type, n) = SYMBOL_NAME (xsym);
	  TYPE_FIELD_TYPE (type, n) = SYMBOL_TYPE (xsym);
	  TYPE_FIELD_BITPOS (type, n) = n;
	  TYPE_FIELD_BITSIZE (type, n) = 0;
	}
    }

  /* Mark it as having been processed */
  TYPE_FLAGS (type) &= ~(TYPE_FLAG_INCOMPLETE);

  /* Check whether we need to fix-up a class type with this function's type */
  if (fixup_class && (fixup_method == type))
    {
      fixup_class_method_type (fixup_class, fixup_method, objfile);
      fixup_class = NULL;
      fixup_method = NULL;
    }

  /* Set the param list of this level of the context stack
     to our local list.  Do this only if this function was
     called for creating a new block, and not if it was called
     simply to get the function type. This prevents recursive
     invocations from trashing param_symbols. */
finish:
  if (newblock)
    param_symbols = local_list;

  return type;
}



/* A file-level variable which keeps track of the current-template
 * being processed. Set in hpread_read_struct_type() while processing
 * a template type. Referred to in hpread_get_nth_templ_arg().
 * Yes, this is a kludge, but it arises from the kludge that already
 * exists in symtab.h, namely the fact that they encode
 * "template argument n" with fundamental type FT_TEMPLATE_ARG and
 * bitlength n. This means that deep in processing fundamental types
 * I need to ask the question "what template am I in the middle of?".
 * The alternative to stuffing a global would be to pass an argument
 * down the chain of calls just for this purpose.
 * 
 * There may be problems handling nested templates... tough.
 */
static struct type *current_template = NULL;

/* Read in and internalize a structure definition.  
 * This same routine is called for struct, union, and class types.
 * Also called for templates, since they build a very similar
 * type entry as for class types.
 */

static struct type *
hpread_read_struct_type (hp_type, dn_bufp, objfile)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{
  /* The data members get linked together into a list of struct nextfield's */
  struct nextfield
    {
      struct nextfield *next;
      struct field field;
      unsigned char attributes;	/* store visibility and virtuality info */
#define ATTR_VIRTUAL 1
#define ATTR_PRIVATE 2
#define ATTR_PROTECT 3
    };


  /* The methods get linked together into a list of struct next_fn_field's */
  struct next_fn_field
    {
      struct next_fn_field *next;
      struct fn_fieldlist field;
      struct fn_field fn_field;
      int num_fn_fields;
    };

  /* The template args get linked together into a list of struct next_template's */
  struct next_template
    {
      struct next_template *next;
      struct template_arg arg;
    };

  /* The template instantiations get linked together into a list of these... */
  struct next_instantiation
    {
      struct next_instantiation *next;
      struct type *t;
    };

  struct type *type, *vtype;
  struct type *baseclass;
  struct type *memtype;
  struct nextfield *list = 0, *tmp_list = 0;
  struct next_fn_field *fn_list = 0;
  struct next_fn_field *fn_p;
  struct next_template *t_new, *t_list = 0;
  struct nextfield *new;
  struct next_fn_field *fn_new;
  struct next_instantiation *i_new, *i_list = 0;
  int n, nfields = 0, n_fn_fields = 0, n_fn_fields_total = 0;
  int n_base_classes = 0, n_templ_args = 0;
  int ninstantiations = 0;
  dnttpointer field, fn_field, parent;
  union dnttentry *fieldp, *fn_fieldp, *parentp;
  int i;
  int static_member = 0;
  int const_member = 0;
  int volatile_member = 0;
  unsigned long vtbl_offset;
  int need_bitvectors = 0;
  char *method_name = NULL;
  char *method_alias = NULL;


  /* Is it something we've already dealt with?  */
  type = hpread_alloc_type (hp_type, objfile);
  if ((TYPE_CODE (type) == TYPE_CODE_STRUCT) ||
      (TYPE_CODE (type) == TYPE_CODE_UNION) ||
      (TYPE_CODE (type) == TYPE_CODE_CLASS) ||
      (TYPE_CODE (type) == TYPE_CODE_TEMPLATE))
    return type;

  /* Get the basic type correct.  */
  if (dn_bufp->dblock.kind == DNTT_TYPE_STRUCT)
    {
      TYPE_CODE (type) = TYPE_CODE_STRUCT;
      TYPE_LENGTH (type) = dn_bufp->dstruct.bitlength / 8;
    }
  else if (dn_bufp->dblock.kind == DNTT_TYPE_UNION)
    {
      TYPE_CODE (type) = TYPE_CODE_UNION;
      TYPE_LENGTH (type) = dn_bufp->dunion.bitlength / 8;
    }
  else if (dn_bufp->dblock.kind == DNTT_TYPE_CLASS)
    {
      TYPE_CODE (type) = TYPE_CODE_CLASS;
      TYPE_LENGTH (type) = dn_bufp->dclass.bitlength / 8;

      /* Overrides the TYPE_CPLUS_SPECIFIC(type) with allocated memory
       * rather than &cplus_struct_default.
       */
      allocate_cplus_struct_type (type);

      /* Fill in declared-type.
       * (The C++ compiler will emit TYPE_CODE_CLASS 
       * for all 3 of "class", "struct"
       * "union", and we have to look at the "class_decl" field if we
       * want to know how it was really declared)
       */
      /* (0==class, 1==union, 2==struct) */
      TYPE_DECLARED_TYPE (type) = dn_bufp->dclass.class_decl;
    }
  else if (dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
    {
      /* Get the basic type correct.  */
      TYPE_CODE (type) = TYPE_CODE_TEMPLATE;
      allocate_cplus_struct_type (type);
      TYPE_DECLARED_TYPE (type) = DECLARED_TYPE_TEMPLATE;
    }
  else
    return type;


  TYPE_FLAGS (type) &= ~TYPE_FLAG_STUB;

  /* For classes, read the parent list.
   * Question (RT): Do we need to do this for templates also?
   */
  if (dn_bufp->dblock.kind == DNTT_TYPE_CLASS)
    {

      /* First read the parent-list (classes from which we derive fields) */
      parent = dn_bufp->dclass.parentlist;
      while (parent.word && parent.word != DNTTNIL)
	{
	  parentp = hpread_get_lntt (INDEX(objfile,parent.dnttp), objfile);

	  /* "parentp" should point to a DNTT_TYPE_INHERITANCE record */

	  /* Get space to record the next field/data-member. */
	  new = (struct nextfield *) alloca (sizeof (struct nextfield));
	  new->next = list;
	  list = new;

	  FIELD_BITSIZE (list->field) = 0;

	  /* The "classname" field is actually a DNTT pointer to the base class */
	  baseclass = hpread_type_lookup (parentp->dinheritance.classname,
					  objfile);
	  FIELD_TYPE (list->field) = baseclass;

	  list->field.name = type_name_no_tag (FIELD_TYPE (list->field));

	  list->attributes = 0;

	  /* Check for virtuality of base, and set the
	   * offset of the base subobject within the object.
	   * (Offset set to -1 for virtual bases (for now).)
	   */
	  if (parentp->dinheritance.Virtual)
	    {
	      B_SET (&(list->attributes), ATTR_VIRTUAL);
	      parentp->dinheritance.offset = -1;
	    }
	  else
	    FIELD_BITPOS (list->field) = parentp->dinheritance.offset;

	  /* Check visibility */
	  switch (parentp->dinheritance.visibility)
	    {
	    case 1:
	      B_SET (&(list->attributes), ATTR_PROTECT);
	      break;
	    case 2:
	      B_SET (&(list->attributes), ATTR_PRIVATE);
	      break;
	    }

	  n_base_classes++;
	  nfields++;

	  parent = parentp->dinheritance.next;
	}
    }

  /* For templates, read the template argument list.
   * This must be done before processing the member list, because
   * the member list may refer back to this. E.g.:
   *   template <class T1, class T2> class q2 {
   *     public:
   *     T1 a;
   *     T2 b;
   *   };
   * We need to read the argument list "T1", "T2" first.
   */
  if (dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
    {
      /* Kludge alert: This stuffs a global "current_template" which
       * is referred to by hpread_get_nth_templ_arg(). The global
       * is cleared at the end of this routine.
       */
      current_template = type;

      /* Read in the argument list */
      field = dn_bufp->dtemplate.arglist;
      while (field.word && field.word != DNTTNIL)
	{
	  /* Get this template argument */
	  fieldp = hpread_get_lntt (INDEX(objfile,field.dnttp), objfile);
	  if (fieldp->dblock.kind != DNTT_TYPE_TEMPLATE_ARG)
	    {
	      warning ("Invalid debug info: Template argument entry is of wrong kind");
	      break;
	    }
	  /* Bump the count */
	  n_templ_args++;
	  /* Allocate and fill in a struct next_template */
	  t_new = (struct next_template *) alloca (sizeof (struct next_template));
	  t_new->next = t_list;
	  t_list = t_new;
	  t_list->arg.name = VT (objfile) + fieldp->dtempl_arg.name;
	  t_list->arg.type = hpread_read_templ_arg_type (field, fieldp,
						 objfile, t_list->arg.name);
	  /* Walk to the next template argument */
	  field = fieldp->dtempl_arg.nextarg;
	}
    }

  TYPE_NTEMPLATE_ARGS (type) = n_templ_args;

  if (n_templ_args > 0)
    TYPE_TEMPLATE_ARGS (type) = (struct template_arg *)
      obstack_alloc (&objfile->type_obstack, sizeof (struct template_arg) * n_templ_args);
  for (n = n_templ_args; t_list; t_list = t_list->next)
    {
      n -= 1;
      TYPE_TEMPLATE_ARG (type, n) = t_list->arg;
    }

  /* Next read in and internalize all the fields/members.  */
  if (dn_bufp->dblock.kind == DNTT_TYPE_STRUCT)
    field = dn_bufp->dstruct.firstfield;
  else if (dn_bufp->dblock.kind == DNTT_TYPE_UNION)
    field = dn_bufp->dunion.firstfield;
  else if (dn_bufp->dblock.kind == DNTT_TYPE_CLASS)
    field = dn_bufp->dclass.memberlist;
  else if (dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
    field = dn_bufp->dtemplate.memberlist;
  else
    field.word = DNTTNIL;

  while (field.word && field.word != DNTTNIL)
    {
      fieldp = hpread_get_lntt (INDEX(objfile, field.dnttp), objfile);

      /* At this point "fieldp" may point to either a DNTT_TYPE_FIELD
       * or a DNTT_TYPE_GENFIELD record. 
       */
      vtbl_offset = 0;
      static_member = 0;
      const_member = 0;
      volatile_member = 0;

      if (fieldp->dblock.kind == DNTT_TYPE_GENFIELD)
	{

	  /* The type will be GENFIELD if the field is a method or
	   * a static member (or some other cases -- see below)
	   */

	  /* Follow a link to get to the record for the field. */
	  fn_field = fieldp->dgenfield.field;
	  fn_fieldp = hpread_get_lntt(INDEX(objfile, fn_field.dnttp),
				      objfile);

	  /* Virtual funcs are indicated by a VFUNC which points to the
	   * real entry
	   */
	  if (fn_fieldp->dblock.kind == DNTT_TYPE_VFUNC)
	    {
	      vtbl_offset = fn_fieldp->dvfunc.vtbl_offset;
	      fn_field = fn_fieldp->dvfunc.funcptr;
	      fn_fieldp = hpread_get_lntt(INDEX(objfile, fn_field.dnttp),
					  objfile);
	    }

	  /* A function's entry may be preceded by a modifier which
	   * labels it static/constant/volatile.
	   */
	  if (fn_fieldp->dblock.kind == DNTT_TYPE_MODIFIER)
	    {
	      static_member = fn_fieldp->dmodifier.m_static;
	      const_member = fn_fieldp->dmodifier.m_const;
	      volatile_member = fn_fieldp->dmodifier.m_volatile;
	      fn_field = fn_fieldp->dmodifier.type;
	      fn_fieldp = hpread_get_lntt(INDEX(objfile, fn_field.dnttp),
					  objfile);
	    }

	  /* Check whether we have a method */
	  if ((fn_fieldp->dblock.kind == DNTT_TYPE_MEMFUNC) ||
	      (fn_fieldp->dblock.kind == DNTT_TYPE_FUNCTION) ||
	      (fn_fieldp->dblock.kind == DNTT_TYPE_DOC_MEMFUNC) ||
	      (fn_fieldp->dblock.kind == DNTT_TYPE_DOC_FUNCTION))
	    {
	      /* Method found */

	      short ix = 0;

	      /* Look up function type of method */
	      memtype = hpread_type_lookup (fn_field, objfile);

	      /* Methods can be seen before classes in the SOM records.
	         If we are processing this class because it's a parameter of a
	         method, at this point the method's type is actually incomplete;
	         we'll have to fix it up later; mark the class for this. */

	      if (TYPE_INCOMPLETE (memtype))
		{
		  TYPE_FLAGS (type) |= TYPE_FLAG_INCOMPLETE;
		  if (fixup_class)
		    warning ("Two classes to fix up for method??  Type information may be incorrect for some classes.");
		  if (fixup_method)
		    warning ("Two methods to be fixed up at once?? Type information may be incorrect for some classes.");
		  fixup_class = type;	/* remember this class has to be fixed up */
		  fixup_method = memtype;	/* remember the method type to be used in fixup */
		}

	      /* HP aCC generates operator names without the "operator" keyword, and
	         generates null strings as names for operators that are 
	         user-defined type conversions to basic types (e.g. operator int ()).
	         So try to reconstruct name as best as possible. */

	      method_name = (char *) (VT (objfile) + fn_fieldp->dfunc.name);
	      method_alias = (char *) (VT (objfile) + fn_fieldp->dfunc.alias);

	      if (!method_name ||	/* no name */
		  !*method_name ||	/* or null name */
		  cplus_mangle_opname (method_name, DMGL_ANSI))		/* or name is an operator like "<" */
		{
		  char *tmp_name = cplus_demangle (method_alias, DMGL_ANSI);
		  char *op_string = strstr (tmp_name, "operator");
		  method_name = xmalloc (strlen (op_string) + 1);	/* don't overwrite VT! */
		  strcpy (method_name, op_string);
		  free (tmp_name);  /* srikanth, 990310, JAGaa80452 */
		}

	      /* First check if a method of the same name has already been seen. */
	      fn_p = fn_list;
	      while (fn_p)
		{
		  if (STREQ (fn_p->field.name, method_name))
		    break;
		  fn_p = fn_p->next;
		}

	      /* If no such method was found, allocate a new entry in the list */
	      if (!fn_p)
		{
		  /* Get space to record this member function */
		  /* Note: alloca used; this will disappear on routine exit */
		  fn_new = (struct next_fn_field *) alloca (sizeof (struct next_fn_field));
		  fn_new->next = fn_list;
		  fn_list = fn_new;

		  /* Fill in the fields of the struct nextfield */

		  /* Record the (unmangled) method name */
		  fn_list->field.name = method_name;
		  /* Initial space for overloaded methods */
		  /* Note: xmalloc is used; this will persist after this routine exits */
		  fn_list->field.fn_fields = (struct fn_field *) xmalloc (5 * (sizeof (struct fn_field)));
		  fn_list->field.length = 1;	/* Init # of overloaded instances */
		  fn_list->num_fn_fields = 5;	/* # of entries for which space allocated */
		  fn_p = fn_list;
		  ix = 0;	/* array index for fn_field */
		  /* Bump the total count of the distinctly named methods */
		  n_fn_fields++;
		}
	      else
		/* Another overloaded instance of an already seen method name */
		{
		  if (++(fn_p->field.length) > fn_p->num_fn_fields)
		    {
		      /* Increase space allocated for overloaded instances */
		      fn_p->field.fn_fields
			= (struct fn_field *) xrealloc (fn_p->field.fn_fields,
		      (fn_p->num_fn_fields + 5) * sizeof (struct fn_field));
		      fn_p->num_fn_fields += 5;
		    }
		  ix = fn_p->field.length - 1;	/* array index for fn_field */
		}

	      /* "physname" is intended to be the name of this overloaded instance. */
	      if ((fn_fieldp->dfunc.language == HP_LANGUAGE_CPLUSPLUS) &&
		  method_alias &&
		  *method_alias)	/* not a null string */
		fn_p->field.fn_fields[ix].physname = method_alias;
	      else
		fn_p->field.fn_fields[ix].physname = method_name;
	      /* What's expected here is the function type */
	      /* But mark it as NULL if the method was incompletely processed
	         We'll fix this up later when the method is fully processed */
	      if (TYPE_INCOMPLETE (memtype))
		{
		  fn_p->field.fn_fields[ix].type = NULL;
		  fn_p->field.fn_fields[ix].args = NULL;
		}
	      else
		{
		  fn_p->field.fn_fields[ix].type = memtype;

		  /* The argument list */
		  fn_p->field.fn_fields[ix].type->type_specific.arg_types =
		    (struct type **) obstack_alloc (&objfile->type_obstack,
			   sizeof (struct type *) * (memtype->nfields + 1));
		  for (i = 0; i < memtype->nfields; i++)
		    fn_p->field.fn_fields[ix].type->type_specific.arg_types[i] = memtype->fields[i].type;
		  /* void termination */
		  fn_p->field.fn_fields[ix].type->type_specific.arg_types[memtype->nfields] = builtin_type_void;

		  /* pai: It's not clear why this args field has to be set.  Perhaps
		   * it should be eliminated entirely. */
		  fn_p->field.fn_fields[ix].args =
		    (struct type **) obstack_alloc (&objfile->type_obstack,
			   sizeof (struct type *) * (memtype->nfields + 1));
		  for (i = 0; i < memtype->nfields; i++)
		    fn_p->field.fn_fields[ix].args[i] = memtype->fields[i].type;
		  /* null-terminated, unlike arg_types above e */
		  fn_p->field.fn_fields[ix].args[memtype->nfields] = NULL;
		}
	      /* For virtual functions, fill in the voffset field with the
	       * virtual table offset. (This is just copied over from the
	       * SOM record; not sure if it is what GDB expects here...).
	       * But if the function is a static method, set it to 1.
	       * 
	       * Note that we have to add 1 because 1 indicates a static
	       * method, and 0 indicates a non-static, non-virtual method */

	      if (static_member)
		fn_p->field.fn_fields[ix].voffset = VOFFSET_STATIC;
	      else
		fn_p->field.fn_fields[ix].voffset = vtbl_offset ? vtbl_offset + 1 : 0;

	      /* Also fill in the fcontext field with the current
	       * class. (The latter isn't quite right: should be the baseclass
	       * that defines the virtual function... Note we do have
	       * a variable "baseclass" that we could stuff into the fcontext
	       * field, but "baseclass" isn't necessarily right either,
	       * since the virtual function could have been defined more
	       * than one level up).
	       */

	      if (vtbl_offset != 0)
		fn_p->field.fn_fields[ix].fcontext = type;
	      else
		fn_p->field.fn_fields[ix].fcontext = NULL;

	      /* Other random fields pertaining to this method */
	      fn_p->field.fn_fields[ix].is_const = const_member;
	      fn_p->field.fn_fields[ix].is_volatile = volatile_member;	/* ?? */
	      switch (fieldp->dgenfield.visibility)
		{
		case 1:
		  fn_p->field.fn_fields[ix].is_protected = 1;
		  fn_p->field.fn_fields[ix].is_private = 0;
		  break;
		case 2:
		  fn_p->field.fn_fields[ix].is_protected = 0;
		  fn_p->field.fn_fields[ix].is_private = 1;
		  break;
		default:	/* public */
		  fn_p->field.fn_fields[ix].is_protected = 0;
		  fn_p->field.fn_fields[ix].is_private = 0;
		}
	      fn_p->field.fn_fields[ix].is_stub = 0;

	      /* HP aCC emits both MEMFUNC and FUNCTION entries for a method;
	         if the class points to the FUNCTION, there is usually separate
	         code for the method; but if we have a MEMFUNC, the method has
	         been inlined (and there is usually no FUNCTION entry)
	         FIXME Not sure if this test is accurate. pai/1997-08-22 */
	      if ((fn_fieldp->dblock.kind == DNTT_TYPE_MEMFUNC) ||
		  (fn_fieldp->dblock.kind == DNTT_TYPE_DOC_MEMFUNC))
		fn_p->field.fn_fields[ix].is_inlined = 1;
	      else
		fn_p->field.fn_fields[ix].is_inlined = 0;

	      fn_p->field.fn_fields[ix].dummy = 0;

	      /* Bump the total count of the member functions */
	      n_fn_fields_total++;

	    }
	  else if (fn_fieldp->dblock.kind == DNTT_TYPE_SVAR)
	    {
	      /* This case is for static data members of classes */

	      /* pai:: FIXME -- check that "staticmem" bit is set */

	      /* Get space to record this static member */
	      new = (struct nextfield *) alloca (sizeof (struct nextfield));
	      new->next = list;
	      list = new;

	      list->field.name = VT (objfile) + fn_fieldp->dsvar.name;
	      FIELD_BITSIZE (list->field) = -1;		/* indicates static member */
	      SET_FIELD_PHYSNAME (list->field, 0);	/* initialize to empty */
	      memtype = hpread_type_lookup (fn_fieldp->dsvar.type, objfile);

	      FIELD_TYPE (list->field) = memtype;
	      list->attributes = 0;
	      switch (fieldp->dgenfield.visibility)
		{
		case 1:
		  B_SET (&(list->attributes), ATTR_PROTECT);
		  break;
		case 2:
		  B_SET (&(list->attributes), ATTR_PRIVATE);
		  break;
		}
	      nfields++;
	    }

	  else if (fn_fieldp->dblock.kind == DNTT_TYPE_FIELD)
	    {
	      /* FIELDs follow GENFIELDs for fields of anonymous unions.
	         Code below is replicated from the case for FIELDs further
	         below, except that fieldp is replaced by fn_fieldp */
	      if (!fn_fieldp->dfield.a_union)
		warning ("Debug info inconsistent: FIELD of anonymous union doesn't have a_union bit set");
	      /* Get space to record the next field/data-member. */
	      new = (struct nextfield *) alloca (sizeof (struct nextfield));
	      new->next = list;
	      list = new;

	      list->field.name = VT (objfile) + fn_fieldp->dfield.name;
	      FIELD_BITPOS (list->field) = fn_fieldp->dfield.bitoffset;

	      /* RM: The following test is okay if bitlength is 8, 16 or 32,
	       	 but not if bitlength is 24. Replaced with test below. */
	      /*
		if (fn_fieldp->dfield.bitlength % 8)
	      */
	      /*
		if ((fn_fieldp->dfield.type.dntti.immediate) &&
		    (fn_fieldp->dfield.bitlength !=
		     fn_fieldp->dfield.type.dntti.bitlength))
		  list->field.bitsize = fn_fieldp->dfield.bitlength;
		else 
		  list->field.bitsize = 0;
	      */

	      memtype = hpread_type_lookup (fn_fieldp->dfield.type, objfile);

	      /* RM: Is the length of the field equal to the length of
	       	 the underlying type? */
	      if (is_integral_type(memtype) &&
		  (memtype->length * 8 != fn_fieldp->dfield.bitlength))
		list->field.bitsize = fn_fieldp->dfield.bitlength;
	      else 
		list->field.bitsize = 0;
            
	      list->field.type = memtype;
	      list->attributes = 0;
	      /* coulter: a FIELD may still have staticMem set */
	      if (fn_fieldp->dfield.staticmem)
                {
		  FIELD_BITSIZE (list->field) = -1; /* indicates static member */
		  SET_FIELD_PHYSNAME (list->field, 0);  /* initialize to empty */
		}
	      switch (fn_fieldp->dfield.visibility)
		{
		case 1:
		  B_SET (&(list->attributes), ATTR_PROTECT);
		  break;
		case 2:
		  B_SET (&(list->attributes), ATTR_PRIVATE);
		  break;
		}
	      nfields++;
	    }
	  else if (fn_fieldp->dblock.kind == DNTT_TYPE_SVAR)
	    {
	      /* Field of anonymous union; union is not inside a class */
	      if (!fn_fieldp->dsvar.a_union)
		warning ("Debug info inconsistent: SVAR field in anonymous union doesn't have a_union bit set");
	      /* Get space to record the next field/data-member. */
	      new = (struct nextfield *) alloca (sizeof (struct nextfield));
	      new->next = list;
	      list = new;

	      list->field.name = VT (objfile) + fn_fieldp->dsvar.name;
	      FIELD_BITPOS (list->field) = 0;	/* FIXME is this always true? */
	      FIELD_BITSIZE (list->field) = 0;	/* use length from type */
	      memtype = hpread_type_lookup (fn_fieldp->dsvar.type, objfile);
	      list->field.type = memtype;
	      list->attributes = 0;
	      /* No info to set visibility -- always public */
	      nfields++;
	    }
	  else if (fn_fieldp->dblock.kind == DNTT_TYPE_DVAR)
	    {
	      /* Field of anonymous union; union is not inside a class */
	      if (!fn_fieldp->ddvar.a_union)
		warning ("Debug info inconsistent: DVAR field in anonymous union doesn't have a_union bit set");
	      /* Get space to record the next field/data-member. */
	      new = (struct nextfield *) alloca (sizeof (struct nextfield));
	      new->next = list;
	      list = new;

	      list->field.name = VT (objfile) + fn_fieldp->ddvar.name;
	      FIELD_BITPOS (list->field) = 0;	/* FIXME is this always true? */
	      FIELD_BITSIZE (list->field) = 0;	/* use length from type */
	      memtype = hpread_type_lookup (fn_fieldp->ddvar.type, objfile);
	      list->field.type = memtype;
	      list->attributes = 0;
	      /* No info to set visibility -- always public */
	      nfields++;
	    }
	  else
	    {			/* Not a method, nor a static data member, nor an anon union field */

	      /* This case is for miscellaneous type entries (local enums,
	         local function templates, etc.) that can be present
	         inside a class. */

	      /* Enums -- will be handled by other code that takes care
	         of DNTT_TYPE_ENUM; here we see only DNTT_TYPE_MEMENUM so
	         it's not clear we could have handled them here at all. */
	      /* FUNC_TEMPLATE: is handled by other code (??). */
	      /* MEMACCESS: modified access for inherited member. Not
	         sure what to do with this, ignoriing it at present. */

	      /* What other entries can appear following a GENFIELD which
	         we do not handle above?  (MODIFIER, VFUNC handled above.) */

	      if ((fn_fieldp->dblock.kind != DNTT_TYPE_MEMACCESS) &&
		  (fn_fieldp->dblock.kind != DNTT_TYPE_MEMENUM) &&
		  (fn_fieldp->dblock.kind != DNTT_TYPE_FUNC_TEMPLATE))
		warning ("Internal error: Unexpected debug record kind %d found following DNTT_GENFIELD",
			 fn_fieldp->dblock.kind);
	    }
	  /* walk to the next FIELD or GENFIELD */
	  field = fieldp->dgenfield.nextfield;

	}
      else if (fieldp->dblock.kind == DNTT_TYPE_FIELD)
	{

	  /* Ordinary structure/union/class field */
	  struct type *anon_union_type;

	  /* Get space to record the next field/data-member. */
	  new = (struct nextfield *) alloca (sizeof (struct nextfield));
	  new->next = list;
	  list = new;

	  list->field.name = VT (objfile) + fieldp->dfield.name;
          memtype = hpread_type_lookup (fieldp->dfield.type, objfile);

          if (current_language->la_language == language_fortran)
            {
             /* f77 emits "@<digit>" for unions and fields of unions, 
              * so trap it here to avoid printing this internal name
              */
              if (list->field.name &&
                  list->field.name[0] == '@' && isdigit (list->field.name[1]))
                list->field.name = "";
              /* Unions have "map" fields whose type is represented as
               * a structure  -- we flag this so that we can print out
               * these fields as "map" instead of "type" fields 
               */
              if  (list->field.name && *list->field.name == NULL
                  && TYPE_CODE (type) == TYPE_CODE_UNION
                  && TYPE_CODE (memtype) == TYPE_CODE_STRUCT)
                TYPE_FLAGS (memtype) |= TYPE_FLAG_MAP;
            }

	  /* A FIELD by itself (without a GENFIELD) can also be a static member */
	  if (fieldp->dfield.staticmem)
	    {
	      FIELD_BITSIZE (list->field) = -1; /* indicates static member
              	                                   value defined by physname */
	      SET_FIELD_PHYSNAME (list->field, 0);  /* initialize to empty */
	    }
	  else
	    /* Non-static data member */
	    {
	      FIELD_BITPOS (list->field) = fieldp->dfield.bitoffset;

	      /* RM: The following test is okay if bitlength is 8, 16 or 32,
	         but not if bitlength is 24. Replaced with test below. */
	      /*
		if (fieldp->dfield.bitlength % 8)
	      */
	      /*
		if ((fieldp->dfield.type.dntti.immediate) &&
		    (fieldp->dfield.bitlength !=
		     fieldp->dfield.type.dntti.bitlength))
		   list->field.bitsize = fieldp->dfield.bitlength;
		else 
		   list->field.bitsize = 0;
	      */

	      /* RM: Is the length of the field equal to the length of
             	 the underlying type? */
	      if (is_integral_type(memtype) &&
		  (memtype->length * 8 != fieldp->dfield.bitlength))
		list->field.bitsize = fieldp->dfield.bitlength;
	      else
		FIELD_BITSIZE (list->field) = 0;
	    }

	  FIELD_TYPE (list->field) = memtype;
	  list->attributes = 0;
	  switch (fieldp->dfield.visibility)
	    {
	    case 1:
	      B_SET (&(list->attributes), ATTR_PROTECT);
	      break;
	    case 2:
	      B_SET (&(list->attributes), ATTR_PRIVATE);
	      break;
	    }
	  nfields++;


          /* Note 1: First, we have to check if the current field is an 
             anonymous union. If it is, then *its* fields are threaded along 
             in the nextfield chain. :-( This was supposed to help debuggers,
             but is really just a nuisance since we deal with anonymous unions
             anyway by checking that the name is null.  So anyway, we skip over
             the fields of the anonymous union. pai/1997-08-22 */
          /* Note 2: In addition, the bitoffsets for the fields of the anon
             union are relative to the enclosing struct, *NOT* relative to the 
             anon union!  This is an even bigger nuisance -- we have to go in
             and munge the anon union's type information appropriately.
             pai/1997-08-22 */

          /* Both tasks noted above are done by a separate function.  This 
             takes us to the next FIELD or GENFIELD, skipping anon unions, and
             recursively processing intermediate types. */

          /* Fortran anonymous unions aren't threaded the way aCC's are 
           * so skip the hpread_get_next_skip_over_anon_unions rigamarole */

          if (current_language->la_language == language_fortran)
            field = fieldp->dfield.nextfield;
          else
	    field = hpread_get_next_skip_over_anon_unions (1, field, &fieldp,
							   objfile);
	}
      else
	{
	  /* neither field nor genfield ?? is this possible?? */
	  /* FIXME Ideally, we should walk to the next field and re-sync,
	     but how? */
	  warning ("Internal error: unexpected DNTT kind %d encountered as field of struct",
		   fieldp->dblock.kind);
	  warning ("Skipping remaining fields of struct");
	  break;		/* get out of loop of fields */
	}
    }

  /* If it's a template, read in the instantiation list */
  if (dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
    {
      ninstantiations = 0;
      field = dn_bufp->dtemplate.expansions;
      while (field.word && field.word != DNTTNIL)
	{
	  fieldp = hpread_get_lntt (INDEX(objfile, field.dnttp), objfile);

	  /* The expansions or nextexp should point to a tagdef */
	  if (fieldp->dblock.kind != DNTT_TYPE_TAGDEF)
	    break;

	  i_new = (struct next_instantiation *) alloca (sizeof (struct next_instantiation));
	  i_new->next = i_list;
	  i_list = i_new;
	  i_list->t = hpread_type_lookup (field, objfile);
	  ninstantiations++;

	  /* And the "type" field of that should point to a class */
	  field = fieldp->dtag.type;
	  fieldp = hpread_get_lntt (INDEX(objfile, field.dnttp), objfile);
	  if (fieldp->dblock.kind != DNTT_TYPE_CLASS)
	    break;

	  /* Get the next expansion */
	  field = fieldp->dclass.nextexp;
	}
    }
  TYPE_NINSTANTIATIONS (type) = ninstantiations;
  if (ninstantiations > 0)
    TYPE_INSTANTIATIONS (type) = (struct type **)
      obstack_alloc (&objfile->type_obstack, sizeof (struct type *) * ninstantiations);
  for (n = ninstantiations; i_list; i_list = i_list->next)
    {
      n -= 1;
      TYPE_INSTANTIATION (type, n) = i_list->t;
    }


  /* Copy the field-list to GDB's symbol table */
  /* RM: When setting up the fields of this type, we may have created
     const/volatile variants of this type. All these need to be
     updated */
  for (vtype = type->cv_type; 1; vtype = vtype->cv_type)
    {
      TYPE_NFIELDS (vtype) = nfields;
      TYPE_N_BASECLASSES (vtype) = n_base_classes;
      TYPE_FIELDS (vtype) = (struct field *)
			    obstack_alloc (&objfile->type_obstack,
					   sizeof (struct field) * nfields);
      /* Copy the saved-up fields into the field vector.  */
      for (n = nfields, tmp_list = list; tmp_list; tmp_list = tmp_list->next)
        {
	  n -= 1;
          TYPE_FIELD (vtype, n) = tmp_list->field;
	}
      if (vtype == type)
        break;
    }

  /* Copy the "function-field-list" (i.e., the list of member
   * functions in the class) to GDB's symbol table 
   */
  TYPE_NFN_FIELDS (type) = n_fn_fields;
  TYPE_NFN_FIELDS_TOTAL (type) = n_fn_fields_total;
  TYPE_FN_FIELDLISTS (type) = (struct fn_fieldlist *)
    obstack_alloc (&objfile->type_obstack, sizeof (struct fn_fieldlist) * n_fn_fields);
  for (n = n_fn_fields; fn_list; fn_list = fn_list->next)
    {
      n -= 1;
      TYPE_FN_FIELDLIST (type, n) = fn_list->field;
    }

  /* pai:: FIXME -- perhaps each bitvector should be created individually */
  for (n = nfields, tmp_list = list; tmp_list; tmp_list = tmp_list->next)
    {
      n -= 1;
      if (tmp_list->attributes)
	{
	  need_bitvectors = 1;
	  break;
	}
    }

  if (need_bitvectors)
    {
      /* pai:: this step probably redundant */
      ALLOCATE_CPLUS_STRUCT_TYPE (type);

      TYPE_FIELD_VIRTUAL_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_VIRTUAL_BITS (type), nfields);

      TYPE_FIELD_PRIVATE_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_PRIVATE_BITS (type), nfields);

      TYPE_FIELD_PROTECTED_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_PROTECTED_BITS (type), nfields);

      /* this field vector isn't actually used with HP aCC */
      TYPE_FIELD_IGNORE_BITS (type) =
	(B_TYPE *) TYPE_ALLOC (type, B_BYTES (nfields));
      B_CLRALL (TYPE_FIELD_IGNORE_BITS (type), nfields);

      while (nfields-- > 0)
	{
	  if (B_TST (&(list->attributes), ATTR_VIRTUAL))
	    SET_TYPE_FIELD_VIRTUAL (type, nfields);
	  if (B_TST (&(list->attributes), ATTR_PRIVATE))
	    SET_TYPE_FIELD_PRIVATE (type, nfields);
	  if (B_TST (&(list->attributes), ATTR_PROTECT))
	    SET_TYPE_FIELD_PROTECTED (type, nfields);

	  list = list->next;
	}
    }
  else
    {
      TYPE_FIELD_VIRTUAL_BITS (type) = NULL;
      TYPE_FIELD_PROTECTED_BITS (type) = NULL;
      TYPE_FIELD_PRIVATE_BITS (type) = NULL;
    }

  if (has_vtable (type))
    {
      /* Allocate space for class runtime information */
      TYPE_RUNTIME_PTR (type) = (struct runtime_info *) xmalloc (sizeof (struct runtime_info));
      /* Set flag for vtable */
      TYPE_VTABLE (type) = 1;
      /* The first non-virtual base class with a vtable. */
      TYPE_PRIMARY_BASE (type) = primary_base_class (type);
      /* The virtual base list. */
      TYPE_VIRTUAL_BASE_LIST (type) = virtual_base_list (type);
    }
  else
    TYPE_RUNTIME_PTR (type) = NULL;
  
  /* srikanth, 000122, There was some provision for recording the SPOS
     information for C++ local types here. I removed it as it is not
     possible to get that information anymore. (basically, it was scanning
     the lntt backwards, given an LNTT entry. Now with demand paging of LNTT,
     there is no way to determine given an LNTT entry, what its index is.)

     It was also not terribly useful stuff. All we get from it is the SPOS
     info on ptype output. See that no other configuration of GDB provides
     this. So if anything, this actually makes us more compatible :-)
  */

  /* Clear the global saying what template we are in the middle of processing */
  current_template = NULL;

  return type;
}

/* Adjust the physnames for each static member of a struct
   or class type to be something like "A::x"; then various
   other pieces of code that do a lookup_symbol on the phyname
   work correctly.
   TYPE is a pointer to the struct/class type
   NAME is a char * (string) which is the class/struct name
   Void return */

static void
fix_static_member_physnames (type, class_name, objfile)
     struct type *type;
     char *class_name;
     struct objfile *objfile;
{
  int i;

  /* We fix the member names only for classes or structs */
  if (TYPE_CODE (type) != TYPE_CODE_STRUCT)
    return;

  for (i = 0; i < TYPE_NFIELDS (type); i++)
    if (TYPE_FIELD_STATIC (type, i))
      {
	if (TYPE_FIELD_STATIC_PHYSNAME (type, i))
	  return;		/* physnames are already set */

	SET_FIELD_PHYSNAME (type->fields[i],
			    obstack_alloc (&objfile->type_obstack,
	     strlen (class_name) + strlen (TYPE_FIELD_NAME (type, i)) + 3));
	strcpy (TYPE_FIELD_STATIC_PHYSNAME (type, i), class_name);
	strcat (TYPE_FIELD_STATIC_PHYSNAME (type, i), "::");
	strcat (TYPE_FIELD_STATIC_PHYSNAME (type, i), TYPE_FIELD_NAME (type, i));
      }
}

/* Fix-up the type structure for a CLASS so that the type entry
 * for a method (previously marked with a null type in hpread_read_struct_type()
 * is set correctly to METHOD.
 * OBJFILE is as for other such functions. 
 * Void return. */

static void
fixup_class_method_type (class, method, objfile)
     struct type *class;
     struct type *method;
     struct objfile *objfile;
{
  int i, j, k;

  if (!class || !method || !objfile)
    return;

  /* Only for types that have methods */
  if ((TYPE_CODE (class) != TYPE_CODE_CLASS) &&
      (TYPE_CODE (class) != TYPE_CODE_UNION))
    return;

  /* Loop over all methods and find the one marked with a NULL type */
  for (i = 0; i < TYPE_NFN_FIELDS (class); i++)
    for (j = 0; j < TYPE_FN_FIELDLIST_LENGTH (class, i); j++)
      if (TYPE_FN_FIELD_TYPE (TYPE_FN_FIELDLIST1 (class, i), j) == NULL)
	{
	  /* Set the method type */
	  TYPE_FN_FIELD_TYPE (TYPE_FN_FIELDLIST1 (class, i), j) = method;
	  /* The argument list */
	  (TYPE_FN_FIELD_TYPE (TYPE_FN_FIELDLIST1 (class, i), j))->type_specific.arg_types
	    = (struct type **) obstack_alloc (&objfile->type_obstack,
			    sizeof (struct type *) * (method->nfields + 1));
	  for (k = 0; k < method->nfields; k++)
	    (TYPE_FN_FIELD_TYPE (TYPE_FN_FIELDLIST1 (class, i), j))->type_specific.arg_types[k] = method->fields[k].type;
	  /* void termination */
	  (TYPE_FN_FIELD_TYPE (TYPE_FN_FIELDLIST1 (class, i), j))->type_specific.arg_types[method->nfields] = builtin_type_void;

	  /* pai: It's not clear why this args field has to be set.  Perhaps
	   * it should be eliminated entirely. */
	  (TYPE_FN_FIELD (TYPE_FN_FIELDLIST1 (class, i), j)).args
	    = (struct type **) obstack_alloc (&objfile->type_obstack,
			    sizeof (struct type *) * (method->nfields + 1));
	  for (k = 0; k < method->nfields; k++)
	    (TYPE_FN_FIELD (TYPE_FN_FIELDLIST1 (class, i), j)).args[k] = method->fields[k].type;
	  /* null-terminated, unlike arg_types above */
	  (TYPE_FN_FIELD (TYPE_FN_FIELDLIST1 (class, i), j)).args[method->nfields] = NULL;

	  /* Break out of both loops -- only one method to fix up in a class */
	  goto finish;
	}

finish:
  TYPE_FLAGS (class) &= ~TYPE_FLAG_INCOMPLETE;
}


/* If we're in the middle of processing a template, get a pointer
 * to the Nth template argument.
 * An example may make this clearer:
 *   template <class T1, class T2> class q2 {
 *     public:
 *     T1 a;
 *     T2 b;
 *   };
 * The type for "a" will be "first template arg" and
 * the type for "b" will be "second template arg".
 * We need to look these up in order to fill in "a" and "b"'s type.
 * This is called from hpread_type_lookup().
 */
static struct type *
hpread_get_nth_template_arg (objfile, n)
     struct objfile *objfile;
     int n;
{
  if (current_template != NULL)
    return TYPE_TEMPLATE_ARG (current_template, n).type;
  else
    return lookup_fundamental_type (objfile, FT_TEMPLATE_ARG);
}

/* Read in and internalize a TEMPL_ARG (template arg) symbol.  */

static struct type *
hpread_read_templ_arg_type (hp_type, dn_bufp, objfile, name)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
     char *name;
{
  struct type *type;

  /* See if it's something we've already deal with.  */
  type = hpread_alloc_type (hp_type, objfile);
  if (TYPE_CODE (type) == TYPE_CODE_TEMPLATE_ARG)
    return type;

  /* Nope.  Fill in the appropriate fields.  */
  TYPE_CODE (type) = TYPE_CODE_TEMPLATE_ARG;
  TYPE_LENGTH (type) = 0;
  TYPE_NFIELDS (type) = 0;
  TYPE_NAME (type) = name;
  return type;
}

/* Read in and internalize a set debug symbol.  */

static struct type *
hpread_read_set_type (hp_type, dn_bufp, objfile)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{
  struct type *type;

  /* See if it's something we've already deal with.  */
  type = hpread_alloc_type (hp_type, objfile);
  if (TYPE_CODE (type) == TYPE_CODE_SET)
    return type;

  /* Nope.  Fill in the appropriate fields.  */
  TYPE_CODE (type) = TYPE_CODE_SET;
  TYPE_LENGTH (type) = dn_bufp->dset.bitlength / 8;
  TYPE_NFIELDS (type) = 0;
  TYPE_TARGET_TYPE (type) = hpread_type_lookup (dn_bufp->dset.subtype,
						objfile);
  return type;
}

/* Read in and internalize an array debug symbol.  */

static struct type *
hpread_read_array_type (hp_type, dn_bufp, objfile)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{
  struct type *type;

  /* Allocate an array type symbol.
   * Why no check for already-read here, like in the other
   * hpread_read_xxx_type routines?  Because it kept us 
   * from properly determining the size of the array!  
   */
  type = hpread_alloc_type (hp_type, objfile);

  TYPE_CODE (type) = TYPE_CODE_ARRAY;

  /* Although the hp-symtab.h does not *require* this to be the case,
   * GDB is assuming that "arrayisbytes" and "elemisbytes" be consistent.
   * I.e., express both array-length and element-length in bits,
   * or express both array-length and element-length in bytes.
   */
  if (!((dn_bufp->darray.arrayisbytes && dn_bufp->darray.elemisbytes) ||
	(!dn_bufp->darray.arrayisbytes && !dn_bufp->darray.elemisbytes)))
    {
      warning ("error in hpread_array_type.\n");
      return;
    }
  else if (dn_bufp->darray.arraylength == 0x7fffffff ||
	   dn_bufp->darray.arraylength == 0x0fffffff )
    {
      /* The HP debug format represents char foo[]; as an array with
	 length 0x7fffffff or 0x0fffffff in the case of f77.  Internally 
       	 GDB wants to represent this as an array of length zero.  
       	 as an array of length zero. */
      TYPE_LENGTH (type) = 0;
    }
  else if (dn_bufp->darray.arrayisbytes)
    TYPE_LENGTH (type) = dn_bufp->darray.arraylength;
  else				/* arraylength is in bits */
    TYPE_LENGTH (type) = dn_bufp->darray.arraylength / 8;

  TYPE_TARGET_TYPE (type) = hpread_type_lookup (dn_bufp->darray.elemtype,
						objfile);

  /* The one "field" is used to store the subscript type */
  /* Since C and C++ multi-dimensional arrays are simply represented
   * as: array of array of ..., we only need one subscript-type
   * per array. This subscript type is typically a subrange of integer.
   * If this gets extended to support languages like Pascal, then
   * we need to fix this to represent multi-dimensional arrays properly.
   */
  TYPE_NFIELDS (type) = 1;
  TYPE_FIELDS (type) = (struct field *)
    obstack_alloc (&objfile->type_obstack, sizeof (struct field));
  TYPE_FIELD_TYPE (type, 0) = hpread_type_lookup (dn_bufp->darray.indextype,
						  objfile);

  if ( TYPE_LENGTH (type) == 0 && TYPE_ARRAY_UPPER_BOUND_VALUE (type) == 0x7fffffff)
    TYPE_ARRAY_UPPER_BOUND_TYPE (type) = BOUND_CANNOT_BE_DETERMINED;
  else
  {
  if (dn_bufp->darray.dyn_low)
    {
      if (dn_bufp->darray.dyn_low == 1)
        {
          TYPE_ARRAY_LOWER_BOUND_TYPE (type) = BOUND_BY_VALUE_ON_STACK;
#ifdef HPREAD_ADJUST_STACK_ADDRESS
          TYPE_ARRAY_LOWER_BOUND_VALUE (type)
            += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
          TYPE_ARRAY_LOW_BOUND_TYPE (type) = TYPE_TARGET_TYPE(TYPE_FIELD_TYPE (type, 0));
        }
      else /* dn_bufp->darray.dyn_low == 2 */
        TYPE_ARRAY_LOWER_BOUND_TYPE (type) = BOUND_BY_VARIABLE;
    }

  if (dn_bufp->darray.dyn_high)
    {
      if (dn_bufp->darray.dyn_high == 1)
        {
          TYPE_ARRAY_UPPER_BOUND_TYPE (type) = BOUND_BY_VALUE_ON_STACK;
#ifdef HPREAD_ADJUST_STACK_ADDRESS
          TYPE_ARRAY_UPPER_BOUND_VALUE (type)
            += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
          TYPE_ARRAY_HIGH_BOUND_TYPE (type) = TYPE_TARGET_TYPE(TYPE_FIELD_TYPE (type, 0));
        }
      else /* dn_bufp->darray.dyn_high == 2 */
        TYPE_ARRAY_UPPER_BOUND_TYPE (type) = BOUND_BY_VARIABLE;

    }
  }

  return type;
}

/* Read in and internalize a subrange debug symbol.  */
static struct type *
hpread_read_subrange_type (hp_type, dn_bufp, objfile)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{
  struct type *type;

  /* Is it something we've already dealt with.  */
  type = hpread_alloc_type (hp_type, objfile);
  if (TYPE_CODE (type) == TYPE_CODE_RANGE)
    return type;

  /* Nope, internalize it.  */
  TYPE_CODE (type) = TYPE_CODE_RANGE;
  TYPE_LENGTH (type) = dn_bufp->dsubr.bitlength / 8;
  TYPE_NFIELDS (type) = 2;
  TYPE_FIELDS (type)
    = (struct field *) obstack_alloc (&objfile->type_obstack,
				      2 * sizeof (struct field));

  switch (dn_bufp->dsubr.dyn_low)
    {
    case 0:
    case 1:
      TYPE_FIELD_BITPOS (type, 0) = dn_bufp->dsubr.lowbound;
      break;

    case 2:
      {
        union dnttentry *var;
        struct symbol *sym;

        sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
                                               sizeof (struct symbol));
        (void) memset (sym, 0, sizeof (struct symbol));
        var = hpread_get_lntt (dn_bufp->dsubr.lowbound, objfile);
        SYMBOL_LANGUAGE (sym) = language_auto;

        if (var->ddvar.kind == DNTT_TYPE_DVAR)
          {
            SYMBOL_NAME (sym) = VT (objfile) + var->ddvar.name;
            SYMBOL_VALUE (sym) = var->ddvar.location;
#if defined(HPREAD_ADJUST_STACK_ADDRESS)
            SYMBOL_VALUE (sym)
              += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
            SYMBOL_TYPE (sym)
              = hpread_type_lookup (dn_bufp->ddvar.type, objfile);

            if (var->ddvar.regvar)
              SYMBOL_CLASS (sym) = LOC_REGISTER;
            else
              SYMBOL_CLASS (sym) = LOC_LOCAL;
          }
        else if (var->dsvar.kind == DNTT_TYPE_SVAR)
          {
            SYMBOL_NAME (sym) = VT (objfile) + var->dsvar.name;
            SYMBOL_VALUE (sym) = var->dsvar.location;
            SYMBOL_TYPE (sym)
              = hpread_type_lookup (dn_bufp->dsvar.type, objfile);
            SYMBOL_CLASS (sym) = LOC_STATIC;
          }
        else
          warning ("unexpected kind of index variable in handling the low "
                   "bound: %d!", var->dsvar.kind);

        TYPE_FIELD_BITPOS (type, 0) = (long)(struct type *)sym;
        break;
      }

    default:
      warning ("unexpected type in handling the low bound: %d!",
               dn_bufp->dsubr.dyn_low);
      break;
    }

  switch (dn_bufp->dsubr.dyn_high)
    {
    case 0:
    case 1:
      TYPE_FIELD_BITPOS (type, 1) = dn_bufp->dsubr.highbound;
      break;

    case 2:
      {
        union dnttentry *var;
        struct symbol *sym;

        sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
                                               sizeof (struct symbol));
        (void) memset (sym, 0, sizeof (struct symbol));
        var = hpread_get_lntt (dn_bufp->dsubr.highbound, objfile);
        SYMBOL_LANGUAGE (sym) = language_auto;

        if (var->ddvar.kind == DNTT_TYPE_DVAR)
          {
            SYMBOL_NAME (sym) = VT (objfile) + var->ddvar.name;
            SYMBOL_VALUE (sym) = var->ddvar.location;
#if defined(HPREAD_ADJUST_STACK_ADDRESS)
            SYMBOL_VALUE (sym)
              += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
            SYMBOL_TYPE (sym)
              = hpread_type_lookup (dn_bufp->ddvar.type, objfile);

            if (var->ddvar.regvar)
              SYMBOL_CLASS (sym) = LOC_REGISTER;
            else
              SYMBOL_CLASS (sym) = LOC_LOCAL;
          }
        else if (var->dsvar.kind == DNTT_TYPE_SVAR)
          {
            SYMBOL_NAME (sym) = VT (objfile) + var->dsvar.name;
            SYMBOL_VALUE (sym) = var->dsvar.location;
            SYMBOL_TYPE (sym)
              = hpread_type_lookup (dn_bufp->dsvar.type, objfile);
            SYMBOL_CLASS (sym) = LOC_STATIC;
          }
        else
          warning ("unexpected kind of index variable in handling the high "
                   "bound: %d!", var->dsvar.kind);

        TYPE_FIELD_BITPOS (type, 1) = (long)(struct type *)sym;
        break;
      }
    default:
      warning ("unexpected value in handling the upper bound: %d!",
               dn_bufp->dsubr.dyn_high);
    }
  
  TYPE_TARGET_TYPE (type) = hpread_type_lookup (dn_bufp->dsubr.subtype,
						objfile);
  return type;
}

/* *INDENT-OFF* */
/* Read in and internalize a dynamic array descriptor debug symbol.

   The dynamic array descriptor debug symbol is used to convey information 
   about a Fortran90 array descriptor that is associated with certain types
   of arrays and pointers.  The array descriptor is a runtime structure that
   is accessed for information about the actual array location and the bounds
   of each dimension of the array.  This runtime structure is documented in 

      /CLO/Components/LIBF90/Doc/array_descriptor_format.html 
      /CLO/Components//FORTRAN90_BRIDGE/Src/dv.h.

   The following fields are accessed in this implementation.

     Byte offset   Field Type        Field Description   

      0            CORE_ADDR         Base address of array or section 
      9 [17]       unsigned char     Exists flag, i.e, has been allocated
     16 [32]       int [long long]   Lower bound of dimension 1
     20 [40]       int [long long]   Extent (no of elements) for dimension 1
     24 [48]       int [long long]   Stride for dimension 1
     .   
     . 
     . 
         
     Bytes 16-27 are the array bounds information for the first dimension.
     There are n of these triplets where n is the rank of the array.

     The []'d items correspond with the offsets and types in the pa64 runtime 
     structure.

   The DNTT_TYPE_DYN_ARRAY_DESC provides information about the element type 
   of the array, the size of the runtime array descriptor, a dnttpointer
   to the first DNTT_TYPE_DESC_SUBRANGE and the rank of the array.  For 
   pointers the dnttpointer to the first DNTT_TYPE_DESC_SUBRANGE is DNTTNIL.

   The DNTT_TYPE_DESC_SUBRANGE entry provides information for the lower
   bound, the type of the lower bound, and the dnttpointer to the next
   DNTT_TYPE_DESC_SUBRANGE.  There is a DNTT_TYPE_DESC_SUBRANGE for each 
   dimension of an array.
   
   A gdb type TYPE_CODE_ARRAY_DESC node is created from the 
   DNTT_TYPE_DYN_ARRAY_DESC entry and one or more TYPE_CODE_ARRAY type nodes
   are created depending on the rank of the array or pointer.  The 
   target_type of the TYPE_CODE_ARRAY_DESC node is the TYPE_CODE_ARRAY node 
   for the last (nth) dimension of the array.  The target_type of the nth
   TYPE_CODE_ARRAY node is the element type of the array.

   The TYPE_CODE_ARRAY type node is built using the information from the 
   DNTT_TYPE_DESC_SUBRANGE node, if provided; otherwise it is built with
   default values. The resulting array [of array...] structure corresponds 
   with the data structure that gdb expects for arrays.
 
   For each TYPE_CODE_ARRAY node a TYPE_INDEX_TYPE node of TYPE_CODE_RANGE
   type is created to hold information about the lower and upper bounds.
   The TYPE_ARRAY_UPPER_BOUND_VALUE of the array node is always set to the
   offset of the bounds information in the runtime array descriptor.  The 
   TYPE_ARRAY_UPPER_BOUND_TYPE of the array node is set to 
   BOUND_BY_VARIABLE_WITH_OFFSET, the bounds type created to indicate 
   that the array bound must be retrieved from the runtime array descriptor.
   The TYPE_ARRAY_LOWER_BOUND_VALUE is set depending on the information in 
   the DNTT_TYPE_DESC_SUBRANGE node; it may be a constant, a variable or 
   the offset to the bounds information in the runtime array descriptor 
   for the given dimension.  The TYPE_ARRAY_LOWER_BOUND_TYPE is set 
   accordingly.

   The TYPE_CODE_ARRAY node has place holders for the address of
   the object that's an array descriptor type (TYPE_ARRAY_ADDR)
   and for the stride (TYPE_ARRAY_STRIDE). TYPE_ARRAY_ADDR is filled in 
   (in eval.c, cp-valprint.c, and printcmd.c) when we know the the address 
   of the object.  TYPE_ARRAY_STRIDE is set when the upper bound is 
   retrieved at runtime.

   Examples of resulting gdb types:

     Array Example (see gdb.fortran/automatic.f): list2 (n,5)
   
       TARGET_TYPE(list2)-
         ----------------|
         |
         V
       code:                         TYPE_CODE_ARRAY_DESC
       length:                       40
       target_type --
         -----------|
         | 
         V
       code:                         TYPE_CODE_ARRAY
       lower_bound_type:             BOUND_SIMPLE
       TYPE_ARRAY_LOWER_BOUND_VALUE: 1
       upper_bound_type:             BOUND_BY_VARIABLE_WITH_OFFSET
       TYPE_ARRAY_UPPER_BOUND_VALUE: 28
       TYPE_ARRAY_ADDR:              (retrieved at runtime)
       TYPE_ARRAY_STRIDE:            (retrieved at runtime)
       target_type --
         -----------|
         | 
         V
       code:                         TYPE_CODE_ARRAY
       lower_bound_type:             BOUND_SIMPLE 
       TYPE_ARRAY_LOWER_BOUND_VALUE: 1
       upper_bound_type:             BOUND_BY_VARIABLE_WITH_OFFSET
       TYPE_ARRAY_UPPER_BOUND_VALUE: 16
       TYPE_ARRAY_ADDR:              (retrieved at runtime)
       TYPE_ARRAY_STRIDE:            (retrieved at runtime)
       target_type:                  integer*4
    
     Pointer Example (see gdb.fortran/ptr90.f): integer, pointer :: p2(:,:)
       target_type(p2) --
         ---------------|
         |
         V
       code:   TYPE_CODE_ARRAY_DESC
       length: 40
       target_type --
         -----------|
         | 
         V
       code:                         TYPE_CODE_ARRAY
       lower_bound_type:             BOUND_BY_VARIABLE_WITH_OFFSET
       TYPE_ARRAY_LOWER_BOUND_VALUE: 28
       upper_bound_type:             BOUND_BY_VARIABLE_WITH_OFFSET
       TYPE_ARRAY_UPPER_BOUND_VALUE: 28
       TYPE_ARRAY_ADDR:              (retrieved at runtime)
       TYPE_ARRAY_STRIDE:            (retrieved at runtime)
       target_type --
         -----------|
         | 
         V
       code:                         TYPE_CODE_ARRAY
       lower_bound_type:             BOUND_BY_VARIABLE_WITH_OFFSET
       TYPE_ARRAY_LOWER_BOUND_VALUE: 16
       upper_bound_type:             BOUND_BY_VARIABLE_WITH_OFFSET
       TYPE_ARRAY_UPPER_BOUND_VALUE: 16
       TYPE_ARRAY_ADDR:              (retrieved at runtime)
       TYPE_ARRAY_STRIDE:            (retrieved at runtime)
       target_type:                  integer*4

   The TYPE_CODE_ARRAY_DESC type is used in f-valprint.c and f-typeprint.c.

   The macros used to access and manipulate the runtime array descriptor is
   declared in gdb/config/pa/tm-hppa.h:

     F_CHECK_ARRAY_DESC(address) 
     F_GET_ARRAY_DESC_ARRAY_LBOUND(type, lower_bound)
     F_GET_ARRAY_DESC_ARRAY_UBOUND(type, upper_bound)
     F_GET_ARRAY_DESC_ARRAY_ADDR(addr)
     F_ADJ_ARRAY_ADDR_W_NEG_STRIDE(addr, type)
     F_ADJ_ARRAY_VALADDR_W_NEG_STRIDE(valaddr, type)

   They are defined in gdb/hppa-tdep.c. */
/* *INDENT-ON* */

static struct type *
hpread_read_dyn_array_desc_type (hp_type, dn_bufp, objfile)
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{
  struct type *type, *atype, *prev_type;
  dnttpointer index_type;
  union dnttentry *indexp;
  int dim;

  /* Although it is unlikely that two variables would have the same
   * array descriptor dntt entry, avoid any potential problems by
   * not calling hpread_alloc_type.
   */

  type = alloc_type (objfile);

  TYPE_CODE (type) = TYPE_CODE_ARRAY_DESC;
  /* Save the array descriptor size in the length field */
  TYPE_LENGTH (type) = dn_bufp->darray_desc.bitlength / 8;

  index_type = dn_bufp->darray_desc.indextype;
  prev_type = 0;
  indexp = 0;
  for (dim=1; dim <= dn_bufp->darray_desc.rank; dim++)
    {
      /* allocate an array type symbol */ 
      atype = alloc_type (objfile);
      TYPE_CODE (atype) = TYPE_CODE_ARRAY;
      TYPE_LENGTH (atype) = 0;
  
      /* Create fortran_struct_type to hold extra information */
      ALLOCATE_FORTRAN_STRUCT_TYPE (atype);

      /* create field node for the dimension, i.e, indextype */
      TYPE_NFIELDS (atype) = 1;
      TYPE_FIELDS (atype) = (struct field *)
        obstack_alloc (&objfile->type_obstack, sizeof (struct field));
      memset (TYPE_FIELDS (atype), 0, sizeof (struct field));

      /* Create subrange type for upper and lower bounds */
      if (index_type.word != DNTTNIL)
        indexp = hpread_get_lntt (INDEX (objfile, index_type.dnttp), objfile);
      TYPE_FIELD_TYPE (atype, 0) 
        = hpread_read_desc_subrange_type (dim, index_type, indexp, objfile);

      /* set element type or link this array to the previous one */
      if (dim == 1)
        TYPE_TARGET_TYPE (atype) 
          = hpread_type_lookup (dn_bufp->darray_desc.elemtype, objfile);
      else
        TYPE_TARGET_TYPE (atype) = prev_type;
      prev_type = atype;

      /* set lower_bound_type and upper_bound_type of the array
         and get next desc_subrange 
       */
      if (index_type.word == DNTTNIL)
        {
          TYPE_ARRAY_LOWER_BOUND_TYPE (atype) = BOUND_BY_VARIABLE_WITH_OFFSET;
          TYPE_ARRAY_UPPER_BOUND_TYPE (atype) = BOUND_BY_VARIABLE_WITH_OFFSET;
        }
      else
        {
          switch (indexp->ddesc_subr.dyn_low)
          {
          case 0:
            /* TYPE_ARRAY_LOWER_BOUND_TYPE (atype) = BOUND_SIMPLE; */
            break;
          case 2:
            TYPE_ARRAY_LOWER_BOUND_TYPE (atype) = BOUND_BY_VARIABLE;
            break;
          case 3:
            TYPE_ARRAY_LOWER_BOUND_TYPE (atype) = BOUND_BY_VARIABLE_WITH_OFFSET;
            break;
          default:
            /* error handled in hpread_read_desc_subrange_type */
            break;
          }
          /* upper bound is always calculated at runtime */
          TYPE_ARRAY_UPPER_BOUND_TYPE (atype) = BOUND_BY_VARIABLE_WITH_OFFSET;
          /* get next desc_subrange */ 
          index_type = indexp->ddesc_subr.indextype;
        }
    }
  TYPE_TARGET_TYPE (type) = atype;
  return type;
}
 
/* Read in and internalize a descriptor subrange debug symbol.
 * This routine is only expected to be called from 
 * hpread_read_dyn_array_desc_type because it needs to know the
 * dimension of the array and because it knows how to handle an 
 * hp_type that is DNTTNIL.  It should never be called from 
 * hpread_type_lookup.
 */

static struct type *
hpread_read_desc_subrange_type (dim, hp_type, dn_bufp, objfile)
     int dim;
     dnttpointer hp_type;
     union dnttentry *dn_bufp;
     struct objfile *objfile;
{

#ifdef GDB_TARGET_IS_HPPA_20W
  #define ARRAY_DESC_FIXED_SIZE   32  /* size in bytes */
  #define ARRAY_DESC_BOUNDS_SIZE  24  /* size in bytes */
#else
  #define ARRAY_DESC_FIXED_SIZE   16  /* size in bytes */
  #define ARRAY_DESC_BOUNDS_SIZE  12  /* size in bytes */
#endif
  struct type *type;

  if (hp_type.word == DNTTNIL)
      type = alloc_type (objfile);
  else
    {
      type = hpread_alloc_type (hp_type, objfile);
      if (TYPE_CODE (type) == TYPE_CODE_RANGE)
        return type;
    }
  TYPE_CODE (type) = TYPE_CODE_RANGE;
  TYPE_FLAGS (type) &= TYPE_FLAG_STUB;
  TYPE_NFIELDS (type) = 2;
  TYPE_FIELDS (type)
    = (struct field *) obstack_alloc (&objfile->type_obstack,
                                      2 * sizeof (struct field));
  memset (TYPE_FIELDS (type), 0, 2 * sizeof (struct field));

  if (hp_type.word == DNTTNIL)
    {
      /* Set offset to bounds information */
      TYPE_FIELD_BITPOS (type, 0) 
        = ARRAY_DESC_FIXED_SIZE + (dim -1) *  ARRAY_DESC_BOUNDS_SIZE;
      TYPE_FIELD_BITPOS (type, 1) 
        = ARRAY_DESC_FIXED_SIZE + (dim -1) *  ARRAY_DESC_BOUNDS_SIZE;
    }
  else
    {
      switch (dn_bufp->ddesc_subr.dyn_low)
      {
      case 0:
        TYPE_FIELD_BITPOS (type, 0) = dn_bufp->ddesc_subr.lowbound;
        break;

      case 2:
        {
          union dnttentry *var;
          struct symbol *sym;

          sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
                                                 sizeof (struct symbol));
          (void) memset (sym, 0, sizeof (struct symbol));
          var = hpread_get_lntt (dn_bufp->ddesc_subr.lowbound, objfile);
          SYMBOL_LANGUAGE (sym) = language_fortran;

          if (var->ddvar.kind == DNTT_TYPE_DVAR)
            {
              SYMBOL_NAME (sym) = VT (objfile) + var->ddvar.name;
              SYMBOL_VALUE (sym) = var->ddvar.location;
#if defined(HPREAD_ADJUST_STACK_ADDRESS)
              SYMBOL_VALUE (sym)
                += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
              SYMBOL_TYPE (sym)
                = hpread_type_lookup (var->ddvar.type, objfile);
              if (var->ddvar.regvar)
                SYMBOL_CLASS (sym) = LOC_REGISTER;
              else
                SYMBOL_CLASS (sym) = LOC_LOCAL;
            }
          else if (var->dsvar.kind == DNTT_TYPE_SVAR)
            {
              SYMBOL_NAME (sym) = VT (objfile) + var->dsvar.name;
              SYMBOL_VALUE (sym) = var->dsvar.location;
              SYMBOL_TYPE (sym)
                = hpread_type_lookup (var->dsvar.type, objfile);
              SYMBOL_CLASS (sym) = LOC_STATIC;
            }
          else
            warning ("hpread_read_desc_subrange_type: unexpected variable kind (%d) in handling low bound at symbol index 0x%x",
                     var->dsvar.kind, INDEX(objfile, hp_type.dnttp));

          TYPE_FIELD_BITPOS (type, 0) = (long)(struct type *)sym;
          break;
        }

      case 3:
        TYPE_FIELD_BITPOS (type, 0) 
          = ARRAY_DESC_FIXED_SIZE + (dim -1) * ARRAY_DESC_BOUNDS_SIZE;
        break;

      default:
        warning ("hpread_read_desc_subrange_type: unexpected value (%d) for lower bound at symbol index 0x%x",
                 dn_bufp->ddesc_subr.lowbound, INDEX(objfile, hp_type.dnttp));
        TYPE_FIELD_BITPOS (type, 0) = 1;
      }
      /* upper bound is always determined at runtime */
      TYPE_FIELD_BITPOS (type, 1) 
        = ARRAY_DESC_FIXED_SIZE + (dim -1) * ARRAY_DESC_BOUNDS_SIZE;
      TYPE_TARGET_TYPE (type)
         = hpread_type_lookup (dn_bufp->ddesc_subr.subtype, objfile);
    }
  return type;
}

/* struct type * hpread_type_lookup(hp_type, objfile)
 *   Arguments:
 *     hp_type: A pointer into the DNTT specifying what type we
 *              are about to "look up"., or else [for fundamental types
 *              like int, float, ...] an "immediate" structure describing
 *              the type.
 *     objfile: ?
 *   Return value: A pointer to a "struct type" (representation of a
 *                 type in GDB's internal symbol table - see gdbtypes.h)
 *   Routine description:
 *     There are a variety of places when scanning the DNTT when we
 *     need to interpret a "type" field. The simplest and most basic 
 *     example is when we're processing the symbol table record
 *     for a data symbol (a SVAR or DVAR record). That has
 *     a "type" field specifying the type of the data symbol. That
 *     "type" field is either an "immediate" type specification (for the
 *     fundamental types) or a DNTT pointer (for more complicated types). 
 *     For the more complicated types, we may or may not have already
 *     processed the pointed-to type. (Multiple data symbols can of course
 *     share the same type).
 *     The job of hpread_type_lookup() is to process this "type" field.
 *     Most of the real work is done in subroutines. Here we interpret
 *     the immediate flag. If not immediate, chase the DNTT pointer to
 *     find our way to the SOM record describing the type, switch on
 *     the SOM kind, and then call an appropriate subroutine depending
 *     on what kind of type we are constructing. (e.g., an array type,
 *     a struct/class type, etc).
 */
static struct type *
hpread_type_lookup (hp_type, objfile)
     dnttpointer hp_type;
     struct objfile *objfile;
{
  union dnttentry *dn_bufp;
  struct type *tmp_type;

  /* First see if it's a simple builtin type.  */
  if (hp_type.dntti.immediate)
    /* If this is a template argument, the argument number is
     * encoded in the bitlength. All other cases, just return
     * GDB's representation of this fundamental type.
     */
    if (hp_type.dntti.type == HP_TYPE_TEMPLATE_ARG)
      return hpread_get_nth_template_arg (objfile, hp_type.dntti.bitlength);
    else if (hp_type.dntti.type == HP_TYPE_FTN_STRING_SPEC)
      return hpread_ftn_string_type (hp_type);
    else
      return lookup_fundamental_type (objfile, hpread_type_translate (hp_type));

  /* Not a builtin type.  We'll have to read it in.  */
  if (INDEX(objfile, hp_type.dnttp) < LNTT_SYMCOUNT (objfile))
    dn_bufp = hpread_get_lntt (INDEX(objfile, hp_type.dnttp), objfile);
  else
    /* This is a fancy way of returning NULL */
    return lookup_fundamental_type (objfile, FT_VOID);

  switch (dn_bufp->dblock.kind)
    {
    case DNTT_TYPE_SRCFILE:
    case DNTT_TYPE_MODULE:
    case DNTT_TYPE_ENTRY:
    case DNTT_TYPE_BEGIN:
    case DNTT_TYPE_END:
    case DNTT_TYPE_IMPORT:
    case DNTT_TYPE_LABEL:
    case DNTT_TYPE_FPARAM:
    case DNTT_TYPE_SVAR:
    case DNTT_TYPE_DVAR:
    case DNTT_TYPE_CONST:
    case DNTT_TYPE_MEMENUM:
    case DNTT_TYPE_VARIANT:
    case DNTT_TYPE_FILE:
    case DNTT_TYPE_WITH:
    case DNTT_TYPE_COMMON:
    case DNTT_TYPE_COBSTRUCT:
    case DNTT_TYPE_XREF:
    case DNTT_TYPE_SA:
    case DNTT_TYPE_MACRO:
    case DNTT_TYPE_BLOCKDATA:
    case DNTT_TYPE_CLASS_SCOPE:
    case DNTT_TYPE_MEMACCESS:
    case DNTT_TYPE_INHERITANCE:
    case DNTT_TYPE_OBJECT_ID:
    case DNTT_TYPE_FRIEND_CLASS:
    case DNTT_TYPE_FRIEND_FUNC:
      /* These are not types - something went wrong.  */
      /* This is a fancy way of returning NULL */
      return lookup_fundamental_type (objfile, FT_VOID);

    case DNTT_TYPE_FUNCTION:
      /* We wind up here when dealing with class member functions 
       * (called from hpread_read_struct_type(), i.e. when processing
       * the class definition itself).
       */
      return hpread_read_function_type (hp_type, dn_bufp, objfile, 0);

    case DNTT_TYPE_DOC_FUNCTION:
      return hpread_read_doc_function_type (hp_type, dn_bufp, objfile, 0);

    case DNTT_TYPE_TYPEDEF:
      {
	/* A typedef - chase it down by making a recursive call */
	struct type *structtype = hpread_type_lookup (dn_bufp->dtype.type,
						      objfile);

	/* The following came from the base hpread.c that we inherited.
	 * It is WRONG so I have commented it out. - RT
	 *...

	 char *suffix;
	 suffix = VT (objfile) + dn_bufp->dtype.name;
	 TYPE_NAME (structtype) = suffix;

	 * ... further explanation ....
	 *
	 * What we have here is a typedef pointing to a typedef.
	 * E.g.,
	 * typedef int foo;
	 * typedef foo fum;
	 *
	 * What we desire to build is (these are pictures
	 * of "struct type"'s): 
	 *
	 *  +---------+     +----------+     +------------+
	 *  | typedef |     | typedef  |     | fund. type |
	 *  |     type| ->  |      type| ->  |            |
	 *  | "fum"   |     | "foo"    |     | "int"      |
	 *  +---------+     +----------+     +------------+
	 *
	 * What this commented-out code is doing is smashing the
	 * name of pointed-to-type to be the same as the pointed-from
	 * type. So we wind up with something like:
	 *
	 *  +---------+     +----------+     +------------+
	 *  | typedef |     | typedef  |     | fund. type |
	 *  |     type| ->  |      type| ->  |            |
	 *  | "fum"   |     | "fum"    |     | "fum"      |
	 *  +---------+     +----------+     +------------+
	 * 
	 */

	return structtype;
      }

    case DNTT_TYPE_TAGDEF:
      {
	/* Just a little different from above.  We have to tack on
	 * an identifier of some kind (struct, union, enum, class, etc).  
	 */
	struct type *structtype = hpread_type_lookup (dn_bufp->dtype.type,
						      objfile);
	char *prefix, *suffix;
	suffix = VT (objfile) + dn_bufp->dtype.name;

	/* Lookup the next type in the list.  It should be a structure,
	 * union, class, enum, or template type.  
	 * We will need to attach that to our name.  
	 */
        if (INDEX(objfile,dn_bufp->dtype.type.dnttp) <
              LNTT_SYMCOUNT (objfile))
          dn_bufp =
            hpread_get_lntt (INDEX(objfile,dn_bufp->dtype.type.dnttp),
                             objfile);
	else
	  {
	    complain (&hpread_type_lookup_complaint);
	    return;
	  }

	if (dn_bufp->dblock.kind == DNTT_TYPE_STRUCT)
	  {
            if (current_language->la_language  == language_fortran)
              prefix = "type ";
            else 
	      prefix = "struct ";
	  }
	else if (dn_bufp->dblock.kind == DNTT_TYPE_UNION)
	  {
	    prefix = "union ";
	  }
	else if (dn_bufp->dblock.kind == DNTT_TYPE_CLASS)
	  {
	    /* Further field for CLASS saying how it was really declared */
	    /* 0==class, 1==union, 2==struct */
	    if (dn_bufp->dclass.class_decl == 0)
	      prefix = "class ";
	    else if (dn_bufp->dclass.class_decl == 1)
	      prefix = "union ";
	    else if (dn_bufp->dclass.class_decl == 2)
	      prefix = "struct ";
	    else
	      prefix = "";
	  }
	else if (dn_bufp->dblock.kind == DNTT_TYPE_ENUM)
	  {
	    prefix = "enum ";
	  }
	else if (dn_bufp->dblock.kind == DNTT_TYPE_TEMPLATE)
	  {
	    prefix = "template ";
	  }
	else
	  {
	    prefix = "";
	  }

	/* Build the correct name.  */

        /* The HP aCC compiler emits a spurious "{unnamed struct}"/
         * "{unnamed union}"/"{unnamed enum}" tag  for unnamed 
         * struct/union/enum's, which we don't want to print, so catch
         * it here.  Use strstr because it may be a nested type and
         * so the name may be Class::{unnamed...
         */

        /* F77 emits a TAGDEF with a NULL name for unnamed nested structs,
         * so catch it here, too.
         */
        
        if (strstr (suffix, "{unnamed") || suffix[0] == '\0')
          {
            TYPE_NAME (structtype) = NULL;
            TYPE_TAG_NAME (structtype) = NULL;
          }
        else
          {
	    structtype->name
	      = (char *) obstack_alloc (&objfile->type_obstack,
					strlen (prefix) + strlen (suffix) + 1);
	    TYPE_NAME (structtype) = strcpy (TYPE_NAME (structtype), prefix);
	    TYPE_NAME (structtype) = strcat (TYPE_NAME (structtype), suffix);
	    TYPE_TAG_NAME (structtype) = suffix;
	  }

	/* For classes/structs, we have to set the static member "physnames"
	   to point to strings like "Class::Member" */
	if (TYPE_CODE (structtype) == TYPE_CODE_STRUCT)
	  fix_static_member_physnames (structtype, suffix, objfile);

	return structtype;
      }

    case DNTT_TYPE_POINTER:
      /* Pointer type - call a routine in gdbtypes.c that constructs
       * the appropriate GDB type.
       */
      return make_pointer_type (
				 hpread_type_lookup (dn_bufp->dptr.pointsto,
						     objfile),
				 NULL);

    case DNTT_TYPE_REFERENCE:
      /* C++ reference type - call a routine in gdbtypes.c that constructs
       * the appropriate GDB type.
       */
      return make_reference_type (
			   hpread_type_lookup (dn_bufp->dreference.pointsto,
					       objfile),
				   NULL);

    case DNTT_TYPE_ENUM:
      return hpread_read_enum_type (hp_type, dn_bufp, objfile);
    case DNTT_TYPE_SET:
      return hpread_read_set_type (hp_type, dn_bufp, objfile);
    case DNTT_TYPE_SUBRANGE:
      return hpread_read_subrange_type (hp_type, dn_bufp, objfile);
    case DNTT_TYPE_ARRAY:
      return hpread_read_array_type (hp_type, dn_bufp, objfile);
    case DNTT_TYPE_STRUCT:
    case DNTT_TYPE_UNION:
      return hpread_read_struct_type (hp_type, dn_bufp, objfile);
    case DNTT_TYPE_FIELD:
      return hpread_type_lookup (dn_bufp->dfield.type, objfile);

    case DNTT_TYPE_FUNCTYPE:
      /* Here we want to read the function SOMs and return a 
       * type for it. We get here, for instance, when processing
       * pointer-to-function type.
       */
      return hpread_read_function_type (hp_type, dn_bufp, objfile, 0);

    case DNTT_TYPE_PTRMEM:
      /* Declares a C++ pointer-to-data-member type. 
       * The "pointsto" field defines the class,
       * while the "memtype" field defines the pointed-to-type.
       */
      {
	struct type *ptrmemtype;
	struct type *class_type;
	struct type *memtype;
	memtype = hpread_type_lookup (dn_bufp->dptrmem.memtype,
				      objfile),
	  class_type = hpread_type_lookup (dn_bufp->dptrmem.pointsto,
					   objfile),
	  ptrmemtype = alloc_type (objfile);
	smash_to_member_type (ptrmemtype, class_type, memtype);
	return make_pointer_type (ptrmemtype, NULL);
      }
      break;

    case DNTT_TYPE_PTRMEMFUNC:
      /* Defines a C++ pointer-to-function-member type. 
       * The "pointsto" field defines the class,
       * while the "memtype" field defines the pointed-to-type.
       */
      {
	struct type *ptrmemtype;
	struct type *class_type;
	struct type *functype;
	struct type *retvaltype;
	struct type * ntype;
	int nargs;
	int i;
	struct type **args_type;
	class_type = hpread_type_lookup (dn_bufp->dptrmem.pointsto,
					 objfile);
	functype = hpread_type_lookup (dn_bufp->dptrmem.memtype,
				       objfile);
	retvaltype = TYPE_TARGET_TYPE (functype);
	nargs = TYPE_NFIELDS (functype);
	if (nargs > 0)
	  {
	    args_type = (struct type **) xmalloc ((nargs + 1) * sizeof (struct type *));
	    for (i = 0; i < nargs; i++)
	      {
		args_type[i] = TYPE_FIELD_TYPE (functype, i);
	      }
	    args_type[nargs] = NULL;
	  }
	else
	  {
	    args_type = NULL;
	  }
	ptrmemtype = alloc_type (objfile);
	smash_to_method_type (ptrmemtype, class_type, retvaltype, args_type);
	ntype = make_pointer_type(ptrmemtype, NULL);
	/* RM: Kludge: member function pointers are actually 3 words
	   long in HP aCC */
	TYPE_LENGTH(ntype) = 3*sizeof(CORE_ADDR);
	return ntype;
      }
      break;

    case DNTT_TYPE_CLASS:
      return hpread_read_struct_type (hp_type, dn_bufp, objfile);

    case DNTT_TYPE_GENFIELD:
      /* Chase pointer from GENFIELD to FIELD, and make recursive
       * call on that.
       */
      return hpread_type_lookup (dn_bufp->dgenfield.field, objfile);

    case DNTT_TYPE_VFUNC:
      /* C++ virtual function.
       * We get here in the course of processing a class type which
       * contains virtual functions. Just go through another level
       * of indirection to get to the pointed-to function SOM.
       */
      return hpread_type_lookup (dn_bufp->dvfunc.funcptr, objfile);

    case DNTT_TYPE_MODIFIER:
      /* Check the modifiers and then just make a recursive call on
       * the "type" pointed to by the modifier DNTT.
       * 
       * pai:: FIXME -- do we ever want to handle "m_duplicate" and
       * "m_void" modifiers?  Is static_flag really needed here?
       * (m_static used for methods of classes, elsewhere).
       */
      tmp_type = make_cv_type (dn_bufp->dmodifier.m_const,
			       dn_bufp->dmodifier.m_volatile,
		      hpread_type_lookup (dn_bufp->dmodifier.type, objfile),
			       0);
      return tmp_type;


    case DNTT_TYPE_MEMFUNC:
      /* Member function. Treat like a function.
       * I think we get here in the course of processing a 
       * pointer-to-member-function type...
       */
      return hpread_read_function_type (hp_type, dn_bufp, objfile, 0);

    case DNTT_TYPE_DOC_MEMFUNC:
      return hpread_read_doc_function_type (hp_type, dn_bufp, objfile, 0);

    case DNTT_TYPE_TEMPLATE:
      /* Template - sort of the header for a template definition,
       * which like a class, points to a member list and also points
       * to a TEMPLATE_ARG list of type-arguments.
       */
      return hpread_read_struct_type (hp_type, dn_bufp, objfile);

    case DNTT_TYPE_TEMPLATE_ARG:
      {
	char *name;
	/* The TEMPLATE record points to an argument list of
	 * TEMPLATE_ARG records, each of which describes one
	 * of the type-arguments. 
	 */
	name = VT (objfile) + dn_bufp->dtempl_arg.name;
	return hpread_read_templ_arg_type (hp_type, dn_bufp, objfile, name);
      }

    case DNTT_TYPE_FUNC_TEMPLATE:
      /* We wind up here when processing a TEMPLATE type, 
       * if the template has member function(s).
       * Treat it like a FUNCTION.
       */
      return hpread_read_function_type (hp_type, dn_bufp, objfile, 0);

    case DNTT_TYPE_LINK:
      /* The LINK record is used to link up templates with instantiations.
       * There is no type associated with the LINK record per se.
       */
      return lookup_fundamental_type (objfile, FT_VOID);

    case DNTT_TYPE_DYN_ARRAY_DESC:
      return hpread_read_dyn_array_desc_type (hp_type, dn_bufp, objfile);

    case DNTT_TYPE_DESC_SUBRANGE:
      /* Should only be called from hpread_read_dyn_array_desc_type */
      return lookup_fundamental_type (objfile, FT_VOID);

    case DNTT_TYPE_HOLE1:
      /* JYG: DNTT_TYPE_HOLE1 signals that the type has been
         overridden in the incremental linking mode (i.e. has gone away).
         However we cannot simply return NULL.  A consistent handling
         of a new GDB type FT_NULL would be helpful but this would
         cause massive changes across the board to handle this new
         type correctly (i.e. ignore whatever debug symbol associated
         with such a type directly or indirectly through pointers /
         references.

         For now, use FT_VOID, and let these overridden global symbols stay
         around :(:
	 (gdb) ptype old_global
	 type = <data variable, no debug info>
      */
      return lookup_fundamental_type (objfile, FT_VOID);
      
      /* Also not yet handled... */
      /* case DNTT_TYPE_BEGIN_EXT: */
      /* case DNTT_TYPE_INLN: */
      /* case DNTT_TYPE_INLN_LIST: */
      /* case DNTT_TYPE_ALIAS: */
    default:
      /* A fancy way of returning NULL */
      warning ("hpread_type_lookup: unsupported type (%d)",
               dn_bufp->dblock.kind);
      return lookup_fundamental_type (objfile, FT_VOID);
    }
}

/* CM: Record line information from the SLT for an entire function at a time.
 * This routine takes an index into the SLT. That index should point to an
 * SLT_FUNCTION entry.  The SLT is then scanned until the next SLT_FUNCTION
 * entry is found.
 */

static void
hpread_record_function_lines (s_idx, objfile, offset)
     sltpointer s_idx;
     struct objfile *objfile;
     CORE_ADDR offset;
{
  union sltentry *sl_bufp;
  char *saved_subfile_name = current_subfile->name;
  int subfile_changed;
  size_t numSLTEntries;
  char *next_sf_name;

  /* Check that s_idx is valid */
  sl_bufp = hpread_get_slt (s_idx, objfile);
  if (sl_bufp->snorm.sltdesc == SLT_FUNCTION)
    {
      /* The s_idx should point to an SLT_FUNCTION. Just skip it. */
      s_idx++;
    }
  else
    {
      /* For some reason, the s_idx did not point to an SLT_FUNCTION.
         This is probably an error, but just continue with a warning. */
      warning ("(internal) "
               "Mismatch in reading line number information (SLT). "
               "Continuing.");
    }

  /* Get the number of SLT entries. */
  numSLTEntries = hpread_get_slt_size(objfile) / (sizeof (union sltentry));

  /* Walk the SLT until the next SLT_FUNCTION, SLT_MODULE, or reached the SLT
     end. */
  for (next_sf_name = NULL, subfile_changed = 0;
       (s_idx < numSLTEntries) &&
         (sl_bufp = hpread_get_slt (s_idx, objfile),
          ((sl_bufp->snorm.sltdesc != SLT_FUNCTION) &&
           (sl_bufp->snorm.sltdesc != SLT_MODULE)));
       s_idx++)
    {
      /* Only record "normal" and "exit" entries in the line table. */
      switch (sl_bufp->snorm.sltdesc) {
      case SLT_SRCFILE:
        {
          /* Setup a new subfile */
          union dnttentry *lntt_entry =
            hpread_get_lntt(sl_bufp->sspec.backptr.word, objfile);

          /* Get name for subfile */
          SET_NAMESTRING (lntt_entry, &next_sf_name, objfile);

          /* Don't start a new subfile, start it lazily. This avoids creating
             extra subfiles. Also, looks like if this is not done lazily, the
             extra subfiles create extra symtabs, which may confuse
             symtab.c:lookup_symtab(). This is due to the debug information
             having a SLT_SRCFILE right before any SLT_MODULE entries. */
        }
        break;

      case SLT_NORMAL_OFFSET:
      case SLT_NORMAL:
      case SLT_EXIT:
        /* Start a subfile lazily */
        if (next_sf_name) {
          start_subfile (next_sf_name, current_compilation_directory);
          record_debugformat ("HP");
          subfile_changed = 1;
          next_sf_name = NULL;
        }
        /* Record SLT line info */
        if (sl_bufp->snorm.sltdesc == SLT_NORMAL_OFFSET)
          {
            record_line (current_subfile, sl_bufp->snormoff.line,
                         sl_bufp->snormoff.address + offset);
          }
        else /* SLT_NORMAL || SLT_EXIT */
          {
            record_line (current_subfile, sl_bufp->snorm.line,
                         sl_bufp->snorm.address + offset);
          }
        break;
      } /* switch */
    } /* for */

  /* Restore old current_subfile if it was changed */
  if (subfile_changed)
    {
      start_subfile (saved_subfile_name, current_compilation_directory);
    }
}

/* CM: Copy "size" bytes from "lines_idx" in "objfile" into an integer variable
   (signed or unsigned) "number" based upon if it "is_signed" or not. Add
   "size" to "lines_idx".

   This macro copies an unaligned integer (assumed big endian format) from a
   character stream of length "size" into a variable "number".  The character
   stream is accessed with the "hpread_get_lines" function.  The "number"
   variable can be a signed or unsigned lvalue (i.e. the first argument can be
   considered a polymorphic integer type).  The interger copied will be sign
   extended if "is_signed" is set to a non-zero value.  Overflows are not
   checked if "number" cannot hold the value.  As a side effect, the index
   into the character stream "lines_idx" is updated by the number of
   characters used, "size".
   */

#define COPY_AND_ADVANCE_LINES(number, lines_idx, objfile, is_signed, size) \
  do \
    { \
      dst_ln_entry_t *lines_bufp; \
      int i; \
      (number) = 0; \
      for (i = 0; i < (size); i++) \
        { \
          lines_bufp = hpread_get_lines ((lines_idx), (objfile)); \
          if (is_signed) \
            { \
              if (i == 0) \
                { \
                  (number) = lines_bufp->sdata; \
                } \
              else \
                { \
                  (number) = ((number) << 8) + lines_bufp->udata; \
                } \
            } \
          else \
            { \
              (number) = ((number) << 8) + lines_bufp->udata; \
            } \
          (lines_idx)++; \
        } \
    } while(0)

/* Multiplier for address delta's in DOC line information */
#define PC_STEP  4

/* CM: Record line information for a DOC function. This routine reads in
 * both the SLT and the LINES information.
 */

/* CM: Uncomment the following line to print out the LINES information as it
   is read. */
/* #define DEBUG_LINES_INFO */

static void
hpread_record_doc_function_lines (slt_idx, lines_idx, end_caddr, objfile,
                                  offset)
     sltpointer slt_idx;
     ltpointer lines_idx;
     CORE_ADDR end_caddr;
     struct objfile *objfile;
     CORE_ADDR offset;
{
  /* Read normal line (SLT) information */
  hpread_record_function_lines(slt_idx, objfile, offset);

  /* Parse LINES information */
  if (LINES (objfile))
    {
      CORE_ADDR cur_addr;
      struct subfile *cur_file;
      unsigned int cur_line;
      unsigned int cur_column;
      unsigned int cur_column_length;
      int line_completed;
      int is_critical;
      int has_column;
      int end_marker_found;
      int error_found;

      for (error_found = 0, cur_file = current_subfile, line_completed = 0,
             is_critical = 0, cur_line = 0, cur_column = 0,
             cur_column_length = 0, has_column = 0, end_marker_found = 0,
             cur_addr = 0;
           (line_completed ? (cur_addr < end_caddr) : 1) &&
             !(end_marker_found);
           )
        {
          /* Data holders for reading escape codes */
          ULONGEST udata1;
          ULONGEST udata2;
          LONGEST sdata1;
          CORE_ADDR pcdata1;

          /* Default data when no escape codes are present. */
          CORE_ADDR def_pcdata;
          dst_ln_entry_t *def_lines_bufp;

          if (line_completed)
            {
              is_critical = 0;
              has_column = 0;
              line_completed = 0;
            }

          /* Process a number of entries that create one abstract line entry. 
           * A line table entry is encoded as 1 byte, that may be interpreted 
           * in various ways.  Pick out the possible encodings.  */
          def_lines_bufp = hpread_get_lines (lines_idx, objfile);
          def_pcdata = def_lines_bufp->delta.pc_delta;

          /* check for first set of escapes */
          if (def_pcdata == DST_LN_ESCAPE_FLAG1)
            {
              unsigned int escape;

#ifdef DEBUG_LINES_INFO
              printf("%20.20s ", "DST_LN_ESCAPE_FLAG1");
#endif

              escape = def_lines_bufp->esc.esc_code;
              lines_idx++;

              switch ((dst_ln_escape1_t) escape)
                {
                case dst_ln_stmt_cp:
                  is_critical = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_stmt_cp");
#endif
                  break;

                case dst_ln_dpc1_dln1:
                  /* PC and line # delta pair - 1 byte each */
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 1);
                  COPY_AND_ADVANCE_LINES(sdata1, lines_idx, objfile, 1, 1);
                  udata1 *= PC_STEP;
                  cur_line += sdata1;
                  cur_addr += udata1;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s +line=%lld +addr=0x%llx\n",
                         "dst_ln_dpc1_dln1",
                         sdata1, udata1);
#endif
                  break;

                case dst_ln_dpc2_dln2:
                  /* PC and line # delta pair - 2 bytes each */
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 2);
                  COPY_AND_ADVANCE_LINES(sdata1, lines_idx, objfile, 1, 2);
                  udata1 *= PC_STEP;
                  cur_line += sdata1;
                  cur_addr += udata1;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s +line=%lld +addr=0x%llx\n",
                         "dst_ln_dpc2_dln2",
                         sdata1, udata1);
#endif
                  break;

                case dst_ln_pc4_ln4:
                  /* PC and line # (absolute) */
                  COPY_AND_ADVANCE_LINES(pcdata1, lines_idx, objfile, 0, 4);
                  COPY_AND_ADVANCE_LINES(sdata1, lines_idx, objfile, 1, 4);
                  cur_line = sdata1;
                  cur_addr = pcdata1 + offset;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s line=%lld addr=0x%llx+0x%llx=0x%llx\n",
                         "dst_ln_pc4_ln4", sdata1, (LONGEST) pcdata1,
                         (LONGEST) offset, (LONGEST) cur_addr);
#endif
                  break;

                case dst_ln_dpc0_dln1:
                  COPY_AND_ADVANCE_LINES(sdata1, lines_idx, objfile, 1, 1);
                  cur_line += sdata1;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s +line=%lld\n", "dst_ln_dpc0_dln1", sdata1);
#endif
                  break;

                case dst_ln_pad:
                case dst_ln_exit:
                case dst_ln_nxt_byte:
                case dst_ln_ln_off_1:
                  /* Ignore */
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_pad, dst_ln_exit, "
                         "dst_ln_nxt_byte, dst_ln_ln_off_1");
#endif
                  break;

                case dst_ln_entry:
                case dst_ln_ln_off:
                  /* Ignore */
                  lines_idx++;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_entry, dst_ln_ln_off");
#endif
                  break;

                case dst_ln_stmt_end:
                  /* Ignore */
                  /* no end offsets for now */  
                  lines_idx += 4;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_stmt_end");
#endif
                  break;

                case dst_ln_end:
                  /* This denotes the end of the line table we are reading. */
                  end_marker_found = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_end");
#endif
                  break;

                default:
                  /* unrecognized code */
                  if (!error_found) {
                    error_found = 1;
                    warning("Unknown escape (FLAG1) code in DOC lines table");
                  }
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s escape=%u\n", "<Unknown>", escape);
#endif
                  break;
                } /* switch */
            } /* if (def_pcdata == DST_LN_ESCAPE_FLAG1) */
          else if (def_pcdata == DST_LN_ESCAPE_FLAG2)
            {
              /* ... and the second set */
              unsigned int escape;

#ifdef DEBUG_LINES_INFO
              printf("%20.20s ", "DST_LN_ESCAPE_FLAG2");
#endif

              escape = def_lines_bufp->esc.esc_code;
              lines_idx++;

              switch ((dst_ln_escape2_t) escape)
                {
                case dst_ln_init_base1:
                  COPY_AND_ADVANCE_LINES(pcdata1, lines_idx, objfile, 0, 4);
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 1);
                  cur_line = udata1;
                  cur_addr = pcdata1 + offset;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s line=%lld addr=0x%llx+0x%llx=0x%llx\n",
                         "dst_ln_init_base1",
                         udata1, (LONGEST) pcdata1, (LONGEST) offset,
                         (LONGEST) cur_addr);
#endif
                  break;

                case dst_ln_init_base2:
                  COPY_AND_ADVANCE_LINES(pcdata1, lines_idx, objfile, 0, 4);
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 2);
                  cur_line = udata1;
                  cur_addr = pcdata1 + offset;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s line=%lld addr=0x%llx+0x%llx=0x%llx\n",
                         "dst_ln_init_base2",
                         udata1, (LONGEST) pcdata1, (LONGEST) offset,
                         (LONGEST) cur_addr);
#endif
                  break;

                case dst_ln_init_base3:
                  COPY_AND_ADVANCE_LINES(pcdata1, lines_idx, objfile, 0, 4);
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 3);
                  cur_line = udata1;
                  cur_addr = pcdata1 + offset;
                  record_doc_line(cur_file, cur_line, cur_column,
                                  cur_column_length, cur_addr, is_critical,
                                  has_column);
                  line_completed = 1;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s line=%lld addr=0x%llx+0x%llx=0x%llx\n",
                         "dst_ln_init_base3",
                         udata1, (LONGEST) pcdata1, (LONGEST) offset,
                         (LONGEST) cur_addr);
#endif
                  break;

                case dst_ln_col_run_1:
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 1);
                  COPY_AND_ADVANCE_LINES(udata2, lines_idx, objfile, 0, 1);
                  has_column = 1;
                  cur_column = udata1;
                  cur_column_length = udata2;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s data1=%llu data2=%llu\n", "dst_ln_col_run_1",
                         udata1, udata2);
#endif
                  break;

                case dst_ln_col_run_2:
                  COPY_AND_ADVANCE_LINES(udata1, lines_idx, objfile, 0, 2);
                  COPY_AND_ADVANCE_LINES(udata2, lines_idx, objfile, 0, 2);
                  has_column = 1;
                  cur_column = udata1;
                  cur_column_length = udata2;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s data1=%llu data2=%llu\n", "dst_ln_col_run_2",
                         udata1, udata2);
#endif
                  break;

                case dst_ln_ctx_1:
                  /* Ignore */
                  lines_idx++;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_ctx_1");
#endif
                  break;

                case dst_ln_ctx_2:
                  /* Ignore */
                  lines_idx += 2;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_ctx_2");
#endif
                  break;

                case dst_ln_ctx_4:
                  /* Ignore */
                  lines_idx += 4;
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_ctx_4");
#endif
                  break;

                case dst_ln_ctx_end:
                  /* Ignore */
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s\n", "dst_ln_ctx_end");
#endif
                  break;

                default:
                  /* unrecognized code */
                  if (!error_found) {
                    error_found = 1;
                    warning("Unknown escape (FLAG2) code in DOC lines table");
                  }
#ifdef DEBUG_LINES_INFO
                  printf("%20.20s escape=%u\n", "<Unknown>", escape);
#endif
                  break;
                } /* switch */
            } /* else if (def_pcdata == DST_LN_ESCAPE_FLAG2) */
          else if ((def_pcdata == DST_LN_CTX_SPEC1) ||
                   (def_pcdata == DST_LN_CTX_SPEC2))
            {   
#ifdef DEBUG_LINES_INFO
              if (def_pcdata == DST_LN_CTX_SPEC1)
                {
                  printf("%20.20s\n", "DST_LN_CTX_SPEC1");
                }
              else
                {
                  printf("%20.20s\n", "DST_LN_CTX_SPEC2");
                }
#endif
              /* ignore contexts */
            }
          else
            {
              def_pcdata *= PC_STEP;
              cur_line += def_lines_bufp->delta.ln_delta;
              lines_idx++;
              cur_addr += def_pcdata;
              record_doc_line(cur_file, cur_line, cur_column,
                              cur_column_length, cur_addr, is_critical,
                              has_column);
              line_completed = 1;
#ifdef DEBUG_LINES_INFO
              printf("%20.20s %20.20s +line=%d +addr=0x%llx\n", "DELTA", " ",
                     def_lines_bufp->delta.ln_delta, (LONGEST) def_pcdata);
#endif
            }
        } /* for */
    } /* if LINES */
}

/* Given a function "f" which is a member of a class, find
 * the classname that it is a member of. Used to construct
 * the name (e.g., "c::f") which GDB will put in the
 * "demangled name" field of the function's symbol.
 * Called from hpread_process_one_debug_symbol()
 * If "f" is not a member function, return NULL.
 */
char *
class_of (functype)
     struct type *functype;
{
  struct type *first_param_type;
  char *first_param_name;
  struct type *pointed_to_type;
  char *class_name;

  /* Check that the function has a first argument "this",
   * and that "this" is a pointer to a class. If not,
   * functype is not a member function, so return NULL.
   */
  if (TYPE_NFIELDS (functype) == 0)
    return NULL;
  first_param_name = TYPE_FIELD_NAME (functype, 0);
  if (first_param_name == NULL)
    return NULL;		/* paranoia */
  if (strcmp (first_param_name, "this"))
    return NULL;
  first_param_type = TYPE_FIELD_TYPE (functype, 0);
  if (first_param_type == NULL)
    return NULL;		/* paranoia */
  if (TYPE_CODE (first_param_type) != TYPE_CODE_PTR)
    return NULL;

  /* Get the thing that "this" points to, check that
   * it's a class, and get its class name.
   */
  pointed_to_type = TYPE_TARGET_TYPE (first_param_type);
  if (pointed_to_type == NULL)
    return NULL;		/* paranoia */
  if (TYPE_CODE (pointed_to_type) != TYPE_CODE_CLASS)
    return NULL;
  class_name = TYPE_NAME (pointed_to_type);
  if (class_name == NULL)
    return NULL;		/* paranoia */

  /* The class name may be of the form "class c", in which case
   * we want to strip off the leading "class ".
   */
  if (strncmp (class_name, "class ", 6) == 0)
    class_name += 6;

  return class_name;
}

#ifdef HPPA_DOC

#define IS_RANGE_INDEX_VALID(rt_size, rt_index) \
  (((rt_index) >= 0) && ((rt_index) < ((rt_size) / sizeof (struct range))))

/***************************************************************
 * make_range_list
 * Create a list of range records for a variable's location
 *
 * IN   range_index     Index to range table 
 * IN   sym             Current symbol 
 * IN   objfile         Current object file 
 */
void
make_range_list (range_index,
                 sym,
                 objfile)
     int range_index;
     struct symbol *sym;
     struct objfile *objfile;
{
  CORE_ADDR                 base_addr = 0;
  CORE_ADDR                 text_offset = CURRENT_FUNCTION_VALUE(objfile);
  boolean                   done = FALSE;
  int                       i;
  struct range_list        *range_list, *prev_range_list;
  struct range             *range_head;
  struct range             *range_p, *prev_range_ptr;
  void                     *val_ptr;
  int                       val_size;
  extern ImportEntry       *find_import_entry();
  ImportEntry              *import_entry;
  struct range             *range_table_ptr = hpread_get_rt(0, objfile);
  long                      range_table_size = RT_SIZE(objfile);
  struct symbol            *get_symbol(), *alias_sym = NULL;
  struct alias_list        *alias_list, *prev_alias_list;
    
  /* No symbol or invalid range */
  if ((!sym) || (!IS_RANGE_INDEX_VALID(range_table_size, range_index)))
    {
      return;
    }

  range_head = ((struct range *)(void *)range_table_ptr) + range_index;
  alias_sym = NULL;
  alias_list = prev_alias_list = NULL;
  prev_range_ptr = NULL;
  prev_range_list = NULL;

  while (!done)
    {
      if (!range_head)
        done = TRUE;
      else if (range_head->link)
        {
          range_p = range_head + 1;
          base_addr = range_head->lowaddr;

          if (range_head->u.link > 0)
            {
              if (!IS_RANGE_INDEX_VALID(range_table_size, range_head->u.link))
                return;

              range_head =
                ((struct range *)(void *) range_table_ptr) +
                  range_head->u.link;
	    }
	  else
            {
              done = TRUE;
            }
        }
      else
        {
           done = TRUE;
           range_p = range_head;
	}

      for(;;)
        {
          if (alias_sym == NULL)
            {
               alias_sym = get_symbol(objfile, SYMBOL_NAME(sym));
               alias_list = (struct alias_list *) 
                             obstack_alloc(&objfile->type_obstack, 
                             sizeof(struct alias_list));
               alias_list->sym = alias_sym;
               alias_list->next = prev_alias_list;
               prev_alias_list = alias_list;
               SYMBOL_ALIASES(sym) = alias_list;
               SYMBOL_CLASS(sym) = LOC_OPTIMIZED_OUT;
            }
           
          if (prev_range_ptr &&
             (prev_range_ptr->unknown != range_p->unknown || 
              prev_range_ptr->loc_type != range_p->loc_type ||
              memcmp(&prev_range_ptr->u.location.loc,
                     &range_p->u.location.loc,
                     sizeof(range_p->u.location.loc))))
            {
               alias_list = (struct alias_list *)
                             obstack_alloc(&objfile->type_obstack, 
                             sizeof(struct alias_list));
               alias_list->sym = get_symbol(objfile, SYMBOL_NAME(sym));;
               alias_list->next = prev_alias_list;
               prev_alias_list = alias_list;
               SYMBOL_ALIASES(sym) = alias_list;
               SYMBOL_RANGES(alias_sym) = range_list;
               alias_sym = alias_list->sym;
               prev_range_ptr = NULL;
               prev_range_list = NULL;
            }
            
          range_list = (struct range_list *)
                        obstack_alloc(&objfile->type_obstack, 
                        sizeof(struct range_list));

          range_list->next = prev_range_list;
          prev_range_list = range_list; 
          prev_range_ptr = range_p;
          range_list->unknown = range_p->unknown;
          range_list->set_early = range_p->set_early;
          range_list->set_late = range_p->set_late;
          range_list->comes_from_line = range_p->u.location.comes_from_line;
	  range_list->moved_to_line = range_p->u.location.moved_to_line;

          SYMBOL_TYPE(alias_sym) = SYMBOL_TYPE(sym);
          SYMBOL_CLASS(alias_sym) = LOC_STATIC;
          range_list->start = range_p->lowaddr +
                              (base_addr ? base_addr : text_offset);
          range_list->end = range_p->hiaddr +
                            (base_addr ? base_addr : text_offset);

          if (range_list->unknown)
            SYMBOL_CLASS(alias_sym) = LOC_OPTIMIZED_OUT;

          if (!range_p->unknown)
            {
              switch (range_p->loc_type)
                {
		case loc_static:
		  if (import_entry = find_import_entry(SYMBOL_NAME(sym),
						       objfile))
		    {
		      SYMBOL_CLASS(alias_sym) = LOC_INDIRECT;
		      SYMBOL_VALUE_ADDRESS(alias_sym) = 
			import_entry->addr.dlt_addr;
		    }
		  else
		    {
		      SYMBOL_CLASS(alias_sym) = LOC_STATIC;
		      SYMBOL_VALUE_ADDRESS(alias_sym) = 
			range_p->u.location.loc.args.arg_first.statloc; 
		    } 
		  break;

		case loc_dynamic:
		  /* Check if the original symbol was an argument */
		  if ((SYMBOL_CLASS(sym) == LOC_ARG) ||
		      (SYMBOL_CLASS(sym) == LOC_REF_ARG) ||
		      (SYMBOL_CLASS(sym) == LOC_REGPARM) ||
		      (SYMBOL_CLASS(sym) == LOC_REGPARM_ADDR) ||
		      (SYMBOL_CLASS(sym) == LOC_LOCAL_ARG) ||
		      (SYMBOL_CLASS(sym) == LOC_BASEREG_ARG))
		    {
		      SYMBOL_CLASS(alias_sym) = LOC_LOCAL_ARG;
		    }
		  else
		    {
		      SYMBOL_CLASS(alias_sym) = LOC_LOCAL;
		    }
		  SYMBOL_VALUE(alias_sym) =
		    range_p->u.location.loc.args.arg_first.dynloc;
		  break;

		case loc_register:
		  SYMBOL_CLASS(alias_sym) = LOC_REGISTER;
		  SYMBOL_VALUE(alias_sym) =
		    range_p->u.location.loc.args.arg_first.regloc;
		  break;

		case loc_address_constant:
		case loc_integer_constant:
		case loc_float_constant:
		case loc_dbl_constant:
		  if (range_p->loc_type == loc_address_constant)
		    val_size = sizeof(CORE_ADDR);
		  else if (range_p->loc_type == loc_integer_constant)
		    val_size = sizeof(int);
		  else if (range_p->loc_type == loc_float_constant)
		    val_size = sizeof(float);
		  else if (range_p->loc_type == loc_dbl_constant)
		    val_size = sizeof(double);
		  else
		    error ("Internal error reading range table information");

		  val_ptr = (void *) obstack_alloc(&objfile->type_obstack,
						   val_size); 
		  memcpy (val_ptr, 
			  (char *) &(range_p->u.location.loc.args.arg_first), 
			  val_size);

		  SYMBOL_CLASS(alias_sym) = LOC_CONST_BYTES;
		  SYMBOL_VALUE_BYTES(alias_sym) = val_ptr; 
		  break;

		case loc_reg_relative:
		case loc_reg_add_const:
		case loc_reg_mul_const:
		case loc_const_sub_reg:
		  if (range_p->loc_type == loc_const_sub_reg)
		    {
		      SYMBOL_BASEREG(alias_sym) = 
			range_p->u.location.loc.args.arg_second.regloc;
		      SYMBOL_VALUE(alias_sym) =
			range_p->u.location.loc.args.arg_first.intconst;
		    }
		  else
		    {
		      SYMBOL_BASEREG(alias_sym) =
			range_p->u.location.loc.args.arg_first.regloc;
		      SYMBOL_VALUE(alias_sym) =
			range_p->u.location.loc.args.arg_second.intconst;
		    }

		  if (range_p->loc_type == loc_reg_relative)
		    SYMBOL_CLASS(alias_sym) = LOC_BASEREG;
		  else if (range_p->loc_type == loc_reg_add_const)
		    SYMBOL_CLASS(alias_sym) = LOC_REG_ADD_CONST;
		  else if (range_p->loc_type == loc_reg_mul_const)
		    SYMBOL_CLASS(alias_sym) = LOC_REG_MUL_CONST;
		  else if (range_p->loc_type == loc_const_sub_reg)
		    SYMBOL_CLASS(alias_sym) = LOC_CONST_SUB_REG;
		  else
		    error ("Internal error reading range table information");
		  break;

		case loc_mem_add_const:
		case loc_mem_mul_const:
		  SYMBOL_MEMADDR(alias_sym) =
		    range_p->u.location.loc.args.arg_first.addrconst;
		  SYMBOL_VALUE(alias_sym) =
		    range_p->u.location.loc.args.arg_second.intconst;

		  if (range_p->loc_type == loc_mem_add_const)
		    SYMBOL_CLASS(alias_sym) = LOC_MEM_ADD_CONST;
		  else if (range_p->loc_type == loc_mem_mul_const)
		    SYMBOL_CLASS(alias_sym) = LOC_MEM_MUL_CONST;
		  else
		    error ("Internal error reading range table information");

		  break;

		case loc_dwarf2:
		case loc_dwarf2_constant:
		  /* Not handled */
		  break;

		default:
		  warning ("Unknown entry in range table information");

                } /* switch (range_p->loc_type) */
            } /* if (!range_p->unknown) */

          if (range_p->last)
            {
              SYMBOL_RANGES(alias_sym) = range_list;
              break;
            }

          range_p++;
        }
    }
}
#endif /* HPPA_DOC */

extern char default_fortran_main_string[];

/* Internalize one native debug symbol. 
 * Called in a loop from hpread_expand_symtab(). 
 * Arguments:
 *   dn_bufp: 
 *   name: 
 *   section_offsets:
 *   objfile:
 *   text_offset: 
 *   text_size: 
 *   filename: 
 *   index:             Index of this symbol
 *   at_module_boundary_p Pointer to boolean flag to control caller's loop.
 */

/* CM: Uncomment out the next line to print the DNTT entries as they
   are processed. */
/* #define PRINT_DNTT_ENTRY_AS_PRCOESSED */

static void
hpread_process_one_debug_symbol (dn_bufp, name, section_offsets, objfile,
				 text_offset, text_size, filename,
                                 index, at_module_boundary_p,
                                 expanding_doom_psymtab
)
     union dnttentry *dn_bufp;
     char *name;
     struct section_offsets *section_offsets;
     struct objfile *objfile;
     CORE_ADDR text_offset;
     int text_size;
     char *filename;
     int index;
     int *at_module_boundary_p;
     int expanding_doom_psymtab;
{
  unsigned long desc;
  int type;
  CORE_ADDR valu = 0;
  CORE_ADDR texthigh = text_offset+text_size;
  CORE_ADDR offset = ANOFFSET (section_offsets, SECT_OFF_TEXT (objfile));
#ifdef GDB_TARGET_IS_HPPA_20W
  CORE_ADDR data_base_address =   ANOFFSET (section_offsets, SECT_OFF_DATA (objfile))
                                + elf_data_start_vma(objfile->obfd);
  CORE_ADDR text_base_address =   ANOFFSET (section_offsets, SECT_OFF_DATA (objfile))
                                + elf_text_start_vma(objfile->obfd);
  CORE_ADDR value_addr;
#endif
  CORE_ADDR data_offset = ANOFFSET (section_offsets, SECT_OFF_DATA (objfile)); /* CLLbs15725 */
  union dnttentry *dn_temp;
  dnttpointer hp_type;
  struct symbol * get_symbol(), *sym = NULL;
  struct context_stack *new = NULL;
  char *class_scope_name;
  extern ImportEntry *find_import_entry (); /* in somread.c */
  ImportEntry *import_entry;
  CORE_ADDR ptr_addr;
  struct minimal_symbol * indirect_msym;
  int sym_in_import_list;
  int do_record;
  sltpointer sl_index;
  union sltentry *sl_bufp; 
  int nsym;
  struct pending *local_symbols_copy, *next, *next1, *save_local_symbols;
  char *p;

  /* 
   * srikanth, 990313, JAGaa80452, we used to allocate a `symbol' from
   * the symbol_obstack here and do some minimal decoration on it. Only 
   * in less than half the number of calls to this function, we actually 
   * need this `symbol'.
   * 
   * For instance if the incoming dntt type is one of  srcfile, module, 
   * begin, end, pointer, enum, memenum, set, subrange, struct, union, 
   * field, functype, with, common, blockdata, class_scope, reference, 
   * ptrmem, ptrmemfunc, class, genfield, vfunc, memaccess, inheritance, 
   * friend_class, friend_func, modifier, object_id, memfunc, doc_memfunc, 
   * template, template_arg, func_template, link ... (out of breath here)
   * then we don't need the `symbol' and we simply leak it. 
   * 
   * What is worse since this symbol is allocated on an obstack, purify and 
   * other such tools would be unable to report the leak. When debugging the 
   * IA64 C compiler, on startup we leak > 15 Mb here. I have added a new 
   * function get_symbol() that is called only on those cases we actually 
   * need the symbol. Remember to call this function if you decide to change 
   * one of the aforementioned cases so that it actually needs a symbol.
   * 
   */

  /* Just a trick in case the SOM debug symbol is a type definition.
   * There are routines that are set up to build a GDB type symbol, given
   * a SOM dnttpointer. So we set up a dummy SOM dnttpointer "hp_type".
   * This allows us to call those same routines.
   */
  hp_type.dnttp.extension = 1;
  hp_type.dnttp.immediate = 0;
  hp_type.dnttp.global = 0;
  hp_type.dnttp.index = index;

  alloca (0); /* srikanth, 000210, force the GC to run */

  /* This "type" is the type of SOM record.
   * Switch on SOM type.
   */
  type = dn_bufp->dblock.kind;
  switch (type)
    {
    case DNTT_TYPE_SRCFILE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_SRCFILE", name);
#endif
      /* This type of symbol indicates from which source file or
       * include file any following data comes. It may indicate:
       *
       * o   The start of an entirely new source file (and thus
       *     a new module)
       *
       * o   The start of a different source file due to #include
       *
       * o   The end of an include file and the return to the original
       *     file. Thus if "foo.c" includes "bar.h", we see first
       *     a SRCFILE for foo.c, then one for bar.h, and then one for
       *     foo.c again.
       *
       * If it indicates the start of a new module then we must
       * finish the symbol table of the previous module 
       * (if any) and start accumulating a new symbol table.  
       */

      valu = text_offset;
      if (!last_source_file)
	{
	  /*
	   * A note on "last_source_file": this is a char* pointing
	   * to the actual file name.  "start_symtab" sets it,
	   * "end_symtab" clears it.
	   *
	   * So if "last_source_file" is NULL, then either this is
	   * the first record we are looking at, or a previous call
	   * to "end_symtab()" was made to close out the previous
	   * module.  Since we're now quitting the scan loop when we
	   * see a MODULE END record, we should never get here, except
	   * in the case that we're not using the quick look-up tables
	   * and have to use the old system as a fall-back.
	   */
          start_symtab (name, current_compilation_directory, valu);
	  record_debugformat ("HP");
	  SL_INDEX (objfile) = dn_bufp->dsfile.address;
	}
      else
        {
	  /* Either a new include file, or a SRCFILE record
	     saying we are back in the main source (or out of
	     a nested include file) again. */
	}

      /* A note on "start_subfile".  This routine will check
       * the name we pass it and look for an existing subfile
       * of that name.  There's thus only one sub-file for the
       * actual source (e.g. for "foo.c" in foo.c), despite the
       * fact that we'll see lots of SRCFILE entries for foo.c
       * inside foo.c.
       */
      start_subfile (name, current_compilation_directory);
      record_debugformat ("HP");
      /* RM: If we don't know the language for the current symtab yet,
         set it now */
      if (current_symtab_language == language_unknown)
        current_symtab_language =
          hpread_language_type(dn_bufp->dsfile.language);
      break;
      
    case DNTT_TYPE_MODULE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_MODULE", name);
#endif
      /*
       * We no longer ignore DNTT_TYPE_MODULE symbols.  The module 
       * represents the meaningful semantic structure of a compilation
       * unit.  We expect to start the psymtab-to-symtab expansion
       * looking at a MODULE entry, and to end it at the corresponding
       * END MODULE entry.
       */

      /* We currently want to bypass f90 module entries that surround
       * functions other than the main function -- we can distinguish
       * these modules from others because they have "%%" postpended to
       * the function name.
       */
      if ((p = strchr (name, '%')) && (strcmp (p, "%%") == 0))
      {
        /* This is a currently just a place holder for f90 modules. */
      }
      else
      {
      valu = text_offset;
#if 0
      /* The module surrounding the f90 main function has as its name,
       * the module name, not the SRCFILE name; we need to get it from
       * the preceding SRCFILE entry.
       */

      if (index - 1 == 0) {
        dn_temp = hpread_get_lntt(index-1, objfile);
        if (dn_temp->dblock.kind == DNTT_TYPE_SRCFILE &&
            dn_temp->dsfile.language == HP_LANGUAGE_FORTRAN)
          name =  VT (objfile) + dn_temp->dsfile.name;
      }
#endif
      if (!last_source_file)
	{
	  /* Start of a new module. We know this because "last_source_file"
	   * is NULL, which can only happen the first time or if we just 
	   * made a call to end_symtab() to close out the previous module.
	   */
          start_symtab (name, current_compilation_directory, valu);
	  record_debugformat ("HP");
	  SL_INDEX (objfile) = dn_bufp->dmodule.address;
	}
      else
	{
	  /* This really shouldn't happen if we're using the quick
	   * look-up tables, as it would mean we'd scanned past an
	   * END MODULE entry.  But if we're not using the tables,
	   * we started the module on the SRCFILE entry, so it's ok.
	   * For now, accept this.
	   */
	  /* warning( "Error expanding psymtab, missed module end, found entry for %s",
	   *           name );
	   */
	  *at_module_boundary_p = -1;
	}

       start_subfile (name, current_compilation_directory);
       record_debugformat ("HP");
      }
      break;

    case DNTT_TYPE_FUNCTION:
    case DNTT_TYPE_ENTRY:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s valu = 0x%016llx texthigh = 0x%016llx\n", "DNTT_TYPE_FUNCTION", name, dn_bufp->dfunc.lowaddr + offset, texthigh);
#endif
      sym = get_symbol(objfile, name);
      valu = (type == DNTT_TYPE_FUNCTION) 
             ? dn_bufp->dfunc.lowaddr + offset
             : dn_bufp->dfunc.entryaddr + offset;

      /* RM: Because of heuristics used to find the beginning and end
       * LNTT offsets for psymtabs in the presence of CTTI, we
       * sometimes include too many LNTT entries in a psymtab. This is
       * often harmless, but we don't want extra functions in the
       * expanded symtab. So if this function starts after the
       * psymtabs texthigh address, close the symtab
       */
      if (valu >= texthigh)
        {
          *at_module_boundary_p = -1;
          break;
        }

      CURRENT_FUNCTION_VALUE (objfile) = valu;

      /* Record lines for this function. */
      hpread_record_function_lines (dn_bufp->dfunc.address, objfile,
#ifdef GDB_TARGET_IS_HPPA_20W
                                    /* lintable pc changed
                                     * to offset for PA ELF
                                     */
                                    CURRENT_FUNCTION_VALUE (objfile)
#else
                                    offset
#endif
                                    );

      WITHIN_FUNCTION (objfile) = 1;
      
      /* Stack will not be empty if we've encountered a DNTT_TYPE_ENTRY. */
      if (context_stack_depth && type != DNTT_TYPE_ENTRY)
        complain (&lbrac_unmatched_complaint, (char *) symnum);
      /* push_context initializes the local_symbols list to null, so
       * we need to save this information for entry points
       */
      save_local_symbols = local_symbols;
      new = push_context (0, valu);
      if (type == DNTT_TYPE_ENTRY)
        local_symbols = save_local_symbols;

      /* Built a type for the function. This includes processing
       * the symbol records for the function parameters.
       */
      SYMBOL_CLASS (sym) = LOC_BLOCK;
      SYMBOL_TYPE (sym) = hpread_read_function_type (hp_type, dn_bufp, objfile, 1);
      new->params=param_symbols;
      SYMBOL_LANGUAGE (sym) = hpread_language_type (dn_bufp->dfunc.language);

      /* The "SYMBOL_NAME" field is expected to be the mangled name
       * (if any), which we get from the "alias" field of the SOM record
       * if that exists.
       */
      if (dn_bufp->dfunc.alias && /* has an alias */ 
	  *(char *) (VT (objfile) + dn_bufp->dfunc.alias))	/* not a null string */
	SYMBOL_NAME (sym) = VT (objfile) + dn_bufp->dfunc.alias;
      else
	SYMBOL_NAME (sym) = VT (objfile) + dn_bufp->dfunc.name;

      /* Special hack to get around HP compilers' insistence on
       * reporting "main" as "_MAIN_" */
      if (STRCMP (SYMBOL_NAME (sym), "_MAIN_") == 0) {
        /* If this this a Fortran main, save dfunc.name as the string for 
           the demangled name only if dfunc.name is not "main"; this situation
           occurs when Fortran sees the program keyword.
         */
        if (SYMBOL_LANGUAGE (sym) == language_fortran) {
          if  (STRCMP(VT (objfile) + dn_bufp->dfunc.name, "main") != 0)
            SYMBOL_FORTRAN_DEMANGLED_NAME(sym) = VT (objfile) + dn_bufp->dfunc.name;
          default_main = default_fortran_main_string;
        }
        SYMBOL_NAME (sym) = default_main;
      }

      /* The SYMBOL_CPLUS_DEMANGLED_NAME field is expected to
       * be the demangled name.
       */
      if (SYMBOL_LANGUAGE (sym) == language_cplus)
	{
	  /* SYMBOL_INIT_DEMANGLED_NAME is a macro which winds up
	   * calling the demangler in libiberty (cplus_demangle()) to
	   * do the job. This generally does the job, even though
	   * it's intended for the GNU compiler and not the aCC compiler
	   * Note that SYMBOL_INIT_DEMANGLED_NAME calls the
	   * demangler with arguments DMGL_PARAMS | DMGL_ANSI.
	   * Generally, we don't want params when we display
	   * a demangled name, but when I took out the DMGL_PARAMS,
	   * some things broke, so I'm leaving it in here, and
	   * working around the issue in stack.c. - RT
	   */
	  SYMBOL_INIT_DEMANGLED_NAME (sym, &objfile->symbol_obstack);
	  if ((SYMBOL_NAME (sym) == VT (objfile) + dn_bufp->dfunc.alias) &&
	      (!SYMBOL_CPLUS_DEMANGLED_NAME (sym)))
	    {

	      /* Well, the symbol name is mangled, but the
	       * demangler in libiberty failed so the demangled
	       * field is still NULL. Try to
	       * do the job ourselves based on the "name" field
	       * in the SOM record. A complication here is that
	       * the name field contains only the function name
	       * (like "f"), whereas we want the class qualification
	       * (as in "c::f"). Try to reconstruct that.
	       */
	      char *basename;
	      char *classname;
	      char *dem_name;
	      basename = VT (objfile) + dn_bufp->dfunc.name;
	      classname = class_of (SYMBOL_TYPE (sym));
	      if (classname)
		{
		  dem_name = xmalloc (strlen (basename) + strlen (classname) + 3);
		  strcpy (dem_name, classname);
		  strcat (dem_name, "::");
		  strcat (dem_name, basename);
		  SYMBOL_CPLUS_DEMANGLED_NAME (sym) = dem_name;
		}
	    }
	}
      else if (SYMBOL_LANGUAGE (sym) == language_fortran &&
               strchr (SYMBOL_NAME (sym), '$') &&
               (strcmp (strchr (SYMBOL_NAME (sym), '$') + 1, name)) == 0)
        {
          /* We have a module_name$name symbol; save name as
             demangled name */
          char *dem_name = xmalloc (strlen (name) + 1);
          strcpy (dem_name, name);
	  SYMBOL_FORTRAN_DEMANGLED_NAME (sym) = dem_name;
        }

      /* Add the function symbol to the list of symbols in this blockvector */
      if (dn_bufp->dfunc.global)
	add_symbol_to_list (sym, &global_symbols);
      else
	add_symbol_to_list (sym, &file_symbols);
      new->name = sym;

      /* Search forward to the next BEGIN and also read
       * in the line info up to that point. 
       * Not sure why this is needed.
       * In HP FORTRAN this code is harmful since there   
       * may not be a BEGIN after the FUNCTION.
       * So I made it C/C++ specific. - RT
       */
      if (SYMBOL_LANGUAGE (sym) == language_c ||
          SYMBOL_LANGUAGE (sym) == language_cplus)
        {
	  while (dn_bufp->dblock.kind != DNTT_TYPE_BEGIN)
	    {
	      dn_bufp = hpread_get_lntt (++index, objfile);
	      if (dn_bufp->dblock.extension)
		continue;
	    }
	  SYMBOL_LINE (sym) = hpread_get_line (dn_bufp->dbegin.address,
					       objfile);
	  record_line (current_subfile, SYMBOL_LINE (sym), valu);
	}
      else
        {
	  /* For cases where there is no DNTT_BEGIN, namely Fortran, use the 
	     the line number associated with the function's SLT index. */
	  SYMBOL_LINE (sym) = hpread_get_line (dn_bufp->dfunc.address,
					       objfile);
	  do_record = true;
	  if (SYMBOL_LANGUAGE (sym) == language_fortran)
	    {
	      if (STRCMP (SYMBOL_NAME (sym),
			  default_fortran_main_string) == 0)  
		/* Don't record this line if this is the main program -- this
		   test is necessary so that setting a breakpoint specifying
		   the first line of a Fortran main program works correctly. */
		do_record = false;
	      else if (context_stack_depth > 1)
	        {
		  /* We've encountered an entry point; don't record
		     this line if there is no code in the function prior to
		     this entry point;
	             this is necessary so that we break in the proper
		     location when setting a breakpoint on a function
		     which has entry points. */
		  do_record = false;
		  sl_index = dn_bufp->dfunc.address;
		  sl_bufp = hpread_get_slt (--sl_index, objfile);
		  while (sl_bufp->snorm.sltdesc != SLT_FUNCTION
			 && sl_bufp->snorm.sltdesc != SLT_ENTRY)
		    {
		      if (sl_bufp->snorm.sltdesc == SLT_NORMAL)
		        {
			  do_record = true;
			  break;
			}
		      else
			sl_bufp = hpread_get_slt (--sl_index, objfile);
		    } 
		}
	    }
	  if (do_record)
	    record_line (current_subfile, SYMBOL_LINE (sym), valu);
	}
      /* RM: If we don't know the language for the current symtab yet,
         set it now */
      if (current_symtab_language == language_unknown)
        current_symtab_language = SYMBOL_LANGUAGE(sym);
      break;

    case DNTT_TYPE_DOC_FUNCTION:
      sym = get_symbol(objfile, name);
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_DOC_FUNCTION", name);
#endif
      valu = dn_bufp->ddocfunc.lowaddr + offset;

      CURRENT_FUNCTION_VALUE (objfile) = valu;

      /* Record lines for this function. */
      hpread_record_doc_function_lines (dn_bufp->ddocfunc.address,
                                        dn_bufp->ddocfunc.lt_offset,
                                        dn_bufp->ddocfunc.hiaddr,
                                        objfile,
#ifdef GDB_TARGET_IS_HPPA_20W
                                        /* lintable pc changed
                                         * to offset for PA ELF
                                         */
                                        CURRENT_FUNCTION_VALUE (objfile)
#else
                                        offset
#endif
                                        );

      WITHIN_FUNCTION (objfile) = 1;

      /* Stack must be empty now.  */
      if (context_stack_depth != 0)
	complain (&lbrac_unmatched_complaint, (char *) symnum);
      new = push_context (0, valu);

      /* Built a type for the function. This includes processing
       * the symbol records for the function parameters.
       */
      SYMBOL_CLASS (sym) = LOC_BLOCK;
      SYMBOL_TYPE (sym) = hpread_read_doc_function_type (hp_type, dn_bufp, objfile, 1);
      new->params=param_symbols;
      SYMBOL_LANGUAGE (sym) = hpread_language_type (dn_bufp->dfunc.language);

      /* The "SYMBOL_NAME" field is expected to be the mangled name
       * (if any), which we get from the "alias" field of the SOM record
       * if that exists.
       */
      if (dn_bufp->ddocfunc.alias && /* has an alias */ 
	  *(char *) (VT (objfile) + dn_bufp->ddocfunc.alias))	/* not a null string */
	SYMBOL_NAME (sym) = VT (objfile) + dn_bufp->ddocfunc.alias;
      else
	SYMBOL_NAME (sym) = VT (objfile) + dn_bufp->ddocfunc.name;

      /* Special hack to get around HP compilers' insistence on
       * reporting "main" as "_MAIN_" */
      if (strcmp (SYMBOL_NAME (sym), "_MAIN_") == 0) {
        if (SYMBOL_LANGUAGE (sym) == language_fortran) {
          if  (strcmp(VT (objfile) + dn_bufp->dfunc.name, "main") != 0)
            SYMBOL_FORTRAN_DEMANGLED_NAME(sym) = VT (objfile) + dn_bufp->dfunc.name;
          default_main = default_fortran_main_string;
        }
        SYMBOL_NAME (sym) = default_main;
      }

      
      if (SYMBOL_LANGUAGE (sym) == language_cplus)
	{

	  /* SYMBOL_INIT_DEMANGLED_NAME is a macro which winds up
	   * calling the demangler in libiberty (cplus_demangle()) to
	   * do the job. This generally does the job, even though
	   * it's intended for the GNU compiler and not the aCC compiler
	   * Note that SYMBOL_INIT_DEMANGLED_NAME calls the
	   * demangler with arguments DMGL_PARAMS | DMGL_ANSI.
	   * Generally, we don't want params when we display
	   * a demangled name, but when I took out the DMGL_PARAMS,
	   * some things broke, so I'm leaving it in here, and
	   * working around the issue in stack.c. - RT 
	   */
	  SYMBOL_INIT_DEMANGLED_NAME (sym, &objfile->symbol_obstack);

	  if ((SYMBOL_NAME (sym) == VT (objfile) + dn_bufp->ddocfunc.alias) &&
	      (!SYMBOL_CPLUS_DEMANGLED_NAME (sym)))
	    {

	      /* Well, the symbol name is mangled, but the
	       * demangler in libiberty failed so the demangled
	       * field is still NULL. Try to
	       * do the job ourselves based on the "name" field
	       * in the SOM record. A complication here is that
	       * the name field contains only the function name
	       * (like "f"), whereas we want the class qualification
	       * (as in "c::f"). Try to reconstruct that.
	       */
	      char *basename;
	      char *classname;
	      char *dem_name;
	      basename = VT (objfile) + dn_bufp->ddocfunc.name;
	      classname = class_of (SYMBOL_TYPE (sym));
	      if (classname)
		{
		  dem_name = xmalloc (strlen (basename) + strlen (classname) + 3);
		  strcpy (dem_name, classname);
		  strcat (dem_name, "::");
		  strcat (dem_name, basename);
		  SYMBOL_CPLUS_DEMANGLED_NAME (sym) = dem_name;
		}
	    }
	}

      /* Add the function symbol to the list of symbols in this blockvector */
      if (dn_bufp->ddocfunc.global)
	add_symbol_to_list (sym, &global_symbols);
      else
	add_symbol_to_list (sym, &file_symbols);
      new->name = sym;

      /* Search forward to the next BEGIN and also read
       * in the line info up to that point. 
       * Not sure why this is needed.
       * In HP FORTRAN this code is harmful since there   
       * may not be a BEGIN after the FUNCTION.
       * So I made it C/C++ specific. - RT
       */
      if (SYMBOL_LANGUAGE (sym) == language_c ||
          SYMBOL_LANGUAGE (sym) == language_cplus)
	{
	  while (dn_bufp->dblock.kind != DNTT_TYPE_BEGIN)
	    {
	      dn_bufp = hpread_get_lntt (++index, objfile);
	      if (dn_bufp->dblock.extension)
		continue;
	    }
        SYMBOL_LINE (sym) = hpread_get_line (dn_bufp->dbegin.address, objfile);
        record_line (current_subfile, SYMBOL_LINE (sym), valu);
      }
      else {
        /* For cases where there is no DNTT_BEGIN use the function's SLT
           index as the first line of the function
         */
        SYMBOL_LINE (sym) = hpread_get_line (dn_bufp->dfunc.address, objfile);
        if (SYMBOL_LANGUAGE (sym) == language_fortran
            && (STRCMP (SYMBOL_NAME (sym), default_fortran_main_string) != 0))
          record_line (current_subfile, SYMBOL_LINE (sym), valu);
      }
      /* RM: If we don't know the language for the current symtab yet,
         set it now */
      if (current_symtab_language == language_unknown)
        current_symtab_language = SYMBOL_LANGUAGE(sym);
      break;

    case DNTT_TYPE_BEGIN:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_BEGIN", name);
#endif
      /* Begin a new scope. */
      if (context_stack_depth == 1 /* this means we're at function level */  &&
	  context_stack[0].name != NULL /* this means it's a function */  &&
	  context_stack[0].depth == 0	/* this means it's the first BEGIN 
					   we've seen after the FUNCTION */
	)
	{
	  /* This is the first BEGIN after a FUNCTION.
	   * We ignore this one, since HP compilers always insert
	   * at least one BEGIN, i.e. it's:
	   * 
	   *     FUNCTION
	   *     argument symbols
	   *     BEGIN
	   *     local symbols
	   *        (possibly nested BEGIN ... END's if there are inner { } blocks)
	   *     END
	   *     END
	   *
	   * By ignoring this first BEGIN, the local symbols get treated
	   * as belonging to the function scope, and "print func::local_sym"
	   * works (which is what we want).
	   */

	  /* All we do here is increase the depth count associated with
	   * the FUNCTION entry in the context stack. This ensures that
	   * the next BEGIN we see (if any), representing a real nested { }
	   * block, will get processed.
	   */

	  context_stack[0].depth++;

	}
      else
	{
	  /* Calculate start address of new scope */
	  valu = hpread_get_location (dn_bufp->dbegin.address, objfile, offset);
	  valu += offset;	/* Relocate for dynamic loading */
	  /* We use the scope start DNTT index as nesting depth identifier! */
	  desc = hpread_get_scope_start (dn_bufp->dbegin.address, objfile);
	  new = push_context (desc, valu);
	}
      break;

    case DNTT_TYPE_END:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s", "DNTT_TYPE_END", name);
#endif
      /* End a scope.  */

      /* Valid end kinds are:
       *  MODULE
       *  FUNCTION
       *  WITH
       *  COMMON
       *  BEGIN
       *  CLASS_SCOPE
       */

      /* RM: with aCC, pxdb seems to be fixing up the endkind field, so
       * we can't use it directly for DOOM. Do this the round about
       * way.
       */
      dn_temp =
        hpread_get_lntt (INDEX(objfile,dn_bufp->dend.beginscope.dnttp),
                         objfile);
      
      switch (dn_temp->dblock.kind)
        {
        case DNTT_TYPE_MODULE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- DNTT_TYPE_MODULE", name);
#endif
	  /* Ending a module ends the symbol table for that module.  
	   * Calling end_symtab() has the side effect of clearing the
	   * last_source_file pointer, which in turn signals 
	   * process_one_debug_symbol() to treat the next DNTT_TYPE_SRCFILE
	   * record as a module-begin.
	   */
	  valu = text_offset + text_size + offset;

	  /* Tell our caller that we're done with expanding the
	   * debug information for a module.
	   */
	  *at_module_boundary_p = 1;

	  /* Don't do this, as our caller will do it!

	   *      (void) end_symtab (valu, objfile, 0);
	   */
	  break;

	case DNTT_TYPE_FUNCTION:
        case DNTT_TYPE_DOC_FUNCTION:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- DNTT_TYPE_FUNCTION", name);
#endif
          /* RM: if we never pushed this function onton the context stack,
           * don't try to pop it
           * ??? What happens to all the symbols we processed in between?
           */
          if (context_stack_depth == 0)
            break;

	  /* Ending a function, well, ends the function's scope.  */
          dn_temp = hpread_get_lntt (INDEX(objfile,dn_bufp->dend.beginscope.dnttp),
				     objfile);
	  valu = dn_temp->dfunc.hiaddr + offset;
          while (context_stack_depth > 0)
	    {
	      new = pop_context ();
	      param_symbols = new->params;
	      /* Insert func params into local list */
            /* Note: finish_block will delete the symbol list, so make
             * a copy of local_symbols, but eliminate the symbols
             * that are the same as the parameters 
             */
            local_symbols_copy = NULL;
            for (next = local_symbols; next; next = next->next) {
                nsym = 0;
                while (next && nsym < next->nsyms) {
                  if (!find_symbol_in_list (param_symbols,
                      SYMBOL_NAME (next->symbol[nsym]),
                      strlen (SYMBOL_NAME (next->symbol[nsym]))))
                    add_symbol_to_list (next->symbol[nsym], 
                                        &local_symbols_copy);
                  nsym++;
                }
            }
            merge_symbol_lists (&param_symbols, &local_symbols_copy);
              
            /* Make a block for the local symbols within. */
            finish_block (new->name, &local_symbols_copy, new->old_blocks,
                          new->start_addr, valu, objfile);
          }
          /* free local_symbols list */
          if (local_symbols)
	    add_free_pendings (local_symbols);

          local_symbols = new->locals;
	  WITHIN_FUNCTION (objfile) = 0;	/* This may have to change for Pascal */
	  break;

	case DNTT_TYPE_BEGIN:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- DNTT_TYPE_BEGIN", name);
#endif
	  if (context_stack_depth == 1 &&
	      context_stack[0].name != NULL &&
	      context_stack[0].depth == 1)
	    {
	      /* This is the END corresponding to the
	       * BEGIN which we ignored - see DNTT_TYPE_BEGIN case above.
	       */
	      context_stack[0].depth--;
	    }
	  else
	    {
	      /* Ending a local scope.  */
	      valu = hpread_get_location (dn_bufp->dend.address, objfile, offset);
	      /* Why in the hell is this needed?  */
	      valu += offset + 9;	/* Relocate for dynamic loading */
	      new = pop_context ();
	      desc = INDEX(objfile,dn_bufp->dend.beginscope.dnttp);
	      if (desc != new->depth)
		complain (&lbrac_mismatch_complaint, (char *) symnum);

	      /* Make a block for the local symbols within.  */
	      finish_block (new->name, &local_symbols, new->old_blocks,
			    new->start_addr, valu, objfile);
	      local_symbols = new->locals;
	      param_symbols = new->params;
	    }
	  break;

	case DNTT_TYPE_WITH:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- DNTT_TYPE_WITH", name);
#endif
	  /* Since we ignore the DNTT_TYPE_WITH that starts the scope,
	   * we can ignore the DNTT_TYPE_END that ends it.
	   */
	  break;

	case DNTT_TYPE_COMMON:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- DNTT_TYPE_COMMON", name);
#endif
          /* End a FORTRAN common block. */
          common_block_end (objfile);
#ifdef GDB_TARGET_IS_HPPA_20W
          processing_common_block = 0;
#endif
          break;

        case DNTT_TYPE_CLASS_SCOPE: 
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- DNTT_TYPE_CLASS_SCOPE", name);
#endif
	  /* pai: FIXME Not handling nested classes for now -- must
	     * maintain a stack */
	  class_scope_name = NULL;

#if 0
	  /* End a class scope */
	  valu = hpread_get_location (dn_bufp->dend.address, objfile, offset);
	  /* Why in the hell is this needed?  */
	  valu += offset + 9;	/* Relocate for dynamic loading */
	  new = pop_context ();
          desc = INDEX(objfile,dn_bufp->dend.beginscope.dnttp);
	  if (desc != new->depth)
	    complain (&lbrac_mismatch_complaint, (char *) symnum);
	  /* Make a block for the local symbols within.  */
	  finish_block (new->name, &local_symbols, new->old_blocks,
			new->start_addr, valu, objfile);
	  local_symbols = new->locals;
	  param_symbols = new->params;
#endif
	  break;

	default:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
          printf("Debug %22.22s %s\n", "- ??", name);
#endif
	  complain (&hpread_unexpected_end_complaint);
	  break;
	}
      break;

      /* DNTT_TYPE_IMPORT is not handled */

    case DNTT_TYPE_LABEL:
      /* srikanth, looks like a leak */
      sym = get_symbol(objfile, name);
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_LABEL", name);
#endif
      SYMBOL_NAMESPACE (sym) = LABEL_NAMESPACE;
      break;

    case DNTT_TYPE_FPARAM:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_FPARAM", name);
#endif
      /* Function parameters.  */
      /* Note 1: This code was present in the 4.16 sources, and then
         removed, because fparams are handled in
         hpread_read_function_type().  However, while fparam symbols
         are indeed handled twice, this code here cannot be removed
         because then they don't get added to the local symbol list of
         the function's code block, which leads to a failure to look
         up locals, "this"-relative member names, etc.  So I've put
         this code back in. pai/1997-07-21 */
      /* Note 2: To fix a defect, we stopped adding FPARAMS to local_symbols
         in hpread_read_function_type(), so FPARAMS had to be handled
         here.  I changed the location to be the appropriate argument
         kinds rather than LOC_LOCAL. pai/1997-08-08 */
      /* Note 3: Well, the fix in Note 2 above broke argument printing
         in traceback frames, and further it makes assumptions about the
         order of the FPARAM entries from HP compilers (cc and aCC in particular
         generate them in reverse orders -- fixing one breaks for the other).
         So I've added code in hpread_read_function_type() to add fparams
         to a param_symbols list for the current context level.  These are
         then merged into local_symbols when a function end is reached.
         pai/1997-08-11 */

      break;			/* do nothing; handled in hpread_read_function_type() */

#if 0				/* Old code */
      if (dn_bufp->dfparam.regparam)
	SYMBOL_CLASS (sym) = LOC_REGISTER;
      else if (dn_bufp->dfparam.indirect)
	SYMBOL_CLASS (sym) = LOC_REF_ARG;
      else
	SYMBOL_CLASS (sym) = LOC_ARG;
      SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
      if (dn_bufp->dfparam.copyparam)
	{
	  SYMBOL_VALUE (sym) = dn_bufp->dfparam.location;
#ifdef HPREAD_ADJUST_STACK_ADDRESS
	  SYMBOL_VALUE (sym)
	    += HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
	}
      else
	SYMBOL_VALUE (sym) = dn_bufp->dfparam.location;
      SYMBOL_TYPE (sym) = hpread_type_lookup (dn_bufp->dfparam.type, objfile);
      add_symbol_to_list (sym, &fparam_symbols);
      break;
#endif

    case DNTT_TYPE_SVAR:
      sym = get_symbol(objfile, name);
      SYMBOL_LANGUAGE (sym) = current_symtab_language;
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_SVAR", name);
#endif
      /* Static variables.  */
#ifdef GDB_TARGET_IS_HPPA_20W
      SYMBOL_CLASS (sym) = LOC_STATIC;
#else
      SYMBOL_CLASS (sym) = dn_bufp->dsvar.indirect ? LOC_INDIRECT : LOC_STATIC;
#endif

      /* Note: There is a case that arises with globals in shared
       * libraries where we need to set the address to LOC_INDIRECT.
       * This case is if you have a global "g" in one library, and
       * it is referenced "extern <type> g;" in another library.
       * If we're processing the symbols for the referencing library,
       * we'll see a global "g", but in this case the address given
       * in the symbol table contains a pointer to the real "g".
       * We use the storage class LOC_INDIRECT to indicate this. RT
       */

      /* RM: Unfortunately, we can't rely on the address in the debug
       * information -- this may either be the address of the DLT slot
       * for the variable, or the address of the variable itself. It's
       * unintuitive, but a variable defined in a shared library may
       * still be in that shared libraries import list. So we must get
       * the DLT address directly, we figured out what it was when
       * initializing the import list (in somread.c).
       */
      if ((import_entry = find_import_entry(SYMBOL_NAME(sym), objfile)))
        {
          SYMBOL_CLASS (sym) = LOC_INDIRECT;
          SYMBOL_VALUE_ADDRESS (sym) = import_entry->addr.dlt_addr;
        }
      else
        {
          /* Fortran common variables provide the base address of the
             common block in the location field.  We need to add the
             offset field in the dsvar to get the correct address.
             */
          /* srikanth, CLLbs15725, 980909 data_offset has to be taken
           * into consideration or else variables from shlibs will be
           * accessed incorrectly ...
           */
          SYMBOL_VALUE_ADDRESS (sym) = dn_bufp->dsvar.location + 
                                       dn_bufp->dsvar.offset + 
                                       data_offset;
        }
      

#ifdef GDB_TARGET_IS_HPPA_20W 
      if (   (sym_in_import_list = is_in_import_list (SYMBOL_NAME(sym), 
                                                      objfile))
          || dn_bufp->dsvar.indirect)
        {
          /* If the indirect bit is on, the debug information has the
             offset from DP of a word that will hold the address.  
             We set the value of the symbol to the address of the word
             and set the class to LOC_INDIRECT.
             */
          /* RM: If we are expanding a DOOM psymtab, then either the
           * location of the variable is its actual address, or it is
           * zero if we couldn't find a minimal symbol for it. If it
           * is zero, we'll have to look it up at runtime using
           * __d_shl_get()
           */
          if (expanding_doom_psymtab)
            ptr_addr = 0;
          else if (processing_common_block)
            {
              ptr_addr = dn_bufp->dsvar.location
                          + objfile->saved_dp_reg;
              SYMBOL_VAR_OFFSET (sym) = dn_bufp->dsvar.offset;
              
            }
          else
            ptr_addr =   dn_bufp->dsvar.location
                          + dn_bufp->dsvar.offset
                          + objfile->saved_dp_reg;
          indirect_msym = lookup_minimal_symbol (SYMBOL_NAME(sym), 
                                                 NULL, 
                                                 objfile);
          if (!indirect_msym || sym_in_import_list )
            {
              /* We can only examine the variable when we are running. 
                 Its type  has already been set to LOC_INDIRECT. 
                 */
              SYMBOL_CLASS (sym) = LOC_INDIRECT;
              SYMBOL_VALUE_ADDRESS (sym) = ptr_addr;
            }
          else
            {
              /* A thread local storage is accessed by first loading an
                 offset from a dp-relative offset, then adding the offset
                 to the tp register (CR27). It's not indirectly referenced
                 through indirect_msym. SYMBOL_VALUE(sym) already
                 contains a dp-relative offset.
              */
              if (!dn_bufp->dsvar.thread_specific)
              {
                /* We can determine the address of the variable now.
                   Leave the type as LOC_STATIC. */
  
                value_addr = SYMBOL_VALUE_ADDRESS(indirect_msym);
                SYMBOL_VALUE_ADDRESS (sym) = value_addr;
              }
            }
        }
      else if (objfile->is_archive_bound)
        {
          /* For historical reasons, dsvar.location for an archive 
             bound program has the actual address of the variable, but
             for a shared bound program it is an offset from the start of
             data.  We mark each object file as it is created to say
             whether it is archive bound.
             */

          SYMBOL_VALUE_ADDRESS (sym) = dn_bufp->dsvar.location + 
                                       dn_bufp->dsvar.offset;
        }
      else
        {
          SYMBOL_VALUE_ADDRESS (sym) = dn_bufp->dsvar.location + 
                                       dn_bufp->dsvar.offset +
                                       data_base_address;
        }
#endif

      if (dn_bufp->dsvar.thread_specific)
        {
          /* Thread-local variable.
           */
          SYMBOL_CLASS (sym) = LOC_THREAD_LOCAL_STATIC;
          SYMBOL_BASEREG (sym) = CR27_REGNUM;

	  /* poorva- JAGac86391 - Thread-locals in shared libraries 
	     do NOT have the standard offset ("data_offset"), we 
	     need to indirect from the address stored in the DLT slot.
	     This gives us the offset from the thread pointer. This 
	     offset can then be added to the value obtained from
	     register CR27 which is the value of the thread pointer.
	     The import_entry has been obtained above and we can obtain
	     the dlt_slot_address from it. */

#ifndef GDB_TARGET_IS_HPPA_20W
	  if (import_entry)
	    SYMBOL_VALUE_ADDRESS (sym) = *(int *)(import_entry->addr.dlt_addr);
#else
	  if (import_entry)
	    SYMBOL_VALUE_ADDRESS (sym) = *(int *)(import_entry->addr.dlt_addr);
#endif
	}
       

      SYMBOL_TYPE (sym) = hpread_type_lookup (dn_bufp->dsvar.type, objfile);

#ifdef HPPA_DOC
       if (dn_bufp->dsvar.doc_ranges)
         make_range_list(dn_bufp->dsvar.location, sym, objfile);
#endif

      if (dn_bufp->dsvar.global)
	add_symbol_to_list (sym, &global_symbols);
      else if (WITHIN_FUNCTION (objfile))
	add_symbol_to_list (sym, &local_symbols);
      else
	add_symbol_to_list (sym, &file_symbols);

      break;

    case DNTT_TYPE_DVAR:
      sym = get_symbol(objfile, name);
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_DVAR", name);
#endif
      /* Dynamic variables.  */
      if (dn_bufp->ddvar.regvar)
	SYMBOL_CLASS (sym) = LOC_REGISTER;
      else
	SYMBOL_CLASS (sym) = LOC_LOCAL;

      SYMBOL_VALUE (sym) = dn_bufp->ddvar.location;
#ifdef HPREAD_ADJUST_STACK_ADDRESS
      SYMBOL_VALUE (sym)
	+= HPREAD_ADJUST_STACK_ADDRESS (CURRENT_FUNCTION_VALUE (objfile));
#endif
      if (dn_bufp->ddvar.indirect)
        SYMBOL_TYPE (sym) = make_reference_type (
          hpread_type_lookup (dn_bufp->ddvar.type, objfile), NULL);
      else
	SYMBOL_TYPE (sym) = hpread_type_lookup (dn_bufp->ddvar.type, objfile);

#ifdef HPPA_DOC
       if (dn_bufp->ddvar.doc_ranges)
         make_range_list(dn_bufp->ddvar.location, sym, objfile);
#endif

      if (dn_bufp->ddvar.global)
	add_symbol_to_list (sym, &global_symbols);
      else if (WITHIN_FUNCTION (objfile))
	add_symbol_to_list (sym, &local_symbols);
      else
	add_symbol_to_list (sym, &file_symbols);
      break;

    case DNTT_TYPE_CONST:
      sym = get_symbol(objfile, name);
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_CONST", name);
#endif
      SYMBOL_TYPE (sym) = hpread_type_lookup (dn_bufp->dconst.type, objfile);
      /*  constant (C++ or fortran).  */
      if (dn_bufp->dconst.location_type ==  LOCATION_IMMEDIATE) {
        SYMBOL_CLASS (sym) = LOC_CONST;
        /* C++ and f77 put the value in the location field, but f90 puts
         * it in the offset field; the value of one or the other is zero,
         * so it shouldn't matter if we add these values
         */
        SYMBOL_VALUE (sym) = dn_bufp->dconst.location + dn_bufp->dconst.offset;
      }
      else if (dn_bufp->dconst.location_type == LOCATION_PTR) {
        SYMBOL_CLASS (sym) = LOC_STATIC;
        /* treat these constants as variables and gdb will handle the rest */
        SYMBOL_VALUE_ADDRESS (sym) = dn_bufp->dconst.location +
                                     dn_bufp->dconst.offset;
#ifdef GDB_TARGET_IS_HPPA_20W
        if (dn_bufp->dconst.indirect) {
          SYMBOL_VALUE_ADDRESS (sym) += objfile->saved_dp_reg;
          SYMBOL_CLASS (sym) = LOC_INDIRECT;
        }
        else {
          SYMBOL_VALUE_ADDRESS (sym) += text_base_address;
        }
#endif
      }
      else if (dn_bufp->dconst.location_type == LOCATION_VT) {
        SYMBOL_VALUE_BYTES (sym) = VT (objfile) + dn_bufp->dconst.location;
        SYMBOL_CLASS (sym) = LOC_CONST_BYTES;
      }
      else {
        warning ("unsupported constant type (%d) for %s",
                 dn_bufp->dconst.location_type, SYMBOL_NAME(sym));
      }
      if (dn_bufp->dconst.global)
	add_symbol_to_list (sym, &global_symbols);
      else if (WITHIN_FUNCTION (objfile))
	add_symbol_to_list (sym, &local_symbols);
      else
	add_symbol_to_list (sym, &file_symbols);
      break;

    case DNTT_TYPE_TYPEDEF:
      sym = get_symbol(objfile, name);
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_TYPEDEF", name);
#endif
      /* A typedef. We do want to process these, since a name is
       * added to the namespace for the typedef'ed name.
       */
      SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
      SYMBOL_TYPE (sym) = hpread_type_lookup (dn_bufp->dtype.type, objfile);
      if (dn_bufp->dtype.global)
	add_symbol_to_list (sym, &global_symbols);
      else if (WITHIN_FUNCTION (objfile))
	add_symbol_to_list (sym, &local_symbols);
      else
	add_symbol_to_list (sym, &file_symbols);
      break;

    case DNTT_TYPE_TAGDEF:
      sym = get_symbol(objfile, name);
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_TAGDEF", name);
#endif
      {
	int global = dn_bufp->dtag.global;
	/* Structure, union, enum, template, or class tag definition */
	/* We do want to process these, since a name is
	 * added to the namespace for the tag name (and if C++ class,
	 * for the typename also).
	 */
	SYMBOL_NAMESPACE (sym) = STRUCT_NAMESPACE;

	/* The tag contains in its "type" field a pointer to the
	 * DNTT_TYPE_STRUCT, DNTT_TYPE_UNION, DNTT_TYPE_ENUM, 
	 * DNTT_TYPE_CLASS or DNTT_TYPE_TEMPLATE
	 * record that actually defines the type.
	 */
	SYMBOL_TYPE (sym) = hpread_type_lookup (dn_bufp->dtype.type, objfile);
	if (TYPE_NAME (sym->type) == 0)
          {
	    /* Do not overwrite the type name contructed by hpread_type_lookup
	       routine.*/
	    TYPE_NAME (sym->type) = SYMBOL_NAME (sym);
	    TYPE_TAG_NAME (sym->type) = SYMBOL_NAME (sym);
	  }
	if (dn_bufp->dtag.global)
	  add_symbol_to_list (sym, &global_symbols);
	else if (WITHIN_FUNCTION (objfile))
	  add_symbol_to_list (sym, &local_symbols);
	else
	  add_symbol_to_list (sym, &file_symbols);

      /* RM: We need to do this here too (it's also done in
       * hpread_type_lookup). This is because there may be tagdefs
       * that nobody points to, so they never get resolved in
       * hpread_type_lookup
       */
      /* For classes/structs, we have to set the static member "physnames"
         to point to strings like "Class::Member" */ 
      if (TYPE_CODE (SYMBOL_TYPE(sym)) == TYPE_CODE_STRUCT)
        fix_static_member_physnames (SYMBOL_TYPE(sym), SYMBOL_NAME (sym),
                                     objfile);


	/* If this is a C++ class, then we additionally 
	 * need to define a typedef for the
	 * class type. E.g., so that the name "c" becomes visible as
	 * a type name when the user says "class c { ... }".
	 * In order to figure this out, we need to chase down the "type"
	 * field to get to the DNTT_TYPE_CLASS record. 
	 *
	 * We also add the typename for ENUM. Though this isn't
	 * strictly correct, it is necessary because of the debug info
	 * generated by the aCC compiler, in which we cannot
	 * distinguish between:
	 *   enum e { ... };
	 * and
	 *   typedef enum { ... } e;
	 * I.e., the compiler emits the same debug info for the above
	 * two cases, in both cases "e" appearing as a tagdef.
	 * Therefore go ahead and generate the typename so that
	 * "ptype e" will work in the above cases.
	 *
	 * We also add the typename for TEMPLATE, so as to allow "ptype t"
	 * when "t" is a template name. 
         * Ditto for structs and unions.
         */
	  {
	    struct symbol *newsym;

	    newsym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
						    sizeof (struct symbol));
	    memset (newsym, 0, sizeof (struct symbol));
	    SYMBOL_NAME (newsym) = name;
	    SYMBOL_LANGUAGE (newsym) = language_auto;
	    SYMBOL_NAMESPACE (newsym) = VAR_NAMESPACE;
	    SYMBOL_LINE (newsym) = 0;
	    SYMBOL_VALUE (newsym) = 0;
	    SYMBOL_CLASS (newsym) = LOC_TYPEDEF;
	    SYMBOL_TYPE (newsym) = sym->type;
	    if (global)
	      add_symbol_to_list (newsym, &global_symbols);
	    else if (WITHIN_FUNCTION (objfile))
	      add_symbol_to_list (newsym, &local_symbols);
	    else
	      add_symbol_to_list (newsym, &file_symbols);
	  }
      }
      break;

    case DNTT_TYPE_POINTER:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_POINTER", name);
#endif
      /* Declares a pointer type. Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_ENUM:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_ENUM", name);
#endif
      /* Declares an enum type. */
      /* srikanth 010700, internalize the enum type now ... */
      if (incremental_expansion)
        hpread_read_enum_type (hp_type, dn_bufp, objfile);
      break;

    case DNTT_TYPE_MEMENUM:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_MEMENUM", name);
#endif
      /* Member of enum */
      /* Ignored at this level, but hpread_read_enum_type() will take
       * care of walking the list of enumeration members.
       */
      break;

    case DNTT_TYPE_SET:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_SET", name);
#endif
      /* Declares a set type. Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_SUBRANGE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_SUBRANGE", name);
#endif
      /* Declares a subrange type. Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_ARRAY:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_ARRAY", name);
#endif
      /* Declares an array type. Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_STRUCT:
    case DNTT_TYPE_UNION:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_STRUCT", name);
#endif
      /* Declares an struct/union type. 
       * Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_FIELD:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_FIELD", name);
#endif
      /* Structure/union/class field */
      /* Ignored at this level, but hpread_read_struct_type() will take
       * care of walking the list of structure/union/class members.
       */
      break;

      /* DNTT_TYPE_VARIANT is not handled by GDB */

      /* DNTT_TYPE_FILE is not handled by GDB */

    case DNTT_TYPE_FUNCTYPE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_FUNCTYPE", name);
#endif
      /* Function type */
      /* Ignored at this level, handled within hpread_type_lookup() */
      break;

    case DNTT_TYPE_WITH:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_WITH", name);
#endif
      /* This is emitted within methods to indicate "with <class>" 
       * scoping rules (i.e., indicate that the class data members
       * are directly visible).
       * However, since GDB already infers this by looking at the
       * "this" argument, interpreting the DNTT_TYPE_WITH 
       * symbol record is unnecessary.
       */
      break;

    case DNTT_TYPE_COMMON:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_COMMON", name);
#endif
      common_block_start (name, objfile);
#ifdef GDB_TARGET_IS_HPPA_20W
      processing_common_block = 1;
#endif
      break;

      /* DNTT_TYPE_COBSTRUCT is not handled by GDB.  */
      /* DNTT_TYPE_XREF is not handled by GDB.  */
      /* DNTT_TYPE_SA is not handled by GDB.  */
      /* DNTT_TYPE_MACRO is not handled by GDB */

    case DNTT_TYPE_BLOCKDATA:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_BLOCKDATA", name);
#endif
      /* Not sure what this is - part of FORTRAN support maybe? 
       * Anyway, not yet handled.
       */
      complain (&hpread_unhandled_blockdata_complaint);
      break;

    case DNTT_TYPE_CLASS_SCOPE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_CLASS_SCOPE", name);
#endif

      /* The compiler brackets member functions with a CLASS_SCOPE/END
       * pair of records, presumably to put them in a different scope
       * from the module scope where they are normally defined.
       * E.g., in the situation:
       *   void f() { ... }
       *   void c::f() { ...}
       * The member function "c::f" will be bracketed by a CLASS_SCOPE/END.
       * This causes "break f" at the module level to pick the
       * the file-level function f(), not the member function
       * (which needs to be referenced via "break c::f"). 
       * 
       * Here we record the class name to generate the demangled names of
       * member functions later.
       *
       * FIXME Not being used now for anything -- cplus_demangle seems
       * enough for getting the class-qualified names of functions. We
       * may need this for handling nested classes and types.  */

      /* pai: FIXME Not handling nested classes for now -- need to
       * maintain a stack */

      dn_temp = hpread_get_lntt (INDEX(objfile,dn_bufp->dclass_scope.type.dnttp), objfile);
      if (dn_temp->dblock.kind == DNTT_TYPE_TAGDEF)
	class_scope_name = VT (objfile) + dn_temp->dtag.name;
      else
	class_scope_name = NULL;

#if 0

      /* Begin a new scope.  */
      SL_INDEX (objfile) = hpread_record_lines (current_subfile,
						SL_INDEX (objfile),
					      dn_bufp->dclass_scope.address,
						objfile, offset);
      valu = hpread_get_location (dn_bufp->dclass_scope.address, objfile, offset);
      valu += offset;		/* Relocate for dynamic loading */
      desc = hpread_get_scope_start (dn_bufp->dclass_scope.address, objfile);
      /* We use the scope start DNTT index as the nesting depth identifier! */
      new = push_context (desc, valu);
#endif
      break;

    case DNTT_TYPE_REFERENCE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_REFERENCE", name);
#endif
      /* Declares a C++ reference type. Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level.
       */
      break;

    case DNTT_TYPE_PTRMEM:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_PTRMEM", name);
#endif
      /* Declares a C++ pointer-to-data-member type. This does not
       * need to be handled at this level; being a type description it
       * is instead handled at the hpread_type_lookup() level.
       */
      break;

    case DNTT_TYPE_PTRMEMFUNC:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_PTRMEMFUNC", name);
#endif
      /* Declares a C++ pointer-to-function-member type. This does not
       * need to be handled at this level; being a type description it
       * is instead handled at the hpread_type_lookup() level.
       */
      break;

    case DNTT_TYPE_CLASS:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_CLASS", name);
#endif
      /* Declares a class type. 
       * Should not be necessary to do anything
       * with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_GENFIELD:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_GENFIELD", name);
#endif
      /* I believe this is used for class member functions */
      /* Ignored at this level, but hpread_read_struct_type() will take
       * care of walking the list of class members.
       */
      break;

    case DNTT_TYPE_VFUNC:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_VFUNC", name);
#endif
      /* Virtual function */
      /* This does not have to be handled at this level; handled in
       * the course of processing class symbols.
       */
      break;

    case DNTT_TYPE_MEMACCESS:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_MEMACCESS", name);
#endif
      /* DDE ignores this symbol table record.
       * It has something to do with "modified access" to class members.
       * I'll assume we can safely ignore it too.
       */
      break;

    case DNTT_TYPE_INHERITANCE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_INHERITANCE", name);
#endif
      /* These don't have to be handled here, since they are handled
       * within hpread_read_struct_type() in the process of constructing
       * a class type.
       */
      break;

    case DNTT_TYPE_FRIEND_CLASS:
    case DNTT_TYPE_FRIEND_FUNC:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_FRIEND_FUNC", name);
#endif
      /* These can safely be ignored, as GDB doesn't need this
       * info. DDE only uses it in "describe". We may later want
       * to extend GDB's "ptype" to give this info, but for now
       * it seems safe enough to ignore it.
       */
      break;

    case DNTT_TYPE_MODIFIER:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_MODIFIER", name);
#endif
      /* Intended to supply "modified access" to a type */
      /* From the way DDE handles this, it looks like it always
       * modifies a type. Therefore it is safe to ignore it at this
       * level, and handle it in hpread_type_lookup().
       */
      break;

    case DNTT_TYPE_OBJECT_ID:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_OBJECT_ID", name);
#endif
      /* Just ignore this - that's all DDE does */
      break;

    case DNTT_TYPE_MEMFUNC:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_MEMFUNC", name);
#endif
      /* Member function */
      /* This does not have to be handled at this level; handled in
       * the course of processing class symbols.
       */
      break;

    case DNTT_TYPE_DOC_MEMFUNC:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_DOC_MEMFUNC", name);
#endif
      /* Member function */
      /* This does not have to be handled at this level; handled in
       * the course of processing class symbols.
       */
      break;

    case DNTT_TYPE_TEMPLATE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_TEMPLATE", name);
#endif
      /* Template - sort of the header for a template definition,
       * which like a class, points to a member list and also points
       * to a TEMPLATE_ARG list of type-arguments.
       * We do not need to process TEMPLATE records at this level though.
       */
      break;

    case DNTT_TYPE_TEMPLATE_ARG:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_TEMPLATE_ARG", name);
#endif
      /* The TEMPLATE record points to an argument list of
       * TEMPLATE_ARG records, each of which describes one
       * of the type-arguments.
       * We do not need to process TEMPLATE_ARG records at this level though.
       */
      break;

    case DNTT_TYPE_FUNC_TEMPLATE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_FUNC_TEMPLATE", name);
#endif
      /* This will get emitted for member functions of templates.
       * But we don't need to process this record at this level though,
       * we will process it in the course of processing a TEMPLATE
       * record.
       */
      break;

    case DNTT_TYPE_LINK:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_LINK", name);
#endif
      /* The LINK record is used to link up templates with instantiations. */
      /* It is not clear why this is needed, and furthermore aCC does
       * not appear to generate this, so I think we can safely ignore it. - RT
       */
      break;

    case DNTT_TYPE_DYN_ARRAY_DESC:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_DYN_ARRAY_DESC", name);
#endif
      /* Declares a dynmic array descriptor type. Should not be necessary 
       * to do anything with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

    case DNTT_TYPE_DESC_SUBRANGE:
#ifdef PRINT_DNTT_ENTRY_AS_PRCOESSED
      printf("Debug %22.22s %s\n", "DNTT_TYPE_DESC_SUBRANGE", name);
#endif
      /* Declares a dynamic array subrange type. Should not be necessary
       * to do anything with the type at this level; these are processed
       * at the hpread_type_lookup() level. 
       */
      break;

      /* DNTT_TYPE_BEGIN_EXT is not handled by GDB */
      /* DNTT_TYPE_INLN is not handled by GDB */
      /* DNTT_TYPE_INLN_LIST is not handled by GDB */
      /* DNTT_TYPE_ALIAS is not handled by GDB */

    default:
      break;
    }
}

struct symbol * get_symbol(struct objfile * objfile, char * name)
{
 
  struct symbol * sym;

  /* Allocate one GDB debug symbol and fill in some default values. */
  sym = (struct symbol *) obstack_alloc (&objfile->symbol_obstack,
                                         sizeof (struct symbol));
  memset (sym, 0, sizeof (struct symbol));

  /* srikanth, 990310, JAGaa80452 : symbol names don't materialize
   * out of nowhere. Most are pointers into the value table. The VT
   * lives as long as the objfile lives. Thus there is little to be
   * gained by duplicating, triplicating, quadruplicating if `name'
   * came from the VT in the first place ...
   */  
  if (permanent_copy_exists(objfile, name))
    SYMBOL_NAME (sym) = name;
  else 
    SYMBOL_NAME (sym) = obsavestring (name, strlen (name), &objfile->symbol_obstack);
  SYMBOL_LANGUAGE (sym) = language_auto;
  SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
  SYMBOL_LINE (sym) = 0;
  SYMBOL_VALUE (sym) = 0;
  SYMBOL_CLASS (sym) = LOC_TYPEDEF;

  return sym;
}

/* Adjust the bitoffsets for all fields of an anonymous union of
   type TYPE by negative BITS.  This handles HP aCC's hideous habit
   of giving members of anonymous unions bit offsets relative to the
   enclosing structure instead of relative to the union itself. */

static void
hpread_adjust_bitoffsets (type, bits)
     struct type *type;
     int bits;
{
  register int i;

  /* This is done only for unions; caller had better check that
     it is an anonymous one. */
  if (TYPE_CODE (type) != TYPE_CODE_UNION)
    return;

  /* Adjust each field; since this is a union, there are no base
     classes. Also no static membes.  Also, no need for recursion as
     the members of this union if themeselves structs or unions, have
     the correct bitoffsets; if an anonymous union is a member of this
     anonymous union, the code in hpread_read_struct_type() will
     adjust for that. */

  for (i = 0; i < TYPE_NFIELDS (type); i++)
    TYPE_FIELD_BITPOS (type, i) -= bits;
}

/* Because of quirks in HP compilers' treatment of anonymous unions inside
   classes, we have to chase through a chain of threaded FIELD entries.
   If we encounter an anonymous union in the chain, we must recursively skip over
   that too.

   This function does a "next" in the chain of FIELD entries, but transparently
   skips over anonymous unions' fields (recursively).

   Inputs are the number of times to do "next" at the top level, the dnttpointer
   (FIELD) and entry pointer (FIELDP) for the dntt record corresponding to it,
   and the ubiquitous objfile parameter. (Note: FIELDP is a **.)  Return value
   is a dnttpointer for the new field after all the skipped ones */

static dnttpointer
hpread_get_next_skip_over_anon_unions (skip_fields, field, fieldp, objfile)
     int skip_fields;
     dnttpointer field;
     union dnttentry **fieldp;
     struct objfile *objfile;
{
  struct type *anon_type;
  register int i;
  int bitoffset;
  char *name;

  for (i = 0; i < skip_fields; i++)
    {
      /* Get type of item we're looking at now; recursively processes the types
         of these intermediate items we skip over, so they aren't lost. */
      anon_type = hpread_type_lookup ((*fieldp)->dfield.type, objfile);
      anon_type = CHECK_TYPEDEF (anon_type);
      bitoffset = (*fieldp)->dfield.bitoffset;
      name = VT (objfile) + (*fieldp)->dfield.name;
      /* First skip over one item to avoid stack death on recursion */
      field = (*fieldp)->dfield.nextfield;
      *fieldp = hpread_get_lntt (INDEX(objfile,field.dnttp), objfile);
      /* Do we have another anonymous union? If so, adjust the bitoffsets
         of its members and skip over its members. */
      if ((TYPE_CODE (anon_type) == TYPE_CODE_UNION) &&
	  (!name || STREQ (name, "")))
	{
	  hpread_adjust_bitoffsets (anon_type, bitoffset);
	  field = hpread_get_next_skip_over_anon_unions (TYPE_NFIELDS (anon_type), field, fieldp, objfile);
	}
    }
  return field;
}
