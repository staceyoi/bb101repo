/* hpread.h
 * Common include file for:
 *   hp_symtab_read.c
 *   hp_psymtab_read.c
 */

/* Copyright 1993, 1996 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Written by the Center for Software Science at the University of Utah
   and by Cygnus Support.  */

#include "defs.h"
#include "bfd.h"
#include "gdb_string.h"
#ifdef BFD64
#include "hp-symtab64.h"
#else 
#include "hp-symtab.h"
#endif
#include "syms.h"
#include "symtab.h"
#include "symfile.h"
#include "objfiles.h"
#include "buildsym.h"
#include "complaints.h"
#include "gdb-stabs.h"
#include "gdbtypes.h"
#include "demangle.h"

#ifndef BFD64 
#define build_doom_tables somread_build_doom_tables
#define dump_relocations somread_dump_relocations
#define relocated_section_size somread_relocated_section_size
#define get_relocated_section_contents \
        somread_get_relocated_section_contents
#else
#define build_doom_tables elfread_build_doom_tables
#define dump_relocations elfread_dump_relocations
#define relocated_section_size elfread_relocated_section_size
#define get_relocated_section_contents \
        elfread_get_relocated_section_contents
#endif

/* RM: DOOM specific structs */
struct cr
{
  unsigned int objname; /* pointer into strings table */
  unsigned int srcname; /* pointer into strings table */
  unsigned int dirname; /* pointer into strings table */
  enum language lang;    /* pointer into strings table */
};

struct comp_map
{
  unsigned int max_cr;
  unsigned int n_cr;
  struct cr cr[1];    /* actually n_cr entries */
};

struct sr
{
  unsigned int spacename;    /* pointer into strings table   */
  unsigned int subspacename; /* pointer into strings table   */
  int objid;                 /* pointer into compilation map */
  int real_id;
  CORE_ADDR start;
  CORE_ADDR end;
  int info;                  /* additional field for internal use by
                              * various algorithms */
};

struct subspace_map
{
  unsigned int max_sr;
  unsigned int n_sr;
  int textstart;
  int textend;
  int datastart;
  int dataend;
  int debugstart;
  int debugend;
  struct sr sr[1];    /* actually n_sr entries */
};

struct typetable_map
{
  char *table;
  size_t table_size;
  int subspacemap_base;
};

struct consttable_map
{
  char *table;
  size_t table_size;
  int subspacemap_base;
};


/* srikanth, 990422, demand paging of SLT...

   A paging scheme to load on demand selected parts of SLT and cache the 
   contents for future references. Under this scheme, there could be upto
   SLT_PAGE_COUNT number of pages per load module and each could hold upto
   SLT_ENTRIES_PER_PAGE number of entries. 

   When we have allocated SLT_PAGE_COUNT number of pages, we reclaim a page
   frame by discarding its cached contents. Eviction is on a "least recently 
   used" basis. 

   Parameters for this paging scheme were chosen such that each page would be 
   8 kilo bytes in size (1024 entries * 8 bytes per entry) and there would be 
   an upper bound of ~50 kilo bytes (6 pages) of SLT space per load module.

   While this may look small, see that more pages is not necessarily better 
   since the SLT is consulted only during the expansion of a function. By its
   very nature, accesses to SLT are highly serialized, localized and in bursts.
   Further more, once gdb metabolizes the contents of an SLT page, all further
   lookups are to the internalized form. Thus having served its life's mission 
   a page begins languishing in memory and hence is a good candidate for 
   replacement.

   All allocation is from the symbol_obstack.
*/

# define SLT_PAGE_COUNT  6
#define SLT_ENTRIES_PER_PAGE      1024      /* this must be a power of two */
#define SLT_PAGE_NUMBER(index)    ((index) >> 10)
#define SLT_ENTRY_IN_PAGE(index)  ((index) &  0x000003ff)

typedef struct slt_page_frame {
      int page_number;                     
      union sltentry * page;    /* actual goodies */
} slt_page_frame; 

/* srikanth, 000121, demand paging of LNTT ...

   A paging scheme to load on demand selected parts of LNTT and
   cache the contents for future references. Under this scheme, there
   could be upto LNTT_PAGE_COUNT number of pages per load module and
   each could hold upto LNTT_ENTRIES_PER_PAGE number of entries.

   When we have allocated LNTT_PAGE_COUNT number of pages, we reclaim
   a page frame by discarding its cached contents. Eviction is on a
   "least recently used" basis.

   Parameters for this paging scheme were chosen such that each page
   would be 12 kilo bytes in size (1024 entries * 12 bytes per entry)
   (16 kb for PA64) and there would be an upper bound of ~100 kilo bytes
   (8 pages) of LNTT space per load module.

   While this may look small, see that more pages is not necessarily
   better since the LNTT is consulted only during the expansion of a
   translation unit. By its very nature, accesses to LNTT are highly
   serialized, localized and in bursts. Further more, once gdb
   metabolizes the contents of an LNTT page, all further lookups are
   to the internalized form. Thus having served its life's mission
   a page begins languishing in memory and hence is a good candidate
   for replacement.

   All allocation is from the symbol_obstack.
 */

#define LNTT_PAGE_COUNT  8
#define LNTT_ENTRIES_PER_PAGE      1024   /* must be a power of two */
#define LNTT_PAGE_NUMBER(index)    ((index) >> 10)
#define LNTT_ENTRY_IN_PAGE(index)  ((index) &  0x000003ff)

typedef struct lntt_page_frame
  {
    int page_number;
    struct dntt_type_block *page;	/* actual goodies */
  }
lntt_page_frame;

struct Hashed_minsymtab_t;

/* Private information attached to an objfile which we use to find
   and internalize the HP C debug symbols within that objfile.  */

struct hpread_symfile_info
  {
    /* The contents of each of the debug sections (there are 4 of them).  */
    /* RM: Each incrementally linked module contributes a new set of
       debug sections, hence each of the following is an array now.
       JYG: MERGE NOTE: except for entries for incremental expansion
       and/or demand paging, which we don't use for ild mode support. */
    char **gntt;
    char *real_gntt;	/* not for ild */
    char **lntt;
    char **slt;
    char **vt;
#ifdef HPPA_DOC
    char **rt;	/* not for ild but vectorized anyway */
    unsigned int *rt_size;	/* not for ild but vectorized anyway */
#endif /* HPPA_DOC */
    char **lines;	/* ??? not for ild? vectorized anyway */
    unsigned int *globals_start;
    unsigned int real_gntt_start;	/* not for ild */

    lntt_page_frame * lntt_page_table;	/* not for ild */
    asection *lntt_section;	/* not for ild */
    int lntt_size;	/* not for ild */

    slt_page_frame * slt_page_table;	/* not for ild */
    asection * slt_section;	/* not for ild */
    int *slt_size;

    /* We keep the size of the $VT$ section for range checking.  */
    unsigned int *vt_size;

    /* Size of the HP_LINES section */
    unsigned int *lines_size;	/* ??? not for ild? vectorized anyway */

    /* Some routines still need to know the number of symbols in the
       main debug sections ($LNTT$ and $GNTT$). */
    unsigned int *lntt_symcount;
    unsigned int *gntt_symcount;
    unsigned int real_gntt_symcount;	/* not for ild */

    /* To keep track of all the types we've processed.  */
    struct type ***type_vector;
    int *type_vector_length;

    /* Keeps track of the beginning of a range of source lines.  */
    sltpointer *sl_index;

    /* Some state variables we'll need.  */
    int within_function;

    /* Keep track of the current function's address.  We may need to look
       up something based on this address.  */
    CORE_ADDR current_function_value;

    /* for ild support */
    int n_debug_chapters;
    int current_debug_chapter;
    
    /* RM: DOOM specific tables */
    struct comp_map *comp_map;
    struct subspace_map *subspace_map;
    struct typetable_map type_table;
    struct consttable_map const_table;
    char *strings;
    struct Hashed_minsymtab_t *ht;
  };

/* subspace/section names containing HP debug information */
/* Q: what about $DEBUG$ and $PINFO$ ? */
#ifdef BFD64
# define HP_HEADER ".debug_header"
# define HP_GNTT   ".debug_gntt"
# define HP_LNTT   ".debug_lntt"
# define HP_SLT    ".debug_slt"
# define HP_VT     ".debug_vt"
#ifdef HPPA_DOC
#define HP_RT     ".debug_range"
#endif /* HPPA_DOC */
#define HP_LINES  ".debug_lines"
#else
# define HP_HEADER "$HEADER$"
# define HP_GNTT   "$GNTT$"
# define HP_LNTT   "$LNTT$"
# define HP_SLT    "$SLT$"
# define HP_VT     "$VT$"
#ifdef HPPA_DOC
#define HP_RT     "$RANGE$"
#endif /* HPPA_DOC */
#define HP_LINES  "$LINES$"
#endif

/* Accessor macros to get at the fields.  */
#define HPUX_SYMFILE_INFO(o) \
  ((struct hpread_symfile_info *)((o)->sym_private))
#define CURRENT_CHAPTER(o)      (HPUX_SYMFILE_INFO(o)->current_debug_chapter)
#define N_CHAPTERS(o)           (HPUX_SYMFILE_INFO(o)->n_debug_chapters)
#define GNTT(o)                 (HPUX_SYMFILE_INFO(o)->gntt[CURRENT_CHAPTER(o)])
#define REAL_GNTT(o)            (HPUX_SYMFILE_INFO(o)->real_gntt)
#define LNTT(o)                 (HPUX_SYMFILE_INFO(o)->lntt[CURRENT_CHAPTER(o)])
#define SLT(o)                  (HPUX_SYMFILE_INFO(o)->slt[CURRENT_CHAPTER(o)])
#define VT(o)                   (HPUX_SYMFILE_INFO(o)->vt[CURRENT_CHAPTER(o)])
#ifdef HPPA_DOC
#define RT(o)                   (HPUX_SYMFILE_INFO(o)->rt[CURRENT_CHAPTER(o)])
#define RT_SIZE(o)              (HPUX_SYMFILE_INFO(o)->rt_size[CURRENT_CHAPTER(o)])
#endif /* HPPA_DOC */
#define VT_SIZE(o)              (HPUX_SYMFILE_INFO(o)->vt_size[CURRENT_CHAPTER(o)])
#define LINES(o)                (HPUX_SYMFILE_INFO(o)->lines[CURRENT_CHAPTER(o)])
#define LINES_SIZE(o)           (HPUX_SYMFILE_INFO(o)->lines_size[CURRENT_CHAPTER(o)])
#define REAL_GNTT_START(o)      (HPUX_SYMFILE_INFO(o)->real_gntt_start)
#define REAL_GNTT_SYMCOUNT(o)   (HPUX_SYMFILE_INFO(o)->real_gntt_symcount)
#define LNTT_SYMCOUNT(o)        (HPUX_SYMFILE_INFO(o)->lntt_symcount[CURRENT_CHAPTER(o)])
#define GNTT_SYMCOUNT(o)        (HPUX_SYMFILE_INFO(o)->gntt_symcount[CURRENT_CHAPTER(o)])
#define GLOBALS_START(o)        (HPUX_SYMFILE_INFO(o)->globals_start[CURRENT_CHAPTER(o)])
#define TYPE_VECTOR(o)          (HPUX_SYMFILE_INFO(o)->type_vector[CURRENT_CHAPTER(o)])
#define TYPE_VECTOR_LENGTH(o)   (HPUX_SYMFILE_INFO(o)->type_vector_length[CURRENT_CHAPTER(o)])
#define SL_INDEX(o)             (HPUX_SYMFILE_INFO(o)->sl_index[CURRENT_CHAPTER(o)])
#define WITHIN_FUNCTION(o)      (HPUX_SYMFILE_INFO(o)->within_function)
#define CURRENT_FUNCTION_VALUE(o) (HPUX_SYMFILE_INFO(o)->current_function_value)
#define COMPMAP(o)              (HPUX_SYMFILE_INFO(o)->comp_map)
#define SUBSPACEMAP(o)          (HPUX_SYMFILE_INFO(o)->subspace_map)
#define STRINGS(o)              (HPUX_SYMFILE_INFO(o)->strings)
#define TYPETABLEMAP(o)          (HPUX_SYMFILE_INFO(o)->type_table)
#define CONSTTABLEMAP(o)          (HPUX_SYMFILE_INFO(o)->const_table)
#define SYMFILE_HT(o)          (HPUX_SYMFILE_INFO(o)->ht)
#define LNTT_PAGE_TABLE(o)      (HPUX_SYMFILE_INFO(o)->lntt_page_table)
#define LNTT_SECTION(o)         (HPUX_SYMFILE_INFO(o)->lntt_section)
#define LNTT_SIZE(o)            (HPUX_SYMFILE_INFO(o)->lntt_size)
#define SLT_PAGE_TABLE(o)       (HPUX_SYMFILE_INFO(o)->slt_page_table)
#define SLT_SECTION(o)          (HPUX_SYMFILE_INFO(o)->slt_section)
#define SLT_SIZE(o)             (HPUX_SYMFILE_INFO(o)->slt_size[CURRENT_CHAPTER(o)])

/* For DOOM, we combine the GNTT and LNTT into one large table. But we
 *  need to fixup GNTT indices before using them
 */

#define INDEX(o, dp)  (dp.global  ?                                     \
                       GLOBALS_START(o) + dp.index :                    \
                       dp.index)

/* Given the native debug symbol SYM, set NAMEP to the name associated
   with the debug symbol.  Note we may be called with a debug symbol which
   has no associated name, in that case we return an empty string.

   Also note we "know" that the name for any symbol is always in the
   same place.  Hence we don't have to conditionalize on the symbol type.  */
#define SET_NAMESTRING(SYM, NAMEP, OBJFILE) \
  if (! hpread_has_name ((SYM)->dblock.kind)) \
    *NAMEP = ""; \
  else if (((unsigned)(SYM)->dsfile.name) >= VT_SIZE (OBJFILE)) \
    { \
      complain (&string_table_offset_complaint, (char *) symnum); \
      *NAMEP = ""; \
    } \
  else \
    *NAMEP = (SYM)->dsfile.name + VT (OBJFILE)


/* Status of the debug information in an object file with DOOM */
enum Doom_ofile_load_status {
  DOOM_OFILE_PRELOADED,           /* Preloaded in executable */
  DOOM_OFILE_DO_NOT_DEMAND_LOAD,  /* Not loaded, do not demand load */
  DOOM_OFILE_DO_DEMAND_LOAD,      /* Not loaded, load on demand */
  DOOM_OFILE_LOADED_WITH_DEBUG,   /* Loaded with debug info */
  DOOM_OFILE_LOADED_NO_DEBUG,     /* Loaded and no debug info found */
  DOOM_OFILE_OPEN_ERROR           /* Could not open file */
};

/* We put a pointer to this structure in the read_symtab_private field
   of the psymtab.  */

struct symloc
  {
    union where
      {
	/* private info for non DOOM psymtabs */
	struct classic
          {
	    /* The offset within the file symbol table of first local
	       symbol for this file.  */

	    int ldsymoff;

	    /* Length (in bytes) of the section of the symbol table devoted to
	       this file's symbols (actually, the section bracketed may contain
	       more than just this file's symbols).  If ldsymlen is 0, the only
	       reason for this thing's existence is the dependency list.
	       Nothing else will happen when it is read in.  */

	    int ldsymlen;

            /* RM: Which of the debug chapters is this symtab in */
            int chapter;
	} classic;
	
        struct
	{
	  unsigned int objid;
	} doom;
    } where;
  
    /* RM: Has this symtab been overridden by an incrementally linked
       module? */
    int overridden;
    enum Doom_ofile_load_status ofile_load_status;
    int is_default;
  };

#define LDSYMOFF(p) (((struct symloc *)((p)->read_symtab_private))->where.classic.ldsymoff)
#define LDSYMLEN(p) (((struct symloc *)((p)->read_symtab_private))->where.classic.ldsymlen)
#define LDCHAPTER(p) (((struct symloc *)((p)->read_symtab_private))->where.classic.chapter)
#define OVERRIDDEN(p) (((struct symloc *)((p)->read_symtab_private))->overridden)
#define OBJID(p) (((struct symloc *)((p)->read_symtab_private))->where.doom.objid)
#define DEFAULT_PSYMTAB(p) (((struct symloc *)((p)->read_symtab_private))->is_default)
#define OFILE_LOAD_STATUS(p) (((struct symloc *)((p)->read_symtab_private))->ofile_load_status)
#define OFILE_LOAD_STATUS_IS_RETRYABLE(p) \
  ((OFILE_LOAD_STATUS(p) == DOOM_OFILE_LOADED_NO_DEBUG) || \
  (OFILE_LOAD_STATUS(p) == DOOM_OFILE_OPEN_ERROR))

#define SYMLOC(p) ((struct symloc *)((p)->read_symtab_private))

/* FIXME: Shouldn't this stuff be in a .h file somewhere?  */
/* Nonzero means give verbose info on gdb action.  */
extern int info_verbose;

/* Complaints about the symbols we have encountered.  */
extern struct complaint string_table_offset_complaint;
extern struct complaint lbrac_unmatched_complaint;
extern struct complaint lbrac_mismatch_complaint;

/* The SLT is demand paged. This means that 

   (a) you cannot do arithmetic on the return value of hpread_get_slt()
       (as of 000122 it has been mechanically verified that we don't do
       this.)
   (b) you cannot store it for future use. It might evaporate.
   (c) you cannot write onto SLT. Currently only doom mode needs to do this.
       But demand paging is not applicable for SLT's from doomed .o files.
*/

extern union sltentry *hpread_get_slt (int, struct objfile *);

extern size_t hpread_get_slt_size (struct objfile *);

extern dst_ln_entry_t *hpread_get_lines (int, struct objfile *);

/* The LNTT is demand paged. This means that

   (a) you cannot do arithmetic on the return value of hpread_get_lntt()
       (as of 000122 it has been mechanically verified that we don't do
       this.)
   (b) you cannot store it for future use. It might evaporate.
   (c) you cannot write onto LNTT.
*/

extern union dnttentry *hpread_get_lntt (int, struct objfile *);

#ifdef HPPA_DOC
extern struct range *hpread_get_rt (int, struct objfile *);
#endif

int hpread_has_name (enum dntt_entry_type);

enum spaces
{
  unknownspace,
  textspace,
  dataspace,
  debugspace
};

struct minimal_symbol *
hpread_lookup_minimal_symbol_within_object (char *, int, struct objfile *);

int hpread_find_objid (struct objfile *, CORE_ADDR, enum spaces);
/* end of hpread.h */
