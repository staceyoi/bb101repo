/* Memory-access and commands for "inferior" process, for GDB.
   Copyright 1986, 87, 88, 89, 91, 92, 95, 96, 1998, 1999
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "top.h"
#include <signal.h>
#include "gdb_string.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "frame.h"
#include "inferior.h"
#include "environ.h"
#include "value.h"
#include "gdbcmd.h"
#include "gdbcore.h"
#include "target.h"
#include "language.h"
#include "symfile.h"
#include "objfiles.h"
#include "thread.h"
#include <dl.h>
#include <sys/utsname.h>
#ifdef UI_OUT
#include "ui-out.h"
#endif
#include "event-top.h"
#include "parser-defs.h"
#ifdef LOG_BETA_RUN_COMMAND_TIME
#include <time.h>
#endif
#ifdef RTC
#include "rtc.h"
#endif

/* Baskar, JAGae40701 */
#include <sys/pstat.h>
#include <sys/stat.h>
#include "annotate.h"

#define DEBUG(x)


#ifdef HP_XMODE
extern int xmode_this_gdb_understands;
extern void xmode_launch_64bit_gdb (char **argv, char *cause);
extern void xmode_launch_32bit_gdb (char **argv, char *cause);
extern int xmode_exec_format_is_different (char *);
extern void xmode_launch_IA_gdb (char **argv, char *cause);
extern enum exec_format xmode_exec_format;
#endif  /* HP_XMODE */
extern boolean sigtramp_stepi_again;
extern CORE_ADDR dummy_break_addr;
/* From breakpoint.c */
extern void invalidate_shadow_contents (void);
extern struct breakpoint * create_rantomain_event_breakpoint (int);

/* From infttrace.c */
extern int sigwait_sig_in_init;

/* From blockframe.c */
extern boolean set_next_inline_frame (void);

extern int is_IA_machine; /* JAGae35325 */
extern int is_steplast_eligible;
/* Functions exported for general use: */

void nofp_registers_info (char *, int);

void all_registers_info (char *, int);

void registers_info (char *, int);

/* Local functions: */

void continue_command (char *, int);

static void print_return_value (int struct_return, struct type *value_type);

static void finish_command_continuation (struct continuation_arg *);

static void until_next_command (int);

static void until_command (char *, int);

static void path_info (char *, int);

static void path_command (char *, int);

static void unset_command (char *, int);

static void float_info (char *, int);

static void detach_command (char *, int);

static void interrupt_target_command (char *args, int from_tty);

#if !defined (DO_REGISTERS_INFO)
static void do_registers_info (int, int);
#endif

static void unset_environment_command (char *, int);

static void set_environment_command (char *, int);

static void environment_info (char *, int);

static void program_info (char *, int);

static void finish_command (char *, int);

static void signal_command (char *, int);

static void jump_command (char *, int);

static void step_once (int skip_subroutines, int single_inst, int count);
static void step_1_continuation (struct continuation_arg *arg);

void nexti_command (char *, int);

void stepi_command (char *, int);

static void next_command (char *, int);

static void step_command (char *, int);

static void steplast_command (char *, int);

static void run_command (char *, int);

static void run_no_args_command (char *args, int from_tty);

static void go_command (char *line_no, int from_tty);

static int strip_bg_char (char **);

extern void refresh_display_chain(void);
#ifdef RTC
void setup_librtc_env();
#endif

#ifdef HPPA_FIX_AND_CONTINUE
static void fix_command (char *, int);

extern void check_unapplied_changes ();
#endif

void _initialize_infcmd (void);

#define GO_USAGE   "Usage: go <location>\n"

static void breakpoint_auto_delete_contents (PTR);

#ifdef GDB_TARGET_IS_HPUX
extern boolean inferior_is_live (void);
#define ERROR_NO_INFERIOR \
{ \
if (!target_has_execution) error ("The program is not being run."); \
if (!inferior_is_live()) { \
	inferior_pid = 0; \
	error ("The program may be killed"); \
	} \
}
#else
#define ERROR_NO_INFERIOR \
if (!target_has_execution) error ("The program is not being run.");
#endif

#ifdef HP_IA64
#include "mixed_mode.h"
#endif

/* RM: boolean indicating whether gdb should complain when SIGTRAP is
   disabled. */
int complain_if_sigtrap_disabled;

/* srikanth, 2002-04-30, should signals that are to be silently passed to the
   program be routed through the debugger ? This boolean controls it. The 
   default is to not involve gdb in the signal delivery. This is done to
   resolve stepping problems at Intel : their application generates SIGALRM
   signals very frequently. This interferes with stepping and gdb gets stuck
   and complains about "Internal error : changing step to continue".

   I am introducing this as an escape hatch as I am reversing the current
   default and the current default is supposed to be that way to work around
   kernel defects. However, the observed behavior is that gdb behaves much
   better with the new default.
*/
int ignore_uninteresting_signals = 1;

/* String containing arguments to give to the program, separated by spaces.
   Empty string (pointer to '\0') means no args.  */

static char *inferior_args;

/* File name for default use for standard in/out in the inferior.  */

char *inferior_io_terminal;

     extern int debugging_aries, debugging_aries64;

     extern char * real_exec_file;

/* Pid of our debugged inferior, or 0 if no inferior now.
   Since various parts of infrun.c test this to see whether there is a program
   being debugged it should be nonzero (currently 3 is used) for remote
   debugging.  */

int inferior_pid;

/* We use this pc to determine whether we want to
   step/next into a function */
CORE_ADDR steplast_pc = 0;

#define RETURN_PC_OF_LAST_CALL_FOR_C(start, end) \
             return_pc_of_last_call_for_C(start, end)

#define RETURN_PC_OF_LAST_CALL_FOR_CPLUSPLUS(start, end, symtab) \
             return_pc_of_last_call_for_cplusplus(start, end, symtab)

/* step_range_end points to the start of the next line.
   But, we are interested in last insn(-4) of the current line */
#define RETURN_PC_OF_LAST_CALL(start, end, symtab) \
            (is_steplast_eligible) \
              ? RETURN_PC_OF_LAST_CALL_FOR_CPLUSPLUS(start, end, symtab) \
              : RETURN_PC_OF_LAST_CALL_FOR_C(start, end - 4)

/* Last signal that the inferior received (why it stopped).  */

enum target_signal stop_signal;

/* Address at which inferior stopped.  */

CORE_ADDR stop_pc;

/* Chain containing status of breakpoint(s) that we have stopped at.  */

bpstat stop_bpstat;

/* Flag indicating that a command has proceeded the inferior past the
   current breakpoint.  */

int breakpoint_proceeded;

/* set when executing next command; not set for step command
   Used by symtab.c to skip inlined code for next
   command.
   FIXME: Need to be in infrun_state for thread protection.
*/ 
int executing_next_command;
int next_command_inline_idx;

/* This is set to 1 when executing stepi and nexti commands. */
boolean single_stepping = false;

/* Nonzero if stopped due to a step command.  */

int stop_step;

/* Nonzero if stopped due to completion of a stack dummy routine.  */

int stop_stack_dummy;

/* Nonzero if stopped due to a random (unexpected) signal in inferior
   process.  */

int stopped_by_random_signal;

/* Range to single step within.
   If this is nonzero, respond to a single-step signal
   by continuing to step if the pc is in this range.  */

CORE_ADDR step_range_start;	/* Inclusive */
CORE_ADDR step_range_end;	/* Exclusive */

/* Stack frame address as of when stepping command was issued.
   This is how we know when we step into a subroutine call,
   and how to set the frame for the breakpoint used to step out.  */

CORE_ADDR step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
     CORE_ADDR step_frame_rse_addr;
#endif

/* Our notion of the current stack pointer.  */

CORE_ADDR step_sp;

/* 1 means step over all subroutine calls.
   0 means don't step over calls (used by stepi).
   -1 means step over calls to undebuggable functions.  */

int step_over_calls;

/* If stepping, nonzero means step count is > 1
   so don't print frame next time inferior stops
   if it stops due to stepping.  */

int step_multi;

/* Environment to use for running inferior,
   in format described in environ.h.  */

struct environ *inferior_environ;

boolean during_run_flag = 0;
extern boolean reread_on_run;

/* This function detects whether or not a '&' character (indicating
   background execution) has been added as *the last* of the arguments ARGS
   of a command. If it has, it removes it and returns 1. Otherwise it
   does nothing and returns 0. */
static int
strip_bg_char (char **args)
{
  char *p = NULL;

  p = strchr (*args, '&');

  if (p)
    {
      if (p == (*args + strlen (*args) - 1))
	{
	  if (strlen (*args) > 1)
	    {
	      do
		p--;
	      while (*p == ' ' || *p == '\t');
	      *(p + 1) = '\0';
	    }
	  else
	    *args = 0;
	  return 1;
	}
    }
  return 0;
}

/* ARGSUSED */
void
tty_command (char *file, int from_tty)
{
  if (file == 0)
    error_no_arg ("terminal name for running target process");

  inferior_io_terminal = savestring (file, (int) strlen (file));
}

extern int if_yes_to_all_commands;

static void
run_command (char *args, int from_tty)
{
  extern void do_run_cleanups (register struct cleanup * old_chain);
#ifdef HPPA_FIX_AND_CONTINUE
  extern void set_reapply_fix (boolean);
#endif
  struct minimal_symbol * msym;
  extern struct breakpoint *breakpoint_chain;
  struct breakpoint *b;

  char *exec_file;
  extern int have_read_load_info;
  extern int errno;
#ifdef LOG_BETA_RUN_COMMAND_TIME
  time_t        start_time;
  time_t        elapsed_time;
  
  start_time = time (NULL);
#endif

#ifdef HPPA_FIX_AND_CONTINUE
  check_unapplied_changes();
#endif
  dont_repeat ();
  if (inferior_pid != 0 && target_has_execution 
#ifdef GDB_TARGET_IS_HPUX
     && inferior_is_live ()
#endif
    )
    {
      if (from_tty
	  && !query ("The program being debugged has been started already.\n\
Start it from the beginning? "))
	error ("Program not restarted.");
    }

  disable_hw_watchpoints ();

#if defined(SOLIB_RESTART)
  SOLIB_RESTART ();
#endif

  if (inferior_pid != 0 && target_has_execution)
    {
      target_kill ();
      init_wait_for_inferior ();
    }

  /* srikanth, 000131, it doesn't hurt to be overcautious. */
  invalidate_shadow_contents ();

  clear_breakpoint_hit_counts ();

  exec_file = (char *) get_exec_file (0);

  /* Purge old solib objfiles. */
  if (!reread_on_run)
    during_run_flag = 1;
  objfile_purge_solibs ();
  during_run_flag = 0;

  do_run_cleanups (NULL);
  /* JAGag13915: Remove core-file information if any. */
  unpush_target (&core_ops);

  /* The exec file is re-read every time we do a generic_mourn_inferior, so
     we just have to worry about the symbol file.  */
  reread_symbols ();
  
  refresh_display_chain();
  /* We keep symbols from add-symbol-file, on the grounds that the
     user might want to add some symbols before running the program
     (right?).  But sometimes (dynamic loading where the user manually
     introduces the new symbols with add-symbol-file), the code which
     the symbols describe does not persist between runs.  Currently
     the user has to manually nuke all symbols between runs if they
     want them to go away (PR 2207).  This is probably reasonable.  */

  if (!args)
    {
      if (event_loop_p && target_can_async_p ())
	async_disable_stdin ();
    }
  else
    {
      char *cmd;
      int async_exec = strip_bg_char (&args);

      /* If we get a request for running in the bg but the target
         doesn't support it, error out. */
      if (event_loop_p && async_exec && !target_can_async_p ())
	error ("Asynchronous execution not supported on this target.");

      /* If we don't get a request of running in the bg, then we need
         to simulate synchronous (fg) execution. */
      if (event_loop_p && !async_exec && target_can_async_p ())
	{
	  /* Simulate synchronous execution */
	  async_disable_stdin ();
	}

      /* If there were other args, beside '&', process them. */
      if (args)
	{
	  cmd = concat ("set args ", args, NULL);
	  make_cleanup (free, cmd);
	  execute_command (cmd, from_tty);
	}
    }

  if (from_tty)
    {
      if (debugging_aries || debugging_aries64)
        exec_file = real_exec_file;

#ifdef UI_OUT
      ui_out_field_string (uiout, NULL, "Starting program");
      ui_out_text (uiout, ": ");
      if (exec_file)
	ui_out_field_string (uiout, "execfile", exec_file);
      ui_out_spaces (uiout, 1);
      ui_out_field_string (uiout, "infargs", inferior_args);
      ui_out_text (uiout, "\n");
      ui_out_flush (uiout);
#else
      puts_filtered ("Starting program: ");
      if (exec_file)
	puts_filtered (exec_file);
      puts_filtered (" ");
      puts_filtered (inferior_args);
      puts_filtered ("\n");
      gdb_flush (gdb_stdout);
#endif
    }

#ifdef HP_IA64
  /* Make sure to re-read the load info on re-run.  Currently, the kernel
     doesn't move the gateway page between runs, so not doing this isn't
     a huge problem yet.  This line will make sure that it never becomes
     a problem in the future.  Also, this will have the side-effect of
     making sure gdb's internal variable '$user_sendsig' gets set for the
     user.  */
  have_read_load_info = 0;
#endif

#ifdef RTC
  /* Reset all the globals to prepare for thread tracing. */
  prepare_for_thread_tracing(0);  /* not attached to */
  /* Reset all the globals to prepare for runtime checking */
  prepare_for_rtc (0);  /* not attached to */
  setup_librtc_env();
#endif

  if (profile_on)
    initiate_profiling_after_launch ();

  /* We don't want to intercept sigwait signals before main. */

  /* This interfers with profiling at least for stripped binaries, may be for
     others too. Disable for now -- Baskar & Srikanth Dec 21 05
  */
  if (!profile_on)
    {
      if (!sigwait_sig_in_init)
        create_rantomain_event_breakpoint (1);
    }

#ifdef HPPA_FIX_AND_CONTINUE
  set_reapply_fix (true);
#endif
  target_create_inferior (exec_file, inferior_args,
 			  environ_vector (inferior_environ));

#ifdef LOG_BETA_RUN_COMMAND_TIME
  elapsed_time = time (NULL) - start_time;
  if (elapsed_time > 5)
    log_test_event (LOG_BETA_RUN_COMMAND_TIME, (int) elapsed_time);
#endif
}


static void
run_no_args_command (char *args, int from_tty)
{
  execute_command ("set args", from_tty);
  run_command ((char *) NULL, from_tty);
}


void
continue_command (char *proc_count_exp, int from_tty)
{
  int async_exec = 0;
  ERROR_NO_INFERIOR;

  /* Find out whether we must run in the background. */
  if (proc_count_exp != NULL)
    async_exec = strip_bg_char (&proc_count_exp);

  /* If we must run in the background, but the target can't do it,
     error out. */
  if (event_loop_p && async_exec && !target_can_async_p ())
    error ("Asynchronous execution not supported on this target.");

  /* If we are not asked to run in the bg, then prepare to run in the
     foreground, synchronously. */
  if (event_loop_p && !async_exec && target_can_async_p ())
    {
      /* Simulate synchronous execution */
      async_disable_stdin ();
    }

#ifdef HPPA_FIX_AND_CONTINUE
  check_unapplied_changes();
#endif

  /* If have argument (besides '&'), set proceed count of breakpoint
     we stopped at.  */
  if (proc_count_exp != NULL)
    {
      bpstat bs = stop_bpstat;
      int num = bpstat_num (&bs);
      if (num <= 0 && from_tty)
	{
	  printf_filtered
	    ("Not stopped at any breakpoint; argument ignored.\n");
	}
      while (num > 0)
	{
	  set_ignore_count (num,
			    (int) (parse_and_eval_address (proc_count_exp) - 1),
			    from_tty);
	  /* set_ignore_count prints a message ending with a period.
	     So print two spaces before "Continuing.".  */
	  if (from_tty)
	    printf_filtered ("  ");
	  num = bpstat_num (&bs);
	}
    }

  if (from_tty)
    printf_filtered ("Continuing.\n");

  clear_proceed_status ();

  proceed ((CORE_ADDR)(long) -1, TARGET_SIGNAL_DEFAULT, 0);
}

/* Step until outside of current statement.  */

/* ARGSUSED */
static void
step_command (char *count_string, int from_tty)
{
  step_1 (0, 0, 0, count_string);
}

/* Step into a function but not into calls for evaluating arguments */
   
static void
steplast_command (char *count_string, int from_tty)
{
   if (( current_language->la_language != language_c )
        && (current_language->la_language != language_cplus) )
       error ("Steplast support is provided only for C and C++ language " );
 
    step_1 (0, 0, 1, count_string);
}

/* 	Steplast support for "C" language
  Given a start & end address of the line, this function will
  return the "rp" of the last call */

CORE_ADDR 
return_pc_of_last_call_for_C (CORE_ADDR step_range_start,
                              CORE_ADDR step_range_end)
{ 
    int index = 0;
    long insn = 0;
    if (step_range_end == 0)
        return 0;
    while ( (step_range_end - index) >= step_range_start )
      {
#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
        insn = read_memory_integer( ( step_range_end - index ), 
                                      INSTRUCTION_SIZE); 
#else
        insn = read_memory_unsigned_integer ( ( step_range_end - index ),
					         INSTRUCTION_SIZE);
#endif
#endif
        if (   ((insn & 0xfc000000) == 0xe8000000) /* B,BL,BLR, BV(&BVE for 64bit)*/
            || ((insn & 0xfc000000) == 0xe4000000) /* BLE*/
            || ((insn & 0xfc000000) == 0xe0000000) /* BE*/
             /* BB, BVB not included yet */
           ) 
             {
	       /* We're at the last branch. So, adding the current "pc"
		  with 2*INSTRUCTION_SIZE will give you the "rp"
		  of the last/desired call.
                */
               return ( step_range_end - index + 2*INSTRUCTION_SIZE );
             }
        index+=4;
      }
   return 0;
}

/* Finds out the steplast_pc for the current line using the
   steplast line table which was constructed from the debug info.*/

CORE_ADDR
return_pc_of_last_call_for_cplusplus (CORE_ADDR stop_pc,
				      CORE_ADDR step_range_end,
 	                              struct symtab_and_line sl) 
 {
    register struct steplast_linetable *slt;    
    int len ;
    int index;
    if ( sl.symtab )
      slt = STEPLAST_LINETABLE (sl.symtab);
    else 
       return 0;
    len = slt->nitems;
    if (len > 0) {
      /* Return the first matching real_call_pc that lies 
         between step_range_start and step_range_end */
      for ( index = 0; index < len; index++)
          if ( ( slt->item[index].real_call_pc >= stop_pc ) 
              && (slt->item[index].real_call_pc < step_range_end ) ) 
               return (slt->item[index].real_call_pc + 2*INSTRUCTION_SIZE); 
    }
    /* Ouch!! We didn't find an entry in the steplast line table */
    return 0;
 }

/* Likewise, but skip over subroutine calls as if single instructions.  */

/* ARGSUSED */
static void
next_command (char *count_string, int from_tty)
{
  struct cleanup *old_chain;
  ERROR_NO_INFERIOR;
  /* following global is set to tell decode_dwarf_lines to
     not step into inline functions.
  */
  executing_next_command = 1;
  next_command_inline_idx = (get_current_frame ())->inline_idx;
  old_chain = make_cleanup (set_var_to_zero, &executing_next_command);
  old_chain = make_cleanup (set_var_to_zero, &next_command_inline_idx);
  step_1 (1, 0, 0, count_string);
  do_cleanups (old_chain);
}

/* Likewise, but step only one instruction.  */

/* ARGSUSED */
void
stepi_command (char *count_string, int from_tty)
{
  struct cleanup *old_chain;
  single_stepping = 1;
  old_chain = make_cleanup (set_var_to_zero, &single_stepping);
  step_1 (0, 1, 0, count_string);
  do_cleanups (old_chain);
}

/* ARGSUSED */
void
nexti_command (char *count_string, int from_tty)
{
  struct cleanup *old_chain;
  single_stepping = 1;
  old_chain = make_cleanup (set_var_to_zero, &single_stepping);
  step_1 (1, 1, 0, count_string);
  do_cleanups (old_chain);
}

#ifdef GET_LONGJMP_TARGET
static void
disable_longjmp_breakpoint_cleanup (void *ignore)
{
  disable_longjmp_breakpoint ();
}
#endif

void
step_1 (int skip_subroutines,
         int single_inst,
         int steplast,
         char *count_string)
{
  register int count = 1;
#if 0
  register int actual_count = 1;
#endif
  struct frame_info *frame;
  struct symtab_and_line sal = { 0 };
  struct breakpoint* breakpoint;
  struct cleanup *old_chain;
  CORE_ADDR tmp_pc;
  int new_pid;
#ifdef GET_LONGJMP_TARGET
  struct cleanup *cleanups = 0;
#endif
  int async_exec = 0;
  CORE_ADDR temp_step_start = 0;
  CORE_ADDR temp_step_end = 0;

  ERROR_NO_INFERIOR;
  if (count_string)
    async_exec = strip_bg_char (&count_string);

  /* If we get a request for running in the bg but the target
     doesn't support it, error out. */
  if (event_loop_p && async_exec && !target_can_async_p ())
    error ("Asynchronous execution not supported on this target.");

  /* If we don't get a request of running in the bg, then we need
     to simulate synchronous (fg) execution. */
  if (event_loop_p && !async_exec && target_can_async_p ())
    {
      /* Simulate synchronous execution */
      async_disable_stdin ();
    }

#ifdef HPPA_FIX_AND_CONTINUE
  check_unapplied_changes();
#endif

#ifdef FIND_ACTIVE_THREAD
  /* If we have CMA threads, we will actually be stepping the active thread.
     Make it the official inferior_pid and inform the user if there is a
     thread switch. */

  new_pid = FIND_ACTIVE_THREAD ();
  if (inferior_pid != new_pid)
    {
      inferior_pid = new_pid;
      switch_to_thread (inferior_pid);
      printf_filtered ("[Switching to thread %d (%s)]\n",
                       pid_to_thread_id (inferior_pid),
#if defined(GDB_TARGET_IS_HPUX)
                       target_tid_to_str (inferior_pid)
#else
                       target_pid_to_str (inferior_pid)
#endif
        );

    }
#endif

  count = count_string ? (int) parse_and_eval_address (count_string) : 1;

/*  I think, steplast <n> feature will not be meaningful as "step <n>"
    or "next <n>". So, for the time being, I am putting the changes under
    "#if 0". In case, any customer asks for this, you have to just remove this
    "if" and error condition and "#if 0's". */

  if ( steplast && ( count > 1 ) )
     error ("Not implemented: steplast <n> feature."); 

#if 0
/* To support steplast <n>, remove #if 0*/
  actual_count = count;
#endif
#ifdef GET_LONGJMP_TARGET
  if (!single_inst || skip_subroutines)		/* leave si command alone */
    {
      enable_longjmp_breakpoint ();
      if (!event_loop_p || !target_can_async_p ())
	cleanups = make_cleanup (disable_longjmp_breakpoint_cleanup, 0);
      else
        make_exec_cleanup (disable_longjmp_breakpoint_cleanup, 0);
    }
#endif

  /* In synchronous case, all is well, just use the regular for loop. */
  if (!event_loop_p || !target_can_async_p ())
    {
      /* JAGag12140: Enable bor call-backs only on steps */
      if (!skip_subroutines)
        SOLIB_REQUEST_BOR_CALLBACK(1);

      for (; count > 0; count--)
	{
	  clear_proceed_status ();

	  frame = get_current_frame ();
          /* Avoid coredump here.  Why tho? */
	  if (!frame)
	    error ("No current frame");

	  step_frame_address = FRAME_FP (frame);
          
#ifdef INLINE_SUPPORT
	  /* When stepping into subroutines, check if we can do a
	     fake step. */
	  if (!skip_subroutines && set_next_inline_frame ())
	    {
	      stop_step = 1;
	      step_multi = (count > 1);
	      normal_stop ();
	      continue;
	    }
#endif

#ifdef REGISTER_STACK_ENGINE_FP
	  step_frame_rse_addr= frame->rse_fp;
#endif
	  step_sp = read_sp ();

	  if (!single_inst)
	    {
              if ( steplast
		   && (current_language->la_language == language_cplus)
                   && !is_steplast_eligible )
                 {
                   error ("Failed to do a steplast at the current line.\n"
                          "Make sure that you are using the A.03.50 "
                          "compiler \n(with -g0 option) to perform steplast.");
                 }

              if (steplast && is_steplast_eligible ) /* For C++ */
                {
                 /* Find out the best symtab for the current "pc" which
		    will be later used to get the steplast line table */
	            sal = find_best_symtab_for_pc (stop_pc, 
                             &step_range_start, &step_range_end);
                } 
              else 
                {
	          find_pc_line_pc_range (stop_pc, &step_range_start, 
                                         &step_range_end);
	        }
	      steplast_pc = 0;
	      if (step_range_end == 0)
		{
		  char *name;
		  if (find_pc_partial_function (stop_pc, &name, 
                                                &step_range_start,
						&step_range_end) == 0)
		    error ("Cannot find bounds of current function");

		  target_terminal_ours ();

                  /* JAGae68084  if yes to breakall commands , be silent */
                  if (!if_yes_to_all_commands)
                    if (!steplast) {
                      printf_filtered ("Single stepping until exit from "
                                       "function %s, \nwhich has no line "
                                       "number information.\n", name);
                    }
		}

              if (steplast)
                {
                  /* Steplast in "C" 
                     We use heuristic approach - using last branch in the
                     current line - to find out steplast_pc.
                     Steplast in "C++"
                     The aCC compiler marks all the real_call
                     pc's with a specific line number with 6th and 11th 
                     bit set for normal and DOC modes respectively
                     when the application is compiled with -g0.
                     This real call pc is nothing but the steplast_pc */

                  steplast_pc = 
                    RETURN_PC_OF_LAST_CALL (stop_pc, step_range_end, sal);
                  if ( !steplast_pc ) 
                    {
#if 0
                      /* To support steplast <n>, remove #if 0*/
                      if ( (actual_count - count ) > 0 )
                        printf_filtered ("Not able to do more than %d "
                                         "steplast(s).\n", 
                                         actual_count - count);
#endif
                      error ("Steplast is not meaningful for the current line."); 
                    }

                  /* Steplast support for C++
		     Set a breakpoint at (steplast_pc - 2 * INSTRUCTION_SIZE)
                     insn and do a continue till that point. Then, do steplast
		     from that insn. If we don't do this, the performance
		     for steplast will be bad, as gdb will be doing a stepi
		     for each insn till it get into the steplast_pc */

                  if ( steplast_pc!=0 && is_steplast_eligible ) 
                    {
                      tmp_pc = steplast_pc - 2 * INSTRUCTION_SIZE;
                      if ( tmp_pc > step_range_start ) 
                        {
                          sal = find_pc_line (tmp_pc, 0);
                          sal.pc = tmp_pc;
                          breakpoint = set_momentary_breakpoint (sal, 
                                       NULL, bp_finish); 
	                  breakpoint->silent = 1;
                          old_chain = 
                            make_cleanup_delete_breakpoint (breakpoint);
	                  temp_step_start = step_range_start;
	                  temp_step_end = step_range_end;
                          clear_proceed_status ();
                          proceed_to_finish = 1;
                          proceed ( (CORE_ADDR)(long) -1, 
                                    TARGET_SIGNAL_DEFAULT, 0);
                          do_cleanups (old_chain); 
                          step_range_start = temp_step_start;
		          step_range_end = temp_step_end;
                        }
                    }
                } 
	    }
	  else
	    {
	      /* Say we are stepping, but stop after one insn whatever it 
                 does.  */
	      step_range_start = step_range_end = 1;
	      if (!skip_subroutines)
		/* It is stepi.
		   Don't step over function calls, not even to functions 
                   lacking line numbers.  */
		step_over_calls = 0;
	    }

	  if (skip_subroutines)
	    step_over_calls = 1;

	  step_multi = (count > 1);
	  proceed ((CORE_ADDR)(long) -1, TARGET_SIGNAL_DEFAULT, 1);
          /* JAGag03846: Perform implicit stepi`s when in sigtramp before 
             actually stepping into signal handler. */
          while (sigtramp_stepi_again)
            {
              single_stepping = true;
              step_over_calls = 0;
              sigtramp_stepi_again = false;
              proceed ((CORE_ADDR)(long) -1, TARGET_SIGNAL_DEFAULT, 1);
            }

	  if (!stop_step)
	    break;

	  /* FIXME: On nexti, this may have already been done (when we hit the
	     step resume break, I think).  Probably this should be moved to
	     wait_for_inferior (near the top).  */
#if defined (SHIFT_INST_REGS)
	  SHIFT_INST_REGS ();
#endif
	}

#ifdef GET_LONGJMP_TARGET
      if (!single_inst || skip_subroutines)
	do_cleanups (cleanups);
#endif
      return;
    }
  /* In case of asynchronous target things get complicated, do only
     one step for now, before returning control to the event loop. Let
     the continuation figure out how many other steps we need to do,
     and handle them one at the time, through step_once(). */
  else
    {
      if (event_loop_p && target_can_async_p ())
	step_once (skip_subroutines, single_inst, count);
    }
}

/* Called after we are done with one step operation, to check whether
   we need to step again, before we print the prompt and return control
   to the user. If count is > 1, we will need to do one more call to
   proceed(), via step_once(). Basically it is like step_once and
   step_1_continuation are co-recursive. */
static void
step_1_continuation (struct continuation_arg *arg)
{
  int count;
  int skip_subroutines;
  int single_inst;
  struct symtab_and_line sal;

  skip_subroutines = arg->data.integer;
  single_inst      = arg->next->data.integer;
  count            = arg->next->next->data.integer;

  if (stop_step)
    {
      /* FIXME: On nexti, this may have already been done (when we hit the
	 step resume break, I think).  Probably this should be moved to
	 wait_for_inferior (near the top).  */
#if defined (SHIFT_INST_REGS)
      SHIFT_INST_REGS ();
#endif
      step_once (skip_subroutines, single_inst, count - 1);
    }
#ifdef GET_LONGJMP_TARGET
  else
    if (!single_inst || skip_subroutines)
      do_exec_cleanups (ALL_CLEANUPS);
#endif
}

/* Do just one step operation. If count >1 we will have to set up a
   continuation to be done after the target stops (after this one
   step). This is useful to implement the 'step n' kind of commands, in
   case of asynchronous targets. We had to split step_1 into two parts,
   one to be done before proceed() and one afterwards. This function is
   called in case of step n with n>1, after the first step operation has
   been completed.*/
static void 
step_once (int skip_subroutines, int single_inst, int count)
{ 
  struct continuation_arg *arg1; 
  struct continuation_arg *arg2;
  struct continuation_arg *arg3; 
  struct frame_info *frame;

  if (count > 0)
    {
      clear_proceed_status ();

      frame = get_current_frame ();
      if (!frame)		/* Avoid coredump here.  Why tho? */
	error ("No current frame");
      step_frame_address = FRAME_FP (frame);
#ifdef REGISTER_STACK_ENGINE_FP
      step_frame_rse_addr= frame->rse_fp;
#endif
      step_sp = read_sp ();

      if (!single_inst)
	{
	  find_pc_line_pc_range (stop_pc, &step_range_start, &step_range_end);
	  if (step_range_end == 0)
	    {
	      char *name;
	      if (find_pc_partial_function (stop_pc, &name, &step_range_start,
					    &step_range_end) == 0)
		error ("Cannot find bounds of current function");

	      target_terminal_ours ();
	      printf_filtered ("\
Single stepping until exit from function %s, \n\
which has no line number information.\n", name);
	    }
	}
      else
	{
	  /* Say we are stepping, but stop after one insn whatever it does.  */
	  step_range_start = step_range_end = 1;
	  if (!skip_subroutines)
	    /* It is stepi.
	       Don't step over function calls, not even to functions lacking
	       line numbers.  */
	    step_over_calls = 0;
	}

      if (skip_subroutines)
	step_over_calls = 1;

      step_multi = (count > 1);
      arg1 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg2 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg3 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg1->next = arg2;
      arg1->data.integer = skip_subroutines;
      arg2->next = arg3;
      arg2->data.integer = single_inst;
      arg3->next = NULL;
      arg3->data.integer = count;
      add_intermediate_continuation (step_1_continuation, arg1);

      
#ifdef INLINE_SUPPORT
      if (!skip_subroutines && set_next_inline_frame ())
	{
	  /* no need to proceed if we are doing a fake step. */
	  stop_step = 1;
	}
      else
#endif
        proceed ((CORE_ADDR)(long) -1, TARGET_SIGNAL_DEFAULT, 1);
    }
}


/* Continue program at specified address.  */

static void
jump_command (char *arg, int from_tty)
{
  register CORE_ADDR addr;
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  struct symbol *fn;
  struct symbol *sfn;
  int async_exec = 0;
  int pick = 0;

  ERROR_NO_INFERIOR;

  /* Find out whether we must run in the background. */
  if (arg != NULL)
    async_exec = strip_bg_char (&arg);

  /* If we must run in the background, but the target can't do it,
     error out. */
  if (event_loop_p && async_exec && !target_can_async_p ())
    error ("Asynchronous execution not supported on this target.");

  /* If we are not asked to run in the bg, then prepare to run in the
     foreground, synchronously. */
  if (event_loop_p && !async_exec && target_can_async_p ())
    {
      /* Simulate synchronous execution */
      async_disable_stdin ();
    }

  if (!arg)
    error_no_arg ("starting address");

  sals = decode_line_spec_1 (arg, 1);
  if (sals.nelts != 1)
    {
      /* Srikanth, in the presence of multiple instantiations
         of the same template, we get multiple choices here.
         Don't error out, pick up the symtab containg the PC.
      */
      struct symtab * pc_symtab;

      pick = -1;
      pc_symtab = find_pc_symtab (stop_pc);
      if (pc_symtab)
        {
          int i;
          for (i = 0; i < sals.nelts; i++)
            if (sals.sals[i].symtab == pc_symtab)
              pick = i;
        }
      if (pick == -1)
        error ("Unreasonable jump request");
    }

  sal = sals.sals[pick];
  free ((PTR) sals.sals);

  if (sal.symtab == 0 && sal.pc == 0)
    error ("No source file has been specified.");

  resolve_sal_pc (&sal);	/* May error out */

  /* See if we are trying to jump to another function. */
  fn = get_frame_function (get_current_frame ());
  sfn = find_pc_function (sal.pc);
  if (fn != NULL && sfn != fn)
    {
      if (!query ("Line %d is not in `%s'.  Jump anyway? ", sal.line,
		  SYMBOL_SOURCE_NAME (fn)))
	{
	  error ("Not confirmed.");
	  /* NOTREACHED */
	}
    }

  if (sfn != NULL)
    {
      fixup_symbol_section (sfn, 0);
      if (section_is_overlay (SYMBOL_BFD_SECTION (sfn)) &&
	  !section_is_mapped (SYMBOL_BFD_SECTION (sfn)))
	{
	  if (!query ("WARNING!!!  Destination is in unmapped overlay!  Jump anyway? "))
	    {
	      error ("Not confirmed.");
	      /* NOTREACHED */
	    }
	}
    }

  addr = sal.pc;
#ifdef HP_IA64_GAMBIT
  if ((addr % 16) != 0)
    {
      error ("Jump address: 0x%llx [0x%llx:%d] is not bundle-aligned.\n",
	     addr, addr & (~0xF), (int) (addr % 16));
    }
#endif /*HP_IA64 */

  if (from_tty)
    {
      printf_filtered ("Continuing at ");
      print_address_numeric (addr, 1, gdb_stdout);
      printf_filtered (".\n");
    }

  clear_proceed_status ();
  proceed (addr, TARGET_SIGNAL_0, 0);
}


/* Go to line or address in current procedure */
static void
go_command (char *line_no, int from_tty)
{
#ifdef HPPA_FIX_AND_CONTINUE
  check_unapplied_changes();
#endif
 if (line_no == (char *) NULL || !*line_no)
    printf_filtered (GO_USAGE);
  else
    {
      tbreak_command (line_no, from_tty);
      jump_command (line_no, from_tty);
    }
}


/* Continue program giving it specified signal.  */

static void
signal_command (char *signum_exp, int from_tty)
{
  enum target_signal oursig;

  dont_repeat ();		/* Too dangerous.  */
  ERROR_NO_INFERIOR;

  if (!signum_exp)
    error_no_arg ("signal number");

  /* It would be even slicker to make signal names be valid expressions,
     (the type could be "enum $signal" or some such), then the user could
     assign them to convenience variables.  */
  oursig = target_signal_from_name (signum_exp);

  if (oursig == TARGET_SIGNAL_UNKNOWN)
    {
      /* No, try numeric.  */
      int num = (int) parse_and_eval_address (signum_exp);

      if (num == 0)
	oursig = TARGET_SIGNAL_0;
      else
	oursig = target_signal_from_command (num);
    }

  if (from_tty)
    {
      if (oursig == TARGET_SIGNAL_0)
	printf_filtered ("Continuing with no signal.\n");
      else
	printf_filtered ("Continuing with signal %s.\n",
			 target_signal_to_name (oursig));
    }

  clear_proceed_status ();
  /* "signal 0" should not get stuck if we are stopped at a breakpoint.
     FIXME: Neither should "signal foo" but when I tried passing
     (CORE_ADDR)-1 unconditionally I got a testsuite failure which I haven't
     tried to track down yet.  */
  proceed (oursig == TARGET_SIGNAL_0 ? (CORE_ADDR)(long) -1 : stop_pc, oursig, 0);
}

/* Call breakpoint_auto_delete on the current contents of the bpstat
   pointed to by arg (which is really a bpstat *).  */

static void
breakpoint_auto_delete_contents (PTR arg)
{
  breakpoint_auto_delete (*(bpstat *) arg);
}


/* Execute a "stack dummy", a piece of code stored in the stack
   by the debugger to be executed in the inferior.

   To call: first, do PUSH_DUMMY_FRAME.
   Then push the contents of the dummy.  It should end with a breakpoint insn.
   Then call here, passing address at which to start the dummy.

   The contents of all registers are saved before the dummy frame is popped
   and copied into the buffer BUFFER.

   The dummy's frame is automatically popped whenever that break is hit.
   If that is the first time the program stops, run_stack_dummy
   returns to its caller with that frame already gone and returns 0.
   
   Otherwise, run_stack-dummy returns a non-zero value.
   If the called function receives a random signal, we do not allow the user
   to continue executing it as this may not work.  The dummy frame is poped
   and we return 1.
   If we hit a breakpoint, we leave the frame in place and return 2 (the frame
   will eventually be popped when we do hit the dummy end breakpoint).  */

int
run_stack_dummy (CORE_ADDR addr, char *buffer)
{
  struct cleanup *old_cleanups = make_cleanup (null_cleanup, 0);
  int saved_async = 0;

  /* Now proceed, having reached the desired place.  */
  clear_proceed_status ();

  if (CALL_DUMMY_BREAKPOINT_OFFSET_P)
    {
      struct breakpoint *bpt;
      struct symtab_and_line sal;

      INIT_SAL (&sal);		/* initialize to zeroes */
      if (CALL_DUMMY_LOCATION == AT_ENTRY_POINT)
	{
	  sal.pc = CALL_DUMMY_ADDRESS ();
	}
      else
	{
	  sal.pc = addr - CALL_DUMMY_START_OFFSET + CALL_DUMMY_BREAKPOINT_OFFSET;
	}
      sal.section = find_pc_overlay (sal.pc);

      /* Set up a FRAME for the dummy frame so we can pass it to
         set_momentary_breakpoint.  We need to give the breakpoint a
         frame in case there is only one copy of the dummy (e.g.
         CALL_DUMMY_LOCATION == AFTER_TEXT_END).  */
      flush_cached_frames ();
      set_current_frame (create_new_frame (read_fp (), sal.pc, 0));

      /* If defined, CALL_DUMMY_BREAKPOINT_OFFSET is where we need to put
         a breakpoint instruction.  If not, the call dummy already has the
         breakpoint instruction in it.

         addr is the address of the call dummy plus the CALL_DUMMY_START_OFFSET,
         so we need to subtract the CALL_DUMMY_START_OFFSET.  */
      bpt = set_momentary_breakpoint (sal,
				      get_current_frame (),
				      bp_call_dummy);
#ifdef HP_IA64
      /* This dummy_break_addr address value is used by caller of this function,
         to record the failed CLC thread's dummy breakpoint address.
         To uniquely identify each CLC */
      dummy_break_addr = sal.pc;
#endif
      bpt->disposition = del;

      /* If all error()s out of proceed ended up calling normal_stop (and
         perhaps they should; it already does in the special case of error
         out of resume()), then we wouldn't need this.  */
      make_cleanup (breakpoint_auto_delete_contents, &stop_bpstat);
    }

  disable_watchpoints_before_interactive_call_start ();
  proceed_to_finish = 1;	/* We want stop_registers, please... */

  if (target_can_async_p ())
    saved_async = target_async_mask (0);

  /* To debug the stack dummy, change the final 0 to 1 to cause gdb to
     single step through the call dummy. */

  proceed (addr, TARGET_SIGNAL_0, 0);

  if (saved_async)
    target_async_mask (saved_async);

  enable_watchpoints_after_interactive_call_stop ();

  discard_cleanups (old_cleanups);

  /* We can stop during an inferior call because a signal is received. */
  if (stopped_by_random_signal)
    return 1;
    
  /* We may also stop prematurely because we hit a breakpoint in the
     called routine. */
  if (!stop_stack_dummy)
    return 2;

  /* On normal return, the stack dummy has been popped already.  */

  memcpy (buffer, stop_registers, REGISTER_BYTES);
  return 0;
}

/* Proceed until we reach a different source line with pc greater than
   our current one or exit the function.  We skip calls in both cases.

   Note that eventually this command should probably be changed so
   that only source lines are printed out when we hit the breakpoint
   we set.  This may involve changes to wait_for_inferior and the
   proceed status code.  */

/* ARGSUSED */
static void
until_next_command (int from_tty)
{
  struct frame_info *frame;
  CORE_ADDR pc;
  struct symbol *func;
  struct symtab_and_line sal;

  clear_proceed_status ();

  frame = get_current_frame ();

  /* Step until either exited from this function or greater
     than the current line (if in symbolic section) or pc (if
     not). */

  pc = read_pc ();
  func = find_pc_function (pc);

  if (!func)
    {
      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (pc);

      if (msymbol == NULL)
	error ("Execution is not within a known function.");

      step_range_start = SYMBOL_VALUE_ADDRESS (msymbol);
      step_range_end = pc;
    }
  else
    {
      sal = find_pc_line (pc, 0);

      step_range_start = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (func)));
      step_range_end = SWIZZLE(sal.end);
    }

  step_over_calls = 1;
  step_frame_address = FRAME_FP (frame);
#ifdef REGISTER_STACK_ENGINE_FP
  step_frame_rse_addr = frame->rse_fp;
#endif
  step_sp = read_sp ();

  step_multi = 0;		/* Only one call to proceed */

  proceed ((CORE_ADDR)(long) -1, TARGET_SIGNAL_DEFAULT, 1);
}

static void
until_command (char *arg, int from_tty)
{
  int async_exec = 0;

  if (!target_has_execution)
    error ("The program is not running.");

  /* Find out whether we must run in the background. */
  if (arg != NULL)
    async_exec = strip_bg_char (&arg);

  /* If we must run in the background, but the target can't do it,
     error out. */
  if (event_loop_p && async_exec && !target_can_async_p ())
    error ("Asynchronous execution not supported on this target.");

  /* If we are not asked to run in the bg, then prepare to run in the
     foreground, synchronously. */
  if (event_loop_p && !async_exec && target_can_async_p ())
    {
      /* Simulate synchronous execution */
      async_disable_stdin ();
    }

#ifdef HPPA_FIX_AND_CONTINUE
  check_unapplied_changes();
#endif

  if (arg)
    until_break_command (arg, from_tty);
  else
    until_next_command (from_tty);
}


/* Print the result of a function at the end of a 'finish' command. */
static void
print_return_value (int structure_return, struct type *value_type)
{
  register value_ptr value;
#ifdef UI_OUT
  static struct ui_stream *stb = NULL;
#endif /* UI_OUT */

  if (!structure_return)
    {
      value = value_being_returned (value_type, stop_registers, structure_return);
#ifdef UI_OUT
      stb = ui_out_stream_new (uiout);
      ui_out_text (uiout, "Value returned is ");
      ui_out_field_fmt (uiout, "gdb-result-var", "$%d", record_latest_value (value));
      ui_out_text (uiout, "= ");
      value_print (value, stb->stream, 0, Val_no_prettyprint);
      ui_out_field_stream (uiout, "return-value", stb);
      ui_out_text (uiout, "\n");
#else /* UI_OUT */
      printf_filtered ("Value returned is $%d = ", record_latest_value (value));
      value_print (value, gdb_stdout, 0, Val_no_prettyprint);
      printf_filtered ("\n");
#endif /* UI_OUT */
    }
  else
    {
      /* We cannot determine the contents of the structure because
	 it is on the stack, and we don't know where, since we did not
	 initiate the call, as opposed to the call_function_by_hand case */
#ifdef VALUE_RETURNED_FROM_STACK
      value = 0;
#ifdef UI_OUT
      ui_out_text (uiout, "Value returned has type: ");
      ui_out_field_string (uiout, "return-type", TYPE_NAME (value_type));
      ui_out_text (uiout, ".");
      ui_out_text (uiout, " Cannot determine contents\n");
#else /* UI_OUT */
      printf_filtered ("Value returned has type: %s.", TYPE_NAME (value_type));
      printf_filtered (" Cannot determine contents\n");
#endif /* UI_OUT */
#else
      value = value_being_returned (value_type, stop_registers, structure_return);
#ifdef UI_OUT
      stb = ui_out_stream_new (uiout);
      ui_out_text (uiout, "Value returned is ");
      ui_out_field_fmt (uiout, "gdb-result-var", "$%d", record_latest_value (value));
      ui_out_text (uiout, "= ");
      value_print (value, stb->stream, 0, Val_no_prettyprint);
      ui_out_field_stream (uiout, "return-value", stb);
      ui_out_text (uiout, "\n");
#else
      printf_filtered ("Value returned is $%d = ", record_latest_value (value));
      value_print (value, gdb_stdout, 0, Val_no_prettyprint);
      printf_filtered ("\n");
#endif
#endif
    }
}

/* Stuff that needs to be done by the finish command after the target
   has stopped.  In asynchronous mode, we wait for the target to stop in
   the call to poll or select in the event loop, so it is impossible to
   do all the stuff as part of the finish_command function itself. The
   only chance we have to complete this command is in
   fetch_inferior_event, which is called by the event loop as soon as it
   detects that the target has stopped. This function is called via the
   cmd_continaution pointer. */
static void
finish_command_continuation (struct continuation_arg *arg)
{
  register struct symbol *function;
  struct breakpoint *breakpoint;
  struct cleanup *cleanups;

  breakpoint = (struct breakpoint *) arg->data.pointer;
  function   = (struct symbol *)     arg->next->data.pointer;
  cleanups   = (struct cleanup *)    arg->next->next->data.pointer;

  if (bpstat_find_breakpoint (stop_bpstat, breakpoint) != NULL
      && function != 0)
    {
      struct type *value_type;
      CORE_ADDR funcaddr;
      int struct_return;

      value_type = TYPE_TARGET_TYPE (SYMBOL_TYPE (function));
      if (!value_type)
	internal_error ("finish_command: function has no target type");

      if (TYPE_CODE (value_type) == TYPE_CODE_VOID)
	{
	  do_exec_cleanups (cleanups);
	  return;
	}

      funcaddr = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (function)));

      struct_return = using_struct_return (value_of_variable (function, NULL),
					   funcaddr,
					   check_typedef (value_type),
					   BLOCK_GCC_COMPILED (SYMBOL_BLOCK_VALUE (function)));

      print_return_value (struct_return, value_type); 
    }
  do_exec_cleanups (cleanups);
}

/* "finish": Set a temporary breakpoint at the place
   the selected frame will return to, then continue.  */

static void
finish_command (char *arg, int from_tty)
{
  struct symtab_and_line sal;
  register struct frame_info *frame;
  register struct symbol *function;
  struct breakpoint *breakpoint;
  struct cleanup *old_chain;
  struct continuation_arg *arg1, *arg2, *arg3;
  int selected_frame_was_inlined = 0;

  int async_exec = 0;

  /* Find out whether we must run in the background. */
  if (arg != NULL)
    async_exec = strip_bg_char (&arg);

  /* If we must run in the background, but the target can't do it,
     error out. */
  if (event_loop_p && async_exec && !target_can_async_p ())
    error ("Asynchronous execution not supported on this target.");

  /* If we are not asked to run in the bg, then prepare to run in the
     foreground, synchronously. */
  if (event_loop_p && !async_exec && target_can_async_p ())
    {
      /* Simulate synchronous execution */
      async_disable_stdin ();
    }

#ifdef HPPA_FIX_AND_CONTINUE
  check_unapplied_changes();
#endif

  if (arg)
    error ("The \"finish\" command does not take any arguments.");
  if (!target_has_execution)
    error ("The program is not running.");
  if (selected_frame == NULL)
    error ("No selected frame.");

  frame = get_prev_frame (selected_frame);
#ifdef COVARIANT_SUPPORT
   /*    	Co-variant return type  
          If the "caller frame" pc happens to be in outbound 
       or inbound outbound thunk, skip this "thunk" frame.
       By this way, we can directly go back to the caller of thunk 
       when a user does a "finish" at the Co-variant function.
    */
  if (frame ? IS_PC_IN_COVARIANT_THUNK (frame->pc) : NULL) 
    {
      frame = get_prev_frame (frame);
    }
#endif
  if (frame == 0)
    error ("\"finish\" not meaningful in the outermost frame.");

  clear_proceed_status ();

  if (inline_debugging)
    {
      if (selected_frame->inline_idx)
        {
            /* If the selected frame corresponds to an inlined function,
               frame->pc is really the context ref PC which points to the
               call site rather than the return address. So we need to peek
               at the inline table to retrieve the return address which we
               can compute to be the high_pc of inlinee + 4.

                  -- Baskar & Srikanth Jul 15th 2005 
            */
            frame->pc = get_inline_return_pc (selected_frame->inline_idx);
            selected_frame_was_inlined = true;
         }

      /* Get the return address sal that corresponds to caller, we need
         to be picky here since there could be PC overlaps between the
         return address and another inline instance there as in
     
                    foo();
                    foo();

         We don't want to finish the first foo, only to automagically land
         in the second foo !!
      */
      sal = find_pc_inline_idx_line (frame->pc, frame->inline_idx, 0);

      /* sal.pc is now left pointing to the start address of the line, we
         don't want that.
      */
      sal.pc = frame->pc;

      /* If this function is called by an inlined function, then sal at
         this points contains the start address of the callsite line and
         not the return PC !!!

         So we need to override the value computed there if we are 
         finishing an out of line function to return to the inlined caller.

         We don't need this override if we are returning from one out of line
         function to another since frame->pc contains the return PC already.
      */
      if (!selected_frame->inline_idx && frame->inline_idx)
        frame->pc = sal.pc = FRAMELESS_FUNCTION_INVOCATION(selected_frame) ?
                               SAVED_PC_AFTER_CALL (selected_frame) :
	                       FRAME_SAVED_PC (selected_frame);
    }
  else
    {
      sal = find_pc_line (frame->pc, 0);   /* No magic here */
      sal.pc = frame->pc;
    }

  breakpoint = set_momentary_breakpoint (sal, frame, bp_finish);

  if (!event_loop_p || !target_can_async_p ())
    old_chain = make_cleanup_delete_breakpoint (breakpoint);
  else
    old_chain = make_exec_cleanup_delete_breakpoint (breakpoint);

  /* Find the function we will return from.  */

  function = find_pc_function (selected_frame->pc);

  /* Print info on the selected frame, including level number
     but not source.  */
  if (from_tty)
    {
      printf_filtered ("Run till exit from ");
      print_stack_frame (selected_frame, selected_frame_level, 0);
    }

  /* If running asynchronously and the target support asynchronous
     execution, set things up for the rest of the finish command to be
     completed later on, when gdb has detected that the target has
     stopped, in fetch_inferior_event. */
  if (event_loop_p && target_can_async_p ())
    {
      arg1 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg2 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg3 =
	(struct continuation_arg *) xmalloc (sizeof (struct continuation_arg));
      arg1->next = arg2;
      arg2->next = arg3;
      arg3->next = NULL;
      arg1->data.pointer = breakpoint;
      arg2->data.pointer = function;
      arg3->data.pointer = old_chain;
      add_continuation (finish_command_continuation, arg1);
    }

  proceed_to_finish = 1;	/* We want stop_registers, please... */
  proceed ((CORE_ADDR)(long) -1, TARGET_SIGNAL_DEFAULT, 0);

  /* Do this only if not running asynchronously or if the target
     cannot do async execution. Otherwise, complete this command when
     the target actually stops, in fetch_inferior_event. */
  if (!event_loop_p || !target_can_async_p ())
    {
      /* If we are finishing an inlined function, there is no way
         we can print the return value. There is no real return
         in the first place and the return registers don't likely
         contain any useful data. Just punt.

         Note however that we cannot use selected_frame anymore
         as it would have been trashed by calls to get_current_frame
         that would have called a thousand times by gdb already during
         ``proceed''
      */

      if (inline_debugging && selected_frame_was_inlined)
          function = 0;   /* force skip of if below */

      /* Did we stop at our breakpoint? */
      if (bpstat_find_breakpoint (stop_bpstat, breakpoint) != NULL
	  && function != 0)
	{
	  struct type *value_type;
	  CORE_ADDR funcaddr;
	  int struct_return;

	  value_type = TYPE_TARGET_TYPE (SYMBOL_TYPE (function));
	  if (!value_type)
	    internal_error ("finish_command: function has no target type");

	  /* FIXME: Shouldn't we do the cleanups before returning? */
	  if (TYPE_CODE (value_type) == TYPE_CODE_VOID)
	    return;

	  funcaddr = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (function)));

	  struct_return =
	    using_struct_return (value_of_variable (function, NULL),
				 funcaddr,
				 check_typedef (value_type),
			BLOCK_GCC_COMPILED (SYMBOL_BLOCK_VALUE (function)));

	  print_return_value (struct_return, value_type); 
	}
      do_cleanups (old_chain);
    }
}

/* ARGSUSED */
static void
program_info (char *args, int from_tty)
{
  bpstat bs = stop_bpstat;
  int num = bpstat_num (&bs);

  if (!target_has_execution)
    {
      printf_filtered ("The program being debugged is not being run.\n");
      return;
    }

  target_files_info ();
  printf_filtered ("Program stopped at %s.\n",
		   local_hex_string ((unsigned long) stop_pc));
  if (stop_step)
    printf_filtered ("It stopped after being stepped.\n");
  else if (num != 0)
    {
      /* There may be several breakpoints in the same place, so this
         isn't as strange as it seems.  */
      while (num != 0)
	{
	  if (num < 0)
	    {
	      printf_filtered ("It stopped at a breakpoint that has ");
	      printf_filtered ("since been deleted.\n");
	    }
	  else
	    printf_filtered ("It stopped at breakpoint %d.\n", num);
	  num = bpstat_num (&bs);
	}
    }
  else if (stop_signal != TARGET_SIGNAL_0)
    {
      printf_filtered ("It stopped with signal %s, %s.\n",
		       target_signal_to_name (stop_signal),
		       target_signal_to_string (stop_signal));
    }

  if (!from_tty)
    {
      printf_filtered ("Type \"info stack\" or \"info registers\" ");
      printf_filtered ("for more information.\n");
    }
}

static void
environment_info (char *var, int from_tty)
{
  if (var)
    {
      register char *val = get_in_environ (inferior_environ, var);
      if (val)
	{
	  puts_filtered (var);
	  puts_filtered (" = ");
	  puts_filtered (val);
	  puts_filtered ("\n");
	}
      else
	{
	  puts_filtered ("Environment variable \"");
	  puts_filtered (var);
	  puts_filtered ("\" not defined.\n");
	}
    }
  else
    {
      register char **vector = environ_vector (inferior_environ);
      while (*vector)
	{
	  puts_filtered (*vector++);
	  puts_filtered ("\n");
	}
    }
}

static void
set_environment_command (char *arg, int from_tty)
{
  register char *p, *val, *var;
  int nullset = 0;

  if (arg == 0)
    error_no_arg ("environment variable and value");

  /* Find seperation between variable name and value */
  p = (char *) strchr (arg, '=');
  val = (char *) strchr (arg, ' ');

  if (p != 0 && val != 0)
    {
      /* We have both a space and an equals.  If the space is before the
         equals, walk forward over the spaces til we see a nonspace 
         (possibly the equals). */
      if (p > val)
	while (*val == ' ')
	  val++;

      /* Now if the = is after the char following the spaces,
         take the char following the spaces.  */
      if (p > val)
	p = val - 1;
    }
  else if (val != 0 && p == 0)
    p = val;

  if (p == arg)
    error_no_arg ("environment variable to set");

  if (p == 0 || p[1] == 0)
    {
      nullset = 1;
      if (p == 0)
	p = arg + strlen (arg);	/* So that savestring below will work */
    }
  else
    {
      /* Not setting variable value to null */
      val = p + 1;
      while (*val == ' ' || *val == '\t')
	val++;
    }

  while (p != arg && (p[-1] == ' ' || p[-1] == '\t'))
    p--;

  var = savestring (arg, (int) (p - arg));
  if (nullset)
    {
      printf_filtered ("Setting environment variable ");
      printf_filtered ("\"%s\" to null value.\n", var);
      set_in_environ (inferior_environ, var, "");
    }
  else
    set_in_environ (inferior_environ, var, val);
  free (var);
}

static void
unset_environment_command (char *var, int from_tty)
{
  if (var == 0)
    {
      /* If there is no argument, delete all environment variables.
         Ask for confirmation if reading from the terminal.  */
      if (!from_tty || query ("Delete all environment variables? "))
	{
	  free_environ (inferior_environ);
	  inferior_environ = make_environ ();
	}
    }
  else
    unset_in_environ (inferior_environ, var);
}

/* Handle the execution path (PATH variable) */

static const char path_var_name[] = "PATH";

/* ARGSUSED */
static void
path_info (char *args,
           int from_tty)
{
  puts_filtered ("Executable and object file path: ");
  puts_filtered (get_in_environ (inferior_environ, path_var_name));
  puts_filtered ("\n");
}

/* Add zero or more directories to the front of the execution path.  */

static void
path_command (char *dirname,
              int from_tty)
{
  char *exec_path;
  char *env;
  dont_repeat ();
  env = get_in_environ (inferior_environ, path_var_name);
  /* Can be null if path is not set */
  if (!env)
    env = "";
  exec_path = strsave (env);
  mod_path (dirname, &exec_path);
  set_in_environ (inferior_environ, path_var_name, exec_path);
  free (exec_path);
  if (from_tty)
    path_info ((char *) NULL, from_tty);
}


#ifdef REGISTER_NAMES
char *gdb_register_names[] = REGISTER_NAMES;
#endif
/* Print out the machine register regnum. If regnum is -1,
   print all registers (fpregs == 1) or all non-float registers
   (fpregs == 0).

   For most machines, having all_registers_info() print the
   register(s) one per line is good enough. If a different format
   is required, (eg, for MIPS or Pyramid 90x, which both have
   lots of regs), or there is an existing convention for showing
   all the registers, define the macro DO_REGISTERS_INFO(regnum, fp)
   to provide that format.  */

#if !defined (DO_REGISTERS_INFO)

#define DO_REGISTERS_INFO(regnum, fp) do_registers_info(regnum, fp)


static void
do_registers_info (int regnum,
                   int fpregs)
{
  register int i;
  int numregs = ARCH_NUM_REGS;

  for (i = 0; i < numregs; i++)
    {
      char raw_buffer[MAX_REGISTER_RAW_SIZE];
      char virtual_buffer[MAX_REGISTER_VIRTUAL_SIZE];

      /* Decide between printing all regs, nonfloat regs, or specific reg.  */
      if (regnum == -1)
	{
	  if (TYPE_CODE (REGISTER_VIRTUAL_TYPE (i)) == TYPE_CODE_FLT && !fpregs)
	    continue;
	}
      else
	{
	  if (i != regnum)
	    continue;
	}

      /* If the register name is empty, it is undefined for this
         processor, so don't display anything.  */
      if (REGISTER_NAME (i) == NULL || *(REGISTER_NAME (i)) == '\0')
	continue;

      fputs_filtered (REGISTER_NAME (i), gdb_stdout);
      print_spaces_filtered (15 - strlen (REGISTER_NAME (i)), gdb_stdout);

      /* Get the data in raw format.  */
      if (read_relative_register_raw_bytes (i, raw_buffer))
	{
	  printf_filtered ("*value not available*\n");
	  continue;
	}

      /* Convert raw data to virtual format if necessary.  */
      if (REGISTER_CONVERTIBLE (i))
	{
	  REGISTER_CONVERT_TO_VIRTUAL (i, REGISTER_VIRTUAL_TYPE (i),
				       raw_buffer, virtual_buffer);
	}
      else
	{
	  memcpy (virtual_buffer, raw_buffer,
		  REGISTER_VIRTUAL_SIZE (i));
	}

      /* If virtual format is floating, print it that way, and in raw hex.  */
      if (TYPE_CODE (REGISTER_VIRTUAL_TYPE (i)) == TYPE_CODE_FLT)
	{
	  register int j;

#ifdef INVALID_FLOAT
	  if (INVALID_FLOAT (virtual_buffer, REGISTER_VIRTUAL_SIZE (i)))
	    printf_filtered ("<invalid float>");
	  else
#endif
	    val_print (REGISTER_VIRTUAL_TYPE (i), virtual_buffer, 0, 0,
		       gdb_stdout, 0, 1, 0, Val_pretty_default);

	  printf_filtered ("\t(raw 0x");
	  for (j = 0; j < REGISTER_RAW_SIZE (i); j++)
	    {
	      register int idx = TARGET_BYTE_ORDER == BIG_ENDIAN ? j
	      : REGISTER_RAW_SIZE (i) - 1 - j;
	      printf_filtered ("%02x", (unsigned char) raw_buffer[idx]);
	    }
	  printf_filtered (")");
	}

/* FIXME!  val_print probably can handle all of these cases now...  */

      /* Else if virtual format is too long for printf,
         print in hex a byte at a time.  */
      else if (REGISTER_VIRTUAL_SIZE (i) > (int) sizeof (long))
	{
	  register int j;
	  printf_filtered ("0x");
	  for (j = 0; j < REGISTER_VIRTUAL_SIZE (i); j++)
	    printf_filtered ("%02x", (unsigned char) virtual_buffer[j]);
	}
      /* Else print as integer in hex and in decimal.  */
      else
	{
	  val_print (REGISTER_VIRTUAL_TYPE (i), virtual_buffer, 0, 0,
		     gdb_stdout, 'x', 1, 0, Val_pretty_default);
	  printf_filtered ("\t");
	  val_print (REGISTER_VIRTUAL_TYPE (i), virtual_buffer, 0, 0,
		     gdb_stdout, 0, 1, 0, Val_pretty_default);
	}

      /* The SPARC wants to print even-numbered float regs as doubles
         in addition to printing them as floats.  */
#ifdef PRINT_REGISTER_HOOK
      PRINT_REGISTER_HOOK (i);
#endif

      printf_filtered ("\n");
    }
}
#endif /* no DO_REGISTERS_INFO.  */

void
registers_info (char *addr_exp,
                int fpregs)
{
  int regnum, numregs;
  register char *end;

  if (!target_has_registers)
    error ("The program has no registers now.");
  if (selected_frame == NULL)
    error ("No selected frame.");

  if (!addr_exp)
    {
      DO_REGISTERS_INFO (-1, fpregs);
      return;
    }

  do
    {
      if (addr_exp[0] == '$')
	addr_exp++;
      end = addr_exp;
      while (*end != '\0' && *end != ' ' && *end != '\t')
	++end;
#ifdef HP_IA64
      if (selected_frame->pa_save_state_ptr)
        {
          /* Mixed mode frame. */
          numregs = is_swizzled ? PA32_NUM_REGS: PA64_NUM_REGS;
        }
      else
#endif
      numregs = ARCH_NUM_REGS;

      regnum = target_map_name_to_register (addr_exp, (int)(end - addr_exp));
      if (regnum >= 0)
	goto found;

      regnum = numregs;

      if (*addr_exp >= '0' && *addr_exp <= '9')
	regnum = atoi (addr_exp);	/* Take a number */
      if (regnum >= numregs)	/* Bad name, or bad number */
	error ("%.*s: invalid register", end - addr_exp, addr_exp);

    found:
      DO_REGISTERS_INFO (regnum, fpregs);

      addr_exp = end;
      while (*addr_exp == ' ' || *addr_exp == '\t')
	++addr_exp;
    }
  while (*addr_exp != '\0');
}

void
all_registers_info (char *addr_exp,
                    int from_tty)
{
  registers_info (addr_exp, 1);
}

void
nofp_registers_info (char *addr_exp, int from_tty)
{
  registers_info (addr_exp, 0);
}


#ifdef HP_IA64
/* jini: QXCR1000876000: gdb-5.9 hangs on attaching to PA-RISC
   processes on Integrity with -pid. 
   Use xmode_launch_*bit_gdb to launch the appropriate PA gdb
   to attach to the PA-RISC process.
*/
void
check_and_launch_pa_gdb (char *filename, char *args)
{
  char *gdb_exec = NULL ;
  int arg_cnt = 0;
  char *argv[10] = { NULL };
  char *pid_tmp = NULL;

  if (xmode_exec_format == exec_pa32)
    {
      gdb_exec = (char *) xmalloc (strlen("gdbpa") + 1);
      strcpy (gdb_exec, "gdbpa");
    }
  else if (xmode_exec_format == exec_pa64)
    {
      gdb_exec = (char *) xmalloc (strlen("gdb64") + 1);
      strcpy (gdb_exec, "gdb64");
    }
  argv[arg_cnt++] = gdb_exec;

  if (annotation_level > 1)
    {
      char *annotate_tmp = (char *) xmalloc (sizeof ("--annotate=2") + 1);
      strcpy (annotate_tmp, "--annotate=2");
      argv[arg_cnt++] = annotate_tmp;
    }
  
  argv[arg_cnt++] = filename;

  pid_tmp = strdup (args); /* inferior pid */
  argv[arg_cnt++] = pid_tmp;
  if (xmode_exec_format == exec_pa32)
    xmode_launch_32bit_gdb(argv, "process");
  else
    xmode_launch_64bit_gdb(argv, "process");

  /* Freeing up stuff incase we error out and don't exec. */
  for (int i = 0; i < arg_cnt; i++)
    {
      free (argv[i]);
    }
}
#endif


/* JAGag06697 - Function pointer definition. */
int (*getcommandline_ptr) (char *buf, 
			   size_t elemsize, 
			   size_t elemcnt, 
			   pid_t pid);

/*
 * TODO:
 * Should save/restore the tty state since it might be that the
 * program to be debugged was started on this tty and it wants
 * the tty in some state other than what we want.  If it's running
 * on another terminal or without a terminal, then saving and
 * restoring the tty state is a harmless no-op.
 * This only needs to be done if we are attaching to a process.
 */

/*
   attach_command --
   takes a program started up outside of gdb and ``attaches'' to it.
   This stops it cold in its tracks and allows us to start debugging it.
   and wait for the trace-trap that results from attaching.  */

void
attach_command (char *args, int from_tty)
{
  extern char* get_inferior_cwd();
  extern boolean is_reinit_frame_cache;
#ifdef SOLIB_ADD
  extern int auto_solib_add;
#endif

#ifdef ATTACH_NO_WAIT
  extern int stop_print_frame;
#endif

  char *exec_file = NULL, *s1;
  char *full_exec_path = NULL;

  /* Baskar JAGae40701 */
  /* NEC(JAGaf79121) - To fix memory leak ( added '= 0') */
  char *filename = 0; /* executable - fully qualified name */
  struct pst_status buf;
  char *pid_tmp, *annotate_tmp;
  char *gdb_exec;
  char *argv[10] = { NULL };
  int arg_cnt = 0;
  struct utsname uts;
  boolean pstat_getcommandline_available = false;
  int temp_pid = 0;
  struct stat stat_buf;

  char cmdline[MAXPATHLEN + 1];
  getcommandline_ptr = NULL;

  /* Carl Burch/Sunil S JAGag06697
   1) Run uname and check if we are on a system where pstat_getcommandline () 
   system call exists. 
   2) Use shl_findsym() to set a function pointer to call 
   pstat_getcommandline() for the PA code. 
   It's cleaner to just use the function pointer to call 
   pstat_getcommandline() in the IPF code as well. */
#ifdef GDB_TARGET_IS_HPPA
  int sys_ver = 0;
  uname (&uts);
  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
    error ("uname: Couldn't resolve the release identifier of the "
           "Operating system.");
  if (sys_ver >= 23) /* This will work for 11.23, 11.31 and 11.41 */
    {
      struct shl_descriptor *desc;
      int index = 1;
      while (shl_get (index, &desc) != -1)
        {
          int shl_stat = 0;
          index++;
          if (strstr (desc->filename, "/libc."))
            {
              shl_stat = shl_findsym (&desc->handle, "pstat_getcommandline",
                                      TYPE_PROCEDURE, &getcommandline_ptr);
              if (!shl_stat)
		{
      		  pstat_getcommandline_available = true;
	          break;
		}
	    }
        }
    }
#else
  getcommandline_ptr = pstat_getcommandline;
  pstat_getcommandline_available = true;
#endif

  /* defined in source.c */
  extern int absolute_path_of (char *, char **);

  /* NEC(JAGaf79121) - To fix memory leak */
  objfile_purge_solibs();
  dont_repeat ();		/* Not for the faint of heart */

  if (batch_rtc || profile_on)
    from_tty = 0;

  if (target_has_execution)
    {
      if (query ("A program is being debugged already.  Kill it? "))
	target_kill ();
      else
	error ("Not killed.");
    }

  invalidate_shadow_contents ();

  /* Baskar JAGae40701 - gdb unable to attach 64-bit process 
     - Find the target executable name
     - Construct the fully qualified pathname
     - Check the executable to determine if the executable is in a
       different format and call the approriate gdb after building
       the argv
  */

#ifdef HP_XMODE
  {
    if (args)
      sscanf (args, "%d", &temp_pid);

    /* First try "get_exec_file()" from bfd, If it fails then try 
       pstat_getproc(). This change is needed because pstat_getproc()
       most of the times does not return full path of executable and 
       some time it may be null. So don't depend on pstat_getproc()
       to get correct full path name of the executable. 
    */ 
    exec_file = get_exec_file(0);   
   
    if (!exec_file)
     { 
       if (getcommandline_ptr && pstat_getcommandline_available)
        {
          if (getcommandline_ptr (cmdline, sizeof(cmdline), 1, temp_pid) != -1)
           {
              DEBUG(printf("Attach to -->> %s pid=%d\n", cmdline, temp_pid));
              exec_file = cmdline;
              /* Truncate the exec_file string to the first space/tab. */
              s1 = exec_file;
              while ( *s1 != '\0' && *s1 != ' ' && *s1 != '\t' )
                s1++;
              *s1 = 0;
           }
        }
      else
       if (pstat_getproc (&buf, sizeof (struct pst_status), 0, temp_pid) != -1)
        {
          exec_file = buf.pst_cmd;
	  /* JAGaf55033.*/
	  /* pst_cmd would have the executable pathname + arguments .*/
	  /* The below check is duplicated later. To be rewritten as part 
           of code clean up.*/
	  /* Truncate the exec_file string to the first space/tab. */
	  s1 = exec_file;
	  while ( *s1 != '\0' && *s1 != ' ' && *s1 != '\t' )
	   s1++;
	  *s1 = 0;
         /* We do not want the below check, if the exec_file is known.*/
	 if (!get_exec_file(0))
           {
             /* On 11.00 pst_cmd has only 60 chars and on 11.11 it has 63 chs.*/
             if (strlen (exec_file) >= 60 )
               /* No harm expect the worst.*/
               {
                 char *command_line = strdup (exec_file);
                 if(command_line && *command_line) 
                   {
                     char *base = 0;
                     base = basename(command_line);
                     if ( strcmp (base,buf.pst_ucomm) != 0 )
                       {
                         free (command_line);
                         error ("Could not find executable.\nTry invoking gdb "
                                "as \"gdb <full_path_of_executable> <pid>\".");
                       }
                     free (command_line);
                   }
               }
           }
        }
     }


    if (!absolute_path_of (exec_file, &full_exec_path))
      full_exec_path = savestring (exec_file, (int) strlen (exec_file));

    filename = tilde_expand (full_exec_path);

    if (xmode_this_gdb_understands == xmode_pa32)
      {
        if (xmode_exec_format_is_different (filename))
          {
            /* build argv */
            /* JAGae35325 - if underlying machine is IA and process is IA */
            if (is_IA_machine)
              {
                gdb_exec = (char *) xmalloc (strlen("gdb") + 1);
                strcpy (gdb_exec, "gdb");
              }
            else
              {
                gdb_exec = (char *) xmalloc (strlen("gdb64") + 1);
                strcpy (gdb_exec, "gdb64");
              }
            argv[arg_cnt++] = gdb_exec;
            
            if (annotation_level > 1)
              {
                annotate_tmp = (char *) xmalloc (sizeof ("--annotate=2") + 1);
                strcpy (annotate_tmp, "--annotate=2");
                argv[arg_cnt++] = annotate_tmp;
              }
		  
            argv[arg_cnt++] = filename;
		
            pid_tmp = strdup (args); /* inferior pid */
            argv[arg_cnt++] = pid_tmp;
            if (is_IA_machine)
              xmode_launch_IA_gdb (argv, "process");
            else	
              xmode_launch_64bit_gdb (argv, "process");
          }
      }
    else if (xmode_this_gdb_understands == xmode_pa64)
      {
        if (xmode_exec_format_is_different (filename))
          {
            /* build argv */
            /* JAGae35325 - if underlying machine is IA and process is IA */
            if (is_IA_machine)
              {
                gdb_exec = (char *) xmalloc (strlen("gdb") + 1);
                strcpy (gdb_exec, "gdb");
              }
            else
              {
                gdb_exec = (char *) xmalloc (strlen("gdb32") + 1);
                strcpy (gdb_exec, "gdb32");
              }
            argv[arg_cnt++] = gdb_exec;

            if (annotation_level > 1)
              {
                annotate_tmp = (char *) xmalloc (sizeof ("--annotate=2") + 1);
                strcpy (annotate_tmp, "--annotate=2");
                argv[arg_cnt++] = annotate_tmp;
              }
            
            argv[arg_cnt++] = filename;
            
            pid_tmp = strdup (args); /* inferior pid */
            argv[arg_cnt++] = pid_tmp;
            if (is_IA_machine)
              xmode_launch_IA_gdb (argv, "process");
            else  
              xmode_launch_32bit_gdb (argv, "process");
          }
      }
#ifdef HP_IA64
    /* jini: QXCR1000876000: gdb-5.9 hangs on attaching to PA-RISC
       processes on Integrity with -pid. 
       Addressing the case of executing 'attach' command from the gdb prompt.
     */
    else if (   xmode_this_gdb_understands == xmode_ia64
             && xmode_exec_format_is_different (filename)
	     && !debugging_aries && !debugging_aries64)
      {
         check_and_launch_pa_gdb (filename, args);
      }
#endif /* HP_IA64 */
  }
#endif /* HP_XMODE */

  target_attach (args, from_tty);

  /*
   * If no exec file is yet known, try to determine it from the
   * process itself. We want to do this before attaching. See JAGae35323.
   */
  exec_file = (char *) get_exec_file (0);
  if (!exec_file && args)
    {
      int getproc_status = 0;
      sscanf (args, "%d", &temp_pid);
      
      getproc_status = 
        pstat_getproc (&buf, sizeof(struct pst_status), 0, temp_pid);

#ifdef HP_IA64
      if (getproc_status != -1)
        {
          if (buf.pst_flag & PS_64ASL)
            is_swizzled = FALSE;
          else
            is_swizzled = TRUE;
        }
#endif

      if (getcommandline_ptr && pstat_getcommandline_available)
	{
          if (getcommandline_ptr (cmdline, sizeof(cmdline), 1, temp_pid) != -1)
            {
              DEBUG(printf("Attach to --->> %s pid = %d\n", cmdline, 
                           temp_pid));
              exec_file = cmdline;
              /* Truncate the exec_file string to the first space/tab. */
              s1 = exec_file;
              while ( *s1 != '\0' && *s1 != ' ' && *s1 != '\t' )
                s1++;
              *s1 = 0;
	    }
	}
      else if (getproc_status != -1)
        {
          exec_file = buf.pst_cmd; /* executable pathname */
          /* Truncate the exec_file string to the first space/tab. */
          s1 = exec_file;
          while ( *s1 != '\0' && *s1 != ' ' && *s1 != '\t' )
            s1++;
          *s1 = 0;
          if(!get_exec_file(0))
            {
              if (strlen (exec_file) >= 60 )
                {
                  char *command_line = strdup (exec_file);
                  if(command_line && *command_line) 
                    {
                      char *base = 0;
                      base = basename(command_line);
                      if ( strcmp (base,buf.pst_ucomm) != 0 )
                        {
                          free (command_line);
                          error ("Could not find executable.\nTry invoking "
                            "gdb as \"gdb <full_path_of_executable> <pid>\".");
                        }
                      free (command_line);
                    }
                }
            }
	}

      /* NEC(JAGaf79121) - To fix memory leak */
      if (full_exec_path) 
        { 
          free(full_exec_path); 
          full_exec_path = NULL; 
        }

      /* NEC(JAGaf79121) - To fix memory leak */
      if (filename) 
        { 
          free(filename); 
          filename = NULL; 
        }

      /* It's possible we don't have a full path, but rather just a
         filename.  Some targets, such as HP-UX, don't provide the
         full path, sigh.
         
         First, try to see if the executable is in $PWD of the
         process we attaching to. If it fails, attempt to qualify 
         the filename against the source path.
         (If that fails, we'll just fall back on the original
         filename.  Not much more we can do...) */
      if (exec_file[0] != '/')
        {
          /* If this is not a full path name, try to figure it out
             by reading the cwd of the inferior */
          char* cwd_path = get_inferior_cwd ();
          if (cwd_path)
            {
              char tmp_fname[MAXPATHLEN];
              strcpy (tmp_fname, cwd_path);
              strcat (tmp_fname, "/");
              strcat (tmp_fname, exec_file);
              if (stat (tmp_fname, &stat_buf) == 0)
                full_exec_path = 
                  savestring (tmp_fname, (int) strlen (tmp_fname));
            }
        }

      if (full_exec_path == NULL)
        {
          if (!absolute_path_of (exec_file, &full_exec_path))
            full_exec_path = savestring (exec_file, (int) strlen (exec_file));
        }
      
#ifdef HP_IA64
     /* jini: QXCR1000876000: gdb-5.9 hangs on attaching to PA-RISC
        processes on Integrity with -pid. 
        Addressing the case of executing 'attach' command from the gdb prompt
        where the application is launched from another directory.
      */
      if (   xmode_this_gdb_understands == xmode_ia64
          && xmode_exec_format_is_different (full_exec_path)
	  && !debugging_aries && !debugging_aries64)
        {
          check_and_launch_pa_gdb (full_exec_path, args);
        }
#endif
      exec_file_attach (full_exec_path, from_tty);

      /* We can't call reinit_frame_cache() until shared library info is 
         loaded. Disable it here. It will be called later from normal_stop() */
      is_reinit_frame_cache = 0;
      symbol_file_command (full_exec_path, from_tty);
      is_reinit_frame_cache = 1;
    }

  /* NEC(JAGaf79121) - To fix memory leak */
  if (full_exec_path) free(full_exec_path);
  /* NEC(JAGaf79121) - To fix memory leak */
  if (filename) { free(filename); filename = 0; }

  /* Set up the "saved terminal modes" of the inferior
     based on what modes we are starting it with.  */
  target_terminal_init ();

  /* Install inferior's terminal modes.  */
  target_terminal_inferior ();

  /* Set up execution context to know that we should return from
     wait_for_inferior as soon as the target reports a stop.  */
  init_wait_for_inferior ();
  clear_proceed_status ();
  stop_soon_quietly = 1;

  /* No traps are generated when attaching to inferior under Mach 3
     or GNU hurd.  */

  /* srikanth, 000719, Also on HP-UX 11.00, ttrace TT_PROC_ATTACH
     should not be followed by a ttrace_wait. The process is already
     stopped, we don't have to wait for it to stop. Simply print
     the frame.
  */           
#ifndef ATTACH_NO_WAIT
  wait_for_inferior ();
#else
 if (!profile_on)
   stop_print_frame = 1; 
#endif

#ifdef SOLIB_CREATE_INFERIOR_HOOK
  SOLIB_CREATE_INFERIOR_HOOK (inferior_pid);
#endif

#ifdef SOLIB_ADD
  if (auto_solib_add)
    {
      /* Add shared library symbols from the newly attached process, if any. */
      SOLIB_ADD ((char *) 0, 0, &current_target);
      re_enable_breakpoints_in_shlibs ();
    }
#endif

/* JAGaf37695 - Reset all the globals to prepare for runtime-checking 
   after loading the shared libraries */
#ifdef RTC
  /* Reset all globals to prepare for thread tracing */
  prepare_for_thread_tracing(1);  /* attached to the process */
  /* Reset all the globals to prepare for runtime checking */
  prepare_for_rtc (1);  /* attached to the process */
#endif

/* Take any necessary post-attaching actions for this platform.
   */
  target_post_attach (inferior_pid);

  /* enable rtc after attach */
#ifdef RTC
  snoop_on_the_heap ();
#endif /* RTC */

  if (profile_on)
    initiate_profiling_after_attach ();

  normal_stop ();

  if (attach_hook)
    attach_hook ();
}

/*
 * detach_command --
 * takes a program previously attached to and detaches it.
 * The program resumes execution and will no longer stop
 * on signals, etc.  We better not have left any breakpoints
 * in the program or it'll die when it hits one.  For this
 * to work, it may be necessary for the process to have been
 * previously attached.  It *might* work if the program was
 * started via the normal ptrace (PTRACE_TRACEME).
 */

static void
detach_command (char *args, int from_tty)
{
  dont_repeat ();		/* Not for the faint of heart */
  target_detach (args, from_tty);
#if defined(SOLIB_RESTART)
  SOLIB_RESTART ();
#endif
  if (detach_hook)
    detach_hook ();
}

/* Stop the execution of the target while running in async mode, in
   the backgound. */
#ifdef UI_OUT
void
interrupt_target_command_wrapper (char *args, int from_tty)
{
  interrupt_target_command (args, from_tty);
}
#endif
static void
interrupt_target_command (char *args,
                          int from_tty)
{
  if (event_loop_p && target_can_async_p ())
    {
      dont_repeat ();		/* Not for the faint of heart */
      target_stop ();
    }
}

/* ARGSUSED */
static void
float_info (char *addr_exp, int from_tty)
{
#ifdef FLOAT_INFO
  FLOAT_INFO;
#else
  printf_filtered ("No floating point info available for this processor.\n");
#endif
}

/* ARGSUSED */
static void
unset_command (char *args,
               int from_tty)
{
  printf_filtered ("\"unset\" must be followed by the name of ");
  printf_filtered ("an unset subcommand.\n");
  help_list (unsetlist, "unset ", -1, gdb_stdout);
}

#ifdef HPPA_FIX_AND_CONTINUE
static void
fix_command (char *args,
             int from_tty)
{
  char *tok, *end_tok, *source;
  int toklen;
  boolean found = false;
  extern void fix_file (char *);
  extern void fix_pc  ();

  if (inferior_pid == 0)
    error ("Fix: %s hasn't been run yet.\nFix Failed.\n", symfile_objfile->name);

#ifdef LOG_BETA_FIX_COMMAND
  log_test_event (LOG_BETA_FIX_COMMAND, 1);
#endif

  if (args == NULL)
    args = current_source_symtab->filename;

  tok = end_tok = args;
  while (*end_tok != '\0')
    {
      while (*tok == ' ' || *tok == '\t')
        tok++;
      end_tok = tok;

      while (*end_tok != ' ' && *end_tok != '\0' && *end_tok != '\t')
        end_tok++;

      toklen = end_tok - tok;

      if (toklen >= 1)
        {
          source = (char *)alloca (toklen + 1);
          strncpy (source, tok, toklen + 1);
          source[toklen] = '\0';
          tok = end_tok;
          fix_file(source);
        }
    }
  /* Move the old pc to the pc correspoding to the original line. */
  fix_pc();
  printf_filtered ("Fix succeeded.\n");
}
#endif

void
_initialize_infcmd ()
{
  struct cmd_list_element *c;

  add_com ("tty", class_run, tty_command,
	   "Set terminal for future runs of program being debugged.\n\n\
Usage:\n\ttty <TERMINAL>\n\n");

  add_show_from_set
    (add_set_cmd ("args", class_run, var_string_noescape,
		  (char *) &inferior_args,
		  "Set argument list to give program being debugged when \
it is started.\n\nUsage:\nTo set new value:\n\t\
set args [ARG1] [ARG2] [...] [> <OUT-FILE> | >> <OUT-FILE>] [< <IN-FILE>]\n\
To see current value:\n\tshow args\n\n\
Follow this command with any number of args, to be passed to the program.",
		  &setlist),
     &showlist);

  add_show_from_set
    (add_set_cmd ("complain-if-sigtrap-disabled", no_class, var_boolean,
                  (char *) &complain_if_sigtrap_disabled,
                "Set whether gdb will complain whenever the program blocks "
                "SIGTRAPs.\n\nUsage:\nTo set new value:\n\tset "
                "complain-if-sigtrap-disabled [on | off]\nTo see current "
                "value:\n\tshow complain-if-sigtrap-disabled\n\n"
                "This is supported only on HPUX 11.00 at present.",
                  &setlist),
     &showlist);

  add_show_from_set
    (add_set_cmd ("ignore-uninteresting-signals", no_class, var_boolean,
                  (char *) &ignore_uninteresting_signals,
                "Set whether gdb should get involved in the delivery of "
                "uninteresting signals.\n\nUsage:\nTo set new value:\n\tset"
                " ignore-uninteresting-signals [on | off]\nTo see current "
                "value:\n\tshow ignore-uninteresting-signals\n",
                  &setlist),
     &showlist);

  c = add_cmd
    ("environment", no_class, environment_info,
     "The environment to give the program, or one variable's value.\n\n\
Usage:\n\tshow environment [<VAR>]\n\n\
With an argument VAR, prints the value of environment variable VAR to\n\
give the program being debugged.  With no arguments, prints the entire\n\
environment to be given to the program.", &showlist);
  c->completer = noop_completer;

  add_prefix_cmd ("unset", no_class, unset_command,
		  "Complement to certain \"set\" commands",
		  &unsetlist, "unset ", 0, &cmdlist);

  c = add_cmd ("environment", class_run, unset_environment_command,
	       "Cancel environment variable VAR for the program.\n\n\
Usage:\n\tunset environment [VAR]\n\n\
This command does not affect the program until the next \"run\" command.",
	       &unsetlist);
  c->completer = noop_completer;

  c = add_cmd ("environment", class_run, set_environment_command,
	       "Set environment variable value to give the program.\n\n\
Usage:\n\tset environment <VAR> [<VALUE>]\n\n\
Arguments: VAR is variable name and VALUE is value.\n\
Values of environment variables are uninterpreted strings.\n\
This command does not affect the program until the next \"run\" command.",
	       &setlist);
  c->completer = noop_completer;

  add_com ("path", class_files, path_command,
	   "Add directory DIR(s) to beginning of search path for object files.\n\n\
Usage:\n\tpath [<DIR 1>] [:<DIR 2>] [: ...]\n\n\
$cwd in the path means the current working directory.\n\
This path is equivalent to the $PATH shell variable.  It is a list of\n\
directories, separated by colons.  These directories are searched to find\n\
fully linked executable files and separately compiled object files as needed.");

  c = add_cmd ("paths", no_class, path_info,
	       "Current search path for finding object files.\n\n\
Usage:\n\tshow paths\n\n\
$cwd in the path means the current working directory.\n\
This path is equivalent to the $PATH shell variable.  It is a list of\n\
directories, separated by colons.  These directories are searched to find\n\
fully linked executable files and separately compiled object files as needed.",
	       &showlist);
  c->completer = noop_completer;

  add_com ("attach", class_run, attach_command,
	   "Attach to a process or file outside of GDB.\n\n\
Usage:\n\tattach <PROCESS-ID> | <DEV-FILE>\n\n\
This command attaches to another target, of the same type as your last\n\
\"target\" command (\"info files\" will show your target stack).\n\
The command may take as argument a process id or a device file.\n\
For a process id, you must have permission to send the process a signal,\n\
and it must have the same effective uid as the debugger.\n\
When using \"attach\" with a process id, the debugger finds the\n\
program running in the process, looking first in the current working\n\
directory, or (if not found there) using the source file search path\n\
(see the \"directory\" command).  You can also use the \"file\" command\n\
to specify the program, and to load its symbol table.");

  add_com ("detach", class_run, detach_command,
	   "Detach a process or file previously attached.\n\n\
Usage:\n\tdetach\n\n\
If a process, it is no longer traced, and it continues its execution.  If\n\
you were debugging a file, the file is closed and gdb no longer accesses it.");

  add_com ("signal", class_run, signal_command,
	   "Continue program giving it signal specified by the integer argument 'N'.\n\n\
Usage:\n\tsignal <N> | 0\n\n\
An argument of \"0\" means continue program without giving it a signal.");

  add_com ("stepi", class_run, stepi_command,
	   "Step one instruction exactly.\n\n\
Usage:\n\tstepi [<N>]\n\t or\n\tsi [<N>]\n\n\
Argument N means do this N times (or till program stops for another reason).");
  add_com_alias ("si", "stepi", class_alias, 0);

  add_com ("nexti", class_run, nexti_command,
	   "Step one instruction, but proceed through subroutine calls.\n\n\
Usage:\n\tnexti [<N>]\n\t or\n\tni [<N>]\n\n\
Argument N means do this N times (or till program stops for another reason).");
  add_com_alias ("ni", "nexti", class_alias, 0);

  add_com ("finish", class_run, finish_command,
	   "Execute until selected stack frame returns.\n\n\
Usage:\n\tfinish\n\n\
Upon return, the value returned is printed and put in the value history.");

  add_com ("next", class_run, next_command,
	   "Step program, proceeding through subroutine calls.\n\n\
Usage:\n\tnext [<N>]\n\n\
Behaves like the \"step\" command as long as subroutine calls do not happen;\n\
when they do, the call is treated as one instruction.\n\
Argument N means do this N times (or till program stops for another reason).");
  add_com_alias ("n", "next", class_run, 1);
  if (xdb_commands)
    add_com_alias ("S", "next", class_run, 1);

  add_com ("step", class_run, step_command,
	   "Step program until it reaches a different source line.\n\n\
Usage:\n\tstep [<N>]\n\n\
Argument N means do this N times (or till program stops for another reason).");
  add_com_alias ("s", "step", class_run, 1);

#ifdef GDB_TARGET_IS_HPUX
  add_com ("steplast", class_run, steplast_command,
	   "steps into the function but not into calls of evaluating arguments.\n\n\
Usage:\n\tsteplast [<N>]\n\n");
  add_com_alias ("sl", "steplast", class_run, 1);
#endif

  add_com ("until", class_run, until_command,
	   "Execute until the program reaches a source line greater than the current\n\
or a specified line or address. Alias: u, until\n\n\
Usage:\n\tuntil [[<FILE>:]<LINE> | [*<ADDR>]]\n\t\
 or\n\tuntil [+<OFFSET> | -<OFFSET>]\n\n\
Execution will also stop upon exit from the current stack frame.");
  add_com_alias ("u", "until", class_run, 1);

  add_com ("jump", class_run, jump_command,
	   "Continue program being debugged at specified line number or address.\n\n\
Usage:\n\tjump [<FILE>:]<LINE> | <*ADDR>\n\n\
Give as argument either LINE or *ADDR, where ADDR is an expression\n\
for an address to start at.");

  /* srikanth, JAGad40045, go command has morphed into xdb only
     command after the Cygnus 5.0 merge. The GUI has dependencies
     on this. 001128.
  */
    add_com ("go", class_run, go_command,
	     "Continue program being debugged, stopping at specified line or address.\n\n\
Usage:\n\tgo [<FILE>:]<LINE> | <*ADDR>\n\n\
Give as argument either LINE or *ADDR, where ADDR is an expression for an address to\n\
start at. This command is a combination of tbreak and jump.");

  if (xdb_commands)
    add_com_alias ("g", "go", class_run, 1);

  add_com ("continue", class_run, continue_command,
	   "Continue program being debugged, after signal or breakpoint.\n\n\
Usage:\n\tcontinue [<N>]\n\n\
If proceeding from breakpoint, a number N may be used as an argument,\n\
which means to set the ignore count of that breakpoint to N - 1 (so that\n\
the breakpoint won't break until the Nth time it is reached).");
  add_com_alias ("c", "cont", class_run, 1);
  add_com_alias ("fg", "cont", class_run, 1);

  add_com ("run", class_run, run_command,
	   "Start debugged program.\n\n\
Usage:\n\trun [ARG1] [ARG2] [...] [> <OUT-FILE> | >> <OUT-FILE>] [< <IN-FILE>]\n\n\
You may specify arguments to give it.\n\
Args may include \"*\", or \"[...]\"; they are expanded using \"sh\".\n\
Input and output redirection with \">\", \"<\", or \">>\" are also allowed.\n\n\
With no arguments, uses arguments last specified (with \"run\" or \"set args\").\n\
To cancel previous arguments and run with no arguments,\n\
use \"set args\" without arguments.");
  add_com_alias ("r", "run", class_run, 1);


#ifdef GDB_TARGET_IS_HPPA
  add_show_from_set
  (add_set_cmd ("sigwait-in-init", class_support, var_zinteger,
                (char *) &sigwait_sig_in_init,
                "Set to see any signal that would be caught by sigwait before reaching main.\n\
By default this is set to 0 and gdb reports these signal only after reaching main.\n",
                &setlist),
   &showlist);
#endif

#ifdef HPPA_FIX_AND_CONTINUE
  add_com ("fix", class_run, fix_command,
	   "Apply fixes to the program. Arguments are file names used by the executable.");
#endif

  if (xdb_commands)
    add_com ("R", class_run, run_no_args_command,
	     "Start debugged program with no arguments.");

  add_com ("interrupt", class_run, interrupt_target_command,
	   "Interrupt the execution of the debugged program.\n\n\
Usage:\n\tinterrupt\n\n");

  add_info ("registers", nofp_registers_info,
	    "List of integer registers and their contents, for selected stack frame.\n\n\
Usage:\n\tinfo registers [<REG>]\n\n\
Register name as argument means describe only that register.");
  add_info_alias ("r", "registers", 1);

  if (xdb_commands)
    add_com ("lr", class_info, nofp_registers_info,
	     "List of integer registers and their contents, for selected stack frame.\n\
  Register name as argument means describe only that register.");
  add_info ("all-registers", all_registers_info,
	    "List of all registers and their contents, for selected stack frame.\n\n\
Usage:\n\tinfo all-registers [<REG>]\n\n\
Register name as argument means describe only that register.");

  add_info ("program", program_info,
	    "Execution status of the program.\n\nUsage:\n\tinfo program\n");

  add_info ("float", float_info,
	    "Print the status of the floating point unit\n\nUsage:\n\tinfo float\n");

  inferior_args = savestring ("", 1);	/* Initially no args */
  complain_if_sigtrap_disabled = 1;
  inferior_environ = make_environ ();
  init_environ (inferior_environ);
}


#ifdef RTC

/* Setup RTC environment */
void
setup_librtc_env()
{
  extern int check_heap_in_this_run, trace_threads_in_this_run;
  char*  override_librtc = 0;
  char*  env_ld_lib_path = 0;
  char*  env_ld_preload = 0;
  char   pthread_link_path[MAXPATHLEN+1];
  char   ld_lib_path[MAXPATHLEN+1];
  char   librtc_path[MAXPATHLEN+1];
  char   env_string[MAXPATHLEN+1];
  static int added_pthread_link_path = 0;
  static int added_librtc_preload = 0;
  char*  pos = 0;
  static char filler[] = "xxxxxx";
  static char librtc_str[] = "librtc";

  /* Make sure these are initialized as NULL strings */
  librtc_path[0] = env_string[0] = 0;

  if (gdb_root[0] != NULL) /* GDB_ROOT is defined */
    {
#ifdef HP_IA64
      strcpy (pthread_link_path, gdb_root);
      strcat (pthread_link_path, "/wdb/lib/hpux32:");
      strcat (pthread_link_path, gdb_root);
      strcat (pthread_link_path, "/wdb/lib/hpux64");
#else
#ifndef GDB_TARGET_IS_HPPA_20W
      strcpy (pthread_link_path, gdb_root);
      strcat (pthread_link_path, "/wdb/lib");
#else 
      strcpy (pthread_link_path, gdb_root);
      strcat (pthread_link_path, "/wdb/lib/pa20_64");
#endif
#endif
    }
  else
    {
#ifdef HP_IA64
      strcpy (pthread_link_path,
              "/opt/langtools/wdb/lib/hpux32:/opt/langtools/wdb/lib/hpux64");
#else
#ifndef GDB_TARGET_IS_HPPA_20W
      strcpy (pthread_link_path, "/opt/langtools/wdb/lib");
#else 
      strcpy (pthread_link_path, "/opt/langtools/wdb/lib/pa20_64");
#endif
#endif
    }

  /* When set thread-check is disabled we want to make sure we
     re-set LD_LIBRARY_PATH to it's original setting */
  if (!trace_threads_in_this_run)
    {
      set_in_environ (inferior_environ, "PTHR_TRACE_ENABLED", "off");
      if (added_pthread_link_path)
        {
          ld_lib_path[0] = 0;
          env_ld_lib_path = 
              get_in_environ(inferior_environ, "LD_LIBRARY_PATH");
          if (env_ld_lib_path) 
            {
              /* Take out pthread_link_path from LD_LIBRARY_APTH */
              char* opt_start = strstr(env_ld_lib_path, pthread_link_path);
              /* Copy whatever came before pthread_link_path */
              if (opt_start != env_ld_lib_path)
                strncpy(ld_lib_path, env_ld_lib_path, 
                        opt_start - env_ld_lib_path);
              /* Copy the rest of the string... */
              opt_start += strlen(pthread_link_path) + 1;
              /* ... if there's anything to copy */
              if (opt_start < env_ld_lib_path + strlen(env_ld_lib_path))
                strcat(ld_lib_path, opt_start);

              if (strlen(ld_lib_path) == 0)
                unset_in_environ (inferior_environ, "LD_LIBRARY_PATH");
              else
                set_in_environ (inferior_environ, "LD_LIBRARY_PATH", 
                                ld_lib_path);
            }
          added_pthread_link_path = 0;
        }
    }

  if (!check_heap_in_this_run)
    {
      set_in_environ (inferior_environ, "HEAP_CHECK_ENABLED", "off");
    }

  /* If neither heap nor thread checking is enabled in this run and we
     have previously added librtc to LD_PRELOAD, remove it now */
  if (!check_heap_in_this_run && !trace_threads_in_this_run &&
      added_librtc_preload)
    {
      env_ld_preload = get_in_environ (inferior_environ, "LD_PRELOAD");
      strcpy (env_string, env_ld_preload);
      pos = strstr (env_string, librtc_str);
      if (pos)
        {
          memcpy (pos, filler, sizeof (filler) - 1);
          pos = strstr (pos + sizeof (filler), librtc_str);
          if (pos)
            memcpy (pos, filler, sizeof (filler) - 1);
        }
      set_in_environ (inferior_environ, "LD_PRELOAD", env_string);
      added_librtc_preload = 0;
    }

  /* srikanth, 000313, export the magic environment variables to have
     librtc mapped in automatically. There are two ways to do this.
     By exporting an environment variable called LD_PRELOAD : this is
     supported only on 11.x (both 32 and 64 bits) beginning dld version
     B.11.19. If we have an earlier dld, by exporting the profiler
     environment variables. This is supported only for 11.0, 32 bits. */
  /* srikanth, Intel wants a way to specify what librtc would be
     used. They don't want to install librtc on all systems. */
  if (!check_heap_in_this_run && !trace_threads_in_this_run) return;

  /* Set LD_LIBRARY_PATH to always pick up libpthread_tr.so instead of
     libpthread.so under gdb. To make it work we will ship a link:

     IPF:
     /opt/langtools/wdb/lib/hpux32/libpthread.so -> 
                       /usr/lib/hpux32/libpthread_tr.so
     /opt/langtools/wdb/lib/hpux32/libpthread.so.1 -> 
                       /usr/lib/hpux32/libpthread_tr.so.1
     /opt/langtools/wdb/lib/hpux64/libpthread.so -> 
                       /usr/lib/hpux64/libpthread_tr.so
     /opt/langtools/wdb/lib/hpux64/libpthread.so.1 -> 
                       /usr/lib/hpux64/libpthread_tr.so.1
     
     PA:
     /opt/langtools/wdb/lib/libpthread.sl -> 
                       /usr/lib/libpthread_tr.sl
     /opt/langtools/wdb/lib/libpthread.sl.1 -> 
                       /usr/lib/libpthread_tr.sl.1
     /opt/langtools/wdb/lib/pa20_64/libpthread.sl -> 
                       /usr/lib/pa20_64/libpthread_tr.sl
     /opt/langtools/wdb/lib/pa20_64/libpthread.sl.1 -> 
                       /usr/lib/pa20_64/libpthread_tr.sl.1
     */
  if (trace_threads_in_this_run)
    {
      env_ld_lib_path = get_in_environ(inferior_environ, "LD_LIBRARY_PATH");
      if (strstr(env_ld_lib_path, pthread_link_path) == NULL)
        {
          if (!added_pthread_link_path)
            {
              added_pthread_link_path = 1;
              strcpy(ld_lib_path, pthread_link_path);
              if (env_ld_lib_path)
                {
                  strcat(ld_lib_path, ":");
                  strcat(ld_lib_path, env_ld_lib_path);
                }
              DEBUG(printf("ld_lib_path = %s\n", ld_lib_path));
              set_in_environ (inferior_environ, "LD_LIBRARY_PATH", 
                              ld_lib_path);
            }
        }
      set_in_environ (inferior_environ, "PTHR_TRACE_ENABLED", "on");
    }

  if (check_heap_in_this_run)
    {
      set_in_environ (inferior_environ, "HEAP_CHECK_ENABLED", "on");
    }

#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
  /* JAGaf37695 - Unset environment variable RTC_INIT in inferior 
     environment when "run" under gdb */
  unset_in_environ(inferior_environ, "RTC_INIT");
#endif

  /* QXCR1000744848 -LIBRTC_SERVER doesn't work when set
     inside the GDB prompt.  To fetch the environment settings
     done outside as well as within the GDB sessions, use
     get_in_environ(). Hence replaced get_env() by get_in_environ().
  */

  if ((override_librtc = get_in_environ(inferior_environ,
                                        "LIBRTC_SERVER")) == NULL)
    {          
      if (gdb_root[0] != NULL) /* GDB_ROOT is defined */
        {
          override_librtc = librtc_path;
          strcpy(librtc_path,gdb_root);
#ifdef HP_IA64
          strcat (librtc_path, "/lib/hpux32/librtc.so:");
          strcat (librtc_path, gdb_root);
          strcat (librtc_path, "/lib/hpux64/librtc.so");
#else
#ifndef GDB_TARGET_IS_HPPA_20W
          strcat (librtc_path,"/lib/librtc.sl");
#else
          strcat (librtc_path,"/lib/pa20_64/librtc.sl");
#endif
#endif
        }
    } 
  DEBUG(printf("LD_PRELOAD=%s\n",override_librtc);)

#ifdef HP_IA64
    override_librtc = override_librtc ? override_librtc :
      "/opt/langtools/lib/hpux32/librtc.so:/opt/langtools/lib/hpux64/librtc.so";
#else
#ifndef GDB_TARGET_IS_HPPA_20W
  override_librtc = override_librtc ? override_librtc :
      "/opt/langtools/lib/librtc.sl";
#else 
  override_librtc = override_librtc ? override_librtc :
      "/opt/langtools/lib/pa20_64/librtc.sl";
#endif
#endif

  env_ld_preload = get_in_environ(inferior_environ, "LD_PRELOAD");
  if (env_ld_preload && strstr (env_ld_preload, filler))
    {
      /* If scrabled string is found, restore it to librtc */
      strcpy (env_string, env_ld_preload);
      pos = strstr (env_string, filler);
      if (pos)
        {
          memcpy (pos, librtc_str, sizeof (librtc_str) - 1);
          pos = strstr (pos + sizeof (librtc_str), filler);
          if (pos)
            memcpy (pos, librtc_str, sizeof (librtc_str) - 1);
        }
    }
  else if (env_ld_preload && strstr (env_ld_preload, librtc_str))
    {
      /* If librtc is found, there's not need to pre-load it */
      strcpy (env_string, env_ld_preload);
    }
  else
    {
      strcpy (env_string, override_librtc);
      if (env_ld_preload)
        {
          strcat (env_string, ":");
          strcat (env_string, env_ld_preload);
        }
    }

  DEBUG(printf("env_string = %s\n", env_string));
  added_librtc_preload = 1;
  set_in_environ (inferior_environ, "LD_PRELOAD", env_string);
}

#endif
