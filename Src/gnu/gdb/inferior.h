/* Variables that describe the inferior process running under GDB:
   Where it is, why it stopped, and how to step it.
   Copyright 1986, 1989, 1992, 1996, 1998 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#if !defined (INFERIOR_H)
#define INFERIOR_H 1

/* For bpstat.  */
#include "breakpoint.h"

/* For enum target_signal.  */
#include "target.h"

/* For enum ttreq_t */
#include <sys/ttrace.h>

/* Structure in which to save the status of the inferior.  Create/Save
   through "save_inferior_status", restore through
   "restore_inferior_status".

   This pair of routines should be called around any transfer of
   control to the inferior which you don't want showing up in your
   control variables.  */

/* JYG: MERGE NOTE: valops.c calls bpstat_clear (inf_status->stop_bpstat)
   so we need this to be externally visible */
struct inferior_status
{
  enum target_signal stop_signal;
  CORE_ADDR stop_pc;
  bpstat stop_bpstat;
  int stop_step;
  int stop_stack_dummy;
  int stopped_by_random_signal;
  int trap_expected;
  CORE_ADDR step_range_start;
  CORE_ADDR step_range_end;
  CORE_ADDR step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
    CORE_ADDR step_frame_rse_addr;
#endif
  int step_over_calls;
  CORE_ADDR step_resume_break_address;
  int stop_after_trap;
  int stop_soon_quietly;
  CORE_ADDR selected_frame_address;
  char *stop_registers;

  /* These are here because if call_function_by_hand has written some
     registers and then decides to call error(), we better not have changed
     any registers.  */
  char *registers;

  int selected_level;
  int breakpoint_proceeded;
  int restore_stack_info;
  int proceed_to_finish;
};

extern struct inferior_status *save_inferior_status (int);

extern void restore_inferior_status (struct inferior_status *);

extern struct cleanup *make_cleanup_restore_inferior_status (struct inferior_status *);

extern void discard_inferior_status (struct inferior_status *);

extern void write_inferior_status_register (struct inferior_status
					    *inf_status, int regno,
					    LONGEST val);

/* This macro gives the number of registers actually in use by the
   inferior.  This may be less than the total number of registers,
   perhaps depending on the actual CPU in use or program being run.  */

#ifndef ARCH_NUM_REGS
#define ARCH_NUM_REGS NUM_REGS
#endif

extern void set_sigint_trap (void);

extern void clear_sigint_trap (void);

extern void set_sigio_trap (void);

extern void clear_sigio_trap (void);

extern int breakpoints_inserted;

/* File name for default use for standard in/out in the inferior.  */

extern char *inferior_io_terminal;

/* Pid of our debugged inferior, or 0 if no inferior now.  */

extern int inferior_pid;

/* Is the inferior running right now, as a result of a 'run&',
   'continue&' etc command? This is used in asycn gdb to determine
   whether a command that the user enters while the target is running
   is allowed or not. */
extern int target_executing;

/* Are we simulating synchronous execution? This is used in async gdb
   to implement the 'run', 'continue' etc commands, which will not
   redisplay the prompt until the execution is actually over. */
extern int sync_execution;

/* This is only valid when inferior_pid is non-zero.

   If this is 0, then exec events should be noticed and responded to
   by the debugger (i.e., be reported to the user).

   If this is > 0, then that many subsequent exec events should be
   ignored (i.e., not be reported to the user).
 */
extern int inferior_ignoring_startup_exec_events;

/* This is only valid when inferior_ignoring_startup_exec_events is
   zero.

   Some targets (stupidly) report more than one exec event per actual
   call to an event() system call.  If only the last such exec event
   need actually be noticed and responded to by the debugger (i.e.,
   be reported to the user), then this is the number of "leading"
   exec events which should be ignored.
 */
extern int inferior_ignoring_leading_exec_events;

/* Inferior environment. */

extern struct environ *inferior_environ;

/* Character array containing an image of the inferior programs'
   registers. */

extern char *registers;

/* Character array containing the current state of each register
   (unavailable<0, valid=0, invalid>0). */

extern signed char *register_valid;

extern void clear_proceed_status (void);

extern void proceed (CORE_ADDR, enum target_signal, int);

extern void kill_inferior (void);

extern void generic_mourn_inferior (void);

extern void terminal_ours (void);

extern int run_stack_dummy (CORE_ADDR, char *);

extern CORE_ADDR read_pc (void);

extern CORE_ADDR read_pc_pid (int);

extern CORE_ADDR generic_target_read_pc (int);

extern void write_pc (CORE_ADDR);

extern void write_pc_pid (CORE_ADDR, int);

extern void generic_target_write_pc (CORE_ADDR, int);

extern CORE_ADDR read_sp (void);

extern CORE_ADDR generic_target_read_sp (void);

extern void write_sp (CORE_ADDR);

extern void generic_target_write_sp (CORE_ADDR);

extern CORE_ADDR read_fp (void);

extern CORE_ADDR generic_target_read_fp (void);

extern void write_fp (CORE_ADDR);

extern void generic_target_write_fp (CORE_ADDR);

extern CORE_ADDR unsigned_pointer_to_address (struct type *type, void *buf);

extern void unsigned_address_to_pointer (struct type *type, void *buf,
					 CORE_ADDR addr);
extern CORE_ADDR signed_pointer_to_address (struct type *type, void *buf);
extern void address_to_signed_pointer (struct type *type, void *buf,
				       CORE_ADDR addr);

extern void wait_for_inferior (void);

extern void fetch_inferior_event (void *);

extern void init_wait_for_inferior (void);

extern void close_exec_file (void);

extern void reopen_exec_file (void);

/* The `resume' routine should only be called in special circumstances.
   Normally, use `proceed', which handles a lot of bookkeeping.  */

extern void resume (int, enum target_signal);

/* From misc files */

extern void store_inferior_registers (int);

extern void fetch_inferior_registers (int);

extern void solib_create_inferior_hook (void);

extern void child_terminal_info (char *, int);

extern void term_info (char *, int);

extern void terminal_ours_for_output (void);

extern void terminal_inferior (void);

extern void terminal_init_inferior (void);

extern void terminal_init_inferior_with_pgrp (int pgrp);

/* From infptrace.c or infttrace.c */

extern int attach (int);

#if !defined(REQUIRE_ATTACH)
#define REQUIRE_ATTACH attach
#endif

#if !defined(REQUIRE_DETACH)
#define REQUIRE_DETACH(pid,siggnal) detach (siggnal)
#endif

extern void detach (int);

/* PTRACE method of waiting for inferior process.  */
int ptrace_wait (int, int *);

extern void child_resume (int, int, enum target_signal);

#ifndef PTRACE_ARG3_TYPE
#define PTRACE_ARG3_TYPE int	/* Correct definition for most systems. */
#endif

extern int call_ptrace (int, int, PTRACE_ARG3_TYPE, int);

/* Bindu: Call for five argument ptrace */
#ifdef FIVE_ARG_PTRACE
extern int call_five_arg_ptrace (int, int, PTRACE_ARG3_TYPE, 
				 int, PTRACE_ARG3_TYPE);
#endif

extern lwpid_t get_lwp_for (tid_t);
extern pthread_t get_pthread_for (tid_t);
extern tid_t get_gdb_tid_from_utid (pthread_t);
extern tid_t get_gdb_tid_from_ktid (lwpid_t);
extern lwpid_t get_lwpid_from_utid (pthread_t utid);
extern int is_process_id ( int );
extern int get_pid_for ( tid_t );
extern int get_main_thread_tid ();
extern int thread_could_run_gc ( tid_t );
/* JAGag12946 */
extern tid_t select_thread_could_run_gc ( int *);
extern int call_ttrace ( ttreq_t , int , uint64_t , uint64_t , uint64_t );
extern int call_real_ttrace ( ttreq_t , int , tid_t , uint64_t , uint64_t , uint64_t );
extern int child_reported_exec_events_per_exec_call ( void );
extern void reset_memory_page_protections ( void );
extern void hppa_enable_page_protection_events ( int );
extern void hppa_disable_page_protection_events ( int );
extern int ptrace_wait ( int , int *);
extern int parent_attach_all ( void );
extern void require_notification_of_events ( int );
extern void child_acknowledge_created_inferior ( int );
extern void child_post_startup_inferior ( int );
extern int child_insert_fork_catchpoint ( int );
extern int child_remove_fork_catchpoint ( int );
extern int child_insert_vfork_catchpoint ( int );
extern int child_remove_vfork_catchpoint ( int );
extern int child_has_forked ( int , int *);
extern int child_has_vforked ( int , int *);
extern int process_has_vforks_prefork ( int , int *);
extern int process_has_forks_prefork ( int , int *);
extern int process_has_failed_prefork ( int , int *);
extern int child_can_follow_vfork_prior_to_exec ( void );
extern int child_insert_exec_catchpoint ( int );
extern int child_remove_exec_catchpoint ( int );
extern int child_has_execd ( int , char **);
extern int child_has_syscall_event ( int , enum target_waitkind *, int *);
extern int child_thread_just_exited ( tid_t );
extern int child_thread_alive ( tid_t );
extern int read_from_register_save_state ( int , uint64_t , void *, int );
extern int write_to_register_save_state ( int , uint64_t , void *, int );
extern int call_ptrace ( int , int , PTRACE_ARG3_TYPE , int );
extern int call_five_arg_ptrace ( int , int , PTRACE_ARG3_TYPE , int , PTRACE_ARG3_TYPE );
extern void clear_mxn_info ( void );
extern void kill_inferior ( void );
extern void child_resume ( tid_t , int , enum target_signal );
extern void update_thread_state ( int );
extern void child_post_attach ( int );
extern void detach ( int );
extern char *child_pid_to_exec_file ( int );
extern void pre_fork_inferior ( void );
extern int hppa_require_attach ( int );
extern int hppa_require_detach ( int , int );
extern int hppa_insert_hw_watchpoint ( int , CORE_ADDR , LONGEST , int );
extern int hppa_remove_hw_watchpoint ( int , CORE_ADDR , LONGEST , enum bptype );
extern int hppa_can_use_hw_watchpoint ( enum bptype , int , enum bptype );
extern int hppa_range_profitable_for_hw_watchpoint ( int , CORE_ADDR , LONGEST );
extern int ia64_can_use_hw_watchpoint ( enum bptype , int , enum bptype );
extern int ia64_insert_hw_watchpoint ( int , CORE_ADDR , LONGEST , int );
extern int ia64_remove_hw_watchpoint ( int , CORE_ADDR , LONGEST , int );
extern char *hppa_tid_to_str ( pid_t );
extern char *hppa_pid_or_tid_to_str ( pid_t );
extern pid_t hppa_switched_threads ( pid_t );
extern void hppa_ensure_vforking_parent_remains_stopped ( int );
extern int hppa_resume_execd_vforking_child_to_get_parent_vfork ( void );
extern int ttrace_write_reg_64 ( int , CORE_ADDR , CORE_ADDR );
extern void do_mxn_init ( int );
extern void enable_ut_exit_events ( int );
extern int cur_thread_in_syscall ( void );
extern void _initialize_infttrace ( void );
extern void continue_child_after_fork ( int );
extern int get_inf_si_code ( void );
extern char *map_si_code_to_str ( int , int );

/* From procfs.c */

extern int proc_iterate_over_mappings (int (*)(int, CORE_ADDR));

extern int procfs_first_available (void);

/* From fork-child.c */

extern void fork_inferior (char *, char *, char **,
			   void (*)(void),
			   void (*)(int), void (*)(void), char *);


extern void clone_and_follow_inferior (int, int *);

extern void startup_inferior (int);

/* From inflow.c */

extern void new_tty_prefork (char *);

extern int gdb_has_a_terminal (void);

/* From infrun.c */

extern void start_remote (void);

extern void normal_stop (void);

extern int signal_stop_state (int);

extern int signal_print_state (int);

extern int signal_pass_state (int);

extern int signal_stop_update (int, int);

extern int signal_print_update (int, int);

extern int signal_pass_update (int, int);

/* From infcmd.c */

extern void tty_command (char *, int);

extern void attach_command (char *, int);

/* Last signal that the inferior received (why it stopped).  */

extern enum target_signal stop_signal;

/* Address at which inferior stopped.  */

extern CORE_ADDR stop_pc;

/* Chain containing status of breakpoint(s) that we have stopped at.  */

extern bpstat stop_bpstat;

/* Flag indicating that a command has proceeded the inferior past the
   current breakpoint.  */

extern int breakpoint_proceeded;

/* Nonzero if stopped due to a step command.  */

extern int stop_step;

/* Nonzero if stopped due to completion of a stack dummy routine.  */

extern int stop_stack_dummy;

/* Nonzero if program stopped due to a random (unexpected) signal in
   inferior process.  */

extern int stopped_by_random_signal;

/* Range to single step within.
   If this is nonzero, respond to a single-step signal
   by continuing to step if the pc is in this range.

   If step_range_start and step_range_end are both 1, it means to step for
   a single instruction (FIXME: it might clean up wait_for_inferior in a
   minor way if this were changed to the address of the instruction and
   that address plus one.  But maybe not.).  */

extern CORE_ADDR step_range_start;	/* Inclusive */
extern CORE_ADDR step_range_end;	/* Exclusive */

/* Stack frame address as of when stepping command was issued.
   This is how we know when we step into a subroutine call,
   and how to set the frame for the breakpoint used to step out.  */

extern CORE_ADDR step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
extern CORE_ADDR step_frame_rse_addr;
#endif

/* Our notion of the current stack pointer.  */

extern CORE_ADDR step_sp;

/* 1 means step over all subroutine calls.
   -1 means step over calls to undebuggable functions.  */

extern int step_over_calls;

/* If stepping, nonzero means step count is > 1
   so don't print frame next time inferior stops
   if it stops due to stepping.  */

extern int step_multi;

/* Nonzero means expecting a trap and caller will handle it themselves.
   It is used after attach, due to attaching to a process;
   when running in the shell before the child program has been exec'd;
   and when running some kinds of remote stuff (FIXME?).  */

extern int stop_soon_quietly;

/* Nonzero if proceed is being used for a "finish" command or a similar
   situation when stop_registers should be saved.  */

extern int proceed_to_finish;

/* Save register contents here when about to pop a stack dummy frame,
   if-and-only-if proceed_to_finish is set.
   Thus this contains the return value from the called function (assuming
   values are returned in a register).  */

extern char *stop_registers;

/* Nonzero if the child process in inferior_pid was attached rather
   than forked.  */

extern int attach_flag;

/* Sigtramp is a routine that the kernel calls (which then calls the
   signal handler).  On most machines it is a library routine that
   is linked into the executable.

   This macro, given a program counter value and the name of the
   function in which that PC resides (which can be null if the
   name is not known), returns nonzero if the PC and name show
   that we are in sigtramp.

   On most machines just see if the name is sigtramp (and if we have
   no name, assume we are not in sigtramp).  */
#if !defined (IN_SIGTRAMP)
#if defined (SIGTRAMP_START)
#define IN_SIGTRAMP(pc, name) \
       ((pc) >= SIGTRAMP_START(pc)   \
        && (pc) < SIGTRAMP_END(pc) \
        )
#else
#define IN_SIGTRAMP(pc, name) \
       (name && STREQ ("_sigtramp", name))
#endif
#endif

/* Possible values for CALL_DUMMY_LOCATION.  */
#define ON_STACK 1
#define BEFORE_TEXT_END 2
#define AFTER_TEXT_END 3
#define AT_ENTRY_POINT 4
#define AT_WDB_CALL_DUMMY 5

#if !defined (USE_GENERIC_DUMMY_FRAMES)
#define USE_GENERIC_DUMMY_FRAMES 0
#endif

#if !defined (CALL_DUMMY_LOCATION)
#define CALL_DUMMY_LOCATION ON_STACK
#endif /* No CALL_DUMMY_LOCATION.  */

#if !defined (CALL_DUMMY_ADDRESS)
#define CALL_DUMMY_ADDRESS() (internal_error ("CALL_DUMMY_ADDRESS"), 0)
#endif
#if !defined (CALL_DUMMY_START_OFFSET)
#define CALL_DUMMY_START_OFFSET (internal_error ("CALL_DUMMY_START_OFFSET"), 0)
#endif
#if !defined (CALL_DUMMY_BREAKPOINT_OFFSET)
#define CALL_DUMMY_BREAKPOINT_OFFSET_P (0)
#define CALL_DUMMY_BREAKPOINT_OFFSET (internal_error ("CALL_DUMMY_BREAKPOINT_OFFSET"), 0)
#endif
#if !defined CALL_DUMMY_BREAKPOINT_OFFSET_P
#define CALL_DUMMY_BREAKPOINT_OFFSET_P (1)
#endif
#if !defined (CALL_DUMMY_LENGTH)
#define CALL_DUMMY_LENGTH (internal_error ("CALL_DUMMY_LENGTH"), 0)
#endif

#if defined (CALL_DUMMY_STACK_ADJUST)
#if !defined (CALL_DUMMY_STACK_ADJUST_P)
#define CALL_DUMMY_STACK_ADJUST_P (1)
#endif
#endif
#if !defined (CALL_DUMMY_STACK_ADJUST)
#define CALL_DUMMY_STACK_ADJUST (internal_error ("CALL_DUMMY_STACK_ADJUST"), 0)
#endif
#if !defined (CALL_DUMMY_STACK_ADJUST_P)
#define CALL_DUMMY_STACK_ADJUST_P (0)
#endif

/* FIXME: cagney/2000-04-17: gdbarch should manage this.  The default
   shouldn't be necessary. */

#if !defined (CALL_DUMMY_P)
#if defined (CALL_DUMMY)
#define CALL_DUMMY_P 1
#else
#define CALL_DUMMY_P 0
#endif
#endif

#if !defined PUSH_DUMMY_FRAME
#define PUSH_DUMMY_FRAME (internal_error ("PUSH_DUMMY_FRAME"), 0)
#endif

#if !defined FIX_CALL_DUMMY
#define FIX_CALL_DUMMY(a1,a2,a3,a4,a5,a6,a7) (internal_error ("FIX_CALL_DUMMY"), 0)
#endif

#if !defined STORE_STRUCT_RETURN
#define STORE_STRUCT_RETURN(a1,a2) (internal_error ("STORE_STRUCT_RETURN"), 0)
#endif


/* Are we in a call dummy? */

extern int pc_in_call_dummy_before_text_end (CORE_ADDR pc, CORE_ADDR sp,
					     CORE_ADDR frame_address);
#if !GDB_MULTI_ARCH
#if !defined (PC_IN_CALL_DUMMY) && CALL_DUMMY_LOCATION == BEFORE_TEXT_END
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) pc_in_call_dummy_before_text_end (pc, sp, frame_address)
#endif /* Before text_end.  */
#endif

extern int pc_in_call_dummy_after_text_end (CORE_ADDR pc, CORE_ADDR sp,
					    CORE_ADDR frame_address);
#if !GDB_MULTI_ARCH
#if !defined (PC_IN_CALL_DUMMY) && CALL_DUMMY_LOCATION == AFTER_TEXT_END
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) pc_in_call_dummy_after_text_end (pc, sp, frame_address)
#endif
#endif

extern int pc_in_call_dummy_on_stack (CORE_ADDR pc, CORE_ADDR sp,
				      CORE_ADDR frame_address);
#if !GDB_MULTI_ARCH
#if !defined (PC_IN_CALL_DUMMY) && CALL_DUMMY_LOCATION == ON_STACK
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) pc_in_call_dummy_on_stack (pc, sp, frame_address)
#endif
#endif

extern int pc_in_call_dummy_at_entry_point (CORE_ADDR pc, CORE_ADDR sp,
					    CORE_ADDR frame_address);
#if !GDB_MULTI_ARCH
#if !defined (PC_IN_CALL_DUMMY) && CALL_DUMMY_LOCATION == AT_ENTRY_POINT
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) pc_in_call_dummy_at_entry_point (pc, sp, frame_address)
#endif
#endif

/* It's often not enough for our clients to know whether the PC is merely
   somewhere within the call dummy.  They may need to know whether the
   call dummy has actually completed.  (For example, wait_for_inferior
   wants to know when it should truly stop because the call dummy has
   completed.  If we're single-stepping because of slow watchpoints,
   then we may find ourselves stopped at the entry of the call dummy,
   and want to continue stepping until we reach the end.)

   Note that this macro is intended for targets (like HP-UX) which
   require more than a single breakpoint in their call dummies, and
   therefore cannot use the CALL_DUMMY_BREAKPOINT_OFFSET mechanism.

   If a target does define CALL_DUMMY_BREAKPOINT_OFFSET, then this
   default implementation of CALL_DUMMY_HAS_COMPLETED is sufficient.
   Else, a target may wish to supply an implementation that works in
   the presense of multiple breakpoints in its call dummy.
 */
#if !defined(CALL_DUMMY_HAS_COMPLETED)
#define CALL_DUMMY_HAS_COMPLETED(pc, sp, frame_address) \
  PC_IN_CALL_DUMMY((pc), (sp), (frame_address))
#endif

/* If STARTUP_WITH_SHELL is set, GDB's "run"
   will attempts to start up the debugee under a shell.
   This is in order for argument-expansion to occur. E.g.,
   (gdb) run *
   The "*" gets expanded by the shell into a list of files.
   While this is a nice feature, it turns out to interact badly
   with some of the catch-fork/catch-exec features we have added.
   In particular, if the shell does any fork/exec's before
   the exec of the target program, that can confuse GDB.
   To disable this feature, set STARTUP_WITH_SHELL to 0.
   To enable this feature, set STARTUP_WITH_SHELL to 1.
   The catch-exec traps expected during start-up will
   be 1 if target is not started up with a shell, 2 if it is.
   - RT
   If you disable this, you need to decrement
   START_INFERIOR_TRAPS_EXPECTED in tm.h. */
#ifndef STARTUP_WITH_SHELL
#define STARTUP_WITH_SHELL 1
#endif

#if !defined(START_INFERIOR_TRAPS_EXPECTED)
#define START_INFERIOR_TRAPS_EXPECTED	2
#endif

extern char *child_pid_to_str (pid_t);
#ifdef HP_MXN
extern int is_mxn;
extern void update_thread_state(int);
extern void mxn_reinitialize(int);
extern void continue_child_after_fork(int);
extern int mxn_libpthread_init_done(void);
extern void mxn_debug_cleanup (void);
extern int mxn_debug_init (void *, int);
extern int mxn_ttrace (ttreq_t, pid_t, lwpid_t, pthread_t, uint64_t, uint64_t, uint64_t);
extern int mxn_core   (ttreq_t, pid_t, lwpid_t, pthread_t, uint64_t, uint64_t, uint64_t);
#endif

#define TID_MARKER  0x80000000
#define KTID_MARKER 0x40000000

#endif /* !defined (INFERIOR_H) */
