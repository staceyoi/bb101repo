/*                    Caveat Emptor

   This file uses many undocumented functions and interfaces to
   support garbage collection of multithread programs. It is next
   to impossible to collect MT programs without resorting to these
   tricks.

   You are not expected to use any of these in your code. HP is
   under no obligation to retain, maintain or enhance these interfaces
   in future releases.

   As a matter of fact the division that owns these interfaces is not
   under any obligation to maintain these interfaces even for us : the
   HP debugger team. They can surprise us by pulling the plug any
   time ....

   So if you are curious about using these in other situations,
   do what I do, don't.
   
*/
/* For PA, _PSTAT64 is defined in source_env.ksh */
#ifndef _PSTAT64
#define _PSTAT64
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <dl.h>
#include <assert.h>
#include <sys/shm.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <alloca.h>
#include <time.h>
#include <dl.h>
#include <dlfcn.h>
#include <limits.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <setjmp.h>
#include <sys/uio.h>
#define _KERNEL_THREADS
#include <threads/rec_mutex.h>
#include "defs.h"
#include "rtc.h"
#include "value.h"
#include "infrtc.h"

#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/pstat.h>
#include <sys/utsname.h>

#include <sys/time.h>

#ifdef HP_IA64
#include <unwind.h> /* for U_STACK_TRACE prototype */
#include <uwx_self.h> /* For self unwinding */
#endif

#ifdef HP_IA64
#include <sys/signal.h>
#include <syscall.h> /* for siginhibit/sigenable */
#endif

#ifdef HP_IA64
extern int syscall(int, ...);
#endif

#ifdef HP_IA64
#include <sys/dyntune.h>
#endif

#ifdef HP_IA64
/* The msemaphore is used in batch mode runs on IPF.
   We would stop all threads and do the work on the current thread.
   For MxN apps, this may sometimes lead to deadlock if the current thread
   attempts a SA switch. According to pthread folks, the way to prevent
   the SA switch is to do a msem_lock, stop all other threads, perform
   tasks from current thread, resume all threads and finally do a
   msem_unlock.
*/
msemaphore msem_for_selfrtc;
#endif

/* ######################################### */

/* Symbol information related data and APIs */

#ifdef HP_IA64
#define ST_TRACE_HASH_SLOTS_UNLOAD_LIB 0x5000

static stack_trace_symbol_hash_entry_structure 
	*symbol_from_unload_lib [ST_TRACE_HASH_SLOTS_UNLOAD_LIB];

static stack_trace_symbol_hash_entry_structure *stack_trace_symbol_buffer_for_unload_lib;

volatile int __stack_trace_cnt_4unload_lib = 0;

void record_symbol_from_unload_lib (void * handle);

/* ###############################################
   RTC callable APIs & their data. These APIs are called from the user
   application
   ###############################################
 */
typedef enum rtc_enable_or_disable {
RTC_LEAKS_CHECK = 1,
RTC_HEAP_CHECK = 2,
RTC_BOUNDS_CHECK = 4,
RTC_FREE_CHECK = 8,
RTC_STRING_CHECK = 16,
RTC_ALL = 31
}rtc_ED_checks;

/* Used for only RTC API */
static int new_leak_info_capacity = 0;  /* how big is new_leak_info[] for
                                           RTC rtc_leaks(new) API  */
static long long total_new_leak_bytes = 0;
static int new_leak_count = 0;
static int total_new_leaks_blocks = 0;

/* RTC API procedures */
int rtc_leaks (int );
int __rtc_leaks_api (int );
int rtc_heap ();
int rtc_heap_corruption ();
static int __rtc_heap_corruption_common (int);
int rtc_logfile (char *, int);
int __rtc_logfile_api (char *, int);
int rtc_enable (int);
int __rtc_enable_api (int);
int rtc_disable (int);
int __rtc_disable_api (int);
static int rtc_ED_common (int, int);
int rtc_running (void);
static int is_it_safe (int );


#define RTC_API_ENTRY if (mutex_trylock (&rtcapi_mutex)) { \
                       fprintf (stderr, "\nAnother thread is executing an RTC API;"\
                       " Returning from the current RTC API call.\n"); \
                       return -1; } \
                      if (mutex_trylock (&mutex)) { \
                       mutex_tryunlock (&rtcapi_mutex); \
                       fprintf (stderr, "\nRTC mutex not available.\n"); \
                       return -1; } \
                      skip_bookkeeping = 1; \
                      is_rtcapi_called = 1;

#define RTC_API_EXIT  mutex_tryunlock(&mutex); \
                      if (mutex_tryunlock (&rtcapi_mutex)) \
                      fprintf (stderr, "\nCould not release the RTC API lock.\n"); \
                     is_rtcapi_called = 0; \
                     skip_bookkeeping = 0;

#define REPORT_TIME curr_time ()

#endif
/* Keeping this variable out of #ifdef HP_IA64 to avoid failure of infrtc.c com
-  -pilation on PARISC machine.
   This variable is checked in rtc_leaks_or_dangling_info_common ()
   while setting the stack end.
   **This will always be zero for PARISC as there is not RTC-API support.
*/
int is_rtcapi_called = 0;
/* ########
   End of data and procedures listing used for RTC API.
                                             #########
 */

/* ############################################## */

/* This is a bug in the include files.  Should be defined by
 * string.h/strings.h
 */

char *strtok_r(char *s1, const char *s2, char **last);

volatile int __rtc_bounds_or_heap = HEAP_INFO;

/* This variable indicates if we have failed to acquire
   the mutex while doing info heap or info corruption.
*/
volatile int failed_to_acquire_mutex = 0;
extern void __rtc_event (enum rtc_event, void*, struct pc_tuple*, size_t);


volatile int __rtc_scramble_blocks = 0;
volatile int __rtc_check_bounds = 0;
volatile int __rtc_retain_freed_blocks = 0; /* Turned off by default */
long salvaged_memory=0;
long salvaged_count=0;
/* Internal debug flag to display both live and freed blocks */
volatile int display_freed_blocks = 0;

int __rtc_check_leaks = 0;
int __rtc_check_heap = 0;

#ifdef HP_IA64
int __rtc_check_openfd = 0;
#endif  /* HP_IA64 */

/*
----------------------------------------------------------
   Data required for reporting heap/leak/open-file from infrtc.c
----------------------------------------------------------
*/

/* This structure is very similar to the memory_info structure used in
   gdbrtc.c. TBD: need to define a common structure in rtc.h, which can
   be then used in both gdbrtc.s and infrtc.c.

   Read below (where this structure is used) to see the reason why this
   is postponed as of now.
*/
static struct rtc_memory_info {

  /* where art thou ? */
  char* address;

  long long size;

  long long min;
  long long max;

  unsigned count;

  struct pc_tuple *pc_tuple;

  int   corrupted; /* set to 1 if the block is corrupted
                      at header or footer by librtc */

  int high_mem_cnt;  /* JAGaf87040 */

  // Not needed ? dangling_ptrs *dptr;
  struct rtc_chunk_info* c_i;

} *leak_info, *block_info, *new_leak_info;

/* Used for BATCH RTC and RTC API when all leaks are requested */
static int leak_info_capacity = 0;  /* how big is leak_info[] ? */
static long long total_leak_bytes = 0;
static long total_leak_blocks = 0;
static long long total_bytes = 0;
static long total_blocks = 0;
static long leak_count = 0;
static long block_count = 0;

#ifdef HP_IA64
enum rtc_filetype {
    /* Subtypes for type VNODE */
    FD_UNKNOWN = 0,
    FD_REG,
    FD_DIR,
    FD_BLKDEV,
    /* Subtypes for type VNODE/STREAM */
    FD_CHARDEV,
    FD_FIFO,
    /* Subtypes for type SOCKET */
    FD_SOCK,
    FD_SOCKSTR
};

char rtc_filetype_str[8][20] = {"Unknown",
                                "Regular type",
                                "Directory",
                                "Block device",
                                "Character device",
                                "Pipe",
                                "Regular Socket",
                                "Stream Socket"};

typedef struct rtc_openfd_info {
    int fd;
    enum rtc_filetype filetype;
    char * filename;
    struct pc_tuple * pc_tuple;
    struct rtc_openfd_info * next_openfd;
} rtc_openfd_info;

static rtc_openfd_info * rtc_openfd_list = NULL;  /* head node */
static rtc_openfd_info * rtc_openfd_tail = NULL;  /* tail node */
static int rtc_openfd_count;
#endif  /* HP_IA64 */

/*
----------------------------------------------------------
   APIs for reporting heap/leak/open-file from infrtc.c
----------------------------------------------------------
*/
static int
compare_blocks (const void *p1, const void *p2);
static int
compare_blocks_by_size (const void *p1, const void *p2);
static void
report_this_block (int block_no, FILE * fp, int type);
static void
print_blocks (struct rtc_memory_info *block_info, FILE * fp, int type);
static int
collect_blocks(struct rtc_memory_info** data_ptr);
static int
compare_leaks (const void *p1, const void *p2);
static void
print_leaks (struct rtc_memory_info* leak_info, FILE* fp, int count);
static void
add_new_leak(struct rtc_chunk_info* c_i);
static void
produce_memory_report(pid_t mypid);

#ifdef HP_IA64
static void
report_this_openfd (rtc_openfd_info *this_openfd_info, FILE * fp);
static void
print_openfds (FILE * fp);
static void
rtc_initialize_openfd (void);
#endif  /* HP_IA64 */

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
extern int thread_trace_enabled;
#endif
extern int __rtc_non_recursive_relock;
extern int __rtc_unlock_not_own;
extern int __rtc_mixed_sched_policy;
extern int __rtc_condvar_multiple_mutexes;
extern int __rtc_condvar_wait_no_mutex;
extern int __rtc_deleted_obj_used;
extern int __rtc_thread_exit_own_mutex;
extern int __rtc_thread_exit_no_join_detach;
extern int __rtc_stack_utilization; /* % */
extern int __rtc_num_waiters;
extern int __rtc_ignore_sys_objects;

/* Use this as a temporary placeholder for the actual value of
   'thread_trace_enabled' in batch mode thread check. This is done because
   turning on 'thread_trace_enabled' before gdb is up and about causes
   __rtc_thread_event() to be invoked due to the numerous libc routines
   we end up calling during the config file processing. This inturn causes
   this application to crash. Hence use 'temp_thread_trace_enabled' to check
   if gdb has to be invoked. Set 'thread_trace_enabled' just after invoking
   gdb. */

static int temp_thread_trace_enabled = 0;

#if HP_VACCINE_DBG_MALLOC_IA64

int __rtc_register_bounds = 0;

/* copied from /CLO/Components/SYZYGY/Src/rtc_aux/rtc_utils.h */
typedef enum {
    RTC_ObjectKind_Unknown,
    RTC_ObjectKind_GlobalVar,
    RTC_ObjectKind_HeapObject,
    RTC_ObjectKind_LocalVar,
    RTC_ObjectKind_NonHeapObject, /* mmap , sbrk, brk, shmat */
    RTC_ObjectKind_FileStaticVar,
    RTC_ObjectKind_FunctionStaticVar,
    RTC_ObjectKind_UserDefinedObject,
    RTC_ObjectKind_Field,
    RTC_ObjectKind_SubObject,    /* the address range is within another object */
    RTC_ObjectKind_MovedPointer,  /* p = p+n; */
    RTC_ObjectKind_OOB_Address,
    RTC_ObjectKind_FreedHeapObject,  /* freed memory which is retained by librtc */
    RTC_ObjectKind_LAST
} RTC_ObjectKind;

struct _rtc_oob_callbacks {
   int (*unregister_object)(void           * addr,
                            RTC_ObjectKind   obj_kind);

   void * (*register_object)(void           * addr,
                           UInt32           byte_size,
                           RTC_ObjectKind   obj_kind,
                           const char     * name,
                           UInt32           line_no);

   void (*check_unknown_bounds)(char        * addr,
                                long          start_idx,
                                UInt32        access_length,
                                const char  * file_name,
                                size_t        line_no,
                                int           is_write);

   void (*check_known_bounds)(char        * addr,
                              long          start_idx,
                              UInt32        access_length,
                              size_t        buffer_length,
                              const char  * file_name,
                              size_t        line_no,
                              int           is_write);

   void (*unwind_local_objects_info)(char *base);

} *the_rtc_oob_callbacks = NULL;

/* ideally the _rtc_oob_scramble_memory should be put in _rtc_oob_callbacks
   struct, but that requires librtc.so and librtc_aux.a be released at same
   time to make synchronized change, the reality is librtc.so is released with 
   wdb, and librtc_aux.a is released as part of compiler package, so any change
   to _rtc_oob_callbacks struct may cause incomaptible between librtc.so
   ans librtc_aux.a. There are two scenarios: new librtc.so and the older
   librtc_aux.a, new librtc_aux.a and old librtc.so .
 */
void (*_rtc_oob_scramble_memory)(void *addr, size_t byte_size) = NULL;

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */


volatile int __rtc_check_free = 0;

volatile int __rtc_frame_count = DEFAULT_FRAME_COUNT;

volatile int __rtc_min_leak_size = DEFAULT_MIN_LEAK_SIZE;
volatile void * __rtc_watch_address = 0;
volatile int __rtc_big_block_size = 0;
volatile int __rtc_heap_growth_size = 0;

/* JAGaf89828 */
volatile int __rtc_min_heap_size = 0;

/* work list stack of "start" and "end" and "location" values saved in 
   this mark() stack frame. */

#define MARK_STACK_SIZE	10000  /* size of local work array */
      
static void *start_stack[MARK_STACK_SIZE], *end_stack[MARK_STACK_SIZE];
static enum root_set location_list[MARK_STACK_SIZE];

/* JAGaf74522 check strcpy, memset, memcpy and similar types */
volatile int __rtc_check_string = 0;
/* did not find an address in c_i */
#define FOUND_ADDR_NO 0
/* found an address in a c_i */
#define FOUND_ADDR    1
/* found an address in a c_i and emitted a warning */ 
#define FOUND_WARNED  2


#define BATCH_MODE_NONE 0
#define BATCH_MODE_ON   1
#define BATCH_MODE_OFF  2
static int batch_mode_env = BATCH_MODE_NONE;
static enum rtc_event last_rtc_event;

#if HP_VACCINE_DBG_MALLOC
static int rtc_print_and_abort = 0;
int __rtc_abort_on_bounds = 0;
int __rtc_abort_on_bad_free = 0;
int __rtc_abort_on_nomem = 0;
int __rtc_check_potential_leak = 0;  /* TODO */
int __rtc_report_raw_data = 0;       /* TODO */
int __rtc_report_address = 0;        /* TODO */
int __rtc_report_freed_memory = 0;   /* TODO */
/*
 * leak_logfile=stderr|default|[+]filename
 * mem_logfile=stderr|default|[+]filename
 * heap_logfile=stderr|default|[+]filename
 *                 Specify the logfile for +check=malloc, 
 *                 stderr:       error message goes to stderr
 *                 default:      error message goes to default log file which
 *                               is executable_name.pid.suffix
 *                 [+]filename:  error message goes to filename, '+' means
 *                               output is appended to the file.
 */
static char *__rtc_leak_logfile = NULL;
static char *__rtc_mem_logfile = NULL;
static char *__rtc_heap_logfile = NULL;

/* variables for RTC API */
static char *rtcapi_output_file_name = NULL;
static FILE *rtcapi_output_file_ptr = NULL;
static int rtcapi_logfile_keep_open = 0;

/* when linked with rtc_dbg.o, __rtc_predefined_config  becomes sencondary,
   the primary value will be linked in */
#pragma extern __rtc_predefined_config
const char *__rtc_predefined_config=NULL;

#if HP_VACCINE_DBG_MALLOC_IA64
const char *__rtc_predefined_threads_config=NULL;
#endif /* HP_VACCINE_DBG_MALLOC_IA64 */

/* the variable is used to provide user meaningful message when he is 
   trying to link or deploy executable compiled with +check=bounds:pointer 
   without proper version wdb/librtc, it needs to be synchonized with the
   name in rtc_bounds.c */
/*
 * CLEANUP: this was under HP_VACCINE_DBG_MALLOC_IA64 because 
 * it's for ipf only.  However the EXPORT_LIST is common to both 
 * PA64 and ipf so there was a warning for PA64.  I put this here
 * just for now.  It's so late in the release I do not want to
 * change the makefile and introduce another EXPORT_LIST for PA64. 
 * __rtc_predefined_config is also specific to ipf but is now in
 * a PA & ipf common place.  Eventually we need to go back and clean
 * it all up.  
 */ 
int __need_wdb_5_7_or_later=0;

#if HP_VACCINE_DBG_MALLOC_IA64
/* the __rtc_predefined_bounds_config points to __rtc_bounds_config if the 
   __rtc_bounds_config is linked in (rtc_bounds_config.o) */
#pragma extern __rtc_predefined_bounds_config
const char *__rtc_predefined_bounds_config=NULL;
#pragma extern __rtc_predefined_bounds_post_config
const char *__rtc_predefined_bounds_post_config=NULL;

/* if _rtc_oob_support_freed_memory_check is defined in 
   rtc_bounds_config.o, then rtc_oob_support_freed_memory_check
   points to its value, otherwise it is NULL */
const char *rtc_oob_support_freed_memory_check=NULL;

#define has_predefined_config()  (__rtc_predefined_config || __rtc_predefined_bounds_config \
                                  || __rtc_predefined_threads_config)

#else 

#define has_predefined_config()  (__rtc_predefined_config)

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

static void *pre_allocated_mem_pool;    /* the pre-allocated memory is used to 
                                           gracefully exit when it is out of 
                                           memory */
#define PREDEFINED_MEM_POOL_SIZE 256*1024     /* 256KB, is it enough? */

static void print_stack_trace_to_log_file(enum rtc_event, void *, 
                                          struct pc_tuple*, size_t);

#endif /* HP_VACCINE_DBG_MALLOC */


/* JAGaf88950 - this loop will run about 12 minutes.  Should be sufficient
 * but not too long
 */
#define MAXWAIT 1024*1024*200

int rtc_debug = 0;

/*
   Suresh: June 08 - Debug/Diagnostic counters:
   The non_RTC* counts are NOT accurate for MT applications, since no 
   locks are acquired before we bump up these. But still serves as a 
   quick and dirty data point. As these counters are typically used at 
   start-up where the chances of having multiple threads are limited. 
   If in future it turns out these are very unreliable, we can make 
   these variables thread specific globals?

   Warning: We cannot acquire locks at the points where we bump up
   these counts, since attimes we do that very early in the process 
   start-up and it might cause deadlocks as locking sub-system code
   in pthreads may not be fully initialized.
  
   The RTC_oustan* statistics are accurate even for MT programs

   We will be able to trace allocations/deallocations, iff the
   application/RTC calls malloc/calloc/free/.. by name. If he calls them 
   using a function pointer got from shl_findsym(), then the call would
   directly go to libc and we will not be able to trace the call.
   For eg, most of RTC internal memory is got, by calling the libc
   routine directly thru a function pointer and hence RTC wrapper
   functions are not called. The only saving grace - we call 
   libc_calloc() in most cases in generic non-wrapper RTC routines
   (like _rtc_initialize), which inturn calls malloc by name, which 
   lands in our wrapper. To summarize, we get a trace of most 
   allocations/ deallocations but NOT all.
*/
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
static int rtc_debug_mallocs = 0;
static long non_RTC_malloc_count = 0;
static long non_RTC_free_count = 0;
static long non_RTC_malloc_size = 0;
static long non_RTC_malloc_missed_count = 0;
static long non_RTC_malloc_missed_size = 0;
/* Not just Heap blocks, RTC metadata tracks multiple other things like 
   mmap regions, etc..  The below 2 counts really track the no of 
   chunks_infos currently allocated (the RTC meta data) and the size of 
   all allocations that we currently know off
*/
static long RTC_outstanding_allocation_count = 0;
static long RTC_outstanding_allocation_size = 0;
#endif

static int rtc_no_abort = 0;

/* interval related defs */
static void heap_interval_check(void);
static int  write_interval_data(void);
static void parse_config_string(const char *cfg_str);
static void parse_config_string_new(const char *cfg_str);
static void add_to_dangling_ptrs_list (struct rtc_chunk_info *, char **, enum root_set);

/* Support for sigaltstack() */
static void mark_altstack (boolean, boolean);
static int get_altstacks_index (void);

struct {
    struct timespec start;
    struct timespec end;
} interval;

volatile int __rtc_check_heap_interval = 0;
volatile int __rtc_check_heap_repeat = DEFAULT_REPEAT_CNT;
volatile int __rtc_check_heap_reset = 0;

static int repeat_count = 0;
static int interval_in_progress = 0;
static char heap_interval_filename[100];
static char heap_interval_idxfile[100];
static int heap_interval_filename_fd = -1;
static int heap_interval_idxfile_fd = -1;

/* Defined in target specific files. */
extern int get_frame_pc_list (void **, int);
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
extern void libcl_stuff ();
#endif

/* When the program is multithreaded, we need to scan the registers
   associated with all the threads. We will allocate a buffer to 
   hold the register values here. GDB is responsible for copying the
   registers over.
*/

volatile char * __rtc_register_file; /* To be set by gdb. */
int register_file_size;

/* Is it invoked by gdb, or internally?
   selfrtc is set to 1, when batch mode does not invoke gdb.

   Initialized to -1.
*/
static int selfrtc = -1;

/* QXCR1000978807: For +check=malloc binaries, selfrtc is set to 1.
   However, when such binaries are run under gdb interactive heap
   debugging, the "info leak" call is invoked fom gdb. The variable
   below is set to 1 only when the leak analysis is being invoked
   internally by rtc_leak_analyze().
*/
static int invoked_internally = 0;

/* QXCR1000989650: Sometimes, when the app is compiled with +check, the rtc_aux
   functions will reset __inside_librtc to 0, even when skip_bookkeeping
   is set. This may lead to bad free errors etc. We need to skip book keeping
   of malloc/free/.. as long as skip_bookkeeping is 1.
*/
__thread int skip_bookkeeping = 0;

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
static boolean find_gdb(char *g, size_t sz);
char *get_executable_path (int pid);
static void wait_for_gdb_here (int pid);
#endif

#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
/* JAGaf15615: Enable heap-analysis on attach,from the time the process starts in PA-32*/
void __rtc_init_leaks();
void _start (int , char **, char ** ); /*JAGaf37695*/ 
#endif

/* JAGag12947 - IA & PA64 test case can call _exit */
void _exit(int);

/* Baskar, JAGae68100 */
__thread_once_t init_once = __THREAD_ONCE_INIT;

__thread_once_t print_once = __THREAD_ONCE_INIT;

#ifdef REGISTER_STACK_ENGINE_FP
volatile char *__rtc_RSE_file; /* To be set by gdb */
int __rtc_RSE_file_size;
#endif /* REGISTER_STACK_ENGINE_FP */

void obtain_backdoor_entry (void);
#ifdef DLD_RTC
#define OBTAIN_BACKDOOR_ENTRY() if (!libc_malloc || !dld_malloc) obtain_backdoor_entry ()
#else // DLD_RTC
#define OBTAIN_BACKDOOR_ENTRY() if (!libc_malloc) obtain_backdoor_entry ()
#endif // DLD_RTC
#define MONITOR_HEAP_GROWTH __rtc_heap_growth_size
#define HEAP_GROWTH_THRESHOLD __rtc_heap_growth_size

/* If __rtc_check_bounds is set to true, then we would allocate extra
   bytes at both ends of the block and fill it up with a known pattern.
   We could then check to verify if the pattern is intact when the
   block is being freed. As things stand now, this only tells there is
   a black sheep in the herd and doesn't say which one. Expect some
   aid in future in this space.
*/
    
#define PADDED_BLOCK     1
#define NON_PADDED_BLOCK 0


/* Sep, 07: Suresh, Even though the footer size can now be bigger, we cannot write the footer at 
   any arbitary address as it could result in an unaligned access. To prevent this the footer 
   pattern is a simple "char", which we repeat for __rtc_footer_size no of times.

   This problem will not be there for the Header, as libc_malloc always returns an aligned 
   address, and we write the header just above this aligned address..
*/

#define HEADER_MAGIC_COOKIE  0xfeedface
#define FOOTER_MAGIC_COOKIE  0xff
#define FREED_BLOCK_MAGIC_COOKIE  0xabcdabcd

/* JAGag42161 - Programmable header and footer sizes.. */
volatile int __rtc_header_size = DEFAULT_HEADER_SIZE;
volatile int __rtc_footer_size = DEFAULT_FOOTER_SIZE;


#define HEAP_BLOCK 1
#define NON_HEAP_BLOCK 0
/*
 * Identifies recorded heap blocks which were allocated even
 * before the first malloc was called by find_mmaps_in_startup
 */
#define NON_HEAP_PREINIT_BLOCK 2

extern rtc_chunk_info * __rtc_leaks_info (void);
extern rtc_chunk_info * __rtc_dangling_info (void);
extern rtc_chunk_info * __rtc_dangling_info_internal (void);
extern int __rtc_heap_info (void);
extern int __rtc_heap_interval_info (void);
extern int __rtc_high_mem_info (void);

rtc_chunk_info * (*volatile leaks_info_plabel) () = __rtc_leaks_info;
rtc_chunk_info * (*volatile dangling_info_plabel) () = __rtc_dangling_info;
rtc_chunk_info * (*volatile dangling_info_plabel_internal)()=__rtc_dangling_info_internal;
int (*volatile heap_info_plabel) () = __rtc_heap_info;
int (*volatile high_mem_info_plabel) () = __rtc_high_mem_info;
int (*volatile interval_info_plabel) () = __rtc_heap_interval_info;

/* Local cache of dld list. We cannot ask for this while GC is in
   progress. We will update this list once at start up and afterwards
   when a library gets loaded or unloaded. GDB will notify us when our
   list is invalid by setting the variable `shlib_info_invalid'.
*/
   
#define SHLIB_INFO_INIT_SIZE 1024
/* Suresh, Jan 08: QXCR1000573545 - Incorrect stack trace after a dlclose.
   Expanding the shlib_info structure to capture more details

   There is now an equivalent struct on the gdb side and the field order
   between them should be the same. If you modify here you need to 
   change the other one

   All the pointers are at the start of the structure to make the math
   easy in gdb side while reading these fields..
*/
struct shlib_info {
  struct shlib_info  *next_unloaded; /* chain for faster access */
  void *text_start;
  void *text_end;
  void *data_start;
  void *data_end;
  int library_index;
  char library_name[MAXPATHLEN+1];
  bool is_it_loaded;
};
volatile struct shlib_info* shlib_info = 0;

static int shlib_info_size = SHLIB_INFO_INIT_SIZE;
/* head of unloaded shlib_info chain */
static struct shlib_info *head_unloaded_shlib_info = NULL; 

/* Used in gdbrtc.c. if type changes, we should change there as well*/
volatile int shlib_info_count = 0;

/* Used in gdbrtc.c. if type changes, we should change there as well*/
volatile int gdb_has_read_shlib_info = 0; 

/* The lib index of -1 is a special value to indicate that its a live 
   and loaded library module and the corresponding PC entry has
   has the absolute address
*/
# define ABSOLUTE_ADDRESS_INDEX -1

/* To turn off dlclose handling, just in case :)- */
volatile int enable_offsets = 1; 

/* JAGaf15615: Turn off Optimization for shlib_info_invalid wherever it 
 * is used. */
volatile int shlib_info_invalid = 0;  /* Unless gdb instructs otherwise ... */

#ifdef HP_IA64
volatile int __rtc_control_mxn_sa_switch = 0; /* gdb sets the value.
                                                 Set to 1 if app is BOR. Not
                                                 set if thread check is on */
volatile int __rtc_mxn_sa_switch_disabled = 0; /* SA switch is disabled
                                                  for MxN */
#endif

/* srikanth, 991001, Run Time Checking (RTC) module for the Wildebeest.
   As of Dec 99, this module allows gdb to track and report memory
   leaks, double frees, bad frees, bad realloc(), heap overflows and
   underflows. In future this would be extended so that uninitialized
   memory reads and unallocated memory accesses would be trapped too.

   Here is a rough overview of how things work :

   o The program must be linked against the shared version of libc.
     Users need not link explicity against librtc (as they once did).  gdb
     sets the LD_PRELOAD environment variable which instructs the linker to
     load this library.

   o The user then should invoke gdb as `gdb -leaks a.out'.
     Alternatively, a user may enable RTC mode by using the 
     `set heap-check' command. In the latter case, the command must be
     issued before the inferior starts running (the corollary is that
     if you attach to a running process, you cannot ask for a leak
     report.)
   
   o The wrappers in this module use a backdoor mechanism to fall back
     on the C standard library for the actual implementation of the
     allocation or deallocation. In addition these routines update
     suitable data structures to capture allocation specific
     information. Since librtc is the first library that is loaded
     all the calls to allocation/deallocation routines of libc will
     now go directly to the wrappers in this module.

   o When the user wants a leak report, he or she uses the new gdb
     command `info leaks'. In response to this command, the debugger
     calls the function __rtc_leaks_info() exported by this library in
     the context of the debuggee. This routine scans all the program
     context and returns a leak list.

   o The debugger walks this list, downloads the data and presents
     it to the user.

   o When the user wants a heap report,  he or she uses the new gdb
     command `info heap'. In response to this command, the debugger
     calls the functions __rtc_heap_info() exported by this library 
     This routine writes the heap report in /tmp/__heap_info.<pid> file.

   Thus most of the work gets done in the a.out and gdb's role is
   limited to supervision.

   For more details, see http://cllweb.cup.hp.com/~srikanth/arm.html
*/

/* The following pragmas are to guarantee that these functions can
   be called directly from other load modules, without having to go
   through the export stubs. This is done to reduce the overhead of
   the snoop on the heap. This is applicable only to PA32 and is a
   NOP for PA64 and IA64 as these runtimes do not use export stubs.

   We need to bypass the export stub for debugger callable functions
   too (leaks_info & heap_info.) Currently on PA32, when we make a
   command line call to a function from a shared library, we actually
   make two calls. The first one is to locate the external plabel
   via shl_findsym. When garbage collecting a threaded program, this is
   a dangerous thing to do, as this call acquires a mutex and a dead
   lock would result if some other thread is holding the mutex.
*/

#ifndef DLD_RTC

#pragma HP_NO_RELOCATION malloc, calloc, valloc
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
#pragma HP_NO_RELOCATION realloc, free, mmap, munmap, __mmap64
#else
#pragma HP_NO_RELOCATION realloc, free, mmap, munmap
#endif
#pragma HP_NO_RELOCATION sbrk, brk, shmat, shmdt
#pragma HP_NO_RELOCATION mprotect
#pragma HP_NO_RELOCATION shl_unload, dlclose
#pragma HP_NO_RELOCATION memcpy, memccpy, memmove
#pragma HP_NO_RELOCATION strcpy, strncpy
#pragma HP_NO_RELOCATION bzero, bcopy, memset
#pragma HP_NO_RELOCATION strcat, strncat
#pragma HP_NO_RELOCATION sigstack, sigaltstack

#pragma HP_LONG_RETURN malloc, calloc, valloc
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
#pragma HP_LONG_RETURN realloc,free, mmap, munmap, __mmap64
#else
#pragma HP_LONG_RETURN realloc,free, mmap, munmap
#endif
#pragma HP_LONG_RETURN sbrk, brk, shmat, shmdt
#pragma HP_LONG_RETURN mprotect
#pragma HP_LONG_RETURN shl_unload, dlclose
#pragma HP_LONG_RETURN memcpy, memccpy, memmove 
#pragma HP_LONG_RETURN strcpy, strncpy
#pragma HP_LONG_RETURN bzero, bcopy, memset
#pragma HP_LONG_RETURN strcat, strncat
#pragma HP_LONG_RETURN sigstack, sigaltstack

#if 1 && HP_VACCINE_DBG_MALLOC_IA64

#pragma HP_NO_RELOCATION  gets
#pragma HP_NO_RELOCATION  fgets
#pragma HP_NO_RELOCATION  getcwd
#pragma HP_NO_RELOCATION  getwd
#pragma HP_NO_RELOCATION  fread
#pragma HP_NO_RELOCATION  read
#pragma HP_NO_RELOCATION  pread
#pragma HP_NO_RELOCATION  readv
#pragma HP_NO_RELOCATION  sprintf
#pragma HP_NO_RELOCATION  vsprintf
#pragma HP_NO_RELOCATION  snprintf
#pragma HP_NO_RELOCATION  vsnprintf
#pragma HP_NO_RELOCATION  strftime
#pragma HP_NO_RELOCATION  wcscat
#pragma HP_NO_RELOCATION  wcsncat
#pragma HP_NO_RELOCATION  wcscpy
#pragma HP_NO_RELOCATION  wcsncpy
#pragma HP_NO_RELOCATION  wmemcpy
#pragma HP_NO_RELOCATION  wmemmove
#pragma HP_NO_RELOCATION  wmemset
#pragma HP_NO_RELOCATION  fgetws
#pragma HP_NO_RELOCATION  swprintf
#pragma HP_NO_RELOCATION  vswprintf
#if 0
#pragma HP_NO_RELOCATION  mbstowcs
#pragma HP_NO_RELOCATION  wcstombs
#endif

#pragma HP_LONG_RETURN  gets
#pragma HP_LONG_RETURN  fgets
#pragma HP_LONG_RETURN  getcwd
#pragma HP_LONG_RETURN  getwd
#pragma HP_LONG_RETURN  fread
#pragma HP_LONG_RETURN  read
#pragma HP_LONG_RETURN  pread
#pragma HP_LONG_RETURN  readv
#pragma HP_LONG_RETURN  sprintf
#pragma HP_LONG_RETURN  vsprintf
#pragma HP_LONG_RETURN  snprintf
#pragma HP_LONG_RETURN  vsnprintf
#pragma HP_LONG_RETURN  strftime
#pragma HP_LONG_RETURN  wcscat
#pragma HP_LONG_RETURN  wcsncat
#pragma HP_LONG_RETURN  wcscpy
#pragma HP_LONG_RETURN  wcsncpy
#pragma HP_LONG_RETURN  wmemcpy
#pragma HP_LONG_RETURN  wmemmove
#pragma HP_LONG_RETURN  wmemset
#pragma HP_LONG_RETURN  fgetws
#pragma HP_LONG_RETURN  swprintf
#pragma HP_LONG_RETURN  vswprintf
#if 0
#pragma HP_LONG_RETURN  mbstowcs
#pragma HP_LONG_RETURN  wcstombs
#endif

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

#if HP_VACCINE_DBG_MALLOC_IA64

#pragma HP_NO_RELOCATION  longjmp
#pragma HP_NO_RELOCATION  _longjmp
#pragma HP_NO_RELOCATION  siglongjmp

#pragma HP_LONG_RETURN  longjmp
#pragma HP_LONG_RETURN  _longjmp
#pragma HP_LONG_RETURN  siglongjmp

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */
#endif // DLD_RTC

#pragma HP_NO_RELOCATION __rtc_leaks_info
#pragma HP_NO_RELOCATION __rtc_dangling_info
#pragma HP_NO_RELOCATION __rtc_dangling_info_internal
#pragma HP_NO_RELOCATION __rtc_heap_info
#pragma HP_NO_RELOCATION __rtc_heap_interval_info
#pragma HP_NO_RELOCATION __rtc_high_mem_info
#pragma HP_LONG_RETURN __rtc_leaks_info
#pragma HP_LONG_RETURN __rtc_dangling_info
#pragma HP_LONG_RETURN __rtc_dangling_info_internal
#pragma HP_LONG_RETURN __rtc_heap_info
#pragma HP_LONG_RETURN __rtc_heap_interval_info
#pragma HP_LONG_RETURN __rtc_high_mem_info

#ifdef HP_IA64
#pragma HP_NO_RELOCATION open, close
#pragma HP_LONG_RETURN open, close
#pragma HP_NO_RELOCATION fopen, fclose
#pragma HP_LONG_RETURN fopen, fclose
#pragma HP_NO_RELOCATION dup, dup2
#pragma HP_LONG_RETURN dup, dup2
#pragma HP_NO_RELOCATION creat, freopen, fcntl
#pragma HP_LONG_RETURN creat, freopen, fcntl
#endif /* HP_IA64 */

static const char * get_rtc_event_str(enum rtc_event);


#if HP_VACCINE_DBG_MALLOC
#define RETURN_COMMON                                                   \
     __inside_librtc = 0;                                                 \
     TRACE(write(2, "return from function ",                            \
                      strlen("return from function ")));                \
     TRACE(write(2, __PRETTY_FUNCTION__, strlen(__PRETTY_FUNCTION__))); \
     TRACE(write(2, "\n", 1));                                          \
     if (rtc_print_and_abort) {                                         \
       rtc_print_and_abort = 0;                                         \
       fprintf(stderr, "Abort on memory event %s, see log file %s "     \
                       "for detail\n", get_rtc_event_str(last_rtc_event), \
                                       mem_info_log_file_name);         \
       U_STACK_TRACE();                                                 \
       fprintf(stderr, "warning:To avoid the abort set the "            \
                       "environment variable RTC_NO_ABORT = 1\n");      \
       __inside_librtc = 1;                                               \
       abort();                                                         \
     }
#else
#define RETURN_COMMON                                                   \
     __inside_librtc = 0;                                                 \
     TRACE(write(2, "return from function ",                            \
                      strlen("return from function ")));                \
     TRACE(write(2, __PRETTY_FUNCTION__, strlen(__PRETTY_FUNCTION__))); \
     TRACE(write(2, "\n", 1)); 
#endif  /* HP_VACCINE_DBG_MALLOC */

#define RETURN0()                                                       \
   do {                                                                 \
     RETURN_COMMON                                                      \
     return;                                                            \
   } while (0)

#define RETURN(val)                                                     \
   do {                                                                 \
     RETURN_COMMON                                                      \
     return (val);                                                      \
   } while (0)

#define CHUNK_INFO_LOT_SIZE 4096

#define PAGE_FROM_POINTER(pointer) \
           ((((unsigned long) (pointer)) & 0xfffffffful) >> 12)


/* The RTC module will use these function pointers to reach into
   the standard C library mallocator. These are initialized suitably
   by obtain_backdoor_entry().
*/

/* JAGaf15615 */
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
static void (*libc__start)  (int argc, char **argv, char **envp); /*JAGaf37695*/
#endif

static void (*libc__exit) (int);

#ifdef DLD_RTC
static void * (*dld_malloc) (size_t size);
static int    (*dld_free)   (void * pointer);
static void * (*dld_mmap)   (void *, size_t, int, int, int, off_t);
static int    (*dld_munmap)  (void *, size_t);
#endif // DLD_RTC

void * (*libc_malloc)  (size_t size);
static void   (*libc_free)    (void * pointer);
static void * (*libc_calloc)  (size_t nelem, size_t size);
static void * (*libc_realloc) (void *pointer, size_t size);
static void * (*libc_valloc)  (size_t size);

static void * (*libc_mmap)  (void *, size_t, int, int, int, off_t);
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
static void * (*libc_mmap64)  (void *, size_t, int, int, int, long long);
#endif
static int (*libc_munmap) (void *, size_t);
static int (*libc_mprotect)  (void *addr, size_t len, int prot);
static int (*libdld_shl_unload) (shl_t handle);
static int (*libdld_dlclose) (void * handle);

/* pthread_self () to get self pthread_t tid. */
static pthread_t (*libpthread_pthread_self) (void);
/* pthread_kill () to know whether a thread is live. 
   Second argument is 0 ==> just retrieve status with no signal delivery.
*/
static int (*libpthread_pthread_kill) (pthread_t, int);
/* 2 pthread_t objects have to be compared using pthread_equal (). */
static int (*libpthread_pthread_equal) (pthread_t, pthread_t);
 
static int (*libc_sigstack) (struct sigstack *, struct sigstack *);
static int (*libc_sigaltstack) (const stack_t *, stack_t *);

static void * (*libc_sbrk) (int);
static int  (*libc_brk) (char *);

static void * (*libc_shmat) (int, const void *, int);
static int (*libc_shmdt) (const void *);

void * (*libc_memcpy)  (void *s1, const void *s2, size_t len);
void * (*libc_memccpy) (void *s1, const void *s2, int c, size_t len);
void * (*libc_memset)  (void *s1, int c, size_t n);
void * (*libc_memmove) (void *s1, const void *s2, size_t n);
void   (*libc_bcopy)   (const void *s1, void *s2, size_t n);
void   (*libc_bzero)   (void *s, size_t n);
char * (*libc_strcpy)  (void *s1, const void *s2);
char * (*libc_strncpy) (void *addr, const void *addr1, size_t n);
char * (*libc_strcat)  (void *s1, const void *s2);
char * (*libc_strncat) (void *addr, const void *addr1, size_t n);

#ifdef HP_IA64
int (*libc_open) (const char *path, int flags, ... /* [mode_t mode] */ );
int (*libc_close) (int);
FILE * (*libc_fopen) (const char *file, const char *mode);
int (*libc_fclose) (FILE *stream);
int (*libc_dup) (int fildes);
int (*libc_dup2) (int fildes, int fildes2);
int (*libc_creat) (const char *path, mode_t mode);
FILE * (*libc_freopen) (const char *pathname, const char *type, FILE *stream);
int (*libc_fcntl) (int fildes, int cmd, ... /* arg */);
#endif /* HP_IA64 */

#if 1 && HP_VACCINE_DBG_MALLOC_IA64

char *   (*libc_gets) (char *s);
char *   (*libc_fgets) (char *s, int n, FILE *stream);
char *   (*libc_getcwd) (char *buf, size_t size);
char *   (*libc_getwd) (char *buf);
size_t   (*libc_fread) (void *ptr, size_t size, size_t nitems, FILE *stream);
ssize_t  (*libc_read) (int fildes, void *buf, size_t nbyte);
ssize_t  (*libc_pread) (int fildes, void *buf, size_t nbyte, off_t offset);
ssize_t  (*libc_readv) (int fildes, const struct iovec *iov, int iovcnt);
int      (*libc_sprintf) (char *s, const char *format, /* [arg,] */ ...);
int      (*libc_vsprintf) (char *s, const char *format, va_list ap);
int      (*libc_snprintf) (char *s, size_t maxsize, const char *format, /* [arg,] */ ...);
int      (*libc_vsnprintf) (char *s, size_t maxsize, const char *format, va_list ap);
size_t   (*libc_strftime) ( char *s, size_t maxsize, const char *format,
          const struct tm *timeptr);
wchar_t *(*libc_wcscat) (wchar_t *ws1, const wchar_t *ws2);
wchar_t *(*libc_wcsncat) (wchar_t *ws1, const wchar_t *ws2, size_t n);
wchar_t *(*libc_wcscpy) (wchar_t *ws1, const wchar_t *ws2);
wchar_t *(*libc_wcsncpy) (wchar_t *ws1, const wchar_t *ws2, size_t n);
wchar_t *(*libc_wmemcpy) (wchar_t *ws1, const wchar_t *ws2, size_t n);
wchar_t *(*libc_wmemmove) (wchar_t *ws1, const wchar_t *ws2, size_t n);
wchar_t *(*libc_wmemset) (wchar_t *ws, wchar_t wc, size_t n);
wchar_t *(*libc_fgetws) (wchar_t *ws, int n, FILE *stream);
int      (*libc_swprintf) (wchar_t *s, size_t n, const wchar_t *format, ... );
int      (*libc_vswprintf) (wchar_t *s, size_t n, const wchar_t *format, va_list arg);
#if 0
size_t   (*libc_mbstowcs) (wchar_t *pwcs, const char *s, size_t n);
size_t   (*libc_wcstombs) (char *s, const wchar_t *pwcs, size_t n);
#endif

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */
            
#if HP_VACCINE_DBG_MALLOC_IA64

void     (*libc_longjmp) (jmp_buf env, int val);
void     (*libc__longjmp) (jmp_buf env, int val);
void     (*libc_siglongjmp) (sigjmp_buf env, int val);

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */



static void (*libc_exit) (int);

static char * heap_base = 0;

static struct rtc_chunk_info *rtc_record_malloc (char *, size_t, int, int);

#ifdef HP_IA64
static void rtc_record_open (int fd, const char *path);
static void rtc_record_close (int fd);
static char * get_filename_for_fd (int fd);

#endif /* HP_IA64 */

/* Under the LD_PRELOAD mechanism we get the very first call to malloc
   and free. This is a little inconvenient as libc and libpthread are
   not initialized yet and we are witnessing the birth of the process
   from too close quarters. Various calls/operations fail since libc
   and lipthread have not been initialized. As a workaround we will use
   this flag to decide whether we should truly intercept calls or simply
   deflect them to libc. This flag will be cleared by gdb after the
   libraries have been loaded and threads subsystem has been initialized.
   (When program runs upto _start on PA32 and main on PA64/IA64.
*/
volatile int __pthreads_not_ready = 1; /* will be cleared by gdb */

/* This variable is used by the debugger to let us know if the main thread
   exited. We shouldn't be scanning the main stack if the main thread
   exited. */
volatile int __main_thread_exited = 0; /* would be set by gdb */

volatile char * stack_base = 0;
static char * stack_end = 0;

static struct altstack_chunk
{
  char *alternate_stack_base;
  char *alternate_stack_end;
  pthread_t thread_id;
} *altstacks;
static int altstacks_count = 0;

static char * real_heap_base;

int rtc_started = 0;
int rtc_disabled = 0;
/* JAGag32833: rtc_no_memory will be set on RTC_NO_MEMORY condition.
   We keep this flag to ensure that we provide as much heap/leak/high-mem/
   corruption info we have collected before entering a critical 'no memory' 
   situation. Before this fix we would not show any heap/leak/high-mem/
   corruption info as we would have set rtc_disabled. We continue to not 
   record any more heap allocations on a no memory situation.
*/   
static int rtc_no_memory = 0;

static long page_size;


/* change this to non static.  For PA when a variable 
 * is static, it's "hidden" in the share lib.  In   
 * gdbrtc.c, when call lookup_minimal_symbol to implement
 * info corruption (of core-file), it fails if total_pages
 * is static.  
 */
long total_pages;

/* In order to quickly locate the chunk_info associated with an
   address, we scatter the chunk_infos into a hash table based
   on the page number of the block. There would be one hash table
   entry for each page in the program's address space and that 
   entry would point to a linked list of chunk_infos for all 
   blocks from the same page.

   The hash table is allocated assuming a page size of 4096 bytes
   and a total addressible space of 4 GB. Thus there would be
   1 million slots in it. For PA64, page i from every 4 GB segment
   would get mapped to the same slot.
*/


/* change this to non static.  For PA when a variable 
 * is static, it's "hidden" in the share lib.  lookup_minimal_symbol
 * will not find it.  This is needed for gdbrtc.c to implement
 * info corruption 
 */
rtc_chunk_info ** chunks_in_page;

/* In the previous version, a fixed number of stack frames (PC values)
   were collected and stored in place within the rtc_chunk_info.
   This is highly wasteful of space, as there could be many tens of
   thousands of allocations from a given place. Now we factor out the
   stack trace and store a unique copy for each unique call stack.

   This is accomplished as follows : The `stack_trace_hash_table'
   points to STACK_TRACE_HASH_SLOTS number of (initially empty)
   link lists. Once a new stack trace is collected, we apply a
   hash function to decide which of the linked list the current stack 
   trace should belong in and add it if it already not there. Also used
   are a couple of buffers for storing the pc array and such much.
*/

#define STACK_TRACE_HASH_SLOTS 0x10000

typedef struct stack_trace_info {
    struct pc_tuple *pc_tuple;
    struct stack_trace_info *next;
    long long int sum_alloc;  /* JAGaf87040 */
} stack_trace_hash_entry;

static stack_trace_hash_entry * stack_trace_hash_table [STACK_TRACE_HASH_SLOTS];

#define STACK_TRACE_LOT_SIZE 1024

/* struct pc_tuples moved to rtc.h */
struct pc_tuple *stack_trace_buffer;
  
   
static struct stack_trace_info * stack_trace_info_buffer;
/* We allocate STACK_TRACE_LOT_SIZE of stack_trace_info each time
   and use from this pool when ever we want to get a new stack_trace_info
   allocated. __rtc_stack_trace_count keeps track of how many are available
   in the allocated lot. */
volatile int __rtc_stack_trace_count = 0;

/* We want to record allocation attempts made by the program.
   Sometimes, a single request from the program could result 
   in a cascade of requests internally. (realloc could call
   malloc and free for example.) We want to ignore these internal
   allocations. The boolean `__inside_librtc' tracks whether
   a request originated from the application. The first call is
   always from the application.
*/  

__thread int __inside_librtc;
__thread int old_cancel_state;

/* Suresh: June 2008: For PA64 & IA64 , TLS is defined way late in 
   the startup. Defining it global may cause incorrect output(race) 
   at times for the diagnostics messages, but we will live with it 
  
   But unfortunately we cannot use locks as well, as the Pthread 
   sub-system may not be initialized fully during the first few calls 
   of the routines (malloc & calloc) that uses this global.

   Also printf() calls malloc only once in the lifetime of the 
   application(not too sure if its during the first call to printf) so 
   we are sure that it would NOT recurse infinetely though its not 
   protected with locks( for IA64 and PA64) and we are resonably safe

   Note: Because its a global, there could be races and we could end
   up missing diagnostic printf of some non-RTC allocations !!
   
*/
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
int __inside_malloc_print = 0;
#endif

/* Pointer to head of a linked list of free chunk_infos. This is
   to allow for quick recycling.
 */
static rtc_chunk_info * free_chunk_info = 0;

static int scanned_this_time = 0;

/* Batch RTC related changes */
#define MAX_RTC_FILES 100

/* String size of commands read from rtcconfig file */    
#define STR_SIZE 80

static boolean file_included = false;  /* set if current executable is being traced */

/* JAGaf88911 - keep track of actual number of files rtcfiles */
static int rtc_files_count;

char *rtcenv[RTC_LAST + 1];    /* keeps all configs and their values */
char *rtcfiles[MAX_RTC_FILES]; /* keeps all of interest */

static void librtc_callback(unsigned int, struct dld_hook_param *);
static boolean is_file_included(char *);
static int init_config(char *, boolean);
static int process_config(void);
static void print_batch_info(void);

#ifdef HP_IA64
bool msem_init_stat = 0;
bool msem_lock_stat = 0;
extern struct lwp_status *lwpbuf;
extern uint64_t *bspstore_regs;

/* When selfrtc is on, the following function initiates all the work
   for heap/leak finding and reporting.
*/
static void rtc_leak_analyze();
static void resume_and_return ();
static void analyze_resume_return ();
#endif

/* Are we doing selfrtc ? */
static int is_selfrtc(void);

/* Do we need to print address of functions in heap/leak report ? */
bool addressprint = 0;

static int disable_rtc(void);
static FILE * create_output_file(char *, char *, char *, char *, char *, int);
static FILE * open_log_file(char *fname);

/* malloc  null-check related changes */
#define DEFAULT_NULL_CHECK_RANDOM_RANGE  100
#define DEFAULT_NULL_CHECK_SEED_VALUE    5 
#define NULL_CHECK_ASSERT()   (( __rtc_null_check_count >= 0 \
			       && __rtc_null_check_size == 0) \
                            || (__rtc_null_check_count == -1 \
                               && __rtc_null_check_size > 0))
#define NULL_CHECK_RANDOM()   (__rtc_null_check_count == 0)
#define NULL_CHECK_ENABLED()  (__rtc_null_check_count >= 0 \
                            || __rtc_null_check_size > 0)
#define NULL_CHECK_EXCEEDED() ((current_null_check_size > \
                              __rtc_null_check_size) \
                            || (current_null_check_count > \
                                __rtc_null_check_count))
#define RESET_NULL_CHECK_VARS() {\
                                    current_null_check_count = -1;\
				    current_null_check_size = -1;\
                                }
/* -1 if not using null check; 0 if using random */
volatile int __rtc_null_check_count = -1; 

/* specified in bytes; -1 if not using null_check_size */
volatile int __rtc_null_check_size = -1;  

volatile int __rtc_catch_nomem = 0;
volatile int __rtc_null_check_random_range = DEFAULT_NULL_CHECK_RANDOM_RANGE;
volatile int __rtc_null_check_seed_value = DEFAULT_NULL_CHECK_SEED_VALUE;
static int current_null_check_count = -1;
static int current_null_check_size = -1;

static char original_path[PATH_MAX+1];
static char changed_path[PATH_MAX+1];


/* JAGaf80611 */
typedef struct region_chunk {
  char *start;
  char *end;
} region_chunk;

static region_chunk *region_list; 
#ifdef DLD_RTC
// ranga
static region_chunk *dld_heap_regions=NULL;
typedef struct dld_heap_region {
  unsigned pre_rtc_map:1;
  char *start;
  char *end;
  struct dld_heap_region *next;
} dld_heap_region; 
dld_heap_region *dld_heap_region_first = NULL,
                *dld_heap_region_last = NULL;

static void dld_heap_region_add(char *start, size_t size, char pre_rtc_map)
{
  dld_heap_region *new_dld_heap_region = libc_malloc(sizeof(dld_heap_region));
  new_dld_heap_region->start = start;
  new_dld_heap_region->end = start+size-sizeof(start);
  new_dld_heap_region->pre_rtc_map = pre_rtc_map;
  new_dld_heap_region->next = NULL;
  if (dld_heap_region_last != NULL) {
    dld_heap_region_last->next = new_dld_heap_region;
  }
  dld_heap_region_last = new_dld_heap_region;

  if (dld_heap_region_first == NULL) {
    dld_heap_region_first = new_dld_heap_region;
  }
}

static dld_heap_region * dld_heap_region_find(char *pointer)
{
  dld_heap_region *heap_region = dld_heap_region_first;
  while (heap_region != NULL)
  {
    if (pointer > heap_region->start &&
        pointer < heap_region->end)
      break;
    heap_region = heap_region->next;
  }
  return heap_region;
}
#endif // DLD_RTC
static int region_cnt = 0;
static int max_regions=1000;

/* JAGaf87040 */
typedef struct high_mem_mark {
  char            *brk_val;
  char            *base;
  char            *end;
  struct pc_tuple *pc_tuple;
  int              count;  /* keeps track of # times sbrk moves, it should 
                              not be bigger than int */
} high_mem_mark;

/*
 * JAGaf87040 - set heap-check high-mem-info <num>,  gdbrtc.c
 * will set __rtc_high_mem_val to this #.  int is sufficient.
 * If it needs to be bigger than int there is some serious issue.
 */
volatile int __rtc_high_mem_val = -1;
static high_mem_mark high_mem; 

static char* heap_end_value;

#if HP_VACCINE_DBG_MALLOC 

static const char* mem_info_log_file_name = NULL;
static int mem_info_log_to_stderr = 0;
static FILE* mem_info_log_fp = NULL;

static FILE*
init_mem_info_log_file() 
{
   FILE *fp = NULL;
   int appended_to_file = 0;
   assert(mem_info_log_file_name == NULL);
   if (__rtc_mem_logfile != NULL) {
      /* only handle non "stderr" case here, "stderr" handled later */
      if (strcmp(__rtc_mem_logfile, "stderr") != 0) {
         mem_info_log_file_name = __rtc_mem_logfile[0] == '+' ?
                                  __rtc_mem_logfile+1 : __rtc_mem_logfile;
         if (__rtc_mem_logfile[0] == '+') {
            /* append to existing log file if it is prefixed with '+' */
            fp = fopen(mem_info_log_file_name, "a+");
            appended_to_file=1;
         }
         else 
            fp = fopen(mem_info_log_file_name, "w");
      } 
   } else {
      char *tp;
      char  pid[10];
      char  buf[2*PATH_MAX + 1];

      tp = strrchr (rtcenv[RTC_FILE], '/');
      if (tp == NULL || *tp != '/')
         tp = rtcenv[RTC_FILE];
      else
         tp++;

      sprintf (pid,"%d", getpid());
      fp = create_output_file (tp, pid, "mem", buf,
                                              rtcenv[RTC_OUTPUT_DIR], 1);
      if (fp != NULL)
         mem_info_log_file_name = strdup(buf);
   }
   if (fp == NULL) {
      /* use stderr if can not open output file */
      fp = stderr;
      mem_info_log_to_stderr = 1;
      mem_info_log_file_name = "stderr";
   } else {
      fprintf(stderr, "warning: Memory corruption info is %s to \"%s\".\n",
               appended_to_file ? "appended" : "written",
               mem_info_log_file_name);
   }
   return fp;
}

static void
print_stack_trace_from_pclist(FILE *fp, struct pc_tuple *pc_tuple_list, int frame_count)
{
  int i;
  int actual_count=0;
  assert(fp && pc_tuple_list);
  for (actual_count=0; actual_count < frame_count; actual_count++) {
    if (pc_tuple_list[actual_count].pc == NULL) break;
  }
  for (i=0; i < actual_count; i++) {


/* Suresh: Jan 08: TODO : Need to see if you can pack the 2-array pc_tuple
   back to a 1-array pc list so that _UNW_STACK_TRACE_PCLIST can handle 
   it correctly 
   Is _UNW_STACK_TRACE_PCLIST defined some where? or is the fprintf() is
   what is used here???

   The HAVE_UNWIND_SUPPORT is not defined, and there is no 
   UNW_STACK_TRACE_PCLIST() in libunwind, all were hypothetic and made up 
   by Xiouhua. It is expected someone someday would support it :-)

   If and when its avialable, then the above TODO becomes valid. For now 
   nothing needs to be done for this!

*/
#ifdef HP_IA64
    /* Print the stack frame PC symbol */
    int ret = backtrace (fp, i, SWIZZLE_PTR((uint64_t)(pc_tuple_list[i].pc)),
                     pc_tuple_list[i].library_index_of_pc, NULL);
    /* Bailout on errors */
    if (!ret)
      return;

#else
#ifdef HAVE_UNWIND_SUPPORT
#if HAVE_UNWIND_SUPPORT
    _UNW_STACK_TRACE_PCLIST(fp, pclist, actual_count);
#else
    fprintf(fp, "(%u) %#p\n", i, pc_tuple_list[i].pc);
#endif
#endif
#endif
  }
}

static void
rtc_print_context_common(FILE *fp, void *ptr, struct pc_tuple* pclist, 
                              size_t sz, int type, int ecode)
{
   /* type is important for BAD_HEADER/FOOTER but not for other mem calls */
   switch (ecode)
     {
       case RTC_BAD_HEADER :
       case RTC_BAD_FOOTER :
         fprintf(fp,
           "Memory block (size = %lld address = %#p) appears to be corrupted "
           "at the %s.\n", 
	   (unsigned long long) sz, ptr, type ? "beginning" : "end");
         break;

       case RTC_BAD_MEMCPY:
         fprintf(fp, "memcpy corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);
         break;

       case RTC_BAD_MEMCCPY:
         fprintf(fp, "memccpy corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

       case RTC_BAD_MEMSET:
         fprintf(fp, "memset corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);
         break;

       case RTC_BAD_MEMMOVE:
         fprintf(fp, "memmove corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

       case RTC_BAD_BZERO:
         fprintf(fp, "bzero corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);
         break;

       case RTC_BAD_BCOPY:
         fprintf(fp, "bcopy corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

       case RTC_BAD_STRCPY:
         fprintf(fp, "strcpy corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

       case RTC_BAD_STRNCPY:
         fprintf(fp, "strncpy corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

       case RTC_BAD_STRCAT:
         fprintf(fp, "strcat corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

       case RTC_BAD_STRNCAT:
         fprintf(fp, "strncat corrupted (size = %lld address = %#p)\n", 
                     (unsigned long long) sz, ptr);

         break;

     } /* switch */

   if (!pclist) return; 

   fprintf(fp, "\nLikely block allocation context is\n");

   print_stack_trace_from_pclist(fp, pclist, __rtc_frame_count);

   fprintf(fp, "\n");

   return;
}

static void
release_pre_allocated_mem_pool()
{
   if (pre_allocated_mem_pool) {
     libc_free(pre_allocated_mem_pool);  /* free emergency mem pool */
     pre_allocated_mem_pool = NULL;
   }
}

#ifndef HP_IA64
extern void U_STACK_TRACE(void); 
void redirect_stack_trace(FILE *filest) { 
   int fd, fd2; 
   fd = fileno(filest); 
   fflush(filest); 
   fflush(stderr); 
   fd2 = dup(STDERR_FILENO); 
   (void)dup2(fd, STDERR_FILENO); 
   U_STACK_TRACE(); 
   fflush(stderr); 
   fd = dup2(fd2, STDERR_FILENO); 
   close(fd2); 
} 
#endif

static void
print_stack_trace_to_log_file(enum rtc_event event, void *ptr, 
                              struct pc_tuple* pclist, size_t size)
{
   if (event == RTC_NO_MEMORY || event == RTC_MEM_NULL)
      release_pre_allocated_mem_pool();
   if (mem_info_log_file_name == NULL)
      mem_info_log_fp = init_mem_info_log_file();
   else {
      if (mem_info_log_to_stderr)
         mem_info_log_fp = stderr;
      else
        assert(mem_info_log_fp != NULL);
   }

   if (mem_info_log_fp != NULL) {
      fprintf(mem_info_log_fp, "-----------------------------------------\n");
      switch (event) {
      case RTC_BAD_FREE :
         fprintf(mem_info_log_fp, "Attempt to free unallocated or already "
                 "freed object at %#p\n", ptr);
         break;
      case RTC_BAD_REALLOC :
         fprintf(mem_info_log_fp, "Attempt to realloc an unallocated "
                     "object at %#p\n", ptr);
         break;
      case RTC_NO_MEMORY :
      case RTC_MEM_NULL :
         fprintf(mem_info_log_fp, "Process ran out of memory when "
                 "attempting to allocate %ld bytes\n", size);
         break;
      case RTC_BAD_HEADER :
         rtc_print_context_common(mem_info_log_fp, ptr, pclist, 
                                  size, 1, event);
         break;
      case RTC_BAD_FOOTER :
         rtc_print_context_common(mem_info_log_fp, ptr, pclist, 
                                  size, 0, event);
         break;

      case RTC_BAD_MEMCPY:
      case RTC_BAD_MEMCCPY:
      case RTC_BAD_MEMSET:
      case RTC_BAD_MEMMOVE:
      case RTC_BAD_BZERO:
      case RTC_BAD_BCOPY:
      case RTC_BAD_STRCPY:
      case RTC_BAD_STRNCPY:
      case RTC_BAD_STRCAT:
      case RTC_BAD_STRNCAT:
         rtc_print_context_common(mem_info_log_fp, ptr, pclist, 
                                  size, 1, event);
         break;

      default:
         fprintf(mem_info_log_fp, "Unknown event %d at addresss %#p\n", 
                 event, ptr);
         break;
      }

      fprintf(mem_info_log_fp, "\nCurrent context is:\n");
      /* print stack trace */
#ifdef HP_IA64
      _UNW_STACK_TRACE(mem_info_log_fp);
#else /* HP_IA64 */
      redirect_stack_trace(mem_info_log_fp);
#endif
      fprintf(mem_info_log_fp, "\n\n\n");
      if (!mem_info_log_to_stderr) {
         /* flush the buffer if it's not stderr */
         fflush(mem_info_log_fp);
      }
   }
} /* print_stack_trace_to_log_file */
#endif /* HP_VACCINE_DBG_MALLOC */


static int in_batch_mode() {
#if HP_VACCINE_DBG_MALLOC
   return (batch_mode_env == BATCH_MODE_ON) 
           || (batch_mode_env != BATCH_MODE_OFF && 
               has_predefined_config());
#else /* HP_VACCINE_DBG_MALLOC */
   return batch_mode_env == BATCH_MODE_ON;
#endif /* !HP_VACCINE_DBG_MALLOC */
}


#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
/* This routine is used for thread checking in the batch mode to create
   the ".rtcthrinit.<pid>" commands file to be fed to gdb. This is on the same
   lines as that of the rtc init file (.rtcinit_pidnumber) created for the
   batch mode heap check case. */
void
create_rtcinit_file (
  char *rtcinit_fname)
{
  int fd;
  FILE *init_fp;
  char  cmd[3*PATH_MAX + 1200];
  char frame_count_cmd [30];
  libc_strcpy (cmd,
    "set height 0\nset {int} &shlib_info_invalid=0\n"
    "set {int} &__gdb_synchronizer=1\n");
  libc_strcat (cmd, "set thread-check on\n");
  sprintf (frame_count_cmd, "set frame-count %d\n", __rtc_frame_count);
  libc_strcat (cmd, frame_count_cmd);
  libc_strcat (cmd, "tb __sync_up_with_gdb\n");
  libc_strcat (cmd, "commands\n");
  libc_strcat (cmd, "silent\n");
  libc_strcat (cmd, "end\n");

  /* Continue the process so that we hit this breakpoint for the sync up. Have
     an additional continue so that we proceed after that -- the app starts up
     and continues.
   */
  libc_strcat (cmd, "c\n");
  /* Suresh - Need to have this as continue and not just a 'c'
   as there is a loop in command_loop() that relies
   on this for batch mode thread check looping
  */
  libc_strcat (cmd, "continue\n");
  /* No quit command is being added to the .rtcthrinit file since this command
     causes a SIGHUP in quit_confirm() -- we would have finished processing
     the commands in the file. 'captured_main' ensures that gdb does a normal
     exit in batch mode anyways. */
  unlink (rtcinit_fname);
  fd = open (rtcinit_fname, O_RDWR | O_EXCL | O_CREAT, 0777);
  if (fd == -1 || (init_fp = fdopen (fd, "w")) == NULL) 
    {
       perror(
         "Cannot open .rtcthrinit. Cannot print thread error information.");
       return; /* bail out, no output */
    }

  fprintf (init_fp, "%s", cmd);
  fclose (init_fp);
  DEBUG(printf ("rtcinit is %s", cmd);)
  return;
}

/* Used for batch mode thread check. Used to fork and exec gdb which would
   inturn attach to this process. The steps are similar to the ones in
   print_batch_info() for batch mode heap check. */

void invoke_gdb (char *filename)
{
  char rtcinit_fname [100];
  char buf [PATH_MAX + 100];
  char pid [20];
  pid_t cpid;
  pid_t temp_pid;

  if (!temp_thread_trace_enabled)
    {
      return;
    }

  temp_pid = getpid();
  sprintf (rtcinit_fname, ".rtcthrinit.%d", temp_pid);
  create_rtcinit_file (rtcinit_fname);
  sprintf (pid,"%d", temp_pid);

  DEBUG(printf("In invoke_gdb pid %s::%s\n", pid, filename);)
  DEBUG(fflush (stdout));

  if ((getcwd(buf, PATH_MAX+1)) == NULL)
    {
      perror("In librtc, unable to get current directory");
    }

  libc_strcpy(changed_path, buf);

  if (chdir(original_path) != 0) 
    {
       sprintf(buf, "In librtc, unable to chdir to original path %s\n", 
               original_path);
       perror(buf);
    }

  boolean found_gdb = find_gdb (buf, PATH_MAX); // invoke gdb to print details

  if (!found_gdb)
    {
      perror(buf);
      DEBUG(printf ("No heap/leak information would be generated"));
      goto bailout;
    }

  DEBUG(printf("exec=%s pid=%s\n", buf, pid);)
  DEBUG(fflush (stdout));

  cpid = fork();
  if (0 == cpid)
    { /* child process */
       char *debug_cmd = "-n";  /* something redundant, but parseable */

       /* JAGag12951 - Pass the executable name while attaching. 
          Use pstat_getproc() to do that*/
       char *exec_name = NULL;
       int temp_pid = 0;

       putenv ("LD_PRELOAD="); /* remove LD_PRELOAD from env before exec */
       sscanf (pid, "%d", &temp_pid);

       __inside_librtc = shlib_info_invalid = 0;
       if (getenv("RTC_DEBUG_GDB") != NULL)
         debug_cmd = "--debug-gdb";

       /* JAGag26786: Use this new function to get the exec_name.
          pst.ucomm will give the basename only not the whole path. */
       exec_name = get_executable_path (temp_pid);
   
       /* jini: QXCR1000757191: Enable being able to specify the o/p dir
          for batch mode thread check. If no output directory has been
          specified yet, use the current working directory.
        */
       if (rtcenv[RTC_OUTPUT_DIR] == NULL)
         {
           rtcenv[RTC_OUTPUT_DIR] = strdup (".");
         }
 
       DEBUG(printf ("pid is %d\n",temp_pid);)
       DEBUG(printf ("Exec file name is %s\n", exec_name);)
       DEBUG(printf ("forked successfully buf = %s cpid=%d\n", buf, cpid);)
       DEBUG(printf ("execl(%s, %s, -p, %s, -n, -x, %s, -batch, "
                     "-tty=/dev/null, -q, -brtc, -threads-logdir, %s,"
                     "(char *)0) \n", buf, buf, pid, rtcinit_fname,
                     rtcenv[RTC_OUTPUT_DIR]));
       DEBUG(fflush (stdout));

       /* Passing the executable name for attach is failing on IA.
       So temporarly putting under ifdef.*/
#ifndef HP_IA64
       if (execl (buf, buf, exec_name, "-p", pid, "-n", debug_cmd,
                  "-x", rtcinit_fname, "-batch", "-tty=/dev/null", "-q",
                  "-brtc", "-threads-logdir", rtcenv[RTC_OUTPUT_DIR],
                  (char *)0) == -1)
#else
       if (execl (buf, buf, "-p", pid, "-n", debug_cmd,
                "-x", rtcinit_fname, "-batch", "-tty=/dev/null", "-q",
                "-brtc", "-threads-logdir", rtcenv[RTC_OUTPUT_DIR],
                  (char *)0) == -1)
#endif
         {
            perror ("execl failed. Cannot print RTC info"); /* bail out */
            DEBUG(printf ("execl failed buf = %s cpid=%d\n", buf, cpid);)
            DEBUG(fflush (stdout));
            unlink (rtcinit_fname); /* remove .rtcthrinit.<pid> file */
            libc_exit (1);
         }
    } 
  else 
    {
      DEBUG(printf ("invoke_gdb: forked successfully cpid=%d \n",
                    cpid);)
      DEBUG(fflush (stdout));

      wait_for_gdb_here (cpid);
      unlink (rtcinit_fname); /* remove .rtcthrinit.<pid> file */
    }

bailout:
    /* JAGaf70635 - return back to the the changed_path */
   if (chdir(changed_path) != 0)
   { 
      sprintf(buf,"In librtc, unable to chdir to %s\n", changed_path);
      perror(buf);
   }
   
   DEBUG(fflush (stdout));
   /* Safe place to turn on thread tracing. */
   thread_trace_enabled = true;
}
#endif


void
_rtc_library_initializer ()
{
   shl_t handle = 
#if defined(HP_IA64)
                  PROG_HANDLE;
#else
                  NULL;
#endif

#if HP_VACCINE_DBG_MALLOC
   void **default_config_ptr=NULL;
#endif  /* HP_VACCINE_DBG_MALLOC */
   char *envstr;
   int rval;
   char *batch_rtc_env = getenv("BATCH_RTC");
   if (batch_rtc_env == NULL)
      batch_mode_env = BATCH_MODE_NONE;
   else if (strcasecmp(batch_rtc_env, "on") == 0)
      batch_mode_env = BATCH_MODE_ON;
   else if (strcasecmp(batch_rtc_env, "off") == 0)
      batch_mode_env = BATCH_MODE_OFF;

   rtc_debug = getenv("RTC_DEBUG") != NULL;
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
   rtc_debug_mallocs = getenv("RTC_MALLOC_TRACE") != NULL;
#endif
   rtc_no_abort = ((envstr = getenv("RTC_NO_ABORT")) != NULL &&
                               strcmp(envstr, "1") == 0);

   addressprint = ((envstr = getenv("RTC_PRINT_ADDR")) != NULL &&
                               strcmp(envstr, "1") == 0);
#if HP_VACCINE_DBG_MALLOC

#if HP_VACCINE_DBG_MALLOC_IA64
   rval = shl_findsym(&handle, "__rtc_bounds_config", TYPE_DATA,
                                                      &default_config_ptr);
   if (rval == 0) {
      __rtc_predefined_bounds_config = (const char  *)*default_config_ptr;
   }

   rval = shl_findsym(&handle, "__rtc_bounds_post_config", TYPE_DATA,
                                                      &default_config_ptr);
   if (rval == 0) {
      __rtc_predefined_bounds_post_config = (const char  *)*default_config_ptr;
   }
   
   rval = shl_findsym(&handle, "_rtc_oob_support_freed_memory_check", TYPE_DATA,
                                                      &default_config_ptr);
   if (rval == 0) {
      rtc_oob_support_freed_memory_check = (const char  *)*default_config_ptr;
   }
#endif /* HP_VACCINE_DBG_MALLOC_IA64 */
   rval = shl_findsym(&handle, "__rtc_default_config", TYPE_DATA,
                                                      &default_config_ptr);
   if (rval == 0) {
      __rtc_predefined_config = (const char  *)*default_config_ptr;
   }

#if HP_VACCINE_DBG_MALLOC_IA64
   rval = shl_findsym(&handle, "__rtc_threads_config", TYPE_DATA,
                                                      &default_config_ptr);
   if (rval == 0) {
      __rtc_predefined_threads_config = (const char  *)*default_config_ptr;
   }

   /* find _rtc_oob_register_object() and _rtc_oob_unregister_object() */
   if (__rtc_predefined_bounds_config != NULL) {
          int shl_stat;
          handle = NULL;
          shl_stat =  shl_findsym (&handle, "__the_rtc_oob_callbacks", 
                                   TYPE_DATA, &the_rtc_oob_callbacks);
          shl_stat =  shl_findsym (&handle, "_rtc_scramble_memory", 
                                   TYPE_PROCEDURE, &_rtc_oob_scramble_memory);
   }
#endif /* HP_VACCINE_DBG_MALLOC_IA64 */

#endif /* HP_VACCINE_DBG_MALLOC */

  envstr = getenv ("HEAP_CHECK_ENABLED");
  if (envstr != NULL) 
    {
      if ((strcmp (envstr, "off") == 0) ||
          (strcmp (envstr, "OFF") == 0) ||
          (strcmp (envstr, "0") == 0)) 
        {
#if HP_VACCINE_DBG_MALLOC_IA64
           /* do not disable librtc if it is enabled by +check=malloc
              or +check=bounds:pointer */
           if (!has_predefined_config())
              rtc_disabled = 1;
#else
          rtc_disabled = 1;
#endif  /*HP_VACCINE_DBG_MALLOC */
        }
      else if ((strcmp (envstr, "on") == 0) ||
               (strcmp (envstr, "ON") == 0) ||
               (strcmp (envstr, "1") == 0)) 
        {
          rtc_disabled = 0;
        }
    }

  /* The user needs to define BATCH_RTC to switch batch rtc on/off */
   if (!in_batch_mode()) {
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
     if (dlhook(DL_LOAD_POST_INIT, librtc_callback) != 0)
       perror("librtc dlhook initialization failed"); /* no RTC info */
#endif
     return;
   }

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
  if (dlhook(DL_LOAD_POST_INIT | DL_UNLOAD_POST_FINI, librtc_callback) != 0)
    perror("librtc dlhook initialization failed"); /* no RTC info */
#endif

  return;
}

/* Free up all resources and shutdown the runtime checking system. The
   wrapper routines will continue to get called as the PLT entries 
   stand patched, but they will simply deflect all action elsewhere.
*/
   
static int 
disable_rtc ()
{
  rtc_disabled = 1;

  /* Resources will be freed in version 2.0 of RTC :-) */

  return 0;
}

/* When the program is multithreaded, we need to make sure that
   the different threads don't step on each others toes in the
   process of updating the chunk_info data structures. We employ
   a mutex for this. While we hold the mutex, we cannot allow the
   thread to be cancelled. So we will disable cancellation
   temporarily.
*/

#ifdef HP_IA64
/*
 * For MxN applications, any unbound/semibound thread can switch to
 * a different SA, while making a pthread library call. For heap/leak
 * detection, gdb stops all threads and only one thread does the heap
 * or leak detection work. If this thread switches SA during heap/leak
 * detection, the application will hang as all other kernel threads
 * are stopped by gdb.
 *
 * This is avoided by making use of siginhibit/sigenable system calls.
 * These syscalls are used by dld while doing BOR symbol resolution.
 * They can be used to disable and enable SA switching for the thread
 * doing the heap/leak detection work.
 * 
 * For this to work properly, the application must not be BOR. Otherwise,
 * dld and the application might use siginhibit and sigenable together,
 * which will lead to problems.
 *
 * Ideally, pthread library should provide an API to disable/enable SA
 * swtich dynamically. A CR QXCR1000907337 has been filed against the
 * pthreads team for this.
 */
static void
enable_mxn_sa_switch()
{
  if (__rtc_mxn_sa_switch_disabled && __rtc_control_mxn_sa_switch)
    {
      sigset_t prevmask;

      /* Enable SA switch */
      syscall(SYS_sigenable, &prevmask);
      __rtc_mxn_sa_switch_disabled = 0;

      DEBUG(printf("enable_mxn_sa_switch\n");)
    }
}

static void
disable_mxn_sa_switch()
{
  sigset_t prevmask;

  DEBUG(printf("trying disable_mxn_sa_switch\n");)
  // TBD: Is this safe enough ? Add a env variable to protect this further ?
  if (__rtc_control_mxn_sa_switch)
    {
      for (int trycount = 0; trycount < 10; trycount++)
        {
          /* Disable SA switch. Multiple attempts might be required for
             siginhibit to succeed */
          int ret;
          if ((ret = syscall(SYS_siginhibit, &prevmask)) == 0)
            {
              __rtc_mxn_sa_switch_disabled = 1;

              DEBUG(printf("disable_mxn_sa_switch\n");)
              break;
            }
          DEBUG(printf("disable_mxn_sa_switch failed %d\n", ret);)
        }
    }
}
#endif

REC_MUTEX mutex, unmap_mutex, split_region_mutex, rtcapi_mutex;

#define mutex_trylock(x) __mutex_trylock(x)
#define mutex_tryunlock(x) __mutex_unlock(x)

static void
mutex_lock (REC_MUTEX * mutex)
{
  __thread_setcancelstate (__THREAD_CANCEL_DISABLE, &old_cancel_state);
  __mutex_lock (mutex);
}

static void
mutex_unlock (REC_MUTEX * mutex)
{
  int bogus;

  /* Call __rtc_event function */
  if (failed_to_acquire_mutex)
    {
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      __rtc_event(RTC_MUTEX_LOCK_FAILED, NULL, NULL, NULL);
    }

  __mutex_unlock (mutex);
  __thread_setcancelstate (old_cancel_state, &bogus);
}

/* pre-fork handler in the about to deliver parent */

static void
do_a_down ()
{
    __mutex_lock (&mutex);
    __mutex_lock (&unmap_mutex);
    __mutex_lock (&split_region_mutex);
}

/* post-fork handler for the whole family */

static void
do_an_up ()
{
  __mutex_unlock (&split_region_mutex);
  __mutex_unlock (&unmap_mutex);
  __mutex_unlock (&mutex);
}

static const char *
get_rtc_event_str(enum rtc_event ecode)
{
  const char *str;
  switch (ecode) {
  case RTC_FCLOSE_FAILED:
    str = "RTC_FCLOSE_FAILED";
    break;
  case RTC_FOPEN_FAILED:
    str = "RTC_FOPEN_FAILED";
    break;
  case RTC_UNSAFE_NOW:
    str = "RTC_UNSAFE_NOW";
    break;
  case RTC_MUTEX_LOCK_FAILED:
    str = "RTC_MUTEX_LOCK_FAILED";
    break;
  case RTC_NOT_RUNNING:
    str = "RTC_NOT_RUNNING";
    break;
  case RTC_NO_ERROR:
    str = "RTC_NO_ERROR";
    break;
  case RTC_NO_MEMORY:
    str = "RTC_NO_MEMORY";
    break;
  case RTC_BAD_FREE:
    str = "RTC_BAD_FREE";
    break;
  case RTC_BAD_REALLOC:
    str = "RTC_BAD_REALLOC";
    break;
  case RTC_ALLOCATED_WATCHED_BLOCK:
    str = "RTC_ALLOCATED_WATCHED_BLOCK";
    break;
  case RTC_DEALLOCATED_WATCHED_BLOCK:
    str = "RTC_DEALLOCATED_WATCHED_BLOCK";
    break;
  case RTC_SBRK_NEGATIVE:
    str = "RTC_SBRK_NEGATIVE";
    break;
  case RTC_STACK_MISSING:
    str = "RTC_STACK_MISSING";
    break;
  case RTC_MALLOC_MISSING:
    str = "RTC_MALLOC_MISSING";
    break;
  case RTC_BAD_HEADER:
    str = "RTC_BAD_HEADER";
    break;
  case RTC_BAD_FOOTER:
    str = "RTC_BAD_FOOTER";
    break;
  case RTC_HUGE_BLOCK:
    str = "RTC_HUGE_BLOCK";
    break;
  case RTC_HEAP_GROWTH:
    str = "RTC_HEAP_GROWTH";
    break;
  case RTC_NOMEM:
    str = "RTC_NOMEM";
    break;
  case RTC_MEM_NULL:
    str = "RTC_MEM_NULL";
    break;
  case RTC_BAD_MEMCPY:
    str = "RTC_BAD_MEMCPY";
    break;
  case RTC_BAD_MEMCCPY:
    str = "RTC_BAD_MEMCCPY";
    break;
  case RTC_BAD_MEMSET:
    str = "RTC_BAD_MEMSET";
    break;
  case RTC_BAD_MEMMOVE:
    str = "RTC_BAD_MEMMOVE";
    break;
  case RTC_BAD_STRCPY:
    str = "RTC_BAD_STRCPY";
    break;
  case RTC_BAD_STRNCPY:
    str = "RTC_BAD_STRNCPY";
    break;
  case RTC_BAD_BZERO:
    str = "RTC_BAD_BZERO";
    break;
  case RTC_BAD_BCOPY:
    str = "RTC_BAD_BCOPY";
    break;
  case RTC_BAD_STRCAT:
    str = "RTC_BAD_STRCAT";
    break;
  case RTC_BAD_STRNCAT:
    str = "RTC_BAD_STRNCAT";
    break;
  default:
    str = "unknown";
    break;
  }
  return str;
}

/* __rtc_event() : When the leak detector module finds something amiss,
   it calls this routine. The debugger is presumed to have a breakpoint
   here. Errors could be "internal" errors such as out of memory, 
   failure to find the stack, or some other assertion failure. Or it 
   could be a program error such as a bad free, bad realloc etc.
   The pointer parameter is applicable only in a few cases based on
   the event code.

   When running under the debugger, the error reporting will be
   undertaken by GDB and there is little that needs to be done here.
*/

extern void
__rtc_event (enum rtc_event ecode, void * pointer, struct pc_tuple *pclist,
                                                                 size_t size)
{
  last_rtc_event = ecode;
  switch (ecode)
    {
    case RTC_SBRK_NEGATIVE  :  /* May day, May day, May day ...  */
    case RTC_STACK_MISSING  :
      disable_rtc ();
      break;
    case RTC_MALLOC_MISSING :
      libc_exit (1);    /* impossible to proceed */
      break;

    case RTC_NO_MEMORY      :  /* May day, May day, May day ...  */
    case RTC_MEM_NULL       :
    case RTC_BAD_FREE       :
    case RTC_BAD_REALLOC    :
    case RTC_BAD_HEADER     :
    case RTC_BAD_FOOTER     :
    case RTC_BAD_MEMCPY:
    case RTC_BAD_MEMCCPY:
    case RTC_BAD_MEMSET:
    case RTC_BAD_MEMMOVE:
    case RTC_BAD_BZERO:
    case RTC_BAD_BCOPY:
    case RTC_BAD_STRCPY:
    case RTC_BAD_STRNCPY:
    case RTC_BAD_STRCAT:
    case RTC_BAD_STRNCAT:
#if HP_VACCINE_DBG_MALLOC
      if (in_batch_mode()) {
        print_stack_trace_to_log_file(ecode, pointer, pclist, size);

        if ((__rtc_abort_on_bounds && 
                 (ecode == RTC_BAD_HEADER || ecode == RTC_BAD_FOOTER)) ||
            (__rtc_abort_on_bad_free &&
                 (ecode == RTC_BAD_FREE || ecode == RTC_BAD_REALLOC)) ||
            (__rtc_abort_on_nomem &&
                 (ecode == RTC_NO_MEMORY || ecode == RTC_MEM_NULL))) {
          rtc_print_and_abort = 1;
        }
      } 
      if (ecode == RTC_NO_MEMORY)
        {
          rtc_no_memory = 1; 
          disable_rtc ();
        }     
#endif /* HP_VACCINE_DBG_MALLOC */
      break;
    default : 
      break;
  }
}
#pragma OPTIMIZE OFF
extern void
__rtc_thread_event (enum rtc_event ecode, void* arg1,
                    void* arg2, void* arg3)
{
  switch (ecode)
    {
    default : 
      break;
    }
}

extern void
__rtc_nomem_event (enum rtc_event ecode, void * pointer)
{
  switch (ecode)
    {
    default : 
      break;
    }
}
#pragma OPTIMIZE ON

/* Put in the magic_cookie in the about to free memory. */
static void
scramble_block (unsigned int * pointer, size_t size)
{
    char * cp = 0;
    size_t i = 0;

#if HP_VACCINE_DBG_MALLOC_IA64
    /* use librtc_aux's scramble_memory when it is linked with
       librtc_aux, the scramble_memory can scramble the memory
       by read the runtime envirnment variable setting
       RTC_UMR="INIT=0xnn", the default value is 0xde */
    if (__rtc_register_bounds && _rtc_oob_scramble_memory) {
       _rtc_oob_scramble_memory(pointer, size);
       return;
    }
#endif
    for (i = 0; size >= sizeof (int); i++, size -= sizeof (int))
      pointer[i] = HEADER_MAGIC_COOKIE;

    cp = (char *) (pointer + i);
    i = 0;
    while (size > 0ul)
      {
        cp[i++] = 0xff;
        size--;
      }
}

/* Sep 07, Suresh: Put in the different magic_cookie in the application freed memory 
   when we retain the free blocks. The idea is, if you see this magic_cookie in memory, 
   its for sure a block that has been freed and not recycled again for a future allocation
*/
static void
scramble_block_for_retain_freed_blocks (unsigned int * pointer, size_t size)
{
    unsigned char * cp = 0;
    size_t i = 0;

    for (i = 0; size >= sizeof (int); i++, size -= sizeof (int))
      pointer[i] = FREED_BLOCK_MAGIC_COOKIE;

    cp = (unsigned char *) (pointer + i);
    i = 0;
    /* For the not alligned bytes at the end we write a "FF" pattern.. */
    while (size > 0ul)
      {
        cp[i++] = 0xff;
        size--;
      }
}

static int
check_freed_block(unsigned int * pointer, size_t size)
{
    unsigned char * cp = 0;
    size_t i = 0;

    for (i = 0; size >= sizeof (int); i++, size -= sizeof (int))
      if(pointer[i] != FREED_BLOCK_MAGIC_COOKIE)
          return RTC_FREED_BLOCK_CORRUPTED;

    cp = (unsigned char *) (pointer + i);
    i = 0;
    /* For the not alligned bytes at the end we check for the "FF" pattern.. */
    while (size > 0ul)
      {
        if(cp[i++] != 0xff)
          return RTC_FREED_BLOCK_CORRUPTED;
        size--;
      }
     return 0;
}

/* Check if the header and footer of the memory block is what we 
   have put in. */
#pragma OPTIMIZE OFF
static void
check_bounds (int * pointer, size_t size, struct pc_tuple *pclist)
{
  unsigned char * block = 0;
  int i;

  /* count and check in terms of int - 4 bytes at a time */
  for (i = 1; i <= __rtc_header_size/sizeof(int); i++)
    if (pointer[-i] !=  (unsigned int)HEADER_MAGIC_COOKIE)
     {
       __rtc_event (RTC_BAD_HEADER, pointer, pclist, size);
       return;
     }

  block = (unsigned char *) pointer;
  for (i = 0; i < __rtc_footer_size; i++)
    if (block[size+i] != (unsigned char) FOOTER_MAGIC_COOKIE)
     {
       __rtc_event (RTC_BAD_FOOTER, pointer, pclist, size);
       return;
     }
}

static int
boundary_checks (unsigned int *pointer, unsigned int size)
{
  unsigned int *start_of_block =
              (unsigned int *)((unsigned char *)pointer-__rtc_header_size);
  unsigned char *end_of_block = (unsigned char *) pointer + size;
  int i;
  int corrupted = 0;

  /* count and check in terms of int - 4 bytes at a time */
  for (i = 0; i < __rtc_header_size/sizeof(int); i++)
    if (start_of_block[i] != (unsigned int) HEADER_MAGIC_COOKIE)
     return(RTC_BAD_HEADER);

  /* count and check in terms of char - 1 byte at a time */
  for (i = 0; i < __rtc_footer_size; i++)
    if (end_of_block[i] != (unsigned char) FOOTER_MAGIC_COOKIE)
     return(RTC_BAD_FOOTER);

 return(corrupted);
}

#pragma OPTIMIZE ON
/* Much of the code in this funcion is what is already present in
   attach_command. */
char *
get_executable_path (int pid)
{
  int (*getcommandline_ptr) (char *, size_t , size_t , pid_t ) = NULL;
  struct utsname uts;
  boolean pstat_getcommandline_available = false;
  struct pst_status buf;
  int sys_ver = 0;
  char cmdline[MAXPATHLEN + 1];
  char *exec_file = NULL, *s1;
  /* Carl Burch/Sunil S JAGag06697
   1) Run uname and check if we are on a system where pstat_getcommandline () 
   system call exists. 
   2) Use shl_findsym() to set a function pointer to call 
   pstat_getcommandline() for the PA code. 
   It's cleaner to just use the function pointer to call 
   pstat_getcommandline() in the IPF code as well. */
#ifdef GDB_TARGET_IS_HPPA
  uname (&uts);
  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
    perror ("uname: Couldn't resolve the release identifier of the " 
            "Operating system.");

  if (sys_ver >= 23) /* This will work for 11.23, 11.31 and 11.41 */
    {
      struct shl_descriptor desc;
      int index = 1;
      while (shl_get_r (index, &desc) != -1)
        {
          int shl_stat = 0;
          index++;
          if (strstr (desc.filename, "/libc."))
            {
              shl_stat = shl_findsym (&desc.handle, "pstat_getcommandline",
                                      TYPE_PROCEDURE, &getcommandline_ptr);
              if (!shl_stat)
		{
      		  pstat_getcommandline_available = true;
	          break;
		}
	    }
        }
    }
#else
  getcommandline_ptr = pstat_getcommandline;
  pstat_getcommandline_available = true;
#endif

  if (getcommandline_ptr && pstat_getcommandline_available)
    {
      if (getcommandline_ptr (cmdline, sizeof(cmdline), 1, pid) != -1)
        exec_file = strdup (cmdline);
    }
  else if (pstat_getproc (&buf, sizeof (struct pst_status), 0, pid) != -1)
    exec_file = strdup (buf.pst_cmd);

  /* JAGaf55033.*/
  /* pst_cmd would have the executable pathname + arguments .*/
  /* The below check is duplicated later. To be rewritten as part 
     of code clean up.*/
  /* Truncate the exec_file string to the first space/tab. */
  s1 = exec_file;
  while ( *s1 != '\0' && *s1 != ' ' && *s1 != '\t' )
    s1++;
  *s1 = '\0';
  return exec_file;
}

/* Determine the initial value of the stack pointer for the main thread. 
   On PA, it's the starting virtual address of the stack segment.
   On IPF, it's the last byte of the stack segment. */
static void*
determine_stack_base ()
{
  struct pst_vm_status buf = {0};
  int count = 0;
  int i = 0;
  long stk_base = 0;
  long page_size = sysconf (_SC_PAGE_SIZE);

  count = pstat_getprocvm (&buf, sizeof(buf), 0, i);
  while ((count > 0) && (buf.pst_type != PS_STACK)) 
    {
      count = pstat_getprocvm (&buf, sizeof(buf), 0, i);
      i++;
    }

  if (count > 0)
    {
#ifdef HP_IA64
      /* On IPF stack grows in the opposite direction and we need to
         calculate where it starts. pst_length indicates the number
         of pages allocated for the stack. */
      return (char*) (unsigned long) ((buf.pst_vaddr + 
				       buf.pst_length * page_size) - 1); 
#else /* HP_IA64 */
      return (char*) (unsigned long) buf.pst_vaddr;
#endif /* HP_IA64 */
    }
  else
    {
      __rtc_event (RTC_STACK_MISSING, 0, 0, 0);
      return 0;
    }
}

struct wrappers
{
    char * name;
    void * funtion_pointer_pointer;
} libc_wrappers[]  = 

{
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
            {"_start",  (void *) &libc__start},
            {"__mmap64", (void *) &libc_mmap64},
#endif         

            {"sigstack", (void *) &libc_sigstack},
            {"sigaltstack", (void *) &libc_sigaltstack},
            {"_exit", (void *) &libc__exit},
            {"malloc",  (void *) &libc_malloc},
            {"free",  (void *) &libc_free},
            {"calloc",  (void *) &libc_calloc},
            {"realloc", (void *) &libc_realloc},
            {"valloc", (void *) &libc_valloc},
            {"mmap", (void *) &libc_mmap},
            {"munmap", (void *) &libc_munmap},
            {"mprotect", (void *) &libc_mprotect},
            {"sbrk", (void *) &libc_sbrk},
            {"brk", (void *) &libc_brk},
            {"shmat", (void *) &libc_shmat},
            {"shmdt", (void *) &libc_shmdt},
            {"exit", (void *) &libc_exit},
            {"memcpy", (void *) &libc_memcpy},
            {"memccpy", (void *) &libc_memccpy},
            {"memset", (void *) &libc_memset},
            {"memmove", (void *) &libc_memmove},
            {"bcopy", (void *) &libc_bcopy},
            {"bzero", (void *) &libc_bzero},
            {"strcpy", (void *) &libc_strcpy},
            {"strncpy", (void *) &libc_strncpy},
            {"strcat", (void *) &libc_strcat},
            {"strncat", (void *) &libc_strncat},
#ifdef HP_IA64
            {"open", (void *) &libc_open},
            {"close", (void *) &libc_close},
            {"fopen", (void *) &libc_fopen},
            {"fclose", (void *) &libc_fclose},
            {"dup", (void *) &libc_dup},
            {"dup2", (void *) &libc_dup2},
            {"creat", (void *) &libc_creat},
            {"freopen", (void *) &libc_freopen},
            {"fcntl", (void *) &libc_fcntl},
#endif /* HP_IA64 */
#if 1 && HP_VACCINE_DBG_MALLOC_IA64
            {"gets", (void *) &libc_gets},
            {"fgets", (void *) &libc_fgets},
            {"getcwd", (void *) &libc_getcwd},
            {"getwd", (void *) &libc_getwd},
            {"fread", (void *) &libc_fread},
            {"read", (void *) &libc_read},
            {"pread", (void *) &libc_pread},
            {"readv", (void *) &libc_readv},
            {"sprintf", (void*) &libc_sprintf},
            {"vsprintf", (void *) &libc_vsprintf},
            {"snprintf", (void *) &libc_snprintf},
            {"vsnprintf", (void *) &libc_vsnprintf},
            {"strftime", (void *) &libc_strftime},

            /* wide character routines */
            {"wcscat", (void *) &libc_wcscat},
            {"wcsncat", (void *) &libc_wcsncat},
            {"wmemcpy", (void *) &libc_wmemcpy},
            {"wmemmove", (void *) &libc_wmemmove},
            {"wcscpy", (void *) &libc_wcscpy},
            {"wcsncpy", (void *) &libc_wcsncpy},
            {"wmemset", (void *) &libc_wmemset},
            {"fgetws", (void *) &libc_fgetws},
            {"swprintf", (void*) &libc_swprintf},
            {"vswprintf", (void *) &libc_vswprintf},
#if 0
            {"mbstowcs", (void *) &libc_mbstowcs},
            {"wcstombs", (void *) &libc_wcstombs},
#endif
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */
            
#if HP_VACCINE_DBG_MALLOC_IA64
            {"longjmp", (void *) &libc_longjmp},
            {"_longjmp", (void *) &libc__longjmp},
            {"siglongjmp", (void *) &libc_siglongjmp},
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */
};

/* Get the pointers to the libc's allocation/deallocation routines. */
void
obtain_backdoor_entry (void)
{
  struct shl_descriptor desc;

  /* Locate the C library's mallocator for our own use. It is quite
     likely the application does not use this mallocator but for the
     purposes of detecting leaks, it should not matter.
  */

#ifdef DLD_RTC
  struct shl_descriptor *dld_desc;
  if (shl_get(-1,&dld_desc) != -1) {
    if (!dld_malloc) {
      shl_findsym(&(dld_desc->handle), "MM_malloc", TYPE_PROCEDURE, &dld_malloc);
    }
    if (!dld_free)
      shl_findsym(&(dld_desc->handle), "MM_free", TYPE_PROCEDURE, &dld_free);
    if (!dld_mmap)
      shl_findsym(&(dld_desc->handle), "mmap", TYPE_PROCEDURE, &dld_mmap);
    if (!dld_munmap)
      shl_findsym(&(dld_desc->handle), "munmap", TYPE_PROCEDURE, &dld_munmap);
  }
#endif // DLD_RTC
  int index = 1;
  bool found_mallocng = 0;
  bool found_libc = 0;

  while (shl_get_r (index, &desc) != -1)
    {
      index++;

      /* Let's first check if libmallocng is used ! */
      if (strstr (desc.filename, "/libmallocng."))
       {
          int i;

          found_mallocng = 1;

          /* Can found_libc be true already ?
           * No, as we break out of the loop when libc is found.
           * If libc comes before libmallocng, then symbols would get resolved
           * to libc instead of libmallocng.
           *
           * From shl_get_r (3X) man page -
           ----------------------------------------------------------------
           Returns information about currently loaded libraries, including
	   those loaded implicitly at startup time.  The index argument is
	   the ordinal position of the shared library in the shared library
	   search list for the process.
           ----------------------------------------------------------------
           * This ensures that we would get the libmallocng descriptor
           * ahead of libc, if libmallocng was loaded before libc.
           */

          for (i = 0; i < sizeof (libc_wrappers)/sizeof(libc_wrappers[0]); i++)
            {
              /* Not doing the error checking for shl_findsym()
               * for libmallocng. It doesn't have all the libc
               * functions we are looking for, so getting a non-0
               * return is expected here for some symbols.
               */
              shl_findsym (&desc.handle,
                           libc_wrappers[i].name, TYPE_PROCEDURE,
                           libc_wrappers[i].funtion_pointer_pointer);

            }
        }

      if (strstr (desc.filename, "/libc."))
        {
          int i;

          found_libc = 1;

          for (i = 0; i < sizeof (libc_wrappers)/sizeof(libc_wrappers[0]); i++)
	  {
              /* Check if the symbol was already resolved to
               * libmallocng, in that case, we want the function to
               * go to libmallocng itself.
               */
              if (   found_mallocng
                  && *(uintptr_t *)(libc_wrappers[i].funtion_pointer_pointer))
                {
                  continue;
                }

	      if (shl_findsym (&desc.handle, libc_wrappers[i].name, TYPE_PROCEDURE,
			  libc_wrappers[i].funtion_pointer_pointer))
	      {
		  fprintf (stderr, " Memory debugging is not supported for this application.\n"
			  " This is either due to a failure of the 'shl_findsym'\n"
			  " routine or due to the '%s' routine missing from\n "
			  " the default library (libc)", libc_wrappers[i].name);
		  abort ();
	      }
	  }
          /* Just to be sure this is not some junky library with
             the name libc, ping for an entry point. Surely if we
             saw a library with a definition of shmdt(), we have
             seen libc ! 
          */
          if (found_libc && libc_shmdt)
            break;
        }

      if (strstr (desc.filename, "/libpthread."))
        {
           /* No exit/abort on error, if its not found then
              the case is handled later.
            */
           if (shl_findsym (&desc.handle, "pthread_self", TYPE_PROCEDURE,
	 		   &libpthread_pthread_self))
             {
               libpthread_pthread_self = NULL;
             }

	   if (shl_findsym (&desc.handle, "pthread_kill", TYPE_PROCEDURE,
			    &libpthread_pthread_kill))
             {
               libpthread_pthread_kill = NULL;
             }

	   if (shl_findsym (&desc.handle, "pthread_equal", TYPE_PROCEDURE,
			    &libpthread_pthread_equal))
             {
               libpthread_pthread_equal = NULL;
             }
        }
    }

  index = 1;
  while (shl_get_r(index, &desc) != -1)
    {
      index++;

      if (strstr(desc.filename, "/libdl.") || strstr(desc.filename,"/libdld."))
        {
          if (shl_findsym (&desc.handle, "shl_unload",
                       TYPE_PROCEDURE, &libdld_shl_unload))
	    {
		perror (" Memory debugging is not supported for this application.\n"
                        " This is either due to a failure of the 'shl_findsym'\n"
		        " routine or due to the 'shl_unload' routine missing from\n"
                        " the default library (libc)");

	      libc_exit(1);
	    }
          if (shl_findsym (&desc.handle, "dlclose",
                       TYPE_PROCEDURE, &libdld_dlclose))
	    {
		perror (" Memory debugging is not supported for this application.\n"
                        " This is either due to a failure of the 'shl_findsym'\n"
		        " routine or due to the 'dlclose' routine missing from\n"
                        " the default library (libc)");

	      libc_exit(1);
	    }
          if (libdld_shl_unload)
            break;
        }
    }

/* JAGaf15615 */
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
  /* Now lookup the libcl entry points of interest. This is done to
     avoid a nasty libcl/OS bug with probe instruction. Search for
     probe in hppa-rtc.c file for details. We do not need this on PA64/IA64.
  */
  libcl_stuff (); /* Defined in hppa-rtc.c */
#endif /* !HP_IA64 && !GDB_TARGET_IS_HPPA_20W */
}


/* Whenever we are asked to allocate a chunk, we will allocate a bunch
   of them (CHUNK_INFO_LOT_SIZE) and put the remaining in the free list.
   When we are asked to allocate the next time, we can just return from
   the free list. */

static rtc_chunk_info *
allocate_chunk_info (void)
{
  rtc_chunk_info * chunk = 0;
  int i = 0;

  if (free_chunk_info)
    { 
      chunk = free_chunk_info;
      free_chunk_info = free_chunk_info->next;
      return chunk; 
    }

  chunk = libc_calloc (CHUNK_INFO_LOT_SIZE, sizeof (rtc_chunk_info));

  if (!chunk)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      return 0;
    }
  
  /* Link them into a list ... */

  for (i=0; i < CHUNK_INFO_LOT_SIZE - 1; i++)
    chunk[i].next = &chunk[i+1];
  chunk[i].next = 0;
  free_chunk_info = chunk->next;
  return chunk;
}

/* Just add it to the free list. */
static void 
deallocate_chunk_info (rtc_chunk_info * c_i)
{
    c_i->next = free_chunk_info;
    free_chunk_info = c_i;
}

/* allocate STACK_TRACE_LOT_SIZE of stack_trace_buffer and 
   stack_trace_info_buffer. 
   stack_trace_buffer holds the lists of pc values & corresponding
   library index seperated by (0,0) tuple between each list. 
   list size is __rtc_frame_count. */
static void
allocate_stack_trace_buffers (void)
{
  /* The stack trace buffer is a permanent data structure. We can
     never free it as there could be many different pointers into
     it from several stack_trace_info structures. So it is alright
     if the initial pointer is sacrificed.
  */

  stack_trace_buffer = libc_calloc (STACK_TRACE_LOT_SIZE, 
    (__rtc_frame_count + 1)*sizeof(struct pc_tuple));

  if (!stack_trace_buffer)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      return;
    } 

  /* allocate stack_trace_info_buffer */
  stack_trace_info_buffer = libc_calloc (STACK_TRACE_LOT_SIZE,
                                             sizeof (struct stack_trace_info));
  if (!stack_trace_info_buffer)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      return;
    } 
  __rtc_stack_trace_count = STACK_TRACE_LOT_SIZE;
}


/*
 * JAGae73849:	
 * This function records all mmmaps (PS_MMF) done by a process at the time
 * this function was called. The function is called in __rtc_initialize to
 * capture mmaps done by pthread library even before librtc gets initialized.
 */
static void
find_mmaps_in_startup ()
{
   long len = 0;
   int i = 0, count = 0;
   struct pst_static pst;
   struct pst_vm_status buf;

   if (pstat_getstatic(&pst, sizeof(pst), (size_t)1, 0) == -1)
     perror("pstat_getstatic");

   count = pstat_getprocvm (&buf, sizeof(buf), 0, i);

   while (count > 0) 
     { 
#ifndef DLD_RTC
       if (buf.pst_vaddr != NULL && buf.pst_type == PS_MMF &&
           !(buf.pst_flags & PS_SHARED_LIBRARY))
#else // DLD_RTC
       if ((buf.pst_vaddr != NULL) &&
           (buf.pst_permission & PROT_WRITE) &&       // writeable region
           ((!(buf.pst_permission & PROT_EXEC) &&     // shared library data segment
             (buf.pst_flags & PS_SHARED_LIBRARY)) ||
            (!(buf.pst_flags & PS_SHARED_LIBRARY) &&  // anonymously mmap'd
             (buf.pst_permission & PROT_EXEC))))      // dld heap region
#endif // DLD_RTC
         {
#if 0
           /* Following changes will be allowed once we get the
              kernel patch and is available for building librtc.
              At that point of time we should unifdef this piece 
              of code. Remove this comment. Once the patch is 
              available we need to make sure it works for PA and
              IA.
           */
           /* kernel does not give size correctly if there
              is hole in mmapped segment because of partial
              munmap earlier.
            */
           if (buf.pst_flags & PS_HOLES)
              continue; 
#endif // 0
           /* pstat_getprocvm returns size in the native page size */
	   len = (long)(buf.pst_length * pst.page_size);

#ifndef DLD_RTC
           rtc_record_malloc (((char *) (unsigned long) buf.pst_vaddr), len, 
	     NON_HEAP_PREINIT_BLOCK, NON_PADDED_BLOCK);
#else // DLD_RTC
           dld_heap_region_add((char *)(buf.pst_vaddr), len, TRUE);
#endif // DLD_RTC
         }
       i++;
       count = pstat_getprocvm (&buf, sizeof(buf), 0, i);
     } /* while */
}


/* Baskar, JAGae68100
   One-Time initialization routine... called by rtc_initialize
*/
static void
__rtc_initialize (void)
{
  int i = 0;
  struct shl_descriptor shl_desc;
  static int rtc_initialized = 0;

  /* Avoid recursive initialization of RTC. */
  if (rtc_initialized)
    return;

  rtc_initialized = 1;

#ifdef DLD_RTC
  OBTAIN_BACKDOOR_ENTRY();
#else // DLD_RTC
  heap_base = (void *) libc_sbrk(0);
#endif // DLD_RTC

  shlib_info = libc_malloc (sizeof (struct shlib_info) * shlib_info_size);
  if (!shlib_info)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      return;
    }
  else
    {
      libc_memset((void *)shlib_info, 0, sizeof (struct shlib_info)
                                   * shlib_info_size);
    }

  page_size = getpagesize ();
        
  total_pages = 0x00100000;
  chunks_in_page =  (rtc_chunk_info **)
                     libc_malloc (total_pages * sizeof (rtc_chunk_info *));
  if (!chunks_in_page)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      return;
    } 
  else
    {
      libc_memset((void *)chunks_in_page,
                  0, total_pages * sizeof (rtc_chunk_info *));
    }

  stack_base = determine_stack_base ();
  DEBUG(printf("Stack base of main thread = %p\n", stack_base);)

  /* Create and initialize a mutex for regulating access to our
     data structures. We may be linked with a threaded program
     and if we are, there could be concurrent calls to malloc ()
     from the different threads. We don't want them to step on
     each other's toes ...
  */
  __rec_mutex_init (&mutex);
  __rec_mutex_init (&unmap_mutex);
  __rec_mutex_init (&split_region_mutex);
  __rec_mutex_init (&rtcapi_mutex);

  /* Install a fork handler : If a thread calls forks, in the
     child it will be the only thread. So if in the parent,
     some other thread had locked and held the mutex, then the
     mutex will be locked in the forked copy. However the thread
     that locked the mutex is not running in the child to unlock
     it, leading quickly to a deadlock! The fork handler
     mechanism allows the forking thread to obtain a lock on
     the mutex and unlock it in both the parent and the child.

       -- srikanth, 000222.
  */
  __thread_atfork (do_a_down, do_an_up, do_an_up);


  /* Determine the real heap base; the one we captured above is the
     heap base now. But malloc calls from shared library initializers
     could have resulted in the adjustment of the break value. What
     was the real base of heap when the program started ?
  */
  shl_get_r (0, &shl_desc); 
  real_heap_base = (char *) shl_desc.dend;

  if (__ismt)
    {
      __rtc_register_file = libc_malloc (RTC_REGISTER_BUFFER_SIZE
                                             * RTC_MAX_THREADS);
      libc_memset((void *)__rtc_register_file,
                  0,
                  RTC_REGISTER_BUFFER_SIZE * RTC_MAX_THREADS);
      register_file_size = RTC_REGISTER_BUFFER_SIZE * RTC_MAX_THREADS;

#ifdef REGISTER_STACK_ENGINE_FP
      __rtc_RSE_file = libc_malloc (RTC_MAX_THREADS * RSE_BUFFER_SIZE);
      libc_memset((void *)__rtc_RSE_file,
                  0, RTC_MAX_THREADS * RSE_BUFFER_SIZE);
      __rtc_RSE_file_size = RTC_MAX_THREADS * RSE_BUFFER_SIZE;
#endif /* REGISTER_STACK_ENGINE_FP */
    }
  else 
    { 
      __rtc_register_file = libc_malloc (RTC_REGISTER_BUFFER_SIZE);
      libc_memset((void *)__rtc_register_file, 0, RTC_REGISTER_BUFFER_SIZE); 
      register_file_size = RTC_REGISTER_BUFFER_SIZE;
#ifdef REGISTER_STACK_ENGINE_FP
      __rtc_RSE_file = libc_malloc (RSE_BUFFER_SIZE);
      libc_memset((void *)__rtc_RSE_file, 0, RSE_BUFFER_SIZE);
      __rtc_RSE_file_size = 1 * RSE_BUFFER_SIZE;
#endif /* REGISTER_STACK_ENGINE_FP */
    }

  if (!__rtc_register_file)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      return;
    }

#ifdef DLD_RTC
  static void update_shlib_info ();
  update_shlib_info();
#endif // DLD_RTC

  /* JAGaf88949 - allows user to change RTC options for
   * attach RTC by setting GDBRTC_CONFIG to point to the
   * rtcconfig file.  Note: this code has to go before
   * find_mmaps_in_startup since find_mmaps will call
   * rtc_record_malloc... hence cached_stack_trace
   */
  /* JAGag05263 - Added a check to see that for
     attach RTC case we are not in the not batch RTC mode.
   */

  if (!in_batch_mode() && getenv ("GDBRTC_CONFIG") != NULL)
    {
      if (!init_config (NULL, false)) /* malloc must have failed */
        perror("init_config failed");

      if (!process_config())
        perror("process_config failed");
    }


  /*
     QXCR1000916099: For batch mode RTC, the shared library is
     not updated at the beginning. This leads to wrong corruption results.
     The shlib_info_invalid is set to 1 here (which is normally done by gdb),
     so that update_shlib_info() gets called at an appropriate time and
     shared library segments are detected early.
  */
  if (in_batch_mode())
    {
      shlib_info_invalid = 1;
    }

 /* JAGae73849	*/
 /* Till we get a fix from kernel team, we will skip the following
    call if _SKIP_INITIALIZER_MMAP=1. Once we get the fix from kernel, this
    flag will have not be needed.
    The defect kernel has is that it does not give you the updated
    mmap size and starting address if the mmap is partially 
    munmapped earlier.
 */

  if (!getenv("_SKIP_INITIALIZER_MMAP"))
     find_mmaps_in_startup();

  rtc_started = 1;

  DEBUG(printf("__rtc_initialize done\n");)

  /* JAGaf80611 - create the first entry which contains all the addrs
   * between real_heap_base & heap_base.  For explanation why
   * this is an array rather than a linked list go to rtc_split_special_region 
   */
  region_list = libc_malloc(sizeof(region_chunk) * max_regions);
  region_list[0].start = real_heap_base;
  region_list[0].end = heap_base;
  region_cnt=1;

  /* JAGaf87040 */
  high_mem.brk_val = heap_base; 
  high_mem.pc_tuple = 0;       
  high_mem.base = 0;       
  high_mem.end = 0;       
  high_mem.count = 0;

  heap_end_value = heap_base;

#ifdef HP_IA64
  if (1 == __rtc_check_openfd && batch_mode_env == BATCH_MODE_ON)
    /* Currently for batch mode only */
    rtc_initialize_openfd ();
#endif /* HP_IA64 */
}

static void 
rtc_initialize (void)
{
  /* Baskar, JAGae68100,
     calling one-time initialization routine..
     I guess __thread_once is an atomic operation

     __thread_once calls the initialization routine only
     if __ismt is non-zero and so the "if (__ismt)" condition
  */

  if (__ismt)
     __thread_once (&init_once, __rtc_initialize);
  else
     __rtc_initialize ();	

}

/* Save this pc list in the stack trace buffer. */
static struct stack_trace_info *
preserve_stack_trace (void **pc)
{
  int i = 0;
  struct stack_trace_info * retval = 0;

  if (__rtc_stack_trace_count == 0) /* no room */
    allocate_stack_trace_buffers ();
         
  /* Suresh: Jan 08: Make the corresponding lib index as -1, when
     we are storing the allocation initially. Only when processing 
     a dlclose() event, these are made to point to the unloaded 
     library index.
     The lib index of ABSOLUTE_ADDRESS_INDEX(-1) is a special value 
     to indicate that its a live and loaded library module
  */
  for (i=0; i < __rtc_frame_count; i++)
   {
    stack_trace_buffer[i].pc = pc[i];
    stack_trace_buffer[i].library_index_of_pc = ABSOLUTE_ADDRESS_INDEX;
   }

  /* Suresh, Jan 08: We set the delimiter pc and index to 0 */
  stack_trace_buffer[i].pc = 0;
  stack_trace_buffer[i].library_index_of_pc = 0;

  retval = stack_trace_info_buffer;
  retval->pc_tuple = stack_trace_buffer;
  
  __rtc_stack_trace_count --;
  stack_trace_info_buffer++;
  stack_trace_buffer += __rtc_frame_count + 1;

  return retval;
}

/* cached_stack_trace () : given a stack trace of __rtc_frame_count 
   # of pc values, retrieve a pointer to an identical trace if one is 
   present. Store the trace and return a pointer otherwise.
*/

/* JAGaf87040 - add ability to sum up allocation size with stack trace */
struct stack_trace_info *
cached_stack_trace (void **pc, long long int alloc_size)
{
  long hash_val = 0;
  struct stack_trace_info * stk_trc = 0;
  int i = 0;

  if (pc == 0 || pc[0] == 0)
    return 0;

  /* hash key is obtained by adding all the PCs and getting the bottom 
     16 bits. */
  hash_val = 0;
  for (i=0; i < __rtc_frame_count; i++)
    hash_val += (long) pc[i];

  hash_val &= (STACK_TRACE_HASH_SLOTS-1); 

  stk_trc  = stack_trace_hash_table [hash_val];

  while (stk_trc != 0)
    {
      /* Be sure not to over run the old and new traces, as the frame
         count value changes during a run. When we preserve a trace we
         always terminate it with a zero.
      */
      for (i=0; stk_trc->pc_tuple[i].pc && i < __rtc_frame_count; i++)
        if (stk_trc->pc_tuple[i].pc != pc[i])
          break;
      
      /* Why do we need to check for stk_trc->pc_tuple[i].pc == pc[i] ?? 
         Wouldn't we miss holding the "extra PCs(over and above the previous 
         smaller frame)" in the current stack trace, and hence we will not 
         be honouring the new "_rtc_frame_count" for this current stack 
         capture.. ??
       */
      if (i == __rtc_frame_count || stk_trc->pc_tuple[i].pc == pc[i])
        {
          /* JAGaf87040 */
          stk_trc->sum_alloc += alloc_size; 
          return stk_trc;   
        }
      else
        stk_trc = stk_trc->next;
    }

   /* Looks like this stack trace is unknown thus far : Add it to the
      head of the linked list.
   */

   stk_trc = preserve_stack_trace (pc);
   stk_trc->next = stack_trace_hash_table [hash_val];
   stack_trace_hash_table [hash_val] = stk_trc;
  
   /* JAGaf87040 */
   stack_trace_hash_table[hash_val]->sum_alloc = alloc_size; 
   return stk_trc; 
}

/* This function is called after gdb notifies the librtc that the 
   current shlib information is invalid (when the shlib gets loaded). 

   Suresh, Jan 08: QXCR1000573545 - Incorrect stack trace after a 
   dlclose.  We don't throw away the shlib_info entries corresponding 
   to the unloaded libraries as we used to earlier. Instead, we need to mark 
   it saying its unloaded. 
   This routine could be called direcly when a shlib is unloaded or 
   indirectly when gdb notifies librtc by setting the shlib_info_invalid 
   flag. Note that we have RTC wrappers for unload(shl_unload & dlclose ) 
   but not for a library load. 
   In fact there could be multiple library loads before this routinue 
   is called. 
   So we could end up discovering here that
   a) 1 or more new libraries from last time + finding one of them unloaded. (OR)
   b) finding one of them is unloaded  (OR)
   c) just 1 or more new libraries from last time.

   Looks like if this routine is called because of a dlclose, which 
   could close all dependent libraries as well that was loaded 
   automatically with the library at dlopen time. Which means case 
   a) and b) above could actually have more than 1 library unloaded. 
   So its safe to assume NOTHING and process all libraries 
   returned by iterating thru shl_get_r()
*/
static void
update_shlib_info ()
{
  int i = 0, j = shlib_info_count, k =0;
  struct shl_descriptor shl_desc;
  long text_address;
  int matching_index = -1;

  /* Reset the flag 'is_it_loaded' of all libraries in our table to 
     start with, as there could be multiple libraries unloaded on a 
     given shl_unload(?) or dlclose()
     Performance wise, this algo loops thru the library list multiple times 
     and may be O(n^2) in the worst case, but given that the no of libraries
     in any real-world application is atmost in the order of 100s, 
     this overload might be OK. More important is to work correctly and 
     take care of all cases of dlclose() and shl_unload()
   */
  for (k=0; k < shlib_info_count ; k++)
   shlib_info[k].is_it_loaded = FALSE;

  /* k = loop index for loop that finds out the matching entry in our
         table for the current shl_desc
     i = is the no of times we have done shl_get_r() at any given point
     j = the current no of entries in our table 'shlib_info'. If we 
         need to add a new entry in the table, it will be at shlib_info[j]
   */
#ifdef DLD_RTC
  if (shl_get_r (-1, &shl_desc) != -1)
    {
        shlib_info[0].library_index = 0;
        libc_strcpy( (void *)shlib_info[0].library_name , shl_desc.filename);
        shlib_info[0].is_it_loaded = TRUE;
        shlib_info[0].text_start = (void *) shl_desc.tstart;
        shlib_info[0].text_end = (void *) shl_desc.tend;
        shlib_info[0].data_start = (void *) shl_desc.dstart;
        shlib_info[0].data_end = (void *) shl_desc.dend;
	DEBUG( printf ("DLD RTC dld path : %s\n", shl_desc.filename););
	DEBUG( printf ("DLD RTC dld tstart : %llx\n", shl_desc.tstart););
	DEBUG( printf ("DLD RTC dld tend : %llx\n", shl_desc.tend););
	DEBUG( printf ("DLD RTC dld dstart : %llx\n", shl_desc.dstart););
	DEBUG( printf ("DLD RTC dld dend : %llx\n", shl_desc.dend););
        j=1;
    }

#else // DLD_RTC
  for (i=0, j=shlib_info_count; shl_get_r (i, &shl_desc) != -1; i++)
    {
     /* We don't need to save the info about librtc as we are not
        going to do memory checking on this. */
      if (!strstr (shl_desc.filename, "/librtc"))
        {
          /* Suresh: Jan 08: Check if this library is already stored in our
             RTC table. If so, we need to set the is_it_loaded flag for 
             this library and note down the new start/end limits before we
             continue the loop, fetching the next shl_get_r(). 
           */
          matching_index = -1;
          for( k=0; k < shlib_info_count || k <= i ; k++)
           {
             if (!strcmp( shl_desc.filename, (const char *) shlib_info[k].library_name))
             { /* Matching entry ; Need to update the text start/end limits
                  as the same library can be loaded/unloaded multiple
                  times, and these address could be different each time
                */
               shlib_info[k].is_it_loaded = TRUE;
               shlib_info[k].text_start = (void *) shl_desc.tstart;
               shlib_info[k].text_end = (void *) shl_desc.tend;
               shlib_info[k].data_start = (void *) shl_desc.dstart;
               shlib_info[k].data_end = (void *) shl_desc.dend;
               matching_index = k;
               break;
             }
           }
          if ( matching_index == -1 ) /* No match; new entry */
          {
            /* Allocate more space in the table if we have used all of it */
            if (j == shlib_info_size)
            {
              shlib_info  = libc_realloc ((void *) shlib_info,
                  sizeof (struct shlib_info) * (shlib_info_size *= 2));
              if (shlib_info == 0)
                {
                  __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
                  return;
                }
             
             }      
             /* Add the entry */
             shlib_info[j].library_index = j;
             strcpy( (void *)shlib_info[j].library_name , shl_desc.filename);
             shlib_info[j].is_it_loaded = TRUE;
             shlib_info[j].text_start = (void *) shl_desc.tstart;
             shlib_info[j].text_end = (void *) shl_desc.tend;
             shlib_info[j].data_start = (void *) shl_desc.dstart;
             shlib_info[j].data_end = (void *) shl_desc.dend;
             j++;
           }
        }
     }
#endif // DLD_RTC

  shlib_info_count = j;
  shlib_info_invalid = 0;

  /* Chain the unloaded libraries for faster access to these entries
     during the markup*()
   */
  head_unloaded_shlib_info = NULL; 
  for (k=0; k < shlib_info_count ; k++)
  { 
   if (shlib_info[k].is_it_loaded == FALSE) 
     {
      shlib_info[k].next_unloaded = head_unloaded_shlib_info;
      head_unloaded_shlib_info = (void *) &shlib_info[k];
     }
  }

}



/* 
 * JAGaf80611 - 2 new routines below deals with pointers 
 * in the special region real_heap_base and heap_base.
 * After __rtc_intialize, there's only one entry in
 * region list which contains start=real_heap_base & 
 * end=heap_base.  If the program calls malloc after
 * librtc came into the picture and pointer is in this
 * region, split will create 2 entries to exclude this
 * new malloc pointer.  For example:  
 *
 * new malloc pointer: 400125a0
 * 
 *    original region list
 * start 400101c0, end 40018000
 *
 *    After split
 * start 400101c0, end 4001258f
 * start 400125a4, end 40018000
 * 
 * when program free (pointer), this pointer will be removed
 * from the chunk_info in a normal way.  When free(pointer)
 * is called again and it's not in chunk_info, the address
 * is in the special region but is not in the region_list
 * then it's a double free
 *
 * NOTE: these addrs are stored in an array and not a linked
 * list for a reason.  On IPF, malloc of a small size
 * result in libc returning a pointer from a small block
 * allocator which is in this special region.  Some of our
 * test cases in the test suite malloc a small size memory,
 * and purposely corrupt that memory (overrun, infobounds).
 * If we use linked list we need to allocate region_chunk
 * which is small.  When the test case corrupts memory it corrupted
 * our region_chunk.  Having an array is nice and compact.
 */


/* called when do malloc, valloc, calloc etc... */

static void
rtc_split_special_region(char *pointer, size_t size)
{
   int i = 0;
   boolean found_region = 0;
   region_chunk *temp_list = 0;
   char *alignptr = 0;

   /* JAGag32374:  Amdocs MT app corruption issue. Making this function thread safe
    * we define our own mutex for this for better concurrency 
    */
   mutex_lock (&split_region_mutex);

   /* if about to run out of space expand */
   if (region_cnt == max_regions-1)
   {
     
   /* JAGag32374 :  Amdocs MT app corruption issue. Fixed incorrect realloc 
    * size which was shrinking the region_list instead of increasing it 
    * and resulting in RTC overwriting the heap that could be subsequently 
    * allocated to the application, which could result in application crashes..
    */
      region_list=libc_realloc(region_list,sizeof(region_chunk) * max_regions * 2);
      if (!region_list)
      {
         __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
         mutex_unlock (&split_region_mutex);
         return;
      }
      max_regions=max_regions*2;
      DEBUG(printf("split_region: max_regions  = %d\n",max_regions);)
   }

   /* traverse through once to determine if need to split */

   found_region = false;
   for (i = 0; i < region_cnt; i++)
   {
     /* if == then found the node so split */ 
     if (pointer >= region_list[i].start &&
         pointer <= region_list[i].end)
     {
        found_region = true; 
        break;
     }
   }


   if (found_region)
   {
      alignptr=pointer+size+__rtc_footer_size+1;

      /* must align it */
      if ((unsigned long) alignptr % sizeof(long))
        alignptr = alignptr + (sizeof(long) - ((unsigned long) alignptr % sizeof(long)));

      assert (region_cnt < max_regions);
      /* must set the new entry first because of the end */  
      region_list[region_cnt].start = alignptr; 
      region_list[region_cnt].end = region_list[i].end;          

      /* start of region_list[i] remains the same */
      region_list[i].end =   pointer-__rtc_header_size-1;        

      region_cnt++;
   }  /* found region so split */

   mutex_unlock (&split_region_mutex);

}  /* rtc_split_special_region */


/* for double free - if in special region (real_heap_base &
 * heap_base) check if it's in region_list.  If it's not 
 * report double free.  If in region_list, the pointer was
 * malloc before librtc came into the picture.  
 */
static boolean 
in_special_region(char *pointer)
{
  int i = 0;

  if  ( ((char*)pointer >= real_heap_base) &&
        ((char*)pointer < heap_base) )
  {
     for (i=0; i < region_cnt; i++)
     {
       if (pointer >= region_list[i].start &&
           pointer <= region_list[i].end)
          return true;
     }
  } 

  return false; 
} /* in_special_region */


#pragma OPTIMIZE OFF
/* 
 * JAGaf87040 - this was created specifically for the submitter
 * so keep the naming convention as is.  Submitter would like the 
 * ability to set a breakpoint in a routine.
 */
void rtc_update_high_mem(high_mem_mark *hwm,  struct pc_tuple *pclist, 
                         long long int sum_alloc)
{
  /*
   * for user to control activation of event. 
   */ 
  int do_hwm_event=0;

  if (do_hwm_event)
    {
       __rtc_event(RTC_HIGH_MEM, hwm->base, pclist, sum_alloc);  
       do_hwm_event=0;  
    }
}
#pragma OPTIMIZE ON


/* Suresh: May 08: Convert the 2 tuple pclist to a 1 tuple pclist. This 
   routine is an API for +check. Not used internally in this file.

   The caller has to pass an allocated buffer(for o/p), that can atleast 
   store (__rtc_frame_count + 1) void* pointers. 

   if successful returns 0, else returns 1. Sets the 1-tuple pclist(2nd 
   argument) to a series of PC pointer entries terminated by 0, each entry 
   is an absolute PC address OR a special value(-1) if the address belongs 
   to a library that is currently unloaded..
*/
# define UNLOADED_LIBRARY_PC -1
int 
convert_to_1_tuple_pclist (char **pc_tuple, void **pclist)
{
  int i;
  struct pc_tuple* temp_pc_tuple = (struct pc_tuple*) pc_tuple;
  /* need to pass a valid buffer, else print error.. */
  if (pclist == NULL )
    {
       return(1);
    }

  /* Make sure we lock the pclist as well as shlib_info before we
     read these values;
     The caller can call this API anytime and we cannot assume anything 
     about the state of pc_tuple & shlib_info data structures, hence we 
     need to acquire the lock that protects these structures before we 
     read them.
  */
  mutex_lock(&mutex);
  for ( i = 0; i < __rtc_frame_count && temp_pc_tuple[i].pc != NULL ; i++)
    {
       /* If its a live index, just copy as is.. */
       if (temp_pc_tuple[i].library_index_of_pc == ABSOLUTE_ADDRESS_INDEX )
         {
           pclist[i] = temp_pc_tuple[i].pc;
         }
       else
         {
           /* relative address; If the same library is loaded at present, 
              convert the relative address to an absolute address, else 
              make it -1(UNLOADED_LIBRARY_PC)
            */
           int lib_index;
           lib_index = temp_pc_tuple[i].library_index_of_pc;
           if ( shlib_info[lib_index].is_it_loaded == TRUE )
             pclist[i] =  (void *) ((CORE_ADDR)temp_pc_tuple[i].pc + 
                           (CORE_ADDR) shlib_info[lib_index].text_start);
           else
             pclist[i] = (void *) UNLOADED_LIBRARY_PC;
         }
    }
  pclist[i] = NULL; /* terminate the list with NULL */
  mutex_unlock( &mutex);
  return 0;
}

/* This function saves the information about the currently allocated
   block in our data structures. */
static struct rtc_chunk_info *
rtc_record_malloc (char * pointer, size_t size, int heap_block,
                                                int padded)
{
  struct rtc_chunk_info c_info, *new_c_i = 0, * c_i = &c_info;
  int error = 0;
  int j = 0;
  long page_no = 0;
  void ** pc_list = 0;

  /* JAGaf87040 */
  struct stack_trace_info *cached_stk_trc = 0;

  if (rtc_disabled || pointer == 0)
    return NULL;

  /* JAGaf80611 */
  if ( ((char*)pointer >= real_heap_base) &&
       ((char*)pointer < heap_base) )
    rtc_split_special_region(pointer, size);

  heap_interval_check ();

  /* If a watched block is allocated, report. */
  if (((char *)__rtc_watch_address) >= pointer &&
      ((char *) __rtc_watch_address) < (pointer + size))
    __rtc_event (RTC_ALLOCATED_WATCHED_BLOCK, pointer, 0, 0);

  /* If the allocated block is bigger a certain size, report. */
  if (__rtc_big_block_size && size >= __rtc_big_block_size && (heap_block == NON_HEAP_PREINIT_BLOCK))
    __rtc_event (RTC_HUGE_BLOCK, pointer, 0, 0);

  page_no = PAGE_FROM_POINTER (pointer);

  c_i->padded_block = padded;
  c_i->do_not_scan = 0; /* if set, do not scan for leaks. */
  c_i->dptr = NULL;

  if (heap_block == NON_HEAP_BLOCK || heap_block == HEAP_BLOCK)
    {
      c_i->heap_block = heap_block;
      c_i->preinit_mmap = 0;
    }
  else if (heap_block == NON_HEAP_PREINIT_BLOCK) 
    {
      c_i->heap_block = NON_HEAP_BLOCK;;
      c_i->preinit_mmap = 1;
    } 
    
  c_i->base = pointer;
  c_i->end = pointer + size;
  c_i->next_leak = 0;
  c_i->old_leak = 0;
  c_i->freed_block = 0;
  c_i->scanned = scanned_this_time;
  c_i->pc_tuple = 0;  /* for now */

  if (interval_in_progress)
    c_i->new = 1; /* new allocation */
  else
    c_i->new = 0; /* ignore */
  
  /* Suresh: Jan 08: pc_list would continue to be a 1-array pc list
     rather than a 2-array pc_tuple, because we self unwind the 
     current stack and get the pc list here 

     But when we call the cached_stack_trace() to see if there
     is a matching stack trace, we take care of correctly comparing
     this 1-array pc_list with c_i->pc_tuple[].pc !!
   */
  if (__rtc_frame_count)
    {
      pc_list = alloca (__rtc_frame_count * sizeof (void *));
#ifndef DLD_RTC
      libc_memset(pc_list, 0, __rtc_frame_count * sizeof (void *));
#else // DLD_RTC
      libc_memset(pc_list, 0, __rtc_frame_count * sizeof (void *));
#endif // DLD_RTC
    }
  else
    {
      pc_list = 0;
    }

  /* Do not bother collecting the stack trace, if the size of this
     block is smaller than the user specified minimum leak size. We
     will still be able to report leaks smaller than this, but without
     a stack trace.
     */

  if ((size >= (size_t)(unsigned) __rtc_min_leak_size) && 
      (__rtc_frame_count != 0))
    {
      /* Let's do the unwinding and get the pc list. */
      get_frame_pc_list (pc_list, __rtc_frame_count);
    }
  
  /*
   * Record the heap end value here. For multithreaded application, it
   * is not safe to call libc_sbrk(0) during heap analysis. If someother thread
   * is stopped in libc_malloc (), we would deadlock since the C
   * library obtains its own mutex. This heap_end_value will be used in the
   * function __rtc_heap_info().
   *
   * Record the value before acquiring the mutex lock. This will avoid any
   * deadlock that may happen due to another thread holding the libc mutex
   * and trying to acquire the rtc mutex.
   */
  heap_end_value = libc_sbrk(0);

  mutex_lock (&mutex);

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
  /* Update statistics - Done under lock so that these are more
     reliable stats than the non_RTC_* statistics */
  DEBUG_MALLOC( RTC_outstanding_allocation_count++;
                RTC_outstanding_allocation_size+=size; )
#endif

  /* JAGaf87040 - calling cached_stack as before with addition size 
   * information.  Note:  in previous version, cached_stack_trace 
   * returned the value of pc which can be null.  After 87040, 
   * it returns ptr to stack_trace_info which contains the pc.  
   * It still has to accommodate for null pc.
   */
  cached_stk_trc = cached_stack_trace (pc_list, c_i->end-c_i->base);
  if (cached_stk_trc)
    c_i->pc_tuple = (void *)cached_stk_trc->pc_tuple;
  else
    c_i->pc_tuple = 0;

#if HP_VACCINE_DBG_MALLOC_IA64
    if (__rtc_register_bounds && the_rtc_oob_callbacks != NULL) {
        /* register the malloc/mmap object's bounds info */
        assert(the_rtc_oob_callbacks->register_object != NULL);
        RTC_ObjectKind obj_kind = 
               (heap_block == HEAP_BLOCK) ? RTC_ObjectKind_HeapObject
                                          : RTC_ObjectKind_NonHeapObject;
        /* Suresh: May 08: Passing 2-tuple pclist now that we have a 
           conversion API to convert it to a 1-tuple pclist

           +check(rtc_bounds.c) can now handle this correctly by 
           calling the API - convert_to_1_tuple_pclist (char** pc_tuple, 
           void **pclist), if it wants to interpret this 2-tuple list 
           correctly.  Just handling this as an opaque object should not 
           require any change in +check code.
         */
        the_rtc_oob_callbacks->register_object(pointer, size, obj_kind, 
                                 (char*)c_i->pc_tuple,  __rtc_frame_count);
    }
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

  new_c_i = allocate_chunk_info ();
  if (new_c_i)
    *new_c_i = *c_i;
  else
   {
    mutex_unlock( &mutex);
    return NULL;
   }
#if (!defined(__LP64__) && defined(HP_IA64))
  if (__rtc_check_string && !(__rtc_retain_freed_blocks) &&
      chunks_in_page[page_no] && 
      (chunks_in_page[page_no] == chunks_in_page[page_no+1]))
    {
      /* We need to update the chunks_in_page pointer when
         we update a new chunks_info in the same page.
         For following example,
         ptr1 = malloc (600); page_no = 263509
         ptr2 = malloc (4194304); page_no = 263509
         free (ptr1);
         ptr1 = malloc (600); page_no = 263509

         While allocating 600 second time, it updates chunks_in_page
         with new chunks_info at same page. So populated pages
         will have wrong pointer. So we update the chunks_in_page 
         such that big chunk will be the starting chunks_info
         followed by the new chunks_info that is recorded and also
         all other chunks_info of that page. */
      struct rtc_chunk_info *tmp_c_i = 0;
      tmp_c_i = chunks_in_page[page_no];
      new_c_i->next = tmp_c_i->next;
      tmp_c_i->next = new_c_i;
    }
  else
    {
      new_c_i->next = chunks_in_page[page_no];
      chunks_in_page[page_no] = new_c_i;
      /* For performance issue on string check for IPF 32 bits.
         Copy the chunks_in_page pointer for all the pages that occupy the
         big chunk of same memory. By populating this chunks_in_page for 
         the pages occupied by the big chunk results in quick searching
         in obtaining the correct chunk_info sooner. Refer CR1000844741 for
         more details. */
      if (__rtc_check_string && !(__rtc_retain_freed_blocks))
        {
          long end_page = PAGE_FROM_POINTER (pointer + size);
          if ((end_page - page_no) > 1)
            {
              for (int i = page_no + 1; i <= end_page - 1; i++)
                chunks_in_page[i] = chunks_in_page[page_no]; 
            }
        }
    }
#else
  new_c_i->next = chunks_in_page[page_no];
  chunks_in_page[page_no] = new_c_i;
#endif
  /*
   * JAGaf87040 - this routine is the heart of RTC.  We enter
   * here for requests such as malloc, calloc....
   * Record the sbrk value if it has been bumped up 
   * due to the allocation request.  This routine already
   * figured out the pc simply save it for printing.  
   * Since high_mem is a global structure, it would make
   * info leaks command incorrect if we do this assigment
   * high_mem.base = c_i->base.  Suppose c_i->base is a leak
   * after this assignment it's no longer a leak because it's
   * being "used or pointed" by a global and thus no longer a leak.
   * It's safe to do -__rtc_header_size and +__rtc_footer_size because we
   * automatically alloca little bit more at the beginning (__rtc_header_size)
   * and at the end __rtc_footer_size.  It all  belongs to this base & end
   * chunk of memory.  Routine __rtc_high_mem_info uses high_mem struct.
   * It needs to remember to +__rtc_header_size and - __rtc_footer_size before
   * saving the address to a file.
   */

  if (heap_end_value > high_mem.brk_val)
    {
      high_mem.brk_val = heap_end_value; 
      high_mem.pc_tuple = c_i->pc_tuple;
      high_mem.base = c_i->base-__rtc_header_size;
      high_mem.end = c_i->end+__rtc_footer_size;
      high_mem.count++; 
      /*
       * if user decides to use cmd info heap high-mem #
       * it automatically triggers an event to stop when
       * high water mark reaches the #.  Submitter also 
       * likes the flexibility to set a break point in routine
       * rtc_update_high_mem to do ignore.  The code below
       * provide both usage options. 
       */
      
      if (__rtc_high_mem_val == high_mem.count)
        __rtc_event(RTC_HIGH_MEM, high_mem.base, high_mem.pc_tuple,
                    cached_stk_trc ? cached_stk_trc->sum_alloc : 0);
      
      else 
        rtc_update_high_mem(&high_mem, high_mem.pc_tuple, 
                            cached_stk_trc ? cached_stk_trc->sum_alloc : 0);
      
    }
  
  /* Determine the heap end now. Turns out when the GC is
     running it is not safe even to query this value ! Also
     update our list of shared libraries if needed. GDB would
     set the variable `shlib_info_invalid' to ask us to do this.
     This is needed since we cannot ask dld for the list of
     shared libraries when GC is in progress. We can't trust
     anyone to give us as much as the time of day !
     */
  
#ifndef DLD_RTC
  if (shlib_info_invalid)
    update_shlib_info ();
#endif // DLD_RTC
  
  mutex_unlock (&mutex);
  
  return new_c_i;
}

#if HP_VACCINE_DBG_MALLOC_IA64
/* get the RTC_ObjectKind for the heap_block, if the librtc_aux.a
   support the freed_memory_check, the kind is RTC_ObjectKind_FreedHeapObject
   when free_the_block is retained, otherwise the kind is
   RTC_ObjectKind_HeapObject or RTC_ObjectKind_NonHeapObject 
 */
static RTC_ObjectKind
get_unregister_object_kind(int heap_block, int free_the_block)
{
   RTC_ObjectKind k;
   if (heap_block == HEAP_BLOCK) {
      if (__rtc_retain_freed_blocks && free_the_block &&
            rtc_oob_support_freed_memory_check)
         k = RTC_ObjectKind_FreedHeapObject;
      else
         k = RTC_ObjectKind_HeapObject;
   }
   else {
      k =  RTC_ObjectKind_NonHeapObject;
   }

   return k;
}
#endif /* HP_VACCINE_DBG_MALLOC_IA64 */

/* Remove this block from our data structures if it exists. */
static void
rtc_record_free (void * pointer, int free_the_block, int is_realloc)
{
    long page_no = 0;
    rtc_chunk_info *this_chunk_info = 0, *prev_chunk_info = 0;

    if (rtc_disabled || pointer == 0)   /* nothing to do */
      return;

    page_no  = PAGE_FROM_POINTER (pointer);

    /* Grab the lock. */
    mutex_lock (&mutex);

    this_chunk_info = chunks_in_page[page_no];

    prev_chunk_info = 0;
    while (this_chunk_info != NULL)
      {
        if (this_chunk_info->base == pointer)
          {
#if HP_VACCINE_DBG_MALLOC_IA64
             /* Suresh, Oct 07: Call OOB callbacks only for the first free of the block.
                Since we are retaining the c_i we need to be careful and not call OOB 
                callbacks if there is double free on the same pointer
              */
             /* calls OOB callbacks for all the free calls, so OOB can check 
                freed memory read/write, double free, free non mallocced object,
                etc. */
             /* only unregister the pointer if it is found */
             if (__rtc_register_bounds && the_rtc_oob_callbacks != NULL) {
                 /* unregister the freed heap object's bounds info */
                 assert(the_rtc_oob_callbacks->unregister_object != NULL);
                 RTC_ObjectKind kind = 
                     get_unregister_object_kind(this_chunk_info->heap_block, 
                                                free_the_block);
                 the_rtc_oob_callbacks->unregister_object(pointer, kind);
             }
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

            if (free_the_block)
            {
              if (this_chunk_info->heap_block)
                {
		  /* Watching this block? report. */
                  if (((char *) __rtc_watch_address) >= this_chunk_info->base &&
                      ((char *) __rtc_watch_address) < this_chunk_info->end)
                     __rtc_event (RTC_DEALLOCATED_WATCHED_BLOCK, pointer, 0, 0);
		  /* bound checks on? check for overflow and underflow. */
                  if (__rtc_check_bounds)
                    if (this_chunk_info->padded_block)
                      check_bounds (pointer, this_chunk_info->end -
                                             this_chunk_info->base, this_chunk_info->pc_tuple);
		  /* scrambling on? scramble the memory here. */
                  if (__rtc_scramble_blocks)
                    scramble_block (pointer, this_chunk_info->end -
                                             this_chunk_info->base);
	          if (__rtc_retain_freed_blocks == 0)
                  {
		    /* call the real free. */
#ifndef DLD_RTC
                    if (this_chunk_info->padded_block)
                      libc_free (((char *) pointer) - __rtc_header_size);
                    else 
                      libc_free (pointer);
#else  // DLD_RTC
                    dld_free(pointer);
#endif // DLD_RTC
                  }
		  else 
		  {
                    /* Sep 07, Suresh: Don't call the real free, instead update 
                       book-keeping to say this has been freed. We will overwrite 
                       the block with a known pattern so that we can detect issues 
                       later, if this block is ever read as a pointer value or written to..

                       Also just like we set "scanned" bit to the global 
                       "scanned_this_time" in case of recording malloc, we need to do 
                       the same for freed blocks as well. 

                       Now that we don't free the c_i on the first free, We handle double 
                       frees by checking if this freed_block bit is already set when we enter here..

		       TODO: LATER(Part II), when we really need it :)-
                        1.Need to see how we can do this for realloc(), as we directly 
                          call libc_free() there. This could be tricky as realloc(), 
                          realloc_padded(),rtc_record_munmap() and shmdt() that call this 
                          function with 2nd arg '0', meaning not to free the block, but 
                          to free the metadata(c_i) only. But if we retain the c_i, we may 
                          end up with 2(or more) c_i's for the same block, for eg., whenever 
                          lib_realloc() is able to grow/shrink the block in-place (at 
                          the same pointer location). To reduce these complications, 
                          currently we don't support this retaining freed blocks for 
                          those realloc'd, mmap'd, shmdt'd blocks that call this function with
                          2nd arg '0' and hence no dangling analysis for those blocks.

                          Note, however for certain simple realloc'd cases where we
                          directly call lib_malloc internally without calling 
                          libc_realloc, we do retain metadata(c_i) and hence support 
                          dangling analysis on those blocks as well.

                        2.(Later - Part II may be ) We need to get the free() stack 
                          trace and replace the existing allocation stack trace..( 
                          Need to take care to not overwrite the alloc stack trace if 
                          its shared by multiple c_i's..)
                          we need to do this, because its more useful to give the freeing 
                          stack trace for these blocks that have become dangling OR have
                          become corrupted(after free) because of a dangling pointer
                     */
 
                   /* Double free check */
                   if ( this_chunk_info->freed_block && __rtc_check_free)
                   {
                     if ((char*)pointer > heap_base ||
                         !(in_special_region(pointer)))
                       __rtc_event (RTC_BAD_FREE, pointer,this_chunk_info->pc_tuple, 
                                    this_chunk_info->end - this_chunk_info->base);


                    } /* if check free */

                    this_chunk_info->freed_block = 1;
                    this_chunk_info->scanned = scanned_this_time;
                    scramble_block_for_retain_freed_blocks(pointer, this_chunk_info->end - 
                                                      this_chunk_info->base);
                    break; /* So that we don't do the deallocate of c_i */
                  }
                }
              else 
                {

                  /* JAGaf80611 - double free? if pointer is outside 
                   * real_heap_base and heap_base complain.  if pointer
                   * is in this region and does not exists in region_list
                   * then complain. 
                   */ 
                  if ( __rtc_check_free)
                  {
                     if ((char*)pointer > heap_base ||
                         !(in_special_region(pointer)))
                       __rtc_event (RTC_BAD_FREE, pointer,this_chunk_info->pc_tuple, 
                                    this_chunk_info->end - this_chunk_info->base);


                   } /* if check free */

                   break;

                } /* else */
            }
            /* During realloc, check the bounds and block watch */
            else if (is_realloc)
            {
              /* Watching this block? report. */
              if (((char *) __rtc_watch_address) >= this_chunk_info->base &&
                 ((char *) __rtc_watch_address) < this_chunk_info->end)
                __rtc_event (RTC_DEALLOCATED_WATCHED_BLOCK, pointer, 0, 0);
              /* bound checks on? check for overflow and underflow. */
              if (__rtc_check_bounds)
                if (this_chunk_info->padded_block)
                  check_bounds (pointer,
                                this_chunk_info->end - this_chunk_info->base,
                                this_chunk_info->pc_tuple);
            }
	    /* Remove this from our list. */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
            /* Update stats - this is not hit for retain_freed_blocks! */
            DEBUG_MALLOC( RTC_outstanding_allocation_count--;
                          RTC_outstanding_allocation_size-= (
                             this_chunk_info->end - this_chunk_info->base);)
#endif
            if (prev_chunk_info)
              prev_chunk_info->next = this_chunk_info->next;
            else  
              chunks_in_page[page_no] = this_chunk_info->next;

#if (!defined(__LP64__) && defined(HP_IA64))
              /* For performance issue on string check for IPF 32 bits.
                 While deallocating 'this_chunk_info', for chunks spanning
                 multiple pages, nullify the corresponding chunks_in_page 
                 indices.*/
            if (__rtc_check_string && !(__rtc_retain_freed_blocks))
              {
                long end_page = PAGE_FROM_POINTER (this_chunk_info->end);
                if ((end_page - page_no) > 1)
                  {
                    for (int i = page_no + 1; i <= end_page - 1; i++)
                      chunks_in_page[i] = 0;  
                  }
              }
#endif
            deallocate_chunk_info (this_chunk_info);
            break;
          }
          prev_chunk_info = this_chunk_info;
          this_chunk_info = this_chunk_info->next;
      } /* while */
    
    /* JAGaf80611 - double free? if pointer is outside 
     * real_heap_base and heap_base complain.  if pointer
     * is in this region and does not exists in region_list
     * then complain. 
     */
    if (this_chunk_info == 0)
#ifndef DLD_RTC
      if ( __rtc_check_free )
      {
         if ((char*)pointer > heap_base || !(in_special_region(pointer)))
            __rtc_event (RTC_BAD_FREE, pointer, 0, 0);
      }
#else
    {
      dld_heap_region *heap_region = dld_heap_region_find(pointer);
      if (heap_region->pre_rtc_map == FALSE)
      {
        __rtc_event (RTC_BAD_FREE, pointer, 0, 0);
      }
    }
#endif // DLD_RTC

#ifndef DLD_RTC
    if (shlib_info_invalid)
      update_shlib_info ();
#endif // DLD_RTC

    /* Unlock. */
    mutex_unlock (&mutex);
}

/* Suresh, Oct 07: Basic function to free the block and the corresponding c_i 
   from our data structures if it exists

   Note: Can be called **only** from the GC path
*/
static void
rtc_record_free_internal (rtc_chunk_info * this_chunk_info)
{
    long page_no = 0;
    rtc_chunk_info  *c_i, *prev_chunk_info = 0;

    if (this_chunk_info == 0)   /* nothing to do */
      return;

   /* Don't free this Non-dangling block, if the block has been corrupted in the past 
      using a dangling pointer, but the dangling pointer changed subsequently and there
      are no dangling pointers at present.  This is an additional check to ensure, 
      the end user doesn't miss out this corrupted block, when he eventually fires 
      an info_corruption..
   */
    if ( RTC_FREED_BLOCK_CORRUPTED == check_freed_block(
                 (unsigned int*)this_chunk_info->base, 
                 (unsigned int) (this_chunk_info->end-this_chunk_info->base)))
      return;

    page_no  = PAGE_FROM_POINTER (this_chunk_info->base);
    c_i = chunks_in_page[page_no];

    while (c_i != NULL)
      {
        if (c_i == this_chunk_info)
          {
	    /* call the real free. */
            if (this_chunk_info->padded_block)
                libc_free (((char *) this_chunk_info->base) - __rtc_header_size);
            else 
                libc_free (this_chunk_info->base);

            /* collect statistics */
            salvaged_memory=salvaged_memory+(this_chunk_info->end-this_chunk_info->base);
            salvaged_count++;

            /*reset the member fields before putting the chunk_info in free list */
            this_chunk_info->freed_block = 0;
            this_chunk_info->scanned = scanned_this_time;

	    /* Remove this from our list. */
            if (prev_chunk_info)
              prev_chunk_info->next = this_chunk_info->next;
            else  
              chunks_in_page[page_no] = this_chunk_info->next;

            deallocate_chunk_info (this_chunk_info);
            return;
          }
          prev_chunk_info = c_i;
          c_i = c_i->next;
      }
/* Not reached */
/* If we reach here, then there is an error and RTC's data structures are corrupted
   Its a good time to ASSERT this and flag a error to catch such rare cases 
*/
  printf("warning: RTC's internal data strcuture seems to be corrected \n");
  printf(" The chunk info = %p doesn't belong to this hash bucket", this_chunk_info);
  assert (c_i != NULL );
}

/* Suresh, Sep 07: If we run out of memory because of the extreme memory pressure caused
   by retaining freed blocks, we need to do something to alleviate the situation so that
   the customer can continue to use RTC for debugging his application.

   There are several ways to salvage the freed blocks

   1. Probably the best way would be to salvage all those freed blocks that are not 
   dangling, which can be found by doing a mark GC. But we cannot do a mark GC completely
   from within the context of RTC, without GDB triggering a GC because there are a lot
   of reasons why this would fail(The reasons why our "light weight" RTC proposal failed.)
   a. We need to quieze all the threads and run GC only from one thread
   b. We need GDB to give the register_file for scanning
   c. ..
   If we can solve this somehow, then this would be a good bet 
  
   2. Salvage all the blocks, that are NOT corrupted as of now. We can do this by checking
    for the presence of the magic pattern we wrote.. But again the chances of this freeing
    a lot of memory is very less, because in a realistic application you will not find
    a lot of blocks being corrupted..

   3. How about giving a new RTC_EVENT, and from GDB implicitly run a GC on this event?
     BUt again currently when the end-user runs a GC, when he is stopped within a 
     RTC_EVENT, he is really within RTC, and we will get the classic "the current 
     thread is inside the allocator" error message. If we can relax this somehow and 
     RUN "info dangling" in silent mode somehow, then we can realy salvage a lot..

     Here is how we will do it::
     -------------------------
     We will acquire the main mutex here before we trigger an rtc_event for 
     doing internal GC

     We will have a new variant of 'info dangling' in gdbrtc.c that will do the 
     same job in a silent mode(no output messages) and call a variant of 
     rtc_info_dangling() that will not try to acquire the mutex(we already have it). 
     The only issue is GC will typically run on the main thread, whereas the thread 
     that triggered GC can be any other thread and not too sure, if there are any 
     issues here? once the GC is run, it will automatically free all non-dangling 
     blocks and return silenty to GDB, which will also quietly return back to 
     the application and control continues here after the _rtc_event() in this 
     function, and we would retry the allocation in the caller of this function
     to see if it can succeed.
*/ 
static void
salvage_freed_blocks()
{

/* TODO: Can we acquire and free the mutex here */
/* If it doesn't work, Can we then relax holding the mutex at all while doing GC? , 
   since any thread holding the main mutex and doing a rtc_record_malloc or 
   a rtc_record_free, is not going to really matash the linked list and Can we safely 
   assume that the GC thread can proceed and read the c_i linked list, without
   any issues??
*/

  mutex_lock (&mutex);
/* call RTC_EVENT with new code */
  __rtc_event (RTC_INTERNAL_GC, NULL, 0, 0);
  mutex_unlock (&mutex);
}

/* Allocate along with header and footer and write the header and footer
   with a known value. */
static void *
malloc_padded (size_t size)
{  
    char * ends_before = 0, * ends_after = 0;
    unsigned int * pointer = 0;
    int i = 0;

#ifdef DLD_RTC
    __inside_librtc = 0;
    pointer = dld_malloc (size + __rtc_header_size + __rtc_footer_size);
    __inside_librtc = 1;
#else // DLD_RTC
    if (MONITOR_HEAP_GROWTH)
      ends_before = libc_sbrk (0);

    pointer = libc_malloc (size + __rtc_header_size + __rtc_footer_size);

    if (MONITOR_HEAP_GROWTH)
      {
        ends_after = libc_sbrk (0);
        if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
          __rtc_event (RTC_HEAP_GROWTH, pointer,0, 0);
      }
#endif // DLD_RTC

    if (pointer)
      {
        for (i=0; i < __rtc_header_size; i += sizeof (int))
          *pointer++ = HEADER_MAGIC_COOKIE;
        {
         /* Make it point to the starting of the FOOTER */
          char *temp_pointer = (char *)pointer+size;
          for (i=0; i < __rtc_footer_size; i++ )
            *temp_pointer++ = FOOTER_MAGIC_COOKIE;
        }
      }

    return pointer;
}

#ifndef DLD_RTC
/* Allocate along with header and footer and write the header and footer
   with a known value. */
static void *
calloc_padded (size_t nelem, size_t size)
{  
    char * ends_before = 0, * ends_after = 0;
    unsigned int * pointer = 0;
    int i = 0;

    if (MONITOR_HEAP_GROWTH)
      ends_before = libc_sbrk (0);

    pointer = libc_malloc (nelem * size + __rtc_header_size + __rtc_footer_size);

      
    if (MONITOR_HEAP_GROWTH)
      {
        ends_after = libc_sbrk (0);
        if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
          __rtc_event (RTC_HEAP_GROWTH, pointer,0,0);
      }

    if (pointer)
      {
        for (i=0; i < __rtc_header_size; i += sizeof (int))
          *pointer++ = HEADER_MAGIC_COOKIE;
        {
         /* Make it point to the starting of the FOOTER */
          char *temp_pointer = (char *)pointer + nelem*size;
          for (i=0; i < __rtc_footer_size; i++ )
            *temp_pointer++ = FOOTER_MAGIC_COOKIE;
        }
        libc_memset (pointer, 0, nelem * size);
      }

    return pointer;
}

static void *
realloc_padded (void * opointer, size_t size, int *bad_realloc /*out*/,
                rtc_chunk_info* c_i)
{  
    char * ends_before = 0, * ends_after = 0;
    unsigned int * pointer = 0;
    int i = 0;

    if (c_i == 0)
      {
        if (__rtc_check_free) 
          __rtc_event (RTC_BAD_REALLOC, opointer,0,0);
        *bad_realloc = 1;
        return 0;
      }

    /* If the old block was not padded, calling realloc on it will be
       disastrous. This could happen since we allow the user to toggle
       bounds check option on the fly. We need to mimic realloc with 
       malloc() and free ().
    */
       
    if (c_i->padded_block)
      {
        /* JAGag42006 - Need to remove this block from RTC book keeping, before we 
           call libc_realloc() as otherwise could result in duplicate c_i entry for 
           the same pointer, if another thread did a malloc and picked up this pointer 
           just released by libc_realloc().
         */
        rtc_record_free (opointer, 0, 1); /* 0: just record. */
        if (MONITOR_HEAP_GROWTH)
          ends_before = libc_sbrk (0);
        pointer = libc_realloc (((char *) opointer) - __rtc_header_size,
                                 size + __rtc_header_size + __rtc_footer_size);
       
        if (MONITOR_HEAP_GROWTH)
          {
            ends_after = libc_sbrk (0);
            if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
              __rtc_event (RTC_HEAP_GROWTH, pointer,0,0);
          }

        if (pointer)
          {
             for (i=0; i < __rtc_header_size; i += sizeof (int))
              *pointer++ = HEADER_MAGIC_COOKIE;
             {
              /* Make it point to the starting of the FOOTER */
               char *temp_pointer = (char *)pointer+size;
               for (i=0; i < __rtc_footer_size; i++ )
                 *temp_pointer++ = FOOTER_MAGIC_COOKIE;
             }

          }
      }
    else
      {
        size_t osize;

        osize = c_i->end - c_i->base;
        pointer = malloc_padded (size);
        /* JAGag42006 - Need to remove this block from RTC book keeping, before we 
           call libc_free() as otherwise could result in duplicate c_i entry for 
           the same pointer, if another thread did a malloc and picked up this pointer 
           just released by libc_free().
         */
        if (pointer)
          {
            /* we now support retain_freed_blocks for certain realloc()
               cases that doesn't grow/shrink at the same block address, 
               but not fully supported for all realloc() cases as yet */ 
            libc_memcpy (pointer, opointer, size < osize ? size : osize);
            /* if the new memory is allocated successfully, we need to 
               free the old memory and record the free */
            rtc_record_free (opointer, 1, 1);
          }
        else
          {
            rtc_record_free (opointer, 0, 1); /* 0: just record. */
          }
      }
    return pointer;
}
#endif // DLD_RTC

static int
calculate_random_count()
{
    int ret = 0;
    srand(__rtc_null_check_seed_value);
    ret = rand() % __rtc_null_check_random_range;
    return ret;
}

static int
handle_null_check(int size)
{
    if (NULL_CHECK_ENABLED())
      {
         /* for null-check random, it is 0 for the
            the first time; we calculate the random
            value and assign to null-check-count
         */
         if (NULL_CHECK_RANDOM())  /* true if the first time */
              __rtc_null_check_count = calculate_random_count();

	 if (__rtc_null_check_count > 0)
           current_null_check_count =
             (current_null_check_count == -1) ? 1 :
               current_null_check_count + 1;
         else
           current_null_check_size =
             (current_null_check_size == -1) ? size :
               current_null_check_size + size;
         if (NULL_CHECK_EXCEEDED())  
           {
             RESET_NULL_CHECK_VARS();
             if (__rtc_catch_nomem) /* for catch_nomem */
               __rtc_nomem_event (RTC_NOMEM, NULL);
             return 1;
           } 
           DEBUG(printf("current_null_check_size = %d\n",current_null_check_size);)
           DEBUG(printf("current_null_check_count = %d\n",current_null_check_count);)
      }
      return 0;
}

void *
#ifndef DLD_RTC
malloc(size_t size)
#else // DLD_RTC
__rtc_malloc (size_t size)
#endif // DLD_RTC
{
    char * ends_before = 0, * ends_after = 0;
    void * pointer = 0;

    OBTAIN_BACKDOOR_ENTRY ();

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    /* To avoid recursion don't call printf, otherwise
       it will recurse as printf allocates memory at times */
    if (__inside_malloc_print) /* to avoid recursion */
    {
        DEBUG_MALLOC( pointer= libc_malloc (size);
                      non_RTC_malloc_missed_count++;
                      non_RTC_malloc_missed_size+=size;
                      return pointer;)
    }
#endif
    /* QXCR1000989650: Sometimes, when the app is compiled with +check,
     * the rtc_aux functions will reset __inside_librtc to 0, even when
     * skip_bookkeeping is set. This may lead to bad free errors etc.
     * We need to skip book keeping of malloc/free/.. as long as
     * skip_bookkeeping is 1.
     */
    if (   rtc_disabled
        || __pthreads_not_ready
        || __inside_librtc
        || skip_bookkeeping)
    {
#ifdef DLD_RTC
        __inside_librtc = 0;
	pointer = dld_malloc (size);
        __inside_librtc = 1;
#else // DLD_RTC
        pointer= libc_malloc (size);
#endif // DLD_RTC
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( __inside_malloc_print = 1;
                      printf("malloc: %p = %ld \n", pointer, size);
                      non_RTC_malloc_count++;
                      non_RTC_malloc_size+=size;
                      __inside_malloc_print = 0;)
#endif
        return pointer;
    }

    __inside_librtc = 1;
    if (!rtc_started)
      rtc_initialize ();

    if (handle_null_check((int)size))
        RETURN(NULL);

    if (__rtc_check_bounds)
    {
      pointer = malloc_padded (size);

      /* If we run of out memory when we are retaining freed blocks, try to salvage
         by really freeing some of the freed blocks and retry the malloc..
       */
      if (pointer == NULL && __rtc_retain_freed_blocks)
      {
           salvage_freed_blocks();
           pointer = malloc_padded (size);
      }
    }
    else 
      {
	/* Check for the size of heap-growth. */
#ifdef DLD_RTC
        __inside_librtc = 0;
	pointer = dld_malloc (size);
        __inside_librtc = 1;
#else // DLD_RTC
        if (MONITOR_HEAP_GROWTH)
          ends_before = libc_sbrk (0);
        pointer = libc_malloc (size);

        /* If we run of out memory when we are retaining freed blocks, try to salvage
           by really freeing some of the freed blocks and retry the malloc..
         */
        if (pointer == NULL && __rtc_retain_freed_blocks)
        { 
            salvage_freed_blocks();
            pointer = libc_malloc (size);
        }

        if (MONITOR_HEAP_GROWTH)
          {
            ends_after = libc_sbrk (0);
            if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
              __rtc_event (RTC_HEAP_GROWTH, pointer,0, 0);
          }
#endif // DLD_RTC
      }

    /* Scramble this block. */
    if (pointer && __rtc_scramble_blocks)
      scramble_block (pointer, size);

    /* Record this allocation. */
    if (pointer != NULL)
      rtc_record_malloc (pointer, size, HEAP_BLOCK,
                                   __rtc_check_bounds);
    else /* JAGaf48255 - gdb should report if malloc returns null pointer */
      if (__rtc_catch_nomem)
	__rtc_nomem_event(RTC_MEM_NULL, NULL);	
      else if (in_batch_mode())
        __rtc_event (RTC_MEM_NULL, pointer, 0, size);

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    DEBUG_MALLOC( printf("record & malloc : %p = %ld\n", pointer, size);)
#endif
    RETURN (pointer);
}

void 
#ifndef DLD_RTC
free (void * p)
#else // DLD_RTC
__rtc_free (void * p)
#endif // DLD_RTC
{
    char * pointer = (char *) p;
    if (pointer == 0)
      return;

    OBTAIN_BACKDOOR_ENTRY ();

    /* QXCR1000989650: Sometimes, when the app is compiled with +check,
     * the rtc_aux functions will reset __inside_librtc to 0, even when
     * skip_bookkeeping is set. This may lead to bad free errors etc.
     * We need to skip book keeping of malloc/free/.. as long as
     * skip_bookkeeping is 1.
     */
    if (   rtc_disabled
        || __pthreads_not_ready
        || __inside_librtc
        || skip_bookkeeping)
      {
#ifndef DLD_RTC
        libc_free (pointer);
#else // DLD_RTC
        dld_free (pointer);
#endif // DLD_RTC
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( printf("free: %p\n", pointer); 
                      non_RTC_free_count++;)
#endif
	return;
      }

    __inside_librtc = 1;
    if (!rtc_started)
      rtc_initialize ();

    /* Obscurity alert ! In the case of C++, we arrive a little
       late for the party. A few mallocations have happened even
       before GDB intercepts and reroutes the calls to this module.
       These malloc calls appear to originate from support library
       code to construct a linked list of loaded libraries, termination
       handlers and such much. for more details on this, see the file
       /CLO/Components/HPCXX/Src/SupportLib/libshlInit.C
     
       However, some of these data structures get deallocated after
       we enter the picture (see undermain.c) As a result, we could see
       a few legal calls to free without having seen the corresponding
       mallocs in the first place. Here is what we will do :

       If we see a call to free and the block being freed is housed
       in the region between &end (end of data segment) and what we
       perceive to be the base of heap in rtc_initialize (), we will
       simply ignore the free and return to the application.

       We cannot call free on this object because, it could have been
       allocated by a different mallocator bound into a.out and should
       this be the case, the C library mallocator is highly likely to 
       get confused. Bindu 022803: Well looks like we are going through
       the libc mallocator...

       This has its downsides. Well, we can make things foolproof,
       can we make it damnfoolproof ?
    */

    /* Bindu 022803: The allocations after we came into picture would have
       gotten memory in between heap_base and real_heap_base (previously
       freed memory from C++ startup). We record these allocations in our
       data structures. If a free for these blocks happen and if we do not
       record that free, info leaks will show that as a leak, which is not
       right. So, try and record this free. If we cannot find the record
       for allocation in our data structures, it might be because we are
       looking at the free of pre-librtc allocation. So, just ignore this
       free in rtc_record_free.
     */

    rtc_record_free (pointer, 1, 0); /* record & free */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    DEBUG_MALLOC( printf("record & free: %p\n", pointer);)
#endif
    RETURN0 ();
}

#ifndef DLD_RTC
void *
realloc (void * pointer, size_t size)
{
    char * ends_before = 0, * ends_after = 0;
    void * old_pointer = 0;
    int bad_realloc = 0;  /* to distinguish bad_realloc and NOMEM */

    OBTAIN_BACKDOOR_ENTRY ();

    /* QXCR1000989650: Sometimes, when the app is compiled with +check,
     * the rtc_aux functions will reset __inside_librtc to 0, even when
     * skip_bookkeeping is set. This may lead to bad free errors etc.
     * We need to skip book keeping of malloc/free/.. as long as
     * skip_bookkeeping is 1.
     */
    if (   rtc_disabled
        || __pthreads_not_ready
        || __inside_librtc
        || skip_bookkeeping)
    {
        pointer=libc_realloc (pointer, size);
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( printf("realloc: %p = %ld\n", pointer, size); )
        /* Quick and dirty - As we don't know the size of the old block 
           Ideally we may need to minus the old size? 
         */
        DEBUG_MALLOC( non_RTC_malloc_count++;
                      non_RTC_malloc_size+=size;)
#endif
        return pointer;
    }

    __inside_librtc = 1;
    if (!rtc_started)
      rtc_initialize ();

    /* Special case (1) If realloc() is called with a non NULL pointer
       and a new size of 0, it is equivalent to freeing the object.
    */
    if (pointer && !size)
      {
        rtc_record_free (pointer, 1, 1); /* record & free */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( printf("record & case 1 realloc: %p = %ld\n", pointer, size); )
#endif
        RETURN (0);
      }

    /* Special case (2) If realloc is called with a NULL pointer, it
       is equivalent to malloc ()
    */

    if (pointer == 0)
      {
        if (__rtc_check_bounds)
          pointer = malloc_padded (size);
        else
          {
            if (MONITOR_HEAP_GROWTH)
              ends_before = libc_sbrk (0);
            pointer = libc_malloc (size);
            if (MONITOR_HEAP_GROWTH)
              {
                ends_after = libc_sbrk (0);
                if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
                  __rtc_event (RTC_HEAP_GROWTH, pointer,0,0);
              }
          }

        if (pointer && __rtc_scramble_blocks)
          scramble_block (pointer, size);

        if (pointer)
          rtc_record_malloc (pointer, size, HEAP_BLOCK,
                                    __rtc_check_bounds);
        else /* JAGaf48255 - gdb should report if malloc returns null pointer */
          if (__rtc_catch_nomem)
            __rtc_nomem_event(RTC_MEM_NULL, NULL);
          else if (in_batch_mode())
            __rtc_event (RTC_MEM_NULL, pointer, 0, size);

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( printf("record & case 2 realloc: %p = %ld\n", pointer, size); )
#endif
        RETURN (pointer);
      }

    old_pointer = pointer;

    long page_no = 0;
    rtc_chunk_info* c_i = NULL;

    page_no = PAGE_FROM_POINTER (old_pointer);

    mutex_lock (&mutex);

    /* Get the c_i associated with this pointer. */
    c_i = chunks_in_page[page_no];

    while (c_i != NULL)
      {
        /* Suresh, Oct 07: Skip the retained freed blocks */
        if (!c_i->freed_block && c_i->base == old_pointer)
            break;
        else
          c_i = c_i->next;
      }

    if (__rtc_check_bounds)
      pointer = realloc_padded (pointer, size, &bad_realloc, c_i);
    else
      {
        /* JAGag42006 - Need to remove this block from RTC book keeping, before we 
           call libc_realloc() as otherwise could result in duplicate c_i entry for 
           the same pointer, if another thread did a malloc and picked up this pointer 
           just released by libc_realloc().
         */
        boolean is_padded = c_i->padded_block;

        rtc_record_free (old_pointer, 0, 0); /* 0: just record,
                                                no bounds check. */
        if (MONITOR_HEAP_GROWTH)
          ends_before = libc_sbrk (0);
        pointer = libc_realloc (is_padded
                                  ? ((char *) pointer) - __rtc_header_size
                                  : pointer, size);
        if (MONITOR_HEAP_GROWTH)
          {
            ends_after = libc_sbrk (0);
            if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
              __rtc_event (RTC_HEAP_GROWTH, pointer,0,0);
          }
      }

    mutex_unlock (&mutex);

    /* Let us not bother scrambling the block. It makes me feel
       queasy to be doing it when part of the block is valid ...
    */
    if (pointer)
      {
        rtc_record_malloc (pointer, size, HEAP_BLOCK, 
                                    __rtc_check_bounds);
      }
    else if (!bad_realloc) /* JAGaf48255 */
      { 
        if (__rtc_catch_nomem)
          __rtc_nomem_event(RTC_MEM_NULL, NULL);
        else if (in_batch_mode())
          __rtc_event (RTC_MEM_NULL, pointer, 0, size);
      }

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    DEBUG_MALLOC( printf("record & realloc: %p = %ld\n", pointer, size); )
#endif
    RETURN (pointer);
}

#pragma _HP_SECONDARY_DEF calloc _calloc
void *
calloc (size_t nelem, size_t size)
{
    char * ends_before = 0, * ends_after = 0;
    void * pointer = 0;

    OBTAIN_BACKDOOR_ENTRY ();

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    /* to avoid recursion don't call printf, otherwise
       it will recurse as printf allocates memory at times */
    if (__inside_malloc_print) /* to avoid recursion */
    {
        DEBUG_MALLOC( pointer = libc_calloc (nelem, size);
                      non_RTC_malloc_missed_count++;
                      non_RTC_malloc_missed_size+=(size*nelem);
                      return pointer;)
    }
#endif
    /* QXCR1000989650: Sometimes, when the app is compiled with +check,
     * the rtc_aux functions will reset __inside_librtc to 0, even when
     * skip_bookkeeping is set. This may lead to bad free errors etc.
     * We need to skip book keeping of malloc/free/.. as long as
     * skip_bookkeeping is 1.
     */
    if (   rtc_disabled
        || __pthreads_not_ready
        || __inside_librtc
        || skip_bookkeeping)
    {
        pointer = libc_calloc (nelem, size);
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( __inside_malloc_print = 1;
                      printf("calloc: %p = %ld\n", pointer, (size*nelem)); 
                      non_RTC_malloc_count++;
                      non_RTC_malloc_size+=(size*nelem);
                      __inside_malloc_print = 0;)
#endif
        return pointer;
    }
    __inside_librtc = 1;
    if (!rtc_started)
      rtc_initialize ();

    if (__rtc_check_bounds)
    {
      pointer = calloc_padded (nelem, size);

      /* If we run of out memory when we are retaining freed blocks, try to salvage
         by really freeing some of the freed blocks and retry the calloc..
       */
      if (pointer == NULL && __rtc_retain_freed_blocks)
      {
           salvage_freed_blocks();
           pointer = calloc_padded (nelem, size);
      }
    }
    else 
      {
        if (MONITOR_HEAP_GROWTH)
          ends_before = libc_sbrk (0);
        pointer = libc_calloc (nelem, size);

       /* If we run of out memory when we are retaining freed blocks, try to salvage
          by really freeing some of the freed blocks and retry the calloc..
        */
        if (pointer == NULL && __rtc_retain_freed_blocks)
         {
          salvage_freed_blocks();
          pointer = libc_calloc (nelem, size);
         }
        if (MONITOR_HEAP_GROWTH)
          {
            ends_after = libc_sbrk (0);
            if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
              __rtc_event (RTC_HEAP_GROWTH, pointer,0,0);
          }
      }


    /* We should not scramble calloc blocks, since they should be 
       initialized to 0. ! */

    if (pointer)
      rtc_record_malloc (pointer, nelem * size, HEAP_BLOCK, 
                                        __rtc_check_bounds);
    else /* JAGaf48255 - gdb should report if malloc returns null pointer */
      if (__rtc_catch_nomem)
        __rtc_nomem_event(RTC_MEM_NULL, NULL);
      else if (in_batch_mode())
        __rtc_event (RTC_MEM_NULL, pointer, 0, size);

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    DEBUG_MALLOC( printf("record & calloc: %p = %ld\n", pointer, nelem*size); )
#endif
    RETURN (pointer);
}

void *
valloc (size_t size)
{
    char * ends_before = 0, * ends_after = 0;
    void * pointer = 0;

    OBTAIN_BACKDOOR_ENTRY ();

    /* QXCR1000989650: Sometimes, when the app is compiled with +check,
     * the rtc_aux functions will reset __inside_librtc to 0, even when
     * skip_bookkeeping is set. This may lead to bad free errors etc.
     * We need to skip book keeping of malloc/free/.. as long as
     * skip_bookkeeping is 1.
     */
    if (   rtc_disabled
        || __pthreads_not_ready
        || __inside_librtc
        || skip_bookkeeping)
    {
        pointer = libc_valloc (size);
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        DEBUG_MALLOC( printf("valloc: %p = %ld\n", pointer, size); 
                      non_RTC_malloc_count++;
                      non_RTC_malloc_size+=size;)
#endif
        return pointer;
    }
    __inside_librtc = 1;
    if (!rtc_started)
      rtc_initialize ();

    /* valloc() is supposed to return a page aligned address which
       means that we cannot attach extra pad bytes at the head. Let
       us also not bother with the footer bytes. This is really an
       obscure HP specific thingy.
    */

    if (MONITOR_HEAP_GROWTH)
      ends_before = libc_sbrk (0);
    pointer = libc_valloc (size);
    /* If we run of out memory when we are retaining freed blocks, try to salvage
       by really freeing some of the freed blocks and retry the valloc.
     */
    if (pointer == NULL && __rtc_retain_freed_blocks)
      {
           salvage_freed_blocks();
           pointer = libc_valloc (size);
      }

    if (MONITOR_HEAP_GROWTH)
      {
        ends_after = libc_sbrk (0);
        if ((ends_after - ends_before) >= HEAP_GROWTH_THRESHOLD)
          __rtc_event (RTC_HEAP_GROWTH, pointer,0,0);
      }

    if (pointer && __rtc_scramble_blocks)
      scramble_block (pointer, size);

    if (pointer)
      rtc_record_malloc (pointer, size, HEAP_BLOCK,
                                    NON_PADDED_BLOCK);
    else /* JAGaf48255 - gdb should report if malloc returns null pointer */
      if (__rtc_catch_nomem)
        __rtc_nomem_event(RTC_MEM_NULL, NULL);
      else if (in_batch_mode())
        __rtc_event (RTC_MEM_NULL, pointer, 0, size);

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    DEBUG_MALLOC( printf("record & valloc: %p = %ld\n", pointer, size); )
#endif
    RETURN (pointer);
}
#endif // DLD_RTC

/* JAGag41249: wrappers for sigstack and sigaltstack to record alternate 
   stack region that might have been allocated on heap.
   The pointer to the alternate stack is stored in the process uarea
   and cannot be read (marked) by librtc.
   If the pointer is not recorded this way then, librtc might end up showing
   this region as a leak. There exist no pstat or ttrace calls to get this
   information, hence we need to track them thus.
*/
int 
sigstack (struct sigstack *ss, struct sigstack *oss)
{
  int ret;
  int altstacks_index = 0;

  OBTAIN_BACKDOOR_ENTRY ();

  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping
      || ss == NULL)
    return libc_sigstack (ss, oss);
 
  __inside_librtc = 1;

  if (!rtc_started)
    rtc_initialize ();

  ret = libc_sigstack (ss, oss);
  /* Suresh,Jan 08: RTC wrapper functions for sigstack() and 
     sigaltstack() cannot return RTC_MUTEX_LOCK_FAILED, when the 
     mutex fails, because the application is not designed to handle 
     this... All wrapper calls in RTC should always return what the 
     eventual libc call returns faithfully..
   */
  mutex_lock (&mutex);
  
  /* Here we can term the stale alternate stacks occuring due to 
     second call for alternate stack by the same thread, as a leak.
  */
  if (libpthread_pthread_self)
    {
      if (!(altstacks_index  = get_altstacks_index ()))
        {
          altstacks_count ++;
          altstacks_index = altstacks_count;
          altstacks = (struct altstack_chunk *) libc_realloc 
            (altstacks, altstacks_count * sizeof (struct altstack_chunk));

          altstacks[altstacks_count - 1].thread_id
            = libpthread_pthread_self ();
        }
    }
  else
    {
      altstacks_index = altstacks_count = 1;
    }

  if (!altstacks)
    {
      altstacks
        = (struct altstack_chunk *)
             libc_malloc (sizeof (struct altstack_chunk));
    }

  altstacks[altstacks_index - 1].alternate_stack_base = ss->ss_sp;
  /* For sigstack, the size is fixed, sigaltstack is used for 
     variable size. */       
  altstacks[altstacks_index - 1].alternate_stack_end
    = (char *)((char *)ss->ss_sp + SIGSTKSZ);

  mutex_unlock (&mutex);

  RETURN (ret);
}

  
int 
sigaltstack (const stack_t *ss, stack_t *oss)
{
  int ret;
  int altstacks_index = 0;

  OBTAIN_BACKDOOR_ENTRY ();
  
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping
      || ss == NULL)
    return libc_sigaltstack (ss, oss);
 
  __inside_librtc = 1;

  if (!rtc_started)
    rtc_initialize ();

  ret = libc_sigaltstack (ss, oss);

  /* Suresh,Jan 08: RTC wrapper functions for sigstack() and 
     sigaltstack() cannot return RTC_MUTEX_LOCK_FAILED, when the 
     mutex fails, because the application is not designed to handle 
     this... All wrapper calls in RTC should always return what the 
     eventual libc call returns faithfully..
   */
  mutex_lock (&mutex);
 
  /* Here we can term the stale alternate stacks occuring due to 
     second call for alternate stack by the same thread, as a leak.
  */
  if (libpthread_pthread_self)
    {
      if (!(altstacks_index  = get_altstacks_index ()))
        {
          altstacks_count ++;
          altstacks_index = altstacks_count;
          altstacks = (struct altstack_chunk *) libc_realloc 
            (altstacks, altstacks_count * sizeof (struct altstack_chunk));

          altstacks[altstacks_count - 1].thread_id
            = libpthread_pthread_self ();
        }
    }
  else
    {
      altstacks_index = altstacks_count = 1;
    }

  if (!altstacks)
    {
      altstacks
        = (struct altstack_chunk *)
             libc_malloc (sizeof (struct altstack_chunk));
    }

  altstacks[altstacks_index - 1].alternate_stack_base = ss->ss_sp;

  altstacks[altstacks_index - 1].alternate_stack_end = 
  (char *)((char *)ss->ss_sp + (long) (char *)ss->ss_size);

  mutex_unlock (&mutex);

  RETURN (ret);
}

/* If we have an alternate stack already defined for this thread, then
   the new call to alternate stack will make the older stack a leak (if the 
   older stack is not freed by the app. 
   This function returns the index into the altstacks array where the alternate
   stack of the thread can be found. retun value 0 ==> not found in the array
*/
static int
get_altstacks_index ()
{
  int i;
  pthread_t tmp = libpthread_pthread_self ();
  for (i = 0; i < altstacks_count; i ++)
    if (libpthread_pthread_equal &&
        libpthread_pthread_equal (altstacks[i].thread_id, tmp))
      {
        DEBUG (printf("found sigaltstack in get_altstacks_index, index %d\n",
                      i);)
        return i + 1;
      }

  DEBUG (printf("didn't find sigaltstack in get_altstacks_index\n");)
  return 0;
}

/* Intercept and record the mmap calls too. For our purposes it is
   just fine to treat them as mallocated blocks.
*/

#ifndef DLD_RTC
void *
mmap (void *addr, size_t len, int prot, int flags, 
                                     int fildes, off_t off)
#else // DLD_RTC
void *
__rtc_mmap (void *addr, size_t len, int prot, int flags, 
                                     int fildes, off_t off)
#endif // DLD_RTC
{
  void * pointer = 0;
  struct rtc_chunk_info *c_i = NULL;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
  {
#ifdef DLD_RTC
    pointer =  dld_mmap (addr, len, prot, flags, fildes, off);
    DEBUG(printf("__rtc_mmap %d %d %d %llx\n", rtc_disabled, __pthreads_not_ready,
                                               __inside_librtc, pointer););
    dld_heap_region_add(pointer, len, FALSE);
    return pointer;
#else // DLD_RTC
    return libc_mmap (addr, len, prot, flags, fildes, off);
#endif // DLD_RTC
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();
#ifndef DLD_RTC
  pointer = libc_mmap (addr, len, prot, flags, fildes, off);
#else // DLD_RTC
  pointer = dld_mmap (addr, len, prot, flags, fildes, off);
  DEBUG(printf("__rtc_mmap returned %llx\n", pointer););
#endif // DLD_RTC

  /* mmap return MAP_FAILED on failure. */
  if (pointer != MAP_FAILED)
    {
      /* The kernel always rounds size to the page boundary so that
         whole page(s) get mapped in. Let us do the same thing ... 
      */

      if (len % page_size)
        len = len + page_size - (len % page_size);
#ifndef DLD_RTC
      c_i =  rtc_record_malloc (pointer, len, NON_HEAP_BLOCK,
                                         NON_PADDED_BLOCK);
      if (c_i && !(flags & MAP_ANONYMOUS))
        c_i->do_not_scan = 1;
#else // DLD_RTC
      dld_heap_region_add(pointer, len, FALSE);
#endif // DLD_RTC
    }

    RETURN (pointer);
}

#ifndef DLD_RTC
/* Intercept and record the __mmap64 calls too. For our purposes it is
   just fine to treat them as mallocated blocks.
   For a 32-bit binary compiled with FILE_OFFSETS=64, mmap gets routed to __mmap64 by virtue
   of including sys/mman.h. This makes mmap () definition provided in the debugee itself making
   it not possible to wrap around mmap () calls as is usually done.
   Hence this wrapper for __mmap64 calls. Related to JAGag16313.
*/

#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
void *
__mmap64 (void *addr, size_t len, int prot, int flags, 
                                     int fildes, long long off)
{
  void * pointer = 0;
  struct rtc_chunk_info *c_i = NULL;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
    return libc_mmap64 (addr, len, prot, flags, fildes, off);

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  pointer = libc_mmap64 (addr, len, prot, flags, fildes, off);

  /* mmap return MAP_FAILED on failure. */
  if (pointer != MAP_FAILED)
    {
      /* The kernel always rounds size to the page boundary so that
         whole page(s) get mapped in. Let us do the same thing ... 
      */

      if (len % page_size)
        len = len + page_size - (len % page_size);

      c_i =  rtc_record_malloc (pointer, len, NON_HEAP_BLOCK,
                                         NON_PADDED_BLOCK);
      if (c_i && !(flags & MAP_ANONYMOUS))
        c_i->do_not_scan = 1;

    }

    RETURN (pointer);
}
#endif

/* The pthreads library manages thread stacks via mmap and munmap.
   Unlike the main thread, the thread stacks cannot grow automatically
   and are limited to fixed size specified initially. In order to
   detect stack overruns, the library allocates a guard page at the
   end and eliminates all permissions from it. Unless we watch out, we
   will be reading from these pages without read permissions when
   we garbage collect. What we will do is to simply leave the read
   permission on.
*/

int
mprotect (void *addr, size_t len, int prot)
{
  OBTAIN_BACKDOOR_ENTRY ();
  prot |= PROT_READ;
  return libc_mprotect (addr, len, prot);
}

/* Suresh: Jan 08: This function is called when a shared library 
   gets unloaded.  Scan through the chunk infos and 'mark' the pc if 
   it falls between the shared lib text segment addrs.

   Also we need to compute the relative 'offset' address of it and store 
   it back at the same location. This is required so that we can do a reverse
   map in GDB and reconstruct the symbolic stack traces. 

 */
void
markup_chunk_pcs_of_unloaded_libs ()
{
  struct shlib_info *l; 
  struct rtc_chunk_info *c_i = 0;

  if (!enable_offsets ) return;

  /* Loop thru all chunks and convert the affected PCs to offset addresses,
     if the PC is not already processed */

  for (int i = 0; i < total_pages; i++)
    { 
      for (c_i = chunks_in_page[i]; c_i; c_i = c_i->next)
        {
          // QXCR1000985554: pc_tuple may be NULL if size < min leaks
          if (!c_i->pc_tuple)
            continue;

          for (int j = 0; j < __rtc_frame_count && c_i->pc_tuple[j].pc != NULL; j++)
            {
            /* process this PC only if its an absolute address , else go for
               the next PC in this stack trace. This is to take care of  2 kinds of
               situations 
               1. Either we have already processed this stack trace coming from some 
                  other chunk_info OR 
               2. We are coming here for processing this new (2nd or more) library 
                  unload whose PCs are also there in this already processed stack 
                  trace
               Since we cannot decide if its 1 or 2, we need to visit all the PCs
               in this stack trace..
             */
              if (c_i->pc_tuple[j].library_index_of_pc != ABSOLUTE_ADDRESS_INDEX )
                continue;

             /* Check with the entries of all unloaded libraries to see if
                the current pc entry is within it, if so, convert it into
                offset address, and the corresponding shlib_info index to
                the pc_tuple;
              */
              for(l = head_unloaded_shlib_info; l != NULL; l = l->next_unloaded)
                if (l->text_start <=  c_i->pc_tuple[j].pc && 
                    l->text_end >=  c_i->pc_tuple[j].pc)
                  {
                    c_i->pc_tuple[j].pc = (void *)((CORE_ADDR)c_i->pc_tuple[j].pc
                                          - (CORE_ADDR)l->text_start);
                    c_i->pc_tuple[j].library_index_of_pc = l->library_index;
                    break;
                  }
            }
        }
#if (!defined(__LP64__) && defined(HP_IA64))
      /* For performance issue on string check for IPF 32 bits.
         Skip the pages for big chunk when we have copied the
         chunks_in_page ptr for the pages occupied by that
         big chunk. */  
      if (__rtc_check_string && (chunks_in_page[i] == chunks_in_page[i+1]) &&
          !(__rtc_retain_freed_blocks))
        {
          c_i = chunks_in_page[i];
          if (c_i)
            i = PAGE_FROM_POINTER (c_i->end) - 1;
        }
#endif
    }
  /* After all the entries are processed we should clear the
     text start/end of all unloaded entries, as different libraries
     could be loaded at the same address, and if this library is
     eventually unloaded, we end by atributing the pc from this 
     library to a another unloaded library..
    
     There are no effects of null'ing the start/end fields on 
     gdb, as all we really care to pass to gdb, is the 
     is_it_loaded/index/name bits
  */
  for(l = head_unloaded_shlib_info; l != NULL; l = l->next_unloaded)
     l->text_start = l->text_end = l->data_start = l->data_end = 0;

}

/* Suresh, Jan 08: Since multiple(dependent) libraries can be
   loaded automatically, on a single dlclose(), we cannot
   markup' the chunks_pcs based on just the handle of the
   unloaded library. So we will do the real shl_unload call
   first and then call update_shlib_info() and then call the 
   markup*() function that will do markup based on all the 
   libraries that are unloaded.

   We do the above for both shl_unload() and dlclose(), though
   from the man pages of shl_unload its unclear if one library
   unload can cause multiple unloads of its dependent libraries
   if any..
   
   Known Issue: If there is an single unload (may trigger multiple 
   dependent unloads )of a library that was loaded previously(
   without any other library unload in between them) then 
   update_shlib_info() will miss finding the text_start and 
   text_end of those libs as RTC doesn't get control on 
   shl_load events to note down the text-start/end of dynamically 
   loaded libs. So our markup*() can not process the PCs 
   corresponding to these libs whose entry is not there in the 
   shlib_info table. This issue is NOT possible on interactive mode 
   as gdb will trigger running of 'update_shlib_info()' on any 
   subsequent first call to any library call for which RTC has 
   wrappers. This is an issue for Batch mode as there is no gdb 
   to trigger this and we will discover new loaded libs only when 
   any unload of libraries happen thru dlclose/shl_unload

   Solution: So we need to call update_sh*() twice here  - once 
   before and once after the real shl_unload/dlclose call. The 
   first call will discover all newly loaded libs since the last 
   time we got control and the second call will learn about all 
   the libs that are unloaded after the call to the real unload function.

*/

int
shl_unload (shl_t handle)
{
  int val = 0;
  OBTAIN_BACKDOOR_ENTRY();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
        return libdld_shl_unload (handle);

  __inside_librtc = 1;

  if (!rtc_started)
    rtc_initialize ();

  mutex_lock(&mutex);
  /* First call to update*(); see above comments for why we do this */
  update_shlib_info ();
#ifdef HP_IA64
  /* Get the symbol information for the PCs coming from this unloaded library */
  record_symbol_from_unload_lib (handle);
#endif
  val=libdld_shl_unload (handle);
  /* Second call to update*(); see above comments for why we do this */
  update_shlib_info ();
  markup_chunk_pcs_of_unloaded_libs ();

  /* Indicate gdb that shlib_info table has been modified so that gdb can read it again */
  gdb_has_read_shlib_info = 0;
  mutex_unlock(&mutex);

  RETURN(val);
}

int
dlclose (void * handle)
{
  int val = 0;

  OBTAIN_BACKDOOR_ENTRY();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
       return libdld_dlclose (handle);

  __inside_librtc = 1;

  if (!rtc_started)
    rtc_initialize ();

  mutex_lock(&mutex);
  /* First call to update*(); see above comments for why we do this */
  update_shlib_info ();
#ifdef HP_IA64
  /* Get the symbol information for the PCs coming from this unloaded library */
  record_symbol_from_unload_lib (handle);
#endif
  val=libdld_dlclose (handle);
  /* Second call to update*(); see above comments for why we do this */
  update_shlib_info ();
  markup_chunk_pcs_of_unloaded_libs ();

  /* Indicate gdb that shlib_info table has been modified so that gdb can read it again */
  gdb_has_read_shlib_info = 0;
  mutex_unlock(&mutex);

  RETURN(val);
}

#endif // DLD_RTC

/*
 * search through chunk_info chain (c_i) for one with this addr.  
 * determine with the len if the operation would go 
 * out of bound then warn immediately.  emit _rtc_event here
 * would have the accurate c_i info. 
 * 
 * return code
 *  1) return FOUND_ADDR_NO if unable to find a c_i in this_c_i list 
 *  2) return FOUND_ADDR if found chunk_info for the pointer
 *  3) return FOUND_ADDR_WARNED if found chunk_info for the pointer
 *     and emitted a warning event.  This is necessary to avoid warning
 *     twice
 */ 

int
search_addr(char *pointer, size_t len, rtc_chunk_info *this_c_i, enum rtc_event ecode)
{
  while (this_c_i != NULL)
    {
     /* Suresh, Oct 07: Skip the retained freed blocks */
      if ( !this_c_i-> freed_block && pointer >= this_c_i->base &&
          pointer <= this_c_i->end )
        {
          /* dest_ptr can be in the middle of the
           * block as long as the len it wants to write
           * stay in the block. 
           */
           if (len > (this_c_i->end-pointer))
             {
                __rtc_event (ecode, pointer, this_c_i->pc_tuple, this_c_i->end-this_c_i->base); 
                return FOUND_WARNED;
             }

           return FOUND_ADDR;
        } /* if within base && end */
      else
        {
          this_c_i = this_c_i->next;
        }
    } /* while this_c_i != null */

  return FOUND_ADDR_NO;  

}

#if HP_VACCINE_DBG_MALLOC
const char *get_name_by_event(enum rtc_event ecode, const char *name)
{
    switch (ecode) {
       case RTC_BAD_MEMCPY:
          return "memcpy";
       case RTC_BAD_MEMCCPY:
          return "memccpy";
       case RTC_BAD_MEMSET:
          return "memset";
       case RTC_BAD_MEMMOVE:
          return "memmove";
       case RTC_BAD_BZERO:
          return "bzero";
       case RTC_BAD_BCOPY:
          return "bcopy";
       case RTC_BAD_STRCPY:
          return "strcpy";
       case RTC_BAD_STRNCPY:
          return "strncpy";
       case RTC_BAD_STRCAT:
          return "strcat";
       case RTC_BAD_STRNCAT:
          return "strncat";
       case RTC_API_OOB_WRITE:
       case RTC_API_OOB_READ:
          return name;
       default:
          return NULL;
    }
}
#endif  /* HP_VACCINE_DBG_MALLOC */

/*
 * Common routine shared by strcpy, memcpy and similar routines.
 * find a chunk_info (c_i) that contains a destination address.
 * If the incoming len is bigger than the len of the destination
 * address to the c_i->end, this call will cause a corruption.
 *
 *
 * NOTE: return true for everything after setting __inside_librtc.  
 * This is very important because the caller needs to call 
 * RETURN to reset __inside_librtc.  
 *
 * Algorithm:
 *  1) for efficiency if address is in shlib, stack, register file
 *     don't continue 
 *  2) for an address, use PAGE_FROM_POINTER(addr) to look up which
 *     index in chunk_in_page this address might be at.
 *     - if found, check if the desire len exceeds the len from the 
 *       addr to the base of the c_i.  If it doesn't exceed the 
 *       operation it should be safe.  If it exceeds then it won't be safe.
 *     - if not found, traverse the chunk_in_page backwards to look
 *       at other indexes.  This can happen when user malloc a huge chunk.
 *       Look at example below.  
 *
 *     ptr = malloc(4194304);
 *     p /x ptr
 *     $1 = 0x40018360
 *     (0x40018360 & 0xffffffff) >> 12  // PAGE_FROM_POINTER(addr) 
 *     $2 = 262168
 *     // create a c_i with base=0x40018360. end=base+4194304
 *     // at page_no 262168
 *     
 *     strcpy(ptr, "something");
 *     // no problem, PAGE_FROM_POINTER(ptr) gives the correct
 *     // page_no index 262168
 * 
 *     strcpy(ptr+4194000, "something else");
 *     // big problem, PAGE_FROM_POINTER(ptr+4194000) will return
 *     // page_no = 263192 for  ptr+4194000 because 
 *     // ptr+4194000 = 0x40418230 and 
 *     // (0x40418230 & 0xffffffff) >> 12  = 263192
 *     // the c_i that contains this huge address is recorded
 *     // page_no 262168.  This is so huge that it expands several pages.
 *     // This is the first time we need to do this in librtc.   
 *     // It's perfectly legal for these string functions to write
 *     // to the middle of the string. 
 * 
 *
 * There are cases when we have an addr but unable to find a c_i for it.
 * After lots of discussions, testing, and many days of agonizing 
 * over what to do,  I have decided it's not safe to automatically
 * warn users about those cases.  We might revisit this issue later
 * but for the current implementation of librtc, we do not have
 * enough information about the address ranges to accurately determine
 * if users misuse a pointer.  
 * This implementation emits warnings only for memory allocation
 * we know about.
 * 
 * For performance improvement of searching chunk_info for IA32, please refer to
 * CR1000844741 for detailed explanation. 
 */ 

boolean
libc_mem_common (void *addr, size_t len, 
                 enum rtc_event ecode, const char *func_name)
{
  long page_no = 0;
  rtc_chunk_info * this_c_i = 0;
  int found_c_i = 0;
  long i = 0;
  boolean ret_val = 0;
  char *dest_ptr = addr;
  char dummy_stack_var = 0;

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
      return false;

#if HP_VACCINE_DBG_MALLOC_IA64
    if (__rtc_register_bounds && the_rtc_oob_callbacks != NULL) {
        /* check libc mem functions to check pointer out of bounds */
         /* if the ecode is RTC_API_OOB_READ, then it means the
            out of bunds check is for reading the buffer, all other 
            code checks for writing the buffer */
        assert(the_rtc_oob_callbacks->check_unknown_bounds != NULL);
        the_rtc_oob_callbacks->check_unknown_bounds(addr, 
                                    0,        /* start_idx */
                                    len,      /* access_length */
                                    get_name_by_event(ecode, func_name),
                                    0,        /* line_no */
                                    ecode == RTC_API_OOB_READ ? 0
                                       : 1         /* is_write */);
    }
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

  /* go back to caller for the big bad SIGSEGV.
   * no point of doing _rtc_event.  
   */
  if (addr == NULL || !__rtc_check_string)
    return false;

  /*
    QXCR1000916099: Initialize RTC here, so that all shared library segments
    are recorded before the dest_ptr is checked against them.
  */
  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  /* for efficiency if addr is in these region don't continue searching */
  for (i = 0; i < shlib_info_count; i++)
    {
      if ( dest_ptr >= (char *) shlib_info[i].data_start && 
           dest_ptr <= (char *) shlib_info[i].data_end )
        {
          __inside_librtc = 0;
          return false;
        }
    }

  stack_end = &dummy_stack_var;
  if ( dest_ptr >= stack_base && dest_ptr <= stack_end)
    {
      __inside_librtc = 0;
      return false;
    }

  if ( dest_ptr >=  __rtc_register_file &&
       dest_ptr <=__rtc_register_file + register_file_size )
    {
      __inside_librtc = 0;
      return false;
    }

  /* Grab the lock. */
  mutex_lock (&mutex); 

  page_no  = PAGE_FROM_POINTER (dest_ptr);
  this_c_i = chunks_in_page[page_no];
#if (!defined(__LP64__) && defined(HP_IA64))
  if (this_c_i != NULL)
#endif
    ret_val = search_addr(dest_ptr, len, this_c_i, ecode);

  /* long comments above this routine explains why
   * additional backward search is necessary
   */
#if (!defined(__LP64__) && defined(HP_IA64))
  if (ret_val == FOUND_ADDR_NO ||
      this_c_i == NULL) 
#else
  if (ret_val == FOUND_ADDR_NO)
#endif
    {
      for (i = page_no; i >= 0; i--)
        {
          this_c_i = chunks_in_page[i]; 
#if (!defined(__LP64__) && defined(HP_IA64))
          if (this_c_i == NULL)
            {
            /* Check only for first NULL chunks_in_page.
               Else come out of the loop since we have not recorded
               the address. */ 
            if (i == page_no)
              continue;
            else
              break;
            }
          else
            {
              /* Check for current and previous page. And also
                 prev and prev-1 page. */
              if ((chunks_in_page[i] == chunks_in_page[i-1]) ||
                  ((i > 1) && chunks_in_page[i-1] == chunks_in_page[i-2]))
                {
                  rtc_chunk_info * c_i = 0;
                  if ((i > 1) && (chunks_in_page[i-1] == chunks_in_page[i-2])
                      && (chunks_in_page[i] != chunks_in_page[i-1]))
                    c_i = chunks_in_page[i-1];
                  else
                    c_i = chunks_in_page[i];
                  ret_val = search_addr (dest_ptr, len, c_i, ecode);
                }
              else
                ret_val = search_addr (dest_ptr, len, this_c_i, ecode);
              /* Break if value is found or not. If we have
                 not obtained the value, then that pointer is bad. */
              break; 
            }
#else
          ret_val = search_addr(dest_ptr, len, this_c_i, ecode);
          if (ret_val)
            break;
#endif
        }
    }  /* ret_val == MEM_NOT_FOUND */

  /*
   * one last chance, now look at addr+len to see if it's in
   * c_i.  If it is then there's a potential corruption.
   * example:  strcpy (ptr-2, "blahblah");  ptr is a valid ptr 
   * ptr-2 is not a ptr c_i recognizes.  After copy "blahblah"
   * it would corrupt an existing valid ptr. 
   */
  if (ret_val == FOUND_ADDR_NO)
    {
      dest_ptr = dest_ptr+len;
      page_no  = PAGE_FROM_POINTER (dest_ptr);
      this_c_i = chunks_in_page[page_no];

      ret_val = search_addr(dest_ptr, len, this_c_i, ecode);
      if (ret_val == FOUND_ADDR)
        __rtc_event (ecode, addr, this_c_i->pc_tuple, len); 
    }

  /* 
   * after looking in our bookkeeping and still not
   * able to locate the c_i just ignore.
   */

  /* Unlock. */
  mutex_unlock (&mutex);

  __inside_librtc = 0;
  return true; 
}

#ifndef DLD_RTC
/* JAGag27195 - In IPF, RTC doesn't intercept calls to memcpy/memset,
 * if we don't have the _mem*() secondary definition..
 * We thought of having this _memcpy secondary defn for PA, but it causes
 * memccpy corruption to be reported twice, so we will have it only for IPF. 
 * whereas the other 2 secondary defn added later in the same file
 * for the same JAG are not only for IPF but for both PA and IPF..
 */
#ifdef HP_IA64
#pragma _HP_SECONDARY_DEF memcpy _memcpy
#endif 

void *
memcpy (void *addr, const void *addr1, size_t len)
{
  boolean status = 0;

  /* JAGag22825 - changed all shl_get to shl_get_r for thread safe.  
   * obtain_backdoor_entry is where we get the pointer to libc_memcpy.
   * However the new routine shl_get_r calls memcpy so it bounces back
   * and forth between here and obtain_backdoor_entry until it blows up.
   * If memcpy pointer is NULL then do manual memory copy.  This should
   * only happen once.  
   */
  if (!libc_memcpy)
    {
      register const char *s = (const char *)addr1;  
      register char *t = (char *)addr;  
      while (len > 0)
        {
          *t++ = *s++;
          len--;
        }

      return addr;
    }

  status = libc_mem_common(addr, len, RTC_BAD_MEMCPY, NULL);
  return libc_memcpy (addr, addr1, len);
}


/* 
 * Copy bytes from the object pointed to by s2 into the
 * object pointed to by s1, stopping after the first
 * occurrence of byte c has been copied, or after n bytes
 * have been copied, whichever comes first.  If copying
 * takes place between objects that overlap, the behavior
 * is undefined.  memccpy() returns a pointer to the byte
 * after the copy of c in s1, or a NULL pointer if c was
 * not found in the first n bytes of s2.
 * 
 * find where the first occurance of c is.  Determine if 
 * occurs before or after n bytes.  
 */
void *
memccpy (void *addr, const void *addr1, int special_val, size_t len)
{
  boolean status = 0;
  char *stop_pointer = NULL;
  int chk_len;          /* the len we want to check in libc_mem_common */

  OBTAIN_BACKDOOR_ENTRY ();

  stop_pointer = memchr(addr1, special_val, len);
  
  /* 
   * if stop pointer is not NULL then find out if the distance 
   * from the beginning to stop pointer is shorter or longer 
   * than len.  
   */
  if (stop_pointer)
    {

      int upto_special_val; /* find the distance from beginning of addr1
                             * to first occurrence of special_val.  If this 
                             * distance is smaller then memccpy only copies
                             * up to this distance.  If it's bigger then
                             * it will copy up to len  
                             */

      upto_special_val = stop_pointer - (char *)addr1; 
      chk_len = (upto_special_val < len) ? upto_special_val : len;  
    }
  else
    /* 
     * if memchr returns null then the special_val doesn't exist so
     * it will copy up to len 
     */ 
    chk_len = len;

  status = libc_mem_common(addr, chk_len, RTC_BAD_MEMCCPY, NULL);

  return libc_memccpy (addr, addr1, special_val, len);

}

/* JAGag27195 - In IPF, RTC doesn't intercept calls to memcpy/memset,
 * if we don't have the _mem*() secondary definition..
 * But we will have secondary defn for PA as well, as there could be users
 * who call the _variant directly and we would like to intercept them
 * as well.
 * Even with this secondary defn, we will not be able to intercept memmove() 
 * in IPF, as the call goes to __milli_memmove(), but in future libc might 
 * correct this to route to _memmove(), so i have a secondary defn for 
 * memmove() to take care of this.
 */
#pragma _HP_SECONDARY_DEF memmove _memmove

void *
memmove (void *addr, const void *addr1, size_t len)
{
  boolean status = 0;
  OBTAIN_BACKDOOR_ENTRY ();

  status = libc_mem_common(addr, len, RTC_BAD_MEMMOVE, NULL);
  return libc_memmove (addr, addr1, len);
}

/* JAGag27195 - In IPF, RTC doesn't intercept calls to memcpy/memset,
 * if we don't have the _mem*() secondary definition..
 * I thought of having this _memset secondary defn for PA as well, but it 
 * causes bzero corruption to be reported twice on PA, so we will have it 
 * only for IPF. 
 * TODO: Probably a cleaner way would be to reset __inside_librtc to zero, 
 * after the  call to the libc_'s version is called - this would mean 
 * multiple changes and also we need to change the macro RETURN_COMMON/RETURN 
 * to handle this. Not too sure of why we call RETURN macro at some places 
 * and directly call the libc_*  function, and hence not making this change..
 * 
 */
#ifdef HP_IA64
#pragma _HP_SECONDARY_DEF memset _memset
#endif 

void *
memset (void *addr, int special_val, size_t len)
{
  boolean status = 0;
  OBTAIN_BACKDOOR_ENTRY ();

  status = libc_mem_common(addr, len, RTC_BAD_MEMSET, NULL);
  return libc_memset (addr, special_val, len);
}


void 
bcopy (const void *addr, void *addr1, size_t len)
{
  boolean status = 0;
  OBTAIN_BACKDOOR_ENTRY ();

  /* copies n bytes from the area pointed to by s1 to the
   * area pointed to by s2.  The parms are swapped
   */
  status = libc_mem_common(addr1, len, RTC_BAD_BCOPY, NULL);
  libc_bcopy (addr, addr1, len);
}



void 
bzero (void *addr, size_t len)
{
  boolean status = 0;
  OBTAIN_BACKDOOR_ENTRY ();

  status = libc_mem_common(addr, len, RTC_BAD_BZERO, NULL);
  libc_bzero (addr, len); 
}


char *
strcpy (char * d, const char *s)
{
  size_t len = 0; 
  boolean status = 0;
  void *addr = d; const void *addr1 = s;
  OBTAIN_BACKDOOR_ENTRY ();

  len = strlen(addr1) + 1;
  status = libc_mem_common (addr, len, RTC_BAD_STRCPY, NULL);
  return (libc_strcpy (addr, addr1));   
}


char *
strncpy (char * d, const char * s, size_t len)
{
  boolean status = 0;
  void *addr = d; const void *addr1 = s;
  OBTAIN_BACKDOOR_ENTRY ();

  /* NOTE: strncpy WILL write EXATCLY `len' bytes */
  status = libc_mem_common (addr, len, RTC_BAD_STRNCPY, NULL);
  return (libc_strncpy (addr, addr1, len));
}


char *
strcat (char * d, const char *s)
{
  size_t len = 0; 
  boolean status = 0;
  void *addr = d; const void *addr1 = s;
  OBTAIN_BACKDOOR_ENTRY ();

  len = strlen(addr) + strlen(addr1) + 1;
  status = libc_mem_common (addr, len, RTC_BAD_STRCAT, NULL);
  return (libc_strcat (addr, addr1));   
}


/*
 * char *strncat(char *s1, const char *s2, size_t n)
 * 
 * ANSIC standard section 7.11.3.2: strncat appends not more than
 * n characters (a null character and characters that follow it are
 * not appended) from array pointed to by s2 to the end of the string
 * pointed by s1.  The initial character of s2 overwrites the null char
 * at the end of s1.  A terminating null char is always appended to the
 * result.  Thus, the maximum number of chars that can end up in the 
 * array pointed to by s1 is strlen(s1) + n + 1
 */ 

char *
strncat (char * d, const char * s, size_t len)
{
  boolean status = 0;
  void *addr = d; const void *addr1 = s;
  int chk_len, len_addr, len_addr1; 

  OBTAIN_BACKDOOR_ENTRY ();

  /*
   * 3 possibilities:  
   * a) if len is bigger than strlen of addr1 then we only strncat 
   *    up to strlen of addr1
   * b) if len is smaller than strlen of addr1 then we only strncat 
   *    up to original len
   * c) if len is equal to strlen of addr1 then it doesn't matter, 
   *    keep using len
   */

  len_addr = strlen(addr);
  len_addr1 = strlen(addr1);

  if (len_addr1 < len)
    chk_len = len_addr + len_addr1 + 1; 
  else
    chk_len = len_addr + len + 1;  

  status = libc_mem_common (addr, chk_len, RTC_BAD_STRNCAT, NULL);

  return (libc_strncat (addr, addr1, len));
}

#if 1 && HP_VACCINE_DBG_MALLOC_IA64

char *   gets (char *s)
{
   boolean status = 0;
   char   *ptr;
   size_t  len;

   OBTAIN_BACKDOOR_ENTRY ();

   ptr = libc_gets(s);
   
   if (ptr) {
      len = strlen(s) + 1;
      status = libc_mem_common (s, len, RTC_API_OOB_WRITE, __func__);
   }
   return(ptr);
}  /* gets */

char *   fgets (char *s, int n, FILE *stream)
{
   boolean status = 0;
   char   *ptr;
   size_t  len;

   OBTAIN_BACKDOOR_ENTRY ();

   ptr = libc_fgets(s, n, stream);
   
   if (ptr) {
      len = strlen(s) + 1;
      status = libc_mem_common (s, len, RTC_API_OOB_WRITE, __func__);
   }
   return(ptr);
}  /* fgets */

char *   getcwd (char *buf, size_t size)
{
   boolean status = 0;
   char   *ptr;
   size_t  len;

   OBTAIN_BACKDOOR_ENTRY ();

   ptr = libc_getcwd(buf, size);
   
   if (ptr && buf) {
      len = strlen(buf) + 1;
      status = libc_mem_common (buf, len, RTC_API_OOB_WRITE, __func__);
   }
   return(ptr);
} /* getcwd */

char *   getwd (char *buf)
{
   boolean status = 0;
   char   *ptr;
   size_t  len;

   OBTAIN_BACKDOOR_ENTRY ();

   ptr = libc_getwd(buf);
   
   if (ptr && buf) {
      len = strlen(buf) + 1;
      status = libc_mem_common (buf, len, RTC_API_OOB_WRITE, __func__);
   }
   return(ptr);
} /* getxwd */




size_t   fread (void *ptr, size_t size, size_t nitems, FILE *stream)
{
   boolean status = 0;
   size_t  rval;
   size_t  len;
   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_fread(ptr, size, nitems, stream);
   
   if (size && nitems) {
      len = size * rval;
      status = libc_mem_common (ptr, len, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* fread */

ssize_t  read (int fildes, void *buf, size_t nbyte)
{
   boolean status = 0;
   ssize_t  rval;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_read(fildes, buf, nbyte);
   
   if (rval > 0) {
      status = libc_mem_common (buf, rval, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* read */

ssize_t  pread (int fildes, void *buf, size_t nbyte, off_t offset)
{
   boolean status = 0;
   ssize_t  rval;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_pread(fildes, buf, nbyte, offset);
   
   if (rval > 0) {
      status = libc_mem_common (buf, rval, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* pread */

ssize_t  readv (int fildes, const struct iovec *iov, int iovcnt)
{
   boolean status = 0;
   ssize_t  rval;
   int     error_no;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_readv(fildes, iov, iovcnt);
   
   if (rval > 0) {
      ssize_t remain_bytes = rval;
      int i = 0;
      error_no = errno;   /* save errno */
      for (; i < iovcnt; i++) {
         ssize_t read_bytes = 
            remain_bytes > iov[i].iov_len ? iov[i].iov_len
                                          : remain_bytes;
         libc_mem_common (iov[i].iov_base, read_bytes, 
                          RTC_API_OOB_WRITE, __func__);
         remain_bytes -= iov[i].iov_len;
         if (remain_bytes <= 0)
            break;
      }
      errno = error_no;
   }
   return(rval);
} /* readv */

int sprintf (char *s, const char *format, /* [arg,] */ ...)
{
   va_list ap;
   va_start(ap, format);

   int rval;
   int     error_no;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_vsprintf(s, format, ap);
   va_end(ap);

   if (rval > 0) {
      libc_mem_common (s, rval, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* sprintf */

int vsprintf (char *s, const char *format, va_list ap)
{
   int rval;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_vsprintf(s, format, ap);
   va_end(ap);

   if (rval > 0) {
      libc_mem_common (s, rval, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* vsprintf */

int snprintf (char *s, size_t maxsize, const char *format, /* [arg,] */ ...)
{
   va_list ap;
   va_start(ap, format);

   int rval;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_vsnprintf(s, maxsize, format, ap);
   va_end(ap);

   if (rval > 0 || rval == -1) {
      // hp-ux has strange return value -1 if the maxsize
      // is trasmitted
      int len = rval > 0 ? rval : maxsize;
      libc_mem_common (s, len, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* snprintf */

int vsnprintf (char *s, size_t maxsize, const char *format, va_list ap)
{
   int rval;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_vsnprintf(s, maxsize, format, ap);
   va_end(ap);

   if (rval > 0 || rval == -1) {
      // hp-ux has strange return value -1 if the maxsize
      // is trasmitted
      int len = rval > 0 ? rval : maxsize;
      libc_mem_common (s, rval, RTC_API_OOB_WRITE, __func__);
   }
   return(rval);
} /* vsnprintf */

size_t   strftime ( char *s, size_t maxsize, const char *format,
          const struct tm *timeptr)
{
   size_t  result_bytes;

   OBTAIN_BACKDOOR_ENTRY ();

   result_bytes = libc_strftime(s, maxsize, format, timeptr);

   if (result_bytes > 0) {
      libc_mem_common (s, result_bytes, RTC_API_OOB_WRITE, __func__);
   }
   return(result_bytes);

} /* strftime */

wchar_t *wcscat (wchar_t *ws1, const wchar_t *ws2)
{
   size_t len = 0; 
   OBTAIN_BACKDOOR_ENTRY ();
 
   len = wcslen(ws1) + wcslen(ws2) + 1;
   libc_mem_common (ws1, len, RTC_API_OOB_WRITE, __func__);
   return (libc_wcscat (ws1, ws2));  
} /* wcscat */

wchar_t *wcsncat (wchar_t *ws1, const wchar_t *ws2, size_t n)
{
   size_t len;
   wchar_t *result_addr;

   OBTAIN_BACKDOOR_ENTRY ();

   result_addr = libc_wcsncat(ws1, ws2, n);
   
   // get the byte size of the result wstring
   len = (wcslen(result_addr) + 1) * sizeof(wchar_t); 
   libc_mem_common(ws1, len, RTC_API_OOB_WRITE, __func__);
   return(result_addr);
} /* wcsncat */ 

wchar_t *wcscpy (wchar_t *ws1, const wchar_t *ws2)
{
   size_t len = 0; 

   OBTAIN_BACKDOOR_ENTRY ();

   len = (wcslen(ws2) + 1) * sizeof(wchar_t);
   libc_mem_common (ws1, len, RTC_API_OOB_WRITE, __func__);
   return (libc_wcscpy (ws1, ws2));
}

wchar_t *wcsncpy (wchar_t *ws1, const wchar_t *ws2, size_t n)
{
   size_t len = 0; 
   wchar_t *result_addr;

   OBTAIN_BACKDOOR_ENTRY ();

   result_addr = libc_wcsncpy(ws1, ws2, n);
   
   len = (wcslen(ws1) + 1) * sizeof(wchar_t);
   libc_mem_common (ws1, len, RTC_API_OOB_WRITE, __func__);

   return (result_addr);
} /* wcsncpy */

wchar_t *wmemcpy (wchar_t *ws1, const wchar_t *ws2, size_t n)
{
   size_t len = 0; 
   wchar_t *result_addr;

   OBTAIN_BACKDOOR_ENTRY ();

   result_addr = libc_wmemcpy(ws1, ws2, n);
   
   len = n * sizeof(wchar_t);
   libc_mem_common (ws1, len, RTC_API_OOB_WRITE, __func__);

   return (result_addr);
} /* wmemcpy */

wchar_t *wmemmove (wchar_t *ws1, const wchar_t *ws2, size_t n)
{
   size_t len = 0; 
   wchar_t *result_addr;

   OBTAIN_BACKDOOR_ENTRY ();

   result_addr = libc_wmemmove(ws1, ws2, n);
   
   len = n * sizeof(wchar_t);
   libc_mem_common (ws1, len, RTC_API_OOB_WRITE, __func__);

   return (result_addr);
} /* wmemmove */

wchar_t *wmemset (wchar_t *ws, wchar_t wc, size_t n)
{
   size_t len = 0; 
   wchar_t *result_addr;

   OBTAIN_BACKDOOR_ENTRY ();

   result_addr = libc_wmemset(ws, wc, n);
   
   len = n * sizeof(wchar_t);
   libc_mem_common (ws, len, RTC_API_OOB_WRITE, __func__);

   return (result_addr);
} /* wmemset */


wchar_t *fgetws (wchar_t *ws, int n, FILE *stream)
{
   size_t len = 0; 
   wchar_t *result_addr;

   OBTAIN_BACKDOOR_ENTRY ();

   result_addr = libc_fgetws(ws, n, stream);
   
   if (result_addr != NULL) {
      len = n * sizeof(wchar_t);
      libc_mem_common (ws, len, RTC_API_OOB_WRITE, __func__);
   }

   return (result_addr);
} /*  fgetws */

int      swprintf (wchar_t *s, size_t n, const wchar_t *format, ... )
{
   va_list ap;
   va_start(ap, format);

   int rval;
   int     error_no;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_vswprintf(s, n, format, ap);
   va_end(ap);

   if (rval > 0) {
      libc_mem_common (s, rval * sizeof(wchar_t), 
                       RTC_API_OOB_WRITE, __func__);
   }
   RETURN(rval);
} /* sprintf */

int vswprintf (wchar_t *s, size_t n, const wchar_t *format, va_list ap)
{
   int rval;

   OBTAIN_BACKDOOR_ENTRY ();

   rval = libc_vswprintf(s, n, format, ap);
   va_end(ap);

   if (rval > 0) {
      libc_mem_common (s, rval * sizeof(wchar_t), 
                       RTC_API_OOB_WRITE, __func__);
   }
   RETURN(rval);
} /* vswprintf */

#if 0
size_t   mbstowcs (wchar_t *pwcs, const char *s, size_t n);
size_t   wcstombs (char *s, const wchar_t *pwcs, size_t n);
#endif

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

#if HP_VACCINE_DBG_MALLOC_IA64

#ifdef HP_IA64
#pragma _HP_SECONDARY_DEF longjmp __longjmp
#endif

/* see /CLO/Components/UNWIND2/Src/UnwindLib/unwind_jmpbuf.h for detail */
#define JB_SP 6
#define get_SP_from_jmp_buf(env) ((char *)(((long long *)env)[JB_SP]))

#pragma NO_RETURN libc_longjmp 
#pragma NO_RETURN libc__longjmp 
#pragma NO_RETURN libc_siglongjmp

void     longjmp (jmp_buf env, int val)
{
  OBTAIN_BACKDOOR_ENTRY ();
  /* unwind local var info */
  if (the_rtc_oob_callbacks && the_rtc_oob_callbacks->unwind_local_objects_info) {
      the_rtc_oob_callbacks->unwind_local_objects_info(get_SP_from_jmp_buf(env));
  }
  libc_longjmp(env, val);

  /* Just to shut up the compiler warning about a ``noreturn''
     function returning ... 
  */
  abort(); /* noreturn, not reached */
}

#ifdef HP_IA64
#pragma _HP_SECONDARY_DEF _longjmp ___longjmp
#endif

void     _longjmp (jmp_buf env, int val)
{
  OBTAIN_BACKDOOR_ENTRY ();
  /* unwind local var info */
  if (the_rtc_oob_callbacks && the_rtc_oob_callbacks->unwind_local_objects_info) {
      the_rtc_oob_callbacks->unwind_local_objects_info(get_SP_from_jmp_buf(env));
  }
  libc__longjmp(env, val);

  /* Just to shut up the compiler warning about a ``noreturn''
     function returning ... 
  */
  abort(); /* noreturn, not reached */
}

#ifdef HP_IA64
#pragma _HP_SECONDARY_DEF siglongjmp _siglongjmp
#endif

void     siglongjmp (sigjmp_buf env, int val)
{
  OBTAIN_BACKDOOR_ENTRY ();
  /* unwind local var info */
  if (the_rtc_oob_callbacks && the_rtc_oob_callbacks->unwind_local_objects_info) {
      the_rtc_oob_callbacks->unwind_local_objects_info(get_SP_from_jmp_buf(env));
  }
 
  libc_siglongjmp(env, val);

  /* Just to shut up the compiler warning about a ``noreturn''
     function returning ... 
  */
  abort(); /* noreturn, not reached */
}

#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

#endif // DLD_RTC


static void
rtc_record_munmap (char * pointer, size_t size)
{
  long page_no = 0;
  size_t this_size = 0, next_size = 0;
  char * cp = 0;
  rtc_chunk_info * c_i = 0;
  rtc_chunk_info * ac_i = 0;

  /* We need to deal with many special cases here. A call to munmap()
     need not unmap the whole region mapped in by a call to mmap(). 
     Selective pages could get unmapped. These pages that get unmapped
     could be in the beginning, middle, or end. Further more, a single
     call to munmap() could unmap several regions mapped in 
     independently as long as they are contiguous.

     Depending upon the values of pointer and size we may need to throw
     away the chunk_info or throw away the old one & create a new one or
     retain the old one and adjust the values in it or split it into 
     two chunk_infos.

     Initially, I had code here that dealt with the many different 
     corner cases. It was too complex and ugly and I am sure it was so
     much ado about nothing. As a result, we now assume that the whole
     mmapped region is being unmapped. If this assumption proves 
     invalid, we would turn off RTC.

     If this ever comes up in a real customer situation, we could 
     address it by reinstating all the special case code ...

  */

  /* It did. The Java virtual machine does all the above and some
     more :-) So here we go with all the rigamarole of interval
     management  -- srikanth, 000222
  */

  /* The kernel always rounds size to the page boundary so that whole 
     page(s) are unmapped. Let us do the same thing ... Pointer is
     guaranteed to be page aligned as otherwise munmap would have
     failed.
  */
 
  if (size % page_size)
    size =  size + page_size - (size % page_size);

  page_no = PAGE_FROM_POINTER (pointer);

munmap_this_region:

  /* We get here from above as well as from below via a goto.
     The invariants to be maintained are : pointer points to
     an mmap region. size is its size. page_no is its pageno.
     We may be passed a pointer to the middle of an mmap region,
     in which case we need to take some pains to find it.
  */
  c_i = 0;
  mutex_lock (&mutex);
  while (page_no >= 0)
    {
      c_i = chunks_in_page[page_no];
      while (c_i)
        {
          /* Suresh, Oct 07: Skip the retained freed blocks */
          if (!c_i->freed_block && pointer >= c_i->base && pointer < c_i->end)
            goto found;
          c_i = c_i->next;
        }
#if (!defined(__LP64__) && defined(HP_IA64))
      /* For performance issue on string check for IPF 32 bits.
         Skip all the pages for which we had copied the
         chunks_in_page pointer for that big chunk. */
      if (__rtc_check_string && (chunks_in_page[page_no] == 
          chunks_in_page[page_no+1]) && !(__rtc_retain_freed_blocks))
        {
          c_i = chunks_in_page[page_no];
          if (c_i)
            page_no = PAGE_FROM_POINTER (c_i->end) - 1;
            continue;
        }
#endif
      --page_no;
    }

found :

  mutex_unlock (&mutex);
  if (c_i)
    {
      struct rtc_chunk_info *new_c_i = NULL;

      if (pointer == c_i->base)
        {
          /* the whole enchilada ? */
          if ((pointer + size) == c_i->end)
            rtc_record_free (pointer, 0, 0);
          else
          if ((pointer + size) < c_i->end)
            {
              /* some initial pages are going away */
              new_c_i =  rtc_record_malloc (pointer + size,
                                     c_i->end - (pointer + size),
                                     NON_HEAP_BLOCK,
                                     NON_PADDED_BLOCK);

              /* In case of partial munmap of a non-anonymous region. */
              if (new_c_i && (c_i->do_not_scan == 1))
                new_c_i->do_not_scan = 1;

              /* Gross ! rtc_record_malloc () would have captured
                 the current stack trace. Patch it with the old
                 one.
              */

              page_no = PAGE_FROM_POINTER (pointer + size);
              mutex_lock (&mutex);
              ac_i = chunks_in_page [page_no];
 
              while (ac_i)
                {
                  /* Suresh, Oct 07: Skip the retained freed blocks */
                  if (!ac_i->freed_block && ac_i->base == (pointer + size))
                    ac_i->pc_tuple = c_i->pc_tuple;
                  ac_i = ac_i->next;
                }
              mutex_unlock (&mutex);
              rtc_record_free (pointer, 0, 0);
            }
          else 
            {
              this_size = c_i->end - c_i->base;
              rtc_record_free (pointer, 0, 0);
              pointer += this_size;
              size -= this_size;
              page_no = PAGE_FROM_POINTER (pointer);
              goto munmap_this_region;
            }
          }
        else
          {
            if ((pointer + size) == c_i->end)
              c_i->end = pointer;
            else 
            if ((pointer + size) > c_i->end)
              {
                size -= c_i->end - pointer;
                cp = c_i->end;
                c_i->end = pointer;
                pointer = cp;
                page_no = PAGE_FROM_POINTER (pointer);
                goto munmap_this_region;
              }
            else 
              {
                next_size = c_i->end - (pointer + size);
                c_i->end = pointer;
                new_c_i =  rtc_record_malloc (pointer + size,
                                              next_size, 
                                              NON_HEAP_BLOCK,
                                              NON_PADDED_BLOCK);

                /* In case of partial munmap of a non-anonymous region. */
                if (new_c_i && (c_i->do_not_scan == 1))
                  new_c_i->do_not_scan = 1;

                page_no = PAGE_FROM_POINTER (pointer + size);
                mutex_lock (&mutex);
                ac_i = chunks_in_page [page_no];
 
                while (ac_i)
                  {
                    /* Suresh, Oct 07: Skip the retained freed blocks */
                    if (!ac_i->freed_block && ac_i->base == (pointer + size))
                      ac_i->pc_tuple = c_i->pc_tuple;
                    ac_i = ac_i->next;
                  }
                mutex_unlock (&mutex);
              }
          }
    }

    return;
}

#ifndef DLD_RTC
int
munmap (void * s, size_t len)
#else // DLD_RTC
int
__rtc_munmap (void * s, size_t len)
#endif // DLD_RTC
{
  int status = 0;
  char * addr = s;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
#ifndef DLD_RTC
    return libc_munmap (addr, len);
#else // DLD_RTC
    return dld_munmap (addr, len);
#endif // DLD_RTC

  __inside_librtc = 1;

  /* The munmap and record_munmap must be atomic. Otherwise,
     if the GC kicks in between, we are going to die a dirty
     death. Use a different mutex for this purpose, so as not
     to block others from calling malloc.
  */

  mutex_lock (&unmap_mutex);

#ifndef DLD_RTC
  status = libc_munmap (addr, len);
#else // DLD_RTC
  status = dld_munmap (addr, len);
#endif // DLD_RTC

  if (!status)    /* munmap went thro ok */
    rtc_record_munmap (addr, len);
  mutex_unlock (&unmap_mutex);

  RETURN (status);
}

#ifndef DLD_RTC
void *
sbrk (int incr)
{
  char * pointer = 0;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   !rtc_started
      || rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
      return libc_sbrk (incr);

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  pointer = libc_sbrk (incr);

  if (pointer == (void *) -1L)  /* sbrk failed, nothing to do */
    RETURN (pointer);

  /* Negative sbrk calls from the application are a sure sign of 
     trouble. They are guaranteed to interfere adversely with the 
     malloc package and confuse RTC. In the unlikely event the program
     attempts this insanity, let us just get out of the suicide scene.
  */
  if (incr < 0)
    {
      __rtc_event (RTC_SBRK_NEGATIVE, 0,0,0);
      RETURN (pointer);
    }

  if (incr && pointer != NULL)
    {
      long aligned_pointer = (long) pointer;

      /* The return value of sbrk() need not have any specific 
         alignment. Force it to have an alignment suitable for a 
         pointer for our internal purposes, as other parts (mark()) 
         assume that the start address is suitably aligned to hold a
         pointer.
      */

      while (aligned_pointer % sizeof (pointer))
        {
          aligned_pointer++;
          incr --;
        }

      rtc_record_malloc ((char *) aligned_pointer, incr,
                                  NON_HEAP_BLOCK, NON_PADDED_BLOCK);
    }

    RETURN (pointer);
}

int 
brk (void * s)
{
  char * heap_end = 0; 
  int status = 0;
  long aligned_pointer = 0;
  char * endds = s;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   !rtc_started
      || rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
      return libc_brk (endds);

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  heap_end = libc_sbrk(0);
  status = libc_brk (endds);

  if (status == -1)  /* brk failed, nothing to do */
    RETURN (status);

  /* Negative brk calls from the application are a sure sign of trouble.
     They are guaranteed to interfere adversely with the malloc package
     and confuse RTC. In the unlikely event the program attempts this 
     insanity, let us just drop the chalupa and back away.
  */
  if (endds < heap_end)
    {
      __rtc_event (RTC_SBRK_NEGATIVE, 0,0,0);
      RETURN (status);
    }

  /* We have a new block between heap_end (the old break value) and
     endds. The return value of sbrk() need not have any specific
     alignment. Force it to have an alignment suitable for a pointer
     for our internal purposes, as other parts (mark()) assume that the
     start address is suitably aligned to hold a pointer.
  */

  aligned_pointer = (long) heap_end;
  while (aligned_pointer % sizeof (heap_end))
    aligned_pointer++;

  rtc_record_malloc ((char *) aligned_pointer, 
                     endds - (char *) aligned_pointer,
                              NON_HEAP_BLOCK,
                              NON_PADDED_BLOCK);

  RETURN (status);
}


/* Wrappers for System V IPC shared memory operations : shmat()
   attaches a shared memory region to a process while shmdt() can
   be used to detach the same. We need to scan these regions for
   pointers to heap, otherwise we could mistakenly declare something
   to be garbage.
*/

void *
shmat(int shmid, const void *shmaddr, int shmflg)
{
  void * pointer = 0;
  struct shmid_ds buf;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
    return libc_shmat (shmid, shmaddr, shmflg);

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  pointer = libc_shmat (shmid, shmaddr, shmflg);
  if (pointer == (void *) -1L) /* shmat() failed, nothing to do */
    RETURN (pointer);

  shmctl (shmid, IPC_STAT, &buf); /* To get the shm_segsz. */
  rtc_record_malloc (pointer, buf.shm_segsz, NON_HEAP_BLOCK, 
                                      NON_PADDED_BLOCK);
      
  RETURN (pointer);
}

int 
shmdt(const void *shmaddr)
{
  int status = 0;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (   rtc_disabled
      || __pthreads_not_ready
      || __inside_librtc
      || skip_bookkeeping)
    return libc_shmdt (shmaddr);

  __inside_librtc = 1;

  /* shmdt () and rtc_record_free () must be atomic. */

  mutex_lock (&unmap_mutex);

  status = libc_shmdt (shmaddr);
  if (status != -1)
    rtc_record_free ((void *) shmaddr, 0, 0);

  mutex_unlock (&unmap_mutex);

  RETURN (status);
} 
/* JAGaf15615: For PA32, To enable RTC on attach, from the time process started. */
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
void  
_start (int argc, char **argv, char **envp)
{
	obtain_backdoor_entry();
        if (!in_batch_mode())
          {
	     /* JAGaf37695 - Environment variable RTC_INIT should be turned 'on' 
	        to collect heap information from startup of the process for PA32 
		application (Attach case).
                $ LD_PRELOAD=/opt/langtools/lib/librtc.sl RTC_INIT=on <executable>

                Caution : If RTC_INIT is turned 'on', librtc starts recording
                heap information by default for PA32 process. So, user should
                avoid export-ing this environment variable for shell and set
                only when appropriate. */
    
	     if (getenv("RTC_INIT") != NULL)
	        __rtc_init_leaks();
          }
         else
          {
	     if (init_config (argv[0], true) && process_config()) 
              {
	        file_included = is_file_included (argv[0]);
                /* JAGag27995 - Disable RTC so that even data collection doesn't
                   happen if the given executable should not be traced.  Batch
                   RTC is truly disabled, except that we deflect all calls that
                   we intercept to libc's version of the same. */
                if (!file_included ) {
                    rtc_disabled = 1;
                    DEBUG(printf(" _start(): rtc_disabled = 1\n");)
                }
                __pthreads_not_ready = 0;
              }
          }
	 libc__start (argc, argv, envp);
}
#endif

void 
_exit (int status)
{
  obtain_backdoor_entry();
  if (file_included && rtc_started) 
    {
#ifdef HP_IA64
      /* If we are doing selfrtc, invoke rtc_leak_analyze to do everything
         from inside librtc, without having to invoke gdb.
      */
      int doing_selfrtc = is_selfrtc(); 

      if (in_batch_mode())
        {
          if (__ismt)
           {
              __thread_once (&print_once,
                             doing_selfrtc ? analyze_resume_return : print_batch_info);
           }
          else
            {
              if (doing_selfrtc)
               {
                 analyze_resume_return ();
               }
              else
                print_batch_info();
            }
        }
#else
      if (in_batch_mode())
        {
          if (__ismt)
            __thread_once (&print_once, print_batch_info);
          else
            print_batch_info();
        }
#endif
    }

  libc__exit (status);

  /* Oh, joy. Just to shut up the compiler warning about a ``noreturn''
     function returning ... - Srikanth.
  */
  abort(); /* noreturn, not reached */
}
#endif // DLD_RTC

/* Collate all the leaked blocks into a linked list. */
static rtc_chunk_info *
sweep_leaks_into_a_list (boolean leak_detection)
{
  long i = 0;
  struct rtc_chunk_info * c_i, *temp_c_i = 0;
  rtc_chunk_info * leak_list = 0;

  leak_list = 0;
  for (i=0; i < total_pages; i++)
    {
      c_i = chunks_in_page[i];
      while (c_i)
        {
	  
         if ( leak_detection == TRUE )
	   {
             /* Skip if its a free block */
             if (c_i->freed_block) 
              {
                c_i = c_i->next;
                continue;
              } 
	     /* JAGae73849: If not marked as scanned, and it is not a preinit mmap
                add to the list.  
              */
             if (c_i->scanned != scanned_this_time && c_i->preinit_mmap == 0)
              {
	        /* Report only the new leaks. */
                if (!c_i->old_leak)
                  {
                    c_i->old_leak = 1;
                    c_i->next_leak = leak_list;
                    leak_list = c_i;
                  } 
              }
             /* Suresh, Oct 07: New semantics for the scanned_this_time bit: we 
                always reset the flag back to 0, when we are out of GC. If we are 
                doing GC, scanned_this_time flag is 1. This change is required 
                to use the same global for both the dangling GC and leak detection GC.

                If its a reachable block, toggle the scanned bit to zero
              */
             if (c_i->scanned == scanned_this_time)
               c_i->scanned = !scanned_this_time;

             c_i = c_i->next;
          }
	 else { // dangling detection case 
             /* Skip if its a live block */
             if (!c_i->freed_block) 
              {
                /* Ifs a reachable live block, we need to reset the bit 
                   because during dangling GC, all the reachable live blocks
                   have the scanned bit set to "scanned_this_time" */
                if (c_i->scanned == scanned_this_time )
                     c_i->scanned = !scanned_this_time; 
                c_i = c_i->next;
                continue;
              } 

             /* If a block is not reachable, it means its not dangling and we can
                free the block to reduce memory pressure..
              */
             if (c_i->scanned != scanned_this_time && c_i->preinit_mmap == 0)
             { 
                temp_c_i = c_i->next;
                rtc_record_free_internal(c_i);
                c_i = temp_c_i;

                continue;
             }

             /* Here if we have reached the block then its dangling, ie, reverse
                semantics of a leak..Also we set reset the scanned bit back, as if to
                pretend like no info dangling is ever called..
              */
             if (c_i->scanned == scanned_this_time && c_i->preinit_mmap == 0)
              {
                c_i->scanned = !scanned_this_time; 

                /* Sep 07, Suresh: we will always report all the dangling blocks - 
                   both old and new, because(unlike the leak case), once a block 
                   becomes dangling it needn't stay dangling at a future point in 
                   the program, so we need to always give the complete list of 
                   dangling blocks to the gdb side..
                   Which means the old_leak bit is not used for the dangling case
                 */
                 c_i->next_leak = leak_list;
                 leak_list = c_i;
              }
            c_i = c_i->next;
	  }
	} /* end of while */
#if (!defined(__LP64__) && defined(HP_IA64))
      /* For performance issue on string check for IPF 32 bits.
         Skip all the pages for which we had copied the
         chunks_in_page pointer for that big chunk. */
      if (__rtc_check_string && (chunks_in_page[i] == chunks_in_page[i+1]) &&
          !(__rtc_retain_freed_blocks))
        {
          c_i = chunks_in_page[i];
          if (c_i)
            i = PAGE_FROM_POINTER (c_i->end) - 1;
        }
#endif
    }
    return leak_list;
}

/* Functions and macros to support delta heap */

static void 
heap_interval_start ()
{
   if (__rtc_check_heap_interval == 0)
     return;

   if (clock_gettime (CLOCK_REALTIME, &interval.start))
     {
        perror("clock_gettime(CLOCK_REALTIME) failed. Not starting heap-check interval");
        return;
     }

  interval.end.tv_sec = interval.start.tv_sec + __rtc_check_heap_interval;
  interval_in_progress = 1;
  return;
  
}

static int
clear_interval_data (void)
{
  long i = 0;
  struct rtc_chunk_info *c_i = 0;
  int retval = 0;

  __inside_librtc = 1;

  if (mutex_trylock (&mutex))
    RETURN (RTC_MUTEX_LOCK_FAILED);

  for (i=0; i < total_pages; i++)
    {
      for (c_i = chunks_in_page[i]; c_i; c_i = c_i->next)
          c_i->new = 0;
    }
  mutex_tryunlock (&mutex);
  RETURN (retval);
}

static int 
heap_interval_expired ()
{
   struct timespec cur_time;

   if (clock_gettime (CLOCK_REALTIME, &cur_time))
     {
        perror("clock_gettime(CLOKC_REALTIME) failed. Switching off heap-check interval");
        __rtc_check_heap_interval = 0;
        clear_interval_data (); /* clears all heap allocations for this interval */
        return 0;
     }
     if (cur_time.tv_sec > interval.end.tv_sec)
       return 1;
     else
       return 0;
}

/*
  Every time the program stops and the user call info heap-interval
  command. We call the following function to get the latest report.
  There are two data files which are used for this functionality:
  __heap_interval_info --> this file is similar to the data file
                           for __rtc_heap_info. It contains all
   heap allocations  which have happended in an interval. The 
   file contains all allocations that happen in each interval.

  __heap_interval_info.idx --> this is a index file with the 
  following format:
  <heap start><heap end><interval start time><interval end time>
  <interval amount><# allocations in this interval>
  GDB reads idx file first and allocated heap_idx array. Each entry in heap_idx array
  has allocations for that interval.
*/

int
__rtc_heap_interval_info (void)
{
   int ret = 0;

#ifdef HP_IA64
   disable_mxn_sa_switch();
#endif

   ret = write_interval_data ();
   repeat_count++;
   interval_in_progress = 0;

#ifdef HP_IA64
   enable_mxn_sa_switch();
#endif

   return ret;
}

static int
write_interval_data (void)
{
  long i, j;
  struct rtc_chunk_info * c_i;
  int count = 0;
  int retval, corrupted = 0;
  char *timestamp_ptr;
  CORE_ADDR size;
  const CORE_ADDR zero = 0;
  CORE_ADDR address;

  __inside_librtc = 1;

  if (mutex_trylock (&mutex))
    RETURN (RTC_MUTEX_LOCK_FAILED);

  /* this check is needed to make sure calling this function
      without making any progress or when the interval checking
     is switched off because of expiry of repeat count or by 
     explicit call from the user, this function does not add
     misleading/confusing records for 0 allocations.
  */
  if (interval.start.tv_sec == 0)
    {
      retval = RTC_NO_ERROR;
      goto done;
    }

  if (heap_interval_filename_fd == -1)
    {
      sprintf (heap_interval_filename, "/tmp/__heap_interval_info.%u", getpid());
      (void) unlink (heap_interval_filename);
      heap_interval_filename_fd = 
	  open (heap_interval_filename, O_RDWR | O_EXCL | O_CREAT, 0777);
      if (heap_interval_filename_fd == -1)
        {
          retval = RTC_FOPEN_FAILED;
          goto done;
        }
      /* 
       * Allow anyone to write, preventing permissions problems if left around.
       * Needed in case umask makes 0777 in open(2) call ineffective.
       */
      (void) fchmod (heap_interval_filename_fd, 0777);
    }
  else
    {
      /* seek to the end of the file, to append the new data */
      lseek(heap_interval_filename_fd, 0, SEEK_END);
    }

  if (heap_interval_idxfile_fd == -1)
    {
      sprintf (heap_interval_idxfile, "/tmp/__heap_interval_info.idx.%u", getpid());
      (void) unlink (heap_interval_idxfile);
      heap_interval_idxfile_fd =
	  open (heap_interval_idxfile, O_RDWR | O_EXCL | O_CREAT, 0777);
      if (heap_interval_idxfile_fd == -1)
        {
          retval = RTC_FOPEN_FAILED;
          goto done;
        }
      /* 
       * Allow anyone to write, preventing permissions problems if left around.
       * Needed in case umask makes 0777 in open(2) call ineffective.
       */
      (void) fchmod (heap_interval_idxfile_fd, 0777);
    }
  else
    {
      /* seek to the end of the file, to append the new data */
      lseek(heap_interval_idxfile_fd, 0, SEEK_END);
    }

  address = (CORE_ADDR) real_heap_base;
  write (heap_interval_filename_fd, &address, sizeof (CORE_ADDR));

  address = (CORE_ADDR) heap_end_value;
  write (heap_interval_filename_fd, &address, sizeof (CORE_ADDR));

  for (i=0; i < total_pages; i++)
    {
      c_i = chunks_in_page[i];
      while (c_i)
        {
          size = c_i->end - c_i->base;

          /* Suresh, Oct 07: Skip the retained freed blocks */
          if (c_i->freed_block || c_i->heap_block != HEAP_BLOCK
              || c_i->new == 0)
            {
               c_i = c_i->next;
               continue;
            }

          count++;

          write (heap_interval_filename_fd, &size, sizeof (CORE_ADDR));
	  address = (CORE_ADDR) c_i->base;
          write (heap_interval_filename_fd, &address, sizeof (CORE_ADDR));
	  address = (CORE_ADDR) c_i->pc_tuple;
          write (heap_interval_filename_fd, &address, sizeof (CORE_ADDR));
          write (heap_interval_filename_fd, &corrupted, sizeof (int));

          if (c_i->pc_tuple)
            {
              /* Write the PC as well as the library index together */
              for (j=0; j < __rtc_frame_count && c_i->pc_tuple[j].pc != NULL; j++)
		{
	          address = (CORE_ADDR) c_i->pc_tuple[j].pc;
          	  write (heap_interval_filename_fd, &address, sizeof (CORE_ADDR));
          	  write (heap_interval_filename_fd, &c_i->pc_tuple[j].library_index_of_pc, sizeof (int));
		}
	      /* Fill up the remaining by zeros. */
              for (; j < __rtc_frame_count; j++)
              {
                write (heap_interval_filename_fd, &zero, sizeof (CORE_ADDR));
                write (heap_interval_filename_fd, &zero, sizeof (int));
              }
            }
          else 
            {
              for (j=0; j < __rtc_frame_count; j++)
                {
                  write (heap_interval_filename_fd, &zero, sizeof (CORE_ADDR));
                  write (heap_interval_filename_fd, &zero, sizeof (int));
                }
            }

          /* this chunk has been reported once, do not report again */
          c_i->new = 0;
          c_i = c_i->next;
        }
#if (!defined(__LP64__) && defined(HP_IA64))
      /* For performance issue on string check for IPF 32 bits.
         Skip all the pages for which we had copied the
         chunks_in_page pointer for that big chunk. */
      if (__rtc_check_string && (chunks_in_page[i] == chunks_in_page[i+1]) &&
          !(__rtc_retain_freed_blocks))
        {
          c_i = chunks_in_page[i];
          if (c_i)
            i = PAGE_FROM_POINTER (c_i->end) - 1;
        }
#endif
    }

  /* write start, end and interval  for each run */
  timestamp_ptr = ctime (&interval.start.tv_sec); 
  timestamp_ptr [strlen (timestamp_ptr)-1] = NULL; /* take out new line */
  write(heap_interval_idxfile_fd, timestamp_ptr, strlen (timestamp_ptr) + 1);

  timestamp_ptr = ctime (&interval.end.tv_sec); 
  timestamp_ptr [strlen (timestamp_ptr)-1] = NULL;
  write (heap_interval_idxfile_fd, timestamp_ptr, strlen(timestamp_ptr) + 1);

  write (heap_interval_idxfile_fd, (const void *) &__rtc_check_heap_interval,
				sizeof(__rtc_check_heap_interval));

  write (heap_interval_idxfile_fd, &count, sizeof(count));

  /* reset interval files back to the beginning of the file for reading. */
  lseek(heap_interval_idxfile_fd, 0, SEEK_SET);
  lseek(heap_interval_filename_fd, 0, SEEK_SET);

  /* reset interval structure */
  interval.start.tv_sec = interval.end.tv_sec = 0;

  retval = count;

done:
  mutex_tryunlock (&mutex);
  RETURN (retval);
}

static void
heap_interval_check()
{
   if (__rtc_check_heap_reset) /* reinit the data files */
     {  /* Truncate data files to erase history of previous intervals. 
	   Will be rewritten again whenever required */
        truncate (heap_interval_filename, 0);
        truncate (heap_interval_idxfile, 0);
        DEBUG(printf("truncated %s and %s.\n",heap_interval_filename, 
						heap_interval_idxfile);)

        __rtc_check_heap_reset = 0; 
     }

   /* do not start sampling till rtc has been initialized and
      interval has been set.
    */
   if (rtc_started  == 0 || __rtc_check_heap_interval == 0)
      return;

   if (interval_in_progress && heap_interval_expired())
     {
        write_interval_data ();

        repeat_count++; /* finished one more round of interval profiling */

        interval_in_progress = 0;
     }

   if ((interval_in_progress == 0) && 
         (repeat_count < __rtc_check_heap_repeat))
       heap_interval_start ();

   return;
}

/* JAGaf87040 - Function that gdb calls to get the info heap high-mem. 
 * This function writes the necessary info to file /tmp/__high_mem_info.<pid>. 
 * upon receiving info heap high-mem, gdb reads  this temp file and 
 * display the information.  It works just like info heap command 
 */

int
__rtc_high_mem_info (void)
{
  int i = 0, j = 0;
  int fd = 0;
  int count = 0;
  int retval = 0, corrupted = 0;
  CORE_ADDR size = 0;
  const CORE_ADDR zero = 0;
  CORE_ADDR address = 0;
  char filename[100];

#ifdef HP_IA64
   disable_mxn_sa_switch();
#endif

  DEBUG(printf("entering into __rtc_high_mem_info() ...\n");)

  if (!rtc_started || (!rtc_no_memory && rtc_disabled))
    return RTC_NOT_RUNNING;

  if (__inside_librtc)
    {
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      return RTC_UNSAFE_NOW;
    }

  __inside_librtc = 1;

  if (mutex_trylock(&mutex))
    {
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      RETURN (RTC_MUTEX_LOCK_FAILED);
    }

  sprintf (filename, "/tmp/__high_mem_info.%u", getpid ());
  (void) unlink (filename);
  fd = open (filename, O_RDWR | O_CREAT | O_EXCL, 0777);
  if (fd == -1)
    {
      perror ("Cannot open RTC communication file - /tmp/__high_mem_info.<pid>.");
      retval = RTC_FOPEN_FAILED;
      goto done;
    }
  /* 
   * Allow anyone to write, preventing permissions problems if left around.
   * Needed in case umask makes 0777 in creat(2) call ineffective.
   */
  (void) fchmod (fd, 0777);

  /*
   * in rtc_record_malloc, __rtc_header_size was subtracted to the base and
   * __rtc_footer_size was added to the end.  Do the opposite adjustment.
   * Read in rtc_record_malloc (grep for JAGaf87040) for
   * comments why this was done.
   */
  size = (high_mem.end-__rtc_footer_size)-(high_mem.base+__rtc_header_size);
  write (fd, &size, sizeof (CORE_ADDR));
  address = (CORE_ADDR) high_mem.base+__rtc_header_size;
  write (fd, &address, sizeof (CORE_ADDR));
  /* Suresh: we are writing the pointer to the stack trace 
     and don't need to write the lib index as we are just
     passing the address of the pc_tuple structure..
   */
  address = (CORE_ADDR) high_mem.pc_tuple;
  write (fd, &address, sizeof (CORE_ADDR));
  corrupted = -1; /* special marker */
  write (fd, &corrupted, sizeof (int));

  if (high_mem.pc_tuple)
    {
      for (j=0; j < __rtc_frame_count && high_mem.pc_tuple[j].pc; j++)
	{
          address = (CORE_ADDR) high_mem.pc_tuple[j].pc;
          write (fd, &address, sizeof (CORE_ADDR));
          /* we write the library index next */
          write (fd, &high_mem.pc_tuple[j].library_index_of_pc, 
                                                      sizeof (int));
	}
      /* Fill up the remaining pc_tuple by zeros. */
      for (; j < __rtc_frame_count; j++)
        {
           write (fd, &zero, sizeof (CORE_ADDR));
           write (fd, &zero, sizeof (int));
        }
    }
  else 
    {
      for (j=0; j < __rtc_frame_count; j++)
        {
           write (fd, &zero, sizeof (CORE_ADDR));
           write (fd, &zero, sizeof (int));
        }
    }


  /* there is 1 complete block
   * the count information is tagged on at the end. 
   */

  write (fd, &high_mem.count, sizeof (int));

  retval = 1;
  if (close (fd) == EOF)
    retval = RTC_FCLOSE_FAILED;

done:
  DEBUG(printf("normal exit from __rtc_high_mem_info(), retval=%d.\n", retval);)
  mutex_tryunlock (&mutex);
#ifdef HP_IA64
  enable_mxn_sa_switch();
#endif
  RETURN (retval);
}


/* Function that gdb calls to get the heap info.  This function writes the
   heap profile into the file /tmp/__heap_info.<pid>.  The temp file is
   created here and removed after being written in gdbrtc.c for every "info
   heap" command.  Therefore no file will be left around after the run
   (JAGag24588), and even scenarios that attach/detach repeatedly will work
   correctly.  */

int
__rtc_heap_info (void)
{
  long i = 0, j = 0;
  struct rtc_chunk_info * c_i = 0;
  int fd = 0;
  int count = 0;
  int retval, corrupted = 0;
  CORE_ADDR size = 0;
  const CORE_ADDR zero = 0;
  CORE_ADDR address = 0;

  char heap_info_filename[100];

  DEBUG(printf("entering __rtc_heap_info() ...\n");)
#ifdef HP_IA64
   disable_mxn_sa_switch();
#endif

  DEBUG(printf("rtc_started=%d, rtc_disabled=%d, __inside_librtc=%d.\n",
	rtc_started, rtc_disabled, __inside_librtc);)

  if (!rtc_started || (!rtc_no_memory && rtc_disabled))
    {
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      return RTC_NOT_RUNNING;
    }

  if (__inside_librtc && !failed_to_acquire_mutex)
    {
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      return RTC_UNSAFE_NOW;
    }

  __inside_librtc = 1;

  int doing_selfrtc = is_selfrtc();

  /* srikanth, 000221, if some other thread is inside librtc, wait until
     it gets out. OTOH, if we could successfully lock the mutex, that
     would also cause other allocations to block until the GC gets over.
  */
  /* if failed_to_acquire_mutex is 1, then we are now in the safe
     place so do not return back.
  */
  /* While doing selfrtc, the lock is already taken */
  if (!doing_selfrtc && mutex_trylock(&mutex) && !failed_to_acquire_mutex)
    {
      __inside_librtc = 0;
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      RETURN (RTC_MUTEX_LOCK_FAILED);
    }

  /* We used to call libc_sbrk () to determine the end of the heap.
     This is not safe in a multithreaded program. If someother thread
     is stopped in libc_malloc (), we would deadlock since the C
     library obtains its own mutex. Now we update this in the function
     rtc_record_malloc (). The global heap_end_value holds the current
     end of heap, as known to rtc.
  */

  /* open temp file */
  sprintf (heap_info_filename, "/tmp/__heap_info.%u", getpid ());
  (void) unlink (heap_info_filename);
  fd = open (heap_info_filename, O_RDWR | O_CREAT | O_EXCL, 0777);
  if (fd == -1)
    {
      perror ("Cannot open RTC communication file - /tmp/__heap_info.<pid>.");
      retval = RTC_FOPEN_FAILED;
      goto done;
    }
  DEBUG(printf("__rtc_heap_info(): %s opened.\n", heap_info_filename);)
  /* 
   * Allow anyone to write, preventing permissions problems if left around.
   * Needed in case umask makes 0777 in creat(2) call ineffective.
   */
  (void) fchmod (fd, 0777);

  /* QXCR1000751009: Don't write heap start and end information
     for bounds data */
  if (__rtc_bounds_or_heap == HEAP_INFO)
    {
      address = (CORE_ADDR) real_heap_base;
      write (fd, &address, sizeof (CORE_ADDR));

      address = (CORE_ADDR) heap_end_value;
      write (fd, &address, sizeof (CORE_ADDR));
    }

  for (i=0; i < total_pages; i++)
    {
      c_i = chunks_in_page[i];
      while (c_i)
        {
          if((__rtc_bounds_or_heap==HEAP_INFO)&& c_i->freed_block&& !display_freed_blocks)
           {
             c_i = c_i->next;
             continue;
           }
          corrupted = 0;
          size = c_i->end - c_i->base;


          /* JAGag23625, Jan 04 2006.
 	   * min-heap-size filter has to be applied after aggregation of the 
	   * blocks and not before. I have removed the code that used to apply this 
	   * filter at the block level. 
	   */

	  /* Write everything in terms of CORE_ADDR.
	     If you change the code here, you need to change the
             corresponding code in gdbrtc.c:down_load_data (). 
           */
          if (c_i->padded_block)
            {
             /* Suresh, Nov 07: So if a freed block has both boundary(header or 
                footer) and in-block corruption, we report the in-block corruption, 
                as we would already reported the boundary error when the block 
                was freed.. 

                On the other hand if there is only boundary corruption and it happened 
                after the block was freed(using a dangling pointer) it would be 
                reported as a boundary corruption 
             */

             /* In-block corruption for only a freed block */
              if ((c_i->freed_block) && __rtc_retain_freed_blocks)
               corrupted = check_freed_block((unsigned int*)c_i->base,(unsigned int)size); 

	     /* Boundary corruption check for both freed and live blocks, only if the
                previous check found nothing */
             if(!corrupted)
              corrupted = boundary_checks((unsigned int*)c_i->base,(unsigned int) size);
            }
          else if ((c_i->freed_block) && __rtc_retain_freed_blocks)
             /* In-block corruption for a freed block, even if the block is not padded*/
             corrupted = check_freed_block((unsigned int*)c_i->base,(unsigned int)size); 
          else
             corrupted = 0;

          /* If we are generating bounds overrun report and the current
             chunk we are looking at is not corrupted, ignore it.
           */
          if (__rtc_bounds_or_heap == BOUNDS_INFO)
            {
               if (corrupted == 0 || c_i->preinit_mmap == 1)
                 {
                    c_i = c_i->next;
                    continue;
                 }
            }
          else
            { /* JAGaf24984: skips __rtc_initialize from heap 
                 info list
               */
               /* JAGag23917: we should skip any NON_HEAP_BLOCK. 
                  As these aren`t part of heap, we do not show them on 'info heap'. 
                  We need to record these in our chunk info list to scan for leaks.
               */ 
               if (c_i->heap_block != HEAP_BLOCK)
                 {
                    c_i = c_i->next;
                    continue;
                 }
            }
          count++;

          DEBUG(fprintf(stderr, "[%d] base: %#p, size: %lld, corrupted: %d\n",
                                count, c_i->base, size, corrupted));
          write (fd, &size, sizeof (CORE_ADDR));
	  address = (CORE_ADDR) c_i->base;
          write (fd, &address, sizeof (CORE_ADDR));
          /* Suresh: we are writing the pointer to the stack trace 
             and don't need to write the lib index as we are just
             passing the address of the pc_tuple structure..
           */
	  address = (CORE_ADDR) c_i->pc_tuple;
          write (fd, &address, sizeof (CORE_ADDR));

          write (fd, &corrupted, sizeof (int));

          if (c_i->pc_tuple)
            {
              DEBUG(print_stack_trace_from_pclist(stderr, c_i->pc_tuple, __rtc_frame_count));
              for (j=0; j < __rtc_frame_count && c_i->pc_tuple[j].pc != NULL ;j++)
		{
	          address = (CORE_ADDR) c_i->pc_tuple[j].pc;
          	  write (fd, &address, sizeof (CORE_ADDR));
                  /* we write the library index next */
                  write (fd, &c_i->pc_tuple[j].library_index_of_pc, sizeof (int));
		}
	      /* Fill up the remaining by zeros. */
              for (; j < __rtc_frame_count; j++)
                {
                  write (fd, &zero, sizeof (CORE_ADDR));
                  write (fd, &zero, sizeof (int));
                }
            }
          else 
            {
              for (j=0; j < __rtc_frame_count; j++)
                {
                  write (fd, &zero, sizeof (CORE_ADDR));
                  write (fd, &zero, sizeof (int));
                }
            }

          /* this chunk has been reported once, do not report again */
          c_i->new = 0;
          c_i = c_i->next;
        }
#if (!defined(__LP64__) && defined(HP_IA64))
      /* For performance issue on string check for IPF 32 bits.
         Skip all the pages for which we had copied the
         chunks_in_page pointer for that big chunk. */
      if (__rtc_check_string && (chunks_in_page[i] == chunks_in_page[i+1]) &&
          !(__rtc_retain_freed_blocks))
        {
          c_i = chunks_in_page[i];
          if (c_i)
            i = PAGE_FROM_POINTER (c_i->end) - 1;
        }
#endif
    }

  retval = count;
  if (close (fd) == EOF)
    retval = RTC_FCLOSE_FAILED;

done:
  DEBUG(printf("normal return from __rtc_heap_info(), retval=%d.\n", retval);)
  DEBUG(fflush (stdout));
  failed_to_acquire_mutex = 0; /* Reset to initial value */
  if (!doing_selfrtc)
    mutex_tryunlock (&mutex);
#ifdef HP_IA64
  enable_mxn_sa_switch();
#endif
  RETURN (retval);
}
#pragma OPTIMIZE ON

/* Scan for pointers to heap blocks in the region bounded by two given
   addresses `first' and `last' and mark reachable blocks as such.
   If we find a pointer to a block, that block would be scanned
   recursively for pointers to additional blocks. We use a depth first
   traversal. The pointer `first' *must* be suitably aligned to point 
   to a pointer, while `last' need not. See that in the natural order 
   of things this ought to be guaranteed by any mallocator.

   We don't know where all the "real" pointers are, so we will assume
   that any suitably aligned location could hold a pointer. In essence
   we scan all the words in memory accessible to the program and see if
   they could be pointing to a heap block.
 
   To do this, we can't just look at the blocks in the page
   corresponding to the pointer ("word") alone since we may have a
   pointer pointing into the middle of a mallocated block and the start
   address of the block could be in a different page than the pointer.
   We cannot assume that all live blocks will have a pointer to the
   beginning somewhere in user space. The rational behind not assuming
   this is that we want to accomodate the not so infrequent practice
   of malloc wrappers wherein a block is mallocated, a few bytes are
   reserved initially and a pointer past that space is returned to the
   application. If we were to decree strictly that blocks without
   pointers to the beginning are garbage, we would run into trouble.

   So ... we will have to look at previous pages too. However, to keep
   things moving at a reasonable pace, we will limit our backward
   search to atmost one page. Thus a block for which there are no
   pointers to the first page_size# of bytes, will be declared 
   garbage. As you can see, this is not foolproof, but then as the
   adage goes it is impossible to make anything foolproof because fools
   are so ingenious.
*/

static void
mark (void *first, void *last, boolean leak_detection, boolean internal_gc, enum root_set location)
{
  char ** start = 0, ** end = 0;
  struct rtc_chunk_info * c_i = 0;
  long page_no = 0;
  /* Do we need to search in the prev page if the current page
     doesn't help? */
  boolean search_prev_page = false;

  unsigned stack_TOS = 0;	/* Top-Of-Stack */  

  start = (char **) first;
  end = (char **) last;

  /* Tail-recursion elimination - this label is where we jump back to 
     instead of recursing on a single pointer in the current block. */
  ITERATE :

  /* Preset the "end" pointer so if the only pointer in this block 
     is the first field we won't recurse pointlessly. */
#ifndef DLD_RTC // ranga : not needed for dld heap checking
  while ((start + 1) <= end )
    {
      /* align to the size of a pointer so dereferences won't SIGBUS */
      char ** temp_end =
	 (char **) (((unsigned long)(end - 1)) & 
		    (~((unsigned long) (sizeof(end) == 4 ? 3 : 7))));

      if (*temp_end >= real_heap_base)
        {
	  /* potential valid pointer */
	  end = (temp_end + 1);
	  break;
        }
      else
        {
	  /* not a probable pointer, skip it. */
	  end--;
        }
    }
#else  // DLD_RTC
    if ((unsigned long) end % sizeof(end))
      end = end + (sizeof(end) - ((unsigned long) end % sizeof(end)));
#endif // DLD_RTC // ranga : not needed for dld heap checking

  while ((start + 1) <= end )
    {
      /* Is this memory loc pointing to a heap address? */
#ifndef DLD_RTC
      if (*start >= real_heap_base) // ranga dld heap check
#endif // DLD_RTC
        {
          page_no = PAGE_FROM_POINTER (*start);

          c_i = chunks_in_page [page_no];
#if (!defined(__LP64__) && defined(HP_IA64))
         /* Dont process and skip for the populated pages with 
            chunks_in_page pointer for a big chunk. */
          if (c_i)
            if (__rtc_check_string && (page_no > 0) &&
                (chunks_in_page[page_no] == chunks_in_page[page_no-1]) &&
                !(__rtc_retain_freed_blocks))
              break;
#endif

          if (!c_i)
            {
	      /* Maybe in the previous page. */
              c_i = chunks_in_page[(page_no ? page_no : total_pages) - 1];
              search_prev_page = false;
            }
          else if (page_no)
            search_prev_page = true;

          while (c_i)
            {
              /* For normal leak detection case, If its a c_i for a freed 
                 block, then we need to skip these blocks for leak detection.
              */
              if (leak_detection == TRUE && c_i->freed_block == 1)
                {
                  c_i = c_i->next;
                  /* TODO: Do we need this going back to prev page for this case?
		     Looks like YES...
                   */
                  if (!c_i && search_prev_page)
                    {
                      search_prev_page = false;
                      c_i = chunks_in_page[(page_no ? page_no : total_pages) - 1];
                    }
                  continue;
                }
              /* Do not scan non-anonymous mmaped regions.
                 Though we do not scan the contents of such regions,
                 we need to set the 'scanned' flag to indicate that
                 these blocks are reachable and so are not leaks.
              */
              if (c_i->do_not_scan == 1)
                {
                  c_i->scanned = scanned_this_time; 
                  c_i = c_i->next;
                  if (!c_i && search_prev_page)
                    {
                      search_prev_page = false;
                      c_i = chunks_in_page[(page_no ? page_no : total_pages) - 1];
                    }
                  continue;
                }

              /* For dangling detection case, If its a reachable live block c_i,
                 then we need to scan this block as well for potential pointers to freed blocks  
              */

              if (*start >= c_i->base)
                {
                  /* If pointer points to a heap block at all, then
                     that block must begin in this page. No use in
                     barking up the wrong tree.
                  */
                  search_prev_page = false;

                  /* ANSI C blesses a pointer just past an array
                     with legal status.
                  */
                  if (*start <= c_i->end)
                    {
		      /* If its the dangling pointer case then log the dangling
                         pointer and the associated block(c_i) if its a freed
                         block
                      */
                      if (!leak_detection && !internal_gc && c_i->freed_block)
                        add_to_dangling_ptrs_list (c_i, start, location);
                     
                      if (c_i->scanned != scanned_this_time)
                        {
                          /*Suresh: Nov 07: DEBUG AID: We spend a lot of time answering
                            queries on why a certain block is not a leak? Storing 
                            the pointer that made this heap block reachable in the 
                            dptr field. dptr field is unused for all live(non freed) 
                            blocks during leak detection..
                            Also we store the location segment info in next_leak field 
                            which is unused for all reachable blocks (overloaded)

                            This is NOT unused any where in RTC code and is for our 
                            internal debugging aid only
                            The below type casts are atrocious, but is done to keep 
                            the compiler silent!
                          */
                          if (leak_detection) {
                              c_i->dptr = (dangling_ptrs *)start;
                              c_i->next_leak = (struct rtc_chunk_info *) location;
                          }

                          c_i->scanned = scanned_this_time;

			  /* found the block. Scan this block too. */
                          /* Sep 07, Suresh: Set this bit only for leak detection. This 
                             is a conservative or reduntant step to ensure the old_leak 
                             is reset freshly, so that if the same c_i is reused for 
                             another block there are no side-effects. This is not 
                             likely to happen because once set(only for a leaked block) 
                             there is no way we can re-cycle this c_i for another 
                             allocation, as the block is a leak. In future, if we go 
                             for automatic GC of a leaked block, then this may be a 
                             safety step for that case
                            */
                          if (leak_detection) c_i->old_leak = 0;

                          /* For dangling case, no need to mark() the block just 
                             reached , if its a freed block*/
                          if (!leak_detection && c_i->freed_block )
                             break;

                          /* to iterate is human, to recurse divine */
                          /* Well, divinity has very little to do with
			     runtime libraries. ;-(  Eliminate tail-recursive
			     calls to prevent stack overflows. */
			  if ((start + 1) < end )
			    {
			      /* multiple probable pointers left */
			      if (stack_TOS == MARK_STACK_SIZE)
				{
			          /* no room left in local worklist stack,
				     so recurse */
                                  mark (c_i->base, c_i->end, leak_detection, internal_gc,
                                        c_i->heap_block ? HEAP : NON_HEAP);
				}
			      else
			        {
				  /* add to local worklist stack */
  				  start_stack[stack_TOS] = c_i->base;
				  end_stack[stack_TOS] = c_i->end;
				  location_list[stack_TOS] = c_i->heap_block ? HEAP : NON_HEAP;
				  stack_TOS++;
			        }
			    }
			  else
			    {
			      /* only one probable pointer left, so reset end 
				 pointers and jump to restart the function
				 without creating another stack frame. */
			      start = (char **)(void *) c_i->base;
			      end = (char **)(void *) c_i->end;
                              location = c_i->heap_block ? HEAP : NON_HEAP;
			      goto ITERATE;
			    }
                        }
                      break;
                    }
                 }
              c_i = c_i->next;
              if (!c_i && search_prev_page)
                {
                  search_prev_page = false;
                  c_i = chunks_in_page[(page_no ? page_no : total_pages) - 1];
                }
            } /* inner while loop. */
         }
      start++;
    } /* outer while loop. */

  if (stack_TOS > 0)
    {
      /* The local worklist stack isn't empty, so process the next item on
         the top of the stack without creating another mark() stack frame. */
      stack_TOS--;
      start = (char **)(void *) start_stack[stack_TOS];
      end = (char **)(void *) end_stack[stack_TOS];
      location = location_list[stack_TOS]; 
      goto ITERATE;
    }

}
#undef MARK_STACK_SIZE

static void
mark_altstack (boolean leak_detection, boolean internal_gc)
{
  int i = 0;
  unsigned long alterate_stack_end = 0;
  _pthread_stack_info_t thread_stack_info;
  thread_stack_info.stk_sp = 0; /* handle no threads case */
  DEBUG (printf("in mark_altstack\n");)

  for (i = 0; i < altstacks_count; i ++)
    {
      /* mark alt stacks of only live threads. */
      if (libpthread_pthread_kill &&
          libpthread_pthread_kill (altstacks[i].thread_id, 0) == ESRCH)
        continue;

      /* mark the alternate stack, mark() will recursively scan the actual
         stack ranges from the c_i holding the stack allocation record.
      */ 
      mark ((void *) & altstacks[i].alternate_stack_base,
            (void *) & altstacks[i].alternate_stack_end,
            leak_detection, internal_gc,
            HEAP);

    }
}

static void
add_to_dangling_ptrs_list (struct rtc_chunk_info *c_i, char **ptr, enum root_set location)
{
  __inside_librtc = 1;
  /* We need to clean-up old pointers, when ever a GC is called first time */
  if ( c_i->scanned != scanned_this_time && c_i->dptr )
    {
       libc_free(c_i->dptr->ptrs);    
       c_i->dptr->ptrs = (dangling_ptrs_struct *) libc_malloc (DANGLING_PTRS_SIZE * 
                                                sizeof (dangling_ptrs_struct));
       c_i->dptr->size = DANGLING_PTRS_SIZE;
       c_i->dptr->count = 0;
       
    }
  if (!c_i->dptr)
    {
      c_i->dptr = (dangling_ptrs *) libc_calloc (1, sizeof (dangling_ptrs));
      if (c_i->dptr) {
        c_i->dptr->ptrs = (dangling_ptrs_struct *) libc_malloc (DANGLING_PTRS_SIZE * 
                                                sizeof (dangling_ptrs_struct));
        c_i->dptr->size = DANGLING_PTRS_SIZE;
        c_i->dptr->count = 0;
      }
    }
  if (c_i->dptr->count + 1 >= c_i->dptr->size)
    {
      c_i->dptr->size *= 2;
      c_i->dptr->ptrs = (dangling_ptrs_struct *) libc_realloc (c_i->dptr->ptrs, 
                   c_i->dptr->size * sizeof (dangling_ptrs_struct));
    }  

  c_i->dptr->ptrs [c_i->dptr->count].ptr =  (char *) ptr;
  c_i->dptr->ptrs [c_i->dptr->count].location = location;
  c_i->dptr->count ++;

  RETURN0 ();
}

/* __rtc_leaks_info (). This is the routine the debugger will have to
   call to obtain a leak list. In the case of a single threaded 
   program, this routine arranges to scan the program data, shared
   library data, stack, register set, all reachable heap blocks, all
   reachable mmapped regions, all reachable (explicitly allocated) 
   sbrk() regions, and shmat blocks.

   We rely on the fact that pointers must be aligned on 4 byte 
   boundaries on ILP32 mode and aligned on 8 byte boundaries on LP64
   mode. The registers are scanned implicitly in the sense that the 
   call by hand mechanism of GDB pushes the register set onto the stack.
 
   This routines returns a pointer to a linked list of leaks found 
   during this attempt. NULL pointer is returned if no new leaks are
   found. GDB will have to walk this list and download data regarding
   each leak and present it to the user.

   On IPF ilp32 programs, even though the registers are 64 long and
   pointers are 32 bit long, we scan 32-bits at a time from register
   array. We rely on the fact that we will analyse a particular register
   to be a pointer into heap when we are scanning the lower 32-bits.
   (and ignore the upper 32-bits, as that doesn't look like a heap
   address.)
*/

/* Suresh, Sep 07: Now we have common function for both leak and dangling detection
   The only diff is for leak detection, we colate all NON-reachable blocks
   but in case of dangling blocks, all reachable freed blocks are dangling..

   We use the same hash chain, for storing both the live and freed blocks.
   and when we walk thru all the c_i's, we are bound see both live and freed blocks
   and we differenciate them based on the new bit "freed_block"

 */

#pragma OPTIMIZE OFF
rtc_chunk_info* 
rtc_leaks_or_dangling_info_common(boolean leak_detection, boolean internal_gc)
{
  int i = 0;
  rtc_chunk_info * new_leaks = 0;
  char dummy_stack_var = 0;

  int      ret;

#ifdef HP_IA64
  disable_mxn_sa_switch();
#endif

  DEBUG(printf("entering into __rtc_leaks_or_dangling_info_common() 1. ...\n");)
  DEBUG(printf("rtc_started = %d, rtc_no_memory = %d, rtc_disabled = %d..\n",
                rtc_started, rtc_no_memory, rtc_disabled);)

  if (!rtc_started || (!rtc_no_memory && rtc_disabled))
    {
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      return (rtc_chunk_info *) (long) RTC_NOT_RUNNING;
    }

  /* If the user hit a ctrl-C and then said `info leaks', there is some
     chance we were interrupted while running inside this module. As the
     data structures may not be kosher, it is best not to touch them.
  */
  DEBUG(printf("__inside_librtc = %d, internal_gc = %d, fail_to_acq.. = %d..\n",
                __inside_librtc, internal_gc, failed_to_acquire_mutex);)

  if (__inside_librtc && !internal_gc &&! failed_to_acquire_mutex)
    {  
#ifdef HP_IA64
      enable_mxn_sa_switch();
#endif
      return (rtc_chunk_info *)(long) RTC_UNSAFE_NOW;
    }

  DEBUG(printf("shlib_info_invalid = %d..\n", shlib_info_invalid);)

  if (shlib_info_invalid && !internal_gc)
    {
        printf("warning: GDB may not have access to the last dynamically loaded library . The following leaks report may show memory leaks which may have a valid pointer in the data segment of the last loaded library. Once the application encounters a malloc or free call after a library is loaded, GDB gets access to the data segment of the loaded library. You may want to continue your application, stop again and ask for leaks report again for more accurate information\n");
    }

  /* srikanth, 000221, if some other thread is inside librtc, wait until
     it gets out. OTOH, if we could successfully lock the mutex, that
     would also cause other allocations to block until the GC gets over.
     Also if an munmap () or shmdt () is in progress don't collect now.
  */

  /* Suresh, Oct 07: Internal GC case, we don't to try to acquire the locks and 
     proceed. The main "mutex" should be already acquired before the internal_gc
     case is called..
  */
  DEBUG(printf("entering into __rtc_leaks_or_dangling_info_common() 2. ...\n");)

  int doing_selfrtc = is_selfrtc();

  if (!internal_gc)
  {
     if (mutex_trylock (&unmap_mutex) && !failed_to_acquire_mutex)
       {
#ifdef HP_IA64
         enable_mxn_sa_switch();
#endif
         return (rtc_chunk_info *)(long) RTC_MUTEX_LOCK_FAILED;
       }
 
     /* The mutex is already locked during selfrtc */
     if (!doing_selfrtc && mutex_trylock (&mutex) && !failed_to_acquire_mutex)
     {
         mutex_tryunlock (&unmap_mutex);
#ifdef HP_IA64
         enable_mxn_sa_switch();
#endif
         return (rtc_chunk_info *)(long) RTC_MUTEX_LOCK_FAILED;
     }

     /* JAGag34056: get a lock on split_region_mutex as well. */
     if (mutex_trylock (&split_region_mutex) && !failed_to_acquire_mutex)
     {
         if (!doing_selfrtc)
           mutex_tryunlock (&mutex);
         mutex_tryunlock (&unmap_mutex);
#ifdef HP_IA64
         enable_mxn_sa_switch();
#endif
         return (rtc_chunk_info *)(long) RTC_MUTEX_LOCK_FAILED;
     }

   }


  /* We used to call libc_sbrk () to determine the end of the heap.
     This is not safe in a multithreaded program. If someother thread
     is stopped in libc_malloc (), we would deadlock since the C
     library obtains its own mutex. Now we update this in the function
     rtc_record_malloc ()
  */

  /* In the classical depth first graph traversal, we need to mark the
     nodes that have already been visited, so as to avoid infinite recursion. 
     This flag has to be "set" before each attempt to traverse. All the c_i's
     would have this "scanned" bit cleared when we come here..
  */
  DEBUG(printf("entering into __rtc_leaks_or_dangling_info_common() 3...\n");)
  scanned_this_time = !scanned_this_time;

  /* Scan the program and all shared library data looking for pointers
     to heap blocks. Mark blocks that are reachable by one or more hops.
  */

  for (i = 0; i < shlib_info_count; i++) 
    mark (shlib_info[i].data_start, shlib_info[i].data_end, leak_detection, internal_gc, DATA_SPACE);

#ifdef DLD_RTC // ranga : need to scan the dld heap here
  dld_heap_region *cur_dld_heap_region = dld_heap_region_first;
  DEBUG_MALLOC (
    while (cur_dld_heap_region != NULL) {
      printf ("dld heap region %llx - %llx\n",
                   cur_dld_heap_region->start, cur_dld_heap_region->end);
      cur_dld_heap_region = cur_dld_heap_region->next;
    }
  )
  cur_dld_heap_region = dld_heap_region_first;
  while (cur_dld_heap_region != NULL) {
    DEBUG (printf ("mark dld heap region %llx - %llx\n",
                    cur_dld_heap_region->start, cur_dld_heap_region->end));
    fflush(stdout);
    mark (cur_dld_heap_region->start, cur_dld_heap_region->end,
          leak_detection, internal_gc, SBA);
    cur_dld_heap_region = cur_dld_heap_region->next;
  }
#else // DLD_RTC
  /* If the library inititialization code allocated some heap space
     before we entered the picture, we need to scan that region too
  */
  
  /* JAGag34056: scan all the split regions in the special heap
     region between real_heap_base and heap_base. 
  */
  if (real_heap_base < heap_base) 
    for (i = 0; i < region_cnt; i++)
      mark (region_list [i].start, region_list[i].end, leak_detection, internal_gc, SBA);

  if (!__main_thread_exited)
    {
      /* For selfrtc, stack end has already been defined. If the current thread
         is not the main thread, then for selfrtc, it will not use the main
         thread stack here.
      */
      bool doing_selfrtc = is_selfrtc();
      /* QXCR1000978807: mark() can be invoked through gdb for +check
         binaries. For such cases, update the stack_end here.
      */
      if ((!doing_selfrtc && !is_rtcapi_called)|| !invoked_internally)
        stack_end = &dummy_stack_var;
#ifdef HP_IA64
      /* On IPF, since stack grows downwards, the end address is lower than
         the base address */
      mark ((void *) stack_end, (void*) stack_base, leak_detection, internal_gc, PROCESS_STACK);   /* scan the stack ... */
#else
      mark ((void *) stack_base, (void*) stack_end, leak_detection, internal_gc, PROCESS_STACK);   /* scan the stack ... */
#endif

      DEBUG( printf("altstacks_count = %d\n", altstacks_count);)

      if (altstacks_count)
        {
          /* as we know the alternate-stack start and size,
             alternate_stack_base is always set < alternate_stack_end
             by librtc. */
          mark_altstack (leak_detection, internal_gc);
        }

    }
  else
    {
      /* Unset this variable. Debugger will set it if the main thread is
         exited for later "info leaks" commands. */
      __main_thread_exited = 0;
    }
#endif // DLD_RTC

  /* GDB would have dumped the inferior's registers on to the register
     file. Scan that too.
  */
    
  mark ((void *) __rtc_register_file, 
	((void *) (long) (__rtc_register_file + register_file_size)), leak_detection, internal_gc, REGISTER_SET);
  libc_memset ((void *)__rtc_register_file, 0, register_file_size);

#ifdef REGISTER_STACK_ENGINE_FP
  /* GDB would have dumped all threads' RSE state (rse_bottom, BSPSTORE)
     into __rtc_RSE_file. Scan that. */
  {
    char * rse_ptr = (char *) __rtc_RSE_file;
    while (rse_ptr < (__rtc_RSE_file + __rtc_RSE_file_size))
      {
        CORE_ADDR __rtc_RSE_bottom = 0, __rtc_RSE_top = 0;

        libc_memcpy (&__rtc_RSE_bottom, rse_ptr, sizeof (CORE_ADDR));
        /* 0 means end. */
        if (__rtc_RSE_bottom == 0)
	  break;
        rse_ptr += sizeof (CORE_ADDR);

	libc_memcpy (&__rtc_RSE_top, rse_ptr, sizeof (CORE_ADDR));
        rse_ptr += sizeof (CORE_ADDR);

        mark ((void *) (unsigned long) __rtc_RSE_bottom, 
	      (void *) (unsigned long) __rtc_RSE_top, leak_detection, internal_gc, REGISTER_SET);
      }
    libc_memset ((void *) __rtc_RSE_file, 0, __rtc_RSE_file_size);
  }
#endif /* REGISTER_STACK_ENGINE_FP */

  /* When the program is multithreaded, we need to scan four additional
     regions : thread stacks, TLS, TSD  and thread registers. Thread
     stacks are mmap regions and pthreads library seems to hold onto
     the beginning addresses (in user space.) There is an API call
     pthread_*_getstackaddr() that retrieves this.

     I suspect pthreads maintains pointers to TLS also in the user
     space. This is ok even otherwise as gdb pushes CR27 onto the
     register file.

     TSD is managed via malloc/free and these are entirely user space
     thingies. The kernel does not even know about these.

     As the thread registers have already been scanned with gdb's help,
     we are done. The one catch here is that we scan the entire thread
     stack, not just the region bounded by stack bottom and current SP.
     So dirty stack beyond SP will hide leaks, but this should even
     out as the program proceeds further.
  */

  new_leaks = sweep_leaks_into_a_list (leak_detection); /* build the leaks list. */

  /* we need to reset the semantics of "scanned_this_time" flag again, so that when 
     we come out of this routine its all set to the same state, as if we have NOT 
     done any info GC command..
   */
    scanned_this_time = !scanned_this_time;

  /* Release the locks. */
  /* Reset the flag to zero here */
  /* Release the lock only if it is not second time gc, as the thread will release
     the mutex at mutex_unlock.
   */
  //if (!failed_to_acquire_mutex)
  failed_to_acquire_mutex = 0; /* Reset to initial value */
  if (!doing_selfrtc)
    mutex_tryunlock (&mutex);
  mutex_tryunlock (&unmap_mutex);
  mutex_tryunlock (&split_region_mutex);
  DEBUG(printf("leaving __rtc_leaks_or_dangling_info_common() ...\n");)

#ifdef HP_IA64
  enable_mxn_sa_switch();
#endif

  RETURN (new_leaks); 
}

rtc_chunk_info *
__rtc_leaks_info (void)
{
   rtc_chunk_info * c_i;
   c_i = rtc_leaks_or_dangling_info_common(TRUE,FALSE);
   return(c_i);
}

//wrapper function for dangling detection, it uses a modified version of mark and sweep
rtc_chunk_info *
__rtc_dangling_info (void)
{
   rtc_chunk_info * c_i;
   c_i = rtc_leaks_or_dangling_info_common(FALSE,FALSE);
   return(c_i);
}

/*wrapper function for internal dangling detection, it uses a modified version of 
  mark and sweep
*/
rtc_chunk_info *
__rtc_dangling_info_internal (void)
{
   rtc_chunk_info * c_i;
   c_i = rtc_leaks_or_dangling_info_common(FALSE,TRUE);
   return(c_i);
}
#pragma OPTIMIZE ON


/* Function to be called by the user program in main, so that we can start 
   collecting the rtc information from the start-up even though gdb is not 
   yet in the picture. Bindu 040303 for attach & rtc. */
void
__rtc_init_leaks ()
{
  __rtc_frame_count = DEFAULT_FRAME_COUNT;
  __pthreads_not_ready = 0;
}

/* Batch RTC related functions */

/* Check if the given file is being traced or not */
static boolean 
is_file_included(char *file)
{
    int i = 0;

    if (rtcfiles[0] == NULL)
      return 1; /* empty rtcfiles; all files are assumed included. */

    /*
     * JAGaf88911 - 
     * in rtcconfig, user specifies files=exec1:exec2:exec3
     * rtcfiles[0]=exec1, rtcfiles[1]=exec2 etc.
     * file argument is the name of the running executable
     * file can appear as ./exec1 or /bin/exec1...
     */
    for (i = 0; i < rtc_files_count; i++)
      {
         if (strstr(file, rtcfiles[i]) != 0)
            return true;
      }

    return false;

}

/* This function takes a string of file names separated by ':' and breaks
   it up in an array of character strings.
 */
static int
parse_values (char *value, char *rtcfiles[])
{
     char *tp;
     int   i = 0;
     char *last = NULL;

     tp = strtok_r (value, ":", &last);
     if ((rtcfiles[i] = (char *)malloc (strlen (tp) + 1)) == NULL) 
       {
         perror ("librtc internal malloc failed");
         return 0;
       }
     libc_strcpy (rtcfiles[i], tp);
     DEBUG(printf("rtcfiles %s\n", rtcfiles[0]);)

     for (i=1; i< MAX_RTC_FILES && (tp = strtok_r (NULL, ":", &last)); i++) 
       {
          if ((rtcfiles[i] = (char *)malloc (strlen(tp) + 1)) == NULL) 
            {
               perror ("librtc internal malloc failed");
               return 0;
            }
         libc_strcpy (rtcfiles[i], tp);
         DEBUG(printf("rtcfiles %s\n", rtcfiles[i]);)
       }

     /*
      * JAGaf88911 - strtok_r stops when there's no more 
      * token to grab.  Save the ith value to avoid traversing
      * MAX_RTC_FILES every time.  
      */ 

     rtc_files_count = i; 
     return 1;
}

#define SET_VALUE(value, config_var) \
    {\
        if(strncmp(value, "on", 2) == 0 || strncmp(value, "1", 1) == 0)\
            config_var = 1;\
        else if(strncmp(value, "off", 3) == 0 || strncmp(value, "0", 1) ==0)\
            config_var = 0;\
        else\
            fprintf(stderr, "Invalid option \"%s\". Ignoring it.\n", value);\
    }

/*
 * Read the config file and put all configurables in rtcenv array; put
 * file names for leak detection in rtcfiles array.
 * Null will be used to signify the end of rtcfiles array.
 * The file supports following syntax:
 * check_leaks=on/off  --> switch on/off leak detection
 * check_heap=on/off  --> switch on/off info heap
 * check_free=on/off  --> switch on bad free (enhancement: to be done in future)
 * output_dir=<dir name> --> name of directory where output file should be
 * files="file1:file2:...." --> files of interest which are to be traced
 * min_leak_size=size --> track mallocs which are bigger than this size
 * scramble_block=on/off --> switch on/off scrambling of blocks
 * frame_count=num --> size of the frame count tobe used in displaying leak context
 * register_bounds=on/off --> switch on/off register the heap object bounds info
 * thread-check=on/off --> turn on/off thread checks
 * recursive-relock=on/off --> detect attempts to relock non-recursive mutexes
 * unlock-not-own=on/off --> detect attempts to unlock mutexes or rwlocks not 
 *                           owned by the current thread
 * mixed-sched-policy=on/off --> detect attempts to synchronize
 *                               threads using different scheduling policies
 * cv-multiple-mxs=on/off --> detect when a condition variable
 *                            is associated with more then one mutex
 * cv-wait-no-mx=on/off --> detect attempts to wait on a
 *                          condition variable when a mutex is not locked
 * thread-exit-own-mutex=on/off --> detect attempts to exit a thread while 
 *                                  it's holding a a mutex or a rwlock
 * thread-exit-no-join-detach=on/off --> detect when a terminated thread is
 *                                       neither joined nor detached
 * stack-util=<num> --> detect when thread stack utilization exceeds a given
 *                      threshold (0 - 100%)
 * num-waiters=<num> --> detect when the number of waiters on any object exceeds 
 *                       a given threshold
 */

static void
parse_config_string(const char *cfg_str)
{
    char *envstr;
    int  val;
    char *inbuf, *p, *head;
    char *name, *value, *eqsep;

    /* This check is needed to catch "NULL" input strings. */
    if (cfg_str == NULL) return;

    head = inbuf = strdup(cfg_str);
    while (inbuf != NULL) 
      {
        p = strchr(inbuf, ';'); /* search for delimiter ';' */
	if (p != NULL) {
	  *p++ = '\0';
	/* JAGag42027
	 * When multiple config strings are present on a single line with 
	 * semicolon as the delimiter, we have to skip the space/s or tab 
	 * present between config strings to correctly read the
	 * configurable values.
	 * Sample rtcconfig line with multiple config strings:
	 * check_bounds=1; check_free=1; scramble_block=1;
	 */
        while (*p == ' ' || *p == '\t')
          p++;
        }

        if ((eqsep = strchr(inbuf,'=')) != NULL)
          {
             *eqsep = '\0';
             name = inbuf;
             value = eqsep + 1;
 
             DEBUG(printf("Read name=%s value=%s\n", name, value);)

             if (strcmp (name, "frame_count") == 0) 
               {
                  if ((val = atoi (value)) >= 0)
                    __rtc_frame_count = val;
                  else
                    fprintf (stderr, "Invalid frame_count value. Using default\n");
               } 
             else if (strstr (name, "check_bound"))
               {
                 SET_VALUE(value, __rtc_check_bounds);
               } 
#if HP_VACCINE_DBG_MALLOC_IA64
             else if (strstr (name, "register_bounds"))
               {
                 SET_VALUE(value, __rtc_register_bounds);
               } 
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */
             else if (strcmp (name, "check_free") == 0) 
               {
                 SET_VALUE(value, __rtc_check_free);
               } 
             else if (strstr (name, "scramble_block")) 
               {
                 SET_VALUE(value, __rtc_scramble_blocks);
               } 
             else if (strcmp (name, "check_leaks") == 0 ||
                      strcmp (name, "leaks") == 0)
               {
                 SET_VALUE(value, __rtc_check_leaks);
               } 
             else if (strcmp (name, "check_heap") == 0 ||
                      strcmp (name, "heap") == 0)
               {
                 SET_VALUE(value, __rtc_check_heap);
               } 
#ifdef HP_IA64
             else if (strcmp (name, "check_openfd") == 0 ||
                      strcmp (name, "openfd") == 0)
               {
                 SET_VALUE(value, __rtc_check_openfd);
               } 
#endif /* HP_IA64 */
             else if (strcmp (name, "retain_freed_blocks") == 0)
               {
                 SET_VALUE(value, __rtc_retain_freed_blocks);
               }
             else if (strcmp (name, "min_leak_size") == 0) 
               {
                 if ((val = atoi (value)) >= 0)
                   __rtc_min_leak_size = val;
                 else
                   fprintf (stderr, 
                            "Invalid min_leak_size value. Ignoring it.\n");
               } 
             else if (strcmp (name, "min_heap_size") == 0) 
               {
                 if ((val = atoi (value)) >= 0)
                   __rtc_min_heap_size = val;
                 else
                   fprintf (stderr, 
                            "Invalid min_heap_size value. Ignoring it.\n");
               } 

#if HP_VACCINE_DBG_MALLOC
             else if (strstr (name, "abort_on_bound"))
               {
                  if (!rtc_no_abort) {
                    SET_VALUE(value, __rtc_abort_on_bounds);
                  }
               }
             else if (strcmp (name, "abort_on_bad_free") == 0)
               {
                  if (!rtc_no_abort) {
                    SET_VALUE(value, __rtc_abort_on_bad_free);
                  }
               }
             else if (strcmp (name, "abort_on_nomem") == 0)
               {
                  if (!rtc_no_abort) {
                    SET_VALUE(value, __rtc_abort_on_nomem);
                  }
               }
             else if (strcmp (name, "check_potential_leak") == 0)
               {
                 SET_VALUE(value, __rtc_check_potential_leak);
               }
             else if (strcmp (name, "report_raw_data") == 0)
               {
                 SET_VALUE(value, __rtc_report_raw_data);
               }
             else if (strcmp (name, "report_address") == 0)
               {
                 SET_VALUE(value, __rtc_report_address);
               }
             else if (strcmp (name, "report_freed_memory") == 0)
               {
                 SET_VALUE(value, __rtc_report_freed_memory);
               }
             else if (strcmp (name, "leak_logfile") == 0)
               {
                  if (__rtc_leak_logfile != NULL)
                    free(__rtc_leak_logfile);
                  if (strcmp (value, "default") == 0)
                    __rtc_leak_logfile = 0;
                  else {
                    if ((__rtc_leak_logfile =
                              malloc (strlen(value) + 1)) != NULL)
                      libc_strcpy (__rtc_leak_logfile, value);
                    else
                      fprintf (stderr, "librtc internal malloc failed.");
                  }
               }
             else if (strcmp (name, "mem_logfile") == 0)
               {
                  if (__rtc_mem_logfile != NULL)
                    free(__rtc_mem_logfile);
                  if (strcmp (value, "default") == 0)
                    __rtc_mem_logfile = 0;
                  else {
                    if ((__rtc_mem_logfile =
                              malloc (strlen(value) + 1)) != NULL)
                      libc_strcpy (__rtc_mem_logfile, value);
                    else
                      fprintf (stderr, "librtc internal malloc failed.");
                  }
               }
             else if (strcmp (name, "heap_logfile") == 0)
               {
                  if (__rtc_heap_logfile != NULL)
                    free(__rtc_heap_logfile);
                  if (strcmp (value, "default") == 0)
                    __rtc_heap_logfile = 0;
                  else {
                    if ((__rtc_heap_logfile =
                              malloc (strlen(value) + 1)) != NULL)
                      libc_strcpy (__rtc_heap_logfile, value);
                    else
                      fprintf (stderr, "librtc internal malloc failed.");
                  }
               }
#endif /* HP_VACCINE_DBG_MALLOC */
             else if (strcmp (name, "output_dir") == 0) 
               {
                  if ((rtcenv[RTC_OUTPUT_DIR] = 
                              malloc (strlen(value) + 1)) != NULL)
                    libc_strcpy (rtcenv[RTC_OUTPUT_DIR], value);
                  else
                    fprintf (stderr, "librtc internal malloc failed.");
               } 
             else if (strcmp (name, "files") == 0) 
               {
                  if (!parse_values (value, rtcfiles)) 
                    {
                       break;
                    }
               }
             else if (strstr (name, "check_string")) 
               {
                  SET_VALUE(value, __rtc_check_string);
               }
#if HP_VACCINE_DBG_MALLOC_IA64
             else if (strcmp (name, "recursive-relock") == 0)
               {
                 SET_VALUE(value, __rtc_non_recursive_relock);
               }
             else if (strcmp (name, "unlock-not-own") == 0)
               {
                 SET_VALUE(value, __rtc_unlock_not_own);
               }
             else if (strcmp (name, "mixed-sched-policy") == 0)
               {
                 SET_VALUE(value, __rtc_mixed_sched_policy);
               }
             else if (strcmp (name, "cv-multiple-mxs") == 0)
               {
                 SET_VALUE(value, __rtc_condvar_multiple_mutexes);
               }
             else if (strcmp (name, "cv-wait-no-mx") == 0)
               {
                 SET_VALUE(value, __rtc_condvar_wait_no_mutex);
               }
             else if (strcmp (name, "thread-exit-own-mutex") == 0)
               {
                 SET_VALUE(value, __rtc_thread_exit_own_mutex);
               }
             else if (strcmp (name, "thread-exit-no-join-detach") == 0)
               {
                 SET_VALUE(value, __rtc_thread_exit_no_join_detach);
               }
             else if (strcmp (name, "stack-util") == 0)
               {
                 val = atoi (value);
                 if (val > 0 && val <= 100)
                   {
                     __rtc_stack_utilization = val;
                     temp_thread_trace_enabled = 1;
                   }
                 else
                   fprintf (stderr,
                           "Invalid stack-util value. Ignoring it.\n");
               }
             else if (strcmp (name, "num-waiters") == 0)
               {
                 if ((val = atoi (value)) >= 0)
                   {
                      __rtc_num_waiters = val;
                      temp_thread_trace_enabled = 1;
                   }
                 else
                   fprintf (stderr,
                          "Invalid num-waiters value. Ignoring it.\n");
               }
             else if (strcmp (name, "thread-check") == 0)
               {
                 SET_VALUE(value, temp_thread_trace_enabled);
                 SET_VALUE(value, __rtc_non_recursive_relock);  
                 SET_VALUE(value, __rtc_unlock_not_own);  
                 SET_VALUE(value, __rtc_mixed_sched_policy);  
                 SET_VALUE(value, __rtc_condvar_multiple_mutexes);  
                 SET_VALUE(value, __rtc_condvar_wait_no_mutex);  
                 SET_VALUE(value, __rtc_thread_exit_own_mutex);  
                 SET_VALUE(value, __rtc_thread_exit_no_join_detach);  
                 if (value[0] == '1') // Tests if value is 1
                   {
                     __rtc_stack_utilization = 80;
                     __rtc_num_waiters = 0;
                   }
               }
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */
             else 
               {
                  fprintf(stderr, "Invalid option \"%s\". Ignoring it.\n", name);
               }
           }
         inbuf = p; /* get next option string */
      } /* End while (inbuf != NULL) */
    free(head);
    return;
}

/*
 * Read the config file. 
 * The file supports following syntax:
 * set heap-check on/off  --> switch on/off heap and leak dectection, bounds
                              and free corruption.
 * set heap-check leaks on/off  --> switch on/off leak detection
 * set heap-check free on/off  --> switch on/off bad free. 
 * set heap-check bounds on/off  --> switch on/off bounds corruption. 
 * set heap-check header-size num  --> Size of Header for bounds corruption.
 * set heap-check footer-size num  --> Size of Footer for bounds corruption.
 * set heap-check string on/off  --> switch on/off string corruption. 
 * set heap-check scramble on/off  --> switch on/off scrambling of blocks. 
 * set heap-check min-leak-size size --> track leaks which are bigger than
                                         this size
 * set heap-check min-heap-size size --> track heaps which are bigger than
                                         this size
 * set heap-check frame_count num --> size of the frame count to be used in
                                      displaying leak context
 * set heap-check retain-freed-blocks on/off  --> Specify whether freed blocks
                                                  have to be retained
 * set thread-check recursive-relock on/off --> detect attempts to relock
                                                non-recursive mutexes
 * set thread-check unlock-not-own on/off --> detect attempts to unlock mutexes
                                              or rwlocks not owned by the
                                              current thread
 * set thread-check mixed-sched-policy on/off --> detect attempts to synchronize
                                                  threads using different
                                                  scheduling policies
 * set thread-check cv-multiple-mxs on/off --> detect when a condition variable
                                               is associated with more then
                                               one mutex
 * set thread-check cv-wait-no-mx on/off --> detect attempts to wait on a
                                             condition variable when a mutex is
                                             not locked
 * set thread-check thread-exit-own-mutex on/off--> detect attempts to exit a
                                                    thread while it's holding a
                                                    a mutex or a rwlock
 * set thread-check thread-exit-no-join-detach on/off--> detect when a
                                                         terminated thread is
                                                         neither joined nor
                                                         detached
 * set thread-check stack-util <num> --> detect when thread stack
                                          utilization exceeds a given
                                          threshold (0 - 100%)
 * set thread-check num-waiters <num> -->detect when the number of waiters
                                         on any object exceeds a given
                                         threshold
 */

static void
parse_config_string_new(const char *cfg_str)
{
  char *inbuf, *head, *p;
  int val;

#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
  static bool pa32_unsupported_msg_displayed = false;
#endif

  /* Checking for NULL input string. */
  if (cfg_str == NULL) return;
     
  head = inbuf = strdup(cfg_str);
    
  while (inbuf != NULL) 
   {
     p = strchr(inbuf, ';'); /* search for delimiter ';' */
     if (p != NULL) {
       *p++ = '\0';
	/* JAGag42027
	 * When multiple config strings are present on a single line with 
	 * semicolon as the delimiter, we have to skip the space/s or tab 
	 * present between config strings to correctly read the
	 * configurable values.
	 * Sample rtcconfig line with multiple config strings:
	 * check_bounds=1; check_free=1; scramble_block=1;
	 */
        while (*p == ' ' || *p == '\t')
          p++;
     }

     while (*inbuf == ' ' || *inbuf == '\t')
       inbuf++;

     DEBUG(printf("Read name=%s \n", inbuf);)

     if (!strncmp (inbuf, "set ", 4) || !strncmp (inbuf, "set\t", 4))
       {
         inbuf += 4;
         while (*inbuf == ' ' || *inbuf == '\t')
           inbuf++;

         if (!strncmp (inbuf, "heap-check ", 11) || 
               !strncmp (inbuf, "heap-check\t", 11))
           {
             inbuf += 11;
             while (*inbuf == ' ' || *inbuf == '\t')
               inbuf++;

             if (!strncmp (inbuf, "leaks ", 6) || !strncmp (inbuf, "leaks\t", 6))
               {
                 inbuf += 6;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_check_leaks);
               }
             else if (!strncmp (inbuf, "string ",7) || !strncmp (inbuf, "string\t", 7))
               {
                 inbuf += 7;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_check_string);
               }
             else if (!strncmp (inbuf, "free ", 5) || !strncmp (inbuf, "free\t", 5))
               {
                 inbuf += 5;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_check_free);
               }
             else if (!strncmp (inbuf, "scramble ", 9) || !strncmp (inbuf, "scramble\t", 9))
               {
		 inbuf += 9;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_scramble_blocks);
               }
             else if (!strncmp (inbuf, "bounds ", 7) || !strncmp (inbuf, "bounds\t", 7))
               {
                 inbuf += 7;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_check_bounds);
               }
             else if (!strncmp (inbuf, "retain-freed-blocks ", 20) || !strncmp (inbuf, "retain-freed-blocks\t", 20))
               {
                 inbuf += 20;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_retain_freed_blocks);
               }
             else if (!strncmp (inbuf, "frame-count ", 12) || !strncmp (inbuf, "frame-count\t", 12))
               {
                 inbuf += 12;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > 0)
                   __rtc_frame_count = val;
                 else
                   fprintf (stderr, "Invalid frame-count value. Using default value.\n");
               }
             else if (!strncmp (inbuf, "header-size ", 12) || !strncmp (inbuf, "header-size \t", 12))
               {
                 inbuf += 12;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > DEFAULT_HEADER_SIZE)
                   if (val % DEFAULT_HEADER_SIZE == 0)
                     __rtc_header_size = val;
                   else
                     __rtc_header_size = (val/DEFAULT_HEADER_SIZE)*DEFAULT_HEADER_SIZE;

                 else
                   fprintf (stderr, "Invalid header-size value. Using default value.\n");
               }
             else if (!strncmp (inbuf, "footer-size ", 12) || !strncmp (inbuf, "footer-size \t", 12))
               {
                 inbuf += 12;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > DEFAULT_FOOTER_SIZE)
                   if (val % DEFAULT_FOOTER_SIZE == 0)
                     __rtc_footer_size = val;
                   else
                     __rtc_footer_size = (val/DEFAULT_FOOTER_SIZE)*DEFAULT_FOOTER_SIZE;

                 else
                   fprintf (stderr, "Invalid footer-size value. Using default value.\n");
               }
             else if (!strncmp(inbuf, "min-heap-size ", 14) ||
                           !strncmp (inbuf, "min-heap-size\t", 14))
               {
                 inbuf += 14;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > 0)
                   __rtc_min_heap_size = val;
                 else
                   fprintf (stderr, "Invalid min-heap-size value. Ignoring option \"%s\"\n",
                               cfg_str);
               }
             else if (!strncmp(inbuf, "min-leak-size ", 14) ||
                           !strncmp (inbuf, "min-leak-size\t", 14))
               {
                 inbuf += 14;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > 0)
                   __rtc_min_leak_size = val;
                 else
                   fprintf (stderr, "Invalid min-leak-size value. Ignoring option \"%s\"\n",
                               cfg_str);
               }
             else if (!strncmp(inbuf, "on", 2) || !strncmp(inbuf, "off", 3))
               {
                 SET_VALUE(inbuf, __rtc_check_leaks);  
                 SET_VALUE(inbuf, __rtc_check_heap);  
                 SET_VALUE(inbuf, __rtc_check_free);  
                 SET_VALUE(inbuf, __rtc_check_bounds);  
               }
             else
               {
                 fprintf (stderr, "Malformed command. Ignoring option \"%s\".\n",
                        cfg_str);
               }
           }
         else if (!strncmp (inbuf, "thread-check ", 13) ||
                  !strncmp (inbuf, "thread-check\t", 13))
           {
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
             if (!pa32_unsupported_msg_displayed)
               {
                 fprintf (stderr, "Warning: Batch mode thread checking "
                                  "is not supported for PA-32 applications.\n");
                 temp_thread_trace_enabled = 0;
                 pa32_unsupported_msg_displayed = true;
               }
#else
             inbuf += 13;
             while (*inbuf == ' ' || *inbuf == '\t')
               inbuf++;
             if (   !strncmp (inbuf, "recursive-relock ", 17)
                 || !strncmp (inbuf, "recursive-relock\t", 17))
               {
                 inbuf += 17;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_non_recursive_relock);
               }
             else if (   !strncmp (inbuf, "unlock-not-own ", 15)
                      || !strncmp (inbuf, "unlock-not-own\t", 15))
               {
                 inbuf += 15;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_unlock_not_own);
               }
             else if (   !strncmp (inbuf, "mixed-sched-policy ", 19)
                      || !strncmp (inbuf, "mixed-sched-policy\t", 19))
               {
                 inbuf += 19;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_mixed_sched_policy);
               }
             else if (   !strncmp (inbuf, "cv-multiple-mxs ", 16)
                      || !strncmp (inbuf, "cv-multiple-mxs\t", 16))
               {
		 inbuf += 16;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_condvar_multiple_mutexes);
               }
             else if (   !strncmp (inbuf, "cv-wait-no-mx ", 14)
                      || !strncmp (inbuf, "cv-wait-no-mx\t", 14))
               {
                 inbuf += 14;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_condvar_wait_no_mutex);
               }
             else if (   !strncmp (inbuf, "thread-exit-own-mutex ", 22)
                      || !strncmp (inbuf, "thread-exit-own-mutex\t", 22))
               {
                 inbuf += 22;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_thread_exit_own_mutex);
               }
             else if (   !strncmp (inbuf, "thread-exit-no-join-detach ", 27)
                      || !strncmp (inbuf, "thread-exit-no-join-detach\t", 27))
               {
                 inbuf += 27;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 SET_VALUE(inbuf, __rtc_thread_exit_no_join_detach);
               }
             else if (   !strncmp (inbuf, "frame-count ", 12)
                      || !strncmp (inbuf, "frame-count\t", 12))
               {
                 inbuf += 12;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > 0)
                   __rtc_frame_count = val;
                 else
                   fprintf (stderr,
                          "Invalid frame-count value. Using default value.\n");
               }
             else if (   !strncmp(inbuf, "stack-util ", 11)
                      || !strncmp (inbuf, "stack-util\t", 11))
               {
                 inbuf += 11;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 val = atoi (inbuf);
                 if (val > 0 && val <= 100)
                   {
                     __rtc_stack_utilization = val;
                     temp_thread_trace_enabled = 1;
                   }
                 else
                   fprintf (stderr,
                           "Invalid stack-util value. Ignoring option \"%s\"\n",
                            cfg_str);
               }
             else if (   !strncmp(inbuf, "num-waiters ", 12)
                      || !strncmp (inbuf, "num-waiters\t", 12))
               {
                 inbuf += 12;
                 while (*inbuf == ' ' || *inbuf == '\t')
                   inbuf++;
                 if ((val = atoi (inbuf)) > 0)
                   {
                      __rtc_num_waiters = val;
                      temp_thread_trace_enabled = 1;
                   }
                 else
                   fprintf (stderr,
                          "Invalid num-waiters value. Ignoring option \"%s\"\n",
                           cfg_str);
               }
             else if (   !strncmp(inbuf, "on", 2)
                      || !strncmp(inbuf, "off", 3))
               {
                 SET_VALUE(inbuf, temp_thread_trace_enabled);
                 SET_VALUE(inbuf, __rtc_non_recursive_relock);  
                 SET_VALUE(inbuf, __rtc_unlock_not_own);  
                 SET_VALUE(inbuf, __rtc_mixed_sched_policy);  
                 SET_VALUE(inbuf, __rtc_condvar_multiple_mutexes);  
                 SET_VALUE(inbuf, __rtc_condvar_wait_no_mutex);  
                 SET_VALUE(inbuf, __rtc_thread_exit_own_mutex);  
                 SET_VALUE(inbuf, __rtc_thread_exit_no_join_detach);  
                 if (inbuf[1] == 'n') // Tests if inbuf is 'on'
                   {
                     __rtc_stack_utilization = 80;
                     __rtc_num_waiters = 0;
                   }
               }
             else
               {
                 fprintf (stderr,
                          "Malformed command. Ignoring option \"%s\".\n",
                          cfg_str);
               }
#endif
           }
        /* The user could directly have set frame-count <frame_count> in
           the rtcconfig file. */
        else if (   !strncmp (inbuf, "frame-count ", 12)
                 || !strncmp (inbuf, "frame-count\t", 12))
           {
             inbuf += 12;
             while (*inbuf == ' ' || *inbuf == '\t')
               inbuf++;
             if ((val = atoi (inbuf)) > 0)
               __rtc_frame_count = val;
             else
               fprintf (stderr,
                      "Invalid frame-count value. Using default value.\n");
           }
         else
           {
	     fprintf( stderr, "Malformed command. Ignoring option \"%s\".\n", cfg_str);
           }
       }
     else if (*inbuf != '\0') /* allow empty config string */
       {
         fprintf( stderr, "Malformed command. Ignoring option \"%s\".\n", cfg_str);
       }
     inbuf = p;
   }/* End while */

   inbuf = NULL;
   free (head);
}

static int
read_config_file(FILE *fp)
{
   int  val, len;
   char *inbuf;
   long int size;

   fseek (fp, 0, SEEK_END);
   size = ftell (fp);
   /* allow empty config file, just return succeed */
   if (size == 0) return 1;
   rewind (fp);
   inbuf = alloca(size);

   while (fgets(inbuf, (int)size, fp) != NULL && (len = (strlen(inbuf) - 1)))
     {
       if (inbuf[len] == '\n')
         inbuf[len] = '\0';
       if (strchr(inbuf,'=') == NULL)
         parse_config_string_new(inbuf);
       else 
         parse_config_string(inbuf);
     }

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
   if (   __rtc_non_recursive_relock
       || __rtc_unlock_not_own
       || __rtc_mixed_sched_policy
       || __rtc_condvar_multiple_mutexes
       || __rtc_condvar_wait_no_mutex
       || __rtc_thread_exit_own_mutex
       || __rtc_thread_exit_no_join_detach)
     {
       temp_thread_trace_enabled = 1;
     }

   if (    temp_thread_trace_enabled
       && (   __rtc_check_leaks || __rtc_check_heap
           || __rtc_check_free || __rtc_check_bounds
#ifdef HP_IA64
           || __rtc_check_openfd
#endif /* HP_IA64 */
          ))
     {
       fprintf (stderr, "Warning: Enabling heap-check and thread-check "
                        "together is not supported. Turning thread-check "
                        "off.\n");
       temp_thread_trace_enabled = 0;
     }
#endif

   if (ferror(fp))
     return 0;
   return 1;
}

/* 
 *  BATCH RTC config file is called rtcconfig
 *  Here is the priority order:
 *  - GDBRTC_CONFIG
 *  - current working directory
 * Returns file pointer to already opened config file. If file
 * cannot be found or opended, it returns NULL;
 */

static FILE * 
open_config_file( char *envstr)
{
    char *fname;
    FILE *fp = NULL;
    char config_file[PATH_MAX + 1];

    config_file[0] = NULL;

    if ((fname = getenv ("GDBRTC_CONFIG")) == NULL) 
      {
        libc_strcpy (config_file, rtcenv[RTC_CWD]);
        libc_strcat (config_file, "/");
        libc_strcat (config_file, "rtcconfig");
      } 
    else
      libc_strcpy (config_file, fname);

    if (config_file[0] != NULL) 
      {
         if ((fp = fopen (config_file, "r")) == NULL) 
           {
#if HP_VACCINE_DBG_MALLOC
              /* it is ok no config file when there is predefined config 
                 or RTC_MALLOC_CONFIG env variable is defined */
              if (has_predefined_config() ||
                  (envstr && *envstr !='\0'))
                {
                  rtcenv[RTC_CONFIG_FILE] = NULL;
                  return (FILE *)NULL;
                }
#endif /* HP_VACCINE_DBG_MALLOC */
              perror ("Cannot open config file - rtcconfig.");
              return (FILE *)NULL;
           }
      }

    if ((rtcenv[RTC_CONFIG_FILE] = 
            (char *)malloc (strlen(config_file) + 1)) == NULL) 
      {
         perror ("librtc internal malloc failed");
         return 0;
      }

    libc_strcpy (rtcenv[RTC_CONFIG_FILE], config_file);
    return fp;
}

/* Look for gdb in the following order
 * - GDB_SERVER
 * - /opt/langtools/bin/gdb
 * Returns nothing
 */
#define GDB_INSTALL_PATH "/opt/langtools/bin/gdb"
static boolean
find_gdb(char *g, size_t sz)
{
    char *tp;
    if ((tp = getenv ("GDB_SERVER")) != NULL) 
      {
         if (strlen (tp) > sz) 
           {
             fprintf (stderr, "GDB_SERVER value too long. Ignoring\n");
             libc_strcpy (g, GDB_INSTALL_PATH);
           } 
           else
             libc_strcpy (g,tp);
       } 
       else
         libc_strcpy (g, GDB_INSTALL_PATH);

    DEBUG(printf("gdb = %s\n",g);)

    /* Make sure that gdb exists */
    struct stat statbuf;

    if (0 == stat(g, &statbuf))
      return true;

    return false;
}
/* JAGag40586: do not call ctime when called from rtc_initialize.
*/
static int 
init_config(char *file, boolean call_ctime)
{
    time_t t;
    char  *tp;
    /* path could be longer than PATH_MAX because of relative paths */
    char   buf[2*PATH_MAX + 1]; 

#if HP_VACCINE_DBG_MALLOC
    if ((pre_allocated_mem_pool = malloc (PREDEFINED_MEM_POOL_SIZE)) == NULL)
      {
         perror ("librtc internal malloc failed");
         return 0;
      }
#endif /* HP_VACCINE_DBG_MALLOC */

    if ((tp = getcwd (buf, PATH_MAX + 1)) == NULL) 
      {
         perror ("getcwd failed");
      }
    if ((rtcenv[RTC_CWD] = (char *)malloc (strlen(buf) + 1)) == NULL) 
      {
         perror ("librtc internal malloc failed");
         return 0;
      }
    libc_strcpy (rtcenv[RTC_CWD], buf);

    if (call_ctime)
      {
        time (&t);
        tp = ctime (&t);
        if ((rtcenv[RTC_START_TIME] = (char *)malloc (strlen(tp) + 1)) == NULL) 
          {
            perror ("librtc internal malloc failed");
            return 0;
          }
        libc_strcpy (rtcenv[RTC_START_TIME],tp);
      }
    else
      rtcenv[RTC_START_TIME] = NULL; 
    if ((rtcenv[RTC_FILE] = (char *)malloc (strlen (file) + 1)) == NULL) 
      {
        perror ("librtc internal malloc failed");
        return 0;
      }

    libc_strcpy (rtcenv[RTC_FILE], file);
    DEBUG(printf("RTC_FILE is %s\n", rtcenv[RTC_FILE]);)

    rtcenv[RTC_OUTPUT_DIR] = rtcenv[RTC_LAST] = NULL;

    /* If nothing in rtcfiles array => trace all executables. */
    rtcfiles[0] = NULL;
    return 1;
}

static int 
process_config()
{
    FILE  *config_fp;
    char  *envstr = getenv("RTC_MALLOC_CONFIG");
    int   rval = 0;
    char path_buf[PATH_MAX+1];

    
    /* JAGaf70635 - saves the original directory in case
     * a.out does a chdir to somewhere else.  
     */

    if ((getcwd(path_buf, PATH_MAX+1)) == NULL)
    {
       perror("In librtc, unable to get current directory");
    }

    libc_strcpy(original_path,path_buf);

#if HP_VACCINE_DBG_MALLOC
    /* process predefined config first */
#if HP_VACCINE_DBG_MALLOC_IA64
    /* __rtc_predefined_bounds_config must be processed before
       __rtc_predefined_config so we can support both +check=bounds:pointer
       and +check=malloc at the same time */
    parse_config_string(__rtc_predefined_bounds_config);
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

    parse_config_string(__rtc_predefined_config);

#if HP_VACCINE_DBG_MALLOC_IA64
    parse_config_string(__rtc_predefined_threads_config);
#endif /* HP_VACCINE_DBG_MALLOC_IA64 */

#if HP_VACCINE_DBG_MALLOC_IA64
    /* __rtc_predefined_bounds_post_config must be processed after
       __rtc_predefined_config to override the +check=malloc setting */
    parse_config_string(__rtc_predefined_bounds_post_config);
#endif  /* HP_VACCINE_DBG_MALLOC_IA64 */

#endif /* HP_VACCINE_DBG_MALLOC */

    /* then process rtcconfig file */
    if ((config_fp = open_config_file (envstr)) != NULL) 
      {

         DEBUG(printf("RTC_CONFIG_FILE %s\n", rtcenv[RTC_CONFIG_FILE]);)
         DEBUG(printf("RTC_CWD %s\n", rtcenv[RTC_CWD]);)

         if (read_config_file (config_fp) == 0) 
           {
              fprintf (stderr, "read_config_file failed\n");
              rval = 0;
           } 
         else
           rval = 1;
         fclose (config_fp);
      }

    /* last config string in the RTC_MALLOC_CONFIG */
    if (envstr != NULL && *envstr != '\0') {
       parse_config_string(envstr);
       rval = 1;
    }

#if HP_VACCINE_DBG_MALLOC
    /* it is ok no config file when there is predefined config */
    if (has_predefined_config())
        rval = 1;
#endif /* HP_VACCINE_DBG_MALLOC */

#if HP_VACCINE_DBG_MALLOC_IA64
   if (   __rtc_non_recursive_relock
       || __rtc_unlock_not_own
       || __rtc_mixed_sched_policy
       || __rtc_condvar_multiple_mutexes
       || __rtc_condvar_wait_no_mutex
       || __rtc_thread_exit_own_mutex
       || __rtc_thread_exit_no_join_detach)
     {
       temp_thread_trace_enabled = 1;
     }

   if (    temp_thread_trace_enabled
       && (   __rtc_check_leaks || __rtc_check_heap
           || __rtc_check_free || __rtc_check_bounds
#ifdef HP_IA64
           || __rtc_check_openfd
#endif /* HP_IA64 */
          ))
     {
       fprintf (stderr, "Warning: Enabling heap-check and thread-check "
                        "together is not supported. Turning thread-check "
                        "off.\n");
       temp_thread_trace_enabled = 0;
     }
#endif /* HP_VACCINE_DBG_MALLOC_IA64 */

    return rval;
}

static int
file_is_librtc(char *s)
{
    char *tp = NULL;

    if ((tp = strrchr (s,'/')) != NULL) 
      {
        ++tp;
#ifdef HP_IA64
#ifndef DLD_RTC
        if (strcmp (tp, "librtc.so") == 0   || 
            strcmp (tp, "librtc.sl") == 0   ||
            strcmp (tp, "librtc64.sl") == 0 ||
            strcmp (tp, "librtc64.so") == 0 ||
	    strcmp (tp, "librtc.so.1") == 0 ||
            strcmp (tp, "librtc64.so.1") == 0) 
#else // DLD_RTC
        if (strncmp (tp, "librtc-dld.so", 13) == 0)
#endif // DLD_RTC
#else
        if (strcmp (tp, "librtc.sl") == 0 || 
            strcmp (tp, "librtc64.sl") == 0) 
#endif
          {
             return 1;
          }
      }
    return 0;
}

/* This function checks to see if the output file can be created.
 * It does not write any data in the file. The following order is used
 * to decide the place for output data
 * - RTC_OUTPUT_DIR, if specified and output file can be created there
 * - the current working directory and if output file can be created there
 * - stdout
 *
 * return opened file if keep_it_open is true, otherwise return NULL
 */

static FILE *
create_output_file(char *file,  char *pid, char *suffix, char *output_file,
                   char *output_dir, int keep_it_open)
{
  int fd;
  FILE *fp = NULL;

  if (output_dir != NULL) 
    {
      sprintf (output_file,"%s/%s.%s.%s", output_dir, file, pid, suffix);
      DEBUG(printf( "Trying output file=%s\n",output_file););

      unlink (output_file);
      fd = open (output_file, O_RDWR | O_EXCL | O_CREAT, 0777);
      if (fd != -1 && (fp = fdopen (fd, "w")) != NULL) 
        {
            if (!keep_it_open)
              {
                fclose (fp);
                fp = NULL;
                unlink (output_file);
              }
            return fp;
        } 
      else 
        {
          perror ("librtc cannot open output file. Using current directory");
        }
    }

  sprintf (output_file,"%s.%s.%s", file, pid, suffix);
  DEBUG(printf ("Trying output file=%s\n",output_file););

  unlink (output_file);
  fd = open (output_file, O_RDWR | O_EXCL | O_CREAT, 0777);
  if (fd != -1 && (fp = fdopen (fd, "w")) != NULL) 
    {
      if (!keep_it_open)
        {
          fclose (fp);
          fp = NULL;
          unlink (output_file);
        }
    }
  else
    {
      perror ("librtc cannot open output file in current directory. "
              "Using stdout/stderr");
      libc_strcpy (output_file, "");
    } 

  DEBUG(printf("output_file = %s\n", output_file);)
  DEBUG(fflush (stdout));
  return fp;
}

/* if fname is "stderr" return stderr
   if fname starts with '+', open the file for append,
   otherwise open the file for write
*/
static FILE *
open_log_file(char *fname)
{
   FILE *fp = stderr;
   if (fname != NULL && strcmp(fname, "stderr") !=0) {
      if (*fname == '+')
        {
          fp = fopen(fname+1, "a+");
        }
      else
        {
          int fd;
          unlink (fname);
          if ((fd = open (fname, O_RDWR | O_EXCL | O_CREAT, 0777)) != -1)
            {
              fp = fdopen(fd, "w");
            }
        }
   }
   if (fp == NULL)
      fp = stderr; 
   return fp;
}

#ifdef HP_IA64
/* This is gdb independent rtc heap/leak analyzer. Instead of invoking gdb,
 * this function collects the necessary info on it's own and initiates the
 * mark and sweep algorithm. At the end, it reports the heap/leak data.
 *
 * =======================================================================
 * NOTE : IMPORTANT ** IMPORTANT ** IMPORTANT **
 * =======================================================================
 * This vvv HP-UX/IPF specific.
 *
 * This function is called only for IPF, from HP-UX version 11.23 onwards. 
 * It is not portable to any other platform, or previous HP-UX versions.
 * It is the responsibility of the caller to check this, before invoking
 * this function.
 * =======================================================================
 */
static void 
rtc_leak_analyze()
{
  static int printed = 0; /* don't print it twice */
  uint64_t final_var_on_stack;

  /* Do not invoke gdb again if this is a batch mode thread check case. */
  if (thread_trace_enabled)
    return; 

#if HP_VACCINE_DBG_MALLOC
  /* Nothing to do if we don't check leak or heap */
  if (!__rtc_check_leaks && !__rtc_check_heap
#ifdef HP_IA64
      && !__rtc_check_openfd
#endif /* HP_IA64 */
     )
    return;
#endif

  if (printed && !is_rtcapi_called)
    return;

  if (!rtc_started)
    return;

  skip_bookkeeping = 1;

  /* Another thread may have taken the lock. Before suspending them,
     we need to take the lock, otherwise we will get into a deadlock */
  mutex_lock (&mutex);

  /* We would stop all threads and do the work on the current thread.
     For MxN apps, this may sometimes lead to deadlock if the current thread
     attempts a SA switch. According to pthread folks, the way to prevent
     the SA switch is to do a msem_lock, stop all other threads, perform
     tasks from current thread, resume all threads and finally do a
     msem_unlock.
  */
  int retval;
  msem_init_stat = 1;
  msem_lock_stat = 1;

  DEBUG(printf("Starting to get semaphore()...\n");)

  if (!msem_init(&msem_for_selfrtc, MSEM_UNLOCKED))
    {
      msem_init_stat = 0;
      DEBUG(printf("msem_init failed errno = %d\n", errno);)
    }

  if (msem_init_stat)
    {
      DEBUG(printf("Got semaphore()...\n");)
      retval = msem_lock(&msem_for_selfrtc, MSEM_IF_NOWAIT);
      if (retval)
        {
          msem_lock_stat = 0;
          DEBUG(printf("msem_lock failed errno = %d\n", errno);)
        }

      if (msem_lock_stat)
        DEBUG(printf("Sem P done\n");)
    }

  printed = 1;

  DEBUG(printf("Starting rtc_leak_analyze()...\n");)

  lwpbuf = libc_malloc (sizeof(struct lwp_status) * RTC_MAX_THREADS);
  bspstore_regs = libc_malloc (sizeof(uint64_t) * RTC_MAX_THREADS);

  /* Get the info on threads -
     1) Stop the other threads.
     2) How many threads ? Their status..
     3) Get registers of other threads
     4) Get RSE of other threads
     5) Do a mark-sweep and report heap/leak data.
     6) Resume the other threads.

     NOTE: The code for these is HP-UX specific as of now.
  */

  /* Task 1 : Stop the other threads : HP-UX 11.23 and 11.31 */
  suspend_all_threads();

  int is_this_main_thread = 0;

  int ret;

  /* Task 2, 3, 4 : Get thread context information */
  ret = get_thread_contexts(&stack_end,
                            &is_this_main_thread);
  libc_free (lwpbuf);
  libc_free (bspstore_regs);

  if (!ret)
    resume_and_return ();

  /* If this current thread is the main thread, then the stack end
     can be defined by the address of a local variable on the current
     frame.
  */
  if (is_this_main_thread)
    stack_end = (char *)&final_var_on_stack;
  else
    {
      /* If this is not the main thread, then reliably getting the stack
         end for main thread is difficult. We use the gr12 from main
         thread context using __uc_get_grs(), but sometimes it's not
         reliable. If the value of gr12 is outside the possible stack region,
         we define the stack end to be at the end of the max possible
         stack end.
      */
      uint64_t max_stack_size = 0;
#ifdef __LP64__
      ret = gettune("maxssiz_64bit", &max_stack_size);
#else
      ret = gettune("maxssiz", &max_stack_size);
#endif
      if (ret == 0)
        {
          DEBUG(printf("main thread stack end = %p\n", stack_end);)
          DEBUG(printf("max stack size = %"PRIx64"\n", max_stack_size);)

          if (stack_base - stack_end > max_stack_size)
            stack_end = (char *)stack_base - max_stack_size + 1;

          DEBUG(printf("main thread stack end = %p\n", stack_end);)
        }
      else
        DEBUG(printf("gettune returned %d, err no = %d\n", ret, errno);)
    }

  /* Fix for JAGag24121:  +check=malloc reports spurious leaks.  Shared
   * library data segments were not being scanned with +check=malloc,
   * leading to spurious leak reports for pointers to blocks kept in shlib
   * data segments.  The fix is to update the shlib_info data structures as
   * Srikanth suggested.
   */
  if (rtc_started)
    update_shlib_info();

  invoked_internally = 1;

  /* Task 5: Do a mark-sweep and generate report */
  /* If leaks or heap or corruption RTC-API is not called then,
     it is batch mode. So produce the memory report only for batch mode */
  if (!is_rtcapi_called)
   produce_memory_report(getpid());

  /* Release the RTC mutex */
  mutex_unlock (&mutex);
}

static void resume_and_return ()
{
  invoked_internally = 0;
  int retval;
  /* Task 6 : Resume all threads */
  resume_all_threads();

  /* Unlock and release the msem, acquired to prevent SA switch */
  if (msem_lock_stat)
    {
      retval = msem_unlock(&msem_for_selfrtc, 0);
      if (retval)
        {
          DEBUG(printf("msem_unlock failed, errno = %d\n", errno);)
        }
    }

  if (msem_init_stat)
    {
      retval = msem_remove(&msem_for_selfrtc);
      if (retval)
        {
          DEBUG(printf("msem_remove failed, errno = %d\n", errno);)
        }
    }

 /* Reset variables representing success/failure of semaphore initialization
    and locking */
  msem_init_stat = 0;
  msem_lock_stat = 0;

  skip_bookkeeping = 0;
  return;
}

/* This function in Batch mode is called,
   to make sure that the rtc_leak_analyze() and resume_and_return ()
   are called once by appication. This function is called from
   __thread_once () at exit time in batch mode.
  **** MUST BE CALLED ONLY IN BATCH MODE ****  */
static void analyze_resume_return ()
{
  rtc_leak_analyze ();
  resume_and_return ();
}

#endif // HP_IA64

volatile int __gdb_synchronizer = 0;

#pragma NOINLINE __sync_up_with_gdb
/* gdb places a breakpoint here. */
void
__sync_up_with_gdb ()
{
}

static void
wait_for_gdb_here (int child_pid)
{
  /*
   * JAGaf88950
   * parent process - wait outside the system call. The
   * following lines are important. The parent process waits here.
   * Later, gdb attaches to this process and changes the value
   * of the variable so that the parent can continue.
   * The parent can not wait in a busy loop.  Sometimes it
   * can wait forever.  Wait for certain amount of time then
   * clean up.
   * JAGag23251 Bindu 11/08/06
   * Debugger also sets a silent breakpoint at __sync_up_with_gdb and
   * continues the process. This is useful when gdb attaches to the parent
   * before fork syscall completed. We want the exiting thread to be
   * out of syscalls before we can issue the info commands
   */
  /*
   * JAGag00737 - the original implementation of 88950 had
   * an inside empty loop to cause additional delay.  The optimizer
   * removed the loop so a different solution is needed.
   * I replaced it with a sleep but that's not good either.
   * it resulted in a Current thread is blocked error because
   * gdbrtc detected the thread was TTS_WASSLEEPING.
   * Even if we use something like nanosleep there is still
   * a danger gdbrtc detects thread is sleeping.  There's not
   * much choice but to have some kind of a loop.
   */
  long long int wait_cnt = 0;
  struct pst_status pst_buf;
  int pstat_ret = 0;

  for (wait_cnt = 0; wait_cnt < MAXWAIT; wait_cnt++)
    {
      int inner=0;
      /* wait; synchronize with gdb */
      for (inner = 0; inner < 1024; inner++)
        {
          if (__gdb_synchronizer)
          break;
        }

      /* check every few iterations to see if the child gdb died */
      if (0 == wait_cnt % (MAXWAIT/1024))
        {
          pstat_ret = pstat_getproc(&pst_buf,
                                    sizeof(struct pst_status),
                                    0,
                                    child_pid);
          /* Has the process exited ? Is the gdb process in zombie state ?
             Return from here in these cases. No heap/leak reports would
             be generated - gdb encountered some problem (version mismatches
             with librtc ?) and either exited or is in zombie state.
          */
          if (   (-1 == pstat_ret) && (ESRCH == errno)
              || (pst_buf.pst_stat == PS_ZOMBIE))
            {
              /* perror reports the log describing the last error occurred.
                 At this point, we would like to report ESRCH. Save any earlier
                 errno and restore it after the perror call.
              */
              int ernum_save = errno;
              errno = ESRCH;
              perror("Gdb process exited. No data would be reported");
              errno = ernum_save;
              return;
            }
        }

      if (__gdb_synchronizer)
        break;
    }

  if (wait_cnt == MAXWAIT)
        perror("Broken synchronization between child/parent process");

  __sync_up_with_gdb ();
}

/* This function prints leaks/heap data to a file. It does it by forking and 
 * execing gdb in child process. gdb attaches to the parent process and uses 
 * the gdb infrastructure to print leaks/heap data.
 */
static void 
print_batch_info()
{
    char *tp;
    int fd;
    pid_t cpid;
    char  pid[20];
    FILE *gdbp;
    char  cnt[20];
    char  buf[PATH_MAX + 100];
    char  cmd[3*PATH_MAX + 1200];
    static int printed=0; /* don't print it twice */
    char  rtcinit_fname[50];  /* contains .rtcinit_pidnumber */
    char *envstr;
 
    /* Do not invoke gdb again if this is a batch mode thread check case. */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
    if (thread_trace_enabled)
       return; 
#endif

#if HP_VACCINE_DBG_MALLOC
    /* do not call gdb if we don't check leak or heap */
    if (!__rtc_check_leaks && !__rtc_check_heap)
       return;
#endif

    if (printed)
      return;
    printed = 1;
    tp = strrchr (rtcenv[RTC_FILE], '/');
    if (tp == NULL || *tp != '/')
      tp = rtcenv[RTC_FILE];
    else
      tp++;

    /* Fix for JAGag24121:  +check=malloc reports spurious leaks.  Shared
     * library data segments were not being scanned with +check=malloc,
     * leading to spurious leak reports for pointers to blocks kept in shlib
     * data segments.  The fix is to update the shlib_info data structures as
     * Srikanth suggested.
     */
#ifndef DLD_RTC
    if (rtc_started)
      update_shlib_info();
#endif // DLD_RTC

    sprintf (pid,"%d", getpid());

    /* JAGag12946 */
    libc_strcpy (cmd, "set height 0\nset {int} &shlib_info_invalid=0\nset {int} &__gdb_synchronizer=1\n");
    libc_strcat(cmd, "set heap-check frame-count ");
    sprintf(cnt, "%d\n", __rtc_frame_count);
    libc_strcat(cmd, cnt);

    libc_strcat(cmd, "set heap-check min-leak-size ");
    sprintf(cnt, "%d\n", __rtc_min_leak_size);
    libc_strcat(cmd, cnt);

    /* JAGaf89828 */
    libc_strcat(cmd, "set heap-check min-heap-size ");
    sprintf(cnt, "%d\n", __rtc_min_heap_size);
    libc_strcat(cmd, cnt);

    /* JAGaf70635 - before fork save the path where
     * we are now, change it to the original path of where
     * a.out was (which was saved in orignal_path in process_config)
     * In most cases, the orignal and changed paths should be
     * the same.  It's different when a.out does chdir. 
     */
    if ((getcwd(buf, PATH_MAX+1)) == NULL)
    {
       perror("In librtc, unable to get current directory");
    }

    libc_strcpy(changed_path, buf);

    if (chdir(original_path) != 0) 
    {
       sprintf(buf, "In librtc, unable to chdir to original path %s\n", 
               original_path);
       perror(buf);
    }
    DEBUG(libc_strcat (cmd, "info threads\n");)

    /* Place a silent temporary breakpoint on __sync_up_with_gdb */
    libc_strcat (cmd, "tb __sync_up_with_gdb\n");
    libc_strcat (cmd, "commands\n");
    libc_strcat (cmd, "silent\n");
    libc_strcat (cmd, "end\n");

    /* Continue the process so that we hit this breakpoint.
       Note that this will be the only breakpoint in batch mode,
       so we should hit this breakpoint on the first continue. */
    libc_strcat (cmd, "c\n");

    if (__rtc_check_leaks) 
      {
         libc_strcat (cmd, "info leaks ");
#if HP_VACCINE_DBG_MALLOC
         if (__rtc_leak_logfile == NULL) {
            create_output_file (tp, pid, "leaks", buf, 
                                rtcenv[RTC_OUTPUT_DIR], 0);
            libc_strcat (cmd, buf);
         } else {
           libc_strcat (cmd, __rtc_leak_logfile);
         }
#else
         create_output_file (tp, pid, "leaks", buf, 
                             rtcenv[RTC_OUTPUT_DIR], 0);
         libc_strcat (cmd, buf);
#endif  /* HP_VACCINE_DBG_MALLOC */     
         libc_strcat (cmd, "\n");
      }
    if (__rtc_check_heap) 
      {
         libc_strcat (cmd, "info heap ");
#if HP_VACCINE_DBG_MALLOC
         if (__rtc_heap_logfile == NULL) {
            create_output_file (tp, pid, "heap", buf, 
                                rtcenv[RTC_OUTPUT_DIR], 0);
            libc_strcat (cmd, buf);
         } else {
           libc_strcat (cmd, __rtc_heap_logfile);
         }
#else
          create_output_file (tp, pid, "heap", buf, 
                              rtcenv[RTC_OUTPUT_DIR], 0);
          libc_strcat (cmd, buf);
#endif  /* HP_VACCINE_DBG_MALLOC */ 
         libc_strcat (cmd, "\n");
      }
    /* Quit, now that we are done printing leak and heap info. */
    libc_strcat (cmd, "q\n");

    /* JAGag39042 - construct a unique .rtcinit file.  It's the same
     * idea as /tmp/__heap_info.###
     */
    /* QXCR1000765439 - use RTC_OUTPUT_DIR, which gets it value from 
       "output_dir" option in the rtcconfig file to create .rtcinit_<pid>
       file.
    */
    if (rtcenv[RTC_OUTPUT_DIR])
      {
        strcpy(rtcinit_fname, (rtcenv[RTC_OUTPUT_DIR]));
        strcat(rtcinit_fname,"/");
        strcat(rtcinit_fname,".rtcinit_");
      }
    else
     strcpy(rtcinit_fname,".rtcinit_");
 
    strcat(rtcinit_fname, pid);
    unlink (rtcinit_fname);
    fd = open (rtcinit_fname, O_RDWR | O_EXCL | O_CREAT, 0777);
    if (fd == -1 || (gdbp = fdopen (fd, "w")) == NULL) 
      {
         perror("Cannot open .rtcinit. Cannot print RTC information");
         return; /* bail out, no output */
      }

    fprintf (gdbp, "%s", cmd);
    fclose (gdbp);
    DEBUG(printf ("rtcinit is written to %s\n", rtcinit_fname);)
    DEBUG(printf ("rtcinit is %s", cmd);)
    
    DEBUG(printf("__rtc_check_leaks=%d __rtc_check_heap=%d\n",
                  __rtc_check_leaks, __rtc_check_heap);)

    boolean found_gdb = find_gdb (buf, PATH_MAX); /* invoke gdb to print details */

    if (!found_gdb)
      {
        perror(buf);
        DEBUG(printf ("No heap/leak information would be generated"));
        goto bailout;
      }

    DEBUG(printf("exec=%s pid=%s\n", buf, pid);)

    if ((cpid = fork ()) == 0) 
      { /* child process */
	 char *debug_cmd = "-n";  /* something redundant, but parseable */

         /* JAGag12951 - Pass the executable name while attaching. 
         Use pstat_getproc() to do that*/
         char *exec_name = NULL;
         int temp_pid = 0;

         putenv ("LD_PRELOAD="); /* remove LD_PRELOAD from env before exec */
         sscanf (pid, "%d", &temp_pid);

         __inside_librtc = shlib_info_invalid = 0;
	 if (getenv("RTC_DEBUG_GDB") != NULL)
	    debug_cmd = "--debug-gdb";
         /* JAGag26786: Use this new function to get the exec_name.
            pst.ucomm will give the basename only not the whole path. */
         exec_name = get_executable_path (temp_pid);
   
         DEBUG(printf ("pid is %d\n",temp_pid);)
         DEBUG(printf ("Exec file name is %s\n", exec_name);)
         DEBUG(printf ("forked successfully buf = %s cpid=%d\n", buf, cpid);)
         DEBUG(printf ("execl(%s, %s, -p, %s, -x, .rtcinit, -tty=/dev/null, "
                       "-q, -brtc, -leaks, (char *)0)\n", buf, buf, pid));
	 DEBUG(fflush (stdout));

       /* Suresh, April 08: New environment variable that will
          allow processing of .gdbinit file during batchrtc and
          +check. Using this mechanism the user gets the power to
          run any command during the batch rtc run. It can also
          be used as a backdoor to run any debug commands like
          info threads or some such much to debug the state of the
          application before the RTC report generation commands
          are run from "rtcinit_fname" file

          But we cannot honour the "RTC_DEBUG_GDB" flag in this 
          case, so the user cannot get an interactive prompt to
          debug batch run, if he specfies RTC_PROCESS_GDBINIT

          This will address the long standing request to run other
          gdb commands during a batch or +check run. This would 
          also address the Amdocs request to set dir/objectdir/pathmap
          commands during a batch RTC run. Instead of putting these
          gdb commands in the rtcconfig file, we have taken this
          approach of allowing the existing .gdbinit file to be 
          processed before processing the hand generated 
          "rtcinit_fname" file. This approach would also make 
          this feature available for free for the +check case 
          as well, without any code change in the +check side !!

          Caution: He has to excercise this power carefully, because
          if he has errnoneous commands in .gdbinit, the batchrtc
          session can possibly hang and not produce the expected
          RTC reports !!

          For eg; 1) even if he uses a 'q'(quit), the session would 
          hang(and finally terminate after 10 mins(approx)), and not 
          execute the commands from hand generated "rtcinit_fname" 
          and hence will not generate any RTC reports. it would print
          "Broken synchronization between child/parent process" error
                  2) if he uses any command that would hog time, that 
          would interfere with RTC's assumptions and print the 
          "Broken synchronization between child/parent process" error
        */
        if ((envstr = getenv("RTC_PROCESS_GDBINIT")) != NULL &&
                               (strcmp(envstr, "1") == 0))
        {

         struct stat stat_buf;
         char config_file[PATH_MAX + 1];

         config_file[0] = NULL;
         libc_strcpy (config_file, rtcenv[RTC_CWD]);
         libc_strcat (config_file, "/");
         libc_strcat (config_file, ".gdbinit");
         if( stat( config_file, &stat_buf) == 0 )
           {
           DEBUG( printf ("\nwarning: Running commands from .gdbinit now \n");)
           }
         else
           {
            perror ("\nwarning: Unable to open .gdbinit");
            printf("Cound not open %s\n", config_file);
           }

/* Passing the executable name for attach is failing on IA.
   So temporarly putting under ifdef.*/
#ifndef HP_IA64
         if (execl (buf, buf, exec_name, "-p", pid,
                  "-x", rtcinit_fname, "-batch", "-tty=/dev/null", "-q",
                  "-brtc", "-leaks", (char *)0) == -1)
#else
         if (execl (buf, buf, "-p", pid, 
                  "-x", rtcinit_fname, "-batch", "-tty=/dev/null", "-q",
                  "-brtc", "-leaks", (char *)0) == -1)
#endif
           {
              perror ("execl failed. Cannot print RTC info"); /* bail out */
              perror ("Not able to spawn gdb OR run the RTC commands"); 
              perror ("Check your .gdbinit file for any errors"); 
              DEBUG(printf ("execl failed buf = %s cpid=%d\n", buf, cpid);)
              unlink (rtcinit_fname); /* remove .rtcinit_pid file */
              libc_exit (1);
           }
        } 
        else  /* No .gdbinit file to process */
        {
/* Passing the executable name for attach is failing on IA.
   So temporarly putting under ifdef.*/
#ifndef HP_IA64
         if (execl (buf, buf, exec_name, "-p", pid, "-n", debug_cmd,
                  "-x", rtcinit_fname, "-batch", "-tty=/dev/null", "-q",
                  "-brtc", "-leaks", (char *)0) == -1)
#else
         if (execl (buf, buf, "-p", pid, "-n", debug_cmd,
                  "-x", rtcinit_fname, "-batch", "-tty=/dev/null", "-q",
                  "-brtc", "-leaks", (char *)0) == -1)
#endif
           {
              perror ("execl failed. Cannot print RTC info"); /* bail out */
              DEBUG(printf ("execl failed buf = %s cpid=%d\n", buf, cpid);)
              unlink (rtcinit_fname); /* remove .rtcinit_pid file */
              libc_exit (1);
           }
        }
      } 
    else 
      {
        DEBUG(printf ("forked successfully cpid=%d\n", cpid);)

        wait_for_gdb_here (cpid);

        unlink (rtcinit_fname); /* remove .rtcinit_pid file */
      }

bailout:
    /* JAGaf70635 - return back to the the changed_path */
     if (chdir(changed_path) != 0)
     { 
        sprintf(buf,"In librtc, unable to chdir to %s\n", changed_path);
        perror(buf);
     }

    return;
}

/* dlhook callback function which handles batch RTC initialization and
   printing. For a full list of interceptible events see <dlfcn.h>
 */
static void 
librtc_callback(unsigned int type, struct dld_hook_param *arg)
{
    static int dl_load_complete = 0;
    static int dl_unload_complete = 0;

    switch(type) 
      {
      case DL_LOAD_POST_INIT:
        /* event is called after the initializers for all  loaded
           libraries have been called. Enable rtc when the first 
           post initializer event is recvd since 
        */

        DEBUG(printf("DL_LOAD_POST_INIT AAA %s=%d\n", arg->filename,file_included);)

        if (dl_load_complete)
          break;
        dl_load_complete = 1;

        if (!in_batch_mode())
          {
             DEBUG(printf("__pthreads_not_ready = 0\n");)
             __pthreads_not_ready = 0;
             return;
          }

        if (!init_config (arg->filename, true)) /* malloc must have failed */
          break; /* bail out; no RTC info */

        if (!process_config()) 
          break; /* malloc failed or config does not exist; bail out */

        file_included = is_file_included (arg->filename);
        /* JAGag27995 - Disable RTC so that even data collection doesn't happen
           if the given executable should not be traced.  Batch RTC is truly
           disabled, except that we deflect all calls that we intercept to libc's
           version of the same. */
        if (!file_included ){
              rtc_disabled = 1;
              DEBUG(printf("rtc_disabled = 1 for file %s\n", arg->filename);)
              /* Disable thread tracing too so that no gdb gets attached
                 to this executable. */
              temp_thread_trace_enabled = 0;
        }
        __pthreads_not_ready = 0;
        DEBUG(printf("In Batch mode; __pthreads_not_ready = 0\n");)

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
        /* Invoke gdb if thread checking has been enabled in the config. */
        if (temp_thread_trace_enabled)
          {
            rtc_disabled = 1;
            invoke_gdb (arg->filename);
          }
#endif

        break;

    case DL_UNLOAD_POST_FINI:

        DEBUG(printf("DL_UNLOAD_POST_FINI %s\n", arg->filename);)

        if (file_included && file_is_librtc (arg->filename)) 
          {
            if (dl_unload_complete)
              break;
            dl_unload_complete = 1;
            DEBUG(printf("DL_UNLOAD_POST_FINI file included %s\n", arg->filename);)

#ifdef HP_IA64
            // This callback for DL_UNLOAD_POST_FINI is enabled only
            // under batch mode.
            /* If we are doing selfrtc, invoke rtc_leak_analyze to do everything
              from inside librtc, without having to invoke gdb.
            */
            int doing_selfrtc = is_selfrtc();
            if (doing_selfrtc)
             {
               rtc_leak_analyze();
               resume_and_return ();
             }
            else
              print_batch_info (); 
#else
            print_batch_info (); 
#endif
          }
        break;
    }

    return;
}

/*
 ============================================================================
 Function to return is selfrtc to be invoked or not.

 Selfrtc is when librtc does not invoke gdb in batch mode. If RTC_NO_SELFRTC
 is set, then selfrtc is not used and the older method of invoking gdb is done.

 As of now, selfrtc is available on IPF, HP-UX, 11.23 and beyond.
 ============================================================================
*/
static int
is_selfrtc()
{
#ifdef HP_IA64
  struct utsname uts;
  int sys_ver = 0;

  if ((selfrtc == 0) || (selfrtc == 1))
    {
      DEBUG(printf("self rtc is already set to %d\n", selfrtc);)
      goto ret_val;
    }
  
  char *envstr;
  if ((envstr = getenv("RTC_NO_SELFRTC")) != NULL &&
                               strcmp(envstr, "1") == 0)
    {
      DEBUG(printf("RTC_NO_SELFRTC is set\n");)
      selfrtc = 0;
      goto ret_val;
    }

  if (!in_batch_mode())
    {
      DEBUG(printf("not in batch mode\n");)
      selfrtc = 0;
      goto ret_val;
    }

  uname (&uts);
  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
    {
      DEBUG(printf("uname: Couldn't resolve the release identifier of the " 
                   "Operating system.");)
      sys_ver = 0;
    }

  DEBUG(printf("system version (HP-UX) is = %d\n", sys_ver);)

  if (sys_ver >= 23)
    selfrtc = 1;
  else
    selfrtc = 0;
#else
  selfrtc = 0;
#endif

ret_val:
  DEBUG(printf("selfrtc = %d\n", selfrtc);)

  return selfrtc;
}

/*
 ============================================================================
 Functions for resolving symbol names during library unload, storing and
 retrieving them.

   * load_or_store_symbol_for_unloading_library
   * record_symbol_from_unload_lib

 IPF, HP-UX specific as of now.
 ============================================================================
*/
#ifdef HP_IA64
struct symbol_from_unload_lib *
load_or_store_symbol_for_unloading_library (
  uint64_t ip,
  uint64_t offset,
  int      index)
{
  long hash_value = 0;
  struct symbol_from_unload_lib *stack_frame_symbol = NULL;

  if (ip == 0 && offset == 0)
    return 0;

  /* hash key is obtained by ...............*/

  hash_value = (offset+index) % ST_TRACE_HASH_SLOTS_UNLOAD_LIB;
  stack_frame_symbol = symbol_from_unload_lib [hash_value];

  /* For collision check & handling.
     The ip value will never be 0 when we are here to insert a symbol
     information into the table. So collision check and handling should
     be done when ip != 0.
     The ip will be always 0 when we are here to fetch an entry from
     the table, and we should be called by backtrace() or other routines
     those are interested in the value returned from here, in such case
     there is no need enter the block below
  */
  if (ip != 0)
    {
      while (stack_frame_symbol != NULL)
        {
          if (stack_frame_symbol->unswizzled_addr == UNSWIZZLE_ADDR(ip))
            {
              DEBUG(printf ("\n Not recorded again with hash value "
                            "%#lx using offset %"PRId64" index %d \n %s, %s, "
                            "%d, %d and slots available = %d \n",
                            hash_value, offset, index,
                           (symbol_from_unload_lib [hash_value])->symbol_name,
                           (symbol_from_unload_lib [hash_value])->src_file_name,
                           (symbol_from_unload_lib [hash_value])->line_number,
                           (symbol_from_unload_lib [hash_value])->offset,
                            __stack_trace_cnt_4unload_lib);)
              DEBUG (printf ("\n **Warning a collision is detected in the"
                             " symbol_from_unload_lib hash table\n");)

              return stack_frame_symbol;
            }
          else
            stack_frame_symbol = stack_frame_symbol->next;
        }
    }
  else
    {
      if (symbol_from_unload_lib [hash_value])
        DEBUG(printf ("\n Returned with hash value %#lx using "
                      "offset %"PRId64" index %d \n %s, %s, %d, %d "
                      "and slots available = %d \n",
                      hash_value, offset, index,
                     (symbol_from_unload_lib [hash_value])->symbol_name,
                     (symbol_from_unload_lib [hash_value])->src_file_name,
                     (symbol_from_unload_lib [hash_value])->line_number,
                     (symbol_from_unload_lib [hash_value])->offset,
                      __stack_trace_cnt_4unload_lib);)
      else
        /* TBD: What to return in this case ? */
        DEBUG(printf("symbol_from_unload_lib [%#lx] is NULL\n", hash_value);)

      return stack_frame_symbol;
    }

  /* Check for the space allocated if not available allocate again */
  if (__stack_trace_cnt_4unload_lib == 0) /* keeps track of allocated mem */
    {
      /* points to the next available storage in the allocated memory */
      stack_trace_symbol_buffer_for_unload_lib
        = (struct symbol_from_unload_lib *)
                       libc_calloc (ST_TRACE_HASH_SLOTS_UNLOAD_LIB,
                           sizeof (struct symbol_from_unload_lib));
      if (stack_trace_symbol_buffer_for_unload_lib == NULL)
        {
           __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
           return 0;
        }
       __stack_trace_cnt_4unload_lib = ST_TRACE_HASH_SLOTS_UNLOAD_LIB;
    }

  /* point the stack_frame_symbol to the storage given by
     stack_trace_symbol_buffer_for_unload_lib and increment it to point next
     available storage in the allocated memory. decrement the
     __stack_trace_cnt_4unload_lib to keep track of available number of
     storage locations.
  */
  stack_frame_symbol = stack_trace_symbol_buffer_for_unload_lib++;
  __stack_trace_cnt_4unload_lib--;

  int ret = backtrace (NULL, 0, ip, index, &stack_frame_symbol);
  if (!ret)
    {
      DEBUG(printf("backtrace returned error %d\n", ret);)
    }
  else
    {
      stack_frame_symbol->unswizzled_addr = UNSWIZZLE_ADDR(ip);

      stack_frame_symbol->next = symbol_from_unload_lib [hash_value];
      symbol_from_unload_lib [hash_value] = stack_frame_symbol;

      DEBUG(printf ("\n Recorded with hash value %#lx using offset "
                    "%"PRId64" index %d \n %s, %s, %d, %d and "
                    "slots available = %d \n",
                    hash_value, offset, index,
                    (symbol_from_unload_lib [hash_value])->symbol_name
                      ? (symbol_from_unload_lib [hash_value])->symbol_name
                      : "NULL symbol name",
                    (symbol_from_unload_lib [hash_value])->src_file_name,
                    (symbol_from_unload_lib [hash_value])->line_number,
                    (symbol_from_unload_lib [hash_value])->offset,
                     __stack_trace_cnt_4unload_lib);)
    }
  return 0; /* return 0 indicating succesful insert into the hash table */
}

/* For a library getting unloaded, record the symbols for
   the collected stack trace PCs beloging to this library.
*/
void record_symbol_from_unload_lib (void * handle)
{
  struct rtc_chunk_info *c_i = 0;
  struct shl_descriptor shl_desc;
  uint64_t addr, addr_offset;
  int idx = 0;

  if (shl_gethandle_r (handle, &shl_desc) != -1);
  for (idx = 0; idx < shlib_info_count; idx++)
    {
      if (!strcmp (shl_desc.filename, (char *)shlib_info[idx].library_name))
      break;
    }

  /* Loop thru all chunks and store symbol information corresponding to the
     pc address which falls in text range of the library being unloaded
  */
  for (int i = 0; i < total_pages; i++)
    {
      for (c_i = chunks_in_page[i]; c_i; c_i = c_i->next)
        {
          // QXCR1000985554: pc_tuple may be NULL if size < min leaks
          if (!c_i->pc_tuple)
            continue;

          for (int j = 0;
               j < __rtc_frame_count && c_i->pc_tuple[j].pc != NULL;
               j++)
            {
              if (c_i->pc_tuple[j].library_index_of_pc != ABSOLUTE_ADDRESS_INDEX)
                continue;
              addr = SWIZZLE_PTR((uint64_t)(c_i->pc_tuple[j].pc));
              if (addr >= SWIZZLE_PTR(shl_desc.tstart) &&
                  addr <= SWIZZLE_PTR(shl_desc.tend))
                {
                  addr_offset =  addr - SWIZZLE_PTR(shl_desc.tstart);
                  if (load_or_store_symbol_for_unloading_library (addr,
                          addr_offset, idx) != 0)
                    {
                      DEBUG (printf ("\nERROR: Failed to insert an entry into the"
                             " stack_trace_symbol hash table\n");)
                    }
                }
             }
        }
    }
}
#endif // HP_IA64

#ifdef HP_IA64
/*
 =======================================================================
 Reporting routine for batch/+check runs on HP-UX, IPF.

 The same reporting happens on gdbrtc (for interactive mem. debugging, or
 batch mode on other platforms), but there are quite a few differences in
 the data collection, printing function etc.

 TBD: Merge the gdb and infrtc side of reporting to one module in future.

 - Common reporting function
   * produce_memory_report

 - Heap reporting
   * compare_blocks
   * compare_blocks_by_size
   * report_this_block
   * print_blocks
   * collect_blocks

 - Leak reporting
   * compare_leaks
   * report_this_leak
   * print_leaks
   * add_new_leak

 =======================================================================
*/

/* Return 1 if the p1's pointer_to_stack_trace is greater than
   to that of p2, -1 other way round. 0 if equal. */
static int
compare_blocks (const void *p1, const void *p2)
{
  struct rtc_memory_info * b1, *b2;

  b1 = (struct rtc_memory_info *) p1;
  b2 = (struct rtc_memory_info *) p2;

  if (b1->pc_tuple < b2->pc_tuple)
    return -1;
  else if (b1->pc_tuple > b2->pc_tuple)
    return 1;

  return 0;
}

/* Returns > 0 if p2's size is greater than that of p1, 0 if equal, else
   returns < 0. Used to sort heap blocks by size. */ 
static int
compare_blocks_by_size (const void *p1, const void *p2)
{
  struct rtc_memory_info * b1, *b2;

  b1 = (struct rtc_memory_info *) p1;
  b2 = (struct rtc_memory_info *) p2;

  /* QXCR1000883689 : Heap profile lists biggest allocation last.
     The value which was getting returned previously, was causing
     an integer over-flow. */
  if (b1->size < b2->size)
    return 1;
  else if (b1->size > b2->size)
    return -1;

  return 0;

}

/* Print out the information about a heap block (block_no). Write it
   either to stdout or to fp if provided.
*/
static void
report_this_block (int block_no, FILE * fp, int type)
{
  int i;
  char local_buf[200];
  struct rtc_memory_info * block;

  if (block_no < 0 || block_no >= block_count)
    perror ("Not a valid block number.");

  block = block_info + block_no;

  if (type == BOUNDS_INFO && !block->corrupted)
    perror ("Block number not corrupted.");

  if (block->count == 1)
    {
      sprintf (local_buf, "%lld bytes at 0x%p "
               "(%2.2f%% of all bytes allocated)\n",
               block->size, block->address,
               ((((float) block->size) / total_bytes) * 100));
      if (fp)
        fprintf (fp, "%s", local_buf);
      else
        printf ("%s", local_buf);
    }
  else
    {
      sprintf (local_buf, "%lld bytes in %d blocks "
               "(%2.2f%% of all bytes allocated)\n",
               block->size, block->count,
               ((((float) block->size) / total_bytes) * 100));

      if (fp)
        fprintf (fp, "%s", local_buf);
      else
        printf ("%s", local_buf);

      sprintf (local_buf, "These range in size from "
                          "%lld to %lld bytes %s\n",
                           block->min, block->max,
                           block->pc_tuple ? "and are allocated" : "");


      if (fp)
        fprintf (fp, "%s", local_buf);
      else
        printf ("%s", local_buf);
    }

  if (!block->pc_tuple)
    return;

  /* Print the frame stack. */
  print_stack_trace_from_pclist(fp, block->pc_tuple, __rtc_frame_count);
  return;
}

/* Print out a report on all the heap blocks provided by block_info.
   If fp is provided print a detailed report. If not write to
   stdout a brief desc of all the blocks.

   TBD: Some of these code are common with the gdbrtc side, make a common
   function for both. There are quite a few differences as well between the
   infrtc and gdbrtc part though about preparing the leak information.
*/
static void
print_blocks (struct rtc_memory_info *block_info, FILE * fp, int type)
{
  int i = 0;
  char local_buf[4096];
  long long heap_size;

  heap_size = (heap_end_value - real_heap_base);
  /* 
   * JAGaf87040 - this routine used to take care of info heap (HEAP_INFO)
   * or info corruption.  We now have one more type for info heap high-mem
   * (HIGH_MEM_INFO) 
   */
  if (fp)
    {
      fprintf (fp, "Actual Heap Usage:\n");
      fprintf (fp, "\tHeap Start\t=\t0x%p\n", real_heap_base);
      fprintf (fp, "\tHeap End\t=\t0x%p\n", heap_end_value);
      fprintf (fp, "\tHeap Size\t=\t%lld bytes\n\n", heap_size);
      fprintf (fp, "Outstanding Allocations:\n");
      fprintf (fp, "\t%lld bytes allocated in %ld blocks\n\n",
                      total_bytes, total_blocks);
    }
  else
    {
      printf ("Actual Heap Usage:\n");
      printf ("\tHeap Start\t=\t0x%p\n", real_heap_base);
      printf ("\tHeap End\t=\t0x%p\n", heap_end_value);
      printf ("\tHeap Size\t=\t%lld bytes\n\n", heap_size);
      printf ("Outstanding Allocations:\n");
      printf ("\t%lld bytes allocated in %ld blocks\n\n",
                             total_bytes, total_blocks);
    }

  if (fp)
    if (type == BOUNDS_INFO)
     fprintf (fp, "No.   Total bytes     Blocks     Corruption        Address         Function\n");
    else
     fprintf (fp, "No.   Total bytes     Blocks     Address        Function\n");
  else 
    if (type == BOUNDS_INFO)
     printf("No.   Total bytes     Blocks     Corruption      Address        Function\n");
    else
     printf("No.   Total bytes     Blocks     Address        Function\n");

  struct symbol_from_unload_lib *stack_frame_symbol
    = libc_malloc(sizeof(struct symbol_from_unload_lib));

  for (i=0; i < block_count; i++, block_info++)
    {
      char * function = "???";
      char * parenth;
      char corrupted[15]="None";
      int  backtrace_ret = 0;

      if (type == BOUNDS_INFO && block_info->corrupted == 0)
      {
        continue;
      }

      /* All blocks below min_leak_size will not have stack frames
         captured and we need to handle this. As even in this case 
         pc_tuple will not be '0' and hence we need to explicitly
         check for 'pc' not being '0'
       */
      if (block_info->pc_tuple && block_info->pc_tuple[0].pc != 0)
        {
          stack_frame_symbol->symbol_name = NULL;
          stack_frame_symbol->src_file_name = NULL;

          backtrace_ret = backtrace (NULL, 0,
                     SWIZZLE_PTR((uint64_t)(block_info->pc_tuple[0].pc)),
                     block_info->pc_tuple[0].library_index_of_pc,
                     &stack_frame_symbol);
          if (backtrace_ret)
            function = stack_frame_symbol->symbol_name;

          /* QXCR1000989650: If the symbol_name is NULL, print "????" */
          if (!function)
            function = "????";
        }

      parenth = ( strchr(function,'(') == NULL &&
                  strchr(function,')') == NULL ) ? "()" : "  ";

      if(block_info->corrupted == RTC_BAD_HEADER)
         strcpy(corrupted, "Beginning    ");
      else if (block_info->corrupted == RTC_BAD_FOOTER)
         strcpy(corrupted, "End of block ");
      else if (block_info->corrupted == RTC_FREED_BLOCK_CORRUPTED)
         strcpy(corrupted, "Inside block ");

      if (fp)
      {
        if (type == BOUNDS_INFO)
          fprintf (fp, "%-3d      %-10lld     %-6d  %s     0x%p     %s%s\n", i,
                                             block_info->size,
                                             block_info->count,
                                             corrupted,
                                             block_info->address,
                                             function, parenth);
        else
          fprintf (fp, "%-3d      %-10lld     %-6d  0x%p   %s%s\n", i, 
                                             block_info->size,
                                             block_info->count,
                                             block_info->address,
                                             function, parenth);
      }
      else 
        {
        if (type == BOUNDS_INFO)
          sprintf (local_buf, "%-3d      %-10lld     %-6d  0x%p   %s   %s%s\n",
                                   i, block_info->size,
                                   block_info->count,
                                   corrupted,
                                   block_info->address,
                                   function, parenth);
        else
          sprintf (local_buf, "%-3d      %-10lld     %-6d  0x%p   %s%s\n",
                                   i, block_info->size,
                                   block_info->count,
                                   block_info->address,
                                   function, parenth);
         printf ("%s", local_buf);
        }

#if 0
        if (stack_frame_symbol->symbol_name)
          libc_free(stack_frame_symbol->symbol_name);
#endif

        if (backtrace_ret)
          {
            stack_frame_symbol->symbol_name = NULL;

            if (stack_frame_symbol->src_file_name)
              libc_free(stack_frame_symbol->src_file_name);

            stack_frame_symbol->src_file_name = NULL;
          }
    }

    libc_free(stack_frame_symbol);

    if (fp)
      {
        fprintf (fp, "\n\n-------------------------------------------------------------------------\n");
        fprintf (fp, "\n"
"                              Detailed Report\n");
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
        for (i=0; i < block_count; i++)
          {
            report_this_block (i, fp, type);
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
          }
       }
}

/* Collect the number of heap allocations together */
static int
collect_blocks(struct rtc_memory_info** data_ptr)
{
  struct rtc_memory_info* data = NULL;
  long data_capacity = 0;
  struct rtc_chunk_info * c_i = 0;
  long long size;
  int count = 0, retval = 0, corrupted = 0;
  int i;

  for (i=0; i < total_pages; i++)
    {
      c_i = chunks_in_page[i];
      while (c_i)
        {
          if(   (__rtc_bounds_or_heap==HEAP_INFO)
             && c_i->freed_block
             && !display_freed_blocks)
            {
              c_i = c_i->next;
              continue;
            }
          corrupted = 0;
          size = c_i->end - c_i->base;

          if (c_i->padded_block)
            {
              /* Suresh, Nov 07: So if a freed block has both boundary(header or 
                 footer) and in-block corruption, we report the in-block
                 corruption, as we would already reported the boundary error
                 when the block was freed.. 

                 On the other hand if there is only boundary corruption and it
                 happened after the block was freed(using a dangling pointer)
                 it would be reported as a boundary corruption 
              */

              /* In-block corruption for only a freed block */
              if ((c_i->freed_block) && __rtc_retain_freed_blocks)
                corrupted = check_freed_block((unsigned int*)c_i->base,
                                              (unsigned int)size); 

	      /* Boundary corruption check for both freed and live blocks, only
                if the previous check found nothing */
              if(!corrupted)
                corrupted = boundary_checks((unsigned int*)c_i->base,
                                            (unsigned int) size);
            }
          else if ((c_i->freed_block) && __rtc_retain_freed_blocks)
            /* In-block corruption for a freed block, even if the block is
               not padded*/
            corrupted = check_freed_block((unsigned int*)c_i->base,
                                          (unsigned int)size); 
          else
            corrupted = 0;

          /* If we are generating bounds overrun report and the current
             chunk we are looking at is not corrupted, ignore it.
           */
          if (__rtc_bounds_or_heap == BOUNDS_INFO)
            {
              if (corrupted == 0 || c_i->preinit_mmap == 1)
                {
                  c_i = c_i->next;
                  continue;
                }
            }
          else
            {
              /* JAGaf24984: skips __rtc_initialize from heap 
                 info list
              */
              /* JAGag23917: we should skip any NON_HEAP_BLOCK. 
                  As these aren`t part of heap, we do not show them on
                 'info heap'. 
                  We need to record these in our chunk info list to scan
                  for leaks.
              */ 
              if (c_i->heap_block != HEAP_BLOCK)
                {
                  c_i = c_i->next;
                  continue;
                }
            }

          /* Write the block info */
          if (count == data_capacity)
            {
              data_capacity += 4096;
              data = libc_realloc (data, data_capacity *
                                   sizeof (struct rtc_memory_info));
            }

          data[count].address = c_i->base;
          data[count].size = size;
          data[count].count = 1;
          data[count].corrupted = corrupted;
          data[count].pc_tuple = c_i->pc_tuple;
          data[count].min = data[count].max = size;
          data[count].c_i = c_i;

          count++;
          total_bytes += size;

          DEBUG(fprintf(stderr, "[%d] base: %#p, size: %lld, corrupted: %d, "
                        "scanned = %d, dptr = %#p\n",
                        count, c_i->base, size, corrupted,
                        c_i->scanned, c_i->dptr));

          /* this chunk has been reported once, do not report again */
          // c_i->new = 0;
          c_i = c_i->next;
        }
#if (!defined(__LP64__) && defined(HP_IA64))
      /* For performance issue on string check for IPF 32 bits.
         Skip all the pages for which we had copied the
         chunks_in_page pointer for that big chunk. */
      if (__rtc_check_string && (chunks_in_page[i] == chunks_in_page[i+1]) &&
          !(__rtc_retain_freed_blocks))
        {
          c_i = chunks_in_page[i];
          if (c_i)
            i = PAGE_FROM_POINTER (c_i->end) - 1;
        }
#endif
    }

  *data_ptr = data;

  retval = count;

  return retval;
}

/* Return true if the size of p2 is greater than size of p1.
   Used by qsort. */
static int
compare_leaks (const void *p1, const void *p2)
{
  struct rtc_memory_info * l1, *l2;
  
  l1 = (struct rtc_memory_info *) p1;
  l2 = (struct rtc_memory_info *) p2;

  return (int) (l2->size - l1->size);
}

/* Print the information about a leak (leak_no). Write it either to
   stdout or to fp if provided. */
static void
report_this_leak (struct rtc_memory_info * leak_head, int leak_no, FILE * fp)
{
  int i;
  struct rtc_memory_info * leak;
  char local_buf[4096];

  if (leak_no < 0 || leak_no >= leak_count)
    perror ("Not a valid leak number.");

  leak = leak_head + leak_no;

  if (leak->count == 1)
    {
      /* Only 1 block. Print the address then. */
      sprintf (local_buf, "%lld bytes leaked at 0x%p "
               "(%2.2f%% of all bytes leaked)\n",
               leak->size,
               leak->address,
               ((((float) leak->size) / total_leak_bytes) * 100));

      if (fp)
        fprintf (fp, "%s", local_buf);
      else
        printf ("%s", local_buf);
    }
  else
    {
      sprintf (local_buf, "%lld bytes leaked in %d blocks "
               "(%2.2f%% of all bytes leaked)\n",
               leak->size, leak->count,
               ((((float) leak->size) / total_leak_bytes) * 100));
      if (fp)
        fprintf (fp, "%s", local_buf);
      else
        printf ("%s", local_buf);

      sprintf (local_buf, "These range in size from "
                          "%lld to %lld bytes %s\n",
                                leak->min, leak->max,
                                leak->pc_tuple ? "and are allocated" : "");
      if (fp)
        fprintf (fp, "%s", local_buf);
      else
        printf ("%s", local_buf);
    }

  if (!leak->pc_tuple)
    return;

    /* Print the frame stack. */
  print_stack_trace_from_pclist(fp, leak->pc_tuple, __rtc_frame_count);
}


/*
   Print leak report.

   TBD: Some of these code are common with the gdbrtc side, make a common
   function for both. There are quite a few differences as well between the
   infrtc and gdbrtc part though about preparing the leak information.
*/
static void
print_leaks (struct rtc_memory_info* leak_info, FILE* fp, int count)
{
  int i = 0;
  char local_buf[4096];
  int actual_records_of_same_type = 0;
  struct rtc_memory_info* leak_info_head = NULL;
  if (!count)
    return;
  /* save the head of the leaks list to pass it to print_this_leak () */
  leak_info_head = leak_info;

  sprintf (local_buf, "\n%lld bytes leaked in %ld blocks\n\n",
           total_leak_bytes, total_leak_blocks);
  if (!fp)
    printf ("%s", local_buf);
  else
    fprintf (fp, "%s", local_buf);

  if (!fp)
    printf ("No.   Total bytes     Blocks     Address     Function\n");
  else
    fprintf (fp, "No.   Total bytes     Blocks     Address     Function\n");

  struct symbol_from_unload_lib *stack_frame_symbol
    = libc_malloc(sizeof(struct symbol_from_unload_lib));

  for (i=0; i < count; i++, leak_info++)
    {
      char * function = "???";
      char * parenth = "  ";
      int backtrace_ret = 0;

      if (leak_info->pc_tuple)
        {
          backtrace_ret = backtrace (NULL, 0,
                     SWIZZLE_PTR((uint64_t)(leak_info->pc_tuple[0].pc)),
                     leak_info->pc_tuple[0].library_index_of_pc,
                     &stack_frame_symbol);
          if (backtrace_ret)
            function = stack_frame_symbol->symbol_name;

          /* QXCR1000989650: If the symbol_name is NULL, print "????" */
          if (!function)
            function = "????";
        }

        if (strncmp(function,"???",3))
          parenth = ( strchr(function,'(') == NULL &&
                      strchr(function,')') == NULL ) ? "()" : "  ";

        sprintf (local_buf, "%-3d      %-10lld     %-6d  0x%p   %s%s\n",
                 actual_records_of_same_type, leak_info->size,
                 leak_info->count,
                 leak_info->address,
                 function, parenth);
        /* We increment this records of same type so that we print a
           running sequential number for each record in the output
         */
        actual_records_of_same_type++;

        if (!fp)
          printf ("%s", local_buf);
        else
          fprintf (fp, "%s", local_buf);
     } /* end of for */

  /* QXCR1001069334, Display this note in the report to help user understand
     it better. */
  if (fp)
    fprintf (fp, "\n\n-------------------------------------------------------"
             "------------------\n");
  sprintf (local_buf, "\nNOTE:\n Function name \"???\" implies that user has set"
           " min-leak-size; function names\nare not fetched for leaks of smaller"
           " sizes for better performance."); 
  if (!fp)
    printf ("%s", local_buf);
  else
    fprintf (fp, "%s", local_buf);


  libc_free(stack_frame_symbol);

  if (fp)
    {
        fprintf (fp, "\n\n-------------------------------------------------------------------------\n");
        fprintf (fp, "\n                              Detailed Report\n");
        fprintf (fp, "\n-------------------------------------------------------------------------\n");

        for (i=0; i < count; i++)
          {
            /* QXCR1001069334, Stop displaying unnecessary messages in the report. */
            if (!(leak_info_head + i)->pc_tuple)
              break;
            report_this_leak (leak_info_head, i, fp);
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
          }
    }

  if (fp)
    fflush(fp);

  return;

}

/*
 Add a leak to leak_info list.
*/
static void
add_new_leak(struct rtc_chunk_info* c_i)
{
  int i;
  long long this_leak_size;

  this_leak_size = c_i->end - c_i->base;
  total_leak_bytes += this_leak_size;
  total_leak_blocks++;

  /* Have we met Mr. Leaky already ? If this is Mr. Anonymous (i.e.,
     no associated stack trace,) let us not lump it with such others.
  */
  if (c_i->pc_tuple)
    {
      for (i=0; i < leak_count; i++)
        {
          if (leak_info[i].pc_tuple == c_i->pc_tuple)
            {
              leak_info[i].count++;
              leak_info[i].size += this_leak_size;

              if (leak_info[i].min > this_leak_size)
                leak_info[i].min = this_leak_size;
              if (leak_info[i].max < this_leak_size)
                leak_info[i].max = this_leak_size;

              return;
            }
        }
    }


  i = leak_count;
  /* expand leak info if needed */

  if (i == leak_info_capacity)
    {
      leak_info_capacity += 4096;
      leak_info = libc_realloc (leak_info, leak_info_capacity *
                                  sizeof (struct rtc_memory_info));
    }

  leak_info[i].address = c_i->base;
  leak_info[i].size = this_leak_size;
  leak_info[i].count = 1;
  leak_info[i].pc_tuple = c_i->pc_tuple;
  leak_info[i].min = leak_info[i].max = this_leak_size;
  leak_info[i].c_i = c_i;

  leak_count++;
}

static rtc_chunk_info *
collect_preprocess_leaks (int need_sorting)
{
  /* Get the leak information */
  rtc_chunk_info * leaks_c_i, *return_c_i;
  leaks_c_i = return_c_i = __rtc_leaks_info();

  if (!is_it_safe ((int)(long) return_c_i))
    return return_c_i;

  while (leaks_c_i)
   {
     add_new_leak(leaks_c_i);
     leaks_c_i = leaks_c_i->next_leak;
   }

  /* Leaks sorting using quick sort method */
  if (need_sorting)
   if (total_leak_blocks > 1)
    qsort (leak_info, leak_count, sizeof (struct rtc_memory_info),
          compare_leaks);

  return return_c_i;
}

static void
collect_preprocess_heap ()
{
  struct rtc_memory_info* data;
  int number_blocks = 0;
  int i, j;

  total_blocks = number_blocks = collect_blocks(&data);
  qsort (data, number_blocks, sizeof (struct rtc_memory_info),
         compare_blocks);

  /* At this point the blocks are sorted on the 'pointer to
     stack trace' field. So all blocks originating in the
     same call stack are adjacent to each other ... */
  int unique_allocations = 0;
  struct pc_tuple* last_allocation_address = NULL;

  for (i=0; i < number_blocks; i++)
    {
      if (data[i].pc_tuple != last_allocation_address)
        {
          unique_allocations++;
          last_allocation_address = data[i].pc_tuple;
        }
    }
  DEBUG(printf("unique_allocations = %d\n", unique_allocations);)

  block_info = libc_malloc (unique_allocations *
                            sizeof (struct rtc_memory_info));

  /* Collate the blocks now so that allocations from the same
     call stack show up together.
  */
  unique_allocations = 0;
  last_allocation_address = NULL;

  for (i=0, j=-1; i < number_blocks; i++)
     {
       /* No block collating for info corruption report.. */
       if (data[i].pc_tuple != last_allocation_address)
         {
           block_info[++j] = data[i];
           block_info[j].min = block_info[j].max = data[i].size;
           last_allocation_address = data[i].pc_tuple;
           unique_allocations++;
         }
       else
         {
           block_info[j].count++;
           block_info[j].size += data[i].size;

           if (block_info[j].min > data[i].size)
           block_info[j].min = data[i].size;
           if (block_info[j].max < data[i].size)
           block_info[j].max = data[i].size;

         }

       /* JAGaf87040 - transfer the count from i data to j block info */
       // TBD
       block_info[j].high_mem_cnt = data[i].high_mem_cnt;


     }

  libc_free (data);
  block_count = unique_allocations;

  /* Now sort based on the size ... */
  qsort (block_info, block_count, sizeof (struct rtc_memory_info),
         compare_blocks_by_size);

  for (i = block_count - 1;
       i >= 0 && block_info[i].size < __rtc_min_heap_size ;
       i--);

  block_count = i+1;

}

/* For selfrtc, this function initiates the mark-sweep algorithm
   and the reporting of the heap/leak data.
*/
static void
produce_memory_report(pid_t mypid)
{
  char  pid[20];
  char  buf[PATH_MAX + 100];
  char *tp;
  FILE *fp = NULL;

  sprintf (pid, "%d", mypid);

  tp = strrchr (rtcenv[RTC_FILE], '/');
  if (tp == NULL || *tp != '/')
    tp = rtcenv[RTC_FILE];
  else
    tp++;

  DEBUG(printf("__rtc_check_leaks=%d __rtc_check_heap=%d __rtc_check_openfd=%d\n",
                __rtc_check_leaks, __rtc_check_heap, __rtc_check_openfd);)

  if (__rtc_check_leaks)
    {
      struct rtc_chunk_info* new_leaks_c_i;
      /* Create a new list for leaks */
      /* Get the leak information */
      new_leaks_c_i = collect_preprocess_leaks (true);
      if (!is_it_safe ((int)(long) new_leaks_c_i))
        return;
      if (total_leak_blocks > 0)
        {
          /* Report the leaks */ 
#if HP_VACCINE_DBG_MALLOC
          if (__rtc_leak_logfile == NULL) {
            fp = create_output_file (tp, pid, "leaks", buf, 
                                     rtcenv[RTC_OUTPUT_DIR], 1);
            if (fp)
              fprintf(stderr, "warning: Memory leak info is written to "
                      "\"%s\".\n", buf);
          } else {
            fp = open_log_file(__rtc_leak_logfile);
          }
#else
          fp = create_output_file (tp, pid, "leaks", buf, 
                                   rtcenv[RTC_OUTPUT_DIR], 1);
          if (fp)
            fprintf(stderr, "warning: Memory leak info is written to "
                    "\"%s\".\n", buf);
#endif  /* HP_VACCINE_DBG_MALLOC */     

          print_leaks(leak_info, fp, leak_count);
        }
    }

  if (__rtc_check_heap) 
    {
      collect_preprocess_heap ();
#if HP_VACCINE_DBG_MALLOC
      if (__rtc_heap_logfile == NULL)
        {
          fp = create_output_file (tp, pid, "heap", buf, 
                                   rtcenv[RTC_OUTPUT_DIR], 1);
          if (fp)
            fprintf(stderr, "Memory heap info is written to "
                    "\"%s\".\n", buf);
        }
      else
        {
          fp = open_log_file(__rtc_heap_logfile);
        }
#else
      fp = create_output_file (tp, pid, "heap", buf, 
                                 rtcenv[RTC_OUTPUT_DIR], 1);
      if (fp)
        fprintf(stderr, "Memory heap info is written to "
                "\"%s\".\n", buf);

#endif  /* HP_VACCINE_DBG_MALLOC */ 

      print_blocks(block_info, fp, HEAP_INFO);
    }
  if (__rtc_check_openfd)
    {
      fp = create_output_file (tp, pid, "openfd", buf,
                               rtcenv[RTC_OUTPUT_DIR], 1);
      if (fp)
        fprintf(stderr, "Open file descriptor info is written to "
                "\"%s\".\n", buf);
      print_openfds(fp);
    }
}
#endif // HP_IA64

#ifdef DLD_RTC
#pragma init "rtc_initialize"
#pragma fini "__rtc_fini"
void __rtc_fini ()
{
  obtain_backdoor_entry();
  if (file_included && rtc_started) 
     if (in_batch_mode())
       {
         if (__ismt)
           __thread_once (&print_once, print_batch_info);
         else
           print_batch_info();
       }

}
#endif // DLD_RTC

/* RTC APIs procedures */
#ifdef HP_IA64

char * curr_time ()
{
  int status;
  time_t timer;
  if ((status = time (&timer)) == -1)
  {
    fprintf (stderr, "Failed to get current time\n");
    return NULL;
  }
  return (ctime (&timer));
}


static void
rtc_api_add_leak (struct rtc_chunk_info* c_i)
{
  int i;
  long long this_leak_size;
  total_new_leaks_blocks++;
  this_leak_size = c_i->end - c_i->base;
  total_new_leak_bytes += this_leak_size;
  /* Have we met Mr. Leaky already ? If this is Mr. Anonymous (i.e.,
     no associated stack trace,) let us not lump it with such others.
  */
  if (c_i->pc_tuple)
    {
      for (i=0; i < new_leak_count; i++)
        {
          if (new_leak_info[i].pc_tuple == c_i->pc_tuple)
            {
              new_leak_info[i].count++;
              new_leak_info[i].size += this_leak_size;

              if (new_leak_info[i].min > this_leak_size)
                new_leak_info[i].min = this_leak_size;
            if (new_leak_info[i].max < this_leak_size)
                new_leak_info[i].max = this_leak_size;

              return;
            }
        }
    }
  i = new_leak_count;
  /* expand new_leak info if needed */

  if (i == new_leak_info_capacity)
    {
      new_leak_info_capacity += 1024;
      new_leak_info = libc_realloc (new_leak_info, new_leak_info_capacity *
                                  sizeof (struct rtc_memory_info));
    }
  new_leak_info[i].address = c_i->base;
  new_leak_info[i].size = this_leak_size;
  new_leak_info[i].count = 1;
  new_leak_info[i].pc_tuple = c_i->pc_tuple;
  new_leak_info[i].min = new_leak_info[i].max = this_leak_size;
  new_leak_info[i].c_i = c_i;

  new_leak_count++;
}

FILE * set_output_file ()
{
  if (rtcapi_output_file_ptr && rtcapi_output_file_name)
   {
     DEBUG(printf ("\nThe RTC report is written to %s", rtcapi_output_file_name);)
     return rtcapi_output_file_ptr;
   }
  return (rtcapi_output_file_ptr = stdout);
}

int rtc_logfile (char *file_name, int set_unset)
{
  if (!rtc_started && !rtc_disabled)
   {
     rtc_initialize ();
     fprintf (stderr, "\nRTC is initialized\n");
   }
  RTC_API_ENTRY;
  int retval = __rtc_logfile_api (file_name, set_unset);
  RTC_API_EXIT;
  return retval;
}

int __rtc_logfile_api (char *file_name, int set_unset)
{
  int is_not_same_file = 1;
  if (rtcapi_output_file_name && file_name)
   {
     DEBUG(printf ("set/unset, file names =%d %s %s", set_unset,
             rtcapi_output_file_name, file_name);)
     is_not_same_file = strcmp (rtcapi_output_file_name, file_name);

     if (is_not_same_file == 0 && set_unset == 1)
      {
        return 0;
      }
     else if (is_not_same_file != 0 && set_unset == 0)
      {
        fprintf (stderr, "\nThe log-file name requested for unset is wrong.\n"
                 "Current log-file name is %s\n", rtcapi_output_file_name);
        return -1;
      }
   }

  if (rtcapi_output_file_name && set_unset)
   {
     fprintf (stderr, "\nThe RTC output file is already set to \"%s\"\n"
               ,rtcapi_output_file_name);
     return 0;
   }

  if (file_name && set_unset && rtcapi_output_file_name == NULL)
   {
     rtcapi_output_file_name = strdup (file_name);
     DEBUG(printf ("Trying output file=%s\n", rtcapi_output_file_name););

     //unlink (rtcapi_output_file_name);
     int fd = open (rtcapi_output_file_name, O_RDWR | O_CREAT | O_APPEND, 0777);
     if (fd != -1 && (rtcapi_output_file_ptr = fdopen (fd, "w")) != NULL)
      {
        rtcapi_logfile_keep_open = 1;
        fprintf (stderr, "\nThe RTC output file is set to %s\n",
                 rtcapi_output_file_name);
        return 0;
      }
     else
      {
        perror ("Failed to open the logfile for writing");
        fprintf (stderr, "\nERROR: The RTC output file is not set to %s\n",
                 rtcapi_output_file_name);
        libc_free (rtcapi_output_file_name);
        rtcapi_output_file_name = NULL;
        rtcapi_output_file_ptr = NULL;
        return -1;
      }
   }
  else if (file_name && !set_unset && rtcapi_output_file_name != NULL &&
           rtcapi_output_file_ptr != NULL)
   {
     if (rtcapi_output_file_name && rtcapi_output_file_ptr)
      {
        if (rtcapi_logfile_keep_open)
         {
           fclose (rtcapi_output_file_ptr);
           libc_free (rtcapi_output_file_name);
       //    unlink (rtcapi_output_file_name);
           rtcapi_output_file_name = NULL;
           rtcapi_output_file_ptr = NULL;
           return 0;
         }
      }
   }
   return -1;
}

/* Called by application.
   Takes new leaks or all leaks?
   Reports depending on the user request.
   Returns nuber of leaks found
*/
int rtc_leaks (int new_leaks)
{
  if (!rtc_started)
   return -1;

  if (!__rtc_check_leaks)
   {
     fprintf (stderr, "\nRTC leaks detection is disabled\n");
     return -1;
   }

  RTC_API_ENTRY;
  if (rtcapi_output_file_ptr == NULL)
   set_output_file ();
  int retval = __rtc_leaks_api (new_leaks);
  RTC_API_EXIT;
  return retval;
}

int __rtc_leaks_api (int new_leaks)
{
  if (new_leaks)
   {
     struct rtc_chunk_info* new_leaks_c_i;
     new_leak_count = 0;
     total_new_leaks_blocks = 0;
     total_new_leak_bytes = 0;

     rtc_leak_analyze ();
     new_leaks_c_i = collect_preprocess_leaks (false);
     resume_and_return ();
     /* skip_bookkeeping need to be set to 1 because resume_and_return()
        resets it */
     skip_bookkeeping = 1;
     if (!is_it_safe ((int)(long) new_leaks_c_i))
      {
        return -1;
      }

     while (new_leaks_c_i)
      {
        rtc_api_add_leak (new_leaks_c_i);
        new_leaks_c_i = new_leaks_c_i->next_leak;
      }

     if (new_leak_count > 1)
      qsort (new_leak_info, new_leak_count, sizeof (struct rtc_memory_info),
             compare_leaks);

     if (new_leak_count > 0)
      {
        /* Should not RTC report only newly leaked bytes and
           newly leaked block numbers? To report only new leaks information */
        long long temp_total_leak_bytes = total_leak_bytes;
        long long temp_total_leak_blocks = total_leak_blocks;

        total_leak_bytes = total_new_leak_bytes;
        total_leak_blocks = total_new_leaks_blocks;

        fprintf (rtcapi_output_file_ptr, "\n****New Leaks report**** on %s\n",
                 REPORT_TIME);
        print_leaks(new_leak_info, rtcapi_output_file_ptr, new_leak_count);

        /* Restore the original numbers.. */
        total_leak_bytes = temp_total_leak_bytes;
        total_leak_blocks = temp_total_leak_blocks;
      }
     return total_new_leaks_blocks;
   }
  else
   {
     /* Collect and preprocess the new leaks and add them to the
        info_leak list */
     struct rtc_chunk_info* all_leaks_c_i;

     rtc_leak_analyze ();
     all_leaks_c_i = collect_preprocess_leaks (true);
     resume_and_return ();

     /* skip_bookkeeping need to be set to 1 because resume_and_return()
        unsets it */
     skip_bookkeeping = 1;
     if (!is_it_safe ((int)(long) all_leaks_c_i))
      {
        skip_bookkeeping = 0;
        return -1;
      }
     if (total_leak_blocks > 0)
      {
        fprintf (rtcapi_output_file_ptr, "\n****All Leaks report**** on %s\n",
                 REPORT_TIME);
        print_leaks(leak_info, rtcapi_output_file_ptr, leak_count);
      }
     return total_leak_blocks;
   }
}

/* Takes nothing but reports heap information */
int rtc_heap ()
{
  if (!rtc_started)
   return -1;
  if (!__rtc_check_heap)
   {
     fprintf (stderr, "\nRTC heap detection is disabled\n");
     return -1;
   }

  RTC_API_ENTRY;
  if (rtcapi_output_file_ptr == NULL)
   set_output_file ();
  int total_heap_blocks = 0;
  /* Toggle the switch to collect all heap blocks including corrupted one */
  __rtc_bounds_or_heap = HEAP_INFO;

  /* Collect and preprocess the new leaks and add them to the
     info_leak list */
  fprintf (rtcapi_output_file_ptr, "\n****Heap report**** on %s\n", REPORT_TIME);
  total_heap_blocks = __rtc_heap_corruption_common (HEAP_INFO);
  RTC_API_EXIT;
  return total_heap_blocks;
}

/* helps both heap and heap corriuptio APIsi.*/
/* Returns total heap/corrupted blocks */
static int
__rtc_heap_corruption_common (int type)
{
  rtc_leak_analyze ();
  collect_preprocess_heap ();
  resume_and_return ();

  /* skip_bookkeeping need to be set to 1 because resume_and_return()
     unsets it */
  skip_bookkeeping = 1;
  if (!is_it_safe ((int)(long) block_info))
   return -1;
  print_blocks(block_info, rtcapi_output_file_ptr, type);
  return total_blocks;
}

int rtc_heap_corruption ()
{
  if (!rtc_started)
   return -1;
  if (!__rtc_check_bounds)
   {
     fprintf (stderr, "\nRTC heap corruption detection is disabled\n");
     return -1;
   }

  RTC_API_ENTRY;
  if (rtcapi_output_file_ptr == NULL)
   set_output_file ();
  int total_corrupted_blocks = 0;
  /* Toggle the switch to collect only corrupted one */
  __rtc_bounds_or_heap = BOUNDS_INFO;

  /* Collect and preprocess the new leaks and add them to the
     info_leak list */
  fprintf (rtcapi_output_file_ptr, "\n****Heap corruption report**** on %s\n",
           REPORT_TIME);
  total_corrupted_blocks = __rtc_heap_corruption_common (BOUNDS_INFO);
  RTC_API_EXIT;
  return total_corrupted_blocks;
}

/* Detects in which mode we are in and enables RTC if possible
   Return 0 on success, -1 on failure  */

int rtc_enable (int enable_what)
{
  if (!rtc_started && !rtc_disabled)
   {
     rtc_initialize ();
     fprintf (stderr, "\nRTC is initialized\n");
   }
  RTC_API_ENTRY;
  if (rtcapi_output_file_ptr == NULL)
   set_output_file ();
  int retval = __rtc_enable_api (enable_what);
  RTC_API_EXIT;
  return retval;
}

int __rtc_enable_api (int enable_what)
{
  /* We are in gdb, user did set heap-check on....
     But rtc_started depends on whether the malloc was called before
     rtc_enable or not.
     If called then, rtc_started is 1 and leak detection is enabled by gdb side.
     If not called then leaks detection is enabled from here.
     In either way we will not miss out any memory allocation libc call..
  */
  if (!rtc_started && rtc_disabled)
   {
     /* Here we are in gdb but user did not do set heap-check on....
        rtc_enable should take care of rtc initialization,
        if application calls rtc_enable */
     char * envstr = getenv ("HEAP_CHECK_ENABLED");
     if (envstr != NULL)
      {
        if ((strcmp (envstr, "off") == 0) ||
            (strcmp (envstr, "OFF") == 0) ||
            (strcmp (envstr, "0") == 0))
         {
           /* *Never ever set rtc_disable to 0 before making sure
              that rtc_started is set to 1. If you do it, it will results
              in ivalid leaks in report and many time call to rtc_initialize()
              , though it is one time call routine.
           */
           rtc_initialize ();
           if (rtc_started)
            {
              rtc_disabled = 0;
              fprintf (stderr, "\nRTC is initialized\n");
            }
           else
            {
              fprintf (stderr, "\nRTC is not intialized\n");
              return -1;
            }
         }
        else
         {
           fprintf (stderr, "\nRTC is not intialized\n");
           return -1;
         }
      }
     /* We are in simply link and run (RTC-API) mode */
     else if (envstr == NULL)
      {

        rtc_initialize ();
        if (rtc_started)
         {
           rtc_disabled = 0;
           fprintf (stderr, "\nRTC is initialized\n");
         }
        else
         {
           fprintf (stderr, "\nRTC is not intialized\n");
           return -1;
         }
      }
   }
  return (rtc_ED_common (enable_what, 1));
}

/* Disable requested check and returns 0 on success, -1 on error */
int rtc_disable (int disable_what)
{
  if (!rtc_started && rtc_disabled)
   {
     fprintf (stderr, "\nRTC is not running\n");
     return 0;
   }
  RTC_API_ENTRY;
  if (rtcapi_output_file_ptr == NULL)
   set_output_file ();
  int retval = __rtc_disable_api (disable_what);
  RTC_API_EXIT;
  return retval;
}

int __rtc_disable_api (int disable_what)
{
  if (rtc_started && !rtc_disabled)
   {
     return (rtc_ED_common (disable_what, 0));
   }
  return -1;
}

/* Helps rtc_enable and rtc_disable */
int rtc_ED_common (int what, int value)
{
     if (what & RTC_LEAKS_CHECK) /* 1st Bit in what */
      __rtc_check_leaks = value;
     if (what & RTC_HEAP_CHECK)  /* 2nd Bit in what */
      __rtc_check_heap = value;
     if (what & RTC_BOUNDS_CHECK) /* 3rd bit in what */
      __rtc_check_bounds = value;
     if (what & RTC_FREE_CHECK) /* 4th bit */
      __rtc_check_free = value;
     if (what & RTC_STRING_CHECK) /* 5th bit */
      __rtc_check_string = value;
     if (what == RTC_ALL) /* All 5 bits */
      {
        __rtc_check_leaks = value;
        __rtc_check_heap = value;
        __rtc_check_bounds = value;
        __rtc_check_free = value;
        __rtc_check_string = value;
      }
     if (what > RTC_ALL || what < RTC_LEAKS_CHECK) /* what is not in between 1 to 31 */
      {
        fprintf (stderr, "\nNot a valid RTC option\n");
        return -1;
      }
  return 0;
}

/* Returns 1 for RTC is running and 0 for not */
int rtc_running ()
{
  return ((rtc_started && !rtc_disabled));
}

static int is_it_safe (int value)
{
  FILE *fptr = rtcapi_output_file_ptr;
  switch (value)
   {
     case RTC_NOT_RUNNING:
      fprintf (fptr, "Heap debugging has not commenced or has been disabled.\n");
      break;

     case RTC_MUTEX_LOCK_FAILED:
       fprintf (fptr, "Some other thread has locked the mutex.\n");
       break;

      case RTC_UNSAFE_NOW:
        fprintf (fptr, "Current thread is inside the allocator.\n");
        break;

      case RTC_FOPEN_FAILED:
        fprintf (fptr, "Failed to open temporary file.\n");
        break;

      case RTC_FCLOSE_FAILED:
        fprintf (fptr, "Failed to close temporary file.\n");
        break;

      default:
        return 1;
        break;
   }
  return 0;
}

int
open(const char *path, int flags, ... /* [mode_t mode] */ )
{
  int fd = -1;
  va_list ap;
  int mode;

  va_start(ap, flags);
  mode = va_arg(ap, int);
  va_end(ap);

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
  {
    fd = libc_open ((char *)path, flags, mode);
    DEBUG_MALLOC( printf("open: %d\n", fd);)
    return fd;
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  fd = libc_open ((char *)path, flags, mode);

  /* Record this fd. */
  if (fd != -1)
    rtc_record_open (fd, path);

  DEBUG_MALLOC( printf("open: recorded & opened : %d\n", fd);)

  RETURN (fd);
}

/* This function saves the information about the currently opened
   file in our data structures. */
static void
rtc_record_open (int fd, const char *path)
{
  struct rtc_openfd_info fd_info = {0}, *new_fd_i = NULL, * fd_i = &fd_info;
  struct rtc_openfd_info * tmp_fd_i = NULL, * prev_fd_i = NULL;
  char *filename;
  void ** pc_list = 0;
  struct stack_trace_info *cached_stk_trc = 0;
  struct pst_fileinfo2 psf;
  struct pst_filedetails psfdetails;
  int count;

  if (rtc_disabled || fd == -1)
    return;

  fd_i->fd = fd;
  if (path != NULL)
    fd_i->filename = strdup (path);
  else
    fd_i->filename = NULL;

  (void)memset(&psf,0,sizeof(psf));
  count = pstat_getfile2(&psf, sizeof(psf), 0, fd, getpid());
  if (count == 1)
    fd_i->filetype = psf.psf_subtype;
  else
    fd_i->filetype = FD_UNKNOWN;

  fd_i->pc_tuple = NULL;  /* for now */
  fd_i->next_openfd = NULL;  /* for now */

  /* pc_list would continue to be a 1-array pc list
     rather than a 2-array pc_tuple, because we self unwind the
     current stack and get the pc list here.
     But when we call the cached_stack_trace() to see if there
     is a matching stack trace, we take care of correctly comparing
     this 1-array pc_list with c_i->pc_tuple[].pc !!
   */
  if (__rtc_frame_count)
    {
      pc_list = alloca (__rtc_frame_count * sizeof (void *));
      libc_memset(pc_list, 0, __rtc_frame_count * sizeof (void *));
    }
  else
    {
      pc_list = 0;
    }

  /* Let's do the unwinding and get the pc list. */
  get_frame_pc_list (pc_list, __rtc_frame_count);

  mutex_lock (&mutex);

  /* Reusing cached_stack_trace routine which already used while recording
   * memory allocations. So passing second argument ZERO.
   */
  cached_stk_trc = cached_stack_trace (pc_list, 0);
  if (cached_stk_trc)
    fd_i->pc_tuple = (void *)cached_stk_trc->pc_tuple;
  else
    fd_i->pc_tuple = 0;

  new_fd_i = libc_calloc (1, sizeof (rtc_openfd_info));

  if (!new_fd_i)
    {
      __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
      mutex_unlock( &mutex);
      return;
    }
  else
    *new_fd_i = *fd_i;

  /* Since currectly not all the file opening and closing libc calls are tabbed,
     it is a possibility that we encounter here a duplicate file-descriptor
     whose entry already exist in our data-structure. If there is a duplicate
     remove the old entry and add the new one. */
  for(tmp_fd_i = rtc_openfd_list; tmp_fd_i != NULL; tmp_fd_i = tmp_fd_i->next_openfd)
    {
      if (tmp_fd_i->fd == fd)
        {
          DEBUG_MALLOC( printf("Duplicate FD encountered: %d\n", fd);)
          if (prev_fd_i != NULL)   /* Not first node. */
            prev_fd_i->next_openfd = tmp_fd_i->next_openfd;
          else                     /* i.e. first node. */
            rtc_openfd_list = fd_i->next_openfd;
          if (rtc_openfd_tail == tmp_fd_i)
            rtc_openfd_tail = prev_fd_i;
          libc_free(tmp_fd_i);
          rtc_openfd_count--;
          break;
        }
      prev_fd_i = tmp_fd_i;
    }

  if (rtc_openfd_tail) 
    rtc_openfd_tail->next_openfd = new_fd_i;
  rtc_openfd_tail = new_fd_i;
  if (!rtc_openfd_list)
    rtc_openfd_list = new_fd_i;
  rtc_openfd_count++;

  if (shlib_info_invalid)
    update_shlib_info ();

  mutex_unlock (&mutex);

  return;
}



int
close (int fd)
{
  int ret = -1;
  if (fd < 0)
    return (ret);

  OBTAIN_BACKDOOR_ENTRY ();

  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
    {
      ret = libc_close (fd);

      DEBUG_MALLOC( printf("close: %d\n", fd);)

      return (ret);
    }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  ret = libc_close (fd);     /* try closing it! */
  if (0 == ret)              /* If closed */
    rtc_record_close (fd);   /* record it! */

  DEBUG_MALLOC( printf("close: recorded & closed: %d\n", fd);)

  RETURN (ret);
}


/* Remove information corresponding to file descriptor fd from our data structures. */
static void
rtc_record_close (int fd)
{
  rtc_openfd_info *fd_i = 0, *prev_fd_i = 0;

  if (rtc_disabled || fd < 0)      /* nothing to do */
    return;

  mutex_lock (&mutex);             /* Grab the lock. */

  fd_i = rtc_openfd_list;     /* Start search from first node. */
  while (fd_i != NULL)             /* Search all open fds. */
    {
      if (fd_i->fd == fd)          /* Got it !! */
        {
          if (prev_fd_i != NULL)   /* Not first node. */
            prev_fd_i->next_openfd = fd_i->next_openfd;
          else                     /* i.e. first node. */
            rtc_openfd_list = fd_i->next_openfd;
          if (rtc_openfd_tail == fd_i)
            rtc_openfd_tail = prev_fd_i;
          libc_free(fd_i);
          rtc_openfd_count--;
          mutex_unlock (&mutex);   /* Release the lock. */
          return;
        }
      prev_fd_i = fd_i;            /* Store as previous */
      fd_i = fd_i->next_openfd;    /* Move to the next node. */
    }      /* while */

  /* Since currently not all the libc functions that cause the file-descriptor
     to open are being tabbed. Execution will reach here if close is called by
     FDs opened by those other system calls. */
  /* Once all the such FD opening libc function calls are tabbed execution
     should not reach here. */

  mutex_unlock (&mutex);           /* Unlock. */

  /* Commenting the warning message below till all the FD opening libc function
     calls are tabbed */   
  /* If we reach here, then there is an error and RTC's data structures are
     corrupted.
  printf("warning: RTC's internal data structure seems to be corrupted. \n");
  */
}



/* Print out a report on all the open files present in rtc_openfd_list.
   If fp is provided print a detailed report. If not write to
   stdout a brief desc of all the open fds. */
static void
print_openfds (FILE * fp)
{
  int i = 0;
  char local_buf[4096];
  rtc_openfd_info * this_openfd_info = NULL;

  if (fp)
    {
      fprintf (fp, "Total no. of files left open:\t=\t%d\n", rtc_openfd_count);
      fprintf (fp, "No. FD       Type     Function        Name\n");
    }
  else
    {
     // printf_filtered ("Total no. of files left open:\t=\t%d\n", rtc_openfd_count);
     // printf_filtered("No. FD       Type     Function        Name\n");
      printf ("Total no. of files left open:\t=\t%d\n", rtc_openfd_count);
      printf ("No. FD       Type     Function        Name\n");
    }

  struct symbol_from_unload_lib *stack_frame_symbol
    = libc_malloc(sizeof(struct symbol_from_unload_lib));

  /* One by one report all the open file-descriptors. */
  for(this_openfd_info = rtc_openfd_list;
        this_openfd_info != NULL;
        this_openfd_info = this_openfd_info->next_openfd, i++)
    {
      struct minimal_symbol * m = NULL;
      char * function = "???";
      char * parenth;
      int  backtrace_ret = 0;

      if (this_openfd_info->pc_tuple && this_openfd_info->pc_tuple[0].pc != 0)
        {
          stack_frame_symbol->symbol_name = NULL;
          stack_frame_symbol->src_file_name = NULL;

          backtrace_ret = backtrace (NULL, 0,
                     SWIZZLE_PTR((uint64_t)(this_openfd_info->pc_tuple[0].pc)),
                     this_openfd_info->pc_tuple[0].library_index_of_pc,
                     &stack_frame_symbol);
          if (backtrace_ret)
            function = stack_frame_symbol->symbol_name;

          /* QXCR1000989650: If the symbol_name is NULL, print "????" */
          if (!function)
            function = "????";
        }

      parenth = ( strchr(function,'(') == NULL &&
                  strchr(function,')') == NULL ) ? "()" : "  ";

      if (fp)
        {
          fprintf (fp, "%d   %d   %s   %s%s   %s\n",
                       i,
                       this_openfd_info->fd,
                       rtc_filetype_str[this_openfd_info->filetype],
                       function, parenth,
                       this_openfd_info->filename);
        }
      else
        {
          sprintf (local_buf, "%d   %d   %s   %s%s   %s\n",
                       i,
                       this_openfd_info->fd,
                       rtc_filetype_str[this_openfd_info->filetype],
                       function, parenth,
                       this_openfd_info->filename);
          puts (local_buf);
        }
      if (backtrace_ret)
        {
          stack_frame_symbol->symbol_name = NULL;

          if (stack_frame_symbol->src_file_name)
            libc_free(stack_frame_symbol->src_file_name);

          stack_frame_symbol->src_file_name = NULL;
        }
    }

    libc_free(stack_frame_symbol);

    /* Now append the detailed report for each open file descriptor. */
    if (fp)
      {
        fprintf (fp, "\n\n-------------------------------------------------------------------------\n");
        fprintf (fp, "\n"
"                              Detailed Report\n");
        fprintf (fp, "\n-------------------------------------------------------------------------\n");
        /* One by one report details of all the open file-descriptors. */
        for(this_openfd_info = rtc_openfd_list;
            this_openfd_info != NULL;
            this_openfd_info = this_openfd_info->next_openfd)
          {
            report_this_openfd (this_openfd_info, fp);
            fprintf (fp, "\n-------------------------------------------------------------------------\n");
          }
      }
}


/* Print out the information about a open file descriptor having ID (id).
   Write it to fp if provided else to stdout.
*/

static void
report_this_openfd (rtc_openfd_info *this_openfd_info, FILE * fp)
{
  char local_buf[200];

  if (!this_openfd_info)
    return;

    /* Now that we have found the node, create the report. */
    sprintf (local_buf, "File Name\t:\t%s\n"
                        "File Descriptor\t:\t%d\n"
                        "File Type\t:\t%s\n",
                        this_openfd_info->filename,
                        this_openfd_info->fd,
                        rtc_filetype_str[this_openfd_info->filetype]);
    if (fp)
      fputs (local_buf, fp);
    else
      puts (local_buf);

    if (!this_openfd_info->pc_tuple)
      return;

    if (fp)
      fprintf (fp, "\nStack-trace :-\n");
    else
      printf ("\nStack-trace :-\n");

    /* Print the frame stack. */
    print_stack_trace_from_pclist (fp, this_openfd_info->pc_tuple, __rtc_frame_count);

    return;
}

static void
rtc_initialize_openfd (void)
{
  static int initialize_openfd_flag = 0;
#define BURST ((size_t)10)
  pid_t target = getpid();
  struct pst_fileinfo2 psf[BURST];
  int count;
  int idx = 0; /* index within the context */
  struct rtc_openfd_info fd_info = {0}, *new_fd_i = NULL, * fd_i = &fd_info;
  int i;

  if (initialize_openfd_flag || rtc_disabled)
    return;

  /* Gather the information about all the file descriptors initially
   * open by the target. */
  /* loop until all fetched */
  while ((count = pstat_getfile2(psf, sizeof(struct pst_fileinfo2),
                                 BURST, idx, target)) > 0) {
    /* process them (max of BURST) at a time */
    for (i = 0; i < count; i++) {
      fd_i->fd = psf[i].psf_fd;
      switch (fd_i->fd) {
        case 0:
          fd_i->filename = "<stdin>";
          break;
        case 1:
          fd_i->filename = "<stdout>";
          break;
        case 2:
          fd_i->filename = "<stderr>";
          break;
        default:
          fd_i->filename = "---";
      }
      fd_i->filetype = psf[i].psf_subtype;
      fd_i->pc_tuple = NULL;  /* For special open files */
      fd_i->next_openfd = NULL;  /* for now */

      mutex_lock (&mutex);

      new_fd_i = libc_calloc (1, sizeof (rtc_openfd_info));

      if (!new_fd_i) {
        __rtc_event (RTC_NO_MEMORY, 0, 0, 0);
        mutex_unlock( &mutex);
        return;
      }
      else
        *new_fd_i = *fd_i;

      if (rtc_openfd_tail)
        rtc_openfd_tail->next_openfd = new_fd_i;
      rtc_openfd_tail = new_fd_i;
      if (!rtc_openfd_list)
        rtc_openfd_list = new_fd_i;
      rtc_openfd_count++;

      if (shlib_info_invalid)
        update_shlib_info ();

      mutex_unlock (&mutex);
    }
    /*
     * Now go back and do it again, using the
     * next index after the current 'burst'
     */
    idx = psf[count-1].psf_fd + 1;
  }
  if (count == -1)
    perror("pstat_getfile2()");
#undef BURST

  initialize_openfd_flag = 1;
}


FILE *
fopen(const char *file, const char *mode)
{
  int fd = -1;
  FILE * file_i = NULL;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
  {
    file_i = libc_fopen ((char *)file, (char *)mode);

    if (file_i != NULL)
      fd = fileno (file_i);
    DEBUG_MALLOC( printf("fopen: %d\n", fd);)
    return (file_i);
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  file_i = libc_fopen ((char *)file, (char *)mode);

  /* Record this fd. */
  if (file_i != NULL)
    {
      fd = fileno (file_i);
      rtc_record_open (fd, file);
    }

  DEBUG_MALLOC( printf("fopen: recorded & opened : %d\n", fd);)

  RETURN (file_i);
}


int
fclose(FILE *stream)
{
  int fd = -1;
  int ret = EOF;
  if (stream == NULL)
    return (ret);

  OBTAIN_BACKDOOR_ENTRY ();

  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
    {
      ret = libc_fclose (stream);

      fd = fileno (stream);
      DEBUG_MALLOC( printf("fclose: %d\n", fd);)

      return (ret);
    }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  ret = libc_fclose (stream);     /* try closing it! */
  if (0 == ret)                   /* If closed */
    {
      fd = fileno (stream);
      rtc_record_close (fd);        /* record it! */
    }

  DEBUG_MALLOC( printf("fclose: recorded & closed: %d\n", fd);)

  RETURN (ret);
}


int
dup (int fildes)
{
  int fd = -1;
  char * path;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
  {
    fd = libc_dup (fildes);
    DEBUG_MALLOC( printf("dup: %d\n", fd);)
    return fd;
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  fd = libc_dup (fildes);

  /* Record this fd. */
  if (fd != -1)
    {
      path = get_filename_for_fd (fildes);
      rtc_record_open (fd, path);
    }

  DEBUG_MALLOC( printf("dup: recorded & opened : %d\n", fd);)

  RETURN (fd);
}


static char *
get_filename_for_fd (int fd)
{
  struct rtc_openfd_info * tmp_fd_i = NULL;
  char *filename = NULL;

  for(tmp_fd_i = rtc_openfd_list; tmp_fd_i != NULL; tmp_fd_i = tmp_fd_i->next_openfd)
    {
      if (tmp_fd_i->fd == fd && tmp_fd_i->filename != NULL)
        {
          filename = strdup(tmp_fd_i->filename);
        }
    }
  return (filename);
}

int
dup2(int fildes, int fildes2)
{
  int fd = -1;
  char * path;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
  {
    fd = libc_dup2 (fildes, fildes2);
    DEBUG_MALLOC( printf("dup2: %d\n", fd);)
    return fd;
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  fd = libc_dup2 (fildes, fildes2);

  /* Record this fd. */
  if (fd != -1)
    {
      rtc_record_close (fildes2); /* Old stream is closed first .*/
      path = get_filename_for_fd (fildes); /* Obtain the filename.*/
      rtc_record_open (fd, path);
    }

  DEBUG_MALLOC( printf("dup2: recorded & opened : %d\n", fd);)

  RETURN (fd);
}


int
creat (const char *path, mode_t mode)
{
  int fd = -1;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
  {
    fd = libc_creat ((char *)path, mode);
    DEBUG_MALLOC( printf("creat: %d\n", fd);)
    return fd;
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  fd = libc_creat ((char *)path, mode);

  /* Record this fd. */
  if (fd != -1)
    rtc_record_open (fd, path);

  DEBUG_MALLOC( printf("creat: recorded & opened : %d\n", fd);)

  RETURN (fd);
}


FILE *
freopen (const char *pathname, const char *type, FILE *stream)
{
  int fd = -1, fd_old = -1;
  FILE * file_i = NULL;

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if ( rtc_disabled
       || __pthreads_not_ready
       || __inside_librtc
       || skip_bookkeeping
       || (batch_mode_env != BATCH_MODE_ON)) /* Currently for batch mode only */
  {
    file_i = libc_freopen ((char *)pathname, (char *)type, stream);

    if (file_i != NULL)
      fd = fileno (file_i);
    DEBUG_MALLOC( printf("freopen: %d\n", fd);)
    return (file_i);
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  if (stream != NULL)
    fd_old = fileno (stream); /* Store it now to remove from records later. */

  file_i = libc_freopen ((char *)pathname, (char *)type, stream);

  /* Record this fd. */
  if (file_i != NULL)
    {
      /* According to the definition of freopen, "stream" is closed first.*/
      rtc_record_close (fd_old); /* Delete it from records. */

      /* Record the newly open file-descriptor. */
      fd = fileno (file_i);
      rtc_record_open (fd, pathname);
    }

  DEBUG_MALLOC( printf("freopen: recorded & opened : %d\n", fd);)

  RETURN (file_i);
}


int
fcntl (int fildes, int cmd, ... /* arg */)
{
  int fd = -1, ret;
  char * path = NULL;
  va_list ap;
  void *argp;

  va_start(ap, cmd);
  argp = va_arg(ap, void *);
  va_end(ap);

  OBTAIN_BACKDOOR_ENTRY ();

  /* QXCR1000989650: Sometimes, when the app is compiled with +check,
   * the rtc_aux functions will reset __inside_librtc to 0, even when
   * skip_bookkeeping is set. This may lead to bad free errors etc.
   * We need to skip book keeping of malloc/free/.. as long as
   * skip_bookkeeping is 1.
   */
  if (cmd != F_DUPFD ||
       ( rtc_disabled
         || __pthreads_not_ready
         || __inside_librtc
         || skip_bookkeeping
         || (batch_mode_env != BATCH_MODE_ON))) /* Currently for batch mode only */
  {
    ret = libc_fcntl (fildes, cmd, argp);
    DEBUG_MALLOC( printf("fcntl: %d\n", ret);)
    return ret;
  }

  __inside_librtc = 1;
  if (!rtc_started)
    rtc_initialize ();

  fd = libc_fcntl (fildes, cmd, (long)argp);

  /* Record this fd. */
  if (fd != -1)
    {
      path = get_filename_for_fd (fildes);
      rtc_record_open (fd, path);
    }

  DEBUG_MALLOC( printf("fcntl: recorded & opened : %d\n", fd);)

  RETURN (fd);
}

#endif /* HP_IA64 */
