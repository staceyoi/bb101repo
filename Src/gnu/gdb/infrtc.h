#ifndef INFRTC_H
#define INFRTC_H

/*
   Sujoy, stuff common to infrtc.c, hp-ia64-rtc.c etc. (inferior code)
*/

/*
---------------------------------------------------------
   Debug print related control variables and macros
---------------------------------------------------------
*/
extern int rtc_debug;

#define DEBUG(x)   if (rtc_debug) x
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
#define DEBUG_MALLOC(x)   if (rtc_debug_mallocs){ x }
#endif
#define TRACE(x)

#define HP_VACCINE_DBG_MALLOC      1  /* enable HP_VACCINE_DBG_MALLOC */
#ifdef HP_IA64
#define HP_VACCINE_DBG_MALLOC_IA64 1
#else
#define HP_VACCINE_DBG_MALLOC_IA64 0
#endif

/*
---------------------------------------------------------
   Misc. commonly used macros
---------------------------------------------------------
*/

#define HEAP_INFO 1
#define BOUNDS_INFO 2

//
// SWIZZLE_PTR
//
// DESCRIPTION:
//  The given 64bit address is converted into a swizzled address.
//
// EQUIVALENT PROTOTYPE:
//  static uint64_t
//  SWIZZLE_PTR (uint64_t addr);
//  UNSWIZZLE_PTR (uint64_t addr);
//
// NOTES:
//  - Where IA64 inline assembly is available, an inlined 'addp4'
//    performs the required swizzling in an efficient manner.
//
#ifdef __hpux
#ifdef __LP64__
#   define SWIZZLE_PTR(addr_) (addr_)
#else
#   define SWIZZLE_PTR(addr_) \
    _Asm_addp4(0, (uint64_t)(addr_))
#endif
#endif

#ifdef __hpux
#ifdef __LP64__
#   define UNSWIZZLE_PTR(addr_) (addr_)
#else
#   define UNSWIZZLE_PTR(addr_) \
    ((uintptr_t)(addr_) & 0xFFFFFFFF)
#endif
#endif

/*
---------------------------------------------------------
   Thread context information holder
---------------------------------------------------------
*/

/* When the program is multithreaded, we need to scan the registers
   associated with all the threads. We will allocate a buffer to 
   hold the register values here. GDB is responsible for copying the
   registers over.
*/

extern volatile char * __rtc_register_file;
extern int register_file_size;

/* How big the register spill area be per thread ?
   defined by RTC_REGISTER_BUFFER_SIZE in target dep files. */

#ifdef REGISTER_STACK_ENGINE_FP
extern volatile char *__rtc_RSE_file;
extern int __rtc_RSE_file_size;
#define RSE_BUFFER_SIZE 16 /* for rse top/bottom. */
#endif /* REGISTER_STACK_ENGINE_FP */

/* This variable is used by the debugger to let us know if the main thread
   exited. We shouldn't be scanning the main stack if the main thread
   exited. */
extern volatile int __main_thread_exited;

/*
---------------------------------------------------------
   Thread control/context APIs
---------------------------------------------------------
*/

#ifdef HP_IA64
void suspend_all_threads();

void resume_all_threads();

int get_thread_contexts (
  char **stack_end_ptr,
  int *is_this_main_thr_ptr
);

/*
---------------------------------------------------------
   Symbol lookup related data and APIs
---------------------------------------------------------
*/

typedef struct symbol_from_unload_lib {
  struct symbol_from_unload_lib *next;
  char *symbol_name, *src_file_name;
  unsigned short line_number, offset;
  unsigned int unswizzled_addr;
} stack_trace_symbol_hash_entry_structure;

int backtrace (FILE *, int, uint64_t, int, 
               struct symbol_from_unload_lib **);

struct symbol_from_unload_lib *
load_or_store_symbol_for_unloading_library (uint64_t, uint64_t, int);

/* Print address of functions with the heap/leak report */
extern bool addressprint;

#endif // HP_IA64

#endif

