/* Target-struct-independent code to start (run) and stop an inferior process.
   Copyright 1986-1989, 1991-2000 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdb_string.h"
#include <ctype.h>
#include "symtab.h"
#include "frame.h"
#include "inferior.h"
#include "breakpoint.h"
#include "gdb_wait.h"
#include "gdbcore.h"
#include "gdbcmd.h"
#include "target.h"
#include "gdbthread.h"
#include "annotate.h"
#include "symfile.h"		/* for overlay functions */
#include "top.h"
#include <signal.h>
#include "inf-loop.h"
#include "thread.h"
#include <sys/pstat.h>

#ifdef RTC
#include "rtc.h"
#endif
#ifdef DYNLINK_HAS_BREAK_HOOK
#include "objfiles.h"
#endif

#ifdef HPPA_FIX_AND_CONTINUE
  extern void set_reapply_fix (boolean);
  extern boolean get_reapply_fix();
#endif
#ifndef GDB_TARGET_IS_HPPA_20W
  extern CORE_ADDR _start_addr;
#endif
/* from ia64-tdep.c or hppa-tdep.c */
extern value_ptr ia64_value_returned_from_stack (struct type *, CORE_ADDR);

/* Used to capture the dummy breakpoint address hit*/
CORE_ADDR current_dummy_brk_addr;
/*From utils.c */
extern int debugging_dld;
/* Used to perform implicit/real stepi`s before stepping into signal handler. 
   These stepi`s will be in sigtramp. This tells us whether we are stepping through sigtramp, 
   if so we need to break and do another stepi and ttrace_wait for single-step trap. 
   For PA, kernel calls signal-handler without an intermediate tramploine, hence the stop_pc 
   is set to the correct signal_handler in debugee without an intermediate trampoline PC, 
   this imposes a stepi, which otherwise would manifest into a continue. */
boolean sigtramp_stepi_again = false;
extern boolean single_stepping;

extern int next_command_inline_idx;
extern int executing_next_command;

extern void reset_prefork_wait (void);
extern int feature_level;

/* JAGae68084 */
extern int if_yes_to_all_commands;
extern int now_re_enable_bp;

/* JAGaf54650 */
int saved_child_pid;
int attach_stopped = 0;

/* To handle bogus sigtrap due to pending events from threads */ 
/* during CLC (command line call) run */
extern int cmd_line_call_cnt;  
#ifdef GDB_TARGET_IS_HPUX
extern boolean inferior_is_live (void); 
#endif

/* Size of the pointer in the inferior. */
#ifdef HP_IA64
#define PTR_SIZE (is_swizzled ? 4 : 8)
#else
#define PTR_SIZE (sizeof (CORE_ADDR))
#endif /* !HP_IA64 */

static boolean nullify_inferior_preload_env (int pid);

/* From infttrace.c */
extern int target_remove_internal_breakpoint (CORE_ADDR addr, void* shadow);

/* From exec.c */
extern void exec_close (int);

/* From source.c */
extern int absolute_path_of (char *, char **);

/* From breakpoint.c */
extern void defer_breakpoints (void);
extern boolean is_hw_break (enum bptype);
extern boolean are_addrs_equal (CORE_ADDR, CORE_ADDR);

/* Structure to save the parent pids for debugging
   multiple level of forking.
 */
struct parent_pid_stack {
	int pid;
	char proc_name[MAXPATHLEN];
	struct parent_pid_stack *next;
};

/* Head of the parent pids' stack */
struct parent_pid_stack *ppid_stack = NULL;

/* To push parent pid into the stack on a fork or vfork */
void push_ppid (int pid);

/* To pop parent pid from the stack on child's pre-exit */
struct parent_pid_stack * pop_ppid (void);

/* JAGaf54650 - end of declarations */

/* From breakpoint.c */
extern int breakpoint_was_here_p (CORE_ADDR pc);

/* From ia64-tdep.c */
extern int is_pc_in_covariant_thunk_return_path (CORE_ADDR pc);

/* Prototypes for local functions */

static void signals_info (char *, int);

static void handle_command (char *, int);

static void sig_print_info (enum target_signal);

static void sig_print_header (void);

static void resume_cleanups (void *);

static int hook_stop_stub (void *);

static void delete_breakpoint_current_contents (void *);

static struct inferior_status *xmalloc_inferior_status (void);

static void free_inferior_status (struct inferior_status *);

static int restore_selected_frame (void *);

static void build_infrun (void);

static void follow_inferior_fork (int parent_pid, int child_pid,
				  int has_forked, int has_vforked);

static void follow_fork (int parent_pid, int child_pid);

static void follow_vfork (int parent_pid, int child_pid);

static void set_schedlock_func (char *args, int from_tty,
				struct cmd_list_element * c);

struct execution_control_state;

static int currently_stepping (struct execution_control_state *ecs);

static void xdb_handle_command (char *args, int from_tty);

void _initialize_infrun (void);

#ifdef TUI
int tui_auto_refresh = 0;
extern void _tuiRefreshAll_command (char *, int);
#endif

int inferior_ignoring_startup_exec_events = 0;
int inferior_ignoring_leading_exec_events = 0;

/* In asynchronous mode, but simulating synchronous execution. */

int sync_execution = 0;

#ifdef HP_IA64_NATIVE
     extern void invalidate_rse_info(void);
#endif

extern void (*libjvm_load_hook) (char*);

/* Globals to maintain the state of breakpoint at __gdb_java_breakpoint. */
/* From stack.c */
extern char gdb_java_break_buffer[16];
extern CORE_ADDR gdb_java_breakpoint_addr;

extern void libjvm_got_loaded (char*);

/* wait_for_inferior and normal_stop use this to notify the user
   when the inferior stopped in a different thread than it had been
   running in.  */

int previous_inferior_pid = 0;

/* This is true for configurations that may follow through execl() and
   similar functions.  At present this is only true for HP-UX native.  */

#ifndef MAY_FOLLOW_EXEC
#define MAY_FOLLOW_EXEC (0)
#endif


/* srikanth, 000711, the variable vfork_in_flight which identifies
   if a vfork is in progress is used only by HP-UX 11.00. For other
   configurations, its value is zero. Declaring it without an initial
   value here and declaring it with an initial value in infttrace.c
   ensures correct linkage in all configurations.
*/
   int vfork_in_flight;

static int may_follow_exec = MAY_FOLLOW_EXEC;

/* resume and wait_for_inferior use this to ensure that when
   stepping over a hit breakpoint in a threaded application
   only the thread that hit the breakpoint is stepped and the
   other threads don't continue.  This prevents having another
   thread run past the breakpoint while it is temporarily
   removed.

   This is not thread-specific, so it isn't saved as part of
   the infrun state.

   Versions of gdb which don't use the "step == this thread steps
   and others continue" model but instead use the "step == this
   thread steps and others wait" shouldn't do this.  */

static int thread_step_needed = 0;

/* This is true if thread_step_needed should actually be used.  At
   present this is only true for HP-UX native.  */

#ifndef USE_THREAD_STEP_NEEDED
#define USE_THREAD_STEP_NEEDED (0)
#endif

static int use_thread_step_needed = USE_THREAD_STEP_NEEDED;

/* Some machines have trampoline code that sits between function callers
   and the actual functions themselves.  If this machine doesn't have
   such things, disable their processing.  */

#ifndef SKIP_TRAMPOLINE_CODE
#define	SKIP_TRAMPOLINE_CODE(pc)	0
#endif

/* Dynamic function trampolines are similar to solib trampolines in that they
   are between the caller and the callee.  The difference is that when you
   enter a dynamic trampoline, you can't determine the callee's address.  Some
   (usually complex) code needs to run in the dynamic trampoline to figure out
   the callee's address.  This macro is usually called twice.  First, when we
   enter the trampoline (looks like a normal function call at that point).  It
   should return the PC of a point within the trampoline where the callee's
   address is known.  Second, when we hit the breakpoint, this routine returns
   the callee's address.  At that point, things proceed as per a step resume
   breakpoint.  */

#ifndef DYNAMIC_TRAMPOLINE_NEXTPC
#define DYNAMIC_TRAMPOLINE_NEXTPC(pc) 0
#endif

/* If the program uses ELF-style shared libraries, then calls to
   functions in shared libraries go through stubs, which live in a
   table called the PLT (Procedure Linkage Table).  The first time the
   function is called, the stub sends control to the dynamic linker,
   which looks up the function's real address, patches the stub so
   that future calls will go directly to the function, and then passes
   control to the function.

   If we are stepping at the source level, we don't want to see any of
   this --- we just want to skip over the stub and the dynamic linker.
   The simple approach is to single-step until control leaves the
   dynamic linker.

   However, on some systems (e.g., Red Hat Linux 5.2) the dynamic
   linker calls functions in the shared C library, so you can't tell
   from the PC alone whether the dynamic linker is still running.  In
   this case, we use a step-resume breakpoint to get us past the
   dynamic linker, as if we were using "next" to step over a function
   call.

   IN_SOLIB_DYNSYM_RESOLVE_CODE says whether we're in the dynamic
   linker code or not.  Normally, this means we single-step.  However,
   if SKIP_SOLIB_RESOLVER then returns non-zero, then its value is an
   address where we can place a step-resume breakpoint to get past the
   linker's symbol resolution function.

   IN_SOLIB_DYNSYM_RESOLVE_CODE can generally be implemented in a
   pretty portable way, by comparing the PC against the address ranges
   of the dynamic linker's sections.

   SKIP_SOLIB_RESOLVER is generally going to be system-specific, since
   it depends on internal details of the dynamic linker.  It's usually
   not too hard to figure out where to put a breakpoint, but it
   certainly isn't portable.  SKIP_SOLIB_RESOLVER should do plenty of
   sanity checking.  If it can't figure things out, returning zero and
   getting the (possibly confusing) stepping behavior is better than
   signalling an error, which will obscure the change in the
   inferior's state.  */

#ifndef IN_SOLIB_DYNSYM_RESOLVE_CODE
#define IN_SOLIB_DYNSYM_RESOLVE_CODE(pc) 0
#endif

#ifndef SKIP_SOLIB_RESOLVER
#define SKIP_SOLIB_RESOLVER(pc) 0
#endif

/* For SVR4 shared libraries, each call goes through a small piece of
   trampoline code in the ".plt" section.  IN_SOLIB_CALL_TRAMPOLINE evaluates
   to nonzero if we are current stopped in one of these. */

#ifndef IN_SOLIB_CALL_TRAMPOLINE
#define IN_SOLIB_CALL_TRAMPOLINE(pc,name)	0
#endif

/* In some shared library schemes, the return path from a shared library
   call may need to go through a trampoline too.  */

#ifndef IN_SOLIB_RETURN_TRAMPOLINE
#define IN_SOLIB_RETURN_TRAMPOLINE(pc,name)	0
#endif

/* This function returns TRUE if pc is the address of an instruction
   that lies within the dynamic linker (such as the event hook, or the
   dld itself).

   This function must be used only when a dynamic linker event has
   been caught, and the inferior is being stepped out of the hook, or
   undefined results are guaranteed.  */

#ifndef SOLIB_IN_DYNAMIC_LINKER
#define SOLIB_IN_DYNAMIC_LINKER(pid,pc) 0
#endif

/* On MIPS16, a function that returns a floating point value may call
   a library helper function to copy the return value to a floating point
   register.  The IGNORE_HELPER_CALL macro returns non-zero if we
   should ignore (i.e. step over) this function call.  */
#ifndef IGNORE_HELPER_CALL
#define IGNORE_HELPER_CALL(pc)	0
#endif

/* On some systems, the PC may be left pointing at an instruction that  won't
   actually be executed.  This is usually indicated by a bit in the PSW.  If
   we find ourselves in such a state, then we step the target beyond the
   nullified instruction before returning control to the user so as to avoid
   confusion. */

#ifndef INSTRUCTION_NULLIFIED
#define INSTRUCTION_NULLIFIED 0
#endif

/* We can't step off a permanent breakpoint in the ordinary way, because we
   can't remove it.  Instead, we have to advance the PC to the next
   instruction.  This macro should expand to a pointer to a function that
   does that, or zero if we have no such function.  If we don't have a
   definition for it, we have to report an error.  */
#ifndef SKIP_PERMANENT_BREAKPOINT 
#define SKIP_PERMANENT_BREAKPOINT (default_skip_permanent_breakpoint)
static void
default_skip_permanent_breakpoint (void)
{
  error_begin ();
  fprintf_filtered (gdb_stderr, "\
The program is stopped at a permanent breakpoint, but GDB does not know\n\
how to step past a permanent breakpoint on this architecture.  Try using\n\
a command like `return' or `jump' to continue execution.\n");
  return_to_top_level (RETURN_ERROR);
}
#endif
   

/* Convert the #defines into values.  This is temporary until wfi control
   flow is completely sorted out.  */

#ifndef HAVE_STEPPABLE_WATCHPOINT
#define HAVE_STEPPABLE_WATCHPOINT 0
#else
#undef  HAVE_STEPPABLE_WATCHPOINT
#define HAVE_STEPPABLE_WATCHPOINT 1
#endif

#ifndef HAVE_NONSTEPPABLE_WATCHPOINT
#define HAVE_NONSTEPPABLE_WATCHPOINT 0
#else
#undef  HAVE_NONSTEPPABLE_WATCHPOINT
#define HAVE_NONSTEPPABLE_WATCHPOINT 1
#endif

#ifndef HAVE_CONTINUABLE_WATCHPOINT
#define HAVE_CONTINUABLE_WATCHPOINT 0
#else
#undef  HAVE_CONTINUABLE_WATCHPOINT
#define HAVE_CONTINUABLE_WATCHPOINT 1
#endif

#ifndef CANNOT_STEP_HW_WATCHPOINTS
#define CANNOT_STEP_HW_WATCHPOINTS 0
#else
#undef  CANNOT_STEP_HW_WATCHPOINTS
#define CANNOT_STEP_HW_WATCHPOINTS 1
#endif

/* Tables of how to react to signals; the user sets them.  */

static unsigned char *signal_stop;
static unsigned char *signal_print;
static unsigned char *signal_program;

#define SET_SIGS(nsigs,sigs,flags) \
  do { \
    int signum = (nsigs); \
    while (signum-- > 0) \
      if ((sigs)[signum]) \
	(flags)[signum] = 1; \
  } while (0)

#define UNSET_SIGS(nsigs,sigs,flags) \
  do { \
    int signum = (nsigs); \
    while (signum-- > 0) \
      if ((sigs)[signum]) \
	(flags)[signum] = 0; \
  } while (0)


/* When a subroutine call goes to the dynamic linker bind-on-reference
   routine, we need to save some information and wait for the 
   bind-on-reference completion event.  This structure holds the 
   information for one call in flight.  The variable bor_events_pending
   points at the list of pending events.  There may be events for 
   multiple threads and it is even possible (in theory) to have more than 
   one event for one thread pending.

   For the case of multiple events for one thread, the last one added to
   our list must complete before any others, so we put it at the head of
   the list so we will find it before any other events for this thread.

   When we get the completion event, we decide whether to put a breakpoint
   at the call address or the return address, depending upon whether the
   destination is debuggable.
   */

typedef struct pending_bor_event_struct {
    struct pending_bor_event_struct *	next;
    CORE_ADDR				return_address;
    int					thread_id;
  } pending_bor_event_t;

pending_bor_event_t * bor_events_pending = 0;

/* JAGaf54451 - Breakpoint not triggered for <stop in/at> command in non-dbx mode */
/* Command list pointer for the "stop" placeholder.  */

extern struct cmd_list_element *stop_hook_command; /* defined in breakpoint.c */
/* JAGaf54451 - END */

/* Nonzero if breakpoints are now inserted in the inferior.  */

int breakpoints_inserted;

/* Suresh : Needed for batch thread RTC to exit cleanly if a signal is
received or if the application is terminated */
int batch_thread_rtc_needs_to_exit = 0;
/* Function inferior was in as of last step command.  */

static struct symbol *step_start_function;

/* Srikanth, did we start stepping while stopped in
   some inline function ? If so capture the index, so
   when stepping finishes we can determine if we are
   stopped in the same instance. This knowledge is
   needed to advise show_and_print_stack_frame what it
   should be printing. Jun  23, 2005.
*/
int step_start_function_inline_index = -1;

/* Nonzero if we are expecting a trace trap and should proceed from it.  */

static int trap_expected;

#ifdef SOLIB_ADD
/* Nonzero if we want to give control to the user when we're notified
   of shared library events by the dynamic linker.  */
static int stop_on_solib_events;
#endif

#ifdef HP_OS_BUG
/* Nonzero if the next time we try to continue the inferior, it will
   step one instruction and generate a spurious trace trap.
   This is used to compensate for a bug in HP-UX.  */

static int trap_expected_after_continue;
#endif

/* These two flags are used if we want to single step before continuing the
   thread. Currently they are used to single step after a SYSCALL_RETURN
   event for sigwait type system calls is received. */

static boolean step_before_continuing;
static boolean trap_expected_after_step;

extern int signal_sigwait (int); /* defined in infttrace.c */

/* Nonzero means expecting a trace trap
   and should stop the inferior and return silently when it happens.  */

int stop_after_trap;

/* Nonzero means expecting a trap and caller will handle it themselves.
   It is used after attach, due to attaching to a process;
   when running in the shell before the child program has been exec'd;
   and when running some kinds of remote stuff (FIXME?).  */

int stop_soon_quietly;

/* Nonzero if proceed is being used for a "finish" command or a similar
   situation when stop_registers should be saved.  */

int proceed_to_finish;

/* Save register contents here when about to pop a stack dummy frame,
   if-and-only-if proceed_to_finish is set.
   Thus this contains the return value from the called function (assuming
   values are returned in a register).  */

char *stop_registers;

/* Nonzero if program stopped due to error trying to insert breakpoints.  */

static int breakpoints_failed;

/* Nonzero after stop if current stack frame should be printed.  */

int stop_print_frame;

struct breakpoint *step_resume_breakpoint = NULL;

static struct breakpoint *through_sigtramp_breakpoint = NULL;

extern CORE_ADDR steplast_pc;

/* This variable keeps track of if we have seen syscall 491 and have
   not seen syscall 492. 491 is used to disable signals of the
   process and 492 to enable the signal. */
static int syscall_491_seen = 0;

/* On some platforms (e.g., HP-UX), hardware watchpoints have bad
   interactions with an inferior that is running a kernel function
   (aka, a system call or "syscall").  wait_for_inferior therefore
   may have a need to know when the inferior is in a syscall.  This
   is a count of the number of inferior threads which are known to
   currently be running in a syscall. */
static int number_of_threads_in_syscalls;

/* This is used to remember when a fork, vfork or exec event
   was caught by a catchpoint, and thus the event is to be
   followed at the next resume of the inferior, and not
   immediately. */
static struct
  {
    enum target_waitkind kind;
    struct
      {
	int parent_pid;
	int saw_parent_fork;
	int child_pid;
	int saw_child_fork;
	int saw_child_exec;
      }
    fork_event;
    char *execd_pathname;
  }
pending_follow;

/* Some platforms don't allow us to do anything meaningful with a
   vforked child until it has exec'd.  Vforked processes on such
   platforms can only be followed after they've exec'd.

   When this is set to 0, a vfork can be immediately followed,
   and an exec can be followed merely as an exec.  When this is
   set to 1, a vfork event has been seen, but cannot be followed
   until the exec is seen.

   (In the latter case, inferior_pid is still the parent of the
   vfork, and pending_follow.fork_event.child_pid is the child.  The
   appropriate process is followed, according to the setting of
   follow-fork-mode.) */
static int follow_vfork_when_exec;

/* JAGaf54650: Introducing a new follow-fork-mode "serial" to 
   debug child on fork and then re-attach to the parent for 
   debugging after the child's exit
 */
static const char follow_fork_mode_ask[] = "ask";
static const char follow_fork_mode_both[] = "both";
static const char follow_fork_mode_child[] = "child";
static const char follow_fork_mode_parent[] = "parent";
static const char follow_fork_mode_serial[] = "serial";

static const char *follow_fork_mode_kind_names[] =
{
  follow_fork_mode_ask,
  /* ??rehrauer: The "both" option is broken, by what may be a 10.20
     kernel problem.  It's also not terribly useful without a GUI to
     help the user drive two debuggers.  So for now, I'm disabling the
     "both" option. */
  /* follow_fork_mode_both, */
  follow_fork_mode_child,
  follow_fork_mode_parent,
  follow_fork_mode_serial, /* JAGaf54650 */
  NULL
};

const char *follow_fork_mode_string = follow_fork_mode_parent;
const char *follow_me;
int follow_mode_ask_flag = 1;

extern int trace_threads_in_this_run;
extern boolean fake_sig0_flag; /* Defined in infttrace.c */

/* JAGaf54650: Routines to push and pop the stack of parent pids
   for multiple level of forking. 
 */
void
push_ppid (int pid)
{
  char *filename = bfd_get_filename (exec_bfd);
  struct parent_pid_stack *temp
  = (struct parent_pid_stack *) xmalloc (sizeof (struct parent_pid_stack));

  temp->pid = get_pid_for (pid);
  strcpy (temp->proc_name, filename);
  temp->next = ppid_stack;
  ppid_stack = temp;
}

struct parent_pid_stack *
pop_ppid (void)
{
  struct parent_pid_stack *link = ppid_stack;
  if (link != NULL)
    ppid_stack = link->next; 
  return (link);
}

static void
follow_inferior_fork (int parent_pid, int child_pid, int has_forked,
		      int has_vforked)
{
  extern int check_heap_in_this_run, trace_threads_in_this_run;
  int saved_inferior_pid;
  int followed_parent = 0;
  int followed_child = 0;

  /* Which process did the user want us to follow? */
  const char *follow_mode = follow_fork_mode_string;

  /* Or, did the user not know, and want us to ask? */
  /* JAGae69495 - to implement 'set follow-fork-mode ask' - Kavitha*/
  if (follow_fork_mode_string == follow_fork_mode_ask)
    {
      follow_mode = follow_me;
    }

  /* JAGaf54650: If the follow-fork-mode is serial, push the parent
     pid after a fork and then proceed as if the follow-fork-mode
     is set to child. We will take care of the rest when we get 
     pre-exit event from the child.
   */
  if (follow_mode == follow_fork_mode_serial)
    {
      push_ppid (inferior_pid);
      follow_mode = follow_fork_mode_child;
    }

  /* If we're to be following the parent, then detach from child_pid.
     We're already following the parent, so need do nothing explicit
     for it. */
  if (follow_mode == follow_fork_mode_parent)
    {
      followed_parent = 1;

      /* Nullify LD_PRELOAD in the child to disable pre-loading of
         librtc. Since we're following the parent, there's no need
         to do heap/thread checking in the child. */
      /* Avoid the nullification for batch mode thread check so that
         the exec-ed binaries are also thread checked. */
      if ((!batch_rtc && trace_threads_in_this_run) || check_heap_in_this_run)
        {
          if (nullify_inferior_preload_env (child_pid))
            warning ("Can't nullify LD_PRELOAD in child's environment."
                     "Heap/thread checking will be enabled in the child process.");
        }

      /* We're already attached to the parent, by default. */

      /* Before detaching from the child, remove all breakpoints from
         it.  (This won't actually modify the breakpoint list, but will
         physically remove the breakpoints from the child.) */
      if (!has_vforked || !follow_vfork_when_exec)
	{
	  detach_breakpoints (child_pid);
          /* RM: the detach_breakpoints removes hardware watchpoints
             from the parent too (sorta). Reinsert here. */
	  /* Bindu 031402: For vfork, don't reinsert the breakpoints for
             the parent before we get the vfork event from the parent. As
             the parent and child share the same address space before exec,
             reinserting the breakpoints in the parent will amount to
             reinserting them in the child. */
	  if (!has_vforked && breakpoints_inserted)
            {
              saved_inferior_pid = inferior_pid;
              inferior_pid = parent_pid;
              remove_breakpoints();
              insert_breakpoints();
              inferior_pid = saved_inferior_pid;
            }
          
#ifdef SOLIB_REMOVE_INFERIOR_HOOK
	  SOLIB_REMOVE_INFERIOR_HOOK (child_pid);
#endif
	}

      /* Detach from the child. */
      dont_repeat ();

      target_require_detach (child_pid, "", 1);
    }

  /* If we're to be following the child, then attach to it, detach
     from inferior_pid, and set inferior_pid to child_pid. */
  else if (follow_mode == follow_fork_mode_child)
    {
      char child_pid_spelling[100];	/* Arbitrary length. */

      followed_child = 1;

      /* Before detaching from the parent, detach all breakpoints from
         the child.  But only if we're forking, or if we follow vforks
         as soon as they happen.  (If we're following vforks only when
         the child has exec'd, then it's very wrong to try to write
         back the "shadow contents" of inserted breakpoints now -- they
         belong to the child's pre-exec'd a.out.) */
      if (!has_vforked || !follow_vfork_when_exec)
	{
	  detach_breakpoints (child_pid);
          remove_breakpoints ();
          insert_breakpoints ();
	}

      /* Before detaching from the parent, remove all breakpoints from it. */
      remove_breakpoints ();

      /* Also reset the solib inferior hook from the parent. */
#ifdef SOLIB_REMOVE_INFERIOR_HOOK
      SOLIB_REMOVE_INFERIOR_HOOK (inferior_pid);
#endif

      /* Detach from the parent. */
      dont_repeat ();
      target_detach (NULL, 1);

      /* Attach to the child. */
      inferior_pid = child_pid;
      sprintf (child_pid_spelling, "%d", child_pid);
      dont_repeat ();

      target_require_attach (child_pid_spelling, 1);
      /* RM: we need notification of events from the child, no? */
      target_post_attach(child_pid);

      /* Was there a step_resume breakpoint?  (There was if the user
         did a "next" at the fork() call.)  If so, explicitly reset its
         thread number.

         step_resumes are a form of bp that are made to be per-thread.
         Since we created the step_resume bp when the parent process
         was being debugged, and now are switching to the child process,
         from the breakpoint package's viewpoint, that's a switch of
         "threads".  We must update the bp's notion of which thread
         it is for, or it'll be ignored when it triggers... */
      if (step_resume_breakpoint &&
	  (!has_vforked || !follow_vfork_when_exec))
	breakpoint_re_set_thread (step_resume_breakpoint);

      /* Reinsert all breakpoints in the child.  (The user may've set
         breakpoints after catching the fork, in which case those
         actually didn't get set in the child, but only in the parent.) */
      if (!has_vforked || !follow_vfork_when_exec)
	{
	  breakpoint_re_set ();
	  insert_breakpoints ();
	}
    }

  /* If we're to be following both parent and child, then fork ourselves,
     and attach the debugger clone to the child. */
  else if (follow_mode == follow_fork_mode_both)
    {
      char pid_suffix[100];	/* Arbitrary length. */

      /* Clone ourselves to follow the child.  This is the end of our
         involvement with child_pid; our clone will take it from here... */
      dont_repeat ();
      target_clone_and_follow_inferior (child_pid, &followed_child);
      followed_parent = !followed_child;

      /* We continue to follow the parent.  To help distinguish the two
         debuggers, though, both we and our clone will reset our prompts. */
      sprintf (pid_suffix, "[%d] ", inferior_pid);
      set_prompt (strcat (get_prompt (), pid_suffix));
    }

  /* The parent and child of a vfork share the same address space.
     Also, on some targets the order in which vfork and exec events
     are received for parent in child requires some delicate handling
     of the events.

     For instance, on ptrace-based HPUX we receive the child's vfork
     event first, at which time the parent has been suspended by the
     OS and is essentially untouchable until the child's exit or second
     exec event arrives.  At that time, the parent's vfork event is
     delivered to us, and that's when we see and decide how to follow
     the vfork.  But to get to that point, we must continue the child
     until it execs or exits.  To do that smoothly, all breakpoints
     must be removed from the child, in case there are any set between
     the vfork() and exec() calls.  But removing them from the child
     also removes them from the parent, due to the shared-address-space
     nature of a vfork'd parent and child.  On HPUX, therefore, we must
     take care to restore the bp's to the parent before we continue it.
     Else, it's likely that we may not stop in the expected place.  (The
     worst scenario is when the user tries to step over a vfork() call;
     the step-resume bp must be restored for the step to properly stop
     in the parent after the call completes!)

     Sequence of events, as reported to gdb from HPUX:

     Parent        Child           Action for gdb to take
     -------------------------------------------------------
     1                VFORK               Continue child
     2                EXEC
     3                EXEC or EXIT
     4  VFORK */
   
   /* Bindu 031402: For vfork, don't reinsert the breakpoints for
     the parent, before we get the vfork event from the parent. */
   if ((has_vforked) && (!STREQ(follow_mode, "parent")))
    {
      target_post_follow_vfork (parent_pid,
				followed_parent,
				child_pid,
				followed_child);
    }

  pending_follow.fork_event.saw_parent_fork = 0;
  pending_follow.fork_event.saw_child_fork = 0;

  /* JAGae69495 - to implement 'set follow-fork-mode ask' */
  if (follow_fork_mode_string == follow_fork_mode_ask)
    follow_mode_ask_flag = 1;
}

static void
follow_fork (int parent_pid, int child_pid)
{
  follow_inferior_fork (parent_pid, child_pid, 1, 0);
}


/* Forward declaration. */
static void follow_exec (int, char *);

static void
follow_vfork (int parent_pid, int child_pid)
{
  follow_inferior_fork (parent_pid, child_pid, 0, 1);

  /* Did we follow the child?  Had it exec'd before we saw the parent vfork? */
  if (pending_follow.fork_event.saw_child_exec &&
      (PIDGET (inferior_pid) == PIDGET (child_pid)))
    {
      pending_follow.fork_event.saw_child_exec = 0;
      pending_follow.kind = TARGET_WAITKIND_SPURIOUS;
      /* JAGaf54650: Before loading the symbol table for the child, 
	 defer the parent breakpoints. 
       */
      if (follow_fork_mode_string == follow_fork_mode_serial)
	defer_breakpoints ();
      follow_exec (inferior_pid, pending_follow.execd_pathname);
      free (pending_follow.execd_pathname);
    }
}

static void
follow_exec (int pid, char *execd_pathname)
{
  int saved_pid = pid;
  struct target_ops *tgt;

  if (!may_follow_exec)
    return;

  /* Did this exec() follow a vfork()?  If so, we must follow the
     vfork now too.  Do it before following the exec. */
  if (follow_vfork_when_exec &&
      (pending_follow.kind == TARGET_WAITKIND_VFORKED))
    {
      pending_follow.kind = TARGET_WAITKIND_SPURIOUS;
      follow_vfork (inferior_pid, pending_follow.fork_event.child_pid);
      follow_vfork_when_exec = 0;
      saved_pid = inferior_pid;

      /* Did we follow the parent?  If so, we're done.  If we followed
         the child then we must also follow its exec(). */
      if (PIDGET(inferior_pid) == PIDGET(pending_follow.fork_event.parent_pid))
	return;
    }

  /* This is an exec event that we actually wish to pay attention to.
     Refresh our symbol table to the newly exec'd program, remove any
     momentary bp's, etc.

     If there are breakpoints, they aren't really inserted now,
     since the exec() transformed our inferior into a fresh set
     of instructions.

     We want to preserve symbolic breakpoints on the list, since
     we have hopes that they can be reset after the new a.out's
     symbol table is read.

     However, any "raw" breakpoints must be removed from the list
     (e.g., the solib bp's), since their address is probably invalid
     now.

     And, we DON'T want to call delete_breakpoints() here, since
     that may write the bp's "shadow contents" (the instruction
     value that was overwritten witha TRAP instruction).  Since
     we now have a new a.out, those shadow contents aren't valid. */
  update_breakpoints_after_exec ();

  /* If there was one, it's gone now.  We cannot truly step-to-next
     statement through an exec(). */
  step_resume_breakpoint = NULL;
  step_range_start = 0;
  step_range_end = 0;

  /* If there was one, it's gone now. */
  through_sigtramp_breakpoint = NULL;

  /* What is this a.out's name? */
  printf_unfiltered ("Executing new program: %s\n", execd_pathname);

  /* We've followed the inferior through an exec.  Therefore, the
     inferior has essentially been killed & reborn. */

  /* First collect the run target in effect.  */
  tgt = find_run_target ();
  /* If we can't find one, things are in a very strange state...  */
  if (tgt == NULL)
    error ("Could find run target to save before following exec");

  gdb_flush (gdb_stdout);
  target_mourn_inferior ();
  inferior_pid = saved_pid;    /* Because mourn_inferior resets inferior_pid. */
#ifdef HP_IA64_GAMBIT
  /*ia64-gambit: Bob Zulawnik's comment below:
   * Tahoe - no child_ops allowed for remote build - defined in inftarg.c
   * which is a NATEDPFILE, so it breaks remote build.
   * FIXME !!!
   * push_target (&child_ops);
   */
#else
  push_target (tgt);
#endif

  /* Reset some part of the shared library package */
#if defined(SOLIB_RESTART)
  SOLIB_RESTART ();
#endif

  /* srikanth, 991105, shouldn't we go into a spell of amnesia here ?? */
  objfile_purge_solibs ();

  /* That a.out is now the one to use. */
  exec_file_attach (execd_pathname, 0);

  /* And also is where symbols can be found. */
  symbol_file_command (execd_pathname, 0);

#ifdef RTC
  /* Reset all the globals to prepare for thread tracing. */
  prepare_for_thread_tracing(0);  /* Not attached to */
  /* Reset all the globals to prepare for runtime checking. */
  prepare_for_rtc (0);  /* Not attached to */
#endif

  /* Reset the shared library package.  This ensures that we get
     a shlib event when the child reaches "_start", at which point
     the dld will have had a chance to initialize the child. */
#ifdef SOLIB_CREATE_INFERIOR_HOOK
  SOLIB_CREATE_INFERIOR_HOOK (inferior_pid);
#endif

  /* Reinsert all breakpoints.  (Those which were symbolic have
     been reset to the proper address in the new a.out, thanks
     to symbol_file_command...) */
  insert_breakpoints ();

 /* After the exec, if we are following a libjvm loaded process,
     and java_debugging is not yet set (i.e., we have not yet 
     reached the __gdb_jave_breakpoint) then re-establish the
     checking for libjvm. */
  if (!java_debugging && gdb_java_breakpoint_addr)
    {
      libjvm_load_hook = libjvm_got_loaded;
      gdb_java_breakpoint_addr = 0;
    }

  /* The next resume of this inferior should bring it to the shlib
     startup breakpoints.  (If the user had also set bp's on
     "main" from the old (parent) process, then they'll auto-
     matically get reset there in the new process.) */
}

/* Non-zero if we just simulating a single-step.  This is needed
   because we cannot remove the breakpoints in the inferior process
   until after the `wait' in `wait_for_inferior'.  */
static int singlestep_breakpoints_inserted_p = 0;


/* Things to clean up if we QUIT out of resume ().  */
/* ARGSUSED */
static void
resume_cleanups (void *ignore)
{
  normal_stop ();
}

static const char schedlock_off[] = "off";
static const char schedlock_on[] = "on";
static const char schedlock_step[] = "step";
static const char *scheduler_mode = schedlock_off;
static const char *scheduler_enums[] =
{
  schedlock_off,
  schedlock_on,
  schedlock_step,
  NULL
};

static void
set_schedlock_func (char *args, int from_tty, struct cmd_list_element *c)
{
  if (c->type == set_cmd)
    if (!target_can_lock_scheduler)
      {
	scheduler_mode = schedlock_off;
	error ("Target '%s' cannot support this command.",
	       target_shortname);
      }
}




/* Resume the inferior, but allow a QUIT.  This is useful if the user
   wants to interrupt some lengthy single-stepping operation
   (for child processes, the SIGINT goes to the inferior, and so
   we get a SIGINT random_signal, but for remote debugging and perhaps
   other targets, that's not true).

   STEP nonzero if we should step (zero to continue instead).
   SIG is the signal to give the inferior (zero for none).  */
void
resume (int step, enum target_signal sig)
{
  int should_resume = 1;
  struct cleanup *old_cleanups = make_cleanup (resume_cleanups, 0);
  QUIT;

#ifdef CANNOT_STEP_BREAKPOINT
  /* Most targets can step a breakpoint instruction, thus executing it
     normally.  But if this one cannot, just continue and we will hit
     it anyway.  */
  if (step && breakpoints_inserted && breakpoint_here_p (read_pc ()))
    step = 0;
#endif

  /* Some targets (e.g. Solaris x86) have a kernel bug when stepping
     over an instruction that causes a page fault without triggering
     a hardware watchpoint. The kernel properly notices that it shouldn't
     stop, because the hardware watchpoint is not triggered, but it forgets
     the step request and continues the program normally.
     Work around the problem by removing hardware watchpoints if a step is
     requested, GDB will check for a hardware watchpoint trigger after the
     step anyway.  */
  if (CANNOT_STEP_HW_WATCHPOINTS && step && breakpoints_inserted)
    remove_hw_watchpoints ();
     

  /* Normally, by the time we reach `resume', the breakpoints are either
     removed or inserted, as appropriate.  The exception is if we're sitting
     at a permanent breakpoint; we need to step over it, but permanent
     breakpoints can't be removed.  So we have to test for it here.  */
  if (breakpoint_here_p (read_pc ()) == permanent_breakpoint_here)
    SKIP_PERMANENT_BREAKPOINT ();

  if (SOFTWARE_SINGLE_STEP_P && step)
    {
      /* Do it the hard way, w/temp breakpoints */
      SOFTWARE_SINGLE_STEP (sig, 1 /*insert-breakpoints */ );
      /* ...and don't ask hardware to do it.  */
      step = 0;
      /* and do not pull these breakpoints until after a `wait' in
         `wait_for_inferior' */
      singlestep_breakpoints_inserted_p = 1;
    }

  /* Handle any optimized stores to the inferior NOW...  */
#ifdef DO_DEFERRED_STORES
  DO_DEFERRED_STORES;
#endif

  /* If there were any forks/vforks/execs that were caught and are
     now to be followed, then do so. */
  switch (pending_follow.kind)
    {
    case (TARGET_WAITKIND_FORKED):
      pending_follow.kind = TARGET_WAITKIND_SPURIOUS;
      follow_fork (inferior_pid, pending_follow.fork_event.child_pid);
      break;

    case (TARGET_WAITKIND_VFORKED):
      {
	int saw_child_exec = pending_follow.fork_event.saw_child_exec;

	pending_follow.kind = TARGET_WAITKIND_SPURIOUS;
	follow_vfork (inferior_pid, pending_follow.fork_event.child_pid);

	/* Did we follow the child, but not yet see the child's exec event?
	   If so, then it actually ought to be waiting for us; we respond to
	   parent vfork events.  We don't actually want to resume the child
	   in this situation; we want to just get its exec event. */
	if (!saw_child_exec &&
	    (PIDGET (inferior_pid) ==
	     PIDGET (pending_follow.fork_event.child_pid)))
	  should_resume = 0;
      }
      break;

    case (TARGET_WAITKIND_EXECD):
      /* If we saw a vfork event but couldn't follow it until we saw
         an exec, then now might be the time! */
      pending_follow.kind = TARGET_WAITKIND_SPURIOUS;
      /* follow_exec is called as soon as the exec event is seen. */
      break;

    default:
      break;
    }

  /* Install inferior's terminal modes.  */
  target_terminal_inferior ();

  if (should_resume)
    {
      int resume_pid;

      if (use_thread_step_needed && thread_step_needed)
	{
	  /* We stopped on a BPT instruction;
	     don't continue other threads and
	     just step this thread. */
	  thread_step_needed = 0;

	  if (!breakpoint_here_p (read_pc ()))
	    {
	      /* Breakpoint deleted: ok to do regular resume
	         where all the threads either step or continue. */
	      resume_pid = -1;
	    }
	  else
	    {
	      if (!step)
		{
		  remove_breakpoints ();
		  breakpoints_inserted = 0;
		  trap_expected = 1;
		  step = 1;
		}
	      resume_pid = inferior_pid;
	    }
	}
      else
	{
	  /* Vanilla resume. */
	  if ((scheduler_mode == schedlock_on) ||
	      (scheduler_mode == schedlock_step && step != 0))
	    resume_pid = inferior_pid;
	  else
	    resume_pid = -1;
	}
      target_resume (resume_pid, step, sig);
    }

  discard_cleanups (old_cleanups);
}


/* Clear out all variables saying what to do when inferior is continued.
   First do this, then set the ones you want, then call `proceed'.  */

void
clear_proceed_status (void)
{
  trap_expected = 0;
  step_range_start = 0;
  step_range_end = 0;
  step_frame_address = 0;
#ifdef REGISTER_STACK_ENGINE_FP
  step_frame_rse_addr= 0;
#endif
  step_over_calls = -1;
  stop_after_trap = 0;
  stop_soon_quietly = 0;
  proceed_to_finish = 0;
  breakpoint_proceeded = 1;	/* We're about to proceed... */

  /* Discard any remaining commands or status from previous stop.  */
  bpstat_clear (&stop_bpstat);
}

/* Basic routine for continuing the program in various fashions.

   ADDR is the address to resume at, or -1 for resume where stopped.
   SIGGNAL is the signal to give it, or 0 for none,
   or -1 for act according to how it stopped.
   STEP is nonzero if should trap after one instruction.
   -1 means return after that and print nothing.
   You should probably set various step_... variables
   before calling here, if you are stepping.

   You should call clear_proceed_status before calling proceed.  */

void
proceed (CORE_ADDR addr, enum target_signal siggnal, int step)
{
  int oneproc = 0;

#ifdef FIND_ACTIVE_THREAD
  inferior_pid = FIND_ACTIVE_THREAD ();
#endif

  if (step > 0)
    {
      step_start_function = find_pc_function (read_pc ());
#if defined (INLINE_SUPPORT)
      step_start_function_inline_index = get_current_frame()->inline_idx;
#endif
    }
  else
    step_start_function_inline_index = -1;

  if (step < 0)
    stop_after_trap = 1;

  if (addr == (CORE_ADDR)(long) -1)
    {
      /* If there is a breakpoint at the address we will resume at,
         step one instruction before inserting breakpoints
         so that we do not stop right away (and report a second
         hit at this breakpoint).  */

      if (read_pc () == stop_pc && breakpoint_here_p (read_pc ()))
	oneproc = 1;

#ifndef STEP_SKIPS_DELAY
#define STEP_SKIPS_DELAY(pc) (0)
#define STEP_SKIPS_DELAY_P (0)
#endif
      /* Check breakpoint_here_p first, because breakpoint_here_p is fast
         (it just checks internal GDB data structures) and STEP_SKIPS_DELAY
         is slow (it needs to read memory from the target).  */
      if (STEP_SKIPS_DELAY_P
	  && breakpoint_here_p (read_pc () + 4)
	  && STEP_SKIPS_DELAY (read_pc ()))
	oneproc = 1;
    }
  else
    {
      write_pc (addr);

      /* New address; we don't need to single-step a thread
         over a breakpoint we just hit, 'cause we aren't
         continuing from there.

         It's not worth worrying about the case where a user
         asks for a "jump" at the current PC--if they get the
         hiccup of re-hiting a hit breakpoint, what else do
         they expect? */
      thread_step_needed = 0;
    }

#ifdef PREPARE_TO_PROCEED
  /* In a multi-threaded task we may select another thread
     and then continue or step.

     But if the old thread was stopped at a breakpoint, it
     will immediately cause another breakpoint stop without
     any execution (i.e. it will report a breakpoint hit
     incorrectly).  So we must step over it first.

     PREPARE_TO_PROCEED checks the current thread against the thread
     that reported the most recent event.  If a step-over is required
     it returns TRUE and sets the current thread to the old thread. */
  if (PREPARE_TO_PROCEED (1) && breakpoint_here_p (read_pc ()))
    {
      oneproc = 1;
      thread_step_needed = 1;
    }

#endif /* PREPARE_TO_PROCEED */

#ifdef HP_OS_BUG
  if (trap_expected_after_continue)
    {
      /* If (step == 0), a trap will be automatically generated after
         the first instruction is executed.  Force step one
         instruction to clear this condition.  This should not occur
         if step is nonzero, but it is harmless in that case.  */
      oneproc = 1;
      trap_expected_after_continue = 0;
    }
#endif /* HP_OS_BUG */

  if (step_before_continuing)
    {
      /* Continuing after syscall return. First single step. Do not pass
	 siggnal to the process as there is no real signal here. */
      step = 1;
      trap_expected_after_step = 1;
      siggnal = TARGET_SIGNAL_0;
      step_before_continuing = 0;
    }

  if (oneproc)
    /* We will get a trace trap after one instruction.
       Continue it automatically and insert breakpoints then.  */
    trap_expected = 1;
  else
    {
      extern struct breakpoint *breakpoint_chain;

      int temp = insert_breakpoints ();
      if (temp)
	{
	  print_sys_errmsg ("insert_breakpoints", temp);
	  error ("Cannot insert breakpoints.\n\
The same program may be running in another process,\n\
or you may have requested too many hardware\n\
breakpoints and/or watchpoints.\n");
	}

      breakpoints_inserted = 1;
    }

  if (siggnal != TARGET_SIGNAL_DEFAULT)
    stop_signal = siggnal;
  /* If this signal should not be seen by program,
     give it zero.  Used for debugging signals.  */
  else if (!signal_program[stop_signal])
    stop_signal = TARGET_SIGNAL_0;

  annotate_starting ();

  /* Make sure that output from GDB appears before output from the
     inferior.  */
  gdb_flush (gdb_stdout);

  /* Resume inferior.  */
  resume (oneproc || step || bpstat_should_step (), stop_signal);

  /* Srikanth, catch errors here if profile on, there is really
     no legitimate case, where we can longjmp across this call.
     In particular, we should not be returning to gdb prompt
     while profiling.
  */
  if (profile_on)
    error_hook = error_while_profiling;

  /* Wait for it to stop (if not standalone)
     and in any case decode why it stopped, and act accordingly.  */
  /* Do this only if we are not using the event loop, or if the target
     does not support asynchronous execution. */
  if (!event_loop_p || !target_can_async_p ())
    {
      wait_for_inferior ();

      /* JAGaf54650: attach_stopped is just a flag that conveys that
	 normal_stop() has already happened through attach_command()
	 itself when attached back to the parent after child's exit.	
	 So, no need to bother on normal_stop here.
       */
      /* Do not normal_stop if performing stepi in sigtramp. */
      if (!attach_stopped && !sigtramp_stepi_again)
        normal_stop ();
      else
        {
          attach_stopped = 0;
          single_stepping = true;
        }
    }
}

/* Record the pc and sp of the program the last time it stopped.
   These are just used internally by wait_for_inferior, but need
   to be preserved over calls to it and cleared when the inferior
   is started.  
   
   990210 Made prev_pc global so we could handle purify calls for PA64.
          at_purify_call needs to know prev_pc, but it is called from
          SAVED_PC_AFTER_CALL which is not passed prev_pc.
          -- Michael Coulter */

CORE_ADDR prev_pc;
static CORE_ADDR prev_func_start;
static char *prev_func_name;
CORE_ADDR return_pc;
#ifdef USE_RETURN_POINTER_TO_CHECK_FOR_SIGTRAMP 
char *return_func_name;
struct frame_info *frame;
#endif


/* Start remote-debugging of a machine over a serial link.  */

void
start_remote (void)
{
  init_thread_list ();
  init_wait_for_inferior ();
  stop_soon_quietly = 1;
  trap_expected = 0;

  /* Always go on waiting for the target, regardless of the mode. */
  /* FIXME: cagney/1999-09-23: At present it isn't possible to
     indicate th wait_for_inferior that a target should timeout if
     nothing is returned (instead of just blocking).  Because of this,
     targets expecting an immediate response need to, internally, set
     things up so that the target_wait() is forced to eventually
     timeout. */
  /* FIXME: cagney/1999-09-24: It isn't possible for target_open() to
     differentiate to its caller what the state of the target is after
     the initial open has been performed.  Here we're assuming that
     the target has stopped.  It should be possible to eventually have
     target_open() return to the caller an indication that the target
     is currently running and GDB state should be set to the same as
     for an async run. */
  wait_for_inferior ();
#ifndef HP_IA64
  /* RM: I want the gambit based IA64 debugger interface to look just
     like a native debugger interface, so no stops till the first
     breakpoint or signal. */
  normal_stop ();
#endif
}

/* Initialize static vars when a new inferior begins.  */

void
init_wait_for_inferior (void)
{
  pending_bor_event_t * next_bor_event;

  /* These are meaningless until the first time through wait_for_inferior.  */
  prev_pc = 0;
  prev_func_start = 0;
  prev_func_name = NULL;

#ifdef HP_OS_BUG
  trap_expected_after_continue = 0;
#endif
  breakpoints_inserted = 0;
  breakpoint_init_inferior (inf_starting);

  /* Don't confuse first call to proceed(). */
  stop_signal = TARGET_SIGNAL_0;

  /* The first resume is not following a fork/vfork/exec. */
  pending_follow.kind = TARGET_WAITKIND_SPURIOUS;	/* I.e., none. */
  pending_follow.fork_event.saw_parent_fork = 0;
  pending_follow.fork_event.saw_child_fork = 0;
  pending_follow.fork_event.saw_child_exec = 0;

  /* See wait_for_inferior's handling of SYSCALL_ENTRY/RETURN events. */
  number_of_threads_in_syscalls = 0;

  clear_proceed_status ();

  while (bor_events_pending)
    {
      next_bor_event = bor_events_pending->next;
      free (bor_events_pending);
      bor_events_pending = next_bor_event;
    }
} /* end init_wait_for_inferior */

static void
delete_breakpoint_current_contents (void *arg)
{
  struct breakpoint **breakpointp = (struct breakpoint **) arg;
  if (*breakpointp != NULL)
    {
      delete_breakpoint (*breakpointp);
      *breakpointp = NULL;
    }
}

/* This enum encodes possible reasons for doing a target_wait, so that
   wfi can call target_wait in one place.  (Ultimately the call will be
   moved out of the infinite loop entirely.) */

/* srikanth, introduced a new reason for doing a wait. When the debuggee
   process forks, two fork events are generated, one for the parent and
   one for the child. These could come in any order. Having seen one we
   need to retrieve the other. If we pass a waiton_pid of -1 to wait,
   meaning retreive the event from either process, HP-UX 10.20 keeps
   returning the same event over and over. This is not a problem on
   11.00 where there is a notion of whether an event has been waited
   for. When we have seen the parent's fork and not the child's fork,
   we need to wait specifically for the child and vice versa.
*/
   
enum infwait_states
{
  infwait_normal_state,
  infwait_isc_normal_state,
  infwait_isc_state,
  infwait_thread_hop_state,
  infwait_nullified_state,
  infwait_nonstep_watch_state,
  infwait_retrieve_other_fork_state
};

/* This enum encodes possible states for doing a 'stepping out of syscall',
   so that wfi can handle stepping out of syscall for fork/vfork/exec */

enum isc_states
{
  isc_neither_state,
  isc_both_state,
  isc_parent_state
};

/* Why did the inferior stop? Used to print the appropriate messages
   to the interface from within handle_inferior_event(). */
enum inferior_stop_reason
{
  /* We don't know why. */
  STOP_UNKNOWN,
  /* Step, next, nexti, stepi finished. */
  END_STEPPING_RANGE,
  /* Found breakpoint. */
  BREAKPOINT_HIT,
  /* Inferior terminated by signal. */
  SIGNAL_EXITED,
  /* Inferior exited. */
  EXITED,
  /* Inferior received signal, and user asked to be notified. */
  SIGNAL_RECEIVED
};

/* This structure contains what used to be local variables in
   wait_for_inferior.  Probably many of them can return to being
   locals in handle_inferior_event.  */

struct execution_control_state
  {
    struct target_waitstatus ws;
    struct target_waitstatus *wp;
    int another_trap;
    CORE_ADDR saved_step_range_start, saved_step_range_end;
    struct minimal_symbol *resume_stepping_when_loaded;
    int random_signal;
    CORE_ADDR stop_func_start;
    CORE_ADDR stop_func_end;
    char *stop_func_name;
    struct symtab_and_line sal;
    int remove_breakpoints_on_following_step;
    int current_line;
    struct symtab *current_symtab;
    int handling_longjmp;	/* FIXME */
    int pid;
    int saved_inferior_pid;
    int update_step_sp;
    int stepping_through_solib_after_catch;
    bpstat stepping_through_solib_catchpoints;
    int enable_hw_watchpoints_after_wait;
    int stepping_through_sigtramp;
    int new_thread_event;
    struct target_waitstatus tmpstatus;
    enum infwait_states infwait_state;
    enum isc_states isc_state;
    int waiton_pid;
    int wait_some_more;
  };

void init_execution_control_state (struct execution_control_state * ecs);

void handle_inferior_event (struct execution_control_state * ecs);

static void check_sigtramp2 (struct execution_control_state *ecs);
static void step_into_function (struct execution_control_state *ecs);
static void step_over_function (struct execution_control_state *ecs);
static void stop_stepping (struct execution_control_state *ecs);
static void prepare_to_wait (struct execution_control_state *ecs);
static void keep_going (struct execution_control_state *ecs);
static void print_stop_reason (enum inferior_stop_reason stop_reason, int stop_info);

/* Set a step-breakpoint at break_addr */

void set_step_breakpoint (CORE_ADDR break_addr, 
			  struct execution_control_state *ecs)
{
  struct symtab_and_line sr_sal;

  INIT_SAL (&sr_sal);	/* initialize to zeroes */
  sr_sal.pc = break_addr;
  sr_sal.section = find_pc_overlay (break_addr);
  /* Do not specify what the fp should be when we stop
     since on some machines the prologue
     is where the new fp value is established.  */
  step_resume_breakpoint =
    set_momentary_breakpoint (sr_sal, NULL, bp_step_resume);
  if (breakpoints_inserted)
    insert_breakpoints ();

  /* And make sure stepping stops right away then.  */
  ecs->saved_step_range_start = ecs->saved_step_range_end 
      = step_range_start = step_range_end = stop_pc;
} /* end set_step_breakpoint */

int in_startup_no_prev_fp = 0;
int in_startup_starting_shell = 0;

/* Wait for control to return from inferior to debugger.
   If inferior gets a signal, we may decide to start it up again
   instead of returning.  That is why there is a loop in this function.
   When this function actually returns it means the inferior
   should be left stopped and GDB should read more commands.  */

void
wait_for_inferior (void)
{
  struct cleanup *old_cleanups;
  struct execution_control_state ecss;
  struct execution_control_state *ecs;

  char *follow_mode =
    savestring (follow_fork_mode_string, (int) strlen (follow_fork_mode_string));

  old_cleanups = make_cleanup (delete_breakpoint_current_contents,
			       &step_resume_breakpoint);
  make_cleanup (delete_breakpoint_current_contents,
		&through_sigtramp_breakpoint);

  /* wfi still stays in a loop, so it's OK just to take the address of
     a local to get the ecs pointer.  */
  ecs = &ecss;

  /* Fill in with reasonable starting values.  */
  init_execution_control_state (ecs);

  thread_step_needed = 0;

  /* We'll update this if & when we switch to a new thread. */
  previous_inferior_pid = inferior_pid;

  overlay_cache_invalid = 1;

  /* We have to invalidate the registers BEFORE calling target_wait
     because they can be loaded from the target while in target_wait.
     This makes remote debugging a bit more efficient for those
     targets that provide critical registers as part of their normal
     status mechanism. */

  registers_changed ();

  while (1)
    {
      if (target_wait_hook)
	ecs->pid = target_wait_hook (ecs->waiton_pid, ecs->wp);
      else
	ecs->pid = target_wait (ecs->waiton_pid, ecs->wp);

      /* srikanth, 000818, Force a thread switch if inferior pid is
         missing. This can happen if wfi is working with inferior_pid
         and the next event gets reported for pid and in between old
         thread exited. */
      if (!target_thread_alive (inferior_pid))
        {
	  load_infrun_state (ecs->pid, &prev_pc,
			     &prev_func_start, &prev_func_name,
			     &trap_expected, &step_resume_breakpoint,
			     &through_sigtramp_breakpoint,
#ifdef REGISTER_STACK_ENGINE_FP
                             &step_frame_rse_addr,
#endif
			     &step_range_start, &step_range_end,
			     &step_frame_address, &ecs->handling_longjmp,
			     &ecs->another_trap,
			     &ecs->stepping_through_solib_after_catch,
			     &ecs->stepping_through_solib_catchpoints,
			     &ecs->stepping_through_sigtramp);
	  previous_inferior_pid = inferior_pid;
	  inferior_pid = ecs->pid;
	  flush_cached_frames ();
	  registers_changed ();
	}
#ifdef FIND_ACTIVE_THREAD
      /* For kernel threads, FIND_ACTIVE_THREAD returns inferior_pid.
         For kernel threads, we don't want to set pid to inferior_pid because
         we will not inform the user of a thread switch. */
      {
        int thread_pid;

        thread_pid = FIND_ACTIVE_THREAD ();
        if (PIDGET(thread_pid) == ecs->pid)
          ecs->pid = thread_pid;
      }
#endif

      /* Now figure out what to do with the result of the result.  */
      handle_inferior_event (ecs);
      /* sigtramp_stepi_again tells us whether we are stepping through sigtramp, if so
         we need to break and do another stepi and ttrace_wait for single-step trap. */
      if (!ecs->wait_some_more || sigtramp_stepi_again)
	break;
    }
  do_cleanups (old_cleanups);
  free (follow_mode);
}

/* Asynchronous version of wait_for_inferior. It is called by the
   event loop whenever a change of state is detected on the file
   descriptor corresponding to the target. It can be called more than
   once to complete a single execution command. In such cases we need
   to keep the state in a global variable ASYNC_ECSS. If it is the
   last time that this function is called for a single execution
   command, then report to the user that the inferior has stopped, and
   do the necessary cleanups. */

struct execution_control_state async_ecss;
struct execution_control_state *async_ecs;

void
fetch_inferior_event (void *client_data)
{
  static struct cleanup *old_cleanups;

  async_ecs = &async_ecss;

  if (!async_ecs->wait_some_more)
    {
      old_cleanups = make_exec_cleanup (delete_breakpoint_current_contents,
					&step_resume_breakpoint);
      make_exec_cleanup (delete_breakpoint_current_contents,
			 &through_sigtramp_breakpoint);

      /* Fill in with reasonable starting values.  */
      init_execution_control_state (async_ecs);

      thread_step_needed = 0;

      /* We'll update this if & when we switch to a new thread. */
      previous_inferior_pid = inferior_pid;

      overlay_cache_invalid = 1;

      /* We have to invalidate the registers BEFORE calling target_wait
         because they can be loaded from the target while in target_wait.
         This makes remote debugging a bit more efficient for those
         targets that provide critical registers as part of their normal
         status mechanism. */

      registers_changed ();
    }

  if (target_wait_hook)
    async_ecs->pid = target_wait_hook (async_ecs->waiton_pid, async_ecs->wp);
  else
    async_ecs->pid = target_wait (async_ecs->waiton_pid, async_ecs->wp);

  /* Now figure out what to do with the result of the result.  */
  handle_inferior_event (async_ecs);

  if (!async_ecs->wait_some_more)
    {
      /* Do only the cleanups that have been added by this
	 function. Let the continuations for the commands do the rest,
	 if there are any. */
      do_exec_cleanups (old_cleanups);
      normal_stop ();
      if (step_multi && stop_step)
	inferior_event_handler (INF_EXEC_CONTINUE, NULL);
      else
	inferior_event_handler (INF_EXEC_COMPLETE, NULL);
    }
}

/* Prepare an execution control state for looping through a
   wait_for_inferior-type loop.  */

void
init_execution_control_state (struct execution_control_state *ecs)
{
  /* ecs->another_trap? */
  ecs->saved_step_range_start = NULL;
  ecs->saved_step_range_end = NULL;
  ecs->resume_stepping_when_loaded = NULL;
  ecs->random_signal = 0;
  ecs->remove_breakpoints_on_following_step = 0;
  ecs->handling_longjmp = 0;	/* FIXME */
  ecs->update_step_sp = 0;
  ecs->stepping_through_solib_after_catch = 0;
  ecs->stepping_through_solib_catchpoints = NULL;
  ecs->enable_hw_watchpoints_after_wait = 0;
  ecs->stepping_through_sigtramp = 0;
  ecs->sal = find_pc_line (prev_pc, 0);
  ecs->current_line = ecs->sal.line;
  ecs->current_symtab = ecs->sal.symtab;
  ecs->infwait_state = infwait_normal_state;
  ecs->isc_state = isc_neither_state;
  ecs->waiton_pid = -1;
  ecs->wp = &(ecs->ws);
}

/* Call this function before setting step_resume_breakpoint, as a
   sanity check.  There should never be more than one step-resume
   breakpoint per thread, so we should never be setting a new
   step_resume_breakpoint when one is already active.  */
static void
check_for_old_step_resume_breakpoint (void)
{
  if (step_resume_breakpoint)
    warning ("GDB bug: infrun.c (wait_for_inferior): dropping old step_resume breakpoint");
}

/* Given an execution control state that has been freshly filled in
   by an event from the inferior, figure out what it means and take
   appropriate action.  */

boolean do_fake_detach = 0;
void
handle_inferior_event (struct execution_control_state *ecs)
{
  CORE_ADDR tmp;
  CORE_ADDR prev_stop_func_start = 0;
  int stepped_after_stopped_by_watchpoint = 0;
  int forking_pid;
  int real_inferior_pid;
#ifdef DYNLINK_HAS_BREAK_HOOK 
  int stop_for_event;
#endif
  const char *follow_mode = follow_fork_mode_string;

  /* JAGae69495 - to implement 'set follow-fork-mode ask' */
  if (follow_fork_mode_string == follow_fork_mode_ask)
    follow_mode = follow_me;

  switch (ecs->infwait_state)
    {
      case infwait_normal_state:
      case infwait_isc_normal_state:
	/* Since we've done a wait, we have a new event.  Don't
	   carry over any expectations about needing to step over a
	   breakpoint. */
	thread_step_needed = 0;

	/* See comments where a TARGET_WAITKIND_SYSCALL_RETURN event
	   is serviced in this loop, below. */
	if (ecs->enable_hw_watchpoints_after_wait)
	  {
	    TARGET_ENABLE_HW_WATCHPOINTS (inferior_pid);
	    ecs->enable_hw_watchpoints_after_wait = 0;
	  }
	stepped_after_stopped_by_watchpoint = 0;
	break;

      case infwait_isc_state:
	if (ecs->enable_hw_watchpoints_after_wait)
	  {
	    TARGET_ENABLE_HW_WATCHPOINTS (inferior_pid);
	    ecs->enable_hw_watchpoints_after_wait = 0;
	  }
	ecs->infwait_state = infwait_normal_state;
	goto process_event_stop_test;

      case infwait_thread_hop_state:
	insert_breakpoints ();

	/* We need to restart all the threads now,
	 * unles we're running in scheduler-locked mode. 
	 * FIXME: shouldn't we look at currently_stepping ()?
	 */
	if (scheduler_mode == schedlock_on)
	  target_resume (ecs->pid, 0, TARGET_SIGNAL_0);
	else
	  target_resume (-1, 0, TARGET_SIGNAL_0);
	ecs->infwait_state = infwait_normal_state;
	prepare_to_wait (ecs);
	return;

      case infwait_nullified_state:
	break;

      case infwait_nonstep_watch_state:
	insert_breakpoints ();

	/* FIXME-maybe: is this cleaner than setting a flag?  Does it
	   handle things like signals arriving and other things happening
	   in combination correctly?  */
	stepped_after_stopped_by_watchpoint = 1;
	break;
    } /* switch (ecs->infwait_state) */
 
  ecs->infwait_state = infwait_normal_state;

  flush_cached_frames ();

  /* JAGae69495 - to implement 'set follow-fork-mode ask' */
  if (follow_fork_mode_string == follow_fork_mode_ask)
    {
      if (((ecs->ws.kind == TARGET_WAITKIND_FORKED) && follow_mode_ask_flag)
          || ((ecs->ws.kind == TARGET_WAITKIND_VFORKED) && vfork_in_flight))
      {
  	 char *requested_mode;

  	 printf_filtered ("Select follow-fork-mode: \n");
  	 printf_filtered ("[0] parent\n[1] child\n");

  	 target_terminal_ours_for_output ();
  	 requested_mode = command_line_input ("> ", 0, "follow-fork-mode");
  	 target_terminal_inferior ();

  	 follow_me = atoi(requested_mode) ? follow_fork_mode_child :
				     follow_fork_mode_parent;
	 follow_mode = follow_me;
	 follow_mode_ask_flag = 0;
      } 
    }

  /* If it's a new process, add it to the thread database */

  ecs->new_thread_event = (   (ecs->pid != PIDGET (inferior_pid))
                           && !in_thread_list (ecs->pid));

  if (   ecs->ws.kind != TARGET_WAITKIND_EXITED
      && ecs->ws.kind != TARGET_WAITKIND_SIGNALLED
      && ecs->new_thread_event)
    {
      add_thread (ecs->pid);
 
      if (info_threadverbose && !profile_on) /* Shut up if profiling */
        {
#ifdef UI_OUT
 	  ui_out_text (uiout, "[New ");
	  ui_out_text (uiout, target_pid_or_tid_to_str (ecs->pid));
	  ui_out_text (uiout, "]\n");
#else
	  printf_filtered ("[New %s]\n",
			   target_pid_or_tid_to_str (ecs->pid));
#endif
        }
    }

  switch (ecs->ws.kind)
    {
      case TARGET_WAITKIND_LOADED:
	/* Ignore gracefully during startup of the inferior, as it
	   might be the shell which has just loaded some objects,
	   otherwise add the symbols for the newly loaded objects.  */
#ifdef SOLIB_ADD
	if (!stop_soon_quietly)
	  {
	    /* Remove breakpoints, SOLIB_ADD might adjust
	       breakpoint addresses via breakpoint_re_set.  */
	    if (breakpoints_inserted)
	      remove_breakpoints ();

	    /* Check for any newly added shared libraries if we're
	       supposed to be adding them automatically.  */
	    if (auto_solib_add)
	      {
		/* Switch terminal for any messages produced by
		   breakpoint_re_set.  */
		target_terminal_ours_for_output ();
		SOLIB_ADD (NULL, 0, NULL);
#ifdef RTC
		snoop_on_the_heap ();
#endif
		target_terminal_inferior ();
	      }

	    /* Reinsert breakpoints and continue.  */
	    if (breakpoints_inserted)
	      insert_breakpoints ();
	  }
#endif
	resume (0, TARGET_SIGNAL_0);
	prepare_to_wait (ecs);
	return;

      case TARGET_WAITKIND_SPURIOUS:
	target_resume (-1, 0, TARGET_SIGNAL_0);
	prepare_to_wait (ecs);
	return;

      case TARGET_WAITKIND_PRE_EXIT:
      /* Kavitha, JAGaf54650:
	 Ah, this is the happening place for debugging parent and
	 child serially on fork or vfork.

	 (1) Remove the hooks and breakpoints before detaching the child.
	 (2) Do a fake detach. That is, clear up all datastructures of
	 the inferior to look like that no process is under gdb. But
	 don't do a ttrace call to detach the process. If ttrace detached
	 it would satisfy parent's wait() and we cannot attach to the 
	 parent before it continues.
	 (3) Leave no evidence of the child process which is just detached.
	 (4) Defer breakpoints for debugging between parent and child, also
	     between reruns.
	 (5) Pop the pid of the immediate parent, load the symbols and 
	     attach the parent to the debugger.
	 (6) Send the continue signal to the child so that we can continue
	     debugging the parent.
       */
	char parent_pid_spelling[100];
	char *full_exec_path;

	saved_child_pid = get_pid_for (inferior_pid);
	remove_breakpoints ();

#ifdef SOLIB_REMOVE_INFERIOR_HOOK
	SOLIB_REMOVE_INFERIOR_HOOK (inferior_pid);
#endif
        dont_repeat ();
	/* TT_PROC_DETACH contines the child to exit. To avoid
	   using ttrace(), do a fake detach by clearing the
	   gdb data structures.
         */
        do_fake_detach = 1;
	target_detach (NULL, 1);
        do_fake_detach = 0;

	exec_close (0);
	free_all_objfiles ();

#if defined(SOLIB_RESTART)
	SOLIB_RESTART ();
#endif
	objfile_purge_solibs ();
	gdb_flush (gdb_stdout);

	defer_breakpoints ();

	while (1)
	  {
	    struct parent_pid_stack *temp = pop_ppid ();
	    if (temp)
	      {
                if (!absolute_path_of (temp->proc_name, &full_exec_path))
                  full_exec_path = savestring (temp->proc_name, 
				       (int) strlen (temp->proc_name));

		/* attach_command() does all these exec_file_attach
		   and symbol_file_command. But I wanted to avoid
		   pstat_getproc() calls in attach_command because
		   pstat_getproc() doesn't provide complete information
		   on the exec'd files.
                 */
	            exec_file_attach (full_exec_path, 0);
	            symbol_file_command (full_exec_path, 0);

                    inferior_pid = temp->pid;
		    free (temp);
                    sprintf (parent_pid_spelling, "%d", inferior_pid);
	            sigignore (SIGTTOU);
	            catch_command_errors (attach_command, parent_pid_spelling, 
					  0, RETURN_MASK_ALL);
	            attach_stopped = 1;
		    call_real_ttrace (TT_PROC_CONTINUE, saved_child_pid, 
				      0, 0, 0, 0);
	            ecs->pid = ecs->saved_inferior_pid = inferior_pid;

	      	    ecs->wait_some_more = 0;
	      	    break;
	      } else   /* !temp */
	          break;
	  } /* while */

        /* Bindu 092106: We need to call stop_stepping inorder to setup
           prev_* globals properly. */
	stop_stepping (ecs);
	return;
	     
      case TARGET_WAITKIND_EXITED:
	target_terminal_ours ();	/* Must do this before mourn anyway */
	print_stop_reason (EXITED, ecs->ws.value.integer);

	/* Record the exit code in the convenience variable $_exitcode, so
	   that the user can inspect this again later.  */
	set_internalvar (lookup_internalvar ("_exitcode"),
			 value_from_longest (builtin_type_int,
					  (LONGEST) ecs->ws.value.integer));
	gdb_flush (gdb_stdout);
	/* RM: On HPUX 11.00, we need to kill the target */
#if defined(HPUX_1100) || defined(HP_IA64)
	target_kill();
#else          
	target_mourn_inferior ();
#endif
	singlestep_breakpoints_inserted_p = 0;	/*SOFTWARE_SINGLE_STEP_P */
	stop_print_frame = 0;
	stop_stepping (ecs);
        /* FIXME: Exiting here now due to 11.23 gdb crashes after the
           app exits. This is a temporary workaround. */ 
        if (batch_rtc && trace_threads_in_this_run)
          {
            exit (0);
          } 
	return;

      case TARGET_WAITKIND_SIGNALLED:
	stop_print_frame = 0;
	stop_signal = ecs->ws.value.sig;
	target_terminal_ours ();	/* Must do this before mourn anyway */

	/* Note: By definition of TARGET_WAITKIND_SIGNALLED, we shouldn't
	   reach here unless the inferior is dead.  However, for years
	   target_kill() was called here, which hints that fatal signals aren't
	   really fatal on some systems.  If that's true, then some changes
	   may be needed. */
#if defined(HPUX_1100) || defined(HP_IA64)
        target_kill();
#else
	target_mourn_inferior ();
#endif

	print_stop_reason (SIGNAL_EXITED, stop_signal);
	singlestep_breakpoints_inserted_p = 0;	/*SOFTWARE_SINGLE_STEP_P */
	stop_stepping (ecs);
	return;

	/* The following are the only cases in which we keep going;
	   the above cases end in a continue or goto. */
      case TARGET_WAITKIND_FORKED:
	stop_signal = TARGET_SIGNAL_TRAP;
	pending_follow.kind = ecs->ws.kind;

	/* Ignore fork events reported for the parent; we're only
	   interested in reacting to forks of the child.  Note that
	   we expect the child's fork event to be available if we
	   waited for it now. */
	/* We can't ignore fork events reported for the parent !!!
	   Fork events are reported in pairs, one for the parent
	   and the other for the child. These can come in any order.
	   So if we first saw a child fork and detached the child,
	   when we see the parent's fork event we will mistake it to
	   be a brand new fork sequence and do a wait for the child's
	   fork and wait forever...   -- srikanth, 000817. */
	if (!is_process_id(PIDGET(inferior_pid)))
	  real_inferior_pid = get_pid_for(inferior_pid);
	else
	  real_inferior_pid = PIDGET(inferior_pid);

	/* srikanth, 000802 the fork may be happening in a thread
	   other than the main thread. */
	if (!is_process_id (PIDGET (ecs->pid)))
	  forking_pid = get_pid_for (ecs->pid);
	else 
	  forking_pid = PIDGET (ecs->pid);

	if (real_inferior_pid == forking_pid)
	  {
	    pending_follow.fork_event.saw_parent_fork = 1;
	    pending_follow.fork_event.parent_pid = ecs->pid;
	    pending_follow.fork_event.child_pid = ecs->ws.value.related_pid;
	    if (!pending_follow.fork_event.saw_child_fork)
	      {
                ecs->infwait_state = infwait_retrieve_other_fork_state;
                ecs->waiton_pid = ecs->ws.value.related_pid;
		prepare_to_wait (ecs);
		return;
	      }
	  }
	else
	  {
	    pending_follow.fork_event.saw_child_fork = 1;
	    pending_follow.fork_event.child_pid = ecs->pid;
	    pending_follow.fork_event.parent_pid = ecs->ws.value.related_pid;
#ifdef HP_MXN
	      /* bindu 101601: Continue the child until it is safe
                 to collect the thread information. Remove any shared library 
		 hooks before calling this function. We do not want to get 
		 any notifications from dld during this time.
                 JAGag29255: Do this only if we are following the child. */
              if (follow_mode == follow_fork_mode_child)
                {
 	          ecs->saved_inferior_pid = inferior_pid;
	          inferior_pid = ecs->pid;
	          SET_TRACE_BIT (UNSET_FLAGS);
	          remove_breakpoints ();
	          inferior_pid = ecs->saved_inferior_pid;

	          continue_child_after_fork (ecs->pid);

                  ecs->saved_inferior_pid = inferior_pid;
                  inferior_pid = ecs->pid;
	          SET_TRACE_BIT (SET_FLAGS);
	          insert_breakpoints ();
	          inferior_pid = ecs->saved_inferior_pid;
                }
#endif
	    if (!pending_follow.fork_event.saw_parent_fork)
	      {
                ecs->infwait_state = infwait_retrieve_other_fork_state;
                ecs->waiton_pid = ecs->ws.value.related_pid;
		prepare_to_wait (ecs);
		return;
	      }
	  }

	stop_pc = read_pc_pid (ecs->pid);
	ecs->saved_inferior_pid = inferior_pid;
	inferior_pid = ecs->pid;
	stop_bpstat = bpstat_stop_status (&stop_pc,
					  (DECR_PC_AFTER_BREAK ?
					   (prev_pc != stop_pc - DECR_PC_AFTER_BREAK
					    && currently_stepping (ecs))
					   : 0));
	ecs->random_signal = !bpstat_explains_signal (stop_bpstat);
	inferior_pid = ecs->saved_inferior_pid;
	ecs->isc_state = isc_both_state;
	goto process_event_stop_test;

	/* If this a platform which doesn't allow a debugger to touch a
	   vfork'd inferior until after it exec's, then we'd best keep
	   our fingers entirely off the inferior, other than continuing
	   it.  This has the unfortunate side-effect that catchpoints
	   of vforks will be ignored.  But since the platform doesn't
	   allow the inferior be touched at vfork time, there's really
	   little choice. */
      case TARGET_WAITKIND_VFORKED:
	stop_signal = TARGET_SIGNAL_TRAP;
	pending_follow.kind = ecs->ws.kind;

	/* Is this a vfork of the parent?  If so, then give any
	   vfork catchpoints a chance to trigger now.  (It's
	   dangerous to do so if the child canot be touched until
	   it execs, and the child has not yet exec'd.  We probably
	   should warn the user to that effect when the catchpoint
	   triggers...) */
	if (!is_process_id (PIDGET (inferior_pid)))
	  real_inferior_pid = get_pid_for (inferior_pid);
	else
	  real_inferior_pid = PIDGET (inferior_pid);

	/* srikanth, we need to accomdate the case where the vfork
	   happens in a thread other than the main thread. 000802. */
	if (!is_process_id (PIDGET (ecs->pid)))
	  forking_pid = get_pid_for (ecs->pid);
	else 
	  forking_pid = PIDGET (ecs->pid);

	if (real_inferior_pid == forking_pid)
	  {
	    pending_follow.fork_event.saw_parent_fork = 1;
	    pending_follow.fork_event.parent_pid = ecs->pid;
	    pending_follow.fork_event.child_pid = ecs->ws.value.related_pid;
	    /* RM: We'll soon detach the child, if it has any
	       pending EXEC events, they are irrelevant */
	    /* RM: huh? what if we are following the child? maybe-FIXME */
	    inferior_ignoring_leading_exec_events =
	      target_reported_exec_events_per_exec_call () - 1;

	    if (follow_mode == follow_fork_mode_parent )
	      {
		/* Stacey 12/14/2001 
		   Remember that we get a parent's vfork event only the
		   child has exited or exec'd.  If the user has asked us to
		   follow the parent and not the child, then by now we have
		   already received the child's vfork event and detached
		   from the child.  The only thing left to do is to continue 
		   debugging the parent process as if the fork never 
		   happened.  The following lines tell gdb to forget about the
		   vfork and not to wait for any more vfork events. */
                /* Bindu 031202: Also do all the post follow work like 
                   putting back the breakpoints here. We cannot do this 
		   immediately after detaching from the child because, before 
		   exec/exit, the address space of child and parent are the 
		   same. So reinserting the breakpoints in parent will amount 
		   to reinserting them into the child. Note that we get vfork 
		   event of parent after child execs/exits on HP_UX. */
                remove_breakpoints();
                insert_breakpoints();

		target_post_follow_vfork (pending_follow.fork_event.parent_pid,
                	               	  1,
                                	  pending_follow.fork_event.child_pid,
                                	  0);

		pending_follow.fork_event.saw_parent_fork = 0;
		pending_follow.kind = TARGET_WAITKIND_SPURIOUS;
	      }
	  } /* if (real_inferior_pid == forking_pid) */
	else
	  {
	    /* If we've seen the child's vfork event but cannot really touch
	       the child until it execs, then we must continue the child now.
	       Else, give any vfork catchpoints a chance to trigger now. */

	    pending_follow.fork_event.saw_child_fork = 1;
	    pending_follow.fork_event.child_pid = ecs->pid;
	    pending_follow.fork_event.parent_pid = ecs->ws.value.related_pid;
	    target_post_startup_inferior (pending_follow.fork_event.child_pid);
	    follow_vfork_when_exec = !target_can_follow_vfork_prior_to_exec ();

            /* srikanth, 001127, This code needs major repair. If our
               intent is to let the child run until it execs, we need to
               lift the breakpoints and then resume. If any events occur
               in between all bets are off. For the time being remove
               shared library bind on reference requests. If this is the
               first dynamic reference to exit() or exec(), it is going
               to trigger a breakpoint.
            */
	    if (follow_vfork_when_exec)
	      {
		if (!STREQ (follow_mode, "parent"))
		  {
		    /* srikanth, 11.11 does not allow us to write to
		       the parent while a vfork is in progress. Lift
		       the breakpoint from the child instead. 010131.
		    */
		    /* Bindu 031202: On IA, we are getting
		       SIGTRAPs from child at _asm_break. So remove the
		       _asm_break trace bit from child till exec. 
		       FIXME: Still getting _asm_break events from the child!!! */
		    ecs->saved_inferior_pid = inferior_pid;
		    inferior_pid = ecs->pid;
		    SOLIB_REMOVE_INFERIOR_HOOK (inferior_pid);
		    remove_breakpoints ();
		    inferior_pid = ecs->saved_inferior_pid;
/* MERGE: For JAGae52783, wait_on_prefork_pid and wait_on_prefork_tid are reset if the child is
   being followed. With the implementation of 'follow-fork-mode ask' functionality, the earlier reset
   in infttrace.c does not happen as the 'follow-mode' is known only after the reset code is executed.
   The call to reset_prefork_wait() resets the 2 variables in case the follow-mode is child and is 
   applicable only for ttrace with feature level >= 10. */
                    if (feature_level>= 10)
        	    {
			reset_prefork_wait();
		    }
		    target_resume (ecs->pid, 0, TARGET_SIGNAL_0);
		  }
		else
		  /* Stacey 12/14/2001  Fix for JAGad66313
		     Since a vfork differs mainly from a fork in that a child
		     process shares its parents address space until it execs,
		     we'll assume users have chosen to use 'vfork' because
		     they intend to have their child process' exec immediately.
		     So nothing important will ever take place in a child
		     process between vfork and exec calls.

		     We used to follow the child until it exec'd and then
		     detach from it.  This worked okay in most cases, but
		     caused problems when debugging a parent process whose
		     vforked child exec'd a setuid process.  Since the
		     child's exec occured while still under debugger control,
		     the child's setuid wasn't honored for security reasons.

		     Given:
		     1.  user is asking us to debug the parent only (shown by
			 follow_mode)
		     2.  nothing interesting will happen to the child until it
			 execs.
		     3.  We'll get the child's vfork event before the
			 parent's event and parent will be unavailable to us
			 until child execs or exits,
		     We can solve the problem with setuid child processes by
		     detaching from the child immediately and then resuming
		     debugging of the parent process.  The child's exec event
		     will happen outside of the debugger so the setuid bit
		     will be honored when the child execs.

		     The following call to follow_vfork will detach from
		     the child process and perform any other necessary and
		     related tasks.  */

		  {
#ifndef HPUX_1020
		/* Fix for JAGae47498. Change might need to be revisited
                   FIXME */
		follow_vfork_when_exec = 0; /* follow it before exec. */
#endif
#ifdef GDB_TARGET_IS_HPUX /* As follow_vfork is defined only for HPUX!!! Why!!!! */
		follow_vfork (inferior_pid,
		pending_follow.fork_event.child_pid);
#endif
		  }
		prepare_to_wait (ecs);
		return;
	      } /* if (follow_vfork_when_exec) */
	  } /* else if (real_inferior_pid == forking_pid) */

	stop_pc = read_pc_pid (ecs->pid);
	stop_bpstat = bpstat_stop_status (&stop_pc,
#if DECR_PC_AFTER_BREAK
					  (DECR_PC_AFTER_BREAK ?
					   (prev_pc != stop_pc - DECR_PC_AFTER_BREAK
					    && currently_stepping (ecs))
					   : 0)
#else /* DECR_PC_AFTER_BREAK zero */
					  0
#endif /* DECR_PC_AFTER_BREAK zero */
					   );
	ecs->random_signal = !bpstat_explains_signal (stop_bpstat);
	ecs->isc_state = isc_parent_state;
	goto process_event_stop_test;

     case TARGET_WAITKIND_PREFORKED:
	    stop_signal = TARGET_SIGNAL_TRAP;
	    prepare_to_wait (ecs);
	    return;

     case TARGET_WAITKIND_FAILED_PREFORK:
  	    stop_signal = TARGET_SIGNAL_TRAP;
	    prepare_to_wait (ecs);
	    return;

      case TARGET_WAITKIND_EXECD:
	stop_signal = TARGET_SIGNAL_TRAP;

	/* Is this a target which reports multiple exec events per actual
	   call to exec()?  (HP-UX using ptrace does, for example.)  If so,
	   ignore all but the last one.  Just resume the exec'r, and wait
	   for the next exec event. */
	if (inferior_ignoring_leading_exec_events)
	  {
	    inferior_ignoring_leading_exec_events--;
	    if (pending_follow.kind == TARGET_WAITKIND_VFORKED)
	      {
	      (void) ENSURE_VFORKING_PARENT_REMAINS_STOPPED (pending_follow.fork_event.parent_pid);
	      }
	    target_resume (ecs->pid, 0, TARGET_SIGNAL_0);
	    prepare_to_wait (ecs);
	    return;
	  }
	inferior_ignoring_leading_exec_events =
	  target_reported_exec_events_per_exec_call () - 1;

	pending_follow.execd_pathname =
	  savestring (ecs->ws.value.execd_pathname,
		      (int) strlen (ecs->ws.value.execd_pathname));

	/* Did inferior_pid exec, or did a (possibly not-yet-followed)
	   child of a vfork exec?

	   ??rehrauer: This is unabashedly an HP-UX specific thing.  On
	   HP-UX, events associated with a vforking inferior come in
	   threes: a vfork event for the child (always first), followed
	   a vfork event for the parent and an exec event for the child.
	   The latter two can come in either order.

	   If we get the parent vfork event first, life's good: We follow
	   either the parent or child, and then the child's exec event is
	   a "don't care".

	   But if we get the child's exec event first, then we delay
	   responding to it until we handle the parent's vfork.  Because,
	   otherwise we can't satisfy a "catch vfork". */
	if (pending_follow.kind == TARGET_WAITKIND_VFORKED)
	  {
	    /* Bindu 111303: JAGae91824, If the catch exec is enabled and we
	       are following the child, we do not want to ignore the exec. 

	       It should also be noted that when following the child, if both
	       "catch vfork" and "catch exec" are set, only exec event might
	       be caught. I.e., if parent's VFORK event comes before the
	       child's EXEC event, then user gets control at both vfork point
	       of the parent and the following exec point of the child. But
	       if the child's EXEC event comes before the parent's VFORK
	       event, user will get control only at the exec point, debugger
	       will detach from the parent and will never get control at the
	       vfork point of the parent.*/
	    if (   (follow_mode == follow_fork_mode_parent)
	        || (!catch_exec_enabled ())
	       )
	      {
	        pending_follow.fork_event.saw_child_exec = 1;

	        /* On some targets, the child must be resumed before
	           the parent vfork event is delivered.  A single-step
	           suffices. */
	        if (RESUME_EXECD_VFORKING_CHILD_TO_GET_PARENT_VFORK ())
                    /* Ptrace on HP-UX 10.20 will not return the vfork event
                       for the parent process.  If we try to resume the target,
                       we'll hang waiting on an event.  Therefore, just resume
                       the inferior and everything will be fine, though
                       we will not be able to catch any vfork events. */
#ifdef HPUX_1020 
	          resume(0, TARGET_SIGNAL_0);
#else
	          target_resume (ecs->pid, 1, TARGET_SIGNAL_0);
#endif
	        /* We expect the parent vfork event to be available now. */
	        prepare_to_wait (ecs);
	        return;
	      }
	  }


	/* JAGaf54650: Before loading the symbol table for the child, 
	   defer the parent breakpoints. 
	 */
	if (follow_fork_mode_string == follow_fork_mode_serial)
	  defer_breakpoints ();

	/* This causes the eventpoints and symbol table to be reset.  Must
	   do this now, before trying to determine whether to stop. */
	follow_exec (inferior_pid, pending_follow.execd_pathname);
	free (pending_follow.execd_pathname);

	stop_pc = read_pc_pid (ecs->pid);
	ecs->saved_inferior_pid = inferior_pid;
	inferior_pid = ecs->pid;
	stop_bpstat = bpstat_stop_status (&stop_pc,
					  (DECR_PC_AFTER_BREAK ?
					   (prev_pc != stop_pc - DECR_PC_AFTER_BREAK
					    && currently_stepping (ecs))
					   : 0));
	ecs->random_signal = !bpstat_explains_signal (stop_bpstat);
	inferior_pid = ecs->saved_inferior_pid;
	ecs->isc_state = isc_parent_state;
	goto process_event_stop_test;

	/* These syscall events are returned on HP-UX, as part of its
	   implementation of page-protection-based "hardware" watchpoints.
	   HP-UX has unfortunate interactions between page-protections and
	   some system calls.  Our solution is to disable hardware watches
	   when a system call is entered, and reenable them when the syscall
	   completes.  The downside of this is that we may miss the precise
	   point at which a watched piece of memory is modified.  "Oh well."

	   Note that we may have multiple threads running, which may each
	   enter syscalls at roughly the same time.  Since we don't have a
	   good notion currently of whether a watched piece of memory is
	   thread-private, we'd best not have any page-protections active
	   when any thread is in a syscall.  Thus, we only want to reenable
	   hardware watches when no threads are in a syscall.

	   Also, be careful not to try to gather much state about a thread
	   that's in a syscall.  It's frequently a losing proposition. */

        /* srikanth, 000817, what if there are threads already in syscalls
	   at the time the watchpoint is created ??? Our nice little scheme
	   of counting threads in syscalls would fall flat. FIXME. */
      case TARGET_WAITKIND_SYSCALL_ENTRY:
	number_of_threads_in_syscalls++;

	if (ecs->wp->value.syscall_id == 491)
          syscall_491_seen = 1;
	if (number_of_threads_in_syscalls == 1)
	  {
	    TARGET_DISABLE_HW_WATCHPOINTS (inferior_pid);
	  }
	resume (0, TARGET_SIGNAL_0);
	prepare_to_wait (ecs);
	return;

	/* Before examining the threads further, step this thread to
	   get it entirely out of the syscall.  (We get notice of the
	   event when the thread is just on the verge of exiting a
	   syscall.  Stepping one instruction seems to get it back
	   into user code.)

	   Note that although the logical place to reenable h/w watches
	   is here, we cannot.  We cannot reenable them before stepping
	   the thread (this causes the next wait on the thread to hang). */
      case TARGET_WAITKIND_SYSCALL_RETURN:
	stop_signal = TARGET_SIGNAL_TRAP;

	if (number_of_threads_in_syscalls > 0)
	  {
	    number_of_threads_in_syscalls--;
	  }

	registers_changed ();
	if (ecs->wp->value.syscall_id == 492)
	  syscall_491_seen = 0;

	if (syscall_491_seen)
	  {
	    /* If it is syscall# 491 which inhibits the signals,
	     * it will be followed by #492 that enables signals. So,
	     * wait till we get the syscall 492 exit event
	     * before enabling the page protections. Disabling signals
	     * when page-protections are on will result in process
	     * getting killed when the signal is posted when accessing
	     * protected page. Just continue here. 
	     */
	    ecs->enable_hw_watchpoints_after_wait = 0;
	    target_resume (ecs->pid, 0, TARGET_SIGNAL_0);
	  }
        else 
          {
	    ecs->enable_hw_watchpoints_after_wait = 
		(number_of_threads_in_syscalls == 0);

        if (   ecs->wp->value.syscall_id == 428 /* sigwaitinfo */
	    || ecs->wp->value.syscall_id == 429 /* sigtimewait */
	    || ecs->wp->value.syscall_id == 430 /* sigwait */
	   )
	  {
	    /* Treat this as signal event. */
	    stop_signal = signal_sigwait (ecs->pid);
            step_before_continuing = 1;
            break;
	  }
	    /* RM: need to step out of syscall */
	    target_resume (ecs->pid, 1, TARGET_SIGNAL_0);
	  }

	ecs->waiton_pid = ecs->pid;
	ecs->wp = &(ecs->ws);
	ecs->infwait_state = infwait_isc_normal_state;
	prepare_to_wait (ecs);
	return;

      case TARGET_WAITKIND_STOPPED:
	stop_signal = ecs->ws.value.sig;
	break;

	/* We had an event in the inferior, but we are not interested
	   in handling it at this level. The lower layers have already
	   done what needs to be done, if anything. This case can
	   occur only when the target is async or extended-async. One
	   of the circumstamces for this to happen is when the
	   inferior produces output for the console. The inferior has
	   not stopped, and we are ignoring the event. */
      case TARGET_WAITKIND_IGNORE:
	ecs->wait_some_more = 1;
	return;
    }

  /* We may want to consider not doing a resume here in order to give
     the user a chance to play with the new thread.  It might be good
     to make that a user-settable option.  */

  /* At this point, all threads are stopped (happens automatically in
     either the OS or the native code).  Therefore we need to continue
     all threads in order to make progress.  */
  if (ecs->new_thread_event)
    {
      target_resume (-1, 0, TARGET_SIGNAL_0);
      prepare_to_wait (ecs);
      return;
    }

  stop_pc = read_pc_pid (ecs->pid);

  /* See if a thread hit a thread-specific breakpoint that was meant for
     another thread.  If so, then step that thread past the breakpoint,
     and continue it.  */

  if (stop_signal == TARGET_SIGNAL_TRAP)
    {
      /* Bindu: If we hit the breakpoint that we placed at
	 __gdb_java_breakpoint, load libjunwind from the location
	 in __gdb_java_unwindlib, remove the breakpoint and continue. */
      if (   gdb_java_breakpoint_addr
	  && (stop_pc == gdb_java_breakpoint_addr))
	{
	  load_java_unwindlib_from_var ();
          /* JAGaf26864 - since we placed the breakpoint using 
             target_insert_internal_breakpoint, call the corresponding
             remove function.
          */
	  target_remove_internal_breakpoint (gdb_java_breakpoint_addr,
			 	    gdb_java_break_buffer);
	  target_resume (-1, 0, TARGET_SIGNAL_0);
	  prepare_to_wait (ecs);
	  return;
	}

#ifdef HPPA_FIX_AND_CONTINUE
#if defined(GDB_TARGET_IS_HPPA_20W)
      if (   (stop_pc == bfd_get_start_address (symfile_objfile->obfd))
          && get_reapply_fix())
        {
          remove_bp_fix_event_breakpoints();
          load_fixed_libraries ();
          set_reapply_fix (false);
          re_enable_breakpoints_in_shlibs ();
          insert_breakpoints();
          target_resume (-1, 0, TARGET_SIGNAL_0);
	  prepare_to_wait (ecs);
	  return;
        }
#endif
#endif
      if (SOFTWARE_SINGLE_STEP_P && singlestep_breakpoints_inserted_p)
	  ecs->random_signal = 0;
      else if (   breakpoints_inserted
	       && breakpoint_here_p (stop_pc - DECR_PC_AFTER_BREAK))
	{
	  ecs->random_signal = 0;
	  if (!breakpoint_thread_match (stop_pc - DECR_PC_AFTER_BREAK,
					ecs->pid))
	    {
	      int remove_status;

	      /* Saw a breakpoint, but it was hit by the wrong thread.
	         Just continue. */
	      write_pc_pid (stop_pc - DECR_PC_AFTER_BREAK, ecs->pid);

                  
	      /* RM: remove breakpoints in the context of the new pid. After
		 a vfork we can write to the child's address space
		 but not to the parents */
	      ecs->saved_inferior_pid = inferior_pid;
	      inferior_pid = ecs->pid;
	      remove_status = remove_breakpoints ();
	      inferior_pid = ecs->saved_inferior_pid;
		
	      /* Did we fail to remove breakpoints?  If so, try
		 to set the PC past the bp.  (There's at least
		 one situation in which we can fail to remove
		 the bp's: On HP-UX's that use ttrace, we can't
		 change the address space of a vforking child
		 process until the child exits (well, okay, not
		 then either :-) or execs. */
	      if (remove_status != 0)
		{
		  write_pc_pid (stop_pc - DECR_PC_AFTER_BREAK + 4, ecs->pid);
		}
	      else
		{		/* Single step */
		  /* RM: ??? If vfork_in_flight is 1, this does a
		     LWP_CONTINUE on the parent too. Not allowed
		     anymore. */
		  int saved_vfork_in_flight = vfork_in_flight;
		  vfork_in_flight = 0;
		  target_resume (ecs->pid, 1, TARGET_SIGNAL_0);
		  vfork_in_flight = saved_vfork_in_flight;

		  /* FIXME: What if a signal arrives instead of the
		     single-step happening?  */

		  /* JYG: MERGE FIXME: the next 4 statements from
		     RM's vfork 11.11 fix. */
		  /* Bindu 032102: Re-insert the breakpoints in the current
		     process. */
		  ecs->saved_inferior_pid = inferior_pid;
		  inferior_pid = ecs->pid;
		  insert_breakpoints ();
		  inferior_pid = ecs->saved_inferior_pid;

		  ecs->waiton_pid = ecs->pid;
		  ecs->wp = &(ecs->ws);
		  ecs->infwait_state = infwait_thread_hop_state;
		  prepare_to_wait (ecs);
		  return;
		}

	      /* We need to restart all the threads now,
	       * unles we're running in scheduler-locked mode. 
	       * FIXME: shouldn't we look at currently_stepping ()?
	       */
	      if (scheduler_mode == schedlock_on)
		target_resume (ecs->pid, 0, TARGET_SIGNAL_0);
	      else
		target_resume (-1, 0, TARGET_SIGNAL_0);

	      prepare_to_wait (ecs);
	      return;
	    }
	  else
	    {
	      /* This breakpoint matches--either it is the right
		 thread or it's a generic breakpoint for all threads.
		 Remember that we'll need to step just _this_ thread
		 on any following user continuation! */
	      thread_step_needed = 1;
	    }
	}
    }
  else
    ecs->random_signal = 1;

  /* See if something interesting happened to the non-current thread.  If
     so, then switch to that thread, and eventually give control back to
     the user.

     Note that if there's any kind of pending follow (i.e., of a fork,
     vfork or exec), we don't want to do this now.  Rather, we'll let
     the next resume handle it. */
  if (   (ecs->pid != inferior_pid)
      && (pending_follow.kind == TARGET_WAITKIND_SPURIOUS))
    {
      /* If it's not SIGTRAP and not a signal we want to stop for, then
	 continue the thread. */

      /* That logic drops the profiling signals meant to be passed to
         gather_pc_sample below. Inserted an additional check for the
         profiling case            -- Srikanth, Oct 10th 2005.
       */
      if (   stop_signal != TARGET_SIGNAL_TRAP
 	  && !signal_stop[stop_signal]
          && !(profile_on && stop_signal == profiler_signal))
        {
 	  int saved_inferior_pid;
	    
	  /* Clear the signal if it should not be passed.
	     Or if this is a sigwait type signal. */
	  if (signal_program[stop_signal] == 0 || step_before_continuing)
	    {
	      stop_signal = TARGET_SIGNAL_0;
	      step_before_continuing = 0;
	    }

	  /* srikanth, 000815, We used to continue just one thread.
	     This is an invitation for deadlock. */
	  saved_inferior_pid = inferior_pid; 
	  inferior_pid = ecs->pid;
	  target_resume (-1, 0, stop_signal);
	  inferior_pid = saved_inferior_pid;
	  prepare_to_wait (ecs);
	  return;
        }

      /* It's a SIGTRAP or a signal we're interested in.  Switch threads,
 	 and fall into the rest of wait_for_inferior().  */

      /* Caution: it may happen that the new thread (or the old one!)
	 is not in the thread list.  In this case we must not attempt
	 to "switch context", or we run the risk that our context may
	 be lost.  This may happen as a result of the target module
	 mishandling thread creation.  */

      if (in_thread_list (inferior_pid) && in_thread_list (ecs->pid))
        { /* Perform infrun state context switch: */
 	  /* Save infrun state for the old thread.  */
	  save_infrun_state (inferior_pid, prev_pc,
			     prev_func_start, prev_func_name,
			     trap_expected, step_resume_breakpoint,
			     through_sigtramp_breakpoint,
#ifdef REGISTER_STACK_ENGINE_FP
			     step_frame_rse_addr,
#endif
			     step_range_start, step_range_end,
			     step_frame_address, ecs->handling_longjmp,
			     ecs->another_trap,
			     ecs->stepping_through_solib_after_catch,
			     ecs->stepping_through_solib_catchpoints,
			     ecs->stepping_through_sigtramp);

	  /* Load infrun state for the new thread.  */
	  load_infrun_state (ecs->pid, &prev_pc,
			     &prev_func_start, &prev_func_name,
			     &trap_expected, &step_resume_breakpoint,
			     &through_sigtramp_breakpoint,
#ifdef REGISTER_STACK_ENGINE_FP
                             &step_frame_rse_addr,
#endif
			     &step_range_start, &step_range_end,
			     &step_frame_address, &ecs->handling_longjmp,
			     &ecs->another_trap,
			     &ecs->stepping_through_solib_after_catch,
			     &ecs->stepping_through_solib_catchpoints,
			     &ecs->stepping_through_sigtramp);
         }

       previous_inferior_pid = inferior_pid;
       inferior_pid = ecs->pid;

       if (context_hook)
 	 context_hook (pid_to_thread_id (ecs->pid));

       flush_cached_frames ();
    }

  if (SOFTWARE_SINGLE_STEP_P && singlestep_breakpoints_inserted_p)
    {
      /* Pull the single step breakpoints out of the target. */
  	 SOFTWARE_SINGLE_STEP (0, 0);
      singlestep_breakpoints_inserted_p = 0;
    }

  /* If PC is pointing at a nullified instruction, then step beyond
     it so that the user won't be confused when GDB appears to be ready
     to execute it. */

  if (   INSTRUCTION_NULLIFIED
      && (   !ecs->random_signal
          || (   currently_stepping (ecs)
              && (stop_signal == TARGET_SIGNAL_TRAP))))
    {
      registers_changed ();
      target_resume (ecs->pid, 1, TARGET_SIGNAL_0);

      /* We may have received a signal that we want to pass to
	 the inferior; therefore, we must not clobber the waitstatus
	 in WS. */

      ecs->infwait_state = infwait_nullified_state;
      ecs->waiton_pid = ecs->pid;
      ecs->wp = &(ecs->tmpstatus);
      prepare_to_wait (ecs);
      return;
    }

  /* It may not be necessary to disable the watchpoint to stop over
     it.  For example, the PA can (with some kernel cooperation)
     single step over a watchpoint without disabling the watchpoint.  */
  if (HAVE_STEPPABLE_WATCHPOINT && STOPPED_BY_WATCHPOINT (ecs->ws))
    {
      resume (1, 0);
      prepare_to_wait (ecs);
      return;
    }

  /* It is far more common to need to disable a watchpoint to step
     the inferior over it.  FIXME.  What else might a debug
     register or page protection watchpoint scheme need here?  */
  if (HAVE_NONSTEPPABLE_WATCHPOINT && STOPPED_BY_WATCHPOINT (ecs->ws))
    {
      /* At this point, we are stopped at an instruction which has
	 attempted to write to a piece of memory under control of
	 a watchpoint.  The instruction hasn't actually executed
	 yet.  If we were to evaluate the watchpoint expression
	 now, we would get the old value, and therefore no change
	 would seem to have occurred.

	 In order to make watchpoints work `right', we really need
	 to complete the memory write, and then evaluate the
	 watchpoint expression.  The following code does that by
	 removing the watchpoint (actually, all watchpoints and
	 breakpoints), single-stepping the target, re-inserting
	 watchpoints, and then falling through to let normal
	 single-step processing handle proceed.  Since this
	 includes evaluating watchpoints, things will come to a
	 stop in the correct manner.  */

      /* RM: this used to be a call to write_pc(), but that is wrong,
       * since write_pc() writes to both pcoqh and pcoqt. The value
       * written to pcoqt is always pcoqh+4. Bad things happen if we
       * stopped in the shadow of a branch and pcoqt was _not_ pcoqh+4.
       */
      write_register(PC_REGNUM, stop_pc - DECR_PC_AFTER_BREAK);

      remove_breakpoints ();
      registers_changed ();
      target_resume (ecs->pid, 1, TARGET_SIGNAL_0);	/* Single step */

      ecs->waiton_pid = ecs->pid;
      ecs->wp = &(ecs->ws);
      ecs->infwait_state = infwait_nonstep_watch_state;
      prepare_to_wait (ecs);
      return;
    }

  /* It may be possible to simply continue after a watchpoint.  */
  if (HAVE_CONTINUABLE_WATCHPOINT)
    STOPPED_BY_WATCHPOINT (ecs->ws);

  prev_stop_func_start = ecs->stop_func_start;
  ecs->stop_func_start = 0;
  ecs->stop_func_end = 0;
  ecs->stop_func_name = 0;
  /* Don't care about return value; stop_func_start and stop_func_name
     will both be 0 if it doesn't work.  */
  find_pc_partial_function (stop_pc, &ecs->stop_func_name,
	                    &ecs->stop_func_start, &ecs->stop_func_end);
  ecs->stop_func_start += FUNCTION_START_OFFSET;
  ecs->another_trap = 0;
  bpstat_clear (&stop_bpstat);
  stop_step = 0;
  stop_stack_dummy = 0;
  stop_print_frame = 1;
  ecs->random_signal = 0;
  stopped_by_random_signal = 0;
  breakpoints_failed = 0;

  /* Look at the cause of the stop, and decide what to do.
     The alternatives are:
     1) break; to really stop and return to the debugger,
     2) drop through to start up again
     (set ecs->another_trap to 1 to single step once)
     3) set ecs->random_signal to 1, and the decision between 1 and 2
     will be made according to the signal handling tables.  */

  /* First, distinguish signals caused by the debugger from signals
     that have to do with the program's own actions.
     Note that breakpoint insns may cause SIGTRAP or SIGILL
     or SIGEMT, depending on the operating system version.
     Here we detect when a SIGILL or SIGEMT is really a breakpoint
     and change it to SIGTRAP.  */

  if (   stop_signal == TARGET_SIGNAL_TRAP
      || (   breakpoints_inserted
          && (   stop_signal == TARGET_SIGNAL_ILL
	      || stop_signal == TARGET_SIGNAL_EMT))
      || stop_soon_quietly)
    {
      if (stop_signal == TARGET_SIGNAL_TRAP && stop_after_trap)
	{
	  stop_print_frame = 0;
	  stop_stepping (ecs);
	  return;
	}
      if (stop_soon_quietly)
	{
	  stop_stepping (ecs);
	  return;
	}

      /* Don't even think about breakpoints
	 if just proceeded over a breakpoint.

	 However, if we are trying to proceed over a breakpoint
	 and end up in sigtramp, then through_sigtramp_breakpoint
	 will be set and we should check whether we've hit the
	 step breakpoint.  */
      if (stop_signal == TARGET_SIGNAL_TRAP && trap_expected
	  && through_sigtramp_breakpoint == NULL)
	bpstat_clear (&stop_bpstat);
      else
	{
          /* srikanth, 000131, we use to remove breakpoints here
             and reinsert them just after bpstat_stop_status()
             returned. The idea is to allow the callees of this
             function to see the original code stream rather than
             the one patched with breakpoints. However this makes
             gdb crawl. Now the function child_xfer_memory is
             enhanced to return the shadow contents associated
             with the breakpoint, should one be planted at the
             address of interest.
           */

          /* Pass TRUE if our reason for stopping is something other
               than hitting a breakpoint.  We do this by checking that
               1) stepping is going on and 2) we didn't hit a breakpoint
               in a signal handler without an intervening stop in
               sigtramp, which is detected by a new stack pointer value
               below any usual function calling stack adjustments.  */
	  stop_bpstat = bpstat_stop_status (
                          &stop_pc,
	                  (DECR_PC_AFTER_BREAK
                           ? (   currently_stepping (ecs)
		              && prev_pc != stop_pc - DECR_PC_AFTER_BREAK
		              && !(   step_range_end
		                   && INNER_THAN (read_sp (), (step_sp - 16))))
                           : 0));
	  /* Following in case break condition called a
	     function.  */
	  stop_print_frame = 1;
	}

      if (stop_signal == TARGET_SIGNAL_TRAP)
	ecs->random_signal
	  = !(bpstat_explains_signal (stop_bpstat)
	      || trap_expected
	      || (   !CALL_DUMMY_BREAKPOINT_OFFSET_P
		  && PC_IN_CALL_DUMMY (stop_pc, read_sp (),
                                       FRAME_FP (get_current_frame ())))
	      || (step_range_end && step_resume_breakpoint == NULL));
      else
	{
	  ecs->random_signal
	    = !(bpstat_explains_signal (stop_bpstat)
	           /* End of a stack dummy.  Some systems (e.g. Sony
	              news) give another signal besides SIGTRAP, so
	              check here as well as above.  */
		|| (   !CALL_DUMMY_BREAKPOINT_OFFSET_P
		    && PC_IN_CALL_DUMMY (stop_pc, read_sp (),
					 FRAME_FP (get_current_frame ()))));
	
#ifdef HP_IA64
	 /* We were not listening to the application correctly.*/
	 /* This is a special case when stepping. As you know that hw breakpoint
	    will cause sigtrap for whole bundle not for an instruction at the
	    address you want a break. So, if you have put hw breakpoint at 
            slot 0 and if you hit other than SIGTRAP signal when the app
	    executes an instruction at slot 1/2.
	    you will get SIGTRAP notification as we have put a hw breakpoint at
	    at <0Xnnnnnnnnn0>.
	    This will make us to think that this stop is not due to random
	    signal and we go set stop_signal to sigtrap below.
	    So check what the app is saying, whether the app has got hurt
	    by some signal other than SIGTRAP that we get for a breakpoint hit.
	    pl see CR1000760164: issues, with HW breakpoints, shared libraries. 
	 */
	 if (is_hw_break (stop_bpstat->breakpoint_at->type)
	      && stop_signal != TARGET_SIGNAL_TRAP)
	  ecs->random_signal
	    = (!are_addrs_equal (stop_pc, stop_bpstat->breakpoint_at->address));
#endif
	  if (!ecs->random_signal)
	    stop_signal = TARGET_SIGNAL_TRAP;
	}
    }

  else
    {
      /* When we reach this point, we've pretty much decided
         that the reason for stopping must've been a random
         (unexpected) signal. */
      ecs->random_signal = 1;
    }

  /* If a fork, vfork or exec event was seen, then there are two
     possible responses we can make:

     1. If a catchpoint triggers for the event (ecs->random_signal == 0),
     then we must stop now and issue a prompt.  We will resume
     the inferior when the user tells us to.
     2. If no catchpoint triggers for the event (ecs->random_signal == 1),
     then we must resume the inferior now and keep checking.

     In either case, we must take appropriate steps to "follow" the
     the fork/vfork/exec when the inferior is resumed.  For example,
     if follow-fork-mode is "child", then we must detach from the
     parent inferior and follow the new child inferior.

     In either case, setting pending_follow causes the next resume()
     to take the appropriate following action. */
process_event_stop_test:
  /* JYG: MERGE NOTE: Per RM's comment below, we need to step out
     of syscall for fork/vfork/exec, and then keep_going.
     In order to control this stepping, which requires two
     steps for fork (both child and parent), and one for vfork or
     exec, we keep track of isc (InSysCall) state via ecs->isc_state.
     Also, we use a infwait_isc_state which handles
     enable_hw_watchpoints_after_wait (like infwait_normal_state), but
     uses ecs->tmpstatus for wait status (unlike infwait_normal_state
     which uses ecs->ws). */
  if (ecs->isc_state != isc_neither_state)
    {
      /* In stepping out of syscall mode */
      if (   ecs->ws.kind == TARGET_WAITKIND_FORKED
          || ecs->ws.kind == TARGET_WAITKIND_VFORKED)
	{
	  /* RM: We don't get informed about syscall returns in
	     the case of fork, vfork and exec.
	     Decrement the syscall count here instead */
	  if (number_of_threads_in_syscalls > 0)
	    number_of_threads_in_syscalls--;
	    /* srikanth, 000815, we used to step the child and
	       parent out of fork and the parent alone in the
	       case of vfork when num of threads reaches 0. I
               am disabling this code as it does not do the right
               thing for the MT case. (If the fork happens in a
               non-main thread, we would step the main thread.) */
	    ecs->isc_state = isc_neither_state;
	}
      else if (ecs->ws.kind == TARGET_WAITKIND_EXECD)
	{
	  number_of_threads_in_syscalls = 0;

	  /* srikanth, 000815, we used to step the child and
	     parent out of fork and the parent alone in the
	     case of vfork. I am disabling this code as it
	     does not do the right thing for the MT case.
	     (If the fork happens in a non-main thread, we would
	     step the main thread.) */
	  ecs->isc_state = isc_neither_state;
	}
    }
    
  if (ecs->ws.kind == TARGET_WAITKIND_FORKED)
    {
      if (ecs->random_signal)	/* I.e., no catchpoint triggered for this. */
	{
	  trap_expected = 1;
	  stop_signal = TARGET_SIGNAL_0;
	  keep_going (ecs);
	  return;
	}
    }
  else if (ecs->ws.kind == TARGET_WAITKIND_VFORKED)
    {
      if (ecs->random_signal)	/* I.e., no catchpoint triggered for this. */
	{
	  stop_signal = TARGET_SIGNAL_0;	
	  keep_going (ecs);
	  return;
	}
    }
  else if (ecs->ws.kind == TARGET_WAITKIND_EXECD)
    {
      pending_follow.kind = ecs->ws.kind;
      if (ecs->random_signal)	/* I.e., no catchpoint triggered for this. */
	{
	  trap_expected = 1;
	  stop_signal = TARGET_SIGNAL_0;
	  keep_going (ecs);
	  return;
	}
    }

  /* For the program's own signals, act according to
     the signal handling tables.  */

#ifdef DYNLINK_HAS_BREAK_HOOK 
  if (SOLIB_AT_DYNLINK_HOOK(ecs->pid, stop_pc))
    {
      CORE_ADDR break_addr;
#ifdef SOLIB_BOR_EVENT
      CORE_ADDR  bound_addr;
      ecs->saved_inferior_pid = inferior_pid;
      inferior_pid = ecs->pid;
      bound_addr = SOLIB_BOR_EVENT();
      inferior_pid = ecs->saved_inferior_pid;
      if (bound_addr)
        {
          /* If this is not a BOR event we are interested in, ignore the
	     event.

	     If this is a BOR event we are interested in, set a breakpoint
	     at either the return address or bound_addr depending upon
	     whether we have debug info for bound_addr.  In either
	     case, go on.
	   */

	  CORE_ADDR			break_addr;
	  pending_bor_event_t * 	examine_pending;
	  pending_bor_event_t ** 	next_event_pp = &bor_events_pending;

	  while (*next_event_pp)
	    {
	      examine_pending = *next_event_pp;

	      /* We have the event we are looking for if the
		 thread_id matches.
	       */
	      if (examine_pending->thread_id == inferior_pid)
	        {
                  /* Set a step_resume breakpoint at either the
		     bound_address or return_address depending upon
		     whether we can debug the routine at bound_addr.
		   */
		  struct symtab_and_line sal;
		      
		  sal = find_pc_line (bound_addr, 0);
		  if (sal.line)
		    {
                      /* It is debuggable, set a breakpoint there */
		      struct symtab *symtab_p;

		      symtab_p = find_pc_symtab (stop_pc);
		      break_addr = bound_addr;
		      if (symtab_p && symtab_p->language != language_asm)
		        break_addr = SKIP_PROLOGUE (break_addr);
	            }
	          else
	            break_addr = examine_pending->return_address;

	          /* Remove the matching BOR event we found */

	          *next_event_pp = examine_pending->next;
	          free (examine_pending);

	          /* Set the breakpoint, then continue */
	          set_step_breakpoint (break_addr, ecs);
	          break;
	        } /* if matching pending bor event found */

	      next_event_pp = &examine_pending->next;
            } /* while looking for matching bor event */
        } /* if  SOLIB_BOR_EVENT */
#endif
      ecs->random_signal = 0;
      stop_for_event = SOLIB_HANDLE_DYNLINK_EVENT(ecs->pid, stop_pc);
      /* RM: dynlink event may be a library load, so re enable
       * any breakpoints we can
       * Bindu: If we are in the middle of vfork event, ignore all 
       * load events.
       */
      /* JAGae68084 - we will call re_enable_breakpoints 
         after all the libraries are loaded, so that in PA64 and IPF so
         we dont have two breakpoints if the breakpoint was initially
         deferred */
      if (if_yes_to_all_commands)
        {
          if (now_re_enable_bp)
            re_enable_breakpoints_in_shlibs();
        }
      else
        {
          if (!vfork_in_flight)
	    re_enable_breakpoints_in_shlibs();
        }

      /* Arrange to continue execution after the break instruction */

#ifndef HP_IA64
      {
        /*JAGaf20646 - We used to just advance the PC past the break
          instruction. */
        CORE_ADDR rp = read_register (RP_REGNUM);
        stop_pc = rp;
        write_pc_pid (stop_pc - DECR_PC_AFTER_BREAK,  ecs->pid);
      }

#else
      /* Break at next instruction if there is one in this bundle,
	 otherwise at the next bundle.
       */
      break_addr = stop_pc + 1;
      if ((break_addr & INSTR_PER_BUNDLE) == INSTR_PER_BUNDLE)
        {
          break_addr = (break_addr - INSTR_PER_BUNDLE) + BUNDLE_SIZE;
        }
      write_pc_pid (break_addr, ecs->pid);
#endif /* else ifndef HP_IA64 */

      /* Either resume, or stop if requested.  Currently the only
         events for which dld calls the breakpoint routine is
         for loads and unloads.
       */

      if (stop_on_solib_events || stop_for_event)
        {
          if (!stop_for_event)
            {
	      target_terminal_ours_for_output ();
	      printf_filtered ("Stopped due to shared library event\n");
            }
          stop_print_frame = 0;
          stop_stepping (ecs);
	  return;
        }

      /* srikanth, insert the breakpoints here as we may have
         enabled some deferred breakpoint by virtue of having
         loaded some new symbol table. This is necessitated by
         the fact that we don't single step over this breakpoint,
         instead resume *after* it.

         Just take care not to leave old breakpoints around, in 
         case the breakpoints get remapped.

         Insert breakpoints in the current process's context.
       */
      ecs->saved_inferior_pid = inferior_pid;
      inferior_pid = ecs->pid;
      remove_breakpoints ();
      insert_breakpoints ();
      inferior_pid =  ecs->saved_inferior_pid;
      target_resume (-1, 0, TARGET_SIGNAL_0);
      prepare_to_wait (ecs);
      return;
    }
#endif

  if (ecs->random_signal)
    {
      /* Signal not for debugging purposes.  */
      int printed = 0;

      if (stop_signal == TARGET_SIGNAL_TRAP && trap_expected_after_step)
	{
          /* We just finished single stepping before a continue. Now
	     silently continue. */
	  trap_expected_after_step = 0;
	  keep_going (ecs);
	  return;
        }
      if (stop_signal == TARGET_SIGNAL_0 && (step_before_continuing || fake_sig0_flag))
	{
	  /* We have stop_signal set to 0 for sigwait type system calls
	     when the system call returns with error. Do not print anything
	     in this case. Just continue. */

          /* QXCR1000563319: Reset step_before_continuing to 0. This is
             because at this point, we would resume the child here itself.
             If we keep step_before_continuing set to 1, then gdb will not
             deliver the next signal received by the target process
             back to the target process. Since the sigwait*() has failed, we
             don't need to follow the step_before_continuing method.
          */
          step_before_continuing = 0;
	  keep_going (ecs);
          return;
	}
      if (stop_signal == TARGET_SIGNAL_TRAP && breakpoints_inserted)
        {
          /* srikanth, in a multi threaded process, sometimes we
             report a deleted breakpoint as having triggered. This
             happens when threads T1 and T2 hit the same breakpoint
             B1 at the same time, we report the breakpoint on T1, the
             user deletes the breakpoint and resumes the process.

             Even though T2 has already hit the breakpoint, reporting
             it is confusing at best. See that as we have blown away
             the breakpoint, we print a somewhat cryptic message
             "Program received signal SIGTRAP, Trace/breakpoint trap."
             See JAGaa80394. 000925.
          */
          /* Suresh, QXCR1000776341, Mar 08: To handle the above described
             problem of printing a cryptic message ("Program received 
             signal SIGTRAP,Trace/breakpoint trap.") which also happens 
             for disabled/"enabled once" breakpoints.
           */
          if (breakpoint_was_here_p (stop_pc) || 
              breakpoint_is_disabled_or_enabled_once (stop_pc))
            {
              target_resume (-1, 0, TARGET_SIGNAL_0);
              prepare_to_wait (ecs);
              return;
            }
          /* This event is pending from thread and we did not process it
	     because user have issued the command line call - CLC to this thread
	     before we process the pending event. 
	     So when we proceed to execute the CLC, we stop in dummy function 
	     or in function called from CLC, to process this pending event. 
	     So, use ecs->sal->pc not the stop_pc to check whether we hit a 
	     genuine breakpoint or is it really a random signal.
	     stop_pc would be in either dummy frame/the function called
	     by user in CLC.
	     The cmd_line_call_cnt greater than 0 when we stop like this. 
  	     The cmd_line_call_cnt is decremented in normal_stop() after
	     completion of the CLC issued by user.
          */
	      
          if (!trap_expected && (stop_bpstat = bpstat_stop_status (&ecs->sal.pc, 0)))
	   {
             if (bpstat_explains_signal (stop_bpstat) && cmd_line_call_cnt)
	      {
	        stop_print_frame = 0;
		printf_filtered ("Stopped due to break point hit by %s",
	    	  	         target_pid_or_tid_to_str (inferior_pid));
	        bpstat_print (stop_bpstat);
	        find_pc_partial_function (ecs->sal.pc, &ecs->stop_func_name,
					  &ecs->stop_func_start, &ecs->stop_func_end);
		printf_filtered ("%s () at %s:%d\n", ecs->stop_func_name, 
			         ecs->sal.symtab->filename,
				 ecs->sal.line);
		stop_stepping (ecs); 
	        return;
              }	
           }   	
          /* Hhhmmm..This check is also required. This is to avoid Bogus sigtrap
	     message when user issue another CLC before successfuly completing
             previously issued CLC to some thread.
	     Though it happens rarely but i observed this and could reproduce
	     this problem 1 time out of 10 times
	  */
	  else if ((breakpoint_was_here_p (ecs->sal.pc)) && cmd_line_call_cnt)
           {
             target_resume (-1, 0, TARGET_SIGNAL_0);
             prepare_to_wait (ecs);
             return;
           }
        }

      stopped_by_random_signal = 1;

      if (signal_print[stop_signal])
	{
	  printed = 1;
	  target_terminal_ours_for_output ();
	  print_stop_reason (SIGNAL_RECEIVED, stop_signal);
	}
      if (signal_stop[stop_signal])
	{
	  stop_stepping (ecs);
	  return;
	}

      if (profile_on && stop_signal == profiler_signal)
        gather_pc_sample ();

      /* If not going to stop, give terminal back
	 if we took it away.  */
      if (printed)
        target_terminal_inferior ();

      /* Clear the signal if it should not be passed.  */
      if (signal_program[stop_signal] == 0)
        stop_signal = TARGET_SIGNAL_0;

      /* Bindu 091406:
         There used to be a step_over_function call here.
         Since no comment explains why we need
         step_over_function call here, my understanding is,
         a random signal in the middle of next can lead to a
         signal handler. We don't want to step into a signal
         handler if we are stepping over. So, we need to
         place a step_resume breakpoint at current pc, since
         we will eventually return back here after signal
         handler is called (and if it returns).
         I removed this as this code doesn't seem to be required
         and we no longer reach this point since we don't even
         get these signal events when nostop noprint is set. */
    }

  /* Handle cases caused by hitting a breakpoint.  */
#ifdef GET_LONGJMP_TARGET
  CORE_ADDR jmp_buf_pc;
#endif
  struct bpstat_what what;

  trap_expected_after_step = 0;

  what = bpstat_what (stop_bpstat);

  if (what.call_dummy)
    {
      stop_stack_dummy = 1;
      /* Also save this stop_pc to check against dummy break point pc */
      current_dummy_brk_addr = stop_pc; 
#ifdef HP_OS_BUG
      trap_expected_after_continue = 1;
#endif
    }

  switch (what.main_action)
    {
#ifdef GET_LONGJMP_TARGET
      case BPSTAT_WHAT_SET_LONGJMP_RESUME:
        /* If we hit the breakpoint at longjmp, disable it for the
	   duration of this command.  Then, install a temporary
	   breakpoint at the target of the jmp_buf. */
	disable_longjmp_breakpoint ();
	remove_breakpoints ();
	breakpoints_inserted = 0;
	if (!GET_LONGJMP_TARGET (&jmp_buf_pc))
	  {
	    keep_going (ecs);
	    return;
	  }

	/* Need to blow away step-resume breakpoint, as it
	   interferes with us */
	if (step_resume_breakpoint != NULL)
	  {
	    delete_breakpoint (step_resume_breakpoint);
	    step_resume_breakpoint = NULL;
	  }
	/* Not sure whether we need to blow this away too, but probably
	   it is like the step-resume breakpoint.  */
	if (through_sigtramp_breakpoint != NULL)
	  {
	    delete_breakpoint (through_sigtramp_breakpoint);
	    through_sigtramp_breakpoint = NULL;
	  }

#if 0
	/* FIXME - Need to implement nested temporary breakpoints */
	if (step_over_calls > 0)
	  set_longjmp_resume_breakpoint (jmp_buf_pc,
					 get_current_frame ());
	else
#endif /* 0 */
	  set_longjmp_resume_breakpoint (jmp_buf_pc, NULL);
	ecs->handling_longjmp = 1;	/* FIXME */
	keep_going (ecs);
	return;
#endif /* GET_LONGJMP_TARGET */

#ifdef GET_LONGJMP_TARGET
      case BPSTAT_WHAT_CLEAR_LONGJMP_RESUME:
      case BPSTAT_WHAT_CLEAR_LONGJMP_RESUME_SINGLE:
	remove_breakpoints ();
	breakpoints_inserted = 0;
#if 0
	/* FIXME - Need to implement nested temporary breakpoints */
	if (   step_over_calls
	    && (INNER_THAN (FRAME_FP (get_current_frame ()),
			      step_frame_address)))
	  {
	    ecs->another_trap = 1;
	    keep_going (ecs);
	    return;
	  }
#endif /* 0 */
	disable_longjmp_breakpoint ();
	ecs->handling_longjmp = 0;	/* FIXME */
	if (what.main_action == BPSTAT_WHAT_CLEAR_LONGJMP_RESUME)
	  break;
	/* else fallthrough */
#endif /* GET_LONGJMP_TARGET */

      case BPSTAT_WHAT_SINGLE:
        if (breakpoints_inserted)
	  {
	    thread_step_needed = 1;
	    /* srikanth, 000131, don't bother removing breakpoints
	       at locations other than the PC. All we want to do
	       is single step the target. Other locations don't
	       matter. */
	    /* backing out this as it may be interfering with
	       command line calls. */

	    remove_breakpoints ();
	  }
	breakpoints_inserted = 0;
	ecs->another_trap = 1;
	/* Still need to check other stuff, at least the case
	   where we are stepping and step out of the right range.  */
	break;

      case BPSTAT_WHAT_STOP_NOISY:
	stop_print_frame = 1;

        /* We are about to nuke the step_resume_breakpoint and
	   through_sigtramp_breakpoint via the cleanup chain, so
	   no need to worry about it here.  */

        stop_stepping (ecs);
	return;

      case BPSTAT_WHAT_STOP_SILENT:
	stop_print_frame = 0;

	/* We are about to nuke the step_resume_breakpoint and
	   through_sigtramp_breakpoint via the cleanup chain, so
	   no need to worry about it here.  */

	stop_stepping (ecs);
	return;

      case BPSTAT_WHAT_STEP_RESUME:
	/* This proably demands a more elegant solution, but, yeah
	   right...

	   This function's use of the simple variable
	   step_resume_breakpoint doesn't seem to accomodate
	   simultaneously active step-resume bp's, although the
	   breakpoint list certainly can.

	   If we reach here and step_resume_breakpoint is already
	   NULL, then apparently we have multiple active
	   step-resume bp's.  We'll just delete the breakpoint we
	   stopped at, and carry on.  

	   Correction: what the code currently does is delete a
	   step-resume bp, but it makes no effort to ensure that
	   the one deleted is the one currently stopped at.  MVS  */

	if (step_resume_breakpoint == NULL)
	  {
	    step_resume_breakpoint =
		bpstat_find_step_resume_breakpoint (stop_bpstat);
	  }
	delete_breakpoint (step_resume_breakpoint);
	step_resume_breakpoint = NULL;
	ecs->saved_step_range_start = ecs->saved_step_range_end 
		= step_range_start = step_range_end = stop_pc;
	break;

      case BPSTAT_WHAT_THROUGH_SIGTRAMP:
	if (through_sigtramp_breakpoint)
	  delete_breakpoint (through_sigtramp_breakpoint);
	through_sigtramp_breakpoint = NULL;

	/* If were waiting for a trap, hitting the step_resume_break
	   doesn't count as getting it.  */
	if (trap_expected)
	  ecs->another_trap = 1;
	break;

      case BPSTAT_WHAT_CHECK_SHLIBS:
      case BPSTAT_WHAT_CHECK_SHLIBS_RESUME_FROM_HOOK:
#ifdef SOLIB_ADD
        extern void * auto_solib_readd;
	/* Remove breakpoints, we eventually want to step over the
	   shlib event breakpoint, and SOLIB_ADD might adjust
	   breakpoint addresses via breakpoint_re_set.  */
	if (breakpoints_inserted)
	  remove_breakpoints ();
	breakpoints_inserted = 0;

	/* Check for any newly added shared libraries if we're
	   supposed to be adding them automatically.  */
	if (auto_solib_add)
	  {
	    /* Switch terminal for any messages produced by
	       breakpoint_re_set.  */
	    target_terminal_ours_for_output ();
	    SOLIB_ADD (NULL, 0, NULL);
	    target_terminal_inferior ();
	  }

#ifdef SOLIB_READD
        /* srikanth, if the user manually loaded libraries,
           reload them automatically, after a rerun. We get
           here not only after a rerun, but also for shl_loads.
           In the latter case, this is just overhead. FIXME.
         */
        if (auto_solib_readd)
          {
	    target_terminal_ours_for_output ();
	    SOLIB_READD ();
	    target_terminal_inferior ();
          }
#endif

#ifdef HPPA_FIX_AND_CONTINUE
#ifndef GDB_TARGET_IS_HPPA_20W
	if (get_reapply_fix() && (read_pc () == _start_addr))
	  {
	    load_fixed_libraries();
	    set_reapply_fix (false);
          }
#endif
#endif

        /* Try to reenable shared library breakpoints, additional
           code segments in shared libraries might be mapped in now. */
        re_enable_breakpoints_in_shlibs ();

        /* If requested, stop when the dynamic linker notifies
           gdb of events.  This allows the user to get control
           and place breakpoints in initializer routines for
           dynamically loaded objects (among other things).  */
        if (stop_on_solib_events)
          {
      	    stop_print_frame = 0;
	    ecs->resume_stepping_when_loaded = NULL;
	    stop_stepping (ecs);
	    return;
          }

        /* If we stopped due to an explicit catchpoint, then the
           (see above) call to SOLIB_ADD pulled in any symbols
           from a newly-loaded library, if appropriate.

           We do want the inferior to stop, but not where it is
           now, which is in the dynamic linker callback.  Rather,
           we would like it stop in the user's program, just after
           the call that caused this catchpoint to trigger.  That
           gives the user a more useful vantage from which to
           examine their program's state. */
        else if (   what.main_action
                 == BPSTAT_WHAT_CHECK_SHLIBS_RESUME_FROM_HOOK)
          {
      	    /* ??rehrauer: If I could figure out how to get the
	       right return PC from here, we could just set a temp
	       breakpoint and resume.  I'm not sure we can without
	       cracking open the dld's shared libraries and sniffing
	       their unwind tables and text/data ranges, and that's
	       not a terribly portable notion.

	       Until that time, we must step the inferior out of the
	       dld callback, and also out of the dld itself (and any
	       code or stubs in libdld.sl, such as "shl_load" and
	       friends) until we reach non-dld code.  At that point,
	       we can stop stepping. */
	    bpstat_get_triggered_catchpoints (stop_bpstat,
				  &ecs->stepping_through_solib_catchpoints);
	    ecs->stepping_through_solib_after_catch = 1;

	    /* Be sure to lift all breakpoints, so the inferior does
	       actually step past this point... */
	    ecs->another_trap = 1;
	    ecs->resume_stepping_when_loaded = NULL;
	    break;
          }
        else
          {
#ifdef IMPLICIT_SHLIB_EVENT_NOTIFICATION_LACKING
            /*
             * srikanth, 980829, CLLbs14756 : on HP-UX 10.20 and
             * 11.00, dld does not invoke the debugger hooks for
             * implicit loads and unloads of shared libraries the
             * application is linked against ...
             *
             * The workaround implemented here is to break on _start
             * and if there are any 'catch load' commands active, see
             * if any of the libraries that the dld has loaded at
             * startup time are of interest.
             *
             */
            if (stopped_at_start(stop_pc))
              {
                extern boolean caught_active_load();

                target_terminal_ours_for_output ();
                if (caught_active_load())
                  {
                    stop_print_frame = 0;
                    ecs->resume_stepping_when_loaded = NULL;
                    stop_stepping (ecs);
	            target_terminal_inferior ();
	            return;
                  }
	        target_terminal_inferior ();
              }

	    /* JAGaa80453/JAGaf03519 - To catch the statically unloaded
               libraries */

            /* HP-UX 10.20 and 11.00, dld does not invoke the debugger hooks
             * for implicit loads and unloads of shared libraries the
             * application is linked against ...
             *
             * The workaround implemented here is to break on _exit
             * and if there are any 'catch unload' commands active, see
             * if any of the libraries that has been unloaded at
             * exit time are of interest.
             */
            if (stopped_at_end(stop_pc))
              {
                extern boolean caught_active_unload();

                target_terminal_ours_for_output ();
                if (caught_active_unload())
                  {
                    stop_print_frame = 0;
                    ecs->resume_stepping_when_loaded = NULL;
                    stop_stepping (ecs);
	            target_terminal_inferior ();
		    return;
                  }
	 	target_terminal_inferior ();
              }
#endif

            /* RM: If we have loaded a shared library that
               causes us to resume stepping (see
               ecs->resume_stepping_when_loaded), set a
               step_resume_breakpoint, and go */
            if (ecs->resume_stepping_when_loaded)
              {
                struct minimal_symbol *m;
                struct symtab_and_line sr_sal;

                m = lookup_minimal_symbol (
                      SYMBOL_NAME (ecs->resume_stepping_when_loaded),
                      NULL,
                      NULL);
                if (m && m->type != mst_solib_trampoline)
                  {
                    ecs->resume_stepping_when_loaded = NULL;
                    INIT_SAL (&sr_sal);   /* initialize to zeroes */
                    sr_sal.pc = SYMBOL_VALUE_ADDRESS(m);
                    sr_sal.section = find_pc_overlay (sr_sal.pc);
                    step_resume_breakpoint =
                      set_momentary_breakpoint (sr_sal,
                                                NULL,
                                                bp_step_resume);
                    if (breakpoints_inserted)
                      insert_breakpoints ();
                    step_range_start = ecs->saved_step_range_start;
                    step_range_end = ecs->saved_step_range_end;
                  }
              }
#ifdef CONTINUE_AT_DYNLINK_HOOK_RETURN_PC
            else
              {
                /* Srikanth, Aug 20th 2002.  Fix for JAGae39135. See
                   the function dynlink_hook_return_pc for details.
                 */

	        if (what.main_action == BPSTAT_WHAT_CHECK_SHLIBS)
                  {
                    CORE_ADDR hook_return_pc;
                    hook_return_pc = dynlink_hook_return_pc (stop_pc);
                    if (hook_return_pc)
                      {
                        write_pc_pid (hook_return_pc, ecs->pid);
                        keep_going (ecs);
	                return;
                      }
                  }
              }
#endif
	    /* We want to step over this breakpoint, then keep going.  */
	    ecs->another_trap = 1;
	    break;
          }
#else /* SOLIB_ADD */
        break;
#endif /* !SOLIB_ADD */

      default:
	break;
    } /* switch */

  /* We come here if we hit a breakpoint but should not
     stop for it.  Possibly we also were stepping
     and should stop for that.  So fall through and
     test for stepping.  But, if not stepping,
     do not stop.  */

#ifdef TARGET_MMAP_HOOK      
    /* RM: If we are at an mmap breakpoint, give target code a
       chance to record what's happened */
    if (   stop_bpstat && stop_bpstat->breakpoint_at
        && stop_bpstat->breakpoint_at->type == bp_mmap)
      TARGET_MMAP_HOOK();
#endif

  /* Are we stepping to get the inferior out of the dynamic
     linker's hook (and possibly the dld itself) after catching
     a shlib event? */
  if (ecs->stepping_through_solib_after_catch)
    {
#if defined(SOLIB_ADD)
      /* Have we reached our destination?  If not, keep going. */
      if (SOLIB_IN_DYNAMIC_LINKER (ecs->pid, stop_pc))
	{
	  ecs->another_trap = 1;
	  keep_going (ecs);
	  return;
	}
#endif
      /* Else, stop and report the catchpoint(s) whose triggering
	 caused us to begin stepping. */
      ecs->stepping_through_solib_after_catch = 0;
      bpstat_clear (&stop_bpstat);
      stop_bpstat = bpstat_copy (ecs->stepping_through_solib_catchpoints);
      bpstat_clear (&ecs->stepping_through_solib_catchpoints);
      stop_print_frame = 1;
      stop_stepping (ecs);
      return;
    }

  if (!CALL_DUMMY_BREAKPOINT_OFFSET_P)
    {
      /* This is the old way of detecting the end of the stack dummy.
	 An architecture which defines CALL_DUMMY_BREAKPOINT_OFFSET gets
	 handled above.  As soon as we can test it on all of them, all
	 architectures should define it.  */

      /* If this is the breakpoint at the end of a stack dummy,
	 just stop silently, unless the user was doing an si/ni, in which
	 case she'd better know what she's doing.  */

      if (   CALL_DUMMY_HAS_COMPLETED (stop_pc, read_sp (),
				       FRAME_FP (get_current_frame ()))
	  && !step_range_end)
	{
	  stop_print_frame = 0;
	  stop_stack_dummy = 1;
#ifdef HP_OS_BUG
	  trap_expected_after_continue = 1;
#endif
	  stop_stepping (ecs);
	  return;
	}
    }

  if (step_resume_breakpoint)
    {
      /* Having a step-resume breakpoint overrides anything
	 else having to do with stepping commands until
	 that breakpoint is reached.  */
      /* I'm not sure whether this needs to be check_sigtramp2 or
	 whether it could/should be keep_going.  */
      check_sigtramp2 (ecs);
      keep_going (ecs);
      return;
    }
    
  if (step_range_end == 0)
    {
      /* Likewise if we aren't even stepping.  */
      /* I'm not sure whether this needs to be check_sigtramp2 or
         whether it could/should be keep_going.  */
      check_sigtramp2 (ecs);
      keep_going (ecs);
      return;
    }

  /* If stepping through a line, keep going if still within it.

     Note that step_range_end is the address of the first instruction
     beyond the step range, and NOT the address of the last instruction
     within it! */
  if (   stop_pc >= step_range_start
      && stop_pc < step_range_end)
    {
      /* RM: We are still in the step range. The
	 step_frame_address should not have changed. But if it has
	 for some reason (weird code generation that fiddles with
	 sp for no good reason), reset it here */
      step_frame_address = FRAME_FP (get_current_frame());
#ifdef REGISTER_STACK_ENGINE_FP
      step_frame_rse_addr= get_current_frame ()->rse_fp;
#endif
          
      /* We might be doing a BPSTAT_WHAT_SINGLE and getting a signal.
	 So definately need to check for sigtramp here.  */
      check_sigtramp2 (ecs);
      keep_going (ecs);
      return;
    }

  /* We stepped out of the stepping range.  */

  /* If we are stepping at the source level and entered the runtime
     loader dynamic symbol resolution code, we keep on single stepping
     until we exit the run time loader code and reach the callee's
     address.  */
  if (step_over_calls < 0 && IN_SOLIB_DYNSYM_RESOLVE_CODE (stop_pc))
    {
      CORE_ADDR pc_after_resolver = SKIP_SOLIB_RESOLVER (stop_pc);

      if (pc_after_resolver)
        {
          /* Set up a step-resume breakpoint at the address
             indicated by SKIP_SOLIB_RESOLVER.  */
          struct symtab_and_line sr_sal;
          INIT_SAL (&sr_sal);
          sr_sal.pc = pc_after_resolver;

          check_for_old_step_resume_breakpoint ();
          step_resume_breakpoint =
            set_momentary_breakpoint (sr_sal, NULL, bp_step_resume);
          if (breakpoints_inserted)
            insert_breakpoints ();
        }

      keep_going (ecs);
      return;
    }

  /* We can't update step_sp every time through the loop, because
     reading the stack pointer would slow down stepping too much.
     But we can update it every time we leave the step range.  */
  ecs->update_step_sp = 1;

  /* Did we just take a signal?  */
  /* RM: Added use of #ifdef below so we use the correct
     pc/function name for HPUX */
  return_pc = stopped_by_random_signal ?
                prev_pc :
	        ADDR_BITS_REMOVE (SAVED_PC_AFTER_CALL (get_current_frame ()));
#ifdef OVERLOADED_LONG_BRANCH_STUBS
  /* srikanth, on PA32 we have to resort to AI to distinguish between
     two flavors of long branch stubs that get encoded the same way in
     the unwind table.
  */
  return_pc = fix_pc_if_in_long_branch_stub (stop_pc, prev_pc, return_pc);
#endif
#ifdef USE_RETURN_POINTER_TO_CHECK_FOR_SIGTRAMP 
  /* RM: Additional check: are we at the first instruction of the signal
     handler? If not, we may have a valid reason to stop there (such as
     the user setting a breakpoint there) */
  frame = get_current_frame();
  if (frame->frame != 0)
    {
      return_func_name = 0;
      find_pc_partial_function (return_pc, &return_func_name, 0, 0);
    }
  if (   frame->frame != 0
      && (stop_pc == ecs->stop_func_start)
      && !IN_SIGTRAMP (stop_pc, ecs->stop_func_name)
      && IN_SIGTRAMP (return_pc, return_func_name)
      && !IN_SIGTRAMP (prev_pc, prev_func_name))
#else      
  if (   IN_SIGTRAMP (stop_pc, ecs->stop_func_name)
      && !IN_SIGTRAMP (prev_pc, prev_func_name)
      && INNER_THAN (read_sp (), step_sp))
#endif
    {
      /* We've just taken a signal; go until we are back to
         the point where we took it and one more.  */
	
      /* RM: following no longer true. The test succeeds _only_
         when we step into a signal handler */
    
      /* Note: The test above succeeds not only when we stepped
         into a signal handler, but also when we step past the last
         statement of a signal handler and end up in the return stub
         of the signal handler trampoline.  To distinguish between
         these two cases, check that the frame is INNER_THAN the
         previous one below. pai/1997-09-11 */

      /* RM: on HP with gcc, we don't get the right frame
         pointer when we are stopped in the prologue -- and we
         are certainly in the prologue of the signal handler
         if we just took a signal. Changing this test to check
         for INNER_THAN or equal to */
      CORE_ADDR current_frame = FRAME_FP (get_current_frame());

      if (   INNER_THAN (current_frame, step_frame_address)
          || (current_frame == step_frame_address))
        {
          /* We have just taken a signal; go until we are back to
             the point where we took it and one more.  */

          /* This code is needed at least in the following case:
             The user types "next" and then a signal arrives (before
             the "next" is done).  */

          /* Note that if we are stopped at a breakpoint, then we need
             the step_resume breakpoint to override any breakpoints at
             the same location, so that we will still step over the
             breakpoint even though the signal happened.  */
          struct symtab_and_line sr_sal = {0};

          INIT_SAL (&sr_sal);
          sr_sal.symtab = NULL;
          sr_sal.line = 0;
          sr_sal.pc = prev_pc;
          /* We could probably be setting the frame to
             step_frame_address; I don't think anyone thought to
             try it.  */
          check_for_old_step_resume_breakpoint ();
          step_resume_breakpoint =
            set_momentary_breakpoint (sr_sal, NULL, bp_step_resume);
	  if (breakpoints_inserted)
        /* JAGag03846: For stepi or nexti, we need to go past sigtramp and then stop,
           instead of keep_going with next pc */
	if (single_stepping && step_range_end == 1)
          {  
            sigtramp_stepi_again = true;
	    return;
          }

	    insert_breakpoints ();
        }

      /* If this is stepi or nexti, make sure that the stepping range
	 gets us past that instruction.  */
      if (step_range_end == 1)
        {
	  /* FIXME: Does this run afoul of the code below which, if
	     we step into the middle of a line, resets the stepping
	     range?  */
	  step_range_end = (step_range_start = prev_pc) + 1;
        }
    
      ecs->remove_breakpoints_on_following_step = 1;
      keep_going (ecs);
      return;
    }

  /* RM: Did we just step out of a signal handler? */
  if (   IN_SIGTRAMP (stop_pc, ecs->stop_func_name)
      && !IN_SIGTRAMP (prev_pc, prev_func_name))
    {
      /* We just stepped out of a signal handler and into its calling
	 trampoline. */
             
      /* RM: Hmmm, why don't we just set a step resume
       * breakpoint at the return point of the trampoline?  The
       * current frame is a sigtramp frame, so we should have
       * the return PC in the sigcontext structure.
       * RM: Turns out this works on PA64, but not on PA32 (at
       * least not on 10.20) */
#ifdef GDB_TARGET_IS_HPPA_20W
      struct symtab_and_line sr_sal;
      sr_sal.symtab = NULL;
      sr_sal.line = 0;
      FRAME_SAVED_PC_IN_SIGTRAMP(get_current_frame(), &sr_sal.pc);
      sr_sal.pc = ADDR_BITS_REMOVE(sr_sal.pc);
      check_for_old_step_resume_breakpoint ();
      step_resume_breakpoint =
	  set_momentary_breakpoint (sr_sal, NULL, bp_step_resume);
      if (breakpoints_inserted)
        insert_breakpoints ();
#else
      /* Normally, we'd jump to step_over_function from here,
         but for some reason GDB can't unwind the stack correctly to find
         the real PC for the point user code where the signal trampoline will
         return -- FRAME_SAVED_PC fails, at least on HP-UX 10.20.  But signal
         trampolines are pretty small stubs of code, anyway, so it's OK
         instead to just single-step out.  Note: assuming such trampolines
         don't exhibit recursion on any platform... */ 
      find_pc_partial_function (stop_pc,
                                &(ecs->stop_func_name),
      			        &(ecs->stop_func_start),
				&(ecs->stop_func_end));
      /* Readjust stepping range */ 
      step_range_start = ecs->stop_func_start;
      step_range_end = ecs->stop_func_end;
      ecs->stepping_through_sigtramp = 1;
#endif
      /* If this is stepi or nexti, make sure that the stepping range
         gets us past that instruction.  */
      if (step_range_end == 1)
        {
          /* FIXME: Does this run afoul of the code below which, if
             we step into the middle of a line, resets the stepping
             range?  */
          step_range_end = (step_range_start = prev_pc) + 1;
        }

      ecs->remove_breakpoints_on_following_step = 1;
      keep_going (ecs);
      return;
    }

  /* 6/18/99 coulter - The test stop_func_start == 0 is intended to
     check if we have stepped into a function in a stripped file.
     The additional check return_pc != stop_pc is to check that
     we haven't returned to a function in a stripped file.  Also
     in_prologue returns TRUE if it doesn't know, so the same check
     is added after the call to in_prologue. */
  if (   (   stop_pc == ecs->stop_func_start		/* Quick test */
          || (   in_prologue (stop_pc, ecs->stop_func_start)
              && !(single_stepping && step_over_calls)  /* handle, when 'ni' in prologue. */
              && return_pc != stop_pc
              && !IN_SOLIB_RETURN_TRAMPOLINE (stop_pc,
                                              ecs->stop_func_name)
             )
          || IN_SOLIB_CALL_TRAMPOLINE (stop_pc, ecs->stop_func_name)
#if defined(COVARIANT_SUPPORT) && defined(HP_IA64) /* Skip cov thunks. */
          || IS_PC_IN_COVARIANT_THUNK (stop_pc)
#endif
          || (ecs->stop_func_start == 0 && return_pc != stop_pc)
          || ecs->stop_func_name == 0
         )
#ifdef GDB_TARGET_IS_HPPA_20W	 
      && !(   SOLIB_IN_DYNAMIC_LINKER (ecs->pid, stop_pc)
           && ((stop_pc >> 62) != 2)) )
#elif defined HP_IA64 
      && !(   SOLIB_IN_DYNAMIC_LINKER (ecs->pid, stop_pc)
           && !debugging_dld) )
#else
      && !SOLIB_IN_DYNAMIC_LINKER (ecs->pid, stop_pc ) )
#endif	
    {
      /* It's a subroutine call.  */

      if (step_over_calls == 0)
        {
          /* I presume that step_over_calls is only 0 when we're
             supposed to be stepping at the assembly language level
             ("stepi").  Just stop.  */
          stop_step = 1;
          print_stop_reason (END_STEPPING_RANGE, 0);
          stop_stepping (ecs);
          return;
        }

      if (   (step_over_calls > 0 || IGNORE_HELPER_CALL (stop_pc))
#if defined(COVARIANT_SUPPORT) && defined(HP_IA64)/* Skip cov thunks. */
	  && !is_pc_in_covariant_thunk_return_path (stop_pc)
#endif
	 )
	{
	  /* We're doing a "next".  */

	  if (   IN_SIGTRAMP (stop_pc, ecs->stop_func_name)
	      && INNER_THAN (step_frame_address, read_sp()))
	    {
	      /* We stepped out of a signal handler, and into its
		 calling trampoline.  This is misdetected as a
		 subroutine call, but stepping over the signal
		 trampoline isn't such a bad idea.  In order to do
		 that, we have to ignore the value in
		 step_frame_address, since that doesn't represent the
		 frame that'll reach when we return from the signal
		 trampoline.  Otherwise we'll probably continue to the
		 end of the program.  */

	      step_frame_address = 0;
#ifdef REGISTER_STACK_ENGINE_FP
	      step_frame_rse_addr= 0;
#endif
	    }

	  step_over_function (ecs);
	  keep_going (ecs);
	  return;
	}

      /* If we are in a function call trampoline (a stub between
	 the calling routine and the real function), locate the real
	 function.  That's what tells us (a) whether we want to step
	 into it at all, and (b) what prologue we want to run to
	 the end of, if we do step into it.  */
      tmp = SKIP_TRAMPOLINE_CODE (stop_pc);
#ifdef START_BOR_CALL
      /* Support for PA64 GDB to step into BOR calls. */
#ifdef GDB_TARGET_IS_HPPA_20W
      if (   tmp
          && !lookup_minimal_symbol_by_pc (tmp)
          && (START_BOR_CALL (stop_pc) || START_BOR_CALL (tmp) ) )
#else
      if (START_BOR_CALL (stop_pc) || START_BOR_CALL (tmp))
#endif
	{ 
          /* We are stepping into the bind-on-reference routine.
	     When we get the bind-on-reference completion event, we can 
	     figure out whether we want to break on where we are going 
	     or the return address.  Remember what we need to know later,
	     then let the process resume.
	   */
	  CORE_ADDR return_address;
	    
	  pending_bor_event_t *new_pending_bor = (pending_bor_event_t *)
	    xmalloc (sizeof (pending_bor_event_t));

	  return_address = ecs->random_signal 
	    ? prev_pc
	    : ADDR_BITS_REMOVE (SAVED_PC_AFTER_CALL (get_current_frame ()));

	  new_pending_bor->return_address = return_address;
	  new_pending_bor->thread_id = inferior_pid; 
	  new_pending_bor->next = bor_events_pending;
	  bor_events_pending = new_pending_bor;

	  /* Make sure stepping stops right away */
	  step_range_end = step_range_start = 0;

	  keep_going (ecs);
	  return;
	}
#endif

      /* RM: We are in a trampoline, but don't know its target
	 yet. Set up the variable ecs->resume_stepping_when_loaded so that
	 the code handling solib events causes control to come back
	 here after the necessary shared library should has been
	 loaded. */
      if (tmp == ((CORE_ADDR)(long) -1)) 
        {
	  ecs->resume_stepping_when_loaded =
	    lookup_minimal_symbol_by_pc (stop_pc);
	  ecs->saved_step_range_start = step_range_start;
	  ecs->saved_step_range_end = step_range_end;
	  step_range_start = 0;
	  step_range_end = 0;
	  insert_breakpoints ();
	  keep_going (ecs);
	  return;
	}      
      
      if (tmp != 0)
	ecs->stop_func_start = tmp;
      else
	{
	  tmp = DYNAMIC_TRAMPOLINE_NEXTPC (stop_pc);
	  if (tmp)
	    {
	      struct symtab_and_line sal;
	      INIT_SAL (&sal);	/* initialize to zeroes */
	      sal.pc = tmp;
	      sal.section = find_pc_overlay (sal.pc);
	      check_for_old_step_resume_breakpoint ();
	      step_resume_breakpoint =
	      set_momentary_breakpoint (sal, NULL, bp_step_resume);
	      insert_breakpoints ();
	      keep_going (ecs);
	      return;
	    }
        }

      /* If we have line number information for the function we
	 are thinking of stepping into, step into it.

	 If there are several symtabs at that PC (e.g. with include
	 files), just want to know whether *any* of them have line
	 numbers.  find_pc_line handles this.  */
      struct symtab_and_line tmp_sal;

      tmp_sal = find_pc_line (ecs->stop_func_start, 0);
      if (tmp_sal.line != 0)
	{
	  /* Steplast support for "C" language 
	      Step into the (desired) function only if the steplast_pc
	      matches with the "rp" of the current frame, when
	      a user does a steplast.
           */
          if (   (   steplast_pc
	          && (    steplast_pc
                       == ((read_register (RP_REGNUM)) & ~0x3)))
	      || !steplast_pc )
             {
	       step_into_function (ecs);	
               steplast_pc = 0;
	       return;
             }
	}
      step_over_function (ecs);
      keep_going (ecs);
      return;
    }

  /* We've wandered out of the step range.  */

  ecs->sal = find_pc_line (stop_pc, 0);

  if (step_range_end == 1)
    {
      /* It is stepi or nexti.  We always want to stop stepping after
         one instruction.  */
      stop_step = 1;
      print_stop_reason (END_STEPPING_RANGE, 0);
      stop_stepping (ecs);
      return;
    }

  /* If we're in the return path from a shared library trampoline,
     we want to proceed through the trampoline when stepping.  */
  if (   IN_SOLIB_RETURN_TRAMPOLINE (stop_pc, ecs->stop_func_name)
#if defined(COVARIANT_SUPPORT) && defined(GDB_TARGET_IS_HPPA)
          /* Returning from a Co-variant function is almost same as 
             returning from a shared library trampoline here. So, if 
             the stop_pc happens to be in the return trampoline of 
             co-variant function we proceed through the trampoline
           */ 
      || IS_PC_IN_COVARIANT_THUNK (stop_pc)
#endif
     )
    {
      CORE_ADDR tmp;

      /* Determine where this trampoline returns.  */
      tmp = SKIP_TRAMPOLINE_CODE (stop_pc);

      /* Only proceed through if we know where it's going.  */
      if (tmp)
        {
          /* And put the step-breakpoint there and go until there. */
          set_step_breakpoint (tmp, ecs);

          /* Restart without fiddling with the step ranges or
             other state.  */
          keep_going (ecs);
          return;
        }
    }

  if (ecs->sal.line == 0)
    {
      /* We have no line number information.  That means to stop
         stepping (does this always happen right after one instruction,
         when we do "s" in a function with no line numbers,
         or can this happen as a result of a return or longjmp?).  */
      stop_step = 1;
      print_stop_reason (END_STEPPING_RANGE, 0);
      stop_stepping (ecs);
      return;
    }

  /* If we just stepped out of a noninline function,
     reset the next_command_inline_idx to the caller. */
  if (   inline_debugging
      && executing_next_command
      && next_command_inline_idx == 0
      && (  step_frame_address != get_current_frame()->frame
#ifdef REGISTER_STACK_ENGINE_FP
	  || step_frame_rse_addr != get_current_frame()->rse_fp
#endif
	 )
     )
    {
      ecs->sal = find_pc_inline_idx_line (stop_pc, -1, 0);
      next_command_inline_idx = ecs->sal.inline_idx;
      set_current_frame (NULL);
      get_current_frame();
    }


  /* ??? RM: I am not sure that the following comment is true. Certainly,
     this causes problems when the first instruction of a line is
     nullified. Let it be for now. */
  /* CM: To stop or not to stop, that is the question. */
  /* CM: Always stop if the line number changed. When debugging
     optimized code, one often lands in the middle of a line. */
  if (   (stop_pc == ecs->sal.pc)
      && (   ecs->current_line != ecs->sal.line
          || ecs->current_symtab != ecs->sal.symtab))
    {
      /* We are at the start of a different line.  So stop.  Note that
	 we don't stop if we step into the middle of a different line.
	 That is said to make things like for (;;) statements work
	 better.  */
      stop_step = 1;
      print_stop_reason (END_STEPPING_RANGE, 0);
      stop_stepping (ecs);
      return;
    }

  /* We aren't done stepping.

     Optimize by setting the stepping range to the line.
     (We might not be in the original line, but if we entered a
     new line in mid-statement, we continue stepping.  This makes
     things like for(;;) statements work better.)  */

  if (ecs->stop_func_end && ecs->sal.end >= ecs->stop_func_end)
    {
      /* If this is the last line of the function, don't keep stepping
	 (it would probably step us out of the function).
	 This is particularly necessary for a one-line function,
	 in which after skipping the prologue we better stop even though
	 we will be in mid-line.  */
      stop_step = 1;
      print_stop_reason (END_STEPPING_RANGE, 0);
      stop_stepping (ecs);
      return;
    }

#if defined (INLINE_SUPPORT) && defined (GDB_TARGET_IS_HPPA)
  if (step_start_function_inline_index != get_current_frame()->inline_idx)
    {
      /* A word (OK, several words) on this business about continuing
         stepping if we land in the middle of a line :

         At least with the PA code generator, sometimes we step out of the
         inlined call and land in the middle of the line in the inline 
         caller.  If we continue to step to the next line, it results in
         confusing behavior. So stop -- Baskar & Srikanth. Jul 12 2005.

         May be we need this for IPF too ?? FIXME.
       */
        
      stop_step = 1;
      print_stop_reason (END_STEPPING_RANGE, 0);
      stop_stepping (ecs);
      return;
    }
#endif

  /* Bindu 061604: Don't reset the step_range_start here. Resetting it will
     effect the until command. JAGaf28236.
     JAGaf55060: Reset it only when we get into a new function. */

  if (ecs->stop_func_start != prev_stop_func_start)
    step_range_start = ecs->sal.pc;
  step_range_end = ecs->sal.end;
  step_frame_address = FRAME_FP (get_current_frame ());
#ifdef REGISTER_STACK_ENGINE_FP
  step_frame_rse_addr= get_current_frame ()->rse_fp;
#endif
  ecs->current_line = ecs->sal.line;
  ecs->current_symtab = ecs->sal.symtab;

  /* In the case where we just stepped out of a function into the middle
     of a line of the caller, continue stepping, but step_frame_address
     must be modified to current frame */
 {
    CORE_ADDR current_frame = FRAME_FP (get_current_frame ());
#ifdef REGISTER_STACK_ENGINE_FP
    CORE_ADDR current_rse= get_current_frame ()->rse_fp;
#endif
    if (!(INNER_THAN (current_frame, step_frame_address)))
      step_frame_address = current_frame;
#ifdef REGISTER_STACK_ENGINE_FP
    if (!(INNER_THAN (current_rse, step_frame_rse_addr)))
      step_frame_rse_addr= current_rse;
#endif
  }

  keep_going (ecs);
}

/* Are we in the middle of stepping?  */

static int
currently_stepping (struct execution_control_state *ecs)
{
  return ((through_sigtramp_breakpoint == NULL
	   && !ecs->handling_longjmp
	   && ((step_range_end && step_resume_breakpoint == NULL)
	       || trap_expected))
	  || ecs->stepping_through_solib_after_catch
	  || bpstat_should_step ());
}

static void
check_sigtramp2 (struct execution_control_state *ecs)
{
  /* RM: Added use of #ifdef below so we use the correct
     pc/function name for HPUX */
#ifdef USE_RETURN_POINTER_TO_CHECK_FOR_SIGTRAMP
  return_pc = stopped_by_random_signal ? prev_pc :
	      ADDR_BITS_REMOVE (SAVED_PC_AFTER_CALL (get_current_frame ()));
  return_func_name = 0;
  find_pc_partial_function (return_pc, &return_func_name, 0, 0);
  if (trap_expected &&
      /* RM: Additional check: are we at the first instruction of
	 the signal handler? If not, we may have a valid reason to
	 stop there (such as the user setting a breakpoint there) */
      stop_pc == ecs->stop_func_start &&
      !IN_SIGTRAMP (stop_pc, ecs->stop_func_name)
      && IN_SIGTRAMP (return_pc, return_func_name)
      && !IN_SIGTRAMP (prev_pc, prev_func_name))
#else        
  if (trap_expected
      && IN_SIGTRAMP (stop_pc, ecs->stop_func_name)
      && !IN_SIGTRAMP (prev_pc, prev_func_name)
      && INNER_THAN (read_sp (), step_sp))
#endif
    {
      /* What has happened here is that we have just stepped the
	 inferior with a signal (because it is a signal which
	 shouldn't make us stop), thus stepping into sigtramp.

	 So we need to set a step_resume_break_address breakpoint and
	 continue until we hit it, and then step.  FIXME: This should
	 be more enduring than a step_resume breakpoint; we should
	 know that we will later need to keep going rather than
	 re-hitting the breakpoint here (see the testsuite,
	 gdb.base/signals.exp where it says "exceedingly difficult").  */

      struct symtab_and_line sr_sal;

      INIT_SAL (&sr_sal);	/* initialize to zeroes */
      sr_sal.pc = prev_pc;
      sr_sal.section = find_pc_overlay (sr_sal.pc);
      /* We perhaps could set the frame if we kept track of what the
	 frame corresponding to prev_pc was.  But we don't, so don't.  */
      through_sigtramp_breakpoint =
	set_momentary_breakpoint (sr_sal, NULL, bp_through_sigtramp);
      if (breakpoints_inserted)
	insert_breakpoints ();

      ecs->remove_breakpoints_on_following_step = 1;
      ecs->another_trap = 1;
    }
}

/* Subroutine call with source code we should not step over.  Do step
   to the first line of code in it.  */

static void
step_into_function (struct execution_control_state *ecs)
{
  struct symtab *s;
  struct symtab_and_line sr_sal;

  s = find_pc_symtab (stop_pc);
  if (s && s->language != language_asm)
    ecs->stop_func_start = SKIP_PROLOGUE (ecs->stop_func_start);

  ecs->sal = find_pc_line (ecs->stop_func_start, 0);
  /* Use the step_resume_break to step until the end of the prologue,
     even if that involves jumps (as it seems to on the vax under
     4.2).  */
  /* If the prologue ends in the middle of a source line, continue to
     the end of that source line (if it is still within the function).
     Otherwise, just go to end of prologue.  */
#ifdef PROLOGUE_FIRSTLINE_OVERLAP
  /* no, don't either.  It skips any code that's legitimately on the
     first line.  */
#else
  if (ecs->sal.end
      && ecs->sal.pc != ecs->stop_func_start
      && ecs->sal.end < ecs->stop_func_end)
    ecs->stop_func_start = ecs->sal.end;
#endif

  if (ecs->stop_func_start == stop_pc)
    {
      /* We are already there: stop now.  */
      stop_step = 1;
      print_stop_reason (END_STEPPING_RANGE, 0);
      stop_stepping (ecs);
      return;
    }
  else
    {
      /* Put the step-breakpoint there and go until there.  */
      INIT_SAL (&sr_sal);	/* initialize to zeroes */
      sr_sal.pc = ecs->stop_func_start;
      sr_sal.section = find_pc_overlay (ecs->stop_func_start);
      /* Do not specify what the fp should be when we stop since on
	 some machines the prologue is where the new fp value is
	 established.  */
      check_for_old_step_resume_breakpoint ();
      step_resume_breakpoint =
	set_momentary_breakpoint (sr_sal, NULL, bp_step_resume);
      if (breakpoints_inserted)
	insert_breakpoints ();

      /* And make sure stepping stops right away then.  */
      step_range_end = step_range_start;
    }
  keep_going (ecs);
}

/* We've just entered a callee, and we wish to resume until it returns
   to the caller.  Setting a step_resume breakpoint on the return
   address will catch a return from the callee.
     
   However, if the callee is recursing, we want to be careful not to
   catch returns of those recursive calls, but only of THIS instance
   of the call.

   To do this, we set the step_resume bp's frame to our current
   caller's frame (step_frame_address, which is set by the "next" or
   "until" command, before execution begins). */

static void
step_over_function (struct execution_control_state *ecs)
{
  struct symtab_and_line sr_sal;

  INIT_SAL (&sr_sal);	/* initialize to zeros */
  sr_sal.pc = stopped_by_random_signal ?
	      prev_pc
	      : ADDR_BITS_REMOVE (SAVED_PC_AFTER_CALL (get_current_frame ()));
#ifdef OVERLOADED_LONG_BRANCH_STUBS
  /* srikanth, on PA32 we have to resort to AI to distinguish between
     two flavors of long branch stubs that get encoded the same way in
     the unwind table.
  */
  sr_sal.pc = fix_pc_if_in_long_branch_stub (stop_pc, prev_pc, sr_sal.pc);
#endif
  sr_sal.section = find_pc_overlay (sr_sal.pc);

  check_for_old_step_resume_breakpoint ();
  step_resume_breakpoint =
    set_momentary_breakpoint (sr_sal,
			      stopped_by_random_signal ?
			      NULL : get_current_frame (),
			      bp_step_resume);

  if (ecs->stepping_through_sigtramp)
    {
      step_resume_breakpoint->frame = (CORE_ADDR) NULL;
#ifdef REGISTER_STACK_ENGINE_FP
                step_resume_breakpoint->rse_fp = (CORE_ADDR) NULL;
#endif
      ecs->stepping_through_sigtramp = 0;
    }
  else if (step_frame_address && !IN_SOLIB_DYNSYM_RESOLVE_CODE (sr_sal.pc))
    {
      step_resume_breakpoint->frame = step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
      step_resume_breakpoint->rse_fp= step_frame_rse_addr;
#endif
    }

  if (breakpoints_inserted)
    insert_breakpoints ();
}

static void
stop_stepping (struct execution_control_state *ecs)
{
  if (target_has_execution)
    {
      /* Are we stopping for a vfork event?  We only stop when we see
         the child's event.  However, we may not yet have seen the
         parent's event.  And, inferior_pid is still set to the
         parent's pid, until we resume again and follow either the
         parent or child.

         To ensure that we can really touch inferior_pid (aka, the
         parent process) -- which calls to functions like read_pc
         implicitly do -- wait on the parent if necessary. */
      if ((pending_follow.kind == TARGET_WAITKIND_VFORKED)
	  && !pending_follow.fork_event.saw_parent_fork)
	{
	  int parent_pid;

	  do
	    {
	      if (target_wait_hook)
		parent_pid = target_wait_hook (-1, &(ecs->ws));
	      else
		parent_pid = target_wait (-1, &(ecs->ws));
	    }
	  while (parent_pid != inferior_pid);
	}

      /* Assuming the inferior still exists, set these up for next
         time, just like we did above if we didn't break out of the
         loop.  */
      prev_pc = read_pc ();
      /* During startup for gambit (IA64), we currently get in trouble if
         we try to get prev_fp when we are stopped at the initial instruction,
         so we don't try.
       */

      prev_func_start = ecs->stop_func_start;
      prev_func_name = ecs->stop_func_name;
    }

  /* Let callers know we don't want to wait for the inferior anymore.  */
  ecs->wait_some_more = 0;
}

/* This function handles various cases where we need to continue
   waiting for the inferior.  */
/* (Used to be the keep_going: label in the old wait_for_inferior) */

static void
keep_going (struct execution_control_state *ecs)
{
  /* ??rehrauer: ttrace on HP-UX theoretically allows one to debug a
     vforked child between its creation and subsequent exit or call to
     exec().  However, I had big problems in this rather creaky exec
     engine, getting that to work.  The fundamental problem is that
     I'm trying to debug two processes via an engine that only
     understands a single process with possibly multiple threads.

     Hence, this spot is known to have problems when
     target_can_follow_vfork_prior_to_exec returns 1. */

  /* Save the pc before execution, to compare with pc after stop.  */
  ecs->saved_inferior_pid = inferior_pid;
  inferior_pid = ecs->pid;
  prev_pc = read_pc ();	/* Might have been DECR_AFTER_BREAK */
  inferior_pid = ecs->saved_inferior_pid;
  prev_func_start = ecs->stop_func_start;	/* Ok, since if DECR_PC_AFTER
						   BREAK is defined, the
						   original pc would not have
						   been at the start of a
						   function. */
  prev_func_name = ecs->stop_func_name;

  if (ecs->update_step_sp)
    step_sp = read_sp ();
  ecs->update_step_sp = 0;

  /* If we did not do break;, it means we should keep running the
     inferior and not return to debugger.  */

  if (trap_expected && stop_signal != TARGET_SIGNAL_TRAP)
    {
      /* We took a signal (which we are supposed to pass through to
	 the inferior, else we'd have done a break above) and we
	 haven't yet gotten our trap.  Simply continue.  */
      resume (currently_stepping (ecs), stop_signal);
    }
  else
    {
      /* Either the trap was not expected, but we are continuing
	 anyway (the user asked that this signal be passed to the
	 child)
	 -- or --
	 The signal was SIGTRAP, e.g. it was our signal, but we
	 decided we should resume from it.

	 We're going to run this baby now!

	 Insert breakpoints now, unless we are trying to one-proceed
	 past a breakpoint.  */
      /* If we've just finished a special step resume and we don't
	 want to hit a breakpoint, pull em out.  */
      if (step_resume_breakpoint == NULL
	  && through_sigtramp_breakpoint == NULL
	  && ecs->remove_breakpoints_on_following_step)
	{
	  ecs->remove_breakpoints_on_following_step = 0;
	  remove_breakpoints ();
	  breakpoints_inserted = 0;
	}
      else if (!breakpoints_inserted &&
	       (through_sigtramp_breakpoint != NULL || !ecs->another_trap))
	{
	  breakpoints_failed = insert_breakpoints ();
	  if (breakpoints_failed)
	    {
	      stop_stepping (ecs);
	      return;
	    }
	  breakpoints_inserted = 1;
	}

      trap_expected = ecs->another_trap;

      /* Do not deliver SIGNAL_TRAP (except when the user explicitly
	 specifies that such a signal should be delivered to the
	 target program).

	 Typically, this would occure when a user is debugging a
	 target monitor on a simulator: the target monitor sets a
	 breakpoint; the simulator encounters this break-point and
	 halts the simulation handing control to GDB; GDB, noteing
	 that the break-point isn't valid, returns control back to the
	 simulator; the simulator then delivers the hardware
	 equivalent of a SIGNAL_TRAP to the program being debugged. */

      if (stop_signal == TARGET_SIGNAL_TRAP
	  && !signal_program[stop_signal])
	stop_signal = TARGET_SIGNAL_0;

#ifdef SHIFT_INST_REGS
      /* I'm not sure when this following segment applies.  I do know,
	 now, that we shouldn't rewrite the regs when we were stopped
	 by a random signal from the inferior process.  */
      /* FIXME: Shouldn't this be based on the valid bit of the SXIP?
	 (this is only used on the 88k).  */

      if (!bpstat_explains_signal (stop_bpstat)
	  && (stop_signal != TARGET_SIGNAL_CHLD)
	  && !stopped_by_random_signal)
	SHIFT_INST_REGS ();
#endif /* SHIFT_INST_REGS */

      resume (currently_stepping (ecs), stop_signal);
    }

    prepare_to_wait (ecs);
}

/* This function normally comes after a resume, before
   handle_inferior_event exits.  It takes care of any last bits of
   housekeeping, and sets the all-important wait_some_more flag.  */

static void
prepare_to_wait (struct execution_control_state *ecs)
{
  if (ecs->infwait_state == infwait_normal_state)
    {
      overlay_cache_invalid = 1;

      /* We have to invalidate the registers BEFORE calling
	 target_wait because they can be loaded from the target while
	 in target_wait.  This makes remote debugging a bit more
	 efficient for those targets that provide critical registers
	 as part of their normal status mechanism. */

      registers_changed ();
      ecs->waiton_pid = -1;
      ecs->wp = &(ecs->ws);
    }
  /* This is the old end of the while loop.  Let everybody know we
     want to wait for the inferior some more and get called again
     soon.  */
  ecs->wait_some_more = 1;
}

/* Print why the inferior has stopped. We always print something when
   the inferior exits, or receives a signal. The rest of the cases are
   dealt with later on in normal_stop() and print_it_typical().  Ideally
   there should be a call to this function from handle_inferior_event()
   each time stop_stepping() is called.*/
static void
print_stop_reason (enum inferior_stop_reason stop_reason, int stop_info)
{
  int inf_si_code;

  switch (stop_reason)
    {
    case STOP_UNKNOWN:
      /* We don't deal with these cases from handle_inferior_event()
         yet. */
      break;
    case END_STEPPING_RANGE:
      /* We are done with a step/next/si/ni command. */
      /* For now print nothing. */
#ifdef UI_OUT
      /* Print a message only if not in the middle of doing a "step n"
	 operation for n > 1 */
      if (!step_multi || !stop_step)
	if (ui_out_is_mi_like_p (uiout))
	  ui_out_field_string (uiout, "reason", "end-stepping-range");
#endif
      break;
    case BREAKPOINT_HIT:
      /* We found a breakpoint. */
      /* For now print nothing. */
      break;
    case SIGNAL_EXITED:
      /* The inferior was terminated by a signal. */
#ifdef UI_OUT
      annotate_signalled ();
      if ( ui_out_is_mi_like_p (uiout))
	ui_out_field_string (uiout, "reason", "exited-signalled");
      ui_out_text (uiout, "\nProgram terminated with signal ");
      annotate_signal_name ();
      ui_out_field_string (uiout, "signal-name", target_signal_to_name (stop_info));
      annotate_signal_name_end ();
      ui_out_text (uiout, ", ");
      annotate_signal_string ();
      ui_out_field_string (uiout, "signal-meaning", target_signal_to_string (stop_info));
      annotate_signal_string_end ();
      ui_out_text (uiout, ".\n");
      ui_out_text (uiout, "The program no longer exists.\n");
#else
      annotate_signalled ();
      printf_filtered ("\nProgram terminated with signal ");
      annotate_signal_name ();
      printf_filtered ("%s", target_signal_to_name (stop_info));
      annotate_signal_name_end ();
      printf_filtered (", ");
      annotate_signal_string ();
      printf_filtered ("%s", target_signal_to_string (stop_info));
      annotate_signal_string_end ();
      printf_filtered (".\n");

      printf_filtered ("The program no longer exists.\n");
      gdb_flush (gdb_stdout);
#endif
      /* mark the thread RTC flag, so we can bail out without 
         looping in command_loop() in top.c for batch thread RTC*/
      batch_thread_rtc_needs_to_exit = 1;
      break;
    case EXITED:
      /* The inferior program is finished. */
#ifdef UI_OUT
      annotate_exited (stop_info);
      if (stop_info)
	{
	  if (ui_out_is_mi_like_p (uiout))
	    ui_out_field_string (uiout, "reason", "exited");
          if (!batch_rtc && !profile_on)
            {
	      ui_out_text (uiout, "\nProgram exited with code ");
	      ui_out_field_fmt (uiout, "exit-code", "0%o", (unsigned int) stop_info);
	      ui_out_text (uiout, ".\n");
            }
	}
      else
	{
	  if (ui_out_is_mi_like_p (uiout))
	    ui_out_field_string (uiout, "reason", "exited-normally");
          /* Suppressing only normal exit message because in case of
             abnormal exit, the message can provide a clue that something
             has gone wrong
           */
          if (!batch_rtc && !profile_on)
	    ui_out_text (uiout, "\nProgram exited normally.\n");
	}
#else
      annotate_exited (stop_info);
      if (stop_info)
        {
          if (!batch_rtc && !profile_on)
	    printf_filtered ("\nProgram exited with code 0%o.\n",
			     (unsigned int) stop_info);
        }
      else
        {
          if (!batch_rtc && !profile_on)
	     printf_filtered ("\nProgram exited normally.\n");
        }
#endif
      /* mark the thread RTC flag, so we can bail out without 
         looping in command_loop() in top.c for batch thread RTC*/
      batch_thread_rtc_needs_to_exit = 1;
      break;
    case SIGNAL_RECEIVED:
      /* Signal received. The signal table tells us to print about
         it. */
      if (inline_debugging)
	{
	  step_start_function_inline_index = -1;
          executing_next_command = 0;
          set_current_frame (NULL);
          get_current_frame ();
	}
#ifdef UI_OUT
      annotate_signal ();
      if (ui_out_is_mi_like_p (uiout) )
	  ui_out_field_string (uiout, "reason", "signal-received");
      ui_out_text (uiout, "\nProgram received signal ");
      annotate_signal_name ();
      ui_out_field_string (uiout, "signal-name", target_signal_to_name (stop_info));
      annotate_signal_name_end ();
      ui_out_text (uiout, ", ");
      annotate_signal_string ();
      ui_out_field_string (uiout, "signal-meaning", target_signal_to_string (stop_info));
      /* Fix for JAGae69869. */
      inf_si_code = get_inf_si_code();
      ui_out_text (uiout, "\n  si_code: ");
      ui_out_text (uiout, ltoa (inf_si_code));
      /* Fix for JAGae99882.  Bharath, 9 Feb 2004. */
      ui_out_text (uiout, " - ");
      ui_out_text (uiout, map_si_code_to_str (stop_info, inf_si_code));
      ui_out_text (uiout, ".\n");
      annotate_signal_string_end ();
#else
      annotate_signal ();
      printf_filtered ("\nProgram received signal ");
      annotate_signal_name ();
      printf_filtered ("%s", target_signal_to_name (stop_info));
      annotate_signal_name_end ();
      printf_filtered (", ");
      annotate_signal_string ();
      printf_filtered ("%s", target_signal_to_string (stop_info));
      /* Fix for JAGae69869. */
      inf_si_code = get_inf_si_code();
      printf_filtered ("\n  si_code: %s", ltoa (inf_si_code));
      /* Fix for JAGae99882.  Bharath, 9 Feb 2004. */
      printf_filtered (" - %s", map_si_code_to_str (stop_info, inf_si_code));
      printf_filtered (".\n");
      annotate_signal_string_end ();
      gdb_flush (gdb_stdout);      
#endif
      /* mark the thread RTC flag, so we can bail out without 
         looping in command_loop() in top.c for batch thread RTC*/
      batch_thread_rtc_needs_to_exit = 1;
      break;
    default:
      internal_error ("print_stop_reason: unrecognized enum value");
      break;
    }
}


/* This function returns TRUE if ep is an internal breakpoint
   set to catch generic shared library (aka dynamically-linked
   library) events.  (This is *NOT* the same as a catchpoint for a
   shlib event.  The latter is something a user can set; this is
   something gdb sets for its own use, and isn't ever shown to a
   user.)
 */
static int
is_internal_shlib_eventpoint (struct breakpoint *ep)
{
  return
    (ep->type == bp_shlib_event)
    ;
}

/* This function returns TRUE if bs indicates that the inferior
   stopped due to a shared library (aka dynamically-linked library)
   event.
 */
static int
stopped_for_internal_shlib_event (bpstat bs)
{
  /* Note that multiple eventpoints may've caused the stop.  Any
     that are associated with shlib events will be accepted.
   */
  for (; bs != NULL; bs = bs->next)
    {
      if ((bs->breakpoint_at != NULL)
	  && is_internal_shlib_eventpoint (bs->breakpoint_at))
	return 1;
    }

  /* If we get here, then no candidate was found. */
  return 0;
}

/* Here to return control to GDB when the inferior stops for real.
   Print appropriate messages, remove breakpoints, give terminal our modes.

   STOP_PRINT_FRAME nonzero means print the executing frame
   (pc, function, args, file, line number and line text).
   BREAKPOINTS_FAILED nonzero means stop was due to error
   attempting to insert breakpoints.  */

void
normal_stop (void)
{
  /* As with the notification of thread events, we want to delay
     notifying the user that we've switched thread context until
     the inferior actually stops.

     (Note that there's no point in saying anything if the inferior
     has exited!) */
  if ((previous_inferior_pid && previous_inferior_pid != inferior_pid)
      && target_has_execution)
    {
      target_terminal_ours_for_output ();
      printf_filtered ("[Switching to %s]\n",
		       target_pid_or_tid_to_str (inferior_pid));
      previous_inferior_pid = inferior_pid;
    }
  else
    previous_inferior_pid = inferior_pid;

    steplast_pc = 0;
#ifdef GDB_TARGET_IS_HPUX
  if (inferior_pid != 0 && target_has_execution && inferior_is_live ())
    /* Disable bor call-backs on normal_stop */
    SOLIB_REQUEST_BOR_CALLBACK (0);
#endif
  /* Make sure that the current_frame's pc is correct.  This
     is a correction for setting up the frame info before doing
     DECR_PC_AFTER_BREAK */
  if (target_has_execution && get_current_frame ())
    (get_current_frame ())->pc = read_pc ();

  if (breakpoints_failed)
    {
      target_terminal_ours_for_output ();
      print_sys_errmsg ("While inserting breakpoints", breakpoints_failed);
      printf_filtered ("Stopped; cannot insert breakpoints.\n\
The same program may be running in another process,\n\
or you may have requested too many hardware breakpoints\n\
and/or watchpoints.\n");
    }

  if (target_has_execution && breakpoints_inserted)
    {
      /* srikanth, 000131, We should be able to get away with
         removing just the breakpoint at PC. However it seems
         to mess up things after a command line call. Needs to
         be investigated. FIXME. */
      if (remove_breakpoints ())
	{
	  target_terminal_ours_for_output ();
	  printf_filtered ("Cannot remove breakpoints because ");
	  printf_filtered ("program is no longer writable.\n");
	  printf_filtered ("It might be running in another process.\n");
	  printf_filtered ("Further execution is probably impossible.\n");
	}
    }
  breakpoints_inserted = 0;

  /* Delete the breakpoint we stopped at, if it wants to be deleted.
     Delete any breakpoint that is to be deleted at the next stop.  */

  breakpoint_auto_delete (stop_bpstat);

  /* If an auto-display called a function and that got a signal,
     delete that auto-display to avoid an infinite recursion.  */

  if (stopped_by_random_signal)
    disable_current_display ();

  /* Don't print a message if in the middle of doing a "step n"
     operation for n > 1 */
  if (step_multi && stop_step)
    goto done;

  target_terminal_ours ();

  /* Did we stop because the user set the stop_on_solib_events
     variable?  (If so, we report this as a generic, "Stopped due
     to shlib event" message.)
   */
  if (stopped_for_internal_shlib_event (stop_bpstat))
    {
      printf_filtered ("Stopped due to shared library event\n");
    }

  /* Look up the hook_stop and run it if it exists.  */
  /* JAGaf54451 - Breakpoint not triggered using <stop in/at> command in non-dbx mode */  
  if (stop_hook_command && stop_hook_command->hook)
    {
      catch_errors (hook_stop_stub, stop_hook_command->hook,
		    "Error while running hook_stop:\n", RETURN_MASK_ALL);
    }
  /* JAGaf54451 - END */
  if (!target_has_stack)
    {

      goto done;
    }

  /* Select innermost stack frame - i.e., current frame is frame 0,
     and current location is based on that.
     Don't do this on return from a stack dummy routine,
     or if the program has exited. */

  if (!stop_stack_dummy)
    {
      select_frame (get_current_frame (), 0);

      /* Print current location without a level number, if
         we have changed functions or hit a breakpoint.
         Print source line if we have one.
         bpstat_print() contains the logic deciding in detail
         what to print, based on the event(s) that just occurred. */

      if (stop_print_frame
	  && selected_frame)
	{
	  int bpstat_ret;
	  int source_flag = 0;
	  int do_frame_printing = 1;

	  bpstat_ret = bpstat_print (stop_bpstat);
	  switch (bpstat_ret)
	    {
	    case PRINT_UNKNOWN:
	      if (stop_step
		  && step_frame_address == FRAME_FP (get_current_frame ())
#ifdef REGISTER_STACK_ENGINE_FP
		  && step_frame_rse_addr == get_current_frame ()->rse_fp
#endif
		  && step_start_function == find_pc_function (stop_pc)
#if defined (INLINE_SUPPORT)
		  && (   step_start_function_inline_index == -1
		      || step_start_function_inline_index ==
			   get_current_frame()->inline_idx )
#endif  
         )
		source_flag = SRC_LINE;   /* finished step, just print source line */
	      else
		source_flag = SRC_AND_LOC;    /* print location and source line */
	      break;
	    case PRINT_SRC_AND_LOC:
	      source_flag = SRC_AND_LOC;    /* print location and source line */
	      break;
	    case PRINT_SRC_ONLY:
	      source_flag = SRC_LINE;
	      break;
	    case PRINT_NOTHING:
	      do_frame_printing = 0;
	      break;
	    default:
	      internal_error ("Unknown value.");
	    }
#ifdef UI_OUT
	  /* For mi, have the same behavior every time we stop:
             print everything but the source line. */
	  if (ui_out_is_mi_like_p (uiout))
	    source_flag = LOC_AND_ADDRESS;
#endif

#ifdef UI_OUT
	  if (ui_out_is_mi_like_p (uiout))
	    ui_out_field_int (uiout, "thread-id", pid_to_thread_id (inferior_pid));
#endif
	  /* The behavior of this routine with respect to the source
	     flag is:
	     SRC_LINE: Print only source line
	     LOCATION: Print only location
	     SRC_AND_LOC: Print location and source line */

/* JAGag30057-Usha: Stepi command prints disassembly for debug code, but not optimized code.
 * Print the disassembly for stepi on PA, by default like IA.
 */ 
#ifdef GDB_TARGET_IS_HPPA
           if (!batch_rtc && do_frame_printing && !profile_on)
             {
              if (!have_partial_symbols () && !have_full_symbols () && single_stepping)
                { 
                  print_insn (selected_frame->pc, gdb_stdout);
                  printf_filtered ("\n");
                  show_and_print_stack_frame (selected_frame, -1, source_flag);
                }
              else
                show_and_print_stack_frame (selected_frame, -1, source_flag);
     	     }	   
#else
             if (!batch_rtc && do_frame_printing && !profile_on)
	       show_and_print_stack_frame (selected_frame, -1, source_flag);
#endif

	  /* Display the auto-display expressions.  */
	  do_displays ();
	}
    }

  /* Save the function value return registers, if we care.
     We might be about to restore their previous contents.  */
  if (proceed_to_finish)
    read_register_bytes (0, stop_registers, REGISTER_BYTES);

  if (stop_stack_dummy)
    {
#ifdef HP_IA64
      extern struct command_line_call *trap_CLC;
      struct command_line_call *temp_trap_CLC = NULL, *prev_trap_CLC = NULL;
      extern int cmd_line_call_failed_cnt;
      
      if (cmd_line_call_failed_cnt && trap_CLC != NULL)
      {
        /*For more info see: QXCR1000577394:Bogus SIGTRAP when command-
          line call on non-current thread' */
        value_ptr val;
	/* Go through the failed CLC thrds chain and find out whether the current
           thrd was failed and is it in the list, by comparing the current dummy 
           breakpoint hit address and stored dummy breakpoint addresses */
        for (temp_trap_CLC = trap_CLC; 
             temp_trap_CLC != NULL; temp_trap_CLC = temp_trap_CLC->next)
         {
	   if ((temp_trap_CLC->trap_dummy_frame_brk_addr == 
	       current_dummy_brk_addr) && 
	       (temp_trap_CLC->cmd_line_call_failed_pid == inferior_pid))
	    break;
	   else
	    {
	      prev_trap_CLC = temp_trap_CLC;
	    } 
   	 }
        if (temp_trap_CLC)
        { 
#ifdef VALUE_RETURNED_FROM_STACK
           if (temp_trap_CLC->trap_struct_return || 
              temp_trap_CLC->trap_is_ipf_cxx_abi_hand_fn_call)
            val = (value_ptr) VALUE_RETURNED_FROM_STACK (temp_trap_CLC->trap_type,
		                         temp_trap_CLC->trap_struct_addr);

            /* RM: check for retval ptr argument */
	    else if (temp_trap_CLC->trap_retval_addr)
             val = (value_ptr) VALUE_RETURNED_FROM_STACK 
	       (temp_trap_CLC->trap_type, temp_trap_CLC->trap_retval_addr);
	    else 	
#endif
            {
              char regbuf [REGISTER_BYTES];
              /* Read the registers now, we will get return values of 
                 the function called in the CLC, if the function is returning 
	         something */ 
              read_register_bytes (0, regbuf, REGISTER_BYTES);
             
	      val = (value_ptr) 
                 value_being_returned (temp_trap_CLC->trap_type, regbuf, 
				   temp_trap_CLC->trap_struct_return);
            }
	    /* Print the function name and parameters passed to the function */
            printf_filtered ("\n");
	    printf_filtered ("Warning:You have issued a command line call"
			     " \"%s (", temp_trap_CLC->func_name);
            for (int i = 0; temp_trap_CLC->args_array[i] != NULL;)
	     { 
  	       print_formatted (temp_trap_CLC->args_array[i++], 0, 0, gdb_stdout);
	       if (temp_trap_CLC->args_array[i] != NULL)
	        printf_filtered (",");
             }
	    printf_filtered (")\"\n");
            printf_filtered ("to %s. The call was interrupted \nbecause of"
			     " event[s] from the application.\n"
	 		     "The call was resumed later and" 
			     " the return value of the called function is\n\n",
			     target_pid_or_tid_to_str (temp_trap_CLC->cmd_line_call_failed_pid));
            
            /* Print the retuen value we got from either STACK or REGISTERS */
            print_formatted (val, 0, 0, gdb_stdout);
	    printf_filtered ("\n");
            cmd_line_call_failed_cnt = cmd_line_call_failed_cnt - 1; 
            
            /* Just now we handled one failed CLC. So free up the structure 
               holding the data of the handled CLC thread */
            /* The prev_trap_CLC not null means that the failed CLC just now
               we handled is not the first in the failed CLCs chain */
            if (prev_trap_CLC)
	      prev_trap_CLC->next = temp_trap_CLC->next;
  	    else
             /* prev_trap_CLC is null, means this thread is the first failed CLC
                in the list. */
 	      trap_CLC = trap_CLC->next;
            /* Free space allocated for holding the func name */
	    free (temp_trap_CLC->func_name); 
	    /* Also free the memory used to hold parametes of the function 
               called in the CLC */
            free (temp_trap_CLC->args_array);
            /* Now free the trapped CLC structure */
            free (temp_trap_CLC); 
        }
      }
#endif
      /* Pop the empty frame that contains the stack dummy.
         POP_FRAME ends with a setting of the current frame, so we
         can use that next. */
      POP_FRAME;
      /* Set stop_pc to what it was before we called the function.
         Can't rely on restore_inferior_status because that only gets
         called if we don't stop in the called function.  */
      stop_pc = read_pc ();
      select_frame (get_current_frame (), 0);
      /* Decrement the CLC count by one */
      cmd_line_call_cnt--;
    }


  TUIDO (((TuiOpaqueFuncPtr) tui_vCheckDataValues, selected_frame));

  /* srikanth, refresh the screen if in tui mode. This is a workaround
     to a gross problem on HP-UX, where the terminal screen gets
     clobbered frequently.

     This workarounds is ok if running directly on an x-display. It is
     painfully slow if running gdb over a dial-up telnet client. Make
     this conditional upon the user issuing a "set tui-auto-refresh on" 
     command. JAGad39595.
  */
#if defined (TUI)

  if (tui_version && tui_auto_refresh)
    _tuiRefreshAll_command (0, 0);

#endif

done:
  annotate_stopped ();
}

static int
hook_stop_stub (void *cmd)
{
  execute_user_command ((struct cmd_list_element *) cmd, 0);
  return (0);
}

int
signal_stop_state (int signo)
{
  return signal_stop[signo];
}

int
signal_print_state (int signo)
{
  return signal_print[signo];
}

int
signal_pass_state (int signo)
{
  return signal_program[signo];
}

int signal_stop_update (int signo, int state)
{
  int ret = signal_stop[signo];
  signal_stop[signo] = state;
  return ret;
}

int signal_print_update (int signo, int state)
{
  int ret = signal_print[signo];
  signal_print[signo] = state;
  return ret;
}

int signal_pass_update (int signo, int state)
{
  int ret = signal_program[signo];
  signal_program[signo] = state;
  return ret;
}

static void
sig_print_header (void)
{
  printf_filtered ("\
Signal        Stop\tPrint\tPass to program\tDescription\n");
}

static void
sig_print_info (enum target_signal oursig)
{
  char *name = target_signal_to_name (oursig);
  int name_padding = (int) (13 - strlen (name));

  if (name_padding <= 0)
    name_padding = 0;

  printf_filtered ("%s", name);
  printf_filtered ("%*.*s ", name_padding, name_padding,
		   "                 ");
  printf_filtered ("%s\t", signal_stop[oursig] ? "Yes" : "No");
  printf_filtered ("%s\t", signal_print[oursig] ? "Yes" : "No");
  printf_filtered ("%s\t\t", signal_program[oursig] ? "Yes" : "No");
  printf_filtered ("%s\n", target_signal_to_string (oursig));
}

/* Specify how various signals in the inferior should be handled.  */

static void
handle_command (char *args, int from_tty)
{
  char **argv;
  int digits, wordlen;
  int sigfirst, signum, siglast;
  enum target_signal oursig;
  int allsigs;
  int nsigs;
  unsigned char *sigs;
  struct cleanup *old_chain;

  if (args == NULL)
    {
      error_no_arg ("signal to handle");
    }

  /* Allocate and zero an array of flags for which signals to handle. */

  nsigs = (int) TARGET_SIGNAL_LAST;
  sigs = (unsigned char *) alloca (nsigs);
  memset (sigs, 0, nsigs);

  /* Break the command line up into args. */


  argv = buildargv (args);
  if (argv == NULL)
    {
      nomem (0);
    }
  old_chain = make_cleanup_freeargv (argv);

  /* Walk through the args, looking for signal oursigs, signal names, and
     actions.  Signal numbers and signal names may be interspersed with
     actions, with the actions being performed for all signals cumulatively
     specified.  Signal ranges can be specified as <LOW>-<HIGH>. */

  while (*argv != NULL)
    {
      wordlen = (int) strlen (*argv);
      for (digits = 0; isdigit ((*argv)[digits]); digits++)
	{;
	}
      allsigs = 0;
      sigfirst = siglast = -1;

      if (wordlen >= 1 && !strncmp (*argv, "all", wordlen))
	{
	  /* Apply action to all signals except those used by the
	     debugger.  Silently skip those. */
	  allsigs = 1;
	  sigfirst = 0;
	  siglast = nsigs - 1;
	}
      else if (wordlen >= 1 && !strncmp (*argv, "stop", wordlen))
	{
	  SET_SIGS (nsigs, sigs, signal_stop);
	  SET_SIGS (nsigs, sigs, signal_print);
	}
      else if (wordlen >= 1 && !strncmp (*argv, "ignore", wordlen))
	{
	  UNSET_SIGS (nsigs, sigs, signal_program);
	}
      else if (wordlen >= 2 && !strncmp (*argv, "print", wordlen))
	{
	  SET_SIGS (nsigs, sigs, signal_print);
	}
      else if (wordlen >= 2 && !strncmp (*argv, "pass", wordlen))
	{
	  SET_SIGS (nsigs, sigs, signal_program);
	}
      else if (wordlen >= 3 && !strncmp (*argv, "nostop", wordlen))
	{
	  UNSET_SIGS (nsigs, sigs, signal_stop);
	}
      else if (wordlen >= 3 && !strncmp (*argv, "noignore", wordlen))
	{
	  SET_SIGS (nsigs, sigs, signal_program);
	}
      else if (wordlen >= 4 && !strncmp (*argv, "noprint", wordlen))
	{
	  UNSET_SIGS (nsigs, sigs, signal_print);
	  UNSET_SIGS (nsigs, sigs, signal_stop);
	}
      else if (wordlen >= 4 && !strncmp (*argv, "nopass", wordlen))
	{
	  UNSET_SIGS (nsigs, sigs, signal_program);
	}
      else if (digits > 0)
	{
	  /* It is numeric.  The numeric signal refers to our own
	     internal signal numbering from target.h, not to host/target
	     signal  number.  This is a feature; users really should be
	     using symbolic names anyway, and the common ones like
	     SIGHUP, SIGINT, SIGALRM, etc. will work right anyway.  */

	  sigfirst = siglast = (int)
	    target_signal_from_command (atoi (*argv));
	  if ((*argv)[digits] == '-')
	    {
	      siglast = (int)
		target_signal_from_command (atoi ((*argv) + digits + 1));
	    }
	  if (sigfirst > siglast)
	    {
	      /* Bet he didn't figure we'd think of this case... */
	      signum = sigfirst;
	      sigfirst = siglast;
	      siglast = signum;
	    }
	}
      else
	{
	  oursig = target_signal_from_name (*argv);
	  if (oursig != TARGET_SIGNAL_UNKNOWN)
	    {
	      sigfirst = siglast = (int) oursig;
	    }
	  else
	    {
	      /* Not a number and not a recognized flag word => complain.  */
	      error ("Unrecognized or ambiguous flag word: \"%s\".", *argv);
	    }
	}

      /* If any signal numbers or symbol names were found, set flags for
         which signals to apply actions to. */

      for (signum = sigfirst; signum >= 0 && signum <= siglast; signum++)
	{
	  switch ((enum target_signal) signum)
	    {
	    case TARGET_SIGNAL_TRAP:
	    case TARGET_SIGNAL_INT:
	      if (!allsigs && !sigs[signum])
		{
		  if (query ("%s is used by the debugger.\n\
Are you sure you want to change it? ",
			     target_signal_to_name
			     ((enum target_signal) signum)))
		    {
		      sigs[signum] = 1;
		    }
		  else
		    {
		      printf_unfiltered ("Not confirmed, unchanged.\n");
		      gdb_flush (gdb_stdout);
		    }
		}
	      break;
	    case TARGET_SIGNAL_0:
	    case TARGET_SIGNAL_DEFAULT:
	    case TARGET_SIGNAL_UNKNOWN:
	      /* Make sure that "all" doesn't print these.  */
	      break;
	    default:
	      sigs[signum] = 1;
	      break;
	    }
	}

      argv++;
    }

  target_notice_signals (inferior_pid);

  if (from_tty)
    {
      /* Show the results.  */
      sig_print_header ();
      for (signum = 0; signum < nsigs; signum++)
	{
	  if (sigs[signum])
	    {
	      sig_print_info (signum);
	    }
	}
    }

#ifdef GDB_TARGET_IS_HPUX
  /* RM: Use OS interface to ignore signals we don't care about */
  if (target_has_execution)
    require_notification_of_events (inferior_pid);
#endif

  do_cleanups (old_chain);
}

static void
xdb_handle_command (char *args, int from_tty)
{
  char **argv;
  struct cleanup *old_chain;

  /* Break the command line up into args. */
  
  /* For JAGaa80619 - In xdb compatibility mode, "z" with no arguments aborts gdb */

  if (args == NULL)
    {
      error_no_arg ("signal to handle");
    }

  /* JAGaa80619 - In xdb compatibility mode, "z" with no arguments aborts gdb - Ends Here */

  argv = buildargv (args);
  if (argv == NULL)
    {
      nomem (0);
    }
  old_chain = make_cleanup_freeargv (argv);
  if (argv[1] != (char *) NULL)
    {
      char *argBuf;
      int bufLen;

      bufLen = (int) strlen (argv[0]) + 20;
      argBuf = (char *) xmalloc (bufLen);
      if (argBuf)
	{
	  int validFlag = 1;
	  enum target_signal oursig;
          char *signal_name;

          oursig = target_signal_from_host (atoi(argv[0]));
	  memset (argBuf, 0, bufLen);
          signal_name = target_signal_to_name (oursig);
	  if (strcmp (argv[1], "Q") == 0)
	    sprintf (argBuf, "%s %s", signal_name, "noprint");
	  else
	    {
	      if (strcmp (argv[1], "s") == 0)
		{
		  if (!signal_stop[oursig])
		    sprintf (argBuf, "%s %s", signal_name, "stop");
		  else
		    sprintf (argBuf, "%s %s", signal_name, "nostop");
		}
	      else if (strcmp (argv[1], "i") == 0)
		{
		  if (!signal_program[oursig])
		    sprintf (argBuf, "%s %s", signal_name, "pass");
		  else
		    sprintf (argBuf, "%s %s", signal_name, "nopass");
		}
	      else if (strcmp (argv[1], "r") == 0)
		{
		  if (!signal_print[oursig])
		    sprintf (argBuf, "%s %s", signal_name, "print");
		  else
		    sprintf (argBuf, "%s %s", signal_name, "noprint");
		}
	      else
		validFlag = 0;
	    }
	  if (validFlag)
	    handle_command (argBuf, from_tty);
	  else
	    printf_filtered ("Invalid signal handling flag.\n");
	  if (argBuf)
	    free (argBuf);
	}
    }
  do_cleanups (old_chain);
}

/* Print current contents of the tables set by the handle command.
   It is possible we should just be printing signals actually used
   by the current target (but for things to work right when switching
   targets, all signals should be in the signal tables).  */

static void
signals_info (char *signum_exp, int from_tty)
{
  enum target_signal oursig;
  sig_print_header ();

  if (signum_exp)
    {
      /* First see if this is a symbol name.  */
      oursig = target_signal_from_name (signum_exp);
      if (oursig == TARGET_SIGNAL_UNKNOWN)
	{
	  /* No, try numeric.  */
	  oursig =
	    target_signal_from_command ((int) parse_and_eval_address (signum_exp));
	}
      sig_print_info (oursig);
      return;
    }

  printf_filtered ("\n");
  /* These ugly casts brought to you by the native VAX compiler.  */
  for (oursig = TARGET_SIGNAL_FIRST;
       (int) oursig < (int) TARGET_SIGNAL_LAST;
       oursig = (enum target_signal) ((int) oursig + 1))
    {
      QUIT;

      if (oursig != TARGET_SIGNAL_UNKNOWN
	  && oursig != TARGET_SIGNAL_DEFAULT
	  && oursig != TARGET_SIGNAL_0)
	sig_print_info (oursig);
    }

  printf_filtered ("\nUse the \"handle\" command to change these tables.\n");
}

static struct inferior_status *
xmalloc_inferior_status (void)
{
  struct inferior_status *inf_status;
  inf_status = xmalloc (sizeof (struct inferior_status));
  inf_status->stop_registers = xmalloc (REGISTER_BYTES);
  inf_status->registers = xmalloc (REGISTER_BYTES);
  return inf_status;
}

static void
free_inferior_status (struct inferior_status *inf_status)
{
  free (inf_status->registers);
  free (inf_status->stop_registers);
  free (inf_status);
}

void
write_inferior_status_register (struct inferior_status *inf_status, int regno,
				LONGEST val)
{
  int size = REGISTER_RAW_SIZE (regno);
  void *buf = (void *) alloca (size);
  store_signed_integer (buf, size, val);
  memcpy (&inf_status->registers[REGISTER_BYTE (regno)], buf, size);
}

/* Save all of the information associated with the inferior<==>gdb
   connection.  INF_STATUS is a pointer to a "struct inferior_status"
   (defined in inferior.h).  */

struct inferior_status *
save_inferior_status (int restore_stack_info)
{
  struct inferior_status *inf_status = xmalloc_inferior_status ();

  inf_status->stop_signal = stop_signal;
  inf_status->stop_pc = stop_pc;
  inf_status->stop_step = stop_step;
  inf_status->stop_stack_dummy = stop_stack_dummy;
  inf_status->stopped_by_random_signal = stopped_by_random_signal;
  inf_status->trap_expected = trap_expected;
  inf_status->step_range_start = step_range_start;
  inf_status->step_range_end = step_range_end;
  inf_status->step_frame_address = step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
  inf_status->step_frame_rse_addr= step_frame_rse_addr;
#endif
  inf_status->step_over_calls = step_over_calls;
  inf_status->stop_after_trap = stop_after_trap;
  inf_status->stop_soon_quietly = stop_soon_quietly;
  /* Save original bpstat chain here; replace it with copy of chain.
     If caller's caller is walking the chain, they'll be happier if we
     hand them back the original chain when restore_inferior_status is
     called.  */
  inf_status->stop_bpstat = stop_bpstat;
  stop_bpstat = bpstat_copy (stop_bpstat);
  inf_status->breakpoint_proceeded = breakpoint_proceeded;
  inf_status->restore_stack_info = restore_stack_info;
  inf_status->proceed_to_finish = proceed_to_finish;

  memcpy (inf_status->stop_registers, stop_registers, REGISTER_BYTES);

  read_register_bytes (0, inf_status->registers, REGISTER_BYTES);

  record_selected_frame (&(inf_status->selected_frame_address),
			 &(inf_status->selected_level));
  return inf_status;
}

struct restore_selected_frame_args
{
  CORE_ADDR frame_address;
  int level;
};

static int
restore_selected_frame (void *args)
{
  struct restore_selected_frame_args *fr =
  (struct restore_selected_frame_args *) args;
  struct frame_info *frame;
  int level = fr->level;

  frame = find_relative_frame (get_current_frame (), &level);

  /* If inf_status->selected_frame_address is NULL, there was no
     previously selected frame.  */
  if (frame == NULL ||
  /*  FRAME_FP (frame) != fr->frame_address || */
  /* elz: deleted this check as a quick fix to the problem that
     for function called by hand gdb creates no internal frame
     structure and the real stack and gdb's idea of stack are
     different if nested calls by hands are made.

     mvs: this worries me.  */
#ifdef HPPA_FIX_AND_CONTINUE
#ifdef GDB_TARGET_IS_HPPA_20W
     /* When a fix is reapplied, gdb reloads the fixed libraries at the entry
        point of the program, the level number returned from find_relative_frame
        is -1. Do not issue the warning for now.*/
      (!get_reapply_fix () && level != 0))
#else
      level != 0)
#endif
#else
      level != 0)
#endif
    {
      /* When GDB issues a command line call to the profile helper
         library, this bogus warning sporadically shows up.
         Shut it up for now. Needs further investigation. FIXME.
      */
      if (!profile_on)
        warning ("Unable to restore previously selected frame.\n");
      return 0;
    }

  select_frame (frame, fr->level);

  return (1);
}

void
restore_inferior_status (struct inferior_status *inf_status)
{
  stop_signal = inf_status->stop_signal;
  stop_pc = inf_status->stop_pc;
  stop_step = inf_status->stop_step;
  stop_stack_dummy = inf_status->stop_stack_dummy;
  stopped_by_random_signal = inf_status->stopped_by_random_signal;
  trap_expected = inf_status->trap_expected;
  step_range_start = inf_status->step_range_start;
  step_range_end = inf_status->step_range_end;
  step_frame_address = inf_status->step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
  step_frame_rse_addr= inf_status->step_frame_rse_addr;
#endif
  step_over_calls = inf_status->step_over_calls;
  stop_after_trap = inf_status->stop_after_trap;
  stop_soon_quietly = inf_status->stop_soon_quietly;
  bpstat_clear (&stop_bpstat);
  stop_bpstat = inf_status->stop_bpstat;
  breakpoint_proceeded = inf_status->breakpoint_proceeded;
  proceed_to_finish = inf_status->proceed_to_finish;

  /* FIXME: Is the restore of stop_registers always needed */
  memcpy (stop_registers, inf_status->stop_registers, REGISTER_BYTES);

  /* The inferior can be gone if the user types "print exit(0)"
     (and perhaps other times).  */
  if (target_has_execution)
      write_register_bytes (0, inf_status->registers, REGISTER_BYTES);

#ifdef HP_IA64_NATIVE
  invalidate_rse_info();
#endif

  /* FIXME: If we are being called after stopping in a function which
     is called from gdb, we should not be trying to restore the
     selected frame; it just prints a spurious error message (The
     message is useful, however, in detecting bugs in gdb (like if gdb
     clobbers the stack)).  In fact, should we be restoring the
     inferior status at all in that case?  .  */

  if (target_has_stack && inf_status->restore_stack_info)
    {
      struct restore_selected_frame_args fr;
      fr.level = inf_status->selected_level;
      fr.frame_address = inf_status->selected_frame_address;
      /* The point of catch_errors is that if the stack is clobbered,
         walking the stack might encounter a garbage pointer and error()
         trying to dereference it.  */
      if (catch_errors (restore_selected_frame, &fr,
			"Unable to restore previously selected frame:\n",
			RETURN_MASK_ERROR) == 0)
	/* Error in restoring the selected frame.  Select the innermost
	   frame.  */


	select_frame (get_current_frame (), 0);

    }

  free_inferior_status (inf_status);
}

static void
do_restore_inferior_status_cleanup (void *sts)
{
  restore_inferior_status (sts);
}

struct cleanup *
make_cleanup_restore_inferior_status (struct inferior_status *inf_status)
{
  return make_cleanup (do_restore_inferior_status_cleanup, inf_status);
}

void
discard_inferior_status (struct inferior_status *inf_status)
{
  /* See save_inferior_status for info on stop_bpstat. */
  bpstat_clear (&inf_status->stop_bpstat);
  free_inferior_status (inf_status);
}


static void
build_infrun (void)
{
  stop_registers = xmalloc (REGISTER_BYTES);
}


/* This routine locates the address in the inferior where we need to write
   '\0' to nullify LD_PRELOAD and writes '\0' into that address. 
   This is done to disable pre-loading of librtc in the child process
   when we follow parent on fork().
   Returns 0 on success and 1 on failure. */
static boolean
nullify_inferior_preload_env(int pid)
{
  struct minimal_symbol* m;
  CORE_ADDR addr;
  char buf[8];
  CORE_ADDR inf_env_addr, env_str_addr, ld_preload_addr;;
  char str_preload[] = "LD_PRELOAD=";
  char env_str[sizeof(str_preload)];
  static char env_ld_preload[MAXPATHLEN];
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;
  static char filler[] = "xxxxxx";
  static char librtc_str[] = "librtc";

  /* Find the 'environ' symbol which is a pointer to the array of
     environment variables */
  m = lookup_minimal_symbol ("environ", NULL, NULL);
  if (!m)
    {
      inferior_pid = saved_inferior_pid;
      return 1;
    }

  addr = SYMBOL_VALUE_ADDRESS (m);
  if (target_read_memory (addr, buf, PTR_SIZE))
    {
      inferior_pid = saved_inferior_pid;
      return 1;
    }
  inf_env_addr = extract_address(buf, PTR_SIZE);

  /* Read environment variables and locate LD_PRELOAD */
  if (target_read_memory (inf_env_addr, buf, PTR_SIZE))
    {
      inferior_pid = saved_inferior_pid;
      return 1;
    }
  env_str_addr = extract_address(buf, PTR_SIZE);

  /* Iterate over environment variables looking fo LD_PRELOAD */
  env_str[sizeof(str_preload) - 1] = 0;
  while (env_str_addr)
    {
      int status;

      /* Read individual environment strings until we find LD_PRELOAD */
      status = target_read_memory (env_str_addr, env_str, 
                                   sizeof(str_preload) - 1);

      /* If we read all the requested bytes for the current environment 
         variable string and it compares to "LD_PRELOAD=", nullify it 
         and return success */
      if (status == 0 && strcmp(env_str, str_preload) == 0)
        {
          /* Read the value setting of LD_PRELOAD one character at a time */
          int i = 0;
          env_ld_preload[0] = 0;
          env_str_addr += (sizeof(str_preload) - 1);
          ld_preload_addr = env_str_addr;

          if (target_read_memory (env_str_addr, &env_ld_preload[i], 1))
            {
              inferior_pid = saved_inferior_pid;
              return 1;
            }

          while (env_ld_preload[i])
            {
              i++;
              env_str_addr++;

              if (target_read_memory (env_str_addr, &env_ld_preload[i], 1))
                {
                  inferior_pid = saved_inferior_pid;
                  return 1;
                }
            }

          /* If the LD_PRELOAD string is not empty, scramble the librtc part */
          if (env_ld_preload[0])
            {
              char* pos = strstr (env_ld_preload, librtc_str);
              if (pos)
                {
                  memcpy (pos, filler, sizeof (filler) - 1);
                  pos = strstr (pos + sizeof (filler), librtc_str);
                  if (pos)
                    memcpy (pos, filler, sizeof (filler) - 1);

                  if (target_write_memory (ld_preload_addr, env_ld_preload, 
                                           strlen (env_ld_preload)))
                    {
                      inferior_pid = saved_inferior_pid;
                      return 1;
                    }
                }
            }

          inferior_pid = saved_inferior_pid;
          /* Success, LD_PRELOAD is nullified */
          return 0;
        }
      
      /* Move to the next entry in the env[] array */
      inf_env_addr += PTR_SIZE;
      if (target_read_memory (inf_env_addr, buf, PTR_SIZE))
        {
          inferior_pid = saved_inferior_pid;
          return 1;
        }
      env_str_addr = extract_address(buf, PTR_SIZE);
  }
  inferior_pid = saved_inferior_pid;

  /* No errors, but we cound't find LD_PRELOAD. Though normally it shouldn't
   happen, return success because heap/thread checking is already disabled
   in the child process, and that's exactly what we want. */
  return 0;
}


void
_initialize_infrun (void)
{
  register int i;
  register int numsigs;
  struct cmd_list_element *c;

  build_infrun ();

  register_gdbarch_swap (&stop_registers, sizeof (stop_registers), NULL);
  register_gdbarch_swap (NULL, 0, build_infrun);

  add_info ("signals", signals_info,
	    "What debugger does when program gets various signals.\n\n\
Usage:\n\tinfo handle [<SIGNAL>]\n\t or\n\tinfo signals [<SIGNAL>]\n\n\
Specify a signal as argument to print info on that signal only.");
  add_info_alias ("handle", "signals", 0);

  add_com ("handle", class_run, handle_command,
	   concat ("Specify how to handle a signal.\n\n\
Usage:\n\thandle <SIGNAL> [<ACTION1>] [<ACTION2>] [...]\n\n\
Args are signals and actions to apply to those signals.\n\
Symbolic signals (e.g. SIGSEGV) are recommended but numeric signals\n\
from 1-15 are allowed for compatibility with old versions of GDB.\n\
Numeric ranges may be specified with the form LOW-HIGH (e.g. 1-5).\n\
The special arg \"all\" is recognized to mean all signals except those\n\
used by the debugger, typically SIGTRAP and SIGINT.\n",
		   "Recognized actions include \"stop\", \"nostop\", \"print\", \"noprint\",\n\
\"pass\", \"nopass\", \"ignore\", or \"noignore\".\n\
Stop means reenter debugger if this signal happens (implies print).\n\
Print means print a message if this signal happens.\n\
Pass means let program see this signal; otherwise program doesn't know.\n\
Ignore is a synonym for nopass and noignore is a synonym for pass.\n\
Pass and Stop may be combined.", NULL));
  if (xdb_commands)
    {
      add_com ("lz", class_info, signals_info,
	       "What debugger does when program gets various signals.\n\
Specify a signal as argument to print info on that signal only.");
      add_com ("z", class_run, xdb_handle_command,
	       concat ("Specify how to handle a signal.\n\
Args are signals and actions to apply to those signals.\n\
Symbolic signals (e.g. SIGSEGV) are recommended but numeric signals\n\
from 1-15 are allowed for compatibility with old versions of GDB.\n\
Numeric ranges may be specified with the form LOW-HIGH (e.g. 1-5).\n\
The special arg \"all\" is recognized to mean all signals except those\n\
used by the debugger, typically SIGTRAP and SIGINT.\n",
		       "Recognized actions include \"s\" (toggles between stop and nostop), \n\
\"r\" (toggles between print and noprint), \"i\" (toggles between pass and \
nopass), \"Q\" (noprint)\n\
Stop means reenter debugger if this signal happens (implies print).\n\
Print means print a message if this signal happens.\n\
Pass means let program see this signal; otherwise program doesn't know.\n\
Ignore is a synonym for nopass and noignore is a synonym for pass.\n\
Pass and Stop may be combined.", NULL));
    }

  numsigs = (int) TARGET_SIGNAL_LAST;
  signal_stop = (unsigned char *)
    xmalloc (sizeof (signal_stop[0]) * numsigs);
  signal_print = (unsigned char *)
    xmalloc (sizeof (signal_print[0]) * numsigs);
  signal_program = (unsigned char *)
    xmalloc (sizeof (signal_program[0]) * numsigs);
  for (i = 0; i < numsigs; i++)
    {
      signal_stop[i] = 1;
      signal_print[i] = 1;
      signal_program[i] = 1;
    }

  /* Signals caused by debugger's own actions
     should not be given to the program afterwards.  */
  signal_program[TARGET_SIGNAL_TRAP] = 0;
  signal_program[TARGET_SIGNAL_INT] = 0;

  /* Signals that are not errors should not normally enter the debugger.  */
  signal_stop[TARGET_SIGNAL_ALRM] = 0;
  signal_print[TARGET_SIGNAL_ALRM] = 0;
  signal_stop[TARGET_SIGNAL_VTALRM] = 0;
  signal_print[TARGET_SIGNAL_VTALRM] = 0;
  signal_stop[TARGET_SIGNAL_PROF] = 0;
  signal_print[TARGET_SIGNAL_PROF] = 0;
  signal_stop[TARGET_SIGNAL_CHLD] = 0;
  signal_print[TARGET_SIGNAL_CHLD] = 0;
  signal_stop[TARGET_SIGNAL_IO] = 0;
  signal_print[TARGET_SIGNAL_IO] = 0;
  signal_stop[TARGET_SIGNAL_POLL] = 0;
  signal_print[TARGET_SIGNAL_POLL] = 0;
  signal_stop[TARGET_SIGNAL_URG] = 0;
  signal_print[TARGET_SIGNAL_URG] = 0;
  signal_stop[TARGET_SIGNAL_WINCH] = 0;
  signal_print[TARGET_SIGNAL_WINCH] = 0;

  /* These signals are used internally by user-level thread
     implementations.  (See signal(5) on Solaris.)  Like the above
     signals, a healthy program receives and handles them as part of
     its normal operation.  */
  signal_stop[TARGET_SIGNAL_LWP] = 0;
  signal_print[TARGET_SIGNAL_LWP] = 0;
  signal_stop[TARGET_SIGNAL_WAITING] = 0;
  signal_print[TARGET_SIGNAL_WAITING] = 0;
  signal_stop[TARGET_SIGNAL_CANCEL] = 0;
  signal_print[TARGET_SIGNAL_CANCEL] = 0;

  /* HP-UX MxN pthreads library internal signal is handled normally */
  signal_stop[TARGET_SIGNAL_LWPTIMER] = 0;
  signal_print[TARGET_SIGNAL_LWPTIMER] = 0;

  /* Real-time signals should be handled by the inferior and not
     enter the debugger */
  for (i = TARGET_SIGNAL_REALTIME_33; i <= TARGET_SIGNAL_REALTIME_63; i++)
    {
      signal_stop[i] = 0;
      signal_print[i] = 0;
    }
  signal_stop[TARGET_SIGNAL_REALTIME_32] = 0;
  signal_print[TARGET_SIGNAL_REALTIME_32] = 0;

#ifdef SOLIB_ADD
  add_show_from_set
    (add_set_cmd ("stop-on-solib-events", class_support, var_zinteger,
		  (char *) &stop_on_solib_events,
		  "Set stopping for shared library events.\n\n\
Usage:\nTo set new value:\n\tset stop-on-solib-events <INTEGER>\n\
To see current value:\n\tshow stop-on-solib-events\n\n\
If nonzero, gdb will give control to the user when the dynamic linker\n\
notifies gdb of shared library events.  The most common event of interest\n\
to the user would be loading/unloading of a new library.\n",
		  &setlist),
     &showlist);
#endif

#ifdef TUI

  /* srikanth, JAGad39595, provide a command to turn on automatic
     TUI mode refresh. This is a temporary workaround to a gross
     problem where the tui windows completely gets messed up every
     now and then and a user is forced to repeatedly refresh the
     screen manually.

     The reason this command exist is to allow a user to control
     this behavior. We don't want to do this by default as the
     performance is really SUX over a telnet dialup window. This
     is a band aid where what ought to be done is a surgery. FIXME.
  */

  add_show_from_set (
          add_set_cmd ("tui-auto-refresh", class_support, var_boolean,
                      (char *) &tui_auto_refresh,
                      "Set automatic screen refresh.\n\nUsage:\nTo set new value:"
                      "\n\tset tui-auto-refresh [on | off]\nTo see current value:"
                      "\n\tshow tui-auto-refresh\n",
                      &setlist),
          &showlist);
#endif

  c = add_set_enum_cmd ("follow-fork-mode",
			class_run,
			follow_fork_mode_kind_names,
			&follow_fork_mode_string,
/* ??rehrauer:  The "both" option is broken, by what may be a 10.20
   kernel problem.  It's also not terribly useful without a GUI to
   help the user drive two debuggers.  So for now, I'm disabling
   the "both" option.  */
/*                      "Set debugger response to a program call of fork \
   or vfork.\n\
   A fork or vfork creates a new process.  follow-fork-mode can be:\n\
   parent  - the original process is debugged after a fork\n\
   child   - the new process is debugged after a fork\n\
   both    - both the parent and child are debugged after a fork\n\
   ask     - the debugger will ask for one of the above choices\n\
   For \"both\", another copy of the debugger will be started to follow\n\
   the new child process.  The original debugger will continue to follow\n\
   the original parent process.  To distinguish their prompts, the\n\
   debugger copy's prompt will be changed.\n\
   For \"parent\" or \"child\", the unfollowed process will run free.\n\
   By default, the debugger will follow the parent process.",
 */
			"Set debugger response to a program call of fork \
or vfork.\n\n\
Usage:\nTo set new value:\n\tset follow-fork-mode ask | child | parent | serial\n\
To see current value:\n\tshow follow-fork-mode\n\n\
A fork or vfork creates a new process.  follow-fork-mode can be:\n\
  parent  - the original process is debugged after a fork\n\
  child   - the new process is debugged after a fork\n\
  ask     - the debugger will ask for one of the above choices\n\
  serial  - the child is debugged after a fork while the parent is \n\
	    waiting and when child exits, continue to debug the parent\n\
For \"parent\" or \"child\", the unfollowed process will run free.\n\
By default, the debugger will follow the parent process.",
			&setlist);
/*  c->function.sfunc = ; */
  add_show_from_set (c, &showlist);

  c = add_set_enum_cmd ("scheduler-locking", class_run,
			scheduler_enums,	/* array of string names */
			&scheduler_mode,	/* current mode  */
			"Set mode for locking scheduler during execution.\n\n\
Usage:\nTo set new value:\n\tset scheduler-locking off | on | step\n\
To see current value:\n\tshow scheduler-locking\n\n\
off  == no locking (threads may preempt at any time)\n\
on   == full locking (no thread except the current thread may run)\n\
step == scheduler locked during every single-step operation.\n\
	In this mode, no other thread may run during a step command.\n\
	Other threads may run while stepping over a function call ('next').",
			&setlist);

  c->function.sfunc = set_schedlock_func;	/* traps on target vector */
  add_show_from_set (c, &showlist);
}
