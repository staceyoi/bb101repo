/* Low level Unix child interface to ttrace, for GDB when running under HP-UX.
   Copyright 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* JYG: _PSTAT64 must be defined on HP-UX 11.x to use the 64bit interface,
       even when the underlying kernel would be 32bit (somehow this just
       works, not the other way around where a 32bit pstat_* interface
       is used). */
/* For PA, _PSTAT64 is defined in source_env.ksh */
#ifndef _PSTAT64
#define _PSTAT64
#endif


/*     
     Two new event of ttrace, TTEVT_PREFORK and TTEVT_FAIL_VFORK, available in
   TTRACE level of 10 or greater versions ( >= 11.22 PA systems) 
   will be used by gdb to prevent the deadlock that's happening 
   due to multiple vforks in a multithreaded
   process.

   TEVT_PREFORK       This event flag indicates that the traced thread 
                      needs to take special action just after the child
                      process id and thread id is known and before the
                      child process is created and set to run
                      Note the upon continutation from this event the traced
                      thread does not guarantee that the child process, with
                      previously returned process id and thread id, would be
                      created. Upon fork failure TTEVT_FORK_FAIL is returned.

   TTEVT_FAIL_FORK    This event flag indicates that traced thread needs to
                      take special action upon failure for fork/vfork.

    Logic:

              1)  Whenever we get a TTEVT_PREFORK event, stop all threads of the
                  process and continue only the vforking thread. We get the
                  vforking child pid & tid from the thread state structure
                  and will wait (in call_real_ttrace_wait) for the CHILD_VFORK 
                  to happen.  In case of TTEVT_FAIL_VFORK, we will 
                  wait for any events from any pids and  tids. Note that we
                  handle PREFORK events only for vforks.
              2)  Once we get the CHILD_VFORK, get the parent pid & tid from
                  the thread state structure and wait for PARENT_VFORK to 
		  happen.
              3)  Once we get the parent vfork continue only the vforked thread and
		  will wait for any events from any pid and tid.
              4)  REPEAT steps 1 ,2 and 3 till there is no prefork event in the 
                  ttrace queue.
              5)  When there is no prefork events pending in the queue, continue
		  all the threads of that process.

     BUILD:
        
             These two events which are defined in ttrace.h of version >= 10, is
          not yet availble in Current BUILD_ENV. Most likely this will be updated
          in BUILD_ENV in 11.31. Define the flag PREVENT_MULTITHREADED_DEADLOCK 
          when the ttrace changes are visible in BUILD_ENV .
             If you are planning to deliver this bits to the customer before 11.31
          define  the flag PREVENT_MULTITHREADED_DEADLOCK and make a local copy of 
          ttrace.h and scall_defs.h (put it in build directory) from a 11.22 or 
          greater PA machine and build it.
  */

/*#define PREVENT_MULTITHREADED_DEADLOCK */

#ifdef PREVENT_MULTITHREADED_DEADLOCK 
#include "ttrace.h"
#else
#include <sys/ttrace.h>
#endif

#include <stdlib.h>

#include "defs.h"
#include "frame.h"
#include "inferior.h"
#include "target.h"
#include "gdb_string.h"
#include "gdb_wait.h"
#include "gdbthread.h"
#include "javalib.h"
#include "command.h"
#include <time.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/dir.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/signal.h>
#include <sys/pstat.h>
#include <sys/mman.h>
#include <sys/ptrace.h>
#include "gdbcore.h"
#include "exec-path.h"
#include <sys/file.h>
#include <pthread.h>
#include <assert.h>
#include <strings.h>	/* for bzero() prototype */
#include <sys/utsname.h>
#include <sys/scall_define.h>

#ifndef LWP_USER_KERNEL_DAEMON
#define LWP_USER_KERNEL_DAEMON 0x1000000  /* Kernel daemon thread*/
#endif

/* TTEVT_UT_EXIT is a User thread exit event. */
#ifndef TTEVT_UT_EXIT
#define TTEVT_UT_EXIT 0x00010000
#endif
int exited_thread = 0;
int just_exited_thread = 0;
int enabled_ut_events = 0;
int last_event_thread; /* thread id of the last thread causing a ttrace event */

#ifdef GDB_TARGET_IS_HPPA
int sigwait_sig_in_init = 0;
#else
int sigwait_sig_in_init = 1;
#endif

/* This flag will be set if the OS version is >= 11.20 . */
int is_11_20_or_greater = 0;

/* This flag will be set if the OS version is >= 11.23. 
   The page-protection hw watchpoints can be activated on IPF only from
   11.23. Prior to that, due to an OS bug(JAGae60600), the system will
   crash. */
int is_11_23_or_greater = 0;

/* To check if the syscall events are enabled from pageprotection watchpoints
   or data breakpoints. */
int syscall_enabled_from_page_prot = 0;
int syscall_enabled_from_data_break = 0;
boolean keep_as_protected = 0;

/* From breakpoint.c */
extern int next_hw_breakpoint (boolean);

/* From main.c */
char* get_monitor_path (char** argv);

/* Forward declaration */
int target_insert_internal_breakpoint (CORE_ADDR addr, void* shadow);
int target_remove_internal_breakpoint (CORE_ADDR addr, void* shadow);

/* QXCR1001053296: Signal handling by wdb is improper when application runs
   in different shell. */
boolean fake_sig0_flag = 0;

/* If unable to retrieve status of inferior process, detect it.
   return false if the process is killed, exited or zombie. */
boolean
inferior_is_live ()
{
  struct pst_status old_proc;
  extern int errno;
  int pid = get_pid_for (inferior_pid);
  errno = 0;
  memset (&old_proc, 0 , sizeof (struct pst_status));
  if(!is_prog_using_cma_threads && pid > 0 &&
     (((pstat_getproc(&old_proc,sizeof(struct pst_status),0, pid) == -1) && errno == ESRCH )
     || old_proc.pst_stat == 4)) /* detect if process is in zombie state */
    return false;
  else
    return true;
}

#define NUM_BYTES_IN_SCBM 82
ttbm_t *safe_syscall_bm;

/* Sets the bit corresponing to the SYSCALL in SYCALL_BM 
   - start from syscall #1
	buf[0] = 11111111
		 ^      ^
		#8     #1
	buf[1] = 11111111
	         ^      ^
		#16    #9
*/
static void
set_syscall_in_bm (ttbm_t * syscall_bm, int syscall)
{
  int byte_num;
  int bit_num;
  byte_num = (syscall - 1)/8 ;
  bit_num = (syscall -1) & 7;
  syscall_bm [byte_num] = (1 << bit_num) | syscall_bm [byte_num];
}

/* Unsets the bit corresponing to the SYSCALL in SYCALL_BM */
static void
unset_syscall_in_bm (ttbm_t * syscall_bm, int syscall)
{
  int byte_num;
  int bit_num;
  byte_num = (syscall - 1)/8 ;
  bit_num = (syscall -1) & 7;
  syscall_bm [byte_num] = ~(1 << bit_num) & syscall_bm [byte_num];
}

/* Bindu 100105: Set the bits corresponding to the syscalls that we do
   not require notifications from. 

   If FOR_WATCH is TRUE: these are all the syscalls that are
   known to not write to the memory. Do not set syscalls siginhibit (491)
   and sigenable (492) as we need notifications for these syscalls for watch.
   See infrun.c. We always need notifications from SYS_SIGWAIT, SYS_SIGTIMEDWAIT
   and SYS_SIGWAITINFO.

   If FOR_WATCH is FALSE: these are all syscalls except the ones we always
   need notification from wether for watch or not. They are SYS_SIGWAIT,
   SYS_SIGTIMEDWAIT and SYS_SIGWAITINFO. */
static void
set_safe_syscall_bm (boolean for_watch)
{
  int syscall;
  const unsigned short safe_syscall_bm_list[] = {
	  1, 	/* SYS_EXIT 1 */
	  2, 	/* SYS_FORK 2 */
	  4, 	/* SYS_WRITE 4 */
	  5, 	/* SYS_OPEN 5 */
	  6, 	/* SYS_CLOSE 6 */
	  8,	/* SYS_CREAT 8 */
	  9,	/* SYS_LINK 9 */
	  10,	/* SYS_UNLINK 10 */
	  12,	/* SYS_CHDIR 12 */
	  14,	/* SYS_MKNOD 14 */
	  15,	/* SYS_CHMOD 15 */
	  16,	/* SYS_CHOWN 16 */
	  17,	/* SYS_BRK 17 */
	  18,	/* SYS_LCHMOD 18 */
	  19,	/* SYS_LSEEK 19 */
	  20,	/* SYS_GETPID 20 */
	  21,	/* SYS_MOUNT 21 */
	  22,	/* SYS_UMOUNT 22 */
	  23,	/* SYS_SETUID 23 */
	  24, 	/* SYS_GETUID 24 */
	  25,	/* SYS_STIME 25 */
	  27,	/* SYS_ALARM 27 */
	  29,	/* SYS_PAUSE 29 */
	  30,	/* SYS_UTIME 30 */
	  31,	/* SYS_STTY 31 */
	  33,	/* SYS_ACCESS 33 */
	  34,	/* SYS_NICE 34 */
	  36,	/* SYS_SYNC 36 */
	  37,	/* SYS_KILL 37 */
	  39,	/* SYS_SETPGRP3 39 */
	  41,	/* SYS_DUP 41 */
	  42,	/* SYS_PIPE 42 */
	  46,	/* SYS_SETGID 46 */
	  47,	/* SYS_GETGID 47 */
	  51,	/* SYS_ACCT 51 */
	  52,	/* SYS_SET_USERTHREADID 52 */
	  55,	/* SYS_REBOOT 55 */
	  56,	/* SYS_SYMLINK 56 */
	  60,	/* SYS_UMASK 60 */
	  61,	/* SYS_CHROOT 61 */
	  63,	/* SYS_ULIMIT 63 */
	  64,	/* SYS_LW_GET_SAINFO 64 */
	  65,	/* SYS_LW_SET_SAINFO 65 */
	  66,	/* SYS_VFORK 66 */
	  67,	/* SYS_LWP_GETPRIVATE 67 */
	  68,	/* SYS_LWP_SETPRIVATE 68 */
	  69,	/* SYS_LW_MXN_SETSIGMASK 69 */
	  70,	/* SYS_LW_SA_PENDING_SIGNALS 70 */
	  71,	/* SYS_MMAP 71 */
	  73,	/* SYS_MUNMAP 73 */
	  74,	/* SYS_MPROTECT 74 */
	  75,	/* SYS_MADVISE 75 */
	  76,	/* SYS_VHANGUP 76 */
	  77,	/* SYS_SWAPOFF 77 */
	  80,	/* SYS_SETGROUPS 80 */
	  81,	/* SYS_GETPGRP2 81 */
	  82,	/* SYS_SETPGID 82 */
	  85,	/* SYS_SWAPON 85 */
	  90,	/* SYS_DUP2 90 */
	  95,	/* SYS_FSYNC 95 */
	  96,	/* SYS_SETPRIORITY 96 */
	  100,	/* SYS_GETPRIORITY 100 */
	  109,	/* SYS_SIGBLOCK 109 */
	  110,	/* SYS_SIGSETMASK 110 */
	  111,	/* SYS_SIGPAUSE 111 */
	  121,	/* SYS_WRITEV 121 */
	  122,	/* SYS_SETTIMEOFDAY 122 */
	  123,	/* SYS_FCHOWN 123 */
	  124,	/* SYS_FCHMOD 124 */
	  126,	/* SYS_SETRESUID 126 */
	  127,	/* SYS_SETRESGID 127 */
	  128,	/* SYS_RENAME 128 */
	  129,	/* SYS_TRUNCATE 129 */
	  130,	/* SYS_FTRUNCATE 130 */
	  132,	/* SYS_SYSCONF 132 */
	  136,	/* SYS_MKDIR 136 */
	  137,	/* SYS_RMDIR 137 */
	  139,	/* SYS_SIGCLEANUP 139 */
	  140,	/* SYS_SETCORE 140 */
	  145,	/* SYS_SETRLIMIT 145 */
	  147,	/* SYS_LWP_SELF 147 */
	  152,	/* SYS_RTPRIO 152 */
	  153,	/* SYS_PLOCK 153 */
	  156,	/* SYS_SEMGET 156 */
	  158,	/* SYS_SEMOP 158 */
	  159,	/* SYS_MSGGET 159 */
	  160,	/* omsgctl */
	  161,	/* SYS_MSGSND 161 */
	  163,	/* SYS_SHMGET 163 */
	  165,	/* SYS_SHMAT 165 */
	  166,	/* SYS_SHMDT 166 */
	  178,	/* SYS_LSYNC 178 */
	  187,	/* SYS_SIGSUSPEND 187 */
	  193,	/* SYS_SETDOMAINNAME 193 */
	  198,	/* SYS_VFSMOUNT 198 */
	  224,	/* SYS_SIGSETRETURN 224 */
	  229,	/* SYS_SET_NO_TRUNC 229 */
	  230,	/* SYS_PATHCONF 230 */
	  231,	/* SYS_FPATHCONF 231 */
	  238,	/* SYS_OFSETACL 238 */
	  240,	/* SYS_GETAUDID 240 */
	  241,	/* SYS_SETAUDID 241 */
	  242,	/* SYS_GETAUDPROC 242 */
	  243,	/* SYS_SETAUDPROC 243 */
	  245,	/* SYS_SETEVENT 245 */
	  246,	/* SYS_AUDWRITE 246 */
	  247,	/* SYS_AUDSWITCH 247 */
	  249,	/* SYS_OGETACCESS 249 */
	  267,	/* SYS_TSYNC 267 */
	  268,	/* SYS_GETNUMFDS 268 */
	  271,	/* SYS_PUTMSG 271 */
	  272,	/* SYS_FCHDIR 272 */
	  276,	/* SYS_BIND 276 */
	  277,	/* SYS_CONNECT 277 */
	  281,	/* SYS_LISTEN 281 */
	  285,	/* SYS_SEND 285 */
	  286,	/* SYS_SENDMSG 286 */
	  287,	/* SYS_SENDTO 287 */
	  288,	/* SYS_SETSOCKOPT 288 */
	  289,	/* SYS_SHUTDOWN 289 */
	  290,	/* SYS_SOCKET 290 */
	  292,	/* SYS_PROC_OPEN 292 */
	  293,	/* SYS_PROC_CLOSE 293 */
	  294,	/* SYS_PROC_SEND 294 */
	  295,	/* SYS_PROC_RECV 295 */
	  296,	/* SYS_PROC_SENDRECV 296 */
	  297,	/* SYS_PROC_SYSCALL 297 */
	  315,	/* SYS_MPCTL 315 */
	  318,	/* SYS_PUTPMSG 318 */
	  320,	/* SYS_MSYNC 320 */
	  327,	/* SYS_FATTACH 327 */
	  328,	/* SYS_FDETACH 328 */
	  329,	/* SYS_SERIALIZE 329 */
	  332,	/* SYS_LCHOWN 332 */
	  333,	/* SYS_GETSID 333 */
	  337,	/* SYS_SCHED_SETPARAM 337 */
	  339,	/* SYS_SCHED_SETSCHEDULER 339 */
	  340,	/* SYS_SCHED_GETSCHEDULER 340 */
	  341,	/* SYS_SCHED_YIELD 341 */
	  342,	/* SYS_SCHED_GET_PRIORITY_MAX 342 */
	  343,	/* SYS_SCHED_GET_PRIORITY_MIN 343 */
	  345,	/* SYS_CLOCK_SETTIME 345 */
	  349,	/* SYS_TIMER_DELETE 349 */
	  352,	/* SYS_TIMER_GETOVERRUN 352 */
	  360,	/* SYS_FTRUNCATE64 360 */
	  364,	/* SYS_LOCKF64 364 */
	  365,	/* SYS_LSEEK64 365 */
	  367,	/* SYS_MMAP64 367 */
	  368,	/* SYS_SETRLIMIT64 368 */
	  370,	/* SYS_TRUNCATE64 370 */
	  374,	/* SYS_PWRITE 374 */
	  378,	/* SYS_PWRITE64 378 */
	  380,	/* SYS_SETCONTEXT 380 */
	  383,	/* SYS_SETPGRP 383 */
	  385,	/* SYS_SENDMSG2 385 */
	  386,	/* SYS_SOCKET2 386 */
	  388,	/* SYS_SETREGID 388 */
	  390,	/* SYS_LWP_TERMINATE 390 */
	  392,	/* SYS_LWP_SUSPEND 392 */
	  393,	/* SYS_LWP_RESUME 393 */
	  394,	/* lwp_self */
	  395,	/* SYS_LWP_ABORT_SYSCALL 395 */
	  397,	/* SYS_LWP_KILL 397 */
	  402,	/* SYS_LWP_EXIT 402 */
	  403,	/* SYS_LWP_CONTINUE 403 */
	  406,	/* SYS_SETACL 406 */
	  407,	/* SYS_FSETACL 407 */
	  408,	/* SYS_GETACCESS 408 */
	  417,	/* SYS_LWP_SETSCHEDULER 417 */
	  419,	/* SYS_LWP_SETSTATE 419 */
	  420,	/* SYS_LWP_DETACH 420 */
	  421,	/* SYS_MLOCK 421 */
	  422,	/* SYS_MUNLOCK 422 */
	  423,	/* SYS_MLOCKALL 423 */
	  424,	/* SYS_MUNLOCKALL 424 */
	  425,	/* SYS_SHM_OPEN 425 */
	  426,	/* SYS_SHM_UNLINK 426 */
	  427,	/* SYS_SIGQUEUE 427 */
	  432,	/* SYS_AIO_WRITE 432 */
	  434,	/* SYS_AIO_ERROR 434 */
	  435,	/* SYS_AIO_RETURN 435 */
	  436,	/* SYS_AIO_CANCEL 436 */
	  437,	/* SYS_AIO_SUSPEND 437 */
	  438,	/* SYS_AIO_FSYNC 438 */
	  439,	/* SYS_MQ_OPEN 439 */
	  440,	/* SYS_MQ_CLOSE 440 */
	  441,	/* SYS_MQ_UNLINK 441 */
	  442,	/* SYS_MQ_SEND 442 */
	  444,	/* SYS_MQ_NOTIFY 444 */
	  448,	/* SYS_KSEM_UNLINK 448 */
	  449,	/* SYS_KSEM_CLOSE 449 */
	  450,	/* SYS_KSEM_POST 450 */
	  451,	/* SYS_KSEM_WAIT 451 */
	  453,	/* SYS_KSEM_TRYWAIT 453 */
	  479,	/* SYS_SENDFILE 479 */
	  480,	/* SYS_SENDPATH 480 */
	  481,	/* SYS_SENDFILE64 481 */
	  482,	/* SYS_SENDPATH64 482 */
	  483,	/* SYS_MODLOAD 483 */
	  484,	/* SYS_MODULOAD 484 */
	  485,	/* SYS_MODPATH 485 */
	  493,	/* SYS_SPUCTL 493 */
	  494,	/* SYS_ZEROKERNELSUM 494 */
	  497,	/* SYS_AIO_WRITE64 497 */
	  498,	/* SYS_AIO_ERROR64 498 */
	  499,	/* SYS_AIO_RETURN64 499 */
	  500,	/* SYS_AIO_CANCEL64 500 */
	  501,	/* SYS_AIO_SUSPEND64 501 */
	  502,	/* SYS_AIO_FSYNC64 502 */
	  506,	/* SYS_SEND2 506 */
	  507,	/* SYS_SENDTO2 507 */
	  517,	/* SYS_SETTUNE 517 */
	  519,	/* SYS_PSET_DESTROY 519 */
	  523,	/* SYS_PSET_SETATTR 523 */
	  524,	/* SYS_PSET_CTL 524 */
	  525,	/* SYS_PSET_RTCTL 525 */
	  528,	/* SYS_LWP_NULL_SYSCALL 528 */
	  530,	/* SYS_LWP_KILL_SIGINFO 530 */
	  531,	/* SYS_TTRACE_NOTIFY 531 */
	  532,	/* SYS_LWP_SA_PROCINIT */
	  534,	/* SYS_LWP_SUSPEND_ALL */
	  535,	/* SYS_LWP_RESUME_ALL */
	  536,	/* SYS_LWP_DEL_PENDSIGS */
	  538,	/* SYS_TTRACE_PROC_TRACED */
	  550,	/* SYS_SEMTIMEDOP */
	  557,	/* pw_post */
	  559,	/* pw_getvmax */
	  561,	/* umount2 */	
	  595,	/* setaudtag */
	  603,	/* dlm_convert */
	  604,	/* dlm_unlk */
	  609,	/* dlm_set_sequence_number */
	  611,	/* dlm_invalidate_sequence_number */
	  612,	/* dlm_close_sequence_number */
	  613,	/* dlm_destroy_sequence_number */
	  614,	/* dlm_lktp */
	  616,	/* dlm_glc_attach_sys */
	  617,	/* dlm_glc_detach_sys */
	  618,	/* dlm_glc_destroy_sys */
	  620,	/* dlm_rd_detach */
	  622,	/* dlm_rd_validate */
	  629,	/* cnx_mgmt */
	  638	/* clu_config_status */ };

    if (!safe_syscall_bm)
      {
        safe_syscall_bm = (ttbm_t*) xmalloc (sizeof(ttbm_t) * NUM_BYTES_IN_SCBM);
      }

  if (for_watch)
    {
      memset (safe_syscall_bm, 0, sizeof (ttbm_t) * NUM_BYTES_IN_SCBM );
      for ( syscall = 0; 
	    syscall < (sizeof(safe_syscall_bm_list) / 
                      sizeof(safe_syscall_bm_list[0]));
	    syscall++)
        {
	  set_syscall_in_bm (safe_syscall_bm, safe_syscall_bm_list[syscall]);
        }
    }
  else
    {
      /* Set all but SYS_SIGWAITINFO, SYS_SIGTIMEDWAIT and SYS_SIGWAIT */
      memset (safe_syscall_bm, 0xffffffff, sizeof (ttbm_t) * NUM_BYTES_IN_SCBM );
      unset_syscall_in_bm (safe_syscall_bm, 428); /* SYS_SIGWAITINFO */
      unset_syscall_in_bm (safe_syscall_bm, 429); /* SYS_SIGTIMEDWAIT */
      unset_syscall_in_bm (safe_syscall_bm, 430); /*  SYS_SIGWAIT */
    }
}

static void ia64_reset_data_reg (void);
static void ia64_reset_inst_reg (void);

extern int java_debugging;
#include <sys/unistd.h>
extern void (*symbol_table_load_hook) (char*);
#ifndef SIGLWPTIMER
#define SIGLWPTIMER 31
#endif
#if TT_FEATURE_LEVEL <= 6
#define TTEVT_BPT_SSTEP   0x00001000
#endif
#if TT_FEATURE_LEVEL <= 8
#define TT_LWP_SET_SIGMASK (TT_LWP_STOP + 16)

/*
 * Mask Options
 */
typedef enum {
        TTMO_NONE       = 0x0,  /* No options */
        TTMO_PROC_INHERIT       = 0x1,  /* Child inherits parent masks (proc) */
        TTMO_LWP_INHERIT        = 0x2   /* Child inherits parent masks (lwp) */
} ttmopt_t;
typedef struct {
  sigset_t    ttm_signals;
  ttmopt_t    ttm_opts;
} ttmask_t;
#endif 

extern bfd *current_core_bfd; /* Defined in corelow.c. This is the bfd
                        that we are dealing with in the corefile case. */

extern void (*symbol_table_load_hook) (char*);

void do_mxn_init (int);
void clear_mxn_info (void);

static void dumpcore_command (char *file_name, int from_tty);

void reset_prefork_wait (void);
static void stop_all_threads_of_process (pid_t real_pid);

#if TT_FEATURE_LEVEL < 10
#define TTEVT_PREFORK   0x00002000
#define TTEVT_FORK_FAIL 0x00004000
#endif

typedef struct
  {
    pid_t     pid;        /* Process identification number */
    lwpid_t   ktid;       /* LWP id. 0 if this thread is not running. */
    pthread_t utid;       /* User space thread id.         */
    tid_t     gdb_tid;    /* Thread id from gdb's pov.     */
  } 
composite_tid_t;

int feature_level;
static composite_tid_t null_tid;
static tid_t wait_on_prefork_tid = 0;
static tid_t wait_on_prefork_pid =  0;
static int is_vfork =  0;
#define WDB_TIMER_FOR_PREFORKS 200000000
int is_mxn = 0;
static CORE_ADDR  mxn_unsafe_lwp_address = 0;
static CORE_ADDR mxn_safe_address = 0;
static CORE_ADDR mxn_safe_fork_address = 0;
static void some_symbol_table_got_loaded (char*);
static int before_fork_dummy = 0;


/* This semaphore is used to coordinate the child and parent processes
   after a fork(), and before an exec() by the child.  See parent_attach_all
   for details.
 */
static struct
  {
    int parent_channel[2]; /* Parent "talks" to [1], child "listens" to [0] */
    int child_channel[2];  /* Child "talks" to [1], parent "listens" to [0] */
  } startup_semaphore;

#define SEM_TALK (1)
#define SEM_LISTEN (0)

/* See can_touch_threads_of_process for details. */
static int vforking_child_pid = 0;
int vfork_in_flight = 0;
int no_of_preforks = 0;
int multi_threaded_preforks = 0;

/* To support PREPARE_TO_PROCEED (hppa_prepare_to_proceed).
 */
static pid_t reported_pid = 0;
static int reported_bpt = 0;

static ttstate_t null_ttstate_t;
static ttevent_t null_ttevent_t;
static int register_num_for_io;

/* Support logging of calls to ttrace and ttrace_wait */

int log_ttrace_calls = 	0;
int log_ttrace_wait_calls = 	0;

/********************************************************************

                 How this works:

   1.  Thread numbers

	To identify the main thread, gdb uses the process id and gdb
   gets "acquainted" with the main thread before this module enters
   the picture. All other threads (if any) are introduced to gdb by
   infttrace.c. Given this, how should this module expose the threads
   to gdb becomes an important question.

        We cannot converse in terms of LWP ids for two reasons :
   (a) There is nothing that stops an LWP from having the same id
   as its owning process. (b) With the future (MxN) looming large, LWP
   ids are a bad choice as there could be several threads multiplexed
   on top of an LWP and each thread must be exposed to gdb as being
   unique.

	As a result we "synthesize" a thread id here that cannot
   conflict with any process's id. See map_to_gdb_tid (). This
   artifical thread ID is used only for internal purposes. They
   cannot be passed to ttrace nor should be exposed to the user.
   As a result we apply a reverse lookup just before calling ttrace.
   See call_real_ttrace & call_real_ttrace_wait -- srikanth, 000814.


   2. Step and Continue

   Since we're implementing the "stop the world" model, sub-model
   "other threads run during step", we have some stuff to do:

   o  User steps require continuing all threads other than the
      one the user is stepping;

   o  Internal debugger steps (such as over a breakpoint or watchpoint,
      but not out of a library load thunk) require stepping only
      the selected thread;

****************************************************************
*/

/* FIX: this is used in inftarg.c/child_wait, in a hack.  */
extern int not_same_real_pid;

/* Sometimes we have to wait on only one thread. Examples are
   when single stepping over a breakpoint or detecting memory
   leaks. If non-zero, the variable wait_on_tid specifies the
   id of the thread.
*/
static tid_t wait_on_tid = 0;


/****************************************************
 * Thread information structure routines and types. *
 ****************************************************
 */
typedef
struct thread_info_struct
  {
    int pid;			/* Process ID */
    tid_t gdb_tid;		/* Thread  ID (internal) */
    lwpid_t ktid;		/* K Thread  ID */
    pthread_t utid;             /* User space thread ID. */
    int seen;			/* was this thread seen on a traverse */
    ttstate_t last_stop_state;	/* The most recent event for this tid */
    struct thread_info_struct *next; /* next thread */
  }
thread_info;

static thread_info * thread_list;

/* For HP-UX IA64, instead of a TTEVT_SIGNAL with a signal of _SIGTRAP,
   we get a TTEVT_BPT_SSTEP event.  Here we translate a TTEVT_BPT_SSTEP
   to the old equivalent.
   For data register breakpoints, don't translate TTEVT_BPT_SSTEP if
   it's really a TTBPT_DBPT; TTBPT_DBPT is processed in proc_wait.
   */
 
#if !defined(HP_IA64) && TT_FEATURE_LEVEL <7  // For JAGaf52708
/*  TTBPT_DBPT event is not there on PA */
#define TRANSLATE_THREAD_STATE(thread_state_p) \
	if ((thread_state_p)->tts_event & TTEVT_BPT_SSTEP) \
	  {							\
	    (thread_state_p)->tts_event &= ~TTEVT_BPT_SSTEP;            \
            (thread_state_p)->tts_event |= TTEVT_SIGNAL;                \
            (thread_state_p)->tts_u.tts_signal.tts_signo = _SIGTRAP;    \
          }
#else
#define TRANSLATE_THREAD_STATE(thread_state_p) \
        if ((thread_state_p)->tts_event & TTEVT_BPT_SSTEP &&            \
            (thread_state_p)->tts_u.tts_bpt_sstep.tts_isbpt != TTBPT_DBPT) \
          {                                                             \
            (thread_state_p)->tts_event &= ~TTEVT_BPT_SSTEP;            \
            (thread_state_p)->tts_event |= TTEVT_SIGNAL;                \
            (thread_state_p)->tts_u.tts_signal.tts_signo = _SIGTRAP;    \
          }
#endif


#ifdef HP_MXN
/* srikanth, if registered, the hook register_io_hook will be notified
   when register I/O happens. The passed parameter identifies the
   register number that is being written to or read from. It is a mini
   nightmare attempting to convert save_state offsets back to register
   numbers. Hence the callback.
*/
void (*register_io_hook) (int) = 0;
#endif

extern int in_startup_starting_shell;
extern void set_trace_bit (int);
extern void invalidate_rse_info (void);

/* MERGE: Code changes for JAGaf07518 - foll-vfork.exp failure in IPF on merge branch. 
   Function to reset the variables wait_on_prefork_pid and wait_on_prefork_tid.     */
void 
reset_prefork_wait ()
{
	wait_on_prefork_pid = wait_on_prefork_tid = 0;
}

/* Create, fill in and link in a thread descriptor.  */
static thread_info *
create_thread_info (int pid, composite_tid_t tid)
{
  thread_info *new_p;

  new_p = xcalloc (1, sizeof (thread_info));
  memset (new_p, 0, sizeof (thread_info));

  new_p->pid = pid;
  new_p->ktid = tid.ktid;
  new_p->utid = tid.utid;
  new_p->gdb_tid = tid.gdb_tid;

  new_p->next = thread_list;
  thread_list = new_p;

  return new_p;
}


static void
clear_thread_info ()
{
  thread_info *p;
  thread_info *q;

  p = thread_list;
  while (p)
    {
      q = p;
      p = p->next;
      free (q);
    }

  thread_list = NULL;
}


static thread_info *
find_thread_info (tid_t gdb_tid)
{
  thread_info *p;

  for (p = thread_list; p; p = p->next)
    if (p->gdb_tid == gdb_tid)
      return p;

  return NULL;
}


/* Now we keep track of the tids exposed to gdb in a field
   called gdb_tid. Because of the way exposed tids are computed
   for the main thread, gdb_tid == pid.
*/ 
static tid_t
map_from_gdb_tid (tid_t gdb_tid)
{
  thread_info *p;

  gdb_tid = PIDGET (gdb_tid);

  for (p = thread_list; p; p = p->next)
    if (p->gdb_tid == gdb_tid)
      return gdb_tid;

  error ("Internal error: unknown thread %d\n", gdb_tid);
  /* silence aCC6 warning about no return value */
  return NULL;
}

/* 3/25/05 coulter 
 * Prior to AR0604 we used the top three bits for:
 *	CMA threads
 *	Kernel threads
 *	MxN threads
 * At 11.11, the maximum number of process IDs was increased to 2^30 which
 * conflicts with the third bit.  At AR0604 the bits were shuffled around
 * and CMA thread debugging was allowed only if the PID <= 2^29, which 
 * will be the case on a small PID system.  Now the top three bits are:
 *	Kernel threads
 *	MxN threads
 *	CMA threads
 * The top two bits do not conflict with the system PIDs.  The third flag
 * is only used for a CMA program and CMA debugging is only allowed
 * if the process PID <= 2^29, the limit prior to 11.11.
 *
 * We may evenutally have to use all 32 bits for the system PID.  This will
 * be difficult for us.  See /home/coulter/PROJ/DEBUG/2004/large_pid_ir
 * for thoughts on what happens then.
 */


/* Given a process ID, lwpid and a user thread id within that process
   compute an artificial thread id that we could pass to gdb such that :
   (a) No two threads in the same process will end up with the same
   computed thread id. (b) For the first thread, the computed thread id
   should be equal to pid. This is because gdb already has a notion of
   the ID of the first thread. We cannot change this notion. Here we
   are assuming that Pthreads does *not* recycle user thread IDs.
   (this is true except for the kernel threads with no user thread id
   associated with them.) (c) A synthesized thread id could never conflict
   with a process id.
*/

/* The synthesized thread id for the first thread in the process is
   equal to its process id. For all other threads, we take the
   pthread id and set the most significant bit. This would work 
   as long as the application creates less that 1 billion threads, 
   which is large enough. I would actually like to set the most 
   significant bit, but that is used already by the CMA implementation. 
   This produces a number which cannot collide with any process id or with 
   other pthread ids -- srikanth, 001015.
   bindu 101601: For the threads with pthread id -1, we take the lwpid 
   and set the second most significant bit. Possible in MxN threads. 
   Now the limit for number of threads/processes is 1/2 billion!!
*/ 
/* bindu 101601: For corefiles, let the ktid be gdbtid. If no ktid available,
   we take utid and set the most significant bit.*/
static tid_t
map_to_gdb_tid (pid_t pid, pthread_t utid, lwpid_t ktid)
{
  thread_info * p;
  int seen_other_threads_for_pid;



  /* srikanth, 001025, If we debug a CMA threaded program under the
     11.00 ttrace gdb, our thread list iterations (get_first & get_next)
     will always show us only one thread as there is only one kernel
     thread for the process.

     However, the kernel is kinda sorta aware of the presence of
     multiple user space threads as evidenced by the changing value of
     the user thread id value in the ttstate structure. (newer versions
     of libcma use a light weight system call to change the kernel's
     notion.)

     We need to watch out for this case and recognize that even when
     the user space thread id changes, it is really one kernel level
     thread that exists for the process. The discovery of current
     thread list is a job that is completely handled by hpux-thread.c
  */

  if (is_prog_using_cma_threads)
    return pid;

  /* Have we already assigned an internal thread id for this ?
     If so return the same synthetic id.
  */

  seen_other_threads_for_pid = 0;
  p = thread_list;
  while (p)
    {
      if (p->pid == pid)
        {
	  /* seen_other_threads_for_pid is used to check if we have seen
	     any user threads for this pid. */
	  if (p->utid != -1)
            seen_other_threads_for_pid = 1;

          /* already assigned internal tid */
	  if (   ((p->utid != -1) && (p->utid == utid)) 
               || ((p->utid == utid) && (p->ktid == ktid)))
            return p->gdb_tid;
        }
      p = p->next;
    }

  /* bindu 101601: For corefiles, let the ktid be gdbtid. If no ktid available, 
     use utid.*/
  if (!target_has_execution && !attach_flag)
    {
      if (ktid)
        return ktid;
      else
        return utid | TID_MARKER;
    }
  else
    {
      /* For the first user thread, set the gdb_tid to pid. */
      if ((!seen_other_threads_for_pid) && (utid != -1))
        return pid;
      else 
        {
          if (utid != -1)
            return utid | TID_MARKER;
          else 
            return ktid | KTID_MARKER;
        }
    }
}

/* Is this pid a real PID or a synthesized TID? */
int
is_process_id (int pid)
{
  thread_info *tinfo;

  /* if the second/third most significant bit is set it cannot be a pid. */
  if ((pid & TID_MARKER) || (pid & KTID_MARKER))
    return 0;

  for (tinfo = thread_list; tinfo; tinfo = tinfo->next)
    if (tinfo->pid == pid)
      return 1;

  return 0;
}


static void
del_tthread (tid_t gdb_tid)
{
  thread_info *this, *previous, *next;

  previous = 0;
  this = thread_list;
  while (this)
    {
      next = this->next;
      if (this->gdb_tid == gdb_tid)
        {
          if (previous == 0)
            thread_list = next;
          else
            previous->next = next;
          
          free (this);
          return;
        }

      previous = this;
      this = next;
    }
  error ("Internal error : Missing thread %d\n", gdb_tid);
}

lwpid_t
get_lwp_for (tid_t gdb_tid)
{
  thread_info * t;

  t = thread_list;
  while (t)
    {
    /* JAGaf62389 - Command line calls fail for programs linked with cma threads library.
       If CMA thread, Mask off top 16/23 bits of <gdb_tid>. */
      if (t->gdb_tid == PIDGET (gdb_tid)) /* JAGaf62389 - <END> */
         return t->ktid;
      t = t->next;
    }

  error ("Internal error : Invalid thread id %d\n", gdb_tid);
  /* silence aCC6 warning about no return value */
  return NULL;
}

/* Returns the user thread id associated with the given gdb_tid. */
pthread_t
get_pthread_for (tid_t gdb_tid)
{
  thread_info * t;

  if (gdb_tid == 0)
    return 0;

  t = thread_list;
  while (t)
    {
      if (t->gdb_tid == gdb_tid)
        return t->utid;
      t = t->next;
    }

  error ("Internal error : Invalid thread id %d\n", gdb_tid);
  /* silence aCC6 warning about no return value */
  return NULL;
}

/* Returns the gdb_tid associated with the given user_thread. */
tid_t
get_gdb_tid_from_utid (pthread_t utid)
{
  thread_info * t;

  t = thread_list;
  while (t)
    {
      if (t->utid == utid)
        return t->gdb_tid;
      t = t->next;
    }

  return -1;
}

/* Returns the kernel tid associated with the given user_thread. */
lwpid_t
get_lwpid_from_utid (pthread_t utid)
{
  thread_info * t;

  t = thread_list;
  while (t)
    {
      if (t->utid == utid)
        return t->ktid;
      t = t->next;
    }

  return -1;
}

/* Returns the gdb_tid associated with the given ktid. */
tid_t
get_gdb_tid_from_ktid (lwpid_t ktid)
{
  thread_info * t;

  t = thread_list;
  while (t)
    {
      if (t->ktid == ktid)
        return t->gdb_tid;
      t = t->next;
    }

  return -1;
}

tid_t
get_main_thread_tid ()
{
  thread_info *p;

  for (p = thread_list; p; p = p->next)
    if (p->utid == 1)
      return p->gdb_tid;

  return 0;
}

int
get_pid_for (tid_t gdb_tid)
{
  thread_info *p;

  for (p = thread_list; p; p = p->next)
    if (p->gdb_tid == gdb_tid)
      return p->pid;

  return 0;
}

/* After a fork, once we have decided to follow the child or stay with
   the parent, discard the threads of the other. We used to add these
   to the deleted thread list, but I blew that away : as we are not
   controlling this process anymore, there is no use for these and
   they only tends to confuse things -- srikanth, 000726.
*/
static void
discard_threads_of (pid_t pid)
{
  thread_info *this, *next;

  this = thread_list;
  while (this)
    {
      next = this->next;
      if (this->pid == pid)
        del_tthread (this->gdb_tid);
      this = next;
    }
}

/************************************************
 *            O/S call wrappers                 *
 ************************************************
 */

/* This function simply calls ttrace with the given arguments.
 * It exists so that all calls to ttrace are isolated.  All
 * parameters should be as specified by "man 2 ttrace".
 *
 * No other "raw" calls to ttrace should exist in this module.
 */
int
call_real_ttrace (ttreq_t request, pid_t pid, tid_t tid,
                  uint64_t addr, uint64_t data, uint64_t addr2)
{
  int tt_status;
  struct lwp_status buf;
  int ret = 0;
  lwpid_t lwp;
  pthread_t utid;
  ttstate_t *thread_state;

  lwp = tid ? get_lwp_for (tid) : 0;
  utid = tid ? get_pthread_for (tid) : 0;
  if (log_ttrace_calls)
    {
      fprintf_filtered (gdb_stderr, 
		        "ttrace call request %d, pid %d, tid 0x%x, "
			  "addr 0x%lx, data 0x%lx, addrs 0x%lx\n",
			request, pid, tid, addr, data, addr2);
    }

  errno = 0;
  /* Bindu, If the thread is unbound and not running currently,
     do not allow register writes. Allowing register writes into
     the user space save state structure could interfere with the
     thread's capability to switch itself back in. This is caught
     at the level of the outer layers themselves. We also check here
     just in case... 020502.
  */

  if (request == TT_LWP_WUREGS && lwp == 0)
    {
      errno = EPROTO;
      return -1;
    }

  /* bindu 101601: Route the ttrace calls in a corefile context to mxn_core.
     The requests used for mxn_core are TT_LWP_RUREGS,
     TT_PROC_GET_FIRST_LWP_STATE and TT_PROC_GET_NEXT_LWP_STATE.
   */

  if (!target_has_execution && target_has_stack && (request != TT_PROC_SETTRC))
    tt_status = mxn_core (request, PIDGET (pid),
                          lwp, utid, addr, data, addr2);
  else
    tt_status = mxn_ttrace (request, PIDGET (pid),
                            lwp, utid, addr, data, addr2);

/* While attaching running HP MPI (Message passing Interface) process
   to gdb, gdb was getting confused because other than "main" thread
   "Kernel daemon" thread (Used by MPI applns) also has the 
   user tid of zero. 

       On HP-UX version of >11.20, pstat_getlwp() interface
   is used to identify "kernel daemon" thread. We initialize
   the utid to -1 ,if it's a kernel daemon thread.
*/
  thread_state = ((ttstate_t *) (unsigned long) addr);
  if (  
      ( is_11_20_or_greater ) /* The flag LWP_USER_KERNEL_DAEMON, used for
				 * identifying Kernel Daemon thread, is avlbl
				 * only on OS version of 11.20 or greater 
			         */
     &&	! errno 
     && ( request == TT_PROC_GET_FIRST_LWP_STATE
          || request == TT_PROC_GET_NEXT_LWP_STATE ) 
     && ( thread_state->tts_user_tid == 0)
     && ( thread_state->tts_pid == pid)
				/* Are we examining correct pid? */
     && ( target_has_stack && target_has_execution)
     )
         {
             bzero(&buf,sizeof(struct lwp_status));

             /* Get the thread state of interested thread */
             ret = pstat_getlwp( &buf, (size_t) sizeof (struct lwp_status),
                                 0, (int) thread_state->tts_lwpid, (pid_t) pid);
             if ( ret == -1 ) 
               {
                 error ("pstat_getlwp failed!\n");
               }

             assert ( buf.lwp_lwpid == thread_state->tts_lwpid );
             assert ( buf.lwp_pid == pid );

             /* If Kenel daemon thread ,change utid to -1 */
             if ( buf.lwp_flag & LWP_USER_KERNEL_DAEMON ) 
               {
                  thread_state->tts_user_tid = (uint64_t)(long) -1;
               } 
        }

  return tt_status;
}

/* This function simply calls ttrace_wait with the given arguments.  
 * It exists so that all calls to ttrace_wait are isolated.
 *
 * No "raw" calls to ttrace_wait should exist elsewhere.
 */
static int
call_real_ttrace_wait (int pid, tid_t tid, ttwopt_t option,
                       ttstate_t * tsp, size_t tsp_size)
{
  int ttw_status;
  thread_info *p;

  *tsp = null_ttstate_t;


  tid = tid ? get_lwp_for (tid) : 0;
  if (log_ttrace_wait_calls)
    {
      fprintf_filtered (gdb_stderr, 
			"ttrace_wait pid %d, tid %d, option %d\n", 
			pid, tid, option);
    }
  errno = 0;
#if TT_FEATURE_LEVEL >= 10
  if (is_vfork && wait_on_prefork_tid && (feature_level>= 10) ) {
      pid = wait_on_prefork_pid;
      tid = wait_on_prefork_tid;
  }
#endif
  fake_sig0_flag = 0;
  ttw_status = ttrace_wait (pid, tid, option, tsp, tsp_size);

  if (ttw_status == -1 && errno == EINTR) 	/* Fix for JAGaf27540. */
    {
      /* Threads won't be stopped for an interrupted ttrace_wait(), so */
      stop_all_threads_of_process (get_pid_for (inferior_pid));

      /* Fake a signal event with signal 0. Pick up the first thread
	 in the thread list as the event producing thread. */
      tsp->tts_pid = thread_list->pid;
      tsp->tts_lwpid = thread_list->ktid;
      tsp->tts_user_tid = thread_list->utid;
      tsp->tts_event = TTEVT_SIGNAL;
      tsp->tts_u.tts_signal.tts_signo = 0;
      ttw_status = 0;
      fake_sig0_flag = 1;
    }

#if TT_FEATURE_LEVEL >= 10
  if ( (tsp->tts_pid == 0) && wait_on_prefork_pid && (feature_level>= 10) ) 
    {
       /* OOPS!! We were expecting wait_on_prefork_tid but
	  we got zero pid. Let us see if it's a Fork failure event */

       tid = get_lwp_for (reported_pid);
       pid = get_pid_for (reported_pid);
       while ( ttw_status != 0 )
        {
          /* Let us do a non-blocking wait on parent pid & tid to get
	     the FAIL vfork event */
          errno = 0;
          ttw_status = ttrace_wait (pid, tid,  TTRACE_NOWAIT, tsp, tsp_size);
          if ( ( tsp->tts_event == (TTEVT_FORK_FAIL)) || ( ttw_status == 0 ) ) 
            break;
          errno = 0;
          ttw_status = ttrace_wait (wait_on_prefork_pid, wait_on_prefork_tid, 
                                  option, tsp, tsp_size);
	  if (log_ttrace_wait_calls)
    	    printf_filtered ("#2 event %d, utid %lld, ktid %d, pid %d\n",
		 	     tsp->tts_event, tsp->tts_user_tid,
			     tsp->tts_lwpid, tsp->tts_pid);
        }
    }
#endif

  /* bindu 082602: Clear mxn_info on exec. We will need to re-initialize
     everything when libpthread library gets loaded in the new executable. */
  if (tsp->tts_event & TTEVT_EXEC)
    {
      clear_mxn_info ();
      exited_thread = 0;
    }

  if (errno)
    {
      clear_sigint_trap (); /* Part of fix for JAGaf27540. */
      perror_with_name ("ttrace wait");
    }

  if (log_ttrace_wait_calls)
    printf_filtered ("event %d, utid %lld, ktid %d, pid %d\n",
		     tsp->tts_event, tsp->tts_user_tid,
		     tsp->tts_lwpid, tsp->tts_pid);

  /* bindu 101601: The only time when utid will be 0 for the first thread
   * in the process is when libpthread is not loaded. Set it to 1.
   */
  /* jini: Aug 2008: On IPF, utid can be zero for threads other than the first
   * thread in an application linked with liblwp.a and using only lwp threads.
   * QXCR1000757565: Debugging MT applications using lwp (no user tid)
   * For apps that rely on only lwp, the utid for any thread is always
   * zero, while the ktids are different. Modify the user tid to -1 in these
   * cases such that the gdb composite tid is created using the ktid.
   * Make the modification only if this is not the first (main) thread.
   * The flag LWP_FIRSTLWP is used to figure this out.
   */ 
  if ( (tsp->tts_user_tid == 0) &&
       (!is_mxn
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
        || !mxn_libpthread_init_done()
#endif
       ))
    {
#ifdef HP_IA64
      struct lwp_status buf;
      int ret = 0;
      bzero (&buf, sizeof (struct lwp_status));
      
      /* Get the thread state of interested thread */
      ret = pstat_getlwp (&buf, (size_t) sizeof (struct lwp_status),
                         0, (int) tsp->tts_lwpid, (pid_t) tsp->tts_pid);
      if (ret == -1) 
         {
            error ("pstat_getlwp failed!\n");
         }

      assert (buf.lwp_lwpid == tsp->tts_lwpid);
      assert (buf.lwp_pid == tsp->tts_pid);

      if (!(buf.lwp_flag & LWP_FIRSTLWP))
         tsp->tts_user_tid = (uint64_t)(long) -1;
      else
#endif
         tsp->tts_user_tid = 1;
    }
  return ttw_status;
}

static composite_tid_t
composite_tid (ttstate_t * state)
{
  composite_tid_t tid;

  tid.pid = state->tts_pid;
  tid.ktid = state->tts_lwpid;


  /* Ttrace returns a user thread id of 0 for the main thread.
     This is wrong. It should be 1 as returned by pthread_self 
     called on the main thread.
  */
#ifdef HP_IA64
  /* jini: Aug 2008: */
  /* QXCR1000757565: Debugging MT applications using lwp (no user tid)
   * For apps that rely on only lwp, the utid for any thread is always
   * zero, while the ktids are different. Modify the user tid to -1 in these
   * cases such that the gdb composite tid is created using the ktid.
   * Make the modification only if this is not the first (main) thread.
   * The flag LWP_FIRSTLWP is used to figure this out.
   */ 
  if ((pthread_t) state->tts_user_tid == 0)
  {
      /* Likely to be a kernel thread. */
      struct lwp_status buf;
      int ret = 0;
      bzero (&buf, sizeof (struct lwp_status));
      
      /* Get the thread state of interested thread */
      ret = pstat_getlwp (&buf, (size_t) sizeof (struct lwp_status),
                         0, (int) state->tts_lwpid, (pid_t) state->tts_pid);
      if (ret == -1) 
         {
            error ("pstat_getlwp failed!\n");
         }

      assert (buf.lwp_lwpid == state->tts_lwpid);
      assert (buf.lwp_pid == state->tts_pid);

      /* If this is not the main thread and the user tid is zero, this ought to
         be a lwp thread. */
      if (!(buf.lwp_flag & LWP_FIRSTLWP))
         {
           state->tts_user_tid = (uint64_t)(long) -1;
         }
  }
#endif
     
  tid.utid = (pthread_t) state->tts_user_tid ? (pthread_t) state->tts_user_tid : 1;

  /* We will expose this thread to gdb as ... */
  tid.gdb_tid = map_to_gdb_tid (tid.pid, tid.utid, tid.ktid);

  return tid;
}



/* A process may have one or more threads, of which all or
   none may be stopped.  This function returns the ID of the first
   thread in a stopped state, or 0 if none are stopped.

   This function can be used with get_process_next_stopped_thread_id
   to iterate over the IDs of all stopped threads of this process.
 */
static composite_tid_t
get_process_first_stopped_thread_id (int pid, ttstate_t * thread_state)
{
  int tt_status;

  *thread_state = null_ttstate_t;

  tt_status = call_real_ttrace (TT_PROC_GET_FIRST_LWP_STATE,
                                (pid_t) pid,
                                0,
                                (uint64_t) thread_state,
                                sizeof (*thread_state),
                                0);

  if (target_has_execution || attach_flag)
    {
      TRANSLATE_THREAD_STATE(thread_state);
      if (errno)
        perror_with_name ("ttrace");
    }

  if (tt_status < 0) /* Failed somehow  */
    {
      errno = EINVAL;
      return null_tid;
    }

    return composite_tid (thread_state);
} /* end get_process_first_stopped_thread_id */

/* This function returns the ID of the "next" thread in a
   stopped state, or 0 if there are none.  "Next" refers to the
   thread following that of the last successful call to this
   function or to get_process_first_stopped_thread_id, using
   the value of thread_state returned by that call.

   This function can be used with get_process_first_stopped_thread_id
   to iterate over the IDs of all stopped threads of this process.
 */
static composite_tid_t
get_process_next_stopped_thread_id (int pid, ttstate_t * thread_state)
{
  int tt_status;

  *thread_state = null_ttstate_t;

  tt_status = call_real_ttrace (TT_PROC_GET_NEXT_LWP_STATE,
                                (pid_t) pid,
                                0,
                                (uint64_t) thread_state,
                                sizeof (*thread_state),
                                0);

  if (target_has_execution || attach_flag)
    {
      TRANSLATE_THREAD_STATE(thread_state);

      if (errno)
        perror_with_name ("ttrace TT_PROC_GET_NEXT_LWP_STATE");
    }

  if (tt_status <= 0) /* Failed || No more threads */
    return null_tid;

  return composite_tid (thread_state);
}

/* Can this thread be continued all by itself at a
   different address ? Yes if it is not suspended,
   sleeping, in syscall, or in exit.
*/
int
thread_could_run_gc (tid_t gdb_tid)
{
  thread_info * p;
  ttstate_t thread_state;
  p = find_thread_info (map_from_gdb_tid(gdb_tid));
  thread_state = p->last_stop_state;

  if (thread_state.tts_flags & TTS_WASSUSPENDED)
    return TTS_WASSUSPENDED;
  if (thread_state.tts_flags & TTS_WASSLEEPING) 
    return TTS_WASSLEEPING;
  if (thread_state.tts_flags & TTS_INSYSCALL)
    return TTS_INSYSCALL;
  if (thread_state.tts_flags & TTS_ATEXIT)
    return TTS_ATEXIT;
  if ((thread_state.tts_event == TTEVT_NONE ||
        thread_state.tts_flags & TTS_WAITEDFOR) && 
        thread_state.tts_flags & TTS_WASRUNNING)
    return TTS_WASRUNNING;

  return 0;
}

/* JAGag12946 - Can this thread be continued all by itself at a different 
   address? Yes if it is not suspended, sleeping, in syscall, or in exit.
   Only consider threads that have a valid user ID. In MxN case some service
   threads are in a state that does not allow to run user code.

   If none of the threads are running then return the state of main thread. 
*/

tid_t
select_thread_could_run_gc (int *thread_status)
{
  thread_info * p;
  ttstate_t thread_state;
  thread_info *this;
  tid_t gdb_tid = 0;

  /* First check if the current thread is OK to run */
  p = find_thread_info (map_from_gdb_tid(inferior_pid));
  thread_state = p->last_stop_state;
  if (p->utid != -1 &&  
      !(thread_state.tts_flags & TTS_WASSUSPENDED) &&
      !(thread_state.tts_flags & TTS_WASSLEEPING)  &&
      !(thread_state.tts_flags & TTS_INSYSCALL)    &&
      !(thread_state.tts_flags & TTS_ATEXIT))
    {
      *thread_status = TTS_WASRUNNING;
      return inferior_pid;
    }

  this = thread_list;
  while (this)
    {
      gdb_tid = this->gdb_tid;
      p = find_thread_info (map_from_gdb_tid(gdb_tid));
      thread_state = p->last_stop_state;

      /* Pick a thread with a valid user ID and is not suspended, sleeping
         in a system call or in atexit */
      if (p->utid == -1 ||  
          (thread_state.tts_flags & TTS_WASSUSPENDED) ||
          (thread_state.tts_flags & TTS_WASSLEEPING)  ||
          (thread_state.tts_flags & TTS_INSYSCALL)    ||
          (thread_state.tts_flags & TTS_ATEXIT))
        {
          this = this->next;
          continue; 
        }
      else
        {
          break;
        }
    }
  if (this)
    {
      /* We got a running. So we can proceed to info leaks on this
         thread */
      *thread_status = TTS_WASRUNNING; 
      return gdb_tid;
    }
  else
    {
      /* We will come here only if none of threads are in running state.
         In such situation we will retain the old behavior of reporting 
         the problem ie we will report the state of the inferior pid.
      */
       gdb_tid = inferior_pid;
       p = find_thread_info (map_from_gdb_tid(gdb_tid));
       thread_state = p->last_stop_state;

       if (thread_state.tts_flags & TTS_WASSUSPENDED)
         *thread_status = TTS_WASSUSPENDED;
       else if (thread_state.tts_flags & TTS_WASSLEEPING)
           *thread_status = TTS_WASSLEEPING;
       else if (thread_state.tts_flags & TTS_INSYSCALL)
           *thread_status = TTS_INSYSCALL;
       else if (thread_state.tts_flags & TTS_ATEXIT)
           *thread_status = TTS_ATEXIT;
       else if ((thread_state.tts_event == TTEVT_NONE ||
           thread_state.tts_flags & TTS_WAITEDFOR) &&
           thread_state.tts_flags & TTS_WASRUNNING)
           *thread_status = TTS_WASRUNNING;
      return 0;
    }
}

/* This function translates the "pid" used by the rest of
 * gdb to a real pid and a tid.  It then calls "call_real_ttrace"
 * with the given arguments.
 *
 * In general, other parts of this module should call this
 * function when they are dealing with external users, who only
 * have tids to pass (but they call it "pid" for historical
 * reasons).
 */
int
call_ttrace (ttreq_t request, int gdb_tid, uint64_t addr,
             uint64_t data, uint64_t addr2)
{
  tid_t real_tid;
  int real_pid;
  int tt_status;

  /* srikanth, 000728, if the thread id is bogus, don't try to work
     around the problem. Just punt. Now that our thread list is accurate
     at all times, there is no excuse for outer layers to pass us a
     bogus thread id.

     The initial SETTRC and SET_EVENT_MASK calls (and all others
     which happen before any threads get set up) should go
     directly to "call_real_ttrace", so they don't happen here.
  */

  real_tid = map_from_gdb_tid (gdb_tid);
  real_pid = get_pid_for (real_tid);

  /* Ttrace doesn't like to see tid values on process requests,
     even if we have the right one.
   */
  if (IS_TTRACE_PROCREQ (request))
    real_tid = 0;

  tt_status = call_real_ttrace (request, real_pid, real_tid, addr, data, addr2);

  return tt_status;
}

/* This function ensures that this thread is stopped in a safe place in
 * pthread library. Some internal threads of pthread library which get
 * reported to gdb can be in an unsafe place where it is not safe to query
 * the state of the threads. If mxn_unsafe_lwp_address is set, the thread is
 * not in a safe place. So run it till the thread hits a breakpoint we
 * place in mxn_safe_address.
 */
static void
mxn_stop_all_threads_of_process (pid_t real_pid)
{
  lwpid_t lwp;
  int tcode;
  ttstate_t stop_status;
  char buffer[16];
  struct minimal_symbol * m;

  if (mxn_unsafe_lwp_address == 0)
    return;

  if (mxn_safe_address == 0)
    return;

  errno = 0;
  tcode = ttrace (TT_PROC_RDDATA, real_pid, 0,
           (uint64_t) mxn_unsafe_lwp_address,  4, (uint64_t) &lwp);
  if (tcode == -1)
    error ("Error reading from process \n");

  if (lwp == 0)
    return;
  /* JAGag26864 - Call target_insert_internal_breakpoint instead of 
     target_insert_breakpoint so that if software breakpoint fails, 
     hardware will be inserted.  
     If hardware insertion also fails, give chatr +dbg enable message.
  */
  int val = target_insert_internal_breakpoint (mxn_safe_address, buffer);
  if (val)
    {
      warning ("The shared libraries were not privately mapped; "
      "MXN threads cannot be debugged properly.\n"
      "Use the following command to enable debugging of shared libraries private.\n"
      "chatr +dbg enable <executable>");
      return;
    }

  tcode = ttrace (TT_LWP_CONTINUE, real_pid, lwp, TT_NOPC, 0, 0);
  if (tcode == -1)
    perror_with_name ("ttrace");
  errno = 0;
  tcode = ttrace_wait (real_pid, lwp, TTRACE_WAITOK, &stop_status,
                                         sizeof(stop_status));
  if (errno)
    perror_with_name ("ttrace wait");

  /* JAGaf26864 - Call the appropriate breakpoint remove function. */
  target_remove_internal_breakpoint (mxn_safe_address, buffer);
}

#if TT_FEATURE_LEVEL >= 10
#define PREFORKING(evt, prevforking) \
  (((evt) == TTEVT_PREFORK) && ( prevforking.tts_type == TTS_VFORK ))
#endif

/* Stop all the threads of a process and update our thread list. */
static void
stop_all_threads_of_process (pid_t real_pid)
{
  composite_tid_t tid;
  ttstate_t tstate;
  thread_info *this, *next;

  multi_threaded_preforks = 0;

  call_real_ttrace (TT_PROC_STOP, real_pid, 0, 0, 0, 0);
  if (errno)
    perror_with_name ("ttrace stop of all threads");

  if (is_mxn)
    mxn_stop_all_threads_of_process (real_pid);

  /* We now consult the system's thread list and adopt new ones and bury
     the dead. There is one small catch. For a brief window of time
     after a fork or a vfork, we are controlling both the parent and the
     child process. If the process just [v]forked, based on the current
     settting for follow-fork-mode, we will detach the other process in
     just a while from now. 

     Unless we are careful, we would declare the other process and its
     threads dead prematurely. To preclude this, we will do is this :
     set only the threads of real_pid as unseen, iterate over the
     system's thread list and discover new and old ones and after the
     iteration discard the unseen threads of this process alone.

     When eventually we detach either the child or the parent,
     discard_threads_of () or clear_thread_info() will take care
     of the rest.
  */
     
  this = thread_list;
  while (this)
    {
      if (this->pid == real_pid)
        this->seen = 0;
      this = this->next;
    }

  if (log_ttrace_calls)
    printf_filtered ("Satop: new threads\n");

  no_of_preforks = 0;
  for (tid = get_process_first_stopped_thread_id (real_pid, &tstate);
       ((tid.gdb_tid != 0)||(tid.ktid != 0)||(tid.utid != 0));
       tid = get_process_next_stopped_thread_id (real_pid, &tstate))
    {
      if (log_ttrace_calls)
	printf_filtered ("gdb_tid: 0x%x, ktid: %d, utid: %d\n",
			 tid.gdb_tid, tid.ktid, tid.utid);
      this = find_thread_info (tid.gdb_tid);

      if (!this || this->pid != real_pid)
        {
          this = create_thread_info (real_pid, tid);

          if (!in_thread_list (tid.gdb_tid))
            add_thread (tid.gdb_tid);
        }
      else     /* LWP may have changed ... */
        {
          this->ktid = tid.ktid;
        }

      this->seen = 1;
      this->last_stop_state = tstate;

#if TT_FEATURE_LEVEL >= 10
      if ( PREFORKING ( tstate.tts_event, tstate.tts_u.tts_prefork)
           && (feature_level>= 10))
        no_of_preforks++;
#endif
    }

  if (log_ttrace_calls)
    printf_filtered ("End satop: new threads\n");

  /* Now get rid of dead threads ... */
  this = thread_list;
  while (this)
    {
      next = this->next;
      if (!this->seen && this->pid == real_pid)
        del_tthread (this->gdb_tid);
      this = next;
    }

#ifdef HP_IA64
  /* Invalidate all global epts */
  invalidate_global_epts ();
#endif

  if (log_ttrace_calls)
    {
      this = thread_list;
      printf_filtered ("satop: thread_list\n");
      while (this)
        {
          next = this->next;
          printf_filtered ("gdb_tid 0x%x, utid %d, ktid %d, pid %d\n",
			   this->gdb_tid, this->utid, this->ktid, this->pid);
          this = next;
        }
      printf_filtered ("satop: end thread_list\n");
    }

#if TT_FEATURE_LEVEL >= 10
    if (no_of_preforks && (feature_level>= 10))
      multi_threaded_preforks = no_of_preforks;
#endif
}



#if TT_FEATURE_LEVEL >= 10
/* Stop all the threads of a process and continue only the vforking thread. */

static void
stop_all_threads_of_process_but_one (pid_t real_pid, lwpid_t lwpid)
{
  int tcode, errno;
  tid_t gdb_tid;
  

  /* Stop all the running threads */

  stop_all_threads_of_process (real_pid);

  /* Now continue only the vforking thread */
  gdb_tid = get_gdb_tid_from_ktid (lwpid);
  tcode = call_ttrace (TT_LWP_CONTINUE, gdb_tid, TT_NOPC, 0, 0);
  if (tcode == -1)
       perror_with_name ("Unable to continue the forking thread");
}
#endif

/* Under some circumstances, it's unsafe to attempt to stop, or even
   query the state of, a process' threads.

   In ttrace-based HP-UX, an example is a vforking child process.  The
   vforking parent and child are somewhat fragile, w/r/t what we can do
   what we can do to them with ttrace, until after the child exits or
   execs, or until the parent's vfork event is delivered.  Until that
   time, we must not try to stop the process' threads, or inquire how
   many there are, or even alter its data segments, or it typically dies
   with a SIGILL.  Sigh.

   Forking child process in MxN context has unreliable thread state
   immediately after fork.

   This function returns 1 if this stopped process, and the event that
   we're told was responsible for its current stopped state, cannot safely
   have its threads examined.
 */
#define CHILD_VFORKED(evt,pid) \
  (((evt) == TTEVT_VFORK) && ((pid) != get_pid_for(inferior_pid)))
#define CHILD_URPED(evt,pid) \
  ((((evt) == TTEVT_EXEC) || ((evt) == TTEVT_EXIT)) && ((pid) != vforking_child_pid))
#define PARENT_VFORKED(evt,pid) \
  (((evt) == TTEVT_VFORK) && ((pid) == get_pid_for(inferior_pid)))
#define CHILD_FORKED(evt,pid)\
  (((evt) == TTEVT_FORK) && (pid != get_pid_for(inferior_pid)))

static int
can_touch_threads_of_process (int pid, ttevents_t stopping_event)
{
  if (CHILD_FORKED (stopping_event, pid))
    return 0;

  if (CHILD_VFORKED (stopping_event, pid) )
    {
      vforking_child_pid = pid;
      vfork_in_flight = 1;
    }

  else if (vfork_in_flight &&
	   (PARENT_VFORKED (stopping_event, pid) ||
	    CHILD_URPED (stopping_event, pid))
            && (stopping_event != TTEVT_PREFORK))
    {
         vfork_in_flight = 0;
         vforking_child_pid = 0;
     }

          return !vfork_in_flight;
   }


/* This function wraps calls to "call_real_ttrace_wait" 
 *
 * Note that typically it is called with a pid of "0", i.e. 
 * the "don't care" value.
 *
 * If pid (internal gdb_tid) is set to a non-zero value, wait should
 * happen on that particular pid/thread.
 * 
 * Return value is the status of the wait.
 */
static int
call_ttrace_wait (int pid, ttwopt_t option, ttstate_t *tsp,
		  size_t tsp_size)
{
  /* This holds the actual, for-real, true process ID.
   */
  static int real_pid;

  /* As an argument to ttrace_wait, zero pid means "Any process"
     and zero tid means "Any thread of the specified process".
   */
  int wait_pid = 0;
  tid_t wait_tid = 0;
  int ttw_status = 0;		/* To be returned */
  thread_info * p;

  /* gdb_pid_for returns the process id associated with this pid (internal
     gdb_tid).  If this thread/process is not yet in the debugger's thread
     list, it returns 0. This situation can occur when we get parent's FORK
     event, we haven't yet seen the child and we try to wait for the child's
     event. If get_pid_for returns 0, set wait_tid to 0 and wait_tid to pid,
     so that we can wait for events from this process even though we do not
     yet know about this process. */
  if (pid)
    {
      real_pid = get_pid_for (pid);
      wait_tid = real_pid? pid: 0;
      wait_pid = real_pid? real_pid: pid;
    }

  /* Sometimes we have to wait for a particular thread
   * (if we're stepping over a bpt).  In that case, we
   * _know_ it's going to complete the single-step we
   * asked for (because we're only doing the step under
   * certain very well-understood circumstances), so it
   * can't block.
   */
  if (wait_on_tid)
    {
      wait_tid = wait_on_tid;
      wait_pid = get_pid_for (wait_on_tid);
    }

  ttw_status = call_real_ttrace_wait (wait_pid, wait_tid, option,
						   tsp, tsp_size);

    /* Reset the sigmask to allow SIGLWPTIMER after single step. */
  if (is_mxn && wait_on_tid )
    {
	ttmask_t mask;

	sigemptyset (&mask.ttm_signals);
	mask.ttm_opts = TTMO_NONE;
	call_ttrace (TT_LWP_SET_SIGMASK, wait_tid, (uint64_t) &mask,
	       sizeof (mask), 0);
    }

  TRANSLATE_THREAD_STATE (tsp);
#ifdef HP_IA64
  invalidate_rse_info ();
#endif

  real_pid = tsp->tts_pid;

#if TT_FEATURE_LEVEL >= 10
       if ( PREFORKING ( tsp->tts_event, tsp->tts_u.tts_prefork )
            && (feature_level>= 10) )
         {
            if ( !vfork_in_flight )
              {
                stop_all_threads_of_process_but_one (real_pid, tsp->tts_lwpid);
                vfork_in_flight = 1;
              }
            return ttw_status;
         } 
#endif 

  /* For most events: Stop the world!

     It's sometimes not safe to stop all threads of a process.
     Sometimes it's not even safe to ask for the thread state
     of a process!
  */
  if (can_touch_threads_of_process (real_pid, tsp->tts_event))
    {
      /* If we're really only stepping a single thread, then don't
         try to stop all the others -- we only do this single-stepping
	 business when all others were already stopped...and the stop
	 would mess up other threads' events.
	 Same with when pid is set. Just update this particular thread's
	 thread state.
      */
      if (!(wait_on_tid || pid)
#if TT_FEATURE_LEVEL >= 10
          || 
          ((no_of_preforks == 0) && (feature_level>= 10))
#endif
          )
        stop_all_threads_of_process (real_pid);
    }
  else 
    {
      thread_info * p;
      composite_tid_t tid;

      /* bindu 101601: When the fork event is reported for the child
       * process, child's thread info state is not set properly. So,
       * set the first thread of this process, which is the thread
       * that reported this event to 1.
       */
      if ((tsp->tts_event & TTEVT_FORK)
          && (real_pid != get_pid_for(inferior_pid)) && is_mxn)
        {
	  /* If the utid != 1 or 0, __pthread_fork_dummy has not yet
	     executed. That's when the utid will be changed to 1 for
	     the main thread in the debuggee. Before changing utid to
	     here, remember if this the thread is before
	     __pthread_fork_dummy is executed. */
	  if (tsp->tts_user_tid != 1 && tsp->tts_user_tid != 0)
	    before_fork_dummy = 1;
          tsp->tts_user_tid = 1;
	}

      tid = composite_tid (tsp);
      p = find_thread_info (tid.gdb_tid);

      if (!p)
        p = create_thread_info (real_pid, tid);
      else
        p->ktid = tid.ktid;  /* LWP may have changed ... */

      p->last_stop_state = *tsp;
    }


  if (wait_on_tid)   /* Done with the fake step.  */
    {
      p = find_thread_info (wait_on_tid);
      p->last_stop_state = *tsp;
      wait_on_tid = 0;
    }

  /* Update the last stop state of this thread. */
  if (pid)
    {
      p = find_thread_info (pid);
      p->last_stop_state = *tsp;
    }

  return ttw_status;
}


int
child_reported_exec_events_per_exec_call ()
{
  return 1;			/* ttrace reports the event once per call. */
}


/* Our implementation of hardware watchpoints involves making memory
   pages write-protected.  We must remember a page's original permissions,
   and we must also know when it is appropriate to restore a page's
   permissions to its original state.

   We use a "dictionary" of hardware-watched pages to do this.  Each
   hardware-watched page is recorded in the dictionary.  Each page's
   dictionary entry contains the original permissions and a reference
   count.  Pages are hashed into the dictionary by their start address.

   When hardware watchpoint is set on page X for the first time, page X
   is added to the dictionary with a reference count of 1.  If other
   hardware watchpoints are subsequently set on page X, its reference
   count is incremented.  When hardware watchpoints are removed from
   page X, its reference count is decremented.  If a page's reference
   count drops to 0, it's permissions are restored and the page's entry
   is thrown out of the dictionary.
 */
typedef struct memory_page
{
  CORE_ADDR page_start;
  int reference_count;
  int  already_protected;
  int original_permissions;
  struct memory_page *next;
  struct memory_page *previous;
}
memory_page_t;

#define MEMORY_PAGE_DICTIONARY_BUCKET_COUNT  128

static struct
  {
    LONGEST page_count;
    int page_size;
    int page_protections_allowed;
    /* These are just the heads of chains of actual page descriptors. */
    memory_page_t buckets[MEMORY_PAGE_DICTIONARY_BUCKET_COUNT];
  }
memory_page_dictionary;


static void
require_memory_page_dictionary ()
{
  int i;

  /* Is the memory page dictionary ready for use?  If so, we're done. */
  if (memory_page_dictionary.page_count >= (LONGEST) 0)
    return;

  /* Else, initialize it. */
  memory_page_dictionary.page_count = (LONGEST) 0;

  for (i = 0; i < MEMORY_PAGE_DICTIONARY_BUCKET_COUNT; i++)
    {
      memory_page_dictionary.buckets[i].page_start = (CORE_ADDR) 0;
      memory_page_dictionary.buckets[i].reference_count = 0;
      memory_page_dictionary.buckets[i].already_protected = 0;
      memory_page_dictionary.buckets[i].next = NULL;
      memory_page_dictionary.buckets[i].previous = NULL;
    }
}


/* Write-protect the memory page that starts at this address.

   Returns the original permissions of the page.
 */
static int
write_protect_page (int pid, CORE_ADDR page_start)
{
  int tt_status;
  int original_permissions = 0;
  int new_permissions;

  if (debug_traces)
    {
      printf_filtered("write_protect_page, page start = 0x%lx\n",
                      page_start);
    }

  tt_status = call_ttrace (TT_PROC_GET_MPROTECT,
			   pid,
			   page_start,
			   0,
			   (uint64_t) &original_permissions);
  if (errno || (tt_status < 0))
    return 0;			/* What else can we do? */

  /* We'll also write-protect the page now, if that's allowed. */
  if (memory_page_dictionary.page_protections_allowed)
    {
      new_permissions = original_permissions & ~PROT_WRITE;

      if (debug_traces)
        {
          printf_filtered("TT_PROC_SET_MPROTECT, page start = 0x%lx, "
                          "size = %d, perm = %x\n",
                           page_start, memory_page_dictionary.page_size,
                           new_permissions);
        }

      tt_status = call_ttrace (TT_PROC_SET_MPROTECT,
			       pid,
			       page_start,
			       memory_page_dictionary.page_size,
			       new_permissions);

      if (errno || (tt_status < 0))
        return 0;		/* What else can we do? */
    }

  return original_permissions;
}


/* Unwrite-protect the memory page that starts at this address, restoring
   (what we must assume are) its original permissions.
 */
static void
unwrite_protect_page (int pid, CORE_ADDR page_start,
                           int original_permissions)
{
  int tt_status;

  tt_status = call_ttrace (TT_PROC_SET_MPROTECT,
			   pid,
			   page_start,
			   memory_page_dictionary.page_size,
			   original_permissions);

  if (errno || (tt_status < 0))
    return;			/* What else can we do? */
}

/* Reset the hw watchpoint data structures to reflect the state 
   after the rerun. */
void
reset_memory_page_protections ()
{
  int i = 0;
  memory_page_dictionary.page_protections_allowed = 1;
  syscall_enabled_from_page_prot = 0;
  syscall_enabled_from_data_break = 0;

#ifdef HP_IA64
  /* Let's cleanup the data breakpoints info as well. */
  ia64_reset_data_reg ();
  ia64_reset_inst_reg ();
#endif

  if (memory_page_dictionary.page_count <= 0)
    return;

  memory_page_dictionary.page_count = (LONGEST) 0;

  for (i = 0; i < MEMORY_PAGE_DICTIONARY_BUCKET_COUNT; i++)
    {
      memory_page_dictionary.buckets[i].page_start = (CORE_ADDR) 0;
      memory_page_dictionary.buckets[i].reference_count = 0;
      memory_page_dictionary.buckets[i].already_protected = 0;
      memory_page_dictionary.buckets[i].next = NULL;
      memory_page_dictionary.buckets[i].previous = NULL;
    }
}


/* Memory page-protections are used to implement "hardware" watchpoints
   on HP-UX.

   For every memory page that is currently being watched (i.e., that
   presently should be write-protected), write-protect it.
 */
void
hppa_enable_page_protection_events (int pid)
{
  int bucket;

  memory_page_dictionary.page_protections_allowed = 1;

  for (bucket = 0; bucket < MEMORY_PAGE_DICTIONARY_BUCKET_COUNT; bucket++)
    {
      memory_page_t *page;

      page = memory_page_dictionary.buckets[bucket].next;
      while (page != NULL)
	{
          if (!page->already_protected)
            {
              page->original_permissions =
                write_protect_page (pid, page->page_start);
              page->already_protected = 1;
            }
	  page = page->next;
	}
    }
}


/* Memory page-protections are used to implement "hardware" watchpoints
   on HP-UX.

   For every memory page that is currently being watched (i.e., that
   presently is or should be write-protected), un-write-protect it.
 */
void
hppa_disable_page_protection_events (int pid)
{
  int bucket;

  for (bucket = 0; bucket < MEMORY_PAGE_DICTIONARY_BUCKET_COUNT; bucket++)
    {
      memory_page_t *page;

      page = memory_page_dictionary.buckets[bucket].next;
      while (page != NULL)
	{
          if (page->already_protected)
            {
              unwrite_protect_page (pid, page->page_start,
				    page->original_permissions);
	      if (!keep_as_protected)
                page->already_protected = 0;
            }
	  page = page->next;
	}
    }

  memory_page_dictionary.page_protections_allowed = 0;
}


/* This function is provided as a sop to clients that are calling
 * ptrace_wait to wait for a process to stop.  (see the
 * implementation of child_wait.)  Return value is the pid for
 * the event that ended the wait.
 *
 * Note: used by core gdb and so uses the internal tids.
 */
int
ptrace_wait (int pid, int * status)
{
  ttstate_t tsp;
  int ttwait_return;
  int real_pid;
  lwpid_t   real_ktid;
  pthread_t real_utid;
  int return_pid;

  /* When -1 is passed as the pid, we wait on events from all the processes.
     call_ttrace_wait expects a 0 for this. */
  if (pid == -1)
    pid = 0;

  *status = 0;
  just_exited_thread = 0;

  ttwait_return = call_ttrace_wait (pid, TTRACE_WAITOK, &tsp, sizeof (tsp));
  if (ttwait_return < 0)
    {
      warning ("Call of ttrace_wait returned with errno %d.", errno);
      *status = ttwait_return;
      return inferior_pid;
    }

  real_pid = tsp.tts_pid;
  real_utid = (pthread_t) tsp.tts_user_tid;
  real_ktid = tsp.tts_lwpid;
  
  /* srikanth, 000726, We used to request thread creation, termination
     and exit notifications and process them here. The only use of these
     was to print a message [New Thread ...]. This gets to be annoying
     when the program creates several hundreds of threads. Now each time
     we stop the inferior with a PROC_STOP, we'll walk the thread list.
  */

  /* Attempt to translate the ttrace_wait-returned status into the
     ptrace equivalent.

     ??rehrauer: This is somewhat fragile.  We really ought to rewrite
     clients that expect to pick apart a ptrace wait status, to use
     something a little more abstract.
   */
  if ((tsp.tts_event & TTEVT_EXEC)
      || (tsp.tts_event & TTEVT_FORK)
      || (tsp.tts_event & TTEVT_VFORK))

    {
#ifdef HP_IA64
      /* We don't want to get any events from the shell other than exec.
         Turn off the trace bit in load_info so uld and dld won't call
         the breakpoint routine.
         */

      if (in_startup_starting_shell)
        {
          in_startup_starting_shell = FALSE;
          set_trace_bit (FALSE);
        }
#endif

      /* Make an exec or fork look like a breakpoint.  Definitely a hack,
         but I don't think non HP-UX-specific clients really carefully
         inspect the first events they get after inferior startup, so
         it probably almost doesn't matter what we claim this is.
       */

      *status = 0177 | (_SIGTRAP << 8);
    } 
#if TT_FEATURE_LEVEL >= 10
    else if (((tsp.tts_event & TTEVT_PREFORK)
              || (tsp.tts_event & TTEVT_FORK_FAIL) )
              && (feature_level>= 10))
     {
      /* Make an prefork or fork failure event look like a breakpoint*/
      *status = 0177 | (_SIGTRAP << 8);
     }
#endif

  /* Special-cases: We ask for syscall entry and exit events to implement
     "fast" (aka "hardware") watchpoints.

     When we get a syscall entry, we want to disable page-protections,
     and resume the inferior; this isn't an event we wish for
     wait_for_inferior to see.  Note that we must resume ONLY the
     thread that reported the syscall entry; we don't want to allow
     other threads to run with the page protections off, as they might
     then be able to write to watch memory without it being caught.

     When we get a syscall exit, we want to reenable page-protections,
     but we don't want to resume the inferior; this is an event we wish
     wait_for_inferior to see.  Make it look like the signal we normally
     get for a single-step completion.  This should cause wait_for_inferior
     to evaluate whether any watchpoint triggered.

     Or rather, that's what we'd LIKE to do for syscall exit; we can't,
     due to some HP-UX "features".  Some syscalls have problems with
     write-protections on some pages, and some syscalls seem to have
     pending writes to those pages at the time we're getting the return
     event.  So, we'll single-step the inferior to get out of the syscall,
     and then reenable protections.

     Note that we're intentionally allowing the syscall exit case to
     fall through into the succeeding cases, as sometimes we single-
     step out of one syscall only to immediately enter another...
   */
  else if ((tsp.tts_event & TTEVT_SYSCALL_ENTRY)
	   || (tsp.tts_event & TTEVT_SYSCALL_RETURN))
    {
      /* Make a syscall event look like a breakpoint.  Same comments
         as for exec & fork events.
       */
      *status = 0177 | (_SIGTRAP << 8);
    }
  else if (tsp.tts_event & TTEVT_UT_EXIT)
    {
      /* Remember the exited_thread here. We should know this to tell that
	 this thread is dead in child_thread_alive. */
      exited_thread = map_to_gdb_tid (real_pid, real_utid, real_ktid);
      just_exited_thread = exited_thread;
      *status = 0177 | (_SIGTRAP << 8);
    }

  else if ((tsp.tts_event & TTEVT_EXIT))
    {				/* WIFEXITED */
      *status = 0 | (tsp.tts_u.tts_exit.tts_exitcode);
    }

  else if (tsp.tts_event & TTEVT_SIGNAL)
    {				/* WIFSTOPPED */
      *status = 0177 | (tsp.tts_u.tts_signal.tts_signo << 8);
    }
#ifdef HP_IA64_NATIVE
  else if (tsp.tts_event & TTEVT_BPT_SSTEP && 
           tsp.tts_u.tts_bpt_sstep.tts_isbpt == TTBPT_DBPT)
    {
      *status = 0177 | SIGSEGV << 8;
    }
#endif
  else
    error ("Internal error ttrace_wait() returned unknown event");

  target_post_wait (tsp.tts_pid, *status);


  /* We have to report this event as having occurred on a particular
     thread. What thread id do we pass to gdb ? We used to expose
     the LWP id if it is a non-main thread and the pid if it is the
     main thread. This is the biggest bogusity ever as a secondary
     thread's LWP id could be equal to the pid. When that happens
     the outer layer loses all capability to distinguish the two
     threads. As a result, we now apply a special mapping function
     to the pid and user tid to guarantee uniqueness -- srikanth.
  */

  return_pid = map_to_gdb_tid (real_pid, real_utid, real_ktid);

  /* Remember this for later use in "hppa_prepare_to_proceed" and for
     "info threads" display of '>' for thread causing last event.
   */
  last_event_thread = reported_pid = return_pid;
  reported_bpt = ((tsp.tts_event & TTEVT_SIGNAL) && (5 == tsp.tts_u.tts_signal.tts_signo));

  return return_pid;
}


/* This function causes the caller's process to be traced by its
   parent.  This is intended to be called after GDB forks itself,
   and before the child execs the target.  Despite the name, it
   is called by the child.

   Note that HP-UX ttrace is rather funky in how this is done.
   If the parent wants to get the initial exec event of a child,
   it must set the ttrace event mask of the child to include execs.
   (The child cannot do this itself.)  This must be done after the
   child is forked, but before it execs.

   To coordinate the parent and child, we implement a semaphore using
   pipes.  After SETTRC'ing itself, the child tells the parent that
   it is now traceable by the parent, and waits for the parent's
   acknowledgement.  The parent can then set the child's event mask,
   and notify the child that it can now exec.

   (The acknowledgement by parent happens as a result of a call to
   child_acknowledge_created_inferior.)
 */
int
parent_attach_all ()
{
  int tt_status;

  /* We need a memory home for a constant, to pass it to ttrace.
     The value of the constant is arbitrary, so long as both
     parent and child use the same value.  Might as well use the
     "magic" constant provided by ttrace...
   */
  uint64_t tc_magic_child = TT_VERSION;
  uint64_t tc_magic_parent = 0;

  tt_status = call_real_ttrace (TT_PROC_SETTRC, 0, 0, 0, TT_VERSION, 0);

  if (tt_status < 0)
    return tt_status;

  /* Notify the parent that we're potentially ready to exec(). */
  write (startup_semaphore.child_channel[SEM_TALK],
	 &tc_magic_child,
	 sizeof (tc_magic_child));

  /* Wait for acknowledgement from the parent. */
  read (startup_semaphore.parent_channel[SEM_LISTEN],
	&tc_magic_parent,
	sizeof (tc_magic_parent));

  if (tc_magic_child != tc_magic_parent)
    warning ("mismatched semaphore magic");

  /* Discard our copy of the semaphore. */
  (void) close (startup_semaphore.parent_channel[SEM_LISTEN]);
  (void) close (startup_semaphore.parent_channel[SEM_TALK]);
  (void) close (startup_semaphore.child_channel[SEM_LISTEN]);
  (void) close (startup_semaphore.child_channel[SEM_TALK]);

  return tt_status;
}

/* require_notification_of_events : When it is called, a forked child
   is running, but waiting on the semaphore.  If you stop the child and
   re-start it, things get confused, so don't do that!  An attached
   child is stopped. Since this is called after either attach or run,
   we have to be the common part of both.
*/
void
require_notification_of_events (int real_pid)
{
  ttevent_t notifiable_events;
  extern int ignore_uninteresting_signals;
  int  nsigs;
  int signum;
  int status;

  int i;

  /* Temporary HACK: tell inftarg.c/child_wait to not
   * loop until pids are the same.
   */
  not_same_real_pid = 0;

  /* RM: ttrace bug causes inferior to continue when doing a step when the
     inferior has a pending signal. Route all signals through wdb for now. */
  /* srikanth, not sure if this defect exists anymore. Undoing this
     change as it interfers terribly with stepping in applications
     that generate a large number of ALARM signals.
  */
  /* Bindu 092106: Enabling this for IPF as I don't see the above mentioned
     problem anymore. */

  if (ignore_uninteresting_signals)
    {
      sigfillset (&notifiable_events.tte_signals);

      /* RM: Let's not bother with signals we don't care about */
      nsigs = (int)TARGET_SIGNAL_LAST;
      for (signum = nsigs - 1; signum >= 0; signum--)
        {
          if ((signal_stop_state (signum)) ||
              (signal_print_state (signum)) ||
              (!signal_pass_state (signum)))
            {
              if (target_signal_to_host_p (signum))
                sigdelset (&notifiable_events.tte_signals,
	    	       target_signal_to_host (signum));
            }
        }
    }
  else
    sigemptyset (&notifiable_events.tte_signals);

  notifiable_events.tte_opts = TTEO_NONE;

  /* This ensures that forked children inherit their parent's
   * event mask, which we're setting here.
   *
   * NOTE: if you debug gdb with itself, then the ultimate
   *       debuggee gets flags set by the outermost gdb, as
   *       a child of a child will still inherit.
   */
  notifiable_events.tte_opts |= TTEO_PROC_INHERIT;
  notifiable_events.tte_opts |= TTEO_NORM_SIGTRAP; //For JAGab68928

  notifiable_events.tte_events = TTEVT_DEFAULT;
#ifdef HP_IA64
  notifiable_events.tte_events |= TTEVT_BPT_SSTEP;
#endif
  notifiable_events.tte_events |= TTEVT_SIGNAL;
  notifiable_events.tte_events |= TTEVT_EXEC;
  notifiable_events.tte_events |= TTEVT_EXIT;
  notifiable_events.tte_events |= TTEVT_FORK;
  notifiable_events.tte_events |= TTEVT_VFORK;
  notifiable_events.tte_events |= TTEVT_SYSCALL_ENTRY;
  notifiable_events.tte_events |= TTEVT_SYSCALL_RETURN;

#if !(defined(HP_IA64)) // For JAGaf52708
  if (feature_level >= 7)
    notifiable_events.tte_events |= TTEVT_BPT_SSTEP;
#endif

  if (feature_level >= 10)
    {
      notifiable_events.tte_events |= TTEVT_PREFORK;
      notifiable_events.tte_events |= TTEVT_FORK_FAIL;
    }

 /* We ask for user thread exit events now. To detect the case when some

     We don't request thread creation, termination, exit events anymore.
     After we stop the process we will iterate over the system's
     thread list and adopt newborns and bury dead ones -- srikanth
  */

  errno = 0;
  call_real_ttrace (TT_PROC_SET_EVENT_MASK,
                    real_pid,
                    0,
                    (uint64_t) &notifiable_events,
                    sizeof (notifiable_events),
                    0);
  if (errno)
    perror_with_name ("ttrace");

if (sigwait_sig_in_init)
{
  /* Always ask for syscall entry and return for sigwait, sigtimedwait and
     sigwaitinfo. */
  set_safe_syscall_bm (0);

  status = call_ttrace (TT_PROC_SET_SCBM,
               real_pid,
               TTSCBM_UNSELECT,
               sizeof (ttbm_t) * NUM_BYTES_IN_SCBM,
               (uint64_t) safe_syscall_bm);
} else {
  /* clear all */
  if (!safe_syscall_bm)
    {
      safe_syscall_bm = (ttbm_t*) xmalloc (sizeof(ttbm_t) * NUM_BYTES_IN_SCBM);
    }
  memset (safe_syscall_bm, 0xffffffff, sizeof (ttbm_t) * NUM_BYTES_IN_SCBM );
  status = call_ttrace (TT_PROC_SET_SCBM,
               real_pid,
               TTSCBM_UNSELECT,
	       sizeof (ttbm_t) * NUM_BYTES_IN_SCBM,
               (uint64_t) safe_syscall_bm);
}

  if (status <0 && errno)
    perror_with_name ("ttrace");
}

void
initiate_sigwait_signals ()
{
  int status;
  /* Always ask for syscall entry and return for sigwait, sigtimedwait and
     sigwaitinfo. */
  set_safe_syscall_bm (0);

  status = call_ttrace (TT_PROC_SET_SCBM,
               inferior_pid,
               TTSCBM_UNSELECT,
               sizeof (ttbm_t) * NUM_BYTES_IN_SCBM,
               (uint64_t) safe_syscall_bm);
  if (status <0 && errno)
    perror_with_name ("ttrace");
}

static void
require_notification_of_exec_events (int real_pid)
{
  ttevent_t notifiable_events;

  /* Temporary HACK: tell inftarg.c/child_wait to not
   * loop until pids are the same.
   */
  not_same_real_pid = 0;

  sigemptyset (&notifiable_events.tte_signals);
  notifiable_events.tte_opts = TTEO_NOSTRCCHLD;

  /* This ensures that forked children don't inherit their parent's
   * event mask, which we're setting here.
   */
  notifiable_events.tte_opts &= ~TTEO_PROC_INHERIT;

  notifiable_events.tte_events = TTEVT_DEFAULT;
  notifiable_events.tte_events |= TTEVT_EXEC;
  notifiable_events.tte_events |= TTEVT_EXIT;

  call_real_ttrace (TT_PROC_SET_EVENT_MASK,
                    real_pid,
                    0,
                    (uint64_t) &notifiable_events,
                    sizeof (notifiable_events),
                    0);
}

/* This function is called by the parent process, with pid being the
 * ID of the child process, after the debugger has forked.
 */
void
child_acknowledge_created_inferior (int pid)
{
  /* We need a memory home for a constant, to pass it to ttrace.
     The value of the constant is arbitrary, so long as both
     parent and child use the same value.  Might as well use the
     "magic" constant provided by ttrace...
   */
  uint64_t tc_magic_parent = TT_VERSION;
  uint64_t tc_magic_child = 0;

  /* Wait for the child to tell us that it has forked. */
  read (startup_semaphore.child_channel[SEM_LISTEN],
	&tc_magic_child,
	sizeof (tc_magic_child));

  /* Clear thread info now.  We'd like to do this in
   * "require...", but that messes up attach.
   */
  clear_thread_info ();

  /* Tell the "rest of gdb" that the initial thread exists.
   *
   * Q: Why don't we also add this thread to the local
   *    list via "create_thread_info"?
   *
   * A: Because we don't know the tid, and can't stop the
   *    the process safely to ask what it is.  Anyway, we'll
   *    add it when it gets the EXEC event.
   */
  add_thread (pid);

  /* We can now set the child's ttrace event mask.
   */
  require_notification_of_exec_events (pid);

  /* Notify the child that it can exec. */
  write (startup_semaphore.parent_channel[SEM_TALK],
	 &tc_magic_parent,
	 sizeof (tc_magic_parent));

  /* Discard our copy of the semaphore. */
  (void) close (startup_semaphore.parent_channel[SEM_LISTEN]);
  (void) close (startup_semaphore.parent_channel[SEM_TALK]);
  (void) close (startup_semaphore.child_channel[SEM_LISTEN]);
  (void) close (startup_semaphore.child_channel[SEM_TALK]);
}


/*
 * arrange for notification of all events by
 * calling require_notification_of_events.
 */
void
child_post_startup_inferior (int real_pid)
{
  require_notification_of_events (real_pid);
}


int
child_insert_fork_catchpoint (int tid)
{
  /* Enable reporting of fork events from the kernel. */
  /* ??rehrauer: For the moment, we're always enabling these events,
     and just ignoring them if there's no catchpoint to catch them.
   */
  return 0;
}


int
child_remove_fork_catchpoint (int tid)
{
  /* Disable reporting of fork events from the kernel. */
  /* ??rehrauer: For the moment, we're always enabling these events,
     and just ignoring them if there's no catchpoint to catch them.
   */
  return 0;
}


int
child_insert_vfork_catchpoint (int tid)
{
  /* Enable reporting of vfork events from the kernel. */
  /* ??rehrauer: For the moment, we're always enabling these events,
     and just ignoring them if there's no catchpoint to catch them.
   */
  return 0;
}


int
child_remove_vfork_catchpoint (int tid)
{
  /* Disable reporting of vfork events from the kernel. */
  /* ??rehrauer: For the moment, we're always enabling these events,
     and just ignoring them if there's no catchpoint to catch them.
   */
  return 0;
}


int
child_has_forked (int tid, int *childpid)
{
  thread_info *tinfo;
  thread_info *p;
  boolean seen_pid = 0;

  tinfo = find_thread_info (map_from_gdb_tid (tid));

  /* Set childpid to the corresponding gdb_tid if found in the thread list. */
  if (tinfo->last_stop_state.tts_event & TTEVT_FORK) 
    {
      for (p = thread_list; p; p = p->next)
	{
	  if (p->pid == tinfo->last_stop_state.tts_u.tts_fork.tts_fpid)
	    {
	      seen_pid = 1;
	      if (p->ktid == tinfo->last_stop_state.tts_u.tts_fork.tts_flwpid)
		{
      	          *childpid = p->gdb_tid;
      	          return 1;
		}
	    }
	}

     
      if (seen_pid)
	{
	  /* Thread information may have changed for forking pid. Stop all
	     the threads collect, the new thread information and try again. */
          stop_all_threads_of_process (tinfo->last_stop_state.tts_u.tts_fork.tts_fpid);
          /* try again */
          for (p = thread_list; p; p = p->next)
           {
             if (   (p->pid == tinfo->last_stop_state.tts_u.tts_fork.tts_fpid)
                 && (p->ktid == tinfo->last_stop_state.tts_u.tts_fork.tts_flwpid))
               {
                 *childpid = p->gdb_tid;
                 return 1;
               }
           }
	}
     /* We get here when we see a parent fork before child fork. */
     *childpid = tinfo->last_stop_state.tts_u.tts_fork.tts_fpid;
     return 1;
    }

  return 0;
}

/* Returns the signal, or 0 on error */
int
signal_sigwait (tid_t tid)
{
  thread_info *tinfo;
  int signal;
  tinfo = find_thread_info (map_from_gdb_tid (tid));

  if (tinfo->last_stop_state.tts_event & TTEVT_SYSCALL_RETURN)
    {
      switch (tinfo->last_stop_state.tts_scno)
        {
	  case 428: /* sigwaitinfo */
	  case 429: /* sigtimedwait */
	    if (    (tinfo->last_stop_state.tts_u.tts_syscall.tts_rval[0] == -1)
		 && (tinfo->last_stop_state.tts_u.tts_syscall.tts_errno))
	      break;
	    else
	      return (int) tinfo->last_stop_state.tts_u.tts_syscall.tts_rval[0];
	  case 430: /* sigwait */
	    if (!tinfo->last_stop_state.tts_u.tts_syscall.tts_rval[0])
	      {
		target_read_memory (tinfo->last_stop_state.tts_scarg[1],
				    (char*) &signal, sizeof (int));
		return signal;
	      }
	    break;
	  default:
	    break;
	}
    }
  return 0;
}

int
child_has_vforked (int tid, int *childpid)
{
  thread_info *tinfo;

  tinfo = find_thread_info (map_from_gdb_tid (tid));

  if (tinfo->last_stop_state.tts_event & TTEVT_VFORK)
    {
      *childpid = tinfo->last_stop_state.tts_u.tts_fork.tts_fpid;
#if TT_FEATURE_LEVEL >= 10
        extern char* follow_fork_mode_string;
	if (!tinfo->last_stop_state.tts_u.tts_fork.tts_isparent
             && (feature_level>= 10)) 
          {   
            if (strcmp(follow_fork_mode_string ,"child") == 0)
              {
		/* If we are following the child and if we have got
		   a child vfork, we will wait for any events from
		   any pid & tid*/
		reset_prefork_wait ();
              }
            else 
             {
		/* If we are following the parent and if we have
	           got child vfork, wait for the parent vfork */
                wait_on_prefork_pid = *childpid;
                wait_on_prefork_tid = tinfo->last_stop_state.tts_u.tts_fork.tts_flwpid;
	     }
	  }
#endif
      return 1;
    }

  return 0;
}


int
process_has_vforks_prefork (int tid, int *childpid)
{
#if TT_FEATURE_LEVEL >= 10
  thread_info *tinfo;

  tinfo = find_thread_info (map_from_gdb_tid (tid));

  if ( (tinfo->last_stop_state.tts_event & TTEVT_PREFORK)
      && (tinfo->last_stop_state.tts_u.tts_prefork.tts_type == TTS_VFORK) )
    {
      /* Once we get the prefork event, we will wait for the
         child vfork to happen (using wait_on_prefork_pid & wait_on_prefork_tid)*/
      wait_on_prefork_pid = tinfo->last_stop_state.tts_u.tts_prefork.tts_fpid;
      wait_on_prefork_tid = tinfo->last_stop_state.tts_u.tts_prefork.tts_flwpid;
      is_vfork = 1;
      return 1;
    }

#endif
  return 0;
}


int
process_has_forks_prefork (int tid, int *childpid)
{
#if TT_FEATURE_LEVEL >= 10
  thread_info *tinfo;

  tinfo = find_thread_info (map_from_gdb_tid (tid));

  if ( (tinfo->last_stop_state.tts_event & TTEVT_PREFORK)
       && 
       (tinfo->last_stop_state.tts_u.tts_prefork.tts_type == TTS_FORK) )
    {
     /* Reset both flags to wait for any events from any threads or process */
     /*  wait_on_prefork_pid = wait_on_prefork_tid = 0; */
      return 1;
    }

#endif
  return 0;
}

int
process_has_failed_prefork (int tid, int *childpid)
{
#if TT_FEATURE_LEVEL >= 10
  thread_info *tinfo;

  tinfo = find_thread_info (map_from_gdb_tid (tid));

  if (tinfo->last_stop_state.tts_event & TTEVT_FORK_FAIL)
    {
       /* We got a FAIL vfork event. Let us wait for any 
          events from the ttrace queue */
	  reset_prefork_wait ();
          printf_filtered (" We got a failed vfork event \n");
          return 1;
    }
#endif
  return 0;
}

int
child_can_follow_vfork_prior_to_exec ()
{
  /* ttrace does allow this.

     ??rehrauer: However, I had major-league problems trying to
     convince wait_for_inferior to handle that case.  Perhaps when
     it is rewritten to grok multiple processes in an explicit way...
   */
  return 0;
}


int
child_insert_exec_catchpoint (int tid)
{
  /* Enable reporting of exec events from the kernel. */
  /* ??rehrauer: For the moment, we're always enabling these events,
     and just ignoring them if there's no catchpoint to catch them.
   */
  return 0;
}


int
child_remove_exec_catchpoint (int tid)
{
  /* Disable reporting of execevents from the kernel. */
  /* ??rehrauer: For the moment, we're always enabling these events,
     and just ignoring them if there's no catchpoint to catch them.
   */
  return 0;
}


int
child_has_execd (int tid, char **execd_pathname)
{
  thread_info *tinfo;

  tinfo = find_thread_info (map_from_gdb_tid (tid));

  if (tinfo->last_stop_state.tts_event & TTEVT_EXEC)
    {
      char *exec_file = target_pid_to_exec_file (tid);
      *execd_pathname = savestring (exec_file, (int) strlen (exec_file));
      return 1;
    }

  return 0;
}


/* JAGaf54650: gdb needs pre exit event to detach from the child
   and attach to the parent for serial debugging. 
 */
int
child_pre_exit (int pid)
{
  thread_info *tinfo;
  ttstate_t ttrace_state;

  tinfo = find_thread_info (map_from_gdb_tid (pid));
  ttrace_state = tinfo->last_stop_state;
  if (ttrace_state.tts_event & TTEVT_EXIT)
    {
      return 1;
    }
  return 0; 
}

int
child_has_syscall_event (int pid, enum target_waitkind * kind,
                                               int *syscall_id)
{
  ttstate_t ttrace_state;
  thread_info *tinfo;

  tinfo = find_thread_info (map_from_gdb_tid (pid));
  ttrace_state = tinfo->last_stop_state;

  *kind = TARGET_WAITKIND_SPURIOUS;	/* Until proven otherwise... */
  *syscall_id = -1;

  if (ttrace_state.tts_event & TTEVT_SYSCALL_ENTRY)
    {
      /* RM: If there are protected pages, we need to pass this event
         up to wait_for_inferior() */      
      /* Bindu: If this is enabled from data breakpoint stuff, we need to
	 pass this on to wait_for_inferior. 
	 If these are sigwait type syscalls, we need to pass them on as well. */
      if (   syscall_enabled_from_data_break
	  || (memory_page_dictionary.page_count > (LONGEST) 0)
	  || (ttrace_state.tts_scno == 428) /* sigwaitinfo */
	  || (ttrace_state.tts_scno == 429) /* sigtimedwait */
	  || (ttrace_state.tts_scno == 430) /* sigwait */
	 )
        {
	  *kind = TARGET_WAITKIND_SYSCALL_ENTRY;
          *syscall_id = ttrace_state.tts_scno;
        }
      return 1;
    }

  if (ttrace_state.tts_event & TTEVT_SYSCALL_RETURN) 
    {
      /* RM: If there are protected pages, we need to pass this event
         up to wait_for_inferior() */      
      /* Bindu: If this is enabled from data breakpoint stuff, we need to
         pass this on to wait_for_inferior. 
	 If these are sigwait type syscalls, we need to pass them on as well.*/
      if (   syscall_enabled_from_data_break
	  || (memory_page_dictionary.page_count > (LONGEST) 0)
	  || (ttrace_state.tts_scno == 428) /* sigwaitinfo */
          || (ttrace_state.tts_scno == 429) /* sigtimedwait */
          || (ttrace_state.tts_scno == 430) /* sigwait */
	 )
        {
	  *kind = TARGET_WAITKIND_SYSCALL_RETURN;
          *syscall_id = ttrace_state.tts_scno;
        }
      return 1;
    }

  return 0;
}

/* Has this thread just reported the TTEVT_UT_EXIT event? */
int
child_thread_just_exited (tid_t gdb_tid)
{
  if (gdb_tid == just_exited_thread)
    return 1;
  else
    return 0;
}

/* Returns 1 if the thread is alive. */
int
child_thread_alive (tid_t gdb_tid)
{
  thread_info *p;

  for (p = thread_list; p; p = p->next)
    if (p->gdb_tid == gdb_tid)
      return 1;

  return 0;
}



/* This function attempts to read the specified number of bytes from the
   save_state_t that is our view into the hardware registers, starting at
   ss_offset, and ending at ss_offset + sizeof_buf - 1

   If this function succeeds, it deposits the fetched bytes into buf,
   and returns 0.

   If it fails, it returns a negative result.  The contents of buf are
   undefined it this function fails.
 */
int
read_from_register_save_state (int tid, uint64_t ss_offset,
                                       void * buf, int sizeof_buf)
{
  int tt_status;

  /* begin purify */
  memset (buf, 0, sizeof_buf);
  /* end purify */
  tt_status = call_ttrace (TT_LWP_RUREGS,
			   tid,
			   ss_offset,
			   sizeof_buf,
			   (uint64_t) buf);

  if (tt_status == 1)
    /* Map ttrace's version of success to our version.
     * Sometime ttrace returns 0, but that's ok here.
     */
    return 0;

  return tt_status;
}


/* This function attempts to write the specified number of bytes to the
   save_state_t that is our view into the hardware registers, starting
   at offset, and ending at ss_offset + size - 1

   On success, it deposits the bytes in buf, and returns 0. Otherwise
   it returns a negative result.  The contents of the save_state_t
   are undefined it this function fails.
 */
int
write_to_register_save_state (int tid, uint64_t offset,
                                       void *buf, int size)
{
  return call_ttrace (TT_LWP_WUREGS, tid, offset, size, (uint64_t) buf);
}


/* This function is a sop to the largeish number of direct calls
   to call_ptrace that exist in other files.  Rather than create
   functions whose name abstracts away from ptrace, and change all
   the present callers of call_ptrace, we'll do the expedient (and
   perhaps only practical) thing.

   Note HP-UX explicitly disallows a mix of ptrace & ttrace on a traced
   process.  Thus, we must translate all ptrace requests into their
   process-specific, ttrace equivalents.
 */
int
call_ptrace (int pt_request, int gdb_tid, PTRACE_ARG3_TYPE addr,
                                                       int data)
{
  ttreq_t tt_request;
  uint64_t tt_addr = (uint64_t) addr;
  uint64_t tt_data = (uint64_t)(long) data;
  uint64_t tt_addr2 = 0;
  int tt_status;
  int register_value;
  int read_buf = 0;

  /* Perform the necessary argument translation.  Note that some
     cases are funky enough in the ttrace realm that we handle them
     very specially.
   */
  switch (pt_request)
    {
      /* The following cases cannot conveniently be handled conveniently
         by merely adjusting the ptrace arguments and feeding into the
         generic call to ttrace at the bottom of this function.

         Note that because all branches of this switch end in "return",
         there's no need for any "break" statements.
       */
    case PT_SETTRC:
      return parent_attach_all ();

    case PT_RUREGS:
      tt_status = read_from_register_save_state (gdb_tid,
						 tt_addr,
						 &register_value,
						 sizeof (register_value));
      if (tt_status < 0)
	return tt_status;
      return register_value;

    case PT_WUREGS:
      register_value = (int) tt_data;
      tt_status = write_to_register_save_state (gdb_tid,
						tt_addr,
						&register_value,
						sizeof (register_value));
      return tt_status;

    case PT_READ_I:
      tt_status = call_ttrace (TT_PROC_RDTEXT,
			       gdb_tid,
			       tt_addr,
			       4,
			       (uint64_t) &read_buf);
      if (tt_status < 0)
	return tt_status;
      return read_buf;

    case PT_READ_D:
      tt_status = call_ttrace (TT_PROC_RDDATA,
			       gdb_tid,
			       tt_addr,
			       4,
			       (uint64_t) &read_buf);
      if (tt_status < 0)
	return tt_status;
      return read_buf;

    case PT_ATTACH:
      tt_status = call_real_ttrace (TT_PROC_ATTACH,
				    map_from_gdb_tid (gdb_tid),
				    0,
				    tt_addr,
				    TT_VERSION,
				    tt_addr2);
      return tt_status;

      /* The following cases are handled by merely adjusting the ptrace
         arguments and feeding into the generic call to ttrace.
       */
    case PT_DETACH:
      tt_request = TT_PROC_DETACH;
      break;

    case PT_WRITE_I:
      tt_request = TT_PROC_WRTEXT;
      tt_data = 4;		/* This many bytes. */
      tt_addr2 = (uint64_t) &data;	/* Address of xfer source. */
      break;

    case PT_WRITE_D:
      tt_request = TT_PROC_WRDATA;
      tt_data = 4;		/* This many bytes. */
      tt_addr2 = (uint64_t) &data;	/* Address of xfer source. */
      break;

    case PT_RDTEXT:
      tt_request = TT_PROC_RDTEXT;
      break;

    case PT_RDDATA:
      tt_request = TT_PROC_RDDATA;
      break;

    case PT_WRTEXT:
      tt_request = TT_PROC_WRTEXT;
      break;

    case PT_WRDATA:
      tt_request = TT_PROC_WRDATA;
      break;

    case PT_CONTINUE:
      tt_request = TT_PROC_CONTINUE;
      break;

    case PT_STEP:
      tt_request = TT_LWP_SINGLE;	/* Should not be making this request? */
      break;

    case PT_KILL:
      tt_request = TT_PROC_EXIT;
      break;

    case PT_GET_PROCESS_PATHNAME:
      tt_request = TT_PROC_GET_PATHNAME;
      break;

    default:
      tt_request = pt_request;	/* Let ttrace be the one to complain. */
      break;
    }

  return call_ttrace (tt_request, gdb_tid, tt_addr, tt_data, tt_addr2);
}

/*
 * Bindu: Provide GDB a mechanism to use five argument ptrace to read/write
 * big chunks of memory in one go.
 */
#ifdef FIVE_ARG_PTRACE
int
call_five_arg_ptrace (int pt_request, int gdb_tid, PTRACE_ARG3_TYPE addr,
                                         int data, PTRACE_ARG3_TYPE addr2)
{
  ttreq_t tt_request;
  uint64_t tt_addr = (uint64_t) addr;
  uint64_t tt_data = (uint64_t)(long) data;
  uint64_t tt_addr2 = (uint64_t) addr2;
  int tt_status;

  switch (pt_request)
    {
      case PT_RDTEXT:
        tt_request = TT_PROC_RDTEXT;
        break;

      case PT_RDDATA:
        tt_request = TT_PROC_RDDATA;
        break;

      case PT_WRTEXT:
        tt_request = TT_PROC_WRTEXT;
        break;

      case PT_WRDATA:
        tt_request = TT_PROC_WRDATA;
        break;

      case PT_RUREGS:
        return read_from_register_save_state (gdb_tid,
                                              tt_addr,
                                              (void *) (unsigned long) tt_addr2,
                                              (int) tt_data);
      default:
        return call_ptrace(pt_request, gdb_tid, addr, data);
    }
  return call_ttrace (tt_request, gdb_tid, tt_addr, tt_data, tt_addr2);
}
#endif

/* Cleanup all the mxn structures. */
void
clear_mxn_info ()
{
  mxn_debug_cleanup (); /* The fat old lady has sung, folks ! */
  is_mxn = 0;
  mxn_unsafe_lwp_address = 0;
  mxn_safe_address = 0;
  mxn_safe_fork_address = 0;
  symbol_table_load_hook = some_symbol_table_got_loaded;
  enabled_ut_events = 0;
}

/* Kill that pesky process!
 */
void
kill_inferior ()
{
  thread_info * t;
  int integer;

  if (inferior_pid == 0)
    return;

  /* srikanth, 000804, First get rid of current process. Otherwise,
     we will detach MT programs instead of forcing them to exit, if
     inferior_pid is not a true process id.
  */
      
  call_ttrace (TT_PROC_EXIT, inferior_pid, 0, 0, 0);

  /* srikanth, 001114, PROC_EXIT turns the process into a zombie. The
     parent has to harvest by retrieving the exit status. Otherwise,
     the process hangs around and causes subsequent compiles to fail
     with a "Text file busy" message.
  */
  wait (&integer);

  discard_threads_of (get_pid_for (inferior_pid));

  target_mourn_inferior ();

  /* We may be left with a forked child, if kill was issued at fork
    catch point, if so detach it ... 
  */

  t = thread_list;
  if (t)
    {
      call_ttrace (TT_PROC_STOP, t->pid, 0, 0, 0);
      call_ttrace (TT_PROC_DETACH, t->pid, 0, TARGET_SIGNAL_0, 0);
    }

  clear_thread_info ();
  clear_mxn_info ();
  exited_thread = 0;
  just_exited_thread = 0;

  if (java_debugging)
  	cleanup_java_lib();
}



/* Use a loop over the threads to continue all the threads but
 * the one specified, which is to be stepped.
 */
static void
threads_continue_all_but_one (tid_t gdb_tid, int signal)
{
  thread_info *p;
  int thread_signal;
  tid_t real_tid;
  int real_pid;

  /* Let the user enable atleast one thread before proceeding. */
  int continue_atleast_one = 0;
  int debug_trace = 0;

  real_tid = map_from_gdb_tid (gdb_tid);
  real_pid = get_pid_for (real_tid);

  if (debug_trace)
    printf ("Exited thread is %x\n", exited_thread);

  /* srikanth, 000725, we used to iterate over the threads
     and add new ones to the thread list here. I deleted that code
     as it is useless : a stopped process cannot create threads and
     whatever it created while running are already in our list.
  */

  for (p = thread_list; p; p = p->next)
    {
      thread_signal = 0;

      if (p->pid != real_pid)
        continue;

      if (p->ktid == 0) /* has no kernel thread associated with it */
        continue;
 
      continue_atleast_one++;
      if (p->gdb_tid != real_tid)
	{
          /* If this thread has a pending event, don't resume it.
             The event will be debuffered by the next call to
             ttrace_wait ().
          */
          if (p->last_stop_state.tts_event != TTEVT_NONE &&
              !(p->last_stop_state.tts_flags & TTS_WAITEDFOR))
            continue;
          
         /* Do not continue the disabled thread. */ 
	  if (is_disabled_pid (p->gdb_tid))
	    {
	      continue_atleast_one--;
	      continue;
	    }

         /* If this thread was reported to us as just exited,
	    do not count this. */
         if (exited_thread && p->gdb_tid == exited_thread)
	   continue_atleast_one--;

	  call_ttrace ( TT_LWP_CONTINUE,
	                p->gdb_tid,
                        TT_NOPC,
	                target_signal_to_host (thread_signal),
	                0);
	  if (debug_trace)
	    printf ("continuing thread %x\n", p->gdb_tid);
	}
      else
	{
          /* User attempt to step an unhandled thread is not an error.
             (as opposed to internal gdb steps, which is a sign of
             trouble with outer layers of gdb. See thread_fake_step())
             Simply ignore step. The impending call to ttrace_wait
             will debuffer the event anyway.
          */

          if (p->last_stop_state.tts_event != TTEVT_NONE &&
              !(p->last_stop_state.tts_flags & TTS_WAITEDFOR))
            continue;

          /* Do not continue the disabled thread. */
	  if (is_disabled_pid (p->gdb_tid))
	    {
	      continue_atleast_one--;
	      continue;
	    }

	  /* If this thread was reported to us as just exited,
             do not count this. */
	  if (exited_thread && p->gdb_tid == exited_thread)
	    continue_atleast_one--;

          call_ttrace ( TT_LWP_SINGLE,
	                real_tid,
		        TT_NOPC,
		        target_signal_to_host (signal),
		        0);
	  if (debug_trace)
	    printf ("continuing thread %x\n", p->gdb_tid);
	}
    }
  if (!continue_atleast_one)
    error ("All threads are disabled. Enable atleast one thread.\n");
}

/* Use a loop over the threads to continue all the threads.
 * This is done when a signal must be sent to any of the threads.
 */
static void
threads_continue_all_with_signals (tid_t gdb_tid, int signal)
{
  thread_info *p;
  int thread_signal;
  tid_t real_tid;
  int real_pid;
  int debug_trace = 0;
  struct timespec     interval, remainder;

   /* Let the user enable atleast one thread before proceeding. */
  int continue_atleast_one = 0;

  real_tid = map_from_gdb_tid (gdb_tid);
  real_pid = get_pid_for (real_tid);
  interval.tv_sec = 0;
  interval.tv_nsec = WDB_TIMER_FOR_PREFORKS;

  if (debug_trace)
      printf ("Exited thread is %x\n", exited_thread);

  /* srikanth, 000725, we used to iterate over the threads
     and add new ones to the thread list here. I deleted that code
     as it is useless : a stopped process cannot create threads and
     whatever it created while running are already in our list.
  */

  for (p = thread_list; p; p = p->next)
    {
      if (p->pid != real_pid)
        continue;

      if (p->ktid == 0)   /* no kernel thread associated with it */
        continue;
       
      continue_atleast_one++;
      if (p->last_stop_state.tts_event != TTEVT_NONE &&
            !(p->last_stop_state.tts_flags & TTS_WAITEDFOR))
        continue;
 
      /* Do not continue the disabled thread. */
      if (is_disabled_pid (p->gdb_tid))
        {
	  continue_atleast_one--;
	  continue;
        }
          
       /* If this thread was reported to us as just exited,
          do not count this. */
       if (exited_thread && p->gdb_tid == exited_thread)
          continue_atleast_one--;

      /* Pass the correct signals along.  */

      if (p->gdb_tid == real_tid)
        thread_signal = signal;
      else
	thread_signal = 0;

      call_ttrace ( TT_LWP_CONTINUE,
                    p->gdb_tid,
                    TT_NOPC,
                    target_signal_to_host (thread_signal),
                    0);
      if (debug_trace)
	    printf ("continuing thread %11x\n", p->gdb_tid);

#if TT_FEATURE_LEVEL >= 10
     /* If we had multiple preforks in the queue and if we
        are going to continue each thread, give a nanosleep to prevent deadlock*/
        if (multi_threaded_preforks && (feature_level>= 10))
           (void) nanosleep(&interval, &remainder); 
#endif
    }
  if (!continue_atleast_one)
    error ("All the threads are disabled. Enable atleast one thread.\n");

}

/* Step one thread only.  
 */
static void
thread_fake_step (tid_t gdb_tid, enum target_signal signal)
{
  thread_info *p;
  tid_t real_tid;

  real_tid = map_from_gdb_tid (gdb_tid);
  p = find_thread_info (real_tid);

  if (p->ktid == 0)
    error ("Single step thread with no kernel thread associated with it!\n");

  /* When doing a single step, don't let this thread to be unbound.
     So, don't let the pthread library receive SIGLWPTIMER. */
  if (p->last_stop_state.tts_event == TTEVT_NONE ||
        p->last_stop_state.tts_flags & TTS_WAITEDFOR)
    {
      ttmask_t mask;

#ifdef SIGLWPTIMER
      sigemptyset (&mask.ttm_signals);
      sigaddset (&mask.ttm_signals, SIGLWPTIMER);
      mask.ttm_opts = TTMO_NONE;
      call_ttrace (TT_LWP_SET_SIGMASK, gdb_tid, (uint64_t) &mask,
               sizeof (mask), 0);
#endif
      call_ttrace (TT_LWP_SINGLE,
                   gdb_tid,
                   TT_NOPC,
                   target_signal_to_host (signal),
                   0);
   }

  /* Let call_ttrace_wait know it has to wait for this thread only. */

  wait_on_tid = gdb_tid;
}  /* End thread_fake_step */

/* Continue one thread when a signal must be sent to it.
 */
static void
threads_continue_one_with_signal (tid_t gdb_tid, int signal)
{
  thread_info *p;
  tid_t real_tid;

  real_tid = map_from_gdb_tid (gdb_tid);
  p = find_thread_info (real_tid);

  if (p->ktid == 0)
    error ("Continue thread with no kernel thread associated with it!\n");

  if (is_disabled_pid (p->gdb_tid))
    return;

  if (p->last_stop_state.tts_event == TTEVT_NONE ||
        p->last_stop_state.tts_flags & TTS_WAITEDFOR)
    call_ttrace (TT_LWP_CONTINUE,
                 gdb_tid,
                 TT_NOPC,
                 target_signal_to_host (signal),
	         0);
}


/* Resume execution of the inferior process.

 *   If STEP is zero,      continue it.
 *   If STEP is nonzero,   single-step it.
 *   
 *   If SIGNAL is nonzero, give it that signal.
 *
 *   If TID is -1,         apply to all threads.
 *   If TID is not -1,     apply to specified thread.
 *   
 *           STEP
 *      \      !0                        0
 *  TID  \________________________________________________
 *       |
 *   -1  |   Step current            Continue all threads
 *       |   thread and              (but which gets any
 *       |   continue others         signal?--We look at
 *       |                           "inferior_pid")
 *       |
 *    N  |   Step _this_ thread      Continue _this_ thread
 *       |   and leave others        and leave others 
 *       |   stopped; internally     stopped; used only for
 *       |   used by gdb, never      hardware watchpoints
 *       |   a user command.         and attach, never a
 *       |                           user command.
 */
void
child_resume (tid_t gdb_tid, int step, enum target_signal signal)
{
  int resume_all_threads;
  tid_t tid;
  extern int rtc_in_progress;


  if (java_debugging)
        cleanup_java_lib ();

  resume_all_threads =
    (gdb_tid == -1) ||
    (vfork_in_flight);
  /* srikanth, 000224, If garbage collection is in progress, continue
     only the collecting thread and keep others stopped. Otherwise, we
     could get incorrect results. The leak detector tries to acquire a
     mutex before proceeding : this would block the other threads if
     they try to allocate or deallocate. However this is not enough.

     There are pathological cases where we would report spurious leaks
     if other threads are running. Think of a case where the only
     pointer to a block is in an as yet unscanned region and it is
     being transferred to an already scanned region even as the
     mark and sweep is proceeding.

     Furthermore, allowing the other threads to continue, really
     messes up things when they hit a breakpoint or otherwise cause
     an event to be generated.
  */

#ifdef RTC
  if (rtc_in_progress)
    {
      resume_all_threads = 0;
      tid = map_from_gdb_tid (inferior_pid);
      wait_on_tid = tid;
    }
  else 
#endif
  if (resume_all_threads)
    {
      /* Resume all threads, but first pick a tid value
       * so we can get the pid when in call_ttrace doing
       * the map.
       */
      if (vfork_in_flight)
	tid = vforking_child_pid;
      else
	tid = map_from_gdb_tid (inferior_pid);
    }
  else
    tid = map_from_gdb_tid (gdb_tid);
#if TT_FEATURE_LEVEL >= 10
   if ((no_of_preforks >= 1) && (feature_level>= 10)) 
     {
       /* We got a PARENT_VFORK. Still, we have preforks
	 events pending in the queue, so don't resume
	 all threads now. Continue only the reported pid */
         resume_all_threads = 0;
         if (!vfork_in_flight) 
	    tid = reported_pid; 
     }
    if (!vfork_in_flight && (feature_level>= 10))
      reset_prefork_wait ();
#endif
  if (step)
    {
      if (resume_all_threads)
        threads_continue_all_but_one (tid, signal);
      else
	{
	  /* "Fake step": gdb is stepping one thread over a
	   * breakpoint, watchpoint, or out of a library load
	   * event, etc.  The rest just stay where they are.
	   *
	   * Also used when there are pending events: we really
	   * step the current thread, but leave the rest stopped.
	   * Users can't request this, but "wait_for_inferior"
	   * does--a lot!
	   */
	  thread_fake_step (tid, signal);
	}
    }
  else
    {
       if (resume_all_threads)
        threads_continue_all_with_signals (tid, signal);
      else
        threads_continue_one_with_signal (tid, signal);
    }
}

/* bindu 101601: Get the thread state.
   */
void
update_thread_state (int pid)
{
  ttstate_t thread_state;
  composite_tid_t tid;

  clear_thread_info ();

  for (tid = get_process_first_stopped_thread_id (pid, &thread_state);
       tid.gdb_tid != 0;
       tid = get_process_next_stopped_thread_id (pid, &thread_state))
    {
      thread_info *p;


      p = create_thread_info (pid, tid);
      p->last_stop_state = thread_state;

      add_thread (tid.gdb_tid);
    }
}

static void
update_thread_state_after_attach (int pid)
{
  ttstate_t thread_state;
  composite_tid_t tid;

  /* srikanth, we used to select a thread and continue it with SIGTRAP.
     This is because, ttrace_wait should not be called after an
     attach, while the upper layers of gdb used to want to wait
     after an attach. This approach was frought with peril as, if
     SIGTRAPs are disabled in the process, it will fall flat.
     See JAGaa86215. Now the upper layer has been changed not to wait.
  */

  attach_flag = 1;
  not_same_real_pid = 0;
  
  update_thread_state (pid);
}

#define ATTACH_NFS_ERR_MSG "\
\nAttaching to process %d failed.\n\
Hint: Check if this process is already being traced by another gdb or \n\
other ttrace tools like caliper and tusc.\n\
Hint: Check whether program is on an NFS-mounted file-system. If so, you \n\
will need to mount the file system with the \"nointr\" option with \n\
mount(1) or make a local copy of the program to resolve this problem.\n"

/* TT_PROC_ATTACH This request allows the calling process to trace the
   process identified by pid.  The process pid does not have to be a child 
   of the calling process, but the effective user ID of the calling process 
   must match the real and saved uid of the process pid unless the 
   effective user ID of the tracing process is super-user. */
/* JAGaf92730 : attaching to a process failed when call to setresuid() */
#define ATTACH_EUID_ERR_MSG "\
\nAttaching to process %d failed. Hint: Due to Security limitation imposed by HPUX for ttrace\n\
debugging of the attached process failed. Try running the debugger as root.\n"

/* Start debugging the process whose number is PID.
 * (A _real_ pid).
 */
int
attach (int pid)
{
  char *sentinel_pid;
  if ((sentinel_pid = getenv("__SENTINEL_ATTACH__"))
       && (pid == atoi(sentinel_pid)))
    {
      /* JAGag12380: we are already attached so skip the ttrace call */
      printf("Attaching debugger to process id %d\n", pid);
      errno = 0; /* convey same behavior as a successful ttrace call */
    }
  else
    {
      call_real_ttrace (TT_PROC_ATTACH, pid, 0, 0, TT_VERSION, 0);
    }

  if (errno)
    {
      if (errno == EACCES) 
        {
          char err_buf[sizeof (ATTACH_NFS_ERR_MSG) + 30];
          uid_t gdb_euid, inferior_ruid = -1, inferior_suid = -1;
          struct pst_status inf_proc;
          errno = 0;
          /* JAGaf92730 : attaching to a process failed when call to setresuid() */
          if (pid > 0 && 
             pstat_getproc (&inf_proc, sizeof(struct pst_status), 0, pid) != -1 
             && errno == 0) 
             {
                inferior_ruid = (uid_t)inf_proc.pst_uid;
                inferior_suid = (uid_t)inf_proc.pst_suid;
             } 
          else if (errno == ESRCH)
            {
               sprintf (err_buf, "\npstat error: No such process"ATTACH_NFS_ERR_MSG, pid);
               error (err_buf);
            }
          else
            { 
              sprintf (err_buf, "\npstat error: Retrieving process status failed"ATTACH_NFS_ERR_MSG, pid);
              error (err_buf);
            }
          gdb_euid = geteuid();

          if ((gdb_euid != inferior_ruid || gdb_euid != inferior_suid)
              && gdb_euid != 0) 
            sprintf (err_buf, ATTACH_EUID_ERR_MSG, pid);
          else
            sprintf (err_buf, ATTACH_NFS_ERR_MSG, pid);
          error(err_buf);
        }
      else
	perror_with_name ("ttrace attach");
    }
  update_thread_state_after_attach (pid);
  last_event_thread = inferior_pid = pid;

#ifdef SET_TRACE_BIT
  SET_TRACE_BIT (SET_FLAGS);
#endif

  return pid;
}


void
child_post_attach (int pid)
{
  require_notification_of_events (pid);

  /* srikanth, we used to do this in attach() above. I have duped it here
     so that we can determine if this is an MxN process and suitably update
     our thread list from the user space thread list. We need to load the
     symbol tables first for this. When we get here they have been
     digested. 001210.
  */
  init_thread_list();
  clear_thread_info ();

#ifdef HP_IA64
  /* Need to invalidate rse state info here as we might get same gdb_tid mapping
     to a different utid now. gdb will wrongly think that we are looking at the
     same thread. */
  invalidate_rse_info ();
#endif
  clear_mxn_info ();
  update_thread_state_after_attach (pid);
  /* Let's reinitialize the libmxn as we are with a new process now. We need to
     do this after getting the thread state above as libmxn tries to read 
     data from the process using our callback (read_tgt_mem2) and we need
     to establish a thread state for this. Otherwise, looking up a gdb_tid
     will give an error. */
  do_mxn_init (pid);

  /* Let's re-update the thread list now. */
  if (is_mxn)
    {
      tid_t gdb_tid = map_from_gdb_tid(pid);
      /* QXCR1000794718 - The "CONTINUE" sometimes made the child
         to execute past the break instructions and child would exit.
         Removed "threads_continue_all_with_signals" as the 
         "stop_all_threads_of_process()" takes care of stopping the threads 
         at a safe place and re-updating the thread list.
      */
      stop_all_threads_of_process(pid);
    }
}


/* Stop debugging the process whose number is PID
   and continue it with signal number SIGNAL.
   SIGNAL = 0 means just continue it.
 */
void
detach (int signal)
{
  extern boolean do_fake_detach;
  thread_info *p;
  char *sentinel_pid;

  /* RM: if the active thread has a pending signal not equal to the
     signal we want to pass to it, cancel that signal before
     detaching */
  p = find_thread_info(map_from_gdb_tid(inferior_pid));
  if (p &&
      (p->last_stop_state.tts_event == TTEVT_SIGNAL) &&
      (p->last_stop_state.tts_u.tts_signal.tts_signo != signal))
    {
      threads_continue_all_with_signals(inferior_pid, signal);
    }
  
  /* If we came here through sentinel, exec it again. */
  if ((sentinel_pid = getenv("__SENTINEL_ATTACH__"))
       && (inferior_pid == atoi(sentinel_pid)))
    {
      char* monitor_path = get_monitor_path (NULL);
      char* monitor_argv[3] = {monitor_path, sentinel_pid, NULL};
      if (execvp (monitor_path, monitor_argv) == -1)
        {
          fprintf_unfiltered (gdb_stderr,
                              "error: cannot exec %s.\n", monitor_path);
          exit (1);
        }
    }

  errno = 0;
  if (!do_fake_detach)
    call_ttrace (TT_PROC_DETACH, inferior_pid, 0, signal, 0);
  attach_flag = 0;

  /* Technically, we should only be deleting the threads belonging
     to the process just detached. However, the following is ok as
     we are either left process less or in the follow-fork child
     case, we are going to call require_attach in just a moment
     which is going to blow this away anyway -- srikanth, 000726
  */
  clear_thread_info ();
  init_thread_list();  /* srikanth, Feb 24th 99, JAGaa80590 */
#ifdef HP_IA64
  /* Need to invalidate rse state info here as we might get same gdb_tid mapping
     to a different utid now. gdb will wrongly think that we are looking at the
     same thread. */
  invalidate_rse_info ();
#endif
}


/* TTrace version of "target_pid_to_exec_file"
 */
char *
child_pid_to_exec_file (int tid)
{
  int tt_status;
  static char exec_file_buffer[1024];
  pid_t pid;
  static struct pst_status buf;

  tt_status = call_ttrace (TT_PROC_GET_PATHNAME,
			   tid,
			   (uint64_t) exec_file_buffer,
			   sizeof (exec_file_buffer) - 1,
			   0);
  if (tt_status >= 0)
    return exec_file_buffer;

  /* srikanth, 000720, The call above fails on various 11.x machines
     due to a kernel bug. This happens particularly after an attach. */
  /* JYG: Try pstat_getproc ().
     The reason it failed in 32bit version of gdb on 11.x was:
     it appears that regardless of the underlying kernel (32- or 64-bit),
     the 64bit API of pstat_* should be used.
     _PSTAT64 is now defined unconditionally in this file before
     <sys/pstat.h> is included to use the 64bit interface. */

  pid = get_pid_for (tid);

  if (pstat_getproc (&buf, sizeof (struct pst_status), 0, pid) != -1)
    {
      char *p = buf.pst_cmd;

      while (*p && *p != ' ')
	p++;
      *p = NULL;

      return (buf.pst_cmd);
    }

  return (NULL);
}


void
pre_fork_inferior ()
{
  int status;

  status = pipe (startup_semaphore.parent_channel);
  if (status < 0)
    {
      warning ("error getting parent pipe for startup semaphore");
      return;
    }

  status = pipe (startup_semaphore.child_channel);
  if (status < 0)
    {
      warning ("error getting child pipe for startup semaphore");
      return;
    }
}


/* This is called to attach to the child process after a parent fork
   if our follow-fork-mode dictates that we follow the child.
   For the attach command, we don't get here and end up in attach ()
   above.
*/
int
hppa_require_attach (int pid)
{
  /* We are already attached, though the process is not stopped now */ 

  call_real_ttrace (TT_PROC_STOP, pid, 0, 0, 0, 0);
  update_thread_state_after_attach (pid);
  return pid;
}

/* This is called to detach the child process after a parent fork
   if our follow-fork-mode dictates that we stay with the parent.
   Otherwise, we don't get here and end up in detach () above.
   See that as a result the signal parameter is meaningless
   (will always be 0).
*/
int
hppa_require_detach (int pid, int signal)
{
  call_ttrace (TT_PROC_DETACH, pid, 0, 0, 0);
  errno = 0;			/* Ignore any errors. */

  discard_threads_of (pid);

  return pid;
}

/* Given the starting address of a memory page, hash it to a bucket in
   the memory page dictionary.
 */
static int
get_dictionary_bucket_of_page (CORE_ADDR page_start)
{
  int hash;

  hash = (int) (page_start / memory_page_dictionary.page_size) 
         % MEMORY_PAGE_DICTIONARY_BUCKET_COUNT;

  return hash;
}


/* Given a memory page's starting address, get (i.e., find an existing
   or create a new) dictionary entry for the page.  The page will be
   write-protected when this function returns, but may have a reference
   count of 0 (if the page was newly-added to the dictionary).
 */
static memory_page_t *
get_dictionary_entry_of_page (int pid, CORE_ADDR page_start)
{
  int bucket;
  memory_page_t *page = NULL;
  memory_page_t *previous_page = NULL;

  /* We're going to be using the dictionary now, than-kew. */
  require_memory_page_dictionary ();

  /* Try to find an existing dictionary entry for this page.  Hash
     on the page's starting address.
   */
  bucket = get_dictionary_bucket_of_page (page_start);
  page = &memory_page_dictionary.buckets[bucket];
  while (page != NULL)
    {
      if (page->page_start == page_start)
	break;
      previous_page = page;
      page = page->next;
    }

  /* Did we find a dictionary entry for this page?  If not, then
     add it to the dictionary now.
   */
  if (page == NULL)
    {
      /* Create a new entry. */
      page = (memory_page_t *) xmalloc (sizeof (memory_page_t));
      page->page_start = page_start;
      page->reference_count = 0;
      page->already_protected = 0;
      page->next = NULL;
      page->previous = NULL;

      /* We'll write-protect the page now, if that's allowed. */
      if (!page->already_protected)
        {
          page->original_permissions = write_protect_page (pid, page_start);
          page->already_protected = 1;
        }

      /* Add the new entry to the dictionary. */
      page->previous = previous_page;
      previous_page->next = page;

      memory_page_dictionary.page_count++;
    }

  return page;
}


static void
remove_dictionary_entry_of_page (int pid, memory_page_t * page)
{
  /* Restore the page's original permissions. */
  if (page->already_protected)
    {
      unwrite_protect_page (pid, page->page_start, page->original_permissions);
      if (!keep_as_protected)
        page->already_protected = 0;
      else
	return;
    }

  /* Kick the page out of the dictionary. */
  if (page->previous != NULL)
    page->previous->next = page->next;
  if (page->next != NULL)
    page->next->previous = page->previous;

  /* Just in case someone retains a handle to this after it's freed. */
  page->page_start = (CORE_ADDR) 0;

  memory_page_dictionary.page_count--;

  free (page);
}


static void
hppa_enable_syscall_events (int pid)
{
  int status;

  set_safe_syscall_bm (1);

  status = call_ttrace (TT_PROC_SET_SCBM,
               pid,
               TTSCBM_UNSELECT,
               sizeof (ttbm_t) * NUM_BYTES_IN_SCBM,
               (uint64_t) safe_syscall_bm);

  if (status <0 && errno)
    perror_with_name ("ttrace");
}


static void
hppa_disable_syscall_events (int pid)
{
  int status;

  set_safe_syscall_bm (0);

  status = call_ttrace (TT_PROC_SET_SCBM,
               pid,
               TTSCBM_UNSELECT,
               sizeof (ttbm_t) * NUM_BYTES_IN_SCBM,
               (uint64_t) safe_syscall_bm);

  if (status <0 && errno)
    perror_with_name ("ttrace");
}


/* The address range beginning with START and ending with START+LEN-1
   (inclusive) is to be watched via page-protection by a new watchpoint.
   Set protection for all pages that overlap that range.

   Note that our caller sets TYPE to:
   0 for a bp_hardware_watchpoint,
   1 for a bp_read_watchpoint,
   2 for a bp_access_watchpoint

   (Yes, this is intentionally (though lord only knows why) different
   from the TYPE that is passed to hppa_remove_hw_watchpoint.)
 */
int
hppa_insert_hw_watchpoint (int pid, CORE_ADDR start, LONGEST len,
                                                        int type)
{
  CORE_ADDR page_start;
  int dictionary_was_empty;
  int page_size;
  int page_id;
  LONGEST range_size_in_pages;

  start = SWIZZLE (start);

  if (type != 0)
    error ("read or access hardware watchpoints not supported on HP-UX");

  /* Examine all pages in the address range. */
  require_memory_page_dictionary ();

  dictionary_was_empty = (memory_page_dictionary.page_count == (LONGEST) 0);

  page_size = memory_page_dictionary.page_size;
  page_start = (start / page_size) * page_size;
  range_size_in_pages
    = ((LONGEST) len + (LONGEST) page_size - 1) / (LONGEST) page_size;

  if (debug_traces)
    {
      printf_filtered("hppa_insert_hw_watchpoint : start = 0x%lx, "
                      "range = %d\n",
                      start, range_size_in_pages);
    } 

  for (page_id = 0;
       page_id < range_size_in_pages;
       page_id++, page_start += page_size)
    {
      memory_page_t *page;

      /* This gets the page entered into the dictionary if it was
         not already entered.
       */
      page = get_dictionary_entry_of_page (pid, page_start);
      page->reference_count++;
    }

  /* Our implementation depends on seeing calls to kernel code, for the
     following reason.  Here we ask to be notified of syscalls.

     When a protected page is accessed by user code, HP-UX raises a SIGBUS.
     Fine.

     But when kernel code accesses the page, it doesn't give a SIGBUS.
     Rather, the system call that touched the page fails, with errno=EFAULT.
     Not good for us.

     We could accomodate this "feature" by asking to be notified of syscall
     entries & exits; upon getting an entry event, disabling page-protections;
     upon getting an exit event, reenabling page-protections and then checking
     if any watchpoints triggered.

     However, this turns out to be a real performance loser.  syscalls are
     usually a frequent occurrence.  Having to unprotect-reprotect all watched
     pages, and also to then read all watched memory locations and compare for
     triggers, can be quite expensive.

   */
  if (dictionary_was_empty)
    {
      syscall_enabled_from_page_prot = 1;
      if (!syscall_enabled_from_data_break)
        hppa_enable_syscall_events (pid);
    }

  return 1;
}


/* The address range beginning with START and ending with START+LEN-1
   (inclusive) was being watched via page-protection by a watchpoint
   which has been removed.  Remove protection for all pages that
   overlap that range, which are not also being watched by other
   watchpoints.
 */
int
hppa_remove_hw_watchpoint (int pid, CORE_ADDR start, LONGEST len,
                                                enum bptype type)
{
  CORE_ADDR page_start;
  int dictionary_is_empty;
  int page_size;
  int page_id;
  LONGEST range_size_in_pages;

  start = SWIZZLE (start);

  if (type != 0)
    error ("read or access hardware watchpoints not supported on HP-UX");

  /* Examine all pages in the address range. */
  require_memory_page_dictionary ();

  page_size = memory_page_dictionary.page_size;
  page_start = (start / page_size) * page_size;
  range_size_in_pages = ((LONGEST) len + (LONGEST) page_size - 1) / (LONGEST) page_size;

  for (page_id = 0; page_id < range_size_in_pages; page_id++, page_start += page_size)
    {
      memory_page_t *page;

      page = get_dictionary_entry_of_page (pid, page_start);
      page->reference_count--;

      /* Was this the last reference of this page?  If so, then we
         must scrub the entry from the dictionary, and also restore
         the page's original permissions.
       */
      if (page->reference_count == 0)
	remove_dictionary_entry_of_page (pid, page);
    }

  dictionary_is_empty = (memory_page_dictionary.page_count == (LONGEST) 0);

  /* If write protections are currently disallowed, then that implies that
     wait_for_inferior believes that the inferior is within a system call.
     Since we want to see both syscall entry and return, it's clearly not
     good to disable syscall events in this state!

     ??rehrauer: Yeah, it'd be better if we had a specific flag that said,
     "inferior is between syscall events now".  Oh well.
   */
  if (dictionary_is_empty && memory_page_dictionary.page_protections_allowed)
    {
      /* Bindu: Mark it as disabled and do not disable them till it is also
	 disabled from data breakpoints. */
      syscall_enabled_from_page_prot = 0;
      if (!syscall_enabled_from_data_break)
        hppa_disable_syscall_events (pid);
    }

  return 1;
}


/* Could we implement a watchpoint of this type via our available
   hardware support?

   This query does not consider whether a particular address range
   could be so watched, but just whether support is generally available
   for such things.  See hppa_range_profitable_for_hw_watchpoint for a
   query that answers whether a particular range should be watched via
   hardware support.
 */
int
hppa_can_use_hw_watchpoint (enum bptype type, int cnt, enum bptype ot)
{
  return (type == bp_hardware_watchpoint);
}


/* Assuming we could set a hardware watchpoint on this address, do
   we think it would be profitable ("a good idea") to do so?  If not,
   we can always set a regular (aka single-step & test) watchpoint
   on the address...
 */
int
hppa_range_profitable_for_hw_watchpoint (int pid, CORE_ADDR start,
                                                      LONGEST len)
{
  int range_is_stack_based;
  int range_is_accessible;
  CORE_ADDR page_start;
  int page_size;
  int page;
  LONGEST range_size_in_pages;

  /* ??rehrauer: For now, say that all addresses are potentially
     profitable.  Possibly later we'll want to test the address
     for "stackness"?
   */
  range_is_stack_based = 0;

  /* If any page in the range is inaccessible, then we cannot
     really use hardware watchpointing, even though our client
     thinks we can.  In that case, it's actually an error to
     attempt to use hw watchpoints, so we'll tell our client
     that the range is "unprofitable", and hope that they listen...
   */
  range_is_accessible = 1;	/* Until proven otherwise. */

  /* Examine all pages in the address range. */
  errno = 0;
  page_size = (int) sysconf (_SC_PAGE_SIZE);

  page_start = (start / page_size) * page_size;
  range_size_in_pages = len / (LONGEST) page_size;

  for (page = 0; page < range_size_in_pages; page++, page_start += page_size)
    {
      int tt_status;
      int page_permissions;

      /* Is this page accessible? */
      errno = 0;
      tt_status = call_ttrace (TT_PROC_GET_MPROTECT,
			       pid,
			       page_start,
			       0,
			       (uint64_t) &page_permissions);
      if (errno || (tt_status < 0))
	{
	  errno = 0;
	  range_is_accessible = 0;
	  break;
	}

      /* Yes, go for another... */
    }

  return (!range_is_stack_based && range_is_accessible);
}

/* Number of available data breakpoint registers */
static int max_num_data_breakpoints = -1;
/* Number of available instruction breakpoint registers */
static int max_num_inst_breakpoints = -1;

/* Bindu 042805 JAGaf61080
   Use this global to set atleast one of the 'other' kind of breakpoint
   register to 0 when inserting a hw break/watch. See JAG for more details.
 */
static boolean breakpoint_regs_set = FALSE;

/* Keeps track of data breakpoint registers in use */
typedef struct data_or_inst_reg_info {
  CORE_ADDR addr;
  LONGEST len;
} data_or_inst_reg_info_t;

static data_or_inst_reg_info_t *data_reg;
static data_or_inst_reg_info_t *inst_reg;


#ifdef HP_IA64
/* Set the max_num_data_breakpoints and return true for
   bptype bp_hardware_watchpoint. We can do both data breakpoints and
   page protection watchpoints (for systems with 11.23 or more). */
int
ia64_can_use_hw_watchpoint (enum bptype type, int cnt, enum bptype ot)
{
  errno = 0;
  if ((max_num_data_breakpoints == -1) && (type == bp_hardware_watchpoint))
    {
      int num_data_regs = ttrace (TT_PROC_GET_NUM_DBPT_REGS, 0, 0, 0, 0, 0);
      if (errno)
	{
	  max_num_data_breakpoints = 0;
	  if (!is_11_23_or_greater)
	    return 0;
	}
      else
        {
          max_num_data_breakpoints = num_data_regs / 2;
          data_reg = xmalloc (sizeof (data_or_inst_reg_info_t) * 
		max_num_data_breakpoints);
          ia64_reset_data_reg (); 
#if 0
          /* We cannot use this as on every rerun we will have to reinstall
	     this cleanup as well. Otherwise, after the first rerun
	     ia64_reset_data_reg is never called. Let's do this work in
	     reset_memory_page_protections . */
          make_run_cleanup (ia64_reset_data_reg, data_reg);
#endif
	}
    }

  if ((max_num_inst_breakpoints == -1) && (type == bp_hardware_breakpoint))
    {
      int num_inst_regs = ttrace (TT_PROC_GET_NUM_IBPT_REGS, 0, 0, 0, 0, 0);
      if (errno)
        {
          max_num_inst_breakpoints = 0;
          return 0;
        }
      else
        {
          max_num_inst_breakpoints = num_inst_regs / 2;
          inst_reg = xmalloc (sizeof (data_or_inst_reg_info_t) *
                max_num_inst_breakpoints);
          ia64_reset_inst_reg ();
        }
    }

  if (type == bp_hardware_watchpoint)
    {
      if (is_11_23_or_greater)
        {
          /* We have infinite hw watchpoints with page-protection. */
          return 1;
        }
      else
        return (cnt <= max_num_data_breakpoints);
    }
  if (type == bp_hardware_breakpoint)
    if (cnt <= max_num_inst_breakpoints)
      return 1;
    else
      return -1;

  return 0;
}

/* addr2 arg to ttrace for TT_PROC_GET_IBPT_REGS and TT_PROC_SET_IBPT_REGS */
typedef struct inst_addr_mask {
  uint64_t execute_match_enable:     1;
  uint64_t ignore:                3;
  uint64_t privilege_level_mask:  4;
  uint64_t mask:                 56;
} inst_addr_mask_t;



typedef struct inst_addr2 {
    CORE_ADDR addr;
    inst_addr_mask_t mask;
} inst_addr2_t;

/* addr2 arg to ttrace for TT_PROC_GET_DBPT_REGS and TT_PROC_SET_DBPT_REGS */

typedef struct addr_mask {
  uint64_t read_match_enable:     1;
  uint64_t write_match_enable:    1;
  uint64_t ignore:                2;
  uint64_t privilege_level_mask:  4;
  uint64_t mask:                 56;
} addr_mask_t;

typedef struct addr2 {
    CORE_ADDR addr;
    addr_mask_t mask;
} addr2_t;

/* SHADOW is ignored */
int
ia64_insert_hw_breakpoint (CORE_ADDR addr, void* shadow)
{
  struct breakpoint *b;
  int i;
  int reg_num = -1;
  inst_addr2_t addr2;
  int debug_trace = 0;

  /* Determine which instruction breakpoint register is available */
  i = 0;
  while (i < max_num_inst_breakpoints)
    {
      if (inst_reg[i].addr == 0)
	{
	  inst_reg[i].addr = addr;
	  reg_num = i * 2;  /* register number is always even */
	  break;
	} 
      i++;
    }
  if (i == max_num_inst_breakpoints)
    {
      warning ("cannot set data breakpoint at 0x%llu\n", addr);
      return -1;
    }
  if (reg_num == -1)
    {
      warning ("cannot set data breakpoint at 0x%llu\n", addr);
      return -1;
    }

  if (!breakpoint_regs_set)
    {
      addr2_t d_addr2;
      bzero (&d_addr2, 16);

      (void) call_ttrace (TT_PROC_SET_DBPT_REGS,
                          inferior_pid,
                          0,
                          16,  /* sizeof(addr2) */
                          (uint64_t) &d_addr2);
      if (errno)
	printf_filtered ("errno on ttrace (TT_PROC_SET_DBPT_REGS): %d\n", errno);
      else
        breakpoint_regs_set = TRUE;
    }
  /* See IA64 architecture software developer's manual, Vol2
     for information on each of these fields. */
  /* Set up addr2 parameter for ttrace */
  /* Make sure addr is 64-bits */
  addr2.addr = SWIZZLE(addr); 

  addr2.mask.execute_match_enable = 1;
  addr2.mask.ignore = 0;
  addr2.mask.privilege_level_mask = 0xf;  /* all levels? */

  /* breakpoint is on just this address. So set all bits in the mask. */ 
  addr2.mask.mask = 0xffffffffffffffULL;

  (void) call_ttrace (TT_PROC_SET_IBPT_REGS,
                           inferior_pid,
                           reg_num,
                           16,  /* sizeof(addr2) */
                           (uint64_t) &addr2);

  if (errno)
      perror_with_name ("ia64_insert_hw_breakpoint: ttrace SET_IBPT_REGS");

  if (debug_trace)
  {
    unsigned long inst_buf[4] = {0, 0, 0, 0};
    (void) call_ttrace (TT_PROC_GET_IBPT_REGS,
                             inferior_pid,
                             reg_num,
                             16,  /* sizeof(addr2) */
                             (uint64_t) inst_buf);
    if (errno)
      printf ("errno on ttrace (TT_PROC_GET_IBPT_REGS): %d\n", errno);

    printf ("ia64_insert_hw_breakpoint: pid: %d, reg: %d, 0x%lx 0x%lx 0x%lx 0x%lx\n",
            inferior_pid, reg_num, inst_buf[0], inst_buf[1], inst_buf[2], inst_buf[3]);
  }

  return 0;
}

int
ia64_remove_hw_breakpoint (CORE_ADDR addr, void* shadow)
{
  inst_addr2_t addr2;
  int index;
  int reg_num = -1;
  int debug_trace = 0;
  unsigned long inst_buf[4];

  /* Determine which instruction breakpoint register was used and remove it from
     the data_reg array */
  index = 0;
  while (index < max_num_inst_breakpoints)
    {
      if (inst_reg[index].addr == addr)
	{ 
	  /* found a match -- now remove it */
          inst_reg[index].addr = (CORE_ADDR) 0;
          inst_reg[index].len = 0;

          reg_num = index * 2;  /* register number is always even */

          /* QXCR1001054246 :
             According to the Intel Itanium Architecture SDM, Revision 2.2 :
             "If IBR.x is 0, that instruction breakpoint register is disabled"

             We need to clear the execute_match_enable field and also reset
             the other bits to reset IBR using TT_PROC_SET_IBPT_REGS.
          */
          addr2.mask.execute_match_enable = 0;
          addr2.mask.ignore = 0;
          addr2.mask.privilege_level_mask = 0xf;  /* all levels? */
	  addr2.addr = 0x0;
          addr2.mask.mask = 0xffffffffffffffULL;

	  if (debug_trace)
	    {
	      memcpy (inst_buf, &addr2, sizeof(addr2)); 
	      printf_filtered ("ia64_remove_hw_breakpoint 1: "
                      "pid: %d, reg: %d, data: 0x%lx 0x%lx 0x%lx 0x%lx\n",
		      inferior_pid, index,
                      inst_buf[0], inst_buf[1], inst_buf[2], inst_buf[3]);
	    }

	  (void) call_ttrace (TT_PROC_SET_IBPT_REGS,
				   inferior_pid,
				   reg_num,
				   16,  /* sizeof(addr2) */
				   (uint64_t) &addr2);
  	  if (errno)
	    {
	      warning ("ia64_remove_hw_breakpoint: ttrace SET_IBPT_REGS"); 
	      return -1;
	    }

	  if (debug_trace)
	    {
	      inst_buf[0] = (unsigned long)(long) -1;
	      inst_buf[1] = (unsigned long)(long) -1;
	      inst_buf[2] = (unsigned long)(long) -1;
	      inst_buf[3] = (unsigned long)(long) -1;
	      (void) call_ttrace (TT_PROC_GET_IBPT_REGS,
				       inferior_pid,
				       reg_num,
	                               16,  /* sizeof(addr2) */
				       (uint64_t) inst_buf);
	      if (errno)
		printf_filtered ("ia64_remove_hw_breakpoint 2: "
                        "errno on ttrace (TT_PROC_GET_IBPT_REGS): "
                        "%d\n", errno);
	      else
	        printf_filtered ("ia64_remove_hw_breakpoint 2: "
                        "pid: %d, reg: %d, data: "
                        "0x%lx 0x%lx 0x%lx 0x%lx\n",
		        inferior_pid, index,
                        inst_buf[0], inst_buf[1],
                        inst_buf[2], inst_buf[3]);
	    }
	  return 0; /* found a match */
	} 
      index++;
    } /* while */

  /* Search for the breakpoint failed. */
  return -1;
}

/* Insert a hardware data breakpoint if any left, else insert
   page protection watchpoint. */
/* See comments for hppa_insert_hw_watchpoint for type parameter */
int
ia64_insert_hw_watchpoint (int pid, CORE_ADDR start, LONGEST len,
                                                        int type)
{
  struct breakpoint *b;
  int i;
  int reg_num = -1;
  addr2_t addr2;
  int debug_trace = 0;
  int new_len;

  if (type != 0)
    error ("read or access hardware watchpoints not supported on HP-UX"); 

  /* Determine which data breakpoint register is available */
  i = 0;
  while (i < max_num_data_breakpoints)
    {
      if (data_reg[i].addr == 0)
	{
	  data_reg[i].addr = start;
	  data_reg[i].len = len;
	  reg_num = i * 2;  /* register number is always even */
	  break;
	} 
      i++;
    }
  if (is_11_23_or_greater && (i == max_num_data_breakpoints))
    {
      /* Insert page-protection watchpoint. */
      return hppa_insert_hw_watchpoint (pid, start, len, type);
    }
  if (reg_num == -1)
    {
      warning ("cannot set data breakpoint at 0x%llu, len = %d\n", start, len);
      return -1;
    }

  if (!breakpoint_regs_set)
    {
      inst_addr2_t i_addr2;
      bzero (&i_addr2, 16);

      (void) call_ttrace (TT_PROC_SET_IBPT_REGS,
                          inferior_pid,
                          0,
                          16,  /* sizeof(addr2) */
                          (uint64_t) &i_addr2);
      if (errno)
        printf_filtered ("errno on ttrace (TT_PROC_SET_IBPT_REGS): "
                         "%d\n", errno);
      else
        breakpoint_regs_set = TRUE;
    }

  /* Set up addr2 parameter for ttrace */
  /* Make sure addr is 64-bits */
  addr2.addr = SWIZZLE(start); 

  addr2.mask.read_match_enable = 0;
  addr2.mask.write_match_enable = 1;
  addr2.mask.ignore = 0;
  addr2.mask.privilege_level_mask = 0xf;  /* all levels? */

  /* mask needs to be power of 2 */ 
  new_len = 1;
  do
    new_len = new_len << 1;
  while (new_len < len);
  addr2.mask.mask = (0xffffffffffffffULL & ~(new_len - 1));

  if (debug_traces)
  {
    printf_filtered ("ia64_insert_hw_watchpoint: \n"
            "calling TT_PROC_SET_DBPT_REGS: "
            "pid: %d, reg: %d, 0x%lx %x %x %x %x 0x%lx\n",
            pid, reg_num, addr2.addr, addr2.mask.read_match_enable,
            addr2.mask.write_match_enable, addr2.mask.ignore,
            addr2.mask.privilege_level_mask, addr2.mask.mask);
  }

  (void) call_ttrace (TT_PROC_SET_DBPT_REGS,
                           pid,
                           reg_num,
                           16,  /* sizeof(addr2) */
                           (uint64_t) &addr2);

  if (errno)
      perror_with_name ("ia64_insert_hw_watchpoint: ttrace SET_DBPT_REGS");

  if (debug_traces)
  {
    unsigned long data_buf[4] = {0, 0, 0, 0};
    (void) call_ttrace (TT_PROC_GET_DBPT_REGS,
                             pid,
                             reg_num,
                             16,  /* sizeof(addr2) */
                             (uint64_t) data_buf);
    if (errno)
      printf ("errno on ttrace (TT_PROC_GET_DBPT_REGS): %d\n", errno);

    printf_filtered ("ia64_insert_hw_watchpoint: "
            "pid: %d, reg: %d, 0x%lx 0x%lx 0x%lx 0x%lx\n",
            pid, reg_num, data_buf[0], data_buf[1],
            data_buf[2], data_buf[3]);
  }

  /* Bindu: Enable syscall events even for data breakpoints. This is because
     data breakpoints are not triggering when in syscall due to a kernel
     defect. So we stop at syscall entry/exit events and see if the watched
     location is changed. */
  if (!syscall_enabled_from_page_prot && !syscall_enabled_from_data_break)
    hppa_enable_syscall_events (pid);
  syscall_enabled_from_data_break = 1;

  return 1;
}
 
/* Remove a hardware watchpoint. It can either be data breakpoint or 
   page-protection watchpoint.  Return 0 on success and -1 on failure. */
int
ia64_remove_hw_watchpoint (int pid, CORE_ADDR start, LONGEST len, int type)
{
  addr2_t addr2;
  int index;
  int reg_num = -1;
  int debug_trace = 0;
  unsigned long data_buf[4];

  if (type != 0)
    error ("read or access hardware watchpoints not supported on HP-UX"); 
  
  /* Determine which data breakpoint register was used and remove it from
     the data_reg array */
  index = 0;
  while (index < max_num_data_breakpoints)
    {
      if (data_reg[index].addr == start && data_reg[index].len == len)
	{ 
	  /* found a match -- now remove it */
          data_reg[index].addr = (CORE_ADDR) 0;
          data_reg[index].len = 0;

          reg_num = index * 2;  /* register number is always even */

          /* QXCR1001054246 :
             According to the Intel Itanium Architecture SDM, Revision 2.2 :
             "If DBR.r and DBR.w are both 0, that data breakpoint register
             is disabled"

             We need to clear the read_match_enable, write_match_enable fields
             and also clear the other bits to reset DBR using
             TT_PROC_SET_DBPT_REGS.
          */
	  addr2.addr = 0x0;
          addr2.mask.read_match_enable = 0;
          addr2.mask.write_match_enable = 0;
          addr2.mask.ignore = 0;
          addr2.mask.privilege_level_mask = 0xf;  /* all levels? */
          addr2.mask.mask = 0xffffffffffffffULL;

	  if (debug_traces)
	    {
	      memcpy (data_buf, &addr2, sizeof(addr2)); 
	      printf_filtered ("ia64_remove_hw_watchpoint 2: pid: %d, "
                               "reg: %d, data: "
                               "0x%lx 0x%lx 0x%lx 0x%lx\n",
		               pid, index,
                               data_buf[0], data_buf[1],
                               data_buf[2], data_buf[3]);
	    }

	  (void) call_ttrace (TT_PROC_SET_DBPT_REGS,
				   pid,
				   reg_num,
				   16,  /* sizeof(addr2) */
				   (uint64_t) &addr2);
  	  if (errno)
	    {
	      warning ("ia64_remove_hw_watchpoint: ttrace SET_DBPT_REGS"); 
	      return -1;
	    }

	  if (debug_traces)
	    {
	      data_buf[0] = (unsigned long)(long) -1;
	      data_buf[1] = (unsigned long)(long) -1;
	      data_buf[2] = (unsigned long)(long) -1;
	      data_buf[3] = (unsigned long)(long) -1;
	      (void) call_ttrace (TT_PROC_GET_DBPT_REGS,
				       pid,
				       reg_num,
	                               16,  /* sizeof(addr2) */
				       (uint64_t) data_buf);
	      if (errno)
		printf ("ia64_remove_hw_watchpoint 2: errno on "
                        "ttrace (TT_PROC_GET_DBPT_REGS): %d\n", errno);
	      else
	        printf_filtered ("ia64_remove_hw_watchpoint 3: "
                                 "pid: %d, reg: %d, data: "
                                 "0x%lx 0x%lx 0x%lx 0x%lx\n",
		                 pid, index,
                                 data_buf[0], data_buf[1],
                                 data_buf[2], data_buf[3]);

	    }
	  /* Bindu: Disable syscall events when all the data breakpoints
	     are removed. */
  	  {
	    int i;
	    for (i = 0; i < max_num_data_breakpoints; i++)
	      if (data_reg[i].addr != 0 && data_reg[i].len != 0)
	        break;
	    if (i == max_num_data_breakpoints)
	      {
	        syscall_enabled_from_data_break = 0;
	        if (!syscall_enabled_from_page_prot)
		  hppa_disable_syscall_events (pid);
	      }
	  }
	  return 0; /* found a match */
	} 
      index++;
    } /* while */

  if (is_11_23_or_greater && (index == max_num_data_breakpoints))
    {
      /* Check the page-protection watchpoints. */
      return hppa_remove_hw_watchpoint (pid, start, len, type);
    }
  /* Search for the watchpoint failed. */
  return -1;
}

static void 
ia64_reset_data_reg (void)
{
  breakpoint_regs_set = FALSE;
  if (data_reg)
    memset (data_reg, 0, sizeof (data_or_inst_reg_info_t) * max_num_data_breakpoints);
}

static void
ia64_reset_inst_reg (void)
{
  breakpoint_regs_set = FALSE;
  if (inst_reg)
    memset (inst_reg, 0, sizeof (data_or_inst_reg_info_t) * max_num_inst_breakpoints);
}
#endif /* HP_IA64 */

/* Software breakpoint can fail if we are trying to place a breakpoint on a
   function in a shared library that is not mapped private. 
   In case of internal breakpoint, it is not user friendly to emit the chatr
   message. 
   So for internal breakpoint on IA, we will first try placing a
   software breakpoint, if s/w breakpoint insertion fail, we will try placing 
   a hardware breakpoint. If this was also unsuccessful we give a message 
   indicating that the user has to do chatr +dbg enable on his executable.
*/
int
target_insert_internal_breakpoint (CORE_ADDR addr, void* shadow)
{
  int val = target_insert_breakpoint (addr, shadow);
#ifdef HP_IA64
  /* Check if software breakpoints were not inserted. 
     If the insertion was unsuccessful, the return value will be 1.
  */
  if (val && DISABLE_UNSETTABLE_BREAK (addr))
    {
      /* If the argument to next_hw_breakpoint is true then it
         returns the number of internal hardware breakpoints used.
      */

      int i = next_hw_breakpoint (1);
      int target_resources_ok =
          TARGET_CAN_USE_HARDWARE_WATCHPOINT (bp_hardware_breakpoint,
                                              i + 1, 0);
      /* Hardware limit exceeded ....*/
      if (target_resources_ok < 0)
        return 1;

      if (target_resources_ok > 0)
        val = ia64_insert_hw_breakpoint (addr, shadow);
    }
#endif
  return val;
}

/* Similar to target_insert_internal_breakpoint, this function
   will call the appopriate remove function based on whether
   software/hardware breakpoint was inserted.
*/
int
target_remove_internal_breakpoint (CORE_ADDR addr, void* shadow)
{
  int ret_val = 0;
/* If it is IA, check if a hardware breakpoint was inserted
   by scanning the instruction breakpoint register.
*/
#ifdef HP_IA64
  ret_val = ia64_remove_hw_breakpoint (addr, shadow);
  if (ret_val == -1)
#endif
  ret_val = target_remove_breakpoint (addr, shadow);

  return ret_val;
}

/* Format a thread id, given TID.  Be sure to terminate
   this with a null--it's going to be printed via a "%s".

   Note: This is a core-gdb tid, not the actual system tid.
   See infttrace.c for details.  */
char *
hppa_tid_to_str (pid_t tid)
{
  /* Static because address returned */
  static char buf[50];

#ifdef CMA_THREAD_FLAG
  if (IS_CMA_PID(tid))
    {
      sprintf( buf, "user thread (%d, %d)", PIDGET(tid), THREAD_IDX(tid) );
    }
  else
#endif
    /* Extra NULLs for paranoia's sake */

  /* On 11.00, we synthesize artificial thread ids so as to avoid
     conflicts between pids and tids. These should not be exposed
     to the user.  -- srikanth, 000929.
  */
  /* bindu 061902: Before we used to expose both system thread and
   * user thread id for MxN threads. Now we print the user thread id
   * in thread.c itself and here we can print only system thread id.
   */
#ifndef HPUX_1020
    {
      sprintf (buf, "system thread %d", 
	    	(target_has_execution
#ifdef HP_MXN
                 ||is_mxn
#endif
                )?get_lwp_for (tid):tid);
      sprintf (buf, "%s\0\0\0\0", buf);
    }
#else 
    sprintf (buf, "system thread %d\0\0\0\0", tid);
#endif

  return buf;
}

char *
hppa_pid_or_tid_to_str (pid_t id)
{
  static char buf[100];		/* Static because address returned. */

  /* Does this appear to be a process?  If so, print it that way. */
  if (is_process_id (id))
    return child_pid_to_str (id);
#ifdef HP_MXN
  if (is_mxn)
    /* Use utid instead of GDB thread number for MxN threads. */
    sprintf (buf, "thread %d (", get_pthread_for (id));
  else 
#endif
    sprintf (buf, "thread %d (", pid_to_thread_id (id));
  strcat (buf, hppa_tid_to_str (id));
  strcat (buf, ")\0");

  return buf;
}


/* If the current pid is not the pid this module reported
 * from "ptrace_wait" with the most recent event, then the
 * user has switched threads.
 *
 * If the last reported event was a breakpoint, then return
 * the old thread id, else return 0.
 */
pid_t
hppa_switched_threads (pid_t gdb_pid)
{
  /* srikanth, 000804, The top level comment is correct, but the
     code here was doing something else. All we are supposed to
     answer is whether we reported a breakpoint and the user chose
     a different thread to resume.
  */
  if (reported_bpt && gdb_pid != reported_pid)
      return reported_pid;

  return 0;
}

void
hppa_ensure_vforking_parent_remains_stopped (int pid)
{
  /* Nothing to do when using ttrace.  Only the ptrace-based implementation
     must do real work.
   */
}


int
hppa_resume_execd_vforking_child_to_get_parent_vfork ()
{
  return 0;			/* No, the parent vfork is available now. */
}


/* Write a register as a 64bit value.  This may be necessary if the
   native OS is too braindamaged to allow some (or all) registers to
   be written in 32bit hunks such as hpux11 and the PC queue registers.

   This is horribly gross and disgusting.  */
 
int
ttrace_write_reg_64 (int gdb_tid, CORE_ADDR dest_addr,
                                  CORE_ADDR src_addr)
{
  int  		tt_status;

  errno = 0;
  tt_status = call_ttrace (TT_LWP_WUREGS, gdb_tid, dest_addr, 8, src_addr);
  return tt_status;
}

/* Callback provided for mxn library to get the register number from 
   save_state_offset. */
int
regno_from_save_state_offset (uint64_t ss_offset)
{
  return register_num_for_io;
}

/* Callback provided for mxn library to read the data from the corefiles. */
static int
read_tgt_mem2 (CORE_ADDR memaddr, CORE_ADDR myaddr, int len)
{
  return target_read_memory (memaddr, (char *) (unsigned long) myaddr, len);
}

/* This is a callback provided for libmxndbg for it to find out if the
   debugee is an ilp32 program. */
static int 
is_ilp32 ()
{
#ifdef SWIZZLE
  return is_swizzled;
#else
  return 0;
#endif
}

extern struct thr_list* read_kthread_list (void);
extern CORE_ADDR lookup_address_of_variable (char *);

void *mxn_debug_callbacks [] = { (void*) lookup_address_of_variable,
				 (void*) read_tgt_mem2,
				 (void*) read_kthread_list,
				 (void*) is_ilp32,
				 (void*) 0, /* set breakpoint */
			       };

/* Check if mxn libpthread is loaded. libmxn does this by looking for
 * __active_pthreads and __mxn_magic symbols from the symbol table.
 * Also set the mxn_safe_address, mxn_unsafe_lwp_address and
 * mxn_safe_fork_address.
 */
void
do_mxn_init (int pid)
{
  if (!is_mxn)
    {
      if (target_has_stack || attach_flag)
        is_mxn = mxn_debug_init (mxn_debug_callbacks, pid);
      if (is_mxn && !mxn_unsafe_lwp_address)
        {
          struct minimal_symbol * m;

          m = lookup_minimal_symbol("__pthread_dbg_unsafe_lwp", 0, 0);
          if (m)
            mxn_unsafe_lwp_address = SYMBOL_VALUE_ADDRESS (m);
          m = lookup_minimal_symbol("__pthread_dbg_dummy", 0, 0);
          if (m)
            mxn_safe_address = SYMBOL_VALUE_ADDRESS (m);
          m = lookup_minimal_symbol("__pthread_fork_dummy", 0, 0);
          if (m)
            mxn_safe_fork_address = SYMBOL_VALUE_ADDRESS (m);
          symbol_table_load_hook = 0;
        }
    }
}

/* We ask for user thread exit events when user issues 'thread disable'
   command. To detect the case when some of the threads are disabled
   by 'thread disable' command, and user hits a continue. All the other
   active threads exit during this run and gdb will be stuck on
   ttrace_wait because all the threads that are alive now are disabled
   threads. Requesting the exit events will let gdb know about the exiting
   threads. */
void
enable_ut_exit_events (int pid)
{
  ttevent_t ttrace_events;

  /* Enable user thread exit events. We need to set the
     __pthread_ttrace_events mask in libpthread so that it will report the
     user thread events to ttrace.
   */
  if (is_mxn && !enabled_ut_events && (feature_level >= 13))
    {
      struct minimal_symbol * m;
      CORE_ADDR anaddr;
      char buf[4];
      int status;

      m = lookup_minimal_symbol ("__pthread_ttrace_events", NULL, NULL);
      if (m)
        {
	  anaddr = SYMBOL_VALUE_ADDRESS (m);
	  store_unsigned_integer (buf, 4, TTEVT_UT_EXIT);

	  status = target_write_memory (anaddr, buf, 4);
	  if (status != 0)
   	    error ("Cannot update __pthread_ttrace_events\n");

          /* Now do the ttrace part of work. */
          /* Get the set of events that are currently enabled. */
	  ttrace_events = null_ttevent_t;
	  call_ttrace (TT_PROC_GET_EVENT_MASK,
	               pid,
	               (uint64_t) &ttrace_events,
	               sizeof (ttrace_events),
	               0);
	  if (errno)
	    perror_with_name ("ttrace");

	  /* Add user thread exit event to that set. */
	  ttrace_events.tte_events |= TTEVT_UT_EXIT;

	  call_ttrace (TT_PROC_SET_EVENT_MASK,
	               pid,
	               (uint64_t) &ttrace_events,
	               sizeof (ttrace_events),
	               0);
	  if (errno)
	    perror_with_name ("ttrace");
        }
      else
	{
	  /* Target may be linked with an old pthread which doesn't
	     support user thread events. Just ignore. */
	}
      enabled_ut_events = 1;
    }
}


/* When a new library gets loaded, check to see if this is libpthread.
 * If the name of the library matches 'libpthread', call do_mxn_init. 
 */
static void
some_symbol_table_got_loaded (char* libname)
{
  char *libpthread_name = "libpthread";
  char * match = strstr (libname, libpthread_name);
  if (match)
    {
      do_mxn_init (get_pid_for (inferior_pid));
    }
}


/* Check if current thread is stopped in system call. This function is
   used in ia64-tdep.c to check if a thread is in syscall. */
/* JAGaf04443 - to check if current thread is stopped in a syscall.
   FIXME: Core file case yet to be handled for PA. */
int 
cur_thread_in_syscall()
{

#ifdef HP_IA64
  /* From bfd/elf.c, not used elsewhere. */
  extern int core_thread_in_syscall ( int , bfd *);

  /* For corefile case, we need to get the info from the bfd. */
  if (!target_has_execution)
    {
      if (inferior_pid & TID_MARKER) /* unbound thread cannot be in syscall. */
        return 0;
      return core_thread_in_syscall (inferior_pid, current_core_bfd);
    }
  else
#endif
    {
      thread_info *tst;
      tst = find_thread_info(inferior_pid);

      if (tst->last_stop_state.tts_flags & TTS_INSYSCALL)
        return 1;
      return 0;
    }
}

static void
fetch_this_register (int regno)
{
  register_num_for_io = regno; 
}

void
_initialize_infttrace ()
{
  struct utsname release_version;
  extern void (*register_io_hook) (int);
  /* Initialize the ttrace-based hardware watchpoint implementation. */
  memory_page_dictionary.page_count = (LONGEST) - 1;
  memory_page_dictionary.page_protections_allowed = 1;

  memory_page_dictionary.page_size = (int) sysconf (_SC_PAGE_SIZE);
  symbol_table_load_hook = some_symbol_table_got_loaded;
#ifdef HP_MXN
  register_io_hook = fetch_this_register;
#endif

  /* srikanth, what version of ttrace is this ? We need 9 to support
     MxN threads.
  */
  errno = 0;
  feature_level = ttrace (TT_NDR_GET_FLEV, 0, 0, 0, 0, 0);
  if (errno)
    feature_level = 0;

   uname (&release_version);
   if ( strcmp (release_version.release, "B.11.20") >= 0 ) 
     {
        is_11_20_or_greater = 1;
     }  
   if (strcmp (release_version.release, "B.11.23") >= 0 )
     {
        is_11_23_or_greater = 1;
     }

  /* dumpcore command. */
  add_com ("dumpcore", no_class, dumpcore_command,
           "Generate a core file without modifying the process's state.\n\
Core file will be saved in core.<pid> file in the current directory.");
  add_com_alias ("dump", "dumpcore", no_class, 1);
  add_com_alias ("gcore", "dumpcore", no_class, 1);
}

/* bindu 101601: By the time gdb receives a fork event from the child 
 * process, the child is not in a safe place to collect the state 
 * information like the active thread list. So, place a breakpoint at the 
 * mxn_safe_fork_address (libpthread) and continue till we hit this 
 * breakpoint. Now stop all the threads and collect the thread list.
 */
   
void
continue_child_after_fork(int pid)
{
  int tcode;
  ttstate_t stop_status;
  char buffer[16];
  int saved_inferior_pid = inferior_pid;

  /* If the thread is not before __pthread_for_dummy or there is
     no __pthread_fork_dummy, we are in a a safe place. Collect 
     the thread info. */
  if ((before_fork_dummy == 0) || (mxn_safe_fork_address == 0))
    {
      stop_all_threads_of_process(get_pid_for(pid));
      return;
    }
  /* re-set to 0. */
  before_fork_dummy = 0;

  inferior_pid = pid;
  /* JAGag26864 - Call target_insert_internal_breakpoint instead of
     target_insert_breakpoint so that if software breakpoint fails,
     hardware will be inserted.
     If hardware insertion also fails, give chatr +dbg enable message.
  */
  int val = target_insert_internal_breakpoint (mxn_safe_fork_address, buffer);
  if (val)
    {
      warning ("The shared libraries were not privately mapped; "
      "MXN threads cannot be debugged properly.\n"
      "Use the following command to enable debugging of shared libraries private.\n"
      "chatr +dbg enable <executable>");
      return;
    }


  tcode = call_real_ttrace (TT_LWP_CONTINUE, get_pid_for(pid), pid, TT_NOPC, 0, 0);
  if (tcode == -1)
    error ("Error continuing unsafe thread \n");
  errno = 0;
  tcode = call_real_ttrace_wait (get_pid_for(pid), pid, TTRACE_WAITOK, 
				 &stop_status, sizeof(stop_status));
  /* JAGag26864 - Call the appropriate breakpoint remove function. */
  target_remove_internal_breakpoint (mxn_safe_fork_address, buffer);
  inferior_pid = saved_inferior_pid;
 
  stop_all_threads_of_process(get_pid_for(pid));
  
}

/* JAGae75018 - dumpcore command to produce core in the middle of execution without killing
   the process. If an argument is provided, dump into that particular file. */

static void
dumpcore_command (char *file_name, int from_tty)
{
  int status;

  if (!target_has_execution)
    {
      printf_filtered ("Process is not running.\n");
      return;
    }

  if (from_tty)
    if (file_name)
      printf_filtered ("Dumping core to the core file %s\n", file_name);
    else
      printf_filtered ("Dumping core to the core file core.%d\n",
                       get_pid_for (inferior_pid));

   status = ttrace (TT_PROC_CORE, get_pid_for (inferior_pid), 0,
                    (uint64_t) file_name, 0, 0);
  
   /* Let's not error out now. ttrace may not be having the support for
    * dumping core into a specified file. Try dumping into the default file.
    */
   if ((status == -1) && file_name)
     {
       printf_filtered ("Feature not yet supported: Dumping core to the"
                        " core file core.%d\n", get_pid_for (inferior_pid));

       status = ttrace (TT_PROC_CORE, get_pid_for (inferior_pid), 0,
                    0, 0, 0);
     }

   if (status == -1)
     perror_with_name ("ttrace:");
}
/*----------------------------------------------------------------------------*/
/* Description: get_inf_si_code() returns the si_code associated with the signal
 *              that has currently stopped the inferior.  Fix for JAGae69869.
 * Input:       -
 * Output:      Valid si_code for the signal received by the inferior.
 * Globals:     inferior_pid is used to relied upon for the pid of the inferior.
 * Notes:
 * Author:      Bharath, Apr 24 2003.
 */
int 
get_inf_si_code (void)
{
  thread_info *tinfo;
  int res;

  /* Can't get a signal for inferior when there is none!
  if (inferior_pid == 0);
    error ("get_inf_si_code(): inferior_pid can't be zero.");
   */

  tinfo = find_thread_info (map_from_gdb_tid (inferior_pid));

  /* If this thread is stopped due to TTEVT_SYSCALL_RETURN event,
     Just return the si_code of 0 since we do not know the si_code
     here. */
  if (tinfo->last_stop_state.tts_event & TTEVT_SYSCALL_RETURN)
    return 0;

  return tinfo->last_stop_state.tts_u.tts_signal.tts_siginfo.si_code;
}
/*----------------------------------------------------------------------------*/
/* Description: map_si_code_tostr() returns a string message for a given si_code.
 *		This is very OS specific even between HP-UX for PA and IA.  It
 *		relies on si_code_map[] array that is defined separately 
 *		for PA and IA.  The message in each array has be obtained from 
 *		siginfo.h where they exist in comment form.  Fix for JAGae99882.
 * Input:       signal - signal number
 *		si_code - si_code for the specified signal.
 * Output:      A string corresponding to the si_code for the specified signal.
 * Globals:     -
 * Notes:	If there are any changes to si_codes and corresponding strings 
 *		in siginfo.h they have to be manually reflected here.
 * Author:      Bharath, 9 Feb 2004.
 */
char *
map_si_code_to_str (int signal, int si_code)
{
  int i;
  extern const int si_code_map_size;
  extern struct target_si_code_t si_code_map[];

  for (i = 0; i < si_code_map_size; i++)
    if (signal == si_code_map[i].signal && si_code == si_code_map[i].si_code)
      return si_code_map[i].si_code_str;

  /* If we don't get a match, the si_code is probably invalid.  Just print
     a blank instead of something ominous and wrong.  Fix for JAGaf46892,
     "Unknown si_code reported for certain signals". */
  return "";
}
