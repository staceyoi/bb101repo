
/* Defines helper functions that read java frame unwind info and 
   java method symbol info from the target Java program or core 
   file space, and then provides the info to GDB. */

#include <defs.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <values.h>
#include <dl.h>
#include "command.h"
#include "gdbcmd.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "demangle.h"
#include "gdbcore.h"
#include "expression.h" /* For language.h */
#include "language.h"
#include "symfile.h"
#include "objfiles.h"
#include "target.h"	/* for target_read_memory() prototype */
static int java_evaluate_expression_2 (PTR arg);

/* Function for evaluating an expression, protected with a catch error*/
/* returns 1 on success, 0 on failure */
int 
java_evaluate_expression_1 (char *name, unsigned long long *reg)
{
  struct expression *expr;
  register value_ptr val;
  register struct cleanup *old_chain = 0;
  unsigned long long ret=0LL;
  int length;    
  expr = parse_expression (name);
  old_chain = make_cleanup (free_current_contents, &expr);
  val = evaluate_expression (expr);
  do_cleanups (old_chain);
  if (VALUE_LAZY (val))
    value_fetch_lazy (val);    /* Actually fetch it */

  /* Align and save into *reg */
  length = TYPE_LENGTH(VALUE_TYPE(val));    
  if ((length > 8) || (length <0))
    return 0;	
  length=max(0,min(8,length));
  bcopy(((char *) &val->aligner.contents[0]), (((char *) &ret)+(8-length)), 
    length );
  *reg=ret;
  return 1;
}

typedef struct
{
  char *name;
  unsigned long long *reg;
}
  args_for_java_evaluate_expression;

int 
java_evaluate_expression (char *name, unsigned long long *reg)
{
  args_for_java_evaluate_expression args;
  args.name=name;
  args.reg=reg;
  return catch_errors (java_evaluate_expression_2, &args,
    NULL, RETURN_MASK_ALL);
}

static int
java_evaluate_expression_2 (PTR arg)
{
  args_for_java_evaluate_expression *args = arg;
  return java_evaluate_expression_1 (args->name, args->reg);
}

/* Decide if the JVM is a debug version: Look for debug information */
int
is_java_debug_version()
{
  struct symbol* ret;
  unsigned long long val;
  ret = lookup_symbol("JVMData",0, STRUCT_NAMESPACE,0,0);
  if (ret==0) 
    return 0 ;
  return 1;
}

/* The general function for reading target memory. Use catch errors for 
   reliability. Return 1 for success, 0 for failure. */

static int java_read_memory_2 (PTR arg);
typedef struct
{
  CORE_ADDR memaddr;
  char *myaddr;
  int len;
}
  args_for_read_memory;

int 
java_read_memory (CORE_ADDR memaddr, char *myaddr, int len)
{
  args_for_read_memory args;
  args.memaddr=memaddr;
  args.myaddr=myaddr;
  args.len=len;
  return catch_errors (java_read_memory_2, &args, NULL, RETURN_MASK_ALL);
}

static int
java_read_memory_2 (PTR arg)
{

  args_for_read_memory *args = arg;
  return (!target_read_memory (args->memaddr, args->myaddr, args->len));
}
