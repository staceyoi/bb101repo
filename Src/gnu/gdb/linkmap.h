#ifndef LINKMAP_INCLUDED
#define LINKMAP_INCLUDED

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LM_INVALID_IDX (-1)

/* BSS symbol information structure */
typedef struct {
	uint32_t	file_index;
	uint32_t	sym_index;
	unsigned int	is_tls:1;
	unsigned int	reserved:31;
} LM_sym_info;

/* Mapping types */
typedef enum {
	LM_INPUT_SECTION,
	LM_LINKER_SECTION,
	LM_LINKER_BSS,
	LM_DATA_STRUCTURE,
	LM_UNDEF_REGION,
	LM_FILE_PADDING,
	LM_GENERIC_PADDING,
	LM_MARK_GENERIC_PADDING_START,
	LM_FILE_DEF,
	LM_END
} LM_maptype;

/* Data structure types */
typedef enum {
    DATA_STRUCT_COMPILATION_UNIT_DICT = 1
} LM_data_struct_type;

/* Mmapping inforamation structure */
typedef struct {
	uint32_t	file_index;
	LM_maptype	mapping_type;
	uint32_t	output_info;
	uint32_t	input_info;
	uint64_t	size;
	uint64_t	offset;
} LM_mapping_info;


/* File entry information structure */
typedef struct {
	uint32_t	link_dir;
	uint32_t	dir_name;
	uint32_t	file_name;
	unsigned int	is_archive:1;
	unsigned int	is_basename:1;
	unsigned int	reserved:30;
} LM_file_entry_info;

/* Error codes */
typedef enum {
	LM_NO_ERROR = 0,
	LM_OUT_OF_MEMORY,
	LM_INCOMPLETE_STREAM,
	LM_USER_CALLBACK_NULL,
	LM_INCORRECT_MAPPING_TYPE,
	LM_LINKMAP_NOT_INITIALIZED,
	LM_CANT_READ_HEADER,
	LM_CANT_WRITE_HEADER,
	LM_INCORRECT_VERSION,
	LM_INCORRECT_INTERNAL_VERSION,
	LM_INDEX_OUTOF_RANGE,
	LM_INVALID_STREAM
} LM_errcode;


/* Function prototypes */
LM_errcode LM_begin_read_linkmap (char* (*linkmap_buffer)  
				  (unsigned int* size));
LM_errcode LM_begin_read_linkmap_bss (char* (*linkmap_bss_buffer) 
				  (unsigned int* size));
LM_errcode LM_begin_read_linkmap_file (char* (*linkmap_file_buffer) 
				  (unsigned int* size));
LM_errcode LM_begin_read_linkmap_file2 (char* (*linkmap_file_buffer) 
				  (unsigned int* size),
				  char* lm_hdr);

LM_errcode LM_end_read_linkmap (void);
LM_errcode LM_end_read_linkmap_bss (void);
LM_errcode LM_end_read_linkmap_file (void);

LM_errcode LM_get_next_bss_symbol (LM_sym_info* symbol);
LM_errcode LM_get_next_mapping (LM_mapping_info* mapping);
LM_errcode LM_get_file_entry (unsigned int index, LM_file_entry_info* file);
LM_errcode LM_get_num_files (unsigned int* num);

LM_errcode LM_begin_write_linkmap (char* (*linkmap_buffer) 
				  (unsigned int* size));
LM_errcode LM_begin_write_linkmap_bss (char* (*linkmap_bss_buffer) 
				  (unsigned int* size));
LM_errcode LM_begin_write_linkmap_file (char* (*linkmap_file_buffer) 
				  (unsigned int* size));

LM_errcode LM_end_write_linkmap (int* size);
LM_errcode LM_end_write_linkmap_bss (int* size);
LM_errcode LM_end_write_linkmap_file (int* size);

LM_errcode LM_put_file_entry (LM_file_entry_info* file);
LM_errcode LM_put_end_file_entry(void);
LM_errcode LM_put_bss_symbol (LM_sym_info* sym); 
LM_errcode LM_put_mapping (LM_mapping_info* mapping);
LM_errcode LM_pad_op_null (int count, char* buffer);
char* LM_get_linkmap_hdr(void);
void LM_dump_map(LM_mapping_info* info);

#ifdef __cplusplus
}
#endif

#endif /* LINKMAP_INCLUDED */
