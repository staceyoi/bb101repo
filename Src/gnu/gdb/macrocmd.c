/* gdb interface for macro commands.

   Copyright (C) 2002 Free Software Foundation, Inc.
   Contributed by Red Hat, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

#include "defs.h"
#include "macrotab.h"
#include "macroexp.h"
#include "macroscope.h"
#include "command.h"
#include "gdbcmd.h"

/* The `macro' prefix command.  */
static struct cmd_list_element *macrolist;

/* Macro expansion command. 
 * For an object like macro, this command expands the macro definitions if there are any
 * otherwise the behavior is similar to "macro show defn" command.
 * For a function like macro, this command substitutes the parameters with the passed values. 
 */
static void
macro_expand_command (char *exp, int from_tty)
{
  struct macro_scope *ms = NULL;
  char *expanded = NULL;
  struct cleanup *cleanup_chain = make_cleanup (free_current_contents, &ms);
  make_cleanup (free_current_contents, &expanded);

  if (! exp || ! *exp)
    error ("You must follow the `macro expand' command with the expression you\n"
           "want to expand.");

  /* Get the spos info. */
  ms = default_macro_scope ();
  if (ms)
    {
      expanded = macro_expand (exp, standard_macro_lookup, ms);
      fputs_filtered ("expands to: ", gdb_stdout);
      fputs_filtered (expanded, gdb_stdout);
      fputs_filtered ("\n", gdb_stdout);
    }
  else
    fputs_filtered ("GDB has no preprocessor macro information for that code.\n",
		     gdb_stdout);

  do_cleanups (cleanup_chain);
  return;
}

static void
macro_command (char *arg, int from_tty)
{
  printf_unfiltered
    ("\"macro\" must be followed by the name of a macro command.\n");
  help_list (macrolist, "macro ", -1, gdb_stdout);
}

/* Helper function used to print the macro information. 
 */
static void
show_pp_source_pos (struct ui_file *stream,
                    struct macro_source_file *file,
                    int line)
{
  fprintf_filtered (stream, "%s:%d\n", file->filename, line);

  while (file->included_by)
    {
      fprintf_filtered (gdb_stdout, "  included at %s:%d\n",
                        file->included_by->filename,
                        file->included_at_line);
      file = file->included_by;
    }
}

/* macro show <MACRO NAME> -- displays the definition of the macro. 
 */
static void
info_macro_command (char *name, int from_tty)
{
  struct macro_scope *ms = NULL;
  struct cleanup *cleanup_chain = make_cleanup (free_current_contents, &ms);
  struct macro_definition *d;
  
  if (! name || ! *name)
    error ("You must follow the `info macro' command with the name"
           " of the macro\n"
           "whose definition you want to see.");

  /* Get the default macro source file corresponding to the current spos. */
  ms = default_macro_scope ();
  if (! ms)
    error ("GDB has no preprocessor macro information for that code.");

  d = find_definition (name, ms->file, ms->line);
  if (d)
    {
      fprintf_filtered (gdb_stdout, "Defined at ");
      show_pp_source_pos (gdb_stdout, d->start_file, d->start_line);
      fprintf_filtered (gdb_stdout, "#define %s", name);

      /* Print the args if its a function like macro. */
      if (d->type == macro_function_like)
        {
          int i;

          fputs_filtered ("(", gdb_stdout);
          for (i = 0; i < d->argc; i++)
            {
              fputs_filtered (d->argv[i], gdb_stdout);
              if (i + 1 < d->argc)
                fputs_filtered (", ", gdb_stdout);
            }
          fputs_filtered (")", gdb_stdout);
        }
      fprintf_filtered (gdb_stdout, " %s\n", d->replacement);
    }
  else
    {
      fprintf_filtered (gdb_stdout,
                        "The macro `%s' has no definition in the current"
                        " scope.\n", name);
    }
    

  do_cleanups (cleanup_chain);
}

/* Initializing the `macrocmd' module.  */
extern initialize_file_ftype _initialize_macrocmds; /* -Wmissing-prototypes */

void
_initialize_macrocmds (void)
{
  struct cmd_list_element *c;

  /* We introduce a new command prefix, `macro', under which we'll put
   * the various commands for working with preprocessor macros.  
   */
  add_prefix_cmd ("macro", class_info, macro_command,
		  "Prefix for commands dealing with C preprocessor macros.",
		  &macrolist, "macro ", 0, &cmdlist);

  /* expand macro <macro name> 
   * This command will expand the macro name to its definition and substitutes the 
   * parameters and expands the macro definitions if there are any.
   * This command will expand macros inside macros recursively until we get an expr
   * that is free from macros.
   */
  add_cmd ("expand", no_class, macro_expand_command, "\
Fully expand any C/C++ preprocessor macro invocations in EXP.\n\n\
Usage:\n\tmacro expand <EXP>\n\n\
Show the expanded expression.",
	   &macrolist);
  add_alias_cmd ("exp", "expand", no_class, 1, &macrolist);

  /* macro show <macro name> and/or info macro <macro name> 
   * This command displays the definition of macro definition provided the definition 
   * exists in that scope. 
   */
  add_cmd ("show", no_class, info_macro_command,
	   "Show the definition of MACRO, and its source location.\n\n\
Usage:\n\tmacro show <MACRO>\n",
	   &macrolist);
  add_cmd ("macro", no_class, info_macro_command,
           "Show the definition of MACRO, and its source location.\n\n\
Usage:\n\tinfo macro <MACRO>\n",
           &infolist);

#if 0
  /* macro list 
   * This command is not yet implemented. This command should basically list all the
   * macro names and their definitions that prevail in the scope. 
   */
  add_cmd ("list", no_class, macro_list_command,
	   "List all the macros defined using the `macro define' command.",
	   &macrolist);
#endif
}
