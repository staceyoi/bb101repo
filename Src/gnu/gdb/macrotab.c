/* C preprocessor macro tables for GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

#include <assert.h>
#include "defs.h"
#include "symtab.h"
#include "objfiles.h"

#include "macrotab.h"
#include "macroscope.h"
#include "fastsym.h"

/* Judy table initialization and error handling. */
static JError_t jerror_t = {0};
static PJError_t PJError = &jerror_t;

/* We handle Judy errors in this routine. */
static void
handle_error (PJError_t PJError)
{
  JU_Errno_t err_num = JU_ERRNO (PJError);
  error ("Judy Error errno = %d", err_num);
}

/* This routine returns a pointer to a malloc'd area.
 * This malloc'd area is then used to keep a pointer to the Judy table.
 * The pointer to the table changes with every insert 
 * that you make into Judy.
 */
void **
init_judytable(struct objfile *objfile, long init_size)
{
  if (!objfile)
    error ("Objfile not found");
  PPvoid_t PPArray = (void **) obstack_alloc (&objfile->symbol_obstack, init_size);
  *PPArray = NULL;

  return PPArray;
}

/* Judy table array holder. When a key is supplied to the Judy array, it returns a ptr of
   the following type if one exists. */
struct macro_defn_list
{
  struct macro_definition *def;
  struct macro_defn_list *next;
};
  
void
add_macro_definition (struct macro_source_file *source, int line,
                      const char *name, int argc, char **argv,
		      const char *replacement, enum macro_type type)
{
  struct macro_definition *d;
  d = (struct macro_definition *) xcalloc (1, sizeof (struct macro_definition));

  d->macro_name = xstrdup (name);
  d->replacement = xstrdup (replacement);
  d->type = type;

  d->start_file = source;
  d->start_line = line;

  d->end_file = NULL;
  d->end_line = -1;

  if (type == macro_function_like)
    {
      d->argv = argv;        
      d->argc = argc;
    }
  else
    {
      /* In case of macro object like, the args doesn't mean anything. Just 
	 initialize them to NULL. */
      d->argv = NULL;	
      d->argc = -1;  	
    } 

  /* Insert the above framed macro defintion into the judy array. */
  insert_macro_into_table (source->table->judy_table, d); 
}

/* Insert the macro defn mac_value into the Judy table.
 * Before insertion check if the value is already present in the judy array. If present, 
 * check for the scope of all the macros definitions linearly. To add a new defn to such
 * a overflow chain, the end_src_file and end_line should be filled for all the other macro defns. 
 * If any of the macro defns are incomplete wrt the above fields, issue a warning but still add 
 * that defn to the overflow chain. Adding this defn doesnt make any sense because the replacement 
 * string of this defn will never be fetched.
 */  

void
insert_macro_into_table (void **table, struct macro_definition *mac_value)
{
  Pvoid_t PArray = (Pvoid_t) (*table);
  struct macro_defn_list *array_holder = NULL, **ptr;  

  /* Create an array holder and insert the values into it. */
  array_holder = (struct macro_defn_list *) xcalloc (1, sizeof(struct macro_defn_list));
  array_holder->def = mac_value;
  array_holder->next = NULL;

  /* First check if the key already exists. If the value exists, JudySLIns returns a ptr
     to the value. Else, it returns NULL. */
  ptr = (struct macro_defn_list **) JudySLIns (&PArray, mac_value->macro_name, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);

  *table = PArray;

  /* Judy insert initializes the value area with a NULL Pointer if nothing already present */
  if (ptr && (*ptr == NULL))
    {
      /* Fresh insertion */
      *JudySLIns (&PArray, mac_value->macro_name, PJError) = array_holder;
      if (JU_ERRNO(PJError))
        handle_error (PJError);
    } 
  /* Got a ptr to old value; insert the new value at the end of list. */
  else
    {
      struct macro_defn_list *temp = *ptr;
      while (temp->next)
	temp = temp->next;
      temp->next = array_holder;
    }
}

/*
 * Retrieve the macro defn from the judy table.
 * 
 * The parameters to the function
 * table - is a pointer to the Judy array.
 * key - is the name of the macro to search for in the table. 
 * file - is the current source file the user has stopped in.
 * line - is the line number where the user has stopped.
 * 
 * The function returns a NULL if nothing found else returns the macro definition 
 * that matches the scope info passed to this argument . If it didn't find an exact 
 * match for the current scope it returns NULL.
 */

struct macro_definition* 
get_macro (void **table, char *key, struct macro_source_file *file, int line)
{
  struct macro_defn_list **PPValue = NULL;
  Pvoid_t PArray = (Pvoid_t) (*table);

  if (!key)
    error ("Macro name to be searched is not specified.");
	    
  if (! *table)
    error ("There is no macro information in the table.");

  /* Search the macro table for the key. On successful retrieval, it returns a ptr to
     to the defn else it returns NULL */
  PPValue = (struct macro_defn_list **) JudySLGet (PArray, key, PJError);
  if(PPValue && (*PPValue == NULL)) 
  /* Macro not found, return. Error message will be printed by the caller. */
    return 0;
	
  /* Store the ptr to the macro defn in val. */
  struct macro_defn_list *val = NULL;
  val = (struct macro_defn_list *) *PPValue;
	
  /* Check whether there exists multiple defns for this macro definition. If there
   * are multiple definitions, select the appropriate one using macro_scope info. 
   */
  struct macro_definition *def;
  while (val)  
    {
      int line1, line2, endline=-1, line2bkp = -1;
      struct macro_source_file *file1, *file2, *endfile = NULL, *file2bkp = NULL;
      def = val->def;

      /*
       * We want to treat positions in an #included file as coming *after*
       * the line containing the #include, but *before* the line after the
       * include.  As we walk up the #inclusion tree toward the main
       * source file, we update fileX and lineX as we go; includedX
       * indicates whether the original position was from the #included
       * file.  
       */
     
      file1 = def->start_file;
      file2 = file;
      if (def->end_file) {
	file2bkp = file2;
        endfile = def->end_file;
      }
  
      line1 = def->start_line;
      line2 = line;
      if (def->end_line != -1) {
        endline = def->end_line;
	line2bkp = line2;
      }

      if (file1 != file2)
        {
  	  /* If one file is deeper than the other, walk up the #inclusion
           * chain until the two files are at least at the same *depth*.
           * Then, walk up both files in synchrony until they're the same
           * file.  That file is the common ancestor.  
	   */
          int depth1 = inclusion_depth (file1);
          int depth2 = inclusion_depth (file2);

          /* Only one of these while loops will ever execute in any given
           * case.  
	   */
          while (depth1 > depth2)
            {
              line1 = file1->included_at_line;
              file1 = file1->included_by;
              depth1--;
            }
          while (depth2 > depth1)
            {
              line2 = file2->included_at_line;
              file2 = file2->included_by;
              depth2--;
            }


          /* Now both file1 and file2 are at the same depth.  Walk toward
           * the root of the tree until we find where the branches meet.  
  	   */
          while (file1 != file2)
            {
              line1 = file1->included_at_line;
              file1 = file1->included_by;
              /* At this point, we know that the case the includedX flags
                 are trying to deal with won't come up, but we'll just
                 maintain them anyway.  */

              line2 = file2->included_at_line;
              file2 = file2->included_by;

              assert (file1 && file2);
            }
	}

      if (endfile && (endline != -1) && (endfile != file2bkp))
        {
	  int depth3 = inclusion_depth (endfile);
	  int depth2bkp = inclusion_depth (file2bkp);

          while (depth3 > depth2bkp)
            {
              endline = endfile->included_at_line;
              endfile = endfile->included_by;
              depth3--;
            }
          while (depth2bkp > depth3)
            {
              line2bkp = file2bkp->included_at_line;
              file2bkp = file2bkp->included_by;
              depth2bkp--;
            }
	  while (file2bkp != endfile)
     	    {
	      line2bkp = file2bkp->included_at_line;
	      file2bkp = file2bkp->included_by;
	
	      endline = endfile->included_at_line;
	      endfile = endfile->included_by;
	    }
        }

/* Now then we have three linenumbers (logically) in the same file. Check if the current scope line
 * number falls between the start and end of a macro definition. If it belongs to the range, return
 * the macro definition. Otherwise, check if there exists a overflow chain for that macro. If exists
 * repeat the same above procedure. If there is no overflow chain, return NULL.
 */
     /* Success condition 1. */
     if (endline != -1 && line2 > line1 && line2 < endline)
       return def;
     /* Success condition 2. */
     else if (endline == -1 && !endfile && (file1 == file2) && (line2 >= line1))
       return def;
     /* Travesre the overflow chain. */
     else if (val->next)
       val = val->next;
     /* If no matching macro definition is found, return NULL. */
     else
       return NULL;  
    } /* End while loop. */

  /* If no matching macro definition is found, return NULL. */
  return NULL;
}
  
/* Allocation and freeing of macro datastructures. */

/* Allocate and initialize a new source file structure.  */
static struct macro_source_file *
new_source_file (struct macro_table *t, const char *filename)
{
  /* Get space for the source file structure itself.  */
  struct macro_source_file *f = xcalloc (1, sizeof (struct macro_source_file));
  f->table = t;
  f->filename = xstrdup (filename);

  return f;
}

/* Free a source file, and all the source files it #included.  */
static void
free_macro_source_file (struct macro_source_file *src)
{
  struct macro_source_file *child, *next_child;

  /* Free this file's children.  */
  for (child = src->includes; child; child = next_child)
    {
      next_child = child->next_included;
      free_macro_source_file (child);
    }

  free (src->filename);
  free (src);
}

/* Return the #inclusion depth of the source file FILE.  This is the
   number of #inclusions it took to reach this file.  For the main
   source file, the #inclusion depth is zero; for a file it #includes
   directly, the depth would be one; and so on.  */
static int
inclusion_depth (struct macro_source_file *file)
{
  int depth;

  for (depth = 0; file->included_by; depth++)
    file = file->included_by;

  return depth;
}

struct macro_source_file *
macro_set_main (struct macro_table *t, const char *filename)
{
  /* You can't change a table's main source file. */ 
  assert (! t->main_source);

  /* Add the new source file as the main_source of the CU. */
  t->main_source = new_source_file (t, filename);

  return t->main_source;
}


struct macro_source_file *
macro_main (struct macro_table *t)
{
  assert (t->main_source);
  return t->main_source;
}


struct macro_source_file *
macro_include (struct macro_source_file *source,
               int line,
               const char *included)
{
  struct macro_source_file *new;
  struct macro_source_file **link;

  /* Find the right position in SOURCE's `includes' list for the new
     file.  Skip inclusions at earlier lines, until we find one at the
     same line or later --- or until the end of the list.  */
  for (link = &source->includes;
       *link && (*link)->included_at_line < line;
       link = &(*link)->next_included)
    ;

  /* Did we find another file already #included at the same line as
     the new one?  */
  if (*link && line == (*link)->included_at_line)
    {
      /* First complain and then insert. */
      complain (&macro_general_complaint,
		 "both `%d' and `%d' allegedly #included at %d:%d", included,
		 (*link)->filename, source->filename, line);

      /* Now, choose a new, unoccupied line number for this
         #inclusion, after the alleged #inclusion line.  */
      while (*link && line == (*link)->included_at_line)
        {
          /* This line number is taken, so try the next line.  */
          line++;
          link = &(*link)->next_included;
        }
    }

  /* At this point, we know that LINE is an unused line number, and
     *LINK points to the entry an #inclusion at that line should
     precede.  */
  new = new_source_file (source->table, included);
  new->included_by = source;
  new->included_at_line = line;
  new->next_included = *link;
  *link = new;

  return new;
}


struct macro_source_file *
macro_lookup_inclusion (struct macro_source_file *source, const char *name)
{
  /* Is SOURCE itself named NAME?  */
  if (strcmp (name, source->filename) == 0)
    return source;

  /* The filename in the source structure is probably a full path, but
     NAME could be just the final component of the name.  */
  {
    int name_len = strlen (name);
    int src_name_len = strlen (source->filename);

    /* We do mean < here, and not <=; if the lengths are the same,
       then the strcmp above should have triggered, and we need to
       check for a slash here.  */
    if (name_len < src_name_len
        && source->filename[src_name_len - name_len - 1] == '/'
        && strcmp (name, source->filename + src_name_len - name_len) == 0)
      return source;
  }
  /* It's not us.  Try all our children, and return the lowest.  */
  {
    struct macro_source_file *child;
    struct macro_source_file *best = NULL;
    int best_depth = 0;

    for (child = source->includes; child; child = child->next_included)
      {
        struct macro_source_file *result
          = macro_lookup_inclusion (child, name);

        if (result)
          {
            int result_depth = inclusion_depth (result);

            if (! best || result_depth < best_depth)
              {
                best = result;
                best_depth = result_depth;
              }
          }
      }
    return best;
  }
}

/* Find the macro defintion in the Judy table for the definition of NAME at LINE in
   SOURCE, or zero if there is none.  */
struct macro_definition *
find_definition (char *name, struct macro_source_file *src, int line)
{
  struct macro_table *t = src->table;
  struct macro_definition *def = get_macro (t->judy_table, name, src, line);
  return def;
}

/* Find the macro definition of NAME and update the endfile and endline fields of that
 * definition.  If no matching definition is found, then the compiler has emitted a bogus
 * undef, just squeak.
 */
void
macro_undef (struct macro_source_file *source, int line, char *name)
{
  /* This fun should actually find the appropriate macro definition and return. */
  struct macro_definition *def = find_definition (name, source, line);

  if (def) 
    {
      def->end_file = source;
      def->end_line = line;
    }
  else
    complain (&macro_general_complaint,
		 "No macro definition found for %s", name);
}

/* Creating and freeing macro tables.  */
struct macro_table *
new_macro_table (struct objfile *objfile)
{
  struct macro_table *t;
  PPvoid_t jtable;

  t = (struct macro_table *) xcalloc (1, sizeof (struct macro_table));
  t->main_source = NULL;
  t->judy_table = (void **) init_judytable (objfile, sizeof(void **));

  return t;
}

/* Free macro definition list. */
void
free_macro_defn (struct macro_definition *def)
{
  free (def->macro_name);
  free (def->replacement);  
   
  /* Free the arguments if the definition is a function type. */
  if (def->type == macro_function_like)
    {
      for (int i = 0; i < def->argc; i++)
	free (def->argv[i]);
	
      free (def->argv);
    }
}

/* Free the Judy array and the overflow chain. */

void
free_judy_table (void **table)
{
  struct macro_defn_list **PPValue = NULL;
  char *key = NULL;
  struct macro_defn_list *val = NULL;

  key = long_name (STRING_SIZE);
  strcpy (key, "");

  /* Free the Over flow chain. */
  PPValue = (struct macro_defn_list **) JudySLFirst (*table, key, PJError);
  if (JU_ERRNO(PJError))
    handle_error (PJError);
  while (PPValue && *PPValue) 
    {
      val = (struct macro_defn_list *) *PPValue;
      /* Free the definition and the ptr. */
      free_macro_defn (val->def);
      free (val->def);
      /* Free the overflow chain, if it exists. */
      while (val->next)
        {
	  free_macro_defn (val->def);
	  free (val->def);
	  val = val->next;
	}
      PPValue = (struct macro_defn_list **) JudySLNext (*table, key, PJError);
      if (JU_ERRNO(PJError))
        handle_error (PJError);
     }
 
  /* Free the key holder also. */
  free (key);

  /* JudySLFreeArray() does not return memory to the
     operating system, but holds it for other Judy use. */
  /* Free the Judy array */
  if (table)
    (void) JudySLFreeArray (table, PJError);
  table = NULL;
}

void
free_macro_table (struct macro_table *table)
{
  /* Free the source file tree.  */
  free_macro_source_file (table->main_source);

  /* Free the Judy table of macro definitions.  */
  free_judy_table (table->judy_table);
}

