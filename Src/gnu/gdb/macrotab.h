#ifndef MACROTAB_H
#define MACROTAB_H

#include "../../../Src/gnu/Judy/src/Judy.h"
#include "macroscope.h"
#include "complaints.h"

struct macro_table
{
  /* The main source file for this compilation unit --- the one whose
     name was given to the compiler.  This is the root of the
     #inclusion tree; everything else is #included from here.  */
  struct macro_source_file *main_source;

  /* Judy table to hold the macro definitions. */
  PPvoid_t judy_table;
};

struct macro_source_file
{
  /* The macro table for the compilation unit this source location is
     a part of.  */
  struct macro_table *table;

  /* A source file --- possibly a header file.  */
  char *filename;

  /* The location we were #included from, or zero if we are the
     compilation unit's main source file.  */
  struct macro_source_file *included_by;

  /* If `included_by' is non-zero, the line number in that source
     file at which we were included.  */
  int included_at_line;

  /* Head of a linked list of the source files #included by this file;
     our children in the #inclusion tree.  This list is sorted by its
     elements' `included_at_line' values, which are unique. */
  struct macro_source_file *includes;

  /* The next file #included by our `included_from' file; our sibling
     in the #inclusion tree.  */
  struct macro_source_file *next_included;
};

/* Different types of macro definitions. */
enum macro_type
{
  macro_object_like,
  macro_function_like
};

struct macro_definition
{
  /* The name of the macro.  This is in the table's bcache, if it has
     one. */
  char *macro_name;

  /* What kind of macro it is.  */
  enum macro_type type;

  /* If `kind' is `macro_function_like', the number of arguments it
     takes, and their names.  The names, and the array of pointers to
     them, are in the table's bcache, if it has one.  */
  int argc;
  char **argv;

  /* The replacement string (body) of the macro.  This is in the
     table's bcache, if it has one.  */
  char *replacement;

  /* The source file and line number where the definition's scope
     begins.  This is also the line of the definition itself.  */
  struct macro_source_file *start_file;
  int start_line;

  /* The first source file and line after the definition's scope.
     (That is, the scope does not include this endpoint.)  If end_file
     is zero, then the definition extends to the end of the
     compilation unit.  */
  struct macro_source_file *end_file;
  int end_line;
};
    
/* Functions */

/* Create a new macro table. */
struct macro_table *new_macro_table (struct objfile *);

/* Free TABLE, and any macro definitions, source file structures,
   etc. it owns. */  
void free_macro_table (struct macro_table *);

/* Add the macro definition into the macro table. If the macro definition
   already exists, add it to the overflow chain.  */
void 
insert_macro_into_table (void **, struct macro_definition *);

/* 
 * Function to retrieve the macro definition from the Judy table. If there
 * are multiple definitions for the same name, select the appropriate one
 * by considering the scope info. 
 */
struct macro_definition* 
get_macro (void **, 
	   char *, 
	   struct macro_source_file *,
	   int);

/* Create macro definition object with the paramaters sent to this function.
 * argc and argv arguments are valid only in the case of function like macro. 
 * For an object like macro, the values will be set to -1 and NULL respectivly.
 * After creating the defintion object, insert into the Judy array.
 */
void
add_macro_definition (struct macro_source_file *, 
		      int line, const char *, 
		      int , char **,
		      const char *, enum macro_type);


/* Function will find the depth of the file in the source file tree.
 * This function is useful while fetching the macro defn structure.
 */
static int
inclusion_depth (struct macro_source_file *);

/* Set FILENAME as the main source file of TABLE.  Return a source
   file structure describing that file; if we record the #definition
   of macros, or the #inclusion of other files into FILENAME, we'll
   use that source file structure to indicate the context.

   The "main source file" is the one that was given to the compiler;
   all other source files that contributed to the compilation unit are
   #included, directly or indirectly, from this one.

   The macro table makes its own copy of FILENAME; the caller is
   responsible for freeing FILENAME when it is no longer needed.  */
struct macro_source_file *macro_set_main (struct macro_table *, const char *);

/* Return the main source file of the macro table TABLE.  */
struct macro_source_file *macro_main (struct macro_table *table);

struct macro_definition *
find_definition (char *name,
                 struct macro_source_file *file,
                 int line);

/* Record an #inclusion.
   Record in SOURCE's macro table that, at line number LINE in SOURCE,
   we #included the file INCLUDED.  Return a source file structure we
   can use for symbols #defined or files #included into that.  If we've
   already created a source file structure for this #inclusion, return
   the same structure we created last time.

   The first line of the source file has a line number of 1, not 0.

   The macro table makes its own copy of INCLUDED; the caller is
   responsible for freeing INCLUDED when it is no longer needed.  */
struct macro_source_file *macro_include (struct macro_source_file *, int, const char *);

/* Record an #undefinition.
   Record in SOURCE's macro table that, at line number LINE in SOURCE,
   we removed the definition for the preprocessor symbol named NAME.  */
void macro_undef (struct macro_source_file *source, int line, char *name);

/* Find any source file structure for a file named NAME, either
   included into SOURCE, or SOURCE itself.  Return zero if we have
   none.  NAME is only the final portion of the filename, not the full
   path.  e.g., `stdio.h', not `/usr/include/stdio.h'.  If NAME
   appears more than once in the inclusion tree, return the
   least-nested inclusion --- the one closest to the main source file.  */
struct macro_source_file *(macro_lookup_inclusion
                           (struct macro_source_file *source,
                            const char *name));

static struct complaint macro_general_complaint =
{
  "%s", 0, 0
};

#endif /* MACROTAB_H */
