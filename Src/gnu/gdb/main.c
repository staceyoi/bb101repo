/* Top level stuff for GDB, the GNU debugger.
   Copyright 1986-1995, 1999-2000 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


/* Baskar,
   _PSTAT64 must be defined to use 64-bit pstat structures in our 32-bit
application even if the underlying kernel is 32-bit. (It's done implicitly
for 64-bit application compiles).  It should have no other side effects on
gdb other than on the pstat related code. */

#define _PSTAT64	1

#include "defs.h"
#include "top.h"
#include "target.h"
#include "inferior.h"
#include "call-cmds.h"
#include <signal.h>

#include "getopt.h"

#include <sys/types.h>
#include "gdb_stat.h"
#include <ctype.h>
#include <libgen.h>

#include "gdbcmd.h"

#include "gdb_string.h"
#ifndef NO_SYS_FILE
#include <sys/file.h>
#include <sys/stat.h>
#endif
#ifdef HP_IA64
#include <unwind.h>
#endif
#include "event-loop.h"
#include "ui-out.h"
#if defined (TUI) || defined (GDBTK)
/* FIXME: cagney/2000-01-31: This #include is to allow older code such
   as that found in the TUI to continue to build. */
#include "tui/tui-file.h"
#endif

#include <assert.h>

#ifdef SEND_BETA_MAIL_ADDR
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
static char** save_argv;
#endif

/* JAGag06697 */
#include <dl.h>
#include <sys/utsname.h>
#include <sys/pstat.h>

#define DEBUG(x)

/* If nonzero, display time usage both at startup and for each command.  */

int display_time;

/* If nonzero, display space usage both at startup and for each command.  */

int display_space;

/* Whether this is the async version or not.  The async version is
   invoked on the command line with the -nw --async options.  In this
   version, the usual command_loop is substituted by and event loop which
   processes UI events asynchronously. */
int event_loop_p = 1;

#ifdef UI_OUT
/* Has an interpreter been specified and if so, which. */
char *interpreter_p;
#endif

/* Whether this is the command line version or not */
int tui_version = 0;
int nimbus_version;
int firebolt_version = 0;
int profile_test_on;

extern int live_core;
int check_heap = 0;
extern int thr_non_recursive_relock;
extern int thr_unlock_not_own;
extern int thr_mixed_sched_policy;
extern int thr_condvar_multiple_mutexes;
extern int thr_condvar_wait_no_mutex;
extern int thr_deleted_obj_used;
extern int thr_thread_exit_own_mutex;
extern int thr_thread_exit_no_join_detach;
extern int thr_stack_utilization; /* %, 0 == disabled */
extern int thr_num_waiters; /* 0 == disabled */
extern int thr_ignore_sys_objects; /* 0 == disabled */
/* is 'set thread-check' set? */
int trace_threads = 0;

/* Used by batch rtc so that gdb does not print any message while 
   extracting leaks/heap information for batch rtc.
*/
int batch_rtc = 0; 

/* Set to true if we see -old_abi option */

static int old_abi_option = 0;

/* srikanth, 001115, if gdb is invoked with the command line option
   "-finalize_prompt" then silently ignore any set prompt commands.
   This is useful for debugging gdb with gdb.
*/
 
int prompt_finalized = 0;

/* Whether xdb commands will be handled */
int xdb_commands = 0;

int mapshared = 0;     /* If true, do not map all shared libraries private. */

int debugging_aries = 0;
int debugging_aries64 = 0;
int debugging_gdb = 0;

/* Whether dbx commands will be handled */
int dbx_commands = 0;

struct ui_file *gdb_stdout;
struct ui_file *gdb_stderr;
struct ui_file *gdb_stdlog;
struct ui_file *gdb_stdtarg;

/* Used to initialize error() - defined in utils.c */

extern void error_init (void);

/* Whether to enable writing into executable and core files */
extern int write_files;

static void print_gdb_help (struct ui_file *);

#ifdef HP_XMODE

/* Switch to tell us whether or not we have been launched from another gdb. 
   If so, switching_modes will be set to 1. This flag tells gdb to read the
   saved history file in case the user invoked some debugger environment
   commands which we want to keep */

static int switching_modes = 0;

/*  Stacey 2/14/02
 *  This variable is defined in top.c and is set to one of the following
 *  enum values to tell us what kinds of executables this gdb can
 *  debug: xmode_ia64, xmode_pa64, xmode_pa32.  
 */

extern enum file_format xmode_this_gdb_understands;

/* Function which is used to read the saved history file passed with the
   --switching_modes argument */

static void xmode_read_abandoning_gdb_record (char *file);

/* Function which is used to switch to gdb64 if necessary only when
   invoked in main() */

void xmode_launch_64bit_gdb (char **argv, char *cause);

/* Function which is used to switch to gdb32 if necessary only when
   invoked in main() */

void xmode_launch_32bit_gdb (char **argv, char *cause);

/* JAGae35325 - Function which is  used to switch to IPF gdb if necessary only when
   invoked in main() */

void xmode_launch_IA_gdb (char **argv, char *cause);

/* 
 * Routine to tell us whether or not the option --switching_modes 
 * is in argv.  If so, we need to set up the variable switching_modes
 * pretty early as we have to use that flag while we are doing other
 * command line processing. 
 */

static int xmode_switching_modes (int argc, char **argv);
/* 
 * External function defined in top.c to check the executable format
 * of the file to debug.
 */

extern int xmode_exec_format_is_different (char *executable);

enum exec_format xmode_exec_format; /* Switching PA to IA and vice versa */ 

#endif /* HP_XMODE */

extern void set_threads_output_dir (char * optarg);

/* Forward declarations */

extern void set_inline_debug (char *);
extern void set_src_no_g (char *);
extern void nls_fix_argv (char**);

#ifdef GDB_CRASH_HANDLER
void U_STACK_TRACE (void);
void gdb_crash_handler (int);
void debug_self (void);

/* Set using a set command to let us start a debugger
   when gdb crashes.*/
char* debug_gdb = 0;
#endif /* GDB_CRASH_HANDLER */

/* RM: we stash away argv[0] in argv0 */
char *argv0;

/* These two are used to set the external editor commands when gdb is farming
   out files to be edited by another program. */

extern int enable_external_editor;
extern char *external_editor_command;

#ifdef __CYGWIN__
#include <windows.h>		/* for MAX_PATH */
#include <sys/cygwin.h>		/* for cygwin32_conv_to_posix_path */
#endif

/* Call command_loop.  If it happens to return, pass that through as a
   non-zero return status. */

static int
captured_command_loop (void *data)
{
  if (command_loop_hook == NULL)
    command_loop ();
  else
    command_loop_hook ();
  /* FIXME: cagney/1999-11-05: A correct command_loop() implementaton
     would clean things up (restoring the cleanup chain) to the state
     they were just prior to the call.  Technically, this means that
     the do_cleanups() below is redundant.  Unfortunatly, many FUNC's
     are not that well behaved.  do_cleanups should either be replaced
     with a do_cleanups call (to cover the problem) or an assertion
     check to detect bad FUNCs code. */
  do_cleanups (ALL_CLEANUPS);
  /* If the command_loop returned, normally (rather than threw an
     error) we try to quit. If the quit is aborted, catch_errors()
     which called this catch the signal and restart the command
     loop. */
  quit_command (NULL, instream == stdin);
  return 1;
}

static  void
get_gdb_root(char *argv0)
{
  char *tp;

  if ((tp = getenv("GDB_ROOT")) != NULL) /* GDB_ROOT is defined */
    strcpy(gdb_root, tp);
  else
    gdb_root[0] = NULL;

  DEBUG(printf("gdb_root is %s\n", gdb_root);)
  return;
}


char*
get_monitor_path (char** argv)
{
  char* monitor_sys_path = "/opt/sentinel/bin/sentinel";
  char monitor_user_path[MAXPATHLEN];
  char* monitor_path = NULL;
  struct stat st;

  if ((monitor_path = getenv ("SENTINEL_PATH")) != NULL)
    {
      if (stat (monitor_path, &st) == -1)
        monitor_path = NULL;
    }

  if (monitor_path == NULL && stat (monitor_sys_path, &st) == 0)
    monitor_path = monitor_sys_path;

  if (monitor_path == NULL && argv)
    {
      /* Couldn't find it in the SENTINEL_PATH or system path, try gdb path */
      char* slash = strrchr (argv[0], '/');
      monitor_user_path[0] = 0;
      if (slash)
        strncpy (monitor_user_path, argv[0], slash - argv[0] + 1);
      strcat (monitor_user_path, "sentinel");
      if (stat (monitor_user_path, &st) == 0)
        monitor_path = monitor_user_path;
    }

  if (monitor_path == NULL)
    {
      /* Still not there. Try current directory */
      strcpy (monitor_user_path, "sentinel");
      monitor_path = monitor_user_path;
    }  
  return monitor_path;
}


static void
invoke_process_monitor (int argc, char** argv)
{
  int opt_idx;
  int monitor_argc = 0;
  char** monitor_argv = NULL;
  char* monitor_path = NULL;
  char gdb_env_path[MAXPATHLEN];

  /* Find the --crashdebug option */
  for (opt_idx = 0; opt_idx < argc; opt_idx++)
    {
      if (strcmp (argv[opt_idx], "--crashdebug") == 0 ||
          strcmp (argv[opt_idx], "-crashdebug") == 0)
        break;
    }

  /* Move to the option following --crashdebug */
  opt_idx++;

  /* Everything before --crashdebug is skipped (argc - opt_idx).
     In addition we need to allocate space for "-abort debug -silent -p" and
     the trailing NULL (+5). */
  monitor_argc = argc - opt_idx + 5;
  monitor_argv = (char**) xmalloc (monitor_argc * sizeof(char*));

  sprintf (gdb_env_path, "SENTINEL_DEBUGGER=%s", argv[0]);
  putenv (gdb_env_path);

  monitor_path = get_monitor_path (argv);

  /* Fill in sentinel path and "-abort debug" option */
  monitor_argv[0] = monitor_path;
  monitor_argv[1] = "-abort";
  monitor_argv[2] = "debug";
  monitor_argv[3] = "-silent";
  
  /* Anything that comes after --crashdebug should either be -pid <pid>
     or the invocation command line for the app to monitor */
  if (strcmp (argv[opt_idx], "--pid") == 0 || 
      strcmp (argv[opt_idx], "-pid") == 0 ||
      strcmp (argv[opt_idx], "--p") == 0 ||
      strcmp (argv[opt_idx], "-p") == 0)
    {
      monitor_argv[4] = "-p";
      monitor_argv[5] = argv[opt_idx + 1];
      monitor_argv[6] = NULL;
    }
  else if (opt_idx == argc || argv[opt_idx][0] == '-')
    {
      /* Error - the only option allowed is --pid */
      fprintf_unfiltered (gdb_stderr,
                          "error: --crashdebug must be followed either by "
                          "--pid <pid> or application invocation command.\n");
      exit (1);
    }
  else
    {
      /* Treat the rest of the command line as invocation command +
         arguments: copy them to the monitor argv */
      int monitor_opt_idx;

      if (opt_idx == argc)
        {
          /* Error - nothing follows -crashdebug */
          fprintf_unfiltered (gdb_stderr,
                              "error: --crashdebug must be followed either by "
                              "--pid <pid> or application invocation "
                              "command.\n");
          exit (1);
        }

      for (monitor_opt_idx = 4; opt_idx < argc; opt_idx++, monitor_opt_idx++)
        monitor_argv[monitor_opt_idx] = argv[opt_idx];

      monitor_argv[monitor_opt_idx] = NULL;
    }

  if (execvp (monitor_path, monitor_argv) == -1)
    {
      fprintf_unfiltered (gdb_stderr,
                          "error: cannot exec %s.\n", monitor_path);
      exit (1);
    }
}

struct captured_main_args
  {
    int argc;
    char **argv;
  };

/* JAGag06697 - Function pointer definition. */
extern int (*getcommandline_ptr) (char *buf, 
				   size_t elemsize, 
				   size_t elemcnt, 
				   pid_t pid);

static int
captured_main (void *data)
{
  extern void objectdir_command (char *dirname, int from_tty);

  struct captured_main_args *context = data;
  int argc = context->argc;
  char **argv = context->argv;

  int count;
  static int quiet = 0;
  static int batch = 0;

  /* Pointers to various arguments from command line.  */
  char *symarg = NULL;
  char *execarg = NULL;
  char *corearg = NULL;
  char *getcorearg = NULL;
  char *cdarg = NULL;
  char *ttyarg = NULL;

  enum arg_core_or_pid
    {
      ARG_NULL,
      ARG_CORE,
      ARG_PID
    } core_or_pid = ARG_NULL;

#ifdef HP_XMODE
  char *abandoning_gdb_record_file=NULL;
/* JAGaf27822 -pid attach */
  int pidarg;
  struct pst_status buf;
  char *exec_file, *s1;
  char *full_exec_path = NULL;

  /* defined in source.c */
  extern int absolute_path_of (char *, char **);
  char cmdline[MAXPATHLEN + 1];
/* JAGaf27822 -pid attach - ends*/
#endif

  /* JAGag06697 */
  struct utsname uts;
  boolean pstat_getcommandline_available;

  /* These are static so that we can take their address in an initializer.  */
  static int print_help;
  static int print_version;

  /* Pointers to all arguments of --command option.  */
  char **cmdarg;
  /* Allocated size of cmdarg.  */
  int cmdsize;
  /* Number of elements of cmdarg used.  */
  int ncmd;

  /* Indices of all arguments of --directory option.  */
  char **dirarg;
  /* Indices of all arguments of --objectdir option.  */
  char **objdirarg;
  /* Allocated size.  */
  int dirsize;
  int objdirsize;
  /* Number of elements used.  */
  int ndir;
  int objndir; 

  struct stat homebuf, cwdbuf;
  char *homedir, *homeinit;

  register int i;

  long time_at_startup = get_run_time ();

#ifdef MULTI_BYTE_EMBEDDED_ASCII
  nls_fix_argv (argv);
#endif

#ifdef SEND_BETA_MAIL_ADDR
  save_argv = argv;
#endif

  START_PROGRESS (argv[0], 0);

#ifdef MPW
  /* Do all Mac-specific setup. */
  mac_init ();
#endif /* MPW */

  /* This needs to happen before the first use of malloc.  */
  init_malloc ((PTR) NULL);

  /* RM: we stash away argv[0] in argv0 */
  argv0 = argv[0];

  /* JAGae78846: Basename for gdb if it is not /opt/langtools/bin.
     Otherwise it is null.
     The idea is if the user starts gdb from a non-standard directory,
     all libraries (e.g librtc) and other gdbs (e.g. gdb64) should be
     picked up relative to the current basename.
 */
  get_gdb_root(argv0);
#ifdef HP_IA64
#pragma diag_suppress 20020
#endif
#if defined (ALIGN_STACK_ON_STARTUP)
#ifdef HP_IA64
#pragma diag_default 20020
#endif
  i = (int) &count & 0x3;
  if (i != 0)
    alloca (4 - i);
#endif

  cmdsize = 1;
  cmdarg = (char **) xmalloc (cmdsize * sizeof (*cmdarg));
  ncmd = 0;
  dirsize = 1;
  dirarg = (char **) xmalloc (dirsize * sizeof (*dirarg));
  objdirsize = 1;
  objdirarg = (char **) xmalloc (objdirsize * sizeof (*objdirarg));
  ndir = 0;
  objndir = 0;

  quit_flag = 0;
  line = (char *) xmalloc (linesize);
  line[0] = '\0';		/* Terminate saved (now empty) cmd line */
  instream = stdin;

  getcwd (gdb_dirbuf, sizeof (gdb_dirbuf));
  current_directory = gdb_dirbuf;

#if defined (TUI) || defined (GDBTK)
  /* Older code uses the tui_file and fputs_unfiltered_hook().  It
     should be using a customized UI_FILE object and re-initializing
     within its own _initialize function.  */
  gdb_stdout = tui_fileopen (stdout);
  gdb_stderr = tui_fileopen (stderr);
#else
  gdb_stdout = stdio_fileopen (stdout);
  gdb_stderr = stdio_fileopen (stderr);
#endif
  gdb_stdlog = gdb_stderr;	/* for moment */
  gdb_stdtarg = gdb_stderr;	/* for moment */

  /* initialize error() */
  error_init ();

#ifdef GDB_CRASH_HANDLER
   /* Print stack trace if we are going to die */
   signal (SIGSEGV, gdb_crash_handler);
   signal (SIGBUS, gdb_crash_handler);
   signal (SIGFPE, gdb_crash_handler);
   signal (SIGILL, gdb_crash_handler);
#endif /* GDB_CRASH_HANDLER */

  /* Parse arguments and options.  */
  {
    int c;
    /* When var field is 0, use flag field to record the equivalent
       short option (or arbitrary numbers starting at 10 for those
       with no equivalent).  */
    static struct option long_options[] =
    {
      {"async", no_argument, &event_loop_p, 1},
      {"noasync", no_argument, &event_loop_p, 0},
#if defined(TUI)
      {"tui", no_argument, &tui_version, 1},
#endif
      /* JAGae75018 - to enable loading core generated by dumpcore command from shell prompt */
      {"lcore", no_argument, &live_core, 1},
#if defined (RTC)
      {"leaks", no_argument, &check_heap, 1},
      {"threads", no_argument, &trace_threads, 1},
      {"thread", no_argument, &trace_threads, 1},
      /* QXCR1000757191: Enable being able to specify the o/p dir for batch mode thread check.
         The rtcconfig file is processed by librtc and hence only librtc knows about the o/p
         directory specified for batch mode thread check. Introduce an internal option called
         -threads-logdir to pass the name of the o/p directory from librtc to gdb. If no
         argument is passed to threads-logdir (happens when the rtcconfig file does not contain
         the output_dir specification), default to the current working directory.
       */
      {"threads-logdir", required_argument, 0, 2},
#endif 
      {"inline", required_argument, 0, 18},
      {"profile", no_argument, &profile_on, 1},
      {"testprofile", no_argument, &profile_test_on, 1},
      {"brtc", no_argument, &batch_rtc, 1}, /* not added to help file as it is an internal option */
      {"finalize_prompt", no_argument, &prompt_finalized, 1},
      {"nimbus", no_argument, &nimbus_version, 1},
      {"lcore", no_argument, &live_core, 1},
      {"old_abi", no_argument, &old_abi_option, 1},
      {"finalize_prompt", no_argument, &prompt_finalized, 1},
      {"xdb", no_argument, &xdb_commands, 1},
      {"aries", no_argument, &debugging_aries, 1},
      {"aries64", no_argument, &debugging_aries64, 1},
      {"dbx", no_argument, &dbx_commands, 1},
      {"readnow", no_argument, &readnow_symbol_files, 1},
      {"r", no_argument, &readnow_symbol_files, 1},
      {"mapped", no_argument, &mapped_symbol_files, 1},
      {"m", no_argument, &mapped_symbol_files, 1},
      {"mapshared", no_argument, &mapshared, 1},
      {"debug-gdb", no_argument, &debugging_gdb, 1},
      {"quiet", no_argument, &quiet, 1},
      {"q", no_argument, &quiet, 1},
      {"silent", no_argument, &quiet, 1},
      {"nx", no_argument, &inhibit_gdbinit, 1},
      {"n", no_argument, &inhibit_gdbinit, 1},
      {"batch", no_argument, &batch, 1},
      {"epoch", no_argument, &epoch_interface, 1},

    /* This is a synonym for "--annotate=1".  --annotate is now preferred,
       but keep this here for a long time because people will be running
       emacses which use --fullname.  */
      {"fullname", no_argument, 0, 'f'},
      {"f", no_argument, 0, 'f'},

      {"annotate", required_argument, 0, 12},
      {"help", no_argument, &print_help, 1},
      {"se", required_argument, 0, 10},
      {"symbols", required_argument, 0, 's'},
      {"s", required_argument, 0, 's'},
      {"exec", required_argument, 0, 'e'},
      {"e", required_argument, 0, 'e'},
      {"core", required_argument, 0, 'c'},
      {"src_no_g", required_argument, 0, 14},
      {"sampling-interval", required_argument, 0, 15},
      {"profile-output-file", required_argument, 0, 16},
      {"sampling-timer-type", required_argument, 0, 17},
      {"c", required_argument, 0, 'c'},
      {"pid", required_argument, 0, 'p'},
      {"p", required_argument, 0, 'p'},
      {"command", required_argument, 0, 'x'},
      {"version", no_argument, &print_version, 1},
      {"x", required_argument, 0, 'x'},
      {"crashdebug", optional_argument, 0, 'm'},
#ifdef GDBTK
      {"tclcommand", required_argument, 0, 'z'},
      {"enable-external-editor", no_argument, 0, 'y'},
      {"editor-command", required_argument, 0, 'w'},
#endif
#ifdef UI_OUT
      {"ui", required_argument, 0, 'i'},
      {"interpreter", required_argument, 0, 'i'},
      {"i", required_argument, 0, 'i'},
#endif
      {"directory", required_argument, 0, 'd'},
      {"d", required_argument, 0, 'd'},
      {"objectdir", required_argument, 0, 'o'},
      {"cd", required_argument, 0, 11},
      {"tty", required_argument, 0, 't'},
      {"baud", required_argument, 0, 'b'},
      {"b", required_argument, 0, 'b'},
      {"nw", no_argument, &use_windows, 0},
      {"nowindows", no_argument, &use_windows, 0},
      {"w", no_argument, &use_windows, 1},
      {"windows", no_argument, &use_windows, 1},
      {"statistics", no_argument, 0, 13},
      {"write", no_argument, &write_files, 1},
#ifdef HP_XMODE
        {"switching_modes",required_argument,0,'z'},
        {"switching_modes_executable",required_argument,0,'y'},
        {"switching_modes_corefile",required_argument,0,'w'},
        {"switching_modes_symfile",required_argument,0,'v'},
        {"switching_modes_getcore",required_argument,0,'u'},
#endif /* HP_XMODE */
/* Allow machine descriptions to add more options... */
#ifdef ADDITIONAL_OPTIONS
      ADDITIONAL_OPTIONS
#endif
      {0, no_argument, 0, 0}
    };

#ifdef HP_XMODE
    /* If we have the XMODE functionality turned on, we want
       to make sure to save relevant command line arguments just
       in case we have to launch another gdb.  Examples of command
       line arguments that will be saved include --fullname, --tty, etc. */

    xmode_save_command_line_args (argc,argv);

    if (xmode_switching_modes (argc,argv))
      switching_modes = 1;
#endif /* HP_XMODE */

    while (1)
      {
	int option_index;

	c = getopt_long_only (argc, argv, "",
			      long_options, &option_index);
	if (c == EOF)
	  break;

	/* Long option that takes an argument.  */
	if (c == 0 && long_options[option_index].flag == 0)
	  c = long_options[option_index].val;

	switch (c)
	  {
	  case 0:
	    /* Long option that just sets a flag.  */
	    break;
          case 2:
            set_threads_output_dir (optarg);
            break;
	  case 10:
	    symarg = optarg;
	    execarg = optarg;
	    break;
	  case 11:
	    cdarg = optarg;
	    break;
	  case 12:
	    /* FIXME: what if the syntax is wrong (e.g. not digits)?  */
	    annotation_level = atoi (optarg);
	    break;
	  case 13:
	    /* Enable the display of both time and space usage.  */
	    display_time = 1;
	    display_space = 1;
	    break;
          case 14:
	    set_src_no_g(optarg);
	    break;
          case 15:
            set_sampling_interval(optarg);
            break;
          case 16:
            set_profile_output_file(optarg);
            break;
          case 17:
            set_sampling_timer_type(optarg);
            break;
          case 18:
            set_inline_debug(optarg);
            break;
          case 'm':
            /* Check if invokes through WDB GUI */
            if (annotation_level == 2)
              {
	        fprintf_unfiltered (gdb_stderr,
                "\nERROR: --crashdebug option is not supported by WDB GUI "
                "and at annotation level 2.\n"
                "Ignoring --crashdebug option....\n\n");
              }
            else
              invoke_process_monitor (argc, argv);
            break;
	  case 'f':
	    annotation_level = 1;
/* We have probably been invoked from emacs.  Disable window interface.  */
	    use_windows = 0;
	    break;
	  case 's':
#ifdef HP_XMODE
            if (!switching_modes)
              symarg = optarg;
#else
            symarg = optarg;
#endif /* HP_XMODE */
	    break;
	  case 'e':
#ifdef HP_XMODE
            if (!switching_modes)
              execarg = optarg;
#else
            execarg = optarg;
#endif /* HP_XMODE */
	    break;
	  case 'c':
#ifdef HP_XMODE
            if (!switching_modes)
              corearg = optarg;
#else
            corearg = optarg;
#endif /* HP_XMODE */
	    core_or_pid = ARG_CORE;
	    break;
	  case 'p':
            corearg = optarg;
	    core_or_pid = ARG_PID;
	    break;
	  case 'x':
	    cmdarg[ncmd++] = optarg;
	    if (ncmd >= cmdsize)
	      {
		cmdsize *= 2;
		cmdarg = (char **) xrealloc ((char *) cmdarg,
					     cmdsize * sizeof (*cmdarg));
	      }
	    break;
#ifdef GDBTK
	  case 'z':
	    {
	      extern int gdbtk_test (char *);
	      if (!gdbtk_test (optarg))
		{
		  fprintf_unfiltered (gdb_stderr, "%s: unable to load tclcommand file \"%s\"",
				      argv[0], optarg);
		  exit (1);
		}
	      break;
	    }
	  case 'y':
	    {
	      /*
	       * This enables the edit/button in the main window, even
	       * when IDE_ENABLED is set to false. In this case you must
	       * use --tclcommand to specify a tcl/script to be called,
	       * Tcl/Variable to store the edit/command is:
	       * external_editor
	       */
	      enable_external_editor = 1;
	      break;
	    }
	  case 'w':
	    {
	      /*
	       * if editor command is enabled, both flags are set
	       */
	      enable_external_editor = 1;
	      external_editor_command = xstrdup (optarg);
	      break;
	    }
#endif /* GDBTK */
#ifdef UI_OUT
	  case 'i':
	    interpreter_p = optarg;
	    break;
#endif
	  case 'd':
	    dirarg[ndir++] = optarg;
	    if (ndir >= dirsize)
	      {
		dirsize *= 2;
		dirarg = (char **) xrealloc ((char *) dirarg,
					     dirsize * sizeof (*dirarg));
	      }
	    break;
          case 'o':
            objdirarg[objndir++] = optarg;
            if (objndir >= objdirsize)
              {
                objdirsize *= 2;
                objdirarg = (char **) xrealloc ((char *)objdirarg,
                                                objdirsize *
                                                sizeof (*objdirarg));
              }
            break;
	  case 't':
	    ttyarg = optarg;
	    break;
	  case 'q':
	    quiet = 1;
	    break;
	  case 'b':
	    {
	      int i;
	      char *p;

	      i = (int) strtol (optarg, &p, 0);
	      if (i == 0 && p == optarg)

		/* Don't use *_filtered or warning() (which relies on
		   current_target) until after initialize_all_files(). */

		fprintf_unfiltered
		  (gdb_stderr,
		   "warning: could not set baud rate to `%s'.\n", optarg);
	      else
		baud_rate = i;
	    }
	    break;
#ifdef HP_XMODE
          case 'v':
            /* Process the --switching_modes_symfile argument.  
	       This contains the new symfile to be read in and 
	       everything else should be ignored. */
            {
              symarg=optarg;
              quiet=1;
            }
          break;
          case 'w':
            /* Process the --switching_modes_corefile argument.  
               This contains the new corefile to be read in and 
               everything else should be ignored. */
            {
              corearg=optarg;
              quiet=1;
            }
          break;
          case 'u':
            /* Process the --switching_modes_getcore argument.  
               This contains the core file name and tar directory name. */
            {
              getcorearg=optarg;
              quiet=1;
            }
          break;
          case 'y':
            /* Process the --switching_modes_executable argument.  
              This contains the new executable to be read in and 
              everything else should be ignored. */
            {
              execarg=optarg;
              symarg=optarg;
              quiet=1;
            }
          break;
          case 'z':
            /* Process the --switching_modes argument.  We need to pick
               up the name of the user record file which is in optarg and
               also set the flag switching_modes.  We want to set the quiet
               flag to avoid getting the GNU advertisement and version. */
            {
              abandoning_gdb_record_file=optarg;
              quiet=1;
            }
          break;
#endif /* HP_XMODE */
	  case 'l':
	    {
	      int i;
	      char *p;

	      i = (int) strtol (optarg, &p, 0);
	      if (i == 0 && p == optarg)

		/* Don't use *_filtered or warning() (which relies on
		   current_target) until after initialize_all_files(). */

		fprintf_unfiltered
		  (gdb_stderr,
		 "warning: could not set timeout limit to `%s'.\n", optarg);
	      else
		remote_timeout = i;
	    }
	    break;

#ifdef ADDITIONAL_OPTION_CASES
	    ADDITIONAL_OPTION_CASES
#endif
	  case '?':
            /* Fixed DTS CLLbs14416 - TUI mode dumps core when given an
               illegal option.
               fprintf_unfiltered calls tuiClearCommandCharCount which
               checks tui_version to determine if it is necessary to reset
               tui command character count for the command window.
               However, tuiInit hasn't been called to initialize windows
               yet.
               Reset tui_version to false to prevent core dump. */
            if (tui_version)
              tui_version = 0;
	    fprintf_unfiltered (gdb_stderr,
			"Use `%s --help' for a complete list of options.\n",
				argv[0]);
	    exit (1);
	  }
      }

    /* If --help or --version, disable window interface.  */
    if (print_help || print_version)
      {
	use_windows = 0;
#ifdef TUI
	/* Disable the TUI as well.  */
	tui_version = 0;
#endif
      }

#ifdef TUI
    /* An explicit --tui flag overrides the default UI, which is the
       window system.  */
    if (tui_version)
      use_windows = 0;
#endif

    /* OK, that's all the options.  The other arguments are filenames.  */
    count = 0;
    for (; optind < argc; optind++)
      switch (++count)
	{
	case 1:
#ifdef HP_XMODE
          if (!switching_modes) 
            {
              symarg = argv[optind];
              execarg = argv[optind];
            }
#else
          symarg = argv[optind];
          execarg = argv[optind];
#endif /* HP_XMODE */
	  break;
	case 2:
#ifdef HP_XMODE
          if (!switching_modes) 
            corearg = argv[optind];
#else
	  corearg = argv[optind];
#endif /* HP_XMODE */
	  break;
	case 3:
	  fprintf_unfiltered (gdb_stderr,
			  "Excess command line arguments ignored. (%s%s)\n",
			  argv[optind], (optind == argc - 1) ? "" : " ...");
	  break;
	}
    if (batch)
      quiet = 1;
  }

#ifdef GDB_CRASH_HANDLER
  if (debugging_gdb)
    {
      /* if --debug-gdb is on, debug gdb with itself */
      debug_gdb = argv0;
      debug_self();
    }
#endif

#ifdef HP_XMODE
  /* If we have been configured for PA32, then check to see if
     the filename, corefile, or symbols file is in 64-bit or IPF format.
     If so, launch corresponding gdb. Likewise do it for other formats too.  */

  if (xmode_this_gdb_understands == xmode_pa32)
    {
      if (execarg) 
        {
          if (xmode_exec_format_is_different (execarg))
             if (xmode_exec_format == exec_pa64) 
               xmode_launch_64bit_gdb(argv, "executable");
             else if (xmode_exec_format == exec_ia64)
               xmode_launch_IA_gdb (argv, "executable");
        }
      else if (corearg)
        {
          if (xmode_exec_format_is_different (corearg))
             if (xmode_exec_format == exec_pa64) 
                xmode_launch_64bit_gdb(argv, "corefile");
             else if (xmode_exec_format == exec_ia64)
                xmode_launch_IA_gdb(argv, "corefile");
        }
      else if (symarg)
        {
          if (xmode_exec_format_is_different (symarg))
             if (xmode_exec_format == exec_pa64) 
                xmode_launch_64bit_gdb (argv, "symbol file");
             else if (xmode_exec_format == exec_ia64)
                xmode_launch_IA_gdb (argv, "symbol file");
        }
    }
  else if (xmode_this_gdb_understands == xmode_pa64)
    {
      if (execarg) 
        {
          if (xmode_exec_format_is_different (execarg))
             if (xmode_exec_format == exec_pa32)
                xmode_launch_32bit_gdb(argv, "executable");
             else if (xmode_exec_format == exec_ia64)
                xmode_launch_IA_gdb (argv, "executable");
         }
      else if (corearg)
        {
          if (xmode_exec_format_is_different (corearg))
             if (xmode_exec_format == exec_pa32)
                xmode_launch_32bit_gdb(argv, "corefile");
             else if (xmode_exec_format == exec_ia64)
                xmode_launch_IA_gdb (argv, "corefile");
  
        }
      else if (symarg)
        {
          if (xmode_exec_format_is_different (symarg))
             if (xmode_exec_format == exec_pa32)
                xmode_launch_32bit_gdb (argv, "symbol file");
             else if (xmode_exec_format == exec_ia64)
                xmode_launch_IA_gdb (argv, "symbol file");
        }
     }
    else if ((xmode_this_gdb_understands == xmode_ia64) && 
	     !debugging_aries && !debugging_aries64)
     {
      /* Stacey 3/11/2002
       * This is an IA gdb.  Check the format of the executable, corefile, or
       * symbol file.  If they're in PA format, launch PA gdb, otherwise continue
       * using IPF gdb.  
       * 
       * In the special case that we're debugging Aries, we ignore the switching
       * code below.  Though the outside world thinks the PA executable file 
       * (given to gdb as an argument) will become a real child process, this 
       * isn't true.  PA programs don't 'run' under aries.  They're just input 
       * data aries uses to emulate the behavior of a PA program.  Therefore, if 
       * we're debugging aries, we don't ever have to check the format of the
       * executable since it's just input data.  Aries is definitely an IPF 
       * library, so continue debugging it with IPF gdb.  The switching code will 
       * actually cause gdb to core dump if it's run when debugging aries.  */
      if (execarg) 
        {
          if (xmode_exec_format_is_different (execarg))
             if (xmode_exec_format == exec_pa32)
                xmode_launch_32bit_gdb(argv, "executable");
             else if (xmode_exec_format == exec_pa64)
                xmode_launch_64bit_gdb(argv, "executable");
         }
       else if (corearg)
        {
          if (xmode_exec_format_is_different (corearg))
             if (xmode_exec_format == exec_pa32)
                xmode_launch_32bit_gdb(argv, "corefile");
             else if (xmode_exec_format == exec_pa64)
                xmode_launch_64bit_gdb(argv, "corefile");
         }
        else if (symarg)
         {
          if (xmode_exec_format_is_different (symarg))
             if (xmode_exec_format == exec_pa32)
                xmode_launch_32bit_gdb(argv, "symbol file");
             else if (xmode_exec_format == exec_pa64)
                xmode_launch_64bit_gdb(argv, "symbol file");
         }
      }
#endif

  if (nimbus_version)
    {
      extern char default_prompt [];
      /*extern int rl_catch_sigwinch;*/
      extern void (*annotate_starting_hook) (void);
      extern void (*annotate_stopped_hook) (void);
      void nimbus_begin_reflection (void);
      void nimbus_end_reflection (void);
      char dirname[80];

      tui_version = 0;
      prompt_finalized = 1;
      /*rl_catch_sigwinch = 0;*/
      strcpy (default_prompt, "(wdb) ");
      printf_unfiltered ("\033\032\032:se gdb_prompt=%s\n\032\032A",
			 default_prompt);
      gdb_flush (gdb_stdout);
      annotate_starting_hook = nimbus_begin_reflection;
      annotate_stopped_hook = nimbus_end_reflection;
      printf_unfiltered ("\033\027jA");
      gdb_flush (gdb_stdout);

      sprintf (dirname, "/tmp/nimbus.%d/", getpid());
      if (mkdir (dirname, 
		 S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP))
	{
	  fprintf_unfiltered ((struct ui_file *) stderr, 
			      "Create %s failed.\n", 
			      dirname);
	}

      if (getenv ("FIREBOLT_PATH"))
        firebolt_version = 1;
    }


   if (trace_threads)
   {
      /* Turn all threads check features on with default values */
      thr_non_recursive_relock = 1;
      thr_unlock_not_own = 1;
      thr_mixed_sched_policy = 1;
      thr_condvar_multiple_mutexes = 1;
      thr_condvar_wait_no_mutex = 1;
      thr_deleted_obj_used = 1;
      thr_thread_exit_own_mutex = 1;
      thr_thread_exit_no_join_detach = 1;
      thr_stack_utilization = 80; /* %, 0 == disabled */
      thr_num_waiters = 0; /* 0 == disabled */
      thr_ignore_sys_objects = 1;
   }
    
#ifdef HP_IA64_NATIVE
    if (tui_version)
      {
        fprintf_unfiltered (gdb_stderr,
		"The terminal user interface (-tui) is not available on IPF.\n"
                "Use /opt/langtools/bin/vdb instead.\n");
        exit (1);
      }
#endif 

#if defined(TUI)
  if (tui_version)
    init_ui_hook = tuiInit;
#endif

  /* Initialize all files.  Give the interpreter a chance to take
     control of the console via the init_ui_hook()) */
  gdb_init (argv[0]);

  if (old_abi_option)
  {
    extern char* abi_string_arg;
    abi_string_arg = savestring ("old", (int) strlen("old"));
    new_cxx_abi = 0;
  }

  /* Do these (and anything which might call wrap_here or *_filtered)
     after initialize_all_files.  */
  if (print_version)
    {
      print_gdb_version (gdb_stdout);
      wrap_here ("");
      printf_filtered ("\n");
      exit (0);
    }

  if (print_help)
    {
      print_gdb_help (gdb_stdout);
      fputs_unfiltered ("\n", gdb_stdout);
      exit (0);
    }

  if (!quiet)
    {
      /* Print all the junk at the top, with trailing "..." if we are about
         to read a symbol file (possibly slowly).  */
      print_gdb_version (gdb_stdout);
      if (symarg)
	printf_filtered ("..");
      wrap_here ("");
      gdb_flush (gdb_stdout);	/* Force to screen during slow operations */
    }

  error_pre_print = "\n\n";
  quit_pre_print = error_pre_print;

  /* We may get more than one warning, don't double space all of them... */
  warning_pre_print = "\nwarning: ";

  /* ttyarg must be handled before executing $HOME/.gdbinit so that
     any target output that results goes to the correct tty. */
  if (ttyarg != NULL)
    catch_command_errors (tty_command, ttyarg, !batch, RETURN_MASK_ALL);

  /* Read and execute $HOME/.gdbinit file, if it exists.  This is done
     *before* all the command line arguments are processed; it sets
     global parameters, which are independent of what file you are
     debugging or what directory you are in.  */
#ifdef __CYGWIN32__
  {
    char *tmp = getenv ("HOME");

    if (tmp != NULL)
      {
	homedir = (char *) alloca (MAX_PATH + 1);
	cygwin32_conv_to_posix_path (tmp, homedir);
      }
    else
      homedir = NULL;
  }
#else
  homedir = getenv ("HOME");
#endif
  if (homedir)
    {
      homeinit = (char *) alloca (strlen (homedir) +
				  strlen (gdbinit) + 10);
      strcpy (homeinit, homedir);
      strcat (homeinit, "/");
      strcat (homeinit, gdbinit);

      if (!inhibit_gdbinit)
	{
	  catch_command_errors (source_command, homeinit, 0, RETURN_MASK_ALL);
	}

      /* Do stats; no need to do them elsewhere since we'll only
         need them if homedir is set.  Make sure that they are
         zero in case one of them fails (this guarantees that they
         won't match if either exists).  */

      memset (&homebuf, 0, sizeof (struct stat));
      memset (&cwdbuf, 0, sizeof (struct stat));

      stat (homeinit, &homebuf);
      stat (gdbinit, &cwdbuf);	/* We'll only need this if
				   homedir was set.  */
    }

  /* Now perform all the actions indicated by the arguments.  */
  if (cdarg != NULL)
    {
      catch_command_errors (cd_command, cdarg, 0, RETURN_MASK_ALL);
    }

  for (i = 0; i < ndir; i++)
    catch_command_errors (directory_command, dirarg[i], 0, RETURN_MASK_ALL);
  free ((PTR) dirarg);
  for (i = 0; i < objndir; i++)
    catch_command_errors (objectdir_command, objdirarg[i], 0, RETURN_MASK_ALL);
  free ((PTR) objdirarg);

  if (execarg != NULL
      && symarg != NULL
      && STREQ (execarg, symarg))
    {
      /* The exec file and the symbol-file are the same.  If we can't
         open it, better only print one error message.
         catch_command_errors returns non-zero on success! */
      if (catch_command_errors (exec_file_command, execarg, !batch, RETURN_MASK_ALL))
	catch_command_errors (symbol_file_command, symarg, 0, RETURN_MASK_ALL);
    }
  else
    {
      if (execarg != NULL)
	catch_command_errors (exec_file_command, execarg, !batch, RETURN_MASK_ALL);
      if (symarg != NULL)
	catch_command_errors (symbol_file_command, symarg, 0, RETURN_MASK_ALL);
    }

  getcommandline_ptr = NULL;
  pstat_getcommandline_available = false;
  exec_file = NULL;
/* Carl Burch/Sunil S JAGag06697
   1) Run uname and check if we are on a system where pstat_getcommandline () system call exists. 
   2) Use dlsym()/shl_findsym() to set a function pointer to call pstat_getcommandline() for the PA code. 
   It's cleaner to just use the function pointer to call pstat_getcommandline() in the IPF code as well.
*/
#ifdef GDB_TARGET_IS_HPPA
  int sys_ver = 0;
  uname (&uts);
  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
    error ("uname: Couldn't resolve the release identifier of the Operating system.");
  if (sys_ver >= 23) /* This will work for 11.23, 11.31 and 11.41 */
    {
      struct shl_descriptor *desc;
      int index = 1;
      while (shl_get (index, &desc) != -1)
        {
          int shl_stat = 0;
          index++;
          if (strstr (desc->filename, "/libc."))
            {
              shl_stat = shl_findsym (&desc->handle, "pstat_getcommandline",
                                      TYPE_PROCEDURE, &getcommandline_ptr);
              if (!shl_stat)
		{
		  pstat_getcommandline_available = true;
	          break;
		}
	    }
        }
    }
#else
  getcommandline_ptr = pstat_getcommandline;
  pstat_getcommandline_available = true;
#endif

  /* After the symbol file has been read, print a newline to get us
     beyond the copyright line...  But errors should still set off
     the error message with a (single) blank line.  */
  if (!quiet)
    printf_filtered ("\n");
  error_pre_print = "\n";
  quit_pre_print = error_pre_print;
  warning_pre_print = "\nwarning: ";

  if (corearg != NULL)
    {
      switch (core_or_pid)
	{
	case ARG_CORE:  	/* -core <core> option */
	  catch_command_errors (core_file_command, corearg, !batch, RETURN_MASK_ALL);
	  break;
	case ARG_PID:		 /* -pid <pid> option */
	  {
/* JAGaf27822 -pid attach */
#ifdef HP_XMODE
            if (!execarg)
	    {
    	       sscanf (corearg, "%d", &pidarg);
    	       if (getcommandline_ptr && pstat_getcommandline_available)
		 {
	            if (getcommandline_ptr (cmdline, sizeof(cmdline), 1, pidarg) != -1)
          	      exec_file = cmdline;
		 }
	       else
		 {
	           if (pstat_getproc (&buf, sizeof (struct pst_status), 0, pidarg) != -1)
        	     exec_file = buf.pst_cmd; /* executable pathname */
		 }
		/* Truncate the exec_file string to the first white space. */
		s1 = strchr (exec_file, ' ');
		if (s1)
		  s1[0] = '\0';
		
		if (exec_file && *exec_file != '/')
		{
	   	   if (openp (getenv ("PATH"), 1, exec_file, O_RDONLY, 0, &full_exec_path) < 0)
		   {
			if (!absolute_path_of (exec_file, &full_exec_path))
	  	  	    full_exec_path = savestring (exec_file, (int) strlen (exec_file));
		   }
		}
		else
  	  	    full_exec_path = savestring (exec_file, (int) strlen (exec_file));

		execarg = tilde_expand (full_exec_path);

		if (xmode_this_gdb_understands == xmode_pa32)
		  {
	    		if (xmode_exec_format_is_different (execarg))
			  xmode_launch_64bit_gdb (argv, "process");
	 	  }
		else if (xmode_this_gdb_understands == xmode_pa64)
	  	  {
	    		if (xmode_exec_format_is_different (execarg))
		  	  xmode_launch_32bit_gdb (argv, "process");
	  	  }
#ifdef HP_IA64
                /* jini: QXCR1000876000: gdb-5.9 hangs on attaching to PA-RISC
                   processes on Integrity with -pid. 
                   Use xmode_launch_*bit_gdb to launch the appropriate PA gdb
                   to attach to the PA-RISC process.
                 */

                else if ((xmode_this_gdb_understands == xmode_ia64) && 
	             !debugging_aries && !debugging_aries64)
                  {
                      if (xmode_exec_format_is_different (execarg))
                         if (xmode_exec_format == exec_pa32)
                            xmode_launch_32bit_gdb(argv, "process");
                         else if (xmode_exec_format == exec_pa64)
                            xmode_launch_64bit_gdb(argv, "process");
                   }
#endif
               }
#endif          
	  catch_command_errors (attach_command, corearg, !batch, RETURN_MASK_ALL); 
	  break;
          }
/* JAGaf27822 -pid attach - ends*/
	default: 		/* could either be a core or pid */
	  {
      	     int tmp_pid, i = 0;
             int len = (int) strlen(corearg);
             struct pst_status buf;
	
	     while ( (i < len) && isdigit(corearg[i]) ) i++;
	
             if (i == len) /* corearg could be a PID */
	       {	       
	         sscanf (corearg, "%d", &tmp_pid);

	         if (pstat_getproc (&buf, sizeof (struct pst_status), 0, tmp_pid) != -1)
	            catch_command_errors (attach_command, corearg, !batch, RETURN_MASK_ALL); 
	         else 
	            catch_command_errors (core_file_command, corearg, !batch, RETURN_MASK_ALL);
	       }
             else /* core file */
	       catch_command_errors (core_file_command, corearg, !batch, RETURN_MASK_ALL);
	  }
	}  /* switch */
    }

  if (getcorearg != NULL)
    catch_command_errors (getcore_command, getcorearg, !batch, RETURN_MASK_ALL);

#ifdef ADDITIONAL_OPTION_HANDLER
  ADDITIONAL_OPTION_HANDLER;
#endif

  /* Error messages should no longer be distinguished with extra output. */
  error_pre_print = NULL;
  quit_pre_print = NULL;
  warning_pre_print = "warning: ";

  /* Read the .gdbinit file in the current directory, *if* it isn't
     the same as the $HOME/.gdbinit file (it should exist, also).  */

  if (!homedir
      || memcmp ((char *) &homebuf, (char *) &cwdbuf, sizeof (struct stat)))
    if (!inhibit_gdbinit)
      {
	catch_command_errors (source_command, gdbinit, 0, RETURN_MASK_ALL);
      }

#ifdef HP_IA64
  /* jini: QXCR1000875628. Read the .gdbinit file in /tmp. */

  if (getenv ("GDB_PROCESS_TMP_GDBINIT") && !inhibit_gdbinit)
    {
      char *tmpinit = (char *) alloca (strlen ("/tmp") + 
                             strlen (gdbinit) + 10);
      strcpy (tmpinit, "/tmp/");
      strcat (tmpinit, gdbinit);

      catch_command_errors (source_command, tmpinit, 0, RETURN_MASK_ALL);
    }
#endif

  for (i = 0; i < ncmd; i++)
    {
      catch_command_errors (source_command, cmdarg[i], !batch,
			    RETURN_MASK_ALL);
    }
  free ((PTR) cmdarg);

  /* Read in the old history after all the command files have been read. */
  init_history ();

#ifdef HP_XMODE
  /* If we were launched by another gdb, read the temp file which has the
     recorded user debug environment and re-execute those commands */

  if (switching_modes && (strlen (abandoning_gdb_record_file) != 0)) 
    {
      xmode_read_abandoning_gdb_record (abandoning_gdb_record_file);
    }
#endif /* HP_XMODE */

  if (batch)
    {
      /* We have hit the end of the batch file.  */
      exit (0);
    }

  /* Do any host- or target-specific hacks.  This is used for i960 targets
     to force the user to set a nindy target and spec its parameters.  */

#ifdef BEFORE_MAIN_LOOP_HOOK
  BEFORE_MAIN_LOOP_HOOK;
#endif

  END_PROGRESS (argv[0]);

  /* Show time and/or space usage.  */

  if (display_time)
    {
      long init_time = get_run_time () - time_at_startup;

      printf_unfiltered ("Startup time: %ld.%06ld\n",
			 init_time / 1000000, init_time % 1000000);
    }

  if (display_space)
    {
#ifdef HAVE_SBRK
      extern char **environ;
      char *lim = (char *) sbrk (0);

      printf_unfiltered ("Startup size: data size %ld\n",
			 (long) (lim - (char *) &environ));
#endif
    }

  /* The default command loop. 
     The WIN32 Gui calls this main to set up gdb's state, and 
     has its own command loop. */
#if !defined _WIN32 || defined __GNUC__
  /* GUIs generally have their own command loop, mainloop, or
     whatever.  This is a good place to gain control because many
     error conditions will end up here via longjmp(). */
  /* NOTE: cagney/1999-11-07: There is probably no reason for not
     moving this loop and the code found in captured_command_loop()
     into the command_loop() proper.  The main thing holding back that
     change - SET_TOP_LEVEL() - has been eliminated. */
  while (1)
    {
      catch_errors (captured_command_loop, 0, "", RETURN_MASK_ALL);
    }
#endif
  /* No exit -- exit is through quit_command.  */
}

int
main (int argc,
      char **argv)
{
  struct captured_main_args args;
  args.argc = argc;
  args.argv = argv;
  catch_errors (captured_main, &args, "", RETURN_MASK_ALL);
  return 0;
}

char *
set_path(char *path)
{
  char *modified_path;

  if (gdb_root[0] != NULL) /* GDB_ROOT is defined - JAGaf03564 */
    {
       modified_path = (char *) xmalloc (strlen(gdb_root) +
                                    strlen("/bin:") +
                                    strlen(path) + 
				    strlen(":/usr/ccs/bin") + 1);
       strcpy (modified_path,gdb_root);
       strcat (modified_path,"/bin:");
       strcat (modified_path,path);
       strcat (modified_path,":/usr/ccs/bin");
    }
  else
    {
       modified_path = (char *) xmalloc (strlen(path) +
                                    strlen(":/opt/langtools/bin") + 
				    strlen(":/usr/ccs/bin") + 1);
       strcpy (modified_path,path); /*JAGaf03564*/ 
       strcat (modified_path,":/opt/langtools/bin");
       strcat (modified_path,":/usr/ccs/bin");
    }
    return modified_path;
}

#ifdef HP_XMODE
/* xmode_read_abandoning_gdb_file(record_file)
   This function takes in the name of the record file passed
   with the --switching_modes option and calls source_command()
   to process each command in the file.  When we are done, we
   can remove the temporary file as we will no longer need it. */

static void
xmode_read_abandoning_gdb_record (char *record_file)
{
  catch_command_errors (source_command, record_file, 0, RETURN_MASK_ALL);
  unlink (record_file);
}


/* xmode_launch_64bit_gdb(argv, cause)

   Arguments: original argv
   cause (either "executable", "corefile", "symbol file")      

   Used to launch the gdb which understands 64-bit executables if we
   were executed via gdb. */

 /* find gdb64 to launch */
void
xmode_launch_64bit_gdb (char **argv, char *cause)
{

  char *gdb_executable;
  char *path = NULL;
  char *modified_path = NULL;
  
  path = getenv("PATH");
  modified_path = set_path(path);

  /* find gdb64 to launch */

   if (openp (modified_path, 1, "gdb64", O_RDONLY, 0, &gdb_executable) < 0 ) 
     {
      printf ("gdb64 could not be found.   Try adding the directory in\n");
      printf ("which gdb64 lives to your path environment variable.\n");
      printf ("Switch to PA64 mode failed\n");
      return;
    }
#ifdef GDB_TARGET_IS_HPPA
#ifndef HPUX_1020
  if ( IS_32BIT_KERNEL )
    {
        if ( strcmp (cause, "executable") == 0 )
          {
	       /* Don't allow to debug 64 bit binary on 32 bit machines */
 	       return;
          }
        if ( strcmp (cause, "corefile") == 0 ) 
          {
              printf_unfiltered ("\nwarning: Detected 64-bit executable and core file\n");
              printf_unfiltered ("on a 32-bit system; debugging is limited.\n\n");
           }
     }
#endif
#endif
  printf_unfiltered ("Detected 64-bit %s.\nInvoking %s.\n",cause,gdb_executable);
  /* Ensure messages are printed before exec'ing cross-mode gdb. */
  gdb_flush (gdb_stdout);
  execvp(gdb_executable,argv);
}

/* xmode_launch_32bit_gdb(argv, cause)

   Arguments: original argv 
   cause (either "executable", "corefile", "symbol file")      

   Used to launch the gdb which understands 32-bit executables if we
   were executed via gdb64 on a PA system or via an ia64 gdb on IPF
   
   Stacey
   Reasons for differences between IPF & PA versions of this function:
     /usr/ccs/bin is the best place to locate PA gdb on an IPF because it is:
     1.  part of a user's path on a bare system
     2.  available to applications being bundled w/ the CORE - unlike 
         /opt which we normally use on PA
  
     The name 'gdbpa' was chosen jointly by Jan, integration, & us to 
     eliminate conflicts name conflicts between the ISU IPF gdb packaging &
     CORE PA gdb packaging.  */

void
xmode_launch_32bit_gdb (char **argv, char *cause)
{
  char *gdb_executable;
  char *path = NULL;
  char *modified_path = NULL;

#ifdef HP_IA64
  char *default_path = ":/usr/ccs/bin"; 
  char *gdb_name = "gdbpa";
  char *exec_type ="PA";
  char *mode_message ="PA";
#else /* not IA64 - Assume we're on PA */
  char *default_path = ":/opt/langtools/bin";
  char *gdb_name = "gdb32";
  char *exec_type="32-bit";
  char *mode_message ="PA32";
#endif /* HP_IA64 */
  path = getenv ("PATH");
  modified_path = set_path(path);

  /* find gdb32 to launch */

  if (openp (modified_path, 1, gdb_name, O_RDONLY, 0, &gdb_executable) < 0)
    {
      printf_unfiltered ("%s could not be found.   Try adding the directory "
			 "in\nwhich %s lives to your path environment "
			 "variable.\nSwitch to PA mode failed\n", 
			 gdb_name, gdb_name, mode_message);
      return;
    }
  printf ("Detected %s %s.\nInvoking %s.\n", exec_type, cause, gdb_executable);
  execvp (gdb_executable, argv);
  printf_unfiltered ("Detected 32-bit %s.\nInvoking %s.\n",cause,gdb_executable);
}

/* JAGae35325 - PA-GDB running on IPF */

void
xmode_launch_IA_gdb (char **argv, char *cause)
{

  char *gdb_executable;
  char *path = NULL;
  char *modified_path = NULL;

  path = getenv("PATH");
  modified_path = (char *) xmalloc (strlen(path) + 
                                    strlen(":/usr/ccs/bin") + 
				    strlen (":/opt/langtools/bin") + 2 ) ;
  strcpy (modified_path, ":/opt/langtools/bin"); /* IA GDB sits in this path in IPF machine*/
  strcat (modified_path,":/usr/ccs/bin"); /* PA gdb sits in this path in IA m/c */
  strcat (modified_path,path);
  
  /* find gdb to launch */

  if (openp (modified_path, 0, "gdb", O_RDONLY, 0, &gdb_executable) < 0 ) 
        {
          printf ("IPF gdb could not be found.   Try adding the directory in\n");
          printf ("which IPF gdb lives to your path environment variable.\n");
          printf ("Switch to IPF mode failed\n");
          return;
        }
  printf_unfiltered ("Detected IPF %s.\nInvoking %s.\n",cause,gdb_executable);
  /* Ensure messages are printed before exec'ing cross-mode gdb. */
  gdb_flush (gdb_stdout);
  execvp(gdb_executable,argv);
}



/* Routine to tell us whether or not the option --switching_modes 
   is in argv.  If so, we need to set up the variable switching_modes
   pretty early as we have to use that flag while we are doing other
   command line processing. */


static int
xmode_switching_modes (int argc, char **argv)
{
  int index;

  for (index = 0; index < argc; index++)
    {
      if (strncmp (argv[index], "--switching_modes", 17) == 0)
	return 1;
    }
  return 0;
}

#endif /* HP_XMODE */
    

#ifdef MULTI_BYTE_EMBEDDED_ASCII

/* nls_fix_argv - As in command_line_input, we quote the second byte of
   HP15 characters if they are in the set 
     '"\
   We don't adjust argv[0] because that isn't parsed by gdb. */

void
nls_fix_argv (argv)
     char** argv;
{
  extern void _initialize_utils (void);
  extern char* nls_fix_readline (char* input_line);

  char** argv_ptr;
  char* temp_arg_ptr;

  /* Call _initialize_utils to define locale_is_c and call setlocale() */
  _initialize_utils ();

  if (locale_is_c || MB_CUR_MAX == 1)
    return;  /* Nothing to do */

  if (!*argv)
    return; /* We don't fix up argv[0].  If there is no argv[1] then done. */

  /* fix up argv[1]... */

  for (argv_ptr = argv + 1;  *argv_ptr; argv_ptr++)
  {
    temp_arg_ptr = malloc (strlen(*argv_ptr) + 1);
    strcpy (temp_arg_ptr, *argv_ptr);

    *argv_ptr = nls_fix_readline (temp_arg_ptr);
  }
} /* end nls_fix_argv */
#endif

/* log_test_event is intended to gather usage information during a beta
   test period.  A target which wants to gather this information should
   define SEND_BETA_MAIL_ADDR to be a mail address and should make some
   or all of the following defines to activate logging these events:

        #define LOG_BETA_DOC_FUNCTION           0
        #define LOG_BETA_EDIT_COMMAND           1
        #define LOG_BETA_F90_ARRAY_VAL          2
        #define LOG_BETA_F90_STRUCT_VAL         3
        #define LOG_BETA_F90_ARRAY_TYPE         4
        #define LOG_BETA_F90_STRUCT_TYPE        5
        #define LOG_BETA_FILTERED_LIBRARIES     6
        #define LOG_BETA_FIX_COMMAND            7
        #define LOG_BETA_GUI_FIX_AND_CONTINUE   8
        #define LOG_BETA_GUI_HEAP_CHECK         9
        #define LOG_BETA_LOAD_TIME              11
        #define LOG_BETA_RTC_USED               15
        #define LOG_BETA_RUN_COMMAND_TIME       17
        #define LOG_BETA_F90_FUNCTION           18

   The initial list is in alphabetical order, but new events should be
   added at the end because the GUI logs some events by invoking the
   log_beta command with an event number, so it is important that the meaning
   of an event number not change.
   */

#define MAX_LOG_EVENT           18

typedef struct event_info_struct
  {
    int         event_count;
    int         event_data_max;
  }
event_info_t;

static event_info_t event_info[MAX_LOG_EVENT + 1];

/* These variable were not used and TOHECC compiler 
   with +wlint was emitting a warning for this.
   So this has been commented for now */

#ifdef SEND_BETA_MAIL_ADDR
static char * event_names[] = { "LOG_BETA_DOC_FUNCTION",
                                "LOG_BETA_EDIT_COMMAND",
                                "LOG_BETA_F90_ARRAY_VAL",
                                "LOG_BETA_F90_STRUCT_VAL",
                                "LOG_BETA_F90_ARRAY_TYPE",
                                "LOG_BETA_F90_STRUCT_TYPE",
                                "LOG_BETA_FILTERED_LIBRARIES",
                                "LOG_BETA_FIX_COMMAND",
                                "LOG_BETA_GUI_FIX_AND_CONTINUE",
                                "LOG_BETA_GUI_HEAP_CHECK",
                                "UNUSED_ENTRY",
                                "LOG_BETA_LOAD_TIME",
                                "UNUSED_ENTRY",
                                "UNUSED_ENTRY",
                                "UNUSED_ENTRY",
                                "LOG_BETA_RTC_USED",
                                "UNUSED_ENTRY",

                                "LOG_BETA_RUN_COMMAND_TIME",
                                "LOG_BETA_F90_FUNCTION" };

#endif

void
log_test_event (int   event_nbr,
                int   event_data)
{
  if (event_nbr < 0 || event_nbr > MAX_LOG_EVENT) 
    {
      assert(0); /* Bad event number. */
      return;  
    }
  
  if (event_info[event_nbr].event_count == 0)
    event_info[event_nbr].event_data_max = event_data;
  else
    if (event_data > event_info[event_nbr].event_data_max)
      event_info[event_nbr].event_data_max = event_data;

  event_info[event_nbr].event_count++;
} /* end log_test_event */

/* send_beta_test_log_mail - if there have been any calls to log_test_event,
   send mail to SEND_BETA_MAIL_ADDR.  If the environment variable 
   GDB_NO_BETA_MAIL has a non-null value, then don't send the mail.  If
   the directory from which gdb came has a .gdb_no_beta_mail that has 
   a "*" line or a line with the user's login name, then don't send the mail.
   
   If the environment variable GDB_BETA_MAIL_ADDR is set, send the mail to that
   address instead.
   */

void 
send_beta_test_log_mail ()
{
  char*         command_string;
  int           event_nbr;
  int           file_desc;
  char          file_name[L_tmpnam];
  char*         file_path;
  int           i;
  char*         mail_address;
  struct passwd * passwd_info;
  char*         path_string;
  FILE*         temp_file;
  char*         str_ptr;
  char*         user_name;
  char          read_buffer[100];

#ifndef SEND_BETA_MAIL_ADDR
  return;
#else

  /* If nothing has been logged, don't send mail. */

  for (i = 0;  i <= MAX_LOG_EVENT; i++)
    {
      if (event_info[i].event_count)
        break;
    }
  if (i > MAX_LOG_EVENT)
    return;  /* Nothing was logged */

  /* If the user has set GDB_NO_BETA_MAIL, don't send the mail. */

  if (getenv("GDB_NO_BETA_MAIL"))
    return;

  /* If there is a .gdb_no_beta_mail file in the directory which contains the
     gdb executable and that file has a line of a single asterisk or
     a line of the user's login name, then don't send the mail.

     If argv[0] contains a slash, we look in the directory which is
     argv[0] up to the last slash, otherwise use openp to search for the
     file using the PATH environment variable. */

  if (strchr (save_argv[0], '/'))
    {
      file_path = xmalloc (strlen(save_argv[0]) + 30);
      strcpy (file_path, save_argv[0]);
      str_ptr = strrchr (file_path, '/');
      if (str_ptr)
	strcpy (str_ptr, "/");
      else
	strcpy (file_path, "./");
      /* file_path is now the path to the directory we will search. */
      strcat (file_path, ".gdb_no_beta_mail");
    }
  else
    {
      file_path = 0;
      path_string = getenv ("PATH");

      /* If openp fails, file_path will be NULL.  If it succeeds, it will
         contain a malloced string of the file opened.
         */
      file_desc = openp (path_string, 0, ".gdb_no_beta_mail", O_RDONLY, 0,
                         &file_path);
      if (file_desc)
        close (file_desc);
    }
  if ((temp_file = fopen (file_path, "r")) != NULL)
    {
      /* The file exists.  Does it have our user's name or "*" as a line? */

      if (passwd_info = getpwuid (geteuid ()))
        user_name = passwd_info->pw_name;
      else
        user_name = "*";

      while (fgets (read_buffer, 100, temp_file))
        {
          read_buffer[strlen(read_buffer) -1 ] = 0;  /* remove new-line */
          if (   strcmp(read_buffer, "*") == 0 
              || strcmp (read_buffer, user_name) == 0)
            {
              fclose (temp_file);
              free (file_path);
              return; /* Either user name or "*" found */
            }
        } /* While there are lines to read */

      fclose (temp_file);
    } /* If the .gdb_no_beta_mail file exists */

  free (file_path);

  /* Construct the letter to send in a temporary file. */
  tmpnam(file_name);
  unlink (file_name);
  file_desc = open (file_name, O_RDWR | O_EXCL | O_CREAT, 0777);
  if (file_desc == -1 || (temp_file = fdopen (file_desc, "w")) == NULL)
    error ("Can't open temp file %s.", file_name);
    
  fprintf(temp_file, "%s\n",
"This letter is being sent as a result of using the beta copy of the gdb\n"
"debugger.  If you want to suppress sending this mail, set the environment\n"
"variable GDB_NO_BETA_MAIL to a non-null value, e.g. \"TRUE\".\n"
"\n"
"Alternatively, you can create a file named .gdb_no_beta_mail in the\n"
"directory that contains the gdb executable.  The file allows you to suppress\n"
"the sending of logging information from all users or selected users.\n"
"\n"
"To suppress logging information for all users, the file should contain a\n"
"line with a single astersk (*).  To suppress logging information for\n"
"specific users, list the user names, one per line, in the file.\n"
"\n"
"If you want to send the mail to a different mail address, set\n"
"the environment variable GDB_BETA_MAIL_ADDR to the desired address,\n"
"e.g. \"mygdblog@mydomain.com\".  The purpose of the logging is to verify\n"
"that particular features have been exercised in the beta testing.\n");

  for (event_nbr = 0;  event_nbr <= MAX_LOG_EVENT; event_nbr++)
    {
      if (event_info[event_nbr].event_count)
        {
          fprintf(temp_file, "%s logged %d times max %d\n",
                  event_names[event_nbr],
                  event_info[event_nbr].event_count,
                  event_info[event_nbr].event_data_max);
        }
    }
  
  fclose(temp_file);

  /* Determine the address to send the mail to. */

  mail_address = getenv("GDB_BETA_MAIL_ADDR");
  if (!mail_address || strlen(mail_address) == 0)
    mail_address = SEND_BETA_MAIL_ADDR;

  /* Send the letter using the system function to invoke mailx. 
        mailx -s 'Beta testing statistics' address < file_name > /dev/null 2>&1
     */

  command_string = xmalloc (strlen(mail_address) + strlen(file_name) + 80);
  strcpy (command_string, "mailx -s 'Beta testing statistics' ");
  strcat (command_string, mail_address);
  strcat (command_string, " < ");
  strcat (command_string, file_name);
  strcat (command_string, "> /dev/null 2>&1");
  system (command_string);
  free (command_string);
#endif  /* SEND_BETA_MAIL_ADDR is defined */

} /* end send_beta_test_log_mail */


#ifdef GDB_CRASH_HANDLER
/* To start a new debugger that debugs us. */
void
debug_self ()
{
  char* command = (char*) alloca (20+ strlen (debug_gdb) + strlen (argv0));
  sprintf (command, "hpterm -e %s %s %d&", debug_gdb, argv0, getpid());

  errno = 0;
  system (command);
  if (errno)
    exit (1);

  sleep (10); /* Let the debugger to attach. */
  return;
}

/* Signal handler for the signals in gdb. */
void
gdb_crash_handler (int sig)
{
  char *prompt;
  char *arg;
  target_terminal_ours();

  if (!nimbus_version)
    {
      /* Print the current gdb stack. */
      U_STACK_TRACE();
      target_terminal_ours (); /* Let's get handle to the terminal. */

      printf_filtered ("GDB crashed with signal %d! About to dump core into"
		       " 'core' in the directory:\n", sig);
      system ("pwd");
      printf_filtered ("Select one of the following options...\n");
      printf_filtered ("[N] No, do not dump core\n");
      printf_filtered ("[Y] Yes, dump core (default)\n"
		"    NOTE: Make sure to rename any existing core file in this\n"
		"          directory, as gdb's core will overwrite it.\n");
      printf_filtered ("[C] Continue execution (at your own risk)\n");
      if (debug_gdb)
        printf_filtered ("[D] Debug gdb\n");
  
      if ((prompt = getenv ("PS2")) == NULL)
        {
          prompt = "> ";
        }

      /* Get the input from user. */
      arg = command_line_input (prompt, 0, "overload-choice");

      if (arg == 0 || *arg == 0)
        {
          printf_filtered ("Dumping core by default...\n");
          abort ();
        }

      /* Skip the white spaces and tabs. */
      while (*arg == ' ' || *arg == '\t')
        arg++;
  
      if (*arg == 'N' || *arg == 'n') /* If no, just exit. */
        exit (0);
      else if (*arg == 'Y' || *arg == 'y') /* If yes call abort to dump core */
        abort (); 
      else if (*arg == 'C' || *arg == 'c') 
        /* If continue, warn the user and continue. */
        {
          /* When the signal handler is called, handler for that signal is
             automatically disabled. Lets re-install. */
          signal (SIGSEGV, gdb_crash_handler);
          signal (SIGBUS, gdb_crash_handler);

          error ("GDB received signal %d, but continuing...\n"
	         "GDB may be in an unstable state.\n", sig);
        }
      else if (debug_gdb && (*arg == 'D' || *arg == 'd'))
	/* Start a debugger to debug self. */
        {
	  debug_self ();

	  /* Re-install signal handler before continuing. */
	  signal (SIGSEGV, gdb_crash_handler);
          signal (SIGBUS, gdb_crash_handler);
	  error ("GDB received signal %d, but continuing...\n"
                 "GDB may be in an unstable state.\n", sig);
        }
      else  /* Default, dump core. */
        {
          printf_filtered ("Dumping core by default...\n");
          abort ();
        }
    } /* !nimbus_version */
  else 
    {
      /* Srikanth, Sep 03, 2002, Print out the stack trace for Nimbus
         version too.
      */
      U_STACK_TRACE();
      gdb_flush (gdb_stdout);
      fflush(stdout);
      abort ();
    } /* nimbus_version */
}/* end gdb_crash_handler */
#endif /* GDB_CRASH_HANDLER */


/* Don't use *_filtered for printing help.  We don't want to prompt
   for continue no matter how small the screen or how much we're going
   to print.  */

static void
print_gdb_help (struct ui_file *stream)
{
  fputs_unfiltered ("\n\
    gdb - HP WDB - the HP supported implementation of the GNU Debugger (GDB).\n\n\
    Usage:\n\n\
    gdb [options][arguments]\n\n\
    Options:\n\n",stream);

  fputs_unfiltered ("\
    -help\t\tList all command line options, with brief explanations.\n\
    -exec=<execfile>\tInvokes gdb on the specified executable, <execfile>.\n\
    -pid=<procID>\tAttaches to the specified process, <procID>,\n\t\t\tfor debugging.\n\
    -core=<corefile>\tExamines the core that is dumped in the file,<corefile>.\n\
    -lcore\t\tDebugs a corefile that is generated by dumpcore command\n\t\t\ton 11.22 system.\n"
     , stream);
  fputs_unfiltered ("\
    -leaks\t\tInvokes gdb and enables leak detection.\n\
    -thread | -threads\tInvokes gdb and enables thread debugging.\n\
    -crashdebug\t\tMonitors program execution and invokes gdb when the\n\t\t\
        program execution is about to abort.\n\
    -inline=<option>\tInvokes gdb and enables inline debugging with the\n\t\t\
        appropriate mode of setting breakpoints on inline\n\t\t\
        functions.\n\
    -src_no_g=<option>\tSets the source level debugging feature when the \n\t\t\
        program is compiled without -g.\n"
    , stream);
  fputs_unfiltered ("\
    -tui\t\tInvokes gdb with the terminal user interface (TUI).\n\
    -tty=<device>\tInvokes gdb and uses <device> as the standard input and\n\t\t\
        output for the program.\n\
    -xdb\t\tInvokes gdb in the XDB compatibility mode, which allows\n\t\t\
        the limited use of some XDB commands.\n\
    -dbx\t\tEnables support for additional dbx commands.\n\
    -version\t\tPrints the version number and the introductory message.\n\
    -quiet\t\tThe quiet mode suppresses the printing of introductory\n\t\t\
        and copyright messages in gdb.\n\
    -annotate=<level>\tSets the annotation level, <level>, inside gdb.\n\
    -interpreter=<inp>\tUses the interpreter, <inp>, for interface\n\t\t\
        with the controlling program or device.\n"
    , stream);
  fputs_unfiltered ("\
    -directory=<dir>\tAdds the directory, <dir>, to the directory listings in\n\t\t\
        the search path when searching for source files.\n\
    -objectdir=<dir>\tAdds the directory, <dir>, to the directory listings in\n\t\t\
        the search path when searching for object files.\n\
    -cd=<dir>\t\tRuns gdb by using the directory , <dir>, instead of\n\t\t\
        the current working directory.\n\
    -mapshared\t\tSets the mapping of all shared libraries loaded in the\n\t\t\
        future, as shared instead of private.\n\
    -symbols=<filename>\tRead symbol table from file <filename>.\n\
    -se=<exe-name>\tRead symbol table from <exe-name> file and use it as\n\t\t\
        the executable file.\n\
    -mapped\t\tEnables gdb to store the symbols in the executable in a\n\t\t\
        reusable memory-mapped file if memory-mapped files are\n\t\t\
        available on the system through the mmap system call.\n\
    -readnow\t\tEnables immediate read of the symbol table from the\n\t\t\
        symbol file instead of the default mode of reading the\n\t\t\
        symbol table incrementally.\n"
    , stream);
  fputs_unfiltered ("\
    -command=<filename>\tExecutes gdb commands in the command file, <filename>.\n\
    -batch\t\tExecutes the gdb commands in the command files that are\n\t\t\
        specified with `-x'.\n\
    -nx\t\t\tSkip execution of the commands from the initialization\n\t\t\
        files ('.gdbinit').\n\
    -fullname\t\tDirects Emacs to set the option for running gdb as a\n\t\t\
        sub-process.\n\
    -baud=bps\t\tSet the line speed (baud rate or bits per second) of any\n\t\t\
        serial interface that is used by gdb for remote\n\t\t\
        debugging.\n"
    , stream);

#ifdef ADDITIONAL_OPTION_HELP
  fputs_unfiltered (ADDITIONAL_OPTION_HELP, stream);
#endif
  fputs_unfiltered ("\n\
    For more information on the command line options for gdb,\n\
    see gdb(1) manpage. For more information on the commands,\n\
    type \"help\" within gdb prompt, or see the \"Debugging\n\
    with GDB\" manual.\n\
    To report any defects, comments, any suggestions on HP WDB,\n\
    send an email to \"wdb-help@cup.hp.com\".\n"
    , stream);
}


void
init_proc ()
{
}

void
proc_remove_foreign (int pid)
{
}

void
_initialize_main ()
{
#ifdef GDB_CRASH_HANDLER
  add_show_from_set
    (add_set_cmd ("debug-gdb", class_support, var_filename,
                  (char *) &debug_gdb,
                  "Set this to a valid debugger to use when gdb coredumps..\n\n\
Usage:\nTo set new value:\n\tset debug-gdb <FILE>\nTo see current value:\n\tshow\
 debug-gdb\n\n", &setlist), &showlist);
#endif

/* -mapshared command */
  add_show_from_set
    (add_set_cmd ("mapshared", class_support, var_boolean,
                  (char *) &mapshared,
                  "Set mapping all shared libraries loaded in the future shared \
instead of private.\n\nUsage:\nTo set new value:\n\tset mapshared [on | off]\n\
To see current value:\n\tshow mapshared\n\n", &setlist), &showlist);
}
