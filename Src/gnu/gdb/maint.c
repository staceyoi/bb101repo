/* Support for GDB maintenance commands.
   Copyright 1992, 1993, 1994 Free Software Foundation, Inc.
   Written by Fred Fish at Cygnus Support.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


#include "defs.h"
#include <ctype.h>
#include <signal.h>
#include "command.h"
#include "gdbcmd.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "demangle.h"
#include "gdbcore.h"
#include "expression.h"		/* For language.h */
#include "language.h"
#include "symfile.h"
#include "objfiles.h"
#include "value.h"

extern void _initialize_maint_cmds (void);

static void maintenance_command (char *, int);

static void maintenance_dump_me (char *, int);

static void maintenance_internal_error (char *args, int from_tty);

static void maintenance_demangle (char *, int);

static void maintenance_time_display (char *, int);

static void maintenance_space_display (char *, int);

static void maintenance_info_command (char *, int);

static void print_section_table (bfd *, asection *, PTR);

static void maintenance_info_sections (char *, int);

static void maintenance_print_command (char *, int);

static void maintenance_do_deprecate (char *, int);

/* Set this to the maximum number of seconds to wait instead of waiting forever
   in target_wait().  If this timer times out, then it generates an error and
   the command is aborted.  This replaces most of the need for timeouts in the
   GDB test suite, and makes it possible to distinguish between a hung target
   and one with slow communications.  */

int watchdog = 0;

/*

   LOCAL FUNCTION

   maintenance_command -- access the maintenance subcommands

   SYNOPSIS

   void maintenance_command (char *args, int from_tty)

   DESCRIPTION

 */

static void
maintenance_command (char *args, int from_tty)
{
  printf_unfiltered ("\"maintenance\" must be followed by the name of a maintenance command.\n");
  help_list (maintenancelist, "maintenance ", -1, gdb_stdout);
}

#ifndef _WIN32
/* ARGSUSED */
static void
maintenance_dump_me (char *args, int from_tty)
{
  if (query ("Should GDB dump core? "))
    {
#ifdef __DJGPP__
      /* SIGQUIT by default is ignored, so use SIGABRT instead.  */
      signal (SIGABRT, SIG_DFL);
      kill (getpid (), SIGABRT);
#else
      signal (SIGQUIT, SIG_DFL);
      kill (getpid (), SIGQUIT);
#endif
    }
}
#endif

/* Stimulate the internal error mechanism that GDB uses when an
   internal problem is detected.  Allows testing of the mechanism.
   Also useful when the user wants to drop a core file but not exit
   GDB. */

static void
maintenance_internal_error (char *args, int from_tty)
{
  internal_error ("internal maintenance");
}

/* Someday we should allow demangling for things other than just
   explicit strings.  For example, we might want to be able to specify
   the address of a string in either GDB's process space or the
   debuggee's process space, and have gdb fetch and demangle that
   string.  If we have a char* pointer "ptr" that points to a string,
   we might want to be able to given just the name and have GDB
   demangle and print what it points to, etc.  (FIXME) */

static void
maintenance_demangle (char *args, int from_tty)
{
  char *demangled;

  if (args == NULL || *args == '\0')
    {
      printf_unfiltered ("\"maintenance demangle\" takes an argument to demangle.\n");
    }
  else
    {
      demangled = cplus_demangle (args, DMGL_ANSI | DMGL_PARAMS);
      if (demangled != NULL)
	{
	  printf_unfiltered ("%s\n", demangled);
	  free (demangled);
	}
      else
	{
	  printf_unfiltered ("Can't demangle \"%s\"\n", args);
	}
    }
}

static void
maintenance_time_display (char *args, int from_tty)
{
  extern int display_time;

  if (args == NULL || *args == '\0')
    printf_unfiltered ("\"maintenance time\" takes a numeric argument.\n");
  else
    display_time = (int) strtol (args, NULL, 10);
}

static void
maintenance_space_display (char *args, int from_tty)
{
  extern int display_space;

  if (args == NULL || *args == '\0')
    printf_unfiltered ("\"maintenance space\" takes a numeric argument.\n");
  else
    display_space = (int) strtol (args, NULL, 10);
}

/* The "maintenance info" command is defined as a prefix, with
   allow_unknown 0.  Therefore, its own definition is called only for
   "maintenance info" with no args.  */

/* ARGSUSED */
static void
maintenance_info_command (char *arg, int from_tty)
{
  printf_unfiltered ("\"maintenance info\" must be followed by the name of an info command.\n");
  help_list (maintenanceinfolist, "maintenance info ", -1, gdb_stdout);
}

static void
print_section_table (bfd *abfd, asection *asect, PTR ignore)
{
  flagword flags;

  flags = bfd_get_section_flags (abfd, asect);

  printf_filtered ("    %s",
                   longest_local_hex_string_custom
		   ((LONGEST) bfd_section_vma (abfd, asect), "08l"));
  printf_filtered ("->%s",
                   longest_local_hex_string_custom
		   ((LONGEST) (  bfd_section_vma (abfd, asect)
			       + bfd_section_size (abfd, asect)),
		    "08l"));
  printf_filtered (" at %s",
		   local_hex_string_custom
		   ((unsigned long) asect->filepos, "08l"));
  printf_filtered (": %s", bfd_section_name (abfd, asect));

  if (flags & SEC_ALLOC)
    printf_filtered (" ALLOC");
  if (flags & SEC_LOAD)
    printf_filtered (" LOAD");
  if (flags & SEC_RELOC)
    printf_filtered (" RELOC");
  if (flags & SEC_READONLY)
    printf_filtered (" READONLY");
  if (flags & SEC_CODE)
    printf_filtered (" CODE");
  if (flags & SEC_DATA)
    printf_filtered (" DATA");
  if (flags & SEC_ROM)
    printf_filtered (" ROM");
  if (flags & SEC_CONSTRUCTOR)
    printf_filtered (" CONSTRUCTOR");
  if (flags & SEC_HAS_CONTENTS)
    printf_filtered (" HAS_CONTENTS");
  if (flags & SEC_NEVER_LOAD)
    printf_filtered (" NEVER_LOAD");
  if (flags & SEC_COFF_SHARED_LIBRARY)
    printf_filtered (" COFF_SHARED_LIBRARY");
  if (flags & SEC_IS_COMMON)
    printf_filtered (" IS_COMMON");

  printf_filtered ("\n");
}

/* ARGSUSED */
static void
maintenance_info_sections (char *arg, int from_tty)
{
  if (exec_bfd)
    {
      printf_filtered ("Exec file:\n");
      printf_filtered ("    `%s', ", bfd_get_filename (exec_bfd));
      wrap_here ("        ");
      printf_filtered ("file type %s.\n", bfd_get_target (exec_bfd));
      bfd_map_over_sections (exec_bfd, print_section_table, 0);
    }

  if (core_bfd)
    {
      printf_filtered ("Core file:\n");
      printf_filtered ("    `%s', ", bfd_get_filename (core_bfd));
      wrap_here ("        ");
      printf_filtered ("file type %s.\n", bfd_get_target (core_bfd));
      bfd_map_over_sections (core_bfd, print_section_table, 0);
    }
}

/* ARGSUSED */
void
maintenance_print_statistics (char *args, int from_tty)
{
  print_objfile_statistics ();
  print_symbol_bcache_statistics ();
}

void
maintenance_print_architecture (char *args, int from_tty)
{
  if (args == NULL)
    gdbarch_dump (current_gdbarch, gdb_stdout);
  else
    {
      struct ui_file *file = gdb_fopen (args, "w");
      if (file == NULL)
	perror_with_name ("maintenance print architecture");
      gdbarch_dump (current_gdbarch, file);    
      ui_file_delete (file);
    }
}

/* The "maintenance print" command is defined as a prefix, with
   allow_unknown 0.  Therefore, its own definition is called only for
   "maintenance print" with no args.  */

/* ARGSUSED */
static void
maintenance_print_command (char *arg, int from_tty)
{
  printf_unfiltered ("\"maintenance print\" must be followed by the name of a print command.\n");
  help_list (maintenanceprintlist, "maintenance print ", -1, gdb_stdout);
}

/* The "maintenance translate-address" command converts a section and address
   to a symbol.  This can be called in two ways:
   maintenance translate-address <secname> <addr>
   or   maintenance translate-address <addr>
 */

static void
maintenance_translate_address (char *arg, int from_tty)
{
  CORE_ADDR address;
  asection *sect;
  char *p;
  struct minimal_symbol *sym;
  struct objfile *objfile;

  if (arg == NULL || *arg == 0)
    error ("requires argument (address or section + address)");

  sect = NULL;
  p = arg;

  if (!isdigit (*p))
    {				/* See if we have a valid section name */
      while (*p && !isspace (*p))	/* Find end of section name */
	p++;
      if (*p == '\000')		/* End of command? */
	error ("Need to specify <section-name> and <address>");
      *p++ = '\000';
      while (isspace (*p))
	p++;			/* Skip whitespace */

      ALL_OBJFILES (objfile)
      {
	sect = bfd_get_section_by_name (objfile->obfd, arg);
	if (sect != NULL)
	  break;
      }

      if (!sect)
	error ("Unknown section %s.", arg);
    }

  address = parse_and_eval_address (p);

  if (sect)
    sym = lookup_minimal_symbol_by_pc_section (address, sect);
  else
    sym = lookup_minimal_symbol_by_pc (address);

  if (sym)
    printf_filtered ("%s+%s\n",
		     SYMBOL_SOURCE_NAME (sym),
		     paddr_u (address - SYMBOL_VALUE_ADDRESS (sym)));
  else if (sect)
    printf_filtered ("no symbol at %s:0x%s\n", sect->name, paddr (address));
  else
    printf_filtered ("no symbol at 0x%s\n", paddr (address));

  return;
}


/* When a comamnd is deprecated the user will be warned the first time
   the command is used.  If possible, a replacement will be
   offered. */

static void
maintenance_deprecate (char *args, int from_tty)
{
  if (args == NULL || *args == '\0')
    {
      printf_unfiltered ("\"maintenance deprecate\" takes an argument, \n\
the command you want to deprecate, and optionally the replacement command \n\
enclosed in quotes.\n");
    }

  maintenance_do_deprecate (args, 1);

}


static void
maintenance_undeprecate (char *args, int from_tty)
{
  if (args == NULL || *args == '\0')
    {
      printf_unfiltered ("\"maintenance undeprecate\" takes an argument, \n\
the command you want to undeprecate.\n");
    }

  maintenance_do_deprecate (args, 0);

}

/* You really shouldn't be using this. It is just for the testsuite.
   Rather, you should use deprecate_cmd() when the command is created
   in _initialize_blah().

   This function deprecates a command and optionally assigns it a
   replacement.  */

static void
maintenance_do_deprecate (char *text, int deprecate)
{

  struct cmd_list_element *alias = NULL;
  struct cmd_list_element *prefix_cmd = NULL;
  struct cmd_list_element *cmd = NULL;

  char *start_ptr = NULL;
  char *end_ptr = NULL;
  int len;
  char *replacement = NULL;

  if (text == NULL)
    return;

  if (!lookup_cmd_composition (text, &alias, &prefix_cmd, &cmd))
    {
      printf_filtered ("Can't find command '%s' to deprecate.\n", text);
      return;
    }

  if (deprecate)
    {
      /* look for a replacement command */
      start_ptr = strchr (text, '\"');
      if (start_ptr != NULL)
	{
	  start_ptr++;
	  end_ptr = strrchr (start_ptr, '\"');
	  if (end_ptr != NULL)
	    {
	      len = (int) (end_ptr - start_ptr);
	      start_ptr[len] = '\0';
	      replacement = xstrdup (start_ptr);
	    }
	}
    }

  if (!start_ptr || !end_ptr)
    replacement = NULL;


  /* If they used an alias, we only want to deprecate the alias.

     Note the MALLOCED_REPLACEMENT test.  If the command's replacement
     string was allocated at compile time we don't want to free the
     memory. */
  if (alias)
    {

      if (alias->flags & MALLOCED_REPLACEMENT)
	free (alias->replacement);

      if (deprecate)
	alias->flags |= (DEPRECATED_WARN_USER | CMD_DEPRECATED);
      else
	alias->flags &= ~(DEPRECATED_WARN_USER | CMD_DEPRECATED);
      alias->replacement = replacement;
      alias->flags |= MALLOCED_REPLACEMENT;
      return;
    }
  else if (cmd)
    {
      if (cmd->flags & MALLOCED_REPLACEMENT)
	free (cmd->replacement);

      if (deprecate)
	cmd->flags |= (DEPRECATED_WARN_USER | CMD_DEPRECATED);
      else
	cmd->flags &= ~(DEPRECATED_WARN_USER | CMD_DEPRECATED);
      cmd->replacement = replacement;
      cmd->flags |= MALLOCED_REPLACEMENT;
      return;
    }
}


static void
maintenance_log_beta (char *args, int from_tty)
{
  long  event_nbr;
  long  event_data;
  char* next_arg;

  if (args == NULL || *args == 0)
    error ("requires two integer arguments (event number and data)");
  
  event_nbr = strtol (args, &next_arg, 10);
  event_data = strtol (next_arg, NULL, 10);
  log_test_event ((int) event_nbr, (int) event_data);
} /* end maintenance_log_beta */

void
show_env_vars (char *args, int from_tty)
{
  printf_unfiltered("%s",
"\n"\
"Environment variables\n"\
"\n"\
"Gdb makes use of the following environment variables:\n"\
"      \n"\
"      BATCH_RTC              If this variable is the string \"on\", then \n"\
"                               batch mode RTC checking is turned on.  \n"\
"                               Otherwise it is off.\n"\
"      C                      Environment variable for the C locale.  If\n"\
"                               either C or POSIX is set, the locale is\n"\
"                               set to C\n"\
"      COLUMNS                The number of columns of the terminal display.\n"\
"                               Gdb will issue a warning for some display\n"\
"                               commands if the screen is too narrow\n"\
"      EDITOR                 Used in edit command to tell gdb what editor\n"\
"                               to invoke\n"\
"      EMACS                  If gdb is running under emacs, this environment\n"\
"                               variable should be set to anything\n"\
"      FIREBOLT_PATH          Environment variable to tell gdb it is running\n"\
"                               under firebolt.  Set by firebolt IDE.\n"\
"      GNUTARGET              See set gnutarget command\n"\
"      GDB_NO_BETA_MAIL       A beta release of gdb may be configured to send\n"\
"                               some usage information to HP.  The sending\n"\
"                               of this mail can be suppressed by setting\n"\
"                               this environment variable.\n"\
"      GDB_BETA_MAIL_ADDR     If beta mail is to be sent, the destination can\n"\
"                               be over-ridden with this\n"\
"      GDB_JAVA_UNWINDLIB     Tells where gdb should find the java library,\n"\
"                               libjunwind.sl, which assists in stack traces\n"\
"                               through java code.\n"\
"      GDB_ROOT               Gdb will look for gdb and librtc here.\n"\
"      GDB_SHLIB_PATH         Path string for searching for shared libraries\n"\
"      GDB_SHLIB_ROOT         Pseudo root for searching for shared libraries\n"\
"      GDB_SERVER             Used internally in batch mode RTC to find the\n"\
"                               gdb to invoke to print the RTC analysis.\n"\
"                               Used by other tools (e.g. vdb) to find gdb.\n"\
"      GDBHISTFILE            Gdb history file.  Defaults to .gdb_history\n"\
"      GDBRTC_CONFIG          If set, then gdb reads RTC configuration\n"\
"                               information from  the file rtcconfig in the \n"\
"                               current RTC working directory\n"\
"      HISTSIZE               How long is the command history (defaults to 256)\n"\
"      HOME                   Standard environment variable to define the users\n"\
"                               home directory path\n"\
"      LD_PRELOAD             This is used by gdb to arrange to pre-load librtc.\n"\
"                               It must be set to enable batch-mode run-time\n"\
"                               checking and to use run-time checking after\n"\
"                               attaching to a program.\n"\
"      LIBHPROF_SERVER        Gdb provies support for prospect.  The\n"\
"                               environment variable\n"\
"                               tells which library to LD_PRELOAD when preparing\n"\
"                               to do profiling.\n"\
"      LIBRTC_SERVER          Change the RTC pre-load library; higher precedence\n"\
"                               than GDB_ROOT\n"\
"      PATH                   Path string.  Used in resolving file to run.\n"\
"      POSIX                  Environment variable for the C locale.  If\n"\
"                               either C or POSIX is set, the locale is\n"\
"                               set to C\n"\
"      PS2                    Prompt string, typically.  If not set gdb uses '>'\n"\
"      RTC_INIT               This is only used for PA32.  If set, gdb will \n"\
"                               prepare to collect leak information when the\n"\
"                               program starts running.\n"\
"      SHELL                  What shell is invoked by the gdb shell command\n"\
"      TERM                   Standard environment variable for specifying\n"\
"                               the kind of terminal, e.g. \"hp\" or \"vt100\"\n"\
"                               or \"xterm\"\n"\
"      UWTHR_DEBUG            If set, overrides /dev/tty as to where thread\n"\
"                               unwinding debug information is written.\n"\
"\n");

}

void
_initialize_maint_cmds ()
{
  add_prefix_cmd ("maintenance", class_maintenance, maintenance_command,
		  "Commands for use by GDB maintainers.\n\
Includes commands to dump specific internal GDB structures in\n\
a human readable form, to cause GDB to deliberately dump core,\n\
to test internal functions such as the C++ demangler, etc.",
		  &maintenancelist, "maintenance ", 0,
		  &cmdlist);

  add_com_alias ("mt", "maintenance", class_maintenance, 1);

  add_prefix_cmd ("info", class_maintenance, maintenance_info_command,
     "Commands for showing internal info about the program being debugged.",
		  &maintenanceinfolist, "maintenance info ", 0,
		  &maintenancelist);
  add_alias_cmd ("i", "info", class_maintenance, 1, &maintenancelist);

  add_cmd ("sections", class_maintenance, maintenance_info_sections,
	   "List the BFD sections of the exec and core files.\n\nUsage:\n\t"
           "maintenance info sections\n",
	   &maintenanceinfolist);

  add_prefix_cmd ("print", class_maintenance, maintenance_print_command,
		  "Maintenance command for printing GDB internal state.",
		  &maintenanceprintlist, "maintenance print ", 0,
		  &maintenancelist);

#ifndef _WIN32
  add_cmd ("dump-me", class_maintenance, maintenance_dump_me,
	   "Get fatal error; make debugger dump its core.\n\n\
Usage:\n\tmaintenance dump-me\n\n\
GDB sets it's handling of SIGQUIT back to SIG_DFL and then sends\n\
itself a SIGQUIT signal.",
	   &maintenancelist);
#endif

  add_cmd ("internal-error", class_maintenance, maintenance_internal_error,
	   "Give GDB an internal error.\n\nUsage:\n\tmaintenance internal-error\n\n\
Cause GDB to behave as if an internal error was detected.\n",
	   &maintenancelist);

  add_cmd ("demangle", class_maintenance, maintenance_demangle,
	   "Demangle a C++ mangled name.\n\nUsage:\n\tmaintenance demangle\
 <NAME>\n\nCall internal GDB demangler routine to demangle a C++ link name\n\
and prints the result.",
	   &maintenancelist);

  add_cmd ("time", class_maintenance, maintenance_time_display,
	   "Set the display of time usage.\n\nUsage:\n\tmaintenance time <N>\n\n\
If nonzero, will cause the execution time for each command to be\n\
displayed, following the command's output.",
	   &maintenancelist);

  add_cmd ("space", class_maintenance, maintenance_space_display,
	   "Set the display of space usage.\n\nUsage:\n\tmaintenance space <N>\n\n\
If nonzero, will cause the execution space for each command to be\n\
displayed, following the command's output.",
	   &maintenancelist);

  add_cmd ("type", class_maintenance, maintenance_print_type,
	   "Print a type chain for a given symbol.\n\n\
Usage:\n\tmaintenance print type <SYM>\n\n\
For each node in a type chain, print the raw data for each member of\n\
the type structure, and the interpretation of the data.",
	   &maintenanceprintlist);

  add_cmd ("symbols", class_maintenance, maintenance_print_symbols,
	   "Print dump of current symbol definitions.\n\n\
Usage:\n\tmaintenance print symbols <OUTFILE> [<SYM-FILE>]\n\n\
Entries in the full symbol table are dumped to file OUTFILE.\n\
If a symbol file 'SYM-FILE' is specified, dump only that file's symbols.",
	   &maintenanceprintlist);

  add_cmd ("msymbols", class_maintenance, maintenance_print_msymbols,
	   "Print dump of current minimal symbol definitions.\n\n\
Usage:\n\tmaintenance print msymbols <OUTFILE> [<SYM-FILE>]\n\n\
Entries in the minimal symbol table are dumped to file OUTFILE.\n\
If a symbol file 'SYM-FILE' is specified, dump only that file's minimal symbols.",
	   &maintenanceprintlist);

  add_cmd ("psymbols", class_maintenance, maintenance_print_psymbols,
	   "Print dump of current partial symbol definitions.\n\n\
Usage:\n\tmaintenance print psymbols <OUTFILE> [<SYM-FILE>]\n\n\
Entries in the partial symbol table are dumped to file OUTFILE.\n\
If a symbol file 'SYM-FILE' is specified, dump only that file's partial symbols.",
	   &maintenanceprintlist);

  add_cmd ("objfiles", class_maintenance, maintenance_print_objfiles,
	   "Print dump of current object file definitions.\n\n"
           "Usage:\n\tmaintenance print objfiles\n",
	   &maintenanceprintlist);

  add_cmd ("statistics", class_maintenance, maintenance_print_statistics,
	   "Print statistics about internal gdb state.\n\n"
           "Usage:\n\tmaintenance print statistics\n",
	   &maintenanceprintlist);

  add_cmd ("architecture", class_maintenance, maintenance_print_architecture,
	   "Print the internal architecture configuration.\n\nUsage:\n\t"
           "maintenance print architecture [<FILE>]\n\nTakes an optional file"
           " parameter.",
	   &maintenanceprintlist);

  add_cmd ("check-symtabs", class_maintenance, maintenance_check_symtabs,
	   "Check consistency of psymtabs and symtabs.\n\nUsage:\n\t"
           "maintenance check-symtabs\n",
	   &maintenancelist);

  add_cmd ("translate-address", class_maintenance, maintenance_translate_address,
	   "Translate a section name and address to a symbol.\n\nUsage:\n\t"
           "maintenance translate-address <ADDR> | <SECTION ADDR>\n\n",
	   &maintenancelist);

  add_cmd ("deprecate", class_maintenance, maintenance_deprecate,
	   "Deprecate a command.\n\nUsage:\n\tmaintenance deprecate <CMD1> [\"<CMD2>\"]\
\n\nwhere 'CMD1' is the cammand to deprecated and 'CMD2' is optional replacement\n\
Note that this is just in here so the testsuite can check the command deprecator.\n\
You probably shouldn't use this, rather you should use the C function deprecate_cmd().\n",
            &maintenancelist);

  add_cmd ("undeprecate", class_maintenance, maintenance_undeprecate,
	   "Undeprecate a command.\n\nUsage:\n\tmaintenance undeprecate <CMD>\n\n\
Note that this is just in here so the testsuite can check the comamnd deprecator.\n\
You probably shouldn't use this.",
	   &maintenancelist);

  add_cmd ("log_beta", class_maintenance, maintenance_log_beta,
           "Log an event and an integer for which the max is kept for mail \n"
             "sent during beta testing.\n\nUsage:\n\tmaintenance log_beta <N> <DATA>"
             "\n\nTwo arguments: event number and data",
           &maintenancelist);

  add_show_from_set (
		      add_set_cmd ("watchdog", class_maintenance, var_zinteger, (char *) &watchdog,
				   "Set watchdog timer.\n\nUsage:\nTo set new value:\
\n\tset watchdog <INTEGER>\nTo see current value:\n\tshow watchdog\n\n\
When non-zero, this timeout is used instead of waiting forever for a target to\n\
finish a low-level step or continue operation.  If the specified amount of time\n\
passes without a response from the target, an error occurs.", &setlist),
		      &showlist);
  add_cmd ("envvars", class_support, show_env_vars,
	   "Show the environment variables used by gdb\n\n\
Usage:\n\tshow envvars\n", &showlist);
}
