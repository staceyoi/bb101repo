/* GDB routines for manipulating the minimal symbol tables.
   Copyright 1992, 93, 94, 96, 97, 1998 Free Software Foundation, Inc.
   Contributed by Cygnus Support, using pieces from other GDB modules.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */


/* This file contains support routines for creating, manipulating, and
   destroying minimal symbol tables.

   Minimal symbol tables are used to hold some very basic information about
   all defined global symbols (text, data, bss, abs, etc).  The only two
   required pieces of information are the symbol's name and the address
   associated with that symbol.

   In many cases, even if a file was compiled with no special options for
   debugging at all, as long as was not stripped it will contain sufficient
   information to build useful minimal symbol tables using this structure.

   Even when a file contains enough debugging information to build a full
   symbol table, these minimal symbols are still useful for quickly mapping
   between names and addresses, and vice versa.  They are also sometimes used
   to figure out what full symbol table entries need to be read in. */


#include "defs.h"
#include <ctype.h>
#include "gdb_string.h"
#include "symtab.h"
#include "bfd.h"
#include "symfile.h"
#include "objfiles.h"
#include "demangle.h"
#include "gdb-stabs.h"
#include "target.h"             /* for target_has_stack */
#include "fastsym.h"

/* Accumulate the minimal symbols for each objfile in bunches of BUNCH_SIZE.
   At the end, copy them all into one newly allocated location on an objfile's
   symbol obstack.  */

/* srikanth, 000615, bumped up the bunch size. Typical load modules
   have several thousand symbols. Allocating big bunches reduces the
   fragmentation into many small heap blocks.
*/

#define BUNCH_SIZE 16 * 1024

int num_symtab_arr;

struct msym_bunch
  {
    struct msym_bunch *next;
    struct minimal_symbol contents[BUNCH_SIZE];
  };

/* Bunch currently being filled up.
   The next field points to chain of filled bunches.  */

static struct msym_bunch *msym_bunch;

/* Number of slots filled in current bunch.  */

static int msym_bunch_index;

/* Total number of minimal symbols recorded so far for the objfile.  */

static int msym_count;

extern int lazy_demangling;

extern int debugging_dld;

char* dld_name;

/* JAGaf60778 - info line _main_ fails for +objdebug compiled fortran executable */
extern int main_line; /* Defined in source.c */
/* JAGaf60778 - <END> */

static int mangled_prefix_len = 0;

/* Prototypes for local functions. */

static int compare_minimal_symbols (const void *, const void *);

static int
compact_minimal_symbols (struct minimal_symbol *, int, struct objfile *);

#ifdef BUCKET_SORT_MINSYMS

/* variables to record the lowest and highest addresses in the
   load module. These values are used to scatter the symbols
   into different partitions.
*/
static CORE_ADDR lowest_address, highest_address;

static int null_symbol_present = 0;

static int
compact_minimal_symbol_partition (struct minsym_address_partition *);

static void
sort_minimal_symbol_partition_by_address (struct minsym_address_partition *);

static void
sort_minimal_symbol_partition_by_name (struct minsym_name_partition *);
#endif

#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
static void add_minsym_to_demangled_hash_table (struct minimal_symbol *sym,
						struct minimal_symbol **table);

/* Compute a hash code based using the same criteria as `strcmp_iw'.  */

unsigned int
msymbol_hash_iw (const char *string)
{
  unsigned int hash = 0;
  while (*string && *string != '(')
    {
      while (isspace (*string))
	++string;
      if (*string && *string != '(')
	hash = (31 * hash) + *string;
      ++string;
    }
  return hash % MINIMAL_SYMBOL_HASH_SIZE;
}

/* Compute a hash code for a string.  */

unsigned int
msymbol_hash (const char *string)
{
  unsigned int hash = 0;
  for (; *string; ++string)
    hash = (31 * hash) + *string;
  return hash % MINIMAL_SYMBOL_HASH_SIZE;
}

/* Add the minimal symbol SYM to an objfile's minsym hash table, TABLE.  */
void
add_minsym_to_hash_table (struct minimal_symbol *sym,
			  struct minimal_symbol **table)
{
  if (sym->hash_next == NULL)
    {
      unsigned int hash = msymbol_hash (SYMBOL_NAME (sym));
      sym->hash_next = table[hash];
      table[hash] = sym;
    }
}
#endif

#ifdef BUCKET_SORT_MINSYMS
/* compare_minimal_symbol_by_address : this is a clone of
   compare_minimal_symbols except that this receives two `struct 
   minimal_symbol **' as opposed to two `struct minimal_symbol *'
*/
static int
compare_minimal_symbols_by_address (fn1p, fn2p)
     const PTR fn1p;
     const PTR fn2p;
{
  register const struct minimal_symbol **fn1;
  register const struct minimal_symbol **fn2;

  fn1 = (const struct minimal_symbol **) fn1p;
  fn2 = (const struct minimal_symbol **) fn2p;

  if (SYMBOL_VALUE_ADDRESS (*fn1) < SYMBOL_VALUE_ADDRESS (*fn2))
    {
      return (-1);		/* addr 1 is less than addr 2 */
    }
  else if (SYMBOL_VALUE_ADDRESS (*fn1) > SYMBOL_VALUE_ADDRESS (*fn2))
    {
      return (1);		/* addr 1 is greater than addr 2 */
    }
  else
    /* addrs are equal: sort by name */
    {
      char *name1 = SYMBOL_NAME (*fn1);
      char *name2 = SYMBOL_NAME (*fn2);

      if (name1 && name2)	/* both have names */
	return strcmp (name1, name2);
      else if (name2)
	return 1;		/* fn1 has no name, so it is "less" */
      else if (name1)		/* fn2 has no name, so it is "less" */
	return -1;
      else
	return (0);		/* neither has a name, so they're equal. */
    }
}

static int
compare_minimal_symbols_by_name (p1, p2)
    const void * p1;
    const void * p2;
{
  int result;

  struct minimal_symbol **m1 = (struct minimal_symbol **) p1;
  struct minimal_symbol **m2 = (struct minimal_symbol **) p2;

  char *name1 = SYMBOL_NAME (*m1);
  char *name2 = SYMBOL_NAME (*m2);

  result = strcmp (name1, name2);

  if (result)
    return result;

  if (SYMBOL_VALUE_ADDRESS (*m1) < SYMBOL_VALUE_ADDRESS (*m2))
    return -1;
  else if (SYMBOL_VALUE_ADDRESS (*m1) > SYMBOL_VALUE_ADDRESS (*m2))
    return 1;
  else 
    return 0;
}
#endif


#ifndef HASH_ALL
/* compare_minimal_symbols_by_prefix : a prefix sensitive comparator. We
   have a match if the search key is a prefix of the candidate msymbol.
   Uses file global variable 'mangled_prefix_len'. This is necessitated
   because there is no way to have it passed as the third argument to this
   function and furthermore because we cannot rely on the search key being
   passed consistently in p1 or p2 below.

   Attempting to compare upto the length of the shortest string is an
   attractive incorrect alternative. (think of what happens at the outer
   layers when the candidate is "sort" and the search key is "sort_it".)
*/

static int
compare_minimal_symbols_by_prefix (p1, p2)
    const void * p1;
    const void * p2;
{
  struct minimal_symbol **m1, **m2;
  char * name1, *name2;

  m1 = (struct minimal_symbol **) p1;
  m2 = (struct minimal_symbol **) p2;

  name1 = SYMBOL_NAME (*m1);
  name2 = SYMBOL_NAME (*m2);

  return strncmp (name1, name2, mangled_prefix_len);
}
#endif	/* !HASH_ALL */
        
/* demangle_the_whole_world : useful when we have read in some symbol 
   table(s) in deferred demangling mode and user for whatever reason
   issues a "set lazy-demangling off" command.
*/

void 
demangle_the_whole_world()
{
  struct objfile * objfile;
  struct minimal_symbol * msymbol;

  ALL_MSYMBOLS (objfile, msymbol)
    {
      if (SYMBOL_LANGUAGE(msymbol) == language_auto)
        SYMBOL_INIT_DEMANGLED_NAME(msymbol, &objfile->symbol_obstack);
    }
}


/* srikanth, an iterator for functions in the linker symbol table.
   Looks up `name' and for each matching symbol, calls `action' and
   passes the selected symbol.
*/

#ifdef AUTO_LOAD_DEBUG_INFO
/* to prevent auto loading being called recursively */
int auto_loading = 0; 
#endif
/* Bindu: Collect the minsyms and corresponding objfiles that match the given
 * name into minsym_and_obj_arr.  Returns total number of minsyms found.
 */
/* Poorva: Put both get_text_minsyms_objfile and foreach_text_minsym 
   together */

int
foreach_text_minsym (char *name,
                     void (*action) (struct minimal_symbol *),
                     struct minsym_and_objfile *minsym_and_obj_arr,
                     int look_for_all)
{
  struct objfile *objfile;
  struct minimal_symbol m, *msymbol = &m;
  char * mangled_prefix;
  num_symtab_arr = 0;
  int minsym_count = 0, count = 0, i = 0;
  int found_type = 0;
  int loop_count = 0;

  /* There are different ways of calling this function. One can call it
     with action set or with the minsym_and_obj_arr allocated.
     If called with action then for all the matching minsyms the 
     'action' function will be called.
     If called with the minsym_and_obj_arr then all matching minsyms
     are put into this array and the total count is returned */
  /* The loop_count counts which objfile is it.
     We don't want to find dld routines.
     Problem is if a user sets a
       break printf
       gdb sets a deferred breakpoint. The first
       printf it finds is the one in dld so it sets it
       there. The user sees a breakpoint set but never
       hits it because he wants the one in libc.
       
       So for now don't look for symbols in dld if
       debugging_dld is not set.
       
       The reason we check to see if count is 2 is because
       dld will be the second load module to be loaded.
       The first is the a.out and the second if there
       is more than one will be dld. We don't have a
       micro loader for now.
       
       If later it is discovered that dld is not the second
       on the list then change to be just an strstr.
       Test with attach and corefiles too.
       For corefiles we want to debug dld since we look for
       the load info structure.
       */

#ifdef HASH_ALL
  ALL_OBJFILES (objfile)
    {
      current_objfile = objfile;
      loop_count ++;
      if (!(target_has_stack && !target_has_execution))
	if (!debugging_dld)
	  if (   (loop_count == 2 || IS_TARGET_LRE) 
	      && dld_name
	      && strstr (objfile->name, dld_name))
	    continue;

      count = retrieve_all_from_table (*objfile->hash_table, name, 
				       action, minsym_and_obj_arr, 
				       minsym_count, look_for_all);

      if (minsym_and_obj_arr) 
	{
          int text_syms_found = 0;  /* number of text-based symbols found */

	  for (i = minsym_count; 
	       (i < (minsym_count + count)) && (i < max_synonyms_seen); ++i)
	    {
	       enum minimal_symbol_type mst;
	       mst = MSYMBOL_TYPE (minsym_and_obj_arr[i].minsym);
	       if (mst == mst_text || mst == mst_file_text)
		 {
	            minsym_and_obj_arr[minsym_count + text_syms_found].objfile =
		      objfile;
	            minsym_and_obj_arr[minsym_count + text_syms_found].minsym =
		      minsym_and_obj_arr[i].minsym;
	  	    text_syms_found++;
		 }
	    }
	    minsym_count += text_syms_found;
	}
    }
  
  if (minsym_and_obj_arr) 
    return minsym_count;
  else
    return 0;
#else

  if (lazy_demangling)
    {
      char *buffer;
      int len;

      /* It is not clear that while the iterator is running, the function
         quick_lookup_minimal_symbol() would not get called somehow by
         action() or one of its descendants. If this happens, the global
         state used by the iterator (the return value of obtain_aCC* and
         the mangled_prefix_len) would get erased, causing problems in the
         iteration. Just be paranoid and make a local copy. See that once
         bsearch returns there are no consumers for the said global state.
       */

      buffer = obtain_aCC_mangled_prefix (name);
      mangled_prefix = (char *) alloca (strlen (buffer) + 1);
      strcpy (mangled_prefix, buffer);
      mangled_prefix_len = len = strlen (mangled_prefix);

minsym_search: /* JAGaf60778 */
      SYMBOL_NAME (msymbol) = mangled_prefix;

      ALL_OBJFILES (objfile)
      {
	struct minimal_symbol **m;

       if (!(target_has_stack && !target_has_execution))
        if (!debugging_dld)
          if (
#ifdef HP_IA64
	      (loop_count == 2 || IS_TARGET_LRE) &&
#endif
              dld_name
              && strstr (objfile->name, dld_name))
            continue;

#ifdef BUCKET_SORT_MINSYMS
	struct minsym_name_partition * partition;

	if (!objfile->msymbols)
	  continue;

	/* srikanth, 000615, locate and sort the partition ... */
	partition = objfile->minsym_name_partitions + mangled_prefix[0];

	sort_minimal_symbol_partition_by_name (partition);

	m = bsearch (&msymbol,
		     partition->msymbols,
		     partition->minimal_symbol_count,
		     sizeof (struct minimal_symbol *),
		     compare_minimal_symbols_by_prefix);
                         
	if (m == NULL)
	  continue;
  
	/* Obtain a pointer to the first matching entry */           
  
	while ( m > partition->msymbols)
	  if (!strncmp(SYMBOL_NAME(*(m-1)), mangled_prefix, len))
	    m--;
	  else 
	    break;
    
#else /* #ifdef BUCKET_SORT_MINSYMS */
	m = bsearch (&msymbol,
		     objfile->alphabetic_msymbols,
		     objfile->minimal_symbol_count,
		     sizeof (struct minimal_symbol *),
		     compare_minimal_symbols_by_prefix);

	if (m == NULL)
	  continue;

	/* Obtain a pointer to the first matching entry */

	while (m > objfile->alphabetic_msymbols)
	  if (!strncmp (SYMBOL_NAME (*(m - 1)), mangled_prefix, len))
	    m--;
	  else
	    break;
#endif /* else #ifdef BUCKET_SORT_MINSYMS */

/* JAGaf60778 - info line  _main_ fails for +objdebug compiled fortran executable.
   Using msymbol <_start>'s address, search the corresponding minimal symbol (minimal
   symbol of  fortran program name) */
#ifndef HP_IA64
        if (main_line) 
	  {
	    struct minimal_symbol *minsym;
	    CORE_ADDR pc;
	    
	  /* Now we have got a pointer to <_start> msymbol. 
	     Get <_start> symbol's address. */
	    pc = (CORE_ADDR)(*m)->ginfo.value.address;
	  
	  /* Suppose we have a fortran executable of program name <cube>,
	     Search for <cube>'s minimal symbol using <_start>'s address. */
	    minsym = lookup_minimal_symbol_by_pc(pc);
	    
	    strcpy (name,SYMBOL_NAME(minsym));
	    buffer = obtain_aCC_mangled_prefix (name);
            mangled_prefix = (char *) alloca (strlen (buffer) + 1);
            strcpy (mangled_prefix, buffer);
            mangled_prefix_len = len = strlen (mangled_prefix);
	    main_line=0;
	    
	  /* Let's goto minsym_search so that we may obtain a pointer to msymbol <name> */
	    goto minsym_search;
	  }
#endif
/* JAGaf60778 - <END> */
	for (; *m; m++)
	  {
	    struct minimal_symbol *msymbol = *m;

	    if (strncmp (SYMBOL_NAME (msymbol), mangled_prefix, len))
	      break;

	    if (SYMBOL_LANGUAGE (msymbol) == language_auto)
	      SYMBOL_INIT_DEMANGLED_NAME (msymbol, &objfile->symbol_obstack);

	    if (SYMBOL_MATCHES_NAME (msymbol, name))
	      {
		enum minimal_symbol_type mst;

		mst = MSYMBOL_TYPE (msymbol);
		if (mst == mst_text || mst == mst_file_text)
		  {
		    if (action)
		      action (msymbol);
		    else
		      {
			if (minsym_count < max_synonyms_seen)
			  {
			    /* populate the minsyms_obj array */
			    minsym_and_obj_arr[minsym_count].minsym = msymbol;
			    minsym_and_obj_arr[minsym_count].objfile = objfile;
			    minsym_count++;
			  }
		      }
		  }
	      }
	  }
      }
    }
  else
    /* anticipatory demangling : do it the painful way */
    {
      int i;
      extern int num_symbol_names;
      extern char **symbol_names;
      ALL_MSYMBOLS (objfile, msymbol)
      {
	if (MSYMBOL_TYPE (msymbol) == mst_text ||
	    MSYMBOL_TYPE (msymbol) == mst_file_text)
	  if (!num_symbol_names)
	    {
	      if (SYMBOL_MATCHES_NAME (msymbol, name))
		{
		  if (action)
		    action (msymbol);
		  else 
		    {
		      if (minsym_count < max_synonyms_seen)
                      {
                        /* populate the minsyms_obj array */
                        minsym_and_obj_arr[minsym_count].minsym = msymbol;
                        minsym_and_obj_arr[minsym_count].objfile = objfile;
                        minsym_count++;
                      }
		    }
		}
	    }
	  else
	    {
	      /* For namespace support we may be required to look
		 for more than one function. 
		 e.g b foo 
		 would mean look for foo, A::foo and A::B::foo
		 We look for foo in each text minsym. If we 
		 find it then we demangle it and check to see if
		 it is in our list of functions to look for. 
		 */

	      if (SYMBOL_CONTAINS_NAME (msymbol, name)) 
		{
		  /* For templates we want to be able to match
		     Foo::bar with Foo<char>::bar hence we use the 
		     strcmp_iwt instead of just strcmp_iw */
		  if (strcmp_iwt (SYMBOL_DEMANGLED_NAME (msymbol), name) == 0)
		    {
		      if (action)
			action (msymbol);
		      else
			{
			  if (minsym_count < max_synonyms_seen)
			    {
			      /* populate the minsyms_obj array */
			      minsym_and_obj_arr[minsym_count].minsym =msymbol;
			      minsym_and_obj_arr[minsym_count].objfile=objfile;
			      minsym_count++;
			    }
			}
		      break;
		    }
  		  if (SYMBOL_LANGUAGE (msymbol) == language_cplus)
		    {
		      /* For templates we want to be able to match
			 Foo::bar with Foo<char>::bar hence we use the 
			 strcmp_iwt instead of just strcmp_iw */

		      for (i = 0; i < num_symbol_names ; ++i)
			if (strcmp_iwt (SYMBOL_DEMANGLED_NAME (msymbol), 
				       symbol_names[i]) == 0)
			  {
			    if (action)
			      action (msymbol);
			    else 
			      {
				if (minsym_count < max_synonyms_seen)
				  {
				    /* populate the minsyms_obj array */
				    minsym_and_obj_arr[minsym_count].minsym 
				      = msymbol;
				    minsym_and_obj_arr[minsym_count].objfile 
				      = objfile;
				    minsym_count++;
				  }
			      }
			    break;
			  }
		    }
		}
	    }
      } /* End of ALL_MSYMBOLS */
    } /* End of else */

  if (minsym_and_obj_arr) 
    return minsym_count;
  else
    return 0;
#endif
}


#ifdef HASH_ALL
/* Poorva: For the performance gdb we call retrieve from table from this
   function. The mpsym chosen is the one that matches the parameters most
   closely. 
   name : name of the minsym to be looked up. Could be mangled or demangled.
   objf : If the user passes an objfile in then look only in that 
          objfile else look everywhere.
   mst_sought: The type of minsym sought - could be static, trampoline, global.
   namesp: namespace to look in it for (not C++ namespace) but VAR_NAMESPACE
           or STRUCT_NAMESPACE. Look in VAR_NAMESPACE if it is a variable
	   or a function, for a type look in struct_namespace.

   found_symbol: signifies we have found a global.
   found_file_symbol: signifies we have found a static symbol.
   found_trampoline_symbol: signifies we have found a trampoline symbol.
   close_match: signifies that we found a symbol of the same name but some
                of the other parameters like namespace did not match.

   If we find a global symbol in any of the objfiles then we are done - we
   do not need to look further. Else we continue our search in the other
   objfiles too.
   
   The return value is 
   Global: if we found one.
   If no globals found then the last static is returned.
   If no globals or statics then the last trampoline 
   symbol found is returned.
   If neither of the three is found then we return the close_match symbol.

 */

struct minimal_symbol *
hash_lookup_minimal_symbol (register const char *name,
                            const char *sfile,
                            struct objfile *objf,
                            enum minimal_symbol_type mst_sought,
                            namespace_enum namesp)
{

  struct objfile *objfile = NULL;
  struct minimal_symbol m, *msymbol = &m;
  struct minimal_symbol *found_symbol = NULL;
  struct minimal_symbol *found_file_symbol = NULL;
  struct minimal_symbol *trampoline_symbol = NULL;
  struct minimal_symbol *close_match = NULL;
  int sym_type = -1;
  int count = 0;

  if (!namesp)
    namesp = VAR_NAMESPACE;

  ALL_OBJFILES (objfile)
    {
      /* We shall break out later if we find that objf is set 
	 and hence go through this loop only once. */
      if (objf)
	objfile = objf;
      else
	{
	  /* Poorva: we don't want to find dld routines.
	     Problem is if a user sets a
	       break printf 
	     gdb sets a deferred breakpoint. The first
	     printf it finds is the one in dld so it sets it
	     there. The user sees a breakpoint set but never
	     hits it because he wants the one in libc. 

	     So for now don't look for symbols in dld if 
	     debugging_dld is not set. 

	     The reason we check to see if count is 2 is because
	     dld will be the second load module to be loaded.
	     The first is the a.out and the second if there
	     is more than one will be dld. We don't have a
	     micro loader for now. 
             
	     If later it is discovered that dld is not the second
	     on the list then change to be just an strstr.
	     Test with attach and corefiles too.
	     For corefiles we want to debug dld since we look for
	     the load info structure.
	   */
	  count ++;
	  if (!(target_has_stack && !target_has_execution))
	    if (!debugging_dld)
	      if (
#ifdef HP_IA64
		     (count == 2) &&
#endif
		  strstr (objfile->name, dld_name))
	        continue;
	}

      if (found_symbol == NULL)
	{
	  current_objfile = objfile;
          /* jini: Converted mst_unknown to mst_sought below. The mst_sought
          argument that was being passed above was not being used at all. */
	  msymbol = (struct minimal_symbol *) 
	    retrieve_from_table (*objfile->hash_table, (char *) name, 0, 
				 mst_sought, &sym_type, 0, namesp, NULL); 
	}
      else
	break;

      if (msymbol)
	{
	  if (sym_type == GLOB)
	    found_symbol = msymbol;
	  else if (sym_type == STAT)
	    found_file_symbol = msymbol;
	  else if (sym_type == TRAMP)
	    trampoline_symbol = msymbol;
	  else
	    close_match = msymbol;
	}

      if (objf)
	break;
    }


  if (found_symbol == NULL && found_file_symbol == NULL &&
      trampoline_symbol == NULL)
    return close_match;

  /* External symbols are best.  */
  if (found_symbol)
    return found_symbol;

  /* File-local symbols are next best.  */
  if (found_file_symbol)
    return found_file_symbol;
  
  /* Symbols for shared library trampolines are next best.  */
  if (trampoline_symbol)
    return trampoline_symbol;

  return NULL;
}
#endif /* #ifdef HASH_ALL */

#if defined(GDB_TARGET_IS_HPUX) || defined(HP_IA64_GAMBIT)

#ifndef HASH_ALL
struct minimal_symbol *
quick_lookup_minimal_symbol (name, sfile, objf, mst_sought)
     register const char *name;
     const char *sfile;
     struct objfile *objf;
     enum minimal_symbol_type mst_sought;
{

  struct objfile *objfile;
  struct minimal_symbol m, *msymbol = &m;
  struct minimal_symbol *found_exported_symbol = NULL;
  struct minimal_symbol *found_symbol = NULL;
  struct minimal_symbol *found_file_symbol = NULL;
  struct minimal_symbol *trampoline_symbol = NULL;
  char * mangled_prefix;

  mangled_prefix = obtain_aCC_mangled_prefix (name);
  mangled_prefix_len = strlen (mangled_prefix);

  SYMBOL_NAME (msymbol) = mangled_prefix;

#ifdef SOFUN_ADDRESS_MAYBE_MISSING
  if (sfile != NULL)
    {
      char *p = strrchr (sfile, '/');
      if (p != NULL)
	sfile = p + 1;
    }
#endif

  for (objfile = object_files;
       objfile != NULL && found_exported_symbol == NULL;
       objfile = objfile->next)
    {
      if (!objfile->msymbols)
        continue;

      if (objf == NULL || objf == objfile)
        {
#ifdef LAZY_DEMANGLING
          struct minimal_symbol ** m; 
          struct minsym_name_partition * partition;

          partition = objfile->minsym_name_partitions + mangled_prefix[0];

          sort_minimal_symbol_partition_by_name (partition);

          m = bsearch (&msymbol,
                       partition->msymbols,
                       partition->minimal_symbol_count,
                       sizeof (struct minimal_symbol *),
                       compare_minimal_symbols_by_prefix);
                     
          if (m == NULL)
            continue;

          /* Obtain a pointer to the first matching entry */           

          while ( m > partition->msymbols)
            if (!strncmp(SYMBOL_NAME(*(m-1)), mangled_prefix, 
                                           mangled_prefix_len))
                  m--;
            else 
                break;

#else /* ifdef LAZY_DEMANGLING */

	  struct minimal_symbol **m;

	  m = bsearch (&msymbol,
		       objfile->alphabetic_msymbols,
		       objfile->minimal_symbol_count,
		       sizeof (struct minimal_symbol *),
		       compare_minimal_symbols_by_prefix);

	  if (m == NULL)
	    continue;

	  /* Obtain a pointer to the first matching entry */

	  while (m > objfile->alphabetic_msymbols)
	    if (!strncmp (SYMBOL_NAME (*(m - 1)), mangled_prefix,
			  mangled_prefix_len))
	      m--;
	    else
	      break;

#endif /* else ifdef LAZY_DEMANGLING */

	  for (; *m && found_exported_symbol == NULL; m++)
	    {
	      struct minimal_symbol *msymbol = *m;

              if (MSYMBOL_OBSOLETED (msymbol))
                continue;

              if (strncmp(SYMBOL_NAME(msymbol), mangled_prefix,
                                              mangled_prefix_len))
                break;

              if (SYMBOL_LANGUAGE(msymbol) == language_auto)
                SYMBOL_INIT_DEMANGLED_NAME(msymbol, &objfile->symbol_obstack);

              if (SYMBOL_MATCHES_NAME (msymbol, name))
                {
#ifdef AUTO_LOAD_DEBUG_INFO
                  /* FIXME : This has hardcoded calls to som library manager */
                  if (objfile->psymtabs == 0 && objfile->sf->sym_add_psymtabs)
                    if (!objfile->has_no_dash_g_table)
		      /* check if we are already auto loading */
		      if (!auto_loading)
			{
			  auto_loading = 1;
                          som_solib_add_sharedlibrary (objfile->name, 1, 0);
			  auto_loading = 0;
			}
#endif
                  switch (MSYMBOL_TYPE (msymbol))
                    {
                    case mst_file_data:
                    case mst_file_bss:

                      if (mst_sought == mst_text)
                        break;
                      /* else fall through ... */

                    case mst_file_text:

                      if (mst_sought == mst_solib_trampoline)
                        break;

#ifdef SOFUN_ADDRESS_MAYBE_MISSING
                      if (sfile == NULL || STREQ (msymbol->filename, sfile))
                        found_file_symbol = msymbol;
#else
                      /* We have neither the ability nor the need to
                         deal with the SFILE parameter.  If we find
                         more than one symbol, just return the latest
                         one (the user can't expect useful behavior in
                         that case).  */
                      found_file_symbol = msymbol;
#endif
                      break;

                    case mst_solib_trampoline:

                      /* if we are specifically searching for a trampoline
                         symbol, we are done. Otherwise, we will continue
                         to look for the real symbol. If none is found, we 
                         will use the trampoline if appropriate.
                      */

                      if (mst_sought == mst_solib_trampoline)
                        found_exported_symbol = msymbol;
                      else 
                      if (mst_sought != mst_text)
                        if (trampoline_symbol == NULL && target_has_stack)
                          trampoline_symbol = msymbol;
                      break;

                    case mst_text :
                      if (mst_sought != mst_solib_trampoline)
		        {
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
                          if ((objfile->flags & OBJF_SHARED) &&
			      !objfile->export_list)
                            init_export_symbols (objfile);
#endif

     /* srikanth, 000210, the code here is using different symbol lookup
        semantics than the linker. As a result we set breakpoints on
        wrong functions if the a.out as well as a .sl defines the same
        function foo(). foo() will not be exported from the main module
        under the normal circumstances while foo from the .sl will be
        (unless the user took the pains to hide it.) So if we found a
        mst_text symbol from the a.out let us consider it as good as
        the greenback. We will still continue to consult the export
        list for resolving conflicts among .sls and live with the
        linear search (ouch).
     */
                          if (!(objfile->flags & OBJF_SHARED))
                            found_exported_symbol = msymbol;
                          else if (find_export_entry((char*) name, objfile))
                            found_exported_symbol = msymbol;
                          else if (found_symbol == NULL)
                            found_symbol = msymbol;
                        }
                      break;

                    case mst_unknown:
                    default:
                      if (mst_sought != mst_solib_trampoline &&
                                          mst_sought != mst_text)
                        {
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
                          if ((objfile->flags & OBJF_SHARED) &&
			      !objfile->export_list)
                            init_export_symbols (objfile);
#endif
                          if (!(objfile->flags & OBJF_SHARED))
                            found_exported_symbol = msymbol;
                          else if (find_export_entry((char*) name, objfile))
                            found_exported_symbol = msymbol;
                          else if (found_symbol == NULL)
                            found_symbol = msymbol;
                        }
                      break;
                    } /* switch (MSYMBOL_TYPE (msymbol)) */
                } /* if (SYMBOL_MATCHES_NAME (msymbol, name)) */
            } /* for (; *m && found_symbol == NULL; m++) */
        } /* if (objf == NULL || objf == objfile) */
    } /* for objfile != NULL && found_symbol == NULL */
  
  /* Exported symbols are best.  */
  if (found_exported_symbol)
    return found_exported_symbol;

  /* External symbols are next best.  */
  if (found_symbol)
    return found_symbol;

  /* File-local symbols are next best.  */
  if (found_file_symbol)
    return found_file_symbol;

  /* Symbols for shared library trampolines are next best.  */
  if (trampoline_symbol)
    return trampoline_symbol;

  return NULL;
} /* end quick_lookup_minimal_symbol */

#endif

#endif /* #ifdef GDB_TARGET_IS_HPUX */

#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
/* Add the minimal symbol SYM to an objfile's minsym demangled hash table,
   TABLE.  */
static void
add_minsym_to_demangled_hash_table (struct minimal_symbol *sym,
                                  struct minimal_symbol **table)
{
  if (sym->demangled_hash_next == NULL)
    {
      unsigned int hash = msymbol_hash_iw (SYMBOL_DEMANGLED_NAME (sym));
      sym->demangled_hash_next = table[hash];
      table[hash] = sym;
    }
}
#endif

struct minimal_symbol *
lookup_minimal_symbol (register const char *name,
                       const char *sfile,
                       struct objfile *objf)
{
  return lookup_minimal_symbol_1 (name, sfile, objf, VAR_NAMESPACE);
}


/* Look through all the current minimal symbol tables and find the
   first minimal symbol that matches NAME.  If OBJF is non-NULL, limit
   the search to that objfile.  If SFILE is non-NULL, limit the search
   to that source file.  Returns a pointer to the minimal symbol that
   matches, or NULL if no match is found.

   namesp: namespace to look in it for (not C++ namespace) but VAR_NAMESPACE
           or STRUCT_NAMESPACE. Look in VAR_NAMESPACE if it is a variable
	   or a function, for a type look in struct_namespace.

   Note:  One instance where there may be duplicate minimal symbols with
   the same name is when the symbol tables for a shared library and the
   symbol tables for an executable contain global symbols with the same
   names (the dynamic linker deals with the duplication). */

struct minimal_symbol *
lookup_minimal_symbol_1 (register const char *name,
                         const char *sfile,
                         struct objfile *objf,
                         namespace_enum namesp)
{
  struct objfile *objfile;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *found_symbol = NULL;
  struct minimal_symbol *found_file_symbol = NULL;
  struct minimal_symbol *trampoline_symbol = NULL;
  int count = 0;
  
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
  unsigned int hash = msymbol_hash (name);
  unsigned int dem_hash = msymbol_hash_iw (name);
#endif

#ifdef HASH_ALL
  return hash_lookup_minimal_symbol (name, sfile, objf, mst_unknown, namesp);
#else

#ifdef LAZY_DEMANGLING
  if (lazy_demangling)
    return quick_lookup_minimal_symbol (name, sfile, objf, mst_unknown);
#endif

#ifdef SOFUN_ADDRESS_MAYBE_MISSING
  if (sfile != NULL)
    {
      char *p = strrchr (sfile, '/');
      if (p != NULL)
	sfile = p + 1;
    }
#endif

  for (objfile = object_files;
       objfile != NULL && found_symbol == NULL;
       objfile = objfile->next)
    {
      if (objf == NULL || objf == objfile
          || objf->separate_debug_objfile == objfile)
	{
	  if (!objf)
	    {
	      /* Poorva: we don't want to find dld routines.
		 Problem is if a user sets a
		 break printf
		 gdb sets a deferred breakpoint. The first
		 printf it finds is the one in dld so it sets it
		 there. The user sees a breakpoint set but never
		 hits it because he wants the one in libc.

		 So for now don't look for symbols in dld if
		 debugging_dld is not set.
		 
		 The reason we check to see if count is 2 is because
		 dld will be the second load module to be loaded.
		 The first is the a.out and the second if there
		 is more than one will be dld. We don't have a
		 micro loader for now.
  
		 We do want to debug dld for corefiles since we 
		 look for the load info structure in dld.
		 */
	      count ++;
	      if (!(target_has_stack && !target_has_execution))
	        if (!debugging_dld)
		  if ((count == 2) && 
                      ((dld_name && objfile->name) ? strstr (objfile->name, dld_name) : NULL))
		    continue;
	    }
	 
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
	  /* Do two passes: the first over the ordinary hash table,
	     and the second over the demangled hash table.  */
	  int pass;

	  for (pass = 1; pass <= 2 && found_symbol == NULL; pass++)
            {
	      /* Select hash list according to pass.  */
	      if (pass == 1)
		msymbol = objfile->msymbol_hash[hash];
	      else
		msymbol = objfile->msymbol_demangled_hash[dem_hash];
	  
	  while (msymbol != NULL && found_symbol == NULL)
#else
	  for (msymbol = objfile->msymbols;
	       msymbol != NULL && SYMBOL_NAME (msymbol) != NULL &&
	       found_symbol == NULL;
	       msymbol++)
#endif
	    {
	      if (SYMBOL_MATCHES_NAME (msymbol, name))
		{
		  switch (MSYMBOL_TYPE (msymbol))
		    {
		    case mst_file_text:
		    case mst_file_data:
		    case mst_file_bss:
#ifdef SOFUN_ADDRESS_MAYBE_MISSING
		      if (sfile == NULL || STREQ (msymbol->filename, sfile))
			found_file_symbol = msymbol;
#else
		      /* We have neither the ability nor the need to
		         deal with the SFILE parameter.  If we find
		         more than one symbol, just return the latest
		         one (the user can't expect useful behavior in
		         that case).  */
		      found_file_symbol = msymbol;
#endif
		      break;

		    case mst_solib_trampoline:

		      /* If a trampoline symbol is found, we prefer to
		         keep looking for the *real* symbol. If the
		         actual symbol is not found, then we'll use the
		         trampoline entry. The trampoline is only meaningful
		         if we have started running, i.e. there is a stack. */
		      if (trampoline_symbol == NULL && target_has_stack)
			trampoline_symbol = msymbol;
		      break;

		    case mst_unknown:
		    default:
		      found_symbol = msymbol;
		      break;
		    }
		}

#ifndef LAZY_DEMANGLING
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
	      /* Find the next symbol on the hash chain.  At the end
		 of the first pass, try the demangled hash list.  */
	      if (pass == 1)
		msymbol = msymbol->hash_next;
	      else
		msymbol = msymbol->demangled_hash_next;
		}
#endif
	    }
	}
    }
  /* External symbols are best.  */
  if (found_symbol)
    return found_symbol;

  /* File-local symbols are next best.  */
  if (found_file_symbol)
    return found_file_symbol;

  /* Symbols for shared library trampolines are next best.  */
  if (trampoline_symbol)
    return trampoline_symbol;
  return NULL;
#endif  /* else ifdef HASH_ALL */
}

/* Look through all the current minimal symbol tables and find the
   first minimal symbol that matches NAME and of text type.  
   If OBJF is non-NULL, limit
   the search to that objfile.  If SFILE is non-NULL, limit the search
   to that source file.  Returns a pointer to the minimal symbol that
   matches, or NULL if no match is found.
 */

struct minimal_symbol *
lookup_minimal_symbol_text (register const char *name,
                            const char *sfile,
                            struct objfile *objf)
{
  struct objfile *objfile;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *found_symbol = NULL;
  struct minimal_symbol *found_file_symbol = NULL;

#ifdef HASH_ALL
  return hash_lookup_minimal_symbol (name, sfile, objf, mst_text, VAR_NAMESPACE);
#else

#ifdef GDB_TARGET_IS_HPUX
  if (lazy_demangling) 
    return quick_lookup_minimal_symbol(name, sfile, objf, mst_text);
#endif

#ifdef SOFUN_ADDRESS_MAYBE_MISSING
  if (sfile != NULL)
    {
      char *p = strrchr (sfile, '/');
      if (p != NULL)
	sfile = p + 1;
    }
#endif

  for (objfile = object_files;
       objfile != NULL && found_symbol == NULL;
       objfile = objfile->next)
    {
      if (objf == NULL || objf == objfile
          || objf->separate_debug_objfile == objfile)
	{
	  for (msymbol = objfile->msymbols;
	       msymbol != NULL && SYMBOL_NAME (msymbol) != NULL &&
	       found_symbol == NULL;
	       msymbol++)
	    {
	      if (SYMBOL_MATCHES_NAME (msymbol, name) &&
		  (MSYMBOL_TYPE (msymbol) == mst_text ||
		   MSYMBOL_TYPE (msymbol) == mst_file_text))
		{
		  switch (MSYMBOL_TYPE (msymbol))
		    {
		    case mst_file_text:
#ifdef SOFUN_ADDRESS_MAYBE_MISSING
		      if (sfile == NULL || STREQ (msymbol->filename, sfile))
			found_file_symbol = msymbol;
#else
		      /* We have neither the ability nor the need to
		         deal with the SFILE parameter.  If we find
		         more than one symbol, just return the latest
		         one (the user can't expect useful behavior in
		         that case).  */
		      found_file_symbol = msymbol;
#endif
		      break;
		    default:
		      found_symbol = msymbol;
		      break;
		    }
		}
	    }
	}
    }
  /* External symbols are best.  */
  if (found_symbol)
    return found_symbol;

  /* File-local symbols are next best.  */
  if (found_file_symbol)
    return found_file_symbol;

  return NULL;
#endif
}

/* Look through all the current minimal symbol tables and find the
   first minimal symbol that matches NAME and of solib trampoline type.  
   If OBJF is non-NULL, limit
   the search to that objfile.  If SFILE is non-NULL, limit the search
   to that source file.  Returns a pointer to the minimal symbol that
   matches, or NULL if no match is found.
 */

struct minimal_symbol *
lookup_minimal_symbol_solib_trampoline (register const char *name,
                                        const char *sfile,
                                        struct objfile *objf)
{
  struct objfile *objfile;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *found_symbol = NULL;

#ifdef HASH_ALL
  return hash_lookup_minimal_symbol (name, sfile, objf, mst_solib_trampoline, 
	VAR_NAMESPACE);
#else

#ifdef GDB_TARGET_IS_HPUX
  if (lazy_demangling)
    return quick_lookup_minimal_symbol(name, sfile, objf,
				       mst_solib_trampoline);
#endif

#ifdef SOFUN_ADDRESS_MAYBE_MISSING
  if (sfile != NULL)
    {
      char *p = strrchr (sfile, '/');
      if (p != NULL)
	sfile = p + 1;
    }
#endif

  for (objfile = object_files;
       objfile != NULL && found_symbol == NULL;
       objfile = objfile->next)
    {
      if (objf == NULL || objf == objfile
          || objf->separate_debug_objfile == objfile)
	{
	  for (msymbol = objfile->msymbols;
	       msymbol != NULL && SYMBOL_NAME (msymbol) != NULL &&
	       found_symbol == NULL;
	       msymbol++)
	    {
	      if (SYMBOL_MATCHES_NAME (msymbol, name) &&
		  MSYMBOL_TYPE (msymbol) == mst_solib_trampoline)
		return msymbol;
	    }
	}
    }

  return NULL;
#endif
}

#ifdef HP_IA64
struct unwind_table_entry *find_unwind_entry (CORE_ADDR);
#endif
/*jini: hppa_find_unwind_entry is defined for both PA and IPF code. */
struct unwind_table_entry *hppa_find_unwind_entry (CORE_ADDR);

#ifdef GDB_TARGET_IS_HPUX
CORE_ADDR
lookup_minimal_symbol_solib_export_trampoline (register const char *name,
                                               struct objfile *objfile)
{
  struct minimal_symbol *msymbol;
  struct unwind_table_entry *u;
  struct minimal_symbol *found_symbol = NULL;

  for (msymbol = objfile->msymbols;
       msymbol != NULL && SYMBOL_NAME (msymbol) != NULL &&
       found_symbol == NULL;
       msymbol++)
    {
      if (SYMBOL_MATCHES_NAME (msymbol, name))
        {
          /* It must be a shared library trampoline.  */
          if (MSYMBOL_TYPE (msymbol) != mst_solib_trampoline)
            continue;
  
          /* It must also be an export stub.  */
#ifdef HP_IA64
          u = find_unwind_entry (SYMBOL_VALUE (msymbol));
#else
          u = hppa_find_unwind_entry (SYMBOL_VALUE (msymbol));
#endif
          if (!u
#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined(HP_IA64)
              || u->stub_unwind.stub_type != EXPORT
#endif
            )
            continue;
          return SYMBOL_VALUE (msymbol);
        }
    }
  return NULL;
}
#endif


/* Search through the minimal symbol table for each objfile and find
   the symbol whose address is the largest address that is still less
   than or equal to PC, and matches SECTION (if non-null).  Returns a
   pointer to the minimal symbol if such a symbol is found, or NULL if
   PC is not in a suitable range.  Note that we need to look through
   ALL the minimal symbol tables before deciding on the symbol that
   comes closest to the specified PC.  This is because objfiles can
   overlap, for example objfile A has .text at 0x100 and .data at
   0x40000 and objfile B has .text at 0x234 and .data at 0x40048.  */

#ifndef BUCKET_SORT_MINSYMS

/* This function is too complicated to make inline changes. Hence the
   alternate version. Bug fixes should fix both -- srikanth, 000615.
*/

struct minimal_symbol *
lookup_minimal_symbol_by_pc_section (CORE_ADDR pc,
                                     asection *section)
{
  int lo;
  int hi = 0;
  int new;
  struct objfile *objfile, *main_objfile, *best_objfile = NULL;
  struct minimal_symbol *msymbol = 0;
  struct minimal_symbol *best_symbol = NULL;
  char *symname;
  
  /* pc has to be in a known section. This ensures that anything beyond
     the end of the last segment doesn't appear to be part of the last
     function in the last segment.  */
  if (find_pc_section (pc) == NULL)
    return NULL;

  for (objfile = object_files;
       objfile != NULL;
       objfile = main_objfile->next)
    {
      main_objfile = objfile; 
      /* In case we have separate debug files, search both the file and
         its separate debug file.  There's no telling which one will have
         the minimal symbols.  */
      if (objfile->separate_debug_objfile)
        objfile = objfile->separate_debug_objfile;

      for (; objfile != NULL;
           objfile = objfile->separate_debug_objfile_backlink)
        {
          /* If this objfile has a minimal symbol table, go search it using
             a binary search.  Note that a minimal symbol table always consists
             of at least two symbols, a "real" symbol and the terminating
             "null symbol".  If there are no real symbols, then there is no
             minimal symbol table at all. */

          if ((msymbol = objfile->msymbols) != NULL)
	    {
              /* RM: Does this symbol overlap with this objfile? */
              struct obj_section *s;
              for (s = objfile->sections; s < objfile->sections_end; ++s)
                if (s->addr <= pc
                    && pc <= s->endaddr)
                  break;
              if (s >= objfile->sections_end)
                continue;
          
	      lo = 0;
	      hi = objfile->minimal_symbol_count - 1;

	      /* This code assumes that the minimal symbols are sorted by
	         ascending address values.  If the pc value is greater than or
	         equal to the first symbol's address, then some symbol in this
	         minimal symbol table is a suitable candidate for being the
	         "best" symbol.  This includes the last real symbol, for cases
	         where the pc value is larger than any address in this vector.

	         By iterating until the address associated with the current
	         hi index (the endpoint of the test interval) is less than
	         or equal to the desired pc value, we accomplish two things:
	         (1) the case where the pc value is larger than any minimal
	         symbol address is trivially solved, (2) the address associated
                 with the hi index is always the one we want when the iteration
	         terminates.  In essence, we are iterating the test interval
	         down until the pc value is pushed out of it from the high end.

	         Warning: this code is trickier than it would appear at first. */

	      /* Should also require that pc is <= end of objfile.  FIXME! */
	      if (pc >= SYMBOL_VALUE_ADDRESS (&msymbol[lo]))
	        {
	          while (SYMBOL_VALUE_ADDRESS (&msymbol[hi]) > pc)
		    {
		      /* pc is still strictly less than highest address */
		      /* Note "new" will always be >= lo */
		      new = (lo + hi) / 2;
		      if ((SYMBOL_VALUE_ADDRESS (&msymbol[new]) >= pc) ||
		          (lo == new))
		        {
		          hi = new;
		        }
		      else
		        {
		          lo = new;
		        }
		    }

	          /* If we have multiple symbols at the same address, we want
                     hi to point to the last one.  That way, we can find the
	             right symbol if it has an index greater than hi.  */
	          while (hi < objfile->minimal_symbol_count - 1
		         && (SYMBOL_VALUE_ADDRESS (&msymbol[hi])
			     == SYMBOL_VALUE_ADDRESS (&msymbol[hi + 1])))
		    hi++;

                  /* QXCR1000960468:
                   * If there are "holes" in the minimal symbol tables (due to some
                   * hidden symbols), then we may get a wrong minsym, with a wrong
                   * pst! To avoid this, check if the pst range covers the PC, else
                   * search ahead.
                   */
                  while (   (hi < objfile->minimal_symbol_count - 1)
                         && (msymbol[hi].pst) // There is a pst
                         && (   (msymbol[hi].type == mst_text)       // Only for
                             || (msymbol[hi].type == mst_file_text)) // text syms
                         && (pc >= msymbol[hi].pst->texthigh) // PC beyond pst
                         && (pc >= SYMBOL_VALUE_ADDRESS(&msymbol[hi])))
                    { 
                      /* The next msymbol should have a valid associated pst,
                         or it should be a stub */
                      if (   (   (msymbol[hi+1].pst)
                              && (msymbol[hi+1].pst->texthigh > 0))
                          || (strstr(SYMBOL_NAME(msymbol+(hi+1)), ".stub")))
                        hi++;
                      else
                       {
                        break;
                       }
                    }

	          /* The minimal symbol indexed by hi now is the best one in this
	             objfile's minimal symbol table.  See if it is the best one
	             overall. */

	          /* Skip any absolute symbols.  This is apparently what adb
	             and dbx do, and is needed for the CM-5.  There are two
	             known possible problems: (1) on ELF, apparently end, edata,
	             etc. are absolute.  Not sure ignoring them here is a big
	             deal, but if we want to use them, the fix would go in
	             elfread.c.  (2) I think shared library entry points on the
	             NeXT are absolute.  If we want special handling for this
	             it probably should be triggered by a special
	             mst_abs_or_lib or some such.  */
	          while (hi > 0
		         && msymbol[hi].type == mst_abs)
		    --hi;

	          /* If "section" specified, skip any symbol from wrong section */
	          /* This is the new code that distinguishes it from the old function */
                  /* For PA32 symbols, when the minimal symbols are recorded, the
                     the bfd section for each symbol is set to zero. Hence the
                     comparison is invalid for these mixed mode cases. For more
                     details, pls refer to JAGaf59103. */
	          if (section
#ifdef HP_IA64
                      && (!objfile->is_mixed_mode_pa_lib)
#endif
                      )
		    while (hi >= 0
		           && SYMBOL_BFD_SECTION (&msymbol[hi]) != section)
		      --hi;

	          /* RM: stupid bug ... */
	          if (hi < 0)
		    continue;
	      
                  /* RM: On HPUX we have left in some weird symbols so we
	             can do linker fixups for DOOM. However, matches for
               	     these should fail.
		     MC: A match for .text should fail if there is a
		     preceeding real symbol with the same address. */
                  symname = SYMBOL_NAME(msymbol+hi);
	          /* JAGag40551: handle .cold name also. */
#ifdef IS_RELOC_ONLY_SYMBOL
                  while (IS_RELOC_ONLY_SYMBOL (symname) ||
		         ((strcmp (symname, ".text") == 0 || strcmp (symname, ".cold") == 0) &&
		          hi > 0 &&
		          SYMBOL_VALUE_ADDRESS (&msymbol[hi]) ==
		          SYMBOL_VALUE_ADDRESS (&msymbol[hi-1])))
                    {
                      --hi;
                      symname = SYMBOL_NAME(msymbol+hi);
                    }
#endif
              
	          if (hi >= 0)
		    symname = SYMBOL_NAME (msymbol + hi);
#ifdef IS_RELOC_ONLY_SYMBOL
	          while (   hi >= 0
	                 && (   IS_RELOC_ONLY_SYMBOL (symname)
		             || ((strcmp (symname, ".text") == 0 || strcmp (symname, ".cold") == 0)
			         && hi > 0
			         && SYMBOL_VALUE_ADDRESS (&msymbol[hi])
			         == SYMBOL_VALUE_ADDRESS (&msymbol[hi - 1]))))
		    {
		      --hi;
		      if (hi >= 0)
		        symname = SYMBOL_NAME (msymbol + hi);
		    }
#endif

	          if (hi >= 0
		      && ((best_symbol == NULL) ||
                          ((SYMBOL_VALUE_ADDRESS (best_symbol) ==
                            SYMBOL_VALUE_ADDRESS (&msymbol[hi])) &&
                            !(best_symbol->pst) && (msymbol[hi].pst) &&
                            objfile->separate_debug_objfile) ||
		          (SYMBOL_VALUE_ADDRESS (best_symbol) <
		           SYMBOL_VALUE_ADDRESS (&msymbol[hi]))))
		    {
		      best_symbol = &msymbol[hi];
		      best_objfile = objfile;
		    }
	        }
	    }
        }
    }

  /* srikanth, demangle if not already done ... */
  if (lazy_demangling && best_symbol &&
      SYMBOL_LANGUAGE(best_symbol) == language_auto)
    SYMBOL_INIT_DEMANGLED_NAME(best_symbol, &best_objfile->symbol_obstack);

  return (best_symbol);
}

#else

struct minimal_symbol *
lookup_minimal_symbol_by_pc_section (pc, section)
     CORE_ADDR pc;
     asection *section;
{
  int lo;
  int hi = 0;
  int new;
  struct objfile *objfile, *best_objfile = NULL;
  struct minimal_symbol **msymbol = 0;
  struct minimal_symbol *best_symbol = NULL;
  char *symname;
  struct minsym_address_partition * partition;
  int p;

  /* pc has to be in a known section. This ensures that anything beyond
     the end of the last segment doesn't appear to be part of the last
     function in the last segment.  */
  if (find_pc_section (pc) == NULL)
    return NULL;

  for (objfile = object_files;
       objfile != NULL;
       objfile = objfile->next)
    {
      if ((objfile->msymbols) != NULL)
	{
	  /* RM: Does this symbol overlap with this objfile? */
	  struct obj_section *s;

          /* are we barking up the wrong tree ? */
          if (pc < objfile -> lowest_address)
            continue;

	  for (s = objfile->sections; s < objfile->sections_end; ++s)
	    if (s->addr <= pc
		&& pc <= s->endaddr)
	      break;

	  if (s >= objfile->sections_end)
	    continue;

          /* srikanth, it is a given that PC is in this load module.
             If we have not loaded the debug info for this load module,
             go ahead and load it now.
          */

#ifdef AUTO_LOAD_DEBUG_INFO
          /* FIXME : This has hardcoded calls to som library manager */
          if (objfile->psymtabs == 0 && objfile->sf->sym_add_psymtabs)
            if (!objfile->has_no_dash_g_table)
		/* check if we are already auto loading */
		if (!auto_loading)
		  {
		    auto_loading = 1;
                    som_solib_add_sharedlibrary (objfile->name, 1, 0);
		    auto_loading = 0;
		  }
#endif

          /* locate the partition from the address */
          if (pc >= objfile -> highest_address)
            p = N_PARTS;
          else
            p = (pc - objfile->lowest_address)/objfile->partition_interval;

          for (p; p >= 0; --p)
            {
              partition = objfile->minsym_address_partitions + p;
              if (partition -> minimal_symbol_count == 0)
                continue;

              sort_minimal_symbol_partition_by_address (partition);

              msymbol = partition->msymbols;

	      if (pc < SYMBOL_VALUE_ADDRESS (msymbol[0]))
                continue;

	      lo = 0;
	      hi = partition->minimal_symbol_count - 1;

              /* If you stare at this while loop long and hard, you can
                 convince yourself that when the loop terminates, `hi'
                 is the index of the symbol with the largest address
                 that is still smaller than or equal to pc.
              */
	      while (SYMBOL_VALUE_ADDRESS (msymbol[hi]) > pc)
                {
		  new = (lo + hi) / 2;
		  if ((SYMBOL_VALUE_ADDRESS (msymbol[new]) >= pc)
		      || (lo == new))
		    hi = new;
		  else
		    lo = new;
                }

	      /* If we have multiple symbols @ the same address, we want
	         hi to point to the last one.  That way, we can find the
	         right symbol if it has an index greater than hi.  */

	      while (hi < partition->minimal_symbol_count - 1
                     && (SYMBOL_VALUE_ADDRESS (msymbol[hi])
		          == SYMBOL_VALUE_ADDRESS (msymbol[hi + 1])))
                hi++;

              /* JAGaf60778 - <info line _main_ fails for fortran executable
	         compiled with +objdebug> - If the fortran exe has no program 
		 name, then the corresponding minimal symbol is <__mainprogram>.
		 Since the symbols <__mainprogram> and <_start> are @ the same 
		 address , we want hi to point to the first one <__mainprogram> */ 
	      #ifndef HP_IA64
              if (main_line && (strcmp(SYMBOL_NAME(msymbol[hi]),"_start")==0) 
	           && (SYMBOL_VALUE_ADDRESS (msymbol[hi]) 
		        == SYMBOL_VALUE_ADDRESS (msymbol[hi - 1])))
	        hi--;
	      #endif
	      /* JAGaf60778 - <END> */
	      
	      /* Skip symbols that are absolute, from the wrong section
                 or internal compiler generated relocation stuff. Also
                 skip the symbol .text if there is a real preceding
                 symbol at the same address. This is what the non-bucket
                 sort cousin does. Mimic that in a braindead manner:
                 Never mind the questions.
              */
	      for (hi; hi >= 0; hi--)
                {
                  if (msymbol[hi]->type == mst_abs)
                    if (hi != 0 || p != 0)
                      continue;
/* JAGaf59103 - <START> - 'info symbol' command is broken for PA32. 		      
   When msymbols are recorded for PA32 , the bfd_section of each msymbol  
   is set as 0 . Check whether msymbol's bfd_section exists and then
   go ahead with the comparison .*/ 
                  if (section && SYMBOL_BFD_SECTION (msymbol[hi]) && SYMBOL_BFD_SECTION (msymbol[hi]) != section)
                    continue;
/* JAGaf59103 - <END> */
#ifdef IS_RELOC_ONLY_SYMBOL
	          symname = SYMBOL_NAME (msymbol[hi]);
	          if (IS_RELOC_ONLY_SYMBOL (symname))
                    if (hi != 0 || p != 0)
                      continue;

                  if (strcmp (symname, ".text") == 0 && hi > 0
		           && SYMBOL_VALUE_ADDRESS (msymbol[hi])
			      == SYMBOL_VALUE_ADDRESS (msymbol[hi - 1]))
                    continue;

                  break;
		}
#endif
              if (hi < 0 && p > 0)
                continue;  /* onto the preceding parttion */
              else
                break;
            }

	  if (hi >= 0
               && ((best_symbol == NULL) ||
		      (SYMBOL_VALUE_ADDRESS (best_symbol) <
		       SYMBOL_VALUE_ADDRESS (msymbol[hi]))))
            {
              best_symbol = msymbol[hi];
              best_objfile = objfile;
            }
	  }
    }

  /* srikanth, demangle if not already done ... */
  if (lazy_demangling && best_symbol &&
      SYMBOL_LANGUAGE (best_symbol) == language_auto)
    SYMBOL_INIT_DEMANGLED_NAME (best_symbol, &best_objfile->symbol_obstack);
  return (best_symbol);
}

#endif

/* Backward compatibility: search through the minimal symbol table 
   for a matching PC (no section given) */

static struct minimal_symbol *
lookup_minimal_symbol_by_pc_1 (CORE_ADDR pc)
{
  return lookup_minimal_symbol_by_pc_section (pc, find_pc_mapped_section (pc));
}

#ifdef SOFUN_ADDRESS_MAYBE_MISSING
CORE_ADDR
find_stab_function_addr (namestring, filename, objfile)
     char *namestring;
     char *filename;
     struct objfile *objfile;
{
  struct minimal_symbol *msym;
  char *p;
  int n;

  p = strchr (namestring, ':');
  if (p == NULL)
    p = namestring;
  n = p - namestring;
  p = alloca (n + 2);
  strncpy (p, namestring, n);
  p[n] = 0;

  msym = lookup_minimal_symbol (p, filename, objfile);
  if (msym == NULL)
    {
      /* Sun Fortran appends an underscore to the minimal symbol name,
         try again with an appended underscore if the minimal symbol
         was not found.  */
      p[n] = '_';
      p[n + 1] = 0;
      msym = lookup_minimal_symbol (p, filename, objfile);
    }

  if (msym == NULL && filename != NULL)
    {
      /* Try again without the filename. */
      p[n] = 0;
      msym = lookup_minimal_symbol (p, 0, objfile);
    }
  if (msym == NULL && filename != NULL)
    {
      /* And try again for Sun Fortran, but without the filename. */
      p[n] = '_';
      p[n + 1] = 0;
      msym = lookup_minimal_symbol (p, 0, objfile);
    }

  return msym == NULL ? 0 : SYMBOL_VALUE_ADDRESS (msym);
}
#endif /* SOFUN_ADDRESS_MAYBE_MISSING */


/* Return leading symbol character for a BFD. If BFD is NULL,
   return the leading symbol character from the main objfile.  */

static int get_symbol_leading_char (bfd *);

static int
get_symbol_leading_char (bfd *abfd)
{
  if (abfd != NULL)
    return bfd_get_symbol_leading_char (abfd);
  if (symfile_objfile != NULL && symfile_objfile->obfd != NULL)
    return bfd_get_symbol_leading_char (symfile_objfile->obfd);
  return 0;
}

/* Prepare to start collecting minimal symbols.  Note that presetting
   msym_bunch_index to BUNCH_SIZE causes the first call to save a minimal
   symbol to allocate the memory for the first bunch. */

void
init_minimal_symbol_collection ()
{

#ifdef BUCKET_SORT_MINSYMS
  null_symbol_present = 0;
  lowest_address = highest_address = 0;
#endif

  msym_count = 0;
  msym_bunch = NULL;
  msym_bunch_index = BUNCH_SIZE;
}

void
prim_record_minimal_symbol (const char *name,
                            CORE_ADDR address,
                            enum minimal_symbol_type ms_type,
                            struct objfile *objfile)
{
  int section;

  switch (ms_type)
    {
    case mst_text:
    case mst_file_text:
    case mst_solib_trampoline:
      section = SECT_OFF_TEXT (objfile);
      break;
    case mst_data:
    case mst_file_data:
      section = SECT_OFF_DATA (objfile);
      break;
    case mst_bss:
    case mst_file_bss:
      section = SECT_OFF_BSS (objfile);
      break;
    default:
      section = -1;
    }

  prim_record_minimal_symbol_and_info (name, address, ms_type,
				       NULL, section, NULL, objfile);
}

/* Record a minimal symbol in the msym bunches.  Returns the symbol
   newly created.  */

struct minimal_symbol *
prim_record_minimal_symbol_and_info (const char *name,
                                     CORE_ADDR address,
                                     enum minimal_symbol_type ms_type,
                                     char *info,
                                     int section,
                                     asection *bfd_section,
                                     struct objfile *objfile)
{
  register struct msym_bunch *new;
  register struct minimal_symbol *msymbol;
  extern int admit_internal_symbols;
  int minsym_len;

#ifdef DISCARD_ACC_INTERNAL_SYMBOLS

  /* srikanth, 000209, HP aCC generates too many internal symbols
     for such things like vtables, exception handling range tables,
     typeids etc. These occupy as much as a third of the table in
     some large applications. Get rid of these as we have no use for
     them. Leave these in if the flag admit_internal_symbols is set.
     Dennis requested this.
  */

  if (!admit_internal_symbols)
    {
      /* vtables and vftptrs : These look like "[Vftptr_8FastPool]key:"
         and "[Vtable_8FastPool]key:" No danger of confusing this with
         any user space names.
      */

      if (name[0] == '[' && name[1] == 'V')
        return 0;

      /* Exception handling range and action tables : These look like
         "$AUX_ACTION$__dt__14Client_ManagerFv" and 
         "$RATBL$_ct__14Client_ManagerFv" */

      if (name[0] == '$' && name[1] == 'A' && name[2] == 'U' &&
             !strncmp(name, "$AUX_ACTION$", 12))
        return 0;

      if (name[0] == '$' && name[1] == 'R' && name[2] == 'A' &&
              !strncmp(name, "$RATBL$", 7))
        return 0;

      /* typeid stuff ... */

      if (ms_type == mst_file_data && name[0] == 's' &&
              name[1] == 't' && !strncmp (name, "static_typeid", 13))
        return 0;

      if (ms_type == mst_data && name[0] == 't' &&
              name[1] == 'y' && !strncmp(name, "typeid*", 7))
        return 0;
    }

#endif

  if (ms_type == mst_file_text)
    {

      /* Don't put gcc_compiled, __gnu_compiled_cplus, and friends into
         the minimal symbols, because if there is also another symbol
         at the same address (e.g. the first function of the file),
         lookup_minimal_symbol_by_pc would have no way of getting the
         right one.  */
      if (name[0] == 'g'
	  && (strcmp (name, GCC_COMPILED_FLAG_SYMBOL) == 0
	      || strcmp (name, GCC2_COMPILED_FLAG_SYMBOL) == 0))
	return (NULL);
      {
	const char *tempstring = name;
	if (tempstring[0] == get_symbol_leading_char (objfile->obfd))
	  ++tempstring;
	if (STREQN (tempstring, "__gnu_compiled", 14))
	  return (NULL);
      }

#ifdef HP_IA64
      /* Clone "_start" into "_main_" for the GUI to query for the
	 first line of Fortran programs with "info line _main_".
	 Fix for JAGaf55720 ("info line _main_" doesn't give a useful 
	 output for Fortran executables).  Not needed on PA, don't know why. */
      if (STREQN (name, "_start", 6))
	{
	  prim_record_minimal_symbol_and_info ("_main_", address, ms_type, info,
					       section, bfd_section, objfile);
	  /* If the objfile has no PC limits set, copy them from the BFD. */
	  if ((objfile->text_low == 0) && (objfile->text_high == 0))
	    {
	      objfile->text_low = bfd_section->vma;
	      objfile->text_high = bfd_section->vma + 
				bfd_get_section_size_before_reloc(bfd_section);
	    }
	}
#endif
    }

  if (msym_bunch_index == BUNCH_SIZE)
    {
      new = (struct msym_bunch *) xmalloc (sizeof (struct msym_bunch));
      msym_bunch_index = 0;
      new->next = msym_bunch;
      msym_bunch = new;
    }
  msymbol = &msym_bunch->contents[msym_bunch_index];
  memset (msymbol, 0, sizeof (struct minimal_symbol));

  /* srikanth, in the case of ELF format, (at least HP stuff) BFD keeps a 
     permanent copy of the string table from the load module. Little to be 
     gained by creating a copy here. 

     Is this safe ? I think the BFD can silently close and re-open abfd
     if there is a shortage of file handles ??? I am not enabling this define
     for now. It may safer to get bfd to blow the strtab. FIXME */

# ifdef BFD_HOLDS_ONTO_MASTER_STRING_TABLE
  SYMBOL_NAME (msymbol) = name;
#else 
  minsym_len = strlen (name);
#ifdef HASH_ALL
  UPDATE_STRING_SIZE (minsym_len + 1);
#endif
  SYMBOL_NAME (msymbol) = obsavestring ((char *) name, minsym_len,
					&objfile->symbol_obstack);
#endif
  SYMBOL_INIT_LANGUAGE_SPECIFIC (msymbol, language_unknown);
  SYMBOL_VALUE_ADDRESS (msymbol) = address;
  SYMBOL_SECTION (msymbol) = section;
  SYMBOL_BFD_SECTION (msymbol) = bfd_section;

  MSYMBOL_TYPE (msymbol) = ms_type;
  /* FIXME:  This info, if it remains, needs its own field.  
   */
  MSYMBOL_INFO (msymbol) = info;	/* FIXME! */
  MSYMBOL_OBSOLETED (msymbol) = false;

#ifdef HASH_ALL
  switch (ms_type)
    {
    case mst_file_text:
    case mst_file_data:
    case mst_file_bss:
      MSYMBOL_IS_GLOBAL (msymbol) = TYPE_STATIC;
      break;
    default:
      MSYMBOL_IS_GLOBAL (msymbol) =  TYPE_GLOBAL;
      break;
    }
#endif

#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
  /* The hash pointers must be cleared! If they're not,
     add_minsym_to_hash_table will NOT add this msymbol to the hash table. */
  msymbol->hash_next = NULL;
  msymbol->demangled_hash_next = NULL;
#endif

  msym_bunch_index++;
  msym_count++;
  OBJSTAT (objfile, n_minsyms++);

#ifdef BUCKET_SORT_MINSYMS

  /* srikanth, 000519, gather information on distribution of addresses.
     We will use this later to partition the minimal symbols into
     different sets.
  */

  if ((lowest_address == 0 && !null_symbol_present) ||
                          address < lowest_address)
    {
      lowest_address = address;
      if (address == 0)
        null_symbol_present = 1;
    }

  if (address > highest_address)
    highest_address = address;

#endif

  return msymbol;
}

/* Compare two minimal symbols by address and return a signed result based
   on unsigned comparisons, so that we sort into unsigned numeric order.  
   Within groups with the same address, sort by name.  */

static int
compare_minimal_symbols (const PTR fn1p,
                         const PTR fn2p)
{
  register const struct minimal_symbol *fn1;
  register const struct minimal_symbol *fn2;

  fn1 = (const struct minimal_symbol *) fn1p;
  fn2 = (const struct minimal_symbol *) fn2p;

  if (SYMBOL_VALUE_ADDRESS (fn1) < SYMBOL_VALUE_ADDRESS (fn2))
    {
      return (-1);		/* addr 1 is less than addr 2 */
    }
  else if (SYMBOL_VALUE_ADDRESS (fn1) > SYMBOL_VALUE_ADDRESS (fn2))
    {
      return (1);		/* addr 1 is greater than addr 2 */
    }
  else
    /* addrs are equal: sort by name */
    {
      char *name1 = SYMBOL_NAME (fn1);
      char *name2 = SYMBOL_NAME (fn2);

      if (name1 && name2)	/* both have names */
	return strcmp (name1, name2);
      else if (name2)
	return 1;		/* fn1 has no name, so it is "less" */
      else if (name1)		/* fn2 has no name, so it is "less" */
	return -1;
      else
	return (0);		/* neither has a name, so they're equal. */
    }
}

/* Discard the currently collected minimal symbols, if any.  If we wish
   to save them for later use, we must have already copied them somewhere
   else before calling this function.

   FIXME:  We could allocate the minimal symbol bunches on their own
   obstack and then simply blow the obstack away when we are done with
   it.  Is it worth the extra trouble though? */

/* This function is a NOP now. install_minimal_symbol() discards the
   bunches just after copying them over - srikanth */

static void
do_discard_minimal_symbols_cleanup (void *arg)
{
  register struct msym_bunch *next;

  while (msym_bunch != NULL)
    {
      next = msym_bunch->next;
      free ((PTR) msym_bunch);
      msym_bunch = next;
    }
}

struct cleanup *
make_cleanup_discard_minimal_symbols (void)
{
  return make_cleanup (do_discard_minimal_symbols_cleanup, 0);
}



/* Compact duplicate entries out of a minimal symbol table by walking
   through the table and compacting out entries with duplicate addresses
   and matching names.  Return the number of entries remaining.

   On entry, the table resides between msymbol[0] and msymbol[mcount].
   On exit, it resides between msymbol[0] and msymbol[result_count].

   When files contain multiple sources of symbol information, it is
   possible for the minimal symbol table to contain many duplicate entries.
   As an example, SVR4 systems use ELF formatted object files, which
   usually contain at least two different types of symbol tables (a
   standard ELF one and a smaller dynamic linking table), as well as
   DWARF debugging information for files compiled with -g.

   Without compacting, the minimal symbol table for gdb itself contains
   over a 1000 duplicates, about a third of the total table size.  Aside
   from the potential trap of not noticing that two successive entries
   identify the same location, this duplication impacts the time required
   to linearly scan the table, which is done in a number of places.  So we
   just do one linear scan here and toss out the duplicates.

   Note that we are not concerned here about recovering the space that
   is potentially freed up, because the strings themselves are allocated
   on the symbol_obstack, and will get automatically freed when the symbol
   table is freed.  The caller can free up the unused minimal symbols at
   the end of the compacted region if their allocation strategy allows it.

   Also note we only go up to the next to last entry within the loop
   and then copy the last entry explicitly after the loop terminates.

   Since the different sources of information for each symbol may
   have different levels of "completeness", we may have duplicates
   that have one entry with type "mst_unknown" and the other with a
   known type.  So if the one we are leaving alone has type mst_unknown,
   overwrite its type with the type from the one we are compacting out.  */

static int
compact_minimal_symbols (struct minimal_symbol *msymbol,
                         int mcount,
                         struct objfile *objfile)
{
  struct minimal_symbol *copyfrom;
  struct minimal_symbol *copyto;

  if (mcount > 0)
    {
      copyfrom = copyto = msymbol;
      while (copyfrom < msymbol + mcount - 1)
	{
	  if (SYMBOL_VALUE_ADDRESS (copyfrom) ==
	      SYMBOL_VALUE_ADDRESS ((copyfrom + 1)) &&
	      (STREQ (SYMBOL_NAME (copyfrom), SYMBOL_NAME ((copyfrom + 1)))))
	    {
              /* RM: prefer non-unknown symbols, and text symbols over
                 trampolines */
              if ((MSYMBOL_TYPE((copyfrom + 1)) == mst_unknown) ||
                  ((MSYMBOL_TYPE((copyfrom + 1)) == mst_solib_trampoline) &&
                   (MSYMBOL_TYPE(copyfrom) == mst_text)))
		{
		  MSYMBOL_TYPE ((copyfrom + 1)) = MSYMBOL_TYPE (copyfrom);
		}
	      copyfrom++;
	    }
	  else
	    {
	      *copyto++ = *copyfrom++;

#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
	      add_minsym_to_hash_table (copyto - 1, objfile->msymbol_hash);
#endif
	    }
	}
      *copyto++ = *copyfrom++;
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
      /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
         mangling lookup + lazy demangling, which cannot use hash
         implementation and does not want demangling the whole world
         behavior */
      add_minsym_to_hash_table (copyto - 1, objfile->msymbol_hash);
#endif
      mcount = (int)(copyto - msymbol);
    }
  return (mcount);
}

#ifdef BUCKET_SORT_MINSYMS

/* compact_minimal_symbol_partition : This is a clone of
   compact_minimal_symbols above, except that we compact
   the pointers and not the symbols themseleves.
*/
static int
compact_minimal_symbol_partition (struct minsym_address_partition *p)
{
  struct minimal_symbol **copyfrom;
  struct minimal_symbol **copyto;
  int mcount;
  struct minimal_symbol **msymbol;

  mcount = p -> minimal_symbol_count;
  msymbol = p->msymbols;

  if (mcount > 0)
    {
      copyfrom = copyto = msymbol;
      while (copyfrom < msymbol + mcount - 1)
	{
	  if (SYMBOL_VALUE_ADDRESS (*copyfrom) == SYMBOL_VALUE_ADDRESS (*(copyfrom + 1))
	  && (STREQ (SYMBOL_NAME (*copyfrom), SYMBOL_NAME (*(copyfrom + 1)))))
	    {
	      /* RM: prefer non-unknown symbols, and text symbols over
	         trampolines */
	      if ((MSYMBOL_TYPE (*(copyfrom + 1)) == mst_unknown) ||
		  ((MSYMBOL_TYPE (*(copyfrom + 1)) == mst_solib_trampoline) &&
		   (MSYMBOL_TYPE (*copyfrom) == mst_text)))
		{
		  MSYMBOL_TYPE (*(copyfrom + 1)) = MSYMBOL_TYPE (*copyfrom);
		}
	      copyfrom++;
	    }
	  else
	    {
	      *copyto++ = *copyfrom++;
	    }
	}
      *copyto++ = *copyfrom++;
      mcount = copyto - msymbol;
    }
  p->minimal_symbol_count = mcount;
}



static void
partition_minimal_symbols_by_name (objfile, histogram)
struct objfile * objfile; int histogram[];
{
  int mcount;
  int i, p;
  char * name;
  struct minimal_symbol * m;
  int index;
  int count;

  mcount = objfile -> minimal_symbol_count;

  /* Allocate arrays of pointers to minsyms. Each array will store
     pointers to minsyms all of whose names start with the same
     ASCII code. We will incrementally sort these arrays just in time.
  */

  for (i=0; i < 256; i++)
    {
      count = histogram[i];
      objfile->minsym_name_partitions[i].sorted = 0;
      objfile->minsym_name_partitions[i].minimal_symbol_count = 0;
      objfile->minsym_name_partitions[i].msymbols =
            (struct minimal_symbol **)
            obstack_alloc (&objfile->symbol_obstack,
		     (count + 1) * sizeof (struct minimal_symbol *));
      objfile->minsym_name_partitions[i].msymbols[count] = 0;
    }
      
  /* Scatter the minimal symbols into a different partitions based
     on the first character in the symbol name. Later when a symbol
     gets looked up by name, it is easy to see which partition would
     contain the symbol and if that partition is not already sorted,
     sort it just in time and then use a binary search. There are 256
     partitions in total.
  */

  for (i=0; i < mcount; i++)
    {
       m = objfile->msymbols + i;
       p = SYMBOL_NAME (m)[0];
       index = objfile->minsym_name_partitions[p].minimal_symbol_count;
       objfile->minsym_name_partitions[p].msymbols[index] = m;
       objfile->minsym_name_partitions[p].minimal_symbol_count++;
     }
}



static void
partition_minimal_symbols_by_address (struct objfile * objfile,
                                      int n_parts)
{
  int mcount, i, count, p, index;
  struct minimal_symbol * m;
  ULONGEST delta;
  CORE_ADDR address;

  mcount = objfile -> minimal_symbol_count;

  /* Allocate arrays of pointers to minsyms. Each array will store
     pointers to minsyms all of whose addresses lie within the same
     range. We will incrementally sort these arrays just in time.
  */

  for (i=0; i <= n_parts; i++)
    {
      count = objfile->minsym_address_partitions[i].minimal_symbol_count;
      objfile->minsym_address_partitions[i].sorted = 0;
      objfile->minsym_address_partitions[i].msymbols =
            (struct minimal_symbol **)
            obstack_alloc (&objfile->symbol_obstack,
		     (count + 1) * sizeof (struct minimal_symbol *));
      objfile->minsym_address_partitions[i].msymbols[count] = 0;
      objfile->minsym_address_partitions[i].minimal_symbol_count = 0;
    }

  /* Scatter the minimal symbols into a different partitions based
     on their address. Later when a symbol gets looked up by address,
     it is easy to see which partition would contain the symbol and
     if that partition is not already sorted, sort it just in time
     and then use a binary search. There are N_PARTS partitions in
     total, each housing symbols that fits within equidistant intervals.
  */

  delta = objfile->partition_interval;
  for (i=0; i < mcount; i++)
    {
       m = objfile->msymbols + i;
       address = SYMBOL_VALUE_ADDRESS (m);
       p = (address - lowest_address) /delta;
       index = objfile->minsym_address_partitions[p].minimal_symbol_count;
       objfile->minsym_address_partitions[p].msymbols[index] = m;
       objfile->minsym_address_partitions[p].minimal_symbol_count++;
     }
}
#endif 



/* Add the minimal symbols in the existing bunches to the objfile's official
   minimal symbol table.  In most cases there is no minimal symbol table yet
   for this objfile, and the existing bunches are used to create one.  Once
   in a while (for shared libraries for example), we add symbols (e.g. common
   symbols) to an existing objfile.

   Because of the way minimal symbols are collected, we generally have no way
   of knowing what source language applies to any particular minimal symbol.
   Specifically, we have no way of knowing if the minimal symbol comes from a
   C++ compilation unit or not.  So for the sake of supporting cached
   demangled C++ names, we have no choice but to try and demangle each new one
   that comes in.  If the demangling succeeds, then we assume it is a C++
   symbol and set the symbol's language and demangled name fields
   appropriately.  Note that in order to avoid unnecessary demanglings, and
   allocating obstack space that subsequently can't be freed for the demangled
   names, we mark all newly added symbols with language_auto.  After
   compaction of the minimal symbols, we go back and scan the entire minimal
   symbol table looking for these new symbols.  For each new symbol we attempt
   to demangle it, and if successful, record it as a language_cplus symbol
   and cache the demangled form on the symbol obstack.  Symbols which don't
   demangle are marked as language_unknown symbols, which inhibits future
   attempts to demangle them if we later add more minimal symbols. */

void
install_minimal_symbols (struct objfile *objfile)
{
  register int bindex;
  register int mcount;
  register struct msym_bunch *bunch, *tmp_bunch;
  register struct minimal_symbol *msymbols;
  int alloc_count;
  register char leading_char;
  char *name;
  int i;
  int histogram[256];
  ULONGEST delta;
  ULONGEST range;
  int bucket;
  CORE_ADDR address;

  if (msym_count <= 0)
    return;

#ifdef  BUCKET_SORT_MINSYMS

  /* rven: 020201 : If objfile->lowest_address is non-zero, we have
     already installed minimal symbol tables. So no action required */

  if (objfile->lowest_address > 0)
     return;

  /* srikanth, 000615, Gather information of the symbol distribution.
     This will be used later to partition the minsyms.
  */

  for (i=0; i<256; i++)
    histogram[i] = 0;

  range = highest_address - lowest_address;

  /* Round up the range so as to avoid fractions ... */
  if (range % N_PARTS)
    range = range + N_PARTS - (range % N_PARTS);

  delta = range / N_PARTS;

  /* If there is only one minimal symbol in this module, be sure not
     to anger the gods. There is actually such a library in our
     benchmarks suite ...
  */
  if (delta == 0)
    delta = 1;

  address = lowest_address;
  for (i=0; i < N_PARTS; i++)
    {
      objfile->minsym_address_partitions[i].lowest_address = address;
      objfile->minsym_address_partitions[i].highest_address =
                          address + delta - 1;
      objfile->minsym_address_partitions[i].minimal_symbol_count = 0;
      address += delta;
    }

    /* Special case handling for the last partition. */
    objfile->minsym_address_partitions[i].lowest_address = address;
    objfile->minsym_address_partitions[i].highest_address = highest_address;
    objfile->lowest_address = lowest_address;
    objfile->highest_address = highest_address;
    objfile->partition_interval = delta;

#endif 
  
  if (msym_count > 0)
    {
      /* Allocate enough space in the obstack, into which we will gather the
         bunches of new and existing minimal symbols, sort them, and then
         compact out the duplicate entries.  Once we have a final table,
         we will give back the excess space.  */

      alloc_count = msym_count + objfile->minimal_symbol_count + 1;
      obstack_blank (&objfile->symbol_obstack,
		     alloc_count * sizeof (struct minimal_symbol));
      msymbols = (struct minimal_symbol *)(void *)
	obstack_base (&objfile->symbol_obstack);

      /* Copy in the existing minimal symbols, if there are any.  */

      if (objfile->minimal_symbol_count)
	memcpy ((char *) msymbols, (char *) objfile->msymbols,
	    objfile->minimal_symbol_count * sizeof (struct minimal_symbol));

      /* Walk through the list of minimal symbol bunches, adding each symbol
         to the new contiguous array of symbols.  Note that we start with the
         current, possibly partially filled bunch (thus we use the current
         msym_bunch_index for the first bunch we copy over), and thereafter
         each bunch is full. */

      mcount = objfile->minimal_symbol_count;
      leading_char = get_symbol_leading_char (objfile->obfd);

      for (bunch = msym_bunch; bunch != NULL; )
	{
          for (bindex = 0;
               bindex < msym_bunch_index && mcount < alloc_count;
               bindex++, mcount++)
	    {
	      msymbols[mcount] = bunch->contents[bindex];
	      SYMBOL_LANGUAGE (&msymbols[mcount]) = language_auto;
              if (SYMBOL_NAME (&msymbols[mcount])
                  && SYMBOL_NAME (&msymbols[mcount])[0] == leading_char)
		{
		  SYMBOL_NAME (&msymbols[mcount])++;
		}

#ifdef BUCKET_SORT_MINSYMS

              name = SYMBOL_NAME (&msymbols[mcount]);
              histogram [name[0]]++;

              address = SYMBOL_VALUE_ADDRESS (&msymbols[mcount]);
              bucket = (address - lowest_address)/delta;
              objfile->minsym_address_partitions[bucket].minimal_symbol_count++;

#endif

	    }
	  msym_bunch_index = BUNCH_SIZE;

          /* srikanth, why wait to do a good deed ? */
          tmp_bunch = bunch->next;
          free (bunch);
          bunch = tmp_bunch;
        }

      msym_bunch = NULL;

#ifndef BUCKET_SORT_MINSYMS

      /* Sort the minimal symbols by address.  */

      qsort (msymbols, mcount, sizeof (struct minimal_symbol),
	     compare_minimal_symbols);

      /* Compact out any duplicates, and free up whatever space we are
         no longer using.  */

      mcount = compact_minimal_symbols (msymbols, mcount, objfile);

#endif

      obstack_blank (&objfile->symbol_obstack,
	       (mcount + 1 - alloc_count) * sizeof (struct minimal_symbol));
      msymbols = (struct minimal_symbol *)(void *)
	obstack_finish (&objfile->symbol_obstack);

      /* We also terminate the minimal symbol table with a "null symbol",
         which is *not* included in the size of the table.  This makes it
         easier to find the end of the table when we are handed a pointer
         to some symbol in the middle of it.  Zero out the fields in the
         "null symbol" allocated at the end of the array.  Note that the
         symbol count does *not* include this null symbol, which is why it
         is indexed by mcount and not mcount-1. */

      SYMBOL_NAME (&msymbols[mcount]) = NULL;
      SYMBOL_VALUE_ADDRESS (&msymbols[mcount]) = 0;

      MSYMBOL_INFO (&msymbols[mcount]) = NULL;
      MSYMBOL_TYPE (&msymbols[mcount]) = mst_unknown;
      SYMBOL_INIT_LANGUAGE_SPECIFIC (&msymbols[mcount], language_unknown);

      /* Attach the minimal symbol table to the specified objfile.
         The strings themselves are also located in the symbol_obstack
         of this objfile.  */

      objfile->minimal_symbol_count = mcount;
      objfile->msymbols = msymbols;

#ifdef HASH_ALL
      current_objfile = objfile;
      objfile->current_pst = NULL;
      objfile->hash_table = init_table (mcount);
      for (; mcount-- > 0; msymbols++)
	{
	  /* Add the msymbol only if it has a name */
	  if (msymbols->ginfo.name)
	    insert_into_table (objfile->hash_table, 
			       (void *) msymbols, NULL, NULL,
			       1, 0, 0, &objfile->symbol_obstack);
	}
    } /* Matches the opening brace for   if (msym_count > 0) */
  /* { - This is a dummy opening brace so that vi matches the closing
     brace down below. The actual opening brace is the one
     after if (msym_count > 0)
     */
#else

      /* Now walk through all the minimal symbols, selecting the newly added
         ones and attempting to cache their C++ demangled names. */

      for (; mcount-- > 0; msymbols++)
	{
          /* srikanth, when just in time demangling is in effect, defer
             all demangling except for C++ operator functions. These have
             to be demangled at start up as otherwise some commands such as
             rbreak delete would fail. */
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
	  objfile->alphabetic_msymbols[mcount] = msymbols;
#endif
          if (lazy_demangling)
            {
              name = SYMBOL_NAME(msymbols);
              if (name && name[0] == '_' && name[1] == '_' &&
		  special_cplusplus_fn(name + 2))
                SYMBOL_INIT_DEMANGLED_NAME (msymbols,
					    &objfile->symbol_obstack);
            }
          else if (SYMBOL_NAME (msymbols))  /* anticipatory demangling */
	    {
	      SYMBOL_INIT_DEMANGLED_NAME (msymbols, &objfile->symbol_obstack);
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
      /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
	 mangling lookup + lazy demangling, which cannot use hash
	 implementation and does not want demangling the whole world
	 behavior */
	      if (SYMBOL_DEMANGLED_NAME (msymbols) != NULL)
		add_minsym_to_demangled_hash_table (msymbols,
					      objfile->msymbol_demangled_hash);
#endif
	    }
        } /* for (; mcount-- > 0; msymbols++) */
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
	qsort (objfile->alphabetic_msymbols,
	       objfile->minimal_symbol_count,
	       sizeof (struct minimal_symbol *),
	       compare_minimal_symbols_by_name);
#endif
        
#ifdef BUCKET_SORT_MINSYMS
      partition_minimal_symbols_by_name (objfile, histogram);
      partition_minimal_symbols_by_address (objfile, N_PARTS);
#endif 
    } /* if (msym_count > 0) */
#endif 
}


/* Sort all the minimal symbols in OBJFILE.  */

void
msymbols_sort (struct objfile *objfile)
{
  qsort (objfile->msymbols, objfile->minimal_symbol_count,
	 sizeof (struct minimal_symbol), compare_minimal_symbols);
}

/* Check if PC is in a shared library trampoline code stub.
   Return minimal symbol for the trampoline entry or NULL if PC is not
   in a trampoline code stub.  */

struct minimal_symbol *
lookup_solib_trampoline_symbol_by_pc (CORE_ADDR pc)
{
  struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (pc);

  if (msymbol != NULL && MSYMBOL_TYPE (msymbol) == mst_solib_trampoline)
    return msymbol;
  return NULL;
}

/* If PC is in a shared library trampoline code stub, return the
   address of the `real' function belonging to the stub.
   Return 0 if PC is not in a trampoline code stub or if the real
   function is not found in the minimal symbol table.

   We may fail to find the right function if a function with the
   same name is defined in more than one shared library, but this
   is considered bad programming style. We could return 0 if we find
   a duplicate function in case this matters someday.  */

CORE_ADDR
find_solib_trampoline_target (CORE_ADDR pc)
{
  struct objfile *objfile;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *tsymbol = lookup_solib_trampoline_symbol_by_pc (pc);

  if (tsymbol != NULL)
    {
      ALL_MSYMBOLS (objfile, msymbol)
      {
	if (MSYMBOL_TYPE (msymbol) == mst_text
	    && STREQ (SYMBOL_NAME (msymbol), SYMBOL_NAME (tsymbol)))
	  return SYMBOL_VALUE_ADDRESS (msymbol);
      }
    }
  return 0;
}

struct minimal_symbol *
lookup_next_minimal_symbol (struct minimal_symbol *m)
{
  struct objfile * objfile;
  static struct minimal_symbol null_sym;
  int mcount;
  int p, i;

#ifndef BUCKET_SORT_MINSYMS
  return ++m;
#else

  /* Where did the input msymbol come from ? */
  for (objfile = object_files;
       objfile != NULL;
       objfile = objfile->next)
    {
      if (objfile->msymbols == 0)
        continue;
      mcount = objfile -> minimal_symbol_count;
      if (m >= objfile->msymbols && m < objfile->msymbols + mcount)
        break;
    }

  if (objfile != NULL)
    {
      CORE_ADDR pc;
      struct minsym_address_partition * partition;
      struct minimal_symbol ** msymbols;

      pc = SYMBOL_VALUE_ADDRESS (m);
      p = (pc - objfile->lowest_address)/objfile->partition_interval;
      partition = objfile->minsym_address_partitions + p;

      sort_minimal_symbol_partition_by_address (partition);

      msymbols = partition->msymbols;
      for (i=0; i < partition->minimal_symbol_count-1; i++)
        if (partition->msymbols[i] == m)
          return partition->msymbols[i+1];

      if (partition->msymbols[i] == m)
        {
          p++;
          while (p <= N_PARTS)
            {
              partition = objfile->minsym_address_partitions + p;
              if (partition->minimal_symbol_count != 0)
                {
                  sort_minimal_symbol_partition_by_address (partition);
                  return partition->msymbols[0];
                }
              p++;
            }
         }
    }
  return &null_sym;
#endif
}

#ifdef BUCKET_SORT_MINSYMS
static void
sort_minimal_symbol_partition_by_address (partition)
struct minsym_address_partition * partition;
{
  if (partition->minimal_symbol_count)
    if (!partition->sorted)
      {
        partition->sorted = 1;
        qsort (partition->msymbols,
               partition->minimal_symbol_count,
               sizeof (struct minimal_symbol *),
               compare_minimal_symbols_by_address);
        compact_minimal_symbol_partition (partition);
      }
}

static void
sort_minimal_symbol_partition_by_name (partition)
struct minsym_name_partition * partition;
{
  if (partition->minimal_symbol_count)
    if (partition->sorted == 0)
      {
        qsort (partition->msymbols,
               partition->minimal_symbol_count,
               sizeof (struct minimal_symbol *),
               compare_minimal_symbols_by_name);
        partition->sorted = 1;
      }
}

#endif /* BUCKET_SORT_MINSYMS */

/**************************************************************************
 * Caching for minimal symbol lookups  
 **************************************************************************/

#define MINIMAL_SYMBOL_CACHE_SIZE  256
#define MINIMAL_SYMBOL_BY_PC_CACHE_SIZE  256
struct minimal_symbol_cache {   /* sorted on an MRU basis */
  char *name;
  char *sfile;
  struct objfile *objf;
  struct minimal_symbol *past_minimal_symbol;  
} minimal_symbol_cache [MINIMAL_SYMBOL_CACHE_SIZE];

struct minimal_symbol_by_pc_cache {   /* sorted on an MRU basis */
  CORE_ADDR pc;
  struct minimal_symbol *past_minimal_symbol;
} minimal_symbol_by_pc_cache [MINIMAL_SYMBOL_BY_PC_CACHE_SIZE];

struct minimal_symbol *
lookup_minimal_symbol_by_pc (CORE_ADDR pc)
{
  struct minimal_symbol_by_pc_cache this, last;
  int i;

  if (!pc)
    return (NULL);

  if (is_swizzled)
    pc = SWIZZLE(pc);

  if (minimal_symbol_by_pc_cache[0].pc == pc)
    return minimal_symbol_by_pc_cache[0].past_minimal_symbol;

  /* not so lucky ... */

  last = minimal_symbol_by_pc_cache[0];

  /* Do we have the desired info in cache ?  As we scan the list of
     entries left to right, looking for the pc of interest, we will
     bubble the entries away from the beginning i.e., the "most recently
     used" slot. This would create a vacuum in the very first slot. 
     When we find the pc we are looking for, we will move that entry to
     this slot. Alternately, if the entry is not in the cache and we
     have to determine it the hard way, we will use this slot as the
     destination. This guarentees that the list is sorted on an MRU
     basis at all times.
  */

  for (i=1; i < MINIMAL_SYMBOL_BY_PC_CACHE_SIZE; i++)
    {
      this = minimal_symbol_by_pc_cache[i];
      minimal_symbol_by_pc_cache[i] = last;

      if (this.pc == pc)
        {
          minimal_symbol_by_pc_cache[0] = this;
          return this.past_minimal_symbol;
        }
      last = this;
    }

  minimal_symbol_by_pc_cache[0].pc = pc;
  minimal_symbol_by_pc_cache[0].past_minimal_symbol = 
    (struct minimal_symbol *) lookup_minimal_symbol_by_pc_1 (pc);
  return minimal_symbol_by_pc_cache[0].past_minimal_symbol;
}

/* In event that the child process is restarted, we clear all cache data
   corresponding to the old process.  
   This function clears 2 caches: 
     minimal_symbol_cache
     minimal_symbol_by_pc_cache */
void
clear_minimal_symbol_caches ()
{
  int i;
  for (i=0; i < MINIMAL_SYMBOL_CACHE_SIZE; i++)
    {
      free (minimal_symbol_cache[i].name);
      free (minimal_symbol_cache[i].sfile);
      minimal_symbol_cache[i].name = 0;
      minimal_symbol_cache[i].sfile = 0;
      minimal_symbol_cache[i].objf = 0;
      minimal_symbol_cache[i].past_minimal_symbol = NULL;
    }

  for (i=0; i < MINIMAL_SYMBOL_BY_PC_CACHE_SIZE; i++)
    {
      minimal_symbol_by_pc_cache[i].pc = 0;
      minimal_symbol_cache[i].past_minimal_symbol = NULL;
    }
}
