/* Target-dependent code for the HP PA architecture, for GDB.
   Copyright 1986, 1987, 1989-1996, 1999-2000 Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>

#include "mixed_mode.h"
#include "pa_save_state.h"
#include "defs.h"
#include "frame.h"
#include "bfd.h"
#include "elf-bfd.h"
#include "inferior.h"
#include "value.h"
#include <inttypes.h>

/* For argument passing to the inferior */
#include "symtab.h"

/* For Fortran array descriptor routines */
#include "f-lang.h"

#ifdef USG
#include <sys/types.h>
#endif

#include <dl.h>
#include <sys/param.h>
#include <signal.h>
#include <siginfo.h>

#include <sys/ptrace.h>
#include <sys/syscall.h>

#ifdef COFF_ENCAPSULATE
#include "a.out.encap.h"
#else
#endif

/*#include <sys/user.h>         After a.out.h  */
#include <sys/file.h>
#include "gdb_stat.h"
#include "gdb_wait.h"

#include "gdbcore.h"
#include "gdbcmd.h"
#include "target.h"
#include "symfile.h"
#include "objfiles.h"
#include "command.h"

#include <memory.h>
#include "hpread.h"
#include "javalib.h"	/* For Java Stack trace */

#include "gdbtypes.h"

#ifdef HP_MXN
#define MXN_THREAD_INITIAL_FRAME_SYMBOL  "__pthread_unbound_body"
#endif

extern boolean print_bad_pc_unwind_msg;

extern asection *bfd_get_section_by_name_and_id (bfd *abfd,
						 const char *name,
						 int id);
static int mixed_mode_find_proc_framesize (CORE_ADDR);
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
extern bool objfile_is_mixed_mode (struct objfile *objfile);
extern int pc_in_user_sendsig(CORE_ADDR pc);

static int find_return_regnum (CORE_ADDR);

union hppa_unwind_table_entry *hppa_find_unwind_entry (CORE_ADDR);

void
mixed_mode_frame_find_saved_regs (struct frame_info *,
                                  struct mixed_mode_frame_saved_regs *);

CORE_ADDR mixed_mode_skip_prologue_hard_way (CORE_ADDR);

CORE_ADDR skip_epilogue_hard_way (CORE_ADDR);

static int prologue_inst_adjust_sp (unsigned long);

static int is_branch (unsigned long);

static int inst_saves_gr (unsigned long);

static int inst_saves_fr (unsigned long);

int hppa_pc_in_gcc_function (CORE_ADDR);

static int pc_in_linker_stub (CORE_ADDR);

static int mixed_mode_compare_unwind_entries (const void *, const void *);

void hppa_read_unwind_info (struct objfile *);

static void mixed_mode_internalize_unwinds (struct objfile *,
				 union hppa_unwind_table_entry *,
				 asection *, unsigned int,
				 unsigned int, CORE_ADDR);
static void mixed_mode_print_registers (void *, int, int);
static void mixed_mode_print_fp_reg (int, enum precision_type, void *);

typedef struct
  {
    struct minimal_symbol *msym;
    CORE_ADDR solib_handle;
    CORE_ADDR return_val;
  }
args_for_find_stub;

/* This is declared in infrun.c. */
extern CORE_ADDR prev_pc;

uint32_t num_pa_std_regs = 2;
struct std_regs *pa_std_regs;

static int is_pa_2 = 0;		/* False */

CORE_ADDR aries_pa2ia_process_addr;
extern bool mixed_mode_pa_unwind;
extern CORE_ADDR inferior_aries_text_start;
extern CORE_ADDR inferior_aries_text_end;
extern bool      aries_helper_lib_loaded;
extern shl_t     aries_shlib_handle;
extern struct frame_info *aries_border_frame;
extern int       context_nbr;

CORE_ADDR
mixed_mode_frame_saved_pc (struct frame_info *frame);

void
mixed_mode_create_pa_std_regs (void)
{
  pa_std_regs = (struct std_regs *) xmalloc
                ((num_pa_std_regs) * sizeof (struct std_regs));

  pa_std_regs[0].name = strdup ("pc");
  pa_std_regs[0].regnum = 33; /* PCOQ_HEAD_REGNUM */
  
  pa_std_regs[1].name = strdup ("fp");
  pa_std_regs[1].regnum = PA_FP_REGNUM;
}

void
mixed_mode_free_std_regs ()
{
  if (pa_std_regs)
    {
      free (pa_std_regs[0].name);
      free (pa_std_regs[1].name);
      free (pa_std_regs);
      pa_std_regs = NULL;
    }
}

void
mixed_mode_cleanup ()
{
  int status = 0;

  mixed_mode_pa_unwind = false;
  context_nbr = -2;
  if (aries_helper_lib_loaded)
    {
      status = shl_unload (aries_shlib_handle);
      if (0 != status)
        {
          error ("Unable to unload the aries helper library.");
        }
    }
  aries_helper_lib_loaded = false;
  inferior_aries_text_start = NULL;
  inferior_aries_text_end = NULL;
  aries_border_frame = NULL;
}


CORE_ADDR
mixed_mode_swizzle (CORE_ADDR pc)
{
  if (!is_swizzled)
    return pc;
  pc = SWIZZLE_ADDR (pc);
  return swizzle (pc);
}

void
mixed_mode_get_frame_saved_regs (struct frame_info *frame,
                                 struct mixed_mode_frame_saved_regs
                                                   *saved_regs_addr)
{
  int nbr_pa_regs = (is_swizzled)? PA32_NUM_REGS: PA64_NUM_REGS;

  if (frame->saved_regs == NULL)
    {
      frame->saved_regs = (CORE_ADDR *)
	frame_obstack_alloc (sizeof (CORE_ADDR) * nbr_pa_regs);
    }
  if (saved_regs_addr == NULL)
    {
      struct mixed_mode_frame_saved_regs saved_regs;
      mixed_mode_frame_find_saved_regs (frame, &saved_regs);
      memcpy (frame->saved_regs,
              &saved_regs,
              sizeof (CORE_ADDR) * nbr_pa_regs);
    }
  else
    {
      mixed_mode_frame_find_saved_regs (frame, saved_regs_addr);
      memcpy (frame->saved_regs, saved_regs_addr,
              sizeof (CORE_ADDR) * nbr_pa_regs);
    }
}

/* Check if we need to handle bad PC values */
int 
mixed_mode_handle_bad_pc (struct frame_info * frame)
{
  /* Are we the top frame or the frame calling a signal handler ?*/
  if (frame->next && !frame->next->signal_handler_caller)
    return 0; /* Can only handle bad PC for these cases */

  /* Is it a Good PC ? */
  if (hppa_find_unwind_entry(frame->pc) 	/* HP C function usually */
    || (hppa_pc_in_gcc_function((CORE_ADDR) frame->pc))) /* In GCC code */
    return 0; /* Good PC */

  /* We can handle this bad PC */
  return 1;
}

/* Obtain the previous frame if the current frame has a bad PC */
void
mixed_mode_do_handle_bad_pc (struct frame_info * frame, struct frame_info * prev)
{
  union hppa_unwind_table_entry *u;

  /* While profiling, PC samples can fall on BOR code and such
     that don't have unwind entries. We don't want the warning
     messages printed for these. Silently handle if possible,
     if not, tough luck - Srikanth, Oct 10th 2005.
  */
  if (!profile_on && print_bad_pc_unwind_msg)
    warning ("Attempting to unwind past bad PC %s ",
                   longest_local_hex_string ((LONGEST) frame->pc));
  if (frame->next == 0)	/* Top frame */
    {
      if (is_swizzled)
        {
          frame->frame = frame->pa_save_state_ptr->ss_narrow.ss_sp;
 	  /* use value of SP */
        }
      else
        {
          frame->frame = frame->pa_save_state_ptr->ss_wide.ss_64.ss_sp;
        }
      }
  else 
    /* Signal Handler caller */
    /* Get SP saved in the save state of the signal handler caller*/
    if (read_relative_register_raw_bytes_for_frame(PA_SP_REGNUM, 
      (char *) &frame->frame, frame)) 
	/* Couldn't find SP, use callee frame's frame pointer */
	frame->frame=frame->next->frame; 

    /* Initialize PC from rp value */
    prev->pc = mixed_mode_frame_saved_pc (frame);

    if (u = hppa_find_unwind_entry(prev->pc))
      {
        unsigned int Total_frame_size = is_swizzled?
                                               u->u32.Total_frame_size:
                                               u->u64.Total_frame_size;
        /* Calculate frame pointer using unwind entry */
        prev->frame = frame->frame - (Total_frame_size << 3);
      }
    else if (hppa_pc_in_gcc_function((CORE_ADDR) prev->pc)) 
    {
      /* gcc + mixed mode : Not supported */
      error ("Debugging of PA gcc compiled code is not supported "
             "with mixed mode.");
#if 0
      if (frame->next == 0) /* Top frame */
        prev->frame = TARGET_READ_FP ();
      else
	if (read_relative_register_raw_bytes_for_frame(PA_FP_REGNUM, 
	  (char *) &prev->frame, frame))
	  /* Couldn't find FP, use callee frame's frame pointer */
	     frame->frame=frame->frame;
#endif
    }
    else
      prev->frame = frame->frame; /* Not a good PC either, use previous FP */

}


int
in_syscall (CORE_ADDR flags)
{
  return ((flags & 0x2) && (!(flags & 0x20)));
}

/* RM: SS_INSYSCALL but not SS_DORFI */
#define IN_SYSCALL(flags) (in_syscall((flags)))

/* Routines to extract various sized constants out of hppa 
   instructions. */

/* This assumes that no garbage lies outside of the lower bits of 
   value. */

static int
sign_extend (unsigned val, unsigned bits)
{
  return (int) (val >> (bits - 1) ? (-1 << bits) | val : val);
}

/* For many immediate values the sign bit is the low bit! */

static int
low_sign_extend (unsigned val, unsigned bits)
{
  return (int) ((val & 0x1 ? (-1 << (bits - 1)) : 0) | val >> 1);
}

/* extract the immediate field from a ld{bhw}s instruction */

static int
extract_5_load (unsigned word)
{
  return low_sign_extend (word >> 16 & MASK_5, 5);
}

/* extract the immediate field from a break instruction */

static unsigned
extract_5r_store (unsigned word)
{
  return (word & MASK_5);
}

/* extract the immediate field from a {sr}sm instruction */

static unsigned
extract_5R_store (unsigned word)
{
  return (word >> 16 & MASK_5);
}

/* extract a 14 bit immediate field */

int
extract_14 (unsigned word)
{
  return low_sign_extend (word & MASK_14, 14);
}

/* deposit a 14 bit constant in a word */

unsigned
deposit_14 (int opnd, unsigned word)
{
  unsigned sign = (opnd < 0 ? 1 : 0);

  return word | ((unsigned) opnd << 1 & MASK_14) | sign;
}

/* extract a 21 bit constant */

static int
extract_21 (unsigned word)
{
  int val;

  word &= MASK_21;
  word <<= 11;
  val = GET_FIELD (word, 20, 20);
  val <<= 11;
  val |= GET_FIELD (word, 9, 19);
  val <<= 2;
  val |= GET_FIELD (word, 5, 6);
  val <<= 5;
  val |= GET_FIELD (word, 0, 4);
  val <<= 2;
  val |= GET_FIELD (word, 7, 8);
  return sign_extend (val, 21) << 11;
}


/* deposit a 21 bit constant in a word. Although 21 bit constants are
   usually the top 21 bits of a 32 bit constant, we assume that only
   the low 21 bits of opnd are relevant */

unsigned
deposit_21 (unsigned opnd, unsigned word)
{
  unsigned val = 0;

  val |= GET_FIELD (opnd, 11 + 14, 11 + 18);
  val <<= 2;
  val |= GET_FIELD (opnd, 11 + 12, 11 + 13);
  val <<= 2;
  val |= GET_FIELD (opnd, 11 + 19, 11 + 20);
  val <<= 11;
  val |= GET_FIELD (opnd, 11 + 1, 11 + 11);
  val <<= 1;
  val |= GET_FIELD (opnd, 11 + 0, 11 + 0);
  return word | val;
}

/* extract a 17 bit constant from branch instructions, returning the
   19 bit signed value. */

static int
extract_17 (unsigned word)
{
  return sign_extend (GET_FIELD (word, 19, 28) |
		      GET_FIELD (word, 29, 29) << 10 |
		      GET_FIELD (word, 11, 15) << 11 |
		      (word & 0x1) << 16, 17) << 2;
}

/* Deposit a 17 bit constant in an instruction (like bl). */

unsigned int
deposit_17 (unsigned opnd, unsigned word)
{
  word |= GET_FIELD (opnd, 15 + 0, 15 + 0); /* w */
  word |= GET_FIELD (opnd, 15 + 1, 15 + 5) << 16; /* w1 */
  word |= GET_FIELD (opnd, 15 + 6, 15 + 6) << 2; /* w2[10] */
  word |= GET_FIELD (opnd, 15 + 7, 15 + 16) << 3; /* w2[0..9] */

  return word;
}

/* Extract an im10a immediate from an instruction like LDD 
   im10a defines 14 bits, the low three bits are zero, plus the sign
   bit makes 14.
*/

int
extract_im10a (unsigned inst)
{
  int im10a;
  int sign_bit;
  int s;

/* RM: Different for PA32 and PA64 */
  if (!is_swizzled)
    {
      sign_bit = inst & 1;
      im10a = (inst >> 4) & 0x03ff;
      im10a <<= 3;
      s = (inst & 0x0000c000) >> 14;
      if (sign_bit)
        {
          im10a |= (~s) << 13;
        }
      else
        {
          im10a |= s << 13;
        }
     }
   else
     {
       sign_bit = inst & 1;
       im10a = (inst >> 4) & 0x03ff;
       im10a <<= 3;
       if (sign_bit)
         im10a |= 0xffffe000;
     }
  return im10a;
} /* end extract_ima10a */


/* Compare the start address for two unwind entries returning 1 if 
   the first address is larger than the second, -1 if the second is
   larger than the first, and zero if they are equal.  */

static int
mixed_mode_compare_unwind_entries (const void *arg1, const void *arg2)
{
  const union hppa_unwind_table_entry *a = arg1;
  const union hppa_unwind_table_entry *b = arg2;

  if (a->u64.region_start > b->u64.region_start)
    return 1;
  else if (a->u64.region_start < b->u64.region_start)
    return -1;
  else
    return 0;
}

static void
mixed_mode_internalize_unwinds (
     struct objfile *objfile,
     union hppa_unwind_table_entry *table,
     asection *section,
     unsigned int entries,
     unsigned int size,
     CORE_ADDR text_offset)
{
  /* We will read the unwind entries into temporary memory, then
     fill in the actual unwind table.  */
  if (size > 0)
    {
      unsigned long tmp;
      unsigned i;
      char *buf = (char *) alloca (size);

      bfd_get_section_contents (objfile->obfd, section, buf, 0, size);

      /* Now internalize the information being careful to handle host/target
         endian issues.  */
      for (i = 0; i < entries; i++)
	{
          if (is_swizzled)
            {
               table[i].u32.region_start = bfd_get_32 (objfile->obfd,
					      (bfd_byte *) buf);
	       table[i].u32.region_start += text_offset;
	       buf += 4;
	       table[i].u32.region_end = bfd_get_32 (objfile->obfd,
                                                    (bfd_byte *) buf);
	       table[i].u32.region_end += text_offset;
	       buf += 4;
	       tmp = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	       buf += 4;
	       table[i].u32.Cannot_unwind = (tmp >> 31) & 0x1;
	       table[i].u32.Millicode = (tmp >> 30) & 0x1;
	       table[i].u32.Millicode_save_sr0 = (tmp >> 29) & 0x1;
	       table[i].u32.Region_description = (tmp >> 27) & 0x3;
	       table[i].u32.reserved1 = (tmp >> 26) & 0x1;
	       table[i].u32.Entry_SR = (tmp >> 25) & 0x1;
	       table[i].u32.Entry_FR = (tmp >> 21) & 0xf;
	       table[i].u32.Entry_GR = (tmp >> 16) & 0x1f;
	       table[i].u32.Args_stored = (tmp >> 15) & 0x1;
	       table[i].u32.Variable_Frame = (tmp >> 14) & 0x1;
	       table[i].u32.Separate_Package_Body = (tmp >> 13) & 0x1;
	       table[i].u32.Frame_Extension_Millicode = (tmp >> 12) & 0x1;
	       table[i].u32.Stack_Overflow_Check = (tmp >> 11) & 0x1;
	       table[i].u32.Two_Instruction_SP_Increment = (tmp >> 10) & 0x1;
	       table[i].u32.Ada_Region = (tmp >> 9) & 0x1;
	       table[i].u32.cxx_info = (tmp >> 8) & 0x1;
	       table[i].u32.cxx_try_catch = (tmp >> 7) & 0x1;
	       table[i].u32.sched_entry_seq = (tmp >> 6) & 0x1;
	       table[i].u32.reserved2 = (tmp >> 5) & 0x1;
	       table[i].u32.Save_SP = (tmp >> 4) & 0x1;
	       table[i].u32.Save_RP = (tmp >> 3) & 0x1;
	       table[i].u32.Save_MRP_in_frame = (tmp >> 2) & 0x1;
	       table[i].u32.extn_ptr_defined = (tmp >> 1) & 0x1;
	       table[i].u32.Cleanup_defined = tmp & 0x1;
	       tmp = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	       buf += 4;
	       table[i].u32.MPE_XL_interrupt_marker = (tmp >> 31) & 0x1;
	       table[i].u32.HP_UX_interrupt_marker = (tmp >> 30) & 0x1;
	       table[i].u32.Large_frame = (tmp >> 29) & 0x1;
	       table[i].u32.Pseudo_SP_Set = (tmp >> 28) & 0x1;
	       table[i].u32.reserved4 = (tmp >> 27) & 0x1;
	       table[i].u32.Total_frame_size = tmp & 0x7ffffff;

	       /* Stub unwinds are handled elsewhere. */
	       table[i].u32.stub_unwind.stub_type = 0;
	       table[i].u32.stub_unwind.padding = 0;
            } /* is_swizzled */
          else
            {
               table[i].u64.region_start = bfd_get_32 (objfile->obfd,
					      (bfd_byte *) buf);
	       table[i].u64.region_start += text_offset;
	       buf += 4;
	       table[i].u64.region_end = bfd_get_32 (objfile->obfd,
                                                    (bfd_byte *) buf);
	       table[i].u64.region_end += text_offset;
	       buf += 4;
	       tmp = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	       buf += 4;
	       table[i].u64.Cannot_unwind = (tmp >> 31) & 0x1;
	       table[i].u64.Millicode = (tmp >> 30) & 0x1;
	       table[i].u64.Region_description = (tmp >> 27) & 0x3;
	       table[i].u64.reserved1 = (tmp >> 26) & 0x1;
	       table[i].u64.Entry_SR = (tmp >> 25) & 0x1;
	       table[i].u64.Entry_FR = (tmp >> 21) & 0xf;
	       table[i].u64.Entry_GR = (tmp >> 16) & 0x1f;
	       table[i].u64.Args_stored = (tmp >> 15) & 0x1;
               table[i].u64.reserved2 = (tmp >> 12) & 0x7;
	       table[i].u64.Stack_Overflow_Check = (tmp >> 11) & 0x1;
	       table[i].u64.Two_Instruction_SP_Increment = (tmp >> 10) & 0x1;
               table[i].u64.reserved3 = (tmp >> 9) & 0x1;
	       table[i].u64.cxx_info = (tmp >> 8) & 0x1;
	       table[i].u64.cxx_try_catch = (tmp >> 7) & 0x1;
	       table[i].u64.sched_entry_seq = (tmp >> 6) & 0x1;
               table[i].u64.reserved4 = (tmp >> 5) & 0x1;
	       table[i].u64.Save_SP = (tmp >> 4) & 0x1;
	       table[i].u64.Save_RP = (tmp >> 3) & 0x1;
	       table[i].u64.Save_MRP_in_frame = (tmp >> 2) & 0x1;
               table[i].u64.reserved5 = (tmp >> 1) & 0x1;
	       table[i].u64.Cleanup_defined = tmp & 0x1;
	       tmp = bfd_get_32 (objfile->obfd, (bfd_byte *) buf);
	       buf += 4;
               table[i].u64.reserved6 = (tmp >> 31) & 0x1;
	       table[i].u64.HP_UX_interrupt_marker = (tmp >> 30) & 0x1;
	       table[i].u64.Large_frame = (tmp >> 29) & 0x1;
               table[i].u64.alloca_frame = (tmp >> 28) & 0x1;
               table[i].u64.reserved7 = (tmp >> 27) & 0x1;
	       table[i].u64.Total_frame_size = tmp & 0x7ffffff;

            } /*! is_swizzled */
	}
    }
}

/* Read in the backtrace information stored in the `$UNWIND_START$' section of
   the object file.  This info is used mainly by hppa_find_unwind_entry() to find
   out the stack frame size and frame pointer used by procedures.  We put
   everything on the psymbol obstack in the objfile so that it automatically
   gets freed when the objfile is destroyed.  */
/* jini: Mixed mode corefile debug support. Renamed read_unwind_info to
   hppa_read_unwind_info to avoid symbol clashes with the IPF version. */

void
hppa_read_unwind_info (objfile)
     struct objfile *objfile;
{
  asection *unwind_sec, *stub_unwind_sec;
  unsigned unwind_size, stub_unwind_size, total_size;
  unsigned index, unwind_entries;
  unsigned stub_entries, total_entries;
  CORE_ADDR text_offset;
  struct hppa_obj_unwind_info *ui;
  hppa_obj_private_data_t *obj_private;

  if (is_swizzled) /* Mixed mode 32 bit case. */
    text_offset = ANOFFSET (objfile->section_offsets, 0);
  else /* Mixed mode 64 bit case. */
    text_offset = elf_text_start_vma(objfile->obfd)
                + ANOFFSET (objfile->section_offsets, 0);

  ui = (struct hppa_obj_unwind_info *) obstack_alloc (&objfile->psymbol_obstack,
				sizeof (struct hppa_obj_unwind_info));

  ui->table = NULL;
  ui->cache = NULL;
  ui->last = -1;

  /* For reasons unknown the HP PA64 tools generate multiple unwinder
     sections in a single executable.  So we just iterate over every
     section in the BFD looking for unwinder sections intead of trying
     to do a lookup with bfd_get_section_by_name. 

     First determine the total size of the unwind tables so that we
     can allocate memory in a nice big hunk.  */
  total_entries = 0;

  for (unwind_sec = objfile->obfd->sections;
       unwind_sec;
       unwind_sec = unwind_sec->next)
    {
      if (strcmp (unwind_sec->name, "$UNWIND_START$") == 0
	  || strcmp (unwind_sec->name, ".PARISC.unwind") == 0)
	{

	  unwind_size = bfd_section_size (objfile->obfd, unwind_sec);
	  unwind_entries = unwind_size / PA_UNWIND_ENTRY_SIZE;

	  total_entries += unwind_entries;
	}
    }

  /* Now compute the size of the stub unwinds.  Note the ELF tools do not
     use stub unwinds at the current time.  */
  stub_unwind_sec = bfd_get_section_by_name (objfile->obfd, "$UNWIND_END$");

  if (stub_unwind_sec)
    {
      stub_unwind_size = bfd_section_size (objfile->obfd, stub_unwind_sec);
      stub_entries = stub_unwind_size / PA_STUB_UNWIND_ENTRY_SIZE;
    }
  else
    {
      stub_unwind_size = 0;
      stub_entries = 0;
    }

  /* Compute total number of unwind entries and their total size.  */
  total_entries += stub_entries;
  total_size = total_entries * sizeof (union hppa_unwind_table_entry);

  /* Allocate memory for the unwind table.  */
  ui->table = (union hppa_unwind_table_entry *)
    obstack_alloc (&objfile->psymbol_obstack, total_size);
  ui->last = total_entries - 1;

  /* Now read in each unwind section and internalize the standard unwind
     entries.  */
  index = 0;
  for (unwind_sec = objfile->obfd->sections;
       unwind_sec;
       unwind_sec = unwind_sec->next)
    {
      if (strcmp (unwind_sec->name, "$UNWIND_START$") == 0
	  || strcmp (unwind_sec->name, ".PARISC.unwind") == 0)
	{
	  unwind_size = bfd_section_size (objfile->obfd, unwind_sec);
	  unwind_entries = unwind_size / PA_UNWIND_ENTRY_SIZE;

	  mixed_mode_internalize_unwinds (objfile, &ui->table[index],
                                          unwind_sec,
			       unwind_entries, unwind_size, text_offset);
	  index += unwind_entries;
	}
    }

  /* Now read in and internalize the stub unwind entries.  */
  if (stub_unwind_size > 0)
    {
      unsigned int i;
      char *buf = (char *) alloca (stub_unwind_size);

      /* Read in the stub unwind entries.  */
      bfd_get_section_contents (objfile->obfd, stub_unwind_sec, buf,
				0, stub_unwind_size);

      /* Now convert them into regular unwind entries.  */
      for (i = 0; i < stub_entries; i++, index++)
	{
	  /* Clear out the next unwind entry.  */
	  memset (&ui->table[index], 0,
                     sizeof (union hppa_unwind_table_entry));

	  /* Convert offset & size into region_start and region_end.  
	     Stuff away the stub type into "reserved" fields.  */
	  ui->table[index].u64.region_start = bfd_get_32 (objfile->obfd,
						      (bfd_byte *) buf);
	  ui->table[index].u64.region_start += text_offset;
	  buf += 4;
          if (is_swizzled)
	    ui->table[index].u32.stub_unwind.stub_type = bfd_get_8 (
                                                objfile->obfd,
						(bfd_byte *) buf);
	  buf += 2;
	  ui->table[index].u64.region_end
	    = ui->table[index].u64.region_start + 4 *
	    (bfd_get_16 (objfile->obfd, (bfd_byte *) buf) - 1);
	  buf += 2;
	}

    }

  /* Unwind table needs to be kept sorted.  */
  qsort (ui->table, total_entries, sizeof (union hppa_unwind_table_entry),
	 mixed_mode_compare_unwind_entries);

  /* Keep a pointer to the unwind information.  */
  if (objfile->obj_private == NULL)
    {
      obj_private = (hppa_obj_private_data_t *)
	obstack_alloc (&objfile->psymbol_obstack,
		       sizeof (hppa_obj_private_data_t));
      obj_private->unwind_info = NULL;
      obj_private->so_info = NULL;
      obj_private->opd = NULL;

      objfile->obj_private = (PTR) obj_private;
    }
  obj_private = (hppa_obj_private_data_t *) objfile->obj_private;
  obj_private->unwind_info = ui;
}

/* Lookup the unwind (stack backtrace) info for the given PC.  We search all
   of the objfiles seeking the unwind table entry for this PC.  Each objfile
   contains a sorted list of struct hppa_unwind_table_entry.Since we do a binary
   search of the unwind tables, we depend upon them to be sorted.  */

union hppa_unwind_table_entry *
hppa_find_unwind_entry (pc)
     CORE_ADDR pc;
{
  int first, middle, last;
  struct objfile *objfile;

  /* A function at address 0?  Not in HP-UX! */
  if (pc == (CORE_ADDR) 0)
    return NULL;

  ALL_OBJFILES (objfile)
  {
    struct hppa_obj_unwind_info *ui = NULL;

    if (!(objfile->is_mixed_mode_pa_lib || objfile_is_mixed_mode (objfile)))
      {
        continue;
      }
    if (objfile->obj_private)
      ui = ((hppa_obj_private_data_t *) (objfile->obj_private))->unwind_info;

    if (!ui)
      {
	hppa_read_unwind_info (objfile);
	if (objfile->obj_private == NULL)
	  error ("Internal error reading unwind information.");
	ui = ((hppa_obj_private_data_t *) (objfile->obj_private))->unwind_info;

#if 0          
          /* RM: ??? The unwind for $cerror indicates that it is a
             millicode routine, but the disassembly shows that the
             return register is r2 instead of r31. Clear the millicode
             flag */
            {
              struct minimal_symbol *msym;
              struct unwind_table_entry *u;
              
              msym = lookup_minimal_symbol("$cerror", 0, objfile);
              if (msym)
                {
                  u = hppa_find_unwind_entry(SYMBOL_VALUE_ADDRESS(msym));
                  if (u)
                    u->Millicode = 0;
                }
            }
#endif
      }

    /* First, check the cache */

    if (ui->cache
	&& pc >= ui->cache->u64.region_start
	&& pc <= ui->cache->u64.region_end)
      return ui->cache;

    /* Not in the cache, do a binary search */

    first = 0;
    last = ui->last;

    /* Since region* is only being compared, it should not make a difference if
     * we are using u32 or u64. */
    while (first <= last)
      {
	middle = (first + last) / 2;
	if (pc >= ui->table[middle].u64.region_start
	    && pc <= ui->table[middle].u64.region_end)
	  {
	    /* RM: ??? Incremental linker bug: the incremental
	       linker seems to be using non-inclusive end addresses for
	       unwind descriptors that are unused. The following hack
	       works around this */
	    while (middle < last &&
		   pc >= ui->table[middle+1].u64.region_start
		   && pc <= ui->table[middle+1].u64.region_end)
	      middle++;
	    ui->cache = &ui->table[middle];
	    return &ui->table[middle];
	  }

	if (pc < ui->table[middle].u64.region_start)
	  last = middle - 1;
	else
	  first = middle + 1;
      }
  }				/* ALL_OBJFILES() */
  return NULL;
}

/* Return the adjustment necessary to make for addresses on the stack
   as presented by hpread.c.

   This is necessary because of the stack direction on the PA and the
   bizarre way in which someone (?) decided they wanted to handle
   frame pointerless code in GDB.  */
int
mixed_mode_hpread_adjust_stack_address (func_addr)
     CORE_ADDR func_addr;
{
  union hppa_unwind_table_entry *u;

  u = hppa_find_unwind_entry (func_addr);
  if (!u)
    return 0;
  unsigned int Total_frame_size = is_swizzled? u->u32.Total_frame_size:
                                               u->u64.Total_frame_size;
  return Total_frame_size << 3;
}


/* Called when no unwind descriptor was found for PC.  Returns 1 if it
   appears that PC is in a linker stub.

   ?!? Need to handle stubs which appear in PA64 code.  */

static int
pc_in_linker_stub (pc)
     CORE_ADDR pc;
{
  int found_magic_instruction = 0;
  int i;
  char buf[4];

  /* If unable to read memory, assume pc is not in a linker stub.  */
  if (target_read_memory (pc, buf, 4) != 0)
    return 0;

  /* We are looking for something like

     ; $$dyncall jams RP into this special spot in the frame (RP')
     ; before calling the "call stub"
     ldw     -18(sp),rp

     ldsid   (rp),r1         ; Get space associated with RP into r1
     mtsp    r1,sp           ; Move it into space register 0
     be,n    0(sr0),rp)      ; back to your regularly scheduled program */

  /* Maximum known linker stub size is 4 instructions.  Search forward
     from the given PC, then backward.  */
  for (i = 0; i < 4; i++)
    {
      /* If we hit something with an unwind, stop searching this direction.  */

      if (hppa_find_unwind_entry (pc + i * 4) != 0)
	break;

      /* Check for ldsid (rp),r1 which is the magic instruction for a 
         return from a cross-space function call.  */
      if (read_memory_integer (pc + i * 4, 4) == 0x004010a1)
	{
	  found_magic_instruction = 1;
	  break;
	}
      /* Add code to handle long call/branch and argument relocation stubs
         here.  */
    }

  if (found_magic_instruction != 0)
    return 1;

  /* Now look backward.  */
  for (i = 0; i < 4; i++)
    {
      /* If we hit something with an unwind, stop searching this direction.  */

      if (hppa_find_unwind_entry (pc - i * 4) != 0)
	break;

      /* Check for ldsid (rp),r1 which is the magic instruction for a 
         return from a cross-space function call.  */
      if (read_memory_integer (pc - i * 4, 4) == 0x004010a1)
	{
	  found_magic_instruction = 1;
	  break;
	}
      /* Add code to handle long call/branch and argument relocation stubs
         here.  */
    }
  return found_magic_instruction;
}

static int
find_return_regnum (CORE_ADDR pc)
{
  union hppa_unwind_table_entry *u;

  u = hppa_find_unwind_entry (pc);

  if (!u)
    return PA_RP_REGNUM;

/* RM: According to the PA64 RAT, local millicode calls put the return
   pointer in RP and external millicode calls put the return pointer
   in r31. This would be a problem, except that we never generate
   external millicode. */
  if (is_swizzled)
    {
      if (u->u32.Millicode)
        return 31;

      /* RM: If LONG_BRANCH_MRP is what I think it is, then this should return
         31 too. */
      if (u->u32.stub_unwind.stub_type == LONG_BRANCH_MRP)
        return 31;
    }
 
  return PA_RP_REGNUM;
}

int
hppa_pc_in_gcc_function (pc)
     CORE_ADDR pc;
{
  struct symbol *func_sym;

  func_sym = find_pc_function (pc);
  if (func_sym
      && BLOCK_GCC_COMPILED (SYMBOL_BLOCK_VALUE (func_sym))
      != HP_COMPILED_TARGET)
    return 1;
  return 0;
}


/* Return size of frame, or -1 if we should use a frame pointer.  */
static int
mixed_mode_find_proc_framesize (pc)
     CORE_ADDR pc;
{
  union hppa_unwind_table_entry *u = NULL;
  struct minimal_symbol *msym_us;

  /* This may indicate a bug in our callers... */
  if (pc == (CORE_ADDR) 0)
    return -1;

  u = hppa_find_unwind_entry (pc);

  if (!u)
    {
      if (pc_in_linker_stub (pc))
	/* Linker stubs have a zero size frame.  */
	return 0;
      else
	return -1;
    }

  msym_us = lookup_minimal_symbol_by_pc (pc);

  if ((is_swizzled && u->u32.Save_SP) || (!is_swizzled && u->u64.Save_SP))
    {
      if (hppa_pc_in_gcc_function(pc))
        return -1;
    }

  /* JYG: MERGE FIXME: the next conditional seems to bomb out
     gdb.hp/gdb.aCC/exception.exp w.r.t. dynamic executable catch catch */
  /* RM: With HP's compilers, the alloca_frame bit indicates whether
     the frame pointer is saved in a register (either r3 or r4) or not */
  if (!hppa_pc_in_gcc_function(pc) &&
      (   (!is_swizzled && u->u64.alloca_frame)
       || (is_swizzled && u->u32.Pseudo_SP_Set)))
    return -1;

  if (is_swizzled)
    return u->u32.Total_frame_size << 3;
  else
    return u->u64.Total_frame_size << 3;
}

/* Return offset from sp at which rp is saved, or 0 if not saved.  */
static int rp_saved (CORE_ADDR);

static int
rp_saved (pc)
     CORE_ADDR pc;
{
  union hppa_unwind_table_entry *u;
  CORE_ADDR prologue_begin, prologue_end, epilogue_begin, epilogue_end;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;
  int target_ptr_bit = (is_swizzled ? 32: 64);

  /* A function at, and thus a return PC from, address 0?  Not in HP-UX! */
  if (pc == (CORE_ADDR) 0)
    return 0;

  /* RM: if we are in the prologue of a function, rp may not yet be
     saved. More hacks. */
  find_pc_partial_function (pc, 0, &prologue_begin, 0);
  prologue_end = mixed_mode_skip_prologue_hard_way (prologue_begin);

  /* RM: similarly for epilogues */
  u = hppa_find_unwind_entry (pc);
  if (u)
    {
      /* RM: Are we in an unwind region with an exit point? */
      unsigned int Region_description =
        is_swizzled ? u->u32.Region_description: u->u64.Region_description;
      CORE_ADDR region_end =
        is_swizzled ? u->u32.region_end: u->u64.region_end;
      if (!(Region_description & 0x1))
        {
          epilogue_end = region_end;
          epilogue_begin = skip_epilogue_hard_way (epilogue_end);

          if (pc > epilogue_begin)
            return 0;
        }
    }
  
  /* RM: for _sr4export, pc is saved at offset -24 */
  msym = lookup_minimal_symbol_by_pc (pc);
  msym_sr4export = lookup_minimal_symbol ("_sr4export", NULL, NULL);
  if (msym
      && msym_sr4export
      && SYMBOL_VALUE_ADDRESS (msym) ==
           SYMBOL_VALUE_ADDRESS (msym_sr4export))
    {
      return -24;
    }

  if (!u)
    {
      if (pc_in_linker_stub (pc))
	/* This is the so-called RP'.  */
	return -24;
      else
	return 0;
    }

  unsigned int Save_RP = is_swizzled? u->u32.Save_RP: u->u64.Save_RP;
  if (Save_RP)
    return (target_ptr_bit == 64 ? -16 : -20);
  else if (is_swizzled && u->u32.stub_unwind.stub_type != 0)
    {
      switch (u->u32.stub_unwind.stub_type)
	{
	case EXPORT:
	case IMPORT:
	  return -24;
	case PARAMETER_RELOCATION:
	  return -8;
	default:
	  return 0;
	}
    }
  else
    return 0;
}

int
mixed_mode_frameless_function_invocation (frame)
     struct frame_info *frame;
{
  union hppa_unwind_table_entry *u;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;

  u = hppa_find_unwind_entry (frame->pc);

  if (u == 0)
    return 0;

  if (is_swizzled)
    return (    u->u32.Total_frame_size == 0
             && u->u32.stub_unwind.stub_type == 0);
  else
    return (u->u64.Total_frame_size == 0);
}


CORE_ADDR
hppa_saved_pc_after_call (struct frame_info *frame)
{
  int ret_regnum;
  CORE_ADDR pc, saved_pc = 0;
  union hppa_unwind_table_entry *u;

  if (is_swizzled)
    {
      /* JYG: PURIFY COMMENT:
         read_register () call below would destroy frame, so we have to
         record saved_pc for possible use later on. */
      saved_pc = mixed_mode_frame_saved_pc (frame);
    }

  ret_regnum = find_return_regnum (get_frame_pc (frame));
  if (is_swizzled)
    { 
      if (31 == ret_regnum)
        pc = (frame->pa_save_state_ptr->ss_narrow.ss_gr31) & ~0x3;
      else
        {
          assert (PA_RP_REGNUM == ret_regnum);
          pc = (frame->pa_save_state_ptr->ss_narrow.ss_rp) & ~0x3;
        }
    }
  else
    { 
      assert (PA_RP_REGNUM == ret_regnum);
      pc = (frame->pa_save_state_ptr->ss_wide.ss_64.ss_rp) & ~0x3;
    }

  /* If PC is in a linker stub, then we need to dig the address
     the stub will return to out of the stack.  */
  if (is_swizzled)
    {
      u = hppa_find_unwind_entry (pc);
      if (u && u->u32.stub_unwind.stub_type != 0)
        return saved_pc;
      else
        return pc;
    }
  else
    return pc;
}

CORE_ADDR
mixed_mode_frame_saved_pc (struct frame_info *frame)
{
  CORE_ADDR pc = get_frame_pc (frame);
  union hppa_unwind_table_entry *u;
  CORE_ADDR old_pc = 0;
  int spun_around_loop = 0;
  int rp_offset = 0;
  int saved_pc_offset;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;
  int target_ptr_bit = (is_swizzled ? 32: 64);
  int register_size = (is_swizzled ? 4: 8);

  if (mixed_mode_frameless_function_invocation (frame))
    {

      int ret_regnum;

      ret_regnum = find_return_regnum (pc);

      if (is_swizzled)
        { 
          if (31 == ret_regnum)
            pc = (frame->pa_save_state_ptr->ss_narrow.ss_gr31) & ~0x3;
              else
            {
              assert (PA_RP_REGNUM == ret_regnum);
              pc = (frame->pa_save_state_ptr->ss_narrow.ss_rp) & ~0x3;
            }
        }
      else
        { 
          assert (PA_RP_REGNUM == ret_regnum);
          pc = (frame->pa_save_state_ptr->ss_wide.ss_64.ss_rp) & ~0x3;
        }
    }
  else
    {
      spun_around_loop = 0;
      old_pc = pc;

    restart:
      rp_offset = rp_saved (pc);
      /* Unwinding from a frameless invocation. We have already read in prev frame`s RP
         from register, we should not read the same for this frame if Save_RP is set. 
         JAGag20972: Sometimes RP is actually saved in the prologue of the frameless 
         function, but with an offset of caller frame`s frame pointer. 
         We also get rp_offset to zero if the RP falls right in the beginning of the
         prologue even though the Save_RP is set for the caller to frameless invocation. 
         For hprof`s call-stack tail-spin issue.See rp_saved () for another fix.- mithun */
      if (    rp_offset == 0
           && frame->next && mixed_mode_frameless_function_invocation
                             (frame->next))
        {
           union hppa_unwind_table_entry *u;
           u = hppa_find_unwind_entry (pc);
           unsigned int Save_RP = is_swizzled? u->u32.Save_RP: u->u64.Save_RP;
           if (u && Save_RP)
             rp_offset = (target_ptr_bit == 64 ? -16 : -20);
        }  

      if (rp_offset == 0)
	{
	  old_pc = pc;
          pc = (is_swizzled) ?
            frame->pa_save_state_ptr->ss_narrow.ss_rp:
            frame->pa_save_state_ptr->ss_wide.ss_64.ss_rp;
          pc &= ~0x3;
	}
      else
	{
	  old_pc = pc;
	  pc = read_memory_integer (frame->frame + rp_offset,
				    target_ptr_bit / 8) & ~0x3;
	  if ((pc==0) && (java_debugging) )
	    {
	      /* rp in the frame marker is 0             */
	      /* Check if external rp contains a Java PC */
	      pc=read_memory_integer (frame->frame -24, 
		  target_ptr_bit / 8) & ~0x3;
	      if (is_java_frame(pc))
	        return pc;
	      pc=0;
	    }	
	}
    }

  /* If PC is inside a linker stub, then dig out the address the stub
     will return to. 

     Don't do this for long branch stubs.  Why?  For some unknown reason
     _start is marked as a long branch stub in hpux10.  */
  if (is_swizzled)
    {
      u = hppa_find_unwind_entry (pc);
      if (u && u->u32.stub_unwind.stub_type != 0
          && u->u32.stub_unwind.stub_type != LONG_BRANCH)
        {
          unsigned int insn;
    
          /* If this is a dynamic executable, and we're in a signal handler,
             then the call chain will eventually point us into the stub for
             _sigreturn.  Unlike most cases, we'll be pointed to the branch
             to the real sigreturn rather than the code after the real branch!. 
    
             Else, try to dig the address the stub will return to in the normal
             fashion.  */
          insn = read_memory_integer (pc, 4);
          if ((insn & 0xfc00e000) == 0xe8000000)
	    return (pc + extract_17 (insn) + 8) & ~0x3;
          else
	    {
	      if (old_pc == pc)
	        spun_around_loop++;

	      if (spun_around_loop > 1)
	        {
	          /* We're just about to go around the loop again with
	             no more hope of success.  Die. */
	          error ("Unable to find return pc for this frame");
	        }
	      else
	        goto restart;
	    }
        }
     }

  if (is_swizzled)
    {
      pc = mixed_mode_swizzle (pc);
    }
  return pc;
}

void
mixed_mode_get_saved_register (char *raw_buffer, int *optimized,
                               CORE_ADDR *addrp, struct frame_info *frame,
                               int regnum, enum lval_type *lval)
{
  CORE_ADDR addr = 0;

  if (!frame->pa_save_state_ptr)
    {
      error ("mixed_mode_get_saved_register(): Not a mixed mode frame.");
    }

  /* Normal systems don't optimize out things with register numbers.  */
  if (optimized != NULL)
    *optimized = 0;

  if (lval != NULL)
    *lval = not_lval;

  if (addrp != NULL)
    *addrp = addr;

  if (is_swizzled)
    {
      if (regnum == 0)
        {
          memcpy ((void *)raw_buffer,
                  (void *)&(frame->pa_save_state_ptr->ss_flags),
                  sizeof (int32_t));
          return;
        }
      if (regnum < PA_FP0_REGNUM)
        {
          int *pa_save_state_start_addr =
            &(frame->pa_save_state_ptr->ss_narrow.ss_gr1);
          pa_save_state_start_addr += (regnum - 1);
          memcpy ((void *)raw_buffer, 
                  (void *)pa_save_state_start_addr,
                  sizeof (int32_t));
        }
      else
        {
          float *pa_save_state_start_addr =
            (float *)(&(frame->pa_save_state_ptr->ss_fpblock));
          pa_save_state_start_addr += (regnum - PA_FP0_REGNUM);
          memcpy ((void *)raw_buffer,
                  (void *)pa_save_state_start_addr,
                   sizeof (double));
        }
    }
  else
    {
      if (regnum == 0)
        {
          memcpy ((void *)raw_buffer,
                  (void *)&(frame->pa_save_state_ptr->ss_flags),
                  sizeof (uint32_t));
          return;
        }
      if (regnum < PA_FP0_REGNUM)
        {
          int64_t *pa_save_state_start_addr =
            &(frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr1);
          pa_save_state_start_addr += (regnum - 1);
          memcpy ((void *)raw_buffer, 
                  (void *)pa_save_state_start_addr,
                  sizeof (int64_t));
        }
      else
        {
          double *pa_save_state_start_addr =
            (double *)(&(frame->pa_save_state_ptr->ss_fpblock));
          pa_save_state_start_addr += (regnum - PA_FP0_REGNUM);
          memcpy ((void *)raw_buffer,
                  (void *)pa_save_state_start_addr,
                   sizeof (double));
        }
    }
}


/* JAGaf70690 - GDB crashes when invoked via WDB-GUI - <BEGIN> */
CORE_ADDR saved_ap_value = 0;
/* JAGaf70690 - <END> */

void
mixed_mode_init_frame_ap (struct frame_info *frame)
{
  int ap_copy_regnum;
  struct frame_info *tmp_frame;
  union hppa_unwind_table_entry *u;

  /* Stacey 10/26/2001 - JAGad87549
     Disable this HP-specific function when running on gcc compiled
     code in order to avoid getting tons of the following errors:
     error "Unable to find AP register" */

  if (hppa_pc_in_gcc_function(frame->pc))
    return;

  if (!is_swizzled)
    {
      /* RM: The compilers copy AP over to r3, r4 or r5, depending on the
       * type of frame. We can determine the location of the dedicated AP
       * register for PA64 by examining two bits in the unwind
       * descriptor: large_frame_r3 and alloca_frame.
       *
       *      large_frame_r3    alloca_frame    AP location
       *     -----------------------------------------------
       *            0                 0             r3
       *            0                 1             r4
       *            1                 0             r4
       *            1                 1             r5
       */
      /* RM: ??? We need to handle all kinds of special frames here:
       * signal handlers, interrupts, etc, etc.
       */
    
      u = hppa_find_unwind_entry(frame->pc);
      /* RM: ??? What if !u? */
      if (u)
        {
          ap_copy_regnum =
            (u->u64.Large_frame && u->u64.alloca_frame) ?
              5 :
              ((u->u64.Large_frame || u->u64.alloca_frame) ? 4 : 3);
          
          tmp_frame = frame->next;
          if (!frame->next)
            { /* Innermost frame */
             
              CORE_ADDR prologue_begin, prologue_end, pc;
              find_pc_partial_function (frame->pc, 0, &prologue_begin, 0);
              prologue_end = mixed_mode_skip_prologue_hard_way (prologue_begin);
              /* If pc is in prologue code, get register 29 (AP) from the aries
                 save state, otherwise, get the register represented by
                 ap_copy_regnum from the aries save state.
               */
              if (frame->pc <= prologue_end)
                {
                  frame->ap = frame->pa_save_state_ptr->ss_wide.ss_64.ss_ret1;
                }
              else
	      { 
                if (5 == ap_copy_regnum)
                  {
                    frame->ap = frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr5;
                  }
                else if (4 == ap_copy_regnum)
                  {
                    frame->ap = frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr4;
                  }
                else
                  {
                    assert (3 == ap_copy_regnum);
                    frame->ap = frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3;
                  }
              /* JAGaf70690 - gdb aborts when invoked via wdb gui - <BEGIN> -
	         Gdb aborts in case of PA64 DOC, when GR3 gets trashed in assembly 
                 level. <frame->ap> is read from reg GR3. While <GR3> still has a
	         value, store it in <saved_ap_value>.*/ 	    
	        if (frame->ap)
	           saved_ap_value = frame->ap;
	        else
	           frame->ap = saved_ap_value;
	       } /* JAGaf70690 - <END> */   
            }
          else
            {
              /* Okay, I copied my AP to ap_copy_regnum before calling my
               * child. ap_copy_regnum is a callee save, so we should be able
               * to dig it out.
               */
	      /* Not valid for gcc frame. We need to check this to handle mixed frames on stack,
	         frames compiled with HP compiler and with gcc. */	
              if (hppa_pc_in_gcc_function (tmp_frame->pc))
	        return;
              while (tmp_frame)
                {
	          struct minimal_symbol * m;
    
                  u = hppa_find_unwind_entry (tmp_frame->pc);
             
                  /* RM: ??? What about if !u? */
                  if (u)
                    {
                      /* Entry_GR specifies the number of callee-saved general
                       * registers saved in the stack.  It starts at r3, so
                       * r3 would be 1.
                       */
                      if (u->u64.Entry_GR >= ap_copy_regnum - 2)
                        break;
                    }
                  tmp_frame = tmp_frame->next;
                }
	      /* Not valid gor gcc frame. */
              if (hppa_pc_in_gcc_function (tmp_frame->pc))
	        return;
    
              if (tmp_frame)
                {
                  CORE_ADDR prologue_begin, prologue_end, pc;
                  find_pc_partial_function (tmp_frame->pc, 0, &prologue_begin, 0);
                  prologue_end = mixed_mode_skip_prologue_hard_way (prologue_begin);
                  if (tmp_frame->pc <= prologue_end)
                    {
                      /* The pc is in the prologue code, the value in
                       * ap_copy_regnum was never saved into the
                       * stack (this ap_copy_regnum still holds the value of
                       * the previous argument pointer).
                       */
                      boolean found = false;
                      for (pc = prologue_begin;
                           !found && (pc <= prologue_end);
                           pc += 4) 
                      {
                        long inst = read_memory_integer (pc, 4);
                        /* Look for one of the following instructions :
                           std,ma r3,off(sp)
                           std,ma r4,off(sp)
                           std,ma r5,off(sp)
                        */
                        if (((inst & 0xfff0000f) == 0x73c00008) &&
                             (inst & 0x001f0000) >> 16 == ap_copy_regnum)
                          found = true;
                      }
    
                      if (found)
                        {
                          /* If the std,ma instruction hasn't been executed yet,
                             read ap from ap_copy_regnum, otherwise read ap
                             from the stack.
                          */
                          if (tmp_frame->pc < pc)
                            {
                              if (5 == ap_copy_regnum)
                                {
                                  frame->ap =
                                  frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr5;
                                }
                              else if (4 == ap_copy_regnum)
                                {
                                  frame->ap =
                                  frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr4;
                                }
                              else
                                {
                                  assert (3 == ap_copy_regnum);
                                  frame->ap =
                                  frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3;
                                }
                            }
                          else
                            frame->ap =
                              read_memory_integer (tmp_frame->frame +
                                                   (u ? 8*u->u64.Entry_FR : 0) +
                                                   (ap_copy_regnum - 3) * 8,
                                                   sizeof(CORE_ADDR));
                        }
                      else
                        error ("Unable to find AP register.");
                    }
                  else
                    {
                      /* We have walked down the chain to a function that saved
                       * ap_copy_regnum.  callee saves starting at r3 are
                       * saved immediately after the callee save FRs
                       */
                      frame->ap =
                        read_memory_integer (tmp_frame->frame +
                                             (u ? 8*u->u64.Entry_FR : 0) +
                                             (ap_copy_regnum - 3) * 8,
                                             sizeof(CORE_ADDR));
                    }
                }
              else
                {
                  /* The value in ap_copy_regnum was never saved into the
                   * stack (thus ap_copy_regnum still holds the value of
                   * the previous argument pointer).
                   */
                  if (5 == ap_copy_regnum)
                    {
                      frame->ap =
                      frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr5;
                    }
                  else if (4 == ap_copy_regnum)
                    {
                      frame->ap =
                      frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr4;
                    }
                  else
                    {
                      assert (3 == ap_copy_regnum);
                      frame->ap =
                      frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3;
                    }
                }
            }
        }
    }
}  
  


/* Given a GDB frame, determine the address of the calling function's frame.
   This will be used to create a new GDB frame struct, and then
   INIT_EXTRA_FRAME_INFO and INIT_FRAME_PC will be called for the new frame.

   This may involve searching through prologues for several functions
   at boundaries where GCC calls HP C code, or where code which has
   a frame pointer calls code without a frame pointer.  */

CORE_ADDR
mixed_mode_frame_chain (struct frame_info *frame)
{
  int my_framesize, caller_framesize;
  union hppa_unwind_table_entry *u = NULL, *u2 = NULL, 
    *callers_unwind_entry = NULL;
  CORE_ADDR frame_base;
  struct frame_info *tmp_frame;

  /* A frame in the current frame list, or zero.  */
  struct frame_info *saved_regs_frame = 0;
  /* Where the registers were saved in saved_regs_frame.
     If saved_regs_frame is zero, this is garbage.  */
  struct mixed_mode_frame_saved_regs saved_regs;

  CORE_ADDR caller_pc;
  CORE_ADDR pc;
  CORE_ADDR prologue_end;
  int frame_base_decrement;
  int pseudo_sp_register;

  struct minimal_symbol *min_frame_symbol;
  struct symbol *frame_symbol;
  char *frame_symbol_name = NULL;
  int target_ptr_bit = (is_swizzled ? 32: 64);

  /* If this is a threaded application, and we see the
     routine "__pthread_exit", treat it as the stack root
     for this thread. */

  /* srikanth, for MxN threads on HP-UX, the start routine
     is "__pthread_unbound_body" 
  */

  min_frame_symbol = lookup_minimal_symbol_by_pc (frame->pc);
  frame_symbol = find_pc_function (frame->pc);

  if ((min_frame_symbol != 0) /* && (frame_symbol == 0) */ )
    {
      /* The test above for "no user function name" would defend
         against the slim likelihood that a user might define a
         routine named "__pthread_exit" and then try to debug it.

         If it weren't commented out, and you tried to debug the
         pthread library itself, you'd get errors.

         So for today, we don't make that check. */
      frame_symbol_name = SYMBOL_NAME (min_frame_symbol);
      if (frame_symbol_name != 0)
	{
	  if (0 == strncmp (frame_symbol_name,
			    "__pthread_exit",
			    sizeof("__pthread_exit")))
	    {
	      /* Pretend we've reached the bottom of the stack. */
	      return (CORE_ADDR) 0;
	    }
#ifdef HP_MXN
          if (!strcmp (frame_symbol_name, MXN_THREAD_INITIAL_FRAME_SYMBOL))
            return 0;
#endif
	}
    }				/* End of hacky code for threads. */

  frame_base = frame->frame;

  /* Get frame sizes for the current frame and the frame of the 
     caller.  */
  my_framesize = mixed_mode_find_proc_framesize (frame->pc);
  caller_pc = mixed_mode_frame_saved_pc (frame);

  /* jini: Returning UINT64_MAX to signify that we should go back
     to IPF unwinding */
  if (!in_mixed_mode_pa_lib (caller_pc, NULL))
    {
      if (caller_pc != frame->non_aries_ia_frame->pc)
        {
          if (    inferior_aries_text_start <= caller_pc
              &&  caller_pc < inferior_aries_text_end)
            {
              char *name;
              /* Due to PA to PA BOR calls, the PA code has to return through
                 aries_pa2ia_process() in the libaries code. */
              find_pc_partial_function (caller_pc, &name,
                                        (CORE_ADDR *) NULL, (CORE_ADDR *) NULL);
              if (IS_ARIES_PA2IA_PROCESS (name))
                {
                  return UINT64_MAX - 1;
                }
              else
                {
                  error ("Obtained a non aries_pa2ia_process() aries caller\n"
                         "PC 0x%p while mixed-mode unwinding.");
                }
            }
          else
            {
              if (   is_swizzled
                  && pc_in_user_sendsig (frame->non_aries_ia_frame->pc)
                  && ((frame->non_aries_ia_frame->pc & 0xffffffff)
                       == caller_pc))
                {
                  /* For a 32 bit signal handler, aries returns the last 32
                     bits of its caller (the user_sendsig frame). If the 
                     non aries frame pc belongs to the user_sendsig frame
                     and the last 32 bits are the same as the caller pc, then
                     go back to IPF unwinding. */
                  return UINT64_MAX;
                }
              error ("The caller PC 0x%p obtained from aries does not match "
                     "the one gdb obtained from unwinding the IA stack.",
                      caller_pc); 
            }
        }
      return UINT64_MAX;
    }

  /* If caller_pc is a Java PC, since we don't know the caller Java frame's 
     size, return frame_base. Java functions don't have unwind entries */
  if ((java_debugging) && (is_java_frame(caller_pc)))
    return frame->frame;

  /* If we can't determine the caller's PC, then it's not likely we can
     really determine anything meaningful about its frame.  We'll consider
     this to be stack bottom. */
  if (caller_pc == (CORE_ADDR) 0)
    return (CORE_ADDR) 0;

  caller_framesize = mixed_mode_find_proc_framesize (mixed_mode_frame_saved_pc (frame));

  /* RM: set up some variables for alloca frames */
  u = hppa_find_unwind_entry (caller_pc);
  callers_unwind_entry = u;
  u2 = hppa_find_unwind_entry (frame->pc);

  /* JYG: PURIFY COMMENT: hppa_find_unwind_entry can return NULL. */
  if (!u || hppa_pc_in_gcc_function(caller_pc))
    {
      pseudo_sp_register = 3;
      frame_base_decrement = 0;
    }
  else
    {
      unsigned int Total_frame_size = is_swizzled? u->u32.Total_frame_size:
                                                   u->u64.Total_frame_size;
      unsigned int Large_frame = is_swizzled? u->u32.Large_frame:
                                              u->u64.Large_frame;
      pseudo_sp_register = Large_frame ? 4 : 3;
      frame_base_decrement = Total_frame_size << 3;
    }

  /* If caller does not have a frame pointer, then its frame
     can be found at current_frame - caller_framesize.  */
  if (caller_framesize != -1)
    {
      return frame_base - caller_framesize;
    }

  /* Both caller and callee have frame pointers and are GCC compiled
     (SAVE_SP bit in unwind descriptor is on for both functions.
     The previous frame pointer is found at the top of the current frame.  */
  if (caller_framesize == -1 && my_framesize == -1)
    {
      return read_memory_integer (frame_base +
	(u2 ? (is_swizzled? u2->u32.Entry_FR*8: u2->u64.Entry_FR*8) : 0) +
	(pseudo_sp_register-3) *
	(target_ptr_bit / 8),
        target_ptr_bit / 8) -
	frame_base_decrement;
    }
  /* Caller has a frame pointer, but callee does not.  This is a little
     more difficult as GCC and HP C lay out locals and callee register save
     areas very differently.

     The previous frame pointer could be in a register, or in one of 
     several areas on the stack.

     Walk from the current frame to the innermost frame examining 
     unwind descriptors to determine if %r3 ever gets saved into the
     stack.  If so return whatever value got saved into the stack.
     If it was never saved in the stack, then the value in %r3 is still
     valid, so use it. 

     We use information from unwind descriptors to determine if %r3
     is saved into the stack (Entry_GR field has this information).  */

  tmp_frame = frame;
  while (tmp_frame)
    {
      u = hppa_find_unwind_entry (tmp_frame->pc);
      if (!u) 
        return (CORE_ADDR) 0;

      /* Entry_GR specifies the number of callee-saved general registers
         saved in the stack.  It starts at %r3, so %r3 would be 1.  */
      if ( (is_swizzled? u->u32.Entry_GR: u->u64.Entry_GR)
                > pseudo_sp_register - 3
          || ((is_swizzled? u->u32.Save_SP: u->u64.Save_SP)
              && hppa_pc_in_gcc_function(tmp_frame->pc)))
	{
          char buf[4];
          unsigned long inst, status;

          /* RM: If we are in the prologue, make sure the register
             has been saved */
          find_pc_partial_function (tmp_frame->pc, 0, &pc, 0);
          prologue_end = mixed_mode_skip_prologue_hard_way(pc);
          for (pc = tmp_frame->pc; pc < prologue_end; pc++)
            {
              status = target_read_memory (pc, buf, 4);
              inst = extract_unsigned_integer (buf, 4);
              if (inst_saves_gr(inst) == pseudo_sp_register)
                {
                  tmp_frame = tmp_frame->next;
                  continue;
                }
            }
          break;  
        }
      else
	tmp_frame = tmp_frame->next;
    }

  if (tmp_frame)
    {
      /* We may have walked down the chain into a function with a frame
         pointer.  */
      if ((      (is_swizzled? u->u32.Save_SP: u->u64.Save_SP)
              && hppa_pc_in_gcc_function(tmp_frame->pc)))
	{
	  return read_memory_integer (tmp_frame->frame, TARGET_PTR_BIT / 8);
	}
      /* %r3 was saved somewhere in the stack.  Dig it out.  */
      else
	{
	  /* Sick.

	     For optimization purposes many kernels don't have the
	     callee saved registers into the save_state structure upon
	     entry into the kernel for a syscall; the optimization
	     is usually turned off if the process is being traced so
	     that the debugger can get full register state for the
	     process.

	     This scheme works well except for two cases:

	     * Attaching to a process when the process is in the
	     kernel performing a system call (debugger can't get
	     full register state for the inferior process since
	     the process wasn't being traced when it entered the
	     system call).

	     * Register state is not complete if the system call
	     causes the process to core dump.

	     The following heinous code is an attempt to deal with
	     the lack of register state in a core dump.  

	     Stacey
	     It checks to see if we have core dumped in a system call.  
	     We can tell this by looking at the flags register.  As
	     indicated above the register state is sometimes not complete
	     when this has happened, so if the flags register doesn't
	     appear to be set then read it again just in case. */

	  /* Abominable hack.  */

	  mixed_mode_get_frame_saved_regs (tmp_frame, &saved_regs);
          if (!tmp_frame->pa_save_state_ptr)
            error ("pa_save_state_ptr for tmp_frame not found. Pls turn"
                   "debug-aries on and reload the corefile.");

	  if (current_target.to_has_execution == 0 && 
	      ( (saved_regs.regs[PA_FLAGS_REGNUM] && 
		 (IN_SYSCALL (read_memory_integer (saved_regs.regs[PA_FLAGS_REGNUM],
						   target_ptr_bit / 8))))	 
		|| (saved_regs.regs[PA_FLAGS_REGNUM] == 0
		    && IN_SYSCALL((CORE_ADDR)
                                   tmp_frame->pa_save_state_ptr->ss_flags))))
	    {
	      u = hppa_find_unwind_entry (mixed_mode_frame_saved_pc (frame));
	      
	      /* If we get here, then we know we have core dumped in a system
		 call.  The following code is in place to handle gcc functions
		 which aren't guaranteed to have an unwind entry and HP C 
		 calls to alloca frames.  In these special cases, we can find
		 the beginning of the frame by using the psuedo stack
		 pointer register. 
		 
		 In all other cases we can simply subtract the frame size
		 from what we believe to be the end of the frame.  */
	      
	      if ( (!u) ||
		   (!is_swizzled && callers_unwind_entry->u64.alloca_frame) ||
		   (is_swizzled && callers_unwind_entry->u32.Pseudo_SP_Set))
		{
		  return read_memory_integer (saved_regs.regs[pseudo_sp_register],
					      target_ptr_bit / 8) -
		    frame_base_decrement;
		}
	      else
		{
		  return frame_base -
                        ((is_swizzled? u->u32.Total_frame_size:
                                       u->u64.Total_frame_size) << 3);
		}
	    }

	  return read_memory_integer (saved_regs.regs[pseudo_sp_register],
				      target_ptr_bit / 8) -
	    frame_base_decrement;
	}
    }
  else
    {
      /* Get the innermost frame.  */
      tmp_frame = frame;
      while (tmp_frame->next != NULL)
	tmp_frame = tmp_frame->next;

      mixed_mode_get_frame_saved_regs (tmp_frame, &saved_regs);

      /* Abominable hack.  See above.  */
      if (current_target.to_has_execution == 0
	  && ((saved_regs.regs[PA_FLAGS_REGNUM]
               && (IN_SYSCALL(read_memory_integer (saved_regs.regs[PA_FLAGS_REGNUM],
						   target_ptr_bit / 8))))
	      || (saved_regs.regs[PA_FLAGS_REGNUM] == 0
		  && IN_SYSCALL((CORE_ADDR)
                                 (tmp_frame->pa_save_state_ptr->ss_flags)))))
	{
	  u = hppa_find_unwind_entry (mixed_mode_frame_saved_pc (frame));
	  if (!u)
	    {
	      return read_memory_integer (saved_regs.regs[pseudo_sp_register],
					  target_ptr_bit / 8) -
		frame_base_decrement;
	    }
	  else
	    {
	      return frame_base - ((is_swizzled? u->u32.Total_frame_size:
                                                 u->u64.Total_frame_size) << 3);
	    }
	}

      /* The value in %r3 was never saved into the stack (thus %r3 still
         holds the value of the previous frame pointer).  */
      CORE_ADDR pseudo_sp_register_value = NULL;
      if (pseudo_sp_register == 4)
        pseudo_sp_register_value = (is_swizzled)?
                                    frame->pa_save_state_ptr->ss_narrow.ss_gr4:
                                    frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr4;
      else
        {
          assert (3 == pseudo_sp_register);
          pseudo_sp_register_value = (is_swizzled)?
                                    frame->pa_save_state_ptr->ss_narrow.ss_gr3:
                                    frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3;
        }
      return pseudo_sp_register_value - frame_base_decrement;
      
    }
}


/* To see if a frame chain is valid, see if the caller looks like it
   was compiled with gcc. */

int
hppa_frame_chain_valid (chain, thisframe)
     CORE_ADDR chain;
     struct frame_info *thisframe;
{
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_us;
  struct minimal_symbol *msym_start;
  struct minimal_symbol *msym_call_dummy;
  struct minimal_symbol *msym_sr4export;
  union hppa_unwind_table_entry *u, *next_u = NULL;
  struct frame_info *next;

  if (!chain)
    return 0;

  u = hppa_find_unwind_entry (thisframe->pc);

  if (u == NULL)
    return 1;

  /* We can't just check that the same of msym_us is "_start", because
     someone idiotically decided that they were going to make a Ltext_end
     symbol with the same address.  This Ltext_end symbol is totally
     indistinguishable (as nearly as I can tell) from the symbol for a function
     which is (legitimately, since it is in the user's namespace)
     named Ltext_end, so we can't just ignore it.  */
  msym_us = lookup_minimal_symbol_by_pc (mixed_mode_frame_saved_pc (thisframe));
  msym_start = lookup_minimal_symbol ("_start", NULL, NULL);
  if (msym_us
      && msym_start
      && SYMBOL_VALUE_ADDRESS (msym_us) == SYMBOL_VALUE_ADDRESS (msym_start))
    /* This additional check for inside_main_func is to allow fortran programs
     * where _start and the main program have the same address.  This check 
     * will return 0 for C and C++ programs where _start is defined in libc.
     */
    return inside_main_func (mixed_mode_frame_saved_pc (thisframe));

  if (!is_swizzled)
    {
      /* Return 0 we reach "main" followed by a millicode routine. */  
      if ((strcmp(SYMBOL_NAME(lookup_minimal_symbol_by_pc (thisframe->pc)),
          "main") == 0) &&
          (strncmp(SYMBOL_NAME(msym_us), "$$", 2) == 0))
          return 0;
    }

  /* Grrrr.  Some new idiot decided that they don't want _start for the
     PRO configurations; $START$ calls main directly....  Deal with it.  */
  msym_start = lookup_minimal_symbol ("$START$", NULL, NULL);
  if (msym_us
      && msym_start
      && SYMBOL_VALUE_ADDRESS (msym_us) == SYMBOL_VALUE_ADDRESS (msym_start))
    return 0;

  next = get_next_frame (thisframe);
  if (next)
    next_u = hppa_find_unwind_entry (next->pc);

  /* If this frame does not save SP, has no stack, isn't a stub,
     and doesn't "call" an interrupt routine or signal handler caller,
     then its not valid.  */
  unsigned int Save_SP = (is_swizzled)? u->u32.Save_SP: u->u64.Save_SP;
  unsigned int Total_frame_size = (is_swizzled)? u->u32.Total_frame_size: u->u64.Total_frame_size;
  unsigned int next_HP_UX_interrupt_marker = NULL;
  if (next_u)
    next_HP_UX_interrupt_marker = (is_swizzled)?
               next_u->u32.HP_UX_interrupt_marker: next_u->u64.HP_UX_interrupt_marker;

  if (Save_SP || Total_frame_size
      || (is_swizzled && u->u32.stub_unwind.stub_type != 0)
      || (thisframe->next && thisframe->next->signal_handler_caller)
      || (next_HP_UX_interrupt_marker))
    return 1;

  if (pc_in_linker_stub (thisframe->pc))
    return 1;

  return 0;
}


/*
 * mixed_mode_pa_is_64bit()
 *   Answer whether the architecture is 64 bit or not
 */
int
mixed_mode_pa_is_64bit (pa_save_state_t *ssp)
{
  static int know_which = 0;  /* False */

  if( !know_which ) {
     is_pa_2 = ssp->ss_flags & SS_WIDEREGS;
     know_which = 1;  /* True */
  }

  return is_pa_2;
}

/* Register alias names used to get the register values. */
static const struct aliasnames pa32_query_reg_aliases[] = {
        /* Thread pointer           */   { "mpsfu_high", { "mpsfu_hi", "cr27",
                                                           "tp", "tr3", "TP"  }
},
        /* Return pointer           */   { "rp", { "r2" } },
        /* millicode return pointer */   { "r31", { "mrp" } },
        /* Return value             */   { "ret0", { "r28" } },
        /* Return value-High part of
           double , Static link,
           argument pointer         */   { "ret1", { "r29", "sl", "ap" } },
        /* Stack pointer            */   { "sp", { "r30" } },
        /* Data/global pointer      */   { "dp", { "r27", "gp" } },
        /* argument                 */   { "r26", { "arg0" } },
        /* argument or high part
           of double                */   { "r25", { "arg1" } },
        /* argument                 */   { "r24", { "arg2" } },
        /* argument or high part
           of double                */   { "r23", { "arg3" } },
        /* Recovery counter reg     */   { "cr0", { "rctr" } },
        /* Protection ID 1          */   { "cr8", { "pidr1" } },
        /* Protection ID 2          */   { "cr9", { "pidr2" } },
        /* Coprocessor config reg   */   { "ccr", { "cr10" } },
        /* Shift amount reg         */   { "sar", { "cr11" } },
        /* Protection ID 3          */   { "cr12", { "pidr3" } },
        /* Protection ID 4          */   { "cr13", { "pidr4" } },
        /* External interrupt enable
           mask                     */   { "eiem", { "cr15" } },
        /* Interrupt instruncton reg*/   { "iir", { "cr19" } },
        /* Interrupt space reg      */   { "isr", { "cr20" } },
        /* Interrupt offset reg     */   { "ior", { "cr21" } },
        /* Interruption processor
           status word              */   { "ipsw", { "cr22" } }
                                       };

/* Register alias names used to get the register values. */
static const struct aliasnames pa64_query_reg_aliases[] = {
        /* Thread pointer           */   { "mpsfu_high", { "mpsfu_hi", "cr27",
                                                           "tp", "tr3", "TP"  }
},
        /* Return pointer           */   { "rp", { "r2" } },
        /* millicode return pointer */   { "r31", { "mrp" } },
        /* Return value             */   { "ret0", { "r28" } },
        /* Return value-High part of
           double , Static link,
           argument pointer         */   { "ret1", { "r29", "sl", "ap" } },
        /* Stack pointer            */   { "sp", { "r30" } },
        /* Data/global pointer      */   { "dp", { "r27", "gp" } },
        /* argument                 */   { "r26", { "arg0" } },
        /* argument or high part
           of double                */   { "r25", { "arg1" } },
        /* argument                 */   { "r24", { "arg2" } },
        /* argument or high part
           of double                */   { "r23", { "arg3" } },
        /* register argument        */   { "r22", { "arg4" } },
        /* register argument        */   { "r21", { "arg5" } },
        /* register argument        */   { "r20", { "arg6" } },
        /* register argument        */   { "r19", { "arg7" } },
        /* Recovery counter reg     */   { "cr0", { "rctr" } },
        /* Protection ID 1          */   { "cr8", { "pidr1" } },
        /* Protection ID 2          */   { "cr9", { "pidr2" } },
        /* Coprocessor config reg   */   { "ccr", { "cr10" } },
        /* Shift amount reg         */   { "sar", { "cr11" } },
        /* Protection ID 3          */   { "cr12", { "pidr3" } },
        /* Protection ID 4          */   { "cr13", { "pidr4" } },
        /* External interrupt enable
           mask                     */   { "eiem", { "cr15" } },
        /* Interrupt instruncton reg*/   { "iir", { "cr19" } },
        /* Interrupt space reg      */   { "isr", { "cr20" } },
        /* Interrupt offset reg     */   { "ior", { "cr21" } },
        /* Interruption processor
           status word              */   { "ipsw", { "cr22" } }
                                       };

/* The mixed mode version of target_map_name_to_register. */

int
mixed_mode_map_name_to_register (char *str, int len)
{
  int i;
  int no_of_groupaliases, each_group, each_alias, regnum;
  int num_regs = is_swizzled ? PA32_NUM_REGS: PA64_NUM_REGS;
  const struct aliasnames *pa_register_aliases = is_swizzled ?
                                                 pa32_query_reg_aliases:
                                                 pa64_query_reg_aliases;

  /* Search architectural register name space. */
  for (i = 0; i < num_regs; i++)
    if (mixed_mode_legacy_register_name (i)
        && len == strlen (mixed_mode_legacy_register_name (i))
	&& STREQN (str, mixed_mode_legacy_register_name (i), len))
      {
	return i; 
      }

  /* Try standard aliases */
  if (NULL == pa_std_regs)
    {
      mixed_mode_create_pa_std_regs ();
    }

  for (i = 0; i < num_pa_std_regs; i++)
    if (pa_std_regs[i].name && len == strlen (pa_std_regs[i].name)
	&& STREQN (str, pa_std_regs[i].name, len))
      {
	return pa_std_regs[i].regnum;
      }

  /* pes,JAGad28813 Check whether the register name is an alias .If so, return
   * the corresponding register number.
   * e.g., cr27,mpsfu_hi and tp are the alias for mpsfu_high
   */ 
  if (is_swizzled)
    {
      no_of_groupaliases =
        sizeof( pa32_query_reg_aliases ) / sizeof (pa32_query_reg_aliases[0] );
    }
  else
    {
      no_of_groupaliases =
        sizeof( pa64_query_reg_aliases ) / sizeof (pa64_query_reg_aliases[0] );
    }

  /* For each group alias*/
  for (  each_group = 0 ; each_group < no_of_groupaliases ; each_group++ ) {
      /* For each aliases in a group */
      for (  each_alias = 0; each_alias < FIVE ; each_alias++ ) { 
          /* If valiad alias name */
          if ((pa_register_aliases[each_group].alias_names[each_alias] ?
                       STREQN ( str, 
                        pa_register_aliases[each_group].alias_names[each_alias], 
                        len ) : NULL) ) {
              const char* found_alias = pa_register_aliases[each_group].alias_to;
              len = (int) strlen( found_alias );
	      /* Now search architectural register namespace */
              for (  regnum = 0; regnum < num_regs; regnum++ ) {
                  if ( mixed_mode_legacy_register_name (regnum) 
                       && len == strlen
                           ( mixed_mode_legacy_register_name ( regnum ) )
	               && STREQN( found_alias,
                                  mixed_mode_legacy_register_name ( regnum ),
                                  len)){  
	                   return regnum;
                  } /* If we found the register number in register namespace */
              } /* End of searching architectural register namespace */
          } /* End of If valid alias name */
      } /* End of For each aliases in a group */
  }  /* End of For each group alias */	

  return -1;
}

/* Print the register regnum, or all registers if regnum is -1 */

void
mixed_mode_do_registers_info (int regnum, int fpregs)
{
  int32_t *raw_regs32 = NULL;
  int64_t *raw_regs64 = NULL;
  int32_t *pa_save_state_start_addr32 = NULL;
  int64_t *pa_save_state_start_addr64 = NULL;
  int i;
  int fp4_regnum = is_swizzled? PA32_FP4_REGNUM: PA64_FP4_REGNUM;

  if (!selected_frame->pa_save_state_ptr)
    {
      error ("mixed_mode_do_registers_info() : Not a mixed mode frame.");
    }

  is_pa_2 = mixed_mode_pa_is_64bit (selected_frame->pa_save_state_ptr);

  if (is_swizzled)
    {
      raw_regs32 = (int32_t *) alloca (PA32_NUM_REGS * sizeof (int32_t) );
      
      raw_regs32[0] = selected_frame->pa_save_state_ptr->ss_flags;
      pa_save_state_start_addr32 =
        &(selected_frame->pa_save_state_ptr->ss_narrow.ss_gr1);
      /* Copy the ss_narrow registers. */
      for (i = 1; i < 64; i++)
        {
          raw_regs32 [i] = *pa_save_state_start_addr32++;
        }
      memcpy ((void *) &raw_regs32[64],
              (void *)&(selected_frame->pa_save_state_ptr->ss_fpblock),
               256);
    }
  else
    {
      raw_regs64 = (int64_t *) alloca (PA64_NUM_REGS * sizeof (int64_t) );
      
      raw_regs64[0] = selected_frame->pa_save_state_ptr->ss_flags;
      pa_save_state_start_addr64 =
        &(selected_frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr1);
      /* Copy the ss_wide registers. */
      for (i = 1; i < 64; i++)
        {
          raw_regs64 [i] = *pa_save_state_start_addr64++;
        }
      memcpy ((void *) &raw_regs64[64],
              (void *)&(selected_frame->pa_save_state_ptr->ss_fpblock),
               256);
    }

  if (regnum == -1)
    {
      if (is_swizzled)
        {
          mixed_mode_print_registers ((void *)raw_regs32, regnum, fpregs);
        }
      else
        {
          mixed_mode_print_registers ((void *)raw_regs64, regnum, fpregs);
        }
    }
  else if (regnum < fp4_regnum)
    {
      int32_t reg_val[2];
      reg_val [0] = reg_val [1] = 0;

      /* Fancy % formats to prevent leading zeros. */
      if (is_swizzled)
        {
          printf_unfiltered ("%s %x\n",
                             mixed_mode_legacy_register_name (regnum),
                             *(int32_t *) (raw_regs32 + regnum));
        }
      else
        {
          reg_val [0] = *(int32_t *)((int32_t *)raw_regs64 + regnum * 2);
          reg_val [1] = *(int32_t *)((int32_t *)raw_regs64 + regnum * 2 + 1);

          if (reg_val[0] == 0)
            printf_unfiltered ("%s %x\n",
              mixed_mode_legacy_register_name (regnum), reg_val[1]);
          else
	    printf_unfiltered ("%s %x%8.8x\n",
               mixed_mode_legacy_register_name (regnum),
			       reg_val[0], reg_val[1]);

	}
    }
  else
    {
      if (!is_swizzled)
        {
          mixed_mode_print_fp_reg (regnum, unspecified_precision, raw_regs64);
          printf_filtered("\n");
        }
      else
        {
          mixed_mode_print_fp_reg (regnum, single_precision, raw_regs32);
          printf_filtered("\n");
          mixed_mode_print_fp_reg (regnum, double_precision, raw_regs32);
          printf_filtered("\n");
        }
    }
}


struct aliasnames1 {
     char *alias_to;
     char *alias_names[1];
};

/* Register alias names used to print the register names. */
const struct aliasnames1 pa32_register_aliases[] = {
        /* Thread pointer           */   { "mpsfu_high", { "mpsfu_hi/tp" } },
 	/* Return pointer           */	 { "rp", { "rp/r2" } },
	/* millicode return pointer */	 { "r31", { "mrp/r31" } },
	/* Return value	 	    */	 { "ret0", { "ret0/r28" } },
	/* Return value-High part of 
	   double , Static link,
	   argument pointer	    */	 { "ret1", { "ret1/ap/r29" } },
	/* Stack pointer	    */   { "sp", { "sp/r30" } },
	/* Data/global pointer 	    */   { "dp", { "dp/gp/r27" } },
	/* argument		    */   { "r26", { "arg0/r26" } },
	/* argument or high part
	   of double    	    */   { "r25", { "arg1/r25" } },
	/* argument		    */   { "r24", { "arg2/r24" } },
        /* argument or high part
	   of double		    */   { "r23", { "arg3/r23" } },
  	/* Recovery counter reg     */   { "cr0", { "rctr/cr0" } },
	/* Protection ID 1 	    */   { "cr8", { "pidr1/cr8" } },
	/* Protection ID 2 	    */   { "cr9", { "pidr2/cr9" } },
	/* Coprocessor config reg   */   { "ccr", { "ccr/cr10" } },
	/* Shift amount reg	    */   { "sar", { "sar/cr11" } },
	/* Protection ID 3	    */   { "cr12", { "pidr3/cr12" } },
	/* Protection ID 4	    */   { "cr13", { "pidr4/cr13" } },
        /* External interrupt enable
	   mask			    */   { "eiem", { "eiem/cr15" } },
	/* Interrupt instruncton reg*/   { "iir", { "iir/cr19" } },
	/* Interrupt space reg	    */   { "isr", { "isr/cr20" } },
	/* Interrupt offset reg     */   { "ior", { "ior/cr21" } },
        /* Interruption processor
	   status word		    */   { "ipsw", { "ipsw/cr22" } }

};

const struct aliasnames1 pa64_register_aliases[] = {
        /* Thread pointer           */   { "mpsfu_high", { "mpsfu_hi/tp" } },
 	/* Return pointer           */	 { "rp", { "rp/r2" } },
	/* millicode return pointer */	 { "r31", { "mrp/r31" } },
	/* Return value	 	    */	 { "ret0", { "ret0/r28" } },
	/* Return value-High part of 
	   double , Static link,
	   argument pointer	    */	 { "ret1", { "ret1/ap/r29" } },
	/* Stack pointer	    */   { "sp", { "sp/r30" } },
	/* Data/global pointer 	    */   { "dp", { "dp/gp/r27" } },
	/* argument		    */   { "r26", { "arg0/r26" } },
	/* argument or high part
	   of double    	    */   { "r25", { "arg1/r25" } },
	/* argument		    */   { "r24", { "arg2/r24" } },
        /* argument or high part
	   of double		    */   { "r23", { "arg3/r23" } },
	/* register argument        */   { "r22", { "arg4/r22" } },
	/* register argument        */   { "r21", { "arg5/r21" } },
	/* register argument        */   { "r20", { "arg6/r20" } },
	/* register argument        */   { "r19", { "arg7/r19" } },
  	/* Recovery counter reg     */   { "cr0", { "rctr/cr0" } },
	/* Protection ID 1 	    */   { "cr8", { "pidr1/cr8" } },
	/* Protection ID 2 	    */   { "cr9", { "pidr2/cr9" } },
	/* Coprocessor config reg   */   { "ccr", { "ccr/cr10" } },
	/* Shift amount reg	    */   { "sar", { "sar/cr11" } },
	/* Protection ID 3	    */   { "cr12", { "pidr3/cr12" } },
	/* Protection ID 4	    */   { "cr13", { "pidr4/cr13" } },
        /* External interrupt enable
	   mask			    */   { "eiem", { "eiem/cr15" } },
	/* Interrupt instruncton reg*/   { "iir", { "iir/cr19" } },
	/* Interrupt space reg	    */   { "isr", { "isr/cr20" } },
	/* Interrupt offset reg     */   { "ior", { "ior/cr21" } },
        /* Interruption processor
	   status word		    */   { "ipsw", { "ipsw/cr22" } }

};
int get_alias (char *reg_name, char **alias1)
{
  int each_group; 
  int pa_regs = is_swizzled ? 22: 26;
  const struct aliasnames1 *pa_register_aliases = is_swizzled ?
                                                 pa32_register_aliases:
                                                 pa64_register_aliases;
  for (  each_group = 0 ; each_group < pa_regs ; each_group++ ) {
      /* For each aliases in a group */
          if ( STREQN ( reg_name, 
			pa_register_aliases[each_group].alias_to,
                        strlen(reg_name) ) && 
			( strlen(reg_name) == strlen(pa_register_aliases[each_group].alias_to) ) ) {
  		*alias1 = xmalloc (sizeof(char) * (strlen(pa_register_aliases[each_group].alias_names[0]) + 1));
		sprintf(*alias1, "%s", pa_register_aliases[each_group].alias_names[0]);
		return 1;
	}
  }
  return 0;
}
/* "Info all-reg" command */

static void
mixed_mode_print_registers (void *raw_regs, int regnum, int fpregs)
{
  int i, j;
  uint32_t raw_val[2];
  uint32_t long_val;
  enum precision_type precision = unspecified_precision;
  char *reg_name = NULL;
  uint32_t *raw_regs32 = NULL;
  uint64_t *raw_regs64 = NULL;

  if (is_swizzled)
    {
      raw_regs32 = (uint32_t *) raw_regs;
    }
  else
    {
      raw_regs64 = (uint64_t *) raw_regs;
    }

  /* RM: On PA32, each (double precision) floating point register is
   * treated like two (single precision) registers. On PA64, a
   * floating point register really is a floating point register.
   * So on PA32 we report 8 32-bit registers fpe0..fpe7 and on PA64
   * we report 4 64-bit registers fpe0..fpe3.
   */


  /*******************************************************
   *   JAGab15917, JAGad06198, and JAGab16990
   *
   *   Stacey: 6/29/01
   *
   *   The code below has been modified to allow gdb's
   *   info reg command to check to see if the user has
   *   done a set width command and display the registers
   *   in rows accordingly.  This is to avoid printing 4 
   *   registers per row on a 40 character wide terminal.
   *
   *   It takes 30 character spaces to output the info for
   *   for one register.  If the width is 0 or some other 
   *   small number, gdb looks to the user's shell to get 
   *   the user's column width from the appropriate 
   *   environment variable.  
   *
   *   9/21/01
   *   
   *   Since this is only an issue for terminal inferaces,
   *   and this new formatting would actually be a very 
   *   difficult formatting for the GUI to parse, there are
   *   2 sections of code below.  The first is for terminal
   *   interfaces like gdb in line mode, VDB (nimbus) and 
   *   the TUI.  
   *   
   *   Since most GUI's operate at a higher annotation level
   *   than the terminal user interface (because they
   *   require extra printed annotation information from gdb) 
   *   they should will use the second section of code as
   *   and will not be effected by the new formatting below.  
   *
   *   LIMITATION: 
   *   This code assumes that the user will remember to
   *   do a set width command each time he re-sizes his
   *   terminal window. If the user forgets to do a set 
   *   width command, this won't work.  
   *******************************************************/

  if (annotation_level == 0)
    {
      int regs = (is_swizzled)? (18 * 4): (17 * 4);

      extern unsigned int get_width ();
      int registers_per_row = (int) ( (int) get_width ()/30);

      if (registers_per_row < 1)
	{
	  registers_per_row = (int) (atoi (getenv ("COLUMNS"))/30);
	  if (registers_per_row < 1)
	    {
	      warning ("Your terminal display isn't wide enough to properly "
		       "display registers so the following output may be "
		       "poorly formatted or difficult to read.\n\n"
		       "To fix this, resize your terminal window and "
		       "execute the gdb 'set width' command to set the "
		       "window width to at least 30 characters\n");
	      registers_per_row=1;
	    }
	}
      
      for (i = 0; i < regs; i++)
	{
	  /* Even fancier % formats to prevent leading zeros
	   * and still maintain the output in columns. */
          if (is_swizzled)
            {
              printf_filtered ("%11.11s:         %8x  ",
		  get_alias(mixed_mode_legacy_register_name (i), &reg_name) ?
                        reg_name : mixed_mode_legacy_register_name (i),
                                  *(uint32_t *) (raw_regs32 + i));
            }
          else
            {
              raw_val [0] = *(uint32_t *)((uint32_t *)raw_regs64 + i * 2);
              raw_val [1] = *(uint32_t *)((uint32_t *)raw_regs64 + i * 2 + 1);

	      /* raw_val = extract_signed_integer (&raw_val, 8); */
              if (raw_val[0] == 0)
                printf_filtered ("%11.11s:         %8x  ",
		    get_alias (mixed_mode_legacy_register_name (i),
                               &reg_name) ? reg_name :
                               mixed_mode_legacy_register_name (i),
		               raw_val[1]);
	      else
		printf_filtered ("%11.11s: %8x%8.8x  ",
		    get_alias (mixed_mode_legacy_register_name (i),
                               &reg_name) ? reg_name :
                               mixed_mode_legacy_register_name (i),
			       raw_val[0], raw_val[1]);
	    }
	  if ( (i % registers_per_row) == 0)
	    printf_unfiltered ("\n");
	}
  
      /* just in case there aren't enough registers left to complete
	 the row */
      printf_unfiltered ("\n");
    }
  else
    {

      /******************************************************
       *   Since most GUI's operate at a higher annotation level
       *   than the terminal user interface (because they
       *   require extra printed annotation information from gdb) 
       *   they should will use the second section of code as
       *   and will not be effected by the new formatting below.  */

      int regs = (is_swizzled) ? 18: 17;
      for (i = 0; i < regs; i++)
	{
	  for (j = 0; j < 4; j++)
	    {
	      if (is_swizzled)
		{
		  /* Being big-endian, on this machine the low bits
		   * (the ones we want to look at) are in the second longword.
		   */
		  printf_filtered ("%11.11s: %10x  ",
				   mixed_mode_legacy_register_name (i * 4 + j),
                                   *(uint32_t *)(raw_regs32 + (i * 4 + j)));
		}
	      else
		{
                  int k = i * 4 + j;
                  raw_val [0] = *(uint32_t *)((uint32_t *)raw_regs64 + k * 2);
                  raw_val [1] = *(uint32_t *)((uint32_t *)raw_regs64 + k * 2 +
                                                                           1);
		  if (raw_val[0] == 0)
		    printf_filtered ("%11.11s:         %8x  ",
		     get_alias (mixed_mode_legacy_register_name (i * 4 + j),
                                &reg_name) ? reg_name :
                                mixed_mode_legacy_register_name (i * 4 + j),
				raw_val[1]);
		  else
		    printf_filtered ("%11.11s: %8x%8.8x  ",
		     get_alias (mixed_mode_legacy_register_name (i * 4 + j),
                                &reg_name) ? reg_name :
                                mixed_mode_legacy_register_name (i * 4 + j),
				raw_val[0], raw_val[1]);
		}
	    }
	  printf_unfiltered ("\n");
	}
    }
  int fp4_regnum = is_swizzled? PA32_FP4_REGNUM: PA64_FP4_REGNUM;
  int pa_num_regs = is_swizzled? PA32_NUM_REGS: PA64_NUM_REGS;

  if (fpregs)
    /* Note that real floating point values only start at
     * FP4_REGNUM.  FP0 and up are just status and error
     * registers, which have integral (bit) values.
     */
    for (i = fp4_regnum; i < pa_num_regs; i++)
      {
        if (!is_swizzled)
          {
            mixed_mode_print_fp_reg (i, unspecified_precision, raw_regs64);
            printf_filtered ("\n");
          }
        else
          {
            mixed_mode_print_fp_reg (i, single_precision, raw_regs32);
            printf_filtered ("\n");
            /* if regnum is even, then this register can also be a
                double-precision FP register */
            if ((i % 2) == 0)
              {
                mixed_mode_print_fp_reg (i, double_precision, raw_regs32);
                printf_filtered ("\n");
              }
          }
      }
}

static void
mixed_mode_print_fp_reg (int i, enum precision_type precision, void *raw_regs)
{
  char virtual_buffer [8];

  char *raw_buffer = NULL;
  int  max_register_raw_size = (is_swizzled)? 4: 8;
  int register_raw_size = (is_swizzled)? 4: 8;
  void *raw_regs_src = (is_swizzled) ? (void *)((uint32_t *) raw_regs + i):
                                       (void *)((uint64_t *) raw_regs + i);

  /* Put it in the buffer.  No conversions are ever necessary.  */
  memcpy (virtual_buffer, raw_regs_src, register_raw_size);

  if ((!is_swizzled && (precision == double_precision) ||
                      (precision == unspecified_precision))
      || (is_swizzled && (precision == double_precision && (i % 2) == 0)))
    {
      /* If the user wants double precision and if the register number is
         even, format it that way.
      */
      char *raw_buffer = (char *) alloca (max_register_raw_size);
 
      if (is_swizzled)
        {
          memcpy (virtual_buffer + register_raw_size,
                  (void *)((uint32_t *) raw_regs + i + 1), register_raw_size);
        }
      fputs_filtered (mixed_mode_legacy_register_name (i), gdb_stdout);
      print_spaces_filtered (8 - strlen (mixed_mode_legacy_register_name (i)), gdb_stdout);
      fputs_filtered ("(double precision)     ", gdb_stdout);
      val_print (builtin_type_double, (char *)virtual_buffer, 0, 0,
                 gdb_stdout, 0, 1, 0, Val_pretty_default);
      printf_filtered ("\n");

      if (is_swizzled && precision == unspecified_precision)
        {
          fprintf_filtered (gdb_stdout, "\n%sL", mixed_mode_legacy_register_name (i));
          print_spaces_filtered (7 - strlen (mixed_mode_legacy_register_name (i)),
                                 gdb_stdout);
          fputs_filtered ("(single precision)     ", gdb_stdout);
          val_print (builtin_type_float,
                             (char *)virtual_buffer, 0, 0,
                             gdb_stdout, 0, 1, 0, Val_pretty_default);
          fprintf_filtered (gdb_stdout, "\n%sR", mixed_mode_legacy_register_name (i));
          print_spaces_filtered (7 - strlen (mixed_mode_legacy_register_name (i)),
                                 gdb_stdout);
          fputs_filtered ("(single precision)    ", gdb_stdout);
          val_print (builtin_type_float,
                             ((char *)virtual_buffer) + sizeof(float),
                             0, 0,
                             gdb_stdout, 0, 1, 0, Val_pretty_default);
        }
    }
  else
    {
      if (precision != unspecified_precision)
        {
          fputs_filtered (mixed_mode_legacy_register_name (i), gdb_stdout);
          print_spaces_filtered (8 - strlen (mixed_mode_legacy_register_name (i)), gdb_stdout);
          fputs_filtered ("(single precision)    ", gdb_stdout);
        }
      else
        fprintf_filtered (gdb_stdout, "%8.8s: ", mixed_mode_legacy_register_name (i));
   
      val_print ((is_swizzled? builtin_type_float: builtin_type_double),
                 (char *)virtual_buffer, 0, 0, gdb_stdout, 0, 1, 0,
                 Val_pretty_default);
      printf_filtered ("\n");

    }
}


/* For the given instruction (INST), return any adjustment it makes
   to the stack pointer or zero for no adjustment. 

   This only handles instructions commonly found in prologues.  */

static int
prologue_inst_adjust_sp (unsigned long inst)
{
  /* This must persist across calls.  */
  static int save_high21;

  /* The most common way to perform a stack adjustment ldo X(sp),sp */
  if ((inst & 0xffffc000) == 0x37de0000)
    return extract_14 (inst);

  /* stwm X,D(sp) */
  if ((inst & 0xffe00000) == 0x6fc00000)
    return extract_14 (inst);

  /* RM: STD,MA instruction used in PA 64*/
  if ((inst & 0xffe0000e) == 0x73c00008)
    {
        return extract_im10a(inst);
    }

  /* RM: epilogues use ldw */
  /* ldw X,D(sp) */
  if ((inst & 0xffe00000) == 0x4fc00000)
    return extract_14 (inst);

  /* addil high21,%r1; ldo low11,(%r1),%r30)
     save high bits in save_high21 for later use.  */
  if ((inst & 0xffe00000) == 0x28200000)
    {
      save_high21 = extract_21 (inst);
      return 0;
    }

  if ((inst & 0xffff0000) == 0x343e0000)
    return save_high21 + extract_14 (inst);

  /* fstws as used by the HP compilers.  */
  if ((inst & 0xffffffe0) == 0x2fd01220)
    return extract_5_load (inst);

  /* RM: fldd in epilogues */
  if ((inst & 0xffffffe0) == 0x2fd13020)
    return extract_5_load (inst);
  
  /* No adjustment.  */
  return 0;
}

/* Return nonzero if INST is a branch of some kind, else return zero.  */

static int
is_branch (inst)
     unsigned long inst;
{
  switch (inst >> 26)
    {
    case 0x20:
    case 0x21:
    case 0x22:
    case 0x23:
    case 0x27:
    case 0x28:
    case 0x29:
    case 0x2a:
    case 0x2b:
    case 0x2f:
    case 0x30:
    case 0x31:
    case 0x32:
    case 0x33:
    case 0x38:
    case 0x39:
    case 0x3a:
    case 0x3b:
      return 1;

    default:
      return 0;
    }
}

/* Return the register number for a GR which is saved by INST or
   zero if INST does not save a GR.  */

static int
inst_saves_gr (inst)
     unsigned long inst;
{
  /* JYG: MERGE FIXME: we don't have all mnemonics matched.  Why? */

  /* Does it look like a stw / stwm ?  */
  if ((inst >> 26) == 0x1a
      || (inst >> 26) == 0x1b
      || ((inst >> 26) == 0x1f && ((inst >> 1) & 0x3) == 0x2)
      || ((inst >> 26) == 0x03 && ((inst >> 6) & 0xf) == 0xa))
    return extract_5R_store (inst);

  /* Does it look like a std?  */
  if (((inst >> 26) == 0x1c && ((inst >> 1) & 0x1) == 0x0)
      || ((inst >> 26) == 0x03 && ((inst >> 6) & 0xf) == 0xb))
    return extract_5R_store (inst);

  /* Does it look like sth or stb?  HPC versions 9.0 and later use these
     too.  */
  if ((inst >> 26) == 0x19
      || (inst >> 26) == 0x18
      || ((inst >> 26) == 0x3
	  && ((((inst >> 6) & 0xf) == 0x8 || (inst >> 6) & 0xf) == 0x9)))
    return extract_5R_store (inst);

  return 0;
}

/* Return the register number for a FR which is saved by INST or
   zero it INST does not save a FR.

   Note we only care about full 64bit register stores (that's the only
   kind of stores the prologue will use).

   FIXME: What about argument stores with the HP compiler in ANSI mode? */

static int
inst_saves_fr (inst)
     unsigned long inst;
{
  /* JYG: MERGE FIXME: we don't have all mnemonics matched.  Why? */
  /* is this an FSTD ? */
  if ((inst & 0xfc00dfc0) == 0x2c001200)
    return extract_5r_store (inst);
  if ((inst & 0xfc000002) == 0x70000002)
    return extract_5R_store (inst);
  /* is this an FSTW ? */
  if ((inst & 0xfc00df80) == 0x24001200)
    return extract_5r_store (inst);
  if ((inst & 0xfc000002) == 0x7c000000)
    return extract_5R_store (inst);
  return 0;
}

/* Advance PC across any function entry prologue instructions
   to reach some "real" code. 

   Use information in the unwind table to determine what exactly should
   be in the prologue.  */


CORE_ADDR
skip_prologue_hard_way_1 (pc)
     CORE_ADDR pc;
{
  char buf[4];
  CORE_ADDR orig_pc = pc;
  CORE_ADDR fn_begin;
  unsigned long inst, stack_remaining, save_gr, save_fr, save_rp, save_sp,
    copy_sp;
  unsigned long args_stored, status, i, restart_gr, restart_fr;
  union hppa_unwind_table_entry *u;
  unsigned int Large_frame;

  status = target_read_memory (pc, buf, 4);
  inst = extract_unsigned_integer (buf, 4);
  if (!is_swizzled)
    {
      if ((inst & 0xffe00000) == 0x23600000)
        return pc;
    }
 else
    {
      if ((inst & 0xffe00000) == 0x22600000)
        return pc;
    }

  if ((inst & 0xffe00000) == 0x22c00000)
    return pc;

  restart_gr = 0;
  restart_fr = 0;

restart:
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return pc;

  /* If we are not at the beginning of a function, then return now. */
  find_pc_partial_function(pc, 0, &fn_begin, 0);
  if (pc != fn_begin)
    return pc;

  /* Skip NOP. */
  while (inst == 0x08000240)
    {
      pc += 4;
      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);
    }

  /* This is how much of a frame adjustment we need to account for.  */
  if (is_swizzled)
    {
      stack_remaining = u->u32.Total_frame_size << 3;

      /* Magic register saves we want to know about.  */
      save_rp = u->u32.Save_RP;
      save_sp = u->u32.Save_SP;
      copy_sp = u->u32.Pseudo_SP_Set;
    }
   else
    {
      stack_remaining = u->u64.Total_frame_size << 3;

      /* Magic register saves we want to know about.  */
      save_rp = u->u64.Save_RP;
      save_sp = u->u64.Save_SP;
      copy_sp = u->u64.alloca_frame;
   }
    

  /* An indication that args may be stored into the stack.  Unfortunately
     the HPUX compilers tend to set this in cases where no args were
     stored too!.  */
  args_stored = 1;

  /* Turn the Entry_GR field into a bitmask.  */
  save_gr = 0;
  for (i = 3; i < (is_swizzled? u->u32.Entry_GR + 3: u->u64.Entry_GR + 3); i++)
    {
      /* Frame pointer gets saved into a special location.  */
      if (   (is_swizzled && u->u32.Save_SP)
          || (!is_swizzled && u->u64.Save_SP)
          && i == 3)
	continue;

      save_gr |= (1 << i);
    }
  save_gr &= ~restart_gr;

  /* Turn the Entry_FR field into a bitmask too.  */
  save_fr = 0;
  for (i = 12; i < (is_swizzled? u->u32.Entry_FR + 12: u->u64.Entry_FR + 12);
       i++)
    save_fr |= (1 << i);
  save_fr &= ~restart_fr;

  if (is_swizzled)
    {
      Large_frame = u->u32.Large_frame;
    }
  else
    {
      Large_frame = u->u64.Large_frame;
    }

  /* Loop until we find everything of interest or hit a branch.

     For unoptimized GCC code and for any HP CC code this will never ever
     examine any user instructions.

     For optimzied GCC code we're faced with problems.  GCC will schedule
     its prologue and make prologue instructions available for delay slot
     filling.  The end result is user code gets mixed in with the prologue
     and a prologue instruction may be in the delay slot of the first branch
     or call.

     Some unexpected things are expected with debugging optimized code, so
     we allow this routine to walk past user instructions in optimized
     GCC code.  */
  while (save_gr || save_fr || save_rp || save_sp || stack_remaining > 0
         || copy_sp || args_stored)
    {
      unsigned int reg_num;
      unsigned long old_stack_remaining, old_save_gr, old_save_fr;
      unsigned long old_save_rp, old_save_sp, old_copy_sp, next_inst;

      /* Save copies of all the triggers so we can compare them later
         (only for HPC).  */
      old_save_gr = save_gr;
      old_save_fr = save_fr;
      old_save_rp = save_rp;
      old_save_sp = save_sp;
      old_copy_sp = copy_sp;
      old_stack_remaining = stack_remaining;

      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);

      /* Yow! */
      if (status != 0)
	return pc;

      /* Note the interesting effects of this instruction.  */
      stack_remaining -= prologue_inst_adjust_sp (inst);

      /* There are limited ways to store the return pointer into the
	 stack.  */
      if (inst == 0x6bc23fd9 || inst == 0x0fc212c1 || inst_saves_gr(inst) == 2)
	save_rp = 0;

      /* RM: If we have an alloca frame, is this the instruction that
         copies sp? */
      if (copy_sp &&
          ((Large_frame && (inst == 0x37c40000)) ||
           (!Large_frame && (inst == 0x37c30000))))
        copy_sp = 0;

      /* These are the only ways we save SP into the stack.  At this time
         the HP compilers never bother to save SP into the stack.  */
      if ((inst & 0xffffc000) == 0x6fc10000
	  || (inst & 0xffffc00c) == 0x73c10008)
	save_sp = 0;

      /* Are we loading some register with an offset from the argument
         pointer?  */
      if ((inst & 0xffe00000) == 0x37a00000
	  || (inst & 0xffffffe0) == 0x081d0240)
	{
	  pc += 4;
	  continue;
	}

      /* Account for general and floating-point register saves.  */
      reg_num = inst_saves_gr (inst);
      save_gr &= ~(1 << reg_num);

      /* Ugh.  Also account for argument stores into the stack.
         Unfortunately args_stored only tells us that some arguments
         where stored into the stack.  Not how many or what kind!

         This is a kludge as on the HP compiler sets this bit and it
         never does prologue scheduling.  So once we see one, skip past
         all of them.   We have similar code for the fp arg stores below.

         FIXME.  Can still die if we have a mix of GR and FR argument
         stores!  */
      if (reg_num >= (!is_swizzled ? 19 : 23) && reg_num <= 26)
	{
	  while (reg_num >= (!is_swizzled ? 19 : 23) && reg_num <= 26)
	    {
	      pc += 4;
	      status = target_read_memory (pc, buf, 4);
	      inst = extract_unsigned_integer (buf, 4);
	      if (status != 0)
		return pc;
	      reg_num = inst_saves_gr (inst);
	    }
	  args_stored = 0;
	  continue;
	}

      reg_num = inst_saves_fr (inst);
      save_fr &= ~(1 << reg_num);

      status = target_read_memory (pc + 4, buf, 4);
      next_inst = extract_unsigned_integer (buf, 4);

      /* Yow! */
      if (status != 0)
	return pc;

      /* We've got to be read to handle the ldo before the fp register
         save.  */
      if ((inst & 0xfc000000) == 0x34000000
	  && inst_saves_fr (next_inst) >= 4
	  && inst_saves_fr (next_inst) <= (!is_swizzled ? 11 : 7))
	{
	  /* So we drop into the code below in a reasonable state.  */
	  reg_num = inst_saves_fr (next_inst);
	  pc -= 4;
	}

      /* Ugh.  Also account for argument stores into the stack.
         This is a kludge as on the HP compiler sets this bit and it
         never does prologue scheduling.  So once we see one, skip past
         all of them.  */
      if (reg_num >= 4 && reg_num <= (!is_swizzled ? 11 : 7))
	{
	  while (reg_num >= 4 && reg_num <= (!is_swizzled ? 11 : 7))
	    {
	      pc += 8;
	      status = target_read_memory (pc, buf, 4);
	      inst = extract_unsigned_integer (buf, 4);
	      if (status != 0)
		return pc;
	      if ((inst & 0xfc000000) != 0x34000000)
		break;
	      status = target_read_memory (pc + 4, buf, 4);
	      next_inst = extract_unsigned_integer (buf, 4);
	      if (status != 0)
		return pc;
	      reg_num = inst_saves_fr (next_inst);
	    }
	  args_stored = 0;
	  continue;
	}

      /* Quit if we hit any kind of branch.  This can happen if a prologue
         instruction is in the delay slot of the first call/branch.  */
      if (is_branch (inst))
	break;

      /* What a crock.  The HP compilers set args_stored even if no
         arguments were stored into the stack (boo hiss).  This could
         cause this code to then skip a bunch of user insns (up to the
         first branch).

         To combat this we try to identify when args_stored was bogusly
         set and clear it.   We only do this when args_stored is nonzero,
         all other resources are accounted for, and nothing changed on
         this pass.  */
      if (args_stored
       && !(save_gr || save_fr || save_rp || save_sp || stack_remaining > 0)
	  && old_save_gr == save_gr && old_save_fr == save_fr
	  && old_save_rp == save_rp && old_save_sp == save_sp
          && old_copy_sp == copy_sp
	  && old_stack_remaining == stack_remaining)
        if (is_swizzled)
          break;
        else
          {
            /* RM: On PA64, the prologue contains an instruction to copy
             * the ap to one of r3, r4, or r5. Don't skip out on seeing
             * this instruction
             */
            if ((inst & 0xfff8ffff) != 0x37a00000)
	      break;
          }

      /* Bump the PC.  */
      pc += 4;
    }

  /* We've got a tenative location for the end of the prologue.  However
     because of limitations in the unwind descriptor mechanism we may
     have went too far into user code looking for the save of a register
     that does not exist.  So, if there registers we expected to be saved
     but never were, mask them out and restart.

     This should only happen in optimized code, and should be very rare.  */
  if (save_gr || (save_fr && !(restart_fr || restart_gr)))
    {
      pc = orig_pc;
      restart_gr = save_gr;
      restart_fr = save_fr;
      goto restart;
    }

  return pc;
}

#define PROLOGUE_CACHE_SIZE  64
#define EPILOGUE_CACHE_SIZE  64

struct prologue_cache {   /* sorted on an MRU basis */
    CORE_ADDR pc;
    CORE_ADDR pc_past_prologue;
} prologue_cache[PROLOGUE_CACHE_SIZE];

struct epilogue_cache {   /* sorted on an MRU basis */
    CORE_ADDR pc;
    CORE_ADDR pc_past_epilogue;
} epilogue_cache[EPILOGUE_CACHE_SIZE];

void
hppa_flush_prologue_cache ()
{
  int i;
  
  for (i=0; i < PROLOGUE_CACHE_SIZE; i++)
    prologue_cache[i].pc = prologue_cache[i].pc_past_prologue = 0;
}

void
hppa_flush_epilogue_cache ()
{
  int i;
  
  for (i=0; i < EPILOGUE_CACHE_SIZE; i++)
    epilogue_cache[i].pc = epilogue_cache[i].pc_past_epilogue = 0;
}

CORE_ADDR
mixed_mode_skip_prologue_hard_way (pc)
     CORE_ADDR pc;
{

  struct prologue_cache this, last;
  int i;

  if (prologue_cache[0].pc == pc)
    return prologue_cache[0].pc_past_prologue;

  /* not so lucky ... */

  last = prologue_cache[0];

  /* Do we have the desired info in cache ?  As we scan the list of
     entries left to right, looking for the pc of interest, we will
     bubble the entries away from the beginning i.e., the "most recently
     used" slot. This would create a vacuum in the very first slot. 
     When we find the pc we are looking for, we will move that entry to
     this slot. Alternately, if the entry is not in the cache and we
     have to determine it the hard way, we will use this slot as the
     destination. This guarentees that the list is sorted on an MRU
     basis at all times.
  */

  for (i=1; i < PROLOGUE_CACHE_SIZE; i++)
    {
      this = prologue_cache[i];
      prologue_cache[i] = last;

      if (this.pc == pc)
        {
          prologue_cache[0] = this;
          return this.pc_past_prologue;
        }

      last = this;
    }

  prologue_cache[0].pc = pc;
  prologue_cache[0].pc_past_prologue = skip_prologue_hard_way_1 (pc);
    
  return prologue_cache[0].pc_past_prologue;
}

/* RM: ??? FIXME: This procedure only finds where the stack decrement
   for a procedure is done. No other epilogue instructions are
   identified */
CORE_ADDR
skip_epilogue_hard_way_1 (pc)
     CORE_ADDR pc;
{
  char buf[4];
  unsigned long inst, stack_remaining;
  union hppa_unwind_table_entry *u;
  unsigned long status;
  CORE_ADDR orig_pc = pc;
  
  u = hppa_find_unwind_entry (pc);
  if (!u)
    return pc;

  /*   If we are not at the end of an unwind region, then return now. */ 
  if (is_swizzled)
    {
      if ((u->u32.Region_description & 0x1) || (pc != u->u32.region_end))
        return pc;
      /* This is how much of a frame adjustment we need to account for.  */
      stack_remaining = u->u32.Total_frame_size << 3;
    }
  else
    {
      if ((u->u64.Region_description & 0x1) || (pc != u->u64.region_end))
        return pc;
      /* This is how much of a frame adjustment we need to account for.  */
      stack_remaining = u->u64.Total_frame_size << 3;
    }


  /* Loop until we find where the stack decrement is */
  while (stack_remaining > 0)
    {
      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);
       
      /* Yow! */
      if (status != 0)
        return pc;

      /* Note the interesting effects of this instruction.  */
      stack_remaining += prologue_inst_adjust_sp (inst);

      /* RM: rp is restored before the epilogue begins */
      if (((inst & 0xffffc000) == 0x4bc20000) ||
          ((inst & 0xffff0002) == 0x53c20000))
        break;
      
      /* Bump the PC.  */
      pc -= 4;

      /* RM: If we went through the whole region, give up */
      if (pc < (is_swizzled? u->u32.region_start: u->u64.region_start))
        return orig_pc;
    }

  return pc+4;
}

CORE_ADDR
skip_epilogue_hard_way (pc)
     CORE_ADDR pc;
{

  struct epilogue_cache this, last;
  int i;

  if (epilogue_cache[0].pc == pc)
    return epilogue_cache[0].pc_past_epilogue;

  /* not so lucky ... */

  last = epilogue_cache[0];

  /* Do we have the desired info in cache ?  As we scan the list of
     entries left to right, looking for the pc of interest, we will
     bubble the entries away from the beginning i.e., the "most recently
     used" slot. This would create a vacuum in the very first slot. 
     When we find the pc we are looking for, we will move that entry to
     this slot. Alternately, if the entry is not in the cache and we
     have to determine it the hard way, we will use this slot as the
     destination. This guarentees that the list is sorted on an MRU
     basis at all times.
  */

  for (i=1; i < EPILOGUE_CACHE_SIZE; i++)
    {
      this = epilogue_cache[i];
      epilogue_cache[i] = last;

      if (this.pc == pc)
        {
          epilogue_cache[0] = this;
          return this.pc_past_epilogue;
        }

      last = this;
    }

  epilogue_cache[0].pc = pc;
  epilogue_cache[0].pc_past_epilogue = skip_epilogue_hard_way_1 (pc);
    
  return epilogue_cache[0].pc_past_epilogue;
}

/* Return the address of the PC after the last prologue instruction if
   we can determine it from the debug symbols.  Else return zero.  */

 int parse_args= 0; /* JAGaf60171 */

static CORE_ADDR
after_prologue (pc)
     CORE_ADDR pc;
{
  struct symtab_and_line sal;
  CORE_ADDR func_addr, func_end, start_pc;
  struct symbol *f;
  CORE_ADDR savepc=pc,newpc;
  /* If we can not find the symbol in the partial symbol table, then
     there is no hope we can determine the function's start address
     with this code.  */
  if (!find_pc_partial_function (pc, NULL, &func_addr, &func_end))
    return 0;

  f = find_pc_function (pc);
  /* MERGEBUG: are the two lines below correct? */
  if (!f)
    return 0;			/* no debug info, do it the hard way! */

  sal = find_pc_line (pc, 0);

  /* There are only two cases to consider.  First, the end of the source line
     is within the function bounds.  In that case we return the end of the
     source line.  Second is the end of the source line extends beyond the
     bounds of the current function.  We need to use the slow code to
     examine instructions in that case. 

     Anything else is simply a bug elsewhere.  Fixing it here is absolutely
     the wrong thing to do.  In fact, it should be entirely possible for this
     function to always return zero since the slow instruction scanning code
     is supposed to *always* work.  If it does not, then it is a bug.  */
  if (sal.end < func_end)
    {
/* *INDENT-OFF */
      /* this happens when the function has no prologue, because the way 
	 find_pc_line works: elz. Note: this may not be a very good
	 way to decide whether a function has a prologue or not, but
	 it is the best I can do with the info available
	 Also, this will work for functions like: int f()
	                                          {
                                                    return 2;
                                                  }
	 I.e. the bp will be inserted at the first open brace.
	 For functions where the body is only one line written like this:
	                                          int f()
                                                  { return 2; }
         this will make the breakpoint to be at the last brace, after the body
         has been executed already. What's the point of stepping through a
	 function without any variables anyway??  */
      /* JAGaf60171: For the function like this, 
						  int f()
						  { return 2;
						  }
	 the breakpoint is planted at close brace, which is not correct.
	 Now, the bp is planted at the first instruction after the nop(s).
	 This also changes the behaviour for the function which has only 
	 one line body as above. The bp will be set at the first valid
	 instruction of the line instead of at the last brace. */
/* *INDENT-ON */
    if ((SYMBOL_LINE(f) > 0) && (SYMBOL_LINE(f) < sal.line))
     return pc; /*no adjusment will be made*/
    find_exact_line_pc (sal.symtab, sal.line, &start_pc); 
    if ((current_language->la_language == language_c)
         && (SYMBOL_LINE(f) == sal.line)
         && (start_pc && (start_pc < pc)))
     {
       char buf[4];
       unsigned long status, inst, reg_num,next_inst;
         status = target_read_memory (pc, buf, 4);
         inst = extract_unsigned_integer (buf, 4);
         reg_num = inst_saves_gr (inst);
    
      /* Skip NOP. */
        while (inst != 0x08000240)
        {
    	 if(pc<func_end)
     	 {
      		pc += 4;
      		status = target_read_memory (pc, buf, 4);
      		inst = extract_unsigned_integer (buf, 4);
      		reg_num = inst_saves_gr (inst);
     	 }
    	 else
         {
     		if(pc==func_end)
     		  pc=savepc;
     		else
     		  pc=sal.end;
   	        break;
    	 }
   	}
    
    	if(inst == 0x08000240)
     	{
   		if(pc<func_end && pc<sal.end)
     		{
     		  pc+=4;
    		}
    		else
    		{
    		  if(pc<func_end || pc<sal.end)
     		    pc=sal.end;
    		  else
    		    pc=savepc;
    		}
   	}
        status = target_read_memory (pc, buf, 4);
        inst = extract_unsigned_integer (buf, 4);
        reg_num = inst_saves_gr (inst);

      /* To skip the argument store instruction if the function is like
         int f (arg) {return 0}; so that conditions can be evaluated
         before the breakpoint for conditional breakpoints. */
      
       if (reg_num >= (TARGET_PTR_BIT == 64 ? 19 : 23) && reg_num <= 26)
       {
        while (reg_num >= (TARGET_PTR_BIT == 64 ? 19 : 23) && reg_num <= 26)
        {
          pc += 4;
          status = target_read_memory (pc, buf, 4);
          inst = extract_unsigned_integer (buf, 4);
          if (status != 0)
            return pc;
          reg_num = inst_saves_gr (inst);
        }
        parse_args = 1;
      }
      else
      {
       parse_args = 0;
      }

      if(parse_args==0)
      {
        reg_num = inst_saves_fr (inst);
  
       if (reg_num >= 4 && reg_num <= (TARGET_PTR_BIT == 64 ? 11 : 7))
        {
          while (reg_num >= 4 && reg_num <= (TARGET_PTR_BIT == 64 ? 11 : 7))
            {
              pc += 8;
              status = target_read_memory (pc, buf, 4);
              inst = extract_unsigned_integer (buf, 4);
              if (status != 0)
                return pc;
             /* if ((inst & 0xfc000000) != 0x34000000)
                break; */
              status = target_read_memory (pc + 4, buf, 4);
              next_inst = extract_unsigned_integer (buf, 4);
              if (status != 0)
                return pc;
              reg_num = inst_saves_fr (next_inst);
            }
           parse_args=1;
        }
       else
         parse_args=0;
      }

     if(parse_args!=0)
	    return pc;
     if(pc<=sal.end)  
  	    return pc;
     else
   	    return sal.end;
    }
    else
   	    return sal.end;  
   }
  else
  /* The line after the prologue is after the end of the function.  In this
     case, put the end of the prologue is the beginning of the function.  */
  /* This should happen only when the function is prologueless and has no
     code in it. For instance void dumb(){} Note: this kind of function
     is  used quite a lot in the test system */
    return pc;
}

/* To skip prologues, I use this predicate.  Returns either PC itself
   if the code at PC does not look like a function prologue; otherwise
   returns an address that (if we're lucky) follows the prologue.  If
   LENIENT, then we must skip everything which is involved in setting
   up the frame (it's OK to skip more, just so long as we don't skip
   anything which might clobber the registers which are being saved.
   Currently we must not skip more on the alpha, but we might the lenient
   stuff some day.  */

CORE_ADDR
hppa_skip_prologue (pc)
     CORE_ADDR pc;
{
  unsigned long inst, status;
  int offset;
  CORE_ADDR post_prologue_pc;
  char buf[4];

  /* JYG: PURIFY FIXME: maybe the actual routine behind target_read_memory
     should be fixed to eliminate this bogus purify UMR error once and
     for all (seems like purify is unable to ascertain whether buf is
     touched or not by target_read_memory). */
  /* BEGIN PURIFY */
  memset (buf, 0, 4);
  /* END PURIFY */

    /* Skip NOP. */
    while (1)
      {
        status = target_read_memory (pc, buf, 4);
        inst = extract_unsigned_integer (buf, 4);
        if (inst != 0x08000240)
          break;
        pc += 4;
      }

  /* See if we can determine the end of the prologue via the symbol table.
     If so, then return either PC, or the PC after the prologue, whichever
     is greater.  */

  post_prologue_pc = after_prologue (pc);

  /* If after_prologue returned a useful address, then use it.  Else
     fall back on the instruction skipping code.

     Some folks have claimed this causes problems because the breakpoint
     may be the first instruction of the prologue.  If that happens, then
     the instruction skipping code has a bug that needs to be fixed.  */
  if (post_prologue_pc != 0)
    return max (pc, post_prologue_pc);
  else
    return (mixed_mode_skip_prologue_hard_way (pc));
}

/* Including an arg_size of size 8 for 32 and 64 bit addresses. */
int arg_size[8];

/* Put here the code to store, into a struct mixed_mode_frame_saved_regs,
   the addresses of the saved registers of frame described by FRAME_INFO.
   This includes special registers such as pc and fp saved in special
   ways in the stack frame.  sp is even more special:
   the address we return for it IS the sp for the next frame.  */

void
mixed_mode_frame_find_saved_regs (
  struct frame_info *frame_info,
  struct mixed_mode_frame_saved_regs *mixed_mode_frame_saved_regs)
{
  CORE_ADDR pc;
  union hppa_unwind_table_entry *u, *u2;
  unsigned long inst, stack_remaining, save_gr, save_fr, save_rp, save_sp;
  int status, i, reg;
  char buf[4];
  int fp_loc = -1;
  extern CORE_ADDR text_end;
  struct minimal_symbol *msym;
  struct minimal_symbol *msym_sr4export;
  int final_iteration;
  int pa_num_regs = (is_swizzled)? 128: 96;
  int target_ptr_bit = (is_swizzled ? 32: 64);
  int register_size = (is_swizzled ? 4: 8);
  int hppa_fp4_regnum = is_swizzled ? 72: 68;

  /* Zero out everything.  */
  memset (mixed_mode_frame_saved_regs, '\0',
          sizeof (struct mixed_mode_frame_saved_regs));

  /* Get the starting address of the function referred to by the PC
     saved in frame.  */
  pc = get_pc_function_start (frame_info->pc);

  /* Yow! */
  u = hppa_find_unwind_entry (pc);

  /* Stacey 11/8/2001
     In most cases the code above will work just fine ... however in the
     special case where we're trying to retrieve registers after a call to 
     a function with a variable size stack frame, the code can hurt us by:
     
     1.  leading us to an unwind entry with bad or insufficient info or
         the stub of the procedure may have a different unwind
	 entry from the body of the procedure, containing less saved
	 registers.  

     2.  making sure we don't get an unwind entry at all 
         the entry stub of a procedure may not have an unwind entry -
	 in which case we will get here having no unwind entry.  This
	 would be a bug on HP C, but we should handle this case anyways.  

     The if/else condition below is designed to handle these special cases by 
     making sure we get

     1.  a number >= to the number of saved registers - even if we've got the 
         wrong unwind entry.  This helps since we don't really use the unwind
	 entry for anything at this point except register information.  While
	 this isn't perfect, at least we won't lose register information.  

     2.  an unwind entry in the case that there is no unwind entry for the
         entry stub of a procedure.  */

  if (!u)
    {
      u = hppa_find_unwind_entry (frame_info->pc);
      if (!u)
	/* we have no unwind entry !!! */
        /* The frame always represents the value of %sp at entry to the
           current function (and is thus equivalent to the "saved" stack
           pointer.  */
        mixed_mode_frame_saved_regs->regs[PA_SP_REGNUM] = frame_info->frame;
	return;
    }
  else 
    {
      u2 = hppa_find_unwind_entry (frame_info->pc);
      if (u2)
        {
          if (is_swizzled && (u2->u32.Entry_GR > u->u32.Entry_GR))
	    u->u32.Entry_GR = u2->u32.Entry_GR;
          else if (!is_swizzled && (u2->u64.Entry_GR > u->u64.Entry_GR))
	    u->u64.Entry_GR = u2->u64.Entry_GR;
        }
    }

  /* This is how much of a frame adjustment we need to account for.  */
  unsigned int Total_frame_size = is_swizzled? u->u32.Total_frame_size:
                                               u->u64.Total_frame_size;

  stack_remaining = Total_frame_size << 3;

  /* Magic register saves we want to know about.  */
  save_rp = is_swizzled? u->u32.Save_RP: u->u64.Save_RP;
  save_sp = is_swizzled? u->u32.Save_SP: u->u64.Save_SP;

  /* rven: In a conversation with Cary Coutant 10/12/2001, Cary told
     me that on PA, it is architected that the callee save registers
     are contiguously saved starting with register r3 at the beginning 
     of the frame */ 
  /* Turn the Entry_GR field into a bitmask.  */
  save_gr = 0;
  unsigned int Entry_GR = is_swizzled?  u->u32.Entry_GR: u->u64.Entry_GR;
  unsigned int Save_SP = is_swizzled?  u->u32.Save_SP: u->u64.Save_SP;
  unsigned int Entry_FR = is_swizzled?  u->u32.Entry_FR: u->u64.Entry_FR;

  for (i = 3; i < Entry_GR + 3; i++)
    {
      /* Frame pointer gets saved into a special location.  */
      if (Save_SP && i == PA_FP_REGNUM)
	continue;

      if (i <= 8)
      	mixed_mode_frame_saved_regs->regs[i] = frame_info->frame + 
		(Entry_FR) * 8 +     /* Skip FP regs which are saved first */
		(i-3) * sizeof (CORE_ADDR);
      else
	save_gr |= (1 << i);
    }

  /* Turn the Entry_FR field into a bitmask too.  */
  save_fr = 0;
  for (i = 12; i < Entry_FR + 12; i++)
    save_fr |= (1 << i);

  /* The frame always represents the value of %sp at entry to the
     current function (and is thus equivalent to the "saved" stack
     pointer.  */
  mixed_mode_frame_saved_regs->regs[PA_SP_REGNUM] = frame_info->frame;
  /* This code is HP compiler specific. */
  if (   (!is_swizzled && !hppa_pc_in_gcc_function (frame_info->pc))
      || (is_swizzled))
    {
    /* For JAGae85195 - Sunil, access the frame to get the contents of r23-r26*/
    /* Although a check whether Args_stored bit is set or not is done, it
       is not guaranteed that we can actually get to the arguments as
       the compiler may have optimized the initial store of args
    */
    register struct symbol *sym = NULL;
    register struct symbol *func = get_frame_function (frame_info);
    struct block *b = NULL;
    int nsyms = 0;

#define R26_REGNUM 26
#define R25_REGNUM 25
#define R24_REGNUM 24
#define R23_REGNUM 23
#define R22_REGNUM 22
#define R21_REGNUM 21
#define R20_REGNUM 20
#define R19_REGNUM 19

    if (func)
      {
        b = SYMBOL_BLOCK_VALUE (func);
        nsyms = BLOCK_NSYMS (b);
      }

    uint64_t temp_sp;
    temp_sp = (is_swizzled)? FRAME_FP(frame_info): frame_info->ap;

    for (i = 0; i < nsyms; i++)
      {
        sym = BLOCK_SYM (b, i);

        if (sym->is_argument != 1) /* do not print locals */
          continue;

        struct type *type = SYMBOL_TYPE (sym);
	
  /* JAGae85195 - Sunil S
     The calculation follows a general pattern

     temp_sp + number of bytes to advance to the next word - argument size - number of bytes 
     to be subtracted from temp_sp to get to the posistion of the corresponding argument.
*/
        switch (i)
          {
            /* for r26 */
            case 0:
    	        arg_size[i] = TYPE_LENGTH (type);
                if (is_swizzled)
                  mixed_mode_frame_saved_regs->regs[R26_REGNUM] = temp_sp + 4 - arg_size[i] - 36;
                else
    	          mixed_mode_frame_saved_regs->regs[R26_REGNUM] = temp_sp + 8 - arg_size[i] - 64;
	        break;
	
            /* for r25 */
            case 1:
    	        arg_size[i] = TYPE_LENGTH (type);
                if (is_swizzled)
                  mixed_mode_frame_saved_regs->regs[R25_REGNUM] = temp_sp + 4 - arg_size[i] - 40;
                else
    	          mixed_mode_frame_saved_regs->regs[R25_REGNUM] = temp_sp + 8 - arg_size[i] - 56;
	        break;

            /* for r24 */
            case 2:
    	        arg_size[i] = TYPE_LENGTH (type);
                if (is_swizzled)
                  mixed_mode_frame_saved_regs->regs[R24_REGNUM] = temp_sp + 4 - arg_size[i] - 44;
                else
    	          mixed_mode_frame_saved_regs->regs[R24_REGNUM] = temp_sp + 8 - arg_size[i] - 48;
	        break;

            /* for r23 */
            case 3:
      	        arg_size[i] = TYPE_LENGTH (type);
                if (is_swizzled)
                  mixed_mode_frame_saved_regs->regs[R23_REGNUM] = temp_sp + 4 - arg_size[i] - 48;
                else
    	          mixed_mode_frame_saved_regs->regs[R23_REGNUM] = temp_sp + 8 - arg_size[i] - 40;
	        break;

	    case 4:
                if (!is_swizzled)
                  {
    	            arg_size[i] = TYPE_LENGTH (type);
    	            mixed_mode_frame_saved_regs->regs[R22_REGNUM] = temp_sp + 8 - arg_size[i] - 32;
	            break;
                  }
                else
                  continue;

	    case 5:
                if (!is_swizzled)
                  {
    	            arg_size[i] = TYPE_LENGTH (type);
    	            mixed_mode_frame_saved_regs->regs[R21_REGNUM] = temp_sp + 8 - arg_size[i] - 24;
	            break;
                  }
                else
                  continue;

	    case 6:
                if (!is_swizzled)
                  {
    	            arg_size[i] = TYPE_LENGTH (type);
    	            mixed_mode_frame_saved_regs->regs[R20_REGNUM] = temp_sp + 8 - arg_size[i] - 16;
	            break;
                  }
                else
                  continue;

	    case 7:
                if (!is_swizzled)
                  {
    	            arg_size[i] = TYPE_LENGTH (type);
    	            mixed_mode_frame_saved_regs->regs[R19_REGNUM] = temp_sp + 8 - arg_size[i] - 8;
	            break;
                  }
                else
                  continue;
            /* Other types of symbols we just skip over.  */
	    default:
	      continue;
	  }
       } /* End for */
    } /* End if (!hppa_pc_in_gcc_function (frame_info)) */
  /* Loop until we find everything of interest or hit a branch.

     For unoptimized GCC code and for any HP CC code this will never ever
     examine any user instructions.

     For optimized GCC code we're faced with problems.  GCC will schedule
     its prologue and make prologue instructions available for delay slot
     filling.  The end result is user code gets mixed in with the prologue
     and a prologue instruction may be in the delay slot of the first branch
     or call.

     Some unexpected things are expected with debugging optimized code, so
     we allow this routine to walk past user instructions in optimized
     GCC code.  */
  final_iteration = 0;
  while ((save_gr || save_fr || save_rp || save_sp || stack_remaining > 0)
	 && pc <= frame_info->pc)
    {
      status = target_read_memory (pc, buf, target_ptr_bit / 8);
      inst = extract_unsigned_integer (buf, 4);

      /* Yow! */
      if (status != 0)
	return;

      /* Note the interesting effects of this instruction.  */
      stack_remaining -= prologue_inst_adjust_sp (inst);

      /* There are limited ways to store the return pointer into the
	 stack.  */
      /* stw rp,-0x14(sr0,sp) */
      /* std rp,-0x10(sr0,sp) */
      if (inst == 0x6bc23fd9 || inst == 0x0fc212c1 || inst_saves_gr(inst) == 2)
        {
          save_rp = 0;
          mixed_mode_frame_saved_regs->regs[PA_RP_REGNUM] = frame_info->frame - 
				    (target_ptr_bit == 64 ? 16 : 20);
	}

      /* Note if we saved SP into the stack.  This also happens to indicate
	 the location of the saved frame pointer.  */
      if (   (inst & 0xffffc000) == 0x6fc10000  /* stw,ma r1,N(sr0,sp) */
          || (inst & 0xffffc00c) == 0x73c10008) /* std,ma r1,N(sr0,sp) */
	{
	  mixed_mode_frame_saved_regs->regs[PA_FP_REGNUM] = frame_info->frame;
	  save_sp = 0;
	}

      /* Account for general and floating-point register saves.  */
      reg = inst_saves_gr (inst);
      if (reg >= 9 && reg <= 18
	  && (!Save_SP || reg != PA_FP_REGNUM))
	{
	  save_gr &= ~(1 << reg);

	  /* stwm with a positive displacement is a *post modify*.  */
	  if ((inst >> 26) == 0x1b
	      && extract_14 (inst) >= 0)
            mixed_mode_frame_saved_regs->regs[reg]
              = frame_info->frame + (fp_loc != -1 ? fp_loc : 0) ;
          /* RM: extract_14 should only be used for STW instructions */
          else if (((inst >> 26) == 0x1a) || ((inst >> 26) == 0x1b))
            {
              int basereg = (inst & 0x03e00000) >> 21;
              if (basereg == PA_SP_REGNUM)
	        {
		  /* Handle code with and without frame pointers.  */
		  if (Save_SP)
		    mixed_mode_frame_saved_regs->regs[reg]
		      = frame_info->frame + extract_14 (inst);
		  else
		    mixed_mode_frame_saved_regs->regs[reg]
		      = frame_info->frame + (Total_frame_size << 3)
		      + extract_14 (inst);
		}
              else if (basereg == 3)
		mixed_mode_frame_saved_regs->regs[reg] = frame_info->frame +
		  extract_14 (inst);
              /* Removed Unexpected instruction warning message.
              From a frame saved registers perspective, the saves will all be
              relative to SP. (R3 is only for gcc/g++, not for HP compilers).*/
            }

          /* RM: Now handle STD instructions */
	  /* A std has explicit post_modify forms.
	     JYG: And so we match for
	     STD long disp form, m-field == 1, a-field == 0, instead of
	     checking whether extract_im10a >= 0 */

	  else if ((inst & 0xfc00000e) == 0x70000008)
	    mixed_mode_frame_saved_regs->regs[reg] = frame_info->frame;
          else if ((inst >> 26) == 0x1c)
            {
              /* Handle code with and without frame pointers.  */
              if (Save_SP)
                mixed_mode_frame_saved_regs->regs[reg]
                  = frame_info->frame + extract_im10a (inst);
              else
                mixed_mode_frame_saved_regs->regs[reg]
                  = frame_info->frame + (Total_frame_size << 3)
                    + extract_im10a (inst);
            }
        }

      /* GCC handles callee saved FP regs a little differently.  

         It emits an instruction to put the value of the start of
         the FP store area into %r1.  It then uses fstds,ma with
         a basereg of %r1 for the stores.

         HP CC emits them at the current stack pointer modifying
         the stack pointer as it stores each register.  */

      /* ldo X(%r3),%r1 or ldo X(%r30),%r1.  */
      if ((inst & 0xffffc000) == 0x34610000
	  || (inst & 0xffffc000) == 0x37c10000)
	fp_loc = extract_14 (inst);

      reg = inst_saves_fr (inst);
      if (reg >= 12 && reg <= 21)
	{
	  /* Note +4 braindamage below is necessary because the FP status
	     registers are internally 8 registers rather than the expected
	     4 registers.  */
	  save_fr &= ~(1 << reg);
	  if (fp_loc == -1)
	    {
	      /* 1st HP CC FP register store.  After this instruction
	         we've set enough state that the GCC and HPCC code are
	         both handled in the same manner.  */
	      mixed_mode_frame_saved_regs->regs[reg + hppa_fp4_regnum + register_size] = frame_info->frame;
	      fp_loc = 8;
	    }
	  else
	    {
	      mixed_mode_frame_saved_regs->regs[reg + PA_FP0_REGNUM + register_size]
		= frame_info->frame + fp_loc;
	      fp_loc += 8;
	    }
	}

      /* Quit if we hit any kind of branch the previous iteration. */
      if (final_iteration)
	break;

      /* We want to look precisely one instruction beyond the branch
	 if we have not found everything yet.  */
      if (is_branch (inst))
	final_iteration = 1;

      /* Bump the PC.  */
      pc += 4;
    }
}

void
_initialize_mixed_mode_tdep ()
{
  /* Retaining a dummy "_initialize_mixed_mode_tdep" function since the
     inclusion of mixed-mode-tdep.c among the files to be built results in
     this function being referenced by init.c in the build directory. */
}

/* We need to correct the PC and the FP for the outermost frame when we are
   in a system call.  */

/* This routine is called by the PA gdb and the IPF gdb (for mixed mode
   unwind) */
void
mixed_mode_init_extra_frame_info (int fromleaf, struct frame_info *frame)
{
  uint64_t flags;
  int framesize = 0;
  CORE_ADDR pc;
  CORE_ADDR sp;
  CORE_ADDR epilogue_begin;
  CORE_ADDR prologue_end;
  char buf[4];
  unsigned long inst;
  unsigned long status;
  union hppa_unwind_table_entry *u;
  int inc=0;

  if (frame->next && !fromleaf)
    return;

  /* If the next frame represents a frameless function invocation
     then we have to do some adjustments that are normally done by
     FRAME_CHAIN.  (FRAME_CHAIN is not called in this case.)  */
  if (fromleaf)
    {
      /* jini: Mixed mode support. Changed from the macro calls to
         SAVED_PC_AFTER_CALL, FRAME_SAVED_PC, etc to directly call
         the required PA functions. For non mixed-mode, the calls would otherwise
         get redirected to the IPF counterparts. */
      frame->pc = hppa_saved_pc_after_call (frame->next);
      /* Find the framesize of *this* frame without peeking at the PC
         in the current frame structure (it isn't set yet).  */
      framesize = mixed_mode_find_proc_framesize
                  (mixed_mode_frame_saved_pc (get_next_frame (frame)));

      /* Now adjust our base frame accordingly.  If we have a frame pointer
         use it, else subtract the size of this frame from the current
         frame.  (we always want frame->frame to point at the lowest address
         in the frame).  */
      if (framesize == -1)
        {
          if (!hppa_pc_in_gcc_function(frame->pc))
            {
              u = hppa_find_unwind_entry (frame->pc);
              /* RM: what to do? */
              if (!u)
                return;
              find_pc_partial_function (frame->pc, 0, &pc, 0);
              prologue_end = mixed_mode_skip_prologue_hard_way(pc);
              if (frame->pc >= prologue_end)
                {
                  if (is_swizzled)
                    {
                      if (u->u32.Large_frame)
                        {
                          frame->frame =
                            frame->pa_save_state_ptr->ss_narrow.ss_gr4 -
                            (u->u32.Total_frame_size << 3);
                        }
                      else
                        {
                          frame->frame =
                            frame->pa_save_state_ptr->ss_narrow.ss_gr3 -
                            (u->u32.Total_frame_size << 3);
                        }
                    }
                  else
                    {
                      if (u->u64.Large_frame)
                        {
                          frame->frame =
                            frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr4
                            - (u->u64.Total_frame_size << 3);
                        }
                      else
                        {
                          frame->frame =
                            frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3 -
                            (u->u64.Total_frame_size << 3);
                        }
                    }
                  return;
                }
              if (is_swizzled)
                {
                  framesize = u->u32.Total_frame_size << 3;
                }
              else
                {
                  framesize = u->u64.Total_frame_size << 3;
                }
            }
          else
            {
              /* Get the FP from the save state structure that aries returned. */
              if (is_swizzled)
                {
                  frame->frame = frame->pa_save_state_ptr->ss_narrow.ss_gr3;
                }
              else
                {
                  frame->frame = frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3;
                }
              return;
            }
        }
      frame->frame -= framesize;
      return;
    }

  assert (mixed_mode_pa_unwind);
  flags = frame->pa_save_state_ptr->ss_flags;

  if (IN_SYSCALL(flags))        /* In system call? */
    {
      if (is_swizzled)
        {
          frame->pc = frame->pa_save_state_ptr->ss_narrow.ss_gr31;
        }
      else
        {
          frame->pc = frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr31;
        }
    }

  /* The outermost frame is always derived from PC-framesize

     One might think frameless innermost frames should have
     a frame->frame that is the same as the parent's frame->frame.
     That is wrong; frame->frame in that case should be the *high*
     address of the parent's frame.  It's complicated as hell to
     explain, but the parent *always* creates some stack space for
     the child.  So the child actually does have a frame of some
     sorts, and its base is the high address in its parent's frame.  */
  framesize = mixed_mode_find_proc_framesize (frame->pc);

  /* RM: Are we in the prologue? */
  find_pc_partial_function (frame->pc, 0, &pc, 0);
  prologue_end = mixed_mode_skip_prologue_hard_way(pc);
  if (framesize == -1)
    {
      if (!hppa_pc_in_gcc_function(frame->pc))
        {
          u = hppa_find_unwind_entry (frame->pc);
          /* RM: what to do? */
          if (!u)
            return;
          if (frame->pc >= prologue_end)
            {
              if (is_swizzled)
                {
                  if (u->u32.Large_frame)
                    {
                       frame->frame =
                         frame->pa_save_state_ptr->ss_narrow.ss_gr4 -
                         (u->u32.Total_frame_size << 3);
                    }
                  else
                    {
                      frame->frame =
                        frame->pa_save_state_ptr->ss_narrow.ss_gr3 -
                        (u->u32.Total_frame_size << 3);
                    }
                }
              else
                {
                  if (u->u64.Large_frame)
                    {
                      frame->frame =
                        frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr4
                        - (u->u64.Total_frame_size << 3);
                    }
                  else
                    {
                      frame->frame =
                        frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3 -
                        (u->u64.Total_frame_size << 3);
                    }
                }
                
              return;
            }
          if (is_swizzled)
            {
              framesize = u->u32.Total_frame_size << 3;
            }
          else
            {
              framesize = u->u64.Total_frame_size << 3;
            }
          /* RM: fall through to prologue analyzer */
        }
      else
        {
          /* Get the FP from the save state structure that aries returned. */
          if (is_swizzled)
            {
              frame->frame = frame->pa_save_state_ptr->ss_narrow.ss_gr3;
            }
          else
            {
              frame->frame = frame->pa_save_state_ptr->ss_wide.ss_64.ss_gr3;
            }
          return;
        }
    }

  /* RM: If we are in the prologue of a function, we need to do
   * some adjustments to the framesize before we can subtract it
   * from the stack pointer. The current frame is probably junk,
   * but at least we can unwind the stack correctly if we find
   * where the current frame would start if we were past the prologue
   *
   * These are terrible hacks.
   */
  if (is_swizzled)
    {
      sp = frame->pa_save_state_ptr->ss_narrow.ss_sp;
    }
  else
    {
      sp = frame->pa_save_state_ptr->ss_wide.ss_64.ss_sp;
    }

  for (pc = frame->pc; pc < prologue_end; pc += 4)
    {
      status = target_read_memory (pc, buf, 4);
      inst = extract_unsigned_integer (buf, 4);
      inc += prologue_inst_adjust_sp(inst);
    }
  
  /* RM: similarly for epilogues */
  u = hppa_find_unwind_entry (pc);
  if (   u
     && (   (is_swizzled && !(u->u32.Region_description & 0x1))
         || (!is_swizzled && !(u->u64.Region_description & 0x1))))
    {
      if (is_swizzled)
        pc = u->u32.region_end;
      else
        pc = u->u64.region_end;

      epilogue_begin = skip_epilogue_hard_way (pc);
      for (pc=frame->pc-4; pc >= epilogue_begin; pc-=4)
        {
          status = target_read_memory (pc, buf, 4);
          inst = extract_unsigned_integer (buf, 4);
          inc -= prologue_inst_adjust_sp(inst);
        }
    }
  
  sp+=inc;
  frame->frame = sp - framesize;

}
