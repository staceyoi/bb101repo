/* Handle HP ELF shared libraries for GDB, the GNU Debugger.
   Copyright 1999 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA. */


#include <elf.h>
#include <elf_hp.h>

#include "defs.h"
#include "pa64solib.h"

#include "frame.h"
#include "bfd.h"
#include "elf-bfd.h"
#include "libhppa.h"
#include "gdbcore.h"
#include "symtab.h"
#include "breakpoint.h"
#include "symfile.h"
#include "objfiles.h"
#include "inferior.h"
#include "gdb-stabs.h"
#include "gdb_stat.h"
#include "gdbcmd.h"
#include "assert.h"
#include "language.h"
#include "breakpoint.h"
#include "top.h"

#include <fcntl.h>

#include <unwind_dld.h>  /* For load_module_desc */

#ifndef O_BINARY
#define O_BINARY 0
#endif

/* Defined in exec.c; used to prevent dangling pointer bug.  */
extern struct target_ops exec_ops;

/* Defined in gdbrtc.c; Used for supressing warnings to end-user */
extern bool add_unloaded_library_symbols;

/* This lives in hppa-tdep.c. */
extern struct unwind_table_entry *find_unwind_entry (CORE_ADDR);

extern char* dld_name;

/*JAGae68084- persistent deferred breakpoint */
extern int if_yes_to_all_commands;
int in_shl_load;
int now_re_enable_bp;
char *shllib_name;
static int strt_loaded;

extern CORE_ADDR text_start;
extern CORE_ADDR text_end;

/* Support for PA64 GDB to step into BOR calls*/
CORE_ADDR solib_bor_event();
void pa64_request_bor_callback (int);

/* Support for batch mode thread check. */
extern void mark_librtc_start_end (CORE_ADDR text_start, CORE_ADDR text_end);
extern void mark_libpthread_start_end (CORE_ADDR text_start,
                                       CORE_ADDR text_end);

/* Baskar, JAGae50486 - On abort in dynamically shared libs, gdb couldn't
   find it in the core file.
*/
uint64_t
dlgetmodinfo1 (int index,
              struct load_module_desc *desc,
              size_t desc_size,
              void *(*read_tgt_mem) (void *buffer, uint64_t ptr, size_t bufsiz, int ident),
              int ident_parm,
              uint64_t load_map_parm);

int
is_executable (int index,
               void *(*read_tgt_mem) (void *buffer, uint64_t ptr, size_t bufsiz, int ident),
               int ident_parm,
               uint64_t load_map_parm);

/* Defined in libxpdl.a from the linker project. */

uint64_t
dlgetmodinfo (int index,
              struct load_module_desc *desc,
              size_t desc_size,
              void *(*read_tgt_mem) (void *buffer,
                                     uint64_t ptr,
                                     size_t bufsiz,
                                     int ident),
              int ident_parm,
              uint64_t load_map_parm);

char *
  dlgetname (struct load_module_desc *desc,
             size_t desc_size,
             void *(*read_tgt_mem) (void *buffer,
                                    uint64_t ptr,
                                    size_t bufsiz,
                                    int ident),
             int ident_parm,
             uint64_t load_map_parm);

/* JAGaf26681 - Warn user that shared libraries are not mapped private. */
int is_solib_mapped_private (void);

/* Need to disable core library mismatch detection if the gdb or the core used
 * an older dld.
 */
static int core_so_mismatch_detection_disabled = 0;
int shlib_load_completed = 1; /* JAGaf35513 */


/* These ought to be defined in some public interface, but aren't.  They
   identify dynamic linker events.
   DLD_CB_LOAD   Arg to break routine for shared library load.
   DLD_CB_UNLOAD Arg to break routine for shared library unload. */

#define DLD_CB_LOAD     1
#define DLD_CB_UNLOAD   0
#define DLD_CB_BOR      2             /* BOR event */
#define DLD_CB_LOAD_COMPLETE	4     /* "loading complete" event */

/*--------------------------------------------------------------------------
   Data Declarations
--------------------------------------------------------------------------*/
#ifdef RTC  /* JAGaf15615 */
extern int check_heap_in_this_run, trace_threads_in_this_run;
extern int rtc_initialized;
extern int batch_rtc;
#endif /* RTC */

typedef struct catch_load_unload_event
  {
    struct breakpoint *breakpt;
    char *filename;
    struct catch_load_unload_event *next;
  }
catch_load_unload_event_t;

catch_load_unload_event_t *catch_load_list = NULL;
catch_load_unload_event_t *catch_unload_list = NULL;

/* A structure to keep track of all the known shared objects.  */
struct so_list
  {
    bfd *abfd;
    char name[MAXPATHLEN];
    struct so_list *next;
    struct objfile *objfile;
    CORE_ADDR pa64_solib_desc_addr;
    struct load_module_desc pa64_solib_desc;
    struct section_table *sections;
    struct section_table *sections_end;
    boolean loaded;
  };

static struct so_list *so_list_head;
static struct so_list *so_list_tail;

/* This is the cumulative size in bytes of the symbol tables of all
   shared objects on the so_list_head list.  (When we say size, here
   we mean of the information before it is brought into memory and
   potentially expanded by GDB.)  When adding a new shlib, this value
   is compared against the threshold size, held by auto_solib_add
   (in megabytes).  If adding symbols for the new shlib would cause
   the total size to exceed the threshold, then the new shlib's symbols
   are not loaded.  */
static LONGEST pa64_solib_total_st_size;

/* When the threshold is reached for any shlib, we refuse to add
   symbols for subsequent shlibs, even if those shlibs' symbols would
   be small enough to fit under the threshold.  (Although this may
   result in one, early large shlib preventing the loading of later,
   smalller shlibs' symbols, it allows us to issue one informational
   message.  The alternative, to issue a message for each shlib whose
   symbols aren't loaded, could be a big annoyance where the threshold
   is exceeded due to a very large number of shlibs.) */
static int pa64_solib_st_size_threshold_exceeded;

/* These addresses should be filled in by pa64_solib_create_inferior_hook.
   They are also used elsewhere in this module. */
typedef struct
  {
    CORE_ADDR address;
    struct unwind_table_entry *unwind;
  }
addr_and_unwind_t;

/* When adding fields, be sure to clear them in _initialize_pa64_solib. 
   or really pa64_solib_restart which is called by it. */
typedef struct
  {
    CORE_ADDR dld_flags_addr;
    long long dld_flags;
    sec_ptr dyninfo_sect;
    boolean have_read_dld_descriptor;
    boolean is_archive_bound;
    boolean is_valid;
    CORE_ADDR load_map;
    CORE_ADDR load_map_addr;
    struct load_module_desc dld_desc;
  }
dld_cache_t;

static dld_cache_t dld_cache;

static char *gdb_shlib_root = 0;
char **gdb_shlib_path_list = 0;
int gdb_shlib_path_length = 0;

static int error_in_shlib_symtab = 0;

/* This are from gdbrtc.c */
extern struct objfile* rtc_dot_sl;

static boolean threshhold_warning_given = 0;

static int pa64_solib_sizeof_symbol_table (char *);

static void pa64_solib_add_solib_objfile (struct so_list *, int, boolean);

static void pa64_solib_load_symbols (struct so_list *, boolean,
				     struct target_ops *);

static void pa64_solib_create_catch_load_unload_hook (int, char *, char *,
						      catch_load_unload_event_t **,
						      const char *);

static void pa64_sharedlibrary_info_command (char *, int);

static void pa64_solib_sharedlibrary_command (char *, int);

static boolean read_dld_descriptor (struct target_ops *);

static boolean read_dynamic_info (asection *, dld_cache_t *);

static void *pa64_target_read_memory (void *, CORE_ADDR, size_t, int);

/* Use add_to_solist_1 with catch_errors instead. */
typedef struct {
  boolean from_tty;
  char *dll_path;
  struct load_module_desc *load_module_desc_p;
  CORE_ADDR load_module_desc_addr;
  struct target_ops *target;
} args_for_add_to_solist;

static int add_to_solist_1 (PTR args);
static void add_to_solist (boolean, char *, struct load_module_desc *,
			   CORE_ADDR, struct target_ops *);

static int handle_dynlink_load_event (int);

static int handle_dynlink_unload_event (int);

/* When examining the shared library for debugging information we have to
   look for HP debug symbols, stabs and dwarf2 debug symbols.  */
static char *pa64_debug_section_names[] = {
  ".debug_header", ".debug_gntt", ".debug_lntt", ".debug_slt", ".debug_vt",
  ".stabs", ".stabstr", ".debug_info", ".debug_abbrev", ".debug_aranges",
  ".debug_macinfo", ".debug_line", ".debug_loc", ".debug_pubnames",
  ".debug_str", NULL
};

/* Return a ballpark figure for the amount of memory GDB will need to
   allocate to read in the debug symbols from FILENAME.  

   Stacey 10/17/2001
   This function returns only if all goes well and errors out otherwise.
   In some cases, we want the error caught so we know about it but can
   ask gdb to continue executing.  Since this function can return 0 if 
   all is well and catch_errors returns 0 for errors or the value of the
   function otherwise, after calling this from catch_errors we won't know
   if the call completed correctly. So I introduced a new global error 
   flag; error_in_shlib_symtab will be 1 if an error occurs in this function 
   and 0 otherwise.  */

static int
pa64_solib_sizeof_symbol_table (filename)
     char *filename;
{
  bfd *abfd;
  int i;
  int desc;
  char *absolute_name;
  int st_size = 0;

  /* We believe that filename was handed to us by the dynamic linker, and
     is therefore always an absolute path.  */
  desc = openp (getenv ("PATH"), 1, filename, O_RDONLY | O_BINARY,
		0, &absolute_name);
  if (desc < 0)
    {
      error_in_shlib_symtab=1;
      perror_with_name (filename);
    }
  filename = absolute_name;

  abfd = bfd_fdopenr (filename, gnutarget, desc);
  if (!abfd)
    {
      close (desc);
      make_cleanup (free, filename);
      error_in_shlib_symtab=1;
      error ("\"%s\": can't open to read symbols: %s.", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

  if (!bfd_check_format (abfd, bfd_object))
    {
      bfd_close (abfd);
      make_cleanup (free, filename);
      error_in_shlib_symtab=1;
      error ("\"%s\": can't read symbols: %s.", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

  /* Sum the sizes of the various sections that compose debug info. */
  for (i = 0; pa64_debug_section_names[i] != NULL; i++)
    {
      asection *sect;

      sect = bfd_get_section_by_name (abfd, pa64_debug_section_names[i]);
      if (sect)
	st_size += (int) bfd_section_size (abfd, sect);
    }

  bfd_close (abfd);
  free (filename);

  /* Unfortunately, just summing the sizes of various debug info
     sections isn't a very accurate measurement of how much heap
     space the debugger will need to hold them.  It also doesn't
     account for space needed by linker (aka "minimal") symbols.

     Anecdotal evidence suggests that just summing the sizes of
     debug-info-related sections understates the heap space needed
     to represent it internally by about an order of magnitude.

     Since it's not exactly brain surgery we're doing here, rather
     than attempt to more accurately measure the size of a shlib's
     symbol table in GDB's heap, we'll just apply a 5x fudge-
     factor to the debug info sections' size-sum.  No, this doesn't
     account for minimal symbols in non-debuggable shlibs.  But it
     all roughly washes out in the end.  */
  return st_size * 5;
}

/* Add a shared library to the objfile list and load its symbols into
   GDB's symbol table.  */
static void
pa64_solib_add_solib_objfile (so, from_tty, threshold_exceeded)
     struct so_list *so;
     int from_tty;
     boolean threshold_exceeded;
{
  bfd *tmp_bfd;
  asection *sec;
  obj_private_data_t *obj_private;
  struct section_addr_info section_addrs;

  memset (&section_addrs, 0, sizeof (section_addrs));
  /* JYG: MERGE FIXME: this is a hack using fixed indices into
     the new section_addrs structure */
  /*
  section_addrs.text_addr = so->pa64_solib_desc.text_base;
  section_addrs.data_addr = so->pa64_solib_desc.data_base;
  section_addrs.bss_addr = so->pa64_solib_desc.data_base;
  */
  section_addrs.other[0].addr = so->pa64_solib_desc.text_base; 
  section_addrs.other[0].name = ".text";
  section_addrs.other[1].addr = so->pa64_solib_desc.data_base; 
  section_addrs.other[1].name = ".data";
  section_addrs.other[2].addr = so->pa64_solib_desc.data_base; 
  section_addrs.other[2].name = ".bss";
  
  /* We need the BFD so that we can look at its sections.  We open up the
     file temporarily, then close it when we are done.  */
  tmp_bfd = symfile_bfd_open (so->name);
  if (tmp_bfd == NULL)
    {
      perror_with_name (so->name);
      return;
    }

  if (!bfd_check_format (tmp_bfd, bfd_object))
    {
      bfd_close (tmp_bfd);
      error ("\"%s\" is not an object file: %s", so->name,
             bfd_errmsg (bfd_get_error ()));
    }


  /* Undo some braindamage from symfile.c.

     First, symfile.c will subtract the VMA of the first .text section
     in the shared library that it finds.  Undo that.

     JYG: symfile.c will also subtract the VMA of the first .data / .bss
     section in the shared library that it finds.  Also pre-adjust
     to offset the 'braindamage' */
  sec = bfd_get_section_by_name (tmp_bfd, ".text");
  if (sec)
    section_addrs.other[0].addr += bfd_section_vma (tmp_bfd, sec);
  section_addrs.other[0].addr -= elf_text_start_vma (tmp_bfd);
  
  sec = bfd_get_section_by_name (tmp_bfd, ".data");
  if (sec)
    section_addrs.other[1].addr += bfd_section_vma (tmp_bfd, sec);
  section_addrs.other[1].addr -= elf_data_start_vma (tmp_bfd);
  
  sec = bfd_get_section_by_name (tmp_bfd, ".bss");
  if (sec)
    section_addrs.other[2].addr += bfd_section_vma (tmp_bfd, sec);
  section_addrs.other[2].addr -= elf_data_start_vma (tmp_bfd);

  /* We are done with the temporary bfd.  Get rid of it and make sure
     nobody else can us it.  */
  bfd_close (tmp_bfd);
  tmp_bfd = NULL;

  /* Now let the generic code load up symbols for this library.  */
  so->objfile = maybe_symbol_file_add (so->name, from_tty, &section_addrs, 0,
				       OBJF_SHARED, threshold_exceeded);

  so->objfile->text_low = so->pa64_solib_desc.text_base;
  so->objfile->text_high =  
	       so->pa64_solib_desc.text_base
	     + so->pa64_solib_desc.text_size;
  so->objfile->data_start = so->pa64_solib_desc.data_base;
  so->objfile->data_size = so->pa64_solib_desc.data_size;

  so->objfile->saved_dp_reg = so->pa64_solib_desc.linkage_ptr;
  so->objfile->is_archive_bound = 0;
  so->abfd = so->objfile->obfd;

  /* Mark this as a shared library and save private data.
   */
  so->objfile->flags |= OBJF_SHARED;

  if (so->objfile->obj_private == NULL)
    {
      obj_private = (obj_private_data_t *)
	obstack_alloc (&so->objfile->psymbol_obstack,
		       sizeof (obj_private_data_t));
      obj_private->unwind_info = NULL;
      obj_private->opd = NULL;
      obj_private->so_info = NULL;
      so->objfile->obj_private = (PTR) obj_private;
    }

  obj_private = (obj_private_data_t *) so->objfile->obj_private;
  obj_private->so_info = so;

  if (batch_rtc)
    {
      /* Store the text start and end addresses of librtc.so and libpthread.so
         so that these addresses can be compared against when we display the
         backtrace at event occurrence for batch mode thread check. */
      /* FIXFIX: Check if we can enable this only for the thread check case.
         trace_threads_in_this_run is not set at this point. */
      char *base_name = basename (so->name);
      if (0 == strncmp (base_name, "librtc", 6))
        mark_librtc_start_end (so->objfile->text_low,
                               so->objfile->text_high);
      else if (0 == strncmp (base_name, "libpthread", 10))
        mark_libpthread_start_end (so->objfile->text_low,
                                   so->objfile->text_high);
    }

}

/* Load debugging information for a shared library.  TARGET may be
   NULL if we are not attaching to a process or reading a core file.  */

static void
pa64_solib_load_symbols (so, from_tty, target)
     struct so_list *so;
     boolean from_tty;
     struct target_ops *target;
{
  char buf[4];
  int status;
  struct section_table *p;
  CORE_ADDR text_addr = so->pa64_solib_desc.text_base;

#ifdef SOLIB_DEBUG
  printf ("--Adding symbols for shared library \"%s\"\n", name);
#endif

  pa64_solib_add_solib_objfile (so, from_tty, 0);

  /* Now we need to build a section table for this library since
     we might be debugging a core file from a dynamically linked
     executable in which the libraries were not privately mapped.  */
  if (build_section_table (so->abfd,
			   &so->sections,
			   &so->sections_end))
    {
      error ("Unable to build section table for shared library.");
      return;
    }

/* JAGad89789 -  For core files detect if there is a mismatch between the libraries
   * on the system and the ones in the core. */
   
  if (target_has_stack && !target_has_execution)
    hppa_core_so_mismatch_detection (so); 

  /* Relocate all the sections based on where they got loaded.  */
  for (p = so->sections; p < so->sections_end; p++)
    {
      if (p->the_bfd_section->flags & SEC_CODE)
	{
	  p->addr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_TEXT (so->objfile));
	  p->endaddr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_TEXT (so->objfile));
	}
      else if (p->the_bfd_section->flags & SEC_DATA)
	{
	  p->addr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_DATA (so->objfile));
	  p->endaddr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_DATA (so->objfile));
	}
    }

  /* Now see if we need to map in the text and data for this shared
     library (for example debugging a core file which does not use
     private shared libraries.). 

     Carefully peek at the first text address in the library.  If the
     read succeeds, then the libraries were privately mapped and were
     included in the core dump file.

     If the peek failed, then the libraries were not privately mapped
     and are not in the core file, we'll have to read them in ourselves. */

  status = target_read_memory (text_addr, buf, 4);

  /* RM: for good measure, try and read the last byte of the shared
     library too. There can be a weird core file in which the
     regions from libc.2 and libcl.2 overlap somewhat. There's not
     much we can do about the overlapping region, but we should at
     least try to handle things outside the overlap */

  if (status == 0)
    {
      status = target_read_memory (so->pa64_solib_desc.text_base +
				   so->pa64_solib_desc.text_size - 4,
				   buf, 4);
    }
  if (status != 0 && target == 0)
    {
      if (hp_wdb_testsuite)
	return;  /* Suppress warning message for test suite */

      warning ("Error encountered reading starting text of shared library");
      return;
    }

  if (status != 0)
    {
      int old, new;

      new = so->sections_end - so->sections;

      old = target_resize_to_sections (target, new);

      /* Copy over the old data before it gets clobbered.  */
      memcpy ((char *) (target->to_sections + old),
	      so->sections,
	      ((sizeof (struct section_table)) * new));
    }
}

/* Add symbols from shared libraries into the symtab list, unless the
   size threshold (specified by auto_solib_add, in megabytes) would
   be exceeded.

   This routine is called when gdb attaches to a running program or when
   it has a core file.  It uses dlgetmodinfo and dlgetname to get 
   the load module descriptor and name of each library and then calls
   add_to_solist.

   Parameters:
   arg_string   - NULL.  In the PA64 implementatation, the
   sharedlibrary command does not call this routine.
   See pa64_solib_sharedlibrary_command.

   from_tty     - 1 if the call is the result of a user command
   such as sharedlibrary.  Presumably 0.

   target               - array of target operation routines;  NULL if
   we are working with an inferior process.  
   &current_target if called for attach or
   a core file. */

void
pa64_solib_add (arg_string, from_tty, target)
     char *arg_string;
     int from_tty;
     struct target_ops *target;
{
  asection *shlib_info;
  int dll_index;
  struct load_module_desc dll_desc;
  char *dll_path;
  int gdb_shlib_list_index = 0;
  args_for_add_to_solist args;

  /* In PA64, pa64_solib_sharedlibrary_command does not call pa64_solib_add.
     arg_string should be NULL.
   */
  assert (arg_string == NULL);

  /* If we're debugging a core file, or have attached to a running
     process, then pa64_solib_create_inferior_hook will not have been
     called.

     We need to first determine if we're dealing with a dynamically
     linked executable.  If not, then return without an error or warning.

     We also need to examine __dld_flags to determine if the shared library
     list is valid and to determine if the libraries have been privately
     mapped.  */
  if (symfile_objfile == NULL)
    return;

  /* First see if the objfile was dynamically linked.  */
  shlib_info = bfd_get_section_by_name (symfile_objfile->obfd, ".dynamic");
  if (!shlib_info)
    return;

  /* It's got a .dynamic section, make sure it's not empty.  */
  if (bfd_section_size (symfile_objfile->obfd, shlib_info) == 0)
    return;

  /* Read in the load map pointer if we have not done so already.  */
  if (! dld_cache.have_read_dld_descriptor)
    if (! read_dld_descriptor (target))
      return;

   /* Bindu 081503: Don't do this during attach. Otherwise, it will cause gdb
      to ignore library loading immediately after attach. */

   /* JAGaf15615 */
  if (!attach_flag && !rtc_initialized && check_heap_in_this_run && target_has_stack)
    snoop_on_the_heap ();

  /* srikanth, JAGaa93423, 990416, Alert the user if the shared libraries
     are not mapped private.  
   */

  /* If the libraries were not mapped private, warn the user. Warning should be displayed for running processes and not for corefiles - JAGaf08125 */
  if ((!batch_rtc) && (target_has_execution) && ((dld_cache.dld_flags & DT_HP_DEBUG_PRIVATE) == 0))
    if (profile_on)
      {
        /* We used to exit here with an error message. This was because
           command line calls didn't work quite well when shared libraries
           are not privately mapped. But with prospect v2.6, we don't
           rely on command line calls any more, so I got rid of the
           block here. The `if' is still needed to avoid the warning
           message below which is not useful for profiling mode.

                     - Srikanth, Nov 16th 2005.
        */
      }
    else
      {	
        printf_unfiltered ("\n");
        warning ("The shared libraries were not privately mapped; setting a\nbreakpoint in a shared library will not work until you rerun the program; stepping over longjmp calls will not work as expected.\n");

      }

  /* For each shaerd library, add it to the shared library list.  */
  for (dll_index = 1; ; dll_index++)
    {
      /* Read in the load module descriptor.  */
      if (dlgetmodinfo1 (dll_index, &dll_desc, sizeof (dll_desc),
			pa64_target_read_memory, 0, dld_cache.load_map)
	  == 0)
	return;

      /* Get the name of the shared library.  */
      dll_path = (char *)dlgetname (&dll_desc, sizeof (dll_desc),
			    pa64_target_read_memory,
			    0, dld_cache.load_map);

      /* srikanth, JAGab25944, 001117. If gdb_shlib_path_list is defined and
	 we are debugging core files, treat that directory as the place
	 where the library will be found. This is useful for analyzing
	 core files on a system other than the one that produced the core

	 stacey, JAGab25944, 6/29/2001
	 the for loop below enhances gdb to search multiple directories
	 for the appropriately named shared library.  The gdb_shlib_path_list
	 variable is a pointer to an array of path names.  Which are obtained
	 from the user's GDB_SHLIB_PATH environment variable.  GDB will search
	 the paths in the order in which they are entered by the user: from
	 first to last.  So if the user types:

	 export GDB_SHLIB_PATH=libs1:libs2:libs3
	 
	 GDB will search libs1, then libs2, and finally libs3 and will stop
	 as soon as it finds the library it wants.  Note: this implementation
	 assumes that all library names are unique. */

      if (gdb_shlib_path_list && !target_has_execution)
	{
	  static char altname[MAXPATHLEN];
	  struct stat statbuf;

	  for (gdb_shlib_list_index = 0;
	       gdb_shlib_list_index < gdb_shlib_path_length;
	       gdb_shlib_list_index++)
	    {
	      sprintf (altname, "%s%s", 
		       gdb_shlib_path_list[gdb_shlib_list_index], 
		       strrchr (dll_path, '/'));

	      if (stat(altname, &statbuf) != -1)
		{
		  dll_path = altname;
		  break;
		}
	    }
	}
    else
    if (gdb_shlib_root && !target_has_execution)
      {
        static char altname[MAXPATHLEN];
        int status;
        struct stat statbuf;

	/* RM: If we find this library in gdb_shlib_root _and_ we are debugging
	   core files, use that version instead. Useful when analyzing core
	   files on a system other than the one that produced the core */
	strcpy(altname, gdb_shlib_root);
	strcat(altname, dll_path);
	status = stat(altname, &statbuf);
	if (status != -1)
	  {
            dll_path = altname;
          }
      }

      if (!dll_path)
        {
	  /* FIXME - we are currently getting a problem which libraries
	     that were loaded with shl_load and then unloaded with shl_unload.
	     They should not be in the list, but dlgetmodinfo is returning
	     a descriptor but dlgetname is not finding the name.  We will
	     just continue past this one until dlgetmodinfo returns NULL 
	     instead of generating an error. */
	  /* error ("pa64_solib_add, error reading shared library path."); */
	  continue;
	}
     
     /* If a core is being loaded, store the name of the library used in the core. */
       if (target_has_stack && !target_has_execution)
	  so_pathname_g = dll_path;

      args.from_tty = from_tty;
      args.dll_path = dll_path;
      args.load_module_desc_p = &dll_desc;
      args.load_module_desc_addr = 0;
      args.target = target;
      if (!catch_errors ((catch_errors_ftype *) add_to_solist_1,
                                        (char *) &args,
                                        NULL, RETURN_MASK_ALL))
        {
          warning ("Error in reading symbols from %s... skipping\n", dll_path);
        }

    }
}

/* This hook gets called just before the first instruction in the
   inferior process is executed.

   This is our opportunity to set magic flags in the inferior so
   that GDB can be notified when a shared library is mapped in and
   to tell the dynamic linker that a private copy of the library is
   needed (so GDB can set breakpoints in the library).

   We need to set two flag bits in this routine.

     DT_HP_DEBUG_PRIVATE to indicate that shared libraries should be
     mapped private.

     DT_HP_DEBUG_CALLBACK to indicate that we want the dynamic linker to
     call the breakpoint routine for significant events.  */

void
pa64_solib_create_inferior_hook ()
{
  struct minimal_symbol *msymbol;
  unsigned int dld_flags, status, have_endo;
  asection *shlib_info;
  char shadow_contents[BREAKPOINT_MAX], buf[sizeof (CORE_ADDR)];
  struct objfile *objfile;
  CORE_ADDR anaddr;
#ifdef HPPA_FIX_AND_CONTINUE
  extern boolean get_reapply_fix (void);
  extern void create_bp_fix_event_breakpoint (CORE_ADDR);
#endif

  /* First, remove all the solib event breakpoints.  Their addresses
     may have changed since the last time we ran the program.  */
  remove_solib_event_breakpoints ();

  if (symfile_objfile == NULL)
    return;

  /* First see if the objfile was dynamically linked.  */
  shlib_info = bfd_get_section_by_name (symfile_objfile->obfd, ".dynamic");
  if (!shlib_info)
    return;

  /* It's got a .dynamic section, make sure it's not empty.  */
  if (bfd_section_size (symfile_objfile->obfd, shlib_info) == 0)
    return;

#ifdef RTC  
  /* srikanth, 000328, If we are running in RTC mode, create an
     internal breakpoint in main. This will be the point at which
     gdb will intercept calls to malloc and free. We were intercepting
     earlier than this, just after libc.sl and librtc.sl are mapped in
     but this causes problems in PA64. If the C library initialization
     code calls malloc, (the newer versions of C libraries do) then
     control winds up in librtc, even before the thread subsystem is
     initialized. CR27 is not initialized properly at this point
     and this leads a segfault when librtc.sl references its TLS. */
     
  if (check_heap_in_this_run)
    {
      struct minimal_symbol * m;
      m = lookup_minimal_symbol ("main", 0, symfile_objfile);
      if (m)
        create_solib_event_breakpoint (SYMBOL_VALUE_ADDRESS(m));
    }
#endif /* RTC */
  have_endo = 0;

  /* Stacey - September 21st 2001
     The following code and comments were taken from gdb32
     somsolib.c */

  /* RM: Not sure we actually need to write to __d_pid for
   * shl_load/shl_unload tracking to work: DDE seems to manage fine
   * without it. It's useful to debug shared libraries even when the
   * executable doesn't have end.o linked in.  */

#if 0  
  /* Slam the pid of the process into __d_pid; failing is only a warning!  */
  msymbol = lookup_minimal_symbol ("__d_pid", NULL, symfile_objfile);
  if (msymbol == NULL)
    {
      warning ("Unable to find __d_pid symbol in object file.");
      warning ("Suggest linking with /opt/langtools/lib/end.o.");
      goto keep_going;
    }
  
  anaddr = SYMBOL_VALUE_ADDRESS (msymbol);
  store_unsigned_integer (buf, sizeof (inferior_pid), inferior_pid);
  status = target_write_memory (anaddr, buf, sizeof (inferior_pid));
  if (status != 0)
    {
      warning ("Unable to write __d_pid");
      warning ("Suggest linking with /opt/langtools/lib/end.o.");
      goto keep_going;
    }
#endif

keep_going:

  /* Read in the .dynamic section.  */
  if (! read_dynamic_info (shlib_info, &dld_cache))
    error ("Unable to read the .dynamic section.");

  /* Turn on the flags we care about.  */

  if (!info_hp_aries_pa64)
    {
/* The mapped private bit should be set explicitly by the debugger only
   when the a.out is started under the debugger and not in an attach case.
   Also not when the -mapshared option is on.  */
     if (!attach_flag && !mapshared)
        dld_cache.dld_flags |= DT_HP_DEBUG_PRIVATE;

      dld_cache.dld_flags |= DT_HP_DEBUG_CALLBACK;
      status = target_write_memory (dld_cache.dld_flags_addr,
				    (char *) &dld_cache.dld_flags,
				    sizeof (dld_cache.dld_flags));
      if (status != 0)
	error ("Unable to modify dynamic linker flags.");
    }

  /* There used to be code here in the PA32 version to set a breakpoint
     at _start.  Two reasons were given and neither is true for PA64, so
     the breakpoint isn't being set.  The reasons were:

     * Not all sites have /opt/langtools/lib/end.o, so it's not always
     possible to track the dynamic linker's events.

     * At this time no events are triggered for shared libraries
     loaded at startup time (what a crock).  */
#ifdef HPPA_FIX_AND_CONTINUE
  if (get_reapply_fix())
    create_bp_fix_event_breakpoint (bfd_get_start_address (symfile_objfile->obfd));
#endif

  /* Wipe out all knowledge of old shared libraries since their
     mapping can change from one exec to another!  */
  while (so_list_head)
    {
      struct so_list *temp;

      temp = so_list_head->next;
      free (so_list_head);
      so_list_head = temp;
    }
  so_list_tail = NULL;
  solib_clear_symtab_users ();
}

/* This operation removes the "hook" between GDB and the dynamic linker,
   which causes the dld to notify GDB of shared library events.

   After this operation completes, the dld will no longer notify GDB of
   shared library events.  To resume notifications, GDB must call
   pa64_solib_create_inferior_hook.

   This operation does not remove any knowledge of shared libraries which
   GDB may already have been notified of.  */

void
pa64_solib_remove_inferior_hook (pid)
     int pid;
{
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;
  
  /* Turn off the DT_HP_DEBUG_CALLBACK bit in the dynamic linker flags.  */
  dld_cache.dld_flags &= ~DT_HP_DEBUG_CALLBACK;
  /* Support for PA64 GDB to step into BOR calls. */
  dld_cache.dld_flags &= ~DT_HP_DEBUG_CALLBACK_BOR;
  target_write_memory (dld_cache.dld_flags_addr,
		       (char *)&dld_cache.dld_flags,
		       sizeof (dld_cache.dld_flags));
  
  inferior_pid = saved_inferior_pid;
}

/* Saravanan, Sep 11th 2002, new function to enable DT_HP_DEBUG_CALLBACK
   flag after a vfork. Currently we unrequest shl_load callbacks after vfork
   (for the child process), but since the child and the parent share
   the same address space, we end up not getting shl_load notifications
   for the parent also : OIOW, gdb is blissfully unaware of any shl_loads
   made by the vforking parent process.
*/
void
pa64_solib_adjust_inferior_hook (pid)
     int pid;
{
  int saved_inferior_pid = inferior_pid;
  inferior_pid = pid;
  
  /* Turn on the DT_HP_DEBUG_CALLBACK bit in the dynamic linker flags.  */
  dld_cache.dld_flags |= DT_HP_DEBUG_CALLBACK;
  target_write_memory (dld_cache.dld_flags_addr,
		       (char *)&dld_cache.dld_flags,
		       sizeof (dld_cache.dld_flags));
  
  inferior_pid = saved_inferior_pid;
}


/* Routine called by pa64_solib_create_catch_load_hook and
   pa64_solib_create_catch_load_hook to do the work.  

   tempflag     1 for tcatch command.
   filename     NULL if all events are to be caught, or the path of the file
   otherwise to just catch one file.
   event_list_pp        Either &catch_load_list or &catch_unload_list.
   The new event is addes to the indicated list.
   type         Either "load" or "unload" */

static void
pa64_solib_create_catch_load_unload_hook (tempflag,
					  cond_string,
					  filename,
					  event_list_pp,
					  type)
     int tempflag;
     char *cond_string;
     char *filename;
     catch_load_unload_event_t **event_list_pp;
     const char *type;
{
  catch_load_unload_event_t *new_event;
  catch_load_unload_event_t *last_event;

  new_event = (catch_load_unload_event_t *)
    xmalloc (sizeof (catch_load_unload_event_t));
  memset ((char *) new_event, 0, sizeof (catch_load_unload_event_t));

  new_event->filename = filename ? savestring (filename, strlen (filename))
    : NULL;
  if (*event_list_pp)
    {
      last_event = *event_list_pp;
      while (last_event->next)
	last_event = last_event->next;
      last_event->next = new_event;
    }
  else
    {
      *event_list_pp = new_event;
    }

  if (strcmp ("load", type) == 0)
    {
      create_solib_load_event_breakpoint ("", tempflag, filename, cond_string,
					  new_event);
    }
  else
    {
      create_solib_unload_event_breakpoint ("", tempflag, filename,
					    cond_string, new_event);
    }
  assert (new_event->breakpt);
  assert (new_event->breakpt->dynlink_event == (void *) new_event);
  assert (new_event->breakpt->type == bp_catch_load
	  || new_event->breakpt->type == bp_catch_unload);
}

/* This function adds something to the end of the catch_load_list.
   If the filename is NULL, then loads of any dll will be caught.  Else,
   only loads of the file whose pathname is the string contained by
   filename will be caught.

   Only file filename argument is used. */

void
pa64_solib_create_catch_load_hook (pid, tempflag, filename, cond_string)
     int pid;
     int tempflag;
     char *filename;
     char *cond_string;
{
  pa64_solib_create_catch_load_unload_hook (tempflag,
					    cond_string,
					    filename,
					    &catch_load_list,
					    "load");
}

/* This function adds something to the end of the catch_unload_list.
   If the filename is NULL, then unloads of any dll will be caught.  Else,
   only unloads of the file whose pathname is the string contained by
   filename will be caught.

   Only file filename argument is used. */

void
pa64_solib_create_catch_unload_hook (pid, tempflag, filename, cond_string)
     int pid;
     int tempflag;
     char *filename;
     char *cond_string;
{
  pa64_solib_create_catch_load_unload_hook (tempflag,
					    cond_string,
					    filename,
					    &catch_unload_list,
					    "unload");
}

/* Return 1 if the breakpoint is the dld.sl breakpoint for notifying
   the debugger about events. */

boolean
pa64_solib_at_dynlink_hook (int pid, CORE_ADDR stop_pc)
{
  unsigned int instruction;
  int status;

  /* If the instruction is no "BREAK 4,8", we presume that this is not
     the break instruction in dld.sl */

  /* RM: Don't assume that you can read every instruction we stop
     at. Check for it */
  status = target_read_memory (stop_pc, (char *) &instruction, 4);
  if (status != 0)
    return 0;
  if (instruction != BREAKPOINT32)
    return 0;

  /* If the instruction is "BREAK 4,8" and the address is in dld, we presume
     this is the break instruction in dld.sl */

  return pa64_solib_in_dynamic_linker (pid, stop_pc);
}

/* Return nonzero if the dynamic linker has reproted that a library
   has been loaded.  */

int
pa64_solib_have_load_event (pid)
     int pid;
{
  CORE_ADDR event_kind;
  CORE_ADDR stop_pc_pid;

  stop_pc_pid = read_pc_pid (pid);
  if (!pa64_solib_at_dynlink_hook (pid, stop_pc))
    return 0;

  event_kind = read_register (ARG0_REGNUM);
  return (event_kind == DLD_CB_LOAD);
}

/* Return nonzero if the dynamic linker has reproted that a library
   has been unloaded.  */
int
pa64_solib_have_unload_event (pid)
     int pid;
{
  CORE_ADDR event_kind;
  CORE_ADDR stop_pc_pid;

  stop_pc_pid = read_pc_pid (pid);
  if (!pa64_solib_at_dynlink_hook (pid, stop_pc))
    return 0;

  event_kind = read_register (ARG0_REGNUM);
  return (event_kind == DLD_CB_UNLOAD);
}

/* Return a pointer to a string indicating the pathname of the most
   recently loaded library.

   The caller is reposible for copying the string before the inferior is
   restarted.  */

char *
pa64_solib_loaded_library_pathname (pid)
     int pid;
{
  CORE_ADDR stop_pc_pid;
  CORE_ADDR dll_path_addr;
  static char dll_path[MAXPATHLEN];

  stop_pc_pid = read_pc_pid (pid);
  if (!pa64_solib_at_dynlink_hook (pid, stop_pc))
    return "";

  dll_path_addr = read_register (ARG3_REGNUM);
  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);
  return dll_path;
}

/* Return a pointer to a string indicating the pathname of the most
   recently unloaded library.

   The caller is reposible for copying the string before the inferior is
   restarted.  */

char *
pa64_solib_unloaded_library_pathname (pid)
     int pid;
{
  CORE_ADDR stop_pc_pid;
  CORE_ADDR dll_path_addr;
  static char dll_path[MAXPATHLEN];

  stop_pc_pid = read_pc_pid (pid);
  if (!pa64_solib_at_dynlink_hook (pid, stop_pc))
    return "";

  dll_path_addr = read_register (ARG3_REGNUM);
  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);
  return dll_path;
}

/* Return nonzero if PC is an address inside the dynamic linker.  */

int
pa64_solib_in_dynamic_linker (pid, pc)
     int pid;
     CORE_ADDR pc;
{
  asection *shlib_info;

  if (symfile_objfile == NULL)
    return 0;

  if (!dld_cache.have_read_dld_descriptor)
    if (!read_dld_descriptor (&current_target))
      return 0;

  return (pc >= dld_cache.dld_desc.text_base
	  && pc < dld_cache.dld_desc.text_base + dld_cache.dld_desc.text_size);
}


/* Return the GOT value for the shared library in which ADDR belongs.  If
   ADDR isn't in any known shared library, return zero.  */

CORE_ADDR
pa64_solib_get_got_by_pc (addr)
     CORE_ADDR addr;
{
  struct so_list *so_list = so_list_head;
  CORE_ADDR got_value = 0;

  while (so_list)
    {
      if (so_list->pa64_solib_desc.text_base <= addr
	  && ((so_list->pa64_solib_desc.text_base
	       + so_list->pa64_solib_desc.text_size)
	      > addr))
	{
	  got_value = so_list->pa64_solib_desc.linkage_ptr;
	  break;
	}
      so_list = so_list->next;
    }
  return got_value;
}

/* Return the address of the handle of the shared library in which ADDR
   belongs.  If ADDR isn't in any known shared library, return zero. 

   This function is used in hppa_fix_call_dummy in hppa-tdep.c.  */

CORE_ADDR
pa64_solib_get_solib_by_pc (addr)
     CORE_ADDR addr;
{
  struct so_list *so_list = so_list_head;
  CORE_ADDR retval = 0;

  while (so_list)
    {
      if (so_list->pa64_solib_desc.text_base <= addr
	  && ((so_list->pa64_solib_desc.text_base
	       + so_list->pa64_solib_desc.text_size)
	      > addr))
	{
	  retval = so_list->pa64_solib_desc_addr;
	  break;
	}
      so_list = so_list->next;
    }
  return retval;
}

/* Dump information about all the currently loaded shared libraries.  */

static void
pa64_sharedlibrary_info_command (ignore, from_tty)
     char *ignore;
     int from_tty;
{
  struct so_list *so_list = so_list_head;
  unsigned int num_shlibs = 0;

  if (exec_bfd == NULL)
    {
      printf_unfiltered ("No executable file.\n");
      return;
    }

  if (so_list == NULL)
    {
      printf_unfiltered ("No shared libraries loaded at this time.\n");
      return;
    }
  
  printf_unfiltered ("Shared Object Libraries\n");
  printf_unfiltered ("   %s\n   %s\n   %-19s%-19s%-19s%-19s\n\n",
		     "  Library Path", "  dlt", 
		     "  text start", "   text end",
		     "  data start", "   data end");

  while (so_list)
    {
      unsigned int flags;

      printf_unfiltered ("%s", so_list->name);
      if (so_list->objfile == NULL)
	printf_unfiltered ("  (symbols not loaded)");
      if (so_list->loaded == 0)
	printf_unfiltered ("  (shared library unloaded)");

      /* Stacey August 27th 2001 - 
	 If users are interested in such things like the tls start address 
	 etc, we need to revisit this function. */
       
      printf_unfiltered ("\n%-18s",
	longest_local_hex_string_custom (so_list->pa64_solib_desc.linkage_ptr,
				 "016l"));
      printf_unfiltered ("\n");
      printf_unfiltered ("%-18s",
        longest_local_hex_string_custom (so_list->pa64_solib_desc.text_base,
				 "016l"));
      printf_unfiltered (" %-18s",
	longest_local_hex_string_custom ((so_list->pa64_solib_desc.text_base
				  + so_list->pa64_solib_desc.text_size),
				 "016l"));
      printf_unfiltered (" %-18s",
	longest_local_hex_string_custom (so_list->pa64_solib_desc.data_base,
				 "016l"));
      printf_unfiltered (" %-18s\n",
	longest_local_hex_string_custom ((so_list->pa64_solib_desc.data_base
				  + so_list->pa64_solib_desc.data_size),
				 "016l"));
      printf_unfiltered ("\n");
      so_list = so_list->next;
      num_shlibs++;
    }
    printf_unfiltered ("Total of %d shared libraries.\n", num_shlibs);
}

/* Load up one or more shared libraries as directed by the user.  */

static void
pa64_solib_sharedlibrary_command (args, from_tty)
     char *args;
     int from_tty;
{
  extern int breakpoints_inserted;
  struct so_list *so_list;

  dont_repeat ();

  if ((re_comp (args ? args : ".")) != NULL)
    {
      error ("Invalid regexp: '%s'", args);
    }

  /* Go through the list of libraries.  If a library matches the args pattern
     and we haven't loaded its debug info, load the info now.
   */
  for (so_list = so_list_head; so_list; so_list = so_list->next)
    {
      if ((so_list->objfile) && (so_list->objfile->psymtabs != NULL))
	continue;		/* debug info already loaded */

      if (!re_exec (so_list->name))
	continue;		/* Name does not match regular expression. */

      printf_unfiltered ("Loading  %s\n", so_list->name);

      pa64_solib_total_st_size +=
	pa64_solib_sizeof_symbol_table (so_list->name);

      if (so_list->objfile == NULL)
	{
	  pa64_solib_load_symbols (so_list,
				   from_tty,
				   (struct target_ops *) 0);
	}
      else if ((so_list->objfile != NULL) &&
	       (so_list->objfile->psymtabs == NULL) &&
	       (so_list->objfile->sf->sym_add_psymtabs != NULL))
	{
	  /* RM: we now do a fair amount of processing (reading
	     unwind info, linker symbol table) even if the
	     threshold is exceeded. We only want to read in the
	     actual debug information when we come here, the
	     rest should already have been done */
	  printf ("--Adding symbols for shared library \"%s\"\n",
		  so_list->name);
	  (*so_list->objfile->sf->sym_add_psymtabs)
	    (so_list->objfile, 0);
	}
    }				/* end for so_list */

  re_enable_breakpoints_in_shlibs ();	/* Fixed CLLbs16090 */
  if (breakpoints_inserted)
    {
      remove_breakpoints ();
      insert_breakpoints ();
    }
}


boolean pa64_this_solib_address (void *solib, CORE_ADDR addr)
{
  struct so_list *so = (struct so_list *) solib;

  /* Is this address within this shlib's text range? */
  return ((addr >= so->pa64_solib_desc.text_base)
          && (addr < so->pa64_solib_desc.text_base
              + so->pa64_solib_desc.text_size));
}


boolean pa64_loaded_solib_address (CORE_ADDR addr)
{
  struct so_list *so = so_list_head;
  while (so)
   {
     if (pa64_this_solib_address((void*) so, addr) && so->loaded) 
       return true;
     so = so->next;
   }

  if (addr >= text_start && addr < text_end) 
    return true;

  return false;
}


/* Return the name of the shared library containing ADDR or NULL if ADDR
   is not contained in any known shared library.  */

char *
pa64_solib_address (addr)
     CORE_ADDR addr;
{
  struct so_list *so = so_list_head;

  while (so)
    {
      /* Is this address within this shlib's text range?  If so,
	 return the shlib's name.  */
      if (addr >= so->pa64_solib_desc.text_base
	  && addr < (so->pa64_solib_desc.text_base
		     + so->pa64_solib_desc.text_size))
	return so->name;

      /* Nope, keep looking... */
      so = so->next;
    }

  /* No, we couldn't prove that the address is within a shlib. */
  return NULL;
}

/* JAGaf03565
   Checks the text low and text high of all shared libraries to find
   if a given address is present in a shared library.

   If not found it checks the main executable to see if address is part
   of that.

   Returns the name of the load module the address was found in
   else returns NULL.
 */

#ifdef PC_SOLIB
char *
solib_and_mainfile_address (CORE_ADDR addr)
{
  char *so_name = NULL;
  /* Check to see if the address is present in a shared library */
  if ((so_name = PC_SOLIB (addr)) != NULL)
    return so_name;
  else if (symfile_objfile) /* If main exe present */
    {
      CORE_ADDR text_base = 0, text_size = 0, text_high = 0;
      CORE_ADDR data_start = 0, data_end = 0;

      /* We cache the text low and text high for an objfile in it.
         Check cached values first.*/
      if (symfile_objfile->text_low
          && symfile_objfile->text_high)
        {
          text_base = symfile_objfile->text_low;
          text_high = symfile_objfile->text_high;
        }
      /* If the load module descriptor not present yet then look for the
         minimal symbols __text_start and _extext which signify start and
         end of text and cache the values.
       */
      else
        {
          text_base = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
                                            ("__text_start",
                                             0, symfile_objfile));
          text_high = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
                                            ("_etext",
                                             0, symfile_objfile));
	  data_start = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
					    ("__data_start",
					     0, symfile_objfile));
	  data_end = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
					    ("_end",
					     0, symfile_objfile));
          symfile_objfile->text_low = text_base;
          symfile_objfile->text_high = text_high;
	  symfile_objfile->data_start = data_start;
	  symfile_objfile->data_size = data_end - data_start;

        }

      if (text_base && text_high
          && text_base <= addr
          && addr <= text_high)
        return symfile_objfile->name;
    }

  return (0);
}
#endif

/* We are killing the inferior and restarting the program.  */

void
pa64_solib_restart ()
{
  struct so_list *sl = so_list_head;

  /* Before the shlib info vanishes, use it to disable any breakpoints
     that may still be active in those shlibs.  */
  disable_breakpoints_in_shlibs (!info_verbose, NULL);

  /* Discard all the shlib descriptors.  */
  while (sl)
    {
      struct so_list *next_sl = sl->next;
      /* For memory leak - NEC(JAGaf79121) JAG */
      if(sl->sections)
        free(sl->sections);
      free (sl);
      sl = next_sl;
    }
  so_list_head = NULL;
  so_list_tail = NULL;

  pa64_solib_total_st_size = (LONGEST) 0;
  pa64_solib_st_size_threshold_exceeded = 0;

  threshhold_warning_given = 0;
  shlib_load_completed = 0; /* JAGaf35513 */

  dld_cache.is_valid = 0;
  dld_cache.have_read_dld_descriptor = 0;
  dld_cache.is_archive_bound = 0;
  dld_cache.dld_flags_addr = 0;
  dld_cache.load_map = 0;
  dld_cache.load_map_addr = 0;
  dld_cache.dld_desc.data_base = 0;
  dld_cache.dld_flags = 0;
  dld_cache.dyninfo_sect = 0;
}

void
_initialize_pa64_solib ()
{
  struct rlimit rlp;
  char *s, *gdb_shlib_path = NULL, *token;
  int i; 
  
  add_com ("sharedlibrary", class_files, pa64_solib_sharedlibrary_command,
	   "Load shared object library symbols for files matching REGEXP.\n\n\
Usage:\n\tsharedlibrary [<REGEXP>]\n\n");
  add_info ("sharedlibrary", pa64_sharedlibrary_info_command,
	    "Status of loaded shared object libraries.\n\nUsage:\n\tinfo share\
\n\t or\n\tinfo sharedlibrary\n");
  add_show_from_set
    (add_set_cmd ("auto-solib-add", class_support, var_zinteger,
		  (char *) &auto_solib_add,
		  "Set autoloading size threshold (in megabytes) of shared library symbols.\n\n\
Usage:\nTo set new value:\n\tset auto-solib-add <INTEGER>\nTo see current value:\n\tshow \
auto-solib-add\n\n\
If nonzero, symbols from all shared object libraries will be loaded\n\
automatically when the inferior begins execution or when the dynamic linker\n\
informs gdb that a new library has been loaded, until the symbol table\n\
of the program and libraries exceeds this threshold.\n\
Otherwise, symbols must be loaded manually, using `sharedlibrary'.",
		  &setlist),
     &showlist);

  /* ??rehrauer: On HP-UX, the kernel parameter MAXDSIZ limits how much
     data space a process can use.  We ought to be reading MAXDSIZ and
     setting auto_solib_add to some large fraction of that value.  If
     not that, we maybe ought to be setting it smaller than the default
     for MAXDSIZ (that being 64Mb, I believe).  However, [1] this threshold
     is only crudely approximated rather than actually measured, and [2]
     50 Mbytes is too small for debugging gdb itself.  Thus, the arbitrary
     100 figure.
   */
  /* RM: Using MAXDSIZ to set the default threshold now */
  /* auto_solib_add = 100;  */

  getrlimit (RLIMIT_DATA, &rlp);
  auto_solib_add = ((rlp.rlim_cur / 1000000) * 3) / 4;

  /* RM: initialize gdb_shlib_root */
  s = getenv("GDB_SHLIB_ROOT");
  if (s)
    {
      gdb_shlib_root = xmalloc(strlen(s) + 1);
      strcpy(gdb_shlib_root, s);
    }
  else
    {
      gdb_shlib_root = 0;
    }

  /* srikanth, 001117, Consult environment for GDB_SHLIB_PATH and
     if defined use that as the directory where the libraries will
     be found. This is useful for debugging core files produced on
     a different system. (JAGab25944)

     stacey, 6/29/2001 GDB_SHLIB_PATH can be either a single directory
     as mentioned above or a colon separated list of directories where
     the libraries will be found.  If the user enters a colon separated
     list, gdb will search the directories in the order the user has 
     entered them from first to last.  The code below parses the user's
     colon separated text into separated library names and stores them 
     in gdb_shlib_path - a pointer to a dynamically allocated array of 
     directory names (JAGab25944)  */

  s = getenv ("GDB_SHLIB_PATH");
  gdb_shlib_path = s ? strdup (s) : s;

  if (gdb_shlib_path != NULL)
    {
      gdb_shlib_path_length = 1;

      /* At this point, we know the user has entered at least one
	 shared library path.  Count the colons in the gdb_shlib_path 
	 string to find out how many additional directory paths the 
	 user has entered.  */

      for (i = 0; gdb_shlib_path[i] != NULL; i++)
	if (gdb_shlib_path[i] == ':')
	  gdb_shlib_path_length++;      

      /* Now parse the gdb_shlib_path and separate into different directories
	 Treat ':'s as separators */

      gdb_shlib_path_list = xmalloc (gdb_shlib_path_length * sizeof (char *));

      for (token = strtok (gdb_shlib_path, ":"), i=0;
	   token != NULL;
	   token = strtok (NULL, ":"), i++)
	gdb_shlib_path_list[i] = strdup (token);

      free (gdb_shlib_path);
    }
  pa64_solib_restart ();
}

/* Get some HPUX-specific data from a shared lib.  */

/* 
   Dinesh 05/22/02 , the tls_start_addr got from load_module_desc 
   defined in dlfcn.h is unsigned long and not unsigned long long
   for 64 bit .

   Extracted from dlfcn.h
   #ifdef __LP64__
   typedef unsigned long           UINT64;
   #else
   typedef unsigned long long      UINT64;
   #endif 
*/

   
unsigned long
so_lib_thread_start_addr (so)
     struct so_list *so;
{
  return so->pa64_solib_desc.tls_start_addr;
}

unsigned long
so_lib_thread_index (so)
     struct so_list *so;
{
  return so->pa64_solib_desc.tls_index;
}

/* Read the dynamic linker's internal shared library descriptor.

   This must happen after dld starts running, so we can't do it in
   read_dynamic_info.  Record the fact that we have loaded the
   descriptor.  If the library is archive bound, then return zero, else
   return nonzero.  */

static boolean
read_dld_descriptor (target)
     struct target_ops *target;
{
  char *dll_path;
  asection *dyninfo_sect;
  int gdb_shlib_list_index = 0;

  if (dld_cache.is_archive_bound)
    return 0;

  /* If necessary call read_dynamic_info to extract the contents of the
     .dynamic section from the shared library.  */
  if (!dld_cache.is_valid) 
    {
      if (symfile_objfile == NULL)
	error ("No object file symbols.");

      dyninfo_sect = bfd_get_section_by_name (symfile_objfile->obfd, 
					      ".dynamic");
      if (!dyninfo_sect) 
	{
	  dld_cache.is_archive_bound = 1;
	  return 0;
	}

      if (!read_dynamic_info (dyninfo_sect, &dld_cache))
	error ("Unable to read in .dynamic section information.");
    }

  /* Read the load map pointer.  */
  if (target_read_memory (dld_cache.load_map_addr,
			  (char*) &dld_cache.load_map,
			  sizeof(dld_cache.load_map))
      != 0)
    {
      error ("Error while reading in load map pointer.");
    }

  /* Read in the dld load module descriptor */
  if (dlgetmodinfo1 (-1, 
		    &dld_cache.dld_desc,
		    sizeof(dld_cache.dld_desc), 
		    pa64_target_read_memory, 
		    0, 
		    dld_cache.load_map)
      == 0)
    {
      error ("Error trying to get information about dynamic linker.");
    }

  /* Indicate that we have loaded the dld descriptor.  */
  dld_cache.have_read_dld_descriptor = 1;

  /* Add dld.sl to the list of known shared libraries so that we can
     do unwind, etc. 

     ?!? This may not be correct.  Consider of dld.sl contains symbols
     which are also referenced/defined by the user program or some user
     shared library.  We need to make absolutely sure that we do not
     pollute the namespace from GDB's point of view.  */
  dll_path = dlgetname (&dld_cache.dld_desc, 
			sizeof(dld_cache.dld_desc), 
			pa64_target_read_memory, 
			0, 
			dld_cache.load_map);
  dld_name = dll_path;

  /* JAGae37373, If gdb_shlib_path_list is defined and we are debugging core files, then dld.sl 
  will be taken from this path. This is useful for analyzing core files on a system other than 
  the one that produced the core */

  if (gdb_shlib_path_list && !target_has_execution)
     {
	static char altname[MAXPATHLEN];
	struct stat statbuf;

	for (gdb_shlib_list_index = 0;
	gdb_shlib_list_index < gdb_shlib_path_length;
	gdb_shlib_list_index++)
	{
        	sprintf (altname, "%s%s",
		gdb_shlib_path_list[gdb_shlib_list_index],
		strrchr (dll_path, '/'));

		if (stat(altname, &statbuf) != -1)
		{
			dll_path = altname;
			break;
		}
	}
     }
  else
        if (gdb_shlib_root && !target_has_execution)
	{
	  static char altname[MAXPATHLEN];
	  int status;
	  struct stat statbuf;

	   /* RM: If we find this library in gdb_shlib_root 
           _and_ we are debugging core files, use that version 
           instead. Useful when analyzing core files on a system 
           other than the one that produced the core */

 		strcpy(altname, gdb_shlib_root);
	        strcat(altname, dll_path);
	        status = stat(altname, &statbuf);
	  	if (status != -1)
	        {
		  dll_path = altname;
      	        }
	 }

        add_to_solist(0, dll_path,  &dld_cache.dld_desc, 0, target);
 
  	return 1;
}

/* Read the .dynamic section and extract the information of interest,
   which is stored in dld_cache.  The routine elf_locate_base in solib.c 
   was used as a model for this.  */

static boolean
read_dynamic_info (dyninfo_sect, dld_cache_p)
     asection *dyninfo_sect;
     dld_cache_t *dld_cache_p;
{
  char *buf;
  char *bufend;
  CORE_ADDR dyninfo_addr;
  int dyninfo_sect_size;
  CORE_ADDR entry_addr;

  /* Read in .dynamic section, silently ignore errors.  */
  dyninfo_addr = bfd_section_vma (symfile_objfile->obfd, dyninfo_sect);
  dyninfo_sect_size = bfd_section_size (exec_bfd, dyninfo_sect);
  buf = (char *) alloca (dyninfo_sect_size);
  if (target_read_memory (dyninfo_addr, buf, dyninfo_sect_size))
    return 0;

  /* Scan the .dynamic section and record the items of interest. 
     In particular, DT_HP_DLD_FLAGS */
  for (bufend = buf + dyninfo_sect_size, entry_addr = dyninfo_addr;
       buf < bufend;
       buf += sizeof (Elf64_Dyn), entry_addr += sizeof (Elf64_Dyn))
    {
      Elf64_Dyn *x_dynp = (Elf64_Dyn*)buf;
      Elf64_Sxword dyn_tag;
      CORE_ADDR	dyn_ptr;
      char pbuf[TARGET_PTR_BIT / HOST_CHAR_BIT];

      dyn_tag = bfd_h_get_64 (symfile_objfile->obfd, 
			      (bfd_byte*) &x_dynp->d_tag);

      /* We can't use a switch here because dyn_tag is 64 bits and HP's
	 lame comiler does not handle 64bit items in switch statements.  */
      if (dyn_tag == DT_NULL)
	break;
      else if (dyn_tag == DT_HP_DLD_FLAGS)
	{
	  /* Set dld_flags_addr and dld_flags in *dld_cache_p */
	  dld_cache_p->dld_flags_addr = entry_addr + offsetof(Elf64_Dyn, d_un);
	  if (target_read_memory (dld_cache_p->dld_flags_addr,
	  			  (char*) &dld_cache_p->dld_flags, 
				  sizeof(dld_cache_p->dld_flags))
	      != 0)
	    {
	      error ("Error while reading in .dynamic section of the program.");
	    }
	}
      else if (dyn_tag == DT_HP_LOAD_MAP)
	{
	  /* Dld will place the address of the load map at load_map_addr
	     after it starts running.  */
	  if (target_read_memory (entry_addr + offsetof(Elf64_Dyn, 
							d_un.d_ptr),
				  (char*) &dld_cache_p->load_map_addr,
				  sizeof(dld_cache_p->load_map_addr))
	      != 0)
	    {
	      error ("Error while reading in .dynamic section of the program.");
	    }
	}
      else if (dyn_tag == DT_HP_TIME_STAMP)
        {
         /* JAGad89789 - If the dld.sl in the core is old, i.e., it doesn't have the
	   * new capability of dlgetmodinfo(), disable core library mismatch
	   * detection.  Otherwise results will be bogus.
	   */
  	
       /* The last dld with this new functionality was build on Dec 17th, 2003, released in GR0104 patch. */  
  	  
          const int new_dld_date = 0x3fe09e79;
          if (x_dynp->d_un.d_val < new_dld_date)
	    core_so_mismatch_detection_disabled = 1;
         } 
      else  
	{
	  /* tag is not of interest */
	}
    }

  /* Record other information and set is_valid to 1. */
  dld_cache_p->dyninfo_sect = dyninfo_sect;

  /* Verify that we read in required info.  These fields are re-set to zero
     in pa64_solib_restart.  */

  if (dld_cache_p->dld_flags_addr != 0 && dld_cache_p->load_map_addr != 0) 
    dld_cache_p->is_valid = 1;
  else 
    return 0;

  return 1;
}

/* Wrapper for target_read_memory to make dlgetmodinfo happy.  */

static void *
pa64_target_read_memory (buffer, ptr, bufsiz, ident)
     void *buffer;
     CORE_ADDR ptr;
     size_t bufsiz;
     int ident;
{
  if (target_read_memory (ptr, buffer, bufsiz) != 0)
    return 0;
  return buffer;
}

static int
add_to_solist_1 (PTR args)
{
  args_for_add_to_solist *args1 = args;
  add_to_solist (args1->from_tty, args1->dll_path, args1->load_module_desc_p,
                 args1->load_module_desc_addr, args1->target);
  return 1;
}

/* Called from handle_dynlink_load_event and pa64_solib_add to add
   a shared library to so_list_head list and possibly to read in the
   debug information for the library.  

   If load_module_desc_p is NULL, then the load module descriptor must
   be read from the inferior process at the address load_module_desc_addr.  */

static void
add_to_solist (from_tty, dll_path, load_module_desc_p,
	       load_module_desc_addr, target)
     boolean from_tty; 
     char *dll_path; 
     struct load_module_desc *load_module_desc_p;
     CORE_ADDR load_module_desc_addr;
     struct target_ops *target;
{
  struct so_list *new_so;
  int pa64_solib_st_size_threshhold_exceeded;
  int st_size;
  struct so_list *next_solist, *saved_so;
#ifdef HPPA_FIX_AND_CONTINUE
  extern is_current_fixed_library (char *);
#endif

  if (symfile_objfile == NULL)
    return;

 /* JAGae68084 capture the name of the shared library loaded*/ 
  if (shllib_name)
    free (shllib_name);
  shllib_name=pathopt(dll_path);

  /* see if the entry for this lib is already in the solist */
  saved_so = NULL;
  next_solist = so_list_head;
  while (next_solist && !saved_so)
    {
      if (strcmp (next_solist->name, dll_path) == 0)
	{
          saved_so = next_solist;
        }
      next_solist = next_solist->next;
    }

  /* Add the shared library to the so_list_head list */
  if (saved_so == NULL)
    {
      new_so = (struct so_list *) xmalloc (sizeof (struct so_list));
      memset ((char *) new_so, 0, sizeof (struct so_list));
      if (so_list_head == NULL)
        {
          so_list_head = new_so;
          so_list_tail = new_so;
        }
      else
        {
          so_list_tail->next = new_so;
          so_list_tail = new_so;
        }
    }
  else
    {
      new_so = saved_so;
    }

  /* Initialize the new_so */
  if (load_module_desc_p)
    new_so->pa64_solib_desc = *load_module_desc_p;
  else
    {
      if (target_read_memory (load_module_desc_addr, 
			      (char*) &new_so->pa64_solib_desc,
			      sizeof (struct load_module_desc))
	  != 0)
	error ("Error while reading in dynamic library %s", dll_path);
    }
  
  new_so->pa64_solib_desc_addr = load_module_desc_addr;
  new_so->loaded = 1;
  strcpy (new_so->name, dll_path);

  /* Stacey 10/17/2001 
     If there's a problem with this library, inform the user what went wrong
     and return to make sure we don't end up 

     1.  erroring out before we try to load other libraries
     2.  doing useless work - i.e. trying to load symbols 
         from a file we have determined doesn't exist.  

     pa64_solib_sizeof_symbol_table sometimes returns 0 when all is well and 
     catch_errors returns 0 when there are errors or the value of the
     function it called otherwise.  The return value from catch_errors
     won't tell us if there was an error in this case.  So I've introduced 
     a new global error_in_shlib_symtab that will be set to 1 if an error 
     occurs inside pa64_solib_sizeof_symbol_table and will be 0 otherwise. */

  st_size = 
    catch_errors ( (catch_errors_ftype *) pa64_solib_sizeof_symbol_table,
		   dll_path, NULL, RETURN_MASK_ALL);
  if (error_in_shlib_symtab)
    {
      /* reset this for future use */
      error_in_shlib_symtab = 0;
      return;
    }

  pa64_solib_st_size_threshhold_exceeded =
    !from_tty && 
      ((auto_solib_add == 0) || 
       ((st_size + pa64_solib_total_st_size) 
        > (auto_solib_add * (LONGEST)1000000)));
#ifdef HPPA_FIX_AND_CONTINUE
  if (!is_current_fixed_library (new_so->name)
      && pa64_solib_st_size_threshhold_exceeded)
#else
  if (pa64_solib_st_size_threshhold_exceeded)
#endif
    {
      if (auto_solib_add && !threshhold_warning_given)
	{
	  warning ("Symbols for some libraries have not been loaded, because\ndoing so would exceed the size threshold specified by auto-solib-add.\nTo manually load symbols, use the 'sharedlibrary' command.\nTo raise the threshold, set auto-solib-add to a larger value and rerun\nthe program.\n");
	  threshhold_warning_given = 1;
	}

      pa64_solib_add_solib_objfile (new_so, from_tty, 1);
      return;
    } 

  /* Now read in debug info. */
  pa64_solib_total_st_size += st_size;

  /* This fills in new_so->objfile, among others. */
  pa64_solib_load_symbols (new_so, from_tty, target);
  reinit_frame_cache ();
  return;
}


static void clear_solib_info(struct so_list *solib)
{
  struct symtab *s;

  /* Diable breakpoints in this shlib */
  disable_breakpoints_in_shlibs(!info_verbose, (void*) solib);

  /* Clear current_source_symtab, if applicable */
  ALL_OBJFILE_SYMTABS(solib->objfile, s)
    {
      if (current_source_symtab == s) current_source_symtab = NULL;
    }
}


/* Called by pa64_solib_handle_dynlink_event to handle a load event.
   Extract the relevant information from the break routine parameters
   and then process loading the shared library.

   Return 1 if gdb should stop for input, 0 if not. */

static int warning_shown = 0;

static int
handle_dynlink_load_event (int pid)
{
  catch_load_unload_event_t *catch_event;
  char *dll_path;
  char dll_path_arr[MAXPATHLEN];
  CORE_ADDR dll_path_addr;
  CORE_ADDR load_module_desc_addr;
  CORE_ADDR load_module_desc_size;

  dll_path = dll_path_arr;
  /* extract the various args, ARG0 had the load event indicator
     ARG1       &(struct load_module_desc)
     ARG2       sizeof(struct load_module_desc)
     dll_path_addr      dll_path_address
   */

  load_module_desc_addr = read_register (ARG1_REGNUM);
  load_module_desc_size = read_register (ARG2_REGNUM);
  dll_path_addr = read_register (ARG3_REGNUM);

  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);

#if 1
    /* srikanth, how can control come here for a core file ??? */
    if (gdb_shlib_root && !target_has_execution)
      {
        char altname[MAXPATHLEN];
        int status;
        struct stat statbuf;

	/* RM: If we find this library in gdb_shlib_root _and_ we are debugging
	   core files, use that version instead. Useful when analyzing core
	   files on a system other than the one that produced the core */
	strcpy(altname, gdb_shlib_root);
	strcat(altname, dll_path);
	status = stat(altname, &statbuf);
	if (status != -1)
	  {
	    dll_path = altname;
	  }
      }
#endif  

  /** The following message is incorrect. The warning message should be as the following as suggested by the 
      linker team:

   "Warning: Size of load module descriptor bigger than expected ; there may be new features in the dynamic
    loader that gdb does not support"

   **/
   /** Apr 12,2002 pes,FIXME : Attention!! Attention!! 
       Remove sizeof (int)  when DTLS change comes in gdb (11.23)
       Just to cope up with the increase by (sizeof int)in the 
       load_module_descr structure by linker (in AR0902) in 
       /CLO/Products/PHOENIX/Src/dld/Libdl/dlfcn.h, we have added
       this temporary workaround 
   
   if ( load_module_desc_size > (sizeof (struct load_module_desc)+sizeof (unsigned long long)))
    {
      warning ("Version of shared library %s not understood; there may be new features in the library that gdb does not support\n", dll_path);
    }
  */

   if ((load_module_desc_size > (sizeof (struct load_module_desc)) && (!warning_shown)))
     {
       warning ("Size of load module descriptor bigger than expected; there may be new features in the dynamic loader that gdb does not support\n");
       warning_shown = 1;
     }

  add_to_solist (FALSE, dll_path, NULL, load_module_desc_addr, NULL);

  /* If the user has requested load notifications, then report it now. */

  catch_event = catch_load_list;
  while (catch_event)
    {
      /* if filename is NULL, the user wants notification for all libraries */
      if (   (   catch_event->filename == NULL
              || strstr (dll_path, catch_event->filename))
	  && catch_event->breakpt->enable != disabled
	  && is_stop_for_bp (catch_event->breakpt)
	 )
	{
	  printf_unfiltered ("Catchpoint %d (loaded %s)\n",
			     catch_event->breakpt->number,
			     dll_path);
	  return 1;		/* stop for input */
	}			/* end if */
      catch_event = catch_event->next;
    }				/* end while catch_event */

  return 0;			/* don't stop for input */
}

/* Called by pa64_solib_handle_dynlink_event to handle an unload event.
   Extract the relevant information from the break routine parameters
   and then process unloading the shared library. */

static int
handle_dynlink_unload_event (int pid)
{
  catch_load_unload_event_t *catch_event;
  char dll_path[MAXPATHLEN];
  CORE_ADDR dll_path_addr;
  boolean found_solib;
  CORE_ADDR load_module_desc_addr;
  CORE_ADDR load_module_desc_size;
  static struct so_list *next_solist;
  static struct so_list *prev_solist_p;
  static struct so_list **prev_solist_pp;
  
  /* extract the various args, ARG0 had the unload event indicator
     ARG1       &(struct load_module_desc)
     ARG2       sizeof(struct load_module_desc)
     dll_path_addr      dll_path_address
   */

  load_module_desc_addr = read_register (ARG1_REGNUM);
  load_module_desc_size = read_register (ARG2_REGNUM);
  dll_path_addr = read_register (ARG3_REGNUM);

  read_memory_string (dll_path_addr, dll_path, MAXPATHLEN);

 /** The following message is incorrect. The warning message should be as the following as suggested by the 
      linker team:

   "Warning: Size of load module descriptor bigger than expected ; there may be new features in the dynamic
    loader that gdb does not support"

   **/
   /** Apr12,2002 pes,FIXME : Attention!! Attention!! 
       Remove sizeof (int)  when DTLS change comes in gdb (11.23)
       Just to cope up with the increase by (sizeof int)in the 
       load_module_descr structure by linker (in AR0902) in 
       /CLO/Products/PHOENIX/Src/dld/Libdl/dlfcn.h, we have added
       this temporary workaround 
    
 
   if ( load_module_desc_size > (sizeof (struct load_module_desc)+sizeof (unsigned long long)))
    {
      warning ("Version of shared library %s not understood; there may be new features in the library that gdb does not support\n", dll_path);
    } 
  **/

   if ((load_module_desc_size > (sizeof (struct load_module_desc)) && (!warning_shown)))
     {
       warning ("Size of load module descriptor bigger than expected; there may be new features in the dynamic loader that gdb does not support\n");
       warning_shown = 1;
     }

  /* Mark the shared library as unloaded by setting loaded to FALSE.
     Also set pa64_solib_desc_addr to NULL. */

  found_solib = FALSE;
  next_solist = so_list_head;
  prev_solist_p = NULL;
  prev_solist_pp = &so_list_head;
  while (next_solist && !found_solib)
    {
      if (strcmp (next_solist->name, dll_path) == 0)
	{
	  found_solib = TRUE;
	  next_solist->pa64_solib_desc_addr = NULL;
	  next_solist->loaded = FALSE;

	  /* If we never loaded debug info for this library, we can
	     forget about it and remove it from the so_list_head list.
	   */

	  if (next_solist->objfile == NULL)
	    {
	      /* This might update so_list_head */
	      *prev_solist_pp = next_solist->next;
	      if (so_list_tail == next_solist)
		so_list_tail = prev_solist_p;
	    }
	}			/* if strcmp == 0 */
      prev_solist_pp = &next_solist->next;
      prev_solist_p = next_solist;
      next_solist = next_solist->next;
    }				/* end while */

  if (found_solib && prev_solist_p->objfile)
    {
      /* Disable the watchpoint expressions(watch on shared lib space)
         that aren`t valid after unloading shlib and removing debug information */
      disable_break_exp_chain (prev_solist_p->objfile);
      /* clear debug info, breakpoints, etc. */
      clear_solib_info(prev_solist_p);
      if (rtc_dot_sl == prev_solist_p->objfile)
        rtc_dot_sl = NULL;
      free_objfile(prev_solist_p->objfile);
      prev_solist_p->objfile = NULL;
    }

  /* If the user has requested unload notifications, then report it now. */

  catch_event = catch_unload_list;
  while (catch_event)
    {
      /* if filename is NULL, the user wants notification for all libraries */
      if (   (   catch_event->filename == NULL
              || strstr (dll_path, catch_event->filename))
	  && catch_event->breakpt->enable != disabled
	  && is_stop_for_bp (catch_event->breakpt))
	{
	  printf_unfiltered ("Catchpoint %d (unloaded %s)\n",
			     catch_event->breakpt->number,
			     dll_path);
	  return 1;		/* stop for input */
	}			/* end if */
      catch_event = catch_event->next;
    }				/* end while catch_event */

  return 0;			/* don't stop for input */
}				/* end handle_dynlink_unload_event */


/* Remove the indicated catch_load_unload_event_t from the appropriate
   list */

void 
pa64_dynlink_delete_event (void_event_p)
     void *void_event_p;
{
  catch_load_unload_event_t *event_p;
  catch_load_unload_event_t **event_list;

  if (void_event_p == NULL)
    return;
  event_p = (catch_load_unload_event_t *) void_event_p;
  if (event_p->breakpt->type == bp_catch_load)
    {
      event_list = &catch_load_list;
    }
  else
    {
      assert (event_p->breakpt->type == bp_catch_unload);
      event_list = &catch_unload_list;
    }

  /* Remove the event from the event list */

  if (*event_list == event_p)
    {
      *event_list = event_p->next;
    }
  else
    {
      while (*event_list && (*event_list)->next != event_p)
	{
	  event_list = &((*event_list)->next);
	}
      if (*event_list)
	{
	  (*event_list)->next = event_p->next;
	}
    }

  free (event_p->filename);
  free (event_p);
}

/* Place a pointer to the breakpoint structure in the catch_load_unload_event_t
   structure pointer passed in the void* pointer */

void
pa64_dynlink_register_bp (void_event_p, breakpt)
     void *void_event_p;
     struct breakpoint *breakpt;
{
  catch_load_unload_event_t *event_p;

  event_p = (catch_load_unload_event_t *) void_event_p;
  event_p->breakpt = breakpt;
}


struct objfile *
pa64_solib_get_objfile_by_name (name)
  char * name;
{
  struct so_list * so_list = so_list_head;

  while (so_list)
    {
      if (strcmp(so_list->objfile->name, name) == 0)
        {
          break;
        }
      so_list = so_list->next;
    }

  if (!so_list)
    return NULL;

  return so_list->objfile;
}

/*JAGae68084 -See if we shl_load event*/
void see_if_shl_load(int pid)
{

if (target_has_execution)
  {
    struct minimal_symbol * m;

    m = lookup_minimal_symbol_by_pc (stop_pc);
    if (m && !strcmp (SYMBOL_NAME (m),  "__dld_break"))
        if (pa64_solib_have_load_event (pid))
               in_shl_load=1;
         
  }
}


/* Process the event signaled by hitting the break instruction in dld.
   Return 0 if gdb should not stop for input, non-zero otherwise; */

int
pa64_solib_handle_dynlink_event (int pid, CORE_ADDR stop_pc)
{
  CORE_ADDR arg0;
  int return_value;
  struct so_list *next_solist;

  /* The first argument (in ARG0_REGNUM) to the break routine is the reason 
     for the notification.  Extract it and act accordingly. */

  return_value = 0;
  arg0 = read_register (ARG0_REGNUM);

  /* Switch terminal for any messages produced. */
  target_terminal_ours_for_output ();

  /* We can't use a switch statement with a 64-bit value */
  if (arg0 == DLD_CB_LOAD)
    {
      strt_loaded=1; /*JAGae68084*/
      return_value = handle_dynlink_load_event (pid);
#ifdef RTC
      snoop_on_the_heap ();
#endif /* RTC - JAGaf15615 */
    }
  else if (arg0 == DLD_CB_UNLOAD)
    {
      return_value = handle_dynlink_unload_event (pid);
#ifdef RTC
      snoop_on_the_heap ();
#endif /* RTC */
    }
  else if (arg0 == DLD_CB_LOAD_COMPLETE)
    {
      shlib_load_completed = 1; /* JAGaf35513 */

      if (check_heap_in_this_run || trace_threads_in_this_run)
        {
          /* Warn if we're doing thread or heap checking and
             librtc is not loaded */
          next_solist = so_list_head;
          while (next_solist)
            {
              if (strstr (next_solist->name, "librtc") != 0)
                break;
              next_solist = next_solist->next;
            }

          if (next_solist == NULL)
            warning ("Cannot find librtc. Heap and/or thread checking "
                     "infomation will not be available. Set LIBRTC_SERVER "
                     "to point to the right librtc or set GDB_ROOT to point"
                     "to your wdb installation directory.");
        }
    }
	/* Support for PA64 GDB to step into BOR calls. */
  else if (arg0 == DLD_CB_BOR)
    {
		/*Will be handled in infrun.c*/
		return_value = 0;
    }
  else
    {
      warning ("Unexpected shared library event ignored.");
    }
     /* JAGae68084 - Call place_breakpoints_in_shlib(), if the user has requested for
     persistent breakpoints and we have seen a load event*/

    if (if_yes_to_all_commands && strt_loaded && shlib_load_completed)
      {
       strt_loaded=0;
       see_if_shl_load(inferior_pid);
       place_breakpoints_in_shlib();
       now_re_enable_bp = 1;
       in_shl_load = 0;
       free(shllib_name);
       shllib_name = NULL;
      }

  target_terminal_inferior ();

  return return_value;
}

/* For a given objfile, fill in a struct section_offsets with
   offsets between the link-time addresses and the runtime addresses of the
   segments for:
   text
   data
   bss   (the same as data)

   For the main program, the addr passed is zero, and the offsets are all
   set to zero.  For anything else (i.e. shared libraries), addr is
   the the run-time address of the text section minus the 
   link-time presumed text address.

   Call error if something goes wrong. */

void
pa64_solib_section_offsets (objfile, addrs)
     struct objfile *objfile;
     struct section_addr_info *addrs;
{
  CORE_ADDR text_addr, data_addr;
  int i;
  char obj_bname[MAXPATHLEN], so_bname[MAXPATHLEN];
  struct so_list *so_list = so_list_head;

  objfile->num_sections = SECT_OFF_MAX;
  objfile->section_offsets = (struct section_offsets *)
			     obstack_alloc (&objfile->psymbol_obstack,
					    SIZEOF_SECTION_OFFSETS);

  /* JYG: MERGE FIXME: hack to initialize these here */
  objfile->sect_index_text = 0;
  objfile->sect_index_data = 1;
  objfile->sect_index_bss = 2;
  objfile->sect_index_rodata = 3;

  if (addrs->other[0].addr == 0)
    {
      /* For the main program, all the offsets are zero. */
      for (i = 0; i < SECT_OFF_MAX; i++)
	ANOFFSET (objfile->section_offsets, i) = 0;
      return;
    }

  while (so_list)
    {
      /* Oh what a pain!  We need the offsets before so_list->objfile
         is valid.  The BFDs will never match.  Make a best guess.  */
     
      /* JAGaf52783 - add-symbol-file command fails for gdb64. Try to 
         compare the basenames of objfile and sharedlibrary */
      if (addrs->other[0].addr == 
          so_list->pa64_solib_desc.text_base - 
          elf_text_start_vma(objfile->obfd))
	{
	  /* The text offset is easy.  */
	  ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile))
	    = ANOFFSET (objfile->section_offsets, SECT_OFF_RODATA (objfile))
	    = addrs->other[0].addr;

	  /* The data segment is very similar */

	  ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile))
	    = ANOFFSET (objfile->section_offsets, SECT_OFF_BSS (objfile))
	    = addrs->other[1].addr;

	  return;
	}
      so_list = so_list->next;
    }

  /* jini: JAGag26122.
     With JAGag06408 changes had been made to avoid adding a new symbol file
     if live debugging is being done since the circumstances in which such
     a scenario gets used was unclear. Later, SAP explained that they
     use such a scenario while debugging code generated by a JVM. So,
     make changes to allow the addition of symbol files during live debugging
     also. */

   /* Skip this warning for the internal use of add_symbol_file_command
      during RTC processing of unloaded shared libraries
   */
   if( !add_unloaded_library_symbols )
     {
       printf_unfiltered ("\n");
       warning ("Unknown shared object. You are trying to load a shared"
            " library/object file that is previously not loaded.");
     }

   ANOFFSET (objfile->section_offsets, SECT_OFF_TEXT (objfile))
     = ANOFFSET (objfile->section_offsets, SECT_OFF_RODATA (objfile))
     = addrs->other[0].addr;

   if (addrs->other[1].name)
     ANOFFSET (objfile->section_offsets, SECT_OFF_DATA (objfile))
       = ANOFFSET (objfile->section_offsets, SECT_OFF_BSS (objfile))
       = addrs->other[1].addr;
   return;
}				/* pa64_solib_section_offsets */

void
pa64_solib_remove (struct objfile * objfile)
{
  struct so_list * so_list, * prev_so_list = NULL;
  for (so_list = so_list_head; so_list; so_list = so_list->next)
    {
      if (so_list->objfile == objfile)
        {
          if (prev_so_list != NULL)
            prev_so_list->next = so_list->next;
          free (so_list);
          break;
        }
      prev_so_list = so_list;
    }
}


/* is_executable
   Called from dlgetmodinfo1 to determine if the load module descriptor
   belongs to the executable. Compare the text_base with the default 
   text base of an executable.

   FIXME. dlgetmodinfo with index -2 has to retrieve load module
   descriptor of the executable. This is avbl only in 11.23 IA.
   Once this is avbl for PA2.0, change this to use index -2.
*/
int
is_executable (index, read_tgt_mem, ident_parm, load_map_parm)
   int index;
   void *(*read_tgt_mem) (void *buffer, uint64_t ptr, size_t bufsiz, int ident);
   int ident_parm;
   uint64_t load_map_parm;
{
   struct load_module_desc desc;
   size_t desc_size;

   if (dlgetmodinfo (index, &desc, sizeof(desc), read_tgt_mem, ident_parm, load_map_parm))
     if (desc.text_base == 0x4000000000000000ULL || desc.text_base == 0x40000000)
       return 1;

   return 0;
}

/* dlgetmodinfo1
   Normally dlgetmodinfo retrieves 
	load module info on dld, if index = -1
	load module info on executable, if index = 0
	load module info on shared libraries, if index = 1 to n

   If BIND_FIRST is used in shl_load(), it places the library at the head
   of the symbol search list. In that case index 0 will return info on
   shared libraries. 

   This function is a wrapper for dlgetmodinfo to retrieve info about all
   the shared libraries both after and before the executable on the load
   module descriptor list.
*/

uint64_t
dlgetmodinfo1 (index, desc, desc_size, read_tgt_mem, ident_parm, load_map_parm)
   int index;
   struct load_module_desc *desc;
   size_t desc_size;
   void *(*read_tgt_mem) (void *buffer, uint64_t ptr, size_t bufsiz, int ident);
   int ident_parm;
   uint64_t load_map_parm;
{
   int i;
   int exec_index = 0;

   if (index == -1) /* dld entry */
     return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, ident_parm, load_map_parm);

   /* BIND_FIRST used ? */
   if (!is_executable (0, read_tgt_mem, ident_parm, load_map_parm))
     {
       /* find out the program entry */
       for (i=1; ; i++)
         if (is_executable (i, read_tgt_mem, ident_parm, load_map_parm))
           {
             exec_index = i;
             break;
           }
     }

   /* program entry in index 0 */
   if (exec_index == 0)
     return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, ident_parm, load_map_parm);
   else
     if (exec_index >= index)
       /* read in shared libs before executable in laod map */
       return dlgetmodinfo (index-1, desc, desc_size, read_tgt_mem, ident_parm, load_map_parm);
     else
       /* read in the shared libs after exec in the load lib */
       return dlgetmodinfo (index, desc, desc_size, read_tgt_mem, ident_parm, load_map_parm);
}

/* ----------------------------------------------------------------------------------
 * Description: This function takes a so_list* as an argument, reads the link time for
 * the associated shared library  from its dynamic section and compares that to 
 * the one returned by dlgetmodinfo() for the same shared library in the core.
 * If they don't match a warning message is issued.  
 * Fix for JAGad89789, JAGaf09159.
 * -----------------------------------------------------------------------------------*/

static void
hppa_core_so_mismatch_detection (struct so_list *so_ptr)
{
  static int disabled;
  static int printed_once;
  char *buf;
  asection *dyn_sect;
  int dyn_sect_size;
  unsigned long so_time_stamp;

  /* This functionality depends upon a new version of dlgetmodinfo(), i.e., dld.sl.
   * If this dld.sl isn't out there, this needs to be disabled to prevent wrong
   * information from being given out.  Once the new dld.sl has been out for a 
   * while the follwing check and the two that follow that can be removed.
   */
  
  if (core_so_mismatch_detection_disabled)
     return;  
   
  /* If the gdb is using an old dld.sl, irrespective of whether the core used the new
   * or the old dld.sl, disable mismatch detection.
   */
  if (so_ptr->pa64_solib_desc.time_stamp == 0)
    {
      core_so_mismatch_detection_disabled = 1;
      return;
    }

 /* Skip the check for dld; it is not accurate or relevant. */
  if (strcmp (basename (so_ptr->name), "dld.sl") == 0)
    return;
 
  dyn_sect = bfd_get_section_by_name (so_ptr->abfd, ".dynamic");

  if (dyn_sect == NULL)
    error ("\n%s doesn't have a .dynamic section.  Could be a bad library.\n", so_ptr->name);

  /* Read in .dynamic section.  */
  dyn_sect_size = bfd_section_size (so_ptr->abfd, dyn_sect);
  buf = (char *) alloca (dyn_sect_size);
  if (!bfd_get_section_contents (so_ptr->abfd, dyn_sect, buf, 0, dyn_sect_size))
    error ("\nCan't read the .dynamic section for %s.\n", so_ptr->name);

  /* Read in the time stamp from the .dynamic section. */
     
      Elf64_Dyn *dyn_sect_p = (Elf64_Dyn *) buf;

      while (dyn_sect_p->d_tag != DT_NULL && dyn_sect_p->d_tag != DT_HP_TIME_STAMP)
	dyn_sect_p++;

      so_time_stamp = dyn_sect_p->d_un.d_val;

  if ((so_ptr->pa64_solib_desc.time_stamp != so_time_stamp) && !printed_once)
    {
      warning ("Some of the libraries in the core file are different from the libraries on this computer.  It might be possible to proceed with your debugging process successfully.  However, if you run into problems you must use the versions of the libraries used by the core.  The mismatches are: \n");
      printed_once = 1;
    }

  if (so_ptr->pa64_solib_desc.time_stamp != so_time_stamp)
       printf ("  %s in the core file is different from\n  %s used by gdb\n\n",
               so_pathname_g, so_ptr->name);
} 

/* Support for PA64 GDB to step into BOR calls. */
boolean
start_bor_call (CORE_ADDR target_addr)
{
  
  int quad;
  
  quad = target_addr >> 62;
  
  if (quad == 1 || quad == 2)
    {
      int st;
      CORE_ADDR start_addr;
      CORE_ADDR limit_addr;
      CORE_ADDR new_target;
      
      new_target = analyze_stub (stop_pc, &st, &start_addr,&limit_addr);
      if (st == 1)
        return TRUE;
    }
  return FALSE;
}

CORE_ADDR
solib_bor_event()
{
	CORE_ADDR arg0;
	CORE_ADDR symbol_name_addr;
	value_ptr symbol_addr_value;
	CORE_ADDR symbol_addr;
	char *symbol_name, *temp;

	register char *cp;
	register int i;
	int symbol_size = 4096;

	arg0 = read_register (ARG0_REGNUM);
	if(arg0 != DLD_CB_BOR)
	  return FALSE;
	
	symbol_name_addr = read_register (ARG3_REGNUM);

	/* JAGaf68908 - GDB64 unable to cope with long C++.
	   Dynamically allocate memory for symbol_name in 
	   chunks of 4096 bytes and read in the order of 
	   512 bytes.
	 */
	symbol_name = (char *) xmalloc (sizeof (char) * symbol_size);
	cp = symbol_name;
	while (1)
	{
	  if ((cp - symbol_name) == symbol_size)
	  {
            temp = symbol_name;
	    symbol_name = (char *) 
			  xrealloc (symbol_name, sizeof (char) * 
				                      (symbol_size += 4096));
	    cp = symbol_name + (cp - temp);
	  }
	  read_memory (symbol_name_addr + (int) (cp - symbol_name), cp, 512);
	  for (i = 0; i < 512 && *cp; i++, cp++)
	    ;
	  if (i != 512)
	    break;   
	}
	/* I am not interested in the name, I need its address.*/
	symbol_addr_value  = find_function_in_inferior_1 (symbol_name);
        if (symbol_addr_value == 0) return 0;
	symbol_addr = value_as_pointer (symbol_addr_value);
	free (symbol_name);
	return symbol_addr;
}

void
pa64_request_bor_callback (int needed)
{
  if (profile_on)
    return;
  /* Avoid unwanted writing to target`s memory. */
  if (needed && (dld_cache.dld_flags & DT_HP_DEBUG_CALLBACK_BOR))
    return;
  else if (!needed && !(dld_cache.dld_flags & DT_HP_DEBUG_CALLBACK_BOR))
    return;
  /* JAGag12140: Mithun 092806 Enabling should be done only while step operations */
  if (needed)
    dld_cache.dld_flags |= DT_HP_DEBUG_CALLBACK_BOR;
  else
    dld_cache.dld_flags &= ~DT_HP_DEBUG_CALLBACK_BOR;
    
  target_write_memory (dld_cache.dld_flags_addr,
		       (char *)&dld_cache.dld_flags,
		       sizeof (dld_cache.dld_flags));
}

/* JAGaf26681 - A return value of 1 implies that the shared libraries have been mapped private. */
int
is_solib_mapped_private()
{
	return (dld_cache.dld_flags & DT_HP_DEBUG_PRIVATE);
}
