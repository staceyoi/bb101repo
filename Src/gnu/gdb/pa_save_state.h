#ifndef PA_SAVE_STATE_INCLUDED
#define PA_SAVE_STATE_INCLUDED

#include <inttypes.h>

#ifndef SS_WIDEREGS
#define       SS_WIDEREGS     0x40    /* The 64-bit registers are live. */
#endif
/*****************************************************************************\
 These data structures are pa specific, and are being moved forward for use
 in mixed mode debugging. The structs are all prefixed with pa to prevent
 symbol conflicts. There are some useful comments about these structures in
 the original files in /usr/include/...
\*****************************************************************************/

typedef struct pa_fp_dbl_block
{
   double ss_fp0;                  /* Must be double word aligned */
   double ss_fp1;
   double ss_fp2;
   double ss_fp3;
   double ss_fp4;
   double ss_fp5;
   double ss_fp6;
   double ss_fp7;
   double ss_fp8;
   double ss_fp9;
   double ss_fp10;
   double ss_fp11;
   double ss_fp12;
   double ss_fp13;
   double ss_fp14;
   double ss_fp15;
   double ss_fp16;
   double ss_fp17;
   double ss_fp18;
   double ss_fp19;
   double ss_fp20;
   double ss_fp21;
   double ss_fp22;
   double ss_fp23;
   double ss_fp24;
   double ss_fp25;
   double ss_fp26;
   double ss_fp27;
   double ss_fp28;
   double ss_fp29;
   double ss_fp30;
   double ss_fp31;
} pa_fp_dbl_block_t;


typedef struct pa_fp_int_block
{
   int ss_fpstat;                  /* Must be double word aligned */
   int ss_fpexcept1;
   int ss_fpexcept2;
   int ss_fpexcept3;
   int ss_fpexcept4;
   int ss_fpexcept5;
   int ss_fpexcept6;
   int ss_fpexcept7;
   int ss_fp4_hi;
   int ss_fp4_lo;
   int ss_fp5_hi;
   int ss_fp5_lo;
   int ss_fp6_hi;
   int ss_fp6_lo;
   int ss_fp7_hi;
   int ss_fp7_lo;
   int ss_fp8_hi;
   int ss_fp8_lo;
   int ss_fp9_hi;
   int ss_fp9_lo;
   int ss_fp10_hi;
   int ss_fp10_lo;
   int ss_fp11_hi;
   int ss_fp11_lo;
   int ss_fp12_hi;
   int ss_fp12_lo;
   int ss_fp13_hi;
   int ss_fp13_lo;
   int ss_fp14_hi;
   int ss_fp14_lo;
   int ss_fp15_hi;
   int ss_fp15_lo;
   int ss_fp16_hi;
   int ss_fp16_lo;
   int ss_fp17_hi;
   int ss_fp17_lo;
   int ss_fp18_hi;
   int ss_fp18_lo;
   int ss_fp19_hi;
   int ss_fp19_lo;
   int ss_fp20_hi;
   int ss_fp20_lo;
   int ss_fp21_hi;
   int ss_fp21_lo;
   int ss_fp22_hi;
   int ss_fp22_lo;
   int ss_fp23_hi;
   int ss_fp23_lo;
   int ss_fp24_hi;
   int ss_fp24_lo;
   int ss_fp25_hi;
   int ss_fp25_lo;
   int ss_fp26_hi;
   int ss_fp26_lo;
   int ss_fp27_hi;
   int ss_fp27_lo;
   int ss_fp28_hi;
   int ss_fp28_lo;
   int ss_fp29_hi;
   int ss_fp29_lo;
   int ss_fp30_hi;
   int ss_fp30_lo;
   int ss_fp31_hi;
   int ss_fp31_lo;
} pa_fp_int_block_t;

typedef struct __pa_reg64 {
	int64_t ss_reserved;
	int64_t ss_gr1;
	int64_t ss_rp;
	int64_t ss_gr3;
	int64_t ss_gr4;
	int64_t ss_gr5;
	int64_t ss_gr6;
	int64_t ss_gr7;
	int64_t ss_gr8;
	int64_t ss_gr9;
	int64_t ss_gr10;
	int64_t ss_gr11;
	int64_t ss_gr12;
	int64_t ss_gr13;
	int64_t ss_gr14;
	int64_t ss_gr15;
	int64_t ss_gr16;
	int64_t ss_gr17;
	int64_t ss_gr18;
	int64_t ss_gr19;
	int64_t ss_gr20;
	int64_t ss_gr21;
	int64_t ss_gr22;
	int64_t ss_arg3;
	int64_t ss_arg2;
	int64_t ss_arg1;
	int64_t ss_arg0;
	uint64_t ss_dp;
	uint64_t ss_ret0;
	uint64_t ss_ret1;
	uint64_t ss_sp;
	uint64_t ss_gr31;
	uint64_t ss_cr11;
	uint64_t ss_pcoq_head;
	uint64_t ss_pcsq_head;
	uint64_t ss_pcoq_tail;
	uint64_t ss_pcsq_tail;
	uint64_t ss_cr15;
	uint64_t ss_cr19;
	uint64_t ss_cr20;
	uint64_t ss_cr21;
	uint64_t ss_cr22;
	uint64_t ss_cpustate;
	uint64_t ss_sr4;
	uint64_t ss_sr0;
	uint64_t ss_sr1;
	uint64_t ss_sr2;
	uint64_t ss_sr3;
	uint64_t ss_sr5;
	uint64_t ss_sr6;
	uint64_t ss_sr7;
	uint64_t ss_cr0;
	uint64_t ss_cr8;
	uint64_t ss_cr9;
	uint64_t ss_cr10;
	uint64_t ss_cr12;
	uint64_t ss_cr13;
	uint64_t ss_cr24;
	uint64_t ss_cr25;
	uint64_t ss_cr26;
	uint64_t ss_cr27;
	uint64_t ss_reserved2[2];
	uint32_t ss_oldcksum;
	uint32_t ss_newcksum;
} __pa_reg64_t;

/*
 * Save area for 64-bit registers (formatted for access one word at a
 * time).  See the definition of __reg64_t above for more details.
 */
typedef struct __pa_reg32 {
	uint32_t ss_reserved[2];
	uint32_t ss_gr1_hi;
	uint32_t ss_gr1_lo;
	uint32_t ss_rp_hi;
	uint32_t ss_rp_lo;
	uint32_t ss_gr3_hi;
	uint32_t ss_gr3_lo;
	uint32_t ss_gr4_hi;
	uint32_t ss_gr4_lo;
	uint32_t ss_gr5_hi;
	uint32_t ss_gr5_lo;
	uint32_t ss_gr6_hi;
	uint32_t ss_gr6_lo;
	uint32_t ss_gr7_hi;
	uint32_t ss_gr7_lo;
	uint32_t ss_gr8_hi;
	uint32_t ss_gr8_lo;
	uint32_t ss_gr9_hi;
	uint32_t ss_gr9_lo;
	uint32_t ss_gr10_hi;
	uint32_t ss_gr10_lo;
	uint32_t ss_gr11_hi;
	uint32_t ss_gr11_lo;
	uint32_t ss_gr12_hi;
	uint32_t ss_gr12_lo;
	uint32_t ss_gr13_hi;
	uint32_t ss_gr13_lo;
	uint32_t ss_gr14_hi;
	uint32_t ss_gr14_lo;
	uint32_t ss_gr15_hi;
	uint32_t ss_gr15_lo;
	uint32_t ss_gr16_hi;
	uint32_t ss_gr16_lo;
	uint32_t ss_gr17_hi;
	uint32_t ss_gr17_lo;
	uint32_t ss_gr18_hi;
	uint32_t ss_gr18_lo;
	uint32_t ss_gr19_hi;
	uint32_t ss_gr19_lo;
	uint32_t ss_gr20_hi;
	uint32_t ss_gr20_lo;
	uint32_t ss_gr21_hi;
	uint32_t ss_gr21_lo;
	uint32_t ss_gr22_hi;
	uint32_t ss_gr22_lo;
	uint32_t ss_arg3_hi;
	uint32_t ss_arg3_lo;
	uint32_t ss_arg2_hi;
	uint32_t ss_arg2_lo;
	uint32_t ss_arg1_hi;
	uint32_t ss_arg1_lo;
	uint32_t ss_arg0_hi;
	uint32_t ss_arg0_lo;
	unsigned int ss_dp_hi;
	unsigned int ss_dp_lo;
	unsigned int ss_ret0_hi;
	unsigned int ss_ret0_lo;
	unsigned int ss_ret1_hi;
	unsigned int ss_ret1_lo;
	unsigned int ss_sp_hi;
	unsigned int ss_sp_lo;
	unsigned int ss_gr31_hi;
	unsigned int ss_gr31_lo;
	unsigned int ss_cr11_hi;
	unsigned int ss_cr11_lo;
	unsigned int ss_pcoq_head_hi;
	unsigned int ss_pcoq_head_lo;
	unsigned int ss_pcsq_head_hi;
	unsigned int ss_pcsq_head_lo;
	unsigned int ss_pcoq_tail_hi;
	unsigned int ss_pcoq_tail_lo;
	unsigned int ss_pcsq_tail_hi;
	unsigned int ss_pcsq_tail_lo;
	unsigned int ss_cr15_hi;
	unsigned int ss_cr15_lo;
	unsigned int ss_cr19_hi;
	unsigned int ss_cr19_lo;
	unsigned int ss_cr20_hi;
	unsigned int ss_cr20_lo;
	unsigned int ss_cr21_hi;
	unsigned int ss_cr21_lo;
	unsigned int ss_cr22_hi;
	unsigned int ss_cr22_lo;
	unsigned int ss_cpustate_hi;
	unsigned int ss_cpustate_lo;
	unsigned int ss_sr4_hi;
	unsigned int ss_sr4_lo;
	unsigned int ss_sr0_hi;
	unsigned int ss_sr0_lo;
	unsigned int ss_sr1_hi;
	unsigned int ss_sr1_lo;
	unsigned int ss_sr2_hi;
	unsigned int ss_sr2_lo;
	unsigned int ss_sr3_hi;
	unsigned int ss_sr3_lo;
	unsigned int ss_sr5_hi;
	unsigned int ss_sr5_lo;
	unsigned int ss_sr6_hi;
	unsigned int ss_sr6_lo;
	unsigned int ss_sr7_hi;
	unsigned int ss_sr7_lo;
	unsigned int ss_cr0_hi;
	unsigned int ss_cr0_lo;
	unsigned int ss_cr8_hi;
	unsigned int ss_cr8_lo;
	unsigned int ss_cr9_hi;
	unsigned int ss_cr9_lo;
	unsigned int ss_cr10_hi;
	unsigned int ss_cr10_lo;
	unsigned int ss_cr12_hi;
	unsigned int ss_cr12_lo;
	unsigned int ss_cr13_hi;
	unsigned int ss_cr13_lo;
	unsigned int ss_cr24_hi;
	unsigned int ss_cr24_lo;
	unsigned int ss_cr25_hi;
	unsigned int ss_cr25_lo;
	unsigned int ss_cr26_hi;
	unsigned int ss_cr26_lo;
	unsigned int ss_cr27_hi;
	unsigned int ss_cr27_lo;
	unsigned int ss_reserved2[4];
	unsigned int ss_oldcksum;
	unsigned int ss_newcksum;
} __pa_reg32_t;

/*
 * Save area for 32-bit registers.
 */
typedef struct __pa_ss_narrow {
	int ss_gr1;			/* General Registers */
	int ss_rp;
	int ss_gr3;
	int ss_gr4;
	int ss_gr5;
	int ss_gr6;
	int ss_gr7;
	int ss_gr8;
	int ss_gr9;
	int ss_gr10;
	int ss_gr11;
	int ss_gr12;
	int ss_gr13;
	int ss_gr14;
	int ss_gr15;
	int ss_gr16;
	int ss_gr17;
	int ss_gr18;
	int ss_gr19;
	int ss_gr20;
	int ss_gr21;
	int ss_gr22;
	int ss_arg3;
	int ss_arg2;
	int ss_arg1;
	int ss_arg0;		/* Following on interrupt stack */
	unsigned int ss_dp;
	unsigned int ss_ret0;
	unsigned int ss_ret1;
	unsigned int ss_sp;
	unsigned int ss_gr31;
	unsigned int ss_cr11;		/* Control Registers */
	unsigned int ss_pcoq_head;
	unsigned int ss_pcsq_head;
	unsigned int ss_pcoq_tail;
	unsigned int ss_pcsq_tail;
	unsigned int ss_cr15;
	unsigned int ss_cr19;		/* For break and assist traps only */
	unsigned int ss_cr20;		/* For data page fault only */
	unsigned int ss_cr21;
	unsigned int ss_cr22;
	unsigned int ss_cpustate;		/* Local for thandler */
	unsigned int ss_sr4;		/* Previous sr4 value */
	unsigned int ss_sr0;
	unsigned int ss_sr1;
	unsigned int ss_sr2;
	unsigned int ss_sr3;
	unsigned int ss_sr5;
	unsigned int ss_sr6;
	unsigned int ss_sr7;
	unsigned int ss_cr0;
	unsigned int ss_cr8;
	unsigned int ss_cr9;
	unsigned int ss_cr10;
	unsigned int ss_cr12;
	unsigned int ss_cr13;
	unsigned int ss_cr24;
	unsigned int ss_cr25;
	unsigned int ss_cr26;
	unsigned int ss_cr27;
	unsigned int ss_mpsfu_low;
	unsigned int ss_mpsfu_ovflo;
} __pa_ss_narrow_t;

typedef struct pa_save_state {
	int ss_flags;		/* Save State Flags */
	__pa_ss_narrow_t	ss_narrow; /* Save area for 32-bit registers */
	int ss_pad;		      /* For padding to double word boundary */
	union {				 /* Must be double word aligned */
#if defined(_KERNEL) || defined(_INCLUDE_HPUX_SOURCE) || !defined(_XPG4_EXTENDED)
		pa_fp_dbl_block_t fpdbl;
		pa_fp_int_block_t fpint;
#else /* !_KERNEL && !_INCLUDE_HPUX_SOURCE && XPG4_EXTENDED*/
		pa_fp_dbl_block_t __fpdbl;
		pa_fp_int_block_t __fpint;
#endif /* _KERNEL || _INCLUDE_HPUX_SOURCE || !_XPG4_EXTENDED*/
	} ss_fpblock;
	char ss_xor[4*32];		/* reserved */
	union {
		/* See the real save_state.h for some useful comments here */
		__pa_reg64_t ss_64;	/* formatted as 64-bit values */
		__pa_reg32_t ss_32;	/* or as pairs of 32-bit values */
	} ss_wide;
} pa_save_state_t;

typedef struct pa89_save_state {
	int ss_flags;		/* Save State Flags  */
	int ss_gr1;		/* General Registers */
	int ss_rp;
	int ss_gr3;
	int ss_gr4;
	int ss_gr5;
	int ss_gr6;
	int ss_gr7;
	int ss_gr8;
	int ss_gr9;
	int ss_gr10;
	int ss_gr11;
	int ss_gr12;
	int ss_gr13;
	int ss_gr14;
	int ss_gr15;
	int ss_gr16;
	int ss_gr17;
	int ss_gr18;
	int ss_gr19;
	int ss_gr20;
	int ss_gr21;
	int ss_gr22;
	int ss_arg3;
	int ss_arg2;
	int ss_arg1;
	int ss_arg0;			/* Following on interrupt stack */
	unsigned int ss_dp;
	unsigned int ss_ret0;
	unsigned int ss_ret1;
	unsigned int ss_sp;
	unsigned int ss_gr31;
	unsigned int ss_cr11;		/* Control Registers */
	unsigned int ss_pcoq_head;
	unsigned int ss_pcsq_head;
	unsigned int ss_pcoq_tail;
	unsigned int ss_pcsq_tail;
	unsigned int ss_cr15;
	unsigned int ss_cr19;		/* For break and assist traps only */
	unsigned int ss_cr20;		/* For data page fault only */
	unsigned int ss_cr21;
	unsigned int ss_cr22;
	unsigned int ss_cpustate;	/* Local for thandler */
	unsigned int ss_sr4;		/* Previous sr4 value */
	unsigned int ss_sr0;
	unsigned int ss_sr1;
	unsigned int ss_sr2;
	unsigned int ss_sr3;
	unsigned int ss_sr5;
	unsigned int ss_sr6;
	unsigned int ss_sr7;
	unsigned int ss_cr0;
	unsigned int ss_cr8;
	unsigned int ss_cr9;
	unsigned int ss_cr10;
	unsigned int ss_cr12;
	unsigned int ss_cr13;
	unsigned int ss_cr24;
	unsigned int ss_cr25;
	unsigned int ss_cr26;
	unsigned int ss_mpsfu_high;
	unsigned int ss_mpsfu_low;
	unsigned int ss_mpsfu_ovflo;
	int ss_pad;		      /* For padding to double word boundary */
	union {			      /* Must be double word aligned */
		pa_fp_dbl_block_t fpdbl;
		pa_fp_int_block_t fpint;
	} ss_fpblock;
} pa89_save_state_t;

#define       SS_INSYSCALL    0x02    /* State saved from system call */

#endif /* PA_SAVE_STATE_INCLUDED */
