/* Functionality for packing and unpacking core file libraries. 

   Copyright 1986, 1987, 1989, 1991-1994, 2000
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  
   
   Designed and developed by Hewlett-Packard for HP-UX on PA-RISC
   and Itanium.	 Author: Bharath.  Date: 17 May 2004. 
			 Michael Coulter major re-write 26 May 2005.

*/

/*----------------------------------------------------------------------------*/
/* Includes.
 */
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <limits.h>
#include <stdio.h>
#include <sys/stat.h>

#include "defs.h"

#include "symtab.h"
#include "objfiles.h"
#include "call-cmds.h"	/* for core_file_command() prototype */
#include "gdbcore.h"
#include "target.h"     
#include "top.h"	/* for cd_command() prototype */

/*----------------------------------------------------------------------------*/
/* External declarations.
 */
extern struct objfile *symfile_objfile;
extern char *current_directory;
extern char **gdb_shlib_path_list;
extern int gdb_shlib_path_length;

/*----------------------------------------------------------------------------*/
/* Local definitions.
 */
#define PACKCORE_BUF_SIZE 4096

static char *tar_output_file;  /* call tempnam() to initialize */

/*----------------------------------------------------------------------------*/
/* Local function declarations.
 */
static int file_readable (char *file);
static int file_exists (char *file);
static char* get_arg (char *arg_str, char *err_msg);
static void add_to_str (char **main_str, int *main_str_buf_len, char *add_str);
static int execute_tar_cmd (char *tar_cmd, char *err_msg, int ignore_err, char *tar_file, char *oldpwd);
static void dump_tar_output (void);

/* Function	: packcore_part2
 * Description  : tar_dir is a directory in populated with modules.tar 
 *		  which contains all of the load modules - the main program
 *		  and all of the shared libraries.  The file program.txt is
 *		  added to the directory which is the name of the program file
 *		  in modules.tar.  Then tar_dir.tar is created from the
 *		  directory.
 *		  Then the core file is added to tar_dir.tar.  If that works,
 *		  the core file is removed.  Compress is invoked on tar_dir.tar
 *		  to make tar_dir.tar.Z.
 *		  oldpwd is a pointer to the previous directory.
 * 
 *		  The items added to tar_dir are:
 *			program.txt - the name of the program file in 
 *					modules.tar
 *
 *		  Other items intended for the future:
 *			readme.txt	directions for the user
 *			sysinfo.txt	output of uname -a gdb version info ...
 *			backtrace.txt	output of backtrace 100 full
 */

void
packcore_part2 (char *tar_dir, char *oldpwd) 
{
  char *		cmd;
  char *		file_path;
  int			status;
  int			fd;
  FILE *		fp = NULL;
  char *                base_name;

  /* place the program name in tar_dir/progname.txt  */
  file_path = xmalloc (strlen(tar_dir) + 30); 
  strcpy (file_path, tar_dir);
  strcat (file_path, "/progname.txt");

  unlink (file_path);
  fd = open (file_path, O_RDWR | O_EXCL | O_CREAT, 0777);
  if (fd == -1 || (fp = fdopen (fd, "w")) == NULL)
    error ("Unable to create '%s'", file_path);
  fputs (basename((char *) exec_bfd->filename), fp);
  fclose (fp);
  free (file_path);

  /* Create packcore.tar from packcore directory */
  cmd = xmalloc (strlen(tar_dir) + 50);

  /* The tar options used in this file are documented here for convenience:
   *	c	create   (x and r are used elsewhere, extract and replace).
   *	h	follow symbolic links
   *	l	complain if a link does not resolve
   *	o	suppress information about owners and modes, on extraction
   *		  files are owned by the user running tar
   *	f	the following argument is the tar file name
   *
   */
  /* JAGag06192: remove -l option, because the errno returned by tar 
     when hard links are found for shlibs is being misinterpreted. */
  strcpy (cmd, "tar -chof ");
  strcat (cmd, tar_dir);
  strcat (cmd, ".tar ");
 
 /* using basename to avoid abs path to get stored when we do tarring. */
  base_name = basename((char *) tar_dir); 
  strcat (cmd, base_name);
  status = system (cmd);
  status = WEXITSTATUS (status);
  free (cmd);  
  if (status != 0)
    {
       /* QXCR1000838888- Go back to the previous directory.*/
       if (chdir (oldpwd) != 0)
       warning ("Could not go back to the previous directory\n");
       error ("Unable to make packcore.tar file from packcore directory.");
    }

  /* Create a symlink packcore/core pointing at the core file, then
   * add it to the .tar file, following links so the core file is added
   * to the archive as "core".
   * ln -s core_file packcore/core
   */

  cmd = xmalloc (2 * strlen(tar_dir) + 50 + strlen (core_bfd->filename));
  strcpy (cmd, "ln -s ");
  strcat (cmd, core_bfd->filename);
  strcat (cmd, " ");
  strcat (cmd, tar_dir);
  strcat (cmd, "/core");
  status = system (cmd);
  status = WEXITSTATUS (status);
  free (cmd);  
  if (status != 0)
    {
       /* QXCR1000838888- Go back to the previous directory.*/
       if (chdir (oldpwd) != 0)
       warning ("Could not go back to the previous directory\n");
       error ("Unable to make symlink packcore/core");
    }
  
  /* tar -rhlof packcore.tar packcore/core" */

  cmd = xmalloc (2 * strlen(tar_dir) + 50 + strlen (core_bfd->filename));
  strcpy (cmd, "tar -rhlof ");
  strcat (cmd, tar_dir);
  strcat (cmd, ".tar  ");
  strcat (cmd, base_name);
  
  /* strcat (cmd, tar_dir);
   * This is not required as we create the packcore directory being in the
   * directory where user wants to create packcore.tar.Z. This is to get 
   * the relative path when tarring. So use the basename of the user specified
   * directory.
   */

  strcat (cmd, "/core");
  status = system (cmd);
  status = WEXITSTATUS (status);
  free (cmd);  
  if (status != 0)
    { 
       /* Unable to add the core file,  presumably lack of space.  Tell the
	* user that the core file must be transported separately, then remove
	* the symbolic link for core and re-make the tar file.
	*/
       warning ("Unable to add core to packcore.tar, you must transport it separately.");

      /* rm -f packcore/core packcore.tar */
      cmd = xmalloc (2 * strlen(tar_dir) + 50 + strlen (core_bfd->filename));
      strcpy (cmd, "rm -f ");
      strcat (cmd, tar_dir);
      strcat (cmd, "/core ");
      strcat (cmd, tar_dir);
      strcat (cmd, ".tar ");
      status = system (cmd);
      /* JAGag06192: remove -l option, because the errno returned by tar 
         when hard links are found for shlibs is being misinterpreted. */
      strcpy (cmd, "tar -chof ");
      strcat (cmd, tar_dir);
      strcat (cmd, ".tar ");
      strcat (cmd, tar_dir);
      status = system (cmd);
      status = WEXITSTATUS (status);
      free (cmd);  
      if (status != 0)
	{
	  if (chdir (oldpwd) != 0)
          warning ("Could not go back to the previous directory\n");
          error (
	       "Unable to re-make packcore.tar file from packcore directory.");
	}

    }
  else 
    {
      /* If adding the core file worked, remove the core file. */

      /* Not a good idea, so providing an interactive prompt */
      printf_unfiltered ("The core file has been added to '%s.tar'\n", tar_dir); 
      if (query ("Do you want to remove the original core file?"))
        {
          cmd = xmalloc (strlen (core_bfd->filename) + 50 );
          strcpy (cmd, "rm -f ");
          strcat (cmd, core_bfd->filename);
          status = system (cmd);
          status = WEXITSTATUS (status);  
          free (cmd); 
     
          if (status != 0)
            {
              warning ("Unable to remove core file.\n");
	      printf_unfiltered ("The core file is not removed.\n");
            }
            else 
            {
	      printf_unfiltered ("The core file has been removed.\n");
	    }
        } 
       else
        {
          printf_unfiltered ("The core file is not removed.\n");
        }
    
    }

  /* We are done with the packcore directory, remove it */
  cmd = xmalloc (strlen (tar_dir) + 50 );
  strcpy (cmd, "rm -rf ");
  strcat (cmd, tar_dir);
  status = system (cmd);
  status = WEXITSTATUS (status);  // ignore status, what to do
  free (cmd);  

  /* Compress the .tar file */

  cmd = xmalloc (strlen(tar_dir) + 50 );
  strcpy (cmd, "compress ");
  strcat (cmd, tar_dir);
  strcat (cmd, ".tar");
  status = system (cmd);
  status = WEXITSTATUS (status);
  free (cmd);  
  if (status != 0)
    {
      /* QXCR1000838888- Go back to the previous directory */
      if (chdir (oldpwd) != 0)
      warning ("Could not go back to the previous directory\n");
      error ("Unable to compress %s.tar.", tar_dir); 
    }
} // end packcore_part2


/*----------------------------------------------------------------------------*/
/* Function	: packcore_command ()
 * Usage	: packcore [ BASENAME ]
 * Description  : The user must be examining a core file.  A compressed
 *		    tar file BASENAME.tar.Z is produced which can be given
 *		    to the unpackcore command on a different system to examine
 *		    the core file there.  It contains the executable, the
 *		    shared libraries in use and the core file.
 * Input	: The command string issued by the user containing optionally
 *		    the basename of the packcore file.  A directory by this
 *		    name is created and populated with files and links.  A
 *		    tar file is created from this directory.
 * Output	: Nothing is returned by this function, but a successful call
 *		    would have created a packcore.tar.Z in the current
 *		    directory.
 * Globals	: symfile_objfile is used to get the executable and shared 
 *		    library paths and names.  This is a (struct objfile *)
 *		    which points to the first objfile structure, i.e, the
 *		    executable.
 *                exec_bfd and core_bfd are used to get the core and executable
 *		    file names.
 * Notes	: The directory structure of the libraries are discarded.  All 
 *		    libraries are stored relative to the current directory 
 *		    without any path.  This was done to simplify the design of 
 *		    unpacking the core.  Other wise, unpack core would have to
 *		    do a lot of directory juggling to work properly, which in 
 *		    turn complicates the design.  This is a reasonable 
 *		    simplification because this is how people debugging cores 
 *		    tend to operate.  The only draw back is if there are two 
 *		    shared libraries with the same name in different 
 *		    directories.  Apparently, this is very rare (no one in the
 *		    linker team had ever come across this before) and linking
 *		    it itself is likely to cause problems.  A check and a 
 *		    warning for this may be a good future enhancement.  Also,
 *		    as a part of packing, the core file is loaded so that 
 *		    symfile_objfile is initialized.  Another point of interest
 *		    is that the tar command is left to handle some failures
 *		    like out of disk space and lack of permissions; this is why
 *		    dump_tar_ouput exists.
 *
 *  	       	    The current implementatio leverages the original
 *		    implementation to make packcore/modules.tar.  It would
 *		    be better to make packcore/modules a pseudo-root populated
 *		    with symbolic links which are transformed to files when
 *		    the tar file is made (follow links) and then using
 *		    GDB_SHLIB_ROOT instead of GDB_SHLIB_PATH to deal with the
 *		    corner case of libraries with the same basename but 
 *		    different directories.
 */
void 
packcore_command (char *command_line, int from_tty)
{
  char *		cmd;
  char *		dir_name;
  char *		file;
  struct objfile *	obj_file;
  char *		saved_command_line;
  int			status;
  char *		tar_cmd = NULL;
  int 			tar_cmd_len = 0;
  char *		tar_dir;
  char *		tar_file;
  char *		tmp;
  char *                base_name;
  char * 		oldpwd;
  char 			buf[PATH_MAX + 1];
  char 			buf_oldpwd[PATH_MAX + 1];

  /* Verify that we are currently examining a core file, we have a stack
   * but not an execution context
   */

  if (target_has_execution || ! target_has_stack)
    {
      error ("You must be examining a core file to execute packcore.");
    }

  /* Process the arguments, optional basename */

  /* QXCR1000983670: strtok can return invalid strings if
     it is passed NULL, and it holds some string pointer from
     a previous invocation. Do not call strtok with NULL string
     here.
  */
  tar_dir = NULL;
  if (command_line)
    {
      tar_dir = get_arg (command_line, NULL);
      if (tar_dir)
        {
          tar_dir = xstrdup (tar_dir);  /* So we can free tar_dir in either case */
          if (get_arg (NULL, NULL) != NULL)
            error ("packcore takes only one argument");
        }
    }

  if (!tar_dir)
    {
      tar_dir = xstrdup ("packcore");
    }

  /* Create the packcore directory */ 

  cmd = (char *) xmalloc (strlen(tar_dir) + 20);
  strcpy (cmd, "mkdir ");
  cmd = strcat (cmd, tar_dir);
  status = system (cmd);
  status = WEXITSTATUS (status);
  free (cmd);
  if (status != 0)
    {
      warning ("Unable to make packcore directory '%s'.", tar_dir);
      /* QXCR1000838888 */
      if (query ("Do you want to create '%s' under /tmp?", basename (tar_dir))) 
        {
          /* Fetch the basename before you free the original tar_dir.
           * i,e the name given by user.
	   * Remove /tmp/basename directory before creating it.
           */
          base_name = basename (tar_dir);
          free (tar_dir);
          tar_dir = xmalloc (strlen ("/tmp/") + strlen (base_name) + 1); 
          strcpy (tar_dir, "/tmp/");
          strcat (tar_dir, base_name);
          cmd = (char *) xmalloc (strlen (tar_dir) + 20);
          strcpy (cmd, "rm -fr ");
	  cmd = strcat (cmd, tar_dir);
	  status = system (cmd);
	  status = WEXITSTATUS (status);
          free (cmd);
 	  if (status !=0)
	    error ("Could not remove the already existing %s.", tar_dir);
   
          cmd = (char *) xmalloc (strlen (tar_dir) + 20); 
          strcpy (cmd, "mkdir ");
          cmd = strcat (cmd, tar_dir);
          status = system (cmd);
          status = WEXITSTATUS (status);
          free (cmd);
          if (status !=0)
            error ("Could not create %s ", tar_dir);
        }
        else 
        { 
          error ("Please specify an alternate directory."); 
        } 
            
    }
  
  /* Now move to dir_name of tar_dir i,e the directory where
   * user wants to create packcore.tar.Z
   */
  /* Should convert relative path given by the user into the absolute 
   * path, After moving to the directory given by the user.
   * Get the pwd i,e getcwd() will return the abs path to that directory.
   * and then append the 'packcore' directory name. Store it in tar_dir.
   * tar_dir will become abs path to the user given directory.
   */

  dir_name = dirname ((char *) tar_dir);
  oldpwd = getcwd (buf_oldpwd, PATH_MAX + 1); 
  status = chdir (dir_name);
  if (status != 0)
  error ("Could not move to the user specified directory\n");
  base_name = basename (tar_dir);
  free (tar_dir);
  tar_dir = getcwd (buf, PATH_MAX + 1);
  strcat (tar_dir, "/");
  strcat (tar_dir, base_name); 

  /* In the packcore directory, create modules.tar containing all the load
   * modules - the main program and all the shared libraries.
   */

  /* Get core file & tar directory name from the command string.
   * Create the tar file name.
   */
  tar_file = (char *) xmalloc (strlen (tar_dir) + strlen ("/modules.tar") + 1);

  strcpy (tar_file, tar_dir);
  strcat (tar_file, "/modules.tar");

  /* Create tar string.
   */
  /* JAGag06192: remove -l option, because the errno returned by tar 
     when hard links are found for shlibs is being misinterpreted. */
  add_to_str (&tar_cmd, &tar_cmd_len, "/usr/bin/tar -chof ");
  add_to_str (&tar_cmd, &tar_cmd_len, tar_file);
  for (obj_file = symfile_objfile; obj_file; obj_file = obj_file->next)
  {
    tmp = xstrdup (obj_file->name);
    if (tmp == NULL)
      error ("Can't packcore. Out of memory");

    if (!file_readable (tmp))	/* Is the library or executable readable? */
      error ("Can't read %s: %s", tmp, safe_strerror (errno));

    dir_name = dirname (tmp);
    file = basename (tmp);

    /* -C is used to remove path from the library while tarring up */
    add_to_str (&tar_cmd, &tar_cmd_len, " -C ");
    add_to_str (&tar_cmd, &tar_cmd_len, dir_name);
    add_to_str (&tar_cmd, &tar_cmd_len, " ");
    add_to_str (&tar_cmd, &tar_cmd_len, file);

    free (tmp);
  }
  add_to_str (&tar_cmd, &tar_cmd_len, " 2>");
  tar_output_file = tar_output_file 
		  ? tar_output_file 
		  : tempnam(NULL, "__gdb_core_tar.out");
  add_to_str (&tar_cmd, &tar_cmd_len, (char*)tar_output_file);

  /* Create the tar.
   */
  if (file_exists (tar_file))	/* Does the tar file exist already? */
    error ("%s already exists", tar_file);

  execute_tar_cmd (tar_cmd, "Can't packcore", 5, NULL, oldpwd);

  if (!file_exists (tar_file))	/* Was the tar successfully created? */
    error ("Unable to create %s", tar_file);

  unlink (tar_output_file);  /* delete the file, keep the name string */
  free (tar_file);
  free (tar_cmd);

  /* Continue with the rest.  Add the other contents and then make
   * packcore.tar.Z from the packcore directory.
   */
  
  packcore_part2 (tar_dir, oldpwd);
  /* free (tar_dir); */
  if (chdir (oldpwd) != 0)
  warning ("Could not go back to the previous directory\n");
}


/*----------------------------------------------------------------------------*/
/* Function	: getcore_command - load up a program and a core file from a
 *			packcore directory after setting GDB_SHLIB_PATH
 *
 * Usage: getcore [ packcore_dir ]
 *
 * Description  : Check to see if a switchover is needed to load the program
 *		  in the packcore directory.  If so, switch over.  If not,
 * 		  set GDB_SHLIB_PATH to point to packcore_dir/modules
 *		  then load up 
 *		  packcore_dir/modules/$(cat packcore_dir/progname.txt)
 * 		  and core file packcore_dir/core.  The user can now to
 *		  a backtrace, etc.
 * Globals	: current_directory is a (char *) which points to a string
 *		    containing the current directory's path name.  Used to
 *		    access the core after cd'ing to the unpack directory.
 *		    gdb_shlib_path_list and gdb_shlib_path_length are saved,
 *		    modified to load libraries from the unpack directory and
 *		    then restored to their original values.
 *
 */
void 
getcore_command (char *command_line, int from_tty)
{
  char *		core_path;
  char *		namefile_path;
  FILE *		namefile;
  char *		modules_path;
  char *		packcore_dir;
  char			progname[MAXPATHLEN];
  char *		progpath;
  char *		saved_command_line;
  int 			saved_gdb_shlib_path_length;
  char **		saved_gdb_shlib_path_list;
  char **		getcore_gdb_shlib_path_list = &modules_path;

  saved_command_line = xstrdup (command_line);

  /* QXCR1000983670: strtok can return invalid strings if
     it is passed NULL, and it holds some string pointer from
     a previous invocation. Do not call strtok with NULL string
     here.
  */
  packcore_dir = NULL;
  if (command_line)
    {
      packcore_dir = get_arg (command_line, NULL);
      if (packcore_dir)
        {
          packcore_dir = xstrdup (packcore_dir);
        }
    }

  if (!packcore_dir)
    {
      packcore_dir = xstrdup ("packcore");
    } 

  core_path = xmalloc (strlen (current_directory) + strlen (packcore_dir) + 20);
  if (packcore_dir[0] != '/')
    {
      strcpy (core_path, current_directory);
      strcat (core_path, "/");
    }
  else
    core_path[0] = 0;
  strcat (core_path, packcore_dir);
  strcat (core_path, "/core");
  if (xmode_exec_format_is_different (core_path))
    xmode_launch_other_gdb (saved_command_line, xmode_getcore);
  free (saved_command_line);

  /* We are running the right gdb.  Set 
   * GDB_SHLIB_PATH to packcore_dir/modules 
   * the executable to packcore_dir/modules/$(cat packcore_dir/progname.txt)
   * and the core file to packcore_dir/core
   */

  /* Load up the executable.  Get the file name from packcore/progname.txt */

  namefile_path = xmalloc (strlen(packcore_dir) + 20);
  strcpy (namefile_path, packcore_dir);
  strcat (namefile_path, "/progname.txt");
  namefile = fopen (namefile_path, "r");
  if (!namefile)
    {
      free (packcore_dir);
      free (core_path);
      error ("Unable to open file with name of program, %s\n", namefile_path);
    }
  free (namefile_path);
  fgets(progname, MAXPATHLEN - 1, namefile);
  fclose (namefile);
  progpath = xmalloc (strlen(packcore_dir) + strlen (progname) + 20);
  strcpy (progpath, packcore_dir);
  strcat (progpath, "/modules/");
  modules_path = strdup (progpath);  // save copy of path to modules directory
  strcat (progpath, progname);
  file_command (progpath, 0);
  free (progpath);

  /* Set the internal gdb_shlib_path to unpack directory.  Load the core; this
   * will check core file availability and validity.  Restore gdb_shlib_path.
   */
  saved_gdb_shlib_path_list = gdb_shlib_path_list;
  saved_gdb_shlib_path_length = gdb_shlib_path_length;

  gdb_shlib_path_list = getcore_gdb_shlib_path_list;
  gdb_shlib_path_length = 1;


  core_file_command (core_path, 1);
  free (core_path);
  free (modules_path);

  gdb_shlib_path_list = saved_gdb_shlib_path_list;
  gdb_shlib_path_length = saved_gdb_shlib_path_length;
  free (packcore_dir);

} // end getcore_command

/*----------------------------------------------------------------------------*/
/* Function	: unpackcore_command ()
 * Usage: unpackcore [ PACKCORE_FILE ] [ CORE_FILE ] 
 *
 * Description  : Thre are two optional arguments, PACKCORE_FILE
 *		  defaults to "packcore.tar.Z", and CORE_FILE is only
 *		  used in the unusual case that the core file didn't fit
 *		  in the PACKCORE_FILE, in which case it is required and
 *		  in other cases must not be given.
 *
 * 		  PACKCORE_FILE must end in ".Z".  If PACKCORE_FILE is
 *		  not given CORE_FILE must not end in ".Z".
 *
 *		  PACKCORE_FILE is uncompressed and then tar is invoked to
 *		  unpack the .tar file which is then removed.  In the packcore
 *		  directory, the modules directory is created from modules.tar.
 *
 *		  getcore is invoked on the resulting directory to load up
 *		  the program and the core file with GDB_SHLIB_PATH set
 *		  to pick up the libraries that were packed with the core file.
 *
 * Input	: The command string issued by the user containing optionally
 *		    the packcore.tar.Z file name and the path to a core file
 *		    if the packcore file does not contain a core file.
 * Output	: Nothing is returned by this function, but a successful call
 *		    would have unpacked the tar file in the unpack directory.
 *		    and established a context for examining the core file.
 * Notes	: This routine will over write existing libraries in the unpack
 *		    directory with the same names as those in the tar file, if
 *		    permissions are right.  Warning the user in this case
 *		    a fair amount of work on gdb's part and it is reasonable
 *		    to expect the user is aware of what directory he or she is
 *		    unpacking to, so the warning logic is not done.  As with
 *		    packcore_command, the tar command is allowed to handle some
 *		    error cases.  Unpacking and loading core file will ignore
 *		    GDB_SHLIB_PATH because the libraries have to be picked up
 *		    from the unpack directory, not some other path.
 */
void 
unpackcore_command (char *command_line, int from_tty)
{

  char *		cmd;
  char *		core_file;
  char *		core_path;
  int			len1 = 0;
  int			len2 = 0; //For the second argument
  char *		packcore_file = NULL;
  char *		fst_arg = NULL;
  char *		sec_arg = NULL;
  int			status;
  char *		tar_path;

  core_file = NULL;
  /* QXCR1000983670: strtok can return invalid strings if
     it is passed NULL, and it holds some string pointer from
     a previous invocation. Do not call strtok with NULL string
     here.
  */
  if (command_line)
    {
      fst_arg = get_arg (command_line, NULL);
      if (!fst_arg)
        packcore_file = xstrdup ("packcore.tar.Z");
      else
        {
          /* QXCR1000792139: - Input provided by the user may be
             1>Packcore file
             2>core file
             3>Packore file and core file (in any order).
          */
          /* Get the second argument if any. */
          sec_arg = get_arg (NULL, NULL);
          if (sec_arg)
            len2 = (int)strlen (sec_arg);

          len1 = (int)strlen (fst_arg);

          /* If there are two arguments from the user then 
             it should packcore file and core file.
          */
          if (fst_arg && sec_arg)
            {  
              /* Look for the packcore file pattern. */
              if (len1 > 2 && fst_arg[len1-1] == 'Z' && fst_arg[len1-2] == '.')
                {
	          packcore_file = xstrdup (fst_arg);
                  core_file = xstrdup (sec_arg);
                }
              else if (len2 > 2 && sec_arg [len2-1] == 'Z' && sec_arg[len2-2] == '.')
                {
                  packcore_file = xstrdup (sec_arg);
                  core_file = xstrdup (fst_arg);
                }
            }
          else
            {
              /* There is only argument. It must be either packcore file
                 or core file.
                 For corefiles, Added another extra error check to avoid 
                 recognizing the *.gz files as corefile. 
              */

              if (len1 > 2 && fst_arg[len1-1] == 'Z' && fst_arg[len1-2] == '.') 
                packcore_file = xstrdup (fst_arg);
              else if (len1 > 2 && fst_arg[len1-1] != 'z'
                       && fst_arg[len1-2] != 'g' && fst_arg[len1-3] != '.')
                {
	          core_file = xstrdup (fst_arg);
	          packcore_file = xstrdup ("packcore.tar.Z");
                }
            }
        }
    }
  else
    {
      packcore_file = xstrdup ("packcore.tar.Z");
    }

  /* Validate for extra arguments. */
  if (packcore_file && core_file)
    {
      if (get_arg (NULL, NULL) != NULL)
        warning ("Unexpected arguments after core file. Ignoring them..\n");
    }      

  /* Check if we have a valid packcore file. */
  if (!packcore_file)
    {
      error ("Invalid arguments.\n"
             "HINT: Unpackcore does not support gzip files.\n"
             "For more information, see help on unpackcore.\n");
    }

  /* Uncompress packcore_file */

  cmd = (char *) xmalloc (strlen(packcore_file) + 50);
  strcpy (cmd, "uncompress ");
  strcat (cmd, packcore_file);
  status = system (cmd);
  status = WEXITSTATUS (status);
  free (cmd);
  if (status != 0)
    {
       error ("Unable to uncompress '%s'\n", packcore_file);
    }

  /* Get rid of ".Z" at end of packcore.tar.Z", then untar the file. */
  packcore_file[strlen (packcore_file) - 2] = 0;
  cmd = (char *) xmalloc (strlen(packcore_file) + 50);
  strcpy (cmd, "tar -xof ");
  strcat (cmd, packcore_file);
  strcat (cmd, " 2> ");
  tar_output_file = tar_output_file 
		  ? tar_output_file 
		  : tempnam(NULL, "__gdb_core_tar.out");
  strcat (cmd, tar_output_file);
  status = execute_tar_cmd (cmd, "Can't unpackcore", 5, packcore_file, NULL);
  free (cmd);

  /* QXCR1000838888:
   * We are done with the .tar file, remove it on success with user 
   * user permission. */
  if (status == 0 && (query ("Unpackcore of %s is done!.\n"
     "Do you want to remove %s ?", packcore_file, packcore_file))) 
    {
      cmd = (char *) xmalloc (strlen (packcore_file) + 20);
      strcpy (cmd, "rm -f ");
      strcat (cmd, packcore_file);
      status = system (cmd);
      free (cmd);
    }
  
  /* Get rid of ".tar" at the end of packcore_file, it is now the
   * packcore directory 
   */

  packcore_file[strlen (packcore_file) - 4] = 0;

  /* If there is a core file, the user must NOT give a CORE_FILE argument.
   * If there is not a core file, the user MUST give a CORE_FILE argument.
   * In the latter case, create a symlink in place of the missing core file
   * pointing to the file provided by the user.  Use file_readable()
   * to check for existence.
   */

  core_path = xmalloc (strlen (packcore_file) + 20);
  strcpy (core_path, packcore_file);
  strcat (core_path, "/core");
  if (file_readable (core_path))
    {
      if (core_file != NULL)
	{
	  warning (
	    "The packcore file contains a core file.  The core file\n"
	    "provided with the command will be ignored.");
	}
    }
  else
    {
      if (core_file == NULL)
	{
	  error (
	    "The packcore file does not contain a core file.\n"
	      "Please place a core file called 'core' in the directory"
	      " %s "
	      "and then examine the core file with the getcore command.", 
	    packcore_file);
	}

      if (!file_readable (core_file))
	{
	  error (
	    "The packcore file does not contain a core file.\n"
	    "and the core file given is not readable.");
	}

      /* Create a symbolic link from core to the core file provided by the
       * user.  Use an abolute path.
       */

      cmd = xmalloc (strlen(packcore_file) + strlen (core_file) 
		     + strlen (current_directory) + 20);
      strcpy (cmd, "ln -s ");

      /* If core_file does not start with '/', then preceed it with
       * current_directory/ to turn it into an absolute path.
       */
      if (core_file[0] != '/') 
	{
	  strcat (cmd, current_directory);
	  strcat (cmd, "/");
	}
      strcat (cmd, core_file);
      strcat (cmd, "  ");
      strcat (cmd, packcore_file);
      strcat (cmd, "/core");
      status = system (cmd);
      status = WEXITSTATUS (status);
      free (cmd);  
      if (status != 0)
	{
	   error ("Unable to make symlink %s/core", packcore_file);
	}
    }
  free (core_file);
  free (core_path);

  /* We need to unpack packcore/modules.tar into packcore/modules.  
   * We make the directory, cd to it and then extract the tar file contents.
   * Make tar_path an absolute path to the tar file.
   */

  tar_path = xmalloc (strlen (current_directory) + strlen (packcore_file) + 20);
  tar_path[0] = 0;
  if (packcore_file[0] != '/')
    {
      strcpy (tar_path, current_directory);
      strcat (tar_path, "/");
    }
  strcat (tar_path, packcore_file);
  strcat (tar_path, "/modules.tar");
  cmd = xmalloc (strlen(tar_path) + strlen (packcore_file) + 100);

 /* QXCR1000838888
  * Remove 'modules' directory if present (due to a previous successful
  * unpackcore) before trying to untar to avoid failure due to permission
  * issues for the modules within.
  */
  strcpy (cmd, "rm -rf "); 
  strcat (cmd, packcore_file);
  strcat (cmd, "/modules");
  if (WEXITSTATUS (system (cmd)))
    warning ("Could not remove old '%s'/modules directory.\n"
             "unpackcore may fail.", packcore_file);
  free (cmd);

  cmd = xmalloc (strlen(tar_path) + strlen (packcore_file) + 100); 
  strcpy (cmd, "mkdir ");
  strcat (cmd, packcore_file);
  strcat (cmd, "/modules ; cd ");
  strcat (cmd, packcore_file);
  strcat (cmd, "/modules ; tar -xof ");
  strcat (cmd, tar_path);
  strcat (cmd, " 2> ");
  tar_output_file = tar_output_file 
		  ? tar_output_file 
		  : tempnam(NULL, "__gdb_core_tar.out");
  strcat (cmd, tar_output_file);
  free (tar_path);
  execute_tar_cmd (cmd, "Can't unpack modules directory", 5, NULL, NULL);
  free (cmd);

  /* We are done with modules.tar, remove it */
  cmd = xmalloc (strlen (packcore_file) + 20);
  strcpy (cmd, "rm -f ");
  strcat (cmd, packcore_file);
  strcat (cmd, "/modules.tar");
  status = system (cmd);
  free (cmd);
  
  /* We now have a packcore_file directory containing a core (possibly a link)
   * Invoke getcore to load up the program, the core file and set
   * GDB_SHLIB_PATH.
   */
  
  getcore_command (packcore_file, from_tty);
  free (packcore_file);

} // end unpackcore_command




/*----------------------------------------------------------------------------*/
/* Function	: file_readable ()
 * Description	: Determines whether or not a file is readable by the current
 *		    gdb session.
 * Input	: Pointer to a file name string.
 * Output	: Returns TRUE if the file is readable, FALSE otherwise.
 * Globals	: - N/A -
 * Notes	: Relies on open() rather than on stat(), getpid(), getgid(), 
 *		    etc.  Makes it simple.
 */
static int 
file_readable (char *file)
{
  int fd;

  fd = open (file, O_RDONLY);
  close (fd);

  if (fd == -1)
    return FALSE;

  return TRUE;
}
/*----------------------------------------------------------------------------*/
/* Function	: file_exists ()
 * Description	: Determines whether or not a given file exists.
 * Input	: Pointer to a file name string.
 * Output	: Returns TRUE if the file is readable, FALSE otherwise.
 * Globals	: - N/A -
 * Notes	: - N/A -
 */
static int 
file_exists (char *file)
{
  int status;
  struct stat stat_buf;

  status = stat (file, &stat_buf);
  if (status == -1)
  {
    if (!(errno == ENOENT || errno == ENOTDIR))
      error ("stat() on %s failed: %s", file, safe_strerror (errno));
    return FALSE;
  }
  else	/* If stat succeeded, then the file exists. */
    return TRUE;
}
/*----------------------------------------------------------------------------*/
/* Function	: get_arg ()
 * Description	: Used to get one argument at a time from a user command string.
 *		    Builds on strtok() and provides error reporting facility if
 *		    argument obtained from the command string is NULL.  Can be
 *		    called upon subsequently just like strtok () which maintains
 *		    state between calls.
 * Input	: Pointer to a user command string and a pointer to an error 
 *		    message.
 * Output	: Pointer to the next argument in the command string.
 * Globals	: - N/A -
 * Notes	: Trashes the argument string by writing '\0' at different 
 *		    points.
 */
static char* 
get_arg (char *arg_str, char *err_msg)
{
  char *tmp;

  tmp = strtok (arg_str, " \t");
  if (tmp == NULL && err_msg != NULL)
    error (err_msg);

  return tmp;
}
/*----------------------------------------------------------------------------*/
/* Function	: add_to_str ()
 * Description	: Adds string s2 to string s1.  This is similar to strcat except
 *		    in one way.  If the size of the buffer in which s1 resides 
 *		    isn't enough to do the concatenation, then s1's buffer is
 *		    expanded with a realloc().  If s1 is NULL, a buffer is 
 *		    allocated for it.
 * Input	: A pointer to string s1's pointer, pointer to the length of s1
 *		    and pointer to string s2.
 * Output	: Nothing is returned, but s1 is modified by appending s2 at the
 *		    end and s1's size is updated.
 * Globals	: - N/A -
 * Notes	: The size of s1's buffer is independent of s1's size.
 */
static void 
add_to_str (char **main_str, int *main_str_buf_len, char *add_str)
{
  if (*main_str == NULL)
  {
    *main_str = (char *) xmalloc (PATH_MAX);
    *main_str_buf_len = PATH_MAX;
    **main_str = '\0';
  }

  if ((strlen (*main_str) + strlen (add_str) + 1) > *main_str_buf_len)
  {
    *main_str = (char *) realloc (*main_str, *main_str_buf_len * 2);
    if (*main_str == NULL)
      error ("Can't packcore. Out of memory");
    *main_str_buf_len *= 2;
  }

  strcat (*main_str, add_str);
}
/*----------------------------------------------------------------------------*/
/* Function	: execute_tar_cmd ()
 * Description	: Takes a user specified tar command and executes it.  If the 
 *		    tar command was unsuccessful, it dumps the output of the 
 *		    command along with an error message.
 * Input	: Pointer to a command string and a pointer to an error message.
 *		  and an error status to treat as zero (may be zero).
 *		  For untarring, we get status 5 if files are not writable.
 *		  'tar_file' is a pointer to the destination file name.
 *		  oldpwd is a  pointer to the last pwd.
 * Output	: return status of tar cmd, error messages are printed on a
 *                failure.
 * Globals	: - N/A -
 * Notes	: This routine can be used to execute any command and have its
 *		    output displayed, with some minor modifications.
 */
static int 
execute_tar_cmd (char *tar_cmd, char *err_msg, int ignore_err, char *tar_file, char *oldpwd)
{
  int status;

  status = system (tar_cmd);
  status = WEXITSTATUS (status);
  /* QXCR1000838888
   * When we extract files with/without read-only permissions, we end up with a
   * status of 0, which we treat as success. When the user again tries to
   * extract the files, tar will return 5 because of the existing files with
   * read-only permission (tar fails to overwrite these files). In this case,
   * the user could try to extract the files in another folder or should change
   * the files permission from read-only mode to write mode in the current
   * directory.
   */
  if (info_verbose || (status != 0 && status != ignore_err))
  {
    dump_tar_output ();
  }

  /* This change dir is to go back to the previous directory.
   * Required only for packcore.
   */
  if (status != 0 && tar_file == NULL && oldpwd != NULL)
   {
     if (chdir (oldpwd) != 0)
     warning ("Could not go back to the previous directory\n");
   }
 
   /* QXCR1000838888
    * This block is to roll-back (recreate packcore.tar.Z file).
    * As we know that we reach here after uncompressing packcore.tar.Z, but
    * after an unsucessful untar. So we are left with packcore.tar on disk.
    * Finding packcore.tar instead of packcore.tar.Z would be unexpected
    * for the user. Moreover, gdb complains when the user again tries to
    * unpackcore the same file "packcore.tar.Z" because it will not find
    * the '.Z' extension. This should be done before calling error() as we
    * never get back here.
    */
  if ((status != 0) && (tar_file != NULL))
  {
    char *tar_file_dup = xstrdup (tar_file);
    int stat;
    char *cmd  = xmalloc (strlen ("compress ") + strlen (tar_file_dup) + 1);
    strcpy (cmd, "compress ");
    strcat (cmd, tar_file_dup); 
    stat = system (cmd);
    stat = WEXITSTATUS (stat);
    free (tar_file_dup);
    free (cmd);
    if (stat != 0)
      error ("Could not unpackcore and recreate uncompressed '%s.Z'\n",
              tar_file);
  }

  if (status != 0 && status != ignore_err)
    error ("%s.  Tar cmd failed.  Tar cmd: %s", err_msg, tar_cmd);
  if (status == ignore_err && tar_file != NULL)
  {
    char *tar_file_dup = xstrdup (tar_file); /* This is to peel off .tar */ 
    tar_file_dup [strlen (tar_file_dup) - 4]= '\0';
    error ("\nUnpackcore failed either due to no write permission in the\n"
           "current directory or the previously extracted folder '%s' or "
           "'%s/[files]'\n"
           "are in read-only mode. Please retry after changing the permission"
           " of the files\n"
           "and folder from read to write mode or remove the folder/files.\n\n"
           , tar_file_dup, tar_file_dup);
  }
  else if (status == ignore_err && tar_file == NULL)
    error ("Packcore failed since the files to be tarred are not present.\n");
  
  return (status);
}

/*----------------------------------------------------------------------------*/
/* Function	: dump_tar_output ()
 * Description	: Displays the contents of the tar output file.  
 * Input	: - N/A -
 * Output	: - N/A -
 * Globals	: tar_output_file is used to find the file from which the 
 *		    contents are displayed.
 * Notes	: With slight modification, this routine can be made generic 
 *		    enough to dump the contents of any text file.
 */
static void 
dump_tar_output (void)
{
  FILE *fd;
  char buf[PACKCORE_BUF_SIZE];

  tar_output_file = tar_output_file 
		  ? tar_output_file 
		  : tempnam(NULL, "__gdb_core_tar.out");
  fd = fopen (tar_output_file, "r");
  if (fd == NULL)
    error ("Can't open tar output file %s: %s", tar_output_file, 
    						safe_strerror (errno));
    
  while (!feof(fd))
  {
    fscanf (fd, "%[^\n]s", buf);
    fgetc (fd);
    printf_filtered ("%s\n", buf);
  }
  fclose (fd);
}
/*----------------------------------------------------------------------------*/
