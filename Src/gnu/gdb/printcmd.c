/* Print values for GNU debugger GDB.
   Copyright 1986-1991, 1993-1995, 1998, 2000 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>
#include <ctype.h>	/* for isspace() */
#include "defs.h"
#include "gdb_string.h"
#include "frame.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "inferior.h"
#include "value.h"
#include "language.h"
#include "expression.h"
#include "gdbcore.h"
#include "gdbcmd.h"
#include "target.h"
#include "breakpoint.h"
#include "demangle.h"
#include "valprint.h"
#include "annotate.h"
#include "symfile.h"		/* for overlay functions */
#include "objfiles.h"		/* ditto */
#include "f-lang.h"		/* for array length stuff */
#include "gdbthread.h"

#ifdef UI_OUT
#include "ui-out.h"
#endif

struct frame_info *currently_printing_frame;        /* JAGae92961 */

#ifdef HP_IA64
#include <a.out.h>
#include "elf-bfd.h"
#include "objfiles.h"
#include "som.h"
#define  EM_IA64 50

#endif /*HP_IA64 */

#include "ui-file.h"
#ifdef TUI
#include "tui-file.h"
#endif /* TUI */

#ifdef REGISTER_STACK_ENGINE_FP
#define SIZE_OF_LOCALS(cfm)      (((cfm) & 0x3f80) >> 7)

/* A NaT collection is written to the RSE BS when the address bits 8:3
   are zero, i.e. the BS address of a register ored with ~0x1f8 has the
   nat collection for that register.  
   */

#define NEXT_NAT_COLLECTION_ADDR(bs_addr) (((bs_addr) + 0x200) & (~0x1f8))

/* The Runtime document specified that SP is always 0 when mod 16. */
#define SP_INCR		16
#endif

extern int asm_demangle;	/* Whether to demangle syms in asm printouts */
extern int addressprint;	/* Whether to print hex addresses in HLL " */
extern int dont_print_value;

extern int firebolt_version;
int assembly_level_debugging = 0;
int hp_ia64_testsuite = 0;
extern int dont_print_value;

#ifdef HP_IA64
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
extern CORE_ADDR skip_prologue_hard_way (CORE_ADDR pc);
#endif /*HP_IA64 */

struct format_data
  {
    int count;
    char format[2];
    char size;
  };

/* Last specified output format.  */

static char last_format[2] = {'x', '?'};

/* Last specified examination size.  'b', 'h', 'w' or `q'.  */

static char last_size = 'w';

/* Default address to examine next.  */

static CORE_ADDR next_address;

/* Default section to examine next. */

static asection *next_section;

/* Last address examined.  */

static CORE_ADDR last_examine_address;

/* Contents of last address examined.
   This is not valid past the end of the `x' command!  */

static value_ptr last_examine_value;

/* Largest offset between a symbolic value and an address, that will be
   printed as `0x1234 <symbol+offset>'.  */

static unsigned int max_symbolic_offset = UINT_MAX;

/* Append the source filename and linenumber of the symbol when
   printing a symbolic value as `<symbol at filename:linenum>' if set.  */
static int print_symbol_filename = 0;

extern void strcat_longest (int format, int use_local, LONGEST val_long,
			    char *buf, int buflen);

extern int open_source_file (struct symtab *s);

extern void find_source_lines (struct symtab *s, int desc);

void refresh_display_chain (void);

/* Number of auto-display expression currently being displayed.
   So that we can disable it if we get an error or a signal within it.
   -1 when not doing one.  */

int current_display_number;

/* Flag to low-level print routines that this value is being printed
   in an epoch window.  We'd like to pass this as a parameter, but
   every routine would need to take it.  Perhaps we can encapsulate
   this in the I/O stream once we have GNU stdio. */

int inspect_it = 0;

/* Srikanth, Jul 16th 2002, function_calls_disallowed, a boolean that
   controls whether function calls are allowed just now. When the WDB-GUI
   user hovers the mouse over an expression, the GUI issues a command to
   evaluate the expression pointed to. If this expression contains a
   call to some function, the results could be disastrous : simply moving
   the mouse around could change the program context and cause undesirable
   side effects !!!

   The GUI is smart enough not to do this in the simple cases. However with
   C++ and operator overloading an expression like blah[i] may involve 
   function call and the GUI has no way of knowing that.

   This problem is found at Intel.
*/

int function_calls_disallowed = 0;

struct display
  {
    /* Chain link to next auto-display item.  */
    struct display *next;
    /* Expression to be evaluated and displayed.  */
    struct expression *exp;
    /* Item number of this auto-display item.  */
    int number;
    /* Display format specified.  */
    struct format_data format;
    /* Innermost block required by this expression when evaluated */
    struct block *block;
    /* Status of this display (enabled or disabled) */
    enum enable status;
    /* save the expression string */
    char *exp_string;
  };

/* Chain of expressions whose values should be displayed
   automatically each time the program stops.  */

static struct display *display_chain;

static int display_number;

/* Pointer to the target-dependent disassembly function.  */

int (*tm_strcat_insn) PARAMS ((bfd_vma, disassemble_info *, char *, int));

int dimension, dim[10];  /* JAGae36708 - array browsing */

char *ascii_filename, *array_name;  /* JAGae36708 - array browsing */

/* Prototypes for exported functions. */

void output_command (char *, int);

void _initialize_printcmd (void);

/* Prototypes for local functions. */

static void delete_display (int);

static void enable_display (char *, int);

static void disable_display_command (char *, int);

static void disassemble_command (char *, int);

static void printf_command (char *, int);

static void print_frame_nameless_args (struct frame_info *, long,
                                       int, int, struct ui_file *);

static void display_info (char *, int);

static void do_one_display (struct display *);

static void undisplay_command (char *, int);

static void free_display (struct display *);

static void display_command (char *, int);

void x_command (char *, int);

static void address_info (char *, int);

static void set_command (char *, int);

static void call_command (char *, int);

static void inspect_command (char *, int);

static void print_command (char *, int);

static void output_command_no_values (char *, int);

static void print_command_1 (char *, int, int);

static void validate_format (struct format_data, char *);

static void do_examine (struct format_data, CORE_ADDR addr,
			asection * section);

void print_formatted (value_ptr, char[], int, struct ui_file *);

static struct format_data decode_format (char **, char[], int);

int print_insn (CORE_ADDR, struct ui_file *);

int sym_info_rtc (char *, int, FILE *);
static void sym_info (char *, int);
     static void output_command_no_values PARAMS ((char *, int));

#ifdef HP_IA64
/* Function definations for RTTI */
#define PTR_SIZE (is_swizzled ? 4 : 8)
#define RETURN_ERR \
{\
  error ("No RTTI found." \
         "\nThis might be due to invalid address of " \
         "C++ polymorphic object.");\
  return;\
}

static void rtti_info (char *, int);
static void print_cxx_object (CORE_ADDR);
static void print_vtable2 (CORE_ADDR, CORE_ADDR);
static void print_type_info_3 (CORE_ADDR, CORE_ADDR, CORE_ADDR);
static void __do_type_info1 (CORE_ADDR ti, void *context, int indent,
              long offset);
#endif

static void dump2file_command (char *, int); /* JAGae36708 - array browsing */

void print_array_in_matrix(void); /* JAGae36708 - array browsing */

/* Decode a format specification.  *STRING_PTR should point to it.
   OFORMAT and OSIZE are used as defaults for the format and size
   if none are given in the format specification.
   If OSIZE is zero, then the size field of the returned value
   should be set only if a size is explicitly specified by the
   user.
   The structure returned describes all the data
   found in the specification.  In addition, *STRING_PTR is advanced
   past the specification and past all whitespace following it.  */

static struct format_data
decode_format (char **string_ptr, char oformat[], int osize)
{
  struct format_data val;
  register char *p = *string_ptr;

  val.format[0] = '?';
  val.format[1] = '?';
  val.size = '?';
  val.count = 1;

  if (*p >= '0' && *p <= '9')
    val.count = atoi (p);
  while (*p >= '0' && *p <= '9')
    p++;

  /* Now process size or format letters that follow.  */

  while (1)
    {
      if (*p == 'b' || *p == 'h' || *p == 'w' || *p == 'g')
	val.size = *p++;
      else if (*p >= 'a' && *p <= 'z')
        {
	  val.format[0] = *p++;
#ifdef HP_IA64
	  if (*p == 'b' || *p == 'f' || *p == 'd' || *p == 'l')
#else
          if (*p == 'b')
#endif
            val.format[1] = *p++;
	    break;
        }
      /* JAGaf75858: support printing wide chars */
      else if (*p == 'W') 
	val.format[0] = *p++;
      else
	break;
    }

  if ((*p != ' ' || *p != '\t' || *p != '\n') &&
      (*p >= 'a' && *p <= 'z'))
    error ("Invalid format letter specifier.");

  while (*p == ' ' || *p == '\t')
    p++;
  *string_ptr = p;

  /* Set defaults for format and size if not specified.  */
  if (val.format[0] == '?' && val.format[1] == '?')
    {
      if (val.size == '?')
	{
	  /* Neither has been specified.  */
	  val.format[0] = oformat[0];
	  val.format[1] = oformat[1];
	  val.size = osize;
	}
      else
	/* If a size is specified, any format makes a reasonable
	   default except 'i'.  */
	val.format[0] = oformat[0] == 'i' ? 'x' : oformat[0];
    }
  else if (val.size == '?')
    switch (val.format[0])
      {
      case 'a':
      case 's':
	/* Pick the appropriate size for an address.  */
	if (TARGET_PTR_BIT == 64)
	  val.size = osize ? 'g' : osize;
	else if (TARGET_PTR_BIT == 32)
	  val.size = osize ? 'w' : osize;
	else if (TARGET_PTR_BIT == 16)
	  val.size = osize ? 'h' : osize;
	else
	  /* Bad value for TARGET_PTR_BIT */
	  abort ();
	break;
      case 'f':
      case 'l':
	/* Floating point has to be word or giantword.  */
	if (osize == 'w' || osize == 'g')
	  val.size = osize;
	else
	  /* Default it to giantword if the last used size is not
	     appropriate.  */
	  val.size = osize ? 'g' : osize;
	break;
      case 'c':
	/* Characters default to one byte.  */
	val.size = osize ? 'b' : osize;
	break;
      case 'd':
#ifdef HP_IA64
        if (val.format[1] == 'b' ||
            val.format[1] == 'd' ||
            val.format[1] == 'f' ||
            val.format[1] == 'l')
#else
	if (val.format[1] == 'b')
#endif
          if (osize == 'w' || osize == 'g')
            val.size = osize;
          else
            val.size = osize ? 'g' : osize;
        else if(val.format[1] == '?')
	  val.size = osize;
        break;
      default:
	/* The default is the size most recently specified.  */
	val.size = osize;
      }

  return val;
}

/* Print value VAL on stream according to FORMAT, a letter or 0.
   Do not end with a newline.
   0 means print VAL according to its own type.
   SIZE is the letter for the size of datum being printed.
   This is used to pad hex numbers so they line up.  */

void
print_formatted (register value_ptr val, char format[], int size, struct ui_file *stream)
{
  struct type *type = check_typedef (VALUE_TYPE (val));
  /* JAGaf75858: handle wide chars */
  struct type *t_type = TYPE_TARGET_TYPE (type); 
  int len = TYPE_LENGTH (type);

  if (VALUE_LVAL (val) == lval_memory)
    {
      next_address = VALUE_ADDRESS (val) + len;
      next_section = VALUE_BFD_SECTION (val);
    }

  /* If the value is not available, print the message and return */
  switch (VALUE_AVAILABILITY (val)) 
    {
      case VA_OPTIMIZED_OUT:
        fputs_filtered ("<value optimized out>", stream);
        return;

      case VA_NOT_AVAILABLE_HERE:
        fprintf_filtered (stream, "<value unavailable at address 0x%llx>", 
                          VALUE_ADDRESS(val));
        return;

      case VA_NOT_A_THING:
        fputs_filtered ("<NaT>", stream);
        return;
    };

  assert (VALUE_AVAILABILITY (val) == VA_AVAILABLE);

  switch (format[0])
    {
    case 's':
      /* FIXME: Need to handle wchar_t's here... */
      next_address = VALUE_ADDRESS (val)
	+ val_print_string (VALUE_ADDRESS (val), -1, 1, stream);
      next_section = VALUE_BFD_SECTION (val);
      break;

    case 'i':
      /* The old comment says
         "Force output out, print_insn not using _filtered".
         I'm not completely sure what that means, I suspect most print_insn
         now do use _filtered, so I guess it's obsolete.
         --Yes, it does filter now, and so this is obsolete.  -JB  */

      /* We often wrap here if there are long symbolic names.  */
      wrap_here ("    ");
      next_address = VALUE_ADDRESS (val)
	+ print_insn (VALUE_ADDRESS (val), stream);
      next_section = VALUE_BFD_SECTION (val);
      break;

    default:
      if (format[0] == 0
	  || TYPE_CODE (type) == TYPE_CODE_ARRAY
	  || TYPE_CODE (type) == TYPE_CODE_STRING
	  || TYPE_CODE (type) == TYPE_CODE_STRUCT
	  || TYPE_CODE (type) == TYPE_CODE_CLASS
	  || TYPE_CODE (type) == TYPE_CODE_UNION
	  || TYPE_CODE (type) == TYPE_CODE_REF) /* QXCR1000590211:Unable to print references in hex. */
	/* If format is 0, use the 'natural' format for
	 * that type of value.  If the type is non-scalar,
	 * we have to use language rules to print it as
	 * a series of scalars.
	 */
	value_print (val, stream, format, Val_pretty_default);
      /* JAGaf75858: handle wide chars */
      /* Wide chars are stored as unsigned ints - 4 bytes*/
      else if (format[0] == 'W') {
        /* check whether the type is of 'unsigned int' (length 4) or
           the target_type is of type unsigned int
         */
        if ((!t_type && len == sizeof(unsigned int)) || (t_type && TYPE_LENGTH(t_type) == sizeof(unsigned int)))
	  value_print (val, stream, format, Val_pretty_default);
        else 
          error ("Output format \"%c\" is applicable to only wide characters", format[0]);
      }
      else
	/* User specified format, so don't look to the
	 * the type to tell us what to do.
	 */
	print_scalar_formatted (VALUE_CONTENTS (val), type,
				format, size, stream);
    }
}

/* Print a scalar of data of type TYPE, pointed to in GDB by VALADDR,
   according to letters FORMAT and SIZE on STREAM.
   FORMAT may not be zero.  Formats s and i are not supported at this level.

   This is how the elements of an array or structure are printed
   with a format.  */

void
print_scalar_formatted (char *valaddr, struct type *type, char format[], int size,
			struct ui_file *stream)
{
  LONGEST val_long = 0;
  unsigned int len = TYPE_LENGTH (type);

  if (len > sizeof (LONGEST)
      && (format[0] == 't'
	  || format[0] == 'c'
	  || format[0] == 'o'
	  || format[0] == 'u'
	  || format[0] == 'd'
	  || format[0] == 'x') &&
#ifdef HP_IA64
      TYPE_CODE (type) != TYPE_CODE_DECFLOAT &&
      TYPE_CODE (type) != TYPE_CODE_FLT &&
      TYPE_CODE (type) != TYPE_CODE_FLOAT80 &&
#endif
      (format[1] != 'b'))
    {
      if (!TYPE_UNSIGNED (type)
	  || !extract_long_unsigned_integer (valaddr, len, &val_long))
	{
	  /* We can't print it normally, but we can print it in hex.
	     Printing it in the wrong radix is more useful than saying
	     "use /x, you dummy".  */
	  /* FIXME:  we could also do octal or binary if that was the
	     desired format.  */
	  /* FIXME:  we should be using the size field to give us a
	     minimum field width to print.  */

	  if (format[0] == 'o')
	    print_octal_chars (stream, (unsigned char *) valaddr, len);
	  else if (format[0] == 'd')
	    print_decimal_chars (stream, (unsigned char *) valaddr, len);
	  else if (format[0] == 't')
	    print_binary_chars (stream, (unsigned char *) valaddr, len);
	  else
	    /* replace with call to print_hex_chars? Looks
	       like val_print_type_code_int is redoing
	       work.  - edie */

	    val_print_type_code_int (type, valaddr, stream);

	  return;
	}

      /* If we get here, extract_long_unsigned_integer set val_long. 
         We dont extract if format is either binary floating point or
         decimal floating point data type. */
    }
  else if ((format[0] != 'f' || format[0] != 'd' ||
           format[0] != 'l') &&
#ifdef HP_IA64
           /* Ignore if its floating point(binary and decimal). */
           (format[1] != 'f' || format[1] != 'd' ||
            format[1] != 'l' || format[1] != 'b'))
#else
           (format[1] != 'b'))
#endif
    val_long = unpack_long (type, valaddr);

  /* If we are printing it as unsigned, truncate it in case it is actually
     a negative signed value (e.g. "print/u (short)-1" should print 65535
     (if shorts are 16 bits) instead of 4294967295).  */
  if (format[0] != 'd')
    {
      if (len < sizeof (LONGEST))
	val_long &= ((LONGEST) 1 << HOST_CHAR_BIT * len) - 1;
    }

  switch (format[0])
    {
    case 'x':
      if (!size)
	{
          if (is_swizzled &&
              (TYPE_CODE(type) == TYPE_CODE_PTR ||
               TYPE_CODE(type) == TYPE_CODE_REF))
            val_long = SWIZZLE (val_long);

	  /* no size specified, like in print.  Print varying # of digits. */
	  print_longest (stream, 'x', 1, val_long);
	}
      else
	switch (size)
	  {
	  case 'b':
	  case 'h':
	  case 'w':
	  case 'g':
	    print_longest (stream, size, 1, val_long);
	    break;
	  default:
	    error ("Undefined output size \"%c\".", size);
	  }
      break;

    case 'd':
      {
        if (format[1])
	  switch (format[1])
            {
	      case '?':
                print_longest (stream, 'd', 1, val_long);
      	        break;

	      case 'b':
                print_floating (valaddr, type, stream, sizeof(double));
      	        break;
#ifdef HP_IA64
              /* Format letters df, dd and dl supported only for IPF. 
                 Print the value in decimal floating point format*/
	      case 'f':
                print_decimal_floating (valaddr, type, stream, sizeof(DECIMAL32));
	        break;

	      case 'd':
		print_decimal_floating (valaddr, type, stream, sizeof(DECIMAL64));
		break;

	      case 'l':
		print_decimal_floating (valaddr, type, stream, sizeof(DECIMAL128));
		break;
#endif
	    }
        else
	  print_longest (stream, 'd', 1, val_long); 
      }
      break;

    case 'u':
      print_longest (stream, 'u', 0, val_long);
      break;

    case 'o':
      if (val_long)
	print_longest (stream, 'o', 1, val_long);
      else
	fprintf_filtered (stream, "0");
      break;

    case 'a':
      {
	/* Truncate address to the size of a target pointer, avoiding
	   shifts larger or equal than the width of a CORE_ADDR.  The
	   local variable PTR_BIT stops the compiler reporting a shift
	   overflow when it won't occure. */
	CORE_ADDR addr = unpack_pointer (type, valaddr);
	int ptr_bit = TARGET_PTR_BIT;
	if (ptr_bit < (sizeof (CORE_ADDR) * HOST_CHAR_BIT))
	  addr &= ((CORE_ADDR) 1 << ptr_bit) - 1;
	print_address (addr, stream);
      }
      break;

    case 'c':
      value_print (value_from_longest (builtin_type_true_char, val_long),
		   stream, 0, Val_pretty_default);
      break;

    case 'f':
      print_floating (valaddr, type, stream, sizeof(float));
      break;

    case 'l':
      print_floating (valaddr, type, stream, sizeof(DOUBLEST));
      break;

    case 0:
      abort ();

    case 't':
      /* Binary; 't' stands for "two".  */
      {
	char bits[8 * (sizeof val_long) + 1];
	char buf[8 * (sizeof val_long) + 32];
	char *cp = bits;
	int width = 0;

	if (!size)
	  width = 8 * (sizeof val_long);
	else
	  switch (size)
	    {
	    case 'b':
	      width = 8;
	      break;
	    case 'h':
	      width = 16;
	      break;
	    case 'w':
	      width = 32;
	      break;
	    case 'g':
	      width = 64;
	      break;
	    default:
	      error ("Undefined output size \"%c\".", size);
	    }

	bits[width] = '\0';
	while (width-- > 0)
	  {
	    bits[width] = (val_long & 1) ? '1' : '0';
	    val_long >>= 1;
	  }
	if (!size)
	  {
	    while (*cp && *cp == '0')
	      cp++;
	    if (*cp == '\0')
	      cp--;
	  }
	strcpy (buf, local_binary_format_prefix ());
	strcat (buf, cp);
	strcat (buf, local_binary_format_suffix ());
	fprintf_filtered (stream, buf);
      }
      break;

    case 'W': 
      /* JAGaf75858 : we handle 'W' as a format for printing wide chars. 
         However, the flow won't reach here in case the user really wants to
         print a wide char */
      error ("Output format \"%c\" is applicable to only wide characters", format[0]);
    default:
      error ("Undefined output format \"%c\".", format[0]);
    }
}

/* Specify default address for `x' command.
   `info lines' uses this.  */

void
set_next_address (CORE_ADDR addr)
{
  next_address = addr;

  /* Make address available to the user as $_.  */
  set_internalvar (lookup_internalvar ("_"),
		   value_from_pointer (lookup_pointer_type (builtin_type_void),
				       addr));
}

/* Optionally print address ADDR symbolically as <SYMBOL+OFFSET> on STREAM,
   after LEADIN.  Print nothing if no symbolic name is found nearby.
   Optionally also print source file and line number, if available.
   DO_DEMANGLE controls whether to print a symbol in its native "raw" form,
   or to interpret it as a possible C++ name and convert it back to source
   form.  However note that DO_DEMANGLE can be overridden by the specific
   settings of the demangle and asm_demangle variables.  */

void
print_address_symbolic (CORE_ADDR addr, struct ui_file *stream, int do_demangle, char *leadin)
{
  char *name = NULL;
  char *filename = NULL;
  int unmapped = 0;
  int offset = 0;
  int line = 0;

  /* throw away both name and filename */
  struct cleanup *cleanup_chain = make_cleanup (free_current_contents, &name);
  make_cleanup (free_current_contents, &filename);

  if (build_address_symbolic (addr, do_demangle, &name, &offset, &filename, &line, &unmapped))
    {
      do_cleanups (cleanup_chain);
      return;
    }

  fputs_filtered (leadin, stream);
  if (unmapped)
    fputs_filtered ("<*", stream);
  else
    fputs_filtered ("<", stream);
  fputs_filtered (name, stream);
  if (offset != 0)
    /* Fix for JAGae65464.	- Bharath, Apr 30 2003. */
    fprintf_filtered (stream, "+%#x", (unsigned int) offset);

  /* Append source filename and line number if desired.  Give specific
     line # of this addr, if we have it; else line # of the nearest symbol.  */
  if (print_symbol_filename && filename != NULL)
    {
      if (line != -1)
        fprintf_filtered (stream, " at %s:%d", filename, line);
      else
        fprintf_filtered (stream, " in %s", filename);

  if (nimbus_version && assembly_level_debugging && strlen (name) > 8)
    {
      char truncated_name[9];
      
      strncpy(truncated_name, name, 7);
      truncated_name[7] = '*';
      truncated_name[8] = '\0';
      fputs_filtered (truncated_name, stream);
    }
  else 
    {
      fputs_filtered (name, stream);
    }
 
    }
  if (unmapped)
    fputs_filtered ("*>", stream);
  else
    fputs_filtered (">", stream);

  do_cleanups (cleanup_chain);
}

#ifdef HP_IA64
/* Is ADDR a possible IA64 instruction address?
 * FIXME: Currently we return TRUE if the address falls inside
 *  the segment ".text" of any objectfile. Will return a TRUE
 *  even for addresses of constants kept in the text segment
 *  by using a (rarely used?) compiler option.
 * 
 *  jini: 060511: Mixed mode support: Added a check to test
 *  if this is a PA library address. PA library addresses for mixed mode
 *  binaries have to be differentiated from IA64 instruction addresses
 *  since no slot numbers should be printed for these.
 *
 *  jini: Nov 2006: Mixed mode support: JAGag21715: Added a check
 *  for PA32 SOM libraries also.
 */
static int
is_ia64_instruction_address (CORE_ADDR addr)
{
  struct obj_section *objsec;
  struct som_exec_data  *somdata = NULL;
  unsigned short e_machine = 0;

  /* Get the section whose range includes ADDR */
  objsec = find_pc_section (addr);
  if (objsec)
    {
      e_machine = elf_elfheader(objsec->objfile->obfd)->e_machine;
      if (e_machine == EM_IA64)
        {
          /* See if this is the ".text" section */
          if (STREQ (objsec->the_bfd_section->name, ".text"))
            {
	      return 1;
            }
        }
    }
  return 0;  /* default: ADDR is NOT an ia64 instruction address */
}

#endif /*HP_IA64 */

/* Given an address ADDR return all the elements needed to print the
   address in a symbolic form. NAME can be mangled or not depending
   on DO_DEMANGLE (and also on the asm_demangle global variable,
   manipulated via ''set print asm-demangle''). Return 0 in case of
   success, when all the info in the OUT paramters is valid. Return 1
   otherwise. */
int
build_address_symbolic (CORE_ADDR addr,  /* IN */
                        int do_demangle, /* IN */
                        char **name,     /* OUT */
                        int *offset,     /* OUT */
                        char **filename, /* OUT */
                        int *line,       /* OUT */
                        int *unmapped)   /* OUT */
{
  struct minimal_symbol *msymbol;
  struct symbol *symbol;
  struct symtab *symtab = 0;
  CORE_ADDR name_location = 0;
  CORE_ADDR size = 0;
  asection *section = 0;
  char *name_temp = "";
  int align_var = 0;

  /* Let's say it is unmapped. */
  *unmapped = 0;

  /* Determine if the address is in an overlay, and whether it is
     mapped. */
  if (overlay_debugging)
    {
      section = find_pc_overlay (addr);
      if (pc_in_unmapped_range (addr, section))
        {
          *unmapped = 1;
          addr = overlay_mapped_address (addr, section);
        }
    }

  /* On some targets, add in extra "flag" bits to PC for
     disassembly.  This should ensure that "rounding errors" in
     symbol addresses that are masked for disassembly favour the
     the correct symbol. */

#ifdef GDB_TARGET_UNMASK_DISAS_PC
  addr = GDB_TARGET_UNMASK_DISAS_PC (addr);
#endif

  /* First try to find the address in the symbol table, then
     in the minsyms.  Take the closest one.  */

  /* This is defective in the sense that it only finds text symbols.  So
     really this is kind of pointless--we should make sure that the
     minimal symbols have everything we need (by changing that we could
     save some memory, but for many debug format--ELF/DWARF or
     anything/stabs--it would be inconvenient to eliminate those minimal
     symbols anyway).  */
  msymbol = lookup_minimal_symbol_by_pc_section (addr, section);
  symbol = find_pc_sect_function (addr, section);

  if (symbol)
    {
      name_location = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (symbol)));
      if (do_demangle)
        name_temp = SYMBOL_SOURCE_NAME (symbol);
      else
        name_temp = SYMBOL_LINKAGE_NAME (symbol);
    }

  if (msymbol != NULL)
    {
      if (SYMBOL_VALUE_ADDRESS (msymbol) > name_location || symbol == NULL)
        {
          /* The msymbol is closer to the address than the symbol;
             use the msymbol instead.  */
          symbol = 0;
          symtab = 0;
          name_location = SYMBOL_VALUE_ADDRESS (msymbol);
          if (do_demangle)
            name_temp = SYMBOL_SOURCE_NAME (msymbol);
          else
            name_temp = SYMBOL_LINKAGE_NAME (msymbol);
        }
    }
  if (symbol == NULL && msymbol == NULL)
    return 1;

  /* On some targets, mask out extra "flag" bits from PC for handsome
     disassembly. */

#ifdef GDB_TARGET_MASK_DISAS_PC
  name_location = GDB_TARGET_MASK_DISAS_PC (name_location);
  addr = GDB_TARGET_MASK_DISAS_PC (addr);
#endif

  /* If the nearest symbol is too far away, don't print anything symbolic.  */

  /* For when CORE_ADDR is larger than unsigned int, we do math in
     CORE_ADDR.  But when we detect unsigned wraparound in the
     CORE_ADDR math, we ignore this test and print the offset,
     because addr+max_symbolic_offset has wrapped through the end
     of the address space back to the beginning, giving bogus comparison.  */
  if (addr > name_location + max_symbolic_offset
      && name_location + max_symbolic_offset > name_location)
    return 1;

#ifdef HP_IA64
  /* Poorva: 01/21/2003 
     used by disassemble and info break kind of commands
     We now check if the pc has gone beyond the end address of 
     this symbol and if we have then we print it
     as being in unknown region or ?? instead of printing the 
     previous functions name.

     There are regions in libc eg handcoded assembly which we 
     don't have linker symbols for and don't know which function
     we're in and just have the address. 
     The align_var is to bundle align the end address.
     e.g start addr = 20 and size = 40 
     so end_addr = 60. It could be 60:0, 60:1 and 60:2 
     The next function will not start at 60 for sure. 
     A new function is guaranteed to start at a bundle aligned address.
     */

  /* jini: mixed mode changes: JAGag21715: The code below should
     be valid only for IPF addresses. Avoid for mixed mode PA addresses */
  if (!in_mixed_mode_pa_lib (addr, NULL))
    {
      size = (CORE_ADDR) MSYMBOL_INFO (msymbol);
      align_var = (int) (2 - (size % 3));
      if (size 
          && ((addr - name_location) > (size + align_var)))
        {
          *offset = 0;
          *name = strdup ("??");
        }
      else
        {
          *offset = (int) (addr - name_location);
          *name = xstrdup (name_temp);
        }
    }
  else
#endif
  {
    *offset = (int) (addr - name_location);
    *name = xstrdup (name_temp);
  }

  if (print_symbol_filename)
    {
      struct symtab_and_line sal;

      sal = find_pc_sect_line (addr, section, 0);

      if (sal.symtab)
        {
          *filename = xstrdup (sal.symtab->filename);
          *line = sal.line;
        }
      else if (symtab && symbol && symbol->line)
        {
          *filename = xstrdup (symtab->filename);
          *line = symbol->line;
        }
      else if (symtab)
        {
          *filename = xstrdup (symtab->filename);
          *line = -1;
        }
    }
  return 0;
}

/* Concat address ADDR in buffer.  USE_LOCAL means the same thing as for
   print_longest.  */
void
strcat_address_numeric (CORE_ADDR addr, int use_local, char *buf, int buflen)
{
#ifdef HP_IA64
  char local_buf[100];
  int local_buf_len = 100;
  int length;


  if (is_ia64_instruction_address (addr))
    {
      int slot_no;
      CORE_ADDR bundle_addr;

      bundle_addr = (addr & (~0xF));
      slot_no = (int) (addr % 16);

      /* This assumes a CORE_ADDR can fit in a LONGEST.  Probably a safe
         assumption.  */
      strcat_longest ('x', use_local, (unsigned LONGEST) bundle_addr, buf, buflen);
      if (buf)
	{
	  length = (int) strlen (buf);
	  strncat (buf, ":", buflen - length - 2);
	  sprintf (local_buf, "%d", slot_no);
	  strncat (buf, local_buf, buflen - length - 2 - strlen (local_buf));
	}
    }
  else
    {
      /* This assumes a CORE_ADDR can fit in a LONGEST.  Probably a safe
         assumption.  */
      strcat_longest ('x', use_local, (unsigned LONGEST) addr, buf, buflen);
    }
#else /*!HP_IA64 */
  /* This assumes a CORE_ADDR can fit in a LONGEST.  Probably a safe
     assumption.  */
  strcat_longest ('x', use_local, (ULONGEST) addr, buf, buflen);
#endif /*HP_IA64 */
}

/* Print address ADDR on STREAM.  USE_LOCAL means the same thing as for
   print_longest.  */
void
print_address_numeric (CORE_ADDR addr, int use_local, struct ui_file *stream)
{
#ifdef HP_IA64
  /* jini: 060511: The following check returns 0 for PA libraries in mixed
   * mode executables too. */
  if (is_ia64_instruction_address (SWIZZLE(addr)))
    {
      int slot_no;
      CORE_ADDR bundle_addr;

      bundle_addr = (addr & (~0xF));
      slot_no = (int) (addr % 16);

      if (hp_ia64_testsuite)
	{
	  print_longest (stream, 'x', use_local,
			 (unsigned LONGEST) bundle_addr + slot_no);
	}
      else
	{
	  print_longest (stream, 'x', use_local, (unsigned LONGEST) bundle_addr);
	  fprintf_filtered (stream, "%s", ":");
	  fprintf_filtered (stream, "%d", slot_no);
	}
    }
  else
    {
      /* This assumes a CORE_ADDR can fit in a LONGEST.  Probably a safe
         assumption.  */
      print_longest (stream, 'x', use_local, (unsigned LONGEST) addr);
    }
#else /*!HP_IA64 */
  /* This assumes a CORE_ADDR can fit in a LONGEST.  Probably a safe
     assumption.  */
  print_longest (stream, 'x', use_local, (ULONGEST) addr);
#endif /*HP_IA64 */
}

/* Print address ADDR symbolically on STREAM.
   First print it as a number.  Then perhaps print
   <SYMBOL + OFFSET> after the number.  */

void
print_address (CORE_ADDR addr, struct ui_file *stream)
{
  print_address_numeric (addr, 1, stream);
  print_address_symbolic (addr, stream, asm_demangle, " ");
}

/* Print address ADDR symbolically on STREAM or to buffer.
   First print it as a number.  Then perhaps print
   <SYMBOL + OFFSET> after the number.  */

void
printOrStrcat_address (CORE_ADDR addr, struct ui_file *stream, char *buf, int buflen)
{
  if (buf == NULL || buflen <= 0)
    print_longest (stream, 'x', 1, (ULONGEST) addr);
  else
    strcat_longest ('x', 1, (ULONGEST) addr, buf, buflen);
  print_address_symbolic(addr, stream, asm_demangle, " ");
}

/* Print address ADDR symbolically on STREAM.  Parameter DEMANGLE
   controls whether to print the symbolic name "raw" or demangled.
   Global setting "addressprint" controls whether to print hex address
   or not.  */

void
print_function_address_demangle (CORE_ADDR addr, struct ui_file *stream, int do_demangle)
{
  CORE_ADDR addr2;
  if (addr == 0)
    {
      fprintf_filtered (stream, "0");
      return;
    }

  addr2 = addr;
#ifdef GDB_TARGET_IS_HPPA
  if (addr & 0x2)
    {
      /* RM: It's a plabel, extract real address */
      addr2 = read_memory_integer(addr & ~0x2, sizeof (CORE_ADDR));
    }
#endif

  if (addressprint)
    {
      print_address_numeric (addr, 1, stream);
      print_address_symbolic (addr2, stream, do_demangle, " ");
    }
  else
    {
      print_address_symbolic (addr2, stream, do_demangle, "");
    }
}

void
print_address_demangle (CORE_ADDR addr, struct ui_file *stream, int do_demangle)
{
  if (addr == 0)
    {
      fprintf_filtered (stream, "0");
    }
  else if (addressprint)
    {
      print_address_numeric (addr, 1, stream);
      print_address_symbolic (addr, stream, do_demangle, " ");
    }
  else
    {
      print_address_symbolic (addr, stream, do_demangle, "");
    }
}


/* These are the types that $__ will get after an examine command of one
   of these sizes.  */

static struct type *examine_i_type;

static struct type *examine_b_type;
static struct type *examine_h_type;
static struct type *examine_w_type;
static struct type *examine_g_type;

/* Examine data at address ADDR in format FMT.
   Fetch it from memory and print on gdb_stdout.  */

static void
do_examine (struct format_data fmt, CORE_ADDR addr, asection *sect)
{
  char format[2] = {0, 0};
  register char size;
  register int count = 1;
  struct type *val_type = NULL;
  register int i;
  register int maxelts;

  format[0] = fmt.format[0];
  format[1] = fmt.format[1];
  size = fmt.size;
  count = fmt.count;
  next_address = addr;
  next_section = sect;

  /* String or instruction format implies fetch single bytes
     regardless of the specified size.  */
  if (format[0] == 's' || format[0] == 'i')
    size = 'b';

  if (format[0] == 'i')
    val_type = examine_i_type;
  else if (size == 'b')
    val_type = examine_b_type;
  else if (size == 'h')
    val_type = examine_h_type;
  else if (size == 'w')
    val_type = examine_w_type;
  else if (size == 'g')
    val_type = examine_g_type;

  maxelts = 8;
  if (size == 'w')
    maxelts = 4;
  if (size == 'g')
    maxelts = 2;
  if (format[0] == 's' || format[0] == 'i')
    maxelts = 1;

  /* Print as many objects as specified in COUNT, at most maxelts per line,
     with the address of the next one at the start of each line.  */

  if (!count)
    {
      last_examine_value = NULL;
      return;
    }

  while (count > 0)
    {
      QUIT;
      print_address (next_address, gdb_stdout);
      printf_filtered (":");
      for (i = maxelts;
	   i > 0 && count > 0;
	   i--, count--)
	{
	  printf_filtered ("\t");
	  /* Note that print_formatted sets next_address for the next
	     object.  */
	  last_examine_address = next_address;

	  if (last_examine_value)
	    value_free (last_examine_value);

	  /* The value to be displayed is not fetched greedily.
	     Instead, to avoid the posibility of a fetched value not
	     being used, its retreval is delayed until the print code
	     uses it.  When examining an instruction stream, the
	     disassembler will perform its own memory fetch using just
	     the address stored in LAST_EXAMINE_VALUE.  FIXME: Should
	     the disassembler be modified so that LAST_EXAMINE_VALUE
	     is left with the byte sequence from the last complete
	     instruction fetched from memory? */
	  last_examine_value = value_at_lazy (val_type, next_address, sect);

	  if (last_examine_value)
	    release_value (last_examine_value);

	  print_formatted (last_examine_value, format, size, gdb_stdout);
	}
      printf_filtered ("\n");
      gdb_flush (gdb_stdout);
    }
}

static void
validate_format (struct format_data fmt, char *cmdname)
{
  if (fmt.size != 0)
    error ("Size letters are meaningless in \"%s\" command.", cmdname);
  if (fmt.count != 1)
    error ("Item count other than 1 is meaningless in \"%s\" command.",
	   cmdname);
  if (fmt.format[0] == 'i' || fmt.format[0] == 's')
    error ("Format letter \"%c\" is meaningless in \"%s\" command.",
	   fmt.format[0], cmdname);

  if ((fmt.format[0] == 'o') || (fmt.format[0] == 'x') ||
      (fmt.format[0] == 'u') || (fmt.format[0] == 't') ||
      (fmt.format[0] == 'f') || (fmt.format[0] == 'a') ||
      (fmt.format[0] == 'i') || (fmt.format[0] == 'c') ||
      (fmt.format[0] == 's'))
    if (fmt.format[1] != '?')
      error ("Invalid format letter \"%s\"", fmt.format);
  /* Check for validity of the format letters allowed. */
  if (fmt.format[0] == 'd')
    if (!((fmt.format[1] == '?')
#ifdef HP_IA64
        || (fmt.format[1] == 'f') || (fmt.format[1] == 'd')
        || (fmt.format[1] == 'l')
#endif
        || (fmt.format[1] == 'b')))
      error ("Invalid format letter \"%s\"", fmt.format);
}

/*  Evaluate string EXP as an expression in the current language and
   print the resulting value.  EXP may contain a format specifier as the
   first argument ("/x myvar" for example, to print myvar in hex).
 */

static void
print_command_1 (char *exp, int inspect, int voidprint)
{
  struct expression *expr;
  register struct cleanup *old_chain = 0;
  char format[2] = {0, 0};
  register value_ptr val;
  struct format_data fmt;
  int cleanup = 0;

  /* Pass inspect flag to the rest of the print routines in a global (sigh). */
  inspect_it = inspect;

  if (exp && *exp == '/')
    {
      exp++;
      fmt = decode_format (&exp, last_format, 0);
      validate_format (fmt, "print");
      last_format[0] = format[0] = fmt.format[0];
      last_format[1] = format[1] = fmt.format[1];
#ifdef HP_IA64
      /* special handling for the printing of changed registers */
      if (nimbus_version)
	ia64_register_parser (exp);
#endif
    }
#ifdef HP_IA64
  /* special handling for the printing of changed registers */
  else if (   nimbus_version 
	   && (ia64_register_parser (exp))
	   )
    {
      fmt.count = 1;
      fmt.format[0] = 'x';
      fmt.format[1] = '?';
      fmt.size = 0;
      last_format[0] = format[0] = fmt.format[0];
      last_format[1] = format[1] = fmt.format[1];
    }
#endif
  else
    {
      fmt.count = 1;
      fmt.format[0] = fmt.format[1] = 0;
      fmt.size = 0;
    }

  if (exp && *exp)
    {
      struct type *type;
      expr = parse_expression (exp);
      annotate_symbols_invalid(expr);
      old_chain = make_cleanup (free_current_contents, &expr);
      cleanup = 1;
      val = evaluate_expression (expr);

      /* C++: figure out what type we actually want to print it as.  */
      type = VALUE_TYPE (val);

      if (objectprint
	  && (TYPE_CODE (type) == TYPE_CODE_PTR
	      || TYPE_CODE (type) == TYPE_CODE_REF)
	  && (TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_STRUCT
	      || TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_UNION
	      || TYPE_CODE (TYPE_TARGET_TYPE (type)) == TYPE_CODE_CLASS))
	{
	  value_ptr v;

	  v = value_from_vtable_info (val, TYPE_TARGET_TYPE (type));
	  if (v != 0)
	    {
	      val = v;
	      type = VALUE_TYPE (val);
	    }
	}
    }
  else
    val = access_value_history (0);

  if (voidprint || (val && VALUE_TYPE (val) &&
		    TYPE_CODE (VALUE_TYPE (val)) != TYPE_CODE_VOID))
    {
      int histindex = record_latest_value (val);

      if (histindex >= 0)
	annotate_value_history_begin (histindex, VALUE_TYPE (val));
      else
	annotate_value_begin (VALUE_TYPE (val));

      if (inspect)
	printf_unfiltered ("\031(gdb-makebuffer \"%s\"  %d '(\"", exp, histindex);
      else if (histindex >= 0)
	printf_filtered ("$%d = ", histindex);

      if (histindex >= 0)
	annotate_value_history_value ();

      print_formatted (val, format, fmt.size, gdb_stdout);
      printf_filtered ("\n");

      if (histindex >= 0)
	annotate_value_history_end ();
      else
	annotate_value_end ();

      if (inspect)
	printf_unfiltered ("\") )\030");
    }

  if (cleanup)
    do_cleanups (old_chain);
  inspect_it = 0;		/* Reset print routines to normal */
}

/* ARGSUSED */
static void
print_command (char *exp, int from_tty)
{
  print_command_1 (exp, 0, 1);
}


/* Same as print, except in epoch, it gets its own window */
/* ARGSUSED */
static void
inspect_command (char *exp, int from_tty)
{
  extern int epoch_interface;

  print_command_1 (exp, epoch_interface, 1);
}

/* Same as print, except it doesn't print void results. */
/* ARGSUSED */
static void
call_command (char *exp, int from_tty)
{
  print_command_1 (exp, 0, 0);
}

/* Srikanth, set print object on interferes with out_no_values used
   by the GUI. Toggle internally. Reported by Intel.
*/

static int old_objectprint;

static void
no_values_cleanup ()
{
  objectprint = old_objectprint;
  dont_print_value = 0;
}

/* ARGSUSED */
static void
output_command_no_values (char *exp, int from_tty)
{
  register struct cleanup *old_chain;
  if (exp)
    dont_print_value = 1;
  old_objectprint = objectprint;
  objectprint = 0;
  old_chain = make_cleanup ((make_cleanup_ftype *) no_values_cleanup, 0);
  output_command (exp, from_tty);
  do_cleanups (old_chain);
  dont_print_value = 0;
}

static void
allow_function_calls ()
{
  function_calls_disallowed = 0;
}


void
output_command (char *exp, int from_tty)
{
  struct expression *expr;
  register struct cleanup *old_chain;
  char format[2] = {0, 0};
  register value_ptr val;
  struct format_data fmt = {0};
  extern int server_command;

  if (exp && *exp == '/')
    {
      exp++;
      fmt = decode_format (&exp, 0, 0);
      validate_format (fmt, "output");
      format[0] = fmt.format[0];
      format[1] = fmt.format[1];
    }

  expr = parse_expression (exp);
  old_chain = make_cleanup (free_current_contents, &expr);

  /* Srikanth, disallow function calls during dwell. */
  make_cleanup ((make_cleanup_ftype *) allow_function_calls, 0);
  if (server_command)
    function_calls_disallowed = 1;
  val = evaluate_expression (expr);
  function_calls_disallowed = 0;

  annotate_value_begin (VALUE_TYPE (val));

  print_formatted (val, format, fmt.size, gdb_stdout);

  annotate_value_end ();

  wrap_here ("");
  gdb_flush (gdb_stdout);

  do_cleanups (old_chain);
}

/* ARGSUSED */
static void
set_command (char *exp, int from_tty)
{
  struct expression *expr = parse_expression (exp);
  register struct cleanup *old_chain =
    make_cleanup (free_current_contents, &expr);
  annotate_symbols_invalid(expr);
  evaluate_expression (expr);
  do_cleanups (old_chain);
}

/*
 * search_frame_sym_reg - search selected_frame for a symbol which resides
 *	at address addr or is kept in register regno.  (The register 
 *	searching is not implemented at this point.  It is intended for
 *	the future "info symreg regno" command.
 *	If found, print the information about the symbol, as in 
 *	info symbol ADDRESS.
 */

static void
search_frame_sym_reg (struct frame_info*	frame,
                      info_sym_data_t* 		info_sym_data, 
		      int 			thread_id, 
		      int 			stack_count, 
		      int 			regno)
{
  struct block *		block;
  int				idx;
  int				offset;
  struct symtab_and_line 	sal;
  int				size;
  struct symbol*		sym;
  int				symbol_found = 0;
  struct type*			type;
  register value_ptr 		val;
  CORE_ADDR			val_addr;

  /* Get the starting block based on select_frame */

  sal = find_pc_line (frame->pc, 0);
  block = block_for_pc (sal.pc);
  while (block)
    { /* Search all the blocks that belong to the function */
      for (idx = 0;  idx < BLOCK_NSYMS (block);  idx++)
	{ /* for each symbol */
	  sym = BLOCK_SYM(block, idx);

	  /* Realize the address of the symbol, see if it matches the
	   * desired address 
	   */
	  val = locate_var_value2 (sym, frame, 0);
	  if (val)
	    {
	      type = check_typedef (VALUE_TYPE (sym));
	      size = TYPE_LENGTH (type);
	      val_addr = SWIZZLE (value_as_long (val));
	      if (   val 
		  && TYPE_CODE (VALUE_TYPE (val)) != TYPE_CODE_VOID
		  && info_sym_data->addr >= val_addr 
		  && val_addr + size > info_sym_data->addr)
		{
		  symbol_found = 1;
		  offset = (int) (info_sym_data->addr - val_addr);

                  if (info_sym_data->is_print)
                    {
                      if (offset == 0)
                          printf_filtered ("Symbol %s in frame %d of thread %d\n",
                                           SYMBOL_NAME(sym),
                                           stack_count,
                                           thread_id);
                      else
                        printf_filtered ("Symbol %s + %ld in frame %d of thread %d\n",
                                       SYMBOL_NAME(sym),
                                       (long) (info_sym_data->addr - val_addr),
                                       stack_count,
                                       thread_id);
                    }
                  else
                    {
                      info_sym_data->offset = offset;
                      info_sym_data->name = SYMBOL_NAME(sym);
                      return;
                    }
		}
	    }
	} /* for each symbol */
      if (BLOCK_FUNCTION (block))
	break;  /* Last block of function */
      block = BLOCK_SUPERBLOCK (block);
    } /* Search all the blocks that belong to the function */

    if (!symbol_found && info_sym_data->is_print)
      printf_filtered ("Address 0x%llx belongs to frame %lld of thread %d\n",
		       (long long) info_sym_data->addr, 
		       (long long) stack_count, 
		       thread_id);
       
} /* end search_frame_sym_reg */


/* search_thread_stack_sym - look for a symbol which occupies the given
 *	address in the current thread's stack.  Called from search_stack_sym
 *	through foreach_live_thread.
 * info_sym_data->match_thread_stack_sym is set to FALSE prior to searching
 * the first thread stack.  This routine sets it to TRUE if a match is found
 * and if info_sym_data->match_thread_stack_sym is already TRUE it does nothing.
 */

void 
search_thread_stack_sym (void *addr_arg)
{
  CORE_ADDR		addr;
  CORE_ADDR 		cfm;
  CORE_ADDR		first_reg_addr;
  int			first_reg;
  struct frame_info*	frame;
  CORE_ADDR		high_sp;
  info_sym_data_t*	info_sym_data;
  CORE_ADDR		low_sp;
  int			offset;
  CORE_ADDR		prev_sp;
  CORE_ADDR		sp;
  int			stack_count;
  int 			thread_id;
  struct thread_info *	tp;
  int			utid;

#ifdef REGISTER_STACK_ENGINE_FP
  CORE_ADDR 		bsp;
  CORE_ADDR 		bsp_limit;
  CORE_ADDR 		prev_bsp;
  int			regno;
#endif



  info_sym_data = ((info_sym_data_t*) addr_arg);
  if (info_sym_data->match_thread_stack_sym)
    {
      return;
    }
  addr = info_sym_data->addr;
  tp = find_thread_pid (inferior_pid);
  if (is_mxn)
    {
    thread_id = get_pthread_for (tp->pid);
    if (thread_id == -1)
      thread_id = -(get_lwp_for (tp->pid));
    }
  else
    thread_id = tp->num;
  /* Go up the stack up to the top frame looking for a frame which owns
   * the address, i.e, the address is between the top-of-stack and
   * the previous frame pointer or between current BSP and the BSP of
   * the prior frame.
   */
  
  frame = selected_frame;
  stack_count = 0;
#ifdef REGISTER_STACK_ENGINE_FP
  /* The initial prev_bsp is used to set bsp at the start of the loop.
   * Initially it is incorrect, but the initial frame's registers
   * are not stored, so the backing address for those registers does
   * not currently exist.
   */
  prev_bsp = frame->rse_fp;
  read_relative_register_raw_bytes_for_frame (CFM_REGNUM, (char *) &cfm, frame);
  /* For startup prev_bsp is the bsp our callee would get.  */
  prev_bsp = add_to_bsp (prev_bsp, (long) SIZE_OF_LOCALS(cfm));
#endif
  read_relative_register_raw_bytes_for_frame (SP_REGNUM, 
					      (char *) &prev_sp, 
					      frame);

  /* loop until we have walked up the stack or found a match */
  while (!info_sym_data->match_thread_stack_sym) 
    {
      sp = prev_sp;
      prev_sp = FRAME_FP (frame);

#ifdef REGISTER_STACK_ENGINE_FP
      bsp = prev_bsp;
      read_relative_register_raw_bytes_for_frame (CFM_REGNUM, 
						  (char *) &cfm, 
						  frame);
      /* SIZE_OF_LOCALS is the number of saved registers.  */

      bsp_limit = add_to_bsp (bsp, + (long) (SIZE_OF_LOCALS(cfm)));
#endif

      if (INNER_THAN (1, 2))
	{ /* stack grows down, e.g. IPF */
	  low_sp = sp;
	  high_sp = prev_sp;
	  
	  if (addr >= low_sp - SP_INCR && addr <  high_sp + SP_INCR)
	      info_sym_data->match_thread_stack_sym = 1;
	}
      else 
	{ /* stack grows up, e.g. PA */
	  low_sp = prev_sp;
	  high_sp = sp;
	  if (addr >= low_sp && addr < high_sp)
	      info_sym_data->match_thread_stack_sym = 1;
	}
      if (info_sym_data->match_thread_stack_sym)
	{
	  search_frame_sym_reg (frame, 
	  		        info_sym_data, 
				thread_id, 
				stack_count, 
				-1);
	  return;
	}
      
#ifdef REGISTER_STACK_ENGINE_FP
      if (addr >= bsp && addr < bsp_limit)
	{
	  info_sym_data->match_thread_stack_sym = 1;

	  /* Tell the user what register is stored there, if it is not a Nat
	   * collection.  
	   */
	  first_reg_addr = bsp;
	  first_reg=32;  
	  while (NEXT_NAT_COLLECTION_ADDR(first_reg_addr) <= addr)
	    {
	      /* REGISTER_SIZE bytes per reg don't count NAT collection*/
	      first_reg += (  (   (  NEXT_NAT_COLLECTION_ADDR(first_reg_addr) 
			             - first_reg_addr)
			        / REGISTER_SIZE 
			      )
			    - 1);  
	      first_reg_addr = NEXT_NAT_COLLECTION_ADDR(first_reg_addr) + REGISTER_SIZE;
	    }
	  if (first_reg_addr > addr)
	    { /* Address belongs to a NAT collection.  Nothing more to say. */

	      offset = (int) (addr % REGISTER_SIZE);
              if (info_sym_data->is_print)
                {
                  if (offset == 0)
                      printf_filtered (
			  "NaT bit collection in frame %d of thread %d\n",
                          stack_count,
                          thread_id);
                  else
                      printf_filtered (
                          "NaT bit collection + %d in frame %d of thread %d\n",
                          offset,
                          stack_count,
                          thread_id);
                }
	      return;
	    }

	  regno = (int) (first_reg + ((addr - first_reg_addr) / REGISTER_SIZE));
	  offset = (int) (addr % REGISTER_SIZE);
          if (info_sym_data->is_print)
            {
              if (offset == 0)
                  printf_filtered ("%s in frame %d of thread %d\n",
                                   REGISTER_NAME(GR0_REGNUM + regno),
                                   stack_count,
                                   thread_id);
              else
                  printf_filtered ("%s + %d in frame %d of thread %d\n",
                                   REGISTER_NAME(GR0_REGNUM + regno),
                                   offset,
                                   stack_count,
                                   thread_id);
            }
	  return;
	}
#endif
      /* Pop the stack and repeat if there are more frames to look at */

      frame = get_prev_frame (frame);
      if (frame == 0)
	{
	  /* info_sym_data->match_thread_stack_sym is set to zero by 
	   * caller initially */

	  return;
	}
#ifdef REGISTER_STACK_ENGINE_FP
      prev_bsp = frame->rse_fp;
#endif
      stack_count++;
    } /* loop until return searching the stack */
  return;
} /* end search_thread_stack_sym */


/* search_stack_sym	- look for a symbol which occupies the given address
 *			  called from sym_info if no global match found.
 *  addr		- the address for which a symbol match is sought
 *  retuns TRUE if a match was found, else 0.
 */

int
search_stacks_sym (CORE_ADDR addr)
{
  info_sym_data_t	info_sym_data;
  extern int 		show_all_kthreads;

  int save_show_all_kthreads = show_all_kthreads;

  show_all_kthreads = 1;
  info_sym_data.addr = addr;
  info_sym_data.match_thread_stack_sym = 0;
  info_sym_data.is_print = 1;

  foreach_live_thread (search_thread_stack_sym, &info_sym_data);
  show_all_kthreads = save_show_all_kthreads;
  return info_sym_data.match_thread_stack_sym;
} // end search_stack_sym


/* When gdbrtc.c calls this function 'which_caller' is 2  */
/* ARGSUSED */
int
sym_info_rtc (char *arg, int which_caller, FILE *fp)
{
  struct minimal_symbol *msymbol;
  struct objfile *objfile;
  struct obj_section *osect;
  asection *sect;
  CORE_ADDR addr, sect_addr;
  int matches = 0;
  unsigned int offset;

  if (!arg)
    error_no_arg ("address");

  if (which_caller !=2 )
    addr = SWIZZLE (parse_and_eval_address (arg));
  else
    addr = SWIZZLE ((CORE_ADDR) arg);
  ALL_OBJSECTIONS (objfile, osect)
  {
    sect = osect->the_bfd_section;
    sect_addr = overlay_mapped_address (addr, sect);

    if (osect->addr <= sect_addr && sect_addr < osect->endaddr &&
	(msymbol = lookup_minimal_symbol_by_pc_section (sect_addr, sect)))
      {
	matches = 1;
	offset = (unsigned int) (sect_addr - SYMBOL_VALUE_ADDRESS (msymbol));
	if (offset)
          if (fp)
	    fprintf (fp, "%s + %u in ",
			   SYMBOL_SOURCE_NAME (msymbol), offset);
          else
	    printf_filtered ("%s + %u in ",
			   SYMBOL_SOURCE_NAME (msymbol), offset);
	else
	  if (fp)
            fprintf (fp, "Symbol %s in ",
			   SYMBOL_SOURCE_NAME (msymbol));
          else
	    printf_filtered ("Symbol %s in ",
			   SYMBOL_SOURCE_NAME (msymbol));
	if (pc_in_unmapped_range (addr, sect))
	  if (fp) 
            fprintf (fp, "load address range of ");
          else
            printf_filtered ("load address range of ");
	if (section_is_overlay (sect))
	  if (fp)
             fprintf (fp, "%s overlay ",
			   section_is_mapped (sect) ? "mapped" : "unmapped");
          else
	     printf_filtered ("%s overlay ",
			   section_is_mapped (sect) ? "mapped" : "unmapped");

	if (fp)
          fprintf (fp, "section %s\n", sect->name);
	else
          printf_filtered ("section %s\n", sect->name);
      }
  }
  if (matches == 0) 
    { /* Search for the address in the execution stacks */
      matches = search_stacks_sym(addr);
    }
  if (matches == 0 && which_caller != 2)
    if (fp)
      fprintf (fp, "No symbol matches %s.\n", arg);
    else
      printf_filtered ("No symbol matches %s.\n", arg);
  else if (matches == 0 && which_caller == 2)
    if (fp)
      fprintf (fp,"No symbol matches at address 0x%llx.\n", addr);
    else
      printf_filtered ("No symbol matches at address 0x%x.\n", addr);
  else if (which_caller == 2)
    if (fp)
      fprintf (fp,"Pointer location address 0x%llx.\n", addr);  
    else
      printf_filtered ("Pointer location address 0x%x.\n", addr);  
  return matches;
}


/* ARGSUSED */
void
sym_module_rtc (char *arg, int which_caller, FILE *fp)
{
  CORE_ADDR		addr;
  CORE_ADDR		data_start;
  CORE_ADDR		data_size;
  struct objfile * 	objfile;

  if (!arg)
    error_no_arg ("address");

  if (which_caller !=2 )
    addr = SWIZZLE (parse_and_eval_address (arg));
  else
    addr = SWIZZLE ((CORE_ADDR) arg);

#ifdef PC_SOLIB
  /* This will main objfile->* stuff below to be initialized */
  solib_and_mainfile_address (0);
#endif
  ALL_OBJFILES (objfile)
    {
      if (   addr >= SWIZZLE (objfile->text_low) 
	  && addr < SWIZZLE (objfile->text_high))
	{

          if (fp)
	    fprintf (fp, "Address 0x%llx belongs to the text segment of %s\n",
			   (long long) addr, 
			   objfile->name);
          else
	    printf_filtered ("Address 0x%llx belongs to the text segment of %s\n",
			   (long long) addr, 
			   objfile->name);
	  return;
	}

      data_start = SWIZZLE (objfile->data_start);
      data_size = SWIZZLE (objfile->data_size);
      if (addr >= data_start && addr < SWIZZLE(data_start + data_size))
	{
        
	  if (fp)
            fprintf (fp, "Address 0x%llx belongs to the data segment of %s\n",
			   (long long) addr, 
			   objfile->name);
          else
	    printf_filtered ("Address 0x%llx belongs to the data segment of %s\n",
			   (long long) addr, 
			   objfile->name);
	  return;
	}

    } /* for each objfile */

  if (which_caller != 2)
    printf_filtered (
      "Address 0x%llx does not belong to the text or data of any load module\n",
		   (long long) addr
		  );
} // end sym_module_rtc


static void
sym_info (char *arg, int from_tty)
{
  struct minimal_symbol *msymbol;
  struct objfile *objfile;
  struct obj_section *osect;
  asection *sect;
  CORE_ADDR addr, sect_addr;
  int matches = 0;
  unsigned int offset;

  if (!arg)
    error_no_arg ("address");

    addr = SWIZZLE (parse_and_eval_address (arg));
  ALL_OBJSECTIONS (objfile, osect)
  {
    /* Only process each object file once, even if there's a separate
       debug file.  */
    if (objfile->separate_debug_objfile_backlink)
      continue;

    sect = osect->the_bfd_section;
    sect_addr = overlay_mapped_address (addr, sect);

    if (osect->addr <= sect_addr && sect_addr < osect->endaddr &&
	(msymbol = lookup_minimal_symbol_by_pc_section (sect_addr, sect)))
      {
	matches = 1;
	offset = (unsigned int) (sect_addr - SYMBOL_VALUE_ADDRESS (msymbol));
	if (offset)
	    printf_filtered ("%s + %u in ",
			   SYMBOL_SOURCE_NAME (msymbol), offset);
	else
	    printf_filtered ("Symbol %s in ",
			   SYMBOL_SOURCE_NAME (msymbol));
	if (pc_in_unmapped_range (addr, sect))
            printf_filtered ("load address range of ");
	if (section_is_overlay (sect))
	     printf_filtered ("%s overlay ",
			   section_is_mapped (sect) ? "mapped" : "unmapped");

          printf_filtered ("section %s\n", sect->name);
      }
  }
  if (matches == 0) 
    { /* Search for the address in the execution stacks */
      matches = search_stacks_sym(addr);
    }
  if (matches == 0)
      printf_filtered ("No symbol matches %s.\n", arg);
}

/* ARGSUSED */
void
sym_module (char *arg, int from_tty)
{
  CORE_ADDR		addr;
  CORE_ADDR		data_start;
  CORE_ADDR		data_size;
  struct objfile * 	objfile;

  if (!arg)
    error_no_arg ("address");

  addr = SWIZZLE (parse_and_eval_address (arg));

#ifdef PC_SOLIB
  /* This will main objfile->* stuff below to be initialized */
  solib_and_mainfile_address (0);
#endif
  ALL_OBJFILES (objfile)
    {
      if (   addr >= SWIZZLE (objfile->text_low) 
	  && addr < SWIZZLE (objfile->text_high))
	{

	    printf_filtered ("Address 0x%llx belongs to the text segment of %s\n",
			   (long long) addr, 
			   objfile->name);
	  return;
	}

      data_start = SWIZZLE (objfile->data_start);
      data_size = SWIZZLE (objfile->data_size);
      if (addr >= data_start && addr < SWIZZLE(data_start + data_size))
	{
        
	    printf_filtered ("Address 0x%llx belongs to the data segment of %s\n",
			   (long long) addr, 
			   objfile->name);
	  return;
	}

    } /* for each objfile */

    printf_filtered (
      "Address 0x%llx does not belong to the text or data of any load module\n",
		   (long long) addr
		  );
} // end sym_module

#ifdef HP_IA64
/* See the C++ ABI and dump RTTI info given any address.
   This does not depend on .debug_info. */
static void
rtti_info (char *arg, int from_tty)
{
  CORE_ADDR addr;

  if (!arg)
    error_no_arg ("Address of C++ polymorphic object");

  /* Parse the input object and get the address of it */
  addr = parse_and_eval_address (arg);
  print_cxx_object (addr);

}

/* This function finds the vtable Address for given object
   Address. It later calls function for printing vtable contents */
static void
print_cxx_object (CORE_ADDR obj)
{
  CORE_ADDR vtbl;

  if (!obj)
    RETURN_ERR

  /* vtbl = *(void **) obj; */
  if (target_read_memory (obj, (char *) &vtbl, PTR_SIZE))
    RETURN_ERR

  /* For 32 bit programs, vtable address is present on top 32 bits. */
  if (is_swizzled)
    vtbl = vtbl >> 32;

  print_vtable2 (vtbl, obj);

}

/* This function finds the typeinfo Address for given vtable
   Address. It later calls function for printing typeinfo contents */
static void
print_vtable2 (CORE_ADDR vtbl, CORE_ADDR obj)
{
  CORE_ADDR ti;

  if (target_read_memory ((CORE_ADDR) (vtbl - sizeof(long)),
                          (char *) &ti, PTR_SIZE))
    RETURN_ERR

  /* For 32 bit application, typeinfo address is present on top 32 bits */
  if (is_swizzled)
    ti = ti >> 32;

  print_type_info_3 (ti, vtbl, obj);

}

/* This function is currently calling "__do_type_info1" for printing demangled
   name for given object. Further enhancements would need this function */
static void
print_type_info_3 (CORE_ADDR ti, CORE_ADDR vtbl, CORE_ADDR obj)
{

  __do_type_info1 (ti, 0, 0, 0);

}

/* This function prints demangled name for given typeinfo Address. */
static void
__do_type_info1 (CORE_ADDR ti, void *context, int indent,
                 long offset)
{
  char *ptr_demangle = NULL;
  char *class_name = NULL;
  char *name = NULL;
  char *filename = NULL;
  CORE_ADDR tti_name;

  int do_demangle = 1;
  int unmapped = 0;
  int addr_offset = 0;
  int line = 0;

  if (target_read_memory ((CORE_ADDR) (ti + PTR_SIZE),
                          (char*) &tti_name, PTR_SIZE))
    RETURN_ERR

  /* For 32 bit application, tti_name address is present on top 32 bits */
  if (is_swizzled)
    tti_name = tti_name >> 32;

  build_address_symbolic (tti_name, do_demangle, &name,
	                  &addr_offset, &filename, &line, &unmapped);

  if (!name)
    RETURN_ERR

  ptr_demangle = cplus_demangle(name,DMGL_ANSI);

  if (!ptr_demangle)
    RETURN_ERR

  class_name = &ptr_demangle[18];
  printf_filtered("RTTI: class %s\n", class_name);

}
#endif

void
describe_pieces (struct symbol *sym)
{
  int idx;

  printf_filtered ("in %d pieces:\n", 
		   SYMBOL_NBR_PIECES(sym));
  for (idx = 0;  idx < SYMBOL_NBR_PIECES(sym); idx++)
    {
      printf_filtered ("  %d bytes from ", 
		       SYMBOL_PIECE_LIST(sym)[idx].piece_size);
      switch (SYMBOL_PIECE_LIST(sym)[idx].piece_type)
	{
	  case PIECE_LOC_REGISTER: 
	    printf_filtered (
		"register %s\n", 
		REGISTER_NAME ((int) SYMBOL_PIECE_LIST(sym)[idx].arg1));
	    break;

	  case PIECE_LOC_STATIC: 
	    printf_filtered (
		"memory at address 0x%lx\n", 
		(long) SYMBOL_PIECE_LIST(sym)[idx].arg1);
	    break;
	    
	  case PIECE_LOC_BASEREG: 
	    printf_filtered (
		"memory at address in register %s plus offset 0x%lx\n",
		REGISTER_NAME ((int)SYMBOL_PIECE_LIST(sym)[idx].arg1),
		(long) SYMBOL_PIECE_LIST(sym)[idx].arg2);
	    break;

	  default:
	    warning ("Report to HP, error in address_info piece list.");
	    break;
	    
	}
	    
    }
} // describe_pieces

/* ARGSUSED */
static void
address_info (char *exp, int from_tty)
{
  register struct symbol *sym;
  register struct minimal_symbol *msymbol;
  register long val;
  register long basereg;
  asection *section;
  CORE_ADDR load_addr;
  int is_a_field_of_this;	/* C++: lookup_symbol sets this to nonzero
				   if exp is a field of `this'. */
  int idx;
  int pieces_printed = 0;

  if (exp == 0)
    error ("Argument required.");

  sym = lookup_symbol (exp, get_selected_block (), VAR_NAMESPACE,
		       &is_a_field_of_this, (struct symtab **) NULL);
  if (sym == NULL)
    {
      if (is_a_field_of_this)
	{
	  printf_filtered ("Symbol \"");
	  fprintf_symbol_filtered (gdb_stdout, exp,
				   current_language->la_language, DMGL_ANSI);
	  printf_filtered ("\" is a field of the local class variable `this'\n");
	  return;
	}

      msymbol = lookup_minimal_symbol (exp, NULL, NULL);

      if (msymbol != NULL)
	{
	  load_addr = SYMBOL_VALUE_ADDRESS (msymbol);

	  printf_filtered ("Symbol \"");
	  fprintf_symbol_filtered (gdb_stdout, exp,
				   current_language->la_language, DMGL_ANSI);
	  printf_filtered ("\" is at ");
	  print_address_numeric (load_addr, 1, gdb_stdout);
	  printf_filtered (" in a file compiled without debugging");
	  section = SYMBOL_BFD_SECTION (msymbol);
	  if (section_is_overlay (section))
	    {
	      load_addr = overlay_unmapped_address (load_addr, section);
	      printf_filtered (",\n -- loaded at ");
	      print_address_numeric (load_addr, 1, gdb_stdout);
	      printf_filtered (" in overlay section %s", section->name);
	    }
	  printf_filtered (".\n");
	}
      else
	error ("No symbol \"%s\" in current context.", exp);
      return;
    }

  printf_filtered ("Symbol \"");
  fprintf_symbol_filtered (gdb_stdout, SYMBOL_NAME (sym),
			   current_language->la_language, DMGL_ANSI);
  printf_filtered ("\" is ");
  val = (long) SYMBOL_VALUE (sym);
  basereg = SYMBOL_BASEREG (sym);
  section = SYMBOL_BFD_SECTION (sym);

  switch (SYMBOL_CLASS (sym))
    {
    case LOC_CONST:
    case LOC_CONST_BYTES:
      printf_filtered ("constant");
      break;

    case LOC_LABEL:
      printf_filtered ("a label at address ");
      print_address_numeric (load_addr = SYMBOL_VALUE_ADDRESS (sym),
			     1, gdb_stdout);
      if (section_is_overlay (section))
	{
	  load_addr = overlay_unmapped_address (load_addr, section);
	  printf_filtered (",\n -- loaded at ");
	  print_address_numeric (load_addr, 1, gdb_stdout);
	  printf_filtered (" in overlay section %s", section->name);
	}
      break;

    case LOC_COMPUTED:
    case LOC_COMPUTED_ARG:
      (SYMBOL_LOCATION_FUNCS (sym)->describe_location) (sym, gdb_stdout);
      break;

    case LOC_REGISTER:
      if (!SYMBOL_NBR_PIECES(sym))
	printf_filtered ("a variable in register %s", REGISTER_NAME ((int) val));
      else
	{
	  printf_filtered ("a variable ");
	  describe_pieces(sym);
	  pieces_printed = 1;
	}
      break;

    case LOC_STATIC:
      printf_filtered ("static storage at address ");
      print_address_numeric (load_addr = SYMBOL_VALUE_ADDRESS (sym),
			     1, gdb_stdout);
      if (section_is_overlay (section))
	{
	  load_addr = overlay_unmapped_address (load_addr, section);
	  printf_filtered (",\n -- loaded at ");
	  print_address_numeric (load_addr, 1, gdb_stdout);
	  printf_filtered (" in overlay section %s", section->name);
	}
      break;

    case LOC_INDIRECT:
      printf_filtered ("external global (indirect addressing), at address *(");
      print_address_numeric (load_addr = SYMBOL_VALUE_ADDRESS (sym),
			     1, gdb_stdout);
      printf_filtered (")");
      if (section_is_overlay (section))
	{
	  load_addr = overlay_unmapped_address (load_addr, section);
	  printf_filtered (",\n -- loaded at ");
	  print_address_numeric (load_addr, 1, gdb_stdout);
	  printf_filtered (" in overlay section %s", section->name);
	}
      break;

    case LOC_REGPARM:
      if (!SYMBOL_NBR_PIECES(sym))
	printf_filtered ("an argument in register %s", REGISTER_NAME ((int) val));
      else
	{
	  printf_filtered ("an argument ");
	  describe_pieces(sym);
	  pieces_printed = 1;
	}
      break;

    case LOC_REGPARM_ADDR:
      if (!SYMBOL_NBR_PIECES(sym))
	printf_filtered ("address of an argument in register %s", REGISTER_NAME ((int) val));
      else
	{
	  printf_filtered ("address of an argument ");
	  describe_pieces(sym);
	  pieces_printed = 1;
	}
      break;

    case LOC_ARG:
      printf_filtered ("an argument at offset %ld", val);
      break;

    case LOC_LOCAL_ARG:
      printf_filtered ("an argument at frame offset %ld", val);
      break;

    case LOC_LOCAL:
      printf_filtered ("a local variable at frame offset %ld", val);
      break;

    case LOC_REF_ARG:
      printf_filtered ("a reference argument at offset %ld", val);
      break;

    case LOC_BASEREG:
      printf_filtered ("a variable at offset %ld from register %s",
		       val, REGISTER_NAME ((int) basereg));
      break;

    case LOC_BASEREG_ARG:
#ifdef GDB_TARGET_IS_HPPA_20W	
      if (SYMBOL_DEBUG_LOCATION_OFFSET (sym) != 0)
	printf_filtered ("an argument at offset %ld from register %s + %d",
			 val, REGISTER_NAME (basereg),
			 SYMBOL_DEBUG_LOCATION_OFFSET (sym));
      else
	printf_filtered ("an argument at offset %ld from register %s",
			 val, REGISTER_NAME (basereg));
#else
	printf_filtered ("an argument at offset %ld from register %s",
			 val, REGISTER_NAME ((int) basereg));
#endif

      break;

    case LOC_BASEREG_REF_ARG:
      printf_filtered ("an reference argument at offset %ld from register %s",
		       val, REGISTER_NAME ((int) basereg));
      break;

    case LOC_TYPEDEF:
      printf_filtered ("a typedef");
      break;

    case LOC_BLOCK:
      printf_filtered ("a function at address ");
#ifdef GDB_TARGET_MASK_DISAS_PC
      print_address_numeric
	(load_addr = GDB_TARGET_MASK_DISAS_PC (SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (sym)))),
	 1, gdb_stdout);
#else
      print_address_numeric (load_addr = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (sym))),
			     1, gdb_stdout);
#endif
      if (section_is_overlay (section))
	{
	  load_addr = overlay_unmapped_address (load_addr, section);
	  printf_filtered (",\n -- loaded at ");
	  print_address_numeric (load_addr, 1, gdb_stdout);
	  printf_filtered (" in overlay section %s", section->name);
	}
      break;

    case LOC_UNRESOLVED:
      {
	struct minimal_symbol *msym;

	msym = lookup_minimal_symbol (SYMBOL_NAME (sym), NULL, NULL);
	if (msym == NULL)
	  printf_filtered ("unresolved");
	else
	  {
	    section = SYMBOL_BFD_SECTION (msym);
	    printf_filtered ("static storage at address ");
	    print_address_numeric (load_addr = SYMBOL_VALUE_ADDRESS (msym),
				   1, gdb_stdout);
	    if (section_is_overlay (section))
	      {
		load_addr = overlay_unmapped_address (load_addr, section);
		printf_filtered (",\n -- loaded at ");
		print_address_numeric (load_addr, 1, gdb_stdout);
		printf_filtered (" in overlay section %s", section->name);
	      }
	  }
      }
      break;

    case LOC_THREAD_LOCAL_STATIC:
    case LOC_THREAD_LOCAL_STATIC_DPREL:
      printf_filtered (
			"a thread-local variable at offset %ld from the thread base register %s",
			val, REGISTER_NAME ((int) basereg));
      break;

    case LOC_OPTIMIZED_OUT:
      printf_filtered ("optimized out");
      break;

    default:
      printf_filtered ("of unknown (botched) type");
      break;
    }
  if (!pieces_printed)
    printf_filtered (".\n");
}

void
x_command (char *exp, int from_tty)
{
  struct expression *expr;
  struct format_data fmt = {0, 0, 0};
  struct cleanup *old_chain;
  struct value *val;

  fmt.format[0] = last_format[0];
  fmt.size = last_size;
  fmt.count = 1;

  if (exp && *exp == '/')
    {
      exp++;
      fmt = decode_format (&exp, last_format, last_size);
    }

  /* If we have an expression, evaluate it and use it as the address.  */

  if (exp != 0 && *exp != 0)
    {
      expr = parse_expression (exp);
      /* Cause expression not to be there any more
         if this command is repeated with Newline.
         But don't clobber a user-defined command's definition.  */
      if (from_tty)
	*exp = 0;
      old_chain = make_cleanup (free_current_contents, &expr);
      val = evaluate_expression (expr);
      if (TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_REF)
	val = value_ind (val);
      /* In rvalue contexts, such as this, functions are coerced into
         pointers to functions.  This makes "x/i main" work.  */
      if (			/* last_format == 'i'
				   && */ TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_FUNC
	   && VALUE_LVAL (val) == lval_memory)
	next_address = VALUE_ADDRESS (val);
      else
	next_address = SWIZZLE (value_as_pointer (val));
      if (VALUE_BFD_SECTION (val))
	next_section = VALUE_BFD_SECTION (val);
      do_cleanups (old_chain);
    }

  do_examine (fmt, next_address, next_section);

  /* If the examine succeeds, we remember its size and format for next time.  */
  last_size = fmt.size;
  last_format[0] = fmt.format[0];
  last_format[1] = fmt.format[1];

  /* Set a couple of internal variables if appropriate. */
  if (last_examine_value)
    {
      /* Make last address examined available to the user as $_.  Use
         the correct pointer type.  */
      struct type *pointer_type
	= lookup_pointer_type (VALUE_TYPE (last_examine_value));
      set_internalvar (lookup_internalvar ("_"),
		       value_from_pointer (pointer_type,
					   last_examine_address));

      /* Make contents of last address examined available to the user as $__. */
      /* If the last value has not been fetched from memory then don't
         fetch it now - instead mark it by voiding the $__ variable. */
      if (VALUE_LAZY (last_examine_value))
	set_internalvar (lookup_internalvar ("__"),
			 allocate_value (builtin_type_void));
      else
	set_internalvar (lookup_internalvar ("__"), last_examine_value);
    }
}


/* Add an expression to the auto-display chain.
   Specify the expression.  */

static void
display_command (char *exp, int from_tty)
{
  struct format_data fmt = {0, 0, 0};
  register struct expression *expr;
  register struct display *new;
  int display_it = 1;

#if defined(TUI)
  if (tui_version && *exp == '$')
    display_it = ((TuiStatus) (long) tuiDo (
		  (TuiOpaqueFuncPtr) tui_vSetLayoutTo, exp) == TUI_FAILURE);
#endif

  if (display_it)
    {
      if (exp == 0)
	{
	  do_displays ();
	  return;
	}

      if (*exp == '/')
	{
	  exp++;
	  fmt = decode_format (&exp, 0, 0);
	  if (fmt.size && fmt.format[0] == 0)
	    fmt.format[0] = 'x';
	  if (fmt.format[0] == 'i' || fmt.format[0] == 's')
	    fmt.size = 'b';
	}

      expr = parse_expression (exp);

      new = (struct display *) xmalloc (sizeof (struct display));

      new->exp = expr;
      new->block = innermost_block;
      new->next = display_chain;
      new->number = ++display_number;
      new->format = fmt;
      new->status = enabled;
      new->exp_string = (char *) xmalloc(sizeof (char) * strlen(exp) + 1);
      strcpy (new->exp_string, exp);
      display_chain = new;

      if (from_tty && target_has_execution)
	do_one_display (new);

      dont_repeat ();
    }
}
/* JAGae33079: Make display expressions persistent across re-runs.
   Refresh the display chain with updating the globals expression pointers. */
void
refresh_display_chain ()
{
   struct display *tmp = display_chain;
   struct display *prev = tmp;
   while (tmp)
     {
        /* Check whether the display block corresponds to globals */
        if (tmp->block == NULL)
          {
             free (tmp->exp);
             tmp->exp = parse_expression (tmp->exp_string);
             prev = tmp;
             tmp = tmp->next;
          }
        else
          {
	/* Delete all those display blocks that correspond to locals of a function */
             if ( prev == tmp)
               {
                  prev = tmp->next;
                  free_display (tmp);
                  tmp = prev;
                  display_chain = tmp;
                  continue;
               }
             prev->next = tmp->next;
             free_display (tmp);
             tmp = prev->next;
          }
     }
}

static void
free_display (struct display *d)
{
  free ((PTR) d->exp);
  free ( d->exp_string);
  free ((PTR) d);
}

/* Clear out the display_chain.
   Done when new symtabs are loaded, since this invalidates
   the types stored in many expressions.  */

void
clear_displays ()
{
  register struct display *d;

  while ((d = display_chain) != NULL)
    {
      free ((PTR) d->exp);
      display_chain = d->next;
      free ((PTR) d);
    }
}

/* Delete the auto-display number NUM.  */

static void
delete_display (int num)
{
  register struct display *d1, *d;

  if (!display_chain)
    error ("No display number %d.", num);

  if (display_chain->number == num)
    {
      d1 = display_chain;
      display_chain = d1->next;
      free_display (d1);
    }
  else
    for (d = display_chain;; d = d->next)
      {
	if (d->next == 0)
	  error ("No display number %d.", num);
	if (d->next->number == num)
	  {
	    d1 = d->next;
	    d->next = d1->next;
	    free_display (d1);
	    break;
	  }
      }
}

/* Delete some values from the auto-display chain.
   Specify the element numbers.  */

static void
undisplay_command (char *args, int from_tty)
{
  register char *p = args;
  register char *p1;
  register int num;

  if (args == 0)
    {
      if (query ("Delete all auto-display expressions? "))
	clear_displays ();
      dont_repeat ();
      return;
    }

  while (*p)
    {
      p1 = p;
      while (*p1 >= '0' && *p1 <= '9')
	p1++;
      if (*p1 && *p1 != ' ' && *p1 != '\t')
	error ("Arguments must be display numbers.");

      num = atoi (p);

      delete_display (num);

      p = p1;
      while (*p == ' ' || *p == '\t')
	p++;
    }
  dont_repeat ();
}

/* Display a single auto-display.  
   Do nothing if the display cannot be printed in the current context,
   or if the display is disabled. */

static void
do_one_display (struct display *d)
{
  int within_current_scope;

  if (d->status == disabled)
    return;

  if (d->block)
    within_current_scope = contained_in (get_selected_block (), d->block);
  else
    within_current_scope = 1;
  if (!within_current_scope)
    return;

  current_display_number = d->number;

  annotate_display_begin ();
  printf_filtered ("%d", d->number);
  annotate_display_number_end ();
  printf_filtered (": ");
  if (d->format.size)
    {
      CORE_ADDR addr;
      value_ptr val;

      annotate_display_format ();

      printf_filtered ("x/");
      if (d->format.count != 1)
	printf_filtered ("%d", d->format.count);
      printf_filtered ("%c", d->format.format[0]);
      if (d->format.format[0] != 'i' && d->format.format[0] != 's')
	printf_filtered ("%c", d->format.size);
      printf_filtered (" ");

      annotate_display_expression ();

      print_expression (d->exp, gdb_stdout);
      annotate_display_expression_end ();

      if (d->format.count != 1)
	printf_filtered ("\n");
      else
	printf_filtered ("  ");

      val = evaluate_expression (d->exp);
      addr = SWIZZLE (value_as_pointer (val));
      if (d->format.format[0] == 'i')
	addr = ADDR_BITS_REMOVE (addr);

      annotate_display_value ();

      do_examine (d->format, addr, VALUE_BFD_SECTION (val));
    }
  else
    {
      annotate_display_format ();

      if (d->format.format[0])
	printf_filtered ("/%c ", d->format.format[0]);

      annotate_display_expression ();

      print_expression (d->exp, gdb_stdout);
      annotate_display_expression_end ();

      printf_filtered (" = ");

      annotate_display_expression ();

      print_formatted (evaluate_expression (d->exp),
		       d->format.format, d->format.size, gdb_stdout);
      printf_filtered ("\n");
    }

  annotate_display_end ();

  gdb_flush (gdb_stdout);
  current_display_number = -1;
}

/* Display all of the values on the auto-display chain which can be
   evaluated in the current scope.  */

void
do_displays ()
{
  register struct display *d;

  for (d = display_chain; d; d = d->next)
    do_one_display (d);
}

/* Delete the auto-display which we were in the process of displaying.
   This is done when there is an error or a signal.  */

void
disable_display (int num)
{
  register struct display *d;

  for (d = display_chain; d; d = d->next)
    if (d->number == num)
      {
	d->status = disabled;
	return;
      }
  printf_unfiltered ("No display number %d.\n", num);
}

void
disable_current_display ()
{
  if (current_display_number >= 0)
    {
      disable_display (current_display_number);
      fprintf_unfiltered (gdb_stderr, "Disabling display %d to avoid infinite recursion.\n",
			  current_display_number);
    }
  current_display_number = -1;
}

static void
display_info (char *ignore, int from_tty)
{
  register struct display *d;

  if (!display_chain)
    printf_unfiltered ("There are no auto-display expressions now.\n");
  else
    printf_filtered ("Auto-display expressions now in effect:\n\
Num Enb Expression\n");

  for (d = display_chain; d; d = d->next)
    {
      printf_filtered ("%d:   %c  ", d->number, "ny"[(int) d->status]);
      if (d->format.size)
	printf_filtered ("/%d%c%c ", d->format.count, d->format.size,
			 d->format.format[0]);
      else if (d->format.format[0])
	printf_filtered ("/%c ", d->format.format[0]);
      print_expression (d->exp, gdb_stdout);
      if (d->block && !contained_in (get_selected_block (), d->block))
	printf_filtered (" (cannot be evaluated in the current context)");
      printf_filtered ("\n");
      gdb_flush (gdb_stdout);
    }
}

static void
enable_display (char *args, int from_tty)
{
  register char *p = args;
  register char *p1;
  register int num;
  register struct display *d;

  if (p == 0)
    {
      for (d = display_chain; d; d = d->next)
	d->status = enabled;
    }
  else
    while (*p)
      {
	p1 = p;
	while (*p1 >= '0' && *p1 <= '9')
	  p1++;
	if (*p1 && *p1 != ' ' && *p1 != '\t')
	  error ("Arguments must be display numbers.");

	num = atoi (p);

	for (d = display_chain; d; d = d->next)
	  if (d->number == num)
	    {
	      d->status = enabled;
	      goto win;
	    }
	printf_unfiltered ("No display number %d.\n", num);
      win:
	p = p1;
	while (*p == ' ' || *p == '\t')
	  p++;
      }
}

/* ARGSUSED */
static void
disable_display_command (char *args, int from_tty)
{
  register char *p = args;
  register char *p1;
  register struct display *d;

  if (p == 0)
    {
      for (d = display_chain; d; d = d->next)
	d->status = disabled;
    }
  else
    while (*p)
      {
	p1 = p;
	while (*p1 >= '0' && *p1 <= '9')
	  p1++;
	if (*p1 && *p1 != ' ' && *p1 != '\t')
	  error ("Arguments must be display numbers.");

	disable_display (atoi (p));

	p = p1;
	while (*p == ' ' || *p == '\t')
	  p++;
      }
}


/* Print the value in stack frame FRAME of a variable
   specified by a struct symbol.  */

void
print_variable_value (struct symbol *var, struct frame_info *frame, struct ui_file *stream)
{
  value_ptr val = read_var_value (var, frame);

  value_print (val, stream, 0, Val_pretty_default);
}


/* Fortran strings have their size passed at runtime, get the size
   from the inferior */
static int
get_parameter_size (struct symbol *sym)
{
  struct type *type = SYMBOL_TYPE (sym);

  if (SYMBOL_LANGUAGE (sym) == language_fortran
      && (TYPE_CODE (type) == TYPE_CODE_STRING
          || TYPE_CODE (type) == TYPE_CODE_ARRAY))
    {
      f77_get_dynamic_length_of_aggregate (type);
      return TARGET_PTR_BIT / TARGET_CHAR_BIT;
    }
  else
    return TYPE_LENGTH (type);
}

/* Print the arguments of a stack frame, given the function FUNC
   running in that frame (as a symbol), the info on the frame,
   and the number of args according to the stack frame (or -1 if unknown).  */

/* References here and elsewhere to "number of args according to the
   stack frame" appear in all cases to refer to "number of ints of args
   according to the stack frame".  At least for VAX, i386, isi.  */

void
print_frame_args (struct symbol *func, struct frame_info *fi, int num, struct ui_file *stream)
{
  struct block *b = NULL;
  int nsyms = 0;
  int first = 1;
  register int i;
  register struct symbol *sym;
  register value_ptr val;
  char val_format[2] = {0, 0};
  /* Offset of next stack argument beyond the one we have seen that is
     at the highest offset.
     -1 if we haven't come to a stack argument yet.  */
  long highest_offset = -1;
  int arg_size = 0;
  /* Number of ints of arguments that we have printed so far.  */
  int args_printed = 0;
#ifdef UI_OUT
  struct cleanup *old_chain, *list_chain;
  struct ui_stream *stb;

  stb = ui_out_stream_new (uiout);
  old_chain = make_cleanup_ui_out_stream_delete (stb);
#endif /* UI_OUT */

currently_printing_frame = fi;  /* JAGae92961 - store the currently frame which is being printed */
boolean inside_prologue = 0;
CORE_ADDR prologue_begin, prologue_end;
/* Check are we stopped inside the prologue of some function */
/* If yes, then stop printing values of func parameters, because
   they may contain garbage values */
  find_pc_partial_function (fi->pc, 0, &prologue_begin, 0);  
  prologue_end = SKIP_PROLOGUE (prologue_begin);
  if (fi->pc < prologue_end)
   inside_prologue = 1;
 
#ifdef INLINE_SUPPORT
  if (fi->inline_idx > 0)
    {
       for (b = get_frame_block(fi); b && b->is_inlined == 0;
            b = BLOCK_SUPERBLOCK (b))
         ;

       if (!b)
         b = get_frame_block(fi);
       nsyms = BLOCK_NSYMS (b);
    }
  else
    {
       if (func)
         {
            b = SYMBOL_BLOCK_VALUE (func);
            nsyms = BLOCK_NSYMS (b);
          }
     }
#else
  if (func)
    {
       b = SYMBOL_BLOCK_VALUE (func);
       nsyms = BLOCK_NSYMS (b);
    }
#endif
  /* Pring only first print_max elements of a larger array when it is passed 
   * as an argument */
  for (i = 0; i < nsyms; i++)
    {
      QUIT;
      sym = BLOCK_SYM (b, i);

#ifdef INLINE_SUPPORT
      if (sym->is_argument != 1) /* not an argument */
        continue;
#endif

      /* Keep track of the highest stack argument offset seen, and
         skip over any kinds of symbols we don't care about.  */

      switch (SYMBOL_CLASS (sym))
	{
	case LOC_ARG:
	case LOC_REF_ARG:
	  {
	    long current_offset = SYMBOL_VALUE (sym);
	    if (!inside_prologue)
              arg_size = get_parameter_size (sym);

	    /* Compute address of next argument by adding the size of
	       this argument and rounding to an int boundary.  */
	    current_offset =
	      ((current_offset + arg_size + sizeof (int) - 1)
		 & ~(sizeof (int) - 1));

	    /* If this is the highest offset seen yet, set highest_offset.  */
	    if (highest_offset == -1
		|| (current_offset > highest_offset))
	      highest_offset = current_offset;

	    /* Add the number of ints we're about to print to args_printed.  */
	    args_printed += (arg_size + sizeof (int) - 1) / sizeof (int);
	  }

	  /* We care about types of symbols, but don't need to keep track of
	     stack offsets in them.  */
	case LOC_REGPARM:
	case LOC_REGPARM_ADDR:
	case LOC_LOCAL_ARG:
	case LOC_BASEREG_ARG:
	case LOC_COMPUTED_ARG:
	case LOC_BASEREG_REF_ARG:
#ifdef INLINE_SUPPORT
	case LOC_REGISTER:

          if ((SYMBOL_CLASS(sym) == LOC_REGISTER) &&
              (fi->inline_idx == 0)) /* not an inlined function */
            break;
#endif
               
	  /* Set length for Fortran array and string parameters */
	  if (   ((SYMBOL_CLASS (sym) == LOC_REGPARM_ADDR)
	      || (SYMBOL_CLASS (sym) == LOC_COMPUTED_ARG)
	      || (SYMBOL_CLASS (sym) == LOC_BASEREG_ARG)
	      || (SYMBOL_CLASS (sym) == LOC_BASEREG_REF_ARG))
              && (!inside_prologue))
	    get_parameter_size (sym);
	  break;

	  /* Other types of symbols we just skip over.  */
	default:
	  continue;
	}

      /* We have to look up the symbol because arguments can have
         two entries (one a parameter, one a local) and the one we
         want is the local, which lookup_symbol will find for us.
         This includes gcc1 (not gcc2) on the sparc when passing a
         small structure and gcc2 when the argument type is float
         and it is passed as a double and converted to float by
         the prologue (in the latter case the type of the LOC_ARG
         symbol is double and the type of the LOC_LOCAL symbol is
         float).  */
      /* But if the parameter name is null, don't try it.
         Null parameter names occur on the RS/6000, for traceback tables.
         FIXME, should we even print them?  */

      if (*SYMBOL_NAME (sym))
	{
	  struct symbol *nsym;
	  nsym = lookup_symbol
	    (SYMBOL_NAME (sym),
	     b, VAR_NAMESPACE, (int *) NULL, (struct symtab **) NULL);
	  if (SYMBOL_CLASS (nsym) == LOC_REGISTER)
	    {
	      /* There is a LOC_ARG/LOC_REGISTER pair.  This means that
	         it was passed on the stack and loaded into a register,
	         or passed in a register and stored in a stack slot.
	         GDB 3.x used the LOC_ARG; GDB 4.0-4.11 used the LOC_REGISTER.

	         Reasons for using the LOC_ARG:
	         (1) because find_saved_registers may be slow for remote
	         debugging,
	         (2) because registers are often re-used and stack slots
	         rarely (never?) are.  Therefore using the stack slot is
	         much less likely to print garbage.

	         Reasons why we might want to use the LOC_REGISTER:
	         (1) So that the backtrace prints the same value as
	         "print foo".  I see no compelling reason why this needs
	         to be the case; having the backtrace print the value which
	         was passed in, and "print foo" print the value as modified
	         within the called function, makes perfect sense to me.

	         Additional note:  It might be nice if "info args" displayed
	         both values.
	         One more note:  There is a case with sparc structure passing
	         where we need to use the LOC_REGISTER, but this is dealt with
	         by creating a single LOC_REGPARM in symbol reading.  */

	      /* Leave sym (the LOC_ARG) alone.  */
	      ;
	    }
	  else
	    sym = nsym;
	}

#ifdef UI_OUT
      /* Print the current arg.  */
      if (!first)
        ui_out_text (uiout, ", ");
      ui_out_wrap_hint (uiout, "    ");

      annotate_arg_begin ();

      list_chain = make_cleanup_ui_out_tuple_begin_end (uiout, NULL);
      fprintf_symbol_filtered (stb->stream, SYMBOL_SOURCE_NAME (sym),
                            SYMBOL_LANGUAGE (sym), DMGL_PARAMS | DMGL_ANSI);
      ui_out_field_stream (uiout, "name", stb);
      annotate_arg_name_end ();
      ui_out_text (uiout, "=");
#else
      /* Print the current arg.  */
      if (!first)
	fprintf_filtered (stream, ", ");
      wrap_here ("    ");

      annotate_arg_begin ();

      fprintf_symbol_filtered (stream, SYMBOL_SOURCE_NAME (sym),
			    SYMBOL_LANGUAGE (sym), DMGL_PARAMS | DMGL_ANSI);
      annotate_arg_name_end ();
      fputs_filtered ("=", stream);
#endif

      /* Avoid value_print because it will deref ref parameters.  We just
         want to print their addresses.  Print ??? for args whose address
         we do not know.  We pass 2 as "recurse" to val_print because our
         standard indentation here is 4 spaces, and val_print indents
         2 for each recurse.  */
#ifdef UI_OUT
       /* JAGaf56550. Arguments that are printed as part of frame info confuses Eclipse.
	    * Since this is also not the expected behaviour in MI mode do not print them.
	    */
      if ( !ui_out_is_mi_like_p(uiout) )
	  {
#endif
      val = read_var_value (sym, fi);

      annotate_arg_value (val == NULL ? NULL : VALUE_TYPE (val));

      /* JAGaf56385: Check if the address is readable before going
	 for prining the value of the arguments. */
      if (val)
	{
	  char buf[sizeof (ULONGEST)];
	  int readable = 1;

	  if (SYMBOL_LANGUAGE (sym) == language_fortran) 
	    readable = (!target_read_memory (VALUE_ADDRESS (val), buf, 1) && 
                        (!inside_prologue));
	  if (readable)
	    {
              if (TYPE_CODE (VALUE_TYPE (val)) == TYPE_CODE_ARRAY_DESC)
                f_fixup_array_desc_type_array_addr (VALUE_TYPE (val),
                                                VALUE_ADDRESS (val));

              /* If the value is not available, print the message and return */
              switch (VALUE_AVAILABILITY (val)) 
                {
                  case VA_OPTIMIZED_OUT:
                    fputs_filtered ("<optimized out>", stream);
                    break;
                  case VA_NOT_AVAILABLE_HERE:
                    fputs_filtered ("<not available>", stream);
                    break;
                  case VA_NOT_A_THING:
                    fputs_filtered ("<NaT>", stream);
                    break;
                  case VA_AVAILABLE:
                    val_print (VALUE_TYPE (val), VALUE_CONTENTS (val), 0,
                               VALUE_ADDRESS (val),
                               stream, val_format, 0, 2, Val_no_prettyprint);
                    break;
                  default:
                    assert ("Wrong value for VALUE_AVAILABILITY(val)" == 0);
                };
  	    }
	  else
	    fputs_filtered ("<Uninitialized>", stream);
	}
      else
	fputs_filtered ("???", stream);
#ifdef UI_OUT
      }
#endif
#ifdef UI_OUT
	  do_cleanups (list_chain);
#endif  

      annotate_arg_end ();

      first = 0;
    }
  /* Don't print nameless args in situations where we don't know
     enough about the stack to find them.  */
  if (num != -1)
    {
      long start;

      if (highest_offset == -1)
	start = FRAME_ARGS_SKIP;
      else
	start = highest_offset;

      print_frame_nameless_args (fi, start, num - args_printed,
				 first, stream);
    }
#ifdef UI_OUT
  do_cleanups (old_chain);
#endif /* no UI_OUT */
}

/* Print nameless args on STREAM.
   FI is the frameinfo for this frame, START is the offset
   of the first nameless arg, and NUM is the number of nameless args to
   print.  FIRST is nonzero if this is the first argument (not just
   the first nameless arg).  */

static void
print_frame_nameless_args (struct frame_info *fi, long start, int num, int first,
			   struct ui_file *stream)
{
  int i;
  CORE_ADDR argsaddr;
  long arg_value;

  for (i = 0; i < num; i++)
    {
      QUIT;
#ifdef NAMELESS_ARG_VALUE
      NAMELESS_ARG_VALUE (fi, start, &arg_value);
#else
      argsaddr = FRAME_ARGS_ADDRESS (fi);
      if (!argsaddr)
	return;

      arg_value = read_memory_integer (argsaddr + start, sizeof (int));
#endif

      if (!first)
	fprintf_filtered (stream, ", ");

#ifdef	PRINT_NAMELESS_INTEGER
      PRINT_NAMELESS_INTEGER (stream, arg_value);
#else
#ifdef PRINT_TYPELESS_INTEGER
      PRINT_TYPELESS_INTEGER (stream, builtin_type_int, (LONGEST) arg_value);
#else
      fprintf_filtered (stream, "%ld", arg_value);
#endif /* PRINT_TYPELESS_INTEGER */
#endif /* PRINT_NAMELESS_INTEGER */
      first = 0;
      start += sizeof (int);
    }
}

/* ARGSUSED */
static void
printf_command (char *arg, int from_tty)
{
  register char *f = NULL;
  register char *s = arg;
  char *string = NULL;
  value_ptr *val_args;
  char *substrings;
  char *current_substring;
  int nargs = 0;
  int allocated_args = 20;
  struct cleanup *old_cleanups;
  char* l_position;
  int len;
  char* temp_buf;

  val_args = (value_ptr *) xmalloc (allocated_args * sizeof (value_ptr));
  old_cleanups = make_cleanup (free_current_contents, &val_args);

  if (s == 0)
    error_no_arg ("format-control string and values to print");

  /* Skip white space before format string */
  while (*s == ' ' || *s == '\t')
    s++;

  /* A format string should follow, enveloped in double quotes */
  if (*s++ != '"')
    error ("Bad format string, missing '\"'.");

  /* Parse the format-control string and copy it into the string STRING,
     processing some kinds of escape sequence. We may convert some %lx to 
     %llx, and %p to %llx so increase by 2. */

  len = (int) strlen (s);
  f = string = (char *) alloca (len + 1 + len);
  temp_buf = (char *) alloca (len + 1 + len);

  while (*s != '"')
    {
      int c = *s++;
      switch (c)
	{
	case '\0':
	  error ("Bad format string, non-terminated '\"'.");

	case '\\':
	  switch (c = *s++)
	    {
	    case '\\':
	      *f++ = '\\';
	      break;
	    case 'a':
#ifdef __STDC__
	      *f++ = '\a';
#else
	      *f++ = '\007';	/* Bell */
#endif
	      break;
	    case 'b':
	      *f++ = '\b';
	      break;
	    case 'f':
	      *f++ = '\f';
	      break;
	    case 'n':
	      *f++ = '\n';
	      break;
	    case 'r':
	      *f++ = '\r';
	      break;
	    case 't':
	      *f++ = '\t';
	      break;
	    case 'v':
	      *f++ = '\v';
	      break;
	    case '"':
	      *f++ = '"';
	      break;
	    default:
	      /* ??? TODO: handle other escape sequences */
	      error ("Unrecognized escape character \\%c in format string.",
		     c);
	    }
	  break;

	default:
	  *f++ = c;
	}
    }

  /* Skip over " and following space and comma.  */
  s++;
  *f++ = '\0';
  while (*s == ' ' || *s == '\t')
    s++;

  if (*s != ',' && *s != 0)
    error ("Invalid argument syntax");

  if (*s == ',')
    s++;
  while (*s == ' ' || *s == '\t')
    s++;

  /* Need extra space for the '\0's.  Doubling the size is sufficient.  */
  substrings = (char *) alloca (strlen (string) * 2);
  current_substring = substrings;

  {
    /* Now scan the string for %-specs and see what kinds of args they want.
       argclass[I] classifies the %-specs so we can give printf_filtered
       something of the right size.  */

    enum argclass
      {
	no_arg, int_arg, string_arg, double_arg, long_long_arg
      };
    enum argclass *argclass;
    enum argclass this_argclass;
    char *last_arg;
    int nargs_wanted;
    int lcount;
    int i;

    argclass = (enum argclass *) alloca (strlen (string) * sizeof *argclass);
    nargs_wanted = 0;
    l_position = 0;  /* initialize for compiler warning */
    f = string;
    last_arg = string;
    while (*f)
      if (*f++ == '%')
	{
	  lcount = 0;
	  while (strchr ("0123456789.hlL-+ #", *f))
	    {
	      if (*f == 'l' || *f == 'L')
	        {
		  lcount++;
		  l_position = f;
		}
	      f++;
	    }
	  switch (*f)
	    {
	    case 's':
	      this_argclass = string_arg;
	      break;

	    case 'e':
	    case 'f':
	    case 'g':
	      this_argclass = double_arg;
	      break;

	    case '*':
	      error ("`*' not supported for precision or width in printf");

	    case 'n':
	      error ("Format specifier `n' not supported in printf");

	    case '%':
	      this_argclass = no_arg;
	      break;

            case 'p':
              /* pointer data.  If it doesn't fit into an int, then
                 chang "p" to "llx" and the type to long long.
                 */
              if (TARGET_PTR_BIT / TARGET_CHAR_BIT <= sizeof(int))
                {
                  this_argclass = int_arg;
                }
              else
                {
                  this_argclass = long_long_arg;
                  strcpy(temp_buf, f + 1); /* copy everything after 'p' */
                  *(f) = 'l';
                  *(f + 1) = 'l';
                  *(f + 2) = 'x';
                  strcpy(f+3, temp_buf);
                  f += 2;  /* we insterted two characters */
                }
              
              break;

	    default:
	      if (lcount > 1)
		this_argclass = long_long_arg;
	      else
                if (   lcount == 0 
                    || TARGET_LONG_BIT / TARGET_CHAR_BIT == sizeof(int))
                  this_argclass = int_arg;
                else
		  {
		    /* The user specified %lx, but a long is bigger than
                       an int, so change the format to %llx and set the
		       type to long_long_arg.  Note that "x" cound be any
		       of the integer specifiers.  Move everything in
		       string after "l" to the right by one and insert another
		       "l". */
                  this_argclass = long_long_arg;
                  strcpy(temp_buf, l_position + 1);
                  *(l_position + 1) = 'l';
                  strcpy(l_position + 2, temp_buf);
                  f++;  /* Move ptr one to account for instered character */
                }
	      break;
	    }
	  f++;
	  if (this_argclass != no_arg)
	    {
	      strncpy (current_substring, last_arg, f - last_arg);
	      current_substring += f - last_arg;
	      *current_substring++ = '\0';
	      last_arg = f;
	      argclass[nargs_wanted++] = this_argclass;
	    }
	}

    /* Now, parse all arguments and evaluate them.
       Store the VALUEs in VAL_ARGS.  */

    while (*s != '\0')
      {
	char *s1;
	if (nargs == allocated_args)
	  val_args = (value_ptr *) xrealloc ((char *) val_args,
					     (allocated_args *= 2)
					     * sizeof (value_ptr));
	s1 = s;
	val_args[nargs] = parse_to_comma_and_eval (&s1);

	/* If format string wants a float, unchecked-convert the value to
	   floating point of the same size */

	if (argclass[nargs] == double_arg)
	  {
	    struct type *type = VALUE_TYPE (val_args[nargs]);
	    if (TYPE_LENGTH (type) == sizeof (float))
	        VALUE_TYPE (val_args[nargs]) = builtin_type_float;
	    if (TYPE_LENGTH (type) == sizeof (double))
	        VALUE_TYPE (val_args[nargs]) = builtin_type_double;
	  }
	nargs++;
	s = s1;
	if (*s == ',')
	  s++;
      }

    if (nargs != nargs_wanted)
      error ("Wrong number of arguments for specified format-string");

    /* Now actually print them.  */
    current_substring = substrings;
    for (i = 0; i < nargs; i++)
      {
	switch (argclass[i])
	  {
	  case string_arg:
	    {
	      char *str;
	      CORE_ADDR tem;
	      int j;
	      tem = SWIZZLE (value_as_pointer (val_args[i]));

	      /* This is a %s argument.  Find the length of the string.  */
	      for (j = 0;; j++)
		{
		  char c;
		  QUIT;
		  read_memory (tem + j, &c, 1);
		  if (c == 0)
		    break;
		}

	      /* Copy the string contents into a string inside GDB.  */
	      str = (char *) alloca (j + 1);
	      read_memory (tem, str, j);
	      str[j] = 0;

	      printf_filtered (current_substring, str);
	    }
	    break;
	  case double_arg:
	    {
	      double val = value_as_double (val_args[i]);
	      printf_filtered (current_substring, val);
	      break;
	    }
	  case long_long_arg:
#if defined (CC_HAS_LONG_LONG) && defined (PRINTF_HAS_LONG_LONG)
	    {
	      long long val = value_as_long (val_args[i]);
	      printf_filtered (current_substring, val);
	      break;
	    }
#else
	    error ("long long not supported in printf");
#endif
	  case int_arg:
	    {
	      /* FIXME: there should be separate int_arg and long_arg.  */
	      long val = value_as_long (val_args[i]);
	      printf_filtered (current_substring, val);
	      break;
	    }
	  default:		/* purecov: deadcode */
	    error ("internal error in printf_command");		/* purecov: deadcode */
	  }
	/* Skip to the next substring.  */
	current_substring += strlen (current_substring) + 1;
      }
    /* Print the portion of the format string after the last argument.  */
    printf_filtered (last_arg);
  }
  do_cleanups (old_cleanups);
}

/* Dump a specified section of assembly code.  With no command line
   arguments, this command will dump the assembly code for the
   function surrounding the pc value in the selected frame.  With one
   argument, it will dump the assembly code surrounding that pc value.
   Two arguments are interpeted as bounds within which to dump
   assembly.

   If disassemble_command_hook is not NULL, output will not go to
   gdb_stdout; instead, output will be buffered and disassemble_command_hook
   will be passed the output buffer, once for each line of disassembly.  */

extern CORE_ADDR tui_vGetLowDisassemblyAddress (va_list);
#define PRINT_WRAP(X) fprintf_filtered (stream, X)
#define PRINT_WRAP_WITH_ARG(FORMAT, ARG) fprintf_filtered (stream, FORMAT, ARG)
#define PRINT_WRAP_WITH_ARGx2(FORMAT, ARG1, ARG2) \
  fprintf_filtered (stream, FORMAT, ARG1, ARG2)
#define PRINT_WRAP_WITH_ARGx3(FORMAT, ARG1, ARG2, ARG3) \
  fprintf_filtered (stream, FORMAT, ARG1, ARG2, ARG3)


/* Struct to pass information to the handler function from the disassemble
   command. */

struct handle_print_doc_state {
  struct ui_file *stream;
  int is_not_first_print;
};

/* Handler function to print the DOC line information with
   foreach_pc_doc_line. The first argument is the DOC linetable entry. The
   second is a flag that is set to non-zero if there was an exact match to
   the PC requested.  The third contains the user argument that was passed
   from the caller. The disassemble command has created a user argument of
   the type handle_print_doc_state. */

static void
handle_print_doc_line_info(struct doc_linetable_entry *dle, int exactly_matched_pc, void *arg1)
{
  struct ui_file *stream;
  struct handle_print_doc_state *state;

  state = (struct handle_print_doc_state *) arg1;
  stream = state->stream;

  if (dle)
    {
      /* Check if first time through */
      if (!state->is_not_first_print)
        {
          PRINT_WRAP (";;;");
        }
      /* Print line and column information */
      if (dle->flags.flag.has_column)
        {
          if (dle->column_pos_length > 0)
            {
              PRINT_WRAP_WITH_ARGx3 (" [%d, %d, %d]",
                                     dle->line,
                                     dle->column_pos,
                                     (dle->column_pos +
                                      dle->column_pos_length - 1));
            }
          else
            {
              PRINT_WRAP_WITH_ARGx2 (" [%d, %d]",
                                     dle->line,
                                     dle->column_pos);
            }
        }
      else
        {
          PRINT_WRAP_WITH_ARG (" [%d]", dle->line);
        }
      /* Print critical point */
      if ((exactly_matched_pc) && (dle->flags.flag.is_critical))
        {
          /* Only print critical point if the PCs exactly match */
          PRINT_WRAP (" (Critical Point)");
        }
      /* Mark that this has executed at least once already. */
      state->is_not_first_print = 1;
    }
}

/* Prints the doc line information for a given PC. */
void
print_doc_lines (CORE_ADDR pc, struct ui_file * stream)
{
  struct handle_print_doc_state state;
  state.stream = stream;
  state.is_not_first_print = 0;
  /* Find the DOC lines */
  foreach_pc_doc_line (pc, 0, handle_print_doc_line_info,
                                 (void *) &state);
  if (state.is_not_first_print)
    fprintf_filtered (stream, "\n");

}


#define PRINT_WRAP(X) fprintf_filtered (stream, X)
#define PRINT_WRAP_WITH_ARG(FORMAT, ARG) fprintf_filtered (stream, FORMAT, ARG)
#define PRINT_WRAP_WITH_ARGx2(FORMAT, ARG1, ARG2) \
  fprintf_filtered (stream, FORMAT, ARG1, ARG2)
#define PRINT_WRAP_WITH_ARGx3(FORMAT, ARG1, ARG2, ARG3) \
  fprintf_filtered (stream, FORMAT, ARG1, ARG2, ARG3)


/* Srikanth, Firebolt disassembly implementation */

/* Read the code stream associated with the frame in one fell scoop
   and cache that for future use. This avoids the penalty of one ttrace
   call per instruction.
*/
static unsigned int * cached_code_stream;
static CORE_ADDR cached_low_address;
static CORE_ADDR cached_high_address;

/* Consult the cache and simply supply available value. */
int
firebolt_read_memory (bfd_vma memaddr, bfd_byte *myaddr, unsigned int len, disassemble_info *info)
{
    int insn_index;

    if (memaddr < cached_low_address || memaddr > cached_high_address)
      return 1;

    insn_index = (int) ((memaddr - cached_low_address)/4);
    memcpy (myaddr, cached_code_stream + insn_index, len);
    return 0;
}

/* Srikanth, unassemble_command : a new function for use with firebolt.
   There are several problems with disassemble_command as it stands now.
   As such clients such as the WDB-GUI, ddd etc suffer from :
       (1) Its output is very cluttered with the <function+offset> fields
           next to the address field and next to the conditional branch
           targets. In C++, with long function names, one has to search hard
           to see the instruction.
       (2) It does not cache the results. So if you are in disassembly mode
           and you do an up and a down, it would result in two disassembles.
           Same applies for step into and return from function.
       (3) It uses 1 ttrace read for each instruction. This is very wasteful.
       (4) It opens the source file and closes it for every source line !!!
       (5) It repeatedly calls find_pc_line for each PC in range !!!
       (6) It emits its output to stdout, this ASCII pipe mechanism is slow.
       (7) On IPF we read each bundle as many times as there are slots !!!
*/
/* ARGSUSED */
void
unassemble_command (char *arg, int from_tty)
{
  CORE_ADDR low = 0, high = 0;
  char *name = 0;
  CORE_ADDR pc = 0, stop_pc = 0;
  int last_line = -1;
  struct symtab_and_line sal;
  struct ui_file *stream = 0;
  int desc = 0;
  FILE *strm = 0;
  char line[4097];
  char pc_pattern[128];
  disassemble_info asm_info;
  char output_file[512];
  size_t bytes_to_read = 0;

  if (arg == 0 && selected_frame == 0)
    error ("No frame selected.\n");

  if (arg == 0)
    stop_pc = pc = get_frame_pc (selected_frame);
  else
    stop_pc = pc = parse_and_eval_address (arg);

  if (find_pc_partial_function (pc, &name, &low, &high) == 0)
    error ("Unable to identify function.\n");

  pc = low;
  sal = find_pc_line(pc, 0);

  /* Assembly output is captured and cached in temporary files of the
     form /tmp/.firebolt.<ppid>.<low>-<high> If the file already exists,
     just reuse that.
  */

  sprintf (output_file, "/tmp/.firebolt.%d.%llu-%llu", getppid(),
                              (unsigned long long) low,
                              (unsigned long long)  high);

  if (access (output_file, R_OK) == 0)
    goto display_file;


  /* No luck. This is the first time we are unassembling this frame,
     Do the hard work and cache the results for future.
  */

  stream = gdb_fopen (output_file, FOPEN_WT);
  if (!stream)
    error ("Could not create temporary file\n");

  INIT_DISASSEMBLE_INFO_NO_ARCH (asm_info, stream, fprintf_unfiltered);
  asm_info.read_memory_func = firebolt_read_memory;
  asm_info.memory_error_func = dis_asm_memory_error;

  fprintf_unfiltered (stream, "Dump of assembler code ");
  if (name != NULL)
    fprintf_unfiltered (stream, "for function %s:\n", name);


  /* Print source file. */
  if (sal.symtab)
    {
      fprintf_unfiltered (stream, "; File: %s\n", sal.symtab->filename);
      last_line = -1;
      desc = open_source_file (sal.symtab);
      if (desc > 0)
        {
          strm = fdopen (desc, FOPEN_RT);
          if (sal.symtab->line_charpos == 0)
            find_source_lines (sal.symtab, desc);
        }
    }

  /* We don't want to read the instructions one at a time using 1 ttrace
     call per instruction. Read in the whole thing at one stroke and
     cache it. We will dole out the pieces later.
  */

  bytes_to_read = high - low;
  free (cached_code_stream);
  cached_low_address = low;
  cached_high_address = high;
  cached_code_stream = xmalloc (bytes_to_read);
  target_read_memory (low, (char*) cached_code_stream, (int) bytes_to_read);

  /* This is the simplified version of the guts of disassemble_command */

  while (pc < high)
    {
      if (desc > 0 && pc >= sal.end)
        {
          sal = find_pc_line(pc, 0);
          last_line = sal.line;
          if (last_line > 0 && last_line <= sal.symtab->nlines)
            {
              if (fseek (strm, sal.symtab->line_charpos[last_line - 1], 0) >= 0)
                {
                  fgets (line, 4096, strm);
                  fprintf_unfiltered (stream, "\n; %s\n", line);
                }
            }
        }
          
        fprintf_unfiltered (stream, (sizeof (CORE_ADDR) == 4) ? 
				    "0x%.8x:\t" : "0x%16.llx:\t", pc);
        pc += tm_print_insn (pc, &asm_info);
        fprintf_unfiltered (stream, "\n");
      }

    if (strm)
      fclose (strm);
    ui_file_delete (stream);
    free (cached_code_stream);
    cached_code_stream = 0;
    cached_low_address = cached_high_address = 0;

display_file:

    /* sal.pc is the first PC in the line, which is not what we want.
       Adjust it to the stop_pc
    */
    sal = find_pc_line(stop_pc, 0);
    sal.pc = stop_pc;

    sprintf (pc_pattern, (sizeof (CORE_ADDR) == 4) ?
					   "0x%.8x" : "0x%.16llx", stop_pc);
    printf_unfiltered ("\033\032\032:se stl=%s\n:e +/^%s %s\n\032\032A",
                wdb_status_line(sal), pc_pattern, output_file);
}


/* ARGSUSED */
static void
disassemble_command (char *arg, int from_tty)
{
#define BUFLEN 512
  CORE_ADDR low, high;
  char *name;
  CORE_ADDR pc, pc_masked;
  char *space_index;
  char *last_file = NULL;
  int last_line = -1;
  struct symtab_and_line sal;
  char *line_buf = 0; /* initialize for compiler warning */
  struct ui_file *stream;
  extern int firebolt_version;
#ifdef HP_IA64
  bool mixed_mode_pa_lib = false;
#endif /*HP_IA64 */

#ifdef TUI
  /* If disassemble_command_hook is set, direct disassembly to a buffer. */
  if (disassemble_command_hook != NULL)
    {
      stream = tui_sfileopen (BUFLEN);
      line_buf = tui_file_get_strbuf (stream);
    }
  else
#endif
    stream = gdb_stdout;

  name = NULL;
  if (!arg)
    {
      if (!selected_frame)
	error ("No frame selected.\n");

#ifdef GDB_TARGET_IS_HPPA
      if (firebolt_version)
        {
          unassemble_command (arg, from_tty);
          return;
        }
#endif

      pc = get_frame_pc (selected_frame);
#ifdef HP_IA64
      /* IA64: Instruction addresses are slot encoded. */
      /* Bundle-align PC */
      pc &= ~(0xF);
      if (is_swizzled)
        pc = SWIZZLE(pc);

#endif /*HP_IA64 */
      if (find_pc_partial_function (pc, &name, &low, &high) == 0)
	error ("No function contains program counter for selected frame.\n");
#if defined(TUI)
      else if (tui_version)
        low = tuiDoCoreAddr(tui_vGetLowDisassemblyAddress, 
                            (CORE_ADDR)low, 
                            (CORE_ADDR)pc);
#endif
      low += FUNCTION_START_OFFSET;
    }
  else if (!(space_index = (char *) strchr (arg, ' ')))
    {
      /* One argument.  */
      pc = parse_and_eval_address (arg);
#ifdef HP_IA64
      /* JAGag22025: jini: For ILP32 applications on IPF, the 64 bit
         shared library addresses do not get swizzled due to the
         pointer type size being 4. Redo the swizzle. */
      pc = SWIZZLE (pc);
#endif

      if (find_pc_partial_function (pc, &name, &low, &high) == 0)
	error ("No function contains specified address.\n");
#if defined(TUI)
      else if (tui_version)
        low = (CORE_ADDR)tuiDoCoreAddr(tui_vGetLowDisassemblyAddress, 
                               (CORE_ADDR)low, 
                               (CORE_ADDR)pc);
#endif
      low += FUNCTION_START_OFFSET;
    }
  else
    {
      /* Two arguments.  */
      *space_index = '\0';
      low = parse_and_eval_address (arg);
      high = parse_and_eval_address (space_index + 1);
    }

#ifdef HP_IA64
  /* IA64: Instruction addresses are slot encoded. */
  /* Bundle-align LOW and HIGH */
  /* jini: 060511: If we are dealing with a PA library in mixed mode execution,
     then avoid the bundle align. */
  mixed_mode_pa_lib = in_mixed_mode_pa_lib (low, NULL);
  if (!mixed_mode_pa_lib)
    {
      low &= ~(0xF);
      high &= ~(0xF);
    }
#endif /*HP_IA64 */

  /* Special support for nimbus assembly level debugging */
  if (nimbus_version && !firebolt_version)
    {
      switch_to_assembly_mode (low, high);

      /* free other non_nimbus resources before return */
      if (disassemble_command_hook != NULL)
        {
          (*disassemble_command_hook) ("");
          ui_file_delete (stream);
	  stream = NULL;
        }
      return;
    }

#if defined(TUI)
  if (!tui_version ||
      m_winPtrIsNull (disassemWin) || !disassemWin->generic.isVisible)
#endif
    {
      int did_print_doc_line_spec = 0;

      /* Don't pass header to hook. */
      if (disassemble_command_hook == NULL)
        {
	  printf_filtered ("Dump of assembler code ");
	  if (name != NULL)
	    {
	      printf_filtered ("for function %s:\n", name);
	    }
	  else
	    {
	      printf_filtered ("from ");
	      print_address_numeric (low, 1, gdb_stdout);
	      printf_filtered (" to ");
	      print_address_numeric (high, 1, gdb_stdout);
	      printf_filtered (":\n");
	    }
	}
      /* Dump the specified range.  */
      pc = low;

#ifdef HP_IA64
      /* jini: 060511: Use the PA disassembler function if this is a
         mixed mode PA library. */
      if (mixed_mode_pa_lib)
        {
          tm_print_insn = print_insn_hppa;
        }
#endif /*HP_IA64 */

#ifdef GDB_TARGET_MASK_DISAS_PC
      pc_masked = GDB_TARGET_MASK_DISAS_PC (pc);
#else
      pc_masked = pc;
#endif

      /* Print each disassembly line. */
      /* JAGaf64788: pc_masked was going right upto high (<=high)
       */
      while (pc_masked < high)
	{
	  QUIT;
          
          /* RM: try to print source line in disassembly display */
          sal = find_pc_line(pc, 0);

          /* Print information on the DOC line format if one exists. */
          if (!did_print_doc_line_spec)
            {
              if (sal.symtab && sal.symtab->doc_linetable)
                {
                  PRINT_WRAP (";;; DOC Line Information: "
                              "[Line, Column Start, Column End] "
                              "[Line, Column] "
                              "[Line]\n");
                }
              did_print_doc_line_spec = 1;
            }

          /* Print information on the DOC line format if one exists. */
          if (!did_print_doc_line_spec)
            {
              if (sal.symtab && sal.symtab->doc_linetable)
                {
                  PRINT_WRAP (";;; DOC Line Inforation: "
                              "[Line, Column Start, Column End] "
                              "[Line, Column] "
                              "[Line]\n");
                }
              did_print_doc_line_spec = 1;
            }

          /* Print source file. */
          if (sal.symtab &&
	      (! last_file || strcmp (sal.symtab->filename, last_file)))
            {
              last_file = sal.symtab->filename;
              if (sal.line == 0)
                PRINT_WRAP (";;; File: ???\n");
               else
                PRINT_WRAP_WITH_ARG (";;; File: %s\n", last_file);
               last_line = -1;
             }

          /* Print DOC line information. */
          {
	    print_doc_lines (pc, stream);
          }

          /* Print each new source line. */
          if (sal.line != last_line)
            {
              last_line = sal.line;
              if (last_line != 0)
                {
                  int desc;

                  desc = open_source_file (sal.symtab);
                  if (desc < 0 )
                    PRINT_WRAP_WITH_ARG (";;; Line: %d\n", last_line);
                  else 
                    {
                      /* Find the source line and print it. */
                      if (sal.symtab->line_charpos == 0)
                        find_source_lines (sal.symtab, desc);
                      if (last_line > 0 && last_line <= sal.symtab->nlines &&
                          (lseek (desc, sal.symtab->line_charpos[last_line - 1], 0) >= 0))
                        {
                          FILE *strm;
                          char c;

                          /* To print the line number for the source lines - JAGaf07855 */
                          PRINT_WRAP_WITH_ARG (";;;  %d ", last_line); 
                          strm = fdopen (desc, FOPEN_RT);
                          clearerr (strm);

                          /* Modified version from source.c */
                          c = fgetc (strm);
                          if (c != EOF)
                            {
                              do
                                {
                                  if (c < 040 && c != '\t' &&
                                      c != '\n' && c != '\r')
                                    PRINT_WRAP_WITH_ARG ("^%c", c + 0100);
                                  else if (c == 0177)
                                    PRINT_WRAP ("^?");
#ifdef CRLF_SOURCE_FILES
                                  else if (c == '\r')
                                    {
                                      /* Just skip \r characters.  */
                                    }
#endif
                                  else
                                    PRINT_WRAP_WITH_ARG ("%c", c);
                                }
                              while (c != '\n' && (c = fgetc (strm)) >= 0);
                            }
                          fclose (strm);
                        }
                    }
                }
            }
          
          print_address (pc, stream);
          PRINT_WRAP (":\t");
          /* We often wrap here if there are long symbolic names.  */
          if (disassemble_command_hook == NULL)
            wrap_here ("    ");
          pc += print_insn (pc, stream);
          PRINT_WRAP ("\n");

#ifdef GDB_TARGET_MASK_DISAS_PC
          pc_masked = GDB_TARGET_MASK_DISAS_PC (pc);
#else
          pc_masked = pc;
#endif

          if (disassemble_command_hook != NULL)
            {
              (*disassemble_command_hook) (line_buf);
              line_buf[0] = (char) 0;
            }
        }
          /* Print DOC line information. */
          {
	    print_doc_lines (pc, stream);
          }

      /* For disassemble_command_hook, flag end of disassembly with empty string. */
      if (disassemble_command_hook != NULL)
        {
          (*disassemble_command_hook) ("");
          ui_file_delete (stream);
	  stream = NULL;
        }
      else
        {
	  printf_filtered ("End of assembler dump.\n");
	  gdb_flush (gdb_stdout);
	}

#ifdef HP_IA64
      /* jini: 060511: Restore the method used to print assembly instructions.
         This was overridden for mixed mode PA libraries. */
      if (mixed_mode_pa_lib)
        {
          tm_print_insn = print_insn_ia64;
        }
#endif /*HP_IA64 */

    }
#if defined(TUI)
  else
    {
      tuiDo ((TuiOpaqueFuncPtr) tui_vAddWinToLayout, DISASSEM_WIN);
      tuiDoCoreAddr ((TuiCoreAddrFuncPtr)tui_vUpdateSourceWindowsWithAddr,low);
    }
#endif
}

/* JAGae36708 - Array Browsing 
 The function dump2file_command is used to dump the values of an integer or float array into a given file in the array format. */ 


static void
dump2file_command (char *args, int fromtty)
 {
  char *arg, *type_str, *exp = 0;  /* initialize for compiler warning */
  int argcnt = 0;
  struct expression *expr;
  register struct cleanup *old_chain;
  register value_ptr val;
  struct format_data fmt = {0, 0, 0, 0} ;
  struct ui_file *stream;
  char format[2] = {0, 0};
  struct type *type;
  struct cleanup *my_cleanups = 0;
  struct type *tmp_type;
  int length, old_repeat_count_threshold, old_print_max;

 /* Checking the number of arguments passed to the  command*/
 
 if (args == NULL)
    error ("dump2file takes an array name and a file name");

 args = xstrdup (args); 

 while (*args != '\000')
  {
    while (isspace (*args))
      args++;
   
    arg = args;

    while ((*args != '\000') && (*args != 32)) 
      args++;

    if (*args != '\000')
      *args++ = '\000';

    if (argcnt == 1)
      {
        ascii_filename = tilde_expand (arg);
        my_cleanups = make_cleanup (free, ascii_filename);
      }
    else
     if (argcnt == 0)
       exp = arg;
    argcnt ++;
   }
  if (argcnt <= 1)
     error ("dump2file takes an array name and a file name");
  if (argcnt > 2)
     error ("Too many arguments");

  expr = parse_expression (exp);
  old_chain = make_cleanup (free_current_contents, &expr);
  val = evaluate_expression (expr);
  type = VALUE_TYPE (val);
   
  stream = gdb_fopen (ascii_filename, FOPEN_WT);   /* Changing GDB's stream to the given filename */

  if (stream == 0)
     perror_with_name (ascii_filename);

  array_name = exp;

  if (TYPE_CODE(type) == TYPE_CODE_ARRAY)   /* Checking whether the given variable is an array */
    {
       dimension = 0;
       tmp_type = type;
       length = TYPE_LENGTH (type);

  /* Loop to find the dimension and the subscripts of the array */

       while ((tmp_type = TYPE_TARGET_TYPE (tmp_type)))
         {
           dim[dimension] = length / (TYPE_LENGTH (tmp_type));
           if (TYPE_CODE (tmp_type) == TYPE_CODE_ARRAY)
              dimension++;
           length = TYPE_LENGTH (tmp_type);
          }   

       old_repeat_count_threshold = repeat_count_threshold; 
       old_print_max = print_max;
       print_max = TYPE_LENGTH (type); 
       repeat_count_threshold = TYPE_LENGTH (type); /* Set to the number of elements in the array */ 
       fprintf_unfiltered (gdb_stdout, "Dumping the values into the file...\n");
       annotate_value_begin (VALUE_TYPE (val));
       print_formatted (val, format, fmt.size, stream);
       fprintf_unfiltered (stream,"\0");
       annotate_value_end ();
       repeat_count_threshold = old_repeat_count_threshold; /* Original value */ 
       print_max = old_print_max; /* Original value */
       print_array_in_matrix ();  /* Printing the array in the required format in the file*/
     }
  else
      fprintf_filtered (gdb_stdout, "The variable is not an array.\n");
  ui_file_delete (stream);
  do_cleanups (old_chain);
  if (my_cleanups)
    do_cleanups (my_cleanups); 
 }

/* Function to print the array in the required format into the file */

void
print_array_in_matrix ()
 {
  struct ui_file *strm;
  int i, j;
  long int length;
  char *array_str, *final_str;
  char  *str, *s ;
  FILE *array_fp;

   
  array_fp = fopen (ascii_filename, "r+");
 
  if (array_fp == 0)
    perror_with_name (ascii_filename);
 
  fseek (array_fp,0, SEEK_END);

  length = ftell (array_fp);
  
  array_str = (char *) xmalloc(length); 
  final_str = (char *) xmalloc(length); 
  rewind (array_fp);
  
  fgets (array_str, (int) length, array_fp);
  fclose (array_fp);

  str = array_str;
  s = final_str;
  strm = gdb_fopen (ascii_filename, FOPEN_WT);

  if (strm == 0)
    perror_with_name (ascii_filename);
 
  make_cleanup_ui_file_delete (strm);
   
  /* The characters ',', ' ','(', ')', '{', '}' are the delimiters. Find them to parse the output and store the values */ 
 
  while ( str && *str != '\0' ) 
    {
      if ((*str != '{') && (*str != '(') && (*str != 32))
        {
         if ((*str != ',') && (*str != '}') && (*str != ')'))
     	   *s++ = *str;
         else
	  // to balance stuff in quotes: ((
          if (((*str == ',') || (*str =='}') ||  (*str == ')')) && ( (*(str-1) != '}') && (*(str-1) != ')') ) )  
 	    *s++ = '\n';
        }
       *str++;
    }
  *s='\0';

  /* Printing the array elements into the file in array format */

  fprintf_unfiltered (strm,"%%%%ArrayBrowsing matrix array %s\n", array_name);
  fprintf_unfiltered (strm,"%% A ");
 
  for (i=0; i<=dimension; i++)
    {
      if (i != dimension)
        fprintf_unfiltered (strm, "%dx", dim[i]);
      else
        fprintf_unfiltered (strm, "%d matrix\n", dim[i]);
    }

  for (i=0; i<=dimension; i++)
    fprintf_unfiltered (strm,"%d ",dim[i]);
 
  fprintf_unfiltered (strm, "\n%s", final_str);
  fprintf_unfiltered (strm, "\n");

  free (array_str);
  free (final_str);
  ui_file_delete (strm);
 
 }


 /*Print the instruction at address MEMADDR in debugged memory,
   on STREAM.  Returns length of the instruction, in bytes.  */

int
print_insn (CORE_ADDR memaddr, struct ui_file *stream)
{
  /* baskar, JAGae31493, set the stream field in disassemble_info */
  TARGET_PRINT_INSN_INFO->stream = (PTR)(stream);

  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    TARGET_PRINT_INSN_INFO->endian = BFD_ENDIAN_BIG;
  else
    TARGET_PRINT_INSN_INFO->endian = BFD_ENDIAN_LITTLE;

  if (TARGET_ARCHITECTURE != NULL)
    TARGET_PRINT_INSN_INFO->mach = TARGET_ARCHITECTURE->mach;
  /* else: should set .mach=0 but some disassemblers don't grok this */

  return TARGET_PRINT_INSN (memaddr, TARGET_PRINT_INSN_INFO);
}

/* Srikanth, this command is for the internal use of Firebolt only.
   If the user issues this command directly, reject it as it would
   cause the GUI to get out of sync with the debugger.
*/
static void
toggle_display_command (char *exp, int from_tty)  /* args not used */
{
    extern int server_command;
    if (!server_command)
      error ("This command cannot be invoked directly.");
    assembly_level_debugging = !assembly_level_debugging;
    frame_command (0, 0);
}

void
_initialize_printcmd ()
{
  current_display_number = -1;

  add_info ("address", address_info,
	    "Describe where symbol SYM is stored.\n\nUsage:\n\tinfo ADDR <SYM>\n");

  add_info ("symbol", sym_info,
	    "Describe what symbol is at location ADDR.\n\n\
Usage:\n\tinfo symbol <ADDR>\n\n\
For symbols that are there in global, static or local scope and not in\
 thread-private scope.");

  add_info ("module", sym_module,
	    "If ADDR is in the text or data of a module, identify the module\n\n"
	    "Usage:\n\tinfo module <ADDR>\n"
	   );

#ifdef HP_IA64
  add_info ("rtti", rtti_info,
            "Print RTTI of C++ polymorphic object.\n\n\
Usage:\n\tinfo rtti <ADDR>\n\n\
The input should be address for C++ polymorphic object.");
#endif

#ifdef HP_IA64
  add_com ("x", class_vars, x_command,
	   concat ("Examine the memory address.\n\n\
Usage:\n\tx[/FMT] <ADDR>\n\n\
ADDR is an expression for the memory address to examine.\n\
FMT is a repeat count followed by a format letter and a size letter.\n\
Format letters are o(octal), x(hex), d(decimal), u(unsigned decimal),\n\
t(binary), f(float), db(double), l(long double), a(address), i(instruction),\n\
c(char), s(string), df(decimal float), dd(decimal double) and\n\
dl(decimal long double).\n",
		   "Size letters are b(byte), h(halfword), w(word), g(giant, 8 bytes).\n\
The specified number of objects of the specified size are printed\n\
according to the format.\n\n\
Defaults for format and size letters are those previously used.\n\
Default count is 1.  Default address is following last thing printed\n\
with this command or \"print\".", NULL));
#else
  add_com ("x", class_vars, x_command,
	   concat ("Examine the memory address.\n\n\
Usage:\n\tx[/FMT] <ADDR>\n\n\
ADDR is an expression for the memory address to examine.\n\
FMT is a repeat count followed by a format letter and a size letter.\n\
Format letters are o(octal), x(hex), d(decimal), u(unsigned decimal),\n\
t(binary), f(float), db(double), l(long double), a(address), i(instruction),\n\
c(char) and s(string).\n",
		   "Size letters are b(byte), h(halfword), w(word), g(giant, 8 bytes).\n\
The specified number of objects of the specified size are printed\n\
according to the format.\n\n\
Defaults for format and size letters are those previously used.\n\
Default count is 1.  Default address is following last thing printed\n\
with this command or \"print\".", NULL));
#endif

  add_com ("disassemble", class_vars, disassemble_command,
	   "Disassemble a specified section of memory.\n\n\
Usage:\n\tdisassemble [<ADDR1>] [<ADDR2>]\n\t or\n\t\
disassemble [<FUNC>]\n\n\
Default is the function surrounding the pc of the selected frame.\n\
With a single argument, the function surrounding that address is dumped.\n\
Two arguments are taken as a range of memory to dump.");
  if (xdb_commands)
    add_com_alias ("va", "disassemble", class_xdb, 0);

  add_info ("display", display_info,
	    "Expressions to display when program stops, with code numbers.\n\n\
Usage:\n\tinfo display\n");

  add_cmd ("undisplay", class_vars, undisplay_command,
	   "Cancel some expressions to be displayed when program stops.\n\n\
Usage:\n\tundisplay [<N1>] [<N2>] [...]\n\n\
Arguments are the code numbers of the expressions to stop displaying.\n\
No argument means cancel all automatic-display expressions.\n\
\"delete display\" has the same effect as this command.\n\
Do \"info display\" to see current list of code numbers.",
	   &cmdlist);

  add_com ("display", class_vars, display_command,
	   "Print value of expression EXP each time the program stops.\n\n\
Usage:\n\tdisplay[/<FMT>] <EXP> | <ADDR>\n\n\
/FMT may be used before EXP as in the \"print\" command.\n\
/FMT \"i\" or \"s\" or including a size-letter is allowed,\n\
as in the \"x\" command, and then EXP is used to get the address to examine\n\
and examining is done as in the \"x\" command.\n\n\
With no argument, display all currently requested auto-display expressions.\n\
Use \"undisplay\" to cancel display requests previously made."
    );

  add_cmd ("display", class_vars, enable_display,
	   "Enable some expressions to be displayed when program stops.\n\n\
Usage:\n\tenable display [<N1>] [<N2>] [...]\n\n\
Arguments are the code numbers of the expressions to resume displaying.\n\
No argument means enable all automatic-display expressions.\n\
Do \"info display\" to see current list of code numbers.", &enablelist);

  add_cmd ("display", class_vars, disable_display_command,
	   "Disable some expressions to be displayed when program stops.\n\n\
Usage:\n\tdisable display [<N1>] [<N2>] [...]\n\n\
Arguments are the code numbers of the expressions to stop displaying.\n\
No argument means disable all automatic-display expressions.\n\
Do \"info display\" to see current list of code numbers.", &disablelist);

  add_cmd ("display", class_vars, undisplay_command,
	   "Cancel some expressions to be displayed when program stops.\n\n\
Usage:\n\tdelete display [<N1>] [<N2>] [...]\n\n\
Arguments are the code numbers of the expressions to stop displaying.\n\
No argument means cancel all automatic-display expressions.\n\
Do \"info display\" to see current list of code numbers.", &deletelist);

  add_com ("printf", class_vars, printf_command,
	   "This is useful for formatted output in user-defined commands.\n\n\
Usage:\n\tprintf \"<PRINT FORMAT STRING>\", [<ARG1>][, <ARG2>][, ...]\n\n");

  add_com ("output", class_vars, output_command,
	   "Like \"print\" but don't put in value history and don't print newline.\n\n\
Usage:\n\tSimilar to 'print' command.\n\n\
This is useful in user-defined commands.");

 /* JAGae36708 - array browsing  New command dump2file */
  add_com ("dump2file", class_vars, dump2file_command,
	   "To dump the value of an array into a file in a matrix form.\n\n\
Usage:\n\tdump2file <ARRAY> <FILE>\n\n");


  add_prefix_cmd ("set", class_vars, set_command,
		  concat ("Evaluate expression EXP and assign result to variable\
 VAR, using assignment\n\n\
Usage:\n\tset <VAR> [:= | =] [EXP]\n\n\
syntax appropriate for the current language (VAR = EXP or VAR := EXP for\n\
example).  VAR may be a debugger \"convenience\" variable (names starting\n\
with $), a register (a few standard names starting with $), or an actual\n\
variable in the program being debugged.  EXP is any valid expression.\n",
			  "Use \"set variable\" for variables with names identical to set subcommands.\n\
\nWith a subcommand, this command modifies parts of the gdb environment.\n\
You can see these environment settings with the \"show\" command.", NULL),
		  &setlist, "set ", 1, &cmdlist);
  if (dbx_commands)
    add_com ("assign", class_vars, set_command, concat ("Evaluate expression \
EXP and assign result to variable VAR, using assignment\n\
syntax appropriate for the current language (VAR = EXP or VAR := EXP for\n\
example).  VAR may be a debugger \"convenience\" variable (names starting\n\
with $), a register (a few standard names starting with $), or an actual\n\
variable in the program being debugged.  EXP is any valid expression.\n",
							"Use \"set variable\" for variables with names identical to set subcommands.\n\
\nWith a subcommand, this command modifies parts of the gdb environment.\n\
You can see these environment settings with the \"show\" command.", NULL));

  /* "call" is the same as "set", but handy for dbx users to call fns. */
  add_com ("call", class_vars, call_command,
	   "Call a function in the program.\n\n\
Usage:\n\tcall <FUNC> ([<ARG1>][, <ARG2>][, ...])\n\n\
The argument is the function name and arguments, in the notation of the\n\
current working language.  The result is printed and saved in the value\n\
history, if it is not void.");

  add_cmd ("variable", class_vars, set_command,
	   "Evaluate expression EXP and assign result to variable VAR,\n\
 using assignment syntax appropriate for the current language \n\
(VAR = EXP or VAR := EXP for example).\n\n\
Usage:\n\tset <VAR> [:= | =] [EXP]\n\n\
VAR may be a debugger \"convenience\" variable (names starting\n\
with $), a register (a few standard names starting with $), or an actual\n\
variable in the program being debugged.  EXP is any valid expression.\n\
This may usually be abbreviated to simply \"set\".",
	   &setlist);

  add_com ("print", class_vars, print_command,
	   concat ("Print value of expression EXP.\n\n\
Usage:\n\tprint[/<FMT>] [<EXP>]\n\n\
Variables accessible are those of the lexical environment of the selected\n\
stack frame, plus all those whose scope is global or an entire file.\n\
\n\
$NUM gets previous value number NUM.  $ and $$ are the last two values.\n\
$$NUM refers to NUM'th value back from the last one.\n\
Names starting with $ refer to registers (with the values they would have\n",
		   "if the program were to return to the stack frame now selected, restoring\n\
all registers saved by frames farther in) or else to debugger\n\
\"convenience\" variables (any such name not a known register).\n\
Use assignment expressions to give values to convenience variables.\n",
		   "\n\
{TYPE}ADREXP refers to a datum of data type TYPE, located at address ADREXP.\n\
@ is a binary operator for treating consecutive data objects\n\
anywhere in memory as an array.  FOO@NUM gives an array whose first\n\
element is FOO, whose second element is stored in the space following\n\
where FOO is stored, etc.  FOO must be an expression whose value\n\
resides in memory.\n",
		   "\n\
EXP may be preceded with /FMT, where FMT is a format letter\n\
but no count or size letter (see \"x\" command).", NULL));
  add_com_alias ("p", "print", class_vars, 1);

  add_com ("out_no_values", class_vars, output_command_no_values,
	   concat ("Output value of expression EXP.\n\n\
Usage:\n\tout_no_values[/<FMT>] <EXP>\n\n\
Same as \"output\" except it does not output values of aggregates\n\
arrays, structs, classes, unions.", NULL));

  add_com ("inspect", class_vars, inspect_command,
	   "Same as \"print\" command, except that if you are running in the epoch\n\
environment, the value is printed in its own window.\n\n\
Usage:\n\tSimilar to 'print' command.\n\n");

  add_show_from_set (
		 add_set_cmd ("max-symbolic-offset", no_class, var_uinteger,
			      (char *) &max_symbolic_offset,
       "Set the largest offset that will be printed in <symbol+1234> form.",
			      &setprintlist),
		      &showprintlist);

  /* Define the xdb style toggle display (td) command for Firebolt.
     This command is for internal use only.
  */
  if (firebolt_version)
    add_com ("td", class_vars, toggle_display_command,
	   "For internal use of Firebolt only.");

  add_show_from_set (
		      add_set_cmd ("symbol-filename", no_class, var_boolean,
				   (char *) &print_symbol_filename,
	   "Set printing of source filename and line number with <symbol>.",
				   &setprintlist),
		      &showprintlist);

  /* For examine/instruction a single byte quantity is specified as
     the data.  This avoids problems with value_at_lazy() requiring a
     valid data type (and rejecting VOID). */
  examine_i_type = init_type (TYPE_CODE_INT, 1, 0, "examine_i_type", NULL);

  examine_b_type = init_type (TYPE_CODE_INT, 1, 0, "examine_b_type", NULL);
  examine_h_type = init_type (TYPE_CODE_INT, 2, 0, "examine_h_type", NULL);
  examine_w_type = init_type (TYPE_CODE_INT, 4, 0, "examine_w_type", NULL);
  examine_g_type = init_type (TYPE_CODE_INT, 8, 0, "examine_g_type", NULL);

}
