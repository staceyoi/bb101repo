#! /opt/gnu/bin/python
import Readline
import sys
import string

class CmdManager:
    "Class for retrieving and dispatching commands."
    def __init__ (self, cmdset, module_name):
	self.cmdset = cmdset

	# Set this command set as the current set (to help the completion
	# function (called by readline) complete on the correct command set.
	cmdset.set_current ()

	# Set Command.cmd_module to contain the current command module namespace
	Command.cmd_module = __import__(module_name)

	Readline.parse_and_bind("tab: complete")
	Readline.completion_entry_function (cmd_completer)

    def set_cmdset (self, cmdset, module_name):
	"Set the command set that the Cmd class is to use for dispatching commands."
	self.cmdset = cmdset
	cmdset.set_current () # See comment above
	Command.cmd_module = __import__(module_name) # See comment above

    def get_input (self, prompt):
	"Post a prompt and get a line of user input."
	line = Readline.readline (prompt)
	if (line == None):
	    line = ""
	return string.strip (line)

    def dispatch (self, cmdstr):
	"Dispatch the command in cmdstr."
	self.cmdset.run (cmdstr)

class Command:
    "Class describing a command, including how to invoke the command."
    cmd_module = None  # The command implementation module currently in use
    def __init__ (self, name, func_name, doc, abbrev_flag):
        self.name = name
        self.func_name = func_name
        self.doc = doc
	self.abbrev_flag = abbrev_flag
    def run (self, args):
        "Invokes a command by evaluating the associated function name."
	# "self.cmd_module" contains the current command module namespace
	cmd = "self.cmd_module." + self.func_name + " (\"" + args + "\")"
	try:
	    exec (cmd)
	except AttributeError:
	    name = string.replace (self.name, " ", "_")
	    print "Internal Error:", name, "command not implemented."

class CmdSet:
    "Class describing an entire command set."
    current = None # The CmdSet instance currently in use.
    def __init__ (self, cmds, prefix=""):
	self.cmds = cmds
	self.list = cmds.keys ()
	self.completions = []
	self.prefix = prefix

    def set_current (self):
	"Sets the 'current' command set (to give access to non-member completer)"
	CmdSet.current = self

    def lookup (self, cmd, return_abbrev=""):
	"Looks up command. Returns list of matches (1 match if match is exact).\nOptionally, returns command abbreviations."
	ret_abbrev = 0
	if (return_abbrev == "return_abbrev"):
	    ret_abbrev = 1

	matches = []
	length = len (cmd)
	for cmdname in self.list:
	    if (cmd == cmdname[:len(cmd)]):
		if ((not ret_abbrev) and self.cmds[cmdname].abbrev_flag):
                    continue
		matches.append (cmdname)
		if (len (cmd) == len (cmdname)):
		    return [cmdname]
	return matches
    
    def completer (self, text, matches):
	"Command completion function."
	if (matches <= 1):
	    self.completions = []
	elif (len (self.completions)):
	    cmpl = self.completions[0]
	    del self.completions[0]
	    return cmpl
	else:
	    return None

	if (text == None or text == ""):
	    return ""
    
	self.completions = self.lookup (text)
	if (len (self.completions) <= 0):
	    return None
	cmpl = self.completions[0]
	del self.completions[0]
	return cmpl

    def report_undefined_cmd (self, cmd):
	if (len (self.prefix)):
	    print "Undefined", self.prefix, "command: \"" + cmd + "\". Try \"help",  self.prefix + "\"."
	else:
	    print "Undefined command: \"" + cmd + "\". Try \"help\"."

    def report_ambiguous_cmd (self, cmd, cmd_matches):
	if (len (self.prefix)):
	    err_msg = "Ambiguous " + self.prefix + " command \""
	else:
	    err_msg = "Ambiguous command \""
	err_msg = err_msg + cmd + "\": "
	for match in cmd_matches:
	    err_msg = err_msg + " " + match + ","
	print err_msg[:-1] + "."

    def run (self, cmdstr):
	"Execute the command string cmdstr."
	cmd = string.split (cmdstr)
	cmd = cmd[0]
	args = string.lstrip (cmdstr[len(cmd):])

	cmd_matches = self.lookup (cmd, "return_abbrev")
	num_matches = len (cmd_matches)
	if (num_matches <= 0):
	    self.report_undefined_cmd (cmd)
	elif (num_matches > 1):
	    self.report_ambiguous_cmd (cmd, cmd_matches)
	else:
	    if (self.cmds.has_key (cmd_matches[0])):
		try:
		    self.cmds[cmd_matches[0]].run (args)
		except NameError:
		    cmdname = ""
		    if (len (self.prefix)):
			cmdname = self.prefix + " " + cmd_matches[0]
		    else:
			cmdname = cmd_matches[0]
		    print "Internal Error: \"" + cmdname + "\" command not implemented."
	    else:
		print "Internal Error: Can't run command", cmd_matches[0]

    def get_doc (self, topic):
	"Get documentation string for command cmd. Validate cmd before calling get_doc()."
	cmd_matches = self.lookup (topic, "return_abbrev")
	num_matches = len (cmd_matches)
	if (num_matches <= 0):
	    self.report_undefined_cmd (topic)
	    return ""
	elif (num_matches == 1):
	    if (self.cmds.has_key (cmd_matches[0])):
		return self.cmds[cmd_matches[0]].doc
	    else:
		print "Internal Error: get_doc(): Unknown help topic: \"" + topic + "\""
		return ""
	else:
	    self.report_ambiguous_cmd (topic, cmd_matches)

def cmd_completer (text, matches):
    "Non-member function that can be passed to readline for command completion."
    if (CmdSet.current != None):
	return CmdSet.current.completer (text, matches)
    else:
	print "Internal Error: No current command set."
	return ""
