#! /opt/gnu/bin/python
import sys
import string
import CmdManager
import GdbCmds
import Hpd

initialized = 0
gdb_cmds = None
info_cmds = None

def initialize ():
    "Initialize gdb command sets (info commands, set commands, etc.)"
    global initialized
    global gdb_cmds
    global info_cmds
    
    if (initialized):
	return None
    initialized = 1
    gdb_cmds = Hpd.get_gdb_cmds ()
    info_cmds = CmdManager.CmdSet (GdbCmds.info_cmds, "info")

def do_break (arg):
    print "Setting breakpoint"

def do_quit (arg):
    sys.exit (0)

def do_help (arg):
    initialize ()
    doc = ""
    prefix = " "
    topic = ""
    cmdset = None

    if (arg == None or arg == ""):
	print "PRINT LIST OF HELP CLASSES"
	return None
    arglist = string.split (arg)
    if (len (arglist) >= 2):
	if (arglist[0] == "info"):
	    global info_cmds
	    prefix = "info"
	    cmdset = info_cmds
	    topic = arglist[1]
	else:
	    global gdb_cmds
	    cmdset = gdb_cmds
	    topic = arglist[0]
    else:
	global gdb_cmds
	cmdset = gdb_cmds
	topic = arglist[0]
    docstr = cmdset.get_doc (topic)
    if (len (docstr)):
	print docstr

def do_info (arg):
    "Run info subcommand."
    global info_cmds

    initialize ()
    
    if (arg == "" or arg == None):
	print "LIST INFO CMD ARGS"
	return None
    info_cmds.run (arg)

def do_info_breakpoints (args):
    print "LISTING BREAKPOINTS"

# Command abbreviations

def do_b (arg):
    do_break (arg)
def do_br (arg):
    do_break (arg)
def do_bre (arg):
    do_break (arg)
def do_brea (arg):
    do_break (arg)

def do_q (arg):
    do_quit (arg)

