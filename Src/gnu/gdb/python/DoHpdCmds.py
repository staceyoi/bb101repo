#! /opt/gnu/bin/python
import sys
import string
import CmdManager
import HpdCmds
import Hpd

def do_hpd_break (arg):
    print "Setting breakpoint"

def do_hpd_step (arg):
    print "stepping"

def do_hpd_quit (arg):
    sys.exit (0)

def do_hpd_help (arg):
    if (arg == None or arg == ""):
	print "PRINT LIST OF HELP TOPICS"
	return None
    hpd_cmds = Hpd.get_hpd_cmds ()
    arglist = string.split (arg)
    docstr = hpd_cmds.get_doc (arglist[0])
    if (len (docstr)):
	print docstr

