#! /opt/gnu/bin/python
import Readline
import sys
import string
sys.path.append ('/CLO/Components/WDB/Src/gnu/gdb/python')
import GdbCmds
import HpdCmds
import CmdManager

gdb_cmdset = None
hpd_cmdset = None
hpd_cmd_module = "DoHpdCmds"
gdb_cmd_module = "DoGdbCmds"
mode_switch = 0

def get_gdb_cmds ():
    "Returns dictionary of the gdb command set. (Call to avoid multiple instantiations.)"
    global gdb_cmdset
    if (gdb_cmdset == None):
	gdb_cmdset = CmdManager.CmdSet (GdbCmds.gdb_cmds)
    return gdb_cmdset

def get_hpd_cmds ():
    "Returns dictionary of the hpd command set. (Call to avoid multiple instantiations.)"
    global hpd_cmdset
    if (hpd_cmdset == None):
	hpd_cmdset = CmdManager.CmdSet (HpdCmds.hpd_cmds)
    return hpd_cmdset

def set_hpd_cmd_module (mod_name):
    "Allows override of default hpd command module (for testing)."
    global hpd_cmd_module
    hpd_cmd_module = mod_name

def set_gdb_cmd_module (mod_name):
    "Allows override of default gdb command module (for testing)."
    global gdb_cmd_module
    gdb_cmd_module = mod_name

def mode_switch_callback ():
    global mode_switch
    mode_switch = 1

def cmd_loop ():
    "Retrieve commands from user and dispatch them."
    global mode_switch
    gdb_mode = "gdb-mode"
    hpd_mode = "hpd-mode"
    gdb_cmds = get_gdb_cmds ()
    hpd_cmds = get_hpd_cmds ()
    cmd_line = CmdManager.CmdManager (hpd_cmds, hpd_cmd_module)
    prompt = "(hpd) "
    current_mode = hpd_mode

    # Do mode switch with key 24 (CTRL-X)
    #Readline.add_defun ("mode-switch", mode_switch_callback, 24); 

    while 1:
	line = cmd_line.get_input (prompt)

	if (mode_switch):
	    print "mode switch!"
	    mode_switch = 0
	    if (current_mode == hpd_mode):
		current_mode = gdb_mode
	    else:
		current_mode = hpd_mode
	    line = current_mode
	    continue

	if (line == gdb_mode):
	    prompt = "(gdb) "
	    cmd_line.set_cmdset (gdb_cmds, gdb_cmd_module)
	    continue
	elif (line == hpd_mode):
	    prompt = "(hpd) "
	    cmd_line.set_cmdset (hpd_cmds, hpd_cmd_module)
	    continue
	if (line == None or line == ""):
	    continue
	cmd_line.dispatch (line)
