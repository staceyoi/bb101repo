from CmdManager import *

######################################
# Command Strings

syntax = "Syntax: "

actions_doc = \
    "Display a list of actionpoints.\n" \
    + syntax + \
    "actions [<actionpoint-list> | -enabled | -disabled | -break | -watch | -barrier | -all]"
alias_doc = \
    "View or create a new user-defined commands(s).\n" \
    + syntax + \
    "alias [<command-name>]"
assign_doc = "Change the value of a scalar program variable.\n" \
    + syntax + \
    "assign <command-name> <command-body>"
attach_doc = \
    "Bring currently exectuting process(es) under control of the debugger.\n" \
    + syntax + \
    "attach <exe> <pid[, pid, ...]>"
barrier_doc = \
    "Define a barrier point.\n" \
    + syntax + \
    "barrier <source-loc> [-stop <stop-set>]"
break_doc = \
    "Define a breakpoint.\n" \
    + syntax + \
    "break {<source_loc>} [-count <n> | if <condition>] [-stop <stop-set>]"
cont_doc = \
    "Resume execution of target process(es), blocking command input\nuntil target process(es) stop.\n" \
    + syntax + \
    "cont [-waitany]"
core_doc = \
    "Load core-file image of process(es) for examination.\n" \
    + syntax + \
    "core <exe> <pid>[,<pid>,...]"
defset_doc = \
    "Assign a set name to a group of processes/threads.\n" \
    + syntax + \
    "defset <set-name> <p/t-set>"
delete_doc = \
    "Remove actionpoint(s).\n" \
    + syntax + \
    "delete <actions-list> | -enabled | -disabled | -break | -watch | -barrier | -all"
detach_doc = \
    "Detach debugger from target process(es), leaving it (or them) executing.\n" \
    + syntax + \
    "attach"
disable_doc = \
    "Temorarily disable actionpoint(s).\n" \
    + syntax + \
    "disable <actions-list> | -break | -watch | -barrier | -all"
down_doc = \
    "Move down one or more levels in the call stack.\n" \
    + syntax + \
    "down [<num-levels>]"
enable_doc = \
    "Re-enable actionpoints(s).\n" \
    + syntax + \
    "enable <actions-list> | -break | -watch | -barrier | -all"
exit_doc = \
    "Terminate the debugging session (same as quit).\n" \
    + syntax + \
    "exit"
export_doc = \
    "Save commands to re-establish actionpoint(s).\n" \
    + syntax + \
    "export <file> {<actionpoint-list> | -enabled | -disabled | -break | -watch | -barrier | -all}"
focus_doc = \
    "Change the current process/thread set.\n" \
    + syntax + \
    "focus [<p/t-set>]"
go_doc = \
    "Resume execution of target process(es).\n" \
    + syntax + \
    "go"
halt_doc = \
    "Suspend execution of target process(es).\n" \
    + syntax + \
    "halt"
help_doc = \
    "Display help information.\n" \
    + syntax + \
    "help [<topic>]"
history_doc = \
    "Display command history list.\n" \
    + syntax + \
    "history [<num-commands>]"
info_doc = \
    "Display debugger environment information.\n" \
    + syntax + \
    "info"
input_doc = \
    "Read and execute debugger commands stored in a file.\n" \
    + syntax + \
    "input <file>"
kill_doc = \
    "Terminate execution of target process(es).\n" \
    + syntax + \
    "kill"
list_doc = \
    "Display code relative to the start of some file or procedure,\nor to some specific code line.\n" \
    + syntax + \
    "list [<source-loc> | $EXEC] [-length [-]<num-lines>]"
load_doc = \
    "Load debugging information about target program and prepare for execution.\n" \
    + syntax + \
    "load <exe> [-copies <ncopies>]"
log_doc = \
    "Start or stop debugger logging.\n" \
    + syntax + \
    "[ {<file> [-quiet] [-output | -input]} | {[<file>] -off} ]"
print_doc = \
    "Evaluate and display the value of a program variable or expression.\n" \
    + syntax + \
    "print <expr> [-name] [-index] [-format <format-spec>]"
proginput_doc = \
    "Send command-line input to the target program, rather than the debugger.\n" \
    + syntax + \
    "proginput"
quit_doc = \
    "Terminate the debugging session (same as exit).\n" \
    + syntax + \
    "quit"
run_doc = \
    "Start or re-start exection of target process(es).\n" \
    + syntax + \
    "run [<prog_args>] [< <file>] [> <outfile>]"
set_doc = \
    "View or change debugger state variables.\n" \
    + syntax + \
    "set <debbugger-variable>"
status_doc = \
    "Show current status of processes or threads.\n" \
    + syntax + \
    "status [-mpi]"
step_doc = \
    "Execute one or more statements by a specific process/thread.\n" \
    + syntax + \
    "step [<repeat-count> [-over]] | -finish"
unalias_doc = \
    "Remove previously defined command.\n" \
    + syntax + \
    "unalias <command-name> | -all"
undefset_doc = \
    "Undefine a previously defined process/thread set.\n" \
    + syntax + \
    "undefset <set-name> | -all"
unset_doc = \
    "Restore default setting for a debugger state variable.\n" \
    + syntax + \
    "step [<repeat-count> [-over]] | -finish"
up_doc = \
    "Move up one or more levels in the call stack.\n" \
    + syntax + \
    "up [<num-levels>]"
viewset_doc = \
    "List the members of a process/thread set.\n" \
    + syntax + \
    "viewset [<set-name>]"
what_doc = \
    "Determine what a target program name refers to.\n" \
    + syntax + \
    "what <symbol-name> | -all"
wait_doc = \
    "Block command input until target process(es) stop.\n" \
    + syntax + \
    "wait [-waitany]"
watch_doc = \
    "Define an unconditional watchpoint.\n" \
    + syntax + \
    "watch <variable> [-length <byte-count>] [-stop {step-set | \"[]\"}]"
where_doc = \
    "Display the current execution location and call stack.\n" \
    + syntax + \
    "where [<num-levels> | -all] [-args] [-u] [-g]"
whichsets_doc = \
    "List all sets to which a prcess/thread belongs.\n" \
    + syntax + \
    "whichsets [<p/t-set>]"

######################################
# Command Dictionary

hpd_cmds = {
    'actions': Command ("actions", "do_hpd_actions", actions_doc, 0),
    'alias': Command ("alias", "do_hpd_alias", alias_doc, 0),
    'assign': Command ("assign", "do_hpd_assign", assign_doc, 0),
    'attach': Command ("attach", "do_hpd_attach", attach_doc, 0),
    'barrier': Command ("barrier", "do_hpd_barrier", barrier_doc, 0),
    'break': Command ("break", "do_hpd_break", break_doc, 0),
    'cont': Command ("cont", "do_hpd_cont", cont_doc, 0),
    'core': Command ("core", "do_hpd_core", core_doc, 0),
    'defset': Command ("defset", "do_hpd_defset", defset_doc, 0),
    'delete': Command ("delete", "do_hpd_delete", delete_doc, 0),
    'detach': Command ("detach", "do_hpd_detach", detach_doc, 0),
    'disable': Command ("disable", "do_hpd_disable", disable_doc, 0),
    'down': Command ("down", "do_hpd_down", down_doc, 0),
    'enable': Command ("enable", "do_hpd_enable", enable_doc, 0),
    'exit': Command ("exit", "do_hpd_exit", exit_doc, 0),
    'export': Command ("export", "do_hpd_export", export_doc, 0),
    'focus': Command ("focus", "do_hpd_focus", focus_doc, 0),
    'go': Command ("go", "do_hpd_go", go_doc, 0),
    'halt': Command ("halt", "do_hpd_halt", halt_doc, 0),
    'help': Command ("help", "do_hpd_help", help_doc, 0),
    'history': Command ("history", "do_hpd_history", history_doc, 0),
    'info': Command ("info", "do_hpd_info", info_doc, 0),
    'input': Command ("input", "do_hpd_input", input_doc, 0),
    'kill': Command ("kill", "do_hpd_kill", kill_doc, 0),
    'list': Command ("list", "do_hpd_list", list_doc, 0),
    'load': Command ("load", "do_hpd_load", load_doc, 0),
    'log': Command ("log", "do_hpd_log", log_doc, 0),
    'print': Command ("print", "do_hpd_print", print_doc, 0),
    'proginput': Command ("proginput", "do_hpd_proginput", proginput_doc, 0),
    'quit': Command ("quit", "do_hpd_quit", quit_doc, 0),
    'run': Command ("run", "do_hpd_run", run_doc, 0),
    'set': Command ("set", "do_hpd_set", set_doc, 0),
    'status': Command ("status", "do_hpd_status", status_doc, 0),
    'step': Command ("step", "do_hpd_step", step_doc, 0),
    'unalias': Command ("unalias", "do_hpd_unalias", unalias_doc, 0),
    'undefset': Command ("undefset", "do_hpd_undefset", undefset_doc, 0),
    'unset': Command ("unset", "do_hpd_unset", unset_doc, 0),
    'up': Command ("up", "do_hpd_up", up_doc, 0),
    'viewset': Command ("viewset", "do_hpd_viewset", viewset_doc, 0),
    'what': Command ("what", "do_hpd_what", what_doc, 0),
    'wait': Command ("wait", "do_hpd_wait", wait_doc, 0),
    'watch': Command ("watch", "do_hpd_watch", watch_doc, 0),
    'where': Command ("where", "do_hpd_where", where_doc, 0),
    'whichsets': Command ("whichsets", "do_hpd_whichsets", whichsets_doc, 0)
}

