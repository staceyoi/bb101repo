#include <stdio.h>
#include <varargs.h>
#include "defs.h"
#include "top.h"
#include "target.h"
#include "gdbcmd.h"
#include "frame.h"
#include "symtab.h"
#include "demangle.h"
#include "inferior.h"
#include "readline.h"
#include "chardefs.h"  /* For CTRL definition */
#include "Python.h"

#define FREE(x) if (x) free (x)

static PyObject *ErrorObject;
#define returnOnError(msg) { PyErr_SetString (ErrorObject, msg); return NULL; }

#define returnOnGdbError(msg) { if (msg) { PyErr_SetString (ErrorObject, \
                                           mark_as_gdb_error (msg)); \
                                           return NULL; } }

#define DEBUG_OBJECT_LIFE

/********************************************
 * GDB I/O
 */
static FILE * devnull = NULL;
static GDB_FILE * save_gdb_stdout = NULL;

void
disable_gdb_stdout ()
{
  if (devnull != NULL)
    devnull = fopen ("/dev/null", "w");
  save_gdb_stdout = gdb_stdout;
  gdb_stdout = NULL;
}

enable_gdb_stdout ()
{
  gdb_stdout = save_gdb_stdout;
}

/*******************************************
 * Utilities
 */

/* Prepend "GDB ERROR" to passed in message to
   help track when gdb code is generating errors. */

static char *
mark_as_gdb_error (char *msg)
{
#define MAX_MSG_LEN 256
  static char buf[MAX_MSG_LEN];
  char * format = "GDB ERROR: %s";

  /* Truncate if necessary. */
  if ((strlen (format) + strlen (msg) + 1) > MAX_MSG_LEN)
    msg [MAX_MSG_LEN - (strlen (format) + 1)] = '\0';
  sprintf (buf, "GDB ERROR: %s", msg);
  return buf;
}

/* The following function is a modified version of print_command_line() from top.c
   Returns a string representing command and any of its control structures. 
   Returned string must be freed by caller.

   In the future, print_command_line() should be modified to take a GDB_FILE argument
   so that we can use print_command_line() instead of the function below. */

/* Recursively print a command (including full control structures).  */
static char *
get_command_line (struct command_line *cmd, unsigned int depth)
{
  unsigned int i;
  char * indent;
  char * spaces = "  ";
  char * cmdstr = NULL;

  indent = xmalloc (strlen (spaces) * depth + 1);
  indent[0] = '\0';
  for (i = 0; i < depth; i++)
    strcat (indent, spaces);

  /* A simple command. */
  if (cmd->control_type == simple_control)
    cmdstr = cmd->line;
  /* loop_continue to jump to the start of a while loop. */
  else if (cmd->control_type == continue_control)
    cmdstr = "loop_continue";
  /* loop_break to break out of a while loop. */
  else if (cmd->control_type == break_control)
    cmdstr = "loop_break";

  if (cmdstr)
    {
      char * str;

      str = xmalloc (strlen (cmdstr) + strlen (indent) + 2);
      sprintf (str, "%s%s\n", indent, cmdstr);
      free (indent);
      return str;
    }

  /* A while command.  Recursively write its subcommands before returning.  */
  if (cmd->control_type == while_control)
    {
      struct command_line *list;
      char * format = "%swhile %s\n";

      /* Alloc all but body: <indent>while <cmd>\n<body> */
      cmdstr = xmalloc (strlen (indent) + strlen (format) + strlen (cmd->line) + 1);
      sprintf (cmdstr, format, indent, cmd->line);

      list = *cmd->body_list;
      while (list)
	{
	  char * nextcmd;

	  nextcmd = get_command_line (list, depth + 1);
	  cmdstr = xrealloc (cmdstr, strlen (cmdstr) + strlen (nextcmd) + 1);
	  strcpy (cmdstr, nextcmd);
	  free (nextcmd);
	  list = list->next;
	}
    }

  /* An if command.  Recursively write both arms before returning.  */
  if (cmd->control_type == if_control)
    {
      char * true_arm;
      char * format = "%sif %s\n%s";
      char * else_str = "else\n";
      char * end_stmt = "end\n";

      /* First get the true arm. */
      true_arm = get_command_line (cmd->body_list[0], depth + 1);

      /* Alloc all but arms and 'else': <indent>if <cmd>\n<true_arm>[else\n<false_arm>]<indent>end\n */
      cmdstr = xmalloc (strlen (indent) * 2 + strlen (format) + strlen (cmd->line)
			+ strlen (true_arm) + strlen (end_stmt) + 1);
      sprintf (cmdstr, format, indent, cmd->line, true_arm);
      free (true_arm);

      /* Show the false arm if it exists.  */
      if (cmd->body_count == 2)
	  {
	    char * else_stmt = "else\n";
	    char * false_arm;

	    false_arm = get_command_line (cmd->body_list[1], depth + 1);
	    cmdstr = xrealloc (cmdstr, strlen (cmdstr) + strlen (else_stmt) + strlen (false_arm) + 1);
	    strcpy (cmdstr, indent);
	    strcpy (cmdstr, else_stmt);
	    strcpy (cmdstr, false_arm);
	    free (false_arm);
	  }
      strcat (cmdstr, end_stmt);
    }
}

/*******************************************
 * Readline stuff (for use when gdb is run with
 * python interpreter).
 */

/* Allow ^X (on empty line) as equivalent of "switch-python-mode" command */
int
mode_switch_callback ()
{
  if (rl_end)
    return ding ();
  else
    {
      rl_insert_text ("switch-python-mode");
      rl_end_of_line ();
      return rl_newline ();
    }
}

void
add_rl_mode_switch_key ()
{
  rl_add_defun ("switch-python-mode", &mode_switch_callback, CTRL ('X'));
}

/*******************************************
 * Routines to wrap calls into gdb and for
 * handling gdb errors.
 */

static char * gdb_error_msg = "";

static void
set_gdb_error_msg (char *msg)
{
  gdb_error_msg = msg;
}

static void
clear_gdb_error_msg ()
{
  gdb_error_msg = NULL;
}

static char *
get_gdb_error_msg ()
{
  char * msg = gdb_error_msg;
  gdb_error_msg = NULL;
  return msg;
}

/* We set gdb's error_hook to this function to catch error messages. */
static void 
catch_gdb_error_msg (char *msg)
{
  set_gdb_error_msg (msg);
  (NORETURN void) _longjmp (error_return, 1);
}

/* Call a gdb routine and catch error returns.

   IN func     gdb routine to call.
   IN err_msg  if called function results in error, err_msg will
               point to resulting error message; else, NULL.
   IN argcount number of arguments to the gdb routine.
   IN va_alist list of arguments. */

static int
call_catch_errors (func, err_msg, argcount, va_alist)
     void * func;
     char **err_msg;
     int argcount;
     va_dcl
{
#define MAXFUNCARGS 4
  SIGJMP_BUF saved_error;
  SIGJMP_BUF saved_quit;
  SIGJMP_BUF tmp_jmp;
  int val;
  struct cleanup *saved_cleanup_chain;
  va_list ap;
  void *arglist[MAXFUNCARGS];
  int i;

  *err_msg = NULL;

  if (argcount > MAXFUNCARGS)
    {
      *err_msg ="Internal Error: Exceeded max args for call_catch_errors().";
      return -1;
    }
      
  va_start (ap);
  for (i = 0; i < argcount; i++)
    arglist[i] = va_arg (ap, void *);
  va_end (ap);

  saved_cleanup_chain = save_cleanups ();

  clear_gdb_error_msg ();

  memcpy ((char *)saved_error, (char *)error_return, sizeof (SIGJMP_BUF));
  memcpy (saved_quit, quit_return, sizeof (SIGJMP_BUF));

  if (SIGSETJMP (tmp_jmp) == 0)
    {
      memcpy (error_return, tmp_jmp, sizeof (SIGJMP_BUF));
      memcpy (quit_return, tmp_jmp, sizeof (SIGJMP_BUF));
      switch (argcount)
	{
	case 0:
	  val = ((int (*) ())func) ();
	  break;
	case 1:
	  val = ((int (*)(void*)) func) (arglist[0]);
	  break;
	case 2:
	  val = ((int (*)(void*, void*)) func) (arglist[0], arglist[1]);
	  break;
	case 3:
	  val = ((int (*)(void*, void*, void*)) func) (arglist[0],
						       arglist[1], arglist[2]);
	  break;
	case 4:
	  val = ((int (*)(void*, void*, void*, void*)) func) (arglist[0], arglist[1],
							      arglist[2], arglist[3]);
	  break;
	default:
	  *err_msg = "Internal Error: Too many args for call_catch_errors().";
	  return -1;
	}
    }
  else
    val = 0;

  *err_msg = get_gdb_error_msg ();

  restore_cleanups (saved_cleanup_chain);

  memcpy (error_return, saved_error, sizeof (SIGJMP_BUF));
  memcpy (quit_return, saved_quit, sizeof (SIGJMP_BUF));
  return val;
}

/*******************************************
 * Command list accessors.
 */

struct cmd_list_element *
init_cmdlist_iterator (struct cmd_list_element *head)
{
  return head;
}

struct cmd_list_element *
next_cmdlist_element (struct cmd_list_element *elem)
{
  return elem = elem->next;
}

char *cmdlist_name (struct cmd_list_element *elem)
{
  return elem->name;
}

char *cmdlist_doc (struct cmd_list_element *elem)
{
  return elem->doc;
}

int cmdlist_abbrev_flag (struct cmd_list_element *elem)
{
  return elem->abbrev_flag;
}

/********************************************
 * Setting breakpoints, watchpoints, etc.
 */

/* Set breakpoint at location or address.

   IN location  specifier such as <function_name> or <file>:<function_name>.
   IN addr      Address string.
   RETURN       NULL on error, Py_None otherwise.  */

PyObject *
_set_breakpoint (PyObject *ignore, PyObject *args)
{
  char *location;
  char *addr;
  int num_qualifiers = 0;
  char *err_msg;

  if (!PyArg_ParseTuple (args, "ss", &location, &addr))
    return NULL;

  if (location[0] != '\0')
    {
      if (!(addr[0] == '\0'))
	returnOnError ("Internal Error: Specify location or address, but not both.");
      call_catch_errors ((void *) break_command, &err_msg, 2, (void *) location, (void *) 1);
      returnOnGdbError (err_msg);
    }
  else if (addr[0] != '\0')
    {
      char *loc;

      loc = xmalloc (strlen (addr) + 2);
      sprintf (loc, "*%s", addr);
      call_catch_errors ((void *) break_command, &err_msg, 2, (void *) loc, (void *) 1);
      free (loc);
      returnOnGdbError (err_msg);
    }
  Py_INCREF (Py_None);
  return (Py_None);
}

/********************************************
 * Listing breakpoints, watchpoints, etc.
 */

/* Use two python objects: one for breakpoint objects,
   one for a list of those breakpoint objects. Wherever possible,
   use gdb's breakpoint struct directly; otherwise, store
   information in our extensions to gdb's breakpoint struct. */

/* Object to hold break/watch/catch info. */
typedef struct
{
  PyObject_HEAD
  struct breakpoint * bpt; /* gdb breakpoint struct */
  char * cmds;             /* commands associated with breakpoint */
  char * cond_str;         /* condition associated with breakpoint */
  char * description;      /* Varies depending on type. */
  char * disp;             /* "keep", "del", etc. */
  char * frame_addr_str;   /* ?? */
  char * func_name;        /* Function containing break. */
  int    is_catchpoint;    /* true if breakpoint is a catchpoint */
  char * symbolic_address; /* If no debug info, use this instead of func_name. */
  char * type;             /* "breakpoint", "watchpoint", etc. */
} breakobject;

/* Object to hold list of breakpoint/watchpoint/catchpoint objects. */
typedef struct
{
  PyObject_HEAD
  int len;
  breakobject **breakobj;
} breaklist;

staticforward PyTypeObject BreakObjectType;

/* Constructor. */
static breakobject *
new_breakobject ()
{
  breakobject * self;

#ifdef DEBUG_OBJECT_LIFE
  printf ("BREAKPOINT CREATE\n");
#endif

  self = PyObject_NEW (breakobject, &BreakObjectType);
  if (self == NULL)
    return NULL;
  self->bpt = NULL;
  self->cmds = NULL;
  self->cond_str = NULL;
  self->description = NULL;
  self->disp = NULL;
  self->frame_addr_str = NULL;
  self->func_name = NULL;
  self->is_catchpoint = 0;
  self->symbolic_address = NULL;
  self->type = NULL;
  return self;
}

/* Destructor. */
static void
breakobject_destruct (breakobject *self)
{
#ifdef DEBUG_OBJECT_LIFE
  printf ("BREAKPOINT DESTRUCT\n");
#endif
  FREE (self->cmds);
  FREE (self->cond_str);
  FREE (self->description);
  FREE (self->disp);
  FREE (self->frame_addr_str);
  FREE (self->func_name);
  FREE (self->symbolic_address);
  FREE (self->type);
  PyMem_DEL (self);
}

staticforward PyTypeObject BreakListType;

/* Constructor. */
static breaklist *
new_breaklist (int length)
{
  int i;
  breaklist *self;

#ifdef DEBUG_OBJECT_LIFE
  printf ("BREAKLIST CREATE\n");
#endif

  self = PyObject_NEW (breaklist, &BreakListType);
  if (self == NULL)
    return NULL;
  self->len = length;
  self->breakobj = xmalloc (sizeof (breakobject) * self->len);
  for (i = 0; i < self->len; i++)
    self->breakobj[i] = new_breakobject ();
  return self;
}

/* Destructor. */
static void
breaklist_destruct (breaklist *self)
{
  int i;
  
#ifdef DEBUG_OBJECT_LIFE
  printf ("BREAKLIST DESTRUCT\n");
#endif
  for (i = 0; i < self->len; i++)
    Py_DECREF ((PyObject *) self->breakobj[i]);
  free (self->breakobj);
  PyMem_DEL (self);
}

/* This function provides the breakpoint attributes availble
   for python breakpoint objects. */
static PyObject *
breakobject_getattr (breakobject *self, char *name)
{
  /* The first group of attributes are available from gdb's breakpoint struct. */
  if (strcmp (name, "id") == 0)
    return Py_BuildValue ("i", self->bpt->number);
  else if (strcmp (name, "enable") == 0)
    return Py_BuildValue ("i", self->bpt->enable);
  else if (strcmp (name, "line") == 0)
    return Py_BuildValue ("i", self->bpt->line_number);
  else if (strcmp (name, "deferred") == 0)
    {
      int deferred = 0;
      if (!self->bpt->address && self->bpt->enable == shlib_disabled)
	deferred = 1;
      return Py_BuildValue ("i", deferred);
    }
  else if (strcmp (name, "hit_count") == 0)
    return Py_BuildValue ("i", self->bpt->hit_count);
  else if (strcmp (name, "ignore_count") == 0)
    return Py_BuildValue ("i", self->bpt->ignore_count);
  else if (strcmp (name, "thread") == 0)
    return Py_BuildValue ("i", self->bpt->thread);

  /* The following attributes are not available in gdb's breakpoint struct. */
  else if (strcmp (name, "cmds") == 0)
    return Py_BuildValue ("s", self->cmds ? self->cmds : "");
  else if (strcmp (name, "cond_str") == 0)
    return Py_BuildValue ("s", self->cond_str ? self->cond_str : "");
  else if (strcmp (name, "description") == 0)
    return Py_BuildValue ("s", self->description ? self->description : "");
  else if (strcmp (name, "disp") == 0)
    return Py_BuildValue ("s", self->disp ? self->disp : "");
  else if (strcmp (name, "frame_addr_str") == 0)
    return Py_BuildValue ("s", self->frame_addr_str ? self->frame_addr_str : "");
  else if (strcmp (name, "func_name") == 0)
    return Py_BuildValue ("s", self->func_name ? self->func_name : "");
  else if (strcmp (name, "is_catchpoint") == 0)
    return Py_BuildValue ("i", self->is_catchpoint);
  else if (strcmp (name, "source_file") == 0)
    return Py_BuildValue ("s", self->bpt->source_file ? self->bpt->source_file : "");
  else if (strcmp (name, "symbolic_address") == 0)
    return Py_BuildValue ("s", self->symbolic_address ? self->symbolic_address : "");
  else if (strcmp (name, "type") == 0)
    return Py_BuildValue ("s", self->type ? self->type : "");
  else
    return NULL;
}

/* This functions provides breakpoint list attributes. */
static PyObject *
breaklist_getattr (breaklist *self, char *name)
{
  if (strcmp (name, "len") == 0)
    return Py_BuildValue ("i", self->len);
  return NULL;
}

/* Called from python for "len (breaklist)". */
static int
breaklist_length (breaklist *self)
{
  return self->len;
}

/* Called from python for "for b in blist" and "blist[n]". */
static PyObject *
breaklist_item (breaklist *self, int index)
{
  if (index < 0 || index >= self->len)
    {
      PyErr_SetString (PyExc_IndexError, "index out-of-bounds");
      return NULL;
    }
  return Py_BuildValue ("O", self->breakobj[index]);
}
  
static PyTypeObject BreakObjectType = 
{
  PyObject_HEAD_INIT (&PyType_Type)
  0,                     /* ob_size */
  "break",               /* tp_name */
  sizeof (breakobject),  /* tp_basicsize */
  0,                     /* tp_itemsize */
  (destructor) breakobject_destruct,
  0,
  (getattrfunc) breakobject_getattr
};

static PySequenceMethods breaklist_as_sequence = {
  (inquiry)       breaklist_length,      /* sq_length      "len(x)"   */
  (binaryfunc)            0,             /* sq_concat      "x + y"    */
  (intargfunc)            0,             /* sq_repeat      "x * n"    */
  (intargfunc)    breaklist_item,        /* sq_item        "x[i], in" */
  (intintargfunc)         0,             /* sq_slice       "x[i:j]"   */
  (intobjargproc)         0,             /* sq_ass_item    "x[i] = v" */
  (intintobjargproc)      0,             /* sq_ass_slice   "x[i:j]=v" */
};

static PyTypeObject BreakListType = 
{
  PyObject_HEAD_INIT (&PyType_Type)
  0,                     /* ob_size */
  "breaklist",           /* tp_name */
  sizeof (breaklist),    /* tp_basicsize */
  0,                     /* tp_itemsize */
  (destructor) breaklist_destruct,
  0,
  (getattrfunc) breaklist_getattr,
  0,
  0,
  0,
  0,
  &breaklist_as_sequence
};

/* The following function is a modified version of breakpoint_1 from gdb/breakpoint.c.

   IN bl        breakpoint list to be filled in.
   IN bp_count  expected number of breakpoints.
   IN bpnum     breakpoint number if retreiving a specific breakpoint.
   RETURN       NULL on error, bl otherwise.

   Below is the original header:

   Print information on breakpoint number BNUM, or -1 if all.
   If WATCHPOINTS is zero, process only breakpoints; if WATCHPOINTS
   is nonzero, process only watchpoints.  */

typedef struct {
  enum bptype  type;
  char *  description;
} ep_type_description_t;

static breaklist *
get_breakpoint_info (breaklist *bl, int bp_count, int bnum)
{
  register struct breakpoint *b;
  register struct command_line *l;
  CORE_ADDR last_addr = (CORE_ADDR)-1;
  int found_a_breakpoint = 0;
  static ep_type_description_t  bptypes[] =
  {
    {bp_none,                "?deleted?"},
    {bp_breakpoint,          "breakpoint"},
    {bp_hardware_breakpoint, "hw breakpoint"},
    {bp_until,               "until"},
    {bp_finish,              "finish"},
    {bp_watchpoint,          "watchpoint"},
    {bp_hardware_watchpoint, "hw watchpoint"},
    {bp_read_watchpoint,     "read watchpoint"},
    {bp_access_watchpoint,   "acc watchpoint"},
    {bp_longjmp,             "longjmp"},
    {bp_longjmp_resume,      "longjmp resume"},
    {bp_step_resume,         "step resume"},
    {bp_through_sigtramp,    "sigtramp"},
    {bp_watchpoint_scope,    "watchpoint scope"},
    {bp_call_dummy,          "call dummy"},
    {bp_shlib_event,         "shlib events"},
    {bp_catch_load,          "catch load"},
    {bp_catch_unload,        "catch unload"},
    {bp_catch_fork,          "catch fork"},
    {bp_catch_vfork,         "catch vfork"},
    {bp_catch_exec,          "catch exec"},
    {bp_catch_catch,         "catch catch"},
    {bp_catch_throw,         "catch throw"}
   };

  extern struct breakpoint *breakpoint_chain;
  static char *bpdisps[] = {"del", "dstp", "dis", "keep"};
  int idx = -1;
  GDB_FILE *stream_buf = tui_sfileopen (512);
  char *strbuf = tui_file_get_strbuf (stream_buf);

  for (b = breakpoint_chain; b; b = b->next)
    if (bnum == -1 || bnum == b->number)
      {
	/*  We only print out user settable breakpoints. */
	if (b->type != bp_breakpoint
            && b->type != bp_catch_load
            && b->type != bp_catch_unload
            && b->type != bp_catch_fork
            && b->type != bp_catch_vfork
            && b->type != bp_catch_exec
	    && b->type != bp_catch_catch
	    && b->type != bp_catch_throw
	    && b->type != bp_hardware_breakpoint
	    && b->type != bp_watchpoint
	    && b->type != bp_read_watchpoint
	    && b->type != bp_access_watchpoint
	    && b->type != bp_hardware_watchpoint)
	  continue;

	/* found_a_breakpoint = 1; */ /* see FIXFIX comment below */
	if (++idx >= bp_count)
	  returnOnError ("Error for bp_count!\n");
	bl->breakobj[idx]->bpt = b;

        if ((int)b->type > (sizeof(bptypes)/sizeof(bptypes[0]))
	    || (int)b->type != bptypes[(int)b->type].type)
	  {
	    char * fmt = "bptypes table does not describe type #%d.";
	    char * errmsg;

	    errmsg = (char *) xmalloc (strlen (fmt) + 10 /* for type number */);
	    sprintf (errmsg, fmt, (int)b->type);
	    /* Leak errmsg. */
	    returnOnError (errmsg);
	  }

	bl->breakobj[idx]->type = strdup (bptypes[(int)b->type].description);
	bl->breakobj[idx]->disp = strdup (bpdisps[(int)b->disposition]);

	switch (b->type)
	  {
	  case bp_watchpoint:
	  case bp_hardware_watchpoint:
	  case bp_read_watchpoint:
	  case bp_access_watchpoint:
	    print_expression (b->exp, stream_buf);
	    bl->breakobj[idx]->description = strdup (strbuf);
	    break;
 
          case bp_catch_load:
          case bp_catch_unload:
            if (b->dll_pathname == NULL)
	      bl->breakobj[idx]->description = strdup ("<any library> ");
            else
	      {
		char * format = "library \"%s\" ";

		bl->breakobj[idx]->description = (char *) xmalloc (strlen (format) + strlen (b->dll_pathname) + 1);
		sprintf (bl->breakobj[idx]->description, format, b->dll_pathname);
	      }
            break;

          case bp_catch_fork:
          case bp_catch_vfork:
	    if (b->forked_inferior_pid != 0)
	      {
		char * format = "process %d ";
		int max_pid_chars = sizeof (b->forked_inferior_pid) * 4;

		bl->breakobj[idx]->description = (char *) xmalloc (strlen (format) + max_pid_chars + 1);
		sprintf (bl->breakobj[idx]->description, format, b->forked_inferior_pid);
	      }
            break;

          case bp_catch_exec:
	    if (b->exec_pathname != NULL)
	      {
		char * format = "program \"%s\" ";

		bl->breakobj[idx]->description = (char *) xmalloc (strlen (format) + strlen (b->exec_pathname) + 1);
		sprintf (bl->breakobj[idx]->description, format, b->exec_pathname);
	      }
            break;
          case bp_catch_catch:
	    bl->breakobj[idx]->description = strdup ("exception catch ");
            break;
          case bp_catch_throw:
	    bl->breakobj[idx]->description = strdup ("exception throw ");
            break;

	  case bp_breakpoint:
	  case bp_hardware_breakpoint:
	  case bp_until:
	  case bp_finish:
#ifdef GET_LONGJMP_TARGET
	  case bp_longjmp:
	  case bp_longjmp_resume:
#endif /* GET_LONGJMP_TARGET */
	  case bp_step_resume:
	  case bp_through_sigtramp:
	  case bp_watchpoint_scope:
	  case bp_call_dummy:
	  case bp_shlib_event:
	    if (!b->address && b->enable == shlib_disabled)
	      bl->breakobj[idx]->description = strdup ("(deferred)");
	    else
	      bl->breakobj[idx]->description = 
		strdup ((char *) longest_local_hex_string_custom((LONGEST)b->address, "08l"));
	    /* last_addr = b->address; see FIXFIX comment below */
	    if (b->source_file)
	      {
		struct symbol *sym;

		sym = find_pc_sect_function (b->address, b->section);
		if (sym)
		  bl->breakobj[idx]->func_name = strdup (SYMBOL_SOURCE_NAME (sym));
	      }
	    else
	      {
		print_address_symbolic (b->address, stream_buf, demangle, " ");
		bl->breakobj[idx]->symbolic_address = strdup (strbuf);
	      }
	    break;
          default:
            break;
	  }

	if (b->frame)
	  {
	    print_address_numeric (b->frame, 1, stream_buf);
	    bl->breakobj[idx]->frame_addr_str = strdup (strbuf);
	  }

	if (b->cond)
	  {
	    print_expression (b->cond, stream_buf);
	    bl->breakobj[idx]->cond_str = strdup (strbuf);
	  }

	bl->breakobj[idx]->is_catchpoint = ep_is_catchpoint (b);

	if ((l = b->commands))
	  {
	    int len = 0;

	    bl->breakobj[idx]->cmds = strdup (get_command_line (l, 4));
	    l = l->next;
	    while (l)
	      {
		char * cmd;

		cmd = get_command_line (l, 4);
		bl->breakobj[idx]->cmds = xrealloc (bl->breakobj[idx]->cmds,
						    strlen (bl->breakobj[idx]->cmds) 
						    + strlen (cmd) + 1);
		strcat (bl->breakobj[idx]->cmds, cmd);
		l = l->next;
	      }
	  }
      }

#ifdef FIXFIX
  Doubt we need to do this for hpd (i.e., set default address for 'x' command).
  if (found_a_breakpoint)
    /* Compare against (CORE_ADDR)-1 in case some compiler decides
       that a comparison of an unsigned with -1 is always false.  */
    if (last_addr != (CORE_ADDR)-1)
      set_next_address (last_addr);
#endif

  gdb_file_delete (&stream_buf);

  return bl;
}


/* Returns a list of python breakpoint objects.
   Set args to -1 to get a list of all breakpoints,
   or to a specific breakpoint number to get a list of one. */

PyObject *
_get_breakpoint (PyObject *ignore, PyObject *args)
{
  /* FIXFIX: Should we cache the breakpoint list? */
  extern struct breakpoint *breakpoint_chain;
  register struct breakpoint *bp;
  int list_len = 0;
  int bnum;
  breaklist *self;
  
  if (!PyArg_ParseTuple (args, "i", &bnum))
    return NULL;

  if (bnum == -1 /* all breakpoints */)
    {
      /* Ascertain the number of breakpoints.
	 Only report user settable breakpoints. */
      for (bp = breakpoint_chain; bp; bp = bp->next)
	if (bp->type == bp_breakpoint
            || bp->type == bp_catch_load
            || bp->type == bp_catch_unload
            || bp->type == bp_catch_fork
            || bp->type == bp_catch_vfork
            || bp->type == bp_catch_exec
	    || bp->type == bp_catch_catch
	    || bp->type == bp_catch_throw
	    || bp->type == bp_hardware_breakpoint
	    || bp->type == bp_watchpoint
	    || bp->type == bp_read_watchpoint
	    || bp->type == bp_access_watchpoint
	    || bp->type == bp_hardware_watchpoint)
	  list_len++;
    }
  else
    list_len = 1;
  self = new_breaklist (list_len);

  /* If get_breakpoint_info () fails, cleanup and raise exception. */
  if (get_breakpoint_info (self, list_len, bnum) == NULL)
    {
      breaklist_destruct (self);
      self = (breaklist *) NULL;
    }
  return (PyObject *) self;
}


/********************************************
 * Target queries
 */

/* Return 1 if target is in stopped running state. */
int
tgt_has_execution ()
{
  return target_has_execution; /* A target #define from target.h */
}

int
get_active_thread ()
{
  int			new_pid;

#ifdef FIND_ACTIVE_THREAD
  /* If we have CMA threads, we will actually be stepping the active thread.
     Make it the official inferior_pid and inform the user if there is a
     thread switch. */

  return pid_to_thread_id(FIND_ACTIVE_THREAD ());
#endif
}

/********************************************
 * Execution control
 */

/* kill_target() is a modified version of kill_command() from gdb/inflow.c.

   No arguments.
   RETURN                NULL on error, Py_None otherwise.
 */

PyObject *
_kill_target (PyObject *ignore, PyObject *args)
{
  if (!PyArg_ParseTuple (args, ""))
    return NULL;

  /* Caller should have checked this already. */
  if (inferior_pid == 0)
    returnOnError ("Internal Error: inferior_pid == 0");

  target_kill ();

  /* Do the cleanup that kill_command () does. */
  init_thread_list();		/* Destroy thread info */

  /* NOTE: For the following code, we either need a way to report the
     information, or the front ends must know to check for it. */
#if SAVED_FROM_KILL_COMMAND
  /* Killing off the inferior can leave us with a core file.  If so,
     print the state we are left in.  */
  if (target_has_stack) {
    printf_filtered ("In %s,\n", target_longname);
    if (selected_frame == NULL)
      fputs_filtered ("No selected stack frame.\n", gdb_stdout);
    else
      print_stack_frame (selected_frame, selected_frame_level, 1);
  }
#endif
  Py_INCREF (Py_None);
  return (Py_None);
}

/* step_target() is a modified version of step_1() from gdb/infcmd.c.

   IN skip_subroutines   1 if stepping over subroutines.
   IN single_inst        1 if stepping by instruction.
   IN repeat_count       number of times to step.
   RETURN                NULL on error, Py_None otherwise

*/

PyObject *
_step_target (PyObject *ignore, PyObject *args)
{
  int    skip_subroutines;
  int    single_inst;
  int    repeat_count;
  struct frame_info *frame;
  int	 new_pid;
#ifdef GET_LONGJMP_TARGET
  struct cleanup *cleanups = 0;
#endif
  
  if (!PyArg_ParseTuple (args, "iii", &skip_subroutines, &single_inst, &repeat_count))
    return NULL;

#ifdef FIND_ACTIVE_THREAD
  /* If we have CMA threads, we will actually be stepping the active thread.
     Make it the official inferior_pid and inform the user if there is a
     thread switch.
     */

  new_pid = FIND_ACTIVE_THREAD ();

  if (new_pid < 1)
    /* Shouldn't get this error unless libgdb caller forgot to check for active target. */
    returnOnError ("Internal Error: Bad PID specifier.");

  if (inferior_pid != new_pid)
  {
    inferior_pid = new_pid;
    switch_to_thread (inferior_pid);
#if 0
    /* FIXFIX: Need to post thread switch in CLI? */
    printf_filtered ("[Switching to thread %d (%s)]\n",
		       pid_to_thread_id(inferior_pid),
#if defined(GDB_TARGET_IS_HPUX)
		       target_tid_to_str (inferior_pid)
#else
		       target_pid_to_str (inferior_pid)
#endif
		    );
#endif

  }
#endif

  if (!single_inst || skip_subroutines) /* leave si command alone */
    {
#ifdef GET_LONGJMP_TARGET
      enable_longjmp_breakpoint();
      cleanups = make_cleanup(disable_longjmp_breakpoint, 0);
#endif /* GET_LONGJMP_TARGET */
    }

  for (; repeat_count > 0; repeat_count--)
    {
      clear_proceed_status ();

      frame = get_current_frame ();
      if (!frame) /* Avoid coredump here.  Why tho? */
	returnOnError ("No current frame");
      step_frame_address = FRAME_FP (frame);
      step_sp = read_sp ();

      if (! single_inst)
	{
	  find_pc_line_pc_range (stop_pc, &step_range_start, &step_range_end);
	  if (step_range_end == 0)
	    {
	      char *name;
	      if (find_pc_partial_function (stop_pc, &name, &step_range_start,
					    &step_range_end) == 0)
		returnOnError ("Cannot find bounds of current function");
	      target_terminal_ours ();
#if 0
	      /* FIXFIX: Need to post this message in CLI! */\
	      printf_filtered ("\
Single stepping until exit from function %s, \n\
which has no line number information.\n", name);
#endif
	    }
	}
      else
	{
	  /* Say we are stepping, but stop after one insn whatever it does.  */
	  step_range_start = step_range_end = 1;
	  if (!skip_subroutines)
	    /* It is stepi.
	       Don't step over function calls, not even to functions lacking
	       line numbers.  */
	    step_over_calls = 0;
	}

      if (skip_subroutines)
	step_over_calls = 1;

      step_multi = (repeat_count > 1);
      proceed ((CORE_ADDR) -1, TARGET_SIGNAL_DEFAULT, 1);
      if (! stop_step)
	break;

      /* FIXME: On nexti, this may have already been done (when we hit the
	 step resume break, I think).  Probably this should be moved to
	 wait_for_inferior (near the top).  */
#if defined (SHIFT_INST_REGS)
      SHIFT_INST_REGS();
#endif
    }

  /* 980317 coulter - if we didn't do make_cleanup above, don't call
  ** do_cleanups here.  put it under the same ifdef.
  */
#ifdef GET_LONGJMP_TARGET
  if (!single_inst || skip_subroutines) {
    do_cleanups(cleanups);
  }
#endif /* GET_LONGJMP_TARGET */

  Py_INCREF (Py_None);
  return (Py_None);

}


/*******************************************
 * Frame related functions
 */

/* Frames are retrieved one at a time (no list object). */

typedef struct
{
  char * args;
  char * func_name;
  int    level;
  char * src_file;
  int    src_line_number;
  char * src_line_text;
} frame_info_base;

/* Python object to hold frame info. */
typedef struct
{
  PyObject_HEAD
  struct frame_info *fi;
  frame_info_base fib;
} frameobject;

staticforward PyTypeObject FrameObjectType;

/* Constructor. */
static frameobject *
new_frameobject ()
{
  frameobject * self;

#ifdef DEBUG_OBJECT_LIFE
  printf ("FRAME CREATE\n");
#endif

  self = PyObject_NEW (frameobject, &FrameObjectType);
  if (self == NULL)
    return NULL;

  self->fi = NULL;

  self->fib.args = NULL;
  self->fib.func_name = NULL;
  self->fib.level = 0;
  self->fib.src_file = NULL;
  self->fib.src_line_number = -1;
  self->fib.src_line_text = NULL;

  return self;
}

/* Destructor. */
static void
frameobject_destruct (frameobject *self)
{
  /* Don't free self->fi; it belongs to gdb. */
#ifdef DEBUG_OBJECT_LIFE
  printf ("FRAME DESTRUCT\n");
#endif
  FREE (self->fib.args);
  FREE (self->fib.func_name);
  FREE (self->fib.src_file);
  FREE (self->fib.src_line_text);
  PyMem_DEL (self);
}

static PyObject *
frameobject_getattr (frameobject *self, char *name)
{
  if (strcmp (name, "frame") == 0)
    return Py_BuildValue ("i", self->fi->frame);
  else if (strcmp (name, "pc") == 0)
    return Py_BuildValue ("i", self->fi->pc);
  else if (strcmp (name, "args") == 0)
    return Py_BuildValue ("s", self->fib.args);
  else if (strcmp (name, "func_name") == 0)
    return Py_BuildValue ("s", self->fib.func_name);
  else if (strcmp (name, "level") == 0)
    return Py_BuildValue ("i", self->fib.level);
  else if (strcmp (name, "src_file") == 0)
    return Py_BuildValue ("s", self->fib.src_file);
  else if (strcmp (name, "src_line_number") == 0)
    return Py_BuildValue ("i", self->fib.src_line_number);
  else if (strcmp (name, "src_line_text") == 0)
    return Py_BuildValue ("s", self->fib.src_line_text);
  else
    return NULL;
}

static PyTypeObject FrameObjectType = 
{
  PyObject_HEAD_INIT (&PyType_Type)
  0,                     /* ob_size */
  "frame",               /* tp_name */
  sizeof (frameobject),  /* tp_basicsize */
  0,                     /* tp_itemsize */
  (destructor) frameobject_destruct,
  0,
  (getattrfunc) frameobject_getattr
};

/* get_frame_info_base() is a modified version of print_frame_info_base()
 * from the file gdb/stack.c. The changes to print_frame_info_base()
 * include writing frame info to a struct instead of printing it, the
 * removal of annotations, and the removal of "#if 0...#endif".
 */
static void
get_frame_info_base (struct frame_info *fi, frame_info_base * fib)
{
  struct symtab_and_line sal;
  struct symbol *func;
  register char *funname = 0;
  enum language funlang = language_unknown;

/* RM: On HPPA, the frame_in_dummy() test fails. Using the
 * PC_IN_CALL_DUMMY test is not expensive, so we'll use it
 */
#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
      if (fi->pc >= fi->frame
          && fi->pc <= (fi->frame + CALL_DUMMY_LENGTH
                         + 32 * 4 + (NUM_REGS - FP0_REGNUM) * 8
                         + 6 * 4))
#else
      if (PC_IN_CALL_DUMMY (fi->pc, 0, 0))
#endif
#else  
  if (frame_in_dummy (fi))
#endif
    {
      /* Do this regardless of SOURCE because we don't have any source
	 to list for this frame.  */
      fib->func_name = strdup ("<function called from gdb>");
      return;
    }
  if (fi->signal_handler_caller)
    {
      /* Do this regardless of SOURCE because we don't have any source
	 to list for this frame.  */
      fib->func_name = strdup ("<signal handler called>");
      return;
    }

  /* If fi is not the innermost frame, that normally means that fi->pc
     points to *after* the call instruction, and we want to get the line
     containing the call, never the next line.  But if the next frame is
     a signal_handler_caller or a dummy frame, then the next frame was
     not entered as the result of a call, and we want to get the line
     containing fi->pc.  */
  sal =
    find_pc_line (fi->pc,
		  fi->next != NULL
		  && !fi->next->signal_handler_caller
		  && !frame_in_dummy (fi->next));

  func = find_pc_function (fi->pc);
  if (func)
    {
      /* In certain pathological cases, the symtabs give the wrong
	 function (when we are in the first function in a file which
	 is compiled without debugging symbols, the previous function
	 is compiled with debugging symbols, and the "foo.o" symbol
	 that is supposed to tell us where the file with debugging symbols
	 ends has been truncated by ar because it is longer than 15
	 characters).  This also occurs if the user uses asm() to create
	 a function but not stabs for it (in a file compiled -g).

	 So look in the minimal symbol tables as well, and if it comes
	 up with a larger address for the function use that instead.
	 I don't think this can ever cause any problems; there shouldn't
	 be any minimal symbols in the middle of a function; if this is
	 ever changed many parts of GDB will need to be changed (and we'll
	 create a find_pc_minimal_function or some such).  */

      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);
      if (msymbol != NULL
	  && (SYMBOL_VALUE_ADDRESS (msymbol) 
	      > BLOCK_START (SYMBOL_BLOCK_VALUE (func))))
	{
	  /* We also don't know anything about the function besides
	     its address and name.  */
	  func = 0;
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	}
      else
	{
          /* I'd like to use SYMBOL_SOURCE_NAME() here, to display
           * the demangled name that we already have stored in
           * the symbol table, but we stored a version with
           * DMGL_PARAMS turned on, and here we don't want
           * to display parameters. So call the demangler again,
           * with DMGL_ANSI only. RT
           * (Yes, I know that printf_symbol_filtered() will
           * again try to demangle the name on the fly, but
           * the issue is that if cplus_demangle() fails here,
           * it'll fail there too. So we want to catch the failure
           * ("demangled==NULL" case below) here, while we still
           * have our hands on the function symbol.)
           */
          char * demangled;
	  funname = SYMBOL_NAME (func);
	  funlang = SYMBOL_LANGUAGE (func);
          if (funlang == language_cplus) {
            demangled = cplus_demangle (funname, DMGL_ANSI);
            if (demangled == NULL)
              /* If the demangler fails, try the demangled name
               * from the symbol table. This'll have parameters,
               * but that's preferable to diplaying a mangled name.
               */
	      funname = SYMBOL_SOURCE_NAME (func);
          }
	}
    }
  else
    {
      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);
      if (msymbol != NULL)
	{
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	}
    }

  fib->func_name = strdup (funname ? funname : "??");
  /* fprintf_symbol_filtered (gdb_stdout, funname ? funname : "??", funlang, DMGL_ANSI); */
  if (sal.symtab)
    {
      if ( sal.symtab->filename)
	fib->src_file = strdup (sal.symtab->filename);
      fib->src_line_number = sal.line;

#ifdef GETSOURCELINES
      int mid_statement = source < 0 && fi->pc != sal.pc;

      if (addressprint && mid_statement)
	{
	  print_address_numeric (fi->pc, 1, gdb_stdout);
	}
      else
	print_source_lines (sal.symtab, sal.line, sal.line + 1, 0);
      current_source_line = max (sal.line - lines_to_list/2, 1);
#endif
    }
#ifdef GETFRAMEARGS
  {
    struct print_args_args args;
    args.fi = fi;
    args.func = func;
    catch_errors (print_args_stub, (char *)&args, "", RETURN_MASK_ALL);
    QUIT;
  }
#endif

#if 0
#ifdef PC_LOAD_SEGMENT
 ERROR - Not expecting PC_LOAD_SEGMENT to be defined in gdb-exports.c 
     /* If we couldn't print out function name but if can figure out what
        load segment this pc value is from, at least print out some info
	about its load segment. */
      if (!funname)
	{
	  annotate_frame_where ();
	  wrap_here ("  ");
	  printf_filtered (" from %s", PC_LOAD_SEGMENT (fi->pc));
	}
#endif
#ifdef PC_SOLIB
 ERROR - Not expecting PC_SOLIB to be defined in gdb-exports.c 
      if (!funname || (!sal.symtab || !sal.symtab->filename))
	{
	  char *lib = PC_SOLIB (fi->pc);
	  if (lib)
	    {
	      annotate_frame_where ();
	      wrap_here ("  ");
	      printf_filtered (" from %s", lib);
	    }
	}
#endif
#endif

#ifdef FIXFIX
 What is the following code doing? (ifdef out for now)
  if (source != 0)
    set_default_breakpoint (1, fi->pc, sal.symtab, sal.line);
#endif

}

static
struct frame_info *get_top_frame ()
{
  struct frame_info *top_fi;
  struct frame_info *fi;
  struct partial_symtab *ps;

  if (!target_has_stack)
    return NULL;

  if (!(top_fi = get_current_frame()))
    return NULL;

  /* Force syms to come in for entire stack. */
  for (fi = top_fi; fi != NULL; fi = get_prev_frame (fi))
    {
      if (ps = find_pc_psymtab (fi->pc))
        PSYMTAB_TO_SYMTAB (ps);
    }

  return top_fi;
}

PyObject *
_get_top_frame (PyObject *ignore, PyObject *args)
{
  frameobject *frame;
  char *err_msg;
  
  if (!PyArg_ParseTuple (args, ""))
    return NULL;

  frame = new_frameobject ();
  if (frame == NULL)
    return NULL;
  
  frame->fi = (struct frame_info *)
    call_catch_errors ((void *)get_top_frame, &err_msg, 0);
  if (err_msg)
    goto gdb_error;
  if (!frame->fi)
    {
      frameobject_destruct (frame);
      returnOnError ("Internal Error: Can't get top frame.");
    }

  call_catch_errors ((void *) get_frame_info_base, &err_msg, 2,
		     (void *) frame->fi, (void *) &frame->fib);
  if (err_msg)
    goto gdb_error;

  return (PyObject *) frame;

 gdb_error:
  frameobject_destruct (frame);
  returnOnGdbError (err_msg);
  
}

PyObject *
_get_next_frame (PyObject *ignore, PyObject *args)
{
  frameobject *prev_frame, *frame;
  struct frame_info *fi;
  char * err_msg;

  if (!PyArg_ParseTuple (args, "O", &prev_frame))
    return NULL;

  fi = (struct frame_info *)
    call_catch_errors ((void *) get_prev_frame, &err_msg, 1, (void *) prev_frame->fi);
  returnOnGdbError (err_msg);

  if (fi == NULL)
    {
      Py_INCREF(Py_None);
      return Py_None;
    }

  frame = new_frameobject ();
  frame->fi = fi;

  frame->fib.level = prev_frame->fib.level + 1;
  call_catch_errors ((void *) get_frame_info_base, &err_msg, 2,
		     (void *) frame->fi, (void *) &frame->fib);
  if (err_msg)
    {
      frameobject_destruct (frame);
      returnOnGdbError (err_msg);
    }

  return (PyObject *) frame;
}

/**********************************
 * Initialize
 */

void
init_gdb_exports (PyObject *dict)
{
  /* Exceptions are raised using ErrorObject. */
  ErrorObject = Py_BuildValue ("s", "libgdb.error");
  PyDict_SetItemString(dict, "error", ErrorObject);

  /* For catching gdb error messages. */
  error_hook = catch_gdb_error_msg;
}








#if 0
/*******************************************
 * Local variables
 */

static void
print_variable_value (var, frame, stream)
     struct symbol *var;
     struct frame_info *frame;
     GDB_FILE *stream;
{
  value_ptr val = read_var_value (var, frame);

  value_print (val, stream, 0, Val_pretty_default);
}


static int
print_block_frame_locals (b, fi, num_tabs, stream)
     struct block *b;
     register struct frame_info *fi;
     int num_tabs;
     register GDB_FILE *stream;
{
  int nsyms;
  register int i, j;
  register struct symbol *sym;
  register int values_printed = 0;

  nsyms = BLOCK_NSYMS (b);

  for (i = 0; i < nsyms; i++)
    {
      sym = BLOCK_SYM (b, i);
      switch (SYMBOL_CLASS (sym))
	{
	case LOC_LOCAL:
	case LOC_REGISTER:
	case LOC_STATIC:
	case LOC_BASEREG:
	  values_printed = 1;
	  for (j = 0; j < num_tabs; j++)
	    fputs_filtered("\t", stream);
	  fputs_filtered (SYMBOL_SOURCE_NAME (sym), stream);
	  fputs_filtered (" = ", stream);
	  print_variable_value (sym, fi, stream);
	  fprintf_filtered (stream, "\n");
	  break;

	default:
	  /* Ignore symbols which are not locals.  */
	  break;
	}
    }
  return values_printed;
}

static void
print_frame_local_vars (fi, num_tabs, stream)
     register struct frame_info *fi;
     register int num_tabs;
     register GDB_FILE *stream;
{
  register struct block *block = get_frame_block (fi);
  register int values_printed = 0;

  if (block == 0)
    {
      fprintf_filtered (stream, "No symbol table info available.\n");
      return;
    }
  
  while (block != 0)
    {
      if (print_block_frame_locals (block, fi, num_tabs, stream))
	values_printed = 1;
      /* After handling the function's top-level block, stop.
	 Don't continue to its superblock, the block of
	 per-file symbols.  */
      if (BLOCK_FUNCTION (block))
	break;
      block = BLOCK_SUPERBLOCK (block);
    }

  if (!values_printed)
    {
      fprintf_filtered (stream, "No locals.\n");
    }
}
#endif

