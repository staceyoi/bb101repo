#! /opt/gnu/bin/python
import sys
import os
import string
sys.path.append ('/CLO/Components/WDB/Src/gnu/gdb/python')
import libgdb

def cmd_loop ():
    python_mode = 0
    python_prompt_text = "python> "
    python_prompt = python_prompt_text
    saved_line = ""

    while 1:
	if (python_mode):
	    line = libgdb.gdb_cmdline_input (python_prompt)
	else:
	    line = libgdb.gdb_cmdline_input (libgdb.get_prompt ())

	if (line == None):
	    break
	line = string.rstrip (line)

	# Check for muli-line python input before stripping left side of line
	if (python_mode):
	    if (len (line) and line[-1] == ':' and saved_line == ""):
		saved_line = line + "\n"
		python_prompt = "....... "
		continue
	    elif (saved_line != ""):
		if (line == ""):
		    line = saved_line
		    saved_line = ""
		    python_prompt = python_prompt_text
		else:
		    saved_line = saved_line + line + "\n"
		    continue

	line = string.lstrip (line)

	if (line == ''):
	    continue

	# For either mode, do quit via gdb command.
	if (line == 'quit'):
	    libgdb.exec_gdb_cmd (line)
	    break

	# Check for python/gdb mode switch
	if (line == "switch-python-mode"):
	    if (python_mode):
		python_mode = 0
	    else:
		python_mode = 1
	    continue
	elif (line == 'python'):
	    python_mode = 1
	    continue
	elif (line == 'gdbcmd'):
	    python_mode = 0
	    continue
	elif (line == 'hpd-mode'):
	    import Hpd
	    Hpd.set_hpd_cmd_module ("test_cmds")
	    Hpd.set_gdb_cmd_module ("test_cmds")
	    Hpd.cmd_loop ()
	    continue

	# Check for one-shot python or gdb command (e.g., to allow
	# executing a gdb command when in python mode)
	cmd = string.split (line)
	if (cmd[0] == 'gdbcmd'):
	    libgdb.exec_gdb_cmd (string.join (cmd[1:]))
	    continue
	elif (cmd[0] == 'python'):
	    try:
		exec (string.join (cmd[1:]))
	    except:
		print 'PYTHON ERROR:', sys.exc_type, sys.exc_value
	    continue

	# Handle command based on current mode
	if (python_mode):
	    try:
		exec (line)
	    except:
		print 'PYTHON ERROR:', sys.exc_type, sys.exc_value
		print line
	else:
	    libgdb.exec_gdb_cmd (line)

libgdb.add_rl_mode_switch_key ()
devnull = os.open ("/dev/null", os.O_WRONLY)
cmd_loop ()
