%module libgdb
%{
%}

%typedef int return_mask;

%typedef int GDB_FILE;

%readonly
/* Chain containing all defined commands.  */
extern struct cmd_list_element *cmdlist;

/* Chain containing all defined info subcommands.  */
extern struct cmd_list_element *infolist;

/* Chain containing all defined enable subcommands. */
extern struct cmd_list_element *enablelist;

/* Chain containing all defined disable subcommands. */
extern struct cmd_list_element *disablelist;

/* Chain containing all defined delete subcommands. */
extern struct cmd_list_element *deletelist;

/* Chain containing all defined toggle subcommands. */
extern struct cmd_list_element *togglelist;

/* Chain containing all defined stop subcommands. */
extern struct cmd_list_element *stoplist;

/* Chain containing all defined "enable breakpoint" subcommands. */
extern struct cmd_list_element *enablebreaklist;

/* Chain containing all defined set subcommands */
extern struct cmd_list_element *setlist;

/* Chain containing all defined unset subcommands */
extern struct cmd_list_element *unsetlist;

/* Chain containing all defined show subcommands.  */
extern struct cmd_list_element *showlist;

/* Chain containing all defined \"set history\".  */
extern struct cmd_list_element *sethistlist;

/* Chain containing all defined \"show history\".  */
extern struct cmd_list_element *showhistlist;

/* Chain containing all defined \"unset history\".  */
extern struct cmd_list_element *unsethistlist;

/* Chain containing all defined maintenance subcommands. */
extern struct cmd_list_element *maintenancelist;

/* Chain containing all defined "maintenance info" subcommands. */
extern struct cmd_list_element *maintenanceinfolist;

/* Chain containing all defined "maintenance print" subcommands. */
extern struct cmd_list_element *maintenanceprintlist;

extern struct cmd_list_element *setprintlist;
extern struct cmd_list_element *showprintlist;
extern struct cmd_list_element *setchecklist;
extern struct cmd_list_element *showchecklist;

%readwrite

extern GDB_FILE *gdb_stdout;
extern GDB_FILE *gdb_stderr;
//extern GDB_FILE *gdb_stringstream;

// extern char *gdb_file_get_strbuf (GDB_FILE *file);
// extern void gdb_file_reset_astring (GDB_FILE *file);

// %readwrite
// extern int ignore_prompt_for_continue;

/////////////////////////////////////////////
// Auto-wrapped gdb functions
//
extern char *get_prompt ();
// extern void set_prompt (char *s);

/////////////////////////////////////////////
// Auto-wrapped helper functions (from gdb-exports.c)
//
extern void init_gdb_exports (PyObject *module);
extern int tgt_has_execution ();

extern void disable_gdb_stdout ();
extern void enable_gdb_stdout ();
extern struct cmd_list_element *init_cmdlist_iterator (struct cmd_list_element *head);
extern struct cmd_list_element *next_cmdlist_element (struct cmd_list_element *elem);
extern char *cmdlist_name (struct cmd_list_element *elem);
extern char *cmdlist_doc (struct cmd_list_element *elem);
extern int cmdlist_abbrev_flag (struct cmd_list_element *elem);
extern void add_rl_mode_switch_key ();


///////////////////////////////////////////
// Hand-wrapped helper functions
//
%native(kill_target) extern PyObject * _kill_target (PyObject *self, PyObject *args);
%native(get_top_frame) extern PyObject *_get_top_frame (PyObject *self, PyObject *args);
%native(get_next_frame) extern PyObject *_get_next_frame (PyObject *self, PyObject *args);
%native(get_breakpoint) extern PyObject *_get_breakpoint (PyObject *self, PyObject *args);
%native(set_breakpoint) extern PyObject *_set_breakpoint (PyObject *self, PyObject *args);
%native(step_target) extern PyObject *_step_target (PyObject *self, PyObject *args);

%inline %{

#define RETURN_MASK_ALL 15

int exec_gdb_cmd_catch_errors (char * cmdstr)
{
    extern void execute_command (char *, int);

    execute_command (cmdstr, 1);
    return 0;
}

void exec_gdb_cmd (char * cmdstr)
{
    extern int catch_errors (int (*func) (char *), char *args, char *errstring, return_mask mask);
    int ret;

    ret = catch_errors (exec_gdb_cmd_catch_errors, cmdstr, NULL, RETURN_MASK_ALL);
}

char * gdb_cmdline_input (char * prompt)
{
    extern char * command_line_input (char *prrompt, int repeat, char *annotation_suffix);

    return command_line_input (prompt, 0, "");
}


%}

// init_gdb_exports() initializes exception handling for
// libgdb. The "d" argument is the libgdb dictionary as
// declared in the generated file libgdb.c.
%init %{
  init_gdb_exports (d);
%}

