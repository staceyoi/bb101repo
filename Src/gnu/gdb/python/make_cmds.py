#! /opt/gnu/bin/python
import sys
import string
sys.path.append ('/CLO/Components/WDB/Src/gnu/gdb/python')
import libgdb

def dump_doc (prefix, cmd_elem):
    "Dump out documentation strings in form: <cmd>_doc = doc_string"
    while 1:
	name = string.replace (libgdb.cmdlist_name (cmd_elem), "-", "_")
	name = string.replace (name, " ", "_")
	name = string.replace (name, "<", "_")
	name = string.replace (name, ">", "_")
	doc  = string.replace (libgdb.cmdlist_doc (cmd_elem), "\\", "\\\\")
	doc  = string.replace (doc, "\n", "\\n\\\n")
	doc  = string.replace (doc, "\"", "\\\"")
	print "\n" + prefix + name + "_doc = \"" + doc + "\""
	cmd_elem = libgdb.next_cmdlist_element (cmd_elem);
	if (not libgdb.cmdlist_name (cmd_elem)):
	    break
    
def dump_dictionary (prefix, cmd_elem):
    "Dump out dictionary of gdb commands with command names as keys."
    while 1:
	name = string.replace (libgdb.cmdlist_name (cmd_elem), "-", "_")
	name = string.replace (name, " ", "_")
	name = string.replace (name, "<", "_")
	name = string.replace (name, ">", "_")
	abbrev_flag = "%d" % libgdb.cmdlist_abbrev_flag (cmd_elem)

	# Create entry: "   '<cmd_key>': Cmds (<cmd>, do_<cmd>, <cmd>_doc, abbrev_flag),"
	entry = "    '" + name + "': Command (\"" + name + "\", \"do_" + prefix + name \
		+ "\", " + prefix + name + "_doc, " + abbrev_flag + ")"
	cmd_elem = libgdb.next_cmdlist_element (cmd_elem);
	if (not libgdb.cmdlist_name (cmd_elem)):
	    print entry + "\n}"
	    break
	else:
	    print entry + ","

def dump_commands ():
    "Dump out gdb doc strings, a dictionary of commands, and a simple list of commands."
    print "\n######################################"
    print "# Command Strings"
    dump_doc ("", libgdb.init_cmdlist_iterator (libgdb.cvar.cmdlist))
    dump_doc ("info_", libgdb.init_cmdlist_iterator (libgdb.cvar.infolist))

    print "\n######################################"
    print "# Command Dictionary\n"
    print "gdb_cmds = {"
    dump_dictionary ("", libgdb.init_cmdlist_iterator (libgdb.cvar.cmdlist))
    print "info_cmds = {"
    dump_dictionary ("info_", libgdb.init_cmdlist_iterator (libgdb.cvar.infolist))

dump_commands ()
libgdb.exec_gdb_cmd ("quit")
