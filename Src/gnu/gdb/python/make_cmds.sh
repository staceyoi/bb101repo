#! /usr/bin/sh
# This script creates the gdb_cmds.py file, the gdb command set.

head="# This file is generated. Do not edit.\n\nfrom CmdManager import *"

echo "$head" >GdbCmds.py
/CLO/Components/WDB/build/hppa1.1-hp-hpux10.20/gdb/gdb -q -python ./make_cmds.py | grep -v "starting the" >>GdbCmds.py
