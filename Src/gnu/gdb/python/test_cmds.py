#! /opt/gnu/bin/python
import sys
import string
import libgdb
import Hpd
import CmdManager
import GdbCmds
import HpdCmds

not_running_err = "The program is not being run."
STEP_INTO = 0
STEP_OVER = 1
STEP_BY_INST = 1
STEP_BY_STAT = 0

initialized = 0
gdb_cmds = None
info_cmds = None
hpd_cmds = None

##################################
# Shared functions (shared between gdb and hpd)

def initialize ():
    "Initialize gdb command sets (info commands, set commands, etc.)"
    global initialized
    global gdb_cmds
    global info_cmds
    global hpd_cmds
    
    if (initialized):
	return None
    initialized = 1
    gdb_cmds = Hpd.get_gdb_cmds ()
    info_cmds = CmdManager.CmdSet (GdbCmds.info_cmds, "info")
    hpd_cmds = Hpd.get_hpd_cmds ()

def print_msg (msg, args = None):
    print "MSG:",
    if (args == None):
	print msg
    else:
	print msg % args

def print_internal_error (msg):
    print "Internal Error:", msg

def query_yes_no (prompt):
    print "MSG:",
    while 1:
	answer = raw_input (prompt)
	if (len (answer) and (answer[0] == 'y' or answer[0] == 'Y'
			      or answer == 'n' or answer[0] == 'N')):
	    break
    if (answer[0] == 'y' or answer[0] == 'Y'):
	return 'y'
    else:
	return 'n'

def quit (arg):
    if (libgdb.tgt_has_execution ()):
	print_msg ("A debugging session is active.")
	answer = query_yes_no ("Do you still want to close the debugger? (y or n) ")
	if (answer[0] == 'n'):
	    return None
	else:
	    libgdb.kill_target ()
    libgdb.exec_gdb_cmd ("quit")

def load_file (arg):
    libgdb.exec_gdb_cmd ("file" + " " + arg)

def backtrace (arg):
    if (not libgdb.tgt_has_execution ()):
	return None
    try:
	frame = libgdb.get_top_frame ()
    except libgdb.error, msg:
	print_msg (msg)
	return None

    while (frame != None):
	print frame.level, frame.func_name, "@ line", frame.src_line_number, "of", frame.src_file,
	print "(pc = %d, frame addr = %d)" % (frame.pc, frame.frame)
	try:
	    frame = libgdb.get_next_frame (frame)
	except libgdb.error, msg:
	    print_msg (msg)
	    return None

def set_breakpoint (location, addr):
    try:
	libgdb.set_breakpoint (location, addr)
    except libgdb.error, msg:
	print_msg (msg)

def list_actionpoints (arg, list = []):
    if (len (list)):
	bpl = libgdb.get_breakpoint (list[0])
    else:
	bpl = libgdb.get_breakpoint (-1)
    if ((not len (bpl)) and arg == "all"):
	print_msg ("No breakpoints or watchpoints.")
	return
    enable = ""
    got_one = 0

    for bp in bpl:
	last_field = ""
	if (arg != "all" and bp.type != arg and (not len (list))):
	    continue
	# First, print main status line.
	got_one = got_one + 1
	if (got_one == 1):
	    print_msg ("Num Type           Disp Enb Address    What")
	if (bp.enable):
	    enable = "y"
	else:
	    enable = "n"

	if (bp.type == "breakpoint" and (not bp.deferred)):
	    # If no function name is available, check for symbolic address instead.
	    if (bp.func_name != ""):
		last_field = "in %s at %s:%d" % (bp.func_name, bp.source_file, bp.line)
	    elif (bp.symbolic_address != ""):
		last_field = bp.symbolic_address
	print_msg ("%-3d %-14s %-4s %-3s %s %s", (bp.id, bp.type, bp.disp, enable, bp.description, last_field))
	
	# Second, print various additional attributes if set.
	if (bp.frame_addr_str != ""):
	    print_msg ("\tstop only in stack frame at %s", (bp.frame_addr_str))
	if (bp.cond_str != ""):
	    print_msg ("\tstop only if %s", (bp.cond_str))
	if (bp.hit_count):
	    ess = ""
	    action = ""
	    if (bp.is_catchpoint):
		action = "catchpoint"
	    else:
		action = "breakpoint"
	    if (bp.hit_count > 1):
		ess = "s"
	    print_msg ("\t%s already hit %d time%s", (action, bp.hit_count, ess))
	if (bp.ignore_count):
	    print_msg ("\tignore next %d hits", (bp.ignore_count))
	if (bp.cmds != ""):
	    print_msg (bp.cmds)
	    

    if (not got_one):
	if (len (list)):
	    print_msg ("No breakpoint or watchpoint number %d.", (list[0]))
	if (arg == "breakpoint"):
	    print_msg ("No breakpoints.")
	elif (arg == "watchpoint"):
	    print_msg ("No watchpoints.")
    
def execute_target (runtype, args):

    if (runtype == "run"):
	libgdb.disable_gdb_stdout ()
	libgdb.exec_gdb_cmd (runtype + " " + args)
	libgdb.enable_gdb_stdout ()
	return None

    if (not libgdb.tgt_has_execution ()):
	print_msg (not_running_err)
	return None

    if (runtype == "continue"):
	# FIXFIX: just using cont command for now.
	libgdb.disable_gdb_stdout ()
	libgdb.exec_gdb_cmd (runtype + " " + args)
	libgdb.enable_gdb_stdout ()
    elif (runtype == "jump"):
	# FIXFIX: just using jump command for now.
	libgdb.disable_gdb_stdout ()
	libgdb.exec_gdb_cmd (runtype + " " + args)
	libgdb.enable_gdb_stdout ()
    else:
	print_internal_error (runtype + " is not a recognized run_target() argument.")
    
def step_target (step_type, step_granularity, count_str):
    "Do a step_type step of the target, where step_type is either STEP_INTO or STEP_OVER,\n\
    and step_granularity is either STEP_BY_STAT or STEP_BY_INST. count_str is a string \n\
    representing the repeat count for stepping. Empty count_str results in a single step."

    repeat_count = 1
    if (not libgdb.tgt_has_execution ()):
    	print_msg (not_running_err)
    	return None

    if (count_str != ""):
	try:
	    repeat_count = string.atoi (count_str)
	except ValueError:
	    print_msg ("Invalid step count \"%s\".", (count_str))
	    return None
    try:
	libgdb.step_target (step_type, step_granularity, repeat_count)
    except libgdb.error, msg:
	print_msg (msg)

def kill (arg):
    if (not libgdb.tgt_has_execution ()):
	print_msg (not_running_err)
    elif (query_yes_no ("Kill the program being debugged? (y or n) ") == 'y'):
	try:
	    libgdb.kill_target ()
	except libgdb.error, msg:
	    print_msg (msg)

def run (arg):
    execute_target ("run", arg)

def print_expr (arg):
    sym = None
    try:
	sym = libgdb.get_symbol_value (arg)
    except libgdb.error, msg:
	print_msg (msg)
	return None
    print_msg ("type == %s, length == %d", (sym.type, sym.length))


##################################
# HPD commands

def do_hpd_actions (arg):
    if (arg == ""):
	list_actionpoints ("all")
	return None
    tokens = string.split (arg)
    if (len (tokens) > 1):
	print_msg ("Too many arguments (expected 1 or none).")
	return None
    token = tokens[0]
    if (token == "-enabled"):
	print_internal_error ("-enabled not implemented.")
    elif (token == "-disabled"):
	print_internal_error ("-disabled not implemented.")
    elif (token == "-break"):
	list_actionpoints ("breakpoint")
    elif (token == "-watch"):
	list_actionpoints ("watchpoint")
    elif (token == "-barrier"):
	print_internal_error ("-barrier not implemented.")
    elif (token[0] == "-"):
	print_msg ("%s is not a recognized argument", (token))
    else:
	num = 0
	try:
	    num = string.atoi(arg);
	except ValueError:
	    print_msg ("Invalid ID: \"%s\".", (arg))
	    return None
	list_actionpoints ("", [num])

def do_hpd_quit (arg):
    quit (arg)
def do_hpd_exit (arg):
    do_hpd_quit (arg)

def do_hpd_load (arg):
    load_file (arg)

def do_hpd_where (arg):
    backtrace (arg)

def do_hpd_break (arg):
    location = ""
    address = ""
    count = 0

    if (arg == ""):
	print_msg ("break location required.")
	return None
    arglist =  string.split (arg)
    arg = arglist[0]
    location = arglist[0]

    num_options = len (arglist)
    skip = 0
    for idx in range (1, num_options):
	if (skip):
	    skip = 0
	    continue
	arg = arglist[idx]
	if (arg[0] == "-"):
	    if (arg == "-count"):
		if (idx < num_options - 1):
		    skip = 1
		    idx = idx + 1
		    try:
			count = string.atoi (arglist[idx])
		    except ValueError:
			print_msg ("-count must be followed by a number")
			return None
		    print_internal_error ("-count not implemented.")
		    return None
		else:
		    print_msg ("-count requires an argument.")
		    return None
	    elif (arg == "-if"):
		print_internal_error ("-if not implemented.")
	    elif (arg == "-stop"):
		print_internal_error ("-stop not implemented.")
	    else:
		print_msg ("Unrecognized option: " + arg)
	    return None
	else:
	    print_msg ("Unrecognized option: " + arg)
	    return None

    if (string.count (location, "#") > 1):
	print_internal_error ("Only <file>#<line> supported at this time.")
	return None

    location = string.replace (location, "#", ":")
    set_breakpoint (location, address)

def do_hpd_kill (arg):
    kill (arg)

def do_hpd_run (arg):
    run (arg)

def do_hpd_cont (arg):
    execute_target ("continue", arg)

def do_hpd_step (arg):
    steptype = STEP_INTO
    stepinst = STEP_BY_STAT
    count_str = ""

    if (arg == ""):
	step_target (steptype, stepinst, "")
	return None
	
    tokens = string.split (arg)
    if (len (tokens) > 2):
	print_msg ("Too many arguments (expected 2 or less).")
	return None
    for token in tokens:
	if (token == "-over"):
	    steptype = STEP_OVER
	elif (token == "-finish"):
	    print_internal_error ("-finish not implemented.")
	    return None
	elif (token[0] == "-"):
	    print_msg ("Unrecognized option: %s", (token))
	    return None
	elif (count_str != ""):
	    print_msg ("Unrecognized argument: %s", (token))
	    return None
	else:
	    count_str = token
    step_target (steptype, stepinst, count_str)

def do_hpd_help (arg):
    global hpd_cmds
    initialize ()

    if (arg == None or arg == ""):
	print_msg ("PRINT LIST OF HELP CLASSES")
	return None
    arglist = string.split (arg)
    if (len (arglist) >= 2):
	print_msg ("Too many arguments")
    docstr = hpd_cmds.get_doc (arglist[0])
    if (len (docstr)):
	print_msg (docstr)

def do_hpd_print (arg):
    print_expr (arg)

##################################
# GDB commands


def do_quit (arg):
    quit (arg)
def do_q (arg):
    do_quit (arg)

def do_file (arg):
    load_file (arg)

def do_bt (arg):
    backtrace (arg)

def do_break (args):
    location = ""
    address = ""

    if (args == ""):
	set_breakpoint (location, address)
	return None
    arglist =  string.split (args)
    arg = arglist[0]
    if (arg[0] == "+" or arg[0] == "-"):
	print_internal_error ("Offsets not yet supported")
	return None
    elif (arg[0] == "*"):
	address = arg[1:]
    else:
	location = arg
    
    if (len (arglist) > 1):
	if (arg == "if"):
	    print_internal_error ("Conditionals not yet supported")
	else:
	    print_msg ("Too many arguments.")
	return None
    set_breakpoint (location, address)

def do_b (arg):
    do_break (arg)
def do_br (arg):
    do_break (arg)
def do_bre (arg):
    do_break (arg)
def do_brea (arg):
    do_break (arg)

def do_info_breakpoints (arg):
    list_breakpoints (arg)

def do_attach (arg):
	libgdb.exec_gdb_cmd ("attach" + " " + args)

def do_continue (arg):
    execute_target ("continue", arg)
def do_c (arg):
    do_continue (arg)

def do_detach (arg):
    libgdb.exec_gdb_cmd ("detach" + " " + args)

def do_finish (arg):
    execute_target ("finish", arg)

def do_go (arg):
    execute_target ("go", arg)

def do_handle (arg):
    libgdb.exec_gdb_cmd ("handle" + " " + args)

def do_jump (arg):
    execute_target ("jump", arg)

def do_kill (arg):
    kill (arg)

def do_next (arg):
    step_target (STEP_OVER, STEP_BY_STAT, arg)
def do_n (arg):
    do_next (arg)

def do_nexti (arg):
    step_target (STEP_OVER, STEP_BY_INST, arg)

def do_run (arg):
    execute_target ("run", arg)
def do_r (arg):
    do_run (arg)

def do_signal (arg):
    libgdb.exec_gdb_cmd ("signal" + " " + args)

def do_step (arg):
    step_target (STEP_INTO, STEP_BY_STAT, arg)

def do_stepi (arg):
    step_target (STEP_INTO, STEP_BY_INST, arg)

def do_until (arg):
    execute_target ("until", arg)

def do_print (arg):
    print_expr (arg)

def do_help (arg):
    initialize ()
    doc = ""
    prefix = " "
    topic = ""
    cmdset = None

    if (arg == None or arg == ""):
	print_msg ("PRINT LIST OF HELP CLASSES")
	return None
    arglist = string.split (arg)
    if (len (arglist) >= 2):
	if (arglist[0] == "info"):
	    global info_cmds
	    prefix = "info"
	    cmdset = info_cmds
	    topic = arglist[1]
	else:
	    global gdb_cmds
	    cmdset = gdb_cmds
	    topic = arglist[0]
    else:
	global gdb_cmds
	cmdset = gdb_cmds
	topic = arglist[0]
    docstr = cmdset.get_doc (topic)
    if (len (docstr)):
	print_msg (docstr)

def do_info (arg):
    if (arg == ""):
	print_msg ("SHOULD LIST INFO CMD ARGS")
	return None
    # arg starts with name of info sub command
    subcmd = string.split (arg)
    subcmd = subcmd[0]
    subargs = string.lstrip (arg[len(subcmd):])

    cmd_matches = lookup_gdb_cmd (subcmd, info_names_list, 1)
    num_matches = len (cmd_matches)
    if (num_matches <= 0):
	print_msg ("Undefined info command: \"" + subcmd + "\".  Try \"help info\".")
    elif (num_matches == 1):
	subname = "info_" + cmd_matches[0]
	try:
	    info_list[subname].run (subargs)
	except:
	    print 'PYTHON ERROR:', sys.exc_type, sys.exc_value
    else:
	err_msg = "Ambiguous info command \"" + subcmd + "\": "
	for match in cmd_matches:
	    err_msg = err_msg + " " + match + ","
	print_msg (err_msg[:-1] + ".")

