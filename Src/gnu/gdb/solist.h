/* Contains the common definition for the "so_list" structure. This will
   be used in hp-ia64-solib.c and somsolib.c.
 
   Copyright 1998 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Written by Hewlett-Packard.  */

/*--------------------------------------------------------------------------
   Includes
--------------------------------------------------------------------------*/

/* System file includes */

#ifdef HP_IA64
#include <unwind_dld.h> /* For load_module_info struct */
#endif

/* Gdb includes */
#include "defs.h"
#include "symtab.h"
#include "bfd.h"
#include "objfiles.h"
#include "target.h"

enum mixed_mode_type_t
  {
    MIXED_MODE_NONE,
    MIXED_MODE_32,
    MIXED_MODE_64
  };

/* JAGag21714:
   jini: mixed mode changes: Introduced a common structure for both PA32 and
   IPF. The architecture specific portion is handled by the "solib_desc"
   field. This points to a structure of type "som_solib_mapped_entry" in the
   case of PA32 and to a structure of type "load_module_desc" in the case of
   IPF. This is done since mixed mode binaries have both ELF and SOM load
   modules in the shared library list -- so represent them with a single
   common structure. */
/* FIXME: Currently only the IPF so_list and the PA32 SOM so_list structures
   are represented with this structure. Include the PA64 so_list structure
   too. */
   
/* A structure to keep track of all the known shared objects.  */
struct so_list
  {
    bfd *abfd;
    char *name; // unused for PA32.
    struct so_list *next;
    struct objfile *objfile;
/* elz: added this field to store the address in target space (in the
   library) of the library descriptor (handle) which we read into
   som_solib_mapped_entry structure */
    CORE_ADDR desc_addr;
    void   *solib_desc;
    struct section_table *sections;
    struct section_table *sections_end;
    boolean loaded;  /* JAGaa80453 - added a new element to check
                        whether the shared library is loaded or not */
    boolean symbols_loaded; // unused for PA32
#ifdef HP_IA64
    /* jini: mixed mode changes: Added a field to check if this is a mixed
     mode PA library. Currently this is true only for PA32 libraries */
    boolean is_mixed_mode_pa_lib;

    /* During core file debugging, flag this to true if the module
       is a mismatch to the one in the core file */
    boolean mismatch;
#endif
  };


