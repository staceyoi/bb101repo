/* Read HP PA/Risc object files for GDB.
   Copyright 1991, 1992, 1996, 1999 Free Software Foundation, Inc.
   Written by Fred Fish at Cygnus Support.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "bfd.h"
#include <syms.h>
#include <sys/mman.h>

/* 10.20 doesn't seem to define this POSIX macro */
#ifndef MAP_FAILED
#define MAP_FAILED (void *) -1L
#endif 

/* CM: The "is_comdat" field was added in HP-UX 11.x. This is here to allow
   gdb to be built using an HP-UX 10.x build environment. */
struct symbol_dictionary_record_hpux11 {
        unsigned int  hidden           : 1;
        unsigned int  secondary_def    : 1;
        unsigned int  symbol_type      : 6;
        unsigned int  symbol_scope     : 4;
        unsigned int  check_level      : 3;
        unsigned int  must_qualify     : 1;
        unsigned int  initially_frozen : 1;
        unsigned int  memory_resident  : 1;
        unsigned int  is_common        : 1;
        unsigned int  dup_common       : 1;
        unsigned int  xleast           : 2;
        unsigned int  arg_reloc        :10;
        union name_pt name;
        union name_pt qualifier_name; 
        unsigned int  has_long_return  : 1;
        unsigned int  no_relocation    : 1;
        unsigned int  is_comdat        : 1;
        unsigned int  reserved         : 5;
        unsigned int  symbol_info      :24;
        unsigned int  symbol_value;
};

#include "symtab.h"
#include "symfile.h"
#include "objfiles.h"
#include "buildsym.h"
#include "stabsread.h"
#include "gdb-stabs.h"
#include "complaints.h"
#include "gdb_string.h"
#include "demangle.h"
#include "som.h"
#include "hpread.h"
#include "libhppa.h"


#ifndef HP_IA64
extern int isdigit(int);
#endif

/* Various things we might complain about... */

static void som_symfile_init (struct objfile *, int);

static void som_new_init (struct objfile *);

static void som_symfile_read (struct objfile *, int, int);

static void som_symfile_finish (struct objfile *);

char *som_symtab_read_stringtab (bfd *, int *);

static void
som_symtab_read (bfd *, struct objfile *, struct section_offsets *);

static void
som_symfile_offsets (struct objfile *, struct section_addr_info *);

/* For JAGaf16859 - Gdb to list source files
 * even when the executable is moved from the 
 * compilation dirctory 
 */

void *
som_set_current_compdir(struct partial_symtab *);

/* FIXME: These should really be in a common header somewhere */

extern void hpread_build_psymtabs (struct objfile *, int);

extern void hpread_symfile_finish (struct objfile *);

extern void hpread_symfile_init (struct objfile *);

#ifdef HP_IA64
extern int som_solib_section_offsets (struct objfile *objfile,
                                      struct section_offsets *offsets,
                                      void *saddrs);
#endif

int initialize_import_list = 0;

char * som_symtab_read_stringtab (abfd, size)
     bfd *abfd;
     int * size;
{
  char * stringtab;
  int val;
  *size = obj_som_stringtab_size (abfd);
  stringtab = xmalloc (*size);
  bfd_seek (abfd, obj_som_str_filepos (abfd), SEEK_SET);
  val = bfd_read (stringtab, *size, 1, abfd);
  if (val != *size)
    error ("Can't read in HP string table.");
  return stringtab;
}

/*

   LOCAL FUNCTION

   som_symtab_read -- read the symbol table of a SOM file

   SYNOPSIS

   void som_symtab_read (bfd *abfd, struct objfile *objfile,
   struct section_offsets *section_offsets)

   DESCRIPTION

   Given an open bfd, a base address to relocate symbols to, and a
   flag that specifies whether or not this bfd is for an executable
   or not (may be shared library for example), add all the global
   function and data symbols to the minimal symbol table.
 */

static void
som_symtab_read (abfd, objfile, section_offsets)
     bfd *abfd;
     struct objfile *objfile;
     struct section_offsets *section_offsets;
{
  unsigned int number_of_symbols;
  int val, dynamic;
  char *stringtab;
  asection *shlib_info;
  struct symbol_dictionary_record *buf, *bufp, *endbufp;
  char *symname;
  CONST int symsize = sizeof (struct symbol_dictionary_record);
  CORE_ADDR text_offset, data_offset;
  int section;
  /* JAGag21714:
     jini: Mixed mode support: Sept 2006: The 'text_offset' and 'data_offset'
     values tend to be 64 bit for mixed mode libraries; but the
     'symbol_value' field of a 'symbol_dictionary_record' structure is
     of type 'unsigned int' (32 bit). Hence use a variable 'symbol_value'
     to represent this. */
  CORE_ADDR symbol_value;


  text_offset = ANOFFSET (section_offsets, 0);
  data_offset = ANOFFSET (section_offsets, 1);

  number_of_symbols = bfd_get_symcount (abfd);

  /* srikanth, mmap() fails if we ask it to map zero bytes.
     The kernel is telling us it can't give us 0 bytes out
     of all the memory it owns. Stinginess indeed. */

  if (number_of_symbols == 0)
    { /* Apparently this object module has been stripped. Give warning
       * JAGaf20680
       */
      if (!profile_on)
        warning ("Load module %s has been stripped\n", objfile->name);
    }

  if (!number_of_symbols || !obj_som_stringtab_size (abfd))
    return;

  /* srikanth, 000205, allocate huge short lived objects via mmap
     rather than alloca. The function alloca turns around and calls
     malloc, resulting in the growth of the process footprint. 

     Under HP-UX, as with most other systems, free never returns
     space to the kernel. Using mmap and munmap is a clean way
     to borrow memory and return it once done. */

  buf = mmap(0,   /* map anywhere */
             symsize * number_of_symbols,
             PROT_READ | PROT_WRITE,
	     MAP_ANONYMOUS | MAP_PRIVATE,
             -1,  
             0); 

  if (buf == MAP_FAILED)
    error ("Out of memory");

  bfd_seek (abfd, obj_som_sym_filepos (abfd), SEEK_SET);
  val = bfd_read (buf, symsize * number_of_symbols, 1, abfd);
  if (val != symsize * number_of_symbols)
    error ("Couldn't read symbol dictionary!");

  stringtab = mmap (0, 
                    (obj_som_stringtab_size (abfd)),
                    PROT_READ | PROT_WRITE,
                    MAP_ANONYMOUS | MAP_PRIVATE,
                    -1,
                    0);

  if (stringtab == MAP_FAILED)
    error ("Out of memory");

  bfd_seek (abfd, obj_som_str_filepos (abfd), SEEK_SET);
  val = bfd_read (stringtab, obj_som_stringtab_size (abfd), 1, abfd);
  if (val != obj_som_stringtab_size (abfd))
    error ("Can't read in HP string table.");

  /* We need to determine if objfile is a dynamic executable (so we
     can do the right thing for ST_ENTRY vs ST_CODE symbols).

     There's nothing in the header which easily allows us to do
     this.  The only reliable way I know of is to check for the
     existance of a $SHLIB_INFO$ section with a non-zero size.  */
  /* The code below is not a reliable way to check whether an
   * executable is dynamic, so I commented it out - RT
   * shlib_info = bfd_get_section_by_name (objfile->obfd, "$SHLIB_INFO$");
   * if (shlib_info)
   *   dynamic = (bfd_section_size (objfile->obfd, shlib_info) != 0);
   * else
   *   dynamic = 0;
   */
  /* I replaced the code with a simple check for text offset not being
   * zero. Still not 100% reliable, but a more reliable way of asking
   * "is this a dynamic executable?" than the above. RT
   */
  dynamic = (text_offset != 0);

  /* 
   * srikanth, 981006, CLLbs16090, the code fragment above i.e., 
   * 
   *         dynamic = (text_offset != 0);  
   * 
   * is distinguishing between a .sl and a main program && _not_ between a
   * shared bound and an archive bound program. What we want is the latter 
   * as we are basically interested in trying to separate out import and 
   * export stubs from actual functions. The minimal symbol type ST_ENTRY 
   * in a shared bound binary refers to a stub while ST_CODE refers to the 
   * actual function ...  
   *
   * According to PA32 runtime doucument, on 10.0 and later versions of 
   * HP-UX the correct answer to this question is obtained by looking into 
   * the first word in the TEXT space and if this matches the symbolic const
   * DL_HDR_VERSION2 defined in <shl.h> then the binary is an incomplete one. 
   *
   */
   
  if (!dynamic)  /* don't bother if it is known to be a .sl */
    {
      asection * text_section;      /* section handle */ 
      unsigned int dl_header_version = 0;   

      /* $TEXT$ space begins with $SHLIB_INFO$ subspace */
      text_section = bfd_get_section_by_name (objfile->obfd, "$SHLIB_INFO$");
      if (text_section)
        {
          /* Get the first word from the TEXT space */ 
          bfd_get_section_contents (objfile->obfd, text_section, 
                                    &dl_header_version, 0, sizeof (int));

          if (dl_header_version == DL_HDR_VERSION_ID2)
            dynamic = 1;
        }
    }

  endbufp = buf + number_of_symbols;

/* JAGag21715:
   jini: Mixed mode changes: Sept 2006: The IA64 definition for
   SMASH_TEXT_ADDRESS does not hold good for SOM symbols. Define
   SMASH_TEXT_ADDRESS to reflect the SOM definition. Reset before
   returning from this function */
#ifdef HP_IA64
#ifdef SMASH_TEXT_ADDRESS
#undef SMASH_TEXT_ADDRESS
#define SMASH_TEXT_ADDRESS(addr) ((addr) &= ~0x3)
#endif
#endif
  for (bufp = buf; bufp < endbufp; ++bufp)
    {
      enum minimal_symbol_type ms_type;

      QUIT;

      symbol_value = bufp->symbol_value;
      switch (bufp->symbol_scope)
	{
	case SS_UNIVERSAL:
	case SS_EXTERNAL:
	  switch (bufp->symbol_type)
	    {
	    case ST_SYM_EXT:
	    case ST_ARG_EXT:
	      continue;

	    case ST_CODE:
	    case ST_PRI_PROG:
	    case ST_SEC_PROG:
	    case ST_MILLICODE:
	      symname = bufp->name.n_strx + stringtab;
	      ms_type = mst_text;
              section = SECT_OFF_TEXT (objfile);
              symbol_value += text_offset;

#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif

	      break;

	    case ST_ENTRY:
	      symname = bufp->name.n_strx + stringtab;
	      /* For a dynamic executable, ST_ENTRY symbols are
	         the stubs, while the ST_CODE symbol is the real
	         function.  */
	      if (dynamic)
		ms_type = mst_solib_trampoline;
	      else
		ms_type = mst_text;
              section = SECT_OFF_TEXT (objfile);
	      symbol_value += text_offset;

#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif
	      break;

	    case ST_STUB:
	      symname = bufp->name.n_strx + stringtab;
	      ms_type = mst_solib_trampoline;
              section = SECT_OFF_TEXT (objfile);
	      symbol_value += text_offset;

#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif
	      break;

	    case ST_DATA:
	      symname = bufp->name.n_strx + stringtab;
	      symbol_value += data_offset;
	      ms_type = mst_data;
              section = SECT_OFF_DATA (objfile);
	      break;
	    default:
	      continue;
	    }
	  break;

#if 0
	  /* SS_GLOBAL and SS_LOCAL are two names for the same thing (!).  */
	case SS_GLOBAL:
#endif
	case SS_LOCAL:
	  switch (bufp->symbol_type)
	    {
	    case ST_SYM_EXT:
	    case ST_ARG_EXT:
	      continue;

	    case ST_CODE:
	      symname = bufp->name.n_strx + stringtab;
	      ms_type = mst_file_text;
              section = SECT_OFF_TEXT (objfile);
	      symbol_value += text_offset;
#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif

	    check_strange_names:
	      /* Utah GCC 2.5, FSF GCC 2.6 and later generate correct local
	         label prefixes for stabs, constant data, etc.  So we need
	         only filter out L$ symbols which are left in due to
	         limitations in how GAS generates SOM relocations.

	         When linking in the HPUX C-library the HP linker has
	         the nasty habit of placing section symbols from the literal
	         subspaces in the middle of the program's text.  Filter
	         those out as best we can.  Check for first and last character
	         being '$'. 

	         And finally, the newer HP compilers emit crud like $PIC_foo$N
	         in some circumstance (PIC code I guess).  It's also claimed
	         that they emit D$ symbols too.  What stupidity.  */

              /* RM: For DOOM, we nned all these symbols to perform
               * relocations, so let's leave them in. */
              
              /* these seem to occupy as much as a third of the table,
                 so skip these if not in DOOM binary - srikanth */

                if (!doom_executable)
                  if (IS_RELOC_ONLY_SYMBOL (symname))
                    continue;

	      break;

	    case ST_PRI_PROG:
	    case ST_SEC_PROG:
	    case ST_MILLICODE:
	      symname = bufp->name.n_strx + stringtab;
	      ms_type = mst_file_text;
              section = SECT_OFF_TEXT (objfile);
	      symbol_value += text_offset;
#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif
	      break;

	    case ST_ENTRY:
	      symname = bufp->name.n_strx + stringtab;
	      /* For a dynamic executable, ST_ENTRY symbols are
	         the stubs, while the ST_CODE symbol is the real
	         function.  */
#ifdef IS_RELOC_ONLY_SYMBOL
#endif
#if 0
	      if (dynamic)
		ms_type = mst_solib_trampoline;
	      else
#endif
		ms_type = mst_file_text;
              section = SECT_OFF_TEXT (objfile);
	      symbol_value += text_offset;
#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif
	      break;

	    case ST_STUB:
	      symname = bufp->name.n_strx + stringtab;
	      ms_type = mst_solib_trampoline;
              section = SECT_OFF_TEXT (objfile);
	      symbol_value += text_offset;
#ifdef SMASH_TEXT_ADDRESS
	      SMASH_TEXT_ADDRESS (symbol_value);
#endif
	      break;


	    case ST_DATA:
              /* mst_file_data symbols are compiler generated internal symbols.
                 These (actually just a small class of these) are useful only 
                 in doom mode : we need them to perform relocations that pxdb 
                 would have performed for us otherwise  -- srikanth.
              */
              if (!doom_executable)
                continue;
	      symname = bufp->name.n_strx + stringtab;
	      symbol_value += data_offset;
	      ms_type = mst_file_data;
              section = SECT_OFF_DATA (objfile);
	      goto check_strange_names;

	    default:
	      continue;
	    }
	  break;

	  /* This can happen for common symbols when -E is passed to the
	     final link.  No idea _why_ that would make the linker force
	     common symbols to have an SS_UNSAT scope, but it does.

	     This also happens for weak symbols, but their type is
	     ST_DATA.  */
          /* RM: Hmm, data unsat's aren't necessarily weak
             symbols. And their value field doesn't necessarily hold a
             valid value. Let's just ignore them for now. 

             BUT the CMA implementation needs to find cma__g_known_threads
             which is an ST_DATA, so we have a speical case (for the moment)
             to accept cma__g_known_threads.  What we really need is to
             be able to tell which ones are valid and which are
             bogus.
             */

#ifdef HP_MXN
           /* Also admit the unsat "__active_pthreads" as the MxN
              implementation requires this -- srikanth, 000929.
           */
#endif
	case SS_UNSAT:
	  switch (bufp->symbol_type)
	    {
	    case ST_STORAGE:
	    case ST_DATA:
	      symname = bufp->name.n_strx + stringtab;
	      if (bufp->symbol_type == ST_STORAGE ||
		  strcmp (symname, "cma__g_known_threads") == 0
#ifdef HP_MXN
                  || strcmp (symname, "__active_pthreads") == 0
#endif
                  )
	        {
		  symbol_value += data_offset;
		  ms_type = mst_data;
		  section = SECT_OFF_DATA (objfile);
		  break;
		}
	      else
		continue;

	    default:
	      continue;
	    }
	  break;

	default:
	  continue;
	}

      if (bufp->name.n_strx > obj_som_stringtab_size (abfd))
	error ("Invalid symbol data; bad HP string table offset: %d",
	       bufp->name.n_strx);

      /* RM: ??? !!! we are assuming that bufp->symbol_type can be
       * fitted into a char * -- probably true, but should be fixed
       * sometime. */
      prim_record_minimal_symbol_and_info (symname,
                                      symbol_value, ms_type,
                                      (char *)(long)bufp->symbol_type, section,
                                      0,
				      objfile);
    }
  munmap (buf, symsize * number_of_symbols);
  munmap (stringtab, (obj_som_stringtab_size (abfd)));

/* jini: Mixed mode changes: JAGag21715. */
#ifdef HP_IA64
#ifdef SMASH_TEXT_ADDRESS
#undef SMASH_TEXT_ADDRESS
#define SMASH_TEXT_ADDRESS(addr);
#endif
#endif
}

/* Read in and initialize the SOM import list which is present
   for all executables and shared libraries.  The import list
   consists of the symbols that are referenced in OBJFILE but
   not defined there.  (Variables that are imported are dealt
   with as "loc_indirect" vars.)
   Return value = number of import symbols read in. */
int
init_import_symbols (objfile)
     struct objfile *objfile;
{
  unsigned int import_list;
  unsigned int import_list_size;
  unsigned int string_table;
  unsigned int string_table_size;
  char *string_buffer;
  register int i;
  register int j;
  register int k;
  asection *text_section;	/* section handle */
  unsigned int dl_header[18];   /* SOM executable header */
  asection * dlt_section, * plt_section; 
  CORE_ADDR dlt_start, plt_start = 0;
  int m;
#ifdef HPPA_FIX_AND_CONTINUE
  extern boolean objfile_needs_fix (struct objfile *);
  boolean needs_fix;
#endif

  /* A struct for an entry in the SOM import list */
  typedef struct
    {
      int name;			/* index into the string table */
      short dont_care1;		/* we don't use this */
      unsigned char type;	/* 0 = NULL, 2 = Data, 3 = Code, 7 = Storage, 13 = Plabel */
      unsigned int reserved2:8;	/* not used */
    }
  SomImportEntry;

  /* We read 100 entries in at a time from the disk file. */
#define SOM_READ_IMPORTS_NUM         100
#define SOM_READ_IMPORTS_CHUNK_SIZE  (sizeof (SomImportEntry) * SOM_READ_IMPORTS_NUM)
  SomImportEntry buffer[SOM_READ_IMPORTS_NUM];

  /* Initialize in case we error out */
  objfile->import_list = NULL;
  objfile->import_list_size = 0;

  /* It doesn't work, for some reason, to read in space $TEXT$;
     the subspace $SHLIB_INFO$ has to be used.  Some BFD quirk? pai/1997-08-05 */

  /* I believe this is because a "section" in bfd terms refers to a 
   * subspace in SOM terms and further because SHLIB_INFO is the first 
   * subspace in $TEXT$ space  --- srikanth, 981007
   */

  text_section = bfd_get_section_by_name (objfile->obfd, "$SHLIB_INFO$");
  if (!text_section)
    return 0;

   /* Madhavi, begin purify */
   memset (dl_header, 0, sizeof (dl_header));
   /* end purify */

  /* Get the SOM executable header */
  bfd_get_section_contents (objfile->obfd, text_section, dl_header, 0, 18 * sizeof (int));

  /* Check header version number for 10.x HP-UX */
  /* Currently we deal only with 10.x systems; on 9.x the version # is 89060912.
     FIXME: Change for future HP-UX releases and mods to the SOM executable format */
  if (dl_header[0] != DL_HDR_VERSION_ID2)
    return 0;

  import_list = dl_header[4];
  import_list_size = dl_header[5];
  if (!import_list_size)
    return 0;
  string_table = dl_header[10];
  string_table_size = dl_header[11];
  if (!string_table_size)
    return 0;

  /* Suck in SOM string table */
  string_buffer = mmap(0, string_table_size,
                PROT_READ | PROT_WRITE,
	        MAP_ANONYMOUS | MAP_PRIVATE,
                -1,
                0); 

  if (string_buffer == MAP_FAILED)
    error ("Out of memory");

  bfd_get_section_contents (objfile->obfd, text_section, string_buffer,
			    string_table, string_table_size);

  /* Allocate import list in the psymbol obstack; this has nothing
     to do with psymbols, just a matter of convenience.  We want the
     import list to be freed when the objfile is deallocated */
  objfile->import_list
    = (ImportEntry *) obstack_alloc (&objfile->psymbol_obstack,
				   import_list_size * sizeof (ImportEntry));

  /* RM: find the dlt start address */
  dlt_section = bfd_get_section_by_name (objfile->obfd, "$DLT$");
  dlt_start =  dlt_section->vma + ANOFFSET(objfile->section_offsets,
                                           SECT_OFF_DATA (objfile));
  
  /* Read in the import entries, a bunch at a time */
#ifdef HPPA_FIX_AND_CONTINUE
#ifndef GDB_TARGET_IS_HPPA_20W
  needs_fix = objfile_needs_fix (objfile);
  if (needs_fix)
    {
      plt_section = bfd_get_section_by_name (objfile->obfd, "$PLT$");
      plt_start = plt_section->vma + ANOFFSET(objfile->section_offsets,
                                              SECT_OFF_DATA (objfile));
      objfile->first_import_index_for_static = -1;
    }
#endif
#endif
  for (j=0, k=0, m=0;
       j < (import_list_size / SOM_READ_IMPORTS_NUM);
       j++)
    {
      bfd_get_section_contents (objfile->obfd, text_section, buffer,
			      import_list + j * SOM_READ_IMPORTS_CHUNK_SIZE,
				SOM_READ_IMPORTS_CHUNK_SIZE);
      for (i = 0; i < SOM_READ_IMPORTS_NUM; i++, k++)
	{
	  if (buffer[i].name != -1 &&
              ((buffer[i].type != (unsigned char) 0) ||
              (k >= dl_header[16])))
	    {
	      objfile->import_list[k].name
		= (char *) obstack_alloc (&objfile->psymbol_obstack, strlen (string_buffer + buffer[i].name) + 1);
	      strcpy (objfile->import_list[k].name, string_buffer + buffer[i].name);
#ifdef HPPA_FIX_AND_CONTINUE
              if (needs_fix && k >= dl_header[16])
#else
              if (k >= dl_header[16])
#endif
                {
                  objfile->import_list[k].addr.plt_addr = plt_start +
                                                          m*sizeof(CORE_ADDR)*2;
                  /* If the type of an entry is zero, it indicates that it's
                     a static function. Remember the index of the first
                     static function.
                  */
                  if ((buffer[i].type == (unsigned char) 0) &&
                      (objfile->first_import_index_for_static == -1))
                    objfile->first_import_index_for_static = k;
                  m++;
                }
              else
                objfile->import_list[k].addr.dlt_addr = dlt_start +
                                                        k*sizeof (CORE_ADDR);

	      /* Some day we might want to record the type and other information too */
	    }
	  else			/* null type */
	    objfile->import_list[k].name = NULL;

	}
    }

  /* Get the leftovers */
  if (k < import_list_size)
    bfd_get_section_contents (objfile->obfd, text_section, buffer,
			      import_list + k * sizeof (SomImportEntry),
			  (import_list_size - k) * sizeof (SomImportEntry));
  for (i = 0; k < import_list_size; i++, k++)
    {
      if (buffer[i].name != -1 &&
          ((buffer[i].type != (unsigned char) 0) ||
#ifdef HPPA_FIX_AND_CONTINUE
          (needs_fix && k >= dl_header[16])))
#else
          (k >= dl_header[16])))
#endif
	{
	  objfile->import_list[k].name
	    = (char *) obstack_alloc (&objfile->psymbol_obstack, strlen (string_buffer + buffer[i].name) + 1);
	  strcpy (objfile->import_list[k].name, string_buffer + buffer[i].name);
#ifdef HPPA_FIX_AND_CONTINUE
          if (needs_fix && k >= dl_header[16])
#else
          if (k >= dl_header[16])
#endif
            {
              objfile->import_list[k].addr.plt_addr = plt_start +
                                                      m*sizeof(CORE_ADDR)*2;
              /* If the type of an entry is zero, it indicates that it's
                 a static function. Remember the index of the first
                 static function.
              */
              if ((buffer[i].type == (unsigned char) 0) &&
                  (objfile->first_import_index_for_static == -1))
                objfile->first_import_index_for_static = k;
              m++;
            }
          else
            objfile->import_list[k].addr.dlt_addr = dlt_start +
                                               k*sizeof(CORE_ADDR);

	  /* Some day we might want to record the type and other information too */
	}
      else
	objfile->import_list[k].name = NULL;
    }

  objfile->import_list_size = import_list_size;
  munmap (string_buffer, string_table_size);
  return import_list_size;
}

/* Scan and build partial symbols for a symbol file.
   We have been initialized by a call to som_symfile_init, which 
   currently does nothing.

   SECTION_OFFSETS is a set of offsets to apply to relocate the symbols
   in each section.  This is ignored, as it isn't needed for SOM.

   MAINLINE is true if we are reading the main symbol
   table (as opposed to a shared lib or dynamically loaded file).

   This function only does the minimum work necessary for letting the
   user "name" things symbolically; it does not read the entire symtab.
   Instead, it reads the external and static symbols and puts them in partial
   symbol tables.  When more extensive information is requested of a
   file, the corresponding partial symbol table is mutated into a full
   fledged symbol table by going back and reading the symbols
   for real.

   We look for sections with specific names, to tell us what debug
   format to look for:  FIXME!!!

   somstab_build_psymtabs() handles STABS symbols.

   Note that SOM files have a "minimal" symbol table, which is vaguely
   reminiscent of a COFF symbol table, but has only the minimal information
   necessary for linking.  We process this also, and use the information to
   build gdb's minimal symbol table.  This gives us some minimal debugging
   capability even for files compiled without -g.  */

static void
som_symfile_read (objfile, mainline, threshold_exceeded)
     struct objfile *objfile;
     int mainline;
     int threshold_exceeded;
{
  bfd *abfd = objfile->obfd;
  struct cleanup *back_to;
  asection * dlt_section;

#ifdef HP_IA64
  objfile->is_mixed_mode_pa_lib = true;
#endif
  init_minimal_symbol_collection ();
  back_to = make_cleanup_discard_minimal_symbols ();

  /* Read in the import list and the export list.  Currently
     the export list isn't used; the import list is used in
     hp-symtab-read.c to handle static vars declared in other
     shared libraries. */
  if (initialize_import_list)
    init_import_symbols (objfile);
  else
    objfile->import_list = NULL;

  /* RM: We need the export list to find "main" in stripped
     executables. We need to find "main" so we can read shared library
     information right after dld has run. */

  /* srikanth, 990629, let us not read in the dld export list right now. It is
     useful only in rare situations (see above.) Even in that case, we need to
     read in the export list of only the main load module. If needed somsolib.c
     will slurp it in.
  */

  objfile->export_list = NULL;
  objfile->export_list_size = 0;

  /*
   * Set objfile's dlt start and end
   */
  dlt_section = bfd_get_section_by_name (objfile->obfd, HP_DLT);
  if (dlt_section)
    {
      objfile->dlt_start_addr =  dlt_section->vma +
                                        ANOFFSET(objfile->section_offsets,
                                                   SECT_OFF_DATA (objfile));
      objfile->dlt_end_addr = objfile->dlt_start_addr +
                                bfd_section_size (objfile->obfd, dlt_section);
    }
  else
    {
      objfile->dlt_start_addr = 0;
      objfile->dlt_end_addr = 0;
    }

  /* Process the normal SOM symbol table first. 
     This reads in the DNTT and string table, but doesn't
     actually scan the DNTT. It does scan the linker symbol
     table and thus build up a "minimal symbol table". */

  som_symtab_read (abfd, objfile, objfile->section_offsets);

  if (!threshold_exceeded)
    {
      /* Now read information from the stabs debug sections.  This is
         a no-op for SOM.  Perhaps it is intended for some kind of
         mixed STABS/SOM situation? */
      stabsect_build_psymtabs (objfile, mainline,
                               "$GDB_SYMBOLS$", "$GDB_STRINGS$", "$TEXT$");
    }

  /* "Install" the minimal symbols that have been collected above.
     The actual "minsym"'s are constructed here, and such things
     as name demangling takes place here. */
  /* RM: This used to be done _after_ we built the psymtab. For DOOM,
     though, I need the minimal symbol tables to be available _before_
     I build the psymtabs. I think moving this up should be okay. */

  /* RM: but we do need to explicitly set the demangling style to
     hp. This used to be done by hpread_build_psymtabs (it still is,
     but it's too late by then). */
  /* Demangling style -- if EDG style already set, don't change it,
     as HP style causes some problems with the KAI EDG compiler */ 
  /* CM: Force it to HP only if we see any spaces with the following
     names. These are the names of the spaces that contain debug
     information produced by HP compilers. */
  if (bfd_get_section_by_name (abfd, "$OBJDEBUG$") ||
      bfd_get_section_by_name (abfd, "$DEBUG$") ||
      bfd_get_section_by_name (abfd, "$PINFO$")) {
    if (current_demangling_style != edg_demangling) {
      /* Otherwise, ensure that we are using HP style demangling */ 
      set_demangling_style (HP_DEMANGLING_STYLE_STRING);
    }
  }

  install_minimal_symbols (objfile);

  /* Now read the native debug information. 
     This builds the psymtab. This used to be done via a scan of
     the DNTT, but is now done via the PXDB-built quick-lookup tables
     together with a scan of the GNTT. See hp-psymtab-read.c. */
  if (!threshold_exceeded)
    {
      hpread_build_psymtabs (objfile, mainline);
    }

  /* Further symbol-reading is done incrementally, file-by-file,
   * in a step known as "psymtab-to-symtab" expansion. hp-symtab-read.c
   * contains the code to do the actual DNTT scanning and symtab building.
   */

  /* Force hppa-tdep.c to re-read the unwind descriptors.  */
  objfile->obj_private = NULL;
  do_cleanups (back_to);
}

static void
som_symfile_add_psymtabs (objfile, mainline)
     struct objfile *objfile;
     int mainline;
{
  hpread_symfile_init (objfile);
  stabsect_build_psymtabs (objfile, mainline,
			   "$GDB_SYMBOLS$", "$GDB_STRINGS$", "$TEXT$");
  hpread_build_psymtabs (objfile, mainline);
}

/* Initialize anything that needs initializing when a completely new symbol
   file is specified (not just adding some symbols from another file, e.g. a
   shared library).

   We reinitialize buildsym, since we may be reading stabs from a SOM file.  */

static void
som_new_init (ignore)
     struct objfile *ignore;
{
  stabsread_new_init ();
  buildsym_new_init ();
}

/* Perform any local cleanups required when we are done with a particular
   objfile.  I.E, we are in the process of discarding all symbol information
   for an objfile, freeing up all memory held for it, and unlinking the
   objfile struct from the global list of known objfiles. */

static void
som_symfile_finish (objfile)
     struct objfile *objfile;
{
  if (objfile->sym_stab_info != NULL)
    {
      mfree (objfile->md, objfile->sym_stab_info);
    }
  hpread_symfile_finish (objfile);
}

/* SOM specific initialization routine for reading symbols.  */

static void
som_symfile_init (objfile, threshold_exceeded)
     struct objfile *objfile;
     int threshold_exceeded;
{
  /* SOM objects may be reordered, so set OBJF_REORDERED.  If we
     find this causes a significant slowdown in gdb then we could
     set it in the debug symbol readers only when necessary.  */
  objfile->flags |= OBJF_REORDERED;
  if (!threshold_exceeded)
    {
      hpread_symfile_init (objfile);
    }
}

/* SOM specific parsing routine for section offsets.

   Plain and simple for now.  */

static void
som_symfile_offsets (objfile, addrs)
     struct objfile *objfile;
     struct section_addr_info *addrs;
{
  int i;
  CORE_ADDR text_addr;

  objfile->num_sections = SECT_OFF_MAX;
  objfile->section_offsets = (struct section_offsets *)
    obstack_alloc (&objfile->psymbol_obstack, SIZEOF_SECTION_OFFSETS);

  /* FIXME: ezannoni 2000-04-20 The section names in SOM are not
     .text, .data, etc, but $TEXT$, $DATA$,... We should initialize
     SET_OFF_* from bfd. (See default_symfile_offsets()). But I don't
     know the correspondence between SOM sections and GDB's idea of
     section names. So for now we default to what is was before these
     changes.*/
  objfile->sect_index_text = 0;
  objfile->sect_index_data = 1;
  objfile->sect_index_bss = 2;
  objfile->sect_index_rodata = 3;

  /* First see if we're a shared library.  If so, get the section
     offsets from the library, else get them from addrs.  */
#if !defined (GDB_TARGET_IS_HPPA_20W)
  /* som_solib_section_offsets isn't defined for PA64.  This file really
     shouldn't be part of the compilation. */
  if (!som_solib_section_offsets (objfile, objfile->section_offsets,
                                 (void *)addrs))
#endif
    {
      /* Note: Here is OK to compare with ".text" because this is the
         name that gdb itself gives to that section, not the SOM
         name. */
      /* JYG: MERGE NOTE: not sure if the above is true.
	 Since the new scheme is to use *_index instead of hardcoded
	 SECT_OFF_TEXT, etc. for SOM, assume index 0 is where
	 text_addr is cached. */
      /*
      for (i = 0; i < SECT_OFF_MAX && addrs->other[i].name; i++)
	if (strcmp (addrs->other[i].name, ".text") == 0)
	  break;
      text_addr = addrs->other[i].addr;
      */
      text_addr = addrs->other[0].addr;
      
      for (i = 0; i < SECT_OFF_MAX; i++)
	ANOFFSET (objfile->section_offsets, i) = text_addr;
    }
}

/* RM: DOOM stuff */

#include "linkmap.h"

#define INIT_COMP_MAP_ENTRIES 256
#define INIT_SUBSPACE_MAP_ENTRIES 1024
#define INIT_STRING_TABLE_SIZE 4096
#define STRING_TABLE_DELTA (1024 * 500)

#define TEXT_SECTION 0
#define DATA_SECTION 1
#define DEBUG_SECTION 2
#define NOTE_SECTION 4
#define OTHER_SECTION 5

int string_table_limit;
int string_table_size;

int notestart;
int noteend;

struct hp_compilation_unit
{
  unsigned int name;        /* symbol strings offset */
  unsigned int language;    /* symbol strings offset */
  unsigned int product_id;  /* symbol strings offset */
  unsigned int version_id;  /* symbol strings offset */
  unsigned int reserved:31;
  unsigned int chunk_flag:1;
  unsigned int compile_time[2];
  unsigned int source_time[2];
};
  
void somread_add_comp_map_entry(objfile,
                                objname,
                                srcname,
                                dirname,
                                language)
  struct objfile *objfile;
  int objname;
  int srcname;
  int dirname;
  int language;
{
    char *s;
    
    /* RM: This should never happen */
    if (COMPMAP(objfile)->n_cr > COMPMAP(objfile)->max_cr) {
        error("Unexpected error while building compilation map"); 
    }

    /* which language is this? */
    s=STRINGS(objfile)+language;
    if (!strcmp(s,"HPC"))
        COMPMAP(objfile)->cr[COMPMAP(objfile)->n_cr].lang = language_c;
    else if (!strcmp(s,"ANSI C++"))
        COMPMAP(objfile)->cr[COMPMAP(objfile)->n_cr].lang = language_cplus;
    else
        COMPMAP(objfile)->cr[COMPMAP(objfile)->n_cr].lang = language_unknown;
        
    COMPMAP(objfile)->cr[COMPMAP(objfile)->n_cr].objname = objname;
    COMPMAP(objfile)->cr[COMPMAP(objfile)->n_cr].srcname = srcname;
    COMPMAP(objfile)->cr[COMPMAP(objfile)->n_cr].dirname = dirname;

    (COMPMAP(objfile)->n_cr)++;
}

static struct subspace_to_cr
{
  int name;
  int objid;
} * subspace_to_cr = 0;

static int subspace_to_cr_index = 0;

void
somread_build_compile_map(objfile, notes_buf, notes_size,
                          space_strings_size)
  struct objfile *objfile;
  char *notes_buf;
  bfd_size_type notes_size;
  int space_strings_size;
{
  struct comp_map *tmp;
  long i;
  int l, n;
  char *s;
  long size;
  struct note *note;
  char *desc;
  int language;
  int objname_idx;
  int notemapentry;
  struct hp_compilation_unit *comp_units;
  struct hp_compilation_unit cur_comp_unit;
  unsigned int src_filename, comp_dirname;
  char *dirstring;

  if (debug_traces)
    printf("\nBuilding compile map...\n");

  subspace_to_cr = xmalloc (sizeof (struct subspace_to_cr) *
                                   SUBSPACEMAP (objfile)->n_sr);
  for (i=0; i < SUBSPACEMAP (objfile)->n_sr; i++)
    subspace_to_cr[i].name = subspace_to_cr[i].objid = 0;

  subspace_to_cr_index = 0;
  
  n = notes_size/sizeof(struct hp_compilation_unit);
  /* Use the same allocation pool as in elfread.c */ 
  COMPMAP(objfile) =
    (struct comp_map *) xmalloc (sizeof(struct comp_map) +
                                         sizeof(struct cr) * (n-1));
  COMPMAP(objfile)->max_cr = n;
  COMPMAP(objfile)->n_cr = 0;

  comp_units = (struct hp_compilation_unit *) notes_buf;

  notemapentry = notestart;
  for (i=0; i<n; i++)
    {
      cur_comp_unit = comp_units[i];

      while ((notemapentry <= noteend) &&
             (SUBSPACEMAP(objfile)->sr[notemapentry].end <=
                i*sizeof(struct hp_compilation_unit)))
        notemapentry++;
      if (notemapentry > noteend)
         /* RM: Can happen, there's no guarantee that there is a subspace
          * entry for non-DOOM files
          */
        break;
      objname_idx = SUBSPACEMAP(objfile)->sr[notemapentry].objid;

      /* srikanth, let us establish the correspondence between
         subspace map and compilation unit record map right now
         rather than wait until later. Postponing it results in
         a quadratic algorithm in somread_complete_subspace_map.
         We still have some work there ...
      */

      if (SUBSPACEMAP (objfile)->sr[notemapentry].real_id == -1)
        {
          SUBSPACEMAP (objfile)->sr[notemapentry].real_id = i;
          subspace_to_cr[subspace_to_cr_index].name = objname_idx;
          subspace_to_cr[subspace_to_cr_index].objid = i;
          subspace_to_cr_index++;
        }

      src_filename = space_strings_size+cur_comp_unit.name;
      language = space_strings_size+cur_comp_unit.language;

      /* Adjust source file name */
      s = STRINGS(objfile) + src_filename;
      while (*s && *s != '\n') 
        s++;

      if (*s == '\n')
        {
          *s = 0;
          
          if (*(s+1) == '/')
            {
              comp_dirname = s-STRINGS(objfile)+1;
              s++;
              while (*s && *s != '\n' && *s != ' ' && *s != '\t') 
                s++;
              if (*s)
                *s = 0;
            }
          else
            {
              comp_dirname = s-STRINGS(objfile);
            }
        }
      else
        {
          comp_dirname = s-STRINGS(objfile);
        }
      
      dirstring = STRINGS(objfile) + comp_dirname;
      if (!(*dirstring))
        dirstring = "Unknown compilation directory";
      
      if (debug_traces >= 1000) {
        printf("  %s\n", STRINGS(objfile)+objname_idx);
        printf("    %s\n", STRINGS(objfile)+src_filename);
        printf("    %s\n", dirstring);
        printf("    %s\n", STRINGS(objfile)+language);
      }

      
      somread_add_comp_map_entry(objfile,
                                 objname_idx,
                                 (int)src_filename,
                                 (int)comp_dirname,
                                 language);
    }
      
  if (debug_traces > 1)
    printf("...done\n");
}  

void
somread_dump_compile_map(objfile)
    struct objfile *objfile;
{
    int i;
    char *language;
    char *dirname;

    printf("Compile map for %s:\n", objfile->name);
    for (i = 0; i < COMPMAP(objfile)->n_cr; i++) {
        if (COMPMAP(objfile)->cr[i].lang == language_c)
            language = "C";
        else if (COMPMAP(objfile)->cr[i].lang == language_cplus)
            language = "C++";
        else
            language = "Unknown language";

        dirname = STRINGS(objfile)+COMPMAP(objfile)->cr[i].dirname;
        if (!(*dirname))
          dirname = "Unknown compilation directory";

        printf("  %s\n    %s\n    %s\n    %s\n",
               STRINGS(objfile)+COMPMAP(objfile)->cr[i].objname,
               STRINGS(objfile)+COMPMAP(objfile)->cr[i].srcname,
               dirname,
               language);
    }        
}

void somread_add_partial_subspace_map_entry(objfile,
                                            spacename,
                                            subspacename,
                                            objname,
                                            start,
                                            end,
                                            info)
  struct objfile *objfile;
  unsigned int spacename;
  unsigned int subspacename;
  int objname;
  CORE_ADDR start;
  CORE_ADDR end;
  int info;
{
    char *s;
    int n;

    /* RM: Try to build maximal subspace entries */
    if ((SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr-1].spacename ==
         spacename) &&
        (SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr-1].subspacename ==
         subspacename) &&
        (SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr-1].objid ==
         objname) &&
        (SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr-1].info ==
         info) &&
        (SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr-1].end == start))
      {
        SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr-1].end = end;
        return;
      }
    
    if (SUBSPACEMAP(objfile)->n_sr >= SUBSPACEMAP(objfile)->max_sr) {
      n = 2*SUBSPACEMAP(objfile)->max_sr;
      SUBSPACEMAP(objfile) = 
        (struct subspace_map *) xrealloc(SUBSPACEMAP(objfile),
                                         sizeof(struct subspace_map) +
                                         sizeof(struct sr) * (n-1));
      SUBSPACEMAP(objfile)->max_sr = n;
    }

    SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr].spacename =
      spacename;

    SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr].subspacename =
      subspacename;
    /* RM: hack: we'll later replace objname with an objid (an index
     * into the compilation map) */
    SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr].objid = objname;

    /* srikanth, hack on top of a hack. Preserve the real object id
       as soon as we know it. Right now we don't. */
    SUBSPACEMAP (objfile)->sr[SUBSPACEMAP (objfile)->n_sr].real_id = -1;
    SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr].start = start;
    SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr].end = end;
    SUBSPACEMAP(objfile)->sr[SUBSPACEMAP(objfile)->n_sr].info = info;

    (SUBSPACEMAP(objfile)->n_sr)++;
}

/* compare two subspace entries, first by section type, and then by
 * starting offset. */
int somread_compare_subspace_entries(s1, s2)
    struct sr *s1, *s2;
{
    if (s1->info < s2->info)
        return -1;
    else if (s1->info > s2->info)
        return 1;
    else if (s1->start < s2->start)
        return -1;
    else if (s1->start > s2->start)
        return 1;
    else
        return 0;
}

char *linkmap_buf;
char *linkmap_bss_buf;
char *linkmap_file_buf;
int linkmap_size;
int linkmap_bss_size;
int linkmap_file_size;
int linkmap_next_byte;
int linkmap_bss_next_byte;
int linkmap_file_next_byte;

void
somread_build_partial_subspace_map(objfile, abfd,
                                   linkmap_strings,
                                   type_table_size,
                                   const_table_size)
  struct objfile *objfile;
  bfd *abfd;
  char *linkmap_strings;
  bfd_size_type type_table_size;
  bfd_size_type const_table_size;
{
  CORE_ADDR start, end;
  int i;
  int space, spacename = 0;
  int subspace = 0, subspacename = 0;
  int objname;
  unsigned int l, m, n, p;
  int textstart, datastart, textend, dataend;
  int debugstart, debugend;
  CORE_ADDR subspace_start;
  CORE_ADDR text_offset, data_offset;
  char *current_space;
  char *current_subspace;
  int info;
  LM_mapping_info subspace_map_record;
  LM_sym_info bss_map_record;
  LM_file_entry_info objinfo;
  LM_errcode err;  
  int *file_info_to_name;
  struct space_dictionary_record *spacedict;
  struct subspace_dictionary_record *subspacedict;
  struct symbol_dictionary_record *syms;
  unsigned int number_of_symbols;
  struct subspace_map *tmp;
  
  text_offset = ANOFFSET(objfile->section_offsets, 0);
  data_offset = ANOFFSET (objfile->section_offsets, 1);

  if (debug_traces)
    printf("Building partial subspace map...\n");

  n = INIT_SUBSPACE_MAP_ENTRIES;
  SUBSPACEMAP(objfile) = (struct subspace_map *)
                           xmalloc(sizeof(struct subspace_map) +
                                   sizeof(struct sr) * (n-1));
  SUBSPACEMAP(objfile)->max_sr = n;
  SUBSPACEMAP(objfile)->n_sr = 0;

  /* RM: convert file info entries to old style DOOM file  names */
  err = LM_get_num_files(&m);
  if (err)
    error("Reading file info from link map\n");
  file_info_to_name = (int *) xmalloc(m*sizeof(int));
  for (i=0; i < m; i++)
    {    
      err = LM_get_file_entry(i, &objinfo);
      if (err)
        error("Reading file info from link map\n");
      if (!objinfo.is_archive)
        {
          /* RM: non-archive case */
          
          l = 0;
          if ((objinfo.is_basename &&
               (*(linkmap_strings+objinfo.dir_name) != '/')) ||
              (!objinfo.is_basename &&
               (*(linkmap_strings+objinfo.file_name) != '/')))
            l += strlen(linkmap_strings + objinfo.link_dir) + 1;
          if (objinfo.is_basename)
            l += strlen(linkmap_strings + objinfo.dir_name) + 1;
          l += strlen(linkmap_strings + objinfo.file_name) + 1;

          /* RM: make sure we have enough space in the strings table
           * for this filename */
          
          if (STRINGS(objfile) == NULL)
            {
              p = (l > INIT_STRING_TABLE_SIZE) ? l :
                INIT_STRING_TABLE_SIZE;
              STRINGS(objfile) = xmalloc(p);
              if (!STRINGS(objfile))
                error("Out of memory\n");
              string_table_limit = 0;
              string_table_size = p;
            }
          else if (string_table_limit + l > string_table_size)
            {
              p = (l > string_table_size) ?
                string_table_size + l :
                string_table_size + STRING_TABLE_DELTA;
              STRINGS(objfile) = xrealloc(STRINGS(objfile), p);
              if (!STRINGS(objfile))
                error("Out of memory\n");
              string_table_size = p;
            }

          file_info_to_name[i] = string_table_limit;
          if ((objinfo.is_basename &&
               (*(linkmap_strings+objinfo.dir_name) != '/')) ||
              (!objinfo.is_basename &&
               (*(linkmap_strings+objinfo.file_name) != '/')))
            {
              strcpy(STRINGS(objfile) + string_table_limit,
                     linkmap_strings + objinfo.link_dir);
              string_table_limit +=
                strlen(linkmap_strings + objinfo.link_dir)+1;
              *(STRINGS(objfile) + string_table_limit - 1) = '/';
            }
          if (objinfo.is_basename)
            {
              strcpy(STRINGS(objfile) + string_table_limit,
                     linkmap_strings + objinfo.dir_name);
              string_table_limit +=
                strlen(linkmap_strings + objinfo.dir_name)+1;
              *(STRINGS(objfile) + string_table_limit - 1) = '/';
            }
          strcpy(STRINGS(objfile) + string_table_limit,
                 linkmap_strings + objinfo.file_name);
          string_table_limit +=
            strlen(linkmap_strings + objinfo.file_name)+1;

          if (debug_traces >= 10000)
            printf("Constructed strings entry for file: %s\n",
                   STRINGS(objfile) + file_info_to_name[i]);
        }
      else
        {
          /* RM: archive case */
          l = 0;
          if (*(linkmap_strings+objinfo.dir_name) != '/')
            l += strlen(linkmap_strings + objinfo.link_dir) + 1;
          l += strlen(linkmap_strings + objinfo.dir_name);
          l += strlen(linkmap_strings + objinfo.file_name) + 3;

          /* RM: make sure we have enough space in the strings table
           * for this filename */
          
          if (STRINGS(objfile) == NULL)
            {
              p = (l > INIT_STRING_TABLE_SIZE) ? l :
                INIT_STRING_TABLE_SIZE;
              STRINGS(objfile) = xmalloc(p);
              if (!STRINGS(objfile))
                error("Out of memory\n");
              string_table_limit = 0;
              string_table_size = p;
            }
          else if (string_table_limit + l > string_table_size)
            {
              p = (l > string_table_size) ?
                string_table_size + l :
                string_table_size + STRING_TABLE_DELTA;
              STRINGS(objfile) = xrealloc(STRINGS(objfile), p);
              if (!STRINGS(objfile))
                error("Out of memory\n");
              string_table_size = p;
            }

          file_info_to_name[i] = string_table_limit;
          if (*(linkmap_strings+objinfo.dir_name) != '/')
            {
              strcpy(STRINGS(objfile) + string_table_limit,
                     linkmap_strings + objinfo.link_dir);
              string_table_limit +=
                strlen(linkmap_strings + objinfo.link_dir)+1;
              *(STRINGS(objfile) + string_table_limit - 1) = '/';
            }
          strcpy(STRINGS(objfile) + string_table_limit,
                 linkmap_strings + objinfo.dir_name);
          string_table_limit +=
            strlen(linkmap_strings + objinfo.dir_name)+1;
          *(STRINGS(objfile) + string_table_limit - 1) = '(';
          strcpy(STRINGS(objfile) + string_table_limit,
                 linkmap_strings + objinfo.file_name);
          string_table_limit +=
            strlen(linkmap_strings + objinfo.file_name)+2;
          *(STRINGS(objfile) + string_table_limit - 2) = ')';
          *(STRINGS(objfile) + string_table_limit - 1) = 0;

          if (debug_traces >= 10000)
            printf("Constructed strings entry for file: %s\n",
                   STRINGS(objfile) + file_info_to_name[i]);

        }
    }

  /* RM: gross stuff to pull in space and subspace dictionaries
   */
  spacedict = xmalloc (obj_som_spacedict_total(abfd)*
                       sizeof(struct space_dictionary_record));
  if (!spacedict && obj_som_spacedict_total(abfd) != 0)
    error("can't read space dictonary");

  if (bfd_seek (abfd, obj_som_spacedict_filepos(abfd),
                SEEK_SET) < 0)
    error("can't read space dictionary");
          
  if (bfd_read (spacedict, 1,
                  obj_som_spacedict_total(abfd)*
                  sizeof(struct space_dictionary_record),
                  abfd) !=
      obj_som_spacedict_total(abfd)*
      sizeof(struct space_dictionary_record))
    error("can't read space dictionary");

  subspacedict = xmalloc (obj_som_subspacedict_total(abfd)*
                          sizeof(struct subspace_dictionary_record));
  if (!subspacedict && obj_som_subspacedict_total(abfd) != 0)
    error("can't read subspace dictonary");

  if (bfd_seek (abfd, obj_som_subspacedict_filepos(abfd),
                SEEK_SET) < 0)
    error("can't read subspace dictionary");
  
  if (bfd_read (subspacedict, 1,
                obj_som_subspacedict_total(abfd)*
                sizeof(struct subspace_dictionary_record),
                abfd) !=
      obj_som_subspacedict_total(abfd)*
      sizeof(struct subspace_dictionary_record))
    error("can't read subspace dictionary");

  while (1)
    {
      err = LM_get_next_mapping(&subspace_map_record);
      /* RM: ??? A particular error code indicates no more
       * entries. Don't know which, so just break on all of them. Check
       * with Dmitry, so we can detect problems other than end of
       * entries
       */
      if (err)
        break;
      
      /* RM: not interested in things other than input sections,
       * linker bss's and compilation unit entries */
      if ((subspace_map_record.mapping_type != LM_INPUT_SECTION) &&
          (subspace_map_record.mapping_type != LM_DATA_STRUCTURE) &&
          (subspace_map_record.mapping_type != LM_LINKER_BSS))
        continue;

      if ((subspace_map_record.mapping_type == LM_DATA_STRUCTURE) &&
          (subspace_map_record.output_info !=
           DATA_STRUCT_COMPILATION_UNIT_DICT))
        continue;


      if ((subspace_map_record.mapping_type == LM_INPUT_SECTION) ||
          (subspace_map_record.mapping_type == LM_LINKER_BSS))
        {
          subspace = subspace_map_record.output_info;
          subspacename = subspacedict[subspace].name.n_strx;
          space = subspacedict[subspace].space_index;
          spacename = spacedict[space].name.n_strx;
        }
      objname=file_info_to_name[subspace_map_record.file_index];

      start=subspace_map_record.offset;
      end=start+subspace_map_record.size;

      current_space = STRINGS(objfile)+spacename;
      current_subspace = STRINGS(objfile)+subspacename;

      if ((subspace_map_record.mapping_type == LM_INPUT_SECTION) &&
          (STREQSPACE(current_space, "$TEXT$")) &&
          (!STREQSPACE(current_subspace, "$LIT$")))
        {          
          info = TEXT_SECTION;
          subspace_start = subspacedict[subspace].subspace_start;
          start += subspace_start;
          end += subspace_start;
        }
      else if (((subspace_map_record.mapping_type == LM_INPUT_SECTION) ||
                (subspace_map_record.mapping_type == LM_LINKER_BSS)) &&
               ((STREQSPACE(current_space, "$PRIVATE$") ||
		(STREQSPACE(current_space, "$THREAD_SPECIFIC$")))))
        {
          info = DATA_SECTION;
          subspace_start = subspacedict[subspace].subspace_start;
          start += subspace_start;
          end += subspace_start;
        }
      else if ((subspace_map_record.mapping_type == LM_INPUT_SECTION) &&
               (STREQSPACE(current_space, "$OBJDEBUG$")) &&
               (STREQSPACE(current_subspace, "$TYPE$")))
        {
          info = DEBUG_SECTION;
        }
      else if ((subspace_map_record.mapping_type == LM_INPUT_SECTION) &&
               (STREQSPACE(current_space, "$OBJDEBUG$")) &&
               (STREQSPACE(current_subspace, "$CONST$")))
        {
          info = DEBUG_SECTION;
          start += type_table_size;
          end += type_table_size;
        }
      else if ((subspace_map_record.mapping_type == LM_INPUT_SECTION) &&
               (STREQSPACE(current_space, "$OBJDEBUG$")) &&
               (STREQSPACE(current_subspace, "$FILE$")))
        {
          info = DEBUG_SECTION;
          start += type_table_size + const_table_size;
          end += type_table_size + const_table_size;
        }
      else if ((subspace_map_record.mapping_type == LM_DATA_STRUCTURE) &&
               (subspace_map_record.output_info ==
                DATA_STRUCT_COMPILATION_UNIT_DICT))
        {
          spacename = -1;
          subspacename = -1;
          info = NOTE_SECTION;
        }
      else
        {
          info = OTHER_SECTION;
        }
      
      if (debug_traces >= 1000) {
        printf("  %s [%016llx-%016llx]: %s\n",
               (subspacename == -1) ?
                 "Compilation unit records" :
                 STRINGS(objfile)+subspacename,
               (long long) start,
               (long long) end,
               STRINGS(objfile)+objname);
      }
      somread_add_partial_subspace_map_entry(objfile,
                                             (unsigned int)spacename,
                                             (unsigned int)subspacename,
                                             objname,
                                             (CORE_ADDR) start,
                                             (CORE_ADDR) end,
                                             info);

    }

  n = SUBSPACEMAP(objfile)->n_sr;
  
  /* Now sort the subspace map so we can perform binary searches on it */
  qsort(&(SUBSPACEMAP(objfile)->sr[0]), n, sizeof(struct sr),
        (int (*)(const void *,const void *))somread_compare_subspace_entries);

  /* Identify text and data portions of the subspace map, so we have
   * something to perform binary searches on. Also patch virtal
   * addresses with section offsets
   */
  textstart = textend = datastart = dataend = -1;
  debugstart = debugend = -1;
  notestart = noteend = -1;
  for (i=0; i< n; i++)
    {
      if (SUBSPACEMAP(objfile)->sr[i].info == TEXT_SECTION)
        {
          if (textstart == -1)
            textstart = i;
          textend = i;
          SUBSPACEMAP(objfile)->sr[i].start += text_offset;
          SUBSPACEMAP(objfile)->sr[i].end += text_offset;
        }
      else if (SUBSPACEMAP(objfile)->sr[i].info == DATA_SECTION)
        {
          if (datastart == -1)
            datastart = i;
          dataend = i;
          SUBSPACEMAP(objfile)->sr[i].start += data_offset;
          SUBSPACEMAP(objfile)->sr[i].end += data_offset;
        }
      else if (SUBSPACEMAP(objfile)->sr[i].info == DEBUG_SECTION)
        {
          if (debugstart == -1)
            debugstart = i;
          debugend = i;
        }
      else if (SUBSPACEMAP(objfile)->sr[i].info == NOTE_SECTION)
        {
          if (notestart == -1)
            notestart = i;
          noteend = i;
        }
    }
    
  SUBSPACEMAP(objfile)->textstart = textstart;
  SUBSPACEMAP(objfile)->textend = textend;
  SUBSPACEMAP(objfile)->datastart = datastart;
  SUBSPACEMAP(objfile)->dataend = dataend;
  SUBSPACEMAP(objfile)->debugstart = debugstart;
  SUBSPACEMAP(objfile)->debugend = debugend;

  free(file_info_to_name);
  free(subspacedict);
  free(spacedict);
  
  if (debug_traces > 1)
    printf("...done\n");
}

char *som_linkmap_buffer(size_p)
  unsigned int *size_p;
{
  if (linkmap_next_byte == 0)
    {
      *size_p = linkmap_size;
      linkmap_next_byte = linkmap_size;
      return linkmap_buf;
    }
  else
    {
      *size_p = 0;
      return 0;
    }
}

char *som_linkmap_bss_buffer(size_p)
  unsigned int *size_p;
{
  if (linkmap_bss_next_byte == 0)
    {
      *size_p = linkmap_bss_size;
      linkmap_bss_next_byte = linkmap_bss_size;
      return linkmap_bss_buf;
    }
  else
    {
      *size_p = 0;
      return 0;
    }
}

char *som_linkmap_file_buffer(size_p)
  unsigned int *size_p;
{
  if (linkmap_file_next_byte == 0)
    {
      *size_p = linkmap_file_size;
      linkmap_file_next_byte = linkmap_file_size;
      return linkmap_file_buf;
    }
  else
    {
      *size_p = 0;
      return 0;
    }
}

static int
compare_subspace_to_cr_entries (const void *p1, const void *p2)
{
  struct subspace_to_cr * s1, *s2;

  s1 = (struct subspace_to_cr *) p1;
  s2 = (struct subspace_to_cr *) p2;

  if (s1->name < s2->name)
    return -1;
  else
  if (s1->name > s2->name)
    return 1;

  return 0;
}

/* replace objnames (offset into strings table) with objids (index
 compilation map)
 */
void
somread_complete_subspace_map(objfile)
  struct objfile *objfile;
{
  int i;
  struct subspace_to_cr key, *result;

  /* srikanth, 000207, we spend a significant amount of the scp engine
     startup time here. This loop is simplified now.
  */

  qsort (subspace_to_cr, subspace_to_cr_index,
         sizeof (struct subspace_to_cr),
         (int (*)(const void *, const void *))compare_subspace_to_cr_entries);
                       
  for (i = 0; i < SUBSPACEMAP (objfile)->n_sr; i++)
    {
      /* Do we know which cr this subspace maps to ? */
      if (SUBSPACEMAP (objfile)->sr[i].real_id != -1)
        SUBSPACEMAP (objfile)->sr[i].objid = 
                           SUBSPACEMAP (objfile)->sr[i].real_id;
      else 
        {
          key.name = SUBSPACEMAP (objfile)->sr[i].objid;
          result = bsearch (&key, subspace_to_cr,
                                  subspace_to_cr_index,
                                  sizeof (struct subspace_to_cr),
                                  compare_subspace_to_cr_entries);
          if (result)
            SUBSPACEMAP (objfile)->sr[i].objid = result->objid;
          else 
            SUBSPACEMAP (objfile)->sr[i].objid = -1;
        }
    }

  free (subspace_to_cr);
  subspace_to_cr_index = 0;
}

void
somread_print_subspace_map_entry(objfile, i)
    struct objfile *objfile;
    int i;
{
    printf("    %s::%s [%08lx-%08lx]: %s\n",
           (SUBSPACEMAP(objfile)->sr[i].spacename == -1) ?
           "" :
           STRINGS(objfile)+SUBSPACEMAP(objfile)->sr[i].spacename,
           (SUBSPACEMAP(objfile)->sr[i].subspacename == -1) ?
           "Compilation unit records" :
           STRINGS(objfile)+SUBSPACEMAP(objfile)->sr[i].subspacename,
           SUBSPACEMAP(objfile)->sr[i].start,
           SUBSPACEMAP(objfile)->sr[i].end,
           (SUBSPACEMAP(objfile)->sr[i].objid == -1) ?
           "" :
           STRINGS(objfile)+
           (COMPMAP(objfile)->cr[SUBSPACEMAP(objfile)->sr[i].objid].objname));
}


void
somread_dump_subspace_map(objfile)
    struct objfile *objfile;
{
    int i;

    printf("Subspace map for %s:\n", objfile->name);

    printf("  Text space map:\n");
    /* First print map for text space */
    for (i = SUBSPACEMAP(objfile)->textstart;
         i <= SUBSPACEMAP(objfile)->textend;
         i++) {
        somread_print_subspace_map_entry(objfile, i);
    }

    printf("\n  Data space map:\n");
    /* then print map for data space */
    for (i = SUBSPACEMAP(objfile)->datastart;
         i <= SUBSPACEMAP(objfile)->dataend;
         i++) {
        somread_print_subspace_map_entry(objfile, i);
    }

    printf("\n  Space map for remaining spaces:\n");
    /* then the rest */
    for (i = 0; i < SUBSPACEMAP(objfile)->n_sr; i++) {
        if ((i >= SUBSPACEMAP(objfile)->textstart) &&
            (i <= SUBSPACEMAP(objfile)->textend))
            continue;
        if ((i >= SUBSPACEMAP(objfile)->datastart) &&
            (i <= SUBSPACEMAP(objfile)->dataend))
            continue;
        somread_print_subspace_map_entry(objfile, i);
    }
}

void
somread_build_doom_tables(objfile)
    struct objfile *objfile;
{
  extern asection *
    bfd_get_section_by_name_and_id (bfd *abfd, const char *name, int id);

  bfd *abfd;
  asection *notes, *section_strings;
  bfd_size_type notes_size, section_strings_size;
  asection *linkmap_strings, *linkmap, *linkmap_bss, *linkmap_file;
  char *linkmap_strings_buf = 0;
  bfd_size_type linkmap_strings_size;
  char *notes_buf = 0;
  int sectionname;
  char *tmp;
  
  asection *doom_type_table;
  bfd_size_type doom_type_table_size = 0;
  asection *doom_const_table;
  bfd_size_type doom_const_table_size = 0;
  char *buf8, *buf9, *buf10;

  LM_errcode err;
  
  SYMFILE_HT(objfile) = NULL;
  abfd = symfile_bfd_open (objfile->name);

  TYPETABLEMAP(objfile).table = NULL;
  CONSTTABLEMAP(objfile).table = NULL;

  doom_type_table = bfd_get_section_by_name (abfd, "$TYPE$");
  if (doom_type_table) {
    doom_type_table_size = bfd_section_size (abfd, doom_type_table);
    if (doom_type_table_size > 0) {
      buf8 = mmap(0, doom_type_table_size,
		  PROT_READ | PROT_WRITE,
		  MAP_ANONYMOUS | MAP_PRIVATE,
		  -1,
		  0); 

      if (buf8 == MAP_FAILED)
	error ("Out of memory");

      if (!bfd_get_section_contents(abfd,
                                    doom_type_table,
                                    buf8, 0,
                                    doom_type_table_size))
        error("bfd_get_section_contents\n");
      TYPETABLEMAP(objfile).table = buf8;
      TYPETABLEMAP(objfile).table_size = doom_type_table_size;
      TYPETABLEMAP(objfile).subspacemap_base = 0;
    }
  }
  
  doom_const_table = bfd_get_section_by_name (abfd, "$CONST$");
  if (doom_const_table) {
    doom_const_table_size = bfd_section_size (abfd, doom_const_table);
    if (doom_const_table_size > 0) {
      buf9 = mmap(0, doom_const_table_size,
		  PROT_READ | PROT_WRITE,
		  MAP_ANONYMOUS | MAP_PRIVATE,
		  -1,
		  0); 

      if (buf9 == MAP_FAILED)
	error ("Out of memory");

      if (!bfd_get_section_contents(abfd,
                                    doom_const_table,
                                    buf9, 0,
                                    doom_const_table_size))
        error("bfd_get_section_contents\n");
      CONSTTABLEMAP(objfile).table = buf9;
      CONSTTABLEMAP(objfile).table_size = doom_const_table_size;
      CONSTTABLEMAP(objfile).subspacemap_base = doom_type_table_size;
    }
  }

  /* RM: The note name is an artifact of the elf name for the section
   * where this info is put. For SOM this corresponds to the
   * compilation unit records
   */
  notes_buf = xmalloc (obj_som_comp_units_total(abfd)*
                       sizeof(struct hp_compilation_unit));
  if (!notes_buf && obj_som_comp_units_total(abfd) != 0)
    error("can't read compilation units table");

  if (bfd_seek (abfd, obj_som_comp_units_filepos(abfd),
                SEEK_SET) < 0)
    error("can't read compilation units table");

  if (bfd_read (notes_buf, 1,
                obj_som_comp_units_total(abfd)*
                  sizeof(struct hp_compilation_unit),
                abfd) !=
      obj_som_comp_units_total(abfd)*
        sizeof(struct hp_compilation_unit))
    error("can't read compilation units table");


  /* Read in link map sections */
  /* RM: Both the space and the subspace we want have the name
   * $LINKMAP$. bfd creates two bfd sections with this name, one for
   * the space and one for the subspace. We want the subspace.
   */
  /* RM: ??? do this right */
  linkmap = bfd_get_section_by_name_and_id(abfd, "$LINKMAP$", 1);
  if (linkmap)
    {
      linkmap_size = bfd_section_size (abfd, linkmap);
      linkmap_buf = xmalloc(linkmap_size);
      if (!bfd_get_section_contents(abfd,
                                    linkmap,
                                    linkmap_buf,
                                    0,
                                    linkmap_size))
        error("bfd_get_section_contents\n");
    }
  else
    {
      linkmap_buf = 0;
      linkmap_size = 0;
    }
  
  linkmap_bss = bfd_get_section_by_name(abfd, "$LINKMAP_BSS$");
  if (linkmap_bss)
    {
      linkmap_bss_size = bfd_section_size (abfd, linkmap_bss);
      linkmap_bss_buf = xmalloc(linkmap_bss_size);
      if (!bfd_get_section_contents(abfd,
                                    linkmap_bss,
                                    linkmap_bss_buf,
                                    0,
                                    linkmap_bss_size))
        error("bfd_get_section_contents\n");
    }
  else
    {
      linkmap_bss_buf = 0;
      linkmap_bss_size = 0;
    }
  
  linkmap_file = bfd_get_section_by_name(abfd, "$LINKMAP_FILE$");
  if (linkmap_file)
    {
      linkmap_file_size = bfd_section_size (abfd, linkmap_file);
      linkmap_file_buf = xmalloc(linkmap_file_size);
      if (!bfd_get_section_contents(abfd,
                                    linkmap_file,
                                    linkmap_file_buf,
                                    0,
                                    linkmap_file_size))
        error("bfd_get_section_contents\n");
    }
  else
    {
      linkmap_file_buf = 0;
      linkmap_file_size = 0;
    }

  linkmap_strings = bfd_get_section_by_name(abfd, "$LINKMAP_STR$");
  if (linkmap_strings)
    {
      linkmap_strings_size = bfd_section_size (abfd, linkmap_strings);
      linkmap_strings_buf = xmalloc(linkmap_strings_size);
      if (!bfd_get_section_contents(abfd,
                                    linkmap_strings,
                                    linkmap_strings_buf,
                                    0,
                                    linkmap_strings_size))
        error("bfd_get_section_contents\n");
    }
  else
    {
      error ("No linkmap strings?");
    }

  linkmap_next_byte = 0;
  linkmap_bss_next_byte = 0;
  linkmap_file_next_byte = 0;
  
  if (linkmap_buf)
    {
      err = LM_begin_read_linkmap(som_linkmap_buffer);
      if (err)
        error("Error in reading linkmap information\n");
    }
  if (linkmap_bss_buf)
    {
      err = LM_begin_read_linkmap_bss(som_linkmap_bss_buffer);
      if (err)
        error("Error in reading linkmap information\n");
    }
  if (linkmap_file_buf)
    {
      err = LM_begin_read_linkmap_file(som_linkmap_file_buffer);
      if (err)
        error("Error in reading linkmap information\n");
    }
  
  /* RM: read space strings and symbol strings into one big strings table.
   */

  /* Allocated some extra room, so we don't realloc immediately. This
     realloc was causing two interesting things to happen : On SCP
     engine, the original allocation here is 13MB and we try to realloc
     double that amount when we have to grow this table. This is changed
     now so that we grow this table arithmetically rather than
     geometrically.

     Secondly, when we tried to allocate the 26MB buffer, the malloc
     package couldn't grow the original block and had to find a
     fresh 26 MB block, causing the footprint to go up by 39 MB !
     Now the extra room seems to suffice and we don't realloc at all.

     There is further scope for improvement here : Very little from
     this buffer gets used later on. We should be able to create
     persistent copies of those strings and blow away this entirely.

           -- srikanth, 000207
  */

  STRINGS(objfile) = xmalloc(obj_som_spacestringtab_size(abfd) +
			     obj_som_stringtab_size (abfd) + 
			     STRING_TABLE_DELTA);
  if (!STRINGS(objfile) && !((obj_som_spacestringtab_size(abfd) == 0) ||
                             (obj_som_spacestringtab_size(abfd) == 0)))
    error("can't read strings");

  if (bfd_seek (abfd, obj_som_spacestr_filepos(abfd),
                SEEK_SET) < 0)
    error("can't read space strings");
          
  if (bfd_read (STRINGS(objfile), 1, obj_som_spacestringtab_size(abfd),
                abfd)
        != obj_som_spacestringtab_size(abfd))
    error("can't read space strings");
          
  if (bfd_seek (abfd, obj_som_str_filepos(abfd),
                SEEK_SET) < 0)
    error("can't read symbol strings");
          
  if (bfd_read (STRINGS(objfile)+obj_som_spacestringtab_size(abfd), 1,
                obj_som_stringtab_size(abfd), abfd)
      != obj_som_stringtab_size(abfd))
    error("can't read symbol strings");

  string_table_size = STRING_TABLE_DELTA + 
    obj_som_spacestringtab_size(abfd) + obj_som_stringtab_size(abfd);

  string_table_limit = 
    obj_som_spacestringtab_size (abfd) + obj_som_stringtab_size (abfd);
  somread_build_partial_subspace_map(objfile, abfd, 
                                     linkmap_strings_buf,
                                     doom_type_table_size,
                                     doom_const_table_size);
  somread_build_compile_map(objfile, notes_buf,
                            (obj_som_comp_units_total(abfd)*
                            sizeof(struct hp_compilation_unit)),
                            (int)obj_som_spacestringtab_size(abfd));

  if (debug_traces >= 10)
    somread_dump_compile_map(objfile);

  somread_complete_subspace_map(objfile);

  if (debug_traces >= 10)
    somread_dump_subspace_map(objfile);

  free(notes_buf);

  LM_end_read_linkmap();
  LM_end_read_linkmap_bss();
  LM_end_read_linkmap_file();
  free(linkmap_buf);
  free(linkmap_bss_buf);
  free(linkmap_file_buf);
  free(linkmap_strings_buf);
  
  bfd_close(abfd);  
}


/* Read in and initialize the SOM export list which is present
   for all executables and shared libraries.  The import list
   consists of the symbols that are referenced in OBJFILE but
   not defined there.  (Variables that are imported are dealt
   with as "loc_indirect" vars.)
   Return value = number of import symbols read in. */
int
init_export_symbols (objfile)
     struct objfile *objfile;
{
  unsigned int export_list;
  unsigned int export_list_size;
  unsigned int string_table;
  unsigned int string_table_size;
  char *string_buffer;
  register int i;
  register int j;
  register int k;
  asection *text_section;	/* section handle */
  unsigned int dl_header[12];	/* SOM executable header */

  /* A struct for an entry in the SOM export list */
  typedef struct
    {
      int next;			/* for hash table use -- we don't use this */
      int name;			/* index into string table */
      int value;		/* offset or plabel */
      int dont_care1;		/* not used */
      unsigned char type;	/* 0 = NULL, 2 = Data, 3 = Code, 7 = Storage, 13 = Plabel */
      char dont_care2;		/* not used */
      short dont_care3;		/* not used */
    }
  SomExportEntry;

  /* We read 100 entries in at a time from the disk file. */
#define SOM_READ_EXPORTS_NUM         100
#define SOM_READ_EXPORTS_CHUNK_SIZE  (sizeof (SomExportEntry) * SOM_READ_EXPORTS_NUM)
  SomExportEntry buffer[SOM_READ_EXPORTS_NUM];

  /* Initialize in case we error out */
  objfile->export_list = NULL;
  objfile->export_list_size = 0;

  /* It doesn't work, for some reason, to read in space $TEXT$;
     the subspace $SHLIB_INFO$ has to be used.  Some BFD quirk? pai/1997-08-05 */

  /* I believe this is because a "section" in bfd terms refers to a 
   * subspace in SOM terms and further because SHLIB_INFO is the first 
   * subspace in $TEXT$ space  --- srikanth, 981007
   */

  text_section = bfd_get_section_by_name (objfile->obfd, "$SHLIB_INFO$");
  if (!text_section)
    return 0;
  /* Get the SOM executable header */
  bfd_get_section_contents (objfile->obfd, text_section, dl_header, 0, 12 * sizeof (int));

  /* Check header version number for 10.x HP-UX */
  /* Currently we deal only with 10.x systems; on 9.x the version # is 89060912.
     FIXME: Change for future HP-UX releases and mods to the SOM executable format */
  if (dl_header[0] != DL_HDR_VERSION_ID2)
    return 0;

  export_list = dl_header[8];
  export_list_size = dl_header[9];
  if (!export_list_size)
    return 0;
  string_table = dl_header[10];
  string_table_size = dl_header[11];
  if (!string_table_size)
    return 0;

  /* Suck in SOM string table */
  string_buffer = mmap(0, string_table_size,
                PROT_READ | PROT_WRITE,
	        MAP_ANONYMOUS | MAP_PRIVATE,
                -1,
                0); 

  if (string_buffer == MAP_FAILED)
    error ("Out of memory");

  bfd_get_section_contents (objfile->obfd, text_section, string_buffer,
			    string_table, string_table_size);

  /* Allocate export list in the psymbol obstack; this has nothing
     to do with psymbols, just a matter of convenience.  We want the
     export list to be freed when the objfile is deallocated */
  objfile->export_list
    = (ExportEntry *) obstack_alloc (&objfile->psymbol_obstack,
				   export_list_size * sizeof (ExportEntry));

  /* Read in the export entries, a bunch at a time */
  for (j = 0, k = 0;
       j < (export_list_size / SOM_READ_EXPORTS_NUM);
       j++)
    {
      bfd_get_section_contents (objfile->obfd, text_section, buffer,
			      export_list + j * SOM_READ_EXPORTS_CHUNK_SIZE,
				SOM_READ_EXPORTS_CHUNK_SIZE);
      for (i = 0; i < SOM_READ_EXPORTS_NUM; i++, k++)
	{
	  if (buffer[i].type != (unsigned char) 0)
	    {
	      objfile->export_list[k].name
		= (char *) obstack_alloc (&objfile->psymbol_obstack, strlen (string_buffer + buffer[i].name) + 1);
	      strcpy (objfile->export_list[k].name, string_buffer + buffer[i].name);
	      objfile->export_list[k].address = buffer[i].value;
	      /* Some day we might want to record the type and other information too */
	    }
	  else
	    /* null type */
	    {
	      objfile->export_list[k].name = NULL;
	      objfile->export_list[k].address = 0;
	    }
	}
    }

  /* Get the leftovers */
  if (k < export_list_size)
    bfd_get_section_contents (objfile->obfd, text_section, buffer,
			      export_list + k * sizeof (SomExportEntry),
			  (export_list_size - k) * sizeof (SomExportEntry));
  for (i = 0; k < export_list_size; i++, k++)
    {
      if (buffer[i].type != (unsigned char) 0)
	{
	  objfile->export_list[k].name
	    = (char *) obstack_alloc (&objfile->psymbol_obstack, strlen (string_buffer + buffer[i].name) + 1);
	  strcpy (objfile->export_list[k].name, string_buffer + buffer[i].name);
	  /* Some day we might want to record the type and other information too */
	  objfile->export_list[k].address = buffer[i].value;
	}
      else
	{
	  objfile->export_list[k].name = NULL;
	  objfile->export_list[k].address = 0;
	}
    }

  objfile->export_list_size = export_list_size;
  munmap (string_buffer, string_table_size);
  return export_list_size;
}


struct reloc_data
{
  unsigned char type;
  char *name;
  char length;
  char d;
};

/* need to do this for compiles on non PA2.0 machines */
#ifndef R_SHORT_PCREL_MODE
#define R_SHORT_PCREL_MODE 62
#endif
#ifndef R_LONG_PCREL_MODE
#define R_LONG_PCREL_MODE  63
#endif

#ifndef R_TP_OVERRIDE
/* Defined in <reloc.h> as of HP-UX 11.00. */
#define R_TP_OVERRIDE     0xde  /* de:     thread pointer override */
#endif

struct reloc_data relocs[256] =
{/* 000 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  0},
 /* 001 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  1},    
 /* 002 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  2},    
 /* 003 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  3},    
 /* 004 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  4},    
 /* 005 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  5},    
 /* 006 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  6},    
 /* 007 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  7},    
 /* 008 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  8},    
 /* 009 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1,  9},    
 /* 010 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 10},    
 /* 011 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 11},    
 /* 012 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 12},    
 /* 013 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 13},    
 /* 014 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 14},    
 /* 015 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 15},    
 /* 016 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 16},    
 /* 017 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 17},    
 /* 018 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 18},    
 /* 019 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 19},    
 /* 020 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 20},    
 /* 021 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 21},    
 /* 022 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 22},    
 /* 023 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      1, 23},    
 /* 024 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      2,  0},    
 /* 025 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      2,  1},    
 /* 026 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      2,  2},    
 /* 027 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      2,  3},    
 /* 028 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      3,  0},    
 /* 029 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      3,  1},    
 /* 030 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      3,  2},    
 /* 031 */ {R_NO_RELOCATION,       "R_NO_RELOCATION",      4,  0},    
 /* 032 */ {R_ZEROES,              "R_ZEROES",             2,  0},    
 /* 033 */ {R_ZEROES,              "R_ZEROES",             4,  0},    
 /* 034 */ {R_UNINIT,              "R_UNINIT",             2,  0},    
 /* 035 */ {R_UNINIT,              "R_UNINIT",             4,  0},    
 /* 036 */ {R_RELOCATION,          "R_RELOCATION",         1,  0},    
 /* 037 */ {R_DATA_ONE_SYMBOL,     "R_DATA_ONE_SYMBOL",    2,  0},    
 /* 038 */ {R_DATA_ONE_SYMBOL,     "R_DATA_ONE_SYMBOL",    4,  0},
 /* 039 */ {R_DATA_PLABEL,         "R_DATA_PLABEL",        2,  0},
 /* 040 */ {R_DATA_PLABEL,         "R_DATA_PLABEL",        4,  0},
 /* 041 */ {R_SPACE_REF,           "R_SPACE_REF",          1,  0},
 /* 042 */ {R_REPEATED_INIT,       "R_REPEATED_INIT",      2,  0},
 /* 043 */ {R_REPEATED_INIT,       "R_REPEATED_INIT",      3,  0},
 /* 044 */ {R_REPEATED_INIT,       "R_REPEATED_INIT",      5,  0},
 /* 045 */ {R_REPEATED_INIT,       "R_REPEATED_INIT",      8,  0},
 /* 046 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 047 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 048 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  0},
 /* 049 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  1},
 /* 050 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  2},
 /* 051 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  3},
 /* 052 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  4},
 /* 053 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  5},
 /* 054 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  6},
 /* 055 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  7},
 /* 056 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  8},
 /* 057 */ {R_PCREL_CALL,          "R_PCREL_CALL",         2,  9},
 /* 058 */ {R_PCREL_CALL,          "R_PCREL_CALL",         3,  0},
 /* 059 */ {R_PCREL_CALL,          "R_PCREL_CALL",         3,  1},
 /* 060 */ {R_PCREL_CALL,          "R_PCREL_CALL",         5,  0},
 /* 061 */ {R_PCREL_CALL,          "R_PCREL_CALL",         5,  1},
 /* 062 */ {R_SHORT_PCREL_MODE,    "R_SHORT_PCREL_MODE",   1,  0},
 /* 063 */ {R_LONG_PCREL_MODE,     "R_LONG_PCREL_MODE",    1,  0},
 /* 064 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  0},
 /* 065 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  1},
 /* 066 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  2},
 /* 067 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  3},
 /* 068 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  4},
 /* 069 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  5},
 /* 070 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  6},
 /* 071 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  7},
 /* 072 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  8},
 /* 073 */ {R_ABS_CALL,            "R_ABS_CALL",           2,  9},
 /* 074 */ {R_ABS_CALL,            "R_ABS_CALL",           3,  0},
 /* 075 */ {R_ABS_CALL,            "R_ABS_CALL",           3,  1},
 /* 076 */ {R_ABS_CALL,            "R_ABS_CALL",           5,  0},
 /* 077 */ {R_ABS_CALL,            "R_ABS_CALL",           5,  1},
 /* 078 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 079 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 080 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  0},
 /* 081 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  1},
 /* 082 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  2},
 /* 083 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  3},
 /* 084 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  4},
 /* 085 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  5},
 /* 086 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  6},
 /* 087 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  7},
 /* 088 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  8},
 /* 089 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1,  9},
 /* 090 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 10},
 /* 091 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 11},
 /* 092 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 12},
 /* 093 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 13},
 /* 094 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 14},
 /* 095 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 15},
 /* 096 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 16},
 /* 097 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 17},
 /* 098 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 18},
 /* 099 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 19},
 /* 100 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 20},
 /* 101 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 21},
 /* 102 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 22},
 /* 103 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 23},
 /* 104 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 24},
 /* 105 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 25},
 /* 106 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 26},
 /* 107 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 27},
 /* 108 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 28},
 /* 109 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 29},
 /* 110 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 30},
 /* 111 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        1, 31},
 /* 112 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        2,  0},
 /* 113 */ {R_DP_RELATIVE,         "R_DP_RELATIVE",        4,  0},
 /* 114 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 115 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 116 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 117 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 118 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 119 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 120 */ {R_DLT_REL,             "R_DLT_REL",            2,  0},
 /* 121 */ {R_DLT_REL,             "R_DLT_REL",            4,  0},
 /* 122 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 123 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 124 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 125 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 126 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 127 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 128 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  0},
 /* 129 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  1},
 /* 130 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  2},
 /* 131 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  3},
 /* 132 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  4},
 /* 133 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  5},
 /* 134 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  6},
 /* 135 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  7},
 /* 136 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  8},
 /* 137 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1,  9},
 /* 138 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 10},
 /* 139 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 11},
 /* 140 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 12},
 /* 141 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 13},
 /* 142 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 14},
 /* 143 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 15},
 /* 144 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 16},
 /* 145 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 17},
 /* 146 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 18},
 /* 147 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 19},
 /* 148 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 20},
 /* 149 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 21},
 /* 150 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 22},
 /* 151 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 23},
 /* 152 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 24},
 /* 153 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 25},
 /* 154 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 26},
 /* 155 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 27},
 /* 156 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 28},
 /* 157 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 29},
 /* 158 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 30},
 /* 159 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    1, 31},
 /* 160 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    2,  0},
 /* 161 */ {R_CODE_ONE_SYMBOL,     "R_CODE_ONE_SYMBOL",    4,  0},
 /* 162 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 163 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 164 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 165 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 166 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 167 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 168 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 169 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 170 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 171 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 172 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 173 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 174 */ {R_MILLI_REL,           "R_MILLI_REL",          2,  0},
 /* 175 */ {R_MILLI_REL,           "R_MILLI_REL",          4,  0},
 /* 176 */ {R_CODE_PLABEL,         "R_CODE_PLABEL",        2,  0},
 /* 177 */ {R_CODE_PLABEL,         "R_CODE_PLABEL",        4,  0},
 /* 178 */ {R_BREAKPOINT,          "R_BREAKPOINT",         1,  0},
 /* 179 */ {R_ENTRY,               "R_ENTRY",              9,  0},
 /* 180 */ {R_ENTRY,               "R_ENTRY",              6,  0},
 /* 181 */ {R_ALT_ENTRY,           "R_ALT_ENTRY",          1,  0},
 /* 182 */ {R_EXIT,                "R_EXIT",               1,  0},
 /* 183 */ {R_BEGIN_TRY,           "R_BEGIN_TRY",          1,  0},
 /* 184 */ {R_END_TRY,             "R_END_TRY",            1,  0},
 /* 185 */ {R_END_TRY,             "R_END_TRY",            2,  0},
 /* 186 */ {R_END_TRY,             "R_END_TRY",            3,  0},
 /* 187 */ {R_BEGIN_BRTAB,         "R_BEGIN_BRTAB",        1,  0},
 /* 188 */ {R_END_BRTAB,           "R_END_BRTAB",          1,  0},
 /* 189 */ {R_STATEMENT,           "R_STATEMENT",          2,  0},
 /* 190 */ {R_STATEMENT,           "R_STATEMENT",          3,  0},
 /* 191 */ {R_STATEMENT,           "R_STATEMENT",          4,  0},
 /* 192 */ {R_DATA_EXPR,           "R_DATA_EXPR",          1,  0},
 /* 193 */ {R_CODE_EXPR,           "R_CODE_EXPR",          1,  0},
 /* 194 */ {R_FSEL,                "R_FSEL",               1,  0},
 /* 195 */ {R_LSEL,                "R_LSEL",               1,  0},
 /* 196 */ {R_RSEL,                "R_RSEL",               1,  0},
 /* 197 */ {R_N_MODE,              "R_N_MODE",             1,  0},
 /* 198 */ {R_S_MODE,              "R_S_MODE",             1,  0},
 /* 199 */ {R_D_MODE,              "R_D_MODE",             1,  0},
 /* 200 */ {R_R_MODE,              "R_R_MODE",             1,  0},
 /* 201 */ {R_DATA_OVERRIDE,       "R_DATA_OVERRIDE",      1,  0},
 /* 202 */ {R_DATA_OVERRIDE,       "R_DATA_OVERRIDE",      2,  0},
 /* 203 */ {R_DATA_OVERRIDE,       "R_DATA_OVERRIDE",      3,  0},
 /* 204 */ {R_DATA_OVERRIDE,       "R_DATA_OVERRIDE",      4,  0},
 /* 205 */ {R_DATA_OVERRIDE,       "R_DATA_OVERRIDE",      5,  0},
 /* 206 */ {R_TRANSLATED,          "R_TRANSLATED",         1,  0},
 /* 207 */ {R_AUX_UNWIND,          "R_AUX_UNWIND",        12,  0},
 /* 208 */ {R_COMP1,               "R_COMP1",              2,  0},
 /* 209 */ {R_COMP2,               "R_COMP2",              5,  0},
 /* 210 */ {R_COMP3,               "R_COMP3",              6,  0},
 /* 211 */ {R_PREV_FIXUP,          "R_PREV_FIXUP",         1,  0},
 /* 212 */ {R_PREV_FIXUP,          "R_PREV_FIXUP",         1,  1},
 /* 213 */ {R_PREV_FIXUP,          "R_PREV_FIXUP",         1,  2},
 /* 214 */ {R_PREV_FIXUP,          "R_PREV_FIXUP",         1,  3},
 /* 215 */ {R_SEC_STMT,            "R_SEC_STMT",           1,  0},
 /* 216 */ {R_N0SEL,               "R_N0SEL",              1,  0},
 /* 217 */ {R_N1SEL,               "R_N1SEL",              1,  0},
 /* 218 */ {R_LINETAB,             "R_LINETAB",           10,  0},
 /* 219 */ {R_LINETAB_ESC,         "R_LINETAB_ESC",        3,  0},
 /* 220 */ {R_LTP_OVERRIDE,        "R_LTP_OVERRIDE",       1,  0},
 /* 221 */ {R_COMMENT,             "R_COMMENT",            6,  0},
 /* 222 */ {R_TP_OVERRIDE,         "R_TP_OVERRIDE",        1,  0},
 /* 223 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 224 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 225 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 226 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 227 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 228 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 229 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 230 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 231 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 232 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 233 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 234 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 235 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 236 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 237 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 238 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 239 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 240 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 241 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 242 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 243 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 244 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 245 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 246 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 247 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 248 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 249 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 250 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 251 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 252 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 253 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 254 */ {R_RESERVED,            "R_RESERVED",           1,  0},
 /* 255 */ {R_RESERVED,            "R_RESERVED",           1,  0},
};

struct Som_fixup_state {
  /* for R_TP_OVERRIDE */
  int fixup_is_tp_ovr;
  /* for R_PREV_FIXUP */
  struct {
    int lru_count;
    char *fixup;
  } fixup_cache[4];
};

/* Saved state for fixups */
static struct Som_fixup_state fixup_state = {
  0,
  {{0, NULL}, {1, NULL}, {2, NULL}, {3, NULL}}
};

void
somread_push_fixup(p)
  char *p;
{
  int j;
  /* increment all LRU counts */
  for (j = 0; j < 4; j++)
    fixup_state.fixup_cache[j].lru_count++;
  
  /* find LRU member */
  for (j = 0; j < 4; j++)
    if (fixup_state.fixup_cache[j].lru_count == 4)
      break;

  fixup_state.fixup_cache[j].lru_count = 0;
  fixup_state.fixup_cache[j].fixup = p;
}
  
char *
somread_pop_fixup(i)
  int i;
{
  int j, k;

  for (j = 0; j < 4; j++)
    if (fixup_state.fixup_cache[j].lru_count == i)
      break;  

  for (k = 0; k < 4; k++)
    if (fixup_state.fixup_cache[k].lru_count < i)
      fixup_state.fixup_cache[k].lru_count++;

  fixup_state.fixup_cache[j].lru_count = 0;
  return fixup_state.fixup_cache[j].fixup;
}

char *
get_symbol_strings(abfd)
  bfd *abfd;
{
#ifndef BFD64
  if (!(obj_som_stringtab(abfd)))
    som_slurp_string_table(abfd);
  return(obj_som_stringtab(abfd));
#else
  return 0;
#endif
}

struct symbol_dictionary_record *
get_symbol_table(abfd)
  bfd *abfd;
{
#ifndef BFD64
  if (!(obj_som_raw_symtab(abfd)))
    som_slurp_raw_symbol_table(abfd);
  
  return(obj_som_raw_symtab(abfd));
#else
  return 0;
#endif
}

int somread_print_fixup(objfile, abfd, buf, depth)
  struct objfile *objfile;
  bfd *abfd;
  unsigned char *buf;
  int depth;
{
  struct symbol_dictionary_record *symbols;
  char *strings;
  unsigned char opcode;
  unsigned int b, s = 0, x, l = 0;
  int i, j;
  char *k;

  symbols = get_symbol_table(abfd);
  strings = get_symbol_strings(abfd); 
    
  opcode = buf[0];
  printf("%s ", relocs[opcode].name);
  if ((depth == 0) && (relocs[opcode].length > 1))
    somread_push_fixup((char*)buf);

  i = 1;
  switch (relocs[opcode].type) {
    case R_NO_RELOCATION:
      if (opcode <= 23)
        l = (relocs[opcode].d + 1) * 4;
      else if (opcode <= 27) {
        b = buf[i++];
        l = ((relocs[opcode].d << 8) + b + 1) * 4;
      }
      else if (opcode <= 30) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        l = ((relocs[opcode].d << 16) + b + 1) * 4;
      }
      else {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      printf("%d", l);
      break;
      
    case R_ZEROES:
      if (opcode == 32) {
        b = buf[i++];
        l = (b+1) * 4;
      }
      else if (opcode == 33) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      printf("%d", l);
      break;
      
    case R_UNINIT:
      if (opcode == 34) {
        b = buf[i++];
        l = (b+1) * 4;
      }
      else if (opcode == 35) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      printf("%d", l);
      break;
      
    case R_DATA_ONE_SYMBOL:
      if (opcode == 37) {
        b = buf[i++];
        s = b;
      }
      if (opcode == 38) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        s = b;
      }
      printf("\"%s\"", strings + symbols[s].name.n_strx);
      break;
      
    case R_PREV_FIXUP:
      x = relocs[opcode].d;
      printf("%d (", x);
      k = somread_pop_fixup((int)x);
      somread_print_fixup(objfile, abfd, (unsigned char*)k, depth+1);
      printf(")");
      break;

    case R_TP_OVERRIDE:
      printf("tp ");
      break;

      /* CM: This can be safely ignored because the R_DATA_ONE_SYMBOL
         relocation should always be in F' mode. */
    case R_FSEL:
      /* RM: end.o has these relocations. We just ignore them for now */
      /* CM: These can be safely ignored on one condition. These change modes
         for other fixups. As long as all of those fixups are not accepted
         (i.e. error or ignored), these can be ignored. */
    case R_SHORT_PCREL_MODE:
    case R_LONG_PCREL_MODE:
    case R_N_MODE:
    case R_S_MODE:
    case R_D_MODE:
    case R_R_MODE:
      break;

    default:
      for (j=1; j < relocs[opcode].length; j++)
        i++;
      printf("(unimplemented)");
      break;
  }

  return i;
}

void
somread_dump_relocations(objfile, abfd, section)
  struct objfile *objfile;
  bfd *abfd;
  asection *section;
{
  char *buf;
  unsigned char opcode;
  int i, j;

  /* Read the relocations */
  buf = xmalloc (som_section_data(section)->reloc_size);
  if (!buf && (som_section_data(section)->reloc_size != 0))
    error("can't read relocations");

  if (bfd_seek (abfd,
                obj_som_reloc_filepos(abfd) + section->rel_filepos,
                SEEK_SET) < 0)
    error("can't read relocations");
          
  if (bfd_read (buf, 1, som_section_data(section)->reloc_size, abfd)
      != som_section_data(section)->reloc_size)
    error("can't read relocations");

  for (i = 0; i < som_section_data(section)->reloc_size;) {
    printf("  ");
    i += somread_print_fixup(objfile, abfd, (unsigned char*)buf+i, 0);
    printf("\n");
  }
  free(buf);
}

int somread_count_fixup(buf, countp, depth)
  unsigned char *buf;
  int *countp;
  int depth;
{
  unsigned char opcode;
  unsigned int l = 0, b, s = 0, x;
  int i, j;
  char *k;

  opcode = buf[0];
  if ((depth == 0) && (relocs[opcode].length > 1))
    somread_push_fixup((char*)buf);

  i = 1;
  switch (relocs[opcode].type) {
    case R_NO_RELOCATION:
      if (opcode <= 23)
        l = (relocs[opcode].d + 1) * 4;
      else if (opcode <= 27) {
        b = buf[i++];
        l = ((relocs[opcode].d << 8) + b + 1) * 4;
      }
      else if (opcode <= 30) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        l = ((relocs[opcode].d << 16) + b + 1) * 4;
      }
      else {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      *countp += l;
      break;
      
    case R_ZEROES:
      if (opcode == 32) {
        b = buf[i++];
        l = (b+1) * 4;
      }
      else if (opcode == 33) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      *countp += l;
      break;
      
    case R_UNINIT:
      if (opcode == 34) {
        b = buf[i++];
        l = (b+1) * 4;
      }
      else if (opcode == 35) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      *countp += l;
      break;
      
    case R_DATA_ONE_SYMBOL:
      if (opcode == 37) {
        b = buf[i++];
        s = b;
      }
      if (opcode == 38) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        s = b;
      }
      *countp += 4;
      break;
      
    case R_PREV_FIXUP:
      x = relocs[opcode].d;
      k = somread_pop_fixup((int)x);
      somread_count_fixup((unsigned char*)k, countp, depth+1);
      break;

    case R_TP_OVERRIDE:
      /* Do nothing */
      break;

      /* CM: This can be safely ignored because the R_DATA_ONE_SYMBOL
         relocation should always be in F' mode. */
    case R_FSEL:
      /* RM: end.o has these relocations. We just ignore them for now */
      /* CM: These can be safely ignored on one condition. These change modes
         for other fixups. As long as all of those fixups are not accepted
         (i.e. error or ignored), these can be ignored. */
    case R_SHORT_PCREL_MODE:
    case R_LONG_PCREL_MODE:
    case R_N_MODE:
    case R_S_MODE:
    case R_D_MODE:
    case R_R_MODE:
      break;
      
    default:
      error("Unimplemented linker fixup\n");
      break;
  }

  return i;
}

int
somread_relocated_section_size(abfd, section)
  bfd *abfd;
  asection *section;
{
  char *buf;
  unsigned char opcode;
  int i, j;
  int count;
  unsigned int val;

  /* Read the relocations */
  buf = xmalloc (som_section_data(section)->reloc_size);
  if (!buf && (som_section_data(section)->reloc_size != 0))
    error("can't read relocations");

  if (bfd_seek (abfd,
                obj_som_reloc_filepos(abfd) + section->rel_filepos,
                SEEK_SET) < 0)
    error("can't read relocations");
          
  if (bfd_read (buf, 1, som_section_data(section)->reloc_size, abfd)
      != som_section_data(section)->reloc_size)
    error("can't read relocations");

  count = 0;
  for (i = 0; i < som_section_data(section)->reloc_size;) {
    i += somread_count_fixup((unsigned char*)buf+i, &count, 0);
  }
  free(buf);

  return(count);
}

/* CM: Create a hashed symbol table for the minimal symbols. This
   should speed the doing relocations on object files. */

struct Hashed_minsymtab_item_t {
  struct minimal_symbol *sym;
  int objid;
  struct Hashed_minsymtab_item_t *next;
};

struct Hashed_minsymtab_t {
  struct Hashed_minsymtab_item_t **table;
  struct Hashed_minsymtab_item_t *syms;
  size_t num_syms;
  size_t table_size;
};

/* CM: Hash function */
#define SOMREAD_MINSYMTAB_HASHER(hash_val, ht, name) \
do { \
  char *name_walker = (name); \
  (hash_val) = 0; \
  for (name_walker = (name); *name_walker != '\0'; name_walker++) { \
    (hash_val) = (hash_val) + ((hash_val) << 3) + (int) (*name_walker); \
  } \
  (hash_val) = (hash_val) % ((ht)->table_size); \
} while(0)

/* CM: Create a hash table */
static struct Hashed_minsymtab_t *somread_create_minsymtab(objfile)
     struct objfile *objfile;
{
  struct Hashed_minsymtab_t *ht;
  int i;
  int count_msyms;
  int num_ht_entries;

  count_msyms = objfile->minimal_symbol_count;

  /* CM: Size of the hash table ... a prime number. Since we already know
     how many symbols there are, choose a table size to match. I just made
     best guesses at what the mapping should be. If emperical measurements
     or queuing theory show that different numbers are better, by all
     means, replace these. */
  if (count_msyms < 100) {
    num_ht_entries = 97;
  } else if (count_msyms < 1000) {
    num_ht_entries = 241;
  } else if (count_msyms < 5000) {
    num_ht_entries = 499;
  } else if (count_msyms < 20000) {
    num_ht_entries = 997;
  } else if (count_msyms < 80000) {
    num_ht_entries = 1999;
  } else if (count_msyms < 160000) {
    num_ht_entries = 7993;
  } else if (count_msyms < 320000) {
    num_ht_entries = 16001;
  } else {
    num_ht_entries = 32003;
  }

  ht = (struct Hashed_minsymtab_t *)
    obstack_alloc(&objfile->symbol_obstack,
		  sizeof (struct Hashed_minsymtab_t) +
                  (num_ht_entries *
                   sizeof(struct Hashed_minsymtab_item_t *)) +
                  (count_msyms * sizeof(struct Hashed_minsymtab_item_t)));
  ht->table = (struct Hashed_minsymtab_item_t **)
    ((unsigned long) ht + sizeof(struct Hashed_minsymtab_t));
  ht->syms = (struct Hashed_minsymtab_item_t *)
    ((unsigned long) ht +
     sizeof(struct Hashed_minsymtab_t) +
     (num_ht_entries * sizeof(struct Hashed_minsymtab_item_t *)));

  for (i = 0; i < num_ht_entries; i++) {
    ht->table[i] = NULL;
  }
  ht->num_syms = 0;
  ht->table_size = num_ht_entries;

  return ht;
}

/* CM: Determine if two symbol types match. Return non-zero
   for match, zero if not match. mst_unknown and mst_abs match
   with everything. This matrix is square and reflective across
   the diagonal, so it does not matter in what order the indicies
   are given. */
static int matchsymtab[mst_num_types][mst_num_types] =
{
  { 1,1,1,1,1,1,1,1,1,1,1 },
  { 1,1,0,0,1,1,0,0,0,0,0 },
  { 1,0,1,1,1,0,0,0,0,0,0 },
  { 1,0,1,1,1,0,0,0,0,0,0 },
  { 1,1,1,1,1,1,1,1,1,1,1 },
  { 1,1,0,0,1,1,0,0,0,0,0 },
  { 1,0,0,0,1,1,1,0,0,0,0 },
  { 1,0,0,0,1,0,0,1,1,0,0 },
  { 1,0,0,0,1,0,0,1,1,0,0 },
  { 1,0,0,0,1,0,0,0,0,1,1 },
  { 1,0,0,0,1,0,0,0,0,1,1 }
};

#define SOMREAD_MATCH_SYMTAB(sym1, sym2) \
  (matchsymtab[MSYMBOL_TYPE(sym1)][MSYMBOL_TYPE(sym2)])

/* CM: Determine if a symbol shold replace another in the
   symbol table. The first index is the old symbol. The
   second is the new symbol. This means that the new
   symbol is along the X-axis of the matrix and the old is
   along the Y-axis. So, a fixed row corresponds to a fixed
   old symbol type.

   Return values:
   0: don't replace
   1: always replace
   2: replace if old symbol is an ST_ENTRY
*/
static int replacesymtab[mst_num_types][mst_num_types] =
{
  { 0,1,1,1,1,1,1,1,1,1,1 },
  { 0,2,0,0,0,0,2,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 },
  { 0,1,0,0,0,0,1,0,0,0,0 },
  { 0,2,0,0,0,0,2,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 },
  { 0,0,0,0,0,0,0,0,0,0,0 }
};

#define SOMREAD_REPLACE_SYMTAB(sym1, sym2) \
  (replacesymtab[MSYMBOL_TYPE(sym1)][MSYMBOL_TYPE(sym2)])

/* CM: Add a symbol to the hash table. If a matching symbol is
   already present, determine if it should be replaced. The
   objid is the object the symbol was defined in. */
static void somread_add_minsymtab(ht, sym, objid)
  struct Hashed_minsymtab_t *ht;
  struct minimal_symbol *sym;
  int objid;
{
  int hash_val;
  struct Hashed_minsymtab_item_t *syms;
  int found = 0;
  char *sym_name = SYMBOL_NAME(sym);

  /* CM: Search for symbol. Match name, objid, and type. */
  SOMREAD_MINSYMTAB_HASHER(hash_val, ht, sym_name);
  syms = ht->table[hash_val];
  while ((syms != NULL) && (!found)) {
    if ((objid == syms->objid) &&
        SOMREAD_MATCH_SYMTAB(sym, syms->sym) &&
        (STREQ(sym_name, SYMBOL_NAME(syms->sym)))) {
      found = 1;
    } else {
      syms = syms->next;
    }
  }
  if (found) {
    /* CM: Replace this found symbol or dump the added symbol */
    switch (SOMREAD_REPLACE_SYMTAB(syms->sym, sym)) {
    case 1:
      syms->sym = sym;
      syms->objid = objid;
      break;
#ifndef BFD64      
    case 2:
      if ((((unsigned int) (MSYMBOL_INFO(syms->sym))) == ST_ENTRY)) {
        syms->sym = sym;
        syms->objid = objid;        
      }
      break;
#endif      
    }
  } else {
    /* CM: Add symbol */
    struct Hashed_minsymtab_item_t *new_sym = &ht->syms[ht->num_syms];
    new_sym->sym = sym;
    new_sym->objid = objid;
    new_sym->next = ht->table[hash_val];
    ht->table[hash_val] = new_sym;
    (ht->num_syms)++;
  }
}

/* CM: Convert a symbol type to a boolean if the symbol is global or local */
static int convtype2isglobal[mst_num_types] =
{ 1,1,1,1,1,1,0,0,0,1,0 };

/* CM: Search the symbol table for a symbol name in a specific
   objid. If objid == -1, look for a global symbol in any objids.
   If objid >= 0, look for a local symbol with that objid */
static struct minimal_symbol *somread_search_minsymtab(ht,
                                                       sym_name,
                                                       objid,
                                                       do_match_objid,
                                                       isglobaltype,
                                                       do_match_isglobaltype)
  struct Hashed_minsymtab_t *ht;
  char *sym_name;
  int objid;
  int do_match_objid;
  int isglobaltype;
  int do_match_isglobaltype;
{
  int hash_val;
  struct Hashed_minsymtab_item_t *syms;

  /* CM: Search for symbol. Match name and objid. */
  SOMREAD_MINSYMTAB_HASHER(hash_val, ht, sym_name);
  syms = ht->table[hash_val];
  while (syms != NULL) {
    if (STREQ(sym_name, SYMBOL_NAME(syms->sym))) {
      int result = 1;
      if (do_match_objid) {
        result = result && (objid == syms->objid);
      }
      if (do_match_isglobaltype) {
        result = result &&
          (convtype2isglobal[MSYMBOL_TYPE(syms->sym)] == isglobaltype);
      }
      if (result) {
        return syms->sym;
      }
    }
    syms = syms->next;
  }
  return NULL;
}

/* CM: Convert a symbol type to a particular space to look. */
static enum space convtype2space[mst_num_types] =
{ textspace, textspace, dataspace, dataspace, dataspace,
  textspace, textspace, dataspace, dataspace, dataspace, dataspace };

/* just like lookup_minimal_symbol, but must locate statics in the
 * right object file. Needed for DOOM for performing linker fixups
 * relative to static symbols.
 */
struct minimal_symbol *
somread_lookup_minimal_symbol_within_object (name, objid, objf,
                                             is_global_type,
                                             is_comdat_type)
     register const char *name;
     int objid;
     struct objfile *objf;
     int is_global_type;
     int is_comdat_type;
{
  struct Hashed_minsymtab_t *ht;

  ht = SYMFILE_HT(objf);
  if (!ht) {
    /* Lazily build hash table of minimal symbols */
    struct minimal_symbol *msymbol;
    int sym_objid;

    ht = somread_create_minsymtab(objf);
    for (msymbol = objf -> msymbols;
         msymbol != NULL && SYMBOL_NAME (msymbol) != NULL;
         msymbol++) {
      char *symname = SYMBOL_NAME (msymbol);

      /* CM: If the symbol name starts with "D$", it is a
         special compiler generated symbol. Treat it specially.
         This needs to be done because an ambiguity exists. Symnbols
         can appear at the end or beginning of a subspace created
         by a compiler.  If it does, in the executable the symbol
         will sit at the border between two subspaces. As a result,
         the debugger cannot tell to which subspace the symbol
         belongs: the end of the first subspace or the beginning
         of the second. */
      if (symname[0] == 'D' && symname[1] == '$') {
        /* CM: For "D$" symbols, perfer it to end a subspace */
        sym_objid =
          hpread_find_objid(objf,
                            SYMBOL_VALUE(msymbol) - 1,
                            convtype2space[SYMBOL_TYPE(msymbol)]);
        if (sym_objid == -1) {
          sym_objid =
            hpread_find_objid(objf,
                              SYMBOL_VALUE(msymbol),
                              convtype2space[SYMBOL_TYPE(msymbol)]);
        }
      } else {
        /* CM: For normal symbols, perfer it to begin a subspace */
        sym_objid =
          hpread_find_objid(objf,
                            SYMBOL_VALUE(msymbol),
                            convtype2space[SYMBOL_TYPE(msymbol)]);
        if (sym_objid == -1) {
          sym_objid =
            hpread_find_objid(objf,
                              SYMBOL_VALUE(msymbol) - 1,
                              convtype2space[SYMBOL_TYPE(msymbol)]);
        }
      }

      somread_add_minsymtab(ht, msymbol, sym_objid);
    }
    SYMFILE_HT(objf) = ht;
  }

  /* CM: Find the symbol. */
  if (is_comdat_type) {
    return somread_search_minsymtab(ht,(char*)name, objid, 1, is_global_type, 0);
  } else {
    return somread_search_minsymtab(ht, (char*)name, objid, (is_global_type ? 0 : 1),
                                    is_global_type, 1);
  }
}

/* CM: Copy a 32-bit word (4 bytes) from a possibly unaligned address to
   another. */
#define COPY_UNALIGNED_TO_UNALIGNED_WORD(dest, src) \
  ((char *) (dest))[0] = ((char *) (src))[0]; \
  ((char *) (dest))[1] = ((char *) (src))[1]; \
  ((char *) (dest))[2] = ((char *) (src))[2]; \
  ((char *) (dest))[3] = ((char *) (src))[3]

int somread_do_fixup(objfile, abfd, objid, buf, ibufp, obufp, depth)
  struct objfile *objfile;
  bfd *abfd;
  int objid;
  unsigned char *buf;
  char **ibufp, **obufp;
  int depth;
{ 
  struct symbol_dictionary_record *symbols;
  char *strings;
  unsigned char opcode;
  unsigned int l = 0, b, s = 0, x;
  int i, j, w;
  char *k, *symname;
  struct minimal_symbol *msym;
  CORE_ADDR val;
  int is_data_override;
  int data_override_value = 0;
  int is_global_sym;
  int is_comdat_sym;

  symbols = get_symbol_table(abfd);
  strings = get_symbol_strings(abfd); 

  opcode = buf[0];
  if ((depth == 0) && (relocs[opcode].length > 1))
    somread_push_fixup((char*)buf);

  i = 1;

  switch (relocs[opcode].type) {
    case R_NO_RELOCATION:
      if (opcode <= 23)
        l = (relocs[opcode].d + 1) * 4;
      else if (opcode <= 27) {
        b = buf[i++];
        l = ((relocs[opcode].d << 8) + b + 1) * 4;
      }
      else if (opcode <= 30) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        l = ((relocs[opcode].d << 16) + b + 1) * 4;
      }
      else {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      for (j = 0; j < l; j++)
        *(*obufp)++ = *(*ibufp)++;
      break;

    case R_ZEROES:
      if (opcode == 32) {
        b = buf[i++];
        l = (b+1) * 4;
      }
      else if (opcode == 33) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      for (j = 0; j < l; j++)
        *(*obufp)++ = 0;
      break;

    case R_UNINIT:
      if (opcode == 34) {
        b = buf[i++];
        l = (b+1) * 4;
      }
      else if (opcode == 35) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        l = b+1;
      }
      for (j = 0; j < l; j++)
        *(*obufp)++ = 0;
      break;

    case R_DATA_ONE_SYMBOL:
      if (opcode == 37) {
        b = buf[i++];
        s = b;
      }
      if (opcode == 38) {
        b = buf[i++];
        b = (b << 8) + buf[i++];
        b = (b << 8) + buf[i++];
        s = b;
      }
      is_global_sym =
        ((symbols[s].symbol_scope == SS_UNIVERSAL) ||
         (symbols[s].symbol_scope == SS_EXTERNAL) ||
         (symbols[s].symbol_scope == SS_UNSAT));
      is_comdat_sym =
        (((struct symbol_dictionary_record_hpux11 *)
          (&symbols[s]))->is_comdat);
      symname = strings + symbols[s].name.n_strx;
      msym = somread_lookup_minimal_symbol_within_object
        ((const char*)symname,
         objid,
         objfile,
         is_global_sym,
         is_comdat_sym);
      is_data_override = 0;
      if (!msym) {
        if (debug_traces > 1)
          warning("Symbol `%s' not found in linker symbol table. "
                  "This symbol may not be locatable later.", symname);
        val = 0;
        if (!is_global_sym || is_comdat_sym) {
          /* CM: This is an error or a COMDAT symbol. Do not do this
             for global symbols now. This is to support shared
             libraries. */
          is_data_override = 1;
          data_override_value = 0;
        }
      } else if (fixup_state.fixup_is_tp_ovr) {
        asection *tp_space;

        /* Check that the executable is loaded */
        if (objfile->obfd == NULL) {
          error("(Internal) cannot apply fixups for TLS symbols.\n");
        }
        /* Get the TLS space address */
        tp_space = bfd_get_section_by_name(objfile->obfd, "$THREAD_SPECIFIC$");
        /* Verify that the TLS space exists */
        if (tp_space == NULL) {
          error("Saw a TLS symbol but no TLS space exists in the "
                "executable/shared library.\n");
        }
        /* Calculate the fixup */
        val = SYMBOL_VALUE(msym) -
              ANOFFSET(objfile->section_offsets, SYMBOL_SECTION(msym)) -
              bfd_get_section_vma(abfd, tp_space);
      } else {
        /* Calculate the fixup */
        val = SYMBOL_VALUE(msym) -
              ANOFFSET(objfile->section_offsets, SYMBOL_SECTION(msym));
      }

      /* Apply the fixup */
      if (is_data_override) {
        w = data_override_value;
      } else {
        COPY_UNALIGNED_TO_UNALIGNED_WORD(&w, *ibufp);
        /* memcpy(&w, *ibufp, 4); */
      }
      *ibufp += 4;
      w = w + val;
      COPY_UNALIGNED_TO_UNALIGNED_WORD(*obufp, &w);
      /* memcpy(*obufp, &w, 4); */
      *obufp += 4;
      fixup_state.fixup_is_tp_ovr = 0;
      break;

    case R_PREV_FIXUP:
      x = relocs[opcode].d;
      k = somread_pop_fixup((int)x);
      somread_do_fixup(objfile, abfd, objid,(unsigned char*)k, ibufp, obufp, depth+1);
      break;

    case R_TP_OVERRIDE:
      fixup_state.fixup_is_tp_ovr = 1;
      break;

      /* CM: This can be safely ignored because the R_DATA_ONE_SYMBOL
         relocation should always be in F' mode. */
    case R_FSEL:
      /* RM: end.o has these relocations. We just ignore them for now */
      /* CM: These can be safely ignored on one condition. These change modes
         for other fixups. As long as all of those fixups are not accepted
         (i.e. error or ignored), these can be ignored. */
    case R_SHORT_PCREL_MODE:
    case R_LONG_PCREL_MODE:
    case R_N_MODE:
    case R_S_MODE:
    case R_D_MODE:
    case R_R_MODE:
      break;

    default:
      warning("Unimplemented linker fixup %s(%d) detected and ignored.",
              relocs[opcode].name, relocs[opcode].type, objfile->name);
      break;
  }

  return i;
}

void
somread_get_relocated_section_contents (objfile, abfd, objid,
                                        section, obuf, count)
  struct objfile *objfile;
  bfd *abfd;
  int objid;
  asection *section;
  char *obuf;
  int count;
{
  char *buf = 0, *buf2 = 0, *ibuf;
  int raw_section_size;
  int i, j;
  
  raw_section_size = bfd_section_size (abfd, section);
  ibuf = buf = (char *) xmalloc(raw_section_size);
  bfd_get_section_contents (abfd, section, buf, 0, raw_section_size);

  /* Read the relocations */
  buf2 = xmalloc (som_section_data(section)->reloc_size);
  if (!buf2 && (som_section_data(section)->reloc_size != 0))
    error("can't read relocations");

  if (bfd_seek (abfd,
                obj_som_reloc_filepos(abfd) + section->rel_filepos,
                SEEK_SET) < 0)
    error("can't read relocations");
          
  if (bfd_read (buf2, 1, som_section_data(section)->reloc_size, abfd)
      != som_section_data(section)->reloc_size)
    error("can't read relocations");

  for (i = 0; i < som_section_data(section)->reloc_size;) {
    i += somread_do_fixup(objfile, abfd, objid, (unsigned char*)buf2+i, &ibuf, &obuf, 0);
  }
  free(buf2);
  free(buf);
}



/* Register that we are able to handle SOM object file formats.  */

#ifndef HP_IA64
/* jini: Mixed mode support: Sep 2006: JAGag21714: Converted som_sym_fns to
   be non static so that it is visible to _initialize_elfread */
static
#endif
struct sym_fns som_sym_fns =
{
  bfd_target_som_flavour,
  som_new_init,			/* sym_new_init: init anything gbl to entire symtab */
  som_symfile_init,		/* sym_init: read initial info, setup for sym_read() */
  som_symfile_read,		/* sym_read: read a symbol file into symtab */
  som_symfile_add_psymtabs, /* sym_add_psymtabs: force symbol reading
                             * for files that were skipped earlier */
  som_symfile_finish,		/* sym_finish: finished with file, cleanup */
  som_symfile_offsets,		/* sym_offsets:  Translate ext. to int. relocation */
  NULL				/* next: pointer to next struct sym_fns */
};

void
_initialize_somread ()
{
  add_symtab_fns (&som_sym_fns);
}

/* For JAGaf16859 - Gdb to list source files
 * even when the executable is moved from the 
 * compilation dirctory.
 */
/* QXCR1000919943: wdb out of memory on setting breakpoint.
 * Function "som_set_current_compdir" is modified so as to
 * avoid leaks when called miltiple times.
 */
void * 
som_set_current_compdir(pst)
	struct partial_symtab *pst;
{
	bfd *abfd;
	char *comp_buf;
	char current_srcfile[PATH_MAX];
	int l, n;
	struct hp_compilation_unit *comp_units;
  	struct hp_compilation_unit cur_comp_unit;
  	unsigned int src_filename, comp_dirname;
  	char *dirstring;
	char * pst_file;
	char * current_file;
  	void *compilation_dir;
	bfd_size_type  section_strings_size;
	char *s;
	int i,comp_size = 0;
	int space_strings_size;

	abfd = symfile_bfd_open (pst->objfile->name);
	comp_size = obj_som_comp_units_total(abfd);

	comp_buf = xmalloc (comp_size*
                       sizeof(struct hp_compilation_unit));
 	if (!comp_buf && comp_size != 0)
	   	error("can't read compilation units table");

  	if (bfd_seek (abfd, obj_som_comp_units_filepos(abfd), SEEK_SET) < 0)
    		error("can't read compilation units table");

  	if (bfd_read (comp_buf, 1,
            comp_size * sizeof(struct hp_compilation_unit), abfd) !=
      	    comp_size * sizeof(struct hp_compilation_unit))
    		error("can't read compilation units table");

        if (!STRINGS(pst->objfile))
	  STRINGS(pst->objfile) = xmalloc(obj_som_spacestringtab_size(abfd) +
                                obj_som_stringtab_size (abfd) + STRING_TABLE_DELTA);
  	if (!STRINGS(pst->objfile) && !((obj_som_spacestringtab_size(abfd) == 0) ||
                    (obj_som_spacestringtab_size(abfd) == 0)))
   	 	error("can't read strings");

  	if (bfd_seek (abfd, obj_som_spacestr_filepos(abfd), SEEK_SET) < 0)
    		error("can't read space strings");

  	if (bfd_read (STRINGS(pst->objfile), 1, obj_som_spacestringtab_size(abfd), abfd)
              != obj_som_spacestringtab_size(abfd))
   		error("can't read space strings");

  	if (bfd_seek (abfd, obj_som_str_filepos(abfd), SEEK_SET) < 0)
    		error("can't read symbol strings");

  	if (bfd_read (STRINGS(pst->objfile)+obj_som_spacestringtab_size(abfd), 1,
                obj_som_stringtab_size(abfd), abfd) != obj_som_stringtab_size(abfd))
    		error("can't read symbol strings");

  	string_table_size = (STRING_TABLE_DELTA) +
    			    obj_som_spacestringtab_size(abfd) + obj_som_stringtab_size(abfd);

  	string_table_limit = obj_som_spacestringtab_size (abfd) + 
		             obj_som_stringtab_size (abfd);

	n = comp_size;

        if (!COMPMAP(pst->objfile))
	  COMPMAP(pst->objfile) = (struct comp_map *) xmalloc (sizeof(struct comp_map) +
                                         sizeof(struct cr) * (n-1));

  	COMPMAP(pst->objfile)->max_cr = n;
  	COMPMAP(pst->objfile)->n_cr = 0;

  	comp_units = (struct hp_compilation_unit *) comp_buf;

	space_strings_size = obj_som_spacestringtab_size(abfd);

        if (abfd->filename)
          /* It was allocated during 'symfile_bfd_open' call earlier in this function.*/
          free ((char *)(abfd->filename));
        bfd_close(abfd);

	for (i=0; i<comp_size; i++)
    	{
     		int j = 0;
		cur_comp_unit = comp_units[i];
		src_filename = space_strings_size+cur_comp_unit.name;
		 
		s = STRINGS(pst->objfile) + src_filename;
		while (j < (PATH_MAX - 1) && s[j] && s[j] != '\n')
		  {
		    current_srcfile[j] = s[j];
		    j++;
		  }
		current_srcfile[j] = 0;
      		while (*s && *s != '\n')
        		s++;

      		if (*s == '\n')
        	{
          		*s = 0;
          		if (*(s+1) == '/')
            		{
              			comp_dirname = s-STRINGS(pst->objfile)+1;
              			s++;
              			while (*s && *s != '\n' && *s != ' ' && *s != '\t')
                		s++;
              			if (*s)
                		*s = 0;
            		}
          		else
            		{
              			comp_dirname = s-STRINGS(pst->objfile);
            		}
        	}
      		else
        	{
          		comp_dirname = s-STRINGS(pst->objfile);
        	}


		pst_file  = strdup(basename(pst->filename));
		current_file = strdup(basename(current_srcfile));

      		if (strcmp (pst_file, current_file)==0)
		{
      	            	compilation_dir = STRINGS(pst->objfile) + comp_dirname;
                        if (comp_buf)
                          {
                            free (comp_buf);
                            comp_buf = NULL;
                          }
                        free (pst_file);
                        free (current_file);
               		return compilation_dir;
	  	}
                free (pst_file);
                free (current_file);
	}
        
        if (comp_buf)
          { 
            free (comp_buf);
            comp_buf = NULL;
          }
	return NULL;
}
