/* Handle HP SOM shared libraries for GDB, the GNU Debugger.
   Copyright 1993, 1996, 1999 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Written by the Center for Software Science at the Univerity of Utah
   and by Cygnus Support.  */

#include <sys/resource.h>

#include "solist.h"
#include "frame.h"
#include "som.h"
#include "libhppa.h"
#include "gdbcore.h"
#include "breakpoint.h"
#include "symfile.h"
#include "inferior.h"
#include "gdb-stabs.h"
#include "gdb_stat.h"
#include "gdbcmd.h"
#include "assert.h"
#include "language.h"
#include "top.h"
#include "somsolib.h"

#include <fcntl.h>
#include <som.h>
#ifdef HP_IA64
/* jini: mixed mode changes: JAGag21714: Include dlfcn.h so that
   som_solib_load_symbols understands "load_module_desc" */
#include <dlfcn.h>
#include "mixed_mode.h"
#endif


#ifndef RLIMIT_DATA
#define RLIMIT_DATA 2
#endif

#ifndef O_BINARY
#define O_BINARY 0
#endif

#define TSD_START_ADDR_OFFSET 152
#define TSD_INDEX 304

#define SHLIB_TEXT_PRIVATE_ENABLE 0x4000

/* Uncomment this to turn on some debugging output.
 */

/* #define SOLIB_DEBUG
 */

/* Defined in exec.c; used to prevent dangling pointer bug.
 */
extern struct target_ops exec_ops;

/* Defined in gdbrtc.c; Used for supressing warnings to end-user */
extern bool add_unloaded_library_symbols;
extern int check_heap_in_this_run, trace_threads_in_this_run;
static int alternate_d_trap_missing = 0;
extern int auto_loading;
#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
extern int pending_dld_relocations;
extern int dld_can_preload;
#endif

extern char* dld_name;

extern CORE_ADDR text_start;
extern CORE_ADDR text_end;

/* This lives in hppa-tdep.c. */
extern struct unwind_table_entry *hppa_find_unwind_entry (CORE_ADDR pc);

/* These ought to be defined in some public interface, but aren't.  They
   define the meaning of the various bits in the distinguished __dld_flags
   variable that is declared in every debuggable a.out on HP-UX, and that
   is shared between the debugger and the dynamic linker.
 */
#define DLD_FLAGS_MAPPRIVATE    0x1
#define DLD_FLAGS_HOOKVALID     0x2
#define DLD_FLAGS_LISTVALID     0x4
#define DLD_FLAGS_BOR_ENABLE    0x8
#define FILTER_LIBS_ENABLED     0x2000

/* TODO:

   * Most of this code should work for hp300 shared libraries.  Does
   anyone care enough to weed out any SOM-isms.

   * Support for hpux8 dynamic linker.  */

/* The basic structure which describes a dynamically loaded object.  This
   data structure is private to the dynamic linker and isn't found in
   any HPUX include file.  */

struct som_solib_mapped_entry
  {
    /* The name of the library.  */
    /* jini: Using the name field of the so_list itself was considered. But
       since this structure is used by the dld, we have to retain and use
       the name field from here. */
    char *name;

    /* Version of this structure (it is expected to change again in hpux10).  */
    unsigned char struct_version;

    /* Binding mode for this library.  */
    unsigned char bind_mode;

    /* Version of this library.  */
    short library_version;

    /* Start of text address,
     * link-time text location (length of text area),
     * end of text address.  */
    CORE_ADDR text_addr;
    CORE_ADDR text_link_addr;
    CORE_ADDR text_end;

    /* Start of data, start of bss and end of data.  */
    CORE_ADDR data_start;
    CORE_ADDR bss_start;
    CORE_ADDR data_end;

    /* Value of linkage pointer (%r19).  */
    CORE_ADDR got_value;

    /* Next entry.  */
    struct som_solib_mapped_entry *next;

    /* There are other fields, but I don't have information as to what is
       contained in them.  */

    /* For versions from HPUX-10.30 and up */

    /* Address in target of offset from thread-local register of
     * start of this thread's data.  I.e., the first thread-local
     * variable in this shared library starts at *(tsd_start_addr)
     * from that area pointed to by cr27 (mpsfu_hi).
     *
     * We do the indirection as soon as we read it, so from then
     * on it's the offset itself.
     */
    CORE_ADDR tsd_start_addr;

    /* Holds the load module index which is used to compute the
     * Address of the TLS symbol in a dynamically loaded module 
     */
    CORE_ADDR tsd_index;

    /* Following this are longwords holding:

     * ?, ?, ?, ptr to -1, ptr to-1, ptr to lib name (leaf name),
     * ptr to __data_start, ptr to __data_end
     */
 
     /* Holds the time_stamp at which the library is created - core file mismatch detection*/
     int gdb_info_offset; 
     struct sys_clock file_time; 
  };

/* Mixed mode support: Use a common so_list_head in hp-ia64-solib.c and
   somsolib.c for IA64. The single linked list consisting of both IA and PA
   load modules is used for mixed mode applications. Retain the static
   definition for PA32. */
#ifdef HP_IA64
extern struct so_list *so_list_head;
#else
static struct so_list *so_list_head;
#endif
static struct so_list *cache_solib_p = 0;

struct dll_reload_list {
    char * regexp;
    struct dll_reload_list * next;
} * auto_solib_readd;

/* This is the cumulative size in bytes of the symbol tables of all
   shared objects on the so_list_head list.  (When we say size, here
   we mean of the information before it is brought into memory and
   potentially expanded by GDB.)  When adding a new shlib, this value
   is compared against the threshold size, held by auto_solib_add
   (in megabytes).  If adding symbols for the new shlib would cause
   the total size to exceed the threshold, then the new shlib's symbols
   are not loaded.
 */
static LONGEST som_solib_total_st_size;
static boolean  main_st_size_added;

/* When the threshold is reached for any shlib, we refuse to add
   symbols for subsequent shlibs, even if those shlibs' symbols would
   be small enough to fit under the threshold.  (Although this may
   result in one, early large shlib preventing the loading of later,
   smalller shlibs' symbols, it allows us to issue one informational
   message.  The alternative, to issue a message for each shlib whose
   symbols aren't loaded, could be a big annoyance where the threshold
   is exceeded due to a very large number of shlibs.)
 */
static int som_solib_st_size_threshold_exceeded;

/* JAGaf26681 - Warn user that shared libraries are not mapped private. */ 
static unsigned int dlheader_has_map_private_flag;
static unsigned int dld_has_map_private_flag;
int is_solib_mapped_private (void);

/* These addresses should be filled in by som_solib_create_inferior_hook.
   They are also used elsewhere in this module.
 */
typedef struct
  {
    CORE_ADDR address;
    struct unwind_table_entry *unwind;
  }
addr_and_unwind_t;

/* When adding fields, be sure to clear them in _initialize_som_solib. */
static struct
  {
    boolean is_valid;
    addr_and_unwind_t hook;
    addr_and_unwind_t hook_stub;
    addr_and_unwind_t load;
    addr_and_unwind_t load_stub;
    addr_and_unwind_t unload;
    addr_and_unwind_t unload2;
    addr_and_unwind_t unload_stub;
  }
dld_cache;

static struct breakpoint * bp_at_start = 0;
CORE_ADDR _start_addr;   /* CLLbs14756 */

/* This are from gdbrtc.c */
extern struct objfile* rtc_dot_sl;

/* JAGaa80453/JAGaf03519 - To catch the statically unloaded libraries */
 
static struct breakpoint * bp_at_end = 0;
CORE_ADDR _end_addr;

static CORE_ADDR dld_flags_addr;

static int error_in_shlib_symtab = 0;

void som_sharedlibrary_info_command (char *, int);

#ifndef HP_IA64
/* These functions are currently unused on IPF */
static void som_solib_sharedlibrary_command (char *, int);

static char * som_solib_library_pathname (int);
#endif

/* JAGaf54436: Deferred bps don't trigger on a 
   dlopened library if main pgm stripped  */
CORE_ADDR find_d_trap_in_stripped_aout ();

/* Return a ballpark figure for the amount of memory GDB will need to
   allocate to read in the debug symbols from FILENAME.  

   Stacey 10/17/2001
   This function returns only if all goes well and errors out otherwise.
   In some cases, we want the error caught so we know about it but can
   ask gdb to continue executing.  Since this function can return 0 if 
   all is well and catch_errors returns 0 for errors or the value of the
   function otherwise, after calling this from catch_errors we won't know
   if the call completed correctly. So I introduced a new global error 
   flag; error_in_shlib_symtab will be 1 if an error occurs in this function 
   and 0 otherwise.  */

/* jini: mixed mode changes: Sept 2006: JAGag21714: Made changes to have the
   som_solib_sizeof_symbol_table as non-static since this needs
   to be called from hp-ia64-solib.c
*/

int
som_solib_sizeof_symbol_table (char *filename)
{
  bfd *abfd;
  int desc;
  char *absolute_name;
  int st_size = 0;
  asection *sect;

  /* We believe that filename was handed to us by the dynamic linker, and
     is therefore always an absolute path.
   */
  desc = openp (getenv ("PATH"), 1, filename, O_RDONLY | O_BINARY, 0, &absolute_name);
  if (desc < 0)
    {
      error_in_shlib_symtab=1;
      perror_with_name (filename);
    }
  filename = absolute_name;

  abfd = bfd_fdopenr (filename, gnutarget, desc);
  if (!abfd)
    {
      close (desc);
      make_cleanup (free, filename);
      error_in_shlib_symtab=1;
      error ("\"%s\": can't open to read symbols: %s.", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

  if (!bfd_check_format (abfd, bfd_object)	/* Reads in section info */
#ifdef HP_IA64
  /* jini: mixed mode support: JAGag21714 */
       && (bfd_get_flavour(abfd) != bfd_target_som_flavour)
#endif
     )
    {
      bfd_close (abfd);		/* This also closes desc */
      make_cleanup (free, filename);
      error_in_shlib_symtab=1;
      error ("\"%s\": can't read symbols: %s.", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

#ifdef HP_IA64
  /* jini: mixed mode support: JAGag21714 */
  assert(bfd_get_flavour(abfd) == bfd_target_som_flavour);
#endif

  /* Sum the sizes of the various sections that compose debug info. */

  /* This contains non-DOC information. */
  sect = bfd_get_section_by_name (abfd, "$DEBUG$");
  if (sect)
    st_size += (int) bfd_section_size (abfd, sect);

  /* This contains DOC information. */
  sect = bfd_get_section_by_name (abfd, "$PINFO$");
  if (sect)
    st_size += (int) bfd_section_size (abfd, sect);

  bfd_close (abfd);		/* This also closes desc */
  free (filename);

  /* Unfortunately, just summing the sizes of various debug info
     sections isn't a very accurate measurement of how much heap
     space the debugger will need to hold them.  It also doesn't
     account for space needed by linker (aka "minimal") symbols.

     Anecdotal evidence suggests that just summing the sizes of
     debug-info-related sections understates the heap space needed
     to represent it internally by about an order of magnitude.

     Since it's not exactly brain surgery we're doing here, rather
     than attempt to more accurately measure the size of a shlib's
     symbol table in GDB's heap, we'll just apply a 5x fudge-
     factor to the debug info sections' size-sum.  No, this doesn't
     account for minimal symbols in non-debuggable shlibs.  But it
     all roughly washes out in the end.
   */

  /* Down scaled the fudge factor by 1/2. We have come a long way
     since the time this number was chosen  -- srikanth, 000210
  */
  return st_size * 5;
}

static void
som_solib_add_solib_objfile (so, name, from_tty, text_addr, threshold_exceeded)
     struct so_list *so;
     char *name;
     int from_tty;
     CORE_ADDR text_addr;
     int threshold_exceeded;
{
#ifdef HP_IA64
  hppa_obj_private_data_t *obj_private = NULL;
#else
  obj_private_data_t *obj_private = NULL;
#endif

  struct section_addr_info section_addrs;
#ifndef HP_IA64
  struct som_solib_mapped_entry *solib_desc = NULL;
#else
  struct load_module_desc *solib_desc = NULL;
#endif

  memset (&section_addrs, 0, sizeof (section_addrs));
  section_addrs.other[0].name = "$TEXT$";
  section_addrs.other[0].addr = text_addr;
  so->objfile = maybe_symbol_file_add (name, from_tty, &section_addrs, 0, OBJF_SHARED, threshold_exceeded);
  so->abfd = so->objfile->obfd;

  /* jini: mixed mode support: JAGag21714 */
#ifdef HP_IA64
  solib_desc = (struct load_module_desc *)(so->solib_desc);
  so->objfile->text_low = solib_desc->text_base;
  so->objfile->text_high = solib_desc->text_base + solib_desc->text_size;
  so->objfile->data_start = solib_desc->data_base;
  so->objfile->data_size = solib_desc->data_size;
#else
  solib_desc = (struct som_solib_mapped_entry *)(so->solib_desc);
  so->objfile->text_low = solib_desc->text_addr;
  so->objfile->text_high = solib_desc->text_end;
  so->objfile->data_start = solib_desc->data_start;
  so->objfile->data_size = solib_desc->data_end - solib_desc->data_start;
#endif

  /* Mark this as a shared library and save private data.
   */
  so->objfile->flags |= OBJF_SHARED;

#ifdef HP_IA64
  if (so->objfile->obj_private == NULL)
    {
      obj_private = (hppa_obj_private_data_t *)
	obstack_alloc (&so->objfile->psymbol_obstack,
		       sizeof (hppa_obj_private_data_t));
      obj_private->unwind_info = NULL;
      obj_private->so_info = NULL;
      so->objfile->obj_private = (PTR) obj_private;
    }
  obj_private = (hppa_obj_private_data_t *) so->objfile->obj_private;
  obj_private->so_info = so;

#else
  if (so->objfile->obj_private == NULL)
    {
      obj_private = (obj_private_data_t *)
	obstack_alloc (&so->objfile->psymbol_obstack,
		       sizeof (obj_private_data_t));
      obj_private->unwind_info = NULL;
      obj_private->so_info = NULL;
      so->objfile->obj_private = (PTR) obj_private;
    }
  obj_private = (obj_private_data_t *) so->objfile->obj_private;
  obj_private->so_info = so;
#endif


  if (!bfd_check_format (so->abfd, bfd_object))
    {
      error ("\"%s\": not in executable format: %s.",
	     name, bfd_errmsg (bfd_get_error ()));
    }
  
}


void
som_solib_load_symbols (struct so_list *so,
                        char *name,
                        int from_tty,
                        CORE_ADDR text_addr,
                        struct target_ops *target)
{
  struct section_table *p;
  int status;
  char buf[4];
  CORE_ADDR presumed_data_start;

#ifdef SOLIB_DEBUG
  printf ("--Adding symbols for shared library \"%s\"\n", name);
#endif

#ifdef HP_IA64
  /* JAGag21714:
     jini: mixed mode changes: Sept 2006: Not setting this to true has the
     effect of "(symbols not loaded)" getting displayed against the PA libraries
     for the mixed mode scenario for the "info shared" command. */
  so->symbols_loaded = true;
#endif

  som_solib_add_solib_objfile (so, name, from_tty, text_addr, 0);

  /* Now we need to build a section table for this library since
     we might be debugging a core file from a dynamically linked
     executable in which the libraries were not privately mapped.  */
  if (build_section_table (so->abfd,
			   &so->sections,
			   &so->sections_end))
    {
      error ("Unable to build section table for shared library\n.");
      return;
    }
 
 /* For core files detect if there is a mismatch between the libraries
   * on the system and the ones in the core.
   */
  if (target_has_stack && !target_has_execution)
      hppa_core_so_mismatch_detection (so); 

  /* Relocate all the sections based on where they got loaded.  */
  for (p = so->sections; p < so->sections_end; p++)
    {
      if (p->the_bfd_section->flags & SEC_CODE)
	{
	  p->addr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_TEXT (so->objfile));
	  p->endaddr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_TEXT (so->objfile));
	}
      else if (p->the_bfd_section->flags & SEC_DATA)
	{
	  p->addr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_DATA (so->objfile));
	  p->endaddr += ANOFFSET (so->objfile->section_offsets, SECT_OFF_DATA (so->objfile));
	}
    }

  /* Now see if we need to map in the text and data for this shared
     library (for example debugging a core file which does not use
     private shared libraries.). 

     Carefully peek at the first text address in the library.  If the
     read succeeds, then the libraries were privately mapped and were
     included in the core dump file.

     If the peek failed, then the libraries were not privately mapped
     and are not in the core file, we'll have to read them in ourselves.  */
  status = target_read_memory (text_addr, buf, 4);
  if (status != 0)
    {
      int old, new;

      new = so->sections_end - so->sections;
      
      old = target_resize_to_sections (target, new);
      
      /* Copy over the old data before it gets clobbered.  */
      memcpy ((char *) (target->to_sections + old),
	      so->sections,
	      ((sizeof (struct section_table)) * new));
    }
}


CORE_ADDR dld_text_offset = 0;
CORE_ADDR dld_data_offset = 0;
int dld_mmap_calls = 0;

static char *gdb_shlib_root = 0;

#ifdef HP_IA64
extern char **gdb_shlib_path_list;
extern int gdb_shlib_path_length;
#else
char **gdb_shlib_path_list = 0;
int gdb_shlib_path_length = 0;
#endif

int threshold_warning_given = 0;

#ifdef LOG_BETA_FILTERED_LIBRARIES
int logged_filtered_libraries = 0;
#endif

/*JAGae68084 */
extern int if_yes_to_all_commands;
char * shllib_name;

/* jini: Converted the type of loaded_shared from 'int' to 'boolean'. */
boolean loaded_shared;
int in_shl_load;

boolean 
pa32_this_solib_address (void *solib, CORE_ADDR addr)
{
  struct so_list *so = (struct so_list *) solib;
  struct som_solib_mapped_entry *solib_desc =
         (struct som_solib_mapped_entry *)(so->solib_desc);

  /* Is this address within this shlib's text range? */
  return (   (addr >= solib_desc->text_addr)
          && (addr < solib_desc->text_end));
}

/* JAGag21714:
   jini: Mixed mode support: Do not include unnecessary functions for
   the IPF build */
#ifndef HP_IA64
/* process a shared library, given the address of its mapped_shl_entry
   structure */
static void
process_a_shared_library (CORE_ADDR addr, int from_tty,
			  struct target_ops *target)
{
  int status;
  char buf[4];
  CORE_ADDR name_addr, text_addr, data_addr;
  unsigned int name_len;
  char name_buf[MAXPATHLEN + 1];
  char *name = name_buf;
  char *cursor, *absolute_name = NULL;
  int chunk;
  struct so_list *new_so;
  struct so_list *so_list = so_list_head;
  struct stat statbuf;
  int st_size;
  int is_main_program;
  int desc;
  struct so_list *so_list_tail,*saved_so;
  unsigned char handle_ver;
  extern boolean is_current_fixed_library (char *);
  extern boolean is_fixed_library (char *);
  int gdb_shlib_list_index = 0;
  /* JAGag21714:
     jini: mixed mode changes: Aug 2006: Converted 'loaded' to be of type
     boolean to be compatible with the common so_list structure */
  boolean loaded = true;
  struct som_solib_mapped_entry *solib_desc = NULL;

  /* defined in source.c */
  extern absolute_path_of (char *, char **);

  /* Bindu: The first 40 bytes of mapped_shl_entry structure that dld gave us
   * is what we are interested in. So read in these 40 bytes in one go.
   */
  char mapped_shl_buf[40];
  char mapped_shl_buf_block2[12];
  status = target_read_memory (addr,mapped_shl_buf, 40); 
  if (status != 0)
    goto err;

/* JAGaf16317 - Relocated the code to determine whether it is the main program */
/* If we've already loaded this one or it's the main program, skip it.  */
#if 0
  is_main_program = (strcmp (name, symfile_objfile->name) == 0);
#endif

/* Baskar, JAGae56408
     The loadmodule name in mapped_shl_entry and from ttrace(TT_PROC_GET
     _PATHNAME) aren't same for the main program. Process A (a.out) forks
     process B and performs exec("b.out"). Process B's mapped_shl_entry->name
     points to "b.out", but the ttrace() returns "./b.out". So comparing
     names to identify main-program won't give the correct result, and b.out
     gets added to so-list. Using the text_addr to identify the main program.
*/

  memcpy(buf, mapped_shl_buf+8, 4);
  text_addr = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 20, 4);
  data_addr = extract_unsigned_integer (buf, 4);

  is_main_program = (text_addr == 0x00001000 || data_addr == 0x40001000) ? 1 : 0;
/* JAGaf16317 - ends */

  /* Bindu: The name_len was once calculated using a complicated
   * algorithm which required reading the name twice and with large
   * number of ptrace calls. Now we use MAXPATHLEN to allocate the
   * name and use a binary search type algorithm to read in the name.
   */

  /* Get a pointer to the name of this library.  */
  memcpy(buf, mapped_shl_buf, 4);
  name_addr = extract_unsigned_integer (buf, 4);

  /* RM: If execl is called with argv0 = NULL, the name_addr for
     the main image seems to be set to NULL. Don't error out, just
     continue down the rest of the list */
  if (!name_addr)
    return;

  chunk = MAXPATHLEN + 1;
  cursor = name;
  while (chunk != 0)
    {
      status = target_read_memory (name_addr, cursor, chunk);
      if (status != 0)
        {
	  status = target_read_memory (name_addr, cursor, chunk/2);
 	  if (status != 0)
            {
	      chunk = chunk/2;
	    }
	  else
	    {
	      chunk = chunk/2;
	      name_addr += chunk;
	      cursor += chunk;
	    }
        }
      else
        {
	  break;
        }
    }

  if ((chunk == 0) && (name == cursor))
    goto err;
  for (cursor = name; cursor < name + MAXPATHLEN + 1; cursor++)
    if (*cursor == '\0')
      goto done;
  goto err;

done:
  name_len = strlen(name) + 1; 
  /* RM: sometimes name doesn't seem to be an absolute path
     name. Try this hack to get the full pathname */
  if (name && (*name != '/'))
    {
      /* JAGaf16317 - use absolute path of main program */
      if (is_main_program && symfile_objfile->name && (*symfile_objfile->name == '/'))
          absolute_name = symfile_objfile->name;

      if (absolute_name == NULL || *absolute_name != '/')
      {
      desc = openp (getenv ("PATH"), 1, name, O_RDONLY | O_BINARY, 0,
		    &absolute_name);
      if (desc < 0)
        {
	  /* Baskar JAGae51552 try using absolute_path_of to get the absolute path name */
	  if (!absolute_path_of (name, &absolute_name))
	    {
	      /* skip this library */
	      warning ("Can't find file %s referenced in dld_list.\n", name);
	      /* JAGae70228 - Commented return, to make a way for the shared library to be added to the list 
	        even if it is not available in the given path */
	     // return; 
	    }
	}
      else
        {
	  close(desc);
	}
      }
      name = absolute_name;
      /* JAGae68084 - capture the shared library name */
      if (shllib_name)
        free (shllib_name);  
      shllib_name = strdup (name); 

      name_len = (name ? (strlen(name) + 1) : 0); 
    }

  /* See if we've already loaded something with this name.  */
  saved_so = NULL;
  while (so_list)
    {
      solib_desc = (struct som_solib_mapped_entry *)(so_list->solib_desc);
      if ((name ? !strcmp (solib_desc->name, name) : 0))
        {
          saved_so = so_list;
          break;
        } 
      so_list = so_list->next;
    }

  /* See if the file exists.  If not, give a warning, but don't die.  */
  /* RM: Don't bother if we have already seen this file */
  if (!so_list)
    {
      if (is_fixed_library (name) && !is_current_fixed_library (name))
        return;

      /* srikanth, JAGab25944, 001117. If gdb_shlib_path_list is defined and
         we are debugging core files, treat that directory as the place
         where the library will be found. This is useful for analyzing
         core files on a system other than the one that produced the
         core without having to create a hierarchy as the gdb_shlib_root
         method requires.

	 stacey, JAGab25944, 6/29/2001
	 the for loop below enhances gdb to search multiple directories
	 for the appropriately named shared library.  The gdb_shlib_path_list
	 variable is a pointer to an array of path names.  Which are obtained
	 from the user's GDB_SHLIB_PATH environment variable.  GDB will search
	 the paths in the order in which they are entered by the user: from
	 first to last.  So if the user types:

	 export GDB_SHLIB_PATH=libs1:libs2:libs3
	 
	 GDB will search libs1, then libs2, and finally libs3 and will stop
	 as soon as it finds the library it wants.  Note: this implementation
	 assumes that all library names are unique. */

      if (gdb_shlib_path_list && !target_has_execution)
        {
          static char copy[MAXPATHLEN + 1];

	  for (gdb_shlib_list_index=0; 
	       gdb_shlib_list_index < gdb_shlib_path_length; 
	       gdb_shlib_list_index++)
	    {
	      sprintf (copy, "%s%s", 
		       gdb_shlib_path_list[gdb_shlib_list_index], 
		       strrchr (name, '/'));

	      if (stat(copy, &statbuf) != -1)
		{
		  name = copy;
		  name_len = strlen(copy) + 1;
		  break;
		}
	    }
        }
      else
      if (gdb_shlib_root && !target_has_execution)
        {
          static char altname[MAXPATHLEN + 1];
          /* RM: If we find this library in gdb_shlib_root _and_ we are debugging
             core files, use that version instead. Useful when analyzing core files
             on a system other than the one that produced the core
          */
          strcpy(altname, gdb_shlib_root);
          strcat(altname, name);
          status = stat(altname, &statbuf);
          if (status != -1) {
            name = altname;
            name_len = strlen(altname) + 1;
          }
        }
      status = (name ? stat (name, &statbuf) : 0);
      if (status == -1)
        {
	  warning ("Can't find file %s referenced in dld_list.\n", name);
          /* JAGae70228 -  Commented return, to make a way for the shared library to be added to the list 
	        even if it is not available in the given path */
	 // return;
	}
    }

  if (so_list || is_main_program)
    {
     /* The below code added for JAGaa80453 - "info shared" should not display 
        unloaded shared libraries */
       if (som_solib_have_load_event (inferior_pid))
	loaded = true;
       else
        loaded = false;
         
      if (so_list)
          so_list->loaded = loaded;
      /* Record the main program's symbol table size. */
      if (is_main_program && !so_list && !main_st_size_added)
        {
	  main_st_size_added = true;
	  st_size = som_solib_sizeof_symbol_table (name);
	  som_solib_total_st_size += st_size;
	}

      /* Was this a shlib that we noted but didn't load the symbols for?
	 If so, were we invoked this time from the command-line, via
	 a 'sharedlibrary' or 'add-symbol-file' command?  If yes to
	 both, we'd better load the symbols this time.
      */
      if (from_tty && so_list && !is_main_program)
	if (re_exec(name))  /* CLLbs15382 */
	  if (so_list->objfile == NULL)
	    {
	      som_solib_load_symbols (so_list,
				      name,
				      from_tty,
				      ((struct som_solib_mapped_entry *)
                                        (so_list->solib_desc))->text_addr,
				      target);
	    }
	  else if ((so_list->objfile != NULL) &&
		   (so_list->objfile->psymtabs == NULL) &&
		   (so_list->objfile->sf->sym_add_psymtabs != NULL))
	    {
	      /* RM: we now do a fair amount of processing (reading
		 unwind info, linker symbol table) even if the
		 threshold is exceeded. We only want to read in the
		 actual debug information when we come here, the
		 rest should already have been done */
              /* Srikanth, if automatically loading shared libraries,
                 do not issue this message, this screws up the GUI
                 annotations. Jul 30th 2002.
              */
              if (!auto_loading)
	        printf("--Adding symbols for shared library \"%s\"\n",
		         name);
	      (*so_list->objfile->sf->sym_add_psymtabs)
		(so_list->objfile, 0);
	    }
      if (!is_main_program)
        {
          if (!loaded)
            {
              new_so = so_list;
              goto process_implementation_libs;
            }
        }
      else
        return;
    }

#if 0 
  memcpy(buf, mapped_shl_buf+8, 4);
  text_addr = extract_unsigned_integer (buf, 4);
#endif

   if (saved_so == NULL)
     {
       new_so = (struct so_list *) xmalloc (sizeof (struct so_list));
       memset ((char *) new_so, 0, sizeof (struct so_list));
       new_so->solib_desc = xcalloc (1, sizeof (struct som_solib_mapped_entry));
       solib_desc = (struct som_solib_mapped_entry *)(new_so->solib_desc);
       solib_desc->name = xmalloc (name_len);
       if (name)
         strcpy (solib_desc->name, name);
       /* JAGag28077 - Save dld's name. The variable "dld_name" is 
          used in "foreach_text_minsym".  if this variable is set we 
          skip looking for symbols in dld. If this is not set then we try to 
          resolve the deferred breakpoint to dld's, because 
          dld is the first library to be loaded. This is wrong. 
          PA64 uses dlgetmodinfo() to get dld's name. If the equivalent of the same 
          becomes avaiable sometime later. Please change it here!
          For more information - see comments in minsyms.c:foreach_text_minsym.
       */
       if (dld_name == NULL && strstr (solib_desc->name, "dld.sl") != NULL)
         dld_name = xstrdup (solib_desc->name);

       /* JAGae68084 -Set this flag to say that a new library has been 
       added to the list of exsisting libraries*/
       loaded_shared = loaded;
       if (so_list_head == NULL)
         so_list_head = new_so;  
       else
         {
           so_list_tail = so_list_head;
          /* Find the end of the list of shared objects.  */
           while (so_list_tail && so_list_tail->next)
             so_list_tail = so_list_tail->next;
           so_list_tail->next = new_so;
         }
    }
   else
     new_so = saved_so;

  solib_desc = (struct som_solib_mapped_entry *)(new_so->solib_desc);
  /* Fill in all the entries in GDB's shared library list. */
  new_so->desc_addr = addr;
  new_so->loaded = loaded;
  memcpy(buf, mapped_shl_buf+4, 4);

  solib_desc->struct_version = extract_unsigned_integer (buf, 1);
  solib_desc->bind_mode = extract_unsigned_integer (buf + 2, 1);

  /* Following is "high water mark", highest version number
   * seen, rather than plain version number.
   */
  solib_desc->library_version = extract_unsigned_integer (buf, 2);

  /* Q: What about longword at "addr + 8"?
       * A: It's read above, out of order, into "text_addr".
       */
  solib_desc->text_addr = text_addr;

  memcpy(buf, mapped_shl_buf + 12, 4);
  solib_desc->text_link_addr = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 16, 4);
  solib_desc->text_end = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 20, 4);
  solib_desc->data_start = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 24, 4);
  solib_desc->bss_start = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 28, 4);
  solib_desc->data_end = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 32, 4);
  solib_desc->got_value = extract_unsigned_integer (buf, 4);

  memcpy(buf, mapped_shl_buf + 36, 4);
  solib_desc->next = (void *) extract_unsigned_integer (buf, 4);
 
  status = target_read_memory (addr+312, mapped_shl_buf_block2, 12); 
  if (status != 0)
    goto err;
  
  memcpy(buf, mapped_shl_buf_block2, 4);
  solib_desc->gdb_info_offset = extract_unsigned_integer(buf, 4);
  
  memcpy(buf, mapped_shl_buf_block2 + 4, 4);
  solib_desc->file_time.secs = extract_unsigned_integer(buf, 4);

  memcpy(buf, mapped_shl_buf_block2 + 8, 4);
  solib_desc->file_time.nanosecs = extract_unsigned_integer(buf, 4);
 
  if (target_has_stack && !target_has_execution)
     so_pathname_g = solib_desc->name; 

  /* Note that we don't re-set "addr" to the next pointer
       * until after we've read the trailing data.
       */

/* Bindu: Do not read the tsd_start_addr for 10.20.
 */
#ifndef NO_KERNEL_THREADS
  status = target_read_memory (addr + TSD_START_ADDR_OFFSET, buf, 4);
  solib_desc->tsd_start_addr = extract_unsigned_integer (buf, 4);
  if (status != 0)
    goto err;
  status = target_read_memory (addr + TSD_INDEX, buf, 4);
  solib_desc->tsd_index = extract_unsigned_integer (buf, 4);
  if (status != 0)
    goto err;
#endif

#ifdef SOLIB_DEBUG
  printf ("\n+ library \"%s\" is described at 0x%x\n", name, addr);
  printf ("  'version' is %d\n", solib_desc->struct_version);
  printf ("  'bind_mode' is %d\n", solib_desc->bind_mode);
  printf ("  'library_version' is %d\n", solib_desc->library_version);
  printf ("  'text_addr' is 0x%x\n", solib_desc->text_addr);
  printf ("  'text_link_addr' is 0x%x\n", solib_desc->text_link_addr);
  printf ("  'text_end' is 0x%x\n", solib_desc->text_end);
  printf ("  'data_start' is 0x%x\n", solib_desc->data_start);
  printf ("  'bss_start' is 0x%x\n", solib_desc->bss_start);
  printf ("  'data_end' is 0x%x\n", solib_desc->data_end);
  printf ("  'got_value' is %x\n", solib_desc->got_value);
  printf ("  'next' is 0x%x\n", solib_desc->next);
  printf ("  'tsd_start_addr' is 0x%x\n", solib_desc->tsd_start_addr);
  printf ("  'tsd_index' is 0x%x\n", solib_desc->tsd_index);
#endif

  /* At this point, we have essentially hooked the shlib into the
     "info share" command.  However, we haven't yet loaded its
     symbol table.  We must now decide whether we ought to, i.e.,
     whether doing so would exceed the symbol table size threshold.

     If the threshold has just now been exceeded, then we'll issue
     a warning message (which explains how to load symbols manually,
     if the user so desires).

     If the threshold has just now or previously been exceeded,
     we'll just add the shlib to the list of object files, but won't
     actually load its symbols.  (This is more useful than it might
     sound, for it allows us to e.g., still load and use the shlibs'
     unwind information for stack tracebacks.) */

  /* Note that we DON'T want to preclude the user from using the
     add-symbol-file command!  Thus, we only worry about the threshold
     when we're invoked for other reasons.
  */

  /* Stacey 10/17/2001 
     If there's a problem with this library, inform the user what went wrong
     and return to make sure we don't end up 

     1.  erroring out before we try to load other libraries
     2.  doing useless work - i.e. trying to load symbols 
         from a file we have determined doesn't exist.  

     som_solib_sizeof_symbol_table returns 0 when all is well and 
     catch_errors returns 0 when there are errors or the value of the
     function it called otherwise.  The return value from catch_errors
     won't tell us if there was an error in this case.  So I've introduced 
     a new global error_in_shlib_symtab that will be set to 1 if an error 
     occurs inside som_solib_sizeof_symbol_table and will be 0 otherwise. */

  st_size = 
    catch_errors ( (catch_errors_ftype *) som_solib_sizeof_symbol_table,
		   name, NULL, RETURN_MASK_ALL);
  if (error_in_shlib_symtab)
    {
      /* reset this for future use */
      error_in_shlib_symtab = 0;
      return;
    }

  som_solib_st_size_threshold_exceeded =
    !from_tty &&
    ((st_size + som_solib_total_st_size) > (auto_solib_add * (LONGEST) 1000000));

  if (!is_current_fixed_library (name) && som_solib_st_size_threshold_exceeded)
    {
      if (!threshold_warning_given)
	warning ("Symbols for some libraries have not been loaded, because\ndoing so would exceed the size threshold specified by auto-solib-add.\nTo manually load symbols, use the 'sharedlibrary' command.\nTo raise the threshold, set auto-solib-add to a larger value and rerun\nthe program.\n");
      threshold_warning_given = 1;

      /* RM: 980929 We still want to read some stuff: unwind
       * tables for instance. Not being able to do a backtrace is
       * unacceptable */
          
      /* DTS CHFts24108 980630 coulter
	 Calling som_solib_add_solib_objfile defeats the purpose of the
	 limit becase the first thing that it does is call 
	 symbol_file_add which reads in all of the debug information.
	 We won't have any debug info on the shared library and won't
	 be able to do a traceback through it.  The call is commented out.
      */

      /* We'll still make note of this shlib, even if we don't
	 read its symbols.  This allows us to use its unwind
	 information well enough to know how to e.g., correctly
	 do a traceback from a PC within the shlib, even if we
	 can't symbolize those PCs...
      */
      som_solib_add_solib_objfile (new_so, name, from_tty, text_addr, 1);
    }
  else
    {
      som_solib_total_st_size += st_size;

      /* This fills in new_so->objfile, among others. */

      /* 
       * srikanth, CLLbs15382, 980818
       * if we are here as a result of a sharedlibrary command,
       * load the symbols only from those libraries that are
       * selected by the regex argument to the command ...
       *
       */

      if (re_exec(name))
	som_solib_load_symbols (new_so, name, from_tty, text_addr, target);
    }
process_implementation_libs:  

  /* RM: Does this dld support filter libraries */
  memcpy(buf, mapped_shl_buf + 4, 1);
  handle_ver = extract_unsigned_integer (buf, 1);
  
  if (!is_main_program && handle_ver > 1)
    { 
      unsigned int dl_header[1024];
      CORE_ADDR impl_lib_addr_array;
      CORE_ADDR impl_lib_addr;
      long dl_header_offset;
      long dl_header_ext_size;
      long n_impl_libs;
      asection *text_section;
      long i;
      unsigned int dl_header_flags;

      /* RM: Process any implementation libraries if this is a filter
         library */

      /* srikanth, 000713, JAGad12819 thou shalt not covet thy
         neighbor's bytes lest they take cruel vengence upon thee when
         thou least expect it ... There are two problems with this
         piece of code : First, we used to assume that dl_header_offset
         is accessible at offset 172 from a mapped_shl_entry. Only the
         first 40 bytes of this structure are guaranteed not to
         change. Second, we used to assume that dl_header_extension
         is always present. This is not true of libraries linked
         using older linkers.
      */

      text_section = bfd_get_section_by_name (new_so->objfile->obfd,
                                              "$SHLIB_INFO$");
      if (!text_section)
        return;

      /* Read the dl_header at offset 0 in $TEXT$. Actually we
         are reading w.r.t $SHLIB_INFO$ which is the first subspace
         in the TEXT space. It is the same thing ... The standard
         header contains 28 words.
      */
      bfd_get_section_contents (new_so->objfile->obfd, text_section, 
                                dl_header, 0, 28 * sizeof (int));

      /* Now check if filtered library mechanism is enabled for
         this module. This is done by consulting dl_header.flags
         (upper half of 18th word)
      */ 
      
      dl_header_flags = dl_header[18];
      dl_header_flags &= 0xffff;

      if ((dl_header_flags & FILTER_LIBS_ENABLED) == 0)
        return;
      
      /* Now we know that an extension header must exist and it
         must be atleast 40 bytes in size ...
      */

      bfd_get_section_contents (new_so->objfile->obfd, text_section,
                                dl_header, 0, 38 * sizeof (int));

      dl_header_ext_size = dl_header[28];

      if (dl_header_ext_size < 40)
        n_impl_libs = 0;
      else
        n_impl_libs = dl_header[36];
      
#ifdef LOG_BETA_FILTERED_LIBRARIES
      if (!logged_filtered_libraries && n_impl_libs > 0)
        {
          logged_filtered_libraries = 1;
          log_test_event(LOG_BETA_FILTERED_LIBRARIES, n_impl_libs);
        }
#endif
      status = target_read_memory (addr+196, buf, 4);
      if (status != 0)
        goto err;
      
      impl_lib_addr_array = extract_unsigned_integer (buf, 4);
      
      for (i = 0; i < n_impl_libs; i++)
        {
          status =
            target_read_memory (impl_lib_addr_array + i*sizeof(CORE_ADDR),
                                buf, 4);
          if (status != 0)
            goto err;
          
          impl_lib_addr = extract_unsigned_integer (buf, 4);

          if (impl_lib_addr != 0)
            {
              process_a_shared_library(impl_lib_addr, from_tty, target);
            }
        }
    }

  return;
  
err:
  error ("Error while reading dynamic library list.\n");
  return;
}

/* Bindu: Load the shared libraries specified by the sharedlibrary 
 * command. Previouly, this used to be done using som_solib_add, which
 * goes through the list of shared libraries using the dld's data 
 * structure, which requires mutiple ptrace calls.
 */
void
som_solib_add_sharedlibrary (arg_string, from_tty, target)
     char *arg_string;
     int from_tty;
     struct target_ops *target;
{
  struct so_list *so_list = so_list_head;
  char *re_err;
  struct som_solib_mapped_entry *solib_desc = NULL;

  /* First validate our arguments.  */
  if ((re_err = re_comp (arg_string ? arg_string : ".")) != NULL)
    {
      error ("Invalid regexp: %s", re_err);
    }

  while (so_list)
    {
      solib_desc = (struct som_solib_mapped_entry *)(so_list->solib_desc);
      if (re_exec(solib_desc->name))
        {
          if (so_list->objfile == NULL)
            {
              som_solib_load_symbols (so_list,
                                      solib_desc->name,
                                      from_tty,
                                      solib_desc->text_addr,
                                      target);
            }
          else if ((so_list->objfile != NULL) &&
                   (so_list->objfile->psymtabs == NULL) &&
                   (so_list->objfile->sf->sym_add_psymtabs != NULL))
            {
              /* RM: we now do a fair amount of processing (reading
                 unwind info, linker symbol table). We only want to 
		 read in the actual debug information when we come here,
		 the rest should already have been done */
              /* Srikanth, if automatically loading shared libraries,
                 do not issue this message, this screws up the GUI
                 annotations. Jul 30th 2002.
              */
              if (!auto_loading)
                printf("--Adding symbols for shared library \"%s\"\n",
                       solib_desc->name);
              (*so_list->objfile->sf->sym_add_psymtabs)
                (so_list->objfile, 0);
            }
 
	    /* Process implementatation libraries */
	    /* Bindu: Implementation libs are not handled yet. FIXME. */
        }
      so_list = so_list->next;
    }
  return;
}

/* JAGaf87574 - Unable to continue after hitting a bkpoint in dynamically unloaded shlib*/

typedef struct catch_load_unload_event
  {
    struct breakpoint *breakpt;
    char *filename;
    struct catch_load_unload_event *next;
  }
catch_load_unload_event_t;

boolean
pa32_loaded_solib_address (CORE_ADDR addr)
{
  struct so_list *so = so_list_head;
  while (so)
   {
     if (pa32_this_solib_address((void*) so, addr) && so->loaded)
       return true;
     so = so->next;
   }

  if (addr >= text_start && addr < text_end)
    return true;

  return false;
}

static void clear_solib_info(struct so_list *solib)
{
  struct symtab *s;

  /* Diable breakpoints in this shlib */
  disable_breakpoints_in_shlibs(!info_verbose, (void*) solib);

  /* Clear current_source_symtab, if applicable */
  ALL_OBJFILE_SYMTABS(solib->objfile, s)
    {
      if (current_source_symtab == s) current_source_symtab = NULL;
    }
}

static int
handle_dynlink_unload_event (int pid)
{
  catch_load_unload_event_t *catch_event;
  static struct so_list *so_list_tail;
  catch_load_unload_event_t *catch_unload_list = NULL;
  boolean found_solib;
  struct so_list *next_solist;
  static struct so_list *prev_solist_p;
  static struct so_list **prev_solist_pp;
  char *dll_path;
  CORE_ADDR dll_path_addr;

  dll_path = som_solib_library_pathname(pid); 
  if (dll_path && (*dll_path != '/'))
    absolute_path_of (dll_path, &dll_path);

  /* Mark the shared library as unloaded by setting loaded to false.*/

  found_solib = false;
  next_solist = so_list_head;
  prev_solist_p = NULL;
  prev_solist_pp = &so_list_head;
  while (next_solist && !found_solib)
    {
      struct som_solib_mapped_entry *solib_desc =
             (struct som_solib_mapped_entry *)(next_solist->solib_desc);
      if (strcmp (solib_desc->name, dll_path) == 0)
        {
          found_solib = true;
          next_solist->loaded = false;

          /* If we never loaded debug info for this library, we can
             forget about it and remove it from the so_list_head list.
           */

          if (next_solist->objfile == NULL)
            {
              /* This might update so_list_head */
              *prev_solist_pp = next_solist->next;
              if (so_list_tail == next_solist)
                so_list_tail = prev_solist_p;
            }
        }                       /* if strcmp == 0 */
      prev_solist_pp = &next_solist->next;
      prev_solist_p = next_solist;
      next_solist = next_solist->next;
    }                           /* end while */

  if (found_solib)
    {
      if (!prev_solist_p->objfile)
        {
          free (prev_solist_p->sections);
          free (((struct som_solib_mapped_entry *)
                 (prev_solist_p->solib_desc))->name);
          free (prev_solist_p->solib_desc);
          free (prev_solist_p);
        }
      else
        {
          /* Disable the watchpoint expressions(watch on shared lib space)
             that aren`t valid after unloading shlib and removing debug
             information */
          disable_break_exp_chain (prev_solist_p->objfile);
          /* clear debug info, breakpoints, etc. */
          clear_solib_info(prev_solist_p);
          if (rtc_dot_sl == prev_solist_p->objfile)
            rtc_dot_sl = NULL;
          free_objfile(prev_solist_p->objfile);
          prev_solist_p->objfile = NULL;
        }
    }

  return 0;                     /* don't stop for input */
}                               /* end handle_dynlink_unload_event */

/* JAGaf54436: Removed function create_break_shared_load_bps() */

/* Add symbols from shared libraries into the symtab list, unless the
   size threshold (specified by auto_solib_add, in megabytes) would
   be exceeded.  */

void
som_solib_add (arg_string, from_tty, target)
     char *arg_string;
     int from_tty;
     struct target_ops *target;
{
  struct minimal_symbol *msymbol;
  CORE_ADDR addr = 0;
  asection *shlib_info;
  int status;
  unsigned int dld_flags;
  char buf[4], *re_err;
  struct so_list *next_solist;

  /* First validate our arguments.  */
  if ((re_err = re_comp (arg_string ? arg_string : ".")) != NULL)
    {
      error ("Invalid regexp: %s", re_err);
    }

  /* If we're debugging a core file, or have attached to a running
     process, then som_solib_create_inferior_hook will not have been
     called.

     We need to first determine if we're dealing with a dynamically
     linked executable.  If not, then return without an error or warning.

     We also need to examine __dld_flags to determine if the shared library
     list is valid and to determine if the libraries have been privately
     mapped.  */
  if (symfile_objfile == NULL)
    return;

  /* First see if the objfile was dynamically linked.  */
  shlib_info = bfd_get_section_by_name (symfile_objfile->obfd, "$SHLIB_INFO$");
  if (!shlib_info && !info_hp_aries_pa32)
    return;

  /* It's got a $SHLIB_INFO$ section, make sure it's not empty.  */
  if (!info_hp_aries_pa32 &&
      bfd_section_size (symfile_objfile->obfd, shlib_info) == 0)
    return;

  /* RM: don't error out, we have other sneaky ways of finding dld
     stuff */
  msymbol = lookup_minimal_symbol ("__dld_flags", NULL, NULL);
  if (msymbol != NULL)
    {
      addr = SYMBOL_VALUE_ADDRESS (msymbol);
      /* Read the current contents.  */
      status = target_read_memory (addr, buf, 4);
      if (status != 0)
        {
         /* JAGaf08040 - <START> - Make the error msg user-friendly. */
	  error ("Unable to read dynamic library list for %s. If you are debugging a core file,it is likely that it is corrupt.If it is a live process,it may have become a zombie losing its address space.If neither of these is the case,report this to HP.\n",symfile_objfile->name);
	 /* JAGaf080840 - <END> */ 
	  return;
	}
      dld_flags = extract_unsigned_integer (buf, 4);
      dld_has_map_private_flag = dld_flags & DLD_FLAGS_MAPPRIVATE;
      /* __dld_list may not be valid.  If not, then we punt, warning
	 the user if we were called as a result of the add-symfile
	 command. */
      if (!info_hp_aries_pa32 && (dld_flags & DLD_FLAGS_LISTVALID) == 0)
        {
	  if (from_tty)
	    error ("__dld_list is not valid according to __dld_flags.\n");
	  return;
	}
      if (attach_flag)
	{
      		unsigned int dl_header[19];
      		asection *text_section;
      		text_section = bfd_get_section_by_name (symfile_objfile->obfd, "$SHLIB_INFO$");
      		if (!text_section)
       			return;
      		bfd_get_section_contents (symfile_objfile->obfd, 
			text_section, dl_header, 0, 19 * sizeof (int));
      		dlheader_has_map_private_flag = dl_header[18] & SHLIB_TEXT_PRIVATE_ENABLE;
	}
      /* If the libraries were not mapped private, warn the user.
         Warning should be thrown only for running processes and not for core files. (JAGaf08125)  */
      if ((!batch_rtc) && (target_has_execution) && (!info_hp_aries_pa32 && (dld_has_map_private_flag || dlheader_has_map_private_flag)) == 0)
        if (profile_on)
          {
            /* We used to exit here with an error message. This was because
               command line calls didn't work quite well when shared libraries
               are not privately mapped. But with prospect v2.6, we don't
               rely on command line calls any more, so I got rid of the
               block here. The `if' is still needed to avoid the warning
               message below which is not useful for profiling mode.

                         - Srikanth, Nov 16th 2005.
            */
          }
        else
	  {
	    puts_unfiltered ("\n");
            warning ("The shared libraries were not privately mapped; setting a\nbreakpoint in a shared library will not work until you rerun the program; stepping over longjmp calls will not work as expected.\n");

	  }
    }

  /* srikanth, 000130, disable the internal breakpoint placed at _start,
     as it will never be hit after this. This breakpoint was placed
     there because the som dynlinker does not notify us when its maps
     in the implicitly linked libraries. For explicitly shl_loaded
     libraries, we still have a breakpoint planted at __d_trap and are
     in good shape.

     It would be great if we could figure out if the inferior *could*
     call shl_load either directly or indirectly and disable the
     breakpoint at __d_trap too, but unfortunately, libCsup.sl and
     libc.sl have shl_load in the dynld import list and the check would
     always fail. We could perhaps indetify the entry points from these
     libraries that call shl_load and look for them ? I am leaving it
     for later ... 
  */

  if (stop_pc == _start_addr && bp_at_start != 0)
    disable_breakpoint (bp_at_start);

  /* If we got here due to a shl_load call, do not walk the entire
     library list. This is awful in cases where the program loads
     hundreds of shared libraries : the quadratic nature of the
     algorithm basically makes life miserable for the user. Instead
     use the information passed by dld to __d_trap -- srikanth, 010502.
  */
  if (target_has_execution)
  {
    struct minimal_symbol * m;

    m = lookup_minimal_symbol_by_pc (stop_pc);
    if (m && !strcmp (SYMBOL_NAME (m),  "__d_trap"))
      {
        if (som_solib_have_load_event (inferior_pid) || som_solib_have_unload_event (inferior_pid))
          {
            addr = read_register (ARG1_REGNUM);
            process_a_shared_library(addr, from_tty, target);
            if (som_solib_have_unload_event (inferior_pid))
               handle_dynlink_unload_event(inferior_pid);

            /*JAGae68084 - If we seee a shl_load event set in_shl_load */
            if(if_yes_to_all_commands && som_solib_have_load_event (inferior_pid))
              {
               in_shl_load=1;
              }
             
            goto done_digestion;
          }
        /* We don't handle shl_unloads !!!!!! FIXME */
      }
  }

  msymbol = lookup_minimal_symbol ("__dld_list", NULL, NULL);
  if (!msymbol)
    {
      /* Older crt0.o files (hpux8) don't have __dld_list as a symbol,
         but the data is still available if you know where to look.  */
      msymbol = lookup_minimal_symbol ("__dld_flags", NULL, NULL);
      if (!msymbol)
	{
          /* RM: May be a stripped executable. Try seeing if it has a
             $GLOBAL$ subspace as a last ditch effort */
          asection *global =
            bfd_get_section_by_name(symfile_objfile->obfd, "$GLOBAL$");
          if (global &&
              (bfd_section_size(symfile_objfile->obfd, global) == 16))
            addr = bfd_section_vma(symfile_objfile->obfd, global);
          else
            error ("Unable to find the dynamic library list for %s.  It could be because this file is either built with an old linker or it is corrupt.  Report to HP that this file doesn't have the symbol __dld_list.\n", symfile_objfile->obfd);
        }
      else
	addr = SYMBOL_VALUE_ADDRESS (msymbol) - 8;
    }
  else
    addr = SYMBOL_VALUE_ADDRESS (msymbol);

  status = target_read_memory (addr, buf, 4);
  if (status != 0)
    {
      error ("Unable to read the dynamic library list for %s.  If you are debugging a core file, it is likely that it is corrupt.  If it is a live process, it may have become a zombie losing its address space.  If neither of these is the case, report this to HP.\n", symfile_objfile->name);
      return;
    }

  addr = extract_unsigned_integer (buf, 4);

  /* If addr is zero, then we're using an old dynamic loader which
     doesn't maintain __dld_list.  We'll have to use a completely
     different approach to get shared library information.  */
  if (addr == 0)
    goto old_dld;

  /* Using the information in __dld_list is the preferred method
     to get at shared library information.  It doesn't depend on
     any functions in /opt/langtools/lib/end.o and has a chance of working
     with hpux10 when it is released.  */
  status = target_read_memory (addr, buf, 4);
  if (status != 0)
    {
      error ("Unable to find dynamic library list.\n");
      return;
    }

  /* addr now holds the address of the first entry in the dynamic
     library list.  */
  addr = extract_unsigned_integer (buf, 4);

  /* Now that we have a pointer to the dynamic library list, walk
     through it and add the symbols for each library.  */

#ifdef SOLIB_DEBUG
  printf ("--About to read shared library list data\n");
#endif

  /* "addr" will always point to the base of the
   * current data entry describing the current
   * shared library.
   */
  while (1)
    {
      if (addr == 0)
        break;

      /* process this shared library */
      process_a_shared_library(addr, from_tty, target);
      
      /* Go on to the next shared library descriptor.
       */
      status = target_read_memory (addr + 36, buf, 4);
      if (status != 0)
        goto err;

      addr = extract_unsigned_integer (buf, 4);
    }
 
#ifdef SOLIB_DEBUG
  printf ("--Done reading shared library data\n");
#endif

  if (check_heap_in_this_run || trace_threads_in_this_run)
    {
      /* Warn if we're doing thread or heap checking and
         librtc is not loaded */
      next_solist = so_list_head;
      while (next_solist)
        {
          if (next_solist->objfile != NULL && 
              strstr (next_solist->objfile->name, "librtc") != 0)
            break;
          next_solist = next_solist->next;
        }

      if (next_solist == NULL)
        warning ("Cannot find librtc. Heap and/or thread checking "
                 "infomation will not be available. Set LIBRTC_SERVER "
                 "to point to the right librtc or set GDB_ROOT to point"
                 "to your wdb installation directory.");
    }

done_digestion:
  /* Getting new symbols may change our opinion about what is
     frameless.  */
  reinit_frame_cache ();
  snoop_on_the_heap (); /* Moved here to avoid doing this for BOR */

/*JAGae68084- Call place_breakpoints_in_shlib(), if the user has requested for 
   persistent breakpoints and we have seen a load event*/

 if(if_yes_to_all_commands 
    && !som_solib_have_unload_event (inferior_pid)
    && loaded_shared)
     {
       place_breakpoints_in_shlib();
       in_shl_load = 0;
       loaded_shared = true;
       free (shllib_name); 
       shllib_name = NULL;
     }
 
  return;

old_dld:
err:
  error ("Error while reading dynamic library list.\n");
  return;
}

/* JAGaf54436: Find __d_trap export symbol in stripped a.out */

CORE_ADDR
find_d_trap_in_stripped_aout ()
{
  ExportEntry *export_entry;

  for (int i=0; i < symfile_objfile->export_list_size; i++)
    {
      if ( symfile_objfile->export_list[i].name
           && (STREQ ("__d_trap", symfile_objfile->export_list[i].name))
           && (symfile_objfile->export_list[i].address < 0x3FFFFFFF) )
             return symfile_objfile->export_list[i].address;
    }
  return 0;
}

/* This hook gets called just before the first instruction in the
   inferior process is executed.

   This is our opportunity to set magic flags in the inferior so
   that GDB can be notified when a shared library is mapped in and
   to tell the dynamic linker that a private copy of the library is
   needed (so GDB can set breakpoints in the library).

   __dld_flags is the location of the magic flags; as of this implementation
   there are 3 flags of interest:

   bit 0 when set indicates that private copies of the libraries are needed
   bit 1 when set indicates that the callback hook routine is valid
   bit 2 when set indicates that the dynamic linker should maintain the
   __dld_list structure when loading/unloading libraries.

   Note that shared libraries are not mapped in at this time, so we have
   run the inferior until the libraries are mapped in.  Typically this
   means running until the "_start" is called.  
   
   Note: to support Aries debugging of an archive bound program, we set the
   flag bits DLD_FLAGS_MAPPRIVATE and DLD_FLAGS_HOOKVALID and set up the
   break in the hook routine, if info_hp_aries_pa32 is true.
   */

void
som_solib_create_inferior_hook ()
{
  struct minimal_symbol *msymbol;
  unsigned int dld_flags, status, have_endo;
  asection *shlib_info;
  char buf[4];
  struct objfile *objfile;
  CORE_ADDR anaddr = 0;
  CORE_ADDR global_subspace_start;
  extern boolean get_reapply_fix ();

  /* First, remove all the solib event breakpoints.  Their addresses
     may have changed since the last time we ran the program.  */
  remove_solib_event_breakpoints ();

  _start_addr = 0;
  dld_flags_addr = 0;
  bp_at_start = 0;
  
 /* JAGaa80453/JAGaf03519 - To catch the statically unloaded libraries */
 
  _end_addr = 0;
  bp_at_end = 0; 

  if (symfile_objfile == NULL)
    return;

  /* 7/15/99 - remove #if 0 code after 1/00 */
  /* To support Aries debugging, do the setup even if it is archive bound. */
  /* First see if the objfile was dynamically linked.  */
  shlib_info = bfd_get_section_by_name (symfile_objfile->obfd, "$SHLIB_INFO$");
  if (!shlib_info && !info_hp_aries_pa32)
    return;

  /* It's got a $SHLIB_INFO$ section, make sure it's not empty.  */
  if (!info_hp_aries_pa32 &&
      bfd_section_size (symfile_objfile->obfd, shlib_info) == 0)
    return;
  
  have_endo = 0;

/* RM: Not sure we actually need to write to __d_pid for
 * shl_load/shl_unload tracking to work: DDE seems to manage fine
 * without it. It's useful to debug shared libraries even when the
 * executable doesn't have end.o linked in.
 */

#if 0  
  /* Slam the pid of the process into __d_pid; failing is only a warning!  */
  msymbol = lookup_minimal_symbol ("__d_pid", NULL, symfile_objfile);
  if (msymbol == NULL)
    {
      warning ("Unable to find __d_pid symbol in object file.");
      warning ("Suggest linking with /opt/langtools/lib/end.o.");
      warning ("GDB will be unable to track shl_load/shl_unload calls");
      goto keep_going;
    }

  anaddr = SYMBOL_VALUE_ADDRESS (msymbol);
  store_unsigned_integer (buf, 4, inferior_pid);
  status = target_write_memory (anaddr, buf, 4);
  if (status != 0)
    {
      warning ("Unable to write __d_pid");
      warning ("Suggest linking with /opt/langtools/lib/end.o.");
      warning ("GDB will be unable to track shl_load/shl_unload calls");
      goto keep_going;
    }
#endif

  /* Get the value of _DLD_HOOK (an export stub) and put it in __dld_hook;
     This will force the dynamic linker to call __d_trap when significant
     events occur.

     Note that the above is the pre-HP-UX 9.0 behaviour.  At 9.0 and above,
     the dld provides an export stub named "__d_trap" as well as the
     function named "__d_trap" itself, but doesn't provide "_DLD_HOOK".
     We'll look first for the old flavor and then the new.
   */
  msymbol = lookup_minimal_symbol ("_DLD_HOOK", NULL, symfile_objfile);
  if (msymbol == NULL)
    msymbol = lookup_minimal_symbol ("__d_trap", NULL, symfile_objfile);
  if (msymbol == NULL)
    anaddr = find_d_trap_in_stripped_aout ();
  else
    anaddr = SYMBOL_VALUE_ADDRESS (msymbol);
  dld_cache.hook.address = anaddr;

  /* Grrr, this might not be an export symbol!  We have to find the
     export stub.  */
  if (msymbol != NULL)
    {
      ALL_OBJFILES (objfile)
      {
        struct unwind_table_entry *u;
        struct minimal_symbol *msymbol2;

        /* What a crock.  */
        msymbol2 = lookup_minimal_symbol_solib_trampoline (SYMBOL_NAME (msymbol),
						           NULL, objfile);
        /* Found a symbol with the right name.  */
        if (msymbol2)
          {
	    struct unwind_table_entry *u;
	    /* It must be a shared library trampoline.  */
	    if (SYMBOL_TYPE (msymbol2) != mst_solib_trampoline)
	      continue;

	    /* It must also be an export stub.  */
	    u = hppa_find_unwind_entry (SYMBOL_VALUE (msymbol2));
	      if (!u)
	        {
	          obj_private_data_t *obj_private;

	          obj_private = (obj_private_data_t *) objfile->obj_private;
	          if (   ! obj_private
		      || ! obj_private->unwind_info
		      || obj_private->unwind_info->last == -1)
		    { /* No unwind.  Give warning and do our best.*/
		      warning ("No unwind trying to resolve __d_trap in '%s'.\n",
			       objfile->name);
		      /* Best guess is that this is the correct import stub.  */
		      anaddr = SYMBOL_VALUE (msymbol2);
		      dld_cache.hook_stub.address = anaddr;
		      break;
		    }
	          else
		    continue;  /* There is unwind, we didn't find it.*/
	        }
              if (!u
#ifndef GDB_TARGET_IS_HPPA_20W
                  || u->stub_unwind.stub_type != EXPORT
#endif
                 )
	      continue;

	    /* OK.  Looks like the correct import stub.  */
	    anaddr = SYMBOL_VALUE (msymbol2);
	    dld_cache.hook_stub.address = anaddr;
	    break;
          }
      }
    } /* (msymbol != NULL) */
  store_unsigned_integer (buf, 4, anaddr);

  msymbol = lookup_minimal_symbol ("__dld_hook", NULL, symfile_objfile);
  if (msymbol == NULL)
    {
      asection *global =
        bfd_get_section_by_name(symfile_objfile->obfd, "$GLOBAL$");
      if (global &&
         (bfd_section_size(symfile_objfile->obfd, global) == 16))
        global_subspace_start = bfd_section_vma(symfile_objfile->obfd, global);
      else
        global_subspace_start = 0;
  
      if (global_subspace_start)
        anaddr = global_subspace_start + 4;
    }
  else
    anaddr = SYMBOL_VALUE_ADDRESS (msymbol);

#if !defined(HP_IA64) && !defined(GDB_TARGET_IS_HPPA_20W)
  /* srikanth, 000311, When heap debugging, request a callback at a
     different address than the one we will have a breakpoint at
     (__d_trap.) This is needed since we don't want the breakpoint to
     be hit when SIGTRAPs are disabled.
     
     It seems to be the favorite passtime of every Joe Sixpack to
     mess with signal masks. If the real call back hook gets hit when
     SIGTRAPs are masked, gdb will not know anything about it and won't
     be able to step the target out of the breakpoint. The program
     cannot continue either and everyone gets stuck in limbo land.

     The routine we use here will front end dld call backs and pass
     them onto the real hook only if SIGTRAPs are enabled.
  */

  if (check_heap_in_this_run && !dld_can_preload)
    {
      struct minimal_symbol * m;

      alternate_d_trap_missing = 0;
      m = lookup_minimal_symbol_text ("__rtc_d_trap", 0, symfile_objfile);
      if (!m)
        {
          warning ("Debugger hook missing in end.o (old end.o?)");
          warning ("Suggest upgrading /opt/langtools/lib/end.o");
          warning ("GDB will not be able to accurately monitor "
                                             "heap usage.");
          alternate_d_trap_missing = 1;
        }
      else
        store_unsigned_integer (buf, 4, SYMBOL_VALUE_ADDRESS(m));
    }
#endif

  status = target_write_memory (anaddr, buf, 4);

  /* Now set a shlib_event breakpoint at __d_trap so we can track
     significant shared library events.  */
  msymbol = lookup_minimal_symbol ("__d_trap", NULL, symfile_objfile);
  if (msymbol == NULL)
    {
      anaddr = locate_sr4export();
      if (anaddr)
        anaddr += 28;
      else
        {
          warning ("Unable to find __d_trap symbol in object file.");
          goto keep_going;
        }
    }
  else
    anaddr = SYMBOL_VALUE_ADDRESS (msymbol);
  create_solib_event_breakpoint (anaddr);

  /* We have all the support usually found in end.o, so we can track
     shl_load and shl_unload calls.  */
  have_endo = 1;

keep_going:

  /* Get the address of __dld_flags, if no such symbol exists, then we can
     not debug the shared code.  */
  /* RM: Yes we can, by making a few educated guesses */
  {
    asection *global =
      bfd_get_section_by_name(symfile_objfile->obfd, "$GLOBAL$");
    if (global &&
        (bfd_section_size(symfile_objfile->obfd, global) == 16))
      global_subspace_start = bfd_section_vma(symfile_objfile->obfd, global);
    else
      global_subspace_start = 0;
  }
  msymbol = lookup_minimal_symbol ("__dld_flags", NULL, NULL);
  if (msymbol == NULL)
    {
      if (global_subspace_start)
        anaddr = global_subspace_start + 8;
      else
        error ("Unable to find  __dld_flags symbol in object file.\n");
    }
  else
    anaddr = SYMBOL_VALUE_ADDRESS (msymbol);

  /* Read the current contents.  */
  status = target_read_memory (anaddr, buf, 4);
  if (status != 0)
    {
     /* JAGaf08040 - <START> - Make the error msg user-friendly */
      error ("Unable to read the dynamic library list.If you are debugging a core file,it is likely that it is corrupt.If it is a live process,it may have become a zombie losing its address space.If neither of these is the case,report this to HP.\n");
     /* JAGaf08040 - <END> */
    }
  dld_flags = extract_unsigned_integer (buf, 4);

  /* The mapped private bit should be set explicitly by the debugger only
     when the a.out is started under the debugger and not in an attach case.
     Also not when the -mapshared option is on.  */
  if (!attach_flag && !mapshared)
  	dld_flags |= DLD_FLAGS_MAPPRIVATE;

  /* When heap debugging, request a BOR call back. This is needed
     so that we can override dld relocations just in time. Ideally
     we should wait until the libraries are mapped in and if there
     pending relocations, then request this. However, some of the older
     dlds have a bug wherein they don't consult the dld flags
     freshly. So we will do it here. -- srikanth, 000304.
  */
 
  /* This seems to interfere with initial library list handling. GOK
     why. Now we do this after we have processed the implicitly linked
     shared libraries. -- srikanth, 000305
  */
#if 0
  if (check_heap_in_this_run)
    dld_flags |= DLD_FLAGS_BOR_ENABLE;
#endif

  dld_flags_addr = anaddr;

  if (have_endo)
    dld_flags |= DLD_FLAGS_HOOKVALID;
  store_unsigned_integer (buf, 4, dld_flags);
  status = target_write_memory (anaddr, buf, 4);
  if (status != 0)
    {
      error ("Unable to write __dld_flags\n");
    }

  /* Now find the address of _start and set a breakpoint there. 
     We still need this code for two reasons:

     * Not all sites have /opt/langtools/lib/end.o, so it's not always
     possible to track the dynamic linker's events.

     * At this time no events are triggered for shared libraries
     loaded at startup time (what a crock).  */

  msymbol = lookup_minimal_symbol ("_start", NULL, symfile_objfile);
  if (msymbol == NULL)
    {
      /* RM: Hmm, no symbols. Let's try finding "main" in the shared
         library export list.
      */

      ExportEntry *export_entry; 

      /* srikanth, 990629, read in the dynamic linker's export list */
      if (!symfile_objfile -> export_list)
        init_export_symbols (symfile_objfile);

      export_entry = find_export_entry ("main", symfile_objfile);
      if (export_entry)
        _start_addr = anaddr = export_entry->address;
      else
	error ("Unable to find _start symbol in object file.\n");
    }
  else
    _start_addr = anaddr = SYMBOL_VALUE_ADDRESS (msymbol);

  /* Make the breakpoint at "_start" a shared library event breakpoint.  */
  bp_at_start = create_solib_event_breakpoint (anaddr);

  /* Wipe out all knowledge of old shared libraries since their
     mapping can change from one exec to another!  */
  while (so_list_head)
    {
      struct so_list *temp;

      temp = so_list_head->next;
      /* srikanth, 991004, let us not forget the section tables ... */
      free (so_list_head->sections);
      /* JYG 000906: som_solib.name is now a malloc'd storage */
      /* jini: mixed mode changes: som_solib.name is now solib_desc->name */
      free (((struct som_solib_mapped_entry *)
             (so_list_head->solib_desc))->name);
      free (so_list_head->solib_desc);
      free (so_list_head);
      so_list_head = temp;
    }
  cache_solib_p = NULL;
  solib_clear_symtab_users ();

  if (get_reapply_fix())
    {
      extern struct breakpoint * breakpoint_chain;
      struct breakpoint *b;
      for (b = breakpoint_chain; b; b = b->next)
        {
          if (b->address == _start_addr)
            b->type = bp_fix_event;
            break;
        }
    }
}

boolean
stopped_at_start(CORE_ADDR pc)
{
  return (pc == _start_addr);
}

boolean
stopped_at_end(CORE_ADDR pc)
{
  return (pc == _end_addr);
}

static void
reset_inferior_pid (saved_inferior_pid)
     int saved_inferior_pid;
{
  inferior_pid = saved_inferior_pid;
}


/* This operation removes the "hook" between GDB and the dynamic linker,
   which causes the dld to notify GDB of shared library events.

   After this operation completes, the dld will no longer notify GDB of
   shared library events.  To resume notifications, GDB must call
   som_solib_create_inferior_hook.

   This operation does not remove any knowledge of shared libraries which
   GDB may already have been notified of.
 */
void
som_solib_remove_inferior_hook (pid)
     int pid;
{
  CORE_ADDR addr;
  struct minimal_symbol *msymbol;
  int status;
  char dld_flags_buffer[TARGET_INT_BIT / TARGET_CHAR_BIT];
  unsigned int dld_flags_value;
  int saved_inferior_pid = inferior_pid;
  struct cleanup *old_cleanups = 
    make_cleanup ((make_cleanup_ftype *) reset_inferior_pid, 
		  (void*) saved_inferior_pid);

  /* Ensure that we're really operating on the specified process. */
  inferior_pid = pid;

  /* We won't bother to remove the solib breakpoints from this process.

     In fact, on PA64 the breakpoint is hard-coded into the dld callback,
     and thus we're not supposed to remove it.

     Rather, we'll merely clear the dld_flags bit that enables callbacks.
   */

  /* Srikanth, This code is suspect. FIXME:

     (a) If a.out is stripped, we won't be able to find __dld_flags
     and so won't be able to derequest the notification. So after
     detach, if the program dlopens/shl_loads, dld will give a
     notification and the program will die with a SIGTRAP. (see
     that the breakpoint in __d_trap is not removed !!!)

     (b) Because, we don't remove the breakpoint in __d_trap, we
     cannot also reliably debug the program after a reattach.
  */

  msymbol = lookup_minimal_symbol ("__dld_flags", NULL, NULL);

  addr = SYMBOL_VALUE_ADDRESS (msymbol);
  status = target_read_memory (addr, dld_flags_buffer, TARGET_INT_BIT / TARGET_CHAR_BIT);

  dld_flags_value = extract_unsigned_integer (dld_flags_buffer,
					      sizeof (dld_flags_value));

  dld_flags_value &= ~DLD_FLAGS_HOOKVALID;
  store_unsigned_integer (dld_flags_buffer,
			  sizeof (dld_flags_value),
			  dld_flags_value);
  status = target_write_memory (addr, dld_flags_buffer, TARGET_INT_BIT / TARGET_CHAR_BIT);

  do_cleanups (old_cleanups);
}

/* Srikanth, Aug 13th 2002, new function to adjust the HOOKVALID flag
   after a vfork. Currently we unrequest shl_load callbacks after vfork
   (for the child process), but since the child and the parent share
   the same address space, we end up not getting shl_load notifications
   for the parent also : OIOW, gdb is blissfully unaware of any shl_loads
   made by the vforking parent process.
*/
void
som_solib_adjust_inferior_hook (pid)
     int pid;
{
  CORE_ADDR addr;
  struct minimal_symbol *msymbol;
  int status;
  char dld_flags_buffer[TARGET_INT_BIT / TARGET_CHAR_BIT];
  unsigned int dld_flags_value;
  int saved_inferior_pid = inferior_pid;
  struct cleanup *old_cleanups = make_cleanup (
	(make_cleanup_ftype *) reset_inferior_pid, 
	(void*) saved_inferior_pid);

  /* Ensure that we're really operating on the specified process. */
  inferior_pid = pid;

  msymbol = lookup_minimal_symbol ("__dld_flags", NULL, NULL);

  addr = SYMBOL_VALUE_ADDRESS (msymbol);
  status = target_read_memory (addr, dld_flags_buffer, TARGET_INT_BIT / TARGET_CHAR_BIT);

  dld_flags_value = extract_unsigned_integer (dld_flags_buffer,
					      sizeof (dld_flags_value));

  dld_flags_value |= DLD_FLAGS_HOOKVALID;
  store_unsigned_integer (dld_flags_buffer,
			  sizeof (dld_flags_value),
			  dld_flags_value);
  status = target_write_memory (addr, dld_flags_buffer, TARGET_INT_BIT / TARGET_CHAR_BIT);

  do_cleanups (old_cleanups);
}

/* This function creates a breakpoint on the dynamic linker hook, which
   is called when e.g., a shl_load or shl_unload call is made.  This
   breakpoint will only trigger when a shl_load call is made.

   If filename is NULL, then loads of any dll will be caught.  Else,
   only loads of the file whose pathname is the string contained by
   filename will be caught.

   Undefined behaviour is guaranteed if this function is called before
   som_solib_create_inferior_hook.
 */
void
som_solib_create_catch_load_hook (pid, tempflag, filename, cond_string)
     int pid;
     int tempflag;
     char *filename;
     char *cond_string;
{
  create_solib_load_event_breakpoint ("__d_trap", tempflag, filename, cond_string);
}

/* This function creates a breakpoint on the dynamic linker hook, which
   is called when e.g., a shl_load or shl_unload call is made.  This
   breakpoint will only trigger when a shl_unload call is made.

   If filename is NULL, then unloads of any dll will be caught.  Else,
   only unloads of the file whose pathname is the string contained by
   filename will be caught.

   Undefined behaviour is guaranteed if this function is called before
   som_solib_create_inferior_hook.
 */
void
som_solib_create_catch_unload_hook (pid, tempflag, filename, cond_string)
     int pid;
     int tempflag;
     char *filename;
     char *cond_string;
{
  create_solib_unload_event_breakpoint ("__d_trap", tempflag, filename, cond_string);
}

int
som_solib_have_load_event (pid)
     int pid;
{
  CORE_ADDR event_kind;

  event_kind = read_register (ARG0_REGNUM);
  return (event_kind == SHL_LOAD);
}

int
som_solib_have_unload_event (pid)
     int pid;
{
  CORE_ADDR event_kind;

  event_kind = read_register (ARG0_REGNUM);
  return (event_kind == SHL_UNLOAD);
}

int
som_solib_have_bor_event ()
{
  CORE_ADDR event_kind;

  if (!target_has_stack)
    return 0;

  event_kind = read_register (ARG0_REGNUM);
  return (event_kind == SHL_BOR);
}

int
som_solib_request_bor_callback (int wanted)
{
  char buf[4];
  int dld_flags;
  int status;

  /* srikanth, workaround for dld bug for Telcordia. Do not request BOR
     callbacks, as this causes MT programs to crash. Instead use the
     LD_PRELOAD mechanism to preload librtc.sl
  */
  if (!check_heap_in_this_run || dld_can_preload)
    return 1;         /* nothing to do */

  /* If alternate __d_trap is missing, don't request BOR. It would
     be nice to have an alternate version in librtc.sl and request
     a callback there now. But this is problematic as some of the
     older dlds do not consult the callback address freshly and use
     the value cached at startup. That would be disastrous.

     We cannot preset the callback function to come from librtc.sl
     as libraries are not mapped in when som_solib_create_inferior_hook
     is called. The real fix for this to upgrade end.o which has been
     done.

     For the time being, we will still continue having already warned
     the user. This may not be a big issue, as dld relocations for
     a.out are already applied and it is only the libraries that are
     pending. Hopefully not many libraries extract addresses of malloc
     free and co.
  */

  if (!dld_flags_addr || !target_has_stack || alternate_d_trap_missing)
    return 0;

  /* Read the current contents.  */
  status = target_read_memory (dld_flags_addr, buf, 4);
  if (status != 0)
    return 0;

  dld_flags = extract_unsigned_integer (buf, 4);

  if (wanted)
    dld_flags |= DLD_FLAGS_BOR_ENABLE;
  else 
    dld_flags &= ~DLD_FLAGS_BOR_ENABLE;
    
  store_unsigned_integer (buf, 4, dld_flags);
  status = target_write_memory (dld_flags_addr, buf, 4);

  return !status;
}


static char *
som_solib_library_pathname (pid)
     int pid;
{
  CORE_ADDR dll_handle_address;
  CORE_ADDR dll_pathname_address;
  struct som_solib_mapped_entry dll_descriptor;
  char *p;
  static char dll_pathname[1024];

  /* Read the descriptor of this newly-loaded library. */
  dll_handle_address = read_register (ARG1_REGNUM);
  read_memory (dll_handle_address, (char *) &dll_descriptor, sizeof (dll_descriptor));

  /* We can find a pointer to the dll's pathname within the descriptor. */
  dll_pathname_address = (CORE_ADDR) dll_descriptor.name;

  /* Read the pathname, one byte at a time. */
  p = dll_pathname;
  for (;;)
    {
      char b;
      read_memory (dll_pathname_address++, (char *) &b, 1);
      *p++ = b;
      if (b == '\0')
	break;
    }

  return dll_pathname;
}

char *
som_solib_loaded_library_pathname (pid)
     int pid;
{
  if (!som_solib_have_load_event (pid))
    error ("Must have a load event to use this query");

  return som_solib_library_pathname (pid);
}

char *
som_solib_unloaded_library_pathname (pid)
     int pid;
{
  if (!som_solib_have_unload_event (pid))
    error ("Must have an unload event to use this query");

  return som_solib_library_pathname (pid);
}

static void
som_solib_desire_dynamic_linker_symbols ()
{
  struct objfile *objfile;
  struct unwind_table_entry *u;
  struct minimal_symbol *dld_msymbol;

  /* Do we already know the value of these symbols?  If so, then
     we've no work to do.

     (If you add clauses to this test, be sure to likewise update the
     test within the loop.)
   */
  if (dld_cache.is_valid)
    return;

  ALL_OBJFILES (objfile)
  {
    dld_msymbol = lookup_minimal_symbol ("shl_load", NULL, objfile);
    if (dld_msymbol != NULL)
      {
	dld_cache.load.address = SYMBOL_VALUE (dld_msymbol);
	dld_cache.load.unwind = hppa_find_unwind_entry (dld_cache.load.address);
      }

    dld_msymbol = lookup_minimal_symbol_solib_trampoline ("shl_load",
							  NULL,
							  objfile);
    if (dld_msymbol != NULL)
      {
	if (SYMBOL_TYPE (dld_msymbol) == mst_solib_trampoline)
	  {
	    u = hppa_find_unwind_entry (SYMBOL_VALUE (dld_msymbol));
	    if ((u != NULL)
#ifndef GDB_TARGET_IS_HPPA_20W
		&& (u->stub_unwind.stub_type == EXPORT)
#endif
		)
	      {
		dld_cache.load_stub.address = SYMBOL_VALUE (dld_msymbol);
		dld_cache.load_stub.unwind = u;
	      }
	  }
      }

    dld_msymbol = lookup_minimal_symbol ("shl_unload", NULL, objfile);
    if (dld_msymbol != NULL)
      {
	dld_cache.unload.address = SYMBOL_VALUE (dld_msymbol);
	dld_cache.unload.unwind = hppa_find_unwind_entry (dld_cache.unload.address);

	/* ??rehrauer: I'm not sure exactly what this is, but it appears
	   that on some HPUX 10.x versions, there's two unwind regions to
	   cover the body of "shl_unload", the second being 4 bytes past
	   the end of the first.  This is a large hack to handle that
	   case, but since I don't seem to have any legitimate way to
	   look for this thing via the symbol table...
	 */
	if (dld_cache.unload.unwind != NULL)
	  {
	    u = hppa_find_unwind_entry (dld_cache.unload.unwind->region_end + 4);
	    if (u != NULL)
	      {
		dld_cache.unload2.address = u->region_start;
		dld_cache.unload2.unwind = u;
	      }
	  }
      }

    dld_msymbol = lookup_minimal_symbol_solib_trampoline ("shl_unload",
							  NULL,
							  objfile);
    if (dld_msymbol != NULL)
      {
	if (SYMBOL_TYPE (dld_msymbol) == mst_solib_trampoline)
	  {
	    u = hppa_find_unwind_entry (SYMBOL_VALUE (dld_msymbol));
	    if ((u != NULL)
#ifndef GDB_TARGET_IS_HPPA_20W
		&& (u->stub_unwind.stub_type == EXPORT)
#endif
		)
	      {
		dld_cache.unload_stub.address = SYMBOL_VALUE (dld_msymbol);
		dld_cache.unload_stub.unwind = u;
	      }
	  }
      }

    /* Did we find everything we were looking for?  If so, stop. */
    if ((dld_cache.load.address != NULL) && (dld_cache.load_stub.address != NULL)
	&& (dld_cache.unload.address != NULL) && (dld_cache.unload_stub.address != NULL))
      {
	dld_cache.is_valid = 1;
	break;
      }
/*JAGaa80453/JAGaf03519 - To catch the statically unloaded libraries, place a breakpoint at "_exit"*/     
    dld_msymbol = lookup_minimal_symbol ("_exit", NULL, objfile);
    if (dld_msymbol != NULL && !bp_at_end)
     {
    	 _end_addr = SYMBOL_VALUE_ADDRESS (dld_msymbol);
	 bp_at_end = create_solib_event_breakpoint (_end_addr);
     }
  }

  dld_cache.hook.unwind = hppa_find_unwind_entry (dld_cache.hook.address);
  dld_cache.hook_stub.unwind = hppa_find_unwind_entry (dld_cache.hook_stub.address);

  /* We're prepared not to find some of these symbols, which is why
     this function is a "desire" operation, and not a "require".
   */
}

int
som_solib_in_dynamic_linker (pid, pc)
     int pid;
     CORE_ADDR pc;
{
  struct unwind_table_entry *u_pc;

  /* Are we in the dld itself?

     ??rehrauer: Large hack -- We'll assume that any address in a
     shared text region is the dld's text.  This would obviously
     fall down if the user attached to a process, whose shlibs
     weren't mapped to a (writeable) private region.  However, in
     that case the debugger probably isn't able to set the fundamental
     breakpoint in the dld callback anyways, so this hack should be
     safe.
   */
  
     /*  
     Srikanth, The check below viz:
     if ((pc & (CORE_ADDR) 0xc0000000) == (CORE_ADDR) 0xc0000000)
     is pretty flaky. It declares any address in the 4th quardant
     as being in the dynamic linker.

     Rajiv's code to dld's symbol table itself has been flawed
     from day 1. See that the check

     if (!dld_already_loaded && dld_text_offset)

     would always fail since dld_text_offset is initialized to
     zero and is never modified !!!

     When the executable is chattered q3p q4p, the OS 11.11 seems
     to load shared libraries in the 4th quardant resulting in
     a wrong conclusion from GDB that the given PC inside the
     library is in dld.

     See if the PC is in any load module, if it is it cannot be
     in dld, since dld.sl does not make it to the so_list.
     
     JAGae70612 - Problem debugguing q3p/q4p binaries
     Dinesh, q3p and q4p enabled binaries with shared library can be
     in 4th quadrant so our assumption that only dld's text is 
     de facto is wrong. As any shared library could be in 4th quadrant.
     */
    if (!som_solib_get_got_by_pc (pc))
     if ((pc & (CORE_ADDR) 0xc0000000) == (CORE_ADDR) 0xc0000000)
    return 1;

  /* Cache the address of some symbols that are part of the dynamic
     linker, if not already known.
   */
  som_solib_desire_dynamic_linker_symbols ();

  /* Are we in the dld callback?  Or its export stub? */
  u_pc = hppa_find_unwind_entry (pc);
  if (u_pc == NULL)
    return 0;

  if ((u_pc == dld_cache.hook.unwind) || (u_pc == dld_cache.hook_stub.unwind))
    return 1;

  /* Srikanth, this is some serious misunderstanding on somebody's
     part. shl_load and friends are not a part of dld.sl. These
     are from libdld.sl which is a user space library and as such
     should be treated no differently from strcpy and printf.

     The code below causes us to stop in shl_load + 0 when stepping
     over a second call to shl_load. Ths first call is ok as it goes
     through BOR text which gets a different treatment.

     Aug 15th 2002. (JAGae38313)
  */
#if 0
  /* Or the interface of the dld (i.e., "shl_load" or friends)? */
  if ((u_pc == dld_cache.load.unwind)
      || (u_pc == dld_cache.unload.unwind)
      || (u_pc == dld_cache.unload2.unwind)
      || (u_pc == dld_cache.load_stub.unwind)
      || (u_pc == dld_cache.unload_stub.unwind))
    return 1;
#endif
  /* Apparently this address isn't part of the dld's text. */
  return 0;
}


/* Return the GOT value for the shared library in which ADDR belongs.  If
   ADDR isn't in any known shared library, return zero.  */

CORE_ADDR
som_solib_get_got_by_pc (addr)
     CORE_ADDR addr;
{
  struct so_list *so_list = so_list_head;
  CORE_ADDR got_value = 0;

  while (so_list)
    {
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so_list->solib_desc;
      if (solib_desc->text_addr <= addr && solib_desc->text_end > addr)
	{
	  got_value = solib_desc->got_value;
	  break;
	}
      so_list = so_list->next;
    }
  return got_value;
}

/* som_solib_get_objfile_by_pc
   If the pc is in a shared library, return a pointer to the objfile
   of the shared library.
   */
struct objfile*
som_solib_get_objfile_by_pc (addr)
  CORE_ADDR addr;
{
  struct so_list *so_list;

  if (cache_solib_p)
    {
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) cache_solib_p->solib_desc;

      if (solib_desc->text_addr <= addr && solib_desc->text_end > addr)
        {
          return cache_solib_p->objfile;
        }
    }

  so_list = so_list_head;
  while (so_list)
    {
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so_list->solib_desc;

      if (solib_desc->text_addr <= addr && solib_desc->text_end > addr)
        {
          break;
        }
      so_list = so_list->next;
    }

  if (!so_list)
    return NULL;

  cache_solib_p = so_list;
  return cache_solib_p->objfile;
}

/* som_solib_get_objfile_by_name
   If the pc is in a shared library, return a pointer to the objfile
   of the shared library.
   */
struct objfile*
som_solib_get_objfile_by_name (name)
  char *name;
{
  struct so_list *so_list;

  if (   cache_solib_p
      && strcmp(
         ((struct som_solib_mapped_entry *)(cache_solib_p->solib_desc))->name,
          name) == 0)
    {
      return cache_solib_p->objfile;
    }

  so_list = so_list_head;
  while (so_list)
    {
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so_list->solib_desc;

      if (strcmp(solib_desc->name, name) == 0)
        {
          break;
        }
      so_list = so_list->next;
    }

  if (!so_list)
    return NULL;

  cache_solib_p = so_list;
  return cache_solib_p->objfile;
} /* end som_solib_get_objfile_by_name */

/*  elz:
   Return the address of the handle of the shared library
   in which ADDR belongs.  If
   ADDR isn't in any known shared library, return zero.  */
/* this function is used in hppa_fix_call_dummy in hppa-tdep.c */

CORE_ADDR
som_solib_get_solib_by_pc (addr)
     CORE_ADDR addr;
{
  struct so_list *so_list = so_list_head;

  while (so_list)
    {
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so_list->solib_desc;

      if (solib_desc->text_addr <= addr && solib_desc->text_end > addr)
	{
	  break;
	}
      so_list = so_list->next;
    }
  if (so_list)
    return so_list->desc_addr;
  else
    return 0;
}

extern boolean add_symbol_file_cmd;
#endif

int
som_solib_section_offsets (struct objfile *objfile,
                           struct section_offsets *offsets,
                           void *saddrs)
{
  struct so_list *so_list = so_list_head;

  /* The typecast to (void *) and back is done to avoid PA build failures.*/
  struct section_addr_info *addrs = (struct section_addr_info *) saddrs;

  /* srikanth, for the main program, the link time addresses are
     identical to runtime addresses. Just get out. This also works
     around the problem of accessing freed memory leading to bad
     behavior. so_list->somsolib.name comes from symfile_objfile's
     obstack and we touch freed memory under this circumstance :
     file a.out; b main; run; file a.out; FIXME.
     JYG 000906: This is fixed in process_a_shared_library by
     malloc'ing storage for so_list->somsolib.name instead of
     obsavestring. */

  /* JAGaf08152 Sunil S 15/02/2005 
        The flag OBJF_SHARED is not always set for example when we load the 
     executable through add-symbol-file command. So commenting out the following
     check. Code below continues to behave exactly as before and return 0/1 
     exactly as before, we are simply removing the short cut check.
  */ 
#if 0
  if (!(objfile->flags & OBJF_SHARED))
    return 0;
#endif

  while (so_list)
    {
      /* Oh what a pain!  We need the offsets before so_list->objfile
         is valid.  The BFDs will never match.  Make a best guess.  */
#ifdef HP_IA64
      struct load_module_desc *solib_desc =
        (struct load_module_desc *) so_list->solib_desc;

      /* Mixed mode support. Populate the offset addresses using the
         VMAs from the SOM file. */
      asection *text_section = NULL;

      if (so_list->objfile && !so_list->objfile->is_mixed_mode_pa_lib)
        {
          so_list = so_list->next;
          continue;
        }

      text_section = bfd_get_section_by_name (objfile->obfd, "$SHLIB_INFO$");
      if (!text_section)
        {
	  warning ("Unable to find $SHLIB_INFO$ in shared library!");
	  ANOFFSET (offsets, SECT_OFF_DATA (objfile)) = 0;
	  ANOFFSET (offsets, SECT_OFF_BSS (objfile)) = 0;
          /* Return 0 so that the text and the rodata offsets get populated
             by the caller. */
	  return 0;
	}

      if (addrs->other[0].addr == 
          solib_desc->text_base - bfd_section_vma (objfile->obfd, text_section))
	{
	  asection *private_section = NULL;

	  /* The text offset is easy.  */

	  ANOFFSET (offsets, SECT_OFF_TEXT (objfile))
	    = addrs->other[0].addr;
	  ANOFFSET (offsets, SECT_OFF_RODATA (objfile))
	    = ANOFFSET (offsets, SECT_OFF_TEXT (objfile));

	  /* We should look at presumed_dp in the SOM header, but
	     that's not easily available.  This should be OK though.  */
	  private_section = bfd_get_section_by_name (objfile->obfd,
						     "$PRIVATE$");
	  if (!private_section)
	    {
	      warning ("Unable to find $PRIVATE$ in shared library!");
	      ANOFFSET (offsets, SECT_OFF_DATA (objfile)) = 0;
	      ANOFFSET (offsets, SECT_OFF_BSS (objfile)) = 0;
	      return 1;
	    }
	  ANOFFSET (offsets, SECT_OFF_DATA (objfile))
	    = (solib_desc->data_base - private_section->vma);
	  ANOFFSET (offsets, SECT_OFF_BSS (objfile))
	    = ANOFFSET (offsets, SECT_OFF_DATA (objfile));
	  return 1;
       }
	
#else
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so_list->solib_desc;

      if (strstr (objfile->name, solib_desc->name))
	{
	  asection *private_section;

	  /* The text offset is easy.  */
	  ANOFFSET (offsets, SECT_OFF_TEXT (objfile))
	    = (solib_desc->text_addr - solib_desc->text_link_addr);
	  ANOFFSET (offsets, SECT_OFF_RODATA (objfile))
	    = ANOFFSET (offsets, SECT_OFF_TEXT (objfile));

	  /* We should look at presumed_dp in the SOM header, but
	     that's not easily available.  This should be OK though.  */
	  private_section = bfd_get_section_by_name (objfile->obfd,
						     "$PRIVATE$");
	  if (!private_section)
	    {
	      warning ("Unable to find $PRIVATE$ in shared library!");
	      ANOFFSET (offsets, SECT_OFF_DATA (objfile)) = 0;
	      ANOFFSET (offsets, SECT_OFF_BSS (objfile)) = 0;
	      return 1;
	    }
	  ANOFFSET (offsets, SECT_OFF_DATA (objfile))
	    = (solib_desc->data_start - private_section->vma);
	  ANOFFSET (offsets, SECT_OFF_BSS (objfile))
	    = ANOFFSET (offsets, SECT_OFF_DATA (objfile));
	  return 1;
	}
#endif
      so_list = so_list->next;
    }

#ifndef HP_IA64
  /* add_symbol_file_cmd is defined only for PA32 compilation. */
  /* Skip this warning for the internal use of add_symbol_file_command
     during RTC processing of unloaded shared libraries
   */
  if (add_symbol_file_cmd && !add_unloaded_library_symbols)
    {
       /* No executable is loaded, just trying to add the library/object file
          through add-symbol-file command. */
       puts_unfiltered ("\n");
       warning ("Unknown shared object. You are trying to load a shared"
                " library/object file that is previously not loaded.");
    }
#endif

  return 0;
}

#ifndef HP_IA64
/* These functions are currently unused on IPF */
/* Dump information about all the currently loaded shared libraries.  */

void
som_sharedlibrary_info_command (ignore, from_tty)
     char *ignore;
     int from_tty;
{
  struct so_list *so_list = so_list_head;
  unsigned int num_shlibs = 0;

  if (exec_bfd == NULL)
    {
      puts_unfiltered ("No executable file.\n");
      return;
    }

  if (so_list == NULL)
    {
      puts_unfiltered ("No shared libraries loaded at this time.\n");
      return;
    }

  puts_unfiltered ("Shared Object Libraries\n");
  printf_unfiltered ("    %-12s%-12s%-12s%-12s%-12s%-12s\n",
	 "  flags", "  tstart", "   tend", "  dstart", "   dend", "   dlt");
  while (so_list)
    {
      unsigned int flags;

      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so_list->solib_desc;

      flags = solib_desc->struct_version << 24;
      flags |= solib_desc->bind_mode << 16;
      flags |= solib_desc->library_version;
      puts_unfiltered (solib_desc->name);
      if (so_list->objfile == NULL)
	puts_unfiltered ("  (symbols not loaded)");
      if (!so_list->loaded)
        puts_unfiltered ("  (shared library unloaded)");
      puts_unfiltered ("\n");
      printf_unfiltered ("    %-12s", local_hex_string_custom (flags, "08l"));
      printf_unfiltered ("%-12s",
	     local_hex_string_custom (solib_desc->text_addr, "08l"));
      printf_unfiltered ("%-12s",
	      local_hex_string_custom (solib_desc->text_end, "08l"));
      printf_unfiltered ("%-12s",
	    local_hex_string_custom (solib_desc->data_start, "08l"));
      printf_unfiltered ("%-12s",
	      local_hex_string_custom (solib_desc->data_end, "08l"));
      printf_unfiltered ("%-12s\n",
	     local_hex_string_custom (solib_desc->got_value, "08l"));
      so_list = so_list->next;
      num_shlibs++;
    }
  printf_unfiltered ("Total of %d shared libraries.\n", num_shlibs);
}

static void
som_solib_sharedlibrary_command (args, from_tty)
     char *args;
     int from_tty;
{
  extern int breakpoints_inserted;
  struct dll_reload_list * this;

  dont_repeat ();
  if (auto_solib_add != 0 )
    som_solib_add_sharedlibrary (args, from_tty, (struct target_ops *) 0);
  else
    som_solib_add (args, from_tty, (struct target_ops *) 0);

  this = xmalloc (sizeof (struct dll_reload_list));
  this -> regexp = (args ? strdup (args) : NULL);
  this ->next = auto_solib_readd;
  auto_solib_readd = this;

  re_enable_breakpoints_in_shlibs(); /* srikanth, 981007, CLLbs16090 */

  if (breakpoints_inserted)
    {
      remove_breakpoints ();
      insert_breakpoints ();
    }
}


som_solib_readd ()
{
   struct dll_reload_list * head;

   head = auto_solib_readd;

   while (head)
     {
       if (auto_solib_add != 0 )
	 som_solib_add_sharedlibrary (head->regexp, 1, 0);
       else
         som_solib_add (head->regexp, 1, 0);
       head = head->next;
     }

   re_enable_breakpoints_in_shlibs();
   if (breakpoints_inserted)
     {
       remove_breakpoints ();
       insert_breakpoints ();
     }
}


char *
som_solib_address (addr)
     CORE_ADDR addr;
{
  struct so_list *so = so_list_head;

  while (so)
    {
      /* Is this address within this shlib's text range?  If so,
         return the shlib's name.
       */
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) so->solib_desc;

      if ((addr >= solib_desc->text_addr) && (addr <= solib_desc->text_end))
	return solib_desc->name;

      /* Nope, keep looking... */
      so = so->next;
    }

  /* No, we couldn't prove that the address is within a shlib. */
  return NULL;
}

/* JAGaf03565
   Checks the text low and text high of all shared libraries to find
   if a given address is present in a shared library.

   If not found it checks the main executable to see if address is part
   of that.

   Returns the name of the load module the address was found in
   else returns NULL.
 */


#ifdef PC_SOLIB 
char *
solib_and_mainfile_address (CORE_ADDR addr)
{
  char *so_name = NULL;
  CORE_ADDR data_start, data_end;
  /* Check to see if the address is present in a shared library */
  if ((so_name = PC_SOLIB (addr)) != NULL)
    return so_name;
  else if (symfile_objfile) /* If main exe present */
    {
      CORE_ADDR text_base = 0, text_size = 0, text_high = 0;

      /* We cache the text low and text high for an objfile in it.
         Check cached values first.*/
      if (symfile_objfile->text_low
          && symfile_objfile->text_high)
        {
          text_base = symfile_objfile->text_low;
          text_high = symfile_objfile->text_high;
        }
      /* If the load module descriptor not present yet then look for the
         minimal symbols __text_start and _extext which signify start and
         end of text and cache the values.
       */
      else
        {
          text_base = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
                                            ("__text_start",
                                             0, symfile_objfile));
          text_high = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
                                            ("_etext",
                                             0, symfile_objfile));
          data_start = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
                                            ("__data_start",
                                             0, symfile_objfile));
          data_end = SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol
                                            ("_end",
                                             0, symfile_objfile));
          symfile_objfile->text_low = text_base;
          symfile_objfile->text_high = text_high;
	  symfile_objfile->data_start = data_start;
	  symfile_objfile->data_size = data_end - data_start;
        }

      if (text_base && text_high
          && text_base <= addr
          && addr <= text_high)
        return symfile_objfile->name;
    }

  return (0);
}
#endif

void
som_solib_restart ()
{
  struct so_list *sl = so_list_head;

  threshold_warning_given = 0;
#ifdef LOG_BETA_FILTERED_LIBRARIES
  logged_filtered_libraries = 0;
#endif

  /* Before the shlib info vanishes, use it to disable any breakpoints
     that may still be active in those shlibs.
   */
  disable_breakpoints_in_shlibs (!info_verbose, NULL);
  if (bp_at_start)
    {
      /* JYG: PURIFY COMMENT: bp_at_start could be pointing to
	 deleted bpt, after infrun.c (follow_exec) calls 
	 update_breakpionts_after_exec () and then
	 SOLIB_RESTART (this routine). */
      extern struct breakpoint *breakpoint_chain;
      struct breakpoint *b;
      int found = 0;
      for (b = breakpoint_chain; b; b = b->next)
        {
	  if (b == bp_at_start)
	    {
	      enable_breakpoint (bp_at_start);
	      found = 1;
	      break;
	    }
	}
      if (! found)
	bp_at_start = NULL;
    }

  /* Discard all the shlib descriptors.
   */
  while (sl)
    {
      struct so_list *next_sl = sl->next;
      /* srikanth, 991004, let us not forget the section tables ... */
      free (sl->sections);
      /* JYG 000906: som_solib.name is now a malloc'd storage */
      /* jini: mixed mode changes: som_solib.name is now solib_desc->name */
      free (((struct som_solib_mapped_entry *)(sl->solib_desc))->name);
      free (sl->solib_desc);
      free (sl);
      sl = next_sl;
    }
  so_list_head = NULL;
  cache_solib_p = NULL;

  som_solib_total_st_size = (LONGEST) 0;
  main_st_size_added = false;
  som_solib_st_size_threshold_exceeded = 0;

  dld_mmap_calls = 0;
  dld_text_offset = 0;
  dld_data_offset = 0;
  
  dld_cache.is_valid = 0;

  dld_cache.hook.address = 0;
  dld_cache.hook.unwind = NULL;

  dld_cache.hook_stub.address = 0;
  dld_cache.hook_stub.unwind = NULL;

  dld_cache.load.address = 0;
  dld_cache.load.unwind = NULL;

  dld_cache.load_stub.address = 0;
  dld_cache.load_stub.unwind = NULL;

  dld_cache.unload.address = 0;
  dld_cache.unload.unwind = NULL;

  dld_cache.unload2.address = 0;
  dld_cache.unload2.unwind = NULL;

  dld_cache.unload_stub.address = 0;
  dld_cache.unload_stub.unwind = NULL;
}

/* Arguments are not used */
static void
fix_auto_solib_add(char *args, int from_tty, struct cmd_list_element * c)
{
  if (auto_solib_add == 0)
    {
      warning ("auto-solib-add adjusted to 1, a setting of 0 would interfere with debugging");
      auto_solib_add = 1;
    }
}
#endif

void
_initialize_som_solib ()
{
  /* jini: mixed mode support: JAGag21714: Retaining a dummy
     "_initialize_som_solib" function for IPF since the inclusion
     of somsolib.c among the files to be built results in this function
     being referenced by init.c in the build directory. */
#ifndef HP_IA64
  struct rlimit rlp;
  char *s, *gdb_shlib_path = NULL, *token;
  int i; 
  struct cmd_list_element *set, *show;


  add_com ("sharedlibrary", class_files, som_solib_sharedlibrary_command,
	   "Load shared object library symbols for files matching REGEXP.");
  add_info ("sharedlibrary", som_sharedlibrary_info_command,
	    "Status of loaded shared object libraries.");

  /* Srikanth, intel would like to disallow setting auto-solib-add to 0.
     They must use this for their Linux debugs so their .gdbinit contains
     this, but allowing this on HP-UX is disastrous as we don't even
     process the linker symbol table and the unwind tables. Jul 30th 2002.
  */
  set = add_set_cmd ("auto-solib-add", class_support, var_zinteger,
		  (char *) &auto_solib_add,
		  "Set autoloading size threshold (in megabytes) of shared library symbols.\n\
If nonzero, symbols from all shared object libraries will be loaded\n\
automatically when the inferior begins execution or when the dynamic linker\n\
informs gdb that a new library has been loaded, until the symbol table\n\
of the program and libraries exceeds this threshold.\n\
Otherwise, symbols must be loaded manually, using `sharedlibrary'.",
		  &setlist);
 show =  add_show_from_set (set, &showlist);
 set -> function.sfunc = fix_auto_solib_add;

  /* ??rehrauer: On HP-UX, the kernel parameter MAXDSIZ limits how much
     data space a process can use.  We ought to be reading MAXDSIZ and
     setting auto_solib_add to some large fraction of that value.  If
     not that, we maybe ought to be setting it smaller than the default
     for MAXDSIZ (that being 64Mb, I believe).  However, [1] this threshold
     is only crudely approximated rather than actually measured, and [2]
     50 Mbytes is too small for debugging gdb itself.  Thus, the arbitrary
     100 figure.
   */
  /* RM: Using MAXDSIZ to set the default threshold now */
  /* auto_solib_add = 100;  */ 
 
  getrlimit(RLIMIT_DATA, &rlp);
  auto_solib_add = ((rlp.rlim_cur / 1000000) * 3) / 4;

  /* RM: initialize gdb_shlib_root */
  s = getenv("GDB_SHLIB_ROOT");
  if (s)
    {
      gdb_shlib_root = xmalloc(strlen(s) + 1);
      strcpy(gdb_shlib_root, s);
    }
  else
    {
      gdb_shlib_root = 0;
    }

  /* srikanth, 001117, Consult environment for GDB_SHLIB_PATH and
     if defined use that as the directory where the libraries will
     be found. This is useful for debugging core files produced on
     a different system. (JAGab25944)

     stacey, 6/29/2001 GDB_SHLIB_PATH can be either a single directory
     as mentioned above or a colon separated list of directories where
     the libraries will be found.  If the user enters a colon separated
     list, gdb will search the directories in the order the user has 
     entered them from first to last.  The code below parses the user's
     colon separated text into separated library names and stores them 
     in gdb_shlib_path - a pointer to a dynamically allocated array of 
     directory names (JAGab25944)  */

  s = getenv("GDB_SHLIB_PATH");
  gdb_shlib_path = s ? strdup (s) : s;

  if (gdb_shlib_path != NULL)
    {
      gdb_shlib_path_length = 1;

      /* At this point, we know the user has entered at least one
	 shared library path.  Count the colons in the gdb_shlib_path 
	 string to find out how many additional directory paths the 
	 user has entered.  */

      for (i = 0; gdb_shlib_path[i] != NULL; i++)
	if (gdb_shlib_path[i] == ':')
	  gdb_shlib_path_length++;      

      /* Now parse the gdb_shlib_path and separate into different directories
	 Treat ':'s as separators */

      gdb_shlib_path_list = xmalloc (gdb_shlib_path_length * sizeof (char *));

      for (token = strtok (gdb_shlib_path, ":"), i=0;
	   token != NULL;
	   token = strtok (NULL, ":"), i++)
	gdb_shlib_path_list[i] = strdup (token);

      free (gdb_shlib_path);
    }
  som_solib_restart ();
#endif
}

#ifndef HP_IA64
/* Get some HPUX-specific data from a shared lib.
 */
CORE_ADDR
so_lib_thread_start_addr (struct so_list *so)
{
  return ((struct som_solib_mapped_entry *)(so->solib_desc))->tsd_start_addr;
}
#endif

/* Get some HPUX-specific data from a shared lib.
 */
CORE_ADDR
so_lib_thread_index (struct so_list *so)
{
  return ((struct som_solib_mapped_entry *)(so->solib_desc))->tsd_index;
}

/* Given a dllname identify whether it has already been loaded */
boolean
som_solib_isloaded (char *dllname)
{
  struct so_list *so_list = so_list_head;

  /* Do a substring match rather than a full compare. This is useful
     to catch the load of a library without having to specify its
     full pathname.  -- srikanth, 000314, JAGaa80611
  */

  while (so_list)
    {
      struct som_solib_mapped_entry *solib_desc =
        (struct som_solib_mapped_entry *) (so_list->solib_desc);

      if (strstr(solib_desc->name, dllname))
        return true;
      so_list = so_list->next;
     }

  return false;
}

void som_solib_remove (struct objfile * objfile)
{
  struct so_list * so_list, * prev_so_list = NULL;
  for (so_list = so_list_head; so_list; so_list = so_list->next)
    {
      if (so_list->objfile == objfile)
        {
          if (prev_so_list != NULL)
            prev_so_list->next = so_list->next;
	  /* JYG 000906: shouldn't we also free section tables here? */
	  free (so_list->sections);
	  /* JYG 000906: som_solib.name is now a malloc'd storage */
          /* jini: mixed mode changes: som_solib.name is now solib_desc->name */
          free (((struct som_solib_mapped_entry *)(so_list->solib_desc))->name);
          free (so_list->solib_desc);
          free (so_list);
          break;
        }
      prev_so_list = so_list;
    }
}

/* JAGaf54436: Removed functions handle_shared_load_return()
   and handle_shared_load_break() */

/* ----------------------------------------------------------------------------------
 * Description: This function takes a so_list* as an argument, reads the file_time for
 * the associated shared library  from its som header and compares that to 
 * the one returned by mapped_shl_entry for the same shared library in the core.
 * If they don't match a warning message is issued. 
 * Fix for JAGad89789, JAGaf09159
 * -----------------------------------------------------------------------------------*/

static void
hppa_core_so_mismatch_detection (struct so_list *so_ptr)
{
  static int disabled;
  static int printed_once;
  char *buf;
  asection *shlib_sect;
  int sect_size;
  unsigned int secs;
  unsigned int nanosecs;
  struct header file_hdr;
  int status;

  struct som_solib_mapped_entry *solib_desc =
    (struct som_solib_mapped_entry *) so_ptr->solib_desc;

  /* This functionality depends upon a new version of ld.
   * If this ld isn't used, this needs to be disabled to prevent wrong
   * information from being given out. 
   * If the gdb is using an old ld, irrespective of whether the core used the new
   * or the old ld, disable mismatch detection for that shared library.
   */
  if (solib_desc->file_time.secs == 0 || solib_desc->struct_version <= 2)
      return;
 
  if (bfd_seek (so_ptr->abfd, 0, SEEK_SET) != 0)
    error ("Error in bfd_seek: %s \n", safe_strerror (errno));
  
  status = bfd_read ((void*) &file_hdr, 1, sizeof (struct header), so_ptr->abfd);
  if (status <= 0)
      error ("Error in bfd_read: %s \n", safe_strerror (errno));

  if (solib_desc->file_time.secs != file_hdr.file_time.secs)
   {
      if (!printed_once)
        {
           warning ("Some of the libraries in the core file are different from the libraries on this computer.  It might be possible to proceed with your debugging process successfully.  However, if you run into problems you must use the versions of the libraries used by the core.  The mismatches are: \n");
           printed_once = 1;
        }
      printf ("  %s in the core file is different from\n  %s used by gdb\n\n",
               so_pathname_g, solib_desc->name); 
   }
} 

#ifndef HP_IA64
/* JAGaf26681 - A return value of 1 implies that the shared libraries have been mapped private. */
int
is_solib_mapped_private ()
{
	return (dld_has_map_private_flag || dlheader_has_map_private_flag);
}
#endif

#if  (defined(HPUXHPPA) && !defined(BFD64))
/* PA32 */
#endif
