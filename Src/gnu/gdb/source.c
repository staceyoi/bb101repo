/* List lines of source files for GDB, the GNU debugger.
   Copyright 1986-1989, 1991-1999 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "limits.h"
#include "defs.h"
#include "symtab.h"
#include "expression.h"
#include "language.h"
#include "command.h"
#include "source.h"
#include "gdbcmd.h"
#include "frame.h"
#include "value.h"

#include <sys/types.h>
#include "gdb_string.h"
#include "gdb_stat.h"
#include <fcntl.h>
#include <ctype.h>
#include "gdbcore.h"
#include "gdb_regex.h"
#include "symfile.h"
#include "objfiles.h"
#include "annotate.h"
#include "gdbtypes.h"
#include "hpread.h"
#ifdef UI_OUT
#include "ui-out.h"
#endif
#include "top.h"

#ifdef CRLF_SOURCE_FILES

/* Define CRLF_SOURCE_FILES in an xm-*.h file if source files on the
   host use \r\n rather than just \n.  Defining CRLF_SOURCE_FILES is
   much faster than defining LSEEK_NOT_LINEAR.  */

#ifndef O_BINARY
#define O_BINARY 0
#endif

#define OPEN_MODE (O_RDONLY | O_BINARY)
#define FDOPEN_MODE FOPEN_RB

#else /* ! defined (CRLF_SOURCE_FILES) */

#define OPEN_MODE O_RDONLY
#define FDOPEN_MODE FOPEN_RT

#endif /* ! defined (CRLF_SOURCE_FILES) */

extern int assembly_level_debugging;
CORE_ADDR nimbus_current_pc = (CORE_ADDR) 0;
char *nimbus_temp_files[MAX_TEMP_FILES] = { NULL };
static void layout_command (char *arg, int from_tty);

extern void objectretry_command (char *arg, int from_tty);

/* Prototypes for exported functions. */

void _initialize_source (void);

extern int nimbus_version;
extern struct cleanup *make_cleanup (void (*function) (PTR), PTR arg);

/* Prototypes for local functions. */

static int get_filename_and_charpos (struct symtab *, char **);

static void reverse_search_command (char *, int);

static void forward_search_command (char *, int);

void line_info (char *, int);

static void list_command (char *, int);

static int is_identical_line_spec (struct symtabs_and_lines *);
static void ambiguous_line_spec (struct symtabs_and_lines *);

static void source_info (char *, int);

static void show_directories (char *, int);

static void edit_command (char *, int);

#ifdef PATHMAP
static void pathmap_command (char *map_str, int from_tty);
static void pathmap_add (char *from, char *to);
static int pathmap_lookup (char *file, char *full_path_buf, int mode);
void pathmap_set_cdir (char *cdir);
static void show_pathmap (char *, int); /* JAGaf32389 */

/* pathmap is a series of 'from' and 'to' mappings that are applied on 
 * compilation directories to locate object files and associated source
 * files.  The map is implemented as a linked list of mappings, each of
 * which has a 'from' string and a 'to' string.  pathmap_t defines one
 * mapping.  For each mapping the compilation directory string is searched
 * for the 'from' substring.  If it exisits then it is replaced with 'to'
 * and file is checked for existence.  See the pathmap section in this file
 * and 'help pathmap' for more information on pathmap.
 */
struct pathmap_t
{
  char *from;
  char *to;
  struct pathmap_t *next;
};

/* All globals used by pathmap are grouped in this structure for encapsulation.
 */
struct pathmap_globals_t
{
  struct pathmap_t *head;	/* Points to the first mapping of the pathmap. */
  struct pathmap_t *tail;	/* Points to the last mapping of the pathmap. */
  char *cdir_p;			/* Points to a copy of the compilation directory */
};

struct pathmap_globals_t pathmap_g;
#endif /* PATHMAP */ 

extern void mfree (PTR md, PTR ptr);

/* Path of directories to search for source files.
   Same format as the PATH environment variable's value.  */

char *source_path;

/* Path of directories to search for object files.
   Same format as the PATH environment variable's value.  */

char *object_path;

/* Symtab of default file for listing lines of.  */

struct symtab *current_source_symtab;

/* Default next line to list.  */

int current_source_line;

int main_line; /* JAGaf60778 */

/* JAGaf49959 Sunil S 13/03/2006
   By default, output of breakpoint command displays the relative path.
   We can make it display the absolute path name by setting this varible.
*/

int display_full_path = 0;

/* Default number of lines to print with commands like "list".
   This is based on guessing how many long (i.e. more than chars_per_line
   characters) lines there will be.  To be completely correct, "list"
   and friends should be rewritten to count characters and see where
   things are wrapping, but that would be a fair amount of work.  */

int lines_to_list = 10;

/* Line number of last line printed.  Default for various commands.
   current_source_line is usually, but not always, the same as this.  */

static int last_line_listed;

/* First line number listed by last listing command.  */

static int first_line_listed;

/* Saves the name of the last source file visited and a possible error code.
   Used to prevent repeating annoying "No such file or directories" msgs */

static struct symtab *last_source_visited = NULL;
static int last_source_error = 0;

/* If namespaces is disabled then set to 0 */
extern int namespace_enabled;

extern int firebolt_version;

/* Set the source file default for the "list" command to be S.

   If S is NULL, and we don't have a default, find one.  This
   should only be called when the user actually tries to use the
   default, since we produce an error if we can't find a reasonable
   default.  Also, since this can cause symbols to be read, doing it
   before we need to would make things slower than necessary.  */

void
select_source_symtab (register struct symtab *s)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  struct partial_symtab *ps;
  struct partial_symtab *cs_pst = 0;
  struct objfile *ofp;

  if (s)
    {
      current_source_symtab = s;
      current_source_line = 1;
      return;
    }

  if (current_source_symtab)
    return;

  /* Make the default place to list be the function `main'
     if one exists.  */
  if (lookup_symbol (default_main, 0, VAR_NAMESPACE, 0, NULL))
    {
      sals = decode_line_spec (default_main, 1);
      sal = sals.sals[0];
      free (sals.sals);
      current_source_symtab = sal.symtab;
      current_source_line = max (sal.line - (lines_to_list - 1), 1);
      if (current_source_symtab)
	return;
    }

  /* All right; find the last file in the symtab list (ignoring .h's).  */

  current_source_line = 1;

  for (ofp = object_files; ofp != NULL; ofp = ofp->next)
    {
      for (s = ofp->symtabs; s; s = s->next)
	{
	  char *name = s->filename;
	  int len = (int) strlen (name);
	  if (!(len > 2 && (STREQ (&name[len - 2], ".h"))))
	    {
	      current_source_symtab = s;
	    }
	}
    }
  if (current_source_symtab)
    return;

  /* Howabout the partial symbol tables? */

  for (ofp = object_files; ofp != NULL; ofp = ofp->next)
    {
      for (ps = ofp->psymtabs; ps != NULL; ps = ps->next)
	{
	  char *name = ps->filename;
	  int len = (int) strlen (name);
	  if (!(len > 2 && (STREQ (&name[len - 2], ".h"))))
	    {
	      cs_pst = ps;
	    }
	}
    }
  if (cs_pst)
    {
      if (cs_pst->readin)
	{
	  internal_error ("select_source_symtab: readin pst found and no symtabs.");
	}
      else
	{
	  current_source_symtab = PSYMTAB_TO_SYMTAB (cs_pst);
	}
    }
  if (current_source_symtab)
    return;

  error ("Can't find a default source file");
}

static void
show_directories (char *ignore, int from_tty)
{
  puts_filtered ("Source directories searched: ");
  puts_filtered (source_path);
  puts_filtered ("\n");
}

static void
show_object_directories (char *ignore, int from_tty)
{
  puts_filtered ("Object directories searched: ");
  puts_filtered (object_path);
  puts_filtered ("\n");
}

/* JAGaf32389 */
#ifdef PATHMAP
static void
show_pathmap (char *ignore, int from_tty)
{
  struct pathmap_t *p = pathmap_g.head;

  if (p)
    puts_filtered ("Pathmap directories searched: \n");
  else
    puts_filtered ("Pathmap directories searched: <empty>\n");

  while (p)
    {
      printf_unfiltered ("From: %s\tTo: %s\n", p->from, p->to);
      p = p->next;
    }
}
#endif /* PATHMAP */

/* Forget what we learned about line positions in source files, and
   which directories contain them; must check again now since files
   may be found in a different directory now.  */

void
forget_cached_source_info ()
{
  register struct symtab *s;
  register struct objfile *objfile;

  for (objfile = object_files; objfile != NULL; objfile = objfile->next)
    {
      for (s = objfile->symtabs; s != NULL; s = s->next)
	{
	  if (s->line_charpos != NULL)
	    {
	      mfree (objfile->md, s->line_charpos);
	      s->line_charpos = NULL;
	    }
	  if (s->fullname != NULL)
	    {
	      mfree (objfile->md, s->fullname);
	      s->fullname = NULL;
	    }
	}
    }
}

void
init_source_path ()
{
  char buf[20];

  sprintf (buf, "$cdir%c$cwd", DIRNAME_SEPARATOR);
  source_path = strsave (buf);
  forget_cached_source_info ();
}

void
init_object_path ()
{
  char buf[20];

  sprintf (buf, "$cdir%c$cwd", DIRNAME_SEPARATOR);
  object_path = strsave (buf);
}

/* Add zero or more directories to the front of the source path.  */

void
directory_command (char *dirname, int from_tty)
{
  int desc = -1, 
    executable_specified = (have_full_symbols () || have_partial_symbols ());

  dont_repeat ();

  /* Stacey - JAGab16696 - August 10th, 2001 
     In addition to updating the list of directories gdb uses to search
     for source files, the directory command should update the source
     display when all of the following are true.  

     1) we're in TUI mode (this condition can be lifted later)

     2) the user has specified an executable file using the file command
        or upon start up (ie gdb a.out)

     3) the user has specified a new directory to be added to the list
        for example: dir /tmp  Note: re-setting the directory list to empty 
	should not effect the source display.  

     4) a previous attempt to display the source failed:
        source files for program or shared library are in a different as 
	well as unspecified directory from executable resulting in gdb 
	being unable to print source code.    
     
     5) the new directory contains the an apropriate source file */

  if (tui_version && executable_specified && (dirname != NULL) )
    {
      /* if the user performs the 'dir' command before running his
	 program, it will be necessary to select a symbol table to use */

      if (current_source_symtab == 0)
	select_source_symtab (0);
      
      /* Check whether gdb's previous attempt to display source failed.  
	 The 'desc' variable will tell us whether or not gdb was able to open 
	 the source file.  If gdb is unable to open the file now, it probably
	 failed to do so earlier as well. */
	 
      desc = open_source_file (current_source_symtab);
      close (desc);
    }

  /* FIXME, this goes to "delete dir"... */
  if (dirname == 0)
    {
      if (from_tty && query ("Reinitialize source path to empty? "))
	{
	  free (source_path);
	  init_source_path ();
	}
    }
  else
    {
      mod_path (dirname, &source_path);
      last_source_visited = NULL;
    }
  if (from_tty || annotation_level == 2) //For JAGaf45739 
    show_directories ((char *) 0, from_tty);
  forget_cached_source_info ();

  if (nimbus_version)
    {
      char * gdb_path = strdup (source_path);
      if (gdb_path)
        {
          char * dollar = strchr (gdb_path, '$');
          if (dollar)
            *dollar = 0;
          printf_unfiltered ("\033\032\032:se dir %s\n\032\032A", gdb_path);
          gdb_flush (gdb_stdout);
       }
      free (gdb_path);
    }

  /* Stacey August 10th 2001 - fix for JAGab16696
     The code below asks gdb to update the source display in TUI mode.  
     the 4 conditions in the if-statement match the 4 conditions listed
     in the comment at the beginning of this function.  */

  if ( (tui_version) && (dirname != NULL) && 
       executable_specified && (desc < 0))
    {
      /* now check to see if the new directory contains the file we want.  
	 if (desc < 0), then the directory added by this call doesn't 
	 contain the apropriate source file needed to refresh the display */

      desc = open_source_file (current_source_symtab);
      if (desc < 0)
	return;
      else 
	{
	  /* if we get here, we know that one of the directories
	     that was added contains the correct source file.*/

	  if (current_source_symtab->line_charpos == 0)
	    find_source_lines (current_source_symtab, desc);

	  /* close the descriptor as soon as we no longer need it - in case
	     an error occurs elsewhere in gdb.  */
	  close (desc);
	  
	  print_source_lines (current_source_symtab, current_source_line,
			      current_source_line + lines_to_list, 0);
	}
    }
}

/* Add zero or more directories to the front of the object path.  */
 
void
objectdir_command (char *dirname, int from_tty)
{
  dont_repeat ();
  /* FIXME, this goes to "delete dir"... */
  if (dirname == 0)
    {
      if (query ("Reinitialize object path to empty? "))
        {
          free (object_path);
          init_object_path ();
        }
    }
  else
    {
       mod_path (dirname, &object_path);
       objectretry_command (NULL, 0);
     }
  if (from_tty || annotation_level == 2) //For JAGaf45739 
    show_object_directories ((char *)0, from_tty);
}

/* Add zero or more directories to the front of an arbitrary path.  */

void
mod_path (char *dirname, char **which_path)
{
  char *old = *which_path;
  int prefix = 0;

  if (dirname == 0)
    return;

  dirname = strsave (dirname);
  make_cleanup (free, dirname);

  do
    {
      char *name = dirname;
      register char *p;
      struct stat st;

      {
	char *separator = strchr (name, DIRNAME_SEPARATOR);
	char *space = strchr (name, ' ');
	char *tab = strchr (name, '\t');

	if (separator == 0 && space == 0 && tab == 0)
	  p = dirname = name + (int) strlen (name);
	else
	  {
	    p = 0;
	    if (separator != 0 && (p == 0 || separator < p))
	      p = separator;
	    if (space != 0 && (p == 0 || space < p))
	      p = space;
	    if (tab != 0 && (p == 0 || tab < p))
	      p = tab;
	    dirname = p + 1;
	    while (*dirname == DIRNAME_SEPARATOR
		   || *dirname == ' '
		   || *dirname == '\t')
	      ++dirname;
	  }
      }

      if (!(SLASH_P (*name) && p <= name + 1)	/* "/" */
#if defined(_WIN32) || defined(__MSDOS__)
      /* On MS-DOS and MS-Windows, h:\ is different from h: */
	  && !(!SLASH_P (*name) && ROOTED_P (name) && p <= name + 3)	/* d:/ */
#endif
	  && SLASH_P (p[-1]))
	/* Sigh. "foo/" => "foo" */
	--p;
      *p = '\0';

      while (p > name && p[-1] == '.')
	{
	  if (p - name == 1)
	    {
	      /* "." => getwd ().  */
	      name = current_directory;
	      goto append;
	    }
	  else if (p > name + 1 && SLASH_P (p[-2]))
	    {
	      if (p - name == 2)
		{
		  /* "/." => "/".  */
		  *--p = '\0';
		  goto append;
		}
	      else
		{
		  /* "...foo/." => "...foo".  */
		  p -= 2;
		  *p = '\0';
		  continue;
		}
	    }
	  else
	    break;
	}

      if (name[0] == '~')
	name = tilde_expand (name);
#if defined(_WIN32) || defined(__MSDOS__)
      else if (ROOTED_P (name) && p == name + 2)	/* "d:" => "d:." */
	name = concat (name, ".", NULL);
#endif
      else if (!ROOTED_P (name) && name[0] != '$')
	name = concat (current_directory, SLASH_STRING, name, NULL);
      else
	name = savestring (name,(int) (p - name));
      make_cleanup (free, name);

      /* Unless it's a variable, check existence.  */
      if (name[0] != '$')
	{
	  /* These are warnings, not errors, since we don't want a
	     non-existent directory in a .gdbinit file to stop processing
	     of the .gdbinit file.

	     Whether they get added to the path is more debatable.  Current
	     answer is yes, in case the user wants to go make the directory
	     or whatever.  If the directory continues to not exist/not be
	     a directory/etc, then having them in the path should be
	     harmless.  */
	  if (stat (name, &st) < 0)
	    {
	      int save_errno = errno;
	      fprintf_unfiltered (gdb_stderr, "Warning: ");
	      print_sys_errmsg (name, save_errno);
	    }
	  else if ((st.st_mode & S_IFMT) != S_IFDIR)
	    warning ("%s is not a directory.", name);
	}

    append:
      {
	register unsigned int len = (unsigned int) strlen (name);

	p = *which_path;
	while (1)
	  {
	    /* FIXME: strncmp loses in interesting ways on MS-DOS and
	       MS-Windows because of case-insensitivity and two different
	       but functionally identical slash characters.  We need a
	       special filesystem-dependent file-name comparison function.

	       Actually, even on Unix I would use realpath() or its work-
	       alike before comparing.  Then all the code above which
	       removes excess slashes and dots could simply go away.  */
	    if (!strncmp (p, name, len)
		&& (p[len] == '\0' || p[len] == DIRNAME_SEPARATOR))
	      {
		/* Found it in the search path, remove old copy */
		if (p > *which_path)
		  p--;		/* Back over leading separator */
		if (prefix > p - *which_path)
		  goto skip_dup;	/* Same dir twice in one cmd */
		strcpy (p, &p[len + 1]);	/* Copy from next \0 or  : */
	      }
	    p = strchr (p, DIRNAME_SEPARATOR);
	    if (p != 0)
	      ++p;
	    else
	      break;
	  }
	if (p == 0)
	  {
	    char tinybuf[2];

	    tinybuf[0] = DIRNAME_SEPARATOR;
	    tinybuf[1] = '\0';

	    /* If we have already tacked on a name(s) in this command,                     be sure they stay on the front as we tack on some more.  */
	    if (prefix)
	      {
		char *temp, c;

		c = old[prefix];
		old[prefix] = '\0';
		temp = concat (old, tinybuf, name, NULL);
		old[prefix] = c;
		*which_path = concat (temp, "", &old[prefix], NULL);
		prefix = (int) strlen (temp);
		free (temp);
	      }
	    else
	      {
		*which_path = concat (name, (old[0] ? tinybuf : old), old, NULL);
		prefix = (int) strlen (name);
	      }
	    free (old);
	    old = *which_path;
	  }
      }
    skip_dup:;
    }
  while (*dirname != '\0');
}


static void
source_info (char *ignore, int from_tty)
{
  register struct symtab *s = current_source_symtab;

  if (!s)
    {
      printf_filtered ("No current source file.\n");
      return;
    }
  printf_filtered ("Current source file is %s\n", s->filename);
  if (s->dirname)
    printf_filtered ("Compilation directory is %s\n", s->dirname);
  if (s->fullname)
    printf_filtered ("Located in %s\n", s->fullname);
  if (s->nlines)
    printf_filtered ("Contains %d line%s.\n", s->nlines,
		     s->nlines == 1 ? "" : "s");

  printf_filtered ("Source language is %s.\n", language_str (s->language));
  printf_filtered ("Compiled with %s debugging format.\n", s->debugformat);
}



/* Open a file named STRING, searching path PATH (dir names sep by some char)
   using mode MODE and protection bits PROT in the calls to open.

   If TRY_CWD_FIRST, try to open ./STRING before searching PATH.
   (ie pretend the first element of PATH is ".").  This also indicates
   that a slash in STRING disables searching of the path (this is
   so that "exec-file ./foo" or "symbol-file ./foo" insures that you
   get that particular version of foo or an error message).

   If FILENAMED_OPENED is non-null, set it to a newly allocated string naming
   the actual file opened (this string will always start with a "/".  We
   have to take special pains to avoid doubling the "/" between the directory
   and the file, sigh!  Emacs gets confuzzed by this when we print the
   source file name!!! 

   If a file is found, return the descriptor.
   Otherwise, return -1, with errno set for the last name we tried to open.  */

/*  >>>> This should only allow files of certain types,
   >>>>  eg executable, non-directory */
int
openp (char *path,
        int try_cwd_first,
        char *string,
        int mode,
        int prot,
        char **filename_opened)
{
  register int fd;
  register char *filename;
  register char *p, *p1;
  register int len;
  int alloclen;

  if (!path)
    path = ".";

#ifdef _WIN32
  mode |= O_BINARY;
#endif

  if (try_cwd_first)
    {
      int i;
      filename = string;
      fd = (filename? open (filename, mode, prot) : -1);
      if (fd >= 0)
	goto done;
      for (i = 0; string[i]; i++)
	if (SLASH_P (string[i]))
	  goto done;
    }

  /* ./foo => foo */
  while (string[0] == '.' && SLASH_P (string[1]))
    string += 2;

  alloclen = (int) strlen (path) + (int) strlen (string) + 2;
  filename = (char *) alloca (alloclen);
  fd = -1;
  for (p = path; p; p = p1 ? p1 + 1 : 0)
    {
      p1 = (char *) strchr (p, DIRNAME_SEPARATOR);
      if (p1)
	len = (int)(p1 - p);
      else
	len = (int) strlen (p);

      if (len == 4 && p[0] == '$' && p[1] == 'c'
	  && p[2] == 'w' && p[3] == 'd')
	{
	  /* Name is $cwd -- insert current directory name instead.  */
	  int newlen;

	  /* First, realloc the filename buffer if too short. */
	  len = (int) strlen (current_directory);
	  newlen = len + (int) strlen (string) + 2;
	  if (newlen > alloclen)
	    {
	      alloclen = newlen;
	      filename = (char *) alloca (alloclen);
	    }
	  strcpy (filename, current_directory);
	}
      else
	{
	  /* Normal file name in path -- just use it.  */
	  strncpy (filename, p, len);
	  filename[len] = 0;
	}

      /* Remove trailing slashes */
      while (len > 0 && SLASH_P (filename[len - 1]))
	filename[--len] = 0;

      strcat (filename + len, SLASH_STRING);
      strcat (filename, string);

      fd = open (filename, mode);
      if (fd >= 0)
	break;
    }

#ifdef PATHMAP
    /* Try applying the mappings in the pathmap before giving up. */
    if (fd < 0)
      fd = pathmap_lookup (string, filename, mode);
#endif /* PATHMAP */

done:
#ifdef PATHMAP
  /* Compilation directory is .o specific; we need to reset it to avoid 
   * using stale pointers because openp() can be called by paths that
   * don't set the compilation directory for pathmap.
   */
  if (pathmap_g.cdir_p != NULL)
    {
      free (pathmap_g.cdir_p);
      pathmap_g.cdir_p = NULL;
    }
#endif /* PATHMAP */

  if (filename_opened)
    {
      if (fd < 0)
	*filename_opened = (char *) 0;
      else if ((filename ? ROOTED_P (filename) : NULL))
	*filename_opened = savestring (filename, (int) strlen (filename));
      else
	{
	  /* Beware the // my son, the Emacs barfs, the botch that catch... */

	  *filename_opened = concat (current_directory,
		 SLASH_P (current_directory[strlen (current_directory) - 1])
				     ? "" : SLASH_STRING,
				     filename, NULL);
	}
    }
#ifdef MPW
  /* This is a debugging hack that can go away when all combinations
     of Mac and Unix names are handled reasonably.  */
  {
    extern int debug_openp;

    if (debug_openp)
      {
	printf ("openp on %s, path %s mode %d prot %d\n  returned %d",
		string, path, mode, prot, fd);
	if (*filename_opened)
	  printf (" (filename is %s)", *filename_opened);
	printf ("\n");
      }
  }
#endif /* MPW */

  return fd;
}


/* This is essentially a convenience, for clients that want the behaviour
   of openp, using source_path, but that really don't want the file to be
   opened but want instead just to know what the full pathname is (as
   qualified against source_path).

   The current working directory is searched first.

   If the file was found, this function returns 1, and FULL_PATHNAME is
   set to the fully-qualified pathname.

   Else, this functions returns 0, and FULL_PATHNAME is set to NULL.
 */
int
source_full_path_of (char *filename,
                     char **full_pathname)
{
  int fd;

  fd = openp (source_path, 0, filename, O_RDONLY, 0, full_pathname);
  if (fd < 0)
    {
      *full_pathname = NULL;
      return 0;
    }

  close (fd);
  return 1;
}

/* Sunil JAGaf49959 07/03/06 */
int
find_full_path_of (char *filename,
                   char **full_pathname)
{
  int fd;

  fd = openp (source_path, 1, filename, O_RDONLY, 0, full_pathname);
  if (fd < 0)
    {
      *full_pathname = NULL;
      return 0;
    }

  close (fd);
  return 1;
}

/* Get the absolute file name without '.', '..', or symbolic links */

char *
pathopt (char *filename)
{
  char *real_pathname =  xmalloc (PATH_MAX + 1);

  if (realpath (filename, real_pathname))
    {
      return real_pathname;
    }
  else
    {
      free (real_pathname);
      return strdup(filename);
    }
}

/* Get the file name after the last '/' */

char *
getbasename (char *name)
{
  char * p;
  if ((p = strrchr (name, '/')))
    return p + 1;
  else
    return name;
}


/* Open a source file given a symtab S.  Returns a file descriptor or
   negative number for error.  */

int
open_source_file (struct symtab *s)
{
  char *path = source_path;
  char *p;
  int result;
  char *fullname;
  char *save_filename = NULL;
  
  /* Quick way out if we already know its full name */
  if (s->fullname)
    {
      result = open (s->fullname, OPEN_MODE);
      if (result >= 0)
	return result;
      /* Didn't work -- free old one, try again. */
      mfree (s->objfile->md, s->fullname);
      s->fullname = NULL;
    }

  if (s->dirname != NULL)
    {
      /* Replace a path entry of  $cdir  with the compilation directory name */
#define	cdir_len	5
      /* We cast strstr's result in case an ANSIhole has made it const,
         which produces a "required warning" when assigned to a nonconst. */
      p = (char *) strstr (source_path, "$cdir");
      if (p && (p == path || p[-1] == DIRNAME_SEPARATOR)
	  && (p[cdir_len] == DIRNAME_SEPARATOR || p[cdir_len] == '\0'))
	{
	  int len;

	  path = (char *)
	    alloca (strlen (source_path) + 1 + strlen (s->dirname) + 1);
	  len = (int)(p - source_path);
	  strncpy (path, source_path, len);	/* Before $cdir */
	  strcpy (path + len, s->dirname);	/* new stuff */
	  strcat (path + len, source_path + len + cdir_len);	/* After $cdir */
#ifdef PATHMAP
	  pathmap_set_cdir (path + len);
#endif /* PATHMAP */
	}
    }

  result = openp (path, 0, s->filename, OPEN_MODE, 0, &s->fullname);
  if (result < 0)
    {
      /* Didn't work.  Try using just the basename. */
      save_filename = strdup(s->filename);
      p = basename (save_filename);
      if (p != s->filename)
        result = openp (path, 0, p, O_RDONLY, 0, &s->fullname);
      free(save_filename);
    }
  
  /* If still not found and a rooted path, try the rooted path */
  if (result < 0 && ROOTED_P (s->filename))
    result = openp (path, 1, s->filename, OPEN_MODE, 0, &s->fullname);

#ifdef MPW
  if (result < 0)
    {
      /* Didn't work.  Try using just the MPW basename. */
      p = (char *) mpw_basename (s->filename);
      if (p != s->filename)
	result = openp (path, 0, p, OPEN_MODE, 0, &s->fullname);
    }
  if (result < 0)
    {
      /* Didn't work.  Try using the mixed Unix/MPW basename. */
      p = (char *) mpw_mixed_basename (s->filename);
      if (p != s->filename)
	result = openp (path, 0, p, OPEN_MODE, 0, &s->fullname);
    }
#endif /* MPW */

  if (result >= 0)
    {
      fullname = s->fullname;
      s->fullname = mstrsave (s->objfile->md, s->fullname);
      free (fullname);
    }
  return result;
}

/* Return the path to the source file associated with symtab.  Returns NULL
   if no symtab.  */

char *
symtab_to_filename (struct symtab *s)
{
  int fd;

  if (!s)
    return NULL;

  /* If we've seen the file before, just return fullname. */

  if (s->fullname)
    return s->fullname;

  /* Try opening the file to setup fullname */

  fd = open_source_file (s);
  if (fd < 0)
    return s->filename;		/* File not found.  Just use short name */

  /* Found the file.  Cleanup and return the full name */

  close (fd);
  return s->fullname;
}


/* Create and initialize the table S->line_charpos that records
   the positions of the lines in the source file, which is assumed
   to be open on descriptor DESC.
   All set S->nlines to the number of such lines.  */

void
find_source_lines (struct symtab *s, int desc)
{
  struct stat st;
  register char *data, *p, *end;
  int nlines = 0;
  int lines_allocated = 1000;
  int *line_charpos;
  long mtime = 0;
  int size;

  line_charpos = (int *) xmmalloc (s->objfile->md,
				   lines_allocated * sizeof (int));
  if (fstat (desc, &st) < 0)
    perror_with_name (s->filename);

  if (s && s->objfile && s->objfile->obfd)
    mtime = bfd_get_mtime (s->objfile->obfd);
  else if (exec_bfd)
    mtime = bfd_get_mtime (exec_bfd);

  if (mtime && mtime < st.st_mtime)
    {
      if (tui_version)
	printf_filtered ("\n");
      if ( !(s->objfile->flags & OBJF_SHARED) )
        warning ("Source file is more recent than executable %s.\n",basename(s->objfile->name));
      else
	warning ("Source file is more recent than library %s.\n",basename(s->objfile->name));
    }

#ifdef LSEEK_NOT_LINEAR
  {
    char c;

    /* Have to read it byte by byte to find out where the chars live */

    line_charpos[0] = lseek (desc, 0, SEEK_CUR);
    nlines = 1;
    while (myread (desc, &c, 1) > 0)
      {
	if (c == '\n')
	  {
	    if (nlines == lines_allocated)
	      {
		lines_allocated *= 2;
		line_charpos =
		  (int *) xmrealloc (s->objfile->md, (char *) line_charpos,
				     sizeof (int) * lines_allocated);
	      }
	    line_charpos[nlines++] = lseek (desc, 0, SEEK_CUR);
	  }
      }
  }
#else /* lseek linear.  */
  {
    struct cleanup *old_cleanups;

    /* st_size might be a large type, but we only support source files whose 
       size fits in an int.  */
    size = (int) st.st_size;

    /* Use malloc, not alloca, because this may be pretty large, and we may
       run into various kinds of limits on stack size.  */
    data = (char *) xmalloc (size);
    old_cleanups = make_cleanup (free, data);

    /* Reassign `size' to result of read for systems where \r\n -> \n.  */
    size = myread (desc, data, size);
    if (size < 0)
      perror_with_name (s->filename);
    end = data + size;
    p = data;
    line_charpos[0] = 0;
    nlines = 1;
    while (p != end)
      {
	if (*p++ == '\n'
	/* A newline at the end does not start a new line.  */
	    && p != end)
	  {
	    if (nlines == lines_allocated)
	      {
		lines_allocated *= 2;
		line_charpos =
		  (int *) xmrealloc (s->objfile->md, (char *) line_charpos,
				     sizeof (int) * lines_allocated);
	      }
	    line_charpos[nlines++] = (int)(p - data);
	  }
      }
    do_cleanups (old_cleanups);
  }
#endif /* lseek linear.  */
  s->nlines = nlines;
  s->line_charpos =
    (int *) xmrealloc (s->objfile->md, (char *) line_charpos,
		       nlines * sizeof (int));

}

/* Return the character position of a line LINE in symtab S.
   Return 0 if anything is invalid.  */

#if 0				/* Currently unused */

int
source_line_charpos (s, line)
     struct symtab *s;
     int line;
{
  if (!s)
    return 0;
  if (!s->line_charpos || line <= 0)
    return 0;
  if (line > s->nlines)
    line = s->nlines;
  return s->line_charpos[line - 1];
}

/* Return the line number of character position POS in symtab S.  */

int
source_charpos_line (s, chr)
     register struct symtab *s;
     register int chr;
{
  register int line = 0;
  register int *lnp;

  if (s == 0 || s->line_charpos == 0)
    return 0;
  lnp = s->line_charpos;
  /* Files are usually short, so sequential search is Ok */
  while (line < s->nlines && *lnp <= chr)
    {
      line++;
      lnp++;
    }
  if (line >= s->nlines)
    line = s->nlines;
  return line;
}

#endif /* 0 */


/* Get full pathname and line number positions for a symtab.
   Return nonzero if line numbers may have changed.
   Set *FULLNAME to actual name of the file as found by `openp',
   or to 0 if the file is not found.  */

static int
get_filename_and_charpos (struct symtab *s,
                          char **fullname)
{
  register int desc, linenums_changed = 0;

  desc = open_source_file (s);
  if (desc < 0)
    {
      if (fullname)
	*fullname = NULL;
      return 0;
    }
  if (fullname)
    *fullname = s->fullname;
  if (s->line_charpos == 0)
    linenums_changed = 1;
  if (linenums_changed)
    find_source_lines (s, desc);
  close (desc);
  return linenums_changed;
}

/* Print text describing the full name of the source file S
   and the line number LINE and its corresponding character position.
   The text starts with two Ctrl-z so that the Emacs-GDB interface
   can easily find it.

   MID_STATEMENT is nonzero if the PC is not at the beginning of that line.

   Return 1 if successful, 0 if could not find the file.  */

int
identify_source_line (struct symtab *s,
                       int line,
                       int mid_statement,
                       CORE_ADDR pc)
{
  if (s->line_charpos == 0)
    get_filename_and_charpos (s, (char **) NULL);
  if (s->fullname == 0)
    return 0;
  if (line > s->nlines)
    /* Don't index off the end of the line_charpos array.  */
    return 0;
  annotate_source (s->fullname, line, s->line_charpos[line - 1],
		   mid_statement, pc);

  current_source_line = line;
  first_line_listed = line;
  last_line_listed = line;
  current_source_symtab = s;
  return 1;
}


/* Print source lines from the file of symtab S,
   starting with line number LINE and stopping before line number STOPLINE. */

static void print_source_lines_base (struct symtab *s, int line, int stopline,
				     int noerror);
static void
print_source_lines_base (struct symtab *s,
                          int line,
                          int stopline,
                          int noerror)
{
  register int c;
  register int desc;
  register FILE *stream;
  int nlines = stopline - line;

  /* Regardless of whether we can open the file, set current_source_symtab. */
  current_source_symtab = s;
  current_source_line = line;
  first_line_listed = line;

#ifdef UI_OUT
  /* If printing of source lines is disabled, just print file and line number */
  if (ui_out_test_flags (uiout, ui_source_list))
    {
#endif
      /* Only prints "No such file or directory" once */
      if ((s != last_source_visited) || (!last_source_error))
	{
	  last_source_visited = s;
	  desc = open_source_file (s);
	}
      else
	{
	  desc = last_source_error;
	  noerror = 1;
	}
#ifdef UI_OUT
    }
  else
    {
      desc = -1;
      noerror = 1;
    }
#endif

  if (desc < 0)
    {
      last_source_error = desc;

      if (!noerror)
	{
	  char *name = (char *) alloca (strlen (s->filename) + 100);
	  sprintf (name, "%d\t%s", line, s->filename);
	  print_sys_errmsg (name, errno);
	}
      else
#ifdef UI_OUT
	ui_out_field_int (uiout, "line", line);
      ui_out_text (uiout, "\tin ");
      ui_out_field_string (uiout, "file", s->filename);
      ui_out_text (uiout, "\n");
#else
	printf_filtered ("%d\tin %s\n", line, s->filename);
#endif

      return;
    }

  last_source_error = 0;

  if (s->line_charpos == 0)
    find_source_lines (s, desc);

  if (line < 1 || line > s->nlines)
    {
      close (desc);
      error ("Line number %d out of range; %s has %d lines.",
	     line, s->filename, s->nlines);
    }

  if (lseek (desc, s->line_charpos[line - 1], 0) < 0)
    {
      close (desc);
      perror_with_name (s->filename);
    }

  stream = fdopen (desc, FDOPEN_MODE);
  clearerr (stream);

  while (nlines-- > 0)
    {
#ifdef UI_OUT
      char buf[20];

      c = fgetc (stream);
      if (c == EOF)
	break;
      last_line_listed = current_source_line;
      sprintf (buf, "%d\t", current_source_line++);
      ui_out_text (uiout, buf);
      do
	{
	  if (c < 040 && c != '\t' && c != '\n' && c != '\r')
	    {
	      sprintf (buf, "^%c", c + 0100);
	      ui_out_text (uiout, buf);
	    }
	  else if (c == 0177)
	    ui_out_text (uiout, "^?");
#ifdef CRLF_SOURCE_FILES
	  else if (c == '\r')
	    {
	      /* Skip a \r character, but only before a \n.  */
	      int c1 = fgetc (stream);

	      if (c1 != '\n')
		printf_filtered ("^%c", c + 0100);
	      if (c1 != EOF)
		ungetc (c1, stream);
	    }
#endif
	  else
	    {
	      sprintf (buf, "%c", c);
	      ui_out_text (uiout, buf);
	    }
	}
      while (c != '\n' && (c = fgetc (stream)) >= 0);
#else
      c = fgetc (stream);
      if (c == EOF)
	break;
      last_line_listed = current_source_line;
      printf_filtered ("%d\t", current_source_line++);
      do
	{
	  if (c < 040 && c != '\t' && c != '\n' && c != '\r')
	    printf_filtered ("^%c", c + 0100);
	  else if (c == 0177)
	    printf_filtered ("^?");
#ifdef CRLF_SOURCE_FILES
	  else if (c == '\r')
	    {
	      /* Just skip \r characters.  */
	    }
#endif
	  else
	    printf_filtered ("%c", c);
	}
      while (c != '\n' && (c = fgetc (stream)) >= 0);
#endif
    }

  fclose (stream);
}

/* Show source lines from the file of symtab S, starting with line
   number LINE and stopping before line number STOPLINE.  If this is the
   not the command line version, then the source is shown in the source
   window otherwise it is simply printed */

void
print_source_lines ( struct symtab *s,
                     int line,
                     int stopline,
                     int noerror )
{
#if defined(TUI)
  if (!tui_version ||
      m_winPtrIsNull (srcWin) || !srcWin->generic.isVisible)
    print_source_lines_base (s, line, stopline, noerror);
  else
    {
      TuiGenWinInfoPtr locator = locatorWinInfoPtr ();
extern void tui_vAddWinToLayout (va_list);
extern void tui_vUpdateSourceWindowsWithLine (va_list);

      /* Regardless of whether we can open the file,
         set current_source_symtab. */
      current_source_symtab = s;
      current_source_line = line;
      first_line_listed = line;

      /* make sure that the source window is displayed */
      tuiDo ((TuiOpaqueFuncPtr) tui_vAddWinToLayout, SRC_WIN);

      tuiDo ((TuiOpaqueFuncPtr) tui_vUpdateSourceWindowsWithLine, s, line);
      tuiDo ((TuiOpaqueFuncPtr) tui_vUpdateLocatorFilename, s->filename);
    }
#else
    print_source_lines_base (s, line, stopline, noerror);
#endif
}


static int
is_identical_line_spec (struct symtabs_and_lines *sals)
{
  int i;

  for (i = 0; i < sals->nelts - 1; ++i)
    {
      if (sals->sals[i].line != sals->sals[i+1].line)
        return 0;
      if (strcmp (sals->sals[i].symtab->filename,
                  sals->sals[i+1].symtab->filename))
        return 0;
    }
  return 1;
}

/* Print a list of files and line numbers which a user may choose from
   in order to list a function which was specified ambiguously (as with
   `list classname::overloadedfuncname', for example).  The vector in
   SALS provides the filenames and line numbers.  */

static void
ambiguous_line_spec (struct symtabs_and_lines *sals)
{
  int i;

  for (i = 0; i < sals->nelts; ++i)
    printf_filtered ("file: \"%s\", line number: %d\n",
		     sals->sals[i].symtab->filename, sals->sals[i].line);
}

static int
browse (char * object)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  char * data;

  data = object;
  sals = decode_line_1 (&data, 0, 0, 0, 0, USER_CHOICE);

  if (sals.nelts == 1)
    {
      sal = sals.sals[0];
      if (sal.symtab)
        {
	  nimbus_current_pc = sal.pc;
	  if (!assembly_level_debugging)
	    {
              printf_unfiltered ("\033\032\032:se stl=%s\n:e +%d "
				 "%s\n:se nu\n\032\032A",
				 wdb_status_line (sal), sal.line,
				 sal.symtab -> fullname ? 
				 sal.symtab -> fullname :
				 sal.symtab -> filename);
	    }
	  else 
	    {
	      /* We're in Nimbus Disassembly Mode.  Generate a temporary
		 file name to send to vim to use to store the assembly code
		 for this debugging session.  Call generate_disassembly_file
		 to fill the file with the apropriate assembly code and then
		 tell vim to look at it.  The temporary file will be named as
		 follows: 

		 /tmp/nimbus.<pid>/<low>-<high>

		 where - pid = nimbus process id, low = lowest address
		 to display, and high = highest address to display.  */
		 
	      FILE *disassem_temp_fstream;
              char disassem_temp_file[80];
	      char gdb_pc[32];
              CORE_ADDR low, high;
  	      char *name;

  	      if (find_pc_partial_function (nimbus_current_pc,
					    &name, &low, &high) == 0)
		error ("No function contains specified address.\n");

#ifdef HP_IA64
  	      /* IA64: Instruction addresses are slot encoded. */
  	      /* Bundle-align LOW and HIGH */
  	      low &= ~(0xF);
  	      high &= ~(0xF);
#endif /*HP_IA64*/

	      sprintf(disassem_temp_file, "/tmp/nimbus.%d/%llx-%llx",
		      getpid(), low, high);
	      
	      if ( (disassem_temp_fstream = 
		    fopen (disassem_temp_file, "r")) == NULL)
		{
		  generate_disassembly_file (disassem_temp_file, low, high);
		}
	      if (disassem_temp_fstream != NULL)
	        fclose (disassem_temp_fstream);
	      
	      strcat_address_numeric (nimbus_current_pc, 1, gdb_pc, 32);

	      /* Finished writing to assembly file - now tell vim to look
		 at it */

              printf_unfiltered ("\033\032\032:se stl=%s\n:e %s\n:/%s "
				"<\n:se nonumber\n\032\032A",
				wdb_status_line(sal), 
				disassem_temp_file, gdb_pc);
	    }
          gdb_flush (gdb_stdout);
          current_source_symtab = sal.symtab;
          current_source_line = sal.line;
          return 1;
        }
    }

  return 0;
}

static void
browse_command (char *arg, int from_tty)
{
  if (!browse (arg))
    error ("Don't know how to browse this object\n");
}


static void
list_command (char *arg, int from_tty)
{
  struct symtabs_and_lines sals, sals_end;
  struct symtab_and_line sal = {0}; /* initialize for compiler warning */
  struct symtab_and_line sal_end = {0};  /* initialize for compiler warning */
  struct symbol *sym;
  char *arg1,*arg2;
  int no_end = 1;
  int dummy_end = 0;
  int dummy_beg = 0;
  int linenum_beg = 0;
  char *p, *copy;
  char *func = 0;

  if (!have_full_symbols () && !have_partial_symbols ())
    if (dbx_commands)
      error ("No symbol table is loaded.  Use the \"symbol-file\" command.");
    else
      error ("No symbol table is loaded.  Use the \"file\" command.");

  /* Pull in a current source symtab if necessary */
  if (current_source_symtab == 0 &&
      (arg == 0 || arg[0] == '+' || arg[0] == '-'))
    select_source_symtab (0);

  /* "l" or "l +" lists next ten lines.  */

  if (arg == 0 || STREQ (arg, "+"))
    {
      if (current_source_symtab == 0)
	error ("No default source file yet.  Do \"help list\".");
      print_source_lines (current_source_symtab, current_source_line,
			  current_source_line + lines_to_list, 0);
      return;
    }

  /* "l -" lists previous ten lines, the ones before the ten just listed.  */
  if (STREQ (arg, "-"))
    {
      if (current_source_symtab == 0)
	error ("No default source file yet.  Do \"help list\".");
      print_source_lines (current_source_symtab,
			  max (first_line_listed - lines_to_list, 1),
			  first_line_listed, 0);
      return;
    }

 /* For Nimbus handle only simple cases ... Fall thorugh for others
    This should cover the common cases of list function,
    list file:num, list *pc etc.
 */
 if (nimbus_version && browse(arg))
   return;

  /* Now if there is only one argument, decode it in SAL
     and set NO_END.
     If there are two arguments, decode them in SAL and SAL_END
     and clear NO_END; however, if one of the arguments is blank,
     set DUMMY_BEG or DUMMY_END to record that fact.  */

  arg2 = arg1 = arg;
  if (*arg1 == ',')
    dummy_beg = 1;
  else
    {
      sals = decode_line_1 (&arg1, 0, 0, 0, 0, USER_CHOICE);

      if (!sals.nelts)
        {
          printf_filtered("Location not found\n");
          return; 
        }

      /* srikanth, multiple template instantiations result in the same
         file:line showing up more than once and we error out. See if
         there really is ambiguity.
      */
      if (sals.nelts > 1 && !is_identical_line_spec (&sals))
	{
	  ambiguous_line_spec (&sals);
	  free (sals.sals);
	  return;
	}

      sal = sals.sals[0];
      free (sals.sals);
    }

 /* JAGad01561 - For xfail,"list filename:function" Wrong filename rejected */
 
 /* Extract filename from FILENAME:FUNC or FILENAME:LINENO*/
 
     copy = (char *) calloc (1, sizeof(char));

     for (p = arg ;*p ; p++)
       {
         if ( p[0] == ':')
           { 
	     free(copy);
             copy = (char *) xmalloc (p - arg + 1);
             memcpy (copy, arg, p - arg);
             copy[p - arg] = 0;
             p = p+1;
             func = arg = p;
             break;
           } 
        }
   
  /* Record whether the BEG arg is all digits.  */

  for (p = arg; p != arg1 && *p >= '0' && *p <= '9'; p++);
  linenum_beg = (p == arg1);

  arg = arg2;
  while (*arg1 == ' ' || *arg1 == '\t')
    arg1++;
  if (*arg1 == ',')
    {
      no_end = 0;
      arg1++;
      while (*arg1 == ' ' || *arg1 == '\t')
	arg1++;
      if (*arg1 == 0)
	dummy_end = 1;
      else
	{
	  if (dummy_beg)
            sals_end = decode_line_1 (&arg1, 0, 0, 0, 0, USER_CHOICE);
	  else
            sals_end = decode_line_1 (&arg1, 0, sal.symtab, sal.line, 0,
                                      USER_CHOICE);
	  if (sals_end.nelts == 0)
            {
              printf_filtered("Location not found\n");
              return;
            }
	  if (sals_end.nelts > 1)
	    {
	      ambiguous_line_spec (&sals_end);
	      free (sals_end.sals);
	      return;
	    }
	  sal_end = sals_end.sals[0];
	  free (sals_end.sals);
	}
    }

  if (*arg1)
    error ("Junk at end of line specification.");

  if (!no_end && !dummy_beg && !dummy_end
      && sal.symtab != sal_end.symtab)
    error ("Specified start and end are in different files.");
  if (dummy_beg && dummy_end)
    error ("Two empty args do not say what lines to list.");

  /* if line was specified by address,
     first print exactly which line, and which file.
     In this case, sal.symtab == 0 means address is outside
     of all known source files, not that user failed to give a filename.  */
  if (*arg == '*')
    {
      if (sal.symtab == 0)
	error ("No source file for address %s.",
               longest_local_hex_string((LONGEST) sal.pc));
      sym = find_pc_function (sal.pc);
      if (sym)
	{
	  print_address_numeric (sal.pc, 1, gdb_stdout);
	  printf_filtered (" is in ");
	  fputs_filtered (SYMBOL_SOURCE_NAME (sym), gdb_stdout);
	  printf_filtered (" (%s:%d).\n", sal.symtab->filename, sal.line);
	}
      else
	{
	  print_address_numeric (sal.pc, 1, gdb_stdout);
	  printf_filtered (" is at %s:%d.\n",
			   sal.symtab->filename, sal.line);
	}
    }

  /* If line was not specified by just a line number,
     and it does not imply a symtab, it must be an undebuggable symbol
     which means no source code.  */

  if (!linenum_beg && sal.symtab == 0)
    error ("No line number known for %s.", arg);

  /* If this command is repeated with RET,
     turn it into the no-arg variant.  */

  if (from_tty)
    *arg = 0;

  if (dummy_beg && sal_end.symtab == 0)
    error ("No default source file yet.  Do \"help list\".");
  if (dummy_beg)
    print_source_lines (sal_end.symtab,
			max (sal_end.line - (lines_to_list - 1), 1),
			sal_end.line + 1, 0);
  else if (sal.symtab == 0)
    error ("No default source file yet.  Do \"help list\".");
  else if (no_end)
    {
      /* JAGad01561 - For xfail,"list filename:function" Wrong filename rejected */
      if ((linenum_beg == 0) && (copy[0]!=NULL)) 
        {
	/* JAGaf55835 - When absolute/relative pathname is used for compilation,
	   for command <list filename:funcname ,the warning is displayed even for 
	   the correct filename. */   
          if ((strcmp(copy, getbasename(sal.symtab->filename))) != 0)
	  {
	    warning("Function %s not defined in %s\n",func,copy);
	    printf_filtered("Displaying function %s from %s\n",func,sal.symtab->filename);
	  } 
	}
	free(copy);
      /* JAGaf55835 - <START> - When listsise is set as unlimited , For "list lineno"
         (where lineno is not in the file), the first_line is set as lineno.  */	
      int first_line;	
      if ( sal.symtab->nlines && (sal.line > sal.symtab->nlines))
        first_line = sal.line ;
      else  /* JAGaf55835 - <END> */	
        first_line = sal.line - lines_to_list / 2;
      if (first_line < 1) first_line = 1;

      int stop_line = first_line + lines_to_list;
      /* JAGad01561 - For xfail, list line 1 with unlimited listsize */ 
      if (stop_line <= 0) stop_line = first_line + sal.symtab->nlines;
      
      print_source_lines (sal.symtab, first_line, stop_line, 0);
    }
  else
    print_source_lines (sal.symtab, sal.line,
			(dummy_end
			 ? sal.line + lines_to_list
			 : sal_end.line + 1),
			0);
}

/* JAGaf91208 - gdb info line command not working properly.
   This routine will reset current_source_symtab. 
*/
static struct symtab *old_source_symtab = 0;

static void
restore_current_source_symtab (void *old_source_symtab)
{
  current_source_symtab = (struct symtab *) old_source_symtab;
}

/* Print info on range of pc's in a specified line.  */

void
line_info (char *arg, int from_tty)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  CORE_ADDR start_pc, end_pc;
  int i;
  struct cleanup *old_chain = NULL;

  /* Sunil S, 060218, The GUI is not supposed to use this variable 
     current_source_symtab in this function, so we are setting it to NULL 
     and restoring it at the end of this function. 
  */
  if (server_command)
    {
      old_source_symtab = current_source_symtab; 
      current_source_symtab = 0;
      old_chain = make_cleanup (restore_current_source_symtab, old_source_symtab);
    }

  INIT_SAL (&sal);		/* initialize to zeroes */

  if (arg == 0)
    {
      sal.symtab = current_source_symtab;
      sal.line = last_line_listed;
      sals.nelts = 1;
      sals.sals = (struct symtab_and_line *)
	xmalloc (sizeof (struct symtab_and_line));
      sals.sals[0] = sal;
    }
  else
    {
      sals = decode_line_spec_1 (arg, 0);
   /* JAGaf60778 - <info line _main_> fails for Fortran executable compiled
      with +objdebug. If its a doom executable , instead of <_main_>,search
      for the symbol <_start>.*/
      #ifndef HP_IA64
      if ((sals.nelts == 0) && doom_executable && (strcmp(arg,"_main_")==0))
        {
          strcpy(arg,"_start");
	  main_line = 1;
	  sals = decode_line_spec_1 (arg, 0);
	 }
      #endif	 
     /* JAGaf60778 - <END> */

      dont_repeat ();

      /* srikanth, 981001, CLLbs15582, decode_line_spec_1() used not to return 
       * if arg is bogus. However this has changed now. 
       */ 
      if (sals.nelts == 0)  
        error("Location not found.");
    }

  /* C++  More than one line may have been specified, as when the user
     specifies an overloaded function name. Print info on them all. */
  for (i = 0; i < sals.nelts; i++)
    {
      sal = sals.sals[i];

      if (sal.symtab == 0)
	{
	  printf_filtered ("No line number information available");
	  if (sal.pc != 0)
	    {
	      /* This is useful for "info line *0x7f34".  If we can't tell the
	         user about a source line, at least let them have the symbolic
	         address.  */
	      printf_filtered (" for address ");
	      wrap_here ("  ");
	      print_address (sal.pc, gdb_stdout);
	    }
	  else
	    printf_filtered (".");
	  printf_filtered ("\n");
	}
      else if (sal.line > 0
	       && find_line_pc_range (sal, &start_pc, &end_pc))
	{
	  if (start_pc == end_pc)
	    {
	      printf_filtered ("Line %d of \"%s\"",
			       sal.line, sal.symtab->filename);
	      wrap_here ("  ");
	      printf_filtered (" is at address ");
	      print_address (start_pc, gdb_stdout);
	      wrap_here ("  ");
	      printf_filtered (" but contains no code.\n");
	    }
	  else
	    {
	      printf_filtered ("Line %d of \"%s\"",
			       sal.line, sal.symtab->filename);
	      wrap_here ("  ");
	      printf_filtered (" starts at address ");
	      print_address (start_pc, gdb_stdout);
	      wrap_here ("  ");
	      printf_filtered (" and ends at ");
	      print_address (end_pc, gdb_stdout);
	      printf_filtered (".\n");
	    }

	  /* x/i should display this line's code.  */
	  set_next_address (start_pc);

	  /* Repeating "info line" should do the following line.  */
	  last_line_listed = sal.line + 1;

	  /* If this is the only line, show the source code.  If it could
	     not find the file, don't do anything special.  */
	  if (annotation_level && sals.nelts == 1)
	    identify_source_line (sal.symtab, sal.line, 0, start_pc);
	}
      else
	/* Is there any case in which we get here, and have an address
	   which the user would want to see?  If we have debugging symbols
	   and no line numbers?  */
	printf_filtered ("Line number %d is out of range for \"%s\".\n",
			 sal.line, sal.symtab->filename);
    }
  free (sals.sals);
  if (server_command)
    {
      do_cleanups (old_chain); 
    }
}

/* Commands to search the source file for a regexp.  */

/* ARGSUSED */
static void
forward_search_command (char *regex, int from_tty)
{
  register int c;
  register int desc;
  register FILE *stream;
  int line;
  char *msg;

#if defined(TUI)
  /* 
     ** If this is the TUI, search from the first line displayed in 
     ** the source window, otherwise, search from last_line_listed+1 
     ** in current_source_symtab 
   */
  if (!tui_version)
    line = last_line_listed;
  else
    {
      if (srcWin->generic.isVisible && srcWin->generic.contentSize > 0)
	line = ((TuiWinContent)(void *)
	 srcWin->generic.content)[0]->whichElement.source.lineOrAddr.lineNo;
      else
	{
	  printf_filtered ("No source displayed.\nExpression not found.\n");
	  return;
	}
    }
  line++;
#else
  line = last_line_listed + 1;
#endif

  msg = (char *) re_comp (regex);
  if (msg)
    error (msg);

  if (current_source_symtab == 0)
    select_source_symtab (0);

  desc = open_source_file (current_source_symtab);
  if (desc < 0)
    perror_with_name (current_source_symtab->filename);

  if (current_source_symtab->line_charpos == 0)
    find_source_lines (current_source_symtab, desc);

  if (line < 1 || line > current_source_symtab->nlines)
    {
      close (desc);
      error ("Expression not found");
    }

  if (lseek (desc, current_source_symtab->line_charpos[line - 1], 0) < 0)
    {
      close (desc);
      perror_with_name (current_source_symtab->filename);
    }

  stream = fdopen (desc, FDOPEN_MODE);
  clearerr (stream);
  while (1)
    {
      static char *buf = NULL;
      register char *p;
      int cursize, newsize;

      cursize = 256;
      buf = xmalloc (cursize);
      p = buf;

      c = getc (stream);
      if (c == EOF)
	break;
      do
	{
	  *p++ = c;
	  if (p - buf == cursize)
	    {
	      newsize = cursize + cursize / 2;
	      buf = xrealloc (buf, newsize);
	      p = buf + cursize;
	      cursize = newsize;
	    }
	}
      while (c != '\n' && (c = getc (stream)) >= 0);

#ifdef CRLF_SOURCE_FILES
      /* Remove the \r, if any, at the end of the line, otherwise
         regular expressions that end with $ or \n won't work.  */
      if (p - buf > 1 && p[-2] == '\r')
	{
	  p--;
	  p[-1] = '\n';
	}
#endif

      /* we now have a source line in buf, null terminate and match */
      *p = 0;
      if (re_exec (buf) > 0)
	{
	  /* Match! */
	  fclose (stream);
	  if (tui_version)
	    print_source_lines_base (current_source_symtab, line, line + 1, 0);
	  print_source_lines (current_source_symtab, line, line + 1, 0);
	  set_internalvar (lookup_internalvar ("_"),
			   value_from_longest (builtin_type_int,
					       (LONGEST) line));
	  current_source_line = max (line - lines_to_list / 2, 1);
	  return;
	}
      line++;
    }

  printf_filtered ("Expression not found\n");
  fclose (stream);
}

/* ARGSUSED */
static void
reverse_search_command (char *regex, int from_tty)
{
  register int c;
  register int desc;
  register FILE *stream;
  int line;
  char *msg;
#if defined(TUI)
  /*
     ** If this is the TUI, search from the first line displayed in
     ** the source window, otherwise, search from last_line_listed-1
     ** in current_source_symtab
   */
  if (!tui_version)
    line = last_line_listed;
  else
    {
      if (srcWin->generic.isVisible && srcWin->generic.contentSize > 0)
	line = ((TuiWinContent)(void *)
	 srcWin->generic.content)[0]->whichElement.source.lineOrAddr.lineNo;
      else
	{
	  printf_filtered ("No source displayed.\nExpression not found.\n");
	  return;
	}
    }
  line--;
#else
  line = last_line_listed - 1;
#endif

  msg = (char *) re_comp (regex);
  if (msg)
    error (msg);

  if (current_source_symtab == 0)
    select_source_symtab (0);

  desc = open_source_file (current_source_symtab);
  if (desc < 0)
    perror_with_name (current_source_symtab->filename);

  if (current_source_symtab->line_charpos == 0)
    find_source_lines (current_source_symtab, desc);

  if (line < 1 || line > current_source_symtab->nlines)
    {
      close (desc);
      error ("Expression not found");
    }

  if (lseek (desc, current_source_symtab->line_charpos[line - 1], 0) < 0)
    {
      close (desc);
      perror_with_name (current_source_symtab->filename);
    }

  stream = fdopen (desc, FDOPEN_MODE);
  clearerr (stream);
  while (line > 1)
    {
/* FIXME!!!  We walk right off the end of buf if we get a long line!!! */
      char buf[4096];		/* Should be reasonable??? */
      register char *p = buf;

      c = getc (stream);
      if (c == EOF)
	break;
      do
	{
	  *p++ = c;
	}
      while (c != '\n' && (c = getc (stream)) >= 0);

#ifdef CRLF_SOURCE_FILES
      /* Remove the \r, if any, at the end of the line, otherwise
         regular expressions that end with $ or \n won't work.  */
      if (p - buf > 1 && p[-2] == '\r')
	{
	  p--;
	  p[-1] = '\n';
	}
#endif

      /* We now have a source line in buf; null terminate and match.  */
      *p = 0;
      if (re_exec (buf) > 0)
	{
	  /* Match! */
	  fclose (stream);
	  if (tui_version)
	    print_source_lines_base (current_source_symtab, line, line + 1, 0);
	  print_source_lines (current_source_symtab, line, line + 1, 0);
	  set_internalvar (lookup_internalvar ("_"),
			   value_from_longest (builtin_type_int,
					       (LONGEST) line));
	  current_source_line = max (line - lines_to_list / 2, 1);
	  return;
	}
      line--;
      if (fseek (stream, current_source_symtab->line_charpos[line - 1], 0) < 0)
	{
	  fclose (stream);
	  perror_with_name (current_source_symtab->filename);
	}
    }

  printf_filtered ("Expression not found\n");
  fclose (stream);
  return;
}

char * nimbus_path;

static void
faq_command (char *arg, int from_tty)
{
    char faq [1024];
    if (!nimbus_version)
      error ("Command available only when run under Nimbus!\n");

    if (!nimbus_path)
      {
          nimbus_path = getenv ("VDB_PATH");
          if (!nimbus_path)
            error ("VDB_PATH undefined : don't know where the faq is !\n");
          else 
            nimbus_path = strdup (nimbus_path);
      }

    sprintf (faq, "%s/faq", nimbus_path);
    if (fork () == 0)
        execlp ("/usr/bin/X11/hpterm", "hpterm", "-e", "/usr/bin/vi", faq, 0);
}

static void
log_command (char *arg, int from_tty)
{
    if (!arg || arg[0] == 0)
      error ("Argument missing.");

    printf_unfiltered ("\033\032\032:se nofs\n:w! %s\n:se fs\n\032\032\027jA",
                                                                arg);
    gdb_flush (gdb_stdout);
}


static void
man_command (char *arg,
             int from_tty)
{
    char command[1024];
    char * file;

    if (arg == 0 || arg[0] == 0)
      error ("Usage : man function.");

    file = tmpnam (0);
    if (!file)
      error ("Error creating temporary file.");

    sprintf (command, "/usr/bin/man %s 2>/dev/null | /usr/bin/col -b > %s", arg, file);
    system (command);
    if (fork () == 0)
        execlp ("/usr/bin/X11/hpterm", "hpterm", "-e", "/usr/bin/vi", file, 0);
}

void * file_to_list;
int set_source_file_command ( char *args,
                              int from_tty,
                              struct cmd_list_element *c )
{
  if (file_to_list)
    current_source_symtab = lookup_symtab (file_to_list);
  else 
    current_source_symtab = 0;
  
  current_source_line = 1;
  return 0;  /* return value not used */
}

void
_initialize_source ()
{
  typedef void (*sfunc_t) (char *, int, struct cmd_list_element *);
  struct cmd_list_element *c;
  struct cmd_list_element *set;
  current_source_symtab = 0;
  init_source_path ();
  init_object_path ();

  /* The intention is to use POSIX Basic Regular Expressions.
     Always use the GNU regex routine for consistency across all hosts.
     Our current GNU regex.c does not have all the POSIX features, so this is
     just an approximation.  */
  re_set_syntax (RE_SYNTAX_GREP);

  c = add_cmd ("objectdir", class_files, objectdir_command,
           "Add directory DIR to beginning of search path for object files.\n\n\
Usage:\n\tobjectdir [<DIR 1>] [[:]<DIR 2>] [...]\n\n\
DIR can also be $cwd for the current working directory, or $cdir for the\n\
directory in which the source file was compiled into object code.\n\
With no argument, reset the search path to $cdir:$cwd, the default.",
               &cmdlist);

#ifdef PATHMAP
   c = add_cmd ("pathmap", class_files, pathmap_command,
"pathmap allows the user to define mappings of paths so as to locate object\n\
and source files easily.\n\n\
Usage:\n\tpathmap <DIR1> <DIR2>\n\n\
pathmap takes two arguments both of which are path strings or substrings.\n\
The first one specifies what path string or substring should be mapped\n\
using the second path string or substring.  For example,\n\
\n\
	(gdb) pathmap /path_a/mydir /path_b/objdir\n\
\n\
will replace all occurrences of /path_a/mydir with /path_b/objdir when\n\
searching for object and source files.\n\
\n\
Specifying more than one pathmap will result in all pathmaps being used to\n\
search for the desired file.  The order of searching will be based on order\n\
in which the pathmaps are specified, i.e., the first pathmap specified will\n\
be used first for searching, then the second and so on. The first match will\n\
be used and the remaining mappings will be ignored. Not specifying arguments\n\
in a pathmap command will result in all earlier pathmaps being erased.\n\
\n\
pathmap is meant to replace the potentially tedious use of objectdir and dir\n\
commands; hence should be used alone.  If pathmap is used in conjunction with\n\
either objectdir or dir commands, it will be lower in the order of precedence,\n\
i.e., all objectdir and dir commands will be used to locate the file, if\n\
they aren't found then pathmap will be used.  pathmap will not apply mapping\n\
to directories specified by objectdir or dir commands.  In general, avoid\n\
mixing pathmap, dir and objectdir.\n", &cmdlist);

/* JAGaf32389 */
  add_cmd ("pathmaps", no_class, show_pathmap,
	   "Current pathmaps for finding object and source files.\n\n\
Usage:\n\tshow pathmaps\n\n\
\"From:\" specifies what path string/substring should be mapped.\n\
\"To:\" specifies the path string/substring to be used for mapping.",
	   &showlist);

#endif /* PATHMAP */

 c->completer = filename_completer;

  if (nimbus_version)
   {
     c = add_cmd ("faq", class_files, faq_command,
          "Frequently asked question about Nimbus & Wildebeest and answers.",
              &cmdlist);
   }

   if (nimbus_version)
     {
       c = add_cmd ("man", class_files, man_command,
            "View manual page.",
                &cmdlist);
     } 

  if (nimbus_version && !firebolt_version)
    {
      c = add_cmd ("layout", class_files, layout_command,
		   "Toggle between source files and disassembly files.",
		   &cmdlist);
    }

  if (nimbus_version)
    {
      c = add_cmd ("log", class_files, log_command,
           "Log debugger output to file specified",
               &cmdlist);
    }

  c = add_cmd ("directory", class_files, directory_command,
	       "Add directory DIR to beginning of search path for source files.\n\n\
Usage:\n\tdirectory [<DIR>]\n\n\
Forget cached info on source file locations and line positions.\n\
DIR can also be $cwd for the current working directory, or $cdir for the\n\
directory in which the source file was compiled into object code.\n\
With no argument, reset the search path to $cdir:$cwd, the default.",
	       &cmdlist);

  if (dbx_commands)
    add_com_alias ("use", "directory", class_files, 0);

  c->completer = filename_completer;

  add_cmd ("directories", no_class, show_directories,
	   "Current search path for finding source files.\n\nUsage:\n\tshow \
directories\n\n$cwd in the path means the current working directory.\n\
$cdir in the path means the compilation directory of the source file.\n",
	   &showlist);

  add_cmd ("objectdir", no_class, show_object_directories,
           "Current search path for finding object files.\n\nUsage:\n\tshow \
objectdir\n\n$cwd in the path means the current working directory.\n\
$cdir in the path means the compilation directory of the source file.\n",
           &showlist);

  if (xdb_commands)
    {
      add_com_alias ("D", "directory", class_files, 0);
      add_cmd ("ld", no_class, show_directories,
	       "Current search path for finding source files.\n\
$cwd in the path means the current working directory.\n\
$cdir in the path means the compilation directory of the source file.",
	       &cmdlist);
    }

  add_info ("source", source_info,
	    "Information about the current source file.\n\nUsage:\n\tinfo source\n");

  add_info ("line", line_info,
	    concat ("Core addresses of the code for a source line.\n\n\
Usage:\n\tinfo line [[<FILE>:]<LINE> | [<FILE>:]FUNC]\n\n\
Line can be specified as\n\
  LINE, to list around that line in current file,\n\
  FILE:LINE, to list around that line in that file,\n\
  FUNC, to list around beginning of that function,\n\
  FILE:FUNC, to distinguish among like-named static functions.\n\
", "\
Default is to describe the last source line that was listed.\n\n\
This sets the default address for \"x\" to the line's first instruction\n\
so that \"x/i\" suffices to start examining the machine code.\n\
The address is also stored as the value of \"$_\".", NULL));

  add_com ("forward-search", class_files, forward_search_command,
	   "Search for regular expression (see regex(3)) from last line listed.\n\n\
Usage:\n\tforward-search [<REGEXP>]\n\t or\n\t\
search [<REGEXP>]\n\n\
The matching line number is also stored as the value of \"$_\".");
  add_com_alias ("search", "forward-search", class_files, 0);

  add_com ("reverse-search", class_files, reverse_search_command,
	   "Search backward for regular expression (see regex(3)) from last line listed.\n\n\
Usage:\n\treverse-search [<REGEXP>]\n\n\
The matching line number is also stored as the value of \"$_\".");

  if (xdb_commands)
    {
      add_com_alias ("/", "forward-search", class_files, 0);
      add_com_alias ("?", "reverse-search", class_files, 0);
    }

  add_com ("list", class_files, list_command,
	   concat ("List specified function or line or address.\n\n\
Usage:\nlist [[<FILE>:]<LINE>|[<FILE>:]<FUNC>|*<ADDR>] \
[,[<FILE>:]<LINE>|[<FILE>:]<FUNC>|*<ADDR>]\n or\nlist [+<OFFSET> | -<OFFSET>]\n\n\
With no argument, lists ten more lines after or around previous listing.\n\
\"list -\" lists the ten lines before a previous ten-line listing.\n\
One argument specifies a line, and ten lines are listed around that line.\n\
Two arguments with comma between specify starting and ending lines to list.\n\
", "\
Lines can be specified in these ways:\n\
  LINE, to list around that line in current file,\n\
  FILE:LINE, to list around that line in that file,\n\
  FUNC, to list around beginning of that function,\n\
  FILE:FUNC, to distinguish among like-named static functions.\n\
  *ADDR, to list around the line containing that address.\n\
With two args if one is empty it stands for ten lines away from the other arg.", NULL));

  if (!xdb_commands)
    add_com_alias ("l", "list", class_files, 1);
  else
    add_com_alias ("v", "list", class_files, 1);

  if (dbx_commands)
    add_com_alias ("file", "list", class_files, 1);

  /* JAGad01561 - For xfail , list line 1 with unlimited listsize */
  
  add_show_from_set
    (add_set_cmd ("listsize", class_support, /* var_uinteger */ var_integer,
		  (char *) &lines_to_list,
		  "Set number of source lines gdb will list by default.\n\n\
Usage:\nTo set new value:\n\tset listsize <INTEGER>\nTo see current value:\
\n\tshow listsize\n", &setlist), &showlist);

  /* JAGaf49959 - Sunil S 13/03/2006 To display the absolute path name of the source file. */
  
  add_show_from_set
    (add_set_cmd ("display-full-path", class_support, /* var_uinteger */ var_integer,
		  (char *) &display_full_path,
		  "Set to display the absolute source pathname of the source file.\n\n\
Usage:\nTo set new value:\n\tset display-full-path <INTEGER>\nTo see current value:\
\n\tshow display-full-path\n\n", &setlist), &showlist);

  if (nimbus_version)
    {
      set = add_set_cmd ("source", class_support, var_string_noescape,
		  (char *) &file_to_list,
		  "Set the current source file.",
		  &setlist);
      (void) add_show_from_set (set, &showlist);
      set->function.sfunc = (void (*) (char *, 
				       int , 
				       struct cmd_list_element *))
			      set_source_file_command;
    }
  add_com ("edit", class_support, edit_command, 
           "Run the editor (specified by $EDITOR) on the current source file.\n\n\
Usage:\n\tedit\n");
  add_com ("browse", class_support, browse_command, 
           "List if argument is a procedure. Ptype otherwise.");
  add_com ("browse", class_support, browse_command,
           "List if argument is a procedure. Ptype otherwise.\n\n\
Usage:\n\tbrowse <ARG>\n");
}

/* 
 * edit_command
 * srikanth, 980830 CLLbs15608 Loads an editor specified by $EDITOR on 
 * the current source file : a marriage of shell_escape() function in 
 * command.c and source_info() in source.c; Uses fork and execl to load
 * the editor so that the debugger does not have to stop. Naturally only 
 * guaranteed to be available on unix systems. 
 *
 * Allocates memory indirectly thro strdup() which is not freed at all.
 * This is done so that we do no not have to consult the environment each 
 * time the function is called.
 */

# define CMD_BUF_SIZE 1024

/* ARGSUSED */
static void
edit_command (char *arg, int from_tty)
{
  register struct symtab *s = NULL;
  char buffer[CMD_BUF_SIZE + 1];
  int pid;
  char *p, *value, *tok, *end_tok, *source;
  int toklen;
  static char *shell = NULL;
  static char *term = NULL;
  static char *editor = NULL;
  static boolean env_consulted = false;
  int i;
  struct partial_symtab *ps;

#ifdef CANT_FORK
  printf_filtered("This command is not supported on this platform.\n");
  return;
#else /* Can fork.  */

#ifdef LOG_BETA_EDIT_COMMAND
  log_test_event (LOG_BETA_EDIT_COMMAND, 1);
#endif

  if (! arg && ! current_source_symtab)
    {
      printf_filtered("No current source file.\n");
      return;
    }
  
  if (!env_consulted)
    {
      env_consulted = true;

      /* check user preference for EDITOR (default vi) */ 

      if ((value = (char *) getenv ("EDITOR")) == NULL)
        value = "vi";  
      editor = strdup(value);

      /* check user preference for SHELL (default sh) */ 

      if ((value = (char *) getenv ("SHELL")) == NULL)
        value = "/bin/sh";
      shell = strdup(value);

      /* check user preference for TERM (default xterm) */ 

      if ((value = (char *) getenv ("TERM")) == NULL)
        value = "xterm";  /* this may be the commonest after all */

      term = strdup(value);

      /* force term type to be lower case */  

      for (i = 0; term[i]; i++)
        term[i] = tolower(term[i]);
    }

  strncpy(buffer,term, CMD_BUF_SIZE);
  strncat(buffer, " -e ", CMD_BUF_SIZE);
  strncat(buffer, editor, CMD_BUF_SIZE);

  if (! arg)
    {
      strncat(buffer, " ", CMD_BUF_SIZE);
      if (current_source_symtab->fullname)
	strncat (buffer, current_source_symtab->fullname, CMD_BUF_SIZE);
      else 
	strncat (buffer, current_source_symtab->filename, CMD_BUF_SIZE);
    }
  else
    {
      tok = end_tok = arg;
      while (*end_tok != '\0')
        {
	  while (*tok == ' ' || *tok == '\t')
	    tok++;
	  end_tok = tok;
	  while (*end_tok != ' ' && *end_tok != '\0' && *end_tok != '\t')
	    end_tok++;
	  toklen = (int)(end_tok - tok);
	  if (toklen >= 1)
	    {
	      source = (char *) alloca (toklen + 1);
	      strncpy (source, tok, toklen + 1);
	      source[toklen] = '\0';
	      tok = end_tok;

	      ALL_OBJFILE_PSYMTABS (symfile_objfile, ps)
		if (!strcmp (source, ps->filename))
		  {
		    s = ps->symtab;
		    break;
		  }

	      strncat (buffer, " ", CMD_BUF_SIZE);
	      if (!s && ps)
		strncat (buffer, ps->filename, CMD_BUF_SIZE);
	      else if (s->fullname)
		strncat (buffer, s->fullname, CMD_BUF_SIZE);
	      else 
		strncat (buffer, s->filename, CMD_BUF_SIZE);
	    }
	}
    }

  /* Get the name of the shell for arg0 */
  if ((p = strrchr (shell, '/')) == NULL)
    p = shell;
  else
    p++;                        /* Get past '/' */

  if ((pid = fork()) == 0)
    {
      execl (shell, p, "-c", buffer, 0);

      /* should not get here unless something is wrong */
      fprintf_unfiltered (gdb_stderr, "Cannot execute %s: %s\n", shell,
                          safe_strerror (errno));
      gdb_flush (gdb_stderr);
      _exit (0177);
    }

  if (pid == -1)
    error ("Fork failed");
#endif /* Can fork.  */
}
# undef CMD_BUF_SIZE

/* Return the fully qualified path name of the source file associated with 
   psymtab, using the psymtab's dirname or compile map information, if 
   available, or if not available, the source path, to construct the path name.
   Return NULL if no psymtab.
 */

char *
psymtab_to_filename (struct partial_symtab *pst)
{
  char * pst_full_pathname = 0;
  char * full_pathname = 0;

  if (!pst)
    return NULL;

  if (pst->dirname)
    {
      pst_full_pathname = xmalloc (strlen (pst->dirname)
                                   + strlen (pst->filename) + 2);
      strcpy (pst_full_pathname, pst->dirname);
      strcat (pst_full_pathname, "/");
      strcat (pst_full_pathname, pst->filename);
    }
#ifdef GET_CDIR_FILENAME
  /* Use the compile map info to construct the full pathname */
  else if (pst_full_pathname = GET_CDIR_FILENAME (pst))
    ;
#endif
  else if (!source_full_path_of (pst->filename, &pst_full_pathname))
    pst_full_pathname = strdup(pst->filename);

  full_pathname =  pathopt (pst_full_pathname);
  free (pst_full_pathname);
  return full_pathname;
}


/*****************************************
** STATIC LOCAL FUNCTIONS FORWARD DECLS    **
******************************************/

static FILE *fstream = NULL;
static int desc = -1;

static void
close_source_file (int unused)
{
  if (desc >= 0)
    close (desc);
  desc = -1;
  if (fstream)
    fclose (fstream);
  fstream = NULL;
}

/* ARGSUSED */
/* Nimbus - assembly level debugging support
   Switch back and forth between source and assembly level debugging mode
   when the user pushes the disassembly button in the hpterm window and
   passes a "src" or "asm" string as an argument to this function.  

   In each case, the assembly_level_debugging flag is set, and 2 messages
   are sent to vim.  The first tells vim which mode we are changing to and
   the second gives filename and line number (in source mode) or address
   info (in assembly mode).  

   The code for accomplishing the above to tasks when switching to source
   mode is located here.  The equivalent code for switching to assembly
   mode is mainly located in switch_to_assembly_mode (). */

static void
layout_command (char *arg, int from_tty)
{
  if (!strcmp (arg, "asm"))
    {
      CORE_ADDR low, high;
      char *name;

      if (find_pc_partial_function (nimbus_current_pc, 
				    &name, &low, &high) == 0)
	error ("No function contains specified address.\n");

      switch_to_assembly_mode (low, high);
    }
  else if (!strcmp (arg, "src")) 
    {
      struct symtab_and_line sal = {0};
      struct symtab *current_symtab = NULL;
      char *current_filename = "";

      if (assembly_level_debugging)
	{
          assembly_level_debugging = 0;
          printf_unfiltered ("\033\032\032:se mode=src\n\032\032A");
          gdb_flush (gdb_stdout);
	}

      if (nimbus_current_pc != (CORE_ADDR) 0)
	{
	  sal = find_pc_line (nimbus_current_pc, 0);
	  current_symtab = sal.symtab;
	}

      if (current_symtab != NULL)
        {
	  current_filename = (current_symtab && current_symtab -> fullname) ?
	    current_symtab -> fullname :
	    (current_symtab && current_symtab -> filename) ?
	    current_symtab -> filename : NULL;
        }

      printf_unfiltered ("\033\032\032:e +%d %s\n:se nu\n\032\032A", 
			 sal.line, current_filename);
      gdb_flush (gdb_stdout);
    }
  else
    {
      printf_unfiltered ("Warning: Invalid layout specified.\n"
			 "Usage: layout asm | src\n");
    }

  return;
}

/*****************************************
** PUBLIC FUNCTIONS                        **
******************************************/

/* VDB assembly and source mode debugging are accomplished by the following

   1.  gdb specifies a file to vim along with some other info.  
   2.  vim displays the file on the screen, obeying any other special options
       sent by gdb.  

   In source level debugging, this is easy.  Just send the source file
   name to vim and tell it to display the file in line number mode.  
   This is slightly more difficult in assembly mode.  GDB must generate a 
   temporary file for vim and fill it with assembly code and address info.

   This function generates the disassembly code for the address range between
   startAddr, and endAddr and stores it in the file specified by
   disassem_temp_file.  disassem_temp_file is currently named and created by
   the switch_to_assembly_mode function.  The absolute filename looks like
   this: 

   /tmp/Nimbus.<pid>/<startAddr>-<endAddr>

   Where pid = process id for this nimbus and startAddr and endAddr are the
   low and high address range - usually for a given function */

void 
generate_disassembly_file (char *disassem_temp_file, CORE_ADDR startAddr,
			   CORE_ADDR endAddr)
{
  static int nimbus_tempfile_index = 0;
  struct ui_file *gdb_dis_out;
  int last_line = -1;
  char *last_file = NULL;
  struct symtab_and_line prev_sal = {0};
  struct symtab_and_line sal = {0};
  struct cleanup *old_chain;
  char buf_line[1024];
  CORE_ADDR pc;
  disassemble_info asmInfo;

  if (startAddr == (CORE_ADDR) 0)
    {
      fprintf_unfiltered (gdb_stderr, "No symbol file specified.\n"
			  "Use the \"file\" or \"symbol-file\" command.");
      return;
    }

  if ( (gdb_dis_out = gdb_fopen (disassem_temp_file, "w")) == NULL)
    perror_with_name (disassem_temp_file);
  old_chain = make_cleanup ((make_cleanup_ftype *) ui_file_delete, gdb_dis_out);

  /* Prevent nimbus from managing too many temporary disassembly files at 
     once.  I'm not sure why this is necessary */
  if (nimbus_temp_files[nimbus_tempfile_index] == NULL)
    {
      nimbus_temp_files[nimbus_tempfile_index] = strdup (disassem_temp_file);
      nimbus_tempfile_index = (nimbus_tempfile_index + 1) % MAX_TEMP_FILES;
    }
  else
    { /* array is full */
      int i;

      for (i = 0; i < MAX_TEMP_FILES; i++)
	{
	  if (!unlink (nimbus_temp_files[nimbus_tempfile_index]))
	    {
	      free (nimbus_temp_files[nimbus_tempfile_index]);
	      nimbus_temp_files[nimbus_tempfile_index] = 
		strdup (disassem_temp_file);
	      nimbus_tempfile_index = 
		(nimbus_tempfile_index + 1) % MAX_TEMP_FILES;
	      break;
	    }
	  else 
	    { /* remove temp file failed, try next one */
	      nimbus_tempfile_index = 
		(nimbus_tempfile_index + 1) % MAX_TEMP_FILES;
	    }
	}

      if (i == MAX_TEMP_FILES) {
	printf_unfiltered("Error: too many disassembly files generated "
			  "and failed to remove any of them.\n");
	return;
      }
    }

  INIT_DISASSEMBLE_INFO_NO_ARCH (asmInfo, gdb_dis_out, 
				 (fprintf_ftype) fprintf_filtered);
  asmInfo.read_memory_func = dis_asm_read_memory;
  asmInfo.memory_error_func = dis_asm_memory_error;

  /* RM: Set last_line if not at the beginning of a line. Ugh gross! */
  if (startAddr-16 > 0)
    prev_sal = find_pc_line (startAddr - 16, 0);
  sal = find_pc_line (startAddr, 0);
  if ( (sal.line != 0) && (sal.line == prev_sal.line))
    last_line = sal.line;

  make_cleanup ((make_cleanup_ftype *) close_source_file, 0);

  /* Now construct each line */
  for (pc = (CORE_ADDR) startAddr; pc < endAddr;)
    {
      /* RM: try to print source line in disassembly display */
      sal = find_pc_line (pc, 0);

      /* Print each new source line. */
      if (sal.line != last_line)
        {
	  last_line = sal.line;
	  if (last_line != 0)
            {
	      if (!sal.symtab || strcmp (sal.symtab->filename, last_file))
                {
		  if (sal.symtab)
                    {
		      last_file = sal.symtab->filename;
		      if ((desc >= 0) && (fstream))
                        {
			  /* close both file number "desc" and 
			     file pointer "fstream". */
			  fclose (fstream);
			  fstream = NULL;
                        }
		      desc = open_source_file (sal.symtab);
		      if (desc >= 0)
                        {
			  fstream = fdopen (desc, FOPEN_RT);
			  clearerr (fstream);
			  if (sal.symtab->line_charpos == 0)
			    find_source_lines (sal.symtab, desc);
                        }
                    }
		  else
		    {
		      if ((desc >= 0) && (fstream))
                        {
			  /* close both file number "desc" and 
			     file pointer "fstream". */
			  fclose (fstream);
                        }
		      desc = -1;
		      fstream = NULL;
		    }
                }
	      /* Find the source line and print it. */
	      if (!fstream)
		sprintf (buf_line, ";;; Line: %6d", last_line);
	      else 
		{
		  if (last_line > 0 && last_line <= sal.symtab->nlines 
		      && sal.symtab->line_charpos 
		      /* we had skipped possible errors */
		      && (fseek (fstream, 
				 sal.symtab->line_charpos[last_line - 1], 
				 SEEK_SET) == 0))
		    {
		      /* yuck */
		      char line[1024];

		      strcpy (buf_line, ";;; ");
		      fgets (line, 1024 - 5, fstream);
		      /* If line > lineWidth - 5, then may have cut newline. */
		      if (line[strlen (line) - 1] != '\n')
			line[strlen (line) - 1] = '\n';
		      strcpy (buf_line + 4, line);
		    }
		}
              if (firebolt_version)
	        fprintf_unfiltered (gdb_dis_out, "\n%s\n", buf_line);
              else
	        fprintf_unfiltered (gdb_dis_out, "%s", buf_line);
	      continue;
            }
        }

      if (firebolt_version)
        print_address_numeric (pc, 1, gdb_dis_out);
      else 
        print_address (pc, gdb_dis_out);

      fprintf_unfiltered(gdb_dis_out, "\t");

      pc += ( (*tm_print_insn) (pc, &asmInfo));
      fprintf_unfiltered(gdb_dis_out, "\n");
      gdb_flush (gdb_dis_out);

        /* reset the buffer to empty */
      buf_line[0] = '\0';
    }
  do_cleanups (old_chain);
  return;
} /* generate_disassembly_file */

/* VDB assembly and source mode debugging are accomplished by the following

   1.  gdb specifies a file to vim along with some other info.  
   2.  vim displays the file on the screen, obeying any other special options
       sent by gdb.  

   In source level debugging, this is easy.  Just specify the source file
   name to vim and tell it to switch to line number mode.  This is slightly
   more difficult in assembly mode.  GDB must generate a temporary file for
   vim to look at and fill it with assembly code and address information.  

   This function handles the switch to assembly level debugging in VDB as
   follows:
   1.  sets assembly_level_debugging flag to notify gdb we're in assembly mode
   2.  sends messages to vim about the mode switch
   3.  creates a temporary file to store the assembly code
   4.  calls generate_disassembly_file to fill the file with assembly code
   5.  asks vim to display the temporary file without line number info 
       line numbers are for source mode only.  We use addresses in assembly
       mode.  */

void
#ifdef __STDC__
switch_to_assembly_mode (CORE_ADDR low, CORE_ADDR high)
#else
     switch_to_assembly_mode (low, high)
     CORE_ADDR low;
     CORE_ADDR high;
#endif
{
  FILE *disassem_temp_fstream;
  char disassem_temp_file[80];
  char gdb_pc[32];

  if (!assembly_level_debugging)
    {
      assembly_level_debugging = 1;
      printf_unfiltered ("\033\032\032:se mode=asm\n\032\032A");
      gdb_flush(gdb_stdout);

#ifdef HP_IA64
      /* Just in case the user wants to print changed register values */
      save_old_register_values();
#endif
    }

#ifdef HP_IA64
  /* IA64: Instruction addresses are slot encoded. */
  /* Bundle-align LOW and HIGH */
  low &= ~(0xF);
  high &= ~(0xF);
#endif /*HP_IA64*/

  sprintf(disassem_temp_file, "/tmp/nimbus.%d/%llx-%llx", getpid(), low, high);

  if ( (disassem_temp_fstream = 
	fopen (disassem_temp_file, "r")) == NULL)
    {
      generate_disassembly_file (disassem_temp_file, low, high);
    }
  if (disassem_temp_fstream != NULL)
    fclose (disassem_temp_fstream);

  strcat_address_numeric (nimbus_current_pc, 1, gdb_pc, 32);
  printf_unfiltered("\033\032\032:e %s\n:/%s <\n:se nonumber\n\032\032A",
		    disassem_temp_file, gdb_pc);
  gdb_flush(gdb_stdout);
}

/* Baskar, JAGae51552.
   source_full_path_of() searches only the current directory. It is of
   no use when trying to attach a running process (invoked ./foo) to 
   gdb invoked in another directory. We will not be able to find out the
   absolute pathname of the exec file using source_file_path_of.

   We have to also look into source_path, if openp fails to open the
   exec_file in cwd.
 
   This function searched current working directory first and if the file
   is not found there then the source_path will be searched.

   If the file was found, this function returns 1, and FULL_PATHNAME is
   set to the fully-qualified pathname.

   Else, this functions returns 0, and FULL_PATHNAME is set to NULL.
 */

int
absolute_path_of (char *exec_file, char **full_pathname)
{
  int fd;

  fd = openp (source_path, 1, exec_file, O_RDONLY, 0, full_pathname);
  if (fd < 0)
    {
      fd = openp (source_path, 0, exec_file, O_RDONLY, 0, full_pathname);
      if (fd < 0)
        {
          *full_pathname = NULL;
          return 0;
        }
    }

  close (fd);
  return 1;
}

/*----------------------------------------------------------------------------*/
#ifdef PATHMAP
/* Function:  pathmap_command()
 *
 * Description:  This function handles the actual pathmap command.  If no
 *	argumentes are specified, it prints the pathmap and then asks the
 *	user and then resets the pathmap to empty if desired.  If two 
 *	arguments, i.e, a from and a to, are specified, it adds them to
 *	the pathmap, else it complains to the user.
 *
 * Inputs:
 *	map_str:  Contains all the arguments in one string.
 *	tty_flag:  Mentions if the command came from a pseudo terminal.
 *
 * Outputs:
 *	None.
 *
 * Globals:
 *	pathmap_g.head:  Written to; to reset the pathmap to empty.
 *	pathmap_g.tail:  Same as above.
 *
 * Notes: 
 *	Directory names with whitespaces aren't supported.
 */
static void 
pathmap_command (char *map_str, int from_tty)
{
  struct pathmap_t *p = pathmap_g.head;
  struct pathmap_t *temp;
  char *from, *to;
  const char *white_space = " \t";
  int len, answer;
  struct partial_symtab *ps;
  struct objfile *objfile;

  dont_repeat();
  if (map_str == NULL)	/* Pathmap invoked with no arguments. */
    {
      answer = query ("Reinitialize pathmap and empty it?");

      p = pathmap_g.head;
      if (answer)
        {
          while (p)		/* Empty the pathmap. */
            {
	      temp = p->next;
              free (p->to);
              free (p->from);
              free (p);
	      p = temp;
            }
          pathmap_g.head = NULL;
          pathmap_g.tail = NULL;
        }
    }
  else		/* Pathmap invoked with one or more arguments. */
    {
      from = map_str;
      len = (int) strcspn (map_str, white_space);

      if (*(from + len) == '\0')
        {
          printf_unfiltered ("pathmap needs two arguments.\n");
	  return;
	}

      *(from + len) = '\0';
      to = from + len + 1;

      /* Skip spaces and tabs between the two paths. */
      while (isspace (*to))
        to++;

      len = (int) strcspn (to, white_space);
      if (*(to + len) != NULL)
      /* There are more than two strings, which is an error. */
      /* if (strspn (to, white_space)) */
        {
          printf_unfiltered ("pathmap takes only two arguments.\n");
	  return;
	}

      pathmap_add (from, to);

      /* JAGaf35671 - Pathmap doesn't work when it is set after 
       * a combo of file load and list.
       * Solution: Update the status of the symbols for demand loading.
       */

      ALL_PSYMTABS (objfile, ps) {
        if (OFILE_LOAD_STATUS_IS_RETRYABLE(ps)) {
          OFILE_LOAD_STATUS(ps) = DOOM_OFILE_DO_DEMAND_LOAD;
        }
      }
    }
/* JAGaf32389 */
    if (from_tty || annotation_level == 2) //For JAGaf45739
      show_pathmap((char *) 0, from_tty);
}
/*----------------------------------------------------------------------------*/
/* Function:  pathmap_add()
 *
 * Description:  Takes pointers to a pair of mapping strings, 'from' and 'to',
 *	duplicates them, cleans them and creates a path mapping with the copies.
 * 	This mapping is added to the end of the pathmap.
 *
 * Inputs:
 *	from:  What string, sub-string or part of a path should be substituted.
 *	to:  What the substitution string should be.
 *
 * Outputs:
 *	None.
 *
 * Globals:
 *	pathmap_g.head:  Written to; to make it point to the first mapping.
 *	pathmap_g.tail:  Written to; to make it point to the last mapping.
 *
 * Notes: 
 *	Allocates space using malloc.  This may not be correct; may have to use
 *	obstack.
 */
static void
pathmap_add (char *from,
             char *to )
{
  struct pathmap_t *p = (struct pathmap_t *) xmalloc (sizeof (struct pathmap_t));
  int len;

  p->from = xstrdup (from);
  p->to = xstrdup (to);
  p->next = NULL;

  /* Remove the unwanted '/' at the end of the from and to paths. */
  len = (int) strlen (p->from);
  if (p->from[len - 1] == '/')
    p->from[len - 1] = '\0';

  len = (int) strlen (p->to);
  if (p->to[len - 1] == '/')
    p->to[len - 1] = '\0';

  if (pathmap_g.tail == NULL)
    pathmap_g.head = p;
  else
    pathmap_g.tail->next = p;

  pathmap_g.tail = p;
}
/*----------------------------------------------------------------------------*/
/* Function:  pathmap_lookup()
 *
 * Description:  This function takes a file name, its associated compilation
 *	directory and searches through the mappings in the pathmap to find if
 * 	any of those mappings are applicable to the given compilation directory.
 *	If so, it substitutes the 'from' string in the mapping with the 'to' 
 *	and obtains a new path.  If the file is present in this path then the
 *	file is opened and its function descriptor is returned.  The full path
 *	of the file is also returned in a buffer.
 *
 * Inputs:
 *	file:  Name of the file to be opened.  Can be any type of file.
 *	full_path_buf:  Pointer to the buffer in which the full file path is
 *		returned if file is successfully located and opened.
 *	mode:  The mode in which the file should be opened.
 *
 * Outputs:
 *	return value:  -1 on error; file descriptor value on success.
 *	full_buf_path:  Fully qualified file name on success; unspecified in
 *		the event of a failure.
 *
 * Globals:
 *	pathmap_g.head:  Read from; to cycle through all mappings, strating
 *		with the first one specified.
 *	pathmap_g.cdir_p:  Read from; to determine the compilation diretory
 *		for the given file.
 *
 * Notes: 
 *	full_buf_path, though unlikely, may overflow.
 */
static int
pathmap_lookup ( char *file,
                 char *full_path_buf,
                 int mode )
{
  struct pathmap_t *p = pathmap_g.head;
  char dir, *pos, *pos2, temp, *path;
  char *mapped_path = full_path_buf;
  int len, fd, absolute_filename;

  /* Since pathmap functionality works only on source and object files,
   * if compilation directory is not set it could mean that neither an object file nor an
   * associated source file is being opened.  In such cases, we don't want
   * to apply pathmap, because gdb could be trying to load an executable or
   * shared library.
   */
  if (pathmap_g.cdir_p == NULL)
    return -1;

  /* If file name is absolute then work on it and forget the compilation path.
   */
  if (file[0] == '/')
  {
    path = file;
    absolute_filename = 1;
  }
  else
  {
    path = pathmap_g.cdir_p;
    absolute_filename = 0;
  }

  /* For each pathmap, see if there is a fit.
   */
  while (p)
  {
    /* If there is a match, then use that pathmap to modify the path
     * and derive the new file name.  If the new file exists, open
     * and return the file descriptor, else continue searching through the
     * pathmap.
     */
    pos = strstr (path, p->from);
    if (pos)
    {
      temp = *pos;
      *pos = '\0';
      strcpy (mapped_path, path);	/* Get the path up to the substitution point. */
      *pos = temp;
      strcat (mapped_path, p->to);	/* Put in the substituition. */
      pos += (int) strlen (p->from);
      strcat (mapped_path, pos);	/* Get the rest of the path */

      /* If is not an absolute file name, then add a '/' and the relative file name
       * to the mapped path.
       */
      if (!absolute_filename)
      {
	len = (int) strlen (mapped_path);
	if (mapped_path[len - 1] != '/')
	  strcat (mapped_path, "/");

	strcat (mapped_path, file);
      }

      fd = open (mapped_path, mode);	
      if (fd > 0)
        return fd;
    }
    p = p->next;
  }
  return -1;
}
/*----------------------------------------------------------------------------*/
/* Function:  pathmap_set_cdir()
 *
 * Description:  Takes a pointer to a compilation directory in a path,
 *	duplicates it, extracts only the compilation directory and then stores
 *	it in the pathmap global state.  This function is invoked at the place
 *	where the $cdir in the path for a given object or source file expanded
 *	to the actual compilation directory.  This is to let pathmap
 *	functionality know the compilation directory that will be needed
 *	for the next file search.
 *
 * Inputs:
 *	cdir:  Pointer to the compilation directory in a path.
 *
 * Outputs:
 *	None.
 *
 * Globals:
 *	pathmap_g.cdir_p:  Written to; to set the compilation directory that
 *	would be needed by pathmap for the next file search.
 *
 * Notes: 
 *	pathmap_g.cdir_p is set by the function.  However, it should also be
 *	reset to NULL after each search pass through the pathmap because it
 *	is specific for a given file.
 */
void
pathmap_set_cdir (char *cdir)
{
  char *pos;

  if (cdir[0] != '/')
    warning ("Compilation directory is not absolute; could result in \
incorrect behavior.  Please report to HP.");

  pathmap_g.cdir_p = xstrdup (cdir);
  
  /* The compilation directory is followed by other paths; we don't want those. */
  pos = strchr (pathmap_g.cdir_p, DIRNAME_SEPARATOR);
  if (pos != NULL)
    *pos = '\0';
}
#endif	/* PATHMAP */
/*----------------------------------------------------------------------------*/
