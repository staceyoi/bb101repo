/* Print and select stack frames for GDB, the GNU debugger.
   Copyright 1986, 1987, 1989, 1991-1996, 1998-2000 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <ctype.h>
#include <sys/param.h>
#include "defs.h"
#include "gdb_string.h"
#include "value.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "expression.h"
#include "language.h"
#include "frame.h"
#include "gdbcmd.h"
#include "gdbcore.h"
#include "target.h"
#include "breakpoint.h"
#include "demangle.h"
#include "inferior.h"
#include "annotate.h"
#include "symfile.h"
#include "objfiles.h"
#include "valprint.h"
#include "exec-path.h"
#include "javalib.h"	/* For Java Unwind library */
#include <dl.h>		/* To shload the Java Unwind library */
#ifdef UI_OUT
#include "ui-out.h"
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "libdas.h"
/* jini: Mixed mode corefile debugging support. */
#ifdef HP_IA64
#include "pa_save_state.h"
#include "mixed_mode.h"
#include "arch-utils.h"
#include <assert.h>
#include <sys/utsname.h>
#include <sys/dyntune.h> /* For gettune() */
#endif
#define DEBUG(x) 

char * wdb_status_line (struct symtab_and_line);

/* Prototypes for exported functions. */

void args_info (char *, int);

void locals_info (char *, int);

void locals_info_no_values PARAMS ((char *, int));

void (*selected_frame_level_changed_hook) (int);

void _initialize_stack (void);

/* Prototypes for local functions. */

static void return_command (char *, int);

static void down_command (char *, int);

static void down_silently_base (char *);

static void down_silently_command (char *, int);

static void up_command (char *, int);

static void up_silently_base (char *);

static void up_silently_command (char *, int);

void frame_command (char *, int);

static void current_frame_command (char *, int);

static void select_frame_command (char *, int);

static void print_frame_arg_vars (struct frame_info *, struct ui_file *);

static void catch_info (char *, int);

static void args_plus_locals_info (char *, int);

#ifndef TARGET_CATCH_INFO
static void print_frame_label_vars (struct frame_info *, int,
				    struct ui_file *);

static int print_block_frame_labels (struct block *, int *,
				     struct ui_file *);
#endif

static void print_frame_local_vars (struct frame_info *, int,
				    struct ui_file *);

static int print_block_frame_locals (struct block *,
				     struct frame_info *,
				     int,
				     struct ui_file *);

static void print_frame (struct frame_info *fi, 
			 int level, 
			 int source, 
			 int args, 
			 struct symtab_and_line sal);

static void print_frame_info_base (struct frame_info *, int, int, int);

static void backtrace_command (char *, int);

static void jvm_state_command (char *, int);

static void java_unwind_info_command (char *, int);

static void java_unwind_table_command (char *, int);
 
static void java_mutex_info_command (char *, int);
 
static void java_object_command (char*, int);

static void java_bytecodes_command (char *, int);

static void java_reference_command (char*, int);

static void java_object_histogram_command (char*, int);

static void java_klass_instances_command (char*, int);

static void java_oop_for_command (char*, int);

static void java_args_command (char*, int);

static void java_locals_command (char*, int);

struct frame_info *parse_frame_specification (char *);

static void backtrace_other_thread_command (char *, int);
CORE_ADDR backtrace_other_thread_bsp = (CORE_ADDR) (long) -1;

static void frame_info (char *, int);
char * wdb_status_line (struct symtab_and_line);

void locals_info_no_values PARAMS ((char *, int));
char * get_java_frame_info (struct basic_frame_info *frame);

/* JAGag29592 */
void set_currently_printing_frame();

/* From infttrace.c */
extern int target_insert_internal_breakpoint (CORE_ADDR addr, void* shadow);

//For JAGaf13527
#ifndef HP_IA64
extern int dummyread;
int dummy_frame_read;
#endif

#ifdef HP_IA64
/* get function start for label */
extern CORE_ADDR handle_label PARAMS ((CORE_ADDR)); 
#endif

extern CORE_ADDR lookup_address_of_variable (char*);

extern int print_insn (CORE_ADDR memaddr, struct ui_file *stream);

extern int
subsetCompare (char *stringToCompare, char *templateString);

extern int addressprint;	/* Print addresses, or stay symbolic only? */
extern int info_verbose;	/* Verbosity of symbol reading msgs */
extern int lines_to_list;	/* # of lines "list" command shows by default */

extern boolean single_stepping; /* From infcmd.c */
extern void print_doc_lines (CORE_ADDR pc, struct ui_file * stream);

/* JAGaf49121 */
extern char *cplus_demangled_name;

int user_not_warned_on_incomplete_bt = 0; /* JAGad92223 - warning for incomplete stack trace */

/* The "selected" stack frame is used by default for local and arg access.
   May be zero, for no selected frame.  */

struct frame_info *selected_frame;

/* JAGag29592 */
extern struct frame_info *currently_printing_frame;
/* Level of the selected frame:
   0 for innermost, 1 for its caller, ...
   or -1 for frame specified by address with no defined level.  */

int selected_frame_level;
int dont_print_value = 0;

/* Zero means do things normally; we are interacting directly with the
   user.  One means print the full filename and linenumber when a
   frame is printed, and do so in a format emacs18/emacs19.22 can
   parse.  Two means print similar annotations, but in many more
   cases and in a slightly different syntax.  */

int annotation_level = 0;

#ifdef HP_IA64
/* jini: QXCR1000875632: If 'unwind_all_frames' is 1, then gdb does not
   limit the nbr of stack frames to be displayed.
 */
bool unwind_all_frames;
#endif
extern int java_debugging;

struct print_stack_frame_args
  {
    struct frame_info *fi;
    int level;
    int source;
    int args;
  };

/* For batch mode thread check. */
extern int trace_threads_in_this_run;
extern int batch_rtc;
extern CORE_ADDR librtc_start;
extern CORE_ADDR librtc_end;
extern CORE_ADDR libpthread_start;
extern CORE_ADDR libpthread_end;

/* jini: Mixed mode support for core file debugging. */
#ifdef HP_IA64
char *aries_tp_str = "aries_tp";
struct objfile  *aries_objfile;
bool   mixed_mode_pa_unwind;
CORE_ADDR aries_tp_addr;
CORE_ADDR aries_pa2ia_export_stub_addr;
int aries_pa2ia_export_stub_size;
int nbr_contexts;
int context_nbr;
int nbr_pa_to_ia_calls;
/* aries_border_frame is the original aries frame at the border while crossing
   over from IA land. Used for java unwinding. */
struct frame_info *aries_border_frame;
extern int mixed_mode_debug_aries;
extern CORE_ADDR inferior_aries_text_start;
extern CORE_ADDR inferior_aries_text_end;
extern void * frame_obstack_alloc (unsigned long size);
struct frame_info * mixed_mode_adjust (struct frame_info *fi);
extern bool in_mixed_mode_pa_lib (CORE_ADDR addr, struct objfile *objfile);
extern void mixed_mode_get_frame_saved_regs (struct frame_info *frame,
                               struct mixed_mode_frame_saved_regs *saved_regs_addr);
/* Make a reference to callbacks that we provide to libaries, so that
   linker will not do procelim of these functions. */
static void* __mixed_mode_callback_refs[] = { 
                                 (void*) mixed_mode_read_memory,
                                 (void*) 0}; 

/* Function pointers for the libaries APIs. */
int (*ptr_aries_get_save_state) (CORE_ADDR        aries_tp_addr,
                                 int              context_nbr,
                                 pa_save_state_t *pa_save_state_ptr,
                                 bool             is_32bit);

int (*ptr_aries_get_nbr_contexts) (CORE_ADDR        aries_tp_addr,
                                   int             *nbr_contexts,
                                   bool             is_32bit);

int (*ptr_aries_in_signal_handler) (CORE_ADDR        aries_tp_addr,
                                    char            *signal_handler_name,
                                    int             *in_signal_handler,
                                    bool             is_32bit);

int (*ptr_aries_get_pa2ia_export_stub_size) ();

#endif

/* Show and print the frame arguments.
   Pass the args the way catch_errors wants them.  */
static int show_and_print_stack_frame_stub (void *args);
static int
show_and_print_stack_frame_stub (void *args)
{
  struct print_stack_frame_args *p = (struct print_stack_frame_args *) args;

  /* Reversed order of these so tuiDo() doesn't occur
   * in the middle of "Breakpoint 1 ... [location]" printing = RT
   */
  if (tui_version)
    print_frame_info_base (p->fi, p->level, p->source, p->args);
  print_frame_info (p->fi, p->level, p->source, p->args);

  return 0;
}

/* Show or print the frame arguments.
   Pass the args the way catch_errors wants them.  */
static int print_stack_frame_stub (void *args);
static int
print_stack_frame_stub (void *args)
{
  struct print_stack_frame_args *p = (struct print_stack_frame_args *) args;

  if (tui_version)
    print_frame_info (p->fi, p->level, p->source, p->args);
  else
    print_frame_info_base (p->fi, p->level, p->source, p->args);
  return 0;
}

/* print the frame arguments to the terminal.  
   Pass the args the way catch_errors wants them.  */
static int print_only_stack_frame_stub (void *);
static int
print_only_stack_frame_stub (void *args)
{
  struct print_stack_frame_args *p = (struct print_stack_frame_args *) args;

  print_frame_info_base (p->fi, p->level, p->source, p->args);
  return 0;
}

/* Show and print a stack frame briefly.  FRAME_INFI should be the frame info
   and LEVEL should be its level in the stack (or -1 for level not defined).
   This prints the level, the function executing, the arguments,
   and the file name and line number.
   If the pc is not at the beginning of the source line,
   the actual pc is printed at the beginning.

   If SOURCE is 1, print the source line as well.
   If SOURCE is -1, print ONLY the source line.  */

void
show_and_print_stack_frame (struct frame_info *fi, int level, int source)
{
  struct print_stack_frame_args args;

  args.fi = fi;
  args.level = level;
  args.source = source;
  args.args = 1;

  catch_errors (show_and_print_stack_frame_stub, &args, "", RETURN_MASK_ALL);
}

void
set_currently_printing_frame()
{
  /* JAGag29592: jini: Reset 'currently_printing_frame' to 'selected_frame'.
     Commands like 'info frame <frame nbr>, backtrace, etc, can alter this
     variable. This results in functions like f77_get_dynamic_upperbound()
     return incorrect 'upper_bound' values, inturn resulting in incorrect
     TYPE_LENGTH associated with the types created.

     Have 'selected_frame' in sync with 'selected_frame_level' before having
     'currently_printing_frame' point to it. */
  int frame_level = selected_frame_level;
  select_frame (find_relative_frame (get_current_frame(),
                                     &frame_level),
                selected_frame_level);
  currently_printing_frame = selected_frame;
}


/* Show or print a stack frame briefly.  FRAME_INFI should be the frame info
   and LEVEL should be its level in the stack (or -1 for level not defined).
   This prints the level, the function executing, the arguments,
   and the file name and line number.
   If the pc is not at the beginning of the source line,
   the actual pc is printed at the beginning.

   If SOURCE is 1, print the source line as well.
   If SOURCE is -1, print ONLY the source line.  */

void
print_stack_frame (struct frame_info *fi, int level, int source)
{
  struct print_stack_frame_args args;

  args.fi = fi;
  args.level = level;
  args.source = source;
  args.args = 1;

  catch_errors (print_stack_frame_stub, (char *) &args, "", RETURN_MASK_ALL);
}

/* Print a stack frame briefly.  FRAME_INFI should be the frame info
   and LEVEL should be its level in the stack (or -1 for level not defined).
   This prints the level, the function executing, the arguments,
   and the file name and line number.
   If the pc is not at the beginning of the source line,
   the actual pc is printed at the beginning.

   If SOURCE is 1, print the source line as well.
   If SOURCE is -1, print ONLY the source line.  */

void
print_only_stack_frame (struct frame_info *fi, int level, int source)
{
  struct print_stack_frame_args args;

  args.fi = fi;
  args.level = level;
  args.source = source;
  args.args = 1;

  catch_errors (print_only_stack_frame_stub, &args, "", RETURN_MASK_ALL);
}

struct print_args_args
{
  struct symbol *func;
  struct frame_info *fi;
  struct ui_file *stream;
};

static int print_args_stub (PTR);

/* Pass the args the way catch_errors wants them.  */

static int
print_args_stub (PTR args)
{
  int numargs;
  struct print_args_args *p = (struct print_args_args *) args;

  numargs = FRAME_NUM_ARGS (p->fi);
  print_frame_args (p->func, p->fi, numargs, p->stream);
  return 0;
}

/* Print information about a frame for frame "fi" at level "level".
   Used in "where" output, also used to emit breakpoint or step
   messages.  
   LEVEL is the level of the frame, or -1 if it is the
   innermost frame but we don't want to print the level.  
   The meaning of the SOURCE argument is: 
   SRC_LINE: Print only source line
   LOCATION: Print only location 
   LOC_AND_SRC: Print location and source line.  */

static void
print_frame_info_base (struct frame_info *fi, int level, int source, int args)
{
  struct symtab_and_line sal;
  int source_print;
  int location_print;
  extern int nimbus_version;
  extern int firebolt_version;
  extern int assembly_level_debugging;
  CORE_ADDR funvalue = 0;
  int freename = 0;
  char *demangled = NULL;
  int constr = 0;
  extern int firebolt_version;

   /* bloomberg hack */

   fi->pc = SWIZZLE (fi->pc);

/* RM: On HPPA, the frame_in_dummy() test fails. Using the
   PC_IN_CALL_DUMMY test is not expensive, so we'll use it */

#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
      if (fi->pc >= fi->frame
          && fi->pc <= (fi->frame +
			(REGISTER_SIZE / INSTRUCTION_SIZE) * CALL_DUMMY_LENGTH
			+ 32 * REGISTER_SIZE + (NUM_REGS - FP0_REGNUM) * 8
			+ 6 * REGISTER_SIZE) ||
	  PC_IN_REDIRECT_THROUGH_DUMMY (fi->pc))
#else
	if (PC_IN_CALL_DUMMY (fi->pc, 0, 0))
#endif
#else  
#ifdef HP_IA64
  if (PC_IN_CALL_DUMMY (fi->pc, sp, fi->frame))
#else
  if (frame_in_dummy (fi))
#endif
#endif
    {
      annotate_frame_begin (level == -1 ? 0 : level, fi->pc);

      /* Do this regardless of SOURCE because we don't have any source
         to list for this frame.  */
      if (level >= 0)
	printf_filtered ("#%-2d ", level);
      annotate_function_call ();
      printf_filtered ("<function called from gdb>\n");
      annotate_frame_end ();
      return;
    }
  if (fi->signal_handler_caller)
    {
      annotate_frame_begin (level == -1 ? 0 : level, fi->pc);

      /* Do this regardless of SOURCE because we don't have any source
         to list for this frame.  */
      if (level >= 0)
	printf_filtered ("#%-2d ", level);
      annotate_signal_handler_caller ();
      printf_filtered ("<signal handler called>\n");
      annotate_frame_end ();
      return;
    }

  /* If fi is not the innermost frame, that normally means that fi->pc
     points to *after* the call instruction, and we want to get the line
     containing the call, never the next line.  But if the next frame is
     a signal_handler_caller or a dummy frame, then the next frame was
     not entered as the result of a call, and we want to get the line
     containing fi->pc. */

#ifdef INLINE_SUPPORT
 /*  Also, if the next frame is an inlined frame
     we want to get the line containing fi->pc */

  if (fi->inline_idx == 0 && fi->next && fi->next->inline_idx == 0)
    {
      sal = find_pc_inline_idx_line (fi->pc,
		  fi->inline_idx, 
                  fi->next != NULL
		  && !fi->next->signal_handler_caller
		  && !frame_in_dummy (fi->next));
     }
   else
     {
	/* Pick-up the line with the inline_idx */
        sal = find_pc_inline_idx_line (fi->pc,
				         fi->inline_idx,
					 fi->next != NULL
					 && fi->next->inline_idx == 0
                  		         && !fi->next->signal_handler_caller
			                 && !frame_in_dummy (fi->next));
      }
#else
  sal =
    find_pc_line (fi->pc,
                  fi->next != NULL
                  && !fi->next->signal_handler_caller
                  && !frame_in_dummy (fi->next));
#endif  /* INLINE_SUPPORT */

  location_print = (source == LOCATION 
		    || source == LOC_AND_ADDRESS
		    || source == SRC_AND_LOC);

  if (location_print || !sal.symtab)
    print_frame (fi, level, source, args, sal);

  source_print = (source == SRC_LINE || source == SRC_AND_LOC);

  if (source_print && sal.symtab)
    {
      int done = 0;

      int mid_statement = (source == SRC_LINE) && (fi->pc != sal.pc);

      if (annotation_level)
	done = identify_source_line (sal.symtab, sal.line, mid_statement,
				     fi->pc);

      if (!done)
	{
	  if (single_stepping == 1 && !tui_version)
	    {
	      /* Print doc line information and the assemble instruction
		 when single stepping. */
	      print_doc_lines (fi->pc, gdb_stdout);
	      print_address_numeric (fi->pc, 1, gdb_stdout);
	      printf_filtered ("\t");
	      print_insn (fi->pc, gdb_stdout);
              printf_filtered ("\n");
	    }
	  if ( addressprint && mid_statement && !tui_version)
	    {
#ifdef UI_OUT
	      if (ui_out_is_mi_like_p(uiout))
  	        ui_out_field_core_addr (uiout, "addr", fi->pc);
	      else
	        if (tui_version || single_stepping != 1)
		  {
	            print_address_numeric (fi->pc, 1, ui_out_stream(uiout));
	            ui_out_text (uiout, "\t");
		  }
#else
	      if (tui_version || single_stepping != 1)
		{
	          print_address_numeric (fi->pc, 1, gdb_stdout);
	          printf_filtered ("\t");
		}
#endif
	    }
	  if (print_frame_info_listing_hook)
	    print_frame_info_listing_hook (sal.symtab, sal.line, sal.line + 1, 0);
	  else if (!tui_version)
	    print_source_lines (sal.symtab, sal.line, sal.line + 1, 0);
	}
      current_source_line = max (sal.line - lines_to_list / 2, 1);

    }

    if (nimbus_version && source_print)
      {
#ifdef HP_IA64
        nimbus_current_pc = fi->pc;

        if (!assembly_level_debugging)
          {
            /* Stacey 03/21/2002
               We're doing source level debugging, so give vim the proper
               source file name and starting line number at which to start
               displaying the file - ask vim to display the file in line
               number mode.  In event we can't find the file info, display
               the 'no file' message from /opt/langtools/lib/nofile.c */

            printf_unfiltered ("\033\032\032:se stl=%s\n:e +%d "
                               "%s\n:se nu\n\032\032A",
                               wdb_status_line (sal), sal.line,
                               (sal.symtab && sal.symtab -> fullname) ?
                               sal.symtab -> fullname :
                               (sal.symtab && sal.symtab -> filename) ?
                               sal.symtab -> filename :
                               "/opt/langtools/lib/nofile.c");
          }
        else
          {
            /* Stacey 03/21/2002
               Create a new file, and name it after the nimbus process id and
               the address range for the code being displayed.
               /tmp/nimbus.pid/low-high.  Call generate_disassembly_file to
               fill it with assembly code, then give it to vim and tell vim
               to display it (with no line numbers).  Pass in the current
               pc so that vim knows where to print the '>' sign which shows
               where the program has stopped.  */

            FILE *disassem_temp_fstream = NULL;
            char disassem_temp_file[80];
            char gdb_pc[32];
            CORE_ADDR low, high;
            char *name;

            if (find_pc_partial_function (nimbus_current_pc,
                                          &name, &low, &high) == 0)
              {
                error ("No function contains specified address.\n");
              }
            /* Bundle-align LOW and HIGH */
            low &= ~(0xF);
            high &= ~(0xF);

            sprintf (disassem_temp_file,
                    "/tmp/nimbus.%d/%llx-%llx", getpid(), low, high);

            if ( (disassem_temp_fstream =
                  fopen (disassem_temp_file, "r")) == NULL)
              {
                generate_disassembly_file (disassem_temp_file, low, high);
              }
            if (disassem_temp_fstream != NULL)
              fclose (disassem_temp_fstream);

            strcat_address_numeric (nimbus_current_pc, 1, gdb_pc, 32);

            if (firebolt_version)
                printf_unfiltered("\033\032\032:se stl=%s\n:e +/^%s %s\n\032\032A",
                              wdb_status_line(sal), gdb_pc,
                              disassem_temp_file);
            else
                printf_unfiltered("\033\032\032:se stl=%s\n:e %s\n:/%s "
                              "<\n:se nonumber\n\032\032A",
                              wdb_status_line(sal),
                              disassem_temp_file, gdb_pc);
          }

#else
        if (firebolt_version && assembly_level_debugging)
          unassemble_command (0, 0);
        else
          printf_unfiltered ("\033\032\032:se stl=%s\n:e +%d %s\n\032\032A",
                    wdb_status_line(sal), sal.line, 
                             (sal.symtab && sal.symtab -> fullname) ?
                             sal.symtab -> fullname : 
                             (sal.symtab && sal.symtab -> filename) ?
                             sal.symtab -> filename : "/opt/langtools/lib/nofile.c");
#endif /* !HP_IA64 */
        gdb_flush (gdb_stdout);
      }

  if (source != 0)
    set_default_breakpoint (1, fi->pc, sal.symtab, sal.line);

  annotate_frame_end ();

  gdb_flush (gdb_stdout);
}

/* Sunil JAGaf49959 */
extern int display_full_path;

static void
print_frame (struct frame_info *fi, 
	     int level, 
	     int source, 
	     int args, 
	     struct symtab_and_line sal)
{
  struct symbol *func;
  register char *funname = 0;
  enum language funlang = language_unknown;
  int freename=0;
  CORE_ADDR funvalue = 0;
  CORE_ADDR funhivalue = 0;
  CORE_ADDR func_addr = 0;
  CORE_ADDR func_hiaddr = 0;
  char *demangled = NULL;
  int constr = 0;
#ifdef HP_IA64
  struct unwind_table_entry *find_unwind_entry (CORE_ADDR);
  struct unwind_table_entry *unw_entry = NULL;
#endif
#ifdef UI_OUT
  struct ui_stream *stb;
  struct cleanup *old_chain;
  struct cleanup *list_chain;
  stb = ui_out_stream_new (uiout);
  old_chain = make_cleanup_ui_out_stream_delete (stb);
#endif /* UI_OUT */


#ifdef INLINE_SUPPORT
  /* the existing code was already quite complicated with various
     paths. I have separated the code for printing inline frame at one
     place - Diwakar 03/28/05 
  */
  if (fi->inline_idx > 0)
    {
       funname = get_inline_name(fi->inline_idx);
       if (is_cplus_name(funname, 1))        /* JAGaf49121 */
         {
            funname = cplus_demangled_name;
            freename = 1; /* free funname later */
         }

       annotate_frame_begin (level == -1 ? 0 : level, fi->pc);
       list_chain = make_cleanup_ui_out_tuple_begin_end (uiout, "frame");
       if (level >= 0)
         {
#ifdef UI_OUT
            if (fi->frame_type == 1)
              ui_out_text(uiout, "\t\tmissing inline frames (...)\n");
            ui_out_text(uiout, "#");       
            ui_out_field_fmt(uiout, "level", "%-2d", level);
	    ui_out_spaces(uiout, 1);
#else
            if (fi->frame_type == 1)
              printf_filtered("\t\tmissing inline frames (...)\n");
            printf_filtered("#%-2d ", level);
#endif
         }

      annotate_frame_address();

      /* it is not the top most inline frame */
      if ((addressprint) && 
          (source == LOC_AND_ADDRESS || fi->next != NULL))
        {
	   annotate_frame_address ();
#ifdef UI_OUT
	  if (ui_out_is_mi_like_p (uiout))
	    ui_out_field_core_addr (uiout, "addr", fi->pc);
	  else
	    print_address_numeric (fi->pc, 1, ui_out_stream (uiout));
	   annotate_frame_address_end ();
	   ui_out_text (uiout, " in ");
#else
	   print_address_numeric (fi->pc, 1, gdb_stdout);
	   annotate_frame_address_end ();
	   printf_filtered (" in ");
#endif
        }

     annotate_frame_function_name ();

#ifdef UI_OUT
     ui_out_text (uiout, "inline ");
     fprintf_symbol_filtered (stb->stream, funname, funlang, DMGL_ANSI);
     ui_out_field_stream (uiout, "func", stb);
     ui_out_wrap_hint (uiout, "   ");
#else
     fputs_filtered ("inline ", gdb_stdout);
     fprintf_symbol_filtered (gdb_stdout, funname, funlang, DMGL_ANSI);
#endif
#ifndef UI_OUT
     wrap_here ("   ");
#endif
     annotate_frame_args ();
      
#ifdef UI_OUT
     ui_out_text (uiout, " (");
#else
     fputs_filtered (" (", gdb_stdout);
#endif

     /* DIWAKAR : code for showing args will go here */
     if (args)
       {
          if (argsprint)
            {
	       struct print_args_args args;
	       args.fi = fi;
	       args.func = find_pc_function (fi->pc);
	       args.stream = gdb_stdout;
#ifdef UI_OUT
	       ui_out_list_begin (uiout, "args");
	       catch_errors (print_args_stub, &args, "", RETURN_MASK_ALL);
	       ui_out_list_end (uiout);
#else
	      catch_errors (print_args_stub, &args, "", RETURN_MASK_ALL);
#endif
	      QUIT;
	    }
          else
            {
	  
#ifdef UI_OUT
	       ui_out_text (uiout, "...");
#else
	       fputs_filtered ("...", gdb_stdout);
#endif
	     }
        }

#ifdef UI_OUT
  ui_out_text (uiout, ")");
#else
     fputs_filtered (")", gdb_stdout);
#endif
     if (sal.symtab && sal.symtab->filename)
        {
           annotate_frame_source_begin ();
#ifdef UI_OUT
           ui_out_wrap_hint (uiout, "   ");
           ui_out_text (uiout, " at ");
           annotate_frame_source_file ();
           ui_out_field_string (uiout, "file",
	       annotation_level > 1 ?
	       symtab_to_filename (sal.symtab) :
	       sal.symtab->filename);
           annotate_frame_source_file_end ();
           ui_out_text (uiout, ":");
           annotate_frame_source_line ();
           ui_out_field_int (uiout, "line", sal.line);
#else
           wrap_here ("   ");

           printf_filtered (" at ");
           annotate_frame_source_file ();
           printf_filtered ("%s", annotation_level > 1 ?
		symtab_to_filename (sal.symtab) :
		sal.symtab->filename);
           annotate_frame_source_file_end ();
           printf_filtered (":");
           annotate_frame_source_line ();
           printf_filtered ("%d", sal.line);
#endif
           annotate_frame_source_end ();
        }
#ifdef UI_OUT
      do_cleanups(list_chain);
      ui_out_text(uiout, "\n");
      do_cleanups(old_chain);
#else
      printf_filtered("\n");
#endif
      if (freename)
         free (funname);
      funname = NULL;
      return;
    }
#endif /* INLINE_SUPPORT */
         
  func = find_pc_function (fi->pc);
  
  if (func)
    {
      /* In certain pathological cases, the symtabs give the wrong
         function (when we are in the first function in a file which
         is compiled without debugging symbols, the previous function
         is compiled with debugging symbols, and the "foo.o" symbol
         that is supposed to tell us where the file with debugging symbols
         ends has been truncated by ar because it is longer than 15
         characters).  This also occurs if the user uses asm() to create
         a function but not stabs for it (in a file compiled -g).

         So look in the minimal symbol tables as well, and if it comes
         up with a larger address for the function use that instead.
         I don't think this can ever cause any problems; there shouldn't
         be any minimal symbols in the middle of a function; if this is
         ever changed many parts of GDB will need to be changed (and we'll
         create a find_pc_minimal_function or some such).  */

      struct minimal_symbol *msymbol;
      funname = SYMBOL_NAME (func);
      funlang = SYMBOL_LANGUAGE (func);

      if (funlang == language_cplus)
        {
	   demangled = cplus_demangle (funname, DMGL_ANSI);
          /* I'd like to use SYMBOL_SOURCE_NAME() here, to display
           * the demangled name that we already have stored in
           * the symbol table, but we stored a version with
           * DMGL_PARAMS turned on, and here we don't want
           * to display parameters. So call the demangler again,
           * with DMGL_ANSI only. RT
           * (Yes, I know that printf_symbol_filtered() will
           * again try to demangle the name on the fly, but
           * the issue is that if cplus_demangle() fails here,
           * it'll fail there too. So we want to catch the failure
           * ("demangled==NULL" case below) here, while we still
           * have our hands on the function symbol.)
           */
           if (demangled == NULL)
             /* If the demangler fails, try the demangled name
              * from the symbol table. This'll have parameters,
              * but that's preferable to diplaying a mangled name.
              */
             funname = SYMBOL_SOURCE_NAME (func);
           else
             {
               /* If the demangling passes then make funname
                  equal to the demangled name since funname is what
                  we print out. Also we set freename to true since
                  the demangler allocates space and we need to
                  free it once we're done with the name
                */
	       if (demangled != funname)
		 freename = 1;
               funname = demangled;
	       constr = is_constructor (demangled);
             }
	}
      if (!constr 
	  && ((msymbol = lookup_minimal_symbol_by_pc (fi->pc)) != NULL)
	  && (SWIZZLE(SYMBOL_VALUE_ADDRESS (msymbol))
	      > SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (func)))))
	{
	  /* We also don't know anything about the function besides
	     its address and name.  */
	  func = 0;
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	  if (new_cxx_abi && is_cplus_name (funname, 1))    /* JAGaf49121 */
	    {
	      demangled = cplus_demangled_name;
	      funname = demangled;
	      freename = 1;
	      constr = is_constructor (demangled);
	    }
	}
      func_addr = func ? BLOCK_START (SYMBOL_BLOCK_VALUE (func)) : 0;
      func_hiaddr = func ? BLOCK_END (SYMBOL_BLOCK_VALUE (func)) : 0;      

      func_addr = SWIZZLE(func_addr);
      func_hiaddr = SWIZZLE(func_hiaddr);
    }
  else
    {
      struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);
      if (msymbol != NULL)
	{
	  CORE_ADDR funvalue1;
	  funvalue = SYMBOL_VALUE_ADDRESS (msymbol);
#ifdef HP_IA64
	  /* check if label and update function start */
	  if ((funvalue1 = handle_label (funvalue)) != funvalue) 
	    {
	      msymbol = lookup_minimal_symbol_by_pc (funvalue1);
	      funvalue = SYMBOL_VALUE_ADDRESS (msymbol);
	    }
	  /* Store high pc of a minsym - we have it for elf */
	  funhivalue = funvalue + (CORE_ADDR) MSYMBOL_INFO (msymbol);
#endif
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	  if (new_cxx_abi && is_cplus_name (funname, 1))    /* JAGaf49121 */
	    {
	      demangled = cplus_demangled_name;
	      funname = demangled;
	      freename = 1;
	      constr = is_constructor (demangled);
	    }
	}
      else if (java_debugging && (is_java_frame (fi->pc)))
        { /* Handle Java case */
#ifdef GET_JAVA_FUNC_NAME
          funname = GET_JAVA_FUNC_NAME (fi);
          freename = 1;
#endif
        }
    }

  annotate_frame_begin (level == -1 ? 0 : level, fi->pc);

 /* JAGad92223 - warning for incomplete stack trace */

  if (    (funname == NULL)
          && (user_not_warned_on_incomplete_bt)
#ifdef PC_SOLIB
          && PC_SOLIB (fi->pc))
#endif
    {
      warning("GDB cannot print complete stack trace since some shared "
              "libraries are missing. Set GDB_SHLIB_PATH and try again.\n");
      /* set to 0 so that warning will not be repeated. */
      user_not_warned_on_incomplete_bt = 0;
    }  

#ifdef UI_OUT
  list_chain = make_cleanup_ui_out_tuple_begin_end (uiout, "frame");
#endif

  if (level >= 0)
    {
#ifdef UI_OUT
      ui_out_text (uiout, "#");
      ui_out_field_fmt (uiout, "level", "%-2d", level);
      ui_out_spaces (uiout, 1);
#else
      printf_filtered ("#%-2d ", level);
#endif
    }
  if (addressprint)
#ifdef INLINE_SUPPORT
    /* print the frame address if there is dummy inline
       frame above this frame.
    */
    if ( fi->pc != sal.pc || !sal.symtab || 
        source == LOC_AND_ADDRESS || (fi->next ? fi->next->inline_idx : 0) > 0)
#else
    if (fi->pc != sal.pc || !sal.symtab || source == LOC_AND_ADDRESS)
#endif
      {
	annotate_frame_address ();
#ifdef UI_OUT
	if (ui_out_is_mi_like_p (uiout))
	  ui_out_field_core_addr (uiout, "addr", fi->pc);
	else
	  print_address_numeric (fi->pc, 1, ui_out_stream (uiout));
	annotate_frame_address_end ();
	ui_out_text (uiout, " in ");
#else
	print_address_numeric (fi->pc, 1, gdb_stdout);
	annotate_frame_address_end ();
	printf_filtered (" in ");
#endif
      }
  annotate_frame_function_name ();
  /* If we have gone beyond the high pc then say that we don't know where
     we are */

#ifdef HP_IA64
  if (   (func_addr && func_hiaddr && ((CORE_ADDR) (fi->pc) > func_hiaddr))
      || (funvalue && funhivalue && ((CORE_ADDR) (fi->pc) > funhivalue)))
    {
      if (funvalue != funhivalue) 
	{
	  unw_entry = find_unwind_entry (fi->pc);
	  
	  if (   unw_entry 
		 && unw_entry->region_start)
		{
#ifdef UI_OUT
		ui_out_text (uiout, "<unknown_procedure> + ");
		ui_out_field_fmt (uiout, "unknown_procedure", "%#x",
			(fi->pc - (CORE_ADDR) unw_entry->region_start));
#else
	    printf_filtered ("<unknown_procedure> + %#x", 
			     (fi->pc - (CORE_ADDR) unw_entry->region_start));
#endif				 
		}				 
	  else
	    /* If the unwind entry is null it probably is a stub 
	       gdb knows about stubs so print funname if you have it */
		   {
#ifdef UI_OUT
		ui_out_field_fmt (uiout, "unknown_procedure", "%s + %#x",
				 funname ? funname: "<unknown_procedure>",
				 (int) fi->pc-funvalue);
#else
	    printf_filtered ("%s + %#x", 
			     funname ? funname : "<unknown_procedure>", 
			     (int) fi->pc-funvalue);
#endif				 
		   }	 
	}
      else /* May not have info in the msymbol hence funvalue == funhivalue */
#ifdef UI_OUT
		ui_out_field_fmt (uiout, "unknown_procedure", "%s + %#x",
				 funname ? funname: "<unknown_procedure>",
				 (int) fi->pc-funvalue);
#else
	printf_filtered ("%s + %#x", 
			 funname ? funname : "<unknown_procedure>", 
			 (int) fi->pc-funvalue);
#endif			 
    }
  else 
#endif
    {
#ifndef HP_IA64
      /* JAGae52820: Instead of showing the closest function for a 
         hidden static function (when linked with '-x' option),
         gdb can say "unknown procedure". This will work out only
         if the PC is found in normal region of the unwind table
         entries for the static function
      */

        struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc ((CORE_ADDR) (fi->pc));
        struct unwind_table_entry *u = (struct unwind_table_entry *) hppa_find_unwind_entry((CORE_ADDR) (fi->pc));
        if (u && (u->Region_description == 0x0 &&  /* 0x0 == Normal region */
                 (SYMBOL_VALUE_ADDRESS (msymbol) != u->region_start)))
          {
#ifdef UI_OUT
		ui_out_text (uiout, "<unknown_procedure> + ");
		ui_out_field_fmt (uiout, "unknown_procedure", "%#x",
			(fi->pc - (CORE_ADDR) u->region_start));
#else
	    printf_filtered ("<unknown_procedure> + %#x", 
			     (fi->pc - (CORE_ADDR) u->region_start));
#endif	
           
         }
       else 
#endif
       {	
#ifdef UI_OUT
	  fprintf_symbol_filtered (stb->stream,
	  			   funname ? funname : "<unknown_procedure>",
				   funlang,
				   			  DMGL_ANSI);
	  ui_out_field_stream (uiout, "func", stb);
	  ui_out_wrap_hint (uiout, "   ");
#else
      fprintf_symbol_filtered (gdb_stdout, 
			       funname ? funname : "<unknown_procedure>", 
			       funlang,
                               DMGL_ANSI);
#endif						   

      /* Fix for Hex Offset defect - Dinesh 03/28/02 */
      if ((funname) && (funvalue))
	{
	  if (funhivalue == 0 
	      || (funhivalue 
		  && ((funvalue + (int) (fi->pc-funvalue)) < funhivalue )))
#ifdef UI_OUT
		ui_out_field_fmt (uiout,NULL,"+%#x",(int)(fi->pc-funvalue));
#else
	    printf_filtered ("+%#x", (int) (fi->pc-funvalue));
#endif		
	}
#ifdef HP_IA64
      else if (!(func_addr || funvalue))
	{
	  unw_entry = find_unwind_entry (fi->pc);
	  
	  if (unw_entry && unw_entry->region_start)
	    printf_filtered (" +%#x", 
			     fi->pc - (CORE_ADDR) unw_entry->region_start);
	}
#endif
      }
    }
#ifndef UI_OUT
  wrap_here ("   ");
#endif
  annotate_frame_args ();
 
  if (!(java_debugging && is_java_frame (fi->pc))) {

#ifdef UI_OUT
    ui_out_text (uiout, " (");
#else
    fputs_filtered (" (", gdb_stdout);
#endif
    if (args)
      {
        if (argsprint)
          {
            struct print_args_args args;
            args.fi = fi;
            args.func = func;
            args.stream = gdb_stdout;
#ifdef UI_OUT
            ui_out_list_begin (uiout, "args");
            catch_errors (print_args_stub, &args, "", RETURN_MASK_ALL);
            /* FIXME: args must be a list. If one argument is a string it will
               have " that will not be properly escaped.  */
            ui_out_list_end (uiout);
#else
            catch_errors (print_args_stub, &args, "", RETURN_MASK_ALL);
#endif
            QUIT;
          }
        else
          {
#ifdef UI_OUT
            ui_out_text (uiout, "...");
#else
            fputs_filtered ("...", gdb_stdout);
#endif
          }
      }
#ifdef UI_OUT
    ui_out_text (uiout, ")");
#else
    printf_filtered (")");
#endif
  }
  if (sal.symtab && sal.symtab->filename)
    {
      annotate_frame_source_begin ();
#ifdef UI_OUT
      ui_out_wrap_hint (uiout, "   ");
      ui_out_text (uiout, " at ");
      annotate_frame_source_file ();
      /* Sunil JAGaf49959 07/03/06
	   Display the absolute path name.
      */
      if (!sal.symtab->fullname)
	find_full_path_of (sal.symtab->filename, &sal.symtab->fullname); 
      ui_out_field_string (uiout, "file",
			   annotation_level > 1 ?
			   symtab_to_filename (sal.symtab) :
			   (display_full_path ?
			   sal.symtab->fullname :
			   sal.symtab->filename) );
      annotate_frame_source_file_end ();
      ui_out_text (uiout, ":");
      annotate_frame_source_line ();
      ui_out_field_int (uiout, "line", sal.line);
#else
      if ((funname) && (funvalue))
	{
	  if (funhivalue == 0 
	      || (funhivalue 
		  && ((funvalue + (int) (fi->pc-funvalue)) < funhivalue )))
	    printf_filtered ("+%#x", (int) (fi->pc-funvalue));
	}
      wrap_here ("   ");

      printf_filtered (" at ");
      annotate_frame_source_file ();
      printf_filtered ("%s", annotation_level > 1 ?
		       symtab_to_filename (sal.symtab) :
		       sal.symtab->filename);
      annotate_frame_source_file_end ();
      printf_filtered (":");
      annotate_frame_source_line ();
      printf_filtered ("%d", sal.line);
#endif
      annotate_frame_source_end ();
    }

#ifdef PC_LOAD_SEGMENT
  /* If we couldn't print out function name but if can figure out what
         load segment this pc value is from, at least print out some info
         about its load segment. */
  if (!funname)
    {
      annotate_frame_where ();
#ifdef UI_OUT
      ui_out_wrap_hint (uiout, "  ");
      ui_out_text (uiout, " from ");
      ui_out_field_string (uiout, "from", PC_LOAD_SEGMENT (fi->pc));
#else
      wrap_here ("  ");
      printf_filtered (" from %s", PC_LOAD_SEGMENT (fi->pc));
#endif
    }
#endif /* PC_LOAD_SEGMENT */

#ifdef PC_SOLIB
  if (!funname || (!sal.symtab || !sal.symtab->filename))
    {
      char *lib = PC_SOLIB (fi->pc);
      if (lib)
	{
	  annotate_frame_where ();
#ifdef UI_OUT
	  ui_out_wrap_hint (uiout, "  ");
	  ui_out_text (uiout, " from ");
	  ui_out_field_string (uiout, "from", lib);
#else
	  wrap_here ("  ");
	  printf_filtered (" from %s", lib);
#endif
	}
    }
#endif /* PC_SOLIB */

  if (freename)
    {
      free(funname);
      funname = NULL;
    }

#ifdef UI_OUT
  do_cleanups (list_chain);
  ui_out_text (uiout, "\n");
  do_cleanups (old_chain);
#else
  printf_filtered ("\n");
#endif
}


/* Show or print the frame info.  If this is the tui, it will be shown in 
   the source display */
void
print_frame_info (struct frame_info *fi, register int level, int source, int args)
{
  if (!tui_version)
    print_frame_info_base (fi, level, source, args);
  else
    {
      if (fi && (frame_in_dummy (fi) || fi->signal_handler_caller))
	print_frame_info_base (fi, level, source, args);
      else
	{
	  TUIDO (((TuiOpaqueFuncPtr) tui_vShowFrameInfo, fi));
	}
    }
}

/* Show the frame info.  If this is the tui, it will be shown in 
   the source display otherwise, nothing is done */
void
show_stack_frame (struct frame_info *fi)
{
  TUIDO (((TuiOpaqueFuncPtr) tui_vShowFrameInfo, fi));
}


/* Read a frame specification in whatever the appropriate format is.
   Call error() if the specification is in any way invalid (i.e.
   this function never returns NULL).  */

struct frame_info *
parse_frame_specification (char *frame_exp)
{
  int numargs = 0;
#define	MAXARGS	4
  CORE_ADDR args[MAXARGS] = {0}; /* initialize for compiler warning */

  if (frame_exp)
    {
      char *addr_string, *p;
      struct cleanup *tmp_cleanup;

      while (*frame_exp == ' ')
	frame_exp++;

      while (*frame_exp)
	{
	  if (numargs > MAXARGS)
	    error ("Too many args in frame specification");
	  /* Parse an argument.  */
	  for (p = frame_exp; *p && *p != ' '; p++)
	    ;
	  addr_string = savestring (frame_exp, (int)(p - frame_exp));

	  {
	    tmp_cleanup = make_cleanup (free, addr_string);
	    args[numargs++] = parse_and_eval_address (addr_string);
	    do_cleanups (tmp_cleanup);
	  }

	  /* Skip spaces, move to possible next arg.  */
	  while (*p == ' ')
	    p++;
	  frame_exp = p;
	}
    }

  switch (numargs)
    {
    case 0:
      if (selected_frame == NULL)
	error ("No selected frame.");
      return selected_frame;
      /* NOTREACHED */
    case 1:
      {
	int level = (int) args[0];
	struct frame_info *fid =
	find_relative_frame (get_current_frame (), &level);
	struct frame_info *tfid;

	if (level == 0)
	  /* find_relative_frame was successful */
	  return fid;

	/* If SETUP_ARBITRARY_FRAME is defined, then frame specifications
	   take at least 2 addresses.  It is important to detect this case
	   here so that "frame 100" does not give a confusing error message
	   like "frame specification requires two addresses".  This of course
	   does not solve the "frame 100" problem for machines on which
	   a frame specification can be made with one address.  To solve
	   that, we need a new syntax for a specifying a frame by address.
	   I think the cleanest syntax is $frame(0x45) ($frame(0x23,0x45) for
	   two args, etc.), but people might think that is too much typing,
	   so I guess *0x23,0x45 would be a possible alternative (commas
	   really should be used instead of spaces to delimit; using spaces
	   normally works in an expression).  */
#ifdef SETUP_ARBITRARY_FRAME
	error ("No frame %d", args[0]);
#endif

	/* If (s)he specifies the frame with an address, he deserves what
	   (s)he gets.  Still, give the highest one that matches.  */

	for (fid = get_current_frame ();
	     fid && fid->frame != args[0];
	     fid = get_prev_frame (fid)
#ifdef HP_IA64
             ,fid = mixed_mode_adjust (fid)
#endif
             )
	  ;

	if (fid)
	  while ((tfid = get_prev_frame (fid)) &&
#ifdef HP_IA64
                 (tfid = mixed_mode_adjust (tfid)) &&
#endif
		 (tfid->frame == args[0]))
	    fid = tfid;
       if (fid)
          return fid;
#if defined(GDB_TARGET_IS_HPUX)
        else
          error ("No frame with frame# or frame_address %lld", args[0]);
        break;
#else
	/* We couldn't identify the frame as an existing frame, but
	   perhaps we can create one with a single argument.  */
#endif
      }

    default:
#ifdef SETUP_ARBITRARY_FRAME
      return SETUP_ARBITRARY_FRAME (numargs, args);
#else
      /* Usual case.  Do it here rather than have everyone supply
         a SETUP_ARBITRARY_FRAME that does this.  */
      if (numargs == 1)
	return create_new_frame (args[0], 0, 0);
      error ("Too many args in frame specification");
#endif
      /* NOTREACHED */
    }
  /* NOTREACHED */
}

/* FRAME_ARGS_ADDRESS_CORRECT is just like FRAME_ARGS_ADDRESS except
   that if it is unsure about the answer, it returns 0
   instead of guessing (this happens on the VAX and i960, for example).

   On most machines, we never have to guess about the args address,
   so FRAME_ARGS_ADDRESS{,_CORRECT} are the same.  */
#if !defined (FRAME_ARGS_ADDRESS_CORRECT)
#define FRAME_ARGS_ADDRESS_CORRECT FRAME_ARGS_ADDRESS
#endif

/* Print verbosely the selected frame or the frame at address ADDR.
   This means absolutely all information in the frame is printed.  */

static void
frame_info (char *addr_exp, int from_tty)
{
  struct frame_info *fi;
  struct symtab_and_line sal;
  struct symbol *func;
  struct symtab *s;
  struct frame_info *calling_frame_info;
  int i, count, numregs;
  char *funname = 0;
  enum language funlang = language_unknown;
#ifdef HP_IA64
  CORE_ADDR saved_pc = NULL;
#endif

  if (!target_has_stack)
    error ("No stack.");

  fi = parse_frame_specification (addr_exp);
  if (fi == NULL)
    error ("Invalid frame specified.");

  sal = find_pc_line (fi->pc,
		      fi->next != NULL
		      && !fi->next->signal_handler_caller
		      && !frame_in_dummy (fi->next));
  func = get_frame_function (fi);
  s = find_pc_symtab (fi->pc);
  if (func)
    {
      /* I'd like to use SYMBOL_SOURCE_NAME() here, to display
       * the demangled name that we already have stored in
       * the symbol table, but we stored a version with
       * DMGL_PARAMS turned on, and here we don't want
       * to display parameters. So call the demangler again,
       * with DMGL_ANSI only. RT
       * (Yes, I know that printf_symbol_filtered() will
       * again try to demangle the name on the fly, but
       * the issue is that if cplus_demangle() fails here,
       * it'll fail there too. So we want to catch the failure
       * ("demangled==NULL" case below) here, while we still
       * have our hands on the function symbol.)
       */
      char *demangled;
      funname = SYMBOL_NAME (func);
      funlang = SYMBOL_LANGUAGE (func);
      if (funlang == language_cplus)
	{
	  demangled = cplus_demangle (funname, DMGL_ANSI);
	  /* If the demangler fails, try the demangled name
	   * from the symbol table. This'll have parameters,
	   * but that's preferable to diplaying a mangled name.
	   */
	  if (demangled == NULL)
	    funname = SYMBOL_SOURCE_NAME (func);
	}
    }
  else
    {
      register struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (fi->pc);
      if (msymbol != NULL)
	{
	  funname = SYMBOL_NAME (msymbol);
	  funlang = SYMBOL_LANGUAGE (msymbol);
	}
    }
  calling_frame_info = get_prev_frame (fi);

#ifdef HP_IA64
  calling_frame_info = mixed_mode_adjust (calling_frame_info);
#endif

  if (!addr_exp && selected_frame_level >= 0)
    {
#ifdef HP_IA64
      if (fi->pa_save_state_ptr)
        printf_filtered ("Stack level %d, PA frame at ", selected_frame_level);
      else
#endif
        printf_filtered ("Stack level %d, frame at ", selected_frame_level);
      print_address_numeric (fi->frame, 1, gdb_stdout);
      printf_filtered (":\n");
    }
  else
    {
#ifdef HP_IA64
      if (fi->pa_save_state_ptr)
        printf_filtered ("PA ");
#endif
      printf_filtered ("Stack frame at ");
      print_address_numeric (fi->frame, 1, gdb_stdout);
      printf_filtered (":\n");
    }
#ifdef HP_IA64
  if (fi->pa_save_state_ptr)
    printf_filtered (" pcoqh = ");
  else
#endif
    printf_filtered (" %s = ", REGISTER_NAME (PC_REGNUM));
  print_address_numeric (fi->pc, 1, gdb_stdout);

  wrap_here ("   ");
  if (funname)
    {
      printf_filtered (" in ");
      fprintf_symbol_filtered (gdb_stdout, funname, funlang,
			       DMGL_ANSI | DMGL_PARAMS);
    }
  wrap_here ("   ");
  if (sal.symtab)
    printf_filtered (" (%s:%d)", sal.symtab->filename, sal.line);
  puts_filtered ("; ");
  wrap_here ("    ");
  
#ifdef HP_IA64
  saved_pc = FRAME_SAVED_PC (fi);
  if (   inferior_aries_text_start
      && inferior_aries_text_end
      && in_mixed_mode_pa_lib (saved_pc, NULL))
    {
      printf_filtered ("saved pcoqh ");
    }
  else
    {
      printf_filtered ("saved %s ", REGISTER_NAME (PC_REGNUM));
    }
  print_address_numeric (saved_pc, 1, gdb_stdout);
#else
  printf_filtered ("saved %s ", REGISTER_NAME (PC_REGNUM));
  print_address_numeric (FRAME_SAVED_PC (fi), 1, gdb_stdout);
#endif
  printf_filtered ("\n");

  {
    int frameless;
    frameless = FRAMELESS_FUNCTION_INVOCATION (fi);
    if (frameless)
      printf_filtered (" (FRAMELESS),");
  }

  if (calling_frame_info)
    {
      printf_filtered (" called by frame at ");
      print_address_numeric (calling_frame_info->frame, 1, gdb_stdout);
    }
  if (fi->next && calling_frame_info)
    puts_filtered (",");
  wrap_here ("   ");
  if (fi->next)
    {
      printf_filtered (" caller of frame at ");
      print_address_numeric (fi->next->frame, 1, gdb_stdout);
    }
  if (fi->next || calling_frame_info)
    puts_filtered ("\n");
  if (s)
    printf_filtered (" source language %s.\n", language_str (s->language));

#ifdef PRINT_EXTRA_FRAME_INFO
  PRINT_EXTRA_FRAME_INFO (fi);
#endif

  {
    CORE_ADDR arg_list = NULL;
    /* Number of args for this frame, or -1 if unknown.  */
    int numargs = 0;

    /* Address of the argument list for this frame, or 0.  */
#ifdef HP_IA64
    if (fi->pa_save_state_ptr)
      {
        arg_list = (is_swizzled)? fi->frame: fi->ap;
      }
    else
#endif
    arg_list = FRAME_ARGS_ADDRESS_CORRECT (fi);


    if (arg_list == 0)
      printf_filtered (" Arglist at unknown address.\n");
    else
      {
	printf_filtered (" Arglist at ");
	print_address_numeric (arg_list, 1, gdb_stdout);
	printf_filtered (",");

	numargs = FRAME_NUM_ARGS (fi);
	if (numargs < 0)
	  puts_filtered (" args: ");
	else if (numargs == 0)
	  puts_filtered (" no args.");
	else if (numargs == 1)
	  puts_filtered (" 1 arg: ");
	else
	  printf_filtered (" %d args: ", numargs);
	print_frame_args (func, fi, numargs, gdb_stdout);
	puts_filtered ("\n");
      }
  }
  {
    CORE_ADDR arg_list = NULL;
    /* Address of the local variables for this frame, or 0.  */
#ifdef HP_IA64
    if (fi->pa_save_state_ptr)
      {
        arg_list = fi->frame;
      }
    else
#endif
    arg_list = FRAME_LOCALS_ADDRESS (fi);

    if (arg_list == 0)
      printf_filtered (" Locals at unknown address,");
    else
      {
	printf_filtered (" Locals at ");
	print_address_numeric (arg_list, 1, gdb_stdout);
	printf_filtered (",");
      }
  }

#ifdef HP_IA64
  if (fi->pa_save_state_ptr)
    mixed_mode_get_frame_saved_regs (fi, NULL);
  else
#endif
    FRAME_INIT_SAVED_REGS (fi);
  if (fi->saved_regs != NULL)
    {
      /* The sp is special; what's returned isn't the save address, but
         actually the value of the previous frame's sp.  */
      printf_filtered (" Previous frame's sp is ");
#ifdef HP_IA64
      if (fi->pa_save_state_ptr)
        {
           print_address_numeric (fi->saved_regs[PA_SP_REGNUM], 1, gdb_stdout);
        }
      else
#endif
        print_address_numeric (fi->saved_regs[SP_REGNUM], 1, gdb_stdout);
      printf_filtered ("\n");
      count = 0;
#ifdef HP_IA64
      numregs = (fi->pa_save_state_ptr)? (is_swizzled? PA32_NUM_REGS:
                                                       PA64_NUM_REGS):
                                          ARCH_NUM_REGS;
#else
      numregs = ARCH_NUM_REGS;
#endif
      for (i = 0; i < numregs; i++)
#ifdef HP_IA64
	if (fi->saved_regs[i] &&
            i != (fi->pa_save_state_ptr? PA_SP_REGNUM: SP_REGNUM))
#else
	if (fi->saved_regs[i] && i != SP_REGNUM)
#endif
	  {
	    if (count == 0)
	      puts_filtered (" Saved registers:\n ");
	    else
	      puts_filtered (",");
	    wrap_here (" ");
#ifdef HP_IA64
            if (fi->pa_save_state_ptr)
              {
                printf_filtered (" %s at ",
                  mixed_mode_legacy_register_name (i));
              }
            else
#endif
	      printf_filtered (" %s at ", REGISTER_NAME (i));
	    print_address_numeric (fi->saved_regs[i], 1, gdb_stdout);
	    count++;
	  }
      if (count)
	puts_filtered ("\n");
    }
  else
    {
      /* We could get some information about saved registers by
         calling get_saved_register on each register.  Which info goes
         with which frame is necessarily lost, however, and I suspect
         that the users don't care whether they get the info.  */
      puts_filtered ("\n");
    }

  /* If this is a Java frame, print out the Java specific frame info. */
  if (java_debugging && is_java_frame (fi->pc))
    {
      char *java_frame_info;
      char *java_locals;
      extern char* get_java_frame_locals_info (struct basic_frame_info *frame);
      struct basic_frame_info bfi;

      /* Initialize bfi being sent to junwindlib */
      bfi.pc = fi->pc;
      bfi.fp = fi->frame;
#ifdef HP_IA64
      bfi.bsp = fi->rse_fp;
      bfi.cfm = fi->cfm;
      bfi.java_ptr = 0;
#endif 

      if (fi->next)
        bfi.sp = fi->next->frame;
      else
        bfi.sp = read_sp ();

      /* Call libjunwind to get the Java frame info str. If it is not null,
         print it out and then free the str. */
      java_frame_info = get_java_frame_info (&bfi);
      if (java_frame_info)
        {
          puts_filtered ("\n");
          puts_filtered (java_frame_info);
          wrap_here (" ");
     
          /* free the str after use */
          free (java_frame_info);
        }

      /* Call libjunwind to get the Java locals info str. If it is not null,
         print it out and then free the str. */
      java_locals = get_java_frame_locals_info (&bfi);
      if (java_locals)
        {
          puts_filtered (java_locals);
          puts_filtered ("\n");
          wrap_here (" ");

          /* free the str after use */
          free (java_locals);
        }
    }
  set_currently_printing_frame();
}

#ifdef HP_IA64

void
populate_aries_pa2ia_export_stub_addr ()
{
  struct minimal_symbol *aries_pa2ia_export_stub_minsym = NULL;

  /* jini: mixed mode debug support: The routine aries_pa2ia_export_stub is a
     special aries routine placed in the data segment. This has no unwind
     entries. Hence, gdb treats this as a special routine and skips this frame
     while unwinding - directly going to the caller. 
     Populate the aries_pa2ia_export_stub start address so that the RP can be
     compared against this while unwinding.
   */

  if (inferior_aries_text_start && !aries_pa2ia_export_stub_addr)
    {
       struct minimal_symbol *aries_pa2ia_export_stub_minsym = NULL;
       struct objfile        *objfile = NULL;

       if (!aries_objfile)
         {
           ALL_OBJFILES (objfile)
             {
                if (   objfile->text_low == inferior_aries_text_start
                     && objfile->text_high == inferior_aries_text_end) 
                  {
                    aries_objfile = objfile;
                    break;
                  }
              }
           }

       aries_pa2ia_export_stub_minsym = lookup_minimal_symbol (
                                        "aries_pa2ia_export_stub",
                                         0,
                                         aries_objfile);
       aries_pa2ia_export_stub_addr =
          SYMBOL_VALUE_ADDRESS (aries_pa2ia_export_stub_minsym);

       if (ptr_aries_get_pa2ia_export_stub_size)
         aries_pa2ia_export_stub_size = (*ptr_aries_get_pa2ia_export_stub_size)();
   }
}


static int
mixed_mode_init_aries ()
{
  struct objfile        *objfile = NULL;
  struct minimal_symbol *aries_tp_minsym = NULL;
  struct minimal_symbol *thrsp_sym = NULL;
  off_t                  sym_tls_offset;
  CORE_ADDR              tls_addr;
  int                    status;
  char                   buf[MAX_REGISTER_RAW_SIZE];

  if (!aries_objfile)
    {
      ALL_OBJFILES (objfile)
        {
          if (   objfile->text_low == inferior_aries_text_start
              && objfile->text_high == inferior_aries_text_end) 
            {
              aries_objfile = objfile;
              break;
            }
        }
     }
  aries_tp_minsym = lookup_minimal_symbol_text (aries_tp_str,
                                                0, 
                                                aries_objfile);
  assert (   aries_tp_minsym->type == mst_tls
          || aries_tp_minsym->type == mst_file_tls);

  thrsp_sym =  lookup_minimal_symbol ("__thread_specific_seg",
                                      0,
                                      aries_objfile);
  sym_tls_offset =   SYMBOL_VALUE_ADDRESS (aries_tp_minsym)
                   - SYMBOL_VALUE_ADDRESS (thrsp_sym);

  tls_addr = ((struct load_module_desc*) ((obj_private_data_t *)
              aries_objfile->obj_private)->lmdp)->tls_start_addr;
  /* Libaries*.so is built with dynamic TLS. */
  assert (tls_addr == (uint64_t) -1);

  int tls_index = (int)((struct load_module_desc*) ((obj_private_data_t *)
           aries_objfile->obj_private)->lmdp)->tls_index;

  aries_tp_addr = SYMBOL_VALUE_ADDRESS (aries_tp_minsym);
  if (tls_index < 0)
    {
      CORE_ADDR tp;
          
      get_saved_register (buf, NULL, NULL, selected_frame, 
                          TP_REGNUM, NULL);
      tp = extract_address (buf, REGISTER_RAW_SIZE (TP_REGNUM));

      aries_tp_addr = tp + tls_index * sizeof(CORE_ADDR);
    }
  else
    {
      aries_tp_addr = read_memory_unsigned_integer (aries_tp_addr,
                            TARGET_PTR_BIT / TARGET_CHAR_BIT);
      aries_tp_addr = aries_tp_addr + tls_index * sizeof (CORE_ADDR);
    }
  aries_tp_addr = read_memory_unsigned_integer (aries_tp_addr,
            TARGET_PTR_BIT / TARGET_CHAR_BIT);
  aries_tp_addr += sym_tls_offset;

#ifdef SWIZZLE
  /* libmxndbg requires that this address be swizzled always. */
  aries_tp_addr = SWIZZLE (aries_tp_addr);
#endif
  if (NULL == aries_tp_addr)
    {
       warning ("Unable to obtain the value of 'aries_tp'. This could be\n"
                "because the emulation has not commenced for this thread.\n"
                "No mixed mode unwinding would be done for this thread.");
       return -1;
    }
  /* Call the libaries API to obtain the number of contexts. */
  status = (*ptr_aries_get_nbr_contexts) (aries_tp_addr,
                                          &nbr_contexts,
                                          is_swizzled);
  if (0 != status)
    {
      mixed_mode_debug_aries = true;
      error ("Libaries error. Unable to get the number of contexts. "
             "The PA stack frames will not be displayed");
    }

  __mixed_mode_callback_refs[1] = (void*) 1L;

  return 0;
}

/* jini: Mixed mode corefile debugging: Skip all contiguous aries frames
   to obtain the next non aries IA frame. */
static struct frame_info *
get_non_aries_ia_frame (struct frame_info  *trailing)
{
  struct frame_info *aries_temp_frame = NULL;
  struct frame_info *next_aries_temp_frame = NULL;
  struct frame_info *non_aries_ia_frame = NULL;

  aries_temp_frame = trailing;
  while (    inferior_aries_text_start <= aries_temp_frame->pc
         &&  aries_temp_frame->pc < inferior_aries_text_end)
    {
      /* We are currently not keeping the aries frames. Discard 
       * immediately. TODO: Check on how can this be optimized by not
       * filling in all the details when the frame gets created. */
      if (next_aries_temp_frame)
        {
          next_aries_temp_frame = NULL;
        }
      next_aries_temp_frame = aries_temp_frame;
      aries_temp_frame = get_prev_frame (next_aries_temp_frame);
    }
  /* At this point, next_aries_temp_frame should be the bottommost
     aries frame in a contiguous list of aries frames and aries_temp_frame
     should be a non-aries ipf frame. */
  non_aries_ia_frame = aries_temp_frame;
  if (next_aries_temp_frame)
    {
      aries_border_frame = next_aries_temp_frame;
    }
  return non_aries_ia_frame;
}


/* Input parameter:
   trailing: If trailing represents an inferior aries frame, then,
             - call get_non_aries_ia_frame() to unwind through the contiguous
               aries frames and get to the non aries IA frame on the IA stack.
             - Set mixed_mode_pa_unwind to true to denote further PA unwinding
             - call the aries API to obtain information regarding the PA frame
             - create the PA frame with the information obtained.

           : If trailing is NULL, it denotes that while unwinding the PA frames,
             we have encountered a PA to PA BOR call which inturn causes the
             rp of a PA frame to point to an aries frame. This means that we
             had done the aries unwinding before. We just need to get the right
             frame contents using the aries API now and create a new PA frame.
*/
           
            
struct frame_info *
get_mixed_mode_pa_frame (struct frame_info  *trailing)
{
  pa_save_state_t        pa_save_state;
  CORE_ADDR              pa_lib_fp = NULL;
  CORE_ADDR              pa_lib_pc = NULL;
  struct frame_info     *non_aries_ia_frame = NULL;
  int                    status;

  if (    !mixed_mode_debug_aries
       && (   !trailing
           || (   inferior_aries_text_start <= trailing->pc
               && trailing->pc < inferior_aries_text_end)))
    {
      if (trailing)
        {
          struct minimal_symbol *msym = NULL;

          /* This is the topmost frame. If this frame belongs to any aries
             signal handler of any of the routines called by the aries
             signal handlers, then the libaries frames rather than the PA
             frames have to be displayed. For this, turn mixed_mode_debug_aries
             to on. The libaries routine 'aries_in_signal_handler' is used
             to figure out if this routine is an aries signal handler or its
             callee frame. */

          if (NULL == trailing->next)
            {
              msym = lookup_minimal_symbol_by_pc (trailing->pc);
              if (msym)
                {
                  int in_handler = 0;
                  status = (*ptr_aries_in_signal_handler) (aries_tp_addr,
                                                           SYMBOL_NAME (msym),
                                                           &in_handler,
                                                           is_swizzled);
                  if (0 != status)
                    {
                      mixed_mode_debug_aries = true;
                      error ("Libaries error in calling "
                             "'aries_in_signal_handler'. The PA stack frames\n"
                             "will not be displayed.");
                    }
                  if (in_handler)
                    {
                      printf_filtered ("Libaries signal handler crash. The PA "
                                       "stack frames will not be displayed.\n");
                      mixed_mode_debug_aries = true;
                      return trailing;
                    }
                 }
              }
      
          non_aries_ia_frame = get_non_aries_ia_frame (trailing);

          /* Obtain the PA frame and set trailing to the PA frame. */
          mixed_mode_pa_unwind = true;
          if ((-2 == context_nbr) || !aries_tp_addr)
            {
              status = mixed_mode_init_aries ();
              if (0 != status)
                {
                  /* This could happen if 'aries_tp' is NULL. This would mean
                     that there are no PA frames for this thread to display.
                     Turn mixed_mode_pa_unwind to off. */
                  mixed_mode_pa_unwind = false;
                  return trailing;
                }

              context_nbr = nbr_contexts - 1;
            }
        }
      else
        {
          mixed_mode_pa_unwind = true;
        }

      /* Call the libaries API to obtain the values of the aries registers. */
      status = (*ptr_aries_get_save_state) (aries_tp_addr,
                                            context_nbr,
                                            &pa_save_state,
                                            is_swizzled);
      if (0 != status)
        {
          mixed_mode_debug_aries = true;
          error ("Libaries error in calling 'aries_get_save_state'. "
                 "The PA stack frames\nwill not be displayed.");
        }
      
      if (is_swizzled)
        {
          pa_lib_fp = (CORE_ADDR) SWIZZLE (pa_save_state.ss_narrow.ss_gr3);
          pa_lib_pc = (CORE_ADDR) SWIZZLE (pa_save_state.ss_narrow.ss_pcoq_head);
        }
      else
        {
          pa_lib_fp = (CORE_ADDR) pa_save_state.ss_wide.ss_64.ss_gr3;
          pa_lib_pc = (CORE_ADDR) pa_save_state.ss_wide.ss_64.ss_pcoq_head;
        }

      /* Remove the encoding in the last 2 bits. */
      pa_lib_pc &= ~0x3;
      trailing = create_new_frame (pa_lib_fp, pa_lib_pc,
                                   (void *) &pa_save_state);
      context_nbr--;
    }
  trailing->non_aries_ia_frame = non_aries_ia_frame;
  return trailing;
}

struct frame_info *
mixed_mode_adjust (struct frame_info *fi)
{
  if (!fi || target_has_execution)
    return fi;

  if (   mixed_mode_pa_unwind && !fi->pa_save_state_ptr
      && in_mixed_mode_pa_lib (fi->pc, NULL))
    {
      pa_save_state_t pa_save_state;
      int             status;
      /* Call the libaries API to obtain the values of the aries registers. */
      status = (*ptr_aries_get_save_state) (aries_tp_addr,
                                            context_nbr,
                                            &pa_save_state,
                                            is_swizzled);
      if (0 != status)
        {
          mixed_mode_debug_aries = true;
          error ("Libaries error in calling 'aries_get_save_state'. "
                 "The PA stack frame(s)\nwill not be displayed.");
        }
      context_nbr--;
      fi->pa_save_state_ptr = (pa_save_state_t *)
             frame_obstack_alloc (sizeof (pa_save_state_t));
      memcpy (fi->pa_save_state_ptr,
              &pa_save_state,
              sizeof (pa_save_state_t));
    } 
  if (  !mixed_mode_debug_aries
       && inferior_aries_text_start <= fi->pc
       && fi->pc < inferior_aries_text_end )
    {
      struct frame_info *fi_next = fi->next;
      mixed_mode_pa_unwind = false;
      fi = get_mixed_mode_pa_frame (fi);
      fi_next->prev = fi;
      fi->next = fi_next;
    }
  return fi;
}
#endif


/* Print briefly all stack frames or just the innermost COUNT frames.  */

void backtrace_command_1 (char *count_exp, int show_locals,
				 int from_tty);
void
backtrace_command_1 (char *count_exp, int show_locals, int from_tty)
{
  struct frame_info *fi;
  register int count;
  register int i;
  register struct frame_info *trailing;
  register int trailing_level;
  user_not_warned_on_incomplete_bt = 1; /* JAGad92223 - set to 1 if call from backtrace_command */
  /* Used for batch mode thread check to determine if we have to skip
     printing a frame. We need to skip the top few frames belonging to librtc
     and the libpthread libraries while displaying the backtrace at a thread
     error event. Don't skip the libpthread frames that might appear in between
     (e.g., __pthread_bound_body()). So, if we have displayed at least one
     frame, stop skipping frames. */
  bool frame_displayed = 0;

//For JAGaf13527
#ifndef HP_IA64
  dummy_frame_read = dummyread; 
#endif  

  if (!target_has_stack)
    error ("No stack.");

#ifdef HP_IA64
  populate_aries_pa2ia_export_stub_addr();
#endif

  /* The following code must do two things.  First, it must
     set the variable TRAILING to the frame from which we should start
     printing.  Second, it must set the variable count to the number
     of frames which we should print, or -1 if all of them.  */
  trailing = get_current_frame ();

  /* The target can be in a state where there is no valid frames
     (e.g., just connected). */
  if (trailing == NULL)
    error ("No stack.");

  trailing_level = 0;
  if (count_exp)
    {
      count = (int) parse_and_eval_address (count_exp);
      if (count < 0)
	{
	  struct frame_info *current;

	  count = -count;

	  current = trailing;
	  while (current && count--)
	    {
	      QUIT;
	      current = get_prev_frame (current);
#ifdef HP_IA64
              current = mixed_mode_adjust (current);
#endif
	    }

	  /* Will stop when CURRENT reaches the top of the stack.  TRAILING
	     will be COUNT below it.  */
	  while (current)
	    {
	      QUIT;
	      trailing = get_prev_frame (trailing);
#ifdef HP_IA64
              trailing = mixed_mode_adjust (trailing);
#endif
	      current = get_prev_frame (current);
#ifdef HP_IA64
              current = mixed_mode_adjust (current);
#endif
	      trailing_level++;
	    }

	  count = -1;
	}
    }
  else
    count = -1;

  if (info_verbose)
    {
      struct partial_symtab *ps;

      /* Read in symbols for all of the frames.  Need to do this in
         a separate pass so that "Reading in symbols for xxx" messages
         don't screw up the appearance of the backtrace.  Also
         if people have strong opinions against reading symbols for
         backtrace this may have to be an option.  */
      i = count;
      for (fi = trailing;
	   fi != NULL && i--;
	   fi = get_prev_frame (fi)
#ifdef HP_IA64
           ,fi = mixed_mode_adjust (fi)
#endif
          )
	{
	  QUIT;
          fi->pc = SWIZZLE(fi->pc);
	  ps = find_pc_psymtab (fi->pc);
	  if (ps)
	    PSYMTAB_TO_SYMTAB (ps);	/* Force syms to come in */
	}
    }

  for (i = 0, fi = trailing;
       fi && count--;
       i++, fi = get_prev_frame (fi))
    {
      QUIT;
      fi->pc = SWIZZLE(fi->pc);
#ifdef HP_IA64
      if (i)
        {
          fi = mixed_mode_adjust (fi);
        }
#endif

      /* Don't use print_stack_frame; if an error() occurs it probably
         means further attempts to backtrace would fail (on the other
         hand, perhaps the code does or could be fixed to make sure
         the frame->prev field gets set to NULL in that case).  */
#ifdef COVARIANT_SUPPORT
      /* 		Co-variant return type
         If the current frame "pc" happens to be in the outbound or
         inbound outbound thunk, skip the current frame in order
         to avoid printing of thunks, an internal compiler 
         generated function
       */
      if (IS_PC_IN_COVARIANT_THUNK (fi->pc)) 
        {
          fi = get_prev_frame (fi);
        }
#endif
      /* Print the frame only if:
         * This is not a batch mode thread check case.
           OR
         * This is a batch mode thread check case and at least one frame
           has been displayed and this is not an internal frame belonging
           to the librtc or libpthread tracer libraries.
       */
      if (!(batch_rtc && trace_threads_in_this_run && !frame_displayed &&
           (  (fi->pc >= librtc_start && fi->pc < librtc_end)
            ||(fi->pc >= libpthread_start && fi->pc < libpthread_end))))
        {
          if (batch_rtc && trace_threads_in_this_run && !frame_displayed)
            {
              printf_filtered (
                "\n<Thread debugging (internal) frames removed.>\n");
            }
          print_frame_info_base (fi, trailing_level + i, 0, 1);
          if (show_locals)
            print_frame_local_vars (fi, 1, gdb_stdout);
          frame_displayed = true;
        }
       else
        {
          /* This is the batch mode thread check case and we have skipped
             printing this frame since this is an internal frame belonging to
             librtc or the libpthread tracer library. Increment count so that
             skipping this internal frame does not affect the frame-count
             specified by the user.
          */
          count++;
        }
    }

  if (batch_rtc && trace_threads_in_this_run && !frame_displayed)
    {
      printf_filtered ("No stack frames. This might be since pthread_exit() "
                       "has been called implicitly.\n\n");
    }

  /* If we've stopped before the end, mention that.  */
  if (fi && from_tty)
    printf_filtered ("(More stack frames follow...)\n");

  set_currently_printing_frame();
}

static void
backtrace_command (char *arg, int from_tty)
{
  struct cleanup *old_chain = (struct cleanup *) NULL;
  char **argv = (char **) NULL;
  int argIndicatingFullTrace = (-1), totArgLen = 0, argc = 0;
  char *argPtr = arg;

  if (arg != (char *) NULL)
    {
      int i;

      argv = buildargv (arg);
      old_chain = make_cleanup_freeargv (argv);
      argc = 0;
      for (i = 0; (argv[i] != (char *) NULL); i++)
	{
	  unsigned int j;

	  for (j = 0; (j < strlen (argv[i])); j++)
	    argv[i][j] = tolower (argv[i][j]);

	  if (argIndicatingFullTrace < 0 && subset_compare (argv[i], "full"))
	    argIndicatingFullTrace = argc;
	  else
	    {
	      argc++;
	      totArgLen += strlen (argv[i]);
	    }
	}
      totArgLen += argc;
      if (argIndicatingFullTrace >= 0)
	{
	  if (totArgLen > 0)
	    {
	      argPtr = (char *) xmalloc (totArgLen + 1);
	      if (!argPtr)
		nomem (0);
	      else
		{
		  memset (argPtr, 0, totArgLen + 1);
		  for (i = 0; (i < (argc + 1)); i++)
		    {
		      if (i != argIndicatingFullTrace)
			{
			  strcat (argPtr, argv[i]);
			  strcat (argPtr, " ");
			}
		    }
		}
	    }
	  else
	    argPtr = (char *) NULL;
	}
    }

  backtrace_command_1 (argPtr, (argIndicatingFullTrace >= 0), from_tty);

  if (argIndicatingFullTrace >= 0 && totArgLen > 0)
    free (argPtr);

  if (old_chain)
    do_cleanups (old_chain);
}

static void backtrace_full_command (char *arg, int from_tty);
static void
backtrace_full_command (char *arg, int from_tty)
{
  backtrace_command_1 (arg, 1, from_tty);
}

/* RM: Command added to allow Java team to backtrace green
 *   threads. Probably fragile.
 */
static void
backtrace_other_thread_command (char *arg, int from_tty)
{
  struct cleanup    *old_chain = (struct cleanup *)NULL;
  char              **argv = (char **)NULL;
  CORE_ADDR         new_sp = 0; /* initialize for compiler warning */
  CORE_ADDR         new_pc = 0; /* initialize for compiler warning */
  CORE_ADDR         new_flags;
  
  if (arg != (char *)NULL)
    {
      argv = buildargv(arg);
      old_chain = make_cleanup((make_cleanup_ftype *) freeargv, (char *)argv);

      if (argv[0] != (char *)NULL)
        new_sp = parse_and_eval_address (argv[0]);
      if (argv[1] != (char *)NULL)
        new_pc = parse_and_eval_address (argv[1]);
#ifdef HP_IA64
      if (   inferior_aries_text_start
          && inferior_aries_text_end
          && in_mixed_mode_pa_lib (new_pc, NULL)) 
        {
          printf_filtered ("backtrace_other_thread is currently not supported "
                           "for PA frames.\n");
          if (old_chain)
            do_cleanups(old_chain);
          return;
        }

      if (argv[2] != (char *)NULL)
        backtrace_other_thread_bsp = parse_and_eval_address (argv[2]);
#endif
    }

  if (   (new_sp == (CORE_ADDR) (long) -1) || (new_pc == (CORE_ADDR) (long) -1)
#ifdef HP_IA64
      || (backtrace_other_thread_bsp == (CORE_ADDR) (long) -1)
#endif
     )
    {
      backtrace_other_thread_bsp = (CORE_ADDR) (long) -1;
      printf("Usage: backtrace_other_thread SP PC"
#ifdef HP_IA64
	     " BSP"
#endif
             "\n");
    }
  else
    {
      supply_register(SP_REGNUM, (char*)&new_sp);
      supply_register(PC_REGNUM, (char*)&new_pc);      
#ifdef GDB_TARGET_IS_HPPA
      new_flags = read_register(FLAGS_REGNUM);
      new_flags &= (~0x2);
      supply_register(FLAGS_REGNUM, (char*)&new_flags);      
#endif
      flush_cached_frames ();
      backtrace_command_1 (NULL, 0, from_tty);
      selected_frame = get_current_frame();
  }
  
  if (old_chain)
    do_cleanups(old_chain);
}



/* Print the local variables of a block B active in FRAME.
   Return 1 if any variables were printed; 0 otherwise.  */

static int
print_block_frame_locals (struct block *b, register struct frame_info *fi, int num_tabs,
			  register struct ui_file *stream)
{
  int nsyms;
  register int i, j;
  register struct symbol *sym;
  register int values_printed = 0;

  nsyms = BLOCK_NSYMS (b);
  for (i = 0; i < nsyms; i++)
    {
      sym = BLOCK_SYM (b, i);

#ifdef INLINE_SUPPORT
      if (sym->is_argument == 1) /* don't print args */
        continue;
#endif

      switch (SYMBOL_CLASS (sym))
	{
	case LOC_LOCAL:
	case LOC_REGISTER:
	case LOC_STATIC:
	case LOC_BASEREG:
	case LOC_COMPUTED:

	  values_printed = 1;
	  for (j = 0; j < num_tabs; j++)
	    fputs_filtered ("\t", stream);
	  fputs_filtered (SYMBOL_SOURCE_NAME (sym), stream);
	  fputs_filtered (" = ", stream);

	  print_variable_value (sym, fi, stream);

#ifdef HP_IA64
	  fputs_filtered (" ", stream);
	  print_hp_symbol_modifiability (sym, stream);
#endif /* HP_IA64 */
	  fprintf_filtered (stream, "\n");
	  break;

	default:
	  /* Ignore symbols which are not locals.  */
	  break;
	}
    }
  return values_printed;
}

#ifndef TARGET_CATCH_INFO
/* Same, but print labels.  */

static int
print_block_frame_labels (struct block *b, int *have_default, register struct ui_file *stream)
{
  int nsyms;
  register int i;
  register struct symbol *sym;
  register int values_printed = 0;

  nsyms = BLOCK_NSYMS (b);

  for (i = 0; i < nsyms; i++)
    {
      sym = BLOCK_SYM (b, i);

      if (STREQ (SYMBOL_NAME (sym), "default"))
	{
	  if (*have_default)
	    continue;
	  *have_default = 1;
	}
      if (SYMBOL_CLASS (sym) == LOC_LABEL)
	{
	  struct symtab_and_line sal;
	  sal = find_pc_line (SYMBOL_VALUE_ADDRESS (sym), 0);
	  values_printed = 1;
	  fputs_filtered (SYMBOL_SOURCE_NAME (sym), stream);
	  if (addressprint)
	    {
	      fprintf_filtered (stream, " ");
	      print_address_numeric (SYMBOL_VALUE_ADDRESS (sym), 1, stream);
	    }
	  fprintf_filtered (stream, " in file %s, line %d\n",
			    sal.symtab->filename, sal.line);
	}
    }
  return values_printed;
}
#endif /* !TARGET_CATCH_INFO */

/* Print on STREAM all the local variables in frame FRAME,
   including all the blocks active in that frame
   at its current pc.

   Returns 1 if the job was done,
   or 0 if nothing was printed because we have no info
   on the function running in FRAME.  */

static void
print_frame_local_vars (register struct frame_info *fi,
			register int num_tabs, register struct ui_file *stream)
{
  register struct block *block = get_frame_block (fi);
  register int values_printed = 0;

  if (block == 0)
    {
      fprintf_filtered (stream, "No symbol table info available.\n");
      return;
    }

  while (block != 0)
    {
      if (print_block_frame_locals (block, fi, num_tabs, stream))
	values_printed = 1;
      /* After handling the function's top-level block, stop.  Don't continue
       * to its superblock, the block of per-file symbols. 
       */
      /* If the block belongs to an entry point rather than a function, then 
       * we need to look at the superblock to get the locals, because all 
       * entry points are children of the function and the locals/parameters 
       * are also children of the function.  Valid only for multiple entry
       * points in fortran and c++.		- Bharath, April 16th, 2003.
       */
      if (BLOCK_FUNCTION (block) &&
  	  !(block->function->type->flags & TYPE_FLAG_ENTRY_POINT))
	break;

      /* Check to see if the start address of the block is the start of an
       * inlined instance. If it is, then current  block defines the
       * scope of an inlined function
       */

      if (block->is_inlined == 1 ) /* start of an inlined function */
        break;

      block = BLOCK_SUPERBLOCK (block);
    }

  if (!values_printed)
    {
      fprintf_filtered (stream, "No locals.\n");
    }
}

#ifndef TARGET_CATCH_INFO
/* Same, but print labels.  */

static void
print_frame_label_vars (register struct frame_info *fi, int this_level_only,
			register struct ui_file *stream)
{
  register struct blockvector *bl;
  register struct block *block = get_frame_block (fi);
  register int values_printed = 0;
  int index, have_default = 0;
  char *blocks_printed;
  CORE_ADDR pc = fi->pc;

  if (block == 0)
    {
      fprintf_filtered (stream, "No symbol table info available.\n");
      return;
    }

  bl = blockvector_for_pc (BLOCK_END (block) - 4, &index);
  blocks_printed = (char *) alloca (BLOCKVECTOR_NBLOCKS (bl) * sizeof (char));
  memset (blocks_printed, 0, BLOCKVECTOR_NBLOCKS (bl) * sizeof (char));

  while (block != 0)
    {
      CORE_ADDR end = BLOCK_END (block) - 4;
      int last_index;

      if (bl != blockvector_for_pc (end, &index))
	error ("blockvector blotch");
      if (BLOCKVECTOR_BLOCK (bl, index) != block)
	error ("blockvector botch");
      last_index = BLOCKVECTOR_NBLOCKS (bl);
      index += 1;

      /* Don't print out blocks that have gone by.  */
      while (index < last_index
	     && BLOCK_END (BLOCKVECTOR_BLOCK (bl, index)) < pc)
	index++;

      while (index < last_index
	     && BLOCK_END (BLOCKVECTOR_BLOCK (bl, index)) < end)
	{
	  if (blocks_printed[index] == 0)
	    {
	      if (print_block_frame_labels (BLOCKVECTOR_BLOCK (bl, index), &have_default, stream))
		values_printed = 1;
	      blocks_printed[index] = 1;
	    }
	  index++;
	}
      if (have_default)
	return;
      if (values_printed && this_level_only)
	return;

      /* After handling the function's top-level block, stop.
         Don't continue to its superblock, the block of
         per-file symbols.  */
      if (BLOCK_FUNCTION (block))
	break;
      block = BLOCK_SUPERBLOCK (block);
    }

  if (!values_printed && !this_level_only)
    {
      fprintf_filtered (stream, "No catches.\n");
    }
}
#endif

/* ARGSUSED */
void
locals_info (char *args, int from_tty)
{
  if (!selected_frame)
    error ("No frame selected.");
  print_frame_local_vars (selected_frame, 0, gdb_stdout);
}

/* Srikanth, set print object on interfers with out_no_values
   Just toggle it internally. Reported by Intel. Jul 16th 2002.
*/
static int old_objectprint;

static void
no_values_cleanup ()
{
  objectprint = old_objectprint;
  dont_print_value = 0;
}

/* ARGSUSED */
void
locals_info_no_values (char *args, int from_tty)
{
  register struct cleanup *old_chain;

  if (!selected_frame)
    error ("No frame selected.");
  dont_print_value = 1;
  old_objectprint = objectprint;
  objectprint = 0;
  /* Srikanth, use cleanups so in case of error, we do clean stuff */
  old_chain = make_cleanup ((make_cleanup_ftype *) no_values_cleanup, 0);
  print_frame_local_vars (selected_frame, 0, gdb_stdout);
  do_cleanups (old_chain);
}

static void
catch_info (char *arg, int from_tty)
{
  struct symtab_and_line *sal;
#ifdef TARGET_CATCH_INFO
   TARGET_CATCH_INFO (arg, from_tty);
   return;
#else

  /* Check for target support for exception handling */
  sal = target_enable_exception_callback (EX_EVENT_CATCH, 1);
  if (sal)
    {
      /* Currently not handling this */
      /* Ideally, here we should interact with the C++ runtime
         system to find the list of active handlers, etc. */
      fprintf_filtered (gdb_stdout, "Info catch not supported with this target/compiler combination.\n");
    }
  else
    {
      /* Assume g++ compiled code -- old v 4.16 behaviour */
      if (!selected_frame)
	error ("No frame selected.");

      print_frame_label_vars (selected_frame, 0, gdb_stdout);
    }
#endif
}

static void
print_frame_arg_vars (register struct frame_info *fi, register struct ui_file *stream)
{
  struct symbol *func = get_frame_function (fi);
  register struct block *b;
  int nsyms;
  register int i;
  register struct symbol *sym, *sym2;
  register int values_printed = 0;

  if (func == 0)
    {
      fprintf_filtered (stream, "No symbol table info available.\n");
      return;
    }

#ifdef INLINE_SUPPORT

  /* inlined frame -- do not print args from the enclosing
     function; instead print args from the current block
     Inner blocks will not have param kind of symbols.
  */
  if (fi->inline_idx > 0)
    {
       for (b = get_frame_block(fi); b && b->is_inlined == 0;
            b = BLOCK_SUPERBLOCK (b))
         ;

      if (!b)
        b = get_frame_block(fi);
    }
  else
    b = SYMBOL_BLOCK_VALUE (func);

  nsyms = BLOCK_NSYMS (b);
#else
  b = SYMBOL_BLOCK_VALUE (func);
  nsyms = BLOCK_NSYMS (b);
#endif

  for (i = 0; i < nsyms; i++)
    {
      sym = BLOCK_SYM (b, i);

#ifdef INLINE_SUPPORT
      if (sym->is_argument != 1) /* do not print locals */
        continue;
#endif

      switch (SYMBOL_CLASS (sym))
	{
	case LOC_ARG:
	case LOC_LOCAL_ARG:
	case LOC_REF_ARG:
	case LOC_REGPARM:
	case LOC_REGPARM_ADDR:
	case LOC_BASEREG_ARG:
	case LOC_COMPUTED_ARG:
#ifdef INLINE_SUPPORT
        /* in inlined routine the argument could be in a register */
	case LOC_REGISTER:
        if ((SYMBOL_CLASS(sym) == LOC_REGISTER) &&
          (fi->inline_idx == 0)) /* not an inlined function */
          break;
#endif
               
	  values_printed = 1;
	  fputs_filtered (SYMBOL_SOURCE_NAME (sym), stream);
	  fputs_filtered (" = ", stream);

	  /* We have to look up the symbol because arguments can have
	     two entries (one a parameter, one a local) and the one we
	     want is the local, which lookup_symbol will find for us.
	     This includes gcc1 (not gcc2) on the sparc when passing a
	     small structure and gcc2 when the argument type is float
	     and it is passed as a double and converted to float by
	     the prologue (in the latter case the type of the LOC_ARG
	     symbol is double and the type of the LOC_LOCAL symbol is
	     float).  There are also LOC_ARG/LOC_REGISTER pairs which
	     are not combined in symbol-reading.  */

	  sym2 = lookup_symbol (SYMBOL_NAME (sym),
		   b, VAR_NAMESPACE, (int *) NULL, (struct symtab **) NULL);
	  print_variable_value (sym2, fi, stream);
	  fprintf_filtered (stream, "\n");
	  break;

	default:
	  /* Don't worry about things which aren't arguments.  */
	  break;
	}
    }

  if (!values_printed)
    {
      fprintf_filtered (stream, "No arguments.\n");
    }
}

void
args_info (char *ignore, int from_tty)
{
  if (!selected_frame)
    error ("No frame selected.");
  print_frame_arg_vars (selected_frame, gdb_stdout);
}


static void
args_plus_locals_info (char *ignore, int from_tty)
{
  args_info (ignore, from_tty);
  locals_info (ignore, from_tty);
}

/* Do the guts of select_frame, but don't do the TUIDO at the end.
   */

struct symtab *
select_frame_no_tui (struct frame_info *fi, int level)
{
  register struct symtab *s;

  selected_frame = fi;
  selected_frame_level = level;
  if (selected_frame_level_changed_hook)
    selected_frame_level_changed_hook (level);

  /* Ensure that symbols for this frame are read in.  Also, determine the
     source language of this frame, and switch to it if desired.  */
  s = NULL;
  if (fi)
    {
      s = find_pc_symtab (fi->pc);
      if (s
	  && s->language != current_language->la_language
	  && s->language != language_unknown
	  && language_mode == language_mode_auto)
	{
	  set_language (s->language);
	}
    }
  return s;
} /* end select_frame_no_tui */

/* Select frame FI, and note that its stack level is LEVEL.
   LEVEL may be -1 if an actual level number is not known.  */

void
select_frame (struct frame_info *fi, int level)
{
  register struct symtab *s;

  s = select_frame_no_tui (fi, level);

  if (fi)
    {
      /* elz: this if here fixes the problem with the pc not being displayed
         in the tui asm layout, with no debug symbols. The value of s 
         would be 0 here, and select_source_symtab would abort the
         command by calling the 'error' function */
      if (s)
	{
	  TUIDO (((TuiOpaqueFuncPtr) tui_vSelectSourceSymtab, s));
	}
    }
#ifdef HP_IA64
  /* When new frame is being selected, invalidate current path info */
  invalidate_current_ept ();
#endif
}


/* Select frame FI, noting that its stack level is LEVEL.  Also print
   the stack frame and show the source if this is the tui version.  */
void
select_and_print_frame (struct frame_info *fi, int level)
{
  select_frame (fi, level);
  if (fi)
    {
      print_stack_frame (fi, level, 1);
      TUIDO (((TuiOpaqueFuncPtr) tui_vCheckDataValues, fi));
    }
}


/* Store the selected frame and its level into *FRAMEP and *LEVELP.
   If there is no selected frame, *FRAMEP is set to NULL.  */

void
record_selected_frame (CORE_ADDR *frameaddrp, int *levelp)
{
  *frameaddrp = selected_frame ? selected_frame->frame : 0;
  *levelp = selected_frame_level;
}

/* Return the symbol-block in which the selected frame is executing.
   Can return zero under various legitimate circumstances.  */

struct block *
get_selected_block ()
{
  if (!target_has_stack)
    return 0;

  if (!selected_frame)
    return get_current_block ();
  return get_frame_block (selected_frame);
}

/* Find a frame a certain number of levels away from FRAME.
   LEVEL_OFFSET_PTR points to an int containing the number of levels.
   Positive means go to earlier frames (up); negative, the reverse.
   The int that contains the number of levels is counted toward
   zero as the frames for those levels are found.
   If the top or bottom frame is reached, that frame is returned,
   but the final value of *LEVEL_OFFSET_PTR is nonzero and indicates
   how much farther the original request asked to go.  */

struct frame_info *
find_relative_frame (register struct frame_info *frame, register int *level_offset_ptr)
{
  register struct frame_info *prev;
  register struct frame_info *frame1;

#ifdef HP_IA64
  populate_aries_pa2ia_export_stub_addr();
#endif

  /* Going up is simple: just do get_prev_frame enough times
     or until initial frame is reached.  */
  while (*level_offset_ptr > 0)
    {
      prev = get_prev_frame (frame);
#ifdef COVARIANT_SUPPORT
       /*                Co-variant return type
         If the "prev" frame "pc" happens to be in the outbound or
         inbound outbound thunk, skip the "prev" frame in order
         to avoid printing of thunks, an internal compiler 
         generated function
       */
      if(prev ? IS_PC_IN_COVARIANT_THUNK (prev->pc) : NULL) 
        {
          prev = get_prev_frame (prev);
        }
#endif
#ifdef HP_IA64
      prev = mixed_mode_adjust (prev);
#endif
      if (prev == 0)
	break;
      (*level_offset_ptr)--;
      frame = prev;
    }
  /* Going down is just as simple.  */
  if (*level_offset_ptr < 0)
    {
      while (*level_offset_ptr < 0)
	{
	  frame1 = get_next_frame (frame);
	  if (!frame1)
	    break;
	  frame = frame1;
	  (*level_offset_ptr)++;
	}
    }
  return frame;
}

/* The "select_frame" command.  With no arg, NOP.
   With arg LEVEL_EXP, select the frame at level LEVEL if it is a
   valid level.  Otherwise, treat level_exp as an address expression
   and select it.  See parse_frame_specification for more info on proper
   frame expressions. */

/* ARGSUSED */
#ifdef UI_OUT
void
select_frame_command_wrapper (char *level_exp, int from_tty)
{
  select_frame_command (level_exp, from_tty);
}
#endif
static void
select_frame_command (char *level_exp, int from_tty)
{
  register struct frame_info *frame, *frame1;
  unsigned int level = 0;

  if (!target_has_stack)
    error ("No stack.");

  frame = parse_frame_specification (level_exp);

#ifdef HP_IA64
  populate_aries_pa2ia_export_stub_addr();
#endif

  /* Try to figure out what level this frame is.  But if there is
     no current stack, don't error out -- let the user set one.  */
  frame1 = 0;
  if (get_current_frame ())
    {
      for (frame1 = get_prev_frame (0);
	   frame1 && frame1 != frame;
	   frame1 = get_prev_frame (frame1)
#ifdef HP_IA64
           ,frame1 = mixed_mode_adjust (frame1)
#endif
           )
        {
	  level++;
        }
    }

  if (!frame1)
    level = 0;

  select_frame (frame, level);
}

/* The "frame" command.  With no arg, print selected frame briefly.
   With arg, behaves like select_frame and then prints the selected
   frame.  */

void
frame_command (char *level_exp, int from_tty)
{
  select_frame_command (level_exp, from_tty);
  show_and_print_stack_frame (selected_frame, selected_frame_level, 1);
}

/* The XDB Compatibility command to print the current frame. */

static void
current_frame_command (char *level_exp, int from_tty)
{
  if (target_has_stack == 0 || selected_frame == 0)
    error ("No stack.");
  print_only_stack_frame (selected_frame, selected_frame_level, 1);
}

/* Select the frame up one or COUNT stack levels
   from the previously selected frame, and print it briefly.  */

/* ARGSUSED */
static void
up_silently_base (char *count_exp)
{
  register struct frame_info *fi;
  int count = 1, count1;
  if (count_exp)
    count = (int) parse_and_eval_address (count_exp);
  count1 = count;

  if (target_has_stack == 0 || selected_frame == 0)
    error ("No stack.");

  fi = find_relative_frame (selected_frame, &count1);
  if (count1 != 0 && count_exp == 0)
    error ("Initial frame selected; you cannot go up.");
  select_frame (fi, selected_frame_level + count - count1);
}

static void
up_silently_command (char *count_exp, int from_tty)
{
  up_silently_base (count_exp);
  if (tui_version)
    print_stack_frame (selected_frame, selected_frame_level, 1);
}

static void
up_command (char *count_exp, int from_tty)
{
  up_silently_base (count_exp);
  show_and_print_stack_frame (selected_frame, selected_frame_level, 1);
}

/* Select the frame down one or COUNT stack levels
   from the previously selected frame, and print it briefly.  */

/* ARGSUSED */
static void
down_silently_base (char *count_exp)
{
  register struct frame_info *frame;
  int count = -1, count1;
  if (count_exp)
    count = (int) -parse_and_eval_address (count_exp);
  count1 = count;

  if (target_has_stack == 0 || selected_frame == 0)
    error ("No stack.");

  frame = find_relative_frame (selected_frame, &count1);
  if (count1 != 0 && count_exp == 0)
    {

      /* We only do this if count_exp is not specified.  That way "down"
         means to really go down (and let me know if that is
         impossible), but "down 9999" can be used to mean go all the way
         down without getting an error.  */

      error ("Bottom (i.e., innermost) frame selected; you cannot go down.");
    }

  select_frame (frame, selected_frame_level + count - count1);
}

/* ARGSUSED */
static void
down_silently_command (char *count_exp, int from_tty)
{
  down_silently_base (count_exp);
  if (tui_version)
    print_stack_frame (selected_frame, selected_frame_level, 1);
}

static void
down_command (char *count_exp, int from_tty)
{
  down_silently_base (count_exp);
  show_and_print_stack_frame (selected_frame, selected_frame_level, 1);
}

#ifdef UI_OUT
void
return_command_wrapper (char *retval_exp, int from_tty)
{
  return_command (retval_exp, from_tty);
}
#endif
static void
return_command (char *retval_exp, int from_tty)
{
  struct symbol *thisfun;
  CORE_ADDR selected_frame_addr;
#ifdef REGISTER_STACK_ENGINE_FP
  CORE_ADDR selected_frame_rse_addr;
#endif
  CORE_ADDR selected_frame_pc;
  struct frame_info *frame;
  value_ptr return_value = NULL;
  int in_covariant_thunks = 0; 

  if (selected_frame == NULL)
    error ("No selected frame.");
  thisfun = get_frame_function (selected_frame);
  selected_frame_addr = FRAME_FP (selected_frame);
#ifdef REGISTER_STACK_ENGINE_FP
  selected_frame_rse_addr= selected_frame->rse_fp;
#endif
  selected_frame_pc = selected_frame->pc;

#ifdef HP_IA64_GAMBIT
  error("return command disabled on IA64-gambit");
#endif

  /* Compute the return value (if any -- possibly getting errors here).  */

  if (retval_exp)
    {
      struct type *return_type = NULL;
  
      return_value = parse_and_eval (retval_exp);

      /* Cast return value to the return type of the function.  */
      if (thisfun != NULL)
	return_type = TYPE_TARGET_TYPE (SYMBOL_TYPE (thisfun));
      if (return_type == NULL)
	return_type = builtin_type_int;
      return_value = value_cast (return_type, return_value);

      /* Make sure we have fully evaluated it, since
         it might live in the stack frame we're about to pop.  */
      if (VALUE_LAZY (return_value))
	value_fetch_lazy (return_value);
    }

  /* If interactive, require confirmation.  */

  if (from_tty)
    {
      if (thisfun != 0)
	{
	  if (!query ("Make %s return now? ", SYMBOL_SOURCE_NAME (thisfun)))
	    {
	      error ("Not confirmed.");
	      /* NOTREACHED */
	    }
	}
      else if (!query ("Make selected stack frame return now? "))
	error ("Not confirmed.");
    }
#ifdef COVARIANT_SUPPORT
  /* 		Co-variant return type 
	If the saved_pc or rp of the selected frame happens to be in
    the outbound thunk, handle the "return command" differently.
    i.e., Overwrite the return value at the Covariant function(callee)
    so that the return value gets updated when the control pass
    thru outbound thunk. 
  */
  if ( IS_PC_IN_COVARIANT_THUNK (FRAME_SAVED_PC (selected_frame))) 
    {
      in_covariant_thunks = 1;
    }
#endif
  /* Do the real work.  Pop until the specified frame is current.  We
     use this method because the selected_frame is not valid after
     a POP_FRAME.  The pc comparison makes this work even if the
     selected frame shares its fp with another frame.  */

  while (selected_frame_addr != (frame = get_current_frame ())->frame
#ifdef REGISTER_STACK_ENGINE_FP
	 || selected_frame_rse_addr != (frame->rse_fp)
#endif
	 || selected_frame_pc != frame->pc)
    POP_FRAME;
#ifdef COVARIANT_SUPPORT
  /* Overwrite the return value before popping up the selected
     frame for Co-variant functions */
  if (in_covariant_thunks && retval_exp) 
    {
      set_return_value (return_value);
    }
#endif
  /* Then pop that frame.  */

  POP_FRAME;

  /* Compute the return value (if any) and store in the place
     for return values.  */
  /* 		Co-variant return type
     Note that for covariant thunks, we should not overwrite
     the "old return value" after popping up the selected 
     frame  because the value returned by the Covariant function 
     should have been modified by outbound/inboundoutbound thunk 
     and we need the modified "return value" to imitate the 
     compiler behaviour of "modifying return value" when returning
     back to caller thru THUNK.
   */
  if (retval_exp && !in_covariant_thunks) 
  {
      set_return_value (return_value);
  }
  /* If interactive, print the frame that is now current.  */

  if (from_tty)
    frame_command ("0", 1);
  else
    select_frame_command ("0", 0);
}

static void
abort_call_command (char *arg, int from_tty)
{
  struct frame_info *fi, *call_frame;

  /* HP-UX IPF ttrace doesn't allow function calls in a syscall context */
  if (cur_thread_in_syscall())
    {
      error (
	"You may not abort a function while a system call is interrupted.");
    }

  /* find the call-dummy frame */
  fi = get_current_frame();
  while (fi)
    {
      
#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
      if (fi->pc >= fi->frame
          && fi->pc <= (fi->frame +
                        (REGISTER_SIZE / INSTRUCTION_SIZE) * CALL_DUMMY_LENGTH
                        + (32 * REGISTER_SIZE)
                        + (NUM_REGS - FP0_REGNUM) * 8
                        + (6 * REGISTER_SIZE)))
#else
      if (PC_IN_CALL_DUMMY (fi->pc, 0, 0))
#endif
#else  
#ifdef HP_IA64
  if (PC_IN_CALL_DUMMY (fi->pc, 0, 0))
#else
  if (frame_in_dummy (fi))
#endif
#endif
        break;

      fi = get_prev_frame(fi);
    }

  if (!fi)
    error("No function called from gdb on stack\n");

  call_frame = (struct frame_info *)xmalloc (sizeof (struct frame_info));
  *call_frame = *fi;
  
  /* If interactive, require confirmation.  */
  if (from_tty)
    {
      if (!query ("Abort gdb command line call? "))
        error ("Not confirmed.");
    }

  /* cancel any pending signals */
  stop_signal = TARGET_SIGNAL_0;

  /* Do the real work.  Pop until the specified frame is current. */
  while (call_frame->frame != (fi = get_current_frame())->frame
#ifdef REGISTER_STACK_ENGINE_FP
	 || call_frame->rse_fp != fi->rse_fp
#endif
         || call_frame->pc != fi->pc)
    POP_FRAME;

  /* Then pop that frame.  */
  POP_FRAME;

  /* If interactive, print the frame that is now current.  */
  if (from_tty)
    frame_command ("0", 1);
  else
    select_frame_command ("0", 0);
  free (call_frame);
}

/* Sets the scope to input function name, provided that the
   function is within the current stack frame */

struct function_bounds
{
  CORE_ADDR low, high;
};

static void func_command (char *arg, int from_tty);
static void
func_command (char *arg, int from_tty)
{
  struct frame_info *fp;
  int found = 0;
  struct symtabs_and_lines sals;
  int i;
  int level = 1;
  struct function_bounds *func_bounds = (struct function_bounds *) NULL;

  if (arg == (char *) NULL)
    return;

  fp = parse_frame_specification ("0");
  sals = decode_line_spec (arg, 1);
  func_bounds = (struct function_bounds *) xmalloc (
			      sizeof (struct function_bounds) * sals.nelts);
  for (i = 0; (i < sals.nelts && !found); i++)
    {
      if (sals.sals[i].pc == (CORE_ADDR) 0 ||
	  find_pc_partial_function (sals.sals[i].pc,
				    (char **) NULL,
				    &func_bounds[i].low,
				    &func_bounds[i].high) == 0)
	{
	  func_bounds[i].low =
	    func_bounds[i].high = (CORE_ADDR) NULL;
	}
    }

  do
    {
      for (i = 0; (i < sals.nelts && !found); i++)
	found = (fp->pc >= func_bounds[i].low &&
		 fp->pc < func_bounds[i].high);
      if (!found)
	{
	  level = 1;
	  fp = find_relative_frame (fp, &level);
	}
    }
  while (!found && level == 0);

  if (func_bounds) {
    free (func_bounds);
    func_bounds = NULL;
  }

  if (!found)
    printf_filtered ("'%s' not within current stack frame.\n", arg);
  else if (fp != selected_frame)
    select_and_print_frame (fp, level);
}

/* Gets the language of the current frame.  */

enum language
get_frame_language ()
{
  register struct symtab *s;
  enum language flang;		/* The language of the current frame */

  if (selected_frame)
    {
      s = find_pc_symtab (selected_frame->pc);
      if (s)
	flang = s->language;
      else
	flang = language_unknown;
    }
  else
    flang = language_unknown;

  return flang;
}

/* Function pointers for Java */
char *(*ptr_get_java_frame_str) (struct basic_frame_info *frame);
int (*ptr_get_prev_frame_info) (struct basic_frame_info *current,
        struct basic_frame_info *prev);
int (*ptr_is_java_frame) (CORE_ADDR  pc);
void (*ptr_cleanup_java_lib) ();
char* (*ptr_get_java_frame_info) (struct basic_frame_info *frame);
char* (*ptr_get_java_thread_info) (struct basic_frame_info *frame);
char* (*ptr_get_java_thread_ns) (struct basic_frame_info *frame); 
char* (*ptr_get_jvm_state) ();
char* (*ptr_get_java_unwind_info) (CORE_ADDR pc_val);
char* (*ptr_get_java_unwind_table) ();
char* (*ptr_get_java_mutex_info) (void);
char* (*ptr_get_java_object) (CORE_ADDR obj);
char* (*ptr_get_java_method_bytecodes) (CORE_ADDR method);
char* (*ptr_get_java_reference_info) (CORE_ADDR oop);
char* (*ptr_get_java_object_histogram) ();
char* (*ptr_get_java_klass_instances) (CORE_ADDR klassOop);
char* (*ptr_get_java_oop_for) (CORE_ADDR addr);
char* (*ptr_get_java_frame_args_info) (struct basic_frame_info *frame);
char* (*ptr_get_java_frame_locals_info) (struct basic_frame_info *frame);

extern void (*libjvm_load_hook) (char*);

/* Globals to maintain the state of breakpoint at __gdb_java_breakpoint. */
char gdb_java_break_buffer[16];
CORE_ADDR gdb_java_breakpoint_addr = 0;

void libjvm_got_loaded (char*);

/* Dummy functions for Java */

/* Get the function name and information for a Java frame */
char *
get_java_frame_str (struct basic_frame_info *frame)
{
  if (ptr_get_java_frame_str != NULL)
    return ptr_get_java_frame_str (frame);
  else
    return NULL;
}

/* Get the previous frame, given a Java frame */
int
get_prev_java_frame_info (struct basic_frame_info *current,
        struct basic_frame_info *prev)
{
  if (ptr_get_prev_frame_info != NULL)
    return ptr_get_prev_frame_info (current, prev);
  else
    return 0;
}

/* Get the previous frame, given a Java frame */
int 
get_prev_frame_info (struct basic_frame_info *current, 
	struct basic_frame_info *prev)
{
  if (ptr_get_prev_frame_info != NULL)
    return ptr_get_prev_frame_info(current,prev);
  return NULL;
}

/* Check if pc is in Java code */
int
is_java_frame (CORE_ADDR  pc)
{
  if (ptr_is_java_frame  != NULL)
    return ptr_is_java_frame(pc);
  else
    return 0;
}

/* Clean up the Java unwind data structures */
void
cleanup_java_lib ()
{
  if (ptr_cleanup_java_lib != NULL)
    ptr_cleanup_java_lib ();
  return;
}

/* Get the frame's Java specific frame info string. */
char *
get_java_frame_info (struct basic_frame_info *frame)
{
  if (ptr_get_java_frame_info != NULL)
    return ptr_get_java_frame_info (frame);
  else
    return NULL;
}

/* Get the current Java thread's info if it is a Java thread, otherwise return NULL. */
char *
get_java_thread_info (struct basic_frame_info *frame)
{
  if (ptr_get_java_thread_info != NULL)
    return ptr_get_java_thread_info (frame);
  else
    return NULL;
}

/* Get the current Java thread's name and state string if this is a Java thread, 
   otherwise return NULL. */
char *
get_java_thread_ns (struct basic_frame_info *frame)
{
  if (ptr_get_java_thread_ns != NULL)
    return ptr_get_java_thread_ns (frame);
  else
    return NULL;
}

/* Get the Java virtual machine's current state string. */
char *
get_jvm_state ()
{
  if (ptr_get_jvm_state != NULL)
    return ptr_get_jvm_state ();
  else
    return NULL;
}

/* Get the pc_val corresponding Java unwind entry info. */
char *
get_java_unwind_info (CORE_ADDR pc_val)
{
  if (ptr_get_java_unwind_info != NULL)
    return ptr_get_java_unwind_info (pc_val);
  else
   return NULL;
}

/* Get the java unwind table info. */
char *
get_java_unwind_table ()
{
  if (ptr_get_java_unwind_table != NULL)
    return ptr_get_java_unwind_table ();
  else
   return NULL;
}

/* Get the list of mutexes. */
char *
get_java_mutex_info (void)
{
  if (ptr_get_java_mutex_info != NULL)
    return ptr_get_java_mutex_info ();
  else
   return NULL;
}

/* Given a Java object pointer, return its fields info string. */
char *
get_java_object (CORE_ADDR obj)
{
  if (ptr_get_java_object != NULL)
    return ptr_get_java_object (obj);
  else
    return NULL;
}

/* Given a Java method pointer, return its bytecodes. */
char *
get_java_method_bytecodes (CORE_ADDR method)
{
  if (ptr_get_java_method_bytecodes != NULL)
    return ptr_get_java_method_bytecodes (method);
  else
    return NULL;
}

/* Given a Java object oop, return the addresses in the Java heap
   that hold the oop. */
char*
get_java_reference_info (CORE_ADDR oop)
{
  if (ptr_get_java_reference_info != NULL)
    return ptr_get_java_reference_info (oop);
  else
    return NULL;
}

/* Return the info of Java heap object histogram. */
char*
get_java_object_histogram ()
{
  if (ptr_get_java_object_histogram)
    return ptr_get_java_object_histogram ();
  else
    return NULL;
}

/* Return the instances oops of a given Java klass. */
char*
get_java_klass_instances (CORE_ADDR klassOop)
{
  if (ptr_get_java_klass_instances)
    return ptr_get_java_klass_instances (klassOop);
  else
    return NULL;
}

/* Return the Java oop whose object include the given address. */
char*
get_java_oop_for (CORE_ADDR addr)
{
  if (ptr_get_java_oop_for)
    return ptr_get_java_oop_for (addr);
  else
    return NULL;
}

/* Return the arguments info of the given Java frame. */
char*
get_java_frame_args_info (struct basic_frame_info *frame)
{
  if (ptr_get_java_frame_args_info)
    return ptr_get_java_frame_args_info (frame);
  else
    return NULL;
}

/* Return the local variables info of the given Java frame. */
char*
get_java_frame_locals_info (struct basic_frame_info *frame)
{
  if (ptr_get_java_frame_locals_info)
    return ptr_get_java_frame_locals_info (frame);
  else  
    return NULL;
}

/* Chain containing all defined java subcommands.  */
struct cmd_list_element *javacmdlist = NULL;

/* The "java" command is defined as a prefix, with allow_unknown = 0.
   Therefore, its own definition is called only for "java" with no args.  */
static void
java_command (char *arg, int from_tty)
{
  printf_unfiltered ("\"java\" must be followed by the name of an java command.\n");
  help_list (javacmdlist, "java ", -1, gdb_stdout);
}

/* Add an element to the list of java subcommands. */
struct cmd_list_element *
add_java_cmd (char *name, void (*fun) (char *, int), char *doc)
{
  return add_cmd (name, class_java, fun, doc, &javacmdlist);
}

  /* Make a reference to callbacks that we provide to java, so that
     linker will not do procelim of these functions. */
typedef void (*java_fn_ptr) (struct basic_frame_info *top_bframe);
static void* __java_callback_refs[] = { 
				 (void*) java_get_saved_register,
                                 (void*) java_read_memory,
                                 (void*) is_java_debug_version,
                                 (void*) java_evaluate_expression,
                                 (void*) java_get_next_frame,
                                 (void*) is_topmost_frame,
#ifdef HP_IA64
				 (void*) java_get_topmost_frame,
#else
				 /* FIXME, define for HP_IA64 */
				 (void*) (java_fn_ptr) -1,
#endif
                                 (void*) is_next_frame_signal_handler,
                                 (void*) 0 };


/* Dynamically load the Java Unwind library */
void
load_java_unwindlib (char *gdb_java_unwind_lib)
{
  shl_t   junwind_shlib_handle = NULL;
  struct  shl_symbol junwindlib_symbol;
  int ret_value;

#if (defined(__LP64__) && defined(HP_IA64))
  /* if we're a 64-bit gdb on IPF, we need to add a "64" suffix to this path. */
  char *gdb64_java_unwind_lib = 
		(char *) alloca (strlen(gdb_java_unwind_lib) + 3 );
  char *insert_point = strrstr(gdb_java_unwind_lib, "libjunwind") + 10;
  memcpy(gdb64_java_unwind_lib, gdb_java_unwind_lib,
				(insert_point - gdb_java_unwind_lib));
  gdb64_java_unwind_lib[(insert_point - gdb_java_unwind_lib)] = '6';
  gdb64_java_unwind_lib[(insert_point - gdb_java_unwind_lib + 1)] = '4';
  gdb64_java_unwind_lib[(insert_point - gdb_java_unwind_lib + 2)] = 0;
  strcat(gdb64_java_unwind_lib, insert_point);
  gdb_java_unwind_lib = gdb64_java_unwind_lib;
#endif /* defined(__LP64__) && defined(HP_IA64) */

  /* Shload the Java Unwind library */
  junwind_shlib_handle = shl_load (gdb_java_unwind_lib,
    					BIND_IMMEDIATE|BIND_VERBOSE, 0L);
  if (junwind_shlib_handle == 0)
  {
    printf_filtered ("errno = %#d\n",errno);
    printf_filtered ("Bad shlib %s\n",gdb_java_unwind_lib);

#if (defined(__LP64__) && defined(HP_IA64))
    printf_filtered ("This gdb requires a 64-bit libjunwind.so, which is only provided\n");
    printf_filtered ("by HP-UX JVMs starting with versions 1.4.2.10 or 1.5.0.03.\n");
    printf_filtered ("To unwind Java frames from your current JVM version, use an earlier\n");
    printf_filtered ("(32-bit) gdb available from www.hp.com/go/wdb.\n");
#endif /* defined(__LP64__) && defined(HP_IA64) */

    libjvm_load_hook = libjvm_got_loaded;
    return;
  }

  /* disable warning 2550-D, "variable set but never used" */
  __java_callback_refs[7] = __java_callback_refs[6];
  __java_callback_refs[7] = (void*) 1L;
  libjvm_load_hook = 0;

  /* Initialize function pointers */
  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_frame_str", TYPE_PROCEDURE, &ptr_get_java_frame_str);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java stack traces will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_prev_frame_info",TYPE_PROCEDURE, &ptr_get_prev_frame_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java stack traces will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "cleanup_java_lib",TYPE_PROCEDURE, &ptr_cleanup_java_lib);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java stack traces will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "is_java_frame",TYPE_PROCEDURE, &ptr_is_java_frame);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java stack traces will not be available\n");
    return;
  }

  java_debugging = 1;

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_frame_info",TYPE_PROCEDURE, &ptr_get_java_frame_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_thread_info",TYPE_PROCEDURE, &ptr_get_java_thread_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_thread_ns",TYPE_PROCEDURE, &ptr_get_java_thread_ns);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_jvm_state",TYPE_PROCEDURE, &ptr_get_jvm_state);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_unwind_info",TYPE_PROCEDURE, &ptr_get_java_unwind_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_unwind_table",TYPE_PROCEDURE, &ptr_get_java_unwind_table);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_object",TYPE_PROCEDURE, &ptr_get_java_object);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_method_bytecodes",TYPE_PROCEDURE, &ptr_get_java_method_bytecodes);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_reference_info",TYPE_PROCEDURE, &ptr_get_java_reference_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_object_histogram",TYPE_PROCEDURE, &ptr_get_java_object_histogram);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_klass_instances",TYPE_PROCEDURE, &ptr_get_java_klass_instances);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_oop_for",TYPE_PROCEDURE, &ptr_get_java_oop_for);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

    ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_frame_args_info",TYPE_PROCEDURE, &ptr_get_java_frame_args_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

    ret_value = shl_findsym (&junwind_shlib_handle,
    "get_java_frame_locals_info",TYPE_PROCEDURE, &ptr_get_java_frame_locals_info);
  if (ret_value == -1)
  {
    printf_filtered ("Older version of Java is being used. "
                     "Java commands will not be available\n");
    return;
  }

  /* Don't fail if get_java_mutex_info does not exist. */
  ret_value = shl_findsym (&junwind_shlib_handle, "get_java_mutex_info",
      TYPE_PROCEDURE, &ptr_get_java_mutex_info);
  if (ret_value == -1)
    printf_filtered ("Bad function %s\n", "get_java_mutex_info");

  /* Add Java debuggging commands. */
  add_prefix_cmd ("java", class_java, java_command,
	"Java and JVM debugging commands.", 
&javacmdlist, "java ", 0, &cmdlist);

  add_java_cmd ("jvm-state", jvm_state_command,
        "Show Java virtual machine's current internal states.");

  add_java_cmd ("unwind-info", java_unwind_info_command,
	"Show the unwind info of the code where the given pc is located.\n\
argument: <pc>");

  add_java_cmd ("unwind-table", java_unwind_table_command,
         "Print out the dynamically generated Java Unwind Table.");

  add_java_cmd ("mutex-info", java_mutex_info_command,
      "Print out details of the static mutexes.");

  add_java_cmd ("object", java_object_command, 
	"Print out the given Java object's fields info.\n\
argument: <java object oop>");

  add_java_cmd ("bytecodes", java_bytecodes_command,
        "Disassemble the given Java method's bytecodes.\n\
argument: <methodOop>");

  add_java_cmd ("references", java_reference_command,
	"Find all the references to the given Java object in the Java heap.\n\
argument: <java object oop>");

  add_java_cmd ("heap-histogram", java_object_histogram_command, 
	"Show the Java heap object histogram.");

  add_java_cmd ("instances", java_klass_instances_command,
	"Find all the instances of the given klassOop in the Java heap.\n\
argument: <klassOop>");

  add_java_cmd ("oop", java_oop_for_command, 
	"Find the Java object oop of the given Java heap address.\n\
argument: <Java heap address>");

  add_java_cmd ("args", java_args_command,
	"Show the current or specified Java frame arguments info.");

  add_java_cmd ("locals", java_locals_command,
        "Show the current or specified Java frame locals info.");
}

static void
jvm_state_command (char* arg, int from_tty)
{
  char *vm_state;

  vm_state = get_jvm_state ();
  if (vm_state) {
    printf_filtered ("%s", vm_state);
    free (vm_state);
  }
}

static void
java_unwind_info_command (char* pc_exp, int from_tty)
{
  CORE_ADDR pc_val;
  char *unwind_info;

  if (!pc_exp) {
    printf_filtered ("command format:  java_unwind_info <pc> \n");
    return;
  }

  pc_val = parse_and_eval_address (pc_exp);

  unwind_info = get_java_unwind_info (pc_val);
  if (!unwind_info)
    /* pc_val is not in any Java compiled code range. */
    printf_filtered ("The pc value is not in any Java code range.\n");
  else {
    printf_filtered ("%s", unwind_info);
    free (unwind_info); 
  }
}

static void
java_unwind_table_command (char *arg, int from)
{
  char *table_info;

  table_info = get_java_unwind_table();
  if (!table_info)
    /* Java unwind table is not available in the JVM. */
    printf_filtered ("The Java unwind table is not available in the JVM.\n");
  else {
    printf_filtered ("%s", table_info);
    free (table_info);
  }
}

static void
java_mutex_info_command (char *arg, int from)
{
  char *mutex_info;

  mutex_info = get_java_mutex_info();
  if (mutex_info == NULL)
    printf_filtered ("The Java mutex information cannot be obtained.\n");
  else {
    printf_filtered ("%s", mutex_info);
    free (mutex_info);
  }
}

static void
java_object_command (char* objptr_exp, int from_tty)
{
  CORE_ADDR objptr;
  char *obj_info;

  if (!objptr_exp) {
    printf_filtered ("command format:  java_object <obj_ptr> \n");
    return;
  }

  objptr = parse_and_eval_address (objptr_exp);

  obj_info = get_java_object (objptr);
  if (obj_info) {
    printf_filtered ("%s", obj_info);
    free (obj_info);  // free the string after use.
  } 
}

static void
java_bytecodes_command (char* method, int from_tty)
{
  CORE_ADDR jmethod;
  char *bytecodes;

  if (!method) {
    printf_filtered ("command format:  java_bytecodes <methodOop>\n");
    return;
  }

  jmethod = parse_and_eval_address (method);
  
  bytecodes = get_java_method_bytecodes (jmethod);
  if (bytecodes) {
    printf_filtered ("%s", bytecodes);
    free (bytecodes);  /* free the str passed in by libjunwind. */
  } else {
    /* jmethod is not a valid Java methodOop. */
    printf_filtered ("The methodOop is not valid.\n");
  }
}

static void
java_reference_command (char* oop_str, int from_tty)
{
  CORE_ADDR oop;
  char *ref_info;

  if (!oop_str) {
    printf_filtered ("command format:  java_heap_address <oop>\n");
    return;
  }

  oop = parse_and_eval_address (oop_str);

  ref_info = get_java_reference_info (oop);
  if (ref_info) {
    printf_filtered ("%s", ref_info);
    free (ref_info); /* free the str passed in by libjunwind. */
  } 
}

static void
java_object_histogram_command (char *arg, int from)
{
  char *obj_hist;

  obj_hist = get_java_object_histogram();
  if (obj_hist) {
    printf_filtered ("%s", obj_hist);
    free (obj_hist);
  }
}

static void
java_klass_instances_command (char* klassOop_exp, int from_tty)
{
  CORE_ADDR klassOop;
  char *oops_info;

  if (!klassOop_exp) {
    printf_filtered ("command format:  java_klass_instances <klassOop> \n");
    return;
  }

  klassOop = parse_and_eval_address (klassOop_exp);

  oops_info = get_java_klass_instances (klassOop);
  if (oops_info) {
    printf_filtered ("%s", oops_info);
    free (oops_info);  // free the string after use.
  }
}

static void
java_oop_for_command (char* addr_exp, int from_tty)
{
  CORE_ADDR addr;
  char *oop_info;

  if (!addr_exp) {
    printf_filtered ("command format:  java_oop_for <Java_heap_address>\n");
    return;
  }

  addr = parse_and_eval_address (addr_exp);

  oop_info = get_java_oop_for (addr);
  if (oop_info) {
    printf_filtered ("%s", oop_info);
    free (oop_info);  // free the string after use.
  }
}

static void
java_args_command (char* addr_exp, int from_tty)
{
  char *args_info;
  struct frame_info *fi;
  struct basic_frame_info bfi;

  fi = parse_frame_specification (addr_exp);
  if (fi == NULL)
    error ("Invalid frame specified.");
 
  if (!is_java_frame(fi->pc)) 
    printf_filtered ("non_Java frame specified.\n");   

  /* Initialize bfi being sent to junwindlib */
  bfi.pc = fi->pc;
  bfi.fp = fi->frame;
#ifdef HP_IA64
  bfi.bsp = fi->rse_fp;
  bfi.cfm = fi->cfm;
  bfi.java_ptr = 0;
#endif

  if (fi->next)
    bfi.sp = fi->next->frame;
  else
    bfi.sp = read_sp ();

  /* Call libjunwind to get the Java frame args info str. If it is not null,
     print it out and then free the str. */
  args_info = get_java_frame_args_info (&bfi);
  if (args_info) {
    printf_filtered ("%s", args_info);
    free (args_info);
  }
}

static void
java_locals_command (char* addr_exp, int from_tty)
{
  char *locals_info;
  struct frame_info *fi;
  struct basic_frame_info bfi;

  fi = parse_frame_specification (addr_exp);
  if (fi == NULL)
    error ("Invalid frame specified.");

  if (!is_java_frame(fi->pc))
    printf_filtered ("non_Java frame specified.\n");

  /* Initialize bfi being sent to junwindlib */
  bfi.pc = fi->pc;
  bfi.fp = fi->frame;
#ifdef HP_IA64
  bfi.bsp = fi->rse_fp;
  bfi.cfm = fi->cfm;
  bfi.java_ptr = 0;
#endif

  if (fi->next)
    bfi.sp = fi->next->frame;
  else
    bfi.sp = read_sp ();

  /* Call libjunwind to get the Java frame locals info str. If it is not null,
     print it out and then free the str. */
  locals_info = get_java_frame_locals_info (&bfi); 
  if (locals_info) {
    printf_filtered ("%s", locals_info);
    free (locals_info);
  }
}

#ifdef HP_IA64
static void
set_unwind_all_frames (char *args,
                       int from_tty,
                       struct cmd_list_element *c)
{
  if (!strcmp (args, "on"))
    {
      unwind_all_frames = true;
    }
  else if (!strcmp (args, "off"))
   {
     unwind_all_frames = false;
   }
}

static void
show_unwind_all_frames (char *args,
                        int from_tty,
                        struct cmd_list_element *c)
{
  if (unwind_all_frames)
    {
      printf_filtered ("unwind-all-frames is set to on.\n");
    }
  else
    {
      printf_filtered ("unwind-all-frames is set to off.\n");
    }
}

#endif

void
_initialize_stack ()
{
  char *s ;
  char *gdb_java_unwind_lib;
  struct stat buf;
  struct cmd_list_element *set, *show;

  add_com ("return", class_stack, return_command,
	   "Make selected stack frame return to its caller.\n\
Usage:\n\treturn [<EXP>]\n\n\
Control remains in the debugger, but when you continue\n\
execution will resume in the frame above the one now selected.\n\
If an argument is given, it is an expression for the value to return.");

  add_com ("abort_call", class_stack, abort_call_command,
           "Abort gdb command line call.\n\
Usage:\n\tabort_call\n\n");

  add_com ("up", class_stack, up_command,
	   "Select and print stack frame that called this one.\n\
Usage:\n\tup [<N>]\n\n\
An argument says how many frames up to go.");
  add_com ("up-silently", class_support, up_silently_command,
	   "Same as the `up' command, but does not print anything.\n\n\
Usage:\n\tup-silently <N>\n\n\
This is useful in command scripts.");

  add_com ("down", class_stack, down_command,
	   "Select and print stack frame called by this one.\n\
Usage:\n\tdown [<N>]\n\n\
An argument says how many frames down to go.");
  add_com_alias ("do", "down", class_stack, 1);
  add_com_alias ("dow", "down", class_stack, 1);
  add_com ("down-silently", class_support, down_silently_command,
	   "Same as the `down' command, but does not print anything.\n\n\
Usage:\n\tdown-silently <N>\n\n\
This is useful in command scripts.");

  add_com ("frame", class_stack, frame_command,
	   "Select and print a stack frame.\n\
Usage:\n\tframe [<N> | <ADDR>]\n\n\
With no argument, print the selected stack frame.  (See also \"info frame\").\n\
An argument specifies the frame to select.\n\
It can be a stack frame number or the address of the frame.\n\
With argument, nothing is printed if input is coming from\n\
a command file or a user-defined command.");

  add_com_alias ("f", "frame", class_stack, 1);

  if (xdb_commands)
    {
      add_com ("L", class_stack, current_frame_command,
	       "Print the current stack frame.\n");
      add_com_alias ("V", "frame", class_stack, 1);
    }
  add_com ("select-frame", class_stack, select_frame_command,
	   "Select a stack frame without printing anything.\n\
Usage:\n\tselect-frame [<N> | <ADDR>]\n\n\
An argument specifies the frame to select.\n\
It can be a stack frame number or the address of the frame.\n");

  add_com ("backtrace", class_stack, backtrace_command,
	   "Print backtrace of all stack frames, or innermost COUNT frames.\n\n\
Usage:\n\tbacktrace [full] [[-]<COUNT>]\n\n\
Alias: bt, b, ba, bac, where\n\
With a negative argument, print outermost -COUNT frames.\n\
Use of the 'full' qualifier also prints the values of the local variables.\n");
  add_com_alias ("bt", "backtrace", class_stack, 0);

  /* RM: ba is a xdb command */
  if (!xdb_commands)
    {
      add_com_alias ("ba", "backtrace", class_stack, 0);
    }
  add_com_alias ("bac", "backtrace", class_stack, 0);

#ifdef GDB_TARGET_IS_HPPA
  add_com ("backtrace_other_thread", class_stack, backtrace_other_thread_command,
           "Print backtrace of all stack frames for a thread with stack pointer SP and \n\
program counter PC.\n\
Usage:\n\tbacktrace_other_thread <SP> <PC>\n\n");
#endif
#ifdef HP_IA64
  add_com ("backtrace_other_thread", class_stack, backtrace_other_thread_command,
           "Print backtrace of all stack frames for a thread with stack pointer SP, \n\
program counter PC and address of gr32 in the backing store BSP\n\
Usage:\n\tbacktrace_other_thread <SP> <PC> <BSP>\n\n");
#endif
  
  if (xdb_commands)
    {
      add_com_alias ("t", "backtrace", class_stack, 0);
      add_com ("T", class_stack, backtrace_full_command,
	       "Print backtrace of all stack frames, or innermost COUNT frames \n\
and the values of the local variables.\n\
With a negative argument, print outermost -COUNT frames.\n\
Usage: T <count>\n");
    }

  add_com_alias ("where", "backtrace", class_alias, 0);
  add_info ("stack", backtrace_command,
	    "Backtrace of the stack, or innermost COUNT frames.\n\n"
            "Usage:\n\tinfo stack [full] [[-]<COUNT>]\n");
  add_info_alias ("s", "stack", 1);
  add_info ("frame", frame_info,
	    "All about selected stack frame, or frame at ADDR.\n\nUsage:\n\tinfo frame [<ADDR>]\n");
  add_info_alias ("f", "frame", 1);
  add_info ("locals", locals_info,
	    "Local variables of current stack frame.\n\nUsage:\n\tinfo locals\n");
  add_info ("loc_no_values", locals_info_no_values,
	    "Local variables of current stack frame - no values for aggregates.\n\n"
            "Usage:\n\tinfo loc_no_values\n");
  add_info ("args", args_info,
	    "Argument variables of current stack frame.\n\nUsage:\n\tinfo args\n");
  if (xdb_commands)
    add_com ("l", class_info, args_plus_locals_info,
	     "Argument and local variables of current stack frame.");

  if (dbx_commands)
    add_com ("func", class_stack, func_command,
      "Select the stack frame that contains <func>.\nUsage: func <name>\n");

	  add_info ("catch", catch_info,
		    "Exceptions that can be caught in the current stack frame.\n"
                    "\nUsage:\n\tinfo catch [ all | *<ADDR> | [<FILE>:]<LINE> ]"
                    "\n\nUse info catch for the selected frame.\nUse info catch "
                    "all to list all the exceptions that can be caught.\nUse info"
                    " catch *address or info catch file:line to list catches for"
                    " a particular instruction.");

#ifdef HP_IA64
  /* jini: QXCR1000875632: Allow the user to be able to unwind beyond
     MAX_NBR_STACK_FRAMES with the 'set unwind-all-frames on' command.
   */
  set = add_set_cmd ("unwind-all-frames", class_support, var_boolean,
          (char *) &unwind_all_frames,
          "Set the ability to unwind beyond 10000 frames.\n\n"
          "Usage:\nTo set new value:\n\tset unwind-all-frames [on | off]\n"
          "To see current value:\n\tshow unwind-all-frames\n"
          "Caution: Setting this to on might result in gdb running out of memory.",
          &setlist);
  set->function.sfunc = set_unwind_all_frames;
  show = add_show_from_set (set, &showlist);
  show->function.sfunc = show_unwind_all_frames;
#endif

  /* Load Java Unwind library if applicable. If user has set the 
     GDB_JAVA_UNWINDLIB environment variable, it takes the precedence.
     Else, get the location from libjvm. */
  gdb_java_unwind_lib = getenv ("GDB_JAVA_UNWINDLIB");
  if (gdb_java_unwind_lib && (stat (gdb_java_unwind_lib, &buf) != -1))
    load_java_unwindlib (gdb_java_unwind_lib);
  else
    libjvm_load_hook = libjvm_got_loaded;
}

/* Function to load the libjunwind by looking up the path from libjvm. 
   The variable __gdb_java_unwindlib is a string (char*) containing 
   the place at which libjunwind is located. This variable is set to the
   right path at the initialization stages of libjvm. libjvm calls a dummy
   function __gdb_java_breakpoint, when the initialization is done.
   If the initialization is not yet done, we put a breakpoint at the dummy
   function and retrive the string when the breakpoint is hit.
   If we find __gdb_java_unwindlib symbol we return 1 else 0.
 */
int
load_java_unwindlib_from_var ()
{
  /* Get the address of __gdb_java_uwindlib */
  CORE_ADDR  gdb_java_unwindlib_addr = 
	lookup_address_of_variable ("__gdb_java_unwindlib");
  if (gdb_java_unwindlib_addr)
    {
      /* Get the address at which __gdb_java_unwindlib is pointing to. */
      CORE_ADDR  gdb_java_unwindlib_charaddr;
      target_read_memory (gdb_java_unwindlib_addr,
		      (char*) &gdb_java_unwindlib_charaddr, sizeof (CORE_ADDR));
#ifdef SWIZZLE
      /* On IPF, for 32-bit programs, pointers are 32 bit. Shift the
	 64-bit value we have read-in by 32 bits. */
      if (is_swizzled)
	{
	  /* On pa32 we get a warning if the shift is not 0..31 */
	  gdb_java_unwindlib_charaddr = 
		gdb_java_unwindlib_charaddr >> 31;
	  gdb_java_unwindlib_charaddr >>= 1;  /* compiler doesn't like 32 */
	}
#endif
      if (gdb_java_unwindlib_charaddr)
        {
	  /* Get the String. Lets read-in MAXPATHLEN bytes. */
  	  char *gdb_java_unwind_lib = (char *) alloca (MAXPATHLEN );
          target_read_memory (gdb_java_unwindlib_charaddr, 
  		              gdb_java_unwind_lib, MAXPATHLEN);
          load_java_unwindlib (gdb_java_unwind_lib);
        }
      else 
        {
	  /* __gdb_java_unwindlib is set to NULL. Put a breakpoint at 
             __gdb_java_breakpoint */
	  struct minimal_symbol *m = 
		lookup_minimal_symbol("__gdb_java_breakpoint", 0, 0);
	  if (m)
            gdb_java_breakpoint_addr = SYMBOL_VALUE_ADDRESS (m);
          if (gdb_java_breakpoint_addr)
	    {
              /* JAGag26864 - Call target_insert_internal_breakpoint instead
                 of target_insert_breakpoint so that if software breakpoint 
                 fails, hardware will be inserted.  If hardware insertion 
                 also fails, give chatr +dbg enable message.
              */
	      int val = target_insert_internal_breakpoint (gdb_java_breakpoint_addr, gdb_java_break_buffer);
              if (val)
    		{
#ifdef HP_IA64
	          struct utsname uts;
		  int sys_ver = 0;
                  uname (&uts);
                  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
		  warning ("uname: Couldn't resolve the release identifier of"
		   	   " the Operating system.");

		  if (sys_ver == 31)
		    {
		      uint64_t value = 0;
                      /* Check whether the varibale is set. If not present 
                         ask user to install the patches.
                      */ 
		      if (gettune((const char *)"shlib_debug_enable", &value)
	  		   == -1 && errno == ENOENT)
 		        {
			  warning ("The shared libraries were not privately "
                                   "mapped;\n Debugger will not be able to do "
                                   "Java debugging.\nUse the following command "
                                   "to enable Java debugging.\n"
                                   "chatr +dbg enable <executable>\nor install "
			           "the patches PHKL_38651 and PHKL_38778"); 
	 		}  	      
		      else if (value == 0)
		        warning ("The shared libraries were not privately "
                                 "mapped;\n Debugger will not be able to do "
                                 "Java debugging.\n"
			         "Please set the kernel variable "
 				 "\"shlib_debug_enable\" to 1 to "
                                 "enable the shared library debugging\n");
		   }
		 else
#endif
                   warning ("The shared libraries were not privately "
                            "mapped;\n Debugger will not be able to do "
                            "Java debugging.\n Use the following command "
                            "to enable Java debugging.\n"
                            "chatr +dbg enable <executable>");

		  return 0;
    		} 
	    }
        }
      return 1;
    }
  return 0;
}

/* Check if libjvm got loaded. If so, get the location of libjunwind from
   __gdb_java_unwindlib. 
   If the process is started under the debugger: the __gdb_java_unwindlib
   won't be set at the load time. so we need to set a breakpoint on
   __gdb_java_breakpoint.
   If during attach or debugging corefile: we can directly call 
   load_java_unwindlib_from_var.
*/
void
libjvm_got_loaded (char* libname)
{
  char *libjvm_name = "libjvm";
  char * match = strstr (libname, libjvm_name);
  if (match && !java_debugging)
    {
      if (attach_flag || !target_has_execution)
	{
          if (load_java_unwindlib_from_var ())
	    libjvm_load_hook = 0;
	}
      else
	{
	  struct minimal_symbol *m = 
		lookup_minimal_symbol("__gdb_java_breakpoint", 0, 0);
	  if (m)
            gdb_java_breakpoint_addr = SYMBOL_VALUE_ADDRESS (m);
          if (gdb_java_breakpoint_addr)
	    {
              /* JAGag26864 - Call target_insert_internal_breakpoint instead
                 of target_insert_breakpoint so that if software breakpoint 
                 fails, hardware will be inserted.  If hardware insertion 
                 also fails, give chatr +dbg enable message.
              */
	      int val = target_insert_internal_breakpoint (gdb_java_breakpoint_addr, gdb_java_break_buffer);
              if (val)
                {
#ifdef HP_IA64
                  struct utsname uts;
                  int sys_ver = 0;
                  uname (&uts);
                  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
		  warning ("uname: Couldn't resolve the release identifier of"
                           " the Operating system.");
                  if (sys_ver == 31)
                   {
                     uint64_t value = 0;
                     /* Check whether the varibale is set. If not present
                        ask user to install the patches.
                     */
                     if (gettune((const char *)"shlib_debug_enable", &value) 
		         == -1 && errno == ENOENT)
                       {
	    	         warning ("The shared libraries were not privately " 
			          "mapped;\n Debugger will not be able to "
                                  "do Java debugging.\n"
                                  "Use the following command to enable "
			          "Java debugging.\n"
                                  "chatr +dbg enable <executable>\nor install "
			          "the patches PHKL_38651 and PHKL_38778"); 
		       }
                     else if (value == 0)
		       warning ("The shared libraries were not privately "
                                "mapped;\n Debugger will not be able to do "
                                "Java debugging.\n"
                                "Please set the kernel variable "
                                "\"shlib_debug_enable\" to 1 to "
                                "enable the shared library debugging\n");

                   }
                 else
#endif
	           warning ("The shared libraries were not privately mapped; "
                            "Debugger will not be able to do Java debugging.\n"
                            "Use the following command to enable Java "
			    "debugging.\n chatr +dbg enable <executable>");

                }
	    }
          libjvm_load_hook = 0;
        }
    }
  return;
}

/* Check if the given demagled name is a constructor. */
int
is_constructor (char *name)
{
  char *class_name, *given_name;

  if (!name)
    return 0;

  /* Make our own copy. We do not want name to be clobered . */
  given_name = (char*) alloca ((strlen (name) + 1) * sizeof (char));
  strcpy (given_name, name);
 
  if (!strstr (given_name, "::"))
    return 0; 
  class_name = strtok (given_name, "::");
  if (strcmp_iw (given_name, class_name) == 0)
    return 1;

  return 0;
}

/* These are 2 auxiliary functions needed for print_ept_detail (). */
#ifdef HP_IA64
/* given 2 PCs, find out whether both of them correspond to the same function. */
boolean
is_pc_out_of_bounds (CORE_ADDR pred_pc, CORE_ADDR pc)
{
  return lookup_minimal_symbol_by_pc (pred_pc) != lookup_minimal_symbol_by_pc (pc);
}

/* main function to decide about printing ept detail. */
enum ept_detail
check_step_into (CORE_ADDR pc, CORE_ADDR next_ept_pc, CORE_ADDR *target)
{
  bfd_byte buffer[16] = {0};
  Bundle b;
  unsigned long long targets[3] = {0};
  int status;
  char s1[255] = {0}, s2[255] = {0}, s3[255] = {0};
  static int das_initialized = 0;
  extern CORE_ADDR curr_pc;

  if (!das_initialized)
    {
      dasInit(DasTemplate | DasTemplateCommas | DasPseudoOps | DasRegNames, 78);
      das_initialized = 1;
    }

  curr_pc = pc;

  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    TARGET_PRINT_INSN_INFO->endian = BFD_ENDIAN_BIG;
  else
    TARGET_PRINT_INSN_INFO->endian = BFD_ENDIAN_LITTLE;

  if (TARGET_ARCHITECTURE != NULL)
    TARGET_PRINT_INSN_INFO->mach = TARGET_ARCHITECTURE->mach;
 
  status = (*(TARGET_PRINT_INSN_INFO)->read_memory_func) 
           (pc & ~0x3, buffer, sizeof (buffer), TARGET_PRINT_INSN_INFO);
  
  if (status != 0)
    {
      (*(TARGET_PRINT_INSN_INFO)->memory_error_func) 
      (status, pc & ~0x3, TARGET_PRINT_INSN_INFO);
      return false;
    }
  
  memcpy (&b, buffer, sizeof(b));
  
  dasBundle (&b, s1, s2, s3);

  if (strstr (s1, "br.cond") || strstr (s3, "br.cond") || 
      strstr (s2, "br.cond"))
    return STOP;


  if (!(strstr (s1, "br.call") || strstr (s3, "br.call") || 
        strstr (s2, "br.call")))
    return CONTINUE;

  bundleTargets (&b, targets);

  *target = targets[0] ? targets[0] : (targets[1] ? targets[1] : targets[2]);
  
  return (*target == 0 || 
          is_pc_out_of_bounds (next_ept_pc, (CORE_ADDR) *target)) ? STOP : STEP_INTO;
}
#endif


#ifdef HP_IA64
/* jini: The following is the callback function that libaries needs to
   call to read information from the mixed mode core file. */

#pragma OPTIMIZE OFF
static int mixed_mode_read_memory_2 (PTR arg);
typedef struct
{
  CORE_ADDR memaddr;
  char *myaddr;
  int len;
}
mixed_mode_args_for_read_memory;

int 
mixed_mode_read_memory (CORE_ADDR memaddr, char *myaddr, int len)
{
  mixed_mode_args_for_read_memory args;
  args.memaddr=memaddr;
  args.myaddr=myaddr;
  args.len=len;
  return catch_errors (mixed_mode_read_memory_2, &args, NULL, RETURN_MASK_ALL);
}

static int
mixed_mode_read_memory_2 (PTR arg)
{

  mixed_mode_args_for_read_memory *args = arg;
  return (!target_read_memory (args->memaddr, args->myaddr, args->len));
}

#pragma OPTIMIZE ON
#endif


/***************************** VDB Functions **************************/
/*  
    Stacey: 03/21/2002
    The following functions are only used in VDB - to find the end of 
    this search for 'end VDB Functions' 
    
    The function below provides current gdb function, line, and address
    info in a format that vim understands.  */
   
char *
wdb_status_line (struct symtab_and_line sal)
{
  static char message[1024];
  char gdb_line[32] = "??";
  char gdb_pc [32] = "??";
  char gdb_file_name [1024] = "??";
  char gdb_function_name [1024 * 16] = "??";
  char * free_this = 0;
  struct minimal_symbol *m;
  int Columns;
  int len;
  char * paren;

  Columns = atoi (getenv ("COLUMNS"));

  if (sal.line)
    sprintf (gdb_line, "%d", sal.line);

  /* faxing 07022001, when nimbus runs in disassembly mode, 
   * sal.pc is inaccurate because it points to the first instruction of 
   * the correpoding source line in fact. I change it to nimbus_current_pc. 
   * 
   * Stacey 03/21/2002
   * This is necessary because users may which to single step at the
   * instruction level, making it possible for the current pc to actually
   * be the third instruction of a source line for example.  */

  if (nimbus_current_pc)
    {
      strcat_address_numeric (nimbus_current_pc, 1, gdb_pc, 32);
    }

  if (sal.symtab && sal.symtab->filename)
    strcpy (gdb_file_name, getbasename (sal.symtab->filename)); 

  m = lookup_minimal_symbol_by_pc (sal.pc);
  if (m)
    {
      if (!is_cplus_name(SYMBOL_SOURCE_NAME(m), DEMANG))
        strcpy (gdb_function_name, SYMBOL_SOURCE_NAME (m));
      else
        strcpy (gdb_function_name,
                free_this = cplus_demangle(SYMBOL_SOURCE_NAME (m), 0));
    }

  if (free_this)
    free (free_this);


  paren = strchr (gdb_function_name, '(');
  if (paren)
    *paren = 0;

  len = sprintf (message, "File:\\ %s\\ \\ \\ \\ Function:\\ %s\\ \\ \\ \\ "
                              "Line:\\ %s\\ \\ \\ \\ Pc:\\ %s",
                              gdb_file_name,
                              gdb_function_name,
                              gdb_line,
                              gdb_pc);
  while (len < Columns - 3)
    message[len++] = ' ';
  message[len] = 0;
  return message;
}

void nimbus_begin_reflection ()
{
    printf_unfiltered ("\033\032\032:se pid=%d\n\032\032A", 
                           get_pid_for (inferior_pid));
    gdb_flush (gdb_stdout);
}

void nimbus_end_reflection ()
{
    printf_unfiltered ("\033\032\032:se pid=0\n\032\032A"); 
    gdb_flush (gdb_stdout);
}
/***************************** VDB Functions **************************/
