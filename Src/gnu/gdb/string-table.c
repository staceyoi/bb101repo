#include <ctype.h>
#include <assert.h>
#include "defs.h"
#include "gdb_string.h"

/* Defines for the string table (totally global to the debugger, though
 * local to this file - so far).  Added 08/25/05 by Carl Burch to save
 * heap space for the many redundant strings seen in the Microstrategy
 * application.  Note : use of a power-of-2 modulus (STRDUPHASHSIZE)
 * assumes a good string-hash function like HASH_STRING() to prevent
 * primary clustering.
 */
#define STRDUPHASHSIZE           (16 * 1024)  /* # ptrs in string hash table  */
#define MIN_STRBLK_ALLOCATION    0x10000 /* 64k space for storing string data */
#define STRSEG_ALLOCATION        100    /* growth inc for string seg array    */
#define STR_HASHNODE_ALLOCATION  \
           (2 * sizeof(void *) + (sizeof(struct stringnode *) * STRDUPHASHSIZE))
        /* Bigger than hash table and link pointer, which are in 1st block. */

struct stringnode {                 /* string table hash node                */
    struct stringnode *next;        /* next hash node                        */
    char *str_ptr;                  /* pointer to string                     */
    unsigned hashval;               /* hash value of string                  */
};

struct str_seg {
    char *block_ptr;                /* ptr to string data                    */
    int size;                       /* bytes in block that have been used    */
};

struct string_table {
    struct stringnode **hash_table; /* optional hash table                   */
    char *free_hnode_blk;           /* block of free hash nodes              */
    struct str_seg *seg_array;      /* array of string segments              */
    int free_hnode_offset;          /* offset into block of next free node   */
    int strblk_allocation_hint;     /* hint for allocating string space      */
    int total_segments;             /* total segment size available          */
    int open_segment;               /* segment we are currently filling      */
    int open_segment_bytes_free;    /* bytes free in segment we are filling  */
};

/* Forward Declarations */
static void gen_string_table(struct string_table *hdl, int allocation_hint);
static void purge_string_table(struct string_table *hdl);


/* Globals */

/* String table to hold strings from all input files */
static struct string_table sym_strings   = { NULL, NULL, NULL, 0, 0, 0, 0, 0 };
struct string_table *string_table_hdl = NULL;

/***************************************************************/
/*   String Hashing  *******************************************/
/***************************************************************/

#define HASH_STRING(str_in, key, str_length)                    \
    {                                                           \
    register unsigned char *s = (unsigned char *) str_in;       \
                                                                \
    key = 0;                                                    \
    while (*s)                                                  \
        key = ((key << 3) ^ (key >> 17)) ^ *s++;                \
    str_length = (unsigned int )(s - (unsigned char *) str_in); \
    key += str_length;						\
    }                                                           \


/***************************************************************/
/*   String Initialization  ************************************/
/***************************************************************/

/******************************************************************************
* FUNCTION :            strings_initialize
*
* ARGUMENTS:
*       None
*
* RETURN VALUE:
*       None
*
* PURPOSE:
*       Initialize string tables used by the gdb.
*
*       The 'sym_strings' string table is used for symbol strings
*
******************************************************************************/

static struct string_table *
strings_initialize ()
{
  gen_string_table (&sym_strings, MIN_STRBLK_ALLOCATION);
  string_table_hdl = &sym_strings;
  return &sym_strings;
}

/******************************************************************************
* FUNCTION :            gen_string_table
*
* ARGUMENTS:
*       hdl              - handle to string table to generate
*       allocation_hint  - hint to be used when allocating blocks of strings
*
* RETURN VALUE:
*       None
*
* PURPOSE:
*      Delete any string table referenced by hdl if it exists and create
*      a new one.
*
* DATA STRUCTURE:
*      The symbol table consists of an array of segment records. Each
*      segment record points to a raw block of characters where the
*      strings are kept and a size field that records the number of
*      bytes in the block that have been used. We fill the segments
*      in sequential order, with fields in the handle structure
*      recording where we are.
*
*      The hash table consists of a linked list of uniform sized
*      blocks of memory. The first word in each block is used as a next
*      field to the next block. The hash table itself is placed in the
*      first block in the list, right after the next field. Hash nodes are
*      allocated out of the remaining blocks.
*
******************************************************************************/

static void 
gen_string_table (struct string_table *hdl,
                              int allocation_hint)
{
  char *node_blk;

  if (hdl->seg_array != NULL)
    purge_string_table (hdl);

  hdl->seg_array =
      (struct str_seg *) xmalloc (sizeof (struct str_seg) * STRSEG_ALLOCATION);
  hdl->total_segments = STRSEG_ALLOCATION;
  hdl->open_segment = 0;
  hdl->open_segment_bytes_free = allocation_hint;
  hdl->strblk_allocation_hint = allocation_hint;
  hdl->seg_array[0].block_ptr = xmalloc (allocation_hint);
  hdl->seg_array[0].size = 0;

  /* Build hash table. */
  assert (STR_HASHNODE_ALLOCATION >
           (sizeof(void *) + (sizeof (struct stringnode *) * STRDUPHASHSIZE)));
  node_blk = xmalloc (STR_HASHNODE_ALLOCATION);
  hdl->hash_table = (struct stringnode **)(void *) (node_blk + sizeof(void *));
  hdl->free_hnode_offset = sizeof (void *) +
                           (sizeof (struct stringnode *) * STRDUPHASHSIZE);
  hdl->free_hnode_blk = node_blk;
  memset (node_blk, '\0', hdl->free_hnode_offset);

}

/******************************************************************************
* FUNCTION :            purge_string_table_hash
*
* ARGUMENTS:
*       hdl - handle to string table
*
* RETURN VALUE:
*       None
*
* PURPOSE:
*       Purge the hash table associated with the string table if the
*       hash table exists.
*
* DATA STRUCTURE
*       see gen_string_table()
*
******************************************************************************/

static void 
purge_string_table_hash (struct string_table * hdl)
{
  char *blk, *next_blk;

  if (hdl->hash_table != NULL)
    {
      blk = (((char *) hdl->hash_table) - sizeof (void *));

      while (blk != NULL)
        {
          next_blk = *(char **)(void *) blk;
          free (blk);
          blk = next_blk;
        }

      hdl->hash_table = NULL;
    }
}

/******************************************************************************
* FUNCTION :            purge_string_table
*
* ARGUMENTS:
*       hdl - handle to string table
*
* RETURN VALUE:
*       None
*
* PURPOSE:
*       Purge the string table referenced by hdl
*
* DATA STRUCTURE:
*       see gen_string_table()
*
******************************************************************************/

static void 
purge_string_table (struct string_table * hdl)
{
  int i;

  /* This string table is not designed to have more than one instance. */
  assert(string_table_hdl == hdl);

  for (i=0; i <= hdl->open_segment; i++)
    {
      if (hdl->seg_array[i].block_ptr != NULL)
        free(hdl->seg_array[i].block_ptr);
    }

  free (hdl->seg_array);
  hdl->seg_array = NULL;

  purge_string_table_hash (hdl);
  string_table_hdl = NULL;
}

/******************************************************************************
* FUNCTION :            add_string
*
* ARGUMENTS:
*       str_ptr       - pointer to string to be added
*       hashval       - hash value for string
*
* RETURN VALUE:
*       pointer to string in the string table
*
* PURPOSE:
*       add the given string to the string table referenced by hdl.
*
* DATA STRUCTURE:
*       see gen_string_table().
*
*       All strings entered into the string table are prefixed with
*       a word containing the length of the string. Because of this,
*       strings must be aligned on "int" boundaries.
*
******************************************************************************/

char *
add_string (char * str_ptr)
{
  int bytes_needed;
  struct stringnode *n;
  struct str_seg *seg_ptr;
  char *dest, *new_blk;
  unsigned int hashkey;
  struct string_table * hdl = string_table_hdl;
  unsigned int hashval;
  unsigned int str_len;

  /* initialize the string table if it isn't already. */
  if (hdl == NULL)
    {
      hdl = strings_initialize ();
    }

  /* search for a duplicate entry */
  assert (hdl->hash_table != NULL);
  HASH_STRING(str_ptr, hashval, str_len);

  hashkey = hashval % STRDUPHASHSIZE;
  n = hdl->hash_table[hashkey];

  while (n != NULL)
    {
      struct stringnode *n_next = n->next;	/* prefetch */
      if (   n->hashval == hashval
          && *(((int *)(void *)n->str_ptr) -1) == str_len
          && STREQ (n->str_ptr, str_ptr))
        {

          /* found a duplicate */
          return (n->str_ptr);
        }
      n = n_next;
    }

  /* add string to table */

  /* add room for length field, null byte, and round up to length
     field alignment */
  bytes_needed = (int)(sizeof (int) + ((str_len + sizeof (int)) & ~(sizeof (int)-1)));

  if (bytes_needed > hdl->open_segment_bytes_free)
    {
      if (++hdl->open_segment >= hdl->total_segments)
        {
          hdl->total_segments += STRSEG_ALLOCATION;
          hdl->seg_array = (struct str_seg *)
                           xrealloc ((char *) hdl->seg_array,
                                      hdl->total_segments
                                         * sizeof (struct str_seg));
        }

      hdl->open_segment_bytes_free = max (bytes_needed,
                                           hdl->strblk_allocation_hint);
      hdl->seg_array[hdl->open_segment].block_ptr =
                               xmalloc (hdl->open_segment_bytes_free);
      hdl->seg_array[hdl->open_segment].size = 0;
    }

  seg_ptr = &hdl->seg_array[hdl->open_segment];
  dest = seg_ptr->block_ptr + seg_ptr->size;

  /* copy the length over */
  *(int *)(void *)dest = str_len;
  dest += sizeof(int);

  strcpy(dest,str_ptr);

  seg_ptr->size += bytes_needed;
  hdl->open_segment_bytes_free -= bytes_needed;

  /* add string to hash table */

  if (hdl->free_hnode_offset + sizeof (struct stringnode) >
                                         STR_HASHNODE_ALLOCATION)
    {
      new_blk = xmalloc(STR_HASHNODE_ALLOCATION);
      hdl->free_hnode_offset = sizeof(void *);
      *((int *)(void *)new_blk) = *(((int *)hdl->hash_table) -1);
      *(((int *)(void *)hdl->hash_table) -1) = (int)(long) new_blk;
      hdl->free_hnode_blk = new_blk;
    }

  n = (struct stringnode *)(void *) (hdl->free_hnode_blk + hdl->free_hnode_offset);
  hdl->free_hnode_offset += sizeof (struct stringnode);

  n->str_ptr = dest;
  n->hashval = hashval;
  n->next = hdl->hash_table[hashkey];
  hdl->hash_table[hashkey] = n;

  return (dest);
}

/***************************************************************/
/*   Query Strings  ********************************************/
/***************************************************************/

/******************************************************************************
* FUNCTION :            sizeof_string_table
*
* ARGUMENTS:
*       hdl - handle to string table
*
* RETURN VALUE:
*       total number of bytes in the string table
*
* PURPOSE:
*       return the total number of bytes in the string table
*
* DATA STRUCTURE:
*       see gen_string_table().
*
******************************************************************************/

int 
sizeof_string_table (struct string_table * hdl)
{
  int i;
  int total;

  total = 0;
  for (i=0; i <= hdl->open_segment; i++)
     total += hdl->seg_array[i].size;

  return (total);
}

