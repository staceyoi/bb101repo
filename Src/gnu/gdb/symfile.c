/* Generic symbol file reading for the GNU debugger, GDB.
   Copyright 1990-1996, 1998, 2000 Free Software Foundation, Inc.
   Contributed by Cygnus Support, using pieces from other GDB modules.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "gdbcore.h"
#include "frame.h"
#include "target.h"
#include "value.h"
#include "symfile.h"
#include "objfiles.h"
#include "gdbcmd.h"
#include "breakpoint.h"
#include "language.h"
#include "complaints.h"
#include "demangle.h"
#include "inferior.h"		/* for write_pc */
#include "gdb-stabs.h"
#include "obstack.h"
#include "hpread.h"
#include "fastsym.h"
#include "top.h"

#include <assert.h>
#include <sys/types.h>
#include <fcntl.h>
#include "gdb_string.h"
#include "gdb_stat.h"
#include <ctype.h>
#include <time.h>

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
#include "elf-bfd.h"
#endif 
#if defined(HP_IA64)
#include "machine/sys/inline.h" /* _Asm_lfetch_excl */
#endif 

char * wdb_status_line (struct symtab_and_line);

char * wdb_status_line PARAMS ((struct symtab_and_line));

#ifndef O_BINARY
#define O_BINARY 0
#endif

#if defined (GDB_TARGET_IS_HPUX)
/* JYG: MERGE NOTE: Cygnus has modified this file, hp-psymtab-read.c,
   and somread.c, to move PXDB processing to sym_read phase.  The
   right way is to perform this check and processing at target-specific
   pre-file-load time.
   I have backed out the Cygnus change, so we do this at
   maybe_symbol_file_add time, a better placement although a hack. */
extern int hpread_pxdb_needed (bfd *);
extern int hpread_call_pxdb (char *);
#endif

extern int reset_all_breakpoints; 

#ifdef GDB_TARGET_IS_HPUX
/* Some HP-UX related globals to clear when a new "main"
   symbol file is loaded. HP-specific.  */

extern int hp_som_som_object_present;
extern int hp_cxx_exception_support_initialized;
extern int is_steplast_eligible;
#define RESET_HP_UX_GLOBALS() do {\
                                    hp_som_som_object_present = 0;             /* indicates HP-compiled code */        \
                                    hp_cxx_exception_support_initialized = 0;  /* must reinitialize exception stuff */ \
                              } while (0)
#endif

int (*ui_load_progress_hook) (const char *section, unsigned long num);
void (*show_load_progress) (const char *section,
			    unsigned long section_sent, 
			    unsigned long section_size, 
			    unsigned long total_sent, 
			    unsigned long total_size);
void (*pre_add_symbol_hook) (char *);
void (*post_add_symbol_hook) (void);
void (*target_new_objfile_hook) (struct objfile *);

void (*symbol_table_load_hook) (char *);
static void clear_symtab_users_cleanup (void *ignore);

#ifdef HP_MXN
void (*symbol_table_load_hook) (char*);
#endif

void (*libjvm_load_hook) (char*);
int endo_missing_in_symfile_objfile = 0;
int found_endo_export_list = 0;

/* Global variables owned by this file */
int readnow_symbol_files;	/* Read full symbols immediately */

struct complaint oldsyms_complaint =
{
  "Replacing old symbols for `%s'", 0, 0
};

struct complaint empty_symtab_complaint =
{
  "Empty symbol table found for `%s'", 0, 0
};

struct complaint unknown_option_complaint =
{
  "Unknown option `%s' ignored", 0, 0
};

/* External variables and functions referenced. */

#ifdef HP_IA64
/* QXCR1000579474: Aries core file debugging. */
extern bool add_aries_symbol_file;
#endif

/* Defined in gdbrtc.c; Used for supressing warnings to end-user */
extern bool add_unloaded_library_symbols;
extern int info_verbose;

/* srikanth, 000601, if the user is debugging a PA a.out on IA, the
   intent is that the user wants to debug the emulator (aries.) In this
   case, we don't want to load the symbol tables from the PA binary as
   it only confuses gdb. Instead silently load the aries boot loader.
   However, remember the real exec file name as we need to pass it
   to exec().
*/
extern int debugging_aries, debugging_aries64;
char * real_exec_file = 0;

extern void report_transfer_performance (unsigned long, time_t, time_t);

extern void hpread_init_debug_in_object_files(bfd  *sym_bfd);

/* Functions this file defines */

#if 0
static int simple_read_overlay_region_table (void);
static void simple_free_overlay_region_table (void);
#endif

static void set_initial_language (void);

static void load_command (char *, int);

#if 0
#ifndef HP_IA64 
static
#endif
#endif /* if 0 */
void add_symbol_file_command (char *, int);

static void add_shared_symbol_files_command (char *, int);

static void cashier_psymtab (struct partial_symtab *);

static int compare_psymbols (const void *, const void *);

static int compare_symbols (const void *, const void *);

bfd *symfile_bfd_open (char *);

static void find_sym_fns (struct objfile *);

static void decrement_reading_symtab (void *);

static void overlay_invalidate_all (void);

static int overlay_is_mapped (struct obj_section *);

void list_overlays_command (char *, int);

void map_overlay_command (char *, int);

void unmap_overlay_command (char *, int);

static void overlay_auto_command (char *, int);

static void overlay_manual_command (char *, int);

static void overlay_off_command (char *, int);

static void overlay_load_command (char *, int);

static void overlay_command (char *, int);

static void simple_free_overlay_table (void);

static void read_target_long_array (CORE_ADDR, unsigned int *, int);

static int simple_read_overlay_table (void);

static int simple_overlay_update_1 (struct obj_section *);

static void add_filename_language (char *ext, enum language lang);

static void set_ext_lang_command (char *args, int from_tty);

static void info_ext_lang_command (char *args, int from_tty);

static char *find_separate_debug_file (struct objfile *objfile);

static void init_filename_language_table (void);

void _initialize_symfile (void);

#ifdef HP_XMODE
/* External functions defined in top.c to check the executable format
   of the program to exec and to launch a different gdb if necessary. */

extern int xmode_exec_format_is_different (char *);
extern void xmode_launch_other_gdb (char *, enum xmode_cause);
#endif

/* srikanth, JAGaa80452 the function pointer permanet_copy_exists will
   point to a function that would answer the question whether a permanent
   copy of a given character string exists somwehere. For HP WDB this would
   be answered by looking at whether the string came from literal value
   table, VT. Since the answer to this question is very platform specific
   I am introducing a level of indirection here. The pointer gets initialized
   to a HP specific routine during the initialization of the symbol reader.
   For other platforms the pointer points to a generic routine that answers
   NO. */

static boolean generic_copy_exists (struct objfile *, char *);
extern boolean (*permanent_copy_exists) (struct objfile *, char *)
     = generic_copy_exists;

extern void mfree (PTR md, PTR ptr);

/* List of all available sym_fns.  On gdb startup, each object file reader
   calls add_symtab_fns() to register information on each format it is
   prepared to read. */

static struct sym_fns *symtab_fns = NULL;

/* Flag for whether user will be reloading symbols multiple times.
   Defaults to ON for VxWorks, otherwise OFF.  */

#ifdef SYMBOL_RELOADING_DEFAULT
int symbol_reloading = SYMBOL_RELOADING_DEFAULT;
#else
int symbol_reloading = 0;
#endif

int is_purified_program = 0;

/* If non-zero, then on HP-UX (i.e., platforms that use somsolib.c),
   this variable is interpreted as a threshhold.  If adding a new
   library's symbol table to those already known to the debugger would
   exceed this threshhold, then the shlib's symbols are not added.

   If non-zero on other platforms, shared library symbols will be added
   automatically when the inferior is created, new libraries are loaded,
   or when attaching to the inferior.  This is almost always what users
   will want to have happen; but for very large programs, the startup
   time will be excessive, and so if this is a problem, the user can
   clear this flag and then add the shared library symbols as needed.
   Note that there is a potential for confusion, since if the shared
   library symbols are not loaded, commands like "info fun" will *not*
   report all the functions that are actually present. 

   Note that HP-UX interprets this variable to mean, "threshhold size
   in megabytes, where zero means never add".  Other platforms interpret
   this variable to mean, "always add if non-zero, never add if zero."
 */

int auto_solib_add = 1;


/* srikanth, 010412, When the user manually loads the debug information
   from the shared libraries by using the "share regexp" command, we
   should remember the list of libraries loaded thus and make sure that
   they are reloaded automatically upon a rerun. Intel has about 200
   libraries, which they control manually. Gdb forgets these between
   reruns. auto_solib_readd is an opaque (void) pointer. Each
   configuration could define it to be a suitable type. For HP, this
   is a list of libraries loaded manually.
*/
void * auto_solib_readd;

boolean is_reinit_frame_cache = 1;

boolean reread_on_run = 1;

/* Since this function is called from within qsort, in an ANSI environment
   it must conform to the prototype for qsort, which specifies that the
   comparison function takes two "void *" pointers. */

static int
compare_symbols (const PTR s1p, const PTR s2p)
{
  register struct symbol **s1, **s2;

  s1 = (struct symbol **) s1p;
  s2 = (struct symbol **) s2p;

  return (STRCMP (SYMBOL_NAME (*s1), SYMBOL_NAME (*s2)));
}

/*

   LOCAL FUNCTION

   compare_psymbols -- compare two partial symbols by name

   DESCRIPTION

   Given pointers to pointers to two partial symbol table entries,
   compare them by name and return -N, 0, or +N (ala strcmp).
   Typically used by sorting routines like qsort().

   NOTES

   Does direct compare of first two characters before punting
   and passing to strcmp for longer compares.  Note that the
   original version had a bug whereby two null strings or two
   identically named one character strings would return the
   comparison of memory following the null byte.

 */

static int
compare_psymbols (const PTR s1p, const PTR s2p)
{
  register char *st1 = SYMBOL_NAME (*(struct partial_symbol **) s1p);
  register char *st2 = SYMBOL_NAME (*(struct partial_symbol **) s2p);

  if ((st1[0] - st2[0]) || !st1[0])
    {
      return (st1[0] - st2[0]);
    }
  else if ((st1[1] - st2[1]) || !st1[1])
    {
      return (st1[1] - st2[1]);
    }
  else
    {
      /* Note: I replaced the STRCMP line (commented out below)
       * with a simpler "strcmp()" which compares the 2 strings
       * from the beginning. (STRCMP is a macro which first compares
       * the initial characters, then falls back on strcmp).
       * The reason is that the STRCMP line was tickling a C compiler
       * bug on HP-UX 10.30, which is avoided with the simpler
       * code. The performance gain from the more complicated code
       * is negligible, given that we have already checked the
       * initial 2 characters above. I reported the compiler bug,
       * and once it is fixed the original line can be put back. RT
       */
      /* return ( STRCMP (st1 + 2, st2 + 2)); */
      return (strcmp (st1, st2));
    }
}

void
sort_pst_symbols (struct partial_symtab *pst)
{
  /* Sort the global list; don't sort the static list */

  qsort (pst->objfile->global_psymbols.list + pst->globals_offset,
	 pst->n_global_syms, sizeof (struct partial_symbol *),
	 compare_psymbols);
}

/* Call sort_block_syms to sort alphabetically the symbols of one block.  */

void
sort_block_syms (register struct block *b)
{
  qsort (&BLOCK_SYM (b, 0), BLOCK_NSYMS (b),
	 sizeof (struct symbol *), compare_symbols);
}

/* Call sort_symtab_syms to sort alphabetically
   the symbols of each block of one symtab.  */

void
sort_symtab_syms (register struct symtab *s)
{
  register struct blockvector *bv;
  int nbl;
  int i;
  register struct block *b;

  if (s == 0)
    return;
  bv = BLOCKVECTOR (s);
  nbl = BLOCKVECTOR_NBLOCKS (bv);
  for (i = 0; i < nbl; i++)
    {
      b = BLOCKVECTOR_BLOCK (bv, i);
      if (BLOCK_SHOULD_SORT (b))
	sort_block_syms (b);
    }
}

/* Make a null terminated copy of the string at PTR with SIZE characters in
   the obstack pointed to by OBSTACKP .  Returns the address of the copy.
   Note that the string at PTR does not have to be null terminated, I.E. it
   may be part of a larger string and we are only saving a substring. */

char *
obsavestring (char *ptr, int size, struct obstack *obstackp)
{
  register char *p = (char *) obstack_alloc (obstackp, size + 1);
  memmove (p, ptr, size);
  p[size] = 0;
  return p;
}

/* Concatenate strings S1, S2 and S3; return the new string.  Space is found
   in the obstack pointed to by OBSTACKP.  */

char *
obconcat (struct obstack *obstackp, const char *s1, const char *s2, const char *s3)
{
  register int len = (int) (strlen (s1) + strlen (s2) + strlen (s3) + 1);
  register char *val = (char *) obstack_alloc (obstackp, len);
  strcpy (val, s1);
  strcat (val, s2);
  strcat (val, s3);
  return val;
}

/* True if we are nested inside psymtab_to_symtab. */

int currently_reading_symtab = 0;

static void
decrement_reading_symtab (void *dummy)
{
  currently_reading_symtab--;
}

/* Get the symbol table that corresponds to a partial_symtab.
   This is fast after the first time you do it.  In fact, there
   is an even faster macro PSYMTAB_TO_SYMTAB that does the fast
   case inline.  */

struct symtab *
psymtab_to_symtab (register struct partial_symtab *pst)
{
  extern CORE_ADDR last_record_line_real_pc;


  last_record_line_real_pc = 0;  /* Re-set for a new symtab */

  /* If it's been looked up before, return it. */
  if (pst->readin && pst->symtab)
    return pst->symtab;

  /* If it has not yet been read in, read it.  */
  if (!pst->readin)
    {
      struct cleanup *back_to = make_cleanup (decrement_reading_symtab, NULL);
      currently_reading_symtab++;
      if (pst->read_symtab)
        (*pst->read_symtab) (pst);
      do_cleanups (back_to);
    }

  /* Set fullname in symtab */
  if (pst->symtab)
    symtab_to_filename (pst->symtab);

  return pst->symtab;
}

/* Initialize entry point information for this objfile. */

void
init_entry_point_info (struct objfile *objfile)
{
  /* Save startup file's range of PC addresses to help blockframe.c
     decide where the bottom of the stack is.  */

  if (bfd_get_file_flags (objfile->obfd) & EXEC_P)
    {
      /* Executable file -- record its entry point so we'll recognize
         the startup file because it contains the entry point.  */
      objfile->ei.entry_point = bfd_get_start_address (objfile->obfd);
    }
  else
    {
      /* Examination of non-executable.o files.  Short-circuit this stuff.  */
      objfile->ei.entry_point = INVALID_ENTRY_POINT;
    }
  objfile->ei.entry_file_lowpc = INVALID_ENTRY_LOWPC;
  objfile->ei.entry_file_highpc = INVALID_ENTRY_HIGHPC;
  objfile->ei.entry_func_lowpc = INVALID_ENTRY_LOWPC;
  objfile->ei.entry_func_highpc = INVALID_ENTRY_HIGHPC;
  objfile->ei.main_func_lowpc = INVALID_ENTRY_LOWPC;
  objfile->ei.main_func_highpc = INVALID_ENTRY_HIGHPC;
}

/* Get current entry point address.  */

CORE_ADDR
entry_point_address ()
{
  return symfile_objfile ? symfile_objfile->ei.entry_point : 0;
}

/* Remember the lowest-addressed loadable section we've seen.  
   This function is called via bfd_map_over_sections. 

   In case of equal vmas, the section with the largest size becomes the
   lowest-addressed loadable section.

   If the vmas and sizes are equal, the last section is considered the
   lowest-addressed loadable section.  */

void
find_lowest_section (bfd *abfd, asection *sect, PTR obj)
{
  asection **lowest = (asection **) obj;

  if (0 == (bfd_get_section_flags (abfd, sect) & SEC_LOAD))
    return;

  if (!*lowest) 
    *lowest = sect;		/* First loadable section */
  else if (bfd_section_vma (abfd, *lowest) > bfd_section_vma (abfd, sect))
    *lowest = sect;		/* A lower loadable section */
  else if (bfd_section_vma (abfd, *lowest) == bfd_section_vma (abfd, sect)
	   && (bfd_section_size (abfd, (*lowest))
	       <= bfd_section_size (abfd, sect)))
    *lowest = sect;
}

/* Return a freshly allocated copy of ADDRS.  The section names, if
   any, are also freshly allocated copies of those in ADDRS.  */
struct section_addr_info *
copy_section_addr_info (struct section_addr_info *addrs)
{
  struct section_addr_info *copy;
  int i;

  copy = (struct section_addr_info *) xmalloc 
           (sizeof (struct section_addr_info));

  for (i = 0; i < MAX_SECTIONS; i++)
    {
      copy->other[i].addr = addrs->other[i].addr;
      if (addrs->other[i].name)
        copy->other[i].name = xstrdup (addrs->other[i].name);
      else
        copy->other[i].name = NULL;
      copy->other[i].sectindex = addrs->other[i].sectindex;
    }

  return copy;
}

/* Build (allocate and populate) a section_addr_info struct from
   an existing section table. */

extern struct section_addr_info *
build_section_addr_info_from_section_table (const struct section_table *start,
                                            const struct section_table *end)
{
  struct section_addr_info *sap;
  const struct section_table *stp;
  int oidx;

  sap = xmalloc (sizeof (struct section_addr_info));
  memset (sap, 0, sizeof (struct section_addr_info));

  for (stp = start, oidx = 0; stp != end; stp++)
    {
      if (stp->the_bfd_section->flags & (SEC_ALLOC | SEC_LOAD)
	  && oidx < MAX_SECTIONS)
	{
	  sap->other[oidx].addr = stp->addr;
	  sap->other[oidx].name = xstrdup (stp->the_bfd_section->name);
	  sap->other[oidx].sectindex = stp->the_bfd_section->index;
	  oidx++;
	}
    }

  return sap;
}


/* Free all memory allocated by build_section_addr_info_from_section_table. */

extern void
free_section_addr_info (struct section_addr_info *sap)
{
  int idx;

  for (idx = 0; idx < MAX_SECTIONS; idx++)
    if (sap->other[idx].name)
      free (sap->other[idx].name);
  free (sap);
}


/* Parse the user's idea of an offset for dynamic linking, into our idea
   of how to represent it for fast symbol reading.  This is the default 
   version of the sym_fns.sym_offsets function for symbol readers that
   don't need to do anything special.  It allocates a section_offsets table
   for the objectfile OBJFILE and stuffs ADDR into all of the offsets.  */

void
default_symfile_offsets (struct objfile *objfile, struct section_addr_info *addrs)
{
  int i;
  asection *sect = NULL;

  objfile->num_sections = SECT_OFF_MAX;
  objfile->section_offsets = (struct section_offsets *)(void *)
    obstack_alloc (&objfile->psymbol_obstack, SIZEOF_SECTION_OFFSETS);
  memset (objfile->section_offsets, 0, SIZEOF_SECTION_OFFSETS);

  /* Now calculate offsets for section that were specified by the
     caller. */
  for (i = 0; i < MAX_SECTIONS && addrs->other[i].name; i++)
    {
      struct other_sections *osp ;

      osp = &addrs->other[i] ;
      if (osp->addr == 0)
  	continue;

      /* Record all sections in offsets */
      /* The section_offsets in the objfile are here filled in using
         the BFD index. */
      ANOFFSET (objfile->section_offsets, osp->sectindex) = osp->addr;
    }

  /* Remember the bfd indexes for the .text, .data, .bss and
     .rodata sections. */

  sect = bfd_get_section_by_name (objfile->obfd, ".text");
  if (sect) 
    objfile->sect_index_text = sect->index;

  sect = bfd_get_section_by_name (objfile->obfd, ".data");
  if (sect) 
    objfile->sect_index_data = sect->index;

  sect = bfd_get_section_by_name (objfile->obfd, ".bss");
  if (sect) 
    objfile->sect_index_bss = sect->index;

  sect = bfd_get_section_by_name (objfile->obfd, ".rodata");
  if (sect) 
    objfile->sect_index_rodata = sect->index;

}

/* Process a symbol file, as either the main file or as a dynamically
   loaded file.

   OBJFILE is where the symbols are to be read from.

   ADDR is the address where the text segment was loaded, unless the
   objfile is the main symbol file, in which case it is zero.

   MAINLINE is nonzero if this is the main symbol file, or zero if
   it's an extra symbol file such as dynamically loaded code.

   VERBO is nonzero if the caller has printed a verbose message about
   the symbol reading (and complaints can be more terse about it).  */

void
syms_from_objfile (struct objfile *objfile, struct section_addr_info *addrs,
                   int mainline, int verbo, int threshold_exceeded)
{
  asection *lower_sect;
  asection *sect;
  CORE_ADDR lower_offset;
  struct section_addr_info local_addr;
  struct cleanup *old_chain;
  int i;

  /* If ADDRS is NULL, initialize the local section_addr_info struct and
     point ADDRS to it.  We now establish the convention that an addr of
     zero means no load address was specified. */

  if (addrs == NULL)
    {
      memset (&local_addr, 0, sizeof (local_addr));
      addrs = &local_addr;
    }

  init_entry_point_info (objfile);
  find_sym_fns (objfile);

  /* Make sure that partially constructed symbol tables will be cleaned up
     if an error occurs during symbol reading.  */
  old_chain = make_cleanup_free_objfile (objfile);

  if (mainline)
    {
      /* We will modify the main symbol table, make sure that all its users
         will be cleaned up if an error occurs during symbol reading.  */
      make_cleanup (clear_symtab_users_cleanup, 0 /*ignore*/);

      /* Since no error yet, throw away the old symbol table.  */

      if (symfile_objfile != NULL)
	{
	  free_objfile (symfile_objfile);
	  symfile_objfile = NULL;
	  objfile_purge_solibs ();
	}

      /* Currently we keep symbols from the add-symbol-file command.
         If the user wants to get rid of them, they should do "symbol-file"
         without arguments first.  Not sure this is the best behavior
         (PR 2207).  */

      (*objfile->sf->sym_new_init) (objfile);
    }

  /* Convert addr into an offset rather than an absolute address.
     We find the lowest address of a loaded segment in the objfile,
     and assume that <addr> is where that got loaded. */
  if (!mainline)
    {
      /* Find lowest loadable section to be used as starting point for 
         contiguous sections. FIXME!! won't work without call to find
	 .text first, but this assumes text is lowest section. */
      /* JAGaf08152 15/02/05 Sunil S 
         add-symbol-file command fails for gdb32. Name of text section in PA32 
         is $TEXT$ and for IA and PA64 it is .text */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
      lower_sect = bfd_get_section_by_name (objfile->obfd, ".text");
#else
      lower_sect = bfd_get_section_by_name (objfile->obfd, "$TEXT$");
#endif
      if (lower_sect == NULL)
	bfd_map_over_sections (objfile->obfd, find_lowest_section,
			       (PTR) &lower_sect);
      if (lower_sect == NULL)
	warning ("no loadable sections found in added symbol-file %s",
		 objfile->name);
      /* JYG: MERGE FIXME: why do we need this warning?
      else 
	if ((bfd_get_section_flags (objfile->obfd, lower_sect) & SEC_CODE) == 0)
	  warning ("Lowest section in %s is %s at %s",
		   objfile->name,
		   bfd_section_name (objfile->obfd, lower_sect),
		   paddr (bfd_section_vma (objfile->obfd, lower_sect)));
      */
      if (lower_sect != NULL)
 	lower_offset = bfd_section_vma (objfile->obfd, lower_sect);
      else
 	lower_offset = 0;
 
       /* Calculate offsets for the loadable sections.
 	 FIXME! Sections must be in order of increasing loadable section
 	 so that contiguous sections can use the lower-offset!!!
 
          Adjust offsets if the segments are not contiguous.
          If the section is contiguous, its offset should be set to
 	 the offset of the highest loadable section lower than it
 	 (the loadable section directly below it in memory).
 	 this_offset = lower_offset = lower_addr - lower_orig_addr */

       /* Calculate offsets for sections. */
      for (i=0 ; i < MAX_SECTIONS && addrs->other[i].name; i++)
	{
	  if (addrs->other[i].addr != 0)
 	    {
 	      sect = bfd_get_section_by_name (objfile->obfd, addrs->other[i].name);
		
 	      if (sect)
 		{
 		  addrs->other[i].addr -= bfd_section_vma (objfile->obfd, sect);
 		  lower_offset = addrs->other[i].addr;
		  /* This is the index used by BFD. */
		  addrs->other[i].sectindex = sect->index ;
 		}
 	      else
		{
		  /* set .data offset properly even for the libraries that donot have
                     .data section. Since we use this to get the data_start. */
		  if ((strcmp (addrs->other[i].name, ".data")) &&
		  	(strcmp (addrs->other[i].name, ".text")))
		    {
		      warning ("section %s not found in %s", addrs->other[i].name,
                               objfile->name);
		      addrs->other[i].addr = 0;
		    }
		}
 	    }
 	  else
 	    addrs->other[i].addr = lower_offset;
	}
    }

  /* Initialize symbol reading routines for this objfile, allow complaints to
     appear for this new file, and record how verbose to be, then do the
     initial symbol reading for this file. */

  (*objfile->sf->sym_init) (objfile, threshold_exceeded);
  clear_complaints (1, verbo);

  (*objfile->sf->sym_offsets) (objfile, addrs);

#ifndef IBM6000_TARGET
  /* This is a SVR4/SunOS specific hack, I think.  In any event, it
     screws RS/6000.  sym_offsets should be doing this sort of thing,
     because it knows the mapping between bfd sections and
     section_offsets.  */
  /* This is a hack.  As far as I can tell, section offsets are not
     target dependent.  They are all set to addr with a couple of
     exceptions.  The exceptions are sysvr4 shared libraries, whose
     offsets are kept in solib structures anyway and rs6000 xcoff
     which handles shared libraries in a completely unique way.

     Section offsets are built similarly, except that they are built
     by adding addr in all cases because there is no clear mapping
     from section_offsets into actual sections.  Note that solib.c
     has a different algorithm for finding section offsets.

     These should probably all be collapsed into some target
     independent form of shared library support.  FIXME.  */
  if (addrs)
    {
      struct obj_section *s;

      /* Map section offsets in "addr" back to the object's 
      	 sections by comparing the section names with bfd's 
	 section names.  Then adjust the section address by
	 the offset. */ /* for gdb/13815 */
 
      ALL_OBJFILE_OSECTIONS (objfile, s)
	{
	  CORE_ADDR s_addr = 0;
	  int i;

	  for (i = 0; 
	       !s_addr && i < MAX_SECTIONS && addrs->other[i].name;
	       i++)
	    if (! strcmp (s->the_bfd_section->name, addrs->other[i].name))
	      s_addr = addrs->other[i].addr; /* end added for gdb/13815 */
	  /* JYG: MERGE NOTE: This is a hack to set s_addr to text addr
	     (index 0 assumed), for SOM */
	  if (! s_addr)
	    s_addr = addrs->other[0].addr;
	  
	  s->addr -= s->offset;
	  s->addr += s_addr;
	  s->endaddr -= s->offset;
	  s->endaddr += s_addr;
	  s->offset += s_addr;
	}
    }
#endif /* not IBM6000_TARGET */

  (*objfile->sf->sym_read) (objfile, mainline, threshold_exceeded);

  /* Don't allow char * to have a typename (else would get caddr_t).
     Ditto void *.  FIXME: Check whether this is now done by all the
     symbol readers themselves (many of them now do), and if so remove
     it from here.  */

  TYPE_NAME (lookup_pointer_type (builtin_type_char)) = 0;
  TYPE_NAME (lookup_pointer_type (builtin_type_void)) = 0;

  /* Mark the objfile has having had initial symbol read attempted.  Note
     that this does not mean we found any symbols... */

  objfile->flags |= OBJF_SYMS;

  /* Discard cleanups as symbol reading was successful.  */

  discard_cleanups (old_chain);

  /* Call this after reading in a new symbol table to give target
     dependant code a crack at the new symbols.  For instance, this
     could be used to update the values of target-specific symbols GDB
     needs to keep track of (such as _sigtramp, or whatever).  */

  TARGET_SYMFILE_POSTREAD (objfile);
}

/* Perform required actions after either reading in the initial
   symbols for a new objfile, or mapping in the symbols from a reusable
   objfile. */

void
new_symfile_objfile (struct objfile *objfile, int mainline, int verbo)
{
    int debug_symbols_not_found = 0;

  /* If this is the main symbol file we have to clean up all users of the
     old main symbol file. Otherwise it is sufficient to fixup all the
     breakpoints that may have been redefined by this symbol file.  */
  if (mainline)
    {
      /* OK, make it the "real" symbol file.  */
      symfile_objfile = objfile;

      reset_all_breakpoints = 1;
      clear_symtab_users ();
      reset_all_breakpoints = 0;

      endo_missing_in_symfile_objfile = found_endo_export_list = 0;

#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W

      /* Srikanth, support for command line calls even when the a.out is
         stripped or not linked with end.o.

         Probe for a known symbol from end.o to see if it is linked in.
       */
       /*
	   Earlier, this code was looking up for the symbol , __d_shl_get, in 
           lookup_minimal_symbol(). This could be a problem when an application
           was stripped with "strip -x" without stripping the minsym. So, 
           gdb assumed that the executable is not stripped as it finds
           the symbol __d_shl_get in minimal symbols. This has caused
	   SIGILL's in a customer application.
              Now, we lookup up for the debug info as well to identify
           these kind of stripped cases.   
        */
     if (!have_partial_symbols () && !have_full_symbols ())
          debug_symbols_not_found = 1;

     if ( (!lookup_minimal_symbol ("__d_shl_get", NULL, symfile_objfile))
          || debug_symbols_not_found )
        {
          if (find_symbol_in_export_list ("__d_shl_get", symfile_objfile))
	      found_endo_export_list = 1;
	   else
               endo_missing_in_symfile_objfile = 1;
        }

      /* This flag is used to trigger special action while performing 
	 command-line calls in stripped a.out
      */
#endif
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
      /* JAGaf39799 - Support for command line calls even when the a.out is
         not linked with end.o.

         Probe for a known symbol from end.o to see if it is linked in.
      */
      struct minimal_symbol * wdb_call_dummy_sym;
      wdb_call_dummy_sym = lookup_minimal_symbol ("__wdb_call_dummy", NULL,
                                                   NULL);
      if (wdb_call_dummy_sym)
        {
            /* 
               Check whether the function addr has come from
               .dynsym section (export list), in that case we
                have to get the function addr from location
                where opd points
            */

	    struct obj_section *funcsect = find_pc_section(SYMBOL_VALUE_ADDRESS(wdb_call_dummy_sym));
	    if (funcsect
		    && STREQ (funcsect->the_bfd_section->name, ".opd"))
               {
                   SYMBOL_VALUE_ADDRESS(wdb_call_dummy_sym) =
                      (CORE_ADDR) read_memory_integer(SYMBOL_VALUE_ADDRESS(wdb_call_dummy_sym)+16,sizeof (CORE_ADDR));
               }
           endo_missing_in_symfile_objfile = 0;
       }
      else
           endo_missing_in_symfile_objfile = 1;

#endif /* GDB_TARGET_IS_HPPA_20W */

    }
  else
    {
      breakpoint_re_set ();
    }

  /* We're done reading the symbol file; finish off complaints.  */
  clear_complaints (0, verbo);
}


/* JAGag07432 - Sunil S
   Variable is set if add_symbol_file_command function is called on PA32.
*/
#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
boolean add_symbol_file_cmd = FALSE;
#endif
#endif

/* Process a symbol file, as either the main file or as a dynamically
   loaded file.

   NAME is the file name (which will be tilde-expanded and made
   absolute herein) (but we don't free or modify NAME itself).
   FROM_TTY says how verbose to be.  MAINLINE specifies whether this
   is the main symbol file, or whether it's an extra symbol file such
   as dynamically loaded code.  If !mainline, ADDR is the address
   where the text segment was loaded.

   Upon success, returns a pointer to the objfile that was added.
   Upon failure, jumps back to command level (never returns). */

struct objfile *
maybe_symbol_file_add (char *name, int from_tty, 
                       struct section_addr_info *addrs,
                       int mainline, int flags, int threshold_exceeded)
{
  struct objfile *objfile;
  struct partial_symtab *psymtab;
  char *debugfile = NULL;
  struct section_addr_info *orig_addrs = NULL;
  bfd *abfd;

  /* Open a bfd for the file, and give user a chance to burp if we'd be
     interactively wiping out any existing symbols.  */

  abfd = symfile_bfd_open (name);
#ifdef BFD_IS_SWIZZLED
  is_swizzled = BFD_IS_SWIZZLED (abfd);

  if (is_swizzled)
    {
      /* is_sizzld -> ILP32 */
      /* long and unsigned long are 32-bit */
      TYPE_LENGTH (builtin_type_long) = 4;
      TYPE_LENGTH (builtin_type_unsigned_long) = 4;
    }
  else
    {
      /* !is_sizzld -> ILP64 */
      /* long and unsigned long are 64-bit */
      /* reset values when a 64-bit file is loaded after a 32-bit file */
      TYPE_LENGTH (builtin_type_long) 
        = TARGET_LONG_BIT / TARGET_CHAR_BIT;
      TYPE_LENGTH (builtin_type_unsigned_long) 
        = TARGET_LONG_BIT / TARGET_CHAR_BIT;
    }
#endif

#ifdef GDB_TARGET_IS_HPUX
  /* The following code is HP-specific.  The "right" way of
     doing this is unknown, but we bet would involve a target-
     specific pre-file-load check using a generic mechanism. */
  
  hpread_init_debug_in_object_files (abfd);

  if (hpread_pxdb_needed (abfd))
    {
      /* This file has not been pre-processed by pxdb to
	 massage symbols, do it now */

      if (hpread_call_pxdb (name))
        {
	  /* The on-disk file has been pre-processed, we need
	     to close the re-open the file */
	  bfd_close (abfd);
	  abfd = symfile_bfd_open (name);
	}
      else
	/* JYG: MERGE FIXME: what about the GNU tool chain?
	   We probably need to support that in hpread_pxdb_needed */
	/* we fail miserably without pxdb!!! */
	/* the warning to the user has already been given
	   from within the call_pxdb function */
	error ("Command ignored.\n");
    }
#endif

  if ((have_full_symbols () || have_partial_symbols ())
      && mainline
      && from_tty
      && !query ("Load new symbol table from \"%s\"? ", name))
    error ("Not confirmed.");

  objfile = allocate_objfile (abfd, flags);

  /* Temporarily set up text_low and text high. It's needed by the dwarf 
     reader to ignore procelim'ed dies. */

  /* QXCR1000805486: GDB crashes when dereference a null pointer. */    
 
  if (addrs)
    {
      objfile->text_low = addrs->other[0].addr;
      orig_addrs = copy_section_addr_info (addrs);
      make_cleanup_free_section_addr_info (orig_addrs);
    } 
  
  objfile->text_high = objfile->text_low + objfile->obfd->text_size;

  /* If the objfile uses a mapped symbol file, and we have a psymtab for
     it, then skip reading any symbols at this time. */

  if ((objfile->flags & OBJF_MAPPED) && (objfile->flags & OBJF_SYMS))
    {
      /* We mapped in an existing symbol table file that already has had
         initial symbol reading performed, so we can skip that part.  Notify
         the user that instead of reading the symbols, they have been mapped.
       */
      if (from_tty || info_verbose)
	{
	  printf_filtered ("Mapped symbols for %s...", name);
	  wrap_here ("");
	  gdb_flush (gdb_stdout);
	}
      init_entry_point_info (objfile);
      find_sym_fns (objfile);
    }
  else
    {
      /* We either created a new mapped symbol table, mapped an existing
         symbol table file which has not had initial symbol reading
         performed, or need to read an unmapped symbol table. */
      if (from_tty || info_verbose || annotation_level ==2) //For JAGaf45739
	{
	  if (pre_add_symbol_hook)
	    pre_add_symbol_hook (name);
	  else
	    {
              if (!batch_rtc && !profile_on)
		printf_filtered ("Reading symbols from %s...", name);
	      wrap_here ("");
	      gdb_flush (gdb_stdout);
	    }
	}
      syms_from_objfile (objfile, addrs, mainline, from_tty,
			 threshold_exceeded);
    }

  /* We now have at least a partial symbol table.  Check to see if the
     user requested that all symbols be read on initial access via either
     the gdb startup command line or on a per symbol file basis.  Expand
     all partial symbol tables for this objfile if so. */

  if ((flags & OBJF_READNOW) || readnow_symbol_files)
    {
      if (from_tty || info_verbose)
	{
	  printf_filtered ("expanding to full symbols...");
	  wrap_here ("");
	  gdb_flush (gdb_stdout);
	}

      for (psymtab = objfile->psymtabs;
	   psymtab != NULL;
	   psymtab = psymtab->next)
	{
	  psymtab_to_symtab (psymtab);
	}
    }

  /* If the file has its own symbol tables it has no separate debug info.
     `.dynsym'/`.symtab' go to MSYMBOLS, `.debug_info' goes to SYMTABS/PSYMTABS.
     `.gnu_debuglink' may no longer be present with `.note.gnu.build-id'.  */

  /* Always check for .gnu_debuglink section, as object files may have been
     compiled with +objdebug or +noobjdebug. For the +objdebug objects,
     the debug information is there in the object files, whereas the debug
     information for files compiled with +noobjdebug be in the side debug
     file, if one was generated.

     Hence, commenting out the psymtab check before caling
     find_separate_debug_file. This will ensure that gdb reads the debug
     info for +noobjdebug objects from the side file, even if we already
     have psymtabs due to +objdebug object files. Since we create the
     separate_debug_objfile in addition to the existing objfile, this should
     work fine.

     // if (objfile->psymtabs == NULL)
  */
  debugfile = find_separate_debug_file (objfile);
  if (debugfile)
    {
      if (debug_traces) // add verbose too ?
        {
          printf_filtered ("Found separate debug file %s...\n",
                           debugfile);
        }
      if (addrs != NULL)
        {
          objfile->separate_debug_objfile
            = symbol_file_add (debugfile, from_tty, orig_addrs, 0, flags);
        }
      else
        {
          objfile->separate_debug_objfile
            = symbol_file_add (debugfile, from_tty, NULL, 0, flags);
        }
      objfile->separate_debug_objfile->separate_debug_objfile_backlink
        = objfile;

      /* Put the separate debug object before the normal one, this is so that
         usage of the ALL_OBJFILES_SAFE macro will stay safe. */
      put_objfile_before (objfile->separate_debug_objfile, objfile);
      free (debugfile);
    }

  if (!profile_on)  /* Just shut up if profiling under prospect. */
    {
      if (!have_partial_symbols () && !have_full_symbols ())
        {
          wrap_here ("");
          if (!threshold_exceeded)
            printf_filtered ("(no debugging symbols found)...");
          else
            printf_filtered ("(skipped since threshold was exceeded)...");
          wrap_here ("");
        }
    }

  if (from_tty || info_verbose)
    {
      if (post_add_symbol_hook)
	post_add_symbol_hook ();
      else
	{
	  printf_filtered ("done.\n");
	  gdb_flush (gdb_stdout);
	}
    }

  new_symfile_objfile (objfile, mainline, from_tty);

  if (target_new_objfile_hook)
    target_new_objfile_hook (objfile);
#ifdef HP_MXN
  /* srikanth, 000809, Notify whoever wants to be notified that a symbol
     table has just been digested. This would be useful for instance,
     to detect if an application is MxN threaded. The detection is
     based on the presence of a signature symbol which could come from
     a shared library. I am not using the post_add_symbol_hook as it is
     used by UI modules and we don't want to inadvertantly change
     anything there.
  */

  if (symbol_table_load_hook)
    symbol_table_load_hook (name);

  if (libjvm_load_hook)
    libjvm_load_hook (name);
#endif

  return (objfile);
}

struct objfile *
symbol_file_add (char *name, int from_tty, struct section_addr_info *addrs,
                 int mainline, int flags)
{
  return maybe_symbol_file_add (name, from_tty, addrs, mainline, flags,
				0);
}

// Not supporting build id right away, let the CRC be
// accepted as of now.
#if 0
struct build_id
  {
    size_t size;
    gdb_byte data[1];
  };

/* Locate NT_GNU_BUILD_ID from ABFD and return its content.  */

static struct build_id *
build_id_bfd_get (bfd *abfd)
{
  struct build_id *retval;

  if (!bfd_check_format (abfd, bfd_object)
      || bfd_get_flavour (abfd) != bfd_target_elf_flavour
      || elf_tdata (abfd)->build_id == NULL)
    return NULL;

  retval = xmalloc (sizeof *retval - 1 + elf_tdata (abfd)->build_id_size);
  retval->size = elf_tdata (abfd)->build_id_size;
  memcpy (retval->data, elf_tdata (abfd)->build_id, retval->size);

  return retval;
}


/* Return if FILENAME has NT_GNU_BUILD_ID matching the CHECK value.  */

static int
build_id_verify (const char *filename, struct build_id *check)
{
  bfd *abfd;
  struct build_id *found = NULL;
  int retval = 0;

  /* We expect to be silent on the non-existing files.  */
  abfd = bfd_openr (filename, gnutarget);
  if (abfd == NULL)
    return 0;

  found = build_id_bfd_get (abfd);

  if (found == NULL)
    warning (("File \"%s\" has no build-id, file skipped"), filename);
  else if (found->size != check->size
           || memcmp (found->data, check->data, found->size) != 0)
    warning (("File \"%s\" has a different build-id, file skipped"), filename);
  else
    retval = 1;

  if (!bfd_close (abfd))
    warning (("cannot close \"%s\": %s"), filename,
             bfd_errmsg (bfd_get_error ()));
  return retval;
}

static char *
build_id_to_debug_filename (struct build_id *build_id)
{
  char *link, *s, *retval = NULL;
  gdb_byte *data = build_id->data;
  size_t size = build_id->size;

  /* DEBUG_FILE_DIRECTORY/.build-id/ab/cdef */
  link = xmalloc (strlen (debug_file_directory) + (sizeof "/.build-id/" - 1) + 1
                  + 2 * size + (sizeof ".debug" - 1) + 1);
  s = link + sprintf (link, "%s/.build-id/", debug_file_directory);
  if (size > 0)
    {
      size--;
      s += sprintf (s, "%02x", (unsigned) *data++);
    }
  if (size > 0)
    *s++ = '/';
  while (size-- > 0)
    s += sprintf (s, "%02x", (unsigned) *data++);
  strcpy (s, ".debug");

  /* lrealpath() is expensive even for the usually non-existent files.  */
  if (access (link, F_OK) == 0)
    retval = lrealpath (link);
  free (link);

  if (retval != NULL && !build_id_verify (retval, build_id))
    {
      free (retval);
      retval = NULL;
    }


  return retval;
}
#endif // if 0, no build id support yet

static char *
get_debug_link_info (struct objfile *objfile, unsigned long *crc32_out)
{
  asection *sect;
  bfd_size_type debuglink_size;
  unsigned long crc32;
  char *contents;
  int crc_offset;
  unsigned char *p;

  sect = bfd_get_section_by_name (objfile->obfd, ".gnu_debuglink");

  if (sect == NULL)
    return NULL;

  debuglink_size = bfd_section_size (objfile->obfd, sect);

  contents = xmalloc (debuglink_size);
  bfd_get_section_contents (objfile->obfd, sect, contents,
                            (file_ptr)0, (bfd_size_type)debuglink_size);

  /* Crc value is stored after the filename, aligned up to 4 bytes. */
  crc_offset = strlen (contents) + 1;
  crc_offset = (crc_offset + 3) & ~3;

  crc32 = bfd_get_32 (objfile->obfd, (bfd_byte *) (contents + crc_offset));

  *crc32_out = crc32;
  return contents;
}

static int
separate_debug_file_exists (const char *name, unsigned long crc)
{
  unsigned long file_crc = 0;
  int fd;
  unsigned char buffer[8*1024];
  int count;

  fd = open (name, O_RDONLY | O_BINARY);
  if (fd < 0)
    return 0;

  if (debug_traces)
    printf_filtered("Opening debug file %s...\n", name);

  while ((count = read (fd, buffer, sizeof (buffer))) > 0)
    file_crc = gnu_debuglink_crc32 (file_crc, buffer, count);

  close (fd);


  return crc == file_crc;
}

char *debug_file_directory = NULL; // "/tmp"; 

#if 0
static void
show_debug_file_directory (struct ui_file *file, int from_tty,
                           struct cmd_list_element *c, const char *value)
{
  fprintf_filtered (file, ("\
The directory where separate debug symbols are searched for is \"%s\".\n"),
                    value);
}
#endif

#if ! defined (DEBUG_SUBDIRECTORY)
#define DEBUG_SUBDIRECTORY ".debug"
#endif

#define IS_DIR_SEPARATOR(c)     ((c) == '/' || (c) == '\\')

static char *
find_separate_debug_file (struct objfile *objfile)
{
  asection *sect;
  char *basename;
  char *dir;
  char *debugfile;
  char *name_copy;
  char *canon_name;
  bfd_size_type debuglink_size;
  unsigned long crc32;
  int i;
/* No build id support yet */
#if 0
  struct build_id *build_id;

  build_id = build_id_bfd_get (objfile->obfd);
  if (build_id != NULL)
    {
      char *build_id_name;

      build_id_name = build_id_to_debug_filename (build_id);
      free (build_id);
      /* Prevent looping on a stripped .debug file.  */
      if (build_id_name != NULL && strcmp (build_id_name, objfile->name) == 0)
        {
          warning (("\"%s\": separate debug info file has no debug info"),
                   build_id_name);
          free (build_id_name);
        }
      else if (build_id_name != NULL)
        return build_id_name;
    }
#endif

  basename = get_debug_link_info (objfile, &crc32);

  if (basename == NULL)
    return NULL;

  dir = xstrdup (objfile->name);

  /* Strip off the final filename part, leaving the directory name,
     followed by a slash.  Objfile names should always be absolute and
     tilde-expanded, so there should always be a slash in there
     somewhere.  */
  for (i = strlen(dir) - 1; i >= 0; i--)
    {
      if (IS_DIR_SEPARATOR (dir[i]))
        break;
    }

  assert (i >= 0 && IS_DIR_SEPARATOR (dir[i]));
  dir[i+1] = '\0';

  debugfile = (char *)alloca (strlen (debug_file_directory) + 1
                              + strlen (dir)
                              + strlen (DEBUG_SUBDIRECTORY)
                              + strlen ("/")
                              + strlen (basename)
                              + 1);


  /* First try in the same directory as the original file.  */
  strcpy (debugfile, dir);
  strcat (debugfile, basename);

  if (separate_debug_file_exists (debugfile, crc32))
    {
      free (basename);
      free (dir);
      return xstrdup (debugfile);
    }

  /* Then try in the subdirectory named DEBUG_SUBDIRECTORY.  */
  strcpy (debugfile, dir);
  strcat (debugfile, DEBUG_SUBDIRECTORY);
  strcat (debugfile, "/");
  strcat (debugfile, basename);

  if (separate_debug_file_exists (debugfile, crc32))
    {
      free (basename);
      free (dir);
      return xstrdup (debugfile);
    }

  /* Then try in the global debugfile directory.  */
  strcpy (debugfile, debug_file_directory);
  strcat (debugfile, "/");
  strcat (debugfile, dir);
  strcat (debugfile, basename);

  if (separate_debug_file_exists (debugfile, crc32))
    {
      free (basename);
      free (dir);
      return xstrdup (debugfile);
    }

  /* See if the environment variable GDB_DEBUG_ABSPATH is set */
  char *gdb_debug_abspath = getenv ("GDB_DEBUG_ABSPATH");
  if (gdb_debug_abspath)
    {
      strcpy (debugfile, gdb_debug_abspath);
      strcat (debugfile, "/");
      strcat (debugfile, basename);

      if (separate_debug_file_exists (debugfile, crc32))
        {
          free (basename);
          free (dir);
          return xstrdup (debugfile);
        }
    }

/* no gdb_sysroot yet */
#if 0
  /* If the file is in the sysroot, try using its base path in the
     global debugfile directory.  */
  canon_name = lrealpath (dir);
  if (canon_name
      && strncmp (canon_name, gdb_sysroot, strlen (gdb_sysroot)) == 0
      && IS_DIR_SEPARATOR (canon_name[strlen (gdb_sysroot)]))
    {
      strcpy (debugfile, debug_file_directory);
      strcat (debugfile, canon_name + strlen (gdb_sysroot));
      strcat (debugfile, "/");
      strcat (debugfile, basename);

      if (separate_debug_file_exists (debugfile, crc32))
        {
          free (canon_name);
          free (basename);
          free (dir);
          return xstrdup (debugfile);
        }
    }

  if (canon_name)
    free (canon_name);
#endif // no gdb_sysroot

  free (basename);
  free (dir);
  return NULL;
}

/* This is the symbol-file command.  Read the file, analyze its
   symbols, and add a struct symtab to a symtab list.  The syntax of
   the command is rather bizarre--(1) buildargv implements various
   quoting conventions which are undocumented and have little or
   nothing in common with the way things are quoted (or not quoted)
   elsewhere in GDB, (2) options are used, which are not generally
   used in GDB (perhaps "set mapped on", "set readnow on" would be
   better), (3) the order of options matters, which is contrary to GNU
   conventions (because it is confusing and inconvenient).  */
/* Note: ezannoni 2000-04-17. This function used to have support for
   rombug (see remote-os9k.c). It consisted of a call to target_link()
   (target.c) to get the address of the text segment from the target,
   and pass that to symbol_file_add(). This is no longer supported. */

void
symbol_file_command (char *args, int from_tty)
{
#ifdef HPPA_FIX_AND_CONTINUE
  extern void cleanup_fixinfo();
#endif

  char **argv;
  char *name = NULL;
  struct cleanup *cleanups;
  int flags = OBJF_USERLOADED;

  dont_repeat ();

  if (real_exec_file)
    free (real_exec_file);

  real_exec_file = (args ? strdup (args) : NULL);

  if (debugging_aries)
    args = "/usr/lib/hpux32/pa_boot32.so";
  else if (debugging_aries64)
    args = "/usr/lib/hpux64/pa_boot64.so";

  is_purified_program = 0;
  is_steplast_eligible = 0;

  if (args == NULL)
    {
      if ((have_full_symbols () || have_partial_symbols ())
	  && from_tty
	  && !query ("Discard symbol table from `%s'? ",
		     symfile_objfile->name))
	error ("Not confirmed.");
      free_all_objfiles ();

      /* solib descriptors may have handles to objfiles.  Since their
         storage has just been released, we'd better wipe the solib
         descriptors as well.
       */
#if defined(SOLIB_RESTART)
      SOLIB_RESTART ();
#endif

      symfile_objfile = NULL;
      if (from_tty)
	  printf_unfiltered ("No symbol file now.\n");
#ifdef GDB_TARGET_IS_HPUX
      RESET_HP_UX_GLOBALS ();
#endif
    }
  else
    {
      if ((argv = buildargv (args)) == NULL)
	{
	  nomem (0);
	}
      cleanups = make_cleanup_freeargv (argv);
      while (*argv != NULL)
	{
	  if (STREQ (*argv, "-mapped"))
	    flags |= OBJF_MAPPED;
	  else 
	    if (STREQ (*argv, "-readnow"))
	      flags |= OBJF_READNOW;
	    else 
	      if (**argv == '-')
		error ("unknown option `%s'", *argv);
	      else
		{
                  name = *argv;
#ifdef INLINE_SUPPORT
		  clear_inline_data();
#endif
		  symbol_file_add (name, from_tty, NULL, 1, flags);
#ifdef GDB_TARGET_IS_HPUX
		  RESET_HP_UX_GLOBALS ();
#endif
		  /* Getting new symbols may change our opinion about
		     what is frameless.  */
                  if (is_reinit_frame_cache)
                    reinit_frame_cache ();

		  set_initial_language ();
		}
	  argv++;
	}

      if (name == NULL)
	{
	  error ("no symbol file name was specified");
	}
      {
        if (nimbus_version)
          {
            CORE_ADDR addr;
            addr = parse_and_eval_address ("main");
            if (!addr)
              addr = parse_and_eval_address ("_MAIN_");
            if (addr)
              {
                struct symtab_and_line sal;

		/* Stacey 03/21/2002
		   The symbol and line table stores address info 
		   corresponding to the first instruction of a line.  We
		   can't rely solely on this info to get the PC because
		   users can step at the instruction level ... we need
		   to maintain our own pc and use this to calculate line
		   info if we want the nimbus status bar to be accurate */
		   
		nimbus_current_pc = addr;
                sal = find_pc_line (addr, 0);

		/* Give vim a file to display and the current function, line,
		   and PC or address info.  */
		
        	if (!assembly_level_debugging)
         	  {
		    /* Source level debugging - tell vim which source file to
		       display */
            	    printf_unfiltered ("\033\032\032:se stl=%s\n:e"
				       " +%d %s\n:se nu\n\032\032A",
				       wdb_status_line (sal), sal.line,
				       (sal.symtab && sal.symtab->fullname) ?
				       sal.symtab->fullname :
				       (sal.symtab && sal.symtab->filename) ?
				       sal.symtab->filename :
				       "/opt/langtools/lib/nofile.c");
          	  }
        	else 
		  {
		    /* In assembly level debugging, generate a file, fill
		       it with assembly code, and tell vim to display it.  */
		    
		    FILE *disassem_temp_fstream = NULL;
            	    char disassem_temp_file[80];
		    char gdb_pc[32];
                    CORE_ADDR low, high;
            	    char *name;

            	    if (find_pc_partial_function (nimbus_current_pc, &name,
						  &low, &high) == 0)
		      error ("No function contains specified address.\n");

#ifdef HP_IA64
              	    /* IA64: Instruction addresses are slot encoded. */
              	    /* Bundle-align LOW and HIGH */
              	    low &= ~(0xF);
              	    high &= ~(0xF);
#endif /*HP_IA64*/

            	    sprintf (disassem_temp_file, "/tmp/nimbus.%d/%llx-%llx", 
			     getpid(), low, high);
		    
		    if ( (disassem_temp_fstream = 
			  fopen (disassem_temp_file, "r")) == NULL)
		      {
			generate_disassembly_file (disassem_temp_file,
						   low, high);
		      }
		    if (disassem_temp_fstream != NULL)
		      fclose (disassem_temp_fstream);

		    strcat_address_numeric (nimbus_current_pc, 1, gdb_pc, 32);
              	    printf_unfiltered("\033\032\032:se stl=%s\n:e %s\n:/%s "
				      "<\n:se nonumber\n\032\032A",
				      wdb_status_line (sal), 
				      disassem_temp_file, gdb_pc);
          	  }
                gdb_flush (gdb_stdout);
              }
          }
      }

      TUIDO (((TuiOpaqueFuncPtr) tuiDisplayMainFunction));
      do_cleanups (cleanups);
    }
#ifdef PURIFY_SIGNATURE_SYMBOL
  is_purified_program = (0 != lookup_minimal_symbol (PURIFY_SIGNATURE_SYMBOL,
						     NULL, NULL));
#endif
#ifdef HPPA_FIX_AND_CONTINUE
  cleanup_fixinfo();
#endif

  /* Bindu 081004 Fix for JAGaf33164: 
     Try if we can re enable any deferred breakpoints. */
  re_enable_breakpoints_in_shlibs ();

}

/* Set the initial language.

   A better solution would be to record the language in the psymtab when reading
   partial symbols, and then use it (if known) to set the language.  This would
   be a win for formats that encode the language in an easily discoverable place,
   such as DWARF.  For stabs, we can jump through hoops looking for specially
   named symbols or try to intuit the language from the specific type of stabs
   we find, but we can't do that until later when we read in full symbols.
   FIXME.  */

static void
set_initial_language ()
{
  struct partial_symtab *pst;
  enum language lang = language_unknown;

  pst = find_main_psymtab ();
  if (pst != NULL)
    {
      lang = pst->language;

      if (lang == language_unknown)
	{
	  /* Make C the default language */
	  lang = language_c;
	}
      set_language (lang);
      expected_language = current_language;	/* Don't warn the user */
    }
}

/* Open file specified by NAME and hand it off to BFD for preliminary
   analysis.  Result is a newly initialized bfd *, which includes a newly
   malloc'd` copy of NAME (tilde-expanded and made absolute).
   In case of trouble, error() is called.  */

bfd *
symfile_bfd_open (char *name)
{
  return symfile_bfd_open_with_path_type(name, getenv("PATH"),
                                         bfd_object, 1, 1);
}

bfd *
symfile_bfd_open_with_path_type (char *name, char *path, bfd_format expect,
                                  int is_verbose_error, int try_cwd_first)
{
  bfd *sym_bfd;
  int desc;
  char *absolute_name;

  name = tilde_expand (name);	/* Returns 1st new malloc'd copy */

  /* Look down path for it, allocate 2nd new malloc'd copy.  */
  desc = openp (path, try_cwd_first, name, O_RDONLY | O_BINARY, 0, &absolute_name);
#if defined(__GO32__) || defined(_WIN32)
  if (desc < 0)
    {
      char *exename = alloca (strlen (name) + 5);
      strcat (strcpy (exename, name), ".exe");
      desc = openp (getenv ("PATH"), 1, exename, O_RDONLY | O_BINARY,
		    0, &absolute_name);
    }
#endif
  if (desc < 0)
    {
      make_cleanup (free, name);
      if (is_verbose_error)
        {
	  perror_with_name (name);
	}
      else
        {
	  return NULL;
	}
    }
  free (name);			/* Free 1st new malloc'd copy */
  name = absolute_name;		/* Keep 2nd malloc'd copy in bfd */
  /* It'll be freed in free_objfile(). */

#ifdef HP_XMODE
      /*
       * For cross mode support between 32-bit and 64-bit applications.
       * If the core file format is different from what this
       * gdb understands, launch the gdb that will understand
       * this core file.
       */
      
  if (xmode_exec_format_is_different (name))
    {
      if (exec_bfd == NULL && core_bfd == NULL)
        xmode_launch_other_gdb (name, xmode_symfile);
      else {
        if (exec_bfd) 
          {
            printf("Symbol file (%s) is in different format from executable\n",
		   absolute_name);
            error("To unload the executable, use the \"file\" command with no arguments\n");
          }
        else if (core_bfd) 
          {
            printf("Symbol file (%s) is in different format from the core file\n",
		   absolute_name);
            error("To unload the core file, use the \"core-file\" command with no arguments\n");
          }
      }
    }
#endif

  sym_bfd = bfd_fdopenr (name, gnutarget, desc);
  if (!sym_bfd)
    {
      close (desc);
      make_cleanup (free, name);
      if (is_verbose_error)
        {
	  error ("\"%s\": can't open to read symbols: %s.", name,
		 bfd_errmsg (bfd_get_error ()));
	}
      else
        {
	  return NULL;
	}
    }
  sym_bfd->cacheable = true;

  if (  !bfd_check_format (sym_bfd, expect)
#ifdef HP_IA64
  /* jini: mixed mode support: JAGag21714 */
      && (bfd_get_flavour (sym_bfd) != bfd_target_som_flavour)
#endif
     )
    {
      /* FIXME: should be checking for errors from bfd_close (for one thing,
         on error it does not free all the storage associated with the
         bfd).  */
      bfd_close (sym_bfd);	/* This also closes desc */
      make_cleanup (free, name);
      if (is_verbose_error)
        {
	  error ("\"%s\": can't read symbols: %s.", name,
		 bfd_errmsg (bfd_get_error ()));
	}
      else
        {
	  return NULL;
	}
    }
  return (sym_bfd);
}

/* Link a new symtab_fns into the global symtab_fns list.  Called on gdb
   startup by the _initialize routine in each object file format reader,
   to register information about each format the the reader is prepared
   to handle. */

void
add_symtab_fns (struct sym_fns *sf)
{
  sf->next = symtab_fns;
  symtab_fns = sf;
}


/* Initialize to read symbols from the symbol file sym_bfd.  It either
   returns or calls error().  The result is an initialized struct sym_fns
   in the objfile structure, that contains cached information about the
   symbol file.  */

static void
find_sym_fns (struct objfile *objfile)
{
  struct sym_fns *sf;
  enum bfd_flavour our_flavour = bfd_get_flavour (objfile->obfd);
  char *our_target = bfd_get_target (objfile->obfd);

  /* Special kludge for apollo.  See dstread.c.  */
  if (STREQN (our_target, "apollo", 6))
    our_flavour = (enum bfd_flavour) -2;

  for (sf = symtab_fns; sf != NULL; sf = sf->next)
    {
      if (our_flavour == sf->sym_flavour)
	{
	  objfile->sf = sf;
	  return;
	}
    }
  error ("I'm sorry, Dave, I can't do that.  Symbol format `%s' unknown.",
	 bfd_get_target (objfile->obfd));
}

/* This function runs the load command of our current target.  */

static void
load_command (char *arg, int from_tty)
{
  if (arg == NULL)
    arg = get_exec_file (1);
  target_load (arg, from_tty);
}

/* This version of "load" should be usable for any target.  Currently
   it is just used for remote targets, not inftarg.c or core files,
   on the theory that only in that case is it useful.

   Avoiding xmodem and the like seems like a win (a) because we don't have
   to worry about finding it, and (b) On VMS, fork() is very slow and so
   we don't want to run a subprocess.  On the other hand, I'm not sure how
   performance compares.  */

static int download_write_size = 512;
static int validate_download = 0;

void
generic_load (char *args, int from_tty)
{
  asection *s;
  bfd *loadfile_bfd;
  time_t start_time, end_time;	/* Start and end times of download */
  unsigned long data_count = 0;	/* Number of bytes transferred to memory */
  unsigned long write_count = 0;	/* Number of writes needed. */
  unsigned long load_offset;	/* offset to add to vma for each section */
  char *filename;
  struct cleanup *old_cleanups;
  char *offptr;
  CORE_ADDR total_size = 0;
  CORE_ADDR total_sent = 0;

  /* Parse the input argument - the user can specify a load offset as
     a second argument. */
  filename = xmalloc (strlen (args) + 1);
  old_cleanups = make_cleanup (free, filename);
  strcpy (filename, args);
  offptr = strchr (filename, ' ');
  if (offptr != NULL)
    {
      char *endptr;
      load_offset = strtoul (offptr, &endptr, 0);
      if (offptr == endptr)
	error ("Invalid download offset:%s\n", offptr);
      *offptr = '\0';
    }
  else
    load_offset = 0;

  /* Open the file for loading. */
  loadfile_bfd = bfd_openr (filename, gnutarget);
  if (loadfile_bfd == NULL)
    {
      perror_with_name (filename);
      return;
    }

  /* FIXME: should be checking for errors from bfd_close (for one thing,
     on error it does not free all the storage associated with the
     bfd).  */
  make_cleanup_bfd_close (loadfile_bfd);

  if (!bfd_check_format (loadfile_bfd, bfd_object))
    {
      error ("\"%s\" is not an object file: %s", filename,
	     bfd_errmsg (bfd_get_error ()));
    }

  for (s = loadfile_bfd->sections; s; s = s->next)
    if (s->flags & SEC_LOAD)
      total_size += bfd_get_section_size_before_reloc (s);

  start_time = time (NULL);

  for (s = loadfile_bfd->sections; s; s = s->next)
    {
      if (s->flags & SEC_LOAD)
	{
	  CORE_ADDR size = bfd_get_section_size_before_reloc (s);
	  if (size > 0)
	    {
	      char *buffer;
	      struct cleanup *old_chain;
	      CORE_ADDR lma = s->lma + load_offset;
	      CORE_ADDR block_size;
	      int err;
	      const char *sect_name = bfd_get_section_name (loadfile_bfd, s);
	      CORE_ADDR sent;

	      if (download_write_size > 0 && size > download_write_size)
		block_size = download_write_size;
	      else
		block_size = size;

	      buffer = xmalloc (size);
	      old_chain = make_cleanup (free, buffer);

	      /* Is this really necessary?  I guess it gives the user something
	         to look at during a long download.  */
#ifdef UI_OUT
	      ui_out_message (uiout, 0, "Loading section %s, size 0x%s lma 0x%s\n",
			   sect_name, paddr_nz (size), paddr_nz (lma));
#else
	      fprintf_unfiltered (gdb_stdout,
				  "Loading section %s, size 0x%s lma 0x%s\n",
				  sect_name, paddr_nz (size), paddr_nz (lma));
#endif

	      bfd_get_section_contents (loadfile_bfd, s, buffer, 0, size);

	      sent = 0;
	      do
		{
		  CORE_ADDR len;
		  CORE_ADDR this_transfer = size - sent;
		  if (this_transfer >= block_size)
		    this_transfer = block_size;
		  len = target_write_memory_partial (lma, buffer,
						     (int) this_transfer, &err);
		  if (err)
		    break;
		  if (validate_download)
		    {
		      /* Broken memories and broken monitors manifest
			 themselves here when bring new computers to
			 life.  This doubles already slow downloads.  */
		      /* NOTE: cagney/1999-10-18: A more efficient
                         implementation might add a verify_memory()
                         method to the target vector and then use
                         that.  remote.c could implement that method
                         using the ``qCRC'' packet.  */
		      char *check = xmalloc (len);
		      struct cleanup *verify_cleanups = make_cleanup (free, check);
		      if (target_read_memory (lma, check, (int)len) != 0)
			error ("Download verify read failed at 0x%s",
			       paddr (lma));
		      if (memcmp (buffer, check, len) != 0)
			error ("Download verify compare failed at 0x%s",
			       paddr (lma));
		      do_cleanups (verify_cleanups);
 		    }
		  data_count += len;
		  lma += len;
		  buffer += len;
		  write_count += 1;
		  sent += len;
		  total_sent += len;
		  if (quit_flag
		      || (ui_load_progress_hook != NULL
			  && ui_load_progress_hook (sect_name, sent)))
		    error ("Canceled the download");

		  if (show_load_progress != NULL)
		    show_load_progress (sect_name, sent, size, total_sent, total_size);
		}
	      while (sent < size);

	      if (err != 0)
		error ("Memory access error while loading section %s.", sect_name);

	      do_cleanups (old_chain);
	    }
	}
    }

  end_time = time (NULL);
  {
    CORE_ADDR entry;
    entry = bfd_get_start_address (loadfile_bfd);
#ifdef UI_OUT
   ui_out_text (uiout, "Start address ");
   ui_out_field_fmt (uiout, "address", "0x%s" , paddr_nz (entry));
   ui_out_text (uiout, ", load size ");
   ui_out_field_fmt (uiout, "load-size", "%ld" , data_count);
   ui_out_text (uiout, "\n");

#else
    fprintf_unfiltered (gdb_stdout,
			"Start address 0x%s , load size %ld\n",
			paddr_nz (entry), data_count);
#endif
    /* We were doing this in remote-mips.c, I suspect it is right
       for other targets too.  */
    write_pc (entry);
  }

  /* FIXME: are we supposed to call symbol_file_add or not?  According to
     a comment from remote-mips.c (where a call to symbol_file_add was
     commented out), making the call confuses GDB if more than one file is
     loaded in.  remote-nindy.c had no call to symbol_file_add, but remote-vx.c
     does.  */

  print_transfer_performance (gdb_stdout, data_count, write_count,
			      end_time - start_time);

  do_cleanups (old_cleanups);
}

/* Report how fast the transfer went. */

/* DEPRECATED: cagney/1999-10-18: report_transfer_performance is being
   replaced by print_transfer_performance (with a very different
   function signature). */

void
report_transfer_performance (unsigned long data_count, time_t start_time, time_t end_time)
{
  print_transfer_performance (gdb_stdout, data_count, end_time - start_time, 0);
}

void
print_transfer_performance (struct ui_file *stream,
			    unsigned long data_count,
			    unsigned long write_count,
			    unsigned long time_count)
{
#ifdef UI_OUT
  ui_out_text (uiout, "Transfer rate: ");
  if (time_count > 0)
    {
      ui_out_field_fmt (uiout, "transfer-rate", "%ld", 
			(data_count * 8) / time_count);
      ui_out_text (uiout, " bits/sec");
    }
  else
    {
      ui_out_field_fmt (uiout, "transferred-bits", "%ld", (data_count * 8));
      ui_out_text (uiout, " bits in <1 sec");    
    }
  if (write_count > 0)
    {
      ui_out_text (uiout, ", ");
      ui_out_field_fmt (uiout, "write-rate", "%ld", data_count / write_count);
      ui_out_text (uiout, " bytes/write");
    }
  ui_out_text (uiout, ".\n");
#else
  fprintf_unfiltered (stream, "Transfer rate: ");
  if (time_count > 0)
    fprintf_unfiltered (stream, "%ld bits/sec", (data_count * 8) / time_count);
  else
    fprintf_unfiltered (stream, "%ld bits in <1 sec", (data_count * 8));
  if (write_count > 0)
    fprintf_unfiltered (stream, ", %ld bytes/write", data_count / write_count);
  fprintf_unfiltered (stream, ".\n");
#endif
}

/* This function allows the addition of incrementally linked object files.
   It does not modify any state in the target, only in the debugger.  */
/* Note: ezannoni 2000-04-13 This function/command used to have a
   special case syntax for the rombug target (Rombug is the boot
   monitor for Microware's OS-9 / OS-9000, see remote-os9k.c). In the
   rombug case, the user doesn't need to supply a text address,
   instead a call to target_link() (in target.c) would supply the
   value to use. We are now discontinuing this type of ad hoc syntax. */

/* ARGSUSED */
#if 0
#ifndef HP_IA64 
static
#endif
#endif /* if 0 */
void
add_symbol_file_command (char *args, int from_tty)
{
  char *filename = NULL;
  int flags = OBJF_USERLOADED;
  char *arg;
  int expecting_option = 0;
  int section_index = 0;
  int argcnt = 0;
  int sec_num = 0;
  int i;
  int expecting_sec_name = 0;
  int expecting_sec_addr = 0;
  CORE_ADDR addr = 0;
 
  /* JAGaf52783 - add-symbol-file command fails for gdb64*/
  #if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
  bfd *tmp_bfd;
  asection *sect;
  #endif 

  /* JAGag07432 */

  #ifdef GDB_TARGET_IS_HPPA
  #ifndef GDB_TARGET_IS_HPPA_20W
    add_symbol_file_cmd = TRUE;
  #endif
  #endif

  struct
  {
    char *name;
    char *value;
  } sect_opts[SECT_OFF_MAX];

  struct section_addr_info section_addrs;
  struct cleanup *my_cleanups = 0;

  dont_repeat ();

  if (args == NULL)
    error ("add-symbol-file takes a file name and an address");

  /* Make a copy of the string that we can safely write into. */
  args = xstrdup (args);

  /* Ensure section_addrs is initialized */
  memset (&section_addrs, 0, sizeof (section_addrs));

  while (*args != '\000')
    {
      /* Any leading spaces? */
      while (isspace (*args))
	args++;

      /* Point arg to the beginning of the argument. */
      arg = args;

      /* Move args pointer over the argument. */
      while ((*args != '\000') && !isspace (*args))
	args++;

      /* If there are more arguments, terminate arg and
         proceed past it. */
      if (*args != '\000')
	*args++ = '\000';

      /* Now process the argument. */
      if (argcnt == 0)
	{
	  /* The first argument is the file name. */
	  filename = tilde_expand (arg);
	  my_cleanups = make_cleanup (free, filename);
	}
      else
	if (argcnt == 1)
	  {
	    /* The second argument is always the text address at which
               to load the program. */
	    /* JAGaf52783 - add-symbol-file command fails for gdb64. Name
	       of text section in PA32 is $TEXT$ and for IA and PA64 it is 
	       .text */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
	    sect_opts[section_index].name = ".text";
#else
	    sect_opts[section_index].name = "$TEXT$";
#endif
	    sect_opts[section_index].value = arg;
	    section_index++;		  
	  }
	else
	  {
	    /* It's an option (starting with '-') or it's an argument
	       to an option */

	    if (*arg == '-')
	      {
		if (strcmp (arg, "-mapped") == 0)
		  flags |= OBJF_MAPPED;
		else 
		  if (strcmp (arg, "-readnow") == 0)
		    flags |= OBJF_READNOW;
		  else 
		    if (strcmp (arg, "-s") == 0)
		      {
			if (section_index >= SECT_OFF_MAX)
			  error ("Too many sections specified.");
			expecting_sec_name = 1;
			expecting_sec_addr = 1;
		      }
	      }
	    else
	      {
		if (expecting_sec_name)
		  {
		    sect_opts[section_index].name = arg;
		    expecting_sec_name = 0;
		  }
		else
		  if (expecting_sec_addr)
		    {
		      sect_opts[section_index].value = arg;
		      expecting_sec_addr = 0;
		      section_index++;		  
		    }
		  else
		    error ("USAGE: add-symbol-file <filename> <textaddress> [-mapped] [-readnow] [-s <secname> <addr>]*");
	      }
	  }
      argcnt++;
    }

  /* Print the prompt for the query below. And save the arguments into
     a sect_addr_info structure to be passed around to other
     functions.  We have to split this up into separate print
     statements because local_hex_string returns a local static
     string. */
#ifdef HP_IA64
  /* QXCR1000579474: Aries core file debugging.
     Do not query if this is an implicit call due to an aries-core command
     or due to handling dlclosed libraries in gdbrtc.c  */
  if (!add_aries_symbol_file && !add_unloaded_library_symbols)
#else
  if (!add_unloaded_library_symbols)
#endif
    {
      printf_filtered ("add symbol table from file \"%s\" at\n", filename);
    }

  for (i = 0; i < section_index; i++)
    {
      CORE_ADDR addr;
      char *val = sect_opts[i].value;
      char *sec = sect_opts[i].name;
 
      val = sect_opts[i].value;
      /* JAGaf52783 - add-symbol-file command fails for gdb64. strtoul
         cannot be used to return unsigned long long which is required 
	 for PA64 and IA so sscanf can be used instead of strtoul 
	 function */
      if (val[0] == '0' && val[1] == 'x')
#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
	sscanf(val+2, "%llx", &addr);
#else 
	sscanf(val+2, "%lx", &addr);
#endif
      else
	sscanf(val,"%lld", &addr);

      /* Here we store the section offsets in the order they were
         entered on the command line. */
      section_addrs.other[sec_num].name = sec;
      section_addrs.other[sec_num].addr = addr;
#ifdef HP_IA64
      if (!add_aries_symbol_file && !add_unloaded_library_symbols)
#else
      if (!add_unloaded_library_symbols)
#endif
        {
          printf_filtered ("\t%s_addr = %s\n",
	                   sec, 
		           longest_local_hex_string ((LONGEST) addr));
        }
      sec_num++;

      /* The object's sections are initialized when a 
	 call is made to build_objfile_section_table (objfile).
	 This happens in reread_symbols. 
	 At this point, we don't know what file type this is,
	 so we can't determine what section names are valid.  */
    }

  if (
#ifdef HP_IA64
      !add_aries_symbol_file  && !add_unloaded_library_symbols && 
       from_tty && (!query ("%s", "")))
#else
      !add_unloaded_library_symbols && from_tty && (!query ("%s", "")))
#endif
    error ("Not confirmed.");

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
	  /* JAGaf52783 - add-symbol-file command fails for gdb64. */
             
	  /* We need the BFD so that we can look at its sections.  We open up the
     	     file temporarily, then close it when we are done.  */
  	  tmp_bfd = symfile_bfd_open (filename);
          if (tmp_bfd == NULL)
    	  {
      		perror_with_name (filename);
      		return;
    	  }

 	  if (!bfd_check_format (tmp_bfd, bfd_object))
    	  {
      		bfd_close (tmp_bfd);
     		error ("\"%s\" is not an object file: %s", filename,
                bfd_errmsg (bfd_get_error ()));
    	  }
	  
	  /* JAGaf08152 15/02/05 Sunil S 
   	     add-symbol-file command fails for gdb32. Name of text section in PA32 
	     is $TEXT$ and for IA and PA64 it is .text */

#if defined(GDB_TARGET_IS_HPPA_20W) || defined(HP_IA64)
	  sect = bfd_get_section_by_name (tmp_bfd, ".text");
#else
	  sect = bfd_get_section_by_name (tmp_bfd, "$TEXT$");
#endif
          if (sect)
    	    section_addrs.other[0].addr += bfd_section_vma (tmp_bfd, sect);
          addr = elf_text_start_vma (tmp_bfd);

          /* jini: JAGag26122: Fix suggested by SAP. We have to check for
             '-1' as return value which is used by elf_text_start_vma()
             to indicate an invalid return value!!! */
          if (addr != (CORE_ADDR)(long) -1)
            section_addrs.other[0].addr -= addr;
	  bfd_close (tmp_bfd);
#endif

  symbol_file_add (filename, from_tty, &section_addrs, 0, flags);

  /* Getting new symbols may change our opinion about what is
     frameless.  */
  reinit_frame_cache ();
  if (my_cleanups)
    do_cleanups (my_cleanups);
}

static void
add_shared_symbol_files_command (char *args, int from_tty)
{
#ifdef ADD_SHARED_SYMBOL_FILES
  ADD_SHARED_SYMBOL_FILES (args, from_tty);
#else
  error ("This command is not available in this configuration of GDB.");
#endif
}

/* Re-read symbols if a symbol-file has changed.  */
void
reread_symbols ()
{
  struct objfile *objfile;
  long new_modtime;
  int reread_one = 0;
  struct stat new_statbuf;
  int res;

  /* With the addition of shared libraries, this should be modified,
     the load time should be saved in the partial symbol tables, since
     different tables may come from different source files.  FIXME.
     This routine should then walk down each partial symbol table
     and see if the symbol table that it originates from has been changed */

  for (objfile = object_files; objfile; objfile = objfile->next)
    {
      if (objfile->obfd)
	{
#ifdef IBM6000_TARGET
	  /* If this object is from a shared library, then you should
	     stat on the library name, not member name. */

	  if (objfile->obfd->my_archive)
	    res = stat (objfile->obfd->my_archive->filename, &new_statbuf);
	  else
#endif
	    res = stat (objfile->name, &new_statbuf);
	  if (res != 0)
	    {
	      /* FIXME, should use print_sys_errmsg but it's not filtered. */
	      printf_filtered ("`%s' has disappeared; keeping its symbols.\n",
			       objfile->name);
	      continue;
	    }
	  new_modtime = new_statbuf.st_mtime;

          /* srikanth, always reread symbols. There are places where
             we matrialize a pointer to the shared library symbol
             table and store it in a.out's symbol table. When the
             user issues a rerun command, we blow away all the
             shared library details, leaving us with a bad pointer.

             objfiles.c forces us to reread symbols by setting
             mtime to 0 on reruns.
          */   
	  if (new_modtime != objfile->mtime)
	    {
	      struct cleanup *old_cleanups;
	      struct section_offsets *offsets;
	      int num_offsets;
	      char *obfd_filename;

	      /* There are various functions like symbol_file_add,
	         symfile_bfd_open, syms_from_objfile, etc., which might
	         appear to do what we want.  But they have various other
	         effects which we *don't* want.  So we just do stuff
	         ourselves.  We don't worry about mapped files (for one thing,
	         any mapped file will be out of date).  */

	      /* If we get an error, blow away this objfile (not sure if
	         that is the correct response for things like shared
	         libraries).  */
	      old_cleanups = make_cleanup_free_objfile (objfile);
	      /* We need to do this whenever any symbols go away.  */
	      make_cleanup (clear_symtab_users_cleanup, 0 /*ignore*/);

	      /* Clean up any state BFD has sitting around.  We don't need
	         to close the descriptor but BFD lacks a way of closing the
	         BFD without closing the descriptor.  */
	      obfd_filename = bfd_get_filename (objfile->obfd);
	      if (!bfd_close (objfile->obfd))
		warning ("Can't close BFD for %s: %s", objfile->name,
		       bfd_errmsg (bfd_get_error ()));
	      objfile->obfd = bfd_openr (obfd_filename, gnutarget);
	      if (objfile->obfd == NULL)
		error ("Can't open %s to read symbols.", objfile->name);
	      /* bfd_openr sets cacheable to true, which is what we want.  */
	      if (!bfd_check_format (objfile->obfd, bfd_object))
		error ("Can't read symbols from %s: %s.", objfile->name,
		       bfd_errmsg (bfd_get_error ()));

	      /* Save the offsets, we will nuke them with the rest of the
	         psymbol_obstack.  */
	      num_offsets = objfile->num_sections;
	      offsets = (struct section_offsets *) alloca (SIZEOF_SECTION_OFFSETS);
	      memcpy (offsets, objfile->section_offsets, SIZEOF_SECTION_OFFSETS);

	      /* Nuke all the state that we will re-read.  Much of the following
	         code which sets things to NULL really is necessary to tell
	         other parts of GDB that there is nothing currently there.  */

	      /* FIXME: Do we have to free a whole linked list, or is this
	         enough?  */
	      if (objfile->global_psymbols.list)
		mfree (objfile->md, objfile->global_psymbols.list);
	      memset (&objfile->global_psymbols, 0,
		      sizeof (objfile->global_psymbols));
	      if (objfile->static_psymbols.list)
		mfree (objfile->md, objfile->static_psymbols.list);
	      memset (&objfile->static_psymbols, 0,
		      sizeof (objfile->static_psymbols));

	      /* Free the obstacks for non-reusable objfiles */
	      obstack_free (&objfile->psymbol_cache.cache, 0);
	      memset (&objfile->psymbol_cache, 0,
		      sizeof (objfile->psymbol_cache));
	      obstack_free (&objfile->psymbol_obstack, 0);
	      obstack_free (&objfile->symbol_obstack, 0);
	      obstack_free (&objfile->type_obstack, 0);
	      objfile->sections = NULL;
	      objfile->symtabs = NULL;
	      objfile->psymtabs = NULL;
	      objfile->free_psymtabs = NULL;
	      objfile->msymbols = NULL;
	      objfile->minimal_symbol_count = 0;

              /* srikanth, 021402, also clearout the range of addresses.
                 Otherwise, install_minimal_symbol would get confused
              */
              objfile->lowest_address = 0;
              objfile->highest_address = 0;
#if !defined(LAZY_DEMANGLING) && !defined(HASH_ALL)
    /* JYG: MERGE NOTE: we use sorted mangled name list and prefix
       mangling lookup + lazy demangling, which cannot use hash
       implementation and does not want demangling the whole world
       behavior */
	      memset (&objfile->msymbol_hash, 0,
		      sizeof (objfile->msymbol_hash));
	      memset (&objfile->msymbol_demangled_hash, 0,
		      sizeof (objfile->msymbol_demangled_hash));
#endif

	      objfile->text_low = 0;
              objfile->text_high = 0;
	      objfile->data_start = 0;
	      objfile->data_size = 0;
	      objfile->fundamental_types = NULL;
  	      objfile->obj_private = NULL;
	      if (objfile->sf != NULL)
		{
		  (*objfile->sf->sym_finish) (objfile);
		}

	      /* We never make this a mapped file.  */
	      objfile->md = NULL;
	      /* obstack_specify_allocation also initializes the obstack so
	         it is empty.  */
	      obstack_specify_allocation (&objfile->psymbol_cache.cache, 0, 0,
					  xmalloc, free);
	      obstack_specify_allocation (&objfile->psymbol_obstack, 0, 0,
					  xmalloc, free);
	      obstack_specify_allocation (&objfile->symbol_obstack, 0, 0,
					  xmalloc, free);
	      obstack_specify_allocation (&objfile->type_obstack, 0, 0,
					  xmalloc, free);
	      if (build_objfile_section_table (objfile))
		{
		  error ("Can't find the file sections in `%s': %s",
			 objfile->name, bfd_errmsg (bfd_get_error ()));
		}

	      /* We use the same section offsets as from last time.  I'm not
	         sure whether that is always correct for shared libraries.  */
	      objfile->section_offsets = (struct section_offsets *)(void *)
		obstack_alloc (&objfile->psymbol_obstack, SIZEOF_SECTION_OFFSETS);
	      memcpy (objfile->section_offsets, offsets, SIZEOF_SECTION_OFFSETS);
	      objfile->num_sections = num_offsets;

	      /* RM: HP-specific hack. Initialize the flag that indicates
		 whether this is a doom file or not */
#ifdef GDB_TARGET_IS_HPUX
	      hpread_init_debug_in_object_files(objfile->obfd);
#endif
          
	      /* What the hell is sym_new_init for, anyway?  The concept of
	         distinguishing between the main file and additional files
	         in this way seems rather dubious.  */
	      if (objfile == symfile_objfile)
		{
#ifdef INLINE_SUPPORT
                  clear_inline_data();
#endif
		  (*objfile->sf->sym_new_init) (objfile);
#ifdef GDB_TARGET_IS_HPUX
		  RESET_HP_UX_GLOBALS ();
#endif
		}

	      (*objfile->sf->sym_init) (objfile, 0);
	      clear_complaints (1, 1);
	      /* The "mainline" parameter is a hideous hack; I think leaving it
	         zero is OK since dbxread.c also does what it needs to do if
	         objfile->global_psymbols.size is 0.  */
	      (*objfile->sf->sym_read) (objfile, 0, 0);
              if (!profile_on)  /* Just shut up if profiling under prospect */
                {
		  if (!have_partial_symbols () && !have_full_symbols ())
		    {
		      wrap_here ("");
		      printf_filtered ("(no debugging symbols found)\n");
		      wrap_here ("");
		    }
                }
	      objfile->flags |= OBJF_SYMS;

	      /* We're done reading the symbol file; finish off complaints.  */
	      clear_complaints (0, 1);

	      /* Getting new symbols may change our opinion about what is
	         frameless.  */

	      reinit_frame_cache ();

	      /* Discard cleanups as symbol reading was successful.  */
	      discard_cleanups (old_cleanups);

	      /* If the mtime has changed between the time we set new_modtime
	         and now, we *want* this to be out of date, so don't call stat
	         again now.  */
	      objfile->mtime = new_modtime;
	      reread_one = 1;

	      /* Call this after reading in a new symbol table to give target
	         dependant code a crack at the new symbols.  For instance, this
	         could be used to update the values of target-specific symbols GDB
	         needs to keep track of (such as _sigtramp, or whatever).  */

	      TARGET_SYMFILE_POSTREAD (objfile);
	    }
	}
    }

  if (reread_one)
    {
      /* Addresses may have changed ... */
      reset_all_breakpoints = 1;
      clear_symtab_users ();
      reset_all_breakpoints = 0;
    }
}



typedef struct
{
  char *ext;
  enum language lang;
}
filename_language;

static filename_language *filename_language_table;
static int fl_table_size, fl_table_next;

static void
add_filename_language (char *ext, enum language lang)
{
  if (fl_table_next >= fl_table_size)
    {
      fl_table_size += 10;
      filename_language_table = realloc (filename_language_table,
					 fl_table_size);
    }

  filename_language_table[fl_table_next].ext = strsave (ext);
  filename_language_table[fl_table_next].lang = lang;
  fl_table_next++;
}

static char *ext_args;

static void
set_ext_lang_command (char *args, int from_tty)
{
  int i;
  char *cp = ext_args;
  enum language lang;

  /* First arg is filename extension, starting with '.' */
  if (*cp != '.')
    error ("'%s': Filename extension must begin with '.'", ext_args);

  /* Find end of first arg.  */
  while (*cp && !isspace (*cp))
    cp++;

  if (*cp == '\0')
    error ("'%s': two arguments required -- filename extension and language",
	   ext_args);

  /* Null-terminate first arg */
  *cp++ = '\0';

  /* Find beginning of second arg, which should be a source language.  */
  while (*cp && isspace (*cp))
    cp++;

  if (*cp == '\0')
    error ("'%s': two arguments required -- filename extension and language",
	   ext_args);

  /* Lookup the language from among those we know.  */
  lang = language_enum (cp);

  /* Now lookup the filename extension: do we already know it?  */
  for (i = 0; i < fl_table_next; i++)
    if (0 == strcmp (ext_args, filename_language_table[i].ext))
      break;

  if (i >= fl_table_next)
    {
      /* new file extension */
      add_filename_language (ext_args, lang);
    }
  else
    {
      /* redefining a previously known filename extension */

      /* if (from_tty) */
      /*   query ("Really make files of type %s '%s'?", */
      /*          ext_args, language_str (lang));           */

      free (filename_language_table[i].ext);
      filename_language_table[i].ext = strsave (ext_args);
      filename_language_table[i].lang = lang;
    }
}

static void
info_ext_lang_command (char *args, int from_tty)
{
  int i;

  printf_filtered ("Filename extensions and the languages they represent:");
  printf_filtered ("\n\n");
  for (i = 0; i < fl_table_next; i++)
    printf_filtered ("\t%s\t- %s\n",
		     filename_language_table[i].ext,
		     language_str (filename_language_table[i].lang));
}

static void
init_filename_language_table ()
{
  if (fl_table_size == 0)	/* protect against repetition */
    {
      fl_table_size = 20;
      fl_table_next = 0;
      filename_language_table =
	xmalloc (fl_table_size * sizeof (*filename_language_table));
      add_filename_language (".c", language_c);
      add_filename_language (".C", language_cplus);
      add_filename_language (".cc", language_cplus);
      add_filename_language (".cp", language_cplus);
      add_filename_language (".cpp", language_cplus);
      add_filename_language (".cxx", language_cplus);
      add_filename_language (".c++", language_cplus);
      add_filename_language (".java", language_java);
      add_filename_language (".class", language_java);
      add_filename_language (".ch", language_chill);
      add_filename_language (".c186", language_chill);
      add_filename_language (".c286", language_chill);
      add_filename_language (".f", language_fortran);
      add_filename_language (".F", language_fortran);
      add_filename_language (".f90", language_fortran);
      add_filename_language (".s", language_asm);
      add_filename_language (".S", language_asm);
      add_filename_language (".pas", language_pascal);
      add_filename_language (".p", language_pascal);
      add_filename_language (".pp", language_pascal);
    }
}

enum language
deduce_language_from_filename (char *filename)
{
  int i;
  char *cp;

  if (filename != NULL)
    if ((cp = strrchr (filename, '.')) != NULL)
      for (i = 0; i < fl_table_next; i++)
	if (strcmp (cp, filename_language_table[i].ext) == 0)
	  return filename_language_table[i].lang;

  return language_unknown;
}

/* allocate_symtab:

   Allocate and partly initialize a new symbol table.  Return a pointer
   to it.  error() if no space.

   Caller must set these fields:
   LINETABLE(symtab)
   symtab->blockvector
   symtab->dirname
   symtab->free_code
   symtab->free_ptr
   possibly free_named_symtabs (symtab->filename);
 */

struct symtab *
allocate_symtab (char *filename, struct objfile *objfile)
{
  register struct symtab *symtab;

  symtab = (struct symtab *)(void *)
    obstack_alloc (&objfile->symbol_obstack, sizeof (struct symtab));
  memset (symtab, 0, sizeof (*symtab));

  /* srikanth, 990310, JAGaa80452 : filenames don't materialize
     out of nowhere. Most are pointers into the literal value table. 
     See if we could piggyback on an already existing version ... */
  if (permanent_copy_exists(objfile, filename))
    symtab -> filename = filename;
  else 
    symtab->filename = obsavestring (filename, (int) strlen (filename),
				     &objfile->symbol_obstack);
  symtab->fullname = NULL;
  symtab->language = deduce_language_from_filename (filename);
  symtab->debugformat = "unknown";

  /* Hook it to the objfile it comes from */

  symtab->objfile = objfile;
  symtab->next = objfile->symtabs;
  objfile->symtabs = symtab;

  /* FIXME: This should go away.  It is only defined for the Z8000,
     and the Z8000 definition of this macro doesn't have anything to
     do with the now-nonexistent EXTRA_SYMTAB_INFO macro, it's just
     here for convenience.  */
#ifdef INIT_EXTRA_SYMTAB_INFO
  INIT_EXTRA_SYMTAB_INFO (symtab);
#endif

  return (symtab);
}

struct partial_symtab *
allocate_psymtab (char *filename, char *dirname, struct objfile *objfile)
{
  struct partial_symtab *psymtab;

  if (objfile->free_psymtabs)
    {
      psymtab = objfile->free_psymtabs;
      objfile->free_psymtabs = psymtab->next;
    }
  else
    psymtab = (struct partial_symtab *)(void *)
      obstack_alloc (&objfile->psymbol_obstack,
		     sizeof (struct partial_symtab));

  memset (psymtab, 0, sizeof (struct partial_symtab));

  /* Added check to check if filename is present. We should be returning
   a null psymtab if filename is null. */

  if (filename)
    if (permanent_copy_exists (objfile, filename))
      psymtab->filename = filename;
    else
      psymtab->filename = obsavestring (filename, (int) strlen (filename),
				      &objfile->psymbol_obstack);
  if (dirname)
    if (permanent_copy_exists (objfile, dirname))
      psymtab->dirname = dirname;
    else
      psymtab->dirname = obsavestring (dirname, (int) strlen (dirname),
				      &objfile->psymbol_obstack);
  psymtab->symtab = NULL;

  /* Prepend it to the psymtab list for the objfile it belongs to.
     Psymtabs are searched in most recent inserted -> least recent
     inserted order. */

  psymtab->objfile = objfile;
  psymtab->next = objfile->psymtabs;
  objfile->psymtabs = psymtab;

  /* RM: Guess the language to set a good default value */
  if (filename != NULL)
    {
      psymtab->language = deduce_language_from_filename (filename);
    }
  else
    {
      psymtab->language = language_unknown;
    }
      
  return (psymtab);
}

void
discard_psymtab (struct partial_symtab *pst)
{
  struct partial_symtab **prev_pst;

  /* From dbxread.c:
     Empty psymtabs happen as a result of header files which don't
     have any symbols in them.  There can be a lot of them.  But this
     check is wrong, in that a psymtab with N_SLINE entries but
     nothing else is not empty, but we don't realize that.  Fixing
     that without slowing things down might be tricky.  */

  /* First, snip it out of the psymtab chain */

  prev_pst = &(pst->objfile->psymtabs);
  while ((*prev_pst) != pst)
    prev_pst = &((*prev_pst)->next);
  (*prev_pst) = pst->next;

  /* Next, put it on a free list for recycling */

  pst->next = pst->objfile->free_psymtabs;
  pst->objfile->free_psymtabs = pst;
}



/* This routine is called for program which use shared libraries at the
   start of each run to forget stuff because the shared libraries might
   have changed.

   Do most of what clear_symtab_users does except don't call
   "target_new_objfile (NULL)" because this will make the CMA implementation
   forget about the symbols of the main program which are not presented
   again. */

void
solib_clear_symtab_users ()
{
  /* Someday, we should do better than this, by only blowing away
     the things that really need to be blown.  */
  /* CAUTION: reset 'current_source_symtab' and 'current_source_line' 
     first lest they be used by any of the functions called below. */
  current_source_symtab = 0;
  current_source_line = 0;
  clear_value_history ();
  /* JAGae33079: Do not clear display_chain, make displays persistent by refreshing
     display_chain */
  /*clear_displays (); */
  clear_internalvars ();
  breakpoint_re_set ();
  set_default_breakpoint (0, 0, 0, 0);

  /* srikanth, 000131, get rid of the dirty cache. */

#ifdef FLUSH_PROLOGUE_CACHE
  FLUSH_PROLOGUE_CACHE ();
#endif 

#ifdef FLUSH_EPILOGUE_CACHE
  FLUSH_EPILOGUE_CACHE ();
#endif 

  clear_minimal_symbol_caches ();
  clear_pc_function_cache ();
}


/* clear_symtab_users is called when a program is loaded to forget anything
   about the previous program.

   Reset all data structures in gdb which may contain references to symbol
   table data.  */

/* See the comments in solib_clear_symtab_users() about how we should
   do something better. */

void
clear_symtab_users ()
{

  extern int firebolt_version;

  /* Get rid of any temporary files generated for firebolt disassembly.
     As the symbol files have changed, these are invalid.
  */
  if (firebolt_version)
    {
      char nuke_asm_files[128];
      sprintf (nuke_asm_files, "/bin/rm -f /tmp/.firebolt.%d* >/dev/null 2>/dev/null", getppid());
      system (nuke_asm_files);
    }

  solib_clear_symtab_users();

  if (target_new_objfile_hook)
    target_new_objfile_hook (NULL);
}

static void
clear_symtab_users_cleanup (void *ignore)
{
  clear_symtab_users ();
}

/* clear_symtab_users_once:

   This function is run after symbol reading, or from a cleanup.
   If an old symbol table was obsoleted, the old symbol table
   has been blown away, but the other GDB data structures that may 
   reference it have not yet been cleared or re-directed.  (The old
   symtab was zapped, and the cleanup queued, in free_named_symtab()
   below.)

   This function can be queued N times as a cleanup, or called
   directly; it will do all the work the first time, and then will be a
   no-op until the next time it is queued.  This works by bumping a
   counter at queueing time.  Much later when the cleanup is run, or at
   the end of symbol processing (in case the cleanup is discarded), if
   the queued count is greater than the "done-count", we do the work
   and set the done-count to the queued count.  If the queued count is
   less than or equal to the done-count, we just ignore the call.  This
   is needed because reading a single .o file will often replace many
   symtabs (one per .h file, for example), and we don't want to reset
   the breakpoints N times in the user's face.

   The reason we both queue a cleanup, and call it directly after symbol
   reading, is because the cleanup protects us in case of errors, but is
   discarded if symbol reading is successful.  */

#if 0
/* FIXME:  As free_named_symtabs is currently a big noop this function
   is no longer needed.  */
static void clear_symtab_users_once (void);

static int clear_symtab_users_queued;
static int clear_symtab_users_done;

static void
clear_symtab_users_once ()
{
  /* Enforce once-per-`do_cleanups'-semantics */
  if (clear_symtab_users_queued <= clear_symtab_users_done)
    return;
  clear_symtab_users_done = clear_symtab_users_queued;

  clear_symtab_users ();
}
#endif

/* Delete the specified psymtab, and any others that reference it.  */

static void
cashier_psymtab (struct partial_symtab *pst)
{
  struct partial_symtab *ps, *pprev = NULL;
  int i;

  /* Find its previous psymtab in the chain */
  for (ps = pst->objfile->psymtabs; ps; ps = ps->next)
    {
      if (ps == pst)
	break;
      pprev = ps;
    }

  if (ps)
    {
      /* Unhook it from the chain.  */
      if (ps == pst->objfile->psymtabs)
	pst->objfile->psymtabs = ps->next;
      else
	pprev->next = ps->next;

      /* FIXME, we can't conveniently deallocate the entries in the
         partial_symbol lists (global_psymbols/static_psymbols) that
         this psymtab points to.  These just take up space until all
         the psymtabs are reclaimed.  Ditto the dependencies list and
         filename, which are all in the psymbol_obstack.  */

      /* We need to cashier any psymtab that has this one as a dependency... */
    again:
      for (ps = pst->objfile->psymtabs; ps; ps = ps->next)
	{
	  for (i = 0; i < ps->number_of_dependencies; i++)
	    {
	      if (ps->dependencies[i] == pst)
		{
		  cashier_psymtab (ps);
		  goto again;	/* Must restart, chain has been munged. */
		}
	    }
	}
    }
}

/* If a symtab or psymtab for filename NAME is found, free it along
   with any dependent breakpoints, displays, etc.
   Used when loading new versions of object modules with the "add-file"
   command.  This is only called on the top-level symtab or psymtab's name;
   it is not called for subsidiary files such as .h files.

   Return value is 1 if we blew away the environment, 0 if not.
   FIXME.  The return valu appears to never be used.

   FIXME.  I think this is not the best way to do this.  We should
   work on being gentler to the environment while still cleaning up
   all stray pointers into the freed symtab.  */

int
free_named_symtabs (char *name)
{
#if 0
  /* FIXME:  With the new method of each objfile having it's own
     psymtab list, this function needs serious rethinking.  In particular,
     why was it ever necessary to toss psymtabs with specific compilation
     unit filenames, as opposed to all psymtabs from a particular symbol
     file?  -- fnf
     Well, the answer is that some systems permit reloading of particular
     compilation units.  We want to blow away any old info about these
     compilation units, regardless of which objfiles they arrived in. --gnu.  */

  register struct symtab *s;
  register struct symtab *prev;
  register struct partial_symtab *ps;
  struct blockvector *bv;
  int blewit = 0;

  /* We only wack things if the symbol-reload switch is set.  */
  if (!symbol_reloading)
    return 0;

  /* Some symbol formats have trouble providing file names... */
  if (name == 0 || *name == '\0')
    return 0;

  /* Look for a psymtab with the specified name.  */

again2:
  for (ps = partial_symtab_list; ps; ps = ps->next)
    {
      if (STREQ (name, ps->filename))
	{
	  cashier_psymtab (ps);	/* Blow it away...and its little dog, too.  */
	  goto again2;		/* Must restart, chain has been munged */
	}
    }

  /* Look for a symtab with the specified name.  */

  for (s = symtab_list; s; s = s->next)
    {
      if (STREQ (name, s->filename))
	break;
      prev = s;
    }

  if (s)
    {
      if (s == symtab_list)
	symtab_list = s->next;
      else
	prev->next = s->next;

      /* For now, queue a delete for all breakpoints, displays, etc., whether
         or not they depend on the symtab being freed.  This should be
         changed so that only those data structures affected are deleted.  */

      /* But don't delete anything if the symtab is empty.
         This test is necessary due to a bug in "dbxread.c" that
         causes empty symtabs to be created for N_SO symbols that
         contain the pathname of the object file.  (This problem
         has been fixed in GDB 3.9x).  */

      bv = BLOCKVECTOR (s);
      if (BLOCKVECTOR_NBLOCKS (bv) > 2
	  || BLOCK_NSYMS (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK))
	  || BLOCK_NSYMS (BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK)))
	{
	  complain (&oldsyms_complaint, name);

	  clear_symtab_users_queued++;
	  make_cleanup (clear_symtab_users_once, 0);
	  blewit = 1;
	}
      else
	{
	  complain (&empty_symtab_complaint, name);
	}

      free_symtab (s);
    }
  else
    {
      /* It is still possible that some breakpoints will be affected
         even though no symtab was found, since the file might have
         been compiled without debugging, and hence not be associated
         with a symtab.  In order to handle this correctly, we would need
         to keep a list of text address ranges for undebuggable files.
         For now, we do nothing, since this is a fairly obscure case.  */
      ;
    }

  /* FIXME, what about the minimal symbol table? */
  return blewit;
#else
  return (0);
#endif
}

/* Allocate and partially fill a partial symtab.  It will be
   completely filled at the end of the symbol list.

   FILENAME is the name of the symbol-file we are reading from. */

struct partial_symtab *
start_psymtab_common (struct objfile *objfile, struct section_offsets *section_offsets,
                      char *filename, char *dirname, CORE_ADDR textlow,
     		      struct partial_symbol **global_syms, struct partial_symbol **static_syms)
{
  struct partial_symtab *psymtab;

  psymtab = allocate_psymtab (filename, dirname, objfile);
  psymtab->section_offsets = section_offsets;
  psymtab->textlow = textlow;
  psymtab->texthigh = psymtab->textlow;		/* default */
  psymtab->globals_offset = (int)(global_syms - objfile->global_psymbols.list);
  psymtab->statics_offset = (int)(static_syms - objfile->static_psymbols.list);
  return (psymtab);
}



/* Add a symbol with a long value to a psymtab.
   Since one arg is a struct, we pass in a ptr and deref it (sigh).

   srikanth, 991111:
   Also record the handle to native debug info so that this psymbol could
   be expanded without also expanding the whole universe ... */

/*long val- Value as a long */
/*CORE_ADDR coreadd - Value as a CORE_ADDR */
void
add_psymbol_to_list ( char *name, int namelength, namespace_enum namespace,
                     enum address_class class, struct psymbol_allocation_list *list,
                     long val, CORE_ADDR coreaddr, enum language language,
                     struct objfile *objfile, unsigned native_dbg_index)
{
  /* Calls add_psymbol_to_list_1 with is_global set to true */
  add_psymbol_to_list_1 (name, namelength, namespace, class, list, val, 
			 coreaddr, language, objfile, TYPE_GLOBAL, 
			 TYPE_CODE_UNDEF, 0, 0, 0, native_dbg_index);
}

/* Add a symbol with a long value to a psymtab.
   Since one arg is a struct, we pass in a ptr and deref it (sigh).  
   
   name: name of the psymbol to be added. For C++ functions it should be
         mangled. If we don't have the mangled name as for 11.20 compilers 
	 we get it using the pc from the minimal_symbol_table sorted by pc. 
	 For hash_all to be able to replace the minsym with info of this
	 psymbol we need the same name in the linker symbol table and 
	 in the debug info. 

   namelength: length of the name passed in.	
   namespace: namespace of psym - could be var_namespace/struct_namespace.
   class: LOC_BLOCK for functions, LOC_STATIC for variables, LOC_TYPEDEF for
          types etc. This goes into the address class field of the psym
	  and is used to decipher what field of the union ginfo in the
	  general_symbol to read. 
   list: What list to attach this psym to eg. objfile->static_psymbols or
         objfile->global_psymbols
   val: never set for IA64 but either val or coreaddr is set.
   coreaddr: address of psym - addr of variable or lowpc of function.
   language: C/C++/fortran
   objfile: current_objfile it is a part of.
   type_codes: TYPE_CODE_STATIC, TYPE_CODE_FUNC etc. 
   is_decl: Flag that tells us if it is a declaration only.
   dies_length: helps to remove duplicate types - tells us the number of 
                dies between this type and its sibling die.		
   type_length: helps to remove duplicate types - tells us the length of
                of this user defined type.
*/

/*long val - Value as a long */
/*CORE_ADDR coreaddr - Value as a CORE_ADDR */
void
add_psymbol_to_list_1 (char *name, int namelength, namespace_enum namespace,
                       enum address_class class, struct psymbol_allocation_list *list,
                       long val, CORE_ADDR coreaddr, enum language language,
                       struct objfile *objfile, int is_global, enum type_code type_codes,
                       int is_decl, int dies_length, 
		       int type_length, unsigned native_dbg_index)
{
  struct partial_symbol *psym = NULL;
  char *buf;
  /* psymbol is static so that there will be no uninitialized gaps in the
     structure which might contain random data, causing cache misses in
     bcache. */
  static struct partial_symbol psymbol;
  struct minimal_symbol *minsym = NULL;

#ifdef HASH_ALL
  /* is_global = 1 means TYPE_GLOBAL is set = 1 in the enum. 
     is_global = 0 means TYPE_STATIC which is set = 0 in the enum. */

  /* val and coreaddr are mutually exclusive, one of them *will* be zero */
  if (val != 0)
    {
      SYMBOL_VALUE (&psymbol) = val;
    }
  else
    {
      SYMBOL_VALUE_ADDRESS (&psymbol) = coreaddr;
    }
  /* If it is a function and the language is C++ the old compiler does
   * not emit mangled names. We need to make sure we get the mangled name
   * from the minimal symbol and install that as the name.
   */

  if (type_codes == TYPE_CODE_FUNC)
    {
      if ((name == NULL || namelength == 0) || 
	  (language == language_cplus 
	   && !is_cplus_name (name, DEMANG)))   /* JAGaf49121 */
	{
	  minsym = lookup_minimal_symbol_by_pc (coreaddr);
	  if (SYMBOL_NAME (minsym)) 
	    name = SYMBOL_NAME (minsym);
	}
    }

#if defined(HP_IA64)
  /* prefetch the name pointer, since it will be used in insert_into_table() */
  _Asm_lfetch_excl (_LFTYPE_NONE, _LFHINT_NONE, name);
  _Asm_lfetch_excl (_LFTYPE_NONE, _LFHINT_NONE, name+64);
#endif 

  SYMBOL_NAME (&psymbol) = name;
  SYMBOL_SECTION (&psymbol) = 0;
  SYMBOL_LANGUAGE (&psymbol) = language;
  PSYMBOL_NAMESPACE (&psymbol) = namespace;
  PSYMBOL_CLASS (&psymbol) = class;
  PSYMBOL_PADDING (&psymbol) = 0;
  /* This says that the type is mst_unknown for when a psymbol doubles 
     as an msymbol */
  SYMBOL_INIT_LANGUAGE_SPECIFIC (&psymbol, language);
  MSYMBOL_IS_GLOBAL (&psymbol) = is_global;
  MSYMBOL_TYPE_CODE (&psymbol) = type_codes;
  MSYMBOL_IS_DECL (&psymbol) = is_decl;

  UPDATE_STRING_SIZE (namelength + 1);
  psym = insert_into_table (objfile->hash_table, (void *) (&psymbol), name,
			    NULL, 0, dies_length, type_length,
                            &objfile->symbol_obstack);

  if (psym)
    OBJSTAT (objfile, n_psyms++);

#else
  buf = (char *) alloca (namelength + 1);
  /* srikanth, 990310, JAGaa80452 do not multiply objects without necessity */

  if (permanent_copy_exists(objfile, name))
    SYMBOL_NAME (&psymbol) = name;
  else 
    {
      /* Create local copy of the partial symbol */
      memcpy (buf, name, namelength);
      buf[namelength] = '\0';
      SYMBOL_NAME (&psymbol) = bcache (buf, namelength + 1, &objfile->psymbol_cache);
    }

  /* val and coreaddr are mutually exclusive, one of them *will* be zero */
  if (val != 0)
    {
      SYMBOL_VALUE (&psymbol) = val;
    }
  else
    {
      SYMBOL_VALUE_ADDRESS (&psymbol) = coreaddr;
    }

  SYMBOL_SECTION (&psymbol) = 0;
  SYMBOL_LANGUAGE (&psymbol) = language;
  PSYMBOL_NAMESPACE (&psymbol) = namespace;
  PSYMBOL_CLASS (&psymbol) = class;
  PSYMBOL_NATIVE_INFO (&psymbol) = native_dbg_index;
  SYMBOL_INIT_LANGUAGE_SPECIFIC (&psymbol, language);

  /* Stash the partial symbol away in the cache */
  psym = bcache (&psymbol, sizeof (struct partial_symbol), &objfile->psymbol_cache);

  /* Save pointer to partial symbol in psymtab, growing symtab if needed. */
  if (list->next >= list->list + list->size)
    {
      extend_psymbol_list (list, objfile);
    }
  *list->next++ = psym;
  OBJSTAT (objfile, n_psyms++);
#endif
}

/*long val - Value as a long */
/*CORE_ADDR coreaddr -  Value as a CORE_ADDR */

void
add_psymbol_with_dem_name_to_list ( char *name, int namelength, char *dem_name,
                                    int dem_namelength, namespace_enum namespace,
     		                    enum address_class class, 
				    struct psymbol_allocation_list *list,
     			            long val, CORE_ADDR coreaddr, enum language language,
     		                    struct objfile *objfile)
{
  add_psymbol_with_dem_name_to_list_1 (name, namelength, dem_name, 
				       dem_namelength, namespace, class, 
				       list, val, coreaddr, language, 
				       objfile, TYPE_GLOBAL, TYPE_CODE_UNDEF,
					0, 0, 0);
}

/* Add a symbol with a long value to a psymtab. This differs from
 * add_psymbol_to_list above in taking both a mangled and a demangled
 * name. */

void
add_psymbol_with_dem_name_to_list_1 (char *name, int namelength, char *dem_name,
     				   int dem_namelength, namespace_enum namespace,
     				   enum address_class class,
     			           struct psymbol_allocation_list *list,
     			           long val,			/* Value as a long */
     		                   CORE_ADDR coreaddr,	/* Value as a CORE_ADDR */
     			           enum language language, struct objfile *objfile,
     				   int is_global, enum type_code type_codes,
     				   int is_decl, int dies_length, int type_length)
{
  struct partial_symbol *psym;
  char *buf;
  /* psymbol is static so that there will be no uninitialized gaps in the
     structure which might contain random data, causing cache misses in
     bcache. */
  static struct partial_symbol psymbol;

#ifdef HASH_ALL
  /* val and coreaddr are mutually exclusive, one of them *will* be zero */
  if (val != 0)
    {
      SYMBOL_VALUE (&psymbol) = val;
    }
  else
    {
      SYMBOL_VALUE_ADDRESS (&psymbol) = coreaddr;
    }

  SYMBOL_NAME (&psymbol) = name;
  SYMBOL_SECTION (&psymbol) = 0;
  SYMBOL_LANGUAGE (&psymbol) = language;
  PSYMBOL_NAMESPACE (&psymbol) = namespace;
  PSYMBOL_CLASS (&psymbol) = class;
  PSYMBOL_PADDING (&psymbol) = 0;
  /* This says that the type is mst_unknown for when a psymbol doubles 
     as an msymbol */
  SYMBOL_INIT_LANGUAGE_SPECIFIC (&psymbol, language);
  MSYMBOL_IS_GLOBAL (&psymbol) = is_global;
  MSYMBOL_TYPE_CODE (&psymbol) = type_codes;
  MSYMBOL_IS_DECL (&psymbol) = is_decl;

  UPDATE_STRING_SIZE (namelength + 1);
  psym = insert_into_table (objfile->hash_table, (void *) (&psymbol), name,
			    dem_name, 0, dies_length, type_length,
                            &objfile->symbol_obstack);
  if (psym) 
    OBJSTAT (objfile, n_psyms++);
#else
  buf = (char *) alloca (namelength + 1);
  /* srikanth, sometimes we get here with empty names. This happens
     with some versions C++ compilers and tagless structs/classes.
     There is no use for these. Just ignore them.
  */

  if (!name || !name[0] || namelength == 0)
    return;

  if (permanent_copy_exists(objfile, name))
    SYMBOL_NAME (&psymbol) = name;
  else
    {
      /* Create local copy of the partial symbol */
      memcpy (buf, name, namelength);
      buf[namelength] = '\0';
      SYMBOL_NAME (&psymbol) = bcache (buf, namelength + 1, &objfile->psymbol_cache);
    }

  buf = (char *) alloca (dem_namelength + 1);
  memcpy (buf, dem_name, dem_namelength);
  buf[dem_namelength] = '\0';

  SYMBOL_INIT_LANGUAGE_SPECIFIC (&psymbol, language);

  switch (language)
    {
    case language_c:
    case language_cplus:
      SYMBOL_CPLUS_DEMANGLED_NAME (&psymbol) =
	bcache (buf, dem_namelength + 1, &objfile->psymbol_cache);
      break;
    case language_chill:
      SYMBOL_CHILL_DEMANGLED_NAME (&psymbol) =
	bcache (buf, dem_namelength + 1, &objfile->psymbol_cache);
      break;
    case language_fortran:
      SYMBOL_FORTRAN_DEMANGLED_NAME (&psymbol) = 
        bcache (buf, dem_namelength + 1, &objfile->psymbol_cache);
    default:
      /* FIXME What should be done for the default case? Ignoring for now. */
      break;
    }

  /* val and coreaddr are mutually exclusive, one of them *will* be zero */
  if (val != 0)
    {
      SYMBOL_VALUE (&psymbol) = val;
    }
  else
    {
      SYMBOL_VALUE_ADDRESS (&psymbol) = coreaddr;
    }
  SYMBOL_SECTION (&psymbol) = 0;
  PSYMBOL_NAMESPACE (&psymbol) = namespace;
  PSYMBOL_CLASS (&psymbol) = class;
  PSYMBOL_NATIVE_INFO (&psymbol) = -1;  /* not used */

  /* Stash the partial symbol away in the cache */
  psym = bcache (&psymbol, sizeof (struct partial_symbol), &objfile->psymbol_cache);

  /* Save pointer to partial symbol in psymtab, growing symtab if needed. */
  if (list->next >= list->list + list->size)
    {
      extend_psymbol_list (list, objfile);
    }
  *list->next++ = psym;
  OBJSTAT (objfile, n_psyms++);
#endif
}

/* Initialize storage for partial symbols.  */

void
init_psymbol_list (struct objfile *objfile, int total_symbols)
{
  /* Free any previously allocated psymbol lists.  */

  if (objfile->global_psymbols.list)
    {
      mfree (objfile->md, (PTR) objfile->global_psymbols.list);
    }
  if (objfile->static_psymbols.list)
    {
      mfree (objfile->md, (PTR) objfile->static_psymbols.list);
    }

  /* Current best guess is that approximately a twentieth
     of the total symbols (in a debugging file) are global or static
     oriented symbols */

  objfile->global_psymbols.size = total_symbols / 10;
  objfile->static_psymbols.size = total_symbols / 10;

  if (objfile->global_psymbols.size > 0)
    {
      objfile->global_psymbols.next =
	objfile->global_psymbols.list = (struct partial_symbol **)
	xmmalloc (objfile->md, (objfile->global_psymbols.size
				* sizeof (struct partial_symbol *)));
    }
  if (objfile->static_psymbols.size > 0)
    {
      objfile->static_psymbols.next =
	objfile->static_psymbols.list = (struct partial_symbol **)
	xmmalloc (objfile->md, (objfile->static_psymbols.size
				* sizeof (struct partial_symbol *)));
    }
}

/* OVERLAYS:
   The following code implements an abstraction for debugging overlay sections.

   The target model is as follows:
   1) The gnu linker will permit multiple sections to be mapped into the
   same VMA, each with its own unique LMA (or load address).
   2) It is assumed that some runtime mechanism exists for mapping the
   sections, one by one, from the load address into the VMA address.
   3) This code provides a mechanism for gdb to keep track of which 
   sections should be considered to be mapped from the VMA to the LMA.
   This information is used for symbol lookup, and memory read/write.
   For instance, if a section has been mapped then its contents 
   should be read from the VMA, otherwise from the LMA.

   Two levels of debugger support for overlays are available.  One is
   "manual", in which the debugger relies on the user to tell it which
   overlays are currently mapped.  This level of support is
   implemented entirely in the core debugger, and the information about
   whether a section is mapped is kept in the objfile->obj_section table.

   The second level of support is "automatic", and is only available if
   the target-specific code provides functionality to read the target's
   overlay mapping table, and translate its contents for the debugger
   (by updating the mapped state information in the obj_section tables).

   The interface is as follows:
   User commands:
   overlay map <name>   -- tell gdb to consider this section mapped
   overlay unmap <name> -- tell gdb to consider this section unmapped
   overlay list         -- list the sections that GDB thinks are mapped
   overlay read-target  -- get the target's state of what's mapped
   overlay off/manual/auto -- set overlay debugging state
   Functional interface:
   find_pc_mapped_section(pc):    if the pc is in the range of a mapped
   section, return that section.
   find_pc_overlay(pc):       find any overlay section that contains 
   the pc, either in its VMA or its LMA
   overlay_is_mapped(sect):       true if overlay is marked as mapped
   section_is_overlay(sect):      true if section's VMA != LMA
   pc_in_mapped_range(pc,sec):    true if pc belongs to section's VMA
   pc_in_unmapped_range(...):     true if pc belongs to section's LMA
   overlay_mapped_address(...):   map an address from section's LMA to VMA
   overlay_unmapped_address(...): map an address from section's VMA to LMA
   symbol_overlayed_address(...): Return a "current" address for symbol:
   either in VMA or LMA depending on whether
   the symbol's section is currently mapped
 */

/* Overlay debugging state: */

int overlay_debugging = 0;	/* 0 == off, 1 == manual, -1 == auto */
int overlay_cache_invalid = 0;	/* True if need to refresh mapped state */

/* Target vector for refreshing overlay mapped state */
static void simple_overlay_update (struct obj_section *);
void (*target_overlay_update) (struct obj_section *) = simple_overlay_update;

/* Function: section_is_overlay (SECTION)
   Returns true if SECTION has VMA not equal to LMA, ie. 
   SECTION is loaded at an address different from where it will "run".  */

int
section_is_overlay (asection *section)
{
  if (overlay_debugging)
    if (section && section->lma != 0 &&
	section->vma != section->lma)
      return 1;

  return 0;
}

/* Function: overlay_invalidate_all (void)
   Invalidate the mapped state of all overlay sections (mark it as stale).  */

static void
overlay_invalidate_all ()
{
  struct objfile *objfile;
  struct obj_section *sect;

  ALL_OBJSECTIONS (objfile, sect)
    if (section_is_overlay (sect->the_bfd_section))
    sect->ovly_mapped = -1;
}

/* Function: overlay_is_mapped (SECTION)
   Returns true if section is an overlay, and is currently mapped. 
   Private: public access is thru function section_is_mapped.

   Access to the ovly_mapped flag is restricted to this function, so
   that we can do automatic update.  If the global flag
   OVERLAY_CACHE_INVALID is set (by wait_for_inferior), then call
   overlay_invalidate_all.  If the mapped state of the particular
   section is stale, then call TARGET_OVERLAY_UPDATE to refresh it.  */

static int
overlay_is_mapped (struct obj_section *osect)
{
  if (osect == 0 || !section_is_overlay (osect->the_bfd_section))
    return 0;

  switch (overlay_debugging)
    {
    default:
    case 0:
      return 0;			/* overlay debugging off */
    case -1:			/* overlay debugging automatic */
      /* Unles there is a target_overlay_update function, 
         there's really nothing useful to do here (can't really go auto)  */
      if (target_overlay_update)
	{
	  if (overlay_cache_invalid)
	    {
	      overlay_invalidate_all ();
	      overlay_cache_invalid = 0;
	    }
	  if (osect->ovly_mapped == -1)
	    (*target_overlay_update) (osect);
	}
      /* fall thru to manual case */
    case 1:			/* overlay debugging manual */
      return osect->ovly_mapped == 1;
    }
}

/* Function: section_is_mapped
   Returns true if section is an overlay, and is currently mapped.  */

int
section_is_mapped (asection *section)
{
  struct objfile *objfile;
  struct obj_section *osect;

  if (overlay_debugging)
    if (section && section_is_overlay (section))
      ALL_OBJSECTIONS (objfile, osect)
	if (osect->the_bfd_section == section)
	return overlay_is_mapped (osect);

  return 0;
}

/* Function: pc_in_unmapped_range
   If PC falls into the lma range of SECTION, return true, else false.  */

CORE_ADDR
pc_in_unmapped_range (CORE_ADDR pc, asection *section)
{
  int size;

  if (overlay_debugging)
    if (section && section_is_overlay (section))
      {
	size = (int) bfd_get_section_size_before_reloc (section);
	if (section->lma <= pc && pc < section->lma + size)
	  return 1;
      }
  return 0;
}

/* Function: pc_in_mapped_range
   If PC falls into the vma range of SECTION, return true, else false.  */

CORE_ADDR
pc_in_mapped_range (CORE_ADDR pc, asection *section)
{
  int size;

  if (overlay_debugging)
    if (section && section_is_overlay (section))
      {
	size = (int) bfd_get_section_size_before_reloc (section);
	if (section->vma <= pc && pc < section->vma + size)
	  return 1;
      }
  return 0;
}

/* Function: overlay_unmapped_address (PC, SECTION)
   Returns the address corresponding to PC in the unmapped (load) range.
   May be the same as PC.  */

CORE_ADDR
overlay_unmapped_address (CORE_ADDR pc, asection *section)
{
  if (overlay_debugging)
    if (section && section_is_overlay (section) &&
	pc_in_mapped_range (pc, section))
      return pc + section->lma - section->vma;

  return pc;
}

/* Function: overlay_mapped_address (PC, SECTION)
   Returns the address corresponding to PC in the mapped (runtime) range.
   May be the same as PC.  */

CORE_ADDR
overlay_mapped_address (CORE_ADDR pc, asection *section)
{
  if (overlay_debugging)
    if (section && section_is_overlay (section) &&
	pc_in_unmapped_range (pc, section))
      return pc + section->vma - section->lma;

  return pc;
}


/* Function: symbol_overlayed_address 
   Return one of two addresses (relative to the VMA or to the LMA),
   depending on whether the section is mapped or not.  */

CORE_ADDR
symbol_overlayed_address (CORE_ADDR address, asection *section)
{
  if (overlay_debugging)
    {
      /* If the symbol has no section, just return its regular address. */
      if (section == 0)
	return address;
      /* If the symbol's section is not an overlay, just return its address */
      if (!section_is_overlay (section))
	return address;
      /* If the symbol's section is mapped, just return its address */
      if (section_is_mapped (section))
	return address;
      /*
       * HOWEVER: if the symbol is in an overlay section which is NOT mapped,
       * then return its LOADED address rather than its vma address!!
       */
      return overlay_unmapped_address (address, section);
    }
  return address;
}

/* Function: find_pc_overlay (PC) 
   Return the best-match overlay section for PC:
   If PC matches a mapped overlay section's VMA, return that section.
   Else if PC matches an unmapped section's VMA, return that section.
   Else if PC matches an unmapped section's LMA, return that section.  */

asection *
find_pc_overlay (CORE_ADDR pc)
{
  struct objfile *objfile;
  struct obj_section *osect, *best_match = NULL;

  if (overlay_debugging)
    ALL_OBJSECTIONS (objfile, osect)
      if (section_is_overlay (osect->the_bfd_section))
      {
	if (pc_in_mapped_range (pc, osect->the_bfd_section))
	  {
	    if (overlay_is_mapped (osect))
	      return osect->the_bfd_section;
	    else
	      best_match = osect;
	  }
	else if (pc_in_unmapped_range (pc, osect->the_bfd_section))
	  best_match = osect;
      }
  return best_match ? best_match->the_bfd_section : NULL;
}

/* Function: find_pc_mapped_section (PC)
   If PC falls into the VMA address range of an overlay section that is 
   currently marked as MAPPED, return that section.  Else return NULL.  */

asection *
find_pc_mapped_section (CORE_ADDR pc)
{
  struct objfile *objfile;
  struct obj_section *osect;

  if (overlay_debugging)
    ALL_OBJSECTIONS (objfile, osect)
      if (pc_in_mapped_range (pc, osect->the_bfd_section) &&
	  overlay_is_mapped (osect))
      return osect->the_bfd_section;

  return NULL;
}

/* Function: list_overlays_command
   Print a list of mapped sections and their PC ranges */

void
list_overlays_command (char *args, int from_tty)
{
  int nmapped = 0;
  struct objfile *objfile;
  struct obj_section *osect;

  if (overlay_debugging)
    ALL_OBJSECTIONS (objfile, osect)
      if (overlay_is_mapped (osect))
      {
	const char *name;
	bfd_vma lma, vma;
	int size;

	vma = bfd_section_vma (objfile->obfd, osect->the_bfd_section);
	lma = bfd_section_lma (objfile->obfd, osect->the_bfd_section);
	size = (int) bfd_get_section_size_before_reloc (osect->the_bfd_section);
	name = bfd_section_name (objfile->obfd, osect->the_bfd_section);

	printf_filtered ("Section %s, loaded at ", name);
	print_address_numeric (lma, 1, gdb_stdout);
	puts_filtered (" - ");
	print_address_numeric (lma + size, 1, gdb_stdout);
	printf_filtered (", mapped at ");
	print_address_numeric (vma, 1, gdb_stdout);
	puts_filtered (" - ");
	print_address_numeric (vma + size, 1, gdb_stdout);
	puts_filtered ("\n");

	nmapped++;
      }
  if (nmapped == 0)
    printf_filtered ("No sections are mapped.\n");
}

/* Function: map_overlay_command
   Mark the named section as mapped (ie. residing at its VMA address).  */

void
map_overlay_command (char *args, int from_tty)
{
  struct objfile *objfile, *objfile2;
  struct obj_section *sec, *sec2;
  asection *bfdsec;

  if (!overlay_debugging)
    error ("\
Overlay debugging not enabled.  Use either the 'overlay auto' or\n\
the 'overlay manual' command.");

  if (args == 0 || *args == 0)
    error ("Argument required: name of an overlay section");

  /* First, find a section matching the user supplied argument */
  ALL_OBJSECTIONS (objfile, sec)
    if (!strcmp (bfd_section_name (objfile->obfd, sec->the_bfd_section), args))
    {
      /* Now, check to see if the section is an overlay. */
      bfdsec = sec->the_bfd_section;
      if (!section_is_overlay (bfdsec))
	continue;		/* not an overlay section */

      /* Mark the overlay as "mapped" */
      sec->ovly_mapped = 1;

      /* Next, make a pass and unmap any sections that are
         overlapped by this new section: */
      ALL_OBJSECTIONS (objfile2, sec2)
	if (sec2->ovly_mapped &&
	    sec != sec2 &&
	    sec->the_bfd_section != sec2->the_bfd_section &&
	    (pc_in_mapped_range (sec2->addr, sec->the_bfd_section) ||
	     pc_in_mapped_range (sec2->endaddr, sec->the_bfd_section)))
	{
	  if (info_verbose)
	    printf_filtered ("Note: section %s unmapped by overlap\n",
			     bfd_section_name (objfile->obfd,
					       sec2->the_bfd_section));
	  sec2->ovly_mapped = 0;	/* sec2 overlaps sec: unmap sec2 */
	}
      return;
    }
  error ("No overlay section called %s", args);
}

/* Function: unmap_overlay_command
   Mark the overlay section as unmapped 
   (ie. resident in its LMA address range, rather than the VMA range).  */

void
unmap_overlay_command (char *args, int from_tty)
{
  struct objfile *objfile;
  struct obj_section *sec;

  if (!overlay_debugging)
    error ("\
Overlay debugging not enabled.  Use either the 'overlay auto' or\n\
the 'overlay manual' command.");

  if (args == 0 || *args == 0)
    error ("Argument required: name of an overlay section");

  /* First, find a section matching the user supplied argument */
  ALL_OBJSECTIONS (objfile, sec)
    if (!strcmp (bfd_section_name (objfile->obfd, sec->the_bfd_section), args))
    {
      if (!sec->ovly_mapped)
	error ("Section %s is not mapped", args);
      sec->ovly_mapped = 0;
      return;
    }
  error ("No overlay section called %s", args);
}

/* Function: overlay_auto_command
   A utility command to turn on overlay debugging.
   Possibly this should be done via a set/show command. */

static void
overlay_auto_command (char *args, int from_tty)
{
  overlay_debugging = -1;
  if (info_verbose)
    printf_filtered ("Automatic overlay debugging enabled.");
}

/* Function: overlay_manual_command
   A utility command to turn on overlay debugging.
   Possibly this should be done via a set/show command. */

static void
overlay_manual_command (char *args, int from_tty)
{
  overlay_debugging = 1;
  if (info_verbose)
    printf_filtered ("Overlay debugging enabled.");
}

/* Function: overlay_off_command
   A utility command to turn on overlay debugging.
   Possibly this should be done via a set/show command. */

static void
overlay_off_command (char *args, int from_tty)
{
  overlay_debugging = 0;
  if (info_verbose)
    printf_filtered ("Overlay debugging disabled.");
}

static void
overlay_load_command (char *args, int from_tty)
{
  if (target_overlay_update)
    (*target_overlay_update) (NULL);
  else
    error ("This target does not know how to read its overlay state.");
}

/* Function: overlay_command
   A place-holder for a mis-typed command */

/* Command list chain containing all defined "overlay" subcommands. */
struct cmd_list_element *overlaylist;

static void
overlay_command (char *args, int from_tty)
{
  printf_unfiltered
    ("\"overlay\" must be followed by the name of an overlay command.\n");
  help_list (overlaylist, "overlay ", -1, gdb_stdout);
}


/* Target Overlays for the "Simplest" overlay manager:

   This is GDB's default target overlay layer.  It works with the 
   minimal overlay manager supplied as an example by Cygnus.  The 
   entry point is via a function pointer "target_overlay_update", 
   so targets that use a different runtime overlay manager can 
   substitute their own overlay_update function and take over the
   function pointer.

   The overlay_update function pokes around in the target's data structures
   to see what overlays are mapped, and updates GDB's overlay mapping with
   this information.

   In this simple implementation, the target data structures are as follows:
   unsigned _novlys;            /# number of overlay sections #/
   unsigned _ovly_table[_novlys][4] = {
   {VMA, SIZE, LMA, MAPPED},    /# one entry per overlay section #/
   {..., ...,  ..., ...},
   }
   unsigned _novly_regions;     /# number of overlay regions #/
   unsigned _ovly_region_table[_novly_regions][3] = {
   {VMA, SIZE, MAPPED_TO_LMA},  /# one entry per overlay region #/
   {..., ...,  ...},
   }
   These functions will attempt to update GDB's mappedness state in the
   symbol section table, based on the target's mappedness state.

   To do this, we keep a cached copy of the target's _ovly_table, and
   attempt to detect when the cached copy is invalidated.  The main
   entry point is "simple_overlay_update(SECT), which looks up SECT in
   the cached table and re-reads only the entry for that section from
   the target (whenever possible).
 */

/* Cached, dynamically allocated copies of the target data structures: */
static unsigned (*cache_ovly_table)[4] = 0;
#if 0
static unsigned (*cache_ovly_region_table)[3] = 0;
#endif
static unsigned cache_novlys = 0;
#if 0
static unsigned cache_novly_regions = 0;
#endif
static CORE_ADDR cache_ovly_table_base = 0;
#if 0
static CORE_ADDR cache_ovly_region_table_base = 0;
#endif
enum ovly_index
  {
    VMA, SIZE, LMA, MAPPED
  };
#define TARGET_LONG_BYTES (TARGET_LONG_BIT / TARGET_CHAR_BIT)

/* Throw away the cached copy of _ovly_table */
static void
simple_free_overlay_table ()
{
  if (cache_ovly_table)
    free (cache_ovly_table);
  cache_novlys = 0;
  cache_ovly_table = NULL;
  cache_ovly_table_base = 0;
}

#if 0
/* Throw away the cached copy of _ovly_region_table */
static void
simple_free_overlay_region_table ()
{
  if (cache_ovly_region_table)
    free (cache_ovly_region_table);
  cache_novly_regions = 0;
  cache_ovly_region_table = NULL;
  cache_ovly_region_table_base = 0;
}
#endif

/* Read an array of ints from the target into a local buffer.
   Convert to host order.  int LEN is number of ints  */
static void
read_target_long_array (CORE_ADDR memaddr, unsigned int *myaddr, int len)
{
  char *buf = (char *) alloca (len * TARGET_LONG_BYTES);
  int i;

  read_memory (memaddr, buf, len * TARGET_LONG_BYTES);
  for (i = 0; i < len; i++)
    myaddr[i] = (unsigned int) extract_unsigned_integer (TARGET_LONG_BYTES * i + buf,
					  TARGET_LONG_BYTES);
}

/* Find and grab a copy of the target _ovly_table
   (and _novlys, which is needed for the table's size) */
static int
simple_read_overlay_table ()
{
  struct minimal_symbol *msym;

  simple_free_overlay_table ();
  msym = lookup_minimal_symbol ("_novlys", 0, 0);
  if (msym != NULL)
    cache_novlys = (unsigned int) read_memory_integer (SYMBOL_VALUE_ADDRESS (msym), 4);
  else
    return 0;			/* failure */
  cache_ovly_table = (void *) xmalloc (cache_novlys * sizeof (*cache_ovly_table));
  if (cache_ovly_table != NULL)
    {
      msym = lookup_minimal_symbol ("_ovly_table", 0, 0);
      if (msym != NULL)
	{
	  cache_ovly_table_base = SYMBOL_VALUE_ADDRESS (msym);
	  read_target_long_array (cache_ovly_table_base,
				  (unsigned int *) cache_ovly_table,
				  cache_novlys * 4);
	}
      else
	return 0;		/* failure */
    }
  else
    return 0;			/* failure */
  return 1;			/* SUCCESS */
}

#if 0
/* Find and grab a copy of the target _ovly_region_table
   (and _novly_regions, which is needed for the table's size) */
static int
simple_read_overlay_region_table ()
{
  struct minimal_symbol *msym;

  simple_free_overlay_region_table ();
  msym = lookup_minimal_symbol ("_novly_regions", 0, 0);
  if (msym != NULL)
    cache_novly_regions = read_memory_integer (SYMBOL_VALUE_ADDRESS (msym), 4);
  else
    return 0;			/* failure */
  cache_ovly_region_table = (void *) xmalloc (cache_novly_regions * 12);
  if (cache_ovly_region_table != NULL)
    {
      msym = lookup_minimal_symbol ("_ovly_region_table", 0, 0);
      if (msym != NULL)
	{
	  cache_ovly_region_table_base = SYMBOL_VALUE_ADDRESS (msym);
	  read_target_long_array (cache_ovly_region_table_base,
				  (int *) cache_ovly_region_table,
				  cache_novly_regions * 3);
	}
      else
	return 0;		/* failure */
    }
  else
    return 0;			/* failure */
  return 1;			/* SUCCESS */
}
#endif

/* Function: simple_overlay_update_1 
   A helper function for simple_overlay_update.  Assuming a cached copy
   of _ovly_table exists, look through it to find an entry whose vma,
   lma and size match those of OSECT.  Re-read the entry and make sure
   it still matches OSECT (else the table may no longer be valid).
   Set OSECT's mapped state to match the entry.  Return: 1 for
   success, 0 for failure.  */

static int
simple_overlay_update_1 (struct obj_section *osect)
{
  int i;

  for (i = 0; i < cache_novlys; i++)
    if (cache_ovly_table[i][VMA] == osect->the_bfd_section->vma &&
	cache_ovly_table[i][LMA] == osect->the_bfd_section->lma)
      {
	read_target_long_array (cache_ovly_table_base + i * TARGET_LONG_BYTES,
				(unsigned int *) cache_ovly_table[i], 4);
	if (cache_ovly_table[i][VMA] == osect->the_bfd_section->vma &&
	    cache_ovly_table[i][LMA] == osect->the_bfd_section->lma	/* &&
									   cache_ovly_table[i][SIZE] == size */ )
	  {
	    osect->ovly_mapped = cache_ovly_table[i][MAPPED];
	    return 1;
	  }
	else			/* Warning!  Warning!  Target's ovly table has changed! */
	  return 0;
      }
  return 0;
}

/* Function: simple_overlay_update
   If OSECT is NULL, then update all sections' mapped state 
   (after re-reading the entire target _ovly_table). 
   If OSECT is non-NULL, then try to find a matching entry in the 
   cached ovly_table and update only OSECT's mapped state.
   If a cached entry can't be found or the cache isn't valid, then 
   re-read the entire cache, and go ahead and update all sections.  */

static void
simple_overlay_update (struct obj_section *osect)
{
  struct objfile *objfile;

  /* Were we given an osect to look up?  NULL means do all of them. */
  if (osect)
    /* Have we got a cached copy of the target's overlay table? */
    if (cache_ovly_table != NULL)
      /* Does its cached location match what's currently in the symtab? */
      if (cache_ovly_table_base ==
	  SYMBOL_VALUE_ADDRESS (lookup_minimal_symbol ("_ovly_table", 0, 0)))
	/* Then go ahead and try to look up this single section in the cache */
	if (simple_overlay_update_1 (osect))
	  /* Found it!  We're done. */
	  return;

  /* Cached table no good: need to read the entire table anew.
     Or else we want all the sections, in which case it's actually
     more efficient to read the whole table in one block anyway.  */

  if (simple_read_overlay_table () == 0)	/* read failed?  No table? */
    {
      warning ("Failed to read the target overlay mapping table.");
      return;
    }
  /* Now may as well update all sections, even if only one was requested. */
  ALL_OBJSECTIONS (objfile, osect)
    if (section_is_overlay (osect->the_bfd_section))
    {
      int i;

      for (i = 0; i < cache_novlys; i++)
	if (cache_ovly_table[i][VMA] == osect->the_bfd_section->vma &&
	    cache_ovly_table[i][LMA] == osect->the_bfd_section->lma)
	  {			/* obj_section matches i'th entry in ovly_table */
	    osect->ovly_mapped = cache_ovly_table[i][MAPPED];
	    break;		/* finished with inner for loop: break out */
	  }
    }
}

/* if ! overridden by platform specific code, don't know what the answer is */
static boolean
generic_copy_exists (struct objfile * objfile, char * string)
{
    return 0;  /* say no */
}

void
_initialize_symfile ()
{
#ifdef GDB_TARGET_IS_HPUX
  extern int incremental_expansion;
#endif

#ifdef TARGET_HAS_DL_HEADER
  extern int initialize_import_list;
#endif
  struct cmd_list_element *c;

  c = add_cmd ("symbol-file", class_files, symbol_file_command,
	       "Load symbol table from executable file FILE.\n\n\
Usage:\n\tsymbol-file [<FILE>]\n\n\
The `file' command can also load symbol tables, as well as setting the file\n\
to execute. No arg means to have no symbols.", &cmdlist);
  c->completer = filename_completer;

  c = add_cmd ("add-symbol-file", class_files, add_symbol_file_command,
	       "Load the symbols from FILE, assuming FILE has been dynamically loaded.\n\n\
Usage:\n\tadd-symbol-file <FILE> <ADDR> [-s <SECT1> <SECT-ADDR1>] \
[-s <SECT2> <SECT-ADDR2>] [...]\n\n\
ADDR is the starting address of the file's text.\n\
The optional arguments are section-name section-address pairs and\n\
should be specified if the data and bss segments are not contiguous\n\
with the text. SECT is a section name to be loaded at SECT_ADDR.",
	       &cmdlist);
  c->completer = filename_completer;

  c = add_cmd ("add-shared-symbol-files", class_files,
	       add_shared_symbol_files_command,
   "Load the symbols from shared objects in the dynamic linker's link map.",
	       &cmdlist);
  c = add_alias_cmd ("assf", "add-shared-symbol-files", class_files, 1,
		     &cmdlist);

  c = add_cmd ("load", class_files, load_command,
	       "Dynamically load FILE into the running program, and record\n\
its symbols for access from GDB.\n\n\
Usage:\n\tload <FILE>\n\n", &cmdlist);
  c->completer = filename_completer;

  add_show_from_set
    (add_set_cmd ("symbol-reloading", class_support, var_boolean,
		  (char *) &symbol_reloading,
	    "Set dynamic symbol table reloading multiple times in one run.\n\n"
            "Usage:\nTo set new value:\n\tset symbol-reloading [on | off]\nTo "
            "see current value:\n\tshow symbol-reloading\n",
		  &setlist),
     &showlist);

#ifdef GDB_TARGET_IS_HPUX
  add_show_from_set
    (add_set_cmd ("incremental-expansion", class_support, var_boolean,
                  (char *) &incremental_expansion,
            "Set incremental symbol table processing behavior.\n\n\
Usage:\nTo set new value:\n\tset incremental-expansion [on | off]\n\
To see current value:\n\tshow incremental-expansion\n",
                  &setlist),
     &showlist);
#endif

#ifdef TARGET_HAS_DL_HEADER
  add_show_from_set
    (add_set_cmd ("initialize-import-list", class_support, var_boolean,
                  (char *) &initialize_import_list,
            "Set whether the import list should be initialized at start up.",
                  &setlist),
     &showlist);

#endif

  add_prefix_cmd ("overlay", class_support, overlay_command,
		  "Commands for debugging overlays.", &overlaylist,
		  "overlay ", 0, &cmdlist);

  add_com_alias ("ovly", "overlay", class_alias, 1);
  add_com_alias ("ov", "overlay", class_alias, 1);

  add_cmd ("map-overlay", class_support, map_overlay_command,
	   "Assert that an overlay section is mapped.\n\n\
Usage:\n\toverlay map-overlay <SECTION>\n", &overlaylist);

  add_cmd ("unmap-overlay", class_support, unmap_overlay_command,
	   "Assert that an overlay section is unmapped.\n\n\
Usage:\n\toverlay unmap-overlay\n", &overlaylist);

  add_cmd ("list-overlays", class_support, list_overlays_command,
	   "List mappings of overlay sections.\n\n\
Usage:\n\toverlay list-overlays\n", &overlaylist);

  add_cmd ("manual", class_support, overlay_manual_command,
	   "Enable overlay debugging.\n\n\
Usage:\n\toverlay manual\n", &overlaylist);
  add_cmd ("off", class_support, overlay_off_command,
	   "Disable overlay debugging.\n\nUsage:\n\toverlay off\n", &overlaylist);
  add_cmd ("auto", class_support, overlay_auto_command,
	   "Enable automatic overlay debugging.\n\n\
Usage:\n\toverlay auto\n", &overlaylist);
  add_cmd ("load-target", class_support, overlay_load_command,
	   "Read the overlay mapping state from the target.\n\n\
Usage:\n\toverlay load-target\n", &overlaylist);

  /* Filename extension to source language lookup table: */
  init_filename_language_table ();
  c = add_set_cmd ("extension-language", class_files, var_string_noescape,
		   (char *) &ext_args,
		   "Set mapping between filename extension and source language.\n\n\
Usage:\n\tset extension-language .<EXT> <LANGUAGE>\n\n\
eg: set extension-language .foo bar",
		   &setlist);
  c->function.cfunc = set_ext_lang_command;

  add_info ("extensions", info_ext_lang_command,
	    "All filename extensions associated with a source language.\n\n\
Usage:\n\tinfo extensions\n");

  add_show_from_set
    (add_set_cmd ("download-write-size", class_obscure,
		  var_integer, (char *) &download_write_size,
		  "Set the write size used when downloading a program.\n\n"
                  "Usage:\nTo set new value:\n\tset download-write-size "
                  "<INTEGER>\nTo see current value:\n\tshow download-write-size"
                  "\n\nOnly used when downloading a program onto a remote target"
                  ".\nSpecify zero or a negative value, as argument, to disable"
		  " blocked writes.\nThe actual size of each transfer is also"
		  "limited by the size of the target packet\nand the memory"
		  "cache.\n",
		  &setlist),
     &showlist);

#ifdef MSYMBOL_DOUBLES_AS_PSYMBOL
    if (   (long long) &((struct minimal_symbol*) 0)->info 
	!= (long long) &((struct partial_symbol*) 0)->native_debug_info_index)
      error (
	"struct minimal_symbol and struct partial_symbol should be similar"); 
#endif

  add_show_from_set
    (add_set_cmd ("reread-on-run", class_support, var_boolean,
                  (char *) &reread_on_run,
                  "Set re-reading of symbols from exec file every time the run"
                  " command is issued.\n\nUsage:\nTo set new value:\n\tSet "
                  "reread-on-run [on | off]\nTo see current value:\n\tshow"
                  " reread-on-run\nNote: This feature is not supported.\n",
                  &setlist),
     &showlist);

} /* end _initialize_symfile */
