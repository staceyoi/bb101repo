/* Symbol table lookup for the GNU debugger, GDB.
   Copyright 1986, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "gdbcore.h"
#include "frame.h"
#include "target.h"
#include "value.h"
#include "symfile.h"
#include "objfiles.h"
#include "gdbcmd.h"
#include "call-cmds.h"
#include "gdb_regex.h"
#include "expression.h"
#include "language.h"
#include "demangle.h"
#include "inferior.h"

#include "obstack.h"

#include <sys/types.h>
#include <fcntl.h>
#include "gdb_string.h"
#include "gdb_stat.h"
#include <ctype.h>
#include "inferior.h"
#ifdef HASH_ALL
#include "fastsym.h"
#endif
#include <assert.h>

#define DEBUG(x)  

/* To support namespaces this is the number of initial using
   directives that we allocate for.*/
#define INIT_USING_ALLOC 20

/* To support namespaces this is the number of initial symbols
   that we allocate for.*/
#define INIT_SYM_ALLOC   20

/* Used to cache the last seen value in lookup_symbol */

static struct {
  struct symbol *sym_returned;
  struct block *block_arg;
  namespace_enum namespace_arg;
  char *name_arg;
  struct symtab *symtab_arg;
  int is_a_field_of_this;
} cached_sym = {NULL, NULL, NULL, NULL, NULL, 0};
extern int use_cached_value; /* => from top.c */

extern int match_one_line_flag;

static int debug_ccover = 0;
extern int procedure_flag;

/* JAGaf23775 - We must override choices if we have overload
   functions. This variable is set to say that we need to skip menu
   and is used for xbp as of now. */
extern boolean skip_bp_menu;

extern int executing_next_command;
extern int next_command_inline_idx;

/* JAGae68084 */
extern int in_shl_load;
extern int if_yes_to_all_commands;
extern char * shllib_name;

/* Poorva: Stores the symtab that the minimal symbol was found in and
   also the minsym that was found. Used in find_functions.
   Since we know what symtab contains the minsym we just look
   through those symtabs instead of looking through all the symtabs. 
*/

/* JAGaf62562 - <info line main> throws up a menu incase of fortran executable for PA GDB.*/
struct symtab_and_minsym
  {
    struct symtab *symtab;       
    struct minimal_symbol *minsym;
  }* symtab_and_minsym_arr;
/* JAGaf62562 - <END> */

/* Prototype for one function in breakpoint.c */
extern void break_at_finish_command_1 (char *, int, int);

/* Prototype for one function in parser-defs.h,
   instead of including that entire file. */

extern char *find_template_name_end (char *);


/* Prototypes for local functions */

static int find_methods (struct type *, char *, struct symbol **);

extern int
find_n_block_functions (struct block *, char *,namespace_enum,
			int, struct symbol **, int);

extern int
find_functions_in_symtab (char *, struct symbol **, 
			  struct symtab_and_block *, struct symtab *);

static struct symtab *find_line_symtab (struct symtab *, int, int *, int *);

static int find_n_line_symtabs (struct symtab *, int);

static int
find_line_symtabs (struct symtab *, int, int, struct symtab_and_line *);

static int
count_exact_line_match(struct linetable *, int, struct symtab *, struct symtab_and_line *);

static void completion_list_add_name (char *, char *, int, char *, char *);

static void build_canonical_line_spec (struct symtab_and_line *,
				       char *, char ***);

static void build_canonical_line_spec_n (struct symtabs_and_lines *,
				       char *, char ***);

/* Given the name, this function goes through the inline table
   and returns the pc associated with them. Defined in hpux-inline.c
*/
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
extern CORE_ADDR* find_all_inline_instances (char *, char *, int, int *); 
#endif

/* JAGae68084 */
void rbreak_all_command (char *, int);

static void rbreak_command (char *, int);

static void types_info (char *, int);

static void functions_info (char *, int);

static void variables_info (char *, int);

static void sources_info (char *, int);

static void output_source_filename (char *, int *);

char *operator_chars (char *, char **);

static int find_line_common (struct linetable *, int, int *);

struct partial_symbol *lookup_partial_symbol (struct partial_symtab *,
					      const char *, int,
					      namespace_enum);
#ifndef HASH_ALL
static struct partial_symbol *fixup_psymbol_section (struct
						     partial_symbol *,
						     struct objfile *);
#endif

static struct symtab *lookup_symtab_1 (char *);

struct symbol *
lookup_block_transparent_type (const struct block *,  const char *);

static struct symbol *find_active_alias (struct symbol *sym, CORE_ADDR addr);
struct symtab *
find_symtab PARAMS ((struct minimal_symbol *));

static struct partial_symtab *
find_psymtab PARAMS ((struct minimal_symbol *));


static struct symtabs_and_lines
decode_line_2_reuse PARAMS ((struct symbol *[], int, int, char ***, struct symtab_and_block[], enum how_to_resolve_multiple_symbols, char*, int));

static struct symtabs_and_lines
decode_line_2_reuse_minsyms PARAMS ((struct minsym_and_objfile *, int, int, char ***, enum how_to_resolve_multiple_symbols));

static struct symbol *
lookup_symbol_1 PARAMS ((const char *, register const struct block *,
			 const namespace_enum, int *, struct symtab **,
			 int *));

static struct symbol *choose_symbol PARAMS ((const char *, struct symbol *));

static const char *check_alias 
  PARAMS ((const char *, const struct block *, struct type *));

static struct symbol * check_ns_usings PARAMS ((const char *, const struct block *));

static int check_ns PARAMS ((const char *, const struct block *));

static void free_arrays PARAMS ((void));

/* This flag is used in hppa-tdep.c, and set in hp-symtab-read.c */
/* Signals the presence of objects compiled by HP compilers */
int hp_som_som_object_present = 0;

static void fixup_section (struct general_symbol_info *, struct objfile *);

static int file_matches (char *, char **, int);

static void print_symbol_info (namespace_enum,
			       struct symtab *, struct symbol *, int, char *);

static void print_msymbol_info (struct minimal_symbol *);

static void symtab_symbol_info (char *, namespace_enum, int);

static void overload_list_add_symbol (struct symbol *sym, char *oload_name);

void _initialize_symtab (void);

/* */

/* The single non-language-specific builtin type */
struct type *builtin_type_error;

/* Block in which the most recently searched-for symbol was found.
   Might be better to make this a parameter to lookup_symbol and 
   value_of_this. */

const struct block *block_found;

static const char no_symtab_msg[] = "No symbol table is loaded.  Use the \"file\" command.";

char *default_main = "main";

char *cplus_demangled_name; /* JAGaf49121 */

/* Initialize the maximum number of overloads of any symbol name to the
   previous static allocation size.  On IPF we'll increase the size if we
   see more overloads of some symbol, so our allocations are always
   guaranteed to be sufficient and we won't need the realloc() calls
   envisioned by the submitter of JAGaf02061, "Should realloc space for more
   overloaded functions as required".  On PA will continue to operate as
   it did with the fixed size arrays. */

int max_synonyms_seen = MAX_NUMBER_OF_OVERLOADED_OR_STATIC_FUNCTION_DEFINITIONS;

static const char char_markers[] =
{'_', '$', '\0'};

/* To filter out the strings that start with _ or $ ,04/15/02 
   Dinesh */
int
is_char_marker (int c)
{
  return c && strchr (char_markers, c) != NULL;
}

/* This is the final list of all the extra names found due to 
   a) using directives, 
   b) namespace aliases and 
   c) prefixing the name of the symbol being looked for with 
   the namespace names of a namespace function if we are in one. 

   While still in the middle of find_using_directives the array 
   will be populated with namespace names and not the whole symbol
   name to look for. */
char **symbol_names = NULL;

/* Holds symbols found due to using directives inside the 
   namespace that the symbol is qualifed with - e.g
   if a user is looking for A::a these are the symbols created
   due to using directives inside namespace A. 
   Say A has using ns B so new_names will also have B::a  */
char **new_names = NULL;

/* Holds ths symbols for all the namespaces found in symbol_names
   e.g symbol_names has the string "A" in it for namespace
   A using_syms will have the symbol associated with namespace A 
   stored in it. The symbol will be at the same array index as 
   the index of the namespace name in symbol_names*/
struct symbol **using_syms = NULL;

int num_using_syms = 0;
/* Holds the list of all the symbols found from the names
   given in symbol_names */
struct symbol **ns_symbols = NULL;

/* Number of entries present in the array symbol_names */
int num_symbol_names = 0; 
/* Number of entries present in the array ns_symbols */
int num_ns_symbols = 0; 
/* Number of entries present in the array new_names */
int num_new_names = 0;
/* Used to allocate space for symbol_names and ns_symbols */
int using_alloc;
/* Used to allocate space for new_names */
int names_alloc;


/* variable set to 1 if an ambiguity is detected between the 
   symbol name you are looking for and the name of the symbol 
   being returned. This may happen for namespaces because of 

   a) We look for symbols that are prefixed with the using 
   directive names too. e.g the user said print j and we found
   a B::j due to the presence of the using directive for ns B.
      
   b) A using declaration was present and so print k translates
   to print A::k where there is a useing A::k; in the source

   This variable is then used in eval.c where we lookup a 
   variable and if an ambiguity was detected we print out the
   name of the real variable we are presenting the user with.

   It is also used in decode_line_2_reuse where we present the 
   user with a menu of function names to choose from. If an ambiguity
   has been detected we actually present him with the using_declaration_name
   instead.
*/

int ambiguity_detected; 
extern int namespace_enabled; /* From source.c; zero => disabled */


/* Check for a symtab of a specific name; first in symtabs, then in
   psymtabs.  *If* there is no '/' in the name, a match after a '/'
   in the symtab filename will also work.  */

static struct symtab *
lookup_symtab_1 (char *name)
{
  register struct symtab *s = 0; /* initialize for compiler warning */
  register struct partial_symtab *ps;
  register char *slash;
  register struct objfile *objfile;
  int i;
  char * basename;
  char * pathname = 0;
  char * f_pathname = 0;
  char * s_pathname = 0;
  int foundit = 0;

  char ccover_tmpdir[] = "/tmp/covc";
  for (i = 0; i < 2; i++)
    {
      /* First, search for an exact match */
      /*JAGae68084 - See if the objfilename matches the library name*/

      ALL_SYMTABS (objfile, s)
       {
         char * objfl_name=s->objfile->name;
	 if (STREQ (name, s->filename)|| STREQ(name, objfl_name) || STREQ (name, getbasename(objfile->name)))
	   return s;
       }

      slash = strchr (name, '/');

      /* Now, search for a matching tail (only if name doesn't have any dirs) */

      if (!slash)
	{
	  ALL_SYMTABS (objfile, s)
	  {
	    char *p = s->filename;
	    char *tail = strrchr (p, '/');

	    if (tail)
	      p = tail + 1;

	    if (STREQ (p, name))
	      return s;
	  }
	}

      /* Search for matching tail expanding filename and symtab's
		{
		  if (debug_ccover)
		    {
		      char *c = strstr (s->filename, ccover_tmpdir);
		      if (c)
			continue;
		    }
		  return s;
		}
         filename to their full pathname */

      name = tilde_expand (name);
      basename = getbasename(name);
      if (source_full_path_of (name, &pathname))
        {
          f_pathname = pathopt (pathname);
          free (pathname);
        }
      else
        f_pathname = pathopt (name);
      ALL_SYMTABS (objfile, s)
        {
	  struct stat f_stat, s_stat;
          if (!STREQ (basename, getbasename (s->filename)))
            continue;
	  else
	    {
	      if (debug_ccover)
	        {
		  char *c = strstr (s->filename, ccover_tmpdir);
		  if (c)
		    continue;
	        }
	    }
          s_pathname = symtab_to_filename (s);
          if (STREQ (f_pathname, s_pathname))
            {
              foundit = 1;
              goto get_out;
            }
           /* These may still be the same files because of automounts, etc;
             use info from stat to determine if they are the same file */
          if (!stat (f_pathname, &f_stat) && !stat (s_pathname, &s_stat) &&
              f_stat.st_dev == s_stat.st_dev && f_stat.st_ino == s_stat.st_ino)
            return s;
        }

get_out:
      free (f_pathname);
      if (foundit)
        return s;

      /* Same search rules as above apply here, but now we look thru the
         psymtabs.  */

      ps = lookup_partial_symtab (name);
      if (!ps)
	return (NULL);

      if (ps->readin)
	error ("Internal: readin %s pst for `%s' found when no symtab found.",
	       ps->filename, name);
            
      s = PSYMTAB_TO_SYMTAB (ps);

      /* Baskar, JAGae57840
         There could be many symtabs for the same psymtab. Simply returning
         the "symtab" might not help, since there is no guarantee that the
         symtab returned by PSYMTAB_TO_SYMTAB is of the specific name.

         For example, psymtab for an object file created by instrumentation
         tools like c-cover, will be expanded to multiple symtabs like...

                (psymtab for test.c)
                +---------+
                | psymtab |-> ...
                +---------+
                    |
                +---------+      +---------+
                | symtab  |->    | symtab  |-> ...
                +---------+      +---------+

        (symtab for              (symtab for test.c
         /tmp/covc/test.c         - actual source file)
         - tmp file created by
         c-cover with its own
         functions)
      */

      /* Just checking if the s->filename has "/tmp/covc", the temp dir used
	 by c-cover to create instrumented source code, then skip that symtab
	 though looks clumsy... 

         "debug_ccover" will be set by "set debug_ccover 1" command
      */

      if (s)
	{
	  if (debug_ccover)
	    {
	      char *c = strstr (s->filename, ccover_tmpdir);
	      if (c)
		continue;
	    }
	  return s;
	}

      /* At this point, we have located the psymtab for this file, but
         the conversion to a symtab has failed.  This usually happens
         when we are looking up an include file.  In this case,
         PSYMTAB_TO_SYMTAB doesn't return a symtab, even though one has
         been created.  So, we need to run through the symtabs again in
         order to find the file.
         XXX - This is a crock, and should be fixed inside of the the
         symbol parsing routines. */
    }

  return NULL;
}

/* Lookup the symbol table of a source file named NAME.  Try a couple
   of variations if the first lookup doesn't work.  */

struct symtab *
lookup_symtab (char *name)
{
  register struct symtab *s;
#if 0
  register char *copy;
#endif

  s = lookup_symtab_1 (name);
  if (s)
    return s;

#if 0
  /* This screws c-exp.y:yylex if there is both a type "tree" and a symtab
     "tree.c".  */

  /* If name not found as specified, see if adding ".c" helps.  */
  /* Why is this?  Is it just a user convenience?  (If so, it's pretty
     questionable in the presence of C++, FORTRAN, etc.).  It's not in
     the GDB manual.  */

  copy = (char *) alloca (strlen (name) + 3);
  strcpy (copy, name);
  strcat (copy, ".c");
  s = lookup_symtab_1 (copy);
  if (s)
    return s;
#endif /* 0 */

  /* We didn't find anything; die.  */
  return 0;
}

/* Lookup the partial symbol table of a source file named NAME.
   *If* there is no '/' in the name, a match after a '/'
   in the psymtab filename will also work.  */

struct partial_symtab *
lookup_partial_symtab (char *name)
{
  register struct partial_symtab *pst;
  register struct objfile *objfile;
  char *basename, *tail, *p;
  char * pathname = 0;
  char * f_pathname = 0;
  char * pst_pathname = 0;
#ifdef GDB_TARGET_IS_HPPA
  /* JAGaf47774 - Retrieve absolute file path from its compilation unit */
  char * pst_dirname = 0;
  char * pst_filename = 0;
  char * pst_real_filename = 0;
  /* JAGaf47774 - ends */
#endif
  int foundit = 0;
  
  ALL_PSYMTABS (objfile, pst)
  {
    /*JAGae68084 - See if the objfilename matches the library name. This is requires
     because the psymtab->filename will be saying global*/
    char * objfl_name=pst->objfile->name;
    if (STREQ (name, pst->filename) || STREQ(name,objfl_name) || STREQ(name, getbasename(objfile->name)))
      {
	return (pst);
      }
  }

  /* Now, search for a matching tail (only if name doesn't have any dirs) */

  if (!strchr (name, '/'))
    ALL_PSYMTABS (objfile, pst)
    {
      p = pst->filename;
      tail = strrchr (p, '/');

      if (tail)
	p = tail + 1;

      if (STREQ (p, name))
	return (pst);
    }

  /* Now, if the basenames of the filename and psymtab's filename match,
     compare the expanded filenames */
  name = tilde_expand (name);
  basename = getbasename (name);
  make_cleanup (free, name);
  if (source_full_path_of (name, &pathname))
    {
      f_pathname = pathopt (pathname);
      free (pathname);
    }
  else
    f_pathname = pathopt (name);
  pst = 0;  /* initialize for compiler warning */
  ALL_PSYMTABS (objfile, pst)
    {
      struct stat f_stat, pst_stat;

      if (! STREQ (basename, getbasename (pst->filename)))
        continue;
      pst_pathname = psymtab_to_filename (pst);
      if  (STREQ (f_pathname, pst_pathname))
        {
          foundit = 1;
          /* srikanth, JAGad13385, a break here doesn't do any good, as
             ALL_PSYMTABS expands into a nested loop.
          */
          goto get_out;
        }
      /* These may still be the same file because of automounts, etc;
	 use stat to determine if they are the same file */ 
      else if (!stat (f_pathname, &f_stat) && !stat (pst_pathname, &pst_stat) &&
          f_stat.st_dev == pst_stat.st_dev && f_stat.st_ino == pst_stat.st_ino)
        {
          foundit = 1;
          goto get_out;
        }
#ifdef GDB_TARGET_IS_HPPA
	else 
        {
	/* JAGaf47774 - Retrieve absolute file path from its compilation unit */
#ifdef GDB_TARGET_IS_HPPA_20W
	   pst_dirname = (char *) elf_set_current_compdir(pst);
#endif

#ifndef GDB_TARGET_IS_HPPA_20W
           pst_dirname = (char *) som_set_current_compdir(pst);
#endif
	   /* JAGaf47774 - Build absolute file path using information retrieved 
	   from its compilation unit and compare to ensure correctness of 
	   psymtab. 
	   JAGad41171 - non-absolute filename path should be appended to 
	   pst_dirname */
	   
	   if (pst_dirname && pst->filename && (*pst->filename != '/'))
	   {
	      pst_filename = xmalloc (strlen (pst_dirname) 
	   				+ strlen (pst->filename) + 2);
	      strcpy(pst_filename, pst_dirname);
	      strcat(pst_filename, "/");
	      strcat(pst_filename, pst->filename);
	      pst_real_filename = pathopt (pst_filename);
	      free (pst_filename);
	      if (STREQ (f_pathname, pst_real_filename))
	      {
	   	foundit = 1;
	   	free (pst_real_filename);
		goto get_out;
	      }
	      free (pst_real_filename);
	   }
	}
#endif
    }
get_out:
  free (f_pathname);
  if (pst_pathname) 
    free (pst_pathname);
  if (foundit)
    return pst;

  /* RM: DOOM on 10.20 causes psymtabs to have filenames without
     extensions. Try these too */
  basename = (char *) alloca (strlen (name) + 1);
  strcpy (basename, name);
  p = strrchr (basename, '.');
  if (p)
    {
      *p = 0;

      pst = 0; /* initialize for compiler warning */
      ALL_PSYMTABS (objfile, pst)
      {
	if (STREQ (basename, pst->filename))
	  {
	    return (pst);
	  }
      }

      /* Now, search for a matching tail (only if name doesn't have any dirs) */
      if (!strchr (basename, '/'))
	ALL_PSYMTABS (objfile, pst)
	{
	  p = pst->filename;
	  tail = strrchr (p, '/');

	  if (tail)
	    p = tail + 1;

	  if (STREQ (p, basename))
	    return (pst);
	}
    }

  /* srikanth, JAGad39445, 001122. If we get here, all attempts to
     locate the file have failed. As a last ditch, try just using
     the base of the pathname encoded in the psymtab and search in
     source path. This is useful in cases, where the source files
     are in one directory, the objects are built in another, they
     are archived in a third directory and finally linked in yet
     another. This is how openview seems to be organized.
  */
  basename = getbasename (name);
  f_pathname = 0;
  ALL_PSYMTABS (objfile, pst)
    {
      char *pst_basename = getbasename (pst->filename);

      if (! STREQ (basename, pst_basename))
        continue;

      if (source_full_path_of (pst_basename, &f_pathname))
        {
          free (f_pathname);
          pst->filename = pst_basename;
          return pst;
        }
    }


  return (NULL);
}

/* Mangle a GDB method stub type.  This actually reassembles the pieces of the
   full method name, which consist of the class name (from T), the unadorned
   method name from METHOD_ID, and the signature for the specific overload,
   specified by SIGNATURE_ID.  Note that this function is g++ specific. */

char *
gdb_mangle_name (struct type *type, int method_id, int signature_id)
{
  int mangled_name_len;
  char *mangled_name;
  struct fn_field *f = TYPE_FN_FIELDLIST1 (type, method_id);
  struct fn_field *method = &f[signature_id];
  char *field_name = TYPE_FN_FIELDLIST_NAME (type, method_id);
  char *physname = TYPE_FN_FIELD_PHYSNAME (f, signature_id);
  char *newname = type_name_no_tag (type);

  /* Does the form of physname indicate that it is the full mangled name
     of a constructor (not just the args)?  */
  int is_full_physname_constructor;

  int is_constructor;

#ifndef HP_IA64
  int is_destructor = DESTRUCTOR_PREFIX_P (physname);
#else
  int is_destructor = DESTRUCTOR_PREFIX_P (physname, type);
#endif

  /* Need a new type prefix.  */
  char *const_prefix = method->is_const ? "C" : "";
  char *volatile_prefix = method->is_volatile ? "V" : "";
  char buf[20];
  int len = (int) (newname == NULL ? 0 : (int) strlen (newname));

  is_full_physname_constructor =
    ((physname[0] == '_' && physname[1] == '_' &&
      (isdigit (physname[2]) || physname[2] == 'Q' || physname[2] == 't'))
     || (strncmp (physname, "__ct", 4) == 0));

  is_constructor =
    is_full_physname_constructor || (newname && STREQ (field_name, newname));

  if (!is_destructor)
    is_destructor = (strncmp (physname, "__dt", 4) == 0);

  if (is_destructor || is_full_physname_constructor)
    {
      mangled_name = (char *) xmalloc (strlen (physname) + 1);
      strcpy (mangled_name, physname);
      return mangled_name;
    }

  if (len == 0)
    {
      sprintf (buf, "__%s%s", const_prefix, volatile_prefix);
    }
  else if (physname[0] == 't' || physname[0] == 'Q')
    {
      /* The physname for template and qualified methods already includes
         the class name.  */
      sprintf (buf, "__%s%s", const_prefix, volatile_prefix);
      newname = NULL;
      len = 0;
    }
  else
    {
      sprintf (buf, "__%s%s%d", const_prefix, volatile_prefix, len);
    }
  mangled_name_len = (int) (((is_constructor ? 0 : strlen (field_name))
		      + strlen (buf) + len
		      + strlen (physname)
		      + 1));

  /* Only needed for GNU-mangled names.  ANSI-mangled names
     work with the normal mechanisms.  */
  if (OPNAME_PREFIX_P (field_name))
    {
      const char *opname = cplus_mangle_opname (field_name + 3, 0);
      if (opname == NULL)
	error ("No mangling for \"%s\"", field_name);
      mangled_name_len += strlen (opname);
      mangled_name = (char *) xmalloc (mangled_name_len);

      strncpy (mangled_name, field_name, 3);
      mangled_name[3] = '\0';
      strcat (mangled_name, opname);
    }
  else
    {
      mangled_name = (char *) xmalloc (mangled_name_len);
      if (is_constructor)
	mangled_name[0] = '\0';
      else
	strcpy (mangled_name, field_name);
    }
  strcat (mangled_name, buf);
  /* If the class doesn't have a name, i.e. newname NULL, then we just
     mangle it using 0 for the length of the class.  Thus it gets mangled
     as something starting with `::' rather than `classname::'. */
  if (newname != NULL)
    strcat (mangled_name, newname);

  strcat (mangled_name, physname);
  return (mangled_name);
}



/* Find which partial symtab on contains PC and SECTION.  Return 0 if none.  */

struct partial_symtab *
find_pc_sect_psymtab (CORE_ADDR pc, asection *section)
{
  register struct partial_symtab *pst = NULL;
  register struct objfile *objfile;
  struct minimal_symbol *msym;

  /*
   * JAGaf91357 - for +nobojdebug (!doom_executable), pst field 
   * of minsym is not set regardless of HASH_ALL or not.
   * Read comments about this wonderful topic in hp-psymtab-read.c
   * and symtab.h.  The fix here is if msym->pst is null then
   * let it look through all PSYMTAB.  Note: pst field is not valid 
   * for PA.  For PA & IA noobjdebug look through all PSYMTABS.
   * IA objdebug benefits from grabbing pst immediately.
   */

#ifdef HP_IA64
  msym = (struct minimal_symbol *) lookup_minimal_symbol_by_pc (pc);
  if (msym && msym->pst)
     return msym->pst;

  ALL_OBJFILES (objfile)
  {
    /* Short-circuit the per-partial-symbol-table tests if the whole
       objfile is out of range.  This test didn't work on PA. */
    if (pc < objfile->text_low || pc >= objfile->text_high)
       continue;

    ALL_OBJFILE_PSYMTABS (objfile, pst)
#else
    ALL_PSYMTABS (objfile, pst)
#endif 
    {
      if (pc >= pst->textlow && pc < pst->texthigh)
        {
	  struct minimal_symbol *msymbol;
	  struct partial_symtab *tpst;

	  /* An objfile that has its functions reordered might have
	     many partial symbol tables containing the PC, but
	     we want the partial symbol table that contains the
	     function containing the PC.  */
	  if (!(objfile->flags & OBJF_REORDERED) &&
	      section == 0)	/* can't validate section this way */
	    return (pst);

	  msymbol = lookup_minimal_symbol_by_pc_section (pc, section);
	  if (msymbol == NULL)
	    return (pst);

	  for (tpst = pst; tpst != NULL; tpst = tpst->next)
	    {
	      if (pc >= tpst->textlow && pc < tpst->texthigh)
	        {
		  struct partial_symbol *p;

		  p = find_pc_sect_psymbol (tpst, pc, section);

#ifdef FAT_FREE_PSYMTABS
  
		/* srikanth, in the case of the Wildebeest, the psymtabs
		   could be empty and hence the call to
		   find_pc_sect_psymbol() could fail to find anything.
		   Due potentially to a combination of bugs in compilers,
		   linkers, loaders, editors and debugger engineers :-),
		   sometimes there is an overlap of textlow and texthigh
		   values between and multiple psymtabs. As a result,
		   if we are handed an address equal to the texthigh or
		   textlow, we could end up identifying the wrong psymtab.

		   To be absolutely sure of the identity of the psymtab,
		   let us verify that the one we narrow down on,
		   also houses pc + 4. */

		if (p == NULL)
		  {
		    CORE_ADDR pc_plus4 = pc + 4;
		    if (pc_plus4 >= tpst->textlow &&
			pc_plus4 <= tpst->texthigh)
		      return tpst;
		  }
#endif 

		  if (p != NULL
		      && SYMBOL_VALUE_ADDRESS (p)
		      == SYMBOL_VALUE_ADDRESS (msymbol))
		    return (tpst);
	        }
	    }
	  return (pst);
        }
#ifdef HP_IA64
    }	/* ALL_OBJFILE_PSYMTABS */
  }	/* ALL_OBJFILES */
#else
    }	/* ALL_PSYMTABS */
#endif

  return (NULL);
}

/* Find which partial symtab contains PC.  Return 0 if none. 
   Backward compatibility, no section */

struct partial_symtab *
find_pc_psymtab (CORE_ADDR pc)
{
  return find_pc_sect_psymtab (pc, find_pc_mapped_section (pc));
}

/* Find which partial symbol within a psymtab matches PC and SECTION.  
   Return 0 if none.  Check all psymtabs if PSYMTAB is 0.  */

struct partial_symbol *
find_pc_sect_psymbol (struct partial_symtab *psymtab, CORE_ADDR pc, asection *section)
{
  struct partial_symbol *best = NULL, *p, **pp;
  CORE_ADDR best_pc;

#ifdef HASH_ALL
  p = (struct partial_symbol *) lookup_minimal_symbol_by_pc (pc);
  return p;
#else

  if (!psymtab)
    psymtab = find_pc_sect_psymtab (pc, section);
  if (!psymtab)
    return 0;

  /* Cope with programs that start at address 0 */
  best_pc = (psymtab->textlow != 0) ? psymtab->textlow - 1 : 0;

  /* Search the global symbols as well as the static symbols, so that
     find_pc_partial_function doesn't use a minimal symbol and thus
     cache a bad endaddr.  */
  for (pp = psymtab->objfile->global_psymbols.list + psymtab->globals_offset;
    (pp - (psymtab->objfile->global_psymbols.list + psymtab->globals_offset)
     < psymtab->n_global_syms);
       pp++)
    {
      p = *pp;
      /* No problemo, fuzzy namespace & LOC_BLOCK don't go together */
      if (PSYMBOL_NAMESPACE (p) == VAR_NAMESPACE
	  && PSYMBOL_CLASS (p) == LOC_BLOCK
	  && pc >= SYMBOL_VALUE_ADDRESS (p)
	  && (SYMBOL_VALUE_ADDRESS (p) > best_pc
	      || (psymtab->textlow == 0
		  && best_pc == 0 && SYMBOL_VALUE_ADDRESS (p) == 0)))
	{
	  if (section)		/* match on a specific section */
	    {
	      fixup_psymbol_section (p, psymtab->objfile);
	      if (SYMBOL_BFD_SECTION (p) != section)
		continue;
	    }
	  best_pc = SYMBOL_VALUE_ADDRESS (p);
	  best = p;
	}
    }

  for (pp = psymtab->objfile->static_psymbols.list + psymtab->statics_offset;
    (pp - (psymtab->objfile->static_psymbols.list + psymtab->statics_offset)
     < psymtab->n_static_syms);
       pp++)
    {
      p = *pp;
      /* No problemo, fuzzy namespace & LOC_BLOCK don't go together */
      if (PSYMBOL_NAMESPACE (p) == VAR_NAMESPACE
	  && PSYMBOL_CLASS (p) == LOC_BLOCK
	  && pc >= SYMBOL_VALUE_ADDRESS (p)
	  && (SYMBOL_VALUE_ADDRESS (p) > best_pc
	      || (psymtab->textlow == 0
		  && best_pc == 0 && SYMBOL_VALUE_ADDRESS (p) == 0)))
	{
	  if (section)		/* match on a specific section */
	    {
	      fixup_psymbol_section (p, psymtab->objfile);
	      if (SYMBOL_BFD_SECTION (p) != section)
		continue;
	    }
	  best_pc = SYMBOL_VALUE_ADDRESS (p);
	  best = p;
	}
    }

  return best;
#endif
}

/* Find which partial symbol within a psymtab matches PC.  Return 0 if none.  
   Check all psymtabs if PSYMTAB is 0.  Backwards compatibility, no section. */

struct partial_symbol *
find_pc_psymbol (struct partial_symtab *psymtab, CORE_ADDR pc)
{
  return find_pc_sect_psymbol (psymtab, pc, find_pc_mapped_section (pc));
}

/* Debug symbols usually don't have section information.  We need to dig that
   out of the minimal symbols and stash that in the debug symbol.  */

static void
fixup_section (struct general_symbol_info *ginfo, struct objfile *objfile)
{
  struct minimal_symbol *msym;
  msym = lookup_minimal_symbol (ginfo->name, NULL, objfile);

  if (msym)
    ginfo->bfd_section = SYMBOL_BFD_SECTION (msym);
}

struct symbol *
fixup_symbol_section (struct symbol *sym, struct objfile *objfile)
{
  if (!sym)
    return NULL;

  if (SYMBOL_BFD_SECTION (sym))
    return sym;

  fixup_section (&sym->ginfo, objfile);

  return sym;
}

#ifndef HASH_ALL
static struct partial_symbol *
fixup_psymbol_section (struct partial_symbol *psym, struct objfile *objfile)
{
  if (!psym)
    return NULL;

  if (SYMBOL_BFD_SECTION (psym))
    return psym;

  fixup_section (&psym->ginfo, objfile);

  return psym;
}
#endif	/* !HASH_ALL */

/* Lookup the given name in the symbol table for a non-unresolved symbol. 
 * We use this function to get the resolved symbol associated with an 
 * unresolved symbol. Check in only GLOBAL_BLOCK. Also we are looking only 
 * for VAR_NAMESPACE variables as these are the only ones which can be 
 * unresolved. 
 */
struct symbol *
lookup_symbol_resolved (const char *name)
{
  register struct symbol *sym = NULL;
  register struct symtab *s = NULL;
  register struct partial_symtab *ps = NULL;
  struct blockvector *bv;
  register struct block *block;
  register struct objfile *objfile = NULL;
  char *copy;
  int copy_name_len, i;
  int sym_type = -1; /* init to -1 and it is set by retrieve_from_table */
  struct minimal_symbol *msym = NULL, *minsym = NULL;

  /* Make a copy of the given name in the lowercase if the case_sensitivity
     is not on. */
  if (case_sensitivity == case_sensitive_on)
    copy = (char*) name;  
  else
    {
      copy_name_len = (int) strlen (name);
      copy = (char *) alloca (copy_name_len + 1);
      for (i = 0; i < copy_name_len; i++)
	copy[i] = tolower (name[i]);
      copy[i] = 0;
    }
  
#ifdef HASH_ALL
  /* For the perf gdb we call retrieve_from_table for this symbol and 
   * ask for a symbol that is not a declaration. The pst is attached to this
   * symbol so we expand that and  return the symbol found in the 
   * corresponding symtab. 
   */

  ALL_OBJFILES (objfile)
  {
    current_objfile = objfile;
    minsym = (struct minimal_symbol *) retrieve_from_table 
      (*objfile->hash_table, copy, 0, mst_unknown, &sym_type, 0, 
       VAR_NAMESPACE, NULL);
    
    if (minsym->pst) 
      {
	ps = minsym->pst;
	s = PSYMTAB_TO_SYMTAB (ps);
	bv = BLOCKVECTOR (s);
	block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	sym = lookup_block_symbol (block, copy, VAR_NAMESPACE);
	if (sym && SYMBOL_CLASS (sym) == LOC_UNRESOLVED)
	  sym = NULL;
	if (sym)
	  return fixup_symbol_section (sym, objfile);
      }
  }
#endif

  /* Check in the symtabs first. */
  ALL_SYMTABS (objfile, s)
  {
    bv = BLOCKVECTOR (s);
    block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
    sym = lookup_block_symbol (block, copy, VAR_NAMESPACE);
    if (sym && SYMBOL_CLASS (sym) == LOC_UNRESOLVED)
      sym = NULL;
    if (sym)
      {
	return fixup_symbol_section (sym, objfile);
      }
  }

  /* Now check in the global symbols of the psymtabs. If we find any 
     expand that symtab and get the symbol. */
  ALL_PSYMTABS (objfile, ps)
  {
    if (!ps->readin && lookup_partial_symbol (ps, copy, 1, VAR_NAMESPACE))
      {
	s = PSYMTAB_TO_SYMTAB (ps);
	bv = BLOCKVECTOR (s);
	block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	sym = lookup_block_symbol (block, copy, VAR_NAMESPACE);
        if (sym && SYMBOL_CLASS (sym) == LOC_UNRESOLVED)
          sym = NULL;
	if (sym)
          return fixup_symbol_section (sym, objfile);
      }
  }
  return NULL;
} /* lookup_symbol_unresolved */


/* Find the definition for a specified symbol name NAME
   in namespace NAMESPACE, visible from lexical block BLOCK.
   Returns the struct symbol pointer, or zero if no symbol is found.
   If SYMTAB is non-NULL, store the symbol table in which the
   symbol was found there, or NULL if not found.
   C++: if IS_A_FIELD_OF_THIS is nonzero on entry, check to see if
   NAME is a field of the current implied argument `this'.  If so set
   *IS_A_FIELD_OF_THIS to 1, otherwise set it to zero.
   BLOCK_FOUND is set to the block in which NAME is found (in the case of
   a field of `this', value_of_this sets BLOCK_FOUND to the proper value.) */

/* This function has a bunch of loops in it and it would seem to be
   attractive to put in some QUIT's (though I'm not really sure
   whether it can run long enough to be really important).  But there
   are a few calls for which it would appear to be bad news to quit
   out of here: find_proc_desc in alpha-tdep.c and mips-tdep.c, and
   nindy_frame_chain_valid in nindy-tdep.c.  (Note that there is C++
   code below which can error(), but that probably doesn't affect
   these calls since they are looking for a known variable and thus
   can probably assume it will never hit the C++ code).  */


static struct symbol *
lookup_symbol_1 (const char *name, register const struct block *block,
		 const namespace_enum namespace, int *is_a_field_of_this,
		 struct symtab **symtab, int *local)
{
  register struct symbol *sym;
  register struct symtab *s = NULL;
  register struct partial_symtab *ps;
  struct blockvector *bv;
  register struct objfile *objfile = NULL, *objfile2 = NULL;
  register struct block *b;
  register struct minimal_symbol *msymbol;

  int do_internal_error_at_end = 0;
  char *internal_error_str1 = NULL;
  char *internal_error_str2 = NULL;

  extern enum case_sensitivity_type case_sensitivity;
  char *copy;
  int copy_name_len, i;
  int search_this_members = TRUE;
  
  if (local)
    *local = 0;
  if (case_sensitivity == case_sensitive_on)
    copy = (char *)name;
  else
    {
      copy_name_len = (int) strlen (name);
      copy = (char *) alloca (copy_name_len + 1);
      for (i= 0; i < copy_name_len; i++)
        copy[i] = tolower (name[i]);
      copy[i] = 0;
    }

  /* Search specified block and its superiors.  */

  while (block != 0)
    {
      sym = lookup_block_symbol (block, copy, namespace);

      /* C++: If requested to do so by the caller, check to see if NAME is a
         field of `this'. */
      /* QXCR1000880103: Can not print value of member variable while in member
         function of that class. After searching for matching local variable in
         first block, following 'if' construct searches for matching member 
         element of 'this'. And then further superblocks are searched one-by-one.
      */
      if (!sym && is_a_field_of_this && search_this_members)
        {
          struct value *v = value_of_this (0);
          *is_a_field_of_this = 0;
          if (v && check_field (v, copy))
            {
              *is_a_field_of_this = 1;
              if (symtab != NULL)
                *symtab = NULL;
              return NULL;
            }
        }
      search_this_members = FALSE;

      if (sym && !SYMBOL_OBSOLETED (sym))
	{
	  /* The symbol has been found in a local block if 
	     the block is a function /namespace block or
	     it has atleast 2 superblocks. 
	     In the blockvector, the zeroth block is the 
	     global block and the first block is the static 
	     block. All function blocks start after that.
	     A local variable will be in a function/lexical
	     context block. 

	     If the block isn't a function/namespace block
	     it could either be a lexical context block, the
	     static block or the global block. 
	     The static block has 1 superblock and the global
	     block has 0 superblocks. 
	     
	     Checking that a block has atleast 2 superblocks
	     guarantees that the block is a lexical context
	     block and not the static/global block.
	     
	     N.B Global using directives will not have symbols
	     so the namespace block that you have found the 
	     symbol in is a local block. 
	   */

	  if (local && (BLOCK_FUNCTION (block) || BLOCK_NAMESPACE (block) ||
	      (BLOCK_SUPERBLOCK (block) && 
	       BLOCK_SUPERBLOCK (BLOCK_SUPERBLOCK (block)))))
	    *local = 1;
	  
	  block_found = block;
	  if (symtab != NULL)
	    {
	      /* Search the list of symtabs for one which contains the
	         address of the start of this block.  */
	      ALL_SYMTABS (objfile, s)
	      {
		bv = BLOCKVECTOR (s);
		b = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
		if (BLOCK_START (b) <= BLOCK_START (block)
		    && BLOCK_END (b) > BLOCK_START (block))
		  {
		    if (local && (block != b) && 
			(block != BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK)))
		      *local = 1;
		    goto found;
		  }
	      }
	    found:
	      *symtab = s;
	    }
	  
	  return fixup_symbol_section (sym, objfile);
	}
      block = BLOCK_SUPERBLOCK (block);
    }

  /* FIXME: this code is never executed--block is always NULL at this
     point.  What is it trying to do, anyway?  We already should have
     checked the STATIC_BLOCK above (it is the superblock of top-level
     blocks).  Why is VAR_NAMESPACE special-cased?  */
  /* Don't need to mess with the psymtabs; if we have a block,
     that file is read in.  If we don't, then we deal later with
     all the psymtab stuff that needs checking.  */
  /* Note (RT): The following never-executed code looks unnecessary to me also.
   * If we change the code to use the original (passed-in)
   * value of 'block', we could cause it to execute, but then what
   * would it do? The STATIC_BLOCK of the symtab containing the passed-in
   * 'block' was already searched by the above code. And the STATIC_BLOCK's
   * of *other* symtabs (those files not containing 'block' lexically)
   * should not contain 'block' address-wise. So we wouldn't expect this
   * code to find any 'sym''s that were not found above. I vote for 
   * deleting the following paragraph of code.
   */
  if (namespace == VAR_NAMESPACE && block != NULL)
    {
      struct block *b;
      /* Find the right symtab.  */
      ALL_SYMTABS (objfile, s)
      {
	bv = BLOCKVECTOR (s);
	b = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
	if (BLOCK_START (b) <= BLOCK_START (block)
	    && BLOCK_END (b) > BLOCK_START (block))
	  {
	    sym = lookup_block_symbol (b, copy, VAR_NAMESPACE);
	    if (sym && !SYMBOL_OBSOLETED (sym))
	      {
		block_found = b;
		if (symtab != NULL)
		  *symtab = s;
		return fixup_symbol_section (sym, objfile);
	      }
	  }
      }
    }

#ifndef HASH_ALL
  /* Now search all global blocks.  Do the symtab's first, then
     check the psymtab's. If a psymtab indicates the existence
     of the desired name as a global, then do psymtab-to-symtab
     conversion on the fly and return the found symbol. */

  ALL_SYMTABS (objfile, s)
  {
    bv = BLOCKVECTOR (s);
    block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
    sym = lookup_block_symbol (block, copy, namespace);
    if (sym && !SYMBOL_OBSOLETED (sym))
      {
	block_found = block;
	if (symtab != NULL)
	  *symtab = s;
	return fixup_symbol_section (sym, objfile);
      }
  }

#if !defined(GDB_TARGET_IS_HPUX)

  /* Check for the possibility of the symbol being a function or
     a mangled variable that is stored in one of the minimal symbol tables.
     Eventually, all global symbols might be resolved in this way.  */

  if (namespace == VAR_NAMESPACE)
    {
      msymbol = lookup_minimal_symbol (copy, NULL, NULL);

      if (msymbol != NULL)
	{
	  s = find_pc_sect_symtab (SYMBOL_VALUE_ADDRESS (msymbol),
				   SYMBOL_BFD_SECTION (msymbol));
	  if (s != NULL)
	    {
	      /* This is a function which has a symtab for its address.  */
	      bv = BLOCKVECTOR (s);
	      block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	      sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
					 namespace);
	      /* We kept static functions in minimal symbol table as well as
	         in static scope. We want to find them in the symbol table. */
	      if (!sym)
		{
		  block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
		  sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
					     namespace);
		}

	      /* sym == 0 if symbol was found in the minimal symbol table
	         but not in the symtab.
	         Return 0 to use the msymbol definition of "foo_".

	         This happens for Fortran  "foo_" symbols,
	         which are "foo" in the symtab.

	         This can also happen if "asm" is used to make a
	         regular symbol but not a debugging symbol, e.g.
	         asm(".globl _main");
	         asm("_main:");
	       */

	      if (symtab != NULL)
		*symtab = s;
	      return fixup_symbol_section (sym, objfile);
	    }
	  else if (MSYMBOL_TYPE (msymbol) != mst_text
		   && MSYMBOL_TYPE (msymbol) != mst_file_text
		   && !STREQ (copy, SYMBOL_NAME (msymbol)))
	    {
	      /* This is a mangled variable, look it up by its
	         mangled name.  */
	      return lookup_symbol_1 (SYMBOL_NAME (msymbol), block,
				      namespace, is_a_field_of_this, symtab,
				      local);
	    }
	  /* There are no debug symbols for this file, or we are looking
	     for an unmangled variable.
	     Try to find a matching static symbol below. */
	}
    }

#endif

  ALL_PSYMTABS (objfile, ps)
  {
      if (!ps->readin && lookup_partial_symbol (ps, copy, 1, namespace))
        {
          s = PSYMTAB_TO_SYMTAB(ps);
	  /* Madhavi, Need to short-circuit this if psymtab_to_symtab 
           * returns NULL. */
          if (!s)
            continue;
          bv = BLOCKVECTOR (s);
          block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
          sym = lookup_block_symbol (block, copy, namespace);
          if (!sym || SYMBOL_OBSOLETED(sym))
            {
              /* This shouldn't be necessary, but as a last resort
               * try looking in the statics even though the psymtab
               * claimed the symbol was global. It's possible that
               * the psymtab gets it wrong in some cases.
               */
              block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
              sym = lookup_block_symbol (block, copy, namespace);
              if (!sym || SYMBOL_OBSOLETED(sym))
                if (ps->symtab == NULL)
		  {
		    /* CM: This means that the symtab could not be created for
		       some reason.  Instead of dying, just continue with the
		       search */
		    continue;
		  }
		else
		  {
		    /* CM: Look through all of the symtabs again just in case
		       expanding out this psymtab introduced additional entries
		       in other symtabs. This can happen in HP's version of
		       template instantiation (CTTI). */
		    ALL_SYMTABS (objfile2, s)
		      {
			bv = BLOCKVECTOR (s);
			block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
			sym = lookup_block_symbol (block, copy, namespace);
			if (sym && !SYMBOL_OBSOLETED(sym))
                          {
			    block_found = block;
			    if (symtab != NULL)
			      *symtab = s;
			    return sym;
			  }
		      }
		    /* CM: Check other psymtabs just in case this is defined
		       elsewhere. Give an error at the end of this function if
		       a match was not found. */
		    do_internal_error_at_end = 1;
		    internal_error_str1 = "global";
		    internal_error_str2 = ps->filename;
		    continue;
		  }
            }
          if (symtab != NULL)
            *symtab = s;
          return sym;
        }
    }

  /* Now search the static file-level symbols.
     Not strictly correct, but more useful than an error.
     Do the symtab's first, then
     check the psymtab's. If a psymtab indicates the existence
     of the desired name as a file-level static, then do psymtab-to-symtab
     conversion on the fly and return the found symbol.
     */

  ALL_SYMTABS (objfile, s)
    {
      bv = BLOCKVECTOR (s);
      block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
      sym = lookup_block_symbol (block, copy, namespace);
      if (sym && !SYMBOL_OBSOLETED (sym))
        {
          block_found = block;
          if (symtab != NULL)
            *symtab = s;
          return sym;
        }
    }

  ALL_PSYMTABS (objfile, ps)
    {
      if (!ps->readin && lookup_partial_symbol (ps, copy, 0, namespace))
        {
          s = PSYMTAB_TO_SYMTAB (ps);
          bv = BLOCKVECTOR (s);
          block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
          sym = lookup_block_symbol (block, copy, namespace);
	  if (!sym || SYMBOL_OBSOLETED(sym))
            {
              /* This shouldn't be necessary, but as a last resort
               * try looking in the globals even though the psymtab
               * claimed the symbol was static. It's possible that
               * the psymtab gets it wrong in some cases.
               */
              block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
              sym = lookup_block_symbol (block, copy, namespace);
	      if (!sym || SYMBOL_OBSOLETED(sym))
                if (ps->symtab == NULL)
		  {
		    /* CM: This means that the symtab could not be created for
		       some reason.  Instead of dying, just continue with the
		       search */
		    continue;
		  }
		else
		  {
		    /* CM: Look through all of the symtabs again just in case
		       expanding out this psymtab introduced additional entries
		       in other symtabs. This can happen in HP's version of
		       template instantiation (CTTI). */
		    ALL_SYMTABS (objfile2, s)
		      {
			bv = BLOCKVECTOR (s);
			block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
			sym = lookup_block_symbol (block, copy, namespace);
			if (sym) 
                          {
			    block_found = block;
			    if (symtab != NULL)
			      *symtab = s;
			    return sym;
			  }
		      }
		    /* CM: Check other psymtabs just in case this is defined
		       elsewhere. Give an error at the end of this function if
		       a match was not found. */
		    do_internal_error_at_end = 1;
		    internal_error_str1 = "static";
		    internal_error_str2 = ps->filename;
		    continue;
		  }
	    }
	  if (symtab != NULL)
	    *symtab = s;
	  return fixup_symbol_section (sym, objfile);
	}
    }
#endif

#if defined(GDB_TARGET_IS_HPUX)

  /* Check for the possibility of the symbol being a function or
     a global variable that is stored in one of the minimal symbol tables.
     The "minimal symbol table" is built from linker-supplied info.

     RT: I moved this check to last, after the complete search of
     the global (p)symtab's and static (p)symtab's. For HP-generated
     symbol tables, this check was causing a premature exit from
     lookup_symbol with NULL return, and thus messing up symbol lookups
     of things like "c::f". It seems to me a check of the minimal
     symbol table ought to be a last resort in any case. I'm vaguely
     worried about the comment below which talks about FORTRAN routines "foo_"
     though... is it saying we need to do the "minsym" check before
     the static check in this case? 
   */


#ifndef HASH_ALL
  if (namespace == VAR_NAMESPACE)
#endif
    
    {
#ifdef HASH_ALL
      msymbol = lookup_minimal_symbol_1 (copy, NULL, NULL, namespace);
#else
      msymbol = lookup_minimal_symbol (copy, NULL, NULL);
#endif
      if (msymbol != NULL && !MSYMBOL_OBSOLETED (msymbol))
	{
	  /* OK, we found a minimal symbol in spite of not
	   * finding any symbol. There are various possible
	   * explanations for this. One possibility is the symbol
	   * exists in code not compiled -g. Another possibility
	   * is that the 'psymtab' isn't doing its job.
	   * A third possibility, related to #2, is that we were confused 
	   * by name-mangling. For instance, maybe the psymtab isn't
	   * doing its job because it only know about demangled
	   * names, but we were given a mangled name...
	   */

	  /* We first use the address in the msymbol to try to
	   * locate the appropriate symtab. Note that find_pc_symtab()
	   * has a side-effect of doing psymtab-to-symtab expansion,
	   * for the found symtab.
	   */

#ifdef HASH_ALL
	  s = find_symtab (msymbol);
#else
	  s = find_pc_symtab (SYMBOL_VALUE_ADDRESS (msymbol));
#endif
	  if (s != NULL)
	    {
	      bv = BLOCKVECTOR (s);
	      block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	      sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
					 namespace);
	      /* We kept static functions in minimal symbol table as well as
	         in static scope. We want to find them in the symbol table. */
	      if (!sym)
		{
		  block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
		  sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
					     namespace);
		}

              /* 
               * JAGaf66675 - for STRUCT_NAMESPACE don't stop at 
               * global and static blocks, keep going.  For other
               * namespaces such as VAR_NAMESPACE don't enter here - 
               * it would find variable from an incorrect scope.
               */
              if (!sym && namespace == STRUCT_NAMESPACE)
                {
                  int num_blocks = BLOCKVECTOR_NBLOCKS (bv);

                  /* 
                   * GLOBAL_BLOCK is block 0.  STATIC_BLOCK is block 1.
                   * Above searched through GLOBAL and STATIC blocks 
                   * already so start at 2.
                   */
                  for (i = 2; i < num_blocks; i++)
                    {
                      block = BLOCKVECTOR_BLOCK (bv, i);
	  	      sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
 			  	         	 namespace);
                      if (sym)
                        break; 

                    }  /* for all blocks starting after STATIC_BLOCK */
                }  /* if !sym */ 


	      /* If we found one, return it */
	      if (sym)
		{
		  if (symtab != NULL)
		    *symtab = s;
		  return sym;
		}

	      /* If we get here with sym == 0, the symbol was 
	         found in the minimal symbol table
	         but not in the symtab.
	         Fall through and return 0 to use the msymbol 
	         definition of "foo_".
	         (Note that outer code generally follows up a call
	         to this routine with a call to lookup_minimal_symbol(),
	         so a 0 return means we'll just flow into that other routine).

	         This happens for Fortran  "foo_" symbols,
	         which are "foo" in the symtab.

	         This can also happen if "asm" is used to make a
	         regular symbol but not a debugging symbol, e.g.
	         asm(".globl _main");
	         asm("_main:");
	       */
	    }

	  /* If the lookup-by-address fails, try repeating the
	   * entire lookup process with the symbol name from
	   * the msymbol (if different from the original symbol name).
	   */
	  else if (MSYMBOL_TYPE (msymbol) != mst_text
		   && MSYMBOL_TYPE (msymbol) != mst_file_text
		   && !STREQ (copy, SYMBOL_NAME (msymbol)))
	    {
	      return lookup_symbol_1 (SYMBOL_NAME (msymbol), block,
				      namespace, is_a_field_of_this, symtab, 
				      local);
	    }
	}
    }

#endif

  if (do_internal_error_at_end)
    {
      error ("Internal: %s symbol `%s' found in %s psymtab but not in symtab.\n%s may be an inlined function, or may be a template function\n(if a template, try specifying an instantiation: %s<type>).", internal_error_str1, name, internal_error_str2, name, name);
    }

  if (symtab != NULL)
    *symtab = NULL;
  return 0;
}

/* srikanth, 001013, This function use to live in m3-nat.c. I have
   moved it to here as this is a platform neutral function.
*/
/* Returns the address of variable NAME or 0 if not found */
CORE_ADDR
lookup_address_of_variable (char * name)
{
  struct symbol *sym = NULL;
  CORE_ADDR symaddr = 0;
  struct minimal_symbol *msymbol = NULL;

  sym = lookup_symbol (name,
                       (struct block *) NULL,
                       VAR_NAMESPACE,
                       (int *) NULL,
                       (struct symtab **) NULL);

  if (sym)
    {
#ifdef HP_IA64
      if (SYMBOL_CLASS (sym) == LOC_THREAD_LOCAL_STATIC ||
          SYMBOL_CLASS (sym) == LOC_THREAD_LOCAL_DYNAMIC ||
          SYMBOL_CLASS (sym) == LOC_THREAD_LOCAL_STATIC_DPREL ||
          SYMBOL_CLASS (sym) == LOC_THREAD_LOCAL_DYNAMIC_DPREL)
        {
          value_ptr val = locate_var_value2(sym, NULL, 0);
          symaddr = SWIZZLE (value_as_long (val));
        }
      else
#endif /* HP_IA64 */
        {
          symaddr = SYMBOL_VALUE (sym);
        }
    }

  if (!symaddr)
    {
      struct objfile *objfile = NULL;
      ALL_OBJFILES (objfile)
        {
          msymbol = lookup_minimal_symbol (name, NULL, objfile);
          if (msymbol)
            break;
        }

      /* added mst_bss to be able to look up malloc_debug.
       * libc export a symbol that libc needs to be able 
       * to use our callback lookup.  This symbol is in the 
       * bss section
       */
      if (msymbol)
        {
          if ((msymbol->type == mst_data) ||
              (msymbol->type == mst_bss)  ||
              (msymbol->type == mst_file_data &&
               (strstr(name, "__gdb_java_unwindlib"))))
            {
              symaddr = SYMBOL_VALUE_ADDRESS (msymbol);
            }
          else if (msymbol->type == mst_tls || msymbol->type == mst_file_tls)
            {
#ifdef HP_IA64
              CORE_ADDR tls_addr;
              struct minimal_symbol *thrsp_sym;
              off_t sym_tls_offset;
              char buf[MAX_REGISTER_RAW_SIZE];
              CORE_ADDR tp;
              
              get_saved_register (buf, NULL, NULL, selected_frame, 
                                  TP_REGNUM, NULL);
              tp = extract_address (buf, REGISTER_RAW_SIZE (TP_REGNUM));

              thrsp_sym =  lookup_minimal_symbol ("__thread_specific_seg",
                                                  NULL, objfile);
              sym_tls_offset = SYMBOL_VALUE_ADDRESS (msymbol) - 
                SYMBOL_VALUE_ADDRESS (thrsp_sym);

              tls_addr = ((struct load_module_desc*) ((obj_private_data_t *)
                objfile->obj_private)->lmdp)->tls_start_addr;
              
              if (tls_addr != (CORE_ADDR) (long) -1)
                {
                  /* Static TLS */
                  symaddr = tp + tls_addr + sym_tls_offset;
                }
              else
                {
                  /* Dynamic TLS */
                  int tls_index = 
                    (int)((struct load_module_desc*) ((obj_private_data_t *)
		    objfile->obj_private)->lmdp)->tls_index;
                  if (tls_index < 0)
                    {
                      symaddr = tp + tls_index * sizeof(CORE_ADDR);
                    }
                  else
                    {
                      symaddr = read_memory_unsigned_integer (symaddr,
                                TARGET_PTR_BIT / TARGET_CHAR_BIT);
                      symaddr = symaddr + tls_index * sizeof(CORE_ADDR);
                    }
                  symaddr = read_memory_unsigned_integer (symaddr,
                                TARGET_PTR_BIT / TARGET_CHAR_BIT);
                  symaddr += sym_tls_offset;
                }
#else /* PA will not work for now */
              symaddr = SYMBOL_VALUE_ADDRESS (msymbol);
#endif /* HP_IA64 */
            }
        }
    }
#ifdef SWIZZLE
  /* libmxndbg requires that this address be swizzled always. */
  symaddr = SWIZZLE (symaddr);
#endif

  return symaddr;
}


/* Poorva: For namespace support - We want to find the matching left 
   pattern given a right pattern. 
   e.g for the string "foo ()" - first is a pointer to the beginning 
   of foo () and rit - reverse iterator points to the last character 
   of the string i.e the right paren. right is the character ')' and
   left is the character '('. This function is useful when you want 
   to eliminate template <> or the argument list of a function etc.
   We iterate over the string from the back and find a matching left
   for the right character. It works for nested parens, nested template
   braces etc.
*/

char *
go_to_matching( char *rit, char left, char right, char *first )
{
  int nested_par = 0;
  int reached = 0;
  while( !reached && rit > first )
    {
      if ( *rit == left )
        {
          if ( nested_par == 1 )
              reached = 1;
          else
              --nested_par;
        }
      else if ( *rit == right )
        ++nested_par;
      --rit;
    }
  return rit;
}

  /* If a user has said print A::D::a look in the namespace A for
     its using directives.
     namespace A { 
         namespace D {using namespace B;};
	 };
     namespace B { using namespace C;}
     namespace C { using namespace A::D;}
     ...
     We will look in namespace A::D, we find B and add B::a to our list. 
     Then we will look in B and add C::a to our list and so on.
     Transitive closure is mantained hence A::a does not get added again.
     We do not need to start looking in namespace A for its using 
     directives only for those in namespace A::D.
     
     The new_names are added to the global array new_names
     */


static void
add_new_names (char *name1, const struct block *bl)
{
  char *ptr = NULL, *name_no_paren = NULL, *paren_ptr = NULL,
    *doub_colon = NULL;
  int i = 0, rit_len = 0, j = 0, k = 0;

  /* We first remove any parenthesis around the symbol to find the
     namespace name. e.g the name may be A::foo (int)
     */

  name_no_paren = paren_ptr = name1;
  rit_len = (int) strlen (name1);
  if (name1[rit_len - 1] == ')')
    {
      char *rit = name1 + rit_len;
      paren_ptr = go_to_matching (rit, '(', ')', paren_ptr);
      if (paren_ptr > name1)
	{
	  name_no_paren= strdup (name1);
	  name_no_paren[paren_ptr - name1] = '\0';
	}
    }

  /* Do the below only if it isn't a templated class. A namespace
     can't be nested inside a class so we make sure that the longest
     prefix is a namespace */

  if (name_no_paren[strlen (name_no_paren) - 1] != '>')
    {
      /* doub_colon points to the string after the double colon */
      doub_colon = strrstr (name_no_paren, "::");
      if (doub_colon)
	{
	  char *ns_name = strdup (name_no_paren);
	  struct symbol *ns_sym;
	  int local2 = 0;
	  struct type *ns_type;      
	  int is_not_ns = 0;
	  int old_num_new_names = num_new_names;
	  int i = 0, len = 0;
	 
	  ns_name[doub_colon - name_no_paren] = '\0';
	  len = (int) strlen (doub_colon);
	  
	  new_names[num_new_names++] = ns_name;
	  is_not_ns = check_ns (new_names[old_num_new_names], bl);
	  if (is_not_ns)
	    {
	      new_names[old_num_new_names] = NULL;
	      free (ns_name);
	      num_new_names = old_num_new_names;
	    }
	  else
	    {
	      for (i = old_num_new_names; i < num_new_names; ++i)
		{
		  /* check_ns checks a namespace for its using directives
		     and adds the new namespace name that we need to look for to
		     the new_names list */
		  /* Caution - num_new_names changes in this loop - check_ns
		     changes it */
		  check_ns (new_names[i], bl);
		}
	      
	      /* We then create a list of symbols to look for by concatenating
		 the end of symbol name to all the using directives */
	      
	      for (i = old_num_new_names; i < num_new_names; ++i)
		{
		  char *n1 = new_names[i];
		  int n1_len = (int) strlen (n1);
		  new_names[i] =  (char *) xmalloc (n1_len + len + 1);
		  sprintf (new_names[i], "%s%s", n1, doub_colon);
		  free (n1);
		}
	    }
	}
    }
}

/* Compute the transitive closure of namespaces to search for each scope
   that we traverse. We keep looking outwards till we reach global scope
   (had there been a local i we wouldn't have been looking at using 
   directives).

   (i) Get the namespace name from the using directive.
   (ii) Check list of aliases - and find the true name of the
        namespace if this is an alias e.g if B is C::D add C::D to the list.
   (iii) Add true name to list of namespaces if not already present.
   (iv) For every namespace that you have added check the usings inside
        that namespace and add the true names of those namespaces too
        to the list.
   (v) If we are in a function in a namespace (add all prefixes of this
       namespace to the list of namespaces to be searched) e.g if you are
       in A::B::foo add A::B and A to the list.(Check aliases as you add)
   
 */
/* local is a NULL pointer when we call find_using_directives from 
   decode_line_1_reuse since we do not want it to exit if it
   finds a local but instead to complete building a list of the
   using directives - Do not change where it seems like it is 
   checking for local for no good reason.*/

static struct symbol *
find_using_directives (const char **name, const struct block *block, const namespace_enum namespace,
		       int *is_a_field_of_this, 
		       struct symtab **symtab, int *local, struct symbol *sym)
{
  extern struct block* expression_context_block;
  struct block* context_block; /* JAGaf45626 */
  char *actual_name;
  const char *real_name = (char *) *name;
  const struct block *block1 = block;
  const struct block *bl = NULL;
  int sym_alloc = INIT_SYM_ALLOC;
  int i = 0, rit_len = 0, j = 0, k = 0;
  char *ptr = NULL, *name_no_paren = NULL, *paren_ptr = NULL, 
    *doub_colon = NULL;
  
  num_ns_symbols = 0;
  num_symbol_names = 0;
  num_new_names = 0;
  symbol_names = NULL;
  using_syms = NULL;
  ns_symbols = NULL;  
  using_alloc = INIT_USING_ALLOC;
  names_alloc = INIT_USING_ALLOC;

  /* If you have a mangled name - don't do the namespace stuff below since
     you know exactly what you are looking for.*/
  if (   is_cplus_name ((const char *) *name, 1) /* JAGaf49121 */
      && current_language->la_language == language_cplus)
    return sym;

  symbol_names = (char **) xmalloc (using_alloc * sizeof (char *));
  using_syms = (struct symbol **) xmalloc (using_alloc * 
					  sizeof (struct symbol *));
  num_using_syms = using_alloc;
  ns_symbols = (struct symbol **) xmalloc (sym_alloc * 
					  sizeof (struct symbol *));
  new_names =  (char **) xmalloc (names_alloc * sizeof (char *));

  if (sym)
    ns_symbols[num_ns_symbols++] = sym;

  /* Looking for using directives and hence constructing additional
     symbols to look for only makes sense if you are stopped in
     a file somewhere and hence have a current block to start with.
     since using directives are valid only for that file. 
     So make sure you have a block - if not then return the symbol
     that the regular lookup found.
     */
  /* JAGaf45626 - Update expressions's context block only when the 
     current context is not null. Otherwise, gdb will lose the 
     expression context block and hence, fail to parse the rest of 
     the expression */ 

  context_block = get_selected_block ();
  if (context_block)
    expression_context_block = context_block;

  bl = block ? block : context_block;
  if (!bl)
    return sym;
  
  /* The first thing to do is to see if the current symbol is
     actually an alias for another.
     e.g if the user says print foo::a where foo is an
     alias for namespace A::B. The actual name of the symbol
     is A::B::a and so do a regular lookup for A::B::a. If you find 
     that it is a local (due to a using declaration) then call 
     choose_symbol to return.
     */

  actual_name = (char *) check_alias (*name, bl, NULL);
  if (local && strcmp (real_name, actual_name))
    {
      sym = lookup_symbol_1 (actual_name, bl, namespace, NULL, NULL, local);
      if (sym)
	{
	  ns_symbols[num_ns_symbols++] = sym;
	  if (*local)
	    return choose_symbol(*name, sym);
	}
    }
  
  /* If you didn't find the symbol in the block passed in from outside
     then look in the current block that you are stopped in.
   */

  if (local && !sym && block)
    {
      sym = lookup_symbol_1 (actual_name, context_block, namespace, 
			     NULL, NULL, local);
      if (sym)
	{
	  ns_symbols[num_ns_symbols++] = sym;
	  if (*local)
	    return choose_symbol(*name, sym);
	}
    }
  
  /* If we are in a function in a namespace (add all prefixes of this
     namespace to the list of namespaces to be searched) e.g if you are
     in A::B::foo add A::B and A to the list.
     The string may also look like A::B::foo<> - ignore the template
     braces.
     */
  
  block1 = block ? block : context_block;
  if (block1 && BLOCK_FUNCTION(block1))
    {
      char *func_name = BLOCK_FUNCTION(block1)->ginfo.name;
      char *block_name = func_name;
      /* If this is mangled demangle it first without the argument list*/
      if (is_cplus_name (func_name, 1)) /* JAGaf49121 */
	block_name = cplus_demangled_name;

      if (block_name)
	{
	  char *ns_name, *end_ptr, *fore_ptr;
	  ns_name = (char *) alloca (strlen (block_name) + 1);
	  fore_ptr = strdup (block_name);
	  end_ptr = fore_ptr + strlen (fore_ptr) - 1;
	  while (end_ptr > fore_ptr)
	    {
	      if (*end_ptr == '>')
		{
	         /* This should be the other way with first parameter as end_ptr. */
		 /* end_ptr = go_to_matching (fore_ptr, '<', '>', end_ptr);*/
                  end_ptr = go_to_matching (end_ptr, '<', '>', fore_ptr);
		  fore_ptr[end_ptr - fore_ptr] = '\0';
		}
	      end_ptr = strrstr (fore_ptr, "::");
	      if (end_ptr)
		{
		  strncpy (ns_name, fore_ptr, end_ptr - fore_ptr);
		  ns_name[end_ptr-fore_ptr] = '\0';
		  fore_ptr[end_ptr - fore_ptr] = '\0';
		  end_ptr--;
                  if (num_symbol_names == using_alloc)
		    {
		       symbol_names = (char **) xrealloc (symbol_names, 
						   2 * using_alloc *
						   sizeof (char *));
		       using_alloc *= 2;
		    }
		  symbol_names[num_symbol_names++] = strdup(ns_name);
		}
	    }
	  free (fore_ptr);
	}
    }
  
  /* If there is a namespace selected by the user try that 
     out first and only if you don't find one do the stuff below. To 
     be implemented after the select command where a user can select
     a namespace to look in.*/

  /* If a user has said print A::D::a look in the namespace A::D for
     its using directives.
     namespace A { 
         namespace D {using namespace B;};
	 };
     namespace B { using namespace C;}
     namespace C { using namespace A::D;}
     ...
     We will look in namespace A::D, we find B and add B::a to our list. 
     Then we will look in B and add C::a to our list and so on.
     Transitive closure is mantained hence A::a does not get added again.
     We do not need to look in namespace A since it is enough to consider 
     only the longest prefix.
     */

  add_new_names (actual_name, bl);
  
  /* Walk the current block outwards to find all the using directives. 
     e.g 
     namespace A {...};
     namespace B {...};
     using namespace A;
     int main()
     { using namespace B; }

     If we are stopped in main we will add B and A to the list of
     namespaces. Each using directive is a block. The block structure 
     has been modified to hold a namespace symbol - if we find 
     that we know the block is a using directive. 
   */
    
  while (bl != 0)
    {
      struct symbol *func;
      func = BLOCK_NAMESPACE(bl);
      if (func && func->type && func->type->code == TYPE_CODE_USINGDIR)
	{
	  struct symbol *new_sym;
	  int i = 0, j = 0;
	  if (num_symbol_names)
	    {
	      for (i = 0; i < num_symbol_names; ++i)
		if (!strcmp (symbol_names[i], func->ginfo.name))
		  break;
	    }
	  
	  if (i == num_symbol_names) 
	    /* The name of this using isn't on the list */
	    {
	      if (num_symbol_names == using_alloc)
		{
		  symbol_names = (char **) xrealloc (symbol_names, 
						   2 * using_alloc *
						   sizeof (char *));
		  using_alloc *= 2;
		}
	      symbol_names[num_symbol_names++] = strdup(func->ginfo.name);
	    }
	}
      bl = BLOCK_SUPERBLOCK(bl);
    }
  
  /* For each namespace that we have found (after we looked for using
     directives) - look inside these namespaces for additonal using
     directives */

  /* Reallocate for using_syms to avoid overbounds */
  if ( (using_alloc > INIT_USING_ALLOC) && (num_using_syms < num_symbol_names) )
    {
       using_syms = (struct symbol **) xrealloc (using_syms,
        		using_alloc 
			* sizeof (struct symbol *));
       num_using_syms = using_alloc; 

    }
  for (i = 0; i < num_symbol_names; ++i)
    {
      /* Caution: num_symbol_names changes in the loop 
	 For each namespace in here - lookup the namespace
	 and add all it's using's with transitive
	 closure to the list e.g 
	 list currently has [A, B]
	 A has using ns B, using ns D
	 List after looking at A has [A, B, D]
	 B has using ns C
	 List after looking at B has [A, B, D, C]
	 C has using ns A
	 List after looking at C has [A, B, D, C]
	 
	 */
      using_syms[i] = check_ns_usings (symbol_names[i], block1);
    }
  
  /* Now that we have the complete list of usings we need to
     check if the symbol requested by the user could be an alias.
     e.g 
     
     namespace D { namespace Foo = A::B; }
     using namespace D;
     int main () {... };

     If the user is stopped in main and says  print Foo::a
     we need to look in namespace D to see if that aliases
     Foo to something. We then replace the symbol we need 
     to look for with A::B::a.
     We also lookup this symbol and if we find a local of this
     name (due to a using declaration) we return it.
     */
  
  for (i = 0; i < num_symbol_names; ++i)
    {
      if (!strcmp (real_name, actual_name))
	{
	  if ( using_syms[i] ? (TYPE_CPLUS_SPECIFIC(using_syms[i]->type))->aliases : 0)
	    {
	      char *n = (char *) check_alias (actual_name, 
					      NULL, 
					      using_syms[i]->type);
	      if (strcmp (actual_name, n))
		{
		  actual_name = n;
		  sym = lookup_symbol_1 (n, block1, namespace, 
					 NULL, NULL, local);
		  add_new_names (actual_name, bl);
		  if (sym)
		    {
		      ns_symbols[num_ns_symbols++] = sym;
		      if (*local)
			return choose_symbol(*name, sym);
		    }			  
		  break;
		}
	    }
	}
      else
	break;
    }

  /* We now need to construct the whole list.
     e.g new_names contains A::D::a, B::a and C::a
     and from our list of using directives we have 
     E and B.
     the complete list of symbols to look for is
     B::a, C::a and 
     E::A::D::a, E::B::a, E::C::a,
     B::A::D::a, B::B::a, B::C::a 
     Since A::D::a could be a semi qualified name
     we need to prefix it with E and B. Similarly 
     B::a and C::a need to be prefixed with E And B.
   */  

  if (num_new_names)
    {
      k = num_symbol_names;
      
      /* We first take B::a from new_names - 
	 ( note the loop variable j starts from 1 not 0 
	 since we will add things to A::D::a later.)
	 and prefix it with the names already present
	 in symbol_names. i.e with E and B 
	 and append these 2 symbols to the list of symbol_names. 
	 We then take C::a from new_names and prefix it 
	 with E and B and append these symbols to the list.
	 
	 Note - num_symbol_names stays constant even though the
	 number of entries in symbol_names is increasing. 
	 k keeps a track of the real number of entries. 
       */

      for (j = 1; j < num_new_names; ++j)
	{
	  for (i = 0; i < num_symbol_names ; ++i)
	    {
	      /* Don't use j = 0 since we don't want the first one
		 it is the symbol that has been passed in */
	      char *full_name = xmalloc (strlen (symbol_names[i]) + 
					 strlen(new_names[j]) + 3);
	      full_name[0] = '\0';
	      sprintf(full_name, "%s%s%s", symbol_names[i], "::", new_names[j]);
	      if (k == using_alloc)
		{
		  symbol_names = (char **) xrealloc (symbol_names,
                                                   2 * using_alloc *
                                                   sizeof (char *));
		  using_alloc *= 2;
		}
	      symbol_names[k++] = full_name;
	    }
	}
      
      /* We then add B::a and C::a to the list */
      for (j = 1; j < num_new_names; ++j)
	{
	  if (k == using_alloc)
	    {
	      symbol_names = (char **) xrealloc (symbol_names,
					       2 * using_alloc *
					       sizeof (char *));
	      using_alloc *= 2;
	    }
	  symbol_names[k++] = strdup (new_names[j]);
	}
    }
  
  if (new_names) 
    {
      for (i = 0; i < num_new_names ; ++i)
	free (new_names[i]);
      free (new_names);
      new_names = NULL;
    }
  
  /* In the end we take A::D::a from new_names and prefix 
     that with the using directives.*/

  for (i = 0; i < num_symbol_names; ++i)
    {
      int len_name = (int) strlen (symbol_names[i]);
      int len_actual = (int) strlen (actual_name);
      char *new_name = xmalloc (len_name + len_actual + 3);
      new_name[0] = '\0';
      sprintf(new_name, "%s%s%s", symbol_names[i], "::", actual_name);
      if (symbol_names[i])
        free (symbol_names[i]);
      symbol_names[i] = new_name;
   }

  /* If the array new_names is not empty it adds symbol names
     to the symbol_names array and so we need to change the 
     num_symbol_names variable */

  if (num_new_names)
    num_symbol_names = k;
  
  /* If actual_name is not the same as real_name you found a namespace
     alias for it and so we also need to look up the actual_name symbol.
     Add it to the end of the symbol_names array */
  if (strcmp (actual_name, real_name))
    {
      if (num_symbol_names == using_alloc)
	{
	  symbol_names = (char **) xrealloc (symbol_names,
					       2 * using_alloc *
					     sizeof (char *));
	  using_alloc *= 2;
	} 
      symbol_names[num_symbol_names++] = strdup (actual_name);   
    }
  /* Make sure to set this variable to 0 before returning
     since the array has been freed */
  num_new_names = 0;
  return 0;
}

/* Modified for namespaces. 
1) Do a regular symbol lookup - If we find a local i (in the function)
   we stop looking further. Using declarations look like locals to us.
2) For each scope from innermost to outermost:
  (a) Compute the transitive closure of namespaces to search for each scope
   that we traverse. We keep looking outwards till we reach global scope
   (had there been a local i we wouldn't have been looking at using 
   directives).
   This is done in find_using_directives.
3) Do lookup for every element in the set of namespaces
4) Print out list of symbols found for user to help resolve ambiguities.
   Done in choose_symbol
*/
 
struct symbol *
lookup_symbol (const char *name, register const struct block *block,
	       const namespace_enum namespace, int *is_a_field_of_this, struct symtab **symtab)
{
  struct symbol *sym = NULL;
  int local = 0, i = 0;
  const struct block *block1;
  int sym_alloc = INIT_SYM_ALLOC;
  extern struct block* expression_context_block;
  /* JAGaf74194 - introduce a new local to address this defect */
  int local_is_a_field_of_this=0;

  /* lookup_symbol gets called multiple times so we cache the last 
     value. With namespaces we would be asking a user to choose 
     from the same list many times 
     - or we could be printing the value of a symbol that the user
     did not ask for. e.g - a call to this function made the user 
     pick from a list and the user picked a namespace symbol.
     Then lookup_symbol got called again but this time we returned 
     just 1 symbol which is different from the one the user 
     picked we would not be printing out the value of the symbol
     the user picked.
     
     For a command like print 'source0.c'::file_local when the 
     user is stopped in source1.c - the block for source0.c
     is passed in by the parser later so the cached value may 
     not be correct. So we return the cached value only if we came 
     in with a NULL block or with the expression_context_block.
     
     We need to make sure that *symtab is not changed and need to
     cache that and return it too. It points to the symtab where 
     the symbol was found and is set by lookup_symbol_1.

     is_a_field_of_this is something that indicates that we need 
     to look at the this pointer if we are stopped in a member 
     function and find the field in that. We make sure that other 
     calls to lookup_symbol_1 (in find_using_directives) do
     not change it's value (by passing a NULL for is_a_field_of_this).
     */

  if (!use_cached_value)
    {
      cached_sym.sym_returned = NULL;
      cached_sym.symtab_arg = NULL;
      cached_sym.block_arg = NULL;
      cached_sym.namespace_arg = NULL;
      cached_sym.name_arg = NULL;
      cached_sym.is_a_field_of_this = 0;
      use_cached_value = 1;
    }
  else if ((current_language->la_language == language_cplus)
      && namespace_enabled)
    if ((cached_sym.name_arg ? !strcmp (name, cached_sym.name_arg) : 0)
        && (cached_sym.namespace_arg == namespace) &&
	(block == NULL || block == expression_context_block))
      {
	if (cached_sym.sym_returned != NULL)
	  {	
	    if (symtab) 
	      *symtab = cached_sym.symtab_arg;
	    if (is_a_field_of_this)
	      *is_a_field_of_this = cached_sym.is_a_field_of_this;
	    return cached_sym.sym_returned;
	  }
      }
  
  if ((current_language->la_language == language_cplus)
      && namespace_enabled)
    free_arrays ();

  ambiguity_detected = 0;
  /* Regular symbol lookup - If you find that the symbol is a local
     return it. */

  sym = lookup_symbol_1 (name, block, namespace, &local_is_a_field_of_this, symtab, 
			 &local);

  if (is_a_field_of_this)
    *is_a_field_of_this = local_is_a_field_of_this;

  if (local)
    {
      /* We set ambiguity_detected to true if we find that the
	 symbol has a using_decl_name which is not the same as the
	 name that the user requested or if the name of the symbol we 
	 are returning is not the same as user requested name */
      if ((SYMBOL_LANGUAGE (sym) == language_cplus) 
	  && (SYMBOL_USING_DECL_NAME(sym) ? 
	      strcmp (name, SYMBOL_USING_DECL_NAME(sym)) 
	      : !SYMBOL_MATCHES_NAME (sym, name)))
	ambiguity_detected = 1;
      return sym;
    }

  if ((current_language->la_language == language_cplus)
      && namespace_enabled)
    {
      sym = find_using_directives (&name, block, namespace,
				   &local_is_a_field_of_this, symtab, &local, sym);

      if (is_a_field_of_this)
        *is_a_field_of_this = local_is_a_field_of_this;
      
      /* If find_using_directives found a single symbol - for the case 
	 that the symbol was a local - return it. */

      if (sym)
	{
	  free_arrays();
	  cached_sym.sym_returned = sym;
	  if ((SYMBOL_LANGUAGE (sym) == language_cplus) 
	      && (SYMBOL_USING_DECL_NAME(sym) ? 
		  strcmp (name, SYMBOL_USING_DECL_NAME(sym)) 
		  : !SYMBOL_MATCHES_NAME (sym, name)))
	    ambiguity_detected = 1;
	  if (cached_sym.name_arg)
	    free (cached_sym.name_arg);
	  cached_sym.name_arg = strdup (name);
	  cached_sym.namespace_arg = namespace;
          cached_sym.is_a_field_of_this=local_is_a_field_of_this;     
	  if (symtab) 
	    cached_sym.symtab_arg = *symtab;
	  return sym;
	}
      
      block1 = block ? block : expression_context_block;

      /* For all the new symbol names in symbol_names do a regular 
	 lookup */
      
      for (i = 0; i < num_symbol_names; ++i)
	{
	  struct symbol * ns_sym = NULL;
	  int local2 = 0, j = 0;
	  
	  ns_sym = lookup_symbol_1 (symbol_names[i], block1, namespace, 
				    NULL, NULL, &local2);
	  
	  /* If it is a local - return it */
	  if (local2)
	    {
	      struct symbol* sym_local = choose_symbol(name, ns_sym);
	      if ((SYMBOL_LANGUAGE (sym_local) == language_cplus) 
		  && (SYMBOL_USING_DECL_NAME(sym_local) ? 
		      strcmp (name, SYMBOL_USING_DECL_NAME(sym_local)) 
		      : !SYMBOL_MATCHES_NAME (sym_local, name)))
		ambiguity_detected = 1;
	      return sym_local;
	    }

	  /* Check that the symbol i snot already in the list 
	     by doing a deep equality check - For functions 
	     the block start address should be the same and 
	     for variables the addresses should be the same.
	     We could have multiple symbols due to using
	     declarations within a namespace e.g
	     namespace A { int a; }
	     namespace B { using A::a;} would cause a symbol 
	     named B::a but with the same attributes as A::a
	     to be present in the list of symbols. 
	     Looking for both A::a and B::a could cause 2 symbols
	     with the exact same address to be emitted in the menu.
	     */
	  if (ns_sym)
	    {
	      for (j = 0; j < num_ns_symbols; ++j)
		{
		  if ((SYMBOL_CLASS(ns_sym) == LOC_BLOCK ? 
		       BLOCK_START (SYMBOL_BLOCK_VALUE(ns_sym)) ==
		       BLOCK_START (SYMBOL_BLOCK_VALUE(ns_symbols[j])) : 0) ||
		      (SYMBOL_VALUE_ADDRESS(ns_symbols[j]) == 
		       SYMBOL_VALUE_ADDRESS(ns_sym)) || 
		      (SYMBOL_VALUE(ns_symbols[j]) == 
		       SYMBOL_VALUE(ns_sym)))
		    {
		      ns_sym = NULL;
		      break;
		    }
		}
	    }

	  if (ns_sym && (num_ns_symbols == sym_alloc))
	    {
	      ns_symbols = (struct symbol **) xrealloc 
		(ns_symbols, 2 * sym_alloc * sizeof (struct symbol *));
	      sym_alloc *= 2;
	    }
	  
	  if (ns_sym)
	    ns_symbols[num_ns_symbols++] = ns_sym;
	}

      /* num_ns_symbols indicates that we found symbols. Call choose_symbol */
	 
      if (num_ns_symbols)
	{
	  cached_sym.sym_returned = choose_symbol(name, NULL);
	  if ((SYMBOL_LANGUAGE (cached_sym.sym_returned) == language_cplus) 
	      && (SYMBOL_USING_DECL_NAME(cached_sym.sym_returned) ? 
		  strcmp (name, SYMBOL_USING_DECL_NAME(cached_sym.sym_returned))
		   : !SYMBOL_MATCHES_NAME (cached_sym.sym_returned, name)))
	    ambiguity_detected = 1;
	  if (cached_sym.name_arg)
	    free (cached_sym.name_arg);
	  cached_sym.name_arg = strdup (name);
	  cached_sym.namespace_arg = namespace;
	  cached_sym.is_a_field_of_this=local_is_a_field_of_this;
	  if (symtab) 
	    cached_sym.symtab_arg = *symtab;
	  return cached_sym.sym_returned;
	}
    }
  else
    return sym;
  return 0;
}

/* Added so we can do a lookup without further qualifying names.
   If we are stopped in a member function of a class we previously 
   had a defect which didn't allow us to say break member_function
   of the same class without fully qualifying it.
   The fix for this qualifies everything with the class name when
   one is stopped in a function. 
   There are cases in gdb where one comes in with fully qualified names
   that do not require further qualification. lookup_typename
   is one such case.
   For those we have this routine which just looksup what it has
   been asked to lookup without further qualifying it. */

struct symbol *
lookup_symbol_unamb (const char *name, register const struct block *block,
		     const namespace_enum namespace, int *is_a_field_of_this, 
		     struct symtab **symtab)
{
  int local = 0;
  return lookup_symbol_1 (name, block, namespace, is_a_field_of_this, 
			  symtab, &local);
}

/* Freeing all the global arrays */

static void
free_arrays ()
{
  int i = 0;
  if (symbol_names)
    {
      for (i = 0; i < num_symbol_names; ++i)
       {
         if (symbol_names[i])
	   free (symbol_names[i]);
         symbol_names[i] = NULL;
       }
      free (symbol_names);
      symbol_names = NULL;
    }
  if (using_syms)
    free (using_syms);
  using_syms = NULL;
  if (ns_symbols)
    free (ns_symbols);
  ns_symbols = NULL;
  num_ns_symbols = 0;
  num_symbol_names = 0;
  num_new_names = 0;
}

/* For the namespace that is passed in we find it's symbol. 
   We then find all the using directives in that namespace 
   and add them to the new_names list. 
 */

static int
check_ns (const char *ns_name, const struct block *block)
{
  struct symbol *ns_sym;
  struct type *ns_type;
  int local2 = 0, i = 0, j = 0;

  ns_sym = lookup_symbol_1 (ns_name, block, STRUCT_NAMESPACE, NULL, 
			    NULL, &local2);
  
  if (ns_sym && (TYPE_CODE(SYMBOL_TYPE(ns_sym)) == TYPE_CODE_NAMESPACE))
    {
      /* Compute transitive closure of all usings within it */
      ns_type = SYMBOL_TYPE (ns_sym);
	  
      for (i = 0; i < TYPE_NFIELDS (ns_type); ++i)
	{
	  char *using_name = TYPE_FIELD_NAME (ns_type, i);
	  for (j = 0; j < num_new_names; ++j)
	    {
	      if (!strcmp(new_names[j], using_name))
		break;
	    }
	  if (j == num_new_names)
	    {
	      if (num_new_names == names_alloc)
		{
		  new_names = (char **) xrealloc (new_names,
						  2 * names_alloc
						  * sizeof (char *));
		  names_alloc *= 2;
		}
	      new_names[num_new_names++] = strdup (using_name);
	    }
	}
      return 0;
    }
  else
    return 1;
}

/* Check if the name is part of the aliases list and
   return the real name if it is.
 */

static const char *
check_alias_in_type (const char *name, struct aliases *aliases)
{
  int i = 0;
  if (aliases)
    {
      for (i = 0; i < aliases->num_aliases; ++i)
	if (!strcmp (aliases->alias[i], name))
	      return aliases->name[i];
    }
  return name;
}

/* Check each block walking outward to see if any of them have
   the name as part of their aliases list.   
 */

static const char *         
check_alias_in_blocks (const char *name, const struct block *block)
{
  const struct block *bl = block;
  int i =0;
  
  /* Need to walk outwards checking each block for the alias till
     you get to the global block */
  
  while (bl)
    {
      if (bl->aliases)
	{
	  for (i = 0; i < bl->aliases->num_aliases; ++i)
	    if (!strcmp (bl->aliases->alias[i], name))
		return bl->aliases->name[i];
	}
      bl = BLOCK_SUPERBLOCK (bl);
    }
  return name;
}

 /* Check if the name is part of an alias list. 
    The alias list could be part of a namespace 
    or a block.

    An alias in a namespace gets added to the type
    of that namespace - hence call the routine
    check_alias_in_types

    An alias in a block - function or global gets
    added to the block hence call check_alias_in_blocks.
    
    This function must be called with either block
    or type set not both. 
    
    If you have the name foo::b::a we check first to 
    find if foo is an alias for something. 
    
    Say foo is not an alias. We then check if b is an
    alias for anything. If b is an alias for 
    A::B then the new name is 
    foo::A::B::a
*/


static const char *         
check_alias (const char *name, const struct block *block, 
	     struct type *type)
{
  char *ptr, * old_ptr;
  const char *r_name;
  char *ns_name, *real_name = NULL;
  int  i = 0, size = 0, n = 0, old_size = 0, new_size = 0;

  old_ptr = (char*) alloca(strlen(name) + 1);
  strcpy (old_ptr, name);
  ptr = old_ptr;

  while (ptr)
    {
      ptr = strstr (ptr, "::");
      if (ptr)
	{
	  ns_name = (char *) alloca (ptr - old_ptr + 1);
	  ns_name[0] = '\0';
	  strncpy (ns_name, old_ptr, ptr - old_ptr);
	  ns_name[ptr - old_ptr] = '\0';
	  ptr+=2;
	  old_ptr = ptr;
	}
      else
	ns_name = old_ptr;
      
      assert (!(block && type));
      r_name = 0; /* initialize for compiler warning */
      if (block)
	r_name = check_alias_in_blocks (ns_name, block);
      else if (type)
	r_name = check_alias_in_type (ns_name, 
				      (TYPE_CPLUS_SPECIFIC(type))->aliases);
      if (real_name)
	strcat (real_name, "::");
	    
      new_size = (int) ((real_name ? strlen (real_name): 0) + strlen (r_name) + 3);
      if (size < new_size)
	{
	  real_name = (char *) xrealloc (real_name, new_size);
	  if (!size)
	    real_name[0] = '\0';
	  size = new_size;
	}
      strcat (real_name, r_name);
    }
  
  return real_name;
} 

/* For the name of the namespace passed in - find the namespace symbol 
   and check for other using directives inside that namespace. */

static struct symbol *
check_ns_usings (const char *name, const struct block* block)
{
  struct symbol *sym;
  struct type *type;
  int nfields, i, j;
  int local;

  sym = lookup_symbol_1 (name, block, STRUCT_NAMESPACE, NULL, NULL, &local);

  /* Currently the fields of the namespace just have the using directives
     added to it and none of the other fields that belong to a
     namespace */
  
  if (!sym)
    return NULL;
  type = SYMBOL_TYPE (sym);
  if (!(TYPE_CODE (type) == TYPE_CODE_NAMESPACE))
    return sym;
  
  for (i = 0; i < TYPE_NFIELDS (type); ++i)
    {
      char *ns_name = TYPE_FIELD_NAME (type, i);
      for (j = 0; j < num_symbol_names; ++j)
	{
	  if (!strcmp(symbol_names[j], ns_name))
	    break;
	}
      if (j == num_symbol_names) 
	{
	  if (num_symbol_names == using_alloc)
	    {
	      symbol_names = (char **) xrealloc (symbol_names,
					       2 * using_alloc 
					       * sizeof (char *));
	  /* This allocation might be redundant, using_syms is allocated
	     as much in find_using_directives */
	      using_syms = (struct symbol **) xrealloc (using_syms,
							2 * using_alloc 
							* sizeof (struct symbol *));

	      using_alloc *= 2;
              num_using_syms = using_alloc;
	    }
	  symbol_names[num_symbol_names++] = strdup (ns_name);
	}
    }
  return sym;
}


/* Makes a copy of the symbol and gives it a new name */

struct symbol *
copy_symbol (const char *name, struct symbol *sym)
{
#if 0
  struct symbol *symbol = (struct symbol *) xmalloc (sizeof (struct symbol));
  memcpy (symbol, sym, sizeof (struct symbol));
  
  /* Where do I free this - can't allocate on the obstack since I
     don't have objfile here (it isn't passed into lookup_symbol) */

  SYMBOL_NAME (symbol) = strdup(name);
  SYMBOL_VAR_OFFSET(symbol) = 0;
  if (SYMBOL_LANGUAGE (symbol) == language_cplus)
    SYMBOL_CPLUS_DEMANGLED_NAME (symbol) = SYMBOL_CPLUS_DEMANGLED_NAME (sym);
  free_arrays ();
  return symbol;
#endif
  free_arrays ();
  return sym;
}

/* Prints out a menu for the user to pick a symbol from the list 
   and returns the symbol chosen. 

   We need to make sure that the name of the symbol being returned 
   is the same as that requested by the user because the parser 
   allocates only that many bytes and if we return a larger name 
   we cause gdb to crash. Hence we need to call copy_symbol before 
   returning the symbol.
*/


static struct symbol *
choose_symbol(const char *name, struct symbol *sym)
{
  char *args = NULL, *arg1;
  char *prompt;
  int i;
  struct symbol *sym1;

  /* If a symbol has been passed in then that is what
     we want to return */

  if (sym)
    {
      if (SYMBOL_MATCHES_NAME(sym, name))
	{
	  name =  SYMBOL_NAME (sym);
	}
      sym1 = copy_symbol (name, sym);
      return sym1;    
    }
  
  /* num_ns_symbols indicates the number of symbols in the
     ns_symbols array. If it is 1 then we have just 
     a single symbol - return it.
     If num_ns_symbols > 1 then provide menu to user with the 
     ns_symbols to disambiguate and then they can pick one.
     */

  if (num_ns_symbols == 1)
    {
      if (SYMBOL_MATCHES_NAME(ns_symbols[0], name))
	{
	  name =  SYMBOL_NAME (ns_symbols[0]);
	}
      sym1 = copy_symbol (name, ns_symbols[0]);
      return sym1;    
    }
  else if (num_ns_symbols > 1)
    {      
      i = 0;
      while (i < num_ns_symbols)
	{
	  printf_filtered ("[%d] %s\n",
			   (i+1),
			   SYMBOL_SOURCE_NAME((struct symbol *) ns_symbols[i]));
	  ++i;
	}
      
      if ((prompt = getenv ("PS2")) == NULL)
	{
	  prompt = "> ";
	}

      args = command_line_input (prompt, 0, "overload-choice");
      
      if (args == 0 || *args == 0)
	error_no_arg ("one or more choice numbers");
      
      i = 0;
      while (*args)
	{
	  int num;
	  
	  arg1 = args;
	  while (*arg1 >= '1' && *arg1 <= '9')
	    arg1++;
	  if (*arg1 && *arg1 != ' ' && *arg1 != '\t')
	    error ("Arguments must be choice numbers.");
	  
	  num = atoi (args);
	  if (num > num_ns_symbols || num == 0)     /* legal choices are [1],..,[num_ns_symbols] */
	    {
	      printf_filtered ("No choice number %d.\n", num);
	    }
	  else
	    {
	      if (SYMBOL_MATCHES_NAME (ns_symbols[num - 1], name))
		{
		  name = SYMBOL_NAME (ns_symbols[num - 1]);
		}
	      sym1 = copy_symbol (name, ns_symbols[num-1]);
	      return sym1;    
	    }
	}
    }
  else
    {
      /* If we don't have any symbols free all the global arrays 
	 and return since they will not be freed elsewhere if
	 symbols have not been found */
      free_arrays ();
      return 0;
    }
  return 0;
}

/* Look, in partial_symtab PST, for symbol NAME.  Check the global
   symbols if GLOBAL, the static symbols if not */

static struct partial_symbol *
lookup_partial_symbol_1 (struct partial_symtab *pst, const char *name, int global,
			 namespace_enum namespace)
{
  struct partial_symbol *temp;
  struct partial_symbol **start, **psym;
  struct partial_symbol **top, **bottom, **center;
  int length = 0;
  int do_linear_search = 1;
  
#ifdef HASH_ALL 
  /* For the performance gdb this is no longer true - one cannot
   * find the psyms in the psymtabs anymore. The psymtabs are
   * empty. 
   */
  return NULL;
#else
  length = (global ? pst->n_global_syms : pst->n_static_syms);
  if (length == 0)
    {
      return (NULL);
    }
  start = (global ?
	   pst->objfile->global_psymbols.list + pst->globals_offset :
	   pst->objfile->static_psymbols.list + pst->statics_offset);
  
  if (global)			/* This means we can use a binary search. */
    {
      /* Binary search.  This search is guaranteed to end with center
         pointing at the earliest partial symbol with the correct
         name.  At that point *all* partial symbols with that name
         will be checked against the correct namespace. */

      bottom = start;
      top = start + length -1;

      do_linear_search = (top == bottom && 
          (SYMBOL_LANGUAGE (*top) == language_cplus ||
	   SYMBOL_LANGUAGE (*top) == language_java ||
           SYMBOL_LANGUAGE (*top) == language_fortran)) ? 1 : 0;

      while (top > bottom)
	{
	  center = bottom + (top - bottom) / 2;
	  if (!(center < top))
	    abort ();
	  if (!do_linear_search
	      && (SYMBOL_LANGUAGE (*center) == language_java
		  || SYMBOL_LANGUAGE (*center) == language_fortran))
	    {
	      do_linear_search = 1;
	    }
	  if (STRCMP (SYMBOL_NAME (*center), name) >= 0)
	    {
	      top = center;
	    }
	  else
	    {
	      bottom = center + 1;
	    }
	}
      if (!(top == bottom))
	abort ();

      /* djb - 2000-06-03 - Use SYMBOL_MATCHES_NAME, not a strcmp, so
	 we don't have to force a linear search on C++. Probably holds true
	 for JAVA as well, no way to check.*/
      while (SYMBOL_MATCHES_NAME (*top,name))
	{
          /* do a fuzzy match */
          namespace_enum nm = PSYMBOL_NAMESPACE (*top);
          if ((nm == VAR_OR_STRUCT_NAMESPACE) && 
	      (namespace == STRUCT_NAMESPACE || 
	       namespace == VAR_NAMESPACE))
            {
              return (*top);
            }
          if (PSYMBOL_NAMESPACE (*top) == namespace)
	    {
		  return (*top);
	    }
	  top++;
	}
    }

  /* Can't use a binary search or else we found during the binary search that
     we should also do a linear search. */

  if (do_linear_search)
    {			
      for (psym = start; psym < start + length; psym++)
	{
          if ((namespace == PSYMBOL_NAMESPACE (*psym)) ||
              (PSYMBOL_NAMESPACE (*psym) == VAR_OR_STRUCT_NAMESPACE &&
	       (namespace == STRUCT_NAMESPACE || 
		namespace == VAR_NAMESPACE)))
	    {
	      if (SYMBOL_MATCHES_NAME (*psym, name))
		{
		  return (*psym);
		}
	    }
	}
    }
  return (NULL);
#endif /* Else of HASH_ALL */
}

struct partial_symbol *
lookup_partial_symbol (struct partial_symtab *pst, const char *name, int global,
		       namespace_enum namespace)
{
  return pst->psymbol_to_expand = lookup_partial_symbol_1 (pst,name, global,
                                                namespace);
}

/* Look up a type named NAME in the struct_namespace.  The type returned
   must not be opaque -- i.e., must have at least one field defined

   This code was modelled on lookup_symbol -- the parts not relevant to looking
   up types were just left out.  In particular it's assumed here that types
   are available in struct_namespace and only at file-static or global blocks. */
/* RM: With HP's DOOM, it is possible that the same block has both a
   complete and an opaque version of a type. Fixed this routine to
   handle this */

struct type *
lookup_transparent_type (const char *name)
{
  register struct symbol *sym = NULL;
  struct minimal_symbol *msym;
  register struct symtab *s = NULL;
  register struct partial_symtab *ps;
  struct blockvector *bv;
  register struct objfile *objfile, *objfile2;
  register struct block *block;

  int do_internal_error_at_end = 0;
  char *internal_error_str1 = NULL;
  char *internal_error_str2 = NULL;
  int sym_type = -1;

#ifdef HASH_ALL
  ALL_OBJFILES (objfile)
    {
      current_objfile = objfile;
      msym = (struct minimal_symbol *) 
	retrieve_from_table (*objfile->hash_table, (char *) name, 
			     0, mst_unknown, &sym_type, 1,
			     STRUCT_NAMESPACE, NULL);
      if (msym)
	{
	  ps = find_psymtab (msym);
	  if (ps && !ps->readin) 
	    s = PSYMTAB_TO_SYMTAB (ps);
	  else
	    s = ps->symtab;
	  bv = BLOCKVECTOR (s);

	  block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	  sym = lookup_block_transparent_type (block, name);
          if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
	    return SYMBOL_TYPE (sym);

          /* This shouldn't be necessary, but as a last resort
           * try looking in the statics even though the psymtab
           * claimed the symbol was global. It's possible that
           * the psymtab gets it wrong in some cases.  */
          block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
          sym = lookup_block_transparent_type (block, name);
          if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
            return SYMBOL_TYPE (sym);

          /* Need to search all other blocks because C++ class
           definitions can be scoped. */
          for (int i = 2; i < BLOCKVECTOR_NBLOCKS (bv); i++)
            {
              block = BLOCKVECTOR_BLOCK (bv, i);
              sym = lookup_block_transparent_type (block, name);
              if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
                return SYMBOL_TYPE (sym);
            }  /* for all blocks starting after STATIC_BLOCK */
	}
    }
#else
  /* Now search all the global symbols.  Do the symtab's first, then
     check the psymtab's. If a psymtab indicates the existence
     of the desired name as a global, then do psymtab-to-symtab
     conversion on the fly and return the found symbol.  */

  ALL_SYMTABS (objfile, s)
  {
    bv = BLOCKVECTOR (s);
    block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
    sym = lookup_block_transparent_type (block, name);
    if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
      {
	return SYMBOL_TYPE (sym);
      }
  }

  ALL_PSYMTABS (objfile, ps)
    {
      if (!ps->readin && lookup_partial_symbol (ps, name, 1, STRUCT_NAMESPACE))
        {
	  s = PSYMTAB_TO_SYMTAB (ps);
	  bv = BLOCKVECTOR (s);
	  block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	  sym = lookup_block_transparent_type (block, name);
	  if (!sym)
	    {
	      /* This shouldn't be necessary, but as a last resort
	       * try looking in the statics even though the psymtab
	       * claimed the symbol was global. It's possible that
	       * the psymtab gets it wrong in some cases.
	       */
	      block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
	      sym = lookup_block_transparent_type (block, name);
	      if (!sym)
                if (ps->symtab == NULL)
		  {
		    /* CM: This means that the symtab could not be created for
		       some reason.  Instead of dying, just continue with the
		       search */
		    continue;
		  }
		else
		  {
		    /* CM: Look through all of the symtabs again just in case
		       expanding out this psymtab introduced additional entries
		       in other symtabs. This can happen in HP's version of
		       template instantiation (CTTI). */
		    ALL_SYMTABS (objfile2, s)
		      {
			bv = BLOCKVECTOR (s);
			block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
			sym = lookup_block_transparent_type (block, name);
			if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
                          {
			    return SYMBOL_TYPE (sym);
			  }
		      }
		    /* CM: Check other psymtabs just in case this is defined
		       elsewhere. Give an error at the end of this function if
		       a match was not found. */
		    do_internal_error_at_end = 1;
		    internal_error_str1 = "global";
		    internal_error_str2 = ps->filename;
		    continue;
		  }
	    }
	  if (!TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
	    return SYMBOL_TYPE (sym);
	}
    }

  /* Now search the static file-level symbols.
     Not strictly correct, but more useful than an error.
     Do the symtab's first, then
     check the psymtab's. If a psymtab indicates the existence
     of the desired name as a file-level static, then do psymtab-to-symtab
     conversion on the fly and return the found symbol.
   */

  ALL_SYMTABS (objfile, s)
    {
      bv = BLOCKVECTOR (s);
      block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
      sym = lookup_block_transparent_type(block, name);
    if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
      {
	return SYMBOL_TYPE (sym);
      }
  }

  ALL_PSYMTABS (objfile, ps)
  {
    if (!ps->readin && lookup_partial_symbol (ps, name, 0, STRUCT_NAMESPACE))
      {
	s = PSYMTAB_TO_SYMTAB (ps);
	bv = BLOCKVECTOR (s);
	block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
	sym = lookup_block_transparent_type (block, name);
	if (!sym)
	  {
	    /* This shouldn't be necessary, but as a last resort
	     * try looking in the globals even though the psymtab
	     * claimed the symbol was static. It's possible that
	     * the psymtab gets it wrong in some cases.
	     */
	    block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	    sym = lookup_block_transparent_type (block, name);
	    if (!sym)
                if (ps->symtab == NULL)
		{
                  /* CM: This means that the symtab could not be created for
                     some reason.  Instead of dying, just continue with the
                     search */
                  continue;
                }
		else
		{
                  /* CM: Look through all of the symtabs again just in case
                     expanding out this psymtab introduced additional entries
                     in other symtabs. This can happen in HP's version of
                     template instantiation (CTTI). */
                  ALL_SYMTABS (objfile2, s)
                    {
                      bv = BLOCKVECTOR (s);
                      block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
                      sym = lookup_block_transparent_type (block, name);
                      if (sym && !TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
                        {
                          return SYMBOL_TYPE (sym);
                        }
                    }
                  /* CM: Check other psymtabs just in case this is defined
                     elsewhere. Give an error at the end of this function if
                     a match was not found. */
                  do_internal_error_at_end = 1;
                  internal_error_str1 = "static";
                  internal_error_str2 = ps->filename;
                  continue;
                }
            }
          if (!TYPE_IS_OPAQUE (SYMBOL_TYPE (sym)))
            return SYMBOL_TYPE (sym);
        }
    }

  /* RM: Actually, we _don't_ want to give this error. Just because we
   * found a type in a psymtab, and didn't find the corresponding
   * _transparent_ type in the symtab (or any of the symtabs) doesn't
   * mean that we are in error: the transparent type might be in a file
   * without debug information.
   */
#if 0   
  if (do_internal_error_at_end)
    {
      error ("Internal: %s symbol \`%s\' found in %s psymtab but not in symtab.\n\%s may be an inlined function, or may be a template function\n\(if a template, try specifying an instantiation: %s<type>).", internal_error_str1, name, internal_error_str2, name, name);
    }
#endif /* if 0 */
#endif /* End of else of HASH_ALL */

  return (struct type *) 0;
}


/* Find the psymtab containing main(). */
/* FIXME:  What about languages without main() or specially linked
   executables that have no main() ? */

struct partial_symtab *
find_main_psymtab ()
{
  register struct partial_symtab *pst;
  register struct objfile *objfile;
  struct minimal_symbol *m;

#ifdef HASH_ALL
  m = lookup_minimal_symbol (default_main, NULL, NULL);
  pst = find_psymtab (m);
  return pst;
#else
  ALL_PSYMTABS (objfile, pst)
  {
    if (lookup_partial_symbol (pst, default_main, 1, VAR_NAMESPACE))
      {
	return (pst);
      }
  }
#endif

#ifdef FAT_FREE_PSYMTABS

  /* srikanth, under the new world order, psymtabs could be empty
     for the Wildebeest. Fall back on the linker symbol table, use the
     address therefrom to find out which psymtab would have housed the
     psymbol were one to exist.
  */
  
  if ((m = lookup_minimal_symbol_text(default_main, NULL, NULL)))
    return find_pc_psymtab (SYMBOL_VALUE_ADDRESS (m));

#endif 

#ifndef HASH_ALL
  return (NULL);
#endif 
}

/* Search BLOCK for symbol NAME in NAMESPACE.

   Note that if NAME is the demangled form of a C++ symbol, we will fail
   to find a match during the binary search of the non-encoded names, but
   for now we don't worry about the slight inefficiency of looking for
   a match we'll never find, since it will go pretty quick.  Once the
   binary search terminates, we drop through and do a straight linear
   search on the symbols.  Each symbol which is marked as being a C++
   symbol (language_cplus set) has both the encoded and non-encoded names
   tested for a match. */

extern boolean maint_cmd;  /* JAGaf65828 */

struct symbol *
lookup_block_symbol (register const struct block *block, const char *name,
		     const namespace_enum namespace)
{
  register int bot, top, inc;
  register struct symbol *sym;
  register struct symbol *sym_found = NULL;
  register int do_linear_search = 1;

  const char* copy_name = name;

#ifdef FORTRAN_ADDS_UNDERSCORE
  char *copy1 = NULL;
  int search_fort_underscore = 0;

searchagain:    /* Search again with tail '_' removed from name. */

#endif

  /* If the blocks's symbols were sorted, start with a binary search.  */

  if ( (block ? BLOCK_SHOULD_SORT (block) : NULL) )
    {
      /* Reset the linear search flag so if the binary search fails, we
         won't do the linear search once unless we find some reason to
         do so, such as finding a C++ symbol during the binary search.
         Note that for C++ modules, ALL the symbols in a block should
         end up marked as C++ symbols. */

      do_linear_search = 0;
      top = BLOCK_NSYMS (block);
      bot = 0;

      /* Advance BOT to not far before the first symbol whose name is NAME. */

      while (1)
	{
	  inc = (top - bot + 1);
	  /* No need to keep binary searching for the last few bits worth.  */
	  if (inc < 4)
	    {
	      break;
	    }
	  inc = (inc >> 1) + bot;
	  sym = BLOCK_SYM (block, inc);
	  if (!do_linear_search
	      && (SYMBOL_LANGUAGE (sym) == language_cplus
		  || SYMBOL_LANGUAGE (sym) == language_java
		  || SYMBOL_LANGUAGE (sym) == language_fortran
	      ))
	    {
	      do_linear_search = 1;
	    }
	  if (SYMBOL_NAME (sym)[0] < copy_name[0])
	    {
	      bot = inc;
	    }
	  else if (SYMBOL_NAME (sym)[0] > copy_name[0])
	    {
	      top = inc;
	    }
	  else if (STRCMP (SYMBOL_NAME (sym), copy_name) < 0)
	    {
	      bot = inc;
	    }
	  else
	    {
	      top = inc;
	    }
	}

      /* Now scan forward until we run out of symbols, find one whose
         name is greater than NAME, or find one we want.  If there is
         more than one symbol with the right name and namespace, we
         return the first one; I believe it is now impossible for us
         to encounter two symbols with the same name and namespace
         here, because blocks containing argument symbols are no
         longer sorted.  */

      top = BLOCK_NSYMS (block);
      while (bot < top)
	{
	  sym = BLOCK_SYM (block, bot);
#ifdef FORTRAN_ADDS_UNDERSCORE
          if (search_fort_underscore
              && SYMBOL_LANGUAGE(sym) != language_fortran)
            {
              bot++;
              continue;
            }
#endif
	  inc = SYMBOL_NAME (sym)[0] - copy_name[0];
	  if (inc == 0)
	    {
	      inc = STRCMP (SYMBOL_NAME (sym), copy_name);
	    }
	  if (inc == 0 && (SYMBOL_NAMESPACE (sym) == namespace) 
/* JAGaf17017 - gdb confuses a variable with a type if they have the same name
                If PA, then skip LOC_TYPEDEF
   JAGaf65828 - make it work for 'maint check-symtab' command and enum case.
 */
#ifdef GDB_TARGET_IS_HPPA 
	      && (maint_cmd || (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_ENUM)
			    || (SYMBOL_CLASS (sym) != LOC_TYPEDEF))
#endif
             )
	    {
	      if (!namespace_enabled) 
		{
		  if (SYMBOL_USING_DECL_NAME (sym))
		    {
		      bot++;
		      continue;
		    }
		}

#ifdef HPPA_DOC
              if (SYMBOL_ALIASES (sym))
                {
                  struct frame_info *frame = get_current_frame ();
                  if (frame)
                    sym = find_active_alias (sym, get_frame_pc (frame));
                  else
                    sym = find_active_alias (sym, read_pc ());
                }
#endif
#ifdef FORTRAN_ADDS_UNDERSCORE
              if (copy1)
                free(copy1);
#endif
	      return (sym);
	    }
          /* JAGaf65828 - make it work for ENUM members for 'maint check-symtab'
			  command also.
	   */
          if (maint_cmd && (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_ENUM))
            {
              for (int i = 0; i < TYPE_NFIELDS (SYMBOL_TYPE (sym)); i++)
                {
                  if (!STRCMP (TYPE_FIELDS (SYMBOL_TYPE (sym))[i].name,
                                copy_name))
                  {
#ifdef FORTRAN_ADDS_UNDERSCORE
                    if (copy1)
                      free(copy1);
#endif
                    return (sym);
                  }
                }
            }
          if (!maint_cmd && inc > 0)
	    {
	      break;
	    }
	  bot++;
	}
    }

  /* Here if block isn't sorted, or we fail to find a match during the
     binary search above.  If during the binary search above, we find a
     symbol which is a C++ symbol, then we have re-enabled the linear
     search flag which was reset when starting the binary search.

     This loop is equivalent to the loop above, but hacked greatly for speed.

     Note that parameter symbols do not always show up last in the
     list; this loop makes sure to take anything else other than
     parameter symbols first; it only uses parameter symbols as a
     last resort.  Note that this only takes up extra computation
     time on a match.  */

  if (do_linear_search)
    {
      top = (block ? BLOCK_NSYMS (block) : NULL);
      bot = 0;
      while (bot < top)
	{
	  sym = BLOCK_SYM (block, bot);
#ifdef FORTRAN_ADDS_UNDERSCORE
          if (search_fort_underscore
              && SYMBOL_LANGUAGE(sym) != language_fortran)
            {
              bot++;
              continue;
            }
#endif
          if (SYMBOL_NAMESPACE (sym) == namespace &&
		 SYMBOL_MATCHES_NAME (sym, copy_name))
	    {
	      if (!namespace_enabled) 
		{
		  if (SYMBOL_USING_DECL_NAME (sym))
		    {
		      bot++;
		      continue;
		    }
		}
	      /* If SYM has aliases, then use any alias that is active
	         at the current PC.  If no alias is active at the current
	         PC, then use the main symbol.

	         ?!? Is checking the current pc correct?  Is this routine
	         ever called to look up a symbol from another context?

		 FIXME: No, it's not correct.  If someone sets a
		 conditional breakpoint at an address, then the
		 breakpoint's `struct expression' should refer to the
		 `struct symbol' appropriate for the breakpoint's
		 address, which may not be the PC.

		 Even if it were never called from another context,
		 it's totally bizarre for lookup_symbol's behavior to
		 depend on the value of the inferior's current PC.  We
		 should pass in the appropriate PC as well as the
		 block.  The interface to lookup_symbol should change
		 to require the caller to provide a PC.  */

	      if (SYMBOL_ALIASES (sym))
		sym = find_active_alias (sym, read_pc ());
	    
              sym_found = sym;

              if (SYMBOL_CLASS (sym) != LOC_ARG &&
		  SYMBOL_CLASS (sym) != LOC_LOCAL_ARG &&
		  SYMBOL_CLASS (sym) != LOC_REF_ARG &&
		  SYMBOL_CLASS (sym) != LOC_REGPARM &&
		  SYMBOL_CLASS (sym) != LOC_REGPARM_ADDR &&
		  SYMBOL_CLASS (sym) != LOC_BASEREG_ARG &&
		  SYMBOL_CLASS (sym) != LOC_COMPUTED_ARG &&
		  SYMBOL_CLASS (sym) != LOC_BASEREG_REF_ARG 
/* JAGaf17017 - gdb confuses a variable with a type if they have the same name
                If PA, then skip LOC_TYPEDEF
   JAGaf65828 - make it work for 'maint check-symtab' command and enum case.
 */
#ifdef GDB_TARGET_IS_HPPA
		  && (maint_cmd 
	              || (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_ENUM)
		      || (SYMBOL_CLASS (sym) != LOC_TYPEDEF))
#endif
                 )
		{
		  break;
		}
	    }
	  bot++;
	}
    }

#ifdef FORTRAN_ADDS_UNDERSCORE
  if (!sym_found && !search_fort_underscore)
    {
      int len = strlen(copy_name);
      if (name[len-1] == '_')
        {
          copy1 = strdup(copy_name);
          copy1[len-1] = '\0';
          copy_name = copy1;
          search_fort_underscore = 1;
          goto searchagain;
        }
    }
  if (copy1)
    free(copy1);
  if (search_fort_underscore && sym_found
      && SYMBOL_LANGUAGE(sym_found) != language_fortran)
    return NULL;
#endif

  return (sym_found);		/* Will be NULL if not found. */
}

struct symbol *
lookup_func_symbol_from_block (struct block *block, const char *name)
{
  struct symbol *sym;
  int i;
  for (i = 0; i < BLOCK_NSYMS (block); i++)
    {
      sym = lookup_block_symbol (block, name, VAR_NAMESPACE);
      if (sym != 0 &&
          TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_FUNC)
        return sym;
    }
  return NULL;
}

struct symbol *
lookup_func_symbol_from_objfile(struct objfile *objfile, const char *name)
{
  struct symtab *s = NULL;
  struct blockvector *bv;
  struct block *block;
  struct symbol *sym;
  int i;

  ALL_OBJFILE_SYMTABS (objfile, s)
    {
      bv = BLOCKVECTOR (s);

      /* global symbols */
      if ((sym = lookup_func_symbol_from_block (
                 BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK), name)) != 0)
        return sym;

      /* file static symbols */
      if ((sym = lookup_func_symbol_from_block (
                 BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK), name)) != 0)
        return sym;

      /* local symbols */
      for (i = FIRST_LOCAL_BLOCK; i < BLOCKVECTOR_NBLOCKS (bv); i++)
        if ((sym = lookup_func_symbol_from_block (
                   BLOCKVECTOR_BLOCK (bv, i), name)) != 0)
          return sym;
    }

  if (objfile->separate_debug_objfile)
    return lookup_func_symbol_from_objfile (objfile->separate_debug_objfile,
                                            name);

  return 0;
}

/* Given a main symbol SYM and ADDR, search through the alias
   list to determine if an alias is active at ADDR and return
   the active alias.

   If no alias is active, then return SYM.  */

static struct symbol *
find_active_alias (struct symbol *sym, CORE_ADDR addr)
{
  struct range_list *r;
  struct alias_list *aliases;
#ifdef HPPA_DOC
  struct symbol *matching_alias_sym = NULL;
#endif

  /* If we have aliases, check them first.  */
  aliases = SYMBOL_ALIASES (sym);

  while (aliases)
    {
      if (!SYMBOL_RANGES (aliases->sym))
	return aliases->sym;
      for (r = SYMBOL_RANGES (aliases->sym); r; r = r->next)
	{
	  if (r->start <= addr && r->end > addr)
	    return aliases->sym;

#ifdef HPPA_DOC
          if (r->end == addr)
            matching_alias_sym = aliases->sym;
#endif 
	}
      aliases = aliases->next;
    }

#ifdef HPPA_DOC
  if (matching_alias_sym != NULL)
    return matching_alias_sym;
#endif

  /* Nothing found, return the main symbol.  */
  return sym;
}

/* RM: Just like lookup_block_symbol, but looks for a transparent type
 * -- i.e. opaque types don't make the cut for lookup.
 */
struct symbol *
lookup_block_transparent_type (register const struct block *block, const char *name)
{
  register int bot, top, inc;
  register struct symbol *sym;
  register struct symbol *sym_found = NULL;
  register int do_linear_search = 1;

  /* If the blocks's symbols were sorted, start with a binary search.  */

  if (BLOCK_SHOULD_SORT (block))
    {
      /* Reset the linear search flag so if the binary search fails, we
         won't do the linear search once unless we find some reason to
         do so, such as finding a C++ symbol during the binary search.
         Note that for C++ modules, ALL the symbols in a block should
         end up marked as C++ symbols. */

      do_linear_search = 0;
      top = BLOCK_NSYMS (block);
      bot = 0;

      /* Advance BOT to not far before the first symbol whose name is NAME. */

      while (1)
        {
          inc = (top - bot + 1);
          /* No need to keep binary searching for the last few bits worth.  */
          if (inc < 4)
            {
              break;
            }
          inc = (inc >> 1) + bot;
          sym = BLOCK_SYM (block, inc);
          if (!do_linear_search && 
              (SYMBOL_LANGUAGE (sym) == language_cplus ||
               SYMBOL_LANGUAGE (sym) == language_fortran))
            {
              do_linear_search = 1;
            }
          if (SYMBOL_NAME (sym)[0] < name[0])
            {
              bot = inc;
            }
          else if (SYMBOL_NAME (sym)[0] > name[0])
            {
              top = inc;
            }
          else if (STRCMP (SYMBOL_NAME (sym), name) < 0)
            {
              bot = inc;
            }
          else
            {
              top = inc;
            }
        }

      /* Now scan forward until we run out of symbols, find one whose
       * name is greater than NAME, or find one we want.
       */
      /* RM: but first scan backwards, since we may be pointing to a
       * section of symbols with the same name but different namespace
       */
      while (bot > 0)
        {
          sym = BLOCK_SYM (block, bot - 1);
          inc = SYMBOL_NAME (sym)[0] - name[0];
          if (inc == 0)
            inc = STRCMP (SYMBOL_NAME (sym), name);
          if (inc == 0)
            bot--;
          else
            break;
        }
          
      top = BLOCK_NSYMS (block);
      while (bot < top)
        {
          sym = BLOCK_SYM (block, bot);
          inc = SYMBOL_NAME (sym)[0] - name[0];
          if (inc == 0)
            {
              inc = STRCMP (SYMBOL_NAME (sym), name);
            }
          if (   (inc == 0) 
	      && (   SYMBOL_NAMESPACE (sym) == STRUCT_NAMESPACE 
		  || (SYMBOL_NAMESPACE (sym) == VAR_OR_STRUCT_NAMESPACE)) 
	      && (!TYPE_IS_OPAQUE (SYMBOL_TYPE (sym))))
            {
              return (sym);
            }
          if (inc > 0)
            {
              break;
            }
          bot++;
        }
    }

  /* Here if block isn't sorted, or we fail to find a match during the
     binary search above.  If during the binary search above, we find a
     symbol which is a C++ symbol, then we have re-enabled the linear
     search flag which was reset when starting the binary search.
   */
  if (do_linear_search)
    {
      top = BLOCK_NSYMS (block);
      bot = 0;
      while (bot < top)
        {
          sym = BLOCK_SYM (block, bot);
	  if (   (   (SYMBOL_NAMESPACE (sym) == STRUCT_NAMESPACE) 
		  || (SYMBOL_NAMESPACE (sym) == VAR_OR_STRUCT_NAMESPACE))
	      && (SYMBOL_MATCHES_NAME (sym, name)) 
	      && (!TYPE_IS_OPAQUE (SYMBOL_TYPE (sym))))
	    {
              sym_found = sym;
              break;
            }
          bot++;
        }
    }
  return (sym_found);           /* Will be NULL if not found. */
}



/* Return the symbol for the function which contains a specified
   lexical block, described by a struct block BL.  */

struct symbol *
block_function (struct block *bl)
{
  while (BLOCK_FUNCTION (bl) == 0 && BLOCK_SUPERBLOCK (bl) != 0)
    bl = BLOCK_SUPERBLOCK (bl);

  return BLOCK_FUNCTION (bl);
}

/* Find the symtab associated with PC and SECTION.  Look through the
   psymtabs and read in another symtab if necessary. */

struct symtab *
find_pc_sect_symtab (CORE_ADDR pc, asection *section)
{
  register struct block *b;
  struct blockvector *bv;
  register struct symtab *s = NULL;
  register struct symtab *best_s = NULL;
  register struct partial_symtab *ps = NULL;
  register struct objfile *objfile;
  register struct minimal_symbol *msym;
  CORE_ADDR distance = 0;

  /* srikanth, 990325, beat it quick if pc == 0 as no program object can be
     at this address. Currently we attempt to search for the psymbtab that
     contains address 0 and as it happens the psymtab for the psuedo-file
     `globals' has its textlow and texthigh set to zero causing the entire
     global symbol table to be read in. This takes forever ... (yawn)
   */

   if (pc == 0)
     return NULL;

   pc = SWIZZLE(pc);

#ifdef HASH_ALL
   msym = lookup_minimal_symbol_by_pc (pc);
   if (msym)
     {
       ps = msym->pst;
       
       if (!ps)
	 {
	   /* For 11.22 compatibility we need to look for one with 
	      a pst. For 11.22 some static functions in the linker symbol 
	      table do not have mangled names. For 11.23 they all have
	      mangled names. By looking up the minsym by pc we find 
	      the one from the linker symbol table. Since Judy cannot
	      make the correlation between these 2 functions (because
	      they have different names) this minsym does not have a 
	      pst. Find one with a pst. Calling hash_lookup_minimal_symbol
	      will prefer one with a pst over one without a pst. 
	      */
	   struct minimal_symbol *msym_with_pst = 0;
	   msym_with_pst = hash_lookup_minimal_symbol (SYMBOL_NAME (msym),
						       NULL, NULL,
						       mst_unknown,
						       VAR_NAMESPACE);
	   if (msym_with_pst 
	       && ((SYMBOL_VALUE_ADDRESS (msym_with_pst) == pc)
		   || SYMBOL_VALUE (msym_with_pst) == pc))
	     ps = msym_with_pst->pst;
	 }

       if (ps)
	 {
	   PSYMTAB_TO_SYMTAB (ps);       
	   if (ps->readin)
	     return ps->symtab;
	 }
     }
   /* For fortran there are 2 symbols with the same address 
      _start and __mainprogram. Till the compiler keeps _start
      the code below checks to see if the entry_func_pc is that
      of _start and if it is then look in the symtabs to find
      __mainprogram instead which has a pst attached to it. 
      _start is present in the linker symbol table but is not 
      present in the debug info. 
      __mainprogram makes it to the debug info */
   /* Bindu 090704 Hmm!! this was done as part of a fix to JAGae17486 by
      Poorva. But it doesn't seem to contribute anything to fix that
      particular defect. Now this breaks the fortran nested functions when
      the nested function is in main program. I am commenting this out. */

/*
   if (symfile_objfile 
       && !((symfile_objfile->ei.entry_func_lowpc < pc)
	    && (pc <= symfile_objfile->ei.entry_func_highpc)))
     return NULL;
*/
#endif

  /* Search all symtabs for the one whose file contains our address, and which
     is the smallest of all the ones containing the address.  This is designed
     to deal with a case like symtab a is at 0x1000-0x2000 and 0x3000-0x4000
     and symtab b is at 0x2000-0x3000.  So the GLOBAL_BLOCK for a is from
     0x1000-0x4000, but for address 0x2345 we want to return symtab b.

     This happens for native ecoff format, where code from included files
     gets its own symtab. The symtab for the included file should have
     been read in already via the dependency mechanism.
     It might be swifter to create several symtabs with the same name
     like xcoff does (I'm not sure).

     It also happens for objfiles that have their functions reordered.
     For these, the symtab we are looking for is not necessarily read in.  */

  ALL_SYMTABS (objfile, s)
  {
    bv = BLOCKVECTOR (s);
    b = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);

    BLOCK_START (b) = SWIZZLE(BLOCK_START (b));
    BLOCK_END (b) = SWIZZLE(BLOCK_END (b));

  /* JAGaf65868 - Block end addresses are exclusive on IA and 
     are inclusive on PA */ 
    if (BLOCK_START (b) <= pc
#ifdef HP_IA64 
      && BLOCK_END (b) > pc
#else
      && BLOCK_END (b) >= pc
#endif
      && (distance == 0
	    || BLOCK_END (b) - BLOCK_START (b) < distance))
      {
	/* For an objfile that has its functions reordered,
	   find_pc_psymtab will find the proper partial symbol table
	   and we simply return its corresponding symtab.  */
	/* In order to better support objfiles that contain both
	   stabs and coff debugging info, we continue on if a psymtab
	   can't be found. */
	if ((objfile->flags & OBJF_REORDERED) && objfile->psymtabs)
	  {
	    ps = find_pc_sect_psymtab (pc, section);
	    if (ps)
	      return PSYMTAB_TO_SYMTAB (ps);
	  }
	if (section != 0)
	  {
	    int i;

	    for (i = 0; i < b->nsyms; i++)
	      {
		fixup_symbol_section (b->sym[i], objfile);
		if (section == SYMBOL_BFD_SECTION (b->sym[i]))
		  break;
	      }
	    if (i >= b->nsyms)
	      continue;		/* no symbol in this symtab matches section */
	  }
	distance = BLOCK_END (b) - BLOCK_START (b);
	best_s = s;
      }
  }

  if (best_s != NULL)
    return (best_s);

  s = NULL;
  ps = find_pc_sect_psymtab (pc, section);
  if (ps)
    {
#ifndef HASH_ALL
      /* jini: Dec 2006: Emit the warning only if it has not
         been emitted before. */
      if (ps->readin && !ps->bad_symtab_warning_done)
	/* Might want to error() here (in case symtab is corrupt and
	   will cause a core dump), but maybe we can successfully
	   continue, so let's not.  */
#ifdef DYNLINK_HAS_BREAK_HOOK
	/* We get a spurious warning here when gdb doesn't know about the break
	   instruction in dld.sl.  Suppress this warning. */
	if (!SOLIB_AT_DYNLINK_HOOK (inferior_pid, stop_pc))
#endif
        {
 warning ("\
(Internal error: pc %s is read in psymtab, but not in symtab.)\n",
		   longest_local_hex_string ((LONGEST) pc)); 
 ps->bad_symtab_warning_done = 1;
 
        }
#endif
      s = PSYMTAB_TO_SYMTAB (ps);
    }
  return (s);
}

/* Find the symtab associated with PC.  Look through the psymtabs and
   read in another symtab if necessary.  Backward compatibility, no section */

struct symtab *
find_pc_symtab (CORE_ADDR pc)
{
  if (pc == 0) 
    {
      return NULL;
    }
  
  return find_pc_sect_symtab (pc, find_pc_mapped_section (pc));
}


#ifdef HASH_ALL
struct symtab *
find_symtab (struct minimal_symbol *minsym)
{ 
  struct partial_symtab *pst = NULL;

  if (!minsym)
    return NULL;
  if (!minsym->pst)
    return NULL;  
  
  /* Return the pst associated with this minsym */
  pst = minsym->pst;
   
  if (pst) 
    {
      if (pst->readin)
	return pst->symtab;
      else
	{
	  return PSYMTAB_TO_SYMTAB (pst);
	}
    }
  
  return NULL;
}

/* Find which partial symtab contains PC.  Return 0 if none. 
   Backward compatibility, no section */

static struct partial_symtab *
find_psymtab (struct minimal_symbol *minsym)
{ 
  if (!minsym)
    return NULL;  
  return (minsym->pst ? minsym->pst : NULL);
}
#endif /* #ifdef HASH_ALL */


#if 0

/* Find the closest symbol value (of any sort -- function or variable)
   for a given address value.  Slow but complete.  (currently unused,
   mainly because it is too slow.  We could fix it if each symtab and
   psymtab had contained in it the addresses ranges of each of its
   sections, which also would be required to make things like "info
   line *0x2345" cause psymtabs to be converted to symtabs).  */

struct symbol *
find_addr_symbol (addr, symtabp, symaddrp)
     CORE_ADDR addr;
     struct symtab **symtabp;
     CORE_ADDR *symaddrp;
{
  struct symtab *symtab, *best_symtab;
  struct objfile *objfile;
  register int bot, top;
  register struct symbol *sym;
  register CORE_ADDR sym_addr;
  struct block *block;
  int blocknum;

  /* Info on best symbol seen so far */

  register CORE_ADDR best_sym_addr = 0;
  struct symbol *best_sym = 0;

  /* FIXME -- we should pull in all the psymtabs, too!  */
  ALL_SYMTABS (objfile, symtab)
  {
    /* Search the global and static blocks in this symtab for
       the closest symbol-address to the desired address.  */

    for (blocknum = GLOBAL_BLOCK; blocknum <= STATIC_BLOCK; blocknum++)
      {
	QUIT;
	block = BLOCKVECTOR_BLOCK (BLOCKVECTOR (symtab), blocknum);
	top = BLOCK_NSYMS (block);
	for (bot = 0; bot < top; bot++)
	  {
	    sym = BLOCK_SYM (block, bot);
	    switch (SYMBOL_CLASS (sym))
	      {
	      case LOC_STATIC:
	      case LOC_LABEL:
		sym_addr = SYMBOL_VALUE_ADDRESS (sym);
		break;

	      case LOC_INDIRECT:
		sym_addr = SYMBOL_VALUE_ADDRESS (sym);
		/* An indirect symbol really lives at *sym_addr,
		 * so an indirection needs to be done.
		 * However, I am leaving this commented out because it's
		 * expensive, and it's possible that symbolization
		 * could be done without an active process (in
		 * case this read_memory will fail). RT
		 sym_addr = read_memory_unsigned_integer
		 (sym_addr, TARGET_PTR_BIT / TARGET_CHAR_BIT);
		 */
		break;

	      case LOC_BLOCK:
		sym_addr = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (sym)));
		break;

	      default:
		continue;
	      }

	    if (sym_addr <= addr)
	      if (sym_addr > best_sym_addr)
		{
		  /* Quit if we found an exact match.  */
		  best_sym = sym;
		  best_sym_addr = sym_addr;
		  best_symtab = symtab;
		  if (sym_addr == addr)
		    goto done;
		}
	  }
      }
  }

done:
  if (symtabp)
    *symtabp = best_symtab;
  if (symaddrp)
    *symaddrp = best_sym_addr;
  return best_sym;
}
#endif /* 0 */


/* Find the source file and line number for a given PC value and section.
   Return a structure containing a symtab pointer, a line number,
   and a pc range for the entire source line.
   The value's .pc field is NOT the specified pc.
   NOTCURRENT nonzero means, if specified pc is on a line boundary,
   use the line that ends there.  Otherwise, in that case, the line
   that begins there is used.  */

/* The big complication here is that a line may start in one file, and end just
   before the start of another file.  This usually occurs when you #include
   code in the middle of a subroutine.  To properly find the end of a line's PC
   range, we must search all symtabs associated with this compilation unit, and
   find the one whose first PC is closer than that of the next line in this
   symtab.  */

/* If it's worth the effort, we could be using a binary search.  */

struct symtab_and_line
find_pc_sect_line (CORE_ADDR pc, struct sec *section, int notcurrent)
{
  struct symtab *s = NULL;
  register struct linetable *l;
  register int len;
  register int i;
  register struct linetable_entry *item;
  struct symtab_and_line val;
  struct blockvector *bv;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *mfunsym;
  struct symbol *func;

  /* Info on best line seen so far, and where it starts, and its file.  */

  struct linetable_entry *best = NULL;
  CORE_ADDR best_end = 0;
  struct symtab *best_symtab = 0;

  /* Store here the first line number
     of a file which contains the line at the smallest pc after PC.
     If we don't find a line whose range contains PC,
     we will use a line one less than this,
     with a range from the start of that file to the first line's pc.  */
  struct linetable_entry *alt = NULL;
  struct symtab *alt_symtab = 0;

  /* Info on best line seen in this file.  */

  struct linetable_entry *prev;

  if (inline_debugging && executing_next_command)
    {
      /* For next command, pick the line table entry with the same
	 or lesser inline_idx. (i.e., don't step into callee frames.) */
      int myidx = next_command_inline_idx;
      val = find_pc_inline_idx_line (pc, myidx, notcurrent);
      while (val.line == 0 && myidx > 0)
        {
          myidx = get_inline_contextref_idx (myidx);
          val = find_pc_inline_idx_line (pc, myidx, notcurrent);
        }
      return val;
    }

  /* If this pc is not from the current frame,
     it is the address of the end of a call instruction.
     Quite likely that is the start of the following statement.
     But what we want is the statement containing the instruction.
     Fudge the pc to make sure we get that.  */

  INIT_SAL (&val);		/* initialize to zeroes */

  if (notcurrent)
    pc -= 1;

  /* elz: added this because this function returned the wrong
     information if the pc belongs to a stub (import/export)
     to call a shlib function. This stub would be anywhere between
     two functions in the target, and the line info was erroneously 
     taken to be the one of the line before the pc. 
   */
  /* RT: Further explanation:

   * We have stubs (trampolines) inserted between procedures.
   *
   * Example: "shr1" exists in a shared library, and a "shr1" stub also
   * exists in the main image.
   *
   * In the minimal symbol table, we have a bunch of symbols
   * sorted by start address. The stubs are marked as "trampoline",
   * the others appear as text. E.g.:
   *
   *  Minimal symbol table for main image 
   *     main:  code for main (text symbol)
   *     shr1: stub  (trampoline symbol)
   *     foo:   code for foo (text symbol)
   *     ...
   *  Minimal symbol table for "shr1" image:
   *     ...
   *     shr1: code for shr1 (text symbol)
   *     ...
   *
   * So the code below is trying to detect if we are in the stub
   * ("shr1" stub), and if so, find the real code ("shr1" trampoline),
   * and if found,  do the symbolization from the real-code address
   * rather than the stub address.
   *
   * Assumptions being made about the minimal symbol table:
   *   1. lookup_minimal_symbol_by_pc() will return a trampoline only
   *      if we're really in the trampoline. If we're beyond it (say
   *      we're in "foo" in the above example), it'll have a closer 
   *      symbol (the "foo" text symbol for example) and will not
   *      return the trampoline.
   *   2. lookup_minimal_symbol_text() will find a real text symbol
   *      corresponding to the trampoline, and whose address will
   *      be different than the trampoline address. I put in a sanity
   *      check for the address being the same, to avoid an
   *      infinite recursion.
   */
  msymbol = lookup_minimal_symbol_by_pc (pc);
  if (msymbol != NULL)
    if (MSYMBOL_TYPE (msymbol) == mst_solib_trampoline)
      {
	mfunsym = lookup_minimal_symbol_text (SYMBOL_NAME (msymbol), NULL, NULL);
	if (mfunsym == NULL)
	  /* I eliminated this warning since it is coming out
	   * in the following situation:
	   * gdb shmain // test program with shared libraries
	   * (gdb) break shr1  // function in shared lib
	   * Warning: In stub for ...
	   * In the above situation, the shared lib is not loaded yet, 
	   * so of course we can't find the real func/line info,
	   * but the "break" still works, and the warning is annoying.
	   * So I commented out the warning. RT */
	  /* warning ("In stub for %s; unable to find real function/line info", SYMBOL_NAME(msymbol)) */ ;
	/* fall through */
	else if (SYMBOL_VALUE (mfunsym) == SYMBOL_VALUE (msymbol))
	  /* Avoid infinite recursion */
	  /* See above comment about why warning is commented out */
	  /* warning ("In stub for %s; unable to find real function/line info", SYMBOL_NAME(msymbol)) */ ;
	/* fall through */
	else
	  return find_pc_line (SYMBOL_VALUE (mfunsym), 0);
      }

#ifdef HASH_ALL
  if (msymbol && msymbol->pst && msymbol->pst->symtab)
    s = msymbol->pst->symtab;
#endif



  if (!s)
    s = find_pc_sect_symtab (pc, section);

  if (!s)
    {
      /* if no symbol information, return previous pc */
      if (notcurrent)
	pc++;
      val.pc = pc;
      return val;
    }

  bv = BLOCKVECTOR (s);

  /* Look at all the symtabs that share this blockvector.
     They all have the same apriori range, that we found was right;
     but they have different line tables.  */

  for (; s && BLOCKVECTOR (s) == bv; s = s->next)
    {
      /* Find the best line in this symtab.  */
      l = LINETABLE (s);
      if (!l)
	continue;
      len = l->nitems;
      if (len <= 0)
	{
	  /* I think len can be zero if the symtab lacks line numbers
	     (e.g. gcc -g1).  (Either that or the LINETABLE is NULL;
	     I'm not sure which, and maybe it depends on the symbol
	     reader).  */
	  continue;
	}

      prev = NULL;
      item = l->item;		/* Get first line info */

      /* The first line does not represent a true statement, skip until
	 we find one */
      i = 0;
      if (!item->is_stmt)
	{
	  for (i = 0; i < len; i++, item++)
	    if (item->is_stmt)
	      break;
          if (i == len)	/* No statements found -- set to last item looked at*/
	    item --;
	}

      /* Is this file's first line closer than the first lines of other files?
         If so, record this file, and its first line, as best alternate.  */
      if (item->pc > pc && (!alt || item->pc < alt->pc))
	{
	  alt = item;
	  alt_symtab = s;
	}
      for (; i < len; i++, item++)
	{
	  /* Ignore non statement lines */
	  if (!item->is_stmt)
	    continue;

          /* Ignore inlined functions */
	  if (!inline_debugging && item->context_ref > 0)
	    continue;


	  /* Leave prev pointing to the linetable entry for the last line
	     that started at or before PC.  */
	  if (item->pc > pc) 
	    break;

	  prev = item;
	}

      /* At this point, prev points at the line whose start addr is <= pc, and
         item points at the next line.  If we ran off the end of the linetable
         (pc >= start of the last line), then prev == item.  If pc < start of
         the first line, prev will not be set.  */

      /* Is this file's best line closer than the best in the other files?
         If so, record this file, and its best line, as best so far.  */

      if (prev && (!best || prev->pc > best->pc))
	{
	  best = prev;
	  best_symtab = s;
	}
      /* CM: Always perform this check. In source files that have many
         C #line statements, especially within a block, the "best_end" may
         be in a different symtab that where "best" came from. So, always
         check for a better "best_end". */
      /* If another line is in the linetable, and its PC is closer
	 than the best_end we currently have, take it as best_end.  */
      if (i < len && (best_end == 0 || best_end > item->pc))
	best_end = item->pc;
    }

  if (!best_symtab)
    {
      if (!alt_symtab)
	{			/* If we didn't find any line # info, just
				   return zeros.  */
	  val.pc = pc;
	}
      else
	{
	  val.symtab = alt_symtab;
	  val.line = alt->line - 1;

	  /* Don't return line 0, that means that we didn't find the line.  */
	  if (val.line == 0)
	    ++val.line;

	  val.pc = BLOCK_END (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK));
	  val.end = alt->pc;

#ifdef INLINE_SUPPORT
          val.context_ref = alt->context_ref;
#endif
	}
    }
  else
    {
      val.symtab = best_symtab;
      val.line = best->line;
      val.pc = best->pc;
      if (best_end && (!alt || best_end < alt->pc))
	val.end = best_end;
      else if (alt)
	val.end = alt->pc;
      else
	val.end = BLOCK_END (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK));

#ifdef INLINE_SUPPORT
      val.context_ref = alt ? alt->context_ref : NULL;
      val.inline_idx = best->inline_idx;
#endif

      /* RM: If the function enclosing val.pc ends before pc, then the
         line number was probably bogus -- this can happen if the function
	 enclosing pc has no line number information, as is the case
	 for compiler generated functions (constructors, for example). The
	 line number we get back is the last line number of the preceding
	 function in the text space, which is obviously incorrect. */
      /* JAGaf95694 - on IA it's incorrect to set these fields to zero */
#ifndef HP_IA64
      func = find_pc_function (val.pc);
      if (func && (BLOCK_END (SYMBOL_BLOCK_VALUE (func)) < pc))
        {
          val.line = 0;
          val.pc = 0;
          val.symtab = 0;
#ifdef INLINE_SUPPORT
          val.context_ref = 0;
#endif
        }
#endif
    }
  val.section = section;
  DEBUG(printf("pc = %x val.pc %x val.line %d val.contex_ref %x\n", pc, val.pc, val.line, val.context_ref);)
  return val;
}

/* Backward compatibility (no section) */

struct symtab_and_line
find_pc_line (CORE_ADDR pc, int notcurrent)
{
  asection *section;

  section = find_pc_overlay (pc);
  if (pc_in_unmapped_range (pc, section))
    pc = overlay_mapped_address (pc, section);
  return find_pc_sect_line (pc, section, notcurrent);
}

/* Find line number LINE in any symtab whose name is the same as
   SYMTAB.

   If found, return the symtab that contains the linetable in which it was
   found, set *INDEX to the index in the linetable of the best entry
   found, and set *EXACT_MATCH nonzero if the value returned is an
   exact match.

   If not found, return NULL.  */

static struct symtab *
find_line_symtab (struct symtab *symtab, int line, int *index, int *exact_match)
{
  int exact;

  /* BEST_INDEX and BEST_LINETABLE identify the smallest linenumber > LINE
     so far seen.  */

  int best_index;
  struct linetable *best_linetable;
  struct symtab *best_symtab;

  /* First try looking it up in the given symtab.  */
  best_linetable = LINETABLE (symtab);
  best_symtab = symtab;
  best_index = find_line_common (best_linetable, line, &exact);
  if (best_index < 0 || !exact)
    {
      /* Didn't find an exact match.  So we better keep looking for
         another symtab with the same name.  In the case of xcoff,
         multiple csects for one source file (produced by IBM's FORTRAN
         compiler) produce multiple symtabs (this is unavoidable
         assuming csects can be at arbitrary places in memory and that
         the GLOBAL_BLOCK of a symtab has a begin and end address).  */

      /* BEST is the smallest linenumber > LINE so far seen,
         or 0 if none has been seen so far.
         BEST_INDEX and BEST_LINETABLE identify the item for it.  */
      int best;

      struct objfile *objfile;
      struct symtab *s;
      struct partial_symtab *ps;
      struct linetable *l;
      int ind;

      if (best_index >= 0)
	best = best_linetable->item[best_index].line;
      else
	best = 0;

      ALL_SYMTABS (objfile, s)
      {
	if (!STREQ (symtab->filename, s->filename))
	  continue;
	l = LINETABLE (s);
	ind = find_line_common (l, line, &exact);
	if (ind >= 0)
	  {
	    if (exact)
	      {
		best_index = ind;
		best_linetable = l;
		best_symtab = s;
		goto done;
	      }
	    if (best == 0 || l->item[ind].line < best)
	      {
		best = l->item[ind].line;
		best_index = ind;
		best_linetable = l;
		best_symtab = s;
	      }
	  }
      }

      /* Try the psymtabs if we haven't found it */
      ALL_PSYMTABS (objfile, ps)
        {
          if (!STREQ (symtab->filename, ps->filename))
            continue;
          if (ps->readin)
            continue;
          s = PSYMTAB_TO_SYMTAB (ps);

	  /* JYG: MERGE NOTE: (related to lookup_symtab_1() comments)
	     gdb core dumps w/ list.exp 'list list0.h:1' on Linux, at
	     the following 'l = LINETABLE (s)' statement.
	     Need to short-circuit this if psymtab_to_symtab returns NULL. */
	  if (!s) 
            continue;
  
          l = LINETABLE (s);
          ind = find_line_common (l, line, &exact);
          if (ind >= 0)
            {
              if (exact)
                {
                  best_index = ind;
                  best_linetable = l;
                  goto done;
                }
              if (best == 0 || l->item[ind].line < best)
                {
                  best = l->item[ind].line;
                  best_index = ind;
                  best_linetable = l;
                }
            }
        }
    }
done:
  if (best_index < 0)
    return NULL;

  if (index)
    *index = best_index;
  if (exact_match)
    *exact_match = exact;

  return best_symtab;
}

/* Count the number of symtabs whose name is the same as SYMTAB
   that contains the linetable in which the line is found.
   Return the number of such symtabs, or 0 if not found. */
/* Diwakar: Earlier this function looked for only one line table
   entry matching the given line number. Now we look thru the whole
   table if match_one_line_flag is not set.
*/

static int
find_n_line_symtabs (struct symtab *symtab, int line)
{
  struct objfile *objfile;
  struct symtab *s;
  struct partial_symtab *ps;
  struct linetable *l;
  int ind;
  int exact;
  int n;
  
  /* In order to find all matches, we need to expand all psymtabs
     that matches the filename of SYMTAB. */
  n = 0;
  ALL_PSYMTABS (objfile, ps)
    {
      if (!STREQ (symtab->filename, ps->filename))
	continue;
      /* PSYMTAB_TO_SYMTAB just returns ps->symtab if ps->readin,
	 or it will go do the expansion. */
      s = PSYMTAB_TO_SYMTAB (ps);
      l = LINETABLE (s);
      exact = 0; /* initialize for compiler warning */

      ind = find_line_common (l, line, &exact);

      if ( (ind >= 0) && exact)
#ifndef HP_IA64
        n += count_exact_line_match(l, line, s, NULL);
#else
        { /* With IA64 FORTRAN, gdb gets confused with the module per
	     function.  If FORTRAN and not C++, then we should only find
	     one.  No templates.  */
          n += count_exact_line_match(l, line, s, NULL);
	  if (s->language == language_fortran)
	    return n;
	}
#endif
    }

  return n;
}

/* This function has been added to workaround another compiler problem.
   If a file has no source line, linker assigns the line table of the object 
   linked before to this object. This results in getting multiple sals for a
   give line number. This function cleans the sal list by getting rid of 
   duplicates.

   I am filing a defect against linker.
   - diwakar
*/
   
static void
clean_duplicates (struct symtab_and_line **sals_arr, int *sals_cnt)
{
    int idx1, idx2, unq_cnt;
    struct symtab_and_line *unq_sals;

    int num_sals = *sals_cnt;
    struct symtab_and_line *sals = *sals_arr;

    for (idx1 = 0; idx1 < num_sals - 1; idx1++)
      {
          for (idx2 = idx1 + 1; idx2 < num_sals; idx2++)
             if ((sals[idx1].line == sals[idx2].line) &&
                 (sals[idx1].pc == sals[idx2].pc))
               sals[idx2].pc  = sals[idx2].line = 0;
      }             

    for (idx1 = 0, unq_cnt = 0; idx1 < num_sals; idx1++)
       if ((sals[idx1].line == 0) && (sals[idx1].pc == 0))
         continue;
       else
         unq_cnt++;

    unq_sals= (struct symtab_and_line *)
	xmalloc (unq_cnt * sizeof (struct symtab_and_line));

    for (idx1 = 0, idx2 = 0; idx1 < num_sals; idx1++)
       if ((sals[idx1].line == 0) && (sals[idx1].pc == 0))
         continue;
       else
         {
            unq_sals[idx2].symtab = sals[idx1].symtab;
            unq_sals[idx2].section = 0;
            unq_sals[idx2].line = sals[idx1].line;
            unq_sals[idx2].pc = sals[idx1].pc;
            unq_sals[idx2].end = 0;
            unq_sals[idx2].inline_idx = sals[idx1].inline_idx;
            idx2++;
         }

    free(sals);

    *sals_cnt = unq_cnt;
    *sals_arr = unq_sals;

    return; 
}


/* Return up to maxn sals which have the same name as SYMTAB and contain
   linetable that houses LINE.
   Returns the actual number of sals found. */
static int
find_line_symtabs (struct symtab *symtab, int line,
		   int maxn, struct symtab_and_line *sals)
{
  struct objfile *objfile;
  struct symtab *s;
  struct linetable *l;
  int ind;
  int exact;
  int n = 0, i = 0;
  int match_count = 0;

  if (maxn <= 0)
    return (0);
  
  ALL_SYMTABS (objfile, s)
    {
      if (!STREQ (symtab->filename, s->filename))
	continue;
      l = LINETABLE (s);
      exact = 0; /* initialize for compiler warning */
      ind = find_line_common (l, line, &exact);
      if ( (ind >= 0) && exact)
        {
           if (match_one_line_flag)
             {
	       sals[n].symtab = s;
	       sals[n].section = 0;
	       sals[n].line = line;
	       sals[n].pc = 0;
	       sals[n].end = 0;
	       n++;
             }
           else
             {
                match_count = count_exact_line_match(l, line, s, &sals[n]);
	        n += match_count;
              }
          
	  if (n >= maxn)
	    goto done;
	}
    }
   
 done:
  return n;
}


/* Get the PC value for a given source file and line number and return true.
   Finds the nearest line number and reports it if no exact match is found.
   To detect bad/missing line numbers, use find_exact_line_pc() below.
   Returns zero for bad symbol table (and sets the PC to 0).
   The source file is specified with a struct symtab.
   Sets line to the new line (nearest one found).
   Sets inline_idx to the inline_idx of the line found.  */

int
find_line_pc (struct symtab *symtab, int *line, CORE_ADDR *pc, int *inline_idx)
{
  struct linetable *l;
  int ind;

  *pc = 0;
  if (symtab == 0)
    return 0;

  symtab = find_line_symtab (symtab, *line, &ind, NULL);
  if (symtab != NULL)
    {
      l = LINETABLE (symtab);
      *pc = l->item[ind].pc;
      *line = l->item[ind].line;
      if (inline_idx)
        *inline_idx = l->item[ind].inline_idx;
      return 1;
    }
  else
    return 0;
}

/* Get the PC value for a given source file and line number and return true.
   Returns zero for invalid line number (and sets the PC to 0).
   The source file is specified with a struct symtab.
   Modifed from find_line_pc() after it turned out to not really
   return zero on invalid line numbers. */

int
find_exact_line_pc (struct symtab *symtab, int line, CORE_ADDR *pc)
{
  struct linetable *l;
  int ind;
  int is_exact_match;  /* is "line" actually found in the line number table? */

  *pc = 0;
  if (symtab == 0)
    return 0;

  symtab = find_line_symtab (symtab, line, &ind, &is_exact_match);
  if ((symtab != NULL) && (is_exact_match))
    {
      l = LINETABLE (symtab);
      *pc = l->item[ind].pc;
      return 1;
    }
  else
    return 0;
}

/* Find the range of pc values in a line.
   Store the starting pc of the line into *STARTPTR
   and the ending pc (start of next line) into *ENDPTR.
   Returns 1 to indicate success.
   Returns 0 if could not find the specified line.  */

int
find_line_pc_range (struct symtab_and_line sal, CORE_ADDR *startptr, CORE_ADDR *endptr)
{
  CORE_ADDR startaddr;
  struct symtab_and_line found_sal;
  int line = sal.line;

  startaddr = SWIZZLE(sal.pc);
  if (startaddr == 0 && !find_line_pc (sal.symtab, &line, &startaddr, NULL))
    return 0;

  /* This whole function is based on address.  For example, if line 10 has
     two parts, one from 0x100 to 0x200 and one from 0x300 to 0x400, then
     "info line *0x123" should say the line goes from 0x100 to 0x200
     and "info line *0x355" should say the line goes from 0x300 to 0x400.
     This also insures that we never give a range like "starts at 0x134
     and ends at 0x12c".  */

  found_sal = find_pc_sect_line (startaddr, sal.section, 0);
  if (found_sal.line != sal.line)
    {
      /* The specified line (sal) has zero bytes.  */
      *startptr = found_sal.pc;
      *endptr = found_sal.pc;
    }
  else
    {
      *startptr = found_sal.pc;
      *endptr = found_sal.end;
    }
  return 1;
}


/* Diwakar JAGaf47591 1/14/5
 So far, wdb assumes that there would be only one line 
 table entry that will  a given line number in a symbol table.
 If there are more than one such entries, their column number will
 be different. This will happen when there more than on program
 statements on one source line. In such cases, wdb is only interested
 in the fist line table entry. 

 This is not true if we are using templates. e.g if source line 13 
 falls within a template function, executing break 13 should put 
 breakpoint on a each instance of this template function. They will 
 all have the same column number and line number.
 Now we find all matching line numbers and put their respective pc
 in sals array. 

 Once we find the entry which has the matching column number, we call
 find_pc_partial_function to make sure the function names are different.
 This function has high overhead. That is why we call it only when
 the entry cannot be resolved by looking at line and column numbers.
 But to get this functionality, match_one_line_flag has to be set
 to 0.
 */

static int 
count_exact_line_match(struct linetable *linetable_ptr, int lineno, struct symtab *s,
		       struct symtab_and_line *sals)
{
  int idx = 0;
  int count = 0;
  int first_col = -1;
  CORE_ADDR first_addr = 0;;
  CORE_ADDR start_addr = 0, end_addr = 0;
  CORE_ADDR cur_start_addr = 0, cur_end_addr = 0;

  if (match_one_line_flag) return 1;

  for (idx = 0; idx < linetable_ptr->nitems; idx++)
    {
       register struct linetable_entry *item = &(linetable_ptr->item[idx]);

       /* Ignore non statement lines */
       if (!item->is_stmt)
	continue;

      if (item->line == lineno)
        {  /* first time you see the matching line in the line table */
           if (first_col == -1 ) 
             {
                first_col = item->column;
                first_addr = item->pc;
                if (sals)  /* need to update pc, symtab, line in sals */
                  {
	             sals[count].symtab = s;
	             sals[count].section = 0;
	             sals[count].line = item->line;
	             sals[count].pc = item->pc;
	             sals[count].end = 0;
                     sals[count].inline_idx = item->inline_idx;
                  }
                count++;
                continue;
             }

           /* lines matched and this is not the first time; match cols etc. */
           if (first_col == item->column)
             {  /* this is a heavyweight function; we use it only 
                   if we have to. That is when we run into this 
                   situation. 
                */
                find_pc_partial_function (first_addr, NULL, &start_addr, 
                         &end_addr);

                find_pc_partial_function (item->pc, NULL, &cur_start_addr, 
                         &cur_end_addr);

                if (start_addr != cur_start_addr) /* two different functions */
                  {
                     if (sals)  /* need to update pc, symtab, line in sals */
                       {
	                  sals[count].symtab = s;
	                  sals[count].section = 0;
	                  sals[count].line = item->line;
	                  sals[count].pc = item->pc;
	                  sals[count].end = 0;
                          sals[count].inline_idx = item->inline_idx;
                       }
                     count++;
                  }
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
                if ((start_addr == cur_start_addr) && item->inline_idx)
                  {
                    if (sals)
                      {
                        sals[count].symtab = s;
                        sals[count].section = 0;
                        sals[count].line = item->line;
                        sals[count].pc = item->pc;
                        sals[count].end = 0;
                        sals[count].inline_idx = item->inline_idx;
                      }
                    count++;
                  }
#endif

              }
        }
     }
     return count;
}

/* Given a line table and a line number, return the index into the line
   table for the pc of the nearest line whose number is >= the specified one.
   Return -1 if none is found.  The value is >= 0 if it is an index.

   Set *EXACT_MATCH nonzero if the value returned is an exact match.  */

static int
find_line_common (register struct linetable *l, register int lineno, int *exact_match)
{
  register int i;
  register int len;

  /* BEST is the smallest linenumber > LINENO so far seen,
     or 0 if none has been seen so far.
     BEST_INDEX identifies the item for it.  */

  int best_index = -1;
  int best = 0;

  DEBUG(printf("lineno=%d\n",lineno);)
  if (lineno <= 0)
    return -1;
  if (l == 0)
    return -1;

  len = l->nitems;
  for (i = 0; i < len; i++)
    {
      register struct linetable_entry *item = &(l->item[i]);
      /* Ignore non statement lines */
      if (!item->is_stmt)
	continue;

     
      if (item->line == lineno) 
	{
	  *exact_match = 1;
	   return i;
	}

      if ((item->line > lineno) && (best == 0 || item->line < best))
	{
	  best = item->line;
	  best_index = i;
	}
    }

  /* If we got here, we didn't get an exact match.  */

  *exact_match = 0;
  DEBUG(printf("best match = %d\n", best);)
  return best_index;
}

int
find_pc_line_pc_range (CORE_ADDR pc, CORE_ADDR *startptr, CORE_ADDR *endptr)
{
  struct symtab_and_line sal;
  sal = find_pc_line (pc, 0);
  *startptr = sal.pc;
  *endptr = sal.end;
  return sal.symtab != 0;
}

/* Find out the best matching symtab for a give "pc". 
   Later, we use this to get the steplast line table
   associated with this symtab */
struct symtab_and_line
find_best_symtab_for_pc (CORE_ADDR pc, CORE_ADDR *startptr, CORE_ADDR *endptr)
{
  struct symtab_and_line sal;
  sal = find_pc_line (pc, 0);
  *startptr = sal.pc;
  *endptr = sal.end;
  return sal;
}

/* Given a function symbol SYM, find the symtab and line for the start
   of the function.
   If the argument FUNFIRSTLINE is nonzero, we want the first line
   of real code inside the function.  */

extern int parse_args; /* JAGaf60171 */

struct symtab_and_line
find_function_start_sal (struct symbol *sym, int funfirstline)
{
  CORE_ADDR pc;
  struct symtab_and_line sal;

  pc = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (sym)));
  fixup_symbol_section (sym, NULL);
  if (funfirstline)
    {				/* skip "first line" of function (which is actually its prologue) */
      asection *section = SYMBOL_BFD_SECTION (sym);
      /* If function is in an unmapped overlay, use its unmapped LMA
         address, so that SKIP_PROLOGUE has something unique to work on */
      if (section_is_overlay (section) &&
	  !section_is_mapped (section))
	pc = overlay_unmapped_address (pc, section);

      pc += FUNCTION_START_OFFSET;
      pc = SKIP_PROLOGUE (pc);

      /* For overlays, map pc back into its mapped VMA range */
      pc = overlay_mapped_address (pc, section);
    }
  /* Find the line using the name of the symbol. Useful for inlining. */
  sal = find_pc_name_line (pc, SYMBOL_NAME (sym));

#ifdef PROLOGUE_FIRSTLINE_OVERLAP
  /* Convex: no need to suppress code on first line, if any */
  sal.pc = pc;
#else
  /* Check if SKIP_PROLOGUE left us in mid-line, and the next
     line is still part of the same function.  */
  /* JAGaf60171: Do this only if skipping any instruction(s)
     more explanation in hppa-tdep.c. */
    #ifndef HP_IA64
     if (!parse_args)
   #endif
  {

     if (sal.pc != pc
        && SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (sym))) <= sal.end
        && sal.end < SWIZZLE(BLOCK_END (SYMBOL_BLOCK_VALUE (sym))))
    {
      /* First pc of next line */
      pc = sal.end;

      /* Recalculate the line number (might not be N+1).  */
      sal = find_pc_name_line (pc, SYMBOL_NAME (sym));
    }
   }
 sal.pc = pc;
#endif

  return sal;
}


/* Foreach DOC linetable entry that exactly matches a PC value, call the
   "handler" function with the entry and an optional arg1 value. The
   "notcurrent" field is described in the function find_pc_sect_symtab. The
   handler takes a DOC linetable entry, a flag set to non-zero if there was
   an exact match with the PC requeusted, and the arg1 value. */

void
foreach_pc_doc_line (CORE_ADDR pc, int notcurrent,
		     void (*handler)(struct doc_linetable_entry *, int, void *),
		     void *arg1)
{
  asection *section;
  struct symtab_and_line sal;

  /* find section and pc */
  section = find_pc_overlay (pc);
  if (pc_in_unmapped_range (pc, section))
    pc = overlay_mapped_address (pc, section);

  /* find sal */
 sal = find_pc_sect_line (pc, section, notcurrent);

  if (sal.symtab)
    {
      struct doc_linetable *doc_lines;

      doc_lines = DOC_LINETABLE(sal.symtab);
      if (doc_lines)
        {
          /* Search of DOC linetable */
          if (doc_lines->nitems > 0)
            {
              int start, run;

              /* Binary search. This search uses the following invariant:

                 1. If there exists a number in the list that is less
                    than or equal to the number being compared, at the
                    end of the search, "start" contains the index of the
                    last number that matches that description.

                 2. If there does not exist a number less than or equal,
                    "start" contains 0. */

              start = 0;
              run = doc_lines->nitems;
              while (run > 1)
                {
                  int middle = start + run / 2;

                  run = run - (middle - start);
                  if (doc_lines->item[middle].pc <= pc)
                    {
                      start = middle;
                    }
                } /* while */

              /* Use the "start" value and the invariant above. */
              if (doc_lines->item[start].pc <= pc)
                {
                  int pc_value = (int) (doc_lines->item[start].pc);

                  /* Walk backwards to the first value that matches. */
                  for (;
                       ((start > 0) &&
                        (doc_lines->item[start-1].pc == pc_value));
                       start--);

                  /* Walk forward again and call the handler. */
                  for (;
                       ((start < doc_lines->nitems) &&
                        (doc_lines->item[start].pc == pc_value));
                       start++)
                    {
                      (*handler)(&doc_lines->item[start],
                                 (pc == pc_value),
                                 arg1);
                    }
                } /* if (doc_lines->item[start].pc <= pc) */
            } /* if (doc_lines->nitems > 0) */
        } /* if (doc_lines) */
    } /* if (sal.symtab) */
}

/* If P is of the form "operator[ \t]+..." where `...' is
   some legitimate operator text, return a pointer to the
   beginning of the substring of the operator text.
   Otherwise, return "".  */
char *
operator_chars (char *p, char **end)
{
  *end = "";
  if (strncmp (p, "operator", 8))
    return *end;
  p += 8;

  /* Don't get faked out by `operator' being part of a longer
     identifier.  */
  if (isalpha (*p) || *p == '_' || *p == '$' || *p == '\0')
    return *end;

  /* Allow some whitespace between `operator' and the operator symbol.  */
  while (*p == ' ' || *p == '\t')
    p++;

  /* Recognize 'operator TYPENAME'. */

  if (isalpha (*p) || *p == '_' || *p == '$')
    {
      register char *q = p + 1;
      while (isalnum (*q) || *q == '_' || *q == '$')
	q++;
      *end = q;
      return p;
    }
  /*
   * Fix for JAGaa80390. Code leveraged from GNU-GDB 6.0
   */
  while (*p)
    switch (*p) /* Only statement of the previous while. */
    {
    /*
     * For JAGaa80390. Levaraged from GNU-GDB 6.0.
     */
    case '\\':			/* regexp quoting */
	if (p[1] == '*')
	  {
	    if (p[2] == '=')	/* 'operator\*=' */
	      *end = p + 3;
	    else			/* 'operator\*'  */
	      *end = p + 2;
	    return p;
	  }
	else if (p[1] == '[')
	  {
	    if (p[2] == ']')
	      error ("mismatched quoting on brackets, try 'operator\\[\\]'");
	    else if (p[2] == '\\' && p[3] == ']')
	      {
		*end = p + 4;	/* 'operator\[\]' */
		return p;
	      }
	    else
	      error ("nothing is allowed between '[' and ']'");
	  }
	else 
	  {
	    /* Gratuitous qoute: skip it and move on. */
	    p++;
	    continue;
	  }
	break;
    case '!':
    case '=':
    case '*':
    case '/':
    case '%':
    case '^':
      if (p[1] == '=')
	*end = p + 2;
      else
	*end = p + 1;
      return p;
    case '<':
    case '>':
    case '+':
    case '-':
    case '&':
    case '|':
      if (p[1] == '=' || p[1] == p[0])
	*end = p + 2;
      else
	*end = p + 1;
      return p;
    case '~':
    case ',':
      *end = p + 1;
      return p;
    case '(':
      if (p[1] != ')')
	error ("`operator ()' must be specified without whitespace in `()'");
      *end = p + 2;
      return p;
    case '?':
      if (p[1] != ':')
	error ("`operator ?:' must be specified without whitespace in `?:'");
      *end = p + 2;
      return p;
    case '[':
      if (p[1] != ']')
	error ("`operator []' must be specified without whitespace in `[]'");
      *end = p + 2;
      return p;
    default:
      error ("`operator %s' not supported", p);
      break;
    }
  *end = "";
  return *end;
}

/* Return the number of methods described for TYPE, including the
   methods from types it derives from. This can't be done in the symbol
   reader because the type of the baseclass might still be stubbed
   when the definition of the derived class is parsed.  */

static int total_number_of_methods (struct type *type);

static int
total_number_of_methods (struct type *type)
{
  int n;
  int count;

  CHECK_TYPEDEF (type);
  if (TYPE_CPLUS_SPECIFIC (type) == NULL)
    return 0;
  count = TYPE_NFN_FIELDS_TOTAL (type);

  for (n = 0; n < TYPE_N_BASECLASSES (type); n++)
    count += total_number_of_methods (TYPE_BASECLASS (type, n));

  return count;
}

/* Recursive helper function for decode_line_1.
   Look for methods named NAME in type T.
   Return number of matches.
   Put matches in SYM_ARR, which should have been allocated with
   a size of total_number_of_methods (T) * sizeof (struct symbol *).
   Note that this function is g++ specific.  */

static int
find_methods (struct type *t, char *name, struct symbol **sym_arr)
{
  int i1 = 0;
  int ibase;
  struct symbol *sym_class;
  char *class_name = type_name_no_tag (t);
  int is_destructor = 0;
  char *new_name = name;

#ifdef HP_IA64
  /* Record that we are looking for a destructor. Increment the name 
     past the ~ since in the field list we need to look in same list as the
     constructors.
     */
  if (name[0] == '~')
    {
      is_destructor = 1;
      new_name++;
    }
#endif

  /* Poorva: If we already have the type don't do a lookup_symbol again */
  if (class_name && !t)
    {
      sym_class = lookup_symbol (class_name,
				 (struct block *) NULL,
				 STRUCT_NAMESPACE,
				 (int *) NULL,
				 (struct symtab **) NULL);
      t = SYMBOL_TYPE (sym_class);
    }
  /* Ignore this class if it doesn't have a name.  This is ugly, but
     unless we figure out how to get the physname without the name of
     the class, then the loop can't do any good.  */
  if (class_name && t)
    {
      int method_counter;


      /* Loop over each method name.  At this level, all overloads of a name
         are counted as a single name.  There is an inner loop which loops over
         each overload.  */

      for (method_counter = TYPE_NFN_FIELDS (t) - 1;
	   method_counter >= 0;
	   --method_counter)
	{
	  int field_counter;
	  char *method_name = TYPE_FN_FIELDLIST_NAME (t, method_counter);
	  char dem_opname[64];

	  if (strncmp (method_name, "__", 2) == 0 ||
	      strncmp (method_name, "op", 2) == 0 ||
	      strncmp (method_name, "type", 4) == 0)
	    {
	      if (cplus_demangle_opname (method_name, dem_opname, DMGL_ANSI))
		method_name = dem_opname;
	      else if (cplus_demangle_opname (method_name, dem_opname, 0))
		method_name = dem_opname;
	    }

	  /* Using strcmp_iwt so that we can check if the name is templated 
	     too */
	  if (!strcmp_iwt (new_name, method_name))
	    /* Find all the overloaded methods with that name.  */
	    for (field_counter = TYPE_FN_FIELDLIST_LENGTH (t, method_counter) - 1;
		 field_counter >= 0;
		 --field_counter)
	      {
		struct fn_field *f;
		char *phys_name;

		f = TYPE_FN_FIELDLIST1 (t, method_counter);

		if (TYPE_FN_FIELD_STUB (f, field_counter))
                  check_stub_method (t, method_counter, field_counter);
#if 0
		/* JYG: MERGE FIXME: This is Cygnus code.  We call 
		   check_stub_method instead, which calls
		   parse_and_eval_type (), ..., the whole nine yard,
		   to create (if missing) a symtab entry for type t.
		   This symtab entry will help later as overload
		   'resolution' for g++ (check gdb.c++/templates.exp
		   'p t5i.value' test points).
		   However, this only works if a user sets a breakpoint
		   to the template function first, and removes it, and
		   does a print func call.  So, should we do the complicated
		   thing for this gain???
		   Definitely the test needs to be fixed to have the
		   'print t5i.value' tested before the 'break' command
		   to test what it really is testing. */
		  {
		    char *tmp_name;

		    tmp_name = gdb_mangle_name (t,
						method_counter,
						field_counter);
		    phys_name = alloca (strlen (tmp_name) + 1);
		    strcpy (phys_name, tmp_name);
		    free (tmp_name);
		  }
		else
#endif
		  phys_name = TYPE_FN_FIELD_PHYSNAME (f, field_counter);

#ifndef HP_IA64
		/* Destructor is handled by caller, dont add to the list */
		if (DESTRUCTOR_PREFIX_P (phys_name))
		  continue;
#else
		/* For IA64 - if we are looking for a destructor and the 
		   current field is not a destructor - continue */
		if (is_destructor)
		  {
		    if (!DESTRUCTOR_PREFIX_P (phys_name, t))
		      continue;
		  }
		else /* not looking for destructor and the current field
			is a destructor - continue */
		  {
		    if (DESTRUCTOR_PREFIX_P (phys_name, t))
		      continue;
		  }
#endif
	        sym_arr[i1] = lookup_symbol (phys_name,
					       NULL, VAR_NAMESPACE,
					       (int *) NULL,
					       (struct symtab **) NULL);
		if (sym_arr[i1])
		  i1++;
		else
		  {
		    /* This error message gets printed, but the method
		       still seems to be found
		       fputs_filtered("(Cannot find method ", gdb_stdout);
		       fprintf_symbol_filtered (gdb_stdout, phys_name,
		       language_cplus,
		       DMGL_PARAMS | DMGL_ANSI);
		       fputs_filtered(" - possibly inlined.)\n", gdb_stdout);
		       */
		  }
	      }
	}
    }
  
  /* Only search baseclasses if there is no match yet, since names in
     derived classes override those in baseclasses.
     
     FIXME: The above is not true; it is only true of member functions
     if they have the same number of arguments (??? - section 13.1 of the
     ARM says the function members are not in the same scope but doesn't
     really spell out the rules in a way I understand.  In any case, if
     the number of arguments differ this is a case in which we can overload
     rather than hiding without any problem, and gcc 2.4.5 does overload
     rather than hiding in this case).  */

  if (i1 == 0)
    for (ibase = 0; ibase < TYPE_N_BASECLASSES (t); ibase++)
      i1 += find_methods (TYPE_BASECLASS (t, ibase), name, sym_arr + i1);

  return i1;
}

/* RM: Modified so that this function now returns only function
   symbols: all callers are only interested in functions, so this is
   the correct thing to do.

   Helper function for find_functions().
   Find first N symbols in  BLOCK matching symbol NAME in NAMESPACE;
   put the symbols found in SYM_ARR starting at postion PUT_AT.
   Avoid multiple instances of the 'same' symbol ( 'same' ness detected by 
   a 'shallow' but fast equality test.) 

   This function is based on lookup_block_symbol() but does not attempt
   a binary search.

   Each symbol which is marked as being a C++
   symbol (language_cplus set) has both the encoded and non-encoded names
   tested for a match. */

int
find_n_block_functions(struct block *block, char *name, namespace_enum namespace,
		       int n, struct symbol **sym_arr, int put_at)
{
  int i1 = 0;
  register int bot, top;
  register struct symbol *sym;
  int j = 0;

  top = BLOCK_NSYMS (block);
  bot = 0;
  while (bot < top)
    {
      sym = BLOCK_SYM (block, bot);
      if (!SYMBOL_OBSOLETED (sym) &&
	  SYMBOL_NAMESPACE (sym) == namespace &&
          SYMBOL_CLASS (sym) == LOC_BLOCK &&
          SYMBOL_MATCHES_NAME (sym, name))
        {
          if (i1 == n){
            /* Already found first 'n' symbols */
            break;
          }
          /* Check if 'sym' is already in 'sym_arr[0]...sym_arr[put_at+i1-1]' */
          /* One scenario this is needed is this: 
             Prototype for foo() defined in 'foo.h'; foo() defined in
             'foo.c' that includes 'foo.h'. Definition of foo() appears
             in the symtabs for both 'foo.h' and 'foo.c' 
             */
          for(j=0; j< (put_at+i1); j++){
            /* fast but shallow equality test first */
            if(sym == sym_arr[j]) 
	      break;
#ifdef HP_IA64
	    /* Better equality test - if they have the lowpc they are
	       equal */
	    if (BLOCK_START (SYMBOL_BLOCK_VALUE (sym)) == 
		BLOCK_START (SYMBOL_BLOCK_VALUE (sym_arr[j])))
	      break;
#endif
          }
          if(j==(put_at+i1))
	    { /* not already present in sym_arr, put it */
	      sym_arr[put_at + i1] = sym;
	      i1 = i1 + 1;
            }
        }

      bot++;
    }
  return (i1);
}


/* RM: Modified so that this function now returns only function
   symbols: all callers are only interested in functions, so this is
   the correct thing to do.

   Helper function for find_functions().
   Find first N symbols in  BLOCK matching symbol NAME in NAMESPACE;
   put the symbols found in SYM_ARR starting at postion PUT_AT.
   Avoid multiple instances of the 'same' symbol ( 'same' ness detected by 
   a 'shallow' but fast equality test.) 

   This function is based on lookup_block_symbol() but does not attempt
   a binary search.

   Each symbol which is marked as being a C++
   symbol (language_cplus set) has both the encoded and non-encoded names
   tested for a match. */

int
find_function_matching_minsym (struct block *block, struct minimal_symbol *minsym,
			       char *name, namespace_enum namespace,
			       struct symbol **sym_arr, int put_at)
{
  int i1 = 0;
  register int bot, top;
  register struct symbol *sym;
  int j = 0;

  top = BLOCK_NSYMS (block);
  bot = 0;
  while (bot < top)
    {
      sym = BLOCK_SYM (block, bot);
      if (SYMBOL_NAMESPACE (sym) == namespace &&
	  SYMBOL_CLASS (sym) == LOC_BLOCK &&
	  (SWIZZLE(BLOCK_START(SYMBOL_BLOCK_VALUE (sym)))== 
	   SWIZZLE(SYMBOL_VALUE_ADDRESS(minsym))))
	{
	  /* Check if 'sym' is already in 'sym_arr[0]...sym_arr[put_at+i1-1]' */
	  /* One scenario this is needed is this: 
	     Prototype for foo() defined in 'foo.h'; foo() defined in
	     'foo.c' that includes 'foo.h'. Definition of foo() appears
	     in the symtabs for both 'foo.h' and 'foo.c' 
	   */
	  for (j = 0; j < (put_at + i1); j++)
	    {
	      /* fast but shallow equality test first */
	      if (sym == sym_arr[j])
		break;

	      if (SYMBOL_CLASS (sym_arr[j]) == LOC_BLOCK
				&& SYMBOL_CLASS (sym) == LOC_BLOCK)
		{ /* We have function symbols */
		  if (   BLOCK_START (SYMBOL_BLOCK_VALUE (sym)) 
		      == BLOCK_START (SYMBOL_BLOCK_VALUE (sym_arr[j])))
		    break;  /* same function */
		}
	      else
		{ /* If not function, check for value address equality */
		  if (SYMBOL_VALUE_ADDRESS(sym) == 
		      SYMBOL_VALUE_ADDRESS(sym_arr[j]))
		    break;
		}
	    }
	  if (j == (put_at + i1))
	    {			/* not already present in sym_arr, put it */
	      sym_arr[put_at + i1] = sym;
	      i1 = i1 + 1;
	    }
	}
      bot++;
    }
  return (i1);
}

#if defined (FAT_FREE_PSYMTABS) || defined (HASH_ALL)

static void
expand_containing_psymtab(struct minimal_symbol *m)
{
    struct objfile *objfile;
    struct partial_symtab * pst = NULL;
    CORE_ADDR pc;
    extern int num_symtab_arr;

    if (!m) 
      return;
    
#ifdef HASH_ALL  
    pst = m->pst;
#else
    pst = find_pc_psymtab (SYMBOL_VALUE_ADDRESS (m));
#endif

    if (pst && !pst->readin)
      PSYMTAB_TO_SYMTAB(pst);
    
    /* Poorva: Let's store the symtab we found the minsym in so we don't
       need to iterate over all the symtabs */
    /* JAGaf62562 - <info line main> throws up a menu incase of fortran 
       executables for PA GDB */
    if (pst)
      {
	if (num_symtab_arr < (max_synonyms_seen - 1))
	  {
	    symtab_and_minsym_arr[num_symtab_arr].minsym = m;
	    symtab_and_minsym_arr[num_symtab_arr++].symtab = pst->symtab;
	  }
      }
     /* JAGaf62562 -<END> */
}
#endif

/* Helper function for decode_line_1.
   Look for functions named NAME in all the symbol tables.
   Return number of matches.
   Put matches in SYM_ARR, which should have been allocated with
   a size of max_synonyms_seen * sizeof (struct symbol *).
   Put the corresponding symtabs in SYMTAB_ARR, which should have been 
   allocated with a size of max_synonyms_seen * sizeof (struct symtab *).

   This function is based on lookup_symbol() -- specialized with the 
   value of formal parameter BLOCK as NULL, namespace as VAR_NAMESPACE,
   and IS_A_FIELD_OF_THIS as 0.
   Unlike lookup_symbol() this function does not quit after the first match.
   Unlike lookup_symbol() this function does not check for error conditions 
   such as a discrepancy between partial_symtab and full_symtab.
   Unlike lookup_symbol() this function does not look for minimal
   symbols; the caller does that if this function returns 0. 

   Modified for namespaces where we may have to search for more that
   one symbol because of the addition of using directives to the source.
   We look into the array symbol_names for additional names we need to 
   look for.
  
   look_for_all is passed on to foreach_text_minsym so we look for all 
   matches for it.  
*/



int
find_functions (char *name, struct symbol **sym_arr, struct symtab_and_block *symtab_and_block_arr,
		int look_for_all)
{
  int i1 = 0;
  int maxi1 = max_synonyms_seen;
  int n_found = 0;
  int lookup_terminated = 0;
  int k, i, j;
  register struct symtab *s = NULL;
  register struct partial_symtab *ps;
  struct blockvector *bv;
  register struct objfile *objfile;
  register struct block *block;
  namespace_enum namespace = VAR_NAMESPACE;
  int b = 0;

  /* Initialize sym_arr and symtab_and_block_arr */
  for (k = 0; k < maxi1; k++)
    {
      symtab_and_block_arr[k].symtab = NULL;
      symtab_and_block_arr[k].block_number = -1;
      sym_arr[k] = NULL;
    }

  /* JAGae68084 - if we have seen a shl_load event and
    user has requested for persistent breakpoint then 
    get the symtab corresponding to shared library
    and search in that particlar symtab*/
  /*if (!in_shl_load)*/
    s = current_source_symtab;
 /* else
    s = lookup_symtab(shllib_name);
*/
  if (namespace_enabled && (current_language->la_language == language_cplus))
    {
      /* For namespaces search the current block first. If you
	 find local symbols that point to functions - this may happen
	 due to using declarations you are done. */
      struct block *block = get_selected_block ();
      int block_no = 0, i = 0;
      if (block)
	{
	  bv = s ? BLOCKVECTOR (s) : NULL ;
	  /* Traverse the blockvector till you come to the
	     current block and use that number*/
	  for (i = 0; i < (bv? bv->nblocks : NULL); ++i)
	    {
	      if (bv->block[i] == block)
		{
		  block_no = i;
		  break;
		}
	    }
	  if ((bv ? i < bv->nblocks : NULL))
	    {
	      n_found = 0;
	      n_found = find_n_block_functions (block, name, namespace, maxi1 - i1, sym_arr, i1);
	      
	      if (n_found != 0)
		{
		  block_found = block;
		  for (k = 0; k < n_found; k++)
		    {
		      if ((i1 + k) == maxi1)
			break;
		      symtab_and_block_arr[i1 + k].symtab = s;
		      symtab_and_block_arr[i1 + k].block_number = block_no;
		    }
		  i1 += n_found;
		  if (i1 >= maxi1)
		    {
		      lookup_terminated = 1;
		      goto lookup_over;
		    }
		  goto lookup_over;
		}
	    }
	}
    }
  
  /* Look into the symbol table for the current source file first */
  /* We look for both static and global definitions here to handle
     oveloaded C++ functions. 
   */
  
  if (s)
    {
      bv = BLOCKVECTOR (s);
      for (b = 0; b < 2; b++)
	{
	  /* We do not want to rely on the #define of GLOBAL/STATIC_BLOCK
	     here  */
	  int block_no = (b == 0) ? GLOBAL_BLOCK : STATIC_BLOCK;
	  block = BLOCKVECTOR_BLOCK (bv, block_no);
	  n_found = 0;
	  n_found = find_n_block_functions (block, name, namespace, maxi1 - i1, sym_arr, i1);
	  if (n_found != 0)
	    {
	      block_found = block;
	      for (k = 0; k < n_found; k++)
		{
		  if ((i1 + k) == maxi1)
		    break;
		  symtab_and_block_arr[i1 + k].symtab = s;
		  symtab_and_block_arr[i1 + k].block_number = block_no;
		}
	      i1 += n_found;
	      if (i1 >= maxi1)
		{
		  lookup_terminated = 1;
		  goto lookup_over;
		}
	      if (namespace_enabled && (current_language->la_language == language_cplus))
		continue;
	      else
		break;
	    }
	}
    }

/*JAGae68084 -For IA if we have a shl_load event 
  and we have got the match the go to lookup_over*/
#if defined (HP_IA64) || defined (HASH_ALL)
    if (in_shl_load && i1)
      goto lookup_over;
#endif

  /* The above comment doesn't take into account 
     overloaded functions in different files - so even if we have
     found matches here - we should be looking in other symtabs */

#if !defined (HP_IA64) || !defined (HASH_ALL)
  if (i1)
    goto lookup_over;
#endif

#if defined (FAT_FREE_PSYMTABS) || defined (HASH_ALL)
  /* srikanth, in the case of the Wildebeest, the psymtabs are no 
     longer populated with psymbols. We need to rely on the linker
     symbol table to lookup the symbol, use its address to decide 
     which psymtab would have housed the psymbol, were one to exist.
     The function `foreach_text_minsym' is an iterator, which would
     call its second argument for each matched minimal symbol
     (mst_text and mst_file_text) and pass the match.
   */
  /* Poorva: Allocate a symtab_minsym array to store the symtabs
     you need to look into in
   */
  /* JAGaf62562 - <info line main> throws up a menu incase of fortran
     executables for PA GDB.*/
  symtab_and_minsym_arr = (struct symtab_and_minsym *)
        alloca (maxi1 * sizeof (struct symtab_and_minsym));
  memset (symtab_and_minsym_arr, 0, 
	  maxi1 * sizeof (struct symtab_and_minsym));
  /* JAGaf62562 - <END> */

  foreach_text_minsym (name, expand_containing_psymtab, NULL, look_for_all);

#ifdef HASH_ALL 
  /* In the new theme we need to look up each name in the list of 
     symbol_names which is a collection of possible names that 
     the user has asked for e.g we say break myfunc but many
     namespace functions like A::myfunc, B::myfunc, Y::myfunc are
     possible then symbol_names will contain all of those. 
     */
  if (namespace_enabled && (current_language->la_language == language_cplus))
    {
      for (i = 0; i < num_symbol_names; ++i)
	foreach_text_minsym (symbol_names[i], expand_containing_psymtab, 
		        	NULL, 0);
    }
#endif

#endif


  /* JAGaf62562 - <info line main> throws up a menu incase of fortran executables for PA GDB. 
     Using <minsym>, search for matching symbol in BLOCK */
  /* Now search all the symtabs in the array here and fill in the
     array symtab_and_block_arr - look in the global and static blocks */
  
  for (i = 0; i < maxi1; ++i)
    {
      int b = 0;
      /* JAGae68084 -if we need to place a persistent breakpoint , 
         then skip the main file as the breakpoint would have already placed on it.*/ 
      if (if_yes_to_all_commands && !strcmp(symfile_objfile->name, symtab_and_minsym_arr[i].symtab->objfile->name))
        continue;

      struct symtab *s = symtab_and_minsym_arr[i].symtab;
      struct minimal_symbol *minsym = symtab_and_minsym_arr[i].minsym;
      if (!s)
	break;
      
      bv = BLOCKVECTOR (s);
      for (b = 0; b < 2; b++)
	{
	  /* We do not want to rely on the #define of GLOBAL/STATIC_BLOCK
	     here  */
	  int block_no = (b == 0) ? GLOBAL_BLOCK : STATIC_BLOCK;
	  block = BLOCKVECTOR_BLOCK (bv, block_no);
	  n_found = 0;
	  n_found = find_function_matching_minsym (block, minsym, name, namespace, sym_arr, i1);
	  if (n_found != 0)
	    {
	      block_found = block;
	      for (k = 0; k < n_found; k++)
		{
		  if ((i1 + k) == maxi1)
		    break;
		  symtab_and_block_arr[i1 + k].symtab = s;
		  symtab_and_block_arr[i1 + k].block_number = block_no;
		}
	      i1 += n_found;
	      if (i1 == maxi1)
		{
		  lookup_terminated = 1;
		  goto lookup_over;
		}
	      break;
	    }
	}
    }
  /* JAGaf62562 - <END> */

#if !defined (HP_IA64) || !defined (HASH_ALL)
  /* Now search all the global symbols.  Do the symtab's first, then
     check the psymtab's. If a psymtab indicates the existence
     of the desired name as a global, then do psymtab-to-symtab
     conversion on the fly and return the found symbol.
   */
  
  ALL_SYMTABS (objfile, s)
    {
      if (s == current_source_symtab)
	continue;
      bv = BLOCKVECTOR (s);
      block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
      n_found = 0;
      n_found = find_n_block_functions (block, name, namespace, maxi1 - i1, sym_arr, i1);
      if (n_found != 0)
	{
	  block_found = block;
	  for (k = 0; k < n_found; k++)
	    {
	      if ((i1 + k) == maxi1)
		break;
              /*JAGae68084 -If persistent breakpoints then we will not be placing a
                 breakpoint on the main load module*/ 
              if (if_yes_to_all_commands && !strcmp(symfile_objfile->name, s->objfile->name))
                continue;  
	      symtab_and_block_arr[i1 + k].symtab = s;
	      symtab_and_block_arr[i1 + k].block_number = GLOBAL_BLOCK;
	    }
          /*if (!if_yes_to_all_commands && strcmp(symfile_objfile->name, s->objfile->name))*/
	    i1 += n_found;
	  if (i1 == maxi1)
	    {
	      lookup_terminated = 1;
	      goto lookup_over;
	    }
	}
    }
#endif  

  if (namespace_enabled && (current_language->la_language == language_cplus))
    {
      int i;
      ALL_SYMTABS (objfile, s)
	{
  	  if (!num_symbol_names)
	    break;
	  for (i = 0; i < num_symbol_names; ++i)
	    {
	      bv = BLOCKVECTOR (s);
	      block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	      n_found = 0;
	      n_found = find_n_block_functions (block,symbol_names[i],namespace,
						maxi1 - i1, sym_arr, i1);
	      if (n_found != 0)
		{
		  block_found = block;
		  for (k = 0; k < n_found; k++)
		    {
		      if ((i1 + k) == maxi1)
			break;
		      symtab_and_block_arr[i1 + k].symtab = s;
		      symtab_and_block_arr[i1 + k].block_number = GLOBAL_BLOCK;
		    }
		  i1 += n_found;
		  if (i1 >= maxi1)
		    {
		      lookup_terminated = 1;
		      goto lookup_over;
		    }
		}
	    }
	  
	}
    }
  
  
#if !defined (FAT_FREE_PSYMTABS) && !defined (HASH_ALL)
  ALL_PSYMTABS (objfile, ps)
  {
    if (!ps->readin && lookup_partial_symbol (ps, name, 1, namespace))
      {
	s = PSYMTAB_TO_SYMTAB (ps);
	bv = BLOCKVECTOR (s);
	block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	n_found = 0;
	n_found = find_n_block_functions (block, name, namespace, maxi1 - i1, sym_arr, i1);
	if (n_found != 0)
	  {
	    block_found = block;
	    for (k = 0; k < n_found; k++)
	      {
		if ((i1 + k) == maxi1)
		  break;
		symtab_and_block_arr[i1 + k].symtab = s;
		symtab_and_block_arr[i1 + k].block_number = GLOBAL_BLOCK;
	      }
	    i1 += n_found;
	    if (i1 >= maxi1)
	      {
		lookup_terminated = 1;
		goto lookup_over;
	      }
	  }
      }
  }
#endif

#if !defined (HP_IA64) || !defined (HASH_ALL)
  /* If we found any matches in the global symbols we are done */
  if (i1)
    goto lookup_over;
#endif

#if !defined (HP_IA64) || !defined (HASH_ALL)
  /* Poorva - only if you don't find globals do you look 
     for static functions */

  /* Now search the static file-level symbols.
     Not strictly correct, but more useful than an error.
     Do the symtab's first, then
     check the psymtab's. If a psymtab indicates the existence
     of the desired name as a file-level static, then do psymtab-to-symtab
     conversion on the fly and return the found symbol.
   */
  ALL_SYMTABS (objfile, s)
  {
    if (s == current_source_symtab)
      continue;
    bv = BLOCKVECTOR (s);
    block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
    n_found = 0;
    n_found = find_n_block_functions (block, name, namespace, maxi1 - i1, sym_arr, i1);
    if (n_found != 0)
      {
	block_found = block;
	for (k = 0; k < n_found; k++)
	  {
	    if (i1 + k == maxi1)
	      break;
	    symtab_and_block_arr[i1 + k].symtab = s;
	    symtab_and_block_arr[i1 + k].block_number = STATIC_BLOCK;
	  }
	i1 += n_found;
	if (i1 >= maxi1)
	  {
	    lookup_terminated = 1;
	    goto lookup_over;
	  }
      }
  }
#endif

  /* Poorva - ???? - Should we be reading in statics of other files 
     probably not since if our using dir says A::myfunc and another 
     file has a ns A inside an anon ns then we would get the 
     <unnamed ns>::A::myfunc if we searched other files statics 
   */

  if (namespace_enabled && (current_language->la_language == language_cplus))
    {
      int i;
      ALL_SYMTABS (objfile, s)
	{
	  if (!num_symbol_names)
	    break;
	  for (i = 0; i < num_symbol_names; ++i)
	    {
	      bv = BLOCKVECTOR (s);
	      block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
	      n_found = 0;
	      n_found = find_n_block_functions (block,symbol_names[i],namespace,
						maxi1 - i1, sym_arr, i1);
	      if (n_found != 0)
		{
		  block_found = block;
		  for (k = 0; k < n_found; k++)
		    {
		      if (i1 + k == maxi1)
			break;
		      symtab_and_block_arr[i1 + k].symtab = s;
		      symtab_and_block_arr[i1 + k].block_number = STATIC_BLOCK;
		    }
		  i1 += n_found;
		  if (i1 >= maxi1)
		    {
		      lookup_terminated = 1;
		      goto lookup_over;
		    }
		}
	    }
	  
	}
    }

#if !defined (FAT_FREE_PSYMTABS) && !defined (HASH_ALL)
  ALL_PSYMTABS (objfile, ps)
  {
    if (!ps->readin && lookup_partial_symbol (ps, name, 0, namespace))
      {
	s = PSYMTAB_TO_SYMTAB (ps);
	bv = BLOCKVECTOR (s);
	block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
	n_found = 0;
	n_found = find_n_block_functions (block, name, namespace, maxi1 - i1, sym_arr, i1);
	if (n_found != 0)
	  {
	    block_found = block;
	    for (k = 0; k < n_found; k++)
	      {
		if (i1 + k == maxi1)
		  break;
		symtab_and_block_arr[i1 + k].symtab = s;
		symtab_and_block_arr[i1 + k].block_number = STATIC_BLOCK;
	      }
	    i1 += n_found;
	    if (i1 >= maxi1)
	      {
		lookup_terminated = 1;
		goto lookup_over;
	      }
	  }
      }
  }
#endif

  /* The caller (decode_line_1) will check for the possibility of the symbol 
     being a function that is stored in one of the minimal symbol tables.
     The "minimal symbol table" is built from linker-supplied info.
   */
lookup_over:

  if (lookup_terminated)
    {
      warning ("\
(Only %d definitions of function %s matched; use \
'info func %s' to find all the matching defintions.\n", maxi1, name, name);
    }

  if (namespace_enabled && 
      (current_language->la_language == language_cplus))
    free_arrays ();
  return i1;
}

/* Helper function for decode_line_1.
   Look for functions named NAME in SYMTAB_IN.
   Return number of matches.
   Put matches in SYM_ARR, which should have been allocated with
   a size of max_synonyms_seen * sizeof (struct symbol *).
   Put the corresponding symtabs in SYMTAB_ARR, which should have been 
   allocated with a size of max_synonyms_seen * sizeof (struct symtab *).
*/

/* This function is based on find_functions() 
   Most of the comments above find_functions() apply here as well.
*/

int
find_functions_in_symtab(char *name, struct symbol **sym_arr,
			 struct symtab_and_block *symtab_and_block_arr,
   			 struct symtab *symtab_in)
{
  int i1 = 0;
  int maxi1 = max_synonyms_seen;
  int n_found = 0;
  int lookup_terminated = 0;
  int k;
  register struct symtab *s = NULL;
  struct blockvector *bv;
  register struct block *block;

  namespace_enum namespace = VAR_NAMESPACE;

  /* Initialize sym_arr and symtab_and_block_arr */
   for(k=0; k<maxi1; k++) {
     symtab_and_block_arr[k].symtab = NULL;
     symtab_and_block_arr[k].block_number = -1;
     sym_arr[k] = NULL;
   }

  /* We look for both static and global definitions here to handle
     oveloaded C++ functions. 
   */
  s = symtab_in;
  if(s){
    int b = 0;
    bv = BLOCKVECTOR (s);
    for(b=0; b<2; b++){
     /* We do not want to rely on the #define of GLOBAL/STATIC_BLOCK
        here  */
      int block_no = (b==0)?GLOBAL_BLOCK:STATIC_BLOCK;
      block = BLOCKVECTOR_BLOCK (bv, block_no);
      n_found = 0;
      n_found = find_n_block_functions(block,name,namespace,maxi1-i1,sym_arr,i1);
      if(n_found != 0){
        block_found = block;
        for(k=0; k<n_found; k++) {
           if( (i1+k) == maxi1) break;
           symtab_and_block_arr[i1+k].symtab = s;
           symtab_and_block_arr[i1+k].block_number = block_no;
        }
        i1 += n_found;
        if (i1 == maxi1){
          lookup_terminated = 1;
          goto lookup_over;
        }
      }
    }
  }
  /* The caller (decode_line_1) will check for the possibility of the symbol 
     being a function that is stored in one of the minimal symbol tables.
     The "minimal symbol table" is built from linker-supplied info.
  */
lookup_over:
  if(lookup_terminated){
    warning ("\
(Only %d definitions of function %s matched; use \
'info func %s' to find all the matching defintions.\n", maxi1, name, name);
  }
  return i1;
}

/* Helper function for decode_line_1.
   Build a canonical line spec in CANONICAL if it is non-NULL and if
   the SAL has a symtab.
   If SYMNAME is non-NULL the canonical line spec is `filename:symname'.
   If SYMNAME is NULL the line number from SAL is used and the canonical
   line spec is `filename:linenum'.  */

static void
build_canonical_line_spec (struct symtab_and_line *sal, char *symname, char ***canonical)
{
  char **canonical_arr;
  char *canonical_name;
  char *filename;
  struct symtab *s = sal->symtab;

  if (s == (struct symtab *) NULL
      || s->filename == (char *) NULL
      || canonical == (char ***) NULL)
    return;

  canonical_arr = (char **) xmalloc (sizeof (char *));
  *canonical = canonical_arr;

  filename = s->filename;
  if (symname != NULL)
    {
      canonical_name = xmalloc (strlen (filename) + strlen (symname) + 2);
      sprintf (canonical_name, "%s:%s", filename, symname);
    }
  else
    {
      canonical_name = xmalloc (strlen (filename) + 30);
      sprintf (canonical_name, "%s:%d", filename, sal->line);
    }
  canonical_arr[0] = canonical_name;
}



/* Helper function for decode_line_1.
   Similar to build_canonical_line_spec, but works for multiple addresses.
   Build a canonical line spec in CANONICAL if it is non-NULL and if
   the SAL has a symtab.
   If SYMNAME is non-NULL the canonical line spec is `filename:symname'.
   If SYMNAME is NULL the line number from SAL is used and the canonical
   line spec is `filename:linenum'.  */

static void
build_canonical_line_spec_n (struct symtabs_and_lines *val, char *symname, char ***canonical)
{
  char **canonical_arr;
  char *canonical_name;
  char *filename;
  struct symtab *s ;
  int i;

  if (canonical == (char ***) NULL)
    return;
  for (i=0; i< val->nelts; i++) 
    {
      s = val->sals[i].symtab;
      if ((s == (struct symtab *) NULL)
        || (s->filename == (char *) NULL))
        return;
    }  

  canonical_arr = (char **) xmalloc (val->nelts * sizeof (char *));
  *canonical = canonical_arr;

  for (i=0; i< val->nelts; i++) 
    {
      s = val->sals[i].symtab;
      filename = s->filename;
      if (symname != NULL)
        {
          canonical_name = xmalloc (strlen (filename) + strlen (symname) + 2);
          sprintf (canonical_name, "%s:%s", filename, symname);
        }
      else
        {
          canonical_name = xmalloc (strlen (filename) + 30);
          sprintf (canonical_name, "%s:%d", filename, val->sals[i].line);
        }
      canonical_arr[i] = canonical_name;
    }
}

/* Find an instance of the character C in the string S that is outside
   of all parenthesis pairs, single-quoted strings, and double-quoted
   strings.  */
static char *
find_toplevel_char (char *s, char c)
{
  int quoted = 0;		/* zero if we're not in quotes;
				   '"' if we're in a double-quoted string;
				   '\'' if we're in a single-quoted string.  */
  int depth = 0;		/* number of unclosed parens we've seen */
  char *scan;

  for (scan = s; *scan; scan++)
    {
      if (quoted)
	{
	  if (*scan == quoted)
	    quoted = 0;
	  else if (*scan == '\\' && *(scan + 1))
	    scan++;
	}
      else if (*scan == c && ! quoted && depth == 0)
	return scan;
      else if (*scan == '"' || *scan == '\'')
	quoted = *scan;
      else if (*scan == '(')
	depth++;
      else if (*scan == ')' && depth > 0)
	depth--;
    }

  return 0;
}


/* If decode_line_1 is called, pass NULL for source_file and line_number */

struct symtabs_and_lines
decode_line_1 (char **argptr, int funfirstline, struct symtab *default_symtab,
	       int default_line, char ***canonical,
	       enum how_to_resolve_multiple_symbols resolve)
{
  return decode_line_1_reuse (argptr, funfirstline, default_symtab, 
			      default_line, canonical, resolve, NULL, 0, 0);
}

/* Parse a string that specifies a line number.
   Pass the address of a char * variable; that variable will be
   advanced over the characters actually parsed.

   The string can be:

   LINENUM -- that line number in current file.  PC returned is 0.
   FILE:LINENUM -- that line in that file.  PC returned is 0.
   FUNCTION -- line number of openbrace of that function.
   PC returned is the start of the function.
   VARIABLE -- line number of definition of that variable.
   PC returned is 0.
   FILE:FUNCTION -- likewise, but prefer functions in that file.
   *EXPR -- line in which address EXPR appears.

   This may all be followed by an "if EXPR", which we ignore.

   FUNCTION may be an undebuggable function found in minimal symbol table.

   If the argument FUNFIRSTLINE is nonzero, we want the first line
   of real code inside a function when a function is specified, and it is
   not OK to specify a variable or type to get its line number.

   DEFAULT_SYMTAB specifies the file to use if none is specified.
   It defaults to current_source_symtab.
   DEFAULT_LINE specifies the line number to use for relative
   line numbers (that start with signs).  Defaults to current_source_line.
   If CANONICAL is non-NULL, store an array of strings containing the canonical
   line specs there if necessary. Currently overloaded member functions and
   line numbers or static functions without a filename yield a canonical
   line spec. The array and the line spec strings are allocated on the heap,
   it is the callers responsibility to free them.

   RESOLVE tells us what to do if we find more than one match for a
   symbol. If it is set to USER_CHOICE, a menu is popped up for the
   user to choose from. If it is set to ALL_SYMBOLS, all matching
   symbols are returned.

   Note that it is possible to return zero for the symtab
   if no file is validly specified.  Callers must check that.
   Also, the line number returned may be invalid.  

   If we get to an overloaded function and source_file and line_number are
   provided, if one of the choices matches the source_file and line_number,
   don't ask the user which one to use.
   */

/* We allow single quotes in various places.  This is a hideous
   kludge, which exists because the completer can't yet deal with the
   lack of single quotes.  FIXME: write a linespec_completer which we
   can use as appropriate instead of make_symbol_completion_list.  */

/* Modified for namespaces - We calculate the list of all the using 
   directives and hence all the additional functions we will need to 
   look for. 
   Find_functions then call foreach_text_minsym which looks for all 
   these functions and adds them to the sym_arr and call decode_line_2_reuse
   with all of them
 */


struct symtabs_and_lines
decode_line_1_reuse (char **argptr, int funfirstline, 
                     struct symtab *default_symtab,
		     int default_line, char ***canonical,
		     enum how_to_resolve_multiple_symbols resolve,
		     char *source_file, int line_number, int inline_idx)
{
  struct symtabs_and_lines values;
#ifdef HPPA_COMPILER_BUG
  /* FIXME: The native HP 9000/700 compiler has a bug which appears
     when optimizing this file with target i960-vxworks.  I haven't
     been able to construct a simple test case.  The problem is that
     in the second call to SKIP_PROLOGUE below, the compiler somehow
     does not realize that the statement val = find_pc_line (...) will
     change the values of the fields of val.  It extracts the elements
     into registers at the top of the block, and does not update the
     registers after the call to find_pc_line.  You can check this by
     inserting a printf at the end of find_pc_line to show what values
     it is returning for val.pc and val.end and another printf after
     the call to see what values the function actually got (remember,
     this is compiling with cc -O, with this patch removed).  You can
     also examine the assembly listing: search for the second call to
     skip_prologue; the LDO statement before the next call to
     find_pc_line loads the address of the structure which
     find_pc_line will return; if there is a LDW just before the LDO,
     which fetches an element of the structure, then the compiler
     still has the bug.

     Setting val to volatile avoids the problem.  We must undef
     volatile, because the HPPA native compiler does not define
     __STDC__, although it does understand volatile, and so volatile
     will have been defined away in defs.h.  */
#undef volatile
  volatile struct symtab_and_line val;
#define volatile		/*nothing */
#else
  struct symtab_and_line val;
#endif
  register char *p, *p1;
  char *q, *r, *pp, *comma_ptr, *colon_ptr, *if_ptr, *p2, *filename_colon;
  register struct symtab *s;

  register struct symbol *sym;
  /* The symtab that SYM was found in.  */
  struct symtab *sym_symtab = NULL;

  register CORE_ADDR pc;
  register struct minimal_symbol *msymbol = 0;
  char *copy = 0;
  char *copy2 = 0; /* initialize for compiler warning */
  char *copy3 = 0; /* JAGaf68403 */
  boolean is_done;
  struct symbol *sym_class, *sym_class_preserve;
  int i1;
  int is_quoted;
  int is_quote_enclosed;
  int has_parens;
  int has_if = 0;
  int has_comma = 0;
  char * name_to_search;
  int template_args_has_comma = 0;
  struct symbol **sym_arr;
  struct symtab_and_block *symtab_and_block_arr;
  struct type *t;
  extern char *gdb_completer_quote_characters;
  int usings_present = 0;
  int is_namespace = 0;

  char *libname = NULL;
  int shlib_call = 0;
  struct symtab *s1 = NULL;

  /* Sometimes we need to return a pointer into a copy of the argument
   * string.  The copy is kept in a static pointer to a buffer which is 
   * reallocated when we need a bigger one.
   */

  static char *arg_name = NULL;
  static int arg_name_buf_len = 0;

  struct minsym_and_objfile *minsym_and_obj_arr;
  int minsym_count;

  values.is_file_and_line = 0;

  INIT_SAL (&val);		/* initialize to zeroes */

  /* Defaults have defaults.  */

  if (default_symtab == 0)
    {
      default_symtab = current_source_symtab;
      default_line = current_source_line;
    }


#ifdef HP_IA64
  /* See if arg is *<bundle>:<slot> */

  if (**argptr == '*')
    {
      int has_colon = 0;
      (*argptr)++;

      /* 'has_colon' is for the syntax:
         * (gdb) break *bundle_address:slot_#
       */

      if ((colon_ptr = strstr (*argptr, ":")) != NULL)
	has_colon = 1;
      /* Temporarily zap out ":slot_#" to not
       * confuse the call to parse_and_eval_address_1 below.
       * This is undone below. Do not change colon_ptr!!
       */
      if (has_colon)
	{
	  *colon_ptr = '\0';
	}

      pc = parse_and_eval_address_1 (argptr);
      if (has_colon && (pc % 16) != 0)
	{
	  error ("Break address: 0x%llx [0x%llx:%d] is not bundle-aligned.\n",
		 pc, pc & (~0xF), (int) (pc % 16));
	}
      if ((pc % 16) > 2)
	{
	  error (
	   "Break address: 0x%llx [0x%llx:%d] is an invalid slot number.\n",
		  pc, pc & (~0xF), (int) (pc % 16));
	}
      if (has_colon)
	{
	  register CORE_ADDR slot_no;
	  *colon_ptr = ' ';	/* so that " slot_#" is now visible */
	  slot_no = parse_and_eval_address_1 (argptr);
	  *colon_ptr = ':';	/* so that address is back to original form */
	  if ((((int) slot_no) >= 0) && (slot_no <= 2))
	    pc = pc | slot_no;
	  else
	    error ("Invalid slot number %d\n", (int) slot_no);
	  /* No need to restore ":" */
	}
      values.sals = (struct symtab_and_line *)
	xmalloc (sizeof (struct symtab_and_line));
      values.nelts = 1;
      values.sals[0] = find_pc_line (pc, 0);
      values.sals[0].pc = pc;
      return values;
    }
#else /* !HP_IA64 */
  /* See if arg is *PC */

  if (**argptr == '*')
    {
      (*argptr)++;
      pc = parse_and_eval_address_1 (argptr);

      values.sals = (struct symtab_and_line *)
	xmalloc (sizeof (struct symtab_and_line));

      values.nelts = 1;
      values.sals[0] = find_pc_line (pc, 0);
      values.sals[0].pc = pc;
      values.sals[0].section = find_pc_overlay (pc);

      return values;
    }
#endif /*HP_IA64 */

  /* 'has_if' is for the syntax:
   *     (gdb) break foo if (a==b)
   */
  if ((if_ptr = strstr (*argptr, " if ")) != NULL ||
      (if_ptr = strstr (*argptr, "\tif ")) != NULL ||
      (if_ptr = strstr (*argptr, " if\t")) != NULL ||
      (if_ptr = strstr (*argptr, "\tif\t")) != NULL ||
      (if_ptr = strstr (*argptr, " if(")) != NULL ||
      (if_ptr = strstr (*argptr, "\tif( ")) != NULL)
    has_if = 1;
  /* Temporarily zap out "if (condition)" to not
   * confuse the parenthesis-checking code below.
   * This is undone below. Do not change if_ptr!!
   */
  if (has_if)
    {
      *if_ptr = '\0';
    }

  /* Set various flags.
   * 'has_parens' is important for overload checking, where
   * we allow things like: 
   *     (gdb) break c::f(int)
   */

  /* Maybe arg is FILE : LINENUM or FILE : FUNCTION */

  is_quoted = (**argptr
	       && strchr (gdb_completer_quote_characters, **argptr) != NULL);

  has_parens = ((pp = strchr (*argptr, '(')) != NULL
		&& (pp = strrchr (pp, ')')) != NULL);

  /* Now that we're safely past the has_parens check,
   * put back " if (condition)" so outer layers can see it 
   */
  if (has_if)
    *if_ptr = ' ';

  /* Maybe we were called with a line range FILENAME:LINENUM,FILENAME:LINENUM
     and we must isolate the first half.  Outer layers will call again later
     for the second half.

     Don't count commas that appear in argument lists of overloaded
     functions, or in quoted strings.  It's stupid to go to this much
     trouble when the rest of the function is such an obvious roach hotel.  */
  /* Even a template args can be separated by commas. To be on the safer
     side let's check for comma only if it's not a template args. In the
     other words check for this sequence '<',',','>' .
   */
   template_args_has_comma = (  (filename_colon = strchr (*argptr, '<'))
                            && (filename_colon = strchr (filename_colon, ','))
                            && (filename_colon = strrchr (filename_colon, '>')));
  comma_ptr = 0; /* initialize for compiler warning */
  if ( !template_args_has_comma ) {
    comma_ptr = find_toplevel_char (*argptr, ',');
    has_comma = (comma_ptr != 0);
  }
  /* Temporarily zap out second half to not
   * confuse the code below.
   * This is undone below. Do not change comma_ptr!!
   */
  if (has_comma)
    {
      *comma_ptr = '\0';
    }

  /* Maybe arg is FILE : LINENUM or FILE : FUNCTION */
  /* May also be CLASS::MEMBER, or NAMESPACE::NAME */
  /* Look for ':', but ignore inside of <> */

  if (**argptr == '"')
    {
      is_quote_enclosed = 1;
      (*argptr)++;
    }
  else
    is_quote_enclosed = 0;

  s = NULL;
  for (p = *argptr; *p; p++)
    {
      if (p[0] == '<')
	{
	  char *temp_end = find_template_name_end (p);
	  if (!temp_end)
	    error ("malformed template specification in command");
	  p = temp_end;
	}
      /* Check for the end of the first half of the linespec.  End of line,
         a tab, a double colon or the last single colon, or a space.  But
         if enclosed in double quotes we do not break on enclosed spaces */
      if (!*p
	  || p[0] == '\t'
	  || ((p[0] == ':')
	      && ((p[1] == ':') || (strchr (p + 1, ':') == NULL)))
	  || ((p[0] == ' ') && !is_quote_enclosed))
	break;
      if (p[0] == '.' && strchr (p, ':') == NULL)	/* Java qualified method. */
	{
	  /* Find the *last* '.', since the others are package qualifiers. */
	  for (p1 = p; *p1; p1++)
	    {
	      if (*p1 == '.')
		p = p1;
	    }
	  break;
	}

/* JAGaf08152 - 13/02/2005 Sunil - Check to find the following type of reg exp 
   lib.sl:class::fun To handle arg of type lib.sl : class :: function Look for ':' 
   and '::' in the arg, if found manipulate the original arg itself. Then find 
   the symtab corresponding to the shared lib. if loaded, and strip the library 
   name from the arg and handle the rest of the arg, CLASS :: FUNCTION in the normal way.

*/
      if(*p == '.' && *(p+3) == ':' && strstr((p+4), "::"))
        {
	  char *str = NULL;
	  int slen = (int) (strlen(*argptr) + 1);
          str = (char *) xmalloc (slen);
          memcpy (str, *argptr, slen);

	  /* Extract the library name. */
	  libname = strtok(str, ":");
	  s1 = lookup_symtab (libname);
  	  if (s1)
	    {
	      /* Make argptr point to the rest of the arg CLASS::FUNCTION. */
	      *argptr = (p+4);
	      for(p1 = (p+4); *p1; p1++)
	        if(*p1 == ':' && *(p1+1) == ':')  
		  {
		    copy = p1;
	      	    shlib_call = 1;
		    break;
	          }
	    }
	}
    }
  if (shlib_call) p = copy;

  while (p[0] == ' ' || p[0] == '\t')
    p++;

  /* if the closing double quote was left at the end, remove it */
  if (is_quote_enclosed)
    {
      char *closing_quote = strchr (p, '"');
      if (closing_quote && closing_quote[1] == '\0')
        {
	  *closing_quote = '\0';
	  is_quote_enclosed = 0;
	}
    }

  /* Now that we've safely parsed the first half,
   * put back ',' so outer layers can see it 
   */
  if (has_comma)
    *comma_ptr = ',';


  /* Poorva: Call the name making routine here to add namespaces
     to the list of things to look for. It applies only if
     we are stopped in a particular file since using directives 
     have file scope. 
     
     and then do the while (1) thing for each name in the namelist
     you have built. If the last thing is a namespace then do a
     find_functions for it. 
     b 10
     b main.c:10
     b main.c:main - Don't bother about adding using directives for
                     this case and the 2 above 
     b foo
     b B::foo
     b ::foo
     b function ()
     We have to build the using directives list if we have a function
     */

#ifdef HP_IA64  
  if (namespace_enabled && 
      (current_language->la_language == language_cplus) && 
      ((p[0] == ':' && strlen (p) > 1 && p[1] == ':') ||( p[0] != ':')))
    {
      if (current_source_symtab && get_selected_block())
	{
	  char *argptr1 = *argptr;
	  char *p1 = p;
	  usings_present = 0;

	  /* For getting the name passed in by the user zap out the 
	   if portion, any leading colons for "break ::foo" and leading
	   and following spaces */

	  if (is_quoted)
	    argptr1 = argptr1 + 1;
	  /* Remove any global namespace specifications e.g ::foo */
	  if ((p1[0] == ':' && strlen (p1) > 1 && p1[1] == ':') && 
	      ((argptr1 == p1) || (p1[-1] == ' ') || (p1[-1] == '\t')))
	    (argptr1) += 2;
	  
	  if (has_if)
	    *if_ptr = '\0';
	  
	  p1 = argptr1 + strlen (argptr1);
	  while (p1 > argptr1 && ((p1[-1] == ' ') || (p1[-1] == '\t')))
	    p1--;
	  if (strchr (gdb_completer_quote_characters, *p1) && is_quoted)
	    p1--;

	  if (arg_name_buf_len < (p1 - argptr1 + 1))
	    {
	      arg_name = (char *) xrealloc (arg_name, (p1 - argptr1 + 1));
	      arg_name_buf_len = (int) (p1 - argptr1 + 1);
	    }

	  memcpy (arg_name, argptr1, p1 - argptr1);
	  arg_name[p1 - argptr1] = '\000';

	  if (has_if)
	    *if_ptr = ' ';
	  find_using_directives ((const char **)&arg_name, get_selected_block(), VAR_NAMESPACE,
				 NULL, NULL, NULL, NULL);
	  if (num_symbol_names > 0) 
	    usings_present = 1;      
	}
    }
#endif
  /* Check to see if the largest prefix of the string is a namespace e.g
     the user has passed in A::B::C::D::foo - check if A::B::C::D
     is a namespace in which case go to the else part of this if.
     The else part calls find_functions which also finds overloaded
     functions as opposed to this which calls lookup_symbol if all 
     else fails. */
  if (namespace_enabled && 
      (current_language->la_language == language_cplus) && 
      (!has_parens && !usings_present))
    {
      char *argptr1 = NULL;
      char *p1 = p;
      char *ns_name = NULL;
      struct symbol *sym_ns = NULL;

      if (has_if)
	*if_ptr = '\0';
      
      argptr1 = *argptr;
      if (is_quoted)
	argptr1 = argptr1 + 1;

      p1 = argptr1 + strlen (argptr1);
      while (p1 > argptr1 && ((p1[-1] == ' ') || (p1[-1] == '\t')))
	p1--;
      if (strchr (gdb_completer_quote_characters, *p1) && is_quoted)
	p1--;
      
      arg_name = (char *) xmalloc (p1 - argptr1 + 1);
      /* JAGag38749 and JAGag37223 */
      arg_name_buf_len = p1 - argptr1 + 1;
      memcpy (arg_name, argptr1, p1 - argptr1);
      arg_name[p1 - argptr1] = '\000';
      
      argptr1 = strdup (*argptr);
      if (is_quoted)
	argptr1 = argptr1 + 1;

      ns_name = strrstr (argptr1, "::");
      if (ns_name)
	{
	  int len_argptr1 = (int) strlen (argptr1);
	  *ns_name  = '\0';
	  /* If the last character is a template end then we 
	     know it is not a namespace */
	  if (len_argptr1 && (argptr1 [len_argptr1 - 1] != '>'))
	    {

	      sym_ns = lookup_symbol (argptr1, 0, STRUCT_NAMESPACE, 0,
				      (struct symtab **) NULL);
	  
	      /* Poorva - Since the namespace symbols aren't getting 
		 added as types to the objdebug type table / or globals
		 in psymtabs we don't expand any psymtabs and never
		 know about the existence of this namespace.
		 Hence if we don't find sym_ns assume that it is a namespace
		 */
	  
	      if (sym_ns 
		  && sym_ns->type 
		  && (TYPE_CODE (sym_ns->type) == TYPE_CODE_NAMESPACE))
		is_namespace = 1;
	      else if (!sym_ns)
		is_namespace = 1;
	    }
	}

      if (has_if)
	 *if_ptr = ' ';
    }
  
  if ((p[0] == ':' || p[0] == '.') && !has_parens && !usings_present
/* MERGE: is_namespace needs to be checked only for IPF since PA doesnot have namespaces */ 
#ifdef HP_IA64       
    && !is_namespace
#endif 
         )
    {
      if (is_quoted)
	*argptr = *argptr + 1;
      if (strlen(p) > 1 && (p[0] == '.' || p[1] ==':'))
	{
	  char *saved_arg2 = *argptr;
	  char *temp_end;
	  /* First check for "global" namespace specification,
	     of the form "::foo". If found, skip over the colons
	     and jump to normal symbol processing */
	  if ((*argptr == p) || (p[-1] == ' ') || (p[-1] == '\t'))
	    saved_arg2 += 2;

	  /* We have what looks like a class or namespace
	     scope specification (A::B), possibly with many
	     levels of namespaces or classes (A::B::C::D).

	     Some versions of the HP ANSI C++ compiler (as also possibly
	     other compilers) generate class/function/member names with
	     embedded double-colons if they are inside namespaces. To
	     handle this, we loop a few times, considering larger and
	     larger prefixes of the string as though they were single
	     symbols.  So, if the initially supplied string is
	     A::B::C::D::foo, we have to look up "A", then "A::B",
	     then "A::B::C", then "A::B::C::D", and finally
	     "A::B::C::D::foo" as single, monolithic symbols, because
	     A, B, C or D may be namespaces.

	     Note that namespaces can nest only inside other
	     namespaces, and not inside classes.  So we need only
	     consider *prefixes* of the string; there is no need to look up
	     "B::C" separately as a symbol in the previous example. */

	  p2 = p;		/* save for restart */
	  while (1)
	    {
	      /* Extract the class name.  */
	      p1 = p;
	      while (p != *argptr && p[-1] == ' ')
		--p;
	      copy = (char *) alloca (p - *argptr + 1);
	      memcpy (copy, *argptr, p - *argptr);
	      copy[p - *argptr] = 0;

	      /* Discard the class name from the arg.  */
	      p = p1 + (p1[0] == ':' ? 2 : 1);
	      while (*p == ' ' || *p == '\t')
		p++;
	      *argptr = p;

	      sym_class = lookup_symbol (copy, 0, STRUCT_NAMESPACE, 0,
					 (struct symtab **) NULL);
              sym_class_preserve = sym_class;
              /* If the symbol is not found in STRUCT_NAMESPACE, search
                 in VAR_NAMESPACE. All typedef's will be in VAR_NAMESPACE.
                 This will enable gdb to set a breakpoint in a function
                 declared inside a typedef'ed class .
                 i.e., typedef class_name type_class_name;
                       (gdb) break type_class_name::func_name
                 Fix for JAGae12211
                */
              if ( !sym_class )
                {
                  sym_class = lookup_symbol (copy, 0, VAR_NAMESPACE, 0,
                  			     (struct symtab **) NULL);

                  if (sym_class &&
                    (t = check_typedef (SYMBOL_TYPE (sym_class)),
                      !(TYPE_CODE (t) == TYPE_CODE_STRUCT
                       || TYPE_CODE (t) == TYPE_CODE_UNION
		       || TYPE_CODE (t) == TYPE_CODE_CLASS )))
                          {
                             sym_class = sym_class_preserve;
                          }
                }

	      if (sym_class 
		  && strcmp_iw (SYMBOL_NAME (sym_class), copy)
		  && !strcmp_iwt (SYMBOL_NAME (sym_class), copy))
		{
		  /* Poorva - since strcmp did not match it means that 
		     the 2 are not exactly equal but the fact that 
		     strcmp_iwt matched tells us that we have 
		     Foo and Foo<char> which means the user is trying to 
		     do something like b Foo::bar where Foo is a templated
		     class. We need to find all instances of Foo::bar
		     e.g Foo<int>::bar and Foo<char>::bar */

		  /* check entire name as a symbol */
		  copy = saved_arg2;
		  
		  /* Look for multiple instantiations of the symbol */
		  sym = 0;
		  i1 = 0;                   /*  counter for the symbol array */
		  sym_arr = (struct symbol **)
		    alloca (max_synonyms_seen * sizeof (struct symbol *));
		  symtab_and_block_arr = (struct symtab_and_block *)
		    alloca (max_synonyms_seen * sizeof (struct symtab_and_block));
  		  /* We set the look_for_all parameter as 1 here since we 
		     know that we need to look for all possible instantiations.
		  */
		  i1 = find_functions (copy, sym_arr, symtab_and_block_arr, 1);

		  /* Code after symbol_found expects S, SYM_SYMTAB, SYM,
		     and COPY to be set correctly */
		  if (**argptr
		      && strchr (gdb_completer_quote_characters, **argptr) != NULL)
		    {
		      p = skip_quoted (*argptr);
		      *argptr = *argptr + 1;
		    }
		  else
		    {
		      p = *argptr;
		      while (*p && *p != ' ' && *p != '\t' && *p != ',' && *p != ':')
			p++;
		    }
		  /* no line number may be specified */
		  while (*p == ' ' || *p == '\t')
		    p++;
		  *argptr = p;
		  if (has_if)
		    *if_ptr = ' ';
		  s = (struct symtab *) 0;
		  
		  if (i1 == 1)
		    {
		      /* There is exactly one function with that name.  */
		      sym = sym_arr[0];
		      sym_symtab = symtab_and_block_arr[0].symtab;
		      /* Yes, we have a symbol; jump to symbol processing */
		      goto symbol_found;
		    }
		  else if (i1 > 1)
		    {
		      /* There is more than one functions with that name
			 (overloaded or file-statics).  Ask the user which 
			 one to use.  */
		      return decode_line_2_reuse (sym_arr, i1, funfirstline, 
						  canonical,
						  symtab_and_block_arr, 
						  resolve,
						  source_file, line_number);
		    }
		}

	      if (sym_class && 
		  (t = check_typedef (SYMBOL_TYPE (sym_class)),
		   (TYPE_CODE (t) == TYPE_CODE_STRUCT
		    || TYPE_CODE (t) == TYPE_CODE_UNION 
		    || TYPE_CODE (t) == TYPE_CODE_CLASS)))
		{
		  /* Arg token is not digits => try it as a function name
		     Find the next token(everything up to end or next blank). */
		    
		  if (**argptr
		      && strchr (gdb_completer_quote_characters, **argptr) != NULL)
		    {
		      p = skip_quoted (*argptr);
		      *argptr = *argptr + 1;
		    }
		  else
		    {
		      p = *argptr;
		      while (*p && *p != ' ' && *p != '\t' && *p != ',' && *p != ':')
			p++;
		    }

		  copy = (char *) alloca (p - *argptr + 1);
		  memcpy (copy, *argptr, p - *argptr);
		  copy[p - *argptr] = '\0';
		  if (p != *argptr && copy[p - *argptr - 1]
		      && strchr (gdb_completer_quote_characters,
				 copy[p - *argptr - 1]) != NULL)
		    copy[p - *argptr - 1] = '\0';

		  /* no line number may be specified */
		  while (*p == ' ' || *p == '\t')
		    p++;
		  *argptr = p;

		  sym = 0;
		  i1 = 0;	/*  counter for the symbol array */
		  sym_arr = (struct symbol **) alloca (total_number_of_methods (t)
						* sizeof (struct symbol *));

#ifndef HP_IA64
		  /* Poorva: For IA64 we need to find multiple instances 
		     of the destructors so do it using find_methods */

		  if (destructor_name_p (copy, t))
		    {
		      /* Destructors are a special case.  */
		      int m_index, f_index;

		      if (get_destructor_fn_field (t, &m_index, &f_index))
			{
			  struct fn_field *f = TYPE_FN_FIELDLIST1 (t, m_index);
			  sym_arr[i1] =
                            lookup_symbol (TYPE_FN_FIELD_PHYSNAME (f, f_index),
                                           NULL, VAR_NAMESPACE, (int *) NULL,
                                           (struct symtab **) NULL);
			  if (sym_arr[i1])
			    i1++;
			}
		    }
		  else
#endif
		    i1 = find_methods (t, copy, sym_arr);
		  if (i1 == 1)
		    {
		      /* There is exactly one field with that name.  */
		      sym = sym_arr[0];

		      if (sym && SYMBOL_CLASS (sym) == LOC_BLOCK)
			{
			  values.sals = (struct symtab_and_line *)
			    xmalloc (sizeof (struct symtab_and_line));
			  values.nelts = 1;
			  values.sals[0] = find_function_start_sal (sym,
							      funfirstline);
			}
		      else
			values.nelts = 0;
		      return values;
		    }
		  if (i1 > 0)
		    {
		      /* There is more than one field with that name
		         (overloaded).  Ask the user which one to use.  */
		      return decode_line_2_reuse (sym_arr, i1, funfirstline, 
						  canonical, 
						  /*symtab_and_block_arr== */ 
						    NULL, 
						  resolve, 
						  source_file, 
						  line_number);
		    }
		  else
		    {
		      char *tmp;

		      if (OPNAME_PREFIX_P (copy))
			{
			  tmp = (char *) alloca (strlen (copy + 3) + 10);
			  strcpy (tmp, "operator ");
			  strcat (tmp, copy + 3);
			}
		      else
                        {
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
                          /* To support breakpoints on inlined class functions,
                             constructors etc.
                           */
                          if (inline_debugging == BP_ALL || 
                              inline_debugging == BP_IND)
            	     	    {
                              CORE_ADDR *inline_pc;
                              int inline_count = 0;
                              inline_pc = find_all_inline_instances (arg_name, 
                                                                     source_file,
                                                                     inline_idx, &inline_count);
                              if (inline_count)
                                {
                                  values.sals = (struct symtab_and_line *)
                                    xmalloc (inline_count * sizeof (struct symtab_and_line));
                                  for (int i = 0; i < inline_count; i++)
                                    values.sals[i] = find_pc_name_line (*(inline_pc+i), arg_name);
 
                                  if (inline_pc)
                                    free (inline_pc);
                                  values.nelts =  inline_count;
                                  return values; 
                                }
                            }
#endif

			  tmp = copy;
                        } 
                      values.nelts = 0;
                      return values;
#ifndef HP_IA64
#endif
		    }
		}

	      /* Move pointer up to next possible class/namespace token */
	      p = p2 + 1;	/* restart with old value +1 */
	      /* Move pointer ahead to next double-colon */
	      while (*p && (p[0] != ' ') && (p[0] != '\t') && (p[0] != '\''))
		{
		  if (p[0] == '<')
		    {
		      temp_end = find_template_name_end (p);
		      if (!temp_end)
			error ("malformed template specification in command");
		      p = temp_end;
		    }
		  else if ((p[0] == ':') && (p[1] == ':'))
		    break;	/* found double-colon */
		  else
		    p++;
		}

	      if (*p != ':')
		break;		/* out of the while (1) */

	      p2 = p;		/* save restart for next time around */
	      *argptr = saved_arg2;	/* restore argptr */
	    }			/* while (1) */

	  /* Last chance attempt -- check entire name as a symbol */
	  /* Use "copy" in preparation for jumping out of this block,
	     to be consistent with usage following the jump target */
	  copy = (char *) alloca (p - saved_arg2 + 1);
	  memcpy (copy, saved_arg2, p - saved_arg2);
	  /* Note: if is_quoted should be true, we snuff out quote here anyway */
	  copy[p - saved_arg2] = '\000';
	  /* Set argptr to skip over the name */
	  *argptr = (*p == '\'') ? p + 1 : p;

	  /* Look up entire name */
          sym = lookup_symbol (copy, 0, VAR_NAMESPACE, 0, &sym_symtab);
	  s = (struct symtab *) 0;
	  /* Prepare to jump: restore the " if (condition)" so outer layers see
	     it - since we may return after calling decode_line_2_reuse
	     so do it here. */
	  if (has_if)
	    *if_ptr = ' ';

	  /* Symbol was found --> jump to normal symbol processing.
	     Code following "symbol_found" expects "copy" to have the
	     symbol name, "sym" to have the symbol pointer, "s" to be
	     a specified file's symtab, and sym_symtab to be the symbol's
	     symtab. */
	  /* By jumping there we avoid falling through the FILE:LINE and
	     FILE:FUNC processing stuff below */
	  if (sym)
	  {
#ifdef GDB_TARGET_IS_HPPA
            /* JAGaa80042 - To set breakpoints on template member functions across all 
	       instantiated template class. The IPF implementation will not hold good
	       for PA. So we do it differently. */
	    char *tname = NULL;
	    tname = cplus_demangle (SYMBOL_NAME (sym), 0);
	    if (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_FUNC && is_template (tname))
	    {
		  /* Look for multiple instantiations of the symbol */
		  sym = 0;
		  i1 = 0;                   /*  counter for the symbol array */
		  sym_arr = (struct symbol **)
		    alloca (max_synonyms_seen * sizeof (struct symbol *));
		  symtab_and_block_arr = (struct symtab_and_block *)
		    alloca (max_synonyms_seen * sizeof (struct symtab_and_block));
  		  /* We set the look_for_all parameter as 1 here since we 
		     know that we need to look for all possible instantiations.
		  */
		  i1 = find_functions (copy, sym_arr, symtab_and_block_arr, 1);
		  if (i1 == 1)
		    {
		      /* There is exactly one function with that name.  */
		      sym = sym_arr[0];
		      sym_symtab = symtab_and_block_arr[0].symtab;
		      /* Yes, we have a symbol; jump to symbol processing */
		      goto symbol_found;
		    }
		  else if (i1 > 1)
		    {
		      /* There is more than one functions with that name
			 (overloaded or file-statics).  Ask the user which 
			 one to use.  */
		      return decode_line_2_reuse (sym_arr, i1, funfirstline, 
						  canonical,
						  symtab_and_block_arr, 
						  resolve,
						  source_file, line_number);
		    }
	    }
	    else
#endif
	    	goto symbol_found;
	  }

	  /* Poorva -  April 11th, 2002 - we now call foreach_text_minsym 
	     instead so we can catch all overloaded instances of the method */
#if 0
          /* RM: try once more, as a minimal symbol -- the file
             containing the symbol may have no debug information */
          msymbol = lookup_minimal_symbol (copy, 0, 0);
          if (msymbol)
            goto minimal_symbol_found;
#endif

	  /* Previously for C++ for a file compiled without -g we would
	     find the first matching minsym by calling lookup_minimal_symbol.
	     Now changed to find all matching minsyms and print out 
	     a menu for the user to choose from. Necessary for C++
	     overloaded methods */

	  minsym_count = 0;
	  minsym_and_obj_arr = (struct minsym_and_objfile *)
	    alloca (max_synonyms_seen * sizeof (struct minsym_and_objfile));

	  /* Get the matching minsyms and corresponding objfiles. */
	  minsym_count = foreach_text_minsym (copy, NULL, minsym_and_obj_arr, 0);
	  if (minsym_count == 1)
	    {
	      msymbol = minsym_and_obj_arr[0].minsym;
	      goto minimal_symbol_found;
	    }
	  else if (minsym_count > 1)
	    {
	      /* There is more than one field with that name
		 (overloaded).  Ask the user which one to use.  */
	      return decode_line_2_reuse_minsyms (minsym_and_obj_arr,
						  minsym_count, 
						  funfirstline,
						  canonical,
						  resolve);
	    }
	  
	  /* Couldn't find any interpretation as classes/namespaces, so give up */
	  values.nelts = 0;
	  return values;
	}
      /*  end of C++  */


      /* Extract the file name.  */
      p1 = p;
      while (p != *argptr && p[-1] == ' ')
	--p;
      if ((*p == '"') && is_quote_enclosed)
	--p;
      copy = (char *) alloca (p - *argptr + 1);
      if ((**argptr == '"') && is_quote_enclosed)
	{
	  memcpy (copy, *argptr + 1, p - *argptr - 1);
	  /* It may have the ending quote right after the file name */
	  if (copy[p - *argptr - 2] == '"')
	    copy[p - *argptr - 2] = 0;
	  else
	    copy[p - *argptr - 1] = 0;
	}
      else
	{
	  memcpy (copy, *argptr, p - *argptr);
	  copy[p - *argptr] = 0;
	}

      /* Find that file's data.  */
      s = lookup_symtab (copy);
/* JAGaf70059 - On loading an objdebug compiled fortran executable, for
   <break lineno>,breakpoint is not hit on rerun - <BEGIN>
   If s=0 i.e. if we are not able to get the corresponding symtab for <copy>,
   search for source symtab by looking up the minsym <default_main>. For ex,
   if the fortran program name is <hello>, <default_main = hello>. If there 
   is no fortran program name,then <default_main = __mainprogram.> */
#ifndef HP_IA64
      if (s == 0 && default_symtab == 0 && doom_executable == 1)
	{
	  select_source_symtab (0);
	  s = default_symtab = current_source_symtab;
	  default_line = current_source_line;
	}
#endif
/* JAGaf70059 - <END> */
      if (s == 0)
	{
          /* RM: We may be debugging a stripped executable with
	     unstripped shared libraries. Just because we have no
	     symbols now doesn't mean we'll never have symbols. */
#if 0
          if (!have_full_symbols () && !have_partial_symbols ())
            error (no_symtab_msg);
#endif
          
          /* RM: source file may be in an as yet unloaded shared
	     library. Just return an empty sal, and let the caller
	     deal with it. */
	  /* JYG: MERGE FIXME: tracecmd.exp tests for this message.
	     Suppress only for HPPA.  Should really be #if 0'd out,
	     and fix the test */
#ifndef GDB_TARGET_IS_HPUX
	  error ("No source file named %s.", copy);
#endif
          p++;
          while (*p == ' ' || *p == '\t')
            p++;
          /* RM: do we have a line number? */
          if (*p == '-' ||  *p == '+' ||
              (*p >= '0' && *p <= '9'))
            {
              p++;
              while (*p >= '0' && *p <= '9')
                p++;
              *argptr = p;
            }
          else
            {
              /* Arg token is not digits => try it as a variable name
               * Find the next token (everything up to end or next
               * whitespace).
               */
              if (*p == '$')        /* May be a convenience variable */
                /* One or two $ chars possible */
                p = skip_quoted(p + ((p[1] == '$') ? 2 : 1));
              else if (is_quoted)
                {
                  p = skip_quoted (p);
                  if (p[-1] != '\'')
                    error ("Unmatched single quote.");
                }
              else if (has_parens)
                {
                  p = pp+1;
                }
              else 
                {
                  p = skip_quoted(p);
                }
              *argptr = p;
            }

          values.nelts = 0;
          return values;
	}

      /* Discard the file name from the arg.  */
      p = p1 + 1;
      while (*p == ' ' || *p == '\t')
	p++;
      *argptr = p;
    }
/* #if 0 */
  /* No one really seems to know why this was added. It certainly
     breaks the command line, though, whenever the passed
     name is of the form ClassName::Method. This bit of code
     singles out the class name, and if funfirstline is set (for
     example, you are setting a breakpoint at this function),
     you get an error. This did not occur with earlier
     verions, so I am ifdef'ing this out. 3/29/99 */
  /* JYG: MERGE FIXME: WDB needs this code, so I'm pulling it back in.
     More comments necessary from WDB team */
  else
    {
      /* Comes in here when you have namespaces, parens etc.*/

      char *class_name = NULL, *last_dotdot = NULL;
      struct symbol *class_sym = NULL;
      int look_for_all = 0, local = 0;

      /* Check if what we have till now is a symbol name */

      /* We may be looking at a template instantiation such
         as "foo<int>".  Check here whether we know about it,
         instead of falling through to the code below which
         handles ordinary function names, because that code
         doesn't like seeing '<' and '>' in a name -- the
         skip_quoted call doesn't go past them.  So see if we
         can figure it out right now. */

      if (usings_present || is_namespace)
	copy = arg_name;
      else 
	{
	  copy = (char *) alloca (p - *argptr + 1);
	  memcpy (copy, *argptr, p - *argptr);
	  copy[p - *argptr] = '\000';
	}

      /* Try to see if a user is trying to do break Foo::bar where
	 Foo is a templated class */
      last_dotdot = strrstr (copy, "::");
      if (last_dotdot)
	{
	  class_name = (char *) alloca (last_dotdot - copy + 1);
	  memcpy (class_name, copy, last_dotdot - copy);
	  class_name [last_dotdot - copy] = 0;
	  class_sym = lookup_symbol_1 (class_name, 0, STRUCT_NAMESPACE, 0,
				     (struct symtab **) NULL, &local);
	  /* We use strcmp_iw to check by ignoring space
	     and we use strcmp_iwt to check ignoring space and 
	     template brackets <> so if the check below passes we know
	     that a user is trying to do something like break Foo:bar
	     where Foo is a templated class and so we set look_for_all
	     equal to true.
	  */
	  if (class_sym
	      && strcmp_iw (SYMBOL_NAME (class_sym), class_name)
	      && !strcmp_iwt (SYMBOL_NAME (class_sym), class_name))
	    look_for_all = 1;
	}


      /* Look for multiple overloaded (in case of C++) or static (both for C
	 and C++ definitions of the function symbol found*/
      sym = 0;
      i1 = 0;        /*  counter for the symbol array */
      sym_arr = (struct symbol **) 
		alloca (max_synonyms_seen * sizeof (struct symbol *));
      symtab_and_block_arr = (struct symtab_and_block *) 
		alloca (max_synonyms_seen * sizeof (struct symtab_and_block));

/* JAGaf68403 - Breakpoint at _main_ disabled for +objdebug compiled PA Fortran 
   executables. - <BEGIN> - In case of objdebug compiled Fortran executables ,
   for <break _main_>, search for <_start> also. */ 
#ifndef HP_IA64
if (doom_executable && (strcmp(*argptr,"_main_")==0))
  { 
   copy3 = (char *) alloca (strlen(copy) + 1);
   strcpy (copy3, "_start");
  }
#endif
/* JAGaf68403 - <END> */
#ifdef FORTRAN_ADDS_UNDERSCORE
      if (current_language->la_language == language_fortran)
	{
	  /* For FORTRAN, if it appends an "_" to functions, look for that
	     first.
	     */

	  copy2 = (char *) alloca (strlen(copy) + 2);
	  strcpy (copy2, copy);
	  strcat (copy2, "_");
	  i1 = find_functions (copy2, sym_arr, symtab_and_block_arr, 0);
	}
      else
	i1 = 0;
      if (i1 == 0)
#endif
      i1 = find_functions (copy, sym_arr, symtab_and_block_arr, look_for_all);

/* JAGaf68403 -  Breakpoint at _main_ disabled for +objdebug compiled PA Fortran 
   executables. - <BEGIN> - If <_main_> symtab is not found, find_function for 
   <_start>. */
#ifndef HP_IA64
if ( doom_executable && (i1 == 0))  
  i1 = find_functions (copy3, sym_arr, symtab_and_block_arr, look_for_all);
#endif
/* JAGaf68403 - <END> */

      /* For namespace code we need to replace the argptr and p etc in
	 a way different from the non namespace code. For non namespace
	 code we want to do it only for i1 > 0. We want to do it for 
	 all cases for namespace code. */

      if ((usings_present || is_namespace))
	{
	  if (has_if)
	    *if_ptr = ' ';
	  *argptr = (*p == '\'') ? p + 1 : p;
	  /* Reset argptr and p to the values that you came in with
	   * if it is a statement that makes sense to the debugger.
	   * When we return from here - we check the value
	   * of argptr to see if there is junk at the end of arguments.
	   */
	  
	  if (has_if)
	    *argptr = if_ptr;
	  else 
	    {
	      int n = (int) strlen (*argptr);
	      *argptr += n;
	      if (**argptr == '$')   /* May be a convenience variable */
		p = skip_quoted(*argptr + (((*argptr)[1] == '$') ? 2 : 1));
	      /* One or two $ chars possible */
	      else if (is_quoted)
		{
		  p = skip_quoted (*argptr);
		  if (p[-1] != '\'')
		    error ("Unmatched single quote.");
		}
	      else if (has_parens)
		{
		  p = pp + 1;
		}
	      else
		{
		  p = skip_quoted (*argptr);
		}
	      while (*p == ' ' || *p == '\t')
		p++;
	      *argptr = p;
	    }
	}
      else if ((i1 > 0))
	{
	  if (has_if)
	    *if_ptr = ' ';
	  *argptr = (*p == '\'') ? p + 1 : p;
	}
	

      if (i1 == 1)
        {
	  /* There is exactly one function with that name.  */
	  sym = sym_arr[0];
	  sym_symtab = symtab_and_block_arr[0].symtab;
	  /* Yes, we have a symbol; jump to symbol processing */
	  /* Code after symbol_found expects S, SYM_SYMTAB, SYM, 
	     and COPY to be set correctly */ 
	  s = (struct symtab *) 0;
	  goto symbol_found;
	}
      else if (i1 > 1)
        {
	  /* There is more than one functions with that name
	     (overloaded or file-statics).  Ask the user which one to use.  */
	  return decode_line_2_reuse (sym_arr, i1, funfirstline, canonical, 
				      symtab_and_block_arr, resolve, 
				      source_file, line_number);
	}
      /* Otherwise fall out from here and go to file/line spec
	 processing, etc. */
    }
/* #endif */

  /* S is specified file's symtab, or 0 if no file specified.
     arg no longer contains the file name.  */

  /* Check whether arg is all digits (and sign) */
  /* If usings_present || is_namespace is true then we have mucked with
     the argptr and it is now pointing to the end of the string. 
     arg_name has it stowed away. Use that instead.
   */
  if (usings_present || is_namespace)
    r = q = arg_name;
  else 
    r = q = *argptr;

  if (*q == '-' || *q == '+')
    q++;

  while (*q >= '0' && *q <= '9')
    q++;

  if (q != r && (*q == 0 || *q == ' ' || *q == '\t' || *q == ','))
    {
      /* We found a token consisting of all digits -- at least one digit.  */
      enum sign
	{
	  none, plus, minus
	}
      sign = none;

      /* We might need a canonical line spec if no file was specified.  */
      int need_canonical = (s == 0) ? 1 : 0;

      /* This is where we need to make sure that we have good defaults.
         We must guarantee that this section of code is never executed
         when we are called with just a function name, since
         select_source_symtab calls us with such an argument  */

      if (s == 0 && default_symtab == 0)
	{
	  select_source_symtab (0);
	  default_symtab = current_source_symtab;
	  default_line = current_source_line;
	}

      if (*r == '+') 
        {
	  sign = plus;
          r++;
        }
      else if (*r == '-')
        {
	  sign = minus;
          r++;
        }

      val.line = atoi (r);

      switch (sign)
	{
	case plus:
	  if (q == r)
	    val.line = 5;
	  if (s == 0)
	    val.line = default_line + val.line;
	  break;
	case minus:
	  if (q == r)
	    val.line = 15;
	  if (s == 0)
	    val.line = default_line - val.line;
	  else
	    val.line = 1;
	  break;
	case none:
	  break;		/* No need to adjust val.line.  */
	}

      while (*q == ' ' || *q == '\t')
	q++;
      *argptr = q;
      if (s == 0)
	s = default_symtab;

      /* When multiple instances of C++ template functions house
	 the given line number, try grabbing all of them. */
      i1 = find_n_line_symtabs (s, val.line);
      if (i1 > 1)
        {
	  values.sals = (struct symtab_and_line *)
			xmalloc (i1 * sizeof (struct symtab_and_line));
	  values.is_file_and_line = 1;
	  values.nelts = find_line_symtabs (s, val.line, i1, values.sals);

          /* This is being done becuase of a defect in the coompiler which
             produces confusing line table information for ld. If a file 
             has empty linetable, ld attaches the previous  compilation
             unit's line table. This results in duplicate entries in sal
             array here.
           */
           clean_duplicates(&values.sals, &values.nelts);

          if (need_canonical) 
	    build_canonical_line_spec_n (&values, NULL, canonical);
	}
      else
        {
	  /* It is possible that this source file has more than one symtab, 
	     and that the new line number specification has moved us from the
	     default (in s) to a new one.  */
	  val.symtab = find_line_symtab (s, val.line, NULL, NULL);
	  if (val.symtab == 0)
	    val.symtab = s;
	  val.pc = 0;
	  values.sals = (struct symtab_and_line *)
			xmalloc (sizeof (struct symtab_and_line));
	  values.sals[0] = val;
	  values.nelts = 1;
          if (need_canonical)
	    build_canonical_line_spec (values.sals, NULL, canonical);
	}
      return values;
    }

  /* Arg token is not digits => try it as a variable name
     Find the next token (everything up to end or next whitespace).  */

  if (**argptr == '$')		/* May be a convenience variable */
    p = skip_quoted (*argptr + (((*argptr)[1] == '$') ? 2 : 1));	/* One or two $ chars possible */
  else if (is_quoted)
    {
      p = skip_quoted (*argptr);
      if (p[-1] != '\'')
	error ("Unmatched single quote.");
    }
  else if (has_parens)
    {
      p = pp + 1;
    }
  else
    {
      p = skip_quoted (*argptr);
    }

  if (is_quote_enclosed && **argptr == '"')
    (*argptr)++;

  /* For namespace code we need to replace copy from arg_name */
  if (usings_present || is_namespace)
    copy = arg_name;
  else
    {
      copy = (char *) alloca (p - *argptr + 1);
      memcpy (copy, *argptr, p - *argptr);
      copy[p - *argptr] = '\0';
    
      if (p != *argptr
	  && copy[0]
	  && copy[0] == copy[p - *argptr - 1]
	  && strchr (gdb_completer_quote_characters, copy[0]) != NULL)
	{
	  copy[p - *argptr - 1] = '\0';
	  copy++;
	}
    }
  while (*p == ' ' || *p == '\t')
    p++;
  *argptr = p;

  /* If it starts with $: may be a legitimate variable or routine name
     (e.g. HP-UX millicode routines such as $$dyncall), or it may
     be history value, or it may be a convenience variable */

  if (*copy == '$')
    {
      value_ptr valx;
      int index = 0;
      int need_canonical = 0;

      p = (copy[1] == '$') ? copy + 2 : copy + 1;
      while (*p >= '0' && *p <= '9')
	p++;
      if (!*p)			/* reached end of token without hitting non-digit */
	{
	  /* We have a value history reference */
	  sscanf ((copy[1] == '$') ? copy + 2 : copy + 1, "%d", &index);
	  valx = access_value_history ((copy[1] == '$') ? -index : index);
	  if (TYPE_CODE (VALUE_TYPE (valx)) != TYPE_CODE_INT)
	    error ("History values used in line specs must have integer values.");
	}
      else
	{
	  /* Not all digits -- may be user variable/function or a
	     convenience variable */

	  /* Look up entire name as a symbol first */
	  sym = lookup_symbol (copy, 0, VAR_NAMESPACE, 0, &sym_symtab);
	  /* Symbol was found --> jump to normal symbol processing.
	     Code following "symbol_found" expects "copy" to have the
	     symbol name, "sym" to have the symbol pointer, "s" to be
	     a specified file's symtab, and sym_symtab to be the symbol's
	     symtab. */
	  if (sym)
	    {
	      s = (struct symtab *) 0;
	      goto symbol_found;
	    }

	  /* If symbol was not found, look in minimal symbol tables */
	  msymbol = lookup_minimal_symbol (copy, 0, 0);
	  /* Min symbol was found --> jump to minsym processing. */
	  if (msymbol)
	    {
	      s = (struct symtab *) 0;
	      goto minimal_symbol_found;
	    }

	  /* Not a user variable or function -- must be convenience variable */
	  need_canonical = (s == 0) ? 1 : 0;
	  valx = value_of_internalvar (lookup_internalvar (copy + 1));
	  if (TYPE_CODE (VALUE_TYPE (valx)) != TYPE_CODE_INT)
	    error ("Convenience variables used in line specs must have integer values.");
	}

      /* Either history value or convenience value from above, in valx */
      if (s == 0)
	s = default_symtab;
      val.line = (int) value_as_long (valx);

      /* When multiple instances of C++ template functions house
         the given line number, try grabbing all of them. */
      i1 = find_n_line_symtabs (s, val.line);
      if (i1 > 1)
        {
          values.sals = (struct symtab_and_line *)
                        xmalloc (i1 * sizeof (struct symtab_and_line));
          values.is_file_and_line = 1;
	  values.nelts = find_line_symtabs (s, val.line, i1, values.sals);
          clean_duplicates(&values.sals, &values.nelts);
        }
      else
        {
	  val.symtab = find_line_symtab (s, val.line, NULL, NULL);
	  if (val.symtab == 0)
	    val.symtab = s;
          val.pc = 0;
          values.sals = (struct symtab_and_line *)
                        xmalloc (sizeof (struct symtab_and_line));
          values.sals[0] = val;
          values.nelts = 1;
        }

      if (need_canonical)
	build_canonical_line_spec (values.sals, NULL, canonical);

      return values;
    }


  /* Look up that token as a variable.
     If file specified, use that file's per-file block to start with.  */
 /* If a file is specified look for multiple definitions of the
    function to break at in the file. */
  
  if(s){
    int i1;

    sym = 0;
    i1 = 0;        /*  counter for the symbol array */

    sym_arr = (struct symbol **)
       alloca (max_synonyms_seen * sizeof (struct symbol *));

    symtab_and_block_arr = (struct symtab_and_block *)
       alloca (max_synonyms_seen * sizeof (struct symtab_and_block));

    i1 = find_functions_in_symtab(copy,sym_arr,symtab_and_block_arr,s);
    if (i1==1) {
      /* There is exactly one function with that name.  */
      sym = sym_arr[0];
      sym_symtab = symtab_and_block_arr[0].symtab;
      /* Yes, we have a symbol; jump to symbol processing */
      /* Code after symbol_found expects S, SYM_SYMTAB, SYM,
      and COPY to be set correctly */
      *argptr = (*p == '\'') ? p + 1 : p;
      /* Let 's' stay as it is */
      goto symbol_found;
    }else if(i1>1){
      /* There is more than one functions with that name
       (overloaded or file-statics).  Ask the user which one to use.
      */
      *argptr = (*p == '\'') ? p + 1 : p;
      return decode_line_2_reuse (sym_arr, i1, funfirstline, canonical,
				  symtab_and_block_arr, resolve, 
				  source_file, line_number);
    }
    /* Otherwise fall out from here */
  }

#ifdef FORTRAN_ADDS_UNDERSCORE
  sym = NULL;
  if (current_language->la_language == language_fortran)
    {
      sym = lookup_symbol (copy2,
			   (s ? BLOCKVECTOR_BLOCK (BLOCKVECTOR (s), STATIC_BLOCK)
			    : get_selected_block ()),
			   VAR_NAMESPACE, 0, &sym_symtab);
    }
  if (sym == NULL)
#endif
  sym = lookup_symbol (copy,
		       (s ? BLOCKVECTOR_BLOCK (BLOCKVECTOR (s), STATIC_BLOCK)
			: get_selected_block ()),
		       VAR_NAMESPACE, 0, &sym_symtab);

symbol_found:	/* We also jump here from inside the C++ class/namespace 
		   code on finding a symbol of the form "A::B::C" */

  if (sym != NULL)
    {
      if (SYMBOL_CLASS (sym) == LOC_BLOCK)
	{
          int i = 0;
      	  int inline_count = 0;
          values.sals = (struct symtab_and_line *)
            xmalloc (sizeof (struct symtab_and_line));

#if defined (HP_IA64) && defined (INLINE_SUPPORT)
          /* If inline-debug is set to "inline_bp_all"
             or "inline_bp_individual, there are chances of
             having a mixture of inline and out of line instances. 
             So you need to search the inlinee_table also. 
          */ 
          if (inline_debugging == BP_ALL || inline_debugging == BP_IND)
            {
              CORE_ADDR *inline_pc;
              inline_pc = find_all_inline_instances (copy, source_file, inline_idx, &inline_count);
              if (inline_count)
                {
                  values.sals = (struct symtab_and_line *)
                    xrealloc (values.sals, (inline_count + 1) * sizeof (struct symtab_and_line));
           	  for (i = 0; i < inline_count; i++)
            	    values.sals[i] = find_pc_name_line (*(inline_pc+i), copy);

                  if (inline_pc)
                    free (inline_pc); 
                  values.nelts = inline_count;
                }
            }
#endif
	  /* Arg is the name of a function */
	  values.sals[i] = find_function_start_sal (sym, funfirstline);
	  values.nelts = inline_count + 1;

	  /* Don't use the SYMBOL_LINE; if used at all it points to
	     the line containing the parameters or thereabouts, not
	     the first line of code.  */

	  /* We might need a canonical line spec if it is a static
	     function.  */
	  if (s == 0)
	    {
	 	/* The error was reported in line 7905 but the input bv has a dependency 
		   on line no 7904. So first check if bv != NULL. Sunil Purify Error check. */
	      struct blockvector *bv = (sym_symtab ? BLOCKVECTOR (sym_symtab) : NULL);
	      struct block *b = (bv ? BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK) : NULL);
	      if (lookup_block_symbol (b, copy, VAR_NAMESPACE) != NULL)
		build_canonical_line_spec (values.sals, copy, canonical);
	    }
	  return values;
	}
      /* Diwakar JAGaf48695 1/14/5
       If the found symbol is not a function and we are being asked
       to put breakpoint on the first line of the function, create a
       deferred breakpoint.
      */
      else if (!funfirstline)
	{
            if (SYMBOL_LINE (sym) != 0)
	    {
	      /* We know its line number.  */
	      values.sals = (struct symtab_and_line *)
		xmalloc (sizeof (struct symtab_and_line));
	      values.nelts = 1;
	      memset (&values.sals[0], 0, sizeof (values.sals[0]));
	      values.sals[0].symtab = sym_symtab;
	      values.sals[0].line = SYMBOL_LINE (sym);
	      return values;
	    }
	  else
	    /* This can happen if it is compiled with a compiler which doesn't
	       put out line numbers for variables.  */
	    /* FIXME: Shouldn't we just set .line and .symtab to zero
	       and return?  For example, "info line foo" could print
	       the address.  */
	    error ("Line number not known for symbol \"%s\"", copy);
         }
    }

    /* Bindu: Get the list of minimal symbols that match. If more 
       than 1, ask the user for which one to use. */
    
    is_done = FALSE;
    minsym_and_obj_arr = (struct minsym_and_objfile *)
	alloca (max_synonyms_seen * sizeof (struct minsym_and_objfile));
    name_to_search = copy;
    while (! is_done)
      {
#ifdef FORTRAN_ADDS_UNDERSCORE
        /* For FORTRAN, look for bar_ before bar. */
	if (   current_language->la_language == language_fortran 
	    && name_to_search == copy)
	  name_to_search = copy2;
	else
	  name_to_search = copy;
#endif

	msymbol = NULL;
	minsym_count = 0;
	minsym_count = foreach_text_minsym (name_to_search, 
					    NULL, 
					    minsym_and_obj_arr,
					    0);
	if (minsym_count == 1)
	  {
	    msymbol = minsym_and_obj_arr[0].minsym;
	    goto minimal_symbol_found;
	  }
	else if (minsym_count > 1)
	  {
	    /* There is more than one field with that name
	       (overloaded).  Ask the user which one to use.  */
	    return decode_line_2_reuse_minsyms (minsym_and_obj_arr,
						minsym_count,
						funfirstline,
						canonical,
						resolve);
	  }
#ifdef FORTRAN_ADDS_UNDERSCORE
        if (name_to_search == copy)
	  is_done = TRUE;
#else
	is_done = TRUE;
#endif
      }

minimal_symbol_found:	/* We also jump here from the case for variables
			   that begin with '$' */
#if defined (HP_IA64) && defined (INLINE_SUPPORT)
  /* If we reach here then the given symbol is not yet loaded or
     we are trying to place a breakpoint on inlines. 
  */
  if ((inline_debugging == BP_ALL || inline_debugging == BP_IND)
      && msymbol == NULL)
    {
      int inline_count, i;
      CORE_ADDR *inline_pc;
      struct symtab_and_line sal;
      inline_pc = find_all_inline_instances (copy, source_file, inline_idx, &inline_count);
      if (inline_count)
        {
          values.sals = (struct symtab_and_line *)
            xmalloc (inline_count * sizeof (struct symtab_and_line));
          for (i = 0; i < inline_count; i++)
            values.sals[i] = find_pc_name_line (*(inline_pc+i), copy);
        
          if (inline_pc)
            free (inline_pc);
          values.nelts =  inline_count;
          return values;
        }
    }
#endif

  if (msymbol != NULL)
    {
      values.sals = (struct symtab_and_line *)
	xmalloc (sizeof (struct symtab_and_line));
      values.sals[0] = find_pc_sect_line (SYMBOL_VALUE_ADDRESS (msymbol),
					  (struct sec *) 0, 0);
      values.sals[0].section = SYMBOL_BFD_SECTION (msymbol);
      if (funfirstline)
	{
	  values.sals[0].pc += FUNCTION_START_OFFSET;
	  values.sals[0].pc = SKIP_PROLOGUE (values.sals[0].pc);
	}
      values.nelts = 1;
      return values;
    }

  /* RM: We may be debugging a stripped executable with unstripped
     shared libraries. Just because we have no symbols now doesn't mean
     we'll never have symbols. */
#if 0  
  if (!have_full_symbols () &&
      !have_partial_symbols () && !have_minimal_symbols ())
    error (no_symtab_msg);
#endif

  /* RM: function may be defined in an as yet unloaded shared
     library. Just return an empty sal, and let the caller deal with
     it. */

  values.nelts = 0;
  return values;
}

struct symtabs_and_lines
decode_line_spec (char *string, int funfirstline)
{
  struct symtabs_and_lines sals;
  if (string == 0)
    error ("Empty line specification.");
  sals = decode_line_1 (&string, funfirstline,
			current_source_symtab, current_source_line,
			(char ***) NULL, USER_CHOICE);
  if (*string)
    error ("Junk at end of line specification: %s", string);
  return sals;
}

/* Poorva: Almost a clone of decode_line_2_reuse with stuff done 
   only for minimal symbols. We don't have a corresponding symtab
   or block to go with the minsym just the pc address of the minsyms
   found and objfile it is in. Prints out a menu for the user to 
   choose from. sym_arr is the minsym_and_objfile array. */

static struct symtabs_and_lines
decode_line_2_reuse_minsyms (struct minsym_and_objfile *sym_arr, int nelts,
			     int funfirstline, char ***canonical,
			     enum how_to_resolve_multiple_symbols resolve)
{
  struct symtabs_and_lines values, return_values;
  char *args = NULL, *arg1;
  int idx;
  char buf[2];
  int choice = 0;
  char *prompt;
  char *symname;
  struct cleanup *old_chain;
  char **canonical_arr = (char **) NULL;
  struct symtab_and_line val;

  values.sals = (struct symtab_and_line *)
    alloca (nelts * sizeof (struct symtab_and_line));
  return_values.sals = (struct symtab_and_line *)
    xmalloc (nelts * sizeof (struct symtab_and_line));
  return_values.is_file_and_line = FALSE;
  old_chain = make_cleanup (free, return_values.sals);

  if (canonical)
    {
      canonical_arr = (char **) xmalloc (nelts * sizeof (char *));
      make_cleanup (free, canonical_arr);
      memset (canonical_arr, 0, nelts * sizeof (char *));
      *canonical = canonical_arr;
    }

  /* Choices are [0],[1],..,[nelts+1] */
  /* Initialize the val with the pc addresses of the minsyms found */
  idx= 0;
  while (idx < nelts)
    {
      INIT_SAL (&return_values.sals[idx]);	/* initialize to zeroes */
      INIT_SAL (&values.sals[idx]);
      INIT_SAL (&val);
      val.pc = SYMBOL_VALUE_ADDRESS (sym_arr[idx].minsym);
      val.section = SYMBOL_BFD_SECTION (sym_arr[idx].minsym);
      if (funfirstline)
        {
          val.pc += FUNCTION_START_OFFSET;
          val.pc = SKIP_PROLOGUE (val.pc);
        }
      values.sals[idx] = val;
      idx++;
    }

  if (resolve == USER_CHOICE && choice == 0)
    {
      printf_filtered ("[0] cancel\n[1] all\n");
      idx = 0;
      /* Choices are [0],[1],..,[nelts+1] */
      while (idx < nelts)
	{
	  if (sym_arr[idx].minsym)
	    {
	      enum minimal_symbol_type mst;
	      char *sym_type;

              mst = MSYMBOL_TYPE (sym_arr[idx].minsym);
              if (mst == mst_text)
		sym_type = "global:";
	      else
		sym_type = "";
	      if (sizeof (CORE_ADDR) > 4)
		{
		  CORE_ADDR	addr;
		  unsigned int high_addr;
		  unsigned int low_addr;

		  addr = SYMBOL_VALUE_ADDRESS (sym_arr[idx].minsym);
		  low_addr = (unsigned int) addr;
		  high_addr = (unsigned int) ((unsigned long long) addr >> 32);
		  printf_filtered ("[%d] %s %s at 0x%x%08x in %s\n",
				   (idx + 2),
				   SYMBOL_SOURCE_NAME (sym_arr[idx].minsym),
				   sym_type,
				   high_addr,
				   low_addr,
				   sym_arr[idx].objfile->name
				   );
		}
	      else
		{
		  printf_filtered ("[%d] %s %s at 0x%x in %s\n",
				   (idx + 2),
				   SYMBOL_SOURCE_NAME (sym_arr[idx].minsym),
				   sym_type,
				   SYMBOL_VALUE_ADDRESS (sym_arr[idx].minsym),
				   sym_arr[idx].objfile->name
				   );
		}
	    }
	  else 
	    printf_filtered ("?HERE\n");
	  idx++;
	}
    }

  if (resolve == USER_CHOICE)
    {
      if (choice != 0)
	{
	  sprintf (buf, "%d", choice);
	  args = buf;
	}
      else
	{
	  if ((prompt = getenv ("PS2")) == NULL)
	    {
	      prompt = "> ";
	    }
	  args = command_line_input (prompt, 0, "overload-choice");
	}
    }
  else if (resolve == ALL_SYMBOLS)
    {
      buf[0] = '1';
      buf[1] = 0;
      args = buf;
    }

  if (args == 0 || *args == 0)
    error_no_arg ("one or more choice numbers");

  idx = 0;
  while (*args)
    {
      int num;

      arg1 = args;
      while (*arg1 >= '0' && *arg1 <= '9')
	arg1++;
      if (*arg1 && *arg1 != ' ' && *arg1 != '\t')
	error ("Arguments must be choice numbers.");

      num = atoi (args);

      if (num == 0)
	error ("cancelled");
      else if (num == 1)
	{
	  if (canonical_arr)
	    {
	      for (idx = 0; idx < nelts; idx++)
		{
		  symname = SYMBOL_NAME (sym_arr[idx].minsym);
		  canonical_arr[idx] = savestring (symname, (int) strlen (symname));
		}
	    }
	  
	  memcpy (return_values.sals, values.sals,
		  (nelts * sizeof (struct symtab_and_line)));
	  return_values.nelts = nelts;
	  discard_cleanups (old_chain);
	  return return_values;
	}

      if (num >= nelts + 2)	/* legal choices are [0],[1],..,[nelts+1] */
	{
	  printf_filtered ("No choice number %d.\n", num);
	}
      else
	{
	  num -= 2;
	  /* If the user asks for a particular one - return that minsym */
	  if (values.sals[num].pc)
	    {
	      if (canonical_arr)
		{
		  symname = SYMBOL_NAME (sym_arr[num].minsym);
		  make_cleanup (free, symname);
		  canonical_arr[idx] = savestring (symname, (int) strlen (symname));
		}
	      return_values.sals[idx++] = values.sals[num];
	    }
	  else
	    {
	      printf_filtered ("duplicate request for %d ignored.\n", num);
	    }
	} /* End of else for if (num >= nelts + 2) */

      args = arg1;
      while (*args == ' ' || *args == '\t')
	args++;
    } /* end of while (*args)*/
  return_values.nelts = idx;
  discard_cleanups (old_chain);
  return return_values;
}


/* Given a list of NELTS symbols in SYM_ARR, return a list of lines to
   operate on (ask user if necessary).
   If CANONICAL is non-NULL return a corresponding array of mangled names
   as canonical line specs there.
   If CANONICAL is non-NULL and if SYMTAB_ARR is non-NULL use SYMTAB_ARR 
   to find whether the symbols in SYM_ARR are static and if they are then 
   return canonical line specs of the form "function:filename".

   RESOLVE tells us what to do if with multiple matching symbols. If
   it is set to USER_CHOICE, a menu is popped up for the user to
   choose from. If it is set to ALL_SYMBOLS, all symbols are
   returned. 

   If source_file and line_number are provided and resolve is USER_CHOICE,
   don't ask the user if one of the choices matches the source_file and
   line_number.

   There is a smaller version of this function decode_line_2_reuse_misyms 
   so any changes to this may require changes in the other too.
 */

/* Following macro is used to get a canonical name for the symbol
   in sym_block_arr indexed by num. Canonical name is stored in
   canonical_arr indexed by idx. This is a  very local macro in the
   sense that it is used to elimiinate common code in the following
   function. That is why it has so many parameters. Do not try to 
   use this macro elsewhere without understanding the role of all params.
 */

#define CREATE_CANONICAL_NAME(canonical_arr, idx, symname, values, sym_block_arr, num) { \
    if (sym_block_arr && sym_block_arr[num].symtab)        \
      { \
        char *filename, *canonical_name;               \
                                                           \
        filename = values.sals[num].symtab->filename,  \
        canonical_name = xmalloc ((int) strlen (filename) +  \
                                  (int) strlen (symname) + 2); \
        strcpy (canonical_name, filename);             \
        strcat (canonical_name, ":");                  \
        strcat (canonical_name, symname);              \
        canonical_arr[idx] = canonical_name;             \
      }                                                    \
    else                                                   \
      { /* SYMTAB_AND_BLOCK_ARR is NULL or no SYMTAB */    \
        canonical_arr[idx] =                                 \
                 savestring (symname, (int) strlen (symname)); \
      }                                                    \
    }

static struct symtabs_and_lines
decode_line_2_reuse (struct symbol *sym_arr[], int nelts, int funfirstline, char ***canonical,
		     struct symtab_and_block symtab_and_block_arr[],
		     enum how_to_resolve_multiple_symbols resolve, char *source_file,
		     int line_number)
{
  struct symtabs_and_lines values, return_values;
  char *args = NULL, *arg1;
  int i, got_valid_input = 0;
  char buf[2];
  int choice;
  char *prompt;
  char *symname;
  struct cleanup *old_chain;
  char **canonical_arr = (char **) NULL;

  values.sals = (struct symtab_and_line *)
    alloca (nelts * sizeof (struct symtab_and_line));
  return_values.sals = (struct symtab_and_line *)
    xmalloc (nelts * sizeof (struct symtab_and_line));
  return_values.is_file_and_line = FALSE;
  old_chain = make_cleanup (free, return_values.sals);

  if (canonical)
    {
      canonical_arr = (char **) xmalloc (nelts * sizeof (char *));
      make_cleanup (free, canonical_arr);
      memset (canonical_arr, 0, nelts * sizeof (char *));
      *canonical = canonical_arr;
    }

  /* Choices are [0],[1],..,[nelts+1] */
  i = 0;
  boolean dont_show_the_darned_menu = false;

  dont_show_the_darned_menu = skip_bp_menu;

  /* Srikanth, Dec 10th 2002, Sometimes we get here with duplicate
     choices. This seems to happen with multiple instantiations of
     the same template. See if we have a real choice, if not one is
     as good as the other, don't show the menu.

     Reported by Intel.
  */
  if (resolve == USER_CHOICE)
    {
      int j, k;

      while (i < nelts)
        {
          INIT_SAL (&values.sals[i]);
	  values.sals[i] = find_function_start_sal (sym_arr[i], funfirstline);
          i++;
        }
       
       for (j = 0; j < i; j++)
         {
           for (k = j + 1; k < i; k++)
             {
	       if (values.sals[j].line != values.sals[k].line)
                 goto valid_choice;
               if (strcmp(SYMBOL_NAME (sym_arr[j]), SYMBOL_NAME (sym_arr[k])))
                 goto valid_choice;
               if (strcmp(values.sals[j].symtab->filename, values.sals[k].symtab->filename))
                 goto valid_choice;
             }
         }
      /* We get here only with identical choices. Just pick one. */
      dont_show_the_darned_menu = true;
    }

valid_choice:
  i = 0;

  while (i < nelts)
    {
      INIT_SAL (&return_values.sals[i]);	/* initialize to zeroes */
      INIT_SAL (&values.sals[i]);
      if (sym_arr[i] && SYMBOL_CLASS (sym_arr[i]) == LOC_BLOCK)
	{
	  values.sals[i] = find_function_start_sal (sym_arr[i], funfirstline);
	}
      i++;
    }

  /* If one of the choices matches the symbol file and the line number,
     user that choice and don't ask the user.
     */

  choice = 0;  /* If we don't find a matching choice it will be 0. */
  if (source_file != NULL && line_number != 0)
    for (i = 0;  i < nelts;  i++) 
      {
	if (   values.sals[i].line == line_number
	    && strcmp (values.sals[i].symtab->filename, source_file) == 0)
	  {
	    choice = i + 2;  /* 0 and 1 are cancel and all.  The 2 we add
				here is subtracted below.
				*/
	    break;  /* We found a match */
	  }
      }

  got_valid_input = 0;
  while (!got_valid_input)
    {
      if (resolve == USER_CHOICE && !dont_show_the_darned_menu && choice == 0)
        {
          /* Choices are [0],[1],..,[nelts+1] */
          printf_filtered ("[0] cancel\n[1] all\n");

          i = 0;

          while (i < nelts)
	    {
	      if (sym_arr[i] && SYMBOL_CLASS (sym_arr[i]) == LOC_BLOCK)
	        {
	          char *sym_name = NULL;

	          if ((current_language->la_language == language_cplus)
		      && namespace_enabled)
		    {
		      char *using_name = SYMBOL_USING_DECL_NAME (sym_arr[i]);

		      /* If using_decl_name is present use that since that has
		         the actual name of the symbol. This symbol is just a clone
		         of the actual one.
		      */
		      if (using_name)
		        {
		          /* Demangle it first since it will be mangled */
		          if (is_cplus_name (using_name, 1)) /* JAGaf49121 */
			    sym_name = cplus_demangled_name;
		          else
			    sym_name = using_name;
		        }
		      else
		        sym_name = SYMBOL_SOURCE_NAME (sym_arr[i]);
		    }
	          else
		    sym_name = SYMBOL_SOURCE_NAME (sym_arr[i]);

	          printf_filtered ("[%d] %s at %s:%d\n",
			     (i + 2),
			     sym_name,
			     values.sals[i].symtab->filename,
			     values.sals[i].line,
			     (symtab_and_block_arr &&
			     symtab_and_block_arr[i].block_number == STATIC_BLOCK) ?
			     " (STATIC)" : "");
	        }
	      else 
	        printf_filtered ("?HERE\n");
	      i++;
	    }
        }

      if (resolve == USER_CHOICE)
        {
          if (choice != 0)
	    {
	      sprintf (buf, "%d", choice);
	      args = buf;
	    }
          else
	    {
	      if ((prompt = getenv ("PS2")) == NULL)
	        {
	          prompt = "> ";
	        }
	      if (!dont_show_the_darned_menu)
	        args = command_line_input (prompt, 0, "overload-choice");
	      else
                if (!skip_bp_menu)
	          args = "2";
                else
                 args = "1";
	    }
        }
      else if (resolve == ALL_SYMBOLS)
        {
          buf[0] = '1';
          buf[1] = 0;
          args = buf;
        }

      if (args == 0 || *args == 0)
        error_no_arg ("one or more choice numbers");

      i = 0;

      while (*args)
        {
          int num;

          arg1 = args;
          while (*arg1 >= '0' && *arg1 <= '9')
	    arg1++;
          if (*arg1 && *arg1 != ' ' && *arg1 != '\t')
	    error ("Arguments must be choice numbers.");

          num = atoi (args);

          if (num == 0) {
	    error ("cancelled");
            got_valid_input = 1;
          }
          else if (num == 1)
	    {
	      if (canonical_arr)
	        {
	          for (i = 0; i < nelts; i++)
		    {
		      if (canonical_arr[i] == NULL)
		        {
		          symname = SYMBOL_NAME (sym_arr[i]);
			  CREATE_CANONICAL_NAME(canonical_arr, i, symname, 
                                              values, symtab_and_block_arr, i);
		        }
		    }
	        }

	      memcpy (return_values.sals, values.sals,
		  (nelts * sizeof (struct symtab_and_line)));
	      return_values.nelts = nelts;
	      discard_cleanups (old_chain);
	      return return_values;
	    }

          if (num >= nelts + 2)	/* legal choices are [0],[1],..,[nelts+1] */
	    {
	      printf_filtered ("No choice number %d.\n", num);
	    }
          else
	    {
	      num -= 2;
	      if (values.sals[num].pc)
	        {
	          if (canonical_arr)
		    {
		      symname = SYMBOL_NAME (sym_arr[num]);
		      make_cleanup (free, symname);
		      canonical_arr[i] = 
                          savestring (symname, (int) strlen (symname));
                      CREATE_CANONICAL_NAME(canonical_arr, i, symname, 
                                            values, symtab_and_block_arr, num);
		    }
	          return_values.sals[i++] = values.sals[num];
	          values.sals[num].pc = 0;
		  got_valid_input = 1;
	        }
	      else
	        {
	          printf_filtered ("duplicate request for %d ignored.\n", num);
	        }
	    }

          args = arg1;
          while (*args == ' ' || *args == '\t')
	    args++;
       }
    } /* while (!got_valid_input) .. */

  return_values.nelts = i;
  discard_cleanups (old_chain);
  return return_values;
}



/* Slave routine for sources_info.  Force line breaks at ,'s.
   NAME is the name to print and *FIRST is nonzero if this is the first
   name printed.  Set *FIRST to zero.  */
static void
output_source_filename (char *name, int *first)
{
  /* Table of files printed so far.  Since a single source file can
     result in several partial symbol tables, we need to avoid printing
     it more than once.  Note: if some of the psymtabs are read in and
     some are not, it gets printed both under "Source files for which
     symbols have been read" and "Source files for which symbols will
     be read in on demand".  I consider this a reasonable way to deal
     with the situation.  I'm not sure whether this can also happen for
     symtabs; it doesn't hurt to check.  */
  static char **tab = NULL;
  /* Allocated size of tab in elements.
     Start with one 256-byte block (when using GNU malloc.c).
     24 is the malloc overhead when range checking is in effect.  */
  static int tab_alloc_size = (256 - 24) / sizeof (char *);
  /* Current size of tab in elements.  */
  static int tab_cur_size;

  char **p;

  if (*first)
    {
      if (tab == NULL)
	tab = (char **) xmalloc (tab_alloc_size * sizeof (*tab));
      tab_cur_size = 0;
    }

  /* Is NAME in tab?  */
  for (p = tab; p < tab + tab_cur_size; p++)
    if (STREQ (*p, name))
      /* Yes; don't print it again.  */
      return;
  /* No; add it to tab.  */
  if (tab_cur_size == tab_alloc_size)
    {
      tab_alloc_size *= 2;
      tab = (char **) xrealloc ((char *) tab, tab_alloc_size * sizeof (*tab));
    }
  tab[tab_cur_size++] = name;

  if (*first)
    {
      *first = 0;
    }
  else
    {
      printf_filtered (", ");
    }

  wrap_here ("");
  fputs_filtered (name, gdb_stdout);
}

static void
sources_info (char *ignore, int from_tty)
{
  register struct symtab *s;
  register struct partial_symtab *ps;
  register struct objfile *objfile;
  int first;

  if (!have_full_symbols () && !have_partial_symbols ())
    {
      error (no_symtab_msg);
    }

  printf_filtered ("Source files for which symbols have been read in:\n\n");

  first = 1;
  ALL_SYMTABS (objfile, s)
  {
    output_source_filename (s->filename, &first);
  }
  printf_filtered ("\n\n");

  printf_filtered ("Source files for which symbols will be read in on demand:\n\n");

  first = 1;
  ALL_PSYMTABS (objfile, ps)
  {
    if (!ps->readin)
      {
	output_source_filename (ps->filename, &first);
      }
  }
  printf_filtered ("\n");
}

static void
extract_leading_id (char *input, char *output)
{
    int i, j;

    i = j = 0;

    output[0] = 0;

    while (input[i] && !isalnum(input[i]) && input[i] != '_')
      i++;

    while(input[i] && (isalnum(input[i]) || input[i] == '_'))
      output[j++] = input[i++]; 
      
    output[j] = 0;

}

static int
file_matches (char *file, char *files[], int nfiles)
{
  int i;

  if (file != NULL && nfiles != 0)
    {
      for (i = 0; i < nfiles; i++)
	{
	  if (strcmp (files[i], getbasename (file)) == 0)
	    return 1;
	}
    }
  else if (nfiles == 0)
    return 1;
  return 0;
}

/* Free any memory associated with a search. */
void
free_search_symbols (struct symbol_search *symbols)
{
  struct symbol_search *p;
  struct symbol_search *next;

  for (p = symbols; p != NULL; p = next)
    {
      next = p->next;
      free (p);
    }
}

static void
do_free_search_symbols_cleanup (void *symbols)
{
  free_search_symbols (symbols);
}

struct cleanup *
make_cleanup_free_search_symbols (struct symbol_search *symbols)
{
  return make_cleanup (do_free_search_symbols_cleanup, symbols);
}


/* srikanth, 000208, the following is to make sure that we don't drown
   the GUI function browser with a flood of irrelevant msymbols.
   The GUI is expected to toggle this off and turn it back on after
   down loading data.
*/
static int include_non_debug_symbols = 1;
static int max_function_matches = 0;

/* Search the symbol table for matches to the regular expression REGEXP,
   returning the results in *MATCHES.

   Only symbols of KIND are searched:
   FUNCTIONS_NAMESPACE - search all functions
   TYPES_NAMESPACE     - search all type names
   METHODS_NAMESPACE   - search all methods NOT IMPLEMENTED
   VARIABLES_NAMESPACE - search all symbols, excluding functions, type names,
   and constants (enums)

   free_search_symbols should be called when *MATCHES is no longer needed.
 */
void
search_symbols (char *regexp, namespace_enum kind, int nfiles, char *files[],
		struct symbol_search **matches)
{
  register struct symtab *s;
  register struct partial_symtab *ps;
  register struct blockvector *bv;
  struct blockvector *prev_bv = 0;
  register struct block *b;
  register int i = 0;
  register int j;
  extern int firebolt_version;
  extern int server_command;
  register struct symbol *sym;
  struct partial_symbol **psym;
  struct objfile *objfile;
  struct minimal_symbol *msymbol;
  char *val;
  int found_misc = 0;
  int matches_found = 0;
  static enum minimal_symbol_type types[]
  =
  {mst_data, mst_text, mst_abs, mst_unknown};
  static enum minimal_symbol_type types2[]
  =
  {mst_bss, mst_file_text, mst_abs, mst_unknown};
  static enum minimal_symbol_type types3[]
  =
  {mst_file_data, mst_solib_trampoline, mst_abs, mst_unknown};
  static enum minimal_symbol_type types4[]
  =
  {mst_file_bss, mst_text, mst_abs, mst_unknown};
  enum minimal_symbol_type ourtype;
  enum minimal_symbol_type ourtype2;
  enum minimal_symbol_type ourtype3;
  enum minimal_symbol_type ourtype4;
  struct symbol_search *sr;
  struct symbol_search *psr;
  struct symbol_search *tail;
  struct cleanup *old_chain = NULL;
  extern int lazy_demangling;
  char * pure_text;

  if (kind <= LABEL_NAMESPACE)
    error ("must search on specific namespace");

  /* srikanth, JAGad40654 rbreak and info func do not work as
     the types are off by one. See symtab.h for the enumeration.
  */
  ourtype = types[(int) (kind - LABEL_NAMESPACE - 1)];
  ourtype2 = types2[(int) (kind - LABEL_NAMESPACE - 1)];
  ourtype3 = types3[(int) (kind - LABEL_NAMESPACE - 1)];
  ourtype4 = types4[(int) (kind - LABEL_NAMESPACE - 1 )];

  sr = *matches = NULL;
  tail = NULL;

  if (regexp != NULL)
    {
      /* Make sure spacing is right for C++ operators.
         This is just a courtesy to make the matching less sensitive
         to how many spaces the user leaves between 'operator'
         and <TYPENAME> or <OPERATOR>. */
      char *opend;
      char *opname = operator_chars (regexp, &opend);
      if (*opname)
	{
	  int fix = -1;		/* -1 means ok; otherwise number of spaces needed. */
	  if (isalpha (*opname) || *opname == '_' || *opname == '$')
	    {
	      /* There should 1 space between 'operator' and 'TYPENAME'. */
	      if (opname[-1] != ' ' || opname[-2] == ' ')
		fix = 1;
	    }
	  else
	    {
	      /* There should 0 spaces between 'operator' and 'OPERATOR'. */
	      if (opname[-1] == ' ')
		fix = 0;
	    }
	  /* If wrong number of spaces, fix it. */
	  if (fix >= 0)
	    {
	      char *tmp = (char *) alloca (opend - opname + 10);
	      sprintf (tmp, "operator%.*s%s", fix, " ", opname);
	      regexp = tmp;
	    }
	}

      if (0 != (val = re_comp (regexp)))
	error ("Invalid regexp (%s): %s", val, regexp);
    }

  /* srikanth, when just in time demangling is in effect, a command like
     rbreak Map<NSTail::Set *> may fail because, the corresponding symbols
     may exist only in their mangled form. The workaround is to extract the
     leading pure text pattern from the regexp, and demangle those symbols
     that contain the pattern. */

  pure_text = 0; /* initialize for compiler warning */
  if (lazy_demangling)
    {
      pure_text = (char *) alloca (regexp ? strlen(regexp) + 1 : 1);
      pure_text[0] = 0;
      if (regexp)
	extract_leading_id(regexp, pure_text);
    }

    
  /* Search through the partial symtabs *first* for all symbols
     matching the regexp.  That way we don't have to reproduce all of
     the machinery below. */
#ifdef HASH_ALL
    if (kind == TYPES_NAMESPACE)
      {
        ALL_PSYMTABS (objfile, ps)
	  PSYMTAB_TO_SYMTAB (ps);
      }
#else
  ALL_PSYMTABS (objfile, ps)
  {
    struct partial_symbol **bound, **gbound, **sbound;
    int keep_going = 0;

    QUIT;
    if (ps->readin)  
     continue;
 
#ifndef HP_IA64
  /* JAGae38893 - Info types not working */
  /* Use two loops to find the global and static symbols */

    for (psym = objfile->global_psymbols.list + ps->globals_offset;
	 psym < (objfile->global_psymbols.list + ps->globals_offset
		 + ps->n_global_syms); psym++)
     {
       
      if (file_matches (ps->filename, files, nfiles)
   	   && ((regexp == NULL || SYMBOL_MATCHES_REGEXP (*psym))
           && ((kind == VARIABLES_NAMESPACE && PSYMBOL_CLASS (*psym) != LOC_TYPEDEF
           && PSYMBOL_CLASS (*psym) != LOC_BLOCK)
    	   || (kind == FUNCTIONS_NAMESPACE && PSYMBOL_CLASS (*psym) == LOC_BLOCK)
	   || (kind == TYPES_NAMESPACE && ((PSYMBOL_CLASS (*psym) == LOC_TYPEDEF) 
	            || (PSYMBOL_CLASS (*psym) == LOC_STATIC))) /* check for static symbol also */
   	   || (kind == METHODS_NAMESPACE && PSYMBOL_CLASS (*psym) == LOC_BLOCK))))
             {
		     ps->psymbol_to_expand = *psym;
		     PSYMTAB_TO_SYMTAB (ps);
		     keep_going = 1;
	     }
     }

    for (psym = objfile->static_psymbols.list + ps->statics_offset;
	 psym < (objfile->static_psymbols.list + ps->statics_offset
		 + ps->n_static_syms); psym++)
     {
	if (file_matches (ps->filename, files, nfiles)
   	   && ((regexp == NULL || SYMBOL_MATCHES_REGEXP (*psym))
           && ((kind == VARIABLES_NAMESPACE && PSYMBOL_CLASS (*psym) != LOC_TYPEDEF
           && PSYMBOL_CLASS (*psym) != LOC_BLOCK)
    	   || (kind == FUNCTIONS_NAMESPACE && PSYMBOL_CLASS (*psym) == LOC_BLOCK)
	   || (kind == TYPES_NAMESPACE && ((PSYMBOL_CLASS (*psym) == LOC_TYPEDEF)
	            || (PSYMBOL_CLASS (*psym) == LOC_STATIC))) /* JAGae38893 - check for static symbols also */ 
	   || (kind == METHODS_NAMESPACE && PSYMBOL_CLASS (*psym) == LOC_BLOCK))))  
           {
	           ps->psymbol_to_expand = *psym;
		   PSYMTAB_TO_SYMTAB (ps);
		   keep_going = 1;
	   }
       }
       if (keep_going)    /* If the psymtab is converted into symtab set psymtab->readin as 1. */
         ps->readin = 1;
      	 
   }  		 

#else  /* HP_IA64 */
    gbound = objfile->global_psymbols.list + ps->globals_offset + ps->n_global_syms;
    sbound = objfile->static_psymbols.list + ps->statics_offset + ps->n_static_syms;
    bound = gbound;

    /* Go through all of the symbols stored in a partial
       symtab in one loop. */
    psym = objfile->global_psymbols.list + ps->globals_offset;
    while (keep_going)
      {
	if (psym >= bound)
	  {
	    if (bound == gbound && ps->n_static_syms != 0)
	      {
		psym = objfile->static_psymbols.list + ps->statics_offset;
		bound = sbound;
	      }
	    else
	      keep_going = 0;
	    continue;
	  }
	else
	  {
	    QUIT;

	    /* If it would match (logic taken from loop below)
	       load the file and go on to the next one */
           if (file_matches (ps->filename, files, nfiles)
		&& ((regexp == NULL || SYMBOL_MATCHES_REGEXP (*psym))
		    && ((kind == VARIABLES_NAMESPACE && PSYMBOL_CLASS (*psym) != LOC_TYPEDEF
			 && PSYMBOL_CLASS (*psym) != LOC_BLOCK)
			|| (kind == FUNCTIONS_NAMESPACE && PSYMBOL_CLASS (*psym) == LOC_BLOCK)
			|| (kind == TYPES_NAMESPACE && (PSYMBOL_CLASS (*psym) == LOC_TYPEDEF)                                                    || (PSYMBOL_CLASS (*psym) == LOC_STATIC))
			|| (kind == METHODS_NAMESPACE && PSYMBOL_CLASS (*psym) == LOC_BLOCK))))
	      {
                ps->psymbol_to_expand = *psym;
		sym = PSYMTAB_TO_SYMTAB (ps);
		keep_going = 0;
	      }
	  }
	psym++;
      }
  }
#endif
#endif

  /* Here, we search through the minimal symbol tables for functions
     and variables that match, and force their symbols to be read.
     This is in particular necessary for demangled variable names,
     which are no longer put into the partial symbol tables.
     The symbol will then be found during the scan of symtabs below.

     For functions, find_pc_symtab should succeed if we have debug info
     for the function, for variables we have to call lookup_symbol
     to determine if the variable has debug info.
     If the lookup fails, set found_misc so that we will rescan to print
     any matching symbols without debug info.
   */

  if (nfiles == 0 && (kind == VARIABLES_NAMESPACE || kind == FUNCTIONS_NAMESPACE))
    {
      ALL_MSYMBOLS (objfile, msymbol)
      {
	QUIT;
	if (MSYMBOL_TYPE (msymbol) == ourtype ||
	    MSYMBOL_TYPE (msymbol) == ourtype2 ||
	    MSYMBOL_TYPE (msymbol) == ourtype3 ||
	    MSYMBOL_TYPE (msymbol) == ourtype4)
	  {
	    if (lazy_demangling && pure_text[0] && 
		strstr(SYMBOL_NAME(msymbol), pure_text))
	      if (SYMBOL_LANGUAGE(msymbol) == language_auto)
		SYMBOL_INIT_DEMANGLED_NAME(msymbol, &objfile->symbol_obstack);
 
	    if (regexp == NULL || SYMBOL_MATCHES_REGEXP (msymbol))
	      {
		if (0 == find_pc_symtab (SYMBOL_VALUE_ADDRESS (msymbol)))
		  {
		    if (kind == FUNCTIONS_NAMESPACE
			|| lookup_symbol (SYMBOL_NAME (msymbol),
					  (struct block *) NULL,
					  VAR_NAMESPACE,
					0, (struct symtab **) NULL) == NULL)
		      found_misc = 1;
		  }
	      }
	  }
      }
      /* RM: allow user to quit */
      QUIT;
    }

  ALL_SYMTABS (objfile, s)
  {
    QUIT;
    bv = BLOCKVECTOR (s);
    /* Often many files share a blockvector.
       Scan each blockvector only once so that
       we don't get every symbol many times.
       It happens that the first symtab in the list
       for any given blockvector is the main file.  */
    if (bv != prev_bv) 
      for (i = GLOBAL_BLOCK; i <= STATIC_BLOCK; i++)
	{
	  b = BLOCKVECTOR_BLOCK (bv, i);
	  /* Skip the sort if this block is always sorted.  */
	  if (!BLOCK_SHOULD_SORT (b))
	    sort_block_syms (b);
	  for (j = 0; j < BLOCK_NSYMS (b); j++)
	    {
	      QUIT;
	      sym = BLOCK_SYM (b, j);
	      if (file_matches (s->filename, files, nfiles)
		  && ((regexp == NULL || SYMBOL_MATCHES_REGEXP (sym))
		      && ((kind == VARIABLES_NAMESPACE && SYMBOL_CLASS (sym) != LOC_TYPEDEF
			   && SYMBOL_CLASS (sym) != LOC_BLOCK
			   && SYMBOL_CLASS (sym) != LOC_CONST)
			  || (kind == FUNCTIONS_NAMESPACE && SYMBOL_CLASS (sym) == LOC_BLOCK)
			  || (kind == TYPES_NAMESPACE && ((SYMBOL_CLASS (sym) == LOC_TYPEDEF)
			              || (SYMBOL_CLASS (sym) == LOC_STATIC))) /* JAGae38893 - Info types - check for static symbols also */
			  || (kind == METHODS_NAMESPACE && SYMBOL_CLASS (sym) == LOC_BLOCK))))
		{
		  /* match */
                  matches_found++;
                  if (max_function_matches && 
                        matches_found > max_function_matches)
                    /* Srikanth, we used to return from here, but that is a bug
                       resulting from the merge with Cygnus 5.0.
                    */
                    goto all_done;

		  psr = (struct symbol_search *) xmalloc (sizeof (struct symbol_search));
		  psr->block = i;
		  psr->symtab = s;
		  psr->symbol = sym;
		  psr->msymbol = NULL;
		  psr->next = NULL;
		  if (tail == NULL)
		    {
		      sr = psr;
		      old_chain = make_cleanup_free_search_symbols (sr);
		    }
		  else
		    tail->next = psr;
		  tail = psr;
		}
	    }
	}
    prev_bv = bv;
  }

  /* If there are no eyes, avoid all contact.  I mean, if there are
     no debug symbols, then print directly from the msymbol_vector.  */

  if (include_non_debug_symbols && (found_misc || kind != FUNCTIONS_NAMESPACE))
    {
      ALL_MSYMBOLS (objfile, msymbol)
      {
	QUIT;
	if (MSYMBOL_TYPE (msymbol) == ourtype ||
	    MSYMBOL_TYPE (msymbol) == ourtype2 ||
	    MSYMBOL_TYPE (msymbol) == ourtype3 ||
	    MSYMBOL_TYPE (msymbol) == ourtype4)
	  {
	    if (lazy_demangling && pure_text[0] && 
		strstr(SYMBOL_NAME(msymbol), pure_text))
	      if (SYMBOL_LANGUAGE(msymbol) == language_auto)
		SYMBOL_INIT_DEMANGLED_NAME(msymbol, &objfile->symbol_obstack);

	    if (regexp == NULL || SYMBOL_MATCHES_REGEXP (msymbol))
	      {
		/* Functions:  Look up by address. */
		if (kind != FUNCTIONS_NAMESPACE ||
		    (0 == find_pc_symtab (SYMBOL_VALUE_ADDRESS (msymbol))))
		  {
		    /* Variables/Absolutes:  Look up by name */
		    if (lookup_symbol (SYMBOL_NAME (msymbol),
				       (struct block *) NULL, VAR_NAMESPACE,
				       0, (struct symtab **) NULL) == NULL)
		      {
			/* match */
			psr = (struct symbol_search *) xmalloc (sizeof (struct symbol_search));
			psr->block = i;
			psr->msymbol = msymbol;
			psr->symtab = NULL;
			psr->symbol = NULL;
			psr->next = NULL;
			if (tail == NULL)
			  {
			    sr = psr;
			    old_chain = make_cleanup_free_search_symbols (sr);
			  }
			else
			  tail->next = psr;
			tail = psr;
		      }
		  }
	      }
	  }
      }
    }

all_done:
  *matches = sr;
  if (sr != NULL)
    discard_cleanups (old_chain);
}


/* This help function for symtab_symbol_info() prints information
   for function symbols when running under firebolt.

   The regular info func command generates voluminous output most
    of which is unneeded and complicated to parse.
 */
static void
print_func_info_for_firebolt (struct symtab *s, struct symbol *sym)
{
  struct symtab_and_line sal;
  struct symtabs_and_lines sals;
  char * symbol_name;
  int i;

  if (!sym || !s)
    return;

  symbol_name = SYMBOL_NAME(sym);
  if (symbol_name == 0 || symbol_name[0] == 0)
    return;

  sals = decode_line_1 (&symbol_name, 0, s, 0, 0, ALL_SYMBOLS);
  if (sals.nelts == 0)
    return;

  for (i=0; i < sals.nelts; i++)
    {
      sal =  sals.sals[i];
      printf_filtered ("File: %s Line: %d %s\n", s->filename, sal.line,
                      SYMBOL_SOURCE_NAME(sym));
    }
}



/* Helper function for symtab_symbol_info, this function uses
   the data returned from search_symbols() to print information
   regarding the match to gdb_stdout.
 */
static void
print_symbol_info (namespace_enum kind, struct symtab *s, struct symbol *sym, int block, char *last)
{
  extern int firebolt_version;
  extern int server_command;

  if (firebolt_version && server_command && kind == FUNCTIONS_NAMESPACE)
    {
      /* Print in a simplified format for Firebolt. */
      print_func_info_for_firebolt(s, sym);
      return;
    }

  if (last == NULL || strcmp (last, s->filename) != 0)
    {
      fputs_filtered ("\nFile ", gdb_stdout);
      fputs_filtered (s->filename, gdb_stdout);
      fputs_filtered (":\n", gdb_stdout);
    }

  if (kind != TYPES_NAMESPACE && block == STATIC_BLOCK)
    printf_filtered ("static ");

  /* Typedef that is not a C++ class */
  if (kind == TYPES_NAMESPACE
      && SYMBOL_NAMESPACE (sym) != STRUCT_NAMESPACE)
    c_typedef_print (SYMBOL_TYPE (sym), sym, gdb_stdout);
  /* variable, func, or typedef-that-is-c++-class */
  else if (kind < TYPES_NAMESPACE ||
	   (kind == TYPES_NAMESPACE &&
	    SYMBOL_NAMESPACE (sym) == STRUCT_NAMESPACE))
    {
      type_print (SYMBOL_TYPE (sym),
		  (SYMBOL_CLASS (sym) == LOC_TYPEDEF
		   ? "" : SYMBOL_SOURCE_NAME (sym)),
		  gdb_stdout, 0);

      printf_filtered (";");
      /* JAGag45793: Specify that the var is a declaration in the file. */
      if (!block && SYMBOL_CLASS (sym) == LOC_UNRESOLVED)
        printf_filtered (" Declaration\n");
      else
        printf_filtered ("\n");

    }
  else
    {
#if 0
      /* Tiemann says: "info methods was never implemented."  */
      char *demangled_name;
      c_type_print_base (TYPE_FN_FIELD_TYPE (t, block),
			 gdb_stdout, 0, 0);
      c_type_print_varspec_prefix (TYPE_FN_FIELD_TYPE (t, block),
				   gdb_stdout, 0);
      if (TYPE_FN_FIELD_STUB (t, block))
	check_stub_method (TYPE_DOMAIN_TYPE (type), j, block);
      demangled_name =
	cplus_demangle (TYPE_FN_FIELD_PHYSNAME (t, block),
			DMGL_ANSI | DMGL_PARAMS);
      if (demangled_name == NULL)
	fprintf_filtered (stream, "<badly mangled name %s>",
			  TYPE_FN_FIELD_PHYSNAME (t, block));
      else
	{
	  fputs_filtered (demangled_name, stream);
	  free (demangled_name);
	}
#endif
    }

}

/* This help function for symtab_symbol_info() prints information
   for non-debugging symbols to gdb_stdout.
 */
static void
print_msymbol_info (struct minimal_symbol *msymbol)
{

  /* srikanth, we need to use longest_* so as to avoid truncating
     32 bits out of the symbol's address. 001114
  */

  printf_filtered ("	%s  %s\n",
           longest_local_hex_string_custom (
             (LONGEST) SYMBOL_VALUE_ADDRESS (msymbol), "08l"),
		   SYMBOL_SOURCE_NAME (msymbol));
}

/* This is the guts of the commands "info functions", "info types", and
   "info variables". It calls search_symbols to find all matches and then
   print_[m]symbol_info to print out some useful information about the
   matches.
 */
static void
symtab_symbol_info (char *regexp, namespace_enum kind, int from_tty)
{
  static char *classnames[]
  =
  {"variable", "function", "type", "method"};
  struct symbol_search *symbols;
  struct symbol_search *p;
  struct cleanup *old_chain;
  char *last_filename = NULL;
  int first = 1;

  /* must make sure that if we're interrupted, symbols gets freed */
  search_symbols (regexp, kind, 0, (char **) NULL, &symbols);
  old_chain = make_cleanup_free_search_symbols (symbols);

  printf_filtered (regexp
		   ? "All %ss matching regular expression \"%s\":\n"
		   : "All defined %ss:\n",
		   classnames[(int) (kind - LABEL_NAMESPACE - 1)], regexp);

  for (p = symbols; p != NULL; p = p->next)
    {
      QUIT;

      if (p->msymbol != NULL)
	{
	  if (first)
	    {
	      printf_filtered ("\nNon-debugging symbols:\n");
	      first = 0;
	    }
	  print_msymbol_info (p->msymbol);
	}
      else
	{
	  print_symbol_info (kind,
			     p->symtab,
			     p->symbol,
			     p->block,
			     last_filename);
	  last_filename = p->symtab->filename;
	}
    }

  do_cleanups (old_chain);
}

static void
variables_info (char *regexp, int from_tty)
{
  symtab_symbol_info (regexp, VARIABLES_NAMESPACE, from_tty);
}

static void
functions_info (char *regexp, int from_tty)
{
  symtab_symbol_info (regexp, FUNCTIONS_NAMESPACE, from_tty);
}


static void
types_info (char *regexp, int from_tty)
{
  symtab_symbol_info (regexp, TYPES_NAMESPACE, from_tty);
}

/* JAGae80684 - This functions is written on the basis of rbreak_command
  The difference is, if there is shl_load and user has asked for a
  persistent breakpoint, then only the symtab corresponding to the
  newly loaded library is searched*/

static int loaded_main = 1;
void
rbreak_all_command (char *regexp, int from_tty)
{
  struct symbol_search *ss;
  struct symbol_search *p;
  struct cleanup *old_chain;

  search_symbols (regexp, FUNCTIONS_NAMESPACE, 0, (char **) NULL, &ss);
  old_chain = make_cleanup_free_search_symbols (ss);

  for (p = ss; p != NULL; p = p->next)
    {
      if (p->msymbol == NULL)
        {
          char *string = (char *) alloca (strlen (p->symtab->filename)
                                          + strlen (SYMBOL_NAME (p->symbol))
                                          + 4);
          if(in_shl_load)
            {
             if(!strcmp(p->symtab->objfile->name,shllib_name))
             {
              strcpy (string, p->symtab->filename);
              strcat (string, ":'");
              strcat (string, SYMBOL_NAME (p->symbol));
              strcat (string, "'");
              if(if_yes_to_all_commands && !strcmp(symfile_objfile->name,p->symtab->objfile->name))continue;
              break_command (string, from_tty);
              print_symbol_info (FUNCTIONS_NAMESPACE,
                             p->symtab,
                             p->symbol,
                             p->block,
                             p->symtab->filename);

             }
           }
 else
           {
            strcpy (string, p->symtab->filename);
            strcat (string, ":'");
            strcat (string, SYMBOL_NAME (p->symbol));
            strcat (string, "'");
            if(if_yes_to_all_commands && !loaded_main &&
            !strcmp(symfile_objfile->name,p->symtab->objfile->name))continue;
            break_command (string, from_tty);
            print_symbol_info (FUNCTIONS_NAMESPACE,
                             p->symtab,
                             p->symbol,
                             p->block,
                             p->symtab->filename);
            loaded_main=0;

           }
        }
      else
        {
          break_command (SYMBOL_NAME (p->msymbol), from_tty);
          printf_filtered ("<function, no debug info> %s;\n",
                           SYMBOL_SOURCE_NAME (p->msymbol));
        }
    }

  do_cleanups (old_chain);
}


#if 0
/* Tiemann says: "info methods was never implemented."  */
static void
methods_info (regexp)
     char *regexp;
{
  symtab_symbol_info (regexp, METHODS_NAMESPACE, 0, from_tty);
}
#endif /* 0 */

/* Breakpoint all functions matching regular expression. */
#ifdef UI_OUT
void
rbreak_command_wrapper (char *regexp, int from_tty)
{
  rbreak_command (regexp, from_tty);
}
#endif
static void
rbreak_command (char *regexp, int from_tty)
{
  struct symbol_search *ss;
  struct symbol_search *p;
  struct cleanup *old_chain;

  search_symbols (regexp, FUNCTIONS_NAMESPACE, 0, (char **) NULL, &ss);
  old_chain = make_cleanup_free_search_symbols (ss);

  for (p = ss; p != NULL; p = p->next)
    {
      if (p->msymbol == NULL)
	{
	  char *string = (char *) alloca (strlen (p->symtab->filename)
					  + strlen (SYMBOL_NAME (p->symbol))
					  + 4);
	  strcpy (string, p->symtab->filename);
	  strcat (string, ":'");
	  strcat (string, SYMBOL_NAME (p->symbol));
	  strcat (string, "'");
	  break_command (string, from_tty);
	  print_symbol_info (FUNCTIONS_NAMESPACE,
			     p->symtab,
			     p->symbol,
			     p->block,
			     p->symtab->filename);
	}
      else
	{
	  break_command (SYMBOL_NAME (p->msymbol), from_tty);
	  printf_filtered ("<function, no debug info> %s;\n",
			   SYMBOL_SOURCE_NAME (p->msymbol));
	}
    }

  do_cleanups (old_chain);
}


/* This function is used to set breakpoints at the procedure 
   entry of all debuggable procedure that match the regular
   expression , 04/15/02 Dinesh */

void rbreak_command_1 (char *regexp, int flag, int from_tty)
{

  struct symbol_search *ss;
  struct symbol_search *p;
  struct cleanup *old_chain;
 
  /* Finds all the function symbols */
  search_symbols (regexp, FUNCTIONS_NAMESPACE, 0, (char **) NULL, &ss);
  old_chain = make_cleanup_free_search_symbols (ss);

  for (p = ss; p != NULL; p = p->next)
    {
      /* Essentially to plant break point in debuggable procedure */ 
      if (p->msymbol == NULL)
	{ 
          if( ! is_char_marker( *SYMBOL_NAME(p->symbol)) )
            {
                if (flag)
                  {
	              char *string = (char *) alloca (
                                            strlen (p->symtab->filename)
				          + strlen (SYMBOL_NAME (p->symbol))
					  + 4);
	              strcpy (string, p->symtab->filename);
	              strcat (string, ":'");
	              strcat (string, SYMBOL_NAME (p->symbol));
	              strcat (string, "'");
	              break_command (string, from_tty);
                  }
                else 
                  {
	              char *string = (char *) alloca (
                                            strlen (p->symtab->filename)
				          + strlen (SYMBOL_NAME (p->symbol))
					  + 4);
	              strcpy (string, p->symtab->filename);
	              strcat (string, ":'");
	              strcat (string, SYMBOL_NAME (p->symbol));
	              strcat (string, "'");
	              break_command (string, from_tty);
	              strcpy (string, SYMBOL_NAME (p->symbol));
	              break_at_finish_command_1 (string, flag, from_tty);
                  }
           } 
        }
    }
  do_cleanups (old_chain);
}


/* Return Nonzero if block a is lexically nested within block b,
   or if a and b have the same pc range.
   Return zero otherwise. */
int
contained_in (struct block *a, struct block *b)
{
  if (!a || !b)
    return 0;
  return BLOCK_START (a) >= BLOCK_START (b)
    && BLOCK_END (a) <= BLOCK_END (b);
}


/* Helper routine for make_symbol_completion_list.  */

static int return_val_size;
static int return_val_index;
static char **return_val;

/* 
 * JAGaf70393 - if the name is a C++ name, construct the list 
 * based on demangled_name 
 */ 
#ifdef HP_IA64
#define COMPLETION_LIST_ADD_SYMBOL(symbol, sym_text, len, text, word) \
  do { \
    if (SYMBOL_DEMANGLED_NAME (symbol) != NULL) \
      /* Put only the mangled name on the list.  */ \
      /* Advantage:  "b foo<TAB>" completes to "b foo(int, int)" */ \
      /* Disadvantage:  "b foo__i<TAB>" doesn't complete.  */ \
      completion_list_add_name \
	(SYMBOL_DEMANGLED_NAME (symbol), (sym_text), (len), (text), (word)); \
    else \
      { \
        if (is_cplus_name(SYMBOL_NAME(symbol), 1)) \
          { \
            cplus_demangled_name = cplus_demangle ( SYMBOL_NAME (symbol), DMGL_ANSI | DMGL_PARAMS); \
            set_demangled_name(symbol); \
            completion_list_add_name \
	      (cplus_demangled_name, (sym_text), (len), (text), (word)); \
          } \
        else \
          { \
            completion_list_add_name \
	      (SYMBOL_NAME (symbol), (sym_text), (len), (text), (word)); \
          } \
      } \
  } while (0)

#else
#define COMPLETION_LIST_ADD_SYMBOL(symbol, sym_text, len, text, word) \
  do { \
    if (SYMBOL_DEMANGLED_NAME (symbol) != NULL) \
      /* Put only the mangled name on the list.  */ \
      /* Advantage:  "b foo<TAB>" completes to "b foo(int, int)" */ \
      /* Disadvantage:  "b foo__i<TAB>" doesn't complete.  */ \
      completion_list_add_name \
        (SYMBOL_DEMANGLED_NAME (symbol), (sym_text), (len), (text), (word)); \
    else \
      completion_list_add_name \
        (SYMBOL_NAME (symbol), (sym_text), (len), (text), (word)); \
  } while (0)
#endif 

/*  Test to see if the symbol specified by SYMNAME (which is already
   demangled for C++ symbols) matches SYM_TEXT in the first SYM_TEXT_LEN
   characters.  If so, add it to the current completion list. */

static void
completion_list_add_name (char *symname, char *sym_text, int sym_text_len, char *text, char *word)
{
  int newsize;
  int i;

  /* clip symbols that cannot match */

  if (strncmp (symname, sym_text, sym_text_len) != 0)
    {
      return;
    }

  /* Clip any symbol names that we've already considered.  (This is a
     time optimization)  */

  for (i = 0; i < return_val_index; ++i)
    {
      if (STREQ (symname, return_val[i]))
	{
	  return;
	}
    }

  /* We have a match for a completion, so add SYMNAME to the current list
     of matches. Note that the name is moved to freshly malloc'd space. */

  {
    char *new;
    if (word == sym_text)
      {
	new = xmalloc (strlen (symname) + 5);
	strcpy (new, symname);
      }
    else if (word > sym_text)
      {
	/* Return some portion of symname.  */
	new = xmalloc (strlen (symname) + 5);
	strcpy (new, symname + (word - sym_text));
      }
    else
      {
	/* Return some of SYM_TEXT plus symname.  */
	new = xmalloc (strlen (symname) + (sym_text - word) + 5);
	strncpy (new, word, sym_text - word);
	new[sym_text - word] = '\0';
	strcat (new, symname);
      }

    /* Recheck for duplicates if we intend to add a modified symbol.  */
    if (word != sym_text)
      {
	for (i = 0; i < return_val_index; ++i)
	  {
	    if (STREQ (new, return_val[i]))
	      {
		free (new);
		return;
	      }
	  }
      }

    if (return_val_index + 3 > return_val_size)
      {
	newsize = (int) ((return_val_size *= 2) * sizeof (char *));
	return_val = (char **) xrealloc ((char *) return_val, newsize);
      }
    return_val[return_val_index++] = new;
    return_val[return_val_index] = NULL;
  }
}

/* Return a NULL terminated array of all symbols (regardless of class) which
   begin by matching TEXT.  If the answer is no symbols, then the return value
   is an array which contains only a NULL pointer.

   Problem: All of the symbols have to be copied because readline frees them.
   I'm not going to worry about this; hopefully there won't be that many.  */

#define MAX_COMPLETION_MATCHES 100

char **
make_symbol_completion_list (char *text, char *word)
{
  register struct symbol *sym;
  register struct symtab *s;
  register struct partial_symtab *ps;
  register struct minimal_symbol *msymbol;
  register struct objfile *objfile;
  register struct block *b, *surrounding_static_block = 0;
  register int i, j;
  struct partial_symbol **psym;
  /* The symbol we are completing on.  Points in same buffer as text.  */
  char *sym_text;
  /* Length of sym_text.  */
  int sym_text_len;
  char *pure_text;
  extern int lazy_demangling;
  int class_qualified;

  /* Now look for the symbol we are supposed to complete on.
     FIXME: This should be language-specific.  */
  {
    char *p;
    char quote_found;
    char *quote_pos = NULL;

    /* First see if this is a quoted string.  */
    quote_found = '\0';
    for (p = text; *p != '\0'; ++p)
      {
	if (quote_found != '\0')
	  {
	    if (*p == quote_found)
	      /* Found close quote.  */
	      quote_found = '\0';
	    else if (*p == '\\' && p[1] == quote_found)
	      /* A backslash followed by the quote character
	         doesn't end the string.  */
	      ++p;
	  }
	else if (*p == '\'' || *p == '"')
	  {
	    quote_found = *p;
	    quote_pos = p;
	  }
      }
    if (quote_found == '\'')
      /* A string within single quotes can be a symbol, so complete on it.  */
      sym_text = quote_pos + 1;
    else if (quote_found == '"')
      /* A double-quoted string is never a symbol, nor does it make sense
         to complete it any other way.  */
      return NULL;
    else
      {
	/* It is not a quoted string.  Break it based on the characters
	   which are in symbols.  */
	while (p > text)
	  {
	    if (isalnum (p[-1]) || p[-1] == '_' || p[-1] == '\0')
	      --p;
	    else
	      break;
	  }
	sym_text = p;
      }
  }

  sym_text_len = (int) strlen (sym_text);

  return_val_size = 100;
  return_val_index = 0;
  return_val = (char **) xmalloc ((return_val_size + 1) * sizeof (char *));
  return_val[0] = NULL;

  /* srikanth, if the user hits a tab by mistake, we could end up
     with a regular expression which is empty which by definition
     is a substring of every string. In this case, we would end up
     constructing a linked list the entire set of symbols from the
     program. This takes forever when there are millions of symbols.
     Just refuse to be a party to monumental dumbosity 0010516.
     Quit looking for additional matches after we have reached 100.
  */

  /* Look through the partial symtabs for all symbols which begin
     by matching SYM_TEXT.  Add each one that you find to the list.  */

  ALL_PSYMTABS (objfile, ps)
  {
    /* If the psymtab's been read in we'll get it when we search
       through the blockvector.  */
    if (ps->readin)
      continue;

    for (psym = objfile->global_psymbols.list + ps->globals_offset;
	 psym < (objfile->global_psymbols.list + ps->globals_offset
		 + ps->n_global_syms);
	 psym++)
      {
	/* If interrupted, then quit. */
	QUIT;
	COMPLETION_LIST_ADD_SYMBOL (*psym, sym_text, sym_text_len, text, word);
        if (return_val_index >= MAX_COMPLETION_MATCHES)
          goto getout;
      }

    for (psym = objfile->static_psymbols.list + ps->statics_offset;
	 psym < (objfile->static_psymbols.list + ps->statics_offset
		 + ps->n_static_syms);
	 psym++)
      {
	QUIT;
	COMPLETION_LIST_ADD_SYMBOL (*psym, sym_text, sym_text_len, text, word);
        if (return_val_index >= MAX_COMPLETION_MATCHES)
          goto getout;
      }
  }

    /* srikanth, with just in time demangling mode, we have a few 
       interesting problems in command line completion. Assume that
       the user is trying to set a breakpoint on a function say
       NSTail::Node::connectOutEdge(Edge &).

       Supposing -g information is available, then the type "NSTail::Node"
       will be present in some psymtab and thus completion hints will work
       upto that point. We run into problems after that. We also have a
       problem for plain non-method functions. They will appear in mangled
       form (unless they were looked up earlier by chance and got demangled
       then.)

       Here is the workaround : The symbol is either :: qualified or
       it is not. If it is, then we hopefully have a long enough text
       pattern (the class name.) We could demangle those symbols that
       contain the class name as an embedded pattern.

       If it is a plain function, then the prefix must start with the
       first identifier in `sym_text'. We could simply extract the first
       identifier and call lookup_minimal_symbol on it. This has the
       side effect of demangling the *relevant* part of the linker symbol
       table.

       Completion in the absence of debug info would still work, but will 
       require more help from the user (he/she will have to complete upto
       the '::'.) If this becomes an issue, we could revisit this in the 
       future and simply demangle all symbols that contain the partial 
       pattern.
    */

    class_qualified = 0;
    pure_text = "";
    if (lazy_demangling)
      {
        if (strstr(sym_text, "::"))
          class_qualified = 1;
        else 
          class_qualified = 0;

        pure_text = (char *) alloca (strlen(sym_text) + 1);
        pure_text[0] = 0;

        extract_leading_id (sym_text, pure_text);

        if (!class_qualified && pure_text[0])
          lookup_minimal_symbol(pure_text, NULL, NULL);
      }

  /* At this point scan through the misc symbol vectors and add each
     symbol you find to the list.  Eventually we want to ignore
     anything that isn't a text symbol (everything else will be
     handled by the psymtab code above).  */

  ALL_MSYMBOLS (objfile, msymbol)
    {
      QUIT;

      if (lazy_demangling && class_qualified && pure_text[0] &&
                  strstr(SYMBOL_NAME(msymbol), pure_text))
        if (SYMBOL_LANGUAGE(msymbol) == language_auto)
               SYMBOL_INIT_DEMANGLED_NAME(msymbol, &objfile->symbol_obstack);

      COMPLETION_LIST_ADD_SYMBOL (msymbol, sym_text, sym_text_len, text, word);
      if (return_val_index >= MAX_COMPLETION_MATCHES)
        goto getout;
    }

  /* Search upwards from currently selected frame (so that we can
     complete on local vars.  */

  for (b = get_selected_block (); b != NULL; b = BLOCK_SUPERBLOCK (b))
    {
      if (!BLOCK_SUPERBLOCK (b))
	{
	  surrounding_static_block = b;		/* For elmin of dups */
	}

      /* Also catch fields of types defined in this places which match our
         text string.  Only complete on types visible from current context. */

      for (i = 0; i < BLOCK_NSYMS (b); i++)
	{
	  sym = BLOCK_SYM (b, i);
	  COMPLETION_LIST_ADD_SYMBOL (sym, sym_text, sym_text_len, text, word);
          if (return_val_index >= MAX_COMPLETION_MATCHES)
            goto getout;
	  if (SYMBOL_CLASS (sym) == LOC_TYPEDEF)
	    {
	      struct type *t = SYMBOL_TYPE (sym);
	      enum type_code c = TYPE_CODE (t);

	      if (c == TYPE_CODE_UNION || c == TYPE_CODE_STRUCT)
		{
		  for (j = TYPE_N_BASECLASSES (t); j < TYPE_NFIELDS (t); j++)
		    {
		      if (TYPE_FIELD_NAME (t, j))
			{
			  completion_list_add_name (TYPE_FIELD_NAME (t, j),
					sym_text, sym_text_len, text, word);
			}
		    }
		}
	    }
	}
    }

  /* Go through the symtabs and check the externs and statics for
     symbols which match.  */

  ALL_SYMTABS (objfile, s)
  {
    QUIT;
    b = BLOCKVECTOR_BLOCK (BLOCKVECTOR (s), GLOBAL_BLOCK);
    for (i = 0; i < BLOCK_NSYMS (b); i++)
      {
	sym = BLOCK_SYM (b, i);
	COMPLETION_LIST_ADD_SYMBOL (sym, sym_text, sym_text_len, text, word);
        if (return_val_index >= MAX_COMPLETION_MATCHES)
          goto getout;
      }
  }

  ALL_SYMTABS (objfile, s)
  {
    QUIT;
    b = BLOCKVECTOR_BLOCK (BLOCKVECTOR (s), STATIC_BLOCK);
    /* Don't do this block twice.  */
    if (b == surrounding_static_block)
      continue;
    for (i = 0; i < BLOCK_NSYMS (b); i++)
      {
	sym = BLOCK_SYM (b, i);
	COMPLETION_LIST_ADD_SYMBOL (sym, sym_text, sym_text_len, text, word);
        if (return_val_index >= MAX_COMPLETION_MATCHES)
          goto getout;
      }
  }

getout:

  return (return_val);
}

#undef MAX_COMPLETION_MATCHES

/* Determine if PC is in the prologue of a function.  The prologue is the area
   between the first instruction of a function, and the first executable line.
   Returns 1 if PC *might* be in prologue, 0 if definately *not* in prologue.

   If non-zero, func_start is where we think the prologue starts, possibly
   by previous examination of symbol table information.
 */

int
in_prologue (CORE_ADDR pc, CORE_ADDR func_start)
{
  struct symtab_and_line sal;
  CORE_ADDR func_addr, func_end;

  /* We have several sources of information we can consult to figure
     this out.
     - Compilers usually emit line number info that marks the prologue
       as its own "source line".  So the ending address of that "line"
       is the end of the prologue.  If available, this is the most
       reliable method.
     - The minimal symbols and partial symbols, which can usually tell
       us the starting and ending addresses of a function.
     - If we know the function's start address, we can call the
       architecture-defined SKIP_PROLOGUE function to analyze the
       instruction stream and guess where the prologue ends.
     - Our `func_start' argument; if non-zero, this is the caller's
       best guess as to the function's entry point.  At the time of
       this writing, handle_inferior_event doesn't get this right, so
       it should be our last resort.  */

  /* Consult the partial symbol table, to find which function
     the PC is in.  */
  if (! find_pc_partial_function (pc, NULL, &func_addr, &func_end))
    {
      CORE_ADDR prologue_end;

      /* We don't even have minsym information, so fall back to using
         func_start, if given.  */
      if (! func_start)
	return 1;		/* We *might* be in a prologue.  */

      prologue_end = SKIP_PROLOGUE (func_start);

      return func_start <= pc && pc < prologue_end;
    }

  /* If we have line number information for the function, that's
     usually pretty reliable.  */
  sal = find_pc_line (func_addr, 0);

  /* Now sal describes the source line at the function's entry point,
     which (by convention) is the prologue.  The end of that "line",
     sal.end, is the end of the prologue.

     Note that, for functions whose source code is all on a single
     line, the line number information doesn't always end up this way.
     So we must verify that our purported end-of-prologue address is
     *within* the function, not at its start or end.  */
  if (sal.line == 0
      || sal.end <= func_addr
      || func_end <= sal.end)
    {
      /* We don't have any good line number info, so use the minsym
	 information, together with the architecture-specific prologue
	 scanning code.  */
      CORE_ADDR prologue_end = SKIP_PROLOGUE (func_addr);

      return func_addr <= pc && pc < prologue_end;
    }

  /* We have line number info, and it looks good.  */
  return func_addr <= pc && pc < sal.end;
}


/* Begin overload resolution functions */
/* Helper routine for make_symbol_completion_list.  */

static int sym_return_val_size;
static int sym_return_val_index;
static struct symbol **sym_return_val;

/*  Test to see if the symbol specified by SYMNAME (which is already
   demangled for C++ symbols) matches SYM_TEXT in the first SYM_TEXT_LEN
   characters.  If so, add it to the current completion list. */

static void
overload_list_add_symbol (struct symbol *sym, char *oload_name)
{
  int newsize;
  int i;

  /* Get the demangled name without parameters */
  char *sym_name = cplus_demangle (SYMBOL_NAME (sym), DMGL_NO_OPTS);
  if (!sym_name)
    {
      sym_name = (char *) xmalloc (strlen (SYMBOL_NAME (sym)) + 1);
      strcpy (sym_name, SYMBOL_NAME (sym));
    }

  /* skip symbols that cannot match */
  if (strcmp (sym_name, oload_name) != 0)
    {
      free (sym_name);
      return;
    }

  /* If there is no type information, we can't do anything, so skip */
  if (SYMBOL_TYPE (sym) == NULL)
    return;

  /* skip any symbols that we've already considered. */
  for (i = 0; i < sym_return_val_index; ++i)
    if (!strcmp (SYMBOL_NAME (sym), SYMBOL_NAME (sym_return_val[i])))
      return;

  /* We have a match for an overload instance, so add SYM to the current list
   * of overload instances */
  if (sym_return_val_index + 3 > sym_return_val_size)
    {
      newsize = (int) ((sym_return_val_size *= 2) * sizeof (struct symbol *));
      sym_return_val = (struct symbol **) xrealloc ((char *) sym_return_val, newsize);
    }
  sym_return_val[sym_return_val_index++] = sym;
  sym_return_val[sym_return_val_index] = NULL;

  free (sym_name);
}

/* Return a null-terminated list of pointers to function symbols that
 * match name of the supplied symbol FSYM.
 * This is used in finding all overloaded instances of a function name.
 * This has been modified from make_symbol_completion_list.  */


struct symbol **
make_symbol_overload_list (struct symbol *fsym)
{
  register struct symbol *sym;
  register struct symtab *s;
  register struct objfile *objfile;
  register struct block *b, *surrounding_static_block = 0;
  register int i;
  /* The name we are completing on. */
  char *oload_name = NULL;

  /* Look for the symbol we are supposed to complete on.
   * FIXME: This should be language-specific.  */

  oload_name = cplus_demangle (SYMBOL_NAME (fsym), DMGL_NO_OPTS);
  if (!oload_name)
    {
      oload_name = (char *) xmalloc (strlen (SYMBOL_NAME (fsym)) + 1);
      strcpy (oload_name, SYMBOL_NAME (fsym));
    }

  sym_return_val_size = 100;
  sym_return_val_index = 0;
  sym_return_val = (struct symbol **) xmalloc ((sym_return_val_size + 1) * sizeof (struct symbol *));
  sym_return_val[0] = NULL;

  /* ??? RM: What in hell is this? overload_list_add_symbol expects a symbol,
     not a partial_symbol or a minimal_symbol. And it looks at the type field
     of the symbol, and we don't know the type of minimal and
     partial symbols */
#if 0
  /* Look through the partial symtabs for all symbols which begin
     by matching OLOAD_NAME.  Make sure we read that symbol table in. */

  ALL_PSYMTABS (objfile, ps)
  {
    struct partial_symbol **psym;

    /* If the psymtab's been read in we'll get it when we search
       through the blockvector.  */
    if (ps->readin)
      continue;

    for (psym = objfile->global_psymbols.list + ps->globals_offset;
	 psym < (objfile->global_psymbols.list + ps->globals_offset
		 + ps->n_global_syms);
	 psym++)
      {
	/* If interrupted, then quit. */
	QUIT;
        /* This will cause the symbol table to be read if it has not yet been */
        s = PSYMTAB_TO_SYMTAB (ps);
      }

    for (psym = objfile->static_psymbols.list + ps->statics_offset;
	 psym < (objfile->static_psymbols.list + ps->statics_offset
		 + ps->n_static_syms);
	 psym++)
      {
	QUIT;
        /* This will cause the symbol table to be read if it has not yet been */
        s = PSYMTAB_TO_SYMTAB (ps);
      }
  }
#endif

  /* Search upwards from currently selected frame (so that we can
     complete on local vars.  */

  for (b = get_selected_block (); b != NULL; b = BLOCK_SUPERBLOCK (b))
    {
      if (!BLOCK_SUPERBLOCK (b))
	{
	  surrounding_static_block = b;		/* For elimination of dups */
	}

      /* Also catch fields of types defined in this places which match our
         text string.  Only complete on types visible from current context. */

      for (i = 0; i < BLOCK_NSYMS (b); i++)
	{
	  sym = BLOCK_SYM (b, i);
	  overload_list_add_symbol (sym, oload_name);
	}
    }

  /* Go through the symtabs and check the externs and statics for
     symbols which match.  */

  ALL_SYMTABS (objfile, s)
  {
    QUIT;
    b = BLOCKVECTOR_BLOCK (BLOCKVECTOR (s), GLOBAL_BLOCK);
    for (i = 0; i < BLOCK_NSYMS (b); i++)
      {
	sym = BLOCK_SYM (b, i);
	overload_list_add_symbol (sym, oload_name);
      }
  }

  ALL_SYMTABS (objfile, s)
  {
    QUIT;
    b = BLOCKVECTOR_BLOCK (BLOCKVECTOR (s), STATIC_BLOCK);
    /* Don't do this block twice.  */
    if (b == surrounding_static_block)
      continue;
    for (i = 0; i < BLOCK_NSYMS (b); i++)
      {
	sym = BLOCK_SYM (b, i);
	overload_list_add_symbol (sym, oload_name);
      }
  }

  free (oload_name);

  return (sym_return_val);
}

/* End of overload resolution functions */


int admit_internal_symbols = 0;

static void
set_debug_ccover_command (char *args, int from_tty, struct cmd_list_element *c)
{
  static int debug_ccover_enabled = 0;

  if (target_has_stack && debug_ccover && !debug_ccover_enabled)
    {
      warning ("GDB may not implant correct breakpoints in c-cover instrumented");
      warning ("executable. You will have to restart the program.");
    }

  if (debug_ccover)
    debug_ccover_enabled = 1;
  else
    debug_ccover_enabled = 0;

}

static void
show_debug_ccover_command (char *args, int from_tty, struct cmd_list_element *c)
{

  if (debug_ccover)
    printf_filtered ("GDB can set correct breakpoints in c-cover instrumented executable.\n");  
  else
    printf_filtered ("GDB cannot set correct breakpoints in c-cover instrumented executable.\n");  

}

void
_initialize_symtab ()
{
  struct cmd_list_element *set, *show;

  set = add_set_cmd ("non-debug-functions", class_support,
                                 var_boolean,
                     (char *) &include_non_debug_symbols,
                    "Set listing of non-debug functions by info functions.\n\nUsage:\n"
                    "To set new value:\n\tset non-debug-functions [on | off]\nTo see "
                    "current value:\n\tshow non-debug-functions\n",
                     &setlist);

  show = add_show_from_set (set, &showlist);

  set = add_set_cmd ("max-function-matches", class_support,
                                 var_integer,
                     (char *) &max_function_matches,
                    "Set maximum number of function matches reported by info functions."
                    "\n(set to zero to report all matches)\n\nUsage:\nTo set new value:"
                    "\n\tset max-function-matches <INTEGER>\nTo see current value:\n\t"
                    "show max-function-matches\n",
                     &setlist);

  show = add_show_from_set (set, &showlist);

  /* Baskar, JAGae57840, allow the user to implant breakpoints in the c-cover
     instrumented source code */

  set = add_set_cmd ("debug-ccover", no_class, var_boolean,
		     (char *) &debug_ccover, "", &setlist);

  set->function.sfunc = set_debug_ccover_command;
  set->doc = "Set breakpoints in c-cover instrumented source files\n\nUsage:\n"
             "To set new value:\n\tset debug-ccover [on | off]\nTo see current "
             "value:\n\tshow debug-ccover\n";
  show = add_show_from_set (set, &showlist);
  show->function.sfunc = show_debug_ccover_command;

  /* srikanth, 000210, allow the user to decide if he wants gdb
     to admit compiler generated symbols into the symbol table.
  */
  set = add_set_cmd ("admit-internal-symbols", class_support,
                                 var_boolean,
                     (char *) &admit_internal_symbols,
                    "Set admission of internal symbols into gdb tables.\n\n\
Usage:\nTo set new value:\n\tset admit-internal-symbols [on | off]\n\
To see current value:\n\tshow admit-internal-symbols\n", &setlist);

  show = add_show_from_set (set, &showlist);

  add_info ("variables", variables_info,
	 "All global and static variable names, or those matching REGEXP.\n\n"
         "Usage:\n\tinfo variables [<REGEXP>]\n");
  if (dbx_commands)
    add_com ("whereis", class_info, variables_info,
	 "All global and static variable names, or those matching REGEXP.");

  add_info ("functions", functions_info,
	    "All function names, or those matching REGEXP.\n\nUsage:\n\tinfo \
functions [<REGEXP>]\n");

  
  /* FIXME:  This command has at least the following problems:
     1.  It prints builtin types (in a very strange and confusing fashion).
     2.  It doesn't print right, e.g. with
     typedef struct foo *FOO
     type_print prints "FOO" when we want to make it (in this situation)
     print "struct foo *".
     I also think "ptype" or "whatis" is more likely to be useful (but if
     there is much disagreement "info types" can be fixed).  */
  add_info ("types", types_info,
	    "All type names, or those matching REGEXP.\n\nUsage:\n\t"
            "info types [<REGEXP>]\n");

#if 0
  add_info ("methods", methods_info,
	    "All method names, or those matching REGEXP::REGEXP.\n\
If the class qualifier is omitted, it is assumed to be the current scope.\n\
If the first REGEXP is omitted, then all methods matching the second REGEXP\n\
are listed.");
#endif
  add_info ("sources", sources_info,
	    "Source files in the program.\n\nUsage:\n\tinfo sources\n");

  add_com ("rbreak", class_breakpoint, rbreak_command,
	   "Set a breakpoint for all functions matching REGEXP.\n\n\
Usage:\n\trbreak <REGEXP>\n\n");

  if (xdb_commands)
    {
      add_com ("lf", class_info, sources_info, "Source files in the program");
      add_com ("lg", class_info, variables_info,
	 "All global and static variable names, or those matching REGEXP.");
    }

  /* Initialize the one built-in type that isn't language dependent... */
  builtin_type_error = init_type (TYPE_CODE_ERROR, 0, 0,
				  "<unknown type>", (struct objfile *) NULL);
}

/* symAddrtoName - called in ../opcodes/ia64-dis.c.  Given an address,
   put a disassembly symbolic name in sname, limited by width.
   */

void 
symAddrtoName (unsigned long long adr, char sname[], int rmdr, int add0x, int width)
{
  struct minimal_symbol *minsym;
  struct symbol *symbol;
  const char* src_name_ptr;
  const char* sym_name_ptr;
  char temp_name[20000];   /* The current max used in C++ is 8K */
  unsigned long long symbol_addr;

  if (width == 0)
    return;  /* Nothing can be done */

  if (width < 0)
    width = - width;

  symbol = find_pc_function (adr);
  minsym = lookup_minimal_symbol_by_pc (adr);
  if (symbol)
    {
      sym_name_ptr = SYMBOL_NAME (symbol);
      src_name_ptr = SYMBOL_SOURCE_NAME (symbol);
      symbol_addr = SWIZZLE(BLOCK_START (SYMBOL_BLOCK_VALUE (symbol)));
    }
  else
    {
      if (minsym)
	{
	  sym_name_ptr = SYMBOL_NAME (minsym);
	  src_name_ptr = SYMBOL_SOURCE_NAME (minsym);
	  symbol_addr = SYMBOL_VALUE_ADDRESS (minsym);
	}
      else
	{
	  symbol_addr = 0;
	  sym_name_ptr = src_name_ptr = 0;
	}
    }
  
  if (   ! symbol 
      && (!sym_name_ptr || !minsym || MSYMBOL_TYPE(minsym) != mst_text))
    sprintf (temp_name, "0x%llx",adr);
  else
    { /* First, try to do it with the demangled name. */
      /* 1000559585 - Usha :Consistency needed in disassembled output for branch targets (hex)*/ 
      sprintf (temp_name, "%s+0x%llx", src_name_ptr, adr - symbol_addr);

      /* If too long, use mangled name */

      if (strlen (temp_name) + 1 >= width)
	sprintf (temp_name, "%s+0x%llx", sym_name_ptr, adr - symbol_addr); /*Fix for 1000559585 -Usha*/

      /* If still too long, use number notation */
      if (strlen (temp_name) + 1 >= width)
	sprintf (temp_name, "0x%llx",adr);
    }
  
  /* If the length is still too long, just fill temp_name with 
     width - 2 '*' characters.
     */
  
  if (strlen (temp_name) + 1 >= width)
    {
      int i;

      for (i = width -2;  i >= 0;  i--)
	temp_name[i] = '*';
      temp_name[width - 1] = 0;
    }
  /* Finally, copy the result to the sname buffer, it must fit. */

  strcpy (sname, temp_name);
}

/* Description: user_symbol_choice() takes an array of symbols, lists them
 *		on the terminal window, asks the user to choose one and returns
 *		that symbol.  It is generic and can be used any place having a
 *		need to list symbols and get a choice.
 * Input:	sym_arr	 - an array of struct symbol pointers.
 *		num_sums - number of symbols in sym_arr.
 * Output:	Pointer to a struct symbol data type selected by the user.
 * Globals:	-
 * Notes:	This function was created because the ability to choose a symbol
 *		from a list is used and will be used repeatedly in gdb, however,
 *		there was no generic function to do this.  The current modus
 *		operandi is ad-hoc for each situation.  Fix for JAGad88566.
 *		Bharath, 23 Feb 2004.
 */
struct symbol *
user_symbol_choice (struct symbol **sym_arr, int num_syms)
{

  int i, choice = 0;
  char *args;

  if (num_syms == 1) 	/* Fix for JAGaf21731.	Bharath, 6 May 2004. */
    return sym_arr[0];

  for (i = 0; i < num_syms; i++)
    printf_filtered ("[%d] %s\n", i + 1, SYMBOL_NAME(sym_arr[i]));

  printf_filtered ("Enter your choice [1-%d].\n", num_syms);
  while (1)
    {
      args = command_line_input ("> ", 0, "");
      if (args == NULL)
        printf_filtered ("Invalid choice.  Please make a valid choice [1-%d].\n", num_syms);
      else
        {
	  choice = (int) atol (args);
	  if (choice < 1 || choice > num_syms)
            printf_filtered ("Invalid choice.  Please make a valid choice [1-%d].\n", num_syms);
          else
	    break;
	}
    }

  return sym_arr[choice - 1];
}

#ifdef HASH_ALL
/* A cache for storing user provided symbol name so that find_templates() can 
 * use it.  find_templates() can't take it as an argument because it is passed 
 * as an argument to foreach_text_minsym(), which in turn stipulates that there 
 * can be more than one argument for its function pointer argument.  
 * Fix for JAGad88566.  Bharath, 23 Feb 2004.
 */
char *find_templates_str;

/* Description:	This rountine is cleanup path in case of a control-c from the 
 *		user during choice selection.  The only thing to make sure is 
 *		clean is the number of symbols found.
 * Input:	syms_found	- this is a void pointer as per the requirments
 *				  of the make_cleanup() routine.  It points to
 *				  the static int syms_found in find_templates().
 * Ouput: 	-
 * Globals: 	-
 * Notes: 	The array need not be freed because it will automatically get 
 *		freed the next time this path is executed without the control-c 
 *		from the user.
 */
void
find_templates_cleanup (void *syms_found)
{
  *((int *) syms_found) = 0;
}

/* Description: For each minimal symbol passed to find_templates(), it finds 
 *		the corresponding template type symbols and collects them in an 
 *		array.  Once it is called with a NULL argument, it lists all 
 *		symbols, finds out the user choice and returns that symbol 
 *		pointer.  Fix for JAGad88566.  Bharath, Feb 23 2004.
 * Input:	msym	- pointer to a minimal symbol that corresponds to the
 *			  lookup of the user provided symbol name.
 * Output:	Pointer to a symbol when argument is NULL, NULL otherwise.
 * Globals:	find_templates_str.  This is used to find out the string name of
 *		the symbol that the user provided.
 * Notes:	-
 */
struct symbol *
find_templates (struct minimal_symbol *msym)
{
  int i, j, num_syms_in_block;
  struct blockvector *bv;
  struct block *block;
  struct symbol *sym;
  struct partial_symtab *pst;
  struct cleanup *old_cleanup_chain = NULL;

  static int sym_arr_size = 20, syms_found;
  static struct symbol **sym_arr;

  
  /* If msym is NULL, ask the user to select a symbol from sym_arr. */
  if (!msym)
    {
      old_cleanup_chain = make_cleanup (find_templates_cleanup, 
      					(void *) &syms_found);
      sym = user_symbol_choice (sym_arr, syms_found);
      do_cleanups (old_cleanup_chain);

      free (sym_arr);
      sym_arr = NULL;
      syms_found = 0;
      find_templates_str = NULL;
      return sym;
    }

  /* Allocate a symbol array only if it hasn't been allocated already. */
  if (!sym_arr)
    sym_arr = (struct symbol **) 
              malloc (sizeof (struct symbol *) * sym_arr_size);

  /* Look at all partial symbol tables that msym is associated with.  If
   * a given partial symbol table hasn't been expanded into a symbol table,
   * do so and then examine the STATIC block of that symbol table
   * to get the symbol information for msym.
   */
  for (pst = msym->pst; pst; pst = pst->next)
    {
      if (pst->readin)
        PSYMTAB_TO_SYMTAB (pst);

      bv = BLOCKVECTOR (pst->symtab);
      block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
      num_syms_in_block = BLOCK_NSYMS (block);
      for (i = 0; i < num_syms_in_block; i++)
        {
	  sym = BLOCK_SYM (block, i);

	  /* To qualify, a symbol should 
	   *	1. be a class type or structure type,
	   *	2. be in STRUCT_NAMESPACE and
	   *	3. return 0 for strcmp_iwt (name, sym)
           */
	  /* JAGaf47322-Menu problems with ptype on a method of 
	   * templated class.
           * Templates can be either class, union or structure type. Union templates
           * are considered as structure template and DWARF entry emitted for union
           * template is structure template only.
	   */
	  if (((TYPE_CODE(SYMBOL_TYPE(sym)) == TYPE_CODE_CLASS) || 
	      (TYPE_CODE(SYMBOL_TYPE(sym)) == TYPE_CODE_STRUCT)) &&
	      SYMBOL_NAMESPACE(sym) == STRUCT_NAMESPACE &&
	      strcmp_iwt (SYMBOL_NAME(sym), find_templates_str) == 0) 
	    {
	      /* Double the space every time you run out of it. */
	      if (syms_found >= sym_arr_size)
	        {
		  sym_arr_size *= 2;
                  sym_arr = (struct symbol **) 
		            realloc (sym_arr, 
			             sizeof (struct symbol *) * sym_arr_size);
	        }

	      /* If the symbol has already been seen, ignore it. */
	      for (j = 0; j < syms_found; j++)
	        if (sym_arr[j] == sym)
		  break;
	      if (j == syms_found || syms_found == 0)
	        sym_arr[syms_found++] = sym;
	    }
        }
    }

   return NULL;
}
#endif	/* HASH_ALL */

/*  JAGaf49121(Kavitha): This is to get the cheaper way to demangle
 *  C++ functions for PA and LRE. It is not easy to implement the 
 *  same way as in IPF since the list of patterns is long. In all 
 *  the places where demangling is required, this function is called
 *  and then cplus_demangle() is called again (cplus_demangle is
 *  already called in this function, hence 2nd time for PA and LRE).
 *  So, revisited all the places where this function is called and 
 *  made the call for cplus_demangle() to minimal. It needed 
 *  restructuring of this function which is common for both IA and PA. 
 *  Pass a flag to tell this function whether to demangle hereitself 
 *  or not. But at the calling places, it is made in a such a way that 
 *  this flag is effective only for IPF. In places where it needs to 
 *  check only whether the function is cplus and don't need demangling, 
 *  0 is passed for IPF. For PA and LRE, all the time it is 1. The 
 *  optimization for PA and LRE is that demangling is not done twice,
 *  i.e, in this function and at the place where it is called. I am
 *  using a global variable to carry the demangled name and used it
 *  whereever demangling is needed after this function is called. 
 */

int
is_cplus_name (const char* name, int demang)
{
  int is_mang = 0;

  cplus_demangled_name = 0;

#ifdef HP_IA64
  if (name && name[0] == '_' && name[1] == 'Z')
    is_mang = 1;

  if (demang && (IS_TARGET_LRE || is_mang))
#endif
	{
          cplus_demangled_name = cplus_demangle (name, DMGL_ANSI | DMGL_PARAMS);
	  if (cplus_demangled_name)
  	    return 1;
	}
  return is_mang;
}

/* JAGaa80042 - To set breakpoints on template member functions across all 
   instantiated template class. Check if the symbol is a template. */
int 
is_template (const char *tname)
{
	return ((strchr (tname, '<')) && (strchr (tname, '>')));
}

#ifdef INLINE_SUPPORT
/* This function is identical to find_pc_sect_line.
   It return the sal corresponding to inline_idx.
   If inline_idx is -1, it returns the sal corresponding to the inner-most
   inline instance.
*/
struct symtab_and_line 
find_pc_inline_idx_line (CORE_ADDR pc, int idx, int notcurrent)
{
  register int i;
  register int len;

  struct symbol *func;
  struct blockvector *bv;
  struct symtab *s = NULL;
  struct symtab_and_line val;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *mfunsym;

  register struct linetable *l;
  struct linetable_entry *prev;
  register struct linetable_entry *item;

  asection *section;
  CORE_ADDR best_end = 0;
  int best_end_idx = 0; /* inline_idx of the best_end line. */
  struct symtab *best_symtab = 0;
  struct linetable_entry *best = NULL; 

  /* Marks the item in the caller whose pc is lower (highest of the lesser)
     than (or equal to) the given PC when given an idx. This is used to check if the PC
     falls off the callee to caller boundary for an inlined function. */
  struct linetable_entry *lowest_item = 0;

  /* 
     Store here the first line number of a file which 
     contains the line at the smallest pc after PC.
     If we don't find a line whose range contains PC,
     we will use a line one less than this, with a range 
     from the start of that file to the first line's pc.  
  */
  struct symtab *alt_symtab = 0;
  struct linetable_entry *alt = NULL;

  if (!inline_debugging)
    return find_pc_line (pc, notcurrent);

  section = find_pc_overlay (pc);

  if (pc_in_unmapped_range (pc, section))
    pc = overlay_mapped_address (pc, section);


  INIT_SAL (&val);
  if (notcurrent)
    pc -= 1;

  msymbol = lookup_minimal_symbol_by_pc (pc);

  if (msymbol != NULL)
    if (MSYMBOL_TYPE (msymbol) == mst_solib_trampoline)
      {
         mfunsym = 
           lookup_minimal_symbol_text (SYMBOL_NAME (msymbol), NULL, NULL);

	 if ((mfunsym != NULL) && 
              (SYMBOL_VALUE (mfunsym) == SYMBOL_VALUE (msymbol)))
	   return find_pc_line (SYMBOL_VALUE (mfunsym), 0);
      }

#ifdef HASH_ALL
  if (msymbol && msymbol->pst && msymbol->pst->symtab)
    s = msymbol->pst->symtab;
#endif

  if (!s)
    s = find_pc_sect_symtab (pc, section);

  if (!s)
    { /* if no symbol information, return previous pc */
      val.pc = pc;
      return val;
    }

  bv = BLOCKVECTOR (s);

  for (; s && BLOCKVECTOR (s) == bv; s = s->next)
    {
      /* Find the best line in this symtab.  */
      l = LINETABLE (s);

      if (!l)
	continue;

      len = l->nitems;

      if (len <= 0)
	  continue;

      prev = NULL;
      item = l->item;		/* Get first line info */

      i = 0;
      if (!item->is_stmt)
	{
	  for (i = 0; i < len; i++, item++)
	    if (item->is_stmt)
	      break;
          if (i == len)	/* No statements found -- set to last item looked at */
	    item --;
	}

      /* this file's first line closer than the first lines of other files? */
      if (   item->pc > pc
	  && (!alt || item->pc < alt->pc)
	  && (idx == -1 || idx == item->inline_idx))
	{
	  alt = item;
	  alt_symtab = s;
	}

      for (; i < len; i++, item++)
	{
	  if (!item->is_stmt) /* Ignore non statement lines */
	    continue;

	  /* Leave prev pointing to the linetable entry for the last line
	     that started at or before PC. And item pointing to the next line with
	     higher pc and same or lesser inline_idx (same function or the caller). */
	  if (   item->pc > pc
	      && (   (prev && item->inline_idx <= prev->inline_idx) 
		  || !prev))
	    break;

	  /* Keep searching */
	  if (item->pc > pc)
	    continue;
	  
	  /* Set the lowest_item to the line table entry in the caller
	     with highest of the lower pc (as compared to PC)*/
	  if (   pc >= item->pc
	      && idx >= 0 && item->inline_idx < idx /* caller? */
	      && (!lowest_item || lowest_item->pc <= item->pc) /* greater than
								  or equal to
								  the last
								  collected? */
	     )
	    lowest_item = item;

	  /* Ignore the lines that don't match idx. */
          if (idx >= 0 && item->inline_idx != idx)
            continue;

	  /* Ignore lines that have lower inline_idx */
	  if (prev && prev->pc == item->pc && prev->inline_idx > item->inline_idx)
	    continue;

	  prev = item;
	}

      if (   prev
          && (   !best
	      || prev->pc > best->pc 
	      || (   prev->pc == best->pc
		  && (prev + 1 != (l->item + len)) /* Don't set the best to prev when there prev is
			     the last inlined line entry in the blockvector.
			     If we do this, sometimes we pickup a wrong line
			     entry when the PC falls just after the end of an
			     inline instance. */
		  && prev->inline_idx >  best->inline_idx)))
	{
	  best = prev;
	  best_symtab = s;
	}

      /* End should be from either the same function (matching inline_idx)
	 or from caller for inlined function (when PC is at the last line of
	 inline instance). If there are more than one line matching the 
         best_end, pick the last one. */
      if (i < len && (best_end == 0 || best_end >= item->pc)
	  && (idx == -1 || (idx >= 0 && item->inline_idx <= idx )))
	{
	  best_end = item->pc;
	  best_end_idx = item->inline_idx;
	}
    } /* for (s && BLOCKVECTOR .... */

  /* If PC falls off the boundary we may pick the last line of the inlined
     function as 'best' and best_end may be pc of the line table entry in
     the caller whose pc is greater than given PC. Note that if best_end is
     in the caller, the best_end_idx will not match the given idx of the
     callee. In this case, lowest_item->pc will be falling between best->pc
     and best_end. If this condition happens, the given PC doesn't fall in
     the inlined instance/function given by 'idx'. */
  if (   idx>= 0
      && best
      && lowest_item
      && best_end_idx != idx
      && lowest_item->pc >= best->pc
      && best_end > lowest_item->pc)
    {
      best = 0;
      best_symtab = 0;
    }

  if (!best_symtab)
    {
      if (!alt_symtab)
	  val.pc = pc;
      else
	{
	  val.symtab = alt_symtab;
	  val.line = alt->line - 1;

	  /* Don't return line 0, that means that we didn't find the line.  */
	  if (val.line == 0)
	    ++val.line;

	  val.pc = BLOCK_END (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK));
	  val.end = alt->pc;

#ifdef INLINE_SUPPORT
          val.context_ref = alt->context_ref;
          val.inline_idx  = alt->inline_idx;
#endif
	}
    }
  else
    {
      val.symtab = best_symtab;
      val.line = best->line;
      val.pc = best->pc;
      if (best_end && (!alt || best_end < alt->pc) && pc >= best_end)
	{
	  /* We picked the wrong bestend. No line with given inline_idx. */
	  val.line = 0;
          val.pc = 0;
          val.symtab = 0;
	}
      else if (best_end && (!alt || best_end < alt->pc))
	val.end = best_end;
      else if (alt)
	val.end = alt->pc;
      else if (!best_end && idx > 0 && idx == best->inline_idx)
        {
	  /* We didn't find a bestend till now. */
          if (pc == best->pc)
            val.end = best->pc;
          else
            {
              val.line = 0;
              val.pc = 0;
              val.symtab = 0;
            }
        }
      else
	val.end = BLOCK_END (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK));

#ifdef INLINE_SUPPORT
      val.context_ref = best->context_ref;
      val.inline_idx  = best->inline_idx;
#endif

      /* If the function enclosing val.pc ends before pc, then the
         line number was probably bogus -- this can happen if the function
	 enclosing pc has no line number information, as is the case
	 for compiler generated functions (constructors, for example). The
	 line number we get back is the last line number of the preceding
	 function in the text space, which is obviously incorrect. 
      */

      func = find_pc_function (val.pc);

      if (func && (BLOCK_END (SYMBOL_BLOCK_VALUE (func)) < pc))
        {
          val.line = 0;
          val.pc = 0;
          val.symtab = 0;
#ifdef INLINE_SUPPORT
          val.context_ref = 0;
          val.inline_idx  = 0;
#endif
        }
    }

  val.section = section;
  return val;
}

/* This function is identical to find_pc_sect_line.
   It return the sal of line table entry whose name with respect to 
   its inline_idx matches the given NAME.
   Used by breakpoint command.
*/
struct symtab_and_line 
find_pc_name_line (CORE_ADDR pc, char *name)
{
  register int i;
  register int len, name_len;

  struct symbol *func;
  struct blockvector *bv;
  struct symtab *s = NULL;
  struct symtab_and_line val;
  struct minimal_symbol *msymbol;
  struct minimal_symbol *mfunsym;

  register struct linetable *l;
  struct linetable_entry *prev;
  register struct linetable_entry *item;

  asection *section;
  CORE_ADDR best_end = 0;
  struct symtab *best_symtab = 0;
  struct linetable_entry *best = NULL; 

  /* 
     Store here the first line number of a file which 
     contains the line at the smallest pc after PC.
     If we don't find a line whose range contains PC,
     we will use a line one less than this, with a range 
     from the start of that file to the first line's pc.  
  */
  struct symtab *alt_symtab = 0;
  struct linetable_entry *alt = NULL;

  name_len = strlen (name);

  if (!inline_debugging)
    return find_pc_line (pc, 0);

  section = find_pc_overlay (pc);

  if (pc_in_unmapped_range (pc, section))
    pc = overlay_mapped_address (pc, section);


  INIT_SAL (&val);

  msymbol = lookup_minimal_symbol_by_pc (pc);

  if (msymbol != NULL)
    if (MSYMBOL_TYPE (msymbol) == mst_solib_trampoline)
      {
         mfunsym = 
           lookup_minimal_symbol_text (SYMBOL_NAME (msymbol), NULL, NULL);

	 if ((mfunsym != NULL) && 
              (SYMBOL_VALUE (mfunsym) == SYMBOL_VALUE (msymbol)))
	   return find_pc_line (SYMBOL_VALUE (mfunsym), 0);
      }

#ifdef HASH_ALL
  if (msymbol && msymbol->pst && msymbol->pst->symtab)
    s = msymbol->pst->symtab;
#endif

  if (!s)
    s = find_pc_sect_symtab (pc, section);

  if (!s)
    { /* if no symbol information, return previous pc */
      val.pc = pc;
      return val;
    }

  bv = BLOCKVECTOR (s);

  for (; s && BLOCKVECTOR (s) == bv; s = s->next)
    {
      /* Find the best line in this symtab.  */
      l = LINETABLE (s);

      if (!l)
	continue;

      len = l->nitems;

      if (len <= 0)
	  continue;

      prev = NULL;
      item = l->item;		/* Get first line info */

      i = 0;
      if (!item->is_stmt)
	{
	  for (i = 0; i < len; i++, item++)
	    if (item->is_stmt)
	      break;
          if (i == len)	/* No statements found -- set to last item looked at */
	    item --;
	}

      /* this file's first line closer than the first lines of other files? */
      /* JAGag02054 - Fortran failures with inline debug on by default. 
         Fix to not skip the assignment for fortran cases where 
         inline_idx is zero and "name" is not NULL. Fortran functions
         do not have prolog.  */

      if (   item->pc > pc 
          && (!alt || item->pc < alt->pc))
        {
           if (name && item->inline_idx > 0)  
             {
                if (strcmp (get_inline_name (item->inline_idx), name) == 0)
                  {
                     alt = item;
                     alt_symtab = s;
	          }
             }
	   else
             {
	        alt = item;
	        alt_symtab = s;
             }
	}

      for (; i < len; i++, item++)
	{
	  if (!item->is_stmt) /* Ignore non statement lines */
	    continue;
	  if (name && item->inline_idx > 0 &&
              ((strcmp (cplus_demangle (get_inline_name (item->inline_idx), DMGL_ANSI), name) != 0)
              && (strcmp (cplus_demangle (get_inline_name (item->inline_idx),
                                          DMGL_ANSI | DMGL_PARAMS), name) != 0)))
            continue;

	  /* Leave prev pointing to the linetable entry for the last line
	     that started at or before PC.  */
	  if (item->pc > pc) 
	    break;

	  /* Prefer name matches over noninlined. */
	  if (name && prev && prev->pc == item->pc &&
		prev->inline_idx != 0 && item->inline_idx == 0)
	    continue;

	  prev = item;
	}

      if (prev && (!best || prev->pc > best->pc 
		   || ( prev->pc == best->pc && best->inline_idx == 0)))
	{
	  best = prev;
	  best_symtab = s;
	}

      if (i < len && (best_end == 0 || best_end > item->pc))
	best_end = item->pc;
    } /* for (s && BLOCKVECTOR .... */

  if (!best_symtab)
    {
      if (!alt_symtab)
	  val.pc = pc;
      else
	{
	  val.symtab = alt_symtab;
	  val.line = alt->line - 1;

	  /* Don't return line 0, that means that we didn't find the line.  */
	  if (val.line == 0)
	    ++val.line;

	  val.pc = BLOCK_END (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK));
	  val.end = alt->pc;

#ifdef INLINE_SUPPORT
          val.context_ref = alt->context_ref;
          val.inline_idx  = alt->inline_idx;
#endif
	}
    }
  else
    {
      val.symtab = best_symtab;
      val.line = best->line;
      val.pc = best->pc;
      if (best_end && (!alt || best_end < alt->pc))
	val.end = best_end;
      else if (alt)
	val.end = alt->pc;
      else if (name && best->inline_idx > 0)
	{
	  if (pc == best->pc)
            val.end = best->pc;
	  else
	    {
	      val.line = 0;
              val.pc = 0;
              val.symtab = 0;
	    }
	}
      else
	val.end = BLOCK_END (BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK));

#ifdef INLINE_SUPPORT
      val.context_ref = best->context_ref;
      val.inline_idx  = best->inline_idx;
#endif

      /* If the function enclosing val.pc ends before pc, then the
         line number was probably bogus -- this can happen if the function
	 enclosing pc has no line number information, as is the case
	 for compiler generated functions (constructors, for example). The
	 line number we get back is the last line number of the preceding
	 function in the text space, which is obviously incorrect. 
      */

      func = find_pc_function (val.pc);

      if (func && (BLOCK_END (SYMBOL_BLOCK_VALUE (func)) < pc))
        {
          val.line = 0;
          val.pc = 0;
          val.symtab = 0;
#ifdef INLINE_SUPPORT
          val.context_ref = 0;
          val.inline_idx  = 0;
#endif
        }
    }

  val.section = section;
  return val;
}

#endif

/* Find a line number based on the actuals line number table. 
   Used by the XPR implementation in exec-path.c */
struct symtab_and_line
find_pc_line_actual (CORE_ADDR pc, int notcurrent)
{
  asection *section;
  struct symtab_and_line sal;

  /* find section and pc */
  section = find_pc_overlay (pc);
  if (pc_in_unmapped_range (pc, section))
    pc = overlay_mapped_address (pc, section);

  /* find sal */
  sal = find_pc_sect_line (pc, section, notcurrent);

  if (sal.symtab)
    {
      struct doc_linetable *doc_lines;

      doc_lines = DOC_LINETABLE(sal.symtab);
      if (doc_lines && doc_lines->nitems > 0)
        {
          int start = 0;
          int end = doc_lines->nitems;
          int found = -1;
          while (start <= end)
            {
              int middle = (start + end) / 2;
              if (pc == doc_lines->item[middle].pc)
                {
                  found = middle;
                  break;
                }
              else if (pc < doc_lines->item[middle].pc)
                {
                  end = middle - 1;
                }
              else /* pc > doc_lines->item[middle].pc */
                {
                  start = middle + 1;
                }
            }

          if (found != -1)
            {
              sal.line = doc_lines->item[found].line;
            }
        }
    }
  return sal;
}
