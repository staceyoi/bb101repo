#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# This file was written by Bindu.
#
# Tests for imports from no-g shared libraries. 
#

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] {
    return
}

# This test is presently only valid on HP-UX, since it requires
# that we use HP-UX-specific compiler & linker options to build
# the testcase.
#
setup_xfail "*-*-*"
clear_xfail "hppa*-*-*hpux*"
clear_xfail "ia64*-hp-*"

set prototypes 0
set testfile "import_nog"
set testfile1 ${objdir}/${subdir}/${testfile}.o
set testfile2 ${objdir}/${subdir}/${testfile}_lib.o

set binfile ${objdir}/${subdir}/${testfile}
set libfile1 ${objdir}/${subdir}/lib${testfile}_lib.sl
set additional_flags "additional_flags="

# Build the shared libraries this test case needs.
#
#cd ${subdir}

if { [istarget "hppa1.1-hp-hpux*"] } then {
   set additional_flags "additional_flags=+z"
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}_lib.c" "${testfile2}" object $additional_flags] != "" } {
    perror "Couldn't compile ${testfile}_lib.c"
    return -1
}


remote_exec build "ld -b ${testfile2} -o ${libfile1}"

# Build the test case

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${testfile1}" object {debug $additional_flags}] != "" } {
    perror "Couldn't compile ${testfile}.c"
    return -1
}

if { [gdb_compile "${testfile1} ${libfile1}" "${binfile}" executable {debug} ] != "" } {
    perror "Couldn't build ${binfile}"
    return -1
}
# Start with a fresh gdb

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if ![runto main] {
  perror "couldn't run to breakpoint main"
  continue
}

send_gdb "ptype bar_v\n"
gdb_expect {
   -re ".*struct tt.*int i.*int k.*\r\n$gdb_prompt $" {
       pass "ptype exported variable"
      }
   -re ".*struct tt.*int i.*int j.*\r\n$gdb_prompt $" {
       pass "ptype exported variable"
      }
   -re ".*$gdb_prompt $" { fail "ptype exported variable" }
   timeout { fail "(timeout) ptype exported variable" }
}

gdb_test "print bar_v" \
    ".*111.*121.*"\
    "Print bar_v"

gdb_test "next" \
    ".*11.*"\
    "first next in main"

gdb_test "next" \
    ".*12.*"\
    "second next in main"

gdb_test "print bar_v" \
    ".*199.*191.*"\
    "Print bar_v second time"

gdb_exit
return 0
