#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This file was written by Rob Savoye. (rob@cygnus.com)

if $tracelevel then {
	strace $tracelevel
	}

#
# test running programs
#
set prms_id 0
set bug_id 0

set oldtimeout $timeout

set testfile "ptype"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}
if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Create and source the file that provides information about the compiler
# used to compile the test case.
if [get_compiler_info ${binfile}] {
    return -1;
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

set char_qualifier "unsigned"
if { [istarget "ia64*-hp-*"]} {
   set char_qualifier "signed"
}

# Test ptype of unnamed enumeration members before any action causes
# the partial symbol table to be expanded to full symbols.  This fails
# with stabs compilers which fail to use a nameless stab (such as
# pre-2.4.5 versions of gcc and most non-gcc compilers).

# srikanth, recent changes to gdb cause it to fail to print enumerations
#  until variable is referenced. made it an xfail.
send_gdb "ptype red1\n"
gdb_expect {
    -re "type = enum primary1_tag \{blue1 = 2, red1 = 0, green1\}.*$gdb_prompt $"\
	{ pass "ptype unnamed enumeration member (aCC6)" }
    -re "type = enum primary1_tag \{red1, green1, blue1\}.*$gdb_prompt $"\
	{
	    # The workaround is in effect.  As this is a compiler, not GDB,
	    # bug, we'll make it a PASS but perhaps it should be an XFAIL.
	    pass "ptype unnamed enumeration member (worked around)"
	}
    -re "type = enum \{red1, green1, blue1\}.*$gdb_prompt $"\
			{ pass "ptype unnamed enumeration member" }
    -re ".*$gdb_prompt $"	{ xfail "ptype unnamed enumeration member" }
    timeout		{ fail "(timeout) ptype unnamed enumeration member" }
}

# pes, recent  C compiler ,does not emit debug info for unused structures.
# The xfail below is for the behavior of the old compiler.

if { $hp_cc_compiler} {
send_gdb "ptype unused_link\n"
gdb_expect {
     -re "No struct type named unused_link..*$gdb_prompt $" \
	{
	    pass "ptype unused link list structure"
        }
     -re "type = struct unused \{\[\r\n\]+\[ \t\]+struct link \\*next;\[\r\n\]+\[ \t\]+struct link \\*\\(\\*linkfunc\\)\\((struct link \\*, int|void|)\\);\[\r\n\]+\[ \t\]+struct t_struct stuff.1..2..3.;\[\r\n\]+\}.*$gdb_prompt $"\
         {
	    xfail "ptype unused link list structure"
         }
     -re "type = <data variable, no debug info>.*$gdb_prompt $" \
        {
	    pass "ptype unused link list structure"
       } 
     -re "type = struct unused \{\[\r\n\]+\[ \t\]+<no data fields>\[\r\n\]+\}.*$gdb_prompt $" {
	    pass "ptype unused link list structure"
	}
     -re ".*$gdb_prompt $" { fail "ptype unused link list structure" }
     timeout  { fail "(timeout) ptype unused link list structure" }
 }
}
#
# test ptype command with structures
#
# Here and elsewhere, we accept
# "long", "long int", or "int" for long variables (whatis.exp already
# has an XFAIL for "int" (Sun cc bug), so no need to fail it here).
gdb_test "ptype struct t_struct" "type = struct t_struct \{.*\[\r\n\]    ($char_qualifier |)char v_char_member;.*\[\r\n\]    (short|short int) v_short_member;.*\[\r\n\]    int v_int_member;.*\[\r\n\]    int v_first_bit_member.*2;.*\[\r\n\]    (long|long int|int) v_long_member;.*\[\r\n\]    float v_float_member;.*\[\r\n\]    double v_double_member;.*\[\r\n\]    int v_second_bit_member.*5;.*\[\r\n\]\}.*" "ptype structure" 

# Test ptype verbose mode 
gdb_test "ptype -v struct t_struct" "type = struct t_struct \{.*off 0 bits, len (320|384) bits.*\[\r\n\]    ($char_qualifier |)char v_char_member;.*off 0 bits, len 8 bits.*\[\r\n\]    (short|short int) v_short_member;.*off 16 bits, len 16 bits.*\[\r\n\]    int v_int_member;.*off 32 bits, len 32 bits.*\[\r\n\]    int v_first_bit_member.*2;.*off 64 bits, len 2 bits.*filler.*off 66 bits, len (30|62) bits.*\[\r\n\]    (long|long int|int) v_long_member;.*off (96|128) bits, len (32|64) bits.*\[\r\n\]    float v_float_member;.*off (128|192) bits, len 32 bits.*\[\r\n\]    double v_double_member;.*off (192|256) bits, len 64 bits.*\[\r\n\]    int v_second_bit_member.*5;.*off (256|320) bits, len 5 bits.*filler.*off (261|325) bits, len 59 bits.*\[\r\n\]\}.*" "ptype -v structure" 


# Test the equivalence between '.' and '->' for struct member references.

if [gdb_test "ptype v_struct1.v_float_member"	"type = float"]<0 then {
    return -1
}
if [gdb_test "ptype v_struct1->v_float_member"	"type = float"]<0 then {
    return -1
}
if [gdb_test "ptype v_t_struct_p.v_float_member"	"type = float"]<0 then {
    return -1
}
if [gdb_test "ptype v_t_struct_p->v_float_member"	"type = float"]<0 then {
    return -1
}


# IBM's xlc puts out bogus stabs--the stuff field is type 42,
# which isn't defined.

gdb_test "ptype struct link" "type = struct link \{\[\r\n\]+\[ \t\]+struct link \\*next;\[\r\n\]+\[ \t\]+struct link \\*\\(\\*linkfunc\\)\\((int|void|)\\);\[\r\n\]+\[ \t\]+struct t_struct stuff.1..2..3.;\[\r\n\]+\}.*" "ptype linked list structure"

# Test ptype verbose mode
gdb_test "ptype -v struct link" "type = struct link \{.*off 0 bits, len (1984|2432) bits.*\[\r\n\]+\[ \t\]+struct link \\*next;.*off 0 bits, len (32|64) bits.*\[\r\n\]+\[ \t\]+struct link \\*\\(\\*linkfunc\\)\\((int|void|)\\);.*off (32|64) bits, len (32|64) bits.*\[\r\n\]+\[ \t\]+struct t_struct stuff.1..2..3.;.*off (64|128) bits, len (1920|2304) bits.*\[\r\n\]+\}.*" "ptype -v linked list structure"

#
# test ptype command with unions
#
gdb_test "ptype union t_union" "type = union t_union \{.*\[\r\n\]    ($char_qualifier |)char v_char_member;.*\[\r\n\]    (short|short int) v_short_member;.*\[\r\n\]    int v_int_member;.*\[\r\n\]    int v_first_bit_member.*2;.*\[\r\n\]    (long|long int|int) v_long_member;.*\[\r\n\]    float v_float_member;.*\[\r\n\]    double v_double_member;.*\[\r\n\]    int v_second_bit_member.*5;.*\[\r\n\]\}.*" "ptype union" 

# Test ptype verbose mode
gdb_test "ptype -v union t_union" "type = union t_union \{.*off 0 bits, len 64 bits.*\[\r\n\]    ($char_qualifier |)char v_char_member;.*off 0 bits, len 8 bits.*\[\r\n\]    (short|short int) v_short_member;.*off 0 bits, len 16 bits.*\[\r\n\]    int v_int_member;.*off 0 bits, len 32 bits.*\[\r\n\]    int v_first_bit_member.*2;.*off 0 bits, len 2 bits.*\[\r\n\]    (long|long int|int) v_long_member;.*off 0 bits, len (32|64) bits.*\[\r\n\]    float v_float_member;.*off 0 bits, len 32 bits.*\[\r\n\]    double v_double_member;.*off 0 bits, len 64 bits.*\[\r\n\]    int v_second_bit_member.*5;.*\[\r\n\]\}.*" "ptype -v union" 

# IBM's xlc puts out bogus stabs--the stuff field is type 42,
# which isn't defined.
gdb_test "ptype union tu_link" "type = union tu_link \{\[\r\n\]+\[ \t\]+struct link \\*next;\[\r\n\]+\[ \t\]+struct link \\*\\(\\*linkfunc\\)\\((int|void|)\\);\[\r\n\]+\[ \t\]+struct t_struct stuff.1..2..3.;\[\r\n\]+\}.*" "ptype linked list union" 

# Test ptype verbose mode
gdb_test "ptype -v union tu_link" "type = union tu_link \{.*off 0 bits, len (1920|2304) bits.*\[\r\n\]+\[ \t\]+struct link \\*next;.*off 0 bits, len (32|64) bits.*\[\r\n\]+\[ \t\]+struct link \\*\\(\\*linkfunc\\)\\((int|void|)\\);.*off 0 bits, len (32|64) bits.*\[\r\n\]+\[ \t\]+struct t_struct stuff.1..2..3.;.*off 0 bits, len (1920|2304) bits.*\[\r\n\]+\}.*" "ptype -v linked list union" 

#
# test ptype command with enums
#

send_gdb "ptype primary\n"
gdb_expect {
  -re ".*type = enum .red, green, blue.*$gdb_prompt $" {pass "ptype unnamed enumeration - aCC5 sytle"}
  -re ".*type = enum _ZUt1_ .red, green, blue.*$gdb_prompt $" {pass "ptype unnamed enumeration - aCC5 sytle"}
  -re ".*type = enum .blue = 2, red = 0, green.*$gdb_prompt $" {pass "ptype unnamed enumeration - aCC6 sytle"}
  -re ".*$gdb_prompt $" {fail "ptype primary"}
}

send_gdb "ptype enum colors\n"
gdb_expect {
  -re "type = enum colors \{yellow, purple, pink\}.*$gdb_prompt $" {pass "ptype named enumeration - aCC5 sytle"}
  -re "type = enum colors \{pink = 2, yellow = 0, purple\}.*$gdb_prompt $" {pass "ptype named enumeration - aCC6 sytle"}
  -re ".*$gdb_prompt $" {fail "ptype enum colors"}
}


#
# test ptype command with enums as typedef
#
gdb_test "ptype boolean" "type = enum (boolean |)\{FALSE, TRUE\}.*" "ptype unnamed typedef'd enumeration" 

# And check that whatis shows the name, not "enum {...}".
# This probably fails for all DWARF 1 cases, so assume so for now. -fnf
# The problem with xlc is that the stabs look like
#   :t51=eFALSE:0,TRUE:1,;
#   boolean:t55=51
#   v_boolean:G51
# GDB's behavior is correct; the type which the variable is defined
# as (51) doesn't have a name.  Only 55 has a name.

# For get_debug_format to do its job, we need to have a current source file.
gdb_test "list main" ""
get_debug_format
setup_xfail_format "DWARF 1"
gdb_test "list" ".*"  "do list to make source durrent for get_debug_format"

if {!$gcc_compiled && !$hp_aCC_compiler} {
    setup_xfail "rs6000-*-*" "i*86-*-sysv4*"
    setup_xfail "hppa*-*-*" JAGaa80386
}

gdb_test "whatis v_boolean" "type = (enum |)boolean" \
  "whatis unnamed typedef'd enum"

# Same thing with struct and union.
gdb_test "ptype t_struct3" "type = struct (t_struct3 |)\{.*
 *double v_double_member;.*
 *int v_int_member;.*\}" "printing typedef'd struct"

# Test ptype verbose mode
gdb_test "ptype -v t_struct3" "type = struct (t_struct3 |)\{.*off 0 bits, len 128 bits.*
 *double v_double_member;.*off 0 bits, len 64 bits.*
 *int v_int_member;.*off 64 bits, len 32 bits.*\}" "printing typedef'd struct(verbose)"

gdb_test "ptype t_union3" "type = union (t_union3 |)\{.*
 *double v_double_member;.*
 *int v_int_member;.*\}" "printing typedef'd union"

# Test ptype verbose mode
gdb_test "ptype -v t_union3" "type = union (t_union3 |)\{.*off 0 bits, len 64 bits.*
 *double v_double_member;.*off 0 bits, len 64 bits.*
 *int v_int_member;.*off 0 bits, len 32 bits.*\}" "printing typedef'd union(verbose)"

gdb_test "ptype enum bvals" "type = enum bvals \{my_false, my_true\}.*" "ptype named typedef'd enumf'd enum"

#
# test ptype command with out-of-order enum values
#
gdb_test "ptype enum misordered" "type = enum misordered \{two = 2, one = 1, zero = 0, three = 3\}.*" "ptype misordered enumeration" 

#
# test ptype command with a named enum's value
#
gdb_test "ptype three" "type = enum misordered \{two = 2, one = 1, zero = 0, three = 3\}.*" "ptype named enumeration member" 

send_gdb "ptype red\n"
gdb_expect {
  -re "type = enum .red, green, blue.*$gdb_prompt $" {pass "ptype unnamed enumeration member #2 - aCC5 sytle"}
  -re "type = enum _ZUt1_ .red, green, blue.*$gdb_prompt $" {pass "ptype unnamed enumeration member #2 - aCC5 sytle"}
  -re "type = enum .blue = 2, red = 0, green.*$gdb_prompt $" {pass "ptype unnamed enumeration member #2 - aCC6 sytle"}
  -re ".*$gdb_prompt $" {fail "ptype unnamed enumerator member"}
}

#
# test ptype command with basic C types
#
# I've commented most of this out because it duplicates tests in whatis.exp.
# I've just left in a token test or 2 which is designed to test that ptype 
# acts like whatis for basic types.  If it is thought to be necessary to
# test both whatis and ptype for all the types, the tests should be
# merged into whatis.exp, or else maintenance will be a royal pain -kingdon
#setup_xfail "i960-*-*" 1821
#setup_xfail "mips-idt-*" "mips-sgi-*" "a29k-*-*"
#send "ptype v_char\n"
#gdb_expect {
#    -re "type = char.*$gdb_prompt $"	{ pass "ptype char" }
#    -re ".*$gdb_prompt $"	{ fail "ptype char" }
#    timeout		{ fail "(timeout) ptype char" }
#}
#
#
#setup_xfail "mips-*-*" "a29k-*-*"
#send "ptype v_signed_char\n"
#gdb_expect {
#    -re "type = signed char.*$gdb_prompt $"	{ pass "ptype signed char" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed char" }
#    timeout		{ fail "(timeout) ptype signed char" }
#}
#
#
#send "ptype v_unsigned_char\n"
#gdb_expect {
#    -re "type = unsigned char.*$gdb_prompt $"	{ pass "ptype unsigned char" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned char" }
#    timeout		{ fail "(timeout) ptype unsigned char" }
#}

gdb_test "ptype v_short" "type = short(| int).*" "ptype short" 
# Test ptype verbose mode
gdb_test "ptype -v v_short" "type = short(| int).*" "ptype -v short" 

#send "ptype v_signed_short\n"
#gdb_expect {
#    -re "type = short.*$gdb_prompt $"	{ pass "ptype signed short" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed short" }
#    timeout		{ fail "(timeout) ptype signed short" }
#}
#
#
#send "ptype v_unsigned_short\n"
#gdb_expect {
#    -re "type = unsigned short.*$gdb_prompt $"	{ pass "ptype unsigned short" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned short" }
#    timeout		{ fail "(timeout) ptype unsigned short" }
#}


gdb_test "ptype v_int" "type = int.*" "ptype int" 
# Test ptype verbose mode
gdb_test "ptype -v v_int" "type = int.*" "ptype -v int" 

#send "ptype v_signed_int\n"
#gdb_expect {
#    -re "type = int.*$gdb_prompt $"	{ pass "ptype signed int" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed int" }
#    timeout		{ fail "(timeout) ptype signed int" }
#}
#
#
#send "ptype v_unsigned_int\n"
#gdb_expect {
#    -re "type = unsigned int.*$gdb_prompt $"	{ pass "ptype unsigned int" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned int" }
#    timeout		{ fail "(timeout) ptype unsigned int" }
#}
#
#
#send "ptype v_long\n"
#gdb_expect {
#    -re "type = long.*$gdb_prompt $"	{ pass "ptype long" }
#    -re ".*$gdb_prompt $"	{ fail "ptype long" }
#    timeout		{ fail "(timeout) ptype long" }
#}
#
#
#send "ptype v_signed_long\n"
#gdb_expect {
#    -re "type = long.*$gdb_prompt $"	{ pass "ptype signed long" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed long" }
#    timeout		{ fail "(timeout) ptype signed long" }
#}
#
#
#send "ptype v_unsigned_long\n"
#gdb_expect {
#    -re "type = unsigned long.*$gdb_prompt $"	{ pass "ptype unsigned long" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned long" }
#    timeout		{ fail "(timeout) ptype unsigned long" }
#}
#
#
#send "ptype v_float\n"
#gdb_expect {
#    -re "type = float.*$gdb_prompt $"	{ pass "ptype float" }
#    -re ".*$gdb_prompt $"	{ fail "ptype float" }
#    timeout		{ fail "(timeout) ptype float" }
#}
#
#
#send "ptype v_double\n"
#gdb_expect {
#    -re "type = double.*$gdb_prompt $"	{ pass "ptype double" }
#    -re ".*$gdb_prompt $"	{ fail "ptype double" }
#    timeout		{ fail "(timeout) ptype double" }
#}


#
# test ptype command with arrays
#
#setup_xfail "i960-*-*" 1821
#setup_xfail "mips-idt-*" "mips-sgi-*" "a29k-*-*"
#send "ptype v_char_array\n"
#gdb_expect {
#    -re "type = char .2..*$gdb_prompt $"	{ pass "ptype char array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype char array" }
#    timeout		{ fail "(timeout) ptype char array" }
#}
#
#
#setup_xfail "mips-*-*" "a29k-*-*"
#send "ptype v_signed_char_array\n"
#gdb_expect {
#    -re "type = (|signed )char .2..*$gdb_prompt $"	{ pass "ptype signed char array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed char array" }
#    timeout		{ fail "(timeout) ptype signed char array" }
#}
#
#
#send "ptype v_unsigned_char_array\n"
#gdb_expect {
#    -re "type = unsigned char .2..*$gdb_prompt $"	{ pass "ptype unsigned char array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned char array" }
#    timeout		{ fail "(timeout) ptype unsigned char array" }
#}
#
#
#
#send "ptype v_int_array\n"
#gdb_expect {
#    -re "type = int .2..*$gdb_prompt $"	{ pass "ptype int array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype int array" }
#    timeout		{ fail "(timeout) ptype int array" }
#}
#
#
#send "ptype v_signed_int_array\n"
#gdb_expect {
#    -re "type = int .2..*$gdb_prompt $"	{ pass "ptype signed int array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed int array" }
#    timeout		{ fail "(timeout) ptype signed int array" }
#}
#
#
#send "ptype v_unsigned_int_array\n"
#gdb_expect {
#    -re "type = unsigned int .2..*$gdb_prompt $"	{ pass "ptype unsigned int array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned int array" }
#    timeout		{ fail "(timeout) ptype unsigned int array" }
#}
#
#
#send "ptype v_long_array\n"
#gdb_expect {
#    -re "type = (long|int|long int) .2..*$gdb_prompt $"	{ 
#	pass "ptype long array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype long array" }
#    timeout		{ fail "(timeout) ptype long array" }
#}
#
#
#send "ptype v_signed_long_array\n"
#gdb_expect {
#    -re "type = (long|int|long int) .2..*$gdb_prompt $"	{ 
#	pass "ptype signed long array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed long array" }
#    timeout		{ fail "(timeout) ptype signed long array" }
#}
#
#
#send "ptype v_unsigned_long_array\n"
#gdb_expect {
#    -re "type = unsigned long .2..*$gdb_prompt $"	{ pass "ptype unsigned long array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned long array" }
#    timeout		{ fail "(timeout) ptype unsigned long array" }
#}
#
#
#send "ptype v_float_array\n"
#gdb_expect {
#    -re "type = float .2..*$gdb_prompt $"	{ pass "ptype float array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype float array" }
#    timeout		{ fail "(timeout) ptype float array" }
#}
#
#
#send "ptype v_double_array\n"
#gdb_expect {
#    -re "type = double .2..*$gdb_prompt $"	{ pass "ptype double array" }
#    -re ".*$gdb_prompt $"	{ fail "ptype double array" }
#    timeout		{ fail "(timeout) ptype double array" }
#}
#

if {!$gcc_compiled} then { setup_xfail "rs6000-*-*" "i*86-*-sysv4*" }
setup_xfail_format "DWARF 1"
# CR JAGad25502  - HPC might never fix this corner case problem
if {!$gcc_compiled} then { setup_xfail ia64*-hp-* }
# HP aCC compiler does not emit debug information for a type that is not
# associated with variable; so flag the xfail as NOT_A_DEFECT
if {$hp_aCC_compiler} {setup_xfail "hppa*-*-*" NOT_A_DEFECT}
gdb_test "ptype t_char_array" "type = (|unsigned )char \\\[0?\\\]"

# Test ptype verbose mode
if {!$gcc_compiled} then { setup_xfail "rs6000-*-*" "i*86-*-sysv4*" }
setup_xfail_format "DWARF 1"
# CR JAGad25502  - HPC might never fix this corner case problem
if {!$gcc_compiled} then { setup_xfail ia64*-hp-* }
# HP aCC compiler does not emit debug information for a type that is not
# associated with variable; so flag the xfail as NOT_A_DEFECT
if {$hp_aCC_compiler} {setup_xfail "hppa*-*-*" NOT_A_DEFECT}
gdb_test "ptype -v t_char_array" "type = (|unsigned )char \\\[0?\\\]"

#
##
## test ptype command with pointers
##
#setup_xfail "i960-*-*" 1821
#setup_xfail "mips-idt-*" "mips-sgi-*" "a29k-*-*"
#send "ptype v_char_pointer\n"
#gdb_expect {
#    -re "type = char \*.*$gdb_prompt $"	{ pass "ptype char pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype char pointer" }
#    timeout		{ fail "(timeout) ptype char pointer" }
#}
#
#
#setup_xfail "mips-*-*" "a29k-*-*"
#send "ptype v_signed_char_pointer\n"
#gdb_expect {
#    -re "type = (|signed )char \*.*$gdb_prompt $"
#	{ pass "ptype signed char pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed char pointer" }
#    timeout		{ fail "(timeout) ptype signed char pointer" }
#}
#
#
#send "ptype v_unsigned_char_pointer\n"
#gdb_expect {
#    -re "type = unsigned char \*.*$gdb_prompt $"	{ pass "ptype unsigned char pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned char pointer" }
#    timeout		{ fail "(timeout) ptype unsigned char pointer" }
#}
#
#
#send "ptype v_short_pointer\n"
#gdb_expect {
#    -re "type = (short|short int) \*.*$gdb_prompt $"	{ pass "ptype short pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype short pointer" }
#    timeout		{ fail "(timeout) ptype short pointer" }
#}
#
#
#send "ptype v_signed_short_pointer\n"
#gdb_expect {
#    -re "type = short \*.*$gdb_prompt $"	{ pass "ptype signed short pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed short pointer" }
#    timeout		{ fail "(timeout) ptype signed short pointer" }
#}
#
#
#send "ptype v_unsigned_short_pointer\n"
#gdb_expect {
#    -re "type = unsigned short \*.*$gdb_prompt $"	{ pass "ptype unsigned short pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned short pointer" }
#    timeout		{ fail "(timeout) ptype unsigned short pointer" }
#}
#
#
#send "ptype v_int_pointer\n"
#gdb_expect {
#    -re "type = int \*.*$gdb_prompt $"	{ pass "ptype int pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype int pointer" }
#    timeout		{ fail "(timeout) ptype int pointer" }
#}
#
#
#send "ptype v_signed_int_pointer\n"
#gdb_expect {
#    -re "type = int \*.*$gdb_prompt $"	{ pass "ptype signed int pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed int pointer" }
#    timeout		{ fail "(timeout) ptype signed int pointer" }
#}
#
#
#send "ptype v_unsigned_int_pointer\n"
#gdb_expect {
#    -re "type = unsigned int \*.*$gdb_prompt $"	{ pass "ptype unsigned int pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned int pointer" }
#    timeout		{ fail "(timeout) ptype unsigned int pointer" }
#}
#
#
#send "ptype v_long_pointer\n"
#gdb_expect {
#    -re "type = long \*.*$gdb_prompt $"	{ pass "ptype long pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype long pointer" }
#    timeout		{ fail "(timeout) ptype long pointer" }
#}
#
#
#send "ptype v_signed_long_pointer\n"
#gdb_expect {
#    -re "type = long \*.*$gdb_prompt $"	{ pass "ptype signed long pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype signed long pointer" }
#    timeout		{ fail "(timeout) ptype signed long pointer" }
#}
#
#
#send "ptype v_unsigned_long_pointer\n"
#gdb_expect {
#    -re "type = unsigned long \*.*$gdb_prompt $"	{ pass "ptype unsigned long pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype unsigned long pointer" }
#    timeout		{ fail "(timeout) ptype unsigned long pointer" }
#}
#
#
#send "ptype v_float_pointer\n"
#gdb_expect {
#    -re "type = float \*.*$gdb_prompt $"	{ pass "ptype float pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype float pointer" }
#    timeout		{ fail "(timeout) ptype float pointer" }
#}
#
#
#send "ptype v_double_pointer\n"
#gdb_expect {
#    -re "type = double \*.*$gdb_prompt $"	{ pass "ptype double pointer" }
#    -re ".*$gdb_prompt $"	{ fail "ptype double pointer" }
#    timeout		{ fail "(timeout) ptype double pointer" }
#}

#
# test ptype command with nested structure and union
#
if { [istarget "ia64*-hp-*"]} {
if { ! $gcc_compiled } {
    set outer "outer_struct::"
    set struct ""
    set union ""
} else { 
    set outer ""
    set struct "struct"
    set union "union"
}
} else {
if {$hp_aCC_compiler} {
    set outer "outer_struct::"
    set struct ""
    set union ""
} else {
    set outer ""
    set struct "struct"
    set union "union"
}
}

send_gdb "ptype struct outer_struct\n"
gdb_expect {
   -re "type = struct outer_struct \{.*\[\r\n\]+\
.*int outer_int;.*\[\r\n\]+\
.*(struct|) ${outer}inner_struct inner_struct_instance;.*\[\r\n\]+\
.*(union|) ${outer}inner_union inner_union_instance;.*\[\r\n\]+\
.*(long|long int|int) outer_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype outer structure - aCC5 style" }

   -re "type = struct outer_struct \{.*\[\r\n\]+\
.*int outer_int;.*\[\r\n\]+\
.*(struct|) inner_struct inner_struct_instance;.*\[\r\n\]+\
.*(union|) inner_union inner_union_instance;.*\[\r\n\]+\
.*(long|long int|int) outer_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype outer structure - aCC6 style" }

  default {fail "ptype outer structure"}
}

# Test ptype verbose mode
send_gdb "ptype -v struct outer_struct\n"
gdb_expect {
   -re "type = struct outer_struct \{.*off 0 bits, len (160|320) bits.*\[\r\n\]+\
.*int outer_int;.*off 0 bits, len 32 bits.*\[\r\n\]+\
.*(struct|) ${outer}inner_struct inner_struct_instance;.*off (32|64) bits, len (64|128) bits.*\[\r\n\]+\
.*(union|) ${outer}inner_union inner_union_instance;.*off (96|192) bits, len (32|64) bits.*\[\r\n\]+\
.*(long|long int|int) outer_long;.*off (128|256) bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v outer structure - aCC5 style" }
   
   -re "type = struct outer_struct \{.*off 0 bits, len (160|320) bits.*\[\r\n\]+\
.*int outer_int;.*off 0 bits, len 32 bits.*\[\r\n\]+\
.*(struct|) inner_struct inner_struct_instance;.*off (32|64) bits, len (64|128) bits.*\[\r\n\]+\
.*(union|) inner_union inner_union_instance;.*off (96|192) bits, len (32|64) bits.*\[\r\n\]+\
.*(long|long int|int) outer_long;.*off (128|256) bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v outer structure - aCC6 style" }

  default {fail "ptype -v outer structure"}
}

if { ![istarget "ia64*-hp-*"] } then {
gdb_test "ptype ${struct} ${outer}inner_struct" "type = struct ${outer}inner_struct \{.*\[\r\n\]    int inner_int;.*\[\r\n\]    (long|long int|int) inner_long;.*\[\r\n\]\}.*" "ptype inner structure" 

# Test ptype verbose mode
gdb_test "ptype -v ${struct} ${outer}inner_struct" "type = struct ${outer}inner_struct \{.*off 0 bits, len (64|128) bits.*\[\r\n\]    int inner_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (long|long int|int) inner_long;.*off (32|64) bits, len (32|64) bits.*\[\r\n\]\}.*" "ptype -v inner structure" 

gdb_test "ptype ${union} ${outer}inner_union" "type = union ${outer}inner_union \{.*\[\r\n\]    int inner_union_int;.*\[\r\n\]    (long|long int|int) inner_union_long;.*\[\r\n\]\}.*" "ptype inner union" 

# Test ptype verbose mode
gdb_test "ptype -v ${union} ${outer}inner_union" "type = union ${outer}inner_union \{.*off 0 bits, len (32|64) bits.*\[\r\n\]    int inner_union_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (long|long int|int) inner_union_long;.*off 0 bits, len (32|64) bits.*\[\r\n\]\}.*" "ptype -v inner union" 
}

send_gdb "ptype nested_su\n"
gdb_expect {
   -re "type = struct outer_struct \{.*\[\r\n\]    int outer_int;.*\[\r\n\]    (struct |)${outer}inner_struct inner_struct_instance;.*\[\r\n\]    (union |)${outer}inner_union inner_union_instance;.*\[\r\n\]    (long|long int|int) outer_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype nested structure - aCC5 sytle"}

   -re "type = struct outer_struct \{.*\[\r\n\]    int outer_int;.*\[\r\n\]    (struct |)inner_struct inner_struct_instance;.*\[\r\n\]    (union |)inner_union inner_union_instance;.*\[\r\n\]    (long|long int|int) outer_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype nested structure  - aCC6 sytle"}

  default {fail "ptype nested structure"}
}

# Test ptype verbose mode
send_gdb "ptype -v nested_su\n"
gdb_expect {
   -re "type = struct outer_struct \{.*off 0 bits, len (160|320) bits.*\[\r\n\]    int outer_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (struct |)${outer}inner_struct inner_struct_instance;.*off (32|64) bits, len (64|128) bits.*\[\r\n\]    (union |)${outer}inner_union inner_union_instance;.*off (96|192) bits, len (32|64) bits.*\[\r\n\]    (long|long int|int) outer_long;.*off (128|256) bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v nested structure - aCC5 sytle"}

   -re "type = struct outer_struct \{.*off 0 bits, len (160|320) bits.*\[\r\n\]    int outer_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (struct |)inner_struct inner_struct_instance;.*off (32|64) bits, len (64|128) bits.*\[\r\n\]    (union |)inner_union inner_union_instance;.*off (96|192) bits, len (32|64) bits.*\[\r\n\]    (long|long int|int) outer_long;.*off (128|256) bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v nested structure  - aCC6 sytle"}

  default {fail "ptype -v nested structure"}
}

gdb_test "ptype nested_su.outer_int" "type = int\[\r\n\]" "ptype outer int" 
# Test ptype verbose mode
gdb_test "ptype -v nested_su.outer_int" "type = int\[\r\n\]" "ptype -v outer int" 

send_gdb "ptype nested_su.inner_struct_instance\n"
gdb_expect {
   -re "type = struct ${outer}inner_struct \{.*\[\r\n\]    int inner_int;.*\[\r\n\]    (long|long int|int) inner_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype nested structure #2 - aCC5 sytle"} 

   -re "type = struct inner_struct \{.*\[\r\n\]    int inner_int;.*\[\r\n\]    (long|long int|int) inner_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype nested structure #2 - aCC6 sytle"}

   default {fail "ptype nested structure #2"}
}

# Test ptype verbose mode
send_gdb "ptype -v nested_su.inner_struct_instance\n"
gdb_expect {
   -re "type = struct ${outer}inner_struct \{.*off 0 bits, len (64|128) bits.*\[\r\n\]    int inner_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (long|long int|int) inner_long;.*off (32|64) bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v nested structure #2 - aCC5 sytle"} 

   -re "type = struct inner_struct \{.*off 0 bits, len (64|128) bits.*\[\r\n\]    int inner_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (long|long int|int) inner_long;.*off (32|64) bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v nested structure #2 - aCC6 sytle"}

   default {fail "ptype -v nested structure #2"}
}

gdb_test "ptype nested_su.inner_struct_instance.inner_int" "type = int\[\r\n\]" "ptype inner int" 
# Test ptype verbose mode
gdb_test "ptype -v nested_su.inner_struct_instance.inner_int" "type = int\[\r\n\]" "ptype -v inner int" 

send_gdb "ptype nested_su.inner_union_instance\n"
gdb_expect {
   -re "type = union ${outer}inner_union \{.*\[\r\n\]    int inner_union_int;.*\[\r\n\]    (long|long int|int) inner_union_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype nested union - aCC5 style"} 

   -re "type = union inner_union \{.*\[\r\n\]    int inner_union_int;.*\[\r\n\]    (long|long int|int) inner_union_long;.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype nested union - aCC6 style"} 

   default {xfail "ptype nested union"} 
}

# Test ptype verbose mode
send_gdb "ptype -v nested_su.inner_union_instance\n"
gdb_expect {
   -re "type = union ${outer}inner_union \{.*off 0 bits, len (32|64) bits.*\[\r\n\]    int inner_union_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (long|long int|int) inner_union_long;.*off 0 bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v nested union - aCC5 style"} 

   -re "type = union inner_union \{.*off 0 bits, len (32|64) bits.*\[\r\n\]    int inner_union_int;.*off 0 bits, len 32 bits.*\[\r\n\]    (long|long int|int) inner_union_long;.*off 0 bits, len (32|64) bits.*\[\r\n\]\}.*$gdb_prompt $" {pass "ptype -v nested union - aCC6 style"} 

   default {xfail "ptype -v nested union"} 
}

# Test printing type of string constants and array constants, but
# requires a running process.  These call malloc, and can take a long
# time to execute over a slow serial link, so increase the timeout.

# UDI can't do this (PR 2416).  XFAIL is not suitable, because attempting
# the operation causes a slow painful death rather than a nice simple failure.

if [runto_main] then {

  if [target_info exists gdb,cannot_call_functions] {
    setup_xfail "*-*-*" 2416
    fail "This target can not call functions"
    continue
  }

  # We need to up this because this can be really slow on some boards.
  # (malloc() is called as part of the test).
  # Test ptype verbose mode also.
  set timeout 60;

  gdb_test "ptype \"abc\""	"type = ($char_qualifier |)char \\\[4\\\]"
  gdb_test "ptype -v \"abc\""	"type = ($char_qualifier |)char \\\[4\\\]"
  gdb_test "ptype {'a','b','c'}"	"type = char \\\[3\\\]"
  gdb_test "ptype -v {'a','b','c'}"	"type = char \\\[3\\\]"
  gdb_test "ptype {0,1,2}"		"type = int \\\[3\\\]"
  gdb_test "ptype -v {0,1,2}"		"type = int \\\[3\\\]"
  gdb_test "ptype {(long)0,(long)1,(long)2}"	  "type = long \\\[3\\\]"
  gdb_test "ptype -v {(long)0,(long)1,(long)2}"	  "type = long \\\[3\\\]"
  gdb_test "ptype {(float)0,(float)1,(float)2}" "type = float \\\[3\\\]"
  gdb_test "ptype -v {(float)0,(float)1,(float)2}" "type = float \\\[3\\\]"
  gdb_test "ptype {{0,1,2},{3,4,5}}"	"type = int \\\[2\\\]\\\[3\\\]"
  gdb_test "ptype -v {{0,1,2},{3,4,5}}"	"type = int \\\[2\\\]\\\[3\\\]"
  gdb_test "ptype {4,5,6}\[2\]"	"type = int"
  gdb_test "ptype -v {4,5,6}\[2\]"	"type = int"
  gdb_test "ptype *&{4,5,6}\[1\]"	"Attempt to dereference a non-pointer value."
  gdb_test "ptype -v *&{4,5,6}\[1\]"	"Attempt to dereference a non-pointer value."
}

# Test JAGae97453
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break 347" "Breakpoint 1.*ptype.c.*347.*" "breakpoint at line 347"
gdb_test "run" "Starting program.*main.*347.*" "breakpoint hit at line 347"

gdb_test "print struct_4" "1 = \{int_member = 101, char_member = 97 .*\}.*" "print struct_4"
set timeout $oldtimeout
