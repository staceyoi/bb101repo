# Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This file was adapted from corefile.exp to reproduce JAGaf93455,
# abort function called from gdb" by Carl Burch.  (cdb@cup.hp.com)
# This file tests that a correct error message is produced when the
# abort command is used while in a system call (doesn't work).

if $tracelevel then {
	strace $tracelevel
}

set prms_id 0
set bug_id 0


# are we on a target board
if ![isnative] then {
    return
}

set testfile "core_sighandler"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

global ADD_FLAGS
global TESTLOOP

# We loop 3 times
#   TL1   ordinary program, original loop
#   TL2   -N program
#   TL3   chatr -N from last loop to be mpas
# Only loops TL2 and TL3 are done on HP-UX.
foreach TESTLOOP "TL1 TL2 TL3" {
  if { [istarget "ia64*-hp-*"] || [istarget "hppa*-hp-hpux*" ] } {

    set ADD_FLAGS "SKIP"
    if { "$TESTLOOP" == "TL3" } then {
      if { [istarget "ia64*-hp-*"] } then {
	set ADD_FLAGS "-Wl,+as,mpas"
      } else {
	set ADD_FLAGS "SKIP"
      }
    } elseif { "$TESTLOOP" == "TL2" }  {
      if { [istarget "ia64*-hp-*"] &&  "$multipass_name" > "7" && "$multipass_name" != "17"  && "$multipass_name" != 21 && "$multipass_name" != 22 } then {
	set ADD_FLAGS "-N"
      } elseif { [istarget "hppa1.1-hp-hpux"] } then {
	set ADD_FLAGS "-N"
      } else {
	set ADD_FLAGS "SKIP"
      }
    } else {
      set ADD_FLAGS ""
    } 
  } else {
      if { "$TESTLOOP" == "TL1" } then {
	set ADD_FLAGS ""
      } else {
	set ADD_FLAGS "SKIP"
      }
  }

  if { "$ADD_FLAGS" != "SKIP" } then {
    if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable [list debug "additional_flags=$ADD_FLAGS" ]] != "" } {
	 gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
    }

    gdb_start
    gdb_reinitialize_dir $srcdir/$subdir
    gdb_load ${binfile}

    if [runto_main] then {
      # call a function that will raise a SIGBUS (signal 10)
      set msg0 "Program received signal SIGBUS, Bus error"
      set msg1 "program being debugged was signaled while in a function called from GDB"
      set msg2 "Evaluation of the expression containing the function .fun.* will be abandoned."
      gdb_test "p fun(10, 1.1)" "$msg0.*$msg1.*$msg2"

      gdb_test "bt" ".*fun.*function called from gdb.*main.*$srcfile.*"

      gdb_test "abort" ".*You may not abort a function while a system call is interrupted.*"

      gdb_test "bt" ".*fun.*function called from gdb.*main.*$srcfile.*"
    }

    gdb_exit
    remote_exec build "rm -f ${binfile}"
  }
}

