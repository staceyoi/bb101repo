#include <stdio.h>

void brk (int i, short k, char c, long l)
{
  printf("Inside function brk \n");
}

void callc (char c1, long l1, short sh, int i, char c2, char c3, char c4, long l)
{
	printf("Inside function callc.. \n");
   	brk (100, 10, 'c', 100000);
}

void callb (char c, long l, short s, int i)
{
	printf("Inside function callb.. \n");
	callc ('c', 10000, 10, 100, 'd', 'e', 'f', 20000);
}

void calla (int i, char c, long l, short sh)
{
	printf("Inside function calla.. \n");
	callb ('c', 10000, 10, 100);
}

int main()
{
	printf("Inside fun main.. \n");
	calla (10, 'c', 10000, 100);
	return 0;
}
