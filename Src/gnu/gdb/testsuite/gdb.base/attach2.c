/* This program is intended to be started outside of gdb, and then
   attached to by gdb.  Thus, it simply spins in a loop.  The loop
   is exited when & if the variable 'should_exit' is non-zero.  (It
   is initialized to zero in this program, so the loop will never
   exit unless/until gdb sets the variable to non-zero.)
   */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dl.h>
int  should_exit = 0;

int main ()
{
  int  local_i = 0;
  shl_t shlib_handle;
  sleep( 10); /* System call causes register fetch to fail */
               /* This is a known HPUX "feature"            */
  while (! should_exit)
    {
      local_i++;
    }

#if defined(__ia64)
# if defined(__LP64__)
    shlib_handle = shl_load ("/usr/lib/hpux64/libelf.so.1", BIND_DEFERRED,  0L);
# else
    shlib_handle = shl_load ("/usr/lib/hpux32/libelf.so.1", BIND_DEFERRED,  0L);
# endif
#else
# if defined(__LP64__)
    shlib_handle = shl_load ("/usr/lib/pa20_64/libelf.sl", BIND_IMMEDIATE,  0L);
# else
    shlib_handle = shl_load ("/usr/lib/libelf.sl", BIND_IMMEDIATE,  0L);
# endif
#endif
  return (0);
}
