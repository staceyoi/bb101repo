#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ttrace.h>

pid_t   ppid;

typedef struct {
  int     val;
  char    *name;
} _exp_t;


static char *
gen_name(_exp_t *base, int val)
{
  _exp_t  *rp;
  
  for (rp = base; rp->name; rp++) {
    if (val == rp->val) {
      return rp->name;
    }
  }
  return NULL;
}


static void
errexit(const char *p)
{
  (void) fprintf(stderr, "%s: %s\n", p, strerror(errno));
  if (ppid) {
    (void) kill(ppid, SIGINT);
  }
  exit (1);
}


static void
dottrace(ttreq_t req, pid_t pid, lwpid_t lwpid, uint64_t addr, uint64_t data,
         uint64_t addr2)
{
  int     rval;
  char    *p;
  static _exp_t tab[] = {
    TT_PROC_SETTRC,                 "PROC_SETTRC",
    TT_PROC_ATTACH,                 "PROC_ATTACH",
    TT_PROC_DETACH,                 "PROC_DETACH",
    TT_PROC_CONTINUE,               "PROC_CONTINUE",
    TT_PROC_SET_EVENT_MASK,         "PROC_SET_EVENT_MASK",
    TT_PROC_GET_FIRST_LWP_STATE,    "PROC_GET_FIRST_LWP_STATE",
    TT_PROC_GET_NEXT_LWP_STATE,     "PROC_GET_NEXT_LWP_STATE",
    TT_LWP_CONTINUE,                "LWP_CONTINUE",
    -1,                             NULL
  };
  
  rval = ttrace(req, pid, lwpid, addr, data, addr2);
  if (rval == -1) {
    p = gen_name(tab, req);
    errexit(p ? p : "ttrace");
  }
}


main(int argc, char **argv)
{
  pid_t           pid;
  --argc, ++argv;
  pid = atoi(*argv);
  
  if (pid) {
    dottrace(TT_PROC_ATTACH, pid, 0, TT_DETACH_ON_EXIT,
             TT_VERSION, 0);
  }
  else {
    return -1;
  }
  
  while (1);

  return 0;
}
