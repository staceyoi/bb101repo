/* source file for badpc.exp.
   Author: Bindu */

#include <stdlib.h>
int bar (float k)
{
  float i = k;
  void (*fptr) (float) = 0;
  i++;
  fptr(i);
  if (i)
    return i;
  return i;
}
int foo (int k)
{
  int i = k;
  i++;
  i = bar (100.99);
  return i;
}

int main()
{
  int i = 10;
  
  i = foo(i);
  return i;
}
