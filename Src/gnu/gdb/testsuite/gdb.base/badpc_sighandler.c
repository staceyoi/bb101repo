/* source file for badpc_sighandler.exp.
   Author: Bindu */

#include <stdlib.h>
#include <signal.h>
void
crash_handler (int sig)
{
    abort ();
}
int bar (float k)
{
  float i = k;
  void (*fptr) (float) = 0;
  i++;
  fptr(i);
  if (i)
    return i;
  return i;
}
int foo (int k)
{
  int i = k;
  i++;
  i = bar (100.99);
  return i;
}

int main()
{
  int i = 10;
  
  signal (SIGSEGV, crash_handler);
  signal (SIGBUS, crash_handler);

  i = foo(i);
  return i;
}

