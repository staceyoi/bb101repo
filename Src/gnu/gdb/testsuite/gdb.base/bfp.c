/* Program for testing floating point constant and floating point variables */
#include <stdio.h>

int
main()
{
  float var1;
  double var2;
  long double var3;

  var1 = 12.34f;
  var2 = 3.4567;
  var3 = 12.345678l;

  printf("%f\n%f\n%f\n",var1,var2,var3);

}
