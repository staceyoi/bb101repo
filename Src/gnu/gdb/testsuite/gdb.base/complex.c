
float _Complex glob_float_complex = 1 + 2*_Imaginary_I;
double _Complex glob_double_complex = 3 + 4*_Imaginary_I;
long double _Complex glob_long_double_complex = 4 + 5*_Imaginary_I;
__float80 _Complex glob_float80_complex = 5 + 6*_Imaginary_I;


short glob_short = 3;
int glob_int = 4;
long glob_long = 5;
long long glob_long_long = 6;

float glob_float = 10;
double glob_double = 11;
long double glob_long_double = 12;
__float80 glob_float80 = 13;


int complex_args (float _Complex arg_float_complex,
		  double _Complex arg_double_complex,
		  long double _Complex arg_long_double_complex)
{
  return (sizeof(  arg_float_complex) 
		 + sizeof(arg_double_complex) 
		 + sizeof (arg_long_double_complex));
}

int float80_args (__float80 float80_arg,
		  __float80 _Complex float80_complex_arg)
{
  int return_value;

  return_value = float80_complex_arg;
  return_value += float80_arg;
  return return_value;
}

int main()
{
  int	value;

  short loc_short;
  int loc_int;
  long loc_long; 
  long long loc_long_long;
  float loc_float;
  double loc_double;
  long double loc_long_double;
  __float80 loc_float80;

  float _Complex loc_float_complex = glob_float_complex;
  double _Complex loc_double_complex = glob_double_complex;
  long double _Complex loc_long_double_complex = glob_long_double_complex;
  __float80 _Complex loc_float80_complex = glob_float80_complex;
  __float80 _Complex float80_complex_init = 10 + 11*_Imaginary_I;
  loc_short = loc_int = loc_float = loc_double = loc_long_double 
	= loc_float80 = loc_long_long = loc_long = 2;
  
  /* Reference the globals */

  value =   glob_short + glob_int + glob_long + glob_long_long 
	  + glob_float + glob_double + glob_long_double +glob_float80;

  value = complex_args (loc_float_complex, 
			loc_double_complex, 
			loc_long_double_complex);
  value = float80_args (glob_float80,
			glob_float80_complex);
  return sizeof(loc_float_complex);
}
