// This is a test program for C++ virtual functions/
// constructors and destructors
//
// Author: Jim Weirich
// Altered: Brian Fahs
//
#include <stdio.h>
#include <unistd.h>

static long loops_per_percent = 20000;
static int total_percent = 0; 

static int i, bound;

/* Invoke this macro to use up a given percentage of the total run time.
 */
#define USE_SOME_TIME(perc)                       {   \
    bound = perc * loops_per_percent;                 \
    for (i=0; i<bound; i++)                           \
        ;                                             \
    total_percent += perc;                            \
}

class Pixel
{
public:
  Pixel() { printf("PIXEL: Creating...\n"); USE_SOME_TIME (1);}
  ~Pixel() { printf("PIXEL: Destroying...\n"); USE_SOME_TIME (1);}
};

class Coloring
{
public:
  Pixel pixels[5];
  Coloring(); 
  ~Coloring();
private:
  int* mycolor;
};

Coloring::Coloring()
{
  printf("COLORING: Creating...\n");
  USE_SOME_TIME (1);
  mycolor = new int;
}

Coloring::~Coloring()
{
  printf("COLORING: Destroying...\n");
  USE_SOME_TIME (15);
  delete mycolor;
}

class Shape
{
public:
    virtual void Draw() = 0;
    virtual void MoveTo (int newx, int newy) = 0;
    virtual void RMoveTo (int dx, int dy) = 0;
    virtual void Hello() = 0;
    Shape();
    virtual ~Shape();
};

Shape::Shape ()
{
  USE_SOME_TIME (1);
  printf("SHAPE: Creating...\n");
}

Shape::~Shape ()
{
  USE_SOME_TIME (1);
  printf("SHAPE: Destroying...\n");
}

// Class Rectangle

class Rectangle: public Shape
{
public:
    Rectangle (int x, int y, int w, int h);
    ~Rectangle() { printf("RECTANGLE: Destroying...\n"); }
    virtual void Draw ();
    virtual void MoveTo (int newx, int newy);
    virtual void RMoveTo (int dx, int dy);
    virtual void SetWidth (int newWidth);
    virtual void SetHeight (int newHeight);
    virtual void Hello();
    void Test();
private:
    int x, y;
    int width;
    int height;
};
    
void Rectangle::Draw ()
{
  printf("Drawing a Rectangle at (%d,%d), width %d, height %d\n",
         x,y,width,height);
};

void Rectangle::Hello()
{
    printf("Rectangle Hello!\n");
}

void Rectangle::Test()
{
  USE_SOME_TIME (5);
  printf("RECTANGLE: Testing...\n");
}

void Rectangle::MoveTo (int newx, int newy)
{
  printf("RECTANGLE: Moving to (%d,%d)\n",newx,newy);
  x = newx;
  y = newy;
}

void Rectangle::RMoveTo (int dx, int dy)
{
  printf("RECTANGLE: Shifting by %d in the x-dir and %d in the y-dir\n",
         dx,dy);
  x += dx;
  y += dy;
}

void Rectangle::SetWidth (int newWidth)
{
  printf("RECTANGLE: Setting Width to %d\n",newWidth);
  width = newWidth;
}

void Rectangle::SetHeight (int newHeight)
{
  printf("RECTANGLE: Setting Height to %d\n",newHeight);
  height = newHeight;
}

Rectangle::Rectangle (int initx, int inity, int initw, int inith)
{
  printf("RECTANGLE: Constructing...\n");
  x = initx;
  y = inity;
  width = initw;
  height = inith;
}

/* Class Circle */

class Circle : public Shape
{
public:
    Coloring mycolor;
    Circle (int initx, int inity, int initr);
    virtual void Draw ();
    virtual void MoveTo (int newx, int newy);
    virtual void RMoveTo (int dx, int dy);
    virtual void SetRadius (int newRadius);
    virtual void Hello();
    ~Circle();
private:
    int x, y;
    int radius;
};

void Circle::Draw ()
{
  printf("Drawing a Circle at (%d,%d), radius %d\n",x,y,radius);
}

void Circle::MoveTo (int newx, int newy)
{
  printf("CIRCLE: Moving to (%d,%d)\n",newx,newy);
  x = newx;
  y = newy;
}

void Circle::Hello ()
{
    printf("CIRCLE: Hello\n");
}

void Circle::RMoveTo (int dx, int dy)
{
  printf("CIRCLE: Shifting by %d in the x-dir and %d in the y-dir\n",dx,dy);
  x += dx;
  y += dy;
}

void Circle::SetRadius (int newRadius)
{
  printf("CIRCLE: Setting radius to %d\n",newRadius);
  radius = newRadius;
}

Circle::Circle (int initx, int inity, int initr)
{
  printf("CIRCLE: Constructing...\n");
  x = initx;
  y = inity;
  radius = initr;
}

Circle::~Circle ()
{
  USE_SOME_TIME (1);
  printf("CIRCLE: Destroying...\n");
}

/* ===================================================================
 * DoSomethingWithShape is a fuction that takes a polymorphic shape
 * and manipulates it according to its interface.
 */

void DoSomethingWithShape (Shape * s)
{
  USE_SOME_TIME (50);
  s->Draw ();
  s->RMoveTo (100, 100);
  s->Draw ();
}

void say_hello()
{
  USE_SOME_TIME (1);
  printf("Hello...\n");
}

/* =================================================================== 
 * Main Program
 */

typedef void (* function_t)(void);

int main ()
{
  
  Pixel pixelarray[5];
  function_t myhello[2];

  myhello[0] = say_hello;
  //myhello[1] = &(Rectangle::Hello);
  //myhello[1] = (void (*)())(&Rectangle::Hello);

  /* using shapes polymorphically */
  Shape * shapes[2];
  shapes[0] = new Rectangle (10, 20, 5, 6);
  shapes[1] = new Circle (15, 25, 8);
  
  for (int i=0; i < 2; ++i) 
  {
      DoSomethingWithShape (shapes[i]);
  }
  
  delete shapes[0];
  delete shapes[1];
  
  /* access a rectangle specific function */
  
  Rectangle * rect = new Rectangle (0, 0, 15, 15);
  rect->Rectangle::SetWidth (30);
  rect->Rectangle::Draw ();
  rect->Rectangle::Test ();
  rect->Hello();
  delete rect;

  Coloring * colors = new Coloring();

  delete colors;

  say_hello();

  (*myhello[0])();
  //(*myhello[1])();

  return 0;
}
