# Copyright (C) 1992, 1994, 1995, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This file was written by Himabindu Vuppula.
# Test for deferred watchpoints.

set prms_id 0
set bug_id 0

set srcfile "deffered-watch.c"
set binfile "${objdir}/${subdir}/deffered-watch"

if { [gdb_compile "${srcdir}/${subdir}/${srcfile} " ${binfile} executable {debug}] != "" } {
        gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Start with a fresh gdb
gdb_start
gdb_reinitialize_dir ${srcdir}/${subdir}/
gdb_load ${binfile}
gdb_test "b main" "Breakpoint 1 at $hex: file ../../../../Src/gnu/gdb/testsuite/gdb.base/deffered-watch.c.*" "break at main"

gdb_test "r" ".*Starting program: $binfile .*Breakpoint 1, main.*" "run to main"

gdb_test "b 11" ""
gdb_test "cont" "" ""
gdb_test "p cc" ".*$hex.*" "read addr"
set addr 0
send_gdb "p cc\n"
gdb_expect {
  -re ".*($hex).*" {
	set addr $expect_out(1,string)
	pass "read in addr $addr"
  }
  -re ".*$gdb_prompt $" {
	fail "reading addr";
	return 0;
  }
  default {fail "reading addr"
	return 0
  }
}

gdb_exit
gdb_start
gdb_reinitialize_dir ${srcdir}/${subdir}/
gdb_load ${binfile}
gdb_test "b main" "Breakpoint 1 at $hex: file ../../../../Src/gnu/gdb/testsuite/gdb.base/deffered-watch.c.*" "break at main"

gdb_test "r" ".*Starting program: $binfile .*Breakpoint 1, main.*" "run to main"

gdb_test "print 1234" ".* = 1234"
gdb_test "watch *((char*)$addr)" ".*deferred.*" "place a deferred watchpoint"
gdb_test "continue" "Hardware watchpoint.*Old value = 0.*New value = 97.*a.*" "Hit the deferred wtachpoint"
gdb_test "continue" "Hardware watchpoint.*Old value = 97.*a.*New value = 98.*b.*" "Hit the deferred wtachpoint"
gdb_test "continue" ".*exited normally.*" "Program exit"

gdb_test "run" ".*main.*" "rerun the program"
gdb_test "continue" "Hardware watchpoint.*Old value = 0.*New value = 97.*a.*" "Hit the deferred wtachpoint"
gdb_test "continue" "Hardware watchpoint.*Old value = 97.*a.*New value = 98.*b.*" "Hit the deferred wtachpoint"
gdb_exit

# Let's try a different scenario now
# In the first run, the watchpoint will be normal one and on the
# rerun, it turns into a deffered watchpoint

gdb_start
gdb_reinitialize_dir ${srcdir}/${subdir}/
gdb_load ${binfile}
gdb_test "b main" "Breakpoint 1 at $hex: file ../../../../Src/gnu/gdb/testsuite/gdb.base/deffered-watch.c.*" "break at main"

gdb_test "r" ".*Starting program: $binfile .*Breakpoint 1, main.*" "run to main"

gdb_test "print 1234" ".* = 1234"

gdb_test "break 11" "Breakpoint 2 at .*" "break at line 11"
gdb_test "continue" "Breakpoint 2.*" "continue to line 11"
gdb_test "clear" "" "clear the breakpoint at line 11"
gdb_test "watch *((char*)$addr)" "Hardware watchpoint.*" "Place a watchpoint"
gdb_test "continue" "Hardware watchpoint.*Old value = 0.*New value = 97.*a.*" "Hit the watchpoint"
gdb_test "continue" "Hardware watchpoint.*Old value = 97.*a.*New value = 98.*b.*" "Hit the watchpoint"
gdb_test "continue" ".*exited normally.*" "Program exit"

gdb_test "run" ".*main.*" "rerun the program"
gdb_test "continue" "Hardware watchpoint.*Old value = 0.*New value = 97.*a.*" "Hit the deferred watchpoint"
gdb_test "continue" "Hardware watchpoint.*Old value = 97.*a.*New value = 98.*b.*" "Hit the deferred watchpoint"
gdb_exit

