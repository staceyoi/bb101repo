#include <stdlib.h>
#include <stdio.h>
#include <sys/signal.h>
#include <unistd.h>

void
myhandler (int i)
{
  printf ("I came here %d\n",i);
  abort ();
}

int
main(int argc, char **argv)
{
  int alarm_time = 0, i;

  if (argc >1)
    alarm_time = atoi (argv[1]);

  printf ("%d is argv1\n", alarm_time);
  signal (SIGALRM, myhandler);

  if (alarm_time != 0) 
    alarm (alarm_time);

  pause ();
  return (0);
}
