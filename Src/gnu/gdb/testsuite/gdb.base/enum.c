#include <stdio.h>

typedef enum a_color
{ 
  RED, ORANGE, YELLOW, GREEN, BLUE
} color;

typedef enum a_number
{ 
  ONE = 1, TWO = 2, EIGHT = 8, SIXTEEN = 16
} number;

int
main ()
{
  /* We get these for free */
  color a = BLUE;
  color b = (color) 0;

  /* Logical and's and or's don't really make sense here, but test
     them anyway */
  color c = (color) (YELLOW && GREEN);
  color d = (color) (RED || BLUE);

  /* Results of these are numbers that correspond to given enum colors
     so this is not new */
  color e = (color) (RED & GREEN);
  color f = (color) (ORANGE | GREEN);

  /* These are new */
  number l = (number) 3;
  number m = (number) 4;
  number n = (number) 5;
  number o = (number) 9;
  number p = (number) 12;
  number q = (number) 13;
  number r = (number) 24;
  number s = (number) 25;
  number t = (number) 27;
  number u = (number) 28;
  number v = (number) 29;
  number w = (number) 32;
  number x = (number) 33;

  return (0);
}
