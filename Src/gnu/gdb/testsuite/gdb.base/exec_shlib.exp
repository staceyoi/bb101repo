# Copyright (C) 1992, 1994, 1995, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Author: Bindu (bindu@cup.hp.com)
# To test the enabling of shared library breakpoints after exec.

if $tracelevel then {
	strace $tracelevel
}

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] then {
    return
}

set testfile1 "exec1"
set srcfile1 ${testfile1}.c
set testfile2 "exec2"
set srcfile2 ${testfile2}.c
if { [istarget "ia64*-hp-*"] } then {
  set sharefile "sharedlib.so"
} else {
  set sharefile "sharedlib.sl"
}
set binfile1 ${objdir}/${subdir}/${testfile1}
set binfile2 ${objdir}/${subdir}/${testfile2}

# bump timeout variable so new packcore commands can take longer
set oldtimeout $timeout

remote_exec build "rm -rf gdb.base/${sharefile} gdb.base/real_${sharefile} packcore.tar.Z packcore.tar packcore"

# Create and source the file that provides information about the compiler
# used to compile the test case.
if [get_compiler_info ${binfile1}] {
    return -1;
}

if {$gcc_compiled} {
    return;
}
if  { [gdb_compile "${srcdir}/${subdir}/${srcfile1}" "${binfile1}.o" object {debug}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}
if  { [gdb_compile "${srcdir}/${subdir}/${srcfile2}" "${binfile2}.o" object {debug}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Compile the exec_shlib.c
if  { [gdb_compile "${srcdir}/${subdir}/exec_shlib.c" "${objdir}/${subdir}/exec_shlib.o" object [list debug "additional_flags=+z"]] != ""} {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}
# Create the shared library from exec_shlib.o
if { [istarget "hppa*-*-hpux*"] || [istarget "ia64*-hp-*"] } then {
	remote_exec build "ld -b ${objdir}/${subdir}/exec_shlib.o -o ${objdir}/${subdir}/${sharefile}"
}

#Create the executable
if {[gdb_compile "${objdir}/${subdir}/${testfile1}.o ${objdir}/${subdir}/${sharefile}" "${binfile1}" executable {debug}] != ""} {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}
if {[gdb_compile "${objdir}/${subdir}/${testfile2}.o ${objdir}/${subdir}/${sharefile}" "${binfile2}" executable {debug}] != ""} {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}


gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile1}

gdb_test "break main" ".*Breakpoint 1 at .*exec1.c.*" "breakpoint at main"
gdb_test "break func_b" ".*Breakpoint 2.*deferred.*func_b.*" "breakpoint at func_b"
gdb_test "run" ".*Breakpoint 1, main.*exec1.c.*" "Hit breakpoint at main before exec"
gdb_test "continue" ".*Breakpoint 2, func_b.*exec_shlib.c.*" "Hit breakpoint at func_b before exec"
gdb_test "continue" ".*Temporarily disabling or deleting shared library breakpoints.*Breakpoint 1, main.*exec2.c.*" "Hit breakpoint at main after exec"
gdb_test "continue" ".*Breakpoint 2, func_b.*exec_shlib.c.*" "Hit breakpoint at func_b after exec"
gdb_test "continue" ".*Program exited .*" "End of program"
gdb_exit

#
# Test packcore.  If the shared library is a symbolic link, we need to pack
# up the library pointed to - JAGaf46410
# First, move the shared library and replace it with a link.  THen
# we make a core file.  Then we use packcore to pack it.
#
remote_exec build "mv gdb.base/${sharefile} gdb.base/real_${sharefile}"
remote_exec build "ln -s real_${sharefile} gdb.base/${sharefile}"
remote_exec build "rm -rf tar_dir unpack_dir"
remote_exec build "mkdir tar_dir unpack_dir"

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile1}

if { ![ runto_main] } {
  fail "Unable to run to main."
  return 0
}

set timeout 160
set corefile "Read_of_Corefile_Name_Failed"
send_gdb "dumpcore\n"
gdb_expect {
  -re "Dumping core to the core file (core.\[0-9\]+).*$gdb_prompt $" {
    set corefile $expect_out(1,string)
    pass "dumpcore"
  }
  -re ".*$gdb_prompt $" { fail "dumpcore - output did not match" }
  timeout           { fail "(timeout) dumpcore" }    
}
gdb_exit

# Now do packcore
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile1} 
#
# There is a problem with older systems and dumpcore.  The "set live-core 1"
# command makes the core files created work for non-threaded programs.
#
gdb_test "set live-core 1"
gdb_test "core ${corefile}" "Core was generated.*6.*func_b.*"
send_gdb "packcore\n" 
gdb_expect {
             -re ".*The core file has been added.*Do you want to remove.* $"
           }
gdb_test "n" \
             ".*The core file is not removed.*" \
             "The file is not removed" 
             
gdb_exit


# Now do unpackcore
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile1} 
gdb_test "set live-core 1"
send_gdb "unpackcore\n"
gdb_expect {
            -re "Unpackcore of.*is done.*Do you want to remove.* $"
             { pass "unpackcore done" }
           } 
gdb_test "n" ".*Core was generated.*6.*func_b.*"
gdb_test "bt" ".*0  main.*exec1.c:6"
gdb_exit


# remote_exec build "rm -f gdb.base/${sharefile} gdb.base/real_${sharefile}"
remote_exec build "rm -rf ${corefile} tar_dir unpack_dir packcore.tar.Z packcore.tar packcore"

set timeout $oldtimeout
