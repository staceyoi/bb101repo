
int		var_int = 1;
float		var_float = 2.2;
double		var_double = 3.3;
long double	var_long_double = 4.4;
__float80	var_float80 = 5.5;

__float80 func_float80 (__float80 arg_float80)
{
  return arg_float80 + var_float80;
}

int main()
{

  int		local_var_int = 1;
  float		local_var_float = 2.2;
  double		local_var_double = 3.3;
  long double	local_var_long_double = 4.4;
  __float80	local_var_float80 = 5.5;

  local_var_float80 = func_float80 (6.6);
  local_var_int = var_int;
  local_var_int = var_float;
  local_var_int = var_double;
  local_var_int = var_long_double;
  local_var_int = var_float80;

  return 0;
}
