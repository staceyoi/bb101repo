#include <stdio.h>
#include <unistd.h>

#ifdef PROTOTYPES
void callee (int i)
#else
void callee (i)
  int  i;
#endif
{
  printf("callee: %d\n", i);
}

int watch_me = 0;
int watch_me1 = 0;
int watch_me2 = 0;
int watch_me3 = 0;
int watch_me4 = 0;

#ifdef PROTOTYPES
int main (void)
#else
main ()
#endif
{
  int  pid;
  int  v = 5;
  watch_me = 5;
  watch_me1 = 5;
  watch_me2 = 5;
  watch_me3 = 5;
  watch_me4 = 5;

  pid = fork ();
  if (pid == 0)
    {
      v++;
      watch_me++;
      /*printf ("I'm the child!\n");*/
    }
  else
    {
      v--;
      watch_me--;
      sleep(1);
      /*printf ("I'm the proud parent of child #%d!\n", pid); */
    }
}
