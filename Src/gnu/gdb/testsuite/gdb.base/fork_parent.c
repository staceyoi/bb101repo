#include <sys/stat.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<dl.h>

int main() {
   int fork_me;
   fork_me = fork();
   if ( fork_me == 0 ) {
      printf ("I am child" );
      exit(0);
   } else {
      printf ("I am parent \n" );
      shl_load ("vfork_share.sl",BIND_DEFERRED,0);
   }
}
