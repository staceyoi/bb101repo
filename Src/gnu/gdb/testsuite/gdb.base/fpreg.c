#define _FPWIDETYPES
#include <float.h>
#include <machine/sys/inline.h>
#include <stdio.h>

int		var_int = 1;
float		var_float = 2.2;
double		var_double = 3.3;
long double	var_long_double = 4.4;
__float80	var_float80 = 6.6;
__fpreg		var_fpreg = 5.5;

__fpreg func_fpreg (__fpreg arg_fpreg)
{
  return arg_fpreg + var_fpreg;
}

int main()
{
  extended w = EXT_MAX;
  int		local_var_int = 1;
  float		local_var_float = 2.2;
  double		local_var_double = 3.3;
  long double	local_var_long_double = 4.4;
  __float80	local_var_float80 = 6.6;
  __fpreg	local_var_fpreg = 5.5;

  local_var_fpreg = func_fpreg (6.6);
  local_var_int = var_int;
  local_var_int = var_float;
  local_var_int = var_double;
  local_var_int = var_long_double;
  local_var_int = var_float80;
  local_var_int = var_fpreg;

  local_var_fpreg = _Asm_fmpy(_PC_NONE, w, 2.0, _SF1);

  return 0;
}
