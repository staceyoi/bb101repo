
/* info.c -Source file for info.exp.  Intended for use by various info command
 * 	tests.
 */

/* Declare variables of various kinds.  Make sure they are used in main. */

	int	glob_int;
	short	glob_short;
	long	glob_long;
	long long	glob_long_long;
	unsigned int	glob_uint;
	float	glob_float;
	double	glob_double;
	struct glob_struct_s { int int_field;  long long_field; };
	struct glob_struct_s glob_struct;

/* Variables used to control the execution */

	int glob_count = 0;
	int go_deeper = 1000;
        int* glob_addr = 0;

/* Leave a bunch of empty lines for later source changes which can be placed
 * here without chaning subsequent line numbers.  ~200 lines
 */






















































































































































































































int call_self (int caller_count)
{
  int i = 110;
  int my_count = glob_count++;
  int* my_count_addr = &my_count;

  int my_loc_1 = my_count + 1;
  int my_loc_2 = my_count + 2;
  unsigned int my_loc_3 = my_count + 3;
  int my_loc_4 = -1;
  int my_loc_5 = 0;

  my_count_addr = & i;
  my_loc_5 = my_loc_1 + my_loc_2 + my_loc_3 + my_loc_4 + my_loc_5;
  if (glob_count == 1)
    {
      my_loc_3 = (unsigned int) 0xface;
      glob_addr = &my_count;
    }

  go_deeper--;
  {
    int i = 111;

    my_count_addr = & i;
    if (go_deeper > 0)
      {
	int i = 112;

	my_count_addr = & i;
	return call_self (my_count);
      }
    else
      return my_count + (my_count_addr != my_count_addr);
  }
}


int main()
{
  call_self (0);
  return 0;
}
