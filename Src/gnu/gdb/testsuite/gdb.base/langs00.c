/* This file is actually in C, it is not supposed to simulate something
   translated from another language or anything like that.  */
#ifdef PROTOTYPES
extern  int fsub_();

int csub (int x)
#else
int
csub (x)
     int x;
#endif
{
  return x + 1;
}

int
_ZN2do6langs0Ev ()
{
  return fsub_ () + 2;
}

int
main ()
{
#ifdef usestubs
  set_debug_traps();
  breakpoint();
#endif
  if (_ZN2do6langs0Ev () == 5003)
    /* Success.  */
    return 0;
  else
    return 1;
}
