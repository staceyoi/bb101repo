/* This is intended to be a vague simulation of cfront output.  */
#ifdef PROTOTYPES
#line 1 "langs2.cxx"
extern int csub (int);
int
_Z3fooi (int x)
{
  return csub (x / 2);
}

extern int cppsub_ (int *y);
int
cppsub_ (int * y)
{
  return _Z3fooi (*y);
}
#else 
#line 1 "langs2.cxx"
extern int csub ();
int
_Z3fooi (x) int x;
{
  return csub (x / 2);
}

extern int cppsub_ ();
int
cppsub_ (y) int *y;
{
  return _Z3fooi (*y);
}
#endif
