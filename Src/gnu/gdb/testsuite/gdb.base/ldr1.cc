#include <stdio.h>
#include "ldr3.h"

extern void foo();
extern void bar();
extern void baz();

extern int acomm;

int main3()
{
  return 1;
}

int main2()
{
  return 0;
}

int main()
{
  acomm = 1;
  (new Info<PP>)->p(new PP);
  foo();
  bar();
  baz();

  return 0;
}
