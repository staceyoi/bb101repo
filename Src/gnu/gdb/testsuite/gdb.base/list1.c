#include <stdio.h>

void long_line (); 
#ifdef PROTOTYPES
void bar (int x)
#else
void bar (x) int x;
#endif
{
    printf ("%d\n", x);

    long_line ();
}

static void
unused ()
{
    /* Not used for anything */
}
/* This routine has a very long line that will break searching in older versions of GDB.  */

void

long_line ()
{
  bar (67);

  bar (6789);
}
