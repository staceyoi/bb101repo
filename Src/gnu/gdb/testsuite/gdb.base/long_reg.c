/* Test whether registers contents are printed properly
 *  when a 32 bit program is executed on 64 bit machine.
 *  A long long value has to be stored in a single 64 bit
 *  register when the program is executed on 64 bit machine.
 *
 * /CLO/BUILD_ENV/Exports/cc -g +e -o long_reg long_reg.c
 *
 * or
 *
 * cc +e +DA2.0 -g -o long_reg long_reg.c
 */

#include <stdio.h>

long long a = 0x0102030405060708;
long long b = 0x0807060504030201;

long long fn(long long c, long long d)
{
   return c + d;
}

int main() {
   long long c = fn(a, b);
   return 0;
}
