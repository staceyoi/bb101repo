	.file	"a.c"
	.psr	msb
	.radix	C
	.global foo
	.type	foo,@function

// Routine 135 ("bar"::103)

	.type	bar,@function
	.radix	C

	.psr	abi64

	.psr	msb

	.section .text = "ax", "progbits"
	.proc	bar
//      $start          CMid904 =               ;; // A

bar:
//file/line/col a.c/7/1
//      $entry          CMid908, r32 =             // A [a.c: 7/1]
        alloc           r34 = ar.pfs, 1, 6, 0, 0   // M [a.c: 7/1] [UVU: ]
        add             r16 = 0, sp                // M [a.c: 7/1] [UVU: ]
        mov             r35 = rp                   // I [a.c: 7/1]
        add             sp = -32, sp               // M [a.c: 7/1] [UVU: ]
        add             r38 = 0, gp                // M
        add             r9 = 0, r32             ;; // I [a.c: 7/1]
        add             r36 = -16, r16             // M [a.c: 7/1]
//      $fence                                     // A [a.c: 7/1] [UVU: ]
        add             r37 = -48, sp              // M [a.c: 7/1]
        add             r33 = 0, r9                // I [a.c: 7/1] [UVU: i]
//file/line/col a.c/7/1/E,8/2
        add             r9 = @ltoff(.tcg1$rodata), r0 ;; // M [a.c: 8/2]
        add             r10 = r9, gp               // M [a.c: 8/2]
        add             r11 = 8, r36               // I [a.c: 8/2]
        add             r9 = 16, r36            ;; // M [a.c: 8/2]
        ld8             r15 = [r10]                // M [a.c: 8/2]
        nop.i           0                       ;; // I
        add             r14 = 8, r15               // M [a.c: 8/2]
        add             r10 = 16, r15           ;; // I [a.c: 8/2]
        nop.i           0                          // I
        ld8             r15 = [r15]             ;; // M [a.c: 8/2]
        st8             [r36] = r15                // M [a.c: 8/2] [UVU]
        nop.i           0                       ;; // I
        ld8             r14 = [r14]             ;; // M [a.c: 8/2]
        st8             [r11] = r14                // M [a.c: 8/2] [UVU]
        nop.i           0                       ;; // I
        ld4             r10 = [r10]             ;; // M [a.c: 8/2]
        st4             [r9] = r10                 // M [a.c: 8/2] [UVU]
//file/line/col a.c/9/1
        add             r9 = @gprel(j), r0         // I [a.c: 9/1]
        add             r10 = @gprel(.tcg0$sdata), r0 ;; // M [a.c: 9/1]
        add             r9 = r9, gp                // M [a.c: 9/1]
        add             r10 = r10, gp           ;; // I [a.c: 9/1]
        ld4             r9 = [r9]                  // M [a.c: 9/1]
        ld4             r10 = [r10]                // M [a.c: 9/1]
        nop.i           0                       ;; // I
        add             r9 = r9, r10            ;; // M [a.c: 9/1]
        add             r33 = r33, r9              // M [a.c: 9/1] [UVU: i] [UVuse]
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
//file/line/col a.c/11/1
        br.call.dptk.few rp = foo               ;; // B [a.c: 11/1] [UVU]
        add             gp = 0, r38                // M [a.c: 11/1]
        mov             rp = r35                   // I [a.c: 11/1]
        add             sp = 32, sp             ;; // I [a.c: 11/1] [UVU: ]
        nop.m           0                          // M
        mov             ar.pfs = r34               // I [a.c: 11/1]
        br.ret.dptk.few rp                      ;; // B [a.c: 11/1]

//      $end                                    ;; // A

	.endp	bar

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.text)
	data8.ua @segrel(.text+240)
	data8.ua @segrel(.tcg$unwind_info_block0000)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 8
	data8.ua @segrel(.llo$annot_info_block0000)
.tcg$unwind_info_block0000:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x03
	data1	0x07, 0xe4, 0x02, 0xb0, 0xa3, 0xe6, 0x00, 0xb1
	data1	0x22, 0xe0, 0x03, 0x02, 0xe2, 0x00, 0x61, 0x26
	data1	0x81, 0xc0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00

// ===


// Routine 164 ("baz"::103)

	.type	baz,@function
	.section .text = "ax", "progbits"
	.proc	baz
//      $start          CMid904 =               ;; // A

baz:
//file/line/col a.c/16/1
//      $entry          CMid908, f8 =              // A [a.c: 16/1]
        alloc           r32 = ar.pfs, 0, 3, 1, 0   // M [a.c: 16/1] [UVU: ]
        add             r16 = 0, sp                // M [a.c: 16/1] [UVU: ]
        fmerge.s        f6 = f8, f8                // F [a.c: 16/1]
        add             sp = -48, sp               // M [a.c: 16/1] [UVU: ]
        mov             r33 = rp                ;; // I [a.c: 16/1]
        add             r17 = 0, r16               // I [a.c: 16/1]
        add             r34 = -32, r16             // M [a.c: 16/1]
        add             out0 = -48, sp          ;; // I [a.c: 16/1]
        nop.i           0                          // I
        stf.spill       [r17] = f16, 16            // M [a.c: 16/1] [UVuse]
//      $fence                                     // A [a.c: 16/1] [UVU: ]
        nop.i           0                          // I
        nop.b           0                       ;; // B
        nop.m           0                          // M
        fmerge.s        f16 = f6, f6               // F [a.c: 16/1] [UVU: i]
//file/line/col a.c/16/1/E,17/2
        add             r10 = @ltoff(.tcg1$rodata), gp // I [a.c: 17/2]
        add             r11 = 8, r34               // M [a.c: 17/2]
        add             r9 = 16, r34            ;; // I [a.c: 17/2]
        nop.i           0                          // I
        ld8             r10 = [r10]                // M [a.c: 17/2]
        nop.i           0                       ;; // I
        add             r10 = 32, r10           ;; // I [a.c: 17/2]
        ld8             r15 = [r10]                // M [a.c: 17/2]
        add             r14 = 8, r10               // I [a.c: 17/2]
        add             r10 = 16, r10           ;; // I [a.c: 17/2]
        st8             [r34] = r15             ;; // M [a.c: 17/2] [UVU]
        ld8             r14 = [r14]                // M [a.c: 17/2]
        nop.i           0                       ;; // I
        st8             [r11] = r14             ;; // M [a.c: 17/2] [UVU]
        ld4             r10 = [r10]                // M [a.c: 17/2]
        nop.i           0                       ;; // I
        st4             [r9] = r10                 // M [a.c: 17/2] [UVU]
//file/line/col a.c/18/1
        fadd.s.s0       f16 = f16, f1              // F [a.c: 18/1] [UVU: i] [UVuse]
//file/line/col a.c/19/1
        add             r9 = 4, r34             ;; // I [a.c: 19/1]
        ld4             r9 = [r9]                  // M [a.c: 19/1]
        nop.i           0                       ;; // I
        add             r9 = 0, r9              ;; // I [a.c: 19/1]
        add             out0 = 0, r9               // M [a.c: 19/1]
        nop.m           0                          // M
        br.call.dptk.few rp = bar               ;; // B [a.c: 19/1] [UVU]
        add             r18 = 48, sp               // M [a.c: 19/1]
        mov             rp = r33                ;; // I [a.c: 19/1]
        mov             ar.pfs = r32               // I [a.c: 19/1]
        add             r19 = 0, r18            ;; // M [a.c: 19/1]
        ldf.fill        f16 = [r19], 16            // M [a.c: 19/1]
        add             sp = 0, r18                // I [a.c: 19/1] [UVU: ]
        nop.m           0                          // M
        nop.m           0                          // M
        br.ret.dptk.few rp                      ;; // B [a.c: 19/1]

//      $end                                    ;; // A

	.endp	baz

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.text)
	data8.ua @segrel(.text+256)
	data8.ua @segrel(.tcg$unwind_info_block0001)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 8
	data8.ua @segrel(.llo$annot_info_block0001)
.tcg$unwind_info_block0001:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x04
	data1	0x0a, 0xe4, 0x04, 0xb0, 0xa1, 0xe6, 0x00, 0xb1
	data1	0x20, 0xb9, 0x00, 0x00, 0x10, 0xb8, 0x00, 0x00
	data1	0x10, 0xe0, 0x03, 0x03, 0xe2, 0x00, 0x61, 0x26
	data1	0x81, 0xc0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00

// ===

	.type	my_init,@function

// Routine 179 ("my_init"::101)

	.section .text = "ax", "progbits"
	.proc	my_init
	.global	my_init
//      $start          CMid904 =               ;; // A

my_init:
//file/line/col a.c/24/1
//      $entry          CMid908 =                  // A [a.c: 24/1]
        alloc           r33 = ar.pfs, 0, 5, 0, 0   // M [a.c: 24/1] [UVU: ]
        add             r36 = -48, sp              // M [a.c: 24/1]
        mov             r34 = rp                   // I [a.c: 24/1]
//      $fence                                     // A [a.c: 24/1] [UVU: ]
        nop.m           0                          // M
//file/line/col a.c/24/1/E,26/3
        movl            r9 = 0x4121999a         ;; // I [a.c: 26/3]
        setf.s          f6 = r9                 ;; // M [a.c: 26/3]
        nop.m           0                          // M
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
        fmerge.s        f6 = f6, f6             ;; // F [a.c: 26/3]
        nop.m           0                          // M
        fmerge.s        f8 = f6, f6                // F [a.c: 26/3]
        br.call.dptk.few rp = baz               ;; // B [a.c: 26/3] [UVU]
        add             r32 = 0, r8             ;; // M [a.c: 26/3] [UVU: i]
//file/line/col a.c/27/3
        add             r32 = 10, r0               // M [a.c: 27/3] [UVU: i]
//file/line/col a.c/28/8
        add             r10 = 100, r0           ;; // I [a.c: 28/8]
        nop.m           0                          // M
        sxt4            r9 = r32                   // I [a.c: 28/8] [UVuse]
        nop.i           0                       ;; // I
        st4             [r9] = r10                 // M [a.c: 28/8] [UVU]
//file/line/col a.c/29/1
        mov             rp = r34                ;; // I [a.c: 29/1]
        mov             ar.pfs = r33               // I [a.c: 29/1]
        nop.m           0                          // M
        nop.m           0                          // M
        br.ret.dptk.few rp                      ;; // B [a.c: 29/1]

//      $end                                    ;; // A

	.endp	my_init

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.text)
	data8.ua @segrel(.text+144)
	data8.ua @segrel(.tcg$unwind_info_block0002)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 8
	data8.ua @segrel(.llo$annot_info_block0002)
.tcg$unwind_info_block0002:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x02
	data1	0x03, 0xe4, 0x02, 0xb0, 0xa2, 0xe6, 0x00, 0xb1
	data1	0x21, 0x38, 0x81, 0xc0, 0x1a, 0x00, 0x00, 0x00

// ===


// Routine 150 ("foo"::101)

	.section .text = "ax", "progbits"
	.proc	foo
	.global	foo
//      $start          CMid904 =               ;; // A

foo:
//file/line/col a.c/32/1
//      $entry          CMid906 =                  // A [a.c: 32/1]
        alloc           r31 = ar.pfs, 0, 1, 0, 0   // M [a.c: 32/1] [UVU: ]
//      $fence                                     // A [a.c: 32/1] [UVU: ]
        add             r8 = -48, sp               // I [a.c: 32/1]
//file/line/col a.c/32/1/E,33/4
        add             r32 = 10, r0            ;; // I [a.c: 33/4] [UVU: i]
//file/line/col a.c/34/1
        add             r32 = 1, r32               // M [a.c: 34/1] [UVU: i] [UVuse]
//file/line/col a.c/35/8
        add             r10 = 100, r0           ;; // I [a.c: 35/8]
        sxt4            r9 = r32                ;; // I [a.c: 35/8] [UVuse]
        st4             [r9] = r10                 // M [a.c: 35/8] [UVU]
//file/line/col a.c/36/1
        add             r8 = 0, r32                // M [a.c: 36/1] [UVuse]
        br.ret.dptk.few rp                      ;; // B [a.c: 36/1]

//      $end                                    ;; // A

	.endp	foo

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.text)
	data8.ua @segrel(.text+48)
	data8.ua @segrel(.tcg$unwind_info_block0003)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 8
	data8.ua @segrel(.llo$annot_info_block0003)
.tcg$unwind_info_block0003:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x01
	data1	0x01, 0x28, 0x81, 0xc0, 0x08, 0x00, 0x00, 0x00

// ===

	.type	main,@function
	.global malloc
	.type	malloc,@function
	.global free
	.type	free,@function
	.global abort
	.type	abort,@function

// Routine 201 ("main"::101)

	.section .text = "ax", "progbits"
	.proc	main
	.global	main
//      $start          CMid904 =               ;; // A

main:
//file/line/col a.c/40/1
//      $entry          CMid920 =                  // A [a.c: 40/1]
        alloc           r33 = ar.pfs, 0, 6, 1, 0   // M [a.c: 40/1] [UVU: ]
        add             r10 = 0, sp                // M [a.c: 40/1] [UVU: ]
        mov             r34 = rp                   // I [a.c: 40/1]
        add             sp = -16, sp               // M [a.c: 40/1] [UVU: ]
        add             r37 = 0, gp             ;; // I
        add             r11 = 0, r10               // I [a.c: 40/1]
        add             r36 = -48, sp           ;; // M [a.c: 40/1]
        stf.spill       [r11] = f16, 16            // M [a.c: 40/1] [UVuse]
//      $fence                                     // A [a.c: 40/1] [UVU: ]
        nop.i           0                          // I
        nop.m           0                          // M
//file/line/col a.c/40/1/E,42/4
        movl            r9 = 0x4121999a         ;; // I [a.c: 42/4]
        setf.s          f16 = r9                ;; // M [a.c: 42/4] [UVU: i]
        nop.m           0                          // M
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
//file/line/col a.c/43/3
        fmerge.s        f6 = f16, f16           ;; // F [a.c: 43/3] [UVuse]
        nop.m           0                          // M
        fmerge.s        f8 = f6, f6                // F [a.c: 43/3]
        br.call.dptk.few rp = baz               ;; // B [a.c: 43/3] [UVU]
        nop.m           0                          // M
        sxt4            r9 = r8                 ;; // I [a.c: 43/3]
        cmp.lt          p6, p0 = r9, r0            // I [a.c: 43/3]
        setf.sig        f6 = r9                 ;; // M [a.c: 43/3]
        nop.m           0                          // M
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
(p6)    fcvt.xf         f6 = f6                 ;; // F [a.c: 43/3]
        nop.m           0                          // M
        fnorm.s.s0      f16 = f6                   // F [a.c: 43/3] [UVU: i]
//file/line/col a.c/44/7
        add             r9 = 1, r0              ;; // I [a.c: 44/7]
        add             out0 = 0, r9               // M [a.c: 44/7]
        nop.m           0                          // M
        br.call.dptk.few rp = malloc            ;; // B [a.c: 44/7] [UVU]
        add             gp = 0, r37                // M [a.c: 44/7]
        sxt4            r32 = r8                ;; // I [a.c: 44/7] [UVU: cc]
//file/line/col a.c/45/1
        add             r9 = 0, r32             ;; // I [a.c: 45/1] [UVuse]
        add             out0 = 0, r9               // M [a.c: 45/1]
        nop.m           0                          // M
        br.call.dptk.few rp = free              ;; // B [a.c: 45/1] [UVU]
        add             gp = 0, r37                // M [a.c: 45/1]
//file/line/col a.c/46/7
        add             r9 = 0x186a0, r0        ;; // I [a.c: 46/7]
        add             out0 = 0, r9               // I [a.c: 46/7]
        nop.m           0                          // M
        nop.m           0                          // M
        br.call.dptk.few rp = malloc            ;; // B [a.c: 46/7] [UVU]
        add             gp = 0, r37                // M [a.c: 46/7]
        sxt4            r32 = r8                   // I [a.c: 46/7] [UVU: cc]
//file/line/col a.c/47/7
        add             r9 = 0x186a0, r0        ;; // I [a.c: 47/7]
        add             out0 = 0, r9               // M [a.c: 47/7]
        nop.m           0                          // M
        br.call.dptk.few rp = malloc            ;; // B [a.c: 47/7] [UVU]
        add             gp = 0, r37                // M [a.c: 47/7]
        sxt4            r32 = r8                   // I [a.c: 47/7] [UVU: cc]
//file/line/col a.c/48/6
        add             r9 = 1, r0              ;; // I [a.c: 48/6]
        st4             [r32] = r9                 // M [a.c: 48/6] [UVU] [UVuse]
//file/line/col a.c/49/6
        add             r9 = 2, r0              ;; // I [a.c: 49/6]
        nop.i           0                          // I
        st4             [r32] = r9                 // M [a.c: 49/6] [UVU] [UVuse]
//file/line/col a.c/50/1
        add             r9 = 0, r32             ;; // I [a.c: 50/1] [UVuse]
        add             out0 = 0, r9               // I [a.c: 50/1]
        nop.m           0                          // M
        nop.m           0                          // M
        br.call.dptk.few rp = free              ;; // B [a.c: 50/1] [UVU]
        add             gp = 0, r37                // M [a.c: 50/1]
        nop.m           0                          // M
//file/line/col a.c/51/1
        br.call.dptk.few rp = abort             ;; // B [a.c: 51/1] [UVU]
        add             gp = 0, r37                // M [a.c: 51/1]
//file/line/col a.c/52/1
        add             r8 = 0, r0                 // M [a.c: 52/1]
        mov             rp = r34                   // I [a.c: 52/1]
        add             r14 = 16, sp            ;; // M [a.c: 52/1]
        add             r15 = 0, r14               // M [a.c: 52/1]
        mov             ar.pfs = r33            ;; // I [a.c: 52/1]
        ldf.fill        f16 = [r15], 16            // M [a.c: 52/1]
        add             sp = 0, r14                // M [a.c: 52/1] [UVU: ]
        br.ret.dptk.few rp                      ;; // B [a.c: 52/1]

//      $end                                    ;; // A

	.endp	main

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.text)
	data8.ua @segrel(.text+416)
	data8.ua @segrel(.tcg$unwind_info_block0004)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 8
	data8.ua @segrel(.llo$annot_info_block0004)
.tcg$unwind_info_block0004:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x04
	data1	0x08, 0xe4, 0x02, 0xb0, 0xa2, 0xe6, 0x00, 0xb1
	data1	0x21, 0xb9, 0x00, 0x00, 0x10, 0xb8, 0x00, 0x01
	data1	0xe0, 0x03, 0x01, 0xe2, 0x00, 0x61, 0x46, 0x81
	data1	0xc0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

// ===


// ===
	.section .sdata = "asw", "progbits"
	.align 8
	.global j
.tcg0$sdata:	data1	0x00, 0x00, 0x00, 0x64
j:	data1	0x00, 0x00, 0x00, 0x64

// ===
	.section .rodata = "a", "progbits"
	.align 16
.tcg1$rodata:	data1	0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x07
	data1	0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x07
	data1	0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x02
	data1	0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x04
	data1	0x00, 0x00, 0x00, 0x05

// ===
	.section .HP.opt_annot = "a", "annot"
	.align 8
.llo$annot_info_block0000:	data1	0x00, 0xac, 0x01, 0xfe, 0xff, 0xfe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xfe, 0x20, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0001:	data1	0x00, 0xac, 0x01, 0xfe, 0xff, 0xfe, 0xff, 0xfe
	data1	0xfe, 0xbc, 0xff, 0xf0, 0x20, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0002:	data1	0x00, 0xac, 0x01, 0xfe, 0xff, 0xfe, 0xff, 0xff
	data1	0xfe, 0xbc, 0xff, 0xff, 0xe8, 0xfe, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0003:	data1	0x00, 0xac, 0x01, 0xfe, 0xff, 0xfe, 0xff, 0xff
	data1	0xff, 0xfc, 0x7f, 0xff, 0xe8, 0xfe, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0004:	data1	0x00, 0xac, 0x01, 0xfe, 0xff, 0xbe, 0xff, 0xfe
	data1	0xfe, 0xbc, 0xff, 0xff, 0x20, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00

// ===
	.section .debug_procs_abbrev = "", "progbits"
	.align 1
$DEBUG_PROCS_ABBREV_CE0101_a_c:	data1	0x03, 0x11, 0x01, 0x10, 0x07, 0x90, 0x40, 0x07
	data1	0x03, 0x08, 0x13, 0x0b, 0x95, 0x40, 0x0b, 0x25
	data1	0x08, 0x1b, 0x08, 0x01, 0x13, 0x00, 0x00, 0x07
	data1	0x2e, 0x01, 0x94, 0x40, 0x0b, 0x96, 0x40, 0x06
	data1	0x11, 0x01, 0x12, 0x01, 0x97, 0x40, 0x01, 0x98
	data1	0x40, 0x01, 0x00, 0x00, 0x00

// ===
	.section .debug_procs_info = "", "progbits"
	.align 1
	data1	0x00, 0x00, 0x00, 0xfa, 0x00, 0x02
	data4.ua @secrel($DEBUG_PROCS_ABBREV_CE0101_a_c)
	data1	0x08, 0x03
	data8.ua @secrel($dbglline_CE0101_a_c)
	data8.ua @secrel($dbgaline_CE0101_a_c)
	stringz	"a.c"
	data1	0x02, 0x01, 0x47, 0x00
	stringz	"/home/bindu/TESTS"
	data1	0x00, 0x00, 0x00, 0x00, 0x07, 0x01, 0x00, 0x00
	data1	0x00, 0x00
	data8.ua .text
	data8.ua .text+240
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data8.ua .text
	data8.ua .text+256
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data8.ua .text
	data8.ua .text+144
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data8.ua .text
	data8.ua .text+48
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data8.ua .text
	data8.ua .text+416
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00

// ===
	.section .debug_line = "", "progbits"
	.align 8
$dbglline_CE0101_a_c:	data1	0x00, 0x00, 0x00, 0xc2, 0x00, 0x02, 0x00, 0x00
	data1	0x00, 0x10, 0x04, 0x01, 0x00, 0x05, 0x0a, 0x01
	data1	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01
	data1	0x00, 0x00, 0x00, 0x09, 0x02
	data8.ua .text
	data1	0x00, 0x08, 0x03
	stringz	"a.c"
	data1	0x00, 0x00, 0x00, 0x00, 0x09, 0x02
	data8.ua .text
	data1	0x04, 0x01, 0x05, 0x01, 0x03, 0x06, 0x01, 0x06
	data1	0x46, 0x06, 0x05, 0x02, 0x0b, 0x05, 0x01
	stringz	"y\\"
	data1	0x09, 0x02
	data8.ua .text
	data1	0x03, 0x05, 0x01, 0x06, 0x64, 0x06, 0x05, 0x02
	data1	0x0b, 0x05, 0x01, 0x7e, 0x10, 0x00, 0x09, 0x02
	data8.ua .text
	data1	0x03, 0x05, 0x01, 0x06, 0x23, 0x06, 0x05, 0x03
	data1	0x0c, 0x5b, 0x05, 0x08, 0x10, 0x05, 0x01, 0x2e
	data1	0x00, 0x09, 0x02
	data8.ua .text
	data1	0x0d, 0x06, 0x14, 0x06, 0x05, 0x04, 0x0b, 0x05
	data1	0x01, 0x15, 0x05, 0x08, 0x10, 0x05, 0x01, 0x1f
	data1	0x00, 0x09, 0x02
	data8.ua .text
	data1	0x0e, 0x06, 0x4b, 0x06, 0x05, 0x04, 0x0c, 0x05
	data1	0x03, 0x38, 0x05, 0x07, 0x6f, 0x05, 0x01, 0x33
	data1	0x05, 0x07, 0x2e, 0x38, 0x05, 0x06, 0x33, 0x1a
	data1	0x05, 0x01, 0x1f, 0x38, 0x1a, 0x02, 0x04, 0x00
	data1	0x01, 0x01

// ===
	.section .debug_actual = "", "progbits"
	.align 8
$dbgaline_CE0101_a_c:	data1	0x00, 0x00, 0x01, 0x3e, 0x00, 0x02, 0x00, 0x00
	data1	0x00, 0x0e, 0x04, 0x01, 0x00, 0x05, 0x0a, 0x01
	data1	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01
	data1	0x00, 0x09, 0x02
	data8.ua .text
	data1	0x00, 0x09, 0x02
	data8.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x0f
	data1	0x00, 0x01, 0x11, 0x19, 0x00, 0x01, 0x11, 0x28
	data1	0x16, 0x00, 0x01, 0x11, 0x4b, 0x00, 0x01, 0x11
	data1	0x1e, 0x00, 0x01, 0x11, 0x1e, 0x10, 0x00, 0x01
	data1	0x11, 0x41, 0x00, 0x01, 0x11, 0x24, 0x00, 0x01
	data1	0x11, 0x1e, 0x00, 0x01, 0x18, 0x1e, 0x00, 0x09
	data1	0x02
	data8.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x0f
	data1	0x00, 0x01, 0x11, 0x19, 0x00, 0x01, 0x11, 0x4b
	data1	0x11, 0x00, 0x01, 0x11, 0x50, 0x00, 0x01, 0x11
	data1	0x1e, 0x00, 0x01, 0x11, 0x1e, 0x00, 0x01, 0x11
	data1	0x10, 0x10, 0x00, 0x01, 0x11, 0x32, 0x00, 0x01
	data1	0x11, 0x32, 0x00, 0x01, 0x18, 0x1e, 0x00, 0x09
	data1	0x02
	data8.ua .text
	data1	0x00, 0x01, 0x11
	stringz	"\X0b%"
	data1	0x01, 0x11, 0x4b, 0x00, 0x01, 0x11, 0x14, 0x00
	data1	0x01, 0x11, 0x10, 0x10, 0x00, 0x01, 0x11, 0x28
	data1	0x10, 0x00, 0x01, 0x18, 0x23, 0x00, 0x09, 0x02
	data8.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x16
	data1	0x00, 0x01, 0x11, 0x15, 0x10, 0x00, 0x01, 0x11
	data1	0x19, 0x10, 0x00, 0x01, 0x18, 0x0f, 0x00, 0x09
	data1	0x02
	data8.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x0f
	data1	0x00, 0x01, 0x11, 0x19, 0x39, 0x00, 0x01, 0x11
	data1	0x19, 0x29, 0x00, 0x01, 0x11, 0x1e, 0x00, 0x01
	data1	0x11, 0x55, 0x10, 0x00, 0x01, 0x11, 0x1e, 0x00
	data1	0x01, 0x11, 0x19, 0x10, 0x00, 0x01, 0x11, 0x1e
	data1	0x1a, 0x00, 0x01, 0x11, 0x23, 0x00, 0x01, 0x11
	data1	0x19, 0x10, 0x00, 0x01, 0x11, 0x1e, 0x00, 0x01
	data1	0x11, 0x19, 0x10, 0x00, 0x01, 0x11, 0x14, 0x10
	data1	0x00, 0x01, 0x11, 0x19, 0x10, 0x00, 0x01, 0x11
	data1	0x23, 0x00, 0x01, 0x11, 0x1f, 0x1a, 0x00, 0x01
	data1	0x11, 0x32, 0x00, 0x01, 0x18, 0x0f, 0x02, 0x04
	data1	0x00, 0x01, 0x01

// ===
	.section .note = "", "note"
	.align 8
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x04
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
	stringz	"HP"
	data1	0x00, 0x00, 0x00, 0x00, 0x00
	stringz	"A.05.55 [Dec 04 2003]\nctcom options = -assembly on -ia64abi all -architecture 64 -ext on -lang c -exception off -inline_power 1 -link_type dynamic -fpeval float -tls_dyn on -target_os 11.23 -ucode hdriver=optlevel%1% -plusolistoption -Ol06const! -plusolistoption -Ol13aggressive! -plusooption -Oq01,al,ag,cn,sz,ic,vo,Mf,Po,es,rs,Rf,Pr,sp,in,cl,om,vc,pi,fa,pe,rr,pa,pv,nf,cp,lx,Pg,ug,lu,lb,uj,dn,sg,pt,kt,em,np,ar,rp,dl,fs,bp,wp,pc,mp,lr,cx,cr,pi,so,Rc,fa,ft,fe,ap,st,lc,Bl,ib,pl,sd,ll,rl,dl,Lt,ol,fl,lm,ts,rd,dp,If!"
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x25, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x04
	stringz	"HP"
	data1	0x00, 0x00, 0x00, 0x00, 0x00
	stringz	"a.c\n/home/bindu/TESTS\nANSI C 64 bits"
	data1	0x00, 0x00, 0x00

 .section .preinit_array = "aw", "preinit_array"
  .align 16
  data8.ua @fptr(my_init)
