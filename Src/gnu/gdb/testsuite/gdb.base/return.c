#include <stdio.h>
/*  Test "return" command.  */

void func1 ()
{ int use_stack_space[10000];
  printf("in func1\n", use_stack_space);
}

int
func2 ()
{
  return -5;
}

double
func3 ()
{
  return -5.0;
}

int tmp2;
double tmp3;
int func4();  /* Forward declaration to not move lines */
int main ()
{
#ifdef usestubs
  set_debug_traps();
  breakpoint();
#endif
  func1 ();
  printf("in main after func1\n");
  tmp2 = func2 ();
  tmp3 = func3 ();
  printf("exiting\n");
  tmp2 = func4 ();
  return 0;
}

int func4 ()
{
  printf ("in func4\n");
  return 9;
}
