#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

extern void baz ();

void
bar () 
{ 
  printf ("In main bar\n"); 
}

#ifdef PROTOTYPES
int main (void)
#else
main ()
#endif
{
  int  pid;
  int stat_loc;

  pid = vfork ();
  if (pid == 0)
    {
      execl ("gdb.base/serial_dbg_c1", (char *) 0);
      printf ("In child one after execl\n");
    }
  else
    {
      wait (&stat_loc);
      printf ("Parent of child one %d.\n", pid);
    }
  printf ("Back to parent after vfork() \n");

  pid = fork ();
  if (pid == 0)
    {
      printf ("Child two from fork\n");
      exit (0);
    }
  else
    {
      wait (&stat_loc);
      printf ("Parent of child two %d.\n", pid);
    }
  printf ("Finished vfork() and fork().\n");
  bar ();
  baz ();
  printf ("Exiting...\n");
  exit (0);
}

