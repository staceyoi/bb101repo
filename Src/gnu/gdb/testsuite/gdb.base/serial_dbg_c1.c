#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

extern void boo ();

void
foo ()
{
	printf ("In foo()\n");
}

int
main () 
{
	int pid;
	int stat_loc;
	printf ("In serial_dbg_c1.\n");
	foo();
	pid = vfork ();
	if (pid == 0)
	  {
	    execl ("gdb.base/serial_dbg_gc", (char *) 0);
	  }
	else
	  {
	    wait (&stat_loc);
	    printf ("Back in child one\n");
	  }	
	boo ();
}
