int import_var1 = 100;
int import_var2 = 200;
int import_var3 = 300;

int local1 = 1;
int local2 = 2;
int local3 = 3;

static int file_static1 = 4;
static int file_static2 = 5;
static int file_static3 = 6;

void foo11();
void
foo1()
{
/* Let us reference all the variables that we have declared till now. */
  foo11();
  import_var1 = 100;
  import_var2 = 200;
  import_var3 = 300;
  
  local1 = 1;
  local2 = 2;
  local3 = 3;
  
  file_static1 = 4;
  file_static2 = 5;
  file_static3 = 6;
  return;
}
