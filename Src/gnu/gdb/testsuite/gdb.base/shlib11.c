int import_var11 = 1001;
int import_var21 = 2001;
int import_var31 = 3001;

int local11 = 111;
int local21 = 211;
int local31 = 311;

static int file_static1 = 411;
static int file_static2 = 511;
static int file_static3 = 611;

void
foo11()
{
  import_var11 = 1001;
  import_var21 = 2001;
  import_var31 = 3001;
  
  local11 = 111;
  local21 = 211;
  local31 = 311;
  
  file_static1 = 411;
  file_static2 = 511;
  file_static3 = 611;
  return;
}
