int import_var4 = 101;
int import_var5 = 201;
int import_var6 = 301;

int local4 = 11;
int local5 = 12;
int local6 = 13;

static int file_static1 = 14;
static int file_static2 = 15;
static int file_static3 = 16;

void foo21();
void
foo2()
{
  foo21();
  import_var4 = 101;
  import_var5 = 201;
  import_var6 = 301;
  local4 = 11;
  local5 = 12;
  local6 = 13;
  file_static1 = 14;
  file_static2 = 15;
  file_static3 = 16;
  
  return;
}
