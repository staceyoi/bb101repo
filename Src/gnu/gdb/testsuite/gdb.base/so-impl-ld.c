/* This program is linked against SOM shared libraries, which the loader
   automatically loads along with the program itself).
   */

#include <stdio.h>

#if defined(__cplusplus)
extern "C" int  solib_main (int  arg); extern "C" int main_fun();
#else
int  solib_main (int  arg);  int main_fun();
#endif

int main ()
{
  int  result;

  /* Call a shlib function. */
  result = solib_main (100);

  /* Call it again. */
  result = solib_main (result);
  return main_fun ();
}

#if defined(__cplusplus)
extern "C" 
#endif
int main_fun ()
{
  return 9;
}
