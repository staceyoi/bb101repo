int main_loc1 = 1000;
int main_loc2 = 2000;
int main_loc3 = 3000;

static int main_file_static1 = 4000;
static int main_file_static2 = 5000;
static int main_file_static3 = 6000;

extern int import_var1;
extern int import_var2;
extern int import_var3;
extern int import_var11;
extern int import_var21;
extern int import_var31;

extern int import_var4;
extern int import_var5;
extern int import_var6;
extern int import_var41;
extern int import_var51;
extern int import_var61;

extern int import_var7;
extern int import_var8;
extern int import_var9;
extern int import_var71;
extern int import_var81;
extern int import_var91;

extern void foo1();
extern void foo2();
extern void foo3();
int main()
{
  foo1();
  foo2();
  foo3();
  main_loc1 = 1000;
  main_loc2 = 2000;
  main_loc3 = 3000;
  main_file_static1 = 4000;
  main_file_static2 = 5000;
  main_file_static3 = 6000;
  import_var1 = 100;
  import_var2 = 200;
  import_var3 = 300;
  import_var4 = 101;
  import_var5 = 201;
  import_var6 = 301;
  import_var7 = 102;
  import_var8 = 202;
  import_var9 = 302;
  return 0;
}
