/*
 * Author 		: P.E.Saravanan
 * Testcase 		: To test steplast feature for C and C++ applications.
 */

#include <stdio.h>

#if defined(__cplusplus) || defined(__STDCPP__)
#include <string>
void steplast_func ( string s1, string s2 )
  {
      int len = s1.length()+s2.length();
  }
#endif
  
int foo (int x) {
  return x*x;
}

int bar (int x) {
  return x*x;
}

void goo (int x) {
}

int main() {
  int val = 0;

/* Test steplast functionality when the func (goo) doesn't return any val*/
   goo ( bar(10) ); 

/* Test steplast functionality when the func (foo) returns a val 
   There was a C++ compiler bug for this case*/
  val = foo ( bar(10) );

#if defined(__cplusplus) || defined(__STDCPP__)
/* Test steplast/steptop functionality */
  val = foo ( bar (10) ) +  bar ( foo (10) ) ;

/* Test steplast/steptop functionality when func spans more than one line
   There was a C++ compiler bug for this case*/
  val = foo ( bar (10) ) 
        +  bar ( foo (10) ) ;

/* Test steplast functionality for STL case */
  string s = "Wildeebeast";
  steplast_func (s, "debugger");
#endif
}
