#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

static void sig_usr(int);  /* signal handler */

   int signo;
int main(void) {

struct sigaction act,oact;
act.sa_handler = sig_usr;
sigemptyset(&act.sa_mask);
sigaddset(&act.sa_mask,SIGINT);
act.sa_flags = 0;

/*
if (sigaction(SIGINT, &act, &oact) < 0)
	printf("Can't catch SIGINT\n");
*/

  printf ("In sigwait\n");
  for ( ; ; )
  {
   int sigerr = sigwait((sigset_t *)&act.sa_mask, &signo); 
   if (signo == SIGINT) {
        printf("received SIGINT\n");
        exit(1);
   }else {
        printf("received signal %d\n", signo);   
   }
  }
}

static void sig_usr(int signo)	
{
if (signo == SIGINT) {
        printf("received SIGINT\n");
        exit(1);
}
else
	printf("error - received signal %d\n",signo);
return;
}

