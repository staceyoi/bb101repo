#include <sys/stat.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<dl.h>

int main() {
   int vfork_me;
   vfork_me = vfork();
   if ( vfork_me == 0 ) {
      printf ("I am child" );
      exit(0);
   } else {
      printf ("I am parent \n" );
      shl_load ("vfork_share.sl",BIND_DEFERRED,0);
   }
}
