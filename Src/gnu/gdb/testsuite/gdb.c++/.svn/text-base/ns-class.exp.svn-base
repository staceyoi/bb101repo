# Copyright (C) 1998 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# tests for misc. C++ namespace stuff
# Written by Poorva Gupta <poorva@cup.hp.com> 2002-02-28

#  This file is part of the gdb testsuite

if $tracelevel then {
        strace $tracelevel
        }

# aCC3 does not seem to support namespaces well
if {![istarget "ia64*-hp-*"]} {
  return;
}


#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "ns-class"
set srcfile ${testfile}.cc
set full_srcfile ${srcdir}/${subdir}/${srcfile}
set binfile ${objdir}/${subdir}/${testfile}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

if [get_compiler_info $binfile "c++"] {
  return -1
}
source ${binfile}.ci

# namespace support is not in g++
if { $gcc_compiled || $gxx_compiled } {
   return 0
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break main" \
    "Breakpoint.*at.* file .*${srcfile}, line 55.*" \
    "breakpoint main"

gdb_test "run" \
    "Starting program:.*${srcfile}:55.*int l = foo1.bar().*;.*" \
    "run program, hit main breakpoint"

gdb_test "break Foo::bar" \
    "Breakpoint.*at.* file .*${srcfile}, line 37.*" \
    "breakpoint Foo::bar"

send_gdb "break Foo::baz\n"
gdb_expect {
 -re ".*\\\[0\\\] cancel.*\\\[1\\\] all.*\\\[2\\\] B::Foo::baz\\\(int\\\).*${srcfile}:42.*\\\[3\\\] B::Foo::baz\\\(double\\\).*${srcfile}:47.*> $" {
        send_gdb "0\n"
        gdb_expect {
               -re ".*cancelled.*$gdb_prompt $" {
                        pass "break Foo::baz" }
                -re ".*$gdb_prompt $" {
                       fail "break Foo::baz" }
                timeout {
                        fail "(timeout)break Foo::baz" }
                }
          }
 -re ".*\\\[0\\\] cancel.*\\\[1\\\] all.*\\\[2\\\] B::Foo::baz\\\(double\\\).*${srcfile}:47.*\\\[3\\\] B::Foo::baz\\\(int\\\).*${srcfile}:42.*> $" {
        send_gdb "0\n"
        gdb_expect {
               -re ".*cancelled.*$gdb_prompt $" {
                        pass "break Foo::baz" }
                -re ".*$gdb_prompt $" {
                       fail "break Foo::baz" }
                timeout {
                        fail "(timeout)break Foo::baz" }
                }
           }
-re ".*$gdb_prompt $" { fail "break Foo::baz(wrong menu) " }
    timeout           { fail "(timeout)break Foo::baz" }
}


gdb_test "continue" \
    ".*B::Foo::bar.*${srcfile}:37.*return 2;.*" \
    "continue program, hit B::Foo::bar breakpoint"

gdb_test "print k" \
    ".* = 4.*" \
    "print k"

    #".*43.*printf \\\(\"Foo::k %d j = %d\", Foo::k, ::j\\\);.*" \

gdb_test "next" \
    ".*56.*foo1.baz\\\(\\\(int\\\) 2\\\);.*" \
    "next"

gdb_test "print Foo::k" \
    ".* = 4.*" \
    "print Foo::k"

gdb_test "print Balias::a" \
    "variable is: B::a.*= 3.1459.*" \
    "print Balias::a"

gdb_test "print Balias2::a" \
    "variable is: B::a.* = 3.1459.*" \
    "print Balias2::a"

gdb_test "print Balias::i" \
    "variable is: A::i.* = 2.*" \
    "print Balias::i"

gdb_test "print Balias2::i" \
    "variable is: A::i.* = 2.*" \
    "print Balias2::i"

gdb_test "print B::j" \
    "variable is: A::j.* = 4.*" \
    "print B::j"

gdb_test "ptype A" \
    ".*namespace  A.*" \
    "ptype A"

gdb_test "ptype B" \
    ".*namespace  B.*" \
    "ptype B"

gdb_test "ptype Foo" \
    ".*type = class B::Foo {.*public:.*static int k;.*int bar\\\(void\\\);.*int baz\\\(int\\\);.*int baz\\\(double\\\);.*}.*" \
    "ptype Foo"

send_gdb "set namespaces-enabled off\n" 
gdb_expect {
	 -re ".*$gdb_prompt $" {
		pass "set namespaces-enabled off" }
	timeout {
                fail "set namespaces-enabled off" }
   }

gdb_test "print j" \
    ".* = 0.*" \
    "print j after namespaces disabled"

gdb_test "break Foo::bar" \
    "Breakpoint.*deferred.*Foo::bar.*" \
    "breakpoint Foo::bar"

send_gdb "set namespaces-enabled on\n"
gdb_expect {
	 -re ".*$gdb_prompt $" {
		pass "set namespaces-enabled on" }
	timeout {
                fail "set namespaces-enabled on" }
   }
   

send_gdb "print j\n"
gdb_expect {
 -re ".*\\\[1\\\] j.*\\\[2\\\] A::j.*> $" {
        send_gdb "2\n"
        gdb_expect {
               -re ".*variable is: A::j.* = 4.*$gdb_prompt $" {
                        pass "print j after namespaces reenabled" }
                -re ".*$gdb_prompt $" {
                       fail "print j after namespaces reenabled" }
                timeout {
                        fail "(timeout)print j after namespaces reenabled" }
                }
        }
-re ".*$gdb_prompt $" { fail "print j(wrong menu) after namespaces reenabled" }
    timeout           { fail "(timeout)print j after namespaces reenabled" }
}



gdb_exit

