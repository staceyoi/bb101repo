/******************************************************************************
*
* (c) Copyright 1995
* (p) Protected 1995
*
* The Hewlett-Packard Company
* California Language Laboratory
* Cupertino, California
*
* HP Confidential
*
*******************************************************************************
* BitUtils.h:
*     Miscellenous bit twiddling utilities
******************************************************************************/

/***************************************************************
*
*  Header File Dependencies
*
***************************************************************/

#ifndef BITUTILS_H
#define BITUTILS_H
#include "langtypes.h"

UInt countLeadingZeros(UInt mask);
UInt countLeadingZeros(Int64 num);

UInt countTrailingZeros(UInt mask);
UInt countTrailingZeros(Int64 num);

Boolean isContiguous(UInt64 mask, UInt *nbits);
Boolean isContiguous(UInt64 mask, UInt *nbits, UInt *trailingZeros);

UInt powerOf2(Int64 num);

/***********************************************************************
* 
*   PROCEDURE: bitcount              (inline static)
* 
*   DESCRIPTION:
*      Fast straight-line code for counting the number of one bits in a
*      32 bit word.  Provided in a header file to allow for inlining.
* 
*   INPUTS: The word to bit count
* 
*   OUTPUTS: The number of one bits in the input word.
* 
*   SIDEEFFECTS: none.
*
*   NOTES: Inline for speed.  Static so that it may be removed once
*          all calls to it are expanded inline.
* 
***********************************************************************/

static inline UInt
bitcount(UInt x)
{
UInt n;

   n = (x>>1) & 0x77777777;
   x = x - n;
   n = (n>>1) & 0x77777777;
   x = x - n;
   x = x - ((n>>1) & 0x77777777);

// Sum adjacent 4-bit fields into count of 8-bit fields

   x = (x + (x>>4)) & 0x0f0f0f0f;

// Mask x is now a 4-digit number radix 256 where each digit
// is the number of one bits that were in the 8-bit field
// originally.  The value is the sum of these digits.
// The next two instructions do this.

   x = x + (x>>8);
   x = (x + (x>>16)) & 0xff;
   return x;
}

// 64 bit version of the same routine
static inline UInt
bitcount(UInt64 x)
{
   UInt firstWord = (UInt) (0xffffffff & x);
   UInt secondWord = (UInt) (x >> 32);
   UInt nbits = bitcount(firstWord);
   nbits += bitcount(secondWord);
   return nbits;
}


#endif // BITUTILS_H
