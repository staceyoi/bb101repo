/******************************************************************************
*
* (c) Copyright 1994
* (p) Protected 1994
*
* The Hewlett-Packard Company
* California Language Laboratory
* Cupertino, California
*
* HP Confidential
*
*******************************************************************************
* Btree.h:
*     Balanced binary tree utility
*     Implemented using red-black trees. All algorithms
*     are from 'Introduction to Algorithms' by Cormen Et Al.
*
*     Modifications made to the algorithm are:
*     - Maintain a leftMost and rightMost node, and update them
*     when additions and deletions are made.
*     - Maintain a 'finger' which points to the last used node.
*     - Instead of swapping node values when deleting, swap the
*     nodes themselves.
*     - No nil sentinel is used. A NULL pointer specifies that
*     its at the end.
*     
******************************************************************************/

#ifndef BTREE_H
#define BTREE_H

enum NodeColor {
   BLACK,
   RED
};

// enum used to specify which subtree. Used by VCG graph print
// method.
enum SubTree {
  RIGHT_SUBTREE,
  LEFT_SUBTREE
};

class LinearPool;

// used to debug tree.
class VCGGraph;
class VCGNode;
class VCGEdge;

template <class NodeInfo> class BtreeNode;

template <class NodeInfo> class BtreeIterator;

/************************************************************************
* CLASS:        Btree
* BASE CLASSES: None
* SUBCLASSES:   None
* FRIENDS:      None.
*
* PURPOSE:
*		Represents the root of a balanced search tree.
*               To use this class, you have to call the methods
*		initialize() after construction and clear()
*               before destruction. This is done so that it
*               can have trivial constructor and desctructor,
*               so that a Btree can be unioned with other
*               members (as in Set class).
*
*************************************************************************/

template <class NodeInfo>
class Btree {

public:

   // copy constructor with storage pool. Cannot have
   // a direct copy constructor since Btree is part of 
   // a union in Set.h.
   void copyBtree(const Btree &tree, LinearPool *storagePool)
   {
      DBG_ASSERT_MSG(theRootNode == NULL, ("Cannot copy into a non-null"
	       " btree. Call initialize first, or delete tree first\n"));
      initialize(tree, storagePool);
   }

   // Useful Methods

   // Initialize an empty tree.
   void initialize()
   {
      theLeftMost = NULL;
      theRightMost = NULL;
      theRootNode = NULL;
      theFinger = NULL;
   }

   // Initialize with an identical copy of tree.
   void initialize(const Btree &tree, LinearPool *storagePool);

   // Search for node with value 'value' in tree. If found,
   // return it. If not found, insert it and return the info.
   NodeInfo&  findOrInsert(Int32 value, Boolean *found, LinearPool *pool);

   // find node with value 'value'. Return NULL if not found.
   BtreeNode<NodeInfo>* find(Int32 value) const
   {
      if (finger() && finger()->info().value() == value) {
         return finger();
      }
      return findNode(value);
   }

   // find the closest value == or > value. Returns a boolean 
   // ref parameter which is TRUE if found.
   BtreeNode<NodeInfo>* findClosest(Int32 value, Boolean *found) const
   {
      if (finger() && finger()->info().value() == value) {
	 *found = TRUE;
         return finger();
      }
      return findClosestNode(value, found);
   }


   // insert a node with the given value. If value already exits,
   // assert.
   NodeInfo&  insert(Int32 value, LinearPool *pool);

   // insert a node at the given node. Used when its known where
   // to insert. It returns the inserted node.
   BtreeNode<NodeInfo>* insert(BtreeNode<NodeInfo> *root, 
				Int32 value,
				LinearPool *pool);

   // Delete the given node from the tree
   void remove(BtreeNode<NodeInfo>* node, LinearPool *pool);

   // delete the node with the given value from the tree.
   void remove(Int32 value, LinearPool *pool);

   void clear(LinearPool *storagePool);


   BtreeNode<NodeInfo> *leftMost() const { return theLeftMost; }
   BtreeNode<NodeInfo> *rightMost() const { return theRightMost; }

   // to change leftMost and right Most. Public because
   // users may decide to only use a subset of the tree.
   void leftMost(BtreeNode<NodeInfo> *n) { theLeftMost = n; }
   void rightMost(BtreeNode<NodeInfo> *n) { theRightMost = n; }

   BtreeNode<NodeInfo> *rootNode() const { return theRootNode; }

   BtreeNode<NodeInfo> *finger() const { return theFinger; }

   void dbgPrint();

   // draw a vcg graph of the tree
   void dbgDraw(char *fileName);

   void vcgAddSubtree(VCGGraph &, 
		      BtreeNode<NodeInfo> *,
		      VCGNode &,
		      SubTree );

   void preOrderPrint(BtreeNode<NodeInfo> *root);
   void postOrderPrint(BtreeNode<NodeInfo> *root);
   void inOrderPrint(BtreeNode<NodeInfo> *root);

private:
   
   void rotateLeft(BtreeNode<NodeInfo> *node);
   void rotateRight(BtreeNode<NodeInfo> *node);

   void insertFixup(BtreeNode<NodeInfo> *node);
   void deleteFixup(BtreeNode<NodeInfo> *node,
		    BtreeNode<NodeInfo> *nodeParent);

   // find node with value 'value'. Return NULL if not found.
   // This is used internally. Call find for more 
   // optimal code.
   BtreeNode<NodeInfo>* findNode(Int32 value) const;

   BtreeNode<NodeInfo>* findClosestNode(Int32 value, Boolean *found) const;

   void rootNode(BtreeNode<NodeInfo> *n) { theRootNode = n; }
   void finger(BtreeNode<NodeInfo> *n) { theFinger = n; }
   
   BtreeNode<NodeInfo> *theLeftMost;
   BtreeNode<NodeInfo> *theRightMost;
   BtreeNode<NodeInfo> *theRootNode;
   mutable BtreeNode<NodeInfo> *theFinger;
};


/************************************************************************
* CLASS:        BtreeNode
* BASE CLASSES: None
* SUBCLASSES:   None
* FRIENDS:      Btree (root of binary tree), BtreeIterator
*
* PURPOSE:
*		Represents the node of a balanced search tree.
*
*************************************************************************/
template <class NodeInfo>
class BtreeNode {

friend class Btree<NodeInfo>;

public:
   BtreeNode()
   {
      theRight = NULL;
      theLeft = NULL;
      theNext = NULL;
   }

   // copy constructor will mainly only copy
   // the data from the input node.
   BtreeNode(const BtreeNode &node)
   {
      theRight = NULL;
      theLeft = NULL;
      theNext = NULL;
      theInfo = node.theInfo;
   }

   BtreeNode<NodeInfo> *successor()
   {
      if (right() != NULL) {
	 return right()->minimum();
      }
      BtreeNode<NodeInfo> *tmp = parent();
      BtreeNode<NodeInfo> *node = this;
      while (tmp != NULL && node == tmp->right()) {
	 node = tmp;
	 tmp = tmp->parent();
      }
      return tmp;
   }

   BtreeNode<NodeInfo> *predecessor()
   {
      if (left() != NULL) {
	 return left()->maximum();
      }
      BtreeNode<NodeInfo> *tmp = parent();
      BtreeNode<NodeInfo> *node = this;
      while (tmp != NULL && node == tmp->left()) {
	 node = tmp;
	 tmp = tmp->parent();
      }
      return tmp;
   }

   BtreeNode<NodeInfo>* left() const { return theLeft; }
   BtreeNode<NodeInfo>* right() const { return theRight; }
   BtreeNode<NodeInfo>* parent() const { return theParent; }
   BtreeNode<NodeInfo>* next() const { return theNext; }

   void left(BtreeNode<NodeInfo> *n) { theLeft = n; }
   void right(BtreeNode<NodeInfo> *n) { theRight = n; }
   void parent(BtreeNode<NodeInfo> *n) { theParent = n; }
   void next(BtreeNode<NodeInfo> *n) { theNext = n; }

   NodeInfo& info() {return theInfo; }

private:

   // minimum from input node
   BtreeNode<NodeInfo> *minimum()
   {
      BtreeNode<NodeInfo> *node = this;
      while (node->left() != NULL) {
	 node = node->left();
      }
      return node;
   }

   // maximum from input node
   BtreeNode<NodeInfo> *maximum()
   {
      BtreeNode<NodeInfo> *node = this;
      while (node->right() != NULL) {
	 node = node->right();
      }
      return node;
   }

   BtreeNode *theParent;
   BtreeNode *theLeft;
   BtreeNode *theRight;
   BtreeNode *theNext;
   NodeInfo theInfo;

};


/************************************************************************
* CLASS:        BtreeIterator
* BASE CLASSES: None
* SUBCLASSES:   None
* FRIENDS:      None
*
* PURPOSE:
*		In-order iteration over the tree.
*
*************************************************************************/
template <class NodeInfo>
class BtreeIterator {

public:

   BtreeIterator(const Btree<NodeInfo>* tree)
   {
      DBG_ASSERT_MSG(tree != NULL, ("NULL tree in iterator!\n"));
      theTree = tree;
      theCurrent = tree->leftMost();
   }

   ~BtreeIterator() {}

   void operator = (const UInt i)
   {
      DBG_ASSERT_MSG(i == 0, ("Invalid assignment value\n"));
      theCurrent = theTree->leftMost();
   }

   NodeInfo& operator *()
   {
      theNext = theCurrent->next();
      return theCurrent->info();
   }

   // To return the current node.
   BtreeNode<NodeInfo>* node() const
   {
      return theCurrent;
   }

   void operator ++()
   {
      theCurrent = theNext;
   }

   Boolean operator !=(const UInt i) const
   {
      DBG_ASSERT_MSG(i == 0, ("Invalid comparison value\n"));
      return( (theCurrent != NULL) ? TRUE : FALSE );
   }

   Boolean operator ==(const UInt i) const
   {
      DBG_ASSERT_MSG(i == 0, ("Invalid comparison value\n"));
      return( (theCurrent == NULL) ? TRUE : FALSE );
   }

private:

   const Btree<NodeInfo>* theTree;
   BtreeNode<NodeInfo>* theCurrent;
   BtreeNode<NodeInfo>* theNext;
   

};


/*******************************************************************************
*   Binary tree implementation routines.
*   The following part used to be in TAIL/Src/utils/Btree.C
********************************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::initialize(const Btree &, LinearPool)
*
*   INPUTS:      none
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None    
*
*   DESCRIPTION: Constructs a copy of the input binary tree.
*                Do a non-recursive pre-order walk of the
*                input tree to construct a new tree.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::initialize(const Btree &tree, LinearPool *pool)
{
   // do a non-recursive pre-order traversal on the input tree, creating 
   // nodes in the corresponding order.

   BtreeNode<NodeInfo> *newNode;
   BtreeNode<NodeInfo> *parentNode = NULL;
   BtreeNode<NodeInfo> *tmp; // used for next pointer update
   BtreeNode<NodeInfo>* root = tree.rootNode();

   Boolean walkDown = TRUE;

   // if incoming tree is null, make this tree NULL
   if (root == NULL) {
      initialize();
      return;
   }

   while (root != NULL) {
      if (walkDown) {
	 newNode = new BtreeNode<NodeInfo>(*root);
	 newNode->info().color(root->info().color());
	 newNode->info().value(root->info().value());
	 newNode->parent(parentNode);
	 if (parentNode) {
	    if (root == root->parent()->left()) {
	       parentNode->left(newNode);
	       // when adding a left child, the successor
	       // is the parent
	       newNode->next(parentNode);
	       tmp = newNode->predecessor();
	       if (tmp) {
		  tmp->next(newNode);
	       }
	    } else {
	       parentNode->right(newNode);
	       // when adding right node, newNode's
	       // successor is parentNode's next,
	       // and tmp is the new next of parentNode
	       newNode->next(parentNode->next());
	       parentNode->next(newNode);
	    }
         } else {
	    // first time.
	    rootNode(newNode);
	 }
	 parentNode = newNode;
	 if (root->left()) {
	    root = root->left();
	 } else if (root->right()) {
	    root = root->right();
	 } else {
	    walkDown = FALSE;
	 }
      } else {
	 if (root->parent() && root == root->parent()->left()) {
	    root = root->parent();
	    parentNode = parentNode->parent();
	    if (root->right()) {
	       root = root->right();
	       walkDown = TRUE;
	    }
	 } else {
	    root = root->parent();
	    parentNode = parentNode->parent();
	 }
      }
   }
   leftMost(rootNode()->minimum());
   rightMost(rootNode()->maximum());
   finger(NULL);
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::findOrInsert
*
*   INPUTS:      Input value of a node, linearpool
*
*   OUTPUTS:     Returns NodeInfo part of BtreeNode.
*
*   SIDEEFFECTS: May create a new entry in the tree.
*
*   DESCRIPTION: Search for a node with the given value. If found,
*                return it. Else, create one with that value. Call
*                insertFixup to fix up the tree after insertion.
*                Algorithm from 'Introduction to Algorithms' by Cormen
*                Et Al.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
NodeInfo &
Btree<NodeInfo>::findOrInsert(Int32 value, Boolean *found, LinearPool *pool)
{
   // check finger first.
   BtreeNode<NodeInfo> *succ; // successor of node.
   BtreeNode<NodeInfo> *node = finger();
   BtreeNode<NodeInfo> *root = rootNode();
   Int32 nodeVal = 0;

   // Assume not found
   if (found) {
      *found = FALSE;
   }
   if (node) {
      nodeVal = node->info().value();
      if (nodeVal == value) {
	 if (found) {
	    *found = TRUE;
	 }
	 return node->info();
      } else if (nodeVal < value) {
	 // check successor.
	 succ = node->next();
	 if (succ) {
	    if (succ->info().value() == value) {
	       if (found) {
	          *found = TRUE;
	       }
	       theFinger = succ;
	       return succ->info();
	    } else if (succ->info().value() > value) {
	       // node->value < value < succ->value
	       // if node has no right child, then
	       // insert at node. If node has a
	       // right child, insert at succ.
	       if (node->right()) {
		  node = insert(succ, value, pool);
	       } else {
		  node = insert(node, value, pool);
	       }
	       return node->info();
	    } 
	    // if value is greater, fall through to 
	    // full search below.
	 } else {
	    // no succ, use node to insert
            node = insert(node, value, pool);
	    return node->info();
	 }
      }
   }

   node = NULL;
   while (root != NULL) {
      node = root;
      nodeVal = node->info().value();
      if (value < nodeVal) {
	 root = root->left();
      } else if (value > nodeVal) {
	 root = root->right();
      } else if (value == nodeVal) {
	 if (found) {
	    *found = TRUE;
	 }
	 theFinger = node;
	 return node->info();
      }
   }

   node = insert(node, value, pool);
   return node->info();
}


/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::insert
*
*   INPUTS:      Input value of a node and a linearpool
*
*   OUTPUTS:     Returns NodeInfo part of BtreeNode.
*
*   SIDEEFFECTS: Creates a node in the tree.
*
*   DESCRIPTION: Add a new node in the tree.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
NodeInfo &
Btree<NodeInfo>::insert(Int32 value, LinearPool *pool)
{
   BtreeNode<NodeInfo> *node = NULL;
   BtreeNode<NodeInfo> *root = rootNode();
   Int32 nodeVal = 0;
   while (root != NULL) {
      node = root;
      nodeVal = node->info().value();
      if (value < nodeVal) {
	 root = root->left();
      } else if (value > nodeVal) {
	 root = root->right();
      } else if (value == nodeVal) {
	 DBG_ASSERT_MSG(FALSE, 
                        ("Trying to insert a value already present\n"));
      }
   }
   node = insert(node, value, pool);
   return node->info();
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::insert
*
*   INPUTS:      Node from which to insert.
*
*   OUTPUTS:     Returns Node inserted
*
*   SIDEEFFECTS: Creates a node in the tree.
*
*   DESCRIPTION: Add a new node in the tree. This routine
*                is useful if caller knows exactly where to
*                insert the new node. If node passed in is NULL
*                make the new inserted one the root.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
BtreeNode<NodeInfo> *
Btree<NodeInfo>::insert(BtreeNode<NodeInfo> *node,
			Int32 value,
			LinearPool *pool)
{
   BtreeNode<NodeInfo> *newNode = new BtreeNode<NodeInfo>;
   BtreeNode<NodeInfo> *tmp; // used for computing next pointer
   newNode->info().value(value);
   newNode->parent(node);
   theFinger = newNode;
   if (node == NULL) {	// root
      rootNode(newNode);
      leftMost(newNode);
      rightMost(newNode);
   } else if (value < node->info().value()) {
      DBG_ASSERT_MSG(node->left() == NULL, ("Non-null left node!\n"));
      node->left(newNode);
      // adjust next pointer. Same as initialize case above.
      newNode->next(node);
      tmp = newNode->predecessor();
      if (tmp) {
         tmp->next(newNode);
      }
      // if the earlier node was the leftmost, this 
      // one is the new leftmost. Check for non-null
      // leftMost first.
      if (leftMost()) {
         if (node == leftMost()) {
	    leftMost(newNode);
         }
      } else {
         leftMost(rootNode()->minimum());
      }
   } else {
      DBG_ASSERT_MSG(node->right() == NULL, ("Non-null right node!\n"));
      node->right(newNode);
      // adjust next pointer as in initialize() function.
      newNode->next(node->next());
      node->next(newNode);
      // if node was the rightmost, this one is the
      // new rightmost. Check for non-null rightMost
      // first.
      if (rightMost()) {
         if (node == rightMost()) {
	    rightMost(newNode);
         }
      } else {
	 // No choice but to set rightMost to tree_maximum
	 rightMost(rootNode()->maximum());
      }
   }

   // fix up tree if needed.
   newNode->info().color(RED);
   if (newNode != rootNode() && 
       newNode->parent()->info().color() == RED) {
      insertFixup(newNode);
   }
   rootNode()->info().color(BLACK);

   return newNode;
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::find
*
*   INPUTS:      Input value of a node
*
*   OUTPUTS:     Returns a BtreeNode pointer
*
*   SIDEEFFECTS: 
*
*   DESCRIPTION: Find a node with input value in the tree. Return
*                the node.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
inline BtreeNode<NodeInfo> *
Btree<NodeInfo>::findNode(Int32 value) const
{
   BtreeNode<NodeInfo> *node = rootNode();

   Int32 key;
   while (node != NULL && value != (key = node->info().value()) ) {
      if (value < key) {
	 node = node->left();
      } else {
	 node = node->right();
      }
   }
   theFinger = node;
   return node;
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::findClosestNode
*
*   INPUTS:      Input value of a node, Boolean parameter
*
*   OUTPUTS:     Returns a BtreeNode pointer
*
*   SIDEEFFECTS: 
*
*   DESCRIPTION: Find a node with input value in the tree. Return
*                the node. If not found, return next highest value.
*                If no such node, return NULL.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
BtreeNode<NodeInfo> *
Btree<NodeInfo>::findClosestNode(Int32 value, Boolean *found) const
{
   BtreeNode<NodeInfo> *node = rootNode();
   BtreeNode<NodeInfo> *prevNode = NULL;

   if (node == NULL) {
      *found = FALSE;
      return NULL;
   }

   Int32 key;
   do {
      key = node->info().value();
      prevNode = node;
      if (value == key) {
	 *found = TRUE;
         theFinger = node;
	 return node;
      } else if (value < key) {
	 node = node->left();
      } else {
	 node = node->right();
      }
   } while (node != NULL);

   *found = FALSE;
   if (value < key) {
      // the node is the closest one with value > key
      return prevNode;
   } else {
      // get the next node from prevNode.
      return prevNode->next();
   }
}
      
/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::remove
*
*   INPUTS:      Input value of a node, and a linearpool
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Remove a node with the value from the tree.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::remove(Int32 value, LinearPool *pool)
{
   BtreeNode<NodeInfo> *node = find(value);
   if (node) {
      remove(node, pool);
   }
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::remove
*
*   INPUTS:      A BtreeNode and a linearPool
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Remove a specific node from the tree. Uses the
*                standard algorithm from the Algorithms book, 
*                with modifications to allow for leftMost and
*                rightMost updates.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::remove(BtreeNode<NodeInfo> *node, LinearPool *pool)
{
   BtreeNode<NodeInfo> *succ;
   BtreeNode<NodeInfo> *tmp;
   BtreeNode<NodeInfo> *tmpParent = NULL; 
				   // parent of tmp, needed 
				   // for balancing if tmp is NULL.
				   // = NULL needed to prevent
				   // warning from compiler
   BtreeNode<NodeInfo> *tmpPred = NULL; // to cache away predecessor.
   NodeColor deletedNodeColor;	// color of deleted node.

   // reset next pointer of predecessor.
   tmpPred = node->predecessor();
   if (tmpPred) {
      tmpPred->next(node->next());
   }

   // adjust leftMost and rightMost, only if
   // non-null (null indicates deleted tree).
   if (leftMost() && node == leftMost()) {
      leftMost(node->next());
   }
   if (rightMost() && node == rightMost()) {
       rightMost(tmpPred);
   }
   // check for leaf first. If leaf, the node can be 
   // removed. If non-leaf, replace it by its 
   // successor
   if (node->left() == NULL || node->right() == NULL) {
      succ = node;
      theFinger = node->next();
   } else {
      succ = node->next();
      // make finger the succesor node.
      theFinger = succ;
   }
   if (succ->left()) {
      tmp = succ->left();
   } else {
      tmp = succ->right();
   }
   if (tmp != NULL) {
      // succ has either a right or left child
      tmp->parent(succ->parent());
   } else {
      // remember parent.
      tmpParent = succ->parent();
   }

   if (succ->parent() == NULL) {
      // we have reached root.
      rootNode(tmp);
   } else if (succ == succ->parent()->left()) {
      succ->parent()->left(tmp);
   } else {
      succ->parent()->right(tmp);
   }
   // remember color of node removed. If 
   // node is not the same as succ, this will be
   // overwritten.
   deletedNodeColor = node->info().color();
   if (succ != node) {
      // succ is not the input node
      // replace node with succ.

      // First, if succ->parent() is the node itself,
      // we are replacing succ->parent() with succ.
      // Adjust tmpParent for this case.
      if (succ->parent() == node) {
	 tmpParent = succ;
      }

      // replacement of node with succ
      succ->parent(node->parent());
      succ->left(node->left());
      succ->right(node->right());
      // remember color of node removed.
      deletedNodeColor = succ->info().color();
      // color replacee to match original color
      succ->info().color(node->info().color());
      // adjust node's children's parent pointers.
      if (node->left()) {
         node->left()->parent(succ);
      }
      if (node->right()) {
         node->right()->parent(succ);
      }
      if (node->parent()) {
	 // adjust parent's pointers.
	 if (node->parent()->left() == node) {
	    node->parent()->left(succ);
	 } else {
	    node->parent()->right(succ);
	 }
      } else {
	 // node removed was the parent node. Make
	 // succ the root node.
	 rootNode(succ);
      }
   }
   if (deletedNodeColor == BLACK) {
      // need to fixup tree
      deleteFixup(tmp, tmpParent);
   }
   delete((char *)node);
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::clear
*
*   INPUTS:      A linearpool from which storage was allocated.
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Delete the entire tree.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
inline void
Btree<NodeInfo>::clear(LinearPool *pool)
{
   // use next pointer to delete nodes.
   BtreeNode<NodeInfo> *node = leftMost();
   BtreeNode<NodeInfo> *nextNode;
   if (node == NULL) {
      // its possible that tree was simply marked empty. 
      // Start from tree minimum.
      if (rootNode()) {
	 node = rootNode()->minimum();
      }
   }

   while (node != NULL) {
      nextNode = node->next();
      delete((char *)node);
      node = nextNode;
   }
   rootNode(NULL);
   leftMost(NULL);
   rightMost(NULL);
   finger(NULL);
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::rotateLeft
*
*   INPUTS:      A BtreeNode to start rotating from.
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Rotate the tree left:
*                       n           y
*                      / \  --->   / \
*                     1   y       n   3
*                        / \     / \
*                       2   3   1   2
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::rotateLeft(BtreeNode<NodeInfo> *node)
{
   BtreeNode<NodeInfo> *y = node->right();

   // turn y's left subtree into node's right subtree.
   node->right(y->left());
   if (y->left() != NULL) {
      y->left()->parent(node);
   }

   // link node's parent to y.
   y->parent(node->parent());
   if (node->parent() == NULL) {
      theRootNode = y;
   } else if (node == node->parent()->left()) {
      node->parent()->left(y);
   } else {
      node->parent()->right(y);
   }

   // put node on y's left
   y->left(node);
   node->parent(y);
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::rotateRight
*
*   INPUTS:      A BtreeNode to start rotating from.
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Rotate the tree right:
*                       n         y
*                      / \ --->  / \
*                     y   3     1   n
*                    / \           / \
*                   1   2         2   3
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::rotateRight(BtreeNode<NodeInfo> *node)
{
   BtreeNode<NodeInfo> *y = node->left();

   // turn y's right subtree into node's left subtree.
   node->left(y->right());
   if (y->right() != NULL) {
      y->right()->parent(node);
   }

   // link node's parent to y.
   y->parent(node->parent());
   if (node->parent() == NULL) {
      theRootNode = y;
   } else if (node == node->parent()->left()) {
      node->parent()->left(y);
   } else {
      node->parent()->right(y);
   }

   // put node on y's right
   y->right(node);
   node->parent(y);
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::insertFixup
*
*   INPUTS:      A BtreeNode which was just inserted.
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: Tree structure gets modified.
*
*   DESCRIPTION: After insertion, rotate to correctly to 
*                preserve red-black property. Algorithm
*                adapted from the Cormen Et Al book.
*
*   NOTES:       
*                This procedure assumes that the
*                current node is not the root, and that
*                it is red and has a red parent.
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::insertFixup(BtreeNode<NodeInfo> *node)
{
   BtreeNode<NodeInfo> *y;
   do {
      // check if parent is left of grandparent or right
      if (node->parent() == node->parent()->parent()->left()) {
         // y is the 'uncle' of node
         y = node->parent()->parent()->right();
         if (y && y->info().color() == RED)  {
	    // both uncle and parent of node are red. Color
	    // both uncle and parent as black, and grandparent
	    // as red. Continue the loop to catch red-red cases.
	    node->parent()->info().color(BLACK);
	    y->info().color(BLACK);
	    node->parent()->parent()->info().color(RED);
	    node = node->parent()->parent();
	 } else {
	    if (node == node->parent()->right()) {
	       node = node->parent();
	       rotateLeft(node);
	    }
	    node->parent()->info().color(BLACK);
	    node->parent()->parent()->info().color(RED);
	    rotateRight(node->parent()->parent());
	 }
      } else {
         // y is the 'uncle' of node
         y = node->parent()->parent()->left();
         if (y && y->info().color() == RED)  {
	    // both uncle and parent of node are red. Color
	    // both uncle and parent as black, and grandparent
	    // as red. Continue the loop to catch red-red cases.
	    node->parent()->info().color(BLACK);
	    y->info().color(BLACK);
	    node->parent()->parent()->info().color(RED);
	    node = node->parent()->parent();
	 } else {
	    if (node == node->parent()->left()) {
	       node = node->parent();
	       rotateRight(node);
	    }
	    node->parent()->info().color(BLACK);
	    node->parent()->parent()->info().color(RED);
	    rotateLeft(node->parent()->parent());
	 }
      }
   } while (node != rootNode() && node->parent()->info().color() == RED);
}

/***********************************************************************
*
*   PROCEDURE:   Btree<NodeInfo>::deleteFixup
*
*   INPUTS:      A BtreeNode
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: Tree structure gets modified.
*
*   DESCRIPTION: Fixup tree to obey r-b property after deletion.
*
*   NOTES:       
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::deleteFixup(BtreeNode<NodeInfo> *node,
			     BtreeNode<NodeInfo> *nodeParent)
{
   BtreeNode<NodeInfo> *w;

   // reset nodeParent for non-null node so that
   // we don't have to check for null node every time.
   if (node != NULL) {
      nodeParent = node->parent();
   }

   // a null node is also considered black.
   while (node != rootNode() && 
	  (node == NULL || node->info().color() == BLACK)) {
      if (node == nodeParent->left()) {
	 w = nodeParent->right();
	 if (w->info().color() == RED) {
	    w->info().color(BLACK);
	    nodeParent->info().color(RED);
	    rotateLeft(nodeParent);
	    w = nodeParent->right();
	 }
	 // left or right null implies they are colored black.
	 if ((w->left() == NULL || w->left()->info().color() == BLACK) &&
	     (w->right() == NULL || w->right()->info().color() == BLACK)) {
	    w->info().color(RED);
	    node = nodeParent;
	    nodeParent = node->parent();
	 } else {
	    if (w->right() == NULL || w->right()->info().color() == BLACK) {
	       w->left()->info().color(BLACK);
               w->info().color(RED);
	       rotateRight(w);
	       w = nodeParent->right();
	    }
	    w->info().color(nodeParent->info().color());
	    nodeParent->info().color(BLACK);
	    w->right()->info().color(BLACK);
	    rotateLeft(nodeParent);
	    // terminate loop
	    node = theRootNode;
	 }
      } else {
	 w = nodeParent->left();
	 if (w->info().color() == RED) {
	    w->info().color(BLACK);
	    nodeParent->info().color(RED);
	    rotateRight(nodeParent);
	    w = nodeParent->left();
	 }
	 if ((w->right() == NULL || w->right()->info().color() == BLACK) &&
	     (w->left() == NULL || w->left()->info().color() == BLACK) ) {
	    w->info().color(RED);
	    node = nodeParent;
	    nodeParent = node->parent();
	 } else {
	    if (w->left() == NULL || w->left()->info().color() == BLACK) {
	       w->right()->info().color(BLACK);
	       w->info().color(RED);
	       rotateLeft(w);
	       w = nodeParent->left();
	    }
	    w->info().color(nodeParent->info().color());
	    nodeParent->info().color(BLACK);
	    w->left()->info().color(BLACK);
	    rotateRight(nodeParent);
	    // terminate loop
	    node = theRootNode;
	 }
      }
   }

   if (node != NULL) {
      node->info().color(BLACK);
   }
}



/***********************************************************************
*
*   PROCEDURES:  dbgPrint(), inorder, preorder and postorder print.
*
*   INPUTS:      None.
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Debug routines.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::dbgPrint()
{
   // print inorder
   fprintf(stderr, "{ ");
   inOrderPrint(rootNode());
   fprintf(stderr, "}\n");
   fflush(stderr);
}


template <class NodeInfo>
void
Btree<NodeInfo>::inOrderPrint(BtreeNode<NodeInfo> *root)
{
   if (root) {
      inOrderPrint(root->left());
      fprintf(stderr, "%d ", root->info().value());
      inOrderPrint(root->right());
   }
}

template <class NodeInfo>
void
Btree<NodeInfo>::preOrderPrint(BtreeNode<NodeInfo> *root)
{
   if (root) {
      fprintf(stderr, "%d ", root->info().value());
      preOrderPrint(root->left());
      preOrderPrint(root->right());
   }
}

template <class NodeInfo>
void
Btree<NodeInfo>::postOrderPrint(BtreeNode<NodeInfo> *root)
{
   if (root) {
      postOrderPrint(root->left());
      postOrderPrint(root->right());
      fprintf(stderr, "%d ", root->info().value());
   }
}

#ifndef RELEASE

/***********************************************************************
*
*   PROCEDURES:  dbgDraw and related procedures
*
*   INPUTS:      file name
*
*   OUTPUTS:     None.
*
*   SIDEEFFECTS: None.
*
*   DESCRIPTION: Draws a VCG graph of the red-black tree in
*                specified filename.
*
*   NOTES:       
* 
***********************************************************************/
template <class NodeInfo>
void
Btree<NodeInfo>::dbgDraw(char *fileName)
{

}

// recursively add a subtree into VCG graph.
template <class NodeInfo>
void
Btree<NodeInfo>::vcgAddSubtree(
		VCGGraph &vcg,
		BtreeNode<NodeInfo> *node, 
		VCGNode &vcgNode,
		SubTree side)
{
  
}

#endif // ndef RELEASE

#endif // BTREE_H
