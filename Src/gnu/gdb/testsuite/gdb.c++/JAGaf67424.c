void noninline (void);

int global;
inline void vinline (void)
{
    global++;
    if (global == 3)
        noninline();
}

int main ()
{
        vinline();
        global = 1;
        vinline();
        vinline();
}

void noninline (void)
{

}

