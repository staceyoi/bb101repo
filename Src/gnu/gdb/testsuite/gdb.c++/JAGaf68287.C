template <class  T>
class A {
public :
A() { a =10; }
int foo () { return a++;}
private :
    T a;
};
 
int main() {
A<int> a;
A<float> a1;
int i;
 
i = a.foo();
 
return i;
}
