/******************************************************************************
*
* (c) Copyright 1994
* (p) Protected 1994
*
* The Hewlett-Packard Company
* California Language Laboratory
* Cupertino, California
*
* HP Confidential
*
*******************************************************************************
* Set.h:
*     Set utility class definition
*
* The Set class provides a facility for representing sparse sets.
*
* The new implementation of sets is geared towards performance. 
* Sets are classified into three types:
*
* InitialSet: An empty set
* SmallSet  : Set with #elements < MAX_SMALL_SETSIZE (1-8 elements)
* LargeSet  : > MAX_SMALL_SETSIZE elements.
*
* All member functions know to work both small sets and large, and
* can convert sets from small to large.
*
* Large sets use Btree routines to perform insertions and deletions.
* Each node in the Btree is of type BtreeNode<SetData>, and the underlying
* SetData has a bit map for a certain number of elements.
* Operations on set members are done by first identifying the correct
* BtreeNode, getting the SetData from it, and performing operations
* on the SetData.
*
******************************************************************************/

#ifndef SET_H
#define SET_H

#include <stdio.h>
#include <limits.h>
#include <malloc.h>
//#include "Pool.h"

#define TRUE 1
#define FALSE 0
#define DBG_ASSERT(cond_)                 (void)0
#define DBG_ASSERT_MSG(cond_,msg_)                 (void)0
#define CLO_MALLOC(a)   malloc(a)

#include "langtypes.h"

#include "SetSize.h"
#include "Btree.h"

class LinearPool;


typedef UInt32          Count;
typedef UInt32          ShortIndex;

namespace NSTail { class Set; }
using namespace NSTail;

class PoolSet;
class SetData;
class SetIterator;
class SlowSetIterator;

template <class NodeInfo> class Btree;
template <class NodeInfo> class BtreeNode;

#define NullSet               ((Set*) 0)
#define NullSetData           ((SetData*) 0)
#define NullSetIterator       ((SetIterator*) 0)
#define NullSlowSetIterator   ((SlowSetIterator*) 0)

void intersect(const Set& aSet, const Set& bSet, Set& cSet);
void subtract(const Set& a, const Set& b, Set& c); 

typedef UInt32   Element;

// typedef for type of set
enum SetType {
   InitialSet,	// implies that set's been just created
   SmallSet,
   LargeSet
};

// hint for type of set
enum SetSizeHint {
   LargeHint
};

// The value of an element is (element / <number of elements in a setData>).
// This gives us the setData bucket which will hold this element. Its
// called value because it corresponds to the value used to index into
// the Btree underlying the Set representation.
#define SET_ELEMENT_VALUE(element)  ((element) >> SetBvBits)

// the corresponding mapping back to an element
#define SET_ELEMENT(value) ((value) << SetBvBits)

// for getting at word# from an element.
#define SET_DATA_WORD(element)	(((element)>>SetDataBits)&(SetDataWords-1))

// for getting the index of an element within a word.
#define SET_INDEX_VALUE(element)	((element) & (SetDataSize-1))

// Some defined constant masks for 64 bit operations

// Most significant bit is one.
#define MSB_ONE	0x8000000000000000ull


// Using Int rather than ShortIndex(UInt32) for i is more efficient for
// the PA Optimizer.
#define FOR_EACH_SET_DATA(i)    for (Int i=0; i < SetDataWords; ++i)

// I apologize for these macros.  They're only here to get unrolling for the
// various SetData loops -- the build environment C++ compiler does not
// do this (08/96).  When it does, we should use a loop macro like the
// one above.
#define UNROLLED_SETDATA_LOOP(LHS, OP, RHS)  LHS[0] OP RHS[0]; \
                                                 LHS[1] OP RHS[1];

#define UNROLLED_SETDATA_ZERO(LHS)   LHS[0] = 0; \
                                         LHS[1] = 0;

/******************************************************************************
* CLASS:        SetData
* BASE CLASSES: None
* SUBCLASSES:   None
* FRIENDS:      Set, SetIterator, SlowSetIterator.
*
* PURPOSE:
*	SetData holds the actual data representing the elements of a set.
*
******************************************************************************/
class SetData {
   // these two are for retrieving/storing color and
   // value of a node.
   friend class Btree<SetData>;
   friend class BtreeNode<SetData>;

   friend class NSTail::Set;
   friend class SetIterator;
   friend class SlowSetIterator;

   friend void ::intersect(const Set& a, const Set& b, Set& c); 
   friend void ::subtract(const Set& a, const Set& b, Set& c); 

private:

   SetData()
   {
      inUseLock = 0;
      // only here because compiler doesn't unroll loop.
      UNROLLED_SETDATA_ZERO(bits)
   }

   // c'tor with data copied from copyData
   SetData(SetData &copyData);

   SetData(const SetData& data);		// dummy copy c'tor

   ~SetData() {}					// d'tor
   
   // return the 1st element in a setData. SetData should
   // be non-null.
   Element firstElement() const;

   // return the next element in a setData, given 
   // the current element.
   // If element is not found, found is set to FALSE.
   Element nextElement(Element elem, Boolean *found) const;

   // member functions to allow access to value from binary tree.
   void value(Int32 v) { theValue = v; }
   Int32 value() const {return (Int32) theValue; }

   // functions for node color used by binary tree.
   NodeColor color() const {return theColor; }
   void color(NodeColor c) { theColor = c; }

   Boolean isNull() const                      // This data item is empty?
   {
      // Originally we had a loop here which we assumed would be
      // unrolled by the PA optimizer.  However, it turns out the
      // version we're currently using is not up to the task.  So,
      // for now we'll unroll it by hand -- dmg 08/12/96

      if ((bits[0] != 0) ||
          (bits[1] != 0))
         return FALSE;

      return TRUE;
   }

   Count operator*() const;			// number of 1's in data

   SetData& operator= (const Element element)	// assign element to this
   {
      theValue = SET_ELEMENT_VALUE(element);
      bits[SET_DATA_WORD(element)] = MSB_ONE >> SET_INDEX_VALUE(element);
      return *this;
   }

   SetData& operator= (const SetData& data)	// assign data to this
   {
      theValue = data.value();
      // compiler doesn't unroll loop.
      UNROLLED_SETDATA_LOOP(bits, =, data.bits)

      return *this;
   }


   SetData& operator+= (const Element element)	// add element to this
   {
      DBG_ASSERT(theValue == SET_ELEMENT_VALUE(element));
      UInt64 ii = MSB_ONE >> SET_INDEX_VALUE(element);
      bits[SET_DATA_WORD(element)] |= ii;
      return *this;
   }

   void addElements(const UInt32 position, UInt64 mask)
   { 
       // Illegal position
     DBG_ASSERT((position < SetDataWords));
     bits[position] |= mask; 
   } 

   SetData& operator+= (const SetData& data);	// add data to this

   // add element to this and set setChanged to TRUE if the element 
   // added didn't exist in the set Data before. 

   SetData& unionAssign(const Element element, Boolean& setChanged) 
   {
      DBG_ASSERT(theValue == SET_ELEMENT_VALUE(element));
      UInt64 ii = MSB_ONE >> SET_INDEX_VALUE(element);
      setChanged |= ((bits[SET_DATA_WORD(element)] & ii) == 0); 
      bits[SET_DATA_WORD(element)] |= ii;
      return *this;
   }

   // add data to this and set the flag to TRUE if the set data changed 
   // after the union. 

   SetData& unionAssign(const SetData& data, Boolean& setChanged) ;


   // subtract element from this
   SetData& operator-= (const Element element)
   {
      DBG_ASSERT(theValue == SET_ELEMENT_VALUE(element));
      bits[SET_DATA_WORD(element)] &= ~(MSB_ONE >> SET_INDEX_VALUE(element));
      return *this;
   }

   SetData& operator-= (const SetData& data);	// subtract data from this

   SetData& operator&= (const Element element)	// and element to this
   {
      DBG_ASSERT(theValue == SET_ELEMENT_VALUE(element));
      bits[SET_DATA_WORD(element)] &= MSB_ONE >> SET_INDEX_VALUE(element);
      return *this;
   }

   SetData& operator&= (const SetData& data);	// and data to this

   Boolean  operator& (const SetData& data);    // and data

   // checks if this == data
   Boolean operator==(const SetData& data) const;

   
   // returns the bits at index
   UInt64
   getBits(const UInt32 index) const
   {
       DBG_ASSERT(this != NullSetData);
       return bits[index];
   }

   // locking/unlocking methods. Used by SetIterator.

   void setLock() 
   {
       // Too many locks!
      DBG_ASSERT(inUseLock < SHRT_MAX);
      ++inUseLock;
   }

   Int getLock() const { return inUseLock; }

   // clear the lock and return  lock value
   Int clearLock()
   {
       // SetData not in use
      DBG_ASSERT(inUseLock>0);
      return(--inUseLock);
   }
   
   // element ^ set
   friend Boolean operator^(const Element element, const Set& set);

   static const char lookupTable[256];	// indexing offset for next element

   UInt32	theValue;    // first element in setData is theValue >> SetBvBits
   NodeColor    theColor:1;  // color function required by binary tree.
   Int16	inUseLock;   // Number of locks for this SetData.

   UInt64       bits[SetDataWords];	// set data bits
};


/******************************************************************************
* CLASS:        Set
* BASE CLASSES: None
* SUBCLASSES:   PoolSet
* FRIENDS:      SetIterator, SlowSetIterator
*
* PURPOSE:
*	This derivative of the Set class uses storage pools to allocate
* data for sets. The advantage of using this over Set class is that when
* you create and manipulate sets for a short time as in live range analysis,
* you don't have to delete each and every Set created, instead, you can
* free up the storage pool, and the space will be recycled.
*
******************************************************************************/
class PoolSet ;

// Size of a small set.
#define MAX_SMALL_SETSIZE 8

namespace NSTail {

class Set {

   friend class ::SetIterator;
   friend class ::SlowSetIterator;

public:

    // These pools are set by the constructor of Procedure class and used for
    // memory allocation by Set
    static LinearPool* setPool;
    static LinearPool* btreeNodePool;

    static void SetPool(LinearPool* pool) { setPool = pool; }
    static void SetBtreeNodePool(LinearPool* pool) { btreeNodePool = pool; }

   // constructor, creates a null set
   Set()
   {
      initSet(FALSE);
   }

   // constructor with large set hint
   Set(SetSizeHint)
   {
      initSet(FALSE);
      theSetType = LargeSet;
      root().initialize();
   }

   // friend to perform set intersection of two sets.

   friend void ::intersect(const Set& a, const Set& b, Set& c); 

   // to peform c = a - b
   friend void ::subtract(const Set& a, const Set& b, Set& c); 

   // copy constructor
   Set(const Set& aSet)
   { 
      isPoolSet = FALSE;
      theSetType = InitialSet;
      inUseLock = 0;
      *this = aSet;
   }
   
   virtual ~Set()
   {
       // Destructing set when iterator is active!
      DBG_ASSERT(getLock() == 0);

      // don't do anything for small sets.
      if (isLarge()) {
	 // cannot delete a large set which is locked.
         largeClear();
      }
   }

   Boolean isNull() const // is it a null set?
   {
      if (isInitial()) {
	 return TRUE;
      }
      if (isSmall()) {
	 if (theSmallSetSize == 0) {
	    return TRUE;
	 } else return FALSE;
      }
      /* large set */
      if (root().leftMost() == NULL) {
	 return TRUE;
      }
      return largeIsNull();
   }

   // returns the first element of the Set. Error if a null set.
   Element firstElement() const
   {
       // first element of a Null Set?
      DBG_ASSERT(isNull()==FALSE);
      if (isSmall()) {
	 return theInfo.elems[0];
      } else {
	 return largeFirstElement();
      }
   }

   void clear()
   {
       // clear called for a locked set!
      DBG_ASSERT(getLock() == 0);
      if (isInitial()) {
	 return;
      }
      if (isSmall()) {
	 theSetType = InitialSet;
      } else {
         largeClear();
      }
   }

   Set& operator=(const Set& set)		// assignment operator
   {
       // Self assignment not allowed
      DBG_ASSERT(this != &set);
      // Set being assigned to is not empty
      DBG_ASSERT(isInitial());

      if (set.isInitial()) {
	 return *this;
      }
      if (set.isSmall()) {
	 Int i;
	 for (i = 0; i < set.theSmallSetSize; i++) {
	    theInfo.elems[i] = set.theInfo.elems[i];
	 }
	 smallSetSize(set.theSmallSetSize);
	 setType(SmallSet);
	 return *this;
      }
      return largeAssign(set);
   }


   Set& operator=(const Element element)	// assignment operator
   {
       // Set being assigned to is not empty
      DBG_ASSERT(isInitial());

      if (element > MAX_UINT_16) {
	 // Too big for unsigned short values in SmallSet.
         convertToLarge();
         addElement(element);
      } else {
	 // Usual case, will fit in unsigned short.
         setType(SmallSet);
         smallSetSize(1);
         theInfo.elems[0] = element;
      }
      return *this;
   }

   Count operator*() const
   {
      if (isInitial()) {
	 return 0;
      } else if (isSmall()) {
	 return smallSetSize();
      } else {
	 return largeCardinality();
      }
   }

   Count cardinality() const { return * *this; }

   // Short-cut to catch small sets. Returns true if
   // size of set is larger than n.
   Boolean cardinalityGreaterThan(Count n) const
   {
      if (isInitial()) {
	 return FALSE;
      } 
      if (isSmall()) {
	 return (theSmallSetSize > n);
      }
      return largeCardinalityGreaterThan(n);
   }

   Set& operator+=(const Element element) // set += element
   {
      if (isInitial()) {
         if (element <= MAX_UINT_16) {
	    // Not too big for unsigned short values in SmallSet.
	    setType(SmallSet);
	    smallSetSize(1);
	    theInfo.elems[0] = element;
	    return *this;
         } else {
	    // Too big for unsigned short values in SmallSet.
            convertToLarge();
            // Fall through to general code.
         }
      }
      addElement(element);
      return *this;
   }

   Set& operator+=(const Set& set)		// set += set
   {
     // simple cases first
     if (set.isInitial()) {
       return *this;
     }
     return realPlusEquals(set);
   }

   // for adding a set of consecutive elements to a set, 
   // we define an interface that takes the first element 
   // and a 32-bit mask representing the all the elements 
   // in the set. It is supposed to improve compile time. 

   Set& addElements(const Element element, UInt64 mask); 

   // it does a set union and sets the flag setChanged to true 
   // if the set changed after union.

   Set& unionAssign(const Set& set, Boolean& setChanged);  
   Set& unionAssign(const Element element, Boolean& setChanged);

   Set& operator-=(const Element element);	// set -= element
   Set& operator-=(const Set& set)		// set -= set
   {
     if (isInitial() || set.isInitial()) {
       return *this;
     }
     return realMinusEquals(set);
   }
     
   Set& operator*=(const Set& set);		// set *= set, intersection
   Boolean operator&(const Set& set);           // any intersection between
                                                // the two sets?

   Boolean operator>(const Set& set) const;	// set > set
   Boolean operator>=(const Set& set) const;	// set >= set

   Boolean operator==(const Set& set) const;	// set == set
   Boolean operator!=(const Set& set) const;	// set != set


   // element * set ; is element in set ?
   Boolean operator*(const Element element) const
   {
      if (isInitial()) {
	 return FALSE;
      }

      if (isSmall()) {
	 for (Int i = 0; i < theSmallSetSize; i++) {
	    if (theInfo.elems[i] == element) {
	       return TRUE;
	    } else if (theInfo.elems[i] > element) {
	       break;
	    }
	 }
	 return FALSE;
      }
      // large set
      return largeMember(element);
   }


   Boolean member(const Element element) const { return *this * element; }


   void print(FILE *out);			// dump set to out   
   char* image();				// dump set to string


protected:

   // Get/Set the poolSet attribute
   Boolean poolSet() const { return isPoolSet; }
   void poolSet(Boolean value) { isPoolSet = value; }
   Set(Boolean value)
   {
      initSet(value);
   }

   Set(Boolean value, SetSizeHint)
   {
      initSet(value);
      theSetType = LargeSet;
      root().initialize();
   }

private:

   // Make these private, since they should only be called from within
   // the Set class.
   Set& realPlusEquals(const Set& set);		// rest of set += set
   Set& realMinusEquals(const Set& set);	// set -= set

   void initSet(Boolean value)
   {
      poolSet(value);
      theSetType = InitialSet;
      inUseLock = 0;
      // Global and static Set not allowed. See the comment in the 
      // source code for more detail
      DBG_ASSERT(poolSet() || setPool != NULL);

      // IMPORTANT!!!!!!!!!!!!!!
      // The new interface uses a LinearPool within Procedure object
      // to allocate set objects. 
      // So this DBG_ASSERT will punt if you ever create a global instance
      // of Set objects. Examples include 
      //  Set myset;
      // or
      //  static Set mystaticSet ;
      // This will again punt if you derive a class C out Set and
      // then create global instances of C.
      // 
      // Thumb Rule: 
      //   Use PoolSet if you really want a global set. 
      //   Right now we can pass Procedure::currentProc->staticPoolSetStorage
      //   to the PoolSet object. But, as a general rule, do this only
      //   if you need the PoolSet to be live across procedures.
   }

   // is a large set NULL?
   Boolean largeIsNull() const;

   // first element in a large set
   Element largeFirstElement() const;

   // assignment
   Set& largeAssign(const Set& set);

   Boolean largeCardinality() const;

   // if card(S) > n
   Boolean largeCardinalityGreaterThan(Count n) const;

   // add an element to a large set
   void largeAddElement(const Element element);

   // clear a large set.
   void largeClear();

   // is element a member of this large set?
   Boolean largeMember(const Element element) const;

   void convertToLarge();

   // add an element to a non-initial set.
   void addElement(const Element element);

   // add several elements from a small set into current set.
   // May convert it to a large set. Return a boolean if
   // set has been changed. Only works for small sets.
   Boolean smallAddnElements(const UInt16 *element, const Int numElems);

   // remove a node from the tree. Won't be able to remove
   // if there's a lock on setData within the node. The
   // routine assumes that the caller has already ensured
   // that the node's data is null.
   void removeNodeIfPossible(BtreeNode<SetData> *node);

   // This is similar to large clear, but deletes storage 
   // only if there's no lock on any node. 
   void largeMakeEmpty();


   // Union for set members.
   //
   // While the external interface to these classes is the 32-bit Element
   // typedef, in practice the values usually stored are almost always
   // small enough to fit in 16 bits.  That led to the redefinition of
   // the Small Set representation to hold twice as many shorts instead
   // of Elements.  Any Element value passed in greater than MAX_UINT_16
   // results in the Set being converted to the Large representation
   // regardless of the number of elements currently held.
   union SetInfo {
      Btree<SetData> root;
      UInt16 elems[MAX_SMALL_SETSIZE];
   };
      

   // Data member accessor functions

   const Btree<SetData>& root() const
   {
       // Asking for root of a small set
      DBG_ASSERT(theSetType == LargeSet);
      // this cast is necessary to get over 'future' errors by
      // aCC when returning a non-const Btree.
      return theInfo.root;
   }

   // this version is used when the caller has a non-const set type.
   Btree<SetData>& root()
   {
       // Asking for root of a small set
      DBG_ASSERT(theSetType == LargeSet);
      return theInfo.root;
   }

   Int smallSetSize() const { return theSmallSetSize; }
   void smallSetSize(Int size) { theSmallSetSize = size; }

   SetType setType() const { return theSetType; }
   void setType(SetType t) { theSetType = t; }

   Boolean isInitial() const { return (theSetType == InitialSet); }
   Boolean isSmall() const { return (theSetType == SmallSet); }
   Boolean isLarge() const { return (theSetType == LargeSet); }

   // locking/unlocking methods. Used by SetIterator. This is
   // necessary even though there's a lock on each setData since
   // it make it easy to check if there's any lock at all on
   // any of the SetDatas in the set.

   Int getLock() const { return inUseLock; }

   void setLock() 
   { 
       // Too many locks!
      DBG_ASSERT(inUseLock < SHRT_MAX);
      ++inUseLock;
   }

   // clear the lock and return  lock value
   Int clearLock()
   {
       // Set not in use
      DBG_ASSERT(inUseLock>0);
      --inUseLock;
      return(inUseLock);
   }

   // poor man's virtual function
   LinearPool* getStoragePool() const;

   // private data fields
   Boolean      isPoolSet:1;            // Is this a Pool Set?
   SetType	theSetType:2;
   Int8		theSmallSetSize;	// 0 to MAX_SMALL_SETSIZE
   Int16	inUseLock;		// lock on any component of set.
   SetInfo 	theInfo;

};

}; // namespace NSTail


/******************************************************************************
* CLASS:        SlowSetIterator
* BASE CLASSES: None
* SUBCLASSES:   None
* FRIENDS:      None
*
* PURPOSE:
*       Slow Iterator to the Set object.  Allows additions/deletions past
*       the current point of iteration (our original Set iterator).
*
******************************************************************************/

class SlowSetIterator {

public:
   // creates iterator to first element
   SlowSetIterator(Set& set)
   {
      theSet = &set;
      set.setLock();
      init();
   }

   // destroys the set iterator
   ~SlowSetIterator()
   {
      theSet->clearLock();
      clearCurrent();
   }

   // reset the iterator
   SlowSetIterator& operator=(const UInt32 i)
   {
      DBG_ASSERT (i==0);
      clear();
      return *this;
   }

   // get current element
   Element operator*() const
   {
      // check it is not a universal set, and its not finished
      DBG_ASSERT(this != NullSlowSetIterator && !finished);
      return elem;
   }

   void operator++();      // get next iterator, NULL if EOS

   // check if iterator is null
   Boolean operator==(const UInt32 i) const
   {
      DBG_ASSERT (i==0);
      return finished;
   }

   // check if iterator is not null
   Boolean operator!=(const UInt32 i) const
   {
      DBG_ASSERT(i==0);
      return (!finished);
   }

   SlowSetIterator(const SlowSetIterator &);    // Copy constructor

private:

   SlowSetIterator& operator=(const SlowSetIterator&); // dummy assignment o'tor

   Boolean initial() const { return (theSetType == InitialSet); }
   Boolean small() const { return (theSetType == SmallSet); }
   Boolean large() const { return (theSetType == LargeSet); }

   void clear()                // resets iterator; points to first element
   {
      clearCurrent();
      init();
   }

   // for initialization and re-initialization
   void init();

   // clear lock on setData
   void clearCurrent();

   // data fields

   Set          *theSet;                // the set it is iterating over
   SetType	theSetType;		// cached from set.
   ShortIndex   smallSetIndex;          // Index of elem in small Set.
   Element	elem;			// the current element.
   Boolean      finished;		// if all elements are over
   BtreeNode<SetData>* theCurrent;	// The current node.
};

/******************************************************************************
* CLASS:        SetIterator
* BASE CLASSES: None
* SUBCLASSES:   None
* FRIENDS:      None
*
* PURPOSE:
*	Iterator to the Set object
*
******************************************************************************/
class SetIterator {

public:
   // creates iterator to first element
   SetIterator(Set& set)
   {
      theSet = &set;
      set.setLock();
      init();
   }

   // destroys the set iterator
   // Also removes lock on theNext.
   ~SetIterator()
   {
      theSet->clearLock();
      clearNext();
   }

   // reset the iterator
   SetIterator& operator=(const UInt32 i)
   {
      DBG_ASSERT (i==0);
      clear();
      return *this;
   }

   // get current element
   Element operator*() const
   {
       // Cannot deference a NULL set!
      DBG_ASSERT(!initial());
      return setCache[index];
   }

   void operator++()
   {
      index++;
      // for small sets, we are done.
      if (! small()) {
         if (index >= maxIndex) {
	    if (curBits != 0) {
	       fillCache();
	    } else {
	       nextSetData();
	    }
	 } 
      }
   }

   // check if iterator is null
   Boolean operator==(const UInt32 i) const
   {
      DBG_ASSERT (i==0);
      return (index >= maxIndex ) ? TRUE : FALSE;
   }

   // check if iterator is not null
   Boolean operator!=(const UInt32 i) const
   {
      DBG_ASSERT(i==0);
      return (index >= maxIndex ) ? FALSE : TRUE;
   }

private:

   SetIterator& operator=(const SetIterator&);// dummy assignment o'tor
   SetIterator(const SetIterator&);	// dummy copy c'tor

   void clear()		// resets iterator; points to first element
   {
      clearNext();
      init();
   }

   // removes lock on next pointer and deletes the node if possible.
   void clearNext();

   void init();

   // This function switches to the next SetData.
   // A non-null parameter implies it needs to start from 
   // parameter node. A null parameter implies clear the lock on 
   // theNext if it exists, and lock theNext if it exists.
   void nextSetData(BtreeNode<SetData>* node = NULL);

   // fills the internal cache for small sets.
   void fillCache();

   Boolean initial() const { return (theSetType == InitialSet); }
   Boolean small() const { return (theSetType == SmallSet); }
   Boolean large() const { return (theSetType == LargeSet); }

   // data fields

   UInt64		curBits;         // current bits to be examined
   UInt64		nextBits;	 // next bits if curbits is 0.
   Int32		arrIndex;        // index into bits array.
   Element		baseElement;     // The element used as base
					 // for computing next element.
   Set*          	theSet;          // Our one and only
   SetType		theSetType;      // Cached from theSet
   BtreeNode<SetData>*  theNext;	 // next node
   Int32        	index;    	 // Current index into bucket
   Int32        	maxIndex;        // Number of elements in bucket
   Element		setCache[64];    // Cache for sets. Hold 64 elements
};
#endif // SET_H
