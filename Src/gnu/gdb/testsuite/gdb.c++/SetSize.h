/******************************************************************************
*
* (c) Copyright 1994
* (p) Protected 1994
*
* The Hewlett-Packard Company
* California Language Laboratory
* Cupertino, California
*
* HP Confidential
*
*******************************************************************************
* SetSize.h:
*     Set utility size definitions.
*
* This header file defines the size parameters associated with the Set
* class.  
******************************************************************************/

#ifndef SET_SIZE_H
#define SET_SIZE_H

/* The size parameters are defined in a separate header file from Set.h 
 * largely to facilitate the definition of certain predefined integer 
 * ResourceIds in ResourceManagerEnums.h (e.g. the OUT regs that we'd
 * like to allocate out of a new GR chunk), without including all of Set.h.)
 */

// enum for various set sizes, which are constants
enum {SetDataSize = 64,			   // bits per data word
      SetDataBits = 6,			   // 2**SetDataBits = SetDataSize
      SetDataWords = 2,			   // number of 'int64s' to hold set data
      SetBvSize = SetDataSize*SetDataWords,// bits per data block
      SetBvBits = 7			   // 2**SetBvBits = SetBvSize
};

#endif // SET_SIZE_H
