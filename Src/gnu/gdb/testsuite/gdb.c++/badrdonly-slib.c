
#include <stdio.h>
#include <string.h>
#include <assert.h>

//
// Trace bits for range generation. These correspond to the various
// strings that can be specified in the +Uzmisc=trgenctl option. 
// 
#define TRACE_BASIC            (1<<0)
#define TRACE_DFA              (1<<1)
#define TRACE_RADUMP           (1<<2)
#define TRACE_FINALDUMP        (1<<3)
#define TRACE_RGEN             (1<<4)
#define TRACE_COLLECT          (1<<5)
#define TRACE_BOUNDARY         (1<<6)
#define TRACE_NOISYHANDLES     (1<<7)

unsigned setTraceOptions(char *opts)
{
   unsigned rval = 0;

   if (opts)
   {
      rval = TRACE_BASIC;
      char *s, *last, *o = opts;
      
      static const char *options[] = {
         "all",
         "dfa", 
         "collect", 
         "radump", 
         "finaldump",
         "boundary",
         "rgen",
         "noisyhandles",
         0
      };
      static const unsigned flags[] = { 
         TRACE_COLLECT|TRACE_RADUMP|TRACE_FINALDUMP|TRACE_RGEN|TRACE_BOUNDARY,
         TRACE_DFA,
         TRACE_COLLECT,
         TRACE_RADUMP,
         TRACE_FINALDUMP,
         TRACE_BOUNDARY,
         TRACE_RGEN,
         TRACE_NOISYHANDLES,
         0
      };
      assert((sizeof(options)/sizeof(char*)) ==
	     (sizeof(flags)/sizeof(unsigned)));
      
      do {
         char *tok = strtok(o, "+");
         o = NULL;
         if (tok)
         {
            bool found = false;
            for (unsigned j = 0; options[j]; ++j)
            {
               if (!strcmp(tok, options[j]))
               {
                  rval |= flags[j];
                  printf("flags[%d] is 0x%x\n", j, flags[j]);
                  found = true;
                  break;
               }
            }
            if (!found)
               fprintf(stderr, "%s: ignoring bad token %s "
                       "in options string %s\n",
                       __FILE__, tok, opts);
         } else
            break;
      } while (1);
   }

   return rval;
}
