class Temp {
public:
    int temp;
    int type;
    Temp(); 
    Temp(int t);
    Temp(int t, int ty);
    int conv();
    int conv(int t);
    int rconv();
    int rconv(int t);
};

Temp:: Temp() { temp = 32, type = 1; }
Temp:: Temp(int t) { temp = t, type = 1; }
Temp:: Temp(int t, int ty) { temp = t, type = ty; }
int Temp:: conv() { return temp*2; }   // bogus formula
int Temp:: conv(int t) { return t*2; } // bogus formula
int Temp:: rconv() { return temp/2; }   // bogus formula
int Temp:: rconv(int t) { return t/2; } // bogus formula


int main(int argc, char **argv)
{
    Temp t2(32);
    Temp t3(32, 2);

    t2.conv();
    t3.conv(100);
}
