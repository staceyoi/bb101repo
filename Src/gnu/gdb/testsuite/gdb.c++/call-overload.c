/* Written by bindu to test overload resolution for
   command line calls */

int foo (int i)
{
  int j;
  j = i;
  return j;
}

int foo (char c)
{
  int i = 0;
  if (c == 'i')
    i = 1;
  return i;
}

int foo (char c, int i)
{
  int j = 1;
  if (c == 'i')
    j = 0;
  j = j + i;
  return j;
}

int 
main ()
{
  int i = 10;
  char c = 'i';
  foo(i);
  foo (c);
  foo (c, i);
  return 0;
}
