


class Base {
 public: 
   virtual void foo (void) {}
};

// A derived class that overrides the virtual function.

class Derived: public Base {
 public: 
  void foo (void);
};

class Master {
  public :
    int bar (void) const { return 1; }

  private:
    Derived data[2];
};

// A class that has a pointer to the Master class and o
//contains a method
// that calls a method from the Master class
class Worker {
public:
        Worker(Master *master_) : master(master_) {}

        void doit(void);

private:
        Master  *master;
};


