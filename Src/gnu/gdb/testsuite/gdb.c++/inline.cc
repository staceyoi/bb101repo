struct X { float a; float b; } xx;

inline int lum( int x )
{
  if (x < 10) {
    x = x < 5 ? x +3 : x+ 13;
    return x* x;
  }
  else  {
    x = x > 9 ? x -5 : x- 10;
    return x+2;
  }
}

inline int tum( int x )
{
  if (x < 10) {
    x = x < 5 ? x +3 : x+ 13;
    return x* x;
  }
  else  {
    x = x > 9 ? x -5 : x- 10;
    return x+2;
  }
}

inline int foo( int x )
{
  int y = 0;

  y = x-2;
  if (x < 30) {
    y = x+5;
    x = x > 7 ? x -3 : x- 13;
    return x* 6;
  }
  else  {
    x = x <= 10 ? x +21 : x+ 16;
    return x+18;
  }
}

// non inlined function
int bar(int x)
{
    return x%2;
}
int main()
{
  int a;
  a = foo(tum(lum(10)));

  a = foo( 30 );

  a = bar(30);
  a = foo( 30 );
  a = bar(40);
  a = foo( 10 );
  a = foo( 30 );
  a = foo( 30 );
  a = bar(40);
}
