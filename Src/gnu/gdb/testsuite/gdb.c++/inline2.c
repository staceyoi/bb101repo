struct X { float a; float b; } xx;


int bum( int x )
{
  if (x < 10) {
    x = x < 5 ? x +3 : x+ 13;
    return x* x;
  }
  else  {
    x = x > 9 ? x -5 : x- 10;
    return x+2;
  }
}
inline int small_function(int x)
{
    x += 13;
    bum(3);
    return x *2;
}
inline int tum( int x )
{
  if (x < 10) {
    small_function(3);
    x = x < 5 ? x +3 : x+ 13;
    return x* x;
  }
  else  {
    x = x > 9 ? x -5 : x- 10;
    return x+2;
  }
}

inline int lum( int x )
{
  if (x < 10) {
    tum(16);
    x = x < 5 ? x +3 : x+ 13;
    return x* x;
  }
  else  {
    tum(7);
    x = x > 9 ? x -5 : x- 10;
    return x+2;
  }
}

inline int foo( int x )
{
  int y = 0;

  y = x-2;
  if (x < 30) {
    y = x+5;
    x = x > 7 ? x -3 : x- 13;
    return x* 6;
  }
  else  {
    x = x <= 10 ? x +21 : x+ 16;
    return x+18;
  }
}

// non inlined function
int bar(int x)
{
    return x%2;
}

int complex_function2()
{
    int i = 0;

    while (i< 10)
    {
        i++;
        lum (7);
    }
    return 0;
}

int complex_function()
{
    int c = 0;

    while (c< 10)
    {
        c++;
    }

    complex_function2();
    return 0;
}
int main()
{
  int a;
  a = lum(13);
  a = foo(tum(lum(10)));

  a = lum(11);
  a = bar(30);
  a = foo( 30 );
  a += bar(40);
  a = foo( 10 );
  a = foo( 30 );
  a = foo( 30 );
  a = bar(40);

  complex_function();
  complex_function();
}
