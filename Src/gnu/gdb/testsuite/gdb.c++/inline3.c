#include "inline3.h"

/* b main
   run

   while (!done)
   {
	step,
	bt,
        up
        info locals
        info args
        down
        info locals
        info args
   }
*/
/* b main
   run

   while (!done)
   {
	next,
	bt,
        up
        info locals
        info args
        down
        info locals
        info args
   }
*/

/* b main
   run

   while (!done)
   {
        if (odd)
	   step
        else
	   next
	bt,
        up
        info locals
        info args
        down
        info locals
        info args
   }
*/
int bar(int a)
{
        int b = 20;
        b = baz ("Hello", b + 123);
        return b;
}

int main ()
{
        int i = 10;
        int j = 0;

        i = bar(123);
        j = baz("World", 321);

        return i;
}
