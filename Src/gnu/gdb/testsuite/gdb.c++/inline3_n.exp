# Test for 'next'

if $tracelevel then {
    strace $tracelevel
}

if {![istarget "hppa*-*-*"] && ![istarget "ia64*-hp-*"] } {
  return 
}

if { [skip_cplus_tests] } { continue }

set oldtimeout $timeout

set testfile "inline3"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} "c++"] {
    return -1;
}
source $binfile.ci

# aCC5 doesn't implement the necessary support for inlined function debugging.
if { $hp_aCC5_compiler } {
  return
}

if {$multipass_name > 16} {
  return
}

if { [istarget "ia64*-hp-*"] } then {
  set comp_flags ""
} else {
  # PA requires an additional option to debug inline functions.
  set comp_flags "+inline_debug"
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable \
	[list debug c++ additional_flags=$comp_flags]] != "" } {
     if [istarget "hppa*-*-hpux*"] then {
        gdb_suppress_entire_file "PA Testcase compile failed - compiler doesn't understand +inline_debug?"
        return
     } else {
        gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
     }
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_test "set inline-debug on" "" ""
gdb_load $binfile

gdb_test "b main" "Breakpoint 1.*${srcfile}.*line.*61.*" "break at main"
gdb_test "run" "Breakpoint 1.*main.*61.*i.*10.*" "run to main"

# next in main
gdb_test "next" "62.*j.*0.*" "next"
gdb_test "bt" ".*#0.*main.*62.*" "backtrace"
gdb_test "up" ".*Initial.*frame.*selected.*you.*cannot.*go.*up.*" "up"
gdb_test "info locals" ".*i.*10.*j.*" "up info locals"
gdb_test "info args" ".*No.*arguments.*" "up info args"
gdb_test "down" ".*Bottom.*innermost.*frame.*selected.*you.*cannot.*go.*down.*" "down"
gdb_test "info locals" ".*i.*10.*j.*" "down info locals"
gdb_test "info args" ".*No.*arguments.*" "down info args"

# next in main
gdb_test "next" "64.*i.*bar.*123.*" "next 1 - main"
gdb_test "bt" ".*#0.*main.*64.*" "backtrace 1 - main"
gdb_test "up" ".*Initial.*frame.*selected.*you.*cannot.*go.*up.*" "up 1 - main"
gdb_test "info locals" ".*i.*10.*j.*0.*" "up info locals 1 - main"
gdb_test "info args" ".*No.*arguments.*" "up info args 1 - main"
gdb_test "down" ".*Bottom.*innermost.*frame.*selected.*you.*cannot.*go.*down.*" "down 1 - main"
gdb_test "info locals" ".*i.*10.*j.*0.*" "down info locals 1 - main"
gdb_test "info args" ".*No.*arguments.*" "down info args 1 - main"

# next over bar

gdb_test "next" "65.*j = baz.*World.*321.*" "next over bar"
gdb_test "bt" ".*#0.*main.*65.*" "backtrace 2"
gdb_test "up" ".*Initial.*frame.*selected.*you.*cannot.*go.*up.*" "up 2"
gdb_test "info locals" ".*i.*43.*j.*0.*" "up info locals 2"
gdb_test "info args" ".*No.*arguments.*" "up info args 2"
gdb_test "down" ".*Bottom.*innermost.*frame.*selected.*you.*cannot.*go.*down.*" "down 2"
gdb_test "info locals" ".*i.*43.*j.*0.*" "down info locals 2"
gdb_test "info args" ".*No arguments.*" "down info args 2"

# next in main
gdb_test "next" "67.*return i.*" "next over baz"
gdb_test "bt" ".*#0.*main.*67.*" "backtrace 3"
gdb_test "up" ".*Initial.*frame.*selected.*you.*cannot.*go.*up.*" "up 3"
gdb_test "info locals" ".*i.*43.*j.*43.*" "up info locals 3"
gdb_test "info args" ".*No.*arguments.*" "up info args 3"
gdb_test "down" ".*Bottom.*innermost.*frame.*selected.*you.*cannot.*go.*down.*" "down 3"
gdb_test "info locals" ".*i.*43.*j.*43.*" "down info locals 3"
gdb_test "info args" ".*No.*arguments.*" "down info args 3"

# next in main (End)
if ![istarget "ia64*-hp-*"] then {
  gdb_test "next" "68.*" "next in main"
  gdb_test "bt" ".*#0.*main.*68.*" "backtrace - main"
  gdb_test "up" ".*Initial.*frame.*selected.*you.*cannot.*go.*up.*" "up - main"
  gdb_test "info locals" ".*i.*43.*j.*43.*" "up info locals - main"
  gdb_test "info args" ".*No.*arguments.*" "up info args - main"
  gdb_test "down" ".*Bottom.*innermost.*frame.*selected.*you.*cannot.*go.*down.*" "down - main"
  gdb_test "info locals" ".*i.*43.*j.*43.*" "down info locals - main"
  gdb_test "info args" ".*No.*arguments.*" "down info args - main"
}

gdb_exit
