#include "inline4_two.h"

inline int baz (char * strm, int xyz)
{
    int a = xyz;
    int b = a+10;
    int deadend = 13; char *z = strm;

    foobar();
    if (b > 10)
    {
        int deadend = 23;
        deadend += 20;
        return deadend;
    }
    else
    {
        int deadend = 45;
        deadend -= 20;
        return deadend;
    }

   return b;
}

