inline int min(int x_min,int y_min) { return (x_min < y_min ? x_min : y_min); }
inline int max(int x_max,int y_max) { return (x_max > y_max ? x_max : y_max); }

inline int foo(int x_foo, int y_foo) {
  return x_foo + y_foo;
}

int main() {
  int x = 10;
  int j = foo(min(10,20), max(10,20));
} 
