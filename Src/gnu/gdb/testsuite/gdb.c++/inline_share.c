#include<string.h>
#if defined(__ia64) && !defined (FLAVOR_GAMBIT)
#include <iostream>
using namespace std;
#else
#include <iostream.h>
#endif
#include"inline_share.h"

int n();
int jum (int, int);

int foo (int foo_a)
{
  return inline_call_1 (foo_a ++);
}

int bar (int bar_a)
{
  return inline_call_2 (bar_a ++);
}


int baz (int baz_a)
{
  return inline_call_3 (baz_a ++);
}

int jum (int ab, int cd)
{
  ab++;
  cd << ab;
  cout << ab;
  return cd;
}

int n()
{
  int i,j;
/* See if we can step into and step next over function with inlined funcions as arguments.
 */
  i = jum(inline_call_3(2),inline_call_3(3));
  j = jum(5,6);
  j = inline_call_4 (j);
  return i+j;
}
