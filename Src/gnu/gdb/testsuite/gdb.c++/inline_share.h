#include <stdio.h>
inline int inline_call_1 ( int abc );
inline int inline_call_2 ( int abcd );
inline int inline_call_3 ( int abcd );

int foo (int);
int bar (int);
int baz (int);

inline int inline_call_1 (int abc)
{
   abc++;
   abc = bar (abc);
   return abc;
}

inline int inline_call_2 (int abcd)
{
   abcd++;
   abcd = baz (abcd);
   abcd = inline_call_3 (abcd);
   return abcd;
}

inline int inline_call_3 (int abcd)
{
   printf ("abcd = %d\n", abcd);
   abcd++;
   return abcd;
}

inline int inline_call_4 (int abcd)
{
   abcd = inline_call_3 (abcd);
   return abcd;
}
