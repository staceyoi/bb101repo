#include<string.h>

#if defined(__ia64) && !defined (FLAVOR_GAMBIT)
#include <iostream>
using namespace std;
#else
#include <iostream.h>
#endif

int foo (int);
int n();

/* See if our bt is correct across a mix of inlined and non-inlined functions.
 */

int main()
{
  int k = 383;
  k = foo(k);
  k = n();
  return k;
}
