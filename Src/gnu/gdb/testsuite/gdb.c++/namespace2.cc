namespace NS1 { class Set; }
using NS1::Set;


namespace NS1 {

class Set {

public:
  int iij;
  int iik;
  void init ( bool b, int sss, int iul)
    {
      iij = 101;
      iik = 102;
      isPoolSet = b;
      theSmallSetSize = sss;
      inUseLock = iul;
    };

private:
   // private data fields
   bool      isPoolSet;            
   int		theSmallSetSize;
   int	inUseLock;	

};

}; // namespace NS1

class SRA {
public:
    void init (Set gs, Set ls, Set ps)
      {
        gSet = gs;
        lSet = ls;
        pSet = ps;
      };
  private:
    Set gSet;
    Set lSet;
    Set pSet;
};
int main ()
{
  Set a, b, c;
  SRA sra;

  a.init (false, 2, 10);
  b.init (true, 3, 11);
  c.init (false, 4, 12);
  sra.init (a, b, c);
  return 0;
}
