namespace A {
  int i = 2;
}

namespace 
{
  float a = 3.1459; 
  int foo () 
    {
      return 4;
    }
  namespace 
    {
      int l = 2;
      int bar() { return l; }
    };
  namespace B 
    {
      int k = 6;
      int baz () { return k; }
      namespace 
	{
	  int j = 4;
	  int bar2 () { return j; }
	};
    };
};

#if 0
int foo()
{
   return 5;
}
#endif

using namespace B;

int main(void)
{
    using A::i;
    foo();
    bar();
    baz();
    bar2();

    return 5 == static_cast<int>(i + a);
}


#if 0
For testing

b main
r

p i
p A::i
p a
p ::a
p l
p k
p j


b foo()
run to it
p a

b bar()
run to it
p l = 2

b baz() run to it
p k = 6
set k = 8
p k


b bar2() run to it
p j = 4
p k (should be 8 and not 6)

#endif
