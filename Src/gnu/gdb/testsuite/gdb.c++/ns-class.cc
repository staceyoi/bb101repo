#include <stdio.h>
/* Testing classes within a namespace, ptype namespace, setting 
namespace off, aliases in function scope and namespace scope*/


namespace A {
    int i = 2;
}

namespace B{
    float a = 3.1459; 
    using namespace A;
    class Foo {
       public:
	static int k;
        int bar();
	int baz (int);
       	int baz (double);
    };
}

namespace A {
    int j = 4;
    namespace Balias = B;
    using namespace Balias;
}


int j;

using namespace B;

int Foo::k = 4;

int Foo::bar ()
{
   return 2;
}

int Foo::baz (int)
{
   return 4;
}

int Foo::baz (double)
{
   return 4;
}


int main(void)
{
    Foo foo1;
    namespace Balias2 = B;
    int l = foo1.bar();
    foo1.baz((int) 2);
    foo1.baz((double) 2);
    printf ("Foo::k %d j = %d", Foo::k, ::j);
    return 5 == static_cast<int>(i + a);
}


#if 0
Testing

after b main
run

b Foo::bar run to the breakpoint and print k

p Foo::k
p Balias::a;
p Balias2::a;
p Balias::i
p Balias2::i



ptype Foo - will not work currently. 

ptype B
ptype A

print j
Menu - 2 entries
j
A::j

set namespaces-enabled off 

b Foo::bar should not work now.
p j should print only 1 global j

#endif

