// DWARF2 test case

namespace {
    int i;
}

namespace A {
    namespace B {
        int h = 7;
        int   j;
        int   myfunc(int a);
        float myfunc(float f)  { return f - 2.0; }
        int   myfunc2(int a)   {return a + 2; }
    }
}

namespace Y {
    // 1. using declaration
    using A::B::j;
    int foo = 11;
}

// 2. using declaration
using A::B::j;

// 3. namespace alias
namespace Foo = A::B;

// 4. using declaration
using Foo::myfunc;

// 5. using directive
using namespace Foo;

namespace A {
    namespace B {
        // 6. using directive
        using namespace Y;
        int k = 8;
    }
}

int Foo::myfunc(int a)
{
    i = 3;
    j = 4;

    return myfunc2(3) + j + i + a + 2;
}

int k = 10;
int h = 9;

int main ()
{
    // the following should be true after the next line:
    // a. j == A::B::j == 4
    // b. i = <unamed-namespace>g::i == 3
    //  print A::B::j = 4
    // print Y::j = 4
    // c. myfunc(7) == A::B::myfunc(7) == 21
    // d. foo == Y::foo == Foo::Foo == 11
    // print Foo::foo = 11
    // print A::B::foo = 11
   // print Y::foo = 11
   // print h + k - should ask for h and then also for k

    A::B::k = myfunc(7) + j + i + foo;
    myfunc ((float) 3.0);
    A::B::h = 6;
    return A::B::k == 39;
}
