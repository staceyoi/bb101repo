namespace A {
    int k = 8;
    namespace B {
        int j = 9;
        double myfunc(double d);
        int myfunc(int a) {
            return j;
        }
        float myfunc(float f) {
            return f - 2.0;
        }
        int myfunc2(int a) {
            return a + 2;
        }
    }
}

namespace {
    int i = 10;
    bool myfunc(bool b) {
        return false;
    }
}

namespace Y {
    using A::B::j;         /* using declaration inside a namespace */
    int foo;
    void myfunc(void) { int i = 2; }
}

namespace C {
    float myfunc(char a) {
        return 11.3;
    }
}

namespace D {
  int myfunc (int b) {
    return 2;
  }
}

using A::B::j;
namespace Foo = A::B;   /* alias in file scope */
using Foo::myfunc;      /* using declaration for overloaded functions */
using namespace Foo; 
using namespace C;      /* Multiple using directives in file scope*/

void bar() {
    bool b = myfunc(true);
}

namespace A {
    namespace B {
        using namespace Y;    /* using directive in a namespace */
        int k;
        char myfunc(char c) {
            return c;
        }
    }
}

double Foo::myfunc(double d) {
    return myfunc2(3) + d + 2.0;
}

int
main() 
{
   int k = 6;                             

   Foo::myfunc ((int) 3);
   Foo::myfunc ((char) 'c');
   myfunc(true);
   Foo::myfunc ((float) 4.0);
   Y::myfunc ();
   D::myfunc (2);
   C::myfunc ('a');
   Foo::myfunc2 (3);
   A::k = 8;
  
   {
     //int k = 7;
     using A::k;           /* using declaration with a lexical block local */
     // using namespace D;
   }
   Y::foo = 8;
   return ((int)myfunc((double)3.1459)) == 10;
}

#if 0
Testing 

b main 
b myfunc should find the anonymous namespace one (bool)
         since we are not yet in an expression context block.


run  - it will break at main.

b myfunc  Give you a menu 
Should find all 
4 A::B::myfunc 
Y::myfunc
anon_ns::myfunc
C::myfunc

Total 7 functions - shouldn't find D::myfunc

b ::myfunc - same as b myfunc

b myfunc (int)
Should find A::B::myfunc  - shouldn't find D::myfunc - set breakpoint on it

b myfunc (bool)
should find anonymous ns myfunc

b myfunc (char)
Menu  with 2 entries
A::B::myfunc(char)
C::myfunc(char)
give a 0

b Foo::myfunc
Menu 5 entries
4 - A::B::myfunc
1 Y::myfunc


b myfunc2 - sets breakpoint on it

b A::B::myfunc2 - sets breakpoitn on it.

print A::B::foo should work.
print k - should print k = 6 the local
print A::k = 8 
print ::i = 10 (the anon ns variable)

print  j = A::B::j due to using Foo 
print Foo::j
print Y::j


in lexical block after 7 nexts 
print k - should print k = 7 the lexical block local
print A::k = 8
print ::i = 10 (the anon ns variable)

b myfunc (int)
Menu with 2 entries
A::B::myfunc (int)
D::myfunc (int)
because we see a using in this lexical block for namespace D

When stopped in A::B::myfunc(int)
print B::j
It should actually look for A::B::j - testing for semi qualified lookup



#endif

