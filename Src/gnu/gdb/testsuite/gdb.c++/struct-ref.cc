struct bar {
   int i;
   const char *p;
};

typedef int int_array [5];

class Base
{
public:
  int a;
  Base (int val) : a (val) {}
  virtual void foo(void) {}
};

class Derived : public Base
{
public:
  int b;
  Derived (int val1, int val2) : b (val2), Base (val1) {}
  virtual void foo(void) {}
};

void foo1(bar &p1, bar &p2, bar *p3, bar *p4) {
    p1.i += 2;
}

void foo2(int_array &p1, int_array &p2, int_array *p3, int_array *p4) {
    p1[1] += 2;
}

bar B = { 2, "B" };
bar &rB = B;
bar *pB = &B;
int_array A = { 1, 2, 3, 4, 5 };
int_array &rA = A;
int_array *pA = &A;
Derived D (1, 2);
Base &B1 = D;
Base *B2 = &D;

int main (void)
{
    foo1(B,rB,&B,pB);
    foo2(A,rA,&A,pA);
    return 0;
}
