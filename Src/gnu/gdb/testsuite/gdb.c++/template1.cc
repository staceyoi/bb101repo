#include <stdio.h>

class B {};
class A {};

template < class TYPE > struct S {
   void test();
};

template <class TYPE>
void S<TYPE>::test()
{
    printf ("test\n");
}

int foo;

class Bar {
   int bar;
};

int main()
{
 S<B> sb;
 S<A> sa;
 sa.test();
 sb.test();
}
