#if defined(__ia64) && !defined (FLAVOR_GAMBIT)
#include <iostream>
using namespace std;
#else
#include <iostream.h>
#endif


class C 
{
 public:
  C ();
  ~C ();
  
  int foo (int);
  int foo (const short);

  int foo (int) const;
  int foo (const short) const;
};

C::C () {}
C::~C () {}

int C::foo (int i) { return (1); }
int C::foo (const short i) { return (2);} 

int C::foo (int i) const { return (3); }
int C::foo (const short i) const { return (4); }
  
int main (void)
{
  C c;
  const C const_c;
  int i = 0;
  const int const_i = 0;
  short s = 0;
  const short const_s = 0;
  
  // should use 'int C::foo (int)', returns 1
  //
  cout << c.foo (i) << endl;
  cout << c.foo (const_i) << endl;

  // should use 'int C::foo (const short)', returns 2
  //
  cout << c.foo (s) << endl;
  cout << c.foo (const_s) << endl;

  // should use 'int C::foo (int) const', returns 3
  //
  cout << const_c.foo (i) << endl;
  cout << const_c.foo (const_i) << endl;

  // should use 'int C::foo (const short) const', returns 4
  //
  cout << const_c.foo (s) << endl;
  cout << const_c.foo (const_s) << endl;

  return (0);
}
