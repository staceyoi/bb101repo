# pa11.exp   Test gdb disassembly operations
#
if ![istarget "hppa*-*-*"] {
    verbose "Tests ignored for all but hppa based targets."
    return
}

if $tracelevel {
    strace $tracelevel
}

set prms_id 0
set bug_id 0

# use this to debug:
#log_user 1

set testfile pa11
set srcfile ${srcdir}/${subdir}/pa11-instr.s
set binfile ${srcdir}/${subdir}/${testfile}
set outfile ${srcdir}/${subdir}/${testfile}.out
set comfile ${srcdir}/${subdir}/${testfile}.com
set tmpfile ${objdir}/${subdir}/${testfile}.tmp
set sedfile ${srcdir}/${subdir}/pa-sed.cmd
set diffile ${objdir}/${subdir}/${testfile}.dif
set tmp2file ${objdir}/${subdir}/${testfile}.tmp2
set out2file ${srcdir}/${subdir}/${testfile}.out.with_bug

# Clean up for test
#
gdb_exit
remote_exec build "rm -f ${tmpfile} ${tmp2file} ${diffile}"

# To build a pa 1.1 executable
#
#     as -o pa11 pa11-instr.s
# or 
#     cc -g -o pa11 pa11-instr.s
#
##if { [gdb_compile "${srcfile}" "${binfile}" executable {debug additional_flags="+DA1.1"}] != "" } {
##    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
##}

# This non-standard start-up sequence is taken from that for
# the standard "gdb_start" in 
# /CLO/Components/WDB/Src/gdb/gdb/testsuite/lib/gdb.exp.
# We use it so we can run gdb in non-interactive mode.
#
global GDB
if { [which $GDB] == 0 } {
    perror "$GDB does not exist."
    exit 1
}

remote_exec build "${srcdir}/${subdir}/redirect_cmd ${tmpfile} $GDB -nx -batch -silent -se ${binfile} -command ${comfile}"

# Remove actual addresses, which may vary from one OS release to another.
#
remote_exec build "${srcdir}/${subdir}/redirect_cmd ${tmp2file} sed -f ${sedfile} ${tmpfile}"

# Should be no differences in the output.
#
remote_exec build "${srcdir}/${subdir}/redirect_cmd ${diffile} diff ${outfile} ${tmp2file}"

if [ file size ${diffile} ] {
    fail "Disassembly"
} else {
    pass "Disassembly"
}

return 0
