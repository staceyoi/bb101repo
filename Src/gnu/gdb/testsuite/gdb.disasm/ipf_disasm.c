/* parse_include.c	Utility to parse -E output of compiler and to
	display the include file structure on stdout, e.g:
     0	     0	     1	myfile.c
     1      29      18    /usr/include/stdio.h
     2      67	    30      /usr/include/sys/include.h
     1      40	    60    /usr/include/unistd.h
     ^       ^       ^  ^
     |       |       |  |
     |       |       |  +- source file name
     |       |       +---- line number in -E input file
     |       +---- line number in including file of #include statement
     +------------ include depth  0 for top level file, etc.

    The include level is printed right justified in a 6 character field 
    followed by a tab the including line number in a 6 character field
    followed by a tab followed by the file name.  The file name is indented
    two spaces for each level of indentation.

   USAGE: parse_include input_file_e_output

   Author: Michael Coulter
*/

/* Includes */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

/* Declarations */

#define MAX_INDCLUDE_DEPTH	100

typedef struct {
  int		current_depth;
  char *	file_list[MAX_INDCLUDE_DEPTH];
  int		include_line;	/* line number in including file of #include */
  FILE*		input_file;
  int		line_number;	/* current line number in the -E input file */
} parse_info_t;

/* Forward declarations */

extern void	initialize_parse (parse_info_t* parse_info_p);
extern char *	parse_args (int argc, char** argv);
extern void	parse_file (const char* 	input_file_path, 
			    parse_info_t* 	parse_info_p);
extern char *	parse_next_set_file (parse_info_t* parse_info_p, 
				     int * new_line);
extern void	print_parse_info (parse_info_t* parse_info_p);

/* main - see file header */

int 
main (int argc, char** argv)
{
  char *	input_file_path;
  parse_info_t	parse_info;

  input_file_path = parse_args (argc, argv);
  initialize_parse (&parse_info);
  parse_file (input_file_path, &parse_info);
  free (input_file_path);
  return 0;
} /* end main */


/* initialize_parse - initialize a part_info_t structure */

void	
initialize_parse (parse_info_t* parse_info_p)
{ 
  parse_info_p->current_depth = -1;
  parse_info_p->line_number = -1;

} /* end initialize_parse */

/* parse_args - see file header;  If successful, return a pointer to 
   a malloced string containing the input file path.  If error, print 
   a message to stderr and call exit()
*/

char *	
parse_args (int argc, char** argv)
{
  if (argc != 2)
  {
    fprintf (stderr, "You must provide 1 argument, not %d\n", argc);
    exit (1);
  }
  return strdup (argv[1]);
} /* end parse_args */
  
/* parse_file - see file header.  Parse the input file, print the include
	analysis as specified in the file header.
*/

extern void	
parse_file (const char* input_file_path, parse_info_t* parse_info_p)
{
  FILE*		input_file;
  char*		new_file;
  int		new_line;

  input_file = fopen (input_file_path, "r");
  if (!input_file)
    {
      fprintf (stderr, "Unable to open '%s'\n", input_file_path);
      exit (1);
    }
  parse_info_p->input_file = input_file;
  parse_info_p->line_number = 1;  /* line 1 is stuff up to first newline */
  
  while (new_file = parse_next_set_file (parse_info_p, &new_line))
    {
      if (new_line == 1)
	{ /* New include file */
	  if (parse_info_p->current_depth >= MAX_INDCLUDE_DEPTH)
	    {
	      fprintf (stderr, "Include depth of %d exceeded.  Abort.\n",
		       MAX_INDCLUDE_DEPTH);
	      exit (1);
	    }
	  parse_info_p->current_depth++;
	  parse_info_p->file_list[parse_info_p->current_depth] = new_file;
	  print_parse_info (parse_info_p);
	} /* end New include file */
      else if (parse_info_p->current_depth <= 0)
	{ /* Must be continuting the top file */
	  if (parse_info_p->current_depth != 0
	      || strcmp (new_file, parse_info_p->file_list[parse_info_p->current_depth]) != 0)
	    {
	      fprintf (stderr, "Parsing error at line %d\n", 
		       parse_info_p->line_number);
	    }
	} 
      else
	{ /* new_file is either continuing the last file or ending it */
	  if (   strcmp (new_file, 
			 parse_info_p->file_list[parse_info_p->current_depth])
	      != 0)
	    { /* Must be ending the last include */
	      free (parse_info_p->file_list[parse_info_p->current_depth]);
	      parse_info_p->current_depth--;
	      if (   strcmp (new_file, 
			     parse_info_p->file_list[
				 parse_info_p->current_depth])
		  != 0)
		fprintf (stderr, "Parsing error at line %d\n", 
			 parse_info_p->line_number);
	    } /* end Must be ending the last include */
	} /* end new_file is either continuing the last file or ending it */

    } /* while more includes to process */

  if (parse_info_p->current_depth != 0)
    {
      fprintf (stderr, "Warning, end of output not at top include level.\n");
      exit (1);
    }
} /* end parse_file */

/* parse_next_set_file - continue reading the input file until the next line 
   that sets the line number.  Set *new_line_p to the line number and return
   a malloced string of the file path.  If EOF, return NULL.

   On entry file position should either be at the start of the file or at a
   newline character.  The file position on return will either be a newline
   character or EOF.

   On entry, the *new_line value is the last line number that was set
   from parsing the input.  On exit, new_line is the line number of the
   of the next included file (i.e. 1) or undefined if EOF is encountered.

   A line which sets a line number will be in one of three forms:

     # 52
     # 1 "../../../Src/gnu/bfd/../include/ansidecl.h"
     # 68 "../../../Src/gnu/bfd/elfcode.h"
   
   The first just sets a line number e.g. the pre-processor removed some 
   lines and the directive gets the line number synced up with the compiler.
   The second form is the first line after a #include directive.
   The third form is emitted when we are done expanding a #include file
   to set the file and line number back to the including file. The '#' is
   in column 1.
   */

char *	
parse_next_set_file (parse_info_t* parse_info_p, int * new_line)
{
  int		next_char;
  int		last_line_nbr;
  int		line_number = *new_line;
  const int	INIT_MAX_NAME = 200;
  static int	cur_max_name = 0;
  int		cur_name_len;
  static char*	temp_name_buf = NULL;

#define GO_TO_LINE_END \
	{ \
	  if (next_char == '\n') \
	    { \
	      parse_info_p->line_number++; \
	      continue; \
	    } \
	  while (   (next_char = getc (parse_info_p->input_file)) != EOF \
		 && next_char != '\n') \
	    ; \
	  if (next_char == EOF) \
	    return NULL; \
	  parse_info_p->line_number++; \
	  continue; \
	}

  while (1)
    {
      if ((next_char = getc (parse_info_p->input_file)) == EOF)
	return NULL;
      
      if (next_char != '#')
	  GO_TO_LINE_END; /* Scan to newline and then continue */
      
      /* The next characters should be blank, line number blank quote string
	 close-quote newline */
      
      next_char = getc (parse_info_p->input_file);
      if (next_char != ' ')
	GO_TO_LINE_END;
      last_line_nbr = line_number;
      line_number = 0;
      while (   (next_char = getc (parse_info_p->input_file)) >= '0' 
	     && next_char <= '9')
	line_number = line_number * 10 + next_char - '0';
      if (next_char != ' ')
	GO_TO_LINE_END;
      next_char = getc (parse_info_p->input_file);
      if (next_char != '"')
	GO_TO_LINE_END;
      cur_name_len = 0;
      while (   (next_char = getc (parse_info_p->input_file)) != EOF
	     && next_char != '"')
	{
	  if (cur_max_name <= cur_name_len)
	    {
	      cur_max_name = cur_max_name * 2 + INIT_MAX_NAME;
	      temp_name_buf = realloc (temp_name_buf, (size_t) cur_max_name);
	    }
	  temp_name_buf[cur_name_len++] = next_char;
	}
      if (next_char != '"')
	GO_TO_LINE_END;
      temp_name_buf[cur_name_len] = 0;
      *new_line = line_number;
      parse_info_p->include_line = last_line_nbr;
      return strdup (temp_name_buf);
    } /* end while (1) */
} /* end parse_next_set_file */

/* print_parse_info - print the information for one include file.
   See the file header.
*/

void	
print_parse_info (parse_info_t* parse_info_p)
{
  int i;

  fprintf (stdout, "%6d	%6d	%6d	", 
	   parse_info_p->current_depth, 
	   parse_info_p->include_line,
	   parse_info_p->line_number);  /* first line has -1, etc. */

  /* Indent two spaces for every include level */
  for (i = 0;  i < parse_info_p->current_depth; i++)
    fprintf (stdout, "  ");

  /* print the file name */

  fprintf (stdout, "%s\n", 
	   parse_info_p->file_list[parse_info_p->current_depth]);
} /* end print_parse_info */
