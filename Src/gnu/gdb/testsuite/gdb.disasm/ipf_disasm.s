	.file "parse_include.c"
	.type	main,@function
	.global parse_args
	.type	parse_args,@function
	.global initialize_parse
	.type	initialize_parse,@function
	.global parse_file
	.type	parse_file,@function
	.global free
	.type	free,@function

// Routine 113 ("main"::101)

	.radix	C

	.psr	abi32

	.psr	msb

	.section .text = "ax", "progbits"
	.proc	main
..L0:
//	$start		CMid904 = 		;; // A

..L2:
main::
//file/line/col parse_include.c/58/1
//	$entry		CMid914, r32, r33 = 	   // A [parse_include.c: 58/1]
	alloc		r37 = ar.pfs, 2, 8, 2, 0   // M [parse_include.c: 58/1] [UVU: ]
	add		r11 = 0, sp		   // M [parse_include.c: 58/1] [UVU: ]
	mov		r38 = rp		   // I [parse_include.c: 58/1]
	add		r41 = 0, gp		   // M
	add		r9 = 0, r33		   // M [parse_include.c: 58/1]
	add		r10 = 0, r32		;; // I [parse_include.c: 58/1]
	add		sp = -432, sp		   // M [parse_include.c: 58/1] [UVU: ]
	add		r39 = -416, r11		   // M [parse_include.c: 58/1]
//	$fence					   // A [parse_include.c: 58/1] [UVU: ]
	add		r34 = 0, r10		   // I [parse_include.c: 58/1] [UVU: argc]
	add		r35 = 0, r9		;; // M [parse_include.c: 58/1] [UVU: argv]
	add		r40 = -48, sp		   // M [parse_include.c: 58/1]
//file/line/col parse_include.c/58/1/E,62/3
	add		out0 = 0, r34		   // I [parse_include.c: 62/3] [UVuse]
	add		out1 = 0, r35		   // M [parse_include.c: 62/3] [UVuse]
	nop.m		0			   // M
	br.call.dptk.few rp = parse_args	;; // B [parse_include.c: 62/3] [UVU] [UVuse]
	add		gp = 0, r41		   // M [parse_include.c: 62/3]
	add		r36 = 0, r8		   // M [parse_include.c: 62/3] [UVU: input_file_path]
//file/line/col parse_include.c/63/1
	add		out0 = 0, r39		   // I [parse_include.c: 63/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = initialize_parse	;; // B [parse_include.c: 63/1] [UVU]
	add		gp = 0, r41		   // M [parse_include.c: 63/1]
//file/line/col parse_include.c/64/1
	add		out0 = 0, r36		   // M [parse_include.c: 64/1] [UVuse]
	add		out1 = 0, r39		   // I [parse_include.c: 64/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = parse_file	;; // B [parse_include.c: 64/1] [UVU]
	add		gp = 0, r41		   // M [parse_include.c: 64/1]
//file/line/col parse_include.c/65/1
	add		out0 = 0, r36		   // I [parse_include.c: 65/1] [UVuse]
	add		r9 = @pltoff(free), r0	;; // I [parse_include.c: 65/1]
	add		r10 = r9, gp		   // M [parse_include.c: 65/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 65/1]
	nop.i		0			   // I
	ld8		r9 = [r10]		   // M [parse_include.c: 65/1]
	add		r10 = 8, r10		;; // I [parse_include.c: 65/1]
	mov		b6 = r9			   // I [parse_include.c: 65/1]
	ld8		gp = [r10]		   // M [parse_include.c: 65/1]
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 65/1] [UVU]
	add		gp = 0, r41		   // M [parse_include.c: 65/1]
//file/line/col parse_include.c/66/1
	add		r8 = 0, r0		   // M [parse_include.c: 66/1]
	mov		rp = r38		   // I [parse_include.c: 66/1]
	add		sp = 432, sp		;; // M [parse_include.c: 66/1] [UVU: ]
	nop.m		0			   // M
	mov		ar.pfs = r37		   // I [parse_include.c: 66/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 66/1]

..L1:
//	$end					;; // A

	.endp	main

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 4
	.radix C
	data4.ua @segrel(.text)
	data4.ua @segrel(.text+256)
	data4.ua @segrel(.tcg$unwind_info_block0000)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 4
	data4.ua @segrel(.llo$annot_info_block0000)
.tcg$unwind_info_block0000:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x05
	data1	0x08, 0xe4, 0x02, 0xb0, 0xa6, 0xe6, 0x00, 0xb1
	data1	0x25, 0xe0, 0x06, 0x1b, 0xe2, 0x04, 0x61, 0x28
	data1	0x81, 0xc0, 0x05, 0x00

// ===


// Routine 154 ("initialize_parse"::101)

	.section .text = "ax", "progbits"
	.proc	initialize_parse
..L0:
//	$start		CMid904 = 		;; // A

..L2:
initialize_parse::
//file/line/col parse_include.c/74/1
//	$entry		CMid906, r32 = 		   // A [parse_include.c: 74/1]
	alloc		r31 = ar.pfs, 1, 1, 0, 0   // M [parse_include.c: 74/1] [UVU: ]
//	$fence					   // A [parse_include.c: 74/1] [UVU: ]
	add		r8 = 0, r32		   // I [parse_include.c: 74/1]
	add		r9 = -48, sp		;; // I [parse_include.c: 74/1]
	add		r33 = 0, r8		   // M [parse_include.c: 74/1] [UVU: parse_info_p]
//file/line/col parse_include.c/74/1/E,75/5
	add		r9 = -1, r0		;; // I [parse_include.c: 75/5]
	addp4		r8 = 0, r33		;; // I [parse_include.c: 75/5] [UVuse]
	st4		[r8] = r9		   // M [parse_include.c: 75/5] [UVU]
//file/line/col parse_include.c/76/5
	addp4		r8 = 0, r33		   // I [parse_include.c: 76/5] [UVuse]
	add		r9 = -1, r0		;; // I [parse_include.c: 76/5]
	add		r8 = 412, r8		;; // M [parse_include.c: 76/5]
	st4		[r8] = r9		   // M [parse_include.c: 76/5] [UVU]
//file/line/col parse_include.c/78/1
	nop.i		0			   // I
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 78/1]

..L1:
//	$end					;; // A

	.endp	initialize_parse

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 4
	data4.ua @segrel(.text)
	data4.ua @segrel(.text+80)
	data4.ua @segrel(.tcg$unwind_info_block0001)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 4
	data4.ua @segrel(.llo$annot_info_block0001)
.tcg$unwind_info_block0001:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x02
	data1	0x01, 0x2e, 0x81, 0xc0, 0x0e, 0x00, 0x00, 0x00

// ===

	.global fprintf
	.type	fprintf,@function
	.global __iob
	.global exit
	.type	exit,@function
	.global strdup
	.type	strdup,@function

// Routine 144 ("parse_args"::101)

	.section .text = "ax", "progbits"
	.proc	parse_args
..L0:
//	$start		CMid904 = 		;; // A

..L2:
parse_args::
//file/line/col parse_include.c/87/1
//	$entry		CMid913, r32, r33 = 	   // A [parse_include.c: 87/1]
	alloc		r36 = ar.pfs, 2, 7, 3, 0   // M [parse_include.c: 87/1] [UVU: ]
	add		r40 = 0, gp		   // M
	mov		r37 = rp		   // I [parse_include.c: 87/1]
//	$fence					   // A [parse_include.c: 87/1] [UVU: ]
	add		r8 = 0, r33		   // M [parse_include.c: 87/1]
	add		r9 = 0, r32		   // M [parse_include.c: 87/1]
	add		r39 = -48, sp		;; // I [parse_include.c: 87/1]
	add		r34 = 0, r9		   // M [parse_include.c: 87/1] [UVU: argc]
	add		r35 = 0, r8		;; // I [parse_include.c: 87/1] [UVU: argv]
//file/line/col parse_include.c/87/1/E,88/1
	cmp4.eq		p6, p0 = 2, r34		   // I [parse_include.c: 88/1] [UVuse]
	nop.m		0			   // M
	nop.m		0			   // M
(p6)	br.dptk.few	..L3			;; // B [parse_include.c: 88/1]

..L4:
//file/line/col parse_include.c/90/1
	add		r9 = @ltoff(__iob), r0	   // M [parse_include.c: 90/1]
	add		out2 = 0, r34		;; // I [parse_include.c: 90/1] [UVuse]
	add		r9 = r9, gp		   // I [parse_include.c: 90/1]
	add		r10 = @ltoff(.tcg0$rodata), r0 ;; // M [parse_include.c: 90/1]
	add		r11 = r10, gp		   // M [parse_include.c: 90/1]
	nop.i		0			   // I
	ld8		r10 = [r9]		   // M [parse_include.c: 90/1]
	add		r9 = @pltoff(fprintf), r0  // M [parse_include.c: 90/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 90/1]
	ld8		out1 = [r11]		   // M [parse_include.c: 90/1]
	add		r9 = r9, gp		   // I [parse_include.c: 90/1]
	add		out0 = 32, r10		;; // I [parse_include.c: 90/1]
	add		r10 = 8, r9		   // M [parse_include.c: 90/1]
	ld8		r9 = [r9]		   // M [parse_include.c: 90/1]
	nop.i		0			;; // I
	ld8		gp = [r10]		   // M [parse_include.c: 90/1]
	mov		b6 = r9			   // I [parse_include.c: 90/1]
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 90/1] [UVU] [UVuse]
	add		gp = 0, r40		   // M [parse_include.c: 90/1]
//file/line/col parse_include.c/91/1
	add		out0 = 1, r0		   // M [parse_include.c: 91/1]
	br.call.dptk.few rp = exit		;; // B [parse_include.c: 91/1] [noret] [UVU]

..L3:
//file/line/col parse_include.c/93/1
	addp4		r10 = 4, r35		   // M [parse_include.c: 93/1] [UVuse]
	add		r9 = @pltoff(strdup), r0 ;; // I [parse_include.c: 93/1]
	add		r9 = r9, gp		   // I [parse_include.c: 93/1]
	ld4		out0 = [r10]		;; // M [parse_include.c: 93/1]
	add		r14 = r0, gp		   // M [parse_include.c: 93/1]
	add		r10 = 8, r9		   // I [parse_include.c: 93/1]
	ld8		r9 = [r9]		;; // M [parse_include.c: 93/1]
	ld8		gp = [r10]		   // M [parse_include.c: 93/1]
	mov		b6 = r9			   // I [parse_include.c: 93/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 93/1] [UVU]
	add		gp = 0, r40		   // M [parse_include.c: 93/1]
	mov		rp = r37		;; // I [parse_include.c: 93/1]
	mov		ar.pfs = r36		   // I [parse_include.c: 93/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 93/1]

..L1:
//	$end					;; // A

	.endp	parse_args

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 4
	data4.ua @segrel(.text)
	data4.ua @segrel(.text+272)
	data4.ua @segrel(.tcg$unwind_info_block0002)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 4
	data4.ua @segrel(.llo$annot_info_block0002)
.tcg$unwind_info_block0002:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x04
	data1	0x03, 0xe4, 0x02, 0xb0, 0xa5, 0xe6, 0x00, 0xb1
	data1	0x24, 0x61, 0x30, 0x81, 0xc0, 0x32, 0x00, 0x00

// ===

	.global fopen
	.type	fopen,@function
	.global parse_next_set_file
	.type	parse_next_set_file,@function
	.global print_parse_info
	.type	print_parse_info,@function
	.global __milli_strcmp
	.type	__milli_strcmp,@function
	.hidden		__milli_strcmp

// Routine 161 ("parse_file"::101)

	.section .text = "ax", "progbits"
	.proc	parse_file
..L0:
//	$start		CMid904 = 		;; // A

..L2:
parse_file::
//file/line/col parse_include.c/102/1
//	$entry		CMid939, r32, r33 = 	   // A [parse_include.c: 102/1]
	alloc		r38 = ar.pfs, 2, 9, 3, 0   // M [parse_include.c: 102/1] [UVU: ]
	add		r11 = 0, sp		   // M [parse_include.c: 102/1] [UVU: ]
	mov		r39 = rp		   // I [parse_include.c: 102/1]
	add		r42 = 0, gp		   // M
	add		r9 = 0, r33		   // M [parse_include.c: 102/1]
	add		r10 = 0, r32		;; // I [parse_include.c: 102/1]
	add		sp = -32, sp		   // M [parse_include.c: 102/1] [UVU: ]
	add		r40 = -16, r11		   // M [parse_include.c: 102/1]
//	$fence					   // A [parse_include.c: 102/1] [UVU: ]
	add		r34 = 0, r10		   // I [parse_include.c: 102/1] [UVU: input_file_path]
	add		r35 = 0, r9		;; // M [parse_include.c: 102/1] [UVU: parse_info_p]
	add		r41 = -48, sp		   // M [parse_include.c: 102/1]
//file/line/col parse_include.c/102/1/E,107/3
	add		r9 = @ltoff(.tcg0$rodata), r0 // I [parse_include.c: 107/3]
	add		out0 = 0, r34		;; // M [parse_include.c: 107/3] [UVuse]
	add		r9 = r9, gp		   // M [parse_include.c: 107/3]
	add		r14 = r0, gp		;; // I [parse_include.c: 107/3]
	ld8		r10 = [r9]		   // M [parse_include.c: 107/3]
	add		r9 = @pltoff(fopen), r0	;; // I [parse_include.c: 107/3]
	add		r9 = r9, gp		   // I [parse_include.c: 107/3]
	add		out1 = 38, r10		;; // M [parse_include.c: 107/3]
	add		r10 = 8, r9		   // M [parse_include.c: 107/3]
	nop.i		0			   // I
	ld8		r9 = [r9]		;; // M [parse_include.c: 107/3]
	ld8		gp = [r10]		   // M [parse_include.c: 107/3]
	mov		b6 = r9			   // I [parse_include.c: 107/3]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 107/3] [UVU] [UVuse]
	add		gp = 0, r42		   // M [parse_include.c: 107/3]
	add		r36 = 0, r8		;; // I [parse_include.c: 107/3] [UVU: input_file]
//file/line/col parse_include.c/108/1
	cmp4.ne		p6, p0 = r0, r36	   // I [parse_include.c: 108/1] [UVuse]
	nop.m		0			   // M
	nop.m		0			   // M
(p6)	br.dptk.few	..L3			;; // B [parse_include.c: 108/1]

..L4:
//file/line/col parse_include.c/110/1
	add		r9 = @ltoff(__iob), r0	   // M [parse_include.c: 110/1]
	add		out2 = 0, r34		;; // I [parse_include.c: 110/1] [UVuse]
	add		r10 = r9, gp		   // I [parse_include.c: 110/1]
	add		r9 = @ltoff(.tcg0$rodata), r0 // M [parse_include.c: 110/1]
	add		r11 = @pltoff(fprintf), r0 ;; // I [parse_include.c: 110/1]
	add		r9 = r9, gp		   // I [parse_include.c: 110/1]
	ld8		r10 = [r10]		   // M [parse_include.c: 110/1]
	add		r11 = r11, gp		   // M [parse_include.c: 110/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 110/1]
	ld8		r9 = [r9]		   // M [parse_include.c: 110/1]
	add		out0 = 32, r10		   // M [parse_include.c: 110/1]
	add		r10 = 8, r11		   // I [parse_include.c: 110/1]
	ld8		r11 = [r11]		;; // M [parse_include.c: 110/1]
	nop.m		0			   // M
	mov		b6 = r11		   // I [parse_include.c: 110/1]
	ld8		gp = [r10]		   // M [parse_include.c: 110/1]
	add		out1 = 48, r9		   // M [parse_include.c: 110/1]
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 110/1] [UVU] [UVuse]
	add		gp = 0, r42		   // M [parse_include.c: 110/1]
//file/line/col parse_include.c/111/1
	add		out0 = 1, r0		   // M [parse_include.c: 111/1]
	br.call.dptk.few rp = exit		;; // B [parse_include.c: 111/1] [noret] [UVU]

..L3:
//file/line/col parse_include.c/113/5
	addp4		r9 = 0, r35		   // M [parse_include.c: 113/5] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r9		;; // I [parse_include.c: 113/5]
	st4		[r9] = r36		   // M [parse_include.c: 113/5] [UVU] [UVuse]
//file/line/col parse_include.c/114/5
	addp4		r9 = 0, r35		   // I [parse_include.c: 114/5] [UVuse]
	add		r10 = 1, r0		;; // I [parse_include.c: 114/5]
	add		r9 = 412, r9		;; // M [parse_include.c: 114/5]
	st4		[r9] = r10		   // M [parse_include.c: 114/5] [UVU]
//file/line/col parse_include.c/116/3
	add		out0 = 0, r35		   // I [parse_include.c: 116/3] [UVuse]
	add		out1 = 0, r40		   // M [parse_include.c: 116/3]
	nop.m		0			   // M
	br.call.dptk.few rp = parse_next_set_file ;; // B [parse_include.c: 116/3] [UVU] [UVuse]
	add		gp = 0, r42		   // M [parse_include.c: 116/3]
	add		r9 = 0, r8		;; // I [parse_include.c: 116/3]
	add		r37 = 0, r9		   // I [parse_include.c: 116/3] [UVU: new_file]
	cmp4.eq		p6, p0 = r0, r9		   // M [parse_include.c: 116/3]
	nop.m		0			   // M
(p6)	br.dptk.few	..L5			;; // B [parse_include.c: 116/3]

..L6:
//file/line/col parse_include.c/118/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L7			;; // B [parse_include.c: 118/1]

..L7:
//file/line/col parse_include.c/118/1/D1/E
	ld4		r8 = [r40]		;; // M [parse_include.c: 118/1]
	cmp4.ne		p6, p0 = 1, r8		   // M [parse_include.c: 118/1]
	nop.i		0			   // I
	nop.m		0			   // M
	nop.m		0			   // M
(p6)	br.dptk.few	..L8			;; // B [parse_include.c: 118/1]

..L9:
//file/line/col parse_include.c/120/1
	addp4		r8 = 0, r35		;; // M [parse_include.c: 120/1] [UVuse]
	ld4		r8 = [r8]		   // M [parse_include.c: 120/1]
	nop.i		0			;; // I
	cmp4.gt		p6, p0 = 100, r8	   // M [parse_include.c: 120/1]
	nop.m		0			   // M
(p6)	br.dptk.few	..L10			;; // B [parse_include.c: 120/1]

..L11:
//file/line/col parse_include.c/122/1
	add		r9 = @ltoff(__iob), r0	;; // M [parse_include.c: 122/1]
	add		r10 = r9, gp		   // M [parse_include.c: 122/1]
	add		out2 = 100, r0		   // I [parse_include.c: 122/1]
	add		r9 = @ltoff(.tcg0$rodata), r0 // M [parse_include.c: 122/1]
	add		r11 = @pltoff(fprintf), r0 ;; // I [parse_include.c: 122/1]
	add		r9 = r9, gp		   // I [parse_include.c: 122/1]
	ld8		r10 = [r10]		   // M [parse_include.c: 122/1]
	add		r11 = r11, gp		   // M [parse_include.c: 122/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 122/1]
	ld8		r9 = [r9]		   // M [parse_include.c: 122/1]
	add		out0 = 32, r10		   // M [parse_include.c: 122/1]
	add		r10 = 8, r11		   // I [parse_include.c: 122/1]
	ld8		r11 = [r11]		;; // M [parse_include.c: 122/1]
	nop.m		0			   // M
	mov		b6 = r11		   // I [parse_include.c: 122/1]
	ld8		gp = [r10]		   // M [parse_include.c: 122/1]
	add		out1 = 144, r9		   // M [parse_include.c: 122/1]
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 122/1] [UVU]
	add		gp = 0, r42		   // M [parse_include.c: 122/1]
//file/line/col parse_include.c/124/1
	add		out0 = 1, r0		   // M [parse_include.c: 124/1]
	br.call.dptk.few rp = exit		;; // B [parse_include.c: 124/1] [noret] [UVU]

..L10:
//file/line/col parse_include.c/126/1
	addp4		r8 = 0, r35		;; // M [parse_include.c: 126/1] [UVuse]
	ld4		r9 = [r8]		   // M [parse_include.c: 126/1]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // M [parse_include.c: 126/1]
	st4		[r8] = r9		   // M [parse_include.c: 126/1] [UVU]
//file/line/col parse_include.c/127/10
	add		r9 = 4, r35		   // I [parse_include.c: 127/10] [UVuse]
	addp4		r8 = 0, r35		;; // M [parse_include.c: 127/10] [UVuse]
	ld4		r8 = [r8]		   // M [parse_include.c: 127/10,127]
	nop.i		0			;; // I
	shladd		r8 = r8, 2, r0		   // M [parse_include.c: 127/10]
	nop.i		0			;; // I
	addp4		r8 = r8, r9		;; // I [parse_include.c: 127/10]
	st4		[r8] = r37		   // M [parse_include.c: 127/10] [UVU] [UVuse]
//file/line/col parse_include.c/128/1
	add		out0 = 0, r35		   // M [parse_include.c: 128/1] [UVuse]
	br.call.dptk.few rp = print_parse_info	;; // B [parse_include.c: 128/1] [UVU] [UVuse]
	add		gp = 0, r42		   // M [parse_include.c: 128/1]
//file/line/col parse_include.c/128/1/D1/E
	nop.m		0			   // M
	br.dptk.few	..L12			;; // B [parse_include.c: 128/1]

..L8:
//file/line/col parse_include.c/130/2,130/2/D1/E
	addp4		r8 = 0, r35		;; // M [parse_include.c: 130/2] [UVuse]
	ld4		r8 = [r8]		   // M [parse_include.c: 130/2]
	nop.i		0			;; // I
	cmp4.lt		p6, p0 = r0, r8		   // M [parse_include.c: 130/2]
	nop.m		0			   // M
(p6)	br.dptk.few	..L13			;; // B [parse_include.c: 130/2]

..L14:
//file/line/col parse_include.c/132/1
	addp4		r8 = 0, r35		;; // M [parse_include.c: 132/1] [UVuse]
	ld4		r8 = [r8]		   // M [parse_include.c: 132/1]
	nop.i		0			;; // I
	cmp4.ne		p6, p0 = r0, r8		   // M [parse_include.c: 132/1]
	nop.m		0			   // M
(p6)	br.dptk.few	..L15			;; // B [parse_include.c: 132/1]

..L16:
//file/line/col parse_include.c/133/2/E
	add		r10 = 4, r35		   // M [parse_include.c: 132/1] [UVuse]
	add		out0 = 0, r37		;; // I [parse_include.c: 132/1] [UVuse]
	addp4		r9 = 0, r35		;; // I [parse_include.c: 132/1] [UVuse]
	ld4		r9 = [r9]		   // M [parse_include.c: 132/1,132]
	nop.i		0			;; // I
	shladd		r9 = r9, 2, r0		;; // I [parse_include.c: 132/1]
	addp4		r9 = r9, r10		;; // M [parse_include.c: 132/1]
	ld4		out1 = [r9]		   // M [parse_include.c: 132/1]
	nop.i		0			   // I
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = __milli_strcmp	;; // B [parse_include.c: 132/1] [UVU] [UVuse]
	cmp4.ne		p6, p0 = r0, r8		   // M [parse_include.c: 132/1]
	nop.m		0			   // M
(p6)	br.dptk.few	..L15			;; // B [parse_include.c: 132/1]

..L17:
//file/line/col parse_include.c/135/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L18			;; // B [parse_include.c: 135/1]

..L15:
	add		r9 = @ltoff(__iob), r0	;; // A [parse_include.c: 135/1]
	add		r15 = r9, gp		   // M [parse_include.c: 135/1]
	addp4		r9 = 0, r35		   // I [parse_include.c: 135/1] [UVuse]
	nop.i		0			;; // I
	add		r10 = @ltoff(.tcg0$rodata), r0 ;; // A [parse_include.c: 135/1]
	add		r10 = r10, gp		   // M [parse_include.c: 135/1]
	add		r11 = 412, r9		   // M [parse_include.c: 135/1]
	add		r9 = @pltoff(fprintf), r0 ;; // A [parse_include.c: 135/1]
	add		r9 = r9, gp		   // I [parse_include.c: 135/1]
	ld8		r15 = [r15]		   // M [parse_include.c: 135/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 135/1]
	add		out0 = 32, r15		   // I [parse_include.c: 135/1]
	ld8		r10 = [r10]		   // M [parse_include.c: 135/1]
	ld4		out2 = [r11]		   // M [parse_include.c: 135/1]
	add		r11 = 8, r9		;; // I [parse_include.c: 135/1]
	ld8		r9 = [r9]		   // M [parse_include.c: 135/1]
	ld8		gp = [r11]		   // M [parse_include.c: 135/1]
	add		out1 = 80, r10		;; // I [parse_include.c: 135/1]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 135/1]
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 135/1] [UVU]
	add		gp = 0, r42		   // M [parse_include.c: 135/1]
//file/line/col parse_include.c/138/1
	nop.m		0			   // M
	br.dptk.few	..L18			;; // B [parse_include.c: 138/1]

..L18:
//file/line/col parse_include.c/138/1/D1/E
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L12			;; // B [parse_include.c: 138/1]

..L13:
//file/line/col parse_include.c/141/1,141/1/D1/E
	add		r10 = 4, r35		   // M [parse_include.c: 141/1] [UVuse]
	add		out0 = 0, r37		;; // I [parse_include.c: 141/1] [UVuse]
	addp4		r9 = 0, r35		;; // I [parse_include.c: 141/1] [UVuse]
	ld4		r9 = [r9]		   // M [parse_include.c: 141/1,141]
	nop.i		0			;; // I
	shladd		r9 = r9, 2, r0		;; // I [parse_include.c: 141/1]
	addp4		r9 = r9, r10		;; // M [parse_include.c: 141/1]
	ld4		out1 = [r9]		   // M [parse_include.c: 141/1]
	nop.i		0			   // I
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = __milli_strcmp	;; // B [parse_include.c: 141/1] [UVU] [UVuse]
	cmp4.eq		p6, p0 = r0, r8		   // M [parse_include.c: 141/1]
	nop.m		0			   // M
(p6)	br.dptk.few	..L12			;; // B [parse_include.c: 141/1]

..L19:
//file/line/col parse_include.c/145/1
	add		r11 = 4, r35		   // M [parse_include.c: 145/1] [UVuse]
	add		r9 = @pltoff(free), r0	;; // A [parse_include.c: 145/1]
	add		r10 = r9, gp		;; // I [parse_include.c: 145/1]
	addp4		r9 = 0, r35		   // I [parse_include.c: 145/1] [UVuse]
	add		r15 = 8, r10		;; // M [parse_include.c: 145/1]
	ld4		r9 = [r9]		   // M [parse_include.c: 145/1,145]
	nop.i		0			;; // I
	ld8		r10 = [r10]		   // M [parse_include.c: 145/1]
	ld8		gp = [r15]		   // M [parse_include.c: 145/1]
	shladd		r9 = r9, 2, r0		;; // I [parse_include.c: 145/1]
	addp4		r9 = r9, r11		   // M [parse_include.c: 145/1]
	mov		b6 = r10		   // I [parse_include.c: 145/1]
	add		r14 = r0, r42		;; // I [parse_include.c: 145/1]
	ld4		out0 = [r9]		   // M [parse_include.c: 145/1]
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 145/1] [UVU]
	add		gp = 0, r42		   // M [parse_include.c: 145/1]
//file/line/col parse_include.c/146/1
	addp4		r9 = 0, r35		;; // I [parse_include.c: 146/1] [UVuse]
	nop.i		0			   // I
	ld4		r10 = [r9]		   // M [parse_include.c: 146/1]
	nop.i		0			;; // I
	add		r10 = -1, r10		;; // I [parse_include.c: 146/1]
	st4		[r9] = r10		   // M [parse_include.c: 146/1] [UVU]
//file/line/col parse_include.c/147/1
	add		r10 = 4, r35		   // M [parse_include.c: 147/1] [UVuse]
	addp4		r9 = 0, r35		   // I [parse_include.c: 147/1] [UVuse]
	add		out0 = 0, r37		;; // M [parse_include.c: 147/1] [UVuse]
	ld4		r9 = [r9]		   // M [parse_include.c: 147/1,147]
	nop.i		0			;; // I
	shladd		r9 = r9, 2, r0		   // M [parse_include.c: 147/1]
	nop.i		0			;; // I
	addp4		r9 = r9, r10		;; // I [parse_include.c: 147/1]
	ld4		out1 = [r9]		   // M [parse_include.c: 147/1]
	nop.m		0			   // M
	br.call.dptk.few rp = __milli_strcmp	;; // B [parse_include.c: 147/1] [UVU] [UVuse]
	cmp4.eq		p6, p0 = r0, r8		   // M [parse_include.c: 147/1]
	nop.m		0			   // M
(p6)	br.dptk.few	..L12			;; // B [parse_include.c: 147/1]

..L20:
//file/line/col parse_include.c/151/1
	add		r9 = @ltoff(__iob), r0	;; // A [parse_include.c: 151/1]
	add		r15 = r9, gp		   // M [parse_include.c: 151/1]
	addp4		r9 = 0, r35		   // I [parse_include.c: 151/1] [UVuse]
	nop.i		0			;; // I
	add		r10 = @ltoff(.tcg0$rodata), r0 ;; // A [parse_include.c: 151/1]
	add		r10 = r10, gp		   // M [parse_include.c: 151/1]
	add		r11 = 412, r9		   // M [parse_include.c: 151/1]
	add		r9 = @pltoff(fprintf), r0 ;; // A [parse_include.c: 151/1]
	add		r9 = r9, gp		   // I [parse_include.c: 151/1]
	ld8		r15 = [r15]		   // M [parse_include.c: 151/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 151/1]
	add		out0 = 32, r15		   // I [parse_include.c: 151/1]
	ld8		r10 = [r10]		   // M [parse_include.c: 151/1]
	ld4		out2 = [r11]		   // M [parse_include.c: 151/1]
	add		r11 = 8, r9		;; // I [parse_include.c: 151/1]
	ld8		r9 = [r9]		   // M [parse_include.c: 151/1]
	ld8		gp = [r11]		   // M [parse_include.c: 151/1]
	add		out1 = 112, r10		;; // I [parse_include.c: 151/1]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 151/1]
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 151/1] [UVU]
	add		gp = 0, r42		   // M [parse_include.c: 151/1]
//file/line/col parse_include.c/153/1
	nop.m		0			   // M
	br.dptk.few	..L12			;; // B [parse_include.c: 153/1]

..L12:
//file/line/col parse_include.c/116/3
	add		out0 = 0, r35		   // M [parse_include.c: 116/3] [UVuse]
	add		out1 = 0, r40		   // M [parse_include.c: 116/3]
	br.call.dptk.few rp = parse_next_set_file ;; // B [parse_include.c: 116/3] [UVU] [UVuse]
	add		gp = 0, r42		   // M [parse_include.c: 116/3]
	add		r9 = 0, r8		;; // I [parse_include.c: 116/3]
	add		r37 = 0, r9		   // I [parse_include.c: 116/3] [UVU: new_file]
	cmp4.ne		p6, p0 = r0, r9		   // M [parse_include.c: 116/3]
	nop.m		0			   // M
(p6)	br.dptk.few	..L7			;; // B [parse_include.c: 116/3]

..L21:
//file/line/col parse_include.c/156/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L5			;; // B [parse_include.c: 156/1]

..L5:
//file/line/col parse_include.c/158/1
	addp4		r8 = 0, r35		;; // M [parse_include.c: 158/1] [UVuse]
	ld4		r8 = [r8]		   // M [parse_include.c: 158/1]
	nop.i		0			;; // I
	cmp4.eq		p6, p0 = r0, r8		   // M [parse_include.c: 158/1]
	nop.m		0			   // M
(p6)	br.dptk.few	..L22			;; // B [parse_include.c: 158/1]

..L23:
//file/line/col parse_include.c/160/1
	add		r9 = @ltoff(__iob), r0	;; // M [parse_include.c: 160/1]
	add		r9 = r9, gp		   // M [parse_include.c: 160/1]
	add		r11 = @pltoff(fprintf), r0 ;; // I [parse_include.c: 160/1]
	ld8		r10 = [r9]		   // M [parse_include.c: 160/1]
	add		r9 = @ltoff(.tcg0$rodata), r0 // M [parse_include.c: 160/1]
	add		r11 = r11, gp		   // I [parse_include.c: 160/1]
	add		r14 = r0, gp		;; // M [parse_include.c: 160/1]
	add		r9 = r9, gp		   // M [parse_include.c: 160/1]
	add		out0 = 32, r10		;; // I [parse_include.c: 160/1]
	ld8		r10 = [r9]		   // M [parse_include.c: 160/1]
	ld8		r9 = [r11]		   // M [parse_include.c: 160/1]
	add		r11 = 8, r11		;; // I [parse_include.c: 160/1]
	ld8		gp = [r11]		   // M [parse_include.c: 160/1]
	add		out1 = 192, r10		   // M [parse_include.c: 160/1]
	mov		b6 = r9			   // I [parse_include.c: 160/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 160/1] [UVU]
	add		gp = 0, r42		   // M [parse_include.c: 160/1]
//file/line/col parse_include.c/161/1
	add		out0 = 1, r0		   // M [parse_include.c: 161/1]
	br.call.dptk.few rp = exit		;; // B [parse_include.c: 161/1] [noret] [UVU]

..L22:
//file/line/col parse_include.c/163/1,163/1/D1/E
	add		sp = 32, sp		   // M [parse_include.c: 163/1] [UVU: ]
	mov		rp = r39		;; // I [parse_include.c: 163/1]
	mov		ar.pfs = r38		   // I [parse_include.c: 163/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 163/1]

..L1:
//	$end					;; // A

	.endp	parse_file

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 4
	data4.ua @segrel(.text)
	data4.ua @segrel(.text+1664)
	data4.ua @segrel(.tcg$unwind_info_block0003)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 4
	data4.ua @segrel(.llo$annot_info_block0003)
.tcg$unwind_info_block0003:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x05
	data1	0x08, 0xe4, 0x02, 0xb0, 0xa7, 0xe6, 0x00, 0xb1
	data1	0x26, 0xe0, 0x06, 0x02, 0xe2, 0x04, 0x61, 0xb0
	data1	0x02, 0x81, 0xc0, 0x05

// ===

	.global __filbuf
	.type	__filbuf,@function
	.global realloc
	.type	realloc,@function

// Routine 281 ("parse_next_set_file"::101)

	.section .text = "ax", "progbits"
	.proc	parse_next_set_file
..L0:
//	$start		CMid904 = 		;; // A

..L2:
parse_next_set_file::
//file/line/col parse_include.c/193/1
//	$entry		CMid951, r32, r33 = 	   // A [parse_include.c: 193/1]
	alloc		r41 = ar.pfs, 2, 12, 2, 0  // M [parse_include.c: 193/1] [UVU: ]
	add		r45 = 0, gp		   // M
	mov		r42 = rp		   // I [parse_include.c: 193/1]
//	$fence					   // A [parse_include.c: 193/1] [UVU: ]
	add		r8 = 0, r33		   // M [parse_include.c: 193/1]
	add		r9 = 0, r32		   // M [parse_include.c: 193/1]
	add		r44 = -48, sp		;; // I [parse_include.c: 193/1]
	add		r34 = 0, r9		   // M [parse_include.c: 193/1] [UVU: parse_info_p]
	add		r35 = 0, r8		;; // I [parse_include.c: 193/1] [UVU: new_line]
//file/line/col parse_include.c/193/1/E,196/4
	addp4		r8 = 0, r35		;; // I [parse_include.c: 196/4] [UVuse]
	ld4		r38 = [r8]		   // M [parse_include.c: 196/4] [UVU: line_number]
//file/line/col parse_include.c/197/5
	add		r39 = 200, r0		;; // I [parse_include.c: 197/5] [UVU: INIT_MAX_NAME]
//file/line/col parse_include.c/218/3
	cmp.ne		p6, p0 = r0, r0		   // I [parse_include.c: 218/3]
	nop.m		0			   // M
	nop.m		0			   // M
(p6)	br.dptk.few	..L3			;; // B [parse_include.c: 218/3]

..L4:
//file/line/col parse_include.c/220/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L5			;; // B [parse_include.c: 220/1]

..L5:
//file/line/col parse_include.c/220/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 220/9] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 220/9]
	ld4		r8 = [r9]		;; // M [parse_include.c: 220/9]
	ld4		r9 = [r9]		   // M [parse_include.c: 220/9]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 220/9]
	ld4		r8 = [r8]		   // M [parse_include.c: 220/9]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 220/9]
	add		r8 = -1, r8		;; // I [parse_include.c: 220/9]
	st4		[r9] = r8		   // M [parse_include.c: 220/9] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 220/9]
(p6)	br.dptk.few	..L6			;; // B [parse_include.c: 220/9]

..L7:
//file/line/col parse_include.c/220/31/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 220/9] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 220/9]
	add		r9 = r9, gp		;; // I [parse_include.c: 220/9]
	add		r11 = 408, r10		   // I [parse_include.c: 220/9]
	add		r14 = r0, gp		   // M [parse_include.c: 220/9]
	add		r10 = 8, r9		;; // I [parse_include.c: 220/9]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 220/9]
	ld8		r9 = [r9]		   // M [parse_include.c: 220/9]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 220/9]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 220/9]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 220/9] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 220/9]
	add		r36 = 0, r8		   // M [parse_include.c: 220/9] [UVU: next_char]
//file/line/col parse_include.c/220/31/D1/E
	br.dptk.few	..L8			;; // B [parse_include.c: 220/9]

..L6:
//file/line/col parse_include.c/220/21,220/21/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 220/21] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 220/21]
	ld4		r8 = [r8]		   // M [parse_include.c: 220/21]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 220/21]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 220/21]
	ld4		r9 = [r8]		   // M [parse_include.c: 220/21]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 220/21]
	ld1		r36 = [r9]		   // M [parse_include.c: 220/21] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/220/23
	ld4		r9 = [r8]		   // M [parse_include.c: 220/23]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 220/23]
	st4		[r8] = r9		   // M [parse_include.c: 220/23] [UVU]
//file/line/col parse_include.c/220/1
	nop.m		0			   // M
	br.dptk.few	..L8			;; // B [parse_include.c: 220/1]

..L8:
//file/line/col parse_include.c/220/1/D1/E
	cmp4.ne		p6, p0 = -1, r36	   // M [parse_include.c: 220/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L9			;; // B [parse_include.c: 220/1]

..L10:
//file/line/col parse_include.c/221/1
	add		r8 = 0, r0		   // M [parse_include.c: 221/1]
	mov		rp = r42		;; // I [parse_include.c: 221/1]
	mov		ar.pfs = r41		   // I [parse_include.c: 221/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 221/1]

..L9:
//file/line/col parse_include.c/223/1
	cmp4.eq		p6, p0 = 35, r36	   // M [parse_include.c: 223/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L11			;; // B [parse_include.c: 223/1]

..L12:
//file/line/col parse_include.c/224/2
	cmp4.ne		p6, p0 = 10, r36	   // M [parse_include.c: 224/2] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L13			;; // B [parse_include.c: 224/2]

..L14:
//file/line/col parse_include.c/224/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 224/9] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 224/9]
	ld4		r9 = [r8]		   // M [parse_include.c: 224/9]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 224/9]
	st4		[r8] = r9		   // M [parse_include.c: 224/9] [UVU]
//file/line/col parse_include.c/224/14
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 224/14]

..L13:
//file/line/col parse_include.c/224/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 224/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 224/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 224/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 224/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 224/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 224/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 224/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 224/25]
	st4		[r9] = r8		   // M [parse_include.c: 224/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 224/25]
(p6)	br.dptk.few	..L16			;; // B [parse_include.c: 224/25]

..L17:
//file/line/col parse_include.c/224/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 224/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 224/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 224/25]
	add		r11 = 408, r10		   // I [parse_include.c: 224/25]
	add		r14 = r0, gp		   // M [parse_include.c: 224/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 224/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 224/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 224/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 224/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 224/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 224/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 224/25]
	add		r36 = 0, r8		   // M [parse_include.c: 224/25] [UVU: next_char]
//file/line/col parse_include.c/224/47/D1/E
	br.dptk.few	..L18			;; // B [parse_include.c: 224/25]

..L16:
//file/line/col parse_include.c/224/37,224/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 224/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 224/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 224/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 224/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 224/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 224/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 224/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 224/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/224/39
	ld4		r9 = [r8]		   // M [parse_include.c: 224/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 224/39]
	st4		[r8] = r9		   // M [parse_include.c: 224/39] [UVU]
//file/line/col parse_include.c/224/19
	nop.m		0			   // M
	br.dptk.few	..L18			;; // B [parse_include.c: 224/19]

..L18:
//file/line/col parse_include.c/224/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 224/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L19			;; // B [parse_include.c: 224/19]

..L20:
//file/line/col parse_include.c/224/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 224/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L19			;; // B [parse_include.c: 224/19]

..L21:
//file/line/col parse_include.c/224/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L22			;; // B [parse_include.c: 224/65]

..L22:
//file/line/col parse_include.c/224/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 224/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 224/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 224/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 224/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 224/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 224/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 224/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 224/25]
	st4		[r9] = r8		   // M [parse_include.c: 224/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 224/25]
(p6)	br.dptk.few	..L23			;; // B [parse_include.c: 224/25]

..L24:
//file/line/col parse_include.c/224/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 224/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 224/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 224/25]
	add		r11 = 408, r10		   // I [parse_include.c: 224/25]
	add		r14 = r0, gp		   // M [parse_include.c: 224/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 224/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 224/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 224/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 224/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 224/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 224/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 224/25]
	add		r36 = 0, r8		   // M [parse_include.c: 224/25] [UVU: next_char]
//file/line/col parse_include.c/224/47/D1/E
	br.dptk.few	..L25			;; // B [parse_include.c: 224/25]

..L23:
//file/line/col parse_include.c/224/37,224/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 224/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 224/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 224/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 224/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 224/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 224/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 224/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 224/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/224/39
	ld4		r9 = [r8]		   // M [parse_include.c: 224/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 224/39]
	st4		[r8] = r9		   // M [parse_include.c: 224/39] [UVU]
//file/line/col parse_include.c/224/19
	nop.m		0			   // M
	br.dptk.few	..L25			;; // B [parse_include.c: 224/19]

..L25:
//file/line/col parse_include.c/224/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 224/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L19			;; // B [parse_include.c: 224/19]

..L26:
//file/line/col parse_include.c/224/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 224/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L19			;; // B [parse_include.c: 224/19]

..L27:
//file/line/col parse_include.c/224/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L22			;; // B [parse_include.c: 224/65]

..L19:
//file/line/col parse_include.c/224/66
	cmp4.ne		p6, p0 = -1, r36	   // M [parse_include.c: 224/66] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L28			;; // B [parse_include.c: 224/66]

..L29:
//file/line/col parse_include.c/224/75
	add		r8 = 0, r0		   // M [parse_include.c: 224/75]
	mov		rp = r42		;; // I [parse_include.c: 224/75]
	mov		ar.pfs = r41		   // I [parse_include.c: 224/75]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 224/75]

..L28:
//file/line/col parse_include.c/224/78
	addp4		r8 = 0, r34		   // M [parse_include.c: 224/78] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 224/78]
	ld4		r9 = [r8]		   // M [parse_include.c: 224/78]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 224/78]
	st4		[r8] = r9		   // M [parse_include.c: 224/78] [UVU]
//file/line/col parse_include.c/224/83
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 224/83]

..L11:
//file/line/col parse_include.c/229/6
	addp4		r8 = 0, r34		   // M [parse_include.c: 229/6] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 229/6]
	ld4		r8 = [r9]		;; // M [parse_include.c: 229/6]
	ld4		r9 = [r9]		   // M [parse_include.c: 229/6]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 229/6]
	ld4		r8 = [r8]		   // M [parse_include.c: 229/6]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 229/6]
	add		r8 = -1, r8		;; // I [parse_include.c: 229/6]
	st4		[r9] = r8		   // M [parse_include.c: 229/6] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 229/6]
(p6)	br.dptk.few	..L30			;; // B [parse_include.c: 229/6]

..L31:
//file/line/col parse_include.c/229/28/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 229/6] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 229/6]
	add		r9 = r9, gp		;; // I [parse_include.c: 229/6]
	add		r11 = 408, r10		   // I [parse_include.c: 229/6]
	add		r14 = r0, gp		   // M [parse_include.c: 229/6]
	add		r10 = 8, r9		;; // I [parse_include.c: 229/6]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 229/6]
	ld8		r9 = [r9]		   // M [parse_include.c: 229/6]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 229/6]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 229/6]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 229/6] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 229/6]
	add		r36 = 0, r8		   // M [parse_include.c: 229/6] [UVU: next_char]
//file/line/col parse_include.c/229/28/D1/E
	br.dptk.few	..L32			;; // B [parse_include.c: 229/6]

..L30:
//file/line/col parse_include.c/229/18,229/18/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 229/18] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 229/18]
	ld4		r8 = [r8]		   // M [parse_include.c: 229/18]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 229/18]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 229/18]
	ld4		r9 = [r8]		   // M [parse_include.c: 229/18]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 229/18]
	ld1		r36 = [r9]		   // M [parse_include.c: 229/18] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/229/20
	ld4		r9 = [r8]		   // M [parse_include.c: 229/20]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 229/20]
	st4		[r8] = r9		   // M [parse_include.c: 229/20] [UVU]
//file/line/col parse_include.c/229/6
	nop.m		0			   // M
	br.dptk.few	..L32			;; // B [parse_include.c: 229/6]

..L32:
//file/line/col parse_include.c/230/1
	cmp4.eq		p6, p0 = 32, r36	   // M [parse_include.c: 230/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L33			;; // B [parse_include.c: 230/1]

..L34:
//file/line/col parse_include.c/231/2
	cmp4.ne		p6, p0 = 10, r36	   // M [parse_include.c: 231/2] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L35			;; // B [parse_include.c: 231/2]

..L36:
//file/line/col parse_include.c/231/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 231/9] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 231/9]
	ld4		r9 = [r8]		   // M [parse_include.c: 231/9]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 231/9]
	st4		[r8] = r9		   // M [parse_include.c: 231/9] [UVU]
//file/line/col parse_include.c/231/14
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 231/14]

..L35:
//file/line/col parse_include.c/231/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 231/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 231/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 231/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 231/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 231/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 231/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 231/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 231/25]
	st4		[r9] = r8		   // M [parse_include.c: 231/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 231/25]
(p6)	br.dptk.few	..L37			;; // B [parse_include.c: 231/25]

..L38:
//file/line/col parse_include.c/231/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 231/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 231/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 231/25]
	add		r11 = 408, r10		   // I [parse_include.c: 231/25]
	add		r14 = r0, gp		   // M [parse_include.c: 231/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 231/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 231/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 231/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 231/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 231/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 231/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 231/25]
	add		r36 = 0, r8		   // M [parse_include.c: 231/25] [UVU: next_char]
//file/line/col parse_include.c/231/47/D1/E
	br.dptk.few	..L39			;; // B [parse_include.c: 231/25]

..L37:
//file/line/col parse_include.c/231/37,231/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 231/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 231/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 231/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 231/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 231/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 231/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 231/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 231/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/231/39
	ld4		r9 = [r8]		   // M [parse_include.c: 231/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 231/39]
	st4		[r8] = r9		   // M [parse_include.c: 231/39] [UVU]
//file/line/col parse_include.c/231/19
	nop.m		0			   // M
	br.dptk.few	..L39			;; // B [parse_include.c: 231/19]

..L39:
//file/line/col parse_include.c/231/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 231/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L40			;; // B [parse_include.c: 231/19]

..L41:
//file/line/col parse_include.c/231/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 231/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L40			;; // B [parse_include.c: 231/19]

..L42:
//file/line/col parse_include.c/231/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L43			;; // B [parse_include.c: 231/65]

..L43:
//file/line/col parse_include.c/231/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 231/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 231/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 231/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 231/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 231/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 231/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 231/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 231/25]
	st4		[r9] = r8		   // M [parse_include.c: 231/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 231/25]
(p6)	br.dptk.few	..L44			;; // B [parse_include.c: 231/25]

..L45:
//file/line/col parse_include.c/231/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 231/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 231/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 231/25]
	add		r11 = 408, r10		   // I [parse_include.c: 231/25]
	add		r14 = r0, gp		   // M [parse_include.c: 231/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 231/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 231/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 231/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 231/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 231/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 231/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 231/25]
	add		r36 = 0, r8		   // M [parse_include.c: 231/25] [UVU: next_char]
//file/line/col parse_include.c/231/47/D1/E
	br.dptk.few	..L46			;; // B [parse_include.c: 231/25]

..L44:
//file/line/col parse_include.c/231/37,231/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 231/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 231/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 231/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 231/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 231/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 231/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 231/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 231/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/231/39
	ld4		r9 = [r8]		   // M [parse_include.c: 231/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 231/39]
	st4		[r8] = r9		   // M [parse_include.c: 231/39] [UVU]
//file/line/col parse_include.c/231/19
	nop.m		0			   // M
	br.dptk.few	..L46			;; // B [parse_include.c: 231/19]

..L46:
//file/line/col parse_include.c/231/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 231/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L40			;; // B [parse_include.c: 231/19]

..L47:
//file/line/col parse_include.c/231/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 231/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L40			;; // B [parse_include.c: 231/19]

..L48:
//file/line/col parse_include.c/231/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L43			;; // B [parse_include.c: 231/65]

..L40:
//file/line/col parse_include.c/231/66
	cmp4.ne		p6, p0 = -1, r36	   // M [parse_include.c: 231/66] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L49			;; // B [parse_include.c: 231/66]

..L50:
//file/line/col parse_include.c/231/75
	add		r8 = 0, r0		   // M [parse_include.c: 231/75]
	mov		rp = r42		;; // I [parse_include.c: 231/75]
	mov		ar.pfs = r41		   // I [parse_include.c: 231/75]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 231/75]

..L49:
//file/line/col parse_include.c/231/78
	addp4		r8 = 0, r34		   // M [parse_include.c: 231/78] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 231/78]
	ld4		r9 = [r8]		   // M [parse_include.c: 231/78]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 231/78]
	st4		[r8] = r9		   // M [parse_include.c: 231/78] [UVU]
//file/line/col parse_include.c/231/83
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 231/83]

..L33:
//file/line/col parse_include.c/232/3
	add		r37 = 0, r38		;; // M [parse_include.c: 232/3] [UVU: last_line_nbr] [UVuse]
//file/line/col parse_include.c/233/3
	add		r38 = 0, r0		   // M [parse_include.c: 233/3] [UVU: line_number]
//file/line/col parse_include.c/234/9
	addp4		r8 = 0, r34		;; // I [parse_include.c: 234/9] [UVuse]
	add		r9 = 408, r8		;; // M [parse_include.c: 234/9]
	ld4		r8 = [r9]		   // M [parse_include.c: 234/9]
	nop.i		0			;; // I
	ld4		r9 = [r9]		   // M [parse_include.c: 234/9]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 234/9]
	addp4		r9 = 0, r9		   // I [parse_include.c: 234/9]
	ld4		r8 = [r8]		   // M [parse_include.c: 234/9]
	nop.i		0			;; // I
	add		r8 = -1, r8		;; // I [parse_include.c: 234/9]
	st4		[r9] = r8		   // M [parse_include.c: 234/9] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 234/9]
(p6)	br.dptk.few	..L51			;; // B [parse_include.c: 234/9]

..L52:
//file/line/col parse_include.c/234/31/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 234/9] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 234/9]
	add		r9 = r9, gp		;; // I [parse_include.c: 234/9]
	add		r11 = 408, r10		   // I [parse_include.c: 234/9]
	add		r14 = r0, gp		   // M [parse_include.c: 234/9]
	add		r10 = 8, r9		;; // I [parse_include.c: 234/9]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 234/9]
	ld8		r9 = [r9]		   // M [parse_include.c: 234/9]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 234/9]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 234/9]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 234/9] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 234/9]
	add		r36 = 0, r8		   // M [parse_include.c: 234/9] [UVU: next_char]
//file/line/col parse_include.c/234/31/D1/E
	br.dptk.few	..L53			;; // B [parse_include.c: 234/9]

..L51:
//file/line/col parse_include.c/234/21,234/21/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 234/21] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 234/21]
	ld4		r8 = [r8]		   // M [parse_include.c: 234/21]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 234/21]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 234/21]
	ld4		r9 = [r8]		   // M [parse_include.c: 234/21]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 234/21]
	ld1		r36 = [r9]		   // M [parse_include.c: 234/21] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/234/23
	ld4		r9 = [r8]		   // M [parse_include.c: 234/23]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 234/23]
	st4		[r8] = r9		   // M [parse_include.c: 234/23] [UVU]
//file/line/col parse_include.c/234/3
	nop.m		0			   // M
	br.dptk.few	..L53			;; // B [parse_include.c: 234/3]

..L53:
//file/line/col parse_include.c/234/3/D1/E
	cmp4.gt		p6, p0 = 48, r36	   // M [parse_include.c: 234/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L54			;; // B [parse_include.c: 234/3]

..L55:
//file/line/col parse_include.c/235/2/D1/E
	cmp4.lt		p6, p0 = 57, r36	   // M [parse_include.c: 234/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L54			;; // B [parse_include.c: 234/3]

..L56:
//file/line/col parse_include.c/236/3
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L57			;; // B [parse_include.c: 236/3]

..L57:
	shladd		r8 = r38, 2, r38	   // M [parse_include.c: 236/3] [UVuse]
	nop.i		0			;; // I
	add		r8 = r8, r8		;; // I [parse_include.c: 236/3]
	add		r8 = r8, r36		;; // M [parse_include.c: 236/3] [UVuse]
	add		r38 = -48, r8		   // M [parse_include.c: 236/3] [UVU: line_number]
//file/line/col parse_include.c/234/9
	addp4		r8 = 0, r34		;; // I [parse_include.c: 234/9] [UVuse]
	add		r9 = 408, r8		;; // M [parse_include.c: 234/9]
	ld4		r8 = [r9]		   // M [parse_include.c: 234/9]
	nop.i		0			;; // I
	ld4		r9 = [r9]		   // M [parse_include.c: 234/9]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 234/9]
	addp4		r9 = 0, r9		   // I [parse_include.c: 234/9]
	ld4		r8 = [r8]		   // M [parse_include.c: 234/9]
	nop.i		0			;; // I
	add		r8 = -1, r8		;; // I [parse_include.c: 234/9]
	st4		[r9] = r8		   // M [parse_include.c: 234/9] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 234/9]
(p6)	br.dptk.few	..L58			;; // B [parse_include.c: 234/9]

..L59:
//file/line/col parse_include.c/234/31/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 234/9] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 234/9]
	add		r9 = r9, gp		;; // I [parse_include.c: 234/9]
	add		r11 = 408, r10		   // I [parse_include.c: 234/9]
	add		r14 = r0, gp		   // M [parse_include.c: 234/9]
	add		r10 = 8, r9		;; // I [parse_include.c: 234/9]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 234/9]
	ld8		r9 = [r9]		   // M [parse_include.c: 234/9]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 234/9]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 234/9]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 234/9] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 234/9]
	add		r36 = 0, r8		   // M [parse_include.c: 234/9] [UVU: next_char]
//file/line/col parse_include.c/234/31/D1/E
	br.dptk.few	..L60			;; // B [parse_include.c: 234/9]

..L58:
//file/line/col parse_include.c/234/21,234/21/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 234/21] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 234/21]
	ld4		r8 = [r8]		   // M [parse_include.c: 234/21]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 234/21]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 234/21]
	ld4		r9 = [r8]		   // M [parse_include.c: 234/21]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 234/21]
	ld1		r36 = [r9]		   // M [parse_include.c: 234/21] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/234/23
	ld4		r9 = [r8]		   // M [parse_include.c: 234/23]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 234/23]
	st4		[r8] = r9		   // M [parse_include.c: 234/23] [UVU]
//file/line/col parse_include.c/234/3
	nop.m		0			   // M
	br.dptk.few	..L60			;; // B [parse_include.c: 234/3]

..L60:
//file/line/col parse_include.c/234/3/D1/E
	cmp4.gt		p6, p0 = 48, r36	   // M [parse_include.c: 234/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L54			;; // B [parse_include.c: 234/3]

..L61:
//file/line/col parse_include.c/235/2/D1/E
	cmp4.lt		p6, p0 = 57, r36	   // M [parse_include.c: 234/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L54			;; // B [parse_include.c: 234/3]

..L62:
//file/line/col parse_include.c/236/10
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L57			;; // B [parse_include.c: 236/10]

..L54:
//file/line/col parse_include.c/237/1
	cmp4.eq		p6, p0 = 32, r36	   // M [parse_include.c: 237/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L63			;; // B [parse_include.c: 237/1]

..L64:
//file/line/col parse_include.c/238/2
	cmp4.ne		p6, p0 = 10, r36	   // M [parse_include.c: 238/2] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L65			;; // B [parse_include.c: 238/2]

..L66:
//file/line/col parse_include.c/238/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 238/9] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 238/9]
	ld4		r9 = [r8]		   // M [parse_include.c: 238/9]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 238/9]
	st4		[r8] = r9		   // M [parse_include.c: 238/9] [UVU]
//file/line/col parse_include.c/238/14
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 238/14]

..L65:
//file/line/col parse_include.c/238/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 238/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 238/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 238/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 238/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 238/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 238/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 238/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 238/25]
	st4		[r9] = r8		   // M [parse_include.c: 238/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 238/25]
(p6)	br.dptk.few	..L67			;; // B [parse_include.c: 238/25]

..L68:
//file/line/col parse_include.c/238/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 238/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 238/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 238/25]
	add		r11 = 408, r10		   // I [parse_include.c: 238/25]
	add		r14 = r0, gp		   // M [parse_include.c: 238/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 238/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 238/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 238/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 238/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 238/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 238/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 238/25]
	add		r36 = 0, r8		   // M [parse_include.c: 238/25] [UVU: next_char]
//file/line/col parse_include.c/238/47/D1/E
	br.dptk.few	..L69			;; // B [parse_include.c: 238/25]

..L67:
//file/line/col parse_include.c/238/37,238/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 238/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 238/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 238/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 238/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 238/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 238/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 238/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 238/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/238/39
	ld4		r9 = [r8]		   // M [parse_include.c: 238/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 238/39]
	st4		[r8] = r9		   // M [parse_include.c: 238/39] [UVU]
//file/line/col parse_include.c/238/19
	nop.m		0			   // M
	br.dptk.few	..L69			;; // B [parse_include.c: 238/19]

..L69:
//file/line/col parse_include.c/238/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 238/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L70			;; // B [parse_include.c: 238/19]

..L71:
//file/line/col parse_include.c/238/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 238/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L70			;; // B [parse_include.c: 238/19]

..L72:
//file/line/col parse_include.c/238/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L73			;; // B [parse_include.c: 238/65]

..L73:
//file/line/col parse_include.c/238/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 238/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 238/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 238/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 238/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 238/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 238/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 238/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 238/25]
	st4		[r9] = r8		   // M [parse_include.c: 238/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 238/25]
(p6)	br.dptk.few	..L74			;; // B [parse_include.c: 238/25]

..L75:
//file/line/col parse_include.c/238/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 238/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 238/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 238/25]
	add		r11 = 408, r10		   // I [parse_include.c: 238/25]
	add		r14 = r0, gp		   // M [parse_include.c: 238/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 238/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 238/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 238/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 238/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 238/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 238/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 238/25]
	add		r36 = 0, r8		   // M [parse_include.c: 238/25] [UVU: next_char]
//file/line/col parse_include.c/238/47/D1/E
	br.dptk.few	..L76			;; // B [parse_include.c: 238/25]

..L74:
//file/line/col parse_include.c/238/37,238/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 238/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 238/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 238/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 238/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 238/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 238/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 238/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 238/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/238/39
	ld4		r9 = [r8]		   // M [parse_include.c: 238/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 238/39]
	st4		[r8] = r9		   // M [parse_include.c: 238/39] [UVU]
//file/line/col parse_include.c/238/19
	nop.m		0			   // M
	br.dptk.few	..L76			;; // B [parse_include.c: 238/19]

..L76:
//file/line/col parse_include.c/238/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 238/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L70			;; // B [parse_include.c: 238/19]

..L77:
//file/line/col parse_include.c/238/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 238/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L70			;; // B [parse_include.c: 238/19]

..L78:
//file/line/col parse_include.c/238/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L73			;; // B [parse_include.c: 238/65]

..L70:
//file/line/col parse_include.c/238/66
	cmp4.ne		p6, p0 = -1, r36	   // M [parse_include.c: 238/66] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L79			;; // B [parse_include.c: 238/66]

..L80:
//file/line/col parse_include.c/238/75
	add		r8 = 0, r0		   // M [parse_include.c: 238/75]
	mov		rp = r42		;; // I [parse_include.c: 238/75]
	mov		ar.pfs = r41		   // I [parse_include.c: 238/75]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 238/75]

..L79:
//file/line/col parse_include.c/238/78
	addp4		r8 = 0, r34		   // M [parse_include.c: 238/78] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 238/78]
	ld4		r9 = [r8]		   // M [parse_include.c: 238/78]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 238/78]
	st4		[r8] = r9		   // M [parse_include.c: 238/78] [UVU]
//file/line/col parse_include.c/238/83
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 238/83]

..L63:
//file/line/col parse_include.c/239/6
	addp4		r8 = 0, r34		   // M [parse_include.c: 239/6] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 239/6]
	ld4		r8 = [r9]		;; // M [parse_include.c: 239/6]
	ld4		r9 = [r9]		   // M [parse_include.c: 239/6]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 239/6]
	ld4		r8 = [r8]		   // M [parse_include.c: 239/6]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 239/6]
	add		r8 = -1, r8		;; // I [parse_include.c: 239/6]
	st4		[r9] = r8		   // M [parse_include.c: 239/6] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 239/6]
(p6)	br.dptk.few	..L81			;; // B [parse_include.c: 239/6]

..L82:
//file/line/col parse_include.c/239/28/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 239/6] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 239/6]
	add		r9 = r9, gp		;; // I [parse_include.c: 239/6]
	add		r11 = 408, r10		   // I [parse_include.c: 239/6]
	add		r14 = r0, gp		   // M [parse_include.c: 239/6]
	add		r10 = 8, r9		;; // I [parse_include.c: 239/6]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 239/6]
	ld8		r9 = [r9]		   // M [parse_include.c: 239/6]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 239/6]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 239/6]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 239/6] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 239/6]
	add		r36 = 0, r8		   // M [parse_include.c: 239/6] [UVU: next_char]
//file/line/col parse_include.c/239/28/D1/E
	br.dptk.few	..L83			;; // B [parse_include.c: 239/6]

..L81:
//file/line/col parse_include.c/239/18,239/18/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 239/18] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 239/18]
	ld4		r8 = [r8]		   // M [parse_include.c: 239/18]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 239/18]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 239/18]
	ld4		r9 = [r8]		   // M [parse_include.c: 239/18]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 239/18]
	ld1		r36 = [r9]		   // M [parse_include.c: 239/18] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/239/20
	ld4		r9 = [r8]		   // M [parse_include.c: 239/20]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 239/20]
	st4		[r8] = r9		   // M [parse_include.c: 239/20] [UVU]
//file/line/col parse_include.c/239/6
	nop.m		0			   // M
	br.dptk.few	..L83			;; // B [parse_include.c: 239/6]

..L83:
//file/line/col parse_include.c/240/1
	cmp4.eq		p6, p0 = 34, r36	   // M [parse_include.c: 240/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L84			;; // B [parse_include.c: 240/1]

..L85:
//file/line/col parse_include.c/241/2
	cmp4.ne		p6, p0 = 10, r36	   // M [parse_include.c: 241/2] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L86			;; // B [parse_include.c: 241/2]

..L87:
//file/line/col parse_include.c/241/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 241/9] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 241/9]
	ld4		r9 = [r8]		   // M [parse_include.c: 241/9]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 241/9]
	st4		[r8] = r9		   // M [parse_include.c: 241/9] [UVU]
//file/line/col parse_include.c/241/14
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 241/14]

..L86:
//file/line/col parse_include.c/241/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 241/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 241/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 241/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 241/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 241/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 241/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 241/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 241/25]
	st4		[r9] = r8		   // M [parse_include.c: 241/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 241/25]
(p6)	br.dptk.few	..L88			;; // B [parse_include.c: 241/25]

..L89:
//file/line/col parse_include.c/241/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 241/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 241/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 241/25]
	add		r11 = 408, r10		   // I [parse_include.c: 241/25]
	add		r14 = r0, gp		   // M [parse_include.c: 241/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 241/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 241/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 241/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 241/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 241/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 241/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 241/25]
	add		r36 = 0, r8		   // M [parse_include.c: 241/25] [UVU: next_char]
//file/line/col parse_include.c/241/47/D1/E
	br.dptk.few	..L90			;; // B [parse_include.c: 241/25]

..L88:
//file/line/col parse_include.c/241/37,241/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 241/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 241/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 241/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 241/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 241/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 241/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 241/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 241/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/241/39
	ld4		r9 = [r8]		   // M [parse_include.c: 241/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 241/39]
	st4		[r8] = r9		   // M [parse_include.c: 241/39] [UVU]
//file/line/col parse_include.c/241/19
	nop.m		0			   // M
	br.dptk.few	..L90			;; // B [parse_include.c: 241/19]

..L90:
//file/line/col parse_include.c/241/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 241/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L91			;; // B [parse_include.c: 241/19]

..L92:
//file/line/col parse_include.c/241/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 241/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L91			;; // B [parse_include.c: 241/19]

..L93:
//file/line/col parse_include.c/241/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L94			;; // B [parse_include.c: 241/65]

..L94:
//file/line/col parse_include.c/241/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 241/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 241/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 241/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 241/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 241/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 241/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 241/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 241/25]
	st4		[r9] = r8		   // M [parse_include.c: 241/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 241/25]
(p6)	br.dptk.few	..L95			;; // B [parse_include.c: 241/25]

..L96:
//file/line/col parse_include.c/241/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 241/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 241/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 241/25]
	add		r11 = 408, r10		   // I [parse_include.c: 241/25]
	add		r14 = r0, gp		   // M [parse_include.c: 241/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 241/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 241/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 241/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 241/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 241/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 241/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 241/25]
	add		r36 = 0, r8		   // M [parse_include.c: 241/25] [UVU: next_char]
//file/line/col parse_include.c/241/47/D1/E
	br.dptk.few	..L97			;; // B [parse_include.c: 241/25]

..L95:
//file/line/col parse_include.c/241/37,241/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 241/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 241/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 241/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 241/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 241/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 241/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 241/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 241/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/241/39
	ld4		r9 = [r8]		   // M [parse_include.c: 241/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 241/39]
	st4		[r8] = r9		   // M [parse_include.c: 241/39] [UVU]
//file/line/col parse_include.c/241/19
	nop.m		0			   // M
	br.dptk.few	..L97			;; // B [parse_include.c: 241/19]

..L97:
//file/line/col parse_include.c/241/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 241/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L91			;; // B [parse_include.c: 241/19]

..L98:
//file/line/col parse_include.c/241/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 241/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L91			;; // B [parse_include.c: 241/19]

..L99:
//file/line/col parse_include.c/241/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L94			;; // B [parse_include.c: 241/65]

..L91:
//file/line/col parse_include.c/241/66
	cmp4.ne		p6, p0 = -1, r36	   // M [parse_include.c: 241/66] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L100			;; // B [parse_include.c: 241/66]

..L101:
//file/line/col parse_include.c/241/75
	add		r8 = 0, r0		   // M [parse_include.c: 241/75]
	mov		rp = r42		;; // I [parse_include.c: 241/75]
	mov		ar.pfs = r41		   // I [parse_include.c: 241/75]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 241/75]

..L100:
//file/line/col parse_include.c/241/78
	addp4		r8 = 0, r34		   // M [parse_include.c: 241/78] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 241/78]
	ld4		r9 = [r8]		   // M [parse_include.c: 241/78]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 241/78]
	st4		[r8] = r9		   // M [parse_include.c: 241/78] [UVU]
//file/line/col parse_include.c/241/83
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 241/83]

..L84:
//file/line/col parse_include.c/242/3
	add		r40 = 0, r0		   // M [parse_include.c: 242/3] [UVU: cur_name_len]
//file/line/col parse_include.c/243/9
	addp4		r8 = 0, r34		;; // I [parse_include.c: 243/9] [UVuse]
	add		r9 = 408, r8		;; // I [parse_include.c: 243/9]
	ld4		r8 = [r9]		;; // M [parse_include.c: 243/9]
	ld4		r9 = [r9]		   // M [parse_include.c: 243/9]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 243/9]
	ld4		r8 = [r8]		   // M [parse_include.c: 243/9]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 243/9]
	add		r8 = -1, r8		;; // I [parse_include.c: 243/9]
	st4		[r9] = r8		   // M [parse_include.c: 243/9] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 243/9]
(p6)	br.dptk.few	..L102			;; // B [parse_include.c: 243/9]

..L103:
//file/line/col parse_include.c/243/31/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 243/9] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 243/9]
	add		r9 = r9, gp		;; // I [parse_include.c: 243/9]
	add		r11 = 408, r10		   // I [parse_include.c: 243/9]
	add		r14 = r0, gp		   // M [parse_include.c: 243/9]
	add		r10 = 8, r9		;; // I [parse_include.c: 243/9]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 243/9]
	ld8		r9 = [r9]		   // M [parse_include.c: 243/9]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 243/9]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 243/9]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 243/9] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 243/9]
	add		r36 = 0, r8		   // M [parse_include.c: 243/9] [UVU: next_char]
//file/line/col parse_include.c/243/31/D1/E
	br.dptk.few	..L104			;; // B [parse_include.c: 243/9]

..L102:
//file/line/col parse_include.c/243/21,243/21/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 243/21] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 243/21]
	ld4		r8 = [r8]		   // M [parse_include.c: 243/21]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 243/21]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 243/21]
	ld4		r9 = [r8]		   // M [parse_include.c: 243/21]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 243/21]
	ld1		r36 = [r9]		   // M [parse_include.c: 243/21] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/243/23
	ld4		r9 = [r8]		   // M [parse_include.c: 243/23]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 243/23]
	st4		[r8] = r9		   // M [parse_include.c: 243/23] [UVU]
//file/line/col parse_include.c/243/3
	nop.m		0			   // M
	br.dptk.few	..L104			;; // B [parse_include.c: 243/3]

..L104:
//file/line/col parse_include.c/243/3/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 243/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L105			;; // B [parse_include.c: 243/3]

..L106:
//file/line/col parse_include.c/244/2/D1/E
	cmp4.eq		p6, p0 = 34, r36	   // M [parse_include.c: 243/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L105			;; // B [parse_include.c: 243/3]

..L107:
//file/line/col parse_include.c/246/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L108			;; // B [parse_include.c: 246/1]

..L108:
//file/line/col parse_include.c/246/1/D1/E
	add		r8 = @gprel(.tcg1$sdata), gp ;; // M [parse_include.c: 246/1]
	ld4		r8 = [r8]		   // M [parse_include.c: 246/1]
	nop.i		0			;; // I
	cmp4.gt		p6, p0 = r8, r40	   // M [parse_include.c: 246/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L109			;; // B [parse_include.c: 246/1]

..L110:
//file/line/col parse_include.c/248/3
	add		r9 = @gprel(.tcg1$sdata), r0 ;; // A [parse_include.c: 248/3]
	add		r11 = r9, gp		;; // M [parse_include.c: 248/3]
	ld4		r9 = [r11]		   // M [parse_include.c: 248/3]
	nop.i		0			;; // I
	shladd		r9 = r9, 1, r0		   // M [parse_include.c: 248/3]
	nop.i		0			;; // I
	add		r9 = r9, r39		;; // I [parse_include.c: 248/3] [UVuse]
	st4		[r11] = r9		   // M [parse_include.c: 248/3] [UVU]
//file/line/col parse_include.c/249/3
	add		r9 = @gprel(.tcg1$sdata+4), r0 ;; // A [parse_include.c: 249/3]
	add		r32 = r9, gp		   // M [parse_include.c: 249/3]
	add		r9 = @pltoff(realloc), r0 ;; // A [parse_include.c: 249/3]
	add		r9 = r9, gp		   // I [parse_include.c: 249/3]
	add		r14 = r0, gp		;; // M [parse_include.c: 249/3]
	ld4		out0 = [r32]		   // M [parse_include.c: 249/3]
	add		r10 = 8, r9		   // I [parse_include.c: 249/3]
	ld4		out1 = [r11]		;; // M [parse_include.c: 249/3]
	ld8		r9 = [r9]		   // M [parse_include.c: 249/3]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 249/3]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 249/3]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 249/3] [UVU]
	st4		[r32] = r8		   // M [parse_include.c: 249/3] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 249/3]
//file/line/col parse_include.c/252/1
	br.dptk.few	..L109			;; // B [parse_include.c: 252/1]

..L109:
//file/line/col parse_include.c/251/7
	add		r8 = @gprel(.tcg1$sdata+4), gp ;; // M [parse_include.c: 251/7]
	ld4		r8 = [r8]		   // M [parse_include.c: 251/7]
	nop.i		0			;; // I
	addp4		r8 = r40, r8		;; // M [parse_include.c: 251/7] [UVuse]
	st1		[r8] = r36		   // M [parse_include.c: 251/7] [UVU] [UVuse]
//file/line/col parse_include.c/251/3
	add		r40 = 1, r40		   // I [parse_include.c: 251/3] [UVU: cur_name_len] [UVuse]
//file/line/col parse_include.c/243/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 243/9] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 243/9]
	ld4		r8 = [r9]		   // M [parse_include.c: 243/9]
	ld4		r9 = [r9]		   // M [parse_include.c: 243/9]
	nop.i		0			;; // I
	addp4		r8 = 0, r8		   // M [parse_include.c: 243/9]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 243/9]
	nop.i		0			   // I
	ld4		r8 = [r8]		   // M [parse_include.c: 243/9]
	nop.i		0			;; // I
	add		r8 = -1, r8		;; // I [parse_include.c: 243/9]
	st4		[r9] = r8		   // M [parse_include.c: 243/9] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 243/9]
(p6)	br.dptk.few	..L111			;; // B [parse_include.c: 243/9]

..L112:
//file/line/col parse_include.c/243/31/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 243/9] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 243/9]
	add		r9 = r9, gp		;; // I [parse_include.c: 243/9]
	add		r11 = 408, r10		   // I [parse_include.c: 243/9]
	add		r14 = r0, gp		   // M [parse_include.c: 243/9]
	add		r10 = 8, r9		;; // I [parse_include.c: 243/9]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 243/9]
	ld8		r9 = [r9]		   // M [parse_include.c: 243/9]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 243/9]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 243/9]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 243/9] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 243/9]
	add		r36 = 0, r8		   // M [parse_include.c: 243/9] [UVU: next_char]
//file/line/col parse_include.c/243/31/D1/E
	br.dptk.few	..L113			;; // B [parse_include.c: 243/9]

..L111:
//file/line/col parse_include.c/243/21,243/21/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 243/21] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 243/21]
	ld4		r8 = [r8]		   // M [parse_include.c: 243/21]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 243/21]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 243/21]
	ld4		r9 = [r8]		   // M [parse_include.c: 243/21]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 243/21]
	ld1		r36 = [r9]		   // M [parse_include.c: 243/21] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/243/23
	ld4		r9 = [r8]		   // M [parse_include.c: 243/23]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 243/23]
	st4		[r8] = r9		   // M [parse_include.c: 243/23] [UVU]
//file/line/col parse_include.c/243/3
	nop.m		0			   // M
	br.dptk.few	..L113			;; // B [parse_include.c: 243/3]

..L113:
//file/line/col parse_include.c/243/3/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 243/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L105			;; // B [parse_include.c: 243/3]

..L114:
//file/line/col parse_include.c/244/2/D1/E
	cmp4.eq		p6, p0 = 34, r36	   // M [parse_include.c: 243/3] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L105			;; // B [parse_include.c: 243/3]

..L115:
//file/line/col parse_include.c/252/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L108			;; // B [parse_include.c: 252/1]

..L105:
//file/line/col parse_include.c/253/1
	cmp4.eq		p6, p0 = 34, r36	   // M [parse_include.c: 253/1] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L116			;; // B [parse_include.c: 253/1]

..L117:
//file/line/col parse_include.c/254/2
	cmp4.ne		p6, p0 = 10, r36	   // M [parse_include.c: 254/2] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L118			;; // B [parse_include.c: 254/2]

..L119:
//file/line/col parse_include.c/254/9
	addp4		r8 = 0, r34		   // M [parse_include.c: 254/9] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 254/9]
	ld4		r9 = [r8]		   // M [parse_include.c: 254/9]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 254/9]
	st4		[r8] = r9		   // M [parse_include.c: 254/9] [UVU]
//file/line/col parse_include.c/254/14
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 254/14]

..L118:
//file/line/col parse_include.c/254/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 254/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 254/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 254/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 254/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 254/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 254/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 254/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 254/25]
	st4		[r9] = r8		   // M [parse_include.c: 254/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 254/25]
(p6)	br.dptk.few	..L120			;; // B [parse_include.c: 254/25]

..L121:
//file/line/col parse_include.c/254/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 254/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 254/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 254/25]
	add		r11 = 408, r10		   // I [parse_include.c: 254/25]
	add		r14 = r0, gp		   // M [parse_include.c: 254/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 254/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 254/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 254/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 254/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 254/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 254/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 254/25]
	add		r36 = 0, r8		   // M [parse_include.c: 254/25] [UVU: next_char]
//file/line/col parse_include.c/254/47/D1/E
	br.dptk.few	..L122			;; // B [parse_include.c: 254/25]

..L120:
//file/line/col parse_include.c/254/37,254/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 254/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 254/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 254/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 254/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 254/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 254/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 254/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 254/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/254/39
	ld4		r9 = [r8]		   // M [parse_include.c: 254/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 254/39]
	st4		[r8] = r9		   // M [parse_include.c: 254/39] [UVU]
//file/line/col parse_include.c/254/19
	nop.m		0			   // M
	br.dptk.few	..L122			;; // B [parse_include.c: 254/19]

..L122:
//file/line/col parse_include.c/254/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 254/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L123			;; // B [parse_include.c: 254/19]

..L124:
//file/line/col parse_include.c/254/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 254/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L123			;; // B [parse_include.c: 254/19]

..L125:
//file/line/col parse_include.c/254/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L126			;; // B [parse_include.c: 254/65]

..L126:
//file/line/col parse_include.c/254/25
	addp4		r8 = 0, r34		   // M [parse_include.c: 254/25] [UVuse]
	nop.i		0			;; // I
	add		r9 = 408, r8		;; // I [parse_include.c: 254/25]
	ld4		r8 = [r9]		;; // M [parse_include.c: 254/25]
	ld4		r9 = [r9]		   // M [parse_include.c: 254/25]
	addp4		r8 = 0, r8		;; // I [parse_include.c: 254/25]
	ld4		r8 = [r8]		   // M [parse_include.c: 254/25]
	addp4		r9 = 0, r9		;; // I [parse_include.c: 254/25]
	add		r8 = -1, r8		;; // I [parse_include.c: 254/25]
	st4		[r9] = r8		   // M [parse_include.c: 254/25] [UVU]
	cmp4.le		p6, p0 = r0, r8		   // M [parse_include.c: 254/25]
(p6)	br.dptk.few	..L127			;; // B [parse_include.c: 254/25]

..L128:
//file/line/col parse_include.c/254/47/E
	addp4		r10 = 0, r34		   // M [parse_include.c: 254/25] [UVuse]
	add		r9 = @pltoff(__filbuf), r0 ;; // A [parse_include.c: 254/25]
	add		r9 = r9, gp		;; // I [parse_include.c: 254/25]
	add		r11 = 408, r10		   // I [parse_include.c: 254/25]
	add		r14 = r0, gp		   // M [parse_include.c: 254/25]
	add		r10 = 8, r9		;; // I [parse_include.c: 254/25]
	nop.i		0			   // I
	ld4		out0 = [r11]		;; // M [parse_include.c: 254/25]
	ld8		r9 = [r9]		   // M [parse_include.c: 254/25]
	nop.i		0			   // I
	ld8		gp = [r10]		;; // M [parse_include.c: 254/25]
	nop.m		0			   // M
	mov		b6 = r9			   // I [parse_include.c: 254/25]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 254/25] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 254/25]
	add		r36 = 0, r8		   // M [parse_include.c: 254/25] [UVU: next_char]
//file/line/col parse_include.c/254/47/D1/E
	br.dptk.few	..L129			;; // B [parse_include.c: 254/25]

..L127:
//file/line/col parse_include.c/254/37,254/37/D1/E
	addp4		r8 = 0, r34		   // M [parse_include.c: 254/37] [UVuse]
	nop.i		0			;; // I
	add		r8 = 408, r8		;; // I [parse_include.c: 254/37]
	ld4		r8 = [r8]		   // M [parse_include.c: 254/37]
	nop.i		0			;; // I
	add		r8 = 4, r8		;; // I [parse_include.c: 254/37]
	addp4		r8 = 0, r8		;; // M [parse_include.c: 254/37]
	ld4		r9 = [r8]		   // M [parse_include.c: 254/37]
	nop.i		0			;; // I
	addp4		r9 = 0, r9		;; // M [parse_include.c: 254/37]
	ld1		r36 = [r9]		   // M [parse_include.c: 254/37] [UVU: next_char]
	nop.i		0			   // I
//file/line/col parse_include.c/254/39
	ld4		r9 = [r8]		   // M [parse_include.c: 254/39]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 254/39]
	st4		[r8] = r9		   // M [parse_include.c: 254/39] [UVU]
//file/line/col parse_include.c/254/19
	nop.m		0			   // M
	br.dptk.few	..L129			;; // B [parse_include.c: 254/19]

..L129:
//file/line/col parse_include.c/254/19/D1/E
	cmp4.eq		p6, p0 = -1, r36	   // M [parse_include.c: 254/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L123			;; // B [parse_include.c: 254/19]

..L130:
//file/line/col parse_include.c/254/61/D1/E
	cmp4.eq		p6, p0 = 10, r36	   // M [parse_include.c: 254/19] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L123			;; // B [parse_include.c: 254/19]

..L131:
//file/line/col parse_include.c/254/65
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L126			;; // B [parse_include.c: 254/65]

..L123:
//file/line/col parse_include.c/254/66
	cmp4.ne		p6, p0 = -1, r36	   // M [parse_include.c: 254/66] [UVuse]
	nop.m		0			   // M
(p6)	br.dptk.few	..L132			;; // B [parse_include.c: 254/66]

..L133:
//file/line/col parse_include.c/254/75
	add		r8 = 0, r0		   // M [parse_include.c: 254/75]
	mov		rp = r42		;; // I [parse_include.c: 254/75]
	mov		ar.pfs = r41		   // I [parse_include.c: 254/75]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 254/75]

..L132:
//file/line/col parse_include.c/254/78
	addp4		r8 = 0, r34		   // M [parse_include.c: 254/78] [UVuse]
	nop.i		0			;; // I
	add		r8 = 412, r8		;; // I [parse_include.c: 254/78]
	ld4		r9 = [r8]		   // M [parse_include.c: 254/78]
	nop.i		0			;; // I
	add		r9 = 1, r9		;; // I [parse_include.c: 254/78]
	st4		[r8] = r9		   // M [parse_include.c: 254/78] [UVU]
//file/line/col parse_include.c/254/83
	nop.m		0			   // M
	br.dptk.few	..L15			;; // B [parse_include.c: 254/83]

..L116:
//file/line/col parse_include.c/255/6
	add		r9 = @gprel(.tcg1$sdata+4), r0 // M [parse_include.c: 255/6]
	nop.i		0			;; // I
	add		r9 = r9, gp		;; // I [parse_include.c: 255/6]
	ld4		r10 = [r9]		   // M [parse_include.c: 255/6]
	nop.i		0			;; // I
	addp4		r10 = r40, r10		;; // I [parse_include.c: 255/6] [UVuse]
	st1		[r10] = r0		   // M [parse_include.c: 255/6] [UVU]
//file/line/col parse_include.c/256/4
	addp4		r10 = 0, r35		;; // I [parse_include.c: 256/4] [UVuse]
	nop.i		0			   // I
	st4		[r10] = r38		   // M [parse_include.c: 256/4] [UVU] [UVuse]
//file/line/col parse_include.c/257/5
	addp4		r10 = 0, r34		;; // I [parse_include.c: 257/5] [UVuse]
	add		r10 = 404, r10		;; // I [parse_include.c: 257/5]
	st4		[r10] = r37		   // M [parse_include.c: 257/5] [UVU] [UVuse]
//file/line/col parse_include.c/258/1
	add		r10 = @pltoff(strdup), r0  // I [parse_include.c: 258/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 258/1]
	ld4		out0 = [r9]		   // M [parse_include.c: 258/1]
	add		r10 = r10, gp		;; // I [parse_include.c: 258/1]
	nop.i		0			   // I
	ld8		r9 = [r10]		   // M [parse_include.c: 258/1]
	add		r10 = 8, r10		;; // I [parse_include.c: 258/1]
	mov		b6 = r9			   // I [parse_include.c: 258/1]
	ld8		gp = [r10]		   // M [parse_include.c: 258/1]
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 258/1] [UVU]
	add		gp = 0, r45		   // M [parse_include.c: 258/1]
	mov		rp = r42		;; // I [parse_include.c: 258/1]
	mov		ar.pfs = r41		   // I [parse_include.c: 258/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 258/1]

..L15:
//file/line/col parse_include.c/218/3
	cmp.eq		p6, p0 = r0, r0		   // M [parse_include.c: 218/3]
	nop.m		0			   // M
(p6)	br.dptk.few	..L5			;; // B [parse_include.c: 218/3]

..L134:
//file/line/col parse_include.c/259/1
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L3			;; // B [parse_include.c: 259/1]

..L3:
//file/line/col parse_include.c/260/1
	nop.m		0			   // M
	mov		rp = r42		;; // I [parse_include.c: 260/1]
	mov		ar.pfs = r41		   // I [parse_include.c: 260/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 260/1]

..L1:
//	$end					;; // A

	.endp	parse_next_set_file

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 4
	data4.ua @segrel(.text)
	data4.ua @segrel(.text+6832)
	data4.ua @segrel(.tcg$unwind_info_block0004)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 4
	data4.ua @segrel(.llo$annot_info_block0004)
.tcg$unwind_info_block0004:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x0f
	data1	0x03, 0xe4, 0x02, 0xb0, 0xaa, 0xe6, 0x00, 0xb1
	data1	0x29, 0x61, 0x4b, 0x81, 0xc0, 0x4d, 0x61, 0x90
	data1	0x01, 0xa1, 0xc0, 0xdd, 0x01, 0x61, 0xcc, 0x01
	data1	0xa1, 0xc0, 0xa9, 0x03, 0x61, 0x9a, 0x02, 0xa1
	data1	0xc0, 0xc3, 0x05, 0x61, 0xcc, 0x01, 0xa1, 0xc0
	data1	0x8f, 0x07, 0x61, 0xbe, 0x02, 0xa1, 0xc0, 0xcd
	data1	0x09, 0x61, 0x27, 0xa1, 0xc0, 0xf4, 0x09, 0x2c
	data1	0xa1, 0xc0, 0x80, 0x0a

// ===


// Routine 312 ("print_parse_info"::101)

	.section .text = "ax", "progbits"
	.proc	print_parse_info
..L0:
//	$start		CMid904 = 		;; // A

..L2:
print_parse_info::
//file/line/col parse_include.c/268/1
//	$entry		CMid913, r32 = 		   // A [parse_include.c: 268/1]
	alloc		r35 = ar.pfs, 1, 7, 5, 0   // M [parse_include.c: 268/1] [UVU: ]
	add		r39 = 0, gp		   // M
	mov		r36 = rp		   // I [parse_include.c: 268/1]
//	$fence					   // A [parse_include.c: 268/1] [UVU: ]
	add		r9 = 0, r32		   // M [parse_include.c: 268/1]
	add		r38 = -48, sp		;; // I [parse_include.c: 268/1]
	add		r33 = 0, r9		   // I [parse_include.c: 268/1] [UVU: parse_info_p]
//file/line/col parse_include.c/268/1/E,271/1
	add		r9 = @ltoff(__iob), r0	;; // M [parse_include.c: 271/1]
	add		r9 = r9, gp		   // M [parse_include.c: 271/1]
	addp4		r10 = 0, r33		   // I [parse_include.c: 271/1] [UVuse]
	add		r14 = r0, gp		;; // M [parse_include.c: 271/1]
	ld8		r15 = [r9]		   // M [parse_include.c: 271/1]
	add		r9 = @ltoff(.tcg0$rodata), r0 // I [parse_include.c: 271/1]
	add		r16 = 404, r10		;; // M [parse_include.c: 271/1]
	ld4		out2 = [r10]		   // M [parse_include.c: 271/1]
	add		r11 = r9, gp		   // I [parse_include.c: 271/1]
	add		r9 = @pltoff(fprintf), r0  // M [parse_include.c: 271/1]
	add		out0 = 16, r15		   // M [parse_include.c: 271/1]
	add		r15 = 412, r10		;; // I [parse_include.c: 271/1]
	ld8		r11 = [r11]		   // M [parse_include.c: 271/1]
	ld4		out3 = [r16]		   // M [parse_include.c: 271/1]
	add		r9 = r9, gp		;; // I [parse_include.c: 271/1]
	ld4		out4 = [r15]		   // M [parse_include.c: 271/1]
	add		r10 = 8, r9		   // M [parse_include.c: 271/1]
	add		out1 = 256, r11		   // I [parse_include.c: 271/1]
	ld8		r9 = [r9]		;; // M [parse_include.c: 271/1]
	ld8		gp = [r10]		   // M [parse_include.c: 271/1]
	mov		b6 = r9			   // I [parse_include.c: 271/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 271/1] [UVU]
	add		gp = 0, r39		   // M [parse_include.c: 271/1]
//file/line/col parse_include.c/277/5
	add		r34 = 0, r0		   // I [parse_include.c: 277/5] [UVU: i]
//file/line/col parse_include.c/277/7
	addp4		r9 = 0, r33		;; // I [parse_include.c: 277/7] [UVuse]
	ld4		r9 = [r9]		;; // M [parse_include.c: 277/7]
	cmp4.ge		p6, p0 = r34, r9	   // M [parse_include.c: 277/7] [UVuse]
	nop.i		0			   // I
	nop.m		0			   // M
	nop.m		0			   // M
(p6)	br.dptk.few	..L3			;; // B [parse_include.c: 277/7]

..L4:
//file/line/col parse_include.c/277/13
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L5			;; // B [parse_include.c: 277/13]

..L5:
//file/line/col parse_include.c/278/1
	add		r9 = @ltoff(__iob), r0	;; // A [parse_include.c: 278/1]
	add		r9 = r9, gp		;; // M [parse_include.c: 278/1]
	ld8		r10 = [r9]		   // M [parse_include.c: 278/1]
	add		r9 = @ltoff(.tcg0$rodata), r0 ;; // A [parse_include.c: 278/1]
	add		r9 = r9, gp		   // I [parse_include.c: 278/1]
	add		r11 = @pltoff(fprintf), r0 ;; // A [parse_include.c: 278/1]
	add		r11 = r11, gp		   // M [parse_include.c: 278/1]
	add		r14 = r0, gp		;; // I [parse_include.c: 278/1]
	add		out0 = 16, r10		   // I [parse_include.c: 278/1]
	ld8		r10 = [r9]		   // M [parse_include.c: 278/1]
	ld8		r9 = [r11]		   // M [parse_include.c: 278/1]
	add		r11 = 8, r11		;; // I [parse_include.c: 278/1]
	ld8		gp = [r11]		   // M [parse_include.c: 278/1]
	add		out1 = 244, r10		   // M [parse_include.c: 278/1]
	mov		b6 = r9			   // I [parse_include.c: 278/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 278/1] [UVU]
	add		gp = 0, r39		   // M [parse_include.c: 278/1]
//file/line/col parse_include.c/277/13
	add		r34 = 1, r34		   // I [parse_include.c: 277/13] [UVU: i] [UVuse]
//file/line/col parse_include.c/277/7
	addp4		r9 = 0, r33		;; // I [parse_include.c: 277/7] [UVuse]
	ld4		r9 = [r9]		;; // M [parse_include.c: 277/7]
	cmp4.lt		p6, p0 = r34, r9	   // M [parse_include.c: 277/7] [UVuse]
	nop.i		0			   // I
	nop.m		0			   // M
	nop.m		0			   // M
(p6)	br.dptk.few	..L5			;; // B [parse_include.c: 277/7]

..L6:
//file/line/col parse_include.c/278/13
	nop.m		0			   // M
	nop.m		0			   // M
	br.dptk.few	..L3			;; // B [parse_include.c: 278/13]

..L3:
//file/line/col parse_include.c/282/1
	add		r17 = 4, r33		   // M [parse_include.c: 282/1] [UVuse]
	add		r9 = @ltoff(__iob), r0	   // I [parse_include.c: 282/1]
	nop.i		0			;; // I
	add		r11 = r9, gp		   // M [parse_include.c: 282/1]
	addp4		r10 = 0, r33		   // M [parse_include.c: 282/1] [UVuse]
	add		r9 = @ltoff(.tcg0$rodata), r0 // I [parse_include.c: 282/1]
	add		r15 = @pltoff(fprintf), r0 // M [parse_include.c: 282/1]
	add		r14 = r0, r39		;; // I [parse_include.c: 282/1]
	add		r9 = r9, gp		   // I [parse_include.c: 282/1]
	ld4		r10 = [r10]		   // M [parse_include.c: 282/1,282]
	add		r16 = r15, gp		;; // I [parse_include.c: 282/1]
	add		r15 = 8, r16		   // I [parse_include.c: 282/1]
	ld8		r11 = [r11]		   // M [parse_include.c: 282/1]
	ld8		r16 = [r16]		   // M [parse_include.c: 282/1]
	shladd		r10 = r10, 2, r0	;; // I [parse_include.c: 282/1]
	ld8		r9 = [r9]		   // M [parse_include.c: 282/1]
	ld8		gp = [r15]		   // M [parse_include.c: 282/1]
	mov		b6 = r16		   // I [parse_include.c: 282/1]
	addp4		r10 = r10, r17		   // M [parse_include.c: 282/1]
	add		out0 = 16, r11		;; // I [parse_include.c: 282/1]
	add		out1 = 248, r9		   // I [parse_include.c: 282/1]
	ld4		out2 = [r10]		   // M [parse_include.c: 282/1]
	nop.m		0			   // M
	br.call.dptk.few rp = b6		;; // B [parse_include.c: 282/1] [UVU]
	add		gp = 0, r39		   // M [parse_include.c: 282/1]
//file/line/col parse_include.c/284/1
	mov		rp = r36		;; // I [parse_include.c: 284/1]
	mov		ar.pfs = r35		   // I [parse_include.c: 284/1]
	nop.m		0			   // M
	nop.m		0			   // M
	br.ret.dptk.few	rp			;; // B [parse_include.c: 284/1]

..L1:
//	$end					;; // A

	.endp	print_parse_info

// ===
	.section .IA_64.unwind = "a", "unwind"
	.align 4
	data4.ua @segrel(.text)
	data4.ua @segrel(.text+560)
	data4.ua @segrel(.tcg$unwind_info_block0005)

// ===
	.section .IA_64.unwind_info = "a", "progbits"
	.align 4
	data4.ua @segrel(.llo$annot_info_block0005)
.tcg$unwind_info_block0005:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x04
	data1	0x03, 0xe4, 0x02, 0xb0, 0xa4, 0xe6, 0x00, 0xb1
	data1	0x23, 0x61, 0x66, 0x81, 0xc0, 0x68, 0x00, 0x00

// ===


// ===
	.section .sdata = "asw", "progbits"
	.align 8
.tcg1$sdata:	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

// ===
	.section .rodata = "a", "progbits"
	.align 16
.tcg0$rodata:	stringz	"You must provide 1 argument, not %d\n"
	data1	0x00, 0x72, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00
	stringz	"Unable to open '%s'\n"
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00
	stringz	"Parsing error at line %d\n"
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	stringz	"Parsing error at line %d\n"
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	stringz	"Include depth of %d exceeded.  Abort.\n"
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00
	stringz	"Warning, end of output not at top include level.\n"
	data1	0x00, 0x00
	stringz	"  "
	data1	0x00
	stringz	"%s\n"
	data1	0x00, 0x00, 0x00, 0x00
	stringz	"%6d\t%6d\t%6d\t"

// ===
	.section .HP.opt_annot = "a", "annot"
	.align 8
.llo$annot_info_block0000:	data1	0x00, 0xac, 0x01, 0xbe, 0xff, 0xfe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xff, 0xa0, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0001:	data1	0x00, 0xac, 0x01, 0xfe, 0xff, 0xfe, 0xff, 0xff
	data1	0xff, 0xfc, 0x7f, 0xff, 0xec, 0xfe, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0002:	data1	0x00, 0xac, 0x01, 0xbe, 0xff, 0xbe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xff, 0xa0, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0003:	data1	0x00, 0xac, 0x01, 0xbe, 0xff, 0xbe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xff, 0x20, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0004:	data1	0x00, 0xac, 0x01, 0xbe, 0xff, 0xbe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xff, 0xa0, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
.llo$annot_info_block0005:	data1	0x00, 0xac, 0x01, 0xbe, 0xff, 0xbe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xfc, 0x20, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x03, 0x88, 0x02, 0x00, 0x1f
	data1	0x00

// ===
	.section .debug_procs_abbrev = "", "progbits"
	.align 1
$DEBUG_PROCS_ABBREV_CE0101_parse_include_c:	data1	0x03, 0x11, 0x01, 0x10, 0x06, 0x90, 0x40, 0x06
	data1	0x03, 0x08, 0x13, 0x0b, 0x95, 0x40, 0x0b, 0x25
	data1	0x08, 0x1b, 0x08, 0x01, 0x13, 0x00, 0x00, 0x07
	data1	0x2e, 0x01, 0x94, 0x40, 0x0b, 0x96, 0x40, 0x06
	data1	0x11, 0x01, 0x12, 0x01, 0x97, 0x40, 0x01, 0x98
	data1	0x40, 0x01, 0x00, 0x00, 0x00

// ===
	.section .debug_procs_info = "", "progbits"
	.align 1
	data1	0x00, 0x00, 0x00, 0xd3, 0x00, 0x02
	data4.ua @secrel($DEBUG_PROCS_ABBREV_CE0101_parse_include_c)
	data1	0x04, 0x03
	data4.ua @secrel($dbglline_CE0101_parse_include_c)
	data4.ua @secrel($dbgaline_CE0101_parse_include_c)
	stringz	"parse_include.c"
	data1	0x02, 0x01, 0x47, 0x00
	stringz	"/tmp_mnt/home/coulter/coulter/X"
	data1	0x00, 0x00, 0x00, 0x00, 0x07, 0x01, 0x00, 0x00
	data1	0x00, 0x00
	data4.ua .text
	data4.ua .text+256
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data4.ua .text
	data4.ua .text+80
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data4.ua .text
	data4.ua .text+272
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data4.ua .text
	data4.ua .text+1664
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data4.ua .text
	data4.ua .text+6832
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00
	data4.ua .text
	data4.ua .text+560
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00

// ===
	.section .debug_line = "", "progbits"
	.align 8
$dbglline_CE0101_parse_include_c:	data1	0x00, 0x00, 0x05, 0x3c, 0x00, 0x02, 0x00, 0x00
	data1	0x00, 0x10, 0x04, 0x01, 0x00, 0x05, 0x0a, 0x01
	data1	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01
	data1	0x00, 0x00, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x14, 0x03
	stringz	"parse_include.c"
	data1	0x00, 0x00, 0x00, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x04, 0x01, 0x05, 0x01, 0x03, 0x39, 0x01, 0x06
	data1	0x50, 0x06, 0x05, 0x03, 0x0e, 0x05, 0x01
	stringz	"3.3["
	data1	0x05, 0x02
	data4.ua .text
	data1	0x03, 0x08, 0x01, 0x06, 0x23, 0x06, 0x05, 0x05
	data1	0x0b, 0x1f, 0x05, 0x01, 0x25, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x03, 0x09, 0x01, 0x06, 0x3c, 0x06, 0x0b, 0x2a
	data1	0x88, 0x1b, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x03, 0x09, 0x01, 0x06, 0x50, 0x06, 0x05, 0x03
	data1	0x03, 0x05, 0x01, 0x05, 0x01, 0x83, 0x2a, 0x88
	data1	0x05, 0x05, 0x1b, 0x24, 0x05, 0x03, 0x25, 0x05
	data1	0x01, 0x52, 0x06, 0x00, 0x02, 0x16, 0x01, 0x1e
	data1	0x06, 0x34, 0x34, 0x89, 0x1b, 0x05, 0x0a, 0x29
	data1	0x05, 0x01, 0x42, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x1e, 0x06, 0x05, 0x02, 0x1b, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x0a, 0x06, 0x05, 0x01, 0x34, 0x06
	data1	0x05, 0x02, 0x33, 0x06, 0x05, 0x01, 0x70, 0xc6
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x19, 0x06, 0x21
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x0a, 0x06, 0x72
	data1	0x88, 0x33, 0x6d, 0xb1, 0x05, 0x03, 0x02, 0x03
	data1	0x03, 0x5b, 0x01, 0x05, 0x01, 0x02, 0x0c, 0x03
	data1	0x28, 0x01, 0x20, 0x34, 0x88, 0x1b, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x0a, 0x06, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x03, 0x1e, 0x01, 0x06, 0x3c, 0x06, 0x05, 0x04
	data1	0x0d, 0x05, 0x05, 0x1a, 0x05, 0x03, 0x02, 0x01
	data1	0x03, 0x15, 0x01, 0x05, 0x01, 0x2a, 0x05, 0x09
	data1	0x1e, 0x06, 0x05, 0x1f, 0x5a, 0x06, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x8c, 0x06, 0x05, 0x15, 0x14
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x0a, 0x06, 0x05
	data1	0x17, 0x5a, 0x05, 0x01, 0x23, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x19, 0x06, 0x1f, 0x34, 0x05, 0x02
	data1	0x1f, 0x05, 0x09, 0x1e, 0x05, 0x0e, 0x37, 0x05
	data1	0x19, 0x19, 0x06, 0x05, 0x2f, 0x5a, 0x06, 0x06
	data1	0x00, 0x02, 0x16, 0x01, 0x8c, 0x06, 0x05, 0x25
	data1	0x14, 0x06, 0x00, 0x02, 0x16, 0x01, 0x0a, 0x06
	data1	0x05, 0x27, 0x5a, 0x05, 0x13, 0x23, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x19, 0x06, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x05, 0x3d, 0x1e, 0x06, 0x05, 0xc1
	data1	0x00, 0x1e, 0x05, 0x19, 0x1e, 0x06, 0x05, 0x2f
	data1	0x5a, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x8c
	data1	0x06, 0x05, 0x25, 0x14, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x0a, 0x06, 0x05, 0x27, 0x5a, 0x05, 0x13
	data1	0x23, 0x06, 0x00, 0x02, 0x16, 0x01, 0x19, 0x06
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x05, 0x3d, 0x1e
	data1	0x06, 0x05, 0xc1, 0x00, 0x1e, 0x05, 0xc2, 0x00
	data1	0x1e, 0x05, 0xcb, 0x00, 0x1e, 0x05, 0xce, 0x00
	data1	0x32, 0x05, 0xd3, 0x00, 0x37, 0x05, 0x06, 0x02
	data1	0x03, 0x03, 0x05, 0x01, 0x06, 0x05, 0x1c, 0x5a
	data1	0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x8c, 0x06
	data1	0x05, 0x12, 0x14, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x0a, 0x06, 0x05, 0x14, 0x5a, 0x05, 0x06, 0x23
	data1	0x05, 0x01, 0x1a, 0x05, 0x02, 0x1f, 0x05, 0x09
	data1	0x1e, 0x05, 0x0e, 0x37, 0x05, 0x19, 0x19, 0x06
	data1	0x05, 0x2f, 0x5a, 0x06, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x8c, 0x06, 0x05, 0x25, 0x14, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x0a, 0x06, 0x05, 0x27, 0x5a
	data1	0x05, 0x13, 0x23, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x19, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x05
	data1	0x3d, 0x1e, 0x06, 0x05, 0xc1, 0x00, 0x1e, 0x05
	data1	0x19, 0x1e, 0x06, 0x05, 0x2f, 0x5a, 0x06, 0x06
	data1	0x00, 0x02, 0x16, 0x01, 0x8c, 0x06, 0x05, 0x25
	data1	0x14, 0x06, 0x00, 0x02, 0x16, 0x01, 0x0a, 0x06
	data1	0x05, 0x27, 0x5a, 0x05, 0x13, 0x23, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x19, 0x06, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x05, 0x3d, 0x1e, 0x06, 0x05, 0xc1
	data1	0x00, 0x1e, 0x05, 0xc2, 0x00, 0x1e, 0x05, 0xcb
	data1	0x00, 0x1e, 0x05, 0xce, 0x00, 0x32, 0x05, 0xd3
	data1	0x00, 0x37, 0x05, 0x03, 0x1a, 0x10, 0x05, 0x09
	data1	0x10, 0x06, 0x05, 0x1f, 0x64, 0x06, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x8c, 0x06, 0x05, 0x15, 0x14
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x0a, 0x06, 0x05
	data1	0x17, 0x5a, 0x05, 0x03, 0x23, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x19, 0x06, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x05, 0x02, 0x1f, 0x06, 0x05, 0x03, 0x1f
	data1	0x05, 0x09, 0x02, 0x0a, 0x03, 0x7e, 0x01, 0x06
	data1	0x05, 0x1f, 0x64, 0x06, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x8c, 0x06, 0x05, 0x15, 0x14, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x0a, 0x06, 0x05, 0x17, 0x5a
	data1	0x05, 0x03, 0x23, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x19, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x05
	data1	0x02, 0x1f, 0x06, 0x05, 0x0a, 0x1f, 0x05, 0x01
	data1	0x1f, 0x05, 0x02, 0x1f, 0x05, 0x09, 0x1e, 0x05
	data1	0x0e, 0x37, 0x05, 0x19, 0x19, 0x06, 0x05, 0x2f
	data1	0x5a, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x8c
	data1	0x06, 0x05, 0x25, 0x14, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x0a, 0x06, 0x05, 0x27, 0x5a, 0x05, 0x13
	data1	0x23, 0x06, 0x00, 0x02, 0x16, 0x01, 0x19, 0x06
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x05, 0x3d, 0x1e
	data1	0x06, 0x05, 0xc1, 0x00, 0x1e, 0x05, 0x19, 0x1e
	data1	0x06, 0x05, 0x2f, 0x5a, 0x06, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x8c, 0x06, 0x05, 0x25, 0x14, 0x06
	data1	0x00, 0x02, 0x16, 0x01, 0x0a, 0x06, 0x05, 0x27
	data1	0x5a, 0x05, 0x13, 0x23, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x19, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x05, 0x3d, 0x1e, 0x06, 0x05, 0xc1, 0x00, 0x1e
	data1	0x05, 0xc2, 0x00, 0x1e, 0x05, 0xcb, 0x00, 0x1e
	data1	0x05, 0xce, 0x00, 0x32, 0x05, 0xd3, 0x00, 0x37
	data1	0x05, 0x06, 0x1a, 0x06, 0x05, 0x1c, 0x5a, 0x06
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x8c, 0x06, 0x05
	data1	0x12, 0x14, 0x06, 0x00, 0x02, 0x16, 0x01, 0x0a
	data1	0x06, 0x05, 0x14, 0x5a, 0x05, 0x06, 0x23, 0x05
	data1	0x01, 0x1a, 0x05, 0x02, 0x1f, 0x05, 0x09, 0x1e
	data1	0x05, 0x0e, 0x37, 0x05, 0x19, 0x19, 0x06, 0x05
	data1	0x2f, 0x5a, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x8c, 0x06, 0x05, 0x25, 0x14, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x0a, 0x06, 0x05, 0x27, 0x5a, 0x05
	data1	0x13, 0x23, 0x06, 0x00, 0x02, 0x16, 0x01, 0x19
	data1	0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x05, 0x3d
	data1	0x1e, 0x06, 0x05, 0xc1, 0x00, 0x1e, 0x05, 0x19
	data1	0x1e, 0x06, 0x05, 0x2f, 0x5a, 0x06, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x8c, 0x06, 0x05, 0x25, 0x14
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x0a, 0x06, 0x05
	data1	0x27, 0x5a, 0x05, 0x13, 0x23, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x19, 0x06, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x05, 0x3d, 0x1e, 0x06, 0x05, 0xc1, 0x00
	data1	0x1e, 0x05, 0xc2, 0x00, 0x1e, 0x05, 0xcb, 0x00
	data1	0x1e, 0x05, 0xce, 0x00, 0x32, 0x05, 0xd3, 0x00
	data1	0x37, 0x05, 0x03, 0x1a, 0x05, 0x09, 0x10, 0x06
	data1	0x05, 0x1f, 0x55, 0x06, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x8c, 0x06, 0x05, 0x15, 0x14, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x0a, 0x06, 0x05, 0x17, 0x5a
	data1	0x05, 0x03, 0x23, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x19, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x05
	data1	0x02, 0x1f, 0x06, 0x05, 0x01, 0x20, 0x06, 0x00
	data1	0x02, 0x16, 0x01, 0x1e, 0x06, 0x05, 0x03, 0x34
	data1	0x4c, 0x05, 0x01, 0x8a, 0x05, 0x07, 0x02, 0x02
	data1	0x03, 0x7f, 0x01, 0x05, 0x03, 0x28, 0x05, 0x09
	data1	0x02, 0x02, 0x03, 0x78, 0x01, 0x06, 0x05, 0x1f
	data1	0x6e, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x8c
	data1	0x06, 0x05, 0x15, 0x14, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x0a, 0x06, 0x05, 0x17, 0x5a, 0x05, 0x03
	data1	0x23, 0x06, 0x00, 0x02, 0x16, 0x01, 0x19, 0x06
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x05, 0x02, 0x1f
	data1	0x06, 0x05, 0x01, 0x02, 0x04, 0x03, 0x08, 0x01
	data1	0x1f, 0x05, 0x02, 0x1f, 0x05, 0x09, 0x1e, 0x05
	data1	0x0e, 0x37, 0x05, 0x19, 0x19, 0x06, 0x05, 0x2f
	data1	0x5a, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01, 0x8c
	data1	0x06, 0x05, 0x25, 0x14, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x0a, 0x06, 0x05, 0x27, 0x5a, 0x05, 0x13
	data1	0x23, 0x06, 0x00, 0x02, 0x16, 0x01, 0x19, 0x06
	data1	0x06, 0x00, 0x02, 0x16, 0x01, 0x05, 0x3d, 0x1e
	data1	0x06, 0x05, 0xc1, 0x00, 0x1e, 0x05, 0x19, 0x1e
	data1	0x06, 0x05, 0x2f, 0x5a, 0x06, 0x06, 0x00, 0x02
	data1	0x16, 0x01, 0x8c, 0x06, 0x05, 0x25, 0x14, 0x06
	data1	0x00, 0x02, 0x16, 0x01, 0x0a, 0x06, 0x05, 0x27
	data1	0x5a, 0x05, 0x13, 0x23, 0x06, 0x00, 0x02, 0x16
	data1	0x01, 0x19, 0x06, 0x06, 0x00, 0x02, 0x16, 0x01
	data1	0x05, 0x3d, 0x1e, 0x06, 0x05, 0xc1, 0x00, 0x1e
	data1	0x05, 0xc2, 0x00, 0x1e, 0x05, 0xcb, 0x00, 0x1e
	data1	0x05, 0xce, 0x00, 0x32, 0x05, 0xd3, 0x00, 0x37
	data1	0x05, 0x06, 0x1a, 0x05, 0x04, 0x38, 0x05, 0x05
	data1	0x1f, 0x05, 0x01, 0x1f, 0x05, 0x03, 0x02, 0x17
	data1	0x03, 0x58, 0x01, 0x05, 0x01, 0x02, 0x04, 0x03
	data1	0x29, 0x01, 0x1f, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x03, 0x08, 0x01, 0x06, 0x32, 0x06, 0x0d, 0x05
	data1	0x05, 0x02, 0x21, 0x03, 0x06, 0x01, 0x05, 0x07
	data1	0x0f, 0x05, 0x0d, 0x3c, 0x05, 0x01, 0x1f, 0x05
	data1	0x0d, 0x02, 0x1d, 0x03, 0x7f, 0x01, 0x05, 0x07
	data1	0x0f, 0x05, 0x0d, 0x3d, 0x05, 0x01, 0x22, 0xb1
	data1	0x02, 0x04, 0x00, 0x01, 0x01

// ===
	.section .debug_actual = "", "progbits"
	.align 8
$dbgaline_CE0101_parse_include_c:	data1	0x00, 0x00, 0x03, 0xe3, 0x00, 0x02, 0x00, 0x00
	data1	0x00, 0x0e, 0x04, 0x01, 0x00, 0x05, 0x0a, 0x01
	data1	0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01
	data1	0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x0f
	data1	0x00, 0x01, 0x11, 0x2d, 0x00, 0x01, 0x11, 0x14
	data1	0x00, 0x01, 0x11, 0x14, 0x16, 0x00, 0x01, 0x11
	data1	0x1e, 0x00, 0x01, 0x11, 0x19, 0x10, 0x00, 0x01
	data1	0x11, 0x1e, 0x1a, 0x00, 0x01, 0x11, 0x23, 0x1a
	data1	0x00, 0x01, 0x11, 0x4b, 0x1a, 0x00, 0x01, 0x11
	data1	0x19, 0x00, 0x01, 0x18, 0x28, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x1e
	data1	0x11, 0x00, 0x01, 0x11, 0x19, 0x10, 0x00, 0x01
	data1	0x11, 0x1e, 0x00, 0x01, 0x18, 0x24, 0x00, 0x05
	data1	0x02
	data4.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x32
	data1	0x00, 0x01, 0x11, 0x0f, 0x11, 0x29, 0x00, 0x01
	data1	0x11, 0x78, 0x1a, 0x00, 0x01, 0x11, 0x0f, 0x15
	data1	0x00, 0x01, 0x11, 0x50, 0x00, 0x01, 0x18, 0x32
	data1	0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x0f
	data1	0x00, 0x01, 0x11, 0x2d, 0x00, 0x01, 0x11, 0x14
	data1	0x00, 0x01, 0x11, 0x14, 0x16, 0x00, 0x01, 0x11
	data1	0x6e, 0x00, 0x01, 0x11, 0x19, 0x10, 0x29, 0x00
	data1	0x01, 0x11, 0x78, 0x1a, 0x00, 0x01, 0x11, 0x0f
	data1	0x15, 0x00, 0x01, 0x11, 0x1e, 0x10, 0x00, 0x01
	data1	0x11, 0x1e, 0x10, 0x00, 0x01, 0x11, 0x1e, 0x00
	data1	0x01, 0x11, 0x1e
	stringz	"3>3"
	data1	0x01, 0x11, 0x78, 0x1a, 0x00, 0x01, 0x11, 0x0f
	data1	0x15, 0x00, 0x01, 0x11, 0x23, 0x10, 0x00, 0x01
	data1	0x11, 0x3c, 0x10, 0x00, 0x01, 0x11, 0x0f
	stringz	"*4"
	data1	0x01, 0x11
	stringz	"x4"
	data1	0x01, 0x11, 0xaa, 0x1f, 0x2a, 0x00, 0x01, 0x11
	stringz	"P*"
	data1	0x01, 0x11, 0x78, 0x1a, 0x00, 0x01, 0x11, 0x2d
	data1	0x10, 0x00, 0x01, 0x11
	stringz	"K)"
	data1	0x01, 0x11, 0xa0, 0x1f, 0x15, 0x00, 0x01, 0x11
	data1	0x14, 0x00, 0x01, 0x11, 0x1e, 0x33, 0x15, 0x33
	data1	0x00, 0x01, 0x11, 0x78, 0x1a, 0x00, 0x01, 0x11
	data1	0x0f, 0x00, 0x01, 0x11, 0x15, 0x00, 0x01, 0x18
	data1	0x28, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x01, 0x11, 0x0c, 0x00, 0x01, 0x11, 0x32
	data1	0x00, 0x01, 0x11, 0x0f, 0x11, 0x00, 0x01, 0x11
	data1	0x14, 0x00, 0x01, 0x11, 0x10, 0x10, 0x33, 0x15
	data1	0x00, 0x01, 0x11, 0x46, 0x00, 0x01, 0x11, 0x8c
	data1	0x00, 0x01, 0x11, 0x19, 0x1c, 0x00, 0x01, 0x11
	data1	0x4b, 0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15, 0x2a
	data1	0x00, 0x01, 0x18, 0x28, 0x15, 0x1f, 0x1f, 0x00
	data1	0x01, 0x11, 0x32, 0x15, 0x15, 0x00, 0x01, 0x11
	data1	0x46, 0x00, 0x01, 0x11, 0x8c, 0x00, 0x01, 0x11
	data1	0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b, 0x00
	data1	0x01, 0x11, 0x1e, 0x15, 0x49, 0x15, 0x00, 0x01
	data1	0x11, 0x46, 0x00, 0x01, 0x11, 0x8c, 0x00, 0x01
	data1	0x11, 0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b
	data1	0x00, 0x01, 0x11, 0x1e, 0x15, 0x49, 0x15, 0x1f
	data1	0x00, 0x01, 0x18, 0x28, 0x15, 0x00, 0x01, 0x11
	data1	0x32, 0x15, 0x15, 0x00, 0x01, 0x11, 0x46, 0x00
	data1	0x01, 0x11, 0x8c, 0x00, 0x01, 0x11, 0x19, 0x1c
	data1	0x00, 0x01, 0x11, 0x4b, 0x1b, 0x00, 0x01, 0x11
	data1	0x1e, 0x15, 0x15, 0x1f, 0x1f, 0x00, 0x01, 0x11
	data1	0x32, 0x15, 0x15, 0x00, 0x01, 0x11, 0x46, 0x00
	data1	0x01, 0x11, 0x8c, 0x00, 0x01, 0x11, 0x19, 0x1c
	data1	0x00, 0x01, 0x11, 0x4b, 0x1b, 0x00, 0x01, 0x11
	data1	0x1e, 0x15, 0x49, 0x15, 0x00, 0x01, 0x11, 0x46
	data1	0x00, 0x01, 0x11, 0x8c, 0x00, 0x01, 0x11, 0x19
	data1	0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b, 0x00, 0x01
	data1	0x11, 0x1e, 0x15, 0x49, 0x15, 0x1f, 0x00, 0x01
	data1	0x18, 0x28, 0x15, 0x00, 0x01, 0x11, 0x32, 0x15
	data1	0x00, 0x01, 0x11, 0x15, 0x00, 0x01, 0x11, 0x10
	data1	0x10, 0x00, 0x01, 0x11, 0x50, 0x00, 0x01, 0x11
	data1	0x8c, 0x00, 0x01, 0x11, 0x19, 0x1c, 0x00, 0x01
	data1	0x11, 0x4b, 0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15
	data1	0x49, 0x00, 0x01, 0x11, 0x2d, 0x10, 0x00, 0x01
	data1	0x11, 0x50, 0x00, 0x01, 0x11, 0x8c, 0x00, 0x01
	data1	0x11, 0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b
	data1	0x00, 0x01, 0x11, 0x1e, 0x15, 0x49, 0x15, 0x1f
	data1	0x1f, 0x00, 0x01, 0x11, 0x32, 0x15, 0x15, 0x00
	data1	0x01, 0x11, 0x46, 0x00, 0x01, 0x11, 0x8c, 0x00
	data1	0x01, 0x11, 0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b
	data1	0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15, 0x49, 0x15
	data1	0x00, 0x01, 0x11, 0x46, 0x00, 0x01, 0x11, 0x8c
	data1	0x00, 0x01, 0x11, 0x19, 0x1c, 0x00, 0x01, 0x11
	data1	0x4b, 0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15, 0x49
	data1	0x15, 0x1f, 0x00, 0x01, 0x18, 0x28, 0x15, 0x00
	data1	0x01, 0x11, 0x32, 0x15, 0x15, 0x00, 0x01, 0x11
	data1	0x46, 0x00, 0x01, 0x11, 0x8c, 0x00, 0x01, 0x11
	data1	0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b, 0x00
	data1	0x01, 0x11, 0x1e, 0x15, 0x15, 0x1f, 0x1f, 0x00
	data1	0x01, 0x11, 0x32, 0x15, 0x15, 0x00, 0x01, 0x11
	data1	0x46, 0x00, 0x01, 0x11, 0x8c, 0x00, 0x01, 0x11
	data1	0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b, 0x00
	data1	0x01, 0x11, 0x1e, 0x15, 0x49, 0x15, 0x00, 0x01
	data1	0x11, 0x46, 0x00, 0x01, 0x11, 0x8c, 0x00, 0x01
	data1	0x11, 0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b, 0x1b
	data1	0x00, 0x01, 0x11, 0x1e, 0x15, 0x49, 0x15, 0x1f
	data1	0x00, 0x01, 0x18, 0x28, 0x15, 0x00, 0x01, 0x11
	data1	0x32, 0x15, 0x00, 0x01, 0x11, 0x15, 0x10, 0x00
	data1	0x01, 0x11, 0x41, 0x00, 0x01, 0x11, 0x8c, 0x00
	data1	0x01, 0x11, 0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b
	data1	0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15
	stringz	"I>"
	data1	0x01, 0x11, 0x46, 0x10, 0x00, 0x01, 0x11, 0x73
	data1	0x00, 0x01, 0x11, 0x14, 0x15, 0x15, 0x00, 0x01
	data1	0x11, 0x23, 0x00, 0x01, 0x11, 0x10, 0x15, 0x00
	data1	0x01, 0x11, 0x5a, 0x00, 0x01, 0x11, 0x8c, 0x00
	data1	0x01, 0x11, 0x19, 0x1c, 0x00, 0x01, 0x11, 0x4b
	data1	0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15, 0x49, 0x15
	data1	0x1f, 0x1f, 0x00, 0x01, 0x11, 0x32, 0x15, 0x15
	data1	0x00, 0x01, 0x11, 0x46, 0x00, 0x01, 0x11, 0x8c
	data1	0x00, 0x01, 0x11, 0x19, 0x1c, 0x00, 0x01, 0x11
	data1	0x4b, 0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15, 0x49
	data1	0x15, 0x00, 0x01, 0x11, 0x46, 0x00, 0x01, 0x11
	data1	0x8c, 0x00, 0x01, 0x11, 0x19, 0x1c, 0x00, 0x01
	data1	0x11, 0x4b, 0x1b, 0x00, 0x01, 0x11, 0x1e, 0x15
	data1	0x49, 0x15, 0x1f, 0x00, 0x01, 0x18, 0x28, 0x15
	data1	0x00, 0x01, 0x11, 0x32, 0x15, 0x15, 0x00, 0x01
	data1	0x11, 0x32, 0x10, 0x00, 0x01, 0x11, 0x19, 0x10
	data1	0x00, 0x01, 0x11, 0x19, 0x10, 0x00, 0x01, 0x11
	data1	0x4b, 0x00, 0x01, 0x18, 0x32, 0x15, 0x29, 0x1a
	data1	0x00, 0x01, 0x18, 0x23, 0x00, 0x05, 0x02
	data4.ua .text
	data1	0x00, 0x01, 0x11, 0x0b, 0x00, 0x01, 0x11, 0x28
	data1	0x16, 0x00, 0x01, 0x11, 0xa0, 0x00, 0x01, 0x11
	data1	0x1a, 0x10, 0x47, 0x15, 0x00, 0x01, 0x11, 0x8c
	data1	0x00, 0x01, 0x11, 0x1a, 0x10, 0x47, 0x15, 0x00
	data1	0x01, 0x11, 0xa0, 0x1a, 0x00, 0x01, 0x18, 0x23
	data1	0x02, 0x04, 0x00, 0x01, 0x01

// ===
	.section .note = "", "note"
	.align 4
	data1	0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x01, 0xfd
	data1	0x00, 0x00, 0x00, 0x01
	stringz	"HP"
	data1	0x00
	stringz	"A.05.50 [Jan 06 2003]\nctcom options = -assembly on -ia64abi all -architecture 32 -inst compiletime -diags 523 -target_os 11.23 -ext on -lang c -exception off -inline_power 1 -link_type dynamic -fpeval float -tls_dyn on -plusolistoption -Ol04const! -plusolistoption -Ol11aggressive! -plusooption -Oq01,al,ag,cn,sz,ic,vo,Mf,Po,es,rs,Rf,Pr,sp,in,cl,om,vc,pi,fa,Pe,rr,pa,pv,nf,cp,lx,Pg,ug,lu,lb,uj,dn,sg,pt,kt,em,np,ar,rp,fs,bp,wp,pc,mp,lr,cx,cr,pi,so,Rc,fa,ft,fe,ap,st,lc,Bl,ib,sd,ll,rl,Lt,ol,fl,lm,ts,rd,dp,If!"
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00
	data1	0x00, 0x00, 0x3f, 0x00, 0x00, 0x00, 0x04
	stringz	"HP"
	data1	0x00
	stringz	"parse_include.c\n/tmp_mnt/home/coulter/coulter/X\nANSI C 32 bits"
	data1	0x00
