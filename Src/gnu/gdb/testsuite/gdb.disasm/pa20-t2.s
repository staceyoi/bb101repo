	.level	2.0N

        .export main,ENTRY
        .export mainend,ENTRY
        .space $TEXT$
        .subspa $CODE$

	.import  word0,data
	.import  word1,data
	.import	 test,code

	.data

	.double 	0.1234
	.dword		0x0123456789abcdef
	.dword		0xfedcba9876543210

	.code

main
        .proc
        .callinfo NO_CALLS,FRAME=0
        .entry

    
MASK10
	SSM	0x001,%r1
	RSM	0x002,%r1
	SSM	0x004,%r1
	RSM	0x008,%r1
	SSM	0x010,%r1
	RSM	0x020,%r1
	SSM	0x040,%r1
	RSM	0x080,%r1
	SSM	0x100,%r1
	RSM	0x200,%r1
	SSM	0x3ff,%r1
	RSM	0x000,%r1

VM_bug
	DEPW,<>		%r14,%cr11,20,%r7
	DEPW		%r14,%cr11,20,%r7
	DEPWI,<>	14,%cr11,20,%r7
	DEPWI		14,%cr11,20,%r7
	EXTRW,<>	%r14,%cr11,20,%r7
	EXTRW		%r14,%cr11,20,%r7
	EXTRD,*<>	%r14,%cr11,20,%r7
	EXTRD,<>	%r14,%cr11,20,%r7
	EXTRD		%r14,%cr11,20,%r7
CLDST
	b	CLDST
	CLDD,0,S,SL	%r5(%sr2,%r6),6
	CLDD,1,MA,SL	8(%sr2,%r7),7
	CLDD,2,M	%r8(%sr3,%r9),8
	CLDD,3,MB	-8(%r10),9
	CLDD,4,SL	%r11(%r12),10
	CLDD,5,SL	0(%r13),11
	CLDD,6		%r14(%r15),12
	CLDD,7		8(%r15),13

	CLDW,0,S,SL	%r5(%sr2,%r6),14
	CLDW,1,MA,SL	8(%sr2,%r7),15
	CLDW,2,M	%r8(%sr3,%r9),16
	CLDW,3,MB	-8(%r10),17
	CLDW,4,SL	%r11(%r12),18
	CLDW,5,SL	0(%r13),19
	CLDW,6		%r14(%r15),20
	CLDW,7		8(%r15),21

	CSTD,0,S,SL	6,%r5(%sr2,%r6)
	CSTD,1,MA,SL	7,8(%sr2,%r7)
	CSTD,2,M	8,%r8(%sr3,%r9)
	CSTD,3,MB	9,-8(%r10)
	CSTD,4,BC	10,%r11(%r12)
	CSTD,5,SL	11,0(%r13)
	CSTD,6		12,%r14(%r15)
	CSTD,7		13,8(%r15)

	CSTW,0,S,SL	14,%r5(%sr2,%r6)
	CSTW,1,MA,SL	15,8(%sr2,%r7)
	CSTW,2,M	16,%r8(%sr3,%r9)
	CSTW,3,MB	17,-8(%r10)
	CSTW,4,SL	18,%r11(%r12)
	CSTW,5,BC	19,0(%r13)
	CSTW,6		20,%r14(%r15)
	CSTW,7		21,8(%r15)
LDtest
	b		LDtest		
	LDD		-24(%r12),%r31
	LDD		-16(%r12),%r31
	LDD		-8(%r12),%r31
	LDD		0(%r12),%r31
	LDD		(%r12),%r31
	LDD		%r6(%r12),%r31
	LDD		8(%r12),%r31
	LDD		16(%r12),%r31
	LDD		24(%r12),%r31
;
	LDW		-24(%r12),%r31
	LDW		-16(%r12),%r31
	LDW		-8(%r12),%r31
	LDW		0(%r12),%r31
	LDW		(%r12),%r31
	LDW		%r6(%r12),%r31
	LDW		8(%r12),%r31
	LDW		16(%r12),%r31
	LDW		24(%r12),%r31
;
	LDH		-24(%r12),%r31
	LDH		-16(%r12),%r31
	LDH		-8(%r12),%r31
	LDH		0(%r12),%r31
	LDH		(%r12),%r31
	LDH		%r6(%r12),%r31
	LDH		8(%r12),%r31
	LDH		16(%r12),%r31
	LDH		24(%r12),%r31
;
	LDB		-24(%r12),%r31
	LDB		-16(%r12),%r31
	LDB		-8(%r12),%r31
	LDB		0(%r12),%r31
	LDB		(%r12),%r31
	LDB		%r6(%r12),%r31
	LDB		8(%r12),%r31
	LDB		16(%r12),%r31
	LDB		24(%r12),%r31
;
;	LDDA		-24(%r12),%r31	; Error
	LDDA		-16(%r12),%r31
	LDDA		-8(%r12),%r31
	LDDA		0(%r12),%r31
	LDDA		(%r12),%r31
	LDDA		%r6(%r12),%r31
	LDDA		8(%r12),%r31
;	LDDA		16(%r12),%r31	; Error
;	LDDA		24(%r12),%r31	; Error
;
;	LDWA		-24(%r12),%r31	; Error
	LDWA		-16(%r12),%r31
	LDWA		-8(%r12),%r31
	LDWA		0(%r12),%r31
	LDWA		(%r12),%r31
	LDWA		%r6(%r12),%r31
	LDWA		8(%r12),%r31
;	LDWA		16(%r12),%r31	; Error
;	LDWA		24(%r12),%r31	; Error
;
;	LDCD		-24(%r12),%r31	; Error
	LDCD		-16(%r12),%r31
	LDCD		-8(%r12),%r31
	LDCD		0(%r12),%r31
	LDCD		(%r12),%r31
	LDCD		%r6(%r12),%r31
	LDCD		8(%r12),%r31
;	LDCD		16(%r12),%r31	; Error
;	LDCD		24(%r12),%r31	; Error
;
;	LDCW		-24(%r12),%r31	; Error
	LDCW		-16(%r12),%r31
	LDCW		-8(%r12),%r31
	LDCW		0(%r12),%r31
	LDCW		(%r12),%r31
	LDCW		%r6(%r12),%r31
	LDCW		8(%r12),%r31
;	LDCW		16(%r12),%r31	; Error
;	LDCW		24(%r12),%r31	; Error
;
LDtest_with_MA
	B		LDtest_with_MA
;
	LDD,MA		-24(%r12),%r31
	LDD,MA		-16(%r12),%r31
	LDD,MA		-8(%r12),%r31
;	LDD,MA		0(%r12),%r31
;	LDD,MA		(%r12),%r31	; Error
;	LDD,MA		%r6(%r12),%r31	; Error
	LDD,MA		8(%r12),%r31
	LDD,MA		16(%r12),%r31
	LDD,MA		24(%r12),%r31
;
	LDW,MA		-24(%r12),%r31
	LDW,MA		-16(%r12),%r31
	LDW,MA		-8(%r12),%r31
;	LDW,MA		0(%r12),%r31	; Error
;	LDW,MA		(%r12),%r31	; Error
;	LDW,MA		%r6(%r12),%r31	; Error
	LDW,MA		8(%r12),%r31
	LDW,MA		16(%r12),%r31
	LDW,MA		24(%r12),%r31
;
;	LDH,MA		-24(%r12),%r31	; error
	LDH,MA		-16(%r12),%r31
	LDH,MA		-8(%r12),%r31
;	LDH,MA		0(%r12),%r31	; Error
;	LDH,MA		(%r12),%r31	; Error
;	LDH,MA		%r6(%r12),%r31	; Error
	LDH,MA		8(%r12),%r31
;	LDH,MA		16(%r12),%r31	; error
;	LDH,MA		24(%r12),%r31	; error
;
;	LDB,MA		-24(%r12),%r31	; error
	LDB,MA		-16(%r12),%r31
	LDB,MA		-8(%r12),%r31
;	LDB,MA		0(%r12),%r31	; Error
;	LDB,MA		(%r12),%r31	; Error
;	LDB,MA		%r6(%r12),%r31	; Error
	LDB,MA		8(%r12),%r31
;	LDB,MA		16(%r12),%r31	; error
;	LDB,MA		24(%r12),%r31	; error
;
;	LDDA,MA		-24(%r12),%r31	; Error
	LDDA,MA		-16(%r12),%r31
	LDDA,MA		-8(%r12),%r31
;	LDDA,MA		0(%r12),%r31	; Error
;	LDDA,MA		(%r12),%r31	; Error
;	LDDA,MA		%r6(%r12),%r31	; Error
	LDDA,MA		8(%r12),%r31
;	LDDA,MA		16(%r12),%r31	; Error
;	LDDA,MA		24(%r12),%r31	; Error
;
;	LDWA,MA		-24(%r12),%r31	; Error
	LDWA,MA		-16(%r12),%r31
	LDWA,MA		-8(%r12),%r31
;	LDWA,MA		0(%r12),%r31	; Error
;	LDWA,MA		(%r12),%r31	; Error
;	LDWA,MA		%r6(%r12),%r31	; Error
	LDWA,MA		8(%r12),%r31
;	LDWA,MA		16(%r12),%r31	; Error
;	LDWA,MA		24(%r12),%r31	; Error
;
;	LDCD,MA		-24(%r12),%r31	; Error
	LDCD,MA		-16(%r12),%r31
	LDCD,MA		-8(%r12),%r31
;	LDCD,MA		0(%r12),%r31	; Error
;	LDCD,MA		(%r12),%r31	; Error
;	LDCD,MA		%r6(%r12),%r31	; Error
	LDCD,MA		8(%r12),%r31
;	LDCD,MA		16(%r12),%r31	; Error
;	LDCD,MA		24(%r12),%r31	; Error
;
;	LDCW,MA		-24(%r12),%r31	; Error
	LDCW,MA		-16(%r12),%r31
	LDCW,MA		-8(%r12),%r31
;	LDCW,MA		0(%r12),%r31	; Error
;	LDCW,MA		(%r12),%r31	; Error
;	LDCW,MA		%r6(%r12),%r31	; Error
	LDCW,MA		8(%r12),%r31
;	LDCW,MA		16(%r12),%r31	; Error
;	LDCW,MA		24(%r12),%r31	; Error
;
LDtest_with_MB
	B		LDtest_with_MB
;
	LDD,MB		-24(%r12),%r31
	LDD,MB		-16(%r12),%r31
	LDD,MB		-8(%r12),%r31
	LDD,MB		0(%r12),%r31
	LDD,MB		(%r12),%r31
;	LDD,MB		%r6(%r12),%r31	; Error
	LDD,MB		8(%r12),%r31
	LDD,MB		16(%r12),%r31
	LDD,MB		24(%r12),%r31
;
	LDW,MB		-24(%r12),%r31
	LDW,MB		-16(%r12),%r31
	LDW,MB		-8(%r12),%r31
	LDW,MB		0(%r12),%r31
	LDW,MB		(%r12),%r31
;	LDW,MB		%r6(%r12),%r31	; Error
	LDW,MB		8(%r12),%r31
	LDW,MB		16(%r12),%r31
	LDW,MB		24(%r12),%r31
;
;	LDH,MB		-24(%r12),%r31	; error
	LDH,MB		-16(%r12),%r31
	LDH,MB		-8(%r12),%r31
	LDH,MB		0(%r12),%r31
	LDH,MB		(%r12),%r31
;	LDH,MB		%r6(%r12),%r31	; Error
	LDH,MB		8(%r12),%r31
;	LDH,MB		16(%r12),%r31	; error
;	LDH,MB		24(%r12),%r31	; error
;
;	LDB,MB		-24(%r12),%r31	; error
	LDB,MB		-16(%r12),%r31
	LDB,MB		-8(%r12),%r31
	LDB,MB		0(%r12),%r31
	LDB,MB		(%r12),%r31
;	LDB,MB		%r6(%r12),%r31	; Error
	LDB,MB		8(%r12),%r31
;	LDB,MB		16(%r12),%r31	; error
;	LDB,MB		24(%r12),%r31	; error
;
;	LDDA,MB		-24(%r12),%r31	; Error
	LDDA,MB		-16(%r12),%r31
	LDDA,MB		-8(%r12),%r31
	LDDA,MB		0(%r12),%r31
	LDDA,MB		(%r12),%r31
;	LDDA,MB		%r6(%r12),%r31	; Error
	LDDA,MB		8(%r12),%r31
;	LDDA,MB		16(%r12),%r31	; Error
;	LDDA,MB		24(%r12),%r31	; Error
;
;	LDWA,MB		-24(%r12),%r31	; Error
	LDWA,MB		-16(%r12),%r31
	LDWA,MB		-8(%r12),%r31
	LDWA,MB		0(%r12),%r31
	LDWA,MB		(%r12),%r31
;	LDWA,MB		%r6(%r12),%r31	; Error
	LDWA,MB		8(%r12),%r31
;	LDWA,MB		16(%r12),%r31	; Error
;	LDWA,MB		24(%r12),%r31	; Error
;
;	LDCD,MB		-24(%r12),%r31	; Error
	LDCD,MB		-16(%r12),%r31
	LDCD,MB		-8(%r12),%r31
	LDCD,MB		0(%r12),%r31
	LDCD,MB		(%r12),%r31
;	LDCD,MB		%r6(%r12),%r31	; Error
	LDCD,MB		8(%r12),%r31
;	LDCD,MB		16(%r12),%r31	; Error
;	LDCD,MB		24(%r12),%r31	; Error
;
;	LDCW,MB		-24(%r12),%r31	; Error
	LDCW,MB		-16(%r12),%r31
	LDCW,MB		-8(%r12),%r31
	LDCW,MB		0(%r12),%r31
	LDCW,MB		(%r12),%r31
;	LDCW,MB		%r6(%r12),%r31	; Error
	LDCW,MB		8(%r12),%r31
;	LDCW,MB		16(%r12),%r31	; Error
;	LDCW,MB		24(%r12),%r31	; Error
;
LDtest_with_O
	B		LDtest_with_O
;
	LDD,O		0(%r12),%r31
	LDD,O		(%r12),%r31
	LDW,O		0(%r12),%r31
	LDW,O		(%r12),%r31
	LDH,O		0(%r12),%r31
	LDH,O		(%r12),%r31
	LDB,O		0(%r12),%r31
	LDB,O		(%r12),%r31
	LDDA,O		0(%r12),%r31
	LDDA,O		(%r12),%r31
	LDWA,O		0(%r12),%r31
	LDWA,O		(%r12),%r31
	LDCD,O		0(%r12),%r31
	LDCD,O		(%r12),%r31
	LDCW,O		0(%r12),%r31
	LDCW,O		(%r12),%r31
;
STtest
	b		STtest		
	STD		%r31,-24(%r12)
	STD		%r31,-16(%r12)
	STD		%r31,-8(%r12)
	STD		%r31,0(%r12)
	STD		%r31,(%r12)
	STD		%r31,8(%r12)
	STD		%r31,16(%r12)
	STD		%r31,24(%r12)
;
	STW		%r31,-24(%r12)
	STW		%r31,-16(%r12)
	STW		%r31,-8(%r12)
	STW		%r31,0(%r12)
	STW		%r31,(%r12)
	STW		%r31,8(%r12)
	STW		%r31,16(%r12)
	STW		%r31,24(%r12)
;
	STH		%r31,-24(%r12)
	STH		%r31,-16(%r12)
	STH		%r31,-8(%r12)
	STH		%r31,0(%r12)
	STH		%r31,(%r12)
	STH		%r31,8(%r12)
	STH		%r31,16(%r12)
	STH		%r31,24(%r12)
;
	STB		%r31,-24(%r12)
	STB		%r31,-16(%r12)
	STB		%r31,-8(%r12)
	STB		%r31,0(%r12)
	STB		%r31,(%r12)
	STB		%r31,8(%r12)
	STB		%r31,16(%r12)
	STB		%r31,24(%r12)
;
;	STDA		%r31,-24(%r12)	; Error
	STDA		%r31,-16(%r12)
	STDA		%r31,-8(%r12)
	STDA		%r31,0(%r12)
	STDA		%r31,(%r12)
	STDA		%r31,8(%r12)
;	STDA		%r31,16(%r12)	; Error
;	STDA		%r31,24(%r12)	; Error
;
;	STWA		%r31,-24(%r12)	; Error
	STWA		%r31,-16(%r12)
	STWA		%r31,-8(%r12)
	STWA		%r31,0(%r12)
	STWA		%r31,(%r12)
	STWA		%r31,8(%r12)
;	STWA		%r31,16(%r12)	; Error
;	STWA		%r31,24(%r12)	; Error
;
;	STDBY		%r31,-24(%r12)	; Error
	STDBY		%r31,-16(%r12)
	STDBY		%r31,-8(%r12)
	STDBY		%r31,0(%r12)
	STDBY		%r31,(%r12)
	STDBY		%r31,8(%r12)
;	STDBY		%r31,16(%r12)	; Error
;	STDBY		%r31,24(%r12)	; Error
;
;	STBY		%r31,-24(%r12)	; Error
	STBY		%r31,-16(%r12)
	STBY		%r31,-8(%r12)
	STBY		%r31,0(%r12)
	STBY		%r31,(%r12)
	STBY		%r31,8(%r12)
;	STBY		%r31,16(%r12)	; Error
;	STBY		%r31,24(%r12)	; Error
;
STtest_with_MA
	b		STtest_with_MA		
	STD,MA		%r31,-24(%r12)
	STD,MA		%r31,-16(%r12)
	STD,MA		%r31,-8(%r12)
;	STD,MA		%r31,0(%r12)	; Error
;	STD,MA		%r31,(%r12)	; Error
	STD,MA		%r31,8(%r12)
	STD,MA		%r31,16(%r12)
	STD,MA		%r31,24(%r12)
;
	STW,MA		%r31,-24(%r12)
	STW,MA		%r31,-16(%r12)
	STW,MA		%r31,-8(%r12)
;	STW,MA		%r31,0(%r12)	; Error
;	STW,MA		%r31,(%r12)	; Error
	STW,MA		%r31,8(%r12)
	STW,MA		%r31,16(%r12)
	STW,MA		%r31,24(%r12)
;
;	STH,MA		%r31,-24(%r12)	; Error
	STH,MA		%r31,-16(%r12)
	STH,MA		%r31,-8(%r12)
;	STH,MA		%r31,0(%r12)	; Error
;	STH,MA		%r31,(%r12)	; Error
	STH,MA		%r31,8(%r12)
;	STH,MA		%r31,16(%r12)	; Error
;	STH,MA		%r31,24(%r12)	; Error
;
;	STB,MA		%r31,-24(%r12)	; Error
	STB,MA		%r31,-16(%r12)
	STB,MA		%r31,-8(%r12)
;	STB,MA		%r31,0(%r12)	; Error
;	STB,MA		%r31,(%r12)	; Error
	STB,MA		%r31,8(%r12)
;	STB,MA		%r31,16(%r12)	; Error
;	STB,MA		%r31,24(%r12)	; Error
;
;	STDA,MA		%r31,-24(%r12)	; Error
	STDA,MA		%r31,-16(%r12)
	STDA,MA		%r31,-8(%r12)
;	STDA,MA		%r31,0(%r12)	; Error
;	STDA,MA		%r31,(%r12)	; Error
	STDA,MA		%r31,8(%r12)
;	STDA,MA		%r31,16(%r12)	; Error
;	STDA,MA		%r31,24(%r12)	; Error
;
;	STWA,MA		%r31,-24(%r12)	; Error
	STWA,MA		%r31,-16(%r12)
	STWA,MA		%r31,-8(%r12)
;	STWA,MA		%r31,0(%r12)	; Error
;	STWA,MA		%r31,(%r12)	; Error
	STWA,MA		%r31,8(%r12)
;	STWA,MA		%r31,16(%r12)	; Error
;	STWA,MA		%r31,24(%r12)	; Error
;
STtest_with_MB
	b		STtest_with_MB		
	STD,MB		%r31,-24(%r12)
	STD,MB		%r31,-16(%r12)
	STD,MB		%r31,-8(%r12)
	STD,MB		%r31,0(%r12)
	STD,MB		%r31,(%r12)
	STD,MB		%r31,8(%r12)
	STD,MB		%r31,16(%r12)
	STD,MB		%r31,24(%r12)
;
	STW,MB		%r31,-24(%r12)
	STW,MB		%r31,-16(%r12)
	STW,MB		%r31,-8(%r12)
	STW,MB		%r31,0(%r12)
	STW,MB		%r31,(%r12)
	STW,MB		%r31,8(%r12)
	STW,MB		%r31,16(%r12)
	STW,MB		%r31,24(%r12)
;
;	STH,MB		%r31,-24(%r12)	; Error
	STH,MB		%r31,-16(%r12)
	STH,MB		%r31,-8(%r12)
	STH,MB		%r31,0(%r12)
	STH,MB		%r31,(%r12)
	STH,MB		%r31,8(%r12)
;	STH,MB		%r31,16(%r12)	; Error
;	STH,MB		%r31,24(%r12)	; Error
;
;	STB,MB		%r31,-24(%r12)	; Error
	STB,MB		%r31,-16(%r12)
	STB,MB		%r31,-8(%r12)
	STB,MB		%r31,0(%r12)
	STB,MB		%r31,(%r12)
	STB,MB		%r31,8(%r12)
;	STB,MB		%r31,16(%r12)	; Error
;	STB,MB		%r31,24(%r12)	; Error
;
;	STDA,MB		%r31,-24(%r12)	; Error
	STDA,MB		%r31,-16(%r12)
	STDA,MB		%r31,-8(%r12)
	STDA,MB		%r31,0(%r12)
	STDA,MB		%r31,(%r12)
	STDA,MB		%r31,8(%r12)
;	STDA,MB		%r31,16(%r12)	; Error
;	STDA,MB		%r31,24(%r12)	; Error
;
;	STWA,MB		%r31,-24(%r12)	; Error
	STWA,MB		%r31,-16(%r12)
	STWA,MB		%r31,-8(%r12)
	STWA,MB		%r31,0(%r12)
	STWA,MB		%r31,(%r12)
	STWA,MB		%r31,8(%r12)
;	STWA,MB		%r31,16(%r12)	; Error
;	STWA,MB		%r31,24(%r12)	; Error
;
STtest_with_O
	b		STtest_with_O		
	STD,O		%r31,0(%r12)
	STD,O		%r31,(%r12)
	STW,O		%r31,0(%r12)
	STW,O		%r31,(%r12)
	STH,O		%r31,0(%r12)
	STH,O		%r31,(%r12)
	STB,O		%r31,0(%r12)
	STB,O		%r31,(%r12)
	STDA,O		%r31,0(%r12)
	STDA,O		%r31,(%r12)
	STWA,O		%r31,0(%r12)
	STWA,O		%r31,(%r12)
;
FLDtest
	b		FLDtest		
	FLDD		-24(%r12),%fr8
	FLDD		-16(%r12),%fr8
	FLDD		-8(%r12),%fr8
	FLDD		0(%r12),%fr8
	FLDD		(%r12),%fr8
	FLDD		%r6(%r12),%fr8
	FLDD		8(%r12),%fr8
	FLDD		16(%r12),%fr8
	FLDD		24(%r12),%fr8
;
	FLDW		-24(%r12),%fr8
	FLDW		-16(%r12),%fr8
	FLDW		-8(%r12),%fr8
	FLDW		0(%r12),%fr8
	FLDW		(%r12),%fr8
	FLDW		%r6(%r12),%fr8
	FLDW		8(%r12),%fr8
	FLDW		16(%r12),%fr8
	FLDW		24(%r12),%fr8
;
FLDtest_with_MA
	b		FLDtest		
	FLDD,MA		-24(%r12),%fr8
	FLDD,MA		-16(%r12),%fr8
	FLDD,MA		-8(%r12),%fr8
;	FLDD,MA		0(%r12),%fr8	; Error
;	FLDD,MA		(%r12),%fr8	; Error
	FLDD,MA		8(%r12),%fr8
	FLDD,MA		16(%r12),%fr8
	FLDD,MA		24(%r12),%fr8
;
	FLDW,MA		-24(%r12),%fr8
	FLDW,MA		-16(%r12),%fr8
	FLDW,MA		-8(%r12),%fr8
;	FLDW,MA		0(%r12),%fr8	; Error
;	FLDW,MA		(%r12),%fr8	; Error
	FLDW,MA		8(%r12),%fr8
	FLDW,MA		16(%r12),%fr8
	FLDW,MA		24(%r12),%fr8
;
FLDtest_with_MB
	b		FLDtest		
	FLDD,MB		-24(%r12),%fr8
	FLDD,MB		-16(%r12),%fr8
	FLDD,MB		-8(%r12),%fr8
	FLDD,MB		0(%r12),%fr8
	FLDD,MB		(%r12),%fr8
	FLDD,MB		8(%r12),%fr8
	FLDD,MB		16(%r12),%fr8
	FLDD,MB		24(%r12),%fr8
;
	FLDW,MB		-24(%r12),%fr8
	FLDW,MB		-16(%r12),%fr8
	FLDW,MB		-8(%r12),%fr8
	FLDW,MB		0(%r12),%fr8
	FLDW,MB		(%r12),%fr8
	FLDW,MB		8(%r12),%fr8
	FLDW,MB		16(%r12),%fr8
	FLDW,MB		24(%r12),%fr8
;
FLDtest_with_O
	b		FLDtest_with_O	
	FLDD,O		0(%r12),%fr8
	FLDD,O		(%r12),%fr8
	FLDW,O		0(%r12),%fr8
	FLDW,O		(%r12),%fr8
;
FSTtest
	b		FSTtest		
	FSTD		%fr8,-24(%r12)
	FSTD		%fr8,-16(%r12)
	FSTD		%fr8,-8(%r12)
	FSTD		%fr8,0(%r12)
	FSTD		%fr8,(%r12)
	FSTD		%fr8,%r6(%r12)
	FSTD		%fr8,8(%r12)
	FSTD		%fr8,16(%r12)
	FSTD		%fr8,24(%r12)
;
	FSTW		%fr8,-24(%r12)
	FSTW		%fr8,-16(%r12)
	FSTW		%fr8,-8(%r12)
	FSTW		%fr8,0(%r12)
	FSTW		%fr8,(%r12)
	FSTW		%fr8,%r6(%r12)
	FSTW		%fr8,8(%r12)
	FSTW		%fr8,16(%r12)
	FSTW		%fr8,24(%r12)
;
FSTtest_with_MA
	b		FSTtest_with_MA
	FSTD,MA		%fr8,-24(%r12)
	FSTD,MA		%fr8,-16(%r12)
	FSTD,MA		%fr8,-8(%r12)
;	FSTD,MA		%fr8,0(%r12)	; Error
;	FSTD,MA		%fr8,(%r12)	; Error
	FSTD,MA		%fr8,8(%r12)
	FSTD,MA		%fr8,16(%r12)
	FSTD,MA		%fr8,24(%r12)
;
	FSTW,MA		%fr8,-24(%r12)
	FSTW,MA		%fr8,-16(%r12)
	FSTW,MA		%fr8,-8(%r12)
;	FSTW,MA		%fr8,0(%r12)	; Error
;	FSTW,MA		%fr8,(%r12)	; Error
	FSTW,MA		%fr8,8(%r12)
	FSTW,MA		%fr8,16(%r12)
	FSTW,MA		%fr8,24(%r12)
;
FSTtest_with_MB
	b		FSTtest_with_MB
	FSTD,MB		%fr8,-24(%r12)
	FSTD,MB		%fr8,-16(%r12)
	FSTD,MB		%fr8,-8(%r12)
	FSTD,MB		%fr8,0(%r12)
	FSTD,MB		%fr8,(%r12)
	FSTD,MB		%fr8,8(%r12)
	FSTD,MB		%fr8,16(%r12)
	FSTD,MB		%fr8,24(%r12)
;
	FSTW,MB		%fr8,-24(%r12)
	FSTW,MB		%fr8,-16(%r12)
	FSTW,MB		%fr8,-8(%r12)
	FSTW,MB		%fr8,0(%r12)
	FSTW,MB		%fr8,(%r12)
	FSTW,MB		%fr8,8(%r12)
	FSTW,MB		%fr8,16(%r12)
	FSTW,MB		%fr8,24(%r12)
;
FSTtest_with_O
	b		FSTtest_with_O
	FSTD,O		%fr8,0(%r12)
	FSTD,O		%fr8,(%r12)
	FSTW,O		%fr8,0(%r12)
	FSTW,O		%fr8,(%r12)
;
	b		FCNVtest
FCNVtest
	FCNV,SGL,W	%fr4L,%fr5L
	FCNV,SGL,W	%fr4L,%fr5R
	FCNV,SGL,W	%fr4R,%fr5L
	FCNV,SGL,W	%fr4R,%fr5R
	FCNV,SGL,UW	%fr4L,%fr5L
	FCNV,SGL,UW	%fr4L,%fr5R
	FCNV,SGL,UW	%fr4R,%fr5L
	FCNV,SGL,UW	%fr4R,%fr5R
	FCNV,SGL,DBL	%fr4L,%fr5
	FCNV,SGL,DBL	%fr4R,%fr5
	FCNV,SGL,DW	%fr4L,%fr5
	FCNV,SGL,DW	%fr4R,%fr5
	FCNV,SGL,UDW	%fr4L,%fr5
	FCNV,SGL,UDW	%fr4R,%fr5
	FCNV,SGL,QUAD	%fr4L,%fr5
	FCNV,SGL,QW	%fr4L,%fr5
	FCNV,SGL,UQW	%fr4L,%fr5
	FCNV,W,SGL	%fr4L,%fr5L
	FCNV,W,SGL	%fr4L,%fr5R
	FCNV,W,SGL	%fr4R,%fr5L
	FCNV,W,SGL	%fr4R,%fr5R
	FCNV,W,DBL	%fr4L,%fr5
	FCNV,W,DBL	%fr4R,%fr5
	FCNV,W,QUAD	%fr4L,%fr5
	FCNV,UW,SGL	%fr4L,%fr5L
	FCNV,UW,SGL	%fr4L,%fr5R
	FCNV,UW,SGL	%fr4R,%fr5L
	FCNV,UW,SGL	%fr4R,%fr5R
	FCNV,UW,DBL	%fr4L,%fr5
	FCNV,UW,DBL	%fr4R,%fr5
	FCNV,UW,QUAD	%fr4L,%fr5
	FCNV,DBL,SGL	%fr4,%fr5L
	FCNV,DBL,SGL	%fr4,%fr5R
	FCNV,DBL,W	%fr4,%fr5L
	FCNV,DBL,W	%fr4,%fr5R
	FCNV,DBL,UW	%fr4,%fr5L
	FCNV,DBL,UW	%fr4,%fr5R
	FCNV,DBL,DW	%fr4,%fr5
	FCNV,DBL,UDW	%fr4,%fr5
	FCNV,DBL,QUAD	%fr4,%fr5
	FCNV,DBL,QW	%fr4,%fr5
	FCNV,DBL,UQW	%fr4,%fr5
	FCNV,DW,SGL	%fr4,%fr5L
	FCNV,DW,SGL	%fr4,%fr5R
	FCNV,DW,DBL	%fr4,%fr5
	FCNV,DW,QUAD	%fr4,%fr5
	FCNV,UDW,SGL	%fr4,%fr5L
	FCNV,UDW,SGL	%fr4,%fr5R
	FCNV,UDW,DBL	%fr4,%fr5
	FCNV,UDW,QUAD	%fr4,%fr5
	FCNV,QUAD,SGL	%fr4,%fr5L
	FCNV,QUAD,W	%fr4,%fr5L
	FCNV,QUAD,UW	%fr4,%fr5L
	FCNV,QUAD,DBL	%fr4,%fr5
	FCNV,QUAD,DW	%fr4,%fr5
	FCNV,QUAD,UDW	%fr4,%fr5
	FCNV,QUAD,QW	%fr4,%fr5
	FCNV,QUAD,UQW	%fr4,%fr5
	FCNV,QW,SGL	%fr4,%fr5L
	FCNV,QW,DBL	%fr4,%fr5
	FCNV,QW,QUAD	%fr4,%fr5
	FCNV,UQW,SGL	%fr4,%fr5L
	FCNV,UQW,DBL	%fr4,%fr5
	FCNV,UQW,QUAD	%fr4,%fr5

	b		FCNV_with_T
FCNV_with_T

	FCNV,SGL,T,W	%fr4L,%fr5L
	FCNV,SGL,W,T	%fr4L,%fr5R
	FCNV,T,SGL,W	%fr4R,%fr5L
	FCNV,SGL,T,W	%fr4R,%fr5R
	FCNV,SGL,UW,T	%fr4L,%fr5L
	FCNV,T,SGL,UW	%fr4L,%fr5R
	FCNV,SGL,T,UW	%fr4R,%fr5L
	FCNV,SGL,UW,T	%fr4R,%fr5R
	FCNV,SGL,DW,T	%fr4L,%fr5
	FCNV,T,SGL,DW	%fr4R,%fr5
	FCNV,SGL,T,UDW	%fr4L,%fr5
	FCNV,SGL,UDW,T	%fr4R,%fr5
	FCNV,SGL,QW,T	%fr4L,%fr5
	FCNV,SGL,T,UQW	%fr4L,%fr5
	FCNV,T,DBL,W	%fr4,%fr5L
	FCNV,DBL,T,W	%fr4,%fr5R
	FCNV,DBL,UW,T	%fr4,%fr5L
	FCNV,T,DBL,UW	%fr4,%fr5R
	FCNV,DBL,DW,T	%fr4,%fr5
	FCNV,T,DBL,UDW	%fr4,%fr5
	FCNV,DBL,QW,T	%fr4,%fr5
	FCNV,T,DBL,UQW	%fr4,%fr5
	FCNV,QUAD,W,T	%fr4,%fr5L
	FCNV,QUAD,T,UW	%fr4,%fr5L
	FCNV,QUAD,T,DW	%fr4,%fr5
	FCNV,QUAD,UDW,T	%fr4,%fr5
	FCNV,QUAD,T,QW	%fr4,%fr5
	FCNV,QUAD,UQW,T	%fr4,%fr5

	b		BBWtest
BBWtest
	BB,<		%r8,0,BBWtest
	BB,<		%r8,1,BBWtest
	BB,<,N		%r8,30,BBWtest
	BB,>=,N		%r8,31,BBWtest
	BB,<		%r8,%sar,BBWtest
	BB,>=		%r8,%sar,BBWtest
	BB,<,N		%r8,%sar,BBWtest
	BB,>=,N		%r8,%sar,BBWtest
	b		BBWtest
BBDtest
	BB,*<		%r8,0,BBDtest
	BB,*>=		%r8,1,BBDtest
	BB,*<,N		%r8,30,BBDtest
	BB,*<,N		%r8,31,BBDtest
	BB,*>=,N	%r8,32,BBDtest
	BB,*>=,N	%r8,33,BBDtest
	bb,*<,n		%r2,60,BBDtest
	BB,*<,N		%r8,62,BBDtest
	BB,*>=,N	%r8,63,BBDtest
	BB,*<		%r8,%sar,BBDtest
	BB,*>=		%r8,%sar,BBDtest
	BB,*<,N		%r8,%sar,BBDtest
	BB,*>=,N	%r8,%sar,BBDtest

	b		systst
systst
	RFI
	RFI,R
	PROBE,R		(%sr2,%r9),%r8,%r7
	PROBE,W		(%sr2,%r9),%r8,%r7
	PROBEI,R	(%sr2,%r9),10,%r7
	PROBEI,W	(%sr2,%r9),10,%r7
	LPA		%r3(%r31),%r8
	LPA,M		%r3(%r31),%r8
	PDTLB		%r5(%sr2,%r9)		
	PDTLB,L		%r5(%sr2,%r9)		
	PITLB		%r5(%sr2,%r9)		
	PITLB,L		%r5(%sr2,%r9)		
	PDTLB,M		%r5(%sr2,%r9)		
	PDTLB,L,M	%r5(%sr2,%r9)		
	PDTLB,M,L	%r5(%sr2,%r9)		
	PITLB,M		%r5(%sr2,%r9)		
	PITLB,L,M	%r5(%sr2,%r9)		
	PITLB,M,L	%r5(%sr2,%r9)
	PDTLBE		%r5(%r9)		
	PDTLBE,M	%r5(%r9)		
	FIC		%r5(%r9)		
	FIC		%r5(%sr2,%r9)		
	FIC		%r5(%sr6,%r9)		
	IDTLBT		%r5,%r9			
	IITLBT		%r5,%r9			

	b		DShifts
DShifts
	SHRPD		%r9,%r10,4,%r10
	SHRPD		%r9,%r10,%sar,%r10
	EXTRD,S		%r9,8,4,%r10
	EXTRD,U		%r9,8,4,%r10
	EXTRD		%r9,8,4,%r10
	EXTRD,S		%r9,%sar,8,%r10
	EXTRD,U		%r9,%sar,8,%r10	
	EXTRD		%r9,%sar,8,%r10
	DEPD		%r9,8,4,%r10
	DEPD,Z		%r9,8,4,%r10
	DEPD		%r9,%sar,8,%r10
	DEPD,Z		%r9,%sar,8,%r10
	DEPDI		4,8,4,%r10
	DEPDI,Z		4,8,4,%r10
	DEPDI		4,%sar,8,%r10
	DEPDI,Z		4,%sar,8,%r10
;
	b		WShifts
WShifts
	SHRPW		%r9,%r10,4,%r10
	SHRPW		%r9,%r10,%sar,%r10
	EXTRW,S		%r9,8,4,%r10
	EXTRW,U		%r9,8,4,%r10
	EXTRW		%r9,8,4,%r10
	EXTRW,S		%r9,%sar,8,%r10
	EXTRW,U		%r9,%sar,8,%r10	
	EXTRW		%r9,%sar,8,%r10
	DEPW		%r9,8,4,%r10
	DEPW,Z		%r9,8,4,%r10
	DEPW		%r9,%sar,8,%r10
	DEPW,Z		%r9,%sar,8,%r10
	DEPWI		4,8,4,%r10
	DEPWI,Z		4,8,4,%r10
	DEPWI		4,%sar,8,%r10
	DEPWI,Z		4,%sar,8,%r10

	b		dcortst
dcortst
	DCOR		%r5,%r6
	DCOR,SHZ	%r5,%r6
	DCOR,I		%r5,%r6
	DCOR,I,SHZ	%r5,%r6
	DCOR,SHZ,I	%r5,%r6
	b		uaddtst
uaddtst
	UADDCM		%r4,%r5,%r6
	UADDCM,SDC	%r4,%r5,%r6
	UADDCM,TC	%r4,%r5,%r6
	UADDCM,SDC,TC	%r4,%r5,%r6
	b		cmpbtst
cmpbtst
	CMPB		%r4,%r5,cmpbtst
	CMPB,=		%r4,%r5,cmpbtst
	CMPB,<		%r4,%r5,cmpbtst
	CMPB,<=		%r4,%r5,cmpbtst
	CMPB,<<		%r4,%r5,cmpbtst
	CMPB,<<=	%r4,%r5,cmpbtst
	CMPB,SV		%r4,%r5,cmpbtst
	CMPB,OD		%r4,%r5,cmpbtst
	CMPB,TR		%r4,%r5,cmpbtst
	CMPB,<>		%r4,%r5,cmpbtst
	CMPB,>=		%r4,%r5,cmpbtst
	CMPB,>		%r4,%r5,cmpbtst
	CMPB,>>=	%r4,%r5,cmpbtst
	CMPB,>>		%r4,%r5,cmpbtst
	CMPB,NSV	%r4,%r5,cmpbtst
	CMPB,EV		%r4,%r5,cmpbtst
	CMPB,*		%r4,%r5,cmpbtst
	CMPB,*=		%r4,%r5,cmpbtst
	CMPB,*<		%r4,%r5,cmpbtst
	CMPB,*<=	%r4,%r5,cmpbtst
	CMPB,*<<	%r4,%r5,cmpbtst
	CMPB,*<<=	%r4,%r5,cmpbtst
	CMPB,*SV	%r4,%r5,cmpbtst
	CMPB,*OD	%r4,%r5,cmpbtst
	CMPB,*TR	%r4,%r5,cmpbtst
	CMPB,*<>	%r4,%r5,cmpbtst
	CMPB,*>=	%r4,%r5,cmpbtst
	CMPB,*>		%r4,%r5,cmpbtst
	CMPB,*>>=	%r4,%r5,cmpbtst
	CMPB,*>>	%r4,%r5,cmpbtst
	CMPB,*NSV	%r4,%r5,cmpbtst
	CMPB,*EV	%r4,%r5,cmpbtst
	b		cmpibtst
cmpibtst
	CMPIB		4,%r5,cmpibtst
	CMPIB,=		4,%r5,cmpibtst
	CMPIB,<		4,%r5,cmpibtst
	CMPIB,<=	4,%r5,cmpibtst
	CMPIB,<<	4,%r5,cmpibtst
	CMPIB,<<=	4,%r5,cmpibtst
	CMPIB,SV	4,%r5,cmpibtst
	CMPIB,OD	4,%r5,cmpibtst
	CMPIB,TR	4,%r5,cmpibtst
	CMPIB,<>	4,%r5,cmpibtst
	CMPIB,>=	4,%r5,cmpibtst
	CMPIB,>		4,%r5,cmpibtst
	CMPIB,>>=	4,%r5,cmpibtst
	CMPIB,>>	4,%r5,cmpibtst
	CMPIB,NSV	4,%r5,cmpibtst
	CMPIB,EV	4,%r5,cmpibtst
	CMPIB,*<<	4,%r5,cmpibtst
	CMPIB,*=	4,%r5,cmpibtst
	CMPIB,*<	4,%r5,cmpibtst
	CMPIB,*<=	4,%r5,cmpibtst
	CMPIB,*>>=	4,%r5,cmpibtst
	CMPIB,*<>	4,%r5,cmpibtst
	CMPIB,*>=	4,%r5,cmpibtst
	CMPIB,*>	4,%r5,cmpibtst
	b		cmpclrtst
cmpclrtst
	CMPCLR		%r4,%r5,%r6
	CMPCLR,=	%r4,%r5,%r6
	CMPCLR,<	%r4,%r5,%r6
	CMPCLR,<=	%r4,%r5,%r6
	CMPCLR,<<	%r4,%r5,%r6
	CMPCLR,<<=	%r4,%r5,%r6
	CMPCLR,SV	%r4,%r5,%r6
	CMPCLR,OD	%r4,%r5,%r6
	CMPCLR,TR	%r4,%r5,%r6
	CMPCLR,<>	%r4,%r5,%r6
	CMPCLR,>=	%r4,%r5,%r6
	CMPCLR,>	%r4,%r5,%r6
	CMPCLR,>>=	%r4,%r5,%r6
	CMPCLR,>>	%r4,%r5,%r6
	CMPCLR,NSV	%r4,%r5,%r6
	CMPCLR,EV	%r4,%r5,%r6
	CMPCLR,*	%r4,%r5,%r6
	CMPCLR,*=	%r4,%r5,%r6
	CMPCLR,*<	%r4,%r5,%r6
	CMPCLR,*<=	%r4,%r5,%r6
	CMPCLR,*<<	%r4,%r5,%r6
	CMPCLR,*<<=	%r4,%r5,%r6
	CMPCLR,*SV	%r4,%r5,%r6
	CMPCLR,*OD	%r4,%r5,%r6
	CMPCLR,*TR	%r4,%r5,%r6
	CMPCLR,*<>	%r4,%r5,%r6
	CMPCLR,*>=	%r4,%r5,%r6
	CMPCLR,*>	%r4,%r5,%r6
	CMPCLR,*>>=	%r4,%r5,%r6
	CMPCLR,*>>	%r4,%r5,%r6
	CMPCLR,*NSV	%r4,%r5,%r6
	CMPCLR,*EV	%r4,%r5,%r6
	b		cmpiclrtst
cmpiclrtst
	CMPICLR		4,%r5,%r6
	CMPICLR,=	4,%r5,%r6
	CMPICLR,<	4,%r5,%r6
	CMPICLR,<=	4,%r5,%r6
	CMPICLR,<<	4,%r5,%r6
	CMPICLR,<<=	4,%r5,%r6
	CMPICLR,SV	4,%r5,%r6
	CMPICLR,OD	4,%r5,%r6
	CMPICLR,TR	4,%r5,%r6
	CMPICLR,<>	4,%r5,%r6
	CMPICLR,>=	4,%r5,%r6
	CMPICLR,>	4,%r5,%r6
	CMPICLR,>>=	4,%r5,%r6
	CMPICLR,>>	4,%r5,%r6
	CMPICLR,NSV	4,%r5,%r6
	CMPICLR,EV	4,%r5,%r6
	CMPICLR,*	4,%r5,%r6
	CMPICLR,*=	4,%r5,%r6
	CMPICLR,*<	4,%r5,%r6
	CMPICLR,*<=	4,%r5,%r6
	CMPICLR,*<<	4,%r5,%r6
	CMPICLR,*<<=	4,%r5,%r6
	CMPICLR,*SV	4,%r5,%r6
	CMPICLR,*OD	4,%r5,%r6
	CMPICLR,*TR	4,%r5,%r6
	CMPICLR,*<>	4,%r5,%r6
	CMPICLR,*>=	4,%r5,%r6
	CMPICLR,*>	4,%r5,%r6
	CMPICLR,*>>=	4,%r5,%r6
	CMPICLR,*>>	4,%r5,%r6
	CMPICLR,*NSV	4,%r5,%r6
	CMPICLR,*EV	4,%r5,%r6
	b		coprtst
coprtst
	COPR,0,0
	FID
	COPR,7,0,n
	COPR,1,0
	COPR,7,31
	COPR,7,32
	COPR,7,64
	COPR,7,128
	COPR,7,256
	COPR,7,512
	COPR,7,0x400
	COPR,7,0x800
	COPR,7,0x1000
	COPR,7,0x2000
	COPR,7,0x4000
	COPR,7,0x8000
	COPR,7,0x10000
	COPR,7,0x20000
	COPR,7,0x40000
	COPR,7,0x80000
	COPR,7,0x100000
	COPR,7,0x200000
	b		spop0tst
spop0tst
	SPOP0,7,0
	SPOP0,7,0,n
	SPOP0,1,0
	SPOP0,7,31
	SPOP0,7,32
	SPOP0,7,64
	SPOP0,7,128
	SPOP0,7,256
	SPOP0,7,512
	SPOP0,7,0x400
	SPOP0,7,0x800
	SPOP0,7,0x1000
	SPOP0,7,0x2000
	SPOP0,7,0x4000
	SPOP0,7,0x8000
	SPOP0,7,0x10000
	SPOP0,7,0x20000
	SPOP0,7,0x40000
	SPOP0,7,0x80000
	b		spop1tst
spop1tst
	SPOP1,7,0	%r5
	SPOP1,7,0,n	%r5
	SPOP1,1,0	%r5
	SPOP1,7,31	%r5
	SPOP1,7,32	%r5
	SPOP1,7,64	%r5
	SPOP1,7,128	%r5
	SPOP1,7,256	%r5
	SPOP1,7,512	%r5
	SPOP1,7,0x400	%r5
	SPOP1,7,0x800	%r5
	SPOP1,7,0x1000	%r5
	SPOP1,7,0x2000	%r5
	SPOP1,7,0x4000	%r5
	b		spop2tst
spop2tst
	SPOP2,7,0	%r5
	SPOP2,7,0,n	%r5
	SPOP2,1,0	%r5
	SPOP2,7,31	%r5
	SPOP2,7,32	%r5
	SPOP2,7,64	%r5
	SPOP2,7,128	%r5
	SPOP2,7,256	%r5
	SPOP2,7,512	%r5
	SPOP2,7,0x400	%r5
	SPOP2,7,0x800	%r5
	SPOP2,7,0x1000	%r5
	SPOP2,7,0x2000	%r5
	SPOP2,7,0x4000	%r5
	b		spop3tst
spop3tst
	SPOP3,7,0	%r5,%r8
	SPOP3,7,0,n	%r5,%r8
	SPOP3,1,0	%r5,%r8
	SPOP3,7,31	%r5,%r8
	SPOP3,7,32	%r5,%r8
	SPOP3,7,64	%r5,%r8
	SPOP3,7,128	%r5,%r8
	SPOP3,7,256	%r5,%r8
	SPOP3,7,512	%r5,%r8
	b		additst
additst
	ADDI		4,%r5,%r6
	ADDI,TSV	4,%r5,%r6
	ADDI,TC		4,%r5,%r6
	ADDI,TSV,TC	4,%r5,%r6
;
	ADDI,=		4,%r5,%r6
	ADDI,TSV,=	4,%r5,%r6
	ADDI,TC,=	4,%r5,%r6
	ADDI,TSV,TC,=	4,%r5,%r6
;
	b		subitst
subitst
	SUBI		4,%r5,%r6
	SUBI,TSV	4,%r5,%r6
;
	SUBI,=		4,%r5,%r6
	SUBI,TSV,=	4,%r5,%r6
;
	b		addtst
addtst
	ADD		%r4,%r5,%r6
	ADD,C		%r4,%r5,%r6
	ADD,DC		%r4,%r5,%r6
	ADD,L		%r4,%r5,%r6
	ADD,TSV		%r4,%r5,%r6
;
	ADD,C,TSV	%r4,%r5,%r6
	ADD,DC,TSV	%r4,%r5,%r6
	ADD,TSV,C	%r4,%r5,%r6
	ADD,TSV,DC	%r4,%r5,%r6
;
	ADD,=		%r4,%r5,%r6
	ADD,C,=		%r4,%r5,%r6
	ADD,DC,=	%r4,%r5,%r6
	ADD,L,=		%r4,%r5,%r6
	ADD,TSV,=	%r4,%r5,%r6
;
	ADD,C,TSV,=	%r4,%r5,%r6
	ADD,DC,TSV,=	%r4,%r5,%r6
	ADD,TSV,C,=	%r4,%r5,%r6
	ADD,TSV,DC,=	%r4,%r5,%r6
;
	b 		shladdtst
shladdtst
	SHLADD		%r4,1,%r5,%r6
	SHLADD,L	%r4,2,%r5,%r6
	SHLADD,TSV	%r4,3,%r5,%r6
;
	SHLADD,=	%r4,1,%r5,%r6
	SHLADD,L,=	%r4,2,%r5,%r6
	SHLADD,TSV,=	%r4,3,%r5,%r6
;
	b		subtst
subtst
	SUB		%r4,%r5,%r6
	SUB,B		%r4,%r5,%r6
	SUB,DB		%r4,%r5,%r6
	SUB,TC		%r4,%r5,%r6
	SUB,TSV		%r4,%r5,%r6
;
	SUB,B,TSV	%r4,%r5,%r6
	SUB,DB,TSV	%r4,%r5,%r6
	SUB,TC,TSV	%r4,%r5,%r6
	SUB,TSV,B	%r4,%r5,%r6
	SUB,TSV,DB	%r4,%r5,%r6
	SUB,TSV,TC	%r4,%r5,%r6
;
	SUB,=		%r4,%r5,%r6
	SUB,B,=		%r4,%r5,%r6
	SUB,DB,=	%r4,%r5,%r6
	SUB,TC,=	%r4,%r5,%r6
	SUB,TSV,=	%r4,%r5,%r6
;
	SUB,B,TSV,=	%r4,%r5,%r6
	SUB,DB,TSV,=	%r4,%r5,%r6
	SUB,TC,TSV,=	%r4,%r5,%r6
	SUB,TSV,B,=	%r4,%r5,%r6
	SUB,TSV,DB,=	%r4,%r5,%r6
	SUB,TSV,TC,=	%r4,%r5,%r6
;
	b		gate
gate
	B,GATE		test
	B,GATE		test,%r1
	B,GATE		test,%r2
	B,GATE		test,%r31

	B btst
btst
	B,L		test,%r0
	B,L		test,%r1
	B,L		test,%r2
	B,L,PUSH	test
	B,L,PUSH	test,%r2
	B,PUSH,L	test
	B,PUSH,L	test,%r2

	B betst
betst
	BE		test(%r8)
	BE,L		test(%r8)
	BE,L		test(%r8),%r31

	B bvetst
bvetst
	BVE		(%r8)
	BVE,L		(%r8)
	BVE,POP		(%r8)

	BVE,L		(%r8),%r2
	BVE,L,PUSH	(%r8)
	BVE,L,PUSH	(%r8),%r2
	BVE,PUSH,L	(%r8)
	BVE,PUSH,L	(%r8),%r2

	B 		bts_tst
bts_tst

	PUSHNOM
	CLRBTS
	POPBTS		1
	POPBTS		255
	POPBTS		256
	POPBTS		511
	PUSHBTS		%r0
	PUSHBTS		%r1
	PUSHBTS		%r31

	b		hsh
hsh
	HSHLADD		%r4,1,%r5,%r6
	HSHLADD		%r4,2,%r5,%r6
	HSHLADD		%r4,3,%r5,%r6
	HSHRADD		%r4,1,%r5,%r6
	HSHRADD		%r4,2,%r5,%r6
	HSHRADD		%r4,3,%r5,%r6

	b		sh
sh
	SHLADD		%r4,1,%r5,%r6
	SHLADD		%r4,2,%r5,%r6
	SHLADD		%r4,3,%r5,%r6


	b		mixs
mixs
	MIXH,L		%r1,%r2,%r3
	MIXH,R		%r1,%r2,%r3
	MIXW,L		%r1,%r2,%r3
	MIXW,R		%r1,%r2,%r3

	b		hw_shifts

hw_shifts
	HSHL		%r4,0,%r6
	HSHR,U		%r4,1,%r6
	HSHR,S		%r4,2,%r6
	HSHL		%r4,3,%r6
	HSHR,U		%r4,4,%r6
	HSHR,S		%r4,5,%r6
	HSHL		%r4,6,%r6
	HSHR,U		%r4,7,%r6
	HSHR,S		%r4,8,%r6
	HSHL		%r4,9,%r6
	HSHR,U		%r4,10,%r6
	HSHR,S		%r4,11,%r6
	HSHL		%r4,12,%r6
	HSHR,U		%r4,13,%r6
	HSHR,S		%r4,14,%r6
	HSHL		%r4,15,%r6

	b		fcmp3
fcmp3
	fcmp,=,sgl,c0	%fr4l,%fr5r
	fcmp,<,c1,dbl	%fr5,%fr6
	fcmp,sgl,<=,c2	%fr6r,%fr7l
	fcmp,dbl,c3,>	%fr7,%fr8
	fcmp,c4,>=,sgl	%fr8l,%fr9r
	fcmp,c5,dbl,<>	%fr9,%fr10
	b		fcmp2
fcmp2
	fcmp,=,sgl	%fr4l,%fr5r
	fcmp,<,c6	%fr5,%fr6
	fcmp,sgl,<=	%fr6r,%fr7l
	fcmp,dbl,c0	%fr7,%fr8
	fcmp,c1,>=	%fr8l,%fr9r
	fcmp,c2,dbl	%fr9,%fr10
	b		fcmp1
fcmp1
	fcmp,<		%fr5,%fr6
	fcmp,dbl	%fr7,%fr8
	fcmp,c3		%fr9,%fr10
	b		fcmp0
fcmp0
	fcmp		%fr4l,%fr5r

	b		ftest2
ftest2
	FTEST,ACC,C0
	b		ftest1a
ftest1a
	FTEST,ACC
	FTEST,ACC8
	FTEST,ACC6
	FTEST,ACC4
	FTEST,ACC2
	FTEST,REJ
	FTEST,REJ8
	b		ftest1b
ftest1b
	FTEST,C0
	FTEST,C1
	FTEST,C2
	FTEST,C3
	FTEST,C4
	FTEST,C5
	FTEST,C6
	b		ftest0
ftest0
	FTEST

	b		fpin
fpin
	FNEG,SGL	%fr5R,%fr6L
	FNEGABS,SGL	%fr5R,%fr6L
	FMPYFADD,SGL	%fr6R,%fr12L,%fr18R,%fr24L
	FMPYNFADD,SGL	%fr6L,%fr12R,%fr18L,%fr24R

	FNEG,DBL	%fr5,%fr6
	FNEGABS,DBL	%fr5,%fr6
	FMPYFADD,DBL	%fr6,%fr12,%fr18,%fr24
	FMPYNFADD,DBL	%fr7,%fr13,%fr19,%fr25

	b		mmin
mmin
	FIC		%r5(%r9)		
	FIC		%r5(%sr2,%r9)		
	FIC		%r5(%sr6,%r9)		
	IDTLBT		%r5,%r9			
	IITLBT		%r5,%r9			
	PDTLB		%r5(%sr2,%r9)		
	PDTLB,L		%r5(%sr2,%r9)		
	PITLB		%r5(%sr2,%r9)		
	PITLB,L		%r5(%sr2,%r9)		

	b		sysin
sysin
	sync
	syncdma
	mfia		%r10
	mtsarcm		%r10

	b		compl64
compl64
	ADD,*		%r5,%r6,%r7
	ADD,*=		%r5,%r6,%r7
	ADD,*<		%r5,%r6,%r7
	ADD,*<=		%r5,%r6,%r7
	SUB,*<<		%r5,%r6,%r7
	SUB,*<<=	%r5,%r6,%r7
	ADD,*SV		%r5,%r6,%r7
	ADD,*OD		%r5,%r6,%r7
	ADD,*<>		%r5,%r6,%r7
	ADD,*>=		%r5,%r6,%r7
	ADD,*>		%r5,%r6,%r7
	SUB,*>>=	%r5,%r6,%r7
	SUB,*>>		%r5,%r6,%r7
	ADD,*NSV	%r5,%r6,%r7
	ADD,*EV		%r5,%r6,%r7
	ADD,*TR		%r5,%r6,%r7
	ADD,*NUV	%r5,%r6,%r7
	ADD,*ZNV	%r5,%r6,%r7
	ADD,*UV		%r5,%r6,%r7
	ADD,*VNZ	%r5,%r6,%r7
	UADDCM,*SWZ	%r5,%r6,%r7
	UADDCM,*SBZ	%r5,%r6,%r7
	UADDCM,*SHZ	%r5,%r6,%r7
	UADDCM,*SDC	%r5,%r6,%r7
	UADDCM,*SWC	%r5,%r6,%r7
	UADDCM,*SBC	%r5,%r6,%r7
	UADDCM,*SHC	%r5,%r6,%r7
	UADDCM,*NWZ	%r5,%r6,%r7
	UADDCM,*NBZ	%r5,%r6,%r7
	UADDCM,*NHZ	%r5,%r6,%r7
	UADDCM,*NDC	%r5,%r6,%r7
	UADDCM,*NWC	%r5,%r6,%r7
	UADDCM,*NBC	%r5,%r6,%r7
	UADDCM,*NHC	%r5,%r6,%r7

	b		WordUnit
WordUnit
	UADDCM		%r5,%r6,%r7
	UADDCM,SBZ	%r5,%r6,%r7
	UADDCM,SHZ	%r5,%r6,%r7
	UADDCM,SDC	%r5,%r6,%r7
	UADDCM,SBC	%r5,%r6,%r7
	UADDCM,SHC	%r5,%r6,%r7
	UADDCM,TR		%r5,%r6,%r7
	UADDCM,NBZ	%r5,%r6,%r7
	UADDCM,NHZ	%r5,%r6,%r7
	UADDCM,NDC	%r5,%r6,%r7
	UADDCM,NBC	%r5,%r6,%r7
	UADDCM,NHC	%r5,%r6,%r7
	.import	Long,code
	.import Short,code
Back
	B,L		Back,%r2
	B,L		Short,%r2
	BE,L		Back(%sr1,%r8),%r31
	BE		Back(%r0)
	BE,L		Short(%r0)
	BV		%r5(%r8)
	BVE		(%r8)
;
; with nullify
;
	BVE,N		(%r8)

        .exit
        .procend

mainend
        .proc
        .callinfo NO_CALLS,FRAME=0
        .entry

        NOP

        .exit
        .procend

        .end
