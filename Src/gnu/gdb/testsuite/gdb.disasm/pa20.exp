# pa20.exp   Tests gdb disassembly operations for PA2.0 code
#
if ![istarget "hppa*-*-*"] {
    verbose "Tests ignored for all but hppa based targets."
    return
}

if $tracelevel {
    strace $tracelevel
}

set prms_id 0
set bug_id 0

# use this to debug:
#log_user 1

set testfile pa20
set srcfile ${srcdir}/${subdir}/pa20-instr.s
set binfile ${srcdir}/${subdir}/${testfile}
set outfile ${objdir}/${subdir}/${testfile}.out
set comfile ${srcdir}/${subdir}/${testfile}.com
set tmpfile ${objdir}/${subdir}/${testfile}.tmp
set sedfile ${srcdir}/${subdir}/pa-sed.cmd
set diffile ${objdir}/${subdir}/${testfile}.dif
set tmp2file ${objdir}/${subdir}/${testfile}.tmp2

gdb_exit
remote_exec build "rm -f ${tmpfile} ${tmp2file} ${diffile}"

# To build a pa 2.0 executable
#
#     as -o pa20 pa20-instr.s
# or 
#     cc -g -o pa20 pa20-instr.s
#
# The +DA2.0N flag doesn't seem to be needed.
#
# Don't reject if there are warnings, as we expect this warning:
#
#    (Warning) At least one PA 2.0 object file (pa20-instr.o) was detected.
#    The linked output may not run on a PA 1.x system.
#
# compile "${srcfile} -g -o ${binfile}"


# This non-standard start-up sequence is taken from that for
# the standard "gdb_start" in 
# /CLO/Components/WDB/Src/gdb/gdb/testsuite/lib/gdb.exp
#
global GDB
if { [which $GDB] == 0 } {
    perror "$GDB does not exist."
    exit 1
}

remote_exec build "${srcdir}/${subdir}/redirect_cmd ${tmpfile} $GDB -nx -batch -silent -se ${binfile} -command ${comfile}"

# Remove actual addresses, which may vary.
#
remote_exec build "${srcdir}/${subdir}/redirect_cmd ${tmp2file}  sed -f ${sedfile} ${tmpfile}"

# Should be no differences after processing.

remote_exec build "${srcdir}/${subdir}/redirect_cmd ${diffile} diff ${outfile} ${tmp2file}"

if [ file size ${diffile} ] {
    fail "Disassembly"
} else {
    pass "Disassembly"
}

return 0
