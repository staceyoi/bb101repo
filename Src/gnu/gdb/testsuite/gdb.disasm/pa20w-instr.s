;  assemble as "as +DA2.0W -o pa20w pa20-instr.s"
; or
;  cc -g +DA2.0W -o pa20w pa20w-instr.s
;
; PA-RISC assembly-language test program for the debugger.
;
; This test is *not* intended to be executed.  Rather, this test serves
; as a comprehensive test for the debugger's PA disassembler.
;

; Some instructions are PA1.x, some PA2.0
        
        .level 2.0W

        .code
        .export main,ENTRY
        .export mainend,ENTRY
        .space $TEXT$
        .subspa $CODE$

main
        .proc
        .callinfo NO_CALLS,FRAME=0
        .entry

labela
        PDC             %r5(0,%r6)
        PDC,M           %r5(%r6)
        FDC             %r5(0,%r6)
        FDC,M           %r5(0,%r6)
;;
;;  This gets an assembly error--it may be format in the draft manual only
;;      FDC             5(0,%r6)

        FIC             %r5(0,%r6)
        FIC,M           %r5(0,%r6)

        FCNV,SGL,DBL    %fr4,%fr6
;;
;;   I don't know the magic to get the truncate flag to work
;;        FCNV,T,DBL,SGL  %fr4,%fr6

        FNEG            %fr4,%fr6
        FNEG,SGL        %fr4,%fr6
        FNEG,DBL        %fr4,%fr6
        FNEG,QUAD       %fr4,%fr6

        FNEGABS         %fr4,%fr6        
        FNEGABS,SGL     %fr4,%fr6
        FNEGABS,DBL     %fr4,%fr6
        FNEGABS,QUAD    %fr4,%fr6

        FMPYFADD        %fr2,%fr4,%fr6,%fr8
        FMPYFADD,SGL    %fr2,%fr4,%fr6,%fr8
        FMPYFADD,DBL    %fr2,%fr4,%fr6,%fr8
        FMPYFADD,QUAD   %fr2,%fr4,%fr6,%fr8

        FMPYNFADD       %fr2,%fr4,%fr6,%fr8
        FMPYNFADD,SGL   %fr2,%fr4,%fr6,%fr8
        FMPYNFADD,DBL   %fr2,%fr4,%fr6,%fr8
        FMPYNFADD,QUAD  %fr2,%fr4,%fr6,%fr8

        FCMP,SGL,true   %fr2,%fr4,3
        FTEST           3

        PMENB
        PMDIS
        PMDIS,N

        MTSARCM         %r5
        IDTLBT          %r5,%r6
        IITLBT          %r5,%r6

        FLDD            512(%r4),%fr5
        FLDD,MA         512(%r4),%fr5
        FLDD,O          0(%r4),%fr5

        FLDW            1024(0,%r4),%fr5
        FLDW,MA         1024(0,%r4),%fr5
        FLDW,MB         1024(0,%r4),%fr5

        FSTD            %fr5,1024(0,%r4)
        FSTD,O          %fr5,0(0,%r4)
        FSTD,MA         %fr5,1024(0,%r4)
        FSTD,MB         %fr5,1024(0,%r4)

        MIXW            %r4,%r5,%r6
        MIXW,L          %r4,%r5,%r6
        MIXW,R          %r4,%r5,%r6

        MIXH            %r4,%r5,%r6
        MIXH,L          %r4,%r5,%r6
        MIXH,R          %r4,%r5,%r6

        HADD            %r1,%r2,%r3
        HADD,US         %r3,%r4,%r5
        HADD,SS         %r6,%r7,%r8

        HSUB            %r1,%r2,%r3
        HSUB,US         %r3,%r4,%r5
        HSUB,SS         %r6,%r7,%r8

        HAVG            %r6,%r7,%r8

        HSHLADD         %r3,3,%r4,%r5
        HSHRADD         %r3,1,%r4,%r5

        HSHL            %r3,11,%r4
        HSHR            %r3,11,%r4
        HSHR,U          %r3,11,%r4
        HSHR,S          %r3,11,%r4

        PERMH,1230      %r1,%r3

        DEPDI           12,%cr11,17,%r3
        DEPDI,Z         12,%cr11,17,%r3
        DEPDI,Z,=       12,%cr11,17,%r3

        EXTRD,<         %r3,12,13,%r4
        EXTRD,U,=       %r3,%cr11,19,%r4

        BB,<            %r9,%cr11,labelc
        BB,>=,N         %r9,31,labelc

        SHRPD,>=        %r5,%r6,41,%r7
        SHRPD,=         %r1,%r2,%cr11,%r3
        SHRPD,OD        %r6,%r7,%cr11,%r3

        BVE             (%r6)
        BVE,N           (%r6)
        BVE,L           (%r5),%r2
        BVE,L,PUSH      (%r5),%r2
        BVE,POP         (%r5)

        PUSHNOM
        CLRBTS
        POPBTS          6
        PUSHBTS         %r4

        LDD             %r1(0,%r2),%r3
        LDD,MA          10(0,%r2),%r3
        LDD,MB          0(0,%r2),%r3
        LDD,O           0(0,%r2),%r3
        LDD,MA,SL       4(0,%r2),%r3

        LDDA            %r1(%r2),%r3
        LDDA,MA         8(%r2),%r3
        LDDA,MB         0(%r2),%r3
        LDDA,O          0(%r2),%r3
        LDDA,MA,SL      8(%r2),%r3

        STD             %r1,(0,%r2)
        STD,MA          %r1,10(0,%r2)
        STD,MB          %r1,0(0,%r2)
        STD,O           %r1,0(0,%r2)
        STD,MA,SL       %r1,4(0,%r2)

        STDA             %r1,(%r2)
        STDA,MA          %r1,10(%r2)
        STDA,MB          %r1,0(%r2)
        STDA,O           %r1,0(%r2)
        STDA,MA,SL       %r1,4(%r2)

        LDCD             %r1(0,%r2),%r3
        LDCD,M           %r1(0,%r2),%r3
        LDCD,MA          10(0,%r2),%r3
        LDCD,MB          0(0,%r2),%r3
        LDCD,O           0(0,%r2),%r3
        LDCD,MA          4(0,%r2),%r3

        STDBY            %r1,5(0,%r2)
        STDBY,B          %r1,5(%r2)
        STDBY,B,M        %r1,5(%r2)
        STDBY,E,M        %r1,5(%r2)
        STDBY,E          %r1,5(%r2)

        LDD             %r1(0,%r2),%r3
        LDD             10(0,%r2),%r3
        ADD             %r1,%r2,%r3
        ADD,C           %r0,%r1,%r2
        ADD,DC          %r0,%r1,%r2
        ADD,L           %r1,%r2,%r3
        ADD,TSV         %r3,%r4,%r5
        ADD,C,TSV       %r3,%r4,%r5
        ADD,DC,TSV      %r3,%r4,%r5

        ADDB,=          %r25,%r26,labelb
        ADDB,=,N        %r25,%r26,labelb
	;; In wide mode, the following commented out completers are not
	;; available
        ;; ADDB,SV         %r27,%r28,labela
        ;; ADDB,OD         %r27,%r28,labela
        ADDB,TR         %r27,%r28,labela
        ;; ADDB,NSV        %r27,%r28,labela
        ;; ADDB,EV         %r27,%r28,labela
        ADDB,<>         %r27,%r28,labela

        ADDBF,NUV       %r1,%r5,labela
        ADDBF,<=,N      %r10,%r6,labelb

        ADDBT,NUV       %r1,%r5,labela
        ADDBT,<=,N      %r10,%r6,labelb

        ADDC            %r28,%r29,%r30
        ADDC,UV         %r28,%r29,%r30
        
        ADDCO           %r28,%r29,%r30
        ADDCO,UV        %r28,%r29,%r30
        
        ADDI            0,%r2,%r0
        ADDI,>          1,%r3,%r0
        ADDI,<          2,%r4,%r0

        ADDIB,>=        -1,%r5,labela
        ADDIB,<=,N      10,%r6,labelb

        ADDIBF,NUV      -1,%r5,labela
        ADDIBF,<=,N     10,%r6,labelb

        ADDIBT,NUV      -1,%r5,labela
        ADDIBT,<=,N     10,%r6,labelb

        ADDIL           -1,%r3
        ADDIL           70000,%r3

        ADDIO           4,%r22,%r21
        ADDIO,<         4,%r22,%r21

        ADDIT           4,%r22,%r21
        ADDIT,TR        4,%r22,%r21

        ADDITO          934,%r25,%r24
        ADDITO,<>       1023,%r25,%r24

        ADDO            %r28,%r29,%r30
        ADDO,SV         %r28,%r29,%r30
        
        ADDL            %r28,%r29,%r30
        ADDL,NSV        %r28,%r29,%r30
        
        AND             %r30,%r31,%r30
        AND,<           %r30,%r31,%r30

        ANDCM           %r26,%r27,%r28
        ANDCM,>         %r26,%r27,%r28

labelb
        B               labelc
        B,N             labelc
        B,GATE          labelc
        B,L             labelb,%r3

        BB,<            %r9,%cr11,labelc
        BB,>=,N         %r9,31,labelc

        BE              100(%sr4,%r11)
        BE,L            0(%sr4,%r11),%sr0,%r31

        BL              labelb,%r3
        BL,N            labelb,%r3

        BLE             12345(%sr0,%r3)
        BLE,N           12345(%sr0,%r3)

        BLR             %r31,%r3
        BLR,N           %r0,%r3

        BREAK           0,1
        BREAK           31,1000

        BV              0(%r1)
        BV,N            (%r20)

        BVB,<           %r3,labela
        BVB,<,N         %r3,labela

        BVE             (%r5)
        BVE,POP         (%r5)
; PA2.0 opcodes:
        BVE,L           (%r5),%r2
        BVE,L,PUSH      (%r5),%r2

labelc
        CLDDS,0         0(%sr2,%r0),0
        CLDDS,1,MA      1(%sr2,%r1),2
        CLDDS,2,MB      9(%sr2,%r6),3

        CLDDX,0         %r3(%sr2,%r10),0
        CLDDX,0,S       %r3(%sr2,%r20),1
        CLDDX,1,M       %r3(%sr2,%r30),2
        CLDDX,2,SM      %r3(%sr2,%r0),3

        CLDWS,0         0(%sr2,%r0),0
        CLDWS,1,MA      3(%sr2,%r0),2
        CLDWS,2,MB      7(%sr2,%r0),3

        CLDWX,0         %r3(%sr2,%r0),0
        CLDWX,0,S       %r3(%sr2,%r0),1
        CLDWX,1,M       %r3(%sr2,%r0),2
        CLDWX,2,SM      %r3(%sr2,%r0),3

        CMPB,<          %r11,%r12,labelc
	;; not accepted in 2.0W
        ;; CMPB,>=,N       %r11,%r12,main

        CMPCLR,<>       %r2,%r3,%r4

        CMPIB,<=        0,%r2,labeld
        CMPIB,NSV,N     -16,%r2,labeld

        CMPICLR,OD      1000,%r0,%r31

        COMBF,<=        %r0,%r2,labeld
        COMBF,<<,N      %r16,%r2,labeld

        COMBT,<=        %r0,%r2,labeld
        COMBT,<<,N      %r16,%r2,labeld

        COMCLR          %r11,%r12,%r13
        COMCLR,>=       %r11,%r12,%r13

        COMIBF,<=       0,%r2,labeld
        COMIBF,<<=,N    -16,%r2,labeld

        COMIBT,<=       0,%r2,labeld
        COMIBT,<,N      -16,%r2,labeld

        COMICLR         1,%r3,%r4
        COMICLR,EV      157,%r3,%r4

        COPR,0,0
        COPR,7,0
        COPR,7,255

        COPY            %r3,%r4

        CSTDS,0         8,0(%sr1,%r31)
        CSTDS,7,MA      11,2(%sr1,%r3)
        CSTDS,4,MB      14,2(%sr1,%r3)

        CSTWS,0         8,0(%sr1,%r31)
        CSTWS,7,MA      11,2(%sr1,%r3)
        CSTWS,4,MB      14,2(%sr1,%r3)

labeld
        DCOR            %r3,%r4
        DCOR,I          %r3,%r4
        DCOR,SBZ        %r4,%r5
        DCOR,SHZ        %r4,%r6
        DCOR,SDC        %r4,%r7
        DCOR,SBC        %r4,%r8
        DCOR,SHC        %r4,%r9
        DCOR,NBZ        %r4,%r10
        DCOR,NHZ        %r4,%r11
        DCOR,NDC        %r4,%r12
        DCOR,NBC        %r4,%r13
        DCOR,NHC        %r4,%r14

        DEP             %r21,14,3,%r22
        DEP,>=          %r21,14,3,%r22

        DEPI            1,14,3,%r22
        DEPI,>=         2,14,3,%r22

        DEPW            %r19,1,2,%r1
        DEPW,Z          %r19,1,2,%r1
        DEPW,Z          %r19,%cr11,31,%r1
        DEPW,Z,<        %r19,30,1,%r1

        DEPWI           15,0,1,%r2
        DEPWI,Z         -16,0,1,%r2

        DIAG            123456

        DS              %r1,%r2,%r3
        DS,<>           %r1,%r2,%r3

labele
        EXTRS           %r1,3,4,%r2
        EXTRS,OD        %r1,3,4,%r2

        EXTRU           %r1,3,4,%r2
        EXTRU,EV        %r1,3,4,%r2

        EXTRW           %r0,3,4,%r1
        EXTRW,S         %r0,3,4,%r1
        EXTRW,U         %r0,3,4,%r1
        EXTRW,U,EV      %r0,3,4,%r1
        EXTRW,<>        %r0,%cr11,4,%r1

labelf
        FABS,SGL        %fr2,%fr3
        FABS,DBL        %fr2,%fr6

        FADD,SGL        %fr2,%fr4,%fr6
        FADD,DBL        %fr2,%fr4,%fr6

        FCMP,SGL        %fr3,%fr2
        FCMP,DBL,false  %fr3,%fr2
        FCMP,DBL,?      %fr3,%fr2
        FCMP,DBL,!<=>   %fr3,%fr2
        FCMP,DBL,=      %fr3,%fr2
        FCMP,DBL,=T     %fr3,%fr2
        FCMP,DBL,?=     %fr3,%fr2
        FCMP,DBL,!<>    %fr3,%fr2
        FCMP,DBL,!?>=   %fr3,%fr2
        FCMP,DBL,<      %fr3,%fr2
        FCMP,DBL,?<     %fr3,%fr2
        FCMP,DBL,!>=    %fr3,%fr2
        FCMP,DBL,!?>    %fr3,%fr2
        FCMP,DBL,<=     %fr3,%fr2
        FCMP,DBL,?<=    %fr3,%fr2
        FCMP,DBL,!>     %fr3,%fr2
        FCMP,DBL,!?<=   %fr3,%fr2
        FCMP,DBL,>      %fr3,%fr2
        FCMP,DBL,?>     %fr3,%fr2
        FCMP,DBL,!<=    %fr3,%fr2
        FCMP,DBL,!?<    %fr3,%fr2
        FCMP,DBL,>=     %fr3,%fr2
        FCMP,DBL,?>=    %fr3,%fr2
        FCMP,DBL,!<     %fr3,%fr2
        FCMP,DBL,!?=    %fr3,%fr2
        FCMP,DBL,<>     %fr3,%fr2
        FCMP,DBL,!=     %fr3,%fr2
        FCMP,DBL,!=T    %fr3,%fr2
        FCMP,DBL,!?     %fr3,%fr2
        FCMP,DBL,<=>    %fr3,%fr2
        FCMP,DBL,true?  %fr3,%fr2
        FCMP,DBL,true   %fr3,%fr2

        FCNV,SGL,DBL    %fr2,%fr4
        FCNV,SGL,W      %fr2,%fr4
        FCNV,SGL,DW     %fr2,%fr4
        FCNV,SGL,QUAD   %fr2,%fr4
        FCNV,SGL,QW     %fr2,%fr4
        FCNV,W,SGL      %fr2,%fr4
        FCNV,W,DBL      %fr2,%fr4
        FCNV,W,QUAD     %fr2,%fr4
        FCNV,DBL,SGL    %fr2,%fr4
        FCNV,DBL,W      %fr2,%fr4
        FCNV,DBL,QUAD   %fr2,%fr4
        FCNV,DBL,QW     %fr2,%fr4
        FCNV,QUAD,SGL   %fr2,%fr6
        FCNV,QUAD,DBL   %fr2,%fr6
        FCNV,QUAD,W     %fr2,%fr6
        FCNV,QUAD,QW    %fr2,%fr6
        FCNV,QW,SGL     %fr2,%fr4
        FCNV,QW,DBL     %fr2,%fr4
        FCNV,QW,QUAD    %fr2,%fr4

        FCNVFXT,DBL     %fr3,%fr4

        FCPY,SGL        %fr5,%fr6
        FCPY,DBL        %fr5,%fr6

        FDC             %r3(0,%r3)
	;; In wide mode, the following commented out completers are not
	;; available
        ;; FDC,M           0(0,%r4)

        FDCE            %r0(%sr3,%r7)
        FDCE,M          %r0(%sr3,%r7)

        FDIV,DBL        %fr1,%fr0,%fr2

        FIC             %r4(0,%r5)
        FIC,M           %r4(%sr2,%r5)

        FICE            %r0(%sr1,%r8)
        FICE,M          %r0(%sr1,%r8)

        DIAG            512
        
        FID

        FLDD            0(0,%r1),%fr1
        FLDD,MA         10(0,%r1),%fr1
        FLDD,MB         0(0,%r1),%fr1
        FLDD,O          0(0,%r1),%fr1
        FLDD,MA,SL      4(0,%r1),%fr1

        FLDDS           0(0,%r1),%fr1
        FLDDS,MA        10(0,%r1),%fr1
        FLDDS,MB        0(0,%r1),%fr1

        FLDDX           0(0,%r1),%fr1
        FLDDX,S         %r10(0,%r1),%fr1
        FLDDX,M         0(0,%r1),%fr1
        FLDDX,SM        0(0,%r1),%fr1

        FLDW            %r1(0,%r1),%fr1
        FLDW,MA         10(0,%r1),%fr1
        FLDW,MB         0(0,%r1),%fr1
        FLDW,O          0(0,%r1),%fr1
        FLDW,MA,SL      4(0,%r1),%fr1

        FLDWS           %r1(0,%r1),%fr1
        FLDWS,MA        10(0,%r1),%fr1
        FLDWS,MB        0(0,%r1),%fr1

        FLDWX           %r1(0,%r1),%fr1
        FLDWX,S         %r10(0,%r1),%fr1
        FLDWX,M         0(0,%r1),%fr1
        FLDWX,SM        0(0,%r1),%fr1

        FMPY,SGL        %fr6,%fr8,%fr10
        FMPY,DBL        %fr6,%fr8,%fr10

        FMPYADD,SGL     %fr16,%fr17,%fr18,%fr19,%fr20
        FMPYADD,DBL     %fr4,%fr7,%fr7,%fr5,%fr6

        FMPYFADD,DBL    %fr10,%fr11,%fr12,%fr13

        FMPYNFADD,DBL   %fr10,%fr11,%fr12,%fr13

        FMPYSUB,SGL     %fr16,%fr17,%fr18,%fr19,%fr30

; PA2.0 opcodes:
        FNEG,DBL        %fr10,%fr1

; PA2.0 opcodes:
        FNEGABS,SGL     %fr1,%fr1

        FRND,DBL        %fr2,%fr3

        FSQRT,SGL       %fr16,%fr17

        FSTD            %fr3,0(0,%r2)
        FSTD,MA         %fr3,8(0,%r2)
        FSTD,MB         %fr3,0(0,%r2)
        FSTD,O          %fr3,0(0,%r2)
        FSTD,MA,SL      %fr3,4(0,%r2)

        FSTDS           %fr3,0(0,%r2)
        FSTDS,MA        %fr3,8(0,%r2)
        FSTDS,MB        %fr3,0(0,%r2)

        FSTDX           %fr3,0(0,%r2)
        FSTDX,S         %fr3,%r8(0,%r2)
        FSTDX,M         %fr3,0(0,%r2)
        FSTDX,SM        %fr3,0(0,%r2)

        FSTW            %fr3,0(0,%r2)
        FSTW,MA         %fr3,8(0,%r2)
        FSTW,MB         %fr3,0(0,%r2)
        FSTW,O          %fr3,0(0,%r2)
        FSTW,MA,SL      %fr3,4(0,%r2)

        FSTWS           %fr3,0(0,%r2)
        FSTWS,MA        %fr3,8(0,%r2)
        FSTWS,MB        %fr3,0(0,%r2)

        FSTWX           %fr3,0(0,%r2)
        FSTWX,S         %fr3,%r8(0,%r2)
        FSTWX,M         %fr3,0(0,%r2)
        FSTWX,SM        %fr3,0(0,%r2)

        FSUB,DBL        %fr5,%fr2,%fr0

        FTEST
        FTEST,ACC
        FTEST,ACC8
        FTEST,ACC6
        FTEST,ACC4
        FTEST,ACC2
        FTEST,REJ
        FTEST,REJ8

labelg
        GATE            labelg,%r3
        GATE,N          labelu,%r3

labelh
; PA2.0 opcodes:
        HADD            %r2,%r3,%r4

labeli
        IDCOR           %r4,%r17
        IDCOR,SBZ       %r4,%r17
        IDCOR,SHZ       %r4,%r17
        IDCOR,SDC       %r4,%r17
        IDCOR,SBC       %r4,%r17
        IDCOR,SHC       %r4,%r17
        IDCOR,TR        %r4,%r17
        IDCOR,NBZ       %r4,%r17
        IDCOR,NHZ       %r4,%r17
        IDCOR,NDC       %r4,%r17
        IDCOR,NBC       %r4,%r17
        IDCOR,NHC       %r4,%r17

; PA2.0 opcodes:
        IDTLBT          %r1,%r2

;        IDTLBA          %r5,(%sr2,%r4)

;        IDTLBP          %r5,(%sr2,%r4)

; PA2.0 opcodes:
        IITLBT          %r2,%r3

;        IITLBA          %r5,(%sr2,%r4)

;        IITLBP          %r5,(%sr2,%r4)

labelj
labelk
labell
        LCI             %r0(0,%r1),%r2

        LDB             %r1(0,%r1),%r1
        LDB,MA          10(0,%r1),%r1
        LDB,MB          0(0,%r1),%r1
        LDB,O           0(0,%r1),%r1
        LDB,MA,SL       4(0,%r1),%r1

        LDBS            %r1(0,%r1),%r1
        LDBS,MA         10(0,%r1),%r1
        LDBS,MB         0(0,%r1),%r1
        LDBS,O          0(0,%r1),%r1
        LDBS,MA,SL      4(0,%r1),%r1

        LDBX            %r1(0,%r1),%r1
        LDBX,S          %r10(0,%r1),%r1
        LDBX,M          0(0,%r1),%r1
        LDBX,SM         0(0,%r1),%r1

; PA2.0 opcodes:
        LDCD            0(0,%r1),%r1

        LDCW            %r1(0,%r1),%r1
        LDCW,MA         10(0,%r1),%r1
        LDCW,MB         0(0,%r1),%r1
        LDCW,O          0(0,%r1),%r1
        LDCW,MA,CO      4(0,%r1),%r1

        LDCWS           %r1(0,%r1),%r1
        LDCWS,MA        10(0,%r1),%r1
        LDCWS,MB        0(0,%r1),%r1
        LDCWS,O         0(0,%r1),%r1
        LDCWS,MA,CO     4(0,%r1),%r1

        LDCWX           %r1(0,%r1),%r1
        LDCWX,S         %r3(0,%r1),%r1
        LDCWX,M         0(0,%r1),%r1
        LDCWX,SM        0(0,%r1),%r1

        LDH             %r1(0,%r1),%r1
        LDH,MA          10(0,%r1),%r1
        LDH,MB          0(0,%r1),%r1
        LDH,O           0(0,%r1),%r1
        LDH,MA,SL       4(0,%r1),%r1

        LDHS            %r1(0,%r1),%r1
        LDHS,MA         10(0,%r1),%r1
        LDHS,MB         0(0,%r1),%r1
        LDHS,O          0(0,%r1),%r1
        LDHS,MA,SL      4(0,%r1),%r1

        LDHX            %r1(0,%r1),%r1
        LDHX,S          %r10(0,%r1),%r1
        LDHX,M          0(0,%r1),%r1
        LDHX,SM         0(0,%r1),%r1

        LDIL            23456,%r6

        LDO             100(%r3),%r20

        LDSID           (0,%r0),%r3

        LDW             %r1(0,%r1),%r1
        LDW,MA          10(0,%r1),%r1
        LDW,MB          0(0,%r1),%r1
        LDW,O           0(0,%r1),%r1
        LDW,MA,SL       4(0,%r1),%r1

        LDWA            %r1(%r3),%r2
        LDWA,MA         8(%r3),%r2
        LDWA,MB         0(%r3),%r2
        LDWA,O          0(%r3),%r2
        LDWA,MA,SL      8(%r3),%r2

        LDWAS           %r1(%r3),%r2
        LDWAS,MA        8(%r3),%r2
        LDWAS,MB        0(%r3),%r2
        LDWAS,O         0(%r3),%r2
        LDWAS,MA,SL     8(%r3),%r2

        LDWAX           %r1(%r3),%r2
        LDWAX,S         %r8(%r3),%r2
        LDWAX,M         0(%r3),%r2
        LDWAX,SM        0(%r3),%r2

        LDWM            0x7fff(%r3),%r4

        LDWS            %r1(0,%r1),%r1
        LDWS,MA         10(0,%r1),%r1
        LDWS,MB         0(0,%r1),%r1
        LDWS,O          0(0,%r1),%r1
        LDWS,MA,SL      4(0,%r1),%r1

        LDWX            %r1(%r3),%r2
        LDWX,S          %r8(%r3),%r2
        LDWX,M          0(%r3),%r2
        LDWX,SM         0(%r3),%r2

        LPA             %r0(0,%r3),%r19
        LPA,M           %r0(%sr2,%r3),%r19

labelm
        MFCTL           %cr0,%r4
        MFCTL           %cr12,%r4

; PA2.0 opcodes:
        MFIA            %r25

        MFSP            %sr4,%r29

; PA2.0 opcodes:
        MIXH,L          %r1,%r2,%r3

        MOVB            %r1,%r2,labelk
        MOVB,N          %r1,%r2,labelj
        MOVB,>=,N       %r1,%r2,labela

	;; Instructions not supported by assembler for 2.0W
        ;; MOVIB           15,%r3,main
        ;; MOVIB,<         15,%r3,main
        ;; MOVIB,<>,N      15,%r3,main

        MTCTL           %r0,%cr17

        MTSAR           %r3

; PA2.0 opcodes:
        MTSARCM         %r7

        MTSM            %r2

        MTSP            %r19,%sr3

labeln
        NOP

labelo
        OR              %r1,%r0,%r3
        OR,EV           %r1,%r0,%r3

labelp
        PDC             %r0(0,%r1)
        PDC,M           %r0(0,%r1)

        PDTLB           %r8(%sr2,%r2)
        PDTLB,M         %r8(%sr2,%r2)
; PA2.0 opcodes:
        PDTLB,L         %r8(%sr2,%r2)
        PDTLB,L,M       %r8(%sr2,%r2)

        PDTLBE          %r4(%sr1,%r21)
        PDTLBE,M        %r4(%sr1,%r21)

        PITLB           %r6(%sr0,%r30)
        PITLB,M         %r6(%sr0,%r30)

        PITLBE          %r6(%sr0,%r30)
        PITLBE,M        %r6(%sr0,%r30)

        PROBE,R         (%sr0,%r26),%r0,%r30
        PROBE,W         (%sr0,%r26),%r0,%r30

        PROBEI,R        (%sr0,%r26),10,%r30
        PROBEI,W        (%sr0,%r26),7,%r30

labelq
labelr
        RFI
        RFI,R

        RFIR

        RSM             31,%r24

labels
        SH1ADD          %r14,%r15,%r16
        SH1ADD,NUV      %r14,%r15,%r16
        SH1ADD,ZNV      %r14,%r15,%r16
        SH1ADD,SV       %r14,%r15,%r16
        SH1ADD,UV       %r14,%r15,%r16
        SH1ADD,VNZ      %r14,%r15,%r16
        SH1ADD,NSV      %r14,%r15,%r16

        SH1ADDL         %r14,%r15,%r16
        SH1ADDL,NUV     %r14,%r15,%r16
        SH1ADDL,ZNV     %r14,%r15,%r16
        SH1ADDL,SV      %r14,%r15,%r16
        SH1ADDL,UV      %r14,%r15,%r16
        SH1ADDL,VNZ     %r14,%r15,%r16
        SH1ADDL,NSV     %r14,%r15,%r16

        SH1ADDO         %r14,%r15,%r16
        SH1ADDO,NUV     %r14,%r15,%r16
        SH1ADDO,ZNV     %r14,%r15,%r16
        SH1ADDO,SV      %r14,%r15,%r16
        SH1ADDO,UV      %r14,%r15,%r16
        SH1ADDO,VNZ     %r14,%r15,%r16
        SH1ADDO,NSV     %r14,%r15,%r16

        SH2ADD          %r14,%r15,%r16
        SH2ADD,NUV      %r14,%r15,%r16
        SH2ADD,ZNV      %r14,%r15,%r16
        SH2ADD,SV       %r14,%r15,%r16
        SH2ADD,UV       %r14,%r15,%r16
        SH2ADD,VNZ      %r14,%r15,%r16
        SH2ADD,NSV      %r14,%r15,%r16

        SH2ADDL         %r14,%r15,%r16
        SH2ADDL,NUV     %r14,%r15,%r16
        SH2ADDL,ZNV     %r14,%r15,%r16
        SH2ADDL,SV      %r14,%r15,%r16
        SH2ADDL,UV      %r14,%r15,%r16
        SH2ADDL,VNZ     %r14,%r15,%r16
        SH2ADDL,NSV     %r14,%r15,%r16

        SH2ADDO         %r14,%r15,%r16
        SH2ADDO,NUV     %r14,%r15,%r16
        SH2ADDO,ZNV     %r14,%r15,%r16
        SH2ADDO,SV      %r14,%r15,%r16
        SH2ADDO,UV      %r14,%r15,%r16
        SH2ADDO,VNZ     %r14,%r15,%r16
        SH2ADDO,NSV     %r14,%r15,%r16

        SH3ADD          %r14,%r15,%r16
        SH3ADD,NUV      %r14,%r15,%r16
        SH3ADD,ZNV      %r14,%r15,%r16
        SH3ADD,SV       %r14,%r15,%r16
        SH3ADD,UV       %r14,%r15,%r16
        SH3ADD,VNZ      %r14,%r15,%r16
        SH3ADD,NSV      %r14,%r15,%r16

        SH3ADDL         %r14,%r15,%r16
        SH3ADDL,NUV     %r14,%r15,%r16
        SH3ADDL,ZNV     %r14,%r15,%r16
        SH3ADDL,SV      %r14,%r15,%r16
        SH3ADDL,UV      %r14,%r15,%r16
        SH3ADDL,VNZ     %r14,%r15,%r16
        SH3ADDL,NSV     %r14,%r15,%r16

        SH3ADDO         %r14,%r15,%r16
        SH3ADDO,NUV     %r14,%r15,%r16
        SH3ADDO,ZNV     %r14,%r15,%r16
        SH3ADDO,SV      %r14,%r15,%r16
        SH3ADDO,UV      %r14,%r15,%r16
        SH3ADDO,VNZ     %r14,%r15,%r16
        SH3ADDO,NSV     %r14,%r15,%r16

        SHD             %r3,%r2,15,%r0
        SHD,<>          %r3,%r2,15,%r0

        SHLADD          %r1,2,%r3,%r6
        SHLADD,TSV      %r1,2,%r3,%r6
        SHLADD,L        %r1,2,%r3,%r6
        SHLADD,=        %r1,2,%r3,%r6
        SHLADD,<        %r1,2,%r3,%r6
        SHLADD,<=       %r1,2,%r3,%r6
        SHLADD,NUV      %r1,2,%r3,%r6
        SHLADD,ZNV      %r1,2,%r3,%r6
        SHLADD,SV       %r1,2,%r3,%r6
        SHLADD,OD       %r1,2,%r3,%r6
        SHLADD,TR       %r1,2,%r3,%r6
        SHLADD,<>       %r1,2,%r3,%r6
        SHLADD,>=       %r1,2,%r3,%r6
        SHLADD,>        %r1,2,%r3,%r6
        SHLADD,UV       %r1,2,%r3,%r6
        SHLADD,VNZ      %r1,2,%r3,%r6
        SHLADD,NSV      %r1,2,%r3,%r6
        SHLADD,EV       %r1,2,%r3,%r6

        SHRPW           %r1,%r2,1,%r3

        SPOP0,0,35

        SPOP1,3,35      %r6

        SPOP2,3,35      %r6

        SPOP3,3,35      %r6,%r7

        SSM             127,%r1

        STB             %r0,8(%sr1,%r3)
        STB,BC          %r0,8(%sr1,%r3)
        STB,SL          %r0,8(%sr1,%r3)

        STBS            %r0,8(%sr1,%r3)
        STBS,BC         %r0,8(%sr1,%r3)
        STBS,SL         %r0,8(%sr1,%r3)

        STBY            %r7,6(%sr1,%r30)
        STBY,B          %r7,6(%sr1,%r30)
        STBY,E          %r7,6(%sr1,%r30)
        STBY,M          %r7,6(%sr1,%r30)
        STBY,B,BC       %r7,6(%sr1,%r30)
        STBY,E,SL       %r7,6(%sr1,%r30)

        STBYS           %r7,6(%sr1,%r30)
        STBYS,B,M       %r7,6(%sr1,%r30)
        STBYS,E,M       %r7,6(%sr1,%r30)
        STBYS,M         %r7,6(%sr1,%r30)
        STBYS,B,BC      %r7,6(%sr1,%r30)
        STBYS,E,SL      %r7,6(%sr1,%r30)

; PA2.0 opcodes:
        STD             %r18,0(%sr3,%r29)

        STH             %r18,0(%sr3,%r29)

        STHS            %r18,0(%sr3,%r29)

        STW             %r17,3(0,%r1)

        STWA            %r16,0(%r6)

        STWM            %r16,0x7fff(%r6)

        STWAS,MA        %r16,2(%r6)

        STWS            %r16,0(%r6)

        SUB             %r1,%r0,%r3
        SUB,B           %r1,%r0,%r3
        SUB,DB          %r1,%r0,%r3
        SUB,TC          %r1,%r0,%r3
        SUB,TSV         %r1,%r0,%r3
        SUB,TSV,TC      %r1,%r0,%r3
        SUB,B,TSV       %r1,%r0,%r3
        SUB,B,TSV,<     %r1,%r0,%r3

        SUBB            %r8,%r9,%r10
        SUBB,<<=        %r8,%r9,%r10
        SUBB,>>=        %r8,%r9,%r10
        SUBB,NSV        %r8,%r9,%r10

        SUBBO           %r8,%r9,%r10
        SUBBO,<<=       %r8,%r9,%r10
        SUBBO,>>=       %r8,%r9,%r10
        SUBBO,NSV       %r8,%r9,%r10

        SUBI            9,%r3,%r5
        SUBI,TSV        2,%r3,%r5

        SUBIO           5,%r27,%r26
        SUBIO,<         5,%r27,%r26

        SUBO            %r8,%r9,%r10
        SUBO,<<=        %r8,%r9,%r10
        SUBO,>>=        %r8,%r9,%r10
        SUBO,NSV        %r8,%r9,%r10

        SUBT            %r8,%r9,%r10
        SUBT,<=         %r8,%r9,%r10
        SUBT,>=         %r8,%r9,%r10
        SUBT,NSV        %r8,%r9,%r10

        SUBTO           %r8,%r9,%r10
        SUBTO,<<=       %r8,%r9,%r10
        SUBTO,>>=       %r8,%r9,%r10
        SUBTO,NSV       %r8,%r9,%r10

        SYNC

        SYNCDMA

labelt
labelu
        UADDCM          %r3,%r4,%r5
        UADDCM,TC       %r3,%r4,%r5

        UADDCMT         %r3,%r4,%r5
        UADDCMT,SHC     %r3,%r4,%r5

        UXOR            %r19,%r3,%r20
        UXOR,SHZ        %r19,%r3,%r20

labelv
        VDEP            %r7,3,%r8
        VDEP,TR         %r2,3,%r8

        VDEPI           7,3,%r8
        VDEPI,=         2,3,%r8

        VEXTRS          %r4,30,%r4
        VEXTRS,<        %r4,30,%r4

        VEXTRU          %r4,30,%r4
        VEXTRU,>=       %r4,30,%r4

        VSHD            %r3,%r2,%r0
        VSHD,<          %r3,%r2,%r0

labelw
labelx
        XMPYU           %fr3,%fr4,%fr5

        XOR             %r0,%r1,%r2
        XOR,TR          %r0,%r1,%r2
        XOR,>=          %r0,%r1,%r2

labely
labelz
        ZDEP            %r18,1,2,%r2
        ZDEP,<>         %r18,2,3,%r2

        ZDEPI           1,1,2,%r2
        ZDEPI,EV        3,2,3,%r2

        ZVDEP           %r18,30,%r2
        ZVDEP,<         %r18,8,%r2

        ZVDEPI          15,30,%r2
        ZVDEPI,OD       8,8,%r2

        .exit
        .procend

mainend
        .proc
        .callinfo NO_CALLS,FRAME=0
        .entry

        NOP

        .exit
        .procend

        .end
