#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.*/
# Fortran common test
#
# Source file: common.f

if {![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0
set testfile "common"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/floatreg

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable {debug f77}] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set MAIN "main"
if {[istarget "hppa*-*-hpux*"]} { set MAIN "_main_"}
if {[istarget "ia64*-hp-*"]} {set MAIN "__mainprogram"}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

#
# set it up at a breakpoint so we can play with the variable values
#

if ![runto $MAIN ] {
    perror "couldn't run to breakpoint $MAIN"
    continue
}

# set up break points and get to first breakpoint

send_gdb "break subf\n"
gdb_expect {
  -re "Breakpoint $decimal at $hex: file .*$srcfile, line 17.*$gdb_prompt $" {
      pass "break subf"
   }
  -re ".*$gdb_prompt $" { fail "break subf" }
  timeout { fail "(timeout) break subf" }
}

send_gdb "break subg\n"
gdb_expect {
  -re "Breakpoint $decimal at $hex: file .*$srcfile, line 28.*$gdb_prompt $" {
      pass "break subg"
   }
  -re ".*$gdb_prompt $" { fail "break subg" }
  timeout { fail "(timeout) break subg" }
}

send_gdb "info reg \$fr10\n"
gdb_expect {
  -re "fr10.*$hex.*$gdb_prompt $" {
      pass "info reg \$fr10"
   }
  -re "fr10.*single precision.*fr10.*double.*$gdb_prompt $" {
      pass "info reg \$fr10"
  }
  -re "fr10.*double precision.*fr10L.*fr10R.*single.*$gdb_prompt $" {
      pass "info reg \$fr10"
  }
  -re ".*$gdb_prompt $" { fail "info reg \$fr10" }
  timeout { fail "(timeout) info reg \$fr10" }
}

send_gdb "print \$fr10\n"
gdb_expect {
  -re "\\$\[0-9\]* =.*$gdb_prompt $" {
      pass "print fp reg"
   }
  -re ".*$gdb_prompt $" { fail "print fp reg" }
  timeout { fail "(timeout) print fp reg" }
}

# verify fp reg within subf
send_gdb "continue\n"
gdb_expect {
  -re "Continuing.*Breakpoint $decimal, (subf|SUBF) \\(.*\\) at .*$srcfile:17.*$gdb_prompt $" { 
      pass "continue to subf" }
  -re ".*$gdb_prompt $" {
      fail "continue to subf"
   }
  timeout { fail "(timeout) continue to subf" }
}

send_gdb "info reg \$fr11\n"
gdb_expect {
  -re "fr11.*$hex.*$gdb_prompt $" {
      pass "info reg \$fr11"
   }
  -re "fr11.*single precision.*fr11.*double.*$gdb_prompt $" {
      pass "info reg \$fr11"
  }
  -re "fr11.*double precision.*fr11L.*fr11R.*single.*$gdb_prompt $" {
      pass "info reg \$fr11"
  }
  -re ".*$gdb_prompt $" { fail "info reg \$fr11" }
}

send_gdb "print \$fr11\n"
gdb_expect {
  -re "\\$\[0-9\]* =.*$gdb_prompt $" {
      pass "print fp reg"
   }
  -re ".*$gdb_prompt $" { fail "print fp reg" }
  timeout { fail "(timeout) print fp reg" }
}

send_gdb "continue\n"
gdb_expect {
  -re "Continuing.*Breakpoint $decimal, (subg|SUBG) \\(\\) at .*$srcfile:28.*$gdb_prompt $"
  {
      pass "continue to subg"
   }
  -re ".*$gdb_prompt $" { fail "continue to subg" }
  timeout { fail "(timeout) continue to subg" }
}

send_gdb "info reg \$fr12\n"
gdb_expect {
  -re "fr12.*$hex.*$gdb_prompt $" {
      pass "info reg \$fr12"
   }
  -re "fr12.*single precision.*fr12.*double.*$gdb_prompt $" {
      pass "info reg \$fr12"
  }
  -re "fr12.*double precision.*fr12L.*fr12R.*single.*$gdb_prompt $" {
      pass "info reg \$fr12"
  }
  -re ".*$gdb_prompt $" { fail "info reg \$fr12" }
  timeout  { fail "(timeout) info reg \$fr12" }
}

send_gdb "print \$fr12\n"
gdb_expect {
  -re "\\$\[0-9\]* =.*$gdb_prompt $" {
      pass "print fp reg"
   }
  -re ".*$gdb_prompt $" { fail "print fp reg" }
  timeout { fail "(timeout) print fp reg" }
}

gdb_exit
