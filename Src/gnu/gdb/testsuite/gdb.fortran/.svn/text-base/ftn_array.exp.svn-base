# Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# This file was written by baskar.
#
# Test whether "fortran array slice" works as supported by DDE
#

if $tracelevel then {
        strace $tracelevel
        }

if { [skip_hp_tests] } then { continue }

set prms_id 0
set bug_id 0

set testfile "ftn_array"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

# Build the test case

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug f77}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Start with a fresh gdb
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# set up the breakpoint and run to it
gdb_test "break 22"\
  "Breakpoint $decimal at $hex: file .*ftn_array.f, line 22."\
  "break point at end"

gdb_test "run" \
  "Starting program:.*Breakpoint $decimal,.*ftn_array.f:22.*"\
  "run to end"

# print single dimensional array/slice/sub_string
gdb_test "print another" \
    ".*= \\'godsaved  \\'" "print another"

gdb_test "print another(3,7)" \
    ".*= \\'dsave\\'" "print another(3,7)"

gdb_test "p another(:)" \
    ".*= \\'godsaved  \\'" "p another(:)"
gdb_test "p another(::)" \
    ".*= \\'godsaved  \\'" "p another(::)"
gdb_test "p another(::2)" \
    ".*= \\'godsaved  \\'" "p another(::2)"

gdb_test "p another(2:)" \
    ".*= \\'odsaved  \\'" "p another(2:)"
gdb_test "p another(2::)" \
    ".*= \\'odsaved  \\'" "p another(2::)"
gdb_test "p another(2::2)" \
    ".*= \\'odsaved  \\'" "p another(2::2)"

gdb_test "p another(:2)" \
    ".*= \\'go\\'" "p another(:2)"
gdb_test "p another(:2:)" \
    ".*= \\'go\\'" "p another(:2:)"
gdb_test "p another(:2:2)" \
    ".*= \\'go\\'" "p another(:2:2)"

gdb_test "p another(1:5)" \
    ".*= \\'godsa\\'" "p another(1:5)"
gdb_test "p another(1:5:)" \
    ".*= \\'godsa\\'" "p another(1:5:)"
gdb_test "p another(1:5:2)" \
    ".*= \\'godsa\\'" "p another(1:5:2)"

# print 2 dimensional array slice
gdb_test "p a" \
    ".*= \\(\\(11, 21, 0\\), \\(12, 2, 0\\), \\(0, 0, 0\\)\\)"

gdb_test "p a(:,2)" \
    ".*= \\(1,2\\) = 12.*\\(2,2\\) = 2.*\\(3,2\\) = 0.*"
gdb_test "p a(::,2)" \
    ".*= \\(1,2\\) = 12.*\\(2,2\\) = 2.*\\(3,2\\) = 0.*"
gdb_test "p a(::2,2)" \
   ".*= \\(1,2\\) = 12.*\\(3,2\\) = 0.*"

gdb_test "p a(1:,1)" \
   ".*= \\(1,1\\) = 11.*\\(2,1\\) = 21.*\\(3,1\\) = 0.*"
gdb_test "p a(1::,1)" \
   ".*= \\(1,1\\) = 11.*\\(2,1\\) = 21.*\\(3,1\\) = 0.*"
gdb_test "p a(1::3,1)" \
   ".*= \\(1,1\\) = 11.*"

gdb_test "p a(:2,1)" \
   ".*= \\(1,1\\) = 11.*\\(2,1\\) = 21.*"
gdb_test "p a(:2:,1)" \
   ".*= \\(1,1\\) = 11.*\\(2,1\\) = 21.*"
gdb_test "p a(:2:2,1)" \
   ".*= \\(1,1\\) = 11.*"

gdb_test "p a(1:3,2)" \
   ".*= \\(1,2\\) = 12.*\\(2,2\\) = 2.*\\(3,2\\) = 0.*"
gdb_test "p a(1:3:,2)" \
   ".*= \\(1,2\\) = 12.*\\(2,2\\) = 2.*\\(3,2\\) = 0.*"
gdb_test "p a(1:3:2,2)" \
   ".*= \\(1,2\\) = 12.*\\(3,2\\) = 0.*"

gdb_test "p a(::2,::2)" \
   ".*= \\(1,1\\) = 11.*\\(3,1\\) = 0.*\\(1,3\\) = 0.*\\(3,3\\) = 0.*"
gdb_test "p a(1::2,1::2)" \
   ".*= \\(1,1\\) = 11.*\\(3,1\\) = 0.*\\(1,3\\) = 0.*\\(3,3\\) = 0.*"
gdb_test "p a(:3:2,:3:2)" \
   ".*= \\(1,1\\) = 11.*\\(3,1\\) = 0.*\\(1,3\\) = 0.*\\(3,3\\) = 0.*"
gdb_test "p a(1:3:2,1:3:2)" \
   ".*= \\(1,1\\) = 11.*\\(3,1\\) = 0.*\\(1,3\\) = 0.*\\(3,3\\) = 0.*"

gdb_exit
return 0
