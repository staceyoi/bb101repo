# Author: Ovidiu Predescu <ovidiu@cup.hp.com>, Sept 2, 1998
#
# Check how the intrinsic Fortran types and values are displayed

if {![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

set testfile "intrinsics"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

proc check_variable {name type value} {
    gdb_test "ptype $name" "$type" "type of $name"
    gdb_test "print $name" "$value" "value of $name"
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable {debug f77}] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set MAIN "main"
if {[istarget "hppa*-*-hpux*"]} { set MAIN "_main_" }
if {[istarget "ia64*-hp-*"]} {set MAIN "__mainprogram"}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if ![runto $MAIN ] {
    perror "couldn't run to breakpoint $MAIN"
    continue
}

check_variable i2 "integer\\*2" 4
check_variable j2 "integer\\*2" 2

check_variable i4 "integer\\*4" 4
check_variable j4 "integer\\*4" 2

check_variable r4 "real\\*4" "4.*"
check_variable s4 "real\\*4" "2.*"

check_variable r8 "real\\*8" "4.*"
check_variable s8 "real\\*8" "2.*"

check_variable r16 "real\\*16" "4.*"
check_variable s16 "real\\*16" "2.*"

check_variable c4 "complex\\*8" "\\(1,[ ]*2\\)"
check_variable d4 "complex\\*8" "\\(2,[ ]*3\\)"

check_variable c8 "complex\\*16" "\\(1,[ ]*2\\)"
check_variable d8 "complex\\*16" "\\(2,[ ]*3\\)"

check_variable ch1 "character\\*1" "'1'"
check_variable ch2 "character\\*1" "'2'"

check_variable l1 "logical\\*1" ".FALSE."
check_variable m1 "logical\\*1" ".TRUE."

check_variable l2 "logical\\*2" ".FALSE."
check_variable m2 "logical\\*2" ".TRUE."

check_variable l4 "logical\\*4" ".FALSE."
check_variable m4 "logical\\*4" ".TRUE."

