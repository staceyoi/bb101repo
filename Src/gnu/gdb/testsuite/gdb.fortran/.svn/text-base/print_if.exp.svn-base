# Written by Jini Susan George (jini@hp.com)
# This is used to test JAGag29592. We need to check if the type length of a
# fortran assumed array remains the same after a call to backtrace or info
# frame <frame nbr>, where <frame nbr> is a frame other than the current frame.
#
# Source file: fortran_entry_type_len.f


if $tracelevel {
    strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "print_if"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f90_compiler } {
    return 0
}


if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable "debug f77"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break print_if" ".*Breakpoint 1.*print_if.f.*" "break at main"
gdb_test "run" "Starting program.*Breakpoint 1.*print_if.*if.*10.*" "first breakpoint"
gdb_test "s" ".*else.*20.*" "single step"
gdb_test "p if" ".*1.*10.*" "Print the value of if"
gdb_test "s" ".*then.*30.*" "single step"
gdb_test "p else" ".*20.*" "Print the value of else"
gdb_test "b 11 if (if .EQ. 10)" ".*Breakpoint 2.*file.*print_if.f.*line 11.*" "Conditional brkpoint"
gdb_test "c" ".*Breakpoint 2.*print_if.*" "second breakpoint"
gdb_exit

