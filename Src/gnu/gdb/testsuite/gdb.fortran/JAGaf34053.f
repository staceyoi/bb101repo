      program debugstep

      USE mod_problem

      implicit none

      call dum()
      call sub()

      end program debugstep


      subroutine sub

      USE mod_problem

      implicit none

       type(boo) :: step0
       type(boo) :: step1
       type(boo) :: step2
       type(boo) :: step3
       type(boo) :: step4

       call init(step0)
       call init(step1)
       call init(step2)
       call init(step3)
       call init(step4)

      end subroutine sub

      subroutine dum
       implicit none
       integer                 :: i
       integer, dimension(100) :: ii
       do i=1,100
         ii(i)=19956759*i
       enddo
      end subroutine dum



