      program test_l1
C	JAGaf66117, "printing Fortran array sections longer than 
C	200 elements prints garbage"
      real  hypot, area

      integer           gi1
      integer*2         gi2
      complex           gx1

      real              rarray1
      dimension         rarray1(2000)
      data rarray1 /2000*0.0d0/

      character*32  strings(5000)
      data strings /' ', ' ', ' ', ' ', ' '/

      integer     LMAX
      common /priority_queue/ LMAX

      hypot(a, b) = sqrt(a**2 + b**2)
      area(a, b) = a*b/2.0

      DO gi1 = 1, 20
         rarray1(gi1) = dble(gi1)
      END DO
      DO gi1 = 1980, 1999
         rarray1(gi1) = dble(gi1)
      END DO

      LMAX = 10

      gi1 = O'377'
      gi2 = Z'FFFF'
      
      gx1 = (255, 4095)

      strings(1)(5:8) = 'the '
      strings(2)(1:4) = 'art '
      strings(3)(17:20) = 'of '
      strings(4)(1:20) = 'scientific ' // 'computing'
      strings(500) = strings(1)(5:8)   // strings(2)(1:4)  // 
     *             strings(3)(17:20) // strings(4)(1:20)
      
      rarray1(1000) = hypot(1.0,2.0)   
      rarray1(2000) = area(1.0,2.0)

      stop

      end
