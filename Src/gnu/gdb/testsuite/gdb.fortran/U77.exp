# Written by Jini Susan George (jini@hp.com)
#
# This is used to test JAGag44406. We need to test if we can set breakpoints
# by providing <filename>::<line number> for fortran applications compiled
# with +U77 on IA-64.
#
# Source file used: u77.f
# Different paths are used for compilation to check if we are OK with all the
# different subfile names in start_subfile().


if {![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "u77"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

# Compiles "../../../../Src/gnu/gdb/testsuite/gdb.fortran/u77.f"

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable "debug f77 additional_flags=+U77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

catch "exec /bin/cp -f ${srcdir}/${subdir}/${srcfile} . "

# Compiles "u77.f"

if {[gdb_compile "$srcfile" "$binfile" executable "debug f77 additional_flags=+U77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

# Compiles "./u77.f"

if {[gdb_compile "./${srcfile}" "$binfile" executable "debug f77 additional_flags=+U77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

catch "exec /bin/mkdir tempdir"
catch "exec /bin/mv ./${srcfile} tempdir"

# Compiles "tempdir/u77.f"

if {[gdb_compile "tempdir/${srcfile}" "$binfile" executable "debug f77 additional_flags=+U77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

# Compiles "tempdir/./u77.f"

if {[gdb_compile "tempdir/./${srcfile}" "$binfile" executable "debug f77 additional_flags=+U77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

catch "exec /bin/cp -f ${srcdir}/${subdir}/${srcfile} /tmp"
catch "exec /bin/rm -rf tempdir"

# Compiles "/tmp/u77.f"

if {[gdb_compile "/tmp/${srcfile}" "$binfile" executable "debug f77 additional_flags=+U77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

catch "exec /bin/rm -rf /tmp/${srcfile}"
set binfile ${objdir}/${subdir}/${testfile}.shared

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable "debug f77 additional_flags=+U77 +sharedlibU77 -g"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break u77.f:2" ".*Breakpoint 1.*u77.f.*line 2.*" "break at u77.f:2"
gdb_test "run" ".*mario.*u77.f:2.*" "run to mario"

gdb_exit

