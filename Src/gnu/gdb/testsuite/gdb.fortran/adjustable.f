C  Explicit-shape Arrays with non-constant bounds
      program adjustable

      integer one_dimen(5)
      data one_dimen / 1, 2, 3, 4, 5/

      integer two_dimen(3,3)
      data two_dimen / 10, 20, 30, 40, 50, 60, 70, 80, 90 /

      call subr (one_dimen, two_dimen, 5, 3)

      end

C list1 and list2 are adjustable arrays -- dummy argument with at least
C one non-constant bound
      subroutine subr(list1, list2, m, n)
        integer m, n
        integer list1(m)
        integer list2(0:n-1,-n:-n+2)
      end subroutine subr
