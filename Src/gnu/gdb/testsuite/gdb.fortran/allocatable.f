      program allocatable_array
      
      call foo (3)
      
      end
      
      subroutine foo (n)
        integer n
        integer, allocatable, dimension(:) :: a1
        integer, allocatable, dimension(:,:) ::a2
      
        allocate (a1(n))
        a1 = 0
        a1(n-1) = n
        allocate (a2(n,n))
        a2 = 0
        a2(n-1,n-1) = n 
        stop
      end
