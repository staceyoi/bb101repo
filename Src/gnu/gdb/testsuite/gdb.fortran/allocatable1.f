       program complex_allocatable_array
       type array_ptr
          integer   :: myvar
          integer, dimension (:), pointer :: ap
       end type array_ptr

       type (array_ptr), allocatable, dimension (:) :: arrays

       allocate (arrays(5))
       do i = 1,5
          arrays(i)%myvar  = i
          allocate (arrays(i)%ap(i))
          print *, arrays(i)%myvar
          do j = 1,i
            arrays(i)%ap(j) = i
C           print *, arrays(i)%ap(j)
          end do
       end do

       stop
       end
