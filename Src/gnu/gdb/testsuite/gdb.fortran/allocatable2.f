        program structure_type
 
        structure /S/
          integer * 2     i2
          logical * 1     l1
          character (4)   four_c
          real*8          r8
          double complex  dcm
        end structure
 
        record /S/ r_arr (:)
	allocatable r_arr

	allocate(r_arr(10))
 
        r_arr(1).i2     = 2
        r_arr(1).l1     = 1
        r_arr(1).four_c = 'dd'
        r_arr(1).r8     = 3.5
        r_arr(1).dcm    = (11.5, 12.5)
 
        r_arr(2).i2     = 3
        r_arr(2).l1     = 2
        r_arr(2).four_c = 'ee'
        r_arr(2).r8     = 4.5
        r_arr(2).dcm    = (12.5, 13.5)

        print *, r_arr(1).i2
 
        end
