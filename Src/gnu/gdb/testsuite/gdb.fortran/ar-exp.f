C explicit bound arrays

      integer double_dimen(3,4)
      data ((double_dimen(i,j),j=1,4),i=1,3) 
     1       /1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12/
      
      integer double_dimen1(3,4)
      data double_dimen1 /1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12/

      integer three_dimen(2,3,4)
      data three_dimen /10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110,
     1                  120, 130, 140, 150, 160, 170, 180, 190, 200,
     1                  210, 220, 230, 240/
      
      real one_dimen(10)
      data one_dimen /1.1,2.2,3.3,4.4,5.5,5*6.6/
      
      complex cplx_arr (3)
      data cplx_arr / (1.2,2.3), (3.4,4.5),(5.6,6.7) /

      integer, dimension (-2:3) :: bounds_arr
      data bounds_arr  /-2, -1, 0, 1, 2, 3/

      integer, dimension (-2:0, 5:7, -4:-2) :: bounds_arr3
      data bounds_arr3 /-13, -12, -11, -10, -9, -8, -7, -6, -5, 
     1                   -4,  -3, -2,   -1,  0,  1,  2,  3,  4,
     1                    5,   6,  7,    8,  9, 10, 11, 12, 13/

      integer max_dimen (2,2,2,2,2,2,2)
      data max_dimen / 128*2/

      integer i,j,k

      print *, "double_dimen = ", double_dimen
      print *, "double_dimen1 = ", double_dimen1
      print *, "one_dimen = ", one_dimen
      print *, "cplx_arr = ", cplx_arr
      print *, "bounds_arr = ", bounds_arr
      print *, "bounds_arr3 = ", bounds_arr3
      print *, "max_dimen = ", max_dimen 

      print *, "double_dim(1,2)", double_dimen(1,2)
      print *, "double_dim1(1,2)", double_dimen1(1,2)

      do k = 1, 4
        do j = 1, 3
          do i = 1, 2
            print *, "three_dimen(", i, ",", j, ",", k, ") = ",
     1            three_dimen(i,j,k)
          enddo
        enddo
      enddo

      max_dimen (1,2,1,2,1,2,1) = 10
      print *, "max_dimen(1,2,1,2,1,2,1)", max_dimen(1,2,1,2,1,2,1)

      end
