#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997, 1998
#   Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# cllgdb@india.hp.com

# Author: P.M. Srividhya December, 2003
#
# Added for JAGae92961 & JAGae91779 - gdb dumps core on bt when we pass assumed-size arrays as 
# function arguments. This happens in 32-bit applications only.

if { ![istarget "hppa1.1-hp-hpux*"] } {
    return 0
}

set testfile "ass_args"
set srcfile $testfile.f
set binfile ${objdir}/${subdir}/$testfile

set timeout 60

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f90_compiler } {
    return 0
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable "debug f77 {additional_flags=+extend_source -O0}"] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if ![runto \$\$divI] {
    perror "couldn't run to breakpoint \$\$divI"
    continue
}
send_gdb "backtrace\n" 
gdb_expect {
   -re  "divI.*buggy.*assumed size array.*" {
         pass "Backtrace command"
      }
   -re  "divI.*gdb-internal-error*" {
         fail "Backtrace command"
      }
  timeout	{ fail "(timeout) backtrace"}   
  }

gdb_exit
