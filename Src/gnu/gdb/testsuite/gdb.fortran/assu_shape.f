C explicit bound arrays
      program assumed_shape_array

      integer :: parts(0:5)
      complex :: coeffs (5)
      real :: omega (-2:1, -1:1, 0:1)

      data parts /6 * 1/
      data coeffs /5 * (1.5, 2.5)/
      data omega /24 * 3.0/
     
#if F90
      interface
        subroutine initialize (a, b, c, n)
        integer :: a(:)
        complex :: b (abs(n):)
        real, dimension (:, :, :) ::c
        integer :: n
        end subroutine initialize
      end interface
#endif
      
      call initialize (parts, coeffs, omega, lbound (omega,1));
      
      end

C assumed shape arrays -- dummy argument that assumes shape of
C corresponding actual argument

      subroutine  initialize (a, b, c, n)
      integer :: a(:)
      complex :: b (abs(n):)
      real, dimension (:, :, :) ::c
      integer :: n
      end

