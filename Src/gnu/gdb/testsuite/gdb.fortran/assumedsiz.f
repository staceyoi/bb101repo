      program assumedsiz

      integer a(2,3)
      data a/1, 2, 3, 4, 5, 6/

      integer a3(2,2,2)
      data a3 / 1,  2,  3,  4,  5,  6,  7,  8 /

      call subr (a)
      call subr3 (a3, 2)

      end

      subroutine subr (arr)
      integer arr (*)
      end

      subroutine subr3 (arr3, n)
      integer, dimension(n, -n:-n+1, 3:*) :: arr3
      integer n
      integer i, j, k

      do k = 3, 4
        do j = -n, -n+1
          do i = 1, n
            print *, i, ",", j, ",", k, ": ", arr3(i,j,k)
          enddo
        enddo
      enddo

      end
