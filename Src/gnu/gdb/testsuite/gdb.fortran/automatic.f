C  Explicit-shape Arrays with non-constant bounds
      program automatic

      call subr (2,2)

      end

C list1, list2 and list3  are automatic arrays -- non-dummy argument with 
C a non-constant bound

      subroutine subr(m, n)
        integer list1 (n)
        integer list2 (n,5)
        integer list3 (0:m-1,-m:n-1)


        do i = 1,n
          list1(i) = i
          print *, "list1(", i, " ):", list1(i)
        enddo

        do j = 1,5
          do i = 1,n
            list2(i,j) = i+j
            print *, "list2(", i, ",", j, " ):", list2(i,j)
          enddo
        enddo
        print *, "list2: ", list2

        do j = -m, n-1
          do i = 0, m-1
            list3(i,j) = i-j
            print *, "list3(", i, ",", j, " ):", list3(i,j)
          enddo
        enddo
        print *, "list3: ", list3

        stop

      end subroutine subr
