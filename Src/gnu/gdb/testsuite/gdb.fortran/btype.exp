#   Copyright (C) 1988, 1990, 1991, 1992, 1994 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This file was written by Sue Kimura. (sue_kimura@hp.com)

#Byte type is an HP-extension so only test only on hpux systems.
if {![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "btype"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable {debug f77}] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

if {$hp_f77_compiler} {
    set ptype_b        " = logical\\*1"
    set print_b_first  " = .TRUE."
    set print_b_second " = .TRUE."
    set print_b_third  " = .FALSE."
} else {
    set ptype_b        " = integer\\*1"
    set print_b_first  " = -128"
    set print_b_second " = 127"
    set print_b_third  " = 0"
}

set MAIN "main"
if {[istarget "hppa*-*-hpux*"]} { set MAIN "_main_"}
if {[istarget "ia64*-hp-*"]} {set MAIN "byte_type"}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

#
# set it up at a breakpoint so we can play with the variable values
#

if ![runto $MAIN ] then {
    perror "couldn't run to breakpoint $MAIN"
    continue
}

#test byte type variables

gdb_test "ptype b"   "$ptype_b"         "ptype b"
gdb_test "print b"   "$print_b_first"   "print b"
gdb_test "x /x \&b"  ".*:.*0x80000000"  "hex value of b"

gdb_test "print b = 127" "$print_b_second"  "print b = 127"
gdb_test "x /x \&b"      ".*:.*0x7f000000"  "hex value of b = 127"
gdb_test "p b = 0 "      "$print_b_third"   "print b = 0"
gdb_test "x /x \&b"      ".*:.*0x00000000"  "hex value of b  = 0"
