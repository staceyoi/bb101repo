C Test for common -- a in main is not in the same common block as are the 
C a's in subf and subg

       integer ::a = 0

       a=3
       print *, "main::a =", a
       call subf()
       print *, "main::a =", a
       end


       subroutine subf()
       integer a
       common a

       a=4
       print *, "subf::a =", a
       call subg()
       print *, "subf::a =", a
       return
       end

       subroutine subg()
       integer a
       common a

       print *, "subg::a =", a
       a=5
       print *, "subg::a =", a
       return
       end
