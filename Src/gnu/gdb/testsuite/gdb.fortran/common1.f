C Similar test to common.f, but here a in _MAIN_ is also in the
C same common block as the a's in subf and subg

       integer a
       common a

       a=3
       print *, "main::a =", a
       call subf()
       print *, "main::a =", a
       end


       subroutine subf()
       integer a
       common a

       print *, "subf::a =", a
       a=4
       print *, "subf::a =", a
       call subg()
       print *, "subf::a =", a
       return
       end


       subroutine subg()
       integer a
       common a

       print *, "subg::a =", a
       a=5
       print *, "subg::a =", a
       return
       end
