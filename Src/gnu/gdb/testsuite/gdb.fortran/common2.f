C subh and subi use common "c" but with different types for a

       integer a
       common a

       a=3
       print *, "main::a =", a
       call subh()
       print *, "main::a =", a
       end


       subroutine subh()
       integer a
       common /c/ a

       a=6
       print *, "subh::a =", a
       call subi()
       print *, "subh::a =", a
       return
       end


       subroutine subi()
       real a
       common /c/ a

       print *, "subi::a =", a
       a=7.5
       print *, "subi::a =", a
       return
       end
