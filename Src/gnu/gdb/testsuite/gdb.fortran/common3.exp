# This file was written by Sue Kimura. (sue_kimura@hp.com)
#
# Fortran common test
#
# Source file: common3.f

if {![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "common3"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable {debug f77}] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set MAIN "main"
if {[istarget "hppa*-*-hpux*"]} { set MAIN "_main_"}
if {[istarget "ia64*-hp-*"]} {set MAIN "__mainprogram"}

set info_common 1
if {[istarget "ia64*-hp-*"]} {set info_common 0}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

#
# set it up at a breakpoint so we can play with the variable values
#

if ![runto $MAIN ] {
    perror "couldn't run to breakpoint $MAIN"
    continue
}

# set up break points and get to first breakpoint

gdb_test "break 9" \
    "Breakpoint $decimal at $hex: file .*$srcfile, line 9." \
    "break in $MAIN:9"

gdb_test "break 12" \
    "Breakpoint $decimal at $hex: file .*$srcfile, line 12." \
    "break in $MAIN:12"

gdb_test "break 34" \
    "Breakpoint $decimal at $hex: file .*$srcfile, line 34." \
    "break in f:34"

gdb_test "break 43" \
    "Breakpoint $decimal at $hex: file .*$srcfile, line 43." \
    "break in f:43"

# try info command in $MAIN

gdb_test "continue" \
    "Continuing.*Breakpoint $decimal, $MAIN \\(.*\\) at .*$srcfile:9.*" \
    "continue to $MAIN:9"

gdb_test "print a"  " = 3"  "$MAIN: print common a before call to f"
gdb_test "print b"  " = 4"  "$MAIN: print common b before call to f"

if {$info_common} {
gdb_test "info common" \
    "All COMMON blocks visible at this level:\r\n\r\n_BLNK" \
    "$MAIN: info common"

gdb_test "info common _BLNK" \
    "Contents of COMMON block '_BLNK':\r\n\r\na = 3\r\nb = 4" \
    "$MAIN: info common _BLNK"
}

# continue to function f

gdb_test "continue" \
    "Continuing.*Breakpoint $decimal, f \\(\\) at .*$srcfile:34.*" \
    "continue to f:34"

gdb_test "print a"  " = 3"  "f: print common a before assignment"

if {$info_common} {
gdb_test "info common" \
    "All COMMON blocks visible at this level:\r\n\r\n(_BLNK\r\nbcommon|bcommon\r\n_BLNK)" \
  "f: info common"

gdb_test "info common _BLNK" \
    "Contents of COMMON block '_BLNK':\r\n\r\n.*a = 3" \
    "f: info common _BLNK"
}

gdb_test "continue" \
    "Continuing.*Breakpoint $decimal, f \\(\\) at .*$srcfile:43.*" \
    "continue to f:43"

gdb_test "print a"   " = 5"   "f: print common a after assignment"
gdb_test "print b"   " = 6"   "f: print local b after assignment"
gdb_test "print b1"  " = 55"  "f: print common b1"
gdb_test "print b2"  " = 66"  "f: print common b2"

if {$info_common} {
gdb_test "info common _BLNK" \
    "Contents of COMMON block '_BLNK':\r\n\r\na = 5" \
    "f: info common _BLNK after assignments "

gdb_test "info common bcommon" \
    "Contents of COMMON block 'bcommon':\r\n\r\nb1 = 55\r\nb2 = 66" \
    "f: info common bcommon"
}

# return to main

gdb_test "continue" \
    "Continuing.*Breakpoint $decimal, $MAIN \\(.*\\) at .*$srcfile:12.*" \
    "continue to main:12"

gdb_test "print a"  " = 5"  "$MAIN: print common a after returning from f"
gdb_test "print b"  " = 4"  "$MAIN: print common b after returning from f"

if {$info_common} {
gdb_test "info common _BLNK" \
    "Contents of COMMON block '_BLNK':\r\n\r\na = 5\r\nb = 4" \
    "$MAIN: info common _BLNK after call to f"
}

gdb_test "continue" \
    "Continuing.*Program exited normally." \
    "$MAIN: continue to end of program"
