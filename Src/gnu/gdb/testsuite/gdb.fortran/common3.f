C Test with multiple variables in a common block

       integer a
       integer b
       common a, b

       a=3
       b=4
       print *, 'main: a=', a
       print *, 'main: b=', b
       call f()
       print *, 'main: a=', a
       print *, 'main: b=', b
       end


       subroutine f()
       integer z

       integer a
       common a

       integer b
       integer y

       integer b1
       common /bcommon/ b1

       integer x 

       integer b2 
       common /bcommon/ b2 

       print *, 'f: a=', a
       print *, 'f: b=', b
       z  = 100
       a  = 5
       b  = 6
       y  = 200
       b1 = 55
       x  = 100
       b2 = 66
       print *, 'f: a=', a
       print *, 'f: b=', b
       print *, 'f: b1=', b1
       print *, 'f: b2=', b2
       return
       end
