      real*4            rarray1(20)
      real*4            rarray2(0:39)
      complex           xarray1(20)
     
      equivalence       ( rarray1, xarray1 )
      equivalence       ( rarray2, xarray1 )

      data (xarray1(i), i=1,17,4)/5*(1,2)/
      data (xarray1(i), i=2,18,4)/5*(2,3)/
      data (xarray1(i), i=3,19,4)/5*(3,4)/
      data (xarray1(i), i=4,20,4)/5*(4,5)/

      integer     ::foo_ = 2
      integer     bAr$
      integer     THIS_is_A_really_LONG_$00$_ident
 
      equivalence (foo_, bAr$, THIS_is_A_really_LONG_$00$_ident)

      integer           iarray1 (3)
      integer           iarray2 (5)

      equivalence (iarray1(2), iarray2(4)) 

      data iarray1 / 1, 2, 3 /
      data iarray2 / 4, 5, 6, 7, 8 /
      
      integer           iarray3 (6)
      integer           iarray4 (6)

      common irray1 iarray3 
      equivalence (iarray3(3), iarray4 (2))

      data iarray3 / 11, 12, 13, 14, 15, 16 /
      data iarray4 / 17, 18, 19, 20, 21, 22 /
      
      print *, "foo_ = ", foo_
      print *, "bAr$ = ", bAr$ 
      print *, "THIS_is_A_really_LONG_$00$_ident =", 
     1          THIS_is_A_really_LONG_$00$_ident

      print *, "rarray1 = ", rarray2
      print *, "rarray2 = ", rarray2
      print *, "xarray1 = ", xarray1

      print *, "iarray1 = ", iarray1
      print *, "iarray2 = ", iarray2
      print *, "iarray3 = ", iarray3
      print *, "iarray4 = ", iarray4

      end
