# This file was written by Sue Kimura. (sue_kimura@hp.com)

# This file tests the up, down, backtrace and finish commands on Fortran 
# programs.
#
# Source file: frames.f

if {![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "frames"
set srcfile ${testfile}.f
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable {debug f77}] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set MAIN "main"
if { [istarget "hppa*-*-hpux*"] } { set MAIN "_main_" }

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

#
# set it up a breakpoint 
#
if ![runto ftn4 ] {
   perror "couldn't run to breakpoint ftn4"
   return -1
}

# test the up command

gdb_test "up" \
    "#1.*$hex in ftn3 \\(three=2\\) at .*$srcfile.*call ftn4 \\(three\\)" \
    "up once in ftn4 "

gdb_test "up" \
    "#2.*$hex in ftn2 \\(two=2\\) at .*$srcfile.*call ftn3 \\(dble \\(two\\)\\)" \
    "up twice in ftn4 "

gdb_test "up" \
    "#3.*$hex in ftn1 \\(one=2\\) at .*$srcfile.*call ftn2 \\(float \\(one\\)\\)" \
    "up three times in ftn4 "

gdb_test "up" \
    "#4.*$hex in $MAIN \\(.*\\) at .*$srcfile.*call ftn1 \\(int \\(i2\\)\\)" \
    "up four times in ftn4 "

gdb_test "up" \
    "Initial frame selected; you cannot go up." \
    "up five times in ftn4 "

# test the down command
gdb_test "down" \
    "#3.*$hex in ftn1 \\(one=2\\) at .*$srcfile.*call ftn2 \\(float \\(one\\)\\)" \
    "down once from $MAIN in ftn4 "

gdb_test "down" \
    "#2.*$hex in ftn2 \\(two=2\\) at .*$srcfile.*call ftn3 \\(dble \\(two\\)\\)" \
    "down twice from $MAIN in ftn4 "

gdb_test "down" \
    "#1.*$hex in ftn3 \\(three=2\\) at .*$srcfile.*call ftn4 \\(three\\)" \
    "down three times from $MAIN in ftn4 "

gdb_test "down" \
    "#0.*ftn4 \\(four=\\(2,0\\)\\) at .*$srcfile.*print \\*, four" \
    "down four times from $MAIN in ftn4"

gdb_test "down" \
    "Bottom \\(i.e., innermost\\) frame selected; you cannot go down." \
    "down five times from $MAIN in ftn4"

#test backtrace command

gdb_test "backtrace" \
    "#0  ftn4 \\(four=\\(2,0\\)\\) at .*$srcfile.*#1  $hex in ftn3 \\(three=2\\) at .*$srcfile.*#2  $hex in ftn2 \\(two=2\\) at .*$srcfile.*#3  $hex in ftn1 \\(one=2\\) at .*$srcfile.*#4  $hex in $MAIN \\(.*\\) at .*$srcfile.*" \
    "backtrace from ftn4"

#test finish command

gdb_test "finish" \
    "Run till exit from #0  ftn4 \\(four=\\(2,0\\)\\) at .*$srcfile.*ftn3 \\(three=2\\) at .*$srcfile.*(print \\*, three|call ftn4 \\(three\\)).*" \
    "finish from ftn4"

gdb_test "finish" \
    "Run till exit from #0  ($hex in |)ftn3 \\(three=2\\) at .*$srcfile.*ftn2 \\(two=2\\) at .*$srcfile.*(print \\*, two|call ftn3 \\(dble \\(two\\)\\)).*" \
    "finish from ftn3"

gdb_test "finish" \
    "Run till exit from #0  ($hex in |)ftn2 \\(two=2\\) at .*$srcfile.*ftn1 \\(one=2\\) at .*$srcfile.*(print \\*, one|call ftn2 \\(float \\(one\\)\\)).*" \
    "finish from ftn2"

gdb_test "finish" \
    "Run till exit from #0  ($hex in |)ftn1 \\(one=2\\) at .*$srcfile.*$MAIN \\(.*\\) at .*$srcfile.*(print \\*, i2|call ftn1 \\(int \\(i2\\)\\)).*" \
    "finish from ftn1"

gdb_test "finish" \
    ".*finish.* not meaningful in the outermost frame." \
    "finish from $MAIN"
