C
C  FORTRAN language test program for the debugger - level 2
C

       program main
       integer*2 i2 
       i2 = 2
       call ftn1 (int (i2))
       print *, i2
       end

       subroutine ftn1 ( one )
       integer*4 one
       call ftn2 (float (one))
       print *, one
       end

       subroutine ftn2 ( two )
       real*4 two
       call ftn3 (dble (two))
       print *, two
       end

       subroutine ftn3 ( three )
       real*8 three
       call ftn4 (three)
       print *, three
       end

       subroutine ftn4 ( four )
       complex*8 four
       print *, four
       end
