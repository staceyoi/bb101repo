       program array
c      Program that demonstrates decl of Array variables 
       
       REAL*4 a(3,3)
       CHARACTER*10 another
       INTEGER*4 b(3,3,3)

       another="godsaved"
       a=0
       a(1,1)=11
       a(1,2)=12
       a(2,1)=21
       a(2,2)=2
       b(1,1,1)=111
       b(1,1,2)=112
       b(1,2,1)=121
       b(1,2,2)=122
       b(2,1,1)=211
       b(2,1,2)=212
       b(2,2,1)=221
       b(2,2,2)=222
       end
