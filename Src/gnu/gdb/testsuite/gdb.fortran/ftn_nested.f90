program testwdb
  implicit none

  integer, target :: s_number(2)
  integer,pointer :: ptr(:)

  type Struktur
    integer :: index
    real    :: zahl
  end type Struktur
  type (Struktur), allocatable :: s(:)

  ! ptr showed to s_number 
  s_number = 0

  ptr => s_number(1:1)
  s_number = 1
  print *, ptr(1)
  print *, 's_number=',s_number

  CALL READ_DATA

  allocate (s(1:1))
  s%index = 1
  s%zahl = 10.

  CALL READ_DATA1

  print *, s

  CONTAINS 

  SUBROUTINE READ_DATA
      REAL :: A_VAR
      A_VAR = 1.99
      PRINT *, A_VAR
  END SUBROUTINE READ_DATA

  SUBROUTINE READ_DATA1
      REAL :: A1_VAR
      A1_VAR = 2.99
      PRINT *, A1_VAR
  END SUBROUTINE READ_DATA1

end program testwdb

