         program fundamental_types

         integer   ::i  = 2147483647
         integer * 2 ::i2 = 2
         integer * 4 ::i4 = 4
 
         logical     ::l  = 0
         logical * 1 ::l1 = 1
         logical * 2 ::l2 = 2
         logical * 4 ::l4 = 4
 
         character     ::c = "a"
         character (1) ::one_c = "b"
         character (2) ::two_c = "cc"
         character (4) ::four_c = "dddd"

         end
