program testwdb
  implicit none

  integer, target :: i1
  integer, target :: i2(2)
  integer,pointer :: p1
  integer,pointer :: p2(:)
  integer,dimension(6) :: a

  i1 = 1
  i2 = 1

  call sub0(p1, i1)
  call sub1(p2, i2)
  call sub2(a, i1)
  i1 = 3
  i2 = 3
  p1 => i1
  p2 => i2(1:1)
  print *, p1
  print *, p2

end program testwdb

  subroutine sub0(t, j)
  integer, pointer :: t
  integer, target :: j

  j = 2
  print *, j
  end subroutine sub0

  subroutine sub1(t, j)
  integer, pointer :: t(:)
  integer, target :: j

  j = 2
  print *, j
  end subroutine sub1

  subroutine sub2(t, j)
  integer, dimension(6) :: t
  integer, target :: j

  j = 2
  print *, j
  end subroutine sub2

