C Commented statements have illegal type arguments
         program intrinsics

         integer * 1  ::i1   = 101
         integer * 1  ::i1_1 = 1
         integer * 8  ::i8   = 108
         integer * 8  ::i8_1 = 8
         logical * 8  ::l8 = .TRUE.

C conversion to complex
         print *, "cmplx (i1) = ",  cmplx (i1)
         print *, "cmplx (i8) = ",  cmplx (i8)
C        print *, "cmplx (l8) = ",  cmplx (l8)

C conversion to real*8
         print *, "dble (i1) = ",  dble (i1)
         print *, "dble (i8) = ",  dble (i8)
C        print *, "dble (l8) = ",  dble (l8)

C conversion from integer to real
         print *, "float (i1) = ",  float (i1)
         print *, "float (i8) = ",  float (i8)
C        print *, "float (l8) = ",  float (l8)

C conversion to integer
         print *, "int (i1) = ",  int (i1)
         print *, "int (i8) = ",  int (i8)
C        print *, "int (l8) = ",  int (l8)

C conversion from real to integer
C        print *, "ifix (i1) = ",  ifix (i1)
C        print *, "ifix (i8) = ",  ifix (i8)
C        print *, "ifix (l8) = ",  ifix (l8)

C conversion from real*8 to integer
C        print *, "idint (i1) = ",  idint (i1)
C        print *, "idint (i8) = ",  idint (i8)
C        print *, "idint (l8) = ",  idint (l8)

C conversion to real
         print *, "real (i1) = ",  real (i1)
         print *, "real (i8) = ",  real (i8)
C        print *, "real (l8) = ",  real (l8)

C conversion from real*8 to real*4
C        print *, "sngl (i1) = ",  sngl (i1)
C        print *, "sngl (i8) = ",  sngl (i8)
C        print *, "sngl (l8) = ",  sngl (l8)

C conversion from numeric to char
         print *, "char (i1) = ",  char (i1)
         print *, "char (i8) = ",  char (i8)
C        print *, "char (l8) = ",  char (l8)

C conversion from character to integer
C        print *, "ichar (i1) = ",  ichar (i1)
C        print *, "ichar (i8) = ",  ichar (i8)
C        print *, "ichar (l8) = ",  ichar (l8)

C absolute value
         print *, "abs (i1) = ",  abs (i1)
         print *, "abs (i8) = ",  abs (i8)
C        print *, "abs (l8) = ",  abs (l8)

C absolute value for integer
         print *, "iabs (i1) = ",  iabs (i1)
         print *, "iabs (i8) = ",  iabs (i8)
C        print *, "iabs (l8) = ",  iabs (l8)

# remainder
         print *, "mod (i1, i1_1) = ",  mod (i1, i1_1)
C        print *, "mod (i1, i8) = ",    mod (i1, i8)
C        print *, "mod (i1, 3) = ",     mod (i1, 3)

         print *, "mod (i8, i8_1) = ",  mod (i8, i8_1)
C        print *, "mod (i8, i1) = ",    mod (i8, i1)
C        print *, "mod (i8, 3) = ",     mod (i8, 3)

         end

