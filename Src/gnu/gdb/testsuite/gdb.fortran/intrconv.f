C Commented statements have illegal type arguments
         program intrinsics

         character    ::c1 = 'h'
         character*3  ::c3 = 'ijk'
         integer * 2  ::i2 = 102
         integer * 4  ::i4 = 104
         logical * 1  ::l1 = .TRUE.
         logical * 2  ::l2 = .TRUE.
         logical * 4  ::l4 = .TRUE.
         real * 4     ::r4 = 4.5
         real * 8     ::r8 = -8.5
         real * 16    ::r16 = -16.5
         complex * 8  ::z8 = (-8.5, 9.5)
         complex * 16 ::z16 = (-16.5, 17.5)

C conversion to complex
C        print *, "cmplx (c1) = ",  cmplx (c1)
         print *, "cmplx (i2) = ",  cmplx (i2)
         print *, "cmplx (i4) = ",  cmplx (i4)
C        print *, "cmplx (l1) = ",  cmplx (l1)
C        print *, "cmplx (l2) = ",  cmplx (l2)
C        print *, "cmplx (l4) = ",  cmplx (l4)
         print *, "cmplx (r4) = ",  cmplx (r4)
         print *, "cmplx (r8) = ",  cmplx (r8)
         print *, "cmplx (r16) = ", cmplx (r16)
         print *, "cmplx (z8) = ",  cmplx (z8)
         print *, "cmplx (z16) = ", cmplx (z16)

C conversion to real*8
C        print *, "dble (c1) = ",  dble (c1)
         print *, "dble (i2) = ",  dble (i2)
         print *, "dble (i4) = ",  dble (i4)
C        print *, "dble (l1) = ",  dble (l1)
C        print *, "dble (l2) = ",  dble (l2)
C        print *, "dble (l4) = ",  dble (l4)
         print *, "dble (r4) = ",  dble (r4)
         print *, "dble (r8) = ",  dble (r8)
         print *, "dble (r16) = ", dble (r16)
         print *, "dble (z8) = ", dble (z8)
         print *, "dble (z16) = ", dble (z16)

C conversion from integer to real
C        print *, "float (c1) = ",  float (c1)
         print *, "float (i2) = ",  float (i2)
         print *, "float (i4) = ",  float (i4)
C        print *, "float (l1) = ",  float (l1)
C        print *, "float (l2) = ",  float (l2)
C        print *, "float (l4) = ",  float (l4)
C        print *, "float (r4) = ",  float (r4)
C        print *, "float (r8) = ",  float (r8)
C        print *, "float (r16) = ",  float (r16)
C        print *, "float (z8) = ",  float (z8)
C        print *, "float (z16) = ",  float (z16)

C conversion to integer
C        print *, "int (c1) = ",  int (c1)
         print *, "int (i2) = ",  int (i2)
         print *, "int (i4) = ",  int (i4)
C        print *, "int (l1) = ",  int (l1)
C        print *, "int (l2) = ",  int (l2)
C        print *, "int (l4) = ",  int (l4)
         print *, "int (r4) = ",  int (r4)
         print *, "int (r8) = ",  int (r8)
         print *, "int (r16) = ", int (r16)
         print *, "int (z8) = ",  int (z8)
         print *, "int (z16) = ",  int (z16)

C conversion from real to integer
C        print *, "ifix (c1) = ",  ifix (c1)
C        print *, "ifix (i2) = ",  ifix (i2)
C        print *, "ifix (i4) = ",  ifix (i4)
C        print *, "ifix (l1) = ",  ifix (l1)
C        print *, "ifix (l2) = ",  ifix (l2)
C        print *, "ifix (l4) = ",  ifix (l4)
         print *, "ifix (r4) = ",  ifix (r4)
C        print *, "ifix (r8) = ",  ifix (r8)
C        print *, "ifix (r16) = ", ifix (r16)
C        print *, "ifix (z8) = ",  ifix (z8)
C        print *, "ifix (z16) = ", ifix (z16)

C conversion from real*8 to integer
C        print *, "idint (c1) = ",  idint (c1)
C        print *, "idint (i2) = ",  idint (i2)
C        print *, "idint (i4) = ",  idint (i4)
C        print *, "idint (l1) = ",  idint (l1)
C        print *, "idint (l2) = ",  idint (l2)
C        print *, "idint (l4) = ",  idint (l4)
C        print *, "idint (r4) = ",  idint (r4)
         print *, "idint (r8) = ",  idint (r8)
C        print *, "idint (r16) = ", idint (r16)
C        print *, "idint (z8) = ",  idint (z8)
C        print *, "idint (z16) = ", idint (z16)

C conversion to real
C        print *, "real (c1) = ",  real (c1)
         print *, "real (i2) = ",  real (i2)
         print *, "real (i4) = ",  real (i4)
C        print *, "real (l1) = ",  real (l1)
C        print *, "real (l2) = ",  real (l2)
C        print *, "real (l4) = ",  real (l4)
         print *, "real (r4) = ",  real (r4)
         print *, "real (r8) = ",  real (r8)
         print *, "real (r16) = ", real (r16)
         print *, "real (z8) = ",  real (z8)
         print *, "real (z16) = ", real (z16)

C conversion from real*8 to real*4
C        print *, "sngl (c1) = ",  sngl (c1)
C        print *, "sngl (i2) = ",  sngl (i2)
C        print *, "sngl (i4) = ",  sngl (i4)
C        print *, "sngl (l1) = ",  sngl (l1)
C        print *, "sngl (l2) = ",  sngl (l2)
C        print *, "sngl (l4) = ",  sngl (l4)
C        print *, "sngl (r4) = ",  sngl (r4)
         print *, "sngl (r8) = ",  sngl (r8)
C        print *, "sngl (r16) = ", sngl (r16)
C        print *, "sngl (z8) = ",  sngl (z8)
C        print *, "sngl (z16) = ", sngl (z16)

C conversion from numeric to char
C        print *, "char (c1) = ",  char (c1)
         print *, "char (i2) = ",  char (i2)
         print *, "char (i4) = ",  char (i4)
C        print *, "char (l1) = ",  char (l1)
C        print *, "char (l2) = ",  char (l2)
C        print *, "char (l4) = ",  char (l4)
C        print *, "char (r4) = ",  char (r4)
C        print *, "char (r8) = ",  char (r8)
C        print *, "char (r16) = ", char (r16)
C        print *, "char (z8) = ",  char (z8)
C        print *, "char (z16) = ", char (z16)

C conversion from character to integer
         print *, "ichar (c1) = ",     ichar (c1)
         print *, "ichar (c3) = ",     ichar (c3)
C        print *, "ichar (i2) = ",     ichar (i2)
C        print *, "ichar (i4) = ",     ichar (i4)
C        print *, "ichar (l1) = ",     ichar (l1)
C        print *, "ichar (l2) = ",     ichar (l2)
C        print *, "ichar (l4) = ",     ichar (l4)
C        print *, "ichar (r4) = ",     ichar (r4)
C        print *, "ichar (r8) = ",     ichar (r8)
C        print *, "ichar (r16) = ",    ichar (r16)
C        print *, "ichar (z8) = ",     ichar (z8)
C        print *, "ichar (z16) = ",    ichar (z16)

         end

