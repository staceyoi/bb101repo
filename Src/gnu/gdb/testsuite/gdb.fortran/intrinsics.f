! The commented out lines below should be tested in the Fortran 90 case too
!      integer(kind=1) :: i1 = 1, j1 = 2
      integer(kind=2) :: i2 = 4, j2 = 2
      integer(kind=4) :: i4 = 4, j4 = 2
!      integer(kind=8) :: i8 = 1, j8 = 2

      real(kind=4) :: r4 = 4, s4 = 2
      real(kind=8) :: r8 = 4, s8 = 2
      real(kind=16) :: r16 = 4, s16 = 2

      complex(kind=4) :: c4 = (1, 2), d4 = (2, 3)
      complex(kind=8) :: c8 = (1, 2), d8 = (2, 3)

      character :: ch1 = '1', ch2 = '2'
      character*3 :: str1 = '123'
      character*4 :: str2 = '1234'

      logical(kind=1) :: l1 = .FALSE., m1 = .TRUE.
      logical(kind=2) :: l2 = .FALSE., m2 = .TRUE.
      logical(kind=4) :: l4 = .FALSE., m4 = .TRUE.
!      logical(kind=8) :: l8 = .FALSE., m8 = .TRUE.

      end
