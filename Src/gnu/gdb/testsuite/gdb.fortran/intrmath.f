C Commented out statements cause compile-time error due to 
C illegal arguments 

         program math_intrinsics

         integer * 4 ::zero = 0

         integer * 2 ::i2 = -2
         integer * 4 ::i4 = 4

         integer * 2 ::i2_1 = 3
         integer * 4 ::i4_1 = 3

         logical * 1 ::l1 = 1
         logical * 2 ::l2 = 2
         logical * 4 ::l4 = 4

         real * 4    ::r4  = 4.5
         real * 8    ::r8  = -8.5
         real * 16   ::r16 = -16.5

         real * 4    ::r4_1  = 3
         real * 8    ::r8_1  = 3
         real * 16   ::r16_1 = 3

         complex * 8  ::z8 =  (3.0, 4.0)
         complex * 16 ::z16 = (4.0, 3.0)

C abs
         print *, "abs (i2) = ", abs (i2)
         print *, "abs (i4) = ", abs (i4)

C        print *, abs (l1)
C        print *, abs (l2)
C        print *, abs (l4)

         print *, "abs (r4) = ", abs (r4)
         print *, "abs (r8) = ", abs (r8)
         print *, "abs (r16) = ", abs (r16)

         print *, "abs (z8) = ", abs (z8)
         print *, "abs (z16) = ", abs (z16)

C        print *, dabs (r4)
         print *, "dabs (r8) = ", dabs (r8)
C        print *, dabs (r16)

         print *, "iabs (i2) = ", iabs (i2)
         print *, "iabs (i4) = ", iabs (i4)

C        print *, iabs (l1)
C        print *, iabs (l2)
C        print *, iabs (l4)


C truncation
         print *, "aint (r4) = ",  aint (r4)
         print *, "aint (r8) = ",  aint (r8)
         print *, "aint (r16) = ", aint (r16)

C truncation for real*8
C        print *, dint (r4)
         print *, "dint (r8) = ",  dint (r8)
C        print *, dint (r16)

C nearest whole number
         print *, "anint (r4) = ",  anint (r4)
         print *, "anint (r8) = ",  anint (r8)
         print *, "anint (r16) = ", anint (r16)

C nearest whole number for real*8
C        print *, dnint (r4)
         print *, "dnint (r8) = ",  dnint (r8)
C        print *, dnint (r16)

C nearest integer
         print *, "nint (r4) = ",  nint (r4)
         print *, "nint (r8) = ",  nint (r8)
         print *, "nint (r16) = ", nint (r16)

C nearest integer for real*8
C        print *, idnint (r4)
         print *, "idnint (r8) = ",  idnint (r8)
C        print *, idnint (r16)

C imaginary part of complex
         print *, "imag (z8) = ",  imag (z8)
         print *, "imag (z16) = ",  imag (z16)

C imaginary part of complex
         print *, "aimag (z8) = ",  aimag (z8)
C        print *, "aimag (z16) = ",  aimag (z16)

C mod
C        print *, mod (i2, i4)
C        print *, mod (i4, i2)
C        print *, mod (i2, 1)
C        print *, mod (1, i2)
C zero divisor causes floating point exception
C        print *, mod (i4, zero)
C        print *, mod (i4, 0)

         print *, "mod(i2, i2_1) = ", mod (i2, i2_1)
         print *, "mod(i4,i4_1) = ", mod (i4, i4_1)
         print *, "mod(i4,3) = ", mod (i4, 3)
         print *, "mod(3,i4) = ", mod (3, i4)

C        print *, mod (r4, r8)
C        print *, mod (r4, r16)
C        print *, mod (r8, r4)
C        print *, mod (r8, r16)
C        print *, mod (r8, -2.0)
C        print *, mod (-2.0, r8)
C        print *, mod (r16,r4)
C        print *, mod (r16, r8)
C        print *, mod (r16, -2.0)
C        print *, mod (-2.0, r16)

         print *, "mod(r4,r4_1) = ", mod (r4, r4_1)
         print *, "mod(r4,-2.0) = ", mod (r4, -2.0)
         print *, "mod(-2.0,r4) = ", mod (-2.0, r4)

         print *, "mod(r8,r8_1) = ", mod (r8, r8_1)
C        print *, "mod(r8,-2.0) = ", mod (r8, -2.0)
C        print *, "mod(-2.0,r8) = ", mod (-2.0, r8)

         print *, "mod(r16,r16_1) = ", mod (r16, r16_1)
C        print *, "mod(r16,-2.0) = ",  mod (r16, -2.0)
C        print *, "mod(-2.0,r16) = ",  mod (-2.0, r16)

         print *, "amod(r4,r4_1) = ", amod (r4, r4_1)
C        print *, amod (r8, r8)
C        print *, amod (r16, r16)

C        print *, dmod (r4, r4)
         print *, "dmod(r8,r8_1) = ", dmod (r8, r8_1)
C        print *, dmod (r16, r16)

C exponential
         r4 = 1.0
         r8 = 2.0
         r16 = -1.0
         print *, "exp (r4) = ",  exp (r4)
         print *, "exp (r8) = ",  exp (r8)
         print *, "exp (r16) = ", exp (r16)

         end
