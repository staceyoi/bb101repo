       program bs15079

       integer   ::integer_v  = 2147483647
       logical   ::logical_v  = 0

       character ::character_v = "a"
         
       real        ::real_v   = 1.2
       complex ::complex_v   =  (9.0, 8.0)

       integer  ::integer_arr(3)
       data integer_arr / 1, 2, 3/

       end
