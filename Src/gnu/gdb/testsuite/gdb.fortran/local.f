C This is a modified version (the orginal did not compile) of code from a 
C customer in Norway who's complained to support@norway.hp.com in May, 1998
C that we can't deal with a simple fortran90 program.  This program is used 
C as a sanity check to verify that this customer's code can be debugged.  
C
C It is also used to verify that we can correctly print local variables 
C in the main program.

        program norway
        integer, parameter :: N=3D1
        integer i
        double precision, dimension(N) :: x

        i = 0
        x = 0
        print *, 'before loop: N = ', N
        print *, 'before loop: i = ', i
        print *, 'before loop: x = ', x

        do i=3D1,N
          x = i/dble(N)
        enddo

        print *, 'after loop: N = ', N
        print *, 'after loop: i = ', i
        print *, 'after loop: x = ', x
        end
