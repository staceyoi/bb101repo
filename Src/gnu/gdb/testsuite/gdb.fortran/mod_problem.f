         module mod_problem

         implicit none 

	 type boo
	   integer                       :: i, j
	   real, pointer, dimension(:,:) :: field
	 end type boo

	 contains

	  subroutine init(step)

	   implicit none

	   type(boo) :: step

	   step%i = 0
	   step%j = 0
	   nullify(step%field)

	   return
	  end subroutine init

	 end module mod_problem
