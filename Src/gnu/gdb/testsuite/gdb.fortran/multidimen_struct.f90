! Program for printing array data structures.
! This program prints the array which has type structures inside it.
! It checks for various dimensionals arrays.

module mod_yeungedata

implicit none

type vector_3D_compl
  complex, dimension(3) :: cx
end type vector_3D_compl

type matrix_3x3_compl
  complex, dimension(3,3):: CM
end type matrix_3x3_compl

Type typ_yeungedata
  integer                    :: nmue
  real, dimension(3)         :: fp
  type(matrix_3x3_compl), dimension(2,2)  :: frad
  type(vector_3D_compl), dimension(2,2,2) :: fdiff
  complex, dimension(2,2,2)  :: wdiffr
end type typ_yeungedata

contains

subroutine test_dummy()
  implicit none
  Type(typ_yeungedata)       :: yeungedata
  call yeungedata_init(yeungedata)
end subroutine test_dummy


subroutine yeungedata_init(ydata)

implicit none

Type(typ_yeungedata)      :: ydata
integer three_dimen(2,2,2)

  ydata%fp = (/2.5,0.5,1.25/)
  ydata%nmue = 2

   ydata%frad%cm(1,1) = (9,10)
   ydata%frad%cm(1,2) = (8,20)
   ydata%frad%cm(1,3) = (7,30)
   ydata%frad%cm(2,1) = (6,40)
   ydata%frad%cm(2,2) = (5,50)
   ydata%frad%cm(2,3) = (4,60)
   ydata%frad%cm(3,1) = (3,70)
   ydata%frad%cm(3,2) = (2,80)
   ydata%frad%cm(3,3) = (1,90)
   ydata%fdiff%cx(1) = (300,7000)
   ydata%fdiff%cx(2) = (200,8000)
   ydata%fdiff%cx(3) = (100,9000)
   ydata%wdiffr = (12000, 110000)
   ydata%wdiffr = (12000, 110000)
   ydata%wdiffr = (12000, 110000)
   three_dimen = 10

   print *,ydata%fp
   print *,ydata%nmue
   print *,ydata%frad
   print *,ydata%fdiff
   print *,ydata%wdiffr
   print *,three_dimen
  return
end subroutine yeungedata_init

end module mod_yeungedata
!------------------------------------------------------------------------------
program ydatadebug
use mod_yeungedata
implicit none
call test_dummy()
end program
!--------------------------------------------------------------------------
