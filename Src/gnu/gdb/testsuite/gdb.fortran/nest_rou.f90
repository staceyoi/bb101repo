Module Gabor
	Public :: GaborTrafo
Contains

Subroutine GaborTrafo (i)

Implicit NONE
integer,intent(in) :: i
integer :: j

j = 20

call addnum(i,j)

if (IsEqual(i,j)) then
  print*,"i and j are equal"
endif

  Contains

Logical Function IsEqual(A,B)

integer, intent(in) :: A,B

if (A == B) then
  IsEqual = .TRUE.
else
  IsEqual = .FALSE.
endif

end function IsEqual

subroutine addnum(i,j)

integer, intent(in) :: i,j
integer :: sum

  sum = i + j

  print*,"Sum of two numbers=", sum
end subroutine addnum

end subroutine GaborTrafo
end module Gabor
