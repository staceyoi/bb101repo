C Fortran names constants test
C Gets compile-time errors with f90 compiler

      program named_constants

      integer           GI1
      integer*2         GI2
      integer*4         GI3
      integer		GI4
      integer		GI5
      integer		GI6

      real              GR1
      real*4            GR2
      real*8            GR3
      double precision  GR4
      real*16           GR5

      logical           GB1
      logical*1         GB2
      logical*2         GB3
      logical*4         GB4

      character         GC1
      character(5)      GC2
      complex           GX1
      complex*8         GX2
      complex*16        GX3
      double complex    GX4

      parameter( GI1 = 14, GI2 = 12, GI3 = -1 )
      parameter( GI4 = B'01001010', GI5 = O'13', GI6 = Z'A' )

      parameter( GR1 = 1E2, GR2 = 2E3, GR3 = 3D4, GR5 = 6Q7, GR4 = 4d5 )

      parameter( GB1 = .TRUE., GB2 = .FALSE. )
      parameter( GB3 = .TRUE., GB4 = .FALSE. )

      parameter(GC1 = 'a', GC2 = "hfhhf" )

      parameter( GX1 = (3.5, 4E2), GX2 = (4.5, 5E3) )
      parameter( GX3 = (5.5, 6E4), GX4 = (6.5, 7E5) )

      print *, "GI1 = ",  GI1
      print *, "GI2 = ",  GI2
      print *, "GI3 = ",  GI3
      print *, "GI4 = ",  GI4
      print *, "GI5 = ",  GI5
      print *, "GI6 = ",  GI6
      print *, "GR1 = ",  GR1
      print *, "GR2 = ",  GR2
      print *, "GR3 = ",  GR3
      print *, "GR4 = ",  GR4
      print *, "GR5 = ",  GR5
      print *, "GB1 = ",  GB1
      print *, "GB2 = ",  GB2
      print *, "GB3 = ",  GB3
      print *, "GB4 = ",  GB4
      print *, "GC1 = ",  GC1
      print *, "GC2 = ",  GC2
      print *, "GX1 = ",  GX1
      print *, "GX2 = ",  GX2
      print *, "GX3 = ",  GX3
      print *, "GX4 = ",  GX4

      end
