C The given simple program exhausts gdb's memory when running into a
C breakpoint.
C 
C Compile with -K so that static storage is allocated; otherwise the
C executable may core dump on startup.
C 

c -------------------------------------------------------------------

      program test_array_parameters

      implicit none

      integer   nmax
      parameter (nmax=1024)

      real*8    a(nmax*nmax*32)

      integer   lda, l, iis, n, npass

      do l=1,nmax*nmax
         a(l)=0.0
      enddo

      do iis=1,10
         lda=nmax
         call blasMXM(A,LDA)
      enddo
      end

c -------------------------------------------------------------------

      SUBROUTINE blasMXM(A,LDA)
      IMPLICIT NONE
      INTEGER lda
      REAL*8 A(lda,lda)

      print *, A(lda,lda)

      RETURN
      END
