# Author: Sue Kimura <srk@cup.hp.com>, September, 1999 #
# Test Fortran 90 pointers

if {![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

set testfile "ptr90"
set srcfile $testfile.f
set binfile ${objdir}/${subdir}/$testfile

if [get_compiler_info ${binfile} f77] {
    return -1
}

if { ! $hp_f77_compiler && ! $hp_f90_compiler } {
    return 0
}


if {$hp_f77_compiler} {
   unsupported "pointer feature"
   continue
}

if {[gdb_compile "$srcdir/$subdir/$srcfile" "$binfile" executable {debug f77}] != ""} {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break foo1" "Breakpoint $decimal at $hex: file .*$srcfile, line 15."
gdb_test "break foo2" "Breakpoint $decimal at $hex: file .*$srcfile, line 43."
gdb_test "break foo3" "Breakpoint $decimal at $hex: file .*$srcfile, line 74."


gdb_test "run" "Starting program.*Breakpoint $decimal,.*$srcfile:15.*p1 => t\\(1:5:1\\)"

gdb_test "print p1" "= Uninitialized dynamic array"
gdb_test "ptype p1" "Uninitialized dynamic array"
gdb_test "ptype p1(1)" "Uninitialized dynamic array"

gdb_test "until 21" ".*foo1.*p1 => t\\(2:10:2\\)"
gdb_test "ptype p1" "integer\\*4 *\\(5\\)" "ptype p1, first time"
gdb_test "print p1" "= \\(1, 2, 3, 4, 5\\)" "print p1, first time"
set val 1
for {set i 1} {$i <= 5} {incr i} {
  gdb_test "print p1($i)" "= $val" "print p1($i): $val first loop"
  incr val
}

gdb_test "until 27" ".*foo1.*p1 => t\\(12:2:-3\\)"
gdb_test "ptype p1" "integer\\*4 *\\(5\\)" "ptype p1, second time"
gdb_test "print p1" "= \\(2, 4, 6, 8, 10\\)" "print p1, second time"
set val 2
for {set i 1} {$i <= 5} {incr i} {
  gdb_test "print p1($i)" ".*= $val" "print p1($i): $val, second loop"
  set val [expr $val+2]
}

gdb_test "until 33" ".*foo1.*end"
gdb_test "ptype p1" "integer\\*4 *\\(4\\)"
gdb_test "print p1" "\\(12, 9, 6, 3\\)" "print p1, third time"
set val 12
for {set i 1} {$i <= 4} {incr i} {
  gdb_test "print p1($i)" "= $val" "print p1($i): $val, third loop"
  set val [expr $val-3]
}

gdb_test "continue" "Breakpoint.*p2 => t\\(1:4:2,2:5:2\\)" "continue to foo2"
gdb_test "print p2" "= Uninitialized dynamic array"
gdb_test "ptype p2" "Uninitialized dynamic array"
gdb_test "ptype p2(1)" "Uninitialized dynamic array"

gdb_test "until 51" ".*foo2.*p2 => t\\(4:2:-1,5:1:-3\\)"
gdb_test "ptype p2" "integer\\*4 *\\(2,2\\)" "ptype p2, first time"
gdb_test "print p2" "= \\(\\(5, 7\\), \\(13, 15\\)\\)" "print p2, first time"
set val 5
for {set j 1} {$j <= 2} {incr j} {
    for {set i 1} {$i <= 2} {incr i} {
        gdb_test "print p2($i,$j)" "= $val" "print p2($i,$j): $val, first loop"
        set val [expr $val+2]
    }
    set val [expr $val+4]
}


gdb_test "until 59" ".*foo2.*end"
gdb_test "ptype p2" "integer\\*4 *\\(3,2\\)" "ptype p2, second time"
gdb_test "print p2" "= \\(\\(20, 19, 18\\), \\(8, 7, 6\\)\\)" "print p2, second time"
set val 20 
for {set j 1} {$j <= 2} {incr j} {
    for {set i 1} {$i <= 3} {incr i} {
        gdb_test "print p2($i,$j)" ".*= $val" "print p2($i,$j): $val, second loop"
        set val [expr $val-1]
    }
    set val [expr $val-9]
}

gdb_test "continue" "Breakpoint.*p3 => t\\(1:4:2,2:4:2,1:4:3\\)" "continue to foo3"
gdb_test "print p3" "= Uninitialized dynamic array"
gdb_test "ptype p3" "Uninitialized dynamic array"
gdb_test "ptype p3(1)" "Uninitialized dynamic array"

gdb_test "until 84" ".*foo3.*p3 => t\\(4:1:-2,3:1:-2,2:1:-1\\)"
gdb_test "ptype p3" "integer\\*4 *\\(2,2,2\\)" "ptype p3, first time"
gdb_test "print p3" \
  "= \\(\\(\\(5, 7\\), \\(13, 15\\)\\), \\(\\(53, 55\\), \\(61, 63\\)\\)\\)" \
  "print p3, first time"

set val 5 
for {set k 1} {$k <= 2} {incr k} {
  for {set j 1} {$j <= 2} {incr j} {
    for {set i 1} {$i <= 2} {incr i} {
      gdb_test "print p3($i,$j,$k)" ".*= $val" "print p3($i,$j,$k): $val, first loop"
      set val [expr $val+2]
    }
    set val [expr $val+4]
  }
  set val [expr $val+32]
}

gdb_test "until 94" ".*foo3.*end"
gdb_test "ptype p3" "integer\\*4 *\\(2,2,2\\)" "ptype p3, second time"
gdb_test "print p3" \
  "= \\(\\(\\(28, 26\\), \\(20, 18\\)\\), \\(\\(12, 10\\), \\(4, 2\\)\\)\\)" \
  "print p3, second time"
set val 28 
for {set k 1} {$k <= 2} {incr k} {
  for {set j 1} {$j <= 2} {incr j} {
    for {set i 1} {$i <= 2} {incr i} {
      gdb_test "print p3($i,$j,$k)" ".*= $val" "print p3($i,$j,$k): $val, second loop"
      set val [expr $val-2]
    }
    set val [expr $val-4]
  }
}
