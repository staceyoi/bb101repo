       program f90_pointer
       call foo1()
       call foo2()
       call foo3()
       end

       subroutine foo1

       integer, pointer :: p1(:)
       integer, target  :: t (20)
       data T /1,   2,  3,  4,  5,  6,  7,  8,  9, 10,
     +        11, 12, 13, 14, 15, 16, 17, 18, 19, 20 /
       integer i
  
       p1 => t(1:5:1)
       print *, p1
       do i=1,5
         print *, "p1(", i, "):", p1(i)
       enddo

       p1 => t(2:10:2)
       print *, p1
       do i=1,5
         print *, "p1(", i, "):", p1(i)
       enddo

       p1 => t(12:2:-3)
       print *, p1
       do i=1,4
         print *, "p1(", i, "):", p1(i)
       enddo

       end

       subroutine foo2

       integer, pointer :: p2(:,:)
       integer, target  :: t (4,5)
       data T /1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
     +        11, 12, 13, 14, 15, 16, 17, 18, 19, 20 /
       integer i,j
  
       p2 => t(1:4:2,2:5:2)
       print *, p2
       do j= 1,2
         do i=1,2
           print *, "p2(", i, ",", j, "):", p2(i,j)
         enddo
       enddo

       p2 => t(4:2:-1,5:1:-3)
       print *, p2
       do j= 1,2
         do i=1,3
           print *, "p2(", i, ",", j, "):", p2(i,j)
         enddo
       enddo

       end

       subroutine foo3

       integer, pointer :: p3(:,:,:)
       integer, target  :: t (4,4,4)
       data T /1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
     +        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
     +        21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
     +        31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
     +        41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
     +        51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
     +        61, 62, 63, 64/
       integer i, j, k
  
       p3 => t(1:4:2,2:4:2,1:4:3)
       print *, p3
       do k = 1,2
         do j= 1,2
           do i=1,2
             print *, "p3(", i, ",", j, ",", k, "):", p3(i,j,k)
           enddo
         enddo
       enddo

       p3 => t(4:1:-2,3:1:-2,2:1:-1)
       print *, p3
       do k = 1,2
         do j= 1,2
           do i=1,2
             print *, "p3(", i, ",", j, ",", k, "):", p3(i,j,k)
           enddo
         enddo
       enddo

       end
