       program real_types
       
       real    ::r   = 1.5
       real*4  ::r4  = 3.5
       real*8  ::r8  = 5.5
       real*16 ::r16 = 7.5

       complex      ::c   =  9.0
       complex * 8  ::c8  =  9.5
       complex * 16 ::c16 =  7.5
       
C complex*16 and double complex are equivalent
       double complex ::dc = 5.5
 
       end
