***************************************************************************
*                                                                         *
*            simul_commun.inc                                             *
*                                                                         *
***************************************************************************

	INTEGER*4	MAX_TEL
	PARAMETER	(MAX_TEL = 256)
	INTEGER*4	MAX_BUF_CIU
	PARAMETER	(MAX_BUF_CIU = 256)	 !LONGUEUR DE LA LISTE BUF_CIU
	CHARACTER*(15)	SIMUL_EVEN_CIU
	PARAMETER       (SIMUL_EVEN_CIU  = 'SIMUL_EVEN_CIU'//CHAR(0))

	INTEGER*4	status

	CHARACTER	lf	/'0A'X/
	CHARACTER	cr	/'0D'X/
	CHARACTER*50	MSG_TX
	CHARACTER*50	MSG_RX
	INTEGER*2	io_read_driv(4)

	LOGICAL*1	DEMARRER
	LOGICAL*1	G_RECU_STX
	LOGICAL*1	G_RECU_VDLE
	LOGICAL*1	ENTREE_GELEE(MAX_TEL)	! MODE FREEZE
	LOGICAL*1	STATION_MUETTE(MAX_TEL)	! STATION INEXISTANTE
	LOGICAL*1	ST_INTERRO(MAX_TEL)

!! POUR TEL020
	CHARACTER	DERNIER_OCT
	INTEGER*2	GET_CRC

	INTEGER*2	PTR_ECRITURE		! POINTEUR LISTE BUF_CIU
	INTEGER*2	PTR_LECTURE		!   "        "      "
	INTEGER*2	MODE_CIU
	INTEGER*2	DELAI_REP
	INTEGER*2	INDIC_JJ		! POINTEUR POUR PSC
	INTEGER*2	INDIC_II		! POINTEUR POUR PSC
	CHARACTER	BUF_CIU(MAX_BUF_CIU)	! LISTE CIRC. RECEP. CARACT. CIU
	CHARACTER	CAR_CIU			! TAMPON RX QIO DU CIU
	LOGICAL*1	XOFF			! INDIC. DTR OFF
	LOGICAL*1	ARRET_AST		! INDIQUE AST CIU NON REARME
	LOGICAL*1	TRAITE_DECO		! iNDIQUE UN D�CODAGE EN COURS

	LOGICAL*1	RECU_QQCH		!6
	LOGICAL*1	SELECTION_ACTIF
	INTEGER*2	DELAI_SELECTION
	INTEGER*2	ADR_SELECT
	INTEGER*2	CODE_SELECT
	INTEGER*4	i,j
	INTEGER*4	MAX_ADR
	CHARACTER*6	TYPE_CIU
	INTEGER*2	LISTE_PTR_ACTUEL
	INTEGER*2	LISTE_PTR_ENVOIE
	INTEGER*2	CIU_ACTUEL					       !6
	CHARACTER*1	PRPN(MAX_TEL)

	INTEGER*2	DONNEES(MAX_TEL)
!	DONNEES:        POINT, REEN, PROT NEU, DD, TLHC, DEF A, DEF B, DEF C, DEF N, TELE, ALIMENTATION, PILE STATION, PILE COFFRET

	REAL*4		F_DONNEES_ANA(3,MAX_TEL)
	REAL*4		DONNEES_ANA(3,MAX_TEL)

	INTEGER*4	TEL_LIS(MAX_TEL+1)				       !6
	CHARACTER*5	TEL_LIS_LCLCL(MAX_TEL+1)			       !6
	CHARACTER*2	TYPE_APP(MAX_TEL)
	INTEGER*4	ST_ENVOIE_FREQ
	LOGICAL*1	ENVOIE_FREQUENT

	CHARACTER*6	PORT_CIU					       !6
	INTEGER*2	PORT_LG

	STRUCTURE /LISTE_CMD/
		INTEGER*2	ADR_ST
		INTEGER*2	CODE
		INTEGER*2	DELAI_TEMPS
		INTEGER*2	DELAI_TEMPS_RS(4)
	END STRUCTURE

	RECORD	/LISTE_CMD/ LISTE_SEQ(101)

	common /SIMULATEUR1/ status
	common /SIMULATEUR4/ indic_jj, indic_ii
	common /SIMULATEUR5/ g_recu_stx, g_recu_vdle, demarrer, xoff
	common /SIMULATEUR6/ io_read_driv
	common /SIMULATEUR7/ msg_tx
	common /SIMULATEUR8/ msg_rx
	common /SIMULATEUR9/ liste_seq
	common /SIMULATEUR10/ liste_ptr_actuel, liste_ptr_envoie
	common /SIMULATEUR11/ traite_deco, arret_ast, recu_qqch, selection_actif
	common /SIMULATEUR12/ prpn
	common /SIMULATEUR13/ i, j, st_envoie_freq, max_adr
	common /SIMULATEUR14/ ptr_ecriture, ptr_lecture
	common /SIMULATEUR15/ buf_ciu
	common /SIMULATEUR16/ car_ciu, dernier_oct, lf, cr
	common /SIMULATEUR17/ delai_rep, adr_select
	common /SIMULATEUR18/ code_select, delai_selection
	common /SIMULATEUR19/ get_crc, ciu_actuel
	common /SIMULATEUR20/ tel_lis_lclcl
	common /SIMULATEUR21/ type_app
	common /SIMULATEUR22/ type_ciu
	common /SIMULATEUR23/ port_ciu
	common /SIMULATEUR24/ tel_lis
	common /SIMULATEUR25/ entree_gelee
	common /SIMULATEUR26/ station_muette
	common /SIMULATEUR27/ st_interro
	common /SIMULATEUR28/ port_lg, mode_ciu
	common /SIMULATEUR29/ donnees
	common /SIMULATEUR30/ donnees_ana
	common /SIMULATEUR31/ f_donnees_ana
	common /SIMULATEUR32/ envoie_frequent
