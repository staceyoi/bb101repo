program

integer*4 ,dimension(1000,6) ::dode_no
integer*4, dimension(100,100) :: code_no
integer*4, dimension(5,5) :: vsmall
integer*4, dimension(10,4) :: small
integer*4, dimension(100,4) :: big
integer*4, dimension(2,2,2) :: thrdim
integer*4, dimension(-2:0,-2:0) :: v1


code_no = 200000

dode_no = 10


vsmall(1,1)= 11
vsmall(1,2)= 12
vsmall(1,3)= 13
vsmall(1,4)= 14
vsmall(1,5)= 15
vsmall(2,1)= 21
vsmall(2,2)= 22
vsmall(2,3)= 23
vsmall(2,4)= 24
vsmall(2,5)= 25
vsmall(3,1)= 31
vsmall(3,2)= 32
vsmall(3,3)= 33
vsmall(3,4)= 34
vsmall(3,5)= 35
vsmall(4,1)= 41
vsmall(4,2)= 42
vsmall(4,3)= 43
vsmall(4,4)= 44
vsmall(4,5)= 45
vsmall(5,1)= 51
vsmall(5,2)= 52
vsmall(5,3)= 53
vsmall(5,4)= 54
vsmall(5,5)= 55


small(1:5,1)= 5
small(6:10,1)= 10
small(1:5,2)=15 
small(6:10,2)= 20
small(1:5,3)=25 
small(6:10,3)= 30
small(1:5,4)=35 
small(6:10,4)= 40

big(1:50,1)= 5
big(51:100,1)= 10
big(1:50,2)=15 
big(51:100,2)= 20
big(1:50,3)=25 
big(51:100,3)= 30
big(1:50,4)=35 
big(51:100,4)= 40

thrdim(1,1,1)= 111
thrdim(1,1,2)= 112
thrdim(1,2,1)= 121
thrdim(1,2,2)= 122
thrdim(2,1,1)= 211
thrdim(2,1,2)= 212
thrdim(2,2,1)= 221
thrdim(2,2,2)= 222

v1(-2,-2)= 11
v1(-2,-1)= 12
v1(-2,0)= 13
v1(-1,-2)= 21
v1(-1,-1)= 22
v1(-1,0)= 23
v1(0,-2)= 31
v1(0,-1)= 32
v1(0,0)= 33

print*,small
print*,big

print*,vsmall
end
