         integer i
         character (53) two
         data two /'ab'/
         character (55) three
         data three /'cdefg'/
         character four (2,2)*80
         data four /4*' '/
         character*10 sstr, funcstr
         
         i = 1
         call subr1 (i, two)
	 call subr2 (i, two)
         call subr3 (two, three, i)
         call subr4 (two, four)
         sstr = funcstr()
         end

         subroutine subr1 (i, cstring)
         integer i
         character (*) cstring
         cstring = "cd"
         j = i*2
         call subr2 (i, cstring)
         end

         subroutine subr2 (i, cstring)
         integer i,j
         character (50) cstring
         cstring = "ef"
         j = i*2
         end

         subroutine subr3 (cstring1, cstring2, i)
         integer i,j
         character (*) cstring1, cstring2
         cstring1 = "9876543210"
         cstring2 = "1234xxx890"
         j = i*2
         end

         character*10 function funcstr()
         funcstr = "hello all"
         end
