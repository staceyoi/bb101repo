       program structure_type

       structure /S/
         integer * 2     i2
         logical * 1     l1
         character (4)   four_c
         real*8          r8
         double complex  dcm
       end structure

       structure /person/
         integer  person_id
         structure /name/ nm
           character (len=5)  last, first, mid
         end structure
         structure /phone/ phones(2)
           integer*2  area_code
           integer    number
         end structure
       end structure


       record /S/ r
       record /person/ p1

       r.i2     = 2
       r.l1     = 1
       r.four_c = 'dd'
       r.r8     = 3.5
       r.dcm    = (11.5, 12.5)
       
       print *, r.i2

       p1.person_id = 1
       p1.nm.last = "who"
       p1.nm.first = "am"
       p1.nm.mid = "i"
       p1.phones(1).area_code = 408
       p1.phones(1).number =  5551212
       p1.phones(2).area_code = 408
       p1.phones(2).number =  5553434
       
       print *, p1.person_id

       end
