        program union_example
        structure /var/
          integer  tag
          union
            map
              integer int
            end map
            map
              real*4 float
            end map
            map
              integer*2 s1
              integer*2 s2
            end map
            map
              structure n1
                union
                  map
                    logical*1 b1
                  end map
                  map
                    logical*1 b2
                  end map
                end union
                logical*2  l2
              end structure
            end map
            map
              structure /nested/ n2
                union
                  map
                    integer*2 s1
                  end map
                  map
                    integer*2 s2
                  end map
                end union
              end structure
            end map
          end union
          character*4 name
        end structure

        record /var/ r1

        r1.tag = 1
        r1.int = 3
        r1.name = 'abcd'

        print *, r1.int, r1.float, r1.name 
        print *, r1.s1, r1.s2
        print *, r1.n1.b1, r1.n1.b2, r1.n1.l2
        print *, r1.n2.s1, r1.n2.s2
        end
