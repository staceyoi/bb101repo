       program watch1

       real*4  garr (5)
       common garr
       data garr /1.0, 2.0, 3.0, 4.0, 5.0 /

       call subf (1)
       call subg (5)

       garr(5) = 15.0

       end

       subroutine subf (i1) 
       integer i1 

       real tab ( 4 )
       data tab / 1.5,  2.5,  3.5,  4.5 / 

       tab(i1) = 2.0

       tab (1) = tab(1) + tab(2)

       return 
       end 

       subroutine subg (i1) 
       integer*4 i1

       real*4 garr (5)
       common garr

       garr(5) = 2.5

       garr(i1) = 3.5

       end

