C subh and subi use common "c" but with different types for a

       program watch2

       integer a
       common a

       a=3
       call subh()
       a=4
       end


       subroutine subh()
       real a
       common /c/ a

       a=6.5
       call subi()
       return
       end


       subroutine subi()
       real b
       common /c/ b

       b=7.5
       return
       end
