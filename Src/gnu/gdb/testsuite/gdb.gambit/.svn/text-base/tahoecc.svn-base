#!/bin/ksh

# WDB-specific 'tahoecc' based on:
#  /CLO/TAHOE_BE/opt/ansic/bin/tahoecc@@/main/TAHOE_SWDEV3A/11
#    Rule: TBE_TAHOE.DEV4_STABLE
# Modifications: Added "VIEW_NAME_PREFIX":
#   Assumption :- this script is being run from a directory in
#   an imported VOB.
# FIXME:
# This script is meant as a temporary workaround for the non-avilability
# of clearcase on 11.0 machines.
#
# For moving to a different version of 'tahoecc' update this
# script and modify the rule fo accessing the latest Tahoe compilers
# in /CLO/Components/WDB/CS/wdb_ia64.latest

#################################################################
#
#
#		tahoecc 
#
#	This script is used as a wrapper to the cc command. It's
#	primary purpose is to set up the pathnames for the various
#	BE components.
#
#	By default the script will pick up all the BE components
#	from the /CLO/TAHOE_BE vob. However, BE components from
#	other paths can be picked up by specifying their pathnames
#	on the command line.
#
#	The script also assumes that a 32-bit compile is default
# 	and if the user wants to a 64-bit compile that too needs
#	to be specified on the command line.
#
#	This script parses out all those command line options 
#	that are meant for tahoecc and sends the rest of the options
#	as command line options to cc. For those options that are
#	sent to cc there is no error checking done in order to
#	validate the options.
#
#	tahoecc run with the -h or the -help option will print
#	the usage message in detail.
#
##################################################################

###############################
## Set up the default constants
###############################

OPT_PROGRAM=$0
MODE=32			 		# ILP32 is default
tahoecc_opts=""
CURRENT_DIR=`pwd`
export VIEW_NAME_PREFIX=`echo $CURRENT_DIR | sed '/CLO.*$/s///'`
TAHOE_ROOT=$VIEW_NAME_PREFIX/CLO/TAHOE_BE
##################################################################
#
#	Usage function
#
##################################################################
function Usage
{
cat <<EOF
        Usage: tahoecc  [-HPC=<dirname>] [-ccom=<filename>] 
                [-cc=<filename>] [-cpp_compat=<filename>] 
                [-cpp_ansi=<filename>] [-ccat=<filename>] 
                [-cppcat=<filename>] [-AS=<dirname>] [-as=<filename>] 
                [-LD=<dirname>] [-ld=<filename>]
                [-ld_fdp=<filename>] [-LPATH=<dirname>] [-milli=<filename>] 
                [-libc=<filename>] [-includir=<dirname>] 
                 [-ld_plugin_config=<filename>] [-tmpdir=<dirname>]
        
               -h             help, prints a command summary.
               -help          help, prints a command summary.
               -HPC=<dirname> directory from where the C compiler
                              components (ccom, cc, cpp, 
                              cpp.ansi, cc.cat, and cpp.cat) 
                              will be picked up 
               -ccom=<filename> absolute pathname to ccom 
               -cc=<filename> absolute pathname to cc 
               -cpp_compat=<filename> absolute pathname to cpp
               -cpp_ansi=<filename> absolute pathname to cpp.ansi 
               -ccat=<filename> absolute pathname to cc.cat 
               -cppcat=<filename> absolute pathname to cpp.cat 
               -AS=<dirname> directory from where the assembler, 
                             as, will be picked up 
               -as=<filename> absolute pathname to as 
               -LD=<dirname> directory from where the linker, 
                             ld, will be picked up 
               -ld=<filename> absolute pathname to ld 
               -ld_fdp=<filename> absolute pathname to fdp 
               -LPATH=<dirname> directory to look for libraries, 
                             before looking in the standard places 
               -milli=<filename> absolute pathname to libmilli 
               -libc=<filename> absolute pathname to libc 
               -includir=<dirname> directory to look for headers , 
                             before looking in the standard places 
               -ld_plugin_config=<filename> absolute pathname to 
                             linker plug-in configuration file
               -tmpdir=<dirname> directory to use as tmp dir 

EOF
}

##################################################################
#
#	Error : arguments to this function are printed to stderr
#
##################################################################
Error()
{
        echo "$OPT_PROGRAM: $*" >&2
}

##################################################################
#
#	Exit : exit status is printed to stdout and the script exits
#
##################################################################
Exit()
{
        typeset -r EXIT_VAL=$1
        [ "$EXIT_VAL" != 0 ] && Error "Exit status is $1"
        exit $1
}

########################################################
#
# Parse command line arguments; first pick up all those
# that specify pathnames to BE components. Validate them.
# Print error message and exit if they are not valid.
#
########################################################

while [[ $1 != "" ]]
do
  case $1 in
    -HPC=*|-ccom=*|-cc=*|-cpp_compat=*|-cpp_ansi=*|-ccat=*|\
    -cppcat=*|-AS=*|-as=*|-LD=*|-ld=*|-ld_fdp=*|\
    -LPATH=*|-milli=*|-libc=*|-includir=*|-ld_plugin_config=*|\
    -tmpdir=*) arg=${1#*=}; 
		if [[ -z $arg ]]; then
#		  { [[ ! -d $arg ]] && [[ ! -f $arg ]] } &&  \
#		    { Error "$arg doesn't exist in current view" ; Exit 1; }
		     Error "$1 is set to null." ; Exit 1;
		fi
#
# The syntax in this next line will set the option to a variable of
# the same name as in the case statement above.
#
		eval ${1#-};;

#
# The help option
#

    -h|-help)	Usage;
		Exit 0;;
    -t)		TEST=echo;
		set -x;;

#
# All other options are to be passed to cc; the +DD option if set is
# also used by tahoecc.
#

    *) tahoecc_opts="${tahoecc_opts} $1";
	case $1 in
	    "+DD64") MODE=64;;
	esac
	;;

  esac
  shift
done 

#######################################################################
#
# If the complete pathname of the component is set then use that.
# If not, to find the complete, final pathname to the component check if
# the directory name of where to find the component is set; and if that 
# is not set then default to the one in $TAHOE_ROOT.
#
########################################################################

if [[ "$ccom" != "" ]]; then
  : ${ST_CCOM:=${ccom}}
elif [[ "$HPC" != "" ]]; then
  : ${ST_CCOM:=${HPC}/ccom}
else
  : ${ST_CCOM:=${TAHOE_ROOT}/opt/ansic/lbin/ccom}
fi
export ST_CCOM

if [[ "$cc" != "" ]]; then
  : ${ST_CC:=${cc}}
elif [[ "$HPC" != "" ]]; then
  : ${ST_CC:=${HPC}/cc}
else
  : ${ST_CC:=${TAHOE_ROOT}/opt/ansic/bin/cc}
fi
export ST_CC

if [[ "$cpp_compat" != "" ]]; then
  : ${ST_CPP_COMPAT:=${cpp_compat}}
elif [[ "$HPC" != "" ]]; then
  : ${ST_CPP_COMPAT:=${HPC}/cpp}
else
  : ${ST_CPP_COMPAT:=${TAHOE_ROOT}/opt/langtools/lbin/cpp}
fi
export ST_CPP_COMPAT

if [[ "$cpp_ansi" != "" ]]; then
  : ${ST_CPP_ANSI:=${cpp_ansi}}
elif [[ "$HPC" != "" ]]; then
  : ${ST_CPP_ANSI:=${HPC}/cpp.ansi}
else
  : ${ST_CPP_ANSI:=${TAHOE_ROOT}/opt/langtools/lbin/cpp.ansi}
fi
export ST_CPP_ANSI

if [[ "$ccat" != "" ]]; then
  : ${ST_CCAT:=${ccat}}
elif [[ "$HPC" != "" ]]; then
  : ${ST_CCAT:=${HPC}/cc.cat}
else
  : ${ST_CCAT:=${TAHOE_ROOT}/opt/ansic/lib/nls/msg/C/cc.cat}
fi
export ST_CCAT

if [[ "$cppcat" != "" ]]; then
  : ${ST_CPPCAT:=${cppcat}}
elif [[ "$HPC" != "" ]]; then
  : ${ST_CPPCAT:=${HPC}/cpp.cat}
else
  : ${ST_CPPCAT:=${TAHOE_ROOT}/usr/lib/nls/msg/C/cpp.cat}
fi
export ST_CPPCAT

if [[ "$as" != "" ]]; then
  : ${ST_ASSEM:=${as}}
elif [[ "$AS" != "" ]]; then
  : ${ST_ASSEM:=${AS}/as}
else
  : ${ST_ASSEM:=${TAHOE_ROOT}/usr/ccs/bin/as}
fi
export ST_ASSEM

if [[ "$ld" != "" ]]; then
  : ${ST_LD:=${ld}}
elif [[ "$LD" != "" ]]; then
  : ${ST_LD:=${LD}/ld}
else
  : ${ST_LD:=${TAHOE_ROOT}/usr/ccs/bin/ld}
fi
export ST_LD

if [[ "$ld_fdp" != "" ]]; then
  : ${HP_LD_FDP:=${ld_fdp}}
else
  : ${HP_LD_FDP:=${TAHOE_ROOT}/usr/ccs/bin/fdp}
fi
export HP_LD_FDP

########################################################################
#
# In cases where there are two versions of the same component one for
# 32-bit and one for 64-bit, rely on the MODE option to be set.
#
########################################################################

if [[ "$LD" = "" ]]; then
       : ${ST_CRT:=${TAHOE_ROOT}/usr/ccs/lib/em_${MODE}/crt0.o}
else
       : ${ST_CRT:=${LD}/../rtm${MODE}/crt0.o}
fi
export ST_CRT

if [[ "$milli" != "" ]]; then
  : ${ST_LIBMILLI:=${milli}}
elif [[ "$LPATH" != "" ]]; then
  : ${ST_LIBMILLI:=${LPATH}/milli.a}
fi

if [[ "$libc" != "" ]]; then
  : ${ST_LIBC:=${libc}}
elif [[ "$LPATH" != "" ]]; then
  : ${ST_LIBC:=${LPATH}/libc.a}
fi

: ${ST_LIBMILLI:=${TAHOE_ROOT}/usr/lib/em_${MODE}/milli.a}
export ST_LIBMILLI
: ${ST_LIBC:=${TAHOE_ROOT}/usr/lib/em_${MODE}/libc.a}
export ST_LIBC
: ${LPATH:=${TAHOE_ROOT}/usr/lib/em_${MODE}}
export LPATH

: ${ST_LIBC_P:=-lc}
export ST_LIBC_P

if [[ "$includir" != "" ]]; then
  : ${INCLUDIR:=-I${includir} -I${TAHOE_ROOT}/usr/include}
else
  : ${INCLUDIR:=-I${TAHOE_ROOT}/usr/include}
fi
export INCLUDIR

: ${HP_LD_PLUGIN_CONFIG:=${ld_plugin_config}}
: ${HP_LD_PLUGIN_CONFIG:=/dev/null}
export HP_LD_PLUGIN_CONFIG

: ${TMPDIR:=${tmpdir}}
: ${TMPDIR:=/tmp}
export TMPDIR

#
# Call cc with its options
#

${TEST} ${ST_CC} ${tahoecc_opts} ${INCLUDIR} -u _divI


########################
#
# end tahoecc
#
########################
