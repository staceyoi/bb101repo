#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997, 1998, 2005
#   Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# wdb-help@cup.hp.com

# This file was written by Carl Burch, adapted from demangler.exp by Saravanan 
# Ekanathan (pes@india.hp.com).

# Test script to reproduce defect JAGae78770, "command-line calls fail with
# 'Cannot resolve method class' messages".

if $tracelevel then {
 strace $tracelevel
}

# are we on a target board
if ![isnative] then {
    return
}

#
# test running programs
#
set prms_id 0
set bug_id 0

set testfile "JAGae78770"
set srcfile1 ${testfile}.main.C
set srcfile2 ${testfile}.shlib1.C
set srcfile3 ${testfile}.shlib2.C
set shlib1 ${objdir}/${subdir}/shlib1.so
set shlib2 ${objdir}/${subdir}/shlib2.so
set binfile ${objdir}/${subdir}/${testfile}.out

# Create and source the file that provides information about the compiler
# used to compile the test case.
if [get_compiler_info ${binfile} "c++"] {
    return -1;
}
source ${binfile}.ci

if {$gxx_compiled} {
    return;
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile2}" "${shlib1}" executable {debug c++ "additional_flags=-b +z"}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this filewill automatically fail."
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile3}" "${shlib2}" executable {debug c++ "additional_flags=-b +z"}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this filewill automatically fail."
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile1} ${shlib1} ${shlib2}" "${binfile}" executable {debug c++}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this filewill automatically fail."
}



gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break main" "Breakpoint.*at $hex: file.*$srcfile1.*" "Break at main"
gdb_test "run" "Starting program:.*Breakpoint 1, main .. at .*main.C:4.*func1.*" "Run to main"
gdb_test "break func1" "Breakpoint 2 at $hex: file.*$srcfile2.*" "Break at func1"
gdb_test "del 2" "" "deleted break at func1"

gdb_test "break func2" "Breakpoint 3 at $hex: file.*$srcfile3.*" "Break at func2"
gdb_test "c" "Continuing.*Breakpoint 3, func2.*$srcfile3.*d.enter.*" "continue to func2"

gdb_test "s" "Derive2::enter.*this=$hex.*Set2.h:13.*return 10.*" "single-step"

# JAGae78770 was fixed for PA, but not for early versions of aCC6 on IPF
#if {$hp_aCC6_compiler} { setup_xfail "ia64*-hp-*" JAGae78770 }
#JAGae78770 was fixed for IPF,now it is passing.Dated:27/01/2009
gdb_test "ptype this" "Derive2.*NSTail.*Set.*int (pri_func|enter).*int (enter|pri_func).*" "ptype this"

# JAGae78770 was fixed for PA, but not for early versions of aCC6 on IPF
# This is the case that failed in JAGae78770, the ptype above might not have
#if {$hp_aCC6_compiler} { setup_xfail "ia64*-hp-*" JAGae78770 }
#JAGae78770 was fixed for IPF,now it is passing.Dated:27/01/2009
gdb_test "p this->private_func()" ".1 = 10.*" "p this->private_func()"

gdb_exit
