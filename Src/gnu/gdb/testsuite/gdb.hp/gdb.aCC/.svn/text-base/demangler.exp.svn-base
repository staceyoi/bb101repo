#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997, 1998
#   Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This file was written by Saravanan Ekanathan (pes@india.hp.com)


if $tracelevel then {
 strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0


set testfile "demangler"
set srcfile ${testfile}.cc
set binfile ${objdir}/${subdir}/${testfile}

# are we on a target board
if ![isnative] then {
    return
}

if { ![istarget "hppa*-hp-hpux*"] } then {
   verbose "This test is only valid on pa. "
   return;
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this filewill automatically fail."
}


# Create and source the file that provides information about the compiler
# used to compile the test case.
if [get_compiler_info ${binfile}] {
    return -1;
}

if {$gcc_compiled} {
    return;
}


gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break 11" "Breakpoint.*at $hex: file.*$srcfile.*" "Break at 11"
gdb_test "run" "Starting program:.*" "Run to line 11"

# If gdb sets the breakpoint at - new,delete,new[] and delete[]- operators,
# the demangling done at startup for these operators is perfect.
if [istarget "hppa*-hp-hpux10.20"] then {
   gdb_test "break operator new(unsigned int)" "Breakpoint.*at.*$hex.*" "Breaking at operator new(unsigned int)"
   gdb_test "break operator new \[\] (unsigned int)" "Breakpoint.*at.*$hex.*" "Breaking at operator new \[\](unsigned int)"
} else {
   gdb_test "break operator new(unsigned long)" "Breakpoint.*at.*$hex.*" "Breaking at operator new(unsigned long)"
   gdb_test "break operator new \[\] (unsigned long)" "Breakpoint.*at.*$hex.*" "Breaking at operator new \[\](unsigned long)"
}
gdb_test "break operator delete (void*)" "Breakpoint.*at.*$hex.*" "Breaking at operartor delete (void*)"


gdb_test "break operator delete \[\] (void*)" "Breakpoint.*at.*$hex.*" "Breaking at operartor delete \[\] (void*)"

# Demangling these symbols were failing earlier.

gdb_test "maint demangle __opPUp__5BlockXTUp_Fv" "Block.*unsigned long long.*operator unsigned long long.*void.*" "Demangling __opPUp__5BlockXTUp_Fv"

gdb_test "maint demangle __ml__Q2_13PriorityQueueXT20DataOrderedContainer_8iteratorFv" "PriorityQueue.*DataOrderedContainer.*iterator.*operator.*void.*" "Demangling __ml__Q2_13PriorityQueueXT20DataOrderedContainer_8iteratorFv"

gdb_test "maint demangle __ct__Q2_13PriorityQueueXT18JumpTableContainer_8iteratorFv" "PriorityQueue.*JumpTableContainer.*iterator.*iterator.*void.*" "Demangling __ct__Q2_13PriorityQueueXT18JumpTableContainer_8iteratorFv"
gdb_test "maint demangle mallocItemFromFreeList__9ArrayPoolXU44_Fv" "ArrayPool.*44U.*mallocItemFromFreeList.*void.*" "Demangling mallocItemFromFreeList__9ArrayPoolXU44_Fv"

gdb_test "maint demangle __opP17CullableHashTableXT5ScopeT6TagKeySP32___17ScopeTablePtrHackCFv" "ScopeTablePtrHack.*operator.*CullableHashTable.*Scope.*TagKey.*32.*void.*const.*" "Demangling __opP17CullableHashTableXT5ScopeT6TagKeySP32___17ScopeTablePtrHackCFv"
