#   Copyright (C) 1999, 2000 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

#
# Test Machine interface (MI) operations leading to JAGaf62706,
#	"GDB crashes after we get [No frame with frame# or frame_address -1]"
#

load_lib mi-support.exp

gdb_exit
if [mi_gdb_start] {
    continue
}

set testfile "eclipse1"
set srcfile ${testfile}.cpp
set input ${objdir}/${subdir}/${testfile}.in
set binfile ${objdir}/${subdir}/${testfile}
#if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++ "additional_flags=+d"}] != "" } {
if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++ "additional_flags=-AA"}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# create input file
remote_exec build "/bin/rm -f $input"
set fd [open $input w]
puts $fd "123 'a'\n"
close $fd

mi_delete_breakpoints
mi_gdb_reinitialize_dir $srcdir/$subdir
mi_gdb_load ${binfile}

mi_gdb_test "0-break-insert -t main" ".*done.*"
mi_gdb_test "1-exec-arguments < $input" ".*done.*"
mi_gdb_test "2-exec-run" ".*running.*"
mi_gdb_test "" ".*stopped,thread-id.*main.*${srcfile}.*" "2-exec-run, 2nd part"
mi_gdb_test "3 whatis test1" ".*done.*"
mi_gdb_test "4-var-create - * test1" ".*done.*"
mi_gdb_test "5-var-evaluate-expression var1" ".*done.*"
mi_gdb_test "6-data-disassemble -f ${srcfile} -l 28 -n 100 -- 1" ".*done.*"
mi_gdb_test "7 next" ".*done.*"
mi_gdb_test "8 next" ".*done.*"
mi_gdb_test "9 next" ".*done.*"
mi_gdb_test "10 next" ".*done.*"
mi_gdb_test "11-var-delete var1" ".*done.*"
#mi_gdb_test "12-data-disassemble -s 0x797c3060 -e 0x797c30c4 -- 0" ".*done.*"
mi_gdb_test "12-data-disassemble -s \"\$pc - 20\" -e \$pc -- 0" ".*done.*"
mi_gdb_test "13 run" ".*done.*"

mi_gdb_exit

# clean up the temp files
remote_exec build "/bin/rm -f $input"

return 0


