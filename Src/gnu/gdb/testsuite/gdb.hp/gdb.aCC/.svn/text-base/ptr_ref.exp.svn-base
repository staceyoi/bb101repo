# Test for JAGae75147 - suppress (shared) at end of output for EDG compiler
#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997, 1998
#   Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
 strace $tracelevel
}

#
# test running programs
#
set prms_id 0
set bug_id 0


set testfile "ptr_ref"
set srcfile ${testfile}.C
set binfile ${objdir}/${subdir}/${testfile}

# are we on a target board
if ![isnative] then {
    return
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable { debug c++}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}


# Create and source the file that provides information about the compiler
# used to compile the test case.
if [get_compiler_info ${binfile}] {
    return -1;
}

if {$gcc_compiled} {
    return;
}


gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break doit" "Breakpoint.*at $hex: file.*$srcfile.*" "Break at function"
gdb_test "run" "Starting program:.*" "Run to function"

gdb_test "p ref.get()" "get.*this.*123.*" "Print member function thru reference variable"
gdb_test "p ref->get()" "Expression must have pointer type.*" "Error out message"

gdb_test "p ptr->get()" "get.*this.*123.*" "Print member function thru pointer variable"
gdb_test "p ptr.get()" "Expression must have class type.*" "Error out message"

gdb_test "p ptrRef->get()" "get.*this.*123.*" "Print member function thru reference ptr variable"
gdb_test "p ptrRef.get()" "Expression must have class type.*" "Error out message"

gdb_exit
