class Set {

public:
  int iij;
  int iik;
  void init ( bool b, int sss, int iul)
    {
      iij = 102;
      iik = 103;
      isPoolSet = b;
      theSmallSetSize = sss;
      inUseLock = iul;
    };

  int foo (int i)
    {
      return theSmallSetSize + i;
    };

private:
   // private data fields
   bool      isPoolSet:1;            // Is this a Pool Set?
   int		theSmallSetSize;	// 0 to MAX_SMALL_SETSIZE
   int	inUseLock;		// lock on any component of set.

};

Set junk;

class ScratchRegisterAllocator {
public:
    void init (Set gs, Set ls, Set ps)
      {
        struct junk {
         Set jj;
	} junky;
	junky.jj = ls;
        generalSet = gs;
        longSet = ls;
        prefferedSet = ps;
      };
    void bar (Set &bulk)
      {
        int i;
        i = bulk.foo (10);
        i++;
      };
    void baz (Set *bulk)
      {
        int i;
        i = bulk->foo(11);
        i++;
      };
  private:
    Set generalSet;
    Set longSet;
    Set prefferedSet;
};
int main ()
{
  Set a, b, c;
  ScratchRegisterAllocator sra;

  a.init (0, 2, 10);
  b.init (1, 3, 11);
  c.init (0, 4, 12);
  sra.init (a, b, c);
  sra.bar (b);
  sra.baz (&c);
  junk = a;
  return 0;
}
