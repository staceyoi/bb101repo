#include <stdio.h>
struct big {
public:
   int f1, f2, f3, f4, f5, f6;
   ~big() {}
};
struct big B = { 1, 2, 3, 4, 5, 6};
void sam(struct big b) {
   printf("address of b: %p, B: %p\n%d %d %d|%d %d %d\n",
          &b, &B, b.f1, b.f2, b.f3, b.f4, b.f5, b.f6);
}
int main() {
   sam(B);
   return 0;
}


