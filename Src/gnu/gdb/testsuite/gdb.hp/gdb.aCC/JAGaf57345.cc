
extern "C" int printf(const char*,...);
 
 class class_a {
 public:
         char i;
 
         class_a(){i= 'a'; printf("class_a::class_a()\n");}
         void virtual ident(void);
 };

class class_b : public class_a {
 public:
         char i;
 
         class_b(){i= 'b'; printf("class_b::class_b()\n");}
         void virtual ident(void);
 };
 
 void class_a::ident(void) {printf("this is an object of type a\n");}
 
 void class_b::ident(void) {printf("this is an object of type b\n");}


// main.c
 // CC -g main.c a.c b.c
 
 int main()
 {
   class_b *obj1 = new class_b;
   class_a *obj2 = obj1;
   obj1->ident();                  // set BREAKPOINT; what *obj2
   obj2->ident();                  // set BREAKPOINT; what *obj2
}
