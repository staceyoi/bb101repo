#define _ISOC99_SOURCE

#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <wchar.h>

int
main( int argc, char* argv[] ) {

      const char * norg = "A Narrow Character String";
      const wchar_t *worg = L"A Wide Character String";

      printf( "The wide character string is: %ls\n",
  	    L"A Wide Character String" );

      printf( "The original narrow character string is: %s\n", norg );
      wchar_t *wstr = NULL;
      size_t len = mbstowcs( NULL, norg, 0);
      wstr = new wchar_t[ len + 1];
      len = mbstowcs( wstr, norg, len+1);

     printf( "The length of the converted narrow string is:%d\n", len );
	printf( "The converted narrow character string is: %ls\n", wstr );


      printf( "The original wide character string is: %ls\n", worg );
      char *str = NULL;
      len = wcslen( worg );
      str = new char[ len + 1];
      len = wcstombs( str, worg, len+1 );

      printf( "The length of the converted wide string is:%d\n", len );
      printf( "The converted wide character string is: %s\n", str );

 }

