#define MY_FLOAT float 
typedef int MY_INT;
typedef MY_INT * MY_INT_PTR;

struct foo {
   int i1;
   static int i2;
   static foo *p3;
   MY_INT i4[10];
   MY_FLOAT f5;
   MY_INT_PTR p7,p8;
   static float* p9;
   static MY_FLOAT f10;
}foo_obj;

int foo::i2 = 99;
foo *foo::p3 = 0;
float *foo::p9 = &(foo::f10); 
float foo::f10 = 3.14;

union foo1{
   char c16;
   char *str17;
   foo s18;
   long i19;
   double d20;
}foo1_obj;

struct foo2 {
   char c11;
   char str12[100];
   static foo s13;
   static foo *p14;
   foo1 u15;
   int i21 : 4;
   MY_INT i22 : 1;
   enum color {RED=0, BLUE, GREEN} e23;
}foo2_obj;

foo *foo2::p14 = 0;

int main() {
   return 0;
}
