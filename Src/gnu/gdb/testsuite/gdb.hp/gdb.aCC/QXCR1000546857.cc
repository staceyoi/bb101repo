namespace std {
 template <class T>
 struct allocator {};
 template <class T, class A = allocator<T> >
 struct vector {};
} // namespace std

template <int T>
struct Triangulation1 {
  void save_user_flags(std::vector<bool>&);
};

template <int T>
void Triangulation1<T>::save_user_flags(std::vector<bool>&){
}

template <char T>
struct Triangulation2 {
  void save_user_flags(std::vector<bool>&);
};

template <char T>
void Triangulation2<T>::save_user_flags(std::vector<bool>&){
}

template <long T>
struct Triangulation3 {
  void save_user_flags(std::vector<bool>&);
};

template <long T>
void Triangulation3<T>::save_user_flags(std::vector<bool>&){
}

template <unsigned T>
struct Triangulation4 {
  void save_user_flags(std::vector<bool>&);
};

template <unsigned T>
void Triangulation4<T>::save_user_flags(std::vector<bool>&){
}

int main() {
   std::vector<bool> vb;
   Triangulation1<2> T;
   Triangulation2<'a'> S;
   Triangulation3<389576589> R;
   Triangulation4<254> Q;
   T.save_user_flags(vb);
   S.save_user_flags(vb);
   R.save_user_flags(vb); 
   Q.save_user_flags(vb); 
}


