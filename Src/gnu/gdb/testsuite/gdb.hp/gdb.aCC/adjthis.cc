#include "adjthis.h"
#include "stdio.h"

B::B()
{
    y = 2;
}

B2::B2()
{
    y = 2;
}

int main()
{
    B *b;
    B2 *b2;

    b = new C;
    b->f();
    delete b;

    b2 = new C2;
    b2->f();
    delete b2;

    return 0;
}
