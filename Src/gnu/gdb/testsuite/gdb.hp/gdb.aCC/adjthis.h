struct A {
    A() : x(0) {}
    int x;
    virtual int f (); 
    
};
struct B {
    B();
    int y;
    virtual int f (); 
};

struct C : A, B {
    int y;
    virtual int f (); 
};

struct A2 {
    A2() : x(0) {}
    int x;
    int array_a2[5000];
    virtual int f (); 
    
};
struct B2 {
    B2();
    int y;
    int array_b2[5000];
    virtual int f (); 
};

struct C2 : A2, B2 {
    int y;
    int array_c2[5000];
    virtual int f (); 
};

