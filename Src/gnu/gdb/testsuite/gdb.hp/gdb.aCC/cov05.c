#if __HP_aCC < 33700
#include <stdio.h>
int main() {  // skip test for HP-UX 10.x
  printf("Testing unwindability\n"
         "In ~TE()\n"
         "Caught the exception: In foo\n"
         "In ~TE()\n"
         "In ~TE()\n");
}
#else
/*
     Testng exception handling and unwinding from covariant functions.
     TA
     TB
     TE : TA, TB 
*/

#include <stdio.h>

struct TA {
  virtual TA* foo(); 
};

struct TB {
  virtual TB* goo(); 
};

TA* TA::foo() { return this; }
TB* TB::goo() { return this; }

struct TE : public TA, TB {
  TE* foo();
  TE* goo();
  ~TE();
};

TE::~TE() { printf("In ~TE()\n"); }
TE* TE::foo() { printf("In foo\n"); return this; }
TE* TE::goo() { 
  int local = 10;
  TE local_te;

  throw local_te;
  return this; 
}

int main() {
  printf("Testing unwindability\n");
  TE *pTE  = new TE;
  TB *pTB1 = pTE;
  try {
    TB *pTB2 = pTB1->goo();
    pTB2 = pTB1->goo();
  }
  catch(TE caught_te) {
    printf("Caught the exception: "); caught_te.foo(); 
  }
}
#endif
