/* Author 	: Saravanan Ekanathan (pes@india.hp.com)
 * Defect	:JAGab15881
 * Language 	:C++
 * Test 	: Test for demangling mangled names
 */

int main() {
    int* var;
    int* arr;
    var = new int;
    delete var;
    arr = new int[5];
    delete[] arr;
}



