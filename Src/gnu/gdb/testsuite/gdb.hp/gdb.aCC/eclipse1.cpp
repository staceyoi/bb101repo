#include <iostream>
using namespace std;

class test{
public:
	int i;
	char ch[10];
	
	void getData();
	void putData();
};

void test::getData(){
	cout<<"Integer: "<<endl;
	cin>>i;
	cout<<"Character:" <<endl;
	cin>>ch;
}

void test::putData() {
	cout<<"Integer is :"<<endl;
	cout<<"Character is :"<<endl;
}

int main(){
	
	test test1;
	test1.getData();
	test1.putData();
	return 0;
}

