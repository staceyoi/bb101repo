// Test file for exception handling support.

#if defined(__ia64) && !defined (FLAVOR_GAMBIT)
#include <iostream>
using namespace std;
#else
#include <iostream.h>
#endif

int foo (int i)
{
  if (i == 10)
    throw (char) 'c';
  if (i < 32)
    throw (int) 13;
  else
    return i * 2;
}

class C {
public:
  int i;
  void bar ();
};

void
C::bar () {

  try {
    foo (i);
  }
  catch (char c) {
    cout << "Got an except " << c << endl;
  }
  return;
}



int main()
{
  int j;
  C obj_c;

  obj_c.i = 10;

  try {
    obj_c.bar ();
  }
  catch (int x) {
    cout << "Got an except " << x << endl;
  }

  try {
    j = foo (20);
  }
  catch (int x) {
    cout << "Got an except " << x << endl;
  }
  
  try {
    try {
      j = foo (20);
    }
    catch (int x) {
      cout << "Got an except " << x << endl;
      throw;
    }
  }
  catch (int y) {
    cout << "Got an except (rethrown) " << y << endl;
  }

  // Not caught 
  foo (20);

  j++;
  
}
