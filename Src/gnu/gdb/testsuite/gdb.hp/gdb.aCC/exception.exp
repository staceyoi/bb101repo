# Copyright (C) 1997, 1998 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

#
# tests for exception-handling support
# Written by Satish Pai <pai@apollo.hp.com> 1997-07-23

# This file is part of the gdb testsuite

# Note: These tests are geared to the HP aCC compiler,
# which has an idiosyncratic way of emitting debug info
# for exceptions -- it uses a callback mechanism, which
# is different from the way g++ records exception info
# for debugging

# The tests are in two parts; the first part deals with
# statically linked (archive-bound) executables, and the
# second part repeats those tests with dynamically linked
# (shared bound) executables.  (In the latter case we use
# a different mechanism to get the address of the notification
# hook in the C++ support library.) The tests themselves are
# the same in both parts.
# 
# IMPORTANT:
# ---------
# IF YOU CHANGE A TEST IN ONE PART MAKE SURE YOU CHANGE IT
# --------------------------------------------------------
# IN THE OTHER PART TOO!
# ----------------------

# remove this when JAGae29175 is fixed
#if [istarget "ia64*-hp-*"] {
#    clone_output "This isn't supported on IPF yet.  Refer to JAGae29175"
#    return 0;
#}

if $tracelevel then {
        strace $tracelevel
        }

if { [skip_hp_tests] } then { continue }

#
# test running programs
#

# Part II : Shared-bound executables
# ----------------------------------

# Start with a fresh gdb
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir

set prms_id 0
set bug_id 0
set compile_flag 0

set testfile "exception"
set srcfile ${testfile}.cc
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile} "c++"] {
    return -1
}

source ${binfile}.ci
	
if { [istarget "ia64*-hp-gambit"] } {
	if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++ additional_flags=-DFLAVOR_GAMBIT} ] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
  }	
} else {	
	if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
  }	
}



if { [istarget "ia64*-hp-*"] } {
if {!$hp_aCC6_compiler} {
  return;
}
}
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}


if ![runto_main] then {
    perror "couldn't run to breakpoint"
    continue
}

# Set a catch catchpoint

send_gdb "catch catch\n"
gdb_expect {
   -re "Catchpoint \[0-9\]* \\(catch\\)\r\n$gdb_prompt $" {
       pass "catch catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "catch catch  (dynamic executable)" }
   timeout { fail "(timeout) catch catch  (dynamic executable)" }
}

# Set a throw catchpoint

send_gdb "catch throw\n"
gdb_expect {
   -re "Catchpoint \[0-9\]* \\(throw\\)\r\n$gdb_prompt $" {
       pass "catch throw (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "catch throw (dynamic executable)" }
   timeout { fail "(timeout) catch throw (dynamic executable)" }
}

# The catchpoints should be listed in the list of breakpoints.

send_gdb "info break\n"
gdb_expect {
   -re ".*\[0-9\]*\[ \]*catch catch\[ \]*keep y\[ \]*exception catch\[ \]*\r\n\[0-9\]*\[ \]*catch throw\[ \]*keep y\[ \]*exception throw\[ \]*\r\n$gdb_prompt $" {
       pass "info break with catchpoints (dynamic executable)"
   }    
   -re ".*$gdb_prompt $" { fail "info break (dynamic executable)" }
   timeout { fail "(timeout) info break (dynamic executable)" }
}

gdb_test "b C::bar" "Breakpoint.*exception.cc.*" "break at C::bar"
gdb_test "continue" "Breakpoint.*C::bar.*" "continue to bar"


# Info catch currently does not work with HP aCC. No easy way to
# list the active handlers on the stack.

send_gdb "info catch\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*Catch locations.*exception.cc:32.*char.*C::bar.*$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (dynamic executable)" }
   timeout { fail "(timeout) info catch (dynamic executable)" }
}

send_gdb "info catch 30\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*Catch locations.*exception.cc:32.*char.*C::bar.*$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (dynamic executable)" }
   timeout { fail "(timeout) info catch (dynamic executable)" }
}

send_gdb "info catch exception.cc:30\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*Catch locations.*exception.cc:32.*char.*C::bar.*$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (dynamic executable)" }
   timeout { fail "(timeout) info catch (dynamic executable)" }
}

send_gdb "info catch exception.cc : 30\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*Catch locations.*exception.cc:32.*char.*C::bar.*$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (dynamic executable)" }
   timeout { fail "(timeout) info catch (dynamic executable)" }
}


send_gdb "info catch all\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re "Catch locations.*exception.cc:32.*char.*C::bar.*50.*int.*main.*$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (dynamic executable)" }
   timeout { fail "(timeout) info catch (dynamic executable)" }
}

send_gdb "info catch \*\$b0-0x5\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re "Catch locations.*exception.cc:50.*int.* in main.*$gdb_prompt $" {
       pass "info catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (dynamic executable)" }
   timeout { fail "(timeout) info catch (dynamic executable)" }
}

# Get the first exception thrown
       
send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:13, catch location .*exception\\.cc:32\r\n.*$gdb_prompt $" {
      pass "caught a throw (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a throw (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a throw? (dynamic executable)" }
}

send_gdb "backtrace\n"
gdb_expect {
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=__EH_NOTIFY_THROW.*\r\n#2\[ \]*$hex in __eh_notify_throw.*\r\n#3\[ \]*$hex in foo \\(i=20\\) at .*exception\\.cc:13\r\n#4\[ \]*$hex in main.* at .*exception\\.cc:31\r\n$gdb_prompt $" {
      pass "backtrace after throw 1 (dynamic executable)"
   }
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=\[0-9\].*\r\n#2\[ \]*$hex in __eh_notify_throw.*\r\n#4\[ \]*$hex in foo \\(i=20\\) at .*exception\\.cc:8\r\n#5\[ \]*$hex in main.* at .*exception\\.cc:26\r\n$gdb_prompt $" {
      pass "backtrace after throw 2 (dynamic executable)"
   }
   -re ".*#3.*foo.*#4.*C::bar.*#5.*main.* at .*exception\\.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after throw 3 (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "backtrace after throw 4 (dynamic executable)" }
   timeout { fail "(timeout) backtrace after throw 5 (dynamic executable)" }
}

# Now intercept it when it is caught.

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:13, catch location .*exception\\.cc:32\r\n.*$gdb_prompt $" {
      pass "caught a catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch? (dynamic executable)" }
}

send_gdb "backtrace\n"
gdb_expect {
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=__EH_NOTIFY_CATCH.*\r\n.*\r\n#3\[ \]*$hex in __throw__.*\r\n#4\[ \]*$hex in foo \\(i=10\\) at .*exception.cc:13\r\n#5\[ \]*$hex in C::bar.*#6\[ \]*$hex in main.* at .*exception.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after catch (dynamic executable)"
   }
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=\[0-9\].*\r\n.*\r\n#3\[ \]*$hex in __throw__.*\r\n#4\[ \]*$hex in foo \\(i=10\\) at .*exception.cc:13\r\n#5\[ \]*$hex in C::bar.*#6\[ \]*$hex in main.* at .*exception.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after catch (dynamic executable)"
   }
   -re ".*#0.*__cxa_begin_catch.*#1.*C::bar.*#2.*main.*exception.cc:48.*$gdb_prompt $" {
      pass "backtrace after catch (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "backtrace after catch (dynamic executable)" }
   timeout { fail "(timeout) backtrace after catch (dynamic executable)" }
}
      
send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:57\r\n.*$gdb_prompt $" {
      pass "caught a throw (2) (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a throw (2) (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a throw (2)? (dynamic executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:57\r\n.*$gdb_prompt $" {
      pass "caught a catch (2) (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (2) (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch (2)? (dynamic executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:65\r\n.*$gdb_prompt $" {
      pass "caught a throw (3) (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a throw (3) (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a throw (3)? (dynamic executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:65\r\n.*$gdb_prompt $" {
      pass "caught a catch (3) (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (3) (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch (3)? (dynamic executable)" }
}

# Now the exception will be rethrown.

send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:67, catch location .*exception\\.cc:70\r\n.*$gdb_prompt $" {
      pass "caught a rethrow (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a rethrow (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a rethrow? (dynamic executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:67, catch location .*exception\\.cc:70\r\n.*$gdb_prompt $" {
      pass "caught a catch (4) (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (4) (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch (4)? (dynamic executable)" }
}

send_gdb "backtrace\n"
gdb_expect {
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=__EH_NOTIFY_CATCH.*\r\n.*\r\n#3\[ \]*$hex in __rethrow.*\r\n#4\[ \]*$hex in main.* at .*exception\\.cc:67\r\n$gdb_prompt $" {
      pass "backtrace after catch (4) (dynamic executable)"
   }
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=\[0-9\].*\r\n.*\r\n#3\[ \]*$hex in __rethrow.*\r\n#4\[ \]*$hex in main.* at .*exception\\.cc:67\r\n$gdb_prompt $" {
      pass "backtrace after catch (4) (dynamic executable)"
   }
   -re "#0.*__cxa_begin_catch.*#1.*main.*$gdb_prompt $" {
      pass "backtrace after catch (4) (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "backtrace after catch (4) (dynamic executable)" }
   timeout { fail "(timeout) backtrace after catch (4) (dynamic executable)" }
}

# Now the exception will be thrown, but not catch-able anywhere.

send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:15, catch location unknown\r\n.*$gdb_prompt $" {
      pass "caught an uncatchable throw (dynamic executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch an uncatchable throw (dynamic executable)" }
   timeout { fail "(timeout) after continue -- didn't catch an uncatchable throw? (dynamic executable)" }
}

gdb_exit

# Part I : Archive-bound executables
# ----------------------------------

set testfile "exception"
set srcfile ${testfile}.cc
set binfile ${objdir}/${subdir}/${testfile}
 
if [get_compiler_info ${binfile} "c++"] {
    return -1;
}
 
 
if { $gcc_compiled } then { continue }

set cmdline "$CXX_FOR_TARGET ${srcdir}/${subdir}/${srcfile} +A -Wl,-a,archive -g -o ${binfile}"

remote_exec build $cmdline

# Start with a fresh gdb

set prms_id 0
set bug_id 0

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if ![runto_main] then {
    perror "couldn't run to breakpoint"
    continue
}

# Set a catch catchpoint

send_gdb "catch catch\n"
gdb_expect {
   -re "Catchpoint \[0-9\]* \\(catch\\)\r\n$gdb_prompt $" {
       pass "catch catch (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "catch catch  (static executable)" }
   timeout { fail "(timeout) catch catch  (static executable)" }
}

# Set a throw catchpoint

send_gdb "catch throw\n"
gdb_expect {
   -re "Catchpoint \[0-9\]* \\(throw\\)\r\n$gdb_prompt $" {
       pass "catch throw (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "catch throw (static executable)" }
   timeout { fail "(timeout) catch throw (static executable)" }
}

# The catchpoints should be listed in the list of breakpoints.

send_gdb "info break\n"
gdb_expect {
   -re ".*\[0-9\]*\[ \]*catch catch\[ \]*keep y\[ \]*exception catch\[ \]*\r\n\[0-9\]*\[ \]*catch throw\[ \]*keep y\[ \]*exception throw\[ \]*\r\n$gdb_prompt $" {
       pass "info break with catchpoints (static executable)"
   }    
   -re ".*$gdb_prompt $" { fail "info break (static executable)" }
   timeout { fail "(timeout) info break (static executable)" }
}

gdb_test "b C::bar" "Breakpoint.*exception.cc.*" "break at C::bar"
gdb_test "continue" "Breakpoint.*C::bar.*" "continue to C::bar"

# Info catch currently does not work with HP aCC. No easy way to
# list the active handlers on the stack.

send_gdb "info catch\n"
gdb_expect {
   -re "Info catch not supported with this target/compiler combination.\r\n$gdb_prompt $" {
       pass "info catch (static executable)"
   }
   -re "Catch locations.*exception.cc:32.*char.*C::bar.*$gdb_prompt $" {
	pass "info catch (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "info catch (static executable)" }
   timeout { fail "(timeout) info catch (static executable)" }
}

# Get the first exception thrown
       
send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:13, catch location .*exception\\.cc:32\r\n.*$gdb_prompt $" {
      pass "caught a throw (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a throw (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a throw? (static executable)" }
}

send_gdb "backtrace\n"
gdb_expect {
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=__EH_NOTIFY_THROW.*\r\n#2\[ \]*$hex in __eh_notify_throw.*\r\n#3\[ \]*$hex in foo \\(i=10\\) at .*exception\\.cc:13\r\n#4\[ \]*$hex in C::bar.*#5\[ \]*$hex in main.* at .*exception\\.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after throw (static executable)"
   }
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=\[0-9\].*\r\n#2\[ \]*$hex in __eh_notify_throw.*\r\n#3\[ \]*$hex in foo \\(i=10\\) at .*exception\\.cc:13\r\n#4\[ \]*$hex in C::bar.*#5\[ \]*$hex in main.* at .*exception\\.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after throw (static executable)"
   }
   -re ".*#3\[ \]*$hex in foo \\(i=10\\) at .*exception\\.cc:13\r\n#4\[ \]*$hex in C::bar.*#5\[ \]*$hex in main.* at .*exception.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after throw (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "backtrace after throw (static executable)" }
   timeout { fail "(timeout) backtrace after throw (static executable)" }
}

# Now intercept it when it is caught.

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:13, catch location .*exception\\.cc:32\r\n.*$gdb_prompt $" {
      pass "caught a catch (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch? (static executable)" }
}

send_gdb "backtrace\n"
gdb_expect {
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=__EH_NOTIFY_CATCH.*\r\n.*\r\n#3\[ \]*$hex in __throw__.*\r\n#4\[ \]*$hex in foo \\(i=10\\) at .*exception.cc:13\r\n#5\[ \]*$hex in C::bar.*#6\[ \]*$hex in main.* at .*exception.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after catch (static executable)"
   }
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=\[0-9\].*\r\n.*\r\n#3\[ \]*$hex in __throw__.*\r\n#4\[ \]*$hex in foo \\(i=10\\) at .*exception.cc:13\r\n#5\[ \]*$hex in C::bar.*#6\[ \]*$hex in main.* at .*exception.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after catch (static executable)"
   }
   -re ".*#1\[ \]*$hex in C::bar.*#2\[ \]*$hex in main.*exception.cc:48\r\n$gdb_prompt $" {
      pass "backtrace after catch (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "backtrace after catch (static executable)" }
   timeout { fail "(timeout) backtrace after catch (static executable)" }
}
      
send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:57\r\n.*$gdb_prompt $" {
      pass "caught a throw (2) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a throw (2) (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a throw (2)? (static executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:57\r\n.*$gdb_prompt $" {
      pass "caught a catch (2) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (2) (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch (2)? (static executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:65\r\n.*$gdb_prompt $" {
      pass "caught a throw (3) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a throw (3) (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a throw (3)? (static executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:15, catch location .*exception\\.cc:65\r\n.*$gdb_prompt $" {
      pass "caught a catch (3) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (3) (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch (3)? (static executable)" }
}

# Now the exception will be rethrown.
send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:67, catch location .*exception\\.cc:70\r\n.*$gdb_prompt $" {
      pass "caught a rethrow (4) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a rethrow (4) (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a rethrow (4)? (static executable)" }
}

send_gdb "continue\n"
gdb_expect {
   -re "Continuing.*Catchpoint \[0-9\]* \\(exception caught\\), throw location.*exception\\.cc:67, catch location .*exception\\.cc:70\r\n.*$gdb_prompt $" {
      pass "caught a catch (4) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch a catch (4) (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch a catch (4)? (static executable)" }
}


send_gdb "backtrace\n"
gdb_expect {
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=__EH_NOTIFY_CATCH.*\r\n.*\r\n#3\[ \]*$hex in __rethrow.*\r\n#4\[ \]*$hex in main.* at .*exception\\.cc:67\r\n$gdb_prompt $" {
      pass "backtrace after catch (4) (static executable)"
   }
   -re "#0\[ \]*__d_eh_break.*\r\n#1\[ \]*$hex in __d_eh_notify_callback \\(eh_type=\[0-9\].*\r\n.*\r\n#3\[ \]*$hex in __rethrow.*\r\n#4\[ \]*$hex in main.* at .*exception\\.cc:67\r\n$gdb_prompt $" {
      pass "backtrace after catch (4) (static executable)"
   }
   -re ".*#1\[ \]*$hex in main.*exception.cc:70\r\n$gdb_prompt $" {
      pass "backtrace after catch (4) (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "backtrace after catch (4) (static executable)" }
   timeout { fail "(timeout) backtrace after catch (4) (static executable)" }
}

# Now the exception will be thrown, but not catch-able anywhere.

send_gdb "continue\n"
gdb_expect {
   -re "Continuing\\.\r\nGot.*\r\nCatchpoint \[0-9\]* \\(exception thrown\\), throw location.*exception\\.cc:15, catch location unknown\r\n.*$gdb_prompt $" {
      pass "caught an uncatchable throw (static executable)"
   }
   -re ".*$gdb_prompt $" { fail "didn't catch an uncatchable throw (static executable)" }
   timeout { fail "(timeout) after continue -- didn't catch an uncatchable throw? (static executable)" }
}

gdb_exit

