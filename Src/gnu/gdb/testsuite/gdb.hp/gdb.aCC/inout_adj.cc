#include <stdio.h>

struct TA {
  virtual TA* foo();
};

struct TB {
  virtual TB* goo();
};

TA* TA::foo() { return this; }
TB* TB::goo() { return this; }

struct TE : public TA, TB {
  TE* foo();
  TE* goo();
};

TE* TE::foo() { return this; }
TE* TE::goo() { 
        printf ("this &TE = %x\n", this); return this;
}

int main() {
  printf ("Test 1\n");

  TE *pTE  = new TE;
  printf ("pTE = %x\n", pTE);

  TB *pTB1 = pTE;

  TB *pTB2 = pTB1->goo();

  printf ("pTB2 = %x\n", pTB2);
  if ((char*)pTE != (((char*)pTB2 - sizeof(void*))))
     printf ("Test 1 FAILED\n");

}
