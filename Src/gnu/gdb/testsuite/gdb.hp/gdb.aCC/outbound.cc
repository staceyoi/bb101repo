#include <stdio.h>
class A {
    int a;
public:
};
class B {
    int b;
};
/* For Covariant return type- Uncomment this*/ 
class C : public B,public A { 

/* One below is a a virtual co-variant */
/*class C : virtual public A, public B { */
    int c;
};

class relateA {
    int relatea;
public:
    virtual A* func(int i);
};

A* relateA::func(int i) {
   printf ("I am in the class A ");
   return (new A);
}
class relateB {
    int relateb;
public:
    virtual B* func(int i);
};

B* relateB::func(int i) {
   printf ("I am in the class B ");
   return (new B);
}

class covary :public relateA,public relateB{
   int covariant;
public:
    C* func(int i); 
};

C* covary::func(int i) {
   printf ("I am in the class covar ");
   return (new C);
}

int main() {
   B *co = new C;
//only outbound thunk
   covary* rb = new covary;
// Both inbound and Outbound thunk
//   relateB* rb = new covary;
   B* b = rb->func(1);
}
