extern "C" int printf(const char *, ...);

struct A
{
    virtual void polymorphic() { }
    A(int x) : a(x) { }
    int a;
};

struct B
{
    virtual void polymorphic() { }
    B(int x) : b(x) { }
    int b;
};

struct C : public A, public B
{
    virtual void polymorphic() { }
    C(int x, int y, int z) : A(x), B(y), c(z) { }
    int c;
};

C thing(123, 456, 789);

void look(B * it)
{
    C * cast_it = (C *)it;
    printf("%d, %d, %d\n", cast_it->a, cast_it->b, cast_it->c);
}

int main(void)
{
    look(&thing);
    return 0;
}

