/* Author 	: Saravanan Ekanathan (pes@india.hp.com)
 * Defect	:JAGae12279
 * Language 	:C++
 * Test		: To test whether "info locals" prints
 *		 the runtime type of class after one
 *		 sets the print obj on option
 */

#if defined(__ia64) && !defined (FLAVOR_GAMBIT)
#include <iostream>
using namespace std;
#else
#include <iostream.h>
#endif

class base {
private :
int xx;
int yy;
public :
base ();
~base();
virtual void vfunc();
};

class derived : public base {
private :
int aa;
int bb;
public :
derived();
~derived();
virtual void vfunc();
};

base::base()
{
xx = 10;
yy = 20;
}

base::~base()
{
xx = 0;
yy = 0;
}


derived::derived()
{
aa = 100;
bb = 200;
}

derived::~derived()
{
aa = 0;
bb = 0;
}

void base::vfunc()
{
cout << "Base :: vfunc" << endl;
}

void derived::vfunc()
{
cout << "Derived :: vfunc" << endl;
}

int main()
{
base * nullbp = 0;
base * bp1, * bp2;

bp1 = new base;
bp2 = new derived;

cout << "Ready for testing" << endl;
bp1->vfunc();
bp2->vfunc();
cout << "Test now";
}



