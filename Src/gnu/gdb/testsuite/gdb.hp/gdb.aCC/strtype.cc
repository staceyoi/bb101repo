/* JAGae12211 - Problems in inserting breakpoints Author : pes <pes@india.hp.com> */
#include <string>
#if defined(__ia64) && !defined (FLAVOR_GAMBIT)
#include <iostream>
using namespace std;
#else
#include <iostream.h>
#endif
int main(){
  string s = "something";
  s.append("appending");
  s.append("pe",1,2);
  s.append(".",3);
  s.append("s","a");
}

