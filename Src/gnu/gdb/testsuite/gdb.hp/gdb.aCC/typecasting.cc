/* Author : Saravanan Ekanathan (pes@india.hp.com) */
#if defined(__ia64)
#include<iostream>
using namespace std; 
#else
#include<iostream.h>
#endif
class Base { int x; public: Base* func(); };
Base* Base::func() {
   return this; };
class SecondaryBase {
  int y;
  public:
  SecondaryBase* func1();
};
SecondaryBase* SecondaryBase::func1() {
   return this;
};
class ThirdBase {
  int y;
  public:
  ThirdBase* func3();
};
ThirdBase* ThirdBase::func3() {
   return this;
};
class Derive : public Base,public SecondaryBase ,public ThirdBase {
  int z;
};
class Grand : public Derive {
 int z1;
public:
Grand* func10();
 
};
Grand* Grand::func10() {
  return this;
}
int main() {
    Base *bas = new Derive;
    Derive *d = new Derive;
    /* Change primary base*  to derive* */
    d = (Derive*)bas->func();
    cout<<(Derive*)bas->func()<<endl;
    SecondaryBase* bp = new Derive;
    /* gdb never changes secondary baseclass* to derive* */
    cout << bp->func1()<<endl;
    /* Seconary base* to Derive*   */
    Derive *d1 = new Derive;
    d1 = (Derive*)bp->func1();
    cout << (Derive*) bp->func1()<<endl;
    ThirdBase* tbp = new Derive;
    ThirdBase* tbp1;
    cout<<tbp->func3()<<endl;
    /* Change Thirdbase* to Derive* and back to ThirdBase*  */
    tbp1 = (ThirdBase*)(Derive*)tbp->func3();
    cout<<(ThirdBase*)(Derive*)tbp->func3()<<endl;
    SecondaryBase* gr = new Grand;
    Grand * junk  = new Grand;
    Grand* junk1;
   /* Changing indirect secondary base* to Grandchild* or Derive* */
    junk1 = (Grand*) gr->func1();
    cout <<(Grand*) gr->func1()<<endl;


   Grand* g = new Grand;
   Base *prim;
   SecondaryBase *sec;
   ThirdBase *third;
   Derive* der;
   prim =  (Base*)g->func10();
   cout<< (Base*)g->func10()<<endl; 
   sec =  (SecondaryBase*)g->func10();
   cout<< (SecondaryBase*)g->func10()<<endl; 
   third = (ThirdBase*)g->func10();
   cout<< (ThirdBase*)g->func10()<<endl; 
   der = (Derive*)g->func10();
   cout<< (Derive*)g->func10()<<endl; 
}
