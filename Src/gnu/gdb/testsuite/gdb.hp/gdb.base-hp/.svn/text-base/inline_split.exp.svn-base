# inline_split.exp -- Expect script to test DOC inline function backtrace 
# capability.
#
# The test name comes from the fact that inline instances split under
# optimization into multiple address ranges, which HP gdb did not support
# at one time.

# Copyright (C) 2006 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */


if $tracelevel then {
    strace $tracelevel
}

if { ![istarget "ia64*-hp*-*"]} {
    verbose "HPUX DOC test ignored on this target."
    return 0
}

set testfile inline_split
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

# Create and source the file that provides information about the compiler
# used to compile the test case.
if [get_compiler_info ${binfile} "c++"] {
    return -1;
}
source ${binfile}.ci

# aCC5 did inlining completely differently, but we don't care
if { $hp_aCC5_compiler } { return 0 }

# do build and test four times, with all combinations of with/without 
# -DINLINE_LEAF, default (+O1) optimization and -O
set define_option "+Onoinline=bar,leaf"

for {set i 0} {$i < 4} {incr i 1} {

# Build the executable 
if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug additional_flags=$define_option]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

  # do test once per path through the program, checking the backtrace of each
  set line 90
  for {set path 0} {$path < 5} {incr path 1} {

    # Do test once per signal type, checking the backtrace of each.
    # Skip the breakpoint tests if -DINLINE_LEAF is set, since 
    # breakpoints in inlined functions aren't actually supported yet.
    set signal 0
    if { "${define_option}" != "" } { set signal 1 }
    incr line 1
    for { } {$signal < 3} {incr signal 1} {

	gdb_start
	gdb_load ${binfile}

        if { "${signal}" == 0 } \
		{ gdb_test "break 25" "Breakpoint 1.*$testfile.*" }

	if { ( ${i} == 3 ) && ( "${signal}" > 0 ) && ( "${path}" == 4 ) } \
		{ setup_xfail ia64*-hp-* JAGag06704  }
	gdb_test "run $path $signal" "Starting program.*leaf.*" \
		"run with args $path $signal compiled : $optimize $define_option"

	# gdb_test "p my_signal" ".*= (NONE|SEGV|BUS).*" "value of my_signal"
	# gdb_test "p/x ptr" ".*= 0x.*" "check value of ptr"

	# XFAIL for breakpoints above +O1 until they handle predicated code
	if { ( ${i} > 1 ) && ( "${signal}" == 0 ) && ( "${path}" > 0 ) } \
		{ setup_xfail ia64*-hp-* JAGag06703 }			 \
	elseif { ( ${i} == 3 ) && ( "${signal}" > 0 ) && ( "${path}" > 2 ) } \
		{ setup_xfail ia64*-hp-* JAGag06704  }
	gdb_test "bt" \
	  ".*leaf.*(inline tum|inline foo|bar).*main.*${testfile}.c\:${line}.*" \
	  "bt with args $path $signal compiled : $optimize $define_option, expected line $line."

	gdb_exit

    # end test once per signal to be caused
    }

  # end test once per path through the program
  }

# loop back and do it all again with different compilation options
if { "${i}" == 0 } { set define_option "-DINLINE_LEAF +Onoinline=bar" }
if { "${i}" == 1 } { set optimize "-O +Onolimit"; set define_option "+Onoinline=bar,leaf" }
if { "${i}" == 2 } { set define_option "-DINLINE_LEAF +Onoinline=bar" }
}

return 0
