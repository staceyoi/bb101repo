//
//
// File:     statmgr.cpp
//
// Author:   Integrated Telecom Solutions, Inc.
//
// Synopsis: This is the implementation of the statistics manager
//
// Note:     na
//
// Revision History:
//
//     Date     Version Author Synopsis
//   ---------- ------- ------ ---------------------------------------------
//   03/09/2004   1.0.0 N.B.    Initial version
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>     //for memcpy
#include <signal.h>     //for setting up signal handlers for timeouts
#include <unistd.h>     //for pause
#include <time.h>
#include <sys/timeb.h>

#include <sys/types.h>  //for the key_t for IPC Key
#include <sys/ipc.h>    //for the IPC structure
#include <sys/shm.h>    //for the shared memory
#include <sys/sem.h>    //for the semaphores
#include <sys/stat.h>   //for IPC permissions
#include <errno.h>      //check error codes
#include <assert.h>     //Validate byte boundaries...
#include <fcntl.h>      //File control

#include <map>
#include <iostream>

//#include "its.h"
//#include "common.h"
//#include "timers.h"

typedef unsigned long long      U64;
typedef unsigned long           U32;
typedef unsigned short          U16;
typedef unsigned char           U8;

typedef          long long      S64;
typedef          long           S32;
typedef          short          S16;
typedef          char           S8;


//////////////////////////////////////////////////////////////////////////
// Inline of timers.h...
//////////////////////////////////////////////////////////////////////////
/* Timer subsystem status values */
#define ITS_TIMERS_NOT_INITIALIZED      0
#define ITS_TIMERS_INITIALIZED          1

/* Maximum number of message timers allowed for one send operation */
#define ITS_MAX_MESSAGE_TIMERS          10

/* Value indicating no message timer is specified for an operation */
#define ITS_TIMER_INT_NONE              -1

/* Timer Environment Type Definitions */
typedef enum
{
  ITS_TIMER_ENV_SELECT          = 0,      /* select() loop IS being used */
  ITS_TIMER_ENV_NO_SELECT       = 1       /* select() loop is NOT being used */
} its_timer_env_def;

#if 1
typedef long long interval_def;      /* For Timer intervals */
#else
typedef struct timespec* interval_def;
#endif


/*
************************************************************************
*  Structures
************************************************************************
*/
/* System Timer TAG */
#if defined(__TANDEM)
typedef S16 sys_timer_tag_def;
#elif defined(__hpux)
typedef U32 sys_timer_tag_def;
#else //!__TANDEM||__hpux
#error "must define sys_timer_tag_def"
#endif //__TANDEM ||_hpux

typedef void (timer_func_sel_def) (int signo, timer_t timer_id,
                            sys_timer_tag_def sys_timer_tag,
                            U16 user_data_1, U32 user_data_2);
typedef void (timer_func_nosel_def) (int signo, siginfo_t *info,
                                      void *context);

/* Individual timer entry */
typedef struct its_timer_entry_def
{
  int                   status;         /* Status of entry:  */
                                        /*   0 = Available   */
                                        /*   1 = In user     */
  timer_t               timer_id;       /* Id returned from timer_create() */
  int                   pipe_fd [2];    /* FDs for pipe: 0 = Read, 1 = Write */
  int                   signo;          /* Signal number for the timer */
  sys_timer_tag_def     sys_timer_tag;  /* System Timer Tag */
  U16                   user_data_1;    /* User data - 2 bytes */
  U32                   user_data_2;    /* User data - 4 bytes */
  timer_func_sel_def   *func_sel;       /* Signal handler for select loop */
  timer_func_nosel_def *func_no_sel;    /* Signal handler for NO select loop */
  struct itimerspec     _timer_spec;    /* Current Timer values */

} its_timer_entry_def;


/* Timer information table */
typedef struct
{
  int                    subsys_status; /* Status of timer subsystem:   */
                                        /* - ITS_TIMERS_INITIALIZED     */
                                        /* - ITS_TIMERS_NOT_INITIALIZED */
  its_timer_env_def      env_type;      /* Select loop or non-select loop */
  interval_def           msg_timers[ITS_MAX_MESSAGE_TIMERS];
                                        /* Array of message timeout intervals */
  int                    msg_timers_count;         
                                        /* Number of message timeout intervals */
                                        /*   currently in msg_timer array      */
  int                    msg_timers_idx;
                                        /* Index of next message timer to user */
  int                    count;         /* Number of entries allocated */
  its_timer_entry_def    timer_ent;     /* Timer entry */

} its_timer_info_def;

/*
************************************************************************
*  Function Prototypes
************************************************************************
*/

int its_timer_init (
    its_timer_env_def       timer_env);

int its_timer_create (
    int                     signo,
    timer_func_sel_def     *func_sel,
    timer_func_nosel_def   *func_no_sel,
    timer_t                *timer_id);

int its_timer_delete (
    timer_t     timer_id);

int its_timer_set_message (
    void               *user_timer_id,
    interval_def       *timeout_int,
    S16                 timer_count,
    S16                 user_data1,
    S32                 user_data2,
    sys_timer_tag_def  *sys_timer_id);

bool its_timer_cancel_message (
    void               *user_timer_id,
    sys_timer_tag_def  *sys_timer_id);

int its_timer_set_general (
    timer_t             timer_id,
    interval_def        timeout_int,
    U16                 param1,
    U32                 param2,
    sys_timer_tag_def  *tag);

int its_timer_set_general_cp (
    timer_t             timer_id,
    U32                 user_timer_id,
    interval_def        timeout_int,
    U16                 user_data1,
    U32                 user_data2,
    sys_timer_tag_def  *sys_timer_id);

bool its_timer_cancel_general (
    timer_t             timer_id,
    sys_timer_tag_def  *sys_timer_tag);
    
bool its_timer_cancel_general_cp (
    timer_t             timer_id,
    U32                *user_timer_id,
    sys_timer_tag_def  *user_data2);

timer_func_sel_def its_timer_handler_general_cp;
/* void its_timer_handler_general_cp ( */
/*     int       signo,  */
/*     timer_t   timer_id,  */
/*     sys_timer_tag_def sys_timer_tag */
/*     U16       user_data1,  */
/*     U32       user_data2); */

void its_timer_get_msg_timeout (
    timeval     *timer);

void its_timer_init_msg_timers (void);

timer_func_nosel_def its_timer_timeout;
/* void its_timer_timeout ( */
/*     int          signo, */
/*     siginfo_t   *sig_info, */
/*     void        *context); */

bool its_timer_pre_select (
    fd_set *readmask);

bool its_timer_post_select (
    fd_set      *readmask);


typedef void (its_timer_app_callback)(timer_t   timer_id, 
                                      U16       user_data1, 
                                      U32       user_data2);
its_timer_app_callback its_app_general_timeout;
//////////////////////////////////////////////////////////////////////////


using namespace std;


// Private Constants
//
//

// Private Variables
//
//
static int      shmId;     // the shared memory identifier
static char*    shmPtr;    // the pointer to the shared memory
static int      semId;     // Number by which the semaphore is known within a program
static key_t    SEM_KEY;
static bool     stats_need_flush = false; // Used in the signal handler
static key_t    SHM_KEY;
static size_t   SHM_SIZE;
static char*    startRegSetRow;
static char*    startStatRow;
static char*    startTimeStamp;
static char*    RegSetNumRowsptr;
static char*    startGrpIDRegSetIdxRow;
static char*    NumStatRowsPtr;
static timer_t  statTimerId;
static int      statTimeoutSeconds;
static sys_timer_tag_def sys_timer_tag;

// Prototypes
// 
// 
static int
    timer_setup( void );
static int
    get_stat_collection_interval( void );
static void
    stat_timeout_handler( int         signo
                        , siginfo_t*  info
                        , void*       context );    // timeout handler
static void
    sig_shutdown( int  signo );     // shutdown handler

// Macros
// 
// 

#undef  showline
//#define showline printf("[%-30.30s: %06.6d]: ", __FILE__, __LINE__)
#define showline _showline(__FILE__,__LINE__)
#undef  Debug
#define Debug(a,b)             showline; b
#undef  log_msg
#define log_msg(a,b,c,d,e,...) showline; printf(__VA_ARGS__)

inline char* _timestring(void)
{
    char *ft=(char*)malloc(40);
    struct timeb tb;
    struct tm ts;
    ftime(&tb);
    strftime(ft, 40, "%H:%M:%S", localtime_r(&tb.time,&ts));
    char mt[6];
    sprintf(mt, ".%03d", tb.millitm);
    strncat(ft,mt,40);
    return ft;
}

inline void _showline(char *file, int line)
{
#define _Debug_fmt_file_line_(f)    "%12.12s [%20.20s: %5d] " f
    char *fname=strchr(file,'/');
    fname = (fname==NULL) ? file : fname+1;
    printf("%12.12s [%20.20s: %5d] ", _timestring(), fname, line);
}


//
// stat_mgr_init
//
// This function creates the semaphores and
// shared memory and attaches to it.
//
// returns int 0-successful 1-unsuccessful
//
int
    stat_mgr_init( void )
{
    int  iRetShmGet = 0;

    Debug(stats, printf("Doing stats mgr init...\n") );

    //
    // register shutdown signal handler
    //
    if ( signal( SIGINT, sig_shutdown ) == SIG_ERR )
    {
        Debug(stats, perror( "Cant catch SIGINT" ) );
        log_msg( NULL, -1, ITS_LOG_ERR, 0, NULL
               , "Statistics Manager: Cant catch SIGINT for shutdown" );
    }

    Debug(stats, printf("Init complete, setting timers\n") );

    timer_setup();

    return 0;

} // stat_mgr_init( void )




//
// stat_mgr_shutdown
//
// This function is called by call flow during shutdown.
// It releases the semaphores and shared memory after
// flushing any statistics that are still in memory.
//
// returns int 0-successful 1-unsuccessful
//
int
    stat_mgr_shutdown( void )
{
    Debug(stats, printf( "Doing stats mgr shutdown...\n" ) );

    return 0;

} // stat_mgr_shutdown( void )




//
// signal handler for shutdown
//
void
    sig_shutdown( int  signo )
{
    if( signo == SIGINT )
    {
        stat_mgr_shutdown();
        exit( 0 );
    }
    else
    {
        Debug(stats, printf( "Statistics manager: received unknown signal: %d\n", signo ) );
    }

} // sig_shutdown( int  signo )


//
// timer_setup
//
// This function is called by stat_mgr_init.
// It creates and initializes the timer API. It sets
// the first timeout. Future timeouts are set by the
// timeout handler function.
//
// returns int 0-successful   -1 -unsuccessful
//
int
    timer_setup( void )
{
    // the stat manager does NOT use select loop
    //
    Debug(stats, printf( "init timer\n" ) );
    if ( its_timer_init( ITS_TIMER_ENV_NO_SELECT ) < 0 )
    {
        Debug(stats, perror( "StatManager: Error initializing Timer, shutting down..." ) );
        stat_mgr_shutdown();
        return -1;
    }
    // set up the signal handler
    //
    Debug(stats, printf( "create timer\n" ) );
    int  ret = its_timer_create( SIGRTMIN               // the signal number
                               , NULL                   // handler for select loop
                               , stat_timeout_handler   // handler for my timeout
                               , &statTimerId );        // ID will be output returned
    if ( ret < 0 )
    {
        Debug(stats, perror( "StatManager: Error creating timer, shutting down..." ) );
        stat_mgr_shutdown();
        return -1;
    }
    // get timeout value from environment
    //
    statTimeoutSeconds = get_stat_collection_interval() * 100;
    Debug(stats, printf( "set timer: %d\n", statTimeoutSeconds ) );
    ret = its_timer_set_general( statTimerId
                               , statTimeoutSeconds
                               , 0
                               , 0
                               , &sys_timer_tag );
    if ( ret < 0 )
    {
        Debug(stats, perror( "StatManager: Error setting timer, shutting down..." ) );
        stat_mgr_shutdown();
        return -1;
    }
    // go to sleep until interrupted

    return 0;

} // timer_setup( void )


//
// timer_setup
//
// This is the function that will be called when the timeout occurs
// It will call the stat_mgr_flush and then reset the timer
//
// Beacause it is dangerous to perform complex operations
// inside a signal handler, the signal handler will mark a flag
// and return...  The real work is up to the main loop
// note: It can also be hard to debug complex operations inside of a
// signal handler.  GDB for one, seems confused regarding its scope
//
// params: it currently does not care about its params. It conforms
//               to the handler function defined in timers.h
//
// returns void
//

void
    stat_timeout_handler( int         signo
                        , siginfo_t*  info
                        , void*       context )
{
    // Keep things simple

    stats_need_flush = true;

    // go to sleep until interrupted
    //pause();

} // stat_timeout_handler( int, siginfo_t*, void* )


//
// get_stat_collection_interval
//
// Gets the statistics collection interval value from the environment
//
// params: it currently does not care about its params. It conforms
//               to the handler function defined in timers.h
//
// returns void
//
int
    get_stat_collection_interval( void )
{
    char*  char_interval          = getenv("STATS_COLLECTION_INTERVAL");
    int    iSTAT_COLLECT_INTERVAL = atoi( char_interval );

    if ( iSTAT_COLLECT_INTERVAL <= 0 )
    {
        iSTAT_COLLECT_INTERVAL = 300;
    }

    return iSTAT_COLLECT_INTERVAL;

} // get_stat_collection_interval( void )


//
// the main function for testing
//
int
    main( int   argc
        , char* argv[] )
{
    int i = 0;

    Debug(stats, printf("Statistics manager: main()\n") );
    if ( stat_mgr_init() == 0 )
    {
        for (stats_need_flush=false; i<10; stats_need_flush=false)
        {
            i++;
            pause();
            if (stats_need_flush) // Set inside timeout signal handler
            {
                Debug( stats, printf("Toy got a timeout\n") );
                // then reset the timer
                //
                int  ret = its_timer_set_general( statTimerId
                                                , statTimeoutSeconds// coll.interval
                                                , 0                 // param1
                                                , 0                 // param2
                                                , &sys_timer_tag ); // tag
                if ( ret < 0 )
                {
                    Debug(stats, perror( "StatManager: Error setting timer, shutting down..." ) );
                    stat_mgr_shutdown();
                }
            }
        }
    }
    else
    {
        log_msg( NULL, -1, ITS_LOG_ERR, 0, NULL
               , "Statistics Manager: Initialization failed" );
    }

    return stat_mgr_shutdown();

} // main( int, char*[] )



//======================================================================
/*
************************************************************************
*  Private globals
************************************************************************
*/

its_timer_info_def  *timer_info_g = NULL;

// Key Definition for Absolute and User Time Tables
typedef U64 time_key_def;
typedef S64 abs_time_def;

/* { */
/*     U32         user_timer_id; */
/*     U32         user_data2; */
/* } time_key_def; */

// GENERAL ABSOLUTE TIME TABLE - maps absolute timeout values to
// userdata2 values
typedef multimap<abs_time_def, time_key_def> GnrlTimeTableDef;
GnrlTimeTableDef g_abs_time_table;

// GENERAL USER DATA TABLE - maps userdata2 values to absolute timeout values 
typedef map<time_key_def, abs_time_def> GnrlUserTableDef;
GnrlUserTableDef g_user_time_table;

#define time_key(uti,ud2)   ((((U64)(uti))<<32) | ((U64)(ud2)))
#define key_uti(x)          ((U32)(((U64)(x))>>32))
#define key_ud2(x)          ((U32)(U64(x)&0xFFFFFFFF))

#define abs_secs(a)         (U32)((a)/100)
#define abs_msec(a)         (U32)((a)%100)

#define abs_hi(a)           (U32)((a)>>32)
#define abs_lo(a)           (U32)((a)&0xFFFFFFFF)

/*
************************************************************************
*  Forwards
************************************************************************
*/

static its_timer_entry_def * its_get_timer_entry (
    its_timer_info_def *timer_info);

static its_timer_entry_def * its_find_timer_by_id (
    its_timer_info_def *timer_info,
    timer_t             timer_id);


void its_app_general_timeout (
		timer_t 	timer_id, 
		U16 			user_data1, 
		U32 			user_data2)
{
	log_msg( NULL, -1, ITS_LOG_INFO, 0, NULL, 
	         "timers: Default its_app_general_timeout()" );
}


int its_timer_init (
    its_timer_env_def     timer_env)
{
    int                   i;
    size_t                table_size;
    its_timer_entry_def  *timer_ent;


    /* Check for already initialized */
    if (timer_info_g != NULL)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Init: Attempt to initialize more than once");
        return true;
    }

    /* Allocate its_timer_info structure */
    table_size = ((sizeof(*timer_info_g) - sizeof(timer_info_g->timer_ent)) +
                  (sizeof(timer_info_g->timer_ent) * 10));   /* Start with 10 entries */  
    if ((timer_info_g = (its_timer_info_def *) malloc (table_size)) == NULL)
    {
        log_msg (NULL, -1, ITS_LOG_CRIT, 0, NULL,
                "Timer Init: Failure allocating timer table");
        return true;
    }

    /* Initialize timer table values */
    timer_info_g->env_type = timer_env;

    for (i = 0; i < ITS_MAX_MESSAGE_TIMERS; i++)
    {
        timer_info_g->msg_timers [i] = ITS_TIMER_INT_NONE;

    } /* End 'for' loop */

    timer_info_g->msg_timers_count = 0;
    timer_info_g->msg_timers_idx   = 0;
    timer_info_g->count            = 10;

    for (i = 0, timer_ent = &timer_info_g->timer_ent;
         i < 10;
         i++, timer_ent++)
    {
        timer_ent->status      =  0;      /* Available */
        timer_ent->timer_id    = (timer_t)-1;
        timer_ent->pipe_fd [0] = -1;
        timer_ent->pipe_fd [1] = -1;
        timer_ent->signo       = -1;
        timer_ent->sys_timer_tag =  0;
        timer_ent->user_data_1 =  0;
        timer_ent->user_data_2 =  0;
        timer_ent->func_sel    = NULL;
        timer_ent->func_no_sel = NULL;

    } /* End 'for' loop */

    /* Set 'initialized' status */
    timer_info_g->subsys_status = ITS_TIMERS_INITIALIZED;

    /* Done */
    return false;         /* Successful */

} /* End of its_timer_init() */

its_timer_entry_def * its_get_timer_entry (
    its_timer_info_def *timer_info)
{
    int                   i;
    its_timer_entry_def  *timer_ent;


    for (i = 0, timer_ent = &timer_info->timer_ent;
         i < timer_info->count;
         i++, timer_ent++)
    {
        if (timer_ent->status ==  0)      /* Available */
        {
            timer_ent->status = 1;          /* In use */
            return timer_ent;
        }
    } /* End 'for' loop */


    /* Didn't find any available entries - Allocate some more memory */
    return NULL;

} /* End of its_get_timer_entry() */

/*-----------------------------------------------------------------*/

its_timer_entry_def * its_find_timer_by_id (
    its_timer_info_def *timer_info,
    timer_t             timer_id)
{
    int                   i;
    its_timer_entry_def  *timer_ent;


    for (i = 0, timer_ent = &timer_info->timer_ent;
         i < timer_info->count;
         i++, timer_ent++)
    {
        if (timer_ent->timer_id == timer_id)
        {
            return timer_ent;
        }
    } /* End 'for' loop */


    /* Didn't find the timer id */
    return NULL;

} /* End of its_find_timer_by_id() */

its_timer_entry_def * its_find_timer_by_signo (
    its_timer_info_def *timer_info,
    int                 signo)
{
    int                   i;
    its_timer_entry_def  *timer_ent;


    for (i = 0, timer_ent = &timer_info->timer_ent;
         i < timer_info->count;
         i++, timer_ent++)
    {
        if (timer_ent->signo == signo)
        {
            return timer_ent;
        }
    } /* End 'for' loop */


    /* Didn't find the timer id */
    return NULL;

} /* End of its_find_timer_by_signo() */

void its_timer_list_timers(void)
{
    its_timer_entry_def    *timer_ent;
    int                     i;

    /* This function only called if its::_debug.timers is set.
     *  Thus, there is no reason to use Debug(timers, ...); */

    Debug(timers, printf( "GLOBAL Timers: Status=%d, type=%d, msg-count=%d, msg-index=%d, count=%d\n"
                        , timer_info_g->subsys_status
                        , timer_info_g->env_type
                        , timer_info_g->msg_timers_count
                        , timer_info_g->msg_timers_idx
                        , timer_info_g->count));
    for (i = 0, timer_ent = &timer_info_g->timer_ent;
         i < timer_info_g->count;
         i++, timer_ent++)
    {
        if (timer_ent->status)
        {
            Debug(timers,
                 printf( "GLOBAL Timer %d/%d (%s), TAG(user_timer_id)=%08x, UD1=%04x, UD2=%08x, Interval=%d.%09d, Value=%d.%09d\n"
                       , timer_ent->timer_id, i
                       , timer_ent->status ? "active  " : "inactive" 
                       , timer_ent->sys_timer_tag
                       , timer_ent->user_data_1
                       , timer_ent->user_data_2
                       , timer_ent->_timer_spec.it_interval.tv_sec
                       , timer_ent->_timer_spec.it_interval.tv_nsec
                       , timer_ent->_timer_spec.it_value.tv_sec
                       , timer_ent->_timer_spec.it_value.tv_nsec ));
        }
    }

    struct timeval tv;
    gettimeofday(&tv, NULL);
    abs_time_def now = ((U64)tv.tv_sec* 100) + tv.tv_usec/10000;
    Debug(timers, printf( "       Timer ___now____: 0x%08x%08x\n"
                        , abs_secs(now)
                        , abs_msec(now)));

    GnrlTimeTableDef::iterator timerMapIter = g_abs_time_table.begin();
    for (timerMapIter  = g_abs_time_table.begin();
         timerMapIter != g_abs_time_table.end();
         timerMapIter++)
    {
        Debug(timers, printf( "ABS    Timer expiration: 0x%x%08x [%06d.%02u0], Context ID: 0x%x user/sys timer_id=%d/%d {%08x%08x/%08x%08x}\n"
                            , abs_hi((*timerMapIter).first)
                            , abs_lo((*timerMapIter).first)
                            , abs_secs(timerMapIter->first - now)
                            , abs_msec(timerMapIter->first - now)
                            , key_uti((*timerMapIter).second)
                            , key_uti((*timerMapIter).second)
                            , key_ud2((*timerMapIter).second)
                            , (U32)(timerMapIter->first>>32)
                            , (U32)(timerMapIter->first&0xFFFFFFFF)
                            , (U32)(timerMapIter->second>>32)
                            , (U32)(timerMapIter->second&0xFFFFFFFF) ));
    }

    GnrlUserTableDef::iterator userMapIter;
    for (userMapIter  = g_user_time_table.begin();
         userMapIter != g_user_time_table.end();
         userMapIter++)
    {
        Debug(timers, printf( "USER   Timer expiration: 0x%x%08x [%06d.%02u0], Context ID: 0x%x user/sys timer_id=%d/%d {%08x%08x/%08x%08x}\n"
                            , abs_hi(userMapIter->second)
                            , abs_lo(userMapIter->second)
                            , abs_secs(userMapIter->second - now)
                            , abs_msec(userMapIter->second - now)
                            , key_uti(userMapIter->first)
                            , key_uti(userMapIter->first)
                            , key_ud2(userMapIter->first)
                            , (U32)(userMapIter->first>>32)
                            , (U32)(userMapIter->first&0xFFFFFFFF)
                            , (U32)(userMapIter->second>>32)
                            , (U32)(userMapIter->second&0xFFFFFFFF) ));
    }

} /* End of its_timer_list_timers() */


int its_timer_create (
    int                         signo,
    timer_func_sel_def         *func_sel,
    timer_func_nosel_def       *func_no_sel,
    timer_t                    *timer_id)
{
    struct sigaction     sa;
    struct sigevent      timer_event;
    timer_t              timer_id_local;
    its_timer_entry_def *timer_ent;


    Debug(timers, printf("***** timer_create called - signo: %d\n",signo));

    /* Verify that timer subsystem is initialized */
    if ((timer_info_g == NULL)   ||
       (timer_info_g->subsys_status != ITS_TIMERS_INITIALIZED))
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Create: Timer subsystem has not been initialized");
        return true;
    }
  
    /* Check arguments */
    if (((timer_info_g->env_type == ITS_TIMER_ENV_SELECT)    &&
        (func_sel == NULL))                                     ||
       ((timer_info_g->env_type == ITS_TIMER_ENV_NO_SELECT) &&
       (func_no_sel == NULL)))
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Create: Invalid handler functions");
        return true;
    }

    /* Arm the signal */
    sigemptyset (&sa.sa_mask);
      //  sa.sa_flags = SA_SIGINFO;
      //  sa.sa_flags |= SA_RESTART;
    sa.sa_flags = (SA_SIGINFO | SA_RESTART);

    if (timer_info_g->env_type == ITS_TIMER_ENV_SELECT)
        sa.sa_sigaction = its_timer_timeout;
    else
        sa.sa_sigaction = func_no_sel;

    if (sigaction (signo, &sa, NULL) < 0)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Create: Failure arming signal: %d, %s",
                signo, strerror(errno));
        return true;
    }

    /* Verify that clock exists */
    if (clock_getres (CLOCK_REALTIME, NULL) < 0)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Create: Failure getting clock resolution: %s",
                strerror(errno));
        return true;
    }

    /* Create the timer */
    timer_event.sigev_notify = SIGEV_SIGNAL;
    timer_event.sigev_signo = signo;
    timer_event.sigev_value.sival_int = 0;    /* ????? What should go here? ????? */

    if (timer_create (CLOCK_REALTIME, &timer_event, &timer_id_local) < 0)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Create: Failure creating timer entity: %s",
                strerror(errno));
        return true;
    }

    Debug(timers, printf("***** timer_create: Timer created - timer_id: %d\n"
                        ,timer_id_local) );

    /* Allocate an entry in the internal timer table */
    if ((timer_ent = its_get_timer_entry (timer_info_g)) == NULL)
    {
        /* Disarm signal - ????????????????  HOW IS THIS DONE ?????????? */


        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Create: Failure allocating internal timer table entry");
        return true;
    }

    timer_ent->timer_id = timer_id_local;
    timer_ent->signo    = signo;
    if (timer_info_g->env_type == ITS_TIMER_ENV_SELECT)
        timer_ent->func_sel = func_sel;
    else
        timer_ent->func_no_sel = func_no_sel;

    /* Create pipe, if select loop being used */
    if (timer_info_g->env_type == ITS_TIMER_ENV_SELECT)
    {
        if (pipe (timer_ent->pipe_fd) < 0)
        {
            log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                    "Timer Create: Failure creating pipe: %s",
                    strerror(errno));
            if (its_timer_delete (timer_ent->timer_id))
            {
                log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                        "Timer Create: Failure deleting timer - id: %d",
                        timer_ent->timer_id);
            }
            return true;
        }

        /* Set pipe's write fd to non-blocking */
        if (fcntl (timer_ent->pipe_fd [1], F_SETFL, O_NONBLOCK) < 0)
        {
            log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                    "Timer Create: Failure setting pipe to non-blocking: %s",
                    strerror(errno));
            if (its_timer_delete (timer_ent->timer_id))
            {
                log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                        "Timer Create: Failure deleting timer - id: %d",
                        timer_ent->timer_id);
            }
            return true;
        }
    }

    /* Done */
    *timer_id = timer_id_local;
    return false;         /* Successful */

} /* End of its_timer_create() */

int its_timer_delete (
    timer_t     timer_id)
{

    if (timer_delete (timer_id))
    {
        return errno;
    }

    /* Done */
    return false;         /* Successful */

} /* End of its_timer_delete() */


int its_timer_set_general (
    timer_t             timer_id,
    interval_def        timeout_int,
    U16                 user_data_1,
    U32                 user_data_2,
    sys_timer_tag_def  *sys_timer_tag) /* should hold value of
                                        * user_timer_id for **_cp timers */
{
    its_timer_entry_def  *timer_ent;

    Debug(timers, printf("***** Timer_Set_General: timer_id = %d, timeout_int=%d\n",
                         timer_id, timeout_int) );

    /* Find timer entry using timer id */
    if ((timer_ent = its_find_timer_by_id (timer_info_g, timer_id)) == NULL)
    {
        Debug(timers, printf("Timer_Set_General: didn't find timer_id = %d\n",timer_id) );
        return true;
    }

    /* Set the timer */
    timer_ent->_timer_spec.it_interval.tv_sec = 0;
    timer_ent->_timer_spec.it_interval.tv_nsec = 0;
    timer_ent->_timer_spec.it_value.tv_sec = timeout_int / 100;
    timer_ent->_timer_spec.it_value.tv_nsec = (timeout_int % 100) * 10000000;

    if (timer_settime (timer_id, 0, &timer_ent->_timer_spec, NULL) < 0)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Set Gen: Failure setting timer - errno: %d",
                errno);
        return true;
    }

    /* Store user data in timer entry */
    timer_ent->user_data_1 = user_data_1;
    timer_ent->user_data_2 = user_data_2;

    /* Store user data in timer entry */
    timer_ent->sys_timer_tag = *sys_timer_tag; /* s/be user_timer_id
                                                * for **_cp timers */
    /* Set sys_timer_tag - Not used on OC-SS7 - Set to zero */
    /* if (sys_timer_tag != NULL) memset (sys_timer_tag, 0, sizeof(sys_timer_tag_def)); */

    Debug(timers, its_timer_list_timers());

    /* Done */
    return false;         /* Successful */

} /* End of its_timer_set_general() */


void its_timer_timeout (
    int          signo,
    siginfo_t   *sig_info,
    void        *context)
{
    size_t                nbytes;
    U8                    write_buf [1];
    its_timer_entry_def  *timer_ent;



    Debug(timers, printf ("***** its_timer_timeout called: signo=%d\n",signo));
    Debug(timers, printf ("*****                   siginfo.cause=%d\n",sig_info->si_code));

    /* Lookup timer entry for signal number */
    if ((timer_ent = its_find_timer_by_signo (timer_info_g, signo)) == NULL)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Timeout: No timer found for signal: %d",
                signo);
        return;
    }

    /* Write dummy byte to pipe */


    Debug(timers, printf ("***** Writing to pipe for timer: %d, fd: %d\n",
                         timer_ent->timer_id, timer_ent->pipe_fd[1]));


      /*write_buf[0] = 0xEE;               ** Arbitrary value indicating timeout */
    write_buf[0] = timer_ent->pipe_fd[1]; /***** FOR TESTING ***** */
    nbytes = write (timer_ent->pipe_fd[1], write_buf, 1);
    if (nbytes == (size_t)-1)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Timeout: Error writing pipe for timer id: %d, %s",
                timer_ent->timer_id, strerror(errno));
        return;
    }

    if (nbytes != 1)
    {
        log_msg (NULL, -1, ITS_LOG_ERR, 0, NULL,
                "Timer Timeout: Bad write to pipe for timer id: %d, nbytes: %d",
                timer_ent->timer_id, nbytes);
        return;
    }

      Debug(timers, printf ("***** Writing to pipe SUCCESSFUL for timer: %d\n",timer_ent->timer_id) );

    /* Done */
    return;

} /* End of its_timer_timeout() */


// Local Variables: ***
// c-file-style: "vcid programming style" ***
// comment-start: "// "  ***
// End: ***
