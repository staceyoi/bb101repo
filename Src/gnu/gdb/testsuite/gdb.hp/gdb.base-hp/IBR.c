#include <unistd.h>
#include <stdlib.h>

extern void foo ();
extern void foo1 ();
extern void foo2 ();
extern void foo3 ();

void clean_stack ()
{
  int z[100] = {0};
}

void nomem_func ()
{
    void* p;
      p = malloc(10);
      clean_stack();
      p = malloc(10);
}

int main(int argc, char *argv[]){

    int i,j;
    int wait_here = 0;
    int nomem = 0;
    (void) alarm(60);
    if (argc > 1)
	wait_here = atoi (argv[1]);
    nomem = wait_here;
    while (wait_here);

    if (nomem == 2)
      nomem_func ();

    foo();
    foo1();
    foo2();
    foo3();
}

