#include <stdio.h>
int bar(int i, int j)
{
    return i + j;
}

int foo(int i, int j, int k)
{
    return k * bar(i, j);
}

int main()
{
    int i = 1;
    int j = 2;
    int k = 3;
    printf("foo() = %d\n", foo(i,j,k));
    return 0;
}
