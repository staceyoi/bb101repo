#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# bsp.exp - test JAGaf80512 frame command doesn't change $bsp. 

if { ![istarget "ia64*-hp-*"] } {
  verbose "testignored for non HP-IA64 targets"
  return 0
}

set testfile "bsp"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

# are we on a target board
if ![isnative] then {
    return
}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug}] != "" } {
  gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Start with a fresh gdb

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "break bar" "Breakpoint.*at.*file .*bsp.c.*" "Break at bar"
gdb_test "run" "Starting program.*bsp.*bar.*bsp.c.*" "Run to bar"

send_gdb "p/x \$bsp\n"
set bsp0 -1
gdb_expect {
    -re ".*\[0-9\]* = ($hex).*$gdb_prompt $" { set bsp0 $expect_out(1,string) }
    default     { fail "(timeout) getting bsp" }
}

send_gdb "up\n"
send_gdb "p/x \$bsp\n"
set bsp1 -1
gdb_expect {
    -re ".*\[0-9\]* = ($hex).*$gdb_prompt $" { set bsp1 $expect_out(1,string) }
    default     { fail "(timeout) getting bsp" }
}

if { "$bsp0" == "$bsp1" } {
   fail "bsp(frame0) == bsp(frame1)"
   return 0
} 

gdb_exit
