#include <stdlib.h>
#include <stdio.h>
int bar(){
    printf("Hello\n");
    abort();
    return 2;
}

int foo(){
    int * res = (int *)malloc(4);
    printf("*res %d\n", *res);
    *res = 20;
    bar();
    return 1;
}

void scramble_load_parm(char* filename, long val)
{
  long load_ptr, *load_map_ptr;

  char sys_cmd[1024];
  char* tmp_file = "./coretest.tmp";

  sprintf(sys_cmd, "elfdump -L %s | grep Load | grep map "
                   "| awk '{ print $4 '} > %s", filename, tmp_file);
  system(sys_cmd);

  FILE *fp = fopen(tmp_file, "r");
  if (!fp)
    return;

  fscanf(fp, "%lx", &load_ptr);

  fclose(fp);
 
  printf("load_ptr read is %lx\n", load_ptr);

  load_map_ptr = (long *)load_ptr;

  printf("load_map_ptr is %lx\n", load_map_ptr);

  switch (val)
  {
    case 0:
      *load_map_ptr = 0xdeadbeef;
      break;
    default:
      *load_map_ptr = *load_map_ptr + val;
      break;
  }
}

int main(int argc, char* argv[]){
    if (argc > 1)
    {
      scramble_load_parm(argv[0], atoi(argv[1]));
    }

    foo();
    return 0;
}
