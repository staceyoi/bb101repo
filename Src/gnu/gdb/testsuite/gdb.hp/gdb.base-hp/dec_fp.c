#include <stdio.h>
#include <stdlib.h>

#define ARG1 (0.0001df)

_Decimal32 dec32_val1 = 3.14159df;
_Decimal32 dec64_val1 = 3.14159dd;
_Decimal128 dec128_val1 = 3.14159dl;

volatile _Decimal32 d32;
volatile _Decimal64 d64;
volatile _Decimal128 d128;

struct dfp_struct
{
  int ivar;
  long lvar;
  float fvar;
  double dvar;
  _Decimal32 dec32;
  _Decimal64 dec64;
  _Decimal128 dec128;
} ds;

static _Decimal32
dfp_arg32 (_Decimal32 arg0, _Decimal32 arg1, _Decimal32 arg2,
         _Decimal32 arg3, _Decimal32 arg4, _Decimal32 arg5)
{
  return arg0;
}

static _Decimal64
dfp_arg64 (_Decimal64 arg0, _Decimal64 arg1, _Decimal64 arg2,
         _Decimal64 arg3, _Decimal64 arg4, _Decimal64 arg5)
{
  return arg0;
}

static _Decimal128
dfp_arg128 (_Decimal128 arg0, _Decimal128 arg1, _Decimal128 arg2,
         _Decimal128 arg3, _Decimal128 arg4, _Decimal128 arg5)
{
  return arg0;
}

int
dfp_args (_Decimal32 arg0, _Decimal64 arg1, _Decimal128 arg2)
{
  return ((arg0 - dec32_val1) < ARG1
	  && (arg0 - dec32_val1) > -ARG1
	  && (arg1 - dec64_val1) < ARG1
	  && (arg1 - dec64_val1) > -ARG1
	  && (arg2 - dec128_val1) < ARG1
	  && (arg2 - dec128_val1) > -ARG1);
}

void pass_arg (_Decimal32 arg0[2], _Decimal64 arg1[2], _Decimal128 arg2[2])
{
  _Decimal32 a1;
  _Decimal64 a2;
  _Decimal128 a3;

  a1 = arg0[0] + arg0[1];
  a2 = arg1[0] + arg1[1];
  a3 = arg2[0] + arg2[1];

}

int main()
{
  int ret;
  _Decimal32 ad32, arrd32[2] = {1.2df, 2.2df};
  _Decimal64 ad64, arrd64[2] = {1.2dd, 2.2dd};
  _Decimal128 ad128, arrd128[2] = {1.2dl, 2.2dl};

  d32 = 1.2345df;		/* Initialize d32.  */
  d64 = 1.2345dd;		/* Initialize d64.  */
  d128 = 1.2345dl;		/* Initialize d128.  */

  /* Functions with decimal floating point as parameter and return value. */
  ad32 = dfp_arg32 (0.1df, 1.0df, 2.0df, 3.0df, 4.0df, 5.0df);
  ad64 = dfp_arg64 (0.1dd, 1.0dd, 2.0dd, 3.0dd, 4.0dd, 5.0dd);
  ad128 = dfp_arg128 (0.1dl, 1.0dl, 2.0dl, 3.0dl, 4.0dl, 5.0dl);

  pass_arg (arrd32, arrd64, arrd128);

  ds.ivar = 1;
  ds.lvar = 2;
  ds.fvar = 3.1;
  ds.dvar = 4.2;
  ds.dec32 = 1.2345df;
  ds.dec64 = 1.2345dd;
  ds.dec128 = 1.2345dl;
  ret = dfp_args (ds.dec32, ds.dec64, ds.dec128);

  return 0;	/* Exit point.  */
}
