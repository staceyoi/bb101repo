#include <ia64/sys/inline.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

double gen_fp_nat()
{
   int nat_exp;
   __fpreg fp_nat;

   // Set the exponent for FP NatVal
   nat_exp = 0x1FFFE;
   fp_nat = _Asm_setf(_FR_EXP,nat_exp);
   // Clear the significant for FP NatVal
   fp_nat = _Asm_fmerge(_FM_SE,fp_nat,0.0);

   // At this point we have FP NatVal
   return fp_nat;
}

int gen_int_nat()
{
   // Generate a FP NatVal first.
   double fp_nat = gen_fp_nat();
   // Convert FP NatVal to integer NaT, while a cast
   // is sufficient, using a getf directly will require
   // one fewer instruction as we don't really need to
   // convert to fixed-point representation first.
   int nat = _Asm_getf(_FR_EXP,fp_nat);
   return nat;
}

long nat() { return (long) gen_int_nat(); }

long arr[1000];

int main(int argc, char **argv) {

  long n = nat();
  double d = n;

  printf("n is %d\n", n);

  if (!n)
   printf("n is false\n");
  else
   printf("n is true\n");

  if (n)
   printf("n is true\n");
  else
   printf("n is false\n");

  if (d > 0.0)
   printf("d is greater than 0.0\n");
  else
   printf("d is less than or equal to 0.0\n");

  if (d <= 0.0)
   printf("d is less than or equal to 0.0\n");
  else
   printf("d is greater than 0.0\n");

  printf("now trying to store n to mem\n");
  arr[101] = n;
  printf("arr[argc] = %d\n", arr[argc]);

  return 0;
}

