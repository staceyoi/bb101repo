#include <unistd.h>
#include <dl.h>
#ifdef PROTOTYPES
int main (void)
#else
main ()
#endif
{
  int  pid;
  void (*solib_foo)();
  shl_t handle;
  pid = vfork ();
  if (pid == 0) {
    execlp ("fork_parent1", "fork_parent1", (char *)0);
  }
  else {
    handle = shl_load ("vfork_share1.sl",BIND_DEFERRED,0);
     shl_findsym (&handle,"sh_foo",TYPE_PROCEDURE,(long *) &solib_foo);
     (*solib_foo)();
  }
}
