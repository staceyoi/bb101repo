#include <sys/stat.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include<dl.h>

int main() {
   int fork_me=1;
   void (*solib_foo)();
  shl_t handle;

   fork_me = fork();
   if ( fork_me == 0 ) {
      printf ("I am child" );
      exit(0);
   } else {
      printf ("I am parent \n" );
     handle = shl_load ("vfork_share1.sl",BIND_DEFERRED,0);
     shl_findsym (&handle,"sh_foo",TYPE_PROCEDURE,(long *) &solib_foo);
     (*solib_foo)();

   }
}
