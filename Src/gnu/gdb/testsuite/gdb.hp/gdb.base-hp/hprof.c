/* testhprof.c : Testing the hierarchical profiler has some basic
   problems in that the samples may fall at unpredicatbale places
   each time the program is profiled. To avoid the non-determinism,
   we generate the profiling signal directly in the program here
   instead of using the setitimer capability.
*/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void the_lazy_dog ()
{
    kill (getpid (), SIGVTALRM);
}

void jumped_over ()
{
	the_lazy_dog ();
}

void the_quick_brown_fox ()
{
	jumped_over();
}

main()
{
        the_quick_brown_fox ();
}
