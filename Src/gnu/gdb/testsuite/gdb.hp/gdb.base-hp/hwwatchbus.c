/* This program raises a SIGBUS signal on HP-UX when the
   pointer "bogus_p" is dereferenced.
   */
int p = 0;
int *bogus_p = &p;

int main()
{
  bogus_p = (int *)(((long) &p) | 3);
  *bogus_p = 0xdeadbeef;
}
