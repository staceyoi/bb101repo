#include <stdlib.h>

#ifdef __HP_cc
#define inline __inline
#endif

#ifdef INLINE_LEAF
#pragma INLINE leaf
#pragma NO_INLINE bar
#else
#pragma NO_INLINE leaf, bar
#endif

enum signal_t {NONE = 0, SEGV = 1, BUS = 2};  /* what kind of signal to create */
enum signal_t my_signal;

int path;  /* which path to take through program => which traceback to abort with */
int cur_path = 0;  /* counter controlling path to take through program */

int *ptr;  /* pointer to cause SIGSEGV or SIGBUS errors */

int leaf ( int x )
{
  if (cur_path++ == path) {
     if (my_signal != NONE)
        *ptr = 12;
     }
  return x;
}

inline int tum( int x )
{
  if (x < 10) {
#pragma ESTIMATED_FREQUENCY 0.001
    x = x < 5 ? x +3 : x+ 13;
    return leaf(x* x);
  }
  else  {
    x = x > 9 ? x -5 : x- 10;
    return leaf(x+2);
  }
}

inline int foo( int x )
{
  int y = 0;

  y = x-2;
  if (x < 30) {
    y = x+5;
    x = x > 7 ? x -3 : x- 13;
    return leaf(y + x* 6);
  }
  else  {
#pragma ESTIMATED_FREQUENCY 0.001
    x = x <= 10 ? x +21 : x+ 16;
    return leaf(x+18);
  }
}

// non inlined function
int bar(int x)
{
    return leaf(x%2);
}

main( int argc, char **argv )
{

  int b = atoi("29");
  int a = atoi("8");

   if ((argc != 3) || (0 == argv )) {
      printf("FAIL - wrong number of arguments.\n");
      printf("usage : %s <path_num> <signal>.\n", argv[0]);
      exit (123);
    } else {
      path = atoi( argv[1] );
      my_signal = (enum signal_t) atoi( argv[2] );
    }

  if (my_signal == SEGV)
    ptr = (int *) 16;
  else if (my_signal == BUS)
    ptr = (int *) (((unsigned long) &a) | 1);
  else 
    ptr = &a;

  /* a += foo(tum(lum(a))); */

  b = foo( b );
  a += bar( a );
  b = foo( b );
  b += tum( b );
  a += tum( a );

  return ( b - a );
}
