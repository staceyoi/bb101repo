#include <dlfcn.h>
#include <stdio.h>
int main () {
  void *dl = dlopen("mixed_mode_pa_lib32.sl", RTLD_LAZY);
  int (*f)() = (int (*)())dlsym(dl,"foo");
  int i = (*f)();
  printf("foo returned %d\n", i);
  return 0;
}
