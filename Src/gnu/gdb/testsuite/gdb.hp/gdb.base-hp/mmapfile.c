#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
 
char *r;

int main()
{
    int  devzfd;
    int  fd, fd2;

    char *p, *q, *s, *t, *u;
    devzfd = open("/dev/zero", O_RDONLY);
    if ( devzfd == -1 )
    {
        perror("open");
        return -1;
    }
    fd = open("/tmp/mmapfile_test1", O_RDWR);
    if ( fd == -1 )
    {
        perror("open");
        return -1;
    }
    p = (char *)mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert( p != (void *)-1 );
 
    q = (char *)mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE, devzfd, 0);
    assert( q != (void *)-1 );
 
    r = (char *)mmap(NULL, 4096, PROT_READ, MAP_PRIVATE, fd, 0);
    assert( r != (void *)-1 );
 
    s = (char *)mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
    assert( s != (void *)-1 );
 
    t = (char *)mmap(NULL, 4096, PROT_READ, MAP_SHARED, fd, 0);
    assert( t != (void *)-1 );
 
    fd2 = open("/tmp/mmapfile_test2", O_RDWR);
    if ( fd2 == -1 )
    {
        perror("open");
        return -1;
    }
    u = (char *)mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
    if (u == MAP_FAILED)
      printf("PROT_READ|PROT_WRITE, MAP_SHARED failed, errno = %d\n", errno);
    else
      assert( u != (void *)-1 );
 
    printf("p %p q %p r %p s %p t %p u %p\n", p, q, r, s, t, u);
    abort();
    return 0;
}


