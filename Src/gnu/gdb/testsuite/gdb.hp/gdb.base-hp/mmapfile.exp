# Copyright (C) 1992, 1994, 1995, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This test-suite checks "mmapfile" command.

if $tracelevel {
    strace $tracelevel
}

if { ![istarget "ia64*-hp-*"] } then {
    return
}

set srcfile "mmapfile.c"
set binfile "${objdir}/${subdir}/mmapfile_exe"

if { [gdb_compile "${srcdir}/${subdir}/${srcfile} " ${binfile} executable {debug}] != "" } {
        gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Run the test case, which dumps core
remote_exec build "/bin/rm -rf core"
remote_exec build "cp ${srcdir}/${subdir}/mmapfile_test1 /tmp/"
remote_exec build "cp ${srcdir}/${subdir}/mmapfile_test2 /tmp/"
remote_exec build "chmod 777 /tmp/mmapfile_test1"
remote_exec build "chmod 777 /tmp/mmapfile_test2"
remote_exec build "${binfile}"

# Start with a fresh gdb
gdb_start
gdb_reinitialize_dir ${srcdir}/${subdir}/
gdb_load ${binfile}

gdb_test "core-file core" ".*terminated with signal 6.*" "core file loaded"

gdb_test "frame 3" ".*in main.*57.*abort.*" "main frame"

# Check the mmapfile help output
gdb_test "help mmapfile" ".*mmapfile <FILENAME> <MAP-ADDRESS> <FILE-OFFSET> <LENGTH>.*MMAP_PRIVATE with RO.*MMAP_SHARED.*" "mmapfile help"

# Check the following -
#  - r is not showing data it points to, because it's not in core
#  - add r with mmapfile with "'mmapfile.c'::r" syntax
#  - test that r is showing data
gdb_test "p r" ".*Address.*out of bounds.*" "r out of bounds"

gdb_test "mmapfile /tmp/mmapfile_test1 \"\'${srcfile}\'::r\" 0 0xe1" "" "Added mmapfile_test1 for r"

gdb_test "p r" ".*include.*foo.*" "r finds data"

# Check the following -
#  - t is not showing data it points to, because it's not in core
#  - add t with mmapfile with main::t syntax
#  - test that t is showing data
gdb_test "p t" ".*Address.*out of bounds.*" "t out of bounds"

gdb_test "mmapfile /tmp/mmapfile_test1 main::t 8 0xe1" "" "Added mmapfile_test1 for t"

gdb_test "p t" ".*stdlib.*foo.*" "t finds data"

# Check the following -
#  - u is not showing data it points to, because it's not in core
#  - add u with mmapfile
#  - test that u is showing data
gdb_test "p u" ".*Address.*out of bounds.*" "u out of bounds"

gdb_test "mmapfile /tmp/mmapfile_test2 u 0 0x100" "" "Added mmapfile_test2 for u"

gdb_test "p u" ".*uc_access.*SIZE.*" "u finds data"

# Check that info target shows the mmapfile segments
send_gdb "info target\n"
gdb_expect {
  -re ".*Added with mmapfile.*/tmp/mmapfile_test1.*Added with mmapfile.*/tmp/mmapfile_test1.*Added with mmapfile.*/tmp/mmapfile_test2.*$gdb_prompt" {
     pass "info target for core file showing mmapfile segments"
  }
  -re ".*$gdb_prompt $" { fail "wrong output for info target" }
  timeout { fail "wrong output for info target" }
}

# Test error checking

# No file found
gdb_test "mmapfile /no/such/file 0x1000 0 0x10" ".*mmapfile = /no/such/file.*can not be opened.*" "No file check"

# Overlaping sections
gdb_test "mmapfile /tmp/mmapfile_test1 s 0 0x10" ".*address overlaps with.*can not be added.*" "Overlaping section 1"

gdb_test "mmapfile /tmp/mmapfile_test1 \(s-0xe0\) 0 0xe1" ".*address overlaps with.*can not be added.*" "Overlaping section2"

gdb_test "mmapfile /tmp/mmapfile_test1 \(s-0xf0\) 0 0x10e1" ".*address overlaps with.*can not be added.*" "Overlaping section3"

# Incomplete usage
gdb_test "mmapfile /tmp/mmapfile_test1" ".*Usage:.*mmapfile <FILENAME> <MAP-ADDRESS> <FILE-OFFSET> <LENGTH>" "Incomplete usage 1"

gdb_test "mmapfile /tmp/mmapfile_test1 0x2000" ".*Usage:.*mmapfile <FILENAME> <MAP-ADDRESS> <FILE-OFFSET> <LENGTH>" "Incomplete usage 2"

gdb_test "mmapfile /tmp/mmapfile_test1 0x2000 5" ".*Usage:.*mmapfile <FILENAME> <MAP-ADDRESS> <FILE-OFFSET> <LENGTH>" "Incomplete usage 3"

gdb_exit

remote_exec build "/bin/rm -rf core"
remote_exec build "/bin/rm -rf /tmp/mmapfile_test1 /tmp/mmapfile_test2"
