# This test script tests:
# * if the IPF gdb can handle a 32 bit mixed mode binary. It should not
#   throw the "File is in different format" error when a PA32 lib load
#   is encountered. (JAGag21712)
# * if the IPF gdb can load symbols from PA32 SOM libs by testing the
#   'info func <pa32_symbol>' command. (JAGag21714)
# * if the IPF gdb can disassemble code from both PA32 and IPF load modules.
#   (JAGag21715)

# use this to debug:
#log_user 1

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

if ![istarget "ia64*-hp-*"] {
    verbose "Mixed mode testing is only for ia64."
    return 0
}

# Avoid checks for +DD64 runs. Those get tested with pixie.exp
if { "$multipass_name" < "9" } {
    continue
}

set testfile "mixed_mode32"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}
set dldlib "dld32.so"
set dldfile ${objdir}/${subdir}/${dldlib}
set pa_lib "mixed_mode_pa_lib32.sl"
set additional_flags "additional_flags=-Ae -Wl,+interp/usr/lib/hpux32/uld.so:${dldfile}"

# Use the dld in the source directory. Note: This will have to be
# modified once pixie stabilizes.
exec /bin/cp -f ${srcdir}/${subdir}/${dldlib} ${dldfile}

# Use the libaries_dbg32.so in the current directory. Note: This will have
# to be modified once pixie stabilizes.

set env(PIXIE_LIBARIES) "${srcdir}/${subdir}/libaries_dbg32.so"
exec /bin/cp -f ${srcdir}/${subdir}/${pa_lib} ${objdir}
exec chmod +x ${objdir}/${pa_lib}
exec chmod +x ${dldfile}


if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable [list debug "${additional_flags}" ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "break 6\n"
gdb_expect {
    -re "Breakpoint.*mixed_mode32.c.*6.*$gdb_prompt $" { 
        pass "breakpoint at line 6" }
    default { fail "breakpoint at line 6" }
}

# The first test is here. Check if we can continue beyond
# the load of the PA32 lib.

send_gdb "run\n"
gdb_expect {
    -re "Breakpoint.*main.*mixed_mode32.c.*$gdb_prompt $" { 
        pass "run to line 6" }
    default { fail "Did not run to line 6." }
}

# Check if the PA symbols are loaded

send_gdb "info func foo\n"
gdb_expect {
    -re "Non-debugging.*0x2000.*foo.*$gdb_prompt $" {
        pass "info func foo" }
    default { fail "info func foo" }
}

# Check if we can print the disassembly of both the PA function,
# "foo" and the IA function "main". Printing the disassembly of
# the IA function is a check to see if the original disassembler
# function (print_insn_ia64) gets restored. Additionally test if
# we have the addil, ldsid and mtsp instructions appear in that
# order.

send_gdb "disass foo\n"
gdb_expect {
   -re "Dump.*assembler.*foo.*addil.*ldsid.*mtsp.*$gdb_prompt $" {
        pass "PA function disassembly dump." }
   default { fail "PA function disassembly dump." }
}

send_gdb "disass main\n"
gdb_expect {
   -re "Dump.*assembler.*main.*$gdb_prompt $" {
        pass "main function disassembly dump." }
   default { fail "main function disassembly dump." }
}

gdb_exit

return 0
