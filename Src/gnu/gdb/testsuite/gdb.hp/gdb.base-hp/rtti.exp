#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
# test for gdb detecting cold function names.
if $tracelevel then {
    strace $tracelevel
}

set testfile "rtti"
set srcfile  ${testfile}.cc
set binfile  ${objdir}/${subdir}/${testfile}

gdb_exit

if {![istarget "ia64*-hp-*"] } {
  gdb_start
  gdb_reinitialize_dir $srcdir/$subdir
  gdb_test "help info rtti" ".*Undefined.*command.*rtti.*" "help info rtti"
  gdb_test "info rtti" ".*Undefined.*command.*rtti.*" "info rtti"
  gdb_exit
  return
}

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_test "help info rtti" ".*Print RTTI.*Usage.*polymorphic.*" "help info rtti"
gdb_test "info rtti" ".*Argument.*polymorphic.*" "info rtti"
gdb_test "info rtti 0x0" ".*No RTTI.*invalid.*polymorphic.*" "info rtti 0x0"
gdb_test "info rtti obj" ".*No symbol.*file.*" "info rtti obj"
gdb_exit

if [get_compiler_info ${binfile} "c++"] { return -1 }

if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug c++}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}
gdb_test "break main" ".*Breakpoint.*file.*" "break main"
gdb_test "run" ".*Breakpoint.*main.*init().*" "run"
gdb_test "info rtti obj" ".*No symbol.*obj.*context.*" "info rtti obj"
gdb_test "next" ".*e.w.*7.*" "next"
gdb_test "next" ".*e.vb.*11.*" "next"
gdb_test "next" ".*test_calls.*" "next"
gdb_test "step" ".*TEST.*pAe.*20.*" "step"
gdb_test "info rtti pAe" ".*RTTI.*class.*E.*" "info rtti pAe"
gdb_test "info rtti pAa" ".*RTTI.*class.*A.*" "info rtti pAe"
gdb_test "info rtti objb" ".*Value.*converted.*integer.*" "info rtti objb"
gdb_test "info rtti &objb" ".*RTTI.*class.*B.*" "info rtti &objb"

#Generate core file
catch "exec /usr/bin/rm -fr core.*"
set pid -1
send_gdb "dumpcore\n"
gdb_expect {
  -re "(.*Dumping core to the core file core.)(${decimal})(.*$gdb_prompt $)"
    {
      set pid $expect_out(2,string);
      pass "dumpcore into core.pid"
    }
  -re ".*$gdb_prompt $" { fail "dumpcore" }
  timeout { fail "dumpcore timeout" }
}

#test core file
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}
gdb_test "core core.${pid}" ".*Core.*generated.*TEST.*pAe.*" "core core.${pid}"
gdb_test "info rtti pAe" ".*RTTI.*class.*E.*" "info rtti pAe"
gdb_test "info rtti pAa" ".*RTTI.*class.*A.*" "info rtti pAe"
gdb_test "info rtti objb" ".*Value.*converted.*integer.*" "info rtti objb"
gdb_test "info rtti &objb" ".*RTTI.*class.*B.*" "info rtti &objb"
gdb_exit
catch "exec /usr/bin/rm -fr core.*"


if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {nodebug c++}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}
gdb_test "break main" ".*Breakpoint.*file.*" "break main"
gdb_test "run" ".*Breakpoint.*main.*init().*" "run"
gdb_test "info rtti obj" ".*No symbol.*obj.*context.*" "info rtti obj"
gdb_test "next" ".*e.w.*7.*" "next"
gdb_test "next" ".*e.vb.*11.*" "next"
gdb_test "next" ".*test_calls.*" "next"
gdb_test "step" ".*TEST.*pAe.*20.*" "step"
gdb_test "info rtti pAe" ".*RTTI.*class.*E.*" "info rtti pAe"
gdb_test "info rtti pAa" ".*RTTI.*class.*A.*" "info rtti pAe"
gdb_test "info rtti objb" ".*warning.*No RTTI.*polymorphic.*" "info rtti objb"
gdb_test "info rtti &objb" ".*RTTI.*class.*B.*" "info rtti &objb"

#Generate core file
catch "exec /usr/bin/rm -fr core.*"
set pid -1
send_gdb "dumpcore\n"
gdb_expect {
  -re "(.*Dumping core to the core file core.)(${decimal})(.*$gdb_prompt $)"
    {
      set pid $expect_out(2,string);
      pass "dumpcore into core.pid"
    }
  -re ".*$gdb_prompt $" { fail "dumpcore" }
  timeout { fail "dumpcore timeout" }
}

#test core file 
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}
gdb_test "core core.${pid}" ".*Core.*generated.*TEST.*pAe.*" "core core.${pid}"
gdb_test "info rtti pAe" ".*RTTI.*class.*E.*" "info rtti pAe"
gdb_test "info rtti pAa" ".*RTTI.*class.*A.*" "info rtti pAe"
gdb_test "info rtti objb" ".*warning.*No RTTI.*polymorphic.*" "info rtti objb"
gdb_test "info rtti &objb" ".*RTTI.*class.*B.*" "info rtti &objb"
gdb_exit
catch "exec /usr/bin/rm -fr core.*"
