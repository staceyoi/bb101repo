#include <stdlib.h>
#include <strings.h>

main(int argc, char** argv, char** envp)
{
  for (int j=0; envp[j]; j++)
    {
      if (strncmp(envp[j], "SENTINEL_DEBUGGER", 17) == 0)
        printf("%s\n", envp[j]);
    }

  for (int i=0; i<argc; i++)
    printf("%s ", argv[i]);

  printf("\n");
}
