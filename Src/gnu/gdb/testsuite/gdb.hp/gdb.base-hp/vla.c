extern int n;

int B[5] = {100, 200, 300, 400, 500};// OK - file scope but not VM type

void foo(int m, int C[m])   // OK - function prototype scope VM type
{
  typedef int VLA[m][m];    // OK - block scope VM type
  int D[m];                 // OK - block scope with VM type
  int (*q)[m];              // OK - block scope with VM type
  static int (*s)[m] = &B;  // OK - static specifier allowed in VM
                            // type since s is pointer to array
  int i = s[0],j;

  int vla2[m+1][m+1];
  
  for (i=0; i<m; i++)
    D[i] = C[i];

  for (i=0; i<m+1; i++)
    for (j=0; j<m+1; j++)
      vla2[i][j] = i*10+j;
 
  q = &vla2[2];

  m = 0;
}

int  n = 5;

int main()
{
  int M[n];
  int i;

  for (i=0; i<n; i++)
    M[i] = i+1;
  
  foo(n, M);
 
  i = 0;
}
