#   Copyright (C) 2001, Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
	strace $tracelevel
	}

set prms_id 0
set bug_id 0

if { [skip_hp_tests] } then { continue }

set testfile "vla"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile}] {
    return -1
}

if {!$hp_cc_compiler} {
    verbose "variable length array test only supported by hp C compilers."
    return 0
}

# build the test case
if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable "debug {additional_flags=-AC99}"] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Start with a fresh gdb

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if ![runto_main] then {
  fail "can't run to main"
  return 0
}

gdb_test "until 38"  "main.*at .*$srcfile:.*foo\\(n, M\\)\\;"

# JAGaf51501 - dwarf for C variable array given as pointer when it should be 
#	       reference
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "print M" "= \\(int \\(&\\)\\\[5\\\]\\) @$hex: \{1, 2, 3, 4, 5\}"

# JAGaf51510  VLA declaration causes bad line table
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "break foo" "Breakpoint 2 at $hex: file .*$srcfile, line (1|7)(0|.).*"

gdb_test "continue" "Continuing.*Breakpoint 2, foo \\(m=5, C=$hex\\).*" "continue to foo"

gdb_test "break 25" "Breakpoint 3 at $hex: file .*$srcfile, line 25."

gdb_test "continue" "Continuing.*Breakpoint 3, foo.*m = 0;" "continue to last statement in foo"

#test 1-dimensional array
gdb_test "set var D\[1\] = 6" ""
# JAGaf51501 - dwarf for C variable array given as pointer when it should be 
#	       reference
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "print D\[1\]" " = 6"
# JAGaf51501 - dwarf for C variable array given as pointer when it should be 
#	       reference
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "print D" "= \\(int \\(&\\)\\\[5\\\]\\) @$hex: \{1, 6, 3, 4, 5\}"

#test 2-dimensional array
# JAGaf51501 - dwarf for C variable array given as pointer when it should be 
#	       reference
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "print vla2"  "= \\(int \\(&\\)\\\[6\\\]\\\[6\\\]\\) @$hex: \{\{0, 1, 2, 3, 4, 5\}, \{10, 11, 12, 13, 14, 15\}, \{20, 21, 22, 23, 24, 25\}, \{30, 31, 32, 33, 34, 35\}, \{40, 41, 42, 43, 44, 45\}, \{50, 51, 52, 53, 54, 55\}\}"
# JAGaf51501 - dwarf for C variable array given as pointer when it should be 
#	       reference
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "print vla2\[3\]" "= \{30, 31, 32, 33, 34, 35\}"
# JAGaf51501 - dwarf for C variable array given as pointer when it should be 
#	       reference
#setup_xfail ia64*-hp-*
#Now it was passing.Dated:27/01/2009
gdb_test "print vla2\[m-1\]\[m\]" "= 45"

#test pointer types
gdb_test "ptype s" "type = int \\(\\\*\\)\\\[5\\\]"
gdb_test "p *s" "= \{100, 200, 300, 400, 500\}"
gdb_test "ptype q" "type = int \\(\\\*\\)\\\[5\\\]"
gdb_test "p *q" "= \{20, 21, 22, 23, 24\}"

gdb_exit
