# Test case for JAGaf33423, "apparent WDB problem with reference (&) 
# params in C++".  Modified for fix to JAGaf18670, "Debugger call
# to class by value function doesn't work" - we now accept the case when the
# pointer/reference's target matches the type of the struct/class passed on
# the command line.

if ![istarget "*-hp*-hpux*"] {
    verbose "Test ignored for all but HP-UX targets."
    return
}

# This test is presently only valid on HP-UX.
#
setup_xfail "*-*-*"
clear_xfail "*-hp*-*hpux*"


set testfile "JAGaf33423"
set srcfile ${testfile}.cc
set rundir ${objdir}/${subdir}
set full_srcfile ${srcdir}/${subdir}/${srcfile}
set binfile  ${rundir}/${testfile}

if { [gdb_compile ${full_srcfile} ${binfile} executable {debug c++}] != "" } {
  gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# save old timeout
set oldtimeout $timeout
# If timeout is not set, set one
if { ![info exists timeout] } {
    set timeout 2
}

# Start with a fresh gdb
gdb_exit
gdb_start
gdb_load ${binfile}

# Check if a command line call with a reference argument is detected and a warning issued.
#
gdb_test "b 34" "Breakpoint 1.*$hex.*$srcfile.*" "Set break at line #35"
gdb_test "r" "Starting.*work list.*Breakpoint 1.*main.*$srcfile.*dumpworklist.*" "Hit break at line #35"

gdb_test "call dumpworklist(l)" "work list contains:  2 3" "Detect reference argument in a command line call"

gdb_exit
