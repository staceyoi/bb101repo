#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Test "add-symbol-file <file>". We should be able to load a
# stripped library with no debug info and later load symbols
# from an unstripped version with a different name to enable full 
# debugging capabilities.

# For now it'll only work on PA64 and IPF
if {![istarget "hppa2.0w*-*-hpux*"] && ![istarget "ia64*-hp-*"]} {
    return 0
}

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

set testfile "add_sym_file"
set srcfile ${testfile}.c
set srcfile1 ${testfile}1.c
set binfile ${objdir}/${subdir}/${testfile}
set testfile1 ${objdir}/${subdir}/${testfile}1.o
set libfile1 ${objdir}/${subdir}/${testfile}1.sl

# Create and source the file that provides information about the compiler
if [get_compiler_info ${binfile}] { return -1 }
set PIC "+z"
if {$gcc_compiled || $gxx_compiled} then { set PIC "-fpic"  }

# Build the shared libraries this test case needs.
if { [gdb_compile "${srcdir}/${subdir}/${testfile}1.c" "${testfile1}" object "debug additional_flags=$PIC"] != ""} {
    perror "Couldn't compile ${testfile}1.c"
    return -1
}
remote_exec build "$LD -b ${testfile1} -o ${libfile1}"

# Make a copy of the debuggable library
exec /bin/cp -f ${libfile1} ${libfile1}.debug

# Strip the original library
exec /bin/strip ${libfile1}

# Build the test case
if { [gdb_compile "${srcdir}/${subdir}/${srcfile} ${libfile1}" "${binfile}" executable {debug}] != "" } {
    perror "Couldn't build ${binfile}"
    return -1
}

# Start with a fresh gdb
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "b main" \
    "Breakpoint $decimal at $hex: file .*$srcfile, line $decimal." 

# continue to first breakpoint
gdb_test "run" \
    ".*Breakpoint $decimal, main.*" \
    "run to main"

# Make sure that printing mydata as a struct fails
gdb_test "print mydata" \
    ".*No debug info available.*\\$$decimal = 1.*" \
    "print mydata - no debug info"

# Now we need to load the debug library at the same location
# stripped library is loaded
set lib_tstart 0
set lib_dstart 0
send_gdb "info shared\n"


gdb_expect {
    -re ".*add_sym_file1\.sl\r\n($hex\r\n| )($hex) ($hex) ($hex).*$gdb_prompt $" {
      set lib_tstart $expect_out(2,string)
      set lib_dstart $expect_out(4,string)
      pass "info shared $lib_tstart $lib_dstart"
    }
    -re ".*$gdb_prompt $" { fail "info shared" }
    timeout { fail "(timeout) loaded info shared" }
  }


send_gdb "add-symbol-file ${libfile1}.debug $lib_tstart -s .data $lib_dstart\n"
gdb_expect {
    -re ".*add symbol table from file.*text_addr = $lib_tstart.*data_addr = $lib_dstart.*$" { 
        pass "add-symbol-file 1" }
    default   { fail "add-symbol-file 1" }
    timeout { fail "(timeout) add-symbol-file 1" }
}

send_gdb "y\n"
gdb_expect {
    -re ".*Reading symbols from.*done.*$gdb_prompt $" { 
        pass "add-symbol-file 2" }
    default   { fail "add-symbol-file 2" }
    timeout { fail "(timeout) add-symbol-file 2" }
}

# Make sure that printing mydata work now
gdb_test "print mydata" \
    ".*{bar = 1, arr = {2, 3, 4}}" \
    "print mydata - with debug info"

gdb_exit
return 0
