#
# Test for JAGag22025: Tests if we are able to disassemble a 32 bit
# shared library function on IPF. This script tests if we can disassemble
# 'printf'.
#

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

if ![istarget "ia64*-hp-*"] {
    verbose "This test is valid only for ia64."
    return 0
}

set prms_id 0
set bug_id 0

# are we on a target board
if { "$multipass_name" < "9" } {
    continue
}

set testfile "disass_shared_lib_func"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "break main\n"
gdb_expect {
    -re "Breakpoint.*disass_shared_lib_func.c.*4.*$gdb_prompt $" { 
        pass "breakpoint at main" }
    default { fail "breakpoint at main" }
}

send_gdb "run\n"
gdb_expect {
    -re "Breakpoint.*main.*disass_shared_lib_func.c.*$gdb_prompt $" { 
        pass "run to main" }
    default { fail "Did not run to main." }
}

# The 'alloc' below is to check if we are getting the right disassembled instrcutions.
send_gdb "disass printf\n"
gdb_expect {
   -re "Dump.*assembler.*printf.*alloc.*$gdb_prompt $" {
        pass "printf disasembly dump." }
   default { fail "printf disassembly dump." }
}

