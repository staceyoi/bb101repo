#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

if $tracelevel then {
    strace $tracelevel
}

if ![istarget "ia64*-hp*-hpux*"] {
    verbose "Test ignored for all but HP-UX IPF based targets."
    return
}


# Test case to check if the parameters and the locals get displayed
# correctly with gcc 4.2, 4.1.2 and 4.1.1 compiled binaries. The binaries
# are compiled with gcc 4.2 unless specifically mentioned.
# Different executables are used to test:
#  * char: 32 bit (test the locals and params of the main module routines)
#  * char.64: 64 bit (test the locals and params of the main module routines)
#  * gcc_test_main: 32 bit (test the locals and params of a shared library)
#  * gcc_test_main.4.1.1: 32 bit compiled with gcc 4.1.1 (test the locals
#                         and params of a shared library)
#  * gcc_test_main.4.1.2: 32 bit compiled with gcc 4.1.2 (test the locals
#                         and params of a shared library)
#  * hello: 64 bit (test the locals and params of a shared library)
#  * hello.4.1.2 : 64 bit compiled with gcc 4.1.2 (test the locals and params of a shared library)
# With char and char.64, different parameters of different data types are
# passed to check if we display them all correctly.


set testfile "char"
set testfile64 "char.64"


gdb_start
gdb_test "file $srcdir/$subdir/${testfile}" ".*"
send_gdb "b 18\n"
gdb_expect {
   -re ".*Breakpoint 1 at.*line 18.*" {
       pass "Breakpoint set" }
   default { fail "Breakpoint set" }
   timeout { fail "(timeout) Breakpoint set" }
}

send_gdb "run\n"
gdb_expect {
   -re ".*" {
       pass "run to testing" }
   default { fail "run to testing" }
   timeout { fail "(timeout) run to testing" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*a = 5.*b = 101 'e'.*c = 15.*f = 5.4.*d = 5.73232.*dd = 236.532.*sptr =.*short int.*0x.*" {
       pass "i locals" }
   default { fail "i locals" }
   timeout { fail "(timeout) i locals" }
}

send_gdb "s\n"
gdb_expect {
   -re ".*testing.*iTest=0x.*acp=5, b_ptr=0x.*e.*bcp=101 'e'.*c_ptr=0x.*ccp=15.*f_ptr=0x.*fcp=5.4.*d_ptr=0x.*dcp=5.73232.*dd_ptr=0x.*ddcp=236.532.*s_ptr=0x.*sptr=0x.*" {
       pass "arguments display" }
   default { fail "arguments display" }
   timeout { fail "(timeout) arguments display" }
}

send_gdb "s\n"
gdb_expect {
   -re ".*" {
       pass "step" }
   default { fail "step" }
   timeout { fail "(timeout) step" }
}

send_gdb "s\n"
gdb_expect {
   -re ".*" {
       pass "step" }
   default { fail "step" }
   timeout { fail "(timeout) step" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*local1 = 1234.*c = 103 'g'.*s = 16.*" {
       pass "i locals" }
   default { fail "i locals" }
   timeout { fail "(timeout) i locals" }
}

send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*a = 5.*b = 101 'e'.*c = 15.*f = 5.4.*d = 5.73232.*dd = 236.532.*sptr =.*short int.*0x.*" {
       pass "i locals of frame 1" }
   default { fail "i locals of frame 1" }
   timeout { fail "(timeout) i locals of frame 1" }
}

gdb_exit

gdb_start
gdb_test "file $srcdir/$subdir/${testfile64}" ".*"
send_gdb "b 18\n"
gdb_expect {
   -re ".*Breakpoint .*at.*line 18.*" {
       pass  "Breakpoint set" }
   default { fail "Breakpoint set" }
   timeout { fail "(timeout) Breakpoint set" }
}

send_gdb "run\n"
gdb_expect {
   -re ".*" {
       pass "run to testing" }
   default { fail "run to testing" }
   timeout { fail "(timeout) run to testing" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*a = 5.*b = 101 'e'.*c = 15.*f = 5.4.*d = 5.73232.*dd = 236.532.*sptr =.*short int.*0x.*" {
       pass "i locals" }
   default { fail "i locals" }
   timeout { fail "(timeout) i locals" }
}

send_gdb "s\n"
gdb_expect {
   -re ".*testing.*iTest=0x.*acp=5, b_ptr=0x.*e.*bcp=101 'e'.*c_ptr=0x.*ccp=15.*f_ptr=0x.*fcp=5.4.*d_ptr=0x.*dcp=5.73232.*dd_ptr=0x.*ddcp=236.532.*s_ptr=0x.*sptr=0x.*" {
       pass "arguments display" }
   default { fail "arguments display" }
   timeout { fail "(timeout) arguments display" }
}

send_gdb "s\n"
gdb_expect {
   -re ".*" {
       pass "step" }
   default { fail "step" }
   timeout { fail "(timeout) step" }
}

send_gdb "s\n"
gdb_expect {
   -re ".*" {
       pass "step" }
   default { fail "step" }
   timeout { fail "(timeout) step" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*local1 = 1234.*c = 103 'g'.*s = 16.*" {
       pass "i locals" }
   default { fail "i locals" }
   timeout { fail "(timeout) i locals" }
}


send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*a = 5.*b = 101 'e'.*c = 15.*f = 5.4.*d = 5.73232.*dd = 236.532.*sptr =.*short int.*0x.*" {
       pass "i locals of frame 1" }
   default { fail "i locals of frame 1" }
   timeout { fail "(timeout) i locals of frame 1" }
}

gdb_exit
set testfile "gcc_test_main"
set env(SHLIB_PATH) "$srcdir/$subdir"

gdb_start
gdb_test "file $srcdir/$subdir/${testfile}" ".*"

send_gdb "b testFunction\n"
gdb_expect {
   -re ".*" {
       pass "breakpoint placed at testFunction" }
   default { fail "breakpoint placed at testFunction" }
   timeout { fail "(timeout) breakpoint placed at testFunction" }
}

send_gdb "run\n"
gdb_expect {
   -re "Starting.*testFunction.*iArgs=0x.*" {
       pass "run to testFunction" }
   default { fail "run to testFunction" }
   timeout { fail "(timeout) run to testFunction" }
}


send_gdb "i args\n"
gdb_expect {
   -re ".*iArgs =.*const int.*0x*" {
       pass "i args" }
   default { fail "i args" }
   timeout { fail "(timeout) i args" }
}


send_gdb "p *iArgs\n"
gdb_expect {
   -re ".*5.*" {
       pass "print iArgs" }
   default { fail "print iArgs" }
   timeout { fail "(timeout) print iArgs" }
}

send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*gcc_test_main.cc.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*testFunction.*const.*0x.*handle.*void.*0x.*a = 5.*" {
       pass "print locals in frame 1" }
   default { fail "print locals in frame 1" }
   timeout { fail "(timeout) print locals in frame 1" }
}

gdb_exit

set testfile "gcc_test_main.4.1.1"
set env(SHLIB_PATH) "$srcdir/$subdir"

gdb_start
gdb_test "file $srcdir/$subdir/${testfile}" ".*"

send_gdb "b testFunction\n"
gdb_expect {
   -re ".*" {
       pass "breakpoint placed at testFunction" }
   default { fail "breakpoint placed at testFunction" }
   timeout { fail "(timeout) breakpoint placed at testFunction" }
}

send_gdb "run\n"
gdb_expect {
   -re "Starting.*testFunction.*iArgs=0x.*" {
       pass "run to testFunction" }
   default { fail "run to testFunction" }
   timeout { fail "(timeout) run to testFunction" }
}


send_gdb "i args\n"
gdb_expect {
   -re ".*iArgs =.*const int.*0x*" {
       pass "i args" }
   default { fail "i args" }
   timeout { fail "(timeout) i args" }
}


send_gdb "p *iArgs\n"
gdb_expect {
   -re ".*5.*" {
       pass "print iArgs" }
   default { fail "print iArgs" }
   timeout { fail "(timeout) print iArgs" }
}

send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*gcc_test_main.4.1.1.cc.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*testFunction.*const.*0x.*handle.*void.*0x.*a = 5.*" {
       pass "print locals in frame 1" }
   default { fail "print locals in frame 1" }
   timeout { fail "(timeout) print locals in frame 1" }
}
gdb_exit

set testfile "gcc_test_main.4.1.2"
set env(SHLIB_PATH) "$srcdir/$subdir"

gdb_start
gdb_test "file $srcdir/$subdir/${testfile}" ".*"

send_gdb "b testFunction\n"
gdb_expect {
   -re ".*" {
       pass "breakpoint placed at testFunction" }
   default { fail "breakpoint placed at testFunction" }
   timeout { fail "(timeout) breakpoint placed at testFunction" }
}

send_gdb "run\n"
gdb_expect {
   -re "Starting.*testFunction.*iArgs=0x.*" {
       pass "run to testFunction" }
   default { fail "run to testFunction" }
   timeout { fail "(timeout) run to testFunction" }
}


send_gdb "i args\n"
gdb_expect {
   -re ".*iArgs =.*const int.*0x*" {
       pass "i args" }
   default { fail "i args" }
   timeout { fail "(timeout) i args" }
}


send_gdb "p *iArgs\n"
gdb_expect {
   -re ".*5.*" {
       pass "print iArgs" }
   default { fail "print iArgs" }
   timeout { fail "(timeout) print iArgs" }
}

send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*gcc_test_main.4.1.2.cc.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*testFunction.*const.*0x.*handle.*void.*0x.*a = 5.*" {
       pass "print locals in frame 1" }
   default { fail "print locals in frame 1" }
   timeout { fail "(timeout) print locals in frame 1" }
}
gdb_exit

set testfile "hello"
set env(SHLIB_PATH) "$srcdir/$subdir:$srcdir/$subdir/hpux64"

gdb_start
gdb_test "file $srcdir/$subdir/${testfile}" ".*"

send_gdb "b countIt\n"
gdb_expect {
   -re ".*" {
       pass "breakpoint placed at countIt" }
   default { fail "breakpoint placed at countIt" }
   timeout { fail "(timeout) breakpoint placed at countIt" }
}

send_gdb "run\n"
gdb_expect {
   -re "Starting.*countIt.*string=0x.*hello.*depth=0.*" {
       pass "run to countIt" }
   default { fail "run to countIt" }
   timeout { fail "(timeout) run to countIt" }
}


send_gdb "i args\n"
gdb_expect {
   -re ".*string = 0x.*hello.*depth = 0.*" {
       pass "i args" }
   default { fail "i args" }
   timeout { fail "(timeout) i args" }
}


send_gdb "p string\n"
gdb_expect {
   -re ".*hello.*" {
       pass "print string" }
   default { fail "print string" }
   timeout { fail "(timeout) print string" }
}

send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*h = 0x.*hello.*w =.*world.*" {
       pass "print locals in frame 1" }
   default { fail "print locals in frame 1" }
   timeout { fail "(timeout) print locals in frame 1" }
}

send_gdb "bt\n"
gdb_expect {
   -re ".*countIt.*string.*hello.*depth=0.*countIt.cpp.*main.*hello.cpp.*" {
       pass "bt gcc frames" }
   default { fail "bt gcc frames" }
   timeout { fail "(timeout) bt gcc frames" }
}

gdb_exit

set testfile "hello.4.1.2"
set env(SHLIB_PATH) "$srcdir/$subdir:$srcdir/$subdir/hpux64"

gdb_start
gdb_test "file $srcdir/$subdir/${testfile}" ".*"

send_gdb "b countIt\n"
gdb_expect {
   -re ".*" {
       pass "breakpoint placed at countIt" }
   default { fail "breakpoint placed at countIt" }
   timeout { fail "(timeout) breakpoint placed at countIt" }
}

send_gdb "run\n"
gdb_expect {
   -re "Starting.*countIt.*string=0x.*hello.*depth=0.*" {
       pass "run to countIt" }
   default { fail "run to countIt" }
   timeout { fail "(timeout) run to countIt" }
}


send_gdb "i args\n"
gdb_expect {
   -re ".*string = 0x.*hello.*depth = 0.*" {
       pass "i args" }
   default { fail "i args" }
   timeout { fail "(timeout) i args" }
}


send_gdb "p string\n"
gdb_expect {
   -re ".*hello.*" {
       pass "print string" }
   default { fail "print string" }
   timeout { fail "(timeout) print string" }
}

send_gdb "f 1\n"
gdb_expect {
   -re ".*#1.*main.*" {
       pass "setting frame 1" }
   default { fail "setting frame 1" }
   timeout { fail "(timeout) setting frame 1" }
}

send_gdb "i locals\n"
gdb_expect {
   -re ".*h = 0x.*hello.*w =.*world.*" {
       pass "print locals in frame 1" }
   default { fail "print locals in frame 1" }
   timeout { fail "(timeout) print locals in frame 1" }
}

send_gdb "bt\n"
gdb_expect {
   -re ".*countIt.*string.*hello.*depth=0.*countIt.cpp.*main.*hello.cpp.*" {
       pass "bt gcc frames" }
   default { fail "bt gcc frames" }
   timeout { fail "(timeout) bt gcc frames" }
}
gdb_exit
return 0


