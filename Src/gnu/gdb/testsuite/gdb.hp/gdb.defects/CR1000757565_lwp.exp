#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
# 
# This is used to test gdb behaviour with applications that use only
# lwp threads. (Refer to QXCR1000757565 for more details).

if $tracelevel then {
    strace $tracelevel
}

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] then {
    return
}

if [istarget "hppa*-*-hpux*"] then {
   return
}

if { ![istarget "ia64*-hp-*"] } {
  return
}

set testfile "suspend0"
set binfile  ${objdir}/${subdir}/${testfile}

# Start with a fresh gdb
catch "exec cp -f ${srcdir}/${subdir}/${testfile} ${objdir}/${subdir}/"
catch "exec chmod +x ${binfile}"

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "b _lwp_create" "Breakpoint 1.*"  "Breakpoint"
gdb_test "r" ".*Breakpoint 1.*in _lwp_create.*" "_lwp_create brkpt reached"
gdb_test "i thr" ".*1 system thread.*" "info threads"
gdb_test "c" ".*Breakpoint 1.*in _lwp_create.*" "_lwp_create brkpt reached"
gdb_test "i thr" ".*2 system thread.*1 system thread.*" "info threads"
gdb_test "c" ".*Breakpoint 1.*in _lwp_create.*" "_lwp_create brkpt reached"
gdb_test "i thr" ".*3 system thread.*2 system thread.*1 system thread.*" "info threads"
gdb_test "bt" ".*_lwp_create.*test_lwp_create.*main.*" "bt"
gdb_test "c" ".*program finishes.*exited.*" "Ran to completion"

gdb_exit 
return 0

