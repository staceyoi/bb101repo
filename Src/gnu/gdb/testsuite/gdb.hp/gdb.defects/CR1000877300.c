#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
int main () {
  void *dl = dlopen("libc.so.1", RTLD_LAZY);
  if (dl != NULL) {
    // find a known function in the library
    void (*func)() = (void (*)())dlsym(dl, "malloc");
    int *p = (int *) malloc (100);
    struct eplt_type {
       unsigned long long proc_addr;
       unsigned long long ltptr;
    } *eplt = (struct splt_type *) func;
    printf ("proc_addr = %llx\nltptr = %llx\n",
                     eplt->proc_addr, eplt->ltptr);
    // compare GP with eplt->proc_addr to verify GP
   free (p);
   abort();
   }
}

