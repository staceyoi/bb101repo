
int array[10];

void set()
{
   int i;
   for (i = 0; i < 10; ++i)
      array[i] = i*i;
}

void dump()
{
   int i;
   for (i = 0; i < 10; ++i)
      printf("%d\n", array[i]);
}

int main()
{
   set();
   dump();
   return 0;
}
