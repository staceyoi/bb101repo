#include <stdlib.h>
#include <string.h>
#include <stdio.h>
void foo(void *p) {
   free((char *)p);
}
void bar(void *p) {
   foo(p);
}
int main() {
   char *p;
   p = (char*)malloc(1000);
   fprintf(stderr, "Allocated %p\n", p);
   fprintf(stderr, "Before destroying before and after\n");
   memset(p, 0x55, 1000+32);
   memset(p-32, 0x55, 32);
   fprintf(stderr, "After destroying before and after, before free\n");
   bar(p);
   return 0;
}

