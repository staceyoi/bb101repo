
struct foo {
   int   foo_int;

   int	 foo_func(int foo_arg);
};

int foo::foo_func (int foo_arg)
{
  return foo_arg;
}

int main()
{
  int i;
  int j;
  struct foo foo_instance;

  j = 0;
  foo_instance.foo_int = 0;
  i = (0 && foo_instance.foo_func(1)  ) || 0;

  return i;
}



