#include <list>
#include <stdio.h>
namespace std {} using namespace std;

class Q {
 public:
  Q(int i) : id(i) { } 
  int id;
};

static void dumpworklist(list<Q *> &l) 
{
  list<Q *>::iterator it = l.begin();
  list<Q *>::iterator en = l.end();
  
  fprintf(stderr, "work list contains: ");
  for (; it != en; ++it) {
    Q *n = (*it);
    fprintf(stderr, " %d", n->id);
  }
  fprintf(stderr, "\n");
}

int main()
{
  Q a(1), b(2), c(3);
  list<Q *> l;
  l.push_front(&c);
  l.push_front(&b);
  l.push_front(&a);

  dumpworklist(l);
  l.pop_front();
  dumpworklist(l);

  return 0;
}
