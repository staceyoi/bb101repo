
#include <sys/mman.h>  /* for mmap */



/* exec_mmap.c - see do_exec_mmap - program to be built exec_magic.
 * allocates lareg mmap region and then makes a core file.
 */

int main()
{
  void * region;
  int * ptr;
  int * value_ptr;

  region = mmap (0, 0x70000000, PROT_READ  | PROT_WRITE, 
		 MAP_ANONYMOUS | MAP_PRIVATE , 0, 0);
  /* make core file */
  value_ptr = (int *) ((char*) region + 0x70000000 - 100);
  *value_ptr = 7469;
  ptr = 0;
  *ptr = 1;

  return !(region != (void*) -1);
}
