/* For JAGaf55033.exp
*/

#include <stdio.h>
#include <unistd.h>

int dont_exit = 1;

int main(int argc, char *argv[])
{
	(void) alarm(600);
	while (dont_exit);
	return 0;
}
