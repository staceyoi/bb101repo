#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# cllgdb@india.hp.com

if $tracelevel then {
	strace $tracelevel
	}

if { ![istarget "hppa*-*-hpux*"] && ![istarget "ia64*-hp-*"] } {
    return 0
}

set testfile "JAGaf55033"
set srcfile  ${testfile}.c
set binfile  ${objdir}/${subdir}/${testfile}
set saved_binfile ${binfile}

set cleanupfile ${objdir}/${subdir}/${testfile}.awk

remote_exec build "rm -f ${binfile}"

# Clean out any old files from past runs.
remote_exec build "${cleanupfile}"

# build the test cases
if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Because we can't attach over nfs, copy binfile to /tmp/${binfile}.${pid}
# and replace binfile with a symbolic link
  set pid [pid]

#######################################################################
#   a.out invoked with abs path and length < 63 chars
#######################################################################

  exec /bin/cp -f ${binfile} /tmp/JAGaf55033.${pid}
  set binfile /tmp/JAGaf55033.${pid}

if [get_compiler_info ${binfile}] {
    return -1
}
  exec /bin/rm -f ${binfile}.ci

# Start the program running and then wait for a bit, to be sure
# that it can be attached to.

# Start it with some arguments with a '/' character in it.
set testpid [eval exec $binfile abc/def/ghi &]
exec sleep 2

# Test that we can simply startup with a "-pid=process id" command line arg
# and able to attach gdb to this running process

gdb_exit

eval "spawn $GDB -nw $GDBFLAGS -pid=$testpid"
expect {
    -re ".*Attaching to.*process.*Reading symbols from.*$binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "gdbargs: -pid=pid , cmd line length < 63 chars"
    }
    -re "Attaching to.*process $decimal.*No such process.*" {
	fail "gdbargs: -pid=pid (no such process), cmdline length < 63 chars"
    }
    -re ".*$gdb_prompt $" { fail "gdbargs: -pid=pid, cmdline length < 63 chars" }
    timeout { fail "(timeout) starting with -pid , cmdline length < 63 chars" }
}

close;
gdb_exit

# Test the startup with both an executable file and -pid argument.

eval "spawn $GDB -nw $GDBFLAGS $binfile -pid=$testpid"
expect {
    -re "Attaching to program.*$binfile, process $testpid.*$gdb_prompt $" {
	pass "gdbargs: execfile -pid=process id, cmdline length < 63 chars"
    }
    -re ".*$binfile: No such file or directory.*Attaching to process $decimal.*"    {
	fail "gdbargs: execfile -pid=process id (no such file or directory), cmdline length < 63 chars"
    }
    -re ".*$gdb_prompt $" { fail "gdbargs: execfile -pid=process id, cmdline length < 63 chars" }
    timeout { fail "(timeout) starting with -pid, cmdline length < 63 chars" }
}

close;

#Now restart Normally
gdb_start

# normal attach
send_gdb "attach $testpid\n"
gdb_expect {
    -re "Attaching to process.*Reading symbols from $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "attach to a running process, cmdline length < 63 chars"
    }
    -re "Attaching to program: $binfile, process $testpid.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
        pass "attach to a running process, cmdline length < 63 chars"
    }
    -re "Attaching to process $decimal.*No such process.*" {
	fail "attach fails (no such process), cmdline length < 63 chars"
    }
    -re "$gdb_prompt $" {fail "attach to a running process, cmdline < 63 chars "}
    timeout         {fail "(timeout) attach to a running process, cmdline < 63 chars"}
}
gdb_test "print dont_exit=0" ".*= 0.*" "Stop the while loop"
gdb_test "continue" "Continuing.*Program exited.*" "Exit"
gdb_exit

# Cleanup the child process, in case any tests failed so we won't have 
# unnecessary process running
  remote_exec build "kill -9 ${testpid}"

########################################################################
#   a.out invoked with abs path, length > 63 chars.
#######################################################################
  set long_binfile "/tmp/JAGaf55033_01234678901234567890123467890123456789012346789012345.${testpid}"
  if {[catch "exec rm -f ${long_binfile}"]} {
    # probable permissions problem - move it aside instead
    catch "exec /usr/bin/date -u +%S%M" time_str
    catch "exec mv ${long_binfile} ${long_binfile}.${time_str}"
  }
  exec /bin/cp -f ${binfile} ${long_binfile}
  set binfile ${long_binfile}

  set testpid [eval exec $binfile abc/def/ghi &]
  exec sleep 2
 
  eval "spawn $GDB -nw $GDBFLAGS -pid=$testpid"
  expect {
    -re ".*Attaching to process.*Reading symbols from $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
      pass "gdbargs: -pid=pid, pathlength > 63 chars"
    }
    -re ".*Could not find executable.\r\nTry invoking.*$gdb_prompt $" {
      pass "Warning on PA when attaching to a process with pathname > 63 chars"
    }
    -re "Attaching to.*process $decimal.*No such process.*" {
      fail "gdbargs: -pid=pid (no such process), pathlength > 63 chars"
    }
    -re ".*$gdb_prompt$" { fail "gdbargs: -pid=pid, pathlength > 63 chars"}
    timeout { fail "(timeout) starting with -pid "}
  }
  
  close;
  gdb_exit

  # Test the startup with both an executable file and -pid argument.
  eval "spawn $GDB -nw $GDBFLAGS $binfile -pid=$testpid"
  expect {
    -re ".*Attaching to program: $binfile, process $testpid.*$gdb_prompt $" {
      pass "gdbargs: execfile -pid=pid, pathlength > 63 chars"
    }
    -re ".*$gdb_prompt $" { fail "gdbargs: execfile -pid=pid, pathlength > 63 chars" }
    timeout { fail "(timeout) starting with execfile & pid with pathlength > 63 chars" }
  }
  close;
  gdb_exit
  
  #Start a fresh gdb to attach
  gdb_start
  send_gdb "attach $testpid\n"
  gdb_expect {
    -re "Could not find executable.\r\nTry invoking.*$gdb_prompt $" {
      pass "Warning on PA when attaching to a process with pathname > 63 chars"
    }
    -re "Attaching to process.*Reading symbols from $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "attach to a running process, pathlength > 63 chars"
    }
    -re "Attaching to program.*$binfile, process $testpid.*$gdb_prompt $" {
        pass "attach to a running process, pathlength > 63 chars"
    }
    -re "Attaching to process $decimal.*No such process.*" {
	fail "attach fails (no such process), pathlength > 63 chars"
    }
    -re "$gdb_prompt $" {fail "attach to a running process, pathlength > 63 chars "}
    timeout {fail "(timeout) attach to a running process, pathlength > 63 chars"}
  }

  gdb_exit
  
  remote_exec build "kill -9 ${testpid}"

  set new_binfile /tmp/JAGaf55033.${testpid}
  if {[catch "exec rm -f ${new_binfile}"]} {
    # probable permissions problem - move it aside instead
    catch "exec /usr/bin/date -u +%S%M" time_str
    catch "exec mv ${new_binfile} ${new_binfile}.${time_str}"
  }
  exec /bin/cp -f $binfile ${new_binfile}
  remote_exec build "rm -f ${binfile}"
  set binfile ${new_binfile}
  set testpid [eval exec $binfile abc/def/ghi 0123456789 0123456789 0123456789 0123456789 0123456789 &]
  exec sleep 2
  
  eval "spawn $GDB -nw $GDBFLAGS -pid=$testpid"
  expect {
    -re ".*Attaching to.*process.*Reading symbols from $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "gdbargs: -pid=pid, pathlength + args > 63 chars"
    }
    -re ".*Attaching to.*program: $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "gdbargs: -pid=pid, pathlength + args > 63 chars"
    }
    -re "Attaching to.*process $decimal.*No such process.*" {
	fail "gdbargs: -pid=pid (no such process), pathlength + args > 63 chars"
    }
    -re ".*$gdb_prompt $" { fail "gdbargs: -pid=pid, pathlength + args > 63 chars" }
    timeout { fail "(timeout) starting with -pid, pathlength + args > 63 chars " }
  }
  
  close;
  gdb_exit
  
  eval "spawn $GDB -nw $GDBFLAGS $binfile -pid=$testpid"
  expect {
    -re ".*Attaching to program: $binfile, process $testpid.*$gdb_prompt $" {
      pass "gdbargs: execfile -pid=pid, pathlength + args > 63 chars"
    }
    -re ".*$gdb_prompt $" { fail "gdbargs: execfile -pid=pid, pathlength + args > 63 chars" }
    timeout { fail "(timeout) starting with execfile & pid with pathlength + args > 63 chars" }
  }
  
  close;
  gdb_exit
  
  #Start a fresh gdb to attach
  gdb_start
  send_gdb "attach $testpid\n"
  gdb_expect {
    -re "Reading symbols from $binfile.*Attaching to program: $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "attach to a running procrss, pathlength + args > 63 chars"
    }
    -re "Attaching to process.*Reading symbols from $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
        pass "attach to a running process, pathlength + args > 63 chars"
    }
    -re ".*Attaching to.*program: $binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "attach to a running process, pathlength + args > 63 chars"
    }
    -re "Attaching to process $decimal.*No such process.*" {
	fail "attach fails (no such process), pathlength + args > 63 chars"
    }
    -re "$gdb_prompt $" {fail "attach to a running process, pathlength + args > 63 chars "}
    timeout {fail "(timeout) attach to a running process, pathlength + args > 63 chars"}
  }
  gdb_exit
  remote_exec build "kill -9 ${testpid}"


###########################################################################################
# a.out invoked with relative path, and pathlength < 63 chars
###########################################################################################

  exec /bin/cp -f ${binfile} /tmp/JAGaf55033.${testpid}
  remote_exec build "rm -f ${binfile}"
  set binfile ./JAGaf55033.${testpid}
  set testdir [ eval exec /usr/bin/pwd ]
  cd /tmp
  set testpid [ eval exec $binfile abc/def &]
  cd $testdir
  exec sleep 2

  gdb_start
  send_gdb "attach $testpid \n"
  gdb_expect {
    -re ".*$binfile: No such file or directory.*$gdb_prompt $" {
      pass "correct warning when a.out is invoked with relative path and gdb is in a diff dir"
    }
    -re ".*$gdb_prompt $" { fail "No such file or directory."}
    timeout { fail "(timeout) No such file or directory."}
  }
  gdb_test "detach" ""

  if { [istarget "ia64*-hp-*"] } {
  set env(PWD) "/tmp"
  cd /tmp
  set testpid [ eval exec $binfile abc/def &]
  cd $testdir
  exec sleep 2

  send_gdb "attach $testpid\n"
  gdb_expect {
    -re ".*Attaching to process.*Reading symbols from /tmp/${binfile}.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
      pass "attach from the same dir, when a.out invoked with relative path 1"
    }
    -re ".*$gdb_prompt $" { fail "attach fails from the same dir, when a.out is invoked with relative path 1" }
    timeout {  fail "(timeout) attach fails from the same dir, when a.out is invoked with relative path 1" }
  }
  gdb_test "detach" ""
  set env(PWD) "$testdir"
  }

  gdb_exit
  gdb_start
  gdb_test "cd /tmp" "Working directory /tmp." ""
  send_gdb "attach $testpid\n"
  gdb_expect {
    -re ".*Attaching to process.*Reading symbols from /tmp/${binfile}.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
      pass "attach from the same dir, when a.out invoked with relative path 2"
    }
    -re ".*Attaching to.*program: /tmp/$binfile.*main.*at.*JAGaf55033.c:$decimal.*$gdb_prompt $" {
	pass "attach to a running process, pathlength + args > 63 chars"
    }
    -re ".*$gdb_prompt $" { fail "attach fails from the same dir, when a.out is invoked with relative path 2" }
    timeout {  fail "(timeout) attach fails from the same dir, when a.out is invoked with relative path 2" }
  }
  
  gdb_exit
  remote_exec build "kill -9 ${testpid}"
#  remote_exec build "rm -f /tmp/${binfile}"
  
# Cleanup the files placed in /tmp and the symlinks
#  remote_exec build "rm -f /tmp/JAGaf55033.${pid} /tmp/JAGaf55033.${pid}.ci "

return 0
