#include <stdio.h>
class foo {
 public:
  int x;
  void print();
};

typedef foo *fooP;
typedef foo fooP1;

void foo::print() {
  printf("%d\n", x);
}

main() {
  foo *f;
  fooP g;
  fooP1 g1;
  f = new foo;
  g = new foo;
  f->x=42;
  f->print();
  g->x = 84;
  g->print();
  g1.print();
  return 0;
}

