#include <stdio.h>

// Define base class A
/*

(gdb) p &AA_obj.A1::ai 
$7 = (int *) 0x7ffff6a0

(gdb) x /15x 0x7ffff6a0
0x200000007ffff6a0:	0x40010098	0x00000004	0x400100b0	0x00000003
0x200000007ffff6b0:	0x00000002	0x00000005	0x00000006	0x00000007
0x200000007ffff6c0:	0x00000008	0x00000009	0x0000000a	0x0000000b
0x200000007ffff6d0:	0x0000000c	0x0000000d	0x00000003
(gdb) 

 A1  -------  AA
    |vtabptr|
     -------
    | a1i=4 |
 A2  -------
    |vtabptr|
     -------
    | a2i=3 |
     -------
    | aai=2 |
 A   ------- 
    | ai=5  |
     -------
    | ab=6  |
     -------
    | ac=7  |
     -------
      etc.  

*/
class A {
  public:
     int ai;
     int ab;
     int ac;
     int ad;
     int ae;
     int af;
     int ag;
     int ah;
     int aj;
     A ()
     {
       ai = 5;
       ab = 6;
       ac = 7;
       ad = 8;
       ae = 9;
       af = 10;
       ag = 11;
       ah = 12;
       aj = 13;
     }
};

// Define class A1 which inherits A (virtual)

class A1 :public virtual A {
  public:
     int a1i;
     A1 () :A ()
     {
       a1i = 4;
     }
};

// Define class A2 which inherits A (virtual).

class A2 :public virtual A {
  public:
     int a2i;
     A2 () :A ()
     {
       a2i = 3;
     }
};

// Define derived class AA which inherits A1 and A2 and thus
// inherits A twice.

class AA : public A1, public A2 {
  public:
     int aai;
     AA () :A1 (), A2 ()
     {
       aai = 2;
     }
};


main ()
{
   // Create objects of derived classes C,A1 and AA

   AA AA_obj;
   A A_obj;
   AA *AA_ptr;
   A *A_ptr;
   AA_ptr = &AA_obj;
   A_ptr = &A_obj; 

     printf("AA_obj.AA::aai = %d\n",AA_obj.AA::aai);
     printf("AA_obj.A2::a2i = %d\n",AA_obj.A2::a2i);
     printf("AA_obj.A1::a1i = %d\n",AA_obj.A1::a1i);
     printf("AA_obj.A1::ai = %d\n",AA_obj.A1::ai);
     printf("AA_obj.A1::ab = %d\n",AA_obj.A1::ab);
     printf("AA_obj.A1::ac = %d\n",AA_obj.A1::ac);
     printf("AA_obj.A1::ad = %d\n",AA_obj.A1::ad);

     printf("AA_obj.A1::ae = %d\n",AA_obj.A1::ae);
     printf("AA_obj.A1::af = %d\n",AA_obj.A1::af);
     printf("AA_obj.A1::ag = %d\n",AA_obj.A1::ag);
     printf("AA_obj.A1::ah = %d\n",AA_obj.A1::ah);
     printf("AA_obj.A1::aj = %d\n",AA_obj.A1::aj);
     printf("AA_obj.A1::aj = %d\n",AA_obj.aj);


   return 0;
}
