#include <stdio.h>

enum SchedulerPhase { PreRalloc, PostRalloc, CycleCount };


class Dag {

public:
  SchedulerPhase phase()		{ return thePhase; }
  Dag(SchedulerPhase, int);
  void theEnd();
private: 
  SchedulerPhase  thePhase;
  int silly; 

}; 

Dag::Dag(SchedulerPhase in1, int in2)
{
  thePhase=in1;
  silly=in2;
}

void Dag::theEnd()
{
   silly += silly;
}

int main()
{
  Dag *DagPtr;
  DagPtr=new Dag(PostRalloc, 9);
  DagPtr=new Dag(PreRalloc, 5);
  DagPtr=new Dag(CycleCount, 3);
  DagPtr->theEnd();


}
