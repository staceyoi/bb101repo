#include <stdio.h>
#include <malloc.h>

static char array[100];

/* 
 * Testing corruption for strcpy etc... 
 * 
 */
#define TINY 10
#define BIG 50

/* dummy func to ease debugging of test case */ 
void set_brkhere()
{}

void main()
{
   char *ptr, *ptr1, *ptr2, *ptr3, *ptr4, *ptr5, *null_ptr;

   ptr = malloc(TINY); 
   ptr1 = malloc(TINY+TINY);
   ptr2 = malloc(BIG);
   ptr3 = malloc(4194304);
   char local_array[2];
   ptr4 = malloc(1);  
   ptr5 = malloc(5);


   /* testing memcpy */
   strcpy(ptr1, "Helsinki");
   strcpy(ptr2, "New York City");

   /* ok */
   printf("OK memcpy_1\n");
   memcpy(ptr, ptr1, TINY);
   printf("OK memcpy_2\n");
   memcpy(ptr+3, ptr1, TINY-5);

   /* bad */
   printf("BAD memcpy_3\n");
   memcpy(ptr+5, ptr1, TINY);
   printf("BAD memcpy_4\n");
   memcpy(ptr, ptr2, BIG);
   printf("BAD memcpy_5\n");
   memcpy(ptr-1, ptr1, TINY);


   set_brkhere();
 
   strcpy(ptr, "Geneva");
   printf("OK memset_1\n");
   memset (ptr, 'H', 3); 
   printf("OK memset_2\n");
   memset (ptr, 'H', TINY); 
   /* not really ok but unable to detect this */
   printf("OK memset_3\n");
   memset (array, 'H', BIG);

   /* bad */   
   printf("BAD memset_3\n");
   memset (ptr, 'H', TINY+TINY); 
   printf("BAD memset_4\n");
   memset (ptr+1, 'H', TINY);
   printf("BAD memset_5\n");
   memset (ptr-1, 'H', TINY);

   set_brkhere();


   strcpy(ptr, "Geneva");
   strcpy(ptr1, "Helsinki");

   printf("OK memmove_1\n");
   memmove (ptr, ptr1, TINY);
   printf("BAD memmove_2\n");
   memmove (ptr+7, ptr1, TINY);
   printf("BAD memmove_3\n");
   memmove (ptr, ptr1, TINY+TINY);
   printf("BAD memmove_4\n");
   memmove (ptr-1, ptr1, TINY);

   set_brkhere();


   strcpy(ptr, "Geneva");
   strcpy(ptr1, "Helsinki");

   printf("OK bzero_1\n");
   bzero(ptr, TINY);
   printf("BAD bzero_2\n");
   bzero(ptr, TINY+TINY);
   printf("BAD bzero_3\n");
   bzero(ptr+7, TINY);
   printf("BAD bzero_4\n");
   bzero(ptr-1, TINY);

   set_brkhere();

   strcpy(ptr, "Geneva");
   strcpy(ptr1, "Helsinki");

   printf("OK bcopy_1\n");
   bcopy(ptr1, ptr, TINY);
   printf("BAD bcopy_2\n");
   bcopy(ptr1, ptr, TINY+TINY);
   printf("BAD bcopy_3\n");
   bcopy(ptr1, ptr+7, TINY);
   printf("BAD bcopy_4\n");
   bcopy(ptr1, ptr-3, TINY);


   set_brkhere();

   strcpy(ptr, "Geneva");
   strcpy(ptr1, "Helsinki");
   strcpy(ptr2, "New York City");   

   printf("OK strcpy_1\n");
   strcpy (ptr, ptr1);

   printf("OK strcpy_2\n");
   strcpy(ptr3+4194000, "still fits");

   /* can't detect this - the way we construct stack_base and stack_end
    * in infrtc.c is not sufficient to detect this.  
    */
   printf("OK strcpy_3\n");
   strcpy(local_array, "hi trang");

   /* can't detect this either - the way we construct shlib_info[i].data_start
    * & shlib_info[i].data_end in batchrtc mode in infrtc.c is not 
    * sufficient to detect this.  
    */
   printf("OK strcpy_4\n");
   strcpy(array, "hi trang");

   printf("BAD strcpy_5\n");
   strcpy (ptr, ptr2);
   printf("BAD strcpy_6\n");
   strcpy (ptr+3, ptr1);
   printf("BAD strcpy_7\n");
   strcpy (ptr-1, ptr1);

   printf("BAD strcpy_8\n");
   strcpy(ptr2-1, "hi trangster");


   printf("BAD strcpy_9\n");
   strcpy(ptr3+4194303, "something long");

   printf("BAD strcpy_10\n");
   strcpy(ptr4, "hi");


   set_brkhere();

   strcpy(ptr, "Geneva");
   strcpy(ptr1, "Helsinki");
   strcpy(ptr2, "New York City");   



   printf("OK strncpy_1\n");
   strncpy (ptr, ptr1, 5);
   printf("OK strncpy_2\n");
   strncpy (ptr, ptr2, TINY-5);
   printf("BAD strncpy_3\n");
   strncpy (ptr+3, ptr1, TINY*2);
   printf("BAD strncpy_4\n");
   strncpy (ptr-3, ptr1, TINY);

}










