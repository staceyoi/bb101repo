#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
# 
# This test case test the separation of string related calls (strcpy...)
# and the rest of RTC intercepted function.
# create a .c file which redefine at least one of the string call such
# as strcpy.  run gdb with turning on set heap-check.  Expect not to
# see any warning.  run gdb with set heap-check string on.  Expect to
# see a warning.

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] {
    return
}

set testfile JAGaf74522b
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set objfile  ${objdir}/${subdir}/${testfile}.o
set binfile  ${objdir}/${subdir}/${testfile}


if { [istarget "ia64*-hp-*"] && "${IS_ILP32}" == "FALSE" } {
  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  set librtc "${objdir}/../librtc64.sl"
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  set librtc "${objdir}/../librtc.sl"
}

set env(GDB_SERVER) "${objdir}/../gdb"

set foo [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug}]
if { "${foo}" != "" } {
    perror "Couldn't compile ${testfile}.c"
    perror "Output is '${foo}'"
    return -1
}


# Start with a fresh gdb

# part 1, expect no warning
gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "set heap-check on" ""  ""
gdb_test "b main" "Breakpoint 1.*"  "Breakpoint"

send_gdb "r\n"
gdb_expect {
 -re ".*warning: Your program seems to override the C.*" { fail "incorrect warning" }
 -re ".*$gdb_prompt $"       { pass "run" }

}

# currenlty  the warning is not given since custom allocators are supported.
# part 2, expect a warning 
gdb_exit 
gdb_start 
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# either run gdb -leaks or do set heap-check on and then 
# set heap-check string on
gdb_test "set heap-check on" ""  ""
gdb_test "set heap-check string on" ""  ""
gdb_test "b main" "Breakpoint 1.*"  "Breakpoint 1"

send_gdb "r\n"
gdb_expect {
 -re ".*warning: Your program seems to override the default C library memory functions.*warning: To support memory checking capabilities all memory functions will be routed to default C library.*$gdb_prompt $" {
	pass "Warning for custom allocator" }
 -re ".*$gdb_prompt $"       { fail "Warning for custom allocator failed." }

}

return 0


