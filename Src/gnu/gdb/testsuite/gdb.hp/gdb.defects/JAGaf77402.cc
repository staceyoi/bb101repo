#include <stdio.h>

class BasicBlock {
 int a,b,c;

public:
 BasicBlock(int, int, int);
 int label();
 void prt();  

}; 

int BasicBlock::label()
{
  return a;
}

BasicBlock::BasicBlock(int x, int y, int z)
{
   a=x;
   b=y;
   c=z;
}

void some(BasicBlock &bb1)
{
  int local;

  local=bb1.label(); 
}

int main()
{
  BasicBlock bb(1,2,3);
  some(bb);
}
