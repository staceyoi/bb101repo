/*
For batch RTC to change frame_count just set it in
rtcconfig file.  For attach RTC we don't have a way
to change the frame_count of other settings until now.
This JAG fix allows user to set  GDBRTC_CONFIG to 
point to rtcconfig to change setting for attach RTC.
It pretty much uses batch RTC technology

NOTE: 6/6/06:  introduce yet another way to make sure this
process doesn't hang.  JAGaf88949.exp is expected to
kill this and it does most of the time.  But if it 
doesn't do it in such a way that this process automatically
exit by itself after awhile. 

corrupt7b.c
*/


#include <malloc.h>

#define MAXWAIT 1024*1024*200

#pragma OPTIMIZE OFF
void corrupt_footer1()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
}
void corrupt_footer2()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
}
void corrupt_footer2a()
{
corrupt_footer2();

}
void corrupt_footer2b()
{
corrupt_footer2a();
}

void corrupt_footer2c()
{
 corrupt_footer2b();

}
void corrupt_footer2d()
{
 corrupt_footer2c();
}



void corrupt_footer3()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
}

void corrupt_header()
{

  char * cp;
  cp = malloc (100);
  cp[-4] = 88;
  cp[-3] = 99;
  cp[-2] = 100;
  cp[-1] = 77;
}

void corrupt_headera()
{

  corrupt_header();
}
void corrupt_headerb()
{
  corrupt_headera();
}

void corrupt_headerc()
{
  corrupt_headerb();

}


volatile int wait_cnt;
volatile int inner;
int main()
{
  int i;

  /* use the below for debug printing of test case */
  printf("stage 1\n"); 
  for (i=0; i<1000; i++)
  {
  corrupt_header();
  corrupt_footer1();
  corrupt_footer2();
  corrupt_footer3();
  }
   printf("stage 2\n");  
  for (i=0; i<500; i++)
  {
  corrupt_headerc();
  corrupt_footer1();
  corrupt_footer2d();
  corrupt_footer3();
  }
  
  /* wait for 12 minutes, enough time for expect script to attach
     to do interesting checking */
  for (wait_cnt = 0; wait_cnt < MAXWAIT; wait_cnt++)
  {
     for (inner = 0; inner < 1024; inner++)
     {
       if (inner > 1024)
          break;
     }
   }
   printf("stage 3\n");  
}

