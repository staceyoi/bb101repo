static void 
foo()
{
	printf(" Inside function foo \n");
}

extern void foobar();

int main()
{
	printf("Inside function main \n");
	foo();
	foobar();

	return 0;
}

