#include <stdlib.h>

struct mystr {
  int i,j,k;
} aaa = {1,2,3};

struct mystr bbb = {4,5,6};

void foo();
void bar();

struct mystr *ptr = &aaa;
int main()
{
  printf("%d\n", ((int (*) (struct mystr*, struct mystr*)) &foo)(ptr, &bbb));
  return 0;
}

