#include <stdio.h>

struct mystr {
  int i,j,k;
};

int foo(struct mystr *b, struct mystr *c)
{
  return b->i+b->j+b->k+c->i+c->j+c->k;
}

int bar(struct mystr* bbb)
{
  return bbb->i+bbb->j+bbb->k+10;
}

