#include <stdio.h>

int i = 1, j = 0;
int k[4][5] = {{11,12,13,14,15}, 
               {21,22,23,24,25},
               {31,32,33,34,35},
               {41,42,43,44,45}};

int foo(int foo_arg)
{
  fprintf(stderr, "foo_arg = %d\n", foo_arg);
  return 1;
}

main()
{
  foo(2);
  printf("i = %d\n", i);
}
