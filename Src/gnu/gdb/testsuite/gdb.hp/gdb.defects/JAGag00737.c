/* This test file was randomly generated on wasp using:
 *   /proj/tail/Mixmaster/bin/test_gen.pl
 * with the following parameters:
 *   seed:          941512650
 *   num tests:     100
 *   max segments:  5
 *   segment file:  /proj/tail/Mixmaster/bin/extract_segments
 *   retval type:   int
 *   join prob:     10
 *   default prob:  1
 * Generator command line:
     /proj/tail/Mixmaster/bin/test_gen.pl \
       -s 941512650 -n 100 -m 5 -r "int" -j 10 -d 1 \
       /proj/tail/Mixmaster/bin/extract_segments
 */
#include <stdlib.h>
#include <stdio.h>
#include <machine/sys/inline.h>

unsigned char      uc;
         char      sc;
unsigned short     us;
         short     ss;
unsigned int       ui;
         int       si;
unsigned long long ull;
         long long sll;

unsigned char       *puc;
         char       *psc;
unsigned short      *pus;
         short      *pss;
unsigned int        *pui;
         int        *psi;
unsigned long long *pull;
         long long *psll;

unsigned char       cuc;
         char       csc;
unsigned short      cus;
         short      css;
unsigned int        cui;
         int        csi;
unsigned long long cull;
         long long csll;

unsigned char       uc_call(unsigned char      x) { return x; }
         char       sc_call(         char      x) { return x; }
unsigned short      us_call(unsigned short     x) { return x; }
         short      ss_call(         short     x) { return x; }
unsigned int        ui_call(unsigned int       x) { return x; }
         int        si_call(         int       x) { return x; }
unsigned long long ull_call(unsigned long long x) { return x; }
         long long sll_call(         long long x) { return x; }

/* Certain segments modify globals; by reinitializing the globals
 * at the top of every test function, we try to ensure both that
 * every test function stands alone and that the state is the same
 * for both optimized and unoptimized versions of the same function.
 */
void init_globals()
{
    uc = 0;
    sc = 0;
    us = 0;
    ss = 0;
    ui = 0;
    si = 0;
   ull = 0;
   sll = 0;

   puc = &uc;
   psc = &sc;
   pus = &us;
   pss = &ss;
   pui = &ui;
   psi = &si;
  pull = &ull;
  psll = &sll;

   cuc =                           0x98;
   csc =      (char)               0x98;
   cus =                         0xba98;
   css =     (short)             0xba98;
   cui =                     0xfedcba98;
   csi =       (int)         0xfedcba98;
  cull =             0x89abcdeffedcba89ULL;
  csll = (long long) 0x89abcdeffedcba89ULL;
}


#pragma OPTIMIZE OFF
int test_1()
{
  init_globals();

  int _retval;
  int x_130_1;
  int x_178_1;
  int x_82_1;
  srand48(941512650);
  x_178_1 = 0xffffffff;
  x_130_1 = x_178_1 << 11;
  x_82_1 = x_130_1 + -2;
  _retval = (int) x_82_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_1()
{
  init_globals();

  int _retval;
  int x_130_1;
  int x_178_1;
  int x_82_1;
  srand48(941512650);
  x_178_1 = 0xffffffff;
  x_130_1 = x_178_1 << 11;
  x_82_1 = x_130_1 + -2;
  _retval = (int) x_82_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_2()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_146_1;
  int x_154_1;
  int x_203_1;
  char x_50_1;
  int y_114_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = 0x80000000;
  x_50_1 = csc;
  x_114_1 = (int) x_50_1;
  y_114_1 = x_114_1; /* join */
  y_154_1 = x_114_1 * y_114_1;
  x_203_1 = x_154_1 & y_154_1;
  x_146_1 = (x_203_1 & (1 << 24)) ? x_203_1 : -2;
  _retval = x_146_1 / 6;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_2()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_146_1;
  int x_154_1;
  int x_203_1;
  char x_50_1;
  int y_114_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = 0x80000000;
  x_50_1 = csc;
  x_114_1 = (int) x_50_1;
  y_114_1 = x_114_1; /* join */
  y_154_1 = x_114_1 * y_114_1;
  x_203_1 = x_154_1 & y_154_1;
  x_146_1 = (x_203_1 & (1 << 24)) ? x_203_1 : -2;
  _retval = x_146_1 / 6;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_3()
{
  init_globals();

  int _retval;
  int x_162_1;
  int x_162_2;
  int x_162_3;
  int x_170_1;
  int x_186_1;
  int y_186_1;
  srand48(941512650);
  x_186_1 = 0x80000000;
  x_162_3 = 0xffffffff;
  x_162_2 = x_162_3 & 0xff0000ff;
  y_186_1 = x_162_2 | 0xff0000ff;
  x_162_1 = x_186_1 >> (y_186_1 & 0xf);
  x_170_1 = x_162_1 ^ 0xff0000ff;
  _retval = x_170_1 | 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_3()
{
  init_globals();

  int _retval;
  int x_162_1;
  int x_162_2;
  int x_162_3;
  int x_170_1;
  int x_186_1;
  int y_186_1;
  srand48(941512650);
  x_186_1 = 0x80000000;
  x_162_3 = 0xffffffff;
  x_162_2 = x_162_3 & 0xff0000ff;
  y_186_1 = x_162_2 | 0xff0000ff;
  x_162_1 = x_186_1 >> (y_186_1 & 0xf);
  x_170_1 = x_162_1 ^ 0xff0000ff;
  _retval = x_170_1 | 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_4()
{
  init_globals();

  int _retval;
  short x_66_1;
  srand48(941512650);
  x_66_1 = css;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_4()
{
  init_globals();

  int _retval;
  short x_66_1;
  srand48(941512650);
  x_66_1 = css;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_5()
{
  init_globals();

  int _retval;
  int x_162_1;
  int x_170_1;
  int x_186_1;
  int x_195_1;
  int x_210_1;
  int y_186_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0xffffffff;
  x_186_1 = (int) 0x89abcdef;
  x_195_1 = 0x80000000;
  y_186_1 = (x_195_1 < -2) ? x_195_1 : -4;
  x_170_1 = x_186_1 >> (y_186_1 & 0xf);
  x_162_1 = x_170_1 ^ 0x00ffff00;
  y_210_1 = x_162_1 & 0xff0000ff;
  _retval = (x_210_1 < y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_5()
{
  init_globals();

  int _retval;
  int x_162_1;
  int x_170_1;
  int x_186_1;
  int x_195_1;
  int x_210_1;
  int y_186_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0xffffffff;
  x_186_1 = (int) 0x89abcdef;
  x_195_1 = 0x80000000;
  y_186_1 = (x_195_1 < -2) ? x_195_1 : -4;
  x_170_1 = x_186_1 >> (y_186_1 & 0xf);
  x_162_1 = x_170_1 ^ 0x00ffff00;
  y_210_1 = x_162_1 & 0xff0000ff;
  _retval = (x_210_1 < y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_6()
{
  init_globals();

  int _retval;
  char x_142_1;
  char x_206_1;
  char x_46_1;
  char x_50_1;
  long long x_94_1;
  char y_206_1;
  srand48(941512650);
  x_206_1 = 4;
  x_94_1 = csll;
  x_142_1 = (char) x_94_1;
  x_46_1 = x_142_1 / 6;
  y_206_1 = (char) x_46_1;
  x_50_1 = (x_206_1 > y_206_1) ? x_206_1 : y_206_1;
  _retval = (int) x_50_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_6()
{
  init_globals();

  int _retval;
  char x_142_1;
  char x_206_1;
  char x_46_1;
  char x_50_1;
  long long x_94_1;
  char y_206_1;
  srand48(941512650);
  x_206_1 = 4;
  x_94_1 = csll;
  x_142_1 = (char) x_94_1;
  x_46_1 = x_142_1 / 6;
  y_206_1 = (char) x_46_1;
  x_50_1 = (x_206_1 > y_206_1) ? x_206_1 : y_206_1;
  _retval = (int) x_50_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_7()
{
  init_globals();

  int _retval;
  int x_170_1;
  char x_174_1;
  char x_191_1;
  char x_52_1;
  long long x_98_1;
  srand48(941512650);
  x_191_1 = -3;
  x_174_1 = (x_191_1 == 0) ? x_191_1 : 0;
  x_52_1 = x_174_1 << 7;
  x_98_1 = (long long) x_52_1;
  x_170_1 = (int) x_98_1;
  _retval = x_170_1 ^ 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_7()
{
  init_globals();

  int _retval;
  int x_170_1;
  char x_174_1;
  char x_191_1;
  char x_52_1;
  long long x_98_1;
  srand48(941512650);
  x_191_1 = -3;
  x_174_1 = (x_191_1 == 0) ? x_191_1 : 0;
  x_52_1 = x_174_1 << 7;
  x_98_1 = (long long) x_52_1;
  x_170_1 = (int) x_98_1;
  _retval = x_170_1 ^ 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_8()
{
  init_globals();

  int _retval;
  unsigned int x_171_1;
  int x_219_1;
  unsigned int x_76_1;
  long long x_98_1;
  srand48(941512650);
  x_171_1 = 9;
  x_76_1 = x_171_1 & 0x00ffff00;
  x_98_1 = (long long) x_76_1;
  x_219_1 = (int) x_98_1;
  _retval = (x_219_1 == 0);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_8()
{
  init_globals();

  int _retval;
  unsigned int x_171_1;
  int x_219_1;
  unsigned int x_76_1;
  long long x_98_1;
  srand48(941512650);
  x_171_1 = 9;
  x_76_1 = x_171_1 & 0x00ffff00;
  x_98_1 = (long long) x_76_1;
  x_219_1 = (int) x_98_1;
  _retval = (x_219_1 == 0);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_9()
{
  init_globals();

  int _retval;
  short x_66_1;
  srand48(941512650);
  x_66_1 = css;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_9()
{
  init_globals();

  int _retval;
  short x_66_1;
  srand48(941512650);
  x_66_1 = css;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_10()
{
  init_globals();

  int _retval;
  int x_106_1;
  srand48(941512650);
  x_106_1 = 0x7fffffff;
  _retval = si_call(x_106_1);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_10()
{
  init_globals();

  int _retval;
  int x_106_1;
  srand48(941512650);
  x_106_1 = 0x7fffffff;
  _retval = si_call(x_106_1);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_11()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x80000000;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_11()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x80000000;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_12()
{
  init_globals();

  int _retval;
  unsigned long long rA_243_1;
  unsigned long long rC_243_1;
  int x_146_1;
  unsigned short x_200_1;
  unsigned short x_58_1;
  unsigned long long x_89_1;
  srand48(941512650);
  rC_243_1 = 2;
  /*-- pattern --*/
# define this_pos 42
# define this_len 9
  rA_243_1 = _Asm_extr(rC_243_1, this_pos, this_len);
  x_89_1 = _Asm_dep_variable(rA_243_1, rC_243_1, this_pos, this_len);
# undef  this_len
# undef  this_pos
  /*-------------*/
  x_200_1 = (unsigned short) x_89_1;
  x_58_1 = (x_200_1 & (1 << 15)) ? x_200_1 : 5;
  x_146_1 = (int) x_58_1;
  _retval = x_146_1 / 10;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_12()
{
  init_globals();

  int _retval;
  unsigned long long rA_243_1;
  unsigned long long rC_243_1;
  int x_146_1;
  unsigned short x_200_1;
  unsigned short x_58_1;
  unsigned long long x_89_1;
  srand48(941512650);
  rC_243_1 = 2;
  /*-- pattern --*/
# define this_pos 42
# define this_len 9
  rA_243_1 = _Asm_extr(rC_243_1, this_pos, this_len);
  x_89_1 = _Asm_dep_variable(rA_243_1, rC_243_1, this_pos, this_len);
# undef  this_len
# undef  this_pos
  /*-------------*/
  x_200_1 = (unsigned short) x_89_1;
  x_58_1 = (x_200_1 & (1 << 15)) ? x_200_1 : 5;
  x_146_1 = (int) x_58_1;
  _retval = x_146_1 / 10;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_13()
{
  init_globals();

  int _retval;
  int x_203_1;
  srand48(941512650);
  x_203_1 = (int) 0x89abcdef;
  _retval = (x_203_1 & (1 << 24)) ? x_203_1 : -1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_13()
{
  init_globals();

  int _retval;
  int x_203_1;
  srand48(941512650);
  x_203_1 = (int) 0x89abcdef;
  _retval = (x_203_1 & (1 << 24)) ? x_203_1 : -1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_14()
{
  init_globals();

  int _retval;
  short x_112_1;
  int x_146_1;
  short x_201_1;
  int x_210_1;
  short x_66_1;
  short y_112_1;
  int y_210_1;
  srand48(941512650);
  x_112_1 = css;
  y_112_1 = css;
  x_201_1 = x_112_1 - y_112_1;
  x_66_1 = (x_201_1 & (1 << 12)) ? x_201_1 : 3;
  x_210_1 = (int) x_66_1;
  x_146_1 = x_210_1; /* join */
  y_210_1 = x_146_1 / 4;
  _retval = (x_210_1 >= y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_14()
{
  init_globals();

  int _retval;
  short x_112_1;
  int x_146_1;
  short x_201_1;
  int x_210_1;
  short x_66_1;
  short y_112_1;
  int y_210_1;
  srand48(941512650);
  x_112_1 = css;
  y_112_1 = css;
  x_201_1 = x_112_1 - y_112_1;
  x_66_1 = (x_201_1 & (1 << 12)) ? x_201_1 : 3;
  x_210_1 = (int) x_66_1;
  x_146_1 = x_210_1; /* join */
  y_210_1 = x_146_1 / 4;
  _retval = (x_210_1 >= y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_15()
{
  init_globals();

  int _retval;
  unsigned short x_113_1;
  int x_162_1;
  unsigned short x_185_1;
  unsigned short x_57_1;
  unsigned short x_58_1;
  unsigned short y_113_1;
  unsigned short y_185_1;
  srand48(941512650);
  x_113_1 = cus;
  x_185_1 = cus;
  y_185_1 = cus;
  x_57_1 = x_185_1 >> (y_185_1 & 0xf);
  y_113_1 = (unsigned short) x_57_1;
  x_58_1 = x_113_1 + y_113_1;
  x_162_1 = (int) x_58_1;
  _retval = x_162_1 ^ 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_15()
{
  init_globals();

  int _retval;
  unsigned short x_113_1;
  int x_162_1;
  unsigned short x_185_1;
  unsigned short x_57_1;
  unsigned short x_58_1;
  unsigned short y_113_1;
  unsigned short y_185_1;
  srand48(941512650);
  x_113_1 = cus;
  x_185_1 = cus;
  y_185_1 = cus;
  x_57_1 = x_185_1 >> (y_185_1 & 0xf);
  y_113_1 = (unsigned short) x_57_1;
  x_58_1 = x_113_1 + y_113_1;
  x_162_1 = (int) x_58_1;
  _retval = x_162_1 ^ 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_16()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0xffffffff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_16()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0xffffffff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_17()
{
  init_globals();

  int _retval;
  int x_130_1;
  int x_130_2;
  int x_146_1;
  int x_154_1;
  int y_154_1;
  srand48(941512650);
  x_130_2 = 0x7fffffff;
  x_154_1 = x_130_2 + -1;
  y_154_1 = x_154_1; /* join */
  x_130_1 = x_154_1 | y_154_1;
  x_146_1 = x_130_1 + 5;
  _retval = x_146_1 / 11;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_17()
{
  init_globals();

  int _retval;
  int x_130_1;
  int x_130_2;
  int x_146_1;
  int x_154_1;
  int y_154_1;
  srand48(941512650);
  x_130_2 = 0x7fffffff;
  x_154_1 = x_130_2 + -1;
  y_154_1 = x_154_1; /* join */
  x_130_1 = x_154_1 | y_154_1;
  x_146_1 = x_130_1 + 5;
  _retval = x_146_1 / 11;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_18()
{
  init_globals();

  int _retval;
  int x_162_1;
  srand48(941512650);
  x_162_1 = -5;
  _retval = x_162_1 & 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_18()
{
  init_globals();

  int _retval;
  int x_162_1;
  srand48(941512650);
  x_162_1 = -5;
  _retval = x_162_1 & 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_19()
{
  init_globals();

  int _retval;
  unsigned long long r2_241_1;
  unsigned long long r3_231_1;
  unsigned long long r3_234_1;
  unsigned long long rA_242_1;
  unsigned long long rC_242_1;
  int x_154_1;
  unsigned long long x_90_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = 4;
  /*-- pattern --*/
  r2_241_1 = (unsigned long long)*pui;
  rC_242_1 = _Asm_extr_u(r2_241_1, 8, 30);
  /*-------------*/
  /*-- pattern --*/
  rA_242_1 = _Asm_extr_u(rC_242_1, 36, 32);
  r3_234_1 = _Asm_dep_variable(rA_242_1, rC_242_1, 21, 7);
  /*-------------*/
  r3_231_1 = _Asm_zxt(_XSZ_1, r3_234_1);
  x_90_1 = _Asm_dep_fixed(0, r3_231_1, 33, 28);
  y_154_1 = (int) x_90_1;
  _retval = x_154_1 & y_154_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_19()
{
  init_globals();

  int _retval;
  unsigned long long r2_241_1;
  unsigned long long r3_231_1;
  unsigned long long r3_234_1;
  unsigned long long rA_242_1;
  unsigned long long rC_242_1;
  int x_154_1;
  unsigned long long x_90_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = 4;
  /*-- pattern --*/
  r2_241_1 = (unsigned long long)*pui;
  rC_242_1 = _Asm_extr_u(r2_241_1, 8, 30);
  /*-------------*/
  /*-- pattern --*/
  rA_242_1 = _Asm_extr_u(rC_242_1, 36, 32);
  r3_234_1 = _Asm_dep_variable(rA_242_1, rC_242_1, 21, 7);
  /*-------------*/
  r3_231_1 = _Asm_zxt(_XSZ_1, r3_234_1);
  x_90_1 = _Asm_dep_fixed(0, r3_231_1, 33, 28);
  y_154_1 = (int) x_90_1;
  _retval = x_154_1 & y_154_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_20()
{
  init_globals();

  int _retval;
  int x_170_1;
  unsigned int x_194_1;
  int x_210_1;
  unsigned int x_226_1;
  unsigned int x_74_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0x80000000;
  x_194_1 = 3;
  x_226_1 = (x_194_1 != 5) ? x_194_1 : 7;
  *pui = x_226_1;
  x_74_1 = *pui;
  y_210_1 = (int) x_74_1;
  x_170_1 = (x_210_1 > y_210_1) ? x_210_1 : y_210_1;
  _retval = x_170_1 ^ 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_20()
{
  init_globals();

  int _retval;
  int x_170_1;
  unsigned int x_194_1;
  int x_210_1;
  unsigned int x_226_1;
  unsigned int x_74_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0x80000000;
  x_194_1 = 3;
  x_226_1 = (x_194_1 != 5) ? x_194_1 : 7;
  *pui = x_226_1;
  x_74_1 = *pui;
  y_210_1 = (int) x_74_1;
  x_170_1 = (x_210_1 > y_210_1) ? x_210_1 : y_210_1;
  _retval = x_170_1 ^ 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_21()
{
  init_globals();

  int _retval;
  char x_102_1;
  char x_118_1;
  char x_118_2;
  char x_223_1;
  char x_50_1;
  char y_118_1;
  char y_118_2;
  srand48(941512650);
  x_118_1 = (char) 0x89;
  x_102_1 = (char) 0x89;
  x_118_2 = sc_call(x_102_1);
  y_118_2 = x_118_2; /* join */
  y_118_1 = (y_118_2 == 0 ? 0 : x_118_2 / y_118_2);
  x_223_1 = (y_118_1 == 0 ? 0 : x_118_1 / y_118_1);
  *psc = x_223_1;
  x_50_1 = *psc;
  _retval = (int) x_50_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_21()
{
  init_globals();

  int _retval;
  char x_102_1;
  char x_118_1;
  char x_118_2;
  char x_223_1;
  char x_50_1;
  char y_118_1;
  char y_118_2;
  srand48(941512650);
  x_118_1 = (char) 0x89;
  x_102_1 = (char) 0x89;
  x_118_2 = sc_call(x_102_1);
  y_118_2 = x_118_2; /* join */
  y_118_1 = (y_118_2 == 0 ? 0 : x_118_2 / y_118_2);
  x_223_1 = (y_118_1 == 0 ? 0 : x_118_1 / y_118_1);
  *psc = x_223_1;
  x_50_1 = *psc;
  _retval = (int) x_50_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_22()
{
  init_globals();

  int _retval;
  unsigned long long r1_238_1;
  unsigned long long r2_238_1;
  unsigned long long r2_240_1;
  unsigned long long r3_231_1;
  unsigned long long r3_238_1;
  int x_162_1;
  unsigned long long x_189_1;
  unsigned long long x_90_1;
  unsigned long long y_189_1;
  srand48(941512650);
  r1_238_1 = 0x80000000ULL;
  /*-- pattern --*/
  r2_240_1 = (unsigned long long)*pui;
  x_189_1 = _Asm_extr(r2_240_1, 0, 29);
  /*-------------*/
  y_189_1 = 3;
  r3_231_1 = x_189_1 >> (y_189_1 & 0xf);
  r2_238_1 = _Asm_dep_fixed(-1, r3_231_1, 36, 20);
  /*-- pattern --*/
  r3_238_1 = _Asm_dep_variable(r1_238_1, r2_238_1, 30, 7);
  x_90_1 = _Asm_extr_u(r3_238_1, 28, 25);
  /*-------------*/
  x_162_1 = (int) x_90_1;
  _retval = x_162_1 ^ 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_22()
{
  init_globals();

  int _retval;
  unsigned long long r1_238_1;
  unsigned long long r2_238_1;
  unsigned long long r2_240_1;
  unsigned long long r3_231_1;
  unsigned long long r3_238_1;
  int x_162_1;
  unsigned long long x_189_1;
  unsigned long long x_90_1;
  unsigned long long y_189_1;
  srand48(941512650);
  r1_238_1 = 0x80000000ULL;
  /*-- pattern --*/
  r2_240_1 = (unsigned long long)*pui;
  x_189_1 = _Asm_extr(r2_240_1, 0, 29);
  /*-------------*/
  y_189_1 = 3;
  r3_231_1 = x_189_1 >> (y_189_1 & 0xf);
  r2_238_1 = _Asm_dep_fixed(-1, r3_231_1, 36, 20);
  /*-- pattern --*/
  r3_238_1 = _Asm_dep_variable(r1_238_1, r2_238_1, 30, 7);
  x_90_1 = _Asm_extr_u(r3_238_1, 28, 25);
  /*-------------*/
  x_162_1 = (int) x_90_1;
  _retval = x_162_1 ^ 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_23()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_146_1;
  unsigned int x_163_1;
  int x_195_1;
  unsigned int x_74_1;
  srand48(941512650);
  x_163_1 = cui;
  x_74_1 = x_163_1 | 0xff0000ff;
  x_146_1 = (int) x_74_1;
  x_106_1 = x_146_1 / 2;
  x_195_1 = si_call(x_106_1);
  _retval = (x_195_1 > -2) ? x_195_1 : 5;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_23()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_146_1;
  unsigned int x_163_1;
  int x_195_1;
  unsigned int x_74_1;
  srand48(941512650);
  x_163_1 = cui;
  x_74_1 = x_163_1 | 0xff0000ff;
  x_146_1 = (int) x_74_1;
  x_106_1 = x_146_1 / 2;
  x_195_1 = si_call(x_106_1);
  _retval = (x_195_1 > -2) ? x_195_1 : 5;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_24()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x80000000;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_24()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x80000000;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_25()
{
  init_globals();

  int _retval;
  int x_227_1;
  srand48(941512650);
  x_227_1 = (int) 0x89abcdef;
  *psi = x_227_1;
  _retval = *psi;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_25()
{
  init_globals();

  int _retval;
  int x_227_1;
  srand48(941512650);
  x_227_1 = (int) 0x89abcdef;
  *psi = x_227_1;
  _retval = *psi;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_26()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_186_1;
  int x_82_1;
  int y_114_1;
  int y_186_1;
  srand48(941512650);
  x_82_1 = -3;
  x_186_1 = (int) x_82_1;
  x_114_1 = 0x7fffffff;
  y_114_1 = x_114_1; /* join */
  y_186_1 = x_114_1 - y_114_1;
  _retval = x_186_1 >> (y_186_1 & 0xf);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_26()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_186_1;
  int x_82_1;
  int y_114_1;
  int y_186_1;
  srand48(941512650);
  x_82_1 = -3;
  x_186_1 = (int) x_82_1;
  x_114_1 = 0x7fffffff;
  y_114_1 = x_114_1; /* join */
  y_186_1 = x_114_1 - y_114_1;
  _retval = x_186_1 >> (y_186_1 & 0xf);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_27()
{
  init_globals();

  int _retval;
  unsigned long long r2_240_1;
  unsigned long long r2_241_1;
  unsigned long long x_125_1;
  unsigned long long x_204_1;
  unsigned long long x_213_1;
  unsigned long long x_90_1;
  unsigned long long y_125_1;
  unsigned long long y_213_1;
  srand48(941512650);
  /*-- pattern --*/
  r2_241_1 = (unsigned long long)*pui;
  x_125_1 = _Asm_extr(r2_241_1, 59, 5);
  /*-------------*/
  x_204_1 = cull;
  y_125_1 = (x_204_1 & (1ULL << 26)) ? x_204_1 : 5;
  x_213_1 = (y_125_1 == 0 ? 0 : x_125_1 / y_125_1);
  /*-- pattern --*/
  r2_240_1 = (unsigned long long)*pui;
  y_213_1 = _Asm_extr_u(r2_240_1, 0, 13);
  /*-------------*/
  x_90_1 = (x_213_1 >= y_213_1) ? x_213_1 : y_213_1;
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_27()
{
  init_globals();

  int _retval;
  unsigned long long r2_240_1;
  unsigned long long r2_241_1;
  unsigned long long x_125_1;
  unsigned long long x_204_1;
  unsigned long long x_213_1;
  unsigned long long x_90_1;
  unsigned long long y_125_1;
  unsigned long long y_213_1;
  srand48(941512650);
  /*-- pattern --*/
  r2_241_1 = (unsigned long long)*pui;
  x_125_1 = _Asm_extr(r2_241_1, 59, 5);
  /*-------------*/
  x_204_1 = cull;
  y_125_1 = (x_204_1 & (1ULL << 26)) ? x_204_1 : 5;
  x_213_1 = (y_125_1 == 0 ? 0 : x_125_1 / y_125_1);
  /*-- pattern --*/
  r2_240_1 = (unsigned long long)*pui;
  y_213_1 = _Asm_extr_u(r2_240_1, 0, 13);
  /*-------------*/
  x_90_1 = (x_213_1 >= y_213_1) ? x_213_1 : y_213_1;
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_28()
{
  init_globals();

  int _retval;
  int x_203_1;
  srand48(941512650);
  x_203_1 = -4;
  _retval = (x_203_1 & (1 << 6)) ? x_203_1 : -4;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_28()
{
  init_globals();

  int _retval;
  int x_203_1;
  srand48(941512650);
  x_203_1 = -4;
  _retval = (x_203_1 & (1 << 6)) ? x_203_1 : -4;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_29()
{
  init_globals();

  int _retval;
  int x_146_1;
  unsigned long long x_90_1;
  srand48(941512650);
  x_90_1 = 0x80000000ULL;
  x_146_1 = (int) x_90_1;
  _retval = x_146_1 / 5;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_29()
{
  init_globals();

  int _retval;
  int x_146_1;
  unsigned long long x_90_1;
  srand48(941512650);
  x_90_1 = 0x80000000ULL;
  x_146_1 = (int) x_90_1;
  _retval = x_146_1 / 5;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_30()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0xffffffff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_30()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0xffffffff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_31()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x7fffffff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_31()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x7fffffff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_32()
{
  init_globals();

  int _retval;
  int x_106_1;
  srand48(941512650);
  x_106_1 = 0x80000000;
  _retval = si_call(x_106_1);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_32()
{
  init_globals();

  int _retval;
  int x_106_1;
  srand48(941512650);
  x_106_1 = 0x80000000;
  _retval = si_call(x_106_1);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_33()
{
  init_globals();

  int _retval;
  int x_122_1;
  char x_142_1;
  char x_215_1;
  char x_46_1;
  char x_50_1;
  int y_122_1;
  srand48(941512650);
  x_122_1 = 0xffffffff;
  x_142_1 = (char) 0x89;
  x_46_1 = x_142_1 / 15;
  x_215_1 = (char) x_46_1;
  x_50_1 = (x_215_1 != -5);
  y_122_1 = (int) x_50_1;
  _retval = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_33()
{
  init_globals();

  int _retval;
  int x_122_1;
  char x_142_1;
  char x_215_1;
  char x_46_1;
  char x_50_1;
  int y_122_1;
  srand48(941512650);
  x_122_1 = 0xffffffff;
  x_142_1 = (char) 0x89;
  x_46_1 = x_142_1 / 15;
  x_215_1 = (char) x_46_1;
  x_50_1 = (x_215_1 != -5);
  y_122_1 = (int) x_50_1;
  _retval = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_34()
{
  init_globals();

  int _retval;
  unsigned char x_103_1;
  char x_223_1;
  unsigned char x_42_1;
  char x_51_1;
  unsigned int x_71_1;
  srand48(941512650);
  x_223_1 = (char) 0x89;
  *psc = x_223_1;
  x_51_1 = *psc;
  x_71_1 = (unsigned int) x_51_1;
  x_103_1 = (unsigned char) x_71_1;
  x_42_1 = uc_call(x_103_1);
  _retval = (int) x_42_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_34()
{
  init_globals();

  int _retval;
  unsigned char x_103_1;
  char x_223_1;
  unsigned char x_42_1;
  char x_51_1;
  unsigned int x_71_1;
  srand48(941512650);
  x_223_1 = (char) 0x89;
  *psc = x_223_1;
  x_51_1 = *psc;
  x_71_1 = (unsigned int) x_51_1;
  x_103_1 = (unsigned char) x_71_1;
  x_42_1 = uc_call(x_103_1);
  _retval = (int) x_42_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_35()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_106_2;
  int x_195_1;
  int x_195_2;
  int x_219_1;
  srand48(941512650);
  x_106_2 = (int) 0x89abcdef;
  x_106_1 = si_call(x_106_2);
  x_219_1 = si_call(x_106_1);
  x_195_2 = (x_219_1 < -5);
  x_195_1 = (x_195_2 <= 2) ? x_195_2 : -1;
  _retval = (x_195_1 >= 5) ? x_195_1 : -4;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_35()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_106_2;
  int x_195_1;
  int x_195_2;
  int x_219_1;
  srand48(941512650);
  x_106_2 = (int) 0x89abcdef;
  x_106_1 = si_call(x_106_2);
  x_219_1 = si_call(x_106_1);
  x_195_2 = (x_219_1 < -5);
  x_195_1 = (x_195_2 <= 2) ? x_195_2 : -1;
  _retval = (x_195_1 >= 5) ? x_195_1 : -4;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_36()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_114_1;
  int x_170_1;
  int x_186_1;
  int x_227_1;
  int y_114_1;
  int y_186_1;
  srand48(941512650);
  x_114_1 = csi;
  x_186_1 = 0xffffffff;
  x_106_1 = 0xffffffff;
  x_227_1 = si_call(x_106_1);
  *psi = x_227_1;
  x_170_1 = *psi;
  y_186_1 = x_170_1 ^ 0x00ffff00;
  y_114_1 = x_186_1 << (y_186_1 & 0xf);
  _retval = x_114_1 - y_114_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_36()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_114_1;
  int x_170_1;
  int x_186_1;
  int x_227_1;
  int y_114_1;
  int y_186_1;
  srand48(941512650);
  x_114_1 = csi;
  x_186_1 = 0xffffffff;
  x_106_1 = 0xffffffff;
  x_227_1 = si_call(x_106_1);
  *psi = x_227_1;
  x_170_1 = *psi;
  y_186_1 = x_170_1 ^ 0x00ffff00;
  y_114_1 = x_186_1 << (y_186_1 & 0xf);
  _retval = x_114_1 - y_114_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_37()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = (int) 0x89abcdef;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_37()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = (int) 0x89abcdef;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_38()
{
  init_globals();

  int _retval;
  int x_186_1;
  char x_199_1;
  unsigned char x_38_1;
  char x_50_1;
  int y_186_1;
  srand48(941512650);
  x_186_1 = (int) 0x89abcdef;
  x_38_1 = cuc;
  x_199_1 = (char) x_38_1;
  x_50_1 = (x_199_1 & (1 << 1)) ? x_199_1 : 0;
  y_186_1 = (int) x_50_1;
  _retval = x_186_1 >> (y_186_1 & 0xf);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_38()
{
  init_globals();

  int _retval;
  int x_186_1;
  char x_199_1;
  unsigned char x_38_1;
  char x_50_1;
  int y_186_1;
  srand48(941512650);
  x_186_1 = (int) 0x89abcdef;
  x_38_1 = cuc;
  x_199_1 = (char) x_38_1;
  x_50_1 = (x_199_1 & (1 << 1)) ? x_199_1 : 0;
  y_186_1 = (int) x_50_1;
  _retval = x_186_1 >> (y_186_1 & 0xf);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_39()
{
  init_globals();

  int _retval;
  int x_138_1;
  srand48(941512650);
  x_138_1 = -1;
  _retval = x_138_1 * 16;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_39()
{
  init_globals();

  int _retval;
  int x_138_1;
  srand48(941512650);
  x_138_1 = -1;
  _retval = x_138_1 * 16;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_40()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_114_1;
  int x_114_2;
  int x_130_1;
  int x_130_2;
  int y_114_1;
  int y_114_2;
  srand48(941512650);
  x_114_1 = (int) 0x89abcdef;
  x_114_2 = csi;
  y_114_2 = csi;
  x_130_2 = x_114_2 + y_114_2;
  x_130_1 = x_130_2 + 3;
  x_106_1 = x_130_1 + 3;
  y_114_1 = si_call(x_106_1);
  _retval = x_114_1 - y_114_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_40()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_114_1;
  int x_114_2;
  int x_130_1;
  int x_130_2;
  int y_114_1;
  int y_114_2;
  srand48(941512650);
  x_114_1 = (int) 0x89abcdef;
  x_114_2 = csi;
  y_114_2 = csi;
  x_130_2 = x_114_2 + y_114_2;
  x_130_1 = x_130_2 + 3;
  x_106_1 = x_130_1 + 3;
  y_114_1 = si_call(x_106_1);
  _retval = x_114_1 - y_114_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_41()
{
  init_globals();

  int _retval;
  int x_122_1;
  int x_162_1;
  int x_195_1;
  int x_203_1;
  int y_122_1;
  srand48(941512650);
  x_195_1 = 0x80000000;
  x_122_1 = (x_195_1 > 4) ? x_195_1 : 0;
  x_203_1 = 0x80000000;
  y_122_1 = (x_203_1 & (1 << 28)) ? x_203_1 : 1;
  x_162_1 = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  _retval = x_162_1 ^ 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_41()
{
  init_globals();

  int _retval;
  int x_122_1;
  int x_162_1;
  int x_195_1;
  int x_203_1;
  int y_122_1;
  srand48(941512650);
  x_195_1 = 0x80000000;
  x_122_1 = (x_195_1 > 4) ? x_195_1 : 0;
  x_203_1 = 0x80000000;
  y_122_1 = (x_203_1 & (1 << 28)) ? x_203_1 : 1;
  x_162_1 = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  _retval = x_162_1 ^ 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_42()
{
  init_globals();

  int _retval;
  int x_122_1;
  unsigned char x_135_1;
  unsigned char x_42_1;
  int y_122_1;
  srand48(941512650);
  x_135_1 = cuc;
  x_42_1 = x_135_1 * 4;
  x_122_1 = (int) x_42_1;
  y_122_1 = x_122_1; /* join */
  _retval = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_42()
{
  init_globals();

  int _retval;
  int x_122_1;
  unsigned char x_135_1;
  unsigned char x_42_1;
  int y_122_1;
  srand48(941512650);
  x_135_1 = cuc;
  x_42_1 = x_135_1 * 4;
  x_122_1 = (int) x_42_1;
  y_122_1 = x_122_1; /* join */
  _retval = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_43()
{
  init_globals();

  int _retval;
  int x_170_1;
  unsigned short x_58_1;
  long long x_97_1;
  srand48(941512650);
  x_97_1 = 0x7fffffffLL;
  x_58_1 = (unsigned short) x_97_1;
  x_170_1 = (int) x_58_1;
  _retval = x_170_1 & 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_43()
{
  init_globals();

  int _retval;
  int x_170_1;
  unsigned short x_58_1;
  long long x_97_1;
  srand48(941512650);
  x_97_1 = 0x7fffffffLL;
  x_58_1 = (unsigned short) x_97_1;
  x_170_1 = (int) x_58_1;
  _retval = x_170_1 & 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_44()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = -4;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_44()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = -4;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_45()
{
  init_globals();

  int _retval;
  long long x_116_1;
  long long x_172_1;
  long long x_205_1;
  long long x_212_1;
  long long x_98_1;
  long long y_116_1;
  long long y_212_1;
  srand48(941512650);
  x_212_1 = 0x7fffffffLL;
  x_172_1 = x_212_1; /* join */
  x_116_1 = x_172_1 & 0x0000ffffffff0000ULL;
  y_116_1 = x_212_1; /* join */
  x_205_1 = x_116_1 - y_116_1;
  y_212_1 = (x_205_1 & (1LL << 34)) ? x_205_1 : -4;
  x_98_1 = (x_212_1 <= y_212_1) ? x_212_1 : y_212_1;
  _retval = (int) x_98_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_45()
{
  init_globals();

  int _retval;
  long long x_116_1;
  long long x_172_1;
  long long x_205_1;
  long long x_212_1;
  long long x_98_1;
  long long y_116_1;
  long long y_212_1;
  srand48(941512650);
  x_212_1 = 0x7fffffffLL;
  x_172_1 = x_212_1; /* join */
  x_116_1 = x_172_1 & 0x0000ffffffff0000ULL;
  y_116_1 = x_212_1; /* join */
  x_205_1 = x_116_1 - y_116_1;
  y_212_1 = (x_205_1 & (1LL << 34)) ? x_205_1 : -4;
  x_98_1 = (x_212_1 <= y_212_1) ? x_212_1 : y_212_1;
  _retval = (int) x_98_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_46()
{
  init_globals();

  int _retval;
  int x_154_1;
  int x_154_2;
  int x_186_1;
  short x_66_1;
  unsigned int x_72_1;
  int y_154_1;
  int y_154_2;
  int y_186_1;
  srand48(941512650);
  x_154_1 = -5;
  x_186_1 = 0x7fffffff;
  x_154_2 = (int) 0x89abcdef;
  x_72_1 = 0xffffffff;
  x_66_1 = (short) x_72_1;
  y_154_2 = (int) x_66_1;
  y_186_1 = x_154_2 ^ y_154_2;
  y_154_1 = x_186_1 >> (y_186_1 & 0xf);
  _retval = x_154_1 | y_154_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_46()
{
  init_globals();

  int _retval;
  int x_154_1;
  int x_154_2;
  int x_186_1;
  short x_66_1;
  unsigned int x_72_1;
  int y_154_1;
  int y_154_2;
  int y_186_1;
  srand48(941512650);
  x_154_1 = -5;
  x_186_1 = 0x7fffffff;
  x_154_2 = (int) 0x89abcdef;
  x_72_1 = 0xffffffff;
  x_66_1 = (short) x_72_1;
  y_154_2 = (int) x_66_1;
  y_186_1 = x_154_2 ^ y_154_2;
  y_154_1 = x_186_1 >> (y_186_1 & 0xf);
  _retval = x_154_1 | y_154_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_47()
{
  init_globals();

  int _retval;
  int x_195_1;
  short x_66_1;
  srand48(941512650);
  x_66_1 = (short) 0x89ab;
  x_195_1 = (int) x_66_1;
  _retval = (x_195_1 < 1) ? x_195_1 : 0;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_47()
{
  init_globals();

  int _retval;
  int x_195_1;
  short x_66_1;
  srand48(941512650);
  x_66_1 = (short) 0x89ab;
  x_195_1 = (int) x_66_1;
  _retval = (x_195_1 < 1) ? x_195_1 : 0;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_48()
{
  init_globals();

  int _retval;
  int x_146_1;
  int x_162_1;
  int x_210_1;
  int x_82_1;
  int y_210_1;
  srand48(941512650);
  x_82_1 = 0xffffffff;
  x_146_1 = (int) x_82_1;
  x_162_1 = x_146_1 / 2;
  x_210_1 = x_162_1 & 0xff0000ff;
  y_210_1 = (int) 0x89abcdef;
  _retval = (x_210_1 <= y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_48()
{
  init_globals();

  int _retval;
  int x_146_1;
  int x_162_1;
  int x_210_1;
  int x_82_1;
  int y_210_1;
  srand48(941512650);
  x_82_1 = 0xffffffff;
  x_146_1 = (int) x_82_1;
  x_162_1 = x_146_1 / 2;
  x_210_1 = x_162_1 & 0xff0000ff;
  y_210_1 = (int) 0x89abcdef;
  _retval = (x_210_1 <= y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_49()
{
  init_globals();

  int _retval;
  int x_162_1;
  srand48(941512650);
  x_162_1 = (int) 0x89abcdef;
  _retval = x_162_1 | 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_49()
{
  init_globals();

  int _retval;
  int x_162_1;
  srand48(941512650);
  x_162_1 = (int) 0x89abcdef;
  _retval = x_162_1 | 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_50()
{
  init_globals();

  int _retval;
  int x_227_1;
  srand48(941512650);
  x_227_1 = 0x80000000;
  *psi = x_227_1;
  _retval = *psi;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_50()
{
  init_globals();

  int _retval;
  int x_227_1;
  srand48(941512650);
  x_227_1 = 0x80000000;
  *psi = x_227_1;
  _retval = *psi;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_51()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = -1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_51()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = -1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_52()
{
  init_globals();

  int _retval;
  int x_146_1;
  srand48(941512650);
  x_146_1 = 0x7fffffff;
  _retval = x_146_1 / 9;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_52()
{
  init_globals();

  int _retval;
  int x_146_1;
  srand48(941512650);
  x_146_1 = 0x7fffffff;
  _retval = x_146_1 / 9;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_53()
{
  init_globals();

  int _retval;
  long long x_132_1;
  int x_186_1;
  unsigned short x_60_1;
  long long x_97_1;
  long long x_98_1;
  int y_186_1;
  srand48(941512650);
  x_186_1 = 0x80000000;
  x_97_1 = 0xffffffffLL;
  x_60_1 = (unsigned short) x_97_1;
  x_132_1 = (long long) x_60_1;
  x_98_1 = x_132_1 + -3;
  y_186_1 = (int) x_98_1;
  _retval = x_186_1 << (y_186_1 & 0xf);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_53()
{
  init_globals();

  int _retval;
  long long x_132_1;
  int x_186_1;
  unsigned short x_60_1;
  long long x_97_1;
  long long x_98_1;
  int y_186_1;
  srand48(941512650);
  x_186_1 = 0x80000000;
  x_97_1 = 0xffffffffLL;
  x_60_1 = (unsigned short) x_97_1;
  x_132_1 = (long long) x_60_1;
  x_98_1 = x_132_1 + -3;
  y_186_1 = (int) x_98_1;
  _retval = x_186_1 << (y_186_1 & 0xf);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_54()
{
  init_globals();

  int _retval;
  unsigned int x_179_1;
  unsigned int x_74_1;
  unsigned long long x_91_1;
  srand48(941512650);
  x_91_1 = 0x7fffffffULL;
  x_179_1 = (unsigned int) x_91_1;
  x_74_1 = x_179_1 << 29;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_54()
{
  init_globals();

  int _retval;
  unsigned int x_179_1;
  unsigned int x_74_1;
  unsigned long long x_91_1;
  srand48(941512650);
  x_91_1 = 0x7fffffffULL;
  x_179_1 = (unsigned int) x_91_1;
  x_74_1 = x_179_1 << 29;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_55()
{
  init_globals();

  int _retval;
  unsigned short x_161_1;
  unsigned short x_58_1;
  srand48(941512650);
  x_161_1 = cus;
  x_58_1 = x_161_1 & 0xf00f;
  _retval = (int) x_58_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_55()
{
  init_globals();

  int _retval;
  unsigned short x_161_1;
  unsigned short x_58_1;
  srand48(941512650);
  x_161_1 = cus;
  x_58_1 = x_161_1 & 0xf00f;
  _retval = (int) x_58_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_56()
{
  init_globals();

  int _retval;
  unsigned int x_131_1;
  unsigned char x_190_1;
  unsigned char x_43_1;
  unsigned int x_74_1;
  srand48(941512650);
  x_190_1 = cuc;
  x_43_1 = (x_190_1 != 2) ? x_190_1 : 6;
  x_131_1 = (unsigned int) x_43_1;
  x_74_1 = x_131_1 + 0;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_56()
{
  init_globals();

  int _retval;
  unsigned int x_131_1;
  unsigned char x_190_1;
  unsigned char x_43_1;
  unsigned int x_74_1;
  srand48(941512650);
  x_190_1 = cuc;
  x_43_1 = (x_190_1 != 2) ? x_190_1 : 6;
  x_131_1 = (unsigned int) x_43_1;
  x_74_1 = x_131_1 + 0;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_57()
{
  init_globals();

  int _retval;
  short x_152_1;
  short x_160_1;
  short x_184_1;
  short x_208_1;
  short x_66_1;
  short y_152_1;
  short y_184_1;
  short y_208_1;
  srand48(941512650);
  x_152_1 = (short) 0x89ab;
  x_184_1 = 4;
  x_208_1 = (short) 0x89ab;
  x_160_1 = (short) 0x89ab;
  y_208_1 = x_160_1 & 0xf00f;
  y_184_1 = (x_208_1 > y_208_1) ? x_208_1 : y_208_1;
  y_152_1 = x_184_1 << (y_184_1 & 0xf);
  x_66_1 = x_152_1 ^ y_152_1;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_57()
{
  init_globals();

  int _retval;
  short x_152_1;
  short x_160_1;
  short x_184_1;
  short x_208_1;
  short x_66_1;
  short y_152_1;
  short y_184_1;
  short y_208_1;
  srand48(941512650);
  x_152_1 = (short) 0x89ab;
  x_184_1 = 4;
  x_208_1 = (short) 0x89ab;
  x_160_1 = (short) 0x89ab;
  y_208_1 = x_160_1 & 0xf00f;
  y_184_1 = (x_208_1 > y_208_1) ? x_208_1 : y_208_1;
  y_152_1 = x_184_1 << (y_184_1 & 0xf);
  x_66_1 = x_152_1 ^ y_152_1;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_58()
{
  init_globals();

  int _retval;
  int x_170_1;
  int x_203_1;
  int x_203_2;
  int x_210_1;
  int y_210_1;
  srand48(941512650);
  x_170_1 = 0x7fffffff;
  x_203_2 = x_170_1 ^ 0x00ffff00;
  x_203_1 = (x_203_2 & (1 << 4)) ? x_203_2 : 5;
  x_210_1 = (x_203_1 & (1 << 18)) ? x_203_1 : -4;
  y_210_1 = x_210_1; /* join */
  _retval = (x_210_1 < y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_58()
{
  init_globals();

  int _retval;
  int x_170_1;
  int x_203_1;
  int x_203_2;
  int x_210_1;
  int y_210_1;
  srand48(941512650);
  x_170_1 = 0x7fffffff;
  x_203_2 = x_170_1 ^ 0x00ffff00;
  x_203_1 = (x_203_2 & (1 << 4)) ? x_203_2 : 5;
  x_210_1 = (x_203_1 & (1 << 18)) ? x_203_1 : -4;
  y_210_1 = x_210_1; /* join */
  _retval = (x_210_1 < y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_59()
{
  init_globals();

  int _retval;
  unsigned char x_119_1;
  unsigned char x_214_1;
  unsigned char x_214_2;
  unsigned char x_42_1;
  unsigned char y_119_1;
  srand48(941512650);
  x_119_1 = 0x89;
  y_119_1 = 0x89;
  x_214_2 = (y_119_1 == 0 ? 0 : x_119_1 / y_119_1);
  x_214_1 = (x_214_2 > 8);
  x_42_1 = (x_214_1 == 0);
  _retval = (int) x_42_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_59()
{
  init_globals();

  int _retval;
  unsigned char x_119_1;
  unsigned char x_214_1;
  unsigned char x_214_2;
  unsigned char x_42_1;
  unsigned char y_119_1;
  srand48(941512650);
  x_119_1 = 0x89;
  y_119_1 = 0x89;
  x_214_2 = (y_119_1 == 0 ? 0 : x_119_1 / y_119_1);
  x_214_1 = (x_214_2 > 8);
  x_42_1 = (x_214_1 == 0);
  _retval = (int) x_42_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_60()
{
  init_globals();

  int _retval;
  unsigned int x_147_1;
  unsigned int x_187_1;
  short x_67_1;
  unsigned int x_74_1;
  unsigned int y_187_1;
  srand48(941512650);
  x_67_1 = css;
  x_187_1 = (unsigned int) x_67_1;
  y_187_1 = 0xffffffff;
  x_147_1 = x_187_1 >> (y_187_1 & 0xf);
  x_74_1 = x_147_1 / 5;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_60()
{
  init_globals();

  int _retval;
  unsigned int x_147_1;
  unsigned int x_187_1;
  short x_67_1;
  unsigned int x_74_1;
  unsigned int y_187_1;
  srand48(941512650);
  x_67_1 = css;
  x_187_1 = (unsigned int) x_67_1;
  y_187_1 = 0xffffffff;
  x_147_1 = x_187_1 >> (y_187_1 & 0xf);
  x_74_1 = x_147_1 / 5;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_61()
{
  init_globals();

  int _retval;
  int x_210_1;
  int x_219_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0x80000000;
  x_219_1 = -1;
  y_210_1 = (x_219_1 >= -2);
  _retval = (x_210_1 < y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_61()
{
  init_globals();

  int _retval;
  int x_210_1;
  int x_219_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0x80000000;
  x_219_1 = -1;
  y_210_1 = (x_219_1 >= -2);
  _retval = (x_210_1 < y_210_1) ? x_210_1 : y_210_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_62()
{
  init_globals();

  int _retval;
  short x_152_1;
  short x_66_1;
  short y_152_1;
  srand48(941512650);
  x_152_1 = (short) 0x89ab;
  y_152_1 = -4;
  x_66_1 = x_152_1 & y_152_1;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_62()
{
  init_globals();

  int _retval;
  short x_152_1;
  short x_66_1;
  short y_152_1;
  srand48(941512650);
  x_152_1 = (short) 0x89ab;
  y_152_1 = -4;
  x_66_1 = x_152_1 & y_152_1;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_63()
{
  init_globals();

  int _retval;
  int x_130_1;
  srand48(941512650);
  x_130_1 = -1;
  _retval = x_130_1 + -1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_63()
{
  init_globals();

  int _retval;
  int x_130_1;
  srand48(941512650);
  x_130_1 = -1;
  _retval = x_130_1 + -1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_64()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = csi;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_64()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = csi;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_65()
{
  init_globals();

  int _retval;
  unsigned long long r1_245_1;
  unsigned long long r2_245_1;
  unsigned long long r3_231_1;
  unsigned long long rA_244_1;
  unsigned long long rB_244_1;
  unsigned long long rC_244_1;
  unsigned short x_61_1;
  unsigned long long x_90_1;
  srand48(941512650);
  x_61_1 = 2;
  r1_245_1 = (unsigned long long) x_61_1;
  /*-- pattern --*/
  r2_245_1 = _Asm_extr_u(r1_245_1, 37, 11);
  rB_244_1 = (*pull = r2_245_1);
  /*-------------*/
  /*-- pattern --*/
  rA_244_1 = _Asm_extr_u(rB_244_1, 51, 63);
  rC_244_1 = _Asm_extr_u(rB_244_1, 35, 42);
  r3_231_1 = _Asm_dep_variable(rA_244_1, rC_244_1, 45, 10);
  /*-------------*/
  x_90_1 = _Asm_dep_fixed(-1, r3_231_1, 13, 41);
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_65()
{
  init_globals();

  int _retval;
  unsigned long long r1_245_1;
  unsigned long long r2_245_1;
  unsigned long long r3_231_1;
  unsigned long long rA_244_1;
  unsigned long long rB_244_1;
  unsigned long long rC_244_1;
  unsigned short x_61_1;
  unsigned long long x_90_1;
  srand48(941512650);
  x_61_1 = 2;
  r1_245_1 = (unsigned long long) x_61_1;
  /*-- pattern --*/
  r2_245_1 = _Asm_extr_u(r1_245_1, 37, 11);
  rB_244_1 = (*pull = r2_245_1);
  /*-------------*/
  /*-- pattern --*/
  rA_244_1 = _Asm_extr_u(rB_244_1, 51, 63);
  rC_244_1 = _Asm_extr_u(rB_244_1, 35, 42);
  r3_231_1 = _Asm_dep_variable(rA_244_1, rC_244_1, 45, 10);
  /*-------------*/
  x_90_1 = _Asm_dep_fixed(-1, r3_231_1, 13, 41);
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_66()
{
  init_globals();

  int _retval;
  int x_130_1;
  int x_203_1;
  unsigned char x_42_1;
  long long x_95_1;
  srand48(941512650);
  x_95_1 = 2;
  x_42_1 = (unsigned char) x_95_1;
  x_130_1 = (int) x_42_1;
  x_203_1 = x_130_1 + -3;
  _retval = (x_203_1 & (1 << 2)) ? x_203_1 : -1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_66()
{
  init_globals();

  int _retval;
  int x_130_1;
  int x_203_1;
  unsigned char x_42_1;
  long long x_95_1;
  srand48(941512650);
  x_95_1 = 2;
  x_42_1 = (unsigned char) x_95_1;
  x_130_1 = (int) x_42_1;
  x_203_1 = x_130_1 + -3;
  _retval = (x_203_1 & (1 << 2)) ? x_203_1 : -1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_67()
{
  init_globals();

  int _retval;
  int x_122_1;
  int x_162_1;
  int x_170_1;
  int x_227_1;
  unsigned int x_74_1;
  int y_122_1;
  srand48(941512650);
  x_122_1 = 0xffffffff;
  x_74_1 = 0xffffffff;
  x_227_1 = (int) x_74_1;
  *psi = x_227_1;
  x_162_1 = *psi;
  y_122_1 = x_162_1 & 0xff0000ff;
  x_170_1 = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  _retval = x_170_1 | 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_67()
{
  init_globals();

  int _retval;
  int x_122_1;
  int x_162_1;
  int x_170_1;
  int x_227_1;
  unsigned int x_74_1;
  int y_122_1;
  srand48(941512650);
  x_122_1 = 0xffffffff;
  x_74_1 = 0xffffffff;
  x_227_1 = (int) x_74_1;
  *psi = x_227_1;
  x_162_1 = *psi;
  y_122_1 = x_162_1 & 0xff0000ff;
  x_170_1 = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  _retval = x_170_1 | 0x00ffff00;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_68()
{
  init_globals();

  int _retval;
  unsigned long long x_90_1;
  srand48(941512650);
  x_90_1 = 0xfedcab9889abcdefULL;
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_68()
{
  init_globals();

  int _retval;
  unsigned long long x_90_1;
  srand48(941512650);
  x_90_1 = 0xfedcab9889abcdefULL;
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_69()
{
  init_globals();

  int _retval;
  long long x_116_1;
  long long x_164_1;
  long long x_205_1;
  long long x_221_1;
  long long x_98_1;
  long long y_116_1;
  srand48(941512650);
  x_116_1 = csll;
  x_205_1 = csll;
  y_116_1 = (x_205_1 & (1LL << 29)) ? x_205_1 : 1;
  x_221_1 = x_116_1 * y_116_1;
  x_164_1 = (x_221_1 != 5);
  x_98_1 = x_164_1 ^ 0xffff00000000ffffULL;
  _retval = (int) x_98_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_69()
{
  init_globals();

  int _retval;
  long long x_116_1;
  long long x_164_1;
  long long x_205_1;
  long long x_221_1;
  long long x_98_1;
  long long y_116_1;
  srand48(941512650);
  x_116_1 = csll;
  x_205_1 = csll;
  y_116_1 = (x_205_1 & (1LL << 29)) ? x_205_1 : 1;
  x_221_1 = x_116_1 * y_116_1;
  x_164_1 = (x_221_1 != 5);
  x_98_1 = x_164_1 ^ 0xffff00000000ffffULL;
  _retval = (int) x_98_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_70()
{
  init_globals();

  int _retval;
  unsigned long long r1_238_1;
  unsigned long long r2_238_1;
  unsigned long long r2_240_1;
  unsigned long long r3_238_1;
  unsigned long long rA_243_1;
  unsigned long long rC_243_1;
  unsigned long long x_133_1;
  unsigned long long x_90_1;
  unsigned long long x_93_1;
  srand48(941512650);
  /*-- pattern --*/
  r2_240_1 = (unsigned long long)*psc;
  r1_238_1 = _Asm_extr_u(r2_240_1, 0, 43);
  /*-------------*/
  r2_238_1 = 0xffffffffULL;
  /*-- pattern --*/
  r3_238_1 = _Asm_dep_variable(r1_238_1, r2_238_1, 5, 16);
  rC_243_1 = _Asm_extr(r3_238_1, 40, 12);
  /*-------------*/
  /*-- pattern --*/
# define this_pos 50
# define this_len 7
  rA_243_1 = _Asm_extr_u(rC_243_1, this_pos, this_len);
  x_93_1 = _Asm_dep_variable(rA_243_1, rC_243_1, this_pos, this_len);
# undef  this_len
# undef  this_pos
  /*-------------*/
  x_133_1 = (unsigned long long) x_93_1;
  x_90_1 = x_133_1 + 4;
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_70()
{
  init_globals();

  int _retval;
  unsigned long long r1_238_1;
  unsigned long long r2_238_1;
  unsigned long long r2_240_1;
  unsigned long long r3_238_1;
  unsigned long long rA_243_1;
  unsigned long long rC_243_1;
  unsigned long long x_133_1;
  unsigned long long x_90_1;
  unsigned long long x_93_1;
  srand48(941512650);
  /*-- pattern --*/
  r2_240_1 = (unsigned long long)*psc;
  r1_238_1 = _Asm_extr_u(r2_240_1, 0, 43);
  /*-------------*/
  r2_238_1 = 0xffffffffULL;
  /*-- pattern --*/
  r3_238_1 = _Asm_dep_variable(r1_238_1, r2_238_1, 5, 16);
  rC_243_1 = _Asm_extr(r3_238_1, 40, 12);
  /*-------------*/
  /*-- pattern --*/
# define this_pos 50
# define this_len 7
  rA_243_1 = _Asm_extr_u(rC_243_1, this_pos, this_len);
  x_93_1 = _Asm_dep_variable(rA_243_1, rC_243_1, this_pos, this_len);
# undef  this_len
# undef  this_pos
  /*-------------*/
  x_133_1 = (unsigned long long) x_93_1;
  x_90_1 = x_133_1 + 4;
  _retval = (int) x_90_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_71()
{
  init_globals();

  int _retval;
  long long x_108_1;
  long long x_140_1;
  int x_162_1;
  long long x_164_1;
  long long x_98_1;
  srand48(941512650);
  x_140_1 = 0x80000000LL;
  x_108_1 = x_140_1 * 6;
  x_164_1 = sll_call(x_108_1);
  x_98_1 = x_164_1 ^ 0xffff00000000ffffULL;
  x_162_1 = (int) x_98_1;
  _retval = x_162_1 & 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_71()
{
  init_globals();

  int _retval;
  long long x_108_1;
  long long x_140_1;
  int x_162_1;
  long long x_164_1;
  long long x_98_1;
  srand48(941512650);
  x_140_1 = 0x80000000LL;
  x_108_1 = x_140_1 * 6;
  x_164_1 = sll_call(x_108_1);
  x_98_1 = x_164_1 ^ 0xffff00000000ffffULL;
  x_162_1 = (int) x_98_1;
  _retval = x_162_1 & 0xff0000ff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_72()
{
  init_globals();

  int _retval;
  char x_158_1;
  int x_219_1;
  char x_223_1;
  char x_46_1;
  char x_50_1;
  srand48(941512650);
  x_46_1 = (char) 0x89;
  x_158_1 = (char) x_46_1;
  x_223_1 = x_158_1 | 0xc3;
  *psc = x_223_1;
  x_50_1 = *psc;
  x_219_1 = (int) x_50_1;
  _retval = (x_219_1 <= -2);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_72()
{
  init_globals();

  int _retval;
  char x_158_1;
  int x_219_1;
  char x_223_1;
  char x_46_1;
  char x_50_1;
  srand48(941512650);
  x_46_1 = (char) 0x89;
  x_158_1 = (char) x_46_1;
  x_223_1 = x_158_1 | 0xc3;
  *psc = x_223_1;
  x_50_1 = *psc;
  x_219_1 = (int) x_50_1;
  _retval = (x_219_1 <= -2);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_73()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_210_1;
  unsigned int x_74_1;
  int y_114_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0x7fffffff;
  x_74_1 = 0x80000000;
  y_210_1 = (int) x_74_1;
  x_114_1 = (x_210_1 == y_210_1) ? x_210_1 : y_210_1;
  y_114_1 = 0x7fffffff;
  _retval = x_114_1 - y_114_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_73()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_210_1;
  unsigned int x_74_1;
  int y_114_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = 0x7fffffff;
  x_74_1 = 0x80000000;
  y_210_1 = (int) x_74_1;
  x_114_1 = (x_210_1 == y_210_1) ? x_210_1 : y_210_1;
  y_114_1 = 0x7fffffff;
  _retval = x_114_1 - y_114_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_74()
{
  init_globals();

  int _retval;
  char x_110_1;
  unsigned int x_218_1;
  int x_219_1;
  char x_51_1;
  unsigned int x_74_1;
  char y_110_1;
  srand48(941512650);
  x_110_1 = csc;
  y_110_1 = 0;
  x_51_1 = x_110_1 - y_110_1;
  x_218_1 = (unsigned int) x_51_1;
  x_74_1 = (x_218_1 < 9);
  x_219_1 = (int) x_74_1;
  _retval = (x_219_1 > 2);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_74()
{
  init_globals();

  int _retval;
  char x_110_1;
  unsigned int x_218_1;
  int x_219_1;
  char x_51_1;
  unsigned int x_74_1;
  char y_110_1;
  srand48(941512650);
  x_110_1 = csc;
  y_110_1 = 0;
  x_51_1 = x_110_1 - y_110_1;
  x_218_1 = (unsigned int) x_51_1;
  x_74_1 = (x_218_1 < 9);
  x_219_1 = (int) x_74_1;
  _retval = (x_219_1 > 2);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_75()
{
  init_globals();

  int _retval;
  unsigned int x_171_1;
  unsigned int x_179_1;
  unsigned int x_74_1;
  srand48(941512650);
  x_171_1 = 0x7fffffff;
  x_179_1 = x_171_1 | 0x00ffff00;
  x_74_1 = x_179_1 >> 25;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_75()
{
  init_globals();

  int _retval;
  unsigned int x_171_1;
  unsigned int x_179_1;
  unsigned int x_74_1;
  srand48(941512650);
  x_171_1 = 0x7fffffff;
  x_179_1 = x_171_1 | 0x00ffff00;
  x_74_1 = x_179_1 >> 25;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_76()
{
  init_globals();

  int _retval;
  short x_144_1;
  short x_160_1;
  short x_201_1;
  short x_64_1;
  short x_66_1;
  srand48(941512650);
  x_160_1 = css;
  x_64_1 = x_160_1 & 0xf00f;
  x_144_1 = (short) x_64_1;
  x_201_1 = x_144_1 / 14;
  x_66_1 = (x_201_1 & (1 << 8)) ? x_201_1 : 3;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_76()
{
  init_globals();

  int _retval;
  short x_144_1;
  short x_160_1;
  short x_201_1;
  short x_64_1;
  short x_66_1;
  srand48(941512650);
  x_160_1 = css;
  x_64_1 = x_160_1 & 0xf00f;
  x_144_1 = (short) x_64_1;
  x_201_1 = x_144_1 / 14;
  x_66_1 = (x_201_1 & (1 << 8)) ? x_201_1 : 3;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_77()
{
  init_globals();

  int _retval;
  long long x_100_1;
  short x_168_1;
  short x_66_1;
  int x_82_1;
  long long x_96_1;
  srand48(941512650);
  x_100_1 = (long long) 0xfedcba9889abcdefULL;
  x_96_1 = (long long) x_100_1;
  x_168_1 = (short) x_96_1;
  x_66_1 = x_168_1 ^ 0x0ff0;
  x_82_1 = (int) x_66_1;
  _retval = (int) x_82_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_77()
{
  init_globals();

  int _retval;
  long long x_100_1;
  short x_168_1;
  short x_66_1;
  int x_82_1;
  long long x_96_1;
  srand48(941512650);
  x_100_1 = (long long) 0xfedcba9889abcdefULL;
  x_96_1 = (long long) x_100_1;
  x_168_1 = (short) x_96_1;
  x_66_1 = x_168_1 ^ 0x0ff0;
  x_82_1 = (int) x_66_1;
  _retval = (int) x_82_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_78()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0xffffffff;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_78()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0xffffffff;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_79()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x80000000;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_79()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = 0x80000000;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_80()
{
  init_globals();

  int _retval;
  int x_122_1;
  int y_122_1;
  srand48(941512650);
  x_122_1 = (int) 0x89abcdef;
  y_122_1 = x_122_1; /* join */
  _retval = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_80()
{
  init_globals();

  int _retval;
  int x_122_1;
  int y_122_1;
  srand48(941512650);
  x_122_1 = (int) 0x89abcdef;
  y_122_1 = x_122_1; /* join */
  _retval = (y_122_1 == 0 ? 0 : x_122_1 / y_122_1);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_81()
{
  init_globals();

  int _retval;
  short x_201_1;
  short x_208_1;
  unsigned char x_40_1;
  short x_63_1;
  short x_66_1;
  short y_208_1;
  srand48(941512650);
  x_208_1 = (short) 0x89ab;
  x_63_1 = css;
  x_40_1 = (unsigned char) x_63_1;
  x_201_1 = (short) x_40_1;
  y_208_1 = (x_201_1 & (1 << 12)) ? x_201_1 : 3;
  x_66_1 = (x_208_1 > y_208_1) ? x_208_1 : y_208_1;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_81()
{
  init_globals();

  int _retval;
  short x_201_1;
  short x_208_1;
  unsigned char x_40_1;
  short x_63_1;
  short x_66_1;
  short y_208_1;
  srand48(941512650);
  x_208_1 = (short) 0x89ab;
  x_63_1 = css;
  x_40_1 = (unsigned char) x_63_1;
  x_201_1 = (short) x_40_1;
  y_208_1 = (x_201_1 & (1 << 12)) ? x_201_1 : 3;
  x_66_1 = (x_208_1 > y_208_1) ? x_208_1 : y_208_1;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_82()
{
  init_globals();

  int _retval;
  unsigned int x_107_1;
  int x_146_1;
  int x_203_1;
  short x_66_1;
  unsigned int x_72_1;
  srand48(941512650);
  x_107_1 = 10;
  x_72_1 = ui_call(x_107_1);
  x_66_1 = (short) x_72_1;
  x_203_1 = (int) x_66_1;
  x_146_1 = (x_203_1 & (1 << 30)) ? x_203_1 : 5;
  _retval = x_146_1 / 14;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_82()
{
  init_globals();

  int _retval;
  unsigned int x_107_1;
  int x_146_1;
  int x_203_1;
  short x_66_1;
  unsigned int x_72_1;
  srand48(941512650);
  x_107_1 = 10;
  x_72_1 = ui_call(x_107_1);
  x_66_1 = (short) x_72_1;
  x_203_1 = (int) x_66_1;
  x_146_1 = (x_203_1 & (1 << 30)) ? x_203_1 : 5;
  _retval = x_146_1 / 14;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_83()
{
  init_globals();

  int _retval;
  long long x_124_1;
  int x_130_1;
  char x_52_1;
  long long x_94_1;
  long long x_98_1;
  long long y_124_1;
  srand48(941512650);
  x_124_1 = 0x7fffffffLL;
  y_124_1 = 0x80000000LL;
  x_94_1 = (y_124_1 == 0 ? 0 : x_124_1 / y_124_1);
  x_52_1 = (char) x_94_1;
  x_98_1 = (long long) x_52_1;
  x_130_1 = (int) x_98_1;
  _retval = x_130_1 + -5;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_83()
{
  init_globals();

  int _retval;
  long long x_124_1;
  int x_130_1;
  char x_52_1;
  long long x_94_1;
  long long x_98_1;
  long long y_124_1;
  srand48(941512650);
  x_124_1 = 0x7fffffffLL;
  y_124_1 = 0x80000000LL;
  x_94_1 = (y_124_1 == 0 ? 0 : x_124_1 / y_124_1);
  x_52_1 = (char) x_94_1;
  x_98_1 = (long long) x_52_1;
  x_130_1 = (int) x_98_1;
  _retval = x_130_1 + -5;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_84()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_203_1;
  int x_219_1;
  int x_82_1;
  srand48(941512650);
  x_106_1 = 0xffffffff;
  x_203_1 = si_call(x_106_1);
  x_82_1 = (x_203_1 & (1 << 10)) ? x_203_1 : -5;
  x_219_1 = (int) x_82_1;
  _retval = (x_219_1 > -2);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_84()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_203_1;
  int x_219_1;
  int x_82_1;
  srand48(941512650);
  x_106_1 = 0xffffffff;
  x_203_1 = si_call(x_106_1);
  x_82_1 = (x_203_1 & (1 << 10)) ? x_203_1 : -5;
  x_219_1 = (int) x_82_1;
  _retval = (x_219_1 > -2);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_85()
{
  init_globals();

  int _retval;
  int x_146_1;
  unsigned char x_42_1;
  unsigned short x_58_1;
  int x_81_1;
  int x_82_1;
  srand48(941512650);
  x_42_1 = 0x89;
  x_82_1 = (int) x_42_1;
  x_81_1 = (int) x_82_1;
  x_58_1 = (unsigned short) x_81_1;
  x_146_1 = (int) x_58_1;
  _retval = x_146_1 / 15;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_85()
{
  init_globals();

  int _retval;
  int x_146_1;
  unsigned char x_42_1;
  unsigned short x_58_1;
  int x_81_1;
  int x_82_1;
  srand48(941512650);
  x_42_1 = 0x89;
  x_82_1 = (int) x_42_1;
  x_81_1 = (int) x_82_1;
  x_58_1 = (unsigned short) x_81_1;
  x_146_1 = (int) x_58_1;
  _retval = x_146_1 / 15;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_86()
{
  init_globals();

  int _retval;
  unsigned char x_127_1;
  int x_146_1;
  unsigned char x_190_1;
  unsigned char x_214_1;
  unsigned char x_42_1;
  srand48(941512650);
  x_190_1 = 0x89;
  x_214_1 = (x_190_1 < 1) ? x_190_1 : 5;
  x_127_1 = (x_214_1 == 9);
  x_42_1 = x_127_1 + -1;
  x_146_1 = (int) x_42_1;
  _retval = x_146_1 / 8;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_86()
{
  init_globals();

  int _retval;
  unsigned char x_127_1;
  int x_146_1;
  unsigned char x_190_1;
  unsigned char x_214_1;
  unsigned char x_42_1;
  srand48(941512650);
  x_190_1 = 0x89;
  x_214_1 = (x_190_1 < 1) ? x_190_1 : 5;
  x_127_1 = (x_214_1 == 9);
  x_42_1 = x_127_1 + -1;
  x_146_1 = (int) x_42_1;
  _retval = x_146_1 / 8;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_87()
{
  init_globals();

  int _retval;
  unsigned char x_42_1;
  srand48(941512650);
  x_42_1 = 6;
  _retval = (int) x_42_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_87()
{
  init_globals();

  int _retval;
  unsigned char x_42_1;
  srand48(941512650);
  x_42_1 = 6;
  _retval = (int) x_42_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_88()
{
  init_globals();

  int _retval;
  short x_104_1;
  short x_168_1;
  short x_176_1;
  short x_217_1;
  short x_66_1;
  srand48(941512650);
  x_104_1 = (short) 0x89ab;
  x_168_1 = ss_call(x_104_1);
  x_217_1 = x_168_1 ^ 0x0ff0;
  x_176_1 = (x_217_1 < -3);
  x_66_1 = x_176_1 >> 9;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_88()
{
  init_globals();

  int _retval;
  short x_104_1;
  short x_168_1;
  short x_176_1;
  short x_217_1;
  short x_66_1;
  srand48(941512650);
  x_104_1 = (short) 0x89ab;
  x_168_1 = ss_call(x_104_1);
  x_217_1 = x_168_1 ^ 0x0ff0;
  x_176_1 = (x_217_1 < -3);
  x_66_1 = x_176_1 >> 9;
  _retval = (int) x_66_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_89()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = csi;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_89()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = csi;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_90()
{
  init_globals();

  int _retval;
  long long x_98_1;
  srand48(941512650);
  x_98_1 = 1;
  _retval = (int) x_98_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_90()
{
  init_globals();

  int _retval;
  long long x_98_1;
  srand48(941512650);
  x_98_1 = 1;
  _retval = (int) x_98_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_91()
{
  init_globals();

  int _retval;
  unsigned long long r2_241_1;
  unsigned int x_194_1;
  unsigned long long x_228_1;
  unsigned short x_59_1;
  unsigned int x_74_1;
  unsigned long long x_89_1;
  srand48(941512650);
  /*-- pattern --*/
  r2_241_1 = (unsigned long long)*psll;
  x_228_1 = _Asm_extr(r2_241_1, 6, 1);
  /*-------------*/
  *pull = x_228_1;
  x_89_1 = *pull;
  x_59_1 = (unsigned short) x_89_1;
  x_194_1 = (unsigned int) x_59_1;
  x_74_1 = (x_194_1 == 9) ? x_194_1 : 5;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_91()
{
  init_globals();

  int _retval;
  unsigned long long r2_241_1;
  unsigned int x_194_1;
  unsigned long long x_228_1;
  unsigned short x_59_1;
  unsigned int x_74_1;
  unsigned long long x_89_1;
  srand48(941512650);
  /*-- pattern --*/
  r2_241_1 = (unsigned long long)*psll;
  x_228_1 = _Asm_extr(r2_241_1, 6, 1);
  /*-------------*/
  *pull = x_228_1;
  x_89_1 = *pull;
  x_59_1 = (unsigned short) x_89_1;
  x_194_1 = (unsigned int) x_59_1;
  x_74_1 = (x_194_1 == 9) ? x_194_1 : 5;
  _retval = (int) x_74_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_92()
{
  init_globals();

  int _retval;
  int x_178_1;
  int x_195_1;
  srand48(941512650);
  x_195_1 = 0xffffffff;
  x_178_1 = (x_195_1 < 1) ? x_195_1 : 1;
  _retval = x_178_1 >> 8;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_92()
{
  init_globals();

  int _retval;
  int x_178_1;
  int x_195_1;
  srand48(941512650);
  x_195_1 = 0xffffffff;
  x_178_1 = (x_195_1 < 1) ? x_195_1 : 1;
  _retval = x_178_1 >> 8;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_93()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = -2;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_93()
{
  init_globals();

  int _retval;
  srand48(941512650);
  _retval = -2;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_94()
{
  init_globals();

  int _retval;
  int x_227_1;
  srand48(941512650);
  x_227_1 = 2;
  *psi = x_227_1;
  _retval = *psi;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_94()
{
  init_globals();

  int _retval;
  int x_227_1;
  srand48(941512650);
  x_227_1 = 2;
  *psi = x_227_1;
  _retval = *psi;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_95()
{
  init_globals();

  int _retval;
  unsigned char x_119_1;
  int x_154_1;
  unsigned long long x_165_1;
  unsigned char x_42_1;
  unsigned long long x_87_1;
  unsigned char y_119_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = (int) 0x89abcdef;
  x_119_1 = cuc;
  x_165_1 = 0xfedcab9889abcdefULL;
  x_87_1 = x_165_1 | 0xffff00000000ffffULL;
  y_119_1 = (unsigned char) x_87_1;
  x_42_1 = (y_119_1 == 0 ? 0 : x_119_1 / y_119_1);
  y_154_1 = (int) x_42_1;
  _retval = x_154_1 | y_154_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_95()
{
  init_globals();

  int _retval;
  unsigned char x_119_1;
  int x_154_1;
  unsigned long long x_165_1;
  unsigned char x_42_1;
  unsigned long long x_87_1;
  unsigned char y_119_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = (int) 0x89abcdef;
  x_119_1 = cuc;
  x_165_1 = 0xfedcab9889abcdefULL;
  x_87_1 = x_165_1 | 0xffff00000000ffffULL;
  y_119_1 = (unsigned char) x_87_1;
  x_42_1 = (y_119_1 == 0 ? 0 : x_119_1 / y_119_1);
  y_154_1 = (int) x_42_1;
  _retval = x_154_1 | y_154_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_96()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_138_1;
  int x_154_1;
  int y_114_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = 0xffffffff;
  y_154_1 = 0x7fffffff;
  x_114_1 = x_154_1 | y_154_1;
  y_114_1 = 0x80000000;
  x_138_1 = x_114_1 * y_114_1;
  _retval = x_138_1 * 12;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_96()
{
  init_globals();

  int _retval;
  int x_114_1;
  int x_138_1;
  int x_154_1;
  int y_114_1;
  int y_154_1;
  srand48(941512650);
  x_154_1 = 0xffffffff;
  y_154_1 = 0x7fffffff;
  x_114_1 = x_154_1 | y_154_1;
  y_114_1 = 0x80000000;
  x_138_1 = x_114_1 * y_114_1;
  _retval = x_138_1 * 12;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_97()
{
  init_globals();

  int _retval;
  unsigned short x_113_1;
  unsigned short x_137_1;
  unsigned short x_192_1;
  unsigned short x_224_1;
  unsigned short x_58_1;
  unsigned short y_113_1;
  srand48(941512650);
  x_113_1 = 0x89ab;
  y_113_1 = 3;
  x_224_1 = x_113_1 - y_113_1;
  *pus = x_224_1;
  x_137_1 = *pus;
  x_192_1 = x_137_1 * 7;
  x_58_1 = (x_192_1 <= 9) ? x_192_1 : 8;
  _retval = (int) x_58_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_97()
{
  init_globals();

  int _retval;
  unsigned short x_113_1;
  unsigned short x_137_1;
  unsigned short x_192_1;
  unsigned short x_224_1;
  unsigned short x_58_1;
  unsigned short y_113_1;
  srand48(941512650);
  x_113_1 = 0x89ab;
  y_113_1 = 3;
  x_224_1 = x_113_1 - y_113_1;
  *pus = x_224_1;
  x_137_1 = *pus;
  x_192_1 = x_137_1 * 7;
  x_58_1 = (x_192_1 <= 9) ? x_192_1 : 8;
  _retval = (int) x_58_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_98()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_138_1;
  int x_210_1;
  unsigned int x_211_1;
  unsigned int x_74_1;
  int y_210_1;
  unsigned int y_211_1;
  srand48(941512650);
  x_210_1 = (int) 0x89abcdef;
  x_211_1 = 0x7fffffff;
  y_211_1 = 0x7fffffff;
  x_74_1 = (x_211_1 <= y_211_1) ? x_211_1 : y_211_1;
  x_138_1 = (int) x_74_1;
  y_210_1 = x_138_1 * 5;
  x_106_1 = (x_210_1 == y_210_1) ? x_210_1 : y_210_1;
  _retval = si_call(x_106_1);
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_98()
{
  init_globals();

  int _retval;
  int x_106_1;
  int x_138_1;
  int x_210_1;
  unsigned int x_211_1;
  unsigned int x_74_1;
  int y_210_1;
  unsigned int y_211_1;
  srand48(941512650);
  x_210_1 = (int) 0x89abcdef;
  x_211_1 = 0x7fffffff;
  y_211_1 = 0x7fffffff;
  x_74_1 = (x_211_1 <= y_211_1) ? x_211_1 : y_211_1;
  x_138_1 = (int) x_74_1;
  y_210_1 = x_138_1 * 5;
  x_106_1 = (x_210_1 == y_210_1) ? x_210_1 : y_210_1;
  _retval = si_call(x_106_1);
  return _retval;
}

#pragma OPTIMIZE OFF
int test_99()
{
  init_globals();

  int _retval;
  int x_138_1;
  int x_154_1;
  int x_210_1;
  char x_50_1;
  int x_82_1;
  int y_154_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = (int) 0x89abcdef;
  x_154_1 = 0xffffffff;
  x_50_1 = -3;
  y_154_1 = (int) x_50_1;
  x_82_1 = x_154_1 & y_154_1;
  y_210_1 = (int) x_82_1;
  x_138_1 = (x_210_1 == y_210_1) ? x_210_1 : y_210_1;
  _retval = x_138_1 * 5;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_99()
{
  init_globals();

  int _retval;
  int x_138_1;
  int x_154_1;
  int x_210_1;
  char x_50_1;
  int x_82_1;
  int y_154_1;
  int y_210_1;
  srand48(941512650);
  x_210_1 = (int) 0x89abcdef;
  x_154_1 = 0xffffffff;
  x_50_1 = -3;
  y_154_1 = (int) x_50_1;
  x_82_1 = x_154_1 & y_154_1;
  y_210_1 = (int) x_82_1;
  x_138_1 = (x_210_1 == y_210_1) ? x_210_1 : y_210_1;
  _retval = x_138_1 * 5;
  return _retval;
}

#pragma OPTIMIZE OFF
int test_100()
{
  init_globals();

  int _retval;
  char x_126_1;
  char x_166_1;
  char x_50_1;
  short x_62_1;
  srand48(941512650);
  x_62_1 = css;
  x_166_1 = (char) x_62_1;
  x_126_1 = x_166_1 & 0x3c;
  x_50_1 = x_126_1 + 3;
  _retval = (int) x_50_1;
  return _retval;
}

#pragma OPTIMIZE ON
int test_opt_100()
{
  init_globals();

  int _retval;
  char x_126_1;
  char x_166_1;
  char x_50_1;
  short x_62_1;
  srand48(941512650);
  x_62_1 = css;
  x_166_1 = (char) x_62_1;
  x_126_1 = x_166_1 & 0x3c;
  x_50_1 = x_126_1 + 3;
  _retval = (int) x_50_1;
  return _retval;
}

#pragma OPTIMIZE OFF
int main()
{
   int failure = 0;

   if (test_1() != test_opt_1())
      { failure = 1; printf("Test 1 failed.\n"); }
   if (test_2() != test_opt_2())
      { failure = 1; printf("Test 2 failed.\n"); }
   if (test_3() != test_opt_3())
      { failure = 1; printf("Test 3 failed.\n"); }
   if (test_4() != test_opt_4())
      { failure = 1; printf("Test 4 failed.\n"); }
   if (test_5() != test_opt_5())
      { failure = 1; printf("Test 5 failed.\n"); }
   if (test_6() != test_opt_6())
      { failure = 1; printf("Test 6 failed.\n"); }
   if (test_7() != test_opt_7())
      { failure = 1; printf("Test 7 failed.\n"); }
   if (test_8() != test_opt_8())
      { failure = 1; printf("Test 8 failed.\n"); }
   if (test_9() != test_opt_9())
      { failure = 1; printf("Test 9 failed.\n"); }
   if (test_10() != test_opt_10())
      { failure = 1; printf("Test 10 failed.\n"); }
   if (test_11() != test_opt_11())
      { failure = 1; printf("Test 11 failed.\n"); }
   if (test_12() != test_opt_12())
      { failure = 1; printf("Test 12 failed.\n"); }
   if (test_13() != test_opt_13())
      { failure = 1; printf("Test 13 failed.\n"); }
   if (test_14() != test_opt_14())
      { failure = 1; printf("Test 14 failed.\n"); }
   if (test_15() != test_opt_15())
      { failure = 1; printf("Test 15 failed.\n"); }
   if (test_16() != test_opt_16())
      { failure = 1; printf("Test 16 failed.\n"); }
   if (test_17() != test_opt_17())
      { failure = 1; printf("Test 17 failed.\n"); }
   if (test_18() != test_opt_18())
      { failure = 1; printf("Test 18 failed.\n"); }
   if (test_19() != test_opt_19())
      { failure = 1; printf("Test 19 failed.\n"); }
   if (test_20() != test_opt_20())
      { failure = 1; printf("Test 20 failed.\n"); }
   if (test_21() != test_opt_21())
      { failure = 1; printf("Test 21 failed.\n"); }
   if (test_22() != test_opt_22())
      { failure = 1; printf("Test 22 failed.\n"); }
   if (test_23() != test_opt_23())
      { failure = 1; printf("Test 23 failed.\n"); }
   if (test_24() != test_opt_24())
      { failure = 1; printf("Test 24 failed.\n"); }
   if (test_25() != test_opt_25())
      { failure = 1; printf("Test 25 failed.\n"); }
   if (test_26() != test_opt_26())
      { failure = 1; printf("Test 26 failed.\n"); }
   if (test_27() != test_opt_27())
      { failure = 1; printf("Test 27 failed.\n"); }
   if (test_28() != test_opt_28())
      { failure = 1; printf("Test 28 failed.\n"); }
   if (test_29() != test_opt_29())
      { failure = 1; printf("Test 29 failed.\n"); }
   if (test_30() != test_opt_30())
      { failure = 1; printf("Test 30 failed.\n"); }
   if (test_31() != test_opt_31())
      { failure = 1; printf("Test 31 failed.\n"); }
   if (test_32() != test_opt_32())
      { failure = 1; printf("Test 32 failed.\n"); }
   if (test_33() != test_opt_33())
      { failure = 1; printf("Test 33 failed.\n"); }
   if (test_34() != test_opt_34())
      { failure = 1; printf("Test 34 failed.\n"); }
   if (test_35() != test_opt_35())
      { failure = 1; printf("Test 35 failed.\n"); }
   if (test_36() != test_opt_36())
      { failure = 1; printf("Test 36 failed.\n"); }
   if (test_37() != test_opt_37())
      { failure = 1; printf("Test 37 failed.\n"); }
   if (test_38() != test_opt_38())
      { failure = 1; printf("Test 38 failed.\n"); }
   if (test_39() != test_opt_39())
      { failure = 1; printf("Test 39 failed.\n"); }
   if (test_40() != test_opt_40())
      { failure = 1; printf("Test 40 failed.\n"); }
   if (test_41() != test_opt_41())
      { failure = 1; printf("Test 41 failed.\n"); }
   if (test_42() != test_opt_42())
      { failure = 1; printf("Test 42 failed.\n"); }
   if (test_43() != test_opt_43())
      { failure = 1; printf("Test 43 failed.\n"); }
   if (test_44() != test_opt_44())
      { failure = 1; printf("Test 44 failed.\n"); }
   if (test_45() != test_opt_45())
      { failure = 1; printf("Test 45 failed.\n"); }
   if (test_46() != test_opt_46())
      { failure = 1; printf("Test 46 failed.\n"); }
   if (test_47() != test_opt_47())
      { failure = 1; printf("Test 47 failed.\n"); }
   if (test_48() != test_opt_48())
      { failure = 1; printf("Test 48 failed.\n"); }
   if (test_49() != test_opt_49())
      { failure = 1; printf("Test 49 failed.\n"); }
   if (test_50() != test_opt_50())
      { failure = 1; printf("Test 50 failed.\n"); }
   if (test_51() != test_opt_51())
      { failure = 1; printf("Test 51 failed.\n"); }
   if (test_52() != test_opt_52())
      { failure = 1; printf("Test 52 failed.\n"); }
   if (test_53() != test_opt_53())
      { failure = 1; printf("Test 53 failed.\n"); }
   if (test_54() != test_opt_54())
      { failure = 1; printf("Test 54 failed.\n"); }
   if (test_55() != test_opt_55())
      { failure = 1; printf("Test 55 failed.\n"); }
   if (test_56() != test_opt_56())
      { failure = 1; printf("Test 56 failed.\n"); }
   if (test_57() != test_opt_57())
      { failure = 1; printf("Test 57 failed.\n"); }
   if (test_58() != test_opt_58())
      { failure = 1; printf("Test 58 failed.\n"); }
   if (test_59() != test_opt_59())
      { failure = 1; printf("Test 59 failed.\n"); }
   if (test_60() != test_opt_60())
      { failure = 1; printf("Test 60 failed.\n"); }
   if (test_61() != test_opt_61())
      { failure = 1; printf("Test 61 failed.\n"); }
   if (test_62() != test_opt_62())
      { failure = 1; printf("Test 62 failed.\n"); }
   if (test_63() != test_opt_63())
      { failure = 1; printf("Test 63 failed.\n"); }
   if (test_64() != test_opt_64())
      { failure = 1; printf("Test 64 failed.\n"); }
   if (test_65() != test_opt_65())
      { failure = 1; printf("Test 65 failed.\n"); }
   if (test_66() != test_opt_66())
      { failure = 1; printf("Test 66 failed.\n"); }
   if (test_67() != test_opt_67())
      { failure = 1; printf("Test 67 failed.\n"); }
   if (test_68() != test_opt_68())
      { failure = 1; printf("Test 68 failed.\n"); }
   if (test_69() != test_opt_69())
      { failure = 1; printf("Test 69 failed.\n"); }
   if (test_70() != test_opt_70())
      { failure = 1; printf("Test 70 failed.\n"); }
   if (test_71() != test_opt_71())
      { failure = 1; printf("Test 71 failed.\n"); }
   if (test_72() != test_opt_72())
      { failure = 1; printf("Test 72 failed.\n"); }
   if (test_73() != test_opt_73())
      { failure = 1; printf("Test 73 failed.\n"); }
   if (test_74() != test_opt_74())
      { failure = 1; printf("Test 74 failed.\n"); }
   if (test_75() != test_opt_75())
      { failure = 1; printf("Test 75 failed.\n"); }
   if (test_76() != test_opt_76())
      { failure = 1; printf("Test 76 failed.\n"); }
   if (test_77() != test_opt_77())
      { failure = 1; printf("Test 77 failed.\n"); }
   if (test_78() != test_opt_78())
      { failure = 1; printf("Test 78 failed.\n"); }
   if (test_79() != test_opt_79())
      { failure = 1; printf("Test 79 failed.\n"); }
   if (test_80() != test_opt_80())
      { failure = 1; printf("Test 80 failed.\n"); }
   if (test_81() != test_opt_81())
      { failure = 1; printf("Test 81 failed.\n"); }
   if (test_82() != test_opt_82())
      { failure = 1; printf("Test 82 failed.\n"); }
   if (test_83() != test_opt_83())
      { failure = 1; printf("Test 83 failed.\n"); }
   if (test_84() != test_opt_84())
      { failure = 1; printf("Test 84 failed.\n"); }
   if (test_85() != test_opt_85())
      { failure = 1; printf("Test 85 failed.\n"); }
   if (test_86() != test_opt_86())
      { failure = 1; printf("Test 86 failed.\n"); }
   if (test_87() != test_opt_87())
      { failure = 1; printf("Test 87 failed.\n"); }
   if (test_88() != test_opt_88())
      { failure = 1; printf("Test 88 failed.\n"); }
   if (test_89() != test_opt_89())
      { failure = 1; printf("Test 89 failed.\n"); }
   if (test_90() != test_opt_90())
      { failure = 1; printf("Test 90 failed.\n"); }
   if (test_91() != test_opt_91())
      { failure = 1; printf("Test 91 failed.\n"); }
   if (test_92() != test_opt_92())
      { failure = 1; printf("Test 92 failed.\n"); }
   if (test_93() != test_opt_93())
      { failure = 1; printf("Test 93 failed.\n"); }
   if (test_94() != test_opt_94())
      { failure = 1; printf("Test 94 failed.\n"); }
   if (test_95() != test_opt_95())
      { failure = 1; printf("Test 95 failed.\n"); }
   if (test_96() != test_opt_96())
      { failure = 1; printf("Test 96 failed.\n"); }
   if (test_97() != test_opt_97())
      { failure = 1; printf("Test 97 failed.\n"); }
   if (test_98() != test_opt_98())
      { failure = 1; printf("Test 98 failed.\n"); }
   if (test_99() != test_opt_99())
      { failure = 1; printf("Test 99 failed.\n"); }
   if (test_100() != test_opt_100())
      { failure = 1; printf("Test 100 failed.\n"); }

   return failure;
}
