#include <stdlib.h>

void f3(int f3_arg)
{
   int f3_var = 34;
   printf("f3_var: %d, f3_arg: %d\n", f3_var, f3_arg);
}

void f2(int f2_arg)
{
  int f2_var = 24;
  f3(f2_arg);
} 


void f1(int f1_arg)
{
  int f1_var = 14;
  f2(f1_arg);
} 


 int main() {
     int vari = 3;
     int varj = 4;
     int vark = 5;
     f1(vark); 
 }

