subroutine parent(i)
  integer i
  integer n
  integer k
  equivalence (n,k)

  n = i
  print *,"parent: i =", i
  call child_sub(n)
  print *, "child_fun = ",child_fun(n), "n = ", n
  print *,"parent: i = ", i, "k = n = ", k

  contains
    subroutine child_sub(j)
      integer j
      i= j + 7
      print *, "child_sub: i = ", i, "n = ", n
    end subroutine
    function child_fun(j)
      integer j, child_fun
      i = j + 7
      child_fun = i
      print *, "child_fun: i = ", i, "n = ", n
    end function 
end subroutine

program main
  implicit none
  integer i
  i = 13
  call parent(i)
end

