#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

void func(void);
int main()
{
  func(); 
  return 0;
}

void func(void)
{
  char filename[]="/tmp/QXCR1000559656_1_text";
  char filename1[]="/tmp/QXCR1000559656_1_text1";
  char buffer[100];
  int i, fd = -1, fd1 = -1, fd2 = -1;
  FILE *file = NULL, *file1 = NULL, *file2 = NULL;

  fd = creat (filename, 0666);
  if (fd > -1)
    printf("creat passed !!!\n");
  else
    {
      printf("creat failed !!!\n");
      return;
    }
  fd1 = open (filename1, O_RDONLY);
  file1 = fdopen (fd1, "r");

  if (fd1 > -1)
    printf("open passed 1 !!!\n");
  else
    {
      printf("open failed 1 !!!\n");
      return;
    }

  fd2 = dup(fd1); 
  file2 = fdopen (fd2, "r");

  if (fd2 > -1 && file2 != NULL)
    printf("dup passed 2 !!!\n");
  else
    {
      printf("dup failed 2 !!!\n");
      return;
    }

  close (fd1);
  fclose (file1);

  fd1 = dup2(fd2, 10);
  file1 = fdopen (fd1, "r");

  if (fd1 > -1 && file1 != NULL)
    printf("dup2 passed 3 !!!\n");
  else
    {
      printf("dup2 failed 3 !!!\n");
      return;
    }

  fd1 = open (filename1, O_RDONLY);
  if (fd1 > -1)
    printf("open passed 4 !!!\n");
  else
    {
      printf("open failed 4 !!!\n");
      return;
    }

  fd = open (filename, O_RDONLY);
  if (fd > -1)
    printf("open passed 5 !!!\n");
  else
    {
      printf("open failed 5 !!!\n");
      return;
    }

  fd1 = dup2(fd2, fd);
  if (fd1 > -1)
    printf("dup2 passed 6 !!!\n");
  else
    {
      printf("dup2 failed 6 !!!\n");
      return;
    }
}
