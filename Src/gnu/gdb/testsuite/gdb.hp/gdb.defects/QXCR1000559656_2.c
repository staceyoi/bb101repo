#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

void func(void);
int main()
{
  func(); 
  return 0;
}

void func(void)
{
  char filename[]="/tmp/QXCR1000559656_2_text";
  char filename1[]="/tmp/QXCR1000559656_2_text1";
  char buffer[100];
  int i, fd = -1, fd1 = -1, fd2 = -1;
  FILE *file = NULL, *file1 = NULL, *file2 = NULL;

  fd = creat (filename, 0666);
  if (fd > -1)
    printf("creat passed !!!\n");
  else
    {
      printf("creat failed !!!\n");
      return;
    }

  file1 = fopen( filename1, "r");
  fd1 = fileno(file1);

  if (file1 != NULL)
    printf("fopen passed 1 !!!\n");
  else
    {
      printf("fopen failed 1 !!!\n");
      return;
    }

  fd2 = dup(fd1); 
  file2 = fdopen (fd2, "r");
  file2 = freopen(filename, "r", file2);

  if (fd2 > -1 && file2 != NULL)
    printf("dup freopen passed 2 !!!\n");
  else
    {
      printf("dup freopen failed 2 !!!\n");
      return;
    }

  close (fd1);
  fclose (file1);

  fd1 = fcntl (fd2, F_DUPFD, 10);
  file1 = fdopen (fd1, "r");
  file1 = freopen(filename1, "r", file1);

  if (fd1 > -1 && file1 != NULL)
    printf("fcntl passed 3 !!!\n");
  else
    {
      printf("fcntl failed 3 !!!\n");
      return;
    }

  file1 = fopen( filename1, "r");
  if (file1 != NULL)
    printf("fopen passed 4 !!!\n");
  else
    {
      printf("fopen failed 4 !!!\n");
      return;
    }

  file = fopen( filename, "r");
  if (file != NULL)
    printf("fopen passed 5 !!!\n");
  else
    {
      printf("fopen failed 5 !!!\n");
      return;
    }

  fd1 = fcntl (fd2, F_DUPFD, fd);
  if (fd1 > -1)
    printf("fcntl passed 6 !!!\n");
  else
    {
      printf("fcntl failed 6 !!!\n");
      return;
    }
}
