/*
  * sig_and_fork.c
  */
 
 #include <stdio.h>
 #include <signal.h>
 #include <stdlib.h>
 #include <fcntl.h>
 #include <sys/wait.h>
 #include <time.h>
 #include <sys/stat.h>
 
 #define OUR_SIGNAL  SIGUSR1
 #define CHILD_MSG   "[child] fork worked fine!\n"
 
 /*
  * Filename to which the child process should write
  * its message
  */
 static const char*  global_output_filename = NULL;
 
 
 static void
 signal_handler (int sig)
 {
   int pid;
 
   printf("Interrupt handler for interrupts out of user code\n");
   fflush(stdout);
 
   printf("do the fork() from signal handler\n");
 
   pid = fork();
   if (0 == pid)
   {
     int  status;
     int  fd;
 
     fd = open(global_output_filename,
               O_WRONLY | O_CREAT | O_TRUNC,
               S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
     if (-1 == fd)
     {
       fprintf(stderr, "open() failed in child [%s]\n",
               global_output_filename);
       exit(2);
     }
 
     write(fd, CHILD_MSG, sizeof(CHILD_MSG) -1);
 
     status = close(fd);
     if (0 != status)
     {
       fprintf(stderr, "close() failed child\n");
       exit(3);
     }
   }
   else
   {
     int  stat;
 
     waitpid(pid, &stat, 0);
 
     if (! (WIFEXITED(stat) && (0 == WEXITSTATUS(stat))))
     {
       fprintf(stderr,
               "waitpid returned %d, WIFEXITED: %d, WEXITSTATUS: %d\n",
               stat, WIFEXITED(stat), WEXITSTATUS(stat));
     }
 
     sleep(1);
     printf("[parent] back from fork(), still in signal handler\n");
   }
 
   printf("Test completed in handler\n\n\n");
   fflush(stdout);
   exit(0);
 }
 
 
 extern int
 main (
   int                argc,
   const char* const  argv[]
 )
 {
   unsigned int sleep_time = 5;
 
   if (argc != 2)
   {
     fprintf(stderr, "%s usage:\n  %s <child_output_file>\n",
             argv[0], argv[0]);
     exit(1);
   }
 
   global_output_filename = argv[1];
 
   do {
     sleep_time = sleep(sleep_time);
   } while (sleep_time);
 
   signal(OUR_SIGNAL, signal_handler);
 
   printf("Test 0 : main() Prior to raising the signal.\n");
   fflush(stdout);
 
   printf("Test 1: main() is raising the signal [signo: %d]\n", OUR_SIGNAL);
   printf("The 'interrupted user' handler should catch this one.\n");
   fflush(stdout);
   raise(OUR_SIGNAL);
 
   return 0;
 }

