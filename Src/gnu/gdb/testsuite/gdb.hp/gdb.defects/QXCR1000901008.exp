#   Copyright (C) 1988, 1990, 1991, 1992, 1994, 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Test operations leading to QXCR1000901008: "set logging overwrite" command Dumps core.
# Source file : no source file. 
#

if { ![istarget "ia64*-hp*-*"] } {
    verbose "The test is not for PA."
    return 0
}

if $tracelevel then {
    strace $tracelevel
}

set testfile "QXCR1000901008"
set logfile ${testfile}.log
set log ${objdir}/${subdir}/${logfile}

if {($multipass_name  == 5)} {

  # run it for single pass.
  gdb_exit
  gdb_start
  #gdb_reinitialize_dir $srcdir/$subdir
  
  # Check the initial state
  gdb_test "show logging" ".*off.*gdb_output.*append.*only to the log file.*" "show logging"
  gdb_test "show redirect" ".*redirection.*off.*file.*gdb_output.*mode.*append.*" "show redirect"
  gdb_test "show logging overwrite" ".*logging overwrites or appends.*off.*" "show logging overwrite"
  gdb_test "show logging redirect" ".*logging output mode.*on.*" "show logging redirect"
    
  gdb_test "set logging file ${log}" "" "set logging file ${log}"
  gdb_test "set logging overwrite" "" "set logging overwrite"
  gdb_test "set logging redirect" "" "set logging redirect"
  # Check the current state
  gdb_test "show logging" ".*off.*${log}.*overwrite.*only to the log file.*" "show logging"
  gdb_test "show redirect" ".*redirection.*off.*file.*${log}.*mode.*overwrite.*" "show redirect"
  gdb_test "show logging overwrite" ".*logging overwrites or appends.*on.*" "show logging overwrite"
  gdb_test "show logging redirect" ".*logging output mode.*on.*" "show logging redirect"

  gdb_test "set logging overwrite off" "" "set logging overwrite off"
  gdb_test "set logging redirect off" "" "set logging redirect off"
  # Check the current state
  gdb_test "show logging" ".*off.*${log}.*append.*log file and display.*" "show logging"
  gdb_test "show redirect" ".*redirection.*off.*file.*${log}.*mode.*append.*" "show redirect"
  gdb_test "show logging overwrite" ".*logging overwrites or appends.*off.*" "show logging overwrite"
  gdb_test "show logging redirect" ".*logging output mode.*off.*" "show logging redirect"

  gdb_test "set logging overwrite on" "" "set logging overwrite on"
  gdb_test "set logging redirect on" "" "set logging redirect on"
  # Check the current state
  gdb_test "show logging" ".*off.*${log}.*overwrite.*only to the log file.*" "show logging"
  gdb_test "show redirect" ".*redirection.*off.*file.*${log}.*mode.*overwrite.*" "show redirect"
  gdb_test "show logging overwrite" ".*logging overwrites or appends.*on.*" "show logging overwrite"
  gdb_test "show logging redirect" ".*logging output mode.*on.*" "show logging redirect"
}

gdb_exit

# clean up the temp files
remote_exec build "/bin/rm -f $log"

return 0
