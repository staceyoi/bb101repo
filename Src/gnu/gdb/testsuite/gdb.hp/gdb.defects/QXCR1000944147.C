#include <stdlib.h>

struct alpha {
public:
  int a;
  void b(void);
};

void alpha::b(void)
{
  printf("I am in alpha.b!!\n");
}

void func(alpha *pA)
{
  printf("I am in func!!\n");
}

int main()
{
   alpha A;
   A.a = 100;
   A.b();
   func(&A);
   return 0;
}
