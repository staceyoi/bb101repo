// The following code duplicates an issue with unaligned accesses and
// watchpoints.
//
// Compile this as:
//    cc -g QXCR1001054246.c
//
// The resulting a.out should run with no errors.  You can also verify
// that it runs under gdb with no errors, except when a watchpoint is
// set.  Do the following to reproduce the error:
//
// gdb a.out
// (gdb) break main
// (gdb) run
// (gdb) watch global_variable
// (gdb) cont
// (gdb) cont
// A SIGSEGV is caught here for the instruction performing the
// second unaligned write.  The first unaligned write spans an
// eight-byte boundary and doesn't trigger the problem.  The second
// spans a 16-byte boundary and does.
//
////////////////////////////////////////////////////////////////////////

#include <machine/sys/inline.h>

static void AllowUnaligned() {
   unsigned long long um = _Asm_mov_from_um();
   _Asm_mov_to_um(um & ~8LL);
}

#pragma init "arm_unaligned_handler"
static void arm_unaligned_handler() {
   AllowUnaligned();
}

char arr[1024];
int global_variable;   /* SET WATCHPOINT HERE */

static void do_unaligned_access()
{
   int *p_i;

   /* OK */
   p_i = (int *)&arr[5];
   (*p_i)++;

   /* CRASHES */
   p_i = (int *)&arr[13];
   (*p_i)++;
}

int main()
{
  global_variable = 1;
  do_unaligned_access();
}

