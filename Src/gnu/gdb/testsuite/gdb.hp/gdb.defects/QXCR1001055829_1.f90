program main
  implicit none
  integer func
  integer i

  i = 10
  i = func()
  call sub()
end

integer function func()
  integer, save :: i = 20

  print *,"func: i =", i
  func = child_func()

  contains
    integer function child_func()
      integer, save :: i = 30

      print *,"child_func: i =", i
      child_func = i
      return
    end function

end function
