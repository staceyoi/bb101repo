subroutine sub()
  integer, save :: i = 40

  print *,"sub: i =", i
  call child_sub()

  contains
    subroutine child_sub()
      integer, save :: i = 50

      print *,"child_sub: i =", i
    end subroutine

end subroutine
