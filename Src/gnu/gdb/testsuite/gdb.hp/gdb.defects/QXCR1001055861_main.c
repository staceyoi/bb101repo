// watchpoints are re-enabled after being disabled
//
// To build test:
//     cc -b -g -o QXCR1001055861_lib.so QXCR1001055861_lib.c
//     cc -g QXCR1001055861_main.c QXCR1001055861_lib.so
//
// Then run:
//     gdb a.out
//     (gdb) break save
//     (gdb) run
//     (gdb) watch savearr
//     (gdb) disable 2       <<-- the watchpoint
//     (gdb) disable 1       <<-- the breakpoint
//     (gdb) run             <<-- restart (hits watchpoint)
//     (gdb) info watch      <<-- break disabled, but watch is enabled
//
/////////////////////////////////////////////////////////////////

#include <stdlib.h>

extern void save(int i);

int main(){
   int i;
   for (i=0; i<10; i++) {
       printf("i=%d\n", i);
       save(i);
   } 
   return 0;
}
