// b foo
// run
// watch res
// b bar
// cont
#include <stdlib.h>
#include <stdio.h>
int main(){
    foo();
    bar();
    return 0;
}
int foo(){
    int * res = malloc(4);
    printf("*res %d\n", *res);
    *res = 20;
    return 1;
}
int bar(){
    printf("Hello\n");
    return 2;
}

