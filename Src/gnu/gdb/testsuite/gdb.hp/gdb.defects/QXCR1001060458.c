#include<stdio.h>
#include<stdlib.h>

int func(int i)
{
  return(i);
}

int main()
{
  int (*f_ptr)(int) = func;
  void * (*m_ptr) (size_t ) = malloc;
  printf("%d\n", f_ptr(10));
  return 0;
}
