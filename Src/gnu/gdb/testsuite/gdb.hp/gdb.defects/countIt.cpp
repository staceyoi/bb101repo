#include "countIt.h"

void countIt(char* string, int depth)
{
	int i;
	char indent[128];

	for(i=0; i < depth; i++)
		strcat(indent, "+ ");

	if(depth < X)
	{
		for(i=0; i<strlen(string); i++)
			cout << indent << i << endl;
		countIt(string, depth+1);
	}
	else
		cout << "goodbye, cruel..." << endl;
}

