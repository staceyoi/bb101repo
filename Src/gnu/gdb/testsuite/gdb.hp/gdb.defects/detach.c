#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

void *thr(void *d) {
    while (1);
}

void main(void) {
    pid_t pid;
    int stat_loc;
    pthread_t tid;
    
    printf("Creating thread...\n");
    pthread_create(&tid, NULL, thr, NULL);

    printf("Main thread\n");
    pthread_join(tid, NULL);

    printf("Program done\n");
    exit (0);
}
