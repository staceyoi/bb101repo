.file   "foo.c"
        .psr    msb
        .radix  C
        .pred.safe_across_calls p1-p5,p16-p63

        .text
        .align  16
        .proc   foo
foo::
        .unwentry
        nop.m           0                          // M
        nop.m           0                          // M
        br.ret.dptk.few rp                      ;; // B [foo.c: 3/1]
        .endp   foo
