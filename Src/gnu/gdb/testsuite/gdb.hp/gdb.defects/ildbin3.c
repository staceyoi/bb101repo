int main_gvar_prepad = 19;
int main_gvar = 8;
int main_gvar_postpad = 19;

const int main_gconst_prepad = 32;
const int main_gconst = 16;
const int main_gconst_postpad = 32;

typedef int main_typedef_prepad;
typedef float main_typedef;
typedef int main_typedef_postpad;

struct main_struct_prepad {
  int main_field;
};

struct main_struct {
  int main_changed_field;
};
struct main_struct mainstructure;
struct main_struct_postpad {
  int main_field;
};

main()
{
    main_gvar = square (main_gvar);
    gdb_halt_here ();
}

gdb_halt_here ()
{
    exit (0);
    main_typedef usehere;
    usehere = 10;
}
