#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int alarm_received = 0;

handler()
{
    alarm_received = 1;
}


void *thr(void *d) {
    while (!alarm_received);
    printf ("Received alarm\n");
}

void main(void) {
    pid_t pid;
    int stat_loc;
    pthread_t tid;
    
    signal (SIGALRM, (void (*) (int)) handler);  
    printf("Creating thread...\n");
    pthread_create(&tid, NULL, thr, NULL);

    pthread_kill (pthread_self(), SIGALRM);
    printf("Main thread\n");

    pthread_join(tid, NULL);

    printf("Program done\n");
    exit (0);
}
