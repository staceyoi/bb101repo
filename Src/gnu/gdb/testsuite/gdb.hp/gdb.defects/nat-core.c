#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

/* This flag controls termination of the main loop. */
volatile int keep_going = 10;

void call_abort()
{
 printf ("Calling abort\n" );
 abort();
}


void
catch_sig (int sig)
{
  call_abort();
}

void
do_stuff ()

{
keep_going--;
}

int
main (void)
{
  /*Establish a handler for SIGALRM signals.*/
  signal (SIGSEGV, catch_sig);
 
 /* Check the flag once in a while to see when to quit.  */
  while (keep_going > 0)
    do_stuff ();
  /* Send signal to go off.*/
  raise (SIGSEGV);
  return 0;
}

