	.file	"main.c"
	.psr	msb
	.radix	C
	.pred.safe_across_calls p1-p5,p16-p63
	.type	foo,@function
	.global	foo
	.type	main,@function
	.global	main
	.type	printf,@function
	.global	printf
	.type	a,@object
	.global	a
	.size	a, 40
	.size	foo, 880
// Routine [id=0001] ( foo )

// ===
	.secalias .abe$4.text, ".text"
	.section .abe$4.text = "ax", "progbits"
	.align	32
        break.m         0x6b
        break.m         0x72
        break.i         0x6e
	.proc	foo
..L0:
//      $start          CMid906 =               ;; // A

..L2:
foo::
.prologue
//      $entry          CMid907 =                  // A [main.c: 4/1]
.body
.label_state 1

//file/line/col main.c/4/1
        add             r10 = @ltoffx(a), gp       // M [main.c: 8/13]
//file/line/col main.c/6/9[2]
        add             r8 = 0, r0              ;; // I
        nop.i           0                          // I
        ldxmov          r10 = [r10], a#         ;; // M [main.c: 8/13]
        add             r11 = 4, r10               // M
        add             r10 = 8, r10            ;; // I
        ld4.s           r20 = [r11], -4            // M [main.c: 8/13]
        ld4.s           r19 = [r10], 4             // M [main.c: 8/13]
        nop.i           0                       ;; // I
        ld4             r21 = [r11], 16            // M [main.c: 8/13]
        ld4.s           r18 = [r10], 8             // M [main.c: 8/13]
        cmp4.eq.unc     p7, p0 = 1, r20         ;; // I [main.c: 8/13]
        ld4.s           r17 = [r11], 8             // M [main.c: 8/13]
        ld4.s           r16 = [r10], 8             // M [main.c: 8/13]
        cmp4.eq.unc     p6, p0 = 1, r21            // I [main.c: 8/13]
//file/line/col main.c/6/36/D2,7/17,8/13/E
        nop.m           0                          // M
        nop.m           0                          // M
.restore sp
(p6)    br.ret.dpnt.many rp                     ;; // B [main.c: 8/13]

..L25:
.body
.copy_state 1

//file/line/col main.c/8/13/D1/E[2]
        ld4.s           r15 = [r11], 8             // M [main.c: 8/13]
        ld4.s           r14 = [r10], 8             // M [main.c: 8/13]
        cmp4.eq.unc     p8, p0 = 2, r21            // I [main.c: 8/13]
        cmp4.eq.unc     p6, p0 = 1, r19            // M [main.c: 8/13]
        nop.m           0                          // M
(p8)    br.dpnt.many    ..L55                   ;; // B [main.c: 8/13]

..L35:
//file/line/col main.c/8/13/D2/E[2]
        ld4.s           r22 = [r11]                // M [main.c: 8/13]
        ld4.s           r11 = [r10]                // M [main.c: 8/13]
        cmp4.eq.unc     p8, p0 = 20, r21           // I [main.c: 8/13]
        nop.m           0                          // M
        nop.m           0                          // M
(p8)    br.dpnt.many    ..L75                   ;; // B [main.c: 8/13]

..L45:
//file/line/col main.c/6/9/D3
        chk.s.m         r20, ..L96                 // M
        add             r8 = 1, r0                 // M [main.c: 6/9]
.restore sp
(p7)    br.ret.dpnt.many rp                     ;; // B [main.c: 8/13]

..L26:
.body
.copy_state 1

//file/line/col main.c/8/13/D1/E[2]
        cmp4.eq.unc     p7, p0 = 2, r20         ;; // M [main.c: 8/13]
        cmp4.eq.or      p7, p0 = 20, r20           // M [main.c: 8/13]
//file/line/col main.c/8/13/D2/E[2]
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
.restore sp
(p7)    br.ret.dpnt.many rp                     ;; // B [main.c: 8/13]

..L46:
.body
.copy_state 1

//file/line/col main.c/6/9/D3
        chk.s.m         r19, ..L97                 // M
        add             r8 = 2, r0                 // M [main.c: 6/9]
.restore sp
(p6)    br.ret.dpnt.many rp                     ;; // B [main.c: 8/13]

..L27:
.body
.copy_state 1

//file/line/col main.c/8/13/D1/E[2]
        cmp4.eq.unc     p6, p0 = 2, r19         ;; // M [main.c: 8/13]
        cmp4.eq.or      p6, p0 = 20, r19           // M [main.c: 8/13]
//file/line/col main.c/8/13/D2/E[2]
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
.restore sp
(p6)    br.ret.dpnt.many rp                     ;; // B [main.c: 8/13]

..L47:
.body
.copy_state 1

//file/line/col main.c/6/9/D3
        chk.s.m         r18, ..L98                 // M
        cmp4.eq.unc     p0, p8 = 1, r18            // M [main.c: 8/13]
        cmp4.eq.unc     p6, p7 = 1, r18            // I [main.c: 8/13]

..L105:
        add             r8 = 3, r0              ;; // M [main.c: 6/9]
        cmp4.eq.andcm   p8, p7 = 20, r18           // M [main.c: 8/13]
        cmp4.eq.andcm   p8, p7 = 2, r18         ;; // I [main.c: 8/13]
(p8)    chk.s.m         r17, ..L99                 // M
        cmp4.eq.andcm   p0, p7 = 20, r17           // M [main.c: 8/13]
        cmp4.eq.andcm   p0, p7 = 1, r17            // I [main.c: 8/13]

..L106:
        cmp4.eq.andcm   p0, p7 = 2, r17            // M [main.c: 8/13]
(p8)    add             r8 = 4, r0                 // M [main.c: 6/9]
(p8)    cmp4.eq.or      p6, p0 = 1, r17         ;; // I [main.c: 8/13]
(p7)    chk.s.m         r16, ..L100                // M
(p7)    cmp4.eq.unc     p0, p9 = 1, r16            // M [main.c: 8/13]
(p7)    add             r8 = 5, r0                 // I [main.c: 6/9]

..L107:
(p7)    cmp4.eq.or      p6, p0 = 1, r16            // M [main.c: 8/13]
        nop.i           0                       ;; // I
(p9)    cmp4.eq.unc     p0, p7 = 2, r16         ;; // I [main.c: 8/13]
//file/line/col main.c/8/13/D1/E[2],8/13/D2/E[2],6/9/D3,8/13/D1/E[2],8/13/D2/E[2],6/9/D3
(p7)    cmp4.eq.unc     p0, p7 = 20, r16        ;; // M [main.c: 8/13]
(p7)    chk.s.m         r15, ..L101                // M
(p7)    cmp4.eq.unc     p0, p8 = 1, r15            // I [main.c: 8/13]

..L108:
(p7)    add             r8 = 6, r0                 // M [main.c: 6/9]
(p7)    cmp4.eq.or      p6, p0 = 1, r15         ;; // I [main.c: 8/13]
(p8)    cmp4.eq.unc     p0, p7 = 2, r15         ;; // I [main.c: 8/13]
//file/line/col main.c/8/13/D1/E[2],8/13/D2/E[2],6/9/D3
(p7)    cmp4.eq.unc     p0, p7 = 20, r15        ;; // M [main.c: 8/13]
(p7)    chk.s.m         r14, ..L102                // M
(p7)    cmp4.eq.unc     p0, p8 = 1, r14            // I [main.c: 8/13]

..L109:
(p7)    add             r8 = 7, r0                 // M [main.c: 6/9]
(p7)    cmp4.eq.or      p6, p0 = 1, r14         ;; // I [main.c: 8/13]
(p8)    cmp4.eq.unc     p0, p7 = 2, r14         ;; // I [main.c: 8/13]
//file/line/col main.c/8/13/D1/E[2],8/13/D2/E[2],6/9/D3
(p7)    cmp4.eq.unc     p0, p7 = 20, r14        ;; // M [main.c: 8/13]
(p7)    chk.s.m         r22, ..L103                // M
(p7)    cmp4.eq.unc     p0, p8 = 1, r22            // I [main.c: 8/13]

..L110:
(p7)    add             r8 = 8, r0                 // M [main.c: 6/9]
(p7)    cmp4.eq.or      p6, p0 = 1, r22         ;; // I [main.c: 8/13]
(p8)    cmp4.eq.unc     p0, p7 = 2, r22         ;; // I [main.c: 8/13]
//file/line/col main.c/8/13/D1/E[2],8/13/D2/E[2],6/9/D3
(p7)    cmp4.eq.unc     p0, p7 = 20, r22        ;; // M [main.c: 8/13]
(p7)    chk.s.m         r11, ..L104                // M
(p7)    cmp4.eq.unc     p0, p8 = 1, r11            // I [main.c: 8/13]

..L111:
(p7)    add             r8 = 9, r0                 // M [main.c: 6/9]
(p7)    cmp4.eq.or      p6, p0 = 1, r11            // M [main.c: 8/13]
//file/line/col main.c/8/13/D1/E[2],8/13/D2/E[2],6/9/D3
.restore sp
(p6)    br.ret.dpnt.many rp                     ;; // B [main.c: 8/13]

..L34:
.body
.copy_state 1

//file/line/col main.c/8/13/D1/E[2]
(p8)    cmp4.eq.unc     p0, p6 = 2, r11            // M [main.c: 8/13]
        nop.i           0                       ;; // I
(p6)    cmp4.eq.unc     p0, p6 = 20, r11        ;; // I [main.c: 8/13]
//file/line/col main.c/8/13/D2/E[2],6/9/D3
(p6)    add             r8 = 10, r0                // M [main.c: 6/9]
        nop.i           0                          // I
        nop.i           0                       ;; // I
//file/line/col main.c/13/9
        nop.m           0                          // M [main.c: 13/9]
        nop.m           0                          // M
.restore sp
        br.ret.sptk.many rp                     ;; // B [main.c: 13/9]

..L55:
.body
.copy_state 1

        add             r8 = 0, r0                 // M
        nop.m           0                          // M
.restore sp
        br.ret.sptk.many rp                     ;; // B

..L75:
.body
.copy_state 1

        add             r8 = 0, r0                 // M
        nop.m           0                          // M
.restore sp
        br.ret.sptk.many rp                     ;; // B

..L96:
.body
.copy_state 1

        add             r8 = -32, r10           ;; // M
        ld4             r20 = [r8]                 // M [main.c: 8/13]
        add             r8 = 1, r0              ;; // I [main.c: 6/9]
        cmp4.eq.unc     p7, p0 = 1, r20            // M [main.c: 8/13]
.restore sp
(p7)    br.ret.dpnt.many rp                        // B [main.c: 8/13]
        br.sptk.few     ..L26                   ;; // B

..L97:
.body
.copy_state 1

        add             r8 = -28, r10           ;; // M
        ld4             r19 = [r8]                 // M [main.c: 8/13]
        add             r8 = 2, r0              ;; // I [main.c: 6/9]
        cmp4.eq.unc     p6, p0 = 1, r19            // M [main.c: 8/13]
.restore sp
(p6)    br.ret.dpnt.many rp                        // B [main.c: 8/13]
        br.sptk.few     ..L27                   ;; // B

..L98:
.body
.copy_state 1

        add             r8 = -24, r10           ;; // M
        ld4             r18 = [r8]                 // M [main.c: 8/13]
        nop.i           0                       ;; // I
        cmp4.eq.unc     p0, p8 = 1, r18            // M [main.c: 8/13]
        cmp4.eq.unc     p6, p7 = 1, r18            // M [main.c: 8/13]
        br.sptk.few     ..L105                  ;; // B

..L99:

        add             r18 = -20, r10          ;; // M
        ld4             r17 = [r18]                // M [main.c: 8/13]
        nop.i           0                       ;; // I
        cmp4.eq.andcm   p0, p7 = 20, r17        ;; // M [main.c: 8/13]
        cmp4.eq.andcm   p0, p7 = 1, r17            // M [main.c: 8/13]
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
        br.sptk.few     ..L106                  ;; // B

..L100:

        add             r18 = -16, r10             // M
(p7)    add             r8 = 5, r0              ;; // I [main.c: 6/9]
        nop.i           0                          // I
        ld4             r16 = [r18]             ;; // M [main.c: 8/13]
(p7)    cmp4.eq.unc     p0, p9 = 1, r16            // M [main.c: 8/13]
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
        br.sptk.few     ..L107                  ;; // B

..L101:

//file/line/col main.c/8/13/D1/E[2]
        add             r16 = -12, r10          ;; // M
        ld4             r15 = [r16]                // M [main.c: 8/13]
        nop.i           0                       ;; // I
(p7)    cmp4.eq.unc     p0, p8 = 1, r15            // M [main.c: 8/13]
        nop.m           0                          // M
        br.sptk.few     ..L108                  ;; // B

..L102:

        add             r15 = -8, r10           ;; // M
        ld4             r14 = [r15]                // M [main.c: 8/13]
        nop.i           0                       ;; // I
(p7)    cmp4.eq.unc     p0, p8 = 1, r14            // M [main.c: 8/13]
        nop.m           0                          // M
        br.sptk.few     ..L109                  ;; // B

..L103:

//file/line/col main.c/8/13/D2/E[2]
        add             r14 = -4, r10           ;; // M
        ld4             r22 = [r14]                // M [main.c: 8/13]
        nop.i           0                       ;; // I
(p7)    cmp4.eq.unc     p0, p8 = 1, r22            // M [main.c: 8/13]
        nop.m           0                          // M
        br.sptk.few     ..L110                  ;; // B

..L104:

        ld4             r11 = [r10]             ;; // M [main.c: 8/13]
(p7)    cmp4.eq.unc     p0, p8 = 1, r11            // M [main.c: 8/13]
        nop.i           0                          // I
        nop.m           0                          // M
        nop.m           0                          // M
.restore sp
        br.sptk.few     ..L111                  ;; // B

..L1:
//      $end                                    ;; // A

	.endp	foo

// ===


// ===
	.secalias .abe$5.IA_64.unwind, ".IA_64.unwind"
	.section .abe$5.IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.abe$4.text+16)
	data8.ua @segrel(.abe$4.text+896)
	data8.ua @segrel(.abe$unwind_info_block00000)

// ===
	.secalias .abe$6.IA_64.unwind_info, ".IA_64.unwind_info"
	.section .abe$6.IA_64.unwind_info = "a", "progbits"
	.align 8
// unwind_info_block: notype local temp
	data8.ua @segrel(.llo$annot_info_block0000)
.abe$unwind_info_block00000:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x07
	data1	0x00, 0x32, 0x81, 0xc0, 0x00, 0x2f, 0xa1, 0xc0
	data1	0x00, 0x26, 0xa1, 0xc0, 0x00, 0x23, 0xa1, 0xc0
	data1	0x00, 0x26, 0xa1, 0xc0, 0x00, 0x61, 0x2a, 0xa1
	data1	0xc0, 0x00, 0x29, 0xa1, 0xc0, 0x00, 0x23, 0xa1
	data1	0xc0, 0x00, 0x23, 0xa1, 0xc0, 0x00, 0x25, 0xa1
	data1	0xc0, 0x00, 0x25, 0xa1, 0xc0, 0x00, 0x61, 0x30
	data1	0xa1, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	.type	_memset,@function
	.global	_memset
	.size	main, 192
// Routine [id=0003] ( main )

// ===
	.secalias .abe$9.text, ".text"
	.section .abe$9.text = "ax", "progbits"
	.align	32
	.proc	main
..L0:
//      $start          CMid915 =               ;; // A

..L2:
main::
.prologue
//      $entry          CMid916 =                  // A [main.c: 17/1]
//file/line/col main.c/17/1
.save ar.pfs, r33
        alloc           r33 = ar.pfs, 0, 4, 3, 0   // M [main.c: 17/1] [UVU: ]
        add             r8 = @ltoffx(a), gp        // M [main.c: 20/9]
.save rp, r34
        mov             r34 = rp                   // I [main.c: 17/1]
        add             r9 = @pltoff(_memset), gp  // M [main.c: 19/4]
        add             r32 = 0, gp                // M
.body
.label_state 1

//file/line/col main.c/19/4
        add             r37 = 0, r0             ;; // I [main.c: 19/4]
        ldxmov          r36 = [r8], a#             // M [main.c: 20/9]
        add             r8 = 8, r9                 // M
        add             r38 = 40, r0               // I [main.c: 19/4]
        ld8.acq         r9 = [r9]                  // M [main.c: 19/4]
        add             r14 = 0, r32               // M [main.c: 19/4]
        add             r35 = 500, r0           ;; // I [main.c: 23/4]
        ld8             gp = [r8]                  // M [main.c: 19/4]
        mov             b7 = r9                 ;; // I [main.c: 19/4]
        nop.i           0                          // I
        lfetch.nt1      [r36]                      // M [main.c: 19/4]
        nop.m           0                          // M
        br.call.sptk.many rp = b7               ;; // B [main.c: 19/4] [OUT: r36,r37,r38]
        add             gp = 0, r32                // M [main.c: 19/4]
        nop.m           0                          // M
//file/line/col main.c/23/4
        br.call.sptk.many rp = foo#             ;; // B [main.c: 23/4] [UVU]
        cmp4.eq.unc     p6, p0 = r8, r35           // M [main.c: 23/4]
        nop.m           0                          // M
(p6)    br.dpnt.many    ..L5                    ;; // B [main.c: 23/4]

..L4:
//file/line/col main.c/26/1
        add             r8 = 0, r0                 // M [main.c: 26/1]
        mov             rp = r34                ;; // I
        mov             ar.pfs = r33               // I
        nop.m           0                          // M
        nop.m           0                          // M
.restore sp
        br.ret.sptk.many rp                     ;; // B [main.c: 26/1]

..L5:
.body
.copy_state 1

//file/line/col main.c/24/8
        add             r36 = @gprel(.abe$3.sdata), gp // M [main.c: 24/8]
        nop.m           0                          // M
        br.call.dptk.many rp = printf#          ;; // B [main.c: 24/8] [OUT: r36] [UVU]
        add             gp = 0, r32                // M [main.c: 24/8]
        nop.m           0                          // M
.restore sp
        br.sptk.many    ..L4                    ;; // B [main.c: 24/8]

..L1:
//      $end                                    ;; // A

	.endp	main

// ===


// ===
	.secalias .abe$10.IA_64.unwind, ".IA_64.unwind"
	.section .abe$10.IA_64.unwind = "a", "unwind"
	.align 8
	data8.ua @segrel(.abe$9.text)
	data8.ua @segrel(.abe$9.text+192)
	data8.ua @segrel(.abe$unwind_info_block00001)

// ===
	.secalias .abe$11.IA_64.unwind_info, ".IA_64.unwind_info"
	.section .abe$11.IA_64.unwind_info = "a", "progbits"
	.align 8
// unwind_info_block: notype local temp
	data8.ua @segrel(.llo$annot_info_block0001)
.abe$unwind_info_block00001:	data1	0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x03
	data1	0x05, 0xe4, 0x02, 0xb0, 0xa2, 0xe6, 0x00, 0xb1
	data1	0x21, 0x39, 0x81, 0xc0, 0x00, 0x26, 0xa1, 0xc0
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

// ===
	.secalias .abe$2.data, ".data"
	.section .abe$2.data = "aw", "progbits"
	.align 16
// a: object global
a::	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	data1	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

// ===
	.secalias .abe$3.sdata, ".sdata"
	.section .abe$3.sdata = "asw", "progbits"
	.align 8
// .sdata: section local
// _noname: notype local temp

.abe$_noname00002:	stringz	"aaa\n"

// ===
	.secalias .abe$7.HP.opt_annot, ".HP.opt_annot"
	.section .abe$7.HP.opt_annot = "a", "annot"
	.align 8
// .llo$annot_info_block0000: object local temp
// .llo$annot_info_block0001: object local temp
.llo$annot_info_block0000:	data1	0x00, 0xac, 0x01, 0xfe, 0xfc, 0x3e, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0x80, 0x32, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x88, 0x02, 0x00, 0x0f, 0x00
.llo$annot_info_block0001:	data1	0x00, 0xac, 0x01, 0x7e, 0xff, 0xbe, 0xff, 0xff
	data1	0xff, 0xfc, 0xff, 0xff, 0xbc, 0xfc, 0x01, 0x02
	data1	0x84, 0x00, 0x02, 0x88, 0x02, 0x00, 0x0f, 0x00

