/* Program to test the "-pid=process id" command line argument
*/

#include <stdio.h>
#include <unistd.h>

int dont_exit = 1;

int main()
{
	(void) alarm(600);
	while (dont_exit);
}
