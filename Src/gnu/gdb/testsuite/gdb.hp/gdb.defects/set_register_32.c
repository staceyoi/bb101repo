/* Test whether registers contents are set properly for gdb32.
 */

#include <stdio.h>

int a = 0x01020304;
int b = 0x08070605;

long long fn(long long c, long long d)
{
   return c + d;
}

int main() {
   long long c = fn(a, b);
   return 0;
}
