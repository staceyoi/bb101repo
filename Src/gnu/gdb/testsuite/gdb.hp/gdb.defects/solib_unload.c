#include<dl.h>
#include<stdio.h>

#define LIB1 "gdb.hp/gdb.defects/solib_unload1.sl"
#define LIB2 "gdb.hp/gdb.defects/solib_unload2.sl"

void libs_loaded()
{
}

void libs_unloaded()
{
}

void all_done()
{
}

int main()
{
  shl_t handle1, handle2;
  void (*symfoo) (void);
  void (*symbar) (void);


  for (int i=0; i<10000; i++) {

      if (i % 2) {
          handle1 =  shl_load (LIB1,BIND_DEFERRED,0);
          handle2 =  shl_load (LIB2,BIND_DEFERRED,0);
          if (!handle1 || !handle2) {
              printf("Shared library loads failed...!\n");
              return 1;
          }
          libs_loaded();
          shl_unload(handle1);
          shl_unload(handle2);
          libs_unloaded();
      }
      else {
          handle2 =  shl_load (LIB2,BIND_DEFERRED,0);
          handle1 =  shl_load (LIB1,BIND_DEFERRED,0);
          if (!handle1 || !handle2) {
              printf("Shared library loads failed...!\n");
              return 1;
          }
          libs_loaded();
          shl_unload(handle2);
          shl_unload(handle1);
          libs_unloaded();
      }
  }

  all_done();
}
