#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

void *thr(void *d) {
    pid_t pid;
    int stat_loc;
    
    printf("The thread...\n");

    if ((pid = fork()) != -1) {
        switch (pid) {
        case 0:                 /* I am the child so exec... */
            printf("Hello from child\n");

            exit(0);
            break;
            
        default:                /* I am the parent so wait... */
            printf("Hello from parent\n");
            wait(&stat_loc);
            printf("In parent, child exited with status %d\n", 
stat_loc);
            break;
            
        }
    } else {
        printf("Fork failed\n");
    }

    printf("Thread done\n"); return 0;
}
int main(void) {
    pid_t pid;
    int stat_loc;
    pthread_t tid;
    
    printf("Creating thread...\n");
    pthread_create(&tid, NULL, thr, NULL);

    printf("Main thread\n");
    pthread_join(tid, NULL);

    printf("Program done\n");
    exit (0);
}
