
#include "hdr.h"

__thread int x;
__thread int xa[100];

__thread double d = 2;
__thread double da[100] = { 1 };

__thread unsigned m;

static int s[100];
static int nthreads;

extern int mod1(int);
extern int mod2(int);
extern int mod3(int);

static void init(int id)
{
   unsigned i;
   d = 2 + (100*id);
   m = id;
   x = (id*2);
   for (i = 0; i < 100; ++i)
   {
      xa[i] = i + (id * 100);
      da[i] = i + (id * 100);
   }
}
   

void *thread_helper(void *arg)
{
   TD *p = (TD *)arg;
   int st = 0;
   int id = p->id;
   
   printf("thread %d starting\n", p->id);
   
   init(id);
   
   st = mod1(id) + mod2(id) + mod3(id);
   s[id] = st;
   
   printf("thread %d complete\n", p->id);
   
   return 0;
}

int main()
{
   int sum = 0, i, train = 0;
   pthread_t *threads = 0;
   TD *td = 0;

   nthreads = 3;
   if (getenv("TRAIN")) 
   {
      train = 1;
      nthreads = 128;
   }

	threads = (pthread_t *) malloc(nthreads*sizeof(*threads));

	td = (TD *) malloc(sizeof(TD) * nthreads);

	/* Start up thread */
	for (i=0; i<nthreads; i++)
	{
		td[i].id = i;
		pthread_create(&threads[i], NULL, thread_helper, (void *)(td+i));
	}

	/* Synchronize the completion of each thread. */
	for (i=0; i<nthreads; i++)
	{
		pthread_join(threads[i],NULL);
	}
	free(td);
   free(threads);

   for (i = 0; i < nthreads; ++i)
      sum += s[i];

   return sum;
}



