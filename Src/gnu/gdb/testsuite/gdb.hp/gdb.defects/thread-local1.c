#include <unistd.h>
#include <stdlib.h>

extern __thread int x;
extern __thread int xa[100];

extern __thread double d;
extern __thread double da[100];

__thread unsigned m = 22;

__thread unsigned sconflict;

int mod1(int id)
{
   m++;
   printf("thread %d m is %d\n", id, m);
   x++;
   printf("thread %d x is %d\n", id, x);
   xa[x]++;
   da[x]++;
   sconflict++;
   return m+x;
}


