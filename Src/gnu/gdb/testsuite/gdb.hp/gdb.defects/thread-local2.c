
#include <unistd.h>
#include <stdlib.h>

extern __thread double d;
extern __thread double da[100];

extern __thread unsigned m;

extern __thread int x;
extern __thread int xa[100];

__thread double sconflict;

int mod2(int id)
{
   m++;
   printf("m is %d\n", m);
   x++;
   xa[id]++;
   da[id]++;
   sconflict = 0;
   return m+x;
}


