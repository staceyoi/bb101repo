#include <unistd.h>
#include <stdlib.h>

extern __thread int x;
extern __thread int xa[100];

extern __thread double d;
extern __thread double da[100];

int mod3(int id)
{
   x++;
   xa[id]++;
   da[id] += d;
   return x;
}
