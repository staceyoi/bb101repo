#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>


void *thr(void *d) {
    
    printf ("will break here\n"); 
    printf ("Done waiting\n");
}

int wait_here = 1;

void main(void) {
    pid_t pid;
    int stat_loc;
    pthread_t tid;
    long oldmask; 
    printf("Creating thread...\n");
    pthread_create(&tid, NULL, thr, NULL);

    printf("Main thread\n");

    while (wait_here);
    printf ("Done waiting.\n");
    pthread_join(tid, NULL);

    wait_here = 1;
    while (wait_here);
    oldmask = sigblock (sigmask (SIGTRAP));
    wait_here = 1;
    while (wait_here);
   
    printf("Program done\n");
    
    exit (0);
}
