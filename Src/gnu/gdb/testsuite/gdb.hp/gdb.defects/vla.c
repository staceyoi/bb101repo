#include <alloca.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 16

int size(void) { return SIZE; }

int main()
{
   char * p, * q, * r;
   {
      char a[size()];
      char bb[10];
      char *pp;
      printf("a = %p\n", a);
      printf("bb = %p\n", bb);
      pp = malloc(100);
      p = alloca(SIZE);
      printf("pp = %p\n", pp);
      printf("p = %p\n", p);
   }
   q = alloca(SIZE);
   printf("q = %p\n", q);
   r = alloca(SIZE);
   printf("r = %p\n", r);
   if ((p != q) && (q != r) && (p != r))
      printf("PASSED\n");
   else
      printf("*** FAILED\n");
   return 0;
}
