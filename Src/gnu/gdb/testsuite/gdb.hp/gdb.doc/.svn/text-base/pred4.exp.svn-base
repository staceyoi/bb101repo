# Copyright (C) 1998 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# JAGag06703.exp -- To test fix for the defect JAGag06703.
# this test is for checking if breakpoints at special instructions hits 
# irrespective of whether the instruction is predicated or not
#
# Here we identify and place breaks at "*.unc" instruction and see if its hit..
# we have h/w and s/w breakpoints at cmp4.unc that is predicated and s/w bkpt at
# tbit.unc that is not predicated.
# The breakpoints should hit at all these instructions.


if $tracelevel then {
    strace $tracelevel
}

if { [skip_hp_tests] } then { continue }

# Predication is applicable only on IA, so skipping this script for PA
if { ![istarget "ia64*-hp-*"] } { return 0 }

# Test specific to 64-bits (passes 1-8 ) as i use a lot of hard coded address
# for setting breakpoints, which change in a 32 bit compile..
if { "$multipass_name" > "8" } {
  verbose "testignored for non 64-bit programs."
  return 0
}

set testfile pred4
set srcfile ${testfile}.c
set binfile ${srcdir}/${subdir}/${testfile}

# We use specific h/w addresses for breakpoints, that tend to change
# based on the compiler version and hence we don't compile the tests each
# time and instead use the pre-compiled binary.

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}


gdb_test "b main" ".*" ""
#Instruction at 0x40008b0:2 is a tbit.unc (non-predicated)
#this bkpt should be hit
gdb_test "b *0x40008b0:2" "Breakpoint 2.*" "setting bkpt in a tbit.unc inst "

#Instruction at 0x40008e1 is a cmp4.unc (QP is false)
#this bkpt should be hit
gdb_test "hbreak *0x40008e1" "Hardware assisted breakpoint 3.*" "setting h/w bkpt in cmp4.unc inst (QP is false) "

#Instruction at 0x40008e2 is a cmp4.unc (QP is true)
#this bkpt should be hit
gdb_test "hbreak *0x40008e2" "Hardware assisted breakpoint 4.*" "setting h/w bkpt in cmp4.unc inst (QP is true)"

#Instruction at 0x40008f0 is a cmp4.unc (QP is false)
#this bkpt should be hit
gdb_test "break *0x40008f0" "Breakpoint 5.*" "setting bkpt in cmp4.unc inst (QP is false) "

#Instruction at 0x40008f1 is a cmp4.unc (QP is true)
#this bkpt should be hit
gdb_test "break *0x40008f1" "Breakpoint 6.*" "setting bkpt in cmp4.unc inst (QP is true)"

gdb_test "r\n" ".*$gdb_prompt $" ""

# 2. Verify that GDB hits s/w breakpoint at tbit.unc
send_gdb "cont\n"
gdb_expect {
  -re ".*Breakpoint 2.*$gdb_prompt $" { 
        send_gdb "disas \$pc \$pc+16\n"
        gdb_expect {
		-re ".*tbit.z.unc.*$gdb_prompt $" {
        	 	pass "s/w bkpt at tbit.unc hit" 
                 }
		-re ".*$gdb_prompt $" {
        	 	fail "s/w bkpt at tbit.unc hit" 
                 }
   		timeout { fail "(timeout) s/w bkpt at tbit.unc hit" }
	}	
    }
   timeout { fail "(timeout) outer: s/w bkpt at tbit.unc hit" }
}

# 3. Verify that GDB hits h/w breakpoint at cmp4.unc ( QP is false )
send_gdb "cont\n"
gdb_expect {
  -re ".*Breakpoint 3.*$gdb_prompt $" { 
        send_gdb "disas \$pc \$pc+16\n"
        gdb_expect {
		-re ".*cmp4.eq.unc.*$gdb_prompt $" {
        		pass "h/w bkpt at cmp4.unc is hit( QP is false )" 
                 }
		-re ".*$gdb_prompt $" {
        		fail "h/w bkpt at cmp4.unc is hit( QP is false )" 
                 }
       		timeout { fail "(timeout) h/w bkpt at cmp4.unc is hit( QP is false )" }
	}
    }
   timeout { fail "(timeout) outer: h/w bkpt at cmp4.unc is hit( QP is false )" }
}

# 4. Verify that GDB hits h/w breakpoint at cmp4.unc ( QP is true )
send_gdb "cont\n"
gdb_expect {
  -re ".*Breakpoint 4.*$gdb_prompt $" { 
        send_gdb "disas \$pc \$pc+16\n"
        gdb_expect {
		-re ".*cmp4.eq.unc.*$gdb_prompt $" {
        		pass "h/w bkpt at cmp4.unc is hit( QP is true )" 
                 }
		-re ".*$gdb_prompt $" {
        		fail "h/w bkpt at cmp4.unc is hit( QP is true )" 
                 }
       		timeout { fail "(timeout) h/w bkpt at cmp4.unc is hit( QP is true )" }
	}
    }
   timeout { fail "(timeout) outer: h/w bkpt at cmp4.unc is hit( QP is true )" }
}

# 5. Verify that GDB hits s/w breakpoint at cmp4.unc ( QP is false )
send_gdb "cont\n"
gdb_expect {
  -re ".*Breakpoint 5.*$gdb_prompt $" { 
        send_gdb "disas \$pc \$pc+16\n"
        gdb_expect {
		-re ".*cmp4.ne.unc.*$gdb_prompt $" {
        		pass "s/w bkpt at cmp4.unc is hit( QP is false )" 
                 }
		-re ".*$gdb_prompt $" {
        		fail "s/w bkpt at cmp4.unc is hit( QP is false )" 
                 }
       		timeout { fail "(timeout) s/w bkpt at cmp4.unc is hit( QP is false )" }
	}
    }
   timeout { fail "(timeout) outer: s/w bkpt at cmp4.unc is hit( QP is false )" }
}
# 6. Verify that GDB hits s/w breakpoint at cmp4.unc ( QP is true )
send_gdb "cont\n"
gdb_expect {
  -re ".*Breakpoint 6.*$gdb_prompt $" { 
        send_gdb "disas \$pc \$pc+16\n"
        gdb_expect {
		-re ".*cmp4.ne.unc.*$gdb_prompt $" {
        		pass "s/w bkpt at cmp4.unc is hit( QP is true )" 
                 }
		-re ".*$gdb_prompt $" {
        		fail "s/w bkpt at cmp4.unc is hit( QP is true )" 
                 }
       		timeout { fail "(timeout) s/w bkpt at cmp4.unc is hit( QP is true )" }
	}
    }
   timeout { fail "(timeout) outer: s/w bkpt at cmp4.unc is hit( QP is true )" }
}
gdb_exit
return 0
