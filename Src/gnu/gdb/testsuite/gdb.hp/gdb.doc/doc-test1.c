/* File doct1.c         Test code with Inlining
      **
      ** From Copperman Thesis, Figure 7.1, Page 23
      **
      ** Test and Expected Results:
      **     Set a breakpoint at routine a. When control reaches that
      **     breakpoint request a call stack traceback. Expected behavior
      **     is a call back display that shows: a() called from b(), b()
      **     called from c(), c() called from d(), and d() called from
      **     main().
      */
 
      #include <stdio.h>
 
      void a() {}
 
      int b() {
          a();
          return 1;
          }
 
      #pragma inline(b)
 
      int c(int x) {
          int y = b();
          return x+y;
          }
 
      int d() {
          return c(5);
          }
 
      #pragma noinline(a,c,d)
 
      int main(unsigned argc, char **argv) {
          printf("%dn", d());
          return 1;
          }

