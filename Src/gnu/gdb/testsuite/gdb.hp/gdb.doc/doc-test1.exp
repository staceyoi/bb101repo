# Copyright (C) 1998 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

#  Ron-Brender testcase 1

# It tests the stack frame for an inlined call.In this example pragma inline is called for inling the 
# functions.

if $tracelevel then {
  strace $tracelevel
}

set prms_id 0
set bug_id 0

# are we on a target board
if { ![isnative] && ![istarget "ia64*-hp-*"] } then {
    return
}

#if hppa then return back
if { [istarget "hppa*-*-*hpux*" ] } {
  return 0
}

set testfile "doc-test1"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug additional_flags=-w}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
    }

if [get_compiler_info ${binfile} "c++" ] {
    return -1
}

source ${binfile}.ci

#if GCC compiled skip this expect script.
if { ${gcc_compiled} || ${gxx_compiled} } {
    continue
}

# Setting flag doc_opt for optimization level.

set doc_opt 0

if { [string match "+O2*" $optimize] || [string match "-O*" $optimize] } {
    set doc_opt 1 
} elseif { [string match "+O1*" $optimize] } {
    set doc_opt 0
}

#Setting breakpoint on function a(non-inline function) and checking stack frame.

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if { $doc_opt && $hp_aCC_compiler } then { setup_xfail "ia64*-hp-*" }

gdb_test "break a" \
   "Breakpoint 1.*doc-test1.c.*line 15.*" \
   " break on routine a"

if { $doc_opt && $hp_aCC_compiler } then { setup_xfail "ia64*-hp-*" }

gdb_test "run" ".*Breakpoint.*a.*doc-test1.c.*" "run till a"

if { $hp_aCC5_compiler } then { setup_xfail "ia64*-hp-*" }

send_gdb "backtrace\n"
gdb_expect {
    -re "#0.*a.*#1.*inline b.*#2.*c.*#3.*d.*#4.*main.*$gdb_prompt $" {
       pass "backtrace of inlined call"
    }
    -re "#0.*a.*#1.*in b.*#2.*in c.*#3.*in d.*#4.*in main.*$gdb_prompt $" {
       fail "backtrace of inlined call"
    }
    -re ".*$gdb_prompt $"	{ fail "backtrace of inlined call" }
    timeout			{ fail "(timeout) backtrace of inlined call" }
}

# Setting breakpoint on function b(inlined function) and checking stack frame.

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

setup_xfail "ia64*-hp-*"

send_gdb "break b\n"
gdb_expect {
   -re "Breakpoint.*doc-test1.c.*$gdb_prompt $" { fail "break on routine b - compiler error" }
   -re "Breakpoint.*deferred.*$gdb_prompt $"    { fail "break on routine b" }
   -re ".*$gdb_prompt $"			   { fail "break on routine b" }
   timeout					   { fail "(timeout) break on routine b" }
}

setup_xfail "ia64*-hp-*"

send_gdb "run\n"
gdb_expect {
   -re "Starting program.*Breakpoint.*doc-test1.c.*$gdb_prompt $" { fail "run till b" }
   -re "Starting program.*Program exited with code.*$gdb_prompt $" {
       fail "run till b"
   }
   -re ".*gdb_prompt $"				  { fail "run till b" }
   timeout					  { fail "(timeout) run till b" }
}

setup_xfail "ia64*-hp-*"

send_gdb "backtrace\n"
gdb_expect {
    -re ".*#0 b.*doc-test1.c.*#1.*c.*#2.*d.*#3main.*$gdb_prompt $" {
       pass "backtrace of inlined call"
    }
    -re "No stack.*$gdb_prompt $" { xfail "backtrace of inlined call" }
    -re ".*$gdb_prompt $"	{ fail "backtrace of inlined call" }
    timeout			{ fail "(timeout) backtrace of inlined call" }
}

# Setting breakpoint on line number where function is inlined and checking for stack frame.

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

if { $doc_opt == 0 && (!$hp_aCC5_compiler || !$hp_aCC_compiler) } then { setup_xfail "ia64*-hp-*" }

send_gdb "break 17\n"
gdb_expect {
   -re "Breakpoint 1.*doc-test1.c.*line 15.*$gdb_prompt $" { fail "break on routine b" }
   -re "Breakpoint.*doc-test1.c.*line 17.*Breakpoint 2.*Multiple.*$gdb_prompt $" { fail "break on routine b" }
   -re "Breakpoint 1.*doc-test1.c.*line 17.*$gdb_prompt $" { pass "break on routine b" }
   -re ".*$gdb_prompt $"			   { fail "break on routine b" }
   timeout					   { fail "(timeout) break on routine b" }
}

if { $doc_opt == 0 || $hp_aCC_compiler } then { setup_xfail "ia64*-hp-*" }

send_gdb "run\n"
gdb_expect {
   -re "Starting program.*Breakpoint.*inline b.*$gdb_prompt $" { pass "run till b" }
   -re "Starting program.*Breakpoint.*$gdb_prompt $" {
       fail "run till b"
   }
   -re ".*gdb_prompt $"				  { fail "run till b" }
   timeout					  { fail "(timeout) run till b" }
}

if { $doc_opt == 0 || $hp_aCC_compiler } then { setup_xfail "ia64*-hp-*" }

send_gdb "backtrace\n"
gdb_expect {
    -re "#0.*c.*x=5.*at.*doc-test1.c.*#1.*in d.*#2.*in main.*$gdb_prompt $" {
       fail " backtrace of inlined call"
    }
    -re ".*#0.*inline b.*#1.*c.*#2.*d.*#3.*main.*$gdb_prompt $" { pass "backtrace of inlined call" }
    -re ".*$gdb_prompt $"	{ fail "backtrace of inlined call" }
    timeout			{ fail "(timeout) backtrace of inlined call" }
}

gdb_exit

