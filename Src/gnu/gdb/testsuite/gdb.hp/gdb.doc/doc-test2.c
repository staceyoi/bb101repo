
      /* File doct2.c    Constant/Copy Propogation and Dead Code Elimination
      **
      ** From Copperman Thesis, Figure 7.2, Page 24
      **
      ** Test and Expected Results:
      ** (1) Examine the compiled code. One or more of the lines marked
      **     below are probably eliminated due to constant/copy propogation.
      **     Set a breakpoint at one of those lines. Expected behavior is
      **     for the debugger to report that the line does not exist and
      **     indicate what action (if any) was taken.
      ** (2) Set a breakpoint at the test for z (or the following
      **     prinf("not z")). Upon reaching that line, display variables
      **     w, y, y and z. Expected results are:
      **         w    arbitrary value ("not defined" would be more useful)
      **         x    5
      **         y    5
      **         z    0
      */
 
      #include <stdio.h>
      #ifndef  __G_FLOAT
      #define  __G_FLOAT 1
      #endif
      #include <stdlib.h>
 
      int main(unsigned argc, char **argv) {
          int w, x, y, z=0;
 
          x = atoi(" 2 ");
          printf("%dn", x);
 
          x = 5;               /* after propogation, possibly eliminated */
          y = x;               /* y gets 5, after propogation, poss elim */
 
          if (y > 2) {         /* always true, possibly eliminated       */
              printf("y > 2");
              }
          else {
              printf("y <= 2");/* dead, possibly eliminated              */
              }
 
          if (z) {             /* always false, possibly eliminated      */
              printf(" & z");  /* dead, possibly eliminated              */
              }
          else {
              printf(" & not z");
              }
 
          printf("n");
          }

