      /*  File doct3.c        Code Motion out of conditional branches
      **
      **  Test and Expected Behavior
      **      Set breakpoint at main() (or at the call to init()). Then
      **      single step by line thru the conditional statement. Expected
      **      behavior is
      **      (1) to not stop at the motioned code prior to the if
      **          statement, and 
      **      (2) to show the current values of x and l while stepping
      **          thru the conditional.
      */
 
      #include <stdio.h>
 
      int i, j, k, l, x, y, z;
 
      /* Define values of global variables, but avoid possible value
      ** propogation (see file doct3a.c).
      */
      void init ();
 
      main() {
 
          init();
 
          /* Possible code motion to here:
          **  <temp> = y + z;
          **  l = <temp> + 5;
          */
          if (i < 0) {
              x = y + z;     /* May become x = <temp>;        */
              l = x + 5;     /* May move completely above     */
              k++;
              }
          else {
              x = -(y + z);  /* May become x = - <temp>       */
              l = x + 5;     /* May move completely above     */
              j--;
              }
 
          /* Keep all values alive
          */
          printf("%d, %d, %d, %d, %d, %d, %dn",
              i, j, k, l, x, y, z);
              
          }

