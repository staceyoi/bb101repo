 
      /*  File doct4.c        Code Motion out of loop
      **
      **  Test and Expected Behavior
      **      Set breakpoint at main() (or at the call to init()). Then
      **      single step by line thru the loop statements. Expected
      **      behavior is
      **      (1) to not stop at the motioned code prior to the loops, and 
      **      (2) to show the current values of x and w while stepping thru
      **          the loops.
      */
 
      #include <stdio.h>
 
      /* Define values of global variables, but avoid possible value
      ** propogation (see file doct4a.c).
      */
      int i, j, k, l, x, y, z;
 
      int init();
 
      main() {
 
          int m,
              max;
 
          max = init();
 
          /* Possible code motion to here:
          **  <temp> = y + z;
          */
          for (m = 1; m <= max; m++)
              x +=                    /* May become x =+ <temp>; */
                   (y + z);
 
          /* Possible code motion to here:
          **  <temp> = l + z;
          */
          j = 1;
          while (j <= max) {
              y +=                    /* May become y =+ <temp>; */
                   (l + z);
              j++;
              }
 
          /* Keep all values alive
          */
          printf("%d, %d, %d, %d, %d, %d, %dn",
              i, j, k, l, x, y, z);
              
          }

