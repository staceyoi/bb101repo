      /*  File doct6.c        Cross-Jumping
      **
      **  Test and Expected Behavior:
      **      Step through the program and note what lines are displayed.
      **      Cross-jumped code might be attributed to either/any arm of a
      **      choise/independent of which path is executed, or might not be
      **      attrbuted to any. Expected behavior is to display the correct
      **      lines as written.
      */
 
      #include <stdio.h>
 
      int k, x, y, z;
 
      void init();
      void rout(int, int, int);
 
      main () {
 
          init();
 
      /* 1 */
          if (x == 1) {           /* evaluates false */
              x 
                  = k
                      + z;
              }
          else {
              x
                  = y
                      + z;
              }
 
          /* Possible cross-jumped code:
          **     add z, assign to x
          */
 
      /* 2 */
          if (z) {                /* evaluates true  */
              y = k;
              rout
                  (
                  x,
                  y,
                  z
                  );
              }
          else {
              k = 10;
              rout
                  (
                  x,
                  9,
                  z
                  );
              }
 
          /* Possible cross-jumped code:
          **     load parameter x or z; call rout
          */
 
      /* 3 */
          switch (k) {            /* k == 3 */
          case 1 : x
                      = k
                          + z;
                   break;
          case 5 : x
                      = 9
                          + z;
                   break;
          case 9 : x
                      = y
                          + z;
                   break;
          case 3 : x              /* select this case */
                      = y
                          + z;
                   break;
          }
 
 
          /* Possible cross-jumped code:
          **     load y (shared by cases 9 and 3), flowing into add z;
          **     assign x
          */
 
          printf("%d %d %d %d", k, x, y, z);
 
          return 0;
          }
