      /*  File doct6a.c       Helper for doct6.c
      */
 
      extern int k, x, y, z;
 
      /* Initialize all global variables
      */
      void init () {
          k = 3;
          x = 4;
          y = 5;
          z = 6;
          }
 
      /* A fake routine called to imply possible global side-effects at the
      ** point of call.
      */
      void rout (int a, int b, int c) {
          return;
          }

