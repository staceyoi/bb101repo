      /*  File doct7.c        Code Motion out of conditional branches and
      **                      Possible short, local lifetimes
      **
      **  Test and Expected Behavior
      **      Set breakpoint at main() (or at the call to init()). Then
      **      single step by line thru the conditional statement. Expected
      **      behavior is
      **      (1) to not stop at the motioned code prior to the if
      **          statement,
      **      (2) to show the current values of x and l while stepping
      **          thru the conditional, and
      **      (3) to show current values of x1 and l1 even when no longer
      **          alive.
      */
 
      #include <stdio.h>
 
      int i, j, k, l, x, y, z;
 
      /* Define values of global variables, but avoid possible value
      ** propogation (see file doct7a.c).
      */
 
      void init ();
 
      main() {
 
          int i1, j1, k1, l1, x1, y1, z1;
 
          init();
 
          /* Copy statics to locals
          */
          i1 = i;
          l1 = l;
          x1 = x;
          y1 = y;
          z1 = z;
 
          /* Possible code motion to here:
          **  <temp> = y1 + z1;
          **  l1 = <temp> + 5;
          */
          if (i1 < 0) {
              x1 = y1 + z1;     /* May become x = <temp>;     */
              l1 = x1 + 5;      /* May move completely above  */
              j1 = j;
              k1 = k;
              k1++;
              j = j1;
              k = k1;
              }
          else {
              x1 = -(y1 + z1);  /* May become x = - <temp>    */
              l1 = x1 + 5;     /* May move completely above   */
              j1 = j;
              k1 = k;
              j1--;
              j = j1;
              k = k1;
              }
 
          /* Copy locals back to statics
          */
          i = i1;
          l = l1;
          x = x1;
          y = y1;
          z = z1;
 
          /* Keep all values alive
          */
          printf("%d, %d, %d, %d, %d, %d, %dn",
              i, j, k, l, x, y, z);
              
          }

