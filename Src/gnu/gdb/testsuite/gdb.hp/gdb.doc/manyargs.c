//
// Test case with large numbers of arguments, also varargs
//

#include <stdio.h>
#include <stdarg.h>

#define DECLINIT(BT, BTC, _x) \
  BT _x##1  = BTC+0 , _x##2  = BTC+1 , _x##3  = BTC+2 , _x##4  = BTC+3 , \
     _x##5  = BTC+4 , _x##6  = BTC+5 , _x##7  = BTC+6 , _x##8  = BTC+7 , \
     _x##9  = BTC+8 , _x##10 = BTC+9 , _x##11 = BTC+10 , _x##12 = BTC+11 , \
     _x##13 = BTC+12 , _x##14 = BTC+13 , _x##15 = BTC+14 , _x##16 = BTC+15 

#define FDECL(BT, _x) \
  BT _x##1  , BT _x##2  , BT _x##3  , BT _x##4  , \
  BT _x##5  , BT _x##6  , BT _x##7  , BT _x##8  , \
  BT _x##9  , BT _x##10 , BT _x##11 , BT _x##12 , \
  BT _x##13 , BT _x##14 , BT _x##15 , BT _x##16

#define SUMS(_x) \
  _x##1  + _x##2  + _x##3  + _x##4  + _x##5  + _x##6  + _x##7  + _x##8 + \
  _x##9  + _x##10 + _x##11 + _x##12 + _x##13 + _x##14 + _x##15 + _x##16

#define LIST(_x) \
  _x##1  , _x##2  , _x##3  , _x##4  , _x##5  , _x##6  , _x##7  , _x##8 , \
  _x##9  , _x##10 , _x##11 , _x##12 , _x##13 , _x##14 , _x##15 , _x##16

double double32(FDECL(double, da),
		FDECL(double, db))
{
  return SUMS(da) + SUMS(db);
}

unsigned unsigned32(FDECL(unsigned, ua),
		    FDECL(unsigned, ub))
{
  return SUMS(ua) + SUMS(ub);
}

double mix64(FDECL(unsigned, ua),
	     FDECL(unsigned, ub),
	     FDECL(double, da),
	     FDECL(double, db))
{
  return (double) (unsigned32(LIST(ua), LIST(ub))) +
    double32(LIST(da), LIST(db));
}

int main()
{
  DECLINIT(double, 0.0, lda);
  DECLINIT(double, 100.0, ldb);
  DECLINIT(unsigned, 0, lua);
  DECLINIT(unsigned, 100, lub);

  double x = mix64(LIST(lua), LIST(lub), LIST(lda), LIST(ldb));

  return (x != 0.0);
}

  

  

