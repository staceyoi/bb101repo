#include<iostream>

class C{
  private:
    char *ptr;
  public:
    C(const char* c)
    {
      int len = strlen(c) + 1;
      ptr = new char[len];
      strncpy(ptr,c,len);
    }
    void print(){
      std::cout<< ptr << std::endl;
    }

    ~C(){
      delete ptr;
    }
};


int main()
{
  C cobj("Muthu");
  cobj.print();
  return 0;
}

