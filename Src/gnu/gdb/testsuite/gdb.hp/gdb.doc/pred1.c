#include <unistd.h>

int main()
{
	int j,i=10, k =5 , l = 3;
	char value;

       /* Our random will always be 1 the first time.. We really don't need
          a random number.. We need some call, so that the compiler generates 
          predicated code and doesn't optimize away the if branch statically..
        */
	j = random() & 01;
	i = i * j;
		if (j == 0 )
		{
			printf("if path taken \n");
			i++;
			k = k * j + k;
			l = l * j + l;
			i = i + k + l;
		}
		else
		{
			printf("else path taken \n");
			i--;
			k = k * j -k;
			l = l * j -l;
			i = i - k - l;
		}
	printf("The value of i = %d  k = %d \n", i,k);
}
