# This file was written by Sunil S (sunil.s@hp.com).
# Supported compiler options for macro support
# cc -g +macro_debug=all/referenced/none

# Case 1:
# gdb should behave the same when the following command line options are used:
# * -g
# * -g0
# * -g +macro_debug=referenced

# Case 2:
# Check gdb macro commands when the src is compiled with "-g +macro_debug=all"

# Case 3: 
# Macro feature should not work when "-g +macro_debug=none" option is used.

if $tracelevel then {
    strace $tracelevel
}

if { ![istarget "ia64*-hp*-*"] } {
    verbose "gdb macro functionality not supported for PA targets."
    return 0
}

set prms_id 0
set bug_id 0

set testfile "mac_hdr_chk"
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

# Case 1
if  { [gdb_compile "${srcfile}" "${binfile}" executable "{additional_flags=-g0}"] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Start the program running.
if {! [runto_main]} {
    fail "macro tests suppressed: couldn't run to main"
    return 0
}

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

send_gdb "info macro M1\n" 
gdb_expect {
  -re "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20" {
	pass "info macro M1"
}
  -re "GDB has no preprocessor macro information for that code.*" { 
	return 0
#	fail "No macro info"
}
}
#gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*file3.*included at.*mac_hdr_chk.*define M3 30"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 30"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*30"
gdb_test "print M1+M2+M3" ".*70"

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

gdb_test "next" ".*" "Next"
gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*mac_hdr_chk.*define M3 35"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 35"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*35"
gdb_test "print M1+M2+M3" ".*75"

#Inside function foo
gdb_test "next" ".*" "Next"
gdb_test "step" ".*" "Step into fun foo ()"
gdb_test "info macro M1" ".*"
gdb_test "info macro M2" ".*"
gdb_test "info macro M3" ".*"
gdb_test "macro expand M1" "expands to: M1"
gdb_test "macro expand M2" "expands to: M2"
gdb_test "macro expand M3" "expands to: M3"
gdb_test "print M1" ".*"
gdb_test "print M2" ".*"
gdb_test "print M3" ".*"
gdb_test "print M1+M2+M3" ".*"

gdb_exit

#aCC -g

if  { [gdb_compile "${srcfile}" "${binfile}" executable "{debug}"] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Start the program running.
if {! [runto_main]} {
    fail "macro tests suppressed: couldn't run to main"
    return 0
}

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

send_gdb "info macro M1\n" 
gdb_expect {
  -re "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20" {
	pass "info macro M1"
}
  -re "GDB has no preprocessor macro information for that code.*" { 
	return 0
#	fail "No macro info"
}
}
#gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*file3.*included at.*mac_hdr_chk.*define M3 30"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 30"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*30"
gdb_test "print M1+M2+M3" ".*70"

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

gdb_test "next" ".*" "Next"
gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*mac_hdr_chk.*define M3 35"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 35"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*35"
gdb_test "print M1+M2+M3" ".*75"

#Inside function foo
gdb_test "next" ".*" "Next"
gdb_test "step" ".*" "Step into fun foo ()"
gdb_test "info macro M1" ".*"
gdb_test "info macro M2" ".*"
gdb_test "info macro M3" ".*"
gdb_test "macro expand M1" "expands to: M1"
gdb_test "macro expand M2" "expands to: M2"
gdb_test "macro expand M3" "expands to: M3"
gdb_test "print M1" ".*"
gdb_test "print M2" ".*"
gdb_test "print M3" ".*"
gdb_test "print M1+M2+M3" ".*"

gdb_exit

# Case 1:
# aCC -g +macro_debug=referenced

if  { [gdb_compile "${srcfile}" "${binfile}" executable {list debug additional_flags=+macro_debug=referenced}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Start the program running.
if {! [runto_main]} {
    fail "macro tests suppressed: couldn't run to main"
    return 0
}

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

send_gdb "info macro M1\n" 
gdb_expect {
  -re "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20" {
	pass "info macro M1"
}
  -re "GDB has no preprocessor macro information for that code.*" { 
	return 0
#	fail "No macro info"
}
}
#gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*file3.*included at.*mac_hdr_chk.*define M3 30"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 30"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*30"
gdb_test "print M1+M2+M3" ".*70"

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

gdb_test "next" ".*" "Next"
gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*mac_hdr_chk.*define M3 35"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 35"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*35"
gdb_test "print M1+M2+M3" ".*75"

#Inside function foo
gdb_test "next" ".*" "Next"
gdb_test "step" ".*" "Step into fun foo ()"
gdb_test "info macro M1" ".*"
gdb_test "info macro M2" ".*"
gdb_test "info macro M3" ".*"
gdb_test "macro expand M1" "expands to: M1"
gdb_test "macro expand M2" "expands to: M2"
gdb_test "macro expand M3" "expands to: M3"
gdb_test "print M1" ".*"
gdb_test "print M2" ".*"
gdb_test "print M3" ".*"
gdb_test "print M1+M2+M3" ".*"

gdb_exit

# Case 2:
# aCC -g +macro_debug=all

if  { [gdb_compile "${srcfile}" "${binfile}" executable {list debug additional_flags=+macro_debug=all}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Start the program running.
if {! [runto_main]} {
    fail "macro tests suppressed: couldn't run to main"
    return 0
}

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

send_gdb "info macro M1\n" 
gdb_expect {
  -re "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20" {
	pass "info macro M1"
}
  -re "GDB has no preprocessor macro information for that code.*" { 
	return 0
#	fail "No macro info"
}
}
#gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*file3.*included at.*mac_hdr_chk.*define M3 30"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 30"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*30"
gdb_test "print M1+M2+M3" ".*70"

#(gdb) info macro M1
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file2.h:4
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.h:9
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:4
#define M1 20
#(gdb) info macro M2
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:2
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M2 20
#(gdb) info macro M3
#Defined at /user/wdb/gdb-6.4.50.20060403/gdb/file3.h:3
#  included at /user/wdb/gdb-6.4.50.20060403/gdb/file1.c:5
#define M3 30

gdb_test "next" ".*" "Next"
gdb_test "info macro M1" "Defined at.*file2.*included at.*file1.*included at.*mac_hdr_chk.*define M1 20"
gdb_test "info macro M2" "Defined at.*file3.*included at.*mac_hdr_chk.*define M2 20"
gdb_test "info macro M3" "Defined at.*mac_hdr_chk.*define M3 35"
gdb_test "macro expand M1" "expands to: 20"
gdb_test "macro expand M2" "expands to: 20"
gdb_test "macro expand M3" "expands to: 35"
gdb_test "print M1" ".*20"
gdb_test "print M2" ".*20"
gdb_test "print M3" ".*35"
gdb_test "print M1+M2+M3" ".*75"

#Inside function foo
gdb_test "next" ".*" "Next"
gdb_test "step" ".*" "Step into fun foo ()"
gdb_test "info macro M1" "Defined at.*mac_hdr_chk.*define M1 10"
gdb_test "info macro M2" ".*"
gdb_test "info macro M3" ".*"
gdb_test "macro expand M1" "expands to: 10"
gdb_test "macro expand M2" "expands to: M2"
gdb_test "macro expand M3" "expands to: M3"
gdb_test "print M1" ".*10"
gdb_test "print M2" ".*"
gdb_test "print M3" ".*"

gdb_exit
