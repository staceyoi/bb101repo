
#include <stdio.h>

#include "file1.h"
#include "file3.h"

int
main ()
{
// M1 value at this point should be 20
// M2 value at this point should be 20
// M3 value at this point should be 30
  int tot = M1+M2+M3;

#undef M3
#define M3 35
 
// M1 value at this point should be 20
// M2 value at this point should be 20
// M3 value at this point should be 35
  tot = M1+M2+M3;

  foo ();

  return 0;
}
