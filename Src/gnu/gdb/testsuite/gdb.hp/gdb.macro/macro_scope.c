
#include <stdio.h>

#define USED1 100
#define USED2 200
#define UNUSED1 0
#define UNUSED2 0

int
main ()
{
  int val = USED1;

#undef UNUSED1
#undef USED2
#undef USED1
#define USED1 101 
#define USED2 201
 
  val = USED1 + USED2;

#undef USED1
#undef UNUSED2
#undef USED2
#define USED1 102

  val = USED1;

  return 0;
}
#if 0
b 13, 21, 28
run
info macro USED1 // shud print 100
info macro USED2 // shudnt print anything
cont
info macro USED1 // shud print 101
info macro USED2 // shud print 201
cont
info macro USED1 // shud print 102
info macro USED2 // shudnt print anything
#endif
