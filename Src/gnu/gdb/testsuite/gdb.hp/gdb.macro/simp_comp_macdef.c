#include <stdio.h>
#include <stdlib.h>

int
foo (int i, int j, int k)
{
  return i+j+k;
}

/* Object-like Macros. */
#define M1 10
#define M2 foo(1,2,3)

/* Function-like macros. */
#define MAX(x,y) ((x) > (y) ? (x) : (y));
#define M4(x,y,z) foo(x,y,z)

/* Check the values of BUFSIZE and TABLESIZE. */
#define BUFSIZE 1020
#define TABLESIZE BUFSIZE
#undef BUFSIZE
#define BUFSIZE 37

/* Only info macro and macro expand works for the following macro defns. */
#define STMT(x,y) {\
			int i=x;	\
			int j=y;	\
			int k=i+j;	\
	   	  }
#define M10 x=foo(1,2,3)
#define ASSERT(expr) ((expr) ? 0:1)

/* Check for improper spaces in the macro defns. */
#define add(x,y) x+y
#define add1(x,y) x               +                  y

/* Nested macros */
#define N1 10
#define N2 N1*N1

#define N3(x,y) MAX(x,y)
#define N4(x) MAX(N1,x)

#define N5(N1, N2) MAX(N1,N2)

#define fail(assertion, file, line) 	\
	error (file, line, "Assertion '%s' failed", assertion)
#define assert(expr)			\
	((void) ((expr) ? 0 : fail (#expr, __FILE__, __LINE__)))


/* Deferring the macro inclusion line number using #line directive. */
#line 60
#define T1
#line 60
#define T2
#define T3

/* # and ## operators -- Only the defn of the below macros can be seen.
   Expansion and evaluation will be supported later. */
#define display(arg) cout << #arg << "\n"
#define concat(arg1, arg2) arg1 ## arg2
#define show_me(arg) int var##arg=arg;	\
		 cout << "var" #arg " is " << var##arg << "\n";

/* variadic macros -- info macro works but expand issues a warning.
   gdb) macro epxand eprintf("buggy")
   varargs macros not implemented yet.*/
#define eprintf(...) fprintf(stderr, __VA_ARGS__)

/* Self-referential macros. */
#define self (4+self)

#define x (4 + y)
#define y (2 * x)

/* Funny macros 

1) Expanding the macro: strange(stderr) "gdb", 35) should yield the following o/p. 
   fprintf(stderr, "%s %d", "gdb", 5.6) 
*/
#define strange(file) fprintf(file, "%s %d", ...

int
main ()
{
  int i = M1 + 10;
  return i;  
}
