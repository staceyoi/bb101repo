# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# QXCR1000586067: hang with +check, when gdb version is mismatched or no gdb.

if $tracelevel then {
	strace $tracelevel
}

set testfile QXCR1000586067
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set binfile  ${objdir}/${subdir}/${testfile}

# +check=malloc is available on IA only
if  { ![istarget "ia64*-hp-*"] } {
  return 0
}

#Get information about compilers.
if [get_compiler_info ${binfile} "c++"] {
   return -1
}

source ${binfile}.ci

# aCC5 does not have +check=malloc option.

if { $hp_aCC5_compiler } then {
    return
}

# +check=all is available on IA only for now.
remote_exec build "$CC_FOR_TARGET -g +check=all -mt ${srcfile} -o ${binfile}"

# +check=all uses the default librtc.so in /opt/langtools/lib/hpux..
# to use one in the view we must set SHLIB_PATH.
# It's slightly complicated because it looks for the name librtc.so
# simply pointing to our /CLO/Components/WDB/build/ia64-hp-hpux-native/gdb/
# will not work for both modes 32 & 64.
# setting LIBRTC_SERVER will not work.  Setting SHLIB_PATH is recommended.

set pid [pid]

set rtcdir /tmp/RTC_DIR_$pid

#clean up first
catch "exec rm -rf  ${rtcdir}"
catch "exec mkdir ${rtcdir}"
catch "exec mkdir ${rtcdir}/hpux32"
catch "exec mkdir ${rtcdir}/hpux64"
catch "exec cp  ${objdir}/../librtc.so  ${rtcdir}/hpux32"
catch "exec cp  ${objdir}/../librtc64.so.1  ${rtcdir}/hpux64/librtc.so"

if { "${IS_ILP32}" == "FALSE" } {
   set env(SHLIB_PATH) "${rtcdir}/hpux64"
} else {
   set env(SHLIB_PATH) "${rtcdir}/hpux32"
}

# Set GDB_SERVER to a bad path. Librtc should handle this gracefully
# without hanging in the application.
set env(GDB_SERVER) "/garbage/path/with/no/gdb"

set env(RTC_NO_SELFRTC) "1"

catch "exec ${binfile} > /tmp/${testfile}.out.$pid 2>/tmp/${testfile}.err.$pid"

catch "exec cat /tmp/${testfile}.err.$pid | grep -c garbage" errorString

if { $errorString != 1} then {
    fail "Failed to handle absence of proper gdb"
  } else {
    pass "Detected absence of a proper gdb successfully"
  }

# clean up
catch "exec rm -rf  /tmp/${testfile}.out.$pid"
catch "exec rm -rf  /tmp/${testfile}.err.$pid"

# Check if we have an old gdb installed (on /proj/debugger/GDB_ARCHIVES
# on cup machines).
catch "exec ls /proj/debugger/GDB_ARCHIVES/4.0/gdb.4.0 > out"
catch "exec /usr/bin/cat out | wc -c" output 
if { 0 == $output } {
  xfail " Old gdb not found "
  return 0
}

# Set GDB_SERVER to an old path. Librtc should handle this gracefully
# without hanging in the application.
set env(GDB_SERVER) "/proj/debugger/GDB_ARCHIVES/4.0/gdb.4.0"

catch "exec ${binfile} > /tmp/${testfile}.out2.$pid 2>/tmp/${testfile}.err2.$pid"

catch "exec cat /tmp/${testfile}.err2.$pid | grep -c exited " errorString

if { $errorString != 1} then {
    fail "Failed to handle mismatch with older gdb"
  } else {
    pass "Handled mismatch with older gdb successfully"
  }

# clean up
catch "exec rm -rf  /tmp/${testfile}.out2.$pid"
catch "exec rm -rf  /tmp/${testfile}.err2.$pid"
catch "exec rm -rf  ${rtcdir}"

set env(GDB_SERVER) ""
set env(SHLIB_PATH) ""

return 0
