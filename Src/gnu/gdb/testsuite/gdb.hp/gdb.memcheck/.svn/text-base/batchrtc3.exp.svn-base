# batchrtc.exp -- Expect script to test batch mode leak detection
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

#This file was written by  Kavyashree N.(kavyashree.n@hp.com)

#This file tests the double free detection capacity of batch RTC mode and 
#verifies that output file generated contains the executable name

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile memset
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set oldtimeout $timeout

set additional_flags "additional_flags=-Ae -g"
set ldflags "ldflags=-lpthread"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags $ldflags ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

if { [istarget "hppa1.1-hp-hpux*"] || [istarget "hppa2.0w*-*-hpux*"] } then {
    # PA requires a separate chatr, since passing +dbg to ld doesn't work
    system "chatr +dbg enable ${binfile} > /dev/null"
}

set pid [pid]
set outdir /tmp/batch_$pid

catch "exec rm -rf ${outdir}"
catch "exec mkdir ${outdir}"
catch "exec rm -f rtcconfig"

system "echo 'check_leaks=on'  > rtcconfig"
system "echo 'check_heap=on'  >> rtcconfig"
system "echo 'check_string=on'  >> rtcconfig"
system "echo 'check_free=on'  >> rtcconfig"
system "echo 'files=$testfile' >> rtcconfig"
system "echo 'output_dir=$outdir' >> rtcconfig"
#system "pwd > junk.state.out"
#system "env >> junk.state.out"

if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}

if { [istarget "ia64*-hp-*"] &&  "${IS_ILP32}" == "FALSE" } {
#  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
   set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
} else {
# set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  set env(LD_PRELOAD) "${objdir}/../librtc.sl"
}

#
# Test 1: Check if leaks and heap info is generated
#system "rm -f $outdir/*.mem"
set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/memset  >& $outdir/memset.out.$pid"
set env(BATCH_RTC) "off"


set file1 $outdir/*.mem
system "ls $outdir/*.mem > out"
catch "exec /usr/bin/cat out | wc -l" exec_output

if {[string first $testfile $file1] } then { 
 pass "output file has the exec name"
   } else {
    fail "output file does not have the exec name."
 } 

set mem_file ${outdir}/mem_file
catch "exec /bin/sh \"/bin/mv ${outdir}/*.mem ${mem_file}\" "

catch "exec fgrep \"Attempt to free unallocated or already freed object at\" ${mem_file} > out"
catch "exec /usr/bin/cat out | wc -c" output1
catch "exec fgrep \"memcpy corrupted (size = 4\" ${mem_file} > out"
catch "exec /usr/bin/cat out | wc -c" output2
catch "exec fgrep \"memcpy\" ${mem_file} > out"
catch "exec /usr/bin/cat out | wc -c" output3

catch "exec fgrep \"libc_mem_common\" ${mem_file} > out"
catch "exec /usr/bin/cat out | wc -c" output4

if { $output1 && $output2 && $output3 && $output4 } {
	pass "corruption events caught"
} else {
	fail "corruption events not caught"
}

if { $exec_output } then {
        pass "batchrtc Test: check_string  passed" 
   } else {
       fail "batchrtc Test    :check_string failed"
}
catch "exec /usr/bin/grep \"No such file\" ${outdir}/*.mem > out1"
catch "exec /usr/bin/cat out1 | wc -c" output
if { $output } then {
        fail "batchrtc Test: file name error"
   } else {
        pass "batchrtc Test: no file-name error"
 }

catch "exec rm -rf ${outdir}"
set env(LD_PRELOAD) ""
return 0
