# Copyright (C) 2006 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Checks whether shl_unload causes ??? to appear in RTC reports...

if $tracelevel then {
	strace $tracelevel
}

set prms_id 0
set bug_id 0

set testfile dlclose
set testfile1 dlclose1
set testfile2 dlclose2
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set srcfile1  ${srcdir}/${subdir}/${testfile1}.c
set srcfile2  ${srcdir}/${subdir}/${testfile2}.c
set objfile1  ${objdir}/${subdir}/${testfile1}.o
set objfile2  ${objdir}/${subdir}/${testfile2}.o
set binfile  ${objdir}/${subdir}/${testfile}
set shlib1  libsl1.sl
set shlib2  libsl2.sl

#Skip this for PA - this is only testing stand alone RTC changes
if { ! [istarget "ia64*-hp-*"] } {
 verbose "test ignored for HP PA targets."
 return 0
}

set additional_flags "additional_flags=-Ae -g"

if { [gdb_compile "${srcfile1}" "${objfile1}" object {debug additional_flags=+z -g}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

remote_exec build "ld -b ${objfile1} -o ${shlib1}"

if { [gdb_compile "${srcfile2}" "${objfile2}" object {debug additional_flags=+z -g}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

remote_exec build "ld -b ${objfile2} -o ${shlib2}"

if { [gdb_compile "${srcfile}" "${binfile}" executable [list debug $additional_flags ${shlib1} ${shlib2}]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set pid [pid]
set outdir /tmp/batch_dlopen_leak_$pid

catch "exec rm -rf ${outdir}"
catch "exec mkdir ${outdir}"

catch "exec rm -f ${subdir}/rtcconfig"

system "echo 'check_leaks=on'  > ${subdir}/rtcconfig"
system "echo 'check_heap=on'  >> ${subdir}/rtcconfig"
system "echo 'output_dir=$outdir' >> ${subdir}/rtcconfig"
system "echo 'set heap-check min-leak-size 5'  >> ${subdir}/rtcconfig"

set env(GDBRTC_CONFIG) "${subdir}/rtcconfig"

if { "${IS_ILP32}" == "FALSE" } {
  set env(LD_PRELOAD) "${objdir}/../librtc64.so"
} else {
  set env(LD_PRELOAD) "${objdir}/../librtc.so"
}

set env(BATCH_RTC) "on"

catch "exec ${binfile} > /tmp/${testfile}.log"

set env(BATCH_RTC) "off"
set env(LD_PRELOAD) ""

catch "exec ls ${outdir} | grep heap " heap_output
catch "exec ls ${outdir} | grep leak " leak_output

catch "exec grep goo ${outdir}/${heap_output} | grep dlclose2.c:9 | wc -l " exec_output
if { $exec_output != 1 } then {
    #catch "exec cat ${outdir}/${heap_output}" exec_output
    send_user "Output file :\n$exec_output\n"
    fail "wrong heap output"
} else {
    pass "heap output passed"
}

catch "exec grep goo ${outdir}/${leak_output} | grep dlclose2.c:9 | wc -l " exec_output
if { $exec_output != 1 } then {
    catch "exec cat ${outdir}/${leak_output}" exec_output
    send_user "Output file :\n$exec_output\n"
    fail "wrong leak output - 1"
} else {
    pass "leak output passed - 1"
}

catch "exec grep foo ${outdir}/${leak_output} | grep dlclose1.c:9 | wc -l " exec_output
if { $exec_output != 2 } then {
    catch "exec cat ${outdir}/${leak_output}" exec_output
    send_user "Output file :\n$exec_output\n"
    fail "wrong leak output - 2"
} else {
    pass "leak output passed - 2"
}


# clean up
catch "exec rm -rf ${binfile} ${shlib1} ${shlib2} ${objfile1} ${objfile2} ${subdir}/rtcconfig ${outdir} /tmp/${testfile}.log"

return 0
