#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Test info corruption for corefile

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] {
    return
}


set testfile malloc_3
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set objfile  ${objdir}/${subdir}/${testfile}.o
set binfile  ${objdir}/${subdir}/${testfile}


if { [istarget "ia64*-hp-*"] && "${IS_ILP32}" == "FALSE" } {
  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  set librtc "${objdir}/../librtc64.sl"
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  set librtc "${objdir}/../librtc.sl"
}

# use this only to test on 11.23 
# to pick up new libc, remove this for 11.31
#if { [istarget "ia64*-hp-*"] } {
#  set env(SHLIB_PATH) /home/trangto/wdb/RTC/libc-dec-16/em_32:/home/trangto/wdb/RTC/libc-dec-16/em_64
#} else {
#  set env(SHLIB_PATH) /home/trangto/wdb/RTC/libc-dec-16/pa_32:/home/trangto/wdb/RTC/libc-dec-16/pa_64
#}

set foo [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug}]
if { "${foo}" != "" } {
    perror "Couldn't compile ${testfile}.c"
    perror "Output is '${foo}'"
    return -1
}


# Start with a fresh gdb

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Switch off this test for non 11.31.
# set 1131_kern 0

# send_gdb "shell uname -r\n"
# gdb_expect {
  # -re ".*11.31.*$gdb_prompt $" { set 1131_kern 1; pass "uname 1131" }
  # -re ".*$gdb_prompt $" { pass "not uname 1131" }
# }

# if { $1131_kern==0 } {
  # gdb_exit;
  # return;
# }



gdb_test "set heap-check on" ""  ""
gdb_test "b set_brkpt_here" "Breakpoint 1.*"  "Breakpoint"
gdb_test "run" ".*Breakpoint 1.*"  "Stopped"


gdb_test "info corruption" ".*Total bytes.*100.*Beginning.*corrupt_header.*100.*End of block.*corrupt_footer.*" "info corruption for live process"

gdb_test "info corruption 0" ".*100 bytes at.*corrupt_header.*malloc_3.c:39.*in main.*malloc_3.c:59.*" "info corruption 0"
gdb_test "info corruption 401" ".*100 bytes at.*corrupt_footer1.*malloc_3.c:8.*in main.*malloc_3.c:60.*" "info corruption 400"

set timeout 160
send_gdb "dumpcore\n"
gdb_expect {
  -re "Dumping core to the core file (core.\[0-9\]+).*$gdb_prompt $" {
    set corefile $expect_out(1,string)
    pass "dumpcore"
  }
  -re ".*$gdb_prompt $" { fail "dumpcore - output did not match" }
  timeout           { fail "(timeout) dumpcore" }
}

gdb_exit


remote_exec build "cp  ${binfile} ${objdir}"

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_test "core  ${corefile}"  ".*was generated.*" "loaded corefile"
gdb_test "info corruption" ".*Total bytes.*100.*Beginning.*corrupt_header.*100.*End of block.*corrupt_footer.*" "info corruption on core"
gdb_test "info corruption 0" ".*100 bytes at.*corrupt_.*malloc_3.c.*in main.*malloc_3.c.*" "info corruption 0"
gdb_test "info corruption 401" ".*100 bytes at.*corrupt_.*malloc_3.c.*in main.*malloc_3.c.*" "info corruption 400"



gdb_exit

# clean up 
remote_exec build "rm -rf ${corefile}"
remote_exec build "rm -rf ${objdir}/${testfile}"

return 0










