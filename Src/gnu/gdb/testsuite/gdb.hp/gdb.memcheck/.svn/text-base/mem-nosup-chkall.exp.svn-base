# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# JAGag20732: 11.31 Apps testing:tcsh:signal 11 with +check option.

# Checks whether shl_findsym is user defined or wrapper functions are not present and gives
# warning that memory debugging is not supported with +check=all compiler option. Suganya Thirumurthy

if $tracelevel then {
	strace $tracelevel
}

set prms_id 0
set bug_id 0

set pid [pid]

set testfile mem-nosup
set testfile1 gmalloc
set testfile2 xmalloc
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set srcfile1  ${srcdir}/${subdir}/${testfile1}.c
set srcfile2  ${srcdir}/${subdir}/${testfile2}.c
set binfile  ${objdir}/${subdir}/${testfile}

# +check=malloc is available on IA only
if  { ![istarget "ia64*-hp-*"] } {
  return 0
}

#Get information about compilers.
if [get_compiler_info ${binfile} "c++"] {
   return -1
}

source ${binfile}.ci

# aCC5 does not have +check=malloc option.

if { $hp_aCC5_compiler } then {
    return
} 

# +check=all is available on IA only for now.
# when it's avail on PA just remove this if condition
remote_exec build "$CC_FOR_TARGET -g +check=all ${srcfile} ${srcfile1} ${srcfile2} -o ${binfile}"

# +check=all uses the default librtc.so in /opt/langtools/lib/hpux..
# to use one in the view we must set SHLIB_PATH.
# It's slightly complicated because it looks for the name librtc.so
# simply pointing to our /CLO/Components/WDB/build/ia64-hp-hpux-native/gdb/
# will not work for both modes 32 & 64.
# setting LIBRTC_SERVER will not work.  Setting SHLIB_PATH is recommended.

set rtcdir /tmp/RTC_DIR_$pid

#clean up first
catch "exec rm -rf  ${rtcdir}"
catch "exec mkdir ${rtcdir}"
catch "exec mkdir ${rtcdir}/hpux32"
catch "exec mkdir ${rtcdir}/hpux64"
catch "exec cp  ${objdir}/../librtc.so  ${rtcdir}/hpux32"
catch "exec cp  ${objdir}/../librtc64.so.1  ${rtcdir}/hpux64/librtc.so"

if { "${IS_ILP32}" == "FALSE" } {
   set env(SHLIB_PATH) "${rtcdir}/hpux64"
} else {
   set env(SHLIB_PATH) "${rtcdir}/hpux32"
}

set env(GDB_SERVER) "${objdir}/../gdb"

catch "exec ${binfile} >& /tmp/${testfile}.out.$pid"

catch "exec cat /tmp/${testfile}.out.$pid | wc -l" exec_output

if { $exec_output } {
  if { [catch "exec fgrep -c Memory $exec_output"] } then {
    pass "Memory debugging is not supported if shl_findsym is user defined."
  } else {
    fail "Failed to issue the Memory debugging is not supported warning iwth +check=all"
  }
} else {
    fail "Failed to issue the Memory debugging is not supported warning with +check=all"
}

# clean up
catch "exec rm -rf /tmp/${testfile}.out.$pid"
catch "exec rm -rf  ${rtcdir}"

set env(GDB_SERVER) ""
set env(SHLIB_PATH) ""

return 0
