# memleaks5.exp -- Expect script to test a memory leak of threaded program
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile memleaks6
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set oldtimeout $timeout

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}

#
set additional_flags "additional_flags=-Ae"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags ldflags=-lpthread]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit

gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "set heap-check on" "" ""
gdb_test "b main" "" ""
gdb_test "b 175" "" ""

gdb_test "run" "" ""

# gdb_test "continue" "" ""

send_gdb "continue\n"
gdb_expect {
  -re ".*Test starting.*Breakpoint 2.*$gdb_prompt" { pass "continue" }

  -re ".*Program received signal SIGTRAP.*Trace/breakpoint trap.*$gdb_prompt" {
    # Switch off this test for 11.22. Setting of natr32 on 11.22 is not working.
    set 1122_kern 0

    send_gdb "shell uname -r\n"
    gdb_expect {
      -re ".*11.22.*$gdb_prompt $" { set 1122_kern 1 }
      -re ".*$gdb_prompt $" {}
    }

    if { $1122_kern==1 } {
      xfail "continue - 11.22 kernel defect giving the child process a SIGTRAP"
      gdb_exit;
      return;
    }
    fail "continue - SIGTRAP, but not on 11.22 where it is known to happen"
    gdb_suppress_tests
    }

  -re ".*$gdb_prompt" { fail "continue" }
  timeout {
    fail "continue (timeout)"
  }
}

send_gdb "info leaks\n"
gdb_expect {
  -re ".*do_it.*$gdb_prompt" { pass "info leaks" 
   }
  -re ".*$gdb_prompt" { fail "info leaks" 
   }
  timeout {
    fail "info leaks (timeout)"
  }
}
gdb_exit

gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "b main" "" ""
gdb_test "b 175" "" ""

gdb_test "run" "" ""
gdb_test "n" "" ""
send_gdb "set heap-check on\n"
gdb_expect {
  -re ".*librtc.*is not loaded.*$gdb_prompt" { pass "librtc not loaded"
  }
  -re ".*$gdb_prompt" { fail "librtc not loaded" 
   }
  timeout {
    fail "librtc not loaded (timeout)"
  }
}
gdb_exit

gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "b main" "" ""
gdb_test "b 175" "" ""
send_gdb "set heap-check on\n"

gdb_test "run" "" ""
gdb_test "n" "" ""
gdb_test "set heap-check off" "" ""
gdb_test "continue" "" ""
send_gdb "set heap-check on\n"
gdb_expect {
  -re "warning.*$gdb_prompt" { pass "set heap-check on"
  }
  -re ".*$gdb_prompt" { fail "set heap-check on" 
   }
  timeout {
    fail "set heap-check on (timeout)"
  }
}
gdb_exit

gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "b main" "" ""
gdb_test "b 175" "" ""
send_gdb "set heap-check leaks on\n"

gdb_test "run" "" ""
gdb_test "n" "" ""
gdb_test "continue" "" ""
send_gdb "set heap-check leaks on\n"
gdb_expect {
  -re "$gdb_prompt" { pass "set heap-check leaks on"
  }
  -re ".*$gdb_prompt" { fail "set heap-check leaks on" 
   }
  timeout {
    fail "set heap-check leaks on (timeout)"
  }
}
gdb_exit

gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "b main" "" ""
gdb_test "b 175" "" ""
send_gdb "set heap-check on\n"

gdb_test "run" "" ""
gdb_test "n" "" ""
send_gdb "set heap-check on\n"
gdb_test "continue" "" ""
send_gdb "set heap-check free on\n"
gdb_expect {
  -re "$gdb_prompt" { pass "set heap-check free on"
  }
  -re ".*$gdb_prompt" { fail "set heap-check free on" 
   }
  timeout {
    fail "set heap-check free on (timeout)"
  }
}
gdb_exit

return 0
