# memleaks8.exp -- Expect script to test memory leak detection in a 
#  doubly-linked list (JAGag22836: Stack growth failure on "info leaks").
# Excessive recursion used to make the mark() routine stack overflow.
#
# Copyright (C) 2006 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX RTC test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile memleaks8
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

# for some reason the PA32 version is very slow
set oldtimeout $timeout
set timeout 350

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}


#
set additional_flags "additional_flags=-Ae"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}


gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "set heap-check on" "" ""

gdb_test "b 22" "Breakpoint 1.*memleaks8.*" "set bp at 22"
gdb_test "run" ".*Breakpoint 1.*main.*memleaks8.*" "run to 22"

gdb_test "info leaks" ".*Scanning for memory leaks.*No new leaks.*" "info leaks"

set timeout $oldtimeout
gdb_exit
return 0
