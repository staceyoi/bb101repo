# attach_rtc.exp -- Expect script to test a memory leak of threaded program
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu
#
# This test is similar to test_rtc.exp.  It checks rtc
# bit from chatr.  On wasp, expect no rtc bit so it should exit
# with 1 pass (message it's exiting).  Otherwise, on newer systems 
# such as cevrst00 expect more passes.  
# 
# pick up /bin/chatr so it's consistent across all users


if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "Test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "Test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile attach_rtc
if { [istarget "ia64*-hp-*"] && "${IS_ILP32}" == "TRUE" } {
  catch "exec /usr/sbin/kctune maxdsiz > out"
  catch "exec grep maxdsiz out > out1"
  catch "exec cut -d \" \" -f 3 out1" maxdsiz
  catch "exec rm -f out out1"
  if { $maxdsiz <= "0xc0000000" } {
    set testfile attach_rtc_sm
  }
}

set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set oldtimeout $timeout
set timeout 260

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}

set additional_flags "additional_flags=-Ae"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable \
	[list debug ${additional_flags} ldflags=-lpthread ] ] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit

set pid [pid]
set outdir /tmp/RTCout_$pid

# clean up before creating the dir
catch "exec rm -rf ${outdir}"
catch "exec mkdir ${outdir}"

#Test if we are using the right version of chatr and dld
system "/bin/chatr ${binfile} > ${outdir}/out_msg 2>&1"

catch "exec fgrep \"rtc value\" ${outdir}/out_msg > ${outdir}/chatr.out"

catch "exec cat ${outdir}/chatr.out | wc -l"  exec_output

#Check if we are using right version of chatr
if { $exec_output == 1 } then {
    pass "check rtc "
} else {
    pass "rtc is not available so exit"
    catch "exec rm -rf ${outdir}"
    return 0
}

system "/bin/chatr +rtc enable ${binfile} > /dev/null"


set GDBFLAGS "-nx -leaks"

set testpid [eval exec "$binfile &"]
# send_log "$testpid ddd"
set env(LD_PRELOAD) ""
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "attach $testpid\n"
gdb_expect {
  -re "Attaching to.*process $testpid.*$gdb_prompt $" {
    pass "attach succeeded"
  }
  -re "$gdb_prompt $" {fail "attach failed"}
    timeout         {fail "(timeout) attach call"}
  }

# force gdb to make ${srcfile} the current file before we set the breakpoint by line number
#gdb_test "list main" ".*main.*110.*pthread_attr_init.*"
gdb_test "b ${srcfile}:125" ".*Breakpoint 1 at 0x.*line 125.*"
gdb_test "p wait_here=0" ".* = 0.*"

#gdb_test "c" ".*Continuing.*"
send_gdb "continue\n"
gdb_expect {
  -re "Continuing.*Program received signal SIGSEGV.*$gdb_prompt $" {
    fail "SIGSEGV on continue"
    gdb_test "bt" ".*.*"
  }
  -re ".*Continuing.*Breakpoint .*${srcfile}:125.*$gdb_prompt $" {
    pass "continue"
  }
  -re ".*$gdb_prompt $" {
    fail "continue - no breakpoint hit?"
  }
  timeout {
    fail "continue (timeout)"
  }
}

# debugging test points

send_gdb "info leaks\n"
gdb_expect {
  -re "Scanning for memory leaks.*bytes leaked in.*Total bytes.*spin.*$gdb_prompt $" {
    pass "info leaks "
  }
  -re ".*Program received signal SIGSEGV, Segmentation fault.*$gdb_prompt $" {
    fail "info leaks - stack overflow?"
  }
  -re ".*$gdb_prompt $" {
    fail "info leaks - no leak report?"
  }
  timeout {
    fail "info leaks (timeout)"
  }
}

gdb_test "c" ".*Continuing.*"

set timeout $oldtimeout
gdb_exit

# clean up process in case it didn't exit.
catch "exec kill -9 ${testpid}" 
#catch "exec rm -f ${binfile}"

# clean up tmp output file
catch "exec rm -rf ${outdir}"

return 0
