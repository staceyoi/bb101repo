# rtcapi_intr.exp -- Expect script to test RTC-API's interactive mode  memory scrambling
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "hppa*-*-hpux11.*"] } {
    verbose "This test ignored for hppa targets."
    return 0
}

set testfile rtcapi_intr
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if { "${IS_ILP32}" == "FALSE" } {
  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug} ] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set env(GDB_SERVER) "${objdir}/../gdb"

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}
gdb_test "set heap-check on" ""  ""
gdb_test "b main" "Breakpoint 1.*"  "Breakpoint"
gdb_test "run" ".*Breakpoint 1.*"  "Stopped"
gdb_test "print rtc_enable(31)" ".*RTC is initialized.* = 0.*" "RTC is initialized"
gdb_test "next 4" ".*" ""
gdb_test "print rtc_leaks(1)"  ".*New Leaks report.*80 bytes leaked in 1 blocks.*func().*func().*" "p rtc_leaks(1)"
gdb_test "next 4" ".*" ""
gdb_test "print rtc_leaks(0)"  ".*All Leaks report.*240 bytes leaked in 3 blocks.*func().*func().*" "p rtc_leaks(0)"
gdb_test "c" "" ""
gdb_exit 

return 0
