# scramble.exp -- Expect script to test RTC batch mode memory scrambling
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile scramble1
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}
set pid [pid]

# Test 1: Do the +check mode scramble check..

# +check=malloc is available on IA only
if  { ![istarget "ia64*-hp-*"] } {
  return 0
}

# when it's avail on PA just remove this if condition
if  { [istarget "ia64*-hp-*"] } {
  if [get_compiler_info ${binfile} "c++" ] {
    return -1;
  }
  source ${binfile}.ci

# Only acc6 and above have +check malloc support, so bail out if its an old compiler
  if {!$hp_aCC6_compiler} {
    return 0
  }
}

remote_exec build "$CC_FOR_TARGET -g +check=malloc ${srcfile} -o ${binfile}"

set rtcdir /tmp/RTC_DIR_$pid
#clean up first
catch "exec rm -rf  ${rtcdir}"
catch "exec mkdir ${rtcdir}"
catch "exec mkdir ${rtcdir}/hpux32"
catch "exec mkdir ${rtcdir}/hpux64"
catch "exec cp  ${objdir}/../librtc.so  ${rtcdir}/hpux32"
catch "exec cp  ${objdir}/../librtc64.so.1  ${rtcdir}/hpux64/librtc.so"

if { "${IS_ILP32}" == "FALSE" } {
   set env(SHLIB_PATH) "${rtcdir}/hpux64"
} else {
   set env(SHLIB_PATH) "${rtcdir}/hpux32"
}

set env(GDB_SERVER) "${objdir}/../gdb"

catch "exec ${binfile} >& /tmp/scramble.out.$pid" exec_output
set env(SHLIB_PATH) ""

# we expect 50 matching patters for this test case; 25 for malloc'd pattern and
# 25 pattern after free()
catch "exec cat /tmp/scramble.out.$pid | grep -c feedface" matchingPatterns

if { $exec_output > 62 } then {
    fail "Test 1:scramble +check test failed - too many lines of output($exec_output > 1 expected)"
    catch "exec cat /tmp/scramble.out.$pid" exec_output
    send_user "Output file :\n$exec_output\n"
} elseif { $matchingPatterns != 50 } then {
    pass "Test 1:scramble +check test passed - scramble mode turned off wiht +check mode."
} else {
    pass "Test 1:scramble +check test passed: scramble pattern found in malloc'd and freed block"
}

# clean up
catch " exec /bin/sh \"/bin/rm -f /tmp/${testfile}.*.heap \" "
catch "exec /bin/rm -f /tmp/${testfile}.out.$pid rtcconfig /tmp/scramble.out.$pid"
catch "exec rm -rf ${binfile} ${rtcdir}"

return 0
