#include <stdio.h>
#include <string.h>
#include <malloc.h>

void memcpy_family_warn()
{
  char *p, buffer[]="123456789123456";

  p = calloc (5, 1);
  memset (p, 'Z', 4); /* ok */
  memmove (p, "abcd", 5); /* ok */
  memcpy (p, "abcd", 5); /* ok */
  memccpy (p, "abcd", 'Z', 5); /* ok */

//corruptions
  memcpy(p, buffer, 6);
  memmove(p, buffer, 6);
  memset(p, 2, 6);
  memccpy(p, buffer, '7',  12);
  _memcpy(p, buffer, 6);
  _memset(p, 2, 6);
}

int main()
{
  memcpy_family_warn();
}




