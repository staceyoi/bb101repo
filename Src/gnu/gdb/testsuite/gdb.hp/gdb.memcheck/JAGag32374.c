#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

#define OUTER_LOOP 1000
#define INNER_LOOP 75
int *pointer_store[OUTER_LOOP][INNER_LOOP];

// Make sure we have aleast 30000 allocations are coming from SBA and hence configuring the SBA this way....
extern int __hp_malloc_num_smallblocks=10000;  // the no of elements in each pool
extern int __hp_malloc_grain=16; // pool size is a multiple of 16
extern int __hp_malloc_maxfast=200; // alloctions of size <= 200 go into SBA...

int main()
{

 int i,j,k;
 int *ret_val;

 printf("The value of SBA opts maxfast = %d, num SBA = %d, grain size = %d \n", __hp_malloc_maxfast, __hp_malloc_num_smallblocks, __hp_malloc_grain);

 for(i=0; i < OUTER_LOOP; i++)
 {
	for(j=0; j < INNER_LOOP; j++)
	{
		// Once in a while ask for memory that should come from the general pool as well
		if( !(j % 15)){
			ret_val = calloc(200,sizeof(int));
		} else {
			ret_val = calloc(j,sizeof(int));
		}
		if (ret_val == NULL) {
			printf(" Failed to allocate memory \n");
			exit(1);
		}
		pointer_store[i][j] = ret_val;

		// Write a known pattern to allocated block...
		for(k=0; k < j; k++)
		 ret_val[k] = 'A';
        }
 }

 // Lets try to access the value... Either you will find the pattern corrupted or the application would have dumped by now..

 for(i=0; i < OUTER_LOOP; i++)
 {
	for(j=0; j < INNER_LOOP; j++)
	{
		ret_val = pointer_store[i][j] ;

		// Read and check the known pattern in allocated block...
		for(k=0; k < j; k++)
		{
		  //printf ("ret_val[%d] = %d ", k,ret_val[k] );
		  if (ret_val[k] != 'A' ) {
			printf ("Heap Corrupted for i= %d, j=%d \n",i,j);
                        exit(1);
		    };
		}
	}
}
// Echo the same message multiple times, so there is more no of lines in the output in 
// the successful case than the core dump case
 printf(" Program runs OK. Heap is OK \n");
 printf(" Program runs OK. Heap is OK \n");
 printf(" Program runs OK. Heap is OK \n");

}

