# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

#JAGag32374 - We will test for the heap corruption. The test program does a lot of
# mallocs and writes a pattern into it. At the end, it tries to check if the pattern
#in intact.
#Either you will find the pattern corrupted or the application would dump core..

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile JAGag32374
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}


set oldtimeout $timeout

set additional_flags "additional_flags=-Ae -g"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

if { [istarget "hppa1.1-hp-hpux*"] || [istarget "hppa2.0w*-*-hpux*"] } then {
    # PA requires a separate chatr, since passing +dbg to ld doesn't work
    system "chatr +dbg enable ${binfile} > /dev/null"
}

set pid [pid]
set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec rm -rf ${outdir}"
catch "exec mkdir ${outdir}"

#set the environment variables
if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}

if { [istarget "ia64*-hp-*"] &&  "${IS_ILP32}" == "FALSE" } {
   set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
} else {
  set env(LD_PRELOAD) "${objdir}/../librtc.sl"
}

##Test: For corruption in Batch Mode.

#set the config spec
system "echo 'set heap-check leaks on'  > rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 4 } then {
   pass "Program executed properly without any errors."
  } else {
   fail "Program failed to execute properly"
}

set file1 [ glob -nocomplain ${outdir}/${testfile}.out ]

if { [file size $file1] } then { 
    catch "exec fgrep -c \"Heap is OK\" ${outdir}/${testfile}.out" patternNum
    if { $patternNum == 3 } then {
      pass "No Heap corruption"
    } else {
      fail "Either the Application crashed/heap has been corrupted"
    }
} else {
   fail "Program output is empty."
}

#catch "exec rm -rf ${outdir}"
#catch "exec rm -f ${objdir}/rtcconfig"

set env(LD_PRELOAD) ""
set env(GDB_SERVER) ""

return 0
