#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef unsigned short T1;

void *ptrs[10];

int main(int argc, char **argv)
{
  unsigned long long N = 536870912;
  unsigned long long tot = 0;
  unsigned long long chunk = 0;
  unsigned seed = 101;
  if (argc == 2) {
    sscanf(argv[1], "%u", &seed);
  }
  srand(seed);
  if (argc == 99)
    N = 10;
  while (N) {
    N--;
    unsigned amt = rand();
    if ((N % 20) == 0) 
      amt <<= 6;
    else if ((N % 10) == 0) 
      amt = amt & 7;
#if 0
    printf("%s%s%u ", 
	   ((N % 20) == 0) ? "B" : "",
	   ((N % 10) == 0) ? "S" : "",
	   amt);
    if ((N & 7) == 0) 
      printf("\n");
#endif
    tot += amt;
    chunk += amt;
    if (chunk > 104857600) {
//      printf("%llu\n", tot);
      chunk = 0;
    }

    void *p = malloc(amt);
    ptrs[N&0x3] = p;
#if 0
    *((char *) p) = '\0';
#endif
  }
 // printf("%p\n", ptrs[3]);
  return 0;
}

