#include <malloc.h>

/* to check for corruption turn do set heap-check bounds on */
void corrupt_footer1()
{   
    
  char * cp;

  cp = malloc (100);
  /* destroy the feedface */
  cp[120] = 120;
  cp[121] = 121;
  //free (cp);  /* should trigger event */
}
    

    
void corrupt_header()
{   
    
  char * cp; 
  cp = malloc (100);
  /* destroy header */
  cp[-20] = 88;
  cp[-21] = 99;
  //free (cp);  /* should trigger event */
}

#pragma OPTIMIZE OFF
void leakage()
{

   char *ptr;
   ptr = malloc(200);
   ptr = malloc(100);
   ptr = malloc(200);
   ptr = malloc(100);

}

int main()
{ 
  int i;
 
  printf("Testing Prog guard bytes feature\n");
  corrupt_header();
  corrupt_footer1();
  leakage();
}
