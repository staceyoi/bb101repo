#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Allocate memory and return it to the caller */
void *allocateMemory(size_t n)
{
        void *memBlock = NULL;
        printf("Allocating memory of size %d\n", n);

        memBlock = malloc(n);
        if (!memBlock)
        {
                printf("No memory available, aborting...\n");
                abort();
        }

        return memBlock;
}

/* Create a header string and return to the caller */
char *createHeader()
{
        char *headerString = (char *) allocateMemory(20);
        strcpy (headerString, "Header : My string ");
        return headerString;
}

/* Create a string with header and input. Return to the caller */
char *createString(char *input, int n)
{
#define MAXBADINPUT (10)

        char *hdrString, *myString;
        int inputLength = strlen(input);
        static int badInput;

        hdrString = createHeader();

        /* Input length too small ? return back from here */
        if (n < inputLength + 20)
        {
                hdrString = NULL;
                printf("Too small size for a string, not allocating\n");
                badInput++;
                printf("Number of badInput is %d\n", badInput);
                if (badInput > MAXBADINPUT)
                {
                        printf("Too many bad inputs.. aborting\n");
                        abort();
                }
                return NULL;
        }

        /* Allocate the string, copy the header to it */
        myString = (char *)allocateMemory(n);
        strcpy(myString, hdrString);
        free(hdrString);

        /* Copy the input to the string after header and return */
        strcpy(myString+20, input);
        return (myString);
}

void compareStrings()
{
        char *str1 = NULL, *str2 = NULL;

        /* Allocate first string and initialize it */
        str1 = createString("String 1", 10);

        if (!str1)
          str1 = createString("String 1", 40);

        str2 = createString("String 2", 40);

        /* Compare strings */
        if (strcmp(str1, str2))
        {
                printf("str1 > str2 \n");
        }
        else
        {
                printf("str1 <= str2 \n");
        }

        free(str1);
        free(str2);
}

void foo()
{
	printf("We reached end\n");
}

int main()
{
        /* Example memory leak program */
        compareStrings();
	foo();
        exit(0);
}
