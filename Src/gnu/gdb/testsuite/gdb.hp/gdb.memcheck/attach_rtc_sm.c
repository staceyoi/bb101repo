/*
To compile: cc -Ae -g -o attach_rtc32 -lpthread attach_rtc32.c
To run: attach_rtc32
*/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#ifndef WAIT_HERE
#define WAIT_HERE	1
#endif
#define N_THREADS 3
#define MAX_LOCAL_VAL 40
#define OUTER_LOOP_COUNT 3
#define LIST_LEN 280000

struct first_ptr {
   struct first_ptr *next;
   int node_num;
};

struct last_ptr {
   int node_num;
   struct last_ptr *next;
};

/* function to clean (zero) the thread stacks */
void *clean ( void * );

struct first_ptr *first_ptr_list = NULL;
struct last_ptr *last_ptr_list = NULL;

pthread_attr_t pthread_attribute;

/* Build lengthy linked lists to stress recursive mark() operation in RTC
   library.  Simulates an RSE stack overflow problem found on IPF.  */

void build_linked_list(void)
{
   int i;
   struct first_ptr *first_p;
   struct last_ptr *last_p;

   for (i = 0; i < LIST_LEN; i++) {
      first_p = malloc(sizeof(struct first_ptr));
      first_p->next = first_ptr_list;
      first_p->node_num = i;
      first_ptr_list = first_p;

      last_p = malloc(sizeof(struct last_ptr));
      last_p->next = last_ptr_list;
      last_p->node_num = i;
      last_ptr_list = last_p;
      }
}

/* Routine for each thread to run, does nothing. */
void *spin( vp )
    void * vp;
{
   void * ll;
   int i, cnt = 0;

   for (i=0; i < 10000; i++)
       cnt += i;
   ll = malloc(i*100);
   ll = (void *) NULL;	/* ensure pointer not left around in register */
   return ((void *) &ll);
}

void
do_pass( int pass , void *fn( void * ) )
    
{
    int i, err;
    pthread_t t[ N_THREADS ];

    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_create( &t[i], NULL, fn, (void *)i );
        if( err != 0 ) {
            printf( "pthread_create error (err=%d) for thread %d\n", err, i );
        }
    }

    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_join(t[i], NULL );
        if( err != 0 ) {
            printf( "pthread_join error for thread %d\n", i );
        }
    }
}

void
do_it()
{
    int i;
    
    for( i = 0; i < OUTER_LOOP_COUNT; i++ )
        do_pass( i, spin );
}

volatile int wait_here = WAIT_HERE;

main()
{
   register char *ll;

   int retval = pthread_attr_init(&pthread_attribute);
   retval = pthread_attr_setscope(&pthread_attribute, PTHREAD_SCOPE_SYSTEM);
   while(wait_here);

   (void) alarm(600);
   ll = malloc (21021);
   ll = malloc (20020);
   ll = malloc (10010);
   ll = NULL;
   do_it();
   do_pass( 0, clean );
   build_linked_list();

   ll = malloc (25025);
   ll = malloc (15015);
   return(0);
}

/* Routine for each thread to run, zeroes the thread stack to prevent 
   false negatives on leaks. */
void *clean( vp )
    void * vp;
{
   void * ll[MAX_LOCAL_VAL];
   int i;

   for (i=0; i < MAX_LOCAL_VAL; i++)
       ll[i] = 0;
   return ((void *) 0);
}

