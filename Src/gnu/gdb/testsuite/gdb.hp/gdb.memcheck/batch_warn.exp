# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

# This script tests Batch RTC warning messages for the new form of commands.

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile newcmd_batch
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set oldtimeout $timeout

set additional_flags "additional_flags=-Ae -g"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

if { [istarget "hppa1.1-hp-hpux*"] || [istarget "hppa2.0w*-*-hpux*"] } then {
    # PA requires a separate chatr, since passing +dbg to ld doesn't work
    system "chatr +dbg enable ${binfile} > /dev/null"
}

set pid [pid]
set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec rm -rf ${outdir}"
catch "exec mkdir ${outdir}"

#set the environment variables
if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}

if { [istarget "ia64*-hp-*"] &&  "${IS_ILP32}" == "FALSE" } {
   set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
} else {
  set env(LD_PRELOAD) "${objdir}/../librtc.sl"
}

#Test 1 : Check the warning message when wrong frame-count value is given.
# "set heap-check frame-count <num>" when <num> value is negative a warning message
# "Invalid frame-count value. Using default value." is given.

#set the config spec
system "echo 'set heap-check on'  > rtcconfig"
system "echo 'set heap-check frame-count -6'  >> rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 6 } then {
   pass "Program executed with warning message."
  } else {
   fail "Program executed without warning message."
}

catch "exec fgrep -c \"Invalid frame-count value\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message for frame-count is found."
} else {
   fail "Warning message for frame-count is not found."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*heap ]

if { [file size $file1] } then { 
    catch "exec /bin/sh \"/bin/cp ${outdir}/${testfile}.*.heap ${outdir}/heapfile\" "
    catch "exec fgrep -c \"#5  main\" ${outdir}/heapfile" heapnum
    if { $heapnum > 1 } then {
      fail "frame-count feature is not working properly for heap file."
    } else {
        catch "exec fgrep -c \"#3 \" ${outdir}/heapfile" heapnum1
        if { $heapnum1 >= 1 } then {
           pass "frame-count feature is working properly taking default value for heap file."
        } else {
           fail "frame-count feature is not taking default value for heap file."
       }
    }
} else {
   fail "Heap report is empty."
}

set file2 [ glob -nocomplain ${outdir}/${testfile}*leaks ]

if { [file size $file2] } then { 
    catch "exec /bin/sh \"/bin/cp ${outdir}/${testfile}.*.leaks ${outdir}/leaksfile\" "
    catch "exec fgrep -c \"#5  main\" ${outdir}/leaksfile" leaknum
    if { $leaknum > 1 } then {
      fail "frame-count feature is not working properly for leak file."
    } else {
        catch "exec fgrep -c \"#3 \" ${outdir}/leaksfile" leaknum1
        if { $leaknum1 >= 1 } then {
           pass "frame-count feature is working properly taking default value for leak file."
        } else {
          fail "frame-count feature is not taking default value for leak file."
        }
    }
} else {
   fail "Leak report is empty."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

#Test 2 : Check the warning message when wrong min-leak-size value is given.
# "set heap-check min-leak-size <num>" when <num> value is negative a warning message
# "Invalid min-leak-size value. Ignoring option" is given.

set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec mkdir ${outdir}"

#set the config spec
system "echo 'set heap-check on'  > rtcconfig"
system "echo 'set heap-check min-leak-size -101'  >> rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 6 } then {
   pass "Program executed with warning message."
  } else {
   fail "Program executed without warning message."
}

catch "exec fgrep -c \"Invalid min-leak-size value\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message for min-leak-size is found."
} else {
   fail "Warning message for min-leak-size is not found."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*leaks ]

if { [file size $file1] } then { 
    catch "exec /bin/sh \"/bin/cp ${outdir}/${testfile}.*.leaks ${outdir}/leakfile\" "
    catch "exec fgrep -c \"\\?\\?\\?\" ${outdir}/leakfile" leak_data
    if { $leak_data == 2 } then {
      fail "min-leak-size feature is not working properly for negative value."
    } else {
        catch "exec fgrep -c \"1.*\\?\\?\\?\" ${outdir}/leakfile" leak_data
        if { $leak_data >= 1 } then {
         fail "min-leak-size feature not working properly."
        } else {
         pass "min-leak-size feature works properly showing leak for default value."
        } 
    }
} else {
   fail "Leak report is empty."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

#Test 3 : Check the warning message when wrong min-heap-size value is given.
# "set heap-check min-heap-size <num>" when <num> value is negative a warning message
# "Invalid min-heap-size value. Ignoring option" is given.

set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec mkdir ${outdir}"

#set the config spec
system "echo 'set heap-check on'  > rtcconfig"
system "echo 'set heap-check min-heap-size -201'  >> rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 6 } then {
   pass "Program executed with warning message."
  } else {
   fail "Program executed without warning message."
}

catch "exec fgrep -c \"Invalid min-heap-size value\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message for min-heap-size is found."
} else {
   fail "Warning message for min-heap-size is not found."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*heap ]

if { [file size $file1] } then { 
    catch "exec /bin/sh \"/bin/cp ${outdir}/${testfile}.*.heap ${outdir}/heapfile\" "
    catch "exec fgrep -c \"101 bytes at\" ${outdir}/heapfile" heap_data
    if { $heap_data == 1 } then {
      pass "min-heap-size feature is working properly with negative value."
    } else {
      fail "min-heap-size feature is not working properly."
    }

} else {
   fail "Heap report is empty."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

#Test 4 : Check the warning message when wrong value for on/off is given.
# "set heap-check options on/off" when options other than on/off s given, a warning message
# "Invalid option "string". Ignoring option" is given.

set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec mkdir ${outdir}"

#set the config spec
system "echo 'set heap-check string okk'  > rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 3 } then {
   pass "Program executed properly without any errors."
  } else {
   fail "Program failed to execute properly."
}

catch "exec fgrep -c \"Invalid option\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message for wrong string in place of on/off is found."
} else {
   fail "Warning message for wrong string in place of on/off is not found."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*heap ]

if {  [file exists $file1] } then {
    fail ".mem corruption file is generated. But wrong option is given."
} else {
    pass "Corruption file is not found since wrong option is given."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

#Test 5 : Check the warning message when wrong option are given.
# "set heap-check <leaks/string/bounds/scramble/free> on/off" when options and command is
# is misspelt a warning message "Malformed command. Ignoring option <command> " is given.

set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec mkdir ${outdir}"

#set the config spec
system "echo 'fit heap-check string on'  > rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 3 } then {
   pass "Program executed properly without any errors."
  } else {
   fail "Program failed to execute properly."
}

catch "exec fgrep -c \"Malformed command. Ignoring option\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message malformed command is given."
} else {
   fail "Warning message malformed command is not given."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*heap ]

if {  [file exists $file1] } then {
    fail ".mem corruption file is generated. But wrong option is given."
} else {
    pass "Corruption file is not found since wrong option is given."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

# When "heap-check" is misspelt.

set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec mkdir ${outdir}"

#set the config spec
system "echo 'set hp-chck string on'  > rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 3 } then {
   pass "Program executed properly without any errors."
  } else {
   fail "Program failed to execute properly."
}

catch "exec fgrep -c \"Malformed command. Ignoring option\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message malformed command is given."
} else {
   fail "Warning message malformed command is not given."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*heap ]

if {  [file exists $file1] } then {
    fail ".mem corruption file is generated. But wrong option is given."
} else {
    pass "Corruption file is not found since wrong option is given."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

# When option is misspelt.

set outdir /tmp/BatchRTC_$pid

#create a directory for output files
catch "exec mkdir ${outdir}"

#set the config spec
system "echo 'set heap-check stirng on'  > rtcconfig"
system "echo 'files=newcmd_batch' >> rtcconfig"
system "echo 'output_dir=${outdir}' >> rtcconfig"

set env(BATCH_RTC) "on"
catch "exec ./gdb.hp/gdb.memcheck/${testfile} >& ${outdir}/${testfile}.out"
set env(BATCH_RTC) "off"

catch "exec cat ${outdir}/${testfile}.out | wc -l" exec_output

if { $exec_output == 3 } then {
   pass "Program executed properly without any errors."
  } else {
   fail "Program failed to execute properly."
}

catch "exec fgrep -c \"Malformed command. Ignoring option\" ${outdir}/${testfile}.out" warn
if { $warn == 1 } then {
   pass "Warning message malformed command is given."
} else {
   fail "Warning message malformed command is not given."
}

set file1 [ glob -nocomplain ${outdir}/${testfile}*heap ]

if {  [file exists $file1] } then {
    fail ".mem corruption file is generated. But wrong option is given."
} else {
    pass "Corruption file is not found since wrong option is given."
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${objdir}/rtcconfig"

set env(LD_PRELOAD) ""
set env(GDB_SERVER) ""

return 0
