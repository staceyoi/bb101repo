#include <stdio.h>
#include <malloc.h>

void lib1();
void lib2();

int
main ()
{
    char *tp;
    tp = malloc (100);
    tp = malloc (100);
    tp = malloc (100);
    lib1 ();
    lib2 ();
    printf ("Batch RTC test over\n");
    exit (0);
}

void 
lib1 ()
{
    void *t;
    t = malloc (50);
    t = malloc (50);
    t = malloc (50);
    return;
}

void 
lib2 ()
{
    void *t;
    t = malloc (250);
    t = malloc (250);
    t = malloc (250);
    return;
}
