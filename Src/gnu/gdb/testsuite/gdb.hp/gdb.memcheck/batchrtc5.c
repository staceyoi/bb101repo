


/* test large malloc.  Do not attempt any header or footer 
 * corruption.  To get interesting data do not call free.  
 */ 
#include <stdio.h>
#include <stdlib.h>


void f1()
{   
  char * cp;

  cp = malloc (5000);
}
    

void f2()
{
  char * cp;

  cp = malloc (7000);
}


void f3()
{
  char * cp;

  cp = malloc (3000);
}
    
void f4()
{   
  char * cp; 

  cp = malloc (6000);
}



void f1_small()
{   
  char * cp;

  cp = malloc (50);
}
    

void f2_small()
{
  char * cp;

  cp = malloc (70);
}


void f3_small()
{
  char * cp;

  cp = malloc (30);
}
    
void f4_small()
{   
  char * cp; 

  cp = malloc (60);
}


/* created this to set a break point before doing info heap arena commands*/
void set_brkpt_here()
{

}


int main()
{


  for (int i=0; i<777; i++)
  { 
    f4_small();
    f1_small();
    f2_small();
    f3_small();
  } 

  set_brkpt_here();


  for (int i=0; i<1000; i++)
  { 
    f4();
    f1();
    f2();
    f3();
  } 

  set_brkpt_here();

}




