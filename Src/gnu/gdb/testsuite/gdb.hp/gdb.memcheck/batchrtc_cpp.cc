
#include <stdio.h>
#include <stdlib.h>


/* note: make sure the functions are not inlined +Onoinline or +d */

class class_f1 {

public: 

  void *cp;
  void f1c();
  void f1b();
  void f1a();
  void f1();

};

void class_f1::f1c() { cp = malloc (5000); }
void class_f1::f1b() {f1c();}
void class_f1::f1a() {f1b();}
void class_f1::f1() {f1a();}
    

class class_f2 {

public: 

  void *cp;
  void f2(char *, int);

};

void class_f2::f2(char *str, int val) 
{ 
  cp = malloc (val); 
  printf("str: %s\n", str);
}


class class_f3 {

public: 

  void *cp;
  void f3();

};


void class_f3::f3() { cp = malloc (3000); }

class class_f4 {

public: 

  void *cp;
  void f4();

};

void class_f4::f4() { cp = malloc (6000); }


/* created this to set a break point before doing info heap arena commands*/
void set_brkpt_here()
{

}


int main()
{

  class_f1 objf1;
  class_f2 objf2;
  class_f3 objf3;
  class_f4 objf4;


  for (int i=0; i<10; i++)
  { 
    objf1.f1();
    objf2.f2("hi monster", 7000);
    objf3.f3();
    objf4.f4();
  } 

  set_brkpt_here();

}




