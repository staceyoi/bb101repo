#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Test info corruption for corefile

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] {
    return
}


set testfile infcorr
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set objfile  ${objdir}/${subdir}/${testfile}.o
set binfile  ${objdir}/${subdir}/${testfile}


if { [istarget "ia64*-hp-*"] && "${IS_ILP32}" == "FALSE" } {
  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  set librtc "${objdir}/../librtc64.sl"
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  set librtc "${objdir}/../librtc.sl"
}

set foo [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug}]
if { "${foo}" != "" } {
    perror "Couldn't compile ${testfile}.c"
    perror "Output is '${foo}'"
    return -1
}


# Start with a fresh gdb

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "set heap-check on" ""  ""
gdb_test "b 24" "Breakpoint 1.*"  "Breakpoint"

send_gdb "run\n"
gdb_expect {
  -re "Attempt to free unallocated or already freed object.*$gdb_prompt $" {
    pass "detect bad free 1"
  }
  -re ".*$gdb_prompt $" { fail "detect bad free 1" }
  timeout           { fail "(timeout) detect bad free 1" }
}

send_gdb "continue\n"
gdb_expect {
  -re "Attempt to free unallocated or already freed object.*$gdb_prompt $" {
    pass "detect bad free 2"
  }
  -re ".*$gdb_prompt $" { fail "detect bad free 2" }
  timeout           { fail "(timeout) detect bad free 2" }
}

send_gdb "continue\n"
gdb_expect {
  -re "appears to be corrupted at the beginning.*$gdb_prompt $" {
    pass "detect corruption"
  }
  -re ".*$gdb_prompt $" { fail "detect corruption" }
  timeout           { fail "(timeout) detect corruption" }
}

gdb_test "continue" ".*Breakpoint 1.*"  "Stopped"

gdb_test "info corruption" ".*Following blocks appear to be corrupted.*Total bytes.*100.*Beginning.*main.*" "info corruption for live process"

gdb_test "info corruption 0" ".*100 bytes at.*main.*infcorr.c:16.*" "info corruption 0"

set timeout 160
send_gdb "dumpcore\n"
gdb_expect {
  -re "Dumping core to the core file (core.\[0-9\]+).*$gdb_prompt $" {
    set corefile $expect_out(1,string)
    pass "dumpcore"
  }
  -re ".*$gdb_prompt $" { fail "dumpcore - output did not match" }
  timeout           { fail "(timeout) dumpcore" }
}

gdb_exit


remote_exec build "cp  ${binfile} ${objdir}"

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_test "core  ${corefile}"  ".*was generated.*" "loaded corefile"
gdb_test "info corruption" ".*Following blocks appear to be corrupted.*Total bytes.*100.*Beginning.*main.*" "info corruption on core"
gdb_test "info corruption 0" ".*100 bytes at.*main.*infcorr.c:16.*.*" "info corruption 0 from core"

gdb_exit

# clean up 
remote_exec build "rm -rf ${corefile}"
remote_exec build "rm -rf ${objdir}/${testfile}"

return 0










