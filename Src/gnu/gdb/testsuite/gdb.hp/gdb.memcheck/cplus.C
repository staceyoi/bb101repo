#include <iostream.h>
class Y {
private:
  char * string;
  int number;
public:
  // Constructor
  Y(const char*, int);
  // Destructor
  ~Y() { delete[] string; }
  // A member function
 void my_y(void )
  { malloc (50); }

};

// Define class Y constructor
Y::Y(const char* n, int a) {
  string = strcpy(new char[strlen(n) + 1 ], n);
  number = a;
  malloc (100);
}

int main () {
  // Create and initialize
  // object of class Y
  Y yobj = Y("somestring", 10);
  yobj.my_y();

  // ...

  // Destructor ~Y is called before
  // control returns from main()
}
