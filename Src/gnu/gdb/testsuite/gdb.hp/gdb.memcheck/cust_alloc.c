#include "system.h"

void func1()
{
  char *ptr;
  ptr = (char *) xmalloc (10);
}

void func2()
{
  char *ptr;
  ptr = (char *) xmalloc (200);
}

void set_brkhere()
{
}

int main(int argc, char *const *argv)
{
  char *name,*sname;
  int allocated_size;
  char *program_name;

  program_name = argv[0];
  allocated_size = 50;
  
  name = (char *) xmalloc (allocated_size);
  sname = (char *) xmalloc (allocated_size);

  strcpy(name, "Custom Allocator");
  strcpy(sname, "Wildebeast");

  func1();
  func2();

  set_brkhere();
  free(name);
  free(sname);
  set_brkhere();
}
