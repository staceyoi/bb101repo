#include <dlfcn.h>
#include <stdlib.h>
 
 
 main()
 {
 
 	 void * l;
         void (*foo) ();

 	 l = dlopen ("libsl1.sl", RTLD_NOW);
         foo = (void (*) ()) dlsym (l, "foo");
         foo ();
         dlclose (l);
         malloc (1000);

 	 l = dlopen ("libsl2.sl", RTLD_NOW);
         foo = (void (*) ()) dlsym (l, "goo");
         foo ();
         malloc (1000);
         printf ("Done\n");
         dlclose (l);

         printf ("Loading the library again that has foo() \n");
 	 l = dlopen ("libsl1.sl", RTLD_NOW);
         foo = (void (*) ()) dlsym (l, "foo");
         foo ();
         dlclose(l);

         printf ("really Done\n");
 }
