/*
 *  malloc-test
 *  cel - Thu Jan  7 15:49:16 EST 1999
 *
 *  Benchmark libc's malloc, and check how well it
 *  can handle malloc requests from multiple threads.
 *
 *  Syntax:
 *  malloc-test [ size [ iterations [ thread count ]]]
 *
 */

/*
 * http://www.citi.umich.edu/projects/linux-scalability/reports/malloc.html
 * Copyright B) 1999 Netscape Communications Corp., all rights reserved. 
 * Trademarked material referenced in this document is copyright by its 
 * respective owner.
 *
 */

/* Copyright 2006 Hewlett packard development company */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

#include <pthread.h>

#define USECSPERSEC 1000000
#define pthread_attr_default NULL
#define MAX_THREADS 50

void run_test(int *);
void * dummy(unsigned);
hrtime_t *malloc_time;

unsigned thread_count = 1;
static unsigned size = 512;
static unsigned iteration_count = 1000000;

int iarr[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31, \
              32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60, \
              61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90, \
              91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115, \
              116,117,118,119,120,121,122,123,124,125,126,127,128,129,130};


int main(int argc, char *argv[])
{
	unsigned i;
	pthread_t thread[MAX_THREADS];

	/*
	 * Parse our arguments
	 */
	switch (argc) {
	case 4:
		/* size, iteration count, and thread count were specified */
		thread_count = atoi(argv[3]);
		if (thread_count > MAX_THREADS) thread_count = MAX_THREADS;
	case 3:
		/* size and iteration count were specified; others default */
		iteration_count = atoi(argv[2]);
	case 2:
		/* size was specified; others default */
		size = atoi(argv[1]);
	case 1:
		/* use default values */
		break;
	default:
		printf("Unrecognized arguments.\n");
		exit(1);
	}

	malloc_time = calloc(sizeof(hrtime_t), thread_count);
	if(malloc_time == NULL)
		printf("calloc failed for malloc_time\n");

	/*
	 * Invoke the tests
	 */
	printf("Starting test...\n");
	for (i=1; i<=thread_count; i++)
		if (pthread_create(&(thread[i]), pthread_attr_default,
					(void *) &run_test, &iarr[i-1]))
			printf("failed.\n");

	/*
	 * Wait for tests to finish
	 */
	for (i=1; i<=thread_count; i++)
		pthread_join(thread[i], NULL);

	/* Caclulate the avg time taken */
	for (i=1; i<thread_count; i++)
		malloc_time[0] += malloc_time[i];

	printf("Avg time for all threads  = %lld\n", (malloc_time[0]/thread_count) );

	exit(0);
}

void run_test(int *num)
{
	register unsigned int i;
	register unsigned request_size = size;
	register unsigned total_iterations = iteration_count;
	hrtime_t start, end, null, elapsed, adjusted;
	int thread_id = *num;

	/*
	 * Time a null loop.  We'll subtract this from the final
	 * malloc loop results to get a more accurate value.
	 */
	start = gethrtime();

	for (i = 0; i < total_iterations; i++) {
		register void * buf;
		buf = dummy(i);
		buf = dummy(i);
	}

	end = gethrtime();
	null = end - start;

	/*
	 * Run the real malloc test
	 */
	start = gethrtime();

	for (i = 0; i < total_iterations; i++) {
		register void * buf;
		buf = malloc(request_size);
		free(buf);
	}

	end = gethrtime();
	elapsed = end - start;
	adjusted = elapsed - null;

	/*
	 * Adjust elapsed time by null loop time
	 */
	printf("Thread %d adjusted timing: %lld seconds for %d requests"
		" of %d bytes.\n", pthread_self(),
		adjusted, total_iterations,
		request_size);

	malloc_time[thread_id] = adjusted;

	pthread_exit(NULL);
}

void * dummy(unsigned i)
{
	return NULL;
}
