#include <stdio.h>
#include <malloc.h>

char * func1()
{
  char *ptr;
  ptr = malloc(1000);
  return ptr;

}

char * func2()
{
  char *ptr;
  ptr = malloc (100);
  return ptr;

}

void func3()
{
  char *ptr;
  ptr = malloc (500);

  free(ptr);

}

void  func4()
{
  char *ptr;
  ptr = malloc (9000);
  free (ptr);

}

void func4a()
{
   func4();
}

void func4b()
{
  func4a();
}

void func4c()
{
  func4b();
}

void func4d()
{
  func4c();
}

void func4e()
{
  func4d();
}

void func5()
{
  char *ptr;
  ptr = malloc (10);
  free (ptr);

}

void func6()
{
  char *ptr;
  ptr = malloc (100);
  free (ptr);

}

void func7()
{
  char *ptr;
  ptr = malloc (5000);
  free (ptr);

}

void main()
{
    char* ptr;
    int i;

    for (i=0; i<100; i++)
     {
       ptr = func1();
       free(ptr);
     }
    ptr = func2();
    func3();
    func4e();

    func5();
    func6();
    func4();
    func7();
}

