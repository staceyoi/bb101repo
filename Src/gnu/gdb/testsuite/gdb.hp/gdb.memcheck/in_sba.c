#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

int main ()
{
  double *not_sba, **sba;
  mallopt (M_MXFAST, 200);
  mallopt (M_NLBLKS, 250);
  mallopt (M_GRAIN, 16);
  sba = (double **) malloc (sizeof (double *));
  not_sba = (double *) calloc (1000, sizeof (double)); 
  *sba = not_sba;
  not_sba = NULL;
  free (sba);
  not_sba = (double *) calloc (1000, sizeof (double));
  return 0;
}
