#include <stdlib.h>

 void lib1_func();
 void lib2();
 void large_allocation();


int
main ()
{
    char *tp, *tp2;
    tp = malloc (100);
    *(tp+120) ='A';
    *(tp+121) ='B';
    *(tp+122) ='C';
    tp2 = malloc (100);
    *(tp2-3) ='c';
    *(tp2-2) ='b';
    *(tp2-1) ='a';
    tp = malloc (100);
    lib1_func ();
    lib2 ();
    large_allocation();
    printf ("Batch RTC test over\n");
    exit (0);
}

void
lib1_func ()
{
    int *t[10];
    int *temp, i;
    for(i=0; i< 10; i++)
      t[i] = malloc (250 + 250*i);
    for(i=0; i< 10; i+=5)
      free(t[i]);

    temp=t[0];
    /* Access only if the pointer is non NULL */
    if (temp)
    *(temp+49) = 100;

    temp=t[1];
    /* Access only if the pointer is non NULL */
    if (temp)
    *(temp+1) = 200;

    temp=t[2];
    /* Access only if the pointer is non NULL */
    if (temp)
    *(temp+3) = 400;

    temp=t[3];
    /* Access only if the pointer is non NULL */
    if (temp)
    *(temp+4) = 0xff;

    temp=t[4];
    *(temp+10) = 0xab;

    temp=t[5];
    *(temp) = 100;

    temp=t[6];
    *(temp+15) = 100;

    temp=t[7];
    *(temp+20) = 100;

    temp=t[8];
    *(temp+51) = 100;

    temp = malloc (50);
    temp = temp + 1;
    /* Illegal free, should complain */
    free(temp);
    free( temp - 1);
    /* double free, should complain */
    free( temp - 1);

    temp = malloc (50);
    for(i = 0; i > -5; i--)
     *(temp + i ) = 'H';
    free(temp);
    return;
}
void
lib2 ()
{
    void *t;
    t = malloc (250);
    t = malloc (250);
    t = malloc (250);
    return;
}

void large_allocation()
{
    int i, j=10;

    loop:  i=10;
    while(i > 0)
    {
        free(malloc(10000));
        i--;
    }
    printf(" One round of large allocations over \n");
    j--;
    if (j > 0) goto loop;
    printf(" Out of large allocations functions \n");
}
 
