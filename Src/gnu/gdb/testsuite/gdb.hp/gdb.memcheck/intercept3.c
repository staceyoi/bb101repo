#include <sys/mman.h>
#include <sys/shm.h>
#include <stdlib.h>

static   int shmid;
char * cp[10];
void shl_first_file_allocate ()
{

    cp[0] = malloc (10);
    cp[1] = calloc (10, 2);
    cp[2] = realloc (0, 30);
    cp[3] = valloc (40);
    cp[4] = malloc (100);
    cp[4] = realloc (cp[4], 50);
    cp[5] = mmap (0, 60, PROT_READ | PROT_WRITE,
		MAP_ANONYMOUS | MAP_PRIVATE,
                -1, 
                0);
    if ((shmid = shmget (IPC_PRIVATE, 70, 0600)) == -1)
        perror("Boot machine. Tests have left behing shared memory segment");
    if ((cp[6] = shmat (shmid, 0, 0)) == SHM_FAILED)
        perror("shmat failed. Aborting test.");;
}

void shl_first_file_free ()
{
    free (cp[0]);
    free (cp[1]);
    realloc (cp[2], 0);
    free (cp[3]);
    free (cp[4]);
    munmap(cp[5], 60);
    if (shmdt (cp[6]) == -1)
       perror("shmdt failed. Aborting test");
    if (shmctl (shmid, IPC_RMID, 0) == -1)
       perror("shmctl failed. Aborting test");
}

static void * (*imalloc)  (size_t size) = malloc;
static void   (*ifree)    (void * pointer) = free;
static void * (*icalloc)  (size_t nelem, size_t size) = calloc;
static void * (*irealloc) (void *pointer, size_t size) = realloc;
static void * (*ivalloc)  (size_t size) = valloc;

static void * (*immap)  (void *, size_t, int, int, int, off_t) = mmap;
static int (*imunmap) (void *, size_t) = munmap;

static void * (*ishmat) (int, const void *, int) = shmat;
static int (*ishmdt) (const void *) = shmdt;

void shl_first_file_iallocate ()
{

    cp[0] = imalloc (10);
    cp[1] = icalloc (10, 2);
    cp[2] = irealloc (0, 30);
    cp[3] = ivalloc (40);
    cp[4] = imalloc (100);
    cp[4] = irealloc (cp[4], 50);
    cp[5] = immap (0, 60, PROT_READ | PROT_WRITE,
		MAP_ANONYMOUS | MAP_PRIVATE,
                -1, 
                0);
    if ((shmid = shmget (IPC_PRIVATE, 70, 0600)) == -1)
        perror("Boot machine. Tests have left behing shared memory segment");
    if ((cp[6] = ishmat (shmid, 0, 0)) == SHM_FAILED)
        perror("ishmat failed. Aborting test.");;
}

void shl_first_file_ifree ()
{
    ifree (cp[0]);
    ifree (cp[1]);
    irealloc (cp[2], 0);
    ifree (cp[3]);
    ifree (cp[4]);
    imunmap(cp[5], 60);
    if (ishmdt (cp[6]) == -1)
       perror("ishmdt failed. Aborting test");
    if (shmctl (shmid, IPC_RMID, 0) == -1)
       perror("shmctl failed. Aborting test");
}
