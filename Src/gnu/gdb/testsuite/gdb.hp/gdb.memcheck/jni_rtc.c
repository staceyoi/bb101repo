#include "jni_0005frtc.h"
#include <unistd.h>
#include <dl.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
void bar ();
void foo ();

JNIEXPORT void JNICALL Java_jni_1rtc_fn (JNIEnv * x, jobject y)
{
  bar ();
}

void bar ()
{
  foo ();
}

void foo ()
{
   int i;
   long j=100;
   char *p[20], buffer[]="0123456789012345", *temp;
   p[1]="1234";
   temp = (char *)malloc (3);
   for (i=0;i< 5;i++) {
        p[i] = (char *)malloc ( 100 * sizeof(int) );
   }
   p[1]= (char *)malloc (4);
   p[2] = (char *)malloc (3);
   memcpy(p[1], buffer, 6);
   memmove(p[1], buffer, 6);
   bzero(p[1], 8);
   memccpy(p[1], buffer, '7',  12);
   //_memcpy(p[1], buffer, 6);
   //_memset(p[1], 2, 6);

   free(p[1]);
   printf("Freeing p[1]\n");
   free(p[1]);

   printf ("End of native code (JNI)\n");
   free(temp);
   sleep (10);
}

