#include "jni_0005frtc.h"
#include <unistd.h>
#include <dl.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
void bar ();
void foo ();

JNIEXPORT void JNICALL Java_jni_1rtc_fn (JNIEnv *, jobject)
{
  bar ();
}

void bar ()
{
  foo ();
}

void foo ()
{
   int i;
   long j=100;
   char *p[20], buffer[]="0123456789012345", *temp;
   p[1]="1234";
   temp = new char (3);
   for (i=0;i< 5;i++) {
	p[i] = new char ( 100 * sizeof(int) );
   }
	strcpy (temp, buffer);
        p[1]= new char[4];
	p[2] = new char (3);
        strcpy(p[2], "qwerty");
	bzero(p[1], 8);
        memcpy(p[1], buffer, 6);
        bzero(p[1], 8);
        delete(p[1]);
        delete(p[1]);
	delete(p[2]);
	delete(temp);
}
