public class jni_rtc {

  static {
   System.loadLibrary("jni_rtc");
  }

  public native void fn();
  public static void main(String[] args) {
     jni_rtc myh =  new jni_rtc();
     myh.fn();
  }
}
