#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Test new heap arena feature

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] {
    return
}



set testfilea malloc_2a
set testfileb malloc_2b
set testfilec malloc_2c
set testfiled malloc_2d

set srcfilea ${srcdir}/${subdir}/${testfilea}.c
set srcfileb ${srcdir}/${subdir}/${testfileb}.c
set srcfilec ${srcdir}/${subdir}/${testfilec}.c
set srcfiled ${srcdir}/${subdir}/${testfiled}.c

set objfileb  ${objdir}/${subdir}/${testfileb}.o
set objfilec  ${objdir}/${subdir}/${testfilec}.o
set objfiled  ${objdir}/${subdir}/${testfiled}.o

# share obj files
set sofileb  ${objdir}/${subdir}/${testfileb}.so
set sofilec  ${objdir}/${subdir}/${testfilec}.so
set sofiled  ${objdir}/${subdir}/${testfiled}.so

set binfile  ${objdir}/${subdir}/${testfilea}


remote_exec build "$CC_FOR_TARGET -g +z -c ${srcfileb} -o ${objfileb}"
remote_exec build "$CC_FOR_TARGET -g +z -c ${srcfilec} -o ${objfilec}"
remote_exec build "$CC_FOR_TARGET -g +z -c ${srcfiled} -o ${objfiled}"

remote_exec build "ld -b  ${objfileb} -o ${sofileb}"
remote_exec build "ld -b  ${objfilec} -o ${sofilec}"
remote_exec build "ld -b  ${objfiled} -o ${sofiled}"

remote_exec build "$CC_FOR_TARGET -g  ${srcfilea} ${sofileb} ${sofilec} ${sofiled} -o ${binfile}"




if { [istarget "ia64*-hp-*"] && "${IS_ILP32}" == "FALSE" } {
  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  set librtc "${objdir}/../librtc64.sl"
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  set librtc "${objdir}/../librtc.sl"
}

# to test on 11.23 remove the below to point to specific libraries and
# remove the shell uname check for 11.23 
# to pick up new libc.  Remove this when get on 11.31
#if { [istarget "ia64*-hp-*"] } {
#  set env(SHLIB_PATH) /home/trangto/wdb/RTC/libc-dec-16/em_32:/home/trangto/wdb/RTC/libc-dec-16/em_64
#set env(SHLIB_PATH) /home/trangto/wdb/2006/new_libc/em_32:/home/trangto/wdb/2006/new_libc/em_64
#} else {
#  remote_exec build "chatr +s enable ${binfile}"
#  remote_exec build "chatr +s enable ${objdir}/../gdb"
#  set env(SHLIB_PATH) /home/trangto/wdb/RTC/libc-dec-16/pa_32:/home/trangto/wdb/RTC/libc-dec-16/pa_64
#}



# Start with a fresh gdb

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Switch off this test for non 11.31.
# PA is running on 11.11 so don't check for 11.23 check for 11.31
set 1131_kern 0

send_gdb "shell uname -r\n"
gdb_expect {
  -re ".*11.31.*$gdb_prompt $" { set 1131_kern 1; pass "uname 1131" }
  -re ".*$gdb_prompt $" { pass "not uname 1131" }
}

if { $1131_kern==0 } {
  gdb_exit;
  return;
}


gdb_test "set heap-check on" ""  ""
gdb_test "b set_brkpt_here" "Breakpoint 1.*"  "Breakpoint"
gdb_test "run" ".*Breakpoint 1.*"  "Stopped"


gdb_test "info heap process" ".*space in arenas.*ordinary blocks.*" "info heap process"

gdb_test "info heap arenas" \
         ".*num_arenas.*expansion.*Total space.*Number of holding.*" \
         "info heap arenas" 

# expect 10-15 seconds here 
gdb_test "info heap arena 0 block 19" \
         "Virtual.*Start address.*Ending address.*" \
         "info heap arena 0 block 19"



set timeout 160
send_gdb "dumpcore\n"
gdb_expect {
  -re "Dumping core to the core file (core.\[0-9\]+).*$gdb_prompt $" {
    set corefile $expect_out(1,string)
    pass "dumpcore"
  }
  -re ".*$gdb_prompt $" { fail "dumpcore - output did not match" }
  timeout           { fail "(timeout) dumpcore" }
}

gdb_exit


# move it to same dir as the core for a moment to load then clean up 
remote_exec build "cp  ${binfile} ${objdir}"

gdb_start
gdb_reinitialize_dir $srcdir/$subdir


gdb_test "core ${corefile}"  ".*was generated.*" "loaded corefile"

gdb_test "info heap process" ".*space in arenas.*ordinary blocks.*" "info heap process"

gdb_test "info heap arenas" \
         ".*num_arenas.*expansion.*Total space.*Number of holding.*" \
         "info heap arenas" 

# expect 10-15 seconds here 
gdb_test "info heap arena 0 block 19" \
         "Virtual.*Start address.*Ending address.*" \
         "info heap arena 0 block 19"

gdb_exit

# clean up 
remote_exec build "rm -rf ${corefile}"
remote_exec build "rm -rf ${objdir}/${testfilea}"

return 0










