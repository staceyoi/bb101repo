#include <malloc.h>

void corrupt_footer1()
{   
    
  char * cp;

  cp = malloc (100);
  cp[100] = 100;
  //free (cp);  /* should trigger event */
}
    

void corrupt_footer2()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
  //free (cp);  /* should trigger event */
}


void corrupt_footer3()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
  //free (cp);  /* should trigger event */
}
    
void corrupt_header()
{   
    
  char * cp; 
  cp = malloc (100);
  cp[-1] = 77;
  //free (cp);  /* should trigger event */
}

void set_brkpt_here()
{

}

int main()
{ 
  int i;
 
/* Suresh, Dec 07: reducing the heap blocks to fix test failure 
   But info corruption on corefiles is really broken. I have filed a
   CR 1000751009 to fix this
*/
  for (i=0; i<101; i++)
  {
    corrupt_header();
    corrupt_footer1();
    corrupt_footer2();
    corrupt_footer3();
  }

  set_brkpt_here();

}



