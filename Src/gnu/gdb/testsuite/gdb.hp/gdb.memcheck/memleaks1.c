#include <stdlib.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <dl.h>
#include <dlfcn.h>

char * gpd = (char *)1;  /* this is data */
char * gpbss;            /* this is bss */
char * shlib_create_handle_in_data (char *);
char * shlib_create_handle_in_bss (char *);
int ** mp;
char ** cpp;
int ** ipp;
void *sbrk(int incr);

int shmid;	/* id of shared memory segment */

void info_leaks ()
{
    /* Srikanth, forcibly clear the stack, as command line
       call bumps up the stack pointer further,some leaks
       are masked due to stale stack content.
    */
    char array[1024 * 64] = "";
}

char * create_handle_in_sbrk_region (char *cp)
{
   long l;
   l = (long) sbrk (100);

   while (l % sizeof(l))
     l++;

   l += sizeof (long);

   cpp = (void *) l;
   *cpp = cp;
   cp = 0;
   return 0;
}

void destroy_handle_in_sbrk_region (char *cp)
{
  *cpp = 0;
  cp = 0;
}

char * create_handle_in_mmap_region (char *cp)
{
    mp = mmap(0,   /* map anywhere */
             8,
             PROT_READ | PROT_WRITE,
             MAP_ANONYMOUS | MAP_PRIVATE,
             -1,
             0);

    mp[0] = (int *) cp;
    cp = 0;
    return cp;
}

void destroy_handle_in_mmap_region (char *cp)
{
    munmap (mp, 8);
    cp = 0;
}

char * create_handle_in_shmat_region (char *cp)
{
  shmid = shmget (IPC_PRIVATE, 8, 0600);
  mp = shmat (shmid, 0, 0);

  mp[0] = (int *) cp;
  cp = 0;
  return 0;
}

void destroy_handle_in_shmat_region (char *cp)
{
    if (shmdt (mp) == -1)
       perror("shmdt failed. Aborting test");
    if (shmctl (shmid, IPC_RMID, 0) == -1)
       perror("shmctl failed. Aborting test");
    cp = 0;
}

char * aout_create_handle_in_data (char *cp)
{
    gpd = cp;
    cp = 0;
    return 0;
}

void aout_destroy_handle_in_data (char * cp)
{
    gpd = cp;
}

char * aout_create_handle_in_bss (char * cp)
{
    gpbss = cp;
    cp = 0;
    return 0;
}

void aout_destroy_handle_in_bss (char * cp)
{
    gpbss = cp;
}

char* create_handle_in_heap (char* cp)
{
  ipp = (int **)malloc (sizeof(int *));
  *ipp = (int*) cp;
  cp = 0;
  return cp;
}

void destroy_handle_in_heap (char* cp)
{
  *ipp = 0;
  free (ipp);
  ipp = 0;
  cp = 0;
}

#if 0
#ifndef __LP64__
#pragma HP_LONG_RETURN __rtc_d_trap
#pragma HP_NO_RELOCATION __rtc_d_trap

static void
__rtc_d_trap (first, second, third, fourth, fifth)
int first, second, third, fourth, fifth;
{
 sigset_t oset;

 /* We are here due to a bind-on-reference notification from dld.
    We need to jump to the real debugger hook, if it is safe to
    do so. It is not safe if SIGTRAPs are disabled for the current
    thread, because the breakpoint will not trigger if SIGTRAPs are
    masked. The program cannot proceed meaningfully having it a
    breakpoint and bad things happen afterward. Fortunately signal
    masks are thread private on HP-UX -- srikanth, 000313.
 */

 sigprocmask(
            SIG_SETMASK,   /* Will be ignored since ...   */
            0,             /* ... this is a NULL pointer  */
            &oset);
 if (sigismember (&oset, SIGTRAP))
   return;

 /* Safe to jump to the real debugger hook ..., The original debugger
    hook gets passed variable number of arguments. But currently we
    treat only BOR events specially, everything else gets treated as
    a SHL_LOAD
 */

 __d_trap (first, second, third, fourth, fifth);
}
#endif
#endif

main()
{
    char * cp;

    shl_t dll = 0;
    shl_t *dll1;
    char * (*dll_create_handle_in_data)() = 0;
    char * (*dll_create_handle_in_bss)() = 0;
    void (*dll_destroy_handle_in_data)() = 0;
    void (*dll_destroy_handle_in_bss)() = 0;

    char * (*dll_create_handle_in_data1)() = 0;
    char * (*dll_create_handle_in_bss1)() = 0;
    void (*dll_destroy_handle_in_data1)() = 0;
    void (*dll_destroy_handle_in_bss1)() = 0;

    /* Do we scan the stack ? */
    cp = malloc (1);
    printf ("Created handle in stack!\n");
    info_leaks ();
    cp = 0;
    printf ("Destroyed handle in stack\n");cp =0;
    info_leaks ();

    /* Do we scan global data ? */
    cp = malloc(1);
    cp = aout_create_handle_in_data (cp);
    info_leaks ();
    aout_destroy_handle_in_data (0);
    printf ("destroyed global data handle\n");
    info_leaks ();

    /* Do we scan global bss ? */
    cp = malloc(1);
    cp = aout_create_handle_in_bss (cp);
    info_leaks ();
    aout_destroy_handle_in_bss (0);
    printf ("destroyed global bss handle\n");
    info_leaks ();

    /* Do we scan shlib data ? */
    cp = malloc (1);
    cp = shlib_create_handle_in_data (cp);
    printf ("created shlib data handle\n");
    info_leaks ();
    shlib_destroy_handle_in_data (0);
    printf ("destroyed shlib data handle\n");
    info_leaks ();

    /* Do we scan shlib bss ? */
    cp = malloc (1);
    cp = shlib_create_handle_in_bss (cp);
    printf ("created shlib bss handle\n");
    info_leaks ();
    shlib_destroy_handle_in_bss (0);
    printf ("destroyed shlib bss handle\n");
    info_leaks ();

    /* Load a dll */
    dll = shl_load ("gdb.hp/gdb.memcheck/libmemleaks2.sl", BIND_DEFERRED, 0);
    dll1 = dlopen ("gdb.hp/gdb.memcheck/libmemleaks3.sl", RTLD_NOW);

    shl_findsym(&dll,
           "dll_create_handle_in_data",
           TYPE_PROCEDURE,
           &dll_create_handle_in_data
      );
    shl_findsym(&dll,
           "dll_destroy_handle_in_data",
           TYPE_PROCEDURE,
           &dll_destroy_handle_in_data
      );

    shl_findsym(&dll,
           "dll_create_handle_in_bss",
           TYPE_PROCEDURE,
           &dll_create_handle_in_bss
      );
    shl_findsym(&dll,
           "dll_destroy_handle_in_bss",
           TYPE_PROCEDURE,
           &dll_destroy_handle_in_bss
      );

     /* Do we scan dll data ? */
    cp = malloc (1);
    cp = dll_create_handle_in_data (cp);
    printf ("created dll data handle\n");
    info_leaks ();
    dll_destroy_handle_in_data (0);
    printf ("destroyed dll data handle\n");
    info_leaks ();

     /* Do we scan dll bss ? */
    cp = malloc (1);
    cp = dll_create_handle_in_bss (cp);
    info_leaks ();
    dll_destroy_handle_in_bss (0);
    printf ("destroyed dll bss handle\n");
    info_leaks ();
    shl_unload(dll);

    /* Is the heap scanned ? */
    cp = malloc (1);
    cp = create_handle_in_heap (cp);
    info_leaks ();
    destroy_handle_in_heap (0);
    printf ("destroyed handle in heap\n");
    info_leaks ();

    /* Do we scan mmap regions ? */
    cp = malloc (1);
    cp = create_handle_in_mmap_region (cp);
    info_leaks ();
    destroy_handle_in_mmap_region (cp);
    printf ("destroyed handle in mmap region\n");
    info_leaks ();

    /* Do we scan shmat regions ? */
    cp = malloc (1);
    cp = create_handle_in_shmat_region (cp);
    info_leaks ();
    destroy_handle_in_shmat_region (cp);
    printf ("destroyed handle in shmat region\n");
    info_leaks ();

    /* Do we scan user sbrk regions ? */
    cp = malloc (1);
    cp = create_handle_in_sbrk_region (cp);
    info_leaks ();
    destroy_handle_in_sbrk_region (cp);
    printf ("destroyed handle in sbrk region\n");
    printf ("Last leak check !\n");
    info_leaks ();

    dll_create_handle_in_data1 = (char *)dlsym(dll1,
           "dll_create_handle_in_data1");

    dll_create_handle_in_bss1 = (char *)dlsym(dll1,
           "dll_create_handle_in_bss1");

    dll_destroy_handle_in_data1 = dlsym(dll1,
           "dll_destroy_handle_in_data1");

    dll_destroy_handle_in_bss1 = dlsym(dll1,
           "dll_destroy_handle_in_bss1");

     /* Do we scan dll data ? */
    cp = malloc (1);
    cp = dll_create_handle_in_data1 (cp);
    info_leaks ();
    dll_destroy_handle_in_data1 (0);
    printf ("destroyed dll1 data handle\n");
    info_leaks ();

     /* Do we scan dll bss ? */
    cp = malloc (1);
    cp = dll_create_handle_in_bss1 (cp);
    info_leaks ();
    dll_destroy_handle_in_bss1 (0);
    printf ("destroyed dll1 bss handle\n");
    dlclose (dll1);
    cp = malloc (1);
    info_leaks ();
}
