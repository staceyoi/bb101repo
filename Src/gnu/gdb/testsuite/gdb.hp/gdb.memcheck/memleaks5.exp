# memleaks5.exp -- Expect script to test a memory leak of threaded program
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile memleaks5
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set oldtimeout $timeout
set timeout 150

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}


#
set additional_flags "additional_flags=-Ae"

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags ldflags=-lpthread]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}


gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir

gdb_load ${binfile}
gdb_test "set heap-check on" "" ""

gdb_test "b main" "" ""
send_gdb "run\n"
gdb_expect {
  -re ".*main.*$gdb_prompt $" { pass "run to main" }
  -re ".*SIGKILL.*$gdb_prompt $" {
    perror "bad linker. suppressing tests."
    return -1
  }
  timeout { fail "run to main (timeout)" }
}

gdb_test "b 174" "Breakpoint 2.*memleaks5.*" "set bp at 174"
gdb_test "continue" ".*Breakpoint 2.*main.*memleaks5.*" "continue"

# Added 07/07/2004 to see if the intermittent failures on the first run
# were due to not being able to make a command-line call on top of the
# service thread.  If so, this command will show us stopped on top of
# the service thread before the timeout failure.
gdb_test "info threads" "system thread.*main.*memleaks5.*" "info threads"

send_gdb "info leaks\n"
gdb_expect {
  -re "Scanning for memory leaks.*bytes leaked in.*Total bytes.*spin.*$gdb_prompt $" {
    pass "info leaks "
  }
  -re ".*$gdb_prompt $" {
    fail "info leaks - no leak report?"
  }
  timeout {
    fail "info leaks (timeout)"
  }
}

set timeout $oldtimeout
gdb_exit
return 0
