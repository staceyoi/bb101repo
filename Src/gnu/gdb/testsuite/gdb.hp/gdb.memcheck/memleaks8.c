#include <stdlib.h>
 struct node {
        struct node * next;
        struct node * prev;
         long data;
 };
 
 main()
 {
     struct node * head = 0, * temp = 0;
     int i;
 
     for (i = 0; i < 1000000; i++)
     {
        temp = calloc (sizeof (struct node), 1);
        if (temp == 0)
            printf ("Out of memory\n");
        temp->next = head;
        temp->prev = head;
        head = temp;
    }
    printf ("Done\n");
     
 }
