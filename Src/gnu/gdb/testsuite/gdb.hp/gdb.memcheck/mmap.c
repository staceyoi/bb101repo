# include <sys/mman.h>
# include <stdlib.h>

void heap_check ()
{


}

/* Program to check if we cope with partial munmaps ... */
int main()
{
    char * pointer, * pointer2;
    int page_size;
    int res;

    page_size = getpagesize ();

    /* Can we cope with munmap(mmap) */
    pointer = mmap(0, page_size, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer == MAP_FAILED) abort();
    heap_check ();
    res = munmap (pointer, page_size);
    if (res != 0) abort();
    heap_check ();

    /* Can we cope with munmap (head,tail) */
    pointer = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer == MAP_FAILED) abort();
    heap_check ();
    res = munmap (pointer, page_size * 2);
    if (res != 0) abort();
    /* printf ("Leaked mmap block !\n"); */
    heap_check ();
    res = munmap (pointer + (page_size * 2), page_size);
    if (res != 0) abort();
    heap_check ();

    /* Can we cope with munmap (tail, head) */
    pointer = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer == MAP_FAILED) abort();
    heap_check ();
    res = munmap (pointer + (page_size * 2), page_size);
    if (res != 0) abort();
    heap_check ();
    res = munmap (pointer, page_size * 2);
    if (res != 0) abort();
    heap_check ();


    /* Can we cope with munmap (trunk, head, tail) */
    pointer = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer == MAP_FAILED) abort();
    res = munmap (pointer +page_size , page_size);
    if (res != 0) abort();
    heap_check ();
    res = munmap (pointer, page_size);
    if (res != 0) abort();
    heap_check ();
    res = munmap (pointer + page_size + page_size, page_size);
    if (res != 0) abort();
    heap_check ();

#if 0
     /* Following tests are commented out as they depend on system behavior
        that the consecutive mmaps are adjacent to each other. It is not always
        true and the following test fails for release build.
      */

    /* Can we cope with overreaching munmaps ? */
    pointer = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer == MAP_FAILED) abort();
    pointer2 = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer2 == MAP_FAILED) abort();

    heap_check ();
    res = munmap (pointer2 + page_size, page_size * 4);
    if (res != 0) abort();
    heap_check ();
    res = munmap (pointer2, page_size);
    if (res != 0) abort();
    heap_check ();
    res = munmap (pointer2 + page_size * 5, page_size);
    if (res != 0) abort();
    heap_check ();


    pointer = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer == MAP_FAILED) abort();
    pointer2 = mmap(0, page_size * 3, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (pointer2 == MAP_FAILED) abort();

    res = munmap (pointer2, page_size * 5);
    if (res != 0) abort();
    heap_check ();
#endif
    return 0;
}

#if 0
#ifndef __LP64__
#include <signal.h>
#pragma HP_LONG_RETURN __rtc_d_trap
#pragma HP_NO_RELOCATION __rtc_d_trap

static void
__rtc_d_trap (first, second, third, fourth, fifth)
int first, second, third, fourth, fifth;
{
 sigset_t oset;

 /* We are here due to a bind-on-reference notification from dld.
    We need to jump to the real debugger hook, if it is safe to
    do so. It is not safe if SIGTRAPs are disabled for the current
    thread, because the breakpoint will not trigger if SIGTRAPs are
    masked. The program cannot proceed meaningfully having it a
    breakpoint and bad things happen afterward. Fortunately signal
    masks are thread private on HP-UX -- srikanth, 000313.
 */

 sigprocmask(
            SIG_SETMASK,   /* Will be ignored since ...   */
            0,             /* ... this is a NULL pointer  */
            &oset);
 if (sigismember (&oset, SIGTRAP))
   return;

 /* Safe to jump to the real debugger hook ..., The original debugger
    hook gets passed variable number of arguments. But currently we
    treat only BOR events specially, everything else gets treated as
    a SHL_LOAD
 */

 __d_trap (first, second, third, fourth, fifth);
}
#endif
#endif
