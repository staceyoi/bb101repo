#include <stdio.h>
#include <malloc.h>

char * func1()
{
  char *ptr, *ptr1;
  ptr = malloc(99);          
  ptr1 = malloc(1);          
  return ptr;
}

char * func2()
{
  char *ptr;
  ptr = malloc (100);   
  return ptr;
}

void func3()
{
  char *ptr;
  ptr = (char*) malloc (80);  
  ptr[81] = 100;
}

void  func4()
{
  char *ptr;
  ptr = (char*)malloc (101);  
  func3();
}

void func5()
{
  char *ptr;
  ptr = malloc (201);  
  func4();
}


void func6()
{
  char *ptr;
  ptr = malloc (202);  
  func5();
}

void func7()
{
  char *ptr;
  ptr = malloc (5000);  
  free(ptr);
  free(ptr);
  func6();
}

void main()
{
    char* ptr, *str, buffer[]="0123456789", **tp;

    ptr = func1();
    ptr = func2();
    str = (char *)malloc(4);
    memcpy(str, buffer, 6);
    tp = malloc (100);
    printf ("Scrambling feature is on %p\n",*tp);
    func7();
    free(str);
    free(tp);
    printf ("Scrambling feature is on while freeing %p\n", *tp);
}
