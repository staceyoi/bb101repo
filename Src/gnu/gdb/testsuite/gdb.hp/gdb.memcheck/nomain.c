#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#define TRUE 1
#define FALSE 0
#define N_THREADS 5
#define MAX_LOCAL_VAL 40
#define OUTER_LOOP_COUNT 1

/* Check the results of thread-local storage. */
int thread_local_val[ N_THREADS ];

char *t;
int dummy()
{
    printf("Dummy\n");
    return 1;
}

/* Routine for each thread to run, does nothing. */
void *spin( vp )
    void * vp;
{
   void * ll;
   int i, cnt = 0;

   for (i=0; i < 1000000; i++)
       cnt += i;
   ll = malloc(100);
   ll = malloc(100);

   /* make a call to printf here to make sure all the
     temporary registers are flushed and leak will show up */
   printf ("leak here\n");

   return ((void *)NULL);
}

void
do_pass( pass )
    int pass;
{
    int i, err;
    pthread_t t[ N_THREADS ];

    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_create( &t[i], NULL, spin, (void *)i );
        if( err != 0 ) {
            printf( "pthread_create error (err=%d) for thread %d\n", err, i );
        }
    }

    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_join(t[i], NULL );
        if( err != 0 ) {
            printf( "pthread_join error for thread %d\n", i );
        }
    }
}

void
do_it()
{
    int i;
    
    for( i = 0; i < OUTER_LOOP_COUNT; i++ )
        do_pass( i );
}

test_nomain_thread()
{
   char* ll;

   ll = malloc (21);
   ll = malloc (20);
   ll = malloc (10);
   do_it();
   return(0);
}

main()
{
	dummy();

        t = malloc(10);
        t = malloc(20);
        t = malloc(30);

        /* should have 3 mallocs and 2 leaks */
	dummy();

	function_from_primary();

        t = malloc(40);
        t = malloc(50);

	function_from_secondary();

        t = malloc(60);

        /* should have 6 mallocs and 5 leaks */
	dummy();

        free(t);
        t = malloc(70);
        free(t);
        t = malloc(80);

        /* should have 6 mallocs and 5 leaks */
	dummy();

        test_nomain_thread();
	dummy();
}
