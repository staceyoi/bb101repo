#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>


int 
main () {
	char *p;

	printf("Starting...\n");

	p = malloc(100);
	p = malloc(100000000000);
	if (p)
	  fprintf (stderr, "malloc did not fail!!!\n");

	p = calloc(1, 100);
	p = calloc(1, 100000000000);
	if (p)
	  fprintf (stderr, "calloc did not fail!!!\n");

	p = valloc(100);
	p = valloc(100000000000);
	if (p)
	  fprintf (stderr, "valloc did not fail!!!\n");

	p = malloc(100);
	p = realloc(p, 100000000000);
	if (p)
	  fprintf (stderr, "realloc did not fail!!!\n");

	if (p)
	  free (p);
}
