#include <stdio.h>
#include <malloc.h>

int  cnt;

void break_at_dummy()
{
    return;
}

void
test_null_check()
{
    int i;
    char *a;

    cnt = 0;
    for(i = 0; i <= 20; i++) {
    	a = malloc(100); /* set null-check to 10; there should 1 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 1 */

    cnt = 0;
    for (i=0; i <= 1; i++) {
    	a = malloc(200); /* null-check is still 10; there should 1 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 1 */

    cnt = 0;
    for (i=0; i <= 21; i++) {
    	a = malloc(300); /* null-check is still 10; there should 2 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 2 */

    cnt = 0;
    for (i=0; i <= 8; i++) {
    	a = malloc(400); /* null-check is still 10; there should 0 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 0 */

    a = malloc(400); 
}

void
test_null_check_size()
{
    int i;
    char *a;

    /* set null check size to 200 */

    cnt = 0;
    for (i=0; i <= 5; i++) {
    	a = malloc(100); /* there should 2 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 2 */

    cnt = 0;
    for (i=0; i < 1; i++) {
    	a = malloc(100); /* there should 0 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 0 */

    cnt = 0;
    for (i=0; i <= 3; i++) {
    	a = malloc(100); /* there should 1 null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 1 */
}

void
test_null_random()
{
    int i;
    char *a;

    /* set random range 10; seed value 5  */

    cnt = 0;
    for (i=0; i <= 500; i++) {
    	a = malloc(100); /* there should be some  null malloc*/
	if (a == NULL) cnt++;
    }
    break_at_dummy(); /* cnt should be 1 */
}

void
test_catch_nomem()
{
    int i;
    char *a;

    cnt = 0;
    for (i=0; i <= 20; i++) {
    	a = malloc(100); /* set null-check to 10; there should 1 null malloc*/
    }

    cnt = 0;
    for (i=0; i <= 1; i++) {
    	a = malloc(200); /* null-check is still 10; there should 1 null malloc*/
    }

    cnt = 0;
    for (i=0; i <= 21; i++) {
    	a = malloc(300); /* null-check is still 10; there should 2 null malloc*/
    }

    cnt = 0;
    for (i=0; i <= 8; i++) {
    	a = malloc(400); /* null-check is still 10; there should 0 null malloc*/
    }
}

int
main()
{
	test_null_check();
	test_null_check_size();
	test_null_random();
	test_catch_nomem();
	exit (0);
}
