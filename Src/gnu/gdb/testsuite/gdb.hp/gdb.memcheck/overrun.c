#include <stdio.h>
#include <malloc.h>

char *t;
char *t1;
char *t2;
char *t3;

char * sm_malloc(sz)
int sz;
{
    return malloc(sz); /* line number 12 */
}

main()
{
    t = (char *)sm_malloc(10);
    strcpy(t, "123456789123");
    t1 = (char *)sm_malloc(10);
    strcpy(t1, "12345678912");
    t2 = (char *)sm_malloc(10);
    strcpy(t2, "1234567891");
    t3 = (char *)sm_malloc(10);
    strcpy(t3, "123456789");
    free (t);
    printf("Hello t\n");
    free (t1);
    printf("Hello t1\n");
    free (t2);
    printf("Hello t2\n");
    free (t3);
    printf("Hello t3\n");
    free (t);
    printf("Hello t \n");
    free (t1);
    printf("Hello t1 \n");
    exit(1);
}
