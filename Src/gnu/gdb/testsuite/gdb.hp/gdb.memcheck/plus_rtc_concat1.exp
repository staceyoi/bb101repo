# Written by Jini Susan George (jini@hp.com)
# This script is used to test a batch heap analysis run of the shell script
# "batch_test_shell_script". The shell script invokes a bunch of commonly
# used commands like 'awk', 'sed', 'cal', 'date', 'echo', etc.
# This tests only 32 bit scenarios -- PA and IPF since the commonly used
# commands are 32 bit.

# This test is currently being skipped for PA. Reenable when JAGag23331 is fixed.

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

# Enable for PA when JAGag23331 is fixed.
if ![istarget "ia64*-hp-*"] {
  return 0
}

set prms_id 0
set bug_id 0

# are we on a target board
if ![isnative] {
    return
}

set pid [pid]
set rtcdir /tmp/RTC_DIR_$pid
#clean up first
catch "exec rm -rf  ${rtcdir}"
catch "exec mkdir ${rtcdir}"

system "echo 'set heap-check on' > rtcconfig"
system "echo 'set heap-check leaks on;  set heap-check free on' >> rtcconfig"
system "echo 'set heap-check scramble on;      set heap-check string on;' >> rtcconfig"
system "echo 'files=ls:sed:cal:touch:date:echo:cat:awk' >> rtcconfig"
system "echo 'output_dir=${rtcdir}' >> rtcconfig"

# Currently commenting off the PA testing portion since due to an issue
# related to _start, LD_PRELOAD-ing librtc on the shell (ksh) causes
# the shell to abort. Please look at JAGaf37695 for more details.

# The PA testing needs to reenabled once the DLD related fix JAGag23331
# is fixed. The testing can be done by chatr +rtc enabling the
# required commands (like cat, sed, etc) and then invoking the
# batch_test_shell_script_pa script. The LD_PRELOAD should apply only
# to the required commands.

# For PA testing copy the required command files onto the cwd so that
# we can chatr +dbg/+rtc enable them.
if { [istarget "hppa1.1-hp-hpux*"] || [istarget "hppa2.0w*-*-hpux*"] } then {
  catch "exec cp /usr/bin/ls ."
  catch "exec cp /usr/bin/sed ."
  catch "exec cp /usr/bin/cal ."
  catch "exec cp /usr/bin/touch ."
  catch "exec cp /usr/bin/date ."
  catch "exec cp /usr/bin/echo ."
  catch "exec cp /usr/bin/cat ."
  catch "exec cp /usr/bin/awk ."

  catch "exec chmod +w ./ls"
  catch "exec chmod +w ./sed"
  catch "exec chmod +w ./cal"
  catch "exec chmod +w ./touch"
  catch "exec chmod +w ./date"
  catch "exec chmod +w ./echo"
  catch "exec chmod +w ./cat"
  catch "exec chmod +w ./awk"

  catch "exec chatr +rtc enable ./ls"
  catch "exec chatr +rtc enable ./sed"
  catch "exec chatr +rtc enable ./cal"
  catch "exec chatr +rtc enable ./touch"
  catch "exec chatr +rtc enable ./date"
  catch "exec chatr +rtc enable ./echo"
  catch "exec chatr +rtc enable ./cat"
  catch "exec chatr +rtc enable ./awk"

  set testfile batch_test_shell_script_pa
}

if [istarget "ia64*-hp-*"] then {
  set testfile batch_test_shell_script_ia
}

catch "exec cp ${srcdir}/${subdir}/fix_dependencies ."
catch "exec cp ${srcdir}/${subdir}/gdb_files ."
catch "exec cp ${srcdir}/${subdir}/${testfile} ."
catch "exec chmod +x ./${testfile} ./fix_dependencies"

set env(GDB_SERVER) "${objdir}/../gdb"
set env(LD_PRELOAD) "${objdir}/../librtc.sl"

set env(BATCH_RTC) "on"
catch "exec ./${testfile} >& ${rtcdir}/${testfile}.out" check_xfail
if { $check_xfail == 1 } then {
  pass "The shell script ran to completion."
} else {
  xfail "The shell script failed. This requires JAGag34829 to be fixed."
} 
set env(BATCH_RTC) "off"
set env(LD_PRELOAD) ""

# Validate the script output
if { [file size ${rtcdir}/${testfile}.out] } then {
  pass "Non-empty script output."
  catch "exec fgrep -c heap-check ${rtcdir}/${testfile}.out" heap_check_nbr
  if { $heap_check_nbr > 0 } then {
    pass "Validate script output."
  } else {
    fail "Validate script output."
  }
} else {
  fail "Incorrect script output."
}

set cat_leaks_file [ glob -nocomplain ${rtcdir}/cat*leaks ]
set chmod_heap_file [ glob -nocomplain ${rtcdir}/chmod*heap ]

# The 'cat' command on some systems leaks. 
if {  [file exists $cat_leaks_file] } then {
    pass "A leaks file has been obtained for the cat command."

    if { [file size $cat_leaks_file] } then {
      catch "exec fgrep -c infrtc $cat_leaks_file" chk_false_leak
      if { $chk_false_leak > 2 } then {
        fail "False positive leak found"
      } else {
        pass "cat leaks file."
      }
    }
} else {
    pass "No leaks file has been obtained for the cat command."
}

# The 'awk' command should produce a heap report.
set awk_heap_file [ glob -nocomplain ${rtcdir}/awk*heap ]
if {  [file exists $awk_heap_file] } then {
    pass "A heap file has been obtained for the awk command."
    # Validate the awk heap report.
    if { [file size $awk_heap_file] } then {
      catch "exec fgrep -c _wrtchk $awk_heap_file" wrtchk_nbr
      catch "exec fgrep -c tostring $awk_heap_file" tostring_nbr
      if { $wrtchk_nbr > 0 } then {
        pass "Validate awk heap file."
      } else {
        fail "Validate awk heap file."
      }
    
      if { $tostring_nbr > 0 } then {
        pass "Validate awk heap file."
      } else {
        fail "Validate awk heap file."
      }
    }
} else {
    fail "No heap file has been obtained for the awk command."
}


# Validate that no heap file has been created for 'chmod'. The 'files'
# option in rtcconfig did not include chmod.
if {  [file exists $chmod_heap_file] } then {
    fail "A heap file has been created for the chmod command."
} else {
    pass "No heap file has been obtained for the chmod command."
}

#cleanup
catch "exec rm -rf ${rtcdir}"
catch "exec rm -rf fix_dependencies gdb_files rtcconfig ${testfile}"

if { [istarget "hppa1.1-hp-hpux*"] || [istarget "hppa2.0w*-*-hpux*"] } then {
  catch "exec rm -f ./ls"
  catch "exec rm -f ./sed"
  catch "exec rm -f ./cal"
  catch "exec rm -f ./touch"
  catch "exec rm -f ./date"
  catch "exec rm -f ./echo"
  catch "exec rm -f ./cat"
  catch "exec rm -f ./awk"
}

return 0
