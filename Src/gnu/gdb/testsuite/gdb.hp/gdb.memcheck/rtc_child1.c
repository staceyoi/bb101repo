/* Program to test the "memory checking" after
   follow-fork/follow-exec event
*/

#include <stdio.h>
#include <unistd.h>

int dont_exit = 1;

int main(int argc, char **argv)
{
	while (dont_exit);

        if (fork() == 0)
        {
            execlp (argv[1], "rtc_child2", 0);
        }
}
