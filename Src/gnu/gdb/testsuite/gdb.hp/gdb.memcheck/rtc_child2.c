/* This program is to test the "memory checking" after
   the follow-fork/follow-exec event
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
        int i = 0;
	char *p;

        for (i=0; i < 100; i++)
                p = (char *)malloc (i);
}
