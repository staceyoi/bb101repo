#include <stdlib.h>

void overflow_check_on ()
{

}

void done_all_tests ()
{
    exit (1);
} 

void sbrk_negative_test ()
{
    sbrk (-10);
}

void huge_alloc_check ()
{

    char * cp;

    cp = malloc (1024 * 1024 * 10);
    free (cp);

}

void heap_growth_check()
{
    char * cp;

    cp = malloc (1024 * 1024 * 10);
    free (cp);
    cp = malloc (1024 * 1024 * 8);
    free (cp);
}

void enable_watch (char *cp)
{

}

void watch_check ()
{
    char * cp = malloc (100);
    enable_watch (cp);
    free (cp);
}

void scramble_check()
{

    char * cp;

    cp = malloc (100);
    strcpy (cp, "Hello world\n");
    free (cp);
    if (!strcmp(cp, "Hello world\n"))
      {
        printf ("block was not scrambled !\n");
        exit (1);
      }
}


void corrupt_footer()
{

  char * cp;
  cp = malloc (100);
  cp[100] = 100;
  free (cp);  /* should trigger event */
}


void corrupt_header()
{

  char * cp;
  cp = malloc (100);
  cp[-1] = 100;
  free (cp);  /* should trigger event */
}

/* When this function is called, gdb should have just check-free on */
void double_free_check()
{
    char * cp;
    cp = malloc (1000);
    free (cp);
    cp = malloc (100);
    strcpy (cp, "Hello world\n");

    /* Corrupt the block. This should not trigger an event now. */
    cp [-1] = 0;
    cp[100] = 0;
    free (cp);
    free (cp);
    if (strcmp(cp, "Hello world\n")) {
      printf ("Block scrambled when scrambling is off !\n");
      exit (1);
    }
}

int main()
{
    char cp;

    double_free_check ();
    overflow_check_on ();
    corrupt_header();
    corrupt_footer();
    scramble_check();
    watch_check ();
    heap_growth_check();
    huge_alloc_check ();
    sbrk_negative_test ();
    done_all_tests (); return 0;
}

#if 0
#ifndef __LP64__
#pragma HP_LONG_RETURN __rtc_d_trap
#pragma HP_NO_RELOCATION __rtc_d_trap

static void
__rtc_d_trap (first, second, third, fourth, fifth)
int first, second, third, fourth, fifth;
{
 sigset_t oset;

 /* We are here due to a bind-on-reference notification from dld.
    We need to jump to the real debugger hook, if it is safe to
    do so. It is not safe if SIGTRAPs are disabled for the current
    thread, because the breakpoint will not trigger if SIGTRAPs are
    masked. The program cannot proceed meaningfully having it a
    breakpoint and bad things happen afterward. Fortunately signal
    masks are thread private on HP-UX -- srikanth, 000313.
 */

 sigprocmask(
            SIG_SETMASK,   /* Will be ignored since ...   */
            0,             /* ... this is a NULL pointer  */
            &oset);
 if (sigismember (&oset, SIGTRAP))
   return;

 /* Safe to jump to the real debugger hook ..., The original debugger
    hook gets passed variable number of arguments. But currently we
    treat only BOR events specially, everything else gets treated as
    a SHL_LOAD
 */

 __d_trap (first, second, third, fourth, fifth);
}
#endif
#endif
