# include <sys/mman.h>
# include <stdlib.h>

int main()
{
    char * pointer;
    int page_size;

    page_size = getpagesize ();
    pointer = mmap(0, page_size, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 2048);
    if (pointer == MAP_FAILED) abort();
    return 0;
}

