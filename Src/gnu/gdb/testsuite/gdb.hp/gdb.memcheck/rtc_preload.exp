# rtc_inc.exp -- Expect script to test a memory leak of threaded program
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

set testfile rtc_preload
set srcfile1 ${srcdir}/${subdir}/${testfile}1.c
set srcfile2 ${srcdir}/${subdir}/${testfile}2.c
set binfile1 ${objdir}/${subdir}/${testfile}1
set binfile2 ${objdir}/${subdir}/${testfile}2

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}

#
if { [gdb_compile "$srcfile1" "${binfile1}" executable [list debug]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}
if { [gdb_compile "$srcfile2" "${binfile2}" executable [list debug]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile2}

send_gdb "set heap-check on\n"
gdb_expect {
  -re "$gdb_prompt $" {
       verbose "Setting heap-check on." 2
  }
  timeout {
      warning "Couldn't set heap-check on."
  }
}

# Follow parent: LD_PRELOAD should not be set in child
send_gdb "set follow-fork-mode parent\n"
gdb_expect {
  -re "$gdb_prompt $" {
       verbose "Setting follow-fork to parent process." 2
  }
  timeout {
      warning "Couldn't set follow-fork mode to parent."
  }
}
gdb_test "run" ".*LD_PRELOAD=.*xxxxxx.*" "run 1"

# Follow child: LD_PRELOAD should be set in child
send_gdb "set follow-fork-mode child\n"
gdb_expect {
  -re "$gdb_prompt $" {
       verbose "Setting follow-fork to child process." 2
  }
  timeout {
      warning "Couldn't set follow-fork mode to child."
  }
}
gdb_test "run" ".*LD_PRELOAD=.*librtc.*" "run 2"

# Test interactive follow-fork mode
send_gdb "set follow-fork-mode ask\n"
gdb_expect {
  -re "$gdb_prompt $" {
       verbose "Setting follow-fork to ask." 2
  }
  timeout {
      warning "Couldn't set follow-fork mode ask."
  }
}

# Select follow parent: LD_PRELOAD should not be set in child
gdb_load ${binfile2}
send_gdb "run\n"
gdb_test "0" ".*LD_PRELOAD=.*xxxxxx.*"  "select 1"

# Select follow child: LD_PRELOAD should be set in child
send_gdb "run\n"
gdb_test "1" ".*LD_PRELOAD=.*librtc.*" "select 2"

return 0
