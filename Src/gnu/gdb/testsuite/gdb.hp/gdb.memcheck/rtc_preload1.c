#include <strings.h>
#include <stdio.h>
#include <unistd.h>

int main (int argc, char* argv[], char* env[]) 
{
  fprintf(stderr, "Starting child\n");
  
  for (int i=0; env[i]; i++)
    {
      if (strncmp(env[i], "LD_PRELOAD", 10) == 0) 
        {
          if (strcmp(env[i], "LD_PRELOAD=") == 0)
            {
              fprintf(stderr, "LD_PRELOAD is not set\n");
              fflush(stderr);
              return 1;
            }
          else
            {
              fprintf(stderr, "%s\n", env[i]);
              fflush(stderr);
              return 0;
            }
        }
    }
  fprintf(stderr, "LD_PRELOAD is not set\n");
  fflush(stderr);
  return 1;
}
