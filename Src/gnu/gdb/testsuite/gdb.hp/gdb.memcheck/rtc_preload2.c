#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>

int main (int argc, char* argv[], char* env[]) 
{
  int pid, stat;
  char exec_name[MAXPATHLEN];
  strcpy(exec_name, argv[0]);

  /* This executable is named <blah>2. Exec <blah>1 */
  exec_name[strlen(argv[0])-1] = '1';
  switch(pid = fork())
    {
    case 0:
      fprintf(stderr, "Child\n");
      execl (exec_name, exec_name, (char *)0);
      break;
    default:
      fprintf(stderr, "Parent\n");
      wait(&stat);
      break;
    }
}

