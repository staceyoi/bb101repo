/* BeginSourceFile rtcapi_batch.c

It is to test RTC-API's in batch mode for memory leak.
*/

#include <stdio.h>
#include <stdlib.h>
#include "../../../../../../../../Products/WDB/Exports/opt/langtools/wdb/include/rtcapi.h"

void func();

int main()
{
        int i,res;
        res=rtc_logfile("gdb.hp/gdb.memcheck/rtcapi_batch_log",RTC_LOG_SET);
        if(!res)
                printf("rtc_logfile: successfull\n");
        else
                printf("rtc_logfile: failed\n");

        res=rtc_enable(RTC_CHECK_LEAKS);
        if(!res)
                printf("rtc_enable: successfull\n");
        else
                printf("rtc_enable: failed\n");

        for ( i=0; i <5;i++)
        {
                func();
		printf("Calling rtc_leaks for iteration: %d\n",i);
                res=rtc_leaks(RTC_LEAK_NEW);
                if(res!=-1)
                        printf("rtc_leaks: successfull\n");
                else
                        printf("rtc_leaks: failed\n");
        }
	res=rtc_leaks(RTC_LEAK_ALL);
	if(res!=-1)
		printf("rtc_leaks: successfull\n");
	else
		printf("rtc_leaks: failed\n");

        return 0;
}

void func()
{
        int *hh=0;
        hh=(int *)malloc(80);
        *hh = 56;
}
