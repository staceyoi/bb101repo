/* BeginSourceFile rtcapi_batch_thrd.c

It is to test RTC-API's in batch mode for multi-threaded program.
This test creates 3 threads and each thread leaks some memory.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include "../../../../../../../../Products/WDB/Exports/opt/langtools/wdb/include/rtcapi.h"

#define TRUE 1
#define FALSE 0
#define N_THREADS 3
#define MAX_LOCAL_VAL 40
#define OUTER_LOOP_COUNT 3

#define START_DEBUG  0

/* True if waiting for attach. */
int wait_here;

/* Thing to check for debugging purposes. */
int a_global = 0;

/* Thread-local storage. */
__thread int a_thread_local;

/* Check the results of thread-local storage. */
int thread_local_val[ N_THREADS ];

char* ll;

/* Routine for each thread to run, does nothing. */
void *spin( vp )
    void * vp;
{
    int i;
    long me = (long) vp;
 
    ll = malloc (20); /* Line number 45 */


#ifdef START_DEBUG
    printf( "== In thread %d\n", me );
#endif

    a_global++;

    a_thread_local = 0;

    for( i = 0; i < a_global; i++ ) {
        a_thread_local += i;
    }

    thread_local_val[ me ] = a_thread_local; /* Line number 65 */

#ifdef START_DEBUG
    printf( "== Thread %d, a_thread_local is %ld\n",
            (long) vp, a_thread_local );
#endif

   if( wait_here ) {
       /* Extend life of thread to extend life of thread-local var.
        * This makes life easier for human debugging (though you'd
        * probably want to make the delay longer).
        */
       sleep( 5 );
   }
   return ((void *)NULL);
}

void
do_pass( pass )
    int pass;
{
    int i;
    pthread_t t[ N_THREADS ];
    int err;

    for( i = 0; i < N_THREADS; i++) {
        thread_local_val[i] = 0;
    }
   
    /* Start N_THREADS threads, then join them so
     * that they are terminated.
     */
    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_create( &t[i], NULL, spin, (void *)i );
        if( err != 0 ) {
            printf( "== Start/stop, error %d in thread %d create\n", err, i );
        }
    }

    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_join(t[i], NULL );    /* Line 99 */
        if( err != 0 ) {                    /* Line 100 */
            printf( "== Start/stop, error in thread %d join\n", i );
        }
    }

    i = 10;  /* Line 111.  Null line for setting bpts on. */

#ifdef START_DEBUG
    for( i = 0; i < N_THREADS; i++) {
        printf( "   Local in thread %d was %d\n", 
		i, thread_local_val[i]);
    }
    printf( "== Pass %d done\n", pass );
#endif
}

void
do_it()
{
    /* We want to start some threads and then
     * end them, and then do it again and again
     */
    int i;
    int dummy;
    
    for( i = 0; i < OUTER_LOOP_COUNT; i++ ) {
        do_pass( i );
        dummy = i;      /* Line 128, null line for setting bps on */
    }
}

//void __rtc_init_leaks (int);

int i, j, k, l;

main( argc, argv )
int    argc;
char **argv;
{
   char* ll;
   int res;

   res=rtc_logfile("gdb.hp/gdb.memcheck/rtcapi_batch_thrd_log",RTC_LOG_SET);

   if(!res)
	printf("rtc_logfile: successfull\n");
   else
	printf("rtc_logfile: failed\n");

   res=rtc_enable(RTC_CHECK_LEAKS);

   if(!res)
	printf("rtc_enable: successfull\n");
   else
	printf("rtc_enable: failed\n");

   wait_here = FALSE;

   ll = malloc (21);  /* Line number 145 */



   if((argc > 1) && (0 != argv )) {
       if( 1 == atoi( argv[1] ) )
           wait_here = TRUE;
   }

   if (wait_here)
     sleep (100);

   while (wait_here);

   free (ll);

   ll = malloc (20); /* Line number 161 */
   ll = malloc (10); /* Line number 162. Not counted as leak in test */
 
#ifdef START_DEBUG 
    printf( "== Test starting\n" );
#endif

    do_it();
    
#ifdef START_DEBUG
    printf( "== Test done\n" );
#endif

    printf("Calling rtc_leaks for new leaks\n");
    res=rtc_leaks(RTC_LEAK_NEW);
    if(res!=-1)
          printf("rtc_leaks: successfull\n");
    else
          printf("rtc_leaks: failed\n");
 
    printf("Calling rtc_leaks for all leaks\n");
    res=rtc_leaks(RTC_LEAK_ALL);
    if(res!=-1)
          printf("rtc_leaks: successfull\n");
    else
          printf("rtc_leaks: failed\n");
 
    return(0); /* Line number 201 */
}

/* EndSourceFile */
