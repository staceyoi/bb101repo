# rtcapi_batch_thrd.exp -- Expect script to test RTC API's batch mode memory scrambling
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "hppa*-*-hpux11.*"] } {
    verbose "This test ignored for all hppa targets."
    return 0
}

set testfile rtcapi_batch_thrd
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}
set logfile ${objdir}/${subdir}/${testfile}_log

if { "${IS_ILP32}" == "FALSE" } {
  set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  set ldflags "ldflags=-lrtc64 -lpthread"
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  set ldflags "ldflags=-lrtc -lpthread"
}

set additional_flags "additional_flags=-L.. "

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags $ldflags]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set pid [pid]
system "echo 'check_leaks=on'  > rtcconfig"
system "echo 'check_heap=on'  >> rtcconfig"
system "echo 'files=rtcapi_batch_thrd' >> rtcconfig"
system "echo 'output_dir=/tmp' >> rtcconfig"
catch "exec rm -f /tmp/${testfile}*leaks /tmp/${testfile}*heap ${logfile}"
set log /tmp/rtcapi_batch_thrd_$pid
set env(GDB_SERVER) "${objdir}/../gdb"

set env(BATCH_RTC) "on"

catch "exec ${binfile} "
catch "exec grep \"New Leaks report\" ${logfile} > ${log}"
catch "exec grep \"All Leaks report\" ${logfile} >> ${log}"
catch "exec grep \"bytes leaked in\" ${logfile} >> ${log}"
catch "exec grep \"Total bytes\" ${logfile} >> ${log}"
catch "exec cat $log | wc -l" match_pattern

if { $match_pattern != 8 } then {
    fail "rtcapi_batch_thrd failed - ($match_pattern = 8 expected)"
    catch "exec cat ${logfile}" exec_output
    send_user "Output file :\n$exec_output\n"
} else {
    pass "rtcapi_batch_thrd  passed "
}

catch "exec /bin/rm -f ${binfile} rtcconfig ${logfile} /tmp/${testfile}.*.heap ${log}"

return 0
