#include <stdio.h>
#include <malloc.h>
#include "../../../../../../../../Products/WDB/Exports/opt/langtools/wdb/include/rtcapi.h"

void corrupt_footer1()
{   
    
  char * cp;

  cp = malloc (100);
  cp[100] = 100;
  //free (cp);  /* should trigger event */
}
    

void corrupt_footer2()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
  //free (cp);  /* should trigger event */
}


void corrupt_footer3()
{

  char * cp;

  cp = malloc (100);
  cp[100] = 100;
  //free (cp);  /* should trigger event */
}
    
void corrupt_header()
{   
    
  char * cp; 
  cp = malloc (100);
  cp[-1] = 77;
  //free (cp);  /* should trigger event */
}

int main()
{ 
  int i,res;
  res=rtc_logfile("gdb.hp/gdb.memcheck/rtcapi_heap_batch_log",RTC_LOG_SET);
  if(!res)
	printf("rtc_logfile: successfull\n");
  else
	printf("rtc_logfile: failed\n");

  res=rtc_enable(RTC_CHECK_HEAP);
  if(!res)
	printf("rtc_enable: successfull\n");
  else
	printf("rtc_enable: failed\n");
 
  for (i=0; i<101; i++)
  {
    rtc_heap();

    corrupt_header();
    corrupt_footer1();

    rtc_heap_corruption();

    corrupt_footer2();

    rtc_heap();

    corrupt_footer3();
  }

  rtc_heap_corruption();

  res=rtc_disable(RTC_CHECK_HEAP);
  if(!res)
        printf("rtc_disable: successfull\n");
  else
        printf("rtc_disable: failed\n");

  res=rtc_enable(RTC_CHECK_LEAKS);
  if(!res)
        printf("rtc_enable: successfull\n");
  else
        printf("rtc_enable: failed\n");

  res=rtc_leaks(0);

  return 1;
}
