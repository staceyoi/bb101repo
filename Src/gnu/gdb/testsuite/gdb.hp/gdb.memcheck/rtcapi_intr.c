/* BeginSourceFile rtcapi_intr.c

It is to test RTC-API's in interactive mode for memory leak.
*/

#include <stdio.h>
#include <stdlib.h>

void func();

int main()
{
        int i,res;

        for ( i=0; i <5;i++)
        {
                func();
        }
        return 1;
}
void func()
{
        int *hh=0;
        hh=(int *)malloc(80);
        *hh = 56;
}
