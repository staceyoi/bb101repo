#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "../../../../../../../../Products/WDB/Exports/opt/langtools/wdb/include/rtcapi.h"
 
int inputValStr1[8][3] = { {10, 90, 0},
                           {67, 0, 0},
                           {12, 11, 89},
                           {56, 0, 0},
                           {15, 74, 0},
                           {10, 49, 0},
                           {90, 0, 0},
                           {12, 17, 87}};

int inputValStr2[8][3] = { {89, 0, 0},
                           {12, 70, 0},
                           {13, 21, 89},
                           {78, 0, 0},
                           {13, 83, 0},
                           {93, 0, 0},
                           {90, 0, 0},
                           {12, 17, 87}};

/* Allocate memory and return it to the caller */ 
void *allocateMemory(size_t n) 
{
  void *memBlock = NULL;
  printf("Allocating memory of size %d\n", n);
    
  memBlock = malloc(n);
  if (!memBlock)
  {
    printf("No memory available, aborting...\n");
    abort ();
  }
 
  return memBlock;
}
 
/* Create a header string and return to the caller */ 
char *createHeader() 
{
  char *headerString = (char *) allocateMemory(20);
  strcpy (headerString, "Header : My string ");
  return headerString;
}
 
/* Create a string with header and input. Return to the caller */ 
char *createString(char *input, int n) 
{ 
  #define MAXBADINPUT (30)

  char *hdrString, *myString;
  int inputLength = strlen(input);
  static int badInput;
  hdrString = createHeader();
  /* Input length too small ? return back from here */
  if (n < inputLength + 20)
  {
    hdrString = NULL;
    printf("Too small size for a string, not allocating\n");
    badInput++;
    printf("Number of badInput is %d\n", badInput);
    if (badInput > MAXBADINPUT)
    {
      printf("Too many bad inputs.. aborting\n");
      abort();
    }
    return NULL;
  }
 
  /* Allocate the string, copy the header to it */
  myString = (char *)allocateMemory(n);
  strcpy(myString, hdrString);
  free(hdrString);
 
  /* Copy the input to the string after header and return */
  strcpy(myString+20, input);
  return (myString);
 }
 

void compareStrings(int i)
{
  int s1length, s2length;
  char *str1 = NULL, *str2 = NULL;
  int attempt = 0;

  /* Allocate first string and initialize it */
  do
  {
    printf("Thread %d : Enter length of string 1\n", i);
    if (attempt > 2)
      s1length = 100;
    else
      s1length = inputValStr1[i-1][attempt++];
    printf("Thread %d : Entered s1length = %d\n", i, s1length);
    str1 = createString("String 1", s1length);
    rtc_leaks(1); 
  } while (str1 == NULL);
 
  attempt = 0;

  rtc_heap();
  /* Allocate second string and initialize it */
  do
  {
    printf("Thread %d : Enter length of string 2\n", i);
    if (attempt > 2)
      s2length = 100;
    else
      s2length = inputValStr2[i-1][attempt++];
    printf("Thread %d : Entered s2length = %d\n", i, s2length);
    str2 = createString("String 2", s2length);
    rtc_leaks(1); 
  } while (str2 == NULL);
 
  rtc_heap();

  /* Compare strings */
  if (strcmp(str1, str2))
  {
    printf("str1 > str2 \n");
  }
  else
  {
    printf("str1 <= str2 \n");
  }

  free(str1);
  free(str2);
}
 
int main(int argc, char *argv[])
{
  pthread_t tid[8];

  if (argc < 2) {
    fprintf(stderr, "Usage : a.out logfile\n");
    return -1;
  }

  int res = rtc_enable(31);
  if (res != 0)
    fprintf(stderr, "rtc_enable failed (res = %d)\n", res);

  res = rtc_logfile(argv[1], RTC_LOG_SET);
  if (res != 0)
    fprintf(stderr, "rtc_logfile failed (res = %d)\n", res);

  /* Example memory leak program */
  for (int i = 0; i < 8; i++) {
    if( pthread_create(&tid[i], NULL, &compareStrings, (void *)(i+1))) {
      printf("call to pthread failed\n");
    }
  }

  for (int i = 0; i < 8; i++) {
    pthread_join(tid[i], NULL);
  }

  exit(0);
}
