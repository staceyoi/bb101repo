#include <stdio.h>
#include <malloc.h>

#define MALLOC_SIZE sizeof(int)*25 + 1
int main ()
{
    
    int  i, *p;
    p = malloc (MALLOC_SIZE);

    printf("Scramble malloc() test\n\n");
    for ( i=0; i < 26; i++)
    	printf ("p[%d] = %p\n", i, p[i] );
    fflush(stdout);

//Lets change the pattern now
    printf("\n\n");
    for ( i=0; i < 25; i++)
    	p[i]= i;

//check if we have changed the pattern correctly...
    //for ( i=0; i < 26; i++)
    	//printf ("Scramble test: p[%d] = %p ", i, p[i] );


// We intentionally use the same block we freed and check the values we see there,
// to check, if RTC has scrambled the freed block
    free(p);
    printf("\n\n Scramble free() test \n\n");
    for ( i=0; i < 26; i++)
    	printf ("p[%d] = %p\n", i, p[i] );
    printf("\n\n");
    exit (0);
}

