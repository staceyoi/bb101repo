#include <stdio.h>

int shared_global = 5;

void boo ()
{

  shared_global = 15;

}

void test ()
{
  int local_shared = 1;
  shared_global = 10;
  boo ();
  shared_global = 20;
  boo ();
}

