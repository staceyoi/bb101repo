# Copyright (C) 2006 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# JAGag24121: +check=malloc reports spurious leaks

# Checks whether shared library data segments are accounted for 
# correctly in +check=malloc mode

if $tracelevel then {
	strace $tracelevel
}

set prms_id 0
set bug_id 0

set testfile shlib_check_malloc
set testfile1 shlib_check_malloc2
set srcfile  ${srcdir}/${subdir}/${testfile}.c
set srcfile1  ${srcdir}/${subdir}/${testfile1}.c
set objfile1  ${objdir}/${subdir}/${testfile1}.o
set binfile  ${objdir}/${subdir}/${testfile}
set shlib  ${objdir}/${subdir}/libsl.sl

if { [gdb_compile "${srcfile1}" "${objfile1}" object {debug additional_flags=+z}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

remote_exec build "ld -b ${objfile1} -o ${shlib}"

if { [gdb_compile "${srcfile}" "${binfile}" executable [list debug ${shlib} ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}

if { [istarget "ia64*-hp-*"] &&  "${IS_ILP32}" == "FALSE" } {
   set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
} else {
   set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}

set GDBFLAGS "$GDBFLAGS -leaks"

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_test "file $binfile"

gdb_test "b 22" ".*Breakpoint 1 at.*${srcfile}.*" "set breakpoint 1"
gdb_test "run" ".*Breakpoint 1.*main.*${srcfile}.*printf.*" "hit breakpoint 1"
gdb_test "info leaks" ".*Scanning.*No new leaks.*" "info leaks"

gdb_exit

if { ![istarget "ia64*-hp-*"] } {
  catch "exec rm -rf ${binfile} ${shlib} "
  return 0
}

# Now reproduce JAGag24121: +check=malloc reports spurious leaks

if { [gdb_compile "${srcfile}" "${binfile}" executable [list debug additional_flags=+check=malloc ${shlib} ]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

set pid [pid]

# +check=malloc doesn't respect LIBRTC_SERVER, so use SHLIB_PATH
if { [istarget "ia64*-hp-*"] &&  "${IS_ILP32}" == "FALSE" } {
   catch "exec rm -rf ${objdir}/${subdir}/librtc.so"
   catch "exec ln ../librtc64.so ${objdir}/${subdir}/librtc.so" exec_output
#  send_user "ln output = |$exec_output|.\n"
   set env(SHLIB_PATH) "${objdir}/${subdir}"
} else {
#  send_user "Took 32-bit path through SHLIB_PATH code.\n"
   set env(SHLIB_PATH) "${objdir}/.."
}
#send_user "SHLIB_PATH=|${objdir}/..|\n"

catch "exec ${binfile} >& /tmp/${testfile}.out.$pid" exec_output
set env(SHLIB_PATH) ""
#send_user "${binfile} output=|${exec_output}|\n"

set exec_output [exec cat /tmp/${testfile}.out.$pid | wc -l 2> /dev/null]

# This program normally puts out two lines of text
if { $exec_output == 2 } then {
    pass "no spurious leaks as in interactive gdb use"
    catch "exec rm -rf /tmp/${testfile}.out.$pid"
} else {
    fail "wrong amount of output produced, probably spurious Memory Leak report"
    catch "exec cat /tmp/${testfile}.out.$pid " exec_output
    send_user "/tmp/${testfile}.out.$pid = $exec_output.\n"
}

# clean up
catch "exec rm -rf ${binfile} ${shlib} ${objdir}/${subdir}/librtc.so "

set env(GDB_SERVER) ""
set env(LIBRTC_SERVER) ""

return 0
