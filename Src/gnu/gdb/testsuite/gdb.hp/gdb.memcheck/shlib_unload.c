#include <stdio.h>
#include <dl.h>
#include <stdlib.h>
 
 
 main()
 {
 	 shl_t l;
         void (*foo) ();
	 int retval;
 
 	 l = shl_load ("gdb.hp/gdb.memcheck/libsl.sl", BIND_IMMEDIATE, 0L);
	 if (l == NULL) {
	     fprintf(stderr, "shl_load (libsl.sl) failed.\n");
	     exit(1);
	     }
         retval = shl_findsym (&l, "foo", TYPE_PROCEDURE, &foo);
	 if (retval == -1) {
	     fprintf(stderr, "shl_findsym (libsl.sl, foo) failed.\n");
	     exit(2);
	     }
         foo ();
         shl_unload (l);
         printf ("Done\n");
         printf ("Really Done\n");
 }
