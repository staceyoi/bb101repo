#include <stdio.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <ucontext.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

void
sig_trap(int iSigNum)
{
  printf ("In SIGSEGV handler\n");
  sleep(10);
  printf ("Returning from SIGSEGV handler\n");
}

int
setup_stack (int signo)
{
    stack_t         sigstk, *oss;
    u_int           ps;
    struct sigaction *act = (struct sigaction *) malloc (sizeof(struct sigaction));
    oss = (stack_t *) malloc (sizeof (stack_t));
    if (((sigstk.ss_sp = (char *) malloc(SIGSTKSZ)) == NULL)) {
        fprintf(stderr, "can't alloc alt stack\n");
    }
    sigstk.ss_size = SIGSTKSZ;
    sigstk.ss_flags = 0;
    if (sigaltstack(&sigstk, oss) < 0) {
        perror("sigaltstack");
    }
    sigstk.ss_sp = NULL;
    act->sa_handler = sig_trap;
    act->sa_flags = (SA_SIGINFO | SA_ONSTACK);

    if (sigaction(SIGSEGV, act, NULL) != 0) {
        perror("sigaction");
        return (1);
    }
    memset (act, 0, sizeof (struct sigaction));
    free (act);
    free (oss);
    oss = NULL;

    return (0);
}

void foo (void)
{
  // Again set up a stack, cause memory leak
  setup_stack (SIGSEGV);
  kill(getpid(), SIGSEGV);
}

void *function1( void *ptr )
{
  setup_stack (SIGSEGV);
  setup_stack (SIGSEGV);
  setup_stack (SIGSEGV);
  sleep(10);
  printf ("Returning from function 1\n");
}

void *function2( void *ptr )
{
  setup_stack (SIGSEGV);
  foo();
  sleep(10);
  printf ("Returning from function 2\n");
  return 0;
}

int
main()
{
  int *p = 0;

  pthread_t thread1, thread2;
  int ret1, ret2;

  printf("sigaltstack : Size of stack = %d\n", SIGSTKSZ);
  /* Create independent threads each of which will execute function */
  ret1 = pthread_create(&thread1, NULL, function1, NULL);
  ret1 = pthread_create(&thread2, NULL, function2, NULL);

  sleep(5);
  printf ("Returning from main \n");
  exit(0);
}
