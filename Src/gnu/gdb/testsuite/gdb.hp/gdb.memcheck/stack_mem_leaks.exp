#   Copyright (C) 1997 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Please email any bugs, comments, and/or additions to this file to:
# wdb-help@cup.hp.com
#
# This script is intended to test the fix for JAGag23792: Spurious 
# leaks reported by gdb. At the first breakpoint we should not report
# leaks because the heap pointers are still on the stack. These become
# real leaks when we leave the routine where they are allocated and pop
# the frame off the stack.
#

set testfile "stack_mem_leaks"
set srcfile  ${testfile}.c
set binfile  ${objdir}/${subdir}/${testfile}
remote_exec build "rm -f ${binfile}"

# build the test cases
#

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
}

#
if { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable [list debug ]] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

gdb_test "set heap-check on" "" ""
gdb_test "b really" "Breakpoint 1 at.*file.*" "b really"
gdb_test "run" ".*Breakpoint 1, really.*" "run"
gdb_test "info leaks" "Scanning for memory leaks.*No new leaks.*" "info leaks 1"
gdb_test "finish" "Run till exit from #0.*really.*" "finish 1"
gdb_test "finish" "Run till exit from #0.*loose.*" "finish 2"
gdb_test "finish" "Run till exit from #0.*broke.*" "finish 3"
gdb_test "info leaks" ".*$decimal bytes leaked in $decimal blocks.*broke.*" "info leaks 2"

gdb_exit
