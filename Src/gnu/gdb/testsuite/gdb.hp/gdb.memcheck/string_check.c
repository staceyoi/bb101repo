#include <stdio.h>
#include <string.h>
#include <malloc.h>


/*
 * strcat: let's do a combination of s2 > s1, s2==s1 s2 < s1.  For > < 
 * cases do bigger/smaller 
 */ 

void strcat_warn()
{
  char * p;

  /* s2 < s1 ok */
  p = calloc (10, 1);
  strcat (p, "helsinki");

  
  /* s2 > s1 not */
  p = calloc (7, 1);
  strcat (p, "helsinki");
  

  /* s2 = s1 not cause no room for NULL */
  p = calloc (8, 1);
  strcat (p, "helsinki");
  

  /* s2-1 = s1 ok with room for NULL */
  p = calloc (9, 1);
  strcat (p, "helsinki");
  
  

  /* do the same set with copying to existing string */

  p = calloc (15, 1);
  strcpy (p, "helsinki");
  strcat (p, "espoo");    /* ok */

  strcpy (p, "helsinki");
  strcat (p, "helsinki"); /* not ok */

  strcpy (p, "helsinki");
  strcat (p, "helsink");  /* not ok, no room for null */
  
  strcpy (p, "helsinki");
  strcat (p, "helsin");  /*  ok, room for null */

}


void strncat_warn()
{
  char * p;

  /* set 1: s1 < s2 (n < s1, n > s1, n==s1) */
  p = calloc (5, 1);
  strncat (p, "helsinki", 4); /* ok, copies up to 4 */
  
  
  p = calloc (5, 1);
  strncat (p, "helsinki", 6); /* NO, both n and s2 are bigger than 5 */
  

  p = calloc (5, 1);
  strncat (p, "helsinki", 5); /* no room for null */
  


  /* set 2: s1 > s2 (n < s1, n > s1, n==s1) */
  p = calloc (10, 1);
  strncat (p, "helsinki", 9); /* ok */
  
  
  p = calloc (10, 1);
  strncat (p, "helsinki", 11); /* ok, copy only helsinki */
  

  p = calloc (10, 1);
  strncat (p, "helsinki", 10); /* ok, copy only helsinki */
  

  /* set 3:  s1 = s2 (n < s1, n > s1, n==s1) */
  p = calloc (8, 1);
  strncat (p, "helsinki", 7); /* ok, copy up to 7 and room for null*/
  
  
  p = calloc (8, 1);
  strncat (p, "helsinki", 11); /* NO copy up to size of helsinki, null */
  

  p = calloc (8, 1);
  strncat (p, "helsinki", 8); /* no room for null */
  

  p = calloc (15, 1);
  strcpy  (p, "helsinki");
  strncat (p, "helsinki", 5);  /* ok */

  strcpy  (p, "helsinki");
  strncat (p, "helsinki", 7);  /* not ok */

  strcpy  (p, "helsinki");
  strncat (p, "espoo", 10);  /* ok */

  
  
}

void memccpy_warn()
{
  char *p; 

  p = calloc (5, 1);
  memccpy (p, "bbbzb", 'z', 1); /* ok */
   

  p = calloc (5, 1);
  memccpy (p, "bbbzbbbbbb", 'z', 10); /* ok, cpy up to z */
  

  p = calloc (5, 1);
  memccpy (p, "bbbbbbbzbb", 'z', 1); /* ok cpy up to 1 */
  

  p = calloc (5, 1);
  memccpy (p, "bbbbbbbzbb", 'z', 10); /* not ok */
 
}

void strcpy_warn()
{
  char *p;
  p = calloc (7, 1);
  strcpy(p, "lapland"); /* not ok, no room for NULL */
  strcpy(p, "laplan");  /* ok */ 
  strcpy(p, "lapland1"); /* bad */
  

}


void strncpy_warn()
{
  char *p;
  p = calloc (5, 1);
  strncpy(p, "lapland", 4); /* ok */
  strncpy(p, "lapland", 6);  /* bad, 6 has precedence */ 
  strncpy(p, "lapland", 5);  /* ok, exactly */
  

}


/*
 * want to test byte c position fits in s1 while n < s1, n=s1, n >s1
 * results: should be ok in all cases since it stops copying after c 
 * and n is useless
 *
 * want to test byte c position fits in s1 but right at the edge of s1
 *    while n < s1, n=s1, n >s1
 * results: should be ok in all cases since it stops copying after c 
 * and n is useless and unlike string family this one doesn't care 
 * about null byte
 *
 * want to test byte c position DOES NOT fit in s1 
 *    while n < s1, n=s1, n >s1
 * results: ok in most cases except when n > s1 since the control factor is 
 * n now!
 */
void memccpy_warn2()
{
  char *p; 

  p = calloc (3, 1);
  memccpy (p, "bzbbb", 'z', 1); /* ok, copy only 1 */
  memccpy (p, "bzbbb", 'z', 3); /* ok, copy only 2 */
  memccpy (p, "bzbbb", 'z', 5); /* ok, copy only 2 */
  memccpy (p, "bbzbbb", 'z', 1); /* ok copy only 1 */
  memccpy (p, "bbzbbb", 'z', 3); /* ok, copy only 3, ok without null */
  memccpy (p, "bbzbbb", 'z', 5); /* ok, copy only 3 */
  memccpy (p, "bbbbzbbb", 'z', 1); /* ok copy only 1 */
  memccpy (p, "bbbbzbbb", 'z', 3); /* ok, copy only 3 */
  memccpy (p, "bbbbzbbb", 'z', 5); /* NO! both n and c byte are out of range */
  

}

/* do a few where special char 'c' does not exist  */
void memccpy_warn3()
{
  char *p;

  p = calloc (5, 1);
  memccpy (p, "abcd", 'Z', 4); /* ok */
  memccpy (p, "abcd", 'Z', 5); /* ok */

  /* tricky, it looks as though it should not warn because 
   * s2 should fit.  After some brainstorming with Dennis
   * we agree it should warn because this is byte move so
   * it follows the 6 and not the len of abcd 
   */
  memccpy (p, "abcd", 'Z', 6); /* NO */

  memccpy (p, "abcde", 'Z', 4); /* ok */
  memccpy (p, "abcde", 'Z', 5); /* ok */
  memccpy (p, "abcde", 'Z', 6); /* NO */

  memccpy (p, "abcdef", 'Z', 4); /* ok */
  memccpy (p, "abcdef", 'Z', 5); /* ok */
  memccpy (p, "abcdef", 'Z', 6); /* NO */

   
}


void main()
{
  strcat_warn(); 
  strncat_warn();  
  memccpy_warn();
  strcpy_warn(); 
  strncpy_warn();
  memccpy_warn2();
  memccpy_warn3();
}




