#include <stdio.h>
#include <malloc.h>



/*
 * everything is controlled by n but still good to test s2 > s1,
 * s2 = s1, s2 < s1 while n > s1, n=s1, n < s1
 */ 

void memcpy_warn()
{
  char *p;

  p = calloc (5, 1);
  memcpy (p, "lapland",  6); /* bad */
  memcpy (p, "lapland",  5); 
  memcpy (p, "lapland",  4); 

  memcpy (p, "espoo",  6); /* bad */
  memcpy (p, "espoo",  5); 
  memcpy (p, "espoo",  4); 

  memcpy (p, "boo",  6); /* bad */
  memcpy (p, "boo",  5); 
  memcpy (p, "boo",  4); 

  
}


void memset_warn()
{
  char *p;

  p = calloc (5, 1);
  memset (p, 'z',  6); /* bad */
  memset (p, 'z',  5); /* ok */
  memset (p, 'z',  4); /* ok */
  

}

void bzero_warn()
{
  char *p;

  p = calloc (5, 1);
  bzero(p, 6);
  bzero(p, 5);
  bzero(p, 4);
  
}


/*
 * copies n bytes from the area pointed to by s1 to the
 * area pointed to by s2 so n is the determining factor.
 * test with s1 < s2, s1 = s2, s1 > s2 while n <s2, n=s2, n>s2
 */ 
 

void bcopy_warn()
{

  char *p;

  p = calloc (5, 1);

  /* s1 < s2 */
  bcopy("booo", p, 6);  /* bad */
  bcopy("booo", p, 5);
  bcopy("booo", p, 4);

  /* s1 = s2 */
  bcopy("espoo", p, 6); /* bad */
  bcopy("espoo", p, 5);
  bcopy("espoo", p, 4);

  /* s1 > s2 */
  bcopy("lapland", p, 6); /* bad */
  bcopy("lapland", p, 5);
  bcopy("lapland", p, 4);

  


}


void memmove_warn()
{
  char *p;

  p = calloc (5, 1);

  /* s2 > s1 */
  memmove(p, "lapland", 6);
  memmove(p, "lapland", 5);
  memmove(p, "lapland", 4);

  /* s2 = s1 */
  memmove(p, "espoo", 6);
  memmove(p, "espoo", 5);
  memmove(p, "espoo", 4);


  /* s2 < s1 */
  memmove(p, "espo", 6);
  memmove(p, "espo", 5);
  memmove(p, "espo", 4);


}


void main()
{
  memcpy_warn(); 
  memset_warn();  
  bzero_warn();
  bcopy_warn(); 
  memmove_warn();
}




