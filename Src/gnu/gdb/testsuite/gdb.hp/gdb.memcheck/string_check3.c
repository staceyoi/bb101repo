#include <stdio.h>
#include <malloc.h>

/* now we want to check the actual operation of each call 
 * check that it operates properly within range.  Check it
 * operates ok when out of range.  Run it through once without
 * string check on, do it with string check.  Both cases should
 * match.    
 */

void memcpy_warn()
{
  char *p;

  p = calloc (5, 1);
  memcpy (p, "lapland",  5); 
  if (!strncmp(p, "lapland", 5))
    printf("memcpy_in_range: ok\n");
  else
    printf("memcpy_in_range: not ok\n");

  memcpy (p, "lapland",  6); /* bad */
  if (!strncmp(p, "lapland", 6))
    printf("memcpy_out_range: ok\n");
  else 
    printf("memcpy_out_range: not ok\n");
   
}


void memset_warn()
{
  char *p;

  p = calloc (5, 1);
  memset (p, 'z',  5); /* ok */
  if (!strncmp(p, "zzzzz", 5))
    printf("memset_in_range: ok\n");
  else
    printf("memset_in_range: not ok\n");

  memset (p, 'z',  6); /* bad */
  if (!strncmp(p, "zzzzzz", 6))
    printf("memset_out_range: ok\n");
  else
    printf("memset_out_range: not ok\n");

}

void bzero_warn()
{
  char *p;

  p = calloc (5, 1);
  strcpy(p, "ABCDE");
  bzero(p, 5);

  strcpy(p, "ABCDEF");
  bzero(p, 6);
}


/*
 * copies n bytes from the area pointed to by s1 to the
 * area pointed to by s2 so n is the determining factor.
 * test with s1 < s2, s1 = s2, s1 > s2 while n <s2, n=s2, n>s2
 */ 
 

void bcopy_warn()
{

  char *p;

  p = calloc (5, 1);

  bcopy ("lapland", p, 5); 
  if (!strncmp(p, "lapland", 5))
    printf("bcopy_in_range: ok\n");
  else
    printf("bcopy_in_range: not ok\n");

  bcopy ("lapland", p, 6); /* bad */
  if (!strncmp(p, "lapland", 6))
    printf("bcopy_out_range: ok\n");
  else
    printf("bcopy_out_range: not ok\n");
  
}


void memmove_warn()
{
  char *p;

  p = calloc (5, 1);

  memmove(p, "lapland", 5);
  if (!strncmp(p, "lapland", 5))
    printf("memmove_in_range: ok\n");
  else
    printf("memmove_in_range: not ok\n");

  memmove(p, "lapland", 6);
  if (!strncmp(p, "lapland", 6))
    printf("memmove_out_range: ok\n");
  else
    printf("memmove_out_range: not ok\n");
  
}


/* ==========================================  */



void strcat_warn()
{
  char * p;

  p = calloc (15, 1);
  strcpy (p, "helsinki");
  strcat (p, "espoo");    /* ok */
  if (!strncmp (p, "helsinkiespoo",13))
    printf("strcat_in_range: ok\n");
  else
    printf("strcat_in_range: not ok\n");

  strcpy (p, "helsinki");
  strcat (p, "helsinki"); /* not ok */
  if (!strncmp (p, "helsinkihelsinki", 16))
    printf("strcat_out_range: ok\n");
  else
    printf("strcat_out_range: not ok\n");

}


void strncat_warn()
{
  char * p;

  p = calloc (15, 1);
  strcpy  (p, "helsinki");
  strncat (p, "lapland", 5);  /* ok */
  if (!strncmp(p, "helsinkilapland", 13))
    printf("strncat_in_range: ok\n");
  else
    printf("strncat_in_range: not ok\n");

  strcpy  (p, "helsinki");
  strncat (p, "helsinki", 7);  /* not ok */
  if (!strncmp (p, "helsinkihelsinki",15))
    printf("strncat_out_range: ok\n");
  else
    printf("strncat_out_range: not ok\n");

}

void memccpy_warn()
{
  char *p; 

  p = calloc (5, 1);
  memccpy (p, "bbbzb", 'z', 1); /* ok */

  if (!strncmp(p,"b",1))
    printf("memccpy_in_range: ok\n");
  else
    printf("memccpy_in_range: not ok\n");

  memccpy (p, "bbbzbbbbbb", 'z', 10); /* ok, cpy up to z */
  if (!strncmp(p,"bbbz",4))
    printf("memccpy_in_range: ok\n");
  else
    printf("memccpy_in_range: not ok\n");

  memccpy (p, "bbbbbbzbb", 'z', 10); /* not ok */
  if (!strncmp(p, "bbbbbbz", 7))
    printf("memccpy_out_range: ok\n");
  else
    printf("memccpy_out_range: not ok\n");

}

void strcpy_warn()
{
  char *p;
  p = calloc (7, 1);
  strcpy(p, "laplan");  /* ok */ 
  if (!strncmp(p, "laplan",6))  /* ok */ 
    printf("strcpy_in_range: ok\n");
  else
    printf("strcpy_in_range: not ok\n");

  strcpy(p, "lapland"); /* not ok, no room for NULL */
  if (!strncmp(p, "lapland", 7)) /* not ok, no room for NULL */
    printf("strcpy_out_range: ok\n");
  else
    printf("strcpy_out_range: not ok\n");


}


void strncpy_warn()
{
  char *p;
  p = calloc (5, 1);
  strncpy(p, "lapland", 4); /* ok */
  if (!strncmp(p, "lapland", 4)) /* ok */
    printf("strncpy_in_range: ok\n");
  else
    printf("strncpy_in_range: not ok\n");

  strncpy(p, "lapland", 6);  /* bad, 6 has precedence */ 
  if (!strncmp(p, "lapland", 6))  /* bad, 6 has precedence */ 
    printf("strncpy_in_range: ok\n");
  else
    printf("strncpy_in_range: not ok\n");
  

}

void main()
{

  memcpy_warn(); 
  memset_warn();  
  bzero_warn();
  bcopy_warn(); 
  memmove_warn();


  strcat_warn(); 
  strncat_warn();  
  memccpy_warn();
  strcpy_warn(); 
  strncpy_warn();
}




