# Used to test mixed mode core file debugging. The mixed application
# crashes with a SIGSEGV. The crash occurs in an IA function called by another
# PA function. The call sequence is like:
# IA func -> PA func -> PA func -> IA func
# The 2nd IPF function called belongs to a different IA shared library and
# is not part of the main load module.
#
# Tests both 32 bit and 64 bit
#
#   Copyright 1994, 1995 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# use this to debug:
# log_user 1

set timeout 800

if $tracelevel {
    strace $tracelevel
}

if { [skip_hp_tests] } { continue }

if ![istarget "ia64*-hp-*"] {
    verbose "Mixed mode testing is only for ia64."
    return 0
}

# Currently commenting out the whole 64 bit testing. TODO: to be enabled when
# 64 bit pixie support is up.
if 0 {
set testfile "pa2pa2ia64"
set binfile ${objdir}/${subdir}/${testfile}
set dldlib "dld64.so"

# Use the dld in the source directory.
catch "exec /bin/cp -f ${srcdir}/${subdir}/${dldlib} ."
catch "exec chmod +x ${dldlib}"
catch "exec /bin/cp -f ${srcdir}/${subdir}/${dldlib} ${objdir}/${subdir}/"

# Use the libaries.so in the current directory.
set env(PX_LIBARIES) "${srcdir}/${subdir}/libaries64.so"
catch "exec cp -f ${srcdir}/${subdir}/pa_2_pa_2_ia.z ${objdir}/${subdir}/"
catch "exec cp -f ${srcdir}/${subdir}/libpa2pa2ia64.sl ."
catch "exec chmod +x ${objdir}/${subdir}/pa_2_pa_2_ia.z"
# Create the required source files, Compile the test binary and
# Create the required links
catch "exec ${objdir}/${subdir}/pa_2_pa_2_ia.z"
# Run the test binary -- it should produce a core
catch "exec ./pa2pa2ia64"
# Rename the core file
catch "exec mv ./core ./core.pa2pa2ia64"
catch "exec mv ./pa2pa2ia64 ./core.pa2pa2ia64 ${objdir}/${subdir}/"

set env(GDB_ARIES_LIB) "${srcdir}/${subdir}/libaries64.so"
set env(GDB_SHLIB_PATH) "${srcdir}/${subdir}:."


gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Test if the topmost IA frame is displayed by default when the core is loaded
gdb_test "core ${objdir}/${subdir}/core.pa2pa2ia64" ".*pa2pa2ia64.*ia_func.*pa2pa2ia.ialib.c.*"

# Test the backtrace -- An IA frame followed by 2 PA frames followed by 
# the 'main' IA frame should get displayed.
gdb_test "where" ".*0.*ia_func.*pa2pa2ia.ialib.c.*1.*goo.*libpa2pa2ia64.*2.*foo.*libpa2pa2ia64.*3.*main.*pa2pa2ia.ia.c.*" 

# Set the frame to the transition PA frame -- 'goo'
gdb_test "f 1" ".*1.*goo.*libpa2pa2ia64.*" 

# Info frame on the PA frame to see PA frame related info
gdb_test "i frame" ".*Stack level 1, PA frame at.*pcoqh.*in goo.*saved pcoqh.*called by frame.*Arglist at.*Locals at.*Previous .*sp.*Saved registers:.*r3 at.*"

# Check if "i reg" shows the PA register values
gdb_test "i reg" ".*flags.*rp/r2.*arg0/r26.*pcoqh.*pcsqh.*"

# Set the frame to the topmost frame (IA)
gdb_test "f 0" ".*0.*ia_func.*pa2pa2ia.ialib.c.*"

# Info frame on the IA frame to see the IA frame related info
gdb_test "i frame" ".*Stack level 0.*ip =.*in ia_func.*pa2pa2ia.ialib.c.*saved pcoqh.*Size of rotating.*Previous frame.*sp.*"

# Check if "i reg" shows the IA register values
gdb_test "i reg" ".*pr0.*gr1.*bsp.*bspst.*sor.*sol.*"

# Set the frame to a different PA frame -- 'foo'
gdb_test "f 2" ".*2.*foo.*libpa2pa2ia64.*" 

# Info frame on the PA frame to see PA frame related info
gdb_test "i frame" ".*Stack level 2.*PA frame at.*pcoqh.*in foo.*saved ip.*called by frame.*Arglist at.*Locals at.*Previous.*sp.*Saved registers.*rp.*r3.*" 

# Check if "i reg" shows the PA register values
gdb_test "i reg" ".*flags.*rp/r2.*arg0/r26.*pcoqh.*pcsqh.*"

# Set the frame to the bottommost IPF frame 
gdb_test "f 3" ".*3.*in main.*pa2pa2ia.ia.c.*"

# Info frame of the IPF frame
gdb_test "i frame" ".*Stack level 3, frame at.*ip.*main.*pa2pa2ia.ia.c.*saved ip.*Size of frame.*Size of locals.*"

# Check if 'down' works and we are able to go to the PA frame.
gdb_test "down" ".*2.*foo.*from.*libpa2pa2ia64.so.*" 

# Check if 'down' works and we are able to go to the other PA frame.
gdb_test "down" ".*1.*goo.*from.*libpa2pa2ia64.so.*"

# Check if 'bt n' works
gdb_test "bt 2" ".*ia_func.*pa2pa2ia.ialib.c.*goo.*libpa2pa2ia64.*More stack frames.*"

gdb_test "bt -2" ".*foo.*libpa2pa2ia64.*main.*"

# Change settings to display the aries frames
gdb_test "set debug-aries on" ".*Please reload the corefile.*"

# Reload the core file 
gdb_test "core ${objdir}/${subdir}/core.pa2pa2ia64" ".*was generated by.*pa2pa2ia64.*Segmentation fault.*ia_func.*pa2pa2ia.ialib.c.*"

# Check if the backtrace displays the aries and other IPF frames
gdb_test "bt" ".*ia_func.*aries_pa2ia_process.*aries_ia2pa_call_process.*aries_ia2pa_process.*main.*"

# A 'bt -1' should display the 'main' routine
gdb_test "bt -1" ".*main.*pa2pa2ia.ia.c.*"

# Switch back to debug-aries off mode and see the frames
# to check if toggling works

gdb_test "set debug-aries off" ".*Please reload the corefile.*"

# Reload the core file
gdb_test "core ${objdir}/${subdir}/core.pa2pa2ia64" ".*pa2pa2ia64.*ia_func.*pa2pa2ia.ialib.*"

# Recheck the backtrace
gdb_test "where" ".*0.*ia_func.*pa2pa2ia.ialib.*1.*goo.*foo.*libpa2pa2ia64.*3.*main.*pa2pa2ia.ia.c.*"

gdb_exit

# Cleanup -- Remove the files which have been copied

catch "exec rm -f ${objdir}/${subdir}/pa_2_pa_2_ia.z"
catch "exec rm -f ./libpa2pa2ia64.sl ./{dldlib} pa2pa2ia.ialib.o pa2pa2ia.ia.o"
catch "exec rm -f pa2pa2ia.pa.c pa2pa2ia.ia.c pa2pa2ia.ialib.c pa2pa.ia.o libpa2pa2ia64.so libpa2pa2ia64.ia.so libpa2pa2ia64.so.ia"
}

# CHECK 32 BIT
set testfile "pa2pa2ia32"
set binfile ${objdir}/${subdir}/${testfile}
set dldlib "dmm.so"

# Use the dld in the source directory.
catch "exec /bin/cp -f ${srcdir}/${subdir}/${dldlib} ."
catch "exec chmod +x ${dldlib}"
catch "exec /bin/cp -f ${srcdir}/${subdir}/${dldlib} ${objdir}/${subdir}/"

# Use the libaries.so in the current directory.
set env(PX_LIBARIES) "${srcdir}/${subdir}/libaries32.so"
catch "exec cp -f ${srcdir}/${subdir}/pa_2_pa_2_ia32.z ${objdir}/${subdir}/"
catch "exec cp -f ${srcdir}/${subdir}/libpa2pa2ia32.sl ."
catch "exec cp -f ${srcdir}/${subdir}/libpa2pa2ia32.so__wp.so ."
catch "exec cp -f ${srcdir}/${subdir}/libpa2pa2ia32.so__wp_pa2ia.so ."
catch "exec chmod +x ${objdir}/${subdir}/pa_2_pa_2_ia32.z"
# Create the required source files, Compile the test binary and
# Create the required links
catch "exec ${objdir}/${subdir}/pa_2_pa_2_ia32.z"
# Run the test binary -- it should produce a core
catch "exec ./pa2pa2ia32"
# Rename the core file
catch "exec mv ./core ./core.pa2pa2ia32"
catch "exec mv ./pa2pa2ia32 ./core.pa2pa2ia32 ${objdir}/${subdir}/"

set env(GDB_ARIES_LIB) "${srcdir}/${subdir}/libaries64.so"
set env(GDB_SHLIB_PATH) "${srcdir}/${subdir}:."


gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

# Test if the topmost IA frame is displayed by default when the core is loaded
gdb_test "core ${objdir}/${subdir}/core.pa2pa2ia32" ".*pa2pa2ia32.*ia_func.*pa2pa2ia32\.ialib\.c.*"

# Test the backtrace -- An IA frame followed by 2 PA frames followed by 
# the 'main' IA frame should get displayed.
gdb_test "where" ".*0.*ia_func.*pa2pa2ia32.ialib.c.*1.*goo+.*libpa2pa2ia32.*2.*foo+.*libpa2pa2ia32.*3.*main.*pa2pa2ia32.ia.c.*" 

# Set the frame to the transition PA frame -- 'goo'
gdb_test "f 1" ".*1.*goo.*libpa2pa2ia32.*" 

# Info frame on the PA frame to see PA frame related info
gdb_test "i frame" ".*Stack level 1, PA frame at.*pcoqh.*in goo.*saved pcoqh.*called by frame.*Arglist at.*Locals at.*Previous .*sp.*Saved registers:.*rp at.*"

# Check if "i reg" shows the PA register values
gdb_test "i reg" ".*flags.*rp/r2.*arg0/r26.*pcoqh.*pcsqh.*"

# Set the frame to the topmost frame (IA)
gdb_test "f 0" ".*0.*ia_func.*pa2pa2ia32.ialib.c.*"

# Info frame on the IA frame to see the IA frame related info
gdb_test "i frame" ".*Stack level 0.*ip =.*in ia_func.*pa2pa2ia32.ialib.c.*saved pcoqh.*Size of rotating.*Previous frame.*sp.*"

# Check if "i reg" shows the IA register values
gdb_test "i reg" ".*pr0.*gr1.*bsp.*bspst.*sor.*sol.*"

# Set the frame to a different PA frame -- 'foo'
gdb_test "f 2" ".*2.*foo.*libpa2pa2ia32.*" 

# Info frame on the PA frame to see PA frame related info
gdb_test "i frame" ".*Stack level 2, PA frame at.*pcoqh.*in foo.*saved ip.*called by frame.*Arglist at.*Locals at.*Previous .*sp.*Saved registers:.*rp at.*" 

# Check if "i reg" shows the PA register values
gdb_test "i reg" ".*flags.*rp/r2.*arg0/r26.*pcoqh.*pcsqh.*"

# Set the frame to the bottommost IPF frame 
gdb_test "f 3" ".*3.*in main.*pa2pa2ia32.ia.c.*"

# Info frame of the IPF frame
gdb_test "i frame" ".*Stack level 3, frame at.*ip.*main.*pa2pa2ia32.ia.c.*saved ip.*Size of frame.*Size of locals.*"

# Check if 'down' works and we are able to go to the PA frame.
gdb_test "down" ".*2.*foo.*from.*libpa2pa2ia32.so.*" 

# Check if 'down' works and we are able to go to the other PA frame.
gdb_test "down" ".*1.*goo.*from.*libpa2pa2ia32.so.*"

# Check if 'bt n' works
gdb_test "bt 2" ".*ia_func.*pa2pa2ia32.ialib.c.*goo.*libpa2pa2ia32.*More stack frames.*"

gdb_test "bt -2" ".*foo.*libpa2pa2ia32.*main.*"

# Change settings to display the aries frames
gdb_test "set debug-aries on" ".*Please reload the corefile.*"

# Reload the core file 
gdb_test "core ${objdir}/${subdir}/core.pa2pa2ia32" ".*was generated by.*pa2pa2ia32.*Segmentation fault.*ia_func.*pa2pa2ia32.ialib.c.*"

# Check if the backtrace displays the aries and other IPF frames
gdb_test "bt" ".*ia_func.*aries_pa2ia_process.*aries_ia2pa_call_process.*aries_ia2pa_process.*main.*"

# A 'bt -1' should display the 'main' routine
gdb_test "bt -1" ".*main.*pa2pa2ia32.ia.c.*"

# Switch back to debug-aries off mode and see the frames
# to check if toggling works

gdb_test "set debug-aries off" ".*Please reload the corefile.*"

# Reload the core file
gdb_test "core ${objdir}/${subdir}/core.pa2pa2ia32" ".*pa2pa2ia32.*ia_func.*pa2pa2ia32.ialib.*"

# Recheck the backtrace
gdb_test "where" ".*0.*ia_func.*pa2pa2ia32.ialib.*1.*goo.*foo.*libpa2pa2ia32.*3.*main.*pa2pa2ia32.ia.c.*"

gdb_exit

# Cleanup -- Remove the files which have been copied
catch "exec rm -f ${objdir}/${subdir}/pa_2_pa_2_ia32.z"
catch "exec rm -f ./libpa2pa2ia32.sl ./{dldlib} pa2pa2ia32.ialib.o pa2pa2ia32.ia.o"
catch "exec rm -f pa2pa2ia32.pa.c pa2pa2ia32.ia.c pa2pa2ia32.ialib.c pa2pa32.ia.o libpa2pa2ia32.so libpa2pa2ia32.ia.so libpa2pa2ia32.so.ia"
catch "exec rm -f pa2pa2ia32_header_export.pa.h pa2pa2ia32_header.pa.h"
catch "exec rm -f libpa2pa2ia32.so__wp.so libpa2pa2ia32.so__wp_pa2ia.so"
catch "exec rm -f ${objdir}/${subdir}/core.pa2pa2ia32"

return 0
