#include <stdio.h>
#include <dlfcn.h>
#include <pa2padlsym_header.pa1.h>
void *dl2;

int pa_func() {
  float flo1 = 634623.6347623;
  double flo2 = flo1 * 2.89;
  dl2 = dlopen("libpadlsym64.2.so", RTLD_LAZY); 
  printf ("dl2 %p\n", dl2);
  int (*f2)() = (int (*)())dlsym(dl2, "pa2");
  int i = (*f2)();
  printf ("pa2() = %d\n", i);
  return i; 
}

int roo() {
  int i = ia_func();
  return i;
}

int goo() {
  int *arr1 = 0;
  return roo();
}

int foo () {
  int arr[8];
  int i;

  for (i=0; i < 8; i++)
  {
    arr[i] = i;
  }
  i = goo();
  return i;
}
