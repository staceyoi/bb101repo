#include <stdio.h>
#include <dlfcn.h>
#include "pa2padlsym32_header.pa1.h"
void *dl2;

int pa_func() {
  printf("before dlopen\n");
  dl2 = dlopen("./libpa2padlsym32.2.so", RTLD_LAZY); 
  printf("after dlopen \n");
  printf ("dl2 = %p\n", dl2);
  int (*f2)() = (int (*)())dlsym(dl2, "pa2");
  printf ("f2 %p\n", f2);
  printf ("f2 %p\n", f2);
  int i = (*f2)();
  printf ("pa2() = %d\n", i);
  return i; 
}

int roo() {
  printf("roo\n");
  int i = ia_func();
  return 100;
}

int goo() {
  int *arr1 = 0;
  printf("goo\n");
  return roo();
}

int foo () {
  int arr[8];
  int i;

  for (i=0; i < 8; i++)
  {
    arr[i] = i;
  }
  printf("foo\n");
  i = goo();
  return i;
}
