#include <stdio.h>
int roo() {
  int *arr1 = 0;
  *arr1 = 99;
  printf("roo\n");
  return 0;
}

int goo() {
#if 0
  int *arr1 = 0;
  roo();
  printf("goo-2\n");
  printf("goo-1\n");
  roo();
  printf("goo-2\n");
  return 1405;
#endif
  return roo();
}

int pa_foo2 (int x, int y) {
   printf("foo-2 Spin for long\n");
#if 0
   while (1) {
     x += y;
     if (x == 0xffffffff)
       break;
   }
#endif
   return x;
}

int pa_foo1 (int jj, int yy) {
#if 0
  int arr[100];
  int i;
  for (i=0; i < 100; i++)
  {
    arr[i] = (i + yy/jj);
  }
  i = goo();
  return i;
#endif
  return goo();
}
