# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Test basic functionality of enhanced support for mutexes
#

if { [skip_hp_tests] } { continue }

set testfile enh_thr_mutex1
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc32.sl"
  }
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug "additional_flags=-Ae -Wl,+s" "ldflags=-lpthread"}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "shell uname -r\n"
gdb_expect {
  -re ".*11.23.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*11.31.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*$gdb_prompt $" { set 1123_kern 0 }
  timeout {fail "(timeout) uname"}
}

# Need to make sure we're running on 11.23 and above
if { $1123_kern != 1 } {
  gdb_exit
  return 0
}

gdb_test "b foo" ".*Breakpoint 1.*" "b foo"
gdb_test "b bar" ".*Breakpoint 2.*" "b bar"
gdb_test "set thread-check on" "" "set thread-check"

send_gdb "run\n"
gdb_expect {
  -re ".*Breakpoint 1, foo.*$gdb_prompt $" { pass "run to foo" }
  timeout { fail "(timeout) run program, hit main breakpoint" }
}

set mtx1_idx -1
set mtx2_idx -1
set mtx3_idx -1

# changed the -re pattern below to be more flexible.  It's necessary after adding in libCsup
# to librtc.  Adding libCsup added more mutex ID.  It went up to 
# double digit.  The search pattern before didn't work.  The main changes
# here are \r\n and \[ \]+ for 1 or more blank.  It's important not
# to do .*
send_gdb "info mutex\n"
gdb_expect {
  -re "\r\n($decimal)\[ \]+$decimal\[ \]+0\[ \]+($hex)\[ \]+mtx1.*$gdb_prompt $" { 
      set mtx1_idx $expect_out(1,string)
      pass "info mutex 1" 
   }
  default { fail "info mutex 1"   }
  timeout { fail "(timeout) info mutex 1" }
}

gdb_test "info mutex $mtx1_idx" "Mutex ID: \t\t$decimal\[ \]*\r\nVariable address: \t$hex\[ \]*\r\nVariable name: \t\tmtx1\r\nProcess shared: \ty\r\nType: \t\t\tnormal\r\nProtocol: \t\tunknown\r\nBlocked count: \t\t0\r\nTrylock failures: \t0\r\nLock count: \t\t1\r\nContended locks: \t0.*Created by:           thread 1.*Owned by:             thread (2|4).*" "info mutex 2"

send_gdb "info mutex\n"
gdb_expect {
  -re "\r\n($decimal)\[ \]+$decimal\[ \]+0\[ \]+($hex)\[ \]+mtx2.*$gdb_prompt $" { 
      set mtx2_idx $expect_out(1,string)
      pass "info mutex 3" 
   }
  default { fail "info mutex 3"   }
  timeout { fail "(timeout) info mutex 3" }
}

gdb_test "info mutex $mtx2_idx" "Mutex ID: \t\t$decimal\[ \]*\r\nVariable address: \t$hex\[ \]*\r\nVariable name: \t\tmtx2\r\nProcess shared: \tn\r\nType: \t\t\tnormal\r\nProtocol: \t\tunknown\r\nBlocked count: \t\t0\r\nTrylock failures: \t0\r\nLock count: \t\t1\r\nContended locks: \t0.*Created by:           thread 1.*Owned by:             thread (2|4).*" "info mutex 4"

send_gdb "info mutex\n"
gdb_expect {
  -re "\r\n($decimal)\[ \]+$decimal\[ \]+0\[ \]+$hex\[ \]+mtx3.*$gdb_prompt $" { 
      set mtx3_idx $expect_out(1,string)
      pass "info mutex 5" 
   }
  default { fail "info mutex 5"   }
  timeout { fail "(timeout) info mutex 5" }
}

gdb_test "info mutex $mtx3_idx" "Mutex ID: \t\t$decimal\[ \]*\r\nVariable address: \t$hex\[ \]*\r\nVariable name: \t\tmtx3\r\nProcess shared: \tn\r\nType: \t\t\tnormal\r\nProtocol: \t\tunknown\r\nBlocked count: \t\t0\r\nTrylock failures: \t0\r\nLock count: \t\t1\r\nContended locks: \t0.*Created by:           thread (2|4).*Owned by:             thread (2|4).*" "info mutex 6"

send_gdb "c\n"
gdb_expect {
  -re ".*Breakpoint 1, foo.*$gdb_prompt $" { pass "continue to foo" }
  timeout { fail "(timeout) continue to foo" }
}

send_gdb "info mutex\n"
gdb_expect {
  -re "\r\n($decimal)\[ \]+$decimal\[ \]+0\[ \]+$hex\[ \]+mtx1.*$gdb_prompt $" { 
      set mtx1_idx $expect_out(1,string)
      pass "info mutex 7" 
   }
  default { fail "info mutex 7"   }
  timeout { fail "(timeout) info mutex 7" }
}

gdb_test "info mutex $mtx1_idx" "Mutex ID: \t\t$decimal\[ \]*\r\nVariable address: \t$hex\[ \]*\r\nVariable name: \t\tmtx1\r\nProcess shared: \ty\r\nType: \t\t\tnormal\r\nProtocol: \t\tunknown\r\nBlocked count: \t\t0\r\nTrylock failures: \t0\r\nLock count: \t\t2\r\nContended locks: \t0.*Created by:           thread 1.*Owned by:             thread (3|5).*" "info mutex 8"

send_gdb "c\n"
gdb_expect {
  -re ".*Breakpoint 2, bar.*$gdb_prompt $" { pass "continue to bar" }
  timeout { fail "(timeout) continue to bar" }
}

send_gdb "info mutex\n"
gdb_expect {
  -re "\r\n($decimal)\[ \]+none\[ \]+0\[ \]+$hex\[ \]+mtx1.*$gdb_prompt $" { 
      set mtx1_idx $expect_out(1,string)
      pass "info mutex 9" 
   }
  default { fail "info mutex 9"   }
  timeout { fail "(timeout) info mutex 9" }
}

send_gdb "info mutex $mtx1_idx\n"
gdb_expect {
  -re "Mutex ID: \t\t$decimal\[ \]*\r\nVariable address: \t$hex\[ \]*\r\nVariable name: \t\tmtx1\r\nProcess shared: \ty\r\nType: \t\t\tnormal\r\nProtocol: \t\tunknown\r\nBlocked count: \t\t0\r\nTrylock failures: \t0\r\nLock count: \t\t2\r\nContended locks: \t0.*Created by:           thread 1.*Owned by:             thread.*$gdb_prompt $" { 
      fail "info mutex 10" 
   }
  -re "Mutex ID: \t\t$decimal\[ \]*\r\nVariable address: \t$hex\[ \]*\r\nVariable name: \t\tmtx1\r\nProcess shared: \ty\r\nType: \t\t\tnormal\r\nProtocol: \t\tunknown\r\nBlocked count: \t\t0\r\nTrylock failures: \t0\r\nLock count: \t\t2\r\nContended locks: \t0.*Created by:           thread 1.*$gdb_prompt $" { 
      pass "info mutex 10" 
   }
  default { fail "info mutex 10"   }
  timeout { fail "(timeout) info mutex 10" }
}

gdb_exit
return 0
