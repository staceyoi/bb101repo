# Copyright (C) 2006 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#

#Test the rule to stop gdb when application attempt to exit thread while holding a mutex 
# and for thread exit with no join and detach.

if { [skip_hp_tests] } { continue }

set testfile missingUnlock 
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set pid [pid]
set outdir /tmp/thr_batch_$pid
set out ${outdir}/out
set dir .

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc32.sl"
  }
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug "addit
ional_flags=-Ae -Wl,+s" "ldflags=-lpthread"}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will autom
atically fail."
}

if { [istarget "hppa1*-hp-hpux*"] } {
exec chatr +s enable ${binfile}
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "shell uname -r\n"
gdb_expect {
  -re ".*11.23.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*11.31.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*$gdb_prompt $" { set 1123_kern 0 }
  timeout {fail "(timeout) uname"}
}

# Need to make sure we're running on 11.23 and above
if { $1123_kern != 1 } {
  gdb_exit
  return 0
}

gdb_test "set thread-check thread-exit-own-mutex on" "" "set thread-check"
gdb_test "b main" ".*Breakpoint 1.*" "putting a breakpoint at main"
gdb_test "run" ".*Breakpoint 1, main.*:100.*" "Breakpoint at main"
gdb_test "c" ".*Continuing.*warning:.*" "thread-exit-own-mutex"
gdb_test "c" ".*Continuing.*warning:.*" "thread-exit-own-mutex"
gdb_test "c" ".*Continuing.*warning:.*" "thread-exit-own-mutex"
gdb_test "c" ".*Continuing.*warning:.*" "thread-exit-own-mutex"
gdb_test "c" ".*Continuing.*Program exited normally.*" ""
gdb_exit

# Do the batch mode check

catch "exec chatr +dbg enable ${binfile}"
system "/usr/bin/rm -f ${dir}/rtcconfig"
system "/usr/bin/rm -f ${testfile}.*.threads"
system "echo 'set thread-check thread-exit-own-mutex on' >> ${dir}/rtcconfig"
system "echo 'set thread-check thread-exit-no-join-detach on' >> ${dir}/rtcconfig"
system "echo 'set frame-count 5' >> ${dir}/rtcconfig"
system "echo 'files=missingUnlock' >> ${dir}/rtcconfig"
set oldtimeout $timeout
set timeout 120

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/hpux32"
  } else {
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/hpux64"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    # Batch mode thread check not supported for PA64 on 11.23
    catch "exec uname -r | grep \"11.23\" > out"
    catch "exec /usr/bin/cat out | wc -c" output1
    if { $output1 } {
      gdb_exit
      return 0
    }
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/pa20_64"
  } else {
    return 0
  }
}

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LD_PRELOAD) "${objdir}/../librtc.sl"
  } else {
    set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
  } else {
    #set env(LD_PRELOAD) "${objdir}/../librtc32.sl"
  }
}


if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}


set env(BATCH_RTC) "on"
spawn "${binfile}"
expect {
  timeout {
    fail "The app run timed out."
    set app_pid [exp_pid]
    system "kill -9 $app_pid"
    set timeout $oldtimeout
    return 0
  }
  eof { pass "The run passed." }
}

set env(BATCH_RTC) "off"
set env(LD_PRELOAD) ""

catch "exec mkdir ${outdir}"
set thrfile ${outdir}/threads_file
system "mv ${testfile}.*.threads ${thrfile}"
catch "exec fgrep \"THREAD ERROR EVENT: Attempt to exit thread\" ${thrfile} | grep \"while
holding a mutex\"  > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Error event obtained about thread exit own mutex"
} else {
  fail "Error event not obtained"
}

catch "exec fgrep \"Detailed information on mutex\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Mutex info printed"
} else {
  fail "Mutex info not printed"
}

catch "exec fgrep \"Thread debugging\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Backtrace printed"
} else {
  fail "Backtrace not printed"
}

catch "exec fgrep \"THREAD ERROR EVENT: Attempt to exit thread\" ${thrfile} | grep \"which has neither
been joined nor detached\"  > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Error event obtained about thread exit no join-detach"
} else {
  fail "Error event not obtained"
}
catch "exec rm -rf ${outdir}"
catch "exec rm -f ${dir}/rtcconfig"

set timeout $oldtimeout
return 0

