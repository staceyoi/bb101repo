#include <pthread.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

pthread_mutex_t	job_lock1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t	job_lock2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t	job_cv = PTHREAD_COND_INITIALIZER;	
extern void		fatal_error(int err, char *f);

void
producer_thread(pthread_mutex_t* job_lock)
{
	int	ret_val;

        /* Acquire the associated mutex lock */
        if ((ret_val = pthread_mutex_lock(job_lock)) != 0)
          fatal_error(ret_val, "p mtx_lock failed");

        /* Signal the condvar to wakeup one thread */
        if ((ret_val = pthread_cond_signal(&job_cv)) != 0)
          fatal_error(ret_val, "cond_signal failed");

        /* Release the associated mutex */
        if ((ret_val = pthread_mutex_unlock(job_lock)) != 0)
          fatal_error(ret_val, "mtx_unlock failed");
}


void
consumer_thread(pthread_mutex_t* job_lock)
{
	int		ret_val;
        pthread_cond_wait(&job_cv, job_lock);
}


#define check_error(return_val, msg) {			\
		if (return_val != 0) 			\
			fatal_error(return_val, msg);	\
	}

main()
{
	pthread_t	tid1, tid2;
	int		ret_val;

        alarm (20);

	/* Create two threads to do the work */
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())consumer_thread, (void *) &job_lock1);
	check_error(ret_val, "pthread_create 1 failed");

	ret_val = pthread_create(&tid2, (pthread_attr_t *)NULL,
		(void *(*)())producer_thread, (void *) &job_lock1);
	check_error(ret_val, "pthread_create 2 failed");

        printf (" in main thread after creating 2 threads \n");

        /* Lets send a signal to overselves to see if GDB handles it
           correctly */
        fflush(stdout);
        fflush(stderr);
        sleep(1);
        kill(getpid(), 2);

}

void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = (char *)strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
