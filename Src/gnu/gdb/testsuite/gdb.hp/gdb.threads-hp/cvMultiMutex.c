#include <pthread.h>
#include "errors.h"

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int value=0;

void *cv_wait_thread (void *arg)
{
  int status;
  status = pthread_mutex_lock(&mutex1);
  if (status!=0)
    err_abort (status, "Lock mutex1");

  status = pthread_cond_wait(&cond, &mutex1);
  if (status!=0)
    err_abort (status, "Cond wait");

  printf("First pthread_cond_wait go thru..\n");
  printf("value=%d\n", value);

  status = pthread_mutex_unlock(&mutex1);
  if (status!=0)
    err_abort (status, "Unlock mutex1");
  return NULL;
}

void *wait_thread (void *arg)
{
  int status;

  sleep(2);
  status = pthread_mutex_lock(&mutex1);
  if (status!=0)
    err_abort (status, "Lock mutex1");
  value=100;
  status = pthread_cond_signal(&cond);
  if (status!=0)
    err_abort (status, "Cond signal");
  status = pthread_mutex_unlock(&mutex1);
  if (status!=0)
    err_abort (status, "Unlock mutex1");

  sleep(2);

  status = pthread_mutex_lock(&mutex2);
  if (status!=0)
    err_abort (status, "Lock mutex2");
  value=200;
  status = pthread_cond_signal(&cond);
if (status!=0)
    err_abort (status, "Cond signal");
  status = pthread_mutex_unlock(&mutex2);
  if (status!=0)
    err_abort (status, "Unlock mutex2");

 return NULL;
}

int main (int argc, char *argv[])
{

  int status;
  pthread_t wait_thread_id;
  pthread_t other_cv_thread_id;

  /* create another thread to wait on a condition variable */
  status = pthread_create(&other_cv_thread_id, NULL, cv_wait_thread, NULL);
  if (status!=0)
    err_abort (status, "Create cv thread");

  status = pthread_create(&wait_thread_id, NULL, wait_thread, NULL);
  if (status!=0)
    err_abort (status, "Create wait thread");

  /* join with the cv thread so we make sure not to get EINVAL on */
  /* our second condition variable wait */
  status = pthread_join(other_cv_thread_id, 0);
  if (status!=0)
     err_abort (status, "Join cv thread");

  //reset value
  value=0;

  //using the same cond variable on a second mutex
  status = pthread_mutex_lock(&mutex2);
  if (status!=0)
    err_abort (status, "Lock mutex2");

  status = pthread_cond_wait(&cond, &mutex2);
  if (status!=0)
    err_abort (status, "Cond wait");

  printf("Second pthread_cond_wait go thru..\n");
  printf("value=%d\n", value);

  status = pthread_mutex_unlock(&mutex2);
  if (status!=0)
    err_abort (status, "Unlock mutex2");

  return 0;
}
