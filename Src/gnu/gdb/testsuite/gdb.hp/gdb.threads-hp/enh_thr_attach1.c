#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

extern void	fatal_error(int err_num, char *function); 

#define check_error(return_val, msg) {   		\
		if (return_val != 0)  			\
			fatal_error(return_val, msg);	\
	}

pthread_mutex_t	my_mtx = PTHREAD_MUTEX_INITIALIZER;

void foo() {}
void bar() {}
void zoo() {}

void thread_default(int arg)
{
  int ret_val;
  ret_val = pthread_mutex_lock(&my_mtx);
  foo();
  ret_val = pthread_mutex_unlock(&my_mtx);
}

volatile int wait_here = 1;

main()
{
	pthread_t		tid1, tid2;
	pthread_attr_t		attr;
	int		ret_val, oldstate;;

        (void) alarm(300);
        while (wait_here);

	/* Create a thread with default attributes */
        ret_val = pthread_mutex_lock(&my_mtx);
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())thread_default, (void*) 0x55);
	check_error(ret_val, "pthread_create 1 failed");
        ret_val = pthread_mutex_unlock(&my_mtx);

        /* Wait for thread to finish executing  */
        ret_val = pthread_join(tid1, (void **)NULL);
        check_error(ret_val, "pthread_join()");
}

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
