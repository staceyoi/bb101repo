#include <pthread.h>
#include <stdlib.h>
#include <errno.h>

struct job_req {
	struct job_req	*next;
	struct job_req	*prev;
	int		num;
	int		terminate;
};

pthread_mutex_t	job_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t	job_cv = PTHREAD_COND_INITIALIZER;	
int		job_count = 0;

extern struct job_req	*job_dequeue();
extern struct job_req	*create_job();
extern void		process_job(struct job_req *ptr);
extern void		job_enqueue(struct job_req *ptr); 

extern void		fatal_error(int err, char *f);

void
producer_thread()
{
	struct job_req	*curr_job;
	int	ret_val;

	for ( ; ; ) {
		/* Create the next job request */
		if ((curr_job = create_job()) == NULL)
			pthread_exit((void *)NULL);

		/* Acquire the associated mutex lock */
		if ((ret_val = pthread_mutex_lock(&job_lock)) != 0)
			fatal_error(ret_val, "p mtx_lock failed");

		/* Put the job_request on the queue */
		job_enqueue(curr_job);
		job_count++;

		/* Signal the condvar to wakeup one thread */
		if ((ret_val = pthread_cond_signal(&job_cv)) != 0)
			fatal_error(ret_val, "cond_signal failed");

		/* Release the associated mutex */
		if ((ret_val = pthread_mutex_unlock(&job_lock)) != 0)
			fatal_error(ret_val, "mtx_unlock failed");
	}
}


void
consumer_thread()
{
	struct job_req	*curr_job;
	int		ret_val;

	for ( ; ; ) {
		/* Acquire the condvar's associated mutex lock */

		if ((ret_val = pthread_mutex_lock(&job_lock)) != 0)
			fatal_error(ret_val, "c mtx_lock failed");

		/* 
		 * Wait for a job request. This test is the condition
		 * variable predicate. If no jobs are available,
		 * wait for one. The condition wait releases the
		 * mutex so that another thread can place jobs on the
		 * queue. The mutex is reacquired before returning
		 * from the condition wait.
		 */
		while (job_count == 0)
			pthread_cond_wait(&job_cv, &job_lock);

		/* 
		 * Get the job_request 
		 */
		curr_job = job_dequeue();
		if (curr_job != (struct job_req *)NULL)
			job_count--;

		/* 
		 * Release the associated mutex 
		 */
		if ((ret_val = pthread_mutex_unlock(&job_lock)) != 0)
			fatal_error(ret_val, "mtx_unlock failed");

		/* 
		 * Process the job request 
		 */
		if (curr_job != (struct job_req *)NULL)
			process_job(curr_job);
	}
}


struct job_req *
create_job()
{
	static int	i = 0;
	struct job_req	*ptr;

	i++;
	if (i > 10)
		return((struct job_req *)NULL);
	else if (i > 7)
		sleep(1);

	ptr = (struct job_req *)malloc(sizeof(struct job_req));
	if (ptr == NULL)
		fatal_error(ENOMEM, "malloc()");

	ptr->num = i;
	if (ptr->num >= 10)
		ptr->terminate = 1;
	else
		ptr->terminate = 0;
	printf("Creating job %d\n", ptr->num);
	return(ptr);
}


void
process_job(struct job_req *ptr)
{
	printf("Processing job %d\n", ptr->num);
	free((void *)ptr);
	if (ptr->terminate)
		pthread_exit((void *)NULL);
	if (ptr->num < 3)
		sleep(1);
}



#define check_error(return_val, msg) {			\
		if (return_val != 0) 			\
			fatal_error(return_val, msg);	\
	}

main()
{
	pthread_t	tid1, tid2;
	int		ret_val;

	/*
	 * Create two threads to do the work
	 */
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())consumer_thread, (void *)NULL);
	check_error(ret_val, "pthread_create 1 failed");

        /* wait till first thread is created */
        for (int i=0; i<100000000; i++); sleep(1);

	ret_val = pthread_create(&tid2, (pthread_attr_t *)NULL,
		(void *(*)())producer_thread, (void *)NULL);
	check_error(ret_val, "pthread_create 2 failed");

	/*
	 * Wait for the threads to finish
	 */
	ret_val = pthread_join(tid1, (void **)NULL);
	check_error(ret_val, "pthread_join: tid1");

	ret_val = pthread_join(tid2, (void **)NULL);
	check_error(ret_val, "pthread_join: tid2");

	exit(0);
}

struct job_req *qptr = NULL;

void
job_enqueue(struct job_req *task)
{
        struct job_req *curr;

        task->next = task->prev = NULL;

        if (qptr == NULL) {
                qptr = task;
                return;
        }

        for (curr = qptr; curr->next != NULL; curr = curr->next)
                ;

        curr->next = task;
        task->prev = curr;
}

struct job_req *
job_dequeue()
{
        struct job_req *ptr;

        if (qptr == NULL)
                exit(-1);

        ptr = qptr;
        qptr = ptr->next;
        return(ptr);
}

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
