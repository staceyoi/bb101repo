#include <pthread.h>
#include <stdlib.h>
#include <errno.h>

pthread_mutex_t	job_lock1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t	job_lock2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t	job_cv = PTHREAD_COND_INITIALIZER;	
extern void		fatal_error(int err, char *f);

void
producer_thread(pthread_mutex_t* job_lock)
{
	int	ret_val;

        /* Acquire the associated mutex lock */
        if ((ret_val = pthread_mutex_lock(job_lock)) != 0)
          fatal_error(ret_val, "p mtx_lock failed");

        /* Signal the condvar to wakeup one thread */
        if ((ret_val = pthread_cond_signal(&job_cv)) != 0)
          fatal_error(ret_val, "cond_signal failed");

        /* Release the associated mutex */
        if ((ret_val = pthread_mutex_unlock(job_lock)) != 0)
          fatal_error(ret_val, "mtx_unlock failed");
}


void
consumer_thread(pthread_mutex_t* job_lock)
{
	int		ret_val;

        /* Acquire the condvar's associated mutex lock */
        if ((ret_val = pthread_mutex_lock(job_lock)) != 0)
          fatal_error(ret_val, "c mtx_lock failed");

        pthread_cond_wait(&job_cv, job_lock);

        /* Release the associated mutex */
        if ((ret_val = pthread_mutex_unlock(job_lock)) != 0)
          fatal_error(ret_val, "mtx_unlock failed");

}


#define check_error(return_val, msg) {			\
		if (return_val != 0) 			\
			fatal_error(return_val, msg);	\
	}

int main(int argc, char* argv[])
{
	pthread_t	tid1, tid2, tid3, tid4;
        pthread_mutex_t *l1, *l2;
	int		ret_val;

        if (argc == 1) {
          fprintf(stderr, "error: no arguments\n");
          exit (1);
        }
        else if (strcmp(argv[1], "bad") == 0) {
          l1 = &job_lock1;
          l2 = &job_lock2;
        }
        else {
          l1 = l2 = &job_lock1;
        }

        alarm(20);

	/* Create two threads to do the work */
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())consumer_thread, (void *) l1);
	check_error(ret_val, "pthread_create 1 failed");

	ret_val = pthread_create(&tid2, (pthread_attr_t *)NULL,
		(void *(*)())producer_thread, (void *) l1);
	check_error(ret_val, "pthread_create 2 failed");

        if (l1 != l2) {
          ret_val = pthread_create(&tid3, (pthread_attr_t *)NULL,
                                   (void *(*)())consumer_thread, (void *) l2);
          check_error(ret_val, "pthread_create 1 failed");

          ret_val = pthread_create(&tid4, (pthread_attr_t *)NULL,
                                   (void *(*)())producer_thread, (void *) l2);
          check_error(ret_val, "pthread_create 2 failed");
        }

	/* Wait for the threads to finish */
	ret_val = pthread_join(tid1, (void **)NULL);
	check_error(ret_val, "pthread_join: tid1");

	ret_val = pthread_join(tid2, (void **)NULL);
	check_error(ret_val, "pthread_join: tid2");

        if (l1 != l2) {
          ret_val = pthread_join(tid3, (void **)NULL);
          check_error(ret_val, "pthread_join: tid3");

          ret_val = pthread_join(tid4, (void **)NULL);
          check_error(ret_val, "pthread_join: tid4");
        }

	exit(0);
}

void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
