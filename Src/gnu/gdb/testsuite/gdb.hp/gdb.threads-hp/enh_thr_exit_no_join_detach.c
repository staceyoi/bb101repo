#include <pthread.h>
#include <stdlib.h>
#include <errno.h>

pthread_mutex_t	job_lock1 = PTHREAD_MUTEX_INITIALIZER;
extern void		fatal_error(int err, char *f);

void
my_thread(void* num)
{
	int		ret_val;

        /* Acquire the associated mutex lock */
        if ((ret_val = pthread_mutex_lock(&job_lock1)) != 0)
          fatal_error(ret_val, "p mtx_lock failed");

        printf ("In thread %d\n", (int) num);

        /* Release the associated mutex */
        if ((ret_val = pthread_mutex_unlock(&job_lock1)) != 0)
          fatal_error(ret_val, "mtx_unlock failed");
}


#define check_error(return_val, msg) {			\
		if (return_val != 0) 			\
			fatal_error(return_val, msg);	\
	}

main()
{
	pthread_t	tid1, tid2, tid3;
	int		ret_val;

	/* Create two threads to do the work */
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())my_thread, (void *)1);
	check_error(ret_val, "pthread_create 1 failed");

	ret_val = pthread_create(&tid2, (pthread_attr_t *)NULL,
		(void *(*)())my_thread, (void *)2);
	check_error(ret_val, "pthread_create 2 failed");

	ret_val = pthread_create(&tid3, (pthread_attr_t *)NULL,
		(void *(*)())my_thread, (void *)3);
	check_error(ret_val, "pthread_create 3 failed");

        /* Detach thread 1 */
	ret_val = pthread_detach(tid1);
	check_error(ret_val, "pthread_join: tid");

        sleep(5);

	/* Wait for the thread 2 to finishes */
	ret_val = pthread_join(tid2, (void **)NULL);
	check_error(ret_val, "pthread_join: tid");

	exit(0);
}

void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
