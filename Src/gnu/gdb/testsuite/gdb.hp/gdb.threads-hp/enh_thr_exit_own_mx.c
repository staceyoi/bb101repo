#include <pthread.h>
#include <stdlib.h>
#include <errno.h>

pthread_mutex_t	job_lock1 = PTHREAD_MUTEX_INITIALIZER;
extern void		fatal_error(int err, char *f);

void
producer_thread(pthread_mutex_t* job_lock)
{
	int	ret_val;

        /* Acquire the associated mutex lock */
        if ((ret_val = pthread_mutex_lock(job_lock)) != 0)
          fatal_error(ret_val, "p mtx_lock failed");
}


#define check_error(return_val, msg) {			\
		if (return_val != 0) 			\
			fatal_error(return_val, msg);	\
	}

main()
{
	pthread_t	tid;
	int		ret_val;

	/* Create two threads to do the work */
	ret_val = pthread_create(&tid, (pthread_attr_t *)NULL,
		(void *(*)())producer_thread, (void *) &job_lock1);
	check_error(ret_val, "pthread_create 2 failed");

	/* Wait for the threads to finish */
	ret_val = pthread_join(tid, (void **)NULL);
	check_error(ret_val, "pthread_join: tid");

	exit(0);
}

void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
