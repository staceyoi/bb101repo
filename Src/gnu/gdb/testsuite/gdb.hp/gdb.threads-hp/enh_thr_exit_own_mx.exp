# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#
# Test the rule to stop gdb when attempting to exit a thread which owns a mutex.
# If the rule is disabled gdb should run the app to completion without stoping
#
# use this to debug:
#log_user 1

if { [skip_hp_tests] } { continue }

set testfile enh_thr_exit_own_mx
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

set pid [pid]
set outdir /tmp/thr_batch_$pid
set out ${outdir}/out
set dir .

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc32.sl"
  }
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug "additional_flags=-Ae -Wl,+s" "ldflags=-lpthread"}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "shell uname -r\n"
gdb_expect {
  -re ".*11.23.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*11.31.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*$gdb_prompt $" { set 1123_kern 0 }
  timeout {fail "(timeout) uname"}
}

# Need to make sure we're running on 11.23 and above
if { $1123_kern != 1 } {
  gdb_exit
  return 0
}

gdb_test "set thread-check on" "" "set thread-check 1"
gdb_test "set thread-check thread-exit-own-mutex on" "" "set thread-check 2"
gdb_test "run" ".*warning: Attempt to exit thread $decimal while holding a mutex $decimal.*" "run 1"

gdb_test "set thread-check thread-exit-own-mutex off" "" "set thread-check 3"

send_gdb "run\n"
gdb_expect {
  -re ".*The program being debugged has been started already.*" { pass "run 2" }
  timeout { fail "(timeout) re-run program" }
}

send_gdb "y\n"
gdb_expect {
  -re ".*Program exited normally.*$gdb_prompt $" { pass "run 3" }
  timeout { fail "(timeout) normal exit" }
}

gdb_exit

# Do the batch mode check

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/hpux32"
  } else {
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/hpux64"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    # Batch mode thread check not supported for PA64 on 11.23
    catch "exec uname -r | grep \"11.23\" > out"
    catch "exec /usr/bin/cat out | wc -c" output1
    if { $output1 } {
      gdb_exit
      return 0
    }
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/pa20_64"
  } else {
    #set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib"
    return 0
  }
}

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LD_PRELOAD) "${objdir}/../librtc.sl"
  } else {
    set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
  } else {
    #set env(LD_PRELOAD) "${objdir}/../librtc32.sl"
  }
}

if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}

catch "exec chatr +dbg enable ${binfile}"
catch "exec rm -f ${dir}/rtcconfig"
system "rm -f ${testfile}.*.threads"
system "echo 'set thread-check on' >> ${dir}/rtcconfig"
system "echo 'set frame-count 5' >> ${dir}/rtcconfig"
system "echo 'files=enh_thr_exit_own_mx' >> ${dir}/rtcconfig"

set oldtimeout $timeout
set timeout 240

set env(BATCH_RTC) "on"
spawn "${binfile}"
expect {
  timeout {
    fail "The app run timed out."
    set app_pid [exp_pid]
    system "kill -9 $app_pid"
    set timeout $oldtimeout
    return 0
  }
  eof { pass "The run passed." }
}

set env(BATCH_RTC) "off"
set env(LD_PRELOAD) ""

catch "exec mkdir ${outdir}"
set thrfile ${outdir}/threads_file
system "mv ${testfile}.*.threads ${thrfile}"
catch "exec fgrep \"THREAD ERROR EVENT: Attempt to exit thread\" ${thrfile} | grep \"while holding a mutex\" > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Error event obtained"
} else {
  fail "Error event not obtained"
}

catch "exec fgrep \"Detailed information on thread\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Thread info printed"
} else {
  fail "Thread info not printed"
}

catch "exec fgrep \"Thread variable name: 		tid\" ${thrfile} > out"
if { $output1 } {
  pass "Thread variable name printed"
} else {
  fail "Thread variable name not printed"
}

catch "exec fgrep \"Start routine name\" ${thrfile} | grep \"producer_thread\" > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Thread start function printed"
} else {
  fail "Thread start function not printed"
}

catch "exec fgrep \"Detailed information on mutex\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Mutex info printed"
} else {
  fail "Mutex info not printed"
}

catch "exec fgrep \"job_lock1\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Mutex variable name printed"
} else {
  fail "Mutex variable name not printed"
}


catch "exec fgrep \"No stack frames\" ${thrfile} | grep \"since pthread_exit\" > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "No stack frame mesg printed"
} else {
  fail "No stack frame mesg not printed"
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${dir}/rtcconfig"

set timeout $oldtimeout
return 0
