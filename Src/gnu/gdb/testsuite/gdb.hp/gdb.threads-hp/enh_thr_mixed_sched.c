#include <pthread.h>
#include <errno.h>
#include <sched.h>
#include <stdio.h>

extern void	*thread1_func(), *thread2_func();
extern void	fatal_error(int err_num, char *func);

pthread_mutex_t         mtx = PTHREAD_MUTEX_INITIALIZER;

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}

#define check_error(return_val, msg) {			\
		if (return_val != 0)   			\
			fatal_error(return_val, msg);	\
	}

void *
thread1_func()
{
        int     ret_val;

        ret_val = pthread_mutex_lock(&mtx);
        check_error(ret_val, "mutex_lock mtx");
	printf("In thread1_func()\n");
        sleep(5);
        ret_val = pthread_mutex_unlock(&mtx);
        check_error(ret_val, "mutex_unlock mtx");

	return((void *)NULL);
}

void *
thread2_func()
{
        int     ret_val;

        ret_val = pthread_mutex_lock(&mtx);
        check_error(ret_val, "mutex_lock mtx");
	printf("In thread2_func()\n");
        sleep(5);
        ret_val = pthread_mutex_unlock(&mtx);
        check_error(ret_val, "mutex_unlock mtx");

	return((void *)NULL);
}

main()
{
	pthread_t	pth_id[2];
	int		ret_val, scope;
	int		old_policy;
	pthread_attr_t	attr;
	struct sched_param	param, old_param;

	/* Initialize the threads attributes object */
	ret_val = pthread_attr_init(&attr);
	check_error(ret_val, "attr_init()");

	/* We want bound threads if they are available. */
	ret_val = pthread_attr_getscope(&attr, &scope);
	check_error(ret_val, "attr_getscope()");
	if (scope != PTHREAD_SCOPE_SYSTEM) {
		scope = PTHREAD_SCOPE_SYSTEM;
		ret_val = pthread_attr_setscope(&attr, scope);
		if ((ret_val != 0) && (ret_val != ENOTSUP))
			fatal_error(ret_val, "attr_setscope()");
	}
	
	/* Thread 1 is a high priority SCHED_FIFO thread.*/
	ret_val = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	check_error(ret_val, "attr_setschedpolicy() 1");

	param.sched_priority = sched_get_priority_max(SCHED_FIFO);
	ret_val = pthread_attr_setschedparam(&attr, &param);
	check_error(ret_val, "attr_setschedparam() 1");

	ret_val = pthread_create(&pth_id[0], &attr, thread1_func, NULL);
	check_error(ret_val, "pthread_create() 1");
	
	/* Thread 2 is a low priority SCHED_RR thread. */
	ret_val = pthread_attr_setschedpolicy(&attr, SCHED_RR);
	check_error(ret_val, "attr_setschedpolicy() 2");

	param.sched_priority = sched_get_priority_min(SCHED_RR);
	ret_val = pthread_attr_setschedparam(&attr, &param);
	check_error(ret_val, "attr_setschedparam() 2");
	
	ret_val = pthread_create(&pth_id[1], &attr, thread2_func, NULL);
	check_error(ret_val, "pthread_create() 2");
	
	/* Destroy the thread attributes object */
	ret_val = pthread_attr_destroy(&attr);
	check_error(ret_val, "attr_destroy()");

	/* wait for the threads to finish */
	ret_val = pthread_join(pth_id[0], (void **)NULL);
	check_error(ret_val, "pthread_join() 1");

	ret_val = pthread_join(pth_id[1], (void **)NULL);
	check_error(ret_val, "pthread_join() 2");
}
