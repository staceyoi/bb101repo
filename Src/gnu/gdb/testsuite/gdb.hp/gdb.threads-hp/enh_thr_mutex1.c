#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <errno.h>

pthread_mutex_t mtx1;   /* global process shared mutex */
pthread_mutex_t	mtx2;
pthread_mutex_t	mtx3 = PTHREAD_MUTEX_INITIALIZER;

extern void	fatal_error(int err_num, char *func);

#define check_error(return_val, msg) {			\
		if (return_val != 0) 			\
			fatal_error(return_val, msg);	\
	}


void bar ()
{
}

main()
{
	pthread_mutexattr_t	mtx_attr1;
	pthread_t	tid1;
	pthread_t	tid2;
	int		ret_val;
	extern void	start_routine();

	/*
	 * Initialize the attributes for mtx1
	 */
	ret_val = pthread_mutexattr_init(&mtx_attr1);
	check_error(ret_val, "mutexattr_init failed");

	/*
	 * mtx1 is shared between multiple processes
	 */
	ret_val = pthread_mutexattr_setpshared(&mtx_attr1,
		PTHREAD_PROCESS_SHARED);
	check_error(ret_val, "mutexattr_setpshared failed");

	/*
	 * Initialize mtx1, then destroy the attributes
	 */
	ret_val = pthread_mutex_init(&mtx1, &mtx_attr1);
	check_error(ret_val, "mutex_init 1 failed");

	ret_val = pthread_mutexattr_destroy(&mtx_attr1);
	check_error(ret_val, "mutexattr_destroy failed");

	/*
	 * Initialize mtx2 with default attributes.
	 * mtx3 has already been statically initialized.
	 */
	ret_val = pthread_mutex_init(&mtx2, 
		(pthread_mutexattr_t *)NULL);
	check_error(ret_val, "mutex_init 2 failed");

	/*
	 * Create two threads to do the work
	 */
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())start_routine, NULL);
	check_error(ret_val, "pthread_create 1 failed");

        /* wait till first thread is created */
        for (int i=0; i<100000000; i++);

	ret_val = pthread_create(&tid2, (pthread_attr_t *)NULL,
		(void *(*)())start_routine, NULL);
	check_error(ret_val, "pthread_create 2 failed");

	/*
	 * Wait for the threads to finish
	 */
	ret_val = pthread_join(tid1, (void **)NULL);
	check_error(ret_val, "pthread_join: tid1");

	ret_val = pthread_join(tid2, (void **)NULL);
	check_error(ret_val, "pthread_join: tid2");

        bar();

	/*
	 * Destroy the mutexes
	 */
	ret_val = pthread_mutex_destroy(&mtx1);
	check_error(ret_val, "mutex_destroy mtx1");

	ret_val = pthread_mutex_destroy(&mtx2);
	check_error(ret_val, "mutex_destroy mtx2");

	ret_val = pthread_mutex_destroy(&mtx3);
	check_error(ret_val, "mutex_destroy mtx3");

	exit(0);
}

void foo ()
{
}

void
start_routine()
{
	int	ret_val;

	ret_val = pthread_mutex_lock(&mtx1);
	check_error(ret_val, "mutex_lock mtx1");

	ret_val = pthread_mutex_lock(&mtx2);
	check_error(ret_val, "mutex_lock mtx2");

	ret_val = pthread_mutex_lock(&mtx3);
	check_error(ret_val, "mutex_lock mtx3");

        foo();

	ret_val = pthread_mutex_unlock(&mtx3);
	check_error(ret_val, "mutex_unlock mtx3");

	ret_val = pthread_mutex_unlock(&mtx2);
	check_error(ret_val, "mutex_unlock mtx2");

	ret_val = pthread_mutex_unlock(&mtx1);
	check_error(ret_val, "mutex_unlock mtx1");
}

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
