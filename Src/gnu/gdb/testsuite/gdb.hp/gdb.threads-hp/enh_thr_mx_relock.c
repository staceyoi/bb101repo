#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

pthread_mutex_t r_mtx;   /* recursive mutex */
pthread_mutex_t n_mtx;   /* normal mutex */
extern void	fatal_error(int err, char *func);

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}

#define check_error(return_val, msg) {		\
	if (return_val != 0)				\
		fatal_error(return_val, msg);		\
}

main()
{
	pthread_mutexattr_t	mtx_attr;
	pthread_t		tid1;
	extern void		start_routine(int num);
	int			ret_val;

        alarm (20);

	/* Initialize the mutex attributes */
	ret_val = pthread_mutexattr_init(&mtx_attr);
	check_error(ret_val, "mutexattr_init failed");

	/* Set the type attribute to recursive */
	ret_val = pthread_mutexattr_settype(&mtx_attr,
			PTHREAD_MUTEX_RECURSIVE);
	check_error(ret_val, "mutexattr_settype failed");

	/* Initialize the recursive mutex */
	ret_val = pthread_mutex_init(&r_mtx, &mtx_attr);
	check_error(ret_val, "mutex_init failed");

	/* Set the type attribute to normal */
	ret_val = pthread_mutexattr_settype(&mtx_attr,
			PTHREAD_MUTEX_NORMAL);
	check_error(ret_val, "mutexattr_settype failed");

	/* Initialize the normal mutex */
	ret_val = pthread_mutex_init(&n_mtx, &mtx_attr);
	check_error(ret_val, "mutex_init failed");

	/* Destroy the attributes object */
	ret_val = pthread_mutexattr_destroy(&mtx_attr);
	check_error(ret_val, "mutexattr_destroy failed");

	/* Rest of application code here */

        /*
         * Create a thread
         */
        ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
                (void *(*)())start_routine, (void *)1);
        check_error(ret_val, "pthread_create 1 failed");

        /*
         * Wait for the threads to finish
         */
        ret_val = pthread_join(tid1, (void **)NULL);
        check_error(ret_val, "pthread_join: tid1");
}

void
start_routine(int thread_num)
{
        int     ret_val;

	sched_yield();

        /* Lock the recursive lock recursively. */
        ret_val = pthread_mutex_lock(&r_mtx);
        check_error(ret_val, "mutex_lock r_mtx");
	printf("Thread %d - got r_mtx\n", thread_num);

        ret_val = pthread_mutex_lock(&r_mtx);
        check_error(ret_val, "mutex_lock r_mtx");
	printf("Thread %d - got r_mtx\n", thread_num);

        ret_val = pthread_mutex_unlock(&r_mtx);
        check_error(ret_val, "mutex_unlock r_mtx");
	printf("Thread %d - released r_mtx\n", thread_num);

        ret_val = pthread_mutex_unlock(&r_mtx);
        check_error(ret_val, "mutex_unlock r_mtx");
	printf("Thread %d - released r_mtx\n", thread_num);

        /* Try locking the non-recursive lock recursively */
        ret_val = pthread_mutex_lock(&n_mtx);
        check_error(ret_val, "mutex_lock n_mtx");
	printf("Thread %d - got n_mtx\n", thread_num);

        ret_val = pthread_mutex_lock(&n_mtx);
        check_error(ret_val, "mutex_lock n_mtx");
	printf("Thread %d - got n_mtx\n", thread_num);

        ret_val = pthread_mutex_unlock(&n_mtx);
        check_error(ret_val, "mutex_unlock n_mtx");
	printf("Thread %d - released n_mtx\n", thread_num);

        ret_val = pthread_mutex_unlock(&n_mtx);
        check_error(ret_val, "mutex_unlock n_mtx");
	printf("Thread %d - released n_mtx\n", thread_num);
}

