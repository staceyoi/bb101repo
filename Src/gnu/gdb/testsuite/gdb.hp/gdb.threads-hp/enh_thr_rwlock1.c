#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <errno.h>

pthread_rwlock_t        rw1;  /* global process shared R/W lock */
pthread_rwlock_t	rw2;
pthread_rwlock_t	rw3 = PTHREAD_RWLOCK_INITIALIZER;

extern void	fatal_error(int err, char *func);

#define check_error(return_val, msg) { 			\
	if (return_val != 0) 				\
		fatal_error(return_val, msg);		\
}

void bar()
{
}

main()
{
	pthread_rwlockattr_t	rw_attr1;
	pthread_t		tid1;
	pthread_t		tid2;
	int			ret_val;
	extern void		start_routine();

	/*
	 * Initialize the attributes for rw1
	 */
	ret_val = pthread_rwlockattr_init(&rw_attr1);
	check_error(ret_val, "rwlockattr_init failed");

	/*
	 * rw1 is shared between multiple processes
	 */
	ret_val = pthread_rwlockattr_setpshared(&rw_attr1,
						PTHREAD_PROCESS_SHARED);
	check_error(ret_val, "setpshared failed");

	/*
	 * Initialize rw1, then destroy the attributes
	 */
	ret_val = pthread_rwlock_init(&rw1, &rw_attr1);
	check_error(ret_val, "rwlock_init 1 failed");

	ret_val = pthread_rwlockattr_destroy(&rw_attr1);
	check_error(ret_val, "rwlockattr_destroy failed");

	/*
	 * Initialize rw2 with default attributes.
	 * rw3 has already been statically initialized.
	 */
	ret_val = pthread_rwlock_init(&rw2, 
		(pthread_rwlockattr_t *)NULL);
	check_error(ret_val, "rwlock_init 2 failed");

	/*
	 * Create two threads to do the work
	 */
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
				 (void *(*)())start_routine, (void *)NULL);
	check_error(ret_val, "pthread_create 1 failed");

        /* wait till first thread is created */
        for (int i=0; i<100000000; i++);

	ret_val = pthread_create(&tid2, (pthread_attr_t *)NULL,
				 (void *(*)())start_routine, (void *)NULL);
	check_error(ret_val, "pthread_create 2 failed");

	/*
	 * Wait for the threads to finish
	 */
	ret_val = pthread_join(tid1, (void **)NULL);
	check_error(ret_val, "pthread_join: tid1");

	ret_val = pthread_join(tid2, (void **)NULL);
	check_error(ret_val, "pthread_join: tid2");

        bar();

	/*
	 * Destroy the R/W locks
	 */
	ret_val = pthread_rwlock_destroy(&rw1);
	check_error(ret_val, "rwlock_destroy rw1");

	ret_val = pthread_rwlock_destroy(&rw2);
	check_error(ret_val, "rwlock_destroy rw2");

	ret_val = pthread_rwlock_destroy(&rw3);
	check_error(ret_val, "rwlock_destroy rw3");

	exit(0);
}

void foo()
{
}

void
start_routine()
{
        int     ret_val;

        ret_val = pthread_rwlock_wrlock(&rw1);
        check_error(ret_val, "rwlock_wrlock rw1");
	printf("Got rw1 write lock\n");

        ret_val = pthread_rwlock_rdlock(&rw2);
        check_error(ret_val, "rwlock_rdlock rw2");
	printf("Got rw2 read lock\n");

        ret_val = pthread_rwlock_rdlock(&rw2);
        check_error(ret_val, "rwlock_rdlock rw2");
	printf("Got rw2 read lock\n");

        ret_val = pthread_rwlock_wrlock(&rw3);
        check_error(ret_val, "rwlock_wrlock rw3");
	printf("Got rw3 write lock\n");

        foo();

        ret_val = pthread_rwlock_unlock(&rw3);
        check_error(ret_val, "rwlock_unlock rw3");
	printf("Released rw3 lock\n");

        ret_val = pthread_rwlock_unlock(&rw2);
        check_error(ret_val, "rwlock_unlock rw2");
	printf("Released rw2 lock\n");

        ret_val = pthread_rwlock_unlock(&rw2);
        check_error(ret_val, "rwlock_unlock rw2");
	printf("Released rw2 lock\n");

        ret_val = pthread_rwlock_unlock(&rw1);
        check_error(ret_val, "rwlock_unlock rw1");
	printf("Released rw1 lock\n");
}

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
