#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

extern void	fatal_error(int err_num, char *function); 

#define check_error(return_val, msg) {   		\
		if (return_val != 0)  			\
			fatal_error(return_val, msg);	\
	}

pthread_mutex_t	mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condvar = PTHREAD_COND_INITIALIZER;	
int flag = 0;

void foo() {}
void bar() {}
void zoo() {}

void thread_default(int arg)
{
  int ret_val;
  ret_val = pthread_mutex_lock(&mtx);
  foo();
  ret_val = pthread_mutex_unlock(&mtx);
}

void thread1_func()
{
  int ret_val, oldstate = 0;
  ret_val = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldstate);
  check_error(ret_val, "pthread_setcancelstate()");

  ret_val = pthread_mutex_lock(&mtx);
  bar();
  ret_val = pthread_mutex_unlock(&mtx);
}

main()
{
	pthread_t		tid1, tid2;
	pthread_attr_t		attr;
	int		ret_val, oldstate;;

	/* Create a thread with default attributes */
        ret_val = pthread_mutex_lock(&mtx);
	ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
		(void *(*)())thread_default, (void*) 0x55);
	check_error(ret_val, "pthread_create 1 failed");
        ret_val = pthread_mutex_unlock(&mtx);

	/* Create the attr object */
	ret_val = pthread_attr_init(&attr);
	check_error(ret_val, "pthread_attr_init()");

        ret_val = pthread_attr_setstacksize(&attr, 0x8000);
        check_error(ret_val, "pthread_attr_setstacksize()");

        pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);
        sleep(1);

	/* Create a new thread to execute thread1_func() */
        ret_val = pthread_mutex_lock(&mtx);
	ret_val = pthread_create(&tid2, &attr, 
				 (void *(*)())thread1_func,
				 (void *)NULL);
	check_error(ret_val, "pthread_create()");

        ret_val = pthread_detach(tid2);
	check_error(ret_val, "pthread_detach()");
        ret_val = pthread_mutex_unlock(&mtx);

        sleep(10);

	/* Its safe to destroy the attr object now */
	ret_val = pthread_attr_destroy(&attr);
	check_error(ret_val, "pthread_attr_dest()");

        /* Wait for thread to finish executing  */
        ret_val = pthread_join(tid1, (void **)NULL);
        check_error(ret_val, "pthread_join()");
}

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}
