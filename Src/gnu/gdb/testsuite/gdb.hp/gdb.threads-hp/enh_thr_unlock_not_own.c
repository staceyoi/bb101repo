#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

pthread_mutex_t         mtx = PTHREAD_MUTEX_INITIALIZER;

/* Print error information, exit with -1 status. */
void
fatal_error(int err_num, char *function)
{
        char    *err_string;

        err_string = strerror(err_num);
        fprintf(stderr, "%s error: %s\n", function, err_string);
        exit(-1);
}

#define check_error(return_val, msg) {		\
	if (return_val != 0)				\
		fatal_error(return_val, msg);		\
}

main()
{
	pthread_t		tid1;
	extern void		start_routine(int num);
	int			ret_val;

        /*
         * Create a thread
         */
        ret_val = pthread_create(&tid1, (pthread_attr_t *)NULL,
                (void *(*)())start_routine, (void *)1);
        check_error(ret_val, "pthread_create 1 failed");

        /*
         * Wait for the threads to finish
         */
        ret_val = pthread_join(tid1, (void **)NULL);
        check_error(ret_val, "pthread_join: tid1");
}

void
start_routine(int thread_num)
{
        int     ret_val;
        ret_val = pthread_mutex_unlock(&mtx);
        check_error(ret_val, "mutex_unlock mtx");
}

