#include <unistd.h>
#include <pthread.h>
#include <dl.h>
#include <sys/types.h>
#include <sys/wait.h>

pthread_mutex_t	job_lock1 = PTHREAD_MUTEX_INITIALIZER;

void *threadfunc(pthread_mutex_t* job_lock) {
   int	ret_val;
   sleep (1);
   /* Acquire the associated mutex lock */
   printf ("Thread\n");
   if ((ret_val = pthread_mutex_lock(job_lock)) != 0)
     printf ("mtx_lock failed: %d", ret_val);
   return NULL;
 }

#ifdef PROTOTYPES
int main (void)
#else
main ()
#endif
{
  int  pid;
  void (*solib_foo)();
  shl_t handle;
  pthread_t          thr[256];
  int i;
  int stat_loc;

   for (i=0 ; i < 1 ; i++) {
     pthread_create(&thr[i], NULL, threadfunc, &job_lock1);
   }
   for (i=0 ; i < 1 ; i++) {
     pthread_join(thr[i],NULL);
   }

  sleep(4);

  pid = fork ();
  if (pid == 0) {
    execlp ("./enh_thr_exit_own_mx", "./enh_thr_exit_own_mx", (char *)0);
  }
  else {
     wait(&stat_loc);
     printf ("Parent\n");
  }
}
