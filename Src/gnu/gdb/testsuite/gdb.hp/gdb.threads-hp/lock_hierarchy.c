#include <pthread.h>

pthread_t       t1;
pthread_t       t2;
pthread_mutex_t mutex_a;
pthread_mutex_t mutex_b;
pthread_mutex_t mutex_c;
pthread_mutex_t mutex_d;

void* mythread(void* lp){
        pthread_mutex_lock(&mutex_a);
        pthread_mutex_lock(&mutex_c);
        pthread_mutex_lock(&mutex_d);
        pthread_mutex_unlock(&mutex_a);
        pthread_mutex_unlock(&mutex_b);
        pthread_mutex_unlock(&mutex_c);
        pthread_mutex_unlock(&mutex_d);
        return NULL;
}

void* myerrorthread(void* lp){
        pthread_mutex_lock(&mutex_d);
        pthread_mutex_lock(&mutex_a);
        pthread_mutex_lock(&mutex_b);
        pthread_mutex_lock(&mutex_c);
        pthread_mutex_unlock(&mutex_a);
        pthread_mutex_unlock(&mutex_b);
       pthread_mutex_unlock(&mutex_c);
        pthread_mutex_unlock(&mutex_d);
        return NULL;
}
int main(void){
        pthread_mutex_init(&mutex_a,NULL);
        pthread_mutex_init(&mutex_b,NULL);
        pthread_mutex_init(&mutex_c,NULL);
        pthread_mutex_init(&mutex_d,NULL);
        pthread_create(&t1,NULL,myerrorthread,NULL);
        pthread_join(t1,NULL);
        pthread_create(&t2,NULL,mythread,NULL);
        pthread_join(t2,NULL);
        return 0;
}
