/* BeginSourceFile main-thr-exit.c

  This file creates threads, and exits main thread so that wdb
  can be tested on main thread delete.

  To compile:

      cc -Ae -g -o main-thr-exit -lpthread main-thr-exit.c

  To run:
  
     main-thr-exit     --normal run
     main-thr-exit 1   --waits in each thread to keep it alive.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#include <sys/mman.h>

#define TRUE 1
#define FALSE 0
#define OUTER_LOOP_COUNT 3
#define N_THREADS 4

/* Uncomment to turn on debugging output */
/* #define START_DEBUG  */

/* True if waiting for attach.
 */
int wait_here;

/* Thing to check for debugging purposes.
*/
int a_global = 0;

void
foo() {
}

/* Routine for each thread to run, does nothing.
 */
void *spin( void * vp)
{
    long me = (long) vp;
    int i;
    char* ll;
    
#ifdef START_DEBUG
    printf( "== In thread %d\n", me );
#endif

    /* Wait till debugger tells us to exit. */
    while (wait_here)
    {};

    /* Sleep so that main thread can exit */
    sleep (1);

    /* dummy function on which debugger places a breakpoint */
    foo();

    a_global++;

    ll = malloc (20);

    if (me != 0)
     free (ll);

  return 0;
}

void
do_pass( pass )
    int pass;
{
    int i;
    pthread_t t[ N_THREADS ];
    int err;

    /* Start N_THREADS threads,
     */
    for( i = 0; i < N_THREADS; i++ ) {
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

        err = pthread_create( &t[i], &attr, spin, (void *)i );
        if( err != 0 ) {
            printf( "== Start/stop, error %d in thread %d create\n", err, i );
        }
    }

    // Exit the main thread
    pthread_exit (NULL);
}

void
do_it()
{
    /* We want to start some threads and then
     * end them, and then do it again and again
     */
    int i;
    int dummy;
    
    for( i = 0; i < OUTER_LOOP_COUNT; i++ ) {
        do_pass( i );
        dummy = i;
    }
}

int
main( argc, argv )
int    argc;
char **argv;
{
   char* ll;

   wait_here = FALSE;
   if((argc > 1) && (0 != argv )) {
       if( 1 == atoi( argv[1] ) )
           wait_here = TRUE;
    }

#ifdef START_DEBUG 
    printf( "== Test starting\n" );
#endif
    ll = malloc (50);
    ll = malloc (50);
    ll = malloc (50);

    ll = malloc (50);
    ll = malloc (50);
    do_it();
    ll = malloc (50);
    ll = malloc (50);
    
#ifdef START_DEBUG
    printf( "== Test done\n" );
#endif
    return(0);
}

///* EndSourceFile */
