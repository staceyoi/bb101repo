#include <pthread.h>
#include "errors.h"

pthread_mutex_t aMutex2 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t aMutex3 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t aMutex4 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t aMutex5 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t aMutex6 = PTHREAD_MUTEX_INITIALIZER;

int count=0;

// a GOOD behaved thread
void *thread_routine2(void *arg)
{
  int status;
  int* temp;

  status = pthread_mutex_lock(&aMutex2);
  if (status!=0)
     err_abort(status, "Lock mutex");
  count=count+2;

  status = pthread_mutex_unlock(&aMutex2);
  if (status!=0)
     err_abort(status, "Unlock mutex");

  return NULL;
}

// THREAD 3 TO THREAD 6 ARE "missingUnlock" cases

// main thread call pthread_join() to "destroy" this thread
void *thread_routine3(void *arg)
{
  int status;
  int* temp;

  status = pthread_mutex_lock(&aMutex3);
  if (status!=0)
     err_abort(status, "Lock mutex");
  count=count+1;

  return NULL;
}

// thread exit naturally
void *thread_routine4(void *arg)
{
  int status;
  int* temp;

  status = pthread_mutex_lock(&aMutex4);
  if (status!=0)
     err_abort(status, "Lock mutex");
  count=count+1;
  return NULL;
}

// thread exit by calling pthread_exit()
void *thread_routine5(void *arg)
{
  int status;
  int* temp;

  status = pthread_mutex_lock(&aMutex5);
  if (status!=0)
     err_abort(status, "Lock mutex");
  count=count+1;
  pthread_exit(NULL);
  return NULL;
}

// main thread calls pthread_cancel() to exit this thread
void *thread_routine6(void *arg)
{
  int status;
  int* temp;
  int counter_loop;

  status = pthread_mutex_lock(&aMutex6);
  if (status!=0)
     err_abort(status, "Lock mutex");
  count=count+1;

  for (counter_loop = 0; ; counter_loop++)
    if ((counter_loop % 1000) == 0)
      pthread_testcancel ();

  // should never reach this point
  errno_abort("pthread_cancel() didn't exit thread 6");
  return NULL;
}


int main (int argc, int argv[ ])
{
  int status, i;
  pthread_t thread2, thread3, thread4, thread5, thread6;

  status = pthread_create (&thread6, NULL, thread_routine6, NULL);
  if (status!=0)
  err_abort(status, "Creat thread6");
 status = pthread_create (&thread2, NULL, thread_routine2, NULL);
  if (status!=0)
     err_abort(status, "Creat thread3");
  status = pthread_create (&thread3, NULL, thread_routine3, NULL);
  if (status!=0)
     err_abort(status, "Creat thread3");
  status = pthread_create (&thread4, NULL, thread_routine4, NULL);
  if (status!=0)
     err_abort(status, "Creat thread4");
  status = pthread_create (&thread5, NULL, thread_routine5, NULL);
  if (status!=0)
     err_abort(status, "Creat thread5");

  status = pthread_cancel (thread6);
  if (status != 0)
    err_abort (status, "Cancel thread6");

  status=pthread_join (thread3, NULL);
  if (status!=0)
     err_abort(status, "Join thread");
  printf("Joining thread 3 suceeded\n");

  status=pthread_join (thread5, NULL);  //thread 5 raises exception handling, which
  if (status!=0)                        //is hard to determine when it will finish
     err_abort(status, "Join thread"); // same for thread6, but we let it start first
  sleep(5);  // since test/test slows the event stream alot, increasing
              // the printing level make it even worse,  therefore this
              // sleep will allow other threads to finish before main
              // thread terminated.

  printf("Main thread finished\n");
  return 0;
}


