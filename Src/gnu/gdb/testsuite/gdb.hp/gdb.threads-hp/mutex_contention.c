/* creates 255 threads and makes them all wait for 1 mutex */

#include <stddef.h>
#include <stdio.h>
#include <unistd.h>
#include "pthread.h"

#define THREAD_COUNT 255
pthread_t t[THREAD_COUNT];
pthread_mutex_t amutex;

//int y=0;

void* test (void* p){
//        int count;
//        count = ++y;
//        printf("thread %d is created\n", count);
        pthread_mutex_lock(&amutex);
//      printf("T[%d] EXITed..\n", count);

        return NULL;
}

int main(void)
{
        void *thread_result;
//        int status;
        int x = 0;
        pthread_mutex_init(&amutex,NULL);
        for(x = 0; x < THREAD_COUNT; x++){
                pthread_create(&t[x],NULL,test,NULL);
        }
        for(x = 0; x < THREAD_COUNT; x++){
                sched_yield();
        }
        for(x = 0; x < THREAD_COUNT; x++){
//              printf("unlocking ..\n");
                pthread_mutex_unlock(&amutex);
                sched_yield();
        }

        sleep(2);
//      printf("Main thread terminated !! \n");
        return 0;
}
