#include <pthread.h>
#include "errors.h"

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int value=0;

void *wait_thread (void *arg)
{
  int status;

  sleep(2);
  status = pthread_mutex_lock(&mutex1);
  if (status!=0)
    err_abort (status, "Lock mutex1");
  value=100;
  status = pthread_cond_signal(&cond);
  if (status!=0)
    err_abort (status, "Cond signal");
  status = pthread_mutex_unlock(&mutex1);
  if (status!=0)
    err_abort (status, "Unlock mutex1");

 return NULL;
}

int main (int argc, char *argv[])
{

  int status;
  pthread_t wait_thread_id;

  status = pthread_create(&wait_thread_id, NULL, wait_thread, NULL);
  if (status!=0)
    err_abort (status, "Create wait thread");

  //apply condictional wait on a non locked mutex
  status = pthread_cond_wait(&cond, &mutex1);
  if (status!=0)
    printf ("Error on condition wait\n");

  printf("Pthread_cond_wait go thru..\n");
  printf("value=%d\n", value);

  status = pthread_join(wait_thread_id, NULL);
  if (status!=0)
    err_abort (status, "Join thread");

  return 0;
}

