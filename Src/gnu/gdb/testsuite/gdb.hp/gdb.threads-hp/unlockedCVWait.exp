# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
#

#Test the rule to stop gdb when application attempt by thread to wait  on conditional
#variable without locking the associated mutex and for unlock the mutex which is not owned by thread.

if { [skip_hp_tests] } { continue }

set testfile unlockedCVWait 
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}
set outdated_libpthread 0
set pid [pid]
set outdir /tmp/thr_batch_$pid
set out ${outdir}/out
set dir .

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LIBRTC_SERVER) "${objdir}/../librtc64.sl"
  } else {
    set env(LIBRTC_SERVER) "${objdir}/../librtc32.sl"
  }
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable {debug "
additional_flags=-Ae -Wl,+s" "ldflags=-lpthread"}] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will
automatically fail."
}

if { [istarget "hppa1*-hp-hpux*"] } {
exec chatr +s enable ${binfile}
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir
gdb_load ${binfile}

send_gdb "shell uname -r\n"
gdb_expect {
  -re ".*11.23.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*11.31.*$gdb_prompt $" { set 1123_kern 1 }
  -re ".*$gdb_prompt $" { set 1123_kern 0 }
  timeout {fail "(timeout) uname"}
}
#To skip this testcase for older version of libpthread

send_gdb "shell what /usr/lib/hpux32/libpthread.so.1\n"
gdb_expect {
  -re ".*0409.*$gdb_prompt $" { set outdated_libpthread 1; pass "what string" }
  -re ".*$gdb_prompt $" {pass "not outdated libpthread" } }

if { $outdated_libpthread==1 } {
  return;
}
# Need to make sure we're running on 11.23 and above
if { $1123_kern != 1 } {
  gdb_exit
  return 0
}

gdb_test "set thread-check on" "" "set thread-check "
gdb_test "b main" ".*Breakpoint 1.*" "putting a breakpoint at main "
gdb_test "run" ".*Breakpoint 1, main.*:33.*" "Breakpoint at main"
gdb_test "c" ".*Continuing.*warning:.*" "cv wait on no mutex"
gdb_test "c" ".*Continuing.*warning:.*" "unlock not own"
gdb_test "c" ".*Continuing.*Program exited normally.*" ""
gdb_exit

# Do the batch mode check
catch "exec chatr +dbg enable ${binfile}"
system "/usr/bin/rm -f ${dir}/rtcconfig"
system "/usr/bin/rm -f ${testfile}.*.threads"
system "echo 'set thread-check on' >> ${dir}/rtcconfig"
system "echo 'set frame-count 5' >> ${dir}/rtcconfig"
system "echo 'files=unlockedCVWait' >> ${dir}/rtcconfig"
set oldtimeout $timeout
set timeout 120

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/hpux32"
  } else {
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/hpux64"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    # Batch mode thread check not supported for PA64 on 11.23
    catch "exec uname -r | grep \"11.23\" > out"
    catch "exec /usr/bin/cat out | wc -c" output1
    if { $output1 } {
      gdb_exit
      return 0
    }
    set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib/pa20_64"
  } else {
    #set env(LD_LIBRARY_PATH) "/opt/langtools/wdb/lib"
    return 0
  }
}

if { [istarget "ia64*-hp-*"] } {
  if { "${IS_ILP32}" == "TRUE" } {
    set env(LD_PRELOAD) "${objdir}/../librtc.sl"
  } else {
    set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
  }
} else {
  if { [istarget "hppa2*-hp-hpux*"] } {
    set env(LD_PRELOAD) "${objdir}/../librtc64.sl"
  } else {
    #set env(LD_PRELOAD) "${objdir}/../librtc32.sl"
  }
}


if [istarget "hppa2.0w-hp-hpux*"] then {
  set env(GDB_SERVER) "${objdir}/../gdb64"
} else {
  set env(GDB_SERVER) "${objdir}/../gdb"
}


set env(BATCH_RTC) "on"
spawn "${binfile}"
expect {
  timeout {
    fail "The app run timed out."
    set app_pid [exp_pid]
    system "kill -9 $app_pid"
    set timeout $oldtimeout
    return 0
  }
  eof { pass "The run passed." }
}

set env(BATCH_RTC) "off"
set env(LD_PRELOAD) ""

catch "exec mkdir ${outdir}"
set thrfile ${outdir}/threads_file
system "mv ${testfile}.*.threads ${thrfile}"
catch "exec fgrep \"THREAD ERROR EVENT: Attempt by thread\" ${thrfile} | grep \"to wait on condition variable\" | grep \"without locking the associated mutex\" > out" 
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Error event obtained about cv wait on no mutex"
} else {
  fail "Error event not obtained"
}

catch "exec grep \"Variable name:\" ${thrfile} | grep \"cond\" > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Condition variable correct"
} else {
  fail "Condition variable incorrect"
}

catch "exec fgrep \"Detailed information on mutex\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Mutex info printed"
} else {
  fail "Mutex info not printed"
}

catch "exec fgrep \"THREAD ERROR EVENT: Attempt to unlock mutex\" ${thrfile} | grep \"not owned by thread\" > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Error event obtained about unlock not own"
} else {
  fail "Error event not obtained"
}

catch "exec fgrep \"Thread debugging\" ${thrfile} > out"
catch "exec /usr/bin/cat out | wc -c" output1
if { $output1 } {
  pass "Backtrace printed"
} else {
  fail "Backtrace not printed"
}

catch "exec rm -rf ${outdir}"
catch "exec rm -f ${dir}/rtcconfig"

set timeout $oldtimeout
return 0


