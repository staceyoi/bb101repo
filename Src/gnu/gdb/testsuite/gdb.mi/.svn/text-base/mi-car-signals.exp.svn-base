#
# Test essential Machine interface (MI) operations
#
# Verify that, using mi commands, the list in signals view matches with the
# GDB "info signal" command.
#

load_lib mi-support.exp
set MIFLAGS "-i=mi"

gdb_exit
if [mi_gdb_start] {
    continue
}

if { [istarget "ia64-*-*"] || [istarget "hppa2.0w-hp-hpux*"] } then {
        verbose "PA specific MI test cases..."
        return 0
}

set testfile "car"
set binfile ${objdir}/${subdir}/${testfile}

if ![file exists ${binfile}] {
cd ${srcdir}/${subdir}/car.src
system "make path=${binfile}"
} else {
verbose "Running test points with the already built car executable..."
}

mi_delete_breakpoints
mi_gdb_reinitialize_dir $srcdir/$subdir/car.src
mi_gdb_load ${binfile}

mi_gdb_test "1-gdb-set confirm off" "1\\\^done"
mi_gdb_test "2-gdb-set width 0" "2\\\^done"
mi_gdb_test "3-gdb-set height 0" "3\\\^done"
mi_gdb_test "4-gdb-show prompt" "4\\\^done,.*gdb.*"
mi_gdb_test "5-gdb-set auto-solib-add on" "5\\\^error,.*"
#mi_gdb_test "6-environment-cd /usr/home/aroraru/CARNEW" "6\\\^done"
mi_gdb_test "7 info threads" "7\\\^error,msg=\"No stack.\""
#mi_gdb_test "8-data-list-register-names" "8\\\^done,register-names=\\\[flags,r1,rp,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,dp,ret0,ret1,sp,r31,sar,pcoqh,pcsqh,pcoqt,pcsqt,eiem,iir,isr,ior,ipsw,goto,sr4,sr0,sr1,sr2,sr3,sr5,sr6,sr7,cr0,cr8,cr9,ccr,cr12,cr13,cr24,cr25,cr26,mpsfu_high,mpsfu_low,mpsfu_ovflo,pad,fpsr,fpe1,fpe2,fpe3,fpe4,fpe5,fpe6,fpe7,fr4,fr4R,fr5,fr5R,fr6,fr6R,fr7,fr7R,fr8,fr8R,fr9,fr9R,fr10,fr10R,fr11,fr11R,fr12,fr12R,fr13,fr13R,fr14,fr14R,fr15,fr15R,fr16,fr16R,fr17,fr17R,fr18,fr18R,fr19,fr19R,fr20,fr20R,fr21,fr21R,fr22,fr22R,fr23,fr23R,fr24,fr24R,fr25,fr25R,fr26,fr26R,fr27,fr27R,fr28,fr28R,fr29,fr29R,fr30,fr30R,fr31,fr31R\\\]"
mi_gdb_test "9-break-insert window.cpp:20" "9\\\^done,bkpt=\{number=\"1\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"Window::rollup\\(void\\)\",file=\"window.cpp\",line=\"20\",times=\"0\"\}"
mi_gdb_test "10-break-insert wheel.cpp:17" "10\\\^done,bkpt=\{number=\"2\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"Wheel::inflate\\(int\\)\",file=\"wheel.cpp\",line=\"17\",times=\"0\"\}"
mi_gdb_test "11-break-disable 2" "11\\\^done"
mi_gdb_test "12-break-insert cartest.cpp:27" "12\\\^done,bkpt=\{number=\"3\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"27\",times=\"0\"\}"
mi_gdb_test "13-break-insert cartest.cpp:33" "13\\\^done,bkpt=\{number=\"4\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"33\",times=\"0\"\}"
mi_gdb_test "14-break-insert cartest.cpp:30" "14\\\^done,bkpt=\{number=\"5\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"30\",times=\"0\"\}"
mi_gdb_test "15-break-insert cartest.cpp:24" "15\\\^done,bkpt=\{number=\"6\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\",times=\"0\"\}"
mi_gdb_test "16-break-insert cartest.cpp:19" "16\\\^done,bkpt=\{number=\"7\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\",times=\"0\"\}"
mi_gdb_test "17-break-insert cartest.cpp:24" "17\\\^done,bkpt=\{number=\"8\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\",times=\"0\"\}"
mi_gdb_test "18-break-insert cartest.cpp:42" "18\\\^done,bkpt=\{number=\"9\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"42\",times=\"0\"\}"
mi_gdb_test "19-break-insert -t main" "19\\\^done,bkpt=\{number=\"10\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\",times=\"0\"\}"
#mi_gdb_test "20-exec-run" "20\\\^running"
# mi_gdb_test cannot be used for asynchronous commands because there are
    # two prompts involved and this can lead to a race condition.
    # The following is equivalent to a send_gdb "000-exec-run\n"
    mi_run_cmd
    gdb_expect {
        -re ".*stopped.*$mi_gdb_prompt$" {
            pass "run to main"
        }
            fail "run to main (timeout)"
        }
mi_gdb_test "21 info program" "21\\\^done"
mi_gdb_test "22 info threads" "22\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "23-stack-info-depth" "23\\\^done,depth=\"1\""
mi_gdb_test "24-stack-list-frames 0 1" "24\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
#mi_gdb_test "25-data-list-changed-registers" "25\\\^done,changed-registers=\\\[0,1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,48,49,50,51,52,53,54,57,58,60,61,63,64,72,73,74,77,79,80,81,82,84,86,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,111,112,113,114,115,116,119,120,122,123,125,126,127\\\]"
mi_gdb_test "26 info sharedlibrary" "26\\\^done"
mi_gdb_test "27 info threads" "27\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "28-stack-info-depth" "28\\\^done,depth=\"1\""
mi_gdb_test "29-stack-list-frames 0 1" "29\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
#mi_gdb_test "30-data-list-changed-registers" "30\\\^done,changed-registers=\\\[\\\]"
mi_gdb_test "31 info sharedlibrary" "31\\\^done"
mi_gdb_test "32 info threads" "32\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "33-stack-info-depth" "33\\\^done,depth=\"1\""
mi_gdb_test "34-stack-list-frames 0 1" "34\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
#mi_gdb_test "35-data-list-changed-registers" "35\\\^done,changed-registers=\\\[\\\]"
mi_gdb_test "36 info sharedlibrary" "36\\\^done"
mi_gdb_test "37-stack-list-arguments 0 0 0" "37\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "38-stack-list-locals 0" "38\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
mi_gdb_test "40-var-create - * bala1" "40\\\^done,name=\"var1\",numchild=\"0\",type=\"int\""
mi_gdb_test "41-var-create - * balas2" "41\\\^done,name=\"var2\",numchild=\"0\",type=\"int\""
mi_gdb_test "42-var-create - * mycar" "42\\\^done,name=\"var3\",numchild=\"0\",type=\"class Ferrari\""
mi_gdb_test "43-var-create - * abcd" "43\\\^done,name=\"var4\",numchild=\"2\",type=\"int \\\[2\\\]\""
mi_gdb_test "44-var-evaluate-expression var1" "44\\\^done,value=\"1\""
mi_gdb_test "45-var-evaluate-expression var3" "45\\\^done,value=\"\{<class Car> = \{registrationno = 0, modelno = 0, currentspeed = 0, engine = \{isrunning = false\}, wheel = \{\{airlevel = 0\}, \{airlevel = 0\}, \{airlevel = 0\}, \{airlevel = 0\}\}, left = \{window = \{isclosed = .*\}\}, right = \{window = \{isclosed = .*\}\}\}, <No data fields>\}\""
mi_gdb_test "46-var-evaluate-expression var2" "46\\\^done,value=\".*\""
mi_gdb_test "47 info signals" "47\\\^done"
#mi_gdb_test "48-exec-next 1" "48\\\^running"
send_gdb "48-exec-next 1\n"
gdb_expect {
    -re "48\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "48\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "49 info threads" "49\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"25\"\}"
mi_gdb_test "50-stack-info-depth" "50\\\^done,depth=\"1\""
mi_gdb_test "51-stack-list-frames 0 1" "51\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"25\"\}\\\]"
mi_gdb_test "52-var-update var1" "52\\\^done,changelist=\{\}"
mi_gdb_test "53-var-update var2" "53\\\^done,changelist=\{\}"
mi_gdb_test "54-var-update var3" "54\\\^done,changelist=\{\}"
mi_gdb_test "55-var-update var4" "55\\\^done,changelist=\{\}"
#mi_gdb_test "56-data-list-changed-registers" "56\\\^done,changed-registers=\\\[2,20,21,22,23,24,25,26,28,29,31,33,35,38,39,40,41,45\\\]"
mi_gdb_test "57 info sharedlibrary" "57\\\^done"
#mi_gdb_test "58-exec-next 1" "58\\\^running"
send_gdb "58-exec-next 1\n"
gdb_expect {
    -re "58\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "58\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "59 info threads" "59\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"26\"\}"
mi_gdb_test "60-stack-info-depth" "60\\\^done,depth=\"1\""
mi_gdb_test "61-stack-list-frames 0 1" "61\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"26\"\}\\\]"
mi_gdb_test "62-var-update var1" "62\\\^done,changelist=\{\}"
mi_gdb_test "63-var-update var2" "63\\\^done,changelist=\{\}"
mi_gdb_test "64-var-update var3" "64\\\^done,changelist=\{\}"
mi_gdb_test "65-var-update var4" "65\\\^done,changelist=\{\}"
#mi_gdb_test "66-data-list-changed-registers" "66\\\^done,changed-registers=\\\[2,33,35,41\\\]"
mi_gdb_test "67 info sharedlibrary" "67\\\^done"
mi_gdb_test "69-data-evaluate-expression \$_exitcode" "69\\\^done,value=\"void\""


mi_gdb_exit
return 0
