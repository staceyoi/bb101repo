/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */
 
#include "car.h"

Car::Car()
{}
Car::~Car()
{}
void Car::increasespeed(int speed)
{
        currentspeed += speed; 
}

void Car::decreasespeed(int speed)
{
        currentspeed -= speed;
}

int Car::getcurrentspeed()
{
        return currentspeed;
}

int Car::getregistrationno()
{
        return registrationno;
}
int Car::getmodelno()
{
        return modelno;
}

void Car::setregistrationno(int regno)
{
        registrationno=regno;
}

void Car::setmodelno(int mno)
{
        modelno=mno;
}
