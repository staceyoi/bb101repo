/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */
 
#include "wheel.h"
#include "door.h"
#include "engine.h"


class Car {

public:

        Car();
        ~Car();
        int registrationno;
        int modelno;
        int currentspeed;
        
        Engine engine;
        Wheel wheel[4];
        Door left,right;

        int getcurrentspeed();
        int getregistrationno();
        int getmodelno();
        void setregistrationno(int regno);
        void setmodelno(int mno);  
        void increasespeed(int speed);
        void decreasespeed(int speed);
        
        };

