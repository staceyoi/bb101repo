/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */
 
#include "window.h"

class Door {

public:
        Window window;

        Door();
        ~Door();
        void open();
        void close();
        
};
