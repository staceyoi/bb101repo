/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */
 
#include "engine.h"
#include <iostream.h>
//using namespace std;

Engine::Engine()
{
}
Engine::~Engine()
{
}
void Engine::start()
{
        isrunning=true;
        cout << "Car started"<<endl;
}

void Engine::stop()
{
        isrunning=false;
        cout << "Car stopped"<<endl;
}
void Engine::rev()
{
        cout << "Car reversed";
}

