/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */

class Engine 
{
public:
        bool isrunning;
        Engine();
        ~Engine();
        void start();
        void stop();
        void rev();
        
};
