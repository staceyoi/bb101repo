/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */
 
class Wheel {
public:
        int airlevel;
        Wheel();
        ~Wheel();
        void inflate(int psi);
};
