/*
 * (C) Copyright 2004-2005 Hewlett-Packard Development Company, L.P.
 */
 
enum BOOL { True, False };
class Window 
{

public:
        bool isclosed;

        BOOL GetUserResponse(char *prompt);
        Window();
        ~Window();
        void rollup();
        void rolldown();
        
        int isWindowClosed();

};
