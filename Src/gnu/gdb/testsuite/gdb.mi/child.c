#include<dlfcn.h>
#include<stdio.h>
#include<stdlib.h>

int main ()
{
  void *handle;
  void *hand;
  char *error;
  void (*symfoo) (void);

  struct load_module_desc desc;
  int k = 383;

  handle = dlopen ("gdb.mi/libfoo.sl",RTLD_LAZY);
  if (!handle) {
    fputs (dlerror(), stderr);
    exit(1);
  }
  
  symfoo = (void (*)())dlsym (handle,"foo");
  if ((error = dlerror()) != NULL)  {
    fputs(error, stderr);
    exit(1);
  }
  (*symfoo)();

  dlclose (handle);

  printf ("K is %d.\n",k);
  return 0;
}
