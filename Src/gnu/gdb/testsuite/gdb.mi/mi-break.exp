#   Copyright (C) 1999 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

#
# Test essential Machine interface (MI) operations
#
# Verify that, using the MI, we can run a simple program and perform basic
# debugging activities like: insert breakpoints, run the program,
# step, next, continue until it ends and, last but not least, quit.
#
# The goal is not to test gdb functionality, which is done by other tests,
# but to verify the correct output response to MI operations.
#

load_lib mi-support.exp

gdb_exit
if [mi_gdb_start] {
    continue
}

set testfile "basics"
set srcfile ${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}
if  { [gdb_compile "${srcdir}/${subdir}/${srcfile}" "${binfile}" executable {debug additional_flags=-DFAKEARGV}] != "" } {
     gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

mi_delete_breakpoints
mi_gdb_reinitialize_dir $srcdir/$subdir
mi_gdb_load ${binfile}

proc test_tbreak_creation_and_listing {} {
    global mi_gdb_prompt
    global srcfile
    global hex

    # Insert some breakpoints and list them
    # Also, disable some so they do not interfere with other tests
    # Tests:
    # -break-insert -t main
    # -break-insert -t basics.c:callee2
    # -break-insert -t basics.c:15
    # -break-insert -t srcfile:6
    # -break-list

    mi_gdb_test "222-break-insert -t main" \
             "222\\^done,bkpt=\{number=\"1\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\"$hex\",func=\"main\",file=\".*basics.c\",line=\"32\",times=\"0\"\}" \
             "break-insert -t operation"

    mi_gdb_test "333-break-insert -t basics.c:callee2" \
             "333\\^done,bkpt=\{number=\"2\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\"$hex\",func=\"callee2\",file=\".*basics.c\",line=\"22\",times=\"0\"\}" \
             "insert temp breakpoint at basics.c:callee2"

    mi_gdb_test "444-break-insert -t basics.c:16" \
             "444\\^done,bkpt=\{number=\"3\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\"$hex\",func=\"callee3\",file=\".*basics.c\",line=\"16\",times=\"0\"\}" \
             "insert temp breakpoint at basics.c:16 (callee3)"

    # Getting the quoting right is tricky.  That is "\"<file>\":6"
    # break "filename":line_number won't work with WDB.
    if ![istarget "*-hp-hpux*"] {
	setup_xfail "*-*-*"
    }

    #On HP-UX we cannot set breakpoint using "filename":line_no, just set as filename:line_no.
    mi_gdb_test "555-break-insert -t ${srcfile}:7" \
             "555\\^done,bkpt=\{number=\"4\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\"$hex\",func=\"callee4\",file=\".*basics.c\",line=\"7\",times=\"0\"\}" \
             "insert temp breakpoint at \"<fullfilename>\":7 (callee4)"

    mi_gdb_test "666-break-list" \
	    "666\\\^done,BreakpointTable=\{nr_rows=\".\",nr_cols=\".\",hdr=\\\[\{width=\".*\",alignment=\".*\",col_name=\"number\",colhdr=\"Num\"\}.*colhdr=\"Type\".*colhdr=\"Disp\".*colhdr=\"Enb\".*colhdr=\"Address\".*colhdr=\"What\".*\\\],body=\\\[bkpt=\{number=\"1\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\"$hex\",func=\"main\",file=\".*basics.c\",line=\"32\",times=\"0\"\}.*\\\]\}" \
                "list of breakpoints"

    mi_gdb_test "777-break-delete" \
	    "777\\^done" \
	    "delete temp breakpoints"
}

proc test_rbreak_creation_and_listing {} {
    global mi_gdb_prompt
    global srcfile
    global hex

    # Insert some breakpoints and list them
    # Also, disable some so they do not interfere with other tests
    # Tests:
    # -break-insert -r main
    # -break-insert -r callee2
    # -break-insert -r callee
    # -break-insert -r .*llee
    # -break-list

    send_gdb "122-break-insert -r main\n"
    if ![istarget "*-hp-hpux*"] {
	setup_xfail "*-*-*"
    }
    gdb_expect {
    -re ".*done,bkpt=\{number=\"5\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"main\",file=\".*basics.c\",line=\"32\",times=\"0\"\}.*\r\n$mi_gdb_prompt$" {
      pass "break-insert -r operation"
      }
    -re ".*$mi_gdb_prompt$" {fail "break-insert -r operation" }
    -re "122\\^done,bkpt=\{number=\"5\",addr=\"$hex\",file=\".*basics.c\",line=\"32\"\}\r\n$mi_gdb_prompt$" \
      {pass "break-insert -r operation"}
    timeout { fail "break-insert -r operation (timeout)"}
    }
    
    if ![istarget "*-hp-hpux*"] {
	setup_xfail "*-*-*"
    }
    send_gdb "133-break-insert -r callee2\n"
    gdb_expect {
      -re ".*done,bkpt=\{number=\"6\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee2\",file=\".*basics.c\",line=\"22\",times=\"0\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp callee2"
      }
      -re ".*done,bkpt=\{number=\"8\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee2\",file=\".*basics.c\",line=\"22\",times=\"0\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp callee2"
      }
      -re "133\\^done,bkpt=\{number=\"6\",addr=\"$hex\",file=\".*basics.c\",line=\"22\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp callee2"
      }
      timeout { fail "insert breakpoint with regexp callee2 (timeout)"}
    }
    
    send_gdb "144-break-insert -r callee\n"
    if ![istarget "*-hp-hpux*"] {
	setup_xfail "*-*-*"
    }
    gdb_expect {
    -re ".*144\\^done,bkpt=\{number=\"7\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee1\",file=\".*basics.c\",line=\"27\",times=\"0\"\}.*\}\r\n$mi_gdb_prompt$" { pass "insert breakpoint with regexp callee"}
    -re ".*144\\^done,bkpt=\{number=\"9\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee1\",file=\".*basics.c\",line=\"27\",times=\"0\"\}.*\}\r\n$mi_gdb_prompt$" { pass "insert breakpoint with regexp callee"}
    -re "144\\^done,bkpt=\{number=\"7\",addr=\"$hex\",file=\".*basics.c\",line=\"27\"\},bkpt=\{number=\"8\",addr=\"$hex\",file=\".*basics.c\",line=\"22\"\},bkpt=\{number=\"9\",addr=\"$hex\",file=\".*basics.c\",line=\"17\"\},bkpt=\{number=\"10\",addr=\"$hex\",file=\".*basics.c\",line=\"8\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp callee"
      }
    timeout { fail "insert breakpoint with regexp callee (timeout)"}
    }
  

    send_gdb "155-break-insert -r \.\*llee\n"
    if ![istarget "*-hp-hpux*"] {
	setup_xfail "*-*-*"
    }
    gdb_expect {
      -re ".155\\^done,bkpt=\{number=\"11\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee1\",file=\".*basics.c\",line=\"27\",times=\"0\"\},bkpt=\{number=\"12\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee2\",file=\".*basics.c\",line=\"22\",times=\"0\"\},bkpt=\{number=\"13\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee3\",file=\".*basics.c\",line=\"17\",times=\"0\"\},bkpt=\{number=\"14\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee4\",file=\".*basics.c\",line=\"8\",times=\"0\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp .*llee"
      }
      -re ".155\\^done,bkpt=\{number=\"13\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee1\",file=\".*basics.c\",line=\"27\",times=\"0\"\},bkpt=\{number=\"14\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee2\",file=\".*basics.c\",line=\"22\",times=\"0\"\},bkpt=\{number=\"15\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee3\",file=\".*basics.c\",line=\"17\",times=\"0\"\},bkpt=\{number=\"16\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"callee4\",file=\".*basics.c\",line=\"8\",times=\"0\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp .*llee"
      }
      -re "155\\^done,bkpt=\{number=\"11\",addr=\"$hex\",file=\".*basics.c\",line=\"27\"\},bkpt=\{number=\"12\",addr=\"$hex\",file=\".*basics.c\",line=\"22\"\},bkpt=\{number=\"13\",addr=\"$hex\",file=\".*basics.c\",line=\"17\"\},bkpt=\{number=\"14\",addr=\"$hex\",file=\".*basics.c\",line=\"8\"\}\r\n$mi_gdb_prompt$" {
      pass "insert breakpoint with regexp .*llee"
      }
      -re ".*$mi_gdb_prompt$" {fail "insert breakpoint with regexp .*llee"}
      timeout { fail "insert breakpoint with regexp .*llee" }
    }
  
    send_gdb "166-break-list\n"
    if ![istarget "*-hp-hpux*"] {
	setup_xfail "*-*-*"
    }
    gdb_expect {
    -re "166\\^done,BreakpointTable=\{nr_rows=\"10\",nr_cols=\"6\",hdr=.\{.*\}.,body=\[bkpt=\{number=\"5\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"main\",file=\".*basics.c\",line=\"32\",times=\"0\"\},.*\}\r\n$mi_gdb_prompt$" {
      pass "list of breakpoints"
    }
    -re "166\\^done,BreakpointTable=\{nr_rows=\"12\",nr_cols=\"6\",hdr=.\{.*\}.,body=\[bkpt=\{number=\"5\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"main\",file=\".*basics.c\",line=\"32\",times=\"0\"\},.*\}\r\n$mi_gdb_prompt$" {
      pass "list of breakpoints"
    }
    -re "166\\^done,BreakpointTable=\{hdr=\{.*\},bkpt=\{number=\"5\",type=\"breakpoint\",disp=\"keep\",enabled=\"y\",addr=\"$hex\",func=\"main\",file=\".*basics.c\",line=\"32\",times=\"0\"\},.*\}\}\r\n$mi_gdb_prompt$" {
      pass "list of breakpoints"
    }
    -re ".*$mi_gdb_prompt$" {fail "list of breakpoints"}
    timeout { fail "list of breakpoints"}
    }
    
    mi_gdb_test "177-break-delete" \
	    "177\\^done" \
	    "delete temp breakpoints"
}

test_tbreak_creation_and_listing
test_rbreak_creation_and_listing

mi_gdb_exit
return 0

# Local variables: 
# change-log-default-name: "ChangeLog-mi"
# End: 
