#
# Test essential Machine interface (MI) operations
#
# Verify the "Instruction stepping mode", using the MI.
#
# The goal is not to test gdb functionality, which is done by other tests,
# but to verify the correct output response to MI operations.
#

load_lib mi-support.exp
set MIFLAGS "-i=mi"

gdb_exit
if [mi_gdb_start] {
    continue
}

if { [istarget "ia64-*-*"] || [istarget "hppa2.0w-hp-hpux*"] } then {
        verbose "PA specific MI test cases..."
        return 0
}

set testfile "car"
set binfile ${objdir}/${subdir}/${testfile}

if ![file exists ${binfile}] {
cd ${srcdir}/${subdir}/car.src
system "make path=${binfile}"
} else {
verbose "Running test points with the already built car executable..."
}

mi_delete_breakpoints
mi_gdb_reinitialize_dir $srcdir/$subdir/car.src
mi_gdb_load ${binfile}

mi_gdb_test "1-gdb-set confirm off" "1\\\^done"
mi_gdb_test "2-gdb-set width 0" "2\\\^done"
mi_gdb_test "3-gdb-set height 0" "3\\\^done"
mi_gdb_test "4-gdb-show prompt" "4\\\^done,value=.*"
mi_gdb_test "5-gdb-set auto-solib-add on" "5\\\^error,msg.*"
#mi_gdb_test "6-environment-cd /usr/home/aroraru/CARNEW" "6\\\^done"
mi_gdb_test "7 info threads" "7\\\^error,msg=\"No stack.\""
#mi_gdb_test "8-data-list-register-names" "8\\\^done,register-names=\\\[flags,r1,rp,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24,r25,r26,dp,ret0,ret1,sp,r31,sar,pcoqh,pcsqh,pcoqt,pcsqt,eiem,iir,isr,ior,ipsw,goto,sr4,sr0,sr1,sr2,sr3,sr5,sr6,sr7,cr0,cr8,cr9,ccr,cr12,cr13,cr24,cr25,cr26,mpsfu_high,mpsfu_low,mpsfu_ovflo,pad,fpsr,fpe1,fpe2,fpe3,fpe4,fpe5,fpe6,fpe7,fr4,fr4R,fr5,fr5R,fr6,fr6R,fr7,fr7R,fr8,fr8R,fr9,fr9R,fr10,fr10R,fr11,fr11R,fr12,fr12R,fr13,fr13R,fr14,fr14R,fr15,fr15R,fr16,fr16R,fr17,fr17R,fr18,fr18R,fr19,fr19R,fr20,fr20R,fr21,fr21R,fr22,fr22R,fr23,fr23R,fr24,fr24R,fr25,fr25R,fr26,fr26R,fr27,fr27R,fr28,fr28R,fr29,fr29R,fr30,fr30R,fr31,fr31R\\\]"
mi_gdb_test "9-break-insert -t main" "9\\\^done,bkpt=\{number=\"1\",type=\"breakpoint\",disp=\"del\",enabled=\"y\",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\",times=\"0\"\}"
#mi_gdb_test "10-exec-run" "10\\\^running.*"
# mi_gdb_test cannot be used for asynchronous commands because there are
    # two prompts involved and this can lead to a race condition.
    # The following is equivalent to a send_gdb "000-exec-run\n"
    mi_run_cmd
    gdb_expect {
        -re "000\\*stopped.*$mi_gdb_prompt$" {
            pass "run to main"
        }
            fail "run to main (timeout)"
        }
mi_gdb_test "11 info program" "11\\\^done"
mi_gdb_test "12 info threads" "12\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "13-stack-info-depth" "13\\\^done,depth=\"1\""
mi_gdb_test "14-stack-list-frames 0 1" "14\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
#mi_gdb_test "15-data-list-changed-registers" "15\\\^done,changed-registers=\\\[0,1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,48,49,50,51,52,53,54,57,58,60,61,63,64,72,73,74,77,79,80,81,82,84,86,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,111,112,113,114,115,116,119,120,122,123,125,126,127\\\]"
mi_gdb_test "16 info sharedlibrary" "16\\\^done"
mi_gdb_test "17-stack-list-arguments 0 0 0" "17\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "18-stack-list-locals 0" "18\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
mi_gdb_test "19-var-create - * bala1" "19\\\^done,name=\"var1\",numchild=\"0\",type=\"int\""
mi_gdb_test "21-var-create - * balas2" "21\\\^done,name=\"var2\",numchild=\"0\",type=\"int\""
mi_gdb_test "22-var-create - * mycar" "22\\\^done,name=\"var3\",numchild=\"0\",type=\"class Ferrari\""
mi_gdb_test "23-var-create - * abcd" "23\\\^done,name=\"var4\",numchild=\"2\",type=\"int \\\[2\\\]\""
mi_gdb_test "24-var-evaluate-expression var1" "24\\\^done,value=\"1\""
mi_gdb_test "25-var-evaluate-expression var3" "25\\\^done,value=\"\{<class Car> = \{registrationno = 0, modelno = 0, currentspeed = 0, engine = \{isrunning = false\}, wheel = \{\{airlevel = 0\}, \{airlevel = 0\}, \{airlevel = 0\}, \{airlevel = 0\}\}, left = \{window = \{isclosed = .*\}\}, right = \{window = \{isclosed = .*\}\}\}, <No data fields>\}\""
mi_gdb_test "26-var-evaluate-expression var2" "26\\\^done,value=\".*\""
#mi_gdb_test "27-exec-next-instruction 1" "27\\\^running.*"
send_gdb "27-exec-next-instruction\n"
gdb_expect {
    -re "27\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "27\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "28 info threads" ".*28\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "29-stack-info-depth" "29\\\^done,depth=\"1\""
mi_gdb_test "30-stack-list-frames 0 1" "30\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
mi_gdb_test "31-var-update var1" "31\\\^done,changelist=\{\}"
mi_gdb_test "32-var-update var2" "32\\\^done,changelist=\{\}"
mi_gdb_test "33-var-update var3" "33\\\^done,changelist=\{\}"
mi_gdb_test "34-var-update var4" "34\\\^done,changelist=\{\}"
#mi_gdb_test "35-data-list-changed-registers" "35\\\^done,changed-registers=\\\[1,33,35,38,39,40\\\]"
mi_gdb_test "36 info sharedlibrary" "36\\\^done"
mi_gdb_test "37-stack-list-arguments 0 0 0" "37\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "38-stack-list-locals 0" "38\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "39-exec-next-instruction 1" "39\\\^running.*"
send_gdb "39-exec-next-instruction 1\n"
gdb_expect {
    -re "39\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "39\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "40 info threads" ".*40\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "41-stack-info-depth" "41\\\^done,depth=\"1\""
mi_gdb_test "42-stack-list-frames 0 1" "42\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
mi_gdb_test "43-var-update var1" "43\\\^done,changelist=\{\}"
mi_gdb_test "44-var-update var2" "44\\\^done,changelist=\{\}"
mi_gdb_test "45-var-update var3" "45\\\^done,changelist=\{\}"
mi_gdb_test "46-var-update var4" "46\\\^done,changelist=\{\}"
#mi_gdb_test "47-data-list-changed-registers" "47\\\^done,changed-registers=\\\[26,33,35\\\]"
mi_gdb_test "48 info sharedlibrary" "48\\\^done"
mi_gdb_test "49-stack-list-arguments 0 0 0" "49\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "50-stack-list-locals 0" "50\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "51-exec-next-instruction 1" "51\\\^running.*"
send_gdb "51-exec-next-instruction 1\n"
gdb_expect {
    -re "51\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "51\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "52 info threads" ".*52\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "53-stack-info-depth" "53\\\^done,depth=\"1\""
mi_gdb_test "54-stack-list-frames 0 1" "54\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
mi_gdb_test "55-var-update var1" "55\\\^done,changelist=\{\}"
mi_gdb_test "56-var-update var2" "56\\\^done,changelist=\{\}"
mi_gdb_test "57-var-update var3" "57\\\^done,changelist=\{\}"
mi_gdb_test "58-var-update var4" "58\\\^done,changelist=\{\}"
#mi_gdb_test "59-data-list-changed-registers" "59\\\^done,changed-registers=\\\[1,33,35,39\\\]"
mi_gdb_test "60 info sharedlibrary" "60\\\^done"
mi_gdb_test "61-stack-list-arguments 0 0 0" "61\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "62-stack-list-locals 0" "62\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "63-exec-next-instruction 1" "63\\\^running.*"
send_gdb "63-exec-next-instruction 1\n"
gdb_expect {
    -re "63\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "63\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "64 info threads" ".*64\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"24\"\}"
mi_gdb_test "65-stack-info-depth" "65\\\^done,depth=\"1\""
mi_gdb_test "66-stack-list-frames 0 1" "66\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"24\"\}\\\]"
mi_gdb_test "67-var-update var1" "67\\\^done,changelist=\{\}"
mi_gdb_test "68-var-update var2" "68\\\^done,changelist=\{\}"
mi_gdb_test "69-var-update var3" "69\\\^done,changelist=\{\}"
mi_gdb_test "70-var-update var4" "70\\\^done,changelist=\{\}"
#mi_gdb_test "71-data-list-changed-registers" "71\\\^done,changed-registers=\\\[0,2,33,35,41\\\]"
mi_gdb_test "72 info sharedlibrary" "72\\\^done"
mi_gdb_test "73-stack-list-arguments 0 0 0" "73\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "74-stack-list-locals 0" "74\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "75-exec-next 1" "75\\\^running.*"
send_gdb "75-exec-next 1\n"
gdb_expect {
    -re "75\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "75\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "76 info threads" ".*76\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"25\"\}"
mi_gdb_test "77-stack-info-depth" "77\\\^done,depth=\"1\""
mi_gdb_test "78-stack-list-frames 0 1" "78\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"25\"\}\\\]"
mi_gdb_test "79-var-update var1" "79\\\^done,changelist=\{\}"
mi_gdb_test "80-var-update var2" "80\\\^done,changelist=\{\}"
mi_gdb_test "81-var-update var3" "81\\\^done,changelist=\{\}"
mi_gdb_test "82-var-update var4" "82\\\^done,changelist=\{\}"
#mi_gdb_test "83-data-list-changed-registers" "83\\\^done,changed-registers=\\\[0,1,2,20,21,22,23,24,25,26,28,29,31,33,35,41,45\\\]"
mi_gdb_test "84 info sharedlibrary" "84\\\^done"
mi_gdb_test "85-stack-list-arguments 0 0 0" "85\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "86-stack-list-locals 0" "86\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "87-exec-next-instruction 1" "87\\\^running.*"
send_gdb "87-exec-next-instruction 1\n"
gdb_expect {
    -re "87\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "87\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "88 info threads" ".*88\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"25\"\}"
mi_gdb_test "89-stack-info-depth" "89\\\^done,depth=\"1\""
mi_gdb_test "90-stack-list-frames 0 1" "90\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"25\"\}\\\]"
mi_gdb_test "91-var-update var1" "91\\\^done,changelist=\{\}"
mi_gdb_test "92-var-update var2" "92\\\^done,changelist=\{\}"
mi_gdb_test "93-var-update var3" "93\\\^done,changelist=\{\}"
mi_gdb_test "94-var-update var4" "94\\\^done,changelist=\{\}"
#mi_gdb_test "95-data-list-changed-registers" "95\\\^done,changed-registers=\\\[1,33,35,39\\\]"
mi_gdb_test "96 info sharedlibrary" "96\\\^done"
mi_gdb_test "97-stack-list-arguments 0 0 0" "97\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "98-stack-list-locals 0" "98\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "99-exec-next-instruction 1" "99\\\^running.*"
send_gdb "99-exec-next-instruction 1\n"
gdb_expect {
    -re "99\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "99\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "100 info threads" ".*100\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"25\"\}"
mi_gdb_test "101-stack-info-depth" "101\\\^done,depth=\"1\""
mi_gdb_test "102-stack-list-frames 0 1" "102\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"25\"\}\\\]"
mi_gdb_test "103-var-update var1" "103\\\^done,changelist=\{\}"
mi_gdb_test "104-var-update var2" "104\\\^done,changelist=\{\}"
mi_gdb_test "105-var-update var3" "105\\\^done,changelist=\{\}"
mi_gdb_test "106-var-update var4" "106\\\^done,changelist=\{\}"
#mi_gdb_test "107-data-list-changed-registers" "107\\\^done,changed-registers=\\\[26,33,35\\\]"
mi_gdb_test "108 info sharedlibrary" "108\\\^done"
mi_gdb_test "109-stack-list-arguments 0 0 0" "109\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "110-stack-list-locals 0" "110\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "111-exec-next-instruction 1" "111\\\^running.*"
send_gdb "111-exec-next-instruction 1\n"
gdb_expect {
    -re "111\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "111\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "112 info threads" ".*112\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"25\"\}"
mi_gdb_test "113-stack-info-depth" "113\\\^done,depth=\"1\""
mi_gdb_test "114-stack-list-frames 0 1" "114\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"25\"\}\\\]"
mi_gdb_test "115-var-update var1" "115\\\^done,changelist=\{\}"
mi_gdb_test "116-var-update var2" "116\\\^done,changelist=\{\}"
mi_gdb_test "117-var-update var3" "117\\\^done,changelist=\{\}"
mi_gdb_test "118-var-update var4" "118\\\^done,changelist=\{\}"
#mi_gdb_test "119-data-list-changed-registers" "119\\\^done,changed-registers=\\\[1,33,35\\\]"
mi_gdb_test "120 info sharedlibrary" "120\\\^done"
mi_gdb_test "121-stack-list-arguments 0 0 0" "121\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "122-stack-list-locals 0" "122\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "123-exec-next-instruction 1" "123\\\^running.*"
send_gdb "123-exec-next-instruction 1\n"
gdb_expect {
    -re "123\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "123\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "124 info threads" ".*124\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"25\"\}"
mi_gdb_test "125-stack-info-depth" "125\\\^done,depth=\"1\""
mi_gdb_test "126-stack-list-frames 0 1" "126\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"25\"\}\\\]"
mi_gdb_test "127-var-update var1" "127\\\^done,changelist=\{\}"
mi_gdb_test "128-var-update var2" "128\\\^done,changelist=\{\}"
mi_gdb_test "129-var-update var3" "129\\\^done,changelist=\{\}"
mi_gdb_test "130-var-update var4" "130\\\^done,changelist=\{\}"
#mi_gdb_test "131-data-list-changed-registers" "131\\\^done,changed-registers=\\\[0,2,33,35,39,41\\\]"
mi_gdb_test "132 info sharedlibrary" "132\\\^done"
mi_gdb_test "133-stack-list-arguments 0 0 0" "133\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "134-stack-list-locals 0" "134\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "135-exec-next-instruction 1" "135\\\^running.*"
send_gdb "135-exec-next-instruction 1\n"
gdb_expect {
    -re "135\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "135\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "136 info threads" ".*136\\\^done,frame=\{func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"26\"\}"
mi_gdb_test "137-stack-info-depth" "137\\\^done,depth=\"1\""
mi_gdb_test "138-stack-list-frames 0 1" "138\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"26\"\}\\\]"
mi_gdb_test "139-var-update var1" "139\\\^done,changelist=\{\}"
mi_gdb_test "140-var-update var2" "140\\\^done,changelist=\{\}"
mi_gdb_test "141-var-update var3" "141\\\^done,changelist=\{\}"
mi_gdb_test "142-var-update var4" "142\\\^done,changelist=\{\}"
#mi_gdb_test "143-data-list-changed-registers" "143\\\^done,changed-registers=\\\[0,1,2,26,33,35,41\\\]"
mi_gdb_test "144 info sharedlibrary" "144\\\^done"
mi_gdb_test "145-stack-list-arguments 0 0 0" "145\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "146-stack-list-locals 0" "146\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "147-exec-next-instruction 1" "147\\\^running.*"
send_gdb "147-exec-next-instruction 1\n"
gdb_expect {
    -re "147\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "147\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "148 info threads" ".*148\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"26\"\}"
mi_gdb_test "149-stack-info-depth" "149\\\^done,depth=\"1\""
mi_gdb_test "150-stack-list-frames 0 1" "150\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"26\"\}\\\]"
mi_gdb_test "151-var-update var1" "151\\\^done,changelist=\{\}"
mi_gdb_test "152-var-update var2" "152\\\^done,changelist=\{\}"
mi_gdb_test "153-var-update var3" "153\\\^done,changelist=\{\}"
mi_gdb_test "154-var-update var4" "154\\\^done,changelist=\{\}"
#mi_gdb_test "155-data-list-changed-registers" "155\\\^done,changed-registers=\\\[1,33,35,39\\\]"
mi_gdb_test "156 info sharedlibrary" "156\\\^done"
mi_gdb_test "157-stack-list-arguments 0 0 0" "157\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "158-stack-list-locals 0" "158\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "159-exec-next-instruction 1" "159\\\^running.*"
send_gdb "159-exec-next-instruction 1\n"
gdb_expect {
    -re "159\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "159\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "160 info threads" ".*160\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"26\"\}"
mi_gdb_test "161-stack-info-depth" "161\\\^done,depth=\"1\""
mi_gdb_test "162-stack-list-frames 0 1" "162\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"26\"\}\\\]"
mi_gdb_test "163-var-update var1" "163\\\^done,changelist=\{\}"
mi_gdb_test "164-var-update var2" "164\\\^done,changelist=\{\}"
mi_gdb_test "165-var-update var3" "165\\\^done,changelist=\{\}"
mi_gdb_test "166-var-update var4" "166\\\^done,changelist=\{\}"
#mi_gdb_test "167-data-list-changed-registers" "167\\\^done,changed-registers=\\\[26,33,35\\\]"
mi_gdb_test "168 info sharedlibrary" "168\\\^done"
mi_gdb_test "169-stack-list-arguments 0 0 0" "169\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "170-stack-list-locals 0" "170\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"
#mi_gdb_test "171-exec-next-instruction 1" "171\\\^running.*"
send_gdb "171-exec-next-instruction 1\n"
gdb_expect {
    -re "171\\^running\r\n$mi_gdb_prompt" {
        pass "Started step over main"
    }
    timeout {
        fail "Started step over main(timeout)"
    }
}

gdb_expect {
    -re "171\\*stopped.*$mi_gdb_prompt$" {
        pass "Finished step over main"
    }
    timeout {
        fail "Finished step over main (timeout)"
    }
}
mi_gdb_test "172 info threads" ".*172\\\^done,frame=\{addr=\".*\",func=\"main\",args=\\\[\{name=\"bala1\"\},\{name=\"balas2\"\}\\\],file=\"cartest.cpp\",line=\"26\"\}"
mi_gdb_test "173-stack-info-depth" "173\\\^done,depth=\"1\""
mi_gdb_test "174-stack-list-frames 0 1" "174\\\^done,stack=\\\[frame=\{level=\"0 \",addr=\".*\",func=\"main\",file=\"cartest.cpp\",line=\"26\"\}\\\]"
mi_gdb_test "175-var-update var1" "175\\\^done,changelist=\{\}"
mi_gdb_test "176-var-update var2" "176\\\^done,changelist=\{\}"
mi_gdb_test "177-var-update var3" "177\\\^done,changelist=\{\}"
mi_gdb_test "178-var-update var4" "178\\\^done,changelist=\{\}"
#mi_gdb_test "179-data-list-changed-registers" "179\\\^done,changed-registers=\\\[1,33,35,39\\\]"
mi_gdb_test "180 info sharedlibrary" "180\\\^done"
mi_gdb_test "181-stack-list-arguments 0 0 0" "181\\\^done,stack-args=\\\[frame=\{level=\"0\",args=\\\[name=\"bala1\",name=\"balas2\"\\\]\}\\\]"
mi_gdb_test "182-stack-list-locals 0" "182\\\^done,locals=\\\[name=\"mycar\",name=\"abcd\"\\\]"

send_gdb "183 jump *0x40bc"
gdb_expect {
  -re  ".*" \
            { pass "yes" }
  timeout   { fail "break printf (timeout)" }
}

mi_gdb_test "184-data-evaluate-expression \$_exitcode" ".*"

mi_gdb_exit
return 0
