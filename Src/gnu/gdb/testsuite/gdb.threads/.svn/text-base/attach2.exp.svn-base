# attach.exp -- Expect script to test attaching to a threaded pgm
# Copyright (C) 1992 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

# Please email any bugs, comments, and/or additions to this file to:
# bug-gdb@prep.ai.mit.edu

if $tracelevel then {
    strace $tracelevel
}

if { [istarget "*gambit*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}
if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64*-hp*-*"] } {
    verbose "HPUX thread test ignored for non-hppa or pre-HP/UX-10.30 targets."
    return 0
}

set testfile quicksort
set srcfile ${srcdir}/${subdir}/${testfile}.c
set binfile ${objdir}/${subdir}/${testfile}

if [get_compiler_info ${binfile}] {
    return -1
}

set oldtimeout $timeout
set oldverbose $verbose

# To build the executable we need to link against the thread library.
#
if { $hp_cc_compiler } {
    set additional_flags "additional_flags=-Ae"
} else {
    set additional_flags ""
}

if { [gdb_compile "${srcdir}/${subdir}/${testfile}.c" "${binfile}" executable [list debug $additional_flags ldflags=-lpthread]] != "" } {
    gdb_suppress_entire_file "Testcase compile failed, so all tests in this file will automatically fail."
}

# Because we can't attach over nfs, copy binfile to /tmp/${binfile}.${pid}
# and replace binfile with a symbolic link

set pid [pid]
catch "exec rm -f /tmp/attach2.${pid}"
exec /bin/cp -f ${binfile} /tmp/attach2.${pid}
catch "exec rm -f ${binfile}"
exec ln -s /tmp/attach2.${pid} ${binfile}

# Thread stuff is _slow_; prepare for long waits.
#
# Further, this test has some "null" lines designed
# to consume output from gdb that was too late to be
# matched (sequence is "gdb_test" sends; timeout and
# on to next send; result finally comes in; mismatch).
#
# The null command is 'gdb_test "p \$pc" ".*" ""'
# NOTE: this command undoes any up/down stuff!
#
proc pre_timeout { how_long } {
    global timeout

    set timeout [expr "$timeout + $how_long"]
}

proc post_timeout {} {
    global timeout
    global oldtimeout

    set timeout $oldtimeout
    gdb_test "p \$pc" ".*" ""
}

# We used to wait 5 seconds , but tiamat is faster than
# hydra...or is it that the OS allocates time differently(?).
#
set delay 5
if { ![istarget "hppa*-*-hpux11.*"] && ![istarget "ia64-hp*-*"] } {
    set delay 45
}

gdb_exit
gdb_start
gdb_reinitialize_dir $srcdir/$subdir

# Start the application running and get its pid.
# Then we wait for it to get started and attach.
# 
set testpid [eval exec $binfile 1 &]
exec sleep $delay

# Now attach to the file.
#
pre_timeout 100
gdb_test "file $binfile" ".*" "Force switch to gdb64 if necessary."
gdb_test "attach $testpid" ".*Attaching to.*process.*" "attach to target"
post_timeout

# Wait for things to quiesce.
#
exec sleep 0

# Look at the threads.
#
pre_timeout 200
send_gdb "info thread\n"
gdb_expect {
  -re ".*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*$gdb_prompt $" \
	     { pass "info threads after attach (mxn)" }
  -re ".*7 system.*6 system.*5 system.*4 system.*3 system.*2 system.*1 system.*thread.*$gdb_prompt $" {
	  pass "info threads after attach (non-mxn)"
  }
  -re ".*$gdb_prompt $" { fail "info threads after attach - no match" }
  timeout { fail "info threads after attach (timeout)" }
}
post_timeout

gdb_test "thread 1" ""

# Look at the threads again, to see if we flag the last thread that
# had a ttrace event with a '>', ala' LaDebug (JAGaf47331).
#
pre_timeout 200
send_gdb "info thread\n"
gdb_expect {
  -re ".*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*$gdb_prompt $" {
	  send_gdb "info thread\n"
	  gdb_expect {
	    -re ".*\\*   1 system thread.*$gdb_prompt $" { pass "2nd info threads after attach (mxn)" }
	    -re ".*$gdb_prompt $" { fail "2nd info threads after attach" }
	    timeout { fail "2nd info threads after attach (timeout)" }
	  }
  }
  -re ".*7 system.*6 system.*5 system.*4 system.*3 system.*2 system.*\\*   1 system.*thread.*$gdb_prompt $" {
	  pass "2nd info threads after attach (non-mxn)"
  }
  -re ".*$gdb_prompt $" { fail "2nd info threads after attach - no match" }
  timeout { fail "2nd info threads after attach (timeout)" }
}
post_timeout

send_gdb "bt\n" 

set do_return 0
set do_go_to_118 0
pre_timeout 400
gdb_expect {
    -re ".*sleep.*work_init.*main.*$gdb_prompt $"   { 
        pass "at expected location" 
    }
    -re ".*drand48.*$gdb_prompt $" {
        set do_go_to_118 1
    }
    -re ".*pthread_mutex_lock.*$gdb_prompt $" {
        set do_go_to_118 1
    }
    -re ".*pthread_mutex_unlock.*$gdb_prompt $" {
        set do_go_to_118 1
    }
    -re ".*main.*$gdb_prompt $" {
        set do_go_to_118 1
    }
    -re ".*No stack.*$gdb_prompt $" {
        fail "Failed attach, change wait amount down, rest would fail"
        set do_return 1
    }
    -re ".*$gdb_prompt $" {
        # Who knows?
        #
	fail "Couldn't unwind after attach"
        set do_go_to_118 1
    }
    timeout { 
        set do_return 1
        fail "timeout on bt, avoiding rest of test" 
    }
}
post_timeout

# Too late; just give up.
#
if { $do_return } {
    set timeout $oldtimeout
    set verbose $oldverbose
    # Make sure we don't leave a process around to confuse
    # the next test run (and prevent the rm by keeping
    # the text file busy).
    catch "exec kill -9 ${testpid}"
    catch "exec rm -f ${binfile} /tmp/attach2.${pid}"
    return 0
}

# Maybe too early--set a temp break and continue.
# We have to set this on both paths, so that we can
# know what numbers breakpoints will be.
#
gdb_test "tb 118" ".*Breakpoint 1.*118.*"
if { $do_go_to_118 } {
    pre_timeout 100
    send_gdb "c\n"
    gdb_expect {
        -re ".*at.*118.*118.*$gdb_prompt $" {
            # Ok, just keep going
        }
        -re ".*Program exited.*$gdb_prompt $" {
            fail "Attached too late, set wait amount downwards"
            set timeout $oldtimeout
            set verbose $oldverbose
            return 0
        }
        -re ".*$gdb_prompt $" {
            fail "Unexpected result on attach" 
            set timeout $oldtimeout
            set verbose $oldverbose
            return 0
        }
        timeout { 
            fail "timeout on continue " 
        }
    }
    post_timeout
}
gdb_test "delete 1" ".*" "delete bp 1 if it is still there."

# Look at the threads.
#

pre_timeout 400
send_gdb "info thread\n"
gdb_expect {
  -re ".*thread.*thread.*thread.*thread.*thread.*thread.*thread.*thread.*thread.*$gdb_prompt $" {
      pass "first info threads (mxn)"
  }
  -re ".*7.*6.*5.*4.*3.*2.*\\\*   1.*thread.*$gdb_prompt $" {
      pass "first info threads (non-mxn)"
  }
  -re ".*$gdb_prompt $" { fail "first info threads" }
  timeout { fail "first info threads (timeout)" }
}
post_timeout

# We expect to be inside the "sleep" call, so check that.
#
if { [expr "!$do_go_to_118"] } {
    gdb_test "up" ".*\#1.*(nanosleep|_sigtimedwait|sigtimedwait).*" "up 1" 
    gdb_test "up" ".*\#2.*sleep.*"     "up 2" 
    pre_timeout 100
    gdb_test "up" ".*\#3.*work_init.*$testfile.*c:11\[48\].*sleep.*" "up 3" 
    post_timeout
} else {
    send_user "Skipped three tests\n"
}

# Get out of that call.
#
gdb_test "b 120" ".*Breakpoint 2.*120.*" "set bp"
pre_timeout 1000
send_gdb "c\n"
gdb_expect {
  -re ".*Breakpoint 2.*at.*120.*$gdb_prompt $" { pass "hit bp" }
  -re ".*SIGTRAP.*$gdb_prompt $" { 
    send_gdb "c\n"
    exp_continue
  }
  -re ".*$gdb_prompt $" { fail "hit bp" }
  timeout { fail "hit bp (timeout)" }
}
post_timeout

# Look at the threads.
#
pre_timeout 200
send_gdb "info thread\n"
gdb_expect {
  -re ".*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*$gdb_prompt $" {
	  send_gdb "info thread\n"
	  gdb_expect {
	    -re ".*$testfile.*c.*120.*$gdb_prompt $" { pass "2nd info threads (mxn)" }
	    -re ".*$gdb_prompt $" { fail "2nd info threads" }
	    timeout { fail "2nd info threads (timeout)" }
	  }
  }
  -re ".*7 system.*6 system.*5 system.*4 system.*3 system.*2 system.*1 system.*thread.*$testfile.*c*120.*$gdb_prompt $" {
	  pass "2nd info threads (non-mxn)"
  }
  -re ".*$gdb_prompt $" { fail "2nd info threads - no match" }
  timeout { fail "2nd info threads (timeout)" }
}
post_timeout

# Do some more stuff, to make sure we can
#
gdb_test "thread 5" ".*Switching to.*thread 5.*" "switch thread"

# Look at the threads again, to see if we flag the last thread that
# had a ttrace event with a '>', ala' LaDebug (JAGaf47331).
#
pre_timeout 200
send_gdb "info thread\n"
gdb_expect {
  -re ".*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*system thread.*$gdb_prompt $" {
	  send_gdb "info thread\n"
	  gdb_expect {
	    -re ".*\>   1 system thread.*$testfile.*c.*120.*$gdb_prompt $" { pass "2nd info threads (mxn)" }
	    -re ".*$gdb_prompt $" { fail "2nd info threads" }
	    timeout { fail "2nd info threads (timeout)" }
	  }
  }
  -re ".*7 system.*6 system.*\\*   5 system.*4 system.*3 system.*2 system.*>   1 system.*thread.*$testfile.*c*120.*$gdb_prompt $" {
	  pass "2nd info threads (non-mxn)"
  }
  -re ".*$gdb_prompt $" { fail "2nd info threads - no match" }
  timeout { fail "2nd info threads (timeout)" }
}
post_timeout

set count 10
send_gdb "up\n"
gdb_expect {
  -re ".*worker.*$gdb_prompt $" { pass "up to worker" }
  -re ".*$gdb_prompt $" {
    if { $count>=1 } {
      set count [expr "$count - 1"]
	send_gdb "up\n"
	exp_continue
    } else {
	fail "up to worker - Did not reach worker after 10 ups"
    }
  }
  timeout { fail "up to worker (timeout)" }
}

send_gdb "up\n"
gdb_expect {
  -re ".*Initial.*cannot go up.*$gdb_prompt $" { pass "found thread base"}
  -re ".*unknown_procedure.*$gdb_prompt $" { pass "found thread base"}
  -re ".*pthread_.*$gdb_prompt $" { pass "found thread base"}
  -re ".*pthread_unbound_body.*$gdb_prompt $" {
      send_gdb "up\n"
      gdb_expect {
	-re ".*Initial.*cannot go up.*$gdb_prompt $" { pass "found thread base"}
	-re ".*$gdb_prompt $" { fail "found thread base"}
	timeout { fail "found thread base (timeout)" }
      }
  }
  -re ".*$gdb_prompt $" { fail "found thread base"}
  timeout { fail "found thread base (timeout)" }
}

gdb_test "b 145 thr 5" ".*Breakpoint 3.*145.*"             "thread-specific bp"
gdb_test "i b"         ".*2.*breakpoint.*at.*120.*3.*breakpoint.*at.*145 .*thread 5.*" "show thread-specific bp"
gdb_test "del 2" ".*"

send_gdb "c\n"
pre_timeout 300
gdb_expect {
  -re ".*Breakpoint 3.*145.*$gdb_prompt $" {
    pass "hit thread-specific bp"
  }
  -re ".*SIGTRAP.*$gdb_prompt $" {
    send_gdb "c\n"
    exp_continue
  }
  -re ".*$gdb_prompt $" { fail "hit thread-specific bp" }
  timeout { fail "hit thread-specific bp (timeout)" }
}
gdb_test "i th" ".*\\\* *5.*145.*" "at correct thread"

gdb_test "n" ".*146.*" "next from thread-specific bp"
post_timeout

gdb_test "d 3" ".*"
gdb_test "c"   ".*Program exited normally\..*" "run to finish"

# Done!
#
gdb_exit

set timeout $oldtimeout
set verbose $oldverbose

# Cleanup the file placed in /tmp and the symlink
#
# Make sure we don't leave a process around to confuse
# the next test run (and prevent the rm by keeping
# the text file busy).
catch "exec kill -9 ${testpid}"
catch "exec rm -f ${binfile} /tmp/attach2.${pid}"

return 0


