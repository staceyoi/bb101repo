 #include <pthread.h>
 #include <stdio.h>
 #include <unistd.h>
 #include <stdlib.h>

 void * call1(void * );
 void f()
 {
  /*printf("calling function f()\n"); */
  system("/bin/ls -l > /dev/null");
 }

 void * call1(void * t)
 {
   while (1) {
     sleep(5);
     f();
   }
   return 0;
 }

 int main()
 {
  pthread_t l_thr;
  pthread_attr_t l_thrAttr;

  unsigned int l_action = 40;  /* max iterations if the main wait loop */
  size_t l_stackSize = 524288;
  size_t l_guardSize = 303104;  // 74 pages of size 4096
  alarm(900);  /* backup in case the process runs away */
  int l_status = pthread_attr_init(&l_thrAttr);
  pthread_attr_setstacksize(&l_thrAttr, l_stackSize);
  pthread_attr_setguardsize(&l_thrAttr, l_guardSize);

  pthread_create(&l_thr, &l_thrAttr, call1, 0);

  int l_interactive=1;
  while (l_interactive) {
     sleep(20);
     l_action--;
     if (l_action == 0) 
        l_interactive = 0;
  }

  return 0;
 }

