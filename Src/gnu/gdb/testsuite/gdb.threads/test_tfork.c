#include <stdio.h>
#include <signal.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

static const char * catched = "Signal Catched!\n";

static void SigHandler (int sig) {
  _exit (8); 
}

static void  * DoNothing (void * arg) {
  long thread_no = *((long *) arg);
  double a, b, c;
  b = 1.0e5;
  while (b > 0.9) {
//    printf ("sqrt going\n");
    fflush (NULL);
    sleep (1);
    c = sqrt (b);
    b = c;
  }
  printf ("Thread %d ended!\n", thread_no);
  return NULL;
}

static void * KeepPopen (void * arg) {
  FILE * pipe;
  int i;
  for (i = 0; i < 5; i++) {
    printf ("Begin the %d popen\n", i);
    pipe = popen ("/usr/sbin/ping 15.70.169.223 -n 2", "r");
    if (pipe == NULL) {
      perror ("Failed to open pipe");
      return (void *) 2;
    }
    char echoLine[512];
    while (fscanf (pipe, "%s", echoLine) != EOF)
{
//      fprintf (stdout,"%s", echoLine);
}
    printf ("\n");
    pclose (pipe);
    sleep (1);
  }
  return NULL;
}
static void * TestBreak (void * arg) {
  printf ("Read from keyboard: ");
  long num;
  scanf ("%d", &num);
  printf ("Value input: %d\n", num);
  printf ("Value input: %d\n", num);
  printf ("Value input: %d\n", num);
  return NULL;
}

int main (int argc, char * argv[]) {
  struct sigaction myAction;
  
  memset (&myAction, 0, sizeof (struct sigaction));

  sigemptyset (&myAction.sa_mask);
  myAction.sa_handler = SigHandler;

  if (sigaction (10, &myAction, NULL) == -1) {
    perror ("Failed sigaction");
    return 4;
  }
  if (sigaction (SIGFPE, &myAction, NULL) == -1) {
    perror ("Failed sigaction");
    return 4;
  }

  pthread_t threadId[10];
  int i;


  for (i = 0; i < 10; i++) {
    if (pthread_create (&threadId[i], NULL, DoNothing, &i) != 0) {
      printf ("Failed to create thread!\n");
    };
  }

  pthread_t brkThread, popenThread;
  pthread_create (&brkThread, NULL, TestBreak, NULL);
  sleep (5);
  pthread_create (&popenThread, NULL, KeepPopen, NULL);
  

  fflush (NULL);
  
  long status;
  void * ptrStatus = &status;

  printf ("Wait for the threads\n");
  for (i = 0; i < 10; i++)
    pthread_join (threadId[i], &ptrStatus);
  pthread_join (brkThread, &ptrStatus);
  pthread_join (popenThread, &ptrStatus);

  printf ("End!\n");
  
  return 0;
}
