/* BeginSourceFile thr-stg.c
   Creates the threads, sets the tls variables so that we can test the gdb's
   access of tls variables.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#define TRUE 1
#define FALSE 0
#define OUTER_LOOP_COUNT 3
#define N_THREADS 3
#define MAX_LOCAL_VAL 40

/* Thread-local storage.
 */

/* globals */
__thread int thread_local1;
__thread int thread_local2;
__thread int thread_local3;

/* file statics */
static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

/* imported variables from thr-stg1.c */
extern __thread int import1_thread_local1;
extern __thread int import1_thread_local2;
extern __thread int import1_thread_local3;

/* imported variables from thr-stg11.c */
extern __thread int import11_thread_local1;
extern __thread int import11_thread_local2;
extern __thread int import11_thread_local3;

/* imported variables from thr-stg2.c */
extern __thread int import2_thread_local1;
extern __thread int import2_thread_local2;
extern __thread int import2_thread_local3;

/* imported variables from thr-stg21.c */
extern __thread int import21_thread_local1;
extern __thread int import21_thread_local2;
extern __thread int import21_thread_local3;

/* Check the results of thread-local storage.
 */

/* for globals */
int val_debugger_saw1[ N_THREADS ];
int val_debugger_saw2[ N_THREADS ];
int val_debugger_saw3[ N_THREADS ];
/* for statics */
int static_val_debugger_saw1[ N_THREADS ];
int static_val_debugger_saw2[ N_THREADS ];
int static_val_debugger_saw3[ N_THREADS ];
/* for imported variables from thr-stg1.c */
int import1_val_debugger_saw1[ N_THREADS ];
int import1_val_debugger_saw2[ N_THREADS ];
int import1_val_debugger_saw3[ N_THREADS ];
/* for imported variables from thr-stg11.c */
int imp11_val_deb_saw1[ N_THREADS ];
int imp11_val_deb_saw2[ N_THREADS ];
int imp11_val_deb_saw3[ N_THREADS ];
/* for imported variables from thr-stg2.c */
int import2_val_debugger_saw1[ N_THREADS ];
int import2_val_debugger_saw2[ N_THREADS ];
int import2_val_debugger_saw3[ N_THREADS ];
/* for imported variables from thr-stg21.c */
int imp21_val_deb_saw1[ N_THREADS ];
int imp21_val_deb_saw2[ N_THREADS ];
int imp21_val_deb_saw3[ N_THREADS ];

/* function in thr-stgb.c */
extern void* myspinb(void*);
extern void do_pass1(int);
extern void do_pass11(int);
extern void do_pass2(int);
extern void do_pass21(int);
extern void do_passb(int);

/* Routine for each thread to run,  assigns thread locals and prints them out.
 */
void *spin( void *vp )
{
    int me = (long) vp;
    int i;
    
    thread_local1 = me + 10;
    thread_local2 = me + 20;
    thread_local3 = me + 30;

    static_thread_local1 = me + 40;
    static_thread_local2 = me + 50;
    static_thread_local3 = me + 60;

    import1_thread_local1 = me + 73;
    import1_thread_local2 = me + 83;
    import1_thread_local3 = me + 93;

    import11_thread_local1 = me + 173;
    import11_thread_local2 = me + 183;
    import11_thread_local3 = me + 193;

    import2_thread_local1 = me + 76;
    import2_thread_local2 = me + 86;
    import2_thread_local3 = me + 96;

    import21_thread_local1 = me + 176;
    import21_thread_local2 = me + 186;
    import21_thread_local3 = me + 196;
    
    myspinb(vp);

    printf( "== Thread %d, thread_local1 is %d, thread_local2 is %d, thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d\n",
            (long) vp, thread_local1, thread_local2, thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3);

   return (void *)0;
}

void
do_pass( int pass )
{
    int i;
    pthread_t t[ N_THREADS ];
    int err;

    for( i = 0; i < N_THREADS; i++) {
        val_debugger_saw1[i] = 0;
        val_debugger_saw2[i] = 0;
        val_debugger_saw3[i] = 0;
	
        static_val_debugger_saw1[i] = 0;
        static_val_debugger_saw2[i] = 0;
        static_val_debugger_saw3[i] = 0;

 	import1_val_debugger_saw1[i] = 0;
	import1_val_debugger_saw2[i] = 0;
	import1_val_debugger_saw3[i] = 0;
        
        imp11_val_deb_saw1[i]=0;
	imp11_val_deb_saw2[i]=0;
 	imp11_val_deb_saw3[i]=0;

	import2_val_debugger_saw1[i]=0;
	import2_val_debugger_saw2[i]=0;
	import2_val_debugger_saw3[i]=0;

	imp21_val_deb_saw1[i]=0;
	imp21_val_deb_saw2[i]=0;
	imp21_val_deb_saw3[i]=0;

    }
#ifndef PROTOTYPES
    do_pass1 (pass);   
    do_pass11 (pass);   
    do_pass2 (pass);   
    do_pass21 (pass);   
    do_passb (pass);   
#endif
    /* Start N_THREADS threads, then join them so
     * that they are terminated.
     */
    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_create( &t[i], NULL, spin, (void *)i );
        if( err != 0 ) {
            printf( "== Start/stop, error in thread %d create\n", i );
        }
    }

    for( i = 0; i < N_THREADS; i++ ) {
        err = pthread_join(t[i], NULL );    /* Line 105 */
        if( err != 0 ) {                    /* Line 106 */
            printf( "== Start/stop, error in thread %d join\n", i );
        }
    }

    i = 10;  /* Line 111.  Null line for setting bpts on. */

    printf( "== Pass %d done\n", pass );
/*#endif*/
   
}

void
do_it()
{
    /* We want to start some threads and then
     * end them, and then do it again and again
     */
    int i;
    int dummy;
    
    for( i = 0; i < OUTER_LOOP_COUNT; i++ ) {
        do_pass( i );
        dummy = i;      /* Line 134, null line for setting bps on */
    }
}

int
main( int argc, char** argv )
{
    do_it();
    
    return(0);
}

/* EndSourceFile */
