#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int lib1_thread_local1;
__thread int lib1_thread_local2;
__thread int lib1_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int import1_thread_local1;
__thread int import1_thread_local2;
__thread int import1_thread_local3;

/* Check the results of thread-local storage.
 */
int lib1_val_debugger_saw1[ N_THREADS ];
int lib1_val_debugger_saw2[ N_THREADS ];
int lib1_val_debugger_saw3[ N_THREADS ];

int lib1_static_val_debugger_saw1[ N_THREADS ];
int lib1_static_val_debugger_saw2[ N_THREADS ];
int lib1_static_val_debugger_saw3[ N_THREADS ];

extern void* myspin11( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin1( void *vp )
{
    int me = (long) vp;
    int i;
    
    lib1_thread_local1 = me + 13;
    lib1_thread_local2 = me + 23;
    lib1_thread_local3 = me + 33;

    static_thread_local1 = me + 43;
    static_thread_local2 = me + 53;
    static_thread_local3 = me + 63;

    import1_thread_local1 = me + 73;
    import1_thread_local2 = me + 83;
    import1_thread_local3 = me + 93;
  
    myspin11(vp);

    printf( "== Thread %d, lib1_thread_local1 is %d, lib1_thread_local2 is %d, lib1_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, import1_thread_local1 is %d, import1_thread_local2 is %d, import1_thread_local3 is %d\n",
            (long) vp, lib1_thread_local1, lib1_thread_local2, lib1_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, import1_thread_local1, import1_thread_local2, import1_thread_local3);

   return (void *)0;
}

void
do_pass1 ( int pass )
{
    int i;
    for( i = 0; i < N_THREADS; i++) {
        lib1_val_debugger_saw1[i];
	 lib1_val_debugger_saw2[i];
	 lib1_val_debugger_saw3[i];

	 lib1_static_val_debugger_saw1[i];
	 lib1_static_val_debugger_saw2[i];
	 lib1_static_val_debugger_saw3[i];
      }
}
