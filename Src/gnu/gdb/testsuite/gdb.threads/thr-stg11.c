#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int lib11_thread_local1;
__thread int lib11_thread_local2;
__thread int lib11_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int import11_thread_local1;
__thread int import11_thread_local2;
__thread int import11_thread_local3;

/* Check the results of thread-local storage.
 */
int lib11_val_debugger_saw1[ N_THREADS ];
int lib11_val_debugger_saw2[ N_THREADS ];
int lib11_val_debugger_saw3[ N_THREADS ];

int lib11_static_val_debugger_saw1[ N_THREADS ];
int lib11_static_val_debugger_saw2[ N_THREADS ];
int lib11_static_val_debugger_saw3[ N_THREADS ];

extern void* myspin2( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin11( void *vp )
{
    int me = (long) vp;
    int i;
    
    lib11_thread_local1 = me + 113;
    lib11_thread_local2 = me + 123;
    lib11_thread_local3 = me + 133;

    static_thread_local1 = me + 143;
    static_thread_local2 = me + 153;
    static_thread_local3 = me + 163;

    import11_thread_local1 = me + 173;
    import11_thread_local2 = me + 183;
    import11_thread_local3 = me + 193;
  
    myspin2(vp);

    printf( "== Thread %d, lib11_thread_local1 is %d, lib11_thread_local2 is %d, lib11_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, import11_thread_local1 is %d, import11_thread_local2 is %d, import11_thread_local3 is %d\n",
            (long) vp, lib11_thread_local1, lib11_thread_local2, lib11_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, import11_thread_local1, import11_thread_local2, import11_thread_local3);

   return (void *)0;
}


void
do_pass11 ( int pass )
{
    int i;
    for( i = 0; i < N_THREADS; i++) {
        lib11_val_debugger_saw1[i];
         lib11_val_debugger_saw2[i];
         lib11_val_debugger_saw3[i];

         lib11_static_val_debugger_saw1[i];
         lib11_static_val_debugger_saw2[i];
         lib11_static_val_debugger_saw3[i];
      }
}

