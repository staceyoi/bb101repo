#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int lib2_thread_local1;
__thread int lib2_thread_local2;
__thread int lib2_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int import2_thread_local1;
__thread int import2_thread_local2;
__thread int import2_thread_local3;

/* Check the results of thread-local storage.
 */
int lib2_val_debugger_saw1[ N_THREADS ];
int lib2_val_debugger_saw2[ N_THREADS ];
int lib2_val_debugger_saw3[ N_THREADS ];

int lib2_static_val_debugger_saw1[ N_THREADS ];
int lib2_static_val_debugger_saw2[ N_THREADS ];
int lib2_static_val_debugger_saw3[ N_THREADS ];

extern void* myspin21( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin2(void* vp )
{
    int me = (long) vp;
    int i;
    
    lib2_thread_local1 = me + 16;
    lib2_thread_local2 = me + 26;
    lib2_thread_local3 = me + 36;

    static_thread_local1 = me + 46;
    static_thread_local2 = me + 56;
    static_thread_local3 = me + 66;

    import2_thread_local1 = me + 76;
    import2_thread_local2 = me + 86;
    import2_thread_local3 = me + 96;

    myspin21(vp);

    printf( "== Thread %d, lib2_thread_local1 is %d, lib2_thread_local2 is %d, lib2_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, import2_thread_local1 is %d, import2_thread_local2 is %d, import2_thread_local3 is %d\n",
            (long) vp, lib2_thread_local1, lib2_thread_local2, lib2_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, import2_thread_local1, import2_thread_local2, import2_thread_local3);

   return (void *)0;
}

void
do_pass2 ( int pass )
{
    int i;
    for( i = 0; i < N_THREADS; i++) {
         lib2_val_debugger_saw1[i];
         lib2_val_debugger_saw2[i];
         lib2_val_debugger_saw3[i];

         lib2_static_val_debugger_saw1[i];
         lib2_static_val_debugger_saw2[i];
         lib2_static_val_debugger_saw3[i];
      }
}

