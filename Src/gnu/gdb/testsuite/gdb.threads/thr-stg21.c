#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int lib21_thread_local1;
__thread int lib21_thread_local2;
__thread int lib21_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int import21_thread_local1;
__thread int import21_thread_local2;
__thread int import21_thread_local3;

/* Check the results of thread-local storage.
 */
int lib21_val_debugger_saw1[ N_THREADS ];
int lib21_val_debugger_saw2[ N_THREADS ];
int lib21_val_debugger_saw3[ N_THREADS ];

int lib21_static_val_debugger_saw1[ N_THREADS ];
int lib21_static_val_debugger_saw2[ N_THREADS ];
int lib21_static_val_debugger_saw3[ N_THREADS ];

/* Routine for each thread to run, does nothing.
 */
void *myspin21( void* vp )
{
    int me = (long) vp;
    int i;
    
    lib21_thread_local1 = me + 116;
    lib21_thread_local2 = me + 126;
    lib21_thread_local3 = me + 136;

    static_thread_local1 = me + 146;
    static_thread_local2 = me + 156;
    static_thread_local3 = me + 166;

    import21_thread_local1 = me + 176;
    import21_thread_local2 = me + 186;
    import21_thread_local3 = me + 196;

    printf( "== Thread %d, lib21_thread_local1 is %d, lib21_thread_local2 is %d, lib21_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, import21_thread_local1 is %d, import21_thread_local2 is %d, import21_thread_local3 is %d\n",
            (long) vp, lib21_thread_local1, lib21_thread_local2, lib21_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, import21_thread_local1, import21_thread_local2, import21_thread_local3);

   return (void *)0;
}

void
do_pass21 ( int pass )
{
    int i;
    for( i = 0; i < N_THREADS; i++) {
         lib21_val_debugger_saw1[i];
         lib21_val_debugger_saw2[i];
         lib21_val_debugger_saw3[i];

         lib21_static_val_debugger_saw1[i];
         lib21_static_val_debugger_saw2[i];
         lib21_static_val_debugger_saw3[i];
      }
}

