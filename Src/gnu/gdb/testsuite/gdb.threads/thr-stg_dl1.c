#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int dl1_thread_local1;
__thread int dl1_thread_local2;
__thread int dl1_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int im_dl1_thread_local1;
__thread int im_dl1_thread_local2;
__thread int im_dl1_thread_local3;

/* Check the results of thread-local storage.
 */
int dl1_val_debugger_saw1[ N_THREADS ];
int dl1_val_debugger_saw2[ N_THREADS ];
int dl1_val_debugger_saw3[ N_THREADS ];

int dl1_static_val_debugger_saw1[ N_THREADS ];
int dl1_static_val_debugger_saw2[ N_THREADS ];
int dl1_static_val_debugger_saw3[ N_THREADS ];

extern void* myspin_dl11( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin_dl1( void *vp )
{
    int me = (long) vp;
    int i;
    
    dl1_thread_local1 = me + 1113;
    dl1_thread_local2 = me + 1123;
    dl1_thread_local3 = me + 1133;

    static_thread_local1 = me + 1143;
    static_thread_local2 = me + 1153;
    static_thread_local3 = me + 1163;

    im_dl1_thread_local1 = me + 1173;
    im_dl1_thread_local2 = me + 1183;
    im_dl1_thread_local3 = me + 1193;
  
    myspin_dl11(vp);

    printf( "== Thread %d, dl1_thread_local1 is %d, dl1_thread_local2 is %d, dl1_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, im_dl1_thread_local1 is %d, im_dl1_thread_local2 is %d, im_dl1_thread_local3 is %d\n",
            (long) vp, dl1_thread_local1, dl1_thread_local2, dl1_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, im_dl1_thread_local1, im_dl1_thread_local2, im_dl1_thread_local3);

   return (void *)0;
}
