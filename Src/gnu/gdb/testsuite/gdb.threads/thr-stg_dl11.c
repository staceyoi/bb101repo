#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int dl11_thread_local1;
__thread int dl11_thread_local2;
__thread int dl11_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int im_dl11_thread_local1;
__thread int im_dl11_thread_local2;
__thread int im_dl11_thread_local3;

/* Check the results of thread-local storage.
 */
int dl11_val_debugger_saw1[ N_THREADS ];
int dl11_val_debugger_saw2[ N_THREADS ];
int dl11_val_debugger_saw3[ N_THREADS ];

int dl11_static_val_debugger_saw1[ N_THREADS ];
int dl11_static_val_debugger_saw2[ N_THREADS ];
int dl11_static_val_debugger_saw3[ N_THREADS ];

//extern void* myspin_dl2( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin_dl11( void *vp )
{
    int me = (long) vp;
    int i;
    
    dl11_thread_local1 = me + 1116;
    dl11_thread_local2 = me + 1126;
    dl11_thread_local3 = me + 1136;

    static_thread_local1 = me + 1146;
    static_thread_local2 = me + 1156;
    static_thread_local3 = me + 1166;

    im_dl11_thread_local1 = me + 1176;
    im_dl11_thread_local2 = me + 1186;
    im_dl11_thread_local3 = me + 1196;
  
 //   myspin11(vp);

    printf( "== Thread %d, dl11_thread_local1 is %d, dl11_thread_local2 is %d, dl11_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, im_dl11_thread_local1 is %d, im_dl11_thread_local2 is %d, im_dl11_thread_local3 is %d\n",
            (long) vp, dl11_thread_local1, dl11_thread_local2, dl11_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, im_dl11_thread_local1, im_dl11_thread_local2, im_dl11_thread_local3);

   return (void *)0;
}
