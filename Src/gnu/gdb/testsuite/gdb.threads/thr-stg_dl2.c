#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int dl2_thread_local1;
__thread int dl2_thread_local2;
__thread int dl2_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int im_dl2_thread_local1;
__thread int im_dl2_thread_local2;
__thread int im_dl2_thread_local3;

/* Check the results of thread-local storage.
 */
int dl2_val_debugger_saw1[ N_THREADS ];
int dl2_val_debugger_saw2[ N_THREADS ];
int dl2_val_debugger_saw3[ N_THREADS ];

int dl2_static_val_debugger_saw1[ N_THREADS ];
int dl2_static_val_debugger_saw2[ N_THREADS ];
int dl2_static_val_debugger_saw3[ N_THREADS ];

extern void* myspin_dl21( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin_dl2( void *vp )
{
    int me = (long) vp;
    int i;
    
    dl2_thread_local1 = me + 2113;
    dl2_thread_local2 = me + 2123;
    dl2_thread_local3 = me + 2133;

    static_thread_local1 = me + 2143;
    static_thread_local2 = me + 2153;
    static_thread_local3 = me + 2163;

    im_dl2_thread_local1 = me + 2173;
    im_dl2_thread_local2 = me + 2183;
    im_dl2_thread_local3 = me + 2193;
  
    myspin_dl21(vp);

    printf( "== Thread %d, dl2_thread_local1 is %d, dl2_thread_local2 is %d, dl2_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, im_dl2_thread_local1 is %d, im_dl2_thread_local2 is %d, im_dl2_thread_local3 is %d\n",
            (long) vp, dl2_thread_local1, dl2_thread_local2, dl2_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, im_dl2_thread_local1, im_dl2_thread_local2, im_dl2_thread_local3);

   return (void *)0;
}
