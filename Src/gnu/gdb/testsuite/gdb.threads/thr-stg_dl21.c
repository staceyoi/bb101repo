#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int dl21_thread_local1;
__thread int dl21_thread_local2;
__thread int dl21_thread_local3;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

__thread int im_dl21_thread_local1;
__thread int im_dl21_thread_local2;
__thread int im_dl21_thread_local3;

/* Check the results of thread-local storage.
 */
int dl21_val_debugger_saw1[ N_THREADS ];
int dl21_val_debugger_saw2[ N_THREADS ];
int dl21_val_debugger_saw3[ N_THREADS ];

int dl21_static_val_debugger_saw1[ N_THREADS ];
int dl21_static_val_debugger_saw2[ N_THREADS ];
int dl21_static_val_debugger_saw3[ N_THREADS ];

//extern void* myspin_dl2( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspin_dl21( void *vp )
{
    int me = (long) vp;
    int i;
    
    dl21_thread_local1 = me + 2116;
    dl21_thread_local2 = me + 2126;
    dl21_thread_local3 = me + 2136;

    static_thread_local1 = me + 2146;
    static_thread_local2 = me + 2156;
    static_thread_local3 = me + 2166;

    im_dl21_thread_local1 = me + 2176;
    im_dl21_thread_local2 = me + 2186;
    im_dl21_thread_local3 = me + 2196;
  
 //   myspin11(vp);

    printf( "== Thread %d, dl21_thread_local1 is %d, dl21_thread_local2 is %d, dl21_thread_local3 is %d, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d, im_dl21_thread_local1 is %d, im_dl21_thread_local2 is %d, im_dl21_thread_local3 is %d\n",
            (long) vp, dl21_thread_local1, dl21_thread_local2, dl21_thread_local3 , static_thread_local1,  static_thread_local2,  static_thread_local3, im_dl21_thread_local1, im_dl21_thread_local2, im_dl21_thread_local3);

   return (void *)0;
}
