/* BeginSourceFile thr-stg_dl.c
   This file creates threads, sets some tls variables calls functions
   in dynamically loaded libraries, so that we can test gdb's handling
   of DTLS.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <dlfcn.h>

/* Thread-local storage.
 */

/* globals */
__thread int dl3_thread_local1;
__thread int dl3_thread_local2;
__thread int dl3_thread_local3;

/* Routine for each thread to run, does nothing.
 */
void *myspin_dl3( int me )
{
    dl3_thread_local1 = me + 10;
    dl3_thread_local2 = me + 20;
    dl3_thread_local3 = me + 30;

   return (void *)0;
}
