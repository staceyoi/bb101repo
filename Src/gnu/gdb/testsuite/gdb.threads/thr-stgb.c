#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define N_THREADS 3

/* Thread-local storage.
 */
__thread int thread_local1b;
__thread int thread_local2b;
__thread int thread_local3b;

static __thread int static_thread_local1;
static __thread int static_thread_local2;
static __thread int static_thread_local3;

/* Check the results of thread-local storage.
 */
int val_debugger_saw1b[ N_THREADS ];
int val_debugger_saw2b[ N_THREADS ];
int val_debugger_saw3b[ N_THREADS ];

int static_val_debugger_saw1b[ N_THREADS ];
int static_val_debugger_saw2b[ N_THREADS ];
int static_val_debugger_saw3b[ N_THREADS ];

extern void* myspin1( void* );
/* Routine for each thread to run, does nothing.
 */
void *myspinb( void *vp )
{
    int me = (long) vp;
    int i;
    
    thread_local1b = me + 110;
    thread_local2b = me + 120;
    thread_local3b = me + 130;

    static_thread_local1 = me + 140;
    static_thread_local2 = me + 150;
    static_thread_local3 = me + 160;

    myspin1(vp);

    printf( "== Thread %d, thread_local1b is %d, thread_local2b is %d, thread_local3b, static_thread_local1 is %d, static_thread_local2 is %d, static_thread_local3 is %d \n",
            (long) vp, thread_local1b, thread_local2b, thread_local3b , static_thread_local1,  static_thread_local2,  static_thread_local3);

    return (void *)0;

}

void
do_passb ( int pass )
{
    int i;
    for( i = 0; i < N_THREADS; i++) {
         val_debugger_saw1b[i];
         val_debugger_saw2b[i];
         val_debugger_saw3b[i];

         static_val_debugger_saw1b[i];
         static_val_debugger_saw2b[i];
         static_val_debugger_saw3b[i];
      }
}

