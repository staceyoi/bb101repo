/* thread forking and execing threaded program. */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void *thr(void *d) {
    pid_t pid;
    int stat_loc;
    
    printf("The thread...\n");

    if ((pid = fork()) != -1) {
        switch (pid) {
        case 0:                 /* I am the child so exec... */
            printf("Hello from child\n");
	    execlp ("gdb.threads/quicksort", "gdb.threads/quicksort", (char *)0);
	    printf("Exec failed\n");
            exit(1);
            break;
            
        default:                /* I am the parent so wait... */
            printf("Hello from parent\n");
            wait(&stat_loc);
            printf("In parent, child exited with status %d\n", 
                   stat_loc);
            /* If the child failed, make the parent fail too. */
            if (stat_loc !=0)
              exit(1);
            break;
            
        }
    } else {
        printf("Fork failed\n");
    }

    printf("Thread done\n"); return 0;
}
int main(int argc, char** argv) {
    pid_t pid;
    int stat_loc;
    pthread_t tid;

    int  wait_here = 0;
    if ((argc > 1) && (0 != argv )) {
       if( 1 == atoi( argv[1] ) )
           wait_here = 1;
    }
    while (wait_here) {};

    malloc (21);
    
    printf("Creating thread...\n");
    pthread_create(&tid, NULL, thr, NULL);

    pthread_join(tid, NULL);

    printf("Program done\n");
    exit (0);
}
