/* Often the behavior of any particular test depends upon what compiler was
   used to compile the test.  As each test is compiled, this file is
   preprocessed by the same compiler used to compile that specific test
   (different tests might be compiled by different compilers, particularly
   if compiled at different times), and used to generate a *.ci (compiler
   info) file for that test.

   I.E., when callfuncs is compiled, a callfuncs.ci file will be generated,
   which can then be sourced by callfuncs.exp to give callfuncs.exp access
   to information about the compilation environment.

   TODO:  It might be a good idea to add expect code that tests each
   definition made with 'set" to see if one already exists, and if so
   warn about conflicts if it is being set to something else.  */

#if defined(__cplusplus) 
set supports_template_debugging 1
#else
set supports_template_debugging 0
#endif

set hp_aCC3_compiler 0
set hp_aCC5_compiler 0
set hp_aCC6_compiler 0
set gcc_compiled 0
set gxx_compiled 0

#if defined (__GNUC__)
#if defined (__cplusplus)
set gxx_compiled __GNUC__
#else
set gcc_compiled __GNUC__
#endif
#else

#if defined (__HP_aCC)
#if (__HP_aCC < 40000)
set hp_aCC3_compiler 1
#elif (__HP_aCC < 60000)
set hp_aCC5_compiler 1
#else
set hp_aCC6_compiler 1
#endif
#endif

#endif

return 0
