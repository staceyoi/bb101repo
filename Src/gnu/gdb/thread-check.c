/* Thread tracing for GDB, the GNU debugger.
   Copyright 1986, 1987, 1988, 1993, 1998, 1999, 2000

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <pthread.h>
#include <sys/pstat.h>
#include <sys/utsname.h>
#include <strings.h>
#include <assert.h>
#include "trace_thread.h"

#include "defs.h"
#include "top.h"
#include "command.h"
#include "gdbcmd.h"
#include "gdbthread.h"
#include "breakpoint.h"
#include "target.h"         
#include "objfiles.h"
#include "inferior.h"
#include "thread_event/trace_event.h"
#include "thread.h"
#include "rtc.h"

#ifdef HP_IA64
#include <sys/dyntune.h> /* For gettune() */
#endif

#define DEBUG(x)
/* Additional debuging - for printing stack trace of inferior */
int thread_RTC_debug1 = 0;
extern void backtrace_command_1 (char *count_exp, int show_locals,
                                 int from_tty);
/* defined in gdbrtc.c */
extern struct objfile* locate_librtc (void);
extern void print_rtc_stack_frame (CORE_ADDR* pcs, int count, FILE *fp);
extern int64_t get_inferior_var_value (char* name, int size);
extern void set_inferior_var_value (char* name, int value);
/* defined in thread.c */
extern void prune_threads (void);
/* defined in infttrace.c */
extern int cur_thread_in_syscall (void);
/* defined in thread.c */
extern char* sym_info_addr(CORE_ADDR addr, CORE_ADDR* offset, int64_t thr);
/* defined in ia64h-nat.c */
extern void invalidate_rse_info (void);

/* From infttrace.c */
extern int target_insert_internal_breakpoint (CORE_ADDR addr, void* shadow);
extern int target_remove_internal_breakpoint (CORE_ADDR addr, void* shadow);

extern int read_thread_trace_info (void);

extern void batch_thread_redirect_backtrace (char *filename);

/* arguments for 'set thread-check' command */
static char* thread_tracing_args = 0;
/* is 'set thread-check' set? */
extern int trace_threads;

/* jini: To specify output directory in batch mode. */
static char *threads_output_dir = 0;
/* arguments for 'set frame-count' command */
static char* frame_count_args = 0;
/* RTC frame count */
extern int frame_count;

/* Global hash table of all objects for random access */
#define OBJECT_HASH_SIZE 1021
static gdb_pthread_object_t* object_hash[OBJECT_HASH_SIZE];

/* Arrays of elements indexed by a user-visible object index. Thread 
   array is the exception: threads have different indexes defined by 
   info thread */
static gdb_pthread_object_t* threads = NULL;
static gdb_pthread_object_t* mutexes = NULL;
static gdb_pthread_object_t** sorted_mutexes = NULL;
static gdb_pthread_object_t* rwlocks = NULL;
static gdb_pthread_object_t** sorted_rwlocks = NULL;
static gdb_pthread_object_t* condvars = NULL;
static gdb_pthread_object_t** sorted_condvars = NULL;

/* Count of the number of elements in above arrays */
static uint32_t thread_count = 0;
static uint32_t mutex_count = 0;
static uint32_t rwlock_count = 0;
static uint32_t cv_count = 0;

/* Are we tracing threads in this run? */
int trace_threads_in_this_run = 0;
/* defined in gdbrtc.c  */
extern int check_heap_in_this_run;

/* These are from gdbrtc.c */
extern struct objfile* rtc_dot_sl;
extern int rtc_in_progress;

/* thread id of the last thread causing a ttrace event */
extern int last_event_thread;
#ifdef GDB_TARGET_IS_HPUX
extern char * wdb_version_nbr;
#endif

#ifdef GDB_TARGET_IS_HPPA
/* For some reason SCHED_NOAGE is not defined in the PA BE */
#define SCHED_NOAGE 8
#endif

static char *thread_info_log_file_name = NULL;
static int thread_info_log_to_stderr = 0;
static FILE* thread_info_log_fp = NULL;
static int  thread_info_log_fd = 0;

CORE_ADDR librtc_start;
CORE_ADDR librtc_end;
CORE_ADDR libpthread_start;
CORE_ADDR libpthread_end;

extern char *get_exec_file (int);

/* Flags for various error detection rules. */
int thr_non_recursive_relock = 0;
int thr_unlock_not_own = 0;
int thr_mixed_sched_policy = 0;
int thr_condvar_multiple_mutexes = 0;
int thr_condvar_wait_no_mutex = 0;
int thr_deleted_obj_used = 0;
int thr_thread_exit_own_mutex = 0;
int thr_thread_exit_no_join_detach = 0;
int thr_stack_utilization = 0; /* %, 0 == disabled */
int thr_num_waiters = 0; /* 0 == disabled */
int thr_ignore_sys_objects = 0;

/* Retrieve pointer to the rtc library structure, and, if it's not
   been located, initialize it. Return NULL if not found. */
struct objfile * 
get_rtc_dot_sl (void)
{
  if (rtc_dot_sl == 0 
#ifdef HASH_ALL
      || rtc_dot_sl->hash_table == 0 || *rtc_dot_sl->hash_table == 0
#endif
      )
    {
      rtc_dot_sl = locate_librtc ();
      if (rtc_dot_sl == 0 
#ifdef HASH_ALL
          || rtc_dot_sl->hash_table == 0 || *rtc_dot_sl->hash_table == 0
#endif
          )
        return NULL;
    }
  return rtc_dot_sl;
}


/* Reset all the globals to prepare for thread tracing. This function is
   called everytime gdb starts debugging a process.  */
void
prepare_for_thread_tracing (int attached)
{
  trace_threads_in_this_run = trace_threads;
}


/* Called by "thread-tracing" command */
static void
set_thread_tracing_command (char *args, int from_tty, 
                            struct cmd_list_element *c)
{
  int switching_thread_tracing_on = 0;
  struct objfile * objfile;
  int found = 0;
  int thchk_enabled = 0;
  char* pthread_link_path;

#ifdef GDB_TARGET_IS_HPPA
  struct utsname uts;
  int sys_ver = 0;
  uname (&uts);
  if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
    warning ("uname: Couldn't resolve the release identifier of the "
           "Operating system. Thread checking might not work.");
  else if (sys_ver < 23) /* This will work for 11.23, 11.31 and 11.41 */
    {
      warning ("Thread checking is available from HPUX 11iv2 onwards.");
      return;
    }
#endif
   
#ifdef HP_IA64
  if (is_swizzled)
    pthread_link_path = "/opt/langtools/wdb/lib/hpux32";
  else
    pthread_link_path = "/opt/langtools/wdb/lib/hpux64";
#else
#ifndef GDB_TARGET_IS_HPPA_20W
  pthread_link_path = "/opt/langtools/wdb/lib";
#else 
  pthread_link_path = "/opt/langtools/wdb/lib/pa20_64";
#endif
#endif

  /* The input args is useless, we need to look in thread_tracing_args.
     GDB's set command handler leaves this pointer pointing after
     the "set thread-check". For example, if the user issued the
     command "set thread-check on", when control reaches this
     point, the char * thread_tracing_args points to the substring "on". */
  args = thread_tracing_args;

  if (args == 0)
    error ("Incomplete command : try `help set thread-tracing'");

  while (*args == ' ' || *args == '\t')
    args++;

  if (!strncmp (args, "recursive-relock", 16))
    {
       args += 16;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
        trace_threads = thr_non_recursive_relock = 1;
       else if (!strncmp (args, "off", 3))
         thr_non_recursive_relock = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_non_recursive_relock", 
                                 thr_non_recursive_relock);
    }
  else if (!strncmp (args, "unlock-not-own", 14))
    {
       args += 14;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         trace_threads = thr_unlock_not_own = 1;
       else if (!strncmp (args, "off", 3))
         thr_unlock_not_own = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_unlock_not_own", 
                                 thr_unlock_not_own);
    }
  else if (!strncmp (args, "mixed-sched-policy", 18))
    {
       args += 18;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         trace_threads = thr_mixed_sched_policy = 1;
       else if (!strncmp (args, "off", 3))
         thr_mixed_sched_policy = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_mixed_sched_policy", 
                                 thr_mixed_sched_policy);
    }
  else if (!strncmp (args, "cv-multiple-mxs", 15))
    {
       args += 15;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         trace_threads = thr_condvar_multiple_mutexes = 1;
       else if (!strncmp (args, "off", 3))
         thr_condvar_multiple_mutexes = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_condvar_multiple_mutexes", 
                                 thr_condvar_multiple_mutexes);
    }
  else if (!strncmp (args, "cv-wait-no-mx", 13))
    {
       args += 13;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         trace_threads = thr_condvar_wait_no_mutex = 1;
       else if (!strncmp (args, "off", 3))
         thr_condvar_wait_no_mutex = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_condvar_wait_no_mutex", 
                                 thr_condvar_wait_no_mutex);
    }
  else if (!strncmp (args, "thread-exit-own-mutex", 21))
    {
       args += 21;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         trace_threads = thr_thread_exit_own_mutex = 1;
       else if (!strncmp (args, "off", 3))
         thr_thread_exit_own_mutex = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_thread_exit_own_mutex", 
                                 thr_thread_exit_own_mutex);
    }
  else if (!strncmp (args, "thread-exit-no-join-detach", 26))
    {
       args += 26;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
         trace_threads = thr_thread_exit_no_join_detach = 1;
       else if (!strncmp (args, "off", 3))
         thr_thread_exit_no_join_detach = 0;

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_thread_exit_no_join_detach", 
                                 thr_thread_exit_no_join_detach);
    }
  else if (!strncmp (args, "stack-util", 10))
    {
       args += 10;
       while (*args == ' ' || *args == '\t')
         args++;

       if (args && !could_be_number(args))
         error ("Invalid value %s. Stack utilization must be an integer.\n",
                args);

       thr_stack_utilization = atoi (args);
       trace_threads = 1;
       if (thr_stack_utilization < 0 || thr_stack_utilization > 100)
         {
           thr_stack_utilization = 0;
           trace_threads = 0;
	   error ("Invalid value: stack utilization must be "
                  "between 0 and 100. \n Disabling the thread-checking.");
         }

       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_stack_utilization", 
                                 thr_stack_utilization);
    }
  else if (!strncmp (args, "num-waiters", 11))
    {
       args += 11;
       while (*args == ' ' || *args == '\t')
         args++;

       if (args && !could_be_number(args))
         error ("Invalid value %s. num-waiters must be an integer.\n",
                args);

       thr_num_waiters = atoi (args);
       trace_threads = 1;
       if (target_has_stack && trace_threads_in_this_run)
         set_inferior_var_value ("__rtc_num_waiters", thr_num_waiters);
    }
  else if (!strncmp (args, "ignore-sys-objects", 18))
    {
       args += 18;
       while (*args == ' ' || *args == '\t')
         args++;

       if (!strncmp (args, "on", 2))
        {
	  thr_ignore_sys_objects = 1;
	  trace_threads = 1;
	}
       else if (!strncmp (args, "off", 3))
         thr_ignore_sys_objects = 0;
    }
  else if (!strncmp (args, "on", 2))
    {
      switching_thread_tracing_on = trace_threads == 0 ? 1 : 0;
      trace_threads = 1;
      /* Turn all threads check features on with default values */
      thr_non_recursive_relock = 1;
      thr_unlock_not_own = 1;
      thr_mixed_sched_policy = 1;
      thr_condvar_multiple_mutexes = 1;
      thr_condvar_wait_no_mutex = 1;
      thr_deleted_obj_used = 1;
      thr_thread_exit_own_mutex = 1;
      thr_thread_exit_no_join_detach = 1;
      thr_stack_utilization = 80; /* %, 0 == disabled */
      thr_num_waiters = 0; /* 0 == disabled */
      thr_ignore_sys_objects = 1;

    }
  else if (!strncmp (args, "off", 3))
    {
      trace_threads = 0;
    }
  else
    {
      error ("Unknown suboption for 'set thread-check'. Use 'help set "
             "thread-check' to get the list of valid suboptions.");
    }

  if (target_has_execution && switching_thread_tracing_on)
    {
       /* Locate librtc */
       ALL_OBJFILES (objfile)
         {
            if (strstr (objfile->name, "/librtc")) 
              {
                 found = 1;
                 break;
              }
         }
         if (!found) 
           error ("librtc is not loaded: Either use -threads command "
                  "line option, set thread-check before starting the "
                  "program, or link librtc explicitly.\n"
                  "In order to enable thread checking librtc must be "
                  "pre-loaded using LD_PRELOAD environment variable or "
                  "linked with the application; the application must be "
                  "linked with libpthread_tr or LD_LIBRARY_PATH "
                  "environment variable must contain %s; "
                  "PTHR_TRACE_ENABLED environment variable must be "
                  "set to \"on\" before running the application.",
                  pthread_link_path);

       /* Locate tracing version of libpthread. The app must use
          libpthread link in /opt/langtools/ to load it */
       found = 0;
       ALL_OBJFILES (objfile)
         {
            if (strstr (objfile->name, "/libpthread")) 
              {
                 if (strstr (objfile->name, "/opt/langtools") ||
                     strstr (objfile->name, gdb_root))
                   {
                     found = 1;
                     break;
                   }
              }
            else if (strstr (objfile->name, "/libpthread_tr")) 
              {
                found = 1;
                break;
              }
         }
       if (!found) 
         {
           error ("Tracing version of libpthread is not loaded: set "
                  "thread-check before starting the program, link your "
                  "program with libpthread_tr or set LD_LIBRARY_PATH to "
                  "contain %s\n"
                  "In order to enable thread checking librtc must me "
                  "pre-loaded using LD_PRELOAD environment variable or "
                  "linked with the application; the application must be "
                  "linked with libpthread_tr or LD_LIBRARY_PATH "
                  "environment variable must contain %s; "
                  "PTHR_TRACE_ENABLED environment variable must be "
                  "set to \"on\" before running the application.",
                  pthread_link_path, pthread_link_path);
         }

       /* make sure tracing is enabled in the running process */
       thchk_enabled = get_inferior_var_value("thread_trace_enabled", 4);
       if (!thchk_enabled)
         error ("Thread checking is not enabled in librtc. Set "
                "PTHR_TRACE_ENABLED to \"on\" before running the "
                "application.\n"
                "In order to enable thread checking librtc must me pre-loaded "
                "using LD_PRELOAD environment variable or linked with the "
                "application; the application must be linked with "
                "libpthread_tr or LD_LIBRARY_PATH environment variable must "
                "contain %s; PTHR_TRACE_ENABLED environment variable must be "
                "set to \"on\" before running the application.",
                pthread_link_path);
     }

   /* Avoid this warning for the batch mode thread check case since gdb
      would attach to the application only after it has started running.
    */
   if (found && switching_thread_tracing_on && !batch_rtc)
     warning("Program is already running. Thread information provided "
             "may not be accurate.");

  /* If the user started the thread check in the middle of execution 
     (or attach). Let's start-up the stuff. In the normal case, this is 
     done when the program starts up. */
  if (target_has_execution && trace_threads && !trace_threads_in_this_run)
    {
      prepare_for_thread_tracing (1);
      snoop_on_the_heap ();
    }
}


/* Show the state of the thread-check option (on or off) */
static void
show_thread_tracing_command (char *args, int from_tty, 
                             struct cmd_list_element *c)
{
  if (trace_threads)
    {
      printf_filtered ("Thread tracing is enabled. Current settings are:\n");
      printf_filtered ("\tDetect non recursive mutex relock: %s\n",  
                       thr_non_recursive_relock ? "on" : "off");
      printf_filtered ("\tDetect unlocks of mutex/rwlock not "
                       "previously locked: %s\n", 
                       thr_unlock_not_own ? "on" : "off");
      printf_filtered ("\tDetect mixed sched policy: %s\n", 
                       thr_mixed_sched_policy ? "on" : "off");
      printf_filtered ("\tDetect condvars associated with multiple "
                       "mutexes: %s\n", 
                       thr_condvar_multiple_mutexes ? "on" : "off");
      printf_filtered ("\tDetect condvar waits with no locked mutex: %s\n", 
                       thr_condvar_wait_no_mutex ? "on" : "off");
      printf_filtered ("\tDetect when deleted object is used: %s\n", 
                       thr_deleted_obj_used ? "on" : "off");
      printf_filtered ("\tDetect when thread exits while holding a "
                       "mutex: %s\n", 
                       thr_thread_exit_own_mutex ? "on" : "off");
      printf_filtered ("\tDetect when terminated thread is neither "
                       "joined nor detached: %s\n", 
                       thr_thread_exit_no_join_detach ? "on" : "off");
      printf_filtered ("\tThread stack utilization threshold: %d\n", 
                       thr_stack_utilization);
      printf_filtered ("\tFrame count: %d\n", frame_count);
    }
  else
    {
      printf_filtered ("Thread tracing is not enabled.\n");
    }
}


/* Set the depth of call stack to be captured with each event */
static void
set_frame_count_command(char *args, int from_tty, 
                        struct cmd_list_element *c)
{
  /* The input args is useless, we need to look in thread_tracing_args.
     GDB's set command handler leaves this pointer pointing after
     the "set frame-count". For example, if the user issued the
     command "set frame-count 12", when control reaches this
     point, the char * frame_count_args points to the substring "12". */
  args = frame_count_args;

  if (args == 0)
    error ("Incomplete command : try `help set frame-count'.");

  if (!could_be_number(args))
    error ("Invalid value %s. Frame count must be an integer.\n",
           args);

  frame_count = atoi (args);

  if (target_has_stack && 
      (check_heap_in_this_run || trace_threads_in_this_run))
    {
      rtc_dot_sl = get_rtc_dot_sl();
      set_inferior_var_value ("__rtc_frame_count", frame_count);
      /* Invalidate the buffers allocated using old parameters. */
      set_inferior_var_value ("__rtc_stack_trace_count", 0);
    }
}


/* Show the depth of call stack to be captured with each event */
static void
show_frame_count_command (char *args, int from_tty, 
                             struct cmd_list_element *c)
{
    printf_filtered("Depth of call stack to be captured is %d\n", frame_count);
}


/* Hash function for the object hash table. It was ripped off from some
   dude's web page. It claims to give good random distribution. 
   For more info: http://burtleburtle.net/bob/hash/evahash.html */
static unsigned int 
object_hash_func (uint64_t key)
{
  register char* k = (char*) &key;
  register unsigned int a, b, c;

  a = b = c = 0x9e3779b9; /* the golden ratio; an arbitrary value */

  b += ((unsigned int) k[7] << 24);
  b += ((unsigned int) k[6] << 16);
  b += ((unsigned int) k[5] << 8);
  b += (unsigned int) k[4];
  
  a += ((unsigned int) k[3] << 24);
  a += ((unsigned int) k[2] << 16);
  a += ((unsigned int) k[1] << 8);
  a += (unsigned int) k[0];
  
  /* Mix the values */
  a -= b; a -= c; a ^= (c >> 13);
  b -= c; b -= a; b ^= (a << 8);
  c -= a; c -= b; c ^= (b >> 13);
  a -= b; a -= c; a ^= (c >> 12);
  b -= c; b -= a; b ^= (a << 16);
  c -= a; c -= b; c ^= (b >> 5);
  a -= b; a -= c; a ^= (c >> 3);
  b -= c; b -= a; b ^= (a << 10);
  c -= a; c -= b; c ^= (b >> 15);
  
  return c;
}


/* Add an object to the hash table. */
static void 
object_hash_add (gdb_pthread_object_t* obj)
{
  unsigned int index = object_hash_func(obj->id) % OBJECT_HASH_SIZE;
  obj->next = object_hash[index];
  object_hash[index] = obj;
}


/* Retrieve an object with a given key from the hash table. Return NULL
   if not found */
static gdb_pthread_object_t* 
object_hash_get (uint64_t key)
{
  unsigned int index = object_hash_func(key) % OBJECT_HASH_SIZE;
  gdb_pthread_object_t* obj = object_hash[index];
  while (obj)
    {
      if (obj->id == key) return obj;
      obj = obj->next;
    }
  return NULL;
}

/* Consolidated error reporting intoa single function. It's called from 
   multiple places, that's why it made sense to move it into one routine */
static void 
report_read_thread_errors (int code, object_type_t obj_type)
{
  char* obj_name = NULL;
  switch (obj_type)
    {
      case OBJT_THREAD: 
          obj_name = "threads";
          break;
      case OBJT_MUTEX: 
          obj_name = "mutexes";
          break;
      case OBJT_RWLOCK: 
          obj_name = "read-write locks";
          break;
      case OBJT_COND: 
          obj_name = "condition variables";
          break;
      default:
          obj_name = "pthread objects";
          break;
    }

    switch (code)
      {
        case RTC_DISABLED:
            error ("Thread checking is not enabled now.");
            break;
        case RTC_NO_LIBRTC:
            error ("The heap debugging library (librtc) is not linked in !\n"
                   "Hint: Use environment variable LIBRTC_SERVER to pick "
                   "correct version of librtc and upgrade to the latest "
                   "linker version (greater than B.11.19);  Or link in "
                   "librtc with your application to enable gdb support "
                   "for heap debugging.");
            break;
        case RTC_NO_STACK:
            error ("There is no running process.");
            break;
        case RTC_NO_RECORDS:
            error ("No known %s.", obj_name);
            break;
        case RTC_IN_SYSCALL:
            error ("Information about %s cannot be obtained while a system "
                   "call is interrupted.", obj_name);
            break;
        case RTC_NOT_RUNNING:
            error ("Thread checking is not running. Information about %s "
                   "cannot be obtained.", obj_name);
            break;
        case RTC_FOPEN_FAILED:
            error ("Cannot open pthread analysis file.");
            break;
        default:
            break;
    }
}


/* Convert object id given by pthread into an gdb user-visible object id.
   For mutexes, condvars and rwlocks gdb id is the index into the 
   corresponding array. For threads, it's a user thread id. */
static int
pthread_to_gdb_id (int64_t pthread_id)
{
  gdb_pthread_object_t* obj = object_hash_get(pthread_id);
  if (obj == NULL) return -1;

  if (obj->type == OBJT_THREAD)
    {
      return obj->id;
    }
  else
    {
      return obj->index;
    }
}


/* Get the id of the primary owner for a given object. Return -1 if 
   not found */
static int64_t
get_owner (gdb_pthread_object_t* obj)
{
  for (int i = 0; i < obj->num_rels; i++)
    {
      if (obj->rel_list[i].type == REL_OWNED_BY)
        return obj->rel_list[i].object_id;
    }
  return -1;
}


/* Get the id of the creator for a given object. Return -1 if not found */
static int64_t
get_creator (gdb_pthread_object_t* obj)
{
  for (int i = 0; i < obj->num_rels; i++)
    {
      if (obj->rel_list[i].type == REL_CREATED_BY)
        return obj->rel_list[i].object_id;
    }
  return -1;
}


/* Print info about a given relation */
static void
print_relation_info (gdb_relation_t* rel)
{
  gdb_pthread_object_t* obj = object_hash_get(rel->object_id);

  if (batch_rtc)
    {
      switch (rel->type)
        {
          case REL_PRIMARY_OWNER:
              fprintf (thread_info_log_fp, "Owns exclusively:     ");
              break;
          case REL_SECONDARY_OWNER:
              fprintf (thread_info_log_fp, "Owns non-exclusively: ");
              break;
          case REL_WAITER:
              fprintf (thread_info_log_fp, "Waiting on:           ");
              break;
          case REL_CREATED_BY:
              fprintf (thread_info_log_fp, "Created by:           ");
              break;
          case REL_OWNED_BY:
              fprintf (thread_info_log_fp, "Owned by:             ");
              break;
          case REL_WAITED_ON_BY:
              fprintf (thread_info_log_fp, "Waited on by:         ");
              break;
          default:
              return;
        }

      if (obj)
        {
          switch (obj->type)
            {
              case OBJT_THREAD:
                  fprintf (thread_info_log_fp, "thread %lld\n", obj->id);
                  break;
              case OBJT_MUTEX:
                  fprintf (thread_info_log_fp, "mutex %d\n", obj->index);
                  break;
              case OBJT_RWLOCK:
                  fprintf (thread_info_log_fp, "rwlock %d\n", obj->index);
                  break;
              case OBJT_COND:
                  fprintf (thread_info_log_fp, "condvar %d\n", obj->index);
                  break;
              default:
                  fprintf (thread_info_log_fp, "unknown\n");
                  break;
            }
        }
      else
        {
          fprintf (thread_info_log_fp, "unknown\n");
        }

      if (rel->frame_count)
        print_rtc_stack_frame(rel->frames, rel->frame_count, thread_info_log_fp);
      else
        fprintf(thread_info_log_fp, "No stack frames\n");
      fprintf(thread_info_log_fp, "\n");
   }
  else
    {
      switch (rel->type)
        {
          case REL_PRIMARY_OWNER:
              printf_filtered ("Owns exclusively:     ");
              break;
          case REL_SECONDARY_OWNER:
              printf_filtered ("Owns non-exclusively: ");
              break;
          case REL_WAITER:
              printf_filtered ("Waiting on:           ");
              break;
          case REL_CREATED_BY:
              printf_filtered ("Created by:           ");
              break;
          case REL_OWNED_BY:
              printf_filtered ("Owned by:             ");
              break;
          case REL_WAITED_ON_BY:
              printf_filtered ("Waited on by:         ");
              break;
          default:
              return;
        }

      if (obj)
        {
          switch (obj->type)
            {
              case OBJT_THREAD:
                  printf_filtered ("thread %lld\n", obj->id);
                  break;
              case OBJT_MUTEX:
                  printf_filtered ("mutex %d\n", obj->index);
                  break;
              case OBJT_RWLOCK:
                  printf_filtered ("rwlock %d\n", obj->index);
                  break;
              case OBJT_COND:
                  printf_filtered ("condvar %d\n", obj->index);
                  break;
              default:
                  printf_filtered ("unknown\n");
                  break;
            }
        }
      else
        {
          printf_filtered ("unknown\n");
        }

      if (rel->frame_count)
        print_rtc_stack_frame(rel->frames, rel->frame_count, NULL);
      else
        printf_filtered("No stack frames\n");
      printf_filtered("\n");
   }
}


/* Printf a list of relations for a given object */
static void
print_relations (gdb_pthread_object_t* obj)
{
  gdb_relation_t* rel;
  for (int i = 0; i < obj->num_rels; i++)
    {
      rel = &obj->rel_list[i];
      if (rel->type == REL_CREATED_BY)
        {
          print_relation_info (rel);
          break;
        }
    }
  for (int i = 0; i < obj->num_rels; i++)
    {
      rel = &obj->rel_list[i];
      if (rel->type == REL_PRIMARY_OWNER)
        {
          print_relation_info (rel);
          break;
        }
    }  
  for (int i = 0; i < obj->num_rels; i++)
    {
      rel = &obj->rel_list[i];
      if (rel->type == REL_SECONDARY_OWNER)
        print_relation_info (rel);
    }
  for (int i = 0; i < obj->num_rels; i++)
    {
      rel = &obj->rel_list[i];
      if (rel->type == REL_WAITER)
        print_relation_info (rel);
    }
  for (int i = 0; i < obj->num_rels; i++)
    {
      rel = &obj->rel_list[i];
      if (rel->type == REL_OWNED_BY)
        print_relation_info (rel);
    }
  for (int i = 0; i < obj->num_rels; i++)
    {
      rel = &obj->rel_list[i];
      if (rel->type == REL_WAITED_ON_BY)
        print_relation_info (rel);
    }
}


/* Return a pointer to the mutex with a given index */
static gdb_pthread_object_t*
get_mutex_obj (int index)
{
  for (int i = 0; i < mutex_count; i++)
    {
      if (mutexes[i].index == index)
        return &mutexes[i];
    }
  return NULL;
}

/* Print a one-line basic mutex info for a given mutex */
static void
print_basic_mutex_info (gdb_pthread_object_t* mutex)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;
  int64_t owner_id;
  assert(mutex->type == OBJT_MUTEX);
  
  printf_filtered ("%-3d ", mutex->index);
  owner_id = get_owner(mutex);
  if (owner_id == -1)
    printf_filtered ("none     ");
  else
    printf_filtered ("%-8lld ", owner_id);

  printf_filtered ("%-3d      ",  mutex->obj.mutex.waiter_count);
  printf_filtered ((sizeof(mutex->obj.mutex.address) > 4) ? 
                   "0x%-16llx  " : "0x%-16lx  ", mutex->obj.mutex.address);

  sym_name = sym_info_addr(mutex->obj.mutex.address, &sym_offset,
                           get_creator(mutex));
  if (sym_name)
    {
      printf_filtered ("%s", sym_name);
      if (sym_offset) 
        printf_filtered ((sizeof(sym_offset) > 4) ? 
                         " + 0x%llx" : " + 0x%lx", sym_offset);
    }
  printf_filtered ("\n");
}


/* Print extended mutex info, including all of its relations,
   for a given mutex */
static void
print_extended_mutex_info (gdb_pthread_object_t* mutex)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;

  assert(mutex->type == OBJT_MUTEX);

  if (batch_rtc)
    {
      fprintf (thread_info_log_fp,
              "\nDetailed information on mutex %d:\n", mutex->index);
      fprintf (thread_info_log_fp,
              "---------------------------------\n");
      fprintf (thread_info_log_fp, "Mutex ID: \t\t%-3d ", mutex->index);
      if (!(mutex->flags & OBJF_CREATED))
        fprintf (thread_info_log_fp, "(initialization incomplete)");
      fprintf (thread_info_log_fp, "\n");

      sym_name = sym_info_addr(mutex->obj.mutex.address, &sym_offset,
                               get_creator(mutex));
      fprintf (thread_info_log_fp, "Variable address: \t");
      fprintf (thread_info_log_fp, (sizeof(mutex->obj.mutex.address) > 4) ?
                       "0x%-16llx\n" : "0x%-16lx\n", mutex->obj.mutex.address);
      fprintf (thread_info_log_fp, "Variable name: \t\t");
      if (sym_name)
        {
          fprintf (thread_info_log_fp, "%s", sym_name);
          if (sym_offset) 
            fprintf (thread_info_log_fp, (sizeof(sym_offset) > 4) ? 
                             " + 0x%llx" : " + 0x%lx", sym_offset);
        }
      else
        {
          fprintf (thread_info_log_fp, "unknown");
        }
    
      fprintf (thread_info_log_fp, "\nProcess shared: \t%s\n", 
                       mutex->obj.mutex.flags & PTHREAD_TRACE_MUT_PSHARED ? 
                       "y" : "n");
    
      fprintf (thread_info_log_fp, "Type: \t\t\t");
      switch (mutex->obj.mutex.type) 
        {
          case PTHREAD_TRACE_MUT_TYPE_NORMAL:
              fprintf (thread_info_log_fp, "normal\n");
              break;
          case PTHREAD_TRACE_MUT_TYPE_RECURSIVE:
              fprintf (thread_info_log_fp, "recursive\n");
              break;
          case PTHREAD_TRACE_MUT_TYPE_ERRORCHECK:
              fprintf (thread_info_log_fp, "error check\n");
              break;
          default:
              fprintf (thread_info_log_fp, "unknown\n");
              break;
        }
    
      fprintf(thread_info_log_fp, "Protocol: \t\t");
      switch (mutex->obj.mutex.protocol) 
        {
          case PTHREAD_TRACE_MUT_PROTO_NONE:
              fprintf (thread_info_log_fp, "normal\n");
              break;
          case PTHREAD_TRACE_MUT_PROTO_PROTECT:
              fprintf (thread_info_log_fp, "protect\n");
              break;
          case PTHREAD_TRACE_MUT_PROTO_INHERIT:
              fprintf (thread_info_log_fp, "inherit\n");
              break;
          default:
              fprintf (thread_info_log_fp, "unknown\n");
              break;
        }
    
      fprintf (thread_info_log_fp, "Blocked count: \t\t%d\n", mutex->obj.mutex.waiter_count);
      fprintf (thread_info_log_fp, "Trylock failures: \t%d\n", 
                       mutex->obj.mutex.trylock_failures);
      fprintf (thread_info_log_fp, "Lock count: \t\t%d\n", mutex->obj.mutex.lock_count);
      fprintf (thread_info_log_fp, "Contended locks: \t%d\n\n", 
                       mutex->obj.mutex.contended_locks);
    }
  else
    {

      printf_filtered ("Mutex ID: \t\t%-3d ", mutex->index);
      if (!(mutex->flags & OBJF_CREATED))
        printf_filtered ("(initialization incomplete)");
      printf_filtered ("\n");
    
      sym_name = sym_info_addr(mutex->obj.mutex.address, &sym_offset,
                               get_creator(mutex));
      printf_filtered ("Variable address: \t");
      printf_filtered ((sizeof(mutex->obj.mutex.address) > 4) ?
                       "0x%-16llx\n" : "0x%-16lx\n", mutex->obj.mutex.address);
      printf_filtered ("Variable name: \t\t");
      if (sym_name)
        {
          printf_filtered ("%s", sym_name);
          if (sym_offset) 
            printf_filtered ((sizeof(sym_offset) > 4) ? 
                             " + 0x%llx" : " + 0x%lx", sym_offset);
        }
      else
        {
          printf_filtered ("unknown");
        }
    
      printf_filtered ("\nProcess shared: \t%s\n", 
                       mutex->obj.mutex.flags & PTHREAD_TRACE_MUT_PSHARED ? 
                       "y" : "n");
    
      printf_filtered ("Type: \t\t\t");
      switch (mutex->obj.mutex.type) 
        {
          case PTHREAD_TRACE_MUT_TYPE_NORMAL:
              printf_filtered ("normal\n");
              break;
          case PTHREAD_TRACE_MUT_TYPE_RECURSIVE:
              printf_filtered ("recursive\n");
              break;
          case PTHREAD_TRACE_MUT_TYPE_ERRORCHECK:
              printf_filtered ("error check\n");
              break;
          default:
              printf_filtered ("unknown\n");
              break;
        }
    
      printf_filtered("Protocol: \t\t");
      switch (mutex->obj.mutex.protocol) 
        {
          case PTHREAD_TRACE_MUT_PROTO_NONE:
              printf_filtered ("normal\n");
              break;
          case PTHREAD_TRACE_MUT_PROTO_PROTECT:
              printf_filtered ("protect\n");
              break;
          case PTHREAD_TRACE_MUT_PROTO_INHERIT:
              printf_filtered ("inherit\n");
              break;
          default:
              printf_filtered ("unknown\n");
              break;
        }

      printf_filtered ("Blocked count: \t\t%d\n", mutex->obj.mutex.waiter_count);
      printf_filtered ("Trylock failures: \t%d\n", 
                       mutex->obj.mutex.trylock_failures);
      printf_filtered ("Lock count: \t\t%d\n", mutex->obj.mutex.lock_count);
      printf_filtered ("Contended locks: \t%d\n\n", 
                       mutex->obj.mutex.contended_locks);
  }

  print_relations(mutex);
}


/* This routine is called in response to info mutex command */
static void
mutex_info_command (char *exp, int from_tty)
{
  int ret;

  dont_repeat ();

  if (!target_has_stack) error ("No stack.");

  /* Re-read the list of threads */
  prune_threads ();
  target_find_new_threads ();

  /* Read additional thread info from librtc */
  ret = read_thread_trace_info();

  if (ret)
    report_read_thread_errors(ret, OBJT_MUTEX);

  if (exp == NULL)
    {
        /* Print a table of all known mutexes */
        if (mutex_count)
          {
            printf_filtered ("ID  OwnerID  NumBlkd  "
                             "Address             Name+Offset\n");
            for (int i = 0; i < mutex_count; i++)
              print_basic_mutex_info(sorted_mutexes[i]);
          }
        else
          {
            error ("No mutex info.");
          }
    }
  else
    {
      if (!could_be_number(exp))
        error ("Invalid value %s. Mutex ID must be an integer.\n", exp);

      int mutex_id = atoi (exp);
      gdb_pthread_object_t* mutex = get_mutex_obj (mutex_id);
      if (mutex == NULL)
        error ("Mutex ID %d not known.  Use the \"info mutex\" "
               "command to\nsee the IDs of currently known mutexes.", 
               mutex_id);
      print_extended_mutex_info(mutex);
    }
}


/* Return a pointer to the read-write lock with a given index */
static gdb_pthread_object_t*
get_rwlock_obj (int index)
{
  for (int i = 0; i < rwlock_count; i++)
    {
      if (rwlocks[i].index == index)
        return &rwlocks[i];
    }
  return NULL;
}


/* Print a one-line basic read-write lock info for a given lock */
static void
print_basic_rwlock_info (gdb_pthread_object_t* rwlock)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;
  int64_t owner_id;
  assert(rwlock->type == OBJT_RWLOCK);

  printf_filtered ("%-3d ", rwlock->index);
  owner_id = get_owner (rwlock);
  if (owner_id == -1)
    printf_filtered ("none     ");
  else
    printf_filtered ("%-8lld ", owner_id);

  printf_filtered ("%-3d      ", rwlock->obj.rwlock.waiter_count);
  printf_filtered ((sizeof(rwlock->obj.rwlock.address) > 4) ? 
                   "0x%-16llx  " : "0x%-16lx  ", rwlock->obj.rwlock.address);

  sym_name = sym_info_addr (rwlock->obj.rwlock.address, &sym_offset,
                            get_creator(rwlock));
  if (sym_name)
    {
      printf_filtered ("%s", sym_name);
      if (sym_offset) 
        printf_filtered ((sizeof(sym_offset) > 4) ? 
                         " + 0x%llx" : " + 0x%lx", sym_offset);
    }
  printf_filtered ("\n");
}


/* Print extended read-write lock info, including all of its relations,
   for a given lock */
static void
print_extended_rwlock_info (gdb_pthread_object_t* rwlock)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;

  assert(rwlock->type == OBJT_RWLOCK);

  if (batch_rtc)
    {
      fprintf (thread_info_log_fp,
              "\nDetailed information on rwlock %d:\n", rwlock->index);
      fprintf (thread_info_log_fp,
              "----------------------------------\n");
      fprintf (thread_info_log_fp, 
               "Read-write lock ID: \t%-3d ", rwlock->index);
      if (!(rwlock->flags & OBJF_CREATED))
        fprintf (thread_info_log_fp, "(initialization incomplete)");
      fprintf (thread_info_log_fp, "\n");

      sym_name = sym_info_addr (rwlock->obj.rwlock.address, &sym_offset,
                                get_creator(rwlock));

      fprintf (thread_info_log_fp, "Variable address: \t");
      fprintf (thread_info_log_fp, (sizeof(rwlock->obj.rwlock.address) > 4) ?
                  "0x%-16llx\n" : "0x%-16lx\n", rwlock->obj.rwlock.address);
      fprintf (thread_info_log_fp, "Variable name: \t\t");
      if (sym_name)
        {
          fprintf (thread_info_log_fp, "%s", sym_name);
          if (sym_offset) 
            fprintf (thread_info_log_fp, (sizeof(sym_offset) > 4) ? 
                             " + 0x%llx" : " + 0x%lx", sym_offset);
        }
      else
        {
          fprintf (thread_info_log_fp, "unknown");
        }

      fprintf (thread_info_log_fp, "\nProcess shared: \t%s\n", 
                       rwlock->obj.rwlock.flags & PTHREAD_TRACE_MUT_PSHARED ? 
                       "y" : "n");
  
      fprintf (thread_info_log_fp, "Blocked count: \t\t%d\n", 
                       rwlock->obj.rwlock.waiter_count);
      fprintf (thread_info_log_fp, "Reader count: \t\t%d\n", 
                       rwlock->obj.rwlock.reader_count);
      fprintf (thread_info_log_fp, "Trylock failures: \t%d\n", 
                       rwlock->obj.rwlock.trylock_failures);
      fprintf (thread_info_log_fp,
               "Lock count: \t\t%d\n", rwlock->obj.rwlock.lock_count);
      fprintf (thread_info_log_fp, "Contended locks: \t%d\n\n", 
                       rwlock->obj.rwlock.contended_locks);

    }
  else
    {
      printf_filtered ("Read-write lock ID: \t%-3d ", rwlock->index);
      if (!(rwlock->flags & OBJF_CREATED))
        printf_filtered ("(initialization incomplete)");
      printf_filtered ("\n");

      sym_name = sym_info_addr (rwlock->obj.rwlock.address, &sym_offset,
                                get_creator(rwlock));

      printf_filtered ("Variable address: \t");
      printf_filtered ((sizeof(rwlock->obj.rwlock.address) > 4) ?
                       "0x%-16llx\n" : "0x%-16lx\n", rwlock->obj.rwlock.address);
      printf_filtered ("Variable name: \t\t");
      if (sym_name)
        {
          printf_filtered ("%s", sym_name);
          if (sym_offset) 
            printf_filtered ((sizeof(sym_offset) > 4) ? 
                             " + 0x%llx" : " + 0x%lx", sym_offset);
        }
      else
        {
          printf_filtered ("unknown");
        }

      printf_filtered ("\nProcess shared: \t%s\n", 
                       rwlock->obj.rwlock.flags & PTHREAD_TRACE_MUT_PSHARED ? 
                       "y" : "n");
  
      printf_filtered ("Blocked count: \t\t%d\n", 
                       rwlock->obj.rwlock.waiter_count);
      printf_filtered ("Reader count: \t\t%d\n", 
                       rwlock->obj.rwlock.reader_count);
      printf_filtered ("Trylock failures: \t%d\n", 
                       rwlock->obj.rwlock.trylock_failures);
      printf_filtered ("Lock count: \t\t%d\n", rwlock->obj.rwlock.lock_count);
      printf_filtered ("Contended locks: \t%d\n\n", 
                       rwlock->obj.rwlock.contended_locks);
    }
  print_relations (rwlock);
}


/* This routine is called in response to info rwlock command */
static void
rwlock_info_command (char *exp, int from_tty)
{
  int ret;

  dont_repeat ();

  if (!target_has_stack) error ("No stack.");

  prune_threads ();
  target_find_new_threads ();

  /* Read additional thread info from librtc */
  ret = read_thread_trace_info();

  if (ret)
    report_read_thread_errors(ret, OBJT_RWLOCK);

  if (exp == NULL)
    {
      if (rwlock_count)
        {
          printf_filtered ("ID  OwnerID  NumBlkd  "
                           "Address             Name+Offset\n");
          for (int i = 0; i < rwlock_count; i++)
            print_basic_rwlock_info (sorted_rwlocks[i]);
        }
      else
        {
          error ("No read-write lock info.");
        }
    }
  else
    {
      if (!could_be_number(exp))
        error ("Invalid value %s. Read-write lock ID must "
               "be an integer.\n", exp);

      int rwlock_id = atoi (exp);
      gdb_pthread_object_t* rwlock = get_rwlock_obj (rwlock_id);
      if (rwlock == NULL)
        error ("Read-write lock ID %d not known.  Use the \"info rwlock\" "
               "command to\nsee the IDs of currently known "
               "read-write locks.", 
               rwlock_id);
      print_extended_rwlock_info (rwlock);
    }
}


/* Return a pointer to the condition variable with a given index */
static gdb_pthread_object_t*
get_condvar_obj (int index)
{
  for (int i = 0; i < cv_count; i++)
    {
      if (condvars[i].index == index)
        return &condvars[i];
    }
  return NULL;
}


/* Print a one-line basic condition variable info for a given condvar */
static void
print_basic_cond_info (gdb_pthread_object_t* cond)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;
  int mutex_id;
  assert(cond && cond->type == OBJT_COND);

  printf_filtered ("%-3d ", cond->index);
  mutex_id = pthread_to_gdb_id(cond->obj.cond.mutex_id);

  if (mutex_id == -1)
    printf_filtered ("none     ");
  else
    printf_filtered ("%-8d ", mutex_id);

  printf_filtered ("%-3d      ", cond->obj.cond.waiter_count);
  printf_filtered ((sizeof(cond->obj.cond.address) > 4) ? 
                   "0x%-16llx  " : "0x%-16lx  ", 
                   cond->obj.cond.address);

  sym_name = sym_info_addr(cond->obj.cond.address, &sym_offset,
                           get_creator(cond));
  if (sym_name)
    {
      printf_filtered ("%s", sym_name);
      if (sym_offset) 
        printf_filtered ((sizeof(sym_offset) > 4) ? 
                         " + 0x%llx" : " + 0x%lx", sym_offset);
    }
  printf_filtered ("\n");
}


/* Print extended condition variable info, including all of its relations,
   for a given condvar */
static void
print_extended_cond_info (gdb_pthread_object_t* cond)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;
  int mutex_id;
  assert(cond && cond->type == OBJT_COND);

  if (batch_rtc)
    {
       fprintf (thread_info_log_fp,
                "\nDetailed information on condition variable %d:\n",
                cond->index);
       fprintf (thread_info_log_fp,
                "---------------------------------------------\n");
       fprintf (thread_info_log_fp,
               "Condition variable ID: \t%-3d ", cond->index);
       if (!(cond->flags & OBJF_CREATED))
         fprintf (thread_info_log_fp,
               "(initialization incomplete)");
       fprintf (thread_info_log_fp, "\n");
 
       sym_name = sym_info_addr(cond->obj.cond.address, &sym_offset,
                                get_creator(cond));
       fprintf (thread_info_log_fp, "Variable address: \t");
       fprintf (thread_info_log_fp, (sizeof(cond->obj.cond.address) > 4) ?
                        "0x%-16llx\n" : "0x%-16lx\n", cond->obj.cond.address);
       fprintf (thread_info_log_fp, "Variable name: \t\t");
       if (sym_name)
         {
           fprintf (thread_info_log_fp, "%s", sym_name);
           if (sym_offset)
             fprintf (thread_info_log_fp, (sizeof(sym_offset) > 4) ? 
                              " + 0x%llx" : " + 0x%lx", sym_offset);
         }
       else
         {
           fprintf (thread_info_log_fp, "unknown");
         }
       fprintf (thread_info_log_fp, "\n");
     
       mutex_id = pthread_to_gdb_id(cond->obj.cond.mutex_id);
       if (mutex_id == -1)
         {
           fprintf (thread_info_log_fp, "Associated mutex: \tnone");
         }
       else
         {
           fprintf (thread_info_log_fp, 
                    "Associated mutex ID: \t%d\n", mutex_id);
           gdb_pthread_object_t* mutex = get_mutex_obj (mutex_id);
           if (mutex == NULL)
             error ("Internal error : can't find info for mutex %d.", mutex_id);
           sym_name = sym_info_addr(mutex->obj.mutex.address, &sym_offset,
                                    get_creator(mutex));
           fprintf (thread_info_log_fp, "Mutex variable name: \t");
           if (sym_name)
             {
               fprintf (thread_info_log_fp, "%s", sym_name);
               if (sym_offset) 
                 fprintf (thread_info_log_fp, (sizeof(sym_offset) > 4) ? 
                                  " + 0x%llx" : " + 0x%lx", sym_offset);
             }
           else
             {
               fprintf (thread_info_log_fp, "unknown");
             }
         }

       fprintf (thread_info_log_fp, "\nProcess shared: \t%s\n", 
                        cond->obj.cond.flags & PTHREAD_TRACE_MUT_PSHARED ? 
                        "y" : "n");

       fprintf (thread_info_log_fp,
                "Blocked count: \t\t%d\n", cond->obj.cond.waiter_count);
       fprintf (thread_info_log_fp,
                "Wait count: \t\t%d\n", cond->obj.cond.wait_count);
       fprintf (thread_info_log_fp,
               "Signal count: \t\t%d\n", cond->obj.cond.signal_count);
       fprintf (thread_info_log_fp,
               "Broadcast count: \t%d\n\n", 
                        cond->obj.cond.broadcast_count);
    }
  else
    {
       printf_filtered ("Condition variable ID: \t%-3d ", cond->index);
       if (!(cond->flags & OBJF_CREATED))
         printf_filtered ("(initialization incomplete)");
       printf_filtered ("\n");
 
       sym_name = sym_info_addr(cond->obj.cond.address, &sym_offset,
                                get_creator(cond));
       printf_filtered ("Variable address: \t");
       printf_filtered ((sizeof(cond->obj.cond.address) > 4) ?
                        "0x%-16llx\n" : "0x%-16lx\n", cond->obj.cond.address);
       printf_filtered ("Variable name: \t\t");
       if (sym_name)
         {
           printf_filtered ("%s", sym_name);
           if (sym_offset)
             printf_filtered ((sizeof(sym_offset) > 4) ? 
                              " + 0x%llx" : " + 0x%lx", sym_offset);
         }
       else
         {
           printf_filtered ("unknown");
         }
       printf_filtered ("\n");

       mutex_id = pthread_to_gdb_id(cond->obj.cond.mutex_id);
       if (mutex_id == -1)
         {
           printf_filtered ("Associated mutex: \tnone");
         }
       else
         {
           printf_filtered ("Associated mutex ID: \t%d\n", mutex_id);
           gdb_pthread_object_t* mutex = get_mutex_obj (mutex_id);
           if (mutex == NULL)
             error ("Internal error : can't find info for mutex %d.", mutex_id);
           sym_name = sym_info_addr(mutex->obj.mutex.address, &sym_offset,
                                    get_creator(mutex));
           printf_filtered ("Mutex variable name: \t");
           if (sym_name)
             {
               printf_filtered ("%s", sym_name);
               if (sym_offset) 
                 printf_filtered ((sizeof(sym_offset) > 4) ? 
                                  " + 0x%llx" : " + 0x%lx", sym_offset);
             }
           else
             {
               printf_filtered ("unknown");
             }
         }

       printf_filtered ("\nProcess shared: \t%s\n", 
                        cond->obj.cond.flags & PTHREAD_TRACE_MUT_PSHARED ? 
                        "y" : "n");

       printf_filtered ("Blocked count: \t\t%d\n", cond->obj.cond.waiter_count);
       printf_filtered ("Wait count: \t\t%d\n", cond->obj.cond.wait_count);
       printf_filtered ("Signal count: \t\t%d\n", cond->obj.cond.signal_count);
       printf_filtered ("Broadcast count: \t%d\n\n", 
                        cond->obj.cond.broadcast_count);
    }

  print_relations(cond);
}


/* This routine is called in response to info condvar command */
static void
cond_info_command (char *exp, int from_tty)
{
  int ret;

  dont_repeat ();

  if (!target_has_stack) error ("No stack.");

  prune_threads ();
  target_find_new_threads ();

  /* Read additional thread info from librtc */
  ret = read_thread_trace_info();

  if (ret)
      report_read_thread_errors(ret, OBJT_COND);

  if (exp == NULL)
    {
      if (cv_count)
        {
          printf_filtered ("ID  MutexID  NumBlkd  "
                           "Address             Name+Offset\n");
          for (int i = 0; i < cv_count; i++)
            print_basic_cond_info(sorted_condvars[i]);
        }
      else
        {
          error ("No condition variable info.");
        }
    }
  else
    {
      if (!could_be_number(exp))
        error ("Invalid value %s. Condition variable ID must "
               "be an integer.\n", exp);

      int cond_id = atoi (exp);
      gdb_pthread_object_t* cond = get_condvar_obj (cond_id);
      if (cond == NULL)
        error ("Condition variable ID %d not known.  Use the "
               "\"info condvar\" command to\nsee the IDs of "
               "currently known condition variables.", 
               cond_id);
      print_extended_cond_info(cond);
    }
}


/* Print extended info for a given thread */
void
print_extended_thread_info (struct thread_info* tp, int current_pid)
{
  char* sym_name;
  CORE_ADDR sym_offset = 0;
  gdb_pthread_object_t* thread = tp->rtc_thread;
  int utid;
  int thread_id;

  if (batch_rtc)
    {
#if defined(GDB_TARGET_IS_HPUX)
      /* If thread has no associated utid, display negative of Kernel tid */
      if (is_mxn)
        {
          utid = get_pthread_for (tp->pid);
          thread_id = (utid == -1)? -(get_lwp_for (tp->pid)): utid;
        }
      else
        {
          thread_id = tp->num;
        }
#else
      thread_id = tp->num;
#endif
      fprintf (thread_info_log_fp,
              "\nDetailed information on thread %d:\n", thread_id);
      fprintf (thread_info_log_fp,
              "---------------------------------\n");
      fprintf (thread_info_log_fp, "Thread ID: \t\t\t");

#if defined(GDB_TARGET_IS_HPUX)
      fprintf (thread_info_log_fp, "%d, %s", thread_id,
                   target_tid_to_str (tp->pid));

      if (!(thread->flags & OBJF_CREATED))
        fprintf (thread_info_log_fp, " (initialization incomplete)");
      fprintf (thread_info_log_fp, "\n");

      /* Get the thread priority using pstat for live process. */
#ifdef GDB_TARGET_IS_HPPA
#ifdef CMA_THREAD_FLAG
      if (!(IS_CMA_PID(current_pid)))
#endif
#endif
        if (target_has_execution)
          {
            struct lwp_status buf;
            int count;
            bzero (&buf, sizeof (struct lwp_status));
            count = pstat_getlwp (&buf, sizeof (struct lwp_status), 0, 
                                  get_lwp_for (tp->pid), get_pid_for (tp->pid));
            if (count > 0)
              {
                fprintf (thread_info_log_fp, "Priority: \t\t\t%lli\n", buf.lwp_pri);
              }
          }
#else
          fprintf (thread_info_log_fp, "%d, %s\n", tp->num, target_pid_to_str (tp->pid));
          if (!(thread->flags & OBJF_CREATED))
            fprintf (thread_info_log_fp, " (initialization incomplete)");
          fprintf (thread_info_log_fp, "\n");
#endif

      fprintf (thread_info_log_fp, "Current thread: \t\t%s\n", 
                       tp->pid == current_pid ? "y" : "n");
      fprintf (thread_info_log_fp, "Last event thread: \t\t%s\n", 
                       tp->pid == last_event_thread ? "y" : "n");
      fprintf (thread_info_log_fp, "Disabled thread: \t\t%s\n", tp->disabled ? "y" : "n");
    
      /* If there is no extended thread info, return */
      if (thread == NULL) return;
    
      fprintf (thread_info_log_fp, "Detached: \t\t\t%s\n", 
                       thread->obj.thread.flags & PTHREAD_TRACE_THD_DETACH ?
                       "y" : "n");
      sym_name = sym_info_addr(thread->obj.thread.address, &sym_offset,
                               get_creator(thread));
      fprintf (thread_info_log_fp, "Thread variable address: \t");
      fprintf (thread_info_log_fp, (sizeof(thread->obj.thread.address) > 4) ? 
                       "0x%llx\n" : "0x%lx\n", thread->obj.thread.address);
      fprintf (thread_info_log_fp, "Thread variable name: \t\t");
      if (sym_name)
        {
          fprintf (thread_info_log_fp, "%s", sym_name);
          if (sym_offset)
            fprintf (thread_info_log_fp, (sizeof(sym_offset) > 4) ? 
                             " + 0x%llx" : " + 0x%lx", sym_offset);
        }
      else
        {
          fprintf (thread_info_log_fp, "unknown");
        }

      fprintf (thread_info_log_fp, "\nStart routine address: \t\t");
      fprintf (thread_info_log_fp, (sizeof(thread->obj.thread.start_routine) > 4) ? 
                       "0x%llx\n" : "0x%lx\n", thread->obj.thread.start_routine);
      sym_name = sym_info_addr(thread->obj.thread.start_routine, &sym_offset,
                               get_creator(thread));
      fprintf (thread_info_log_fp, "Start routine name (argument): \t");
      if (sym_name)
          fprintf (thread_info_log_fp, "%s", sym_name);
      else
          fprintf (thread_info_log_fp, "unknown");
      fprintf (thread_info_log_fp, (sizeof(thread->obj.thread.start_arg) > 4) ?
                       " (0x%llx)\n" : " (0x%lx)\n", thread->obj.thread.start_arg);
    
      fprintf (thread_info_log_fp, "Cancel state: \t\t\t%s\n", 
                       thread->obj.thread.cancel & PTHREAD_TRACE_CANCEL_STATE ?
                       "enabled" : "disabled");
      fprintf (thread_info_log_fp, "Contention scope: \t\t%s\n", 
                       thread->obj.thread.flags & PTHREAD_TRACE_THD_SYS_SCOPE ?
                       "system" : "process");

      fprintf (thread_info_log_fp, "Scheduling policy: \t\t");
      switch (thread->obj.thread.policy)
        {
          case SCHED_FIFO:
              fprintf (thread_info_log_fp,
                       "SCHED_FIFO; strict First-In/First-Out policy\n");
              break;
          case SCHED_RR:
              fprintf (thread_info_log_fp,
                       "SCHED_RR; FIFO, with a Round-Robin interval\n");
              break;
          case SCHED_HPUX:
              fprintf (thread_info_log_fp,
                       "SCHED_HPUX; default HP-UX scheduling policy\n");
              break;
          case SCHED_RR2:
              fprintf (thread_info_log_fp,
                       "SCHED_RR2; Round-Robin, with a per-priority "
                               "RR interval\n");
              break;
          case SCHED_RTPRIO:
              fprintf (thread_info_log_fp,
                       "SCHED_RTPRIO; HP-UX rtprio(2) realtime "
                               "scheduling\n");
              break;
          case SCHED_NOAGE:
              fprintf (thread_info_log_fp,
                       "SCHED_NOAGE; HPUX without decay from usage\n");
              break;
          default:
              fprintf (thread_info_log_fp, "unknown\n");
              break;
        }

      fprintf (thread_info_log_fp, "Stack base: \t\t\t");
      fprintf (thread_info_log_fp,
               (sizeof(thread->obj.thread.stack_base) > 4) ?
               "0x%llx\n" : "0x%lx\n", thread->obj.thread.stack_base);
      fprintf (thread_info_log_fp, "Stack size: \t\t\t");
      fprintf (thread_info_log_fp, "0x%x\n", thread->obj.thread.stack_size);
      fprintf (thread_info_log_fp, "Stack high watermark: \t\t");
      fprintf (thread_info_log_fp,
                (sizeof(thread->obj.thread.stack_highwater) > 4) ?
                "0x%llx\n\n" : "0x%lx\n\n", 
                thread->obj.thread.stack_highwater);
    }
  else
    {
      printf_filtered ("Thread ID: \t\t\t");

#if defined(GDB_TARGET_IS_HPUX)
      /* If thread has no associated utid, display negative of Kernel tid */
      if (is_mxn)
        {
          utid = get_pthread_for (tp->pid);
          printf_filtered ("%d, %s", 
                           (utid == -1)? -(get_lwp_for (tp->pid)): utid, 
                           target_tid_to_str (tp->pid));
        }
      else
        {
          printf_filtered ("%d, %s", tp->num, target_tid_to_str (tp->pid));
        }

      if (!(thread->flags & OBJF_CREATED))
        printf_filtered (" (initialization incomplete)");
      printf_filtered ("\n");

      /* Get the thread priority using pstat for live process. */
#ifdef GDB_TARGET_IS_HPPA
#ifdef CMA_THREAD_FLAG
      if (!(IS_CMA_PID(current_pid)))
#endif
#endif
        if (target_has_execution)
          {
            struct lwp_status buf;
            int count;
            bzero (&buf, sizeof (struct lwp_status));
            count = pstat_getlwp (&buf, sizeof (struct lwp_status), 0, 
                                  get_lwp_for (tp->pid), get_pid_for (tp->pid));
            if (count > 0)
              {
                printf_filtered ("Priority: \t\t\t%lli\n", buf.lwp_pri);
              }
          }
#else
          printf_filtered ("%d, %s\n", tp->num, target_pid_to_str (tp->pid));
          if (!(thread->flags & OBJF_CREATED))
            printf_filtered (" (initialization incomplete)");
          printf_filtered ("\n");
#endif

      printf_filtered ("Current thread: \t\t%s\n", 
                       tp->pid == current_pid ? "y" : "n");
      printf_filtered ("Last event thread: \t\t%s\n", 
                       tp->pid == last_event_thread ? "y" : "n");
      printf_filtered ("Disabled thread: \t\t%s\n", tp->disabled ? "y" : "n");

      /* If there is no extended thread info, return */
      if (thread == NULL) return;

      printf_filtered ("Detached: \t\t\t%s\n", 
                       thread->obj.thread.flags & PTHREAD_TRACE_THD_DETACH ?
                       "y" : "n");
      sym_name = sym_info_addr(thread->obj.thread.address, &sym_offset,
                               get_creator(thread));
      printf_filtered ("Thread variable address: \t");
      printf_filtered ((sizeof(thread->obj.thread.address) > 4) ? 
                       "0x%llx\n" : "0x%lx\n", thread->obj.thread.address);
      printf_filtered ("Thread variable name: \t\t");
      if (sym_name)
        {
          printf_filtered ("%s", sym_name);
          if (sym_offset)
            printf_filtered ((sizeof(sym_offset) > 4) ? 
                             " + 0x%llx" : " + 0x%lx", sym_offset);
        }
      else
        {
          printf_filtered ("unknown");
        }

      printf_filtered ("\nStart routine address: \t\t");
      printf_filtered ((sizeof(thread->obj.thread.start_routine) > 4) ? 
                       "0x%llx\n" : "0x%lx\n", thread->obj.thread.start_routine);
      sym_name = sym_info_addr(thread->obj.thread.start_routine, &sym_offset,
                               get_creator(thread));
      printf_filtered ("Start routine name (argument): \t");
      if (sym_name)
          printf_filtered ("%s", sym_name);
      else
          printf_filtered ("unknown");
      printf_filtered ((sizeof(thread->obj.thread.start_arg) > 4) ?
                   " (0x%llx)\n" : " (0x%lx)\n", thread->obj.thread.start_arg);

      printf_filtered ("Cancel state: \t\t\t%s\n", 
                       thread->obj.thread.cancel & PTHREAD_TRACE_CANCEL_STATE ?
                       "enabled" : "disabled");
      printf_filtered ("Contention scope: \t\t%s\n", 
                       thread->obj.thread.flags & PTHREAD_TRACE_THD_SYS_SCOPE ?
                       "system" : "process");

      printf_filtered ("Scheduling policy: \t\t");
      switch (thread->obj.thread.policy)
        {
          case SCHED_FIFO:
              printf_filtered ("SCHED_FIFO; strict First-In/First-Out policy\n");
              break;
          case SCHED_RR:
              printf_filtered ("SCHED_RR; FIFO, with a Round-Robin interval\n");
              break;
          case SCHED_HPUX:
              printf_filtered ("SCHED_HPUX; default HP-UX scheduling policy\n");
              break;
          case SCHED_RR2:
              printf_filtered ("SCHED_RR2; Round-Robin, with a per-priority "
                               "RR interval\n");
              break;
          case SCHED_RTPRIO:
              printf_filtered ("SCHED_RTPRIO; HP-UX rtprio(2) realtime "
                               "scheduling\n");
              break;
          case SCHED_NOAGE:
              printf_filtered ("SCHED_NOAGE; HPUX without decay from usage\n");
              break;
          default:
              printf_filtered ("unknown\n");
              break;
        }

      printf_filtered ("Stack base: \t\t\t");
      printf_filtered ((sizeof(thread->obj.thread.stack_base) > 4) ?
                       "0x%llx\n" : "0x%lx\n", thread->obj.thread.stack_base);
      printf_filtered ("Stack size: \t\t\t");
      printf_filtered ((sizeof(thread->obj.thread.stack_size) > 4) ?
                       "0x%llx\n" : "0x%lx\n", thread->obj.thread.stack_size);
      printf_filtered ("Stack high watermark: \t\t");
      printf_filtered ((sizeof(thread->obj.thread.stack_highwater) > 4) ?
                       "0x%llx\n\n" : "0x%lx\n\n", 
                       thread->obj.thread.stack_highwater);
   }

  print_relations(thread);
}

static int
get_currently_executing_thread_id ()
{
  int current_pid = inferior_pid;
#ifdef FIND_ACTIVE_THREAD
  if (!(IS_CMA_PID(current_pid)))
    {
      current_pid = FIND_ACTIVE_THREAD ();
    }
#endif
  return current_pid;
}


/* This is a wrapper routine for print_extended_thread_info. This
   is being currently used for batch mode thread checking. 
   The intention is to convert the thread id passed to the right
   parameters to be used as input to print_extended_thread_info().
   'id' represents the utid of the thread. Search through the
   infttrace thread list to get the gdb_tid. Pass this gdb_tid as
   'pid' to find_thread_pid() to get the general thread info
   structure. */
static void
extended_thread_info_wrapper(int64_t id)
{
  struct thread_info *tp = NULL;
  tp = find_thread_pid (get_gdb_tid_from_utid (id));

  print_extended_thread_info (tp, get_currently_executing_thread_id());
}


/* Compare function for two objects used to sort objects
   in order of increasing indexes */
static int 
obj_index_compare(const void* obj1, const void* obj2)
{
  return (*((gdb_pthread_object_t**) obj1))->index - 
    (*((gdb_pthread_object_t**) obj2))->index;
}


/* Read the data from /tmp/__thread_info, which produced by a command
   line call to librtc's __rtc_pthread_info() */
static void 
download_thread_data (char* thread_info_filename)
{
  FILE* fp;
  gdb_pthread_object_t tmp_obj;
  gdb_pthread_object_t* cur_obj = NULL;
  uint32_t cur_thread = 0;
  uint32_t cur_mutex = 0;
  uint32_t cur_rwlock = 0;
  uint32_t cur_cv = 0;
  uint32_t total_count;

  fp = fopen (thread_info_filename, "r");
  if (!fp)
    error ("Pthread analysis file missing!");

  /* Read the number of pthread objects first */
  fread (&thread_count, sizeof(thread_count), 1, fp);
  fread (&mutex_count, sizeof(mutex_count), 1, fp);
  fread (&rwlock_count, sizeof(rwlock_count), 1, fp);
  fread (&cv_count, sizeof(cv_count), 1, fp);

  /* Allocate space to store downloaded data */
  if (thread_count)
    {
      threads = xmalloc (thread_count * sizeof(gdb_pthread_object_t));
      memset (threads, 0, thread_count * sizeof(gdb_pthread_object_t));
    }
  if (mutex_count)
    {
      mutexes = xmalloc (mutex_count * sizeof(gdb_pthread_object_t));
      memset (mutexes, 0, mutex_count * sizeof(gdb_pthread_object_t));
      sorted_mutexes = xmalloc (mutex_count * sizeof(gdb_pthread_object_t*));
    }
  if (rwlock_count)
    {
      rwlocks = xmalloc (rwlock_count * sizeof(gdb_pthread_object_t));
      memset (rwlocks, 0, rwlock_count * sizeof(gdb_pthread_object_t));
      sorted_rwlocks = xmalloc (rwlock_count * sizeof(gdb_pthread_object_t*));
    }
  if (cv_count)
    {
      condvars = xmalloc (cv_count * sizeof(gdb_pthread_object_t));
      memset (condvars, 0, cv_count * sizeof(gdb_pthread_object_t));
      sorted_condvars = xmalloc (cv_count * sizeof(gdb_pthread_object_t*));
    }

  total_count = thread_count + mutex_count + rwlock_count + cv_count;
  for (int i = 0; i < total_count; i++)
    {
      /* Read in an object */
      if (fread (&tmp_obj, offsetof(gdb_pthread_object_t, next), 1, fp) != 1)
        {
          fclose (fp);
          unlink (thread_info_filename);
          error ("Can't read records from pthread analysis file.");
        }

      switch(tmp_obj.type)
        {
          case OBJT_THREAD:
              cur_obj = &threads[cur_thread];
              memcpy (cur_obj, &tmp_obj, offsetof(gdb_pthread_object_t, next));
              cur_thread++;
              break;
              
          case OBJT_MUTEX:
              cur_obj = &mutexes[cur_mutex];
              memcpy (cur_obj, &tmp_obj, offsetof(gdb_pthread_object_t, next));
              sorted_mutexes[cur_mutex] = cur_obj;
              cur_mutex++;
              break;
              
          case OBJT_RWLOCK:
              cur_obj = &rwlocks[cur_rwlock];
              memcpy (cur_obj, &tmp_obj, offsetof(gdb_pthread_object_t, next));
              sorted_rwlocks[cur_rwlock] = cur_obj;
              cur_rwlock++;
              break;
              
          case OBJT_COND:
              cur_obj = &condvars[cur_cv];
              memcpy (cur_obj, &tmp_obj, offsetof(gdb_pthread_object_t, next));
              sorted_condvars[cur_cv] = cur_obj;
              cur_cv++;
              break;
              
          default:
              fclose (fp);
              unlink (thread_info_filename);
              error ("Bad object type in pthread analysis file.");
        }

      /* Read in this object's relations */
      if (cur_obj->num_rels)
        {
          cur_obj->rel_list = 
              xmalloc (cur_obj->num_rels * sizeof(gdb_relation_t));
          memset (cur_obj->rel_list, 0, 
                  cur_obj->num_rels * sizeof(gdb_relation_t));
        }

      for (int j = 0; j < cur_obj->num_rels; j++)
        {
          /* Read header info */
          gdb_relation_t* cur_rel = &cur_obj->rel_list[j];
          if (fread (cur_rel, offsetof(gdb_relation_t, frames), 1, fp) != 1)
            {
              fclose (fp);
              unlink (thread_info_filename);
              error ("Can't read records from pthread analysis file.");
            }

          /* Read the stack frames */
          if (cur_rel->frame_count)
            {
              cur_rel->frames = 
                  xmalloc (cur_rel->frame_count * sizeof(CORE_ADDR));
              if (fread (cur_rel->frames, sizeof(CORE_ADDR), 
                         cur_rel->frame_count, fp) != cur_rel->frame_count)
                {
                  fclose (fp);
                  unlink (thread_info_filename);
                  error ("Can't read records from pthread analysis file.");
                }
            }  
        }

      /* Add this object to the hash table */
      object_hash_add (cur_obj);
    }

  if (cur_thread != thread_count || cur_mutex != mutex_count ||
      cur_rwlock != rwlock_count || cur_cv != cv_count) 
    {
      fclose (fp);
      unlink (thread_info_filename);
      error ("Pthread analysis file is corrupted.");
    }
  
  fclose (fp);
  unlink (thread_info_filename);

  /* Sort arrays of objects for better output readability */
  qsort (sorted_mutexes, mutex_count, sizeof(gdb_pthread_object_t*),
         obj_index_compare);
  qsort (sorted_rwlocks, rwlock_count, sizeof(gdb_pthread_object_t*),
         obj_index_compare);
  qsort (sorted_condvars, cv_count, sizeof(gdb_pthread_object_t*),
         obj_index_compare);
}


/* Given an object, remove all it's data that was allocated dynamically,
   namely, remove all relations */
static void
clear_object (gdb_pthread_object_t* obj)
{
  for (int i = 0; i < obj->num_rels; i++)
    {
      gdb_relation_t* rel = &obj->rel_list[i];
      if (rel->frames) free(rel->frames);
    }
  if (obj->rel_list) free(obj->rel_list);
}


/* Clear all arrary that hold tread checking data. This routine is called
   every time thread data is re-read from librtc. */
static void 
clear_old_pthread_data (void)
{
  int i;

  /* Re-set the hash table */
  memset(object_hash, 0, sizeof(gdb_pthread_object_t*) * OBJECT_HASH_SIZE);

  for (i = 0; i < thread_count; i++) clear_object(&threads[i]);
  if (threads) free(threads);
  for (i = 0; i < mutex_count; i++) clear_object(&mutexes[i]);
  if (mutexes) free(mutexes);
  for (i = 0; i < rwlock_count; i++) clear_object(&rwlocks[i]);
  if (rwlocks) free(rwlocks);
  for (i = 0; i < cv_count; i++) clear_object(&condvars[i]);
  if (condvars) free(condvars);

  thread_count = mutex_count = rwlock_count = cv_count = 0;
  threads = mutexes = rwlocks = condvars = NULL;
}


/* We want to avoid inconsisten data coming in from librtc. For that librtc
   maintains a so-called critical thread: it's a thread which is currently
   in process of updating thread-checking data structures. We make an 
   attempt here to continue that thread until it gets out of the critical
   region of code so we can get a consistent view of the data we are
   about to download. */
static void
continue_critical_thread (void)
{
  char buffer[16];
  struct minimal_symbol* m;
  CORE_ADDR safe_addr = 0;
  int saved_pid;

  /* Read the ID of the critical thread from librtc */
  int64_t thread_id = get_inferior_var_value("__rtc_critical_thread", 8);
  /* None of the threads are in the critical section, just return */
  if (thread_id == -1) return;
    
  m = lookup_minimal_symbol("__rtc_pthread_dummy", 0, 0);
  if (!m) 
#ifdef GDB_TARGET_IS_HPUX
    error ("Internal error : __rtc_pthread_dummy missing in librtc!\n"
           "It might be due to version mismatch of librtc and "
           "gdb(Version %s).\nHint: Use environment variable "
           "LIBRTC_SERVER to pick correct "
           "version of librtc.", wdb_version_nbr);
#else
    error ("Internal error : __rtc_pthread_dummy missing in librtc!");
#endif  

  safe_addr = SYMBOL_VALUE_ADDRESS (m);

  /* Insert a breakpoint on exit from the critical section */
  /* JAGag26864 - Call target_insert_internal_breakpoint instead of
     target_insert_breakpoint so that if software breakpoint fails,
     hardware will be inserted.
     If hardware insertion also fails, give chatr +dbg enable message.
  */
  int val = target_insert_internal_breakpoint (safe_addr, buffer);
  if (val)
    {
#ifdef HP_IA64
      struct utsname uts;
      int sys_ver = 0;
      uname (&uts);
      if (sscanf (uts.release, "B.11.%d", &sys_ver) != 1)
      warning ("uname: Couldn't resolve the release identifier of"
               " the Operating system.");
      if (sys_ver == 31)
       {
         uint64_t value = 0;
         /* Check whether the varibale is set. If not present
            ask user to install the patches.
         */
         if (gettune((const char *)"shlib_debug_enable" , &value) == -1 && errno == ENOENT)
           {             
	     warning ("The shared libraries were not privately mapped; "
                      "Thread-check output may be inconsistent.\n"
                      "Use the following command to enable debugging of shared "
		      "libraries private.\nchatr +dbg enable <executable>\n"
                      "or install the patches PHKL_38651 and PHKL_38778");
	   }
         else if (value == 0)
           warning ("The shared libraries were not privately "
                    "mapped;\n Debugger will not be able to do "
                    "Java debugging.\n"
                    "Please set the kernel variable \"shlib_debug_enable\""
                    " to 1 to enable the shared library debugging\n");

       }
      else 
#endif
        warning ("The shared libraries were not privately mapped; "
                 "Thread-check output may be inconsistent.\n"
                 "Use the following command to enable debugging of "
		 "shared libraries private.\nchatr +dbg enable <executable>");
      return;	 	
    } 

  tid_t gdb_tid = get_gdb_tid_from_utid (thread_id);
  errno = 0;
  /* Suresh - Fix for occasional hang..; if you set rtc_in_progress here
    it results in setting the global wait_on_tid, which makes the subsequent
    target_wait(<all threads>) we do below to just wait on 1 thread 
    instead, irrespective of what argument we pass to target_wait()
  */
  rtc_in_progress = 0;

  /* We need to save and restore inferior_pid because in target_resume()
     the pid we pass as and argument is ignored inder RTC. */
  saved_pid = inferior_pid;
  inferior_pid = gdb_tid;
  if (thread_RTC_debug1 == 1)
  {
    printf (" \nStack trace before resuming the critical thread \n");
    backtrace_command_1("18",1, 1);
  }
  /* Suresh: Here do we need to resume all threads instead of just one 
     thread?? The fact that there is a mutex lock for thread RTC, 
     all the threads cannot enter into RTC and produce multiple 
     thread events. So we are probably safe ??
 
     But when you resume all threads you run into other issues as GDB 
     is not designed to resume all threads, this much deep in the
     processing of a breakpoint event. Remember this routinue is executed
     in the context of breakpoint processing, and GDB gets confused by 
     the unprocessed SIGTRAP because this breakpoint is not fully serviced 
     as yet!

     FIXME: We need to see if we need to somehow fix this in future??( to 
     indirectly make sure there are no deadlocks when we resume just the
     critical thread)
  */ 
  target_resume (gdb_tid, 0, 0);
  inferior_pid = saved_pid;
  rtc_in_progress = 0;
  if (errno == 0)
    {
      struct target_waitstatus sw;
      registers_changed ();

      /* Suresh - We need to wait on all threads and not just the current
         thread
       */
      target_wait (-1, &sw);
#if 0
      target_wait (gdb_tid, &sw);
#endif
    }
  target_remove_internal_breakpoint (safe_addr, buffer);
#ifdef HP_IA64
  invalidate_rse_info ();
#endif
}


/* Call __rtc_pthread_info() in the inferior to generate thread trace
   data and read the data in from the temp file into gdb internal data
   structures. Returns an error code or RTC_NO_ERROR if successful. */
int
read_thread_trace_info (void)
{
  struct thread_info* thr_list;
  value_ptr funcval, val;
  long num_records;
  char thread_info_filename[50];
  int real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
					PIDGET(inferior_pid) :
					get_pid_for(inferior_pid));

  if (!trace_threads_in_this_run)
      return RTC_DISABLED;
      
  rtc_dot_sl = get_rtc_dot_sl();

  /* On PA64 and IPF, it is possible that we get here without the
     necessary libs */ 
  if (!rtc_dot_sl)
      return RTC_NO_LIBRTC;

  if (!target_has_stack)
      return RTC_NO_STACK;

  if (cur_thread_in_syscall ())
      return RTC_IN_SYSCALL;

  /* Before we call the function in the inferior to retrieve thread
     info, we need to make sure that none of the threads are in the
     thread event processing critical section */
  continue_critical_thread ();

  sprintf (thread_info_filename, "/tmp/__thread_info.%u", real_inferior_pid);

  /* Remove the data file in case it didn't get removed */
  unlink (thread_info_filename);

  funcval = find_function_in_inferior ("__rtc_pthread_info");
  rtc_in_progress = 1;
  val = call_function_by_hand (funcval, 0, 0);
  rtc_in_progress = 0;
  num_records = (long) value_as_long (val);

  /* Clear old pthread data */
  clear_old_pthread_data ();

  /* If there are records to read, download them */
  if (num_records > 0)
  {
      download_thread_data (thread_info_filename);
  }
  else if (num_records == 0)
  {
      unlink (thread_info_filename);
      return RTC_NO_RECORDS;
  }
  else
  {
      /* If num_records < 0 it indicates an error code from 
         __rtc_pthread_info() */
      return num_records;
  }

  /* Populate the thread list with extended thread info */
  for (int i = 0; i < thread_count; i++)
  {
      gdb_pthread_object_t* thr = &threads[i];
      tid_t gdb_tid = get_gdb_tid_from_utid(thr->id);
      struct thread_info* thr_info = find_thread_pid(gdb_tid);
      if (thr_info) thr_info->rtc_thread = thr;
  }

  return RTC_NO_ERROR;
}

/* QXCR1000757191: Enable being able to specify the o/p dir for batch
   mode thread check. Save the output directory specified with 
   -threads-logdir to threads_output_dir, so that it can be used
   later to create the thread error event file.
*/
void set_threads_output_dir (char * optarg)
{
    if (optarg && optarg[0])
      threads_output_dir = xstrdup (optarg);
    else
      threads_output_dir = xstrdup (".");
}


static FILE *
init_thread_info_log_file ()
{
   FILE *fp = NULL;
   char  output_file[2*PATH_MAX + 1];
   int fd;
   int real_inferior_pid;
   char *exec_file = NULL;
   static int log_file_msg_done = 0;

   if (thread_info_log_file_name == NULL)
     {
       real_inferior_pid = (is_process_id(PIDGET(inferior_pid)) ?
	                    PIDGET(inferior_pid) :
			    get_pid_for(inferior_pid));

       exec_file = basename (get_exec_file (0));
       sprintf (output_file, "%s/%s.%d.%s", threads_output_dir,
         exec_file, real_inferior_pid, "threads");
       DEBUG (printf ("Trying thread output file %s\n", output_file);)
       thread_info_log_file_name = xstrdup(output_file);
     }

   free (threads_output_dir);
   assert (thread_info_log_file_name != NULL);
   fd = open (thread_info_log_file_name,
              O_WRONLY | O_CREAT | O_APPEND | O_DSYNC | O_SYNC,
              0777);
   if (fd != -1 && (fp = fdopen (fd, "a+")) != NULL) 
     {
       DEBUG (printf ("opened %s\n", thread_info_log_file_name););
     }
   else
     {
       perror ("Could not open the Thread Error Log File\n");
     }

  thread_info_log_fd = fd;
  if (fp == NULL)
    {
      /* use stderr if can not open output file */
      fp = stderr;
      thread_info_log_to_stderr = 1;
      free (thread_info_log_file_name);
      thread_info_log_file_name = xstrdup ("stderr");
    }
  else if (!log_file_msg_done)
    {
      fprintf(stderr,
              "warning: Thread error event info is written to \"%s\".\n",
              thread_info_log_file_name);
      log_file_msg_done = 1;
    }
  return fp;
}
   
static void
print_event_to_log_file (enum rtc_event ecode,
                         gdb_pthread_object_t *obj1,
                         gdb_pthread_object_t *obj2,
                         gdb_pthread_object_t *obj3)
{
  static int thread_error_event_nbr = 1;
  int thread_involved = 0;

  if (thread_info_log_fp == NULL)
    thread_info_log_fp = init_thread_info_log_file();
  else
    {
      if (thread_info_log_to_stderr)
         thread_info_log_fp = stderr;
      else
        assert(thread_info_log_fp != NULL);
    }

  assert (thread_info_log_fp != NULL);

  fprintf(
    thread_info_log_fp,
    "\n\n===============================================================\n");
  fprintf(thread_info_log_fp, "%d. THREAD ERROR EVENT: ",
          thread_error_event_nbr++);
  switch (ecode)
    {
      case RTC_NUM_WAITERS:
        {
          char* obj_name = NULL;
          assert (obj1);
          switch (obj1->type)
            {
              case OBJT_THREAD: 
                obj_name = "thread";
                break;
              case OBJT_MUTEX: 
                obj_name = "mutex";
                break;
              case OBJT_RWLOCK: 
                obj_name = "read-write lock";
                break;
              case OBJT_COND: 
                obj_name = "condition variable";
                break;
              default:
                obj_name = "pthread object";
                break;
            }
          thr_num_waiters = (long) obj3;
          if (obj1->type == OBJT_THREAD)
            {
              fprintf (thread_info_log_fp,
                   "Number of waiters on %s %lld exceeded threshold of %d.\n",
                   obj_name, obj1->id, thr_num_waiters);
              extended_thread_info_wrapper (obj1->id);
            }
          else
            {
              fprintf (thread_info_log_fp,
                      "Number of waiters on %s %d exceeded threshold of %d.\n",
                      obj_name, obj1->index, thr_num_waiters);
              if (obj1->type == OBJT_MUTEX)
                {
                  print_extended_mutex_info (obj1);
                }
              else if (obj1->type == OBJT_RWLOCK)
                {
                  print_extended_rwlock_info (obj1);
                }
              else if (obj1->type == OBJT_COND)
                {
                  print_extended_cond_info (obj1);
                }
            }
          break;
        }
      case RTC_NON_RECURSIVE_RELOCK:
        {
          assert (obj1->type == OBJT_MUTEX || obj1->type == OBJT_RWLOCK);
          assert (obj2->type == OBJT_THREAD);
          fprintf (thread_info_log_fp,
                  "Attempt to recursively acquire non-recursive %s %d "
                  "from thread %lld.\n", 
                  obj1->type == OBJT_MUTEX ? "mutex" : "read-write lock",
                  obj1->index, obj2->id);
          extended_thread_info_wrapper (obj2->id);
          if (obj1->type == OBJT_MUTEX)
            print_extended_mutex_info (obj1);
          else
            print_extended_rwlock_info (obj1);
          break;
        }
      case RTC_UNLOCK_NOT_OWN:
        {
          assert (obj1->type == OBJT_MUTEX || obj1->type == OBJT_RWLOCK);
          assert (obj2->type == OBJT_THREAD);
          fprintf (thread_info_log_fp,
                   "Attempt to unlock %s %d not owned by thread %lld.\n",
                   obj1->type == OBJT_MUTEX ? "mutex" : "read-write lock",
                   obj1->index, obj2->id);
          if (obj1->type == OBJT_MUTEX)
            print_extended_mutex_info (obj1);
          else
            print_extended_rwlock_info (obj1);
          extended_thread_info_wrapper (obj2->id);
          break;
        }
      case RTC_MIXED_SCHED_POLICY:
        {
          assert (obj1->type == OBJT_THREAD);
          assert (obj2->type == OBJT_THREAD);
          fprintf (thread_info_log_fp,
                  "Attempt to synchronize threads %lld and %lld with different "
                  "scheduling policies.\n", obj1->id, obj2->id);
          extended_thread_info_wrapper (obj1->id);
          extended_thread_info_wrapper (obj2->id);
          break;
        }
      case RTC_CONDVAR_MULTIPLE_MUTEXES:
        {
          assert (obj1->type == OBJT_COND);
          assert (obj2->type == OBJT_MUTEX);
          assert (obj3->type == OBJT_MUTEX);
          fprintf (thread_info_log_fp,
                  "Attempt to associate condition variable %d with mutexes "
                  "%d and %d.\n", obj1->index, obj2->index, 
                  obj3->index);

          print_extended_cond_info (obj1);
          print_extended_mutex_info (obj2);
          print_extended_mutex_info (obj3);
          break;
        }
      case RTC_CONDVAR_WAIT_NO_MUTEX:
        {
          assert (obj1->type == OBJT_THREAD);
          assert (obj2->type == OBJT_COND);
          assert (obj3->type == OBJT_MUTEX);
          fprintf (thread_info_log_fp, 
                   "Attempt by thread %lld to wait on condition variable %d "
                   "without locking the associated mutex %d.\n", obj1->id,
                   obj2->index, obj3->index);
          extended_thread_info_wrapper (obj1->id);
          print_extended_cond_info (obj2);
          print_extended_mutex_info (obj3);
          break;
        }
      case RTC_DELETED_OBJ_USED:
        {
          fprintf (thread_info_log_fp, 
                   "Attempt to use a deleted object.\n");
          break;
        }
      case RTC_THREAD_EXIT_OWN_MUTEX:
        {
          assert (obj1->type == OBJT_THREAD);
          assert (obj2->type == OBJT_MUTEX || obj2->type == OBJT_RWLOCK);
          fprintf (thread_info_log_fp, 
                   "Attempt to exit thread %lld while holding a %s %d.\n\n",
                   obj1->id, 
                   obj2->type == OBJT_MUTEX ? "mutex" : "read-write lock",
                   obj2->index);
          /* Print extended information regarding the concerned objects.*/
          extended_thread_info_wrapper (obj1->id);
          if (obj2->type == OBJT_MUTEX)
            {
              print_extended_mutex_info (obj2);
            }
          else
            {
              print_extended_rwlock_info (obj2);
            }
          break;
        }
      case RTC_THREAD_EXIT_NO_JOIN_DETACH:
        {
          assert (obj1->type == OBJT_THREAD);
          fprintf (thread_info_log_fp, 
                   "Attempt to exit thread %lld which has neither been joined "
                   "nor detached.\n\n", obj1->id);
          break;
        }
      case RTC_THREAD_STACK_UTILIZATION:
        {
          assert (obj1->type == OBJT_THREAD);
          fprintf (
            thread_info_log_fp, 
            "Thread %lld exceeded stack utilization threshold of %ld%%.\n\n",
            obj1->id, (long) obj2);
          extended_thread_info_wrapper (obj1->id);
          break;
        }
      default:
        {  
           fprintf(thread_info_log_fp, "Unknown event %d\n", 
                   ecode);
           break;
        }
    }
  fprintf (thread_info_log_fp, 
           "Backtrace at event occurrence (Currently executing thread: %d)\n",
           get_pthread_for (get_currently_executing_thread_id()));
  fprintf (thread_info_log_fp, 
           "--------------------------------------------------------------\n");
  fflush (thread_info_log_fp);
  fclose (thread_info_log_fp);
  close (thread_info_log_fd);
  thread_info_log_fp = NULL;
  batch_thread_redirect_backtrace (thread_info_log_file_name);

/* Suresh - Why do we need to proceed() here ?? Looks like this is the 
   mother of all problems !! - recursive proceed, gdb dumping on ^c etc !!
   Instead we will now loop in command_loop()
   Fixes : QXCR 1000757179 & QXCR 1000762032 
*/
#if 0
  proceed ((CORE_ADDR)-1, TARGET_SIGNAL_DEFAULT, 0);
#endif /* if 0 */
  return;
}


void
decode_thread_event (enum rtc_event ecode)
{
  CORE_ADDR arg1, arg2, arg3;
  int ret;
  char *env_data;

  arg1 = (CORE_ADDR) read_register (ARG1_REGNUM);
  arg2 = (CORE_ADDR) read_register (ARG2_REGNUM);
  arg3 = (CORE_ADDR) read_register (ARG3_REGNUM);

  env_data = getenv ("THREAD_RTC_DEBUG1");
  if (env_data != NULL)
    {
      if ((strcmp (env_data, "off") == 0) ||
          (strcmp (env_data, "OFF") == 0) ||
          (strcmp (env_data, "0") == 0))
        {
          thread_RTC_debug1 = 0;
        }
      else if ((strcmp (env_data, "on") == 0) ||
               (strcmp (env_data, "ON") == 0) ||
               (strcmp (env_data, "1") == 0))
        {
          thread_RTC_debug1 = 1;
        }
    }
  /* Re-read the list of threads */
  prune_threads ();
  target_find_new_threads ();

  /* Read additional thread info from librtc */
  ret = read_thread_trace_info();

  if (ret)
    report_read_thread_errors(ret, -1);

  switch (ecode)
    {
    case RTC_NUM_WAITERS:
      {
        char* obj_name = NULL;
        gdb_pthread_object_t* wait_obj = object_hash_get (arg1);

        if (wait_obj == NULL)
          error ("Internal error : can't find info about object %lld.", arg1);

        if (batch_rtc)
          {
            print_event_to_log_file (RTC_NUM_WAITERS, wait_obj,
                                     NULL, (gdb_pthread_object_t*) arg3);
          }
        else
          {
            switch (wait_obj->type)
              {
                case OBJT_THREAD: 
                  obj_name = "thread";
                  break;
                case OBJT_MUTEX: 
                  obj_name = "mutex";
                  break;
                case OBJT_RWLOCK: 
                  obj_name = "read-write lock";
                  break;
                case OBJT_COND: 
                  obj_name = "condition variable";
                  break;
                default:
                  obj_name = "pthread object";
                  break;
              }
            if (wait_obj->type == OBJT_THREAD)
              warning ("Number of waiters on %s %lld exceeded threshold of %d.",
                       obj_name, wait_obj->id, thr_num_waiters);
            else
              warning ("Number of waiters on %s %d exceeded threshold of %d.",
                       obj_name, wait_obj->index, thr_num_waiters);
         }
         break;
      }
    case RTC_NON_RECURSIVE_RELOCK:
      {
        gdb_pthread_object_t* lock_obj = object_hash_get (arg1);
        if (lock_obj == NULL)
          error ("Internal error : can't find info about object %lld.", arg1);
        assert (lock_obj->type == OBJT_MUTEX || lock_obj->type == OBJT_RWLOCK);

        gdb_pthread_object_t* thread_obj = object_hash_get (arg2);
        if (thread_obj == NULL)
          error ("Internal error : can't find info about thread %lld.", arg2);
        assert (thread_obj->type == OBJT_THREAD);

        if (batch_rtc)
          {
            print_event_to_log_file (RTC_NON_RECURSIVE_RELOCK, lock_obj,
                                     thread_obj, NULL);
          }
        else
          {
            warning ("Attempt to recursively acquire non-recursive %s %d "
                     "from thread %lld.", 
                 lock_obj->type == OBJT_MUTEX ? "mutex" : "read-write lock",
                 lock_obj->index, thread_obj->id);
          }
        break;
      }
    case RTC_UNLOCK_NOT_OWN:
      {
        gdb_pthread_object_t* lock_obj = object_hash_get (arg1);
        if (lock_obj == NULL)
          error ("Internal error : can't find info about object %lld.", arg1);
        assert (lock_obj->type == OBJT_MUTEX || lock_obj->type == OBJT_RWLOCK);

        gdb_pthread_object_t* thread_obj = object_hash_get (arg2);
        if (thread_obj == NULL)
          error ("Internal error : can't find info about thread %lld.", arg2);
        assert (thread_obj->type == OBJT_THREAD);

        if (batch_rtc)
          {
	      if (thread_RTC_debug1 == 1)
	      {
	        printf ("\nStack trace before print_event_to_log_file in RTC_UNLOCK_NOT_OWN \n");
	        backtrace_command_1("18",0, 1);
	      }
            print_event_to_log_file (RTC_UNLOCK_NOT_OWN, lock_obj,
                                     thread_obj, NULL);
          }
        else
          {
            warning ("Attempt to unlock %s %d not owned by thread %lld.",
                 lock_obj->type == OBJT_MUTEX ? "mutex" : "read-write lock",
                 lock_obj->index, thread_obj->id);
          }
        break;
      }
    case RTC_MIXED_SCHED_POLICY:
      {
        gdb_pthread_object_t* thread_obj1 = object_hash_get (arg1);
        if (thread_obj1 == NULL)
          error ("Internal error : can't find info about thread %lld.", arg1);
        assert (thread_obj1->type == OBJT_THREAD);

        gdb_pthread_object_t* thread_obj2 = object_hash_get (arg2);
        if (thread_obj2 == NULL)
          error ("Internal error : can't find info about thread %lld.", arg2);
        assert (thread_obj2->type == OBJT_THREAD);
        if (batch_rtc)
          {
            print_event_to_log_file (RTC_MIXED_SCHED_POLICY, thread_obj1,
                                     thread_obj2, NULL);
          }
        else
          {
            warning (
                 "Attempt to synchronize threads %lld and %lld with different "
                 "scheduling policies.", thread_obj1->id, thread_obj2->id);
          }
        break;
      }
    case RTC_CONDVAR_MULTIPLE_MUTEXES:
      {
        gdb_pthread_object_t* cv_obj = object_hash_get (arg1);
        if (cv_obj == NULL)
          error ("Internal error : can't find info about condvar %lld.", arg1);
        assert (cv_obj->type == OBJT_COND);

        gdb_pthread_object_t* mutex_obj1 = object_hash_get (arg2);
        if (mutex_obj1 == NULL)
          error ("Internal error : can't find info about mutex %lld.", arg2);
        assert (mutex_obj1->type == OBJT_MUTEX);

        gdb_pthread_object_t* mutex_obj2 = object_hash_get (arg3);
        if (mutex_obj2 == NULL)
          error ("Internal error : can't find info about mutex %lld.", arg3);
        assert (mutex_obj2->type == OBJT_MUTEX);

        if (batch_rtc)
          {
            print_event_to_log_file (RTC_CONDVAR_MULTIPLE_MUTEXES, cv_obj,
                                     mutex_obj1, mutex_obj2);
          }
        else
          {
            warning ("Attempt to associate condition variable %d with mutexes "
                     "%d and %d.", cv_obj->index, mutex_obj1->index, 
                     mutex_obj2->index);
          }
        break;
      }
    case RTC_CONDVAR_WAIT_NO_MUTEX:
      {
        gdb_pthread_object_t* thread_obj = object_hash_get (arg1);
        if (thread_obj == NULL)
          error ("Internal error : can't find info about thread %lld.", arg1);
        assert (thread_obj->type == OBJT_THREAD);

        gdb_pthread_object_t* cv_obj = object_hash_get (arg2);
        if (cv_obj == NULL)
          error ("Internal error : can't find info about condvar %lld.", arg2);
        assert (cv_obj->type == OBJT_COND);

        gdb_pthread_object_t* mutex_obj = object_hash_get (arg3);
        if (mutex_obj == NULL)
          error ("Internal error : can't find info about mutex %lld.", arg3);
        assert (mutex_obj->type == OBJT_MUTEX);

        if (batch_rtc)
          {
	      if (thread_RTC_debug1 == 1)
	      {
	        printf ("\nStack trace before print_event_to_log_file in WAIT_NO_MUTEX \n");
	        backtrace_command_1("18",0, 1);
	      }
            print_event_to_log_file (RTC_CONDVAR_WAIT_NO_MUTEX, thread_obj,
                                     cv_obj, mutex_obj);
          }
        else
          {
            warning ("Attempt by thread %lld to wait on condition variable %d "
                     "without locking the associated mutex %d.", thread_obj->id,
                     cv_obj->index, mutex_obj->index);
          }
        break;
      }
    case RTC_DELETED_OBJ_USED:
      {
        if (batch_rtc)
          {
            print_event_to_log_file (RTC_DELETED_OBJ_USED, NULL, NULL, NULL);
          }
        else
          {
            warning ("Attempt to use a deleted object.");
          }
        break;
      }
    case RTC_THREAD_EXIT_OWN_MUTEX:
      {
        gdb_pthread_object_t* thread_obj = object_hash_get (arg1);
        if (thread_obj == NULL)
          error ("Internal error : can't find info about thread %lld.", arg1);
        assert (thread_obj->type == OBJT_THREAD);

        gdb_pthread_object_t* lock_obj = object_hash_get (arg2);
        if (lock_obj == NULL)
          error ("Internal error : can't find info about object %lld.", arg2);
        assert (lock_obj->type == OBJT_MUTEX || lock_obj->type == OBJT_RWLOCK);

        if (batch_rtc)
          {
            print_event_to_log_file (RTC_THREAD_EXIT_OWN_MUTEX,
                                     thread_obj, lock_obj, NULL);
          }
        else
          {
            warning ("Attempt to exit thread %lld while holding a %s %d.",
                 thread_obj->id, 
                 lock_obj->type == OBJT_MUTEX ? "mutex" : "read-write lock",
                 lock_obj->index);
          }
        break;
      }
    case RTC_THREAD_EXIT_NO_JOIN_DETACH:
      {
        gdb_pthread_object_t* thread_obj = object_hash_get (arg1);
        if (thread_obj == NULL)
          error ("Internal error : can't find info about thread %lld.", arg1);
        assert (thread_obj->type == OBJT_THREAD);

        if (batch_rtc)
          {
            print_event_to_log_file (RTC_THREAD_EXIT_NO_JOIN_DETACH,
                                     thread_obj, NULL, NULL);
          }
        else
          {
            warning (
                 "Attempt to exit thread %lld which has neither been joined "
                 "nor detached.", thread_obj->id);
          }
        break;
      }
    case RTC_THREAD_STACK_UTILIZATION:
      {
        gdb_pthread_object_t* thread_obj = object_hash_get (arg1);
        if (thread_obj == NULL)
          error ("Internal error : can't find info about thread %lld.", arg1);
        assert (thread_obj->type == OBJT_THREAD);

        if (batch_rtc)
          {
            print_event_to_log_file (RTC_THREAD_STACK_UTILIZATION,
                                     thread_obj,
                                     (gdb_pthread_object_t *)arg2, NULL);
          }
        else
          {
            warning (
                 "Thread %lld exceeded stack utilization threshold of %d%%.",
                 thread_obj->id, thr_stack_utilization);
          }
        break;
      }
    default:
      {
        warning ("Unknown event.");
        break;
      }
    }
}


void
_initialize_thread_tr (void)
{
  struct cmd_list_element *set, *show;

  add_info ("mutex", mutex_info_command, 
            "Mutex report for all mutexes, or for a specified mutex.\n\nUsage:\n\t"
            "info mutex [<MUTEX-ID>]\n");
  add_info ("rwlock", rwlock_info_command, 
            "Read-write lock report for all locks, or for a specified lock.\n\n"
            "Usage:\n\tinfo rwlock [<RWLOCK-ID>]\n");
  add_info ("condvar", cond_info_command, 
            "Condition variable report for all variables, or for a "
            "specified variable.\n\nUsage:\n\tinfo condvar [<CONDVAR-ID>]\n");
  set = add_set_cmd ("thread-check", class_support, var_string_noescape,
                     (char *) &thread_tracing_args,
    "Set thread debugging commands\n"
    "Usage:\n\tSet thread-check [ [on|off] | <OPTION> [on|off] | <OPTION> <NUM> ].\n"
    "    where <OPTION> [on|off] or <OPTION> <NUM> can be one of the following:\n\n"
    "    recursive-relock on|off           : detect attempts to relock\n"
    "                                        non-recursive mutexes\n"
    "    unlock-not-own on|off             : detect attempts to unlock mutexes\n"
    "                                        or rwlocks not owned by the\n"
    "                                        current thread\n"
    "    mixed-sched-policy on|off         : detect attempts to synchronize\n"
    "                                        threads using different scheduling\n"
    "                                        policies\n"
    "    cv-multiple-mxs on|off            : detect when a condition variable is\n"
    "                                        associated with more then one mutex\n"
    "    cv-wait-no-mx on|off              : detect attempts to wait on a\n"
    "                                        condition variable when a mutex is\n"
    "                                        not locked\n"
    "    thread-exit-own-mutex on|off      : detect attempts to exit a thread\n"
    "                                        while it's holding a mutex or a\n"
    "                                        rwlock\n"
    "    thread-exit-no-join-detach on|off : detect when a terminated thread is\n"
    "                                        neither joined nor detached\n"
    "    stack-util <NUM>                  : detect when thread stack\n"
    "                                        utilization exceeds a given\n"
    "                                        threshold (0 - 100%)\n"
    "    num-waiters <NUM>                 : detect when the number of waiters\n"
    "                                        on any object exceeds a given\n"
    "                                        threshold\n",
                   &setlist);
  set->function.sfunc = set_thread_tracing_command;
  show = add_show_from_set (set, &showlist);
  show->function.sfunc = show_thread_tracing_command;

  /* This is just an alias for 'set heap-check frame-count <num>' with one
     exception: it does not set heap-checking on, just the frame_count */
  set = add_set_cmd ("frame-count", class_support, var_string_noescape,
                     (char *) &frame_count_args, "Set the depth of call stack to \
be captured\n\nUsage:\nTo set new value:\n\tset frame-count [<INTEGER>]\nTo see \
current value:\n\tshow frame-count\n", &setlist);
  set->function.sfunc = set_frame_count_command;
  show = add_show_from_set (set, &showlist);
  show->function.sfunc = show_frame_count_command;
}

void
mark_librtc_start_end (CORE_ADDR text_start,
                       CORE_ADDR text_end)
{
  librtc_start = text_start;
  librtc_end = text_end;
}

void
mark_libpthread_start_end (CORE_ADDR text_start,
                           CORE_ADDR text_end)
{
  libpthread_start = text_start;
  libpthread_end = text_end;
}


