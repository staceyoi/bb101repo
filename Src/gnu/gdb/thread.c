/* Multi-process/thread control for GDB, the GNU debugger.
   Copyright 1986, 1987, 1988, 1993, 1998, 1999, 2000

   Contributed by Lynx Real-Time Systems, Inc.  Los Gatos, CA.
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "symtab.h"
#include "frame.h"
#include "inferior.h"
#include "environ.h"
#include "value.h"
#include "target.h"
#include "gdbthread.h"
#include "command.h"
#include "gdbcmd.h"
#include "top.h"
#include "objfiles.h"
#include "thread.h"
#include "exec-path.h"
#include "javalib.h" 	/* For Java Unwind library */

#include <ctype.h>
#include <sys/types.h>
#include <sys/pstat.h>
#include <signal.h>
#include <strings.h>
#ifdef UI_OUT
#include "ui-out.h"
#endif

/*#include "lynxos-core.h" */

/* Definition of struct thread_info exported to gdbthread.h */

/* Prototypes for exported functions. */

void _initialize_thread (void);

/* Prototypes for local functions. */

#ifdef REGISTER_STACK_ENGINE_FP
    CORE_ADDR step_frame_rse_addr;
#endif
static struct thread_info *thread_list = NULL;
static int highest_thread_num;

#ifdef HP_IA64_NATIVE
extern void invalidate_rse_info (void); 
#endif

extern void enable_ut_exit_events (int);
extern void print_extended_thread_info(struct thread_info*, int);

/* thread id of the last thread causing a ttrace event */
extern int last_event_thread;

#ifdef HP_MXN
/* Show/do not show all the kernel threads in a process. By default,
   this is set to 0, which means that we do not show those kernel 
   threads that do not have any user threads associated with them 
 */
int show_all_kthreads = 0;
#endif

static struct thread_info *find_thread_id (int num);

static void thread_command (char *tidstr, int from_tty);
static void thread_apply_all_command (char *, int);
static int thread_alive (struct thread_info *);
static void info_threads_command (char *, int);
static void thread_apply_command (char *, int);
static void restore_current_thread (int);
static struct cleanup * make_cleanup_restore_current_thread (int);
void switch_to_thread (int pid);
void prune_threads (void);
static void print_thread_priority (int ktid, int pid);

extern boolean threaded_program(void); 
extern char * get_java_thread_info (struct basic_frame_info *frame);
extern char * get_java_thread_ns (struct basic_frame_info *frame);
extern void search_thread_stack_sym (void *addr_arg);
int is_prog_using_cma_threads = 0;  /* default to FALSE */
int cma_process_pid;

boolean print_bad_pc_unwind_msg = 1;

struct thread_info *
get_thread_list(void)
{
  return thread_list;
}

/* The functions for executing a gdb command safely.
   Return 1 for success, 0 for failure. */

static int execute_command_2 (PTR arg);

typedef struct
{
  char *p;
  int from_tty;
} args_for_execute_command;

static int
execute_command_1 (char *p, int from_tty)
{
  args_for_execute_command args;
  args.p=p;
  args.from_tty=from_tty;
  /* should call with RETURN_MASK_ERROR rather than RETURN_MASK_ALL mask,
     so that thread_apply_all_command doesn't ignore quits from user. */
  return catch_errors (execute_command_2, &args, NULL, RETURN_MASK_ERROR);
}

static int
execute_command_2 (PTR arg)
{
  args_for_execute_command *args = arg;
  execute_command (args->p, args->from_tty);
  return 1;
}

void
init_thread_list ()
{
  struct thread_info *tp, *tpnext;

  if (!thread_list)
    return;

  for (tp = thread_list; tp; tp = tpnext)
    {
      tpnext = tp->next;

      /* FIXME: do I ever need to call the back-end to give it a
	 chance at this private data before deleting the thread?  */
      if (tp->private)
	free (tp->private);

      free (tp);
    }

  thread_list = NULL;
  highest_thread_num = 0;
}

/* add_thread now returns a pointer to the new thread_info, 
   so that back_ends can initialize their private data.  */

struct thread_info *
add_thread (int pid)
{
  struct thread_info *tp;

  tp = (struct thread_info *) xmalloc (sizeof (struct thread_info));

  tp->pid = pid;
  tp->num = ++highest_thread_num;
  tp->prev_pc = 0;
  tp->prev_func_start = 0;
  tp->prev_func_name = NULL;
  tp->step_range_start = 0;
  tp->step_range_end = 0;
  tp->step_frame_address = 0;
#ifdef REGISTER_STACK_ENGINE_FP
  tp->step_frame_rse_addr= 0;
#endif
  tp->step_resume_breakpoint = 0;
  tp->through_sigtramp_breakpoint = 0;
  tp->handling_longjmp = 0;
  tp->trap_expected = 0;
  tp->another_trap = 0;
  tp->stepping_through_solib_after_catch = 0;
  tp->stepping_through_solib_catchpoints = NULL;
  tp->stepping_through_sigtramp = 0;
  tp->next = thread_list;
  tp->disabled = 0; /* Enabled by default. */
  tp->private = NULL;
  tp->rtc_thread = NULL;
  tp->global_ept = NULL;
  thread_list = tp;
  return tp;
}

/* add_user_thread - re-use a thread list entry for the first user
   thread.

   In infttrace.c:child_acknowledge_created_inferior, add_thread is called
   for the main process thread.  We want to re-use this entry for the
   first user thread so we don't get an extra entry in the thread list.

   If an entry in the thread list has a pid of real_process_id, set its
   pid to the pid passed in and return the thread number, otherwise just
   call add_thread.
   */

int
add_user_thread (int pid, int real_process_id)
{
  struct thread_info *tp, *tpprev;

  for (tp = thread_list; tp; tp = tp->next)
    {
      if (tp->pid == real_process_id)
        {
          tp->pid = pid;
          return tp->num;
        }
    }
  tp = add_thread (pid);
  return tp->num;
} /* end add_user_thread */

void
delete_thread (int pid)
{
  struct thread_info *tp, *tpprev;

  tpprev = NULL;

  for (tp = thread_list; tp; tpprev = tp, tp = tp->next)
    if (tp->pid == pid)
      break;

  if (!tp)
    return;

  if (tpprev)
    tpprev->next = tp->next;
  else
    thread_list = tp->next;

  /* NOTE: this will take care of any left-over step_resume breakpoints,
     but not any user-specified thread-specific breakpoints. */
  if (tp->step_resume_breakpoint)
    delete_breakpoint (tp->step_resume_breakpoint);

  /* FIXME: do I ever need to call the back-end to give it a
     chance at this private data before deleting the thread?  */
  if (tp->private)
    free (tp->private);

#ifdef HP_IA64
  /* Clean up global execution path table */
  if (tp->global_ept)
    { 
      if (get_current_ept () == tp->global_ept)
        invalidate_current_ept ();
      ept_free (tp->global_ept);
    }
#endif

  free (tp);

  return;
}

static struct thread_info *
find_thread_id (int num)
{
  struct thread_info *tp;

#if defined(FIND_NEW_THREADS)
  FIND_NEW_THREADS ();
#endif

  for (tp = thread_list; tp; tp = tp->next)
    if (tp->num == num)
      return tp;

  return NULL;
}

/* Find a thread_info by matching 'pid'.  */
struct thread_info *
find_thread_pid (int pid)
{
  struct thread_info *tp;

  for (tp = thread_list; tp; tp = tp->next)
    if (tp->pid == pid)
      return tp;

  return NULL;
}

/*
 * Thread iterator function.
 *
 * Calls a callback function once for each thread, so long as
 * the callback function returns false.  If the callback function
 * returns true, the iteration will end and the current thread
 * will be returned.  This can be useful for implementing a 
 * search for a thread with arbitrary attributes, or for applying
 * some operation to every thread.
 *
 * FIXME: some of the existing functionality, such as 
 * "Thread apply all", might be rewritten using this functionality.
 */

struct thread_info *
iterate_over_threads (int (*callback) (),
                     void *data)
{
  struct thread_info *tp;

  for (tp = thread_list; tp; tp = tp->next)
    if ((*callback) (tp, data))
      return tp;

  return NULL;
}

int
valid_thread_id (int num)
{
  struct thread_info *tp;

  for (tp = thread_list; tp; tp = tp->next)
    if (tp->num == num)
      return 1;

  return 0;
}

int
pid_to_thread_id (int pid)
{
  struct thread_info *tp;

  for (tp = thread_list; tp; tp = tp->next)
    if (tp->pid == pid)
      return tp->num;

  return 0;
}

int
thread_id_to_pid (int num)
{
  struct thread_info *thread = find_thread_id (num);
  if (thread)
    return thread->pid;
  else
    return -1;
}

int
in_thread_list (int pid)
{
  struct thread_info *tp;

  for (tp = thread_list; tp; tp = tp->next)
    if (tp->pid == pid)
      return 1;

  return 0;			/* Never heard of 'im */
}
#ifdef UI_OUT
/* Print a list of thread ids currently known, and the total number of
   threads. To be used from within catch_errors. */
static int 
do_captured_list_thread_ids (void *arg)
{
  struct thread_info *tp;
  int num = 0;
  /* Changes inherited from info_threads_command.*/
  prune_threads ();
  target_find_new_threads ();
  
  ui_out_tuple_begin (uiout, "thread-ids");
  for (tp = thread_list; tp; tp = tp->next)
    {
      num++;
#ifdef GDB_TARGET_IS_HPUX
     if (is_mxn)
       {
         int utid = get_pthread_for (tp->pid);
	 ui_out_field_int (uiout, "thread-id", 
		(utid == -1)? -(get_lwp_for(tp->pid)) : utid );
       }
     else
#endif 
      ui_out_field_int (uiout, "thread-id", tp->num);
    }

  ui_out_tuple_end (uiout);
  ui_out_field_int (uiout, "number-of-threads", num);
  return GDB_RC_OK;
}

/* Official gdblib interface function to get a list of thread ids and
   the total number. */
enum gdb_rc
gdb_list_thread_ids (/* output object */)
{
  return catch_errors (do_captured_list_thread_ids, NULL,
		       NULL, RETURN_MASK_ALL);
}
#endif

/* Load infrun state for the thread PID.  */

void
load_infrun_state (int pid, CORE_ADDR *prev_pc, CORE_ADDR *prev_func_start, char **prev_func_name,
		   int *trap_expected, struct breakpoint **step_resume_breakpoint,
		   struct breakpoint **through_sigtramp_breakpoint, 
#ifdef REGISTER_STACK_ENGINE_FP
		   CORE_ADDR *step_frame_rse_addr,
#endif
		   CORE_ADDR *step_range_start,
		   CORE_ADDR *step_range_end, 
                   CORE_ADDR *step_frame_address,
		   int *handling_longjmp, int *another_trap,
		   int *stepping_through_solib_after_catch,
		   bpstat *stepping_through_solib_catchpoints,
		   int *stepping_through_sigtramp)
{
  struct thread_info *tp;

  /* If we can't find the thread, then we're debugging a single threaded
     process.  No need to do anything in that case.  */
  tp = find_thread_id (pid_to_thread_id (pid));
  if (tp == NULL)
    return;

  *prev_pc = tp->prev_pc;
  *prev_func_start = tp->prev_func_start;
  *prev_func_name = tp->prev_func_name;
  *step_resume_breakpoint = tp->step_resume_breakpoint;
  *step_range_start = tp->step_range_start;
  *step_range_end = tp->step_range_end;
  *step_frame_address = tp->step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
  *step_frame_rse_addr= tp->step_frame_rse_addr;
#endif
  *through_sigtramp_breakpoint = tp->through_sigtramp_breakpoint;
  *handling_longjmp = tp->handling_longjmp;
  *trap_expected = tp->trap_expected;
  *another_trap = tp->another_trap;
  *stepping_through_solib_after_catch = tp->stepping_through_solib_after_catch;
  *stepping_through_solib_catchpoints = tp->stepping_through_solib_catchpoints;
  *stepping_through_sigtramp = tp->stepping_through_sigtramp;
}

/* Save infrun state for the thread PID.  */

void
save_infrun_state (int pid, CORE_ADDR prev_pc, CORE_ADDR prev_func_start, char *prev_func_name,
                  int trap_expected, struct breakpoint *step_resume_breakpoint,
                  struct breakpoint *through_sigtramp_breakpoint,
#ifdef REGISTER_STACK_ENGINE_FP
                   CORE_ADDR step_frame_rse_addr,
#endif
                   CORE_ADDR step_range_start,
                   CORE_ADDR step_range_end,
                   CORE_ADDR step_frame_address,
                   int handling_longjmp, int another_trap,
                   int stepping_through_solib_after_catch,
                   bpstat stepping_through_solib_catchpoints,
                   int stepping_through_sigtramp)
{
  struct thread_info *tp;

  /* If we can't find the thread, then we're debugging a single-threaded
     process.  Nothing to do in that case.  */
  tp = find_thread_id (pid_to_thread_id (pid));
  if (tp == NULL)
    return;

  tp->prev_pc = prev_pc;
  tp->prev_func_start = prev_func_start;
  tp->prev_func_name = prev_func_name;
  tp->step_resume_breakpoint = step_resume_breakpoint;
  tp->step_range_start = step_range_start;
  tp->step_range_end = step_range_end;
  tp->step_frame_address = step_frame_address;
#ifdef REGISTER_STACK_ENGINE_FP
  tp->step_frame_rse_addr= step_frame_rse_addr;
#endif
  tp->through_sigtramp_breakpoint = through_sigtramp_breakpoint;
  tp->handling_longjmp = handling_longjmp;
  tp->trap_expected = trap_expected;
  tp->another_trap = another_trap;
  tp->stepping_through_solib_after_catch = stepping_through_solib_after_catch;
  tp->stepping_through_solib_catchpoints = stepping_through_solib_catchpoints;
  tp->stepping_through_sigtramp = stepping_through_sigtramp;
}

/* Return true if TP is an active thread. */
static int
thread_alive (struct thread_info *tp)
{
  if (tp->pid == -1)
    return 0;
  if (!target_thread_alive (tp->pid))
    {
      tp->pid = -1;		/* Mark it as dead */
      return 0;
    }
  return 1;
}

void
prune_threads ()
{
  struct thread_info *tp, *next;

  for (tp = thread_list; tp; tp = next)
    {
      next = tp->next;
      if (!thread_alive (tp))
	delete_thread (tp->pid);
    }
}

#ifdef GDB_TARGET_IS_HPUX
/* Function to print thread priority information with info threads output - JAGae06271
   The arguments to the function are ktid - kernal thread id and pid - process id.   */
static void 
print_thread_priority (int ktid, int pid)
{
  struct lwp_status buf;
  int count;
  bzero (&buf, sizeof (struct lwp_status));
  count = pstat_getlwp (&buf, sizeof (struct lwp_status), 0, ktid, pid);
  if (count > 0)
  {
      printf_filtered ("Priority:%lli", buf.lwp_pri);
  }
}
#endif


static void
print_basic_thread_info(struct thread_info* tp, int current_pid)
{
  char *extra_info;

#ifdef HP_MXN
  /* bindu 101601: Don't show the thread with no user thread associated with. */
  if (show_all_kthreads || !is_mxn || 
      (is_mxn && (get_pthread_for(tp->pid) != -1)))
#endif
    {
      if (tp->pid == current_pid)
        printf_filtered ("* ");
      else if (tp->pid == last_event_thread)
          /* Mark the thread reporting the last ttrace event with a '>' 
             like LaDebug, if it's not the current thread. (JAGaf47331) */
        printf_filtered ("> ");
      else
        printf_filtered ("  ");

      /* Show the disabled threads with a D in front. */
      if (tp->disabled)
        printf_filtered ("D");
      else
        printf_filtered (" ");

      printf_filtered (" ");

#if defined(GDB_TARGET_IS_HPUX)
      /* Bindu 061902: If thread has no associated utid, display negative
         of Kernel thread id. */
      if (is_mxn)
        {
          int utid = get_pthread_for (tp->pid);
          printf_filtered ("%d %s  ", 
                           (utid == -1)? -(get_lwp_for (tp->pid)): utid, 
                           target_tid_to_str (tp->pid));
        }
      else
        printf_filtered ("%d %s ", tp->num, target_tid_to_str (tp->pid));

      /* Get the thread priority using pstat for live process. */
#ifdef GDB_TARGET_IS_HPPA
#ifdef CMA_THREAD_FLAG
      if (!(IS_CMA_PID(current_pid)))
#endif
#endif
        if (target_has_execution)
          print_thread_priority (get_lwp_for (tp->pid), get_pid_for (tp->pid));
#else
      printf_filtered ("%d %s ", tp->num, target_pid_to_str (tp->pid));
#endif
      extra_info = target_extra_thread_info (tp);
      if (extra_info)
        printf_filtered (" (%s)", extra_info);
      puts_filtered ("  ");
      switch_to_thread (tp->pid);
      if (selected_frame)
        print_only_stack_frame (selected_frame, -1, 0);
      else
        printf_filtered ("[No stack.]\n");

      /* If this is a Java thread, print out Java thread name and state. */
      if (java_debugging && selected_frame) 
        {
            char* java_thread_info;
            struct basic_frame_info bfi;

            /* initialize bfi being sent to junwindlib */
            bfi.pc = selected_frame->pc;
            bfi.fp = selected_frame->frame;
#ifdef HP_IA64
            bfi.bsp = selected_frame->rse_fp;
            bfi.cfm = selected_frame->cfm;
            bfi.java_ptr = 0;
#endif
            if (selected_frame->next)
              bfi.sp = selected_frame->next->frame;
            else
              bfi.sp = read_sp ();

            java_thread_info = get_java_thread_ns(&bfi);
            if (java_thread_info) 
              {
                puts_filtered (java_thread_info);
                wrap_here (" ");

                /* free the str after use */
                free(java_thread_info);
              }
        }
    }
}


static void 
info_threads_command_all(int current_pid)
{
  struct thread_info *tp;
  struct cleanup *old_chain;

  old_chain = make_cleanup_restore_current_thread (inferior_pid);
  for (tp = thread_list; tp; tp = tp->next)
    print_basic_thread_info(tp, current_pid);
  do_cleanups (old_chain);
}


static void 
info_thread_id_command(int num, int current_pid)
{
  int thr = num;
  struct thread_info *tp;

#ifdef HP_MXN
  /* If the thread id is -ve, it's a ktid. */
  if (is_mxn)
    {
      if (!show_all_kthreads && !num)
	error ("Cannot switch to thread with no user thread associated.");
      if (num < 0)
	num = get_gdb_tid_from_ktid (-num);
      else
	num = get_gdb_tid_from_utid (num);
      num = pid_to_thread_id (num);
    }
#endif

  tp = find_thread_id (num);
  
  if (!tp)
    error ("Thread ID %d not known.  Use the \"info threads\" command to\n\
see the IDs of currently known threads.", thr);

  if (!thread_alive (tp))
    error ("Thread ID %d has terminated.\n", thr);

  print_extended_thread_info(tp, current_pid);
}


/* Print information about currently known threads 

 * Note: this has the drawback that it _really_ switches
 *       threads, which frees the frame cache.  A no-side
 *       effects info-threads command would be nicer.
 */

static void
info_threads_command (char *arg, int from_tty)
{
  int current_pid = inferior_pid;
  struct frame_info *cur_frame;
  int saved_frame_level = selected_frame_level;
  int counter;
  int is_threaded = 0; /* JAGae87713 */
  int ret; 


#ifdef FIND_ACTIVE_THREAD
  if (!(IS_CMA_PID(current_pid)))
    {
      current_pid = FIND_ACTIVE_THREAD ();
    }
#endif

  if (!target_has_stack) error ("No stack.");

  prune_threads ();
  target_find_new_threads ();

  if (arg == NULL)
    {
      info_threads_command_all (current_pid);
    }
  else
    {
      if (!could_be_number(arg))
        error ("Invalid thread ID %s. Thread ID must be an integer.\n",
               arg);

      int thread_id = atoi (arg);

      /* Read additional thread info from librtc */
      read_thread_trace_info();
      info_thread_id_command (thread_id, current_pid);
    }

  /* Code below copied from "up_silently_base" in "stack.c".
   * It restores the frame set by the user before the "info threads"
   * command.  We have finished the info-threads display by switching
   * back to the current thread.  That switch has put us at the top
   * of the stack (leaf frame).
   */
 
  is_threaded = threaded_program(); 
   /* JAGae87713 -  Allow  the reselection of current frame
    * only if the application is multithreaded  
    */
  if (is_threaded) 
  {
    counter = saved_frame_level;
    cur_frame = find_relative_frame (selected_frame, &counter);
    if (counter != 0)
      {
	/* Ooops, can't restore, tell user where we are. */
	warning ("Couldn't restore frame in current thread, at frame 0");
	print_stack_frame (selected_frame, -1, 0);
      }
    else
      {
	select_frame (cur_frame, saved_frame_level);
      }

    /* re-show current frame. */
    show_stack_frame (cur_frame);
  }
}

/* Switch from one thread to another. */
/* Bindu 080404: User of this function should make sure not to call this
   when show_all_kthreads is set and pid is -ve, if you do not want to switch
   to kernel thread with no user thread associated. */
void
switch_to_thread (int pid)
{
  char *saved_registers;
  if (!target_has_stack)
    error ("No stack.");

  if (pid == inferior_pid)
    return;

  inferior_pid = pid;
  flush_cached_frames ();
#ifdef HP_IA64_NATIVE
  invalidate_rse_info();
#endif
  registers_changed ();

  /* Poorva - JAGad00566 We want to save the values in the registers 
     buffer since read_pc changes the values of the buffer. We will 
     restore the values after select_frame_no_tui. We also mark them 
     as changed since read_pc read them in */

#ifdef CMA_THREAD_FLAG
  saved_registers = (char *) alloca (REGISTER_BYTE (ARCH_NUM_REGS));
  memcpy(saved_registers, registers, REGISTER_BYTE (ARCH_NUM_REGS));
#endif

  stop_pc = read_pc ();

  select_frame_no_tui (get_current_frame (), 0);

#ifdef CMA_THREAD_FLAG
  memcpy (registers, saved_registers, REGISTER_BYTE (ARCH_NUM_REGS) );
  registers_changed ();
#endif

#ifdef FORCE_CMA_THREAD_SWITCH
  FORCE_CMA_THREAD_SWITCH (pid);
#endif
}

static void
restore_current_thread (int pid)
{
  if (pid != inferior_pid)
    {
      switch_to_thread (pid);
      /* rven: Don't need to print the first frame at this time */
      /* print_stack_frame (get_current_frame (), 0, -1); */
    }
}

struct current_thread_cleanup
{
  int inferior_pid;
};

static void
do_restore_current_thread_cleanup (void *arg)
{
  struct current_thread_cleanup *old = arg;
  restore_current_thread (old->inferior_pid);
  free (old);
}

static struct cleanup *
make_cleanup_restore_current_thread (int inferior_pid)
{
  struct current_thread_cleanup *old
    = xmalloc (sizeof (struct current_thread_cleanup));
  old->inferior_pid = inferior_pid;
  return make_cleanup (do_restore_current_thread_cleanup, old);
}

/* Apply a GDB command to a list of threads.  List syntax is a whitespace
   seperated list of numbers, or ranges, or the keyword `all'.  Ranges consist
   of two numbers seperated by a hyphen.  Examples:

   thread apply 1 2 7 4 backtrace       Apply backtrace cmd to threads 1,2,7,4
   thread apply 2-7 9 p foo(1)  Apply p foo(1) cmd to threads 2->7 & 9
   thread apply all p x/i $pc   Apply x/i $pc cmd to all threads
 */

static void
thread_apply_all_command (char *cmd, int from_tty)
{
  struct thread_info *tp;
  struct cleanup *old_chain;

  if (cmd == NULL || *cmd == '\000')
    error ("Please specify a command following the thread ID list");

  old_chain = make_cleanup_restore_current_thread (inferior_pid);

  for (tp = thread_list; tp; tp = tp->next)
    if (thread_alive (tp))
      {
#ifdef HP_MXN
	/* Do not show threads with no user thread associated. */
	int utid = 0; /* initialize for compiler warning */
	if (is_mxn)
	  {
	    utid = get_pthread_for (tp->pid);
	    if (!show_all_kthreads && (utid == -1))
	      continue;
	  }
#endif
	switch_to_thread (tp->pid);
#ifdef GDB_TARGET_IS_HPUX
        printf_filtered ("\nThread %d (%s):\n",
#ifdef HP_MXN
		            /* Print negative of ktid if utid is -1 */
                            (is_mxn)? ((utid == -1)? -(get_lwp_for (tp->pid)): utid): tp->num,
#else
			    tp->num,
#endif /* HP_MXN */
			    target_tid_to_str (inferior_pid));
#endif /* GDB_TARGET_IS_HPUX */
	execute_command_1 (cmd, from_tty);
      }

  do_cleanups (old_chain);
}

/* srikanth, 000223, A new thread iterator. For each live thread
   call a given function after switching to that thread. The function
   receives an opaque pointer as argument and does what it does with
   it. The thread list is expected to be current.
*/

void
foreach_live_thread (void (*function) (void *), void * args)
{
  struct thread_info *tp;
  struct cleanup *old_chain;

  old_chain = make_cleanup_restore_current_thread (inferior_pid);

  for (tp = thread_list; tp; tp = tp->next)
    if (thread_alive (tp))
      {
#ifdef HP_MXN
        /* Do not show threads with no user thread associated. */
        int utid;
        if (is_mxn)
          {
            utid = get_pthread_for (tp->pid);
            if (!show_all_kthreads && (utid == -1))
              continue;
          }
#endif
	switch_to_thread (tp->pid);
        function (args);
      }
  do_cleanups (old_chain);
}


static void
thread_apply_command (char *tidlist, int from_tty)
{
  char *cmd;
  char *p;
  struct cleanup *old_chain;

  if (tidlist == NULL || *tidlist == '\000')
    error ("Please specify a thread ID list");

  for (cmd = tidlist; *cmd != '\000' && !isalpha (*cmd); cmd++);

  if (*cmd == '\000')
    error ("Please specify a command following the thread ID list");

  old_chain = make_cleanup_restore_current_thread (inferior_pid);

  while (tidlist < cmd)
    {
      struct thread_info *tp;
      int start, end;

      start = (int)strtol (tidlist, &p, 10);
      if (p == tidlist)
	error ("Error parsing %s", tidlist);
      tidlist = p;

      while (*tidlist == ' ' || *tidlist == '\t')
	tidlist++;
#ifdef HP_MXN
      /* If showing all kernel threads, user can use ( -10 -12) as the
         list of threads. This should not be equivalent to (-10 - 12),
	 where a range is specified. */
      if ((!show_all_kthreads && (*tidlist == '-')) 
	  || (show_all_kthreads && (*tidlist == '-') && (*tidlist == ' ')))
#else
      if (*tidlist == '-')
		/* Got a range of IDs? */
#endif /* HP_MXN */
	{
	  tidlist++;		/* Skip the - */
	  end = (int)strtol (tidlist, &p, 10);
	  if (p == tidlist)
	    error ("Error parsing %s", tidlist);
	  tidlist = p;

	  while (*tidlist == ' ' || *tidlist == '\t')
	    tidlist++;
	}
      else
	end = start;

      for (; start <= end; start++)
	{
	  /* Bindu 061902: Save the actual thread id user used in 'thr'.
	     We can use this to report back to the user. */
          /* QXCR1000963428: We used to change the "start" value.
              Use "thr" inside loop instead. */
	  int thr = start;
#ifdef HP_MXN
	  /* For mxn case, user uses utid. Get the real thread_id
             from utid. */
	  if (is_mxn)
	    {
              /* Bindu 061902: If it is a -ve number, user is using ktid.*/
	      if (thr < 0)
	        {
	          thr = get_gdb_tid_from_ktid (-thr);
	        }
	      else 
	        {
	          thr = get_gdb_tid_from_utid (thr);
	        }
	      thr = pid_to_thread_id (thr);
	    }
#endif
	  tp = find_thread_id (thr);

	  if (!tp)
	    warning ("Unknown thread %d.", start);
	  else if (!thread_alive (tp))
	    warning ("Thread %d has terminated.", start);
#ifdef HP_MXN
	  else if (is_mxn && !show_all_kthreads && !get_pthread_for (tp->pid))
	    warning ("Cannot show thread %d with no user thread associated.",
                     start);
#endif
	  else
	    {
	      switch_to_thread (tp->pid);
#ifdef HP_MXN
              if (is_mxn)
		printf_filtered ("\nThread %d (%s):\n", start,
                               target_tid_to_str (inferior_pid));
	      else
	        printf_filtered ("\nThread %d (%s):\n", tp->num,
				 target_tid_to_str (inferior_pid));
#else
	      printf_filtered ("\nThread %d (%s):\n", start,
			       target_pid_to_str (inferior_pid));
#endif
	      execute_command_1 (cmd, from_tty);
	    }
	}
    }

  do_cleanups (old_chain);
}

/* Switch to the specified thread.  Will dispatch off to thread_apply_command
   if prefix of arg is `apply'.  */

static void
thread_command (char *tidstr, int from_tty)
{
  int num;
  struct thread_info *tp;
  /* Bindu 061902: Save the actual thread id user used in 'thr'.
     We can use this to report back to the user. */
  int thr;
  if (!target_has_stack)
    error ("No stack.");

  if (!tidstr)
    {
      /* Don't generate an error, just say which thread is current. */
      if (target_has_stack)
	{
#ifdef HP_MXN
	  if (is_mxn)
	    {
	      int utid = get_pthread_for (inferior_pid);
	      printf_filtered ("[Current thread is %d (%s)]\n",
			       (utid == -1)? -(get_lwp_for (inferior_pid)): utid,
			       target_tid_to_str (inferior_pid) );
	    }
	  else
#endif
	printf_filtered ("[Current thread is %d (%s)]\n",
			 pid_to_thread_id (inferior_pid),
#if defined(GDB_TARGET_IS_HPUX)
			 target_tid_to_str (inferior_pid)
#else
			 target_pid_to_str (inferior_pid)
#endif
	  );

          /* If this is Java thread, print out Java thread specific info. */
	  if (java_debugging) {
	    char* java_thread_info;
	    struct frame_info *fi;
	    struct basic_frame_info bfi;
	
	    /* Get the current frame, and initialize bfi being sent to junwindlib */
	    fi = get_current_frame();
 	    bfi.pc = fi->pc;
 	    bfi.fp = fi->frame;
#ifdef HP_IA64
    	    bfi.bsp = fi->rse_fp;
            bfi.cfm = fi->cfm;
            bfi.java_ptr = 0;
#endif
            if (fi->next)
              bfi.sp = fi->next->frame;
            else
              bfi.sp = read_sp ();
	
	    java_thread_info = get_java_thread_info(&bfi);
	    if (java_thread_info) {
       	       puts_filtered ("\n");
               puts_filtered (java_thread_info);
               puts_filtered ("\n");
               wrap_here (" ");

              /* free the str after use */
              free(java_thread_info);
            }
	  }
	}
      else
	error ("No stack.");
      return;
    }

  if (!could_be_number(tidstr))
    error ("Invalid thread ID %s. Thread ID must be an integer.\n",
           tidstr);

  num = atoi (tidstr);
  thr = num;

#ifdef HP_MXN
  /* If the thread id is -ve, it's a ktid. */
  if (is_mxn)
    {
      if (!show_all_kthreads && !num)
	error ("Cannot switch to thread with no user thread associated.");
      if (num < 0)
	num = get_gdb_tid_from_ktid (-num);
      else
	num = get_gdb_tid_from_utid (num);
      num = pid_to_thread_id (num);
    }
#endif

  tp = find_thread_id (num);

  if (!tp)
    error ("Thread ID %d not known.  Use the \"info threads\" command to\n\
see the IDs of currently known threads.", thr);

  if (!thread_alive (tp))
    error ("Thread ID %d has terminated.\n", thr);

#if defined(GDB_TARGET_IS_HPUX)
  if (tp->pid == inferior_pid)
    {
      if (!batch_rtc)
        printf_filtered ("[Current thread is already %d (%s)]\n",
#ifdef HP_MXN
		       (is_mxn)? thr:
#endif /* HP_MXN */
		       pid_to_thread_id (inferior_pid),
		       target_tid_to_str (inferior_pid));
      return;
    }

#endif /* defined(GDB_TARGET_IS_HPUX) */

  switch_to_thread (tp->pid);

  printf_filtered ("[Switching to thread %d (%s)]\n",
#ifdef HP_MXN
                       (is_mxn)? thr:
#endif /* HP_MXN */
		   pid_to_thread_id (inferior_pid),
#if defined(GDB_TARGET_IS_HPUX)
		   target_tid_to_str (inferior_pid)
#else
		   target_pid_to_str (inferior_pid)
#endif
    );
  print_stack_frame (selected_frame, selected_frame_level, 1);
}

/*
  gdb_thread_select (tidstr);
}
*/


static void thread_enable_command (char *, int);
static void thread_disable_command (char *, int);

/* Given a pid, this function tells if it is a disabled pid or not. */
int 
is_disabled_pid (int pid)
{
  struct thread_info *tp;

  for (tp = thread_list; tp; tp = tp->next)
    {
      if (tp->pid == pid) 
        {
          return tp->disabled;
        }
    }
  return 0;
}

/* User can enable threads that have been disabled previously.
   'info thread' command shows all threads that have been disabled.
   This function takes in a NULL string or a thread number.
   If the tidstr is NULL, we enable the current thread. (inferior_pid)
   Otherwise we enable the given thread. */
static void
enable_a_thread (char *tidstr)
{
  int enable_pid;
  struct thread_info *tp;

  if (!target_has_execution)
    error ("Cannot enable thread with no process to execute.");

  if (!tidstr)
    {
      /* enable the current thread. */
      enable_pid = inferior_pid;
      tp = find_thread_pid (enable_pid);
    }
  else
    {
      if (!could_be_number((char *)tidstr))
        error ("Invalid thread ID %s. Thread ID must be an integer.\n",
               (char *)tidstr);

      int num = atoi ((char *)tidstr);
#ifdef HP_MXN
      if (is_mxn)
        {
          /* If the thread id is -ve, it is a thread with no user thread.
	     Do not allow this command on these threads. */
          if (!num)
	    error ("Cannot use this command on thread with no"
		" user thread associated.");

	  /* In mxn case, thread num is user thread id. 
	     Get the pid associated with this utid from infttrace's
	     thread_list. */
	  enable_pid = get_gdb_tid_from_utid (num);
	  if (!enable_pid)
            error ("No thread %s\n", tidstr);

	  tp = find_thread_pid (enable_pid);
	  if (!tp)
	    error ("No thread %s\n", tidstr);

        }
      else
#endif /* HP_MXN */
        {
	  /* Get the tp associated with the given thread number. */
	  tp = find_thread_id (num);
	  if (!tp)
	    error ("No thread %s\n", tidstr);
          enable_pid = tp->pid;;
        }
    }

  if (!tp->disabled)
    {
      /* Already enabled. */
      if (tidstr)
        printf_filtered ("Thread %s is already enabled\n", tidstr);
      else
        {
#ifdef HP_MXN
          if (is_mxn)
            {
              int utid = get_pthread_for (inferior_pid);
              printf_filtered ("Thread %d is already enabled\n",
                                (utid == -1)? -(get_lwp_for (inferior_pid)): utid);
            }
          else
#endif
            printf_filtered ("Thread %d is already enabled\n",
                              pid_to_thread_id (inferior_pid));
	}
      return;
    }

  if (tidstr)
    printf_filtered ("Enabling thread %s\n", tidstr);
  else
#ifdef HP_MXN
    if (is_mxn)
      {
        int utid = get_pthread_for (inferior_pid);
        printf_filtered ("Enabling thread %d\n",
                          (utid == -1)? -(get_lwp_for (inferior_pid)): utid);
      }
    else
#endif
      printf_filtered ("Enabling thread %d\n",
                        pid_to_thread_id (inferior_pid));

  /* Enable it. */
  tp->disabled = 0;
  return;
}

/* Enable command.
   User can enable threads that have been disabled previously.
   'info thread' command shows all threads that have been disabled.
   Command:
	o thread enable <thread_id> to enable a single thread.
	o thread enable all to enable all threads.
	o thread enable to enable current thread.
 */
static void
thread_enable_command (char *tidstr, int from_tty)
{
  if (tidstr && !strcmp (tidstr, "all"))
    {
      /* enable all threads. */
      struct thread_info *tp;
      int saved_inferior_pid = inferior_pid;
      for (tp = thread_list; tp; tp = tp->next)
        {
          if (!tp->disabled)
            continue;

	  /* Don't do this for the threads that do not have utid associated. */
	  if (tp->pid & KTID_MARKER)
	    continue;

          /* Let's call enable_a_thread by setting inferior_pid to this pid.
             Our enable current thread mechanism should handle this. */
          inferior_pid = tp->pid;
          enable_a_thread (NULL);
        }
      /* Restore the inferior pid. */
      inferior_pid = saved_inferior_pid;
      return;
    }
  else
    enable_a_thread (tidstr);
}

/* This function takes in a NULL string or a thread number.
   If the tidstr is NULL, we disable the current thread. (inferior_pid)
   Otherwise we disable the given thread. */
static void
disable_a_thread (char *tidstr)
{
  int pid;
  struct thread_info *tp;

  if (!target_has_execution)
    error ("Cannot disable thread with no process to execute.");

  if (!tidstr)
    {
      /* disable the current thread. */
      pid = inferior_pid;
      tp = find_thread_pid (pid);
    }
  else
    {
      if (!could_be_number((char *)tidstr))
        error ("Invalid thread ID %s. Thread ID must be an integer.\n",
               (char *)tidstr);

      int num = atoi ((char *)tidstr);
#ifdef HP_MXN
      if (is_mxn)
        {
	  /* If the thread id is -ve, it is a thread with no user thread.
             Do not allow this command on these threads. */
          if (!num)
	    error ("Cannot use this command on thread with no"
		" user thread associated.");

	  /* In mxn case, thread num is user thread id.
             Get the pid associated with this utid from infttrace's
             thread_list. */
	  pid = get_gdb_tid_from_utid (num);
	  if (!pid)
            error ("No thread %s\n", tidstr);

	  tp = find_thread_pid (pid);
	  if (!tp)
	    error ("No thread %s\n", tidstr);
        }
      else
#endif /* HP_MXN */
        {
	  /* Get the tp associated with the given thread number. */
	  tp = find_thread_id (num);
	  if (!tp)
            error ("No thread %s\n", tidstr); 
	  pid = tp->pid;
	}
    }

  if (tp->disabled)
    {
      /* Already disabled. */
      if (tidstr)
        printf_filtered ("Thread %s is already disabled\n", tidstr);
      else

#ifdef HP_MXN
      if (is_mxn)
        {
          int utid = get_pthread_for (inferior_pid);
          printf_filtered ("Thread %d is already disabled\n",
                            (utid == -1)? -(get_lwp_for (inferior_pid)): utid);
        }
      else
#endif
        printf_filtered ("Thread %d is already disabled\n",
                          pid_to_thread_id (inferior_pid));
      return;
    }

  if (tidstr)
    printf_filtered ("Disabling thread %s\n", tidstr);
  else
#ifdef HP_MXN
    if (is_mxn)
      {
        int utid = get_pthread_for (inferior_pid);
        printf_filtered ("Disabling thread %d\n",
                          (utid == -1)? -(get_lwp_for (inferior_pid)): utid);
      }
    else
#endif
      printf_filtered ("Disabling thread %d\n",
			pid_to_thread_id (inferior_pid));

  /* Disable it. */
  tp->disabled = 1;
  return;
}

/* Disable command.
   Command:
	o thread disable <thread_id> to disable a single thread.
	o thread disable all to disable all threads.
	o thread disable to disable current thread.
 */
static void
thread_disable_command (char *tidstr, int from_tty)
{
  warning ("ATTENTION!! Disabling threads may result in deadlocks in your"
	   " program.");
/* First let's enable User thread exit events. */
  enable_ut_exit_events (inferior_pid);
  if (tidstr && !strcmp (tidstr, "all"))
    {
      /* disable all threads. */
      struct thread_info *tp;
      int saved_inferior_pid = inferior_pid;
      for (tp = thread_list; tp; tp = tp->next)
        {
          if (tp->disabled)
            continue;

	  /* Don't do this for the threads that do not have utid associated. */
          if( tp->pid & KTID_MARKER)
            continue;

          /* Let's call disable_a_thread by setting inferior_pid to this pid.
             Our disable current thread mechanism should handle this. */
          inferior_pid = tp->pid;
          disable_a_thread (NULL);
        }
      /* Restore the inferior pid. */
      inferior_pid = saved_inferior_pid;
      return;
    }
  else
    disable_a_thread (tidstr);
}

static int
do_captured_thread_select (void *tidstr)
{
  int num;
  struct thread_info *tp;
  /* Bindu 061902: Save the actual thread id user used in 'thr'.
     We can use this to report back to the user. */
  int thr;

  if (tidstr && !could_be_number((char *)tidstr))
    error ("Invalid thread ID %s. Thread ID must be an integer.\n",
           (char *)tidstr);

  num = atoi ((char *)tidstr);
  thr = num;

#ifdef HP_MXN
  /* If the thread id is -ve, it's a ktid. */
  if (is_mxn)
    {
      if (!show_all_kthreads && !num)
	error ("Cannot switch to thread with no user thread associated.");
      if (num < 0)
	num = get_gdb_tid_from_ktid (-num);
      else
	num = get_gdb_tid_from_utid (num);
      num = pid_to_thread_id (num);
    }
#endif

  tp = find_thread_id (num);

#ifdef UI_OUT
  if (!tp)
    error ("Thread ID %d not known.", num);
#else
  if (!tp)
    error ("Thread ID %d not known.  Use the \"info threads\" command to\n\
see the IDs of currently known threads.", thr);
#endif

  if (!thread_alive (tp))
    error ("Thread ID %d has terminated.\n", thr);

#if defined(GDB_TARGET_IS_HPUX)
  if( tp->pid == inferior_pid )
    {
      if (!batch_rtc)
        printf_filtered( "[Current thread is already %d (%s)]\n",
#ifdef HP_MXN
		       (is_mxn)? thr:
#endif /* HP_MXN */
                       pid_to_thread_id(  inferior_pid ),
                       target_tid_to_str( inferior_pid ));
      return GDB_RC_NONE;
    }

  /* bindu 101601: There is no valid state for the thread with no user thread 
     associated. So, don't allow the users to switch to this thread.*/
  if( is_mxn && !show_all_kthreads && tp->pid & KTID_MARKER)
    {
      printf_filtered( "Can't switch to thread with no associated user thread\n" );
      return GDB_RC_FAIL;
    }

#endif /* defined(GDB_TARGET_IS_HPUX) */

  switch_to_thread (tp->pid);

#ifdef UI_OUT
  ui_out_text (uiout, "[Switching to thread ");
  ui_out_field_int (uiout, "new-thread-id", pid_to_thread_id (inferior_pid));
  ui_out_text (uiout, " (");
#if defined(GDB_TARGET_IS_HPUX)
  ui_out_text (uiout, target_tid_to_str (inferior_pid));
#else
  ui_out_text (uiout, target_pid_to_str (inferior_pid));
#endif
  ui_out_text (uiout, ")]");
#else /* UI_OUT */
  printf_filtered ("[Switching to thread %d (%s)]\n",
#ifdef HP_MXN
                       (is_mxn)? thr:
#endif /* HP_MXN */
		   pid_to_thread_id (inferior_pid),
#if defined(GDB_TARGET_IS_HPUX)
		   target_tid_to_str (inferior_pid)
#else
		   target_pid_to_str (inferior_pid)
#endif
    );
#endif /* UI_OUT */

  print_stack_frame (selected_frame, selected_frame_level, 1);
  return GDB_RC_OK;
}


enum gdb_rc
gdb_thread_select (char *tidstr)
{
  return catch_errors (do_captured_thread_select, tidstr,
		       NULL, RETURN_MASK_ALL);
}


/* This function is similar to info addr from printcmd.c. It searches
   process global data and, it no symbol was found, searches the stack
   of a thread given by 'thr'. This was added primarily for lookups of
   variable names of pthread primitives in the enhanced thread debugging
   support. Searching all stacks is very expensive, so this was added 
   as an optimization: we can always tell which thread created the pthread
   primitive, which means we alny need to search that thread's stack */
char* 
sym_info_addr(CORE_ADDR addr, CORE_ADDR* offset, int64_t thr)
{
  struct minimal_symbol *msymbol;
  struct objfile *objfile;
  struct obj_section *osect;
  asection *sect;
  CORE_ADDR sect_addr;
  int matches = 0;

  addr = SWIZZLE (addr);

  /* Search globals */
  ALL_OBJSECTIONS (objfile, osect)
  {
    sect = osect->the_bfd_section;
    sect_addr = overlay_mapped_address (addr, sect);

    if (osect->addr <= sect_addr && sect_addr < osect->endaddr &&
	(msymbol = lookup_minimal_symbol_by_pc_section (sect_addr, sect)))
      {
	matches = 1;
	*offset = (CORE_ADDR) (sect_addr - SYMBOL_VALUE_ADDRESS (msymbol));
        return SYMBOL_SOURCE_NAME (msymbol);
      }
  }

  /* If symbol not found search the stack of thread 'thr' */
  if (matches == 0) 
    { 
      struct thread_info* tp;
      struct cleanup* old_chain;
      tid_t gdb_tid;

      info_sym_data_t info_sym_data;
      info_sym_data.addr = addr;
      info_sym_data.name = NULL;
      info_sym_data.offset = 0;
      info_sym_data.is_print = 0;
      info_sym_data.match_thread_stack_sym = 0;

      old_chain = make_cleanup_restore_current_thread (inferior_pid);

      gdb_tid = get_gdb_tid_from_utid (thr);
      tp = find_thread_pid (gdb_tid);
      if (tp && thread_alive (tp))
        {
          switch_to_thread (tp->pid);
          print_bad_pc_unwind_msg = 0;
          search_thread_stack_sym (&info_sym_data);
          print_bad_pc_unwind_msg = 1;
        }

      do_cleanups (old_chain);

      if (info_sym_data.match_thread_stack_sym)
        {
          *offset = info_sym_data.offset;
          return info_sym_data.name;
        }
    }
  
  return NULL;
}


/* Commands with a prefix of `thread'.  */
struct cmd_list_element *thread_cmd_list = NULL;

void
_initialize_thread ()
{
  static struct cmd_list_element *thread_apply_list = NULL;

  add_info ("threads", info_threads_command,
	    "Information about currently known threads.\n\nUsage:\n\tinfo threads \
[<THREAD-ID>]\n\nIf THREAD-ID is specified, the detailed report of that thread is displayed");

  add_prefix_cmd ("thread", class_run, thread_command,
		  "Use this command to switch between threads.\n\n\
Usage:\n\tthread [<THREAD-ID>]\n\n\
The new thread ID must be currently known.", &thread_cmd_list, "thread ", 1,
		  &cmdlist);

  add_prefix_cmd ("apply", class_run, thread_apply_command,
		  "Apply a command 'CMD' to a list of threads.\n\n\
Usage:\n\tthread apply <THREAD-ID1> [<THREAD-ID2>] [...] <CMD>\n",
		  &thread_apply_list, "apply ", 1, &thread_cmd_list);

  add_cmd ("all", class_run, thread_apply_all_command,
	   "Apply a command 'CMD' to all threads.\n\nUsage:\n\tthread apply all <CMD>\n\n",
	   &thread_apply_list);

 /* thread enable, disable */
  add_cmd ("enable", class_run, thread_enable_command,
           "Enable a thread from disabled list.\n\n\
Usage:\n\tthread enable all | <THREAD-ID>\n\n\
Use 'thread enable all' to enable all threads.\n\
Use it on a thread-id to enable one specific thread.",
           &thread_cmd_list);
  add_cmd ("disable", class_run, thread_disable_command,
           "Disable a thread.\n\n\
Usage:\n\tthread disable all | <THREAD-ID>\n\n\
Use 'thread disable all' to disable all threads.\n\
Use it on a thread-id to disable one specific thread.",
           &thread_cmd_list);

  if (!xdb_commands)
    add_com_alias ("t", "thread", class_run, 1);

#ifdef HP_MXN
  add_show_from_set
    (add_set_cmd ("show-all-kthreads", class_support, var_zinteger,
                  (char *) &show_all_kthreads,
                  "Set to see all the kernel threads in the program.\n\n\
Usage:\nTo set new value:\n\tset show-all-kthreads <INTEGER>\nTo see current \
value:\n\tshow show-all-kthreads\n\n\
By default this is set to 0 and gdb reports only those kernel threads which have\n\
user threads associated with them.\n", 
                  &setlist),
     &showlist);
#endif

}
