/* Data structures associated with thread tracing in GDB.
   Copyright (C) 1992, 93, 94, 95, 96, 98, 1999 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#if !defined (THREAD_TRACING_H)
#define THREAD_TRACING_H 1

#include <pthread.h>
#include <inttypes.h>
#include "defs.h"

void prepare_for_thread_tracing (int attached);

int read_thread_trace_info();

/* Enum describes relationships types batween objects */
typedef enum relation_type_tag {
    REL_UNKNOWN,
    REL_PRIMARY_OWNER,	// This object is the owner (writer-owner)
    REL_SECONDARY_OWNER,// This object a secondary owner (reader-owner)
    REL_OWNED_BY,       // This object is owned by
    REL_WAITER,		// This object is waiting
    REL_WAITED_ON_BY,	// This object is waited on by
    REL_CREATED_BY	// This object is created by
} relation_type_t;

/* Enum describes object types */
typedef enum object_type_tag {
    OBJT_UNKNOWN,
    OBJT_THREAD,
    OBJT_MUTEX,
    OBJT_RWLOCK,
    OBJT_COND
} object_type_t;

/* Flags bits used in the object flags field */
typedef enum object_flags_tag {
    OBJF_DELETED = 0x1,		   // Object no longer exists
    OBJF_DEADLOCKED = 0x2,	   // Object is involved in a deadlock
    OBJF_RECURSIVELY_LOCKED = 0x4, // Object was locked in a recursive manner
    OBJF_SUSPENDED = 0x8,	   // Thread is suspended
    OBJF_CREATED = 0x16		   // Object creation complete
} object_flags_t;

/* Gdb view of a pthread 'thread' object */
typedef struct gdb_thread_tag {
    CORE_ADDR address; /* thread address */
    CORE_ADDR attributes; /* Attributes object pointer */
    CORE_ADDR stack_base;	/* Base of thread's stack */
    CORE_ADDR stack_reserve;	/* First byte of reserved zone */
    CORE_ADDR stack_yellow;	/* First byte of yellow zone */
    CORE_ADDR stack_guard;	/* First byte of guard zone */
    CORE_ADDR stack_highwater;  /* Stack high water mark */
    CORE_ADDR start_routine; /* Thread start rtn */
    CORE_ADDR start_arg;	/* Thread start arg */
    uint32_t stack_size; /* Total size of stack */
    int32_t waiter_count; /* Number of waiters */
    int32_t policy; /* Thread's scheduling policy */
    int32_t priority; /* Thread's (base) priority */
    int32_t flags; /* PTHREAD_TRACE_THD_ flags */
    uint32_t init_cancel; 	/* Initial cancel state */
    uint32_t cancel; 	/* Cancel state */
    uint8_t kind; /* thread kind */
    uint8_t creator; /* Creating interface */
    uint8_t state; /* thread state */
} gdb_thread_t;


/* Gdb view of a read-write lock object */
typedef struct gdb_rwlock_tag {
    CORE_ADDR address; /* Mutex address */
    CORE_ADDR attributes; /* Attributes object pointer */
    int32_t reader_count; /* Current number of readers */
    int32_t trylock_failures;  /* Cumulative number of trylock failures */
    int32_t lock_count;  /* Cumulative number of lock requests */ 
    int32_t waiter_count; /* Current number of waters */
    int32_t contended_locks; /* Cumulative number of contended locks */
    uint32_t flags; /* Miscellaneous flags */
} gdb_rwlock_t;


/* Gdb view of a condition variable object */
typedef struct gdb_cond_tag {
    CORE_ADDR address; /* condvar address */
    CORE_ADDR attributes; /* Attributes object pointer */
    uint64_t mutex_id; /* ID of the corresponding mutex */
    int32_t waiter_count; /* Current number of waters */
    int32_t wait_count; /* Cumulative number of wait requests */
    int32_t broadcast_count; /* Cumulative number of broadcasts */
    int32_t signal_count; /* Cumulative number of signals */
    uint32_t flags; /* Miscellaneous flags */
} gdb_cond_t;


/* Gdb view of a mutex object */
typedef struct gdb_mutex_tag {
    CORE_ADDR address; /* Mutex address */
    CORE_ADDR attributes; /* Attributes object pointer */
    int32_t trylock_failures;
    int32_t lock_count;
    int32_t waiter_count;
    int32_t contended_locks;
    int32_t priority; /* Current priority */
    int32_t prioceiling; /* Priority ceiling */
    uint32_t flags; /* Miscellaneous flags */
    uint8_t type; /* Mutex type */
    uint8_t protocol; /* Mutex protocol */
} gdb_mutex_t;


/* Relation between objects */
typedef struct gdb_relation_tag {
    int64_t object_id; /* object ID */
    int32_t frame_count; /* number of stack frames */
    uint8_t type; /* relation type */
    CORE_ADDR* frames; /* stack frames */
} gdb_relation_t;


/* Generic type to describe the gdb view of a pthread object */
typedef struct gdb_pthread_object_tag {
    int64_t id; /* object id */
    int32_t index; /* gdb index visible to the user (not used for threads) */
    int32_t num_rels; /* number of relations */
    uint8_t type; /* object type */
    uint8_t flags; /* object flags */

    union {
        gdb_thread_t thread;
        gdb_mutex_t mutex;
        gdb_rwlock_t rwlock;
        gdb_cond_t cond;
    } obj;

    struct gdb_pthread_object_tag* next; /* next ptr */
    gdb_relation_t* rel_list; /* list of relations */
} gdb_pthread_object_t;

#endif /* !defined (THREAD_TRACING_H) */
