#include <stdlib.h>
#include <pthread.h>
#include <dl.h>
#include <dlfcn.h>
#include "analysis.h"
#include "trace_util.h"
#include "defs.h"
#include "rtc.h"


#define OBJECT_HASH_SIZE 1021
static object_t* object_hash[OBJECT_HASH_SIZE];
/* Spinlock to control access to rtc pthread-related data structures */
static __pthread_trace_spinlock_t hash_lock = __PTHREAD_SPINLOCK_INIT;
/* Thread in critical section currently updating pthread-related 
   data structures */
volatile int64_t __rtc_critical_thread = -1;

/* Flags to indicate which rules are enabled */
volatile int __rtc_non_recursive_relock = 0;
volatile int __rtc_unlock_not_own = 0;
volatile int __rtc_mixed_sched_policy = 0;
volatile int __rtc_condvar_multiple_mutexes = 0;
volatile int __rtc_condvar_wait_no_mutex = 0;
volatile int __rtc_deleted_obj_used = 0;
volatile int __rtc_thread_exit_own_mutex = 0;
volatile int __rtc_thread_exit_no_join_detach = 0;
volatile int __rtc_stack_utilization = 0; /* % */
volatile int __rtc_num_waiters = 0;
volatile int __rtc_ignore_sys_objects = 1;

int __rtc_pthread_info (void);
int (*volatile threads_info_plabel) (void) = __rtc_pthread_info;

/* File name for the temp data file */
char thread_info_filename[50] = {0};

/* flags to indicate if thread tracing or debug tracing is enabled.
   defined in thread_trace.c */
extern int thread_trace_enabled;
extern int dbg_trace_enabled;

extern void __rtc_thread_event (enum rtc_event ecode, void* arg1,
                                void* arg2, void* arg3);

#pragma HP_NO_RELOCATION __rtc_pthread_mutex_init, __rtc_pthread_mutex_lock
#pragma HP_NO_RELOCATION __rtc_pthread_mutex_unlock, __rtc_pthread_mutex_trylock
#pragma HP_NO_RELOCATION __rtc_pthread_mutex_destroy
#pragma HP_NO_RELOCATION __rtc_pthread_detach, __rtc_pthread_exit
#pragma HP_NO_RELOCATION __rtc_pthread_create, __rtc_pthread_join
#pragma HP_NO_RELOCATION __rtc_pthread_rwlock_init, __rtc_pthread_rwlock_destroy
#pragma HP_NO_RELOCATION __rtc_pthread_rwlock_rdlock, __rtc_pthread_rwlock_unlock
#pragma HP_NO_RELOCATION __rtc_pthread_rwlock_wrlock, __rtc_pthread_rwlock_tryrdlock
#pragma HP_NO_RELOCATION __rtc_pthread_rwlock_trywrlock
#pragma HP_NO_RELOCATION __rtc_pthread_cond_init, __rtc_pthread_cond_destroy
#pragma HP_NO_RELOCATION __rtc_pthread_cond_wait, __rtc_pthread_cond_signal
#pragma HP_NO_RELOCATION __rtc_pthread_cond_wait, __rtc_pthread_cond_timedwait
#pragma HP_NO_RELOCATION __rtc_pthread_info

#pragma HP_LONG_RETURN __rtc_pthread_mutex_init, __rtc_pthread_mutex_lock
#pragma HP_LONG_RETURN __rtc_pthread_mutex_unlock, __rtc_pthread_mutex_trylock
#pragma HP_LONG_RETURN __rtc_pthread_mutex_destroy
#pragma HP_LONG_RETURN __rtc_pthread_detach, __rtc_pthread_exit
#pragma HP_LONG_RETURN __rtc_pthread_create, __rtc_pthread_join
#pragma HP_LONG_RETURN __rtc_pthread_rwlock_init, __rtc_pthread_rwlock_destroy
#pragma HP_LONG_RETURN __rtc_pthread_rwlock_rdlock, __rtc_pthread_rwlock_unlock
#pragma HP_LONG_RETURN __rtc_pthread_rwlock_wrlock, __rtc_pthread_rwlock_tryrdlock
#pragma HP_LONG_RETURN __rtc_pthread_rwlock_trywrlock
#pragma HP_LONG_RETURN __rtc_pthread_cond_init, __rtc_pthread_cond_destroy
#pragma HP_LONG_RETURN __rtc_pthread_cond_wait, __rtc_pthread_cond_signal
#pragma HP_LONG_RETURN __rtc_pthread_cond_timedwait
#pragma HP_LONG_RETURN __rtc_pthread_info


/* Function pointers used to re-direct calls from rtc intercept points to
   the actual libpthread interfaces */
static int (*real_pthread_mutex_init) (pthread_mutex_t *mutex, 
                                       const pthread_mutexattr_t *attr) = 0;
static int (*real_pthread_mutex_lock) (pthread_mutex_t *mutex) = 0;
static int (*real_pthread_mutex_trylock) (pthread_mutex_t *mutex) = 0;
static int (*real_pthread_mutex_unlock) (pthread_mutex_t *mutex) = 0;
static int (*real_pthread_mutex_destroy) (pthread_mutex_t *mutex) = 0;
static int (*real_pthread_create) (pthread_t *thread, 
                                   const pthread_attr_t *attr,
                                   void *(*start_routine)(void *),
                                   void *arg) = 0;
static void (*real_pthread_exit) (void *value_ptr) = 0;
static int (*real_pthread_detach) (pthread_t thread) = 0;
static int (*real_pthread_join) (pthread_t thread, void **value_ptr) = 0;
static int (*real_pthread_rwlock_init) (pthread_rwlock_t *rwlock,
                                        const pthread_rwlockattr_t *attr) = 0;
static int (*real_pthread_rwlock_destroy) (pthread_rwlock_t *rwlock) = 0;
static int (*real_pthread_rwlock_rdlock) (pthread_rwlock_t *rwlock) = 0;
static int (*real_pthread_rwlock_tryrdlock) (pthread_rwlock_t *rwlock) = 0;
static int (*real_pthread_rwlock_wrlock) (pthread_rwlock_t *rwlock) = 0;
static int (*real_pthread_rwlock_trywrlock) (pthread_rwlock_t *rwlock) = 0;
static int (*real_pthread_rwlock_unlock) (pthread_rwlock_t *rwlock) = 0;
static int (*real_pthread_cond_init) (pthread_cond_t *cond,
                                      const pthread_condattr_t *attr) = 0;
static int (*real_pthread_cond_destroy) (pthread_cond_t *cond) = 0;
static int (*real_pthread_cond_wait) (pthread_cond_t *cond,
                                 pthread_mutex_t *mutex) = 0;
static int (*real_pthread_cond_timedwait) (pthread_cond_t *cond,
                                           pthread_mutex_t *mutex,
                                           const struct timespec *abstime) = 0;
static int (*real_pthread_cond_signal) (pthread_cond_t *cond) = 0;

/* Initialize pthread intercept mechanism and allocate memory.
   Since we can't do memory allocation at the time we need it in pthread 
   tracing calls, we pre-allocate chunks at every interface entry, if 
   necessary, and dish them out later */
#define INIT_AND_ALLOC_POOL() \
{ \
    if (!real_pthread_create) init_pthread_intercepts(); \
    if (thread_trace_enabled) { \
       allocate_new_block(1); \
    } \
}

/* Wrapper function for pthread_mutex_init */
#pragma _HP_SECONDARY_DEF __rtc_pthread_mutex_init pthread_mutex_init
int 
__rtc_pthread_mutex_init (pthread_mutex_t *mutex, 
                             const pthread_mutexattr_t *attr)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_mutex_init (mutex, attr);
}


/* Wrapper function for pthread_mutex_lock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_mutex_lock pthread_mutex_lock
int 
__rtc_pthread_mutex_lock (pthread_mutex_t *mutex)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_mutex_lock(mutex);
}


/* Wrapper function for pthread_mutex_unlock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_mutex_unlock pthread_mutex_unlock
int 
__rtc_pthread_mutex_unlock (pthread_mutex_t *mutex)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_mutex_unlock (mutex);
}


/* Wrapper function for pthread_mutex_trylock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_mutex_trylock pthread_mutex_trylock
int 
__rtc_pthread_mutex_trylock (pthread_mutex_t *mutex)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_mutex_trylock (mutex);
}


/* Wrapper function for pthread_mutex_destroy */
#pragma _HP_SECONDARY_DEF __rtc_pthread_mutex_destroy pthread_mutex_destroy
int 
__rtc_pthread_mutex_destroy (pthread_mutex_t *mutex)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_mutex_destroy (mutex);
}


/* Wrapper function for pthread_create */
#pragma _HP_SECONDARY_DEF __rtc_pthread_create pthread_create
int 
__rtc_pthread_create (pthread_t *thread, 
                      const pthread_attr_t *attr,
                      void *(*start_routine)(void *),
                      void *arg)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_create (thread, attr, start_routine, arg);
}


/* Wrapper function for pthread_exit */
#pragma _HP_SECONDARY_DEF __rtc_pthread_exit pthread_exit
void 
__rtc_pthread_exit (void *value_ptr)
{
  INIT_AND_ALLOC_POOL ();
  real_pthread_exit (value_ptr);
}


/* Wrapper function for pthread_detach */
#pragma _HP_SECONDARY_DEF __rtc_pthread_detach pthread_detach
int 
__rtc_pthread_detach (pthread_t thread)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_detach (thread);
}


/* Wrapper function for pthread_join */
#pragma _HP_SECONDARY_DEF __rtc_pthread_join pthread_join
int 
__rtc_pthread_join (pthread_t thread, void **value_ptr)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_join (thread, value_ptr);
}


/* Wrapper function for pthread_rwlock_init */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_init pthread_rwlock_init
int 
__rtc_pthread_rwlock_init (pthread_rwlock_t *rwlock,
                           const pthread_rwlockattr_t *attr)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_init (rwlock, attr);
}


/* Wrapper function for pthread_rwlock_destroy */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_destroy pthread_rwlock_destroy
int 
__rtc_pthread_rwlock_destroy (pthread_rwlock_t *rwlock)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_destroy (rwlock);
}


/* Wrapper function for pthread_rwlock_rdlock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_rdlock pthread_rwlock_rdlock
int 
__rtc_pthread_rwlock_rdlock (pthread_rwlock_t *rwlock)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_rdlock (rwlock);
}


/* Wrapper function for pthread_rwlock_tryrdlock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_tryrdlock pthread_rwlock_tryrdlock
int 
__rtc_pthread_rwlock_tryrdlock (pthread_rwlock_t *rwlock)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_tryrdlock (rwlock);
}


/* Wrapper function for pthread_rwlock_wrlock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_wrlock pthread_rwlock_wrlock
int 
__rtc_pthread_rwlock_wrlock (pthread_rwlock_t *rwlock)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_wrlock (rwlock);
}


/* Wrapper function for pthread_rwlock_trywrlock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_trywrlock pthread_rwlock_trywrlock
int 
__rtc_pthread_rwlock_trywrlock (pthread_rwlock_t *rwlock)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_trywrlock (rwlock);
}


/* Wrapper function for pthread_rwlock_unlock */
#pragma _HP_SECONDARY_DEF __rtc_pthread_rwlock_unlock pthread_rwlock_unlock
int 
__rtc_pthread_rwlock_unlock (pthread_rwlock_t *rwlock)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_rwlock_unlock (rwlock);
}


/* Wrapper function for pthread_cond_init */
#pragma _HP_SECONDARY_DEF __rtc_pthread_cond_init pthread_cond_init
int 
__rtc_pthread_cond_init (pthread_cond_t *cond,
                         const pthread_condattr_t *attr)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_cond_init (cond, attr);
}


/* Wrapper function for pthread_cond_destroy */
#pragma _HP_SECONDARY_DEF __rtc_pthread_cond_destroy pthread_cond_destroy
int 
__rtc_pthread_cond_destroy (pthread_cond_t *cond)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_cond_destroy (cond);
}


/* Wrapper function for pthread_cond_wait */
#pragma _HP_SECONDARY_DEF __rtc_pthread_cond_wait pthread_cond_wait
int 
__rtc_pthread_cond_wait (pthread_cond_t *cond,
                         pthread_mutex_t *mutex)
{
  INIT_AND_ALLOC_POOL();
  return real_pthread_cond_wait (cond, mutex);
}


/* Wrapper function for pthread_cond_timedwait */
#pragma _HP_SECONDARY_DEF __rtc_pthread_cond_timedwait pthread_cond_timedwait
int 
__rtc_pthread_cond_timedwait (pthread_cond_t *cond,
                              pthread_mutex_t *mutex,
                              const struct timespec *abstime)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_cond_timedwait (cond, mutex, abstime);
}


/* Wrapper function for pthread_cond_signal */
#pragma _HP_SECONDARY_DEF __rtc_pthread_cond_signal pthread_cond_signal
int 
__rtc_pthread_cond_signal (pthread_cond_t *cond)
{
  INIT_AND_ALLOC_POOL ();
  return real_pthread_cond_signal (cond);
}


/* Dump info about a given thread into a file specified by fd */
static void 
dump_thread_info (int fd, object_t* obj)
{
  gdb_pthread_object_t gdb_obj;

  assert (obj->type == OBJT_THREAD);

  gdb_obj.id = obj->id;
  gdb_obj.index = -1;
  gdb_obj.type = obj->type;
  gdb_obj.flags = obj->flags;
  gdb_obj.num_rels = obj->num_rels;

  gdb_obj.obj.thread.address = obj->obj.thread.tinit.handle;
  gdb_obj.obj.thread.attributes = obj->obj.thread.tinit.attributes;
  gdb_obj.obj.thread.stack_base = obj->obj.thread.tinit.stack_base;
  gdb_obj.obj.thread.stack_reserve = obj->obj.thread.tinit.stack_reserve;
  gdb_obj.obj.thread.start_routine = obj->obj.thread.tinit.start;
  gdb_obj.obj.thread.start_arg = obj->obj.thread.tinit.start_arg;
  gdb_obj.obj.thread.stack_size = obj->obj.thread.tinit.stack_size;
  gdb_obj.obj.thread.policy = obj->obj.thread.tinit.policy;
  gdb_obj.obj.thread.priority = obj->obj.thread.tinit.priority;
  gdb_obj.obj.thread.flags = obj->obj.thread.tinit.flags;
  gdb_obj.obj.thread.init_cancel = obj->obj.thread.tinit.cancel;
  gdb_obj.obj.thread.kind = obj->obj.thread.tinit.kind;
  gdb_obj.obj.thread.creator = obj->obj.thread.tinit.creator;
  
  gdb_obj.obj.thread.stack_highwater = 
      (CORE_ADDR) obj->obj.thread.stack_highwater;
  gdb_obj.obj.thread.state = obj->obj.thread.thread_state;
  gdb_obj.obj.thread.cancel = obj->obj.thread.cancel_state;

  write (fd, &gdb_obj, offsetof (gdb_pthread_object_t, next));
}


/* Dump info about a given mutex into a file specified by fd */
static void 
dump_mutex_info (int fd, object_t* obj)
{
  gdb_pthread_object_t gdb_obj;

  assert (obj->type == OBJT_MUTEX);

  gdb_obj.id = obj->id;
  gdb_obj.index = obj->gdb_idx;
  gdb_obj.type = obj->type;
  gdb_obj.flags = obj->flags;
  gdb_obj.num_rels = obj->num_rels;

  gdb_obj.obj.mutex.type = obj->obj.mutex.minit.type;
  gdb_obj.obj.mutex.protocol = obj->obj.mutex.minit.protocol;
  gdb_obj.obj.mutex.attributes = obj->obj.mutex.minit.attributes;
  gdb_obj.obj.mutex.address = obj->obj.mutex.minit.address;
  gdb_obj.obj.mutex.flags = obj->obj.mutex.minit.flags;
  gdb_obj.obj.mutex.prioceiling = obj->obj.mutex.minit.prioceiling;
  gdb_obj.obj.mutex.priority = obj->obj.mutex.minit.priority;

  gdb_obj.obj.mutex.waiter_count = obj->obj.mutex.waiter_count;
  gdb_obj.obj.mutex.lock_count = obj->obj.mutex.lock_count;
  gdb_obj.obj.mutex.trylock_failures = obj->obj.mutex.trylock_failures;
  gdb_obj.obj.mutex.contended_locks = obj->obj.mutex.contended_locks;

  write (fd, &gdb_obj, offsetof (gdb_pthread_object_t, next));
}


/* Dump info about a given condition variable  into a file specified by fd */
static void 
dump_condvar_info (int fd, object_t* obj)
{
  gdb_pthread_object_t gdb_obj;

  assert (obj->type == OBJT_COND);

  gdb_obj.id = obj->id;
  gdb_obj.index = obj->gdb_idx;
  gdb_obj.type = obj->type;
  gdb_obj.flags = obj->flags;
  gdb_obj.num_rels = obj->num_rels;

  gdb_obj.obj.cond.attributes = obj->obj.cond.cinit.attributes;
  gdb_obj.obj.cond.address = obj->obj.cond.cinit.address;
  gdb_obj.obj.cond.flags = obj->obj.cond.cinit.flags;
  
  gdb_obj.obj.cond.mutex_id = obj->obj.cond.mutex_id;
  gdb_obj.obj.cond.waiter_count = obj->obj.cond.waiter_count;
  gdb_obj.obj.cond.wait_count = obj->obj.cond.wait_count;
  gdb_obj.obj.cond.broadcast_count = obj->obj.cond.broadcast_count;
  gdb_obj.obj.cond.signal_count = obj->obj.cond.signal_count;

  write (fd, &gdb_obj, offsetof (gdb_pthread_object_t, next));
}


/* Dump info about a given read-write lock into a file specified by fd */
static void 
dump_rwlock_info (int fd, object_t* obj)
{
  gdb_pthread_object_t gdb_obj;

  assert (obj->type == OBJT_RWLOCK);

  gdb_obj.id = obj->id;
  gdb_obj.index = obj->gdb_idx;
  gdb_obj.type = obj->type;
  gdb_obj.flags = obj->flags;
  gdb_obj.num_rels = obj->num_rels;

  gdb_obj.obj.rwlock.attributes = obj->obj.rwlock.rwlinit.attributes;
  gdb_obj.obj.rwlock.address = obj->obj.rwlock.rwlinit.address;
  gdb_obj.obj.rwlock.flags = obj->obj.rwlock.rwlinit.flags;

  gdb_obj.obj.rwlock.reader_count = obj->obj.rwlock.waiter_count;
  gdb_obj.obj.rwlock.waiter_count = obj->obj.rwlock.waiter_count;
  gdb_obj.obj.rwlock.lock_count = obj->obj.rwlock.lock_count;
  gdb_obj.obj.rwlock.trylock_failures = obj->obj.rwlock.trylock_failures;
  gdb_obj.obj.rwlock.contended_locks = obj->obj.rwlock.contended_locks;

  write (fd, &gdb_obj, offsetof (gdb_pthread_object_t, next));
}


/* Dump given object's list of relations into a file specified by fd */
static void 
dump_relation_info (int fd, object_t* obj)
{
  gdb_relation_t gdb_rel;
  relation_t* rel = obj->rel_list;
  static CORE_ADDR frames[MAX_CALL_FRAMES];

  while (rel)
    {
      gdb_rel.object_id = rel->object_id;
      gdb_rel.frame_count = rel->frame_count;
      gdb_rel.type = rel->type;
      write (fd, &gdb_rel, offsetof (gdb_relation_t, frames));

      for (int i = 0; i < rel->frame_count; i++)
          frames[i] = (CORE_ADDR) rel->frames[i];

      write (fd, frames, rel->frame_count * sizeof (CORE_ADDR));
      rel = rel->next;
    }
}


/* This routine if called by gdb to retrieve extended info about pthread 
   primitives. It goes over the entire hash table of objects and dumps
   info about each of them into /tmp/__thread_info.<pid> */
int 
__rtc_pthread_info (void)
{
  int fd;
  uint32_t thread_count = 0;
  uint32_t mutex_count = 0;
  uint32_t rwlock_count = 0;
  uint32_t cv_count = 0;

  if (!thread_trace_enabled) return RTC_NOT_RUNNING;

  /* Create temp output files */
  fd = creat (thread_info_filename, O_RDWR|O_CREAT|O_EXCL);

  if (fd == -1)
    {
      if (fd != -1) close (fd);
      return RTC_FOPEN_FAILED;
    }

  /* Write zero record count, it will be re-writen after we have 
     the actual counts */
  write (fd, &thread_count, sizeof (thread_count));
  write (fd, &mutex_count, sizeof (mutex_count));
  write (fd, &rwlock_count, sizeof (rwlock_count));
  write (fd, &cv_count, sizeof (cv_count));

  for (int i = 0; i < OBJECT_HASH_SIZE; i++)
    {
      object_t* obj = object_hash[i];
      while (obj)
        {
          switch (obj->type)
            {
              case OBJT_THREAD:
                  thread_count++;
                  dump_thread_info (fd, obj);
                  break;
              case OBJT_MUTEX:
                  mutex_count++;
                  dump_mutex_info (fd, obj);
                  break;
              case OBJT_COND:
                  cv_count++;
                  dump_condvar_info (fd, obj);
                  break;
              case OBJT_RWLOCK:
                  rwlock_count++;
                  dump_rwlock_info (fd, obj);
                  break;
              default:
                  return RTC_BAD_HEADER;
            }
          dump_relation_info (fd, obj);
          obj = obj->next;
        }
    }

  /* Write actual record counts */
  lseek (fd, 0, SEEK_SET);
  write (fd, &thread_count, sizeof (thread_count));
  write (fd, &mutex_count, sizeof (mutex_count));
  write (fd, &rwlock_count, sizeof (rwlock_count));
  write (fd, &cv_count, sizeof (cv_count));
  close (fd);

  return thread_count+mutex_count+rwlock_count+cv_count;
}


/* Set up call intercepts for most pthread entry points. The reason
   for this is to pre-allocate memory necessary to store trace info
   generated by libpthread_tr. Because a lot of tracing calls are
   generated from within libpthread_tr, we cannot use libc memory allocation
   inside our event processing routines. The memory is allocated on entry
   to libpthread_tr if we're in danger of running out, and dished out
   inside event processing as needed. */
void 
init_pthread_intercepts (void)
{
  struct shl_descriptor *desc;

  if (real_pthread_mutex_init) return;

  /* Initialize the object hash */
  utl_memclr(object_hash, sizeof (object_hash));

  int index = 1;
  while (shl_get (index, &desc) != -1)
    {
      index++;

      if (utl_strstr (desc->filename, "/libpthread.") ||
          utl_strstr (desc->filename, "/libc."))
        {
          shl_findsym (&desc -> handle, "pthread_mutex_init",
                       TYPE_PROCEDURE, &real_pthread_mutex_init);
          shl_findsym (&desc -> handle, "pthread_mutex_lock",
                       TYPE_PROCEDURE, &real_pthread_mutex_lock);
          shl_findsym (&desc -> handle, "pthread_mutex_trylock",
                       TYPE_PROCEDURE, &real_pthread_mutex_trylock);
          shl_findsym (&desc -> handle, "pthread_mutex_unlock",
                       TYPE_PROCEDURE, &real_pthread_mutex_unlock);
          shl_findsym (&desc -> handle, "pthread_mutex_destroy",
                       TYPE_PROCEDURE, &real_pthread_mutex_destroy);
          shl_findsym (&desc -> handle, "pthread_create",
                       TYPE_PROCEDURE, &real_pthread_create);
          shl_findsym (&desc -> handle, "pthread_exit",
                       TYPE_PROCEDURE, &real_pthread_exit);
          shl_findsym (&desc -> handle, "pthread_detach",
                       TYPE_PROCEDURE, &real_pthread_detach);
          shl_findsym (&desc -> handle, "pthread_join",
                       TYPE_PROCEDURE, &real_pthread_join);
          shl_findsym (&desc -> handle, "pthread_rwlock_init",
                       TYPE_PROCEDURE, &real_pthread_rwlock_init);
          shl_findsym (&desc -> handle, "pthread_rwlock_destroy",
                       TYPE_PROCEDURE, &real_pthread_rwlock_destroy);
          shl_findsym (&desc -> handle, "pthread_rwlock_rdlock",
                       TYPE_PROCEDURE, &real_pthread_rwlock_rdlock);
          shl_findsym (&desc -> handle, "pthread_rwlock_tryrdlock",
                       TYPE_PROCEDURE, &real_pthread_rwlock_tryrdlock);
          shl_findsym (&desc -> handle, "pthread_rwlock_wrlock",
                       TYPE_PROCEDURE, &real_pthread_rwlock_wrlock);
          shl_findsym (&desc -> handle, "pthread_rwlock_trywrlock",
                       TYPE_PROCEDURE, &real_pthread_rwlock_trywrlock);
          shl_findsym (&desc -> handle, "pthread_rwlock_unlock",
                       TYPE_PROCEDURE, &real_pthread_rwlock_unlock);
          shl_findsym (&desc -> handle, "pthread_cond_init",
                       TYPE_PROCEDURE, &real_pthread_cond_init);
          shl_findsym (&desc -> handle, "pthread_cond_destroy",
                       TYPE_PROCEDURE, &real_pthread_cond_destroy);
          shl_findsym (&desc -> handle, "pthread_cond_wait",
                       TYPE_PROCEDURE, &real_pthread_cond_wait);
          shl_findsym (&desc -> handle, "pthread_cond_timedwait",
                       TYPE_PROCEDURE, &real_pthread_cond_timedwait);
          shl_findsym (&desc -> handle, "pthread_cond_signal",
                       TYPE_PROCEDURE, &real_pthread_cond_signal);
          
          if (real_pthread_mutex_init) break;
        }
    }

  if (!real_pthread_mutex_init) 
    {
      trc_internal_error_msg ("Pthread intercept initialization failed\n");
      thread_trace_enabled = 0;
    }
}


/* Delete a gived relation */
static void 
relation_delete (relation_t* rel)
{
  utl_free (rel->frames);
  utl_free (rel);
}


/* Delete all relations of a given object */
static void
relation_delete_all (object_t* obj)
{
  relation_t* rel = obj->rel_list;
  while(rel)
    {
      relation_t* saved_rel = rel;
      rel = rel->next;
      relation_delete (saved_rel);
    }
  obj->rel_list = NULL;
  obj->num_rels = 0;
}


/* Delete a gived object */
static void 
object_delete (object_t* obj)
{
  relation_delete_all (obj);
  utl_free (obj);
}


/* Add a gived object to the hash table */
static void 
object_hash_add (object_t* obj)
{
#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_hex ("Hash add object ", obj->id, 1);
#endif
  unsigned int index = hash (obj->id) % OBJECT_HASH_SIZE;
  obj->next = object_hash[index];
  object_hash[index] = obj;
}


/* Retrieve an object with a given key from the hash table.
   Return NULL if not found. */
static object_t* 
object_hash_get (vt_long_t key)
{
#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_hex ("Get object ", key, 1);
#endif
  unsigned int index = hash (key) % OBJECT_HASH_SIZE;
  object_t* obj = object_hash[index];
  while (obj)
    {
      if (obj->id == key) return obj;
      obj = obj->next;
    }
#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_hex ("Can't get object ", key, 1);
#endif
  return NULL;
}


/* Rmove an object with a given key from the hash table.
   Do nothing if not found. */
static void 
object_hash_remove (vt_long_t key)
{
#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_hex ("Hash remove object ", key, 1);
#endif
  unsigned int index = hash (key) % OBJECT_HASH_SIZE;
  object_t* obj = object_hash[index];
  object_t* prev_obj = NULL;
  while (obj)
    {
      if (obj->id == key)
        {
          if (prev_obj)
            prev_obj->next = obj->next;
          else
            object_hash[index] = obj->next;

          object_delete (obj);
#ifndef NDEBUG
          if (dbg_trace_enabled)
            dbg_print_hex ("Removed object ", key, 1);
#endif
          return;
        }
      prev_obj = obj;
      obj = obj->next;
    }
#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_hex ("Can't find object being removed", key, 1);
#endif
}


/* Event argument iterator helper functions. */


/* Initialize an event argument iterator. */
static void
init_event_arg_itr (event_arg_itr_t* itr, vtEventHeader_t* event)
{
  assert (itr && event);
  itr->event = event;
  itr->cur_arg_num = 0;
  itr->cur_arg_ptr = (char *) event->argt +
    event->argc * sizeof (event->argt[0]);
  if (event->flags & PTHREAD_TRACE_HEAD_NAME) 
    itr->cur_arg_ptr += utl_strlen (itr->cur_arg_ptr) + 1;
  itr->cur_arg_ptr = (char *) Align (itr->cur_arg_ptr, 8);
}


/* Retrieve the next argument of a given type from the argument iterator */
static void*
get_next_event_arg (event_arg_itr_t* itr, pthreadTraceType_t* type)
{
  void* ret;
  pthreadTraceType_t data_type;

  assert (itr);

  /* Reached the end of the list? */
  if (itr->cur_arg_num >= itr->event->argc) 
    {
      if (type) *type = PTHREAD_TRACE_TYPE_UNUSED;
      return NULL;
    }

  data_type = itr->event->argt[itr->cur_arg_num];
  if (type) *type = data_type;
  switch (data_type) 
    {
      case PTHREAD_TRACE_TYPE_CHAR:
      case PTHREAD_TRACE_TYPE_BOOL:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (char));
          itr->cur_arg_ptr += sizeof (char);
          break;

      case PTHREAD_TRACE_TYPE_SHORT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (short));
          itr->cur_arg_ptr += sizeof (short);
          break;

      case PTHREAD_TRACE_TYPE_INT:
      case PTHREAD_TRACE_TYPE_RAD:
      case PTHREAD_TRACE_TYPE_CPU:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (int));
          itr->cur_arg_ptr += sizeof (int);
          break;

      case PTHREAD_TRACE_TYPE_LONG:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (vt_long_t);
          break;

      case PTHREAD_TRACE_TYPE_THREADID: 
      case PTHREAD_TRACE_TYPE_MUTEXID: 
      case PTHREAD_TRACE_TYPE_LOCKID: 
      case PTHREAD_TRACE_TYPE_CONDID: 
      case PTHREAD_TRACE_TYPE_MUTEX:
      case PTHREAD_TRACE_TYPE_COND:
      case PTHREAD_TRACE_TYPE_THREAD:
      case PTHREAD_TRACE_TYPE_KEY:
      case PTHREAD_TRACE_TYPE_LOCK:
      case PTHREAD_TRACE_TYPE_EXCADDR:
      case PTHREAD_TRACE_TYPE_THDARG: 
      case PTHREAD_TRACE_TYPE_VP:
      case PTHREAD_TRACE_TYPE_AID: 
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (vt_long_t);
          break;
	
      case PTHREAD_TRACE_TYPE_POINTER: 
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_addr_t));
          itr->cur_arg_ptr += sizeof (vt_addr_t);
          break;

      case PTHREAD_TRACE_TYPE_ERRNO:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (unsigned int));
          itr->cur_arg_ptr += sizeof (unsigned int);
          break;

      case PTHREAD_TRACE_TYPE_EXCSTAT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (unsigned int));
          itr->cur_arg_ptr += sizeof (unsigned int);
          break;

      case PTHREAD_TRACE_TYPE_STRING: 
      case PTHREAD_TRACE_TYPE_NAME:
      case PTHREAD_TRACE_TYPE_JAVA_STACK:
      case PTHREAD_TRACE_TYPE_LABEL:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (char));
          itr->cur_arg_ptr += utl_strlen ((char *) itr->cur_arg_ptr) + 1;
          break;

      case PTHREAD_TRACE_TYPE_BINARY:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += * (vt_long_t *) itr->cur_arg_ptr;
          itr->cur_arg_ptr += sizeof (vt_long_t);
          break;
          
      case PTHREAD_TRACE_TYPE_EXCNATIVE:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          /* Add in the size of the os-dependant part */
          itr->cur_arg_ptr += 
              ((pthreadTraceNatExc_t *) itr->cur_arg_ptr)->size;
          /* Add in fixed part */
          itr->cur_arg_ptr += offsetof (pthreadTraceNatExc_t,os);
          break;

      case PTHREAD_TRACE_TYPE_TIME:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceTimespec_t);
          break;

      case PTHREAD_TRACE_TYPE_SCHED:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (unsigned int));
          itr->cur_arg_ptr += sizeof (unsigned int);
          itr->cur_arg_ptr += sizeof (unsigned int);
          break;

      case PTHREAD_TRACE_TYPE_MINIT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceMutexInit_t);
          break;

      case PTHREAD_TRACE_TYPE_CINIT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceCondInit_t);
          break;

      case PTHREAD_TRACE_TYPE_TINIT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceThreadInit_t);
          break;

      case PTHREAD_TRACE_TYPE_KINIT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceKeyInit_t);
          break;

      case PTHREAD_TRACE_TYPE_LINIT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceRwlockInit_t);
          break;

      case PTHREAD_TRACE_TYPE_CANCEL:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceCancel_t);
          break;

      case PTHREAD_TRACE_TYPE_OBJECT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (char));
          itr->cur_arg_ptr += sizeof (char);
          break;
          
      case PTHREAD_TRACE_TYPE_OPERATION:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (unsigned short));
          itr->cur_arg_ptr += sizeof (unsigned short);
          break;
          
      case PTHREAD_TRACE_TYPE_BLOCK_REASON:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (unsigned int));
          itr->cur_arg_ptr += sizeof (pthreadTraceBlockReason_t);
          break;

      case PTHREAD_TRACE_TYPE_ENVIRONMENT:
          ret = itr->cur_arg_ptr = 
              (char *) Align (itr->cur_arg_ptr, sizeof (vt_long_t));
          itr->cur_arg_ptr += sizeof (pthreadTraceEnvironment_t);
          break;

      case PTHREAD_TRACE_TYPE_UNUSED:
      case PTHREAD_TRACE_TYPE_MAX:
      default: 
          return NULL;
    }

  itr->cur_arg_num++;
  return ret;
}


/* Retrieve event stack frames from the event header. */
static void**
get_event_stack_frames (vtEventHeader_t* event)
{
  char* ptr = NULL;
  if (event->frames) 
    {
      ptr = ((char*) event) + event->size - event->frames * sizeof (void*);
    }
  return ((void**) ptr);
}


/* Add a relation of the specified type to the given object */
static void 
relation_add (object_t* this_obj, relation_type_t rel_type, 
              vt_long_t rel_obj_id, vtEventHeader_t* event)
{
  void** stack_frames;
  relation_t* rel = 
      (relation_t*) utl_malloc (sizeof (relation_t));
  rel->type = rel_type;
  rel->object_id = rel_obj_id;

  /* Save stack frames associated with this relation */
  rel->frame_count = event->frames;
  rel->frames = NULL;
  stack_frames = get_event_stack_frames (event);
  if (stack_frames)
    {
      rel->frames = utl_malloc (event->frames * sizeof (void*));
      utl_memcpy (rel->frames, stack_frames, event->frames * sizeof (void*));
    }

  /* Link it into the list */
  rel->next = this_obj->rel_list;
  this_obj->rel_list = rel;
  this_obj->num_rels++;
}


/* Retrieve a relation of the specified type from the given object */
static relation_t*
relation_get (object_t* this_obj, relation_type_t rel_type,
              vt_long_t rel_obj_id)
{
  relation_t* rel = this_obj->rel_list;
  relation_t* prev_rel = NULL;
  while (rel)
    {
      if ((rel->type == rel_type && rel_obj_id == -1) ||
          (rel->type == rel_type && rel->object_id == rel_obj_id))
        {
          /* Make sure the object in the relation is still valid */
          if (object_hash_get (rel->object_id))
            {
              return rel;
            }
          else
            {
              /* If the object has been deleted, remove the reation
                 and return NULL */
              if (prev_rel)
                prev_rel->next = rel->next;
              else
                this_obj->rel_list = rel->next;

              relation_delete (rel);
              this_obj->num_rels--;
              return NULL;
            }
        }
      rel = rel->next;
    }
  return NULL;
}


/* Remove a relation of the specified type from the given object */
static void
relation_remove (object_t* this_obj, relation_type_t rel_type,
                 vt_long_t rel_obj_id)
{
  relation_t* rel = this_obj->rel_list;
  relation_t* prev_rel = NULL;
  relation_t* tmp = NULL;

  while (rel)
    {
      if (rel->type == rel_type && rel->object_id == rel_obj_id)
        {
          if (prev_rel)
            prev_rel->next = rel->next;
          else
            this_obj->rel_list = rel->next;

          tmp = rel->next;
          relation_delete (rel);
          rel = tmp;
          this_obj->num_rels--;
          return;
        }
      prev_rel = rel;
      rel = rel->next;
    }
}


/* Check if this thread owns any mutexes. Should be called on thread exit. */
static void
check_thread_exit_own_mutex (object_t* thread_obj)
{
  if (!(thread_obj->flags & OBJF_CREATED))
    return;

  relation_t* rel = thread_obj->rel_list;
  while (rel)
    {
      if (rel->type == REL_PRIMARY_OWNER || rel->type == REL_SECONDARY_OWNER)
        {
          __rtc_thread_event (RTC_THREAD_EXIT_OWN_MUTEX, 
                              (void*) thread_obj->id,
                              (void*) rel->object_id, (void*) 0);
          return;
        }
      rel = rel->next;
    }
}


static void
check_thread_exit_no_join_detach (object_t* thread_obj)
{
  if (!(thread_obj->flags & OBJF_CREATED) ||
      thread_obj->obj.thread.tinit.kind == PTHREAD_TRACE_THD_KIND_INITIAL)
    return;

  boolean is_joined = false;
  relation_t* rel = thread_obj->rel_list;
  while (rel)
    {
      if (rel->type == REL_WAITED_ON_BY)
        {
          is_joined = true;
          break;
        }
      rel = rel->next;
    }

  if (!is_joined && 
      !(thread_obj->obj.thread.tinit.flags & PTHREAD_TRACE_THD_DETACH))
    {
      __rtc_thread_event (RTC_THREAD_EXIT_NO_JOIN_DETACH, 
                          (void*) thread_obj->id, (void*) 0, (void*) 0);
    }
}


/* Check that we don't attempt to synchonize threads that use different
   scheduling policies */
static void
check_mixed_sched_policy (relation_t* rel, object_t* thread)
{
  if (!(thread->flags & OBJF_CREATED))
    return;

  while (rel)
    {
      if (rel->type == REL_OWNED_BY)
        {
          object_t* owner_thread = object_hash_get (rel->object_id);

          if (owner_thread->flags & OBJF_CREATED)
            {
              assert (owner_thread->type == OBJT_THREAD);
              if (owner_thread->obj.thread.tinit.policy !=
                  thread->obj.thread.tinit.policy)
                {
                  __rtc_thread_event (RTC_MIXED_SCHED_POLICY, 
                                      (void*) thread->id,
                                      (void*) owner_thread->id, (void*) 0);
                  return;
                }
            }
        }
      rel = rel->next;
    }
}


/* Initialize fields in the thread object structure */
static void 
thread_init (object_t* thread_obj, vt_long_t thread_handle)
{
  thread_obj->type = OBJT_THREAD;
  thread_obj->id = thread_handle;
  thread_obj->rel_list = NULL;
  thread_obj->num_rels = 0;
  thread_obj->next = NULL;
  thread_obj->flags = 0;
  thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;
  thread_obj->obj.thread.cancel_state = PTHREAD_TRACE_CANCEL_STATE;
  thread_obj->obj.thread.stack_highwater = 0;
  thread_obj->obj.thread.waiter_count = 0;
}


/* In case we receive any event prior to the creation event, create an
   object with default initializations and return it */
static object_t*
thread_pre_create(vt_long_t thread_handle)
{
  object_t* thread_obj = (object_t*) utl_malloc (sizeof (object_t));
  assert (thread_obj);
  thread_init(thread_obj, thread_handle);
  object_hash_add (thread_obj);
  return thread_obj;
}


/* Handle thread exit events */
static void 
thread_exit (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_EXIT);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_hex ("Exit thread ", event->thread, 1);
#endif

  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  if (__rtc_thread_exit_own_mutex)
    check_thread_exit_own_mutex (thread_obj);
}


/* Handle thread kill events */
static void 
thread_kill (vtEventHeader_t* event)
{
#ifndef NDEBUG
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_KILL);

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  if (dbg_trace_enabled)
    dbg_print_hex ("Kill thread ", event->thread, 1);
#endif
}


/* Handle thread destriction events */
static void 
thread_destroy (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_DESTROY);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Destroy thread ", thread_handle, 1);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Remove the object from the hash table */
  object_hash_remove (thread_handle);
}


/* Handle join requests. This routine is called when pthread_join() is
   called, but the actual joining of the 2 threads has not occured */
static void 
thread_join_request (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_JOIN);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Join request for thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  object_t* self_obj = object_hash_get (event->thread);
  assert (self_obj);

  /* Increment number of waiters */
  thread_obj->obj.thread.waiter_count++;

  /* If the number of waiters exceeds the threshold and the rule is enabled */
  if (__rtc_num_waiters > 0 &&
      thread_obj->obj.thread.waiter_count > __rtc_num_waiters)
    {
      // Passing __rtc_num_waiters as the 4th parameter
      // so that gdb gets to know what the value is during batch
      // mode thread check. During batch mode thread check only
      // librtc would have read in the 'rtcconfig' file and set
      // the values.
      __rtc_thread_event (RTC_NUM_WAITERS, (void*) thread_obj->id, 
                          (void*) event->thread, (void*) __rtc_num_waiters);
    }

  /* Note that the causal thread is now wating on this thread */
  relation_add (self_obj, REL_WAITER, thread_obj->id, event);

  /* Add a dependency between the threads */
  relation_add (thread_obj, REL_WAITED_ON_BY, self_obj->id, event);

  /* Set the thread state to blocked */
  self_obj->obj.thread.thread_state = THREAD_STATE_BLOCKED;
}


/* Handle join events. This routine is called to indicate that the two
   threads are joined */
static void 
thread_join_acquire (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_JOIN);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Join aquire for thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj)
    {
      /* Decrement number of waiters */
      thread_obj->obj.thread.waiter_count--;
      assert (thread_obj->obj.thread.waiter_count >= 0);

      /* The threads no longer depend on each other */
      relation_remove (thread_obj, REL_WAITED_ON_BY, event->thread);
    }
  else
    {
      thread_obj = thread_pre_create(thread_handle);
    }

  object_t* self_obj = object_hash_get (event->thread);
  assert (self_obj);

  /* The causal thread is no longer waiting */
  relation_remove (self_obj, REL_WAITER, thread_handle);

  /* Set the thread state to running */
  self_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;
}


/* Handle detach events, pthread_detach() */
static void 
thread_detach (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_DETACH);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Detach thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  thread_obj->obj.thread.tinit.flags |= PTHREAD_TRACE_THD_DETACH;
}


/* Handle events to initialize thread stack info. These events are generated for
   every pthread internal event in __pthread_trace_child_callback() */
static void 
thread_stack_init (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_STACKINIT);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Initialize stack info for thread ", 
                     event->thread, 1);
    }
#endif

  /* Get the thread which initiated this event */
  object_t* self_obj = object_hash_get (event->thread);
  assert (self_obj);
  
  init_event_arg_itr (&arg_itr, event);

  /* Extract stack size from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LONG);
  self_obj->obj.thread.tinit.stack_size = *((vt_long_t*) data_ptr);

  /* Extract stack base from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_POINTER);
  self_obj->obj.thread.tinit.stack_base = *((vt_addr_t*) data_ptr);
}


/* Handle events to update stack ussage. These events are generated for
   every pthread internal event in rtc version of 
   __pthread_trace_internal_event() */
static void 
thread_stack_high_watermark (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_STACKHIGH);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Set stack high water mark for thread ", 
                     event->thread, 1);
    }
#endif

  /* Get the thread which initiated this event */
  object_t* self_obj = object_hash_get (event->thread);
  assert (self_obj);
  
  init_event_arg_itr (&arg_itr, event);

  /* Extract high watermark from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_POINTER);
  self_obj->obj.thread.stack_highwater = *((vt_addr_t*) data_ptr);

  /* Check the stack utilization rule if enabled */
  if (__rtc_stack_utilization > 0 && __rtc_stack_utilization <= 100 &&
      self_obj->flags & OBJF_CREATED)
    {
      int stack_util = 
        abs (self_obj->obj.thread.tinit.stack_base - 
             self_obj->obj.thread.stack_highwater) * 100. / 
        self_obj->obj.thread.tinit.stack_size;

      if (stack_util >= __rtc_stack_utilization)
        {
          // Passing __rtc_stack_utilization as the 3rd parameter
          // so that gdb gets to know what the value is during batch
          // mode thread check. During batch mode thread check only
          // librtc would have read in the 'rtcconfig' file and set
          // the values.
          __rtc_thread_event (RTC_THREAD_STACK_UTILIZATION, 
                              (void*) event->thread,
                              (void*) __rtc_stack_utilization,
                              (void*) 0);
        }
    }
}


/* Handle thread suspend events */
static void 
thread_suspend (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_SUSPEND);

  /* Ignore everything but a successful suspend */
  if ((mod & PTHREAD_TRACE_STAT_FAIL) || (mod & PTHREAD_TRACE_STAT_REQUEST)) 
    return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Suspend thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif
  
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);
  thread_obj->flags |= OBJF_SUSPENDED;
}


/* Handle thread resume events */
static void 
thread_resume (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_RESUME);

  /* Ignore everything but a successful resume */
  if ((mod & PTHREAD_TRACE_STAT_FAIL) || (mod & PTHREAD_TRACE_STAT_REQUEST)) 
    return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Resume thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);
  thread_obj->flags &= ~OBJF_SUSPENDED;
}


/* Handle events to set thread cancelability */
static void 
thread_setcan (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  unsigned int cf = 0;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_SETCAN);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract cancel state/type from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_INT);
  cf = *((int*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Set cancel state for thread ", event->thread, 1);
    }
#endif

  /* Get the thread which initiated this event */
  object_t* self_obj = object_hash_get (event->thread);
  if (!self_obj) return;
  
  if (cf & PTHREAD_TRACE_CANCEL_STATE)
    self_obj->obj.thread.cancel_state |= PTHREAD_TRACE_CANCEL_STATE;
  if (cf & PTHREAD_TRACE_CANCEL_DISABLE)
    self_obj->obj.thread.cancel_state &= ~PTHREAD_TRACE_CANCEL_STATE;
  if (cf & PTHREAD_TRACE_CANCEL_TYPE)
    self_obj->obj.thread.cancel_state |= PTHREAD_TRACE_CANCEL_TYPE;
  if (cf & PTHREAD_TRACE_CANCEL_DEFERRED)
    self_obj->obj.thread.cancel_state &= ~PTHREAD_TRACE_CANCEL_TYPE;
}


/* Handle ready events for threads */
static void 
thread_ready (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_READY);

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Ready for thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);
    
  /* Set the thread state to ready */
  thread_obj->obj.thread.thread_state = THREAD_STATE_READY;
}


/* Handle run events for threads */
static void 
thread_run(vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);
  
  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_RUN);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Run thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  object_t* self_obj = object_hash_get (event->thread);
  assert (self_obj);

  /* When we get a thread.run event and the causal thread is running,
     this is an implied state change for the causal thread out of the
     running state. If the target thread is the same as the causal thread,
     this may just be a change of vps for the thread. The SetState() call
     later on will set the target (causal) thread running again on the 
     new vp. If the target and causal threads are different, the causal 
     thread is moved to ready here. */
  if (self_obj->obj.thread.thread_state == THREAD_STATE_RUNNING)
    self_obj->obj.thread.thread_state = THREAD_STATE_READY;

  thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;
}


/* Handle events to set thread scheduling policy */
static void 
thread_setsched (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_SETSCHED);

  /* If it's a failing call, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Setsched for thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Extract scheduling policy and priority */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  __pthreadLongAddr_t tmp = *((__pthreadLongAddr_t*) data_ptr);
  int* arg = (int*) &tmp;
  thread_obj->obj.thread.tinit.policy = arg[0];
  thread_obj->obj.thread.tinit.priority = arg[1];
}


/* Handle thread start events. These are generated from the child
   callback handler to indicate that the thread is about to start running */
static void 
thread_start (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  pthreadTraceThreadInit_t* tinit;
  vt_long_t thread_handle, parent_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_START);

  /* If it's a failing operation, don't record it */
  if (mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  parent_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Start thread ", thread_handle, 0);
      dbg_print_hex (". Parent thread ", parent_handle, 1);
    }
#endif

  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Add a relantion to indicate where this thread was created.
     This won't work for the initial thread, so don't create a relation */
  if (thread_obj->obj.thread.tinit.kind != PTHREAD_TRACE_THD_KIND_INITIAL)
    {
      object_t* parent_obj = object_hash_get (parent_handle);
      assert (parent_obj);
      relation_t* rel = 
          relation_get (thread_obj, REL_CREATED_BY, parent_handle);
      if (rel == NULL)
        relation_add (thread_obj, REL_CREATED_BY, parent_obj->id, event);
    }

  thread_obj->flags |= OBJF_CREATED;
}


/* Handle thread creation events */
static void 
thread_create (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  pthreadTraceThreadInit_t* tinit;
  vt_long_t thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_THREAD &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_CREATE);

  /* If it's a failing operation, don't record it */
  if(mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Create thread ", thread_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_TINIT);
  tinit = (pthreadTraceThreadInit_t*) data_ptr;
  
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Add a relantion to indicate where this thread was created.
     This won't work for the initial thread, so don't create a relation */
  if (thread_obj->obj.thread.tinit.kind != PTHREAD_TRACE_THD_KIND_INITIAL)
    {
      object_t* parent_obj = object_hash_get (event->thread);
      assert (parent_obj);
      relation_t* rel = 
          relation_get (thread_obj, REL_CREATED_BY, event->thread);
      if (rel == NULL)
        {
          relation_add (thread_obj, REL_CREATED_BY, event->thread, event);
        }
      else
        {
          /* If frame_count is 0, it means that OP_START event finished
             first and added a created-by relation already. Replace it
             with a more informative realtion that provides a stack trace */
          if (rel->frame_count == 0)
            {
              relation_remove (thread_obj, REL_CREATED_BY, event->thread);
              relation_add (thread_obj, REL_CREATED_BY, event->thread, event);
            }
        }
    }

  thread_obj->obj.thread.tinit = *tinit;
  if (thread_obj->obj.thread.tinit.kind == PTHREAD_TRACE_THD_KIND_INITIAL)
    thread_obj->flags |= OBJF_CREATED;
}


/* Initialize fields in the read-write lock object structure */
static void 
rwlock_init (object_t* rwlock_obj, vt_long_t rwlock_handle)
{
  static unsigned int current_idx = 0;
  rwlock_obj->type = OBJT_RWLOCK;
  rwlock_obj->rel_list = NULL;
  rwlock_obj->num_rels = 0;
  rwlock_obj->flags = 0;
  rwlock_obj->obj.rwlock.reader_count = 0;
  rwlock_obj->obj.rwlock.trylock_failures = 0;
  rwlock_obj->obj.rwlock.lock_count = 0;
  rwlock_obj->obj.rwlock.contended_locks = 0;
  rwlock_obj->obj.rwlock.waiter_count = 0;
  rwlock_obj->id = rwlock_handle;
  rwlock_obj->gdb_idx = current_idx++;
}


/* In case we receive any event prior to the creation event, create an
   object with default initializations and return it */
static object_t*
rwlock_pre_create(vt_long_t rwlock_handle)
{
  object_t* rwlock_obj = (object_t*) utl_malloc (sizeof (object_t));
  assert (rwlock_obj);
  rwlock_init(rwlock_obj, rwlock_handle);
  object_hash_add (rwlock_obj);
  return rwlock_obj;
}


/* Handle read-write lock creation events */
static void 
rwlock_create(vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle;
  object_t* rwlock_obj;
  boolean add_object = false;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_CREATE);

  /* If it's a failing operation, don't record it */
  if(mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Create rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* To handle multiple create events on the same object, allocate it
     only when necessary */
  rwlock_obj = object_hash_get (rwlock_handle);
  if (rwlock_obj == NULL)
    {
      rwlock_obj = (object_t*) utl_malloc (sizeof (object_t));
      add_object = true;
      rwlock_obj->next = NULL;
      rwlock_init (rwlock_obj, rwlock_handle);
    }
  else
    {
      /* Delete relation only if object creation has already completed.
         If it hasn't, it's a pre-created object from an event that got 
         processed before we had a chance to complete object creation */
      if (rwlock_obj->flags & OBJF_CREATED)
        {
          relation_delete_all(rwlock_obj);
          rwlock_init (rwlock_obj, rwlock_handle);
        }
      add_object = false;
    }
  
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LINIT);
  rwlock_obj->obj.rwlock.rwlinit = *((pthreadTraceRwlockInit_t*) data_ptr);

  /* Add a relantionship to indicate where this thread was created */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);
  relation_add (rwlock_obj, REL_CREATED_BY, thread_obj->id, event);

  if (add_object)
    object_hash_add (rwlock_obj);

  rwlock_obj->flags |= OBJF_CREATED;
}


/* Handle read-write lock request events. These are generated on entry
   to pthread_rwlock_[rd][wr]lock() before the causal thread acquires it
   or blocks on it */
static void 
rwlock_request (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_LOCK);

  /* If it's a failing request, return  */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract lock handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Lock request for rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Lock object better be there... it should have been created by now */
  object_t* rwlock_obj = object_hash_get (rwlock_handle);
  if (rwlock_obj == NULL)
    rwlock_obj = rwlock_pre_create(rwlock_handle);
  assert (rwlock_obj);

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* Note that the thread is now wating on the lock */
  relation_add (thread_obj, REL_WAITER, rwlock_obj->id, event);

  /* Add a dependency for the lock on the thread */
  relation_add (rwlock_obj, REL_WAITED_ON_BY, thread_obj->id, event);

  /* If already owned by the locking thread it is a recursive use */
  if (relation_get (thread_obj, REL_PRIMARY_OWNER, rwlock_obj->id))
    {
      rwlock_obj->flags |= OBJF_RECURSIVELY_LOCKED;

      /* Check the rule: can't re-lock a non-recursive mutex */
      if (__rtc_non_recursive_relock)
        {
          __rtc_thread_event (RTC_NON_RECURSIVE_RELOCK, (void*) rwlock_obj->id,
                              (void*) thread_obj->id, (void*) 0);
        }
    }

  /* Check if we attempt to synchronize threads which use different
     scheduling policies. */
  if (__rtc_mixed_sched_policy)
    check_mixed_sched_policy (rwlock_obj->rel_list, thread_obj);
}


/* Handle read-write lock acquire events. These are generated when the lock 
   gets actually acquired. */
static void 
rwlock_acquire (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_LOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract rwlock handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Lock acquire for rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* rwlock object better be there... it should have been created by now */
  object_t* rwlock_obj = object_hash_get (rwlock_handle);
  if (rwlock_obj == NULL)
    rwlock_obj = rwlock_pre_create (rwlock_handle);
  assert (rwlock_obj);

  /* Count the number of trylock failures and return */
  if ((mod & PTHREAD_TRACE_STAT_NONBLOCK) &&
     (mod & PTHREAD_TRACE_STAT_FAIL)) 
    {
      rwlock_obj->obj.rwlock.trylock_failures++;
      return;
    }

  /* Count the number of times the lock was acquired */
  rwlock_obj->obj.rwlock.lock_count++;

  /* If it's a failing lock, return  */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* If this thread was blocked on this lock, we can remove it now */
  relation_t* waiter = relation_get (thread_obj, REL_WAITER, rwlock_obj->id);
  if (waiter)
    {
      /* Change the thread state to running */
      thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;

      /* The thread is no longer waiting for the rwlock */
      relation_remove (thread_obj, REL_WAITER, rwlock_obj->id);

      /* The rwlock no longer depends on the thread */
      relation_remove (rwlock_obj, REL_WAITED_ON_BY, thread_obj->id);
    }

  /* The thread now owns the rwlock, add a relationship */ 
  relation_add (rwlock_obj, REL_OWNED_BY, thread_obj->id, event);

  /* If already owned by the locking thread it is a recursive use */
  if (relation_get (thread_obj, REL_PRIMARY_OWNER, rwlock_obj->id))
    rwlock_obj->flags |= OBJF_RECURSIVELY_LOCKED;

  /* Make the thread a primary/seconday owner of the rwlock 
     depenging on whether it's write or read lock */
  relation_type_t rel_type = (mod & PTHREAD_TRACE_STAT_MULTIPLE) ?
      REL_SECONDARY_OWNER : REL_PRIMARY_OWNER;
  relation_add (thread_obj, rel_type, rwlock_obj->id, event);

  /* Increment the number of readers */
  if (mod & PTHREAD_TRACE_STAT_MULTIPLE)
    rwlock_obj->obj.rwlock.reader_count++;
}


/* Handle read-write lock release events. */
static void 
rwlock_release (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_UNLOCK);

  /* If it's a failing unlock, return  */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract rwlock handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Lock release for rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Rwlock object better be there... it should have been created by now */
  object_t* rwlock_obj = object_hash_get (rwlock_handle);
  if (rwlock_obj == NULL)
    rwlock_obj = rwlock_pre_create(rwlock_handle);
  assert (rwlock_obj);

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  relation_t* primary_rel = 
    relation_get (thread_obj, REL_PRIMARY_OWNER, rwlock_obj->id);
  relation_t* secondary_rel = 
    relation_get (thread_obj, REL_SECONDARY_OWNER, rwlock_obj->id);

  /* If the unlocking thread is a seconday owner, it's a read unlock.
     Need to decrement the reader count here */
  if (secondary_rel)
    rwlock_obj->obj.rwlock.reader_count--;

  /* Move the object into the unlocked state */
  relation_remove (rwlock_obj, REL_OWNED_BY, thread_obj->id);

  /* The unlocking thread is either a primary or a secondary owner.
     Remove the relation */
  if (primary_rel)
    relation_remove (thread_obj, REL_PRIMARY_OWNER, rwlock_obj->id);
  else if (secondary_rel)
    relation_remove (thread_obj, REL_SECONDARY_OWNER, rwlock_obj->id);
  else
    {
      /* If the thread is neither primary nor secondary owner, it's an error */
      if (__rtc_unlock_not_own)
        {
          __rtc_thread_event (RTC_UNLOCK_NOT_OWN, (void*) rwlock_obj->id,
                              (void*) thread_obj->id, (void*) 0);
        }
    }
}


/* Handle read-write lock block events. These are generated when the
   causal thread blocks wating for the read-write lock */
static void 
rwlock_block (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle, thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_BLOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract lock handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Block on rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Extract blocking thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  /* Lock object better be there... it should have been created by now */
  object_t* rwlock_obj = object_hash_get (rwlock_handle);
  if (rwlock_obj == NULL)
    rwlock_obj = rwlock_pre_create(rwlock_handle);
  assert (rwlock_obj);

  /* Get the blocking thread object */
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Increment the number of waiters */
  rwlock_obj->obj.rwlock.waiter_count++;

  /* If the number of waiters exceeds the threshold and the rule is enabled */
  if (__rtc_num_waiters > 0 &&
      rwlock_obj->obj.rwlock.waiter_count > __rtc_num_waiters)
    {
      // Passing __rtc_num_waiters as the 4th parameter
      // so that gdb gets to know what the value is during batch
      // mode thread check. During batch mode thread check only
      // librtc would have read in the 'rtcconfig' file and set
      // the values.
      __rtc_thread_event (RTC_NUM_WAITERS, (void*) rwlock_obj->id, 
                          (void*) thread_obj->id, (void*) __rtc_num_waiters);
    }

  /* Increment the number of contended locks */
  rwlock_obj->obj.rwlock.contended_locks++;

  /* Set the thread state to blocked */
  thread_obj->obj.thread.thread_state = THREAD_STATE_BLOCKED;
}


/* Handle read-write lock unblock events. These are generated when the
   causal thread unblocks the read-write lock wait before the lock is
   acquired. */
static void 
rwlock_unblock (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle, thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_BLOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract rwlock handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Unblock from rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Extract blocking thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  /* rwlock object better be there... it should have been created by now */
  object_t* rwlock_obj = object_hash_get (rwlock_handle);
  if (rwlock_obj)
    {
      /* Decrement the number of waiters */
      rwlock_obj->obj.rwlock.waiter_count--;
      assert (rwlock_obj->obj.rwlock.waiter_count >= 0);
    }
  else
    {
      rwlock_obj = rwlock_pre_create(rwlock_handle);
    }
  assert (rwlock_obj);

  /* Get the blocking thread object */
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);
  
  /* Set the thread state to running */
  thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;
}


/* Handle read-write lock destruction events. */
static void 
rwlock_destroy (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t rwlock_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);
  
  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_LOCK &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_DESTROY);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract lock handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_LOCK);
  rwlock_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Destroy rwlock ", rwlock_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Remove the object from the hash table */
  object_hash_remove (rwlock_handle);
}


/* Initialize fields in the mutex object structure */
static void 
mutex_init (object_t* mutex_obj, vt_long_t mutex_handle)
{
  static unsigned int current_idx = 0;
  mutex_obj->type = OBJT_MUTEX;
  mutex_obj->rel_list = NULL;
  mutex_obj->num_rels = 0;
  mutex_obj->flags = 0;
  mutex_obj->obj.mutex.trylock_failures = 0;
  mutex_obj->obj.mutex.lock_count = 0;
  mutex_obj->obj.mutex.contended_locks = 0;
  mutex_obj->obj.mutex.waiter_count = 0;
  mutex_obj->id = mutex_handle;
  mutex_obj->gdb_idx = current_idx++;
}


/* In case we receive any event prior to the creation event, create an
   object with default initializations and return it */
static object_t*
mutex_pre_create(vt_long_t mutex_handle)
{
  object_t* mutex_obj = (object_t*) utl_malloc (sizeof (object_t));
  assert (mutex_obj);
  mutex_init(mutex_obj, mutex_handle);
  object_hash_add (mutex_obj);
  return mutex_obj;
}


/* Handle mutex creation events */
static void 
mutex_create (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle;
  pthreadTraceMutexInit_t* minit;
  object_t* mutex_obj;
  boolean add_object = false;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);
  
  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_CREATE);

  /* If it's a failing operation, don't record it */
  if (mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Create mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* To handle multiple create events on the same object, allocate it
     only when necessary */
  mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj == NULL)
    {
      mutex_obj = (object_t*) utl_malloc (sizeof (object_t));
      add_object = true;
      mutex_obj->next = NULL;
      mutex_init (mutex_obj, mutex_handle);
    }
  else
    {
      /* Delete relation only if object creation has already completed.
         If it hasn't, it's a pre-created object from an event that got 
         processed before we had a chance to complete object creation */
      if (mutex_obj->flags & OBJF_CREATED)
        {
          relation_delete_all(mutex_obj);
          mutex_init (mutex_obj, mutex_handle);
        }
      add_object = false;
    }

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MINIT);
  mutex_obj->obj.mutex.minit = *((pthreadTraceMutexInit_t*) data_ptr);

  /* Add a relantionship to indicate where this thread was created */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);
  relation_add (mutex_obj, REL_CREATED_BY, thread_obj->id, event);

  if (add_object)
    object_hash_add (mutex_obj);

  mutex_obj->flags |= OBJF_CREATED;
}


/* Handle mutex lock request events. These are generated on entry
   to pthread_mutex_lock() before the causal thread acquires it
   or blocks on it */
static void 
mutex_request (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_LOCK);

  /* If it's a failing request, return  */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Lock request for mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Mutex object better be there... it should have been created by now */
  object_t* mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj == NULL)
    mutex_obj = mutex_pre_create(mutex_handle);
  assert (mutex_obj);

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* Note that the thread is now wating on the mutex */
  relation_add (thread_obj, REL_WAITER, mutex_obj->id, event);

  /* Add a dependency for the mutex on the thread */
  relation_add (mutex_obj, REL_WAITED_ON_BY, thread_obj->id, event);

  /* If already owned by the locking thread it is a recursive use */
  if (relation_get (thread_obj, REL_PRIMARY_OWNER, mutex_obj->id))
    {
      mutex_obj->flags |= OBJF_RECURSIVELY_LOCKED;

      /* Check the rule: can't re-lock a non-recursive mutex */
      if (__rtc_non_recursive_relock &&
          mutex_obj->obj.mutex.minit.type != PTHREAD_TRACE_MUT_TYPE_RECURSIVE)
        {
          __rtc_thread_event (RTC_NON_RECURSIVE_RELOCK, (void*) mutex_obj->id,
                              (void*) thread_obj->id, (void*) 0);
        }
    }

  /* Check if we attempt to synchronize threads which use different
     scheduling policies. */
  if (__rtc_mixed_sched_policy)
    check_mixed_sched_policy (mutex_obj->rel_list, thread_obj);
}


/* Handle mutex lock acquire events. These are generated when the lock 
   gets actually acquired. */
static void 
mutex_acquire (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_LOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Lock acquire for mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Mutex object better be there... it should have been created by now */
  object_t* mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj == NULL)
    mutex_obj = mutex_pre_create(mutex_handle);
  assert (mutex_obj);

  /* Count the number of trylock failures and return */
  if ((mod & PTHREAD_TRACE_STAT_NONBLOCK) &&
      (mod & PTHREAD_TRACE_STAT_FAIL)) 
    {
      mutex_obj->obj.mutex.trylock_failures++;
      return;
    }

  /* Count the number of times the lock was acquired */
  mutex_obj->obj.mutex.lock_count++;

  /* If it's a failing lock, return  */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* If this thread was blocked on this lock, we can remove it now */
  relation_t* waiter = relation_get (thread_obj, REL_WAITER, mutex_obj->id);
  if (waiter)
    {
      /* Change the thread state to running */
      thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;

      /* The thread is no longer waiting for the mutex */
      relation_remove (thread_obj, REL_WAITER, mutex_obj->id);

      /* The mutex no longer depends on the thread */
      relation_remove (mutex_obj, REL_WAITED_ON_BY, thread_obj->id);
    }

  /* The thread now owns the mutex, add a relationship */ 
  relation_add (mutex_obj, REL_OWNED_BY, thread_obj->id, event);

  /* If already owned by the locking thread it is a recursive use */
  if (relation_get (thread_obj, REL_PRIMARY_OWNER, mutex_obj->id))
    mutex_obj->flags |= OBJF_RECURSIVELY_LOCKED;

  /* Make the thread a primary owner of the mutex */
  relation_add (thread_obj, REL_PRIMARY_OWNER, mutex_obj->id, event);
}


/* Handle mutex release events. */
static void 
mutex_release (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_UNLOCK);

  /* If it's a failing unlock, return  */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Lock release for mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Mutex object better be there... it should have been created by now */
  object_t* mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj == NULL)
    mutex_obj = mutex_pre_create(mutex_handle);
  assert (mutex_obj);

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* Move the object into the unlocked state */
  relation_remove (mutex_obj, REL_OWNED_BY, thread_obj->id);

  /* If not already owned by the unlocking thread, it's an error */
  if (__rtc_unlock_not_own &&
      !relation_get (thread_obj, REL_PRIMARY_OWNER, mutex_obj->id))
    {
      __rtc_thread_event (RTC_UNLOCK_NOT_OWN, (void*) mutex_obj->id,
                          (void*) thread_obj->id, (void*) 0);
    }

  /* If the unlocking thread is the owner, remove it */
  relation_remove (thread_obj, REL_PRIMARY_OWNER, mutex_obj->id);
}


/* Handle mutex block events. These are generated when the causal thread 
   blocks wating for the mutex */
static void 
mutex_block (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle, thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_BLOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Block on mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Extract blocking thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  /* Mutex object better be there... it should have been created by now */
  object_t* mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj == NULL)
    mutex_obj = mutex_pre_create(mutex_handle);
  assert (mutex_obj);

  /* Get the blocking thread object */
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Increment the number of waiters */
  mutex_obj->obj.mutex.waiter_count++;

  /* If the number of waiters exceeds the threshold and the rule is enabled */
  if (__rtc_num_waiters > 0 &&
      mutex_obj->obj.mutex.waiter_count > __rtc_num_waiters)
    {
      // Passing __rtc_num_waiters as the 4th parameter
      // so that gdb gets to know what the value is during batch
      // mode thread check. During batch mode thread check only
      // librtc would have read in the 'rtcconfig' file and set
      // the values.
      __rtc_thread_event (RTC_NUM_WAITERS, (void*) mutex_obj->id, 
                          (void*) thread_obj->id, (void*) __rtc_num_waiters);
    }

  /* Increment the number of contended locks */
  mutex_obj->obj.mutex.contended_locks++;

  /* Set the thread state to blocked */
  thread_obj->obj.thread.thread_state = THREAD_STATE_BLOCKED;
}


/* Handle mutex unblock events. These are generated when the  causal 
   thread unblocks the mutex wait before the lock is acquired. */
static void 
mutex_unblock(vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle, thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);
  
  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_BLOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Unblock from mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Extract blocking thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  /* Mutex object better be there... it should have been created by now */
  object_t* mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj)
    {
      /* Decrement the number of waiters */
      mutex_obj->obj.mutex.waiter_count--;
      assert (mutex_obj->obj.mutex.waiter_count >= 0);
    }
  else
    {
      mutex_obj = mutex_pre_create(mutex_handle);
    }
  assert (mutex_obj);

  /* Get the blocking thread object */
  object_t* thread_obj = object_hash_get (thread_handle);
  assert (thread_obj);

  /* Set the thread state to running */
  thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;
}


/* Handle mutex destruction events. */
static void 
mutex_destroy (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t mutex_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_MUTEX &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_DESTROY);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Destroy mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Remove the object from the hash table */
  object_hash_remove (mutex_handle);
}


/* Initialize fields in the condvar object structure */
static void 
cond_init (object_t* cond_obj, vt_long_t cond_handle)
{
  static unsigned int current_idx = 0;
  cond_obj->type = OBJT_COND;
  cond_obj->rel_list = NULL;
  cond_obj->num_rels = 0;
  cond_obj->flags = 0;
  cond_obj->obj.cond.mutex_id = -1;
  cond_obj->obj.cond.waiter_count = 0;
  cond_obj->obj.cond.wait_count = 0;
  cond_obj->obj.cond.broadcast_count = 0;
  cond_obj->obj.cond.signal_count = 0;
  cond_obj->id = cond_handle;
  cond_obj->gdb_idx = current_idx++;
}


/* In case we receive any event prior to the creation event, create an
   object with default initializations and return it */
static object_t*
cond_pre_create(vt_long_t cond_handle)
{
  object_t* cond_obj = (object_t*) utl_malloc (sizeof (object_t));
  assert (cond_obj);
  cond_init(cond_obj, cond_handle);
  object_hash_add (cond_obj);
  return cond_obj;
}


/* Handle condition variable creation events */
static void 
cond_create (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle;
  pthreadTraceCondInit_t* cinit;
  object_t* cond_obj;
  boolean add_object = false;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_CREATE);

  /* If it's a failing operation, don't record it */
  if(mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Create condvar ", cond_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* To handle multiple create events on the same object, allocate it
     only when necessary */
  cond_obj = object_hash_get (cond_handle);
  if (cond_obj == NULL)
    {
      cond_obj = (object_t*) utl_malloc (sizeof (object_t));
      add_object = true;
      cond_obj->next = NULL;
      cond_init (cond_obj, cond_handle);
    }
  else
    {
      /* Delete relation only if object creation has already completed.
         If it hasn't, it's a pre-created object from an event that got 
         processed before we had a chance to complete object creation */
      if (cond_obj->flags & OBJF_CREATED)
        {
          relation_delete_all (cond_obj);
          cond_init (cond_obj, cond_handle);
        }
      add_object = false;
    }

  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_CINIT);
  cond_obj->obj.cond.cinit = *((pthreadTraceCondInit_t*) data_ptr);

  /* Add a relantionship to indicate where this thread was created */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);
  relation_add (cond_obj, REL_CREATED_BY, thread_obj->id, event);

  if (add_object)
    object_hash_add (cond_obj);

  cond_obj->flags |= OBJF_CREATED;
}


/* Handle condition variable wait  request events. These are generated on 
   entry to pthread_cond_wait() before the causal thread acquires it
   or blocks on it */
static void 
cond_request (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle, mutex_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_WAIT);

  /* If it's a failing operation, don't record it */
  if(mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract condvar handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

  /* Extract mutex handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_MUTEX);
  mutex_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Wait request for condvar ", cond_handle, 0);
      dbg_print_hex (", mutex ", mutex_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Condvar object better be there... it should have been created by now */
  object_t* cond_obj = object_hash_get (cond_handle);
  if (cond_obj == NULL)
    cond_obj = cond_pre_create(cond_handle);
  assert (cond_obj);

  /* Mutex object better be there... it should have been created by now */
  object_t* mutex_obj = object_hash_get (mutex_handle);
  if (mutex_obj == NULL)
    mutex_obj = mutex_pre_create(mutex_handle);
  assert (mutex_obj);

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* Check that this condvar is associated only with one mutex */
  if (__rtc_condvar_multiple_mutexes)
    {
      if (cond_obj->obj.cond.mutex_id != -1 &&
          cond_obj->obj.cond.mutex_id != mutex_handle)
        {
          __rtc_thread_event (RTC_CONDVAR_MULTIPLE_MUTEXES, 
                              (void*) cond_obj->id, 
                              (void*) cond_obj->obj.cond.mutex_id, 
                              (void*) mutex_handle);
        }
    }

  /* Save the associated mutex id */
  cond_obj->obj.cond.mutex_id = mutex_handle;
  cond_obj->obj.cond.wait_count++;

  /* This thread better be the owner of the mutex associated with the condvar.
     Otherwise it's an error */
  if (__rtc_condvar_wait_no_mutex)
    {
      if (!relation_get (thread_obj, REL_PRIMARY_OWNER, mutex_obj->id))
        {
          __rtc_thread_event (RTC_CONDVAR_WAIT_NO_MUTEX, 
                              (void*) thread_obj->id, 
                              (void*) cond_obj->id, 
                              (void*) mutex_obj->id);
        }
    }

  /* Condition wait can generate multiple events for the same thread,
     but all we need is to have one */
  if (relation_get (thread_obj, REL_WAITER, cond_obj->id) == NULL)
    {
      /* Note that the thread is now wating on the condvar */
      relation_add (thread_obj, REL_WAITER, cond_obj->id, event);
    }

  if (relation_get (cond_obj, REL_WAITED_ON_BY, thread_obj->id) == NULL)
    {
      /* Add a dependency for the condvar on the thread */
      relation_add (cond_obj, REL_WAITED_ON_BY, thread_obj->id, event);
    }
}


/* Handle condition variable acquire events. These are generated when the 
   condition variable wait is over. */
static void 
cond_acquire (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle, mutex_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_WAIT);

  /* If it's a failing operation, don't record it */
  if(mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract condvar handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Wait acquire for condvar ", cond_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Condvar object better be there... it should have been created by now */
  object_t* cond_obj = object_hash_get (cond_handle);
  if (cond_obj == NULL)
    cond_obj = cond_pre_create(cond_handle);
  assert (cond_obj);

  /* Get the thread which initiated this event */
  object_t* thread_obj = object_hash_get (event->thread);
  assert (thread_obj);

  /* If this thread was blocked on this condvar, we can remove it now */
  relation_t* waiter = relation_get (thread_obj, REL_WAITER, cond_obj->id);
  if (waiter)
    {
      /* Change the thread state to running */
      thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;

      /* The thread is no longer waiting for the condvar */
      relation_remove (thread_obj, REL_WAITER, cond_obj->id);

      /* The condvar no longer depends on the thread */
      relation_remove (cond_obj, REL_WAITED_ON_BY, thread_obj->id);
    }
}


/* Handle condition variable release events. */
static void 
cond_release (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_SIGNAL);

  /* If it's a failing operation, don't record it */
  if(mod & PTHREAD_TRACE_STAT_FAIL) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract condvar handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Signal for condvar ", cond_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Condvar object better be there... it should have been created by now */
  object_t* cond_obj = object_hash_get (cond_handle);
  if (cond_obj == NULL)
    cond_obj = cond_pre_create(cond_handle);
  assert (cond_obj);
  
  if(mod & PTHREAD_TRACE_STAT_MULTIPLE)
    cond_obj->obj.cond.broadcast_count++;
  else
    cond_obj->obj.cond.signal_count++;
}


/* Handle condition variable block events. */
static void 
cond_block (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle, thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_BLOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract condvar handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Block for condvar ", cond_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Extract blocking thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  /* Condvar object better be there... it should have been created by now */
  object_t* cond_obj = object_hash_get (cond_handle);
  if (cond_obj == NULL)
    cond_obj = cond_pre_create(cond_handle);
  assert (cond_obj);

  /* Get the blocking thread object */
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Increment the number of waiters */
  cond_obj->obj.cond.waiter_count++;

  /* If the number of waiters exceeds the threshold and the rule is enabled */
  if (__rtc_num_waiters > 0 &&
      cond_obj->obj.cond.waiter_count > __rtc_num_waiters)
    {
      // Passing __rtc_num_waiters as the 4th parameter
      // so that gdb gets to know what the value is during batch
      // mode thread check. During batch mode thread check only
      // librtc would have read in the 'rtcconfig' file and set
      // the values.
      __rtc_thread_event (RTC_NUM_WAITERS, (void*) cond_obj->id, 
                          (void*) thread_obj->id, (void*) __rtc_num_waiters);
    }

  /* Set the thread state to blocked */
  thread_obj->obj.thread.thread_state = THREAD_STATE_BLOCKED;
}


/* Handle condition variable unblock events. */
static void 
cond_unblock (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle, thread_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_BLOCK);

  init_event_arg_itr (&arg_itr, event);

  /* Extract condvar handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Unblock for condvar ", cond_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Extract blocking thread handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_THREAD);
  thread_handle = *((vt_long_t*) data_ptr);

  /* Condvar object better be there... it should have been created by now */
  object_t* cond_obj = object_hash_get (cond_handle);
  if (cond_obj)
    {
      /* Decreament the number of waiters */
      cond_obj->obj.cond.waiter_count--;
      assert (cond_obj->obj.cond.waiter_count >= 0);
    }
  else
    {
      cond_obj = cond_pre_create(cond_handle);
    }
  assert (cond_obj);

  /* Get the blocking thread object */
  object_t* thread_obj = object_hash_get (thread_handle);
  if (thread_obj == NULL)
    thread_obj = thread_pre_create(thread_handle);
  assert (thread_obj);

  /* Set the thread state to blocked */
  thread_obj->obj.thread.thread_state = THREAD_STATE_RUNNING;
}


/* Handle condition variable destruction events. */
static void 
cond_destroy (vtEventHeader_t* event)
{
  event_arg_itr_t arg_itr;
  pthreadTraceType_t data_type;
  void* data_ptr;
  vt_long_t cond_handle;
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  assert (PTHREAD_TRACE_OBJ(event->event) == PTHREAD_TRACE_OBJ_COND &&
          PTHREAD_TRACE_OP(event->event) == PTHREAD_TRACE_OP_DESTROY);

  /* If it's a failing destroy, return */
  if ((mod & PTHREAD_TRACE_STAT_FAIL)) return;

  init_event_arg_itr (&arg_itr, event);

  /* Extract condvar handle from the arguments */
  data_ptr = get_next_event_arg (&arg_itr, &data_type);
  assert (data_ptr && data_type == PTHREAD_TRACE_TYPE_COND);
  cond_handle = *((vt_long_t*) data_ptr);

#ifndef NDEBUG
  if (dbg_trace_enabled)
    {
      dbg_print_hex ("Destroy condvar ", cond_handle, 0);
      dbg_print_hex (". Caller thread ", event->thread, 1);
    }
#endif

  /* Remove the object from the hash table */
  object_hash_remove (cond_handle);
}


/* Handle program termination event. */
static void 
program_terminate (vtEventHeader_t* event)
{
  /* Here we need to check if there are any threads left that have neither
     been joined nor detached. By the time we are ready to terminate the
     only undetached thread left should be the main thread. */
  if (__rtc_thread_exit_no_join_detach)
    {
      for (int i = 0; i < OBJECT_HASH_SIZE; i++)
        {
          object_t* obj = object_hash[i];
          while (obj)
            {
              if (obj->type == OBJT_THREAD)
                check_thread_exit_no_join_detach (obj);

              obj = obj->next;
            }
        }
    }
}


/* Top-level routine for event processing */
void 
process_trace_event (vtEventHeader_t* event)
{
  pthreadTraceObject_t obj_type = PTHREAD_TRACE_OBJ(event->event);
  pthreadTraceOperation_t op_type = PTHREAD_TRACE_OP(event->event);
  unsigned int mod = PTHREAD_TRACE_MOD(event->event);

  __pthread_trace_spinlock_lock (&hash_lock);
  __rtc_critical_thread = event->thread;
  switch (obj_type)
    {
      case PTHREAD_TRACE_OBJ_COND:
          switch (op_type)
            {
              case PTHREAD_TRACE_OP_CREATE:
                  cond_create (event);
                  break;
                  
              case PTHREAD_TRACE_OP_SIGNAL:
                  cond_release (event);
                  break;
                  
              case PTHREAD_TRACE_OP_BLOCK:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    cond_block (event);
                  else
                    cond_unblock (event);
                  break;
                  
              case PTHREAD_TRACE_OP_WAIT:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    cond_request (event);
                  else
                    cond_acquire (event);
                  break;
                  
              case PTHREAD_TRACE_OP_DESTROY:
                  cond_destroy (event);
                  break;
                  
              default:
                  break;
            }
          break;
          
      case PTHREAD_TRACE_OBJ_LOCK:
          switch (op_type)
            {
              case PTHREAD_TRACE_OP_CREATE:
                  rwlock_create (event);
                  break;
                  
              case PTHREAD_TRACE_OP_LOCK:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    rwlock_request (event);
                  else
                    rwlock_acquire(event);
                  break;
                  
              case PTHREAD_TRACE_OP_UNLOCK:
                  rwlock_release (event);
                  break;
                  
              case PTHREAD_TRACE_OP_BLOCK:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    rwlock_block (event);
                  else
                    rwlock_unblock (event);
                  break;
                  
              case PTHREAD_TRACE_OP_DESTROY:
                  rwlock_destroy (event);
                  break;
                  
              default:
                  break;
            }
          break;
          
      case PTHREAD_TRACE_OBJ_MUTEX:
          switch (op_type)
            {
              case PTHREAD_TRACE_OP_CREATE:
                  mutex_create (event);
                  break;
                  
              case PTHREAD_TRACE_OP_LOCK:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    mutex_request (event);
                  else
                    mutex_acquire (event);
                  break;
                  
              case PTHREAD_TRACE_OP_UNLOCK:
                  mutex_release (event);
                  break;
                  
              case PTHREAD_TRACE_OP_BLOCK:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    mutex_block (event);
                  else
                    mutex_unblock (event);
                  break;
                  
              case PTHREAD_TRACE_OP_DESTROY:
                  mutex_destroy (event);
                  break;
                  
              default:
                  break;
            }
          break;
          
      case PTHREAD_TRACE_OBJ_THREAD:
          switch (op_type)
            {
              case PTHREAD_TRACE_OP_CREATE:
                  thread_create (event);
                  break;
                  
              case PTHREAD_TRACE_OP_START:
                  thread_start (event);
                  break;
                  
              case PTHREAD_TRACE_OP_DESTROY:
                  thread_destroy (event);
                  break;
                  
              case PTHREAD_TRACE_OP_JOIN:
                  if (mod & PTHREAD_TRACE_STAT_REQUEST)
                    thread_join_request (event);
                  else
                    thread_join_acquire (event);
                  break;
                  
              case PTHREAD_TRACE_OP_DETACH:
                  thread_detach (event);
                  break;
                  
              case PTHREAD_TRACE_OP_SUSPEND:
                  thread_suspend (event);
                  break;
                  
              case PTHREAD_TRACE_OP_RESUME:
                  thread_resume (event);
                  break;
                  
              case PTHREAD_TRACE_OP_SETCAN:
                  thread_setcan (event);
                  break;
                  
              case PTHREAD_TRACE_OP_READY:
                  thread_ready (event);
                  break;
                  
              case PTHREAD_TRACE_OP_RUN:
                  thread_run (event);
                  break;
              case PTHREAD_TRACE_OP_SETSCHED:
                  thread_setsched (event);
                  break;
                  
              case PTHREAD_TRACE_OP_STACKHIGH:
                  thread_stack_high_watermark (event);
                  break;
                  
              case PTHREAD_TRACE_OP_STACKINIT:
                  thread_stack_init (event);
                  break;
                  
              case PTHREAD_TRACE_OP_EXIT:
                  thread_exit (event);
                  break;
                  
              case PTHREAD_TRACE_OP_KILL:
                  thread_kill (event);
                  break;
                  
              case PTHREAD_TRACE_OP_CANCEL:
                __rtc_critical_thread = -1;
                  break;

              default:
                  break;
            }
          break;

      case PTHREAD_TRACE_OBJ_EVENT:
          switch (op_type)
            {
              case PTHREAD_TRACE_OP_DESTROY:
                  program_terminate (event);
                  break;
              default:
                  break;
            }

      default:
          break;
    }
  __rtc_critical_thread = -1;
  __pthread_trace_spinlock_unlock (&hash_lock);
}
