#ifndef _INF_THREAD_TRACE_H_
# define _INF_THREAD_TRACE_H_

#ifdef __cplusplus
    extern "C" {
#endif


#include "global_defs.h"
#include "thread.h"


void process_trace_event (vtEventHeader_t* event);
void init_pthread_intercepts (void);

/* Event argument iterator */
typedef struct event_arg_itr_tag {
    vtEventHeader_t* event; /* event whose args we iterate */
    int cur_arg_num; /* current argument index */
    char* cur_arg_ptr; /* current argument pointer */
} event_arg_itr_t;

/* Thread object */
typedef struct thread_object_tag {
    pthreadTraceThreadInit_t tinit; /* thread init from trace_event.h */
    vt_addr_t stack_highwater; /* stack high watermark */
    int waiter_count; /* current waiter count */
    enum THREAD_values thread_state; /* thread state, from global_defs.h */
    unsigned int cancel_state; /* thread cancel state */
} thread_obj_t;

/* Mutex object */
typedef struct mutex_object_tag {
    pthreadTraceMutexInit_t minit;  /* mutex init from trace_event.h */
    int trylock_failures; /* cumulative number of trylock failures */
    int lock_count; /* cumulative number of lock requests */
    int contended_locks; /* cumulative number of contended locks */
    int waiter_count; /* current waiter count */
} mutex_obj_t;

typedef struct rwlock_object_tag {
    pthreadTraceRwlockInit_t rwlinit; /* rwlock init from trace_event.h */
    int reader_count; /* current reader count */
    int trylock_failures; /* cumulative number of trylock failures */
    int lock_count; /* cumulative number of lock requests */
    int contended_locks; /* cumulative number of contended locks */
    int waiter_count; /* current waiter count */
} rwlock_obj_t;

typedef struct cond_object_tag {
    pthreadTraceCondInit_t cinit; /* condvar init from trace_event.h */
    vt_long_t mutex_id; /* corresponding mutex id */
    int waiter_count; /* current waiter count */
    int wait_count; /* cumulative number of wait requests */
    int broadcast_count; /* cumulative number of broadcasts */
    int signal_count; /* cumulative number of signals */
} cond_obj_t;

typedef struct object_tag object_t;

/* Relation */
typedef struct relation_tag {
    struct relation_tag* next;
    relation_type_t type; /* relation type */
    vt_long_t object_id; /* object id */
    void** frames; /* stack frames associated with this relation */
    unsigned int frame_count; /* number of stack frames */
} relation_t;

/* Generic pthread object */
struct object_tag {
    struct object_tag* next;
    object_type_t type; /* object type */
    object_flags_t flags; /* object flags */
    vt_long_t id; /* object id */
    int gdb_idx; /* user visible index in gdb */

    union {
        thread_obj_t thread;
        mutex_obj_t mutex;
        rwlock_obj_t rwlock;
        cond_obj_t cond;
    } obj;

    unsigned int num_rels; /* number of relations */
    relation_t* rel_list;  /* list od relations */
};

#ifdef __cplusplus
    }
#endif

#endif /* _INF_THREAD_TRACE_H_ */
