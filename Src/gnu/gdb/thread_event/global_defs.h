#ifndef _GLOBAL_DEFS_H_
#define _GLOBAL_DEFS_H_

#include <inttypes.h>
#include <stdio.h>
#include "trace_event.h"

#define MAX_CALL_FRAMES 128 /* Max number of call frames per event */

/* SUPPORT MACROS */
#define Align(x,size) ((((unsigned long)x) + (size-1)) & ~(size-1))

extern int trc_event_fini (pthreadTraceFiniCode_t);
extern void trc_event_init (void);
extern void __init_event_vt (void);
extern void __fini_event_vt (void);

enum THREAD_values {
	THREAD_STATE_UNKNOWN = 0x0,
	THREAD_STATE_RUNNING = 0x1,
	THREAD_STATE_BLOCKED = 0x2,
	THREAD_STATE_READY = 0x3,
	THREAD_STATE_NEW = 0x4,
	THREAD_STATE_TERMINATED = 0x5,
	THREAD_STATE_DELETED = 0x6,
	THREAD_M_STATE_ONLY = 0x7,
	THREAD_STATE_MAX = 0x7,
	THREAD_M_STATE_DEADLOCKED = 0x10,
	THREAD_M_STATE_LOCK_BLOCKED = 0x20,
	THREAD_M_STATE_SUSPENDED = 0x40,
	THREAD_M_STATE_HELD = 0x80,
	THREAD_MAX 
};

#ifdef __cplusplus
typedef bool vt_bool_t;
#else
typedef char vt_bool_t;
#endif

typedef	int64_t vt_long_t;
typedef	int32_t vt_int_t;
typedef	uint32_t vt_uint_t;
typedef	int16_t vt_word_t;
typedef	int8_t vt_byte_t;
typedef	uint8_t vt_ubyte_t;
typedef	uint64_t vt_ulong_t;
typedef	uint64_t vt_addr_t;

typedef struct vtEventMinHeader_t {
  /* Fixed fields: */
  pthreadTraceEvent_t event; /* Event class id from trace_event.h */
  unsigned int size;         /* Size of this trace packet in bytes */
} vtEventMinHeader_t;

typedef pthreadTraceLogHead_t vtEventHeader_t;

/* Event representing a buffer header */
typedef struct vtEventEHdHeader_t { 
  /* Fixed fields: */ 
  pthreadTraceEvent_t event;  /* Event class identifier 'EHd'  */
  unsigned int size;   /* Size of this trace packet in bytes */
  /* 'EHd' specific fields: */ 
  vt_long_t sequence;   /* Sequence number for this 8k buffer */
  vt_long_t thread;	/* Thread that filled this buffer */
  int buffer_size; /* Size of the buffer represented by this header */
} vtEventEHdHeader_t;


/* Set of programmatic actions that an event system can perform
   when processing an EAk event. */
typedef enum { 
  vtActionNone = 0,  // Just continue 
  vtActionReport  = 1,    // Generate a debugger event report
  vtActionAbort = 2,    // Force a core dump 
  vtActionDisableSchedulingEvents = 3, // Turn off scheduling events 
  vtActionEnableSchedulingEvents = 4  // Turn on scheduling events 
  /* additional actions may be defined as needed */ 
} vtEventAction_t; 

/* Event format used when acknowledging an event from a tool */
typedef struct vtEventEAkHeader_t {
  /* Fixed fields: */ 
  pthreadTraceEvent_t event;         /* Event class identifier 'EAk' */ 
  unsigned int        size;          /* Size of trace packet pad in bytes */ 
  /* 'EAk' specific fields: */ 
  vt_long_t         thread;	       /* Ids thread which requires synch */ 
  vtEventAction_t   action;          /* Ids action to be taken */ 

  /* New support to assist in debugging Ack support */
  unsigned int cycles; /* cycles field of the timestamp on originating event */
} vtEventEAkHeader_t; 

/* Event format used when acknowledging an event from a tool */
typedef struct vtEventEAkReport_t {
  /* common header */
  vtEventEAkHeader_t	hdr; 
  /* report ack specific data */
  char s1[1];	/* event name */
  char s2[1];	/* optional debugger command */
} vtEventEAkReport_t; 

#endif /* _GLOBAL_DEFS_H_ */
