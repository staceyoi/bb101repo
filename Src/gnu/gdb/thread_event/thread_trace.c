/* Pthread event system to enable logging of events for Visual Threads tool
   The routines in this module are called from very restricted locations in 
   the DECthreads library.  Care must be taken not to call out to anything 
   that might potentially call into DECthreads (such as libc, libcxx, etc).
   Although we are calling some simply string manipulation routines as well 
   as getenv (which searches the environ),  there is some potential risk in 
   doing so, if the implementation of those routines are ever changed.  
   Thus the safest rule is that this module should be restricted to call only
   system calls */

#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <dl.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <sys/param.h>
#include <unistd.h>
#ifdef __ia64
#include <uwx.h> /* Unwind express */
#include <uwx_self.h> /* For self unwinding */
#endif

#include "trace_thread.h"
#include "trace_internal.h"
#include "trace_util.h"
#include "analysis.h"

#define TRC_TYPE_CNT 20 /* Max number of event arguments */

#ifdef __ia64
#define VA_ARG_CHAR int
#define VA_ARG_SHORT int
#else
#define VA_ARG_CHAR char
#define VA_ARG_SHORT short
#endif

/* For systems with 32-bit pointers
   POINTER64 -- clears the upper half
   SWIZZLE64 -- propagates the value to the upper byte using Sizzle function */
#if defined(_ILP32)
#define POINTER64(x) ((unsigned long)(x))&0xffffffffUL
#define SWIZZLE64(x) _UNW_swizzlePtr(x)
#else
#define POINTER64(x) x
#define SWIZZLE64(x) x
#endif

/* Use object address (as no sequence number is available) */
#define GETSEQ(x) ((vt_ulong_t)x)

#if !defined(GDB_TARGET_IS_HPPA_20W) && !defined (HP_IA64)
extern int __pthread_cps_get_stacksize (void);
extern int _pthread_callback_np (int, 
                                 void (*fp)(pthread_t, pthread_t, 
                                            const _pthread_stack_info_t*));
#endif

extern int get_frame_pc_list (void ** pc_list, int frame_count);
extern int get_frame_pc_list_thr (void ** pc_list, int frame_count);
extern int __rtc_frame_count;

#ifdef __ia64
extern int (*uwx_set_nofr_ptr) (struct uwx_env *env);
extern int (*uwx_self_init_state_cache_ptr) (struct uwx_self_info *info,
                                             int nrows, int assoc, int entsz);
#endif

/* Return pointer to the pthread_trace_private_data field of the 
   specified thread. */
static int __pthread_trace_private_offset;
static struct pthread* (*__pthread_trace_getself)(void);
static struct trc_thread_data *get_thread_trc_data (struct pthread*);
static int trc_event_write (pthreadTraceEvent_t event, 
                            char *name, 
                            pthread_t thread,
                            int count,
                            va_list arglist);
void __pthread_trace_child_callback (pthread_t parent, 
                                     pthread_t child,
                                     const _pthread_stack_info_t *stack_info);

#define TRACE_SELF_DATA(self) \
         ((pthread_trace_private_data*)((char*)(self) + \
                                        __pthread_trace_private_offset))

#define TRACE_THREAD_DISABLE(self) \
         TRACE_SELF_DATA(self)->p_trace_enabled = 0
#define TRACE_THREAD_ENABLE(self) \
         TRACE_SELF_DATA(self)->p_trace_enabled = 1
#define THREAD_TRACE_TURNED_ON(self) \
         (TRACE_SELF_DATA(self)->p_trace_enabled)

/* The static initializer only works as an initializer for a static object,
   so make one that we can copy to initialize the field in TLS later at 
   runtime. */
static __pthread_trace_spinlock_t __pthread_trace_spinlock_initial = 
       __PTHREAD_SPINLOCK_INIT;

void (*__pthread_trace_spinlock_lock)(__pthread_trace_spinlock_t*) = NULL;
void (*__pthread_trace_spinlock_unlock)(__pthread_trace_spinlock_t*) = NULL;

typedef struct trc_dump_rec_t {
    __pthreadLongAddr_t	data;	/* Pointer/value */
    int		size;		/* Variable size info */
    int		flags;		/* Flags */
    pthreadTraceType_t   type;	/* Argument Type */
} trc_dump_rec_t;


/* The following is taken from thd_defs.h to make sure we get a
   full 64-bit timespec, which is what the Event system is
   (probably) expecting. */
typedef struct utlTime_tag 
{
  __pthreadLongInt_t  tv_sec;
  __pthreadLongUint_t tv_nsec;
} utlTime_t, *utlTime_p;


/* Structure of per-thread data for use by VT tracing system. */
typedef struct trc_thread_data 
{
  // High water usage of stack (note stacks grow toward lower addresses)
  void*   stack_highwater;
  __pthread_trace_spinlock_t	  tracelock;
  
  // Params saved between START/END events for reporting of thread creation
  pthreadTraceThreadInit_t tinit;
  // Parameters saved between START/END events for reporting mutex creation
  pthreadTraceMutexInit_t minit;
  // Parameters saved for reporting condition variable creation
  pthreadTraceCondInit_t	cinit;
  // Parameters saved for reporting rwlock creation
  pthreadTraceRwlockInit_t linit;
  
#ifdef HP_LIBUWX
  // Used by our callback routines for temporary buffers
  struct uwx_self_info *unwind_self_info;
#endif
} trc_thread_data;


/* Initial portion of a pthread_d to enable us to access the pthread_t value.
   taken from the Pthread -- internal.h */
typedef struct _pthread_queue 
{
  struct _pthread_queue *pq_next;
  struct _pthread_queue *pq_prev;
} pthread_queue;


typedef struct pthread 
{
  union 
  {
    struct 
    {
      /* Note: p_link MUST be first as queue mgmt function assume this. */
      pthread_queue p_link;  /* current q                  */
      pthread_t     p_id;    /* pthread identifier         */
    } internals;
  } data;
} pthread_d;

/* Events are not enabled until __pthread_trace_startup() */
int thread_trace_enabled = 0;
int dbg_trace_enabled = 0;

/* Libc and libpthread data start and end */
unsigned long libc_data_start = 0, libc_data_end = 0;
unsigned long pthread_data_start = 0, pthread_data_end = 0;
extern int __rtc_ignore_sys_objects;

/* From analysis.c */
extern char thread_info_filename[];

#if 0
/* Get an acuurate (down to a nanosecond) timestamp */
static int 
trace_gettimestamp (pthread_t thread,
                   pthreadTraceTimestamp_p ts) 
{
  struct timespec  t;
  if (clock_gettime(CLOCK_REALTIME, &t) != 0) 
    {
      trc_internal_error_abort ("clock_gettimed() failed =", errno);
    }
  ts->toffset = 0;
  ts->cycles = 0;
  ts->time.tv_sec = t.tv_sec;
  ts->time.tv_nsec = t.tv_nsec;
  return 0;
}
#endif


/* Routine to generate events using the standard event routine */
void 
trc_internal_event (int ident, int argc,...) 
{
  pthreadTraceEvent_t event;
  va_list ap;	

  event = ident;
  va_start(ap,argc);
  trc_event_write (event, 0, pthread_self (), argc, ap);
  va_end(ap);
}

static void
set_system_objects_data_addr ()
{
  /* Locate libc and libpthread and record their data segment start and
     finish. This is needed to identify system objects during event 
     processing */
  int index = 1;
  struct shl_descriptor *desc;

  while (shl_get (index, &desc) != -1)
    {
      index++;
      if (utl_strstr (desc->filename, "/libpthread."))
        {
          pthread_data_start = desc->dstart;
          pthread_data_end = desc->dend;
        }
      else if (utl_strstr (desc->filename, "/libc."))
        {
          libc_data_start = desc->dstart;
          libc_data_end = desc->dend;
        }
    }

#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_dec (
      "Set the start and end data addresses of libc and libpthread",
       getpid(), 1);
#endif

}

/* Initialize the event library automatically upon activation, by
   translating the value of PTHREAD_CONFIG to get tracing parameters. */
void
trc_event_init () 
{
  char *env_data;			/* config pointer */
  int st;	/* Status from uname */
  shl_t handle = NULL;
  int (*pthread_callback_ptr)(int, 
                              void (*fp)(pthread_t, pthread_t, 
                                         const _pthread_stack_info_t*)) = NULL;

  /* Set up the file name for the temp data file */
  sprintf (thread_info_filename, "/tmp/__thread_info.%u", getpid ());

  /* Lookup _pthread_callback_np instead of making a direct call
     to avoid creating a dependency on libpthread */
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
  if ((shl_findsym (&handle, "_pthread_callback_np", TYPE_PROCEDURE,
                    &pthread_callback_ptr)) == 0)
    {
      st = pthread_callback_ptr (PTHREAD_CHILD_NP, 
                                 &__pthread_trace_child_callback);
      if (st != 0) 
        dbg_print ("Warning! _pthread_callback_np failed\n");
    }
  else
    {
      dbg_print ("Warning! _pthread_callback_np lookup failed\n");
    }
#else
  /* For some reason symbol lookup fails on PA32. Just use a direct
     reference. This is less desireable because it creates a dependency
     on libpthread which is not necessary if a user is doing just leaks
     detection and doesn't care about threads */
  st = _pthread_callback_np (PTHREAD_CHILD_NP, 
                             &__pthread_trace_child_callback);
  if (st != 0) 
    dbg_print ("Warning! _pthread_callback_np failed\n");
#endif

#ifdef __ia64
  /* Lookup uwx_set_nofr and uwx_self_init_state_cache_ptr. Can't call it 
     directly in get_frame_pc_list() because older versions if unwind 
     library don't have it. Ignore error return from shl_findsym, it'll be 
     handled when uwx_set_nofr_ptr or uwx_self_init_state_cache_ptr is
     used in hp-ia64-rtc.c. */
  if (uwx_set_nofr_ptr == NULL)
    shl_findsym (&handle, "uwx_set_nofr", TYPE_PROCEDURE, &uwx_set_nofr_ptr);
  if (uwx_self_init_state_cache_ptr == NULL)
    shl_findsym (&handle, "uwx_self_init_state_cache", TYPE_PROCEDURE, 
                 &uwx_self_init_state_cache_ptr);
#endif

  thread_trace_enabled = 0;
  env_data = getenv ("PTHR_TRACE_ENABLED");
  if (env_data != NULL) 
    {
      if ((strcmp (env_data, "off") == 0) ||
          (strcmp (env_data, "OFF") == 0) ||
          (strcmp (env_data, "0") == 0)) 
        {
          thread_trace_enabled = 0;
        }
      else if ((strcmp (env_data, "on") == 0) ||
               (strcmp (env_data, "ON") == 0) ||
               (strcmp (env_data, "1") == 0)) 
        {
          thread_trace_enabled = 1;
        }
    }

#ifndef NDEBUG
  env_data = getenv ("PTHR_DBG_TRACE_ENABLED");
  if (env_data != NULL) 
    {
      if ((strcmp (env_data, "off") == 0) ||
          (strcmp (env_data, "OFF") == 0) ||
          (strcmp (env_data, "0") == 0)) 
        {
          dbg_trace_enabled = 0;
        }
      else if ((strcmp (env_data, "on") == 0) ||
               (strcmp (env_data, "ON") == 0) ||
               (strcmp (env_data, "1") == 0)) 
        {
          dbg_trace_enabled = 1;
        }
    }
#endif

  if (!thread_trace_enabled) 
    return;

  set_system_objects_data_addr ();

#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_dec ("Initialized thread tracing for pid ", getpid (), 1);
#endif
}


int 
trc_event_fini (pthreadTraceFiniCode_t reason) 
{
  if (thread_trace_enabled) 
    {
      trc_internal_event (PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_EVENT,
                                              PTHREAD_TRACE_OP_DESTROY, 0), 0);
      thread_trace_enabled = 0;
    }
  return 0;
}


#pragma OPTIMIZE OFF
/* This function is called after the crtitical function, process_trace_event,
   which updates the internal state if the tracing library and is used by 
   gdb to continue the critical thread until it gets out of the critical
   section */
void
__rtc_pthread_dummy ()
{
}
#pragma OPTIMIZE ON


/* Loader +init function. */
void 
__init_event_vt () 
{
  /* Here we need to call every external function used in 
     __rtc_pthread_info() to make sure all symbol are bound. If some
     symbols are not bound when __rtc_pthread_info() is called from gdb
     we may see a deadlock in BOR if any thread is stopped in BOR and
     is holding a BOR mutex in dld */
  int fd;
  char tmp[5];
  fd = creat ("/dev/null", O_WRONLY);
  lseek (fd, 0, SEEK_SET);
  write (fd, &fd, 1);
  close (fd);
  sprintf (tmp, "a%d", fd);
  __rtc_pthread_dummy ();

#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_dec ("Initializer for thread tracing for pid ", getpid(), 1);
#endif
}


/* Finalize use of the event library (called automatically during
   process rundown). */
void 
__fini_event_vt () 
{
  /* Call the event system fini routine to emulate pthreads */
  trc_event_fini (PTHREAD_TRACE_FINI_DESIST);
#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_dec ("Terminated thread tracing for pid ", getpid (), 1);
#endif
}


/* Format an event and write it to the current output stream.
   event -- identifier describing the event
   name -- optional text description of event
   thread -- The thread causing the event
   count -- number of arguments
   arglist -- Type/Value pairs providing arguments */
static int 
trc_event_write (pthreadTraceEvent_t ident, char *name, pthread_t thread, 
                 int argc, va_list ap)
{
  unsigned int size = 0;
  unsigned int hdrsize = 0;
  unsigned int namesize = 0;
  char* dataBuf = NULL;
  vtEventHeader_t* traceHdr = NULL;
  trc_dump_rec_t traceData[TRC_TYPE_CNT];
  trc_dump_rec_t* tD = NULL;
  trc_dump_rec_t* tDend = NULL;
  pthreadTraceTimestamp_t timeOfCall = {0};
  int i, st, frames_found, requested_frames;
  pthreadTraceEventUnion_t evt_id;
  void* frames[MAX_CALL_FRAMES];
  vt_long_t threadid;
  long long trace_mgs_buffer[128];
  
  assert (argc <= TRC_TYPE_CNT);

  /* Get time early, closer to actual event. For now, disable time stamping */
#if 0
  trace_gettimestamp (thread, &timeOfCall);
#endif
  evt_id.mask = ident;

  /* Get threadid, so we don't have to call this function multiple times */
  threadid = GETSEQ (thread);

  /* Loop over arguments calculating the size */
  tD = traceData;
  size = 0;
  for (i = 0; i < argc; i++) 
    {
      pthreadTraceType_t t = va_arg (ap,pthreadTraceType_t);
      tD->type = t;
      tD->flags = 0;
      
      switch (t) 
        {
          case PTHREAD_TRACE_TYPE_CHAR:	/* char (%c) */
          case PTHREAD_TRACE_TYPE_BOOL:	/* char	TRUE, FALSE */
            tD->data = (__pthreadLongAddr_t) va_arg (ap, VA_ARG_CHAR);
            tD->size = sizeof (char);
            size += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_SHORT:
            size = Align (size,sizeof (short));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, VA_ARG_SHORT);
            tD->size = sizeof (short);
            size += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_INT:	/* "int" (%d) */
          case PTHREAD_TRACE_TYPE_RAD:	/* "int" (%d) */
          case PTHREAD_TRACE_TYPE_CPU:	/* "int" (%d) */
          case PTHREAD_TRACE_TYPE_ERRNO:	/* Status (error number: %d) */
          case PTHREAD_TRACE_TYPE_OBJECT:	/* trace object code */
          case PTHREAD_TRACE_TYPE_OPERATION:/* trace operation code */
          case PTHREAD_TRACE_TYPE_CANCEL:	/* pthreadTraceCancel_t */
            assert (sizeof (int) == sizeof (pthreadTraceCancel_t));
            size = Align (size, sizeof (int));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, int);
            tD->size = sizeof (int);
            size += tD->size;
            break;
			
          case PTHREAD_TRACE_TYPE_POINTER:	/* "void*" (%p) */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (vt_long_t);
            size += tD->size;
            break;
		
          case PTHREAD_TRACE_TYPE_THREADID:/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_MUTEXID:	/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_CONDID:	/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_LOCKID:	/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_LONG:	/* "long" (%lx) */
          case PTHREAD_TRACE_TYPE_EXCADDR:	/* Exception address (%p) */
          case PTHREAD_TRACE_TYPE_EXCSTAT:	/* Exception status (%lx) */
          case PTHREAD_TRACE_TYPE_THDARG:	/* Thread argument (%p) */
          case PTHREAD_TRACE_TYPE_KEY:	/* pthread_key_t */
            /* Analysis for Instrumentation Dependant seq. # */
          case PTHREAD_TRACE_TYPE_AID:
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, unsigned long);
            tD->size = sizeof (vt_long_t);
            size += tD->size;
            break;
			
          case PTHREAD_TRACE_TYPE_STRING:	/* Null-term string (%s) */
          case PTHREAD_TRACE_TYPE_NAME:	/* Object name (%s) */
          case PTHREAD_TRACE_TYPE_JAVA_STACK:/* Null-term string (%s) */
          case PTHREAD_TRACE_TYPE_LABEL:	/* Null-term string (%s) */
            tD->data = (__pthreadLongAddr_t) va_arg (ap, char*);
            /* Hack to avoid crashes on a null */
            if (tD->data == 0) tD->data = (__pthreadLongAddr_t)"";
            tD->size = utl_strlen ((char*)tD->data) + 1;
            size += tD->size;
            break;
			
          case PTHREAD_TRACE_TYPE_BINARY:	/* Binary data (long %d: %s) */
            /* Align for long to contain size */
            size = Align (size, sizeof(vt_long_t));
            /* For size field in rep */
            size += sizeof (vt_long_t);
            /* Actual data size */
            tD->size = va_arg (ap, vt_long_t);
            /* Pointer to the data */
            tD->data = (__pthreadLongAddr_t) va_arg (ap, char*);
            size += tD->size;
            break;
			
          case PTHREAD_TRACE_TYPE_THREAD:	/* pthread_t */
          case PTHREAD_TRACE_TYPE_LOCK:	/* pthread_rwlock_t* */
          case PTHREAD_TRACE_TYPE_VP:	/* (vp*) long    vp %ld */
            size = Align (size, sizeof(vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (vt_long_t);
            size += tD->size;
            break;

            /* These two object types may Filtered */
          case PTHREAD_TRACE_TYPE_MUTEX:	/* pthread_mutex_t* */
          case PTHREAD_TRACE_TYPE_COND:	/* pthread_cond_t* */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);

            /* If the first argument is a pthread object in libc or libpthread,
               then ignore the event */
            if (__rtc_ignore_sys_objects && i == 0 && 
                /* Not in libc data area */
                ((tD->data >= libc_data_start && 
                  tD->data < libc_data_end) ||
                 /* Not in libpthread data area */
                 (tD->data >= pthread_data_start && 
                  tD->data < pthread_data_end)))
              {
                return 0;
              }

            tD->size = sizeof (vt_long_t);
            size += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_TIME:	/* struct timespec* */
            size = Align (size, sizeof (vt_long_t)); // Align for struct
            tD->data = (__pthreadLongAddr_t)
              va_arg (ap, pthreadTraceTimespec_t*); // Actual data 
            // For size field in rep
            tD->size = sizeof (pthreadTraceTimespec_t);
            size += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_SCHED:	/* Policy: Priority (%d: %d) */
            size = Align (size, sizeof (int));  // Rep is int, int
            tD->data = (__pthreadLongAddr_t)
              (((vt_ulong_t)(va_arg (ap, int)) << 32) | 
               // Combine values into data field
               (vt_ulong_t) (va_arg (ap, int)));
            tD->size = sizeof (int) + sizeof (int); // Rep size
            size +=tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_MINIT:	/* pthreadTraceMutexInit_t* */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceMutexInit_t);
            size += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_CINIT:	/* pthreadTraceCondInit_t* */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceCondInit_t);
            size += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_TINIT:	/* pthreadTraceThreadInit_t* */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceThreadInit_t);
            size += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_KINIT:	/* pthreadTraceKeyInit_t* */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceKeyInit_t);
            size += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_LINIT:	/* pthreadTraceRwlockInit_t* */
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceRwlockInit_t);
            size += tD->size;
            break;
            /* pthreadTraceBlockReason_t */	
          case PTHREAD_TRACE_TYPE_BLOCK_REASON:
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceBlockReason_t);
            size += tD->size;
            break;
            /* phreadTraceEnvirontment_t */
          case PTHREAD_TRACE_TYPE_ENVIRONMENT:
            size = Align (size, sizeof (vt_long_t));
            tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
            tD->size = sizeof (pthreadTraceEnvironment_t);
            size += tD->size;
            break;
            /* OS native exception */
          case PTHREAD_TRACE_TYPE_EXCNATIVE: 
            {
              size_t                  exrecSize;
              pthreadTraceNatExc_p    neProto;
            
              size = Align (size, sizeof (vt_long_t));
              tD->data = (__pthreadLongAddr_t) va_arg (ap, void*);
              /* Compute the space needed to store the native exception.
                 No extra data on this platform */
              exrecSize = 0;
              /* Add space for the generic wrapper object. */
              exrecSize += sizeof (pthreadTraceNatExc_t)  /* Fixed part */
                - sizeof (neProto->os);     /* The built-in .os[0] */
              
              tD->size = exrecSize;
              size += tD->size;
              break;
            }
          default:
            trc_internal_error_abort (
                           "Unexpected parameter type code value in " __FILE__ 
                           ", tD->type=", tD->type);
            break;
        }
		
      /* Next argument to fill in */
      tD++;
    }
	
  /* Collect current callstack. Only save stack for interesting events */
  frames_found = 0;
  if (evt_id.fields.operation == PTHREAD_TRACE_OP_CREATE ||
      evt_id.fields.operation == PTHREAD_TRACE_OP_INIT ||
      evt_id.fields.operation == PTHREAD_TRACE_OP_JOIN ||
      evt_id.fields.operation == PTHREAD_TRACE_OP_LOCK ||
      evt_id.fields.operation == PTHREAD_TRACE_OP_WAIT ||
      evt_id.fields.operation == PTHREAD_TRACE_OP_BLOCK)
    {
      if (__rtc_frame_count > 0)
        {
#ifdef __ia64
          frames_found = get_frame_pc_list_thr (frames, __rtc_frame_count + 3);
#else
          frames_found = get_frame_pc_list (frames, __rtc_frame_count + 3);
#endif
          frames_found = frames_found > 3 ? frames_found - 3 : 0;
        }
    }

  tDend = tD;	/* Indicator for end of formated arguments */
  va_end (ap);
    
  /* Determine the size of the event header */
  hdrsize = offsetof (vtEventHeader_t, argt); /* Fixed size header */
  /* Number of type bytes beyond those always in the header */
  hdrsize += argc * sizeof (traceHdr->argt);
  
  /* TBS -- We may decide to only include this for User events as the name
     is likely pthreads internal data for primitive events. If an explicit 
     name is specified, include it is header size calculation */
  if (name) 
    {
      namesize = utl_strlen (name) + 1;
      hdrsize += namesize;
    }
 
  /* Align prior to arguments */
  hdrsize = Align (hdrsize, 8);	
    
  /* Add in the call stack data area size */
  size = Align (size,8);
  size += frames_found * sizeof (void*);
  
  /* Set traceHdr to point to a local buffer */
  traceHdr = (vtEventHeader_t*) &trace_mgs_buffer;
  
  /* Init event header fields (except size & ident which are overloaded 
     to show state of this event) */
  traceHdr->timestamp = timeOfCall;
  traceHdr->thread = threadid;
  
  traceHdr->frames = frames_found;
  traceHdr->argc = argc;
  traceHdr->flags = 0;
  if (frames_found < __rtc_frame_count) 
    traceHdr->flags = PTHREAD_TRACE_HEAD_MOREFRAMES;

  /* If name is present, copy it into the header */
  if (name) 
    {
      /* Set flag to indicate assocated name */
      traceHdr->flags |= PTHREAD_TRACE_HEAD_NAME;
      
      /* Copy the name information into the event header */
      {	
        /* Name follows alignment after arguments */
        char *p = (char*) &(traceHdr->argt) + argc;
        utl_memcpy ((char *) p, name, namesize);
      }
    }

  // Data area starts immediately after hdrsize bytes
  dataBuf = (char *) traceHdr + hdrsize;
  dataBuf = (char *) Align (dataBuf, 8);

  // Reset loop to 
  tD = traceData;
  i = 0;
  
  while(tD != tDend) 
    {
      traceHdr->argt[i++] = tD->type;
      switch (tD->type) 
        {
          case PTHREAD_TRACE_TYPE_CHAR:	/* "char" (%c) */
          case PTHREAD_TRACE_TYPE_BOOL:	/* char	TRUE, FALSE */
            *dataBuf = (char) tD->data;
            dataBuf++;
            break;
          case PTHREAD_TRACE_TYPE_SHORT:
            dataBuf = (char*) Align (dataBuf, sizeof (short));
            *(short*) dataBuf = (short) tD->data;
            dataBuf += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_INT:	/* "int" (%d) */
          case PTHREAD_TRACE_TYPE_RAD:	/* "int" (%d) */
          case PTHREAD_TRACE_TYPE_CPU:	/* "unsiged int" (%d) */
          case PTHREAD_TRACE_TYPE_ERRNO:	/* Status (error number: %d) */
          case PTHREAD_TRACE_TYPE_OBJECT:	/* trace object code */
          case PTHREAD_TRACE_TYPE_OPERATION:/* trace operation code */
          case PTHREAD_TRACE_TYPE_CANCEL:	/* pthreadTraceCancel_t */
            dataBuf = (char*) Align (dataBuf, sizeof (int));
            *(int*) dataBuf = (int) tD->data;
            dataBuf += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_MUTEXID:	/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_CONDID:	/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_LOCKID:	/* DECthreads obj ID (%lx) */
          case PTHREAD_TRACE_TYPE_THREADID:/* DECthreads obj ID (%lx) */
            {
              dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
              *(vt_long_t*) dataBuf = (vt_long_t) tD->data;
              dataBuf += tD->size;
            }
          break;
          case PTHREAD_TRACE_TYPE_LONG:	/* "long" (%lx) */
          case PTHREAD_TRACE_TYPE_POINTER:	/* "void*" (%p) */
          case PTHREAD_TRACE_TYPE_EXCADDR:	/* Exception address (%p) */
          case PTHREAD_TRACE_TYPE_EXCSTAT:	/* Exception status (%lx) */
          case PTHREAD_TRACE_TYPE_THDARG:	/* Thread argument (%p) */
          case PTHREAD_TRACE_TYPE_KEY:	/* pthread_key_t */
            /* Analysis for instrumentation dependant seq. # */
          case PTHREAD_TRACE_TYPE_AID:
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            *(vt_long_t*) dataBuf = (vt_long_t) tD->data;
            dataBuf += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_STRING:	/* Null-term string (%s) */
          case PTHREAD_TRACE_TYPE_NAME:	/* Object name (%s) */
          case PTHREAD_TRACE_TYPE_JAVA_STACK:/* Null-term string (%s) */
          case PTHREAD_TRACE_TYPE_LABEL:	/* Null-term string (%s) */
            utl_memcpy ((char*) dataBuf, (char*) tD->data, tD->size);
            dataBuf += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_BINARY:	/* Binary data (long %d: %s) */
            // Align for size field
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            // Copy size field
            *(vt_long_t*) dataBuf = (vt_long_t) tD->size;
            // Update marshalling pointer
            dataBuf += sizeof (vt_long_t);
            // Copy data
            utl_memcpy ((char*) dataBuf, (char*) tD->data, tD->size);
            // Point to next arg
            dataBuf += tD->size;
            break;
                
          case PTHREAD_TRACE_TYPE_MUTEX:	/* pthread_mutex_t* */
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            *(vt_long_t*) dataBuf = GETSEQ ((pthread_mutex_t*) tD->data);
            dataBuf += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_COND:	/* pthread_cond_t* */
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            *(vt_long_t*) dataBuf = GETSEQ ((pthread_cond_t*) tD->data);
            dataBuf += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_THREAD:	/* pthread_t */
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            /* TBS: For HPUX-32 a pthread_t* is only 32 bits, but we 
               copied this to the 64-bits tD->data.  So we need to 
               reference it as 64-bits instead of a 32-bit pthread_t* 
               as this looses the low-order 32-bits usually leaving a 
               zero. */
            *(vt_long_t*) dataBuf = tD->data;
            dataBuf += tD->size;
            break;
            /* (vp*) long    vp %ld -- Header of a VP is same as a TCB */
          case PTHREAD_TRACE_TYPE_VP:
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            *(vt_long_t*) dataBuf = GETSEQ (*((pthread_t*) &tD->data));
            dataBuf += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_LOCK:	/* pthread_rwlock_t* */
            dataBuf = (char*) Align (dataBuf, sizeof(vt_long_t));
            *(vt_long_t*) dataBuf = GETSEQ ((pthread_rwlock_t*) tD->data);
            dataBuf += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_TIME:	/* struct timespec* */
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));	
            // Copy data
            utl_memcpy ((char*) dataBuf, (char*) tD->data, tD->size);
            dataBuf += tD->size;
            break;
            
          case PTHREAD_TRACE_TYPE_SCHED:	/* Policy: Priority (%d: %d) */
            dataBuf = (char*) Align (dataBuf, sizeof (int));	
            *(unsigned int*) dataBuf = ((vt_ulong_t) tD->data >> 32);
            dataBuf += sizeof (int);
            *(unsigned int*) dataBuf = 
              ((vt_ulong_t) tD->data & 0xffffffff);
            dataBuf += sizeof (int);
            break;
            
          case PTHREAD_TRACE_TYPE_MINIT:	/* pthreadTraceMutexInit_t* */
          case PTHREAD_TRACE_TYPE_CINIT:	/* pthreadTraceCondInit_t* */
          case PTHREAD_TRACE_TYPE_TINIT:	/* pthreadTraceThreadInit_t* */
          case PTHREAD_TRACE_TYPE_KINIT:	/* pthreadTraceKeyInit_t* */
          case PTHREAD_TRACE_TYPE_LINIT:	/* pthreadTraceRwlockInit_t* */
            /* pthreadTraceBlockReason_t */
          case PTHREAD_TRACE_TYPE_BLOCK_REASON:
            /* phreadTraceEnvirontment_t */
          case PTHREAD_TRACE_TYPE_ENVIRONMENT:
            // Use maximal alignment
            dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
            // Copy data
            utl_memcpy ((char*) dataBuf, (void*) tD->data, tD->size);
            dataBuf += tD->size;
            break;
          case PTHREAD_TRACE_TYPE_EXCNATIVE:    /* OS native exception */
            {	
              pthreadTraceNatExc_p ne;
              // Use maximal alignment
              dataBuf = (char*) Align (dataBuf, sizeof (vt_long_t));
              ne = (pthreadTraceNatExc_p) dataBuf;
              
              // The size of the os part is everything but the header
              ne->size = tD->size - offsetof (pthreadTraceNatExc_t, os);
              utl_memcpy ((char*) ne->os, (void*) tD->data, ne->size);
              break;
            }		
          default:
            trc_internal_error_abort (
                          "Unexpected parameter type code value in " __FILE__ 
                          ", tD->type=", tD->type);
        }
		
      tD++;
    }

  // Append callstack PCs
  utl_memcpy ((char *)((unsigned long) traceHdr + hdrsize + size - 
                      (traceHdr->frames * sizeof (void*))),
             &frames[3],
             frames_found * sizeof (void*));
  
  traceHdr->event = ident;
  traceHdr->size = hdrsize + size;
  
  // Process the trace event
  process_trace_event (traceHdr);
  
  /* Call a dummy function where debugger can set a breakpoint
     to stop threads which were in the critical section */
  __rtc_pthread_dummy ();
  
  return 0;
}


/* Get the VT thread-private data area for the specified thread. */
static struct trc_thread_data* 
get_thread_trc_data (struct pthread *t) 
{
  if (t == 0) return 0;
  return (trc_thread_data*)
    Align ((&(TRACE_SELF_DATA (t)->p_trace_type_specific_data)), 8);
}


/* Check the stack use of the current thread and generate a new high water 
   mark if necessary.  This function should only be called when self matches 
   the current thread (sp). The highwater is updated in page increments to 
   reduce the number of events generated. */
void 
trc_update_stack_use (pthread_d *self) 
{
  trc_thread_data *myData = get_thread_trc_data (self);
  void *sp = &myData;
    
  if (myData == 0) return;

  if (myData->stack_highwater == 0 || 
#ifdef HP_IA64
      sp < myData->stack_highwater
#else
      sp > myData->stack_highwater
#endif
      )
    {
      long pgsz = getpagesize ();

#ifdef HP_IA64
      myData->stack_highwater = (void*) (((long) sp) & ~(pgsz - 1));
#else
      myData->stack_highwater = (void*) (((long) sp + pgsz - 1) & ~(pgsz - 1));
#endif

      trc_internal_event (PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
                                                PTHREAD_TRACE_OP_STACKHIGH, 0),
                          1,
                          PTHREAD_TRACE_TYPE_POINTER, myData->stack_highwater);
    }
}


/* Macro for a generic event using a single parameter of a particular 
   object type */
#define TRC_EVENT_1_ARG(objtype,op,stat,a1,status) {\
		trc_internal_event(\
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_##objtype,op,stat),\
                                  (status == 0)?1:2,\
                                  PTHREAD_TRACE_TYPE_##objtype,a1,\
                                  PTHREAD_TRACE_TYPE_ERRNO,status);\
}

#define TRC_EVENT_2_ARG(objtype,op,stat,a1,p2type,p2,status) {\
		trc_internal_event(\
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_##objtype,op,stat),\
			(status == 0)?2:3,\
			PTHREAD_TRACE_TYPE_##objtype,a1,\
			PTHREAD_TRACE_TYPE_##p2type,p2,\
			PTHREAD_TRACE_TYPE_ERRNO,status);\
}

/* Convert a C function pointer addres (e.g. &foo()) which is really the 
   address of a procedure descriptor, to the PC of the actual code. See 
   the Runtime Architecture documents on http://cllweb.cup.hp.com/runtime/ 
   for details. */
#if defined(__hppa)
#ifdef _ILP32
/* For pa11_32 images: Procedure label */
#define PC_FROM_FP(fp) ((fp==0) ? 0 : *(unsigned long*)((unsigned long)fp-2))
#else
/* For pa20_64 images: PC is 16 bytes from the beginning of the FP */
#define PC_FROM_FP(fp) ((fp==0) ? 0 : *(void **)((char *)fp+16))
#endif
#else
/* For IA64 */
#define PC_FROM_FP(fp) ((fp==0) ? 0 : *(vt_ulong_t*)(fp))
#endif

/* big fat Spinlock to control access to thread RTC */
static __pthread_trace_spinlock_t big_fat_thread_rtc_lock = __PTHREAD_SPINLOCK_INIT;


/* Convert a HP threads trace event, into the equivalent VT event. */
void
__pthread_trace_internal_event (struct pthread *self,
                                pthread_trace_cat_t cat,
                                pthread_trace_event_t event,
                                pthread_trace_type_t type,
                                void* caller, void* retval, 
                                void* a1, void* a2, void* a3, 
                                void* a4, char * a5) 
{
  int st; 
  int stat = 0;
  int op;
  int status = 0;
  int enable = 0;
	
  /* Defer tracing of HP-internal events until after VT has generated 
     it's internal init event.  Those use trc_internal_event directly and 
     therefore will come through */
  if (!thread_trace_enabled) return;

  // Don't trace pthread_self(), just return
  if (event == SELF) return;

 /* Suresh : QXCR1000758169; June 2008 :
    Lets acquire the big fat lock to avoid deadlocks while GDB resumes 
    just the critical thread. This avoids deadlocks due to different 
    locking order between threads that acquire the various thread RTC locks 
    ie, freelist_lock, hash_lock & __pthread_trace_spinlock_initial(this
    looks like a dummy lock and unused currently!)
  
    Now this big fat lock is acquired right at the start of the first
    routine that the pthread tracer library calls(this routine), so that 
    we are guaranteed to get any other RTC locks subsequently(when GDB 
    resumes just the critical thread) 

    FIXME 1: Not the best possible design as this new lock is coarse in 
    granularity, resulting in reduced parallelism inside thread RTC (and 
    also makes the other RTC locks redundent.) May be we should correct the
    locking order somehow between hash_lock & freelist_lock which were the
    ones deadlocking frequently.

    FIXME 2: There are other paths(like __rtc_pthread_join) which are 
    wrappers for actual pthread calls and directly called by application 
    by passing this rotinue. Need to study all these direct pthread 
    wrappers to see if there is any other potential for deadlocks when we 
    resume just 1 thread(the critical thread) OR go for 
    a design where we resume all threads in GDB(in thread-check.c)
  */
  __pthread_trace_spinlock_lock (&big_fat_thread_rtc_lock);

  enable = THREAD_TRACE_TURNED_ON (self);
  
  // Prevent recursion of tracing
  TRACE_THREAD_DISABLE(self);
  
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
  /* If this is batch mode thread check and if we have come here for
     the first time, the values of pthread_data_[start|end] and
     libc_data_[start|end] would not have been set. Set them.
   */
  if (   (0 == pthread_data_start) && (0 == pthread_data_end)
      && (0 == libc_data_start) && (0 == libc_data_end))
    {
      set_system_objects_data_addr ();
    }
#endif


  // Update stack use for this thread
  if (event != BLOCK && event != USCHED_RUN && event != USCHED_RUNNABLE) 
    trc_update_stack_use (self);
  
  if (type == TRACE_START) 
    stat = stat | PTHREAD_TRACE_STAT_REQUEST;
    
  // check the return value, but only on all-in-one operations, 
  // or the ending operation
  if (type == TRACE_END || type == TRACE_SINGLE) 
    {
      status = (long) retval;
      if (status != 0)
        stat = stat | PTHREAD_TRACE_STAT_FAIL;
    }

  switch (event) 
    {
        // MUTEX Events
      case MUTEX_SETPRIO:
      case MUTEX_GETPRIO:
        // TBS 
        break;
      case MUTEX_LOCK:
        TRC_EVENT_1_ARG (MUTEX, PTHREAD_TRACE_OP_LOCK, stat, a1, status);
        break;
      case MUTEX_UNLOCK:
        TRC_EVENT_1_ARG (MUTEX, PTHREAD_TRACE_OP_UNLOCK, stat, a1, status);
        break;
      case MUTEX_TRYLOCK:
        stat = stat | PTHREAD_TRACE_STAT_NONBLOCK;
        TRC_EVENT_1_ARG (MUTEX, PTHREAD_TRACE_OP_LOCK, stat, a1, status);
        break;
      case MUTEX_DESTROY:
        TRC_EVENT_1_ARG (MUTEX, PTHREAD_TRACE_OP_DESTROY, stat, a1, status);
        break;
      case MUTEX_BLOCK:
        /* TBS: do we do anything with the mtx_waiting parameter that's 
           also here? Also note, we are assuming that it is THIS 
           thread blocking on the mutex */
        TRC_EVENT_2_ARG (MUTEX, PTHREAD_TRACE_OP_BLOCK, stat, a1, 
                         THREAD, self->data.internals.p_id, status);
        break;
      case MUTEX_INIT:
        {
          trc_thread_data *pData = get_thread_trc_data (self);
          pthreadTraceMutexInit_t *minit = &pData->minit;
          
          switch (type) 
            {
              case TRACE_SINGLE:
                minit->address = 
                  (pthread_trace_uint64_t) POINTER64 (a1);
                op = PTHREAD_TRACE_OP_CREATE;
                trc_internal_event(
                                PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_MUTEX,
                                                      op, stat),
				(status == 0) ? 2:3, // argument count
				PTHREAD_TRACE_TYPE_MUTEX, a1,
				PTHREAD_TRACE_TYPE_MINIT, minit,
				PTHREAD_TRACE_TYPE_ERRNO, status);
                break;

              case TRACE_ATTR_START:
                utl_memclr (minit, sizeof (*minit));
                if ((long) a1 == PTHREAD_PROCESS_SHARED)
                  minit->flags |= PTHREAD_TRACE_MUT_PSHARED;
                // Figure out the type of the mutex
                switch ((long)a2) 
                  {
                    case PTHREAD_MUTEX_RECURSIVE:
                      minit->type = PTHREAD_TRACE_MUT_TYPE_RECURSIVE;
                      break;
                    case PTHREAD_MUTEX_ERRORCHECK:
                      minit->type = PTHREAD_TRACE_MUT_TYPE_ERRORCHECK;
                      break;
                    case PTHREAD_MUTEX_NORMAL:
                    case PTHREAD_MUTEX_DEFAULT:
                      minit->type = PTHREAD_TRACE_MUT_TYPE_NORMAL;
                      break;
                    default:
                      break;
                  }

                // Remember the mutex protocol value
                if ((long) a3 == PTHREAD_PRIO_NONE)
                  minit->protocol = PTHREAD_TRACE_MUT_PROTO_NONE;
                if ((long) a3 == PTHREAD_PRIO_PROTECT)
                  minit->protocol = PTHREAD_TRACE_MUT_PROTO_PROTECT;
                if ((long) a3 == PTHREAD_PRIO_INHERIT)
                  minit->protocol = PTHREAD_TRACE_MUT_PROTO_INHERIT;
                break;

              case TRACE_ATTR_END:
                minit->priority = (long) a1;
                /* TBS - there seems also to be a "spin" parameter 
                   available in a2, but we don't have a place to put 
                   this in the minit block... */
                break;
              default:
                break;
            } // end switch on mutex event type
          break; // end of MUTEX_INIT case
        }
            // RWLOCK Events
      case RWLOCK_INIT:
        {
          trc_thread_data *lData = get_thread_trc_data (self);
          pthreadTraceRwlockInit_t *linit = &lData->linit;
            
          if (type == TRACE_ATTR_SINGLE) 
            {
              // The first initialization event we get is an attribute event
              utl_memclr (linit, sizeof (*linit));
              if ((long) a1 == PTHREAD_PROCESS_SHARED)
                linit->flags |= PTHREAD_TRACE_MUT_PSHARED;
            } 
          else if (type == TRACE_SINGLE) 
            {
              // When the initialization is complete, we get a 
              // TRACE_SINGLE event
              linit->address = (pthread_trace_uint64_t) POINTER64(a1);
              linit->attributes = (pthread_trace_uint64_t) POINTER64(a2);
                
              op = PTHREAD_TRACE_OP_CREATE;
              trc_internal_event (
                              PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_LOCK,
                                                    op, stat),
                              (status == 0) ? 2:3, // argument count
                              PTHREAD_TRACE_TYPE_LOCK, a1,
                              PTHREAD_TRACE_TYPE_LINIT, linit,
                              PTHREAD_TRACE_TYPE_ERRNO, status);
            } 
          break;
        }
      case RWLOCK_RDLOCK:
        stat = stat | PTHREAD_TRACE_STAT_MULTIPLE;
        TRC_EVENT_1_ARG (LOCK, PTHREAD_TRACE_OP_LOCK, stat, a1, status);
        break;
      case RWLOCK_WRLOCK:
        TRC_EVENT_1_ARG (LOCK, PTHREAD_TRACE_OP_LOCK, stat, a1, status);
        break;
      case RWLOCK_TRYRDLOCK:
        stat = stat | PTHREAD_TRACE_STAT_NONBLOCK | 
          PTHREAD_TRACE_STAT_MULTIPLE;
        TRC_EVENT_1_ARG (LOCK, PTHREAD_TRACE_OP_LOCK, stat, a1, status);
        break;
      case RWLOCK_TRYWRLOCK:
        stat = stat | PTHREAD_TRACE_STAT_NONBLOCK;
        TRC_EVENT_1_ARG (LOCK, PTHREAD_TRACE_OP_LOCK, stat, a1, status);
        break;
      case RWLOCK_UNLOCK:
        /* We don't know if the target of the unlock is a readlock or 
           writelock, but... Assume not a Multiple, PTLockObj.cpp will 
           have to adjust stat = stat | PTHREAD_TRACE_STAT_MULTIPLE; */
        TRC_EVENT_1_ARG (LOCK, PTHREAD_TRACE_OP_UNLOCK, stat, a1, status);
        break;
      case RWLOCK_DESTROY:
        TRC_EVENT_1_ARG (LOCK, PTHREAD_TRACE_OP_DESTROY, stat, a1, status);
        break;
      case RWLOCK_BLOCK:
        TRC_EVENT_2_ARG (LOCK, PTHREAD_TRACE_OP_BLOCK, stat, a1,
                         THREAD, self->data.internals.p_id, status);
        break;
            
        // CONDITION VARIABLE Events
      case COND_INIT:
        {
          trc_thread_data *cData = get_thread_trc_data (self);
          pthreadTraceCondInit_t *cinit = &cData->cinit;
          
          if (type == TRACE_ATTR_SINGLE) 
            {
              // The first initialization event we get is an attribute event
              utl_memclr (cinit, sizeof (*cinit));
              if ((long) a1 == PTHREAD_PROCESS_SHARED)
                cinit->flags |= PTHREAD_TRACE_MUT_PSHARED;
            } 
          else if (type == TRACE_SINGLE) 
            {
              // When the initialization is complete, we get a 
              // TRACE_SINGLE event
              cinit->address = (pthread_trace_uint64_t) POINTER64 (a1);
              cinit->attributes = (pthread_trace_uint64_t) POINTER64 (a2);
              
              op = PTHREAD_TRACE_OP_CREATE;
              trc_internal_event (
                       PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_COND, op, stat),
                       (status == 0) ? 2:3, // argument count
                       PTHREAD_TRACE_TYPE_COND, a1,
                       PTHREAD_TRACE_TYPE_CINIT, cinit,
                       PTHREAD_TRACE_TYPE_ERRNO, status);
            }
            break;
          }
      case COND_DESTROY:
        TRC_EVENT_1_ARG (COND, PTHREAD_TRACE_OP_DESTROY, stat, a1, status);
        break;
      case COND_SIGNAL:
        TRC_EVENT_1_ARG (COND, PTHREAD_TRACE_OP_SIGNAL, stat, a1, status);
        break;
      case COND_BROADCAST:
        stat = stat | PTHREAD_TRACE_STAT_MULTIPLE;
        TRC_EVENT_1_ARG (COND, PTHREAD_TRACE_OP_SIGNAL, stat, a1, status);
        break;
      case COND_BLOCK:
        TRC_EVENT_2_ARG (COND, PTHREAD_TRACE_OP_BLOCK, stat, a1,
                         THREAD, self->data.internals.p_id, status);
        break;
      case COND_WAIT:
        op = PTHREAD_TRACE_OP_WAIT;
        trc_internal_event (
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_COND, op, stat),
                2,
                PTHREAD_TRACE_TYPE_COND, a1,
                PTHREAD_TRACE_TYPE_MUTEX, a2);
        break;
      case COND_TIMEDWAIT:
        utlTime_t iExpiration;
        iExpiration.tv_sec = (long) a3; // expiration in seconds;
        iExpiration.tv_nsec = 0; // TBS??  Can we get nsecs from anywhere?
        op = PTHREAD_TRACE_OP_WAIT;
        stat = stat | PTHREAD_TRACE_STAT_TIMED;
        trc_internal_event (
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_COND, op, stat),
                3,
                PTHREAD_TRACE_TYPE_COND, a1,
                PTHREAD_TRACE_TYPE_MUTEX, a2,
                PTHREAD_TRACE_TYPE_TIME, &iExpiration);
        break;		
            
        // THREAD Events
      case CREATE:
        {
          trc_thread_data *pData = get_thread_trc_data (self);
          pthreadTraceThreadInit_t *tinit = &pData->tinit;
          
          /* Save the parameters from first 4 events, and send 
             event on the TRACE_END event */
          switch (type) 
            {
              case TRACE_START:
                /* Set defaults */
                utl_memclr (tinit, sizeof (*tinit));
                tinit->creator = PTHREAD_TRACE_CREATOR_POSIX;
                // TBS: tinit->kind needs to handle "internal" threads
                tinit->kind = PTHREAD_TRACE_THD_KIND_NORMAL;
                // TBS: tinit->teb
                // TBS: tinit->cancel
                // TBS: tinit->cpu_mask
                        
                /* Save event data */
                tinit->handle = (pthread_trace_uint64_t) a1;
                tinit->attributes = (pthread_trace_uint64_t) a2;
                /* a3 is the proc descr, dereference to get the PC of the 
                   entry point  mask out upper bits if we are compiled +DD32 */
                tinit->start = 
                  (pthread_trace_uint64_t) POINTER64(PC_FROM_FP(a3));
                tinit->start_arg = (pthread_trace_uint64_t) a4;
                break;

              case TRACE_ATTR_START:
                // If detached set, set the flag
                if ((long) a1 == PTHREAD_CREATE_DETACHED) 
                  tinit->flags |= PTHREAD_TRACE_THD_DETACH;
                break;

              case TRACE_ATTR_MIDDLE:
                if (((long) a4 & PTHREAD_SCOPE_SYSTEM) == 
                    PTHREAD_SCOPE_SYSTEM) 
                  tinit->flags |= PTHREAD_TRACE_THD_SYS_SCOPE; 
                break;

              case TRACE_ATTR_END:
                tinit->policy = (long) a1;
                tinit->priority = (long) a2;
                break;

              case TRACE_END:
                /* Only generate failures here,  success is handled
                   in TRACE_SINGLE as generated by the 
                   pthread_callback_np() on the PARENT callback */
                if (status != 0) 
                  {
                    op = PTHREAD_TRACE_OP_CREATE;
                    trc_internal_event(
				PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
                                                   op, stat),
                                (status == 0)? 2 : 3, // argument count
                                PTHREAD_TRACE_TYPE_THREAD, a1,
                                PTHREAD_TRACE_TYPE_TINIT, tinit,
                                PTHREAD_TRACE_TYPE_ERRNO, status);
                  }
                break;
            }
          break;
        }
      case CANCEL:
        // This is the call to pthread_cancel, so report it as a request
        stat |= PTHREAD_TRACE_STAT_REQUEST;
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_CANCEL, stat, a1, status);
        break;
      case CANCELED:
        // This is the delivery of the cancel to the current thread.
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_CANCEL, stat,
                         self->data.internals.p_id, status);
        break;
      case BLOCK:
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_BLOCK, stat,
                         self->data.internals.p_id, status);
        break;
      case DESTROY:
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_DESTROY, stat, a1, status);
        break;
      case TEST_CANCEL:
        // do nothing
        break;
      case JOIN:
        op = PTHREAD_TRACE_OP_JOIN;
        if (type == TRACE_START) 
          {
            trc_internal_event (
		    PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                    (status == 0) ? 1 : 2, // argument count
                    PTHREAD_TRACE_TYPE_THREAD, a1,
                    PTHREAD_TRACE_TYPE_ERRNO, status);
          }
        else 
          {
            trc_internal_event (
		    PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                    (status == 0) ? 2 : 3, // argument count
                    PTHREAD_TRACE_TYPE_THREAD, a1,
                    PTHREAD_TRACE_TYPE_THDARG, a2,
                    PTHREAD_TRACE_TYPE_ERRNO, status);
          }
        break;
      case EXIT:
        op = PTHREAD_TRACE_OP_EXIT;
        trc_internal_event (
		      PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, 
                                            op, stat),
                      1, // argument count
                      PTHREAD_TRACE_TYPE_THDARG, a1);
        break;
      case DETACH:
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_DETACH, stat, a1, status);
        break;
      case KILL:
        // Kill event is call to pthread_kill
        stat |= PTHREAD_TRACE_STAT_REQUEST;
        // Intentionally drop into KILLED
      case KILLED:
        // Signal actually delivered to thread,
        op = PTHREAD_TRACE_OP_KILL;
        trc_internal_event (
		     PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                     (status == 0) ? 2 : 3, // argument count
                     PTHREAD_TRACE_TYPE_THREAD, a1,
                     PTHREAD_TRACE_TYPE_INT, a2,
                     PTHREAD_TRACE_TYPE_ERRNO, status);
        break;
      case SIGMASK:
        // TBS
        break;
      case SUSPEND:
        // Suspend count increased
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_SUSPEND, stat, a1, status);
        break;
      case RESUME:
        /* This resume request event happens after the RESUMED event, 
           but since this is only a "resume.request" and  it would be 
           out of order when the thread is actually resumed, just 
           ignore it. */
        // TRC_EVENT_1_ARG(THREAD,PTHREAD_TRACE_OP_RESUME,
        // stat|PTHREAD_TRACE_STAT_REQUEST,a1,status);
        break;
      case RESUMED:
        // Suspend count has gone to 0, so actually resume
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_RESUME, stat, a1, status);
        break;
      case CONTINUE:
        // Call to pthread_continue() which forces suspend count to 0,
        // so do a resume
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_RESUME, stat, a1, status);
        break;
      case CANCEL_STATE:
        {
          unsigned int cfs = 0;
          op = PTHREAD_TRACE_OP_SETCAN;
          if ((long) a1 == PTHREAD_CANCEL_ENABLE)
            cfs = PTHREAD_TRACE_CANCEL_STATE;
          else if ((long) a1 == PTHREAD_CANCEL_DISABLE)
            cfs = PTHREAD_TRACE_CANCEL_DISABLE;
          trc_internal_event (
		    PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                    (status == 0) ? 1 : 2, // argument count
                    PTHREAD_TRACE_TYPE_INT, cfs,
                    PTHREAD_TRACE_TYPE_ERRNO, status);
          break;
        }
      case CANCEL_TYPE:
        {
          unsigned int cft = 0;
          if ((long) a1 == PTHREAD_CANCEL_ASYNCHRONOUS)
            cft = PTHREAD_TRACE_CANCEL_TYPE;
          else if ((long) a1 == PTHREAD_CANCEL_DEFERRED)
            cft = PTHREAD_TRACE_CANCEL_DEFERRED;
          op = PTHREAD_TRACE_OP_SETCAN;
          trc_internal_event (
	            PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                    (status == 0) ? 1 : 2, // argument count
                    PTHREAD_TRACE_TYPE_INT, cft,
                    PTHREAD_TRACE_TYPE_ERRNO, status);
          break;
        }
      case USCHED_RUNNABLE:
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_READY, stat, a1, status);
        break;
      case USCHED_RUN:
        TRC_EVENT_1_ARG (THREAD, PTHREAD_TRACE_OP_RUN, stat, a1, status);
        break;
      case EQUAL:
        // Ignore
        break;
      case PSET_BIND:
        // TBS -- Determine if we can do something with this
        break;
      case GET_SCHED:
        // Ignore
        break;
      case SET_SCHED:
        op = PTHREAD_TRACE_OP_SETSCHED;
        trc_internal_event (
		    PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                    (status == 0) ? 2 : 3, // argument count
                    PTHREAD_TRACE_TYPE_THREAD, a1,
                    PTHREAD_TRACE_TYPE_SCHED, a2, a3,
                    PTHREAD_TRACE_TYPE_ERRNO, status);
        break;
      case ONCE:
        op = PTHREAD_TRACE_OP_ONCE;
        trc_internal_event (
		    PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_ONCE, op, stat),
                    (type == TRACE_END) ? 3 : 2, // argument count
                    PTHREAD_TRACE_TYPE_POINTER, a1,
                    PTHREAD_TRACE_TYPE_POINTER, a2,
                    PTHREAD_TRACE_TYPE_INT, (long) a4);
        break;
#ifdef WE_MAY_NEED_THESE_SOME_DAY
      case KEY_CREATE:
        // Send key events, if enabled, and not Key_0 which the vte 
        // cannot handle
        if (a1 != 0) 
          {
            pthreadTraceKeyInit_t kinit;
			
            utl_memclr (&kinit,sizeof (kinit));
            kinit.address = (pthread_trace_uint64_t) &a1;
            kinit.destructor.standard = POINTER64(PC_FROM_FP(a2));
            op = PTHREAD_TRACE_OP_CREATE;
            trc_internal_event (
			PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_KEY, op, stat),
                        (status == 0) ? 2 : 3, // argument count
                        PTHREAD_TRACE_TYPE_KEY, a1,
                        PTHREAD_TRACE_TYPE_KINIT, &kinit,
                        PTHREAD_TRACE_TYPE_ERRNO, status);
          }
        break;
      case KEY_DELETE:
        // Send key events, if enabled, and not Key_0 which the vte 
        // cannot handle
        if (a1 != 0) 
          {
            op = PTHREAD_TRACE_OP_DESTROY;
            trc_internal_event (
			PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_KEY, op, stat),
                        (status == 0) ? 1 : 2, // argument count
                        PTHREAD_TRACE_TYPE_KEY, a1,
                        PTHREAD_TRACE_TYPE_ERRNO, status);
          }
        break;
      case GET_SPECIFIC:
        // Send key events, if enabled, and not Key_0 which the vte 
        // cannot handle
        if (a1 != 0) 
          {
            // retval means something different for this call...
            if (retval == 0)
              stat |=  PTHREAD_TRACE_STAT_FAIL;
            else
              stat &= ~PTHREAD_TRACE_STAT_FAIL;
            op = PTHREAD_TRACE_OP_GETPROP;
            trc_internal_event (
			PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_KEY, op, stat),
                        2, // argument count
                        PTHREAD_TRACE_TYPE_KEY, a1,
                        // do we really get a pointer here?
                        PTHREAD_TRACE_TYPE_POINTER, retval);
          }
        break;
      case SET_SPECIFIC:
        // Send key events, if enabled, and not Key_0 which the vte 
        // cannot handle
        if (a1 != 0) 
          {
            op = PTHREAD_TRACE_OP_SETPROP;
            trc_internal_event (
			PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_KEY, op, stat),
                        (status == 0) ? 2 : 3, // argument count
                        PTHREAD_TRACE_TYPE_KEY, a1,
                        PTHREAD_TRACE_TYPE_POINTER, a2,
                        PTHREAD_TRACE_TYPE_ERRNO, status);
            }
        break;
      case SET_CONCURR:
        trc_internal_event (
			PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_SYSTEM,
                                              PTHREAD_TRACE_OP_SETPROP, stat),
			(status == 0) ? 1 : 2, // argument count
			PTHREAD_TRACE_TYPE_INT, (int) a1,
			PTHREAD_TRACE_TYPE_ERRNO, status);
        break;
		
        // USER_THR_FUNC Events
      case TSD_DEST_START:
        // Thread-specific destructor called
        trc_internal_event (
		    PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD, op, stat),
                    2, // argument count
                    PTHREAD_TRACE_TYPE_POINTER, a1,
                    PTHREAD_TRACE_TYPE_POINTER, POINTER64(PC_FROM_FP(a2)));
        break;
      case CANC_HAND_START:
        // TBS
        break;
      case ATFORK:
        // Registration of a fork handler, VT doesn't display these
        break;
      case FORK:
        // Called before and after fork in the parent
        if (type == TRACE_START) 
          {
            /*
              trc_internal_event(
              PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
              PTHREAD_TRACE_OP_BLOCK,stat),
              2, // argument count
              PTHREAD_TRACE_TYPE_THREAD, self->data.internals.p_id,
              PTHREAD_TRACE_TYPE_STRING, "fork"
              );
              */
          } 
        else 
          {
            /*
              trc_internal_event(
              PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
              PTHREAD_TRACE_OP_BLOCK,stat),
              2, // argument count
              PTHREAD_TRACE_TYPE_THREAD, self->data.internals.p_id,
              PTHREAD_TRACE_TYPE_STRING, "fork"
              );
              */
          }
        break;
      case FORK_PRE:
        // TBS
        break;
      case FORK_POST:
        // TBS
        break;
#endif
        
      default:
        // Unknown/unexpected event - do nothing
        break;
        
    } /* end switch 'event'  */
  
  // Re-enable tracing
  if (enable) TRACE_THREAD_ENABLE (self);
  __pthread_trace_spinlock_unlock (&big_fat_thread_rtc_lock);
}


/* Callback registered with _pthread_callback_np to report child startup */
void
__pthread_trace_child_callback (pthread_t parent, 
                                pthread_t child,
                                const _pthread_stack_info_t *stack_info) 
{
  if (!thread_trace_enabled) return;
  struct pthread *self = __pthread_trace_getself ();
  int enable = 0;
#ifdef HP_IA64
  /* On IPF the stack size reported in stack_info includes both user and RSE
     stack. We're only interested in the user stack size. */
  long stack_size = stack_info->stk_stacksize - stack_info->stk_rsestacksize;
#else
  long stack_size = stack_info->stk_stacksize;
#endif

  enable =  THREAD_TRACE_TURNED_ON (self);
  TRACE_THREAD_DISABLE (self);

  /* Communicate thread stack info to the event system */
  trc_internal_event (
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
                                   PTHREAD_TRACE_OP_STACKINIT, 0),
		2,
                PTHREAD_TRACE_TYPE_LONG, stack_size,
                PTHREAD_TRACE_TYPE_POINTER, stack_info->stk_stack_base);

  trc_internal_event (
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
                                   PTHREAD_TRACE_OP_START, 0),
		2,
		PTHREAD_TRACE_TYPE_THREAD, child,
                PTHREAD_TRACE_TYPE_THREAD, parent);

  if (enable) TRACE_THREAD_ENABLE (self);
}


/* On startup of the HP tracing system, register callbacks to intercept 
   thread creation in both the child and parent. */
void
__pthread_trace_startup (struct pthread_trace_info_t *info) 
{
  /* Create initial memory allocation for event tracing */
  allocate_new_block (0);

  /* Save trace offsets and function pointers */
  __pthread_trace_private_offset = info->private_offset;
  __pthread_trace_getself = info->getself;
  __pthread_trace_spinlock_lock = 
    (void (*) (__pthread_trace_spinlock_t*)) info->lock;
  __pthread_trace_spinlock_unlock = 
    (void (*) (__pthread_trace_spinlock_t*))  info->unlock;
  
  get_thread_trc_data (__pthread_trace_getself ())->tracelock = 
    __pthread_trace_spinlock_initial;

  trc_event_init ();
}


/* When a new thread is created, enable tracing for the thread. Internal 
   server threads do not call this routine, so they do not have tracing 
   enabled. */
void
__pthread_trace_create (pthread_d *thread) 
{
  shl_t handle = NULL;
  static int (*pthread_cps_get_stacksize_ptr)(void) = NULL;

#ifndef NDEBUG
  if (dbg_trace_enabled)
    dbg_print_dec ("__pthread_trace_create() for pid ", getpid(), 1);
#endif

  TRACE_THREAD_DISABLE (thread);
  
  get_thread_trc_data(thread)->tracelock = __pthread_trace_spinlock_initial;
  
  /* For the default thread, genereate an init event providing 
     stack information */
  if (thread->data.internals.p_id == 1) 
    {
      pthreadTraceThreadInit_t tinit;	
      struct sched_param param;
      int policy;
      int st = pthread_getschedparam (1, &policy, &param);
      assert(st == 0);

#ifdef __ia64
      /* Lookup uwx_set_nofr and uwx_self_init_state_cache_ptr. Can't call 
         it directly in get_frame_pc_list() because older versions if 
         unwind library don't have them */
      if (uwx_set_nofr_ptr == NULL)
        shl_findsym (&handle, "uwx_set_nofr", 
                     TYPE_PROCEDURE, &uwx_set_nofr_ptr);
      if (uwx_self_init_state_cache_ptr == NULL)
        shl_findsym (&handle, "uwx_self_init_state_cache", 
                     TYPE_PROCEDURE, &uwx_self_init_state_cache_ptr);
#endif

#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
      handle = NULL;
      if (pthread_cps_get_stacksize_ptr == NULL)
        shl_findsym (&handle, "__pthread_cps_get_stacksize", 
                     TYPE_PROCEDURE, &pthread_cps_get_stacksize_ptr);

      if (pthread_cps_get_stacksize_ptr == NULL)
        dbg_print ("Warning! __pthread_cps_get_stacksize lookup failed\n");
#endif

      /* Set defaults */
      utl_memclr (&tinit, sizeof (tinit));
      tinit.creator = PTHREAD_TRACE_CREATOR_POSIX;
      tinit.kind = PTHREAD_TRACE_THD_KIND_INITIAL;
      // TBS: This stack_base isn't exact, but the closest we can 
      // guess currently
      tinit.stack_base = (pthread_trace_uint64_t) (((char*)&tinit) + 4000); 
#if defined(GDB_TARGET_IS_HPPA_20W) || defined (HP_IA64)
      if (pthread_cps_get_stacksize_ptr)
        tinit.stack_size = pthread_cps_get_stacksize_ptr ();
#else
      tinit.stack_size = __pthread_cps_get_stacksize ();
#endif
      tinit.policy = policy;
      tinit.priority = param.sched_priority;
      TRC_EVENT_2_ARG (THREAD, PTHREAD_TRACE_OP_INIT, 0, 1, TINIT, &tinit, 0);
    }
  else
    {
      /* For non-default threads stack info is available a little later 
         and will be communicated to the event system in the child thread
         creation callback __pthread_trace_child_callback() */
      trc_thread_data *pData = 
        get_thread_trc_data (__pthread_trace_getself ());
      pthreadTraceThreadInit_t *tinit_p = &pData->tinit;
      TRC_EVENT_2_ARG (THREAD, PTHREAD_TRACE_OP_INIT, 0, 
                       thread->data.internals.p_id, TINIT, tinit_p, 0);
    }
    
  TRACE_THREAD_ENABLE (thread);
#ifndef NDEBUG
  if (dbg_trace_enabled) 
    dbg_print_dec ("Done __pthread_trace_create() for pid ", getpid (), 1);
#endif
}


/* Detect thread descruction */
void
__pthread_trace_exit (pthread_d *thread) 
{
  /* If tracing has been disabled for this thread  such as in a newly 
     forked child, don't trace this event
     if (!THREAD_TRACE_TURNED_ON(thread)) return; */
  TRACE_THREAD_DISABLE (thread);
  
  // Don't generate the event if thread-tracing is off in VMPI 
  // standard analysis
  trc_internal_event (
		PTHREAD_TRACE_MAKE_ID(PTHREAD_TRACE_OBJ_THREAD,
                                      PTHREAD_TRACE_OP_TERM, 0),
                0, // Can trace stack here, so use current context
                1,
                PTHREAD_TRACE_TYPE_THREAD, thread->data.internals.p_id);
    
  TRACE_THREAD_ENABLE (thread);
}

