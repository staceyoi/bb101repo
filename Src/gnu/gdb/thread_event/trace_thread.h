/*
 * BEGIN_DESC
 * 
 *  File: 
 *      @(#)	common/pthreads/trace_thread.h		$Revision: $
 * 
 *  Purpose:
 *      <<please update with a synopis of the functionality provided by this file>>
 * 
 *  Classification:			Release to Release Consistency Req:
 *		<<please select one of the following:>>
 * 	kernel subsystem private		none
 * 	kernel private				none
 * 	kernel 3rd party private		limited source
 * 	public					binary & source
 * 
 *  BE header:  no
 *
 *  Shipped:  yes
 *	/usr/include/sys/trace_thread.h
 *
 *  <<please delete the following note if this is a "public" header>>
 *  NOTE:
 *	This header file contains information specific to the internals
 *	of the HP-UX implementation.  The contents of this header file
 *	are subject to change without notice.  Such changes may affect
 *	source code, object code, or binary compatibility between
 *	releases of HP-UX.  Code which uses the symbols contained within
 *	this header file is inherently non-portable (even between HP-UX
 *	implementations).
 * 
 * END_DESC  
*/


/*
 * Tracing Features to be completed yet:
 *
 *	- Attribute Records for:
 *		pthread_create()
 *		pthread_mutex_init()
 *		pthread_cond_init()
 *		pthread_rwlock_init()
 *
 *	- Make the "caller" field actually display the address of the caller
 *
 */

/*****************************************************************************
 *
 * For each thread in the process, a trace file will be generated. Each file
 * has the name "tr.<process_id>.<pthread_id>". For example, a sample
 * program generated the files:
 *
 *	tr.02457.00001
 *	tr.02457.00002
 *	tr.02457.00003
 *
 * Each file contains trace records for the indicated thread. Each trace
 * record is separated by a newline character.
 *
 * The format of the trace record is as follows:
 *
 *	t1:t2:lwpid:pid:tid:cat:event:type:caller:retval:a1:a2:a3:a4:a5
 *
 * Where:
 *
 *	Field	Type			Meaning
 *	----- 	------			-------
 *	t1	time_t (int32_t)	time in secods
 *	t2	long			time in nanoseconds
 *	lwp	lwpid_t (int)		lwp ID of the thread
 *	pid	pid_t (int)		process ID
 *	tid	pthread_t (int)		pthread ID of the thread
 *	cat	pthread_trace_cat_t 	category ID of the generated event
 *	event	pthread_trace_event_t	ID of the generated event
 *	type	pthread_trace_type_t	type of event record
 *	caller  void *			address of the caller of this function
 *					or NULL if it can't be determined.
 *	retval  (int)/(void *)		return value of function
 *					
 *	NOTE: The rest of the fields are unused (ie, set to 0) if the type
 *	      is set to TRACE_BLOCK_START or TRACE_BLOCK_END
 *
 *	a1	<see chart>		depends on event
 *	a2	<see chart>		depends on event
 *	a3	<see chart>		depends on event
 *	a4	<see chart>		depends on event
 *	a5	for user defined events only - displays a user-defined string
 *
 *
 * Environment Variables:
 *
 *	THR_TRACE_DIR - Where to place the trace data files. If this is not
 *			defined, the files go to the current working directory.
 *
 *	THR_TRACE_SYNC -By default, trace records are buffered and only written
 *			to the file when the buffer is full. If this variable
 *			is set to any non-NULL value, data is immediately
 *			written to the trace file.
 *
 *	THR_TRACE_EVENTS - By default, all pthread events are traced. If this
 *			variable is defined, only the categories defined will
 *			be traced. Each category is separated by a ':'. The
 *			possible trace categories are:
 *
 *				thread:cond:mutex:rwlock
 *
 *			For example, to only trace thread and mutex operations
 *			set the THR_TRACE_EVENTS variable to:
 *
 *				"thread:mutex"
 *
 *****************************************************************************/

typedef short enum __pthread_trace_cat {
	THREAD		= 1,		/* thread management functions */
	MUTEX		= 2,		/* thread mutex functions */
	COND		= 3,		/* thread condvar functions */
	/* RESERVED     = 4, */
	RWLOCK		= 5,		/* thread rwlock functions */
	/* RESERVED     = 6, */
	/* RESERVED     = 7, */
	/* RESERVED     = 8, */
	/* RESERVED     = 9, */
	USER_THR_FUNC	= 10,		/* user defined TSD or CANC functions */
	/* RESERVED     = 11-998, */
	USER_DEFINED	= 999		/* user defined event */
	
} pthread_trace_cat_t;

/*
 * Trace event record identifier for all pthread_*() functions EXCEPT
 * for the attribute functions pthread_attr_*(), pthread_mutexattr_*(),
 * pthread_condattr_*(), and pthread_rwlockattr_*() which do not generate
 * trace records.
 *
 * NOTE: user-defined events can chose any number since they have a different
 *	 category number.
 */
typedef short enum __pthread_trace_event {
	CREATE		= 1,		/* pthread_create() */
	JOIN		= 2,		/* pthread_join() */
	DETACH		= 3,		/* pthread_detach() */
	EXIT		= 4,		/* pthread_exit() */
	SELF		= 5,		/* pthread_self() */
	SUSPEND		= 6,		/* pthread_suspend() */
	RESUME		= 7,		/* pthread_resume_np() */
	CONTINUE	= 8,		/* pthread_continue() */
	ONCE		= 9,		/* pthread_once() */
	KILL		= 10,		/* pthread_kill() */
	SIGMASK		= 11,		/* pthread_sigmask() */

	KEY_CREATE	= 12,		/* pthread_key_create() */
	KEY_DELETE	= 13,		/* pthread_key_delete() */
	GET_SPECIFIC	= 14,		/* pthread_getspecific() */
	SET_SPECIFIC	= 15,		/* pthread_setspecific() */
	TSD_DEST_START	= 16,		/* user defined TSD destructor func */

	GET_SCHED	= 17,		/* pthread_getschedparam() */
	SET_SCHED	= 18,		/* pthread_setschedparam() */
	SET_CONCURR	= 19,		/* pthread_setconcurrency() */
	GET_CONCURR	= 20,		/* pthread_getconcurrency() */

	CANCEL		= 21,		/* pthread_cancel() */
	CANCEL_STATE	= 22,		/* pthread_setcancelstate() */
	CANCEL_TYPE	= 23,		/* pthread_setcanceltype() */
	TEST_CANCEL	= 24,		/* pthread_testcancel() */
	CANC_HAND_START	= 25,		/* user defined canc handler */

	NUM_PROCESSORS	= 26,		/* pthread_num_processors_np() */
	PROCESSOR_ID	= 27,		/* pthread_processor_id_np() */
	PROCESSOR_BIND	= 28,		/* pthread_processor_bind_np */

	ATFORK		= 29,		/* pthread_atfork() */
	FORK_PRE 	= 30,		/* fork() pre handler */
	FORK_POST	= 31,		/* fork() post handler */
	FORK		= 32,		/* actual fork() system call */

	MUTEX_INIT	= 33,		/* pthread_mutex_init() */
	MUTEX_DESTROY	= 34,		/* pthread_mutex_destroy() */
	MUTEX_TRYLOCK	= 35,		/* pthread_mutex_trylock() */
	MUTEX_LOCK	= 36,		/* pthread_mutex_lock() */
	MUTEX_UNLOCK	= 37,		/* pthread_mutex_unlock() */
	MUTEX_SETPRIO	= 38,		/* pthread_mutex_setprioceiling() */
	MUTEX_GETPRIO	= 39,		/* pthread_mutex_getprioceiling() */

	RWLOCK_INIT	= 40,		/* pthread_rwlock_init() */
	RWLOCK_DESTROY	= 41,		/* pthread_rwlock_destroy() */
	RWLOCK_RDLOCK	= 42,		/* pthread_rwlock_rdlock() */
	RWLOCK_TRYRDLOCK= 43,		/* pthread_rwlock_tryrdlock() */
	RWLOCK_WRLOCK	= 44,		/* pthread_rwlock_wrlock() */
	RWLOCK_TRYWRLOCK= 45,		/* pthread_rwlock_trywrlock() */
	RWLOCK_UNLOCK	= 46,		/* pthread_rwlock_unlock() */

	COND_INIT	= 47,		/* pthread_cond_init() */
	COND_DESTROY	= 48,		/* pthread_cond_destroy() */
	COND_SIGNAL	= 49,		/* pthread_cond_signal() */
	COND_BROADCAST	= 50,		/* pthread_cond_broadcast() */
	COND_WAIT	= 51,		/* pthread_cond_wait() */
	COND_TIMEDWAIT	= 52,		/* pthread_cond_timedwait() */

	EQUAL		= 53,		/* pthread_equal() */

	PSET_BIND	= 54,		/* pthread_pset_bind_np */
	MUTEX_BLOCK 	= 55, 		/* mutex block event */
	COND_BLOCK 	= 56, 		/* condvar block event */
	RWLOCK_BLOCK 	= 57, 		/* rwlock block event */
	BLOCK		= 58,		/* thread block event */
	DESTROY		= 59,		/* thread id destroyed */
	RESUMED		= 60,		/* thread suspcnt = 0 */
	USCHED_RUNNABLE = 61,		/* unbound thread is put on runq */
	USCHED_RUN	= 62,		/* unbound thread is running */
	CANCELED	= 63,		/* thread being canceled */
	KILLED		= 64		/* unbound threads posted sig to self */

	/* up to 999 RESERVED */

} pthread_trace_event_t;


/*****************************************************************************
 *
 * Type of trace event record
 *
 * The list of functions after the typedef lists the types of records that
 * are associated with each of the functions and what their a1:a2:a3:a4:a5
 * fields mean.
 *
 ******************************************************************************/

typedef short enum __pthread_trace_type {
	TRACE_SINGLE		= 0,	/* record generated on entry / exit */
	TRACE_START		= 1,	/* record generated on function entry */
	TRACE_END		= 2,	/* record generated on function exit */
	TRACE_ATTR_SINGLE	= 3,	/* record generated on object create */
	TRACE_ATTR_START	= 4,	/* record generated on object create */
	TRACE_ATTR_MIDDLE	= 5,	/* record generated on object create */
	TRACE_ATTR_END		= 6 	/* record generated on object create */
} pthread_trace_type_t;


#define	TRACE_NOBLOCK		0	/* Caller didn't block in function */
#define	TRACE_BLOCKED		1	/* Caller blocked in function */

#define	TRACE_NOCALL		0	/* Caller didn't call init function */
#define	TRACE_CALLED		1	/* Caller called init function */


/*
 * retval:a1:a2:a3:a4:a5 values
 *
 * a1-a4 are 1 word type elements (ie, char int, long, pointer). The function
 * interface describes what the data type is. NOTE: for data types of long
 * or pointer, the application reading this data must understand that on
 * 32/64 bit systems/application these data types will be of different sizes.
 * NOTE: currently the retval field is always an int EXCEPT for the retval from
 * pthread_getspecific() (GET_SPECIFIC) which returns a void *.
 *
 * For argument parameters (i.e., arg1, arg2, arg3, arg4), if the value
 * listed below does not contain a "*", the actual value is in the record.
 * These are input only parameters to the function, hence only passed parameter
 * is displayed. If this is a pointer, the pointer is displayed, not what 
 * the pointer * points to.
 *
 * For argument parameters (i.e., arg1, arg2, arg3, arg4), if the value
 * listed contains a "*" (i.e., *arg1), the parameter is a pointer and
 * the contents of the pointer is displayed. Note: this is only done when
 * the pointer points to a single word (ie, thread ID, exit status ptr, etc).
 * These are generally ouput parameters.
 *
 * For fields which do not contain a value, the field is not used and 
 * thus nothing is ouput into the field (i.e., the field looks like "::"
 * for that field).
 *
 * If a function record indicates the function did not complete successfully,
 * some of the output fields (ie, *arg1) may not be displayed. For example,
 * pthread_create() may return EINVAL because arg1 (ptr to pthread_t) is
 * NULL. Consequently, *arg1 is not displayed. The field will contain no
 * output (i.e., the field looks like "::") if the field cannot reliably
 * be accessed.
 *
 * For fields which state "block?", a 1 will be displayed if the caller had
 * to block waiting for the event to complete. A 0 will be displated if the
 * caller did not have to block to wait for the event to complete.
 *
 * For fields which state "call?", a 1 will be displayed if the caller had
 * to call the once init function. A 0 will be displated if the caller did not 
 * have to call the once init function.
 *
 * For fields which state "waiter?", a number will be displayed which indicates
 * how many threads were waiting for this event also. For type SINGLE, this
 * number indicates how many threads were waiting on entry to the event.
 * For type START, this number indicates how many threads were waiting on
 * entry to the event. For type END, this number indicates how many threads
 * were waiting when the event completed (for example, mutex_lock has a START
 * and END record. These records will show the number of waiters on lock start
 * and on lock end when the mutex is acquired). NOTE: this information is not
 * available for synchronization primitives created with the 
 * PTHREAD_PROCESS_SHARED attribute.
 *
 * If the "retval" field contains an error indication and one of the fields
 * a1-a4 are output parameters to the function, the contents of the output
 * parameter will always be 0 (ie, undefined).
 *
 *
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_create()                START           arg1    arg2    arg3    arg4
 * pthread_create()                END     retval  *arg1   arg2    arg3    arg4
 * pthread_create()                ATTR_START      detach  stksize stkaddr grdsize
 * pthread_create()                ATTR_MIDDLE     spu     bndtype inherit contention
 * pthread_create()                ATTR_END        policy  sched_prio
 *
 * pthread_join()                  START           arg1    arg2
 * pthread_join()                  END     retval  arg1    *arg2 
 * pthread_once()                  START           arg1    arg2
 * pthread_once()                  END     retval  arg1    arg2           call?
 * pthread_suspend()               START           arg1
 * pthread_suspend()               END     retval  arg1
 * 
 * pthread_resume_np()             SINGLE  retval  arg1    arg2
 * pthread_continue()              SINGLE  retval  arg1
 * pthread_detach()                SINGLE  retval  arg1
 * pthread_exit()                  SINGLE          arg1
 * pthread_self()                  SINGLE  retval
 * pthread_equal()                 SINGLE  retval  arg1    arg2
 * pthread_kill()                  SINGLE  retval  arg1    arg2
 * pthread_sigmask()               SINGLE  retval  arg1    arg2    arg3
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_key_create()            SINGLE  retval  *arg1   arg2
 * pthread_key_delete()            SINGLE  retval  arg1
 * pthread_getspecific()           SINGLE  retval  arg1
 * pthread_setspecific()           SINGLE  retval  arg1    arg2
 * user destructor function        SINGLE          arg1    func	   key
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_getschedparam()         SINGLE  retval  arg1    *arg2    *arg3(pri)
 * pthread_setschedparam()         SINGLE  retval  arg1    arg2     *arg3(pri)
 * pthread_setconcurrency()        SINGLE  retval  arg1
 * pthread_getconcurrency()        SINGLE  retval 
 * pthread_num_processors_np()     SINGLE  retval
 * pthread_processor_id_np()       SINGLE  retval  arg1    *arg2   arg3
 * pthread_processor_bind_np()     SINGLE  retval  arg1    *arg2   arg3    arg4
 * pthread_pset_bind_np()     	   SINGLE  retval  arg1    *arg2   arg3    
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_cancel()                SINGLE  retval  arg1
 * pthread_setcancelstate()        SINGLE  retval  arg1    *arg2
 * pthread_setcanceltype()         SINGLE  retval  arg1    *arg2
 * pthread_testcancel()            SINGLE
 * user cancellation handler       SINGLE          arg1    func
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_atfork()                SINGLE  retval  arg1    arg2    arg3
 * fork() pre-handler              SINGLE          func
 * fork() fost-handler             SINGLE          func
 * actual fork() system call       START   
 * actual fork() system call       END     retval
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_mutex_init()            SINGLE  retval  arg1    arg2
 * pthread_mutex_init()            ATTR_START      pshared type    how     protocol
 * pthread_mutex_init()            ATTR_END        prio
 *
 * pthread_mutex_lock()            START           arg1                  waiter?
 * pthread_mutex_lock()            END     retval  arg1           block? waiter?
 *
 * pthread_mutex_destroy()         SINGLE  retval  arg1
 * pthread_mutex_trylock()         SINGLE  retval  arg1                  waiter?
 * pthread_mutex_unlock()          SINGLE  retval  arg1                  waiter?
 * pthread_mutex_setprioceiling()  SINGLE  retval  arg1    arg2    *arg3
 * pthread_mutex_getprioceiling()  SINGLE  retval  arg1    *arg2
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_rwlock_init()           SINGLE  retval  arg1    arg2
 * pthread_rwlock_init()           ATTR_SINGLE     pshared
 *
 * pthread_rwlock_rdlock()         START           arg1          r_wait? w_wait?
 * pthread_rwlock_rdlock()         END     retval  arg1   block? r_wait? w_wait?
 * pthread_rwlock_wrlock()         START           arg1          r_wait? w_wait?
 * pthread_rwlock_wrlock()         END     retval  arg1   block? r_wait? w_wait?
 *
 * pthread_rwlock_tryrdlock()      SINGLE  retval  arg1          r_wait? w_wait?
 * pthread_rwlock_trywrlock()      SINGLE  retval  arg1          r_wait? w_wait?
 * pthread_rwlock_unlock()         SINGLE  retval  arg1          r_wait? w_wait?
 * pthread_rwlock_destroy()        SINGLE  retval  arg1
 * 
 * Function Name                   Type    retval   a1      a2      a3      a4
 * -----------------------         -----   ------  -----   -----   -----   -----
 * pthread_cond_init()             SINGLE  retval  arg1    arg2
 * pthread_cond_init()             ATTR_SINGLE     pshared
 *
 * pthread_cond_wait()             START           arg1    arg2          waiter?
 * pthread_cond_wait()             END     retval  arg1    arg2          waiter?
 * pthread_cond_timedwait()        START           arg1    arg2   a3.sec waiter?
 * pthread_cond_timedwait()        END     retval  arg1    arg2   a3.sec waiter?
 *
 * pthread_cond_destroy()          SINGLE  retval  arg1
 * pthread_cond_signal()           SINGLE  retval  arg1                  waiter?
 * pthread_cond_broadcast()        SINGLE  retval  arg1                  waiter?
 * 
 * Function Name      Type    retval   a1      a2      a3      a4    a5
 * ---------------    -----   ------  -----   -----   -----   -----  -------
 * user defined       SINGLE                                         *string
 *
 */
