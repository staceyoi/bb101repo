/*		New Malloc - Memory allocator
		by Edgar Circenis, HP, UDL, 8/89

BLOCK STRUCTURE

  +-+-+-+----------------+-+
  |s| | |                | |
  |i| | |                |&|
  |z|p|n|                |h|
  |e|r|e|   block data   |e|
  |-|e|x|                |a|
  |F|v|t|                |d|
  |-| | |                |e|
  |U| | |                |r|
  +-+-+-+----------------+-+
  
  The basic block data structure is as above.
  
  size: size of block in bytes (includes overhead).
  F: if set, previous block is free. (bit 1 of size field)
  U: if set, current block is in use. (bit 0 of size field)
  prev: pointer to previous block's size field.
  next: pointer to next block's size field.
  &header: pointer to current blocks' size field.

  When a block is allocated, only the size field is used.  When a block is
  freed, it is inserted into the linked list of free blocks, its &header field
  is set to point to the start of the block, and the F bit of the next block is
  set to indicated that the block preceeding it is free.

  A dummy word is stored at the end of each mmap() region to serve as a
  dummy malloc header. This is needed when we try to update the next 
  block to indicate whether the previous block is free. */

#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include "trace_util.h"

#define NTH_CHAR(x, n) (((char *) (&(x)))[(n)])

/* define minimum size of a malloc allocation. */
#define MALLOC_MINSIZE	(4*sizeof(m_header))

#define FREE		(0x2)	/* signifies that previous block is free */
#define USED		(0x1)	/* signifies that current block is used  */
#define MASK		(FREE|USED)	/* mask for size bits of header    */
#define SIZE(x)		(*(x)&~MASK)	/* returns size of block in bytes  */
					/* this includes the size of the   */
					/* header!			   */
#define FLAGS(x)	(*(x)&MASK)	/* returns FREE and USED bits	   */

/* bit manipulation for USED bit */
#define TST_USED(x)	(*(x)&USED)
#define CLR_USED(x)	(*(x)&= ~USED)
#define SET_USED(x)	(*(x)|= USED)

/* bit manipulation for FREE bit */
#define TST_FREE(x)	(*(x)&FREE)
#define CLR_FREE(x)	(*(x)&= ~FREE)
#define SET_FREE(x)	(*(x)|= FREE)

/* Pointer to previous free block in block list */
#define PREV_FREE(x)	(*(m_header **)((x)+1))
/* Pointer to next free block in block list */
#define NEXT_FREE(x)	(*(m_header **)((x)+2))

/* Pointer to last word of current block */
#define LAST_WORD(x)	(*(m_header **)((m_header)(x)+SIZE(x)-sizeof(m_header)))

/* Pointer to header of free block preceeding current block */
#define PREV_BLOCK(x)	((m_header *)(*(m_header *)((x)-1)))
/* Pointer to next block in memory (not necessarily free) */
#define NEXT_BLOCK(x)	(m_header *)((m_header)(x)+SIZE(x))

/* This macro will insert a previously used block at the head of 
   the freelist */
#define ADD_TO_FREELIST(x) \
		if (NEXT_FREE(x)=freelist) \
			/* must re-link backward pointers */ \
			PREV_FREE(freelist)=x; \
		PREV_FREE(x) = NULL; \
		freelist = x

/* This macro will remove a free block from the freelist */
#define REMOVE_FROM_FREELIST(x)	\
		if (PREV_FREE(x))	/* not first block in list */ \
			NEXT_FREE(PREV_FREE(x)) = NEXT_FREE(x); \
		else			/* first block in list */ \
			freelist = NEXT_FREE(x); \
		if (NEXT_FREE(x))	/* not last in list */ \
			PREV_FREE(NEXT_FREE(x)) = PREV_FREE(x); \
		/* must clear FREE bit on next block */ \
		CLR_FREE(NEXT_BLOCK(x))

/* This macro will remove a free block from the freelist */
/* but, it won't clear the FREE bit on the next block	 */
#define REMOVE_FROM_FREELIST2(x)	\
		if (PREV_FREE(x))	/* not first block in list */ \
			NEXT_FREE(PREV_FREE(x)) = NEXT_FREE(x); \
		else			/* first block in list */ \
			freelist = NEXT_FREE(x); \
		if (NEXT_FREE(x))	/* not last in list */ \
			PREV_FREE(NEXT_FREE(x)) = PREV_FREE(x)

/* This macro will relink block x using pointers in block y */
#define RELINK(x,y) \
		if (NEXT_FREE(x) = NEXT_FREE(y)) \
			/* re-link backward pointer */ \
			PREV_FREE(NEXT_FREE(x))=x; \
		if (PREV_FREE(x) = PREV_FREE(y)) \
			/* re-link forward pointer */ \
			NEXT_FREE(PREV_FREE(x)) = x; \
		else \
			/* x is now first in list */ \
			freelist = x

#define ROUND(num, base) (((num) + (base)-1) & ~((base)-1))

typedef uintptr_t m_header;		/* must be size of a pointer */

static m_header *freelist = 0;	/* head of free block list */
static int freemem_size = 0;
static __pthread_trace_spinlock_t freelist_lock = __PTHREAD_SPINLOCK_INIT;
static m_header *grow_arena();		/* function to grow arena */

/* utl_malloc() -- allocate a block of memory out of the arena.
   utl_malloc returns a pointer to a block of the size requested.
   utl_alloc(0) returns a NULL pointer 
   will return (void *)NULL if cannot allocate memory */
void*
utl_malloc (unsigned int nbytes)
{
  m_header s;
  m_header *allocptr,*tempblk;
  void* retval;

  /* returns NULL for zero size blocks */
  if (nbytes == 0) 
    return(NULL);

  /* Add in malloc header size. Size of pointer of overhead with minimum 
     block size of 16 bytes we must ROUND the request to a double word 
     multiple (needed to handle floating point load/store in libunwind) 
     since the size field is only able to handle word multiples 
     (last two bits used as flags) */
  nbytes = ROUND (nbytes + 2 * sizeof (uint64_t), 2 * sizeof (uint64_t));
  nbytes = (nbytes < MALLOC_MINSIZE) ? MALLOC_MINSIZE : nbytes;

  __pthread_trace_spinlock_lock (&freelist_lock);
  allocptr = freelist;

  while (allocptr) 
    {
      /* get size of block and compare to what's needed */
      if ((SIZE (allocptr)) >= nbytes)
        break;
      else
        allocptr = NEXT_FREE (allocptr); /* next free block */
    }

  /* if didn't find a block, we can't continue */
  if (!allocptr) 
    {
      __pthread_trace_spinlock_unlock (&freelist_lock);
      trc_internal_error_abort ("Out of memory.", ENOMEM);
    }

  /* drop through with big enough block */
  if (SIZE (allocptr) >= nbytes + MALLOC_MINSIZE) 
    { /* split into 2 blocks */
      s = SIZE (allocptr) - nbytes;
      /* mark size of block and USED bit */
      *allocptr = (nbytes | USED);
      /* tempblk is newly split-off free block */
      tempblk = NEXT_BLOCK (allocptr);
      /* re-link freelist using tempblk instead of allocptr */
      RELINK(tempblk, allocptr);
      /* store size of block */
      *tempblk = s;
      /* last word points to start of block */
      LAST_WORD (tempblk) = tempblk;
    } 
  else 
    { /* use entire block */
      /* remove block from freelist */
      REMOVE_FROM_FREELIST (allocptr);
      /* mark block as used */
      SET_USED (allocptr);
    }

  freemem_size -= SIZE (allocptr);
  /* return beginning of data */
  retval = ((char*) allocptr) + 2 * sizeof (uint64_t);

  __pthread_trace_spinlock_unlock (&freelist_lock);
  return retval;
}

/* utl_free() -- Free a block allocated by ultMalloc().
   If the pointer passed to utlree() is not a pointer to a block
   allocated by utl_malloc(), or points to a block that had previously
   been freed, the linked list structure will be corrupted. */
void 
utl_free (void *p)
{
  m_header *allocptr;
  m_header *nextblk;
  m_header s;
  int coalesced=0;

  if (p == (char *)NULL) 
    { /* utl_free() of NULL pointer does nothing */
      return;
    }

  __pthread_trace_spinlock_lock (&freelist_lock);
  allocptr = (m_header *) ((char*) p - 2 * sizeof (uint64_t));
  CLR_USED(allocptr);	       /* so that we can detect multiple free's
			          even after block has been coalesced */

  /* get size of block to be freed */
  s = SIZE (allocptr);
  freemem_size += s;
  if (TST_FREE (allocptr)) 
    {  /* coalesce with previous block */
      allocptr = PREV_BLOCK (allocptr);
      /* add size of previous block */
      s += SIZE (allocptr);
      *allocptr = s;
      /* set FREE bit on next block */
      SET_FREE (NEXT_BLOCK (allocptr));
      coalesced++;	       /* block is part of free list now */
    }

  if (!TST_USED (nextblk = NEXT_BLOCK (allocptr))) 
    {
      if (coalesced) 
        { /* already coalesced with previous block */
          /* remove nextblk from free list */
          /* but, don't clear the FREE bit on next block */
          REMOVE_FROM_FREELIST2 (nextblk);
        } 
      else 
        { /* forward coalesce only */
          /* re-link freelist using allocptr instead of nextblk */
          RELINK (allocptr, nextblk);
          coalesced++;
        }
      
      /* store new size (previous block will always be in use) */
      *allocptr = s + SIZE (nextblk);
    }

  if (!coalesced) 
    {
      /* insert at head of free list */
      ADD_TO_FREELIST (allocptr);
      /* set FREE bit on next block */
      SET_FREE (NEXT_BLOCK (allocptr));
    }
  
  /* last word of free block contains addr */
  LAST_WORD(allocptr) = allocptr;
  __pthread_trace_spinlock_unlock (&freelist_lock);
}

/* This value should be tuned. We do not want to allocate too much memory
   since that will force excessive swap to be allocated. On the other 
   hand, we want to reduce the number of malloc() requests that are made. 
   Currently set to: 4MB */
#define GROW_INCREMENT (1 << 22)

/* dummy area at the end of each arena that serves as a dummy malloc header */
#define ARENA_PAD      sizeof(m_header *)

static m_header*
grow_arena (int nbytes, int lock)
{
  m_header *blk;
  int size;
  int zero_fd = -1;
  
  size = ROUND (nbytes + ARENA_PAD, GROW_INCREMENT);
  while (size > 0) 
    {
      blk = (m_header *) malloc (size);
      if (blk != NULL) 
        {
          /* set up to look like a large free block */
          *blk = size;

          /* leave pad at the end of the region for setting 
             and clearing of bits */
          LAST_WORD (blk) = (m_header *) USED;
          *blk -= ARENA_PAD;

          /* because of the requirments for the smallest possible blocks,
             these bits will always be 0 after a size assignment. */
          /* CLR_USED(blk); */
          /* CLR_FREE(blk); */
          PREV_FREE (blk) = 0;
          LAST_WORD (blk) = blk;

          if (lock) 
            __pthread_trace_spinlock_lock (&freelist_lock);

          if (!freelist) 
              NEXT_FREE (blk) = 0;
          else 
              NEXT_FREE (blk) = freelist;

          freelist = blk;
          freemem_size += size;

          if (lock) 
            __pthread_trace_spinlock_unlock (&freelist_lock);

          return (blk);
        }
      return(NULL);
    }
  return(NULL);
}


void 
allocate_new_block (int lock)
{
  static int growth_incr = GROW_INCREMENT;

  if (freemem_size > GROW_INCREMENT) 
    return;

  if (!grow_arena (growth_incr, lock)) 
    {
      trc_internal_error_abort ("Out of memory.", ENOMEM);
    }
  growth_incr *= 2;
}


/* libc-isolation routines                                             
   The following routine emulate some functions provided by libc.
   due to the restricted environment required by code in this
   module, we need to limit our use of libc.  These routines
   provide common support needed. */

/* NAME:        nvmatch
   FUNCTION:    see if 's1' matches 's2'.  's1' is either "name" or
               "name=value" and 's2' is "name=value".
   RETURN VALUE DESCRIPTION:    s2 if they match, else NULL  */
static char*
nvmatch (char *s1, char *s2)
{
  while (*s1 == *s2++)
    if (*s1++ == '=')
      return (s2);

  if (*s1 == '\0' && *(s2 - 1) == '=')
    return (s2);

  return (NULL);
}


/* From getenv.c to allow access to environ without TIS locking */
char*
utl_getenv(char *name)
{
  char **p, *v;
	
  if ((p = environ) != NULL) 
    {
      while (*p != NULL) 
        {
          if (*name != **p) 
            {
              p++;
            }
          else 
            {
              if ((v = nvmatch ((char *)name, *p++)) != NULL) 
                  return v;
            }
        }
    }
  return (NULL);
}

/* From strstr.c */
char*
utl_strstr (char *s1, char *s2)
{
  unsigned char *p, *q, *r;
  register int pp, qq, rr;

  if( *s2 == '\0' )
    return((char *) s1);

  q = (unsigned char *) s1;
  while (1) 
    {
      qq = *q;
      if (qq != '\0') 
        {
          r = q;
          p = (unsigned char *) s2;
          while (1) 
            {
              rr = *r;
              pp = *p;
              if (rr != '\0' && pp != '\0') 
                {
                  if (pp != rr)
                    break;
                  r++;
                  p++;
                }
              else
                break;
            }
          if (pp == '\0')
            break;
          q++;
        }
      else
        break;
    }

  if (qq)
    return ((char *) q);
  else
    return (NULL);
}


char*
utl_strrchr (char *s, int c)
{
  char *r;

  r = NULL;
  do 
    {
      if(*(unsigned char *) s == c)
        r = (char *)s;
    } 
  while(*s++);

  return (r);
}


char*
utl_strcpy (char *str1, char *str2)
{
  char *p1 = str1;
  const char *p2 = str2;

  while (1)
    if ((*p1++ = *p2++) == '\0') 
      break;

  return str1;
}

char*
utl_strncpy (char *str1, char *str2, int length)
{
  char        *p1 = str1;
  const char  *p2 = str2;
  int    i = length;

  while (i-- > 0)
    if ((*p1++ = *p2++) == '\0') break;

  return str1;
}


int
utl_strncmp (char *str1, char *str2, int length)
{
  const char  *s1 = str1, *s2 = str2;
  int i = length;

  while ((*s1 != '\0') && (*s2 != '\0')
         && (*s1 == *s2) && (i > 0)) 
    {
      s1++;
      s2++;
      i--;
    }

  if (i <= 0)
    return 0;
  else
    return (int) (*s1 - *s2);
}

int
utl_strcmp (char *str1, char *str2)
{
  const char  *s1 = str1, *s2 = str2;
    
  while ((*s1 != '\0') && (*s2 != '\0') && (*s1 == *s2)) 
    {
      s1++;
      s2++;
    }
  
  return (int) ((unsigned char) *s1 - (unsigned char) *s2);
}


char*
utl_strcat (char *str1, char *str2)
{
  char                *p1 = str1;
  const char          *p2 = str2;

  /* Find the end of the first string */
  while (*p1 != 0) p1++;

  while (1)
    if ((*p1++ = *p2++) == '\0') break;

  return str1;
}


int
utl_strlen (char *str)
{
  const char *tmpstr = str;

  while (*tmpstr++ != '\0');

  return (tmpstr - str) - 1;
}


void*
utl_memcpy (void *s1, void *s2, size_t size)
{
  const char  *p1 = (const char *) s2;
  char        *p2 = (char *) s1;
  char        *lm = (char *) s1 + size;

  /* FIX-ME:  A compiler intrinsic should be used instead of this routine
     where available.  (And where not, this routine ought to copy
     in bigger chunks!)  */
  while (p2 < lm)
    *p2++ = *p1++;

  return s1;
}


void
utl_memclr (void *s1, size_t size) 
{
  char *p2 = (char*) s1;
  char *lm = (char*) s1 + size;
  while (p2 < lm)
    *p2++ = 0;
}

/* Convert a unsigned long to string of decimal digits */
static char* 
utl_format_dec (char *a, uint64_t n) 
{
  unsigned r = n % (unsigned) 10; 
  if (n /= (unsigned) 10) a = utl_format_dec (a, n); 
  *a++ = r + '0';
  *a = 0; 
  return a; 
}

/* Convert a unsigned long to hex */
static void 
utl_format_hex (char num[17], vt_ulong_t n) 
{
  int i; /* loop over each byte in value */
  char digits[] = "0123456789ABCDEF";  /* symbol for each byte */

  for (i = 0; i <= 15; i++) 
    {/* Convert each byte to text */
      num[i] = digits[(n >> (4*(15-i))) & 0xf];
    }
  num[16] = 0; /* Add nul */
}

/* Convert an errno value to decimal, and add newline */
static void 
utl_format_errno (char num[20], int n) 
{
  int i; /* loop over each byte in value */
  int o = 0; /* Output digit */
  char digits[] = "0123456789ABCDEF";  /* symbol for each byte */
    
  /* Add 0x prefix to output */
  num[0] = '0';
  num[1] = 'x';
  num = &num[2];

  for (i = 0; i <= 15; i++) 
    { /* Convert each byte to text */
      int digit = digits[(n >> (4*(15-i))) & 0xf];
      /* Drop all leading zeros */
      if (digit != '0' || o) num[o++] = digit;
    }

  num[o++] = '\n'; /* Add newline */
  num[o++] = 0; /* Add nul */
}


#ifndef NDEBUG
/************************************************************************/
/*  Message format & Printing routines                                  */
/************************************************************************/
void
dbg_print(char *x)
{
  trc_internal_error_msg (x);
}

void
dbg_print_dec(char *x, vt_ulong_t b, int nl)
{
  char num1[17]; /* Resulting number 8-bytes + nul */
  utl_format_dec (num1, b);
  trc_internal_error_msg (x);
  trc_internal_error_msg (num1);
  if (nl) 
    trc_internal_error_msg ("\n");
}

void
dbg_print_hex(char *x, vt_ulong_t b, int nl)
{
  char num1[17]; /* Resulting number 8-bytes + nul */
  utl_format_hex (num1, b);
  trc_internal_error_msg (x);
  trc_internal_error_msg (num1);
  if (nl) 
    trc_internal_error_msg ("\n");
}

/* Print the file/line where the assertion failed */
void 
trc_assert(char *a, char *f, int line) 
{
  char num1[17]; /* Resulting number 8-bytes + nul */

  (void) utl_format_dec (num1, line);
  trc_internal_error_msg ("Assertion failure abort:");
  trc_internal_error_msg (f);
  trc_internal_error_msg (":");
  trc_internal_error_msg (num1);
  trc_internal_error_msg ("\n");
  trc_internal_error_msg ("Exiting...\n");
  (void) abort ();
}
#endif

/* Internal Error message support routine.
   This routine formats an error message to stderr.  
   Internal newlines must be specified using the NL macro.  On VMS,
   this maps to "\n\r" for proper formatting.  On UNIX this maps to "\n"
   This routine also requires an explicit "\n" to terminate the message.

 Arguments:
   msg -- error message to be printed */
void 
trc_internal_error_msg(char *msg) 
{
  int st;

  /* Make sure the message gets out */
  do 
    {
      st = _write_sys (2, msg, utl_strlen (msg)); 
    } 
  while (st < 0 && errno == EINTR);
}

/* Internal Error Abort support routine.
   This routine formats a termination message. If the msg argument ends with 
   an '=' character, then the specified err value is formated into hex and 
   appended to the msg argument. Finally the "Aborting..." message is printed.
   Internal newlines must be specified using the NL macro.  On VMS,
   this maps to "\n\r" for proper formatting.  On UNIX this maps to "\n"
   This routine does not require an explicit newline at the end of the message
   it is always added.

   Arguments:
     msg -- description of particular error
     err -- errno value converted to hex and printed following message */
void 
trc_internal_error_abort(char *msg, int err) 
{
  /* Get length of message so we can check for trailing space */
  int len = utl_strlen (msg);

  /* Write header */
  trc_internal_error_msg ("Trace library -- Non-recoverable error.\n");

  /* Write message */
  trc_internal_error_msg (msg);

  /* Determine if errno value is expected (indicated by trailing =) */
  if (msg[len-1] == '=') 
    {
      char errval[17];
      utl_format_errno (errval, err);
      trc_internal_error_msg (errval);
    }
  else 
    {
      trc_internal_error_msg ("\n");
    }

  /* Print aborting */
  trc_internal_error_msg ("Exiting...\n");

  /* Abort causes a signal which won't really cause us to abort
     so use _exit instead, will cause us to exit without cleanup */
  _exit (13);
}


/* Hash function. */
unsigned int 
hash (int64_t key)
{
  register char* k = (char*) &key;
  register unsigned int a, b, c;

  a = b = c = 0x9e3779b9;

  b += ((unsigned int) k[7] << 24);
  b += ((unsigned int) k[6] << 16);
  b += ((unsigned int) k[5] << 8);
  b += (unsigned int) k[4];
  
  a += ((unsigned int) k[3] << 24);
  a += ((unsigned int) k[2] << 16);
  a += ((unsigned int) k[1] << 8);
  a += (unsigned int) k[0];
    
  /* Mix the values */
  a -= b; a -= c; a ^= (c >> 13);
  b -= c; b -= a; b ^= (a << 8);
  c -= a; c -= b; c ^= (b >> 13);
  a -= b; a -= c; a ^= (c >> 12);
  b -= c; b -= a; b ^= (a << 16);
  c -= a; c -= b; c ^= (b >> 5);
  a -= b; a -= c; a ^= (c >> 3);
  b -= c; b -= a; b ^= (a << 10);
  c -= a; c -= b; c ^= (b >> 15);
  
  return c;
}
