#ifndef _MEM_UTILS_H
#define _MEM_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "global_defs.h"

/* Private sys call interfaces (from pthread/internal.h) */
extern int _write_sys (int fildes, const void *buf, size_t nbyte);

char* utl_getenv (char *name);
char* utl_strstr (char *s1, char *s2);
char* utl_strrchr (char *s, int c);
char* utl_strcpy (char *str1, char *str2);
char* utl_strncpy (char *str1, char *str2, int length);
int utl_strncmp (char *str1, char *str2, int length);
int utl_strcmp (char *str1, char *str2);
char* utl_strcat (char *str1, char *str2);
int utl_strlen (char *str);
void* utl_memcpy (void *s1, void *s2, size_t size);
void utl_memclr (void *s1, size_t size);
unsigned int hash (int64_t key);

void dbg_print (char *x);
void trc_internal_error_msg (char *msg);
void trc_internal_error_abort (char *msg, int err);
void trc_assert (char *a, char *f, int line);

void* utl_malloc (unsigned int nbytes);
void utl_free (void *p);
void allocate_new_block (int);

#ifdef NDEBUG
#define assert(x)
#else
#define assert(x) if (!(x)) { trc_assert(#x, __FILE__, __LINE__); }
void dbg_print_hex (char *x, vt_ulong_t b, int nl);
void dbg_print_dec (char *x, vt_ulong_t b, int nl);
#endif

/* Spinlock realted declarations */
typedef struct __pthread_trace_spinlock_t 
{
  int                     sp_lock[4];
#ifndef __LP64__
  unsigned int            sp_pad_64;      /* 64-bit sz == 32-bit sz */
#endif /* __LP64__ */
  void     *sp_reserved;   /* used by library */
  unsigned int            sp_spincnt;
  unsigned int            sp_waiter;
} __pthread_trace_spinlock_t;

extern void (*__pthread_trace_spinlock_lock)(__pthread_trace_spinlock_t*);
extern void (*__pthread_trace_spinlock_unlock)(__pthread_trace_spinlock_t*);


#define __SPINS_PER_LOOP        4096

#define __SPINCNT_YIELDFREQ     256
#define __MAX_SPINCNT           (__SPINCNT_YIELDFREQ*3)

#define __SPIN_WAITER           1
#define __SPIN_NOWAITERS        0

#ifndef __LP64__
#define __PTHREAD_SPINLOCK_INIT                 \
{                                               \
        1, 1, 1, 1,                             \
        (unsigned int) 0, /* must be 0 */       \
        (void *) 1,              \
        (unsigned int) 1,                       \
        (unsigned int) __SPIN_NOWAITERS,        \
}
#else /* ! __LP64__ */
#define __PTHREAD_SPINLOCK_INIT                 \
{                                               \
        1, 1, 1, 1,                             \
        (void *) 1,              \
        (unsigned int) 1,                       \
        (unsigned int)__SPIN_NOWAITERS,         \
}
#endif /* ! __LP64__ */


#ifdef __cplusplus
}
#endif

#endif /* _MEM_UTILS_H */
