/* Top level stuff for GDB, the GNU debugger.  Copyright 1986-2000 Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include "gdbcmd.h"
#include "call-cmds.h"
#include "symtab.h"
#include "inferior.h"
#include "signals.h"
#include "target.h"
#include "breakpoint.h"
#include "gdbtypes.h"
#include "expression.h"
#include "value.h"
#include "language.h"
#include "terminal.h"		/* For job_control.  */
#include "annotate.h"
#include "top.h"
#include "version.h"

/* readline include files */
#include <readline/readline.h>
#include <readline/history.h>

/* readline defines this.  */
#undef savestring

#include <sys/types.h>

#ifdef HP_IA64
#include <a.out.h>
#endif

#include "event-top.h"
#include "gdb_string.h"
#ifndef NO_SYS_FILE
#include <sys/file.h>
#endif
#include "gdb_stat.h"
#include <ctype.h>
#ifdef GDB_TARGET_IS_HPUX
#include <sys/utsname.h>
#endif 
extern int nimbus_version;

#ifdef UI_OUT
#include "ui-out.h"
#include "cli-out.h"
#endif
#include "arch-utils.h"

#include "elf.h"            /* JAGae35325 - PA gdb running on IA */
#include "sys/utsname.h"
#define EM_IA64 50
int is_IA_machine = 0;

extern int nimbus_version;
extern int xdb_commands;  /* whether or not the user invoked -xdb */
extern int dbx_commands;  /* whether or not the user invoked -dbx */

/* Prototypes for local functions */

static void dont_repeat_command (char *, int);

static void source_cleanup_lines (PTR);

static void user_defined_command (char *, int);

static void init_signals (void);

#ifdef STOP_SIGNAL
static void stop_sig (int);
#endif

static char *line_completion_function (char *, int, char *, int);

static char *readline_line_completion_function (char *, int);

/* NOTE 1999-04-29: this function will be static again, after we make the
   event loop be the default command loop for gdb, and we merge
   event-top.c into this file, top.c */
/* static */ void command_loop_marker (void *);

static void while_command (char *, int);

static void if_command (char *, int);

static struct command_line *build_command_line (enum command_control_type,
						char *);

static struct command_line *get_command_line (enum command_control_type,
					      char *);

static void realloc_body_list (struct command_line *, int);

static enum misc_command_type read_next_line (struct command_line **);

static enum command_control_type
recurse_read_control_structure (struct command_line *);

static struct cleanup *setup_user_args (char *);

static char *locate_arg (char *);

static char *insert_args (char *);

static void arg_cleanup (void *);

static void init_main (void);

static void init_cmd_lists (void);

static void float_handler (int);

static void init_signals (void);

#if 0 /* 11/19/04 - JAGae93202-Disabling threadverbose feature */
static void set_threadverbose (char *, int, struct cmd_list_element *);
#endif

static void set_verbose (char *, int, struct cmd_list_element *);

static void show_history (char *, int);

static void set_history (char *, int);

static void set_history_size_command (char *, int, struct cmd_list_element *);

static void set_hp_aries_pa32 (char *, int, struct cmd_list_element *);

static void set_hp_aries_pa64 (char *, int, struct cmd_list_element *);

static void show_commands (char *, int);

static void echo_command (char *, int);

static void pwd_command (char *, int);

static void show_version (char *, int);

static void document_command (char *, int);

static void define_command (char *, int);

static void validate_comname (char *);

static void help_command (char *, int);

static void show_command (char *, int);

static void info_command (char *, int);

static void complete_command (char *, int);

static void do_nothing (int);

static void show_debug (char *, int);

static void set_debug (char *, int);

#ifdef SIGHUP
/* NOTE 1999-04-29: This function will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ int quit_cover (PTR);

static void disconnect (int);
#endif

static void do_restore_instream_cleanup (void *stream);

static struct cleanup *make_cleanup_free_command_lines (struct command_line **);
     void set_endian_little PARAMS ((char *, int));

#ifdef HP_XMODE

/*
 * Some helper functions 
 */

static int xmode_is_env_command (char *command);
static void xmode_record_user_env_command (char *command);
static void xmode_open_record_file (void);

void xmode_launch_other_gdb (char *new_executable, enum xmode_cause reason);
void xmode_save_command_line_args (int argc, char **argv);
int xmode_exec_format_is_different (char *executable);

/* File static variables */

/* 
 * pointer to the temporary file that WE will use to SAVE
 * user state in this gdb session.  It will be opened 
 * as necessary by xmode_open_record_file().
 */

static FILE *xmode_user_env_record = NULL;

/*
 *  name of temporary record file returned by tmpnam().
 */

static char xmode_user_env_record_name[L_tmpnam];

/*
 * Holds the number of command line arguments that we actually saved.
 */

static int  xmode_save_argc;

/*
 * Holds the saved command line arguments passed to this gdb.
 */

static char *xmode_save_argv[100];

/*  Stacey - 
 *  This variable tells us which gdb we are running.  Currently we 
 *  could either be running gdb32, gdb64, an IPF gdb.  For more on
 *  the 'enum file format' enumerator, look in config/ia64
 */

#ifdef HP_IA64
  enum file_format xmode_this_gdb_understands = xmode_ia64;
  extern int debugging_aries;
  extern int debugging_aries64;
  extern void mixed_mode_cleanup (void);
  extern void mixed_mode_free_std_regs (void);
#else /* not HP_IA64 */
#ifdef GDB_TARGET_IS_HPPA_20W
  enum file_format xmode_this_gdb_understands = xmode_pa64;
#else /* not GDB_TARGET_IS_HPPA_20W */
  enum file_format xmode_this_gdb_understands = xmode_pa32;
#endif /* GDB_TARGET_IS_HPPA_20W */
#endif /* HP_IA64 */
#endif /* HP_XMODE */

/* For catching instead of printing errors in call_catch_errors(). */
static char * caught_error_msg = NULL;

/* Default command line prompt.  This is overriden in some configs. */

#ifndef DEFAULT_PROMPT
#define DEFAULT_PROMPT  default_prompt
char default_prompt [16] = "(gdb) ";
#endif

/* Default command line prompt.  This is overriden in some configs. */

#ifndef DEFAULT_PROMPT
#define DEFAULT_PROMPT	default_prompt
char default_prompt [16] = "(gdb) ";
#endif

/* Initialization file name for gdb.  This is overridden in some configs.  */

#ifndef	GDBINIT_FILENAME
#define	GDBINIT_FILENAME	".gdbinit"
#endif
char gdbinit[] = GDBINIT_FILENAME;

int inhibit_gdbinit = 0;

/* If nonzero, and GDB has been configured to be able to use windows,
   attempt to open them upon startup.  */

int use_windows = 0;

#ifdef GDB_TARGET_IS_HPUX
/* HP Wildebeest version string and version number. */

extern char * wdb_version;
extern char * wdb_version_nbr;
#endif

/* Suresh: For thread Bacth RTC to work correctly 
  defined in thread-check.c and infrun.c respectively*/
extern int trace_threads_in_this_run;
extern int batch_thread_rtc_needs_to_exit;

extern char lang_frame_mismatch_warn[];		/* language.c */

/* Flag for whether we want all the "from_tty" gubbish printed.  */

int caution = 1;		/* Default is yes, sigh. */

/* Define all cmd_list_elements.  */

/* Chain containing all defined commands.  */

struct cmd_list_element *cmdlist;

/* Chain containing all defined info subcommands.  */

struct cmd_list_element *infolist;

/* Chain containing all defined enable subcommands. */

struct cmd_list_element *enablelist;

/* Chain containing all defined disable subcommands. */

struct cmd_list_element *disablelist;

/* Chain containing all defined toggle subcommands. */

struct cmd_list_element *togglelist;

/* Chain containing all defined stop subcommands. */

struct cmd_list_element *stoplist;

/* Chain containing all defined delete subcommands. */

struct cmd_list_element *deletelist;

/* Chain containing all defined "enable breakpoint" subcommands. */

struct cmd_list_element *enablebreaklist;

/* Chain containing all defined set subcommands */

struct cmd_list_element *setlist;

/* Chain containing all defined unset subcommands */

struct cmd_list_element *unsetlist;

/* Chain containing all defined show subcommands.  */

struct cmd_list_element *showlist;

/* Chain containing all defined \"set logging\".  */

struct cmd_list_element *setlogginglist;

/* Chain containing all defined \"show logging\".  */

struct cmd_list_element *showlogginglist;


/* Chain containing all defined \"set history\".  */

struct cmd_list_element *sethistlist;

/* Chain containing all defined \"show history\".  */

struct cmd_list_element *showhistlist;

/* Chain containing all defined \"unset history\".  */

struct cmd_list_element *unsethistlist;

/* Chain containing all defined maintenance subcommands. */

struct cmd_list_element *maintenancelist;

/* Chain containing all defined "maintenance info" subcommands. */

struct cmd_list_element *maintenanceinfolist;

/* Chain containing all defined "maintenance print" subcommands. */

struct cmd_list_element *maintenanceprintlist;

struct cmd_list_element *setprintlist;

struct cmd_list_element *showprintlist;

struct cmd_list_element *setdebuglist;

struct cmd_list_element *showdebuglist;

struct cmd_list_element *setchecklist;

struct cmd_list_element *showchecklist;

/* stdio stream that command input is being read from.  Set to stdin normally.
   Set by source_command to the file we are sourcing.  Set to NULL if we are
   executing a user-defined command or interacting via a GUI.  */

FILE *instream;

/* Current working directory.  */

char *current_directory;

/* The directory name is actually stored here (usually).  */
char gdb_dirbuf[1024];

/* gdb_root for alternate root path */
char gdb_root[PATH_MAX + 1];

/* Function to call before reading a command, if nonzero.
   The function receives two args: an input stream,
   and a prompt string.  */

void (*window_hook) (FILE *, char *);

int epoch_interface;
int xgdb_verbose;

/* gdb prints this when reading a command interactively */
static char *gdb_prompt_string;	/* the global prompt string */
extern char *get_prompt (void);	/* access function for prompt string */

/* Buffer used for reading command lines, and the size
   allocated for it so far.  */

char *line;
int linesize = 100;

/* Nonzero if the current command is modified by "server ".  This
   affects things like recording into the command history, commands
   repeating on RETURN, etc.  This is so a user interface (emacs, GUI,
   whatever) can issue its own commands and also send along commands
   from the user, and have the user not notice that the user interface
   is issuing commands too.  */
int server_command;

/* Baud rate specified for talking to serial target systems.  Default
   is left as -1, so targets can choose their own defaults.  */
/* FIXME: This means that "show remotebaud" and gr_files_info can print -1
   or (unsigned int)-1.  This is a Bad User Interface.  */

int baud_rate = -1;

/* Timeout limit for response from target. */

/* The default value has been changed many times over the years.  It 
   was originally 5 seconds.  But that was thought to be a long time 
   to sit and wait, so it was changed to 2 seconds.  That was thought
   to be plenty unless the connection was going through some terminal 
   server or multiplexer or other form of hairy serial connection.

   In mid-1996, remote_timeout was moved from remote.c to top.c and 
   it began being used in other remote-* targets.  It appears that the
   default was changed to 20 seconds at that time, perhaps because the
   Hitachi E7000 ICE didn't always respond in a timely manner.

   But if 5 seconds is a long time to sit and wait for retransmissions,
   20 seconds is far worse.  This demonstrates the difficulty of using 
   a single variable for all protocol timeouts.

   As remote.c is used much more than remote-e7000.c, it was changed 
   back to 2 seconds in 1999. */

int remote_timeout = 2;

/* Non-zero tells remote* modules to output debugging info.  */

int remote_debug = 0;

/* Non-zero means the target is running. Note: this is different from
   saying that there is an active target and we are stopped at a
   breakpoint, for instance. This is a real indicator whether the
   target is off and running, which gdb is doing something else. */
int target_executing = 0;

/* Level of control structure.  */
static int control_level;

/* Variable which is set to 1 when we are running the testsuite */

int hp_wdb_testsuite = 0;

/* Indicates if we want lookup_symbol to use the last cached value 
   0 if we don't want it to and 1 if we want it to. 
   We set it to 0 in execute_command since we have a new command
   and we don't want the last cached value */
      int use_cached_value = 0;

/* Structure for arguments to user defined functions.  */
#define MAXUSERARGS 10
struct user_args
  {
    struct user_args *next;
    struct
      {
	char *arg;
	int len;
      }
    a[MAXUSERARGS];
    int count;
  }
 *user_args;

/* Signal to catch ^Z typed while reading a command: SIGTSTP or SIGCONT.  */

#ifndef STOP_SIGNAL
#ifdef SIGTSTP
#define STOP_SIGNAL SIGTSTP
static void stop_sig (int);
#endif
#endif

/* Some System V have job control but not sigsetmask(). */
#if !defined (HAVE_SIGSETMASK)
#if !defined (USG)
#define HAVE_SIGSETMASK 1
#else
#define HAVE_SIGSETMASK 0
#endif
#endif

#if 0 == (HAVE_SIGSETMASK)
#define sigsetmask(n)
#endif

/* Hooks for alternate command interfaces.  */

/* Called after most modules have been initialized, but before taking users
   command file.  */

void (*init_ui_hook) (char *argv0);

/* This hook is called from within gdb's many mini-event loops which could
   steal control from a real user interface's event loop. It returns
   non-zero if the user is requesting a detach, zero otherwise. */

int (*ui_loop_hook) (int);

/* Called instead of command_loop at top level.  Can be invoked via
   return_to_top_level.  */

void (*command_loop_hook) (void);


/* Called from print_frame_info to list the line we stopped in.  */

void (*print_frame_info_listing_hook) (struct symtab * s, int line,
				       int stopline, int noerror);
/* Replaces most of query.  */

int (*query_hook) (const char *, va_list);

/* Replaces most of warning.  */

void (*warning_hook) (const char *, va_list);

/* These three functions support getting lines of text from the user.  They
   are used in sequence.  First readline_begin_hook is called with a text
   string that might be (for example) a message for the user to type in a
   sequence of commands to be executed at a breakpoint.  If this function
   calls back to a GUI, it might take this opportunity to pop up a text
   interaction window with this message.  Next, readline_hook is called
   with a prompt that is emitted prior to collecting the user input.
   It can be called multiple times.  Finally, readline_end_hook is called
   to notify the GUI that we are done with the interaction window and it
   can close it. */

void (*readline_begin_hook) (char *, ...);
char *(*readline_hook) (char *);
void (*readline_end_hook) (void);

/* Called as appropriate to notify the interface of the specified breakpoint
   conditions.  */

void (*create_breakpoint_hook) (struct breakpoint * bpt);
void (*delete_breakpoint_hook) (struct breakpoint * bpt);
void (*modify_breakpoint_hook) (struct breakpoint * bpt);

/* Called as appropriate to notify the interface that we have attached
   to or detached from an already running process. */

void (*attach_hook) (void);
void (*detach_hook) (void);

/* Called during long calculations to allow GUI to repair window damage, and to
   check for stop buttons, etc... */

void (*interactive_hook) (void);

/* Called when the registers have changed, as a hint to a GUI
   to minimize window update. */

void (*registers_changed_hook) (void);

/* Tell the GUI someone changed the register REGNO. -1 means
   that the caller does not know which register changed or
   that several registers have changed (see value_assign). */
void (*register_changed_hook) (int regno);

/* Tell the GUI someone changed LEN bytes of memory at ADDR */
void (*memory_changed_hook) (CORE_ADDR addr, int len);

/* Called when going to wait for the target.  Usually allows the GUI to run
   while waiting for target events.  */

int (*target_wait_hook) (int pid, struct target_waitstatus * status);

/* Used by UI as a wrapper around command execution.  May do various things
   like enabling/disabling buttons, etc...  */

void (*call_command_hook) (struct cmd_list_element * c, char *cmd,
			   int from_tty);

/* Called after a `set' command has finished.  Is only run if the
   `set' command succeeded.  */

void (*set_hook) (struct cmd_list_element * c);

/* Called when the current thread changes.  Argument is thread id.  */

void (*context_hook) (int id);

/* Takes control from error ().  Typically used to prevent longjmps out of the
   middle of the GUI.  Usually used in conjunction with a catch routine.  */

     NORETURN void (*error_hook) PARAMS ((char *)) ATTR_NORETURN;

/* Used by disassemble_command to pass disassembly to buffer
   rather than stdout. Useful for a GUI. */

void (*disassemble_command_hook) (char *);

#ifdef HP_XMODE
/* Called from xmode_launch_other_gdb(). Lets client know
   that another gdb is about to be invoked. */

void (*xmode_hook) (FILE *);
#endif


/* Where to go for return_to_top_level.  */
SIGJMP_BUF *catch_return;

/* Return for reason REASON to the nearest containing catch_errors().  */

NORETURN void
return_to_top_level (enum return_reason reason)
{
  quit_flag = 0;
  immediate_quit = 0;

  /* Perhaps it would be cleaner to do this via the cleanup chain (not sure
     I can think of a reason why that is vital, though).  */
  bpstat_clear_actions (stop_bpstat);	/* Clear queued breakpoint commands */

#ifdef HP_IA64
  /* jini: 060511: Reset the method used to print assembly instructions.
     This might have been overridden for mixed mode PA libraries */
  tm_print_insn = print_insn_ia64;
#endif

  disable_current_display ();
  do_cleanups (ALL_CLEANUPS);
  if (event_loop_p && target_can_async_p () && !target_executing)
    do_exec_cleanups (ALL_CLEANUPS);
  if (event_loop_p && sync_execution)
    do_exec_error_cleanups (ALL_CLEANUPS);

  if (annotation_level > 1)
    switch (reason)
      {
      case RETURN_QUIT:
	annotate_quit ();
	break;
      case RETURN_ERROR:
	annotate_error ();
	break;
      }

  /* Jump to the containing catch_errors() call, communicating REASON
     to that call via setjmp's return value.  Note that REASON can't
     be zero, by definition in defs.h. */

  (NORETURN void) SIGLONGJMP (*catch_return, (int) reason);
}

/* Call FUNC with arg ARGS, catching any errors.  If there is no
   error, return the value returned by FUNC.  If there is an error,
   print ERRSTRING, print the specific error message, then return
   zero.

   Must not be called with immediate_quit in effect (bad things might
   happen, say we got a signal in the middle of a memcpy to quit_return).
   This is an OK restriction; with very few exceptions immediate_quit can
   be replaced by judicious use of QUIT.

   MASK specifies what to catch; it is normally set to
   RETURN_MASK_ALL, if for no other reason than that the code which
   calls catch_errors might not be set up to deal with a quit which
   isn't caught.  But if the code can deal with it, it generally
   should be RETURN_MASK_ERROR, unless for some reason it is more
   useful to abort only the portion of the operation inside the
   catch_errors.  Note that quit should return to the command line
   fairly quickly, even if some further processing is being done.  */

/* MAYBE: cagney/1999-11-05: catch_errors() in conjunction with
   error() et.al. could maintain a set of flags that indicate the the
   current state of each of the longjmp buffers.  This would give the
   longjmp code the chance to detect a longjmp botch (before it gets
   to longjmperror()).  Prior to 1999-11-05 this wasn't possible as
   code also randomly used a SET_TOP_LEVEL macro that directly
   initialize the longjmp buffers. */

/* MAYBE: cagney/1999-11-05: Should the catch_erros and cleanups code
   be consolidated into a single file instead of being distributed
   between utils.c and top.c? */

int
catch_errors (catch_errors_ftype *func, PTR args, char *errstring,
     	      return_mask in_mask)
{
  volatile SIGJMP_BUF *saved_catch;
  SIGJMP_BUF catch;
  int val = 0; /* initialize for compiler warning */
  static catch_errors_ftype *saved_func;
  static PTR saved_args;
  volatile return_mask mask = in_mask;
  volatile struct cleanup *saved_cleanup_chain;
  static char *saved_error_pre_print;
  static char *saved_quit_pre_print;

  /* Return value from SIGSETJMP(): enum return_reason if error or
     quit caught, 0 otherwise. */
  int caught;

  /* Override error/quit messages during FUNC. */

  saved_error_pre_print = error_pre_print;
  saved_quit_pre_print = quit_pre_print;
  saved_func = func;
  saved_args = args;

  if (mask & RETURN_MASK_ERROR)
    error_pre_print = errstring;
  if (mask & RETURN_MASK_QUIT)
    quit_pre_print = errstring;

  /* Prevent error/quit during FUNC from calling cleanups established
     prior to here. */

  saved_cleanup_chain = save_cleanups ();

  /* Call FUNC, catching error/quit events. */

  saved_catch = (volatile jmp_buf *) catch_return;
  catch_return = &catch;
  caught = SIGSETJMP (catch);
  if (!caught)
    val = (*saved_func) ((void *) saved_args);
  catch_return = (jmp_buf *) saved_catch;

  /* FIXME: cagney/1999-11-05: A correct FUNC implementaton will
     clean things up (restoring the cleanup chain) to the state they
     were just prior to the call.  Unfortunatly, many FUNC's are not
     that well behaved.  This could be fixed by adding either a
     do_cleanups call (to cover the problem) or an assertion check to
     detect bad FUNCs code. */

  /* Restore the cleanup chain and error/quit messages to their
     original states. */

  restore_cleanups ((struct cleanup *) saved_cleanup_chain);

  if (mask & RETURN_MASK_QUIT)
    quit_pre_print = (char *) saved_quit_pre_print;
  if (mask & RETURN_MASK_ERROR)
    error_pre_print = (char *) saved_error_pre_print;

  /* Return normally if no error/quit event occurred. */

  if (!caught)
    return val;

  /* If the caller didn't request that the event be caught, relay the
     event to the next containing catch_errors(). */

  if (!(mask & RETURN_MASK (caught)))
    return_to_top_level (caught);

  /* Tell the caller that an event was caught.

     FIXME: nsd/2000-02-22: When MASK is RETURN_MASK_ALL, the caller
     can't tell what type of event occurred.

     A possible fix is to add a new interface, catch_event(), that
     returns enum return_reason after catching an error or a quit.

     When returning normally, i.e. without catching an error or a
     quit, catch_event() could return RETURN_NORMAL, which would be
     added to enum return_reason.  FUNC would return information
     exclusively via ARGS.

     Alternatively, normal catch_event() could return FUNC's return
     value.  The caller would need to be aware of potential overlap
     with enum return_reason, which could be publicly restricted to
     negative values to simplify return value processing in FUNC and
     in the caller. */

  return 0;
}

struct captured_command_args
  {
    catch_command_errors_ftype *command;
    char *arg;
    int from_tty;
  };

static int
do_captured_command (void *data)
{
  struct captured_command_args *context = data;
  context->command (context->arg, context->from_tty);
  /* FIXME: cagney/1999-11-07: Technically this do_cleanups() call
     isn't needed.  Instead an assertion check could be made that
     simply confirmed that the called function correctly cleaned up
     after its self.  Unfortunatly, old code (prior to 1999-11-04) in
     main.c was calling SET_TOP_LEVEL(), calling the command function,
     and then *always* calling do_cleanups().  For the moment we
     remain ``bug compatible'' with that old code..  */
  do_cleanups (ALL_CLEANUPS);
  return 1;
}

int
catch_command_errors (catch_command_errors_ftype * command,
		      char *arg, int from_tty, return_mask mask)
{
  struct captured_command_args args;
  args.command = command;
  args.arg = arg;
  args.from_tty = from_tty;
  return catch_errors (do_captured_command, &args, "", mask);
}

/* Set error_hook to this function to catch error messages. */
static void 
catch_error_msg (char *msg)
{
  if (caught_error_msg != NULL)
    free (caught_error_msg);
  caught_error_msg = (char *) xmalloc (strlen (msg) + 1);
  strcpy (caught_error_msg, msg);
  (NORETURN void) _longjmp (*catch_return, 1);
}

/* Call a gdb routine and catch error returns.

   IN func     routine to call.
   OUT err_msg if called function results in error, err_msg will
               point to resulting error message; else, NULL.
   IN argcount number of arguments to the routine to be called.
   IN mask     RETURN_MASK_ALL or RETURN_MASK_ERROR (see commets for catch_errors()).
   IN arg1,... list of arguments to routine that is to be called. */

long
call_catch_errors (void *func, char **err_msg, int argcount,
                   return_mask mask, void * arg1, ...)
{
#define MAXFUNCARGS 4
  SIGJMP_BUF saved_error;
  SIGJMP_BUF saved_quit;
  SIGJMP_BUF tmp_jmp;
  long val;
  va_list ap;
  int i;

  /* "static" only to suppress aCC6 uninit warning */
  static struct cleanup *saved_cleanup_chain;
  static void *arglist[MAXFUNCARGS];
  static void (*saved_error_hook) (char *);

  if (argcount > MAXFUNCARGS)
    {
      *err_msg ="Internal Error: Exceeded max args for call_catch_errors().";
      return -1;
    }

  *err_msg = NULL;
  if (caught_error_msg != NULL)
    {
      free (caught_error_msg);
      caught_error_msg = NULL;
    }
  saved_error_hook = error_hook;
  error_hook = catch_error_msg;
  saved_cleanup_chain = save_cleanups ();

  for (i=0; i < MAXFUNCARGS; i++)
    arglist[i] = NULL;

  if (argcount)
    {
      va_start (ap, arg1);
      arglist[0] = arg1;
      for (i = 1; i < argcount; i++)
        arglist[i] = va_arg (ap, void *);
      va_end (ap);
    }
  if (mask & RETURN_MASK_ERROR)
    memcpy ((char *)saved_error, (char *)*catch_return, sizeof (SIGJMP_BUF));
  if (mask & RETURN_MASK_QUIT)
    memcpy (saved_quit, *catch_return, sizeof (SIGJMP_BUF));

  if (SIGSETJMP (tmp_jmp) == 0)
    {
      if (mask & RETURN_MASK_ERROR)
	memcpy (*catch_return, tmp_jmp, sizeof (SIGJMP_BUF));
      if (mask & RETURN_MASK_QUIT)
	memcpy (*catch_return, tmp_jmp, sizeof (SIGJMP_BUF));
      switch (argcount)
        {
        case 0:
          val = ((long (*) ())func) ();
          break;
        case 1:
          val = ((long (*)(void*)) func) (arglist[0]);
          break;
        case 2:
          val = ((long (*)(void*, void*)) func) (arglist[0], arglist[1]);
          break;
        case 3:
          val = ((long (*)(void*, void*, void*)) func) (arglist[0],
                                                       arglist[1], arglist[2]);
          break;
        case 4:
          val = ((long (*)(void*, void*, void*, void*)) func) (arglist[0], arglist[1],
                                                              arglist[2], arglist[3]);
          break;
        default:
          *err_msg = "Internal Error: Too many args for call_catch_errors().";
          return -1;
        }
    }
  else
    val = 0;

  *err_msg = caught_error_msg;
  error_hook = saved_error_hook;
  restore_cleanups (saved_cleanup_chain);

  if (mask & RETURN_MASK_ERROR)
    memcpy (*catch_return, saved_error, sizeof (SIGJMP_BUF));
  if (mask & RETURN_MASK_QUIT)
    memcpy (*catch_return, saved_quit, sizeof (SIGJMP_BUF));

  return val;
}

/* Handler for SIGHUP.  */

#ifdef SIGHUP
static void
disconnect (int signo)
{
  catch_errors (quit_cover, NULL,
	      "Could not kill the program being debugged", RETURN_MASK_ALL);
  signal (SIGHUP, SIG_DFL);
  kill (getpid (), SIGHUP);
}

/* Just a little helper function for disconnect().  */

/* NOTE 1999-04-29: This function will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ int
quit_cover (PTR s)
{
  caution = 0;			/* Throw caution to the wind -- we're exiting.
				   This prevents asking the user dumb questions.  */
  quit_command ((char *) 0, 0);
  return 0;
}
#endif /* defined SIGHUP */

/* Line number we are currently in in a file which is being sourced.  */
/* NOTE 1999-04-29: This variable will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ int source_line_number;

/* Name of the file we are sourcing.  */
/* NOTE 1999-04-29: This variable will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ char *source_file_name;

/* Buffer containing the error_pre_print used by the source stuff.
   Malloc'd.  */
/* NOTE 1999-04-29: This variable will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ char *source_error;
static int source_error_allocated;

/* Something to glom on to the start of error_pre_print if source_file_name
   is set.  */
/* NOTE 1999-04-29: This variable will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ char *source_pre_error;

/* Clean up on error during a "source" command (or execution of a
   user-defined command).  */

static void
do_restore_instream_cleanup (void *stream)
{
  /* Restore the previous input stream.  */
  instream = stream;
}

/* Read commands from STREAM.  */
void
read_command_file (FILE *stream)
{
  struct cleanup *cleanups;

  cleanups = make_cleanup (do_restore_instream_cleanup, instream);
  instream = stream;
  command_loop ();
  do_cleanups (cleanups);
}

extern void init_proc (void);

void (*pre_init_ui_hook) (void);

#ifdef __MSDOS__
void
do_chdir_cleanup (void *old_dir)
{
  chdir (old_dir);
  free (old_dir);
}
#endif

void
gdb_init (char *argv0)
{
  if (pre_init_ui_hook)
    pre_init_ui_hook ();

  /* Check if the environment variable which denotes whether
     or not we are running the testsuite is set to true.  If so,
     set the external variable hp_wdb_testsuite. */

  if (getenv("HP_WDB_TESTSUITE") != NULL)
    hp_wdb_testsuite = 1;

  /* Run the init function of each source file */

  getcwd (gdb_dirbuf, sizeof (gdb_dirbuf));

  /* srikanth, on HPUX 11.00 when NFS and symbolic links and *something
     else* is in the picture, getcwd fails for some mysterious reason. In
     this case, "." ought to be a better a result than ""
  */

  if (gdb_dirbuf[0] == 0)
    strcpy(gdb_dirbuf, ".");

  current_directory = gdb_dirbuf;

#ifdef __MSDOS__
  /* Make sure we return to the original directory upon exit, come
     what may, since the OS doesn't do that for us.  */
  make_final_cleanup (do_chdir_cleanup, xstrdup (current_directory));
#endif

  init_cmd_lists ();		/* This needs to be done first */
  initialize_targets ();	/* Setup target_terminal macros for utils.c */
  initialize_utils ();		/* Make errors and warnings possible */
  initialize_all_files ();
  initialize_current_architecture ();
  init_main ();			/* But that omits this file!  Do it now */

  /* The signal handling mechanism is different depending whether or
     not the async version is run. NOTE: in the future we plan to make
     the event loop be the default engine of gdb, and this difference
     will disappear. */
  if (event_loop_p)
    async_init_signals ();
  else
    init_signals ();

  /* We need a default language for parsing expressions, so simple things like
     "set width 0" won't fail if no language is explicitly set in a config file
     or implicitly set by reading an executable during startup. */
  set_language (language_c);
  expected_language = current_language;		/* don't warn about the change.  */

#ifdef UI_OUT
  /* Install the default UI */
  uiout = cli_out_new (gdb_stdout);
#endif

#ifdef UI_OUT
  /* All the interpreters should have had a look at things by now.
     Initialize the selected interpreter. */
  if (interpreter_p && !init_ui_hook)
    {
      fprintf_unfiltered (gdb_stderr, "Interpreter `%s' unrecognized.\n",
			  interpreter_p);
      exit (1);
    }
#endif

  if (init_ui_hook)
    init_ui_hook (argv0);
}

/* Allocate, initialize a new command line structure for one of the
   control commands (if/while).  */

static struct command_line *
build_command_line (enum command_control_type type, char *args)
{
  struct command_line *cmd;

  if (args == NULL)
    error ("if/while commands require arguments.\n");

  cmd = (struct command_line *) xmalloc (sizeof (struct command_line));
  cmd->next = NULL;
  cmd->control_type = type;

  cmd->body_count = 1;
  cmd->body_list
    = (struct command_line **) xmalloc (sizeof (struct command_line *)
					* cmd->body_count);
  memset (cmd->body_list, 0, sizeof (struct command_line *) * cmd->body_count);
  cmd->line = savestring (args, (int) strlen (args));
  return cmd;
}

/* Build and return a new command structure for the control commands
   such as "if" and "while".  */

static struct command_line *
get_command_line (enum command_control_type type, char *arg)
{
  struct command_line *cmd;
  struct cleanup *old_chain = NULL;

  /* Allocate and build a new command line structure.  */
  cmd = build_command_line (type, arg);

  old_chain = make_cleanup_free_command_lines (&cmd);

  /* Read in the body of this command.  */
  if (recurse_read_control_structure (cmd) == invalid_control)
    {
      warning ("error reading in control structure\n");
      do_cleanups (old_chain);
      return NULL;
    }

  discard_cleanups (old_chain);
  return cmd;
}

/* Recursively print a command (including full control structures).  */
#ifdef UI_OUT
void
print_command_lines (struct ui_out *uiout, struct command_line *cmd, unsigned int depth)
{
  struct command_line *list;

  list = cmd;
  while (list)
    {

      if (depth)
	ui_out_spaces (uiout, 2 * depth);

      /* A simple command, print it and continue.  */
      if (list->control_type == simple_control)
	{
	  ui_out_field_string (uiout, NULL, list->line);
	  ui_out_text (uiout, "\n");
	  list = list->next;
	  continue;
	}

      /* loop_continue to jump to the start of a while loop, print it
         and continue. */
      if (list->control_type == continue_control)
	{
	  ui_out_field_string (uiout, NULL, "loop_continue");
	  ui_out_text (uiout, "\n");
	  list = list->next;
	  continue;
	}

      /* loop_break to break out of a while loop, print it and continue.  */
      if (list->control_type == break_control)
	{
	  ui_out_field_string (uiout, NULL, "loop_break");
	  ui_out_text (uiout, "\n");
	  list = list->next;
	  continue;
	}

      /* A while command.  Recursively print its subcommands and continue.  */
      if (list->control_type == while_control)
	{
	  ui_out_text (uiout, "while ");
	  ui_out_field_fmt (uiout, NULL, "while %s", list->line);
	  ui_out_text (uiout, "\n");
	  print_command_lines (uiout, *list->body_list, depth + 1);
	  ui_out_field_string (uiout, NULL, "end");
	  if (depth)
	    ui_out_spaces (uiout, 2 * depth);
	  ui_out_text (uiout, "end\n");
	  list = list->next;
	  continue;
	}

      /* An if command.  Recursively print both arms before continueing.  */
      if (list->control_type == if_control)
	{
	  ui_out_text (uiout, "if ");
	  ui_out_field_fmt (uiout, NULL, "if %s", list->line);
	  ui_out_text (uiout, "\n");
	  /* The true arm. */
	  print_command_lines (uiout, list->body_list[0], depth + 1);

	  /* Show the false arm if it exists.  */
	  if (list->body_count == 2)
	    {
	      if (depth)
		ui_out_spaces (uiout, 2 * depth);
	      ui_out_field_string (uiout, NULL, "else");
	      ui_out_text (uiout, "else\n");
	      print_command_lines (uiout, list->body_list[1], depth + 1);
	    }

	  ui_out_field_string (uiout, NULL, "end");
	  if (depth)
	    ui_out_spaces (uiout, 2 * depth);
	  ui_out_text (uiout, "end\n");
	  list = list->next;
	  continue;
	}

      /* ignore illegal command type and try next */
      list = list->next;
    }				/* while (list) */
}
#else
void
print_command_line (struct command_line *cmd, unsigned int depth, struct ui_file *stream)
{
  unsigned int i;

  if (depth)
    {
      for (i = 0; i < depth; i++)
	fputs_filtered ("  ", stream);
    }

  /* A simple command, print it and return.  */
  if (cmd->control_type == simple_control)
    {
      fputs_filtered (cmd->line, stream);
      fputs_filtered ("\n", stream);
      return;
    }

  /* loop_continue to jump to the start of a while loop, print it
     and return. */
  if (cmd->control_type == continue_control)
    {
      fputs_filtered ("loop_continue\n", stream);
      return;
    }

  /* loop_break to break out of a while loop, print it and return.  */
  if (cmd->control_type == break_control)
    {
      fputs_filtered ("loop_break\n", stream);
      return;
    }

  /* A while command.  Recursively print its subcommands before returning.  */
  if (cmd->control_type == while_control)
    {
      struct command_line *list;
      fputs_filtered ("while ", stream);
      fputs_filtered (cmd->line, stream);
      fputs_filtered ("\n", stream);
      list = *cmd->body_list;
      while (list)
	{
	  print_command_line (list, depth + 1, stream);
	  list = list->next;
	}
    }

  /* An if command.  Recursively print both arms before returning.  */
  if (cmd->control_type == if_control)
    {
      fputs_filtered ("if ", stream);
      fputs_filtered (cmd->line, stream);
      fputs_filtered ("\n", stream);
      /* The true arm. */
      print_command_line (cmd->body_list[0], depth + 1, stream);

      /* Show the false arm if it exists.  */
      if (cmd->body_count == 2)
	{
	  if (depth)
	    {
	      for (i = 0; i < depth; i++)
		fputs_filtered ("  ", stream);
	    }
	  fputs_filtered ("else\n", stream);
	  print_command_line (cmd->body_list[1], depth + 1, stream);
	}
      if (depth)
	{
	  for (i = 0; i < depth; i++)
	    fputs_filtered ("  ", stream);
	}
      fputs_filtered ("end\n", stream);
    }
}
#endif

/* Execute the command in CMD.  */

enum command_control_type
execute_control_command (struct command_line *cmd)
{
  struct expression *expr;
  struct command_line *current;
  struct cleanup *old_chain = 0;
  value_ptr val;
  value_ptr val_mark;
  int loop;
  enum command_control_type ret;
  char *new_line;

  switch (cmd->control_type)
    {
    case simple_control:
      /* A simple command, execute it and return.  */
      new_line = insert_args (cmd->line);
      if (!new_line)
	return invalid_control;
      old_chain = make_cleanup (free_current_contents, &new_line);
      execute_command (new_line, 0);
      /* JYG: PURIFY COMMENT:
	 This could be a 'free memory read' error if cmd proceeds
	 the inferior and if the inferior deletes the wpt / bpt
	 associated with the command.  However it's hard to fix! */
      ret = cmd->control_type;
      break;

    case continue_control:
    case break_control:
      /* Return for "continue", and "break" so we can either
         continue the loop at the top, or break out.  */
      ret = cmd->control_type;
      break;

    case while_control:
      {
	/* Parse the loop control expression for the while statement.  */
	new_line = insert_args (cmd->line);
	if (!new_line)
	  return invalid_control;
	old_chain = make_cleanup (free_current_contents, &new_line);
	expr = parse_expression (new_line);
	make_cleanup (free_current_contents, &expr);

	ret = simple_control;
	loop = 1;

	/* Keep iterating so long as the expression is true.  */
	while (loop == 1)
	  {
	    int cond_result;

	    QUIT;

	    /* Evaluate the expression.  */
	    val_mark = value_mark ();
	    val = evaluate_expression (expr);
	    cond_result = value_true (val);
	    value_free_to_mark (val_mark);

	    /* If the value is false, then break out of the loop.  */
	    if (!cond_result)
	      break;

	    /* Execute the body of the while statement.  */
	    current = *cmd->body_list;
	    while (current)
	      {
		ret = execute_control_command (current);

		/* If we got an error, or a "break" command, then stop
		   looping.  */
		if (ret == invalid_control || ret == break_control)
		  {
		    loop = 0;
		    break;
		  }

		/* If we got a "continue" command, then restart the loop
		   at this point.  */
		if (ret == continue_control)
		  break;

		/* Get the next statement.  */
		current = current->next;
	      }
	  }

	/* Reset RET so that we don't recurse the break all the way down.  */
	if (ret == break_control)
	  ret = simple_control;

	break;
      }

    case if_control:
      {
	new_line = insert_args (cmd->line);
	if (!new_line)
	  return invalid_control;
	old_chain = make_cleanup (free_current_contents, &new_line);
	/* Parse the conditional for the if statement.  */
	expr = parse_expression (new_line);
	make_cleanup (free_current_contents, &expr);

	current = NULL;
	ret = simple_control;

	/* Evaluate the conditional.  */
	val_mark = value_mark ();
	val = evaluate_expression (expr);

	/* Choose which arm to take commands from based on the value of the
	   conditional expression.  */
	if (value_true (val))
	  current = *cmd->body_list;
	else if (cmd->body_count == 2)
	  current = *(cmd->body_list + 1);
	value_free_to_mark (val_mark);

	/* Execute commands in the given arm.  */
	while (current)
	  {
	    ret = execute_control_command (current);

	    /* If we got an error, get out.  */
	    if (ret != simple_control)
	      break;

	    /* Get the next statement in the body.  */
	    current = current->next;
	  }

	break;
      }

    default:
      warning ("Invalid control type in command structure.");
      return invalid_control;
    }

  if (old_chain)
    do_cleanups (old_chain);

  return ret;
}

/* "while" command support.  Executes a body of statements while the
   loop condition is nonzero.  */

static void
while_command (char *arg, int from_tty)
{
  struct command_line *command = NULL;

  control_level = 1;
  command = get_command_line (while_control, arg);
  control_level = 0;

  if (command == NULL)
    return;

  execute_control_command (command);
  free_command_lines (&command);
}

/* "if" command support.  Execute either the true or false arm depending
   on the value of the if conditional.  */

static void
if_command (char *arg, int from_tty)
{
  struct command_line *command = NULL;

  control_level = 1;
  command = get_command_line (if_control, arg);
  control_level = 0;

  if (command == NULL)
    return;

  execute_control_command (command);
  free_command_lines (&command);
}

/* Cleanup */
static void
arg_cleanup (void *ignore)
{
  struct user_args *oargs = user_args;
  if (!user_args)
    internal_error ("Internal error, arg_cleanup called with no user args.\n");

  user_args = user_args->next;
  free (oargs);
}

/* Bind the incomming arguments for a user defined command to
   $arg0, $arg1 ... $argMAXUSERARGS.  */

static struct cleanup *
setup_user_args (char *p)
{
  struct user_args *args;
  struct cleanup *old_chain;
  unsigned int arg_count = 0;

  args = (struct user_args *) xmalloc (sizeof (struct user_args));
  memset (args, 0, sizeof (struct user_args));

  args->next = user_args;
  user_args = args;

  old_chain = make_cleanup (arg_cleanup, 0/*ignored*/);

  if (p == NULL)
    return old_chain;

  while (*p)
    {
      char *start_arg;
      int squote = 0;
      int dquote = 0;
      int bsquote = 0;

      if (arg_count >= MAXUSERARGS)
	{
	  error ("user defined function may only have %d arguments.\n",
		 MAXUSERARGS);
	  return old_chain;
	}

      /* Strip whitespace.  */
      while (*p == ' ' || *p == '\t')
	p++;

      /* P now points to an argument.  */
      start_arg = p;
      user_args->a[arg_count].arg = p;

      /* Get to the end of this argument.  */
      while (*p)
	{
	  if (((*p == ' ' || *p == '\t')) && !squote && !dquote && !bsquote)
	    break;
	  else
	    {
	      if (bsquote)
		bsquote = 0;
	      else if (*p == '\\')
		bsquote = 1;
	      else if (squote)
		{
		  if (*p == '\'')
		    squote = 0;
		}
	      else if (dquote)
		{
		  if (*p == '"')
		    dquote = 0;
		}
	      else
		{
		  if (*p == '\'')
		    squote = 1;
		  else if (*p == '"')
		    dquote = 1;
		}
	      p++;
	    }
	}

      user_args->a[arg_count].len = (int) (p - start_arg);
      arg_count++;
      user_args->count++;
    }
  return old_chain;
}

/* Given character string P, return a point to the first argument ($arg),
   or NULL if P contains no arguments.  */

static char *
locate_arg (char *p)
{
  while ((p = strchr (p, '$')))
    {
      if (strncmp (p, "$arg", 4) == 0 && isdigit (p[4]))
	return p;
      p++;
    }
  return NULL;
}

/* Insert the user defined arguments stored in user_arg into the $arg
   arguments found in line, with the updated copy being placed into nline.  */

static char *
insert_args (char *line)
{
  char *p, *save_line, *new_line;
  unsigned len, i;

  /* First we need to know how much memory to allocate for the new line.  */
  save_line = line;
  len = 0;
  while ((p = locate_arg (line)))
    {
      len += p - line;
      i = p[4] - '0';

      if (i >= user_args->count)
	{
	  error ("Missing argument %d in user function.\n", i);
	  return NULL;
	}
      len += user_args->a[i].len;
      line = p + 5;
    }

  /* Don't forget the tail.  */
  len += strlen (line);

  /* Allocate space for the new line and fill it in.  */
  new_line = (char *) xmalloc (len + 1);
  if (new_line == NULL)
    return NULL;

  /* Restore pointer to beginning of old line.  */
  line = save_line;

  /* Save pointer to beginning of new line.  */
  save_line = new_line;

  while ((p = locate_arg (line)))
    {
      int i, len;

      memcpy (new_line, line, p - line);
      new_line += p - line;
      i = p[4] - '0';

      len = user_args->a[i].len;
      if (len)
	{
	  memcpy (new_line, user_args->a[i].arg, len);
	  new_line += len;
	}
      line = p + 5;
    }
  /* Don't forget the tail.  */
  strcpy (new_line, line);

  /* Return a pointer to the beginning of the new line.  */
  return save_line;
}

void
execute_user_command (struct cmd_list_element *c, char *args)
{
  register struct command_line *cmdlines;
  struct cleanup *old_chain;
  enum command_control_type ret;

  old_chain = setup_user_args (args);

  cmdlines = c->user_commands;
  if (cmdlines == 0)
    /* Null command */
    return;

  /* Set the instream to 0, indicating execution of a
     user-defined function.  */
  old_chain = make_cleanup (do_restore_instream_cleanup, instream);
  instream = (FILE *) 0;
  while (cmdlines)
    {
      ret = execute_control_command (cmdlines);
      if (ret != simple_control && ret != break_control)
	{
	  warning ("Error in control structure.\n");
	  break;
	}
      cmdlines = cmdlines->next;
    }
  do_cleanups (old_chain);
}

/* Execute the line P as a command.
   Pass FROM_TTY as second argument to the defining function.  */

void
execute_command (char *p, int from_tty)
{
  register struct cmd_list_element *c;
  register enum language flang;
  static int warned = 0;
  char *line;
  /* FIXME: These should really be in an appropriate header file */
extern void serial_log_command (const char *);

#ifdef HP_XMODE

  /*
   * if we are capable of performing cross mode debugging,
   * save the command that we got in.  We need to save it 
   * because p will be destroyed by the following processing.
   */

  char *save_line = NULL;

  save_line = (char *) alloca (strlen (p) + 1);
  strcpy (save_line,p);

#endif /* HP_XMODE */

  free_all_values ();
  use_cached_value = 0;

  /* Force cleanup of any alloca areas if using C alloca instead of
     a builtin alloca.  */
  alloca (0);

  /* This can happen when command_line_input hits end of file.  */
  if (p == NULL)
    return;

  serial_log_command (p);

  while (*p == ' ' || *p == '\t')
    p++;
  if (*p)
    {
      char *arg;
      line = p;

      c = lookup_cmd (&p, cmdlist, "", 0, 1);

      /* If the target is running, we allow only a limited set of
         commands. */
      if (event_loop_p && target_can_async_p () && target_executing)
	if (!strcmp (c->name, "help")
	    && !strcmp (c->name, "pwd")
	    && !strcmp (c->name, "show")
	    && !strcmp (c->name, "stop"))
	  error ("Cannot execute this command while the target is running.");

      /* Pass null arg rather than an empty one.  */
      arg = *p ? p : 0;

      /* Clear off trailing whitespace, except for set and complete command.  */
      if (arg && c->type != set_cmd && c->function.cfunc != complete_command)
	{
	  p = arg + strlen (arg) - 1;
	  while (p >= arg && (*p == ' ' || *p == '\t'))
	    p--;
	  *(p + 1) = '\0';
	}

      /* If this command has been hooked, run the hook first. */
      if (c->hook)
	execute_user_command (c->hook, (char *) 0);

      if (c->flags & DEPRECATED_WARN_USER)
	deprecated_cmd_warning (&line);
 
      if (c->class == class_user)
	execute_user_command (c, arg);
      else if (c->type == set_cmd || c->type == show_cmd)
	do_setshow_command (arg, from_tty & caution, c);
      else if (c->function.cfunc == NO_FUNCTION)
	error ("That is not a command, just a help topic.");
      else if (call_command_hook)
	call_command_hook (c, arg, from_tty & caution);
      else
	(*c->function.cfunc) (arg, from_tty & caution);
    }

  /* Tell the user if the language has changed (except first time).  */
  if (current_language != expected_language)
    {
      if (language_mode == language_mode_auto)
	{
	  language_info (1);	/* Print what changed.  */
	}
      warned = 0;
    }

  /* Warn the user if the working language does not match the
     language of the current frame.  Only warn the user if we are
     actually running the program, i.e. there is a stack. */
  /* FIXME:  This should be cacheing the frame and only running when
     the frame changes.  */

  if (target_has_stack)
    {
      flang = get_frame_language ();
      if (!warned
	  && flang != language_unknown
	  && flang != current_language->la_language)
	{
	  printf_filtered ("%s\n", lang_frame_mismatch_warn);
	  warned = 1;
	}
    }

#ifdef HP_XMODE

  /* 
   * If we are capable of performing cross mode debugging,
   * check to see if the command coming into this routine is
   * a command to set some aspect of the user environment.  If
   * so, record the command in the temporary file.
   */

  xmode_record_user_env_command (save_line);
#endif /* HP_XMODE */
}

/* ARGSUSED */
/* NOTE 1999-04-29: This function will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ void
command_loop_marker (void *foo)
{
}

/* Read commands from `instream' and execute them
   until end of file or error reading instream.  */

void
command_loop ()
{
  struct cleanup *old_chain;
  char *command;
  int stdin_is_tty = ISATTY (stdin);
  long time_at_cmd_start;
#ifdef HAVE_SBRK
  long space_at_cmd_start = 0;
#endif
  extern int display_time;
  extern int display_space;

  while (instream && !feof (instream))
    {
#if defined(TUI)
      extern int insert_mode;
#endif
      if (window_hook && instream == stdin)
	(*window_hook) (instream, get_prompt ());

      quit_flag = 0;
      if (instream == stdin && stdin_is_tty)
	reinitialize_more_filter ();

      if (nimbus_version)
	reinitialize_more_filter ();

      old_chain = make_cleanup (command_loop_marker, 0);

#if defined(TUI)
      /* A bit of paranoia: I want to make sure the "insert_mode" global
       * is clear except when it is being used for command-line editing
       * (see tuiIO.c, utils.c); otherwise normal output will
       * get messed up in the TUI. So clear it before/after
       * the command-line-input call. - RT
       */
      insert_mode = 0;
#endif
      /* Reset just in case something unexpected occured on the previous
	command */
      /* Get a command-line. This calls the readline package. */
      command = command_line_input (instream == stdin ?
				    get_prompt () : (char *) NULL,
				    instream == stdin, "prompt");
#if defined(TUI)
      insert_mode = 0;
#endif
      if (command == 0)
	return;

      time_at_cmd_start = get_run_time ();

      if (display_space)
	{
#ifdef HAVE_SBRK
	  /* The address of the symbol __data_start is the beginning
	     address of the program's data area. */
	  extern void* __data_start;
	  char *lim = (char *) sbrk (0);

	  space_at_cmd_start = (long) (lim - (char *) &__data_start);
#endif
	}

      loop: execute_command (command, instream == stdin);
      /* Do any commands attached to breakpoint we stopped at.  */
      bpstat_do_actions (&stop_bpstat);
      do_cleanups (old_chain);

      if (display_time)
	{
	  long cmd_time = get_run_time () - time_at_cmd_start;

	  printf_unfiltered ("Command execution time: %ld.%06ld\n",
			     cmd_time / 1000000, cmd_time % 1000000);
	}

      if (display_space)
	{
#ifdef HAVE_SBRK
	  extern void* __data_start;
	  char *lim = (char *) sbrk (0);
	  long space_now = lim - (char *) &__data_start;
	  long space_diff = space_now - space_at_cmd_start;

	  printf_unfiltered ("Space used: %ld (%c%ld for this command)\n",
			     space_now,
			     (space_diff >= 0 ? '+' : '-'),
			     space_diff);
#endif
	}
      /* QXCR1000757179 && QXCR1000762032(indirecly fixed by the first CR)
         Suresh - June 2008 - Have logic that doesn't terminate the 
         application for batch mode thread check, where-in we need to 
         repeatedly continue for ever till the application exists, to 
         catch all the thread error events of interest..

         To do this, we simply call execute_command again, so that looping 
         happens. Actually the execute_command loop is done here instead 
         of doing a proceed() loop at the end of print_event_to_log_file() 
         in thread-check.c.

         We do this looping, if its batch mode && threads check is enabled 
         && if we are processing the continue command && if there is no 
         signal received by the inferior(batch_thread_rtc_needs_to_exit 
         is set to 1 if there is a signal received )
       */
      if (batch_rtc && 
          trace_threads_in_this_run  && 
          !batch_thread_rtc_needs_to_exit && 
          (strcmp(command, "continue") == 0))
        goto loop;

    }
}

/* Read commands from `instream' and execute them until end of file or
   error reading instream. This command loop doesnt care about any
   such things as displaying time and space usage. If the user asks
   for those, they won't work. */
void
simplified_command_loop (char *(*read_input_func) (char *),
		         void (*execute_command_func) (char *, int))
{
  struct cleanup *old_chain;
  char *command;
  int stdin_is_tty = ISATTY (stdin);

  while (instream && !feof (instream))
    {
      quit_flag = 0;
      if (instream == stdin && stdin_is_tty)
	reinitialize_more_filter ();

      if (nimbus_version)
	reinitialize_more_filter ();

      old_chain = make_cleanup (command_loop_marker, 0);

      /* Get a command-line. */
      command = (*read_input_func) (instream == stdin ?
				    get_prompt () : (char *) NULL);

      if (command == 0)
	return;

      (*execute_command_func) (command, instream == stdin);

      /* Do any commands attached to breakpoint we stopped at.  */
      bpstat_do_actions (&stop_bpstat);

      do_cleanups (old_chain);
    }
}

/* Commands call this if they do not want to be repeated by null lines.  */

void
dont_repeat ()
{
  if (server_command)
    return;

  /* If we aren't reading from standard input, we are saving the last
     thing read from stdin in line and don't want to delete it.  Null lines
     won't repeat here in any case.  */
  if (instream == stdin)
    *line = 0;
}

/* Read a line from the stream "instream" without command line editing.

   It prints PROMPT_ARG once at the start.
   Action is compatible with "readline", e.g. space for the result is
   malloc'd and should be freed by the caller.

   A NULL return means end of file.  */
char *
gdb_readline (char *prompt_arg)
{
  int c;
  char *result;
  int input_index = 0;
  int result_size = 80;

  if (prompt_arg)
    {
      /* Don't use a _filtered function here.  It causes the assumed
         character position to be off, since the newline we read from
         the user is not accounted for.  */
      fputs_unfiltered (prompt_arg, gdb_stdout);
#ifdef MPW
      /* Move to a new line so the entered line doesn't have a prompt
         on the front of it. */
      fputs_unfiltered ("\n", gdb_stdout);
#endif /* MPW */
      gdb_flush (gdb_stdout);
    }

  result = (char *) xmalloc (result_size);

  while (1)
    {
      /* Read from stdin if we are executing a user defined command.
         This is the right thing for prompt_for_continue, at least.  */
      c = fgetc (instream ? instream : stdin);

      if (c == EOF)
	{
	  if (input_index > 0)
	    /* The last line does not end with a newline.  Return it, and
	       if we are called again fgetc will still return EOF and
	       we'll return NULL then.  */
	    break;
	  free (result);
	  return NULL;
	}

      if (c == '\n')
#ifndef CRLF_SOURCE_FILES
	break;
#else
	{
	  if (input_index > 0 && result[input_index - 1] == '\r')
	    input_index--;
	  break;
	}
#endif

      result[input_index++] = c;
      while (input_index >= result_size)
	{
	  result_size *= 2;
	  result = (char *) xrealloc (result, result_size);
	}
    }

  result[input_index++] = '\0';
  return result;
}

/* Variables which control command line editing and history
   substitution.  These variables are given default values at the end
   of this file.  */
static int command_editing_p;
/* NOTE 1999-04-29: This variable will be static again, once we modify
   gdb to use the event loop as the default command loop and we merge
   event-top.c into this file, top.c */
/* static */ int history_expansion_p;
static int write_history_p;
static int history_size;
static char *history_filename;

/* readline uses the word breaks for two things:
   (1) In figuring out where to point the TEXT parameter to the
   rl_completion_entry_function.  Since we don't use TEXT for much,
   it doesn't matter a lot what the word breaks are for this purpose, but
   it does affect how much stuff M-? lists.
   (2) If one of the matches contains a word break character, readline
   will quote it.  That's why we switch between
   gdb_completer_word_break_characters and
   gdb_completer_command_word_break_characters.  I'm not sure when
   we need this behavior (perhaps for funky characters in C++ symbols?).  */

/* Variables which are necessary for fancy command line editing.  */
char *gdb_completer_word_break_characters =
" \t\n!@#$%^&*()+=|~`}{[]\"';:?/>.<,-";

/* When completing on command names, we remove '-' from the list of
   word break characters, since we use it in command names.  If the
   readline library sees one in any of the current completion strings,
   it thinks that the string needs to be quoted and automatically supplies
   a leading quote. */
char *gdb_completer_command_word_break_characters =
" \t\n!@#$%^&*()+=|~`}{[]\"';:?/>.<,";

/* When completing on file names, we remove from the list of word
   break characters any characters that are commonly used in file
   names, such as '-', '+', '~', etc.  Otherwise, readline displays
   incorrect completion candidates.  */
char *gdb_completer_file_name_break_characters = " \t\n*|\"';:?/><";

/* Characters that can be used to quote completion strings.  Note that we
   can't include '"' because the gdb C parser treats such quoted sequences
   as strings. */
char *gdb_completer_quote_characters =
"'";

/* Functions that are used as part of the fancy command line editing.  */

/* This can be used for functions which don't want to complete on symbols
   but don't want to complete on anything else either.  */
/* ARGSUSED */
char **
noop_completer (char *text, char *prefix)
{
  return NULL;
}

/* Complete on filenames.  */
char **
filename_completer (char *text, char *word)
{
  /* From readline.  */
extern char *filename_completion_function (char *, int);
  int subsequent_name;
  char **return_val;
  int return_val_used;
  int return_val_alloced;

  return_val_used = 0;
  /* Small for testing.  */
  return_val_alloced = 1;
  return_val = (char **) xmalloc (return_val_alloced * sizeof (char *));

  subsequent_name = 0;
  while (1)
    {
      char *p;
      p = filename_completion_function (text, subsequent_name);
      if (return_val_used >= return_val_alloced)
	{
	  return_val_alloced *= 2;
	  return_val =
	    (char **) xrealloc (return_val,
				return_val_alloced * sizeof (char *));
	}
      if (p == NULL)
	{
	  return_val[return_val_used++] = p;
	  break;
	}
      /* We need to set subsequent_name to a non-zero value before the
	 continue line below, because otherwise, if the first file seen
	 by GDB is a backup file whose name ends in a `~', we will loop
	 indefinitely.  */
      subsequent_name = 1;
      /* Like emacs, don't complete on old versions.  Especially useful
         in the "source" command.  */
      if (p[strlen (p) - 1] == '~')
	continue;

      {
	char *q;
	if (word == text)
	  /* Return exactly p.  */
	  return_val[return_val_used++] = p;
	else if (word > text)
	  {
	    /* Return some portion of p.  */
	    q = xmalloc (strlen (p) + 5);
	    strcpy (q, p + (word - text));
	    return_val[return_val_used++] = q;
	    free (p);
	  }
	else
	  {
	    /* Return some of TEXT plus p.  */
	    q = xmalloc (strlen (p) + (text - word) + 5);
	    strncpy (q, word, text - word);
	    q[text - word] = '\0';
	    strcat (q, p);
	    return_val[return_val_used++] = q;
	    free (p);
	  }
      }
    }
  return return_val;
}

/* Here are some useful test cases for completion.  FIXME: These should
   be put in the test suite.  They should be tested with both M-? and TAB.

   "show output-" "radix"
   "show output" "-radix"
   "p" ambiguous (commands starting with p--path, print, printf, etc.)
   "p "  ambiguous (all symbols)
   "info t foo" no completions
   "info t " no completions
   "info t" ambiguous ("info target", "info terminal", etc.)
   "info ajksdlfk" no completions
   "info ajksdlfk " no completions
   "info" " "
   "info " ambiguous (all info commands)
   "p \"a" no completions (string constant)
   "p 'a" ambiguous (all symbols starting with a)
   "p b-a" ambiguous (all symbols starting with a)
   "p b-" ambiguous (all symbols)
   "file Make" "file" (word break hard to screw up here)
   "file ../gdb.stabs/we" "ird" (needs to not break word at slash)
 */

/* Generate completions one by one for the completer.  Each time we are
   called return another potential completion to the caller.
   line_completion just completes on commands or passes the buck to the
   command's completer function, the stuff specific to symbol completion
   is in make_symbol_completion_list.

   TEXT is the caller's idea of the "word" we are looking at.

   MATCHES is the number of matches that have currently been collected from
   calling this completion function.  When zero, then we need to initialize,
   otherwise the initialization has already taken place and we can just
   return the next potential completion string.

   LINE_BUFFER is available to be looked at; it contains the entire text
   of the line.  POINT is the offset in that line of the cursor.  You
   should pretend that the line ends at POINT.

   Returns NULL if there are no more completions, else a pointer to a string
   which is a possible completion, it is the caller's responsibility to
   free the string.  */

static char *
line_completion_function (char *text, int matches, char *line_buffer, int point)
{
  static char **list = (char **) NULL;	/* Cache of completions */
  static int index;		/* Next cached completion */
  char *output = NULL;
  char *tmp_command, *p;
  /* Pointer within tmp_command which corresponds to text.  */
  char *word;
  struct cmd_list_element *c, *result_list;

  if (matches == 0)
    {
      /* The caller is beginning to accumulate a new set of completions, so
         we need to find all of them now, and cache them for returning one at
         a time on future calls. */

      if (list)
	{
	  /* Free the storage used by LIST, but not by the strings inside.
	     This is because rl_complete_internal () frees the strings. */
	  free ((PTR) list);
	}
      list = 0;
      index = 0;

      /* Choose the default set of word break characters to break completions.
         If we later find out that we are doing completions on command strings
         (as opposed to strings supplied by the individual command completer
         functions, which can be any string) then we will switch to the
         special word break set for command strings, which leaves out the
         '-' character used in some commands.  */

      rl_completer_word_break_characters =
	gdb_completer_word_break_characters;

      /* Decide whether to complete on a list of gdb commands or on symbols. */
      tmp_command = (char *) alloca (point + 1);
      p = tmp_command;

      strncpy (tmp_command, line_buffer, point);
      tmp_command[point] = '\0';
      /* Since text always contains some number of characters leading up
         to point, we can find the equivalent position in tmp_command
         by subtracting that many characters from the end of tmp_command.  */
      word = tmp_command + point - strlen (text);

      if (point == 0)
	{
	  /* An empty line we want to consider ambiguous; that is, it
	     could be any command.  */
	  c = (struct cmd_list_element *) ( long)-1;
	  result_list = 0;
	}
      else
	{
	  c = lookup_cmd_1 (&p, cmdlist, &result_list, 1);
	}

      /* Move p up to the next interesting thing.  */
      while (*p == ' ' || *p == '\t')
	{
	  p++;
	}

      if (!c)
	{
	  /* It is an unrecognized command.  So there are no
	     possible completions.  */
	  list = NULL;
	}
      else if (c == (struct cmd_list_element *)(long) -1)
	{
	  char *q;

	  /* lookup_cmd_1 advances p up to the first ambiguous thing, but
	     doesn't advance over that thing itself.  Do so now.  */
	  q = p;
	  while (*q && (isalnum (*q) || *q == '-' || *q == '_'))
	    ++q;
	  if (q != tmp_command + point)
	    {
	      /* There is something beyond the ambiguous
	         command, so there are no possible completions.  For
	         example, "info t " or "info t foo" does not complete
	         to anything, because "info t" can be "info target" or
	         "info terminal".  */
	      list = NULL;
	    }
	  else
	    {
	      /* We're trying to complete on the command which was ambiguous.
	         This we can deal with.  */
	      if (result_list)
		{
		  list = complete_on_cmdlist (*result_list->prefixlist, p,
					      word);
		}
	      else
		{
		  list = complete_on_cmdlist (cmdlist, p, word);
		}
	      /* Insure that readline does the right thing with respect to
	         inserting quotes.  */
	      rl_completer_word_break_characters =
		gdb_completer_command_word_break_characters;
	    }
	}
      else
	{
	  /* We've recognized a full command.  */

	  if (p == tmp_command + point)
	    {
	      /* There is no non-whitespace in the line beyond the command.  */

	      if (p[-1] == ' ' || p[-1] == '\t')
		{
		  /* The command is followed by whitespace; we need to complete
		     on whatever comes after command.  */
		  if (c->prefixlist)
		    {
		      /* It is a prefix command; what comes after it is
		         a subcommand (e.g. "info ").  */
		      list = complete_on_cmdlist (*c->prefixlist, p, word);

		      /* Insure that readline does the right thing
		         with respect to inserting quotes.  */
		      rl_completer_word_break_characters =
			gdb_completer_command_word_break_characters;
		    }
		  else if (c->enums)
		    {
		      list = complete_on_enum (c->enums, p, word);
		      rl_completer_word_break_characters =
			gdb_completer_command_word_break_characters;
		    }
		  else
		    {
		      /* It is a normal command; what comes after it is
		         completed by the command's completer function.  */
		      list = (*c->completer) (p, word);
		      if (c->completer == filename_completer)
			rl_completer_word_break_characters =
			  gdb_completer_file_name_break_characters;
		    }
		}
	      else
		{
		  /* The command is not followed by whitespace; we need to
		     complete on the command itself.  e.g. "p" which is a
		     command itself but also can complete to "print", "ptype"
		     etc.  */
		  char *q;

		  /* Find the command we are completing on.  */
		  q = p;
		  while (q > tmp_command)
		    {
		      if (isalnum (q[-1]) || q[-1] == '-' || q[-1] == '_')
			--q;
		      else
			break;
		    }

		  list = complete_on_cmdlist (result_list, q, word);

		  /* Insure that readline does the right thing
		     with respect to inserting quotes.  */
		  rl_completer_word_break_characters =
		    gdb_completer_command_word_break_characters;
		}
	    }
	  else
	    {
	      /* There is non-whitespace beyond the command.  */

	      if (c->prefixlist && !c->allow_unknown)
		{
		  /* It is an unrecognized subcommand of a prefix command,
		     e.g. "info adsfkdj".  */
		  list = NULL;
		}
	      else if (c->enums)
		{
		  list = complete_on_enum (c->enums, p, word);
		}
	      else
		{
		  /* It is a normal command.  */
		  list = (*c->completer) (p, word);
		  if (c->completer == filename_completer)
		    rl_completer_word_break_characters =
		      gdb_completer_file_name_break_characters;
		}
	    }
	}
    }

  /* If we found a list of potential completions during initialization then
     dole them out one at a time.  The vector of completions is NULL
     terminated, so after returning the last one, return NULL (and continue
     to do so) each time we are called after that, until a new list is
     available. */

  if (list)
    {
      output = list[index];
      if (output)
	{
	  index++;
	}
    }
  return (output);
}

/* Line completion interface function for readline.  */

static char *
readline_line_completion_function (char *text, int matches)
{
  return line_completion_function (text, matches, rl_line_buffer, rl_point);
}

/* Skip over a possibly quoted word (as defined by the quote characters
   and word break characters the completer uses).  Returns pointer to the
   location after the "word". */

char *
skip_quoted (char *str)
{
  char quote_char = '\0';
  char *scan;

  for (scan = str; *scan != '\0'; scan++)
    {
      if (quote_char != '\0')
	{
	  /* Ignore everything until the matching close quote char */
	  if (*scan == quote_char)
	    {
	      /* Found matching close quote. */
	      scan++;
	      break;
	    }
	}
      else if (strchr (gdb_completer_quote_characters, *scan))
	{
	  /* Found start of a quoted string. */
	  quote_char = *scan;
	}
      else if (strchr (gdb_completer_word_break_characters, *scan))
	{
	   /* JAGaf58096 - gdb not setting breakpoint in a funcname containing "$" */
	   /* Since a fortran funcname can have a "$", there is no need to split
	      the funcname on encountering special character "$". */
	   char *temp = "$";
	   if ((strchr(temp,*scan)) && current_language->la_language == language_fortran )
	     continue; 
	   else /* JAGaf58096 - <END> */ 
	     break;
	}
    }
  return (scan);
}


#ifdef STOP_SIGNAL
static void
stop_sig (int signo)
{
#if STOP_SIGNAL == SIGTSTP
  signal (SIGTSTP, SIG_DFL);
  sigsetmask (0);

#if defined(TUI)
  if (tui_version)
    {
      extern void tui_go_background (int);

      tui_go_background (0);
    }
#endif

  kill (getpid (), SIGTSTP);
  signal (SIGTSTP, stop_sig);
#else
  signal (STOP_SIGNAL, stop_sig);
#endif
#if defined(TUI)
  if (tui_version)
    {
      extern void tui_restore_screen (void);

      tui_restore_screen ();
    }
#endif /* TUI */

  printf_unfiltered ("%s", get_prompt ());
  gdb_flush (gdb_stdout);

  /* Forget about any previous command -- null line now will do nothing.  */
  dont_repeat ();
}
#endif /* STOP_SIGNAL */

/* Initialize signal handlers. */
static void
do_nothing (int signo)
{
  /* Under System V the default disposition of a signal is reinstated after
     the signal is caught and delivered to an application process.  On such
     systems one must restore the replacement signal handler if one wishes
     to continue handling the signal in one's program.  On BSD systems this
     is not needed but it is harmless, and it simplifies the code to just do
     it unconditionally. */
  signal (signo, do_nothing);
}

static void
init_signals ()
{
  signal (SIGINT, request_quit);

  /* If SIGTRAP was set to SIG_IGN, then the SIG_IGN will get passed
     to the inferior and breakpoints will be ignored.  */
#ifdef SIGTRAP
  signal (SIGTRAP, SIG_DFL);
#endif

  /* If we initialize SIGQUIT to SIG_IGN, then the SIG_IGN will get
     passed to the inferior, which we don't want.  It would be
     possible to do a "signal (SIGQUIT, SIG_DFL)" after we fork, but
     on BSD4.3 systems using vfork, that can affect the
     GDB process as well as the inferior (the signal handling tables
     might be in memory, shared between the two).  Since we establish
     a handler for SIGQUIT, when we call exec it will set the signal
     to SIG_DFL for us.  */
  signal (SIGQUIT, do_nothing);
#ifdef SIGHUP
  if (signal (SIGHUP, do_nothing) != SIG_IGN)
    signal (SIGHUP, disconnect);
#endif
  signal (SIGFPE, float_handler);

#if defined(SIGWINCH) && defined(SIGWINCH_HANDLER)
  signal (SIGWINCH, SIGWINCH_HANDLER);
#endif
}

#ifdef MULTI_BYTE_EMBEDDED_ASCII
/* nls_fix_readline - return a malloced string in which certain ASCII 
   characters which are part of multi-byte characters have been quoted.  
   This is necessary when the ASCII character has meaning when parsing 
   the input.  Specifically the characters: 
        "       double quote
        `       single quote
        \       backslash
   are quoted.

   input_line - a malloced string which may or may not be returned.  If
                not returned, it is freed here.
   */
char* 
nls_fix_readline (char* input_line)
{
  int                   clen;
  unsigned char*        from_p;
  int                   idx;
  char*                 return_buf;
  unsigned char*        to_p;

  if (locale_is_c || MB_CUR_MAX == 1)
  {
    return input_line;
  }

  /* copy input_line to return_buf, quoting special ASCII characters which are
     part of a multi-byte character.
     */

  return_buf = (char*) malloc (strlen(input_line)*2 + 1);
  from_p = (unsigned char*) input_line;
  to_p = (unsigned char*) return_buf;
  while (*from_p)
  {
    if ((clen = mblen ((char*) from_p, MB_CUR_MAX)) > 1)
    {
      for (idx = 0;  idx < clen;  idx++) 
      {
        if (*from_p == '\'' || *from_p == '"' || *from_p == '\\')
        {
          *to_p++ = '\\';
          *to_p++ = *from_p++;
        } 
        else
        {
          *to_p++ = *from_p++;
        }
      } /* end for idx */
    } /* end if clen */
    else
    {
      *to_p++ = *from_p++;
    } /* end else if clen */
  } /* end while (*from_p) */
  *to_p++ = 0; /* terminate string */
  
  free (input_line);
  return return_buf;
} /* end nls_fix_readline */
#endif /* ifdef MULTI_BYTE_EMBEDDED_ASCII */

/* Read one line from the command input stream `instream'
   into the local static buffer `linebuffer' (whose current length
   is `linelength').
   The buffer is made bigger as necessary.
   Returns the address of the start of the line.

   NULL is returned for end of file.

   *If* the instream == stdin & stdin is a terminal, the line read
   is copied into the file line saver (global var char *line,
   length linesize) so that it can be duplicated.

   This routine either uses fancy command line editing or
   simple input as the user has requested.  */

char *
command_line_input (char *prompt_arg, int repeat, char *annotation_suffix)
{
  static char *linebuffer = 0;
  static unsigned linelength = 0;
  register char *p;
  char *p1;
  char *rl;
  char *local_prompt = prompt_arg;
  char *nline;
  char got_eof = 0;

  /* The annotation suffix must be non-NULL.  */
  if (annotation_suffix == NULL)
    annotation_suffix = "";

  if (annotation_level > 1 && instream == stdin)
    {
      local_prompt = (char *) alloca ((prompt_arg == NULL 
		      ? 0 
		      : strlen (prompt_arg)) + strlen (annotation_suffix) + 40);
      if (prompt_arg == NULL)
	local_prompt[0] = '\0';
      else
	strcpy (local_prompt, prompt_arg);
      strcat (local_prompt, "\n\032\032");
      strcat (local_prompt, annotation_suffix);
      strcat (local_prompt, "\n");
    }

  if (linebuffer == 0)
    {
      linelength = 80;
      linebuffer = (char *) xmalloc (linelength);
    }

  p = linebuffer;

  /* Control-C quits instantly if typed while in this loop
     since it should not wait until the user types a newline.  */
  immediate_quit++;
#ifdef STOP_SIGNAL
  if (job_control)
    {
      if (event_loop_p)
	signal (STOP_SIGNAL, handle_stop_sig);
      else
	signal (STOP_SIGNAL, stop_sig);
    }
#endif

  while (1)
    {
      /* Make sure that all output has been output.  Some machines may let
         you get away with leaving out some of the gdb_flush, but not all.  */
      wrap_here ("");
      gdb_flush (gdb_stdout);
      gdb_flush (gdb_stderr);

      if (source_file_name != NULL)
	{
	  ++source_line_number;
	  sprintf (source_error,
		   "%s%s:%d: Error in sourced command file:\n",
		   source_pre_error,
		   source_file_name,
		   source_line_number);
	  error_pre_print = source_error;
	}

      if (annotation_level > 1 && instream == stdin)
	{
	  printf_unfiltered ("\n\032\032pre-");
	  printf_unfiltered (annotation_suffix);
	  printf_unfiltered ("\n");
	}

      if (nimbus_version && instream == stdin)
        {
          printf_unfiltered ("\033\032\032$A");
          gdb_flush (gdb_stdout);
        }

      /* Don't use fancy stuff if not talking to stdin.  */
      if (readline_hook && instream == NULL)
	{
	  rl = (*readline_hook) (local_prompt);
	}
      else if (command_editing_p && instream == stdin && ISATTY (instream))
	{
	  rl = readline (local_prompt);
	}
      else
	{
	  rl = gdb_readline (local_prompt);
	}

#ifdef MULTI_BYTE_EMBEDDED_ASCII
      rl = nls_fix_readline (rl);
#endif

      if (annotation_level > 1 && instream == stdin)
	{
	  printf_unfiltered ("\n\032\032post-");
	  printf_unfiltered (annotation_suffix);
	  printf_unfiltered ("\n");
	}

      if (!rl || rl == (char *)(long) EOF)
	{
	  got_eof = 1;
	  break;
	}
      if (strlen (rl) + 1 + (p - linebuffer) > linelength)
	{
	  linelength = (unsigned int) (strlen (rl) + 1 + (p - linebuffer));
	  nline = (char *) xrealloc (linebuffer, linelength);
	  p += nline - linebuffer;
	  linebuffer = nline;
	}
      p1 = rl;
      /* Copy line.  Don't copy null at end.  (Leaves line alone
         if this was just a newline)  */
      while (*p1)
	*p++ = *p1++;

      free (rl);		/* Allocated in readline.  */

      if (p == linebuffer || *(p - 1) != '\\')
	break;

      /* If there were an even number of backslashes, then the newline isn't
	 being quoted, the last backslash is being quoted.
	 */
      if (p != linebuffer)
	{
	   char *count_ptr = p - 1;
	   int nbr_backslash = 0;

	   while (count_ptr >= linebuffer && *count_ptr == '\\')
	     {
	       nbr_backslash++;
	       count_ptr--;
	     }
	   if ((nbr_backslash % 2) == 0)
	     break;  /* even, last backslash was itself quoted */
	}

      p--;			/* Put on top of '\'.  */
      local_prompt = (char *) 0;
    }

#ifdef STOP_SIGNAL
  if (job_control)
    signal (STOP_SIGNAL, SIG_DFL);
#endif
  immediate_quit--;

  if (got_eof)
    return NULL;

#define SERVER_COMMAND_LENGTH 7
  server_command =
    (p - linebuffer > SERVER_COMMAND_LENGTH)
    && STREQN (linebuffer, "server ", SERVER_COMMAND_LENGTH);
  if (server_command)
    {
      /* Note that we don't set `line'.  Between this and the check in
         dont_repeat, this insures that repeating will still do the
         right thing.  */
      *p = '\0';
      return linebuffer + SERVER_COMMAND_LENGTH;
    }

  /* Do history expansion if that is wished.  */
  if (history_expansion_p && instream == stdin
      && ISATTY (instream))
    {
      char *history_value;
      int expanded;

      *p = '\0';		/* Insert null now.  */
      expanded = history_expand (linebuffer, &history_value);
      if (expanded)
	{
	  /* Print the changes.  */
	  printf_unfiltered ("%s\n", history_value);

	  /* If there was an error, call this function again.  */
	  if (expanded < 0)
	    {
	      free (history_value);
	      return command_line_input (prompt_arg, repeat, annotation_suffix);
	    }
	  if (strlen (history_value) > linelength)
	    {
	      linelength = (unsigned int) (strlen (history_value) + 1);
	      linebuffer = (char *) xrealloc (linebuffer, linelength);
	    }
	  strcpy (linebuffer, history_value);
	  p = linebuffer + strlen (linebuffer);
	  free (history_value);
	}
    }

  /* If we just got an empty line, and that is supposed
     to repeat the previous command, return the value in the
     global buffer.  */
  if (repeat && p == linebuffer)
    return line;
  for (p1 = linebuffer; *p1 == ' ' || *p1 == '\t'; p1++);
  if (repeat && !*p1)
    return line;

  *p = 0;

  /* Add line to history if appropriate.  */
  if (instream == stdin
      && ISATTY (stdin) && *linebuffer)
    add_history (linebuffer);
  else
  if (nimbus_version && instream == stdin && *linebuffer)
     add_history (linebuffer);

  /* Note: lines consisting solely of comments are added to the command
     history.  This is useful when you type a command, and then
     realize you don't want to execute it quite yet.  You can comment
     out the command and then later fetch it from the value history
     and remove the '#'.  The kill ring is probably better, but some
     people are in the habit of commenting things out.  */
  if (*p1 == '#')
    *p1 = '\0';			/* Found a comment. */

  /* Save into global buffer if appropriate.  */
  if (repeat)
    {
      if (linelength > linesize)
	{
	  line = xrealloc (line, linelength);
	  linesize = linelength;
	}
      strcpy (line, linebuffer);
      return line;
    }

  return linebuffer;
}


/* Expand the body_list of COMMAND so that it can hold NEW_LENGTH
   code bodies.  This is typically used when we encounter an "else"
   clause for an "if" command.  */

static void
realloc_body_list (struct command_line *command, int new_length)
{
  int n;
  struct command_line **body_list;

  n = command->body_count;

  /* Nothing to do?  */
  if (new_length <= n)
    return;

  body_list = (struct command_line **)
    xmalloc (sizeof (struct command_line *) * new_length);

  memcpy (body_list, command->body_list, sizeof (struct command_line *) * n);

  free (command->body_list);
  command->body_list = body_list;
  command->body_count = new_length;
}

/* Read one line from the input stream.  If the command is an "else" or
   "end", return such an indication to the caller.  */

static enum misc_command_type
read_next_line (struct command_line **command)
{
  char *p, *p1, *prompt_ptr, control_prompt[256];
  int i = 0;

  if (control_level >= 254)
    error ("Control nesting too deep!\n");

  /* Set a prompt based on the nesting of the control commands.  */
  if (instream == stdin || (instream == 0 && readline_hook != NULL))
    {
      for (i = 0; i < control_level; i++)
	control_prompt[i] = ' ';
      control_prompt[i] = '>';
      control_prompt[i + 1] = '\0';
      prompt_ptr = (char *) &control_prompt[0];
    }
  else
    prompt_ptr = NULL;

  p = command_line_input (prompt_ptr, instream == stdin, "commands");

  /* Not sure what to do here.  */
  if (p == NULL)
    return end_command;

  /* Strip leading and trailing whitespace.  */
  while (*p == ' ' || *p == '\t')
    p++;

  p1 = p + strlen (p);
  while (p1 != p && (p1[-1] == ' ' || p1[-1] == '\t'))
    p1--;

  /* Blanks and comments don't really do anything, but we need to
     distinguish them from else, end and other commands which can be
     executed.  */
  if (p1 == p || p[0] == '#')
    return nop_command;

  /* Is this the end of a simple, while, or if control structure?  */
  if (p1 - p == 3 && !strncmp (p, "end", 3))
    return end_command;

  /* Is the else clause of an if control structure?  */
  if (p1 - p == 4 && !strncmp (p, "else", 4))
    return else_command;

  /* Check for while, if, break, continue, etc and build a new command
     line structure for them.  */
  if (p1 - p > 5 && !strncmp (p, "while", 5))
    *command = build_command_line (while_control, p + 6);
  else if (p1 - p > 2 && !strncmp (p, "if", 2))
    *command = build_command_line (if_control, p + 3);
  else if (p1 - p == 10 && !strncmp (p, "loop_break", 10))
    {
      *command = (struct command_line *)
	xmalloc (sizeof (struct command_line));
      (*command)->next = NULL;
      (*command)->line = NULL;
      (*command)->control_type = break_control;
      (*command)->body_count = 0;
      (*command)->body_list = NULL;
    }
  else if (p1 - p == 13 && !strncmp (p, "loop_continue", 13))
    {
      *command = (struct command_line *)
	xmalloc (sizeof (struct command_line));
      (*command)->next = NULL;
      (*command)->line = NULL;
      (*command)->control_type = continue_control;
      (*command)->body_count = 0;
      (*command)->body_list = NULL;
    }
  else
    {
      /* A normal command.  */
      *command = (struct command_line *)
	xmalloc (sizeof (struct command_line));
      (*command)->next = NULL;
      (*command)->line = savestring (p, (int) (p1 - p));
      (*command)->control_type = simple_control;
      (*command)->body_count = 0;
      (*command)->body_list = NULL;
    }

  /* Nothing special.  */
  return ok_command;
}

/* Recursively read in the control structures and create a command_line 
   structure from them.

   The parent_control parameter is the control structure in which the
   following commands are nested.  */

static enum command_control_type
recurse_read_control_structure (struct command_line *current_cmd)
{
  int current_body, i;
  enum misc_command_type val;
  enum command_control_type ret = 0; /* initialize for compiler warning */
  struct command_line **body_ptr, *child_tail, *next;

  child_tail = NULL;
  current_body = 1;

  /* Sanity checks.  */
  if (current_cmd->control_type == simple_control)
    {
      error ("Recursed on a simple control type\n");
      return invalid_control;
    }

  if (current_body > current_cmd->body_count)
    {
      error ("Allocated body is smaller than this command type needs\n");
      return invalid_control;
    }

  /* Read lines from the input stream and build control structures.  */
  while (1)
    {
      dont_repeat ();

      next = NULL;
      val = read_next_line (&next);

      /* Just skip blanks and comments.  */
      if (val == nop_command)
	continue;

      if (val == end_command)
	{
	  if (current_cmd->control_type == while_control
	      || current_cmd->control_type == if_control)
	    {
	      /* Success reading an entire control structure.  */
	      ret = simple_control;
	      break;
	    }
	  else
	    {
	      ret = invalid_control;
	      break;
	    }
	}

      /* Not the end of a control structure.  */
      if (val == else_command)
	{
	  if (current_cmd->control_type == if_control
	      && current_body == 1)
	    {
	      realloc_body_list (current_cmd, 2);
	      current_body = 2;
	      child_tail = NULL;
	      continue;
	    }
	  else
	    {
	      ret = invalid_control;
	      break;
	    }
	}

      if (child_tail)
	{
	  child_tail->next = next;
	}
      else
	{
	  body_ptr = current_cmd->body_list;
	  for (i = 1; i < current_body; i++)
	    body_ptr++;

	  *body_ptr = next;

	}

      child_tail = next;

      /* If the latest line is another control structure, then recurse
         on it.  */
      if (next->control_type == while_control
	  || next->control_type == if_control)
	{
	  control_level++;
	  ret = recurse_read_control_structure (next);
	  control_level--;

	  if (ret != simple_control)
	    break;
	}
    }

  dont_repeat ();

  return ret;
}

/* Read lines from the input stream and accumulate them in a chain of
   struct command_line's, which is then returned.  For input from a
   terminal, the special command "end" is used to mark the end of the
   input, and is not included in the returned chain of commands. */

#define END_MESSAGE "End with a line saying just \"end\"."

struct command_line *
read_command_lines (char *prompt_arg, int from_tty)
{
  struct command_line *head, *tail, *next;
  struct cleanup *old_chain;
  enum command_control_type ret = 0; /* initialize for compiler warning */
  enum misc_command_type val;

  control_level = 0;
  if (readline_begin_hook)
    {
      /* Note - intentional to merge messages with no newline */
      (*readline_begin_hook) ("%s  %s\n", prompt_arg, END_MESSAGE);
    }
  else if (from_tty && input_from_terminal_p ())
    {
      printf_unfiltered ("%s\n%s\n", prompt_arg, END_MESSAGE);
      gdb_flush (gdb_stdout);
    }

  head = tail = NULL;
  old_chain = NULL;

/* JAGag06295: Check moved up the call stack to prevent repetition of warning message  */
#ifdef HP_IA64
  /* If we are stopped in an optimized routine, emit a warning. */

  if (instream == stdin && target_has_stack)
    {
      struct symbol*		sym;
      struct minimal_symbol *	msym;
      if (   selected_frame 
	  && (sym = find_pc_function (selected_frame->pc))
	  && (sym->ginfo.opt_level > 1))
	{
	  printf_unfiltered (
    "Warning: this routine is optimized.  Debugging information unreliable.\n");
	}
    }
#endif

  while (1)
    {
      val = read_next_line (&next);

      /* Ignore blank lines or comments.  */
      if (val == nop_command)
	continue;

      if (val == end_command)
	{
	  ret = simple_control;
	  break;
	}

      if (val != ok_command)
	{
	  ret = invalid_control;
	  break;
	}

      if (next->control_type == while_control
	  || next->control_type == if_control)
	{
	  control_level++;
	  ret = recurse_read_control_structure (next);
	  control_level--;

	  if (ret == invalid_control)
	    break;
	}

      if (tail)
	{
	  tail->next = next;
	}
      else
	{
	  head = next;
	  old_chain = make_cleanup_free_command_lines (&head);
	}
      tail = next;
    }

  dont_repeat ();

  if (head)
    {
      if (ret != invalid_control)
	{
	  discard_cleanups (old_chain);
	}
      else
	do_cleanups (old_chain);
    }

  if (readline_end_hook)
    {
      (*readline_end_hook) ();
    }
  return (head);
}

/* Free a chain of struct command_line's.  */

void
free_command_lines (struct command_line **lptr)
{
  register struct command_line *l = *lptr;
  register struct command_line *next;
  struct command_line **blist;
  int i;

  while (l)
    {
      if (l->body_count > 0)
	{
	  blist = l->body_list;
	  for (i = 0; i < l->body_count; i++, blist++)
	    free_command_lines (blist);
	}
      next = l->next;
      free (l->line);
      free ((PTR) l);
      l = next;
    }
}

static void
do_free_command_lines_cleanup (void *arg)
{
  free_command_lines (arg);
}

static struct cleanup *
make_cleanup_free_command_lines (struct command_line **arg)
{
  return make_cleanup (do_free_command_lines_cleanup, arg);
}

/* JAGab65132 - temporary breakpoints do not execute their command
   New function copy_command_lines() added. */
struct command_line *
copy_command_lines (struct command_line *cmds)
{
  struct command_line *result = NULL;

  if (cmds)
    {
      result = (struct command_line *) xmalloc (sizeof (struct command_line));

      result->next = copy_command_lines (cmds->next);
      result->line = xstrdup (cmds->line);
      result->control_type = cmds->control_type;
      result->body_count = cmds->body_count;
      if (cmds->body_count > 0)
        {
          int i;

          result->body_list = (struct command_line **)
            xmalloc (sizeof (struct command_line *) * cmds->body_count);

          for (i = 0; i < cmds->body_count; i++)
            result->body_list[i] = copy_command_lines (cmds->body_list[i]);
        }
      else
        result->body_list = NULL;
    }

  return result;
}


/* Add an element to the list of info subcommands.  */

struct cmd_list_element *
add_info ( char *name, void (*fun) (char *, int), char *doc)
{
  return add_cmd (name, no_class, fun, doc, &infolist);
}

/* Add an alias to the list of info subcommands.  */

struct cmd_list_element *
add_info_alias (char *name, char *oldname, int abbrev_flag)
{
  return add_alias_cmd (name, oldname, 0, abbrev_flag, &infolist);
}

/* The "info" command is defined as a prefix, with allow_unknown = 0.
   Therefore, its own definition is called only for "info" with no args.  */

/* ARGSUSED */
static void
info_command (char *arg, int from_tty)
{
  printf_unfiltered ("\"info\" must be followed by the name of an info command.\n");
  help_list (infolist, "info ", -1, gdb_stdout);
}

/* The "complete" command is used by Emacs to implement completion.  */

/* ARGSUSED */
static void
complete_command (char *arg, int from_tty)
{
  int i;
  int argpoint;
  char *completion;

  dont_repeat ();

  if (arg == NULL)
    arg = "";
  argpoint = (int) strlen (arg);

  for (completion = line_completion_function (arg, i = 0, arg, argpoint);
       completion;
       completion = line_completion_function (arg, ++i, arg, argpoint))
    {
      printf_unfiltered ("%s\n", completion);
      free (completion);
    }
}

/* The "show" command with no arguments shows all the settings.  */

/* ARGSUSED */
static void
show_command (char *arg, int from_tty)
{
  /* QXCR1000946156: WDB crashes if 'show' command is input twice consecutively.
     Reset the level as it might contain junk value due to the previous
     incomplete 'show' command.
  */
  uiout->level = 0;
  cmd_show_list (showlist, from_tty, "");
}

/* Add an element to the list of commands.  */

struct cmd_list_element *
add_com (char *name, enum command_class class, void (*fun) (char *, int), char *doc)
{
  return add_cmd (name, class, fun, doc, &cmdlist);
}

/* Add an alias or abbreviation command to the list of commands.  */

struct cmd_list_element *
add_com_alias (char *name, char *oldname, enum command_class class,
               int abbrev_flag)
{
  return add_alias_cmd (name, oldname, class, abbrev_flag, &cmdlist);
}

void
error_no_arg (char *why)
{
  error ("Argument required (%s).", why);
}

/* ARGSUSED */
static void
help_command (char *command, int from_tty /* Ignored */)
{
  help_cmd (command, gdb_stdout);
}

static void
validate_comname (char *comname)
{
  register char *p;

  if (comname == 0)
    error_no_arg ("name of command to define");

  p = comname;
  while (*p)
    {
      if (!isalnum (*p) && *p != '-' && *p != '_')
	error ("Junk in argument list: \"%s\"", p);
      p++;
    }
}

/* This is just a placeholder in the command data structures.  */
static void
user_defined_command (char *ignore, int from_tty)
{
}

static void
define_command (char *comname, int from_tty)
{
  register struct command_line *cmds;
  register struct cmd_list_element *c, *newc, *hookc = 0;
  char *tem = comname;
  char tmpbuf[128];
#define	HOOK_STRING	"hook-"
#define	HOOK_LEN 5

  validate_comname (comname);

  /* Look it up, and verify that we got an exact match.  */
  c = lookup_cmd (&tem, cmdlist, "", -1, 1);
  if (c && !STREQ (comname, c->name))
    c = 0;

  if (c)
    {
      if (c->class == class_user || c->class == class_alias)
	tem = "Redefine command \"%s\"? ";
      else
	tem = "Really redefine built-in command \"%s\"? ";
      if (!query (tem, c->name))
	error ("Command \"%s\" not redefined.", c->name);
    }

  /* If this new command is a hook, then mark the command which it
     is hooking.  Note that we allow hooking `help' commands, so that
     we can hook the `stop' pseudo-command.  */

  if (!strncmp (comname, HOOK_STRING, HOOK_LEN))
    {
      /* Look up cmd it hooks, and verify that we got an exact match.  */
      tem = comname + HOOK_LEN;
      hookc = lookup_cmd (&tem, cmdlist, "", -1, 0);
      if (hookc && !STREQ (comname + HOOK_LEN, hookc->name))
	hookc = 0;
      if (!hookc)
	{
	  warning ("Your new `%s' command does not hook any existing command.",
		   comname);
	  if (!query ("Proceed? "))
	    error ("Not confirmed.");
	}
    }

  comname = savestring (comname, (int) strlen (comname));

  /* If the rest of the commands will be case insensitive, this one
     should behave in the same manner. */
  for (tem = comname; *tem; tem++)
    if (isupper (*tem))
      *tem = tolower (*tem);

  sprintf (tmpbuf, "Type commands for definition of \"%s\".", comname);
  cmds = read_command_lines (tmpbuf, from_tty);

  if (c && c->class == class_user)
    free_command_lines (&c->user_commands);

  newc = add_cmd (comname, class_user, user_defined_command,
		  (c && c->class == class_user)
		  ? c->doc : savestring ("User-defined.", 13), &cmdlist);
  newc->user_commands = cmds;

  /* If this new command is a hook, then mark both commands as being
     tied.  */
  if (hookc)
    {
      hookc->hook = newc;	/* Target gets hooked.  */
      newc->hookee = hookc;	/* We are marked as hooking target cmd.  */
    }
}

static void
document_command (char *comname, int from_tty)
{
  struct command_line *doclines;
  register struct cmd_list_element *c;
  char *tem = comname;
  char tmpbuf[128];

  validate_comname (comname);

  c = lookup_cmd (&tem, cmdlist, "", 0, 1);

  if (c->class != class_user)
    error ("Command \"%s\" is built-in.", comname);

  sprintf (tmpbuf, "Type documentation for \"%s\".", comname);
  doclines = read_command_lines (tmpbuf, from_tty);

  if (c->doc)
    free (c->doc);

  {
    register struct command_line *cl1;
    register int len = 0;

    for (cl1 = doclines; cl1; cl1 = cl1->next)
      len += strlen (cl1->line) + 1;

    c->doc = (char *) xmalloc (len + 1);
    *c->doc = 0;

    for (cl1 = doclines; cl1; cl1 = cl1->next)
      {
	strcat (c->doc, cl1->line);
	if (cl1->next)
	  strcat (c->doc, "\n");
      }
  }

  free_command_lines (&doclines);
}

#if !defined(GDB_TARGET_IS_HPPA) && !defined (HP_IA64)
/* Print the GDB banner. */
void
print_gdb_version (struct ui_file *stream)
{
  /* From GNU coding standards, first line is meant to be easy for a
     program to parse, and is just canonical program name and version
     number, which starts after last space. */

#ifdef UI_OUT
  /* Print it console style until a format is defined */
  fprintf_filtered (stream, "GNU gdb %s (UI_OUT)\n", version);
#else
  fprintf_filtered (stream, "GNU gdb %s\n", version);
#endif

  /* Second line is a copyright notice. */

  fprintf_filtered (stream, "Copyright 2000 Free Software Foundation, Inc.\n");

  /* Following the copyright is a brief statement that the program is
     free software, that users are free to copy and change it on
     certain conditions, that it is covered by the GNU GPL, and that
     there is no warranty. */

  fprintf_filtered (stream, "\
GDB is free software, covered by the GNU General Public License, and you are\n\
welcome to change it and/or distribute copies of it under certain conditions.\n\
Type \"show copying\" to see the conditions.\n\
There is absolutely no warranty for GDB.  Type \"show warranty\" for details.\n");

  /* After the required info we print the configuration information. */

  fprintf_filtered (stream, "This GDB was configured as \"");
  if (!STREQ (host_name, target_name))
    {
      fprintf_filtered (stream, "--host=%s --target=%s", host_name, target_name);
    }
  else
    {
      fprintf_filtered (stream, "%s", host_name);
    }
  fprintf_filtered (stream, "\".");
#ifdef SEND_BETA_MAIL_ADDR
  fprintf_filtered (stream, 
                    "This version of gdb sends mail to log beta testing.\n");
#endif
}
#endif /* ifndef GDB_TARGET_IS_HPPA */

#ifdef GDB_TARGET_IS_HPUX
void
print_gdb_version (struct ui_file *stream)
{
  struct utsname name;
    
  /* From GNU coding standards, first line is meant to be easy for a
     program to parse, and is just canonical program name and version
     number, which starts after last space. */

  fprintf_filtered (stream, "HP gdb %s for ", wdb_version_nbr);

  /* Stacey - August 13, 2001
     In order to shorten the welcome message, but keep its content, print
     configuration info as part of the first line of the welcome message
     instead of printing an extra line of text to convey the same info */
  {
    int parisc_primary_version;
    int parisc_secondary_version;
    int hpux_primary_version;
    int hpux_secondary_version;
    char *ptr;
    int is_64bit = 0;
    char  host_description [100];  /* Arbitrary, but should be more 
                                      than enough. */
    char  hpux_version_spelling [10];  /* Arbitrary, but should be more 
                                          than enough. */


    ptr = (char *) host_name;

    /* 
     *  If we do not have a name beginning with "hppa", then this
     *  is not an hpux configuration.  Simply print out the host_name
     *  for the version
     */
   
    if (strncmp(host_name,"hppa",4) != 0)
      {
	if (strncmp (host_name, "ia64-hp", 7) == 0)
          {
            fprintf_filtered (stream, "HP Itanium (32 or 64 bit) ");
            fprintf_filtered (stream, "and target HP-UX 11iv2 and 11iv3.\n");
          }
	else
	  {
            strcpy (host_description, host_name);
            fprintf_filtered (stream, "%s\n", host_description);
            fprintf_filtered (stream, "and target %s.\n", target_name); 
	  }
      }
    else 
      {
        ptr = (char *) host_name;
        /* skip over hppa portion */
        ptr += 4;
        
        /* pick up parisc primary version (either 1 or 2) for
           PA 1.1 or PA 2.0 respectively*/

        sscanf(ptr,"%1d",&parisc_primary_version);
        
        /* skip over digit and "." */

        ptr += 2;

        /* pick up parisc secondary version either (1 or 0) for
           PA 1.1 or PA 2.0 respectively. */

        sscanf(ptr,"%1d",&parisc_secondary_version);
        ptr += 1;

        if (*ptr == 'w')
          { /* wide mode */
            is_64bit = 1;
            ptr++;
          }

        /* pick up the os version */

        sscanf(ptr,"-hp-hpux%2d.%2d",
               &hpux_primary_version,
               &hpux_secondary_version);

        if (is_64bit) 
            strcpy (host_description, "PA-RISC 2.0 (wide)");
        else
            strcpy (host_description, "PA-RISC 1.1 or 2.0 (narrow)");
        strcat (host_description, ", HP-UX 11i ");
       /* sprintf (hpux_version_spelling, "%02d.%02d", hpux_primary_version, hpux_secondary_version); 
        strcat (host_description, hpux_version_spelling); 
       */ 
        fprintf_filtered (stream, "%s\n", host_description);
        fprintf_filtered (stream, "and target %s.\n", target_name); 
      }
  }

    /* Stacey - August 13, 2001
       We've just finished printing configuration info.  Now print the
       rest of the required GNU info - GNU GPL, copyright and warranty.
       The message has been shortened in response to user complaints 
       about the verboseness of gdb, and the copyright info has been 
       updated to read 2001 instead of 1989 */

    /* Second line is a copyright notice. */

    fprintf_filtered (stream, "Copyright 1986 - 2011 Free Software Foundation,"
		      " Inc.\n");

    /* Identify the program as HP Wildebeest */

    fprintf_filtered (stream, 
		      "Hewlett-Packard Wildebeest %s (based on GDB) ",
		      wdb_version_nbr);

    /* Following the copyright is a brief statement that the program is
       free software, that users are free to copy and change it on
       certain conditions, that it is covered by the GNU GPL, and that
       there is no warranty. */

    fprintf_filtered (stream, "\
is covered by the\n\
GNU General Public License. Type \"show copying\" to see the conditions to\n\
change it and/or distribute copies. Type \"show warranty\" for warranty/support.\n");

    /* Madhavi, added some code to make sure 10.20 refuses to run on 
       11.00 and vice versa. These are not intended usage patterns :
       11.00 version would core dump on 10.20 and 10.20 version would
       misbehave on 11.00
    */

  uname(&name);
#ifdef HPUX_1020
  if (!strstr(name.release,"10.20"))
    {
      warning ("This version of WDB is built for HP-UX 10.20 but you are running on HP-UX %s", name.release);
      if (!query ("Are you sure you want to proceed? "))
        exit (0);
    }
#endif /* HPUX_1020 */

#ifndef HPUX_1020
  if (!strstr(name.release,"11."))
    {
      warning ("This version of WDB is built for HP-UX 11.0 but you are running on HP-UX %s", name.release);
      if (!query ("Are you sure you want to proceed? "))
        exit (0);
    }
#endif /* ! HPUX_1020 */

#ifdef SEND_BETA_MAIL_ADDR
  fprintf_filtered (stream, 
                    "This version of gdb sends mail to log beta testing.\n");
#endif

} /* end print_gdb_version */
#endif /* ifdef GDB_TARGET_IS_HPUX*/

/* ARGSUSED */
static void
show_version (char *args, int from_tty)
{
  immediate_quit++;
  print_gdb_version (gdb_stdout);
  printf_filtered ("\n");
  immediate_quit--;
}

/* get_prompt: access method for the GDB prompt string.  */

#define MAX_PROMPT_SIZE 256

/*
 * int get_prompt_1 (char * buf);
 *
 * Work-horse for get_prompt (called via catch_errors).
 * Argument is buffer to hold the formatted prompt.
 *
 * Returns: 1 for success (use formatted prompt)
 *          0 for failure (use gdb_prompt_string).
 */

static int gdb_prompt_escape;

static int
get_prompt_1 (char *formatted_prompt)
{
  char *local_prompt;

  if (event_loop_p)
    local_prompt = PROMPT (0);
  else
    local_prompt = gdb_prompt_string;


  if (gdb_prompt_escape == 0)
    {
      return 0;			/* do no formatting */
    }
  else
    /* formatted prompt */
    {
      char fmt[40], *promptp, *outp, *tmp;
      value_ptr arg_val;
      DOUBLEST doubleval;
      LONGEST longval;
      CORE_ADDR addrval;

      int i, len;
      struct type *arg_type, *elt_type;

      promptp = local_prompt;
      outp = formatted_prompt;

      while (*promptp != '\0')
	{
	  int available = (int) (MAX_PROMPT_SIZE - (outp - formatted_prompt) - 1);

	  if (*promptp != gdb_prompt_escape)
	    {
	      if (available >= 1)	/* overflow protect */
		*outp++ = *promptp++;
	    }
	  else
	    {
	      /* GDB prompt string contains escape char.  Parse for arg.
	         Two consecutive escape chars followed by arg followed by
	         a comma means to insert the arg using a default format.
	         Otherwise a printf format string may be included between
	         the two escape chars.  eg:
	         %%foo, insert foo using default format
	         %2.2f%foo,     insert foo using "%2.2f" format
	         A mismatch between the format string and the data type
	         of "foo" is an error (which we don't know how to protect
	         against).  */

	      fmt[0] = '\0';	/* assume null format string */
	      if (promptp[1] == gdb_prompt_escape)	/* double esc char */
		{
		  promptp += 2;	/* skip past two escape chars. */
		}
	      else
		{
		  /* extract format string from between two esc chars */
		  i = 0;
		  do
		    {
		      fmt[i++] = *promptp++;	/* copy format string */
		    }
		  while (i < sizeof (fmt) - 1 &&
			 *promptp != gdb_prompt_escape &&
			 *promptp != '\0');

		  if (*promptp != gdb_prompt_escape)
		    error ("Syntax error at prompt position %d",
			   promptp - local_prompt);
		  else
		    {
		      promptp++;	/* skip second escape char */
		      fmt[i++] = '\0';	/* terminate the format string */
		    }
		}

	      arg_val = parse_to_comma_and_eval (&promptp);
	      if (*promptp == ',')
		promptp++;	/* skip past the comma */
	      arg_type = check_typedef (VALUE_TYPE (arg_val));
	      switch (TYPE_CODE (arg_type))
		{
		case TYPE_CODE_ARRAY:
		  elt_type = check_typedef (TYPE_TARGET_TYPE (arg_type));
		  if (TYPE_LENGTH (arg_type) > 0 &&
		      TYPE_LENGTH (elt_type) == 1 &&
		      TYPE_CODE (elt_type) == TYPE_CODE_INT)
		    {
		      int len = TYPE_LENGTH (arg_type);

		      if (VALUE_LAZY (arg_val))
			value_fetch_lazy (arg_val);
		      tmp = VALUE_CONTENTS (arg_val);

		      if (len > available)
			len = available;	/* overflow protect */

		      /* FIXME: how to protect GDB from crashing
		         from bad user-supplied format string? */
		      if (fmt[0] != 0)
			sprintf (outp, fmt, tmp);
		      else
			strncpy (outp, tmp, len);
		      outp[len] = '\0';
		    }
		  break;
		case TYPE_CODE_PTR:
		  elt_type = check_typedef (TYPE_TARGET_TYPE (arg_type));
		  addrval = value_as_pointer (arg_val);

		  if (TYPE_LENGTH (elt_type) == 1 &&
		      TYPE_CODE (elt_type) == TYPE_CODE_INT &&
		      addrval != 0)
		    {
		      /* display it as a string */
		      char *default_fmt = "%s";
		      char *tmp;
		      int err = 0;

		      /* Limiting the number of bytes that the following call
		         will read protects us from sprintf overflow later. */
		      i = target_read_string (addrval,	/* src */
					      &tmp,	/* dest */
					      available,	/* len */
					      &err);
		      if (err)	/* read failed */
			error ("%s on target_read", safe_strerror (err));

		      tmp[i] = '\0';	/* force-terminate string */
		      /* FIXME: how to protect GDB from crashing
		         from bad user-supplied format string? */
		      sprintf (outp, fmt[0] == 0 ? default_fmt : fmt,
			       tmp);
		      free (tmp);
		    }
		  else
		    {
		      /* display it as a pointer */
		      char *default_fmt = "0x%x";

		      /* FIXME: how to protect GDB from crashing
		         from bad user-supplied format string? */
		      if (available >= 16 /*? */ )	/* overflow protect */
			sprintf (outp, fmt[0] == 0 ? default_fmt : fmt,
				 (long) addrval);
		    }
		  break;
		case TYPE_CODE_FLT:
		  {
		    char *default_fmt = "%g";

		    doubleval = value_as_double (arg_val);
		    /* FIXME: how to protect GDB from crashing
		       from bad user-supplied format string? */
		    if (available >= 16 /*? */ )	/* overflow protect */
		      sprintf (outp, fmt[0] == 0 ? default_fmt : fmt,
			       (double) doubleval);
		    break;
		  }
		case TYPE_CODE_INT:
		  {
		    char *default_fmt = "%d";

		    longval = value_as_long (arg_val);
		    /* FIXME: how to protect GDB from crashing
		       from bad user-supplied format string? */
		    if (available >= 16 /*? */ )	/* overflow protect */
		      sprintf (outp, fmt[0] == 0 ? default_fmt : fmt,
			       (long) longval);
		    break;
		  }
		case TYPE_CODE_BOOL:
		  {
		    /* no default format for bool */
		    longval = value_as_long (arg_val);
		    if (available >= 8 /*? */ )		/* overflow protect */
		      {
			if (longval)
			  strcpy (outp, "<true>");
			else
			  strcpy (outp, "<false>");
		      }
		    break;
		  }
		case TYPE_CODE_ENUM:
		  {
		    /* no default format for enum */
		    longval = value_as_long (arg_val);
		    len = TYPE_NFIELDS (arg_type);
		    /* find enum name if possible */
		    for (i = 0; i < len; i++)
		      if (TYPE_FIELD_BITPOS (arg_type, i) == longval)
			break;	/* match -- end loop */

		    if (i < len)	/* enum name found */
		      {
			char *name = TYPE_FIELD_NAME (arg_type, i);

			strncpy (outp, name, available);
			/* in casel available < strlen (name), */
			outp[available] = '\0';
		      }
		    else
		      {
			if (available >= 16 /*? */ )	/* overflow protect */
			  sprintf (outp, "%ld", (long) longval);
		      }
		    break;
		  }
		case TYPE_CODE_VOID:
		  *outp = '\0';
		  break;	/* void type -- no output */
		default:
		  error ("bad data type at prompt position %d",
			 promptp - local_prompt);
		  break;
		}
	      outp += strlen (outp);
	    }
	}
      *outp++ = '\0';		/* terminate prompt string */
      return 1;
    }
}

char *
get_prompt ()
{
  static char buf[MAX_PROMPT_SIZE];

  if (catch_errors ((catch_errors_ftype *) get_prompt_1, buf, 
		    "bad formatted prompt: ", RETURN_MASK_ALL))
    {
      return &buf[0];		/* successful formatted prompt */
    }
  else
    {
      /* Prompt could not be formatted.  */
      if (event_loop_p)
	return PROMPT (0);
      else
	return gdb_prompt_string;
    }
}

void
set_prompt (char *s)
{
/* ??rehrauer: I don't know why this fails, since it looks as though
   assignments to prompt are wrapped in calls to savestring...
   if (prompt != NULL)
   free (prompt);
 */
  if (event_loop_p)
    PROMPT (0) = savestring (s, (int) strlen (s));
  else
    gdb_prompt_string = savestring (s, (int) strlen (s));
}


/* If necessary, make the user confirm that we should quit.  Return
   non-zero if we should quit, zero if we shouldn't.  */

int
quit_confirm ()
{
  if (inferior_pid != 0 && target_has_execution)
    {
      char *s;

      /* This is something of a hack.  But there's no reliable way to
         see if a GUI is running.  The `use_windows' variable doesn't
         cut it.  */
      if (init_ui_hook)
	s = "A debugging session is active.\nDo you still want to close the debugger?";
      else if (attach_flag)
	s = "The program is running.  Quit anyway (and detach it)? ";
      else
	s = "The program is running.  Exit anyway? ";

      if (!query (s))
	return 0;
    }

  return 1;
}

/* Quit without asking for confirmation.  */

void
quit_force (char *args, int from_tty)
{
  int exit_code = 0;
  extern int firebolt_version;

  /* An optional expression may be used to cause gdb to terminate with the 
     value of that expression. */
  if (args)
    {
      value_ptr val = parse_and_eval (args);

      exit_code = (int) value_as_long (val);
    }

  if (inferior_pid != 0 && target_has_execution)
    {
      if (attach_flag)
	target_detach (args, from_tty);
      else
	target_kill ();
    }

  /* UDI wants this, to kill the TIP.  */
  target_close (1);

  /* Save the history information if it is appropriate to do so.  */
  if (write_history_p && history_filename)
    write_history (history_filename);

  do_final_cleanups (ALL_CLEANUPS);	/* Do any final cleanups before exiting */

#if defined(TUI)
  /* tuiDo((TuiOpaqueFuncPtr)tuiCleanUp); */
  /* The above does not need to be inside a tuiDo(), since
   * it is not manipulating the curses screen, but rather,
   * it is tearing it down.
   */
  if (tui_version)
    tuiCleanUp ();
#endif

#ifdef HP_XMODE

  /* 
   *  If we are capable of doing cross mode debugging and we
   *  have a temporary file open, close it and remove it as we
   *  do not need it after a "quit" command has been entered.
   */

  if (xmode_user_env_record != NULL) 
    {
      fclose (xmode_user_env_record);
      unlink (xmode_user_env_record_name);
    }

#endif /* HP_XMODE */

  /* When we quit VDB, make sure to clean up.  Remove all the temporary
     directory and files VDB used to store assembly code.  */

  if (nimbus_version)
    {
      int i;
      char dirname[80];
      extern char *nimbus_temp_files[MAX_TEMP_FILES];
      
      for (i = 0; i < MAX_TEMP_FILES; i++)
	{
	  if (nimbus_temp_files[i]) 
	    {
	      unlink(nimbus_temp_files[i]);
	      free (nimbus_temp_files[i]);
	    }
	  else break;
	}
      
      sprintf(dirname, "/tmp/nimbus.%d/", getpid());
      rmdir(dirname);
      printf_unfiltered ("\033:qa!\n");
      gdb_flush (gdb_stdout);

      /* We don't need the delay for Firebolt. We shouldn't need it
         even for Nimbus, but the terminal settings are not restored
         for some reason in -tui mode. Srikanth, Aug 5th 2002.
      */
      if (!firebolt_version)
        sleep (1);
    }

  if (firebolt_version)
    {
      char nuke_asm_files[128];
      sprintf (nuke_asm_files, "/bin/rm -f /tmp/.firebolt.%d* >/dev/null 2>/dev/null", getppid());
      system (nuke_asm_files);
    }

  exit (exit_code);
}

/* wrapper for exit(2) call, for use via "error_hook" so we'll see the
 * error message and still get a zero exit status from gdb.  Part of
 * fix for JAGag21088, "cant quit gdb if target gets killed".
 */

static void 
exit_wrapper(char *msg)
{
  printf_unfiltered ("Error while quiting gdb : %s\n", msg);
  gdb_flush (gdb_stdout);
  exit(0);
}

/* Handle the quit command.  */

void
quit_command (char *args, int from_tty)
{
  if (!quit_confirm ())
    error ("Not confirmed.");

  /* JAGag21088, "cant quit gdb if target gets killed".  Just define
   * "error_hook" to point to a wrapper around exit(2), as we don't 
   * care about errors when we're quitting.
   */
  error_hook = exit_wrapper;

#ifdef SEND_BETA_MAIL_ADDR
  send_beta_test_log_mail ();
#endif
#ifdef HPPA_FIX_AND_CONTINUE
  cleanup_fixinfo();
#endif
#ifdef HP_IA64
  mixed_mode_cleanup ();
  mixed_mode_free_std_regs ();
#endif
  quit_force (args, from_tty);
}

/* Returns whether GDB is running on a terminal and whether the user
   desires that questions be asked of them on that terminal.  */

int
input_from_terminal_p ()
{
  return gdb_has_a_terminal () && (instream == stdin) & caution;
}

/* ARGSUSED */
static void
pwd_command (char *args, int from_tty)
{
  if (args)
    error ("The \"pwd\" command does not take an argument: %s", args);
  getcwd (gdb_dirbuf, sizeof (gdb_dirbuf));

  if (!STREQ (gdb_dirbuf, current_directory))
    printf_unfiltered ("Working directory %s\n (canonically %s).\n",
		       current_directory, gdb_dirbuf);
  else
    printf_unfiltered ("Working directory %s.\n", current_directory);
}

void
cd_command (char *dir, int from_tty)
{
  int len;
  /* Found something other than leading repetitions of "/..".  */
  int found_real_path;
  char *p;

  /* If the new directory is absolute, repeat is a no-op; if relative,
     repeat might be useful but is more likely to be a mistake.  */
  dont_repeat ();

  if (dir == 0)
    error_no_arg ("new working directory");

  dir = tilde_expand (dir);
  make_cleanup (free, dir);

  if (chdir (dir) < 0)
    perror_with_name (dir);
  else 
  if (nimbus_version)
    {
      printf_unfiltered ("\033\032\032:cd %s\n\032\032a", dir);
      gdb_flush (gdb_stdout);
    }

#if defined(_WIN32) || defined(__MSDOS__)
  /* There's too much mess with DOSish names like "d:", "d:.",
     "d:./foo" etc.  Instead of having lots of special #ifdef'ed code,
     simply get the canonicalized name of the current directory.  */
  dir = getcwd (gdb_dirbuf, sizeof (gdb_dirbuf));
#endif

  len = (int) strlen (dir);
  if (SLASH_P (dir[len - 1]))
    {
      /* Remove the trailing slash unless this is a root directory
         (including a drive letter on non-Unix systems).  */
      if (!(len == 1)		/* "/" */
#if defined(_WIN32) || defined(__MSDOS__)
	  && !(!SLASH_P (*dir) && ROOTED_P (dir) && len <= 3)	/* "d:/" */
#endif
	  )
	len--;
    }

  dir = savestring (dir, len);
  if (ROOTED_P (dir))
    current_directory = dir;
  else
    {
      if (SLASH_P (current_directory[strlen (current_directory) - 1]))
	current_directory = concat (current_directory, dir, NULL);
      else
	current_directory = concat (current_directory, SLASH_STRING, dir, NULL);
      free (dir);
    }

  /* Now simplify any occurrences of `.' and `..' in the pathname.  */

  found_real_path = 0;
  for (p = current_directory; *p;)
    {
      if (SLASH_P (p[0]) && p[1] == '.' && (p[2] == 0 || SLASH_P (p[2])))
	strcpy (p, p + 2);
      else if (SLASH_P (p[0]) && p[1] == '.' && p[2] == '.'
	       && (p[3] == 0 || SLASH_P (p[3])))
	{
	  if (found_real_path)
	    {
	      /* Search backwards for the directory just before the "/.."
	         and obliterate it and the "/..".  */
	      char *q = p;
	      while (q != current_directory && !SLASH_P (q[-1]))
		--q;

	      if (q == current_directory)
		/* current_directory is
		   a relative pathname ("can't happen"--leave it alone).  */
		++p;
	      else
		{
		  strcpy (q - 1, p + 3);
		  p = q - 1;
		}
	    }
	  else
	    /* We are dealing with leading repetitions of "/..", for example
	       "/../..", which is the Mach super-root.  */
	    p += 3;
	}
      else
	{
	  found_real_path = 1;
	  ++p;
	}
    }

  forget_cached_source_info ();

  if (from_tty)
    pwd_command ((char *) 0, 1);
}

struct source_cleanup_lines_args
{
  int old_line;
  char *old_file;
  char *old_pre_error;
  char *old_error_pre_print;
};

static void
source_cleanup_lines (PTR args)
{
  struct source_cleanup_lines_args *p =
  (struct source_cleanup_lines_args *) args;
  source_line_number = p->old_line;
  source_file_name = p->old_file;
  source_pre_error = p->old_pre_error;
  error_pre_print = p->old_error_pre_print;
}

/* ARGSUSED */
static void
do_fclose_cleanup (void *stream)
{
  fclose (stream);
}

void
source_command (char *args, int from_tty)
{
  FILE *stream;
  struct cleanup *old_cleanups;
  char *file = args;
  struct source_cleanup_lines_args old_lines;
  int needed_length;

  if (file == NULL)
    {
      error ("source command requires pathname of file to source.");
    }

  file = tilde_expand (file);
  old_cleanups = make_cleanup (free, file);

  stream = fopen (file, FOPEN_RT);
  if (!stream)
    {
      if (from_tty)
	perror_with_name (file);
      else
	return;
    }

  make_cleanup (do_fclose_cleanup, stream);

  old_lines.old_line = source_line_number;
  old_lines.old_file = source_file_name;
  old_lines.old_pre_error = source_pre_error;
  old_lines.old_error_pre_print = error_pre_print;
  make_cleanup (source_cleanup_lines, &old_lines);
  source_line_number = 0;
  source_file_name = file;
  source_pre_error = error_pre_print == NULL ? "" : error_pre_print;
  source_pre_error = savestring (source_pre_error, (int) strlen (source_pre_error));
  make_cleanup (free, source_pre_error);
  /* This will get set every time we read a line.  So it won't stay "" for
     long.  */
  error_pre_print = "";

  needed_length = (int) (strlen (source_file_name) + strlen (source_pre_error) + 80);
  if (source_error_allocated < needed_length)
    {
      source_error_allocated *= 2;
      if (source_error_allocated < needed_length)
	source_error_allocated = needed_length;
      if (source_error == NULL)
	source_error = xmalloc (source_error_allocated);
      else
	source_error = xrealloc (source_error, source_error_allocated);
    }

  read_command_file (stream);

  do_cleanups (old_cleanups);
}

/* ARGSUSED */
static void
echo_command (char *text, int from_tty)
{
  char *p = text;
  register int c;

  if (text)
    while ((c = *p++) != '\0')
      {
	if (c == '\\')
	  {
	    /* \ at end of argument is used after spaces
	       so they won't be lost.  */
	    if (*p == 0)
	      return;

	    c = parse_escape (&p);
	    if (c >= 0)
	      printf_filtered ("%c", c);
	  }
	else
	  printf_filtered ("%c", c);
      }

  /* Force this output to appear now.  */
  wrap_here ("");
  gdb_flush (gdb_stdout);
}

/* ARGSUSED */
static void
dont_repeat_command (char *ignored, int from_tty)
{
  *line = 0;			/* Can't call dont_repeat here because we're not
				   necessarily reading from stdin.  */
}

/* Called by ``set endian little''.  */
void
set_endian_little (char *args, int from_tty)
{
#ifdef TARGET_BYTE_ORDER_SELECTABLE
  target_byte_order = LITTLE_ENDIAN;
  target_byte_order_auto = 0;
#else
  printf_unfiltered ("Byte order is not selectable.");
  show_endian (args, from_tty);
#endif
}


/* Functions to manipulate command line editing control variables.  */

/* Number of commands to print in each call to show_commands.  */
#define Hist_print 10
static void
show_commands (char *args, int from_tty)
{
  /* Index for history commands.  Relative to history_base.  */
  int offset;

  /* Number of the history entry which we are planning to display next.
     Relative to history_base.  */
  static int num = 0;

  /* The first command in the history which doesn't exist (i.e. one more
     than the number of the last command).  Relative to history_base.  */
  int hist_len;

extern HIST_ENTRY *history_get (int);

  /* Print out some of the commands from the command history.  */
  /* First determine the length of the history list.  */
  hist_len = history_size;
  for (offset = 0; offset < history_size; offset++)
    {
      if (!history_get (history_base + offset))
	{
	  hist_len = offset;
	  break;
	}
    }

  if (args)
    {
      if (args[0] == '+' && args[1] == '\0')
	/* "info editing +" should print from the stored position.  */
	;
      else
	/* "info editing <exp>" should print around command number <exp>.  */
	num = (int) ((parse_and_eval_address (args) - history_base) - Hist_print / 2);
    }
  /* "show commands" means print the last Hist_print commands.  */
  else
    {
      num = hist_len - Hist_print;
    }

  if (num < 0)
    num = 0;

  /* If there are at least Hist_print commands, we want to display the last
     Hist_print rather than, say, the last 6.  */
  if (hist_len - num < Hist_print)
    {
      num = hist_len - Hist_print;
      if (num < 0)
	num = 0;
    }

  for (offset = num; offset < num + Hist_print && offset < hist_len; offset++)
    {
      printf_filtered ("%5d  %s\n", history_base + offset,
		       (history_get (history_base + offset))->line);
    }

  /* The next command we want to display is the next one that we haven't
     displayed yet.  */
  num += Hist_print;

  /* If the user repeats this command with return, it should do what
     "show commands +" does.  This is unnecessary if arg is null,
     because "show commands +" is not useful after "show commands".  */
  if (from_tty && args)
    {
      args[0] = '+';
      args[1] = '\0';
    }
}

/* Called by do_setshow_command.  */
/* ARGSUSED */
static void
set_history_size_command (char *args, int from_tty, struct cmd_list_element *c)
{
  if (history_size == INT_MAX)
    unstifle_history ();
  else if (history_size >= 0)
    stifle_history (history_size);
  else
    {
      history_size = INT_MAX;
      error ("History size must be non-negative");
    }
}

/* ARGSUSED */
static void
set_history (char *args, int from_tty)
{
  printf_unfiltered ("\"set history\" must be followed by the name of a history subcommand.\n");
  help_list (sethistlist, "set history ", -1, gdb_stdout);
}

/* ARGSUSED */
static void
show_history (char *args, int from_tty)
{
  cmd_show_list (showhistlist, from_tty, "");
}

int info_hp_aries_pa32 = 0;  /* Default assume not running under Aries on PA32*/

int info_hp_aries_pa64 = 0;  /* Default assume not running under Aries on PA64*/

/* Patterned on set_hp_aries_pa64 below */

static void
set_hp_aries_pa32 (char *args, int from_tty, struct cmd_list_element *c)
{
  char *cmdname = "hp-aries-pa32";
  struct cmd_list_element *showcmd;

  showcmd = lookup_cmd_1 (&cmdname, showlist, NULL, 1);

  if (info_verbose)
    {
      c->doc = 
         "Set support for debugging Aries on PA32.  Don't set dld_flags bits.";
      showcmd->doc = 
        "Show support for debugging Aries on PA32.  Don't set dld_flags bits.";
    }
  else
    {
      c->doc = "Set support for debugging Aries on PA32.\n\n\
Usage:\n\tset hp-aries-pa32 [on | off]\n";
      showcmd->doc = "Show support for debugging Aries on PA32.\n\n\
Usage:\n\tshow hp-aries-pa32\n";
    }
} /* end set_hp_aries_pa32 */

/* Patterned on set_verbose below */

static void
set_hp_aries_pa64 (char *args, int from_tty, struct cmd_list_element *c)
{
  char *cmdname = "hp-aries-pa64";
  struct cmd_list_element *showcmd;

  showcmd = lookup_cmd_1 (&cmdname, showlist, NULL, 1);

  if (info_verbose)
    {
      c->doc = 
         "Set support for debugging Aries on PA64.  Don't set dld_flags bits.";
      showcmd->doc = 
        "Show support for debugging Aries on PA64.  Don't set dld_flags bits.";
    }
  else
    {
      c->doc = "Set support for debugging Aries on PA64.\n\n\
Usage:\n\tset hp-aries-pa64 [on | off]\n";
      showcmd->doc = "Show support for debugging Aries on PA64.\n\n\
Usage:\n\tshow hp-aries-pa64\n";
    }
} /* end set_hp_aries_pa64 */

int info_threadverbose = 1;     /* Default verbose thread messages on */

#if 0 /* 11/19/04 - JAGae93202-Disabling threadverbose feature */
/* Patterned on set_verbose below */

static void
set_threadverbose (char *args, int from_tty, struct cmd_list_element *c)
{
  char *cmdname = "threadverbose";
  struct cmd_list_element *showcmd;

  showcmd = lookup_cmd_1 (&cmdname, showlist, NULL, 1);

  if (info_verbose)
    {
      c->doc = "Set verbose printing of thread informational messages.";
      showcmd->doc = "Show verbose printing of thread informational messages.";
    }
  else
    {
      c->doc = "Set thread verbosity.";
      showcmd->doc = "Show thread verbosity.";
    }
}
#endif /* 0 11/19/04 - JAGae93202-Disabling threadverbose feature */

int info_verbose = 0;		/* Default verbose msgs off */

/* Called by do_setshow_command.  An elaborate joke.  */
/* ARGSUSED */
static void
set_verbose (char *args, int from_tty, struct cmd_list_element *c)
{
  char *cmdname = "verbose";
  struct cmd_list_element *showcmd;

  showcmd = lookup_cmd_1 (&cmdname, showlist, NULL, 1);

  if (info_verbose)
    {
      c->doc = "Set verbose printing of informational messages.";
      showcmd->doc = "Show verbose printing of informational messages.";
    }
  else
    {
      c->doc = "Set verbosity.\n\nUsage:\n\tset verbose [on | off]\n";
      showcmd->doc = "Show verbosity.\n\nUsage:\n\tshow verbose\n";
    }
}

static void
float_handler (int signo)
{
  /* This message is based on ANSI C, section 4.7.  Note that integer
     divide by zero causes this, so "float" is a misnomer.  */
  signal (SIGFPE, float_handler);
  error ("Erroneous arithmetic operation.");
}

static void
set_debug (char *arg, int from_tty)
{
  printf_unfiltered ("\"set debug\" must be followed by the name of a print subcommand.\n");
  help_list (setdebuglist, "set debug ", -1, gdb_stdout);
}

static void
show_debug (char *args, int from_tty)
{
  cmd_show_list (showdebuglist, from_tty, "");
}

static void
init_cmd_lists ()
{
  cmdlist = NULL;
  infolist = NULL;
  enablelist = NULL;
  disablelist = NULL;
  togglelist = NULL;
  stoplist = NULL;
  deletelist = NULL;
  enablebreaklist = NULL;
  setlist = NULL;
  unsetlist = NULL;
  showlist = NULL;
  setlogginglist = NULL;
  showlogginglist = NULL;
  sethistlist = NULL;
  showhistlist = NULL;
  unsethistlist = NULL;
  maintenancelist = NULL;
  maintenanceinfolist = NULL;
  maintenanceprintlist = NULL;
  setprintlist = NULL;
  showprintlist = NULL;
  setchecklist = NULL;
  showchecklist = NULL;
}

/* Init the history buffer.  Note that we are called after the init file(s)
 * have been read so that the user can change the history file via his
 * .gdbinit file (for instance).  The GDBHISTFILE environment variable
 * overrides all of this.
 */

void
init_history ()
{
  char *tmpenv;

  tmpenv = getenv ("HISTSIZE");
  if (tmpenv)
    history_size = atoi (tmpenv);
  else if (!history_size)
    history_size = 256;

  stifle_history (history_size);

  tmpenv = getenv ("GDBHISTFILE");
  if (tmpenv)
    history_filename = savestring (tmpenv, (int) strlen (tmpenv));
  else if (!history_filename)
    {
      /* We include the current directory so that if the user changes
         directories the file written will be the same as the one
         that was read.  */
#ifdef __MSDOS__
      /* No leading dots in file names are allowed on MSDOS.  */
      history_filename = concat (current_directory, "/_gdb_history", NULL);
#else
      history_filename = concat (current_directory, "/.gdb_history", NULL);
#endif
    }
  if (nimbus_version && history_filename)
    {
      printf_unfiltered ("\033\032\032:se history %s\n\032\032A",
                     history_filename);
      gdb_flush (gdb_stdout);
    }
  read_history (history_filename);
}

#ifdef HP_XMODE

/* 
 * xmode_open_record_file(void)
 *
 * Arguments: none
 *
 * Returns: none
 *
 * Side Effects: 
 *     Sets xmode_usr_env_record to file pointer of file used to
 *          record user commands.
 *     xmode_usr_env_record_name to name of file used to record
 *          user commands.
 *
 *
 * Grabs a temporary name in the form of /var/tmp/.... and attempts
 * to open the file for writing.  This temporary file will be used
 * to record user environment commands set in this session.  This is
 * called by xmode_record_user_env_command().
 */

static void 
xmode_open_record_file (void)
{
  if (xmode_user_env_record == NULL) 
    {
      int fd;
      tmpnam (xmode_user_env_record_name);
      unlink (xmode_user_env_record_name);
      if ((fd = open (xmode_user_env_record_name, 
                      O_RDWR | O_EXCL | O_CREAT, 0777)) != -1)
        {
          xmode_user_env_record = fdopen (fd, "w");
        }
    }
} 

/*
 * xmode_record_user_env_command(command)
 *
 * Argument: string holding command to record.
 *
 * Returns: none
 *
 * Opens the temporary record file if it is not opened,
 * determines if command passed in is one of the user environment
 * commands such as "set" and "dir", and if so, writes the command
 * out to the temporary file.  This file will be passed with the
 * --switching_modes option if we end up launching the "other" gdb.
 * This is called by execute_command() in top.c.
 *
 */

static void 
xmode_record_user_env_command (char *command)
{

  static int could_not_open = 0;   
  char *ptr, *arg;
  register struct cmd_list_element *c;
 
  /* For whatever reason, we tried and could not open the record file.
     Don't even attempt to write into this file -- just return */

  if (could_not_open)                
    return;

  /* Temporary file has not yet been opened.  Try and open it. */

  if (xmode_user_env_record == NULL)
    {
      xmode_open_record_file ();
      if (xmode_user_env_record == NULL) /* error */
        { 
          could_not_open = 1;
          return;
        }
    }
  
  /* If we have a user environment command such as "set",  write it
     out to the temporary file */

  if (xmode_is_env_command (command)) 
    {
      fprintf (xmode_user_env_record,"%s\n", command);
    }

}

/*
 * xmode_is_env_command(command)
 *
 * Argument:  string which contains command to record
 *
 * Returns:  1 if command affects user environment; 0 otherwise.
 *
 * Check to see if the command is a command which affects the user
 * environment.  User environment commands are:  set, unset, dir,
 * tty, handle, and objectdir. This function is called by
 * xmode_record_user_env_command().
 */

 
static int 
xmode_is_env_command (char *command)
{
  char *ptr;
  register struct cmd_list_element *c;


  /* 
   * check for some set commands.  We do not want to record those
   * that are specific to one executable.  Examples are:
   * set gnutarget
   * set args
   * set variable
   */


  if (strstr (command,"set") == command)
    {    

      /* we have a set command */

      ptr = command;

      /*
       * lookup the command using the gdb mechanism.  This returns
       * all sorts of information like whether or not we have a valid
       * item to set, whether or not we are setting a value to a 
       * variable, etc.
       */

      c = lookup_cmd (&ptr, cmdlist, "", 0, 1);


      /*
       * Do not want to record set commands which are really not
       * designated as a set command.  The exception is set enviornment...
       * which for some reason has a type which is a not_set_cmd.  
       */

      if ((c->type == not_set_cmd) && (strcmp (c->name,"environment") != 0))
        return 0;

      /* 
       * Not valid across executables -- return 0.
       */

      else if (!strcmp (c->name,"gnutarget"))
        return 0;

      /*
       * Setting values to variables are commands which 
       * affect the user enviornment, but are not valid
       * across executables.  Return 0.
       */

      else if (!strcmp (c->name,"args"))
        return 0;

      /*
       * Setting values to variables are not commands which 
       * affect the user enviornment.
       */

      else if (c->class == class_vars)
        return 0;
    
    }

  /* These commands are valid user enviornment commands */

  if ((strstr (command, "handle") == command) ||
      (strstr (command, "set") == command) ||
      (strstr (command, "unset") == command) ||
      (strstr (command, "dir") == command) ||
      (xdb_commands && (strstr(command,"D ") == command)) ||
      (dbx_commands && (strstr(command,"use") == command)) ||
      (strstr (command, "tty") == command) ||
      (strstr (command, "path") == command) ||
      (strstr (command, "objectdir") == command))
    return 1;

  /*
   * If we are using the tui, save some tui commands too.
   */

  if (tui_version) 
    {
      if ((strstr (command, "layout") == command) ||
          (strstr (command, "dis") == command) ||
          (strstr (command, "tabset") == command) ||
          (strstr (command, "winheight") == command) ||
          (strstr (command, "focus") == command) ||
          (command[0] == 'w') ||
          (strstr (command, "ts") == command) ||          
          (strstr (command, "td") == command))
        return 1;
    }

  return 0;
}

/*
 * xmode_launch_other_gdb(file, reason)
 * 
 * Argument: name of file which caused cross mode to happen
 *           reason xmode happened (executable, corefile, or symfile
 *                                  not in the same format.)
 *
 * Assumptions: gdb32 and gdb64 are in the same place on disk
 *              in a directory that can be found by examining
 *              the user's path variable.  
 *
 * Returns:  This routine never returns.
 *
 * This function does the work of launching the "other" gdb.  
 * On PA: If this gdb was configured for 64-bits, the "other"
 *        gdb will be ./gdb32.  
 *
 * On IA: If this gdb was configured for IA, the "other"
 *        gdb will be a PA gdb.  
 * 
 * This routine figures out which gdb to launch, creates the 
 * --switching_modes option with the record file to which we are save
 * user commands, grabs the terminal back from the debugged program,
 * creates the appropriate argv, and calls execvp to launch the "other" gdb.
 * This routine is called from exec_file_attach() in exec.c and also in
 * corelow.c and symfile.c.
 */

void
xmode_launch_other_gdb (char *file, enum xmode_cause reason)
{
  char *gdb_executable;
  char *path = NULL;
  char *modified_path = NULL;
  char *tmp_file_arg = NULL;
  char *exec_file_arg = NULL;
  char *core_file_arg = NULL;
  char *getcore_arg = NULL;
  char *sym_file_arg = NULL;
#ifdef HP_IA64
  char *default_path = ":/usr/ccs/bin";
#else /* not HP_IA64 */
  char *default_path = ":/opt/langtools/bin";
#endif /* HP_IA64 */

#ifdef HP_IA64
  /* Stacey 7/12/2002
     Don't switch to PA gdb if we're debugging aries */
  if (debugging_aries || debugging_aries64)
    return;
#endif

#ifdef SEND_BETA_MAIL_ADDR
  send_beta_test_log_mail ();
#endif

   path = getenv("PATH");
   modified_path = set_path(path);

  /* Figure out which gdb executable to launch. */

  if (is_IA_machine) /* JAGae35325 - PA gdb running on IA */
  {
    if (openp (modified_path, 1, "gdb", O_RDONLY, 0, &gdb_executable) < 0) /* Cannot find IA gdb in the given path */
      {
        printf_unfiltered ("IPF gdb  could not be found. Try adding the directory in\n");
	printf_unfiltered ("which IPF gdb lives to your path enviornment variable. \n");
	error ("Switch to IPF mode failed\n");
      }
    else  /* Found the IA gdb and invoked it */
      {
        if (reason == xmode_executable)
	{
	  printf_unfiltered ("\nDetected IPF executable.\nInvoking %s", gdb_executable);
	  printf_unfiltered ("\nUse \"run\" to continue execution\n");
	 }
	else if (reason == xmode_corefile || reason == xmode_getcore)
          printf_unfiltered ("\nDetected IPF core-file.\nInvoking %s\n", gdb_executable);
	else if (reason == xmode_symfile)
	  printf_unfiltered ("Detected IPF symbol-file.\nInvoking %s\n", gdb_executable);

	}
     }
  else  /* GDB running on PA */
  {
#ifdef HP_IA64
  /* Stacey 3/11/2002
   * This is an ia gdb, try to start gdbpa if we have a PA executable,
   * core file, or symbol file and we're not debugging Aries.  
   * 
   * In the special case that we're debugging Aries, we ignore the switching
   * code below.  The outside world thinks the PA executable file 
   * (given to gdb as an argument) will become a real child process.  This 
   * isn't true.  PA programs don't 'run' under aries.  They're just input 
   * data aries uses to emulate the behavior of a PA program.  Therefore, if 
   * we're debugging aries, we don't ever have to check the format of the
   * executable since it's just input data.  Aries is definitely an IPF 
   * library, so continue debugging it with IPF gdb.  The switching code will 
   * actually cause gdb to core dump if it's run when debugging aries.  */

  if (!debugging_aries && !debugging_aries64)
    {
      if (openp (modified_path, 1, "gdbpa", O_RDONLY, 0, &gdb_executable) < 0)
	{
	  printf_unfiltered ("gdbpa could not be found.   Try adding the"
			     " directory in\nwhich gdbpa lives to your path"
			     "environment variable.\n");
	  error ("Switch to PA mode failed\n");
	}
      if (reason == xmode_executable)
	{
	  printf_unfiltered ("\nDetected PA executable.\nInvoking %s.\n"
			     "Use \"run\" to continue execution.\n",
			     gdb_executable);
	}
      if (reason == xmode_corefile || reason == xmode_getcore)
	{
	  printf_unfiltered ("\nDetected PA core file\nInvoking %s.\n"
			     "Use \"run\" to continue execution.\n",
			     gdb_executable);
	}
      if (reason == xmode_symfile)
	{
	  printf_unfiltered ("\nDetected PA symbol file\nInvoking %s.\n"
			     "Use \"run\" to continue execution.\n",
			     gdb_executable);
	}  
    }
    
#else /* not HP_IA64 */
  if (xmode_this_gdb_understands == xmode_pa32)
    {
      if (openp (modified_path, 1, "gdb64", O_RDONLY, 0, &gdb_executable) < 0) 
        {
          printf_unfiltered ("gdb64 could not be found.   Try adding the directory in\n");
          printf_unfiltered ("which gdb64 lives to your path environment variable.\n");
          error ("Switch to PA64 mode failed\n");
        }
      if (reason == xmode_executable)
        {
#ifndef HPUX_1020
	  /* Don't allow to debug 64 bit applications on 32 bit machines */
          if ( IS_32BIT_KERNEL ) 
            {
               printf_unfiltered ("\nerror: cannot debug a 64-bit executable on a 32-bit system;\n");
               error ("program terminated. \n");
	    }
#endif
          printf_unfiltered ("\nDetected 64-bit executable.\nInvoking %s.\n",gdb_executable);
          printf_unfiltered ("Use \"run\" to continue execution.\n");
        }
      else if (reason == xmode_corefile || reason == xmode_getcore)
        {
#ifndef HPUX_1020
          if ( IS_32BIT_KERNEL ) 
            {
               warning ("Detected 64-bit executable and core file on a");
               printf_unfiltered ("32-bit system; debugging is limited\n");
	    }
#endif
          printf_unfiltered ("\nDetected 64-bit core file.\nInvoking %s.\n",gdb_executable);
        }
      else if (reason == xmode_symfile) 
        {
          printf_unfiltered ("\nDetected 64-bit symbol file.\nInvoking %s.\n",gdb_executable);
        }
    }
  else 
    {
      if (openp (modified_path, 1, "gdb32", O_RDONLY, 0, &gdb_executable) < 0) 
        {
          printf_unfiltered ("gdb32 could not be found.  Try adding the directory in\n");
          printf_unfiltered ("which gdb32 lives to your path environment variable.\n");
          error ("Switch to PA32 mode failed\n");
        }
      if (reason == xmode_executable)
        {
          printf_unfiltered ("\nDetected 32-bit executable.\nInvoking %s.\n",gdb_executable);
          printf_unfiltered ("Use \"run\" to continue execution.\n");
        }
      else if (reason == xmode_corefile || reason == xmode_getcore)
        {
          printf_unfiltered ("\nDetected 32-bit core file.\nInvoking %s.\n",gdb_executable);
        }
      else if (reason == xmode_symfile)
        {
          printf_unfiltered ("\nDetected 32-bit symbol file.\nInvoking %s.\n",gdb_executable);
        }
    }
#endif /* HP_IA64 */
  }

  /* Ensure messages are printed before exec'ing cross-mode gdb. */
  gdb_flush (gdb_stdout);

  /* 
   *  create --switching_modes argument which looks like:
   *
   *  --switching_modes=/var/tmp/....
   *
   */

  tmp_file_arg = (char *) xmalloc (strlen ("--switching_modes=") +
                                   strlen (xmode_user_env_record_name) +
                                   1);

  strcpy (tmp_file_arg,"--switching_modes=");

  /* If a gdb client desires notification of impending mode switch,
   * it will have set xmode_hook.
   */
  if (xmode_hook != NULL)
    {
      if (xmode_user_env_record == NULL) 
        xmode_open_record_file ();

      /* If we can't open it, we're stuck. */
      if (xmode_user_env_record == NULL) 
        error ("Can't save gdb server state. May loose some server state information.\n");
      else
        xmode_hook (xmode_user_env_record);
    }

  if (xmode_user_env_record != NULL) 
    {
      strcat (tmp_file_arg,xmode_user_env_record_name);
      fclose (xmode_user_env_record);
    }

  /* Grab the terminal from the debugged process -- otherwise gdb
     will not start up correctly. */

  target_terminal_ours_for_output ();

  /* 
   * Create a new argv for the "other" gdb.  
   *
   * argv[0] = gdb executable
   * argv[1 .. n] = command line args which were saved
   * argv[n+1] = --switching_mode_executable==file 
   */

  xmode_save_argv[0]=gdb_executable;
  xmode_save_argv[++xmode_save_argc]=tmp_file_arg;

  /* 
   * Create --switching_mode_executable=file argument or
   * Create --switching_mode_corefile=file argument or
   * Create --switching_mode_symfile=file argument.
   */

  if (reason == xmode_executable)
    {
      exec_file_arg = (char *) xmalloc (strlen (file) +
                                        strlen ("--switching_modes_executable=") +
                                        1);

      strcpy (exec_file_arg,"--switching_modes_executable=");
      strcat (exec_file_arg,file);
      xmode_save_argv[++xmode_save_argc]=exec_file_arg;
    }
  else if (reason == xmode_corefile)
    {
      core_file_arg = (char *) xmalloc (strlen(file) +
                                        strlen("--switching_modes_corefile=") +
                                        1);
      strcpy (core_file_arg,"--switching_modes_corefile=");
      strcat (core_file_arg,file);
      xmode_save_argv[++xmode_save_argc]=core_file_arg;
    }    
  else if (reason == xmode_getcore)
    {
      getcore_arg = (char *) xmalloc (strlen(file) +
                                        strlen("--switching_modes_getcore=") +
                                        1);
      strcpy (getcore_arg,"--switching_modes_getcore=");
      strcat (getcore_arg,file);
      xmode_save_argv[++xmode_save_argc]=getcore_arg;
    }    
  else if (reason == xmode_symfile)
    {
      sym_file_arg = (char *) xmalloc (strlen(file) +
                                        strlen("--switching_modes_symfile=") +
                                        1);
      strcpy (sym_file_arg,"--switching_modes_symfile=");
      strcat (sym_file_arg,file);
      xmode_save_argv[++xmode_save_argc]=sym_file_arg;
    }    

  /* If gdb is running as a CORBA server (which is the case when
   * xmode_hook is not NULL), then fork() before exec()ing a new
   * gdb; else, simply exec(). fork()ing the server allows us to
   * do a proper exit(), which in turn causes the server's destructors
   * to be called (which in turn shut down the CORBA connection).
   *
   * The server typically has about 25-35 pipes, sockets, devices, and
   * regular files open; non-server gdb, 10-20 open. Make sure these are
   * closed before exec()ing (or risk gdb using up it's open file resource).
   * Note that closing a non-existent file descriptor is harmless.
   */

  if (xmode_hook)
    {
      int fd;

      /* Flush output before exiting current server. */
      gdb_flush (gdb_stdout);
      gdb_flush (gdb_stderr);
  
      switch(fork())
        {
        case -1:
          printf_unfiltered ("Fork for cross-mode gdb failed");
          exit(1);
        case 0:
          for (fd = 3; fd <= 40; fd++)
            close (fd);
          execvp (gdb_executable, (char **)xmode_save_argv);
          fprintf (stderr, "exec of cross-mode gdb failed %s", 
					safe_strerror(errno));
          kill (getppid(), SIGKILL);
          exit (1); 
        default:
          exit (0);
          break;
        }
    }
  else
    {
      int fd;
      for (fd = 3; fd <= 25; fd++)
        close (fd);
      execvp (gdb_executable, (char **)xmode_save_argv);
    }
}    

/*
 * xmode_exec_format_is_different(char *executable)
 *
 * Arguments:  name of file to check executable format.
 * 
 * Returns: 1 if executable is in different format from one we understand
 *
 * This function checks the executable to determine if the
 * executable is in a different format from one we understand.  It does 
 * this by looking at the magic number of the file.
 * This function is called from exec_file_attach() in exec.c.
 */

#include <elf.h>
#include <elf_parisc.h>
#include <elf_em.h>

extern enum exec_format xmode_exec_format; /* JAGaf36204 - Switching between PA and IPF */

int 
xmode_exec_format_is_different (char *executable)
{
  char buf[10];
  int is_elf_format = 0;
  int different_from_our_format = 0;
  int fd;
  int symbols;
  Elf64_Ehdr elfhdr;  /* JAGae35325 */
  struct utsname name;
  int i;
  is_IA_machine = 0; 
  xmode_exec_format = exec_pa32; /* PA32 */
#ifdef HP_IA64
  struct header somhdr;
#endif
 
  /* Read magic number */

  fd = open (executable,O_RDONLY, 0);

  if (fd < 0) 
    {
      return 0;
    }
      

  if (read (fd,buf,10) == 10) 
    {
         
      /* skip archive files */

      if (strncmp (buf, "!<arch>", 7) == 0)
	{
	  close (fd);
	  return (0);
	}

      /* If true, we have a ELF executable */

      if ((buf[0] == 0x7f) &&
	  (buf[1] == 'E') &&
	  (buf[2] == 'L') &&
	  (buf[3] == 'F'))
         {
            is_elf_format = 1;
#ifdef HP_IA64
            /* JAGag22156: jini: Oct 2006. Set 'xmode_exec_format'. */
            xmode_exec_format = exec_ia64; /* IA64 */
#else
            xmode_exec_format = exec_pa64; /* PA64 */
#endif
         }


#ifdef HP_IA64
      /* We have an IA gdb.  If this executable isn't in elf format,
	 it must be a PA file.  So we need to switch gdb's - return 1 */

      /* jini: Assume this is IPF till proven otherwise. */
      xmode_exec_format = exec_ia64;
      if (!is_elf_format)
        {
          /* JAGag21714:
             jini: mixed mode changes: 060910: Check if this is a PA SOM
             library. */
	  if (lseek (fd, 0, SEEK_SET) == -1) 
	    { /* ERROR */
	      close (fd);
	      return 0;
	    }
          if (read (fd, &somhdr, sizeof (somhdr)) == sizeof (somhdr))
            {
    	      if ((somhdr.a_magic != DL_MAGIC) && (somhdr.a_magic != SHL_MAGIC))
                {
                  different_from_our_format = 1;
                  /* Check whether loaded file is PA executable,
                     if not, dont switch IA gdb to PA-gdb (return 0)
		  */
                  if ((somhdr.a_magic != EXEC_MAGIC) 
		     && (somhdr.a_magic != SHARE_MAGIC))
                   {
                     /* Assume it is core dump (by PA32 bit process) */
                     char cmd[60+PATH_MAX] = "/usr/bin/file ";
                     strcat (cmd, executable);
                     strcat (cmd, " | /usr/bin/grep \"core file\" > /dev/null");
                     /* return 0 if it is not core file, system | grep
                        returns 0 on successful
                     */
                     /* Check before calling system, 
		        /usr/bin/file & /usr/bin/grep exist
		     */
                     struct stat buf;
                     if ((stat ("/usr/bin/file", &buf) == 0)
                        && (stat ("/usr/bin/grep" ,&buf) == 0))
                      if (system(cmd))
                       return 0;
                   }
                  xmode_exec_format = exec_pa32;
                }
	
            }
	  else /* ERROR */
	    {
	      close (fd);
	      return 0;
	    }
        }
      else
	{
	  /* We have an elf file.  So, this could be either a PA64 file
	     or an IA64 file */

	  if (lseek (fd,0,SEEK_SET) == -1) 
	    { /* ERROR */
	      close (fd);
	      return 0;
	    }
	  if (read (fd,&elfhdr,sizeof(elfhdr)) == sizeof(elfhdr)) 
	    if (elfhdr.e_machine == EM_PARISC)
              {
                 /* jini: 060511: With mixed mode support, we tolerate
                    PA shared libraries. 'different_from_our_format' has to
                    be set for the PA main module. */
                if (elfhdr.e_type != ET_DYN)
                  {
	            different_from_our_format = 1;
                    xmode_exec_format = exec_pa64;
                  }
              }
	  else /* ERROR */
	    {
	      close (fd);
	      return 0;
	    }
	}	  

#else /* not HP_IA64 - assume we're on PA */
      /* If true, we have a PA64 executable or IA64 executable */
      if (lseek (fd,0,SEEK_SET) == -1)
       { /* ERROR */
         close (fd);
         return 0;
       }

       /* Check whether it is an IA executable */
    if (read (fd, &elfhdr, sizeof(elfhdr)) == sizeof(elfhdr))
      if (elfhdr.e_machine == EM_IA64) /* IA executable */
       {
          uname (&name);  /* Check whether we are on IA machine */
          xmode_exec_format = exec_ia64;   /* IPF */
	  if(strstr(name.machine, "ia64")) /* IA machine */
	  {
	    different_from_our_format = 1;
	    is_IA_machine = 1;
	  }
	  else /* PA machine - cannot invoke IA executable - Quit*/
	  {
	   printf_unfiltered("\nDetected IPF executable.\nCannot invoke IPF gdb on a PA machine.\n");
	   printf_unfiltered("Program not loaded.\n");
           exit(0);  /* Do not continue if and IPF executable is loaded on a PA gdb */
	   }
      }
      else
      {
     
      /* If we have a 32-bit executable and we only understand PA64,
         return 1 */

      if ( (xmode_this_gdb_understands == xmode_pa64) && !is_elf_format)
	different_from_our_format = 1;

      /* If we have a 64-bit executable and we only understand PA32,
         return 1 */

      else if (is_elf_format && xmode_this_gdb_understands != xmode_pa64)
        different_from_our_format = 1;
      }
#endif
    } 
  
  /* close the file */
  close(fd);
  return (different_from_our_format);
}  
/*
 * xmode_save_command_line_args(argc,argv)
 * 
 * Arguments:  argc, argv
 *
 * Returns: nothing
 *
 * This function saves all command line arguments to gdb EXCEPT
 * -switching_modes, -symbols, -exec, -se, -core, the filename of
 * the file with which we were invoked, and the core file with
 * which we were invoked.  These are not passed to the "other"
 * gdb as they are not relevant to the new, launched gdb.
 * This function is called from main() in main.c.
 */

void 
xmode_save_command_line_args (int argc,char **argv)
{
  char *arg;
  int index;
 
  xmode_save_argc = 0;

  index = 1;
  while (index < argc) 
    {
      arg = argv[index];
      index++;

      /* do not save argument --switching_modes.  This is not
         a user argument */

      if (strstr (arg,"--switching_modes=") == arg) 
        continue;

      if (strstr (arg,"--switching_modes_executable=") == arg)
        continue;

      if (strstr (arg,"--switching_modes_corefile=") == arg)
        continue;

      if (strstr (arg,"--switching_modes_getcore=") == arg)
        continue;

      if (strstr (arg,"--switching_modes_symfile=") == arg)
        continue;

      else 
        {
          xmode_save_argc++;
          xmode_save_argv[xmode_save_argc]=arg;
        }

    }
}

#endif /* HP_XMODE */

static void
init_main ()
{
  struct cmd_list_element *c;

  /* If we are running the asynchronous version,
     we initialize the prompts differently. */
  if (!event_loop_p)
    {
      gdb_prompt_string = savestring (DEFAULT_PROMPT, (int) strlen (DEFAULT_PROMPT));
    }
  else
    {
      /* initialize the prompt stack to a simple "(gdb) " prompt or to
         whatever the DEFAULT_PROMPT is. */
      the_prompts.top = 0;
      PREFIX (0) = "";
      PROMPT (0) = savestring (DEFAULT_PROMPT, (int) strlen (DEFAULT_PROMPT));
      SUFFIX (0) = "";
      /* Set things up for annotation_level > 1, if the user ever decides
         to use it. */
      async_annotation_suffix = "prompt";
      /* Set the variable associated with the setshow prompt command. */
      new_async_prompt = savestring (PROMPT (0),(int)  strlen (PROMPT (0)));

      /* JYG: along with the prompt stack idea, --annotate=2
	 command line option needs extra processing. */
      if (annotation_level > 1)
	set_async_annotation_level (NULL, 0, NULL);
    }
  gdb_prompt_escape = 0;	/* default to none.  */

  /* Set the important stuff up for command editing.  */
  command_editing_p = 1;
  history_expansion_p = 0;
  write_history_p = 0;

  /* Setup important stuff for command line editing.  */
  rl_completion_entry_function = (int (*)()) readline_line_completion_function;
  rl_completer_word_break_characters = gdb_completer_word_break_characters;
  rl_completer_quote_characters = gdb_completer_quote_characters;
  rl_readline_name = "gdb";

  /* Define the classes of commands.
     They will appear in the help list in the reverse of this order.  */

  add_cmd ("internals", class_maintenance, NO_FUNCTION,
	   "Maintenance commands.\n\
Some gdb commands are provided just for use by gdb maintainers.\n\
These commands are subject to frequent change, and may not be as\n\
well documented as user commands.",
	   &cmdlist);
  add_cmd ("obscure", class_obscure, NO_FUNCTION, "Obscure features.", &cmdlist);
  add_cmd ("aliases", class_alias, NO_FUNCTION, "Aliases of other commands.", &cmdlist);
  add_cmd ("user-defined", class_user, NO_FUNCTION, "User-defined commands.\n\
The commands in this class are those defined by the user.\n\
Use the \"define\" command to define a command.", &cmdlist);
  add_cmd ("support", class_support, NO_FUNCTION, "Support facilities.", &cmdlist);
  if (!dbx_commands)
    add_cmd ("status", class_info, NO_FUNCTION, "Status inquiries.", &cmdlist);
  add_cmd ("files", class_files, NO_FUNCTION, "Specifying and examining files.", &cmdlist);
  add_cmd ("breakpoints", class_breakpoint, NO_FUNCTION, "Making program stop at certain points.", &cmdlist);
  add_cmd ("data", class_vars, NO_FUNCTION, "Examining data.", &cmdlist);
  add_cmd ("stack", class_stack, NO_FUNCTION, "Examining the stack.\n\
The stack is made up of stack frames.  Gdb assigns numbers to stack frames\n\
counting from zero for the innermost (currently executing) frame.\n\n\
At any time gdb identifies one frame as the \"selected\" frame.\n\
Variable lookups are done with respect to the selected frame.\n\
When the program being debugged stops, gdb selects the innermost frame.\n\
The commands below can be used to select other frames by number or address.",
	   &cmdlist);
  add_cmd ("running", class_run, NO_FUNCTION, "Running the program.", &cmdlist);

  add_com ("pwd", class_files, pwd_command,
	"Print working directory. This is used for your program as well.\n\n\
Usage:\n\tpwd\n\n");

  c = add_cmd ("cd", class_files, cd_command,
	       "Set working directory to DIR for debugger and program being debugged.\n\n\
Usage:\n\tcd <DIR>\n\n\
The change does not take effect for the program being debugged\n\
until the next time it is started.", &cmdlist);
  c->completer = filename_completer;

  /* The set prompt command is different depending whether or not the
     async version is run. NOTE: this difference is going to
     disappear as we make the event loop be the default engine of
     gdb. */
  if (!event_loop_p)
    {
      add_show_from_set
	(add_set_cmd ("prompt", class_support, var_string,
		      (char *) &gdb_prompt_string, "Set gdb's prompt.\n\nUsage:\n"
                      "To set new value:\n\tset prompt <PROMPT>\nTo see current "
                      "value:\n\tshow prompt\n",
		      &setlist),
	 &showlist);
    }
  else
    {
      c = add_set_cmd ("prompt", class_support, var_string,
		       (char *) &new_async_prompt, "Set gdb's prompt.\n\nUsage:\n"
                      "To set new value:\n\tset prompt <PROMPT>\nTo see current "
                      "value:\n\tshow prompt\n",
		       &setlist);
      add_show_from_set (c, &showlist);
      c->function.sfunc = set_async_prompt;
    }

  add_show_from_set
    (add_set_cmd ("prompt-escape-char", class_support, var_zinteger,
		  (char *) &gdb_prompt_escape,
		  "Set escape character for formatting of gdb's prompt.\n\nUsage:"
                  "\nTo set new value:\n\tset prompt-escape-char <INTEGER>\nTo see"
                  " current value:\n\tshow prompt-escape-char\n",
		  &setlist),
     &showlist);

  add_com ("echo", class_support, echo_command,
	   "Print a constant string.\n\n\
Usage:\n\techo <TEXT>\n\n\
Give string as argument.\n\
C escape sequences may be used in the argument.\n\
No newline is added at the end of the argument;\n\
use \"\\n\" if you want a newline to be printed.\n\
Since leading and trailing whitespace are ignored in command arguments,\n\
if you want to print some you must use \"\\\" before leading whitespace\n\
to be printed or after trailing whitespace.");
  add_com ("document", class_support, document_command,
	   "Document a user-defined command.\n\n\
Usage:\n\tdocument <NAME>\n\t[<LINE1>]\n\t[<LINE2>]\n\t[...]\n\tend\n\n\
Give command name as argument.  Give documentation on following lines.\n\
End with a line of just \"end\".");
  add_com ("define", class_support, define_command,
	   "Define a new command name.\n\n\
Usage:\n\tdefine <NAME>\n\t[<CMD1>]\n\t[<CMD_2>]\n\t[...]\n\tend\n\n\
Command name is argument.\n\
Definition appears on following lines, one command per line.\n\
End with a line of just \"end\".\n\
Use the \"document\" command to give documentation for the new command.\n\
Commands defined in this way may have up to ten arguments.");

#ifdef __STDC__
  c = add_cmd ("source", class_support, source_command,
	       "Read commands from a file named FILE.\n\n\
Usage:\n\tsource <FILE>\n\n\
Note that the file \"" GDBINIT_FILENAME "\" is read automatically in this way\n\
when gdb is started.", &cmdlist);
#else
  /* Punt file name, we can't help it easily.  */
  c = add_cmd ("source", class_support, source_command,
	       "Read commands from a file named FILE.\n\
Note that the file \".gdbinit\" is read automatically in this way\n\
when gdb is started.", &cmdlist);
#endif
  c->completer = filename_completer;

  add_com ("quit", class_support, quit_command, "Exit gdb.\n\nUsage:\n\tquit [<EXP>]\n\n\
If you do not supply expression, GDB will terminate normally, otherwise\n\
it will terminate using the result of expression as the error code.\n");
  add_com ("help", class_support, help_command, "Print list of commands.\n\n\
Usage:\n\thelp <CMD>\n");
  add_com_alias ("q", "quit", class_support, 1);
  add_com_alias ("e", "quit", class_support, 1);
  add_com_alias ("exit", "quit", class_support, 1);
  add_com_alias ("h", "help", class_support, 1);

  add_com ("dont-repeat", class_support, dont_repeat_command, "Don't repeat this command.\n\n\
Usage:\n\tdont-repeat\n\n\
Primarily used inside of user-defined commands that should not be repeated when\n\
hitting return.");

/* JAGae93202-Disabling threadverbose feature

  c = add_set_cmd ("threadverbose", class_support, var_boolean, 
                   (char *)&info_threadverbose, "", &setlist);
  add_show_from_set (c, &showlist);
  c->function.sfunc = set_threadverbose;
  set_threadverbose (NULL, 0, c);

*/
              
  c = add_set_cmd ("verbose", class_support, var_boolean, (char *) &info_verbose,
		   "",
		   &setlist);
  add_show_from_set (c, &showlist);
  c->function.sfunc = set_verbose;
  set_verbose (NULL, 0, c);

  /* The set editing command is different depending whether or not the
     async version is run. NOTE: this difference is going to disappear
     as we make the event loop be the default engine of gdb. */
  if (!event_loop_p)
    {
      add_show_from_set
	(add_set_cmd ("editing", class_support, var_boolean, (char *) &command_editing_p,
		      "Set editing of command lines as they are typed.\n\
Use \"on\" to enable the editing, and \"off\" to disable it.\n\
Without an argument, command line editing is enabled.  To edit, use\n\
EMACS-like or VI-like commands like control-P or ESC.", &setlist),
	 &showlist);
    }
  else
    {
      c = add_set_cmd ("editing", class_support, var_boolean, (char *) &async_command_editing_p,
		       "Set editing of command lines as they are typed.\n\n\
Usage:\nTo set new value:\n\tset editing [on | off]\nTo see current value:\
\n\tshow editing\n\n\
Use \"on\" to enable the editing, and \"off\" to disable it.\n\
Without an argument, command line editing is enabled.  To edit, use\n\
EMACS-like or VI-like commands like control-P or ESC.", &setlist);

      add_show_from_set (c, &showlist);
      c->function.sfunc = set_async_editing_command;
    }

  add_prefix_cmd ("history", class_support, set_history,
		  "Generic command for setting command history parameters.",
		  &sethistlist, "set history ", 0, &setlist);
  add_prefix_cmd ("history", class_support, show_history,
		  "Generic command for showing command history parameters.",
		  &showhistlist, "show history ", 0, &showlist);

  c = add_set_cmd ("hp-aries-pa32", class_support, var_boolean, 
                   (char *)&info_hp_aries_pa32, "", &setlist);
  add_show_from_set (c, &showlist);
  c->function.sfunc = set_hp_aries_pa32;
  set_hp_aries_pa32 (NULL, 0, c);

  c = add_set_cmd ("hp-aries-pa64", class_support, var_boolean, 
                   (char *)&info_hp_aries_pa64, "", &setlist);
  add_show_from_set (c, &showlist);
  c->function.sfunc = set_hp_aries_pa64;
  set_hp_aries_pa64 (NULL, 0, c);

  add_show_from_set
    (add_set_cmd ("expansion", no_class, var_boolean, (char *) &history_expansion_p,
		  "Set history expansion on command input.\n\n\
Usage:\nTo set new value:\n\tset history expansion [on | off]\nTo see current value:\
\n\tshow history expansion\n\n\
Without an argument, history expansion is enabled.", &sethistlist),
     &showhistlist);

  add_show_from_set
    (add_set_cmd ("save", no_class, var_boolean, (char *) &write_history_p,
		  "Set saving of the history record on exit.\n\n\
Usage:\nTo set new value:\n\tset history save [on | off]\nTo see current value:\
\n\tshow history save\n\n\
Use \"on\" to enable the saving, and \"off\" to disable it.\n\
Without an argument, saving is enabled.", &sethistlist),
     &showhistlist);

  c = add_set_cmd ("size", no_class, var_integer, (char *) &history_size,
		   "Set the size of the command history.\n\n\
Usage:\nTo set new value:\n\tset history size <INTEGER>\nTo see current value:\
\n\tshow history size\n\n\
ie. the number of previous commands to keep a record of.", &sethistlist);
  add_show_from_set (c, &showhistlist);
  c->function.sfunc = set_history_size_command;

  add_show_from_set
    (add_set_cmd ("filename", no_class, var_filename, (char *) &history_filename,
		  "Set the filename in which to record the command history.\n\n\
Usage:\nTo set new value:\n\tset history filename <FILE>\nTo see current value:\
\n\tshow history filename\n\nCommand history:\
 (the list of previous commands of which a record is kept).", &sethistlist),
     &showhistlist);

  add_show_from_set
    (add_set_cmd ("confirm", class_support, var_boolean,
		  (char *) &caution,
		  "Set whether to confirm potentially dangerous operations.\n\n\
Usage:\nTo set new value:\n\tset confirm [on | off]\nTo see current value:\
\n\tshow confirm\n", &setlist), &showlist);

  add_prefix_cmd ("info", class_info, info_command,
     "Generic command for showing things about the program being debugged.",
		  &infolist, "info ", 0, &cmdlist);
  add_com_alias ("i", "info", class_info, 1);

  add_com ("complete", class_obscure, complete_command,
	   "List the completions for the rest of the line as a command.");

  add_prefix_cmd ("show", class_info, show_command,
		  "Generic command for showing things about the debugger.\
\nUsage:\n\tshow <VAR>\n\n",
		  &showlist, "show ", 0, &cmdlist);
  /* Another way to get at the same thing.  */
  add_info ("set", show_command, "Show all GDB settings.\n\nUsage:\n\tinfo set\n");

  add_cmd ("commands", no_class, show_commands,
	   "Show the history of commands you typed.\n\nUsage:\n\tshow commands \
[<N> | +]\n\nNo argument : Display the last ten commands in the command history.\n\
'N' as argument : Print ten commands centered on command number N.\n\
'+' as argument : Print ten commands just after the commands last printed.\n",
	   &showlist);

  add_cmd ("version", no_class, show_version,
	   "Show what version of GDB this is.\n\nUsage:\n\tshow version\n", &showlist);

  add_com ("while", class_support, while_command,
	   "Execute nested commands WHILE the conditional expression is non zero.\n\n\
Usage:\n\twhile <COND>\n\t[<CMD1>]\n\t[<CMD2>]\n\t[...]\n\tend\n\n\
The conditional expression must follow the word `while' and must in turn be\n\
followed by a new line.  The nested commands must be entered one per line,\n\
and should be terminated by the word `end'.");

  add_com ("if", class_support, if_command,
	   "Execute nested commands once IF the conditional expression is non zero.\n\n\
Usage:\n\tif <COND>\n\t[<CMD1>]\n\t[<CMD2>]\n\t[...]\n\t[else]\n\t[<CMD3>]\n\t[<CMD4>]\
\n\t[...]\nend\n\n\
The conditional expression must follow the word `if' and must in turn be\n\
followed by a new line.  The nested commands must be entered one per line,\n\
and should be terminated by the word 'else' or `end'.  If an else clause\n\
is used, the same rules apply to its nested commands as to the first ones.");

  /* If target is open when baud changes, it doesn't take effect until the
     next open (I think, not sure).  */
  add_show_from_set (add_set_cmd ("remotebaud", no_class,
				  var_zinteger, (char *) &baud_rate,
				  "Set baud rate for remote serial I/O.\n\
This value is used to set the speed of the serial port when debugging\n\
using remote targets.", &setlist),
		     &showlist);

  c = add_set_cmd ("remotedebug", no_class, var_zinteger,
		   (char *) &remote_debug,
		   "Set debugging of remote protocol.\n\
When enabled, each packet sent or received with the remote target\n\
is displayed.", &setlist);
  deprecate_cmd (c, "set debug remote");
  deprecate_cmd (add_show_from_set (c, &showlist), "show debug remote");

  add_show_from_set (add_set_cmd ("remote", no_class, var_zinteger,
				  (char *) &remote_debug,
				  "Set debugging of remote protocol.\n\
When enabled, each packet sent or received with the remote target\n\
is displayed.", &setdebuglist),
		     &showdebuglist);

  add_show_from_set (
		      add_set_cmd ("remotetimeout", no_class, var_integer, (char *) &remote_timeout,
				   "Set timeout limit to wait for target to respond.\n\
This value is used to set the time limit for gdb to wait for a response\n\
from the target.", &setlist),
		      &showlist);

  /* The set annotate command is different depending whether or not
     the async version is run. NOTE: this difference is going to
     disappear as we make the event loop be the default engine of
     gdb. */
  if (!event_loop_p)
    {
      c = add_set_cmd ("annotate", class_obscure, var_zinteger,
		       (char *) &annotation_level, "Set annotation_level.\n\
0 == normal.\n1 == fullname (for use when running under emacs).\n\
2 == output annotated suitably for use by programs that control GDB.",
		       &setlist);
      c = add_show_from_set (c, &showlist);
    }
  else
    {
      c = add_set_cmd ("annotate", class_obscure, var_zinteger,
		       (char *) &annotation_level, "Set annotation_level.\n\nUsage:\n\
To set new value:\n\tset annotate 0 | 1 | 2\nTo see current value:\n\tshow annotate\n\n\
0 == normal.\n1 == fullname (for use when running under emacs).\n\
2 == output annotated suitably for use by programs that control GDB.",
		       &setlist);
      add_show_from_set (c, &showlist);
      c->function.sfunc = set_async_annotation_level;
    }
  if (event_loop_p)
    {
      add_show_from_set
	(add_set_cmd ("exec-done-display", class_support, var_boolean, (char *) &exec_done_display_p,
		      "Set notification of completion for asynchronous execution commands.\n\n\
Usage:\nTo set new value:\n\tset exec-done-display [on | off]\nTo see current value:\n\t\
show exec-done-display\n\n\
Use \"on\" to enable the notification, and \"off\" to disable it.", &setlist),
	 &showlist);
    }
  add_prefix_cmd ("debug", no_class, set_debug,
		  "Generic command for setting gdb debugging flags",
		  &setdebuglist, "set debug ", 0, &setlist);

  add_prefix_cmd ("debug", no_class, show_debug,
		  "Generic command for showing gdb debugging flags",
		  &showdebuglist, "show debug ", 0, &showlist);
}
