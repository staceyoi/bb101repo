/*
   ** tuiDisassem.c
   **         This module contains functions for handling disassembly display.
 */


#include "defs.h"
#include "ui-file.h"
#include "tui/tui-file.h"
#include "symtab.h"
#include "breakpoint.h"
#include "frame.h"
#include "source.h"

#include "tui.h"
#include "tuiData.h"
#include "tuiLayout.h"
#include "tuiSourceWin.h"
#include "tuiStack.h"
#include "tuiWin.h"


/*****************************************
** STATIC LOCAL FUNCTIONS FORWARD DECLS    **
******************************************/

static struct breakpoint *_hasBreak (CORE_ADDR);

static FILE *fstream = NULL;
static int desc = -1;

void 
close_source_file (int foo)
{
  if (desc >= 0)
    close (desc);
  desc = -1;
  if (fstream)
    fclose (fstream);
  fstream = NULL;
}

/*****************************************
** PUBLIC FUNCTIONS                        **
******************************************/

/*
   ** tuiSetDisassemContent().
   **        Function to set the disassembly window's content.
 */
TuiStatus
#ifdef __STDC__
tuiSetDisassemContent (
			struct symtab *s,
			CORE_ADDR startAddr)
#else
tuiSetDisassemContent (s, startAddr)
     struct symtab *s;
     CORE_ADDR startAddr;
#endif
{
  TuiStatus ret = TUI_FAILURE;
  struct ui_file *gdb_dis_out;
  int last_line = -1;
  char *last_file = NULL;
  struct symtab_and_line prev_sal;
  struct symtab_and_line sal;
  struct cleanup *old_chain;

  if (startAddr != (CORE_ADDR) NULL)
    {
      register int i;

      if ((ret = tuiAllocSourceBuffer (disassemWin)) == TUI_SUCCESS)
	{
	  register int offset = disassemWin->detail.sourceInfo.horizontalOffset;
	  register int threshold, curLine = 0, lineWidth, maxLines;
	  CORE_ADDR newpc = 0, pc;
	  disassemble_info asmInfo;
	  TuiGenWinInfoPtr locator = locatorWinInfoPtr ();
	  extern void strcat_address (CORE_ADDR, char *, int);
	  extern void strcat_address_numeric (CORE_ADDR, int, char *, int);
	  int curLen = 0;
	  int tab_len = tuiDefaultTabLen ();

	  maxLines = disassemWin->generic.height - 2;	/* account for hilite */
	  lineWidth = disassemWin->generic.width - 1;
	  threshold = (lineWidth - 1) + offset;

	  /* now init the ui_file structure */
	  gdb_dis_out = tui_sfileopen (threshold);

	  INIT_DISASSEMBLE_INFO_NO_ARCH (asmInfo,
					 gdb_dis_out,
					 (fprintf_ftype) fprintf_filtered);
	  asmInfo.read_memory_func = dis_asm_read_memory;
	  asmInfo.memory_error_func = dis_asm_memory_error;

	  disassemWin->detail.sourceInfo.startLineOrAddr.addr = startAddr;

	  /* RM: Set last_line if not at the beginning of a line. Ugh
	     gross! */
	  prev_sal = find_pc_line (startAddr - 4, 0);
	  sal = find_pc_line (startAddr, 0);
	  if ((sal.line != 0) && (sal.line == prev_sal.line))
	    last_line = sal.line;
	  /* RM: Set file name to first line in display. This will get
	     overridden if the exec point is also in the display */
	  if (sal.symtab && (sal.line != 0))
	    tuiUpdateLocatorFilename (sal.symtab->filename);
	  else
	    tuiUpdateLocatorFilename ("???");

	  old_chain = make_cleanup ((make_cleanup_ftype *) close_source_file, 
				    0);

	  /* Now construct each line */
	  for (curLine = 0, pc = (CORE_ADDR) startAddr; (curLine < maxLines);)
	    {
	      TuiWinElementPtr element
	      = (TuiWinElementPtr)(void *) disassemWin->generic.content[curLine];
	      struct breakpoint *bp;

	      /* RM: try to print source line in disassembly display */
	      sal = find_pc_line (pc, 0);

	      /* Print each new source line. */
	      if (sal.line != last_line)
		{
		  last_line = sal.line;
		  if (last_line != 0)
		    {
		      element->whichElement.source.lineOrAddr.addr = 0;
		      element->whichElement.source.isExecPoint = 0;
		      element->whichElement.source.hasBreak = 0;

		      if (!sal.symtab ||
			  strcmp (sal.symtab->filename, last_file))
			{
			  if (sal.symtab)
			    {
			      last_file = sal.symtab->filename;
			      if (desc >= 0)
				close (desc);
			      desc = open_source_file (sal.symtab);
			      if (desc >= 0)
				{
				  if (fstream)
				    fclose (fstream);
				  fstream = fdopen (desc, FOPEN_RT);
				  clearerr (fstream);
				  if (sal.symtab->line_charpos == 0)
				    find_source_lines (sal.symtab, desc);
				}
			    }
			  else
			    {
			      if (desc >= 0)
				close (desc);
			      desc = -1;
			      if (fstream)
				fclose (fstream);
			      fstream = NULL;
			    }
			}
		      /* Find the source line and print it. */
		      if (!fstream)
			sprintf (element->whichElement.source.line,
				 ";;; Line: %6d", last_line);
		      else
			{
			  if (last_line > 0 && last_line <= sal.symtab->nlines &&
			      (fseek (fstream, sal.symtab->line_charpos[last_line - 1], SEEK_SET) == 0))
			    {
			      /* yuck */
			      char line[1024];

			      strcpy (element->whichElement.source.line,
				      ";;; ");
			      fgets (line, lineWidth - 5, fstream);
			      /* If line > lineWidth - 5, then may have cut newline. */
			      if (line[strlen (line) - 1] != '\n')
				line[strlen (line) - 1] = '\n';
			      strcpy (element->whichElement.source.line + 4,
				      line);
			    }
			}
		      curLine++;
		      continue;
		    }
		}

	      print_address (pc, gdb_dis_out);

	      curLen = (int) strlen (tui_file_get_strbuf (gdb_dis_out));
	      i = curLen - ((curLen / tab_len) * tab_len);

	      /* adjust buffer length if necessary */
	      tui_file_adjust_strbuf ((tab_len - i > 0) ? (tab_len - i) : 0, gdb_dis_out);

	      /* Add spaces to make the instructions start onthe same column */
	      while (i < tab_len)
		{
		  tui_file_get_strbuf (gdb_dis_out)[curLen] = ' ';
		  i++;
		  curLen++;
		}
	      tui_file_get_strbuf (gdb_dis_out)[curLen] = '\0';

	      newpc = pc + ((*tm_print_insn) (pc, &asmInfo));

	      /* Now copy the line taking the offset into account */
	      if (strlen (tui_file_get_strbuf (gdb_dis_out)) > offset)
		strcpy (element->whichElement.source.line,
			&(tui_file_get_strbuf (gdb_dis_out)[offset]));
	      else
		element->whichElement.source.line[0] = '\0';
	      element->whichElement.source.lineOrAddr.addr = (CORE_ADDR) pc;
	      element->whichElement.source.isExecPoint =
		(pc == (CORE_ADDR) ((TuiWinElementPtr)(void *) locator->content[0])->whichElement.locator.addr);
	      bp = _hasBreak (pc);
	      element->whichElement.source.hasBreak =
		(bp != (struct breakpoint *) NULL &&
		 (!element->whichElement.source.isExecPoint ||
		  (bp->disposition != del || bp->hit_count <= 0)));

	      /* Exec point takes precedence. Print source file. */
	      if (element->whichElement.source.isExecPoint && sal.symtab)
		{
		  if (sal.line != 0)
		    tuiUpdateLocatorFilename (sal.symtab->filename);
		  else
		    tuiUpdateLocatorFilename ("???");
		}

	      curLine++;
	      pc = newpc;
	      /* reset the buffer to empty */
	      tui_file_get_strbuf (gdb_dis_out)[0] = '\0';
	    }
	  ui_file_delete (gdb_dis_out);
	  gdb_dis_out = NULL;
	  disassemWin->generic.contentSize = curLine;
	  ret = TUI_SUCCESS;

	  do_cleanups (old_chain);
	}
    }

  return ret;
}				/* tuiSetDisassemContent */


/*
   ** tuiShowDisassem().
   **        Function to display the disassembly window with disassembled code.
 */
void
#ifdef __STDC__
tuiShowDisassem (
		  CORE_ADDR startAddr)
#else
tuiShowDisassem (startAddr)
     CORE_ADDR startAddr;
#endif
{
  struct symtab *s = find_pc_symtab ((CORE_ADDR) startAddr);
  TuiWinInfoPtr winWithFocus = tuiWinWithFocus ();

  tuiAddWinToLayout (DISASSEM_WIN);
  tuiUpdateSourceWindow (disassemWin, s, startAddr, FALSE);
  /*
     ** if the focus was in the src win, put it in the asm win, if the
     ** source view isn't split
   */
  if (currentLayout () != SRC_DISASSEM_COMMAND && winWithFocus == srcWin)
    tuiSetWinFocusTo (disassemWin);

  return;
}				/* tuiShowDisassem */


/*
   ** tuiShowDisassemAndUpdateSource().
   **        Function to display the disassembly window.
 */
void
#ifdef __STDC__
tuiShowDisassemAndUpdateSource (
				 CORE_ADDR startAddr)
#else
tuiShowDisassemAndUpdateSource (startAddr)
     CORE_ADDR startAddr;
#endif
{
  struct symtab_and_line sal;

  tuiShowDisassem (startAddr);
  if (currentLayout () == SRC_DISASSEM_COMMAND)
    {
      TuiGenWinInfoPtr locator = locatorWinInfoPtr ();
      /*
         ** Update what is in the source window if it is displayed too,
         ** note that it follows what is in the disassembly window and visa-versa
       */
      sal = find_pc_line ((CORE_ADDR) startAddr, 0);
      current_source_symtab = sal.symtab;
      tuiUpdateSourceWindow (srcWin, sal.symtab, (CORE_ADDR)(long) sal.line, TRUE);
      tuiUpdateLocatorFilename (sal.symtab->filename);
    }

  return;
}				/* tuiShowDisassemAndUpdateSource */


/*
   ** tuiShowDisassemAsIs().
   **        Function to display the disassembly window.  This function shows
   **        the disassembly as specified by the horizontal offset.
 */
void
#ifdef __STDC__
tuiShowDisassemAsIs (
		      CORE_ADDR addr)
#else
tuiShowDisassemAsIs (addr)
     CORE_ADDR addr;
#endif
{
  tuiAddWinToLayout (DISASSEM_WIN);
  tuiUpdateSourceWindowAsIs (disassemWin, (struct symtab *) NULL, addr, FALSE);
  /*
     ** Update what is in the source window if it is displayed too, not that it
     ** follows what is in the disassembly window and visa-versa
   */
  if (currentLayout () == SRC_DISASSEM_COMMAND)
    tuiShowSourceContent (srcWin);	/*????  Need to do more? */

  return;
}				/* tuiShowDisassem */


/*
   ** tuiGetBeginAsmAddress().
 */
CORE_ADDR
#ifdef __STDC__
tuiGetBeginAsmAddress (void)
#else
tuiGetBeginAsmAddress ()
#endif
{
  TuiGenWinInfoPtr locator;
  TuiLocatorElementPtr element;
  CORE_ADDR addr;

  locator = locatorWinInfoPtr ();
  element = &((TuiWinElementPtr)(void *) locator->content[0])->whichElement.locator;

  if (element->addr == (CORE_ADDR) 0)
    {
      /*the target is not executing, because the pc is 0 */


      addr = (CORE_ADDR) parse_and_eval_address (default_main);

      if (addr == (CORE_ADDR) 0)
	addr = (CORE_ADDR) parse_and_eval_address ("MAIN");

    }
  else				/* the target is executing */
    addr = element->addr;

  return addr;
}				/* tuiGetBeginAsmAddress */


/*
   ** tuiVerticalDisassemScroll().
   **      Scroll the disassembly forward or backward vertically
 */
void
#ifdef __STDC__
tuiVerticalDisassemScroll (
			    TuiScrollDirection scrollDirection,
			    int numToScroll)
#else
tuiVerticalDisassemScroll (scrollDirection, numToScroll)
     TuiScrollDirection scrollDirection;
     int numToScroll;
#endif
{
  if (disassemWin->generic.content != (OpaquePtr) NULL)
    {
      CORE_ADDR pc, lowAddr;
      TuiWinContent content;
      struct symtab *s;

      content = (TuiWinContent)(void *) disassemWin->generic.content;
      if (current_source_symtab == (struct symtab *) NULL)
	s = find_pc_symtab (selected_frame->pc);
      else
	s = current_source_symtab;

      /* RM: Can't have two successive lines with no address - if the
         first doesn't work, the second must */
      pc = content[0]->whichElement.source.lineOrAddr.addr;
      if (!pc)
	pc = content[1]->whichElement.source.lineOrAddr.addr;
      if (find_pc_partial_function ((CORE_ADDR) pc,
				    (char **) NULL,
				    (CORE_ADDR *) & lowAddr,
				    (CORE_ADDR) NULL) == 0)
	error ("No function contains prgram counter for selected frame.\n");
      else
	{
	  register int line = 0;
	  register CORE_ADDR newLow;
	  bfd_byte buffer[4];

	  newLow = pc;
	  if (scrollDirection == FORWARD_SCROLL)
	    {
	      for (; line < numToScroll; line++)
		newLow += sizeof (bfd_getb32 (buffer));
	      /* FIXME: Compiler warning "Operand of sizeof is not evaluated and 
	       * has no side effects. */
	    }
	  else
	    {
	      for (; newLow >= sizeof (bfd_getb32 (buffer)) && 
		     line < numToScroll; line++)
		newLow -= sizeof (bfd_getb32 (buffer));
	      /* FIXME: Compiler warning "Operand of sizeof is not evaluated and 
	       * has no side effects. */
	    }
	  tuiUpdateSourceWindowAsIs (disassemWin, s, newLow, FALSE);
	}
    }

  return;
}				/* tuiVerticalDisassemScroll */



/*****************************************
** STATIC LOCAL FUNCTIONS                 **
******************************************/
/*
   ** _hasBreak().
   **      Answer whether there is a break point at the input line in the
   **      source file indicated
 */
static struct breakpoint *
#ifdef __STDC__
_hasBreak (
	    CORE_ADDR addr)
#else
_hasBreak (addr)
     CORE_ADDR addr;
#endif
{
  struct breakpoint *bpWithBreak = (struct breakpoint *) NULL;
  struct breakpoint *bp;
  extern struct breakpoint *breakpoint_chain;


  for (bp = breakpoint_chain;
       (bp != (struct breakpoint *) NULL &&
	bpWithBreak == (struct breakpoint *) NULL);
       bp = bp->next)
    if (addr == bp->address)
      bpWithBreak = bp;

  return bpWithBreak;
}				/* _hasBreak */
