#ifndef _TUI_DISASSEM_H
#define _TUI_DISASSEM_H
/*
   ** This header file supports
 */

/*****************************************
** TYPE DEFINITIONS                        **
******************************************/



/*****************************************
** PUBLIC FUNCTION EXTERNAL DECLS        **
******************************************/
extern TuiStatus tuiSetDisassemContent (struct symtab *, CORE_ADDR);
extern void tuiShowDisassem (CORE_ADDR);
extern void tuiShowDisassemAndUpdateSource (CORE_ADDR);
extern void tuiVerticalDisassemScroll (TuiScrollDirection, int);
extern CORE_ADDR tuiGetBeginAsmAddress (void);

#endif
/*_TUI_DISASSEM_H*/
