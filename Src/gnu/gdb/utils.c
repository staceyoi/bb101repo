/* General utility routines for GDB, the GNU debugger.
   Copyright 1986, 1989, 1990-1992, 1995, 1996, 1998, 2000
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include "defs.h"
#include <ctype.h>
#include <stdio.h>
#include "gdb_string.h"
#include "event-top.h"
#ifdef TUI
#include "tui/tui-file.h"
#endif
#ifdef UI_OUT
#include "ui-out.h"
#endif

#ifdef HAVE_CURSES_H
#include <curses.h>
#endif
#ifdef HAVE_TERM_H
#include <term.h>
#endif

#ifdef __GO32__
#include <pc.h>
#endif

/* SunOS's curses.h has a '#define reg register' in it.  Thank you Sun. */
#ifdef reg
#undef reg
#endif

#include <stdlib.h>
#include "signals.h"
#include "gdbcmd.h"
#include "serial.h"
#include "bfd.h"
#include "target.h"
#include "demangle.h"
#include "expression.h"
#include "language.h"
#include "annotate.h"

#include "inferior.h" /* for signed_pointer_to_address */

#define NONSTANDARD 1
#include <term.h>
#include <readline/readline.h>

#undef XMALLOC
#define XMALLOC(TYPE) ((TYPE*) xmalloc (sizeof (TYPE)))

/* readline defines this.  */
#undef savestring

extern int debug_dwutils_processed;

extern int nimbus_version;

extern char *_errlocale(const char *cmd);
extern int _rl_convert_meta_chars_to_ascii;

extern int batch_rtc;
extern int trace_threads_in_this_run;
extern int frame_count;
/* Use backtrace_command_1 to get the backtrace at the point of
   error event occurrence for batch mode thread checking. */
extern void backtrace_command_1 (char *count_exp, int show_locals,
				 int from_tty);
void (*error_begin_hook) (void);

/* Holds the last error message issued by gdb */

static struct ui_file *gdb_lasterr;

/* Prototypes for local functions */

static void vfprintf_maybe_filtered (struct ui_file *, const char *,
				     va_list, int);

static void fputs_maybe_filtered (const char *, struct ui_file *, int);

#if defined (USE_MMALLOC) && !defined (NO_MMCHECK)
static void malloc_botch (void);
#endif

static void prompt_for_continue (void);

static void set_width_command (char *, int, struct cmd_list_element *);

static void set_width (void);

static void
set_logging_command (void);
static void
show_logging_command (void);
static void
set_logging_on (void);
static void
set_logging_off (void);
static void
set_logging_overwrite(void);
static void
set_logging_redirect(void);

static void
redirect_command (char *, int);
static void
set_redirect_command (char *, int, struct cmd_list_element *);
static void
set_redirect_mode_command (char *, int, struct cmd_list_element *);
static void
set_redirect_file_command (char *, int, struct cmd_list_element *);
static void
show_redirect_command (char *, int, struct cmd_list_element *);


/* Chain of cleanup actions established with make_cleanup,
   to be executed if an error happens.  */

static struct cleanup *cleanup_chain;	/* cleaned up after a failed command */
static struct cleanup *final_cleanup_chain;	/* cleaned up when gdb exits */
static struct cleanup *run_cleanup_chain;	/* cleaned up on each 'run' */
static struct cleanup *objfile_purge_cleanup_chain;	/* cleaned up on each objfile_purge */
static struct cleanup *exec_cleanup_chain;	/* cleaned up on each execution command */
/* cleaned up on each error from within an execution command */
static struct cleanup *exec_error_cleanup_chain; 

/* Pointer to what is left to do for an execution command after the
   target stops. Used only in asynchronous mode, by targets that
   support async execution.  The finish and until commands use it. So
   does the target extended-remote command. */
struct continuation *cmd_continuation;
struct continuation *intermediate_continuation;

/* Nonzero if we have job control. */

int job_control;

/* Nonzero means a quit has been requested.  */

int quit_flag;

int donot_print_errors = 0;

/* Nonzero means quit immediately if Control-C is typed now, rather
   than waiting until QUIT is executed.  Be careful in setting this;
   code which executes with immediate_quit set has to be very careful
   about being able to deal with being interrupted at any time.  It is
   almost always better to use QUIT; the only exception I can think of
   is being able to quit out of a system call (using EINTR loses if
   the SIGINT happens between the previous QUIT and the system call).
   To immediately quit in the case in which a SIGINT happens between
   the previous QUIT and setting immediate_quit (desirable anytime we
   expect to block), call QUIT after setting immediate_quit.  */

int immediate_quit;

/* Nonzero means that encoded C++ names should be printed out in their
   C++ form rather than raw.  */

int demangle = 1;

/* Nonzero means that encoded C++ names should be printed out in their
   C++ form even in assembler language displays.  If this is set, but
   DEMANGLE is zero, names are printed raw, i.e. DEMANGLE controls.  */

int asm_demangle = 1;

/* Nonzero means that namespace support as per dwarf2 standard is present.
   We modify the lookup and provide the user with a menu to choose from. */
int namespace_enabled = 1;

/* 0 means can't debug dld else we look in dld too for function names */
int debugging_dld = 0;

/* locale_is_c is true if the locale is C or POSIX */
boolean locale_is_c = true;

/* Nonzero means that strings with character values >0x7F should be printed
   as octal escapes.  Zero means just print the value (e.g. it's an
   international character, and the terminal or window can cope.)  */

int sevenbit_strings = 0;

/* String to be printed before error messages, if any.  */

char *error_pre_print;

/* String to be printed before quit messages, if any.  */

char *quit_pre_print;

/* String to be printed before warning messages, if any.  */

char *warning_pre_print = "\nwarning: ";

int pagination_enabled = 1;

/* Flag that controls printing of changed registers in nimbus assembly
   mode debugging.  The current default is "off".  You can change this
   by setting the variable below to 1.  */

int print_changed_registers_enabled = 0;


/* Madhavi, file into which output of print, help, show etc is
 * redirected */
struct ui_file *nonstdout;
static char *redirect_args;
static char *redirect_mode_args;
static char *redirect_file_args=".gdb_output";
int redirection_on = FALSE;
static char *redirect_file = ".gdb_output";
static int redirect_mode = 1; /* Default redirection mode is "append" which
                               * is a non-zero value. */

static char *saved_filename = ".gdb_output";
static char *logging_overwrite_args;
static char *logging_redirect_args;
static char *logging_on_args;
static char *logging_off_args;
static int   logging_overwrite = 0;
static int   logging_redirect = 1;

static struct ui_file *gdb_stdout_saved;

/* Add a new cleanup to the cleanup_chain,
   and return the previous chain pointer
   to be passed later to do_cleanups or discard_cleanups.
   Args are FUNCTION to clean up with, and ARG to pass to it.  */

struct cleanup *
make_cleanup (make_cleanup_ftype *function, void *arg)
{
  return make_my_cleanup (&cleanup_chain, function, arg);
}

struct cleanup *
make_final_cleanup (make_cleanup_ftype *function, void *arg)
{
  return make_my_cleanup (&final_cleanup_chain, function, arg);
}

struct cleanup *
make_run_cleanup (make_cleanup_ftype *function, void *arg)
{
  return make_my_cleanup (&run_cleanup_chain, function, arg);
}

struct cleanup *
make_objfile_purge_cleanup (make_cleanup_ftype *function, void *arg)
{
  return make_my_cleanup (&objfile_purge_cleanup_chain, function, arg);
}

struct cleanup *
make_exec_cleanup (make_cleanup_ftype *function, void *arg)
{
  return make_my_cleanup (&exec_cleanup_chain, function, arg);
}

struct cleanup *
make_exec_error_cleanup (make_cleanup_ftype *function, void *arg)
{
  return make_my_cleanup (&exec_error_cleanup_chain, function, arg);
}

static void
do_freeargv (void *arg)
{
  freeargv ((char **) arg);
}

struct cleanup *
make_cleanup_freeargv (char **arg)
{
  return make_my_cleanup (&cleanup_chain, do_freeargv, arg);
}

static void
do_bfd_close_cleanup (void *arg)
{
  bfd_close (arg);
}

struct cleanup *
make_cleanup_bfd_close (bfd *abfd)
{
  return make_cleanup (do_bfd_close_cleanup, abfd);
}

static void
do_close_cleanup (void *arg)
{
  close ((int)(long) arg);
}

struct cleanup *
make_cleanup_close (int fd)
{
  /* int into void*. Outch!! */
  return make_cleanup (do_close_cleanup, (void *)(long) fd);
}

static void
do_ui_file_delete (void *arg)
{
  ui_file_delete (arg);
}

struct cleanup *
make_cleanup_ui_file_delete (struct ui_file *arg)
{
  return make_my_cleanup (&cleanup_chain, do_ui_file_delete, arg);
}

static void
do_free_section_addr_info (void *arg)
{
  free_section_addr_info (arg);
}

struct cleanup *
make_cleanup_free_section_addr_info (struct section_addr_info *addrs)
{
  return make_my_cleanup (&cleanup_chain, do_free_section_addr_info, addrs);
}

struct cleanup *
make_my_cleanup (struct cleanup **pmy_chain, make_cleanup_ftype *function,
		 void *arg)
{
  register struct cleanup *new
  = (struct cleanup *) xmalloc (sizeof (struct cleanup));
  register struct cleanup *old_chain = *pmy_chain;

  new->next = *pmy_chain;
  new->function = function;
  new->arg = arg;
  *pmy_chain = new;

  return old_chain;
}

/* Discard cleanups and do the actions they describe
   until we get back to the point OLD_CHAIN in the cleanup_chain.  */

void
do_cleanups (register struct cleanup *old_chain)
{
  do_my_cleanups (&cleanup_chain, old_chain);
}

void
do_final_cleanups (register struct cleanup *old_chain)
{
  do_my_cleanups (&final_cleanup_chain, old_chain);
}

void
do_run_cleanups (register struct cleanup *old_chain)
{
  do_my_cleanups (&run_cleanup_chain, old_chain);
}

void
do_objfile_purge_cleanups (register struct cleanup *old_chain)
{
  do_my_cleanups (&objfile_purge_cleanup_chain, old_chain);
}

void
do_exec_cleanups (register struct cleanup *old_chain)
{
  do_my_cleanups (&exec_cleanup_chain, old_chain);
}

void
do_exec_error_cleanups (register struct cleanup *old_chain)
{
  do_my_cleanups (&exec_error_cleanup_chain, old_chain);
}

void
do_my_cleanups (register struct cleanup **pmy_chain,
		register struct cleanup *old_chain)
{
  register struct cleanup *ptr;
  while ((ptr = *pmy_chain) != old_chain)
    {
      *pmy_chain = ptr->next;	/* Do this first incase recursion */
      (*ptr->function) (ptr->arg);
      free (ptr);
    }
}

/* Discard cleanups, not doing the actions they describe,
   until we get back to the point OLD_CHAIN in the cleanup_chain.  */

void
discard_cleanups (register struct cleanup *old_chain)
{
  discard_my_cleanups (&cleanup_chain, old_chain);
}

void
discard_final_cleanups (register struct cleanup *old_chain)
{
  discard_my_cleanups (&final_cleanup_chain, old_chain);
}

void
discard_exec_error_cleanups (register struct cleanup *old_chain)
{
  discard_my_cleanups (&exec_error_cleanup_chain, old_chain);
}

void
discard_my_cleanups (register struct cleanup **pmy_chain,
		     register struct cleanup *old_chain)
{
  register struct cleanup *ptr;
  while ((ptr = *pmy_chain) != old_chain)
    {
      *pmy_chain = ptr->next;
      free (ptr);
    }
}

/* Set the cleanup_chain to 0, and return the old cleanup chain.  */
struct cleanup *
save_cleanups (void)
{
  return save_my_cleanups (&cleanup_chain);
}

struct cleanup *
save_final_cleanups (void)
{
  return save_my_cleanups (&final_cleanup_chain);
}

struct cleanup *
save_my_cleanups (struct cleanup **pmy_chain)
{
  struct cleanup *old_chain = *pmy_chain;

  *pmy_chain = 0;
  return old_chain;
}

/* Restore the cleanup chain from a previously saved chain.  */
void
restore_cleanups (struct cleanup *chain)
{
  restore_my_cleanups (&cleanup_chain, chain);
}

void
restore_final_cleanups (struct cleanup *chain)
{
  restore_my_cleanups (&final_cleanup_chain, chain);
}

void
restore_my_cleanups (struct cleanup **pmy_chain,
     		     struct cleanup *chain)
{
  *pmy_chain = chain;
}

/* This function is useful for cleanups.
   Do

   foo = xmalloc (...);
   old_chain = make_cleanup (free_current_contents, &foo);

   to arrange to free the object thus allocated.  */

void
free_current_contents (void *ptr)
{
  void **location = ptr;
  if (location == NULL)
    internal_error ("free_current_contents: NULL pointer");
  if (*location != NULL)
    {
      free (*location);
      *location = NULL;
    }
}

void
set_var_to_zero (void *ptr)
{
   int * i = ptr;
   if (i)
     *i = 0;
}

/* Provide a known function that does nothing, to use as a base for
   for a possibly long chain of cleanups.  This is useful where we
   use the cleanup chain for handling normal cleanups as well as dealing
   with cleanups that need to be done as a result of a call to error().
   In such cases, we may not be certain where the first cleanup is, unless
   we have a do-nothing one to always use as the base. */

/* ARGSUSED */
void
null_cleanup (void *arg)
{
}

/* Add a continuation to the continuation list, the gloabl list
   cmd_continuation. The new continuation will be added at the front.*/
void
add_continuation (void (*continuation_hook) (struct continuation_arg *),
     		  struct continuation_arg *arg_list)
{
  struct continuation *continuation_ptr;

  continuation_ptr = (struct continuation *) xmalloc (sizeof (struct continuation));
  continuation_ptr->continuation_hook = continuation_hook;
  continuation_ptr->arg_list = arg_list;
  continuation_ptr->next = cmd_continuation;
  cmd_continuation = continuation_ptr;
}

/* Walk down the cmd_continuation list, and execute all the
   continuations. There is a problem though. In some cases new
   continuations may be added while we are in the middle of this
   loop. If this happens they will be added in the front, and done
   before we have a chance of exhausting those that were already
   there. We need to then save the beginning of the list in a pointer
   and do the continuations from there on, instead of using the
   global beginning of list as our iteration pointer.*/
void
do_all_continuations (void)
{
  struct continuation *continuation_ptr;
  struct continuation *saved_continuation;

  /* Copy the list header into another pointer, and set the global
     list header to null, so that the global list can change as a side
     effect of invoking the continuations and the processing of
     the preexisting continuations will not be affected. */
  continuation_ptr = cmd_continuation;
  cmd_continuation = NULL;

  /* Work now on the list we have set aside. */
  while (continuation_ptr)
     {
       (continuation_ptr->continuation_hook) (continuation_ptr->arg_list);
       saved_continuation = continuation_ptr;
       continuation_ptr = continuation_ptr->next;
       free (saved_continuation);
     }
}

/* Walk down the cmd_continuation list, and get rid of all the
   continuations. */
void
discard_all_continuations (void)
{
  struct continuation *continuation_ptr;

  while (cmd_continuation)
    {
      continuation_ptr = cmd_continuation;
      cmd_continuation = continuation_ptr->next;
      free (continuation_ptr);
    }
}

/* Add a continuation to the continuation list, the global list
   intermediate_continuation. The new continuation will be added at the front.*/
void
add_intermediate_continuation (void (*continuation_hook)
						(struct continuation_arg *),
     			       struct continuation_arg *arg_list)
{
  struct continuation *continuation_ptr;

  continuation_ptr = (struct continuation *) xmalloc (sizeof (struct continuation));
  continuation_ptr->continuation_hook = continuation_hook;
  continuation_ptr->arg_list = arg_list;
  continuation_ptr->next = intermediate_continuation;
  intermediate_continuation = continuation_ptr;
}

/* Walk down the cmd_continuation list, and execute all the
   continuations. There is a problem though. In some cases new
   continuations may be added while we are in the middle of this
   loop. If this happens they will be added in the front, and done
   before we have a chance of exhausting those that were already
   there. We need to then save the beginning of the list in a pointer
   and do the continuations from there on, instead of using the
   global beginning of list as our iteration pointer.*/
void
do_all_intermediate_continuations (void)
{
  struct continuation *continuation_ptr;
  struct continuation *saved_continuation;

  /* Copy the list header into another pointer, and set the global
     list header to null, so that the global list can change as a side
     effect of invoking the continuations and the processing of
     the preexisting continuations will not be affected. */
  continuation_ptr = intermediate_continuation;
  intermediate_continuation = NULL;

  /* Work now on the list we have set aside. */
  while (continuation_ptr)
     {
       (continuation_ptr->continuation_hook) (continuation_ptr->arg_list);
       saved_continuation = continuation_ptr;
       continuation_ptr = continuation_ptr->next;
       free (saved_continuation);
     }
}

/* Walk down the cmd_continuation list, and get rid of all the
   continuations. */
void
discard_all_intermediate_continuations (void)
{
  struct continuation *continuation_ptr;

  while (intermediate_continuation)
    {
      continuation_ptr = intermediate_continuation;
      intermediate_continuation = continuation_ptr->next;
      free (continuation_ptr);
    }
}



/* Print a warning message.  Way to use this is to call warning_begin,
   output the warning message (use unfiltered output to gdb_stderr),
   ending in a newline.  There is not currently a warning_end that you
   call afterwards, but such a thing might be added if it is useful
   for a GUI to separate warning messages from other output.

   FIXME: Why do warnings use unfiltered output and errors filtered?
   Is this anything other than a historical accident?  */

void
warning_begin (void)
{
  target_terminal_ours ();
  wrap_here ("");		/* Force out any buffered output */
  gdb_flush (gdb_stdout);
  if (warning_pre_print)
    fprintf_unfiltered (gdb_stderr, warning_pre_print);
}

/* JYG: MERGE FIXME: remove this function and fix caller to use
   the implementation here inline */
/* Print the start of a warning message.  Do not terminate it with /n.
   This routine is used for warning messages that need to make other 
   function calls to format CORE_ADDR's, etc.  See warning(). */

void
warning_start (char *string, ...)
{
  va_list args;
  va_start (args, string);
  warning_begin ();
  vfprintf_unfiltered (gdb_stderr, string, args);
  va_end (args);
}

/* Print a warning message.
   The first argument STRING is the warning message, used as a fprintf string,
   and the remaining args are passed as arguments to it.
   The primary difference between warnings and errors is that a warning
   does not force the return to command level.  */

void
warning (const char *string,...)
{
  va_list args;
  va_start (args, string);
  if (warning_hook)
    (*warning_hook) (string, args);
  else
    {
      warning_begin ();
      vfprintf_unfiltered (gdb_stderr, string, args);
      fprintf_unfiltered (gdb_stderr, "\n");
      gdb_flush (gdb_stderr);
      va_end (args);
    }
}

/* Start the printing of an error message.  Way to use this is to call
   this, output the error message (use filtered output to gdb_stderr
   (FIXME: Some callers, like memory_error, use gdb_stdout)), ending
   in a newline, and then call return_to_top_level (RETURN_ERROR).
   error() provides a convenient way to do this for the special case
   that the error message can be formatted with a single printf call,
   but this is more general.  */
void
error_begin (void)
{
  if (error_begin_hook)
    error_begin_hook ();

  target_terminal_ours ();
  wrap_here ("");		/* Force out any buffered output */
  gdb_flush (gdb_stdout);

  annotate_error_begin ();

  if (error_pre_print)
    fprintf_filtered (gdb_stderr, error_pre_print);
}

/* Print an error message and return to command level.
   The first argument STRING is the error message, used as a fprintf string,
   and the remaining args are passed as arguments to it.  */

NORETURN void
verror (const char *string, va_list args)
{
  /* JYG: MERGE FIXME: looks like the error_hook changes WDB added
     need more scrutiny to fit into the new scheme */
  char *err_string;
  struct cleanup *err_string_cleanup;
  /* FIXME: cagney/1999-11-10: All error calls should come here.
     Unfortunatly some code uses the sequence: error_begin(); print
     error message; return_to_top_level.  That code should be
     flushed. */
  if (error_hook) {
      /* What happens if we get an error in vasprintf or error_hook? */
      vasprintf(&err_string, string, args);
      (*error_hook) (err_string);
  } else {
    error_begin ();

    /* NOTE: It's tempting to just do the following...
       vfprintf_filtered (gdb_stderr, string, args);
       and then follow with a similar looking statement to cause the message
       to also go to gdb_lasterr.  But if we do this, we'll be traversing the
       va_list twice which works on some platforms and fails miserably on
       others. */
    /* Save it as the last error */
    ui_file_rewind (gdb_lasterr);
    vfprintf_filtered (gdb_lasterr, string, args);
    /* Retrieve the last error and print it to gdb_stderr */
    err_string = error_last_message ();
    err_string_cleanup = make_cleanup (free, err_string);
if (!donot_print_errors)
  {
    fputs_filtered (err_string, gdb_stderr);
    fprintf_filtered (gdb_stderr, "\n");
  }
    do_cleanups (err_string_cleanup);
  }
  return_to_top_level (RETURN_ERROR);
}

NORETURN void
error (const char *string,...)
{
  va_list args;
  target_terminal_ours ();
  va_start (args, string);
  verror (string, args);
  va_end (args);
}

NORETURN void
error_stream (struct ui_file *stream)
{
  long size;
  char *msg = ui_file_xstrdup (stream, &size);
  make_cleanup (free, msg);
  error ("%s", msg);
}

/* Get the last error message issued by gdb */

char *
error_last_message (void)
{
  long len;
  return ui_file_xstrdup (gdb_lasterr, &len);
}
  
/* This is to be called by main() at the very beginning */

void
error_init (void)
{
  gdb_lasterr = mem_fileopen ();
}

/* Print a message reporting an internal error. Ask the user if they
   want to continue, dump core, or just exit. */

NORETURN void
internal_verror (const char *fmt, va_list ap)
{
  static char msg[] = "Internal GDB error: recursive internal error.\n";
  static int dejavu = 0;
  int continue_p;
  int dump_core_p;

  /* don't allow infinite error recursion. */
  switch (dejavu)
    {
    case 0:
      dejavu = 1;
      break;
    case 1:
      dejavu = 2;
      fputs_unfiltered (msg, gdb_stderr);
      abort ();
    default:
      dejavu = 3;
      write (STDERR_FILENO, msg, sizeof (msg));
      exit (1);
    }

  /* Try to get the message out */
  target_terminal_ours ();
  fputs_unfiltered ("gdb-internal-error: ", gdb_stderr);
  vfprintf_unfiltered (gdb_stderr, fmt, ap);
  fputs_unfiltered ("\n", gdb_stderr);

  /* Default (no case) is to quit GDB.  When in batch mode this
     lessens the likelhood of GDB going into an infinate loop. */
  continue_p = query ("\
An internal GDB error was detected.  This may make make further\n\
debugging unreliable.  Continue this debugging session? ");

  /* Default (no case) is to not dump core.  Lessen the chance of GDB
     leaving random core files around. */
  dump_core_p = query ("\
Create a core file containing the current state of GDB? ");

  if (continue_p)
    {
      if (dump_core_p)
	{
	  if (fork () == 0)
	    abort ();
	}
    }
  else
    {
      if (dump_core_p)
	abort ();
      else
	exit (1);
    }

  dejavu = 0;
  return_to_top_level (RETURN_ERROR);
}

NORETURN void
internal_error (char *string, ...)
{
  va_list ap;
  va_start (ap, string);

  internal_verror (string, ap);
  va_end (ap);
}

/* The strerror() function can return NULL for errno values that are
   out of range.  Provide a "safe" version that always returns a
   printable string. */

char *
safe_strerror (int errnum)
{
  char *msg;
  static char buf[32];

  if ((msg = strerror (errnum)) == NULL)
    {
      sprintf (buf, "(undocumented errno %d)", errnum);
      msg = buf;
    }
  return (msg);
}

/* Print the system error message for errno, and also mention STRING
   as the file name for which the error was encountered.
   Then return to command level.  */

NORETURN void
perror_with_name (char * string)
{
  char *err;
  char *combined;

  err = safe_strerror (errno);
  combined = (char *) alloca (strlen (err) + strlen (string) + 3);
  strcpy (combined, string);
  strcat (combined, ": ");
  strcat (combined, err);

  /* I understand setting these is a matter of taste.  Still, some people
     may clear errno but not know about bfd_error.  Doing this here is not
     unreasonable. */
  bfd_set_error (bfd_error_no_error);
  errno = 0;

  error ("%s.", combined);
}

/* Print the system error message for ERRCODE, and also mention STRING
   as the file name for which the error was encountered.  */

void
print_sys_errmsg (char *string, int errcode)
{
  char *err;
  char *combined;

  err = safe_strerror (errcode);
  combined = (char *) alloca (strlen (err) + strlen (string) + 3);
  strcpy (combined, string);
  strcat (combined, ": ");
  strcat (combined, err);

  /* We want anything which was printed on stdout to come out first, before
     this message.  */
  gdb_flush (gdb_stdout);
  fprintf_unfiltered (gdb_stderr, "%s.\n", combined);
}

/* Control C eventually causes this to be called, at a convenient time.  */

void
quit (void)
{
  serial_t gdb_stdout_serial = serial_fdopen (1);

  target_terminal_ours ();

  /* We want all output to appear now, before we print "Quit".  We
     have 3 levels of buffering we have to flush (it's possible that
     some of these should be changed to flush the lower-level ones
     too):  */

  /* 1.  The _filtered buffer.  */
  wrap_here ((char *) 0);

  /* 2.  The stdio buffer.  */
  gdb_flush (gdb_stdout);
  gdb_flush (gdb_stderr);

  /* 3.  The system-level buffer.  */
  SERIAL_DRAIN_OUTPUT (gdb_stdout_serial);
  SERIAL_UN_FDOPEN (gdb_stdout_serial);

  annotate_error_begin ();

  /* Don't use *_filtered; we don't want to prompt the user to continue.  */
  if (quit_pre_print)
    fprintf_unfiltered (gdb_stderr, quit_pre_print);

#ifdef __MSDOS__
  /* No steenking SIGINT will ever be coming our way when the
     program is resumed.  Don't lie.  */
  fprintf_unfiltered (gdb_stderr, "Quit\n");
#else
  if (job_control
  /* If there is no terminal switching for this target, then we can't
     possibly get screwed by the lack of job control.  */
      || current_target.to_terminal_ours == NULL)
    fprintf_unfiltered (gdb_stderr, "Quit\n");
  else
    fprintf_unfiltered (gdb_stderr,
	       "Quit (expect signal SIGINT when the program is resumed)\n");
#endif
  return_to_top_level (RETURN_QUIT);
}


#if defined(_MSC_VER)		/* should test for wingdb instead? */

/*
 * Windows translates all keyboard and mouse events 
 * into a message which is appended to the message 
 * queue for the process.
 */

void
notice_quit (void)
{
  int k = win32pollquit ();
  if (k == 1)
    quit_flag = 1;
  else if (k == 2)
    immediate_quit = 1;
}

#else /* !defined(_MSC_VER) */

void
notice_quit (void)
{
  /* Done by signals */
}

#endif /* !defined(_MSC_VER) */

/* Control C comes here */
void
request_quit (int signo)
{
  quit_flag = 1;
  /* Restore the signal handler.  Harmless with BSD-style signals, needed
     for System V-style signals.  So just always do it, rather than worrying
     about USG defines and stuff like that.  */
  signal (signo, request_quit);

#ifdef REQUEST_QUIT
  REQUEST_QUIT;
#else
  if (immediate_quit)
    quit ();
#endif
}

/* Memory management stuff (malloc friends).  */

/* Make a substitute size_t for non-ANSI compilers. */

#ifndef HAVE_STDDEF_H
#ifndef size_t
#define size_t unsigned int
#endif
#endif

#if !defined (USE_MMALLOC)

PTR
mcalloc (PTR md, size_t number, size_t size)
{
  return calloc (number, size);
}

PTR
mmalloc (PTR md, size_t size)
{
  return malloc (size);
}

PTR
mrealloc (PTR md, PTR ptr, size_t size)
{
  if (ptr == 0)			/* Guard against old realloc's */
    return malloc (size);
  else
    return realloc (ptr, size);
}

void
mfree (PTR md, PTR ptr)
{
  free (ptr);
}

#endif /* USE_MMALLOC */

#if !defined (USE_MMALLOC) || defined (NO_MMCHECK)

void
init_malloc (void *md)
{
}

#else /* Have mmalloc and want corruption checking */

static void
malloc_botch (void)
{
  fprintf_unfiltered (gdb_stderr, "Memory corruption\n");
  abort ();
}

/* Attempt to install hooks in mmalloc/mrealloc/mfree for the heap specified
   by MD, to detect memory corruption.  Note that MD may be NULL to specify
   the default heap that grows via sbrk.

   Note that for freshly created regions, we must call mmcheckf prior to any
   mallocs in the region.  Otherwise, any region which was allocated prior to
   installing the checking hooks, which is later reallocated or freed, will
   fail the checks!  The mmcheck function only allows initial hooks to be
   installed before the first mmalloc.  However, anytime after we have called
   mmcheck the first time to install the checking hooks, we can call it again
   to update the function pointer to the memory corruption handler.

   Returns zero on failure, non-zero on success. */

#ifndef MMCHECK_FORCE
#define MMCHECK_FORCE 0
#endif

void
init_malloc (void *md)
{
  if (!mmcheckf (md, malloc_botch, MMCHECK_FORCE))
    {
      /* Don't use warning(), which relies on current_target being set
         to something other than dummy_target, until after
         initialize_all_files(). */

      fprintf_unfiltered
	(gdb_stderr, "warning: failed to install memory consistency checks; ");
      fprintf_unfiltered
	(gdb_stderr, "configuration should define NO_MMCHECK or MMCHECK_FORCE\n");
    }

  mmtrace ();
}

#endif /* Have mmalloc and want corruption checking  */

/* Called when a memory allocation fails, with the number of bytes of
   memory requested in SIZE. */

NORETURN void
nomem (long size)
{
/* JAGag06777 - Suggest the user to use chatr to increase data segment 
   size for PA gdb when we run out of virtual memory.. BTW, not too 
   sure if i can use any of gdb's print routines here, as some of them 
   allocate memory themselves! */

#if defined (GDB_TARGET_IS_HPPA) || defined (GDB_TARGET_IS_HPPA_20W)
 if (size > 0)
    {
      internal_error ("virtual memory exhausted: can't allocate %ld bytes. \nTo overcome this you could try the 'chatr' options +q3p/+q4p on gdb executable. See the chatr man page for more details.", size);
    }
  else
    {
      internal_error ("virtual memory exhausted. \nTo overcome this you could try the 'chatr' options +q3p/+q4p on gdb executable. See the chatr man page for more details.");
    }
#else
  if (size > 0)
    {
      internal_error ("virtual memory exhausted: can't allocate %ld bytes.", size);
    }
  else
    {
      internal_error ("virtual memory exhausted.");
    }
#endif
}

/* Like mmalloc but get error if no storage available, and protect against
   the caller wanting to allocate zero bytes.  Whether to return NULL for
   a zero byte request, or translate the request into a request for one
   byte of zero'd storage, is a religious issue. */

PTR
xmmalloc (PTR md, long size)
{
  register PTR val;

  if (size == 0)
    {
      val = NULL;
    }
  else if ((val = mmalloc (md, size)) == NULL)
    {
      nomem (size);
    }
  return (val);
}

/* Like mrealloc but get error if no storage available.  */

PTR
xmrealloc (PTR md, PTR ptr, long size)
{
  register PTR val;

  if (ptr != NULL)
    {
      val = mrealloc (md, ptr, size);
    }
  else
    {
      val = mmalloc (md, size);
    }
  if (val == NULL)
    {
      nomem (size);
    }
  return (val);
}

/* Like malloc but get error if no storage available, and protect against
   the caller wanting to allocate zero bytes.  */

PTR
xmalloc (size_t size)
{
  return (xmmalloc ((PTR) NULL, size));
}

/* Like calloc but get error if no storage available */

PTR
xcalloc (size_t number, size_t size)
{
  void *mem = mcalloc (NULL, number, size);
  if (mem == NULL)
    nomem (number * size);
  return mem;
}

/* Like mrealloc but get error if no storage available.  */

PTR
xrealloc (PTR ptr, size_t size)
{
  return (xmrealloc ((PTR) NULL, ptr, size));
}


/* My replacement for the read system call.
   Used like `read' but keeps going if `read' returns too soon.  */

int
myread (int desc, char *addr, int len)
{
  register int val;
  int orglen = len;

  while (len > 0)
    {
      val = (int) read (desc, addr, len);
      if (val < 0)
	return val;
      if (val == 0)
	return orglen - len;
      len -= val;
      addr += val;
    }
  return orglen;
}

/* Make a copy of the string at PTR with SIZE characters
   (and add a null character at the end in the copy).
   Uses malloc to get the space.  Returns the address of the copy.  */

char *
savestring (const char *ptr, int size)
{
  register char *p = (char *) xmalloc (size + 1);
  memcpy (p, ptr, size);
  p[size] = 0;
  return p;
}

char *
msavestring (void *md, const char *ptr, int size)
{
  register char *p = (char *) xmmalloc (md, size + 1);
  memcpy (p, ptr, size);
  p[size] = 0;
  return p;
}

/* The "const" is so it compiles under DGUX (which prototypes strsave
   in <string.h>.  FIXME: This should be named "xstrsave", shouldn't it?
   Doesn't real strsave return NULL if out of memory?  */
char *
strsave (const char *ptr)
{
  return savestring (ptr, (int) strlen (ptr));
}

char *
mstrsave (void *md, const char *ptr)
{
  return (msavestring (md, ptr, (int) strlen (ptr)));
}

void
print_spaces (register int n, register struct ui_file *file)
{
  fputs_unfiltered (n_spaces (n), file);
}

/* Print a host address.  */

void
gdb_print_host_address (void *addr, struct ui_file *stream)
{

  /* We could use the %p conversion specifier to fprintf if we had any
     way of knowing whether this host supports it.  But the following
     should work on the Alpha and on 32 bit machines.  */

  fprintf_filtered (stream, "0x%lx", (unsigned long) addr);
}

/* Ask user a y-or-n question and return 1 iff answer is yes.
   Takes three args which are given to printf to print the question.
   The first, a control string, should end in "? ".
   It should not say how to answer, because we do that.  */

/* VARARGS */
int
query (char *ctlstr,...)
{
  va_list args;
  register int answer;
  register int ans2;
  int retval = 0; /* initialize for compiler warning */
  extern int caution;
  struct ui_file *tmp_gdb_stdout = 0; /* initialize for compiler warning */

  if (profile_on)
    return 1;   /* Say, what is it that we are agreeing to ??  GOK :) */

  va_start (args, ctlstr);

  /* baskar, to display the queries in gdb screen, even when the o/p is 
     redirected to a file */

  if (redirection_on)
    {
      tmp_gdb_stdout = gdb_stdout;
      gdb_stdout = gdb_stdout_saved;
    } 

  if (query_hook)
    {
      return query_hook (ctlstr, args);
    }

  /* If the GUI is shutting down, we cannot be asking questions.
     See that the SIGHUP handler clears the variable caution.
     Jul 30th 2002, Srikanth.
  */
  if (nimbus_version && !caution)
    return 1;

  /* Automatically answer "yes" if input is not from a terminal.  */
  if (!nimbus_version && !input_from_terminal_p ())
    return 1;
#ifdef MPW
  /* FIXME Automatically answer "yes" if called from MacGDB.  */
  if (mac_app)
    return 1;
#endif /* MPW */

  while (1)
    {
      wrap_here ("");		/* Flush any buffered output */
      gdb_flush (gdb_stdout);

      if (annotation_level > 1)
	printf_filtered ("\n\032\032pre-query\n");

      if (nimbus_version)
        {
          printf_unfiltered ("\033\032\032qA");
          gdb_flush (gdb_stdout);
        }

      vfprintf_filtered (gdb_stdout, ctlstr, args);
      printf_filtered ("(y or n) ");

      if (annotation_level > 1)
	printf_filtered ("\n\032\032query\n");

#ifdef MPW
      /* If not in MacGDB, move to a new line so the entered line doesn't
         have a prompt on the front of it. */
      if (!mac_app)
	fputs_unfiltered ("\n", gdb_stdout);
#endif /* MPW */

      wrap_here ("");
      gdb_flush (gdb_stdout);

#if defined(TUI)
      if (!tui_version || cmdWin == tuiWinWithFocus ())
#endif
	answer = fgetc (stdin);
#if defined(TUI)
      else
	answer = (unsigned char) tuiBufferGetc ();

#endif
      clearerr (stdin);		/* in case of C-d */

      if (answer == EOF)	/* C-d */
	{
	  retval = 1;
	  break;
	}
      /* Eat rest of input line, to EOF or newline */
      if ((answer != '\n') || (tui_version && answer != '\r'))
	do
	  {
#if defined(TUI)
	    if (!tui_version || cmdWin == tuiWinWithFocus ())
#endif
	      ans2 = fgetc (stdin);
#if defined(TUI)
	    else
	      ans2 = (unsigned char) tuiBufferGetc ();
#endif
	    clearerr (stdin);
	  }
	while (ans2 != EOF && ans2 != '\n' && ans2 != '\r');
      TUIDO (((TuiOpaqueFuncPtr) tui_vStartNewLines, 1));

      if (answer >= 'a')
	answer -= 040;
      if (answer == 'Y')
	{
	  retval = 1;
	  break;
	}
      if (answer == 'N')
	{
	  retval = 0;
	  break;
	}
      printf_filtered ("Please answer y or n.\n");
    }

  if (annotation_level > 1)
    printf_filtered ("\n\032\032post-query\n");

  if (redirection_on)
    gdb_stdout = tmp_gdb_stdout;

  return retval;
}


/* Parse a C escape sequence.  STRING_PTR points to a variable
   containing a pointer to the string to parse.  That pointer
   should point to the character after the \.  That pointer
   is updated past the characters we use.  The value of the
   escape sequence is returned.

   A negative value means the sequence \ newline was seen,
   which is supposed to be equivalent to nothing at all.

   If \ is followed by a null character, we return a negative
   value and leave the string pointer pointing at the null character.

   If \ is followed by 000, we return 0 and leave the string pointer
   after the zeros.  A value of 0 does not mean end of string.  */

int
parse_escape (char **string_ptr)
{
  register int c = *(*string_ptr)++;
  switch (c)
    {
    case 'a':
      return 007;		/* Bell (alert) char */
    case 'b':
      return '\b';
    case 'e':			/* Escape character */
      return 033;
    case 'f':
      return '\f';
    case 'n':
      return '\n';
    case 'r':
      return '\r';
    case 't':
      return '\t';
    case 'v':
      return '\v';
    case '\n':
      return -2;
    case 0:
      (*string_ptr)--;
      return 0;
    case '^':
      c = *(*string_ptr)++;
      if (c == '\\')
	c = parse_escape (string_ptr);
      if (c == '?')
	return 0177;
      return (c & 0200) | (c & 037);

    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
      {
	register int i = c - '0';
	register int count = 0;
	while (++count < 3)
	  {
	    if ((c = *(*string_ptr)++) >= '0' && c <= '7')
	      {
		i *= 8;
		i += c - '0';
	      }
	    else
	      {
		(*string_ptr)--;
		break;
	      }
	  }
	return i;
      }
    default:
      return c;
    }
}

/* Print the character C on STREAM as part of the contents of a literal
   string whose delimiter is QUOTER.  Note that this routine should only
   be call for printing things which are independent of the language
   of the program being debugged. */

static void printchar (int c, void (*do_fputs) (const char *, struct ui_file*), void (*do_fprintf) (struct ui_file*, const char *, ...), struct ui_file *stream, int quoter);

static void
printchar (int c,
     	   void (*do_fputs) (const char *, struct ui_file *),
     	   void (*do_fprintf) (struct ui_file *, const char *, ...),
     	   struct ui_file *stream,
     	   int quoter)
{

  c &= 0xFF;			/* Avoid sign bit follies */

  if (c < 0x20 ||		/* Low control chars */
      (c >= 0x7F && c < 0xA0) ||	/* DEL, High controls */
      (sevenbit_strings && c >= 0x80))
    {				/* high order bit set */
      switch (c)
	{
	case '\n':
	  do_fputs ("\\n", stream);
	  break;
	case '\b':
	  do_fputs ("\\b", stream);
	  break;
	case '\t':
	  do_fputs ("\\t", stream);
	  break;
	case '\f':
	  do_fputs ("\\f", stream);
	  break;
	case '\r':
	  do_fputs ("\\r", stream);
	  break;
	case '\033':
	  do_fputs ("\\e", stream);
	  break;
	case '\007':
	  do_fputs ("\\a", stream);
	  break;
	default:
	  do_fprintf (stream, "\\%.3o", (unsigned int) c);
	  break;
	}
    }
  else
    {
      if (c == '\\' || c == quoter)
	do_fputs ("\\", stream);
      do_fprintf (stream, "%c", c);
    }
}

/* Print the character C on STREAM as part of the contents of a
   literal string whose delimiter is QUOTER.  Note that these routines
   should only be call for printing things which are independent of
   the language of the program being debugged. */

void
fputstr_filtered (const char *str, int quoter, struct ui_file *stream)
{
  while (*str)
    printchar (*str++, fputs_filtered, fprintf_filtered, stream, quoter);
}

void
fputstr_unfiltered (const char *str, int quoter, struct ui_file *stream)
{
  while (*str)
    printchar (*str++, fputs_unfiltered, fprintf_unfiltered, stream, quoter);
}

void
fputstrn_unfiltered (const char *str, int n, int quoter, struct ui_file *stream)
{
  int i;
  for (i = 0; i < n; i++)
    printchar (str[i], fputs_unfiltered, fprintf_unfiltered, stream, quoter);
}



/* Number of lines per page or UINT_MAX if paging is disabled.  */
static unsigned int lines_per_page;
/* Number of chars per line or UINT_MAX if line folding is disabled.  */
static unsigned int chars_per_line;
/* Current count of lines printed on this page, chars on this line.  */
static unsigned int lines_printed, chars_printed;

/* Buffer and start column of buffered text, for doing smarter word-
   wrapping.  When someone calls wrap_here(), we start buffering output
   that comes through fputs_filtered().  If we see a newline, we just
   spit it out and forget about the wrap_here().  If we see another
   wrap_here(), we spit it out and remember the newer one.  If we see
   the end of the line, we spit out a newline, the indent, and then
   the buffered output.  */

/* Malloc'd buffer with chars_per_line+2 bytes.  Contains characters which
   are waiting to be output (they have already been counted in chars_printed).
   When wrap_buffer[0] is null, the buffer is empty.  */
static char *wrap_buffer;

/* Pointer in wrap_buffer to the next character to fill.  */
static char *wrap_pointer;

/* String to indent by if the wrap occurs.  Must not be NULL if wrap_column
   is non-zero.  */
static char *wrap_indent;

/* Column number on the screen where wrap_buffer begins, or 0 if wrapping
   is not in effect.  */
static int wrap_column;


/* Inialize the lines and chars per page */
void
init_page_info (void)
{
#if defined(TUI)
  if (tui_version && m_winPtrNotNull (cmdWin))
    {
      lines_per_page = cmdWin->generic.height;
      chars_per_line = cmdWin->generic.width;
    }
  else
#endif
    {
      /* These defaults will be used if we are unable to get the correct
         values from termcap.  */
#if defined(__GO32__)
      lines_per_page = ScreenRows ();
      chars_per_line = ScreenCols ();
#else
      lines_per_page = 24;
      chars_per_line = 80;

#if !defined (MPW) && !defined (_WIN32)
      /* No termcap under MPW, although might be cool to do something
         by looking at worksheet or console window sizes. */
      /* Initialize the screen height and width from termcap.  */
      {
	char *termtype = getenv ("TERM");

	/* Positive means success, nonpositive means failure.  */
	int status;

	/* 2048 is large enough for all known terminals, according to the
	   GNU termcap manual.  */
	char term_buffer[2048];

	if (termtype)
	  {
	    status = tgetent (term_buffer, termtype);
	    if (status > 0)
	      {
		int val;
		int running_in_emacs = getenv ("EMACS") != NULL;

		val = tgetnum ("li");
		if (val >= 0 && !running_in_emacs)
		  lines_per_page = val;
		else
		  /* The number of lines per page is not mentioned
		     in the terminal description.  This probably means
		     that paging is not useful (e.g. emacs shell window),
		     so disable paging.  */
		  lines_per_page = UINT_MAX;

		val = tgetnum ("co");
		if (val >= 0)
		  chars_per_line = val;
	      }
	  }
      }
#endif /* MPW */

#if defined(SIGWINCH) && defined(SIGWINCH_HANDLER)

      /* If there is a better way to determine the window size, use it. */
      SIGWINCH_HANDLER (SIGWINCH);
#endif
#endif
      /* If the output is not a terminal, don't paginate it.  */
      if (!ui_file_isatty (gdb_stdout))
	lines_per_page = UINT_MAX;
    }				/* the command_line_version */
  set_width ();
}

static void
set_width (void)
{
  if (chars_per_line == 0)
    init_page_info ();

  if (!wrap_buffer)
    {
      wrap_buffer = (char *) xmalloc (chars_per_line + 2);
      wrap_buffer[0] = '\0';
    }
  else
    wrap_buffer = (char *) xrealloc (wrap_buffer, chars_per_line + 2);
  wrap_pointer = wrap_buffer;	/* Start it at the beginning */
}

/* ARGSUSED */
static void
set_width_command (char *args, int from_tty, struct cmd_list_element *c)
{
  set_width ();
}

/* Wait, so the user can read what's on the screen.  Prompt the user
   to continue by pressing RETURN.  */

static void
prompt_for_continue (void)
{
  char *ignore;
  char cont_prompt[120];

  /* srikanth, what is the use of this variable that gets set and unset by
     the set pagination on/off command ??? I am using this here.
  */
  if (!pagination_enabled)
    return;

  if (profile_on)
    return;

  if (annotation_level > 1)
    printf_unfiltered ("\n\032\032pre-prompt-for-continue\n");

  if (nimbus_version)
    {
      printf_unfiltered ("\033\032\032mA");
      gdb_flush (gdb_stdout);
    }

  strcpy (cont_prompt,
	  "---Type <return> to continue, or q <return> to quit---");
  if (annotation_level > 1)
    strcat (cont_prompt, "\n\032\032prompt-for-continue\n");

  /* We must do this *before* we call gdb_readline, else it will eventually
     call us -- thinking that we're trying to print beyond the end of the 
     screen.  */
  reinitialize_more_filter ();

  immediate_quit++;
  /* On a real operating system, the user can quit with SIGINT.
     But not on GO32.

     'q' is provided on all systems so users don't have to change habits
     from system to system, and because telling them what to do in
     the prompt is more user-friendly than expecting them to think of
     SIGINT.  */
  /* Call readline, not gdb_readline, because GO32 readline handles control-C
     whereas control-C to gdb_readline will cause the user to get dumped
     out to DOS.  */
  ignore = readline (cont_prompt);

  if (annotation_level > 1)
    printf_unfiltered ("\n\032\032post-prompt-for-continue\n");

  if (ignore)
    {
      char *p = ignore;
      while (*p == ' ' || *p == '\t')
	++p;
      if (p[0] == 'q')
	{
	  if (!event_loop_p)
	    request_quit (SIGINT);
	  else
	    async_request_quit (0);
	}
      free (ignore);
    }
  immediate_quit--;

  /* Now we have to do this again, so that GDB will know that it doesn't
     need to save the ---Type <return>--- line at the top of the screen.  */
  reinitialize_more_filter ();

  dont_repeat ();		/* Forget prev cmd -- CR won't repeat it. */
}

/* Reinitialize filter; ie. tell it to reset to original values.  */

void
reinitialize_more_filter (void)
{
  lines_printed = 0;
  chars_printed = 0;
}

/* Indicate that if the next sequence of characters overflows the line,
   a newline should be inserted here rather than when it hits the end. 
   If INDENT is non-null, it is a string to be printed to indent the
   wrapped part on the next line.  INDENT must remain accessible until
   the next call to wrap_here() or until a newline is printed through
   fputs_filtered().

   If the line is already overfull, we immediately print a newline and
   the indentation, and disable further wrapping.

   If we don't know the width of lines, but we know the page height,
   we must not wrap words, but should still keep track of newlines
   that were explicitly printed.

   INDENT should not contain tabs, as that will mess up the char count
   on the next line.  FIXME.

   This routine is guaranteed to force out any output which has been
   squirreled away in the wrap_buffer, so wrap_here ((char *)0) can be
   used to force out output from the wrap_buffer.  */

void
wrap_here (char *indent)
{
  /* This should have been allocated, but be paranoid anyway. */
  if (!wrap_buffer)
    abort ();

  if (wrap_buffer[0])
    {
      *wrap_pointer = '\0';
      fputs_unfiltered (wrap_buffer, gdb_stdout);
    }
  wrap_pointer = wrap_buffer;
  wrap_buffer[0] = '\0';
  if (chars_per_line == UINT_MAX)	/* No line overflow checking */
    {
      wrap_column = 0;
    }
  else if (chars_printed >= chars_per_line)
    {
      puts_filtered ("\n");
      if (indent != NULL)
	puts_filtered (indent);
      wrap_column = 0;
    }
  else
    {
      wrap_column = chars_printed;
      if (indent == NULL)
	wrap_indent = "";
      else
	wrap_indent = indent;
    }
}

/* Ensure that whatever gets printed next, using the filtered output
   commands, starts at the beginning of the line.  I.E. if there is
   any pending output for the current line, flush it and start a new
   line.  Otherwise do nothing. */

void
begin_line (void)
{
  if (chars_printed > 0)
    {
      puts_filtered ("\n");
    }
}

/* Like fputs but if FILTER is true, pause after every screenful.

   Regardless of FILTER can wrap at points other than the final
   character of a line.

   Unlike fputs, fputs_maybe_filtered does not return a value.
   It is OK for LINEBUFFER to be NULL, in which case just don't print
   anything.

   Note that a longjmp to top level may occur in this routine (only if
   FILTER is true) (since prompt_for_continue may do so) so this
   routine should not be called when cleanups are not in place.  */

static void
fputs_maybe_filtered (const char *linebuffer, struct ui_file *stream, int filter)
{
  const char *lineptr;

  if (linebuffer == 0)
    return;

  /* Don't do any filtering if it is disabled.  */
  /* Madhavi, don't filter if redirection is turned on, ts_filestream is
   * made to point to a file in redirect_command in this case */
  if (stream != gdb_stdout
      || (lines_per_page == UINT_MAX && chars_per_line == UINT_MAX) || redirection_on)
    {
      fputs_unfiltered (linebuffer, stream);
      return;
    }

  /* Go through and output each character.  Show line extension
     when this is necessary; prompt user for new page when this is
     necessary.  */

  lineptr = linebuffer;
  while (*lineptr)
    {
      /* Possible new page.  */
      if (filter &&
	  (lines_printed >= lines_per_page - 1))
	prompt_for_continue ();

      while (*lineptr && *lineptr != '\n')
	{
	  /* Print a single line.  */
	  if (*lineptr == '\t')
	    {
	      if (wrap_column)
		*wrap_pointer++ = '\t';
	      else
		fputc_unfiltered ('\t', stream);
	      /* Shifting right by 3 produces the number of tab stops
	         we have already passed, and then adding one and
	         shifting left 3 advances to the next tab stop.  */
	      chars_printed = ((chars_printed >> 3) + 1) << 3;
	      lineptr++;
	    }
	  else
	    {
	      if (wrap_column)
		*wrap_pointer++ = *lineptr;
	      else
		fputc_unfiltered (*lineptr, stream);
	      chars_printed++;
	      lineptr++;
	    }

	  if (chars_printed >= chars_per_line)
	    {
	      unsigned int save_chars = chars_printed;

	      chars_printed = 0;
	      lines_printed++;
	      /* If we aren't actually wrapping, don't output newline --
	         if chars_per_line is right, we probably just overflowed
	         anyway; if it's wrong, let us keep going.  */
	      if (wrap_column)
		fputc_unfiltered ('\n', stream);

	      /* Possible new page.  */
	      if (lines_printed >= lines_per_page - 1)
		prompt_for_continue ();

	      /* Now output indentation and wrapped string */
	      if (wrap_column)
		{
		  fputs_unfiltered (wrap_indent, stream);
		  *wrap_pointer = '\0';		/* Null-terminate saved stuff */
		  fputs_unfiltered (wrap_buffer, stream);	/* and eject it */
		  /* FIXME, this strlen is what prevents wrap_indent from
		     containing tabs.  However, if we recurse to print it
		     and count its chars, we risk trouble if wrap_indent is
		     longer than (the user settable) chars_per_line. 
		     Note also that this can set chars_printed > chars_per_line
		     if we are printing a long string.  */
		  chars_printed = (int) strlen (wrap_indent)
		    + (save_chars - wrap_column);
		  wrap_pointer = wrap_buffer;	/* Reset buffer */
		  wrap_buffer[0] = '\0';
		  wrap_column = 0;	/* And disable fancy wrap */
		}
	    }
	}

      if (*lineptr == '\n')
	{
	  chars_printed = 0;
	  wrap_here ((char *) 0);	/* Spit out chars, cancel further wraps */
	  lines_printed++;
	  fputc_unfiltered ('\n', stream);
	  lineptr++;
	}
    }
}

void
fputs_filtered (const char *linebuffer, struct ui_file *stream)
{
  fputs_maybe_filtered (linebuffer, stream, 1);
}

int
putchar_unfiltered (int c)
{
  char buf = c;
  ui_file_write (gdb_stdout, &buf, 1);
  return c;
}

int
fputc_unfiltered (int c, struct ui_file *stream)
{
  char buf = c;
  ui_file_write (stream, &buf, 1);
  return c;
}

int
fputc_filtered (int c, struct ui_file *stream)
{
  char buf[2];

  buf[0] = c;
  buf[1] = 0;
  fputs_filtered (buf, stream);
  return c;
}

/* puts_debug is like fputs_unfiltered, except it prints special
   characters in printable fashion.  */

void
puts_debug (char *prefix, char *string, char *suffix)
{
  int ch;

  /* Print prefix and suffix after each line.  */
  static int new_line = 1;
  static int return_p = 0;
  static char *prev_prefix = "";
  static char *prev_suffix = "";

  if (*string == '\n')
    return_p = 0;

  /* If the prefix is changing, print the previous suffix, a new line,
     and the new prefix.  */
  if ((return_p || (strcmp (prev_prefix, prefix) != 0)) && !new_line)
    {
      fputs_unfiltered (prev_suffix, gdb_stdlog);
      fputs_unfiltered ("\n", gdb_stdlog);
      fputs_unfiltered (prefix, gdb_stdlog);
    }

  /* Print prefix if we printed a newline during the previous call.  */
  if (new_line)
    {
      new_line = 0;
      fputs_unfiltered (prefix, gdb_stdlog);
    }

  prev_prefix = prefix;
  prev_suffix = suffix;

  /* Output characters in a printable format.  */
  while ((ch = *string++) != '\0')
    {
      switch (ch)
	{
	default:
	  if (isprint (ch))
	    fputc_unfiltered (ch, gdb_stdlog);

	  else
	    fprintf_unfiltered (gdb_stdlog, "\\x%02x", ch & 0xff);
	  break;

	case '\\':
	  fputs_unfiltered ("\\\\", gdb_stdlog);
	  break;
	case '\b':
	  fputs_unfiltered ("\\b", gdb_stdlog);
	  break;
	case '\f':
	  fputs_unfiltered ("\\f", gdb_stdlog);
	  break;
	case '\n':
	  new_line = 1;
	  fputs_unfiltered ("\\n", gdb_stdlog);
	  break;
	case '\r':
	  fputs_unfiltered ("\\r", gdb_stdlog);
	  break;
	case '\t':
	  fputs_unfiltered ("\\t", gdb_stdlog);
	  break;
	case '\v':
	  fputs_unfiltered ("\\v", gdb_stdlog);
	  break;
	}

      return_p = ch == '\r';
    }

  /* Print suffix if we printed a newline.  */
  if (new_line)
    {
      fputs_unfiltered (suffix, gdb_stdlog);
      fputs_unfiltered ("\n", gdb_stdlog);
    }
}


/* Print a variable number of ARGS using format FORMAT.  If this
   information is going to put the amount written (since the last call
   to REINITIALIZE_MORE_FILTER or the last page break) over the page size,
   call prompt_for_continue to get the users permision to continue.

   Unlike fprintf, this function does not return a value.

   We implement three variants, vfprintf (takes a vararg list and stream),
   fprintf (takes a stream to write on), and printf (the usual).

   Note also that a longjmp to top level may occur in this routine
   (since prompt_for_continue may do so) so this routine should not be
   called when cleanups are not in place.  */

static void
vfprintf_maybe_filtered (struct ui_file *stream,
     			 const char *format,
     			 va_list args,
     			 int filter)
{
  char *linebuffer;
  struct cleanup *old_cleanups;

  vasprintf (&linebuffer, format, args);
  if (linebuffer == NULL)
    {
      fputs_unfiltered ("\ngdb: virtual memory exhausted.\n", gdb_stderr);
      exit (1);
    }
  old_cleanups = make_cleanup (free, linebuffer);
  fputs_maybe_filtered (linebuffer, stream, filter);
  do_cleanups (old_cleanups);
}


void
vfprintf_filtered (struct ui_file *stream, const char *format, va_list args)
{
  vfprintf_maybe_filtered (stream, format, args, 1);
}

void
vfprintf_unfiltered (struct ui_file *stream, const char *format, va_list args)
{
  char *linebuffer;
  struct cleanup *old_cleanups;

  vasprintf (&linebuffer, format, args);
  if (linebuffer == NULL)
    {
      fputs_unfiltered ("\ngdb: virtual memory exhausted.\n", gdb_stderr);
      exit (1);
    }
  old_cleanups = make_cleanup (free, linebuffer);
  fputs_unfiltered (linebuffer, stream);
  do_cleanups (old_cleanups);
}

void
vprintf_filtered (const char *format, va_list args)
{
  vfprintf_maybe_filtered (gdb_stdout, format, args, 1);
}

void
vprintf_unfiltered (const char *format, va_list args)
{
  vfprintf_unfiltered (gdb_stdout, format, args);
}

void
fprintf_filtered (struct ui_file * stream, const char *format,...)
{
  va_list args;
  va_start (args, format);
  vfprintf_filtered (stream, format, args);
  va_end (args);
}

void
fprintf_unfiltered (struct ui_file * stream, const char *format,...)
{
  va_list args;
  va_start (args, format);
  vfprintf_unfiltered (stream, format, args);
  va_end (args);
}

/* Like fprintf_filtered, but prints its result indented.
   Called as fprintfi_filtered (spaces, stream, format, ...);  */

void
fprintfi_filtered (int spaces, struct ui_file * stream, const char *format,...)
{
  va_list args;
  va_start (args, format);
  print_spaces_filtered (spaces, stream);

  vfprintf_filtered (stream, format, args);
  va_end (args);
}


void
printf_filtered (const char *format,...)
{
  va_list args;
  va_start (args, format);
  vfprintf_filtered (gdb_stdout, format, args);
  va_end (args);
}


void
printf_unfiltered (const char *format,...)
{
  va_list args;
  va_start (args, format);
  vfprintf_unfiltered (gdb_stdout, format, args);
  va_end (args);
}

/* Like printf_filtered, but prints it's result indented.
   Called as printfi_filtered (spaces, format, ...);  */

void
printfi_filtered (int spaces, const char *format,...)
{
  va_list args;
  va_start (args, format);
  print_spaces_filtered (spaces, gdb_stdout);
  vfprintf_filtered (gdb_stdout, format, args);
  va_end (args);
}

/* Easy -- but watch out!

   This routine is *not* a replacement for puts()!  puts() appends a newline.
   This one doesn't, and had better not!  */

void
puts_filtered (const char *string)
{
  fputs_filtered (string, gdb_stdout);
}

void
puts_unfiltered (const char *string)
{
  fputs_unfiltered (string, gdb_stdout);
}

/* Return a pointer to N spaces and a null.  The pointer is good
   until the next call to here.  */
char *
n_spaces (int n)
{
  char *t;
  static char *spaces = 0;
  static int max_spaces = -1;

  if (n > max_spaces)
    {
      if (spaces)
	free (spaces);
      spaces = (char *) xmalloc (n + 1);
      for (t = spaces + n; t != spaces;)
	*--t = ' ';
      spaces[n] = '\0';
      max_spaces = n;
    }

  return spaces + max_spaces - n;
}

/* Print N spaces.  */
void
print_spaces_filtered (int n, struct ui_file *stream)
{
  fputs_filtered (n_spaces (n), stream);
}

/* C++ demangler stuff.  */

/* fprintf_symbol_filtered attempts to demangle NAME, a symbol in language
   LANG, using demangling args ARG_MODE, and print it filtered to STREAM.
   If the name is not mangled, or the language for the name is unknown, or
   demangling is off, the name is printed in its "raw" form. */

void
fprintf_symbol_filtered (struct ui_file *stream,
     			 char *name,
     			 enum language lang,
     			 int arg_mode)
{
  char *demangled;

  if (name != NULL)
    {
      /* If user wants to see raw output, no problem.  */
      if (!demangle)
	{
	  fputs_filtered (name, stream);
	}
      else
	{
	  switch (lang)
	    {
	    case language_cplus:
	      demangled = cplus_demangle (name, arg_mode);
	      break;
	    case language_java:
	      demangled = cplus_demangle (name, arg_mode | DMGL_JAVA);
	      break;
	    case language_chill:
	      demangled = chill_demangle (name);
	      break;
	    default:
	      demangled = NULL;
	      break;
	    }
          /* Fix for Millicode defect -Dinesh 03/28/02 */
          if (demangled && !strcmp(demangled,""))
            {
              demangled=NULL;
            }
	  fputs_filtered (demangled ? demangled : name, stream);
	  if (demangled != NULL)
	    {
	      free (demangled);
	    }
	}
    }
}

/* Do a strcmp() type operation on STRING1 and STRING2, ignoring any
   differences in whitespace.  Returns 0 if they match, non-zero if they
   don't (slightly different than strcmp()'s range of return values).

   As an extra hack, string1=="FOO(ARGS)" matches string2=="FOO".
   This "feature" is useful when searching for matching C++ function names
   (such as if the user types 'break FOO', where FOO is a mangled C++
   function). */

int
strcmp_iw (const char *string1, const char *string2)
{
  while ((*string1 != '\0') && (*string2 != '\0'))
    {
      while (isspace (*string1))
	{
	  string1++;
	}
      while (isspace (*string2))
	{
	  string2++;
	}
      if (*string1 != *string2)
	{
	  break;
	}
      if (*string1 != '\0')
	{
	  string1++;
	  string2++;
	}
    }

  /* srikanth, we need to ignore white spaces at the end too. Otherwise
     "foo(int)" will not match "foo(int) " */

  while (isspace(*string1))
    string1++;

  while (isspace(*string2))
    string2++;

  return (*string1 != '\0' && *string1 != '(') || (*string2 != '\0');
}


/* Poorva: Do a strcmp() type operation on STRING1 and STRING2, ignoring any
   whitespace and template angle brackets.  Returns 0 if they match
   (same as strcmp) and non-zero if they don't (slightly different 
   than strcmp()'s range of return values).
   e.g  A<int>::B<char> should match A::B e.g break  A::B::foo
   should match all A<blah>::B<blahblah>::foo
   A should not match A<char>::foo
      
   As an extra hack, string1=="FOO(ARGS)" matches string2=="FOO".
   or string1 == FOO<char>(ARGS) matches string2 == FOO
   This "feature" is useful when searching for matching C++ function names
   (such as if the user types 'break FOO', where FOO is a mangled C++
   function). 

   String1 will be the one with the extras e.g (ARGS) or angle brackets etc
   since that is the one we get from our symbol tables and so it has the full 
   name. String2 is the string passed in by the user which may or maynot 
   have angle brackets.

   e.g User passes in string2 = Foo<char>::bar
   and we are comparing against the symbols in another load module which has
   a global variable Foo and so string1 = Foo. The function should not return 
   that they match. Even if string1 = Foo::bar from another load module we 
   don't want it to match since we are looking for template class function.

   Algorithm used - 
   While either of string1 or string2 does not reach the end of the string do
   ignore any whitespaces,
   Then check if the characters in string1 and string2 are equal 
   and string1 is not a '<'.
   If they are not equal and the char in string1 is not '<' 
      break out of loop.
      
   If string1 is '<' then ignore everything inside the angle brackets.
   <...>. Takes care of nested brackets too by keeping a counter.

   At the end of this loop check that the char in string1 and string2 are 
   equal - if not then break. 
   
   Increment pointer of string1 and string2 to the next character.

   The way it matches (ARGS) is in the return statement - if it sees
   that string2 has come to an end and string1 is at '(' it means that
   we are matching A(ARGS) to A so it returns a match.

   skip_void is set to true to make something like Temp::conv(void)
   match Temp::conv().  It is set by the call in strcmp_iwt_void.
   */


int
strcmp_iwt_1 (int skip_void, const char *string1, const char *string2)
{
  const char * find_paren;
  int last_char_open_paren = 0;
  int ret = 0;

  if (string1 == NULL && string2 == NULL)
    return 0;
  if (string1 != NULL && string2 == NULL)
    return 1;
  if (string1 == NULL && string2 != NULL)
    return -1;

  while ((*string1 != '\0') && (*string2 != '\0'))
    {
      while (isspace (*string1))
	{
	  string1++;
	}
      while (isspace (*string2))
	{
	  string2++;
	}

      if (   skip_void 
	  && last_char_open_paren
	  && *string1 == 'v' 
	  && (strncmp (string1, "void", 4) == 0))
	{
	  /* Skip "void" only if the next non-whitespace is close paren */
	  find_paren = string1 + 4;
	  while (isspace (*find_paren))
	    find_paren++;
	  if (*find_paren == ')')
	    {
	      string1 += 4;
	      if (*string2 == 'v' && strncmp (string2, "void", 4) == 0) 
		string2 += 4;
	      continue;
	    }
	}
      else if (   skip_void 
	       && last_char_open_paren
	       && *string2 == 'v' 
	       && (strncmp (string2, "void", 4) == 0))
	{
	  /* Skip "void" only if the next non-whitespace is close paren */
	  find_paren = string1 + 4;
	  while (isspace (*find_paren))
	    find_paren++;
	  if (*find_paren == ')')
	    {
	      string2 += 4;
	      continue;
	    }
	}
      else if (*string1 != *string2 && *string1 != '<')
	{
	  break;
	}
      else if (*string1 == '<' && *string2 != '<')
        {
	  int count = 0;
	  while (*string1 != '\0')
	    {
	      if (*string1 == '<')
		++count;
	      if (*string1 == '>')
		--count;
	      string1 ++;
	      if (count == 0)
		break;
	    }

	  /* Now that you have skipped the angle brackets check
	     string1 and string2 again. e.g 
	     string2 is at Foo: and string1 was at Foo<
	     After getting past the angle brackets string1 is now
	     at Foo<char>: - we need to make sure that string1
	     and string2 both match before we increment them again.*/
	  
	  if (*string1 != *string2)
	    break;
	}
      if (*string1 != '\0')
	{
	  last_char_open_paren = *string1 == '(';
	  string1++;
	  string2++;
	}
    }

  /* Necessary for templated functions e.g 
     string1 = A should match string2 = A<char>()
     This is necessary so that we get past the angle brackets if any
     so that in the return statement we are at '(' after the A<char> 
     and we return 0 for a match.
     */

  if (*string1 == '<')
    {
      int count = 0;
      while (*string1 != '\0')
	{
	  if (*string1 == '<')
	    ++count;
	  if (*string1 == '>')
	    --count;
	  string1 ++;
	  if (count == 0)
	    break;
	}
    }

  /* srikanth, we need to ignore white spaces at the end too. Otherwise
     "foo(int)" will not match "foo(int) "
   */

  while (isspace (*string1))
    string1++;

  while (isspace (*string2))
    string2++;

   /* The way it matches (ARGS) is in the return statement - if it sees
   that string2 has come to an end and string1 is at '(' it means that
   we are matching A(ARGS) to A so it returns a match. */

  ret = (*string1 != '\0' && *string1 != '(') || (*string2 != '\0');

  /* if *string1 < *string2 return 1 else return -1 */
  if (ret) 
    {  
      if (*string2 == '\0' && *string1 != '\0')
	return -1; /* string2 is less than string1 */
      else if (*string1 == '\0' && *string2 != '\0')
	return 1; /* string1 is less than string2 */

      if (*string1 < *string2)
	return 1;
      else if (*string1 > *string2)
	return -1;
    }

  return ret;

    /* 
  return (*string1 != '\0' && *string1 != '(') || (*string2 != '\0');
  */
}


int
strcmp_iwt (const char *string1, const char *string2)
{
  return strcmp_iwt_1 (0, string1, string2);
}

int
strcmp_iwt_void (const char *string1, const char *string2)
{
  return strcmp_iwt_1 (1, string1, string2);
}

/*
   ** subset_compare()
   **    Answer whether string_to_compare is a full or partial match to
   **    template_string.  The partial match must be in sequence starting
   **    at index 0.
 */
int
subset_compare (char *string_to_compare, char *template_string)
{
  int match;
  if (template_string != (char *) NULL && string_to_compare != (char *) NULL &&
      strlen (string_to_compare) <= strlen (template_string))
    match = (strncmp (template_string,
		      string_to_compare,
		      strlen (string_to_compare)) == 0);
  else
    match = 0;
  return match;
}


static void pagination_on_command (char *arg, int from_tty);
static void
pagination_on_command ( char *arg, int from_tty)
{
  pagination_enabled = 1;
}

static void pagination_on_command (char *arg, int from_tty);
static void
pagination_off_command (char *arg, int from_tty)
{
  pagination_enabled = 0;
}


void
initialize_utils (void)
{
  struct cmd_list_element *c, *set, *show, *com;
  /* allocate memory on heap to avoid freeing of non-heap memory later */
  redirect_file_args = xstrdup (".gdb_output");
  redirect_file = xstrdup (".gdb_output");
  saved_filename = xstrdup (".gdb_output");
  logging_overwrite_args = xstrdup ("off");
  logging_redirect_args = xstrdup ("on");

  c = add_set_cmd ("width", class_support, var_uinteger,
		   (char *) &chars_per_line,
		   "Set number of characters gdb thinks are in a line.\n\n\
Usage:\nTo set new value:\n\tset width <INTEGER>\nTo see current value:\
\n\tshow width\n", &setlist);
  add_show_from_set (c, &showlist);
  c->function.sfunc = set_width_command;

  add_show_from_set
    (add_set_cmd ("height", class_support,
		  var_uinteger, (char *) &lines_per_page,
		  "Set number of lines gdb thinks are in a page.\n\n\
Usage:\nTo set new value:\n\tset height <INTEGER>\nTo see current value:\
\n\tshow height\n", &setlist),
     &showlist);

  init_page_info ();

  /* If the output is not a terminal, don't paginate it.  */
  if (!ui_file_isatty (gdb_stdout))
    lines_per_page = UINT_MAX;

  set_width_command ((char *) NULL, 0, c);

  add_show_from_set
    (add_set_cmd ("demangle", class_support, var_boolean,
		  (char *) &demangle,
	     "Set demangling of encoded C++ names when displaying symbols.\n\n\
Usage:\nTo set new value:\n\tset print demangle [on | off]\nTo see current value:\
\n\tshow print demangle\n", &setprintlist),
     &showprintlist);

  add_show_from_set
    (add_set_cmd ("pagination", class_support,
		  var_boolean, (char *) &pagination_enabled,
		  "Set state of pagination.\n\nUsage:\nTo set new value:\n\t\
set pagination [on | off]\nTo see current value:\n\tshow pagination\n", &setlist),
     &showlist);

  if (xdb_commands)
    {
      add_com ("am", class_support, pagination_on_command,
	       "Enable pagination");
      add_com ("sm", class_support, pagination_off_command,
	       "Disable pagination");
    }

  add_show_from_set
    (add_set_cmd ("sevenbit-strings", class_support, var_boolean,
		  (char *) &sevenbit_strings,
		  "Set printing of 8-bit characters in strings as \\nnn.\n\n\
Usage:\nTo set new value:\n\tset print sevenbit-strings [on | off]\nTo see current\
 value:\n\tshow sevenbit-strings\n", &setprintlist),
     &showprintlist);

  add_show_from_set
    (add_set_cmd ("print-changed-registers", class_support,
		  var_boolean, (char *) &print_changed_registers_enabled,
		  "Set showing of register changes.\n\nUsage:\nTo set new value:\n\t\
set print-changed-registers [on | off]\nTo see current value:\n\tshow \
print-changed-registers\n", &setlist), &showlist);

  add_show_from_set
    (add_set_cmd ("asm-demangle", class_support, var_boolean,
		  (char *) &asm_demangle,
		  "Set demangling of C++ names in disassembly listings.\n\nUsage:\n\
To set new value:\n\tset print asm-demangle [on | off]\nTo see current value:\n\tshow \
asm-demangle\n", &setprintlist), &showprintlist);

  /* Madhavi, commands added to support redirection to a file */
  add_com ("redirect", class_support, redirect_command, "Redirect the \
output of a single command to the redirect file.\n\n\
Usage:\n\tredirect\n\n\
[Also see 'set redirect']");

  set = add_set_cmd("redirect", class_support, var_string_noescape, (char *) \
                     &redirect_args,
     "Set redirection to file.\n\nUsage:\nTo set new value:\n\tset redirect on | off\
\nTo see current value:\n\tshow redirect\n\n\
[Also see 'set redirect-file' and 'set redirect-mode']",&setlist);
  set->function.sfunc = set_redirect_command;
  show =   add_show_from_set(set, &showlist);
  show->function.sfunc = show_redirect_command;


  set = add_set_cmd("redirect-mode", class_support, var_string_noescape, \
                    (char *) &redirect_mode_args, "Set redirection to file \
                    mode",&setlist);
  set->function.sfunc = set_redirect_mode_command;
  set->doc =
     "Set the mode of redirection to specified.\n\nUsage:\n\tSet redirect-mode \
overwrite | append\n\n[default : append]";

  set = add_set_cmd("redirect-file", class_support, var_string_noescape, \
  (char *) &redirect_file_args, "",&setlist);
  set->function.sfunc = set_redirect_file_command;
  set->doc =
     "Set redirection file to FILE.\n\nUsage:\n\tSet redirect-file <FILE>\n\n\
[default = .gdb_output]";

add_prefix_cmd ("logging", class_support, 
                (void (*)(char *,int))set_logging_command, "set logging options."
                "\n\nUsage:\n\tset logging on [<FILE>]\n\tset logging off\n", 
                &setlogginglist, "set logging ", 0, &setlist);
add_prefix_cmd ("logging", class_support, 
                (void (*)(char *,int))show_logging_command, "Show logging options."
                "\n\nUsage:\n\tshow logging\n", &showlogginglist, "show logging ",
                0, &showlist);

com = add_set_cmd ("file", no_class, var_filename, (char *) &redirect_file_args, 
                   "Set the current logfile.\n\nUsage:\nTo set new value:\n\tset"
                   " logging file <FILE>\nTo see current value:\n\tshow logging "
                   "file\n", &setlogginglist);
     
com->function.sfunc = set_redirect_file_command;
add_show_from_set(com,&showlogginglist);

com = add_set_cmd ("overwrite", no_class, var_string_noescape, 
                   (char *) &logging_overwrite_args, "Set whether logging "
                   "overwrites or appends to the log file.\n\nUsage:\nTo set new "
                   "value:\n\tset logging overwrite [on | off]\nTo see current "
                   "value:\n\tshow logging overwrite\n", &setlogginglist);
     
com->function.sfunc = (void(*)(char *,int,struct cmd_list_element *)) set_logging_overwrite;
add_show_from_set(com,&showlogginglist);

com = add_set_cmd ("redirect", no_class, var_string_noescape, 
                   (char *) &logging_redirect_args, 
                   "Set the logging output mode.\n\n\
Usage:\nTo set new value:\n\tset logging redirect [on | off]\n\
To see current value:\n\tshow logging redirect\n\n\
If redirect is off, output will go to both the screen and the log file\n\
If redirect is on, output will go only to the log file",
                    &setlogginglist);
     
com->function.sfunc = (void(*)(char *,int,struct cmd_list_element *)) set_logging_redirect;
add_show_from_set(com,&showlogginglist);

com = add_set_cmd ("on", no_class, var_string_noescape, (char *) &logging_on_args, "Set logging on\nEnables logging", &setlogginglist);
     
com->function.sfunc = (void(*)(char *,int,struct cmd_list_element *)) set_logging_on;

com = add_set_cmd ("off", no_class, var_string_noescape, (char *) &logging_off_args, "Set logging off\nDisables logging", &setlogginglist);
     
com->function.sfunc = (void(*)(char *,int,struct cmd_list_element *)) set_logging_off;


  add_show_from_set
    (add_set_cmd ("namespaces-enabled", class_support, var_boolean,
		  (char *) &namespace_enabled,
		  "Set enabling of namespace support. Default is on.\n\nUsage:\n"
                  "To set new value:\n\tset namespaces-enabled [on | off]\n"
                  "To see current value:\n\tshow namespaces-enabled\n",
		  &setlist),
     &showlist);

  add_show_from_set
    (add_set_cmd ("debug-dld", class_support, var_boolean,
		  (char *) &debugging_dld,
		  "Set enabling of debugging dld. Default is off.\n\n\
Usage:\nTo set new value:\n\tset debug-dld [on | off]\nTo see current value:\n\
\tshow debug-dld\n\n", &setlist), &showlist);
#ifdef HP_IA64
  add_show_from_set
    (add_set_cmd ("debug-dwutils-processed", class_support, var_boolean,
		  (char *) &debug_dwutils_processed,
		  "Set enabling of debugging dwutils processed binaries with cross\
 compilation unit references.\n\nUsage:\nTo set new value:\n\tset \
debug-dwutils-processed [on | off]\nTo see current value:\n\tshow \
debug-dwutils-processed\n\nDefault is off. This has to be set before issuing any \
command in the debugging session.", &setlist), &showlist);
#endif
}

/* Machine specific function to handle SIGWINCH signal. */

#ifdef  SIGWINCH_HANDLER_BODY
SIGWINCH_HANDLER_BODY
#endif

/* Support for converting target fp numbers into host DOUBLEST format.  */

/* XXX - This code should really be in libiberty/floatformat.c, however
   configuration issues with libiberty made this very difficult to do in the
   available time.  */

#include "floatformat.h"
#include <math.h>		/* ldexp */

/* The odds that CHAR_BIT will be anything but 8 are low enough that I'm not
   going to bother with trying to muck around with whether it is defined in
   a system header, what we do if not, etc.  */
#define FLOATFORMAT_CHAR_BIT 8

static unsigned long get_field (unsigned char *,
				enum floatformat_byteorders,
				unsigned int, unsigned int, unsigned int);

/* Extract a field which starts at START and is LEN bytes long.  DATA and
   TOTAL_LEN are the thing we are extracting it from, in byteorder ORDER.  */
static unsigned long
get_field (unsigned char *data,
     	   enum floatformat_byteorders order,
     	   unsigned int total_len,
     	   unsigned int start,
     	   unsigned int len)
{
  unsigned long result;
  unsigned int cur_byte;
  int cur_bitshift;

  /* Start at the least significant part of the field.  */
  if (order == floatformat_little || order == floatformat_littlebyte_bigword)
    {
      /* We start counting from the other end (i.e, from the high bytes
	 rather than the low bytes).  As such, we need to be concerned
	 with what happens if bit 0 doesn't start on a byte boundary. 
	 I.e, we need to properly handle the case where total_len is
	 not evenly divisible by 8.  So we compute ``excess'' which
	 represents the number of bits from the end of our starting
	 byte needed to get to bit 0. */
      int excess = FLOATFORMAT_CHAR_BIT - (total_len % FLOATFORMAT_CHAR_BIT);
      cur_byte = (total_len / FLOATFORMAT_CHAR_BIT) 
                 - ((start + len + excess) / FLOATFORMAT_CHAR_BIT);
      cur_bitshift = ((start + len + excess) % FLOATFORMAT_CHAR_BIT) 
                     - FLOATFORMAT_CHAR_BIT;
    }
  else
    {
      cur_byte = (start + len) / FLOATFORMAT_CHAR_BIT;
      cur_bitshift =
	((start + len) % FLOATFORMAT_CHAR_BIT) - FLOATFORMAT_CHAR_BIT;
    }
  if (cur_bitshift > -FLOATFORMAT_CHAR_BIT)
    result = *(data + cur_byte) >> (-cur_bitshift);
  else
    result = 0;
  cur_bitshift += FLOATFORMAT_CHAR_BIT;
  if (order == floatformat_little || order == floatformat_littlebyte_bigword)
    ++cur_byte;
  else
    --cur_byte;

  /* Move towards the most significant part of the field.  */
  while (cur_bitshift < len)
    {
      result |= (unsigned long)*(data + cur_byte) << cur_bitshift;
      cur_bitshift += FLOATFORMAT_CHAR_BIT;
      if (order == floatformat_little || order == floatformat_littlebyte_bigword)
	++cur_byte;
      else
	--cur_byte;
    }
  if (len < sizeof(result) * FLOATFORMAT_CHAR_BIT)
    /* Mask out bits which are not part of the field */
    result &= ((1UL << len) - 1);
  return result;
}

/* Checks whether the floating-point register has NaTVal 
   or not. Returns 1 if it has NaTVal else returns 0.
 */

int
check_natval (const struct floatformat *fmt,
     	      char *from)
{
  unsigned char *ufrom = (unsigned char *) from;
  long exponent;
  unsigned long mant = 0;
  unsigned int mant_bits, mant_off = fmt->man_start;
  int mant_bits_left = fmt->man_len;
  int sign;

  exponent = get_field (ufrom, fmt->byteorder, fmt->totalsize,
                        fmt->exp_start, fmt->exp_len);

  while (mant_bits_left > 0)
    {
      mant_bits = min (mant_bits_left, 32);

      mant = get_field (ufrom, fmt->byteorder, fmt->totalsize,
			mant_off, mant_bits);
      if (mant)
	break;
      mant_off += mant_bits;
      mant_bits_left -= mant_bits;
    }

  sign = get_field (ufrom, fmt->byteorder, fmt->totalsize,
                    fmt->sign_start, 1);

  return ((exponent == 0x1fffe && !mant && !sign) ? 1 : 0);

}

/* Convert from FMT to a DOUBLEST.
   FROM is the address of the extended float.
   Store the DOUBLEST in *TO.  */

void
floatformat_to_doublest (const struct floatformat *fmt,
     			 char *from,
     			 DOUBLEST *to)
{
  unsigned char *ufrom = (unsigned char *) from;
  DOUBLEST dto;
  long exponent;
  unsigned long mant;
  unsigned int mant_bits, mant_off;
  int mant_bits_left;
  int special_exponent;		/* It's a NaN, denorm or zero */

  /* If the mantissa bits are not contiguous from one end of the
     mantissa to the other, we need to make a private copy of the
     source bytes that is in the right order since the unpacking
     algorithm assumes that the bits are contiguous.

     Swap the bytes individually rather than accessing them through
     "long *" since we have no guarantee that they start on a long
     alignment, and also sizeof(long) for the host could be different
     than sizeof(long) for the target.  FIXME: Assumes sizeof(long)
     for the target is 4. */

  if (fmt->byteorder == floatformat_littlebyte_bigword)
    {
      static unsigned char *newfrom;
      unsigned char *swapin, *swapout;
      int longswaps;

      longswaps = fmt->totalsize / FLOATFORMAT_CHAR_BIT;
      longswaps >>= 3;

      if (newfrom == NULL)
	{
	  newfrom = (unsigned char *) xmalloc (fmt->totalsize);
	}
      swapout = newfrom;
      swapin = ufrom;
      ufrom = newfrom;
      while (longswaps-- > 0)
	{
	  /* This is ugly, but efficient */
	  *swapout++ = swapin[4];
	  *swapout++ = swapin[5];
	  *swapout++ = swapin[6];
	  *swapout++ = swapin[7];
	  *swapout++ = swapin[0];
	  *swapout++ = swapin[1];
	  *swapout++ = swapin[2];
	  *swapout++ = swapin[3];
	  swapin += 8;
	}
    }

  exponent = get_field (ufrom, fmt->byteorder, fmt->totalsize,
			fmt->exp_start, fmt->exp_len);
  /* Note that if exponent indicates a NaN, we can't really do anything useful
     (not knowing if the host has NaN's, or how to build one).  So it will
     end up as an infinity or something close; that is OK.  */

  mant_bits_left = fmt->man_len;
  mant_off = fmt->man_start;
  dto = 0.0;

  special_exponent = exponent == 0 || exponent == fmt->exp_nan;

  /* Fix for JAGae72875.  IA64 register format has a 17-bit exponent all of
     which are 1 for a NaN.  Using ldexp() to algebraically recreate the fp
     number (NaN in this case) with such a large exponent produces infinity
     and not NaN.  So create the NaN separately. */
  if ((unsigned long) exponent == fmt->exp_nan && fmt->exp_len == 17)
    {
      unsigned long long *d_p = (unsigned long long*) &dto;

      memcpy (((char*)&dto)+8, from + 4, 8); /* Copy significand/mantissa. */

      *d_p &= ~(0xffffull << 48); /* Clear the exponent bits. */
      *d_p |= 0x7fffull << 48; /* Copy the NAN exponent. */
      *d_p |= ((long long) /* Copy the sign bit of fp #. */
               (((from[1] >> 1) & 0x1)) << 63);

      *to = dto;
      return;
    }

  /* If 17 bit exponent, issue a warning if it reaches an out-of-bounds
     value for a DOUBLEST which has only 15 bit exponent */
  if (   !special_exponent && fmt->exp_len == 17
      && (exponent < 0xC001 || exponent > 0x13FFE)
      && exponent != 0x1fffe )
    {
      printf (" warning: Exponent out-of-range in conversion to long double\n");
    }

  /* Don't bias NaNs. Use minimum exponent for denorms. For simplicity,
   we don't check for zero as the exponent doesn't matter. */
  if (!special_exponent)
    exponent -= fmt->exp_bias;
  else if (exponent == 0)
    {
      /* In case of hpintel format when exponent is == 0, it still 
         represents a float80 subnormal, so we need to use float80
         exponent bias. */
      if (fmt->exp_bias == 0xffff)
          /* hpintel de-norm case: need to adjust for explicit integer 
             before the decimal point */
        exponent = 2L - (long) 0x3fff;
      else
          /* Regular IEEE de-norm case: */
        exponent = 1L - (long) fmt->exp_bias;
    }

  /* Build the result algebraically.  Might go infinite, underflow, etc;
     who cares. */

/* If this format uses a hidden bit, explicitly add it in now.  Otherwise,
   increment the exponent by one to account for the integer bit.  */

  if (!special_exponent)
    {
      if (fmt->intbit == floatformat_intbit_no)
#ifdef HP_IA64
	dto = (DOUBLEST) ldexpl (1.0, (int) exponent);
#else
	dto = (DOUBLEST) ldexp (1.0, (int) exponent);
#endif
      else
          /* De-norms for hpintel are handled above */
	exponent++;
    }

  while (mant_bits_left > 0)
    {
      mant_bits = min (mant_bits_left, 32);

      mant = get_field (ufrom, fmt->byteorder, fmt->totalsize,
			mant_off, mant_bits);

#ifdef HP_IA64
      dto += (DOUBLEST) ldexpl ((DOUBLEST) mant, (int)(exponent - mant_bits));
#else
      dto += (DOUBLEST) ldexp ((DOUBLEST) mant, (int)(exponent - mant_bits));
#endif
      exponent -= mant_bits;
      mant_off += mant_bits;
      mant_bits_left -= mant_bits;
    }

  /* Negate it if negative.  */
  if (get_field (ufrom, fmt->byteorder, fmt->totalsize, fmt->sign_start, 1))
    dto = -dto;
  *to = dto;
}

static void put_field (unsigned char *, enum floatformat_byteorders,
		       unsigned int,
		       unsigned int, unsigned int, unsigned long);

/* Set a field which starts at START and is LEN bytes long.  DATA and
   TOTAL_LEN are the thing we are extracting it from, in byteorder ORDER.  */
static void
put_field ( unsigned char *data,
     	    enum floatformat_byteorders order,
     	    unsigned int total_len,
     	    unsigned int start,
     	    unsigned int len,
     	    unsigned long stuff_to_put)
{
  unsigned int cur_byte;
  int cur_bitshift;

  /* Start at the least significant part of the field.  */
  if (order == floatformat_little || order == floatformat_littlebyte_bigword)
    {
      int excess = FLOATFORMAT_CHAR_BIT - (total_len % FLOATFORMAT_CHAR_BIT);
      cur_byte = (total_len / FLOATFORMAT_CHAR_BIT) 
                 - ((start + len + excess) / FLOATFORMAT_CHAR_BIT);
      cur_bitshift = ((start + len + excess) % FLOATFORMAT_CHAR_BIT) 
                     - FLOATFORMAT_CHAR_BIT;
    }
  else
    {
      cur_byte = (start + len) / FLOATFORMAT_CHAR_BIT;
      cur_bitshift =
	((start + len) % FLOATFORMAT_CHAR_BIT) - FLOATFORMAT_CHAR_BIT;
    }
  if (cur_bitshift > -FLOATFORMAT_CHAR_BIT)
    {
      *(data + cur_byte) &=
	~(((1 << ((start + len) % FLOATFORMAT_CHAR_BIT)) - 1)
	  << (-cur_bitshift));
      *(data + cur_byte) |=
	(stuff_to_put & ((1 << FLOATFORMAT_CHAR_BIT) - 1)) << (-cur_bitshift);
    }
  cur_bitshift += FLOATFORMAT_CHAR_BIT;
  if (order == floatformat_little || order == floatformat_littlebyte_bigword)
    ++cur_byte;
  else
    --cur_byte;

  /* Move towards the most significant part of the field.  */
  while (cur_bitshift < len)
    {
      if (len - cur_bitshift < FLOATFORMAT_CHAR_BIT)
	{
	  /* This is the last byte.  */
	  *(data + cur_byte) &=
	    ~((1 << (len - cur_bitshift)) - 1);
	  *(data + cur_byte) |= (stuff_to_put >> cur_bitshift);
	}
      else
	*(data + cur_byte) = ((stuff_to_put >> cur_bitshift)
			      & ((1 << FLOATFORMAT_CHAR_BIT) - 1));
      cur_bitshift += FLOATFORMAT_CHAR_BIT;
      if (order == floatformat_little || order == floatformat_littlebyte_bigword)
	++cur_byte;
      else
	--cur_byte;
    }
}

#ifdef HAVE_LONG_DOUBLE
/* Return the fractional part of VALUE, and put the exponent of VALUE in *EPTR.
   The range of the returned value is >= 0.5 and < 1.0.  This is equivalent to
   frexp, but operates on the long double data type.  */

static long double ldfrexp (long double value, int *eptr);

static long double
ldfrexp (long double value, int *eptr)
{
  long double tmp;
  int exp;

  /* Unfortunately, there are no portable functions for extracting the exponent
     of a long double, so we have to do it iteratively by multiplying or dividing
     by two until the fraction is between 0.5 and 1.0.  */

  if (value < 0.0l)
    value = -value;

  tmp = 1.0l;
  exp = 0;

  if (value >= tmp)		/* Value >= 1.0 */
    while (value >= tmp)
      {
	tmp *= 2.0l;
	exp++;
      }
  else if (value != 0.0l)	/* Value < 1.0  and > 0.0 */
    {
      while (value < tmp)
	{
	  tmp /= 2.0l;
	  exp--;
	}
      tmp *= 2.0l;
      exp++;
    }

  *eptr = exp;
  return value / tmp;
}
#endif /* HAVE_LONG_DOUBLE */


/* The converse: convert the DOUBLEST *FROM to an extended float
   and store where TO points.  Neither FROM nor TO have any alignment
   restrictions.  */

void
floatformat_from_doublest (CONST struct floatformat *fmt, DOUBLEST *from, char *to)
{
  DOUBLEST dfrom;
  int exponent;
  DOUBLEST mant;
  unsigned int mant_bits, mant_off;
  int mant_bits_left;
  unsigned char *uto = (unsigned char *) to;

  memcpy (&dfrom, from, sizeof (dfrom));
  memset (uto, 0, (fmt->totalsize + FLOATFORMAT_CHAR_BIT - 1) 
                    / FLOATFORMAT_CHAR_BIT);
  if (dfrom == 0)
    return;			/* Result is zero */
  if (dfrom != dfrom)		/* Result is NaN */
    {
      /* From is NaN */
      put_field (uto, fmt->byteorder, fmt->totalsize, fmt->exp_start,
		 fmt->exp_len, fmt->exp_nan);
      /* Be sure it's not infinity, but NaN value is irrel */
      put_field (uto, fmt->byteorder, fmt->totalsize, fmt->man_start,
		 32, 1);
      return;
    }

  /* If negative, set the sign bit.  */
  if (dfrom < 0)
    {
      put_field (uto, fmt->byteorder, fmt->totalsize, fmt->sign_start, 1, 1);
      dfrom = -dfrom;
    }

  if (dfrom + dfrom == dfrom && dfrom != 0.0)	/* Result is Infinity */
    {
      /* Infinity exponent is same as NaN's.  */
      put_field (uto, fmt->byteorder, fmt->totalsize, fmt->exp_start,
		 fmt->exp_len, fmt->exp_nan);
      /* Infinity mantissa is all zeroes.  */
      put_field (uto, fmt->byteorder, fmt->totalsize, fmt->man_start,
		 fmt->man_len, 0);
      return;
    }

#ifdef HAVE_LONG_DOUBLE
  mant = ldfrexp (dfrom, &exponent);
#else
  mant = frexp (dfrom, &exponent);
#endif

  put_field (uto, fmt->byteorder, fmt->totalsize, fmt->exp_start, fmt->exp_len,
	     exponent + fmt->exp_bias - 1);

  mant_bits_left = fmt->man_len;
  mant_off = fmt->man_start;
  while (mant_bits_left > 0)
    {
      unsigned long mant_long;
      mant_bits = mant_bits_left < 32 ? mant_bits_left : 32;

      mant *= 4294967296.0;
      mant_long = ((unsigned long) mant) & 0xffffffffL;
      mant -= mant_long;

      /* If the integer bit is implicit, then we need to discard it.
         If we are discarding a zero, we should be (but are not) creating
         a denormalized number which means adjusting the exponent
         (I think).  */
      if (mant_bits_left == fmt->man_len
	  && fmt->intbit == floatformat_intbit_no)
	{
	  mant_long <<= 1;
	  mant_long &= 0xffffffffL;
	  mant_bits -= 1;
	}

      if (mant_bits < 32)
	{
	  /* The bits we want are in the most significant MANT_BITS bits of
	     mant_long.  Move them to the least significant.  */
	  mant_long >>= 32 - mant_bits;
	}

      put_field (uto, fmt->byteorder, fmt->totalsize,
		 mant_off, mant_bits, mant_long);
      mant_off += mant_bits;
      mant_bits_left -= mant_bits;
    }
  if (fmt->byteorder == floatformat_littlebyte_bigword)
    {
      int count;
      unsigned char *swaplow = uto;
      unsigned char *swaphigh = uto + 4;
      unsigned char tmp;

      for (count = 0; count < 4; count++)
	{
	  tmp = *swaplow;
	  *swaplow++ = *swaphigh;
	  *swaphigh++ = tmp;
	}
    }
}

/* print routines to handle variable size regs, etc. */

/* temporary storage using circular buffer */
#define NUMCELLS 16
#define CELLSIZE 32
static char *
get_cell (void)
{
  static char buf[NUMCELLS][CELLSIZE];
  static int cell = 0;
  if (++cell >= NUMCELLS)
    cell = 0;
  return buf[cell];
}

int
strlen_paddr (void)
{
  return (TARGET_PTR_BIT / 8 * 2);
}

char *
paddr (CORE_ADDR addr)
{
  return phex (addr, TARGET_PTR_BIT / 8);
}

char *
paddr_nz (CORE_ADDR addr)
{
  return phex_nz (addr, TARGET_PTR_BIT / 8);
}

static void
decimal2str (char *paddr_str, char *sign, ULONGEST addr)
{
  /* steal code from valprint.c:print_decimal().  Should this worry
     about the real size of addr as the above does? */
  unsigned long temp[3];
  int i = 0;
  do
    {
      temp[i] = addr % (1000 * 1000 * 1000);
      addr /= (1000 * 1000 * 1000);
      i++;
    }
  while (addr != 0 && i < (sizeof (temp) / sizeof (temp[0])));
  switch (i)
    {
    case 1:
      sprintf (paddr_str, "%s%lu",
	       sign, temp[0]);
      break;
    case 2:
      sprintf (paddr_str, "%s%lu%09lu",
	       sign, temp[1], temp[0]);
      break;
    case 3:
      sprintf (paddr_str, "%s%lu%09lu%09lu",
	       sign, temp[2], temp[1], temp[0]);
      break;
    default:
      abort ();
    }
}

char *
paddr_u (CORE_ADDR addr)
{
  char *paddr_str = get_cell ();
  decimal2str (paddr_str, "", addr);
  return paddr_str;
}

char *
paddr_d (LONGEST addr)
{
  char *paddr_str = get_cell ();
  if (addr < 0)
    decimal2str (paddr_str, "-", -addr);
  else
    decimal2str (paddr_str, "", addr);
  return paddr_str;
}

/* eliminate warning from compiler on 32-bit systems */
static int thirty_two = 32;

char *
phex (ULONGEST l, int sizeof_l)
{
  char *str = get_cell ();
  switch (sizeof_l)
    {
    case 8:
      sprintf (str, "%08lx%08lx",
	       (unsigned long) (l >> thirty_two),
	       (unsigned long) (l & 0xffffffff));
      break;
    case 4:
      sprintf (str, "%08lx", (unsigned long) l);
      break;
    case 2:
      sprintf (str, "%04x", (unsigned short) (l & 0xffff));
      break;
    default:
      phex (l, sizeof (l));
      break;
    }
  return str;
}

char *
phex_nz (ULONGEST l, int sizeof_l)
{
  char *str = get_cell ();
  switch (sizeof_l)
    {
    case 8:
      {
	unsigned long high = (unsigned long) (l >> thirty_two);
	if (high == 0)
	  sprintf (str, "%lx", (unsigned long) (l & 0xffffffff));
	else
	  sprintf (str, "%lx%08lx",
		   high, (unsigned long) (l & 0xffffffff));
	break;
      }
    case 4:
      sprintf (str, "%lx", (unsigned long) l);
      break;
    case 2:
      sprintf (str, "%x", (unsigned short) (l & 0xffff));
      break;
    default:
      phex_nz (l, sizeof (l));
      break;
    }
  return str;
}


/* Convert to / from the hosts pointer to GDB's internal CORE_ADDR
   using the target's conversion routines. */
CORE_ADDR
host_pointer_to_address (void *ptr)
{
  if (sizeof (ptr) != TYPE_LENGTH (builtin_type_ptr))
    internal_error ("core_addr_to_void_ptr: bad cast");
  return POINTER_TO_ADDRESS (builtin_type_ptr, &ptr);
}

void *
address_to_host_pointer (CORE_ADDR addr)
{
  void *ptr;
  if (sizeof (ptr) != TYPE_LENGTH (builtin_type_ptr))
    internal_error ("core_addr_to_void_ptr: bad cast");
  ADDRESS_TO_POINTER (builtin_type_ptr, &ptr, addr);
  return ptr;
}

#ifdef MULTI_BYTE_EMBEDDED_ASCII

/**************************************************************************
  nls_initialize

  Description:
    1. Initialize gdb's locale with a call to setlocale.
    2. Verify the current locale is valid and issue and appropriate
       message in the event of a problem.

    3. Eventually we should open the message catalog here.
 */

void 
nls_initialize (void)
{

  /*  Various initialization routines might call nls_initialize to be sure that
      setlocale() has been called.  We only want to do the initialization once.
   */
  static boolean nls_initialize_called = false;

  /* We only want to do these initializations once. */

  if (nls_initialize_called) 
    return;
  nls_initialize_called = true;

  if (!setlocale(LC_ALL, "")) {
    /* Bad locale, output HP standard "unavailable language warning" */
    fprintf_unfiltered (gdb_stderr, _errlocale ("gdb"), stderr);
  }
}
#endif /* end ifdef MULTI_BYTE_EMBEDDED_ASCII */

/* _initialize_utils

  Initializations 

    sevenbit_strings    Set to 1 for "C" locale so that non-ASCII or
                        non-printable characters will be printed in
                        octal.  For internationalization locales, it is
                        set to 0 so that non-ASCII characters are printed
                        raw in the hopes that the terminal can display them.

                        The "POSIX" and "" locals are equivalent to "C".
                        The variables LC_ALL, LC_CTYPE, and LANG are
                        checked in order for a setting.  See environ(5).

    _rl_convert_meta_chars_to_ascii  Set to "1" for "C" locale so that
                        non-ASCII input characters are translated to ASCII.
                        For internationalization locales, it is is set
                        to 0 so that non-ASCII characters can be entered.
*/

void
_initialize_utils (void)
{
  static boolean initialize_utils_called = false;

  char*         env_vars[] = {"LC_ALL", "LC_CTYPE", "LANG"};
  int           env_vars_count = 3;
  int           env_var_idx;
  char*         env_var_value;
#ifdef MULTI_BYTE_EMBEDDED_ASCII
  struct locale_data*   locale_info_p;
  char*                 locale_name;
#endif

  if (initialize_utils_called)
    return;
  initialize_utils_called = true;


#ifndef MULTI_BYTE_EMBEDDED_ASCII

  locale_is_c = true;
  for (env_var_idx = 0;  env_var_idx < env_vars_count; env_var_idx++) 
  {
    env_var_value = getenv (env_vars[env_var_idx]);
    if (env_var_value != NULL && strlen(env_var_value) != 0)
    {
      if (   strcmp (env_var_value, "C"    ) != 0
          && strcmp (env_var_value, "POSIX") != 0)
        {
          locale_is_c = false;
        }
      break; /* We found the contolling environment variable. */
    }
  }
#else
  nls_initialize ();
  locale_info_p = getlocale(LOCALE_STATUS);
  locale_name = strdup(locale_info_p->LC_CTYPE_D);
  strtok(locale_name, "_.@");
  locale_is_c = false;
  if (   strcmp (locale_name, "C") == 0
      || strcmp (locale_name, "POSIX") == 0
      || strcmp (locale_name, "") == 0
     )
  {
    locale_is_c = true;
  }
  free(locale_name);
#endif

  if (locale_is_c) 
  {
    sevenbit_strings = 1; 
    _rl_convert_meta_chars_to_ascii = 1;
  }
  else
  {
    sevenbit_strings = 0;
    _rl_convert_meta_chars_to_ascii = 0;
  }

} /* end _initialize_utils */

static void
set_logging_command (void)
{
  printf_unfiltered ("\"set logging\" lets you log output to a file.\n");
  printf_unfiltered ("Usage: set logging on [<FILE>]\n");
  printf_unfiltered ("       set logging off\n");
  printf_unfiltered ("       set logging file <FILE>\n");
  printf_unfiltered ("       set logging overwrite [on | off]\n");
  printf_unfiltered ("       set logging redirect [on | off]\n");
}

static void
show_logging_command (void)
{
  printf_unfiltered ("Logging settings are :-\n");
  if (redirection_on)
    {
      printf_unfiltered ("Currently logging : on\n");
      printf_unfiltered ("Log file\t  : %s\n", saved_filename);
    }
  else
    {
      printf_unfiltered ("Currently logging : off\n");
      printf_unfiltered ("Log file\t  : %s\n", redirect_file);
    }

  if (logging_overwrite)
    printf_unfiltered ("Mode\t\t  : overwrite\n");
  else
    printf_unfiltered ("Mode\t\t  : append\n");

  if (logging_redirect)
    printf_unfiltered ("Output only to the log file.\n");
  else
    printf_unfiltered ("Output to both log file and display.\n");
}

static void
set_logging_on (void)
{
  if(logging_on_args && *logging_on_args)
    redirect_file = logging_on_args;

  redirect_args = xstrdup ("on");
  set_redirect_command("on",1,NULL);
}

static void 
set_logging_off (void)
{
  redirect_args = xstrdup ("off");
  set_redirect_command("off",1,NULL);
}

static void
set_logging_overwrite(void)
{
  
	char *arg;

  arg = logging_overwrite_args;

  if (arg == 0)
   error ("Incomplete command : try `help set redirect-mode'");

  while (*arg == ' ' || *arg == '\t')
    arg++;

  if (!strcmp(arg,"on")){
    logging_overwrite = 1;
    redirect_mode_args = xstrdup ("overwrite");
    set_redirect_mode_command("overwrite",1,NULL);
  }
  else if (!strcmp(arg,"off")){
    logging_overwrite = 0;
    redirect_mode_args = xstrdup ("append");
    set_redirect_mode_command("append",1,NULL);
  }
  /* QXCR1000901008: "set logging overwrite" command Dumps core. Following code
     is added to handle the case when neither of "on" nor "off" is passed with
     "set logging overwrite" command. */
  else if (*arg == '\0'){
    logging_overwrite_args = xstrdup ("on");
    logging_overwrite = 1;
    redirect_mode_args = xstrdup ("overwrite");
    set_redirect_mode_command("overwrite",1,NULL);
  }

}

static void
set_logging_redirect(void)
{
  
	char *arg;
  arg = logging_redirect_args;

  if (arg == 0)
   error ("Incomplete command : try `help set redirect-mode'");

  while (*arg == ' ' || *arg == '\t')
    arg++;

  if (!strcmp(arg,"on")){
			logging_redirect = 1;
  }
  else if (!strcmp(arg,"off")){
			logging_redirect = 0;
  }
  /* Following code is added to handle the case when neither of "on" nor "off"
     is passed with "set logging redirect" command. */
  else if (*arg == '\0'){
			logging_redirect = 1;
                        logging_redirect_args = xstrdup ("on"); 
  }

}

/* Madhavi, turns on redirection to file in default mode. ".gdb_output"
 * is the default filename and it is opened in append mode */
static void
set_redirect_command (char *arg, int from_tty, struct cmd_list_element *c)
{
  arg = redirect_args;
  char *ptr  = NULL;

  /* QXCR1000793261: WDB gui hangs on bad user-defined cmd
     Check if we invoke 'set redirect/logging command' through WDB-GUI*/
  if (annotation_level == 2)
    {
      error ("Setting redirect/logging command is not supported under WDB-GUI.\n"
             "Ignoring 'set redirect/logging on' command ...");
    }

  if (arg == 0)
   error ("Incomplete command : try `help set redirect'");

  while (*arg == ' ' || *arg == '\t')
    arg++;
  
  /* Strip off any white spaces at the end of the string.
   */
  ptr = arg;
  while (*ptr != NULL)
  {
    if ( *ptr == ' ' || *ptr == '\t')
    {
      *ptr = 0;
      break;
    }  
    ptr++;
  }
  /* arg is simply "off", then first check if redirect was previously
   * turned on, if not print error mesg. If it was previously turned
   * on, then close any open files and set all the options to default
   * options. */
  if (!strcmp(arg,"off"))
    {
      if (!redirection_on)
      {
        error ("Redirection not previously turned on");
        return;
      }

      ui_file_delete(gdb_stdout);

#if defined (TUI) || defined (GDBTK)
      gdb_stdout = tui_fileopen (stdout);
#else
      gdb_stdout = stdio_fileopen (stdout);
#endif
#ifdef UI_OUT
	ui_out_redirect (uiout, NULL);
	ui_out_set_stream (uiout,gdb_stdout);
#endif      
      redirection_on = FALSE;
    }

  /* If arg is simply "on", then open the default output file in the default
   * mode and change the gdb->stdout to this new file. Set the redirection flag
   * to true. */
  else if (!strcmp(arg,"on"))
         {
           if (redirection_on)
              return; /* Redirection already turned on */

           if (redirect_mode)
             {
               nonstdout = gdb_fopen(redirect_file, "a");
               if (nonstdout == 0)
                 perror_with_name (redirect_file);

             }
           else
             {
               nonstdout = gdb_fopen(redirect_file, "w");
               if (nonstdout == 0)
                 perror_with_name (redirect_file);
             }
	   if (!logging_redirect)
    	     {
      	       nonstdout = tee_file_new (gdb_stdout, 0, nonstdout, 1);
      	       if (nonstdout == NULL)
	         perror_with_name ("set logging");
      	       if (from_tty)
		 fprintf_unfiltered (gdb_stdout, "Copying output to %s.\n",
			    	    redirect_file);
    	      }
  	    else if (   from_tty
                     && !(batch_rtc && trace_threads_in_this_run))
    	       fprintf_unfiltered (gdb_stdout, "Redirecting output to %s.\n",
                                   redirect_file);

	  /* baskar, save gdb_stdout so that it can be used in query() */
	  gdb_stdout_saved = gdb_stdout;
          saved_filename = xstrdup (redirect_file);

#if defined (TUI) || defined (GDBTK)
          gdb_stdout = nonstdout;
#else
          gdb_stdout = nonstdout;
#endif
#ifdef UI_OUT
  if (ui_out_redirect (uiout, gdb_stdout) < 0)
    warning ("Current output protocol does not support redirection");
#endif
           redirection_on = TRUE;
         }
  else
    error ("Incomplete command : try `help set redirect'");
}

/* Madhavi, sets the mode of the redirect file */
static void
set_redirect_mode_command (char *arg, int from_tty, struct cmd_list_element *c)
{
  arg = redirect_mode_args;

  if (arg == 0)
   error ("Incomplete command : try `help set redirect-mode'");

  while (*arg == ' ' || *arg == '\t')
    arg++;

 if (!strncmp(arg, "append", 6))
   {
     redirect_mode = 1; /* non-zero is append */
   }

   else if (!strncmp(arg, "overwrite", 9))
       {
         redirect_mode = 0; /* zero is overwrite */
       }
     else
      {
        error ("Incomplete command : try `help set redirect-mode'");
      }
}

/* Madhavi, sets the redirect file to the one specified. Opens the file 
 * in requested mode if specified earlier, else opens in default mode, which 
 * is "append" */
static void
set_redirect_file_command (char *arg, int from_tty, struct cmd_list_element *c)
{
  arg = redirect_file_args;

  if (arg == 0)
   {
     error ("Incomplete command : try `help set redirect-file'");
     return;
   }
  if ((int) strlen(arg) <= 0)
   {
     error ("Incomplete command : try `help set redirect-file'");
     return;
   }

  redirect_file = arg; /* set redirect_file to new file */
}

static void
show_redirect_command (char *args, int from_tty, struct cmd_list_element *c)
{
  printf_filtered ("Redirection settings are :-\n");
  printf_filtered ("\tredirection : %s\n",redirection_on ? "on" : "off");
  printf_filtered ("\tfile        : %s\n",redirect_file);
  printf_filtered ("\tmode        : %s\n",redirect_mode ? "append" : "overwrite");
}

/* Madhavi, redirecting the output of a single command. Open the
 * redirection file, execute the command, any output gets
 * written into it, finally close the file. */
static void
redirect_command(char *args, int from_tty)
{
   if (redirection_on)
     {
       printf_filtered ("%s\n",args); /* prints command also to file */
       execute_command(args,from_tty); /* Redirection already turned on, just
                                 * execute command and get out. */
       return;
    }

   if (redirect_mode)
     {
       nonstdout = gdb_fopen(redirect_file, "a");
       if (nonstdout == 0)
         perror_with_name (redirect_file);
     }
   else
     {
       nonstdout = gdb_fopen(redirect_file, "w");
       if (nonstdout == 0)
         perror_with_name (redirect_file);
     }

#if defined (TUI) || defined (GDBTK)
   gdb_stdout = nonstdout;
#else
  gdb_stdout = nonstdout;
#endif
#ifdef UI_OUT
  ui_out_redirect (uiout,gdb_stdout);
#endif

   redirection_on = TRUE;

   printf_filtered ("%s\n",args); /* prints command also to file */
   execute_command(args,from_tty);

   ui_file_delete(gdb_stdout);

#if defined (TUI) || defined (GDBTK)
   gdb_stdout = tui_fileopen (stdout);
#else
  gdb_stdout = stdio_fileopen (stdout);
#endif
#ifdef UI_OUT
  ui_out_set_stream (uiout,gdb_stdout);
#endif

   redirection_on = FALSE;
}


/* jini: Redirect the backtrace output to the thread log file. Used to give
   more insight into the point of error event occurrence in the batch mode
   thread check case. */
void
batch_thread_redirect_backtrace (char *filename)
{
   static int modified_redirect_file = 0;
   char temp_frame_count[30];

   sprintf (temp_frame_count, "%d", frame_count);
   if (!modified_redirect_file)
     {
       if (redirect_file_args)
         free (redirect_file_args);

       redirect_file_args = xstrdup (filename);

       if (redirect_file)
         free (redirect_file);

       redirect_file = xstrdup (filename);
       modified_redirect_file = 1;
     }

   catch_command_errors (
                (catch_command_errors_ftype *)set_redirect_file_command,
                filename,
                1, RETURN_MASK_ALL);
   set_logging_on ();
   backtrace_command_1 (temp_frame_count, 0, 1);
   gdb_flush (gdb_stdout);

   /* Close the file too. */
   set_logging_off ();
}


unsigned int
get_width (void)
{
  return chars_per_line;
}

/* Returns true if the input string does not look like a number.
   This is used to determine whether the output of `info heap'
   and `info leaks' should be directed to a file. In the command
   info [heap|leaks] 123, the string 123 should be treated as a 
   heap-block|leak number. In the command `info [heap|leaks] junk',
   the output should be sent to file junk. 
*/

int
could_be_filename (char * string)
{
  int retval = 0;
  while (*string && isdigit (*string))
    string++;
  if (*string)
    return 1;
  else 
    return 0;
}


/* 
 * retuns true if the input string looks like a number
 */ 
int 
could_be_number (char * string)
{
  int retval = 0;

  /* accommodate negative number */
  if (string[0] == '-')
    string++;

  while (*string && isdigit (*string))
    string++;
  if (*string)
    return 0;
  else 
    return 1;
}


/*
 * convert entire string to lower case to not make 
 * info heap arena commands case sensitive
 */

char * 
convert_lower(char * oldstring)
{
   char * newstring;
   int i;
   
   newstring= (char *) calloc(strlen(oldstring)+1, sizeof(char));
   for (i=0; i<strlen(oldstring); i++)
      newstring[i]=tolower(oldstring[i]); 

   newstring[i]='\0';
   return newstring;

}

/* This is the 32-bit CRC function used by the GNU separate debug
   facility.  An executable may contain a section named
   .gnu_debuglink, which holds the name of a separate executable file
   containing its debug info, and a checksum of that file's contents,
   computed using this function.  */
unsigned long
gnu_debuglink_crc32 (unsigned long crc, unsigned char *buf, size_t len)
{
  static const unsigned long crc32_table[256] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419,
    0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4,
    0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07,
    0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
    0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856,
    0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
    0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4,
    0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
    0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a,
    0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599,
    0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190,
    0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f,
    0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e,
    0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed,
    0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3,
    0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
    0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a,
    0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5,
    0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010,
    0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17,
    0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6,
    0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
    0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
    0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1, 0xf00f9344,
    0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
    0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a,
    0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1,
    0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c,
    0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef,
    0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe,
    0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31,
    0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c,
    0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b,
    0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
    0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1,
    0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
    0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
    0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7,
    0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66,
    0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605,
    0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8,
    0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b,
    0x2d02ef8d
  };
  unsigned char *end;

  crc = ~crc & 0xffffffff;
  for (end = buf + len; buf < end; ++buf)
    crc = crc32_table[(crc ^ *buf) & 0xff] ^ (crc >> 8);
  return ~crc & 0xffffffff;;
}

