/* Perform non-arithmetic operations on values, for GDB. 
   Copyright 1986, 1987, 1989, 1991, 1992, 1993, 1994, 1995, 1996, 1997
   Copyright 1986, 87, 89, 91, 92, 93, 94, 95, 96, 97, 1998
   Free Software Foundation, Inc.

   This file is part of GDB.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

#include <assert.h>
#include <errno.h>

#include "defs.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "value.h"
#include "frame.h"
#include "inferior.h"
#include "gdbcore.h"
#include "target.h"
#include "demangle.h"
#include "language.h"
#include "gdbcmd.h"
#include "top.h"

#include "gdb_string.h"
#include "gdbarch.h"

#ifndef HP_COMPILED_TARGET
# define HP_COMPILED_TARGET -1
#endif

/* Flag indicating HP compilers were used; needed to correctly handle some
   value operations with HP aCC code/runtime. */
extern int hp_som_som_object_present;
extern int hp_dwarf2_object_present;

/* from ia64-tdep.c or hppa-tdep.c */
extern void push_dummy_frame (struct inferior_status *);
extern value_ptr ia64_value_returned_from_stack (struct type *, CORE_ADDR);

/* Starting address of __wdb_call_dummy. */
CORE_ADDR wdb_call_dummy_start_addr = (CORE_ADDR) (long) -1;

/* The address of first available slot in __wdb_call_dummy. */
CORE_ADDR wdb_call_dummy_avail_addr = (CORE_ADDR) (long) -1;
extern CORE_ADDR
hppa_push_arguments (int, value_ptr *, CORE_ADDR, int, CORE_ADDR);
extern int overload_debug;
/* used to note the begin and end of call_dummy on stack*/
CORE_ADDR call_dummy_begin = 0;
CORE_ADDR call_dummy_end = 0;


#ifdef HASH_ALL
struct symtab *
find_symtab (struct minimal_symbol *);
#endif

/* JAGae23887 - Define an arbitrary number where we think the offset into
   the vtable is too large.  This indicates that the file being debugged
   was created by an older compiler that used the old values for
   DW_VIRTUALITY.  Thus an attempt is being made to read the vtable for a 
   class that is not a virtual class. */
#define LARGE_VTABLE_OFFSET 1000000
#ifdef HP_IA64
/* Below variables are used to trap CLC failures and print return value 
   of the CLC completed */
int internal_CLC;
int cmd_line_call_failed_cnt;
CORE_ADDR dummy_break_addr;
/* chain of all failed CLC */
struct command_line_call *trap_CLC;
#endif
int cmd_line_call_cnt;
/* Local functions.  */

enum looking_for_baseclass_type
{
  no_baseclass,
  only_baseclass,
  struct_or_baseclass
};

static int typecmp (int staticp, struct type *t1[], value_ptr t2[]);

static CORE_ADDR find_function_addr (value_ptr, struct type **);
static value_ptr value_arg_coerce (value_ptr, struct type *, int);


static CORE_ADDR value_push (CORE_ADDR, value_ptr);

static value_ptr
search_struct_field (char *, value_ptr, int, struct type *,
		     enum looking_for_baseclass_type, char *);

static value_ptr
search_struct_field_aux (char *, value_ptr, int, struct type *,
			 enum looking_for_baseclass_type,
			 int *, char **, struct type **, char *);

static value_ptr search_struct_method (char *, value_ptr *,
				       value_ptr *,
				       int, int *, struct type *);

static int check_field_in (struct type *, const char *);

static CORE_ADDR allocate_space_in_inferior (int);

static value_ptr cast_into_complex (struct type *, value_ptr);

static struct fn_field *find_method_list (value_ptr * argp, char *method,
					  int offset, int *static_memfuncp,
					  struct type *type, int *num_fns,
					  struct type **basetype,
					  int *boffset);

void _initialize_valops (void);

static value_ptr cast_from_complex PARAMS ((struct type *, value_ptr));

#ifdef HP_IA64
static value_ptr cast_from_decfloat (struct type *, value_ptr);
#endif
#define VALUE_SUBSTRING_START(VAL) VALUE_FRAME(VAL)

#ifdef HP_IA64

/* Macros to recognize the Itanium C++ ABI convention for passing return
   aggregates from member functions by reference before the "this" ptr.
   From http://www.codesourcery.com/cxx-abi/abi.html#normal-call :

      3.1.4 Return Values
      In general, C++ return values are handled just like C return values.
      This includes class type results returned in registers.  However, if
      the return value type has a non-trivial copy constructor or
      destructor, the caller allocates space for a temporary, and passes a
      pointer to the temporary as an implicit first parameter preceding both
      the this parameter and user parameters.  The callee constructs the
      return value into this temporary.  On Itanium, the pointer is passed
      in out0, different from other large class result buffer pointers,
      passed in r8.
   */

#define TYPE_FIELD_ARTIFICIAL   TYPE_FIELD_BITPOS

#define IPF_CXX_ABI_MEMBER_FN(t1, type, nargs)		\
	((t1[0] != NULL) &&				\
	 (t1[1] != NULL) &&				\
	 ((TYPE_CODE (t1[0]) == TYPE_CODE_PTR) ||	\
	  (TYPE_CODE (t1[0]) == TYPE_CODE_REF)) &&	\
	 ((TYPE_CODE (t1[1]) == TYPE_CODE_PTR) ||	\
	  (TYPE_CODE (t1[1]) == TYPE_CODE_REF)) &&	\
	 (TYPE_TARGET_TYPE(t1[1]) == type) &&		\
	 (TYPE_FIELD_ARTIFICIAL(type, 1)) &&		\
	 (!TYPE_CONST(t1[0])))

#else
#define IPF_CXX_ABI_MEMBER_FN(t1, type, nargs)	0
#endif

static boolean is_ipf_cxx_abi_hand_fn_call;  /* is the current hand call to
						a member function using the
						IPF C++ ABI return value 
						hidden pointer?  */

int overload_resolution = 0;

/* This boolean tells what gdb should do if a signal is received while in
   a function called from gdb (call dummy).  If set, gdb unwinds the stack
   and restore the context to what as it was before the call.
   The default is to stop in the frame where the signal was received. */

int unwind_on_signal_p = 0;

/* Find the address of function name NAME in the inferior.  */

value_ptr
find_function_in_inferior_1 (char* name)
{
  register struct symbol *sym;
  struct objfile * objfile;
  struct minimal_symbol *msymbol = 0; 

#ifndef HP_IA64
  if (strcmp (name, "__tls_get_addr") == 0)
    {
       ALL_OBJFILES (objfile)
         {
            if (!strstr (objfile -> name, "/libpthread."))
              continue;
            msymbol = lookup_minimal_symbol (name, NULL, objfile);
            break;
          }
    } /* if __tls_get_addr */
  else
#endif
    {
      sym = lookup_symbol (name, 0, VAR_NAMESPACE, 0, NULL);
      if (sym != NULL)
        {
	
	/* JAGaf62986  Removing error message and adding check to return value_of_variable only 
	in case of functions.  It is assumed that only the class LOC_BLOCK is for functions */
	
          if (SYMBOL_CLASS (sym) == LOC_BLOCK)
            {
          	return value_of_variable (sym, NULL);
            }	
        }
    } /* else */
  if (!msymbol)
    {
       msymbol = lookup_minimal_symbol (name, NULL, NULL);
    }
  if (msymbol != NULL)
    {
      struct type *type;
      CORE_ADDR maddr;
      type = lookup_pointer_type (builtin_type_char);
      type = lookup_function_type (type);
      type = lookup_pointer_type (type);
      maddr = SYMBOL_VALUE_ADDRESS (msymbol);
      return value_from_pointer (type, maddr);
    }
  else
    {
	/* Baskar, stripped exec... */
#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
      ExportEntry *export_entry = NULL;
     
      export_entry = find_symbol_in_export_list (name, NULL);
      if (export_entry)
	{
      	  struct type *type;
      	  CORE_ADDR maddr;
      	  type = lookup_pointer_type (builtin_type_char);
      	  type = lookup_function_type (type);
      	  type = lookup_pointer_type (type);
      	  maddr = (CORE_ADDR)export_entry->address;
      	  return value_from_pointer (type, maddr);
	}
#endif
#endif
    }
  /* silence aCC6 message about missing return value */
  return NULL;
}


value_ptr
find_function_in_inferior (char* name)
{
  value_ptr ret = find_function_in_inferior_1(name);
  if (ret == 0)
    {
      if (!target_has_execution)
	error ("evaluation of this expression requires the target program to be active");
      else
	error ("evaluation of this expression requires the program to have a function \"%s\".", name);
    }
  return ret;
}

/* Allocate NBYTES of space in the inferior using the inferior's malloc
   and return a value that is a pointer to the allocated space. */

value_ptr
value_allocate_space_in_inferior (int len)
{
  value_ptr blocklen;
  register value_ptr val = find_function_in_inferior ("malloc");

  blocklen = value_from_longest (builtin_type_long, (LONGEST) len);
  val = call_function_by_hand (val, 1, &blocklen);
  if (value_logical_not (val))
    {
      if (!target_has_execution)
	error ("No memory available to program now: you need to start the target first");
      else
	error ("No memory available to program: call to malloc failed");
    }

  return val;
}

static CORE_ADDR
allocate_space_in_inferior (int len)
{
  return value_as_long (value_allocate_space_in_inferior (len));
}

/* Cast value ARG2 to type TYPE and return as a value.
   More general than a C cast: accepts any two types of the same length,
   and if ARG2 is an lvalue it can be cast into anything at all.  */
/* In C++, casts may change pointer or object representations.  */

value_ptr
value_cast (struct type *type, register value_ptr arg2)
{
  register enum type_code code1;
  register enum type_code code2;
  register int scalar;
  struct type *type2;

  if (VALUE_AVAILABILITY (arg2) != VA_AVAILABLE)
    {
      VALUE_TYPE (arg2) = type;
      return arg2;
    }

  int convert_to_boolean = 0;

  if (VALUE_TYPE (arg2) == type
#ifdef HP_IA64
      /* Need to cast to other type of Decimal Floating type.
         So dont return if it is DFP. */
      && !(type->code == TYPE_CODE_DECFLOAT)
#endif
      ) 
    return arg2;

  CHECK_TYPEDEF (type);
  code1 = TYPE_CODE (type);
  COERCE_REF (arg2);
  type2 = check_typedef (VALUE_TYPE (arg2));
  /* Some fundamental types are typedef now, code below is passing type 
     to value_cast 
     */
  VALUE_TYPE (arg2) = type2;

#ifdef TARGET_FLOAT80_BIT
  if (type2->code == TYPE_CODE_FLOAT80)
    {  /* Convert the __float80 to long double and then cast the long double
	  to whatever is really needed.
	  */
       __float80   float80_value;

       float80_value = 0; /* initialize for compiler warning */
       memcpy ((char*) &float80_value, 
	       VALUE_CONTENTS (arg2), 
	       sizeof(__float80));
       return value_cast (type, long_double_val_from_float80 (float80_value));
    }

  if (type2->code == TYPE_CODE_FLOAT80_COMPLEX)
    { /* Convert the __float80 _Complex to a long double _Complex and then
	 cast that to whatever is needed.
	 */
      
      __float80 _Complex float80_complex;

      memcpy ((char*) &float80_complex, 
	      VALUE_CONTENTS (arg2), 
	      sizeof(__float80 _Complex));
      return value_cast (type, 
			 long_double_complex_val_from_float80_complex (
			   float80_complex));
    }
  if (type2->code == TYPE_CODE_FLOATHPINTEL)
    {  /* Convert the __fpreg to long double and then cast the long double
          to whatever is really needed.
          */

      value_ptr long_double_value = allocate_value (builtin_type_long_double);
      REGISTER_CONVERT_TO_VIRTUAL (FR0_REGNUM, builtin_type_long_double,
         (VALUE_CONTENTS(arg2))+4, VALUE_CONTENTS (long_double_value));
       return value_cast (type, long_double_value);
    }

  if (code1 == TYPE_CODE_FLOAT80)
    { /* Convert arg2 to long double then make a value from that */
      long double long_double_number;

      if (type2->code != TYPE_CODE_FLT || TYPE_LENGTH (type2) != sizeof(long double))
	{
	  arg2 = value_cast (builtin_type_long_double, arg2);
	}
      memcpy (&long_double_number, 
	      VALUE_CONTENTS (arg2), 
	      sizeof (long double));
      return float80_val_from_long_double (long_double_number);
    }
  if (code1 == TYPE_CODE_FLOATHPINTEL)
    { /* Convert arg2 to long double then make a value from that */
      value_ptr fpreg_value = allocate_value (builtin_type_floathpintel);
      if (type2->code != TYPE_CODE_FLT || TYPE_LENGTH (type2) != TYPE_LENGTH (builtin_type_long_double))
        {
          arg2 = value_cast (builtin_type_long_double, arg2);
        }
      REGISTER_CONVERT_TO_RAW (builtin_type_long_double, FR0_REGNUM,
	VALUE_CONTENTS (arg2), (VALUE_CONTENTS (fpreg_value))+4);
      return fpreg_value;
    }
#endif


  /* A cast to an undetermined-length array_type, such as (TYPE [])OBJECT,
     is treated like a cast to (TYPE [N])OBJECT,
     where N is sizeof(OBJECT)/sizeof(TYPE). */
  if (code1 == TYPE_CODE_ARRAY)
    {
      struct type *element_type = TYPE_TARGET_TYPE (type);
      unsigned element_length = TYPE_LENGTH (check_typedef (element_type));
      if (element_length > 0
	&& TYPE_ARRAY_UPPER_BOUND_TYPE (type) == BOUND_CANNOT_BE_DETERMINED)
	{
	  struct type *range_type = TYPE_INDEX_TYPE (type);
	  int val_length = TYPE_LENGTH (type2);
	  LONGEST low_bound, high_bound, new_length;
	  if (get_discrete_bounds (range_type, &low_bound, &high_bound) < 0)
	    low_bound = 0, high_bound = 0;
	  new_length = val_length / element_length;
	  if (val_length % element_length != 0)
	    warning ("array element type size does not divide object size in cast");
	  /* FIXME-type-allocation: need a way to free this type when we are
	     done with it.  */
	  range_type = create_range_type ((struct type *) NULL,
					  TYPE_TARGET_TYPE (range_type),
					  (int) low_bound,
					  (int) (new_length + low_bound - 1));
	  VALUE_TYPE (arg2) = create_array_type ((struct type *) NULL,
						 element_type, range_type);
	  return arg2;
	}
    }

  if (current_language->c_style_arrays
      && TYPE_CODE (type2) == TYPE_CODE_ARRAY)
    arg2 = value_coerce_array (arg2);

  if (TYPE_CODE (type2) == TYPE_CODE_FUNC)
    arg2 = value_coerce_function (arg2);

  type2 = check_typedef (VALUE_TYPE (arg2));
  COERCE_VARYING_ARRAY (arg2, type2);
  code2 = TYPE_CODE (type2);

#ifdef HP_IA64
  /* If LHS is DFP and RHS is other type, then cast the other type variable
     into dfp variable. */
  if (code1 == TYPE_CODE_DECFLOAT)
    return cast_into_decfloat (type, arg2);

  /* If LHS is other type and RHS is DFP, then cast the dfp variable
     into other type variable. */
  if (code2 == TYPE_CODE_DECFLOAT)
    return cast_from_decfloat (type, arg2);
#endif

  if (code1 == TYPE_CODE_COMPLEX || code1 == TYPE_CODE_FLOAT80_COMPLEX)
    return cast_into_complex (type, arg2);
  
  if (code2 == TYPE_CODE_COMPLEX)
    return cast_from_complex (type, arg2);

  if (code1 == TYPE_CODE_BOOL)
    {
      code1 = TYPE_CODE_INT;
#ifdef GDB_TARGET_IS_HPUX
      /* Don't convert fortran logicals to boolean in HP debugger */
      if (current_language->la_language != language_fortran)
#endif
	convert_to_boolean = 1;
    }
  if (code1 == TYPE_CODE_CHAR)
    code1 = TYPE_CODE_INT;
  if (code2 == TYPE_CODE_BOOL || code2 == TYPE_CODE_CHAR)
    code2 = TYPE_CODE_INT;

  scalar = (code2 == TYPE_CODE_INT || code2 == TYPE_CODE_FLT
	    || code2 == TYPE_CODE_ENUM || code2 == TYPE_CODE_RANGE);

  if (((code1 == TYPE_CODE_STRUCT && code2 == TYPE_CODE_STRUCT) 
      || (code1 == TYPE_CODE_CLASS && code2 == TYPE_CODE_CLASS))
      && TYPE_NAME (type) != 0)
    {
      /* Look in the type of the source to see if it contains the
         type of the target as a superclass.  If so, we'll need to
         offset the object in addition to changing its type.  */
      value_ptr v = search_struct_field (type_name_no_tag (type),
					 arg2, 0, type2, only_baseclass,
					 NULL);
      if (v)
	{
	  VALUE_TYPE (v) = type;
	  return v;
	}
    }
  if (code1 == TYPE_CODE_FLT && (scalar || code2 == TYPE_CODE_COMPLEX))
    return value_from_double (type, value_as_double (arg2));
  else if ((code1 == TYPE_CODE_INT || code1 == TYPE_CODE_ENUM
	    || code1 == TYPE_CODE_RANGE)
	   && (scalar || code2 == TYPE_CODE_PTR || code2 == TYPE_CODE_COMPLEX))
    {
      LONGEST longest;

      if (hp_som_som_object_present &&	/* if target compiled by HP aCC */
	  (code2 == TYPE_CODE_PTR))
	{
	  unsigned int *ptr;
	  value_ptr retvalp;

	  switch (TYPE_CODE (TYPE_TARGET_TYPE (type2)))
	    {
	      /* With HP aCC, pointers to data members have a bias */
	    case TYPE_CODE_MEMBER:
	      retvalp = value_from_longest (type, value_as_long (arg2));
	      /* force evaluation */
	      ptr = (unsigned int *) (void *) VALUE_CONTENTS (retvalp);	
	      *ptr &= ~0x20000000;	/* zap 29th bit to remove bias */
	      return retvalp;

	      /* While pointers to methods don't really point to a function */
	    case TYPE_CODE_METHOD:
	      error ("Pointers to methods not supported with HP aCC");

	    default:
	      break;		/* fall out and go to normal handling */
	    }
	}
      longest = value_as_long (arg2);
      return value_from_longest (type, convert_to_boolean ? 
				 (LONGEST) (longest ? 1 : 0) : longest);
    }
  else if (TYPE_LENGTH (type) == TYPE_LENGTH (type2))
    {
      if (code1 == TYPE_CODE_PTR && code2 == TYPE_CODE_PTR)
	{
	  struct type *t1 = check_typedef (TYPE_TARGET_TYPE (type));
	  struct type *t2 = check_typedef (TYPE_TARGET_TYPE (type2));
	  if (((TYPE_CODE (t1) == TYPE_CODE_STRUCT
	      && TYPE_CODE (t2) == TYPE_CODE_STRUCT) 
	      || (TYPE_CODE (t1) == TYPE_CODE_CLASS
              && TYPE_CODE (t2) == TYPE_CODE_CLASS))
	      && !value_logical_not (arg2))
	    {
	      value_ptr v;

	      /* Look in the type of the source to see if it contains the
	         type of the target as a superclass.  If so, we'll need to
	         offset the pointer rather than just change its type.  */
	      if (TYPE_NAME (t1) != NULL)
		{
		  v = search_struct_field (type_name_no_tag (t1),
					   value_ind (arg2), 0, t2,
					   only_baseclass, NULL);
		  if (v)
		    {
		      v = value_addr (v);
		      VALUE_TYPE (v) = type;
		      return v;
		    }
		}

	      /* Look in the type of the target to see if it contains the
	         type of the source as a superclass.  If so, we'll need to
	         offset the pointer rather than just change its type.
	         FIXME: This fails silently with virtual inheritance.  */
              if ( (strcmp(TYPE_NAME(t1), TYPE_NAME(t2)) != 0) &&
                   TYPE_NAME (t2) != NULL)
		{
		  v = search_struct_field (type_name_no_tag (t2),
					   value_zero (t1, not_lval), 0, t1,
					   only_baseclass, NULL);
		  if (v)
		    {
		      value_ptr v2 = value_ind (arg2);
		      VALUE_ADDRESS (v2) -= VALUE_ADDRESS (v)
			+ VALUE_OFFSET (v);
		      
                      /* JYG: adjust the new pointer value and
			 embedded offset. */
                      v2->aligner.contents[0] -=  VALUE_EMBEDDED_OFFSET (v);
                      VALUE_EMBEDDED_OFFSET (v2) = 0;	
		      /* pes,For run time types, value_ind() returns the
		 	 run time object,in which case we need not change
			 embedded offset because it's going to point to
			 the derived(runtime) object anyway. For static
			 object, we have to do the following to get
			 the "this" pointer of derived class object
			 (Address of Base class - Embedded offset to the
			 derived class+offset). For static objects, type of v2
			 and "enclosing type" of v2 should be same .
			 value_addr() does this manipulation*/
		      if ( VALUE_TYPE(v2) == VALUE_ENCLOSING_TYPE(v2) )
			{
			   VALUE_EMBEDDED_OFFSET (v2) = - (VALUE_EMBEDDED_OFFSET(v));
                           /* QXCR1000905370: WDB prints the value of the data
                              member of derived class object incorrectly. */
#ifdef HP_IA64
		           v2 = value_addr (v2);
		           VALUE_TYPE (v2) = type;
                           VALUE_ENCLOSING_TYPE (v2) = type;
		           return v2;
#endif
			}
		      v2 = value_addr (v2);
		      VALUE_TYPE (v2) = type;
		      return v2;
		    }
		}
	    }
	  /* No superclass found, just fall through to change ptr type.  */
	}
      VALUE_TYPE (arg2) = type;
      VALUE_ENCLOSING_TYPE (arg2) = type;
      VALUE_POINTED_TO_OFFSET (arg2) = 0;
      return arg2;
    }
  else if (code1 == TYPE_CODE_PTR &&
           (code2 == TYPE_CODE_INT ||
	    code2 == TYPE_CODE_ENUM ||
	    code2 == TYPE_CODE_RANGE))
    {
      /* Handle converting any integer type to a pointer.  Above, if code2
         was BOOL or CHAR, it was already converted to INT.  In the else
         clause above, we handle the case where the size of arg2 is already
         the size of a pointer.  Here we deal with a change of size.
         This comes up in LP64 targets where an int is not the size of a 
         pointer.  We presume that a long is the same size as a pointer.
         */

      arg2 = value_cast (builtin_type_long, arg2);
      return value_cast (type, arg2);
    }
  else if (chill_varying_type (type))
    {
      struct type *range1, *range2, *eltype1, *eltype2 = NULL;
      value_ptr val;
      int count1, count2;
      LONGEST low_bound, high_bound;
      char *valaddr, *valaddr_data;
      /* For lint warning about eltype2 possibly uninitialized: */
      eltype2 = NULL;
      if (code2 == TYPE_CODE_BITSTRING)
	error ("not implemented: converting bitstring to varying type");
      if ((code2 != TYPE_CODE_ARRAY && code2 != TYPE_CODE_STRING)
	  || (eltype1 = check_typedef (TYPE_TARGET_TYPE (TYPE_FIELD_TYPE (type, 1))),
	      eltype2 = check_typedef (TYPE_TARGET_TYPE (type2)),
	      (TYPE_LENGTH (eltype1) != TYPE_LENGTH (eltype2)
      /* || TYPE_CODE (eltype1) != TYPE_CODE (eltype2) */ )))
	error ("Invalid conversion to varying type");
      range1 = TYPE_FIELD_TYPE (TYPE_FIELD_TYPE (type, 1), 0);
      range2 = TYPE_FIELD_TYPE (type2, 0);
      if (get_discrete_bounds (range1, &low_bound, &high_bound) < 0)
	count1 = -1;
      else
	count1 = (int) (high_bound - low_bound + 1);
      if (get_discrete_bounds (range2, &low_bound, &high_bound) < 0)
	count1 = -1, count2 = 0;	/* To force error before */
      else
	count2 = (int) (high_bound - low_bound + 1);
      if (count2 > count1)
	error ("target varying type is too small");
      val = allocate_value (type);
      valaddr = VALUE_CONTENTS_RAW (val);
      valaddr_data = valaddr + TYPE_FIELD_BITPOS (type, 1) / 8;
      /* Set val's __var_length field to count2. */
      store_signed_integer (valaddr, TYPE_LENGTH (TYPE_FIELD_TYPE (type, 0)),
			    count2);
      /* Set the __var_data field to count2 elements copied from arg2. */
      memcpy (valaddr_data, VALUE_CONTENTS (arg2),
	      count2 * TYPE_LENGTH (eltype2));
      /* Zero the rest of the __var_data field of val. */
      memset (valaddr_data + count2 * TYPE_LENGTH (eltype2), '\0',
	      (count1 - count2) * TYPE_LENGTH (eltype2));
      return val;
    }
#ifdef GDB_TARGET_IS_HPUX
  else if (code1 == TYPE_CODE_STRING && code2 == TYPE_CODE_STRING &&
           current_language->la_language == language_fortran)
    {
      /* handle fortran character strings when the type's length
         is not the same as arg2's length
       */
      value_ptr val =  allocate_value (type);
      LONGEST to_len = TYPE_LENGTH(type);
      LONGEST from_len = TYPE_LENGTH(type2);
      memcpy (VALUE_CONTENTS_RAW (val), VALUE_CONTENTS (arg2),
              to_len < from_len ? to_len : from_len);
      /* blank fill val if arg2's length is shorter */
      if (from_len < to_len) 
        memset(VALUE_CONTENTS_RAW(val)+from_len, ' ', to_len-from_len);
      return val;
    }
#endif
  else if (VALUE_LVAL (arg2) == lval_memory)
    {
      if (TYPE_LENGTH (type2) > TYPE_LENGTH (type))
        warning ("Casting a data object into another of smaller size. \
If the casted data object is used as a pointer or reference, it may crash \
your process.\n");

      if (code1 == TYPE_CODE_REF && code2 == TYPE_CODE_CLASS)
        printf_filtered ("Hint: To obtain a reference to object X at the \
gdb prompt, use &X instead of X.\n\n");

      return value_at_lazy (type, VALUE_ADDRESS (arg2) + VALUE_OFFSET (arg2),
			    VALUE_BFD_SECTION (arg2));
    }
  else if (code1 == TYPE_CODE_VOID)
    {
      return value_zero (builtin_type_void, not_lval);
    }
  else
    {
      error ("Invalid cast.");
      return 0;
    }
}

/* Create a value of type TYPE that is zero, and return it.  */

value_ptr
value_zero (struct type *type, enum lval_type lv)
{
  register value_ptr val = allocate_value (type);
  int length = TYPE_LENGTH (check_typedef (type));

  if (length > 0)
    { 
      memset (VALUE_CONTENTS (val), 0, length);
    }
  VALUE_LVAL (val) = lv;

  return val;
}

/* Return a value with type TYPE located at ADDR.

   Call value_at only if the data needs to be fetched immediately;
   if we can be 'lazy' and defer the fetch, perhaps indefinately, call
   value_at_lazy instead.  value_at_lazy simply records the address of
   the data and sets the lazy-evaluation-required flag.  The lazy flag
   is tested in the VALUE_CONTENTS macro, which is used if and when
   the contents are actually required.

   Note: value_at does *NOT* handle embedded offsets; perform such
   adjustments before or after calling it. */

value_ptr
value_at (struct type *type, CORE_ADDR addr, asection *sect)
{
  register value_ptr val;

  addr = SWIZZLE (addr);  /* Addr may have come from &symbol */
  if (TYPE_CODE (check_typedef (type)) == TYPE_CODE_VOID)
    error ("Attempt to dereference a generic pointer.");

  val = allocate_value (type);

  read_memory (addr, VALUE_CONTENTS_ALL_RAW (val), TYPE_LENGTH (type));

  VALUE_LVAL (val) = lval_memory;
  VALUE_ADDRESS (val) = addr;
  VALUE_BFD_SECTION (val) = sect;

  return val;
}

/* Return a lazy value with type TYPE located at ADDR (cf. value_at).  */

value_ptr
value_at_lazy (struct type *type, CORE_ADDR addr, asection *sect)
{
  register value_ptr val;

  if (TYPE_CODE (check_typedef (type)) == TYPE_CODE_VOID)
    error ("Attempt to dereference a generic pointer.");

  val = allocate_value (type);

  VALUE_LVAL (val) = lval_memory;
  VALUE_ADDRESS (val) = addr;
  VALUE_LAZY (val) = 1;
  VALUE_BFD_SECTION (val) = sect;

  return val;
}

/* Called only from the VALUE_CONTENTS and VALUE_CONTENTS_ALL macros,
   if the current data for a variable needs to be loaded into
   VALUE_CONTENTS(VAL).  Fetches the data from the user's process, and
   clears the lazy flag to indicate that the data in the buffer is valid.

   If the value is zero-length, we avoid calling read_memory, which would
   abort.  We mark the value as fetched anyway -- all 0 bytes of it.

   This function returns a value because it is used in the VALUE_CONTENTS
   macro as part of an expression, where a void would not work.  The
   value is ignored.  */

int
value_fetch_lazy (register value_ptr val)
{
  CORE_ADDR addr = VALUE_ADDRESS (val) + VALUE_OFFSET (val);
  struct type *val_type = VALUE_ENCLOSING_TYPE (val);
  int length = TYPE_LENGTH (val_type);

  addr = SWIZZLE (addr);

  if (length > 0)
    read_memory (addr, VALUE_CONTENTS_ALL_RAW (val), length);
  
  VALUE_LAZY (val) = 0;
  return 0;
}

/* copy_to_piece - copy a piece of a value from the from buffer 
 * pointed to by from_ptr to both the VALUE_CONTENTS_RAW field of the 
 * toval pointed to by to_ptr and the register or memory indicated by
 * the piece.  *to_ptr and *from_ptr are incremented.
 */

static void
copy_to_piece (int has_float80, 
	       symbol_piece_t* to_piece, 
	       char** to_ptr, 
	       char** from_ptr)
{
  CORE_ADDR             addr;
  CORE_ADDR		data_ptr;
  enum lval_type        lval;
  int                   optim;
  struct type*          piece_type = builtin_type_float;
  char 			raw_buffer[MAX_REGISTER_RAW_SIZE];

  /* Copy the raw value buffer bytes first. */

  memcpy (*to_ptr, *from_ptr, to_piece->piece_size);

  /* Figure out the data type being represented in the register */

  if (REGISTER_CONVERTIBLE (to_piece->arg1))
    {
      switch (to_piece->piece_size)
	{
	  case TARGET_FLOAT_BIT / TARGET_CHAR_BIT:	
		    piece_type = builtin_type_float;
		    break;

	  case TARGET_DOUBLE_BIT / TARGET_CHAR_BIT:	
		    piece_type = builtin_type_double;
		    break;

	  case TARGET_LONG_DOUBLE_BIT / TARGET_CHAR_BIT:
		    if (has_float80)
		      piece_type = builtin_type_float80;
		    else
		      piece_type = builtin_type_long_double;
		    break;

	  default:	warning ("Report to HP Internal error in copy_to_piece."
			    );
		    piece_type = builtin_type_float;
		    break;
	}
    }

  /* Now we need to update this piece in a register or in memory. 
     First, we get the right floating point representation into raw_buffer.
   */

  if (to_piece->piece_type == PIECE_LOC_REGISTER)
    { 
      if (REGISTER_CONVERTIBLE (to_piece->arg1)) 
        {
#ifdef HP_IA64
	  assert (to_piece->arg1 <= FRLAST_REGNUM);
#endif
	  REGISTER_CONVERT_TO_RAW (piece_type, 
				   to_piece->arg1, 
				   from_ptr, 
				   raw_buffer);
	  write_register_gen ((int) to_piece->arg1, raw_buffer);
	}
      else
        {
	  write_register_gen ((int) to_piece->arg1, *from_ptr);
	}

      *to_ptr += to_piece->piece_size;
      *from_ptr += to_piece->piece_size;
      return;
    }

    /* This piece is memory.  Get the address where the from_ptr data
     * will be copied into data_ptr.
     */

    if (to_piece->piece_type == PIECE_LOC_BASEREG)
      {  /* The address register arg1 plus offset arg2 */
	get_saved_register (raw_buffer, 
			    &optim, 
			    &addr, 
			    0, 
			    (int) to_piece->arg1, 
			    &lval);
	memcpy (&data_ptr, 
		raw_buffer,
		sizeof(CORE_ADDR)
	       );
	if (IS_TARGET_LRE && IS_FLIP_REG (to_piece->arg1))
	  { // the address in the register was fipped.
	    endian_flip (&data_ptr, sizeof(CORE_ADDR));
	  }
	data_ptr += to_piece->arg2;
      }
    else if (to_piece->piece_type == PIECE_LOC_STATIC)
      {
	data_ptr = (long) to_piece->arg1;
      }
    else
      {
	warning ("Report to HP Internal error in copy_to_piece.");
      }

  target_write_memory (data_ptr, *to_ptr, to_piece->piece_size);
  *to_ptr += to_piece->piece_size;
  *from_ptr += to_piece->piece_size;

} // end copy_to_piece

/* value_to_pieces copy the value in fromval to the value in toval, which
 * has nbr_pieces > 0.  fromval has already been coerced to the same type
 * as toval and has been realized, so basically the data we need to copy is
 * in the raw buffer of to_val.  It need to be copied to the various 
 * pieces, which are typically inregisters.  See piece_list in symbab.h.
 */
static void
value_to_pieces (value_ptr toval, value_ptr fromval)
{
  int		idx;
  char*		from_ptr;
  char*		to_ptr;

  from_ptr = VALUE_CONTENTS_RAW (fromval);
  to_ptr = VALUE_CONTENTS_RAW (toval);
  for (idx = 0;  idx < VALUE_NBR_PIECES(toval); idx++)
    {
      copy_to_piece (   TYPE_CODE (VALUE_TYPE(toval)) 
		     == TYPE_CODE (builtin_type_float80_complex),
	             &(VALUE_PIECE_LIST (toval)[idx]), 
		     &to_ptr, 
		     &from_ptr);
    }
} // end value_to_pieces


/* Store the contents of FROMVAL into the location of TOVAL.
   Return a new value with the location of TOVAL and contents of FROMVAL.  */

value_ptr
value_assign (register value_ptr toval, register value_ptr fromval)
{
  register struct type *type;
  register value_ptr val;
  char raw_buffer[MAX_REGISTER_RAW_SIZE];
  int use_buffer = 0;

  if (!toval->modifiable)
    error ("Left operand of assignment is not a modifiable lvalue.");

  if (VALUE_AVAILABILITY (toval) != VA_AVAILABLE)
    error ("Left operand of assignment is not available at this PC address.");

  if (VALUE_AVAILABILITY (fromval) != VA_AVAILABLE)
    error ("Right operand of assignment is not available at this PC address.");

  COERCE_REF (toval);

  type = VALUE_TYPE (toval);
  if (VALUE_LVAL (toval) != lval_internalvar)
    fromval = value_cast (type, fromval);
  else
    COERCE_ARRAY (fromval);
  CHECK_TYPEDEF (type);

  /* If TOVAL is a special machine register requiring conversion
     of program values to a special raw format,
     convert FROMVAL's contents now, with result in `raw_buffer',
     and set USE_BUFFER to the number of bytes to write.  */

  if (VALUE_REGNO (toval) >= 0)
    {
      int regno = VALUE_REGNO (toval);
      if (REGISTER_CONVERTIBLE (regno))
	{
	  struct type *fromtype = check_typedef (VALUE_TYPE (fromval));
	  REGISTER_CONVERT_TO_RAW (fromtype, regno,
				     VALUE_CONTENTS (fromval), raw_buffer);
	  use_buffer = REGISTER_RAW_SIZE (regno);
	}
    }

  switch (VALUE_LVAL (toval))
    {
    case lval_internalvar:
      set_internalvar (VALUE_INTERNALVAR (toval), fromval);
      val = value_copy (VALUE_INTERNALVAR (toval)->value);
      VALUE_ENCLOSING_TYPE (val) = VALUE_ENCLOSING_TYPE (fromval);
      VALUE_EMBEDDED_OFFSET (val) = VALUE_EMBEDDED_OFFSET (fromval);
      VALUE_POINTED_TO_OFFSET (val) = VALUE_POINTED_TO_OFFSET (fromval);
      return val;

    case lval_internalvar_component:
      set_internalvar_component (VALUE_INTERNALVAR (toval),
				 VALUE_OFFSET (toval),
				 VALUE_BITPOS (toval),
				 VALUE_BITSIZE (toval),
				 fromval);
      break;

    case lval_memory:
      {
	char *dest_buffer;
	CORE_ADDR changed_addr;
	int changed_len;

	if (VALUE_BITSIZE (toval))
	  {
	    char buffer[sizeof (LONGEST)];
	    /* We assume that the argument to read_memory is in units of
	       host chars.  FIXME:  Is that correct?  */
	    changed_len = (VALUE_BITPOS (toval)
			   + VALUE_BITSIZE (toval)
			   + HOST_CHAR_BIT - 1)
	      / HOST_CHAR_BIT;

	    if (changed_len > (int) sizeof (LONGEST))
	      error ("Can't handle bitfields which don't fit in a %d bit word.",
		     sizeof (LONGEST) * HOST_CHAR_BIT);

	    read_memory (SWIZZLE(VALUE_ADDRESS (toval) + VALUE_OFFSET (toval)),
			 buffer, changed_len);
	    modify_field (buffer, value_as_long (fromval),
			  VALUE_BITPOS (toval), VALUE_BITSIZE (toval));
	    changed_addr = SWIZZLE (  VALUE_ADDRESS (toval) 
				    + VALUE_OFFSET (toval));
	    dest_buffer = buffer;
	  }
	else if (use_buffer)
	  {
	    changed_addr = VALUE_ADDRESS (toval) + VALUE_OFFSET (toval);
	    changed_len = use_buffer;
	    dest_buffer = raw_buffer;
	  }
	else
	  {
	    changed_addr = VALUE_ADDRESS (toval) + VALUE_OFFSET (toval);
	    changed_len = TYPE_LENGTH (type);
	    dest_buffer = VALUE_CONTENTS (fromval);
	  }

	write_memory (changed_addr, dest_buffer, changed_len);
	if (memory_changed_hook)
	  memory_changed_hook (changed_addr, changed_len);
      }
      break;

    case lval_register:
      if (VALUE_NBR_PIECES(toval))
	{
	  value_to_pieces (toval, fromval);
	  break;
	}
#ifdef HP_IA64_GAMBIT
      if (VALUE_REGNO (toval) == PC_REGNUM)
	{
	  /* FIXME: Assumes value_as_pointer() returns a LONGEST
	   * value that fits in a CORE_ADDR.
	   */
	  CORE_ADDR pc_value = value_as_pointer (fromval);
	  if ((pc_value % 16) != 0)
	    {
	      error ("Value 0x%llx is not bundle-aligned.\n", pc_value);
	    }
	}
#endif /* HP_IA64_GAMBIT */
      if (VALUE_BITSIZE (toval))
	{
	  char buffer[sizeof (LONGEST)];
	  int len = 
		REGISTER_RAW_SIZE (VALUE_REGNO (toval)) - VALUE_OFFSET (toval);

	  if (len > (int) sizeof (LONGEST))
	    error ("Can't handle bitfields in registers larger than %d bits.",
		   sizeof (LONGEST) * HOST_CHAR_BIT);

	  if (VALUE_BITPOS (toval) + VALUE_BITSIZE (toval)
	      > len * HOST_CHAR_BIT)
	    /* Getting this right would involve being very careful about
	       byte order.  */
	    error ("Can't assign to bitfields that cross register "
		   "boundaries.");

	  read_register_bytes ((int) VALUE_ADDRESS (toval) + VALUE_OFFSET (toval),
			       buffer, len);
	  modify_field (buffer, value_as_long (fromval),
			VALUE_BITPOS (toval), VALUE_BITSIZE (toval));
	  write_register_bytes ((int) VALUE_ADDRESS (toval) + VALUE_OFFSET (toval),
				buffer, len);
	}
      else if (use_buffer)
	write_register_bytes ((int) VALUE_ADDRESS (toval) + VALUE_OFFSET (toval),
			      raw_buffer, use_buffer);
      else
	{
	  /* Do any conversion necessary when storing this type to more
	     than one register.  */
#ifdef REGISTER_CONVERT_FROM_TYPE
	  memcpy (raw_buffer, VALUE_CONTENTS (fromval), TYPE_LENGTH (type));
	  REGISTER_CONVERT_FROM_TYPE (VALUE_REGNO (toval), type, raw_buffer);
	  write_register_bytes (VALUE_ADDRESS (toval) + VALUE_OFFSET (toval),
				raw_buffer, TYPE_LENGTH (type));
#else
	  write_register_bytes ((int) VALUE_ADDRESS (toval) + VALUE_OFFSET (toval),
			      VALUE_CONTENTS (fromval), TYPE_LENGTH (type));
#endif
#ifdef HP_IA64
	  if (VALUE_REGNO (toval) == PSR_REGNUM)
	    {
	      warning ("Only low 5 bits have been modified in PSR.");
	    }
#endif
	}
      /* Assigning to the stack pointer, frame pointer, and other
         (architecture and calling convention specific) registers may
         cause the frame cache to be out of date.  We just do this
         on all assignments to registers for simplicity; I doubt the slowdown
         matters.  */
      reinit_frame_cache ();
      break;
    case lval_reg_frame_relative:
      {
	/* value is stored in a series of registers in the frame
	   specified by the structure.  Copy that value out, modify
	   it, and copy it back in.  */
	int amount_to_copy = (VALUE_BITSIZE (toval) ? 1 : TYPE_LENGTH (type));
	int reg_size = REGISTER_RAW_SIZE (VALUE_FRAME_REGNUM (toval));
	int byte_offset = VALUE_OFFSET (toval) % reg_size;
	int reg_offset = VALUE_OFFSET (toval) / reg_size;
	int amount_copied;

	/* Make the buffer large enough in all cases.  */
	char *buffer = (char *) alloca (amount_to_copy
					+ sizeof (LONGEST)
					+ MAX_REGISTER_RAW_SIZE);

	int regno;
	struct frame_info *frame;

	/* Figure out which frame this is in currently.  */
	for (frame = get_current_frame ();
	     frame && FRAME_FP (frame) != VALUE_FRAME (toval);
	     frame = get_prev_frame (frame))
	  ;

	if (!frame)
	  error ("Value being assigned to is no longer active.");

	amount_to_copy += (reg_size - amount_to_copy % reg_size);

	/* Copy it out.  */
	for ((regno = VALUE_FRAME_REGNUM (toval) + reg_offset,
	      amount_copied = 0);
	     amount_copied < amount_to_copy;
	     amount_copied += reg_size, regno++)
	  {
	    get_saved_register (buffer + amount_copied,
				(int *) NULL, (CORE_ADDR *) NULL,
				frame, regno, (enum lval_type *) NULL);
	  }

	/* Modify what needs to be modified.  */
	if (VALUE_BITSIZE (toval))
	  modify_field (buffer + byte_offset,
			value_as_long (fromval),
			VALUE_BITPOS (toval), VALUE_BITSIZE (toval));
	else if (use_buffer)
	  memcpy (buffer + byte_offset, raw_buffer, use_buffer);
	else
	  memcpy (buffer + byte_offset, VALUE_CONTENTS (fromval),
		  TYPE_LENGTH (type));

	/* Copy it back.  */
	for ((regno = VALUE_FRAME_REGNUM (toval) + reg_offset,
	      amount_copied = 0);
	     amount_copied < amount_to_copy;
	     amount_copied += reg_size, regno++)
	  {
	    enum lval_type lval;
	    CORE_ADDR addr;
	    int optim;

	    /* Just find out where to put it.  */
	    get_saved_register ((char *) NULL,
				&optim, &addr, frame, regno, &lval);

	    if (optim)
	      error ("Attempt to assign to a value that was optimized out.");
	    if (lval == lval_memory)
	      write_memory (addr, buffer + amount_copied, reg_size);
	    else if (lval == lval_register)
	      write_register_bytes ((int) addr, buffer + amount_copied, reg_size);
	    else
	      error ("Attempt to assign to an unmodifiable value.");
	  }

	if (register_changed_hook)
	  register_changed_hook (-1);
      }
      break;


    default:
      error ("Left operand of assignment is not an lvalue.");
    }

  /* If the field does not entirely fill a LONGEST, then zero the sign bits.
     If the field is signed, and is negative, then sign extend. */
  if ((VALUE_BITSIZE (toval) > 0)
      && (VALUE_BITSIZE (toval) < 8 * (int) sizeof (LONGEST)))
    {
      LONGEST fieldval = value_as_long (fromval);
      LONGEST valmask = (((ULONGEST) 1) << VALUE_BITSIZE (toval)) - 1;

      fieldval &= valmask;
      if (!TYPE_UNSIGNED (type) && (fieldval & (valmask ^ (valmask >> 1))))
	fieldval |= ~valmask;

      fromval = value_from_longest (type, fieldval);
    }

  val = value_copy (toval);
  memcpy (VALUE_CONTENTS_RAW (val), VALUE_CONTENTS (fromval),
	  TYPE_LENGTH (type));
  VALUE_TYPE (val) = type;
  VALUE_ENCLOSING_TYPE (val) = VALUE_ENCLOSING_TYPE (fromval);
  VALUE_EMBEDDED_OFFSET (val) = VALUE_EMBEDDED_OFFSET (fromval);
  VALUE_POINTED_TO_OFFSET (val) = VALUE_POINTED_TO_OFFSET (fromval);

  return val;
}

/* Extend a value VAL to COUNT repetitions of its type.  */

value_ptr
value_repeat (value_ptr arg1, int count)
{
  register value_ptr val;

  if (VALUE_LVAL (arg1) != lval_memory)
    error ("Only values in memory can be extended with '@'.");
  if (count < 1)
    error ("Invalid number %d of repetitions.", count);

  /* If the value isn't available, pass the right type back up. */
  if (VALUE_AVAILABILITY (arg1) != VA_AVAILABLE)
    return arg1;

  val = allocate_repeat_value (VALUE_ENCLOSING_TYPE (arg1), count);

  /* pai: should this be repeated in a loop???  What is repeat used
     for??  There is one call to this under BINOP_REPEAT in eval.c; I
     don't know what the intended semantics are; sounds like a
     "replicate" operation. "Repeating" C++ objects sounds perilous
     anyway -- do we use constructors or copy constructors, do we use
     array constructors, or simply copy memory?  So should the
     enclosing_type stuff be used at all here?  Perhaps this operation
     should be forbidden for C++. */

  read_memory (SWIZZLE(VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1)),
	       VALUE_CONTENTS_ALL_RAW (val),
	       TYPE_LENGTH (VALUE_ENCLOSING_TYPE (val)));
  VALUE_LVAL (val) = lval_memory;
  VALUE_ADDRESS (val) = SWIZZLE(VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1));

  return val;
}

#ifdef HPPA_DOC
void display_opt_debug_info(struct symbol *sym)
{
  char *set_late_str = "set late";
  char *set_early_str = "set early";
  CORE_ADDR addr;
  struct range_list *r = SYMBOL_RANGES(sym);
  struct range_list *matching_range = NULL;
  struct frame_info *frame;

  /* Check for range table */
  if (!r)
    return;

  /* Check that the target is executing */
  if (!(target_has_execution))
    return;

  /* Get address from frame */
  frame = get_current_frame ();
  addr = frame ? get_frame_pc (frame) : read_pc();

  for (; r ; r = r->next)
    {
      if (r->start <= addr && r->end > addr)
        {
          matching_range = r;
          break;
        }

      if (r->end == addr)
        matching_range = r;
    }

  if (!matching_range)
    return;

  if (matching_range->set_early || matching_range->set_late)
    {
      printf_filtered("%s: %s (assignment from line %d, moved to line %d)\n",
		      SYMBOL_NAME(sym),
		      matching_range->set_late ? set_late_str : set_early_str,
		      matching_range->comes_from_line,
		      matching_range->moved_to_line);
    }
}
#endif /* HPPA_DOC */

/* Return a long double complex value of (0, 1), i.e. 0 + 1 * _Imaginary_I */
value_ptr
value_of_imaginary_i (int complain)
{
  register value_ptr val = allocate_value (builtin_type_imaginary);
  DOUBLEST *	part_value;

  part_value = (DOUBLEST *) (void *) VALUE_CONTENTS_RAW(val);
  *part_value = 1.0L;  /* _Imaginary_I has an imaginary part of 1. */
  return val;
} /* end value_of_imaginary_i */

value_ptr
value_of_variable (struct symbol *var, struct block *b)
{
  value_ptr val;
  struct frame_info *frame = NULL;

  if (!b)
    frame = NULL;		/* Use selected frame.  */
  else if (symbol_read_needs_frame (var))
    {
      frame = block_innermost_frame (b);
      if (!frame)
	{
	  if (BLOCK_FUNCTION (b)
	      && SYMBOL_SOURCE_NAME (BLOCK_FUNCTION (b)))
	    error ("No frame is currently executing in block %s.",
		   SYMBOL_SOURCE_NAME (BLOCK_FUNCTION (b)));
	  else
	    error ("No frame is currently executing in specified block");
	}
    }

  val = read_var_value (var, frame);

#ifdef HPPA_DOC
  if (val && VALUE_AVAILABILITY (val) == VA_OPTIMIZED_OUT)
    {
      display_opt_debug_info(var);
      error ("Due to optimization, the address/value of \"%s\" is unknown for "
             "the current location.", SYMBOL_SOURCE_NAME (var));
      return NULL;
    }
#endif

  if (!val)
    error ("Address of symbol \"%s\" is unknown.", SYMBOL_SOURCE_NAME (var));

#ifdef HPPA_DOC
  display_opt_debug_info(var);
#endif

  return val;
}

/* Given a value which is an array, return a value which is a pointer to its
   first element, regardless of whether or not the array has a nonzero lower
   bound.

   FIXME:  A previous comment here indicated that this routine should be
   substracting the array's lower bound.  It's not clear to me that this
   is correct.  Given an array subscripting operation, it would certainly
   work to do the adjustment here, essentially computing:

   (&array[0] - (lowerbound * sizeof array[0])) + (index * sizeof array[0])

   However I believe a more appropriate and logical place to account for
   the lower bound is to do so in value_subscript, essentially computing:

   (&array[0] + ((index - lowerbound) * sizeof array[0]))

   As further evidence consider what would happen with operations other
   than array subscripting, where the caller would get back a value that
   had an address somewhere before the actual first element of the array,
   and the information about the lower bound would be lost because of
   the coercion to pointer type.
 */

value_ptr
value_coerce_array (value_ptr arg1)
{
  register struct type *type = check_typedef (VALUE_TYPE (arg1));

  if (VALUE_LVAL (arg1) != lval_memory)
    error ("Attempt to take address of value not located in memory.");

  /* If the value isn't available, pass the right type back up. */
  if (VALUE_AVAILABILITY (arg1) != VA_AVAILABLE)
    {
      VALUE_TYPE (arg1) = lookup_pointer_type (TYPE_TARGET_TYPE (type));
      return arg1;
    }

  return value_from_pointer (lookup_pointer_type (TYPE_TARGET_TYPE (type)),
			     (VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1)));
}

/* Given a value which is a function, return a value which is a pointer
   to it.  */

value_ptr
value_coerce_function (value_ptr arg1)
{
  value_ptr retval;

  if (VALUE_LVAL (arg1) != lval_memory)
    error ("Attempt to take address of value not located in memory.");

  retval = value_from_pointer (lookup_pointer_type (VALUE_TYPE (arg1)),
			       (VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1)));
  VALUE_BFD_SECTION (retval) = VALUE_BFD_SECTION (arg1);
  return retval;
}

/* Return a pointer value for the object for which ARG1 is the contents.  */

value_ptr
value_addr (value_ptr arg1)
{
  value_ptr arg2;

  struct type *type = check_typedef (VALUE_TYPE (arg1));

  if (TYPE_CODE (type) == TYPE_CODE_REF)
    {
      /* Copy the value, but change the type from (T&) to (T*).
         We keep the same location information, which is efficient,
         and allows &(&X) to get the location containing the reference. */
      arg2 = value_copy (arg1);
      VALUE_TYPE (arg2) = lookup_pointer_type (TYPE_TARGET_TYPE (type));
      return arg2;
    }
  if (TYPE_CODE (type) == TYPE_CODE_FUNC)
    return value_coerce_function (arg1);

  /* If the value isn't available, pass the right type back up. */
  if (VALUE_AVAILABILITY (arg1) != VA_AVAILABLE)
    {
      VALUE_TYPE (arg1) = lookup_pointer_type (TYPE_TARGET_TYPE (type));
      return arg1;
    }

  if (VALUE_LVAL (arg1) != lval_memory)
    {
      int len = TYPE_LENGTH(VALUE_ENCLOSING_TYPE(arg1));
      CORE_ADDR addr = allocate_space_in_inferior(len);
      if (!addr)
        error("Could not allocate memory in inferior\n");
      
      write_memory (addr, VALUE_CONTENTS_ALL_RAW(arg1), len);

      arg2 = value_from_pointer (lookup_pointer_type (VALUE_TYPE (arg1)),
				 addr + VALUE_EMBEDDED_OFFSET (arg1));
    }

  else
    {
      /* Get target memory address */
      arg2 = value_from_pointer (lookup_pointer_type (VALUE_TYPE (arg1)),
                                 VALUE_ADDRESS (arg1)
				 + VALUE_OFFSET (arg1)
				 + VALUE_EMBEDDED_OFFSET (arg1));
    }

  /* This may be a pointer to a base subobject; so remember the
     full derived object's type ... */
  VALUE_ENCLOSING_TYPE (arg2) = lookup_pointer_type (VALUE_ENCLOSING_TYPE (arg1));
  /* ... and also the relative position of the subobject in the full object */
  VALUE_POINTED_TO_OFFSET (arg2) = VALUE_EMBEDDED_OFFSET (arg1);
  VALUE_BFD_SECTION (arg2) = VALUE_BFD_SECTION (arg1);
  return arg2;
}

/* Given a value of a pointer type, apply the C unary * operator to it. 
   If it's a reference type, dereference it */

value_ptr
value_ind (value_ptr arg1)
{
  struct type *base_type;
  value_ptr arg2;

  COERCE_ARRAY (arg1);

  base_type = check_typedef (VALUE_TYPE (arg1));

  if (TYPE_CODE (base_type) == TYPE_CODE_MEMBER)
    error ("not implemented: member types in value_ind");

  /* If the value isn't available, pass the right type back up. */
  if (VALUE_AVAILABILITY (arg1) != VA_AVAILABLE)
    {
      VALUE_TYPE (arg1) = base_type;
      return arg1;
    }

  /* Allow * on an integer so we can cast it to whatever we want.
     This returns an int, which seems like the most C-like thing
     to do.  "long long" variables are rare enough that
     BUILTIN_TYPE_LONGEST would seem to be a mistake.  */
  /* pes,JAGae03507 - Dereferencing a 64 bit pointer in 64 bit gdb, was 
     showing 32 bit result because gdb was dereferencing the pointer as
     integer(32 bits - builtin_type_int). Changed builtin_type_int to 
     builtin_type_long (64 bits) to display 64 bit results */
#ifdef HP_IA64
  if (TYPE_CODE (base_type) == TYPE_CODE_INT)
    {
      /* ia64 - allow dereference to return something longer than int */
      if (TYPE_LENGTH (base_type) == TYPE_LENGTH (builtin_type_int))
	return value_at (builtin_type_int,
			 (CORE_ADDR) value_as_long (arg1),
			 VALUE_BFD_SECTION (arg1));
      else
	return value_at (builtin_type_long_long,
			 (CORE_ADDR) value_as_long (arg1),
			 VALUE_BFD_SECTION (arg1));
    }
#else
  if (TYPE_CODE (base_type) == TYPE_CODE_INT)
    return value_at (builtin_type_long,
		     (CORE_ADDR) value_as_long (arg1),
		     VALUE_BFD_SECTION (arg1));
#endif /* HP_IA64 */
  else if (TYPE_CODE (base_type) == TYPE_CODE_PTR)
    {
      struct type *enc_type;
      /* We may be pointing to something embedded in a larger object */
      /* Get the real type of the enclosing object */
      enc_type = check_typedef (VALUE_ENCLOSING_TYPE (arg1));
      enc_type = TYPE_TARGET_TYPE (enc_type);
      /* Retrieve the enclosing object pointed to */
      arg2 = value_at_lazy (enc_type,
		   value_as_pointer (arg1) - VALUE_POINTED_TO_OFFSET (arg1),
			    VALUE_BFD_SECTION (arg1));
      /* Re-adjust type */
      VALUE_TYPE (arg2) = TYPE_TARGET_TYPE (base_type);
      /* Add embedding info */
      VALUE_ENCLOSING_TYPE (arg2) = enc_type;
      VALUE_EMBEDDED_OFFSET (arg2) = VALUE_POINTED_TO_OFFSET (arg1);
      /* We may be pointing to an object of some derived type */
      arg2 = value_full_object (arg2, NULL, 0, 0, 0);
      return arg2;
    }
  /* QXCR1000901677: ptype when "set print object": Attempt to take contents of
     a non-pointer value. */
  else if (TYPE_CODE (base_type) == TYPE_CODE_STRUCT
			    || TYPE_CODE (base_type) == TYPE_CODE_CLASS)
      /* COERCE_ARRAY has taken care of dereferencing the reference */
    return arg1;

  error ("Attempt to take contents of a non-pointer value.");
  return 0;			/* For lint -- never reached */
}

/* Pushing small parts of stack frames.  */

/* Push one word (the size of object that a register holds).  */

CORE_ADDR
push_word (CORE_ADDR sp, ULONGEST word)
{
  register int len = REGISTER_SIZE;
  char buffer[MAX_REGISTER_RAW_SIZE];

  store_unsigned_integer (buffer, len, word);
  if (INNER_THAN (1, 2))
    {
      /* stack grows downward */
      sp -= len;
      write_memory (sp, buffer, len);
    }
  else
    {
      /* stack grows upward */
      write_memory (sp, buffer, len);
      sp += len;
    }

  return sp;
}

/* Push LEN bytes with data at BUFFER.  */

CORE_ADDR
push_bytes (CORE_ADDR sp, char *buffer, int len)
{
  if (INNER_THAN (1, 2))
    {
      /* stack grows downward */
      sp -= len;
      write_memory (sp, buffer, len);
    }
  else
    {
      /* stack grows upward */
      write_memory (sp, buffer, len);
      sp += len;
    }

  return sp;
}

#ifndef PARM_BOUNDARY
#define PARM_BOUNDARY (0)
#endif

/* Push onto the stack the specified value VALUE.  Pad it correctly for
   it to be an argument to a function.  */

static CORE_ADDR
value_push (register CORE_ADDR sp, value_ptr arg)
{
  register int len = TYPE_LENGTH (VALUE_ENCLOSING_TYPE (arg));
  register int container_len = len;
  register int offset;

  /* How big is the container we're going to put this value in?  */
  if (PARM_BOUNDARY)
    container_len = ((len + PARM_BOUNDARY / TARGET_CHAR_BIT - 1)
		     & ~(PARM_BOUNDARY / TARGET_CHAR_BIT - 1));

  /* Are we going to put it at the high or low end of the container?  */
  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
    offset = container_len - len;
  else
    offset = 0;

  if (INNER_THAN (1, 2))
    {
      /* stack grows downward */
      sp -= container_len;
      write_memory (sp + offset, VALUE_CONTENTS_ALL (arg), len);
    }
  else
    {
      /* stack grows upward */
      write_memory (sp + offset, VALUE_CONTENTS_ALL (arg), len);
      sp += container_len;
    }

  return sp;
}

#ifndef PUSH_ARGUMENTS
#define PUSH_ARGUMENTS default_push_arguments
#endif

CORE_ADDR
default_push_arguments (int nargs, value_ptr *args, CORE_ADDR sp,
			int struct_return, CORE_ADDR struct_addr)
{
  /* ASSERT ( !struct_return); */
  int i;
  for (i = nargs - 1; i >= 0; i--)
    sp = value_push (sp, args[i]);
  return sp;
}


/* A default function for COERCE_FLOAT_TO_DOUBLE: do the coercion only
   when we don't have any type for the argument at hand.  This occurs
   when we have no debug info, or when passing varargs.

   This is an annoying default: the rule the compiler follows is to do
   the standard promotions whenever there is no prototype in scope,
   and almost all targets want this behavior.  But there are some old
   architectures which want this odd behavior.  If you want to go
   through them all and fix them, please do.  Modern gdbarch-style
   targets may find it convenient to use standard_coerce_float_to_double.  */
int
default_coerce_float_to_double (struct type *formal, struct type *actual)
{
  return formal == NULL;
}


/* Always coerce floats to doubles when there is no prototype in scope.
   If your architecture follows the standard type promotion rules for
   calling unprototyped functions, your gdbarch init function can pass
   this function to set_gdbarch_coerce_float_to_double to use its logic.  */
int
standard_coerce_float_to_double (struct type *formal, struct type *actual)
{
  return 1;
}


/* Perform the standard coercions that are specified
   for arguments to be passed to C functions.

   If PARAM_TYPE is non-NULL, it is the expected parameter type.
   IS_PROTOTYPED is non-zero if the function declaration is prototyped.  */

static value_ptr
value_arg_coerce (value_ptr arg, struct type *param_type, int is_prototyped)
{
  register struct type *arg_type = check_typedef (VALUE_TYPE (arg));
  register struct type *type
  = param_type ? check_typedef (param_type) : arg_type;

  /* If the value isn't available, pass the right type back up. */
  if (VALUE_AVAILABILITY (arg) != VA_AVAILABLE)
    {
      VALUE_TYPE (arg) = type;
      return arg;
    }

  switch (TYPE_CODE (type))
    {
    case TYPE_CODE_REF:
    case TYPE_CODE_PTR:
      /*CR1000593682*/
      if (TYPE_CODE (arg_type) == TYPE_CODE_INT &&
         (TYPE_CODE (type) == TYPE_CODE_PTR))
         break;        

      /* If the argument type isn't an address and the callee formal 
	 parameter is, make the conversion for the user. */
      if ((TYPE_CODE (arg_type) != TYPE_CODE_REF) &&
          (TYPE_CODE (arg_type) != TYPE_CODE_PTR))
	{
	  arg = value_addr (arg);
	  VALUE_TYPE (arg) = param_type;
	  return arg;
	}
      break;
    case TYPE_CODE_INT:
    case TYPE_CODE_CHAR:
    case TYPE_CODE_BOOL:
    case TYPE_CODE_ENUM:
      /* If we don't have a prototype, coerce to integer type if necessary.  */
      /* Here one more check needed for a character type. When a variable is 
         declared as char type, the variable type would be TYPE_CODE_INT
         and length would be 1 byte (not 4 bytes).
         But actually that may be used to store an interger value or 
         a single character. So if the type name is "char" then it is good 
         to not to change its type length to builtin_type_int length (4 bytes)
         For more info see QXCR1000577394:Bogus SIGTRAP when command-line 
         call on non-current thread   */
      if (!is_prototyped)
	{
	  if (TYPE_LENGTH (type) < TYPE_LENGTH (builtin_type_int) &&
	      (strcmp (param_type->name, "char") != 0))
	    type = builtin_type_int;
	}
      /* Currently all target ABIs require at least the width of an integer
         type for an argument.  We may have to conditionalize the following
         type coercion for future targets.  */
      if (TYPE_LENGTH (type) < TYPE_LENGTH (builtin_type_int) && 
          (strcmp (param_type->name, "char") != 0))
	type = builtin_type_int;
      break;
    case TYPE_CODE_FLT:
      /* FIXME: We should always convert floats to doubles in the
         non-prototyped case.  As many debugging formats include
         no information about prototyping, we have to live with
         COERCE_FLOAT_TO_DOUBLE for now.  */
      if (!is_prototyped && COERCE_FLOAT_TO_DOUBLE (param_type, arg_type))
	{
	  if (TYPE_LENGTH (type) < TYPE_LENGTH (builtin_type_double))
	    type = builtin_type_double;
	  else if (TYPE_LENGTH (type) > TYPE_LENGTH (builtin_type_double))
	    type = builtin_type_long_double;
	}
      break;
    case TYPE_CODE_FUNC:
      type = lookup_pointer_type (type);
      break;
    case TYPE_CODE_ARRAY:
      if (current_language->c_style_arrays)
	type = lookup_pointer_type (TYPE_TARGET_TYPE (type));
      break;
    case TYPE_CODE_UNDEF:
    case TYPE_CODE_STRUCT:
    case TYPE_CODE_CLASS:
    case TYPE_CODE_UNION:
    case TYPE_CODE_VOID:
    case TYPE_CODE_SET:
    case TYPE_CODE_RANGE:
    case TYPE_CODE_STRING:
    case TYPE_CODE_BITSTRING:
    case TYPE_CODE_ERROR:
    case TYPE_CODE_MEMBER:
    case TYPE_CODE_METHOD:
    case TYPE_CODE_COMPLEX:
    default:
      break;
    }

  return value_cast (type, arg);
}

/* Determine a function's address and its return type from its value.
   Calls error() if the function is not valid for calling.  */

static CORE_ADDR
find_function_addr (value_ptr function, struct type **retval_type)
{
  register struct type *ftype = check_typedef (VALUE_TYPE (function));
  register enum type_code code = TYPE_CODE (ftype);
  struct type *value_type = NULL;
  CORE_ADDR funaddr = (CORE_ADDR) NULL;
  struct obj_section *section;

  /* If it's a member function, just look at the function
     part of it.  */

  /* Determine address to call.  */
  if (code == TYPE_CODE_FUNC || code == TYPE_CODE_METHOD)
    {
      funaddr = VALUE_ADDRESS (function);
      value_type = TYPE_TARGET_TYPE (ftype);
    }
  else if (code == TYPE_CODE_PTR)
    {
      funaddr = value_as_pointer (function);
      ftype = check_typedef (TYPE_TARGET_TYPE (ftype));
      if (TYPE_CODE (ftype) == TYPE_CODE_FUNC
	  || TYPE_CODE (ftype) == TYPE_CODE_METHOD)
	{
#ifdef CONVERT_FROM_FUNC_PTR_ADDR
	  /* FIXME: This is a workaround for the unusual function
	     pointer representation on the RS/6000, see comment
	     in config/rs6000/tm-rs6000.h  */
	  funaddr = CONVERT_FROM_FUNC_PTR_ADDR (funaddr);
#endif
	  value_type = TYPE_TARGET_TYPE (ftype);

/*bindu:
  On IA, the pointer to function points to a stucture of two 64 bit words. 
  First word being the entry address for the function and second word
  the gp value. FIXME: better check!!
*/

#ifdef HP_IA64
      /* funaddr might have come from &symbol and may need swizzle */
      section = find_pc_section (swizzle (funaddr));
      if (!section) 
	funaddr = (CORE_ADDR) read_memory_integer (SWIZZLE(funaddr), 
						     sizeof(CORE_ADDR));
#endif
	}
      else
	value_type = builtin_type_int;
    }
  else if (code == TYPE_CODE_INT)
    {
      /* Handle the case of functions lacking debugging info.
         Their values are characters since their addresses are char */
      if (TYPE_LENGTH (ftype) == 1)
	funaddr = value_as_pointer (value_addr (function));
      else
	/* Handle integer used as address of a function.  */
	funaddr = (CORE_ADDR) value_as_long (function);

      value_type = builtin_type_int;
    }
  else
    error ("Invalid data type for function to be called.");

  *retval_type = value_type;
  return funaddr;
}

/* All this stuff with a dummy frame may seem unnecessarily complicated
   (why not just save registers in GDB?).  The purpose of pushing a dummy
   frame which looks just like a real frame is so that if you call a
   function and then hit a breakpoint (get a signal, etc), "backtrace"
   will look right.  Whether the backtrace needs to actually show the
   stack at the time the inferior function was called is debatable, but
   it certainly needs to not display garbage.  So if you are contemplating
   making dummy frames be different from normal frames, consider that.  */

/* Perform a function call in the inferior.
   ARGS is a vector of values of arguments (NARGS of them).
   FUNCTION is a value, the function to be called.
   Returns a value representing what the function returned.
   May fail to return, if a breakpoint or signal is hit
   during the execution of the function.

   ARGS is modified to contain coerced values. */

/* RM: flag to indicate whether we should restore inferior status when
   (if) the call succeeds. Usually true, but can be turned off for
   internal calls (like find_stub_with_shl_get) on HPPA */
int restore_inferior_status_on_call_completion = 1;

static value_ptr
hand_function_call (value_ptr function, int nargs, value_ptr *args)
{
  register CORE_ADDR sp;
  register CORE_ADDR temp_sp;
  register int i;
  int rc;
  CORE_ADDR start_sp;
  /* CALL_DUMMY is an array of words (REGISTER_SIZE), but each word
     is in host byte order.  Before calling FIX_CALL_DUMMY, we byteswap it
     and remove any extra bytes which might exist because ULONGEST is
     bigger than REGISTER_SIZE.

     NOTE: This is pretty wierd, as the call dummy is actually a
     sequence of instructions.  But CISC machines will have
     to pack the instructions into REGISTER_SIZE units (and
     so will RISC machines for which INSTRUCTION_SIZE is not
     REGISTER_SIZE).

     NOTE: This is pretty stupid.  CALL_DUMMY should be in strict
     target byte order. */
  static ULONGEST *dummy;
  int sizeof_dummy1;
  char *dummy1;
  CORE_ADDR old_sp;
  struct type *value_type;
  unsigned char struct_return;
  CORE_ADDR struct_addr = 0;
  struct inferior_status *inf_status;
  struct cleanup *old_chain;
  CORE_ADDR funaddr;
  int using_gcc;		/* Set to version of gcc in use, or zero if not gcc */
  CORE_ADDR real_pc;
  struct type *param_type = NULL;
  struct type *ftype = check_typedef (SYMBOL_TYPE (function));
  struct symbol *fsym;
  int fortran_function;
  extern int found_endo_export_list;
  extern int endo_missing_in_symfile_objfile; /* JAGaf39799 */
  int callee_nargs;	/* number of args of the identified callee function */
  boolean local_is_ipf_cxx_abi_hand_fn_call;  /* local temp copy */

#ifdef GDB_TARGET_IS_HPPA
#ifndef GDB_TARGET_IS_HPPA_20W
  /* FIXME, 
     While making command-line call, if the inferior's PC is in a.out gdb
     hangs. (possibly expecting an event to occur.. ttrace_wait is waiting
     for an event to occur)
  */
  if ( found_endo_export_list &&
       (!som_solib_get_got_by_pc (target_read_pc (inferior_pid))) )

	error ("Internal Error: Couldn't make comman line call.");
#endif
#endif

  dummy = (ULONGEST *) alloca (SIZEOF_CALL_DUMMY_WORDS);
  sizeof_dummy1 = (int) (REGISTER_SIZE * SIZEOF_CALL_DUMMY_WORDS / sizeof (ULONGEST));
  dummy1 = (char *) alloca (sizeof_dummy1);
  memcpy (dummy, CALL_DUMMY_WORDS, SIZEOF_CALL_DUMMY_WORDS);

  if (!target_has_execution)
    noprocess ();

#ifdef HP_IA64_GAMBIT
  if (hp_ia64_testsuite)
    {
       /* Under testing, silently step up to a bundle boundary */
       pc = read_register (PC_REGNUM);
       while (pc & 0x3)
	 {
	   step_1 (0, 1, 0, NULL);
	   registers_changed ();
	   pc = read_register (PC_REGNUM);
	 }
    }
#endif

  inf_status = save_inferior_status (1);
  old_chain = make_cleanup_restore_inferior_status (inf_status);

  /* PUSH_DUMMY_FRAME is responsible for saving the inferior registers
     (and POP_FRAME for restoring them).  (At least on most machines)
     they are saved on the stack in the inferior.  */
  PUSH_DUMMY_FRAME;

  old_sp = sp = read_sp ();

/* call_dummy_begin is 0 when the dummy frame is first one on the stack */
  if (call_dummy_begin == 0)
    call_dummy_begin = old_sp;

/* On IA64 we donot need to worry about STACK_ALIGN here. Its taken care in 
   push_dummy_frame */
  if (INNER_THAN (1, 2))
    {
      /* Stack grows down */
      sp -= sizeof_dummy1;
      start_sp = sp;
    }
  else
    {
      /* Stack grows up */
#ifdef GDB_TARGET_IS_HPPA_20W
      start_sp = STACK_ALIGN(sp);
#else
      start_sp = sp;
#endif
      sp += sizeof_dummy1;
    }
  call_dummy_end = start_sp;

  funaddr = find_function_addr (function, &value_type);
  fsym = find_pc_function (funaddr);
  fortran_function = fsym && SYMBOL_LANGUAGE (fsym) == language_fortran;
  CHECK_TYPEDEF (value_type);

  {
    struct block *b = block_for_pc (funaddr);
    /* If compiled without -g, assume GCC 2.  */
    using_gcc = (b == NULL ? 2 : BLOCK_GCC_COMPILED (b));
  }

  /* Are we returning a value using a structure return or a normal
     value return? */

  struct_return = using_struct_return (function, funaddr, value_type,
				       using_gcc);

  /* Create a call sequence customized for this function
     and the number of arguments for it.  */

  /* MERGE: memcpy doesn't work for PA32, so use store_unsigned_integer
     to write the stack dummy for non-LRE target */
  if ( !IS_TARGET_LRE )
    for (i = 0; i < (int) (SIZEOF_CALL_DUMMY_WORDS / sizeof (dummy[0])); i++)
      store_unsigned_integer (&dummy1[i * REGISTER_SIZE],
  			      REGISTER_SIZE,
			      (ULONGEST) dummy[i]);
  else
    memcpy (dummy1, dummy, SIZEOF_CALL_DUMMY_WORDS); 

#ifdef GDB_TARGET_IS_HPPA
  real_pc = FIX_CALL_DUMMY (dummy1, start_sp, funaddr, nargs, args,
			    value_type, using_gcc);
#else
  FIX_CALL_DUMMY (dummy1, start_sp, funaddr, nargs, args,
		  value_type, using_gcc);
  real_pc = start_sp;
#endif

  if (CALL_DUMMY_LOCATION == ON_STACK)
    {
      write_memory (start_sp, (char *) dummy1, sizeof_dummy1);
    }

  if (CALL_DUMMY_LOCATION == BEFORE_TEXT_END)
    {
      /* Convex Unix prohibits executing in the stack segment. */
      /* Hope there is empty room at the top of the text segment. */
      extern CORE_ADDR text_end;
      static int checked = 0;
      if (!checked)
	for (start_sp = text_end - sizeof_dummy1; start_sp < text_end; ++start_sp)
	  if (read_memory_integer (start_sp, 1) != 0)
	    error ("text segment full -- no place to put call");
      checked = 1;
      sp = old_sp;
      real_pc = text_end - sizeof_dummy1;
      write_memory (real_pc, (char *) dummy1, sizeof_dummy1);
    }

  if (CALL_DUMMY_LOCATION == AFTER_TEXT_END)
    {
      extern CORE_ADDR text_end;
      int errcode;
      sp = old_sp;
#ifdef AFTER_TEXT_END_OFFSET
      real_pc = text_end + AFTER_TEXT_END_OFFSET;
#else
      real_pc = text_end;
#endif
      errcode = target_write_memory (real_pc, (char *) dummy1, sizeof_dummy1);
      if (errcode != 0)
	error ("Cannot write text segment -- call_function failed");
    }

  if (CALL_DUMMY_LOCATION == AT_ENTRY_POINT)
    {
      real_pc = funaddr;
    }

#ifdef GDB_TARGET_IS_HPPA_20W
  if (CALL_DUMMY_LOCATION == AT_WDB_CALL_DUMMY)
    {
     /* JAGaf39799 -
        Baskar, gdb64 to support command-line calls in a.out not linked
	with end.o.

	I'm trying to construct the dummy-call in STACK segment
	instead of using __wdb_call_dummy.
     */
     if (endo_missing_in_symfile_objfile)
       {
        CORE_ADDR pcsqh, pcsqt, pcoqh, pcoqt, r1;
        char buf[4];
        int inst1, inst2;
        int status;
        struct target_waitstatus tgt_wait;

	/* Push call-dmmy instructions into stack */
        real_pc = start_sp;
        write_memory (start_sp, (char *) dummy1, sizeof_dummy1);
        wdb_call_dummy_start_addr = start_sp;
        wdb_call_dummy_avail_addr = start_sp + sizeof_dummy1;

        /* We are going to push the call dummy instructions into stack,
           not in __wdb_call_dummy.

	   Stack is in 3Q, and we have set the PCSQH to point to SR6, before
	   transfering the control to call-dummy (which is in stack).

	   We cannot set the space queues drectly, so we will exec "BVE"
	   in inferiors process space to set PCSQ to point the call dummy
	   in stack.

                Save contents of pcoqh & pcoqt
                Set r1 -> start_sp (start of call-dummy)
                Push "BVE,r1 & NOP" into pcoqh & pcoqt location
                Exec those instructions so that the PCSQ will point to stack segment
                Restore contents of pcoqh & pcoqt
        */
	
        /* Read in offset queues */
        pcoqh = read_register (PCOQ_HEAD_REGNUM);
        pcoqt = read_register (PCOQ_TAIL_REGNUM);

        /* Read in instructions from pc location */
        if (target_read_memory (pcoqh, buf, 4) != 0)
          error ("Couldn't modify pc space queue");
        inst1 = extract_unsigned_integer (buf, 4);

        if (target_read_memory (pcoqt, buf, 4) != 0)
          error ("Couldn't modify pc space queue");
        inst2 = extract_unsigned_integer (buf, 4);

        /* Use BVE,r1 instruction to set PCSQH to point to stack */
        *((int *)buf) = 0xe820d000;
        if (target_write_memory (pcoqh, buf, 4) != 0)
          error ("Couldn't modify pc space queue");

        /* NOP instruction..... */
        *((int *)buf) = 0xe8000240;
        if (target_write_memory (pcoqt, buf, 4) != 0)
          {
            *((int *)buf) = inst1;
            target_write_memory (pcoqh, buf, 4);
            error ("Couldn't modify pc space queue");
          }

        /* set r1 -> start_sp (start of call dummy frame in stack */
	r1 = read_register(1);
        write_register (1, start_sp);

        /* Execute BVE & NOP instruction so that the space queues
           points to the stack segment (call dummy) */

        resume (1,0);
        target_wait (inferior_pid, &tgt_wait);
        registers_changed ();
        resume (1,0);
        target_wait (inferior_pid, &tgt_wait);
        registers_changed ();

        /* Restore the original instructions... */
        *((int *)buf) = inst1;
        target_write_memory (pcoqh, buf, 4);
        *((int *)buf) = inst2;
        target_write_memory (pcoqt, buf, 4);
	write_register(1, r1);
       }
     else /* Normal executable linked with end.o */
     {
      /* Write to __wdb_call_dummy (in end.o) which contains enough nops for
	 call_dummy. */
      int errcode;
      struct minimal_symbol * wdb_call_dummy_sym;
      sp = old_sp;
      wdb_call_dummy_sym = lookup_minimal_symbol ("__wdb_call_dummy", NULL,
						  NULL);

      /* Baskar, stripped exec .... */
      if (!wdb_call_dummy_sym)
	{
	  ExportEntry *export_entry = 0;
	  export_entry = find_symbol_in_export_list ("__wdb_call_dummy", NULL);	
	  if (export_entry)
	    real_pc = (CORE_ADDR)export_entry->address;
	  else
	    error ("Cannot find __wdb_call_dummy in /opt/langtools/lib/end.o. "
	    	   "Call function failed.\nHint : Try linking /opt/langtools/lib/end.o with your application.");
	}
      else
	real_pc = SYMBOL_VALUE_ADDRESS (wdb_call_dummy_sym);
	
       
        {
	  errcode = target_write_memory (real_pc, (char *)dummy1,
					 sizeof_dummy1);
	  if (errcode)
	    error ("Cannot write to __wdb_call_dummy -- call function failed");
	  else
            {
	      wdb_call_dummy_start_addr = real_pc;
	      wdb_call_dummy_avail_addr = real_pc + sizeof_dummy1;
	    }
	}
      }
    }
  else
    error ("Cannot find __wdb_call_dummy in /opt/langtools/lib/end.o. "
	       "Call function failed.\nHint : Try linking /opt/langtools/lib/end.o with your application.");
#endif

#ifdef lint
  sp = old_sp;			/* It really is used, for some ifdef's... */
#endif

#ifdef GDB_TARGET_IS_HPPA_20W
  sp = STACK_ALIGN (sp);
#endif

  callee_nargs = TYPE_NFIELDS(ftype);
  if (is_ipf_cxx_abi_hand_fn_call)
    callee_nargs--;
  if (nargs < callee_nargs)
    error ("too few arguments in function call");

  if (fortran_function)
    {
      if (nargs > callee_nargs)
	error ("too many arguments in function call");

      for (i = 0; i < nargs; i++)
	{
	  /* Check for arrays with hidden parameters or array descriptors */
	  param_type = TYPE_FIELD_TYPE (ftype, i);
	  if (TYPE_CODE (param_type) == TYPE_CODE_ARRAY &&
	      TYPE_ARRAY_LOWER_BOUND_TYPE (param_type) != BOUND_SIMPLE)
	    error ("cannot call Fortran function with hidden array parameters");
	}
    }

  for (i = nargs - 1; i >= 0; i--)
    {
      /* If we're off the end of the known arguments, do the standard
         promotions.  FIXME: if we had a prototype, this should only
         be allowed if ... were present.  */
      if (i >= callee_nargs)
	args[i] = value_arg_coerce (args[i], NULL, 0);

      else
	{
	  /* PA debug info never sets TYPE_FLAG_PROTOTYPED, so fix
	     JAGag15817, "Infinite loop while trying to call a method from
	     wdb" by adding the observation that all functions in C++ have
	     prototypes.  */
	  int is_prototyped =
	    ((TYPE_FLAGS (ftype) & TYPE_FLAG_PROTOTYPED) || 
	     (SYMBOL_LANGUAGE (fsym) == language_cplus));

	  struct type *arg_type = check_typedef (VALUE_TYPE (args[i]));

	  param_type = TYPE_FIELD_TYPE (ftype, i);

	  /* If we have a prototype, cast as for assignment */
	  if (is_prototyped)
	    {
	      /* If the debug info says the callee expects this param to
		 be passed by reference and the type otherwise matches,
		 just do that for the user.  Note that this will accept
		 cases previously flagged as an error, like passing
		 an object to a callee declared as receiving a pointer
		 to that class.  I suspect users will forgive us.  Carl
		 Burch for JAGaf18670 "Debugger call to class by value
		 function doesn't work", 05/12/06. 
		 Added test for lval_memory for JAGag15817, "Infinite loop
		 while trying to call a method from wdb", since we probably
		 don't want to do this pass-by-reference coercion for values
		 that aren't in memory.  An uncasted argument of zero passed
		 to a pointer formal was trying to be coerced to pass it by
		 reference.  Carl Burch, 04/20/2007.  */
	      if (((TYPE_CODE (param_type) == TYPE_CODE_REF) || 
		   (TYPE_CODE (param_type) == TYPE_CODE_PTR)) &&
		  (VALUE_LVAL (args[i]) == lval_memory) &&
	          (TYPE_CODE (check_typedef (TYPE_TARGET_TYPE (param_type))) ==
		   TYPE_CODE (arg_type)))
	        args[i] = value_arg_coerce (args[i], param_type, is_prototyped);
	      else
	        args[i] = value_cast (param_type, args[i]);
	    }

          /* Not for a lval_memory argument for a Fortran call where
	     we need to pass the address of the argument.
	     Note that for a non-lval_memory argument in a Fortran call,
	     we promote it here and below we arrange to pass the address
	     of the value. */
          else if ((!fortran_function  ||
                   fortran_function && VALUE_LVAL (args[i]) != lval_memory) &&
                   (i != (TYPE_RETVAL_PTR_IDX(ftype))))
	    args[i] = value_arg_coerce (args[i], param_type, is_prototyped);
	}

      /*elz: this code is to handle the case in which the function to be called
         has a pointer to function as parameter and the corresponding actual argument
         is the address of a function and not a pointer to function variable.
         In aCC compiled code, the calls through pointers to functions (in the body
         of the function called by hand) are made via $$dyncall_external which
         requires some registers setting, this is taken care of if we call
         via a function pointer variable, but not via a function address.
         In cc this is not a problem. */

      if (using_gcc == HP_COMPILED_TARGET)
	if (param_type)
	  /* if this parameter is a pointer to function */
	  if (TYPE_CODE (param_type) == TYPE_CODE_PTR)
	    if (TYPE_CODE (param_type->target_type) == TYPE_CODE_FUNC)
	      /* elz: FIXME here should go the test about the compiler used
	         to compile the target. We want to issue the error
	         message only if the compiler used was HP's aCC.
	         If we used HP's cc, then there is no problem and no need
	         to return at this point */
	      if (using_gcc == HP_COMPILED_TARGET)	/* && compiler == aCC */
		/* go see if the actual parameter is a variable of type
		   pointer to function or just a function */
		if (args[i]->lval == not_lval)
		  {
		    char *arg_name;
		    boolean is_function;

		    if (is_swizzled)
		      is_function = 
			   find_pc_partial_function (
				(sizeof(CORE_ADDR) == 8) ?
				(args[i]->aligner.contents[0] >> 31) :
				args[i]->aligner.contents[0],
				&arg_name, 
				NULL, 
				NULL) 
			!= NULL;
		    else
		      is_function = 
			   find_pc_partial_function (
			       (CORE_ADDR) args[i]->aligner.contents[0], 
			       &arg_name, 
			       NULL, 
			       NULL)
		        != NULL;
		    if (is_function)
		      error ("\
You cannot use function <%s> as argument. \n\
You must use a pointer to function type variable. Command ignored.", arg_name);
		  }
    }
  if (fortran_function)
    /* All parameters in Fortran are pass by reference so we need to
     * pass the address of the thing we're passing, not the value
     */
  {
#ifdef LOG_BETA_F90_FUNCTION
    log_test_event (LOG_BETA_F90_FUNCTION, 1);
#endif
    for (i = nargs - 1; i >= 0; i--)
      {
        struct type *arg_type = check_typedef (VALUE_TYPE (args[i]));
        CORE_ADDR addr, copyaddr;
        int len; 
        int aligned_len;
        /*If the value is an address, pass the address as the value. 
          If the value is not an address, copy it to the stack 
          and pass the address of what we pushed  as the value */
        if (VALUE_LVAL (args[i]) == lval_memory)
          args[i] = value_addr (args[i]);
        else
          {
            len = TYPE_LENGTH (arg_type);
#ifdef STACK_ALIGN
            aligned_len = STACK_ALIGN (len);
#else
            aligned_len = len;
#endif
#if !(INNER_THAN(1, 2))
            /* The stack grows up, so the address of the thing we push
               is the stack pointer before we push it.  */
            addr = sp;
#else
            sp -= aligned_len;
#endif
            /* Push the contents of the value  */
            write_memory (sp, VALUE_CONTENTS_ALL (args[i]), len);
#if INNER_THAN(1,2)
          /* The stack grows down, so the address of the thing we push
             is the stack pointer after we push it.  */
            addr = sp;
#else
            sp += aligned_len;
#endif
            /* The value we're going to pass is the address of the thing
               we just pushed */
            args[i] = value_from_longest (lookup_pointer_type (arg_type),
                                          (LONGEST) addr);
          }
      }
  }
  else if (REG_STRUCT_HAS_ADDR_P ())
    {
      /* This is a machine like the sparc, where we may need to pass a
	 pointer to the structure, not the structure itself.  */
      for (i = nargs - 1; i >= 0; i--)
	{
	  struct type *arg_type = check_typedef (VALUE_TYPE (args[i]));
	  if ((TYPE_CODE (arg_type) == TYPE_CODE_STRUCT
	       || TYPE_CODE (arg_type) == TYPE_CODE_CLASS
	       || TYPE_CODE (arg_type) == TYPE_CODE_UNION
	       || TYPE_CODE (arg_type) == TYPE_CODE_ARRAY
	       || TYPE_CODE (arg_type) == TYPE_CODE_STRING
	       || TYPE_CODE (arg_type) == TYPE_CODE_BITSTRING
	       || TYPE_CODE (arg_type) == TYPE_CODE_SET
	       || (TYPE_CODE (arg_type) == TYPE_CODE_FLT
		   && TYPE_LENGTH (arg_type) > 8)
	       )
	      && REG_STRUCT_HAS_ADDR (using_gcc, arg_type))
	    {
	      CORE_ADDR addr = 0; /* initialize for compiler warning */
	      int len;		/*  = TYPE_LENGTH (arg_type); */
	      int aligned_len;
	      arg_type = check_typedef (VALUE_ENCLOSING_TYPE (args[i]));
	      len = TYPE_LENGTH (arg_type);

	      if (STACK_ALIGN_P ())
		/* MVS 11/22/96: I think at least some of this
		   stack_align code is really broken.  Better to let
		   PUSH_ARGUMENTS adjust the stack in a target-defined
		   manner.  */
		aligned_len = STACK_ALIGN (len);
	      else
		aligned_len = len;
	      if (INNER_THAN (1, 2))
		{
		  /* stack grows downward */
		  sp -= aligned_len;
		}
	      else
		{
		  /* The stack grows up, so the address of the thing
		     we push is the stack pointer before we push it.  */
		  addr = sp;
		}
	      /* Push the structure.  */
	      write_memory (sp, VALUE_CONTENTS_ALL (args[i]), len);
	      if (INNER_THAN (1, 2))
		{
		  /* The stack grows down, so the address of the thing
		     we push is the stack pointer after we push it.  */
		  addr = sp;
		}
	      else
		{
		  /* stack grows upward */
		  sp += aligned_len;
		}
	      /* The value we're going to pass is the address of the
		 thing we just pushed.  */
	      /*args[i] = value_from_longest (lookup_pointer_type (value_type),
		(LONGEST) addr); */
	      args[i] = value_from_pointer (lookup_pointer_type (arg_type),
					    addr);
	    }
	}
    }

  /* Reserve space for the return structure to be written on the
     stack, if necessary */

  if (struct_return || is_ipf_cxx_abi_hand_fn_call)
    {
      int len = TYPE_LENGTH (value_type);
      if (STACK_ALIGN_P ())
	/* MVS 11/22/96: I think at least some of this stack_align
	   code is really broken.  Better to let PUSH_ARGUMENTS adjust
	   the stack in a target-defined manner.  */
	len = STACK_ALIGN (len);
      if (INNER_THAN (1, 2))
	{
	  /* stack grows downward */
	  sp -= len;
	  struct_addr = sp;
	}
      else
	{
	  /* stack grows upward */
	  struct_addr = sp;
	  sp += len;
	}
    if (is_ipf_cxx_abi_hand_fn_call)
      {
        /* make room for object return pointer in the arg list by copying
	   the array to a one-longer temp array and then updating "arg" 
	   to point at the temp. */
        value_ptr *temp_args = (value_ptr *) 
				alloca ((nargs+1) * sizeof(value_ptr));
	memcpy (&temp_args[1], args, (nargs * sizeof(value_ptr)));
	nargs++;
	temp_args[0] = value_from_pointer (lookup_pointer_type (ftype),
					    struct_addr);
	args = temp_args;
      }
    }

/* elz: on HPPA no need for this extra alignment, maybe it is needed
   on other architectures. This is because all the alignment is taken care
   of in the above code (ifdef REG_STRUCT_HAS_ADDR) and in
   hppa_push_arguments */
/* RM: same for IA64 */
#ifndef NO_EXTRA_ALIGNMENT_NEEDED

  /* MVS 11/22/96: I think at least some of this stack_align code is
     really broken.  Better to let PUSH_ARGUMENTS adjust the stack in
     a target-defined manner.  */
  if (STACK_ALIGN_P () && INNER_THAN (1, 2))
    {
      /* If stack grows down, we must leave a hole at the top. */
      int len = 0;

      for (i = nargs - 1; i >= 0; i--)
	len += TYPE_LENGTH (VALUE_ENCLOSING_TYPE (args[i]));
      if (CALL_DUMMY_STACK_ADJUST_P)
	len += CALL_DUMMY_STACK_ADJUST;
      sp -= STACK_ALIGN (len) - len;
    }
#endif /* NO_EXTRA_ALIGNMENT_NEEDED */

#ifdef HP_IA64
  /* Check the function type to know how it is expecting aggregates
     if pass_by_addr is set then it expects aggregate address.
     Set pass_by_addr for all aggregate parameters if pass_by_addr 
     is set for the function.
     pass_by_addr information of an aggregate is used by 
     ia64_push_arguments to decide how it should pass this aggregate.
  */
     struct type * func_type = check_typedef (VALUE_TYPE (function));
     for (i = nargs - 1; i >= 0; i--)
      {
        struct type *arg_type = check_typedef (VALUE_TYPE (args[i]));
        if (TYPE_CODE (arg_type) == TYPE_CODE_STRUCT
            || TYPE_CODE (arg_type) == TYPE_CODE_CLASS
            || TYPE_CODE (arg_type) == TYPE_CODE_UNION)
         {
           if (func_type->pass_by_addr == 1)
	    arg_type->pass_by_addr = 1;
           else
            arg_type->pass_by_addr = 0;
         }
      }
#endif

  temp_sp = sp;
  sp = PUSH_ARGUMENTS (nargs, args, temp_sp, struct_return, struct_addr);

#ifdef PUSH_RETURN_ADDRESS	/* for targets that use no CALL_DUMMY */
  /* There are a number of targets now which actually don't write any
     CALL_DUMMY instructions into the target, but instead just save the
     machine state, push the arguments, and jump directly to the callee
     function.  Since this doesn't actually involve executing a JSR/BSR
     instruction, the return address must be set up by hand, either by
     pushing onto the stack or copying into a return-address register
     as appropriate.  Formerly this has been done in PUSH_ARGUMENTS,
     but that's overloading its functionality a bit, so I'm making it
     explicit to do it here.  */
  sp = PUSH_RETURN_ADDRESS (real_pc, sp);
#endif /* PUSH_RETURN_ADDRESS */

  if (STACK_ALIGN_P () && !INNER_THAN (1, 2))
    {
      /* If stack grows up, we must leave a hole at the bottom, note
         that sp already has been advanced for the arguments!  */
      if (CALL_DUMMY_STACK_ADJUST_P)
	sp += (CORE_ADDR) (unsigned) CALL_DUMMY_STACK_ADJUST;
      sp = (CORE_ADDR) STACK_ALIGN ((signed long long) sp);
    }

/* XXX This seems wrong.  For stacks that grow down we shouldn't do
   anything here!  */
  /* MVS 11/22/96: I think at least some of this stack_align code is
     really broken.  Better to let PUSH_ARGUMENTS adjust the stack in
     a target-defined manner.  */
  if (CALL_DUMMY_STACK_ADJUST_P)
    if (INNER_THAN (1, 2))
      {
	/* stack grows downward */
	sp -= CALL_DUMMY_STACK_ADJUST;
      }

  /* Store the address at which the structure is supposed to be
     written.  Note that this (and the code which reserved the space
     above) assumes that gcc was used to compile this function.  Since
     it doesn't cost us anything but space and if the function is pcc
     it will ignore this value, we will make that assumption.

     Also note that on some machines (like the sparc) pcc uses a
     convention like gcc's.  */

  if (struct_return)
    STORE_STRUCT_RETURN (struct_addr, sp);

  /* Write the stack pointer.  This is here because the statements above
     might fool with it.  On SPARC, this write also stores the register
     window into the right place in the new stack frame, which otherwise
     wouldn't happen.  (See store_inferior_registers in sparc-nat.c.)  */
  write_sp (sp);

  if (SAVE_DUMMY_FRAME_TOS_P ())
    SAVE_DUMMY_FRAME_TOS (sp);

  {
    char retbuf[REGISTER_BYTES];
    char *name;
    struct symbol *symbol;

    name = NULL;
    symbol = find_pc_function (funaddr);
    if (symbol)
      {
	name = SYMBOL_SOURCE_NAME (symbol);
      }
    else
      {
	/* Try the minimal symbols.  */
	struct minimal_symbol *msymbol = lookup_minimal_symbol_by_pc (funaddr);

	if (msymbol)
	  {
	    name = SYMBOL_SOURCE_NAME (msymbol);
	  }
      }
    if (name == NULL)
      {
	name = (char *) alloca (80);
        sprintf (name, "at %s", longest_local_hex_string ((LONGEST) funaddr));
      }

    /* Execute the stack dummy routine, calling FUNCTION.
       When it is done, discard the empty frame
       after storing the contents of all regs into retbuf.  */
    /* Please make sure that you store correct inferior_pid by 
       reading it before you call run_stack_dummy() this is because
       run_stack_dummy calls proceed, so inferior_may get switch.
       This inturn changes inferior_pid to other thread and that 
       is not the the one on which we are running this dummy func().
       so store the current thread_pid in CLC_thread_pid & use it below.
     */ 
    int CLC_thread_pid = inferior_pid;
    rc = run_stack_dummy (real_pc + CALL_DUMMY_START_OFFSET, retbuf);
#ifdef HP_IA64
    if (rc && !unwind_on_signal_p)
     {
       /* For more info see: QXCR1000577394:Bogus SIGTRAP when command-
          line call on non-current thread'
          
          Once if we are in this block of code, means that CLC issued 
          to the thread failed and we have to capture relevant details
          of the CLC and use it when the thread finishes exectuing the 
	  called function. 
	  gdb use the "command_line_call" structure (defined in gdbcmd.h)
	  to store important values of the CLC.
   	  Here gdb gathers all required details, such as thread ID,
	  return type, function name, function parameters value,
          and many other values.
	  The key attribute to distinguish each thread is it dummy
          breakpoint address of the CLC.
	  
	  The dummy break point address of the failed CLC is captured
          "dummy_break_addr" (in gdbcmd.c) and stored into the trap_CLC
          structure (of type structure command_line_call).
	  This trap_CLC is a chain of all failed CLCs.
       
          When the process is resumed and the thread hits its dummy break
          point at the end of the dummy function, it enters normal_stop()
          in infrun.c. 
          In normal_stop() function gdb checks, whether the thread which hit the 
          dummy breakpoint is in the list of failed CLC threads, by 
	  comparing the dummy breakpoint address of the thread against 
          the dummy breakpoint addresses of all failed CLC threads in the
          trap_CLC chain (list of failed CLC threads)
  
          If gdb finds any failed CLC thread's dummy break point matching 
          the current inferior thread's stop_pc (dummy breakpoint hit addr)
          then, stored data values of that failed CLC thread is read from
          trap_CLC structer and used to print the return type.

          The return values are read from the registers. Just after hitting
          the dummy breakpoint and before popping out the dummy frame from the 
   	  process (thread) stack. */

       cmd_line_call_failed_cnt = cmd_line_call_failed_cnt + 1;
       struct command_line_call *temp = 
	(struct command_line_call *) xmalloc (sizeof (struct command_line_call));
       bzero (temp, sizeof (struct command_line_call));
       temp->trap_dummy_frame_brk_addr = dummy_break_addr;
       temp->cmd_line_call_failed_pid = CLC_thread_pid;
       temp->trap_struct_return = struct_return;
       temp->trap_struct_addr = struct_addr;
       temp->trap_type = value_type;
#ifdef VALUE_RETURNED_FROM_STACK
       temp->trap_is_ipf_cxx_abi_hand_fn_call = false;
       if (struct_return || is_ipf_cxx_abi_hand_fn_call)
        {  
          temp->trap_is_ipf_cxx_abi_hand_fn_call = is_ipf_cxx_abi_hand_fn_call;
        }

        /* RM: check for retval ptr argument */
       if (TYPE_RETVAL_PTR_IDX(ftype) != -1)
        {
          CORE_ADDR addr = value_as_pointer (args[TYPE_RETVAL_PTR_IDX(ftype)]);
          if (is_swizzled)
          temp->trap_retval_addr = SWIZZLE (addr);
        }   
#endif
       /* Store the function name called in CLC */
       temp->func_name = strdup (name);
      
       /* Allocate memory for array of pointers (struct value **) to store
          args value_ptr[s] */
       temp->args_array = (value_ptr *) xmalloc (sizeof (value_ptr) * (nargs + 1));
       /* Fill the "args_array" array pointing to value_ptr of arguments */
       int i =0; 
       for (; i < nargs; i++)
         temp->args_array [i] = value_copy (args [i]);
       /* Mark the end using NULL */
       temp->args_array [i] = NULL;

       if (trap_CLC == NULL)
        {
	  trap_CLC = temp;
	  trap_CLC->next = NULL;
        }
       else
        {
	  temp->next = trap_CLC; 
          trap_CLC = temp; 
	}
     }
#endif
    if (rc == 1)
      {
	/* We stopped inside the FUNCTION because of a random signal.
	   Further execution of the FUNCTION is not allowed. */

        if (unwind_on_signal_p)
	  {
	    /* The user wants the context restored. */

            /* We must get back to the frame we were before the dummy call. */
            POP_FRAME;

	    /* FIXME: Insert a bunch of wrap_here; name can be very long if it's
	       a C++ name with arguments and stuff.  */
	    error ("\
The program being debugged was signaled while in a function called from GDB.\n\
GDB has restored the context to what it was before the call.\n\
To change this behavior use \"set unwindonsignal off\"\n\
Evaluation of the expression containing the function (%s) will be abandoned.",
		   name);
	  }
	else
	  {
	    /* The user wants to stay in the frame where we stopped (default).*/

	    /* If we did the cleanups, we would print a spurious error
	       message (Unable to restore previously selected frame),
	       would write the registers from the inf_status (which is
	       wrong), and would do other wrong things.  */
	    discard_cleanups (old_chain);
	    discard_inferior_status (inf_status);

	    /* FIXME: Insert a bunch of wrap_here; name can be very long if it's
	       a C++ name with arguments and stuff.  */
	    error ("\
The program being debugged was signaled while in a function called from GDB.\n\
GDB remains in the frame where the signal was received.\n\
To change this behavior use \"set unwindonsignal on\"\n\
Evaluation of the expression containing the function (%s) will be abandoned.",
		   name);
	  }
      }

    if (rc == 2)
      {
	/* We hit a breakpoint inside the FUNCTION. */

	/* If we did the cleanups, we would print a spurious error
	   message (Unable to restore previously selected frame),
	   would write the registers from the inf_status (which is
	   wrong), and would do other wrong things.  */
	discard_cleanups (old_chain);
	discard_inferior_status (inf_status);

	/* The following error message used to say "The expression
	   which contained the function call has been discarded."  It
	   is a hard concept to explain in a few words.  Ideally, GDB
	   would be able to resume evaluation of the expression when
	   the function finally is done executing.  Perhaps someday
	   this will be implemented (it would not be easy).  */

	/* FIXME: Insert a bunch of wrap_here; name can be very long if it's
	   a C++ name with arguments and stuff.  */
	error ("\
The program being debugged stopped while in a function called from GDB.\n\
When the function (%s) is done executing, GDB will silently\n\
stop (instead of continuing to evaluate the expression containing\n\
the function call).", name);
      }

/* restore the call dummy */
#if INNER_THAN (1, 2) /*stack grows down*/
    call_dummy_end += sizeof_dummy1;
#else
    call_dummy_end -= sizeof_dummy1;
#endif
    if (call_dummy_begin == call_dummy_end) 
      {
        call_dummy_begin = 0;
        call_dummy_end = 0;
      }


    /* If we get here the called FUNCTION run to completion. */
    if (restore_inferior_status_on_call_completion)
      do_cleanups (old_chain);
    else
      {
        discard_cleanups (old_chain);
	discard_inferior_status (inf_status);	
      }

    /* Figure out the value returned by the function.  */
/* elz: I defined this new macro for the hppa architecture only.
   this gives us a way to get the value returned by the function from the stack,
   at the same address we told the function to put it.
   We cannot assume on the pa that r28 still contains the address of the returned
   structure. Usually this will be overwritten by the callee.
   I don't know about other architectures, so I defined this macro
 */

    /* reset for next hand function call */
    local_is_ipf_cxx_abi_hand_fn_call = is_ipf_cxx_abi_hand_fn_call;
    is_ipf_cxx_abi_hand_fn_call = false;

#ifdef VALUE_RETURNED_FROM_STACK
    if (struct_return || local_is_ipf_cxx_abi_hand_fn_call)
      {
        return (value_ptr) VALUE_RETURNED_FROM_STACK (value_type, struct_addr);
      }

    /* RM: check for retval ptr argument */
    if (TYPE_RETVAL_PTR_IDX(ftype) != -1)
      {
	CORE_ADDR addr = value_as_pointer (args[TYPE_RETVAL_PTR_IDX(ftype)]);
	if (is_swizzled)
  	    addr = SWIZZLE (addr);
	return (value_ptr) VALUE_RETURNED_FROM_STACK (value_type, addr);
      }
#endif

    return value_being_returned (value_type, retbuf, struct_return);
  }
}

value_ptr
call_function_by_hand (value_ptr function, int nargs, value_ptr *args)
{
  value_ptr ret_val = NULL;
  extern int function_calls_disallowed;
  /* HP-UX IPF ttrace doesn't allow function calls in a syscall context */
  /* JAGaf04443 - Applicable for PA also. */
  if (cur_thread_in_syscall())
    {
      error (
	"You may not execute a function while a system call is interrupted.");
    }

  /* Srikanth, we should NEVER EVER call a function when the GUI is trying to
     evaluate expressions (dwell).
  */
  if (function_calls_disallowed)
    error ("Evaluation involves a call (not done.)");

  /* JAGaf26681 - Command line calls cannot be done when stopped in the shared library space 
     and if the shared libraries are not mapped private */
#ifdef GDB_TARGET_IS_HPPA
  if (target_has_execution && attach_flag
                           && !is_solib_mapped_private ()
#ifdef GDB_TARGET_IS_HPPA_20W
                           && pa64_solib_address (read_register (PC_REGNUM))
#else
                           && som_solib_address (read_register (PC_REGNUM))
#endif
                           )
    {
	if (batch_rtc)
		error ("To run Batch RTC,\nDebug enable the executable using"
		       "\n\t\"chatr +dbg enable <executable>\" or \n\t\""
		       "/opt/langtools/bin/pxdb -s on <executable>\".\n");
	else
        	error ("Cannot make command-line call from shared library space;\n"
                        "To be able to make the call;\nDebug enable the executable "
                        "using\n\t\"chatr +dbg enable <executable> \" or \n\t\""
                        "/opt/langtools/bin/pxdb -s on <executable>  \".\n");
    }
#endif

  if (CALL_DUMMY_P)
    {
      cmd_line_call_cnt++;
      ret_val =  hand_function_call (function, nargs, args);
      cmd_line_call_cnt--;
      return ret_val;
    }
  else
    {
      error ("Cannot invoke functions on this machine.");
    }
  /* silences aCC6 warning message about missing return statement */
  return NULL;
}



/* Create a value for an array by allocating space in the inferior, copying
   the data into that space, and then setting up an array value.

   The array bounds are set from LOWBOUND and HIGHBOUND, and the array is
   populated from the values passed in ELEMVEC.

   The element type of the array is inherited from the type of the
   first element, and all elements must have the same size (though we
   don't currently enforce any restriction on their types). */

value_ptr
value_array (int lowbound, int highbound, value_ptr *elemvec)
{
  int nelem;
  int idx;
  unsigned int typelength;
  value_ptr val;
  struct type *rangetype;
  struct type *arraytype;
  CORE_ADDR addr;

  /* Validate that the bounds are reasonable and that each of the elements
     have the same size. */

  nelem = highbound - lowbound + 1;
  if (nelem <= 0)
    {
      error ("bad array bounds (%d, %d)", lowbound, highbound);
    }
  typelength = TYPE_LENGTH (VALUE_ENCLOSING_TYPE (elemvec[0]));
  for (idx = 1; idx < nelem; idx++)
    {
      if (TYPE_LENGTH (VALUE_ENCLOSING_TYPE (elemvec[idx])) != typelength)
	{
	  error ("array elements must all be the same size");
	}
    }

  rangetype = create_range_type ((struct type *) NULL, builtin_type_int,
				 lowbound, highbound);
  arraytype = create_array_type ((struct type *) NULL,
			      VALUE_ENCLOSING_TYPE (elemvec[0]), rangetype);

  if (!current_language->c_style_arrays)
    {
      val = allocate_value (arraytype);
      for (idx = 0; idx < nelem; idx++)
	{
	  memcpy (VALUE_CONTENTS_ALL_RAW (val) + (idx * typelength),
		  VALUE_CONTENTS_ALL (elemvec[idx]),
		  typelength);
	}
      VALUE_BFD_SECTION (val) = VALUE_BFD_SECTION (elemvec[0]);
      return val;
    }

  /* Allocate space to store the array in the inferior, and then initialize
     it by copying in each element.  FIXME:  Is it worth it to create a
     local buffer in which to collect each value and then write all the
     bytes in one operation? */

  addr = allocate_space_in_inferior (nelem * typelength);
  for (idx = 0; idx < nelem; idx++)
    {
      write_memory (addr + (idx * typelength), VALUE_CONTENTS_ALL (elemvec[idx]),
		    typelength);
    }

  /* Create the array type and set up an array value to be evaluated lazily. */

  val = value_at_lazy (arraytype, addr, VALUE_BFD_SECTION (elemvec[0]));
  return (val);
}

/* Create a value for a string constant by allocating space in the inferior,
   copying the data into that space, and returning the address with type
   TYPE_CODE_STRING.  PTR points to the string constant data; LEN is number
   of characters.
   Note that string types are like array of char types with a lower bound of
   zero and an upper bound of LEN - 1.  Also note that the string may contain
   embedded null bytes. */

value_ptr
value_string (char *ptr, int len)
{
  value_ptr val;
  int lowbound = current_language->string_lower_bound;
  struct type *rangetype = create_range_type ((struct type *) NULL,
					      builtin_type_int,
					      lowbound, len + lowbound - 1);
  struct type *stringtype
  = create_string_type ((struct type *) NULL, rangetype);
  CORE_ADDR addr;

  if (current_language->c_style_arrays == 0)
    {
      val = allocate_value (stringtype);
      memcpy (VALUE_CONTENTS_RAW (val), ptr, len);
      return val;
    }


  /* Allocate space to store the string in the inferior, and then
     copy LEN bytes from PTR in gdb to that address in the inferior. */

  addr = allocate_space_in_inferior (len);
  write_memory (addr, ptr, len);

  val = value_at_lazy (stringtype, addr, NULL);
  return (val);
}

value_ptr
value_bitstring (char *ptr, int len)
{
  value_ptr val;
  struct type *domain_type = create_range_type (NULL, builtin_type_int,
						0, len - 1);
  struct type *type = create_set_type ((struct type *) NULL, domain_type);
  TYPE_CODE (type) = TYPE_CODE_BITSTRING;
  val = allocate_value (type);
  memcpy (VALUE_CONTENTS_RAW (val), ptr, TYPE_LENGTH (type));
  return val;
}

/* See if we can pass arguments in T2 to a function which takes arguments
   of types T1.  Both t1 and t2 are NULL-terminated vectors.  If some
   arguments need coercion of some sort, then the coerced values are written
   into T2.  Return value is 0 if the arguments could be matched, or the
   position at which they differ if not.

   STATICP is nonzero if the T1 argument list came from a
   static member function.

   For non-static member functions, we ignore the first argument,
   which is the type of the instance variable.  This is because we want
   to handle calls with objects from derived classes.  This is not
   entirely correct: we should actually check to make sure that a
   requested operation is type secure, shouldn't we?  FIXME.  */

static int
typecmp (int staticp, struct type *t1[], value_ptr t2[])
{
  int i;

  /* Should not we return 0 when there is no arg. I.e when t2 is NULL.
     This is required when the user want to print address of a method.
     like "(gdb) print class_ptr->my_method".
     where class_ptr is pointer to an object of class type "A" 
     and my_method is method in the class "A".
  */
  if (t2 == 0)
    return 0;
  if (staticp && t1 == 0)
    return t2[1] != 0;
  if (t1 == 0)
    return 1;
  if (TYPE_CODE (t1[0]) == TYPE_CODE_VOID)
    return 0;
  if (t1[!staticp] == 0)
    return 0;
  for (i = !staticp; t1[i] && TYPE_CODE (t1[i]) != TYPE_CODE_VOID; i++)
    {
      struct type *tt1, *tt2;
      if (!t2[i])
	return i + 1;
      tt1 = check_typedef (t1[i]);
      tt2 = check_typedef (VALUE_TYPE (t2[i]));
      if (TYPE_CODE (tt1) == TYPE_CODE_REF
      /* We should be doing hairy argument matching, as below.  */
	  && (TYPE_CODE (check_typedef (TYPE_TARGET_TYPE (tt1))) == TYPE_CODE (tt2)))
	{
	  if (TYPE_CODE (tt2) == TYPE_CODE_ARRAY)
	    t2[i] = value_coerce_array (t2[i]);
	  else
	    t2[i] = value_addr (t2[i]);
	  continue;
	}

      while (TYPE_CODE (tt1) == TYPE_CODE_PTR
	     && (TYPE_CODE (tt2) == TYPE_CODE_ARRAY
		 || TYPE_CODE (tt2) == TYPE_CODE_PTR))
	{
	  tt1 = check_typedef (TYPE_TARGET_TYPE (tt1));
	  tt2 = check_typedef (TYPE_TARGET_TYPE (tt2));
	}
      if (TYPE_CODE (tt1) == TYPE_CODE (tt2))
	continue;
      /* Array to pointer is a `trivial conversion' according to the ARM.  */

      /* We should be doing much hairier argument matching (see section 13.2
         of the ARM), but as a quick kludge, just check for the same type
         code.  */
      /* pai: FIXME Requires enhancement for WDB. Perhaps the code for
         overload resolution in gdbtypes.c can be used here. */
      if (TYPE_CODE (t1[i]) != TYPE_CODE (VALUE_TYPE (t2[i])))
	return i + 1;
    }
  if (!t1[i])
    return 0;
  return t2[i] ? i + 1 : 0;
}

/* Helper function used by value_struct_elt to recurse through baseclasses.
   Look for a field NAME in ARG1. Adjust the address of ARG1 by OFFSET bytes,
   and search in it assuming it has (class) type TYPE.
   If found, return value, else return NULL.

   looking_for_baseclass has three possible values :
   1) no_baseclass : look for structure field. 
   2) only_baseclass : look for baseclass named NAME.
   3) struct_or_baseclass : look for structure field first, and then
   the baseclass named NAME. Return the first
   structure field if it is found.
   The value is used when hp_som_som_object_present
   is true. */

static value_ptr
search_struct_field (char *name, register value_ptr arg1, int offset,
		     register struct type *type,
		     enum looking_for_baseclass_type looking_for_baseclass,
                     char *domain_name)
{
  int found = 0;
  char *found_class = NULL;	  /* allocated by callee, freed by caller */
  value_ptr v;
  struct type *vbase = NULL;

  v = search_struct_field_aux (name, arg1, offset, type,
			       looking_for_baseclass, &found,
			       &found_class, &vbase, domain_name);
  if (found > 1)
    warning ("%s ambiguous; using %s::%s. Use a cast to disambiguate.",
             name, found_class, name);
  if (found_class != NULL)
    free (found_class);

  return v;
}

static value_ptr
search_struct_field_aux (char *name,
			 register value_ptr arg1,
			 int offset,
			 register struct type *type,
			 enum looking_for_baseclass_type looking_for_baseclass,
			 int *found,
			 char **found_class_name,
			 struct type **vbase,
			 char *domain_name)
{
  int i;
  value_ptr retval = NULL;
  char *tmp_class_name = NULL;	/* allocated by callee, freed by caller */
  int tmp_found = 0;
  int assigned = 0;
  int nbases = TYPE_N_BASECLASSES (type);

  CHECK_TYPEDEF (type);

  if (looking_for_baseclass == no_baseclass ||
       looking_for_baseclass == struct_or_baseclass)
    for (i = TYPE_NFIELDS (type) - 1; i >= nbases; i--)
      {
	char *t_field_name = TYPE_FIELD_NAME (type, i);

	if (t_field_name && ((strcmp_iw (t_field_name, name) == 0)
	/* JAGaf71986 - <Gdb does not print object value incase of fortran> - Incase 
	   of Fortran, convert both field_name and name to lowercase and compare.*/
	    || ( current_language->la_language == language_fortran 
	    && strcasecmp (t_field_name, name) == 0))) /* JAGaf71986 - <END> */
	  {
	    value_ptr v = NULL;
	    if (TYPE_FIELD_STATIC (type, i))
	      v = value_static_field (type, i);
            if (v != NULL)
              {
                if (!*found)
                  {
                    /* Record return value and class name, and continue
                       looking for possible ambiguous members */
                    char *class_name = TYPE_TAG_NAME (type);
                    retval = v;
                    if (class_name)
		      {
              	        /* length of the string plus 1 for null char */
                        *found_class_name = (char *) xmalloc (strlen(TYPE_TAG_NAME (type))+1);
                        strcpy (*found_class_name, class_name);
		      }
                    else
                      *found_class_name = NULL;
                  }
                (*found)++;
              }
	    else
	      {
		char *fullname = (domain_name ?
				  (strncmp (domain_name, "data member of", 14) ?
				   NULL : &domain_name[15])
				  : NULL );
		if (!fullname || !strcmp(TYPE_TAG_NAME(type), fullname))
		  {
		    v = value_primitive_field (arg1, offset, i, type);
                    if (v != NULL)
                      {
                        if (!*found)
                          {
                            /* Record return value and class name, and continue
                               looking for possible ambiguous members */
                            char *class_name = TYPE_TAG_NAME (type);
                            retval = v;
                            if (class_name)
			      {
                                /* length of the string plus 1 for null char */
              		        *found_class_name = (char *) xmalloc (strlen(TYPE_TAG_NAME (type))+1);
                                strcpy (*found_class_name, class_name);
			      }
                            else
                              *found_class_name = NULL;
                          }
                        (*found)++;
                      }
                  }
              }

            if (v == 0 && looking_for_baseclass != struct_or_baseclass)
              error ("Couldn't retrieve field named %s", name);
          }

	if (t_field_name
	    && (t_field_name[0] == '\0'
		|| (TYPE_CODE (type) == TYPE_CODE_UNION
		    && (strcmp_iw (t_field_name, "else") == 0))))
	  {
	    struct type *field_type = TYPE_FIELD_TYPE (type, i);
	    if (TYPE_CODE (field_type) == TYPE_CODE_UNION
		|| TYPE_CODE (field_type) == TYPE_CODE_STRUCT
	        || TYPE_CODE (field_type) == TYPE_CODE_CLASS)
	      {
		/* Look for a match through the fields of an anonymous union,
		   or anonymous struct.  C++ provides anonymous unions.

		   In the GNU Chill implementation of variant record types,
		   each <alternative field> has an (anonymous) union type,
		   each member of the union represents a <variant alternative>.
		   Each <variant alternative> is represented as a struct,
		   with a member for each <variant field>.  */

		value_ptr v;
		int new_offset = offset;

		/* This is pretty gross.  In G++, the offset in an anonymous
		   union is relative to the beginning of the enclosing struct.
		   In the GNU Chill implementation of variant records,
		   the bitpos is zero in an anonymous union field, so we
		   have to add the offset of the union here. */
		if ((TYPE_CODE (field_type) == TYPE_CODE_STRUCT
	 	     || TYPE_CODE (field_type) == TYPE_CODE_CLASS)
		    || (TYPE_NFIELDS (field_type) > 0
			&& TYPE_FIELD_BITPOS (field_type, 0) == 0))
		  new_offset += TYPE_FIELD_BITPOS (type, i) / 8;

		v = search_struct_field_aux (name, arg1, new_offset,
					     field_type,
					     looking_for_baseclass,
					     &tmp_found, &tmp_class_name,
					     vbase, domain_name);
                if (!*found && v)
                  {
	            /* Length of the buffer needed to pass to caller is the sum
		       of the max lengths of the items concatenated plus 1. */
                    int retval_len = strlen(TYPE_TAG_NAME (type)) +
				     strlen(tmp_class_name) + 4;

                    /* Record return value and class name, and continue
                       looking for possible ambiguous members */
                    retval = v;
                    
		    *found_class_name = (char *) xmalloc (retval_len);
                    /* TYPE_TAG_NAME can be null in case of an anonymous union */
                    if (TYPE_TAG_NAME (type))
                      strcpy (*found_class_name, TYPE_TAG_NAME (type));
                    else
                      strcpy (*found_class_name, " ");
                    strcat (*found_class_name, "::");
                    strcat (*found_class_name, tmp_class_name);
  		    if (tmp_class_name != NULL)
    		      free (tmp_class_name);
                  }
                *found += tmp_found;
                tmp_found = 0;
              }
          }
      }

  /* Return the structure field if it is found. */
  if (*found > 0 && looking_for_baseclass == struct_or_baseclass)
    return retval;

  /* RM: If we are looking for a structure field, and we have found
     one, don't look through the baseclasses -- names there are
     hidden. ANSI C++ standard, section 10.2 */
  if (*found > 0 && looking_for_baseclass == no_baseclass)
    return retval;

  for (i = 0; i < nbases; i++)
    {
      value_ptr v;
      struct type *basetype = check_typedef (TYPE_BASECLASS (type, i));
      /* If we are looking for baseclasses, this is what we get when we
         hit them.  But it could happen that the base part's member name
         is not yet filled in.  */
      int found_baseclass = (looking_for_baseclass
			     && TYPE_BASECLASS_NAME (type, i) != NULL
			     && (strcmp_iw (name, TYPE_BASECLASS_NAME (type, i)) == 0));

      if (BASETYPE_VIA_VIRTUAL (type, i))
	{
	  int boffset;
	  value_ptr v2 = allocate_value (VALUE_ENCLOSING_TYPE (arg1));

          if (TYPE_HAS_VTABLE (type) && ! IS_TARGET_LRE)
            {
              /* HP aCC compiled type, use Taligent/HP runtime model */
              int skip;
              find_rt_vbase_offset (type, TYPE_BASECLASS (type, i),
                                    VALUE_CONTENTS_ALL (arg1),
                                    offset + VALUE_EMBEDDED_OFFSET (arg1),
                                    &boffset, &skip);
              if (skip >= 0)
                error ("Virtual base class offset not found from vtable");
            }

          else
            {

              boffset = baseclass_offset (type, i,
                                          VALUE_CONTENTS_ALL (arg1) + offset,
                                          VALUE_ADDRESS (arg1)
                                          + VALUE_OFFSET (arg1) + offset);
              if (boffset == -1)
                error ("virtual baseclass botch");

              /* The virtual base class pointer might have been clobbered by the
                 user program. Make sure that it still points to a valid memory
                 location.  */

              if ((boffset + offset) < 0 ||
                  (boffset + offset) >= TYPE_LENGTH (type))
                {
                  CORE_ADDR base_addr;

                  base_addr = VALUE_ADDRESS (arg1) + VALUE_OFFSET (arg1) +
                    boffset + offset;
		  base_addr = SWIZZLE (base_addr);
                  if (target_read_memory (base_addr, VALUE_CONTENTS_RAW (v2),
                                          TYPE_LENGTH (basetype)) != 0)
                    error ("virtual baseclass botch");
                  VALUE_LVAL (v2) = lval_memory;
                  VALUE_ADDRESS (v2) = base_addr;
                  assigned = 1;
                }
            }

          if (!assigned)
            {
              VALUE_LVAL (v2) = VALUE_LVAL (arg1);
              VALUE_ADDRESS (v2) = VALUE_ADDRESS (arg1);
            }

          /* Earlier, this code used to allocate a value of type
             basetype and copy the contents of arg1 at the
             appropriate offset into the new value.  This doesn't
             work because there is important stuff (virtual bases,
             for example) that could be anywhere in the contents
             of arg1, and not just within the length of a basetype
             object.  In particular the boffset below could be
             negative, with the HP/Taligent C++ runtime system.
             So, the only way to ensure that required information
             is not lost is to always allocate a value of the same
             type as arg1 and to fill it with the _entire_
             contents of arg1.  It sounds wasteful, but there is
             really no way around it if later member lookup,
             casts, etc. have to work correctly with the returned
             value.  */


          VALUE_TYPE (v2) = basetype;
          VALUE_OFFSET (v2) = VALUE_OFFSET (arg1);
          VALUE_EMBEDDED_OFFSET (v2)
            = VALUE_EMBEDDED_OFFSET (arg1) + offset + boffset;
          if (VALUE_LAZY (arg1))
            VALUE_LAZY (v2) = 1;
          else
            memcpy ((char *) (v2)->aligner.contents,
                    (char *) (arg1)->aligner.contents,
                    TYPE_LENGTH (VALUE_ENCLOSING_TYPE (arg1)));

          if (found_baseclass)
            {
              /*return v2; */

              if (!*found)      /* not yet found anything */
                {
                  /* Record return value and class name, and continue
                     looking for possible ambiguous members */
                  retval = v2;
                  /* length of the string plus 1 for null char */   
                  *found_class_name = (char *) xmalloc (strlen(TYPE_TAG_NAME (type))+1);
                  strcpy (*found_class_name, TYPE_TAG_NAME (type));
                }
              /* Don't count virtual bases twice when deciding ambiguity */
              if (*vbase != basetype)   /* works for null *vbase */
                (*found)++;
              /* Is this the first virtual base where we "found" something? */
              if (!*vbase)
                *vbase = basetype;
            }
          else
            /* base not found, or looking for member */
            {
              v = search_struct_field_aux (name, arg1, offset + boffset,
                                           TYPE_BASECLASS (type, i),
                                           looking_for_baseclass, &tmp_found,
                                        &tmp_class_name, vbase, domain_name);
              if (!*found && v)
                {
	          /* Length of the buffer needed to pass to caller is the sum
		     of the maximum lengths of the items concatenated plus 1. */
                  int retval_len = strlen(TYPE_TAG_NAME (type)) +
				   strlen(tmp_class_name) + 4;

                  /* Record return value and class name, and continue
                     looking for possible ambiguous members */
                  retval = v;
                  
		  *found_class_name = (char *) xmalloc (retval_len);
                  /* TYPE_TAG_NAME can be null in case of an anonymous union */
                  if (TYPE_TAG_NAME (type))
                    strcpy (*found_class_name, TYPE_TAG_NAME (type));
                  else
                    strcpy (*found_class_name, " ");
                  strcat (*found_class_name, "::");
                  strcat (*found_class_name, tmp_class_name);
  		  if (tmp_class_name != NULL)
    		    free (tmp_class_name);
                }
              /* Don't count virtual bases twice when deciding ambiguity */
              if (*vbase != basetype)   /* works for null *vbase */
                *found += tmp_found;
              /* Is this the first virtual base where we "found" something? */
              if (!*vbase)
                *vbase = basetype;
              tmp_found = 0;
            }
        }
      else if (found_baseclass)
        {
          v = value_primitive_field (arg1, offset, i, type);
          if (!*found)
            {
              /* Record return value and class name, and continue
                 looking for possible ambiguous members */
              retval = v;

              /* length of the string plus 1 for null char */ 
              *found_class_name = (char *) xmalloc (strlen(TYPE_TAG_NAME (type))+1);
              strcpy (*found_class_name, TYPE_TAG_NAME (type));
            }
          (*found)++;
        }
      else
        {
          v = search_struct_field_aux (name, arg1,
                               (int) (offset + TYPE_BASECLASS_BITPOS (type, i) / 8),
                                basetype, looking_for_baseclass, &tmp_found,
                                       &tmp_class_name, vbase, domain_name);
          if (!*found && v)
            {
	      /* Length of the buffer needed to pass to caller is the sum
		 of the maximum lengths of the items concatenated plus one. */
              int retval_len = strlen(TYPE_TAG_NAME (type)) +
			       strlen(tmp_class_name) + 4;

              /* Record return value and class name, and continue
                 looking for possible ambiguous members */
              retval = v;

              *found_class_name = (char *) xmalloc (retval_len);
              /* TYPE_TAG_NAME can be null in case of an anonymous union */
              if (TYPE_TAG_NAME (type))
                strcpy (*found_class_name, TYPE_TAG_NAME (type));
              else
                strcpy (*found_class_name, " ");
              strcat (*found_class_name, "::");
              strcat (*found_class_name, tmp_class_name);
  	      if (tmp_class_name != NULL)
    		free (tmp_class_name);
            }
          *found += tmp_found;
          tmp_found = 0;
        }
    }
  return retval;
}

/* Return the offset (in bytes) of the virtual base of type BASETYPE
 * in an object pointed to by VALADDR (on the host), assumed to be of
 * type TYPE.  OFFSET is number of bytes beyond start of ARG to start
 * looking (in case VALADDR is the contents of an enclosing object).
 *
 * This routine recurses on the primary base of the derived class because
 * the virtual base entries of the primary base appear before the other
 * virtual base entries.
 *
 * If the virtual base is not found, a negative integer is returned.
 * The magnitude of the negative integer is the number of entries in
 * the virtual table to skip over (entries corresponding to various
 * ancestral classes in the chain of primary bases).
 *
 * Important: This assumes the HP / Taligent C++ runtime
 * conventions. Use baseclass_offset() instead to deal with g++
 * conventions.  */

void
find_rt_vbase_offset (struct type *type, struct type *basetype, char *valaddr,
		      int offset, int *boffset_p, int *skip_p)
{
  int boffset;			/* offset of virtual base */
  int index;			/* displacement to use in virtual table */
  int skip;
  int virtual_base_index;

  value_ptr vp;
  CORE_ADDR vtbl;		/* the virtual table pointer */
  struct type *pbc;		/* the primary base class */

  /* JAGae23887 - DW_VIRTUALITY values in dwarf2.h were modified to
     comply with the DWARF2 standard.  As such a class which is not a
     virtual class may be mistaken as a virtual class.  Thus the entry
     in the vtable may be junk and this would be reflected in an offset
     which is much too large. */
  if (offset > LARGE_VTABLE_OFFSET) 
    error("\nError printing info about %s:\
           \nProbably compiled with an older compiler.\
           \nUse gdb version 1.3.02 or older. OR\
           \nRun gdb, set old-vtable on, then load file.", type->name);

  /* Look for the virtual base recursively in the primary base, first.
   * This is because the derived class object and its primary base
   * subobject share the primary virtual table.  */

  boffset = 0;
  pbc = TYPE_PRIMARY_BASE (type);
  if (pbc)
    {
      find_rt_vbase_offset (pbc, basetype, valaddr, offset, &boffset, &skip);
      if (skip < 0)
	{
	  *boffset_p = boffset;
	  *skip_p = -1;
	  return;
	}
    }
  else
    skip = 0;


  /* Find the index of the virtual base according to HP/Taligent
     runtime spec. (Depth-first, left-to-right.)  */
  index = virtual_base_index_skip_primaries (basetype, type);

  if (index < 0)
    {
      *skip_p = skip + virtual_base_list_length_skip_primaries (type);
      *boffset_p = 0;
      return;
    }

  /* First word (sizeof(CORE_ADDR) bytes) in object layout is the 
     vtable pointer */
  if (!is_swizzled)  /* LP64 for IPF */
    vtbl = *(CORE_ADDR *) (void *) (valaddr + offset);
  else  /* ILP32 */
    vtbl = *(unsigned int *) (void *) (valaddr + offset);

  if (IS_TARGET_LRE)
    endian_flip (&vtbl, sizeof (vtbl));  /* flip vtbl ptr read in */

  /* Before the constructor is invoked, things are usually zero'd out. */
  if (vtbl == 0)
    error ("Couldn't find virtual table -- object may not be constructed yet.");


  /* Find virtual base's offset -- jump over entries for primary base
   * ancestors, then use the index computed above.  But also adjust by
   * HP_ACC_VBASE_START for the vtable slots before the start of the
   * virtual base entries.  Offset is negative -- virtual base entries
   * appear _before_ the address point of the virtual table. */
  if (!is_swizzled)   /* LP64 for IPF */
    {
      if (!new_cxx_abi)
	vp = value_at (builtin_type_CORE_ADDR,
		       vtbl - sizeof (CORE_ADDR) * 
		       (skip + index + HP_ACC_VBASE_START),
		       NULL);
      else
	{
	  /* The virtual base offset is at virtual_base_index
	     CORE_ADDR's from the vtable address point.
	     */

	  virtual_base_index = TYPE_FIELD_VIRTUAL_BASE_INDEX(type, index);
	  vp = value_at (builtin_type_CORE_ADDR,
			 vtbl + (  (CORE_ADDR) sizeof (CORE_ADDR) 
				 * virtual_base_index),
			 NULL);
	}
#ifndef HP_IA64
      boffset = value_as_long (vp);
#else
      /* Poorva: Why do we want to do value_as_long. It is already there in aligner
	contents. Just get it from there 
      */
      boffset = (int) (*(CORE_ADDR *) (void *) (VALUE_CONTENTS (vp)));
#endif
    }
  else /*ILP 32 */
    {
      if (!new_cxx_abi)
	{
	  vp = value_at (builtin_type_CORE_ADDR,
			 vtbl - sizeof (unsigned long) * 
			 (skip + index + HP_ACC_VBASE_START),
			 NULL);
	}
      else
	{
	  virtual_base_index = TYPE_FIELD_VIRTUAL_BASE_INDEX(type, index);
	  vp = value_at (builtin_type_CORE_ADDR,
			 vtbl + (  (CORE_ADDR) sizeof (CORE_ADDR)
				 * virtual_base_index),
			 NULL);
	}
#ifndef HP_IA64
      boffset = value_as_long (vp);
#else
      /* Poorva: Why do we want to do value_as_long. It is already there in aligner
	contents. Just get it from there 
      */
      boffset = *(unsigned int *) (void *) (VALUE_CONTENTS (vp));
     /* JAGae23887 - DW_VIRTUALITY values in dwarf2.h were modified to
        comply with the DWARF2 standard.  As such a class which is not a
        virtual class may be mistaken as a virtual class.  Thus the entry
        in the vtable may be junk and this would be reflected in an offset
        which is much too large. */
     if (boffset > LARGE_VTABLE_OFFSET) 
       error("\nError printing info about %s:\
              \nProbably compiled with an older compiler.\
              \nUse gdb version 1.3.02 or older. OR\
              \nRun gdb, set old-vtable on, then load file.", type->name);
#endif
    }

  vp = value_at (builtin_type_CORE_ADDR, 
                 vtbl - sizeof(CORE_ADDR) * (skip + index + HP_ACC_VBASE_START),
                 NULL);
  *skip_p = -1;
  *boffset_p = boffset;
  return;
}


/* Helper function used by value_struct_elt to recurse through baseclasses.
   Look for a field NAME in ARG1. Adjust the address of ARG1 by OFFSET bytes,
   and search in it assuming it has (class) type TYPE.
   If found, return value, else if name matched and args not return (value)-1,
   else return NULL. */

static value_ptr
search_struct_method (char *name, register value_ptr *arg1p, register value_ptr *args,
		      int offset, int *static_memfuncp, register struct type *type)
{
  int i;
  value_ptr v;
  int name_matched = 0;
  char dem_opname[64];

  CHECK_TYPEDEF (type);
  for (i = TYPE_NFN_FIELDS (type) - 1; i >= 0; i--)
    {
      char *t_field_name = TYPE_FN_FIELDLIST_NAME (type, i);
      /* FIXME!  May need to check for ARM demangling here */
      if (strncmp (t_field_name, "__", 2) == 0 ||
	  strncmp (t_field_name, "op", 2) == 0 ||
	  strncmp (t_field_name, "type", 4) == 0)
	{
	  if (cplus_demangle_opname (t_field_name, dem_opname, DMGL_ANSI))
	    t_field_name = dem_opname;
	  else if (cplus_demangle_opname (t_field_name, dem_opname, 0))
	    t_field_name = dem_opname;
	}
      if (t_field_name && (strcmp_iw (t_field_name, name) == 0))
	{
	  int j = TYPE_FN_FIELDLIST_LENGTH (type, i) - 1;
	  struct fn_field *f = TYPE_FN_FIELDLIST1 (type, i);
	  name_matched = 1;

	  if (j > 0 && args == 0)
	    error ("cannot resolve overloaded method `%s': no arguments supplied", name);
	  while (j >= 0)
	    {
	      struct type **t1 = TYPE_FN_FIELD_ARGS (f, j);
	      if (TYPE_FN_FIELD_STUB (f, j))
		check_stub_method (type, i, j);
	      is_ipf_cxx_abi_hand_fn_call =
		IPF_CXX_ABI_MEMBER_FN(t1, type, TYPE_NFIELDS(type));
	      if (is_ipf_cxx_abi_hand_fn_call)
		t1++;
	      if (!typecmp (TYPE_FN_FIELD_STATIC_P (f, j), t1, args))
		{
		  if (TYPE_FN_FIELD_VIRTUAL_P (f, j))
		    return value_virtual_fn_field (arg1p, f, j, type, offset);
                  if (TYPE_FN_FIELD_STATIC_P (f, j))
                    {
                      if (static_memfuncp)
			*static_memfuncp = 1;
                      /* RM: Don't try to do this pointer adjustment
                         for static functions -- the object we have
                         may not be an lvalue (eg, the dummy object
                         created for A::foo()) */
                      v = value_fn_field (0, f, j, type, offset);
                    }
                  else
                    {
		      v = value_fn_field (arg1p, f, j, type, offset);
		    }
		  if (v != NULL)
		    return v;
		}
	      j--;
	    }
	}
    }

  for (i = TYPE_N_BASECLASSES (type) - 1; i >= 0; i--)
    {
      int base_offset;

      if (BASETYPE_VIA_VIRTUAL (type, i))
	{
	  if (TYPE_HAS_VTABLE (type))
	    {
	      /* HP aCC compiled type, search for virtual base offset
	         according to HP/Taligent runtime spec.  */
	      int skip;
	      find_rt_vbase_offset (type, TYPE_BASECLASS (type, i),
				    VALUE_CONTENTS_ALL (*arg1p),
				    offset + VALUE_EMBEDDED_OFFSET (*arg1p),
				    &base_offset, &skip);
	      if (skip >= 0)
		error ("Virtual base class offset not found in vtable");
	    }
	  else
	    {
	      struct type *baseclass = check_typedef (TYPE_BASECLASS (type, i));
	      char *base_valaddr;

	      /* The virtual base class pointer might have been clobbered by the
	         user program. Make sure that it still points to a valid memory
	         location.  */

	      if (offset < 0 || offset >= TYPE_LENGTH (type))
		{
		  base_valaddr = (char *) alloca (TYPE_LENGTH (baseclass));
		  if (target_read_memory (SWIZZLE(VALUE_ADDRESS (*arg1p))
					  + VALUE_OFFSET (*arg1p) + offset,
					  base_valaddr,
					  TYPE_LENGTH (baseclass)) != 0)
		    error ("virtual baseclass botch");
		}
	      else
		base_valaddr = (char*) (unsigned long) SWIZZLE (
			(CORE_ADDR) VALUE_CONTENTS (*arg1p) + offset);

	      base_offset =
		baseclass_offset (type, i, base_valaddr,
				  SWIZZLE(VALUE_ADDRESS (*arg1p))
				  + VALUE_OFFSET (*arg1p) + offset);
	      if (base_offset == -1)
		error ("virtual baseclass botch");
	    }
	}
      else
	{
	  base_offset = (int) (TYPE_BASECLASS_BITPOS (type, i) / 8);
	}
      v = search_struct_method (name, arg1p, args, base_offset + offset,
				static_memfuncp, TYPE_BASECLASS (type, i));
      if (v == ((value_ptr) - 1L))
	{
	  name_matched = 1;
	}
      else if (v)
	{
/* FIXME-bothner:  Why is this commented out?  Why is it here?  */
/*        *arg1p = arg1_tmp; */
	  return v;
	}
    }
  if (name_matched)
    return ((value_ptr) - 1L);
  else
    return NULL;
}

/* Given *ARGP, a value of type (pointer to a)* structure/union,
   extract the component named NAME from the ultimate target structure/union
   and return it as a value with its appropriate type.
   ERR is used in the error message if *ARGP's type is wrong.

   C++: ARGS is a list of argument types to aid in the selection of
   an appropriate method. Also, handle derived types.

   STATIC_MEMFUNCP, if non-NULL, points to a caller-supplied location
   where the truthvalue of whether the function that was resolved was
   a static member function or not is stored.

   ERR is an error message to be printed in case the field is not found.  */

value_ptr
value_struct_elt (register value_ptr *argp, register value_ptr *args, char *name,
		  int *static_memfuncp, char *err)
{
  register struct type *t;
  value_ptr v = NULL;

  COERCE_ARRAY (*argp);

#ifdef HP_IA64
  /* QXCR1000944147: WDB hangs when trying to dereference the optimized-out
     variable. Return from here w/o evaluating any further if the argument
     is optimized-out. */
  if (argp && (*argp != NULL) && ((*argp)->availability == VA_OPTIMIZED_OUT))
    return (*argp);
#endif

  t = check_typedef (VALUE_TYPE (*argp));

  /* Follow pointers until we get to a non-pointer.  */

  while (TYPE_CODE (t) == TYPE_CODE_PTR || TYPE_CODE (t) == TYPE_CODE_REF)
    {
      *argp = value_ind (*argp);
      /* Don't coerce fn pointer to fn and then back again!  */
      if (TYPE_CODE (VALUE_TYPE (*argp)) != TYPE_CODE_FUNC)
	COERCE_ARRAY (*argp);
      t = check_typedef (VALUE_TYPE (*argp));
    }

  if (TYPE_CODE (t) == TYPE_CODE_MEMBER)
    error ("not implemented: member type in value_struct_elt");

  if (TYPE_CODE (t) != TYPE_CODE_STRUCT
      && TYPE_CODE (t) != TYPE_CODE_UNION
      && TYPE_CODE (t) != TYPE_CODE_CLASS)
    error ("Attempt to extract a component of a value that is not a %s.", err);

  /* If value can't be evaluated, return value struct with correct type. */
  if (VALUE_AVAILABILITY (*argp) != VA_AVAILABLE)
     {
       VALUE_TYPE (*argp) = t;
       return *argp;
     }

  /* Assume it's not, unless we see that it is.  */
  if (static_memfuncp)
    *static_memfuncp = 0;

  if (!args)
    {
      /* if there are no arguments ...do this...  */

      /* Try as a field first, because if we succeed, there
         is less work to be done.  */
      /* RM: add in embedded offset of class */
      if (strncmp(err, "data member", 11))
        {
          v = search_struct_field (name, *argp, VALUE_EMBEDDED_OFFSET(*argp),
                                   t, no_baseclass, NULL);
        }
      else
        {
          /* It can be a class data member. */
          v = search_struct_field (name, *argp, VALUE_EMBEDDED_OFFSET(*argp),
                                   t, struct_or_baseclass, err);
        }
      if (v)
	return v;

      /* C++: If it was not found as a data field, then try to
         return it as a pointer to a method.  */

      if (destructor_name_p (name, t))
	error ("Cannot get value of destructor");

      v = search_struct_method (name, argp, args, 0, static_memfuncp, t);

      if (v == ((value_ptr) - 1L))
	error ("Cannot take address of a method");
      else if (v == 0)
	{
	  if (TYPE_NFN_FIELDS (t))
	    error ("There is no member or method named %s.", name);
	  else
	    error ("There is no member named %s.", name);
	}
      return v;
    }

  if (destructor_name_p (name, t))
    {
      if (!args[1])
	{
	  /* Destructors are a special case.  */
	  int m_index, f_index;

	  v = NULL;
	  if (get_destructor_fn_field (t, &m_index, &f_index))
	    {
	      v = value_fn_field (NULL, TYPE_FN_FIELDLIST1 (t, m_index),
				  f_index, NULL, 0);
	    }
	  if (v == NULL)
	    error ("could not find destructor function named %s.", name);
	  else
	    return v;
	}
      else
	{
	  error ("destructor should not have any argument");
	}
    }
  else
    v = search_struct_method (name, argp, args, 0, static_memfuncp, t);

  if (v == ((value_ptr) - 1L))
    {
      error ("Argument list of %s mismatch with component in the structure.", name);
    }
  else if (v == 0)
    {
      /* See if user tried to invoke data as function.  If so,
         hand it back.  If it's not callable (i.e., a pointer to function),
         gdb should give an error.  */
      v = search_struct_field (name, *argp, 0, t, no_baseclass, NULL);
    }

  if (!v)
    error ("Structure has no component named %s.", name);
  return v;
}

/* Search through the methods of an object (and its bases)
 * to find a specified method. Return the pointer to the
 * fn_field list of overloaded instances.
 * Helper function for value_find_oload_list.
 * ARGP is a pointer to a pointer to a value (the object)
 * METHOD is a string containing the method name
 * OFFSET is the offset within the value
 * STATIC_MEMFUNCP is set if the method is static
 * TYPE is the assumed type of the object
 * NUM_FNS is the number of overloaded instances
 * BASETYPE is set to the actual type of the subobject where the method is found
 * BOFFSET is the offset of the base subobject where the method is found */

static struct fn_field *
find_method_list (value_ptr *argp, char *method, int offset, int *static_memfuncp,
		  struct type *type, int *num_fns, struct type **basetype, int *boffset)
{
  int i;
  struct fn_field *f;
  CHECK_TYPEDEF (type);

  *num_fns = 0;

  /* First check in object itself */
  for (i = TYPE_NFN_FIELDS (type) - 1; i >= 0; i--)
    {
      /* pai: FIXME What about operators and type conversions? */
      char *fn_field_name = TYPE_FN_FIELDLIST_NAME (type, i);

      /* For templates we want Foo::bar to match Foo<char>::bar
	 hence we use strcmp_iwt instead of strcmp_iw */
      if (fn_field_name && !strcmp_iwt (fn_field_name, method))
	{
	  *num_fns = TYPE_FN_FIELDLIST_LENGTH (type, i);
	  *basetype = type;
	  *boffset = offset;
	  return TYPE_FN_FIELDLIST1 (type, i);
	}
    }

  /* Not found in object, check in base subobjects */
  for (i = TYPE_N_BASECLASSES (type) - 1; i >= 0; i--)
    {
      int base_offset;
      if (BASETYPE_VIA_VIRTUAL (type, i))
	{
	  if (TYPE_HAS_VTABLE (type))
	    {
	      /* HP aCC compiled type, search for virtual base offset
	       * according to HP/Taligent runtime spec.  */
	      int skip;
	      find_rt_vbase_offset (type, TYPE_BASECLASS (type, i),
				    VALUE_CONTENTS_ALL (*argp),
				    offset + VALUE_EMBEDDED_OFFSET (*argp),
				    &base_offset, &skip);
	      if (skip >= 0)
		error ("Virtual base class offset not found in vtable");
            }
	  else
	    {
	      /* probably g++ runtime model */
	      base_offset = VALUE_OFFSET (*argp) + offset;
	      base_offset =
		baseclass_offset (type, i,
				  VALUE_CONTENTS (*argp) + base_offset,
				  VALUE_ADDRESS (*argp) + base_offset);
	      if (base_offset == -1)
		error ("virtual baseclass botch");
	    }
	}
      else
	/* non-virtual base, simply use bit position from debug info */
	{
	  base_offset = (int) (TYPE_BASECLASS_BITPOS (type, i) / 8);
	}
      f = find_method_list (argp, method, base_offset + offset,
      static_memfuncp, TYPE_BASECLASS (type, i), num_fns, basetype, boffset);
      if (f)
	return f;
    }
  return NULL;
}

/* Return the list of overloaded methods of a specified name.
 * ARGP is a pointer to a pointer to a value (the object)
 * METHOD is the method name
 * OFFSET is the offset within the value contents
 * STATIC_MEMFUNCP is set if the method is static
 * NUM_FNS is the number of overloaded instances
 * BASETYPE is set to the type of the base subobject that defines the method
 * BOFFSET is the offset of the base subobject which defines the method */

struct fn_field *
value_find_oload_method_list (value_ptr *argp, char *method, int offset, int *static_memfuncp,
			      int *num_fns, struct type **basetype, int *boffset)
{
  struct type *t;
  struct fn_field *method_list = NULL;

  t = check_typedef (VALUE_TYPE (*argp));

  /* code snarfed from value_struct_elt */
  while (TYPE_CODE (t) == TYPE_CODE_PTR || TYPE_CODE (t) == TYPE_CODE_REF)
    {
      *argp = value_ind (*argp);
      /* Don't coerce fn pointer to fn and then back again!  */
      if (TYPE_CODE (VALUE_TYPE (*argp)) != TYPE_CODE_FUNC)
	COERCE_ARRAY (*argp);
      t = check_typedef (VALUE_TYPE (*argp));
    }

  if (TYPE_CODE (t) == TYPE_CODE_MEMBER)
    error ("Not implemented: member type in value_find_oload_lis");

  if (TYPE_CODE (t) != TYPE_CODE_STRUCT
      && TYPE_CODE (t) != TYPE_CODE_UNION
      && TYPE_CODE (t) != TYPE_CODE_CLASS)
    error ("Attempt to extract a component of a value that is not a struct or union");

  /* Assume it's not static, unless we see that it is.  */
  if (static_memfuncp)
    *static_memfuncp = 0;

  method_list = find_method_list (argp, method, 0, static_memfuncp, t,
                                  num_fns, basetype, boffset);
  
#ifdef HP_IA64
  /* QXCR1000890046: Gdb is unable to make command line calls to some methods
     within ecom. The type obtained at the first go might not be the right one.
     If we have not found the method yet, re-evaluate the expression to obtain
     the correct type. (Following the ptype method here.)
   */

  if (!method_list)
    {
      struct expression *expr = NULL;
      struct type *new_type = NULL;
      if (t->name != NULL)
        {
          expr = parse_expression (t->name);
          if (expr->elts[0].opcode == OP_TYPE)
            {
              new_type = expr->elts[1].type;
              method_list = find_method_list (argp, method, 0, static_memfuncp,
                                              new_type, num_fns, basetype,
                                              boffset);
            }
        }
    }
#endif
  return method_list;
}

#ifdef HP_IA64
/* Given a name of symbol search for it from local scope till we reach
   global scope in outword direction.
   If we found a symbol for the function from a symtab but if it is 
   optimized out (If that objfile is compiled with > +O2) then, the
   information about the function from that symbol is not reliable
   I.e. No return type information, No number of parameters, and type 
   information for parameters is available.
   In such case search for an unoptimized symbol for the function in 
   the call in other objfiles.
   If we get an unoptimized symbol then return it for calling.
*/
   struct symbol * lookup_symbol_from_unoptimized_objfile (char *name,
	register const struct block *block, const namespace_enum namespace,
		int *is_a_field_of_this, struct symtab **symtab)
    {
      struct symbol *un_opt_sym = NULL, *temp = NULL;
      
      /* First call the lookup_symbol to get a symbol for the function in call
	 before starting the sequential search for an unoptimized symbol */ 
      un_opt_sym = lookup_symbol (name, block, namespace, is_a_field_of_this,
		                  symtab);
      temp = un_opt_sym;
 
      /* Check whether we have got the unoptimized symbol in the first attempt */
      if (un_opt_sym && (un_opt_sym->ginfo.opt_level < 2))
        return un_opt_sym;
     
      struct objfile *objfile = NULL; 
      struct minimal_symbol *msymbol = NULL;  
      struct symtab *un_opt_symtab = NULL;
      struct blockvector *bv = NULL;  
      ALL_OBJFILES (objfile)
      {
        msymbol = lookup_minimal_symbol_1 (name, NULL, objfile, namespace);
#ifdef HASH_ALL
        un_opt_symtab = find_symtab (msymbol);    
#else
	un_opt_symtab = find_pc_symtab (SYMBOL_VALUE_ADDRESS (msymbol));  	
#endif  	
	if (un_opt_symtab != NULL)
	 {
	   bv = BLOCKVECTOR (un_opt_symtab);
	   block = BLOCKVECTOR_BLOCK (bv, GLOBAL_BLOCK);
	   un_opt_sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
						namespace); 
	  
	   if (un_opt_sym && (un_opt_sym->ginfo.opt_level < 2))
            return un_opt_sym; 		 
	   
  	   /* We kept static functions in minimal symbol table as well as
                 in static scope. We want to find them in the symbol table.
	   */
           if (!un_opt_sym)
            {
              block = BLOCKVECTOR_BLOCK (bv, STATIC_BLOCK);
              un_opt_sym = lookup_block_symbol (block, SYMBOL_NAME (msymbol),
                                             namespace);
              if (un_opt_sym && (un_opt_sym->ginfo.opt_level < 2))
               return un_opt_sym;
	    }

        }
      } 
      if (temp)
       warning ("The function you are calling is optimized.\n"
           "Calling optimized functions may result in un-expected behaviour\n");
	return (temp);
    }   
     
#endif


/* Given an array of argument types (ARGTYPES) (which includes an
   entry for "this" in the case of C++ methods), the number of
   arguments NARGS, the NAME of a function whether it's a method or
   not (METHOD), and the degree of laxness (LAX) in conforming to
   overload resolution rules in ANSI C++, find the best function that
   matches on the argument types according to the overload resolution
   rules.

   In the case of class methods, the parameter OBJ is an object value
   in which to search for overloaded methods.

   In the case of non-method functions, the parameter FSYM is a symbol
   corresponding to one of the overloaded functions.

   Return value is an integer: 0 -> good match, 10 -> debugger applied
   non-standard coercions, 100 -> incompatible.

   If a method is being searched for, VALP will hold the value.
   If a non-method is being searched for, SYMP will hold the symbol for it.

   If a method is being searched for, and it is a static method,
   then STATICP will point to a non-zero value.

   Note: This function does *not* check the value of
   overload_resolution.  Caller must check it to see whether overload
   resolution is permitted.
 
 */

int
find_overload_match (struct type **arg_types, int nargs, char *name, enum call_type method,
		     int lax, value_ptr obj, struct symbol *fsym, value_ptr *valp,
		     struct symbol **symp, int *staticp)
{
  int nparms;
  struct type **parm_types;

  short oload_champ = -1;	/* Index of best overloaded function */
  short oload_ambiguous = 0;	/* Current ambiguity state for overload resolution */
  /* 0 => no ambiguity, 1 => two good funcs, 2 => incomparable funcs */
  short oload_non_standard = 0;	/* did we have to use non-standard conversions? */
  short oload_incompatible = 0;	/* are args supplied incompatible with any function? */

  struct badness_vector *bv;	/* A measure of how good an overloaded instance is */
  struct badness_vector *oload_champ_bv = NULL;		/* The measure for the current best match */

  value_ptr temp = obj;
  struct fn_field *fns_ptr = NULL;	/* For methods, the list of overloaded methods */
  struct symbol **oload_syms = NULL;	/* For non-methods, the list of overloaded function symbols */
  int num_fns = 0;		/* Number of overloaded instances being considered */
  struct symbol *current_oload_champ_sym = NULL, *sym = NULL; /*  pointer to a symbol */ 
  struct type *basetype = NULL;
  int boffset;
  register int ix;

  char *obj_type_name = NULL;
  char *func_name = NULL;

  /* Get the list of overloaded methods or functions */
  if (method)
    {
      int i;
      int len;
      struct type *domain;
      obj_type_name = TYPE_NAME (VALUE_TYPE (obj));
      /* Hack: evaluate_subexp_standard often passes in a pointer
         value rather than the object itself, so try again */
      if ((!obj_type_name || !*obj_type_name) &&
	  (TYPE_CODE (VALUE_TYPE (obj)) == TYPE_CODE_PTR))
	obj_type_name = TYPE_NAME (TYPE_TARGET_TYPE (VALUE_TYPE (obj)));

      fns_ptr = value_find_oload_method_list (&temp, name, 0,
					      staticp,
					      &num_fns,
					      &basetype, &boffset);
      if (!fns_ptr || !num_fns)
	error ("Couldn't find method %s%s%s",
	       obj_type_name,
	       (obj_type_name && *obj_type_name) ? "::" : "",
	       name);
      domain = TYPE_DOMAIN_TYPE (fns_ptr[0].type);
      /* PURIFY COMMENT: domain could be NULL, which is OK depending
	 on the implementation and tool chain.  We don't want to
	 continue further to check_stub_method in this case. */
      if (domain)
        {
	  len = TYPE_NFN_FIELDS (domain);
	  /* NOTE: dan/2000-03-10: This stuff is for STABS, which won't
	     give us the info we need directly in the types. We have to
	     use the method stub conversion to get it. Be aware that this
	     is by no means perfect, and if you use STABS, please move to
	     DWARF-2, or something like it, because trying to improve
	     overloading using STABS is really a waste of time. */
	  for (i = 0; i < len; i++)
	    {
	      int j;
	      struct fn_field *f = TYPE_FN_FIELDLIST1 (domain, i);
	      int len2 = TYPE_FN_FIELDLIST_LENGTH (domain, i);

	      for (j = 0; j < len2; j++)
	        {
		  if (TYPE_FN_FIELD_STUB (f, j) && (!strcmp_iw (TYPE_FN_FIELDLIST_NAME (domain,i),name)))
		    check_stub_method (domain, i, j);
		}
	    }
	}
    }
  else
    {
      int i = -1;
      func_name = cplus_demangle (SYMBOL_NAME (fsym), DMGL_NO_OPTS);

      /* If the name is NULL this must be a C-style function.
         Just return the same symbol. */
      if (!func_name)
        {
	  *symp = fsym;
          return 0;
        }

      oload_syms = make_symbol_overload_list (fsym);
      while (oload_syms[++i])
	num_fns++;
      if (!num_fns)
	error ("Couldn't find function %s", func_name);
    }

  oload_champ_bv = NULL;

  /* Consider each candidate in turn */
  for (ix = 0; ix < num_fns; ix++)
    {
      int jj, kk, nn;

      /* RM: check to see if the function has an retval ptr argument */
      /* RM: unfortunately, the debug information for the MEMFUNC doesn't
         contain information about artificial retval pointers. We need to
         find the debug information for the FUNC entry */
      if (method)   
#ifdef HP_IA64
        sym = lookup_symbol_from_unoptimized_objfile (fns_ptr[ix].physname,
                             0, VAR_NAMESPACE, 0, NULL);
#else
	sym = lookup_symbol (fns_ptr[ix].physname, 0, VAR_NAMESPACE, 0, NULL);
#endif
      else
        sym = oload_syms[ix];

      nparms = method ?
               (sym ?
                TYPE_NFIELDS (SYMBOL_TYPE (sym)) :
                TYPE_NFIELDS (fns_ptr[ix].type)) 
               : TYPE_NFIELDS (SYMBOL_TYPE (oload_syms[ix]));

      nn = nparms;
      if (sym && (TYPE_RETVAL_PTR_IDX(SYMBOL_TYPE(sym)) != -1))
        nparms--;

      /* Prepare array of parameter types */
      parm_types = (struct type **) xmalloc (nparms * (sizeof (struct type *)));
      for (jj = 0, kk = 0; jj < nn; jj++)
        {
          if (sym && (TYPE_RETVAL_PTR_IDX(SYMBOL_TYPE(sym)) == jj))
            continue;
          parm_types[kk++] = method ?
                             (sym ?
                              TYPE_FIELD_TYPE (SYMBOL_TYPE (sym), jj) :
                              TYPE_FIELD_TYPE (fns_ptr[ix].type, jj))
                             : TYPE_FIELD_TYPE (SYMBOL_TYPE (oload_syms[ix]), jj);
        }
      
      /* Compare parameter types to supplied argument types */
      bv = rank_function (parm_types, nparms, arg_types, nargs);

      if (!oload_champ_bv)
	{
	  oload_champ_bv = bv;
	  oload_champ = 0;
	}
      else
	/* See whether current candidate is better or worse than previous best */
	switch (compare_badness (bv, oload_champ_bv))
	  {
	  case 0:
	    oload_ambiguous = 1;	/* top two contenders are equally good */
	    break;
	  case 1:
	    oload_ambiguous = 2;	/* incomparable top contenders */
            /* CR1000903410:In this situation it is safer to make champion
	       whose number of parameters closely matches to user supplied
       	       number of arguments.
   	    */
	    if (bv->rank[0] < oload_champ_bv->rank[0])
	      {
	        oload_champ = ix;
	        oload_champ_bv = bv;
	      } 
  	    break;
	  case 2:
	    oload_champ_bv = bv;	/* new champion, record details */
	    oload_ambiguous = 0;
	    oload_champ = ix;
	    break;
	  case 3:
	  default:
	    break;
	  }
      free (parm_types);
     
      if (overload_debug)
       {
         if (method)
	 fprintf_filtered (gdb_stderr,"Overloaded method instance %s, #"
                           " of parms %d\n", fns_ptr[ix].physname, nparms);
      else
         fprintf_filtered (gdb_stderr,"Overloaded function instance %s #"
          " of parms %d\n", SYMBOL_DEMANGLED_NAME (oload_syms[ix]), nparms);
      for (jj = 0; jj < nargs; jj++)
	fprintf_filtered (gdb_stderr,"...Badness @ %d : %d\n", jj,
			  bv->rank[jj]);
      fprintf_filtered (gdb_stderr,"Overload resolution champion is %d,"
	 		" ambiguous? %d\n", oload_champ, oload_ambiguous);
       }
      
#ifdef HP_IA64
      /* If the sym is oload champ then point the current_oload_champ
	 to the sym */
      if (oload_champ == ix) 
       current_oload_champ_sym = sym;
#endif
    } /* end loop over all candidates */

  /* Check how bad the best match is */
  for (ix = 1; ix <= nargs; ix++)
    {
      if (oload_champ_bv->rank[ix] == 10)
	oload_non_standard = 1;	/* non-standard type conversions needed */
      else if (oload_champ_bv->rank[ix] >= 100) /* JAGaf49951: anything greater than 100 makes it truly mismatched. */ 
	oload_incompatible = 1;	/* truly mismatched types */
    }
  if (oload_incompatible)
    {
      if (method)
	error ("Cannot resolve method %s%s%s to any overloaded instance",
	       obj_type_name,
	       (obj_type_name && *obj_type_name) ? "::" : "",
	       name);
      else
	error ("Cannot resolve function %s to any overloaded instance",
	       func_name);
    }
  else if (oload_non_standard)
    {
      if (method)
	warning ("Using non-standard conversion to match method %s%s%s to supplied arguments",
		 obj_type_name,
		 (obj_type_name && *obj_type_name) ? "::" : "",
		 name);
      else
	warning ("Using non-standard conversion to match function %s to supplied arguments",
		 func_name);
    }

  if (method)
    {
      /* JAGae28186 if the virtual function is called through struct objects,
         handle it in the normal way. */  
      if (method == POLYMORPHIC_CALL && TYPE_FN_FIELD_VIRTUAL_P (fns_ptr, oload_champ))
	*valp = value_virtual_fn_field (&temp, fns_ptr, oload_champ, basetype, boffset);
      /* RM: Don't try to do this pointer adjustment for static
         functions -- the object we have may not be an lvalue (eg, the
         dummy object created for A::foo()) */
      else if (TYPE_FN_FIELD_STATIC_P (fns_ptr, oload_champ))
        *valp = value_fn_field (0, fns_ptr, oload_champ, basetype, boffset);
      else
	*valp = value_fn_field (&temp, fns_ptr, oload_champ, basetype, boffset);
    }
  else
    {
      *symp = oload_syms[oload_champ];
      free (func_name);
    }

#ifdef HP_IA64
/* Check to see we are returning a proper unoptimized symbol for the function
   after return from value_virtual_fn_field() or value_fn_field() or 
   value_fn_field(). If we are about to return an unoptimized function symbol
   then, point valp->type to the unoptimized symbol we found while comparing
   the function parameters type against argments type given by the user.
*/
/* NOTE: Do not do this for virtual functions (POLYMORPHIC_CALL)
   because virtual functions are searched and found through vtable and that
   is taken care by above value_virtual_fn_field().
   If we don't do this we will overwrite the virtual function type with 
   NULL symbol type returned from lookup_symbol_from_unoptimized_objfile() as we 
   will get NULL for vritual functions in lookup_symbol().
*/     
  if (valp && current_oload_champ_sym && (method != POLYMORPHIC_CALL))
   {
    if (TYPE_NFIELDS (VALUE_TYPE (*valp)) != 
	TYPE_NFIELDS (SYMBOL_TYPE (current_oload_champ_sym))) 
     VALUE_TYPE (*valp) = SYMBOL_TYPE (current_oload_champ_sym);
   } 
#endif
return oload_incompatible ? 100 : (oload_non_standard ? 10 : 0);
}

/* C++: return 1 is NAME is a legitimate name for the destructor
   of type TYPE.  If TYPE does not have a destructor, or
   if NAME is inappropriate for TYPE, an error is signaled.  */
int
destructor_name_p (const char *name, const struct type *type)
{
  /* destructors are a special case.  */

  if (name[0] == '~')
    {
      char *dname = type_name_no_tag (type);
      char *cp = strchr (dname, '<');
      char *basename, *tmp_basename;

      /* RM: strip off leading namespaces/nested classes from dname */
      basename = strstr (dname, "::");
      if (basename)
	{
	  while ((tmp_basename = strstr (basename + 2, "::")) != NULL)
	    basename = tmp_basename;
	  dname = basename + 2;
	}

      /* Do not compare the template part for template classes.  */
      if (cp != NULL)
	{
#ifndef HP_IA64
          unsigned int len = cp - dname;
	  if (strlen (name + 1) != len || !STREQN (dname, name + 1, len))
	    error ("name of destructor must equal name of class");
	  else
	    return 1;
#endif
	}

#ifdef HP_IA64
	/* Poorva: Revisit when template stuff is in place in the compiler */
        if (strcmp_iw (dname, name + 1))
	  error ("name of destructor must equal name of class");
        else
	  return 1;
#endif
    }
  return 0;
}

/* Helper function for check_field: Given TYPE, a structure/union,
   return 1 if the component named NAME from the ultimate
   target structure/union is defined, otherwise, return 0. */

static int
check_field_in (register struct type *type, const char *name)
{
  register int i;

  for (i = TYPE_NFIELDS (type) - 1; i >= TYPE_N_BASECLASSES (type); i--)
    {
      char *t_field_name = TYPE_FIELD_NAME (type, i);
      if (t_field_name && (strcmp_iw (t_field_name, name) == 0))
	return 1;
    }

  /* C++: If it was not found as a data field, then try to
     return it as a pointer to a method.  */

  /* Destructors are a special case.  */
  if (destructor_name_p (name, type))
    {
      int m_index, f_index;

      return get_destructor_fn_field (type, &m_index, &f_index);
    }

  for (i = TYPE_NFN_FIELDS (type) - 1; i >= 0; --i)
    {
      if (strcmp_iw (TYPE_FN_FIELDLIST_NAME (type, i), name) == 0)
	return 1;
    }

  for (i = TYPE_N_BASECLASSES (type) - 1; i >= 0; i--)
    {
      /* Srikanth, make sure that we are not fiddling with an opaque type.
         This problem is showing up in Intel. Jul 15th 2002.
      */
      struct type * base = TYPE_BASECLASS (type, i);
      base = check_typedef (base);
      if (check_field_in (base, name))
        return 1;
    }

  return 0;
}


/* C++: Given ARG1, a value of type (pointer to a)* structure/union,
   return 1 if the component named NAME from the ultimate
   target structure/union is defined, otherwise, return 0.  */

int
check_field (register value_ptr arg1, const char *name)
{
  register struct type *t;

  COERCE_ARRAY (arg1);

  t = VALUE_TYPE (arg1);

  /* Follow pointers until we get to a non-pointer.  */

  for (;;)
    {
      CHECK_TYPEDEF (t);
      if (TYPE_CODE (t) != TYPE_CODE_PTR && TYPE_CODE (t) != TYPE_CODE_REF)
	break;
      t = TYPE_TARGET_TYPE (t);
    }

  if (TYPE_CODE (t) == TYPE_CODE_MEMBER)
    error ("not implemented: member type in check_field");

  if (TYPE_CODE (t) != TYPE_CODE_STRUCT
      && TYPE_CODE (t) != TYPE_CODE_UNION
      && TYPE_CODE (t) != TYPE_CODE_CLASS)
    error ("Internal error: `this' is not an aggregate");

  return check_field_in (t, name);
}

/* C++: Given an aggregate type CURTYPE, and a member name NAME,
   return the address of this member as a "pointer to member"
   type.  If INTYPE is non-null, then it will be the type
   of the member we are looking for.  This will help us resolve
   "pointers to member functions".  This function is used
   to resolve user expressions of the form "DOMAIN::NAME".  */
extern int destructor_prefix_p(char *name,struct type *type);
value_ptr
value_struct_elt_for_reference (struct type *domain, int offset, struct type *curtype,
			 	char *name, struct type *intype,
				int look_for_this)
{
  register struct type *t = check_typedef (curtype);
  register int i;
  value_ptr v;
  int request_for_destructor = 0;
  if (TYPE_CODE (t) != TYPE_CODE_STRUCT
      && TYPE_CODE (t) != TYPE_CODE_UNION
      && TYPE_CODE (t) != TYPE_CODE_CLASS)
    error ("Internal error: non-aggregate type to value_struct_elt_for_reference");

  for (i = TYPE_NFIELDS (t) - 1; i >= TYPE_N_BASECLASSES (t); i--)
    {
      char *t_field_name = TYPE_FIELD_NAME (t, i);

      if (t_field_name && STREQ (t_field_name, name))
	{
	  value_ptr argp;
	  if (TYPE_FIELD_STATIC (t, i))
	    {
	      v = value_static_field (t, i);
	      if (v == NULL)
		error ("Internal error: could not find static variable %s",
		       name);
	      return v;
	    }
	  if (TYPE_FIELD_PACKED (t, i))
	    error ("pointers to bitfield members not allowed");

          if ((hp_som_som_object_present || hp_dwarf2_object_present) && look_for_this)
            {
              /* It can be a class data member. */
              argp = value_of_this(1);
              if (argp != 0)
                {
                  char class_name[80];
                  strcat(class_name, "data member of ");
                  strcat(class_name, domain->tag_name);
                  v = value_struct_elt (&argp, NULL, name, NULL,
                                        class_name);
                  if (v!=0)
                  return v;
                }
            }

	  return value_from_longest
	    (lookup_reference_type (lookup_member_type (TYPE_FIELD_TYPE (t, i),
							domain)),
	     offset + (LONGEST) (TYPE_FIELD_BITPOS (t, i) >> 3));
	}
    }

  /* C++: If it was not found as a data field, then try to
     return it as a pointer to a method.  */

  /* Destructors are a special case.  */
  if (name [0] == '~') 
    {
    #ifdef HP_IA64
	name++;
    #endif
	request_for_destructor = 1;
    }
  if (destructor_name_p (name, t))
    {
      error ("member pointers to destructors not implemented yet");
    }

  /* Perform all necessary dereferencing.  */
  while (intype && TYPE_CODE (intype) == TYPE_CODE_PTR)
    intype = TYPE_TARGET_TYPE (intype);

  for (i = TYPE_NFN_FIELDS (t) - 1; i >= 0; --i)
    {
      char *t_field_name = TYPE_FN_FIELDLIST_NAME (t, i);
      char dem_opname[64];
      
      if (strncmp (t_field_name, "__", 2) == 0 ||
	  strncmp (t_field_name, "op", 2) == 0 ||
	  strncmp (t_field_name, "type", 4) == 0)
	{
	  if (cplus_demangle_opname (t_field_name, dem_opname, DMGL_ANSI))
	    t_field_name = dem_opname;
	  else if (cplus_demangle_opname (t_field_name, dem_opname, 0))
	    t_field_name = dem_opname;
	}
      if (t_field_name && STREQ (t_field_name, name))
	{
	  int j = TYPE_FN_FIELDLIST_LENGTH (t, i);
	  struct fn_field *f = TYPE_FN_FIELDLIST1 (t, i);

	  /* Consider all the matches and let the user pick. */
	  if (intype == 0 && j > 1)
	    {
	      char* arg, *arg1;
	      char *prompt;
	      int choice = 0;
	      struct symbol *sym;
	      struct symtab_and_line sal;
	      int des_m = 0;
	      int has_destructor = 0; 
	      /* Choices are [0],[1],..,[nelts+1] */
              if (!request_for_destructor) printf_filtered ("[0] cancel\n");
	      while (choice < j)
		{
		  sym = lookup_symbol (TYPE_FN_FIELD_PHYSNAME (f, choice),
				0, VAR_NAMESPACE, 0, NULL);
		  if (!sym)
		    {
		      choice++;
		      continue;
		    }
		  /* JAGaf47282: Do not show destructor along with constructor list on IA */
		  des_m = destructor_prefix_p (TYPE_FN_FIELD_PHYSNAME (f, choice), t);
		  if (des_m)
		    {
		       has_destructor = choice; 
		       if (request_for_destructor)
		          return read_var_value(sym,0);
                    }
		  else if(!des_m && !request_for_destructor) 
		    { 
	                sal = find_function_start_sal (sym, 1);
		        printf_filtered ("[%d] %s at %s:%d\n",
                             (choice + 1),
			     SYMBOL_SOURCE_NAME (sym),
                             sal.symtab->filename,
                             sal.line);
		    }
		    choice++;
		}
	      if ((prompt = getenv ("PS2")) == NULL)
                {
                  prompt = "> ";
                }

              arg = command_line_input (prompt, 0, "overload-choice");
	      if (arg == 0 || *arg == 0)
    		error_no_arg ("Select a choice number");

	      arg1 = arg;
      	      while (*arg1 >= '0' && *arg1 <= '9')
                arg1++;
              if (*arg1 && *arg1 != ' ' && *arg1 != '\t')
        	error ("Argument must be choice numbers.");

	      choice = atoi (arg);
	      if (choice == 0)
        	error ("cancelled");
	      if ((has_destructor && choice > j-1) || choice > j)
		error ("Argument must be given choice numbers");
	      /* Now set j to chosen entry. Adjust the choice,
	       if destructor present along with constructors */
	      j = (has_destructor && has_destructor == choice) ? choice : choice - 1;
	    }
	  else if (intype)
	    {
	      while (j--)
		if (TYPE_FN_FIELD_TYPE (f, j) == intype)
		  break;
	      if (j < 0)
		error ("no member function matches that type instantiation");
	    }
	  else
	    j = 0;

	  if (TYPE_FN_FIELD_STUB (f, j))
	    check_stub_method (t, i, j);
	  if (TYPE_FN_FIELD_VIRTUAL_P (f, j))
	    {
	      return value_from_longest
		(lookup_reference_type
		 (lookup_member_type (TYPE_FN_FIELD_TYPE (f, j),
				      domain)),
		 (LONGEST) METHOD_PTR_FROM_VOFFSET (TYPE_FN_FIELD_VOFFSET (f, j)));
	    }
	  else
	    {
	      struct symbol *s;
	      s = lookup_symbol (TYPE_FN_FIELD_PHYSNAME (f, j),
						0, VAR_NAMESPACE, 0, NULL);

	      if (s == NULL)
		{
		  v = 0;
		}
	      else
		{
		  v = read_var_value (s, 0);
		}
	      return v;
	    }
	}
    }
  for (i = TYPE_N_BASECLASSES (t) - 1; i >= 0; i--)
    {
      value_ptr v;
      int base_offset;

      if (BASETYPE_VIA_VIRTUAL (t, i))
	base_offset = 0;
      else
	base_offset = (int) (TYPE_BASECLASS_BITPOS (t, i) / 8);
      v = value_struct_elt_for_reference (domain,
					  offset + base_offset,
					  TYPE_BASECLASS (t, i),
					  name,
					  intype,
					  look_for_this);
      if (v)
	return v;
    }
  return 0;
}


/* Find the real run-time type of a value using RTTI.
 * V is a pointer to the value.
 * A pointer to the struct type entry of the run-time type
 * is returneed.
 * FULL is a flag that is set only if the value V includes
 * the entire contents of an object of the RTTI type.
 * TOP is the offset to the top of the enclosing object of
 * the real run-time type.  This offset may be for the embedded
 * object, or for the enclosing object of V.
 * USING_ENC is the flag that distinguishes the two cases.
 * If it is 1, then the offset is for the enclosing object,
 * otherwise for the embedded object.
 *
 */

struct type *
value_rtti_type (value_ptr v, int *full, int *top, int *using_enc)
{
  struct type *known_type;
  struct type *rtti_type;
  CORE_ADDR coreptr;
  value_ptr vp;
  int using_enclosing = 0;
  CORE_ADDR top_offset = 0;
  char rtti_type_name[256];
  int str_index = 0;
    
  if (full)
    *full = 0;
  if (top)
    *top = -1;
  if (using_enc)
    *using_enc = 0;

  /* Get declared type */
  known_type = VALUE_TYPE (v);
  CHECK_TYPEDEF (known_type);
  /* RTTI works only or class objects */
  if (TYPE_CODE (known_type) != TYPE_CODE_CLASS &&  (TYPE_CODE (known_type) != TYPE_CODE_STRUCT))
    return NULL;

  /* If neither the declared type nor the enclosing type of the
   * value structure has a HP ANSI C++ style virtual table,
   * let G++ support code take a shot. */
  if (!TYPE_HAS_VTABLE (known_type))
    {
      known_type = VALUE_ENCLOSING_TYPE (v);
      CHECK_TYPEDEF (known_type);
      if (   (   (TYPE_CODE (known_type) == TYPE_CODE_CLASS) 
	      || (TYPE_CODE (known_type) == TYPE_CODE_STRUCT)) 
	  && TYPE_HAS_VTABLE (known_type))
	using_enclosing = 1;
    }


  /* The following logic was added for LRE because there is similar logic
   * in FSF gdb for IPF and we get into trouble if we don't bail out.
   * This code causes problems in other passes if not guarded by
   * IS_TARGET_LRE/IS_LRE_HACK.  IS_LRE_HACK is here to mark that
   * it is unclear this logic should be here and we might need to fix the
   * ways it gets broken if we don't bail out.  See inherit.exp pass 22.
   */

  if (IS_LRE_HACK)
    {
      if (TYPE_VPTR_FIELDNO(known_type) < 0)
	fill_in_vptr_fieldno(known_type);

      if (TYPE_VPTR_FIELDNO(known_type) < 0)
	return NULL;
    }

  if (TYPE_HAS_VTABLE(known_type))
    {
      char * demang_name = NULL;
      if (using_enclosing && using_enc)
	*using_enc = 1;


      /* First get the virtual table address */
      if (!is_swizzled) /* LP64 for IPF */
	{
          /* QXCR1000981185: If print object is ON, print cmd gives error struct
             element of other struct. Offset need not be added here. */

	  coreptr = *(CORE_ADDR *) (void *) ((VALUE_CONTENTS_ALL (v))
				    + (using_enclosing ? 0 : 
				       VALUE_EMBEDDED_OFFSET (v)));
	  if (IS_TARGET_LRE)
	    endian_flip (&coreptr, sizeof (coreptr));

	  if (coreptr == 0)
	    return NULL;	/* return silently -- maybe called on gdb-generated value */
	  /* Fetch the top offset of the object */
	  if (new_cxx_abi)
	    {
	      vp = value_at (builtin_type_CORE_ADDR,
			     coreptr - sizeof (CORE_ADDR) * 
			     HP_ACC_NEW_ABI_TOP_OFFSET_OFFSET,
			     VALUE_BFD_SECTION (v));
	      top_offset = *(CORE_ADDR *) (void *) (VALUE_CONTENTS(vp));
	    }
	  else
	    {
	      vp = value_at (builtin_type_CORE_ADDR,
			     coreptr + sizeof (CORE_ADDR) * 
			     HP_ACC_TOP_OFFSET_OFFSET,
			     VALUE_BFD_SECTION (v));
	      top_offset = value_as_long (vp);
	    }
	}
      else /* ILP 32 */
	{
          /* QXCR1000981185: If print object is ON, print cmd gives error struct
             element of other struct. Offset need not be added here. */

	  coreptr = *(unsigned int *) (void *) ((VALUE_CONTENTS_ALL (v))
				    + (using_enclosing ? 0 : 
				       VALUE_EMBEDDED_OFFSET (v)));
	  
	  if (coreptr == 0)
	    return NULL;	/* return silently -- maybe called on gdb-generated value */

	  /* Fetch the top offset of the object */
	  if (new_cxx_abi)
	    {
	      vp = value_at (builtin_type_CORE_ADDR,
			     coreptr - sizeof (CORE_ADDR) *
			     HP_ACC_NEW_ABI_TOP_OFFSET_OFFSET,
			     VALUE_BFD_SECTION (v));
	      top_offset = *(unsigned int *) (void *) (VALUE_CONTENTS(vp));
	    }
	  else
	    {
	      vp = value_at (builtin_type_CORE_ADDR,
			     coreptr + sizeof (unsigned int) * 
			     HP_ACC_TOP_OFFSET_OFFSET,
			     VALUE_BFD_SECTION (v));
	      top_offset = value_as_long (vp);	      
	    }
	}

      if (top)
	*top = (int) top_offset;

      /* Fetch the typeinfo pointer */
      if (!is_swizzled)  /* LP64 for IPF */
	{
	  if (new_cxx_abi)
	    vp = value_at (builtin_type_CORE_ADDR,
			   coreptr - sizeof (CORE_ADDR) * 
			   HP_ACC_NEW_ABI_TYPEINFO_OFFSET,
			   VALUE_BFD_SECTION (v));
	  else
	    vp = value_at (builtin_type_CORE_ADDR,
			   coreptr + sizeof (CORE_ADDR) * 
			   HP_ACC_TYPEINFO_OFFSET,
			   VALUE_BFD_SECTION (v));

	  /* Indirect through the typeinfo pointer and retrieve the pointer
	   * to the string name */
	  coreptr = *(CORE_ADDR *) (void *) (VALUE_CONTENTS (vp));
	}
      else /* ILP32 */
	{
	  if (new_cxx_abi)
	    vp = value_at (builtin_type_CORE_ADDR,
			   coreptr - sizeof (CORE_ADDR) * 
			   HP_ACC_NEW_ABI_TYPEINFO_OFFSET,
			   VALUE_BFD_SECTION (v));
	  else
	    vp = value_at (builtin_type_CORE_ADDR,
			   coreptr + sizeof (unsigned long) * 
			   HP_ACC_TYPEINFO_OFFSET,
			   VALUE_BFD_SECTION (v));

	  /* Indirect through the typeinfo pointer and retrieve the pointer
	   * to the string name */
	  coreptr = *(unsigned int *) (void *) (VALUE_CONTENTS (vp));
	}

      if (IS_TARGET_LRE)
	{
	  endian_flip (&coreptr, sizeof (coreptr));
	}
      /* RM: Sigh. Don't error out, at least print the object as known
	 at compile time */
      if (!coreptr)
	/* error ("Retrieved null typeinfo pointer in trying to determine run-time type"); */
	return 0;
   
      /* sizeof(CORE_ADDR) -> offset of name field */
      if (!is_swizzled) /* LP64 for IPF */
	{
	  vp = value_at (builtin_type_CORE_ADDR,
			 coreptr + sizeof (CORE_ADDR),
			 VALUE_BFD_SECTION (v));
	  
	  coreptr = *(CORE_ADDR *) (void *) (VALUE_CONTENTS (vp));
	}
      else /* ILP32 */
	{
	  vp = value_at (builtin_type_CORE_ADDR,
			 coreptr + sizeof (unsigned int),
			 VALUE_BFD_SECTION (v));
	  
	  coreptr = *(unsigned int *) (void *) (VALUE_CONTENTS (vp));
	}

      if (IS_TARGET_LRE)
	endian_flip (&coreptr, sizeof (coreptr));

      /* QXCR1000807286: downcasts and polymorphic type recognition are broken.
         Following code is added/modified to first try and demangle the 
         internal type-name, "rtti_type_name" obtained from "read_memory_string"
         function call. Then continue the processing with the demangled-name 
         thus obtained. */
      read_memory_string (coreptr, rtti_type_name, 256);
      if (strlen (rtti_type_name) == 0)
        error ("Retrieved null type name from typeinfo");
      demang_name = cplus_demangle (rtti_type_name, DMGL_INTERNAL);
      if (demang_name && strcmp (demang_name, rtti_type_name))
        strcpy (rtti_type_name, demang_name);

      /* search for type */
      rtti_type = lookup_typename (rtti_type_name, (struct block *) 0, 1);

      if (!rtti_type)
	/* RM: Don't error out, it may simply be the case that the real type
	   was declared in a module without debug information. */
	/* error ("Could not find run-time type: invalid type name %s in typeinfo??", rtti_type_name); */
	return 0;
  
      CHECK_TYPEDEF (rtti_type);

      /* Check whether we have the entire object */
      if (full			/* Non-null pointer passed */
	  &&
	  /* Either we checked on the whole object in hand and found the
	     top offset to be zero */
	  (((top_offset == 0) &&
	    using_enclosing &&
	    TYPE_LENGTH (known_type) == TYPE_LENGTH (rtti_type))
	   ||
	   /* Or we checked on the embedded object and top offset was the
	      same as the embedded offset */
	   ((top_offset == VALUE_EMBEDDED_OFFSET (v)) &&
	    !using_enclosing &&
	    TYPE_LENGTH (VALUE_ENCLOSING_TYPE (v)) == TYPE_LENGTH (rtti_type))))

	*full = 1;
    }
  else
    {
    /*
      Right now this is G++ RTTI. Plan on this changing in the
      future as i get around to setting the vtables properly for G++
      compiled stuff. Also, i'll be using the type info functions, 
      which are always right. Deal with it until then.
    */
      CORE_ADDR vtbl;
      struct minimal_symbol *minsym;
      struct symbol *sym;
      char *demangled_name;
      struct type *btype;
      /* If the type has no vptr fieldno, try to get it filled in */
      if (TYPE_VPTR_FIELDNO(known_type) < 0)
	fill_in_vptr_fieldno(known_type);

      /* If we still can't find one, give up */
      if (TYPE_VPTR_FIELDNO(known_type) < 0)
	return NULL;

      /* Make sure our basetype and known type match, otherwise, cast
	 so we can get at the vtable properly.
      */
      btype = TYPE_VPTR_BASETYPE (known_type);
      CHECK_TYPEDEF (btype);
      if (btype != known_type )
	{
	  v = value_cast (btype, v);
	  if (using_enc)
	    *using_enc=1;
	}
      /*
	We can't use value_ind here, because it would want to use RTTI, and 
	we'd waste a bunch of time figuring out we already know the type.
        Besides, we don't care about the type, just the actual pointer
      */
      if (VALUE_ADDRESS (value_field (v, TYPE_VPTR_FIELDNO (known_type))) == 0)
	return NULL;

      /*
	 If we are enclosed by something that isn't us, adjust the
	 address properly and set using_enclosing.
      */
      if (VALUE_ENCLOSING_TYPE(v) != VALUE_TYPE(v))
	{
	  value_ptr tempval;
	  tempval=value_field(v,TYPE_VPTR_FIELDNO(known_type));
	  VALUE_ADDRESS(tempval)+=(TYPE_BASECLASS_BITPOS(known_type,TYPE_VPTR_FIELDNO(known_type))/8);
	  vtbl=value_as_pointer(tempval);
	  using_enclosing=1;
	}
      else
	{
	  vtbl=value_as_pointer(value_field(v,TYPE_VPTR_FIELDNO(known_type)));
	  using_enclosing=0;
	}

      /* Try to find a symbol that is the vtable */
      minsym=lookup_minimal_symbol_by_pc(vtbl);
      if (minsym==NULL || (demangled_name=SYMBOL_NAME(minsym))==NULL || !VTBL_PREFIX_P(demangled_name))
	return NULL;

      /* If we just skip the prefix, we get screwed by namespaces */
      demangled_name=cplus_demangle(demangled_name,DMGL_PARAMS|DMGL_ANSI);
      *(strchr(demangled_name,' '))=0;

      /* Lookup the type for the name */
      rtti_type=lookup_typename(demangled_name, (struct block *)0,1);

      if (rtti_type==NULL)
	return NULL;

      if (TYPE_N_BASECLASSES(rtti_type) > 1 &&  full && (*full) != 1)
	{
	  if (top)
	    *top= (int) (TYPE_BASECLASS_BITPOS(rtti_type,TYPE_VPTR_FIELDNO(rtti_type))/8);
	  if (top && ((*top) >0))
	    {
	      if (TYPE_LENGTH(rtti_type) > TYPE_LENGTH(known_type))
		{
		  if (full)
		    *full=0;
		}
	      else
		{
		  if (full)
		    *full=1;
		}
	    }
	}
      else
	{
	  if (full)
	    *full=1;
	}
      if (using_enc)
	*using_enc=using_enclosing;
    }
  
  return rtti_type;
}

/* Given a pointer value V, find the real (RTTI) type
   of the object it points to.
   Other parameters FULL, TOP, USING_ENC as with value_rtti_type()
   and refer to the values computed for the object pointed to. */

struct type *
value_rtti_target_type (value_ptr v, int *full, int *top, int *using_enc)
{
  value_ptr target;
  target = value_ind (v);
  return value_rtti_type (target, full, top, using_enc);
}

/* Given a value pointed to by ARGP, check its real run-time type, and
   if that is different from the enclosing type, create a new value
   using the real run-time type as the enclosing type (and of the same
   type as ARGP) and return it, with the embedded offset adjusted to
   be the correct offset to the enclosed object
   RTYPE is the type, and XFULL, XTOP, and XUSING_ENC are the other
   parameters, computed by value_rtti_type(). If these are available,
   they can be supplied and a second call to value_rtti_type() is avoided.
   (Pass RTYPE == NULL if they're not available */

value_ptr
value_full_object (value_ptr argp, struct type *rtype, int xfull, int xtop, int xusing_enc)
{
  struct type *real_type;
  int full = 0;
  int top = -1;
  int using_enc = 0;
  value_ptr new_val;
  int abs_val_top;

  if (rtype)
    {
      real_type = rtype;
      full = xfull;
      top = xtop;
      using_enc = xusing_enc;
    }
  else
    real_type = value_rtti_type (argp, &full, &top, &using_enc);

  /* If no RTTI data, or if object is already complete, do nothing */
  if (!real_type || real_type == VALUE_ENCLOSING_TYPE (argp))
    return argp;

  /* If we have the full object, but for some reason the enclosing
     type is wrong, set it */ /* pai: FIXME -- sounds rather iffy! */
  if (full)
    {
      VALUE_ENCLOSING_TYPE (argp) = real_type;
      return argp;
    }

  /* Check if object is in memory */
  if (VALUE_LVAL (argp) != lval_memory)
    {
      warning ("Couldn't retrieve complete object of RTTI type %s; object may be in register(s).", TYPE_NAME (real_type));

      return argp;
    }

  /* If can't evaluate the value, return the right type. */
  if (VALUE_AVAILABILITY (argp) != VA_AVAILABLE)
    return argp;

  /* All other cases -- retrieve the complete object */
  /* Go back by the computed top_offset from the beginning of the object,
     adjusting for the embedded offset of argp if that's what value_rtti_type
     used for its computation. */

#ifndef HP_IA64
  new_val = value_at_lazy (real_type, VALUE_ADDRESS (argp) - top +
			   (using_enc ? 0 : VALUE_EMBEDDED_OFFSET (argp)),
			   VALUE_BFD_SECTION (argp));
  VALUE_EMBEDDED_OFFSET (new_val) = using_enc ? top + VALUE_EMBEDDED_OFFSET (argp) : top;

#else
  new_val = value_at_lazy (real_type, VALUE_ADDRESS (argp) + top +
			   (using_enc ? 0 : VALUE_EMBEDDED_OFFSET (argp)),
			   VALUE_BFD_SECTION (argp));
  abs_val_top = abs(top);
  VALUE_EMBEDDED_OFFSET (new_val) = using_enc ? abs_val_top + VALUE_EMBEDDED_OFFSET (argp) : abs_val_top;
#endif

  VALUE_TYPE (new_val) = VALUE_TYPE (argp);
  return new_val;
}




/* C++: return the value of the class instance variable, if one exists.
   Flag COMPLAIN signals an error if the request is made in an
   inappropriate context.  */

value_ptr
value_of_this (int complain)
{
  struct symbol *func, *sym;
  struct block *b;
  int i;
  static const char funny_this[] = "this";
  value_ptr this;

  if (selected_frame == 0)
    {
      if (complain)
	error ("no frame selected");
      else
	return 0;
    }

  func = get_frame_function (selected_frame);
  if (!func)
    {
      if (complain)
	error ("no `this' in nameless context");
      else
	return 0;
    }

  b = SYMBOL_BLOCK_VALUE (func);
  i = BLOCK_NSYMS (b);
  if (i <= 0)
    {
      if (complain)
	error ("no args, no `this'");
      else
	return 0;
    }

  /* Calling lookup_block_symbol is necessary to get the LOC_REGISTER
     symbol instead of the LOC_ARG one (if both exist).  */
  sym = lookup_block_symbol (b, funny_this, VAR_NAMESPACE);
  if (sym == NULL)
    {
      if (complain)
	error ("current stack frame not in method");
      else
	return NULL;
    }

  this = read_var_value (sym, selected_frame);
  if (this == 0 && complain)
    error ("`this' argument at unknown address");
  return this;
}

/* Create a slice (sub-string, sub-array) of ARRAY, that is LENGTH elements
   long, starting at LOWBOUND.  The result has the same lower bound as
   the original ARRAY.  */

value_ptr
value_slice (value_ptr array, int lowbound, int length)
{
  struct type *slice_range_type, *slice_type, *range_type;
  LONGEST lowerbound, upperbound, offset;
  value_ptr slice;
  struct type *array_type;

  if (VALUE_AVAILABILITY (array) != VA_AVAILABLE)
    return array;

  array_type = check_typedef (VALUE_TYPE (array));
  COERCE_VARYING_ARRAY (array, array_type);
  if (TYPE_CODE (array_type) != TYPE_CODE_ARRAY
      && TYPE_CODE (array_type) != TYPE_CODE_STRING
      && TYPE_CODE (array_type) != TYPE_CODE_BITSTRING)
    error ("cannot take slice of non-array");
  range_type = TYPE_INDEX_TYPE (array_type);
  if (get_discrete_bounds (range_type, &lowerbound, &upperbound) < 0)
    error ("slice from bad array or bitstring");
  if (lowbound < lowerbound || length < 0)
    error ("slice out of range");

  /* FIXME-type-allocation: need a way to free this type when we are
     done with it.  */
  slice_range_type = create_range_type ((struct type *) NULL,
					TYPE_TARGET_TYPE (range_type),
					lowbound, lowbound + length - 1);
  if (TYPE_CODE (array_type) == TYPE_CODE_BITSTRING)
    {
      int i;
      slice_type = create_set_type ((struct type *) NULL, slice_range_type);
      TYPE_CODE (slice_type) = TYPE_CODE_BITSTRING;
      slice = value_zero (slice_type, not_lval);
      for (i = 0; i < length; i++)
	{
	  int element = value_bit_index (array_type,
					 VALUE_CONTENTS (array),
					 lowbound + i);
	  if (element < 0)
	    error ("internal error accessing bitstring");
	  else if (element > 0)
	    {
	      int j = i % TARGET_CHAR_BIT;
	      if (BITS_BIG_ENDIAN)
		j = TARGET_CHAR_BIT - 1 - j;
	      VALUE_CONTENTS_RAW (slice)[i / TARGET_CHAR_BIT] |= (1 << j);
	    }
	}
      /* We should set the address, bitssize, and bitspos, so the clice
         can be used on the LHS, but that may require extensions to
         value_assign.  For now, just leave as a non_lval.  FIXME.  */
    }
  else
    {
      struct type *element_type = TYPE_TARGET_TYPE (array_type);
      offset
	= (lowbound - lowerbound) * TYPE_LENGTH (check_typedef (element_type));
      slice_type = create_array_type ((struct type *) NULL, element_type,
				      slice_range_type);
      TYPE_CODE (slice_type) = TYPE_CODE (array_type);
      slice = allocate_value (slice_type);
      if (VALUE_LAZY (array))
	VALUE_LAZY (slice) = 1;
      else
	memcpy (VALUE_CONTENTS (slice), VALUE_CONTENTS (array) + offset,
		TYPE_LENGTH (slice_type));
      if (VALUE_LVAL (array) == lval_internalvar)
	VALUE_LVAL (slice) = lval_internalvar_component;
      else
	VALUE_LVAL (slice) = VALUE_LVAL (array);
      VALUE_ADDRESS (slice) = VALUE_ADDRESS (array);
      VALUE_OFFSET (slice) = (int) (VALUE_OFFSET (array) + offset);
    }
  return slice;
}

/* Assuming chill_varying_type (VARRAY) is true, return an equivalent
   value as a fixed-length array. */

value_ptr
varying_to_slice (value_ptr varray)
{
  struct type *vtype = check_typedef (VALUE_TYPE (varray));
  LONGEST length = unpack_long (TYPE_FIELD_TYPE (vtype, 0),
				VALUE_CONTENTS (varray)
				+ TYPE_FIELD_BITPOS (vtype, 0) / 8);
  return value_slice (value_primitive_field (varray, 0, 1, vtype), 0, (int) length);
}

/* Create a value for a FORTRAN complex number.  Currently most of
   the time values are coerced to COMPLEX*16 (i.e. a complex number
   composed of 2 doubles.  This really should be a smarter routine
   that figures out precision inteligently as opposed to assuming
   doubles. FIXME: fmb */

value_ptr
value_literal_complex (value_ptr arg1, value_ptr arg2, struct type *type)
{
  register value_ptr val;
  struct type *real_type = TYPE_TARGET_TYPE (type);

  val = allocate_value (type);
  arg1 = value_cast (real_type, arg1);
  arg2 = value_cast (real_type, arg2);

  memcpy (VALUE_CONTENTS_RAW (val),
	  VALUE_CONTENTS (arg1), TYPE_LENGTH (real_type));
  memcpy (VALUE_CONTENTS_RAW (val) + TYPE_LENGTH (real_type),
	  VALUE_CONTENTS (arg2), TYPE_LENGTH (real_type));
  return val;
}

/* Cast a value from the given complex data type into the desired type. 
   Handles the normal complex types and __float80 _Complex.
   The cast into type, type, should not be complex
   */

static value_ptr cast_from_complex (struct type *type, register value_ptr val) 
{ 
  struct type *from_type = VALUE_TYPE (val);
  value_ptr return_val;
  value_ptr temp_complex;
  value_ptr temp_value;
  DOUBLEST	double_num;

  /* cast the value to a long double complex and then create a new value
     from the real part, then cast that value to the desired type.
     Not the most efficient scheme, but the simplest.

     Note that casting to long double _Complex will call cast_into_complex,
     even if we are casting from a complex.  See value_cast.
     */
  
  temp_complex = value_cast (builtin_type_long_double_complex, val);
  memcpy (&double_num, VALUE_CONTENTS (temp_complex), sizeof (DOUBLEST));
  /* value_from_double can only make a floating point type */
  temp_value = value_from_double (builtin_type_long_double, double_num);
  return_val = value_cast (type, temp_value);
  return return_val;
} /* cast_from_complex */

/* Cast a value into the appropriate complex data type. */

static value_ptr
cast_into_complex (struct type *type, register value_ptr val)
{
  struct type *real_type = TYPE_TARGET_TYPE (type);
  struct type *val_type = check_typedef (VALUE_TYPE (val));

#ifdef TARGET_FLOAT80_BIT
  if (TYPE_CODE (type) == TYPE_CODE_FLOAT80_COMPLEX)
    { /* Cast val to a long double _Complex, then make a __float80 _Complex
	 value from that
	 */
      value_ptr temp_val;
      value_ptr return_val;
      DOUBLEST	real, imaginary;
      __float80 float80_real, float80_imaginary;
      __float80* write_val_ptr;

      temp_val = value_cast (builtin_type_long_double_complex, val);
      memcpy (&real, VALUE_CONTENTS (temp_val), sizeof (real));
      float80_real = real;
      memcpy (&imaginary, 
	      VALUE_CONTENTS (temp_val) + sizeof(real), 
	      sizeof (real));
      float80_imaginary = imaginary;
      return_val = allocate_value (builtin_type_float80_complex);
      memcpy (VALUE_CONTENTS_RAW (return_val), 
	      &float80_real,
      	      sizeof(__float80));
      memcpy (VALUE_CONTENTS_RAW (return_val) + sizeof(__float80), 
	      &float80_imaginary,
      	      sizeof(__float80));
      return return_val;
    }
#endif

  if (TYPE_CODE (val_type) == TYPE_CODE_COMPLEX)
    {
      struct type *val_real_type = TYPE_TARGET_TYPE (VALUE_TYPE (val));
      value_ptr re_val = allocate_value (val_real_type);
      value_ptr im_val = allocate_value (val_real_type);

      memcpy (VALUE_CONTENTS_RAW (re_val),
	      VALUE_CONTENTS (val), TYPE_LENGTH (val_real_type));
      memcpy (VALUE_CONTENTS_RAW (im_val),
	      VALUE_CONTENTS (val) + TYPE_LENGTH (val_real_type),
	      TYPE_LENGTH (val_real_type));

      return value_literal_complex (re_val, im_val, type);
    }
  else if (TYPE_CODE (val_type) == TYPE_CODE_FLT
	   || TYPE_CODE (val_type) == TYPE_CODE_INT)
    return value_literal_complex (val, value_zero (real_type, not_lval), type);
  else
    error ("cannot cast non-number to complex");
  /* silence aCC6 error for missing return statement */
  return NULL;
}

#ifdef HP_IA64
/* Cast the given value into decimal float value. */
value_ptr
cast_into_decfloat (struct type *type, register value_ptr val)
{
  struct type *val_type = check_typedef (VALUE_TYPE (val));

  if (TYPE_CODE (type) == TYPE_CODE_DECFLOAT)
    return value_from_decfloat (type, value_as_decfloat (val, 
                                                         TYPE_LENGTH (type)));
  else
    error ("Cannot cast non-number to decimal float");

  return NULL;
}

/* Cast the decimal float value into the required type. */
static value_ptr
cast_from_decfloat (struct type *type, register value_ptr val)
{
  struct type *val_type = check_typedef (VALUE_TYPE (val));
  int len = TYPE_LENGTH (val_type);
  enum type_code code = TYPE_CODE (type);
  int nosign = TYPE_UNSIGNED (type);
  int tlen = TYPE_LENGTH (type);
  DECFLOAT *dval = extract_decfloat (VALUE_CONTENTS (val), len);
  switch (code)
    {
    case TYPE_CODE_TYPEDEF:
      cast_from_decfloat (check_typedef (type), val);
      break;
    case TYPE_CODE_ENUM:
    case TYPE_CODE_BOOL:
    case TYPE_CODE_INT:
    case TYPE_CODE_CHAR:
    case TYPE_CODE_RANGE:
      if (nosign)
        {
          return value_from_ulongest (type,
				     unsigned_int_from_dfp (dval, len, tlen));
        }
      else
        {
          return value_from_longest (type,
				      signed_int_from_dfp (dval, len, tlen));
        }
    case TYPE_CODE_FLT:
      {
        return value_from_double (type,
				  bfp_from_dfp (dval, len, tlen));
      }
#ifdef TARGET_FLOAT80_BIT
    case TYPE_CODE_FLOAT80:
      {
        return value_from_f80 (f80_from_dfp (dval, len));
                              
      }
#endif
    default:
      error ("Value can't be casted.");
    }
  return 0;
}
#endif

/* Handle Fortran intrinsic conversion functions. 
   AINT and DINT are considered math functions, however, they behave 
   like conversion functions and so are handled here.
 */

value_ptr
value_ftn_cast (value_ptr arg, enum exp_opcode op, enum noside noside)
{
  struct type *type, *to_type = 0;  /* initialize for compiler warning */
  int code;
  int len;
  int invalid_arg = 0;
  value_ptr val;

  type = check_typedef (VALUE_TYPE (arg));
  code = TYPE_CODE (type);
  len = TYPE_LENGTH (type);

  switch (op)
    {
    case UNOP_FTN_AINT: /* truncation */
      if (code != TYPE_CODE_FLT)
        invalid_arg = 1;
      to_type = type;
      break;

    case UNOP_FTN_DINT: /* truncation for real*8 */
      if (!(code == TYPE_CODE_FLT &&
            len == TARGET_DOUBLE_BIT / TARGET_CHAR_BIT))
        invalid_arg = 1;
     to_type = type;
     break;

    case UNOP_FTN_CHAR: /* from integer to character */
      if (code != TYPE_CODE_INT) 
        invalid_arg = 1;
      to_type = builtin_type_f_character;
      break;

    case UNOP_FTN_CMPLX: /* from numeric to complex */
      if (code != TYPE_CODE_INT &&
          code != TYPE_CODE_FLT &&
          code != TYPE_CODE_COMPLEX)
        invalid_arg = 1;
      to_type = builtin_type_f_complex_s8;
      break;

    case UNOP_FTN_DBLE: /* from numeric to real*8 */
      if (code != TYPE_CODE_INT &&
          code != TYPE_CODE_FLT &&
          code != TYPE_CODE_COMPLEX)
        invalid_arg = 1;
      to_type = builtin_type_f_real_s8;
      break;

    case UNOP_FTN_FLOAT: /* from integer to real */
      if (code != TYPE_CODE_INT)
        invalid_arg = 1;
      to_type = builtin_type_f_real;
      break;

    case UNOP_FTN_ICHAR:  /* from character to integer */
      if (code != TYPE_CODE_STRING && type != builtin_type_f_character)
        invalid_arg = 1;
      if (code == TYPE_CODE_STRING)
        /* Extract the character from the string */
        arg = value_from_longest (builtin_type_f_character, 
              unpack_long (builtin_type_f_character, VALUE_CONTENTS (arg)));
      to_type = builtin_type_f_integer;
      break;

    case UNOP_FTN_IDINT: /* from real*8 to integer */
      if (!(code == TYPE_CODE_FLT &&
            len  == TARGET_DOUBLE_BIT / TARGET_CHAR_BIT))
        invalid_arg = 1;
      to_type = builtin_type_f_integer;
      break;

    case UNOP_FTN_IFIX:  /* from real to integer */
      if (code != TYPE_CODE_FLT)
        invalid_arg = 1;
      to_type = builtin_type_f_integer;
      break;

    case UNOP_FTN_INT:  /* from numeric to integer */
      if (code != TYPE_CODE_INT &&
          code != TYPE_CODE_FLT &&
          code != TYPE_CODE_COMPLEX)
        invalid_arg = 1;
      to_type = builtin_type_f_integer;
      break;

    case UNOP_FTN_REAL: /* from numeric to real */
      if (code != TYPE_CODE_INT &&
          code != TYPE_CODE_FLT &&
          code != TYPE_CODE_COMPLEX)
        invalid_arg = 1;
      to_type = builtin_type_f_real;
      break;

    case UNOP_FTN_SNGL: /* from real*8 to real*4 */
      if (!(code == TYPE_CODE_FLT &&
            len  == TARGET_DOUBLE_BIT / TARGET_CHAR_BIT))
        invalid_arg = 1;
      to_type = builtin_type_f_real;
      break;

    default:
      error ("value_ftn_cast: unknown op = %d", op);
    }

  if (invalid_arg)
    error ("Invalid argument type");

  if (noside == EVAL_AVOID_SIDE_EFFECTS)
    return value_zero (to_type, not_lval);

  if (op == UNOP_FTN_AINT || op == UNOP_FTN_DINT)
    to_type = builtin_type_long_long;

  val = value_cast (to_type, arg);

  if (op == UNOP_FTN_AINT || op == UNOP_FTN_DINT)
    val = value_cast (type, val);

  return val;
}

void
_initialize_valops ()
{
  add_show_from_set
    (add_set_cmd ("overload-resolution", class_support, var_boolean, (char *) &overload_resolution,
		  "Set overload resolution in evaluating C++ functions.\n\n\
Usage:\nTo set new value:\n\tset overload-resolution [on | off]\nTo see current value:\
\n\tshow overload-resolution\n", &setlist), &showlist);

  overload_resolution = 1;

  add_show_from_set (
  add_set_cmd ("unwindonsignal", no_class, var_boolean,
	       (char *) &unwind_on_signal_p,
"Set unwinding of stack if a signal is received while in a call dummy.\n\nUsage:\
\nTo set new value:\n\tset unwindonsignal [on | off]\nTo see current value:\n\t\
show unwindonsignal\n\nThe unwindonsignal lets the user determine what gdb should\
 do if a signal is\nreceived while in a function called from gdb (call dummy).\n\
If set, gdb unwinds the stack and restore the context to what it was before the call.\
\nThe default is to stop in the frame where the signal was received.\n", &setlist),
		     &showlist);
}
