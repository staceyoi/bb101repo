/*
 *  @(#) dlfcn.h 1.0
 *
 *  (c) Copyright 1996 Hewlett-Packard Co., All Rights Reserved.
 */

#ifndef _DLFCN_INCLUDED
#define _DLFCN_INCLUDED
#include <sys/types.h>
#ifdef __cplusplus
extern "C" {
#endif

#ifdef __LP64__
typedef unsigned long		UINT64;
#else
typedef unsigned long long	UINT64;
#endif

/* A Load module descriptor structure */
#ifndef _LOAD_MODULE_DESC_T
#define _LOAD_MODULE_DESC_T
struct load_module_desc {
   UINT64	text_base;
   UINT64	text_size;
   UINT64	data_base;
   UINT64	data_size;
   UINT64	unwind_base;
   UINT64	linkage_ptr;
   UINT64	phdr_base;
   UINT64       tls_size;
   UINT64       tls_start_addr;
   UINT64       unwind_size;
};
#endif /* ! _LOAD_MODULE_DESC_T */

#if defined(__STDC__) || defined(__cplusplus)
extern void*	dlopen(char* filename,
                       unsigned int flags);
extern void*	dlsym(void* handle,
	              const char* name);
extern int	dlclose(void* handle);
extern void*	dlget(unsigned int index, 
                      struct load_module_desc* desc,
                      unsigned int desc_size);
extern UINT64	dlmodinfo(UINT64 ip_value,
                          struct load_module_desc* desc,
                          size_t desc_size,
                          void* (*read_tgt_mem)(void* buffer,
                                                UINT64 ptr,
                                                size_t bufsiz,
                                                int ident),
                          int ident_parm,
                          UINT64 load_map_parm);
extern UINT64	dlgetmodinfo(int index,
			     struct load_module_desc* desc,
			     size_t desc_size,
			     void* (*read_tgt_mem)(void* buffer,
						   UINT64 ptr,
						   size_t bufsiz,
						   int ident),
			     int ident_parm,
			     UINT64 load_map_parm);
extern char*	dlgetname(struct load_module_desc* desc,
                          size_t desc_size,
                          void* (*read_tgt_mem)(void* buffer,
                                                UINT64 ptr,
                                                size_t bufsiz,
                                                int ident),
                          int ident_parm,
                          UINT64 load_map_parm);
extern char*	dlerror(void);
#else /* not __STDC__ || __cplusplus */
extern void*	dlopen();
extern void*	dlsym();
extern int	dlclose();
extern void*	dlget();
extern UINT64	dlmodinfo();
extern UINT64	dlgetmodinfo();
extern char*	dlgetname();
extern char*	dlerror();
#endif /* not __STDC__ || __cplusplus */

/* dlopen flags */
#define RTLD_NOW        0x00000001  /* bind immediately */
#define RTLD_LAZY       0x00000002  /* bind deferred */
#define RTLD_GLOBAL     0x00000004  /* symbols are globally visible */
#define RTLD_LOCAL      0x00000008  /* symbols only visible to this load */

/* dlsym special next handle */
#define RTLD_NEXT	((void*) -1L)  /* start symbol search from the dll
					  next to the caller of dlsym() */
#ifdef __cplusplus
}
#endif
#endif /* _DLFCN_INCLUDED */
