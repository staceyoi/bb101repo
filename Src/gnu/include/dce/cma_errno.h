/*
 * @OSF_COPYRIGHT@
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE in the
 * src directory for the full copyright text.
 */
/*
 * @HP_DCE_VERSION@
 * @(#)Module: cma_errno.h $Revision: /main/HPDCE01/1 $ $Date: 1994/01/24 15:49 $
 */
/*
 * HISTORY
 * $Log: cma_errno.h,v $
 * Revision /main/HPDCE01/1  1994/01/24  15:49  truitt
 * 	merge kk and hpdce01
 * 
 * Revision 1.1.7.4  1993/09/21  12:34:39  truitt
 * 	merge kk and dce1_0_3
 * 	[1993/09/21  12:34:16  truitt]
 *
 * Revision 1.1.8.3  1993/09/15  17:43:28  root
 * 	    Loading drop DCE1_0_3b03
 * 
 * Revision 1.1.7.3  1993/08/04  15:50:24  hopkins
 * 	C++ support:
 * 	    add 'extern "C" { ... }'
 * 	[1993/08/03  18:12:14  hopkins]
 * 
 * Revision 1.1.7.2  1993/07/08  18:12:40  root
 * 	Initial King Kong branch
 * 	[1993/07/08  18:12:05  root]
 * 
 * Revision 1.1.5.2  1993/06/04  19:20:33  hopkins
 * 	Added revision control strings
 * 	[1993/06/04  18:33:18  hopkins]
 * 
 * 	[hinman@sni] - Final merge before bsubmit
 * 	[1993/01/31  17:25:59  hinman]
 * 
 * 	[hinman] - Check in merged SNI version
 * 	[1993/01/11  16:19:39  hinman]
 * 
 * Revision 1.1.1.11  1993/05/04  22:34:03  mgm
 * 	    Loading drop DCE1_0_2FNL
 * 
 * Revision 9.5.1.6  92/09/22  16:06:43  raj
 * 	[raj] On SVR4 redefine errno to cma_errno. This insulates applications from
 * 	internal implementation of errno.
 * 
 * Revision 9.5.3.2  92/09/21  17:50:07  raj
 * 	[raj] On SVR4 redefine errno to cma_errno so that applications are insulated from
 * 	      internal implentation.
 * 
 * Revision 9.5.1.5  92/09/10  16:32:24  hinman
 * 	[hinman] - Fold forward from raj_om
 * 
 * Revision 9.5.2.2  92/09/10  16:01:41  raj
 * 	[raj] merging SVR4 changes into 1.0.2
 * 
 * Revision 9.9  92/09/02  14:02:52  hinman
 * 	Update checkin
 * 
 * Revision 1.1.2.3  1992/08/14  20:21:37  bolinger
 * 	Fix reference to _CMA_REENTRANT_CLIB_ to reflect fact that it's
 * 	always defined (to either 1 or 0).
 * 	[1992/08/14  20:02:59  bolinger]
 * 
 * 	Bring forward 1.0.1 revision 1.1.2.2 (fix for OT defect 1787).
 * 	[1992/08/10  15:21:52  bolinger]
 * 
 * Revision 1.1.2.2  1992/05/08  23:18:11  bolinger
 * 	BL10 CMA sources from DEC.
 * 	[1992/05/08  23:17:57  bolinger]
 * 
 * $EndLog$
 */
/*
 *	%W%	(DEC OSF/1)	%G%
 */
/*
 *  Copyright (c) 1990, 1992 by
 *  Digital Equipment Corporation, Maynard Massachusetts.
 *  All rights reserved.
 *
 *  This software is furnished under a license and may be used and  copied
 *  only  in  accordance  with  the  terms  of  such  license and with the
 *  inclusion of the above copyright notice.  This software or  any  other
 *  copies  thereof may not be provided or otherwise made available to any
 *  other person.  No title to and ownership of  the  software  is  hereby
 *  transferred.
 *
 *  The information in this software is subject to change  without  notice
 *  and  should  not  be  construed  as  a commitment by DIGITAL Equipment
 *  Corporation.
 *
 *  DIGITAL assumes no responsibility for the use or  reliability  of  its
 *  software on equipment which is not supplied by DIGITAL.
 */

/*
 *  FACILITY:
 *
 *	CMA services
 *
 *  ABSTRACT:
 *
 *	Header file for CMA's per-thread errno.
 *
 *  AUTHORS:
 *
 *	Dave Butenhof
 *
 *  CREATION DATE:
 *
 *	11 October 1990
 *
 *  MODIFIED BY:
 *
 *	Dave Butenhof
 *	Paul Curtin
 */

#ifndef CMA_ERRNO
# define CMA_ERRNO

/*
 *  INCLUDE FILES
 */

# include <errno.h>

#ifdef __cplusplus
extern "C" {
#endif

# ifndef EDEADLK
#  define EDEADLK	EWOULDBLOCK
# endif

/*
 * CONSTANTS AND MACROS
 */

/*
 * Errno function
 */
# if !defined(_CMA_REENTRANT_CLIB_) || (_CMA_UNIX_TYPE ==  _CMA__SVR4)
#  ifndef _CMA_NOWRAPPERS_
#   undef errno
#   define errno	*cma_errno()
#  endif

/*
 * TYPEDEFS
 */

/*
 *  GLOBAL DATA
 */

/*
 * INTERNAL INTERFACES
 */

extern int *
cma_errno _CMA_PROTOTYPE_ ((void));

# endif /* !_CMA_REENTRANT_CLIB_ */

#ifdef __cplusplus
}
#endif

#endif
