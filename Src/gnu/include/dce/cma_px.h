/*
 * (c) Copyright 1990, 1991, 1992, 1993 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 *
 * (c) Copyright 1991, 1992 Siemens-Nixdorf Information Systems
 * Burlington, MA, USA
 */
/*
 * @(#)HP DCE/9000 1.7.2
 * @(#)Module: cma_px.h $Revision: /main/HPDCE02/2 $ $Date: 1994/07/07 16:07 UTC $
 */
/*
 */
/*
*/
/*
 *  Copyright (c) 1990. 1991 by
 *  Digital Equipment Corporation, Maynard Massachusetts.
 *  All rights reserved.
 *
 *  This software is furnished under a license and may be used and  copied
 *  only  in  accordance  with  the  terms  of  such  license and with the
 *  inclusion of the above copyright notice.  This software or  any  other
 *  copies  thereof may not be provided or otherwise made available to any
 *  other person.  No title to and ownership of  the  software  is  hereby
 *  transferred.
 *
 *  The information in this software is subject to change  without  notice
 *  and  should  not  be  construed  as  a commitment by DIGITAL Equipment
 *  Corporation.
 *
 *  DIGITAL assumes no responsibility for the use or  reliability  of  its
 *  software on equipment which is not supplied by DIGITAL.
 */

/*
 *  FACILITY:
 *
 *	CMA services
 *
 *  ABSTRACT:
 *
 *	Header file for POSIX wrapper routines
 *
 *  AUTHORS:
 *
 *	Dave Butenhof
 *
 *  CREATION DATE:
 *
 *	18 May 1990
 *
 *  MODIFICATION HISTORY:
 *
 *	001	Webb Scales	25 Septemeber 1990
 *		Put in ifndef/define around POSIX type definitions to prevent
 *		collisions with other pre-POSIX facilities.
 *	002	Paul Curtin	 11 December 1990
 *		Added sigaction work, and split some of cma_px.h off
 *		into cma_sigwait.h
 *	003	Paul Curtin	31 January 1991
 *		Added _CMA_NOWRAPPERS_ conditional statements
 *	004	Dave Butenhof	5 February 1991
 *		To avoid breaking client code (now that this is pulled in
 *		transparently by cma.h, pthread.h, pthread_exc.h), drop all
 *		the header file includes; which means also dropping the
 *		prototypes (oh well).
 *	005	Dave Butenhof	10 May 1991
 *		Remove cma_signal() declaration... the macro is sufficient,
 *		since it will "reroute" the declaration in
 *		/usr/include/signal.h when the client code includes it.
 *	006	Dave Butenhof	23 May 1991
 *		Add conditionals for _CMA_UNIPROCESSOR_ so kernel-thread
 *		versions of DECthreads can still use some wrappers (e.g.,
 *		cma_sigaction()).
 *	007	Webb Scales	10 June 1991
 *		Conditionalize out sigaction macro for kernel threads.
 *	008	Webb Scales	 8 July 1991
 *		Fix typo in os-impl symbol name.
 *	009	Dave Butenhof	19 September 1991
 *		Integrate HPUX CMA5 reverse drop: add check for
 *		_POSIX_REENTRANT_FUNCTIONS to disable wrappers.
 *	010	Dave Butenhof	04 October 1991
 *		Clean up use of _CMA_UNIPROCESSOR_
 *	011	Dave Butenhof	19 November 1991
 *		Remove direct check for _POSIX_REENTRANT_FUNCTIONS; rely on
 *		_CMA_REENTRANT_CLIB_, since we control that. OSF/1 defines
 *		_POSIX_REENTRANT_FUNCTIONS, but OSF DCE reference port isn't
 *		using libc_r.a.
 */


#ifndef CMA_PX
#define CMA_PX

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  INCLUDE FILES
 */

/*
 * CONSTANTS AND MACROS
 */

#if (!_CMA_REENTRANT_CLIB_) || (_CMA_UNIX_TYPE ==  _CMA__SVR4) || (_CMA_VENDOR_ == _CMA__HP)
# if (_CMA_UNIX_TYPE ==  _CMA__SVR4) && (!defined(_CMA_NOWRAPPERS_))
#  if !defined(_REENTRANT_LIBC_) && \
		!defined(_REENTRANT_RTLD_) && \
        !defined(_REENTRANT_MALLOC_) && \
		!defined(_REENTRANT_EXIT_) && \
        !defined(_REENTRANT_STDIO_) && \
		!defined(_REENTRANT_LIBGEN_) && \
        !defined(_REENTRANT_LIBM_) && \
		!defined(_REENTRANT_NSL_) && \
        !defined(_REENTRANT_LIBNSL_) && \
		!defined(_REENTRANT_LIBSOCKET_) && \
        !defined(_REENTRANT_LIBRESOLV_) && \
		!defined(_REENTRANT_TCPIP_)
#   define sigaction cma_sigaction
#  endif
# else
#  ifndef _CMA_NOWRAPPERS_
#   if !_CMA_THREAD_IS_VP_
#    define sigaction cma_sigaction   
#   endif
#  endif
# endif  /*_CMA__SVR4 */
#endif


 
/*
 * TYPEDEFS
 */


#ifdef __hpux

#include <dce/hpdce_platform.h>

#if !defined(_STRUCT_TIMESPEC)
# if __hpux1000p || defined(hp9000s700) || defined(__hp9000s700)
#  include <sys/timers.h>
#  if !defined(_STRUCT_TIMESPEC)
#   define _STRUCT_TIMESPEC
#  endif
# else
#  define _STRUCT_TIMESPEC
   struct timespec {
       unsigned long	tv_sec;		/* seconds */
       long		tv_nsec;	/* and nanoseconds */
   };
# endif
#endif

#if !defined(_TIMESPEC_T_)
# define _TIMESPEC_T_
  typedef struct timespec timespec_t;
#endif

#else	/* !__hpux */

#if _CMA_OSIMPL_ == _CMA__OS_OSF
#include <standards.h>
#endif

#ifdef _AES_SOURCE
# include <sys/timers.h>
#else
# ifndef _TIMESPEC_T_
# define _TIMESPEC_T_
typedef struct timespec {
    unsigned long	tv_sec;		/* seconds */
    long		tv_nsec;	/* and nanoseconds */
    } timespec_t;
# endif
#endif

#endif	/* __hpux */

/*
 * INTERFACES
 */

#ifdef __cplusplus
}
#endif

#endif  /* CMA_PX */
/*  DEC/CMS REPLACEMENT HISTORY, Element CMA_PX.H */
/*  *15   19-NOV-1991 12:18:07 BUTENHOF "Remove tests for _POSIX_REENTRANT_FUNCTIONS" */
/*  *14   14-OCT-1991 13:39:45 BUTENHOF "Refine/fix use of config symbols" */
/*  *13   24-SEP-1991 16:27:42 BUTENHOF "Merge CMA5 reverse IBM/HP/Apollo drops" */
/*  *12    8-JUL-1991 15:46:25 SCALES "Fix typo in os-impl symbol" */
/*  *11   10-JUN-1991 19:54:59 SCALES "Convert to stream format for ULTRIX build" */
/*  *10   10-JUN-1991 19:21:16 BUTENHOF "Fix the sccs headers" */
/*  *9    10-JUN-1991 18:22:56 SCALES "Add sccs headers for Ultrix" */
/*  *8    10-JUN-1991 17:54:54 SCALES "Conditionalize sigaction macro for kernel threads" */
/*  *7     5-JUN-1991 18:38:04 BUTENHOF "Include sys/timers.h for timespec" */
/*  *6    29-MAY-1991 17:02:17 BUTENHOF "Change wrapper macros for MP" */
/*  *5    10-MAY-1991 17:52:06 BUTENHOF "Remove prototype" */
/*  *4     6-FEB-1991 01:33:10 BUTENHOF "Drop prototypes to avoid pulling in signal.h, socket.h, etc." */
/*  *3    31-JAN-1991 16:37:42 CURTIN "added _CMA_NOWRAPPERS_ conditional statments" */
/*  *2    17-DEC-1990 14:34:36 CURTIN "split with cma_sigwait.h" */
/*  *1    12-DEC-1990 21:48:42 BUTENHOF "P1003.4a support" */
/*  DEC/CMS REPLACEMENT HISTORY, Element CMA_PX.H */
