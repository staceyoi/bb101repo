/*
 * (c) Copyright 1990, 1991, 1992, 1993 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
 * COPYRIGHT NOTICE
 * Copyright (c) 1990, 1991, 1992, 1993, 1994 Open Software Foundation, Inc.
 * ALL RIGHTS RESERVED (DCE).  See the file named COPYRIGHT.DCE for
 * the full copyright text.
 *
 * (c) Copyright 1991, 1992 Siemens-Nixdorf Information Systems
 * Burlington, MA, USA
 */
/*
 * @(#)HP DCE/9000 1.7.2
 * @(#)Module: cma_ux.h $Revision: /main/HPDCE02/11 $ $Date: 1998/01/22 16:56 UTC $
 */
/*
 */
/*
*/
/*
 *  Copyright (c) 1989, 1991 by
 *  Digital Equipment Corporation, Maynard Massachusetts.
 *  All rights reserved.
 *
 *  This software is furnished under a license and may be used and  copied
 *  only  in  accordance  with  the  terms  of  such  license and with the
 *  inclusion of the above copyright notice.  This software or  any  other
 *  copies  thereof may not be provided or otherwise made available to any
 *  other person.  No title to and ownership of  the  software  is  hereby
 *  transferred.
 *
 *  The information in this software is subject to change  without  notice
 *  and  should  not  be  construed  as  a commitment by DIGITAL Equipment
 *  Corporation.
 *
 *  DIGITAL assumes no responsibility for the use or  reliability  of  its
 *  software on equipment which is not supplied by DIGITAL.
 */

/*
 *  FACILITY:
 *
 *	CMA services
 *
 *  ABSTRACT:
 *
 *	Header file for unix system call wrapper routines
 *
 *  AUTHORS:
 *
 *	Hans Oser
 *
 *  CREATION DATE:
 *
 *	19 September 1989
 *
 *  MODIFICATION HISTORY:
 *
 *	001	Webb Scales	6 December 1989
 *		Made inclusion conditional, added macros for memory allocation
 *		routines, added CMA routine prototypes.
 *	002	Dave Butenhof	18 December 1989
 *		Change ifndef name for module to correspond to file name.
 *	003	Dave Butenhof	13 February 1990
 *		Add a macro for errno reference (this version still uses the
 *		global per-process errno, but the macro access mechanism
 *		should help locate code which might be incompatible with the
 *		final multiprocessor-compatible solution, which will require
 *		such a macro).
 *	004	Webb Scales	22 March 1990
 *		Add definintions for 'sigwait' function.
 *	005	Webb Scales	27 March 1990
 *		Deleted cma_shutdown, added cma_socketpair.
 *		Split the C RTL wrappers, et al, off into cma_crtlx.h
 *	006	Webb Scales	24 July 1990
 *		Added 'creat', 'dup', and 'dup2'.
 *	007	Webb Scales	16 August 1990
 *		Added Apollo changes for new platforms
 *	008	Webb Scales	23 October 1990
 *		Added "import fd" routine.
 *	009	Paul Curtin	11 December 1990
 *		Added msghdr data structure.
 *	010	Webb Scales	11 December 1990
 *		Added wrapper for fcntl().
 *	011	Paul Curtin	31 January 1991
 *		Added _CMA_NOWRAPPERS_ conditional statement
 *	012	Dave Butenhof	5 February 1991
 *		To avoid breaking client code (now that this is pulled in
 *		transparently by cma.h, pthread.h, pthread_exc.h), drop all
 *		the header file includes; which means also dropping the
 *		prototypes (oh well).
 *	013	Dave Butenhof	12 February 1991
 *		Add fork wrapper
 *	014	Paul Curtin	24 April 1991
 *		Removed all wrapper routine prototypes.  Routine
 *		prototypes should now be picked up from the original
 *		headers and #defined to be protos for our routines.
 *		***NOTE: this makes the assumption that the argument
 *		types defined in our wrappers match all protos on all systems.
 *	015	Webb Scales	2 May 1991
 *		Moved the cma_import_fd prototype here, as this is the best
 *		of several poor choices.
 *	016	Dave Butenhof	23 May 1991
 *		Add conditionals for _CMA_UNIPROCESSOR_ so kernel-thread
 *		versions of DECthreads can still use some wrappers (e.g.,
 *		cma_fork()).
 *	017	Webb Scales	30 May 1991
 *		Created an un-import_fd function, and put the prototype here
 *		so it is with the import_fd function.
 *	018	Paul Curtin	06 June 1991
 *		Added Al Simon's atfork work.
 *	019	Dave Butenhof	19 September 1991
 *		Integrate HPUX CMA5 reverse drop: remove wrappers when
 *		_POSIX_REENTRANT_FUNCTIONS is defined.
 *	020	Dave Butenhof	03 October 1991
 *		Exclude wrappers if _CMA_THREAD_SYNC_IO_ is defined.
 *	021	Dave Butenhof	19 November 1991
 *		Remove direct check for _POSIX_REENTRANT_FUNCTIONS; rely on
 *		_CMA_REENTRANT_CLIB_, since we control that. OSF/1 defines
 *		_POSIX_REENTRANT_FUNCTIONS, but OSF DCE reference port isn't
 *		using libc_r.a.
 */


#ifndef CMA_UX
#define CMA_UX

#include <sys/socket.h>
#include <sys/sem.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <stropts.h>
#include <poll.h>

/*
 *  INCLUDE FILES
 */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * CONSTANTS AND MACROS
 */

#if !_CMA_THREAD_SYNC_IO_
# if !defined(_CMA_NOWRAPPERS_) && _CMA_UNIPROCESSOR_ 

/*
 * U*ix I/O System Call Wrappers
 */

/* We don't want to define wrappers for creat and open in the
 * 64 bit compile environment for large files
 */
/* ifdef this out for a 64 bit test run - jdl */
#ifdef THIS_IS_NOT_DEFINED
# if !defined (_FILE_OFFSET_BITS)
#     define creat    cma_creat
#     define open     cma_open
# else
# if  _FILE_OFFSET_BITS != 64
#     define creat    cma_creat
#     define open     cma_open
#  endif/* _FILE_OFFSET_BITS != 64 */
# endif /* _FILE_OFFSET_BITS */
#else
#     define creat    cma_creat
#     define open     cma_open
#endif

#  define accept	cma_accept
#  define close		cma_close
#  define connect	cma_connect
#  define dup		cma_dup
#  define dup2		cma_dup2
#  define fcntl		cma_fcntl
#  define pipe		cma_pipe
#  define recv		cma_recv
#  define recvmsg	cma_recvmsg
#  define recvfrom	cma_recvfrom
#  define read		cma_read
#  define readv		cma_readv
#  define select	cma_select
#  define send		cma_send
#  define sendmsg	cma_sendmsg
#  define sendto	cma_sendto
#  define socket	cma_socket
#  define socketpair	cma_socketpair
#  define write		cma_write
#  define writev	cma_writev
# endif
/*
 * U*ix process control wrappers
 */
# if !defined(_CMA_NOWRAPPERS_)
#  define fork		cma_fork
#  define atfork	cma_atfork
#  if !_CMA_UNIPROCESSOR_
#   define cma_import_fd(fd)
#  endif
# endif

# if ((_CMA_VENDOR_ == _CMA__HP) && defined(_CMA_REENTRANT_CLIB_))
#  if !defined(_CMA_NOWRAPPERS_) && _CMA_UNIPROCESSOR_ 
#   define ioctl	cma_ioctl
#   define msgrcv	cma_msgrcv
#   define msgsnd	cma_msgsnd
#   define semop	cma_semop
#   define setrlimit	cma_setrlimit
#   define vfork	cma_fork
#   define wait		cma_wait
#   define wait3	cma_wait3
#   define waitpid	cma_waitpid
#   define system	cma_system
/*
 * We only need one exec wrapper, cma_execve(). All of
 * the other versions of exec are implemented to eventually
 * call execve() in libc. execve() in libc will vector to 
 * cma_execve(). So all versions of exec will eventually end
 * up executing the cma_execve() wrapper.
 */
#   define execve	cma_execve
#   define sleep	cma_sleep

/*
 * These defines are here for use with HPUX 10.0.
 * Calls to these routines will fail prior to
 * 10.0 regardless of the state of the defines.
 */
#   define getmsg	cma_getmsg
#   define getpmsg	cma_getpmsg
#   define poll		cma_poll
#   define putmsg	cma_putmsg
#   define putpmsg	cma_putpmsg

#  if defined(_XOPEN_SOURCE_EXTENDED)
#   undef  accept
#   undef  connect
#   undef  socket
#   undef  sockpair
#   undef  sendmsg
#   undef  recvmsg
#   undef  recv
#   undef  recvfrom
#   undef  send
#   undef  sendto
#  endif  /* _XOPEN_SOURCE_EXTENDED */

/*
 * These cals are new for HPUX 10.20
 *
 */
#ifdef _LARGEFILE64_SOURCE
#  define setrlimit64    cma_setrlimit64
#endif

#  define waitid	 cma_waitid
#  define nanosleep	 cma_nanosleep

#  endif
# endif
#endif

/*
 * TYPEDEFS
 */

typedef void (*cma_t_fork_rtn) (cma_t_address);

extern int  cma_accept (int ,void *,int *);
extern int  cma_select(int,int *,int *,int *,struct timeval *);
extern int  cma_close (int);
extern int  cma_connect (int,const void *,int);
extern int  cma_creat (const char *,mode_t);
extern int  cma_dup (int);
extern int  cma_dup2 (int ,int);
extern int  cma_fcntl (int,int,...);
extern int  cma_open (const char *,int,...);
extern int  cma_pipe (int []);
extern int  cma_recv (int,void *,int, int);
extern int  cma_recvmsg (int, struct msghdr msg[], int);
extern int  cma_recvfrom (int, 	void *, int, int, void *, int *);
extern ssize_t  cma_read (int, void *,size_t);
/* XPG4 compliant version of readv has int as final parameter.
   size_t causes a problem in 64 bit builds - jdl */
#ifdef __LP64__
extern ssize_t  cma_readv (int, const struct iovec *, int);
#else
extern ssize_t  cma_readv (int, const struct iovec *, size_t);
#endif
extern int  cma_send (int , void *, int, int );
extern int  cma_sendmsg (int, const struct msghdr msg[] , int);
extern int  cma_sendto (int,const void *,int, int, const void *,int);
extern int  cma_socket (int, int, int);
extern int  cma_socketpair (int, int, int, int[2]);
extern ssize_t  cma_write (int ,const void *,size_t);
/* XPG4 compliant version of readv has int as final parameter.
   size_t causes a problem in 64 bit builds - jdl */
#ifdef __LP64__
extern ssize_t  cma_writev (int, const struct iovec *,int);
#else
extern ssize_t  cma_writev (int, const struct iovec *,size_t);
#endif
extern pid_t  cma_fork (void);

extern void cma_atfork (
            cma_t_address, cma_t_fork_rtn, cma_t_fork_rtn, cma_t_fork_rtn);

# if _CMA_UNIPROCESSOR_
extern void cma_import_fd (int);
# endif

extern void cma_unimport_fd (int);
extern void _cma_atexec_hp (void (*pre_exec)(), void (*post_exec)());

extern int  cma_ioctl (int, int,...);
extern int  cma_msgrcv (int, void *, size_t, long, int);
extern int  cma_msgsnd (int, const void *, size_t, int);
extern int  cma_semop (int, struct sembuf *, unsigned int);
extern int  cma_setrlimit (int, const struct rlimit *);
extern pid_t  cma_wait (int *);
extern pid_t  cma_waitpid (pid_t, int *, int);
extern pid_t  cma_wait3 (int *, int, int *);
extern int  cma_system (const char *);
extern int  cma_execve (const char *, char *const [], char *const []);
extern unsigned int  cma_sleep(unsigned int);

extern int  cma_getmsg (int, struct strbuf *, struct strbuf *, int *);
extern int  cma_getpmsg (int, struct strbuf *, struct strbuf *, int *, int *);
extern int  cma_poll (struct pollfd *, int, int);
extern int  cma_putmsg (int, struct strbuf *, struct strbuf *, int);
extern int  cma_putpmsg (int, struct strbuf *, struct strbuf *, int, int);

#ifdef _LARGEFILE64_SOURCE
extern int  cma_setrlimit64 (int, const struct rlimit64 *);
#endif
extern int  cma_waitid (idtype_t, id_t, siginfo_t *, int);
extern int  cma_nanosleep (const struct timespec *, struct timespec *);

# if defined(_XOPEN_SOURCE_EXTENDED)
extern int  cma_socket2 (int, int, int);
extern int  cma_socketpair2 (int, int, int, int[2]);
extern ssize_t  cma_sendmsg2 (int, const struct msghdr *, int);
extern ssize_t  cma_recvmsg2 (int, struct msghdr *, int);
extern ssize_t  cma_recv2 (int, void *, size_t, int);
extern ssize_t  cma_send2 (int, const void *, size_t, int);
/*
 * the following differ from the man page because the last arg
 * is coerced from socklen_t[*] to int[*] by libxnet
 */
extern ssize_t  cma_recvfrom2 (int, void *, size_t, int, struct sockaddr *, int *);
extern ssize_t  cma_sendto2 (int, const void *, size_t, int, const struct sockaddr *, int);
# endif  /* _XOPEN_SOURCE_EXTENDED */

#endif
#ifdef __cplusplus
}
#endif

