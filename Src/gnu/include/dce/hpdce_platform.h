/*
 * (c) Copyright 1992, 1993, 1994 Hewlett-Packard Co.
 */
/*
 */

#ifndef	__HPDCE_VERSION_H
#define	__HPDCE_VERSION_H

#if !defined(__ux_version)
#define __ux_version 1101
#endif

#if !defined(__hpux1000)
#define	__hpux1000	(__ux_version == 1000)
#endif

#if !defined(__hpux1000p)
#define	__hpux1000p	(__ux_version >= 1000)
#endif

#endif	/* __HPDCE_VERSION_H */

