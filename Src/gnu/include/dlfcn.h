/*
 *  @(#) dlfcn.h 1.0
 *
 *  (c) Copyright 1996 Hewlett-Packard Co., All Rights Reserved.
 */

#ifndef _DLFCN_INCLUDED
#define _DLFCN_INCLUDED
#include <sys/types.h>
#ifdef __cplusplus
extern "C" {
#endif

/* Standard Interface */

/* dlopen flags */
#define RTLD_NOW        0x00000001  /* bind immediately */
#define RTLD_LAZY       0x00000002  /* bind deferred */
#define RTLD_GLOBAL     0x00000004  /* symbols are globally visible */
#define RTLD_LOCAL      0x00000008  /* symbols only visible to this load */

/* dlsym special handles */
#define RTLD_NEXT	((void*) -1L)  /* start symbol search from the dll
					  next to the caller of dlsym() */

#pragma HP_DEFINED_EXTERNAL	dlopen
#pragma HP_DEFINED_EXTERNAL	dlsym 
#pragma HP_DEFINED_EXTERNAL	dlclose
#pragma HP_DEFINED_EXTERNAL	dlerror

#ifdef __ia64
#pragma extern	dlopen
#pragma extern	dlsym 
#pragma extern	dlclose
#pragma extern	dlerror
#endif

#if defined(__STDC__) || defined(__cplusplus)
extern void*	dlopen(const char*,
                       int);

/* backward compatibility for Unix2003 API*/
#if !defined(__restrict)
#   define __restrict	/* nothing */
#endif
extern void*	dlsym(void* __restrict,
	              const char* __restrict);

extern int	dlclose(void*);
extern char*	dlerror(void);

#else /* not __STDC__ || __cplusplus */
extern void*	dlopen();
extern void*	dlsym();
extern int	dlclose();
extern char*	dlerror();

#endif /* not __STDC__ || __cplusplus */

/*  HPUX Interface */
#ifdef _INCLUDE_HPUX_SOURCE

#ifdef __LP64__
typedef unsigned long		UINT64;
#else
typedef unsigned long long	UINT64;
#endif

/* Current libdl.sl version */
#define DL_CURRENT_VERSION	3

/* A Load module descriptor structure */
#ifndef _LOAD_MODULE_DESC_T
#define _LOAD_MODULE_DESC_T
struct load_module_desc {
   UINT64       text_base;
   UINT64       text_size;
   UINT64       data_base;
   UINT64       data_size;
   UINT64       unwind_base;
   UINT64       linkage_ptr;
   UINT64       phdr_base;
   UINT64       tls_size;
   UINT64       tls_start_addr;
   /* added in version 2 */
   UINT64       unwind_size;
   UINT64       tls_index;
#ifdef _LOAD_MODULE_DESC_EXT
   /* added in version 3 */
   UINT64       inode;
   UINT64       device;
   UINT64       checksum;
   UINT64       time_stamp;
   unsigned int version;
#endif /* _LOAD_MODULE_DESC_EXT */
};
#endif /* ! _LOAD_MODULE_DESC_T */

struct dld_hook_param {
   unsigned int version;
   char* filename; 
   union {
      struct { 
         unsigned int startup_load; 
         struct load_module_desc* desc; 
         size_t desc_size; 
      } load_event_desc; 
      struct { 
         struct load_module_desc* desc; 
         size_t desc_size; 
      } unload_event_desc; 
      struct { 
	 char*  sym_name;
         struct load_module_desc* desc; 
         size_t desc_size; 
      } bor_event_desc; 
      struct {
	 char* sym_name;
	 struct load_module_desc* desc;
	 size_t desc_size;
	 UINT64 opd_ptr;
      } opd_event_desc;
   } event_desc;
};

struct dlfileinfo {
   size_t text_size;
   size_t data_size;
   char *filename;
};

struct dlopen_opts {
   long flags;
   char *text_addr;
   char *data_addr;
};

#define load_event 	event_desc.load_event_desc
#define unload_event 	event_desc.unload_event_desc
#define bor_event 	event_desc.bor_event_desc
#define opd_event       event_desc.opd_event_desc


#pragma HP_DEFINED_EXTERNAL	dlget
#pragma HP_DEFINED_EXTERNAL	dlmodinfo
#pragma HP_DEFINED_EXTERNAL	dlgetmodinfo
#pragma HP_DEFINED_EXTERNAL	dlgetname
#pragma HP_DEFINED_EXTERNAL	dlhook
#pragma HP_DEFINED_EXTERNAL	dlmodadd
#pragma HP_DEFINED_EXTERNAL	dlmodremove
#pragma HP_DEFINED_EXTERNAL	dlsetlibpath
#pragma HP_DEFINED_EXTERNAL	dlgetfileinfo
#pragma HP_DEFINED_EXTERNAL	dlerrno
#pragma HP_DEFINED_EXTERNAL	dlopene
#pragma HP_DEFINED_EXTERNAL	dlsetnonuniqsymflag

#ifdef __ia64
#pragma extern	dlget
#pragma extern	dlmodinfo
#pragma extern	dlgetmodinfo
#pragma extern	dlgetname
#pragma extern	dlhook
#pragma extern	dlmodadd
#pragma extern	dlmodremove
#pragma extern	dlsetlibpath
#pragma extern	dlsetnonuniqsymflag
#pragma extern	dlgetfileinfo
#pragma extern  dlerrno
#pragma extern  dlopene
#endif

#if defined(__STDC__) || defined(__cplusplus)
extern void*	dlget(int index, 
                      struct load_module_desc* desc,
                      unsigned int desc_size);
extern UINT64	dlmodinfo(UINT64 ip_value,
                          struct load_module_desc* desc,
                          size_t desc_size,
                          void* (*read_tgt_mem)(void* buffer,
                                                UINT64 ptr,
                                                size_t bufsiz,
                                                int ident),
                          int ident_parm,
                          UINT64 load_map_parm);
extern UINT64	dlgetmodinfo(int index,
			     struct load_module_desc* desc,
			     size_t desc_size,
			     void* (*read_tgt_mem)(void* buffer,
						   UINT64 ptr,
						   size_t bufsiz,
						   int ident),
			     int ident_parm,
			     UINT64 load_map_parm);
extern char*	dlgetname(struct load_module_desc* desc,
                          size_t desc_size,
                          void* (*read_tgt_mem)(void* buffer,
                                                UINT64 ptr,
                                                size_t bufsiz,
                                                int ident),
                          int ident_parm,
                          UINT64 load_map_parm);
extern int	dlhook(unsigned int flags, 
		       void (*dld_hook)(unsigned int type,
					struct dld_hook_param* arg));
extern void*    dlmodadd(void* associate_handle,
                         void *func_start,
                         size_t func_size,
                         void *linkage_ptr,
                         void *unwind_info);
extern int      dlmodremove(void* handle);
extern int      dlsetlibpath(const char *libpath, int flags);
extern int      dlsetnonuniqsymflag(int *flagp);
extern int      dlgetfileinfo(const char *file, size_t info_size,
                              struct dlfileinfo *info);
typedef struct {
   const char *dli_fname;
   void *dli_fbase;
   const char *dli_sname;
   void *dli_saddr;
#if defined(__LP64__) || defined(__ia64)
   size_t dli_size;
   int dli_bind;
#endif
   int dli_type;
} Dl_info;
extern int dladdr(void *address, Dl_info *dlip);
extern int dlerrno(void);
extern void* dlopene(const char* filename,
		     int mode,
		     struct dlopen_opts *opts);
#else /* not __STDC__ || __cplusplus */
extern void*	dlget();
extern UINT64	dlmodinfo();
extern UINT64	dlgetmodinfo();
extern char*	dlgetname();
extern int	dlhook();
extern void*	dlmodadd();
extern int dlmodremove();
extern int	dlsetlibpath();
extern int	dlsetnonuniqsymflag();
extern int	dlgetfileinfo();
typedef struct {
   char *dli_fname;
   void *dli_fbase;
   char *dli_sname;
   void *dli_saddr;
#if defined(__LP64__) || defined(__ia64)
   size_t dli_size;
   int dli_bind;
#endif
   int dli_type;
} Dl_info;
extern int dladdr();
extern int dlerrno();
extern void* dlopene();
#endif /* not __STDC__ || __cplusplus */

/* additional dlopen flags */
#define RTLD_NOLOAD     0x00000010  /* don't load; see if already present */
#define RTLD_NODELETE   0x00000020  /* never unload */
#define RTLD_GROUP      0x00000040  /* bind unsats only to the same group */
#if defined(__LP64__) || defined(__ia64)  /* not yet supported by SOM dld */
#define RTLD_PARENT     0x00000080  /* bind unsats to caller also */
#define RTLD_WORLD      0x00000100  /* bind unsats to only global dlls */
#define RTLD_TEXT_PRIVATE	0x00000200	/* map text private */
#else 				    /* not yet supported by PA64 */
#define RTLD_VERBOSE 	0x00008000  /* Implementing BIND_VERBOSE of shl_load */
#endif

/* additional dlopene flags */
#define RTLD_EXT_TEXT_ADDR	0x00001000	/* explicit placement of 
						   text address */
#define RTLD_EXT_DATA_ADDR	0x00002000 	/* explicit placement of 
						   data address */
#define RTLD_EXT_DATA_NO_ZERO_FILL	0x00004000	/* disable zero-fill
							   bss region of
							   data segment */

/* dlsym special handles */
#define RTLD_DEFAULT	((void*) -2L)  /* lookup symbol from caller's scope */
#define RTLD_SELF	((void*) -3L)  /* start symbol search from the
                                          caller of dlsym() */

/* dlhook event flags */
#define DL_LOAD_PRE_INIT	0x00000001  /* load event before initializers */
#define DL_LOAD_POST_INIT	0x00000002  /* load event after initializers */
#define DL_LOAD_COMPLETE	0x00000004  /* load event complete */
#define DL_UNLOAD_PRE_FINI	0x00000008  /* unload event before terminators*/
#define DL_UNLOAD_POST_FINI	0x00000010  /* unload event after terminators */
#define DL_UNLOAD_COMPLETE	0x00000020  /* unload event complete */
#define DL_BOR			0x00000040  /* bind-on-reference event */
#define DL_DYN_OPD_POST_INIT    0x00000080  /* dynamic opd generation event 
					       after initializers */
#define DL_TERMINATE_START      0x00000100  /* begin program termination */

/* dlsetlibpath flags 
 *
 * These flags disable paths used to search for shared libraries.
 *
 * RTLD_FLAG_DISABLE_DYNAMIC_PATH     disables searching pathname passed by
 *                                    dlsetlibpath()
 * RTLD_FLAG_DISABLE_LD_LIBRARY_PATH  disables searching LD_LIBRARY_PATH
 * RTLD_FLAG_DISABLE_SHLIB_PATH       disables searching SHLIB_PATH
 * RTLD_FLAG_DISABLE_EMBEDDED_PATH    disables searching embedded paths in
 *                                    load modules
 * RTLD_FLAG_DISABLE_STD_PATH         disable searching the standard library
 *                                    path
 * RTLD_FLAG_DISABLE_CWD_PATH         disable searching the current working
 *                                    directory
 */
#define RTLD_FLAG_DISABLE_DYNAMIC_PATH    0x00000001
#define RTLD_FLAG_DISABLE_LD_LIBRARY_PATH 0x00000002
#define RTLD_FLAG_DISABLE_SHLIB_PATH      0x00000004
#define RTLD_FLAG_DISABLE_EMBEDDED_PATH   0x00000008
#define RTLD_FLAG_DISABLE_STD_PATH        0x00000010
#define RTLD_FLAG_DISABLE_CWD_PATH        0x00000020

/* dlerrno error code */
#define RTLD_ERR_UNKNOWN_ERR			-2
#define RTLD_ERR_NO_ERR				-1
#define RTLD_ERR_OPEN				0
#define RTLD_ERR_IO				1
#define RTLD_ERR_BAD_DLL			2
#define RTLD_ERR_BAD_ELF_VER			3
#define RTLD_ERR_LIB_OPEN			4
#define RTLD_ERR_NO_MEMORY			5
#define RTLD_ERR_BAD_RELOC			6
#define RTLD_ERR_INTERNAL_ERROR			7
#define RTLD_ERR_DLOPEN_BAD_FLAGS		8
#define RTLD_ERR_CANT_APPLY_RELOC		9
#define RTLD_ERR_INV_NEXT_HANDLE		10
#define RTLD_ERR_UNKNOWN_SYMBOL			11
#define RTLD_ERR_INV_LIB_INDEX			12
#define RTLD_ERR_INV_HANDLE			13
#define RTLD_ERR_DLCLOSE_REMAINING_DEP		14
#define RTLD_ERR_TPREL_NON_TLS_SYM		15
#define RTLD_ERR_NON_TLS_RELOC_TO_TLS_SYM	16
#define RTLD_ERR_MMAP_FAILED			17
#define RTLD_ERR_DLOPEN_TLS_LIB			18
#define RTLD_ERR_UNLOAD_HANDLE			19
#define RTLD_ERR_UNLOAD_DEP			20
#define RTLD_ERR_CODE_UNSAT			21
#define RTLD_ERR_DATA_UNSAT			22
#define RTLD_ERR_BAD_SYMNAME			23
#define RTLD_ERR_BAD_SYMTYPE			24
#define RTLD_ERR_SHL_LOAD_BAD_FLAGS		25
#define RTLD_ERR_BAD_TYPE_DEFINESYM 		26
#define RTLD_ERR_UNKNOWN_HANDLE 		27
#define RTLD_ERR_INV_BUFFER_ARGUMENT		28
#define RTLD_ERR_INV_ADDRESS			29
#define RTLD_ERR_INV_DESC_VERSION		30
#define RTLD_ERR_FB_CANNOT_WRITE		31
#define RTLD_ERR_FB_CANNOT_OPEN_AOUT		32
#define RTLD_ERR_FB_CANNOT_READ			33
#define RTLD_ERR_FB_DATA_OUT_OF_DATE		34
#define RTLD_ERR_INV_OPTION 			35
#define RTLD_ERR_FB_NO_FASTBIND_DATA		36
#define RTLD_ERR_SIGINHIBIT_FAILED		37
#define RTLD_ERR_SIGENABLE_FAILED		38
#define RTLD_ERR_READ_TGT_MEM_FAILED		39
#define RTLD_ERR_BAD_ABI1			40
#define RTLD_ERR_BAD_ABI2			41
#define RTLD_ERR_INV_DLHOOK_FLAG		42
#define RTLD_ERR_BAD_DLL_MAGIC_NUM		43
#define RTLD_ERR_BAD_DLL_ALIGNMENT		44
#define RTLD_ERR_BAD_DLL_NO_SYMTAB		45
#define RTLD_ERR_BAD_DLL_BAD_PHDR 		46
#define RTLD_ERR_BAD_DLL_BAD_MACHINE		47
#define RTLD_ERR_BAD_DLL_BAD_OBJFILE		48
#define RTLD_ERR_DLDD_COMM_FAILURE		49
#define RTLD_ERR_BAD_DLL_SEGMENT_COUNT		50
#define RTLD_ERR_SHL_LOAD_NULL_FILE_NAME	51
#define RTLD_ERR_BAD_SHL_GETSYMBOLS_FLAG	52
#define RTLD_ERR_INV_DLMODADD_ARGUMENT		53
#define RTLD_ERR_FILTER_TLS			54
#define RTLD_ERR_INST_OPEN 			55
#define RTLD_ERR_INST_NOTE_SEG			56
#define RTLD_ERR_INST_PIPE			57
#define RTLD_ERR_INST_FORK			58
#define RTLD_ERR_INST_EXEC			59
#define RTLD_ERR_INST_CALIPER_ACK		60
#define RTLD_ERR_INST_INTERFERENCE		61
#define RTLD_ERR_INST_STAT			62
#define RTLD_ERR_DYN_FILTER_STLS_REF		63
#define RTLD_ERR_SETCANCELSTATE_FAILED		64
#define RTLD_ERR_DLADDR_NOTFOUND		65
#define RTLD_ERR_MPROTECT_FAILED 		66
#define RTLD_ERR_INV_DLSETLIBPATH_ARGUMENT	67
#define RTLD_ERR_DLGETFILEINFO_MTEXT		68
#define RTLD_ERR_DLGETFILEINFO_MDATA		69
#define RTLD_ERR_INV_DLGETFILEINFO_ARGUMENT	70
#define RTLD_ERR_DLGETFILEINFO_IO	      	71
#define RTLD_ERR_DLOPENE_BAD_ADDR		72
#define RTLD_ERR_PREALLOC_ADDR_NOT_USE		73
#define RTLD_ERR_NOMMAP_FAILED			74
#define RTLD_ERR_ARCH_EXT_NOT_SUPPORTED		75


#endif	/* _INCLUDE_HPUX_SOURCE */

#ifdef __cplusplus
}
#endif
#endif /* _DLFCN_INCLUDED */
