/*============================================================================
   File: elf_hp.h
        Include file for ELF HP file. Defines both 32-/64-bit files.
   Copyright Hewlett-Packard Co. 1996.  All rights reserved.
============================================================================*/

#ifndef ELF_HP_INCLUDED
#define ELF_HP_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


/*============================================================================
   The ELFABIVERSION* macros are the allowed values of e_ident[EI_ABIVERSION].
============================================================================*/

#define ELFABIVERSION_HPUX_CURRENT	0 /* Current version */


/*============================================================================
   The ET_* macros define the values of the e_type field of the ElfXX_Ehdr
   structure.
============================================================================*/

#define ET_HP_IFILE	(ET_LOOS + 0x0)	/* HP-UX instrumented object file */


/*============================================================================
   The EF_* macros define bits in the e_flags field of the ElfXX_Ehdr
   structure.
============================================================================*/

#define EF_HP_LAZYSWAP	0x00400000 /* lazy swap allocation */


/*============================================================================
   The SHF_* macros are the allowed values of the sh_flags field of 
   ElfXX_Shdr.  These 1-bit flags define attributes of a section.
============================================================================*/

#define SHF_HP_TLS		0x01000000 /* TLS data */
#define SHF_HP_NEAR_SHARED	0x02000000 /* near shared data */
#define SHF_HP_FAR_SHARED	0x04000000 /* far shared data */
#define SHF_HP_COMDAT		0x08000000 /* member of a comdat group */


/*============================================================================
   The SHI_* macros are the special values of the sh_info field of 
   ElfXX_Shdr.
============================================================================*/

#define SHI_HP_MERGE		0x0	   /* Merge all instances of section */
#define SHI_HP_PICKANY		0x1        /* pick any COMDAT section */

/*============================================================================
   SHN_* macros are reserved section header indices.  An object file will
   not have sections for these special indices.
============================================================================*/

#define SHN_TLS_COMMON		(SHN_LOOS + 0x0) /* TLS */


/*============================================================================
   SHT_* macros are the values of sh_type in ElfXX_Shdr
============================================================================*/

#define SHT_HP_OVLBITS	(SHT_LOOS + 0x0) /* Overlays */
#define SHT_HP_DLKM	(SHT_LOOS + 0x1) /* dynamically loaded kernel */
					 /* modules */
#define SHT_HP_COMDAT	(SHT_LOOS + 0x2) /* directory of possibly duplicated */
					 /* sections */


/*===========================================================================
   The STT_* macros are the allowed values of the type information part of
   st_info in ElfXX_Sym.
============================================================================*/

#define STT_HP_OPAQUE	(STT_LOOS + 0x1) /* OVLBITS Opaque region */
#define STT_HP_STUB	(STT_LOOS + 0x2) /* Stub symbol type */


/*============================================================================
   The PT_* macros are the values of p_type in ElfXX_Phdr.
============================================================================*/

#define PT_HP_TLS		(PT_LOOS + 0x0) /* TLS */
#define PT_HP_CORE_NONE		(PT_LOOS + 0x1) /* core file information */
#define PT_HP_CORE_VERSION	(PT_LOOS + 0x2)
#define PT_HP_CORE_KERNEL	(PT_LOOS + 0x3)
#define PT_HP_CORE_COMM		(PT_LOOS + 0x4)
#define PT_HP_CORE_PROC		(PT_LOOS + 0x5)
#define PT_HP_CORE_LOADABLE	(PT_LOOS + 0x6)
#define PT_HP_CORE_STACK	(PT_LOOS + 0x7)
#define PT_HP_CORE_SHM		(PT_LOOS + 0x8)
#define PT_HP_CORE_MMF		(PT_LOOS + 0x9)
#define PT_HP_PARALLEL		(PT_LOOS + 0x10) /* parallel information header */
#define PT_HP_FASTBIND		(PT_LOOS + 0x11) /* fastbind data segment */


/*============================================================================
   The PF_* macros are the segment flag bits in p_flags of ElfXX_Phdr.
============================================================================*/

#define PF_HP_PAGE_SIZE		0x00100000 /* use explicit page size */
#define PF_HP_FAR_SHARED	0x00200000 /* far shared data */
#define PF_HP_NEAR_SHARED	0x00400000 /* near shared data */
#define PF_HP_CODE		0x01000000 /* code hint */
#define PF_HP_MODIFY		0x02000000 /* modify hint */
#define PF_HP_LAZYSWAP		0x04000000 /* lazy swap allocation */


/*============================================================================
   The NOTE_* macros are the note types for SHT_NOTE sections
============================================================================*/

#define NOTE_HP_COMPILER	1 /* Compiler identification string */
#define NOTE_HP_COPYRIGHT	2 /* Copyright string */
#define NOTE_HP_VERSION		3 /* Version string */


/*============================================================================
   SHNX_* macros are reserved section header indexes for use with 24-bit
   section tables.  An object file will not have sections for these 
   special indexes.
============================================================================*/

#define SHNX_UNDEF	0	  /* undefined, e.g. undefined symbol */
#define SHNX_LORESERVE	0xffff00  /* Lower bound of reserved indexes */
#define SHNX_TLS_COMMON	0xffff20  /* TLS */
#define SHNX_ABS	0xfffff1  /* Absolute value, not relocated */
#define SHNX_COMMON	0xfffff2  /* FORTRAN common or unallocated C */
#define SHNX_HIRESERVE	0xffffff  /* upper bound of reserved indexes */

#define SHNX_LOPROC	0xffff00  /* Lower bound processor-specific index */
#define SHNX_HIPROC	0xffff1f  /* Lower bound processor-specific index */


/*============================================================================
   The ELF_STRING_xxx macros are names of common sections
============================================================================*/

#define ELF_STRING_hp_init	".HP.init"
#define ELF_STRING_hp_preinit	".preinit"


/*============================================================================
   The DT_* defines are the allowed values of d_tag in ElfXX_dyn.
   These are the Dynamic Array types.
============================================================================*/

						/* (i)gnore (m)andatory */
						/* (o)ptional */
						/* d_un		Exec	DLL */
						/* ----		----	--- */
#define DT_HP_LOAD_MAP		(DT_LOOS + 0x0) /* d_ptr	m	-   */
#define DT_HP_DLD_FLAGS		(DT_LOOS + 0x1) /* d_val	m	-   */
#define DT_HP_DLD_HOOK		(DT_LOOS + 0x2) /* d_ptr	m	-   */
#define DT_HP_UX10_INIT		(DT_LOOS + 0x3) /* d_ptr	o	o   */
#define DT_HP_UX10_INITSZ	(DT_LOOS + 0x4) /* d_ptr	o	o   */
#define DT_HP_PREINIT		(DT_LOOS + 0x5) /* d_ptr	o	-   */
#define DT_HP_PREINITSZ		(DT_LOOS + 0x6) /* d_ptr	o	-   */
#define DT_HP_NEEDED		(DT_LOOS + 0x7) /* d_val	o	o   */
#define DT_HP_TIME_STAMP	(DT_LOOS + 0x8) /* d_val	o	o   */
#define DT_HP_CHECKSUM		(DT_LOOS + 0x9) /* d_val	o	o   */


/*============================================================================
   The DT_* defines are flags for the DT_HP_DLD_FLAGS entry.
============================================================================*/

#define DT_HP_DEBUG_PRIVATE		0x00000001 /* Map text private */
#define DT_HP_DEBUG_CALLBACK		0x00000002 /* Callback */
#define DT_HP_DEBUG_CALLBACK_BOR	0x00000004 /* BOR callback */
#define DT_HP_NO_ENVVAR			0x00000008 /* No env var */
#define DT_HP_BIND_NOW			0x00000010 /* Bind now */
#define DT_HP_BIND_NONFATAL		0x00000020 /* Bind non-fatal */
#define DT_HP_BIND_VERBOSE		0x00000040 /* Bind verbose */
#define DT_HP_BIND_RESTRICTED		0x00000080 /* Bind restricted */
#define DT_HP_BIND_SYMBOLIC		0x00000100 /* Bind symbolic */
#define DT_HP_RPATH_FIRST		0x00000200 /* RPATH first */
#define DT_HP_BIND_DEPTH_FIRST		0x00000400 /* Bind depth-first */

/*============================================================================
   The ELF_X_* macros are used to process files with 24-bit section tables.
============================================================================*/

#define ELF64_X_DECODE(sym, is_lrg)                                          \
   (is_lrg ? ((((Elf64_Word)(sym).st_other) << 16) |                         \
              (Elf64_Word)(sym).st_shndx)                                    \
           : (((sym).st_shndx < SHN_LORESERVE) ? (sym).st_shndx              \
	                                       : 0xFF0000 | (sym).st_shndx))

#define ELF64_X_ENCODE_SHNDX(val, is_lrg)                                    \
   ((Elf64_Half)(is_lrg ? ((val) & 0x0000FFFF)                               \
                        : (val)))

#define ELF64_X_ENCODE_OTHER(val, is_lrg)                                    \
   ((Elf64_Byte)(is_lrg ? (((val) >> 16) & 0x000000FF)                       \
                        : 0x00))


#define ELF32_X_DECODE(sym, is_lrg)                                          \
   (is_lrg ? ((((Elf32_Word)(sym).st_other) << 16) |                         \
              (Elf32_Word)(sym).st_shndx)                                    \
           : (((sym).st_shndx < SHN_LORESERVE) ? (sym).st_shndx              \
	                                       : 0xFF0000 | (sym).st_shndx))

#define ELF32_X_ENCODE_SHNDX(val, is_lrg)                                    \
   ((Elf32_Half)(is_lrg ? ((val) & 0x0000FFFF)                               \
                        : (val)))

#define ELF32_X_ENCODE_OTHER(val, is_lrg)                                    \
   ((Elf32_Byte)(is_lrg ? (((val) >> 16) & 0x000000FF)                       \
                        : 0x00))

#ifndef _HP_NO_ELF_DEPRICATED

/* These defines have been depricated and will be removed in a future release.*/

#define DT_FINISZ		(DT_LOPROC + 0x1) /* d_val	o	o   */
#define DT_INITSZ		(DT_LOPROC + 0x0) /* d_val	o	o   */
#define ELFOSABI_HPUX_BE2	128 /* HP-UX PA-64 BE2 */
#define PF_HP_SBP		0x08000000 /* static branch prediction */
#define SHF_HP_SBP		0x08000000 /* static branch prediction */
#define SHT_OVLBITS     	(SHT_LOOS + 0x0) /* Overlays */
#define STT_OPAQUE		(STT_LOOS + 0x1) /* OVLBITS Opaque region */
#define STT_STUB		(STT_LOOS + 0x2) /* Stub symbol type */
#define PT_HP_PARALLEL_DEPR	(PT_LOOS + 0xa) /* parallel information header*/
#define PT_HP_FASTBIND_DEPR	(PT_LOOS + 0xb) /* fastbind data segment */


#endif /* ifndef _HP_NO_ELF_DEPRICATED */



#ifdef __cplusplus
}
#endif

#endif /* ELF_HP_INCLUDED */
