/*============================================================================
   File: elf_parisc.h
        Include file for ELF PARISC file. Defines both 32-/64-bit files.
   Copyright Hewlett-Packard Co. 1996.  All rights reserved.
============================================================================*/

#ifndef HP_ELF_PARISC_INCLUDED
#define HP_ELF_PARISC_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include <hp_elf_hp.h>

/*============================================================================
   The EM_* macros are the allowed values of e_machine
============================================================================*/

#define EM_PARISC	15	/* PA-RISC */


/*============================================================================
   The EF_* macros define bits in the e_flags field of the ElfXX_Ehdr
   structure.
============================================================================*/

#define EF_PARISC_TRAPNIL	0x00010000 /* Trap nil pointer dereferences */
#define EF_PARISC_EXT		0x00020000 /* Program uses arch. extensions */
#define EF_PARISC_LSB		0x00040000 /* little-endian                 */
#define EF_PARISC_WIDE		0x00080000 /* 64-bits                       */
#define EF_PARISC_NO_KABP	0x00100000 /* kernel assisted branch pred.  */
#define EF_PARISC_LAZYSWAP	0x00400000 /* lazy swap for dyn prog segs   */
#define EF_PARISC_ARCH		0x0000FFFF /* Architecture version mask */
#define EFA_PARISC_1_0		0x020b /* PA-RISC 1.0 */
#define EFA_PARISC_1_1		0x0210 /* PA-RISC 1.1 */
#define EFA_PARISC_2_0		0x0214 /* PA-RISC 2.0 */


/*============================================================================
   The PF_* macros are the segment flag bits in p_flags of ElfXX_Phdr.
============================================================================*/

#define	PF_PARISC_SBP		0x08000000 /* static branch prediction */


/*============================================================================
   The PT_* macros are the values of p_type in ElfXX_Phdr.
============================================================================*/

#define PT_PARISC_ARCHEXT	(PT_LOPROC + 0x0) /* arch. extension bits */
#define PT_PARISC_UNWIND	(PT_LOPROC + 0x1) /* unwind */


/*============================================================================
   The ELF_STRING_xxx macros are names of common sections
============================================================================*/

#define ELF_STRING_parisc_archext	".PARISC.archext"
#define ELF_STRING_parisc_milli		".PARISC.milli"
#define ELF_STRING_parisc_unwind	".PARISC.unwind"
#define ELF_STRING_parisc_nsdata	".PARISC.nsdata"
#define ELF_STRING_parisc_nsbss		".PARISC.nsbss"
#define ELF_STRING_parisc_fsdata	".PARISC.fsdata"
#define ELF_STRING_parisc_fsbss		".PARISC.fsbss"


/*============================================================================
   The SHF_* macros are the allowed values of the sh_flags field of 
   ElfXX_Shdr.  These 1-bit flags define attributes of a section.
============================================================================*/

#define SHF_PARISC_HUGE		0x40000000 /* HUGE model COMMON */
#define SHF_PARISC_SHORT	0x20000000 /* Near global pointer */


/*============================================================================
   SHN_* macros are reserved section header indices.  An object file will
   not have sections for these special indices.
============================================================================*/

#define SHN_PARISC_ANSI_COMMON	(SHN_LOPROC + 0x0) /* ANSI Common symbols */
#define SHN_PARISC_HUGE_COMMON	(SHN_LOPROC + 0x1) /* HUGE model COMMON */


/*============================================================================
   SHNX_* macros are reserved section header indexes for use with 24-bit
   section tables.  An object file will not have sections for these 
   special indexes.
============================================================================*/

#define SHNX_PARISC_ANSI_COMMON  (SHNX_LOPROC + 0)
#define SHNX_PARISC_HUGE_COMMON	 (SHNX_LOPROC + 0x1)

/*============================================================================
   SHT_* macros are the values of sh_type in ElfXX_Shdr
============================================================================*/

#define SHT_PARISC_EXT		(SHT_LOPROC + 0x0) /* product-specific */
						   /* extension bits */
#define SHT_PARISC_UNWIND	(SHT_LOPROC + 0x1) /* unwind bits */
#define SHT_PARISC_DOC		(SHT_LOPROC + 0x2) /* doc bits */
#define SHT_PARISC_ANNOT	(SHT_LOPROC + 0x3) /* annotations */
#define SHT_PARISC_DLKM		(SHT_LOPROC + 0x4) /* DLKM special section */


/*===========================================================================
   The STT_* macros are the allowed values of the type information part of
   st_info in ElfXX_Sym.
============================================================================*/

#define STT_PARISC_MILLI	(STT_LOPROC + 0x0) /* Millicode */


/*============================================================================
   The R_PARISC_* macros are the 64-bit PA-RISC relocation types
   Updated 960106 to reflect latest ELF draft
============================================================================*/

#define R_PARISC_NONE		0

#define R_PARISC_DIR32		1	/* sym + add, .word    */
#define R_PARISC_DIR21L		2	/* LR(sym, add), LDIL  */
#define R_PARISC_DIR17R		3	/* RR(sym, add), BE    */
#define R_PARISC_DIR17F		4	/* sym + add, BE       */
#define R_PARISC_DIR14R		6	/* RR(sym, add), ld/st */

#define R_PARISC_PCREL32	9	/* sym - PC + add, .word */
#define R_PARISC_PCREL21L	10	/* L(sym - PC - 8 + add), LDIL  */
#define R_PARISC_PCREL17R	11	/* R(sym - PC - 8 + add), BE    */
#define R_PARISC_PCREL17F	12	/* sym - PC - 8 + add, BL       */
#define R_PARISC_PCREL17C	13	/* sym - PC - 8 + add, BL       */
#define R_PARISC_PCREL14R	14	/* R(sym - PC - 8 + add), ld/st */

#define R_PARISC_DPREL21L	18	/* L(sym - dp + addend)  */
#define R_PARISC_DPREL14WR	19	/* R(sym - dp + addend)  */
#define R_PARISC_DPREL14DR	20	/* R(sym - dp + addend)  */
#define R_PARISC_DPREL14R	22	/* R(sym - dp + addend)  */

#define R_PARISC_GPREL21L	26	/* LR(sym - GP, add), ADDIL */
#define R_PARISC_GPREL14R	30	/* RR(sym - GP, add), ld/st */

#define R_PARISC_LTOFF21L	34	/* L(ltoff(sym + add)), ADDIL */
#define R_PARISC_LTOFF14R	38	/* R(ltoff(sym + add)), ld/st */
#define R_PARISC_LTOFF14F	39	/* ltoff(sym + add) */

#define R_PARISC_SETBASE	40	/* Set base */
#define R_PARISC_SECREL32	41	/* sym - SC + add, .word */
#define R_PARISC_BASEREL21L	42	/* L(sym - base + addend) */
#define R_PARISC_BASEREL17R	43	/* sym - base + addend    */
#define R_PARISC_BASEREL14R	46	/* R(sym - base + addend) */

#define R_PARISC_SEGBASE	48	/* Set SB */
#define R_PARISC_SEGREL32	49	/* sym - SB + add, .word */

#define R_PARISC_PLTOFF21L	50	/* LR(pltoff(sym), add), ADDIL */
#define R_PARISC_PLTOFF14R	54	/* RR(pltoff(sym), add), ld/st */
#define R_PARISC_PLTOFF14F	55	/* pltoff(sym) + add */

#define R_PARISC_LTOFF_FPTR32	57	/* ltoff(fptr(sym)), .word    */
#define R_PARISC_LTOFF_FPTR21L	58	/* L(ltoff(fptr(sym))), ADDIL */
#define R_PARISC_LTOFF_FPTR14R	62	/* R(ltoff(fptr(sym))), ld/st */

#define R_PARISC_FPTR64		64	/* fptr(sym), .dword  */
#define R_PARISC_FPTR32		65	/* fptr(sym), .word   */

#define R_PARISC_PCREL64	72	/* sym - PC - 8 + add, .dword */
#define R_PARISC_PCREL22C	73	/* sym - PC - 8 + add, BLL            */
#define R_PARISC_PCREL22F	74	/* sym - PC - 8 + add, BLL            */
#define R_PARISC_PCREL14WR	75	/* R(sym - PC - 8 + add), word ld/st  */
#define R_PARISC_PCREL14DR	76	/* R(sym - PC - 8 + add), dword ld/st */
#define R_PARISC_PCREL16F	77	/* sym - PC - 8 + add, ld/st          */
#define R_PARISC_PCREL16WF	78	/* sym - PC - 8 + add, word ld/st     */
#define R_PARISC_PCREL16DF	79	/* sym - PC - 8 + add, dword ld/st    */

#define R_PARISC_DIR64		80	/* sym + add, .dword         */
#define R_PARISC_DIR14WR	83	/* RR(sym, add), word ld/st  */
#define R_PARISC_DIR14DR	84	/* RR(sym, add), dword ld/st */
#define R_PARISC_DIR16F		85	/* sym + add, ld/st          */
#define R_PARISC_DIR16WF	86	/* sym + add, word ld/st     */
#define R_PARISC_DIR16DF	87	/* sym + add, dword ld/st    */

#define R_PARISC_GPREL64		88	/* sym + add, .dword         */
#define R_PARISC_GPREL14WR	91	/* RR(sym - GP, add), word ld/st  */
#define R_PARISC_GPREL14DR	92	/* RR(sym - GP, add), dword ld/st */
#define R_PARISC_GPREL16F	93	/* sym - GP + add, ld/st          */
#define R_PARISC_GPREL16WF	94	/* sym - GP + add, word ld/st     */
#define R_PARISC_GPREL16DF	95	/* sym - GP + add, dword ld/st    */

#define R_PARISC_LTOFF64	96	/* ltoff(sym + add) - debug record */
#define R_PARISC_LTOFF14WR	99	/* R(ltoff(sym + add)), word ld/st  */
#define R_PARISC_LTOFF14DR	100	/* R(ltoff(sym + add)), dword ld/st */
#define R_PARISC_LTOFF16F	101	/* ltoff(sym + add), ld/st          */
#define R_PARISC_LTOFF16WF	102	/* ltoff(sym + add), word ld/st     */
#define R_PARISC_LTOFF16DF	103	/* ltoff(sym + add), dword ld/st    */

#define R_PARISC_SECREL64	104	/* sym - SC + add, .dword */

#define R_PARISC_BASEREL14WR	107	/* R(sym - base + addend)            */
#define R_PARISC_BASEREL14DR	108	/* R(sym - base + addend)            */

#define R_PARISC_SEGREL64	112	/* sym - SB + add, .dword */

#define R_PARISC_PLTOFF14WR	115	/* RR(pltoff(sym), add), word ld/st  */
#define R_PARISC_PLTOFF14DR	116	/* RR(pltoff(sym), add), dword ld/st */
#define R_PARISC_PLTOFF16F	117	/* pltoff(sym) + add, ld/st          */
#define R_PARISC_PLTOFF16WF	118	/* pltoff(sym) + add, word ld/st     */
#define R_PARISC_PLTOFF16DF	119	/* pltoff(sym) + add, dword ld/st    */

#define R_PARISC_LTOFF_FPTR64	120	/* ltoff(fptr(sym)), .dword         */
#define R_PARISC_LTOFF_FPTR14WR	123	/* R(ltoff(fptr(sym))), word ld/st  */
#define R_PARISC_LTOFF_FPTR14DR	124	/* R(ltoff(fptr(sym))), dword ld/st */
#define R_PARISC_LTOFF_FPTR16F	125	/* ltoff(fptr(sym)), ld/st          */
#define R_PARISC_LTOFF_FPTR16WF	126	/* ltoff(fptr(sym)), word ld/st     */
#define R_PARISC_LTOFF_FPTR16DF	127	/* ltoff(fptr(sym)), dword ld/st    */

#define R_PARISC_COPY		128	/* dyn reloc, data copy    */
#define R_PARISC_IPLT		129	/* dyn reloc, imported PLT */
#define R_PARISC_EPLT		130	/* dyn reloc, exported PLT */

#define R_PARISC_TPREL32	153	/* sym - TP + addend, .word          */
#define R_PARISC_TPREL21L	154	/* L(sym - TP + addend)              */
#define R_PARISC_TPREL14R	158	/* R(sym - TP + addend)              */
#define R_PARISC_LTOFF_TP21L	162	/* L(ltoff(sym - TP + addend))       */
#define R_PARISC_LTOFF_TP14R	166	/* R(ltoff(sym - TP + addend))       */
#define R_PARISC_LTOFF_TP14F	167	/* ltoff(sym - TP + addend)          */

#define R_PARISC_TPREL64	216	/* sym - TP + addend, .dword         */
#define R_PARISC_TPREL14WR	219	/* R(sym - TP + addend), word ld/st  */
#define R_PARISC_TPREL14DR	220	/* R(sym - TP + addend), dword ld/st */
#define R_PARISC_TPREL16F	221	/* sym - TP + addend, ld/st          */
#define R_PARISC_TPREL16WF	222	/* sym - TP + addend, word ld/st     */
#define R_PARISC_TPREL16DF	223	/* sym - TP + addend, dword ld/st    */

#define R_PARISC_LTOFF_TP64	224	/* ltoff(sym - TP + addend) - debug  */
#define R_PARISC_LTOFF_TP14WR	227	/* R(ltoff(sym - TP + addend))       */
#define R_PARISC_LTOFF_TP14DR	228	/* R(ltoff(sym - TP + addend))       */
#define R_PARISC_LTOFF_TP16F	229	/* ltoff(sym - TP + addend)          */
#define R_PARISC_LTOFF_TP16WF	230	/* ltoff(sym - TP + addend)          */
#define R_PARISC_LTOFF_TP16DF	231	/* ltoff(sym - TP + addend)          */

#ifndef _HP_NO_ELF_DEPRICATED
/* These defines have been depricated and will be removed in a future release.*/


#endif /* ifndef _HP_NO_ELF_DEPRICATED */

#define ET_PARISC_IFILE	(ET_LOPROC + 0x0) /* PARISC instrumented object file */

#ifdef __cplusplus
}
#endif

#endif /* HP_ELF_PARISC_INCLUDED */
