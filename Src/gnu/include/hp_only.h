/*============================================================================
   File: hp_only.h
	Extracted from hp_elf.h those things which were not defined elsewhere,
	e.g. in common.h.

	Include file for ELF file.  Defines both 32-/64-bit files.
   Copyright Hewlett-Packard Co. 1996.  All rights reserved.
============================================================================*/

#ifndef ELF_INCLUDED
#define ELF_INCLUDED

#include <hp_elftypes.h>

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================
   Constant values for the sizes of fundamental types 
============================================================================*/

#define ELF64_FSZ_ADDR		8
#define ELF64_FSZ_HALF		2
#define ELF64_FSZ_OFF		8
#define ELF64_FSZ_SWORD		4
#define ELF64_FSZ_WORD		4
#define ELF64_FSZ_SXWORD	8
#define ELF64_FSZ_XWORD		8

#define ELF32_FSZ_ADDR  	4
#define ELF32_FSZ_HALF  	2
#define ELF32_FSZ_OFF   	4
#define ELF32_FSZ_SWORD 	4
#define ELF32_FSZ_WORD  	4
#define ELF32_FSZ_XWORD  	4


/*============================================================================
   Misc. defines fro hp_elf.h
============================================================================*/


#define EI_NIDENT	16 /* Size of e_ident[] */
#define SHN_XINDEX	0xffff/* Escape value for large section header indices */
#define SHT_SYMTAB_SHNDX	18 /* Section header indices of symbol table entries */

/*============================================================================
   ElfXX_Ehdr - ELF file header structure.
============================================================================*/

typedef struct {
   unsigned char	e_ident[EI_NIDENT];	/* ELF identification */
   Elf64_Half		e_type;		/* Object file type */
   Elf64_Half		e_machine;	/* Machine type */
   Elf64_Word		e_version;	/* Object file version */
   Elf64_Addr		e_entry;	/* Entry point address */
   Elf64_Off		e_phoff;	/* Program header offset */
   Elf64_Off		e_shoff;	/* Section header offset */
   Elf64_Word		e_flags;	/* Processor-specific flags */
   Elf64_Half		e_ehsize;	/* ELF header size */
   Elf64_Half		e_phentsize;	/* Size of program header entry */
   Elf64_Half		e_phnum;	/* Number of program header entries */
   Elf64_Half		e_shentsize;	/* Size of section header entry */
   Elf64_Half		e_shnum;	/* Number of section header entries */
   Elf64_Half		e_shstrndx;	/* Section name string table index */
} Elf64_Ehdr;

typedef struct {
  unsigned char	e_ident[EI_NIDENT];	/*16/ ELF "magic number" */
  Elf32_Half	e_type;		/* 2/ Identifies object file type */
  Elf32_Half	e_machine;	/* 2/ Specifies required architecture */
  Elf32_Word	e_version;	/* 4/ Identifies object file version */
  Elf32_Addr	e_entry;	/* 4/ Entry point virtual address */
  Elf32_Off	e_phoff;	/* 4/ Program header table file offset */
  Elf32_Off	e_shoff;	/* 4/ Section header table file offset */
  Elf32_Word	e_flags;	/* 4/ Processor-specific flags */
  Elf32_Half	e_ehsize;	/* 2/ ELF header size in bytes */
  Elf32_Half	e_phentsize;	/* 2/ Program header table entry size */
  Elf32_Half    e_phnum;	/* 2/ Program header table entry count */
  Elf32_Half	e_shentsize;	/* 2/ Section header table entry size */
  Elf32_Half	e_shnum;	/* 2/ Section header table entry count */
  Elf32_Half	e_shstrndx;	/* 2/ Section header string table index */
} Elf32_Ehdr;



/*============================================================================
   The ELFCLASS* macros are the defined values of e_ident[EI_CLASS].
     (The values defined in hp_elf.h but not common.h )
============================================================================*/
#define ELFCLASS64_A	200 /* 64-bit objects (interim definition) */


/*============================================================================
   The ELFOSIABI* macros are the allowed values of e_ident[EI_OSABI].
============================================================================*/

#define ELFOSABI_SYSV		0 /* System V ABI, third edition, no checking */


/*============================================================================
   The ELFABIVERSION* macros are the allowed values of e_ident[EI_ABIVERSION].
============================================================================*/

#define ELFABI_HPUX_NONE	0 /* None specified */



/*============================================================================
   The ELFMAG* macros are the values of e_ident[EI_MAG0-4]
============================================================================*/

#define ELFMAG			"\177ELF" /* magic string */
#define SELFMAG			4         /* magic string length */


/*============================================================================
   ElfXX_Phdr - program header structure.  Element of the array of
   program headers.
============================================================================*/

typedef struct {
   Elf64_Word		p_type;		/* Type of segment */
   Elf64_Word		p_flags;	/* Segment attributes */
   Elf64_Off		p_offset;	/* Offset in file */
   Elf64_Addr		p_vaddr;	/* Virtual address in memory */
   Elf64_Addr		p_paddr;	/* Reserved */
   Elf64_Xword		p_filesz;	/* Size of segment in file */
   Elf64_Xword		p_memsz;	/* Size of segment in memory */
   Elf64_Xword		p_align;	/* Alignment of segment */
} Elf64_Phdr;

typedef struct {
   Elf32_Word		p_type;		/* Type of segment */
   Elf32_Off		p_offset;	/* Offset in file */
   Elf32_Addr		p_vaddr;	/* Virtual address in memory */
   Elf32_Addr		p_paddr;	/* Reserved */
   Elf32_Xword		p_filesz;	/* Size of segment in file */
   Elf32_Xword		p_memsz;	/* Size of segment in memory */
   Elf32_Word		p_flags;	/* Segment attributes */
   Elf32_Xword		p_align;	/* Alignment of segment */
} Elf32_Phdr;




/*============================================================================
   ElfXX_Shdr - structure for an ELF section header.  Element of the array
   of section headers.
============================================================================*/

typedef struct {
   Elf64_Word		sh_name;	/* Section name */
   Elf64_Word		sh_type;	/* Section type */
   Elf64_Xword		sh_flags;	/* Section attributes */
   Elf64_Addr		sh_addr;	/* Virtual address in memory */
   Elf64_Off		sh_offset;	/* Offset in file */
   Elf64_Xword		sh_size;	/* Size of section */
   Elf64_Word		sh_link;	/* Link to other section */
   Elf64_Word		sh_info;	/* Miscellaneous information */
   Elf64_Xword		sh_addralign;	/* Address alignment boundary */
   Elf64_Xword		sh_entsize;	/* Entry size, if section has table */
} Elf64_Shdr;

typedef struct {
   Elf32_Word		sh_name;	/* Section name */
   Elf32_Word		sh_type;	/* Section type */
   Elf32_Xword		sh_flags;	/* Section attributes */
   Elf32_Addr		sh_addr;	/* Virtual address in memory */
   Elf32_Off		sh_offset;	/* Offset in file */
   Elf32_Xword		sh_size;	/* Size of section */
   Elf32_Word		sh_link;	/* Link to other section */
   Elf32_Word		sh_info;	/* Miscellaneous information */
   Elf32_Xword		sh_addralign;	/* Address alignment boundary*/
   Elf32_Xword		sh_entsize;	/* Entry size, if section has table */
} Elf32_Shdr;


/*============================================================================
   The ELF_STRING_xxx macros are names of common sections
============================================================================*/

#define ELF_STRING_bss		".bss"
#define ELF_STRING_hbss		".hbss"
#define ELF_STRING_sbss		".sbss"
#define ELF_STRING_tbss		".tbss"
#define ELF_STRING_data		".data"
#define ELF_STRING_hdata	".hdata"
#define ELF_STRING_sdata	".sdata"
#define ELF_STRING_fini		".fini"
#define ELF_STRING_init		".init"
#define ELF_STRING_interp	".interp"
#define ELF_STRING_rodata	".rodata"
#define ELF_STRING_text		".text"
#define ELF_STRING_comment	".comment"
#define ELF_STRING_dynamic	".dynamic"
#define ELF_STRING_dynstr	".dynstr"
#define ELF_STRING_dynsym	".dynsym"
#define ELF_STRING_dlt		".dlt"
#define ELF_STRING_note		".note"
#define ELF_STRING_opd          ".opd"
#define ELF_STRING_plt		".plt"
#define ELF_STRING_bss_rela     ".rela.bss"
#define ELF_STRING_hbss_rela	".rela.hbss"
#define ELF_STRING_data_rela    ".rela.data"
#define ELF_STRING_dlt_rela     ".rela.dlt"
#define ELF_STRING_plt_rela     ".rela.plt"
#define ELF_STRING_sdata_rela   ".rela.sdata"
#define ELF_STRING_strtab	".strtab"
#define ELF_STRING_symtab	".symtab"
#define ELF_STRING_hash		".hash"
#define ELF_STRING_shstrtab	".shstrtab"
#define ELF_STRING_shsymtab	".shsymtab"
#define ELF_STRING_rela		".rela"
#define ELF_STRING_rel		".rel"




/*============================================================================
   ElfXX_Sym - ELF symbol structure.
============================================================================*/

typedef struct {
   Elf64_Word		st_name;	/* Symbol name */
   unsigned char	st_info;	/* Type and Binding attributes */
   unsigned char	st_other;	/* Reserved */
   Elf64_Half		st_shndx;	/* Section table index */
   Elf64_Addr		st_value;	/* Symbol value */
   Elf64_Xword		st_size;	/* Size of object (e.g., common) */
} Elf64_Sym;

typedef struct {
   Elf32_Word		st_name;	/* Symbol name */
   Elf32_Addr		st_value;	/* Symbol value */
   Elf32_Xword		st_size;	/* Size of object (e.g., common) */
   unsigned char	st_info;	/* Type and Binding attributes */
   unsigned char	st_other;	/* Reserved */
   Elf32_Half		st_shndx;	/* Section table index */
} Elf32_Sym;




/*============================================================================
   ElfXX_Rel - ELF relocation without an explicit addend.
============================================================================*/

typedef struct {
   Elf64_Addr		r_offset;	/* Address of reference */
   Elf64_Xword		r_info;		/* Symbol index, relocation type */
} Elf64_Rel;

typedef struct {
   Elf32_Addr		r_offset;	/* Address of reference */
   Elf32_Xword		r_info;		/* Symbol index, reloc. type */
} Elf32_Rel;


/*============================================================================
   ElfXX_Rela - ELF relocation with an explicit addend.
============================================================================*/

typedef struct {
   Elf64_Addr		r_offset;	/* Address of reference */
   Elf64_Xword		r_info;		/* Symbol index, relocation type */
   Elf64_Sxword		r_addend;	/* Constant part of expression */
} Elf64_Rela;

typedef struct {
   Elf32_Addr		r_offset;	/* Address of reference */
   Elf32_Xword		r_info;	        /* Symbol index, reloc. type */
   Elf32_Sword		r_addend;	/* Constant part of expression */
} Elf32_Rela;




/*============================================================================
   ElfXX_Dyn - dynamic array entry.
============================================================================*/

typedef struct {
   Elf32_Sword		d_tag;
   union {
      Elf32_Word	d_val;	/* unsigned word */
      Elf32_Addr	d_ptr;	/* address */
   }  d_un;
} Elf32_Dyn;

extern Elf32_Dyn	_DYNAMIC32[];

typedef struct {
   Elf64_Sxword		d_tag;
   union {
      Elf64_Xword	d_val;	/* unsigned word */
      Elf64_Addr	d_ptr;	/* address */
   }  d_un;
} Elf64_Dyn;

extern Elf64_Dyn        _DYNAMIC[];



#ifndef _HP_NO_ELF_DEPRICATED

/* These defines have been depricated and will be removed in a future release.*/

#define ELFOSABI_NONE		0 /* None specified */
#define ELFOSABI_NT		2 /* NT operating system */
#define ELFOSABI_SCOUNIX	3 /* SCO UNIX operating system */
#define SHF_COMDAT	0x8 /* Is a member of a COMDAT group */
#define SHT_COMDAT	12 /* COMDAT group directory -> SHT_HP_COMDAT */

#endif /* ifndef _HP_NO_ELF_DEPRICATED */

#ifdef __cplusplus
}
#endif /* ifndef _HP_NO_ELF_DEPRICATED */

#endif /* ifndef ELF_INCLUDED */
