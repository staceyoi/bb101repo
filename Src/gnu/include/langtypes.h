/*=====================================================================

   File: langtypes_nob.h

   Purpose: common type definition

   Copyright Hewlett-Packard Co. 1994.  All rights reserved.

=====================================================================*/

	/*  Note: The "signed" keyword is commented out until the
	**  HPC++ compiler will handle it with no warnings 
	*/

#ifndef LANGTYPES_INCLUDED
#define LANGTYPES_INCLUDED

typedef unsigned int	Boolean;

typedef /* signed */ long long  Int64;
typedef unsigned long long	  UInt64;

typedef /* signed */ int	Int;
typedef unsigned int	UInt;

typedef /* signed */ char	Int8;
typedef /* signed */ short	Int16;
typedef /* signed */ int	Int32;
typedef unsigned char	UInt8;
typedef unsigned short	UInt16;
typedef unsigned int	UInt32;

#ifdef __cplusplus

static const  Int64 MAX_INT_64  = 0x7fffffffffffffffLL;
static const UInt64 MAX_UINT_64 = 0xffffffffffffffffULL;
static const  Int   MAX_INT_32  = 0x7fffffff;
static const UInt   MAX_UINT_32 = 0xffffffffU;
static const UInt   MAX_UINT_16 = 0x0000ffffU;

static const  Int64 MIN_INT_64  = 0x8000000000000000LL;
static const  Int   MIN_INT_32  = 0x80000000;

#else

#define MAX_INT_64  0x7fffffffffffffffLL
#define MAX_UINT_64 0xffffffffffffffffULL
#define MAX_INT_32  0x7fffffff
#define MAX_UINT_32 0xffffffffU
#define MAX_UINT_16 0x0000ffffU

#define MIN_INT_64  0x8000000000000000LL
#define MIN_INT_32  0x80000000

#endif /* __cplusplus */

#ifndef ARCH_PTR_SIZE
#   define ARCH_PTR_SIZE 64
#endif

#if (ARCH_PTR_SIZE == 64)
   
   typedef UInt64       Address;
   typedef Int64        Addend;
#elif (ARCH_PTR_SIZE == 32)
   typedef UInt32	Address;
   typedef Int32	Addend;
#else
   error - ARCH_PTR_SIZE currently must be 32 or 64
#endif

typedef unsigned long   Generic_Word;

#endif /* LANGTYPES_INCLUDED */
