/*
 *  $Header: /Ski/libsym.h 1.5 1997/01/29 23:28:37 hull Rel $
 *
 *  Tahoe Symbol Table Library Header
 *
 *  Copyright (c) 1996
 *  Systems Architecture & Design Lab
 *  The Hewlett-Packard Company
 *  Palo Alto, California
 *
 *  *** HP Confidential ***
 */

typedef struct {
    char *name;			/* symbol name */
    unsigned long long addr;	/* value (address) associated with symbol */
} Symbol;

/* function prototypes */
unsigned long long symAddrtoAddr(unsigned long long addr, int dist);
void symInsert(const char *name, unsigned long long addr);
int symNametoAddr(const char *name, unsigned long long *paddr);
void symAddrtoName(unsigned long long addr, char sname[], int rmdr, int add0x,
		   int width);
void symNextSymInit(void);
int symNextSym(Symbol *sym);
void symlDelete(void);
