/*
 * BEGIN_DESC
 * 
 *  File: 
 *      @(#)	pa/sys/inline.h		$Revision: $
 * 
 *  Purpose:
 *	HP Precision Architecture Inline Assembly Instruction Macros
 * 
 *  Classification:			Release to Release Consistency Req:
 * 	public					binary & source
 * 
 *  BE header:  yes
 *
 *  Shipped:  yes
 *	/usr/include/pa/inline.h
 *	/usr/conf/pa/inline.h
 *
 * END_DESC  
*/

#ifdef __NO_PA_HDRS
    PA header file -- do not include this header file for non-PA builds.
#endif


#if defined(lint) || defined(_lint)

#  define _RFI()		/* nop */
#  define _SSM(i,t)		(i)	/* nop */
#  define _RSM(i,t)		(t = i)	/* Doesn't work for t == 0 */
#  define _MTSM(r)		(r = r)	/* Reads r */
#  define _LDSID(s,b,t)		(t = s * (int)b)
#  define _MTSP(r,sr)		(r = r)	/* Reads r */
#  define _MTCTL(r,t)		(*(long *)r = r) /* Reads r */
#  define _MFSP(sr,t)		(t = sr)
#  define _MFCTL(r,t)		((long)t = (long)r)
#  define _SYNC()		/* nop */
#  define _SYNCDMA()		/* nop */
#  define _PROBER(s,b,x,t)	(t = x + s * b)
#  define _PROBERI(s,b,i,t)	(t = i + s * b)
#  define _PROBEW(s,b,x,t)	(t = x + s * b)
#  define _PROBEWI(s,b,i,t)	(t = i + s * b)
#  define _LPA(x,s,b,t)		(t = x + s * b)
#  define _LHA(x,s,b,t)		(t = x + s * b)
#  define _PDTLB(x,s,b)		/* nop */
#  define _PITLB(x,s,b)		/* nop */
#  define _PDTLBE(x,s,b)	/* nop */
#  define _PITLBE(x,s,b)	/* nop */
#  define _IDTLBA(r,s,b)	/* nop */
#  define _IITLBA(r,s,b)	/* nop */
#  define _IDTLBP(r,s,b)	/* nop */
#  define _IITLBP(r,s,b)	/* nop */
#  define _PDC(x,s,b)		/* nop */
#  define _FDC(x,s,b)		/* nop */
#  define _FIC(x,s,b)		/* nop */
#  define _FDCE(x,s,b)		/* nop */
#  define _FICE(x,s,b)		/* nop */
#  define _BREAK(i1,i2)		/* nop */
#  define _LDCD(i,s,b,t)	(t = i + s * (int)b)
#  define _LDCWS(i,s,b,t)	(t = i + s * (int)b)
#  define _LDCWX(x,s,b,t)	(t = x + s * (int)b)
#  define _SHD(r1,r2,p,t)	(t = (r2 >> p) + (r1 << (31-p)))

#  define _DEP(r,p,l,t)		/* fill in later if needed */
#  define _DEPI(s,p,l,t)	/* fill in later if needed */
#  define _EXTRU(r,p,l,t)	/* fill in later if needed */
#  define _HADD(r,b,t)		/* fill in later if needed */
#  define _HADD_ss(r,b,t)	/* fill in later if needed */
#  define _HADD_us(r,b,t)	/* fill in later if needed */
#  define _HAVE(r,b,t)		/* fill in later if needed */
#  define _HSL1ADD(r,b,t)	/* fill in later if needed */
#  define _HSL2ADD(r,b,t)	/* fill in later if needed */
#  define _HSL3ADD(r,b,t)	/* fill in later if needed */
#  define _HSR1ADD(r,b,t)	/* fill in later if needed */
#  define _HSR2ADD(r,b,t)	/* fill in later if needed */
#  define _HSR3ADD(r,b,t)	/* fill in later if needed */
#  define _HSUB(r,b,t)		/* fill in later if needed */
#  define _HSUB_ss(r,b,t)	/* fill in later if needed */
#  define _HSUB_us(r,b,t)	/* fill in later if needed */
#  define _LDB(d,s,b,t)		/* fill in later if needed */
#  define _LDBS(d,s,b,t)	/* fill in later if needed */
#  define _LDW(d,s,b,t)		/* fill in later if needed */
#  define _LDWAX(r,b,t)		/* fill in later if needed */
#  define _LDWS(d,s,b,t)	/* fill in later if needed */
#  define _STB(r,p,l,t)		/* fill in later if needed */
#  define _STBS(r,p,l,t)	/* fill in later if needed */
#  define _STBYS(r,p,l,t)	/* fill in later if needed */
#  define _STWS(r,p,l,t)	/* fill in later if needed */
#  define _ZDEP(r,p,l,t)	/* fill in later if needed */

#  define _PREFETCH(d,b)		/*noop*/
#  define _ADD(cmplt,r1,r2,t)		(t = t)
#  define _SHLADD(cmplt,r1,sa,r2,t)	(t = t)
#  define _SUB(cmplt,r1,r2,t)		(t = t)
#  define _DS(cond,r1,r2,t)		(t = t)
#  define _CMPCLR(cond,r1,r2,t)		(t = t)
#  define _OR(cond,r1,r2,t)		(t = t)
#  define _XOR(cond,r1,r2,t)		(t = t)
#  define _AND(cond,r1,r2,t)		(t = t)
#  define _ANDCM(cond,r1,r2,t)		(t = t)
#  define _UXOR(cond,r1,r2,t)		(t = t)
#  define _UADDCM(trapc,r1,r2,t)	(t = t)
#  define _DCOR(cmplt,r,t)		(t = t)
#  define _ADDI(cmplt,i,r,t)		(t = t)
#  define _SUBI(cmplt,i,r,t)		(t = t)
#  define _CMPICLR(cond,i,r,t)		(t = t)
#  define _SHRPD(cond,r1,r2,sa,t)	(t = t)
#  define _SHRPW(cond,r1,r2,sa,t)	(t = t)
#  define _EXTRD(cmplt,r,pos,len,t)	(t = t)
#  define _EXTRW(cmplt,r,pos,len,t)	(t = t)
#  define _DEPD(cmplt,r,pos,len,t)	(t = t)
#  define _DEPDI(cmplt,i,pos,len,t)	(t = t)
#  define _DEPW(cmplt,r,pos,len,t)	(t = t)
#  define _DEPWI(cmplt,i,pos,len,t)	(t = t)
#  define _HAVG(r1,r2,t)		(t = t)
#  define _HSHLADD(r1,sa,r2,t)		(t = t)
#  define _HSHRADD(r1,sa,r2,t)		(t = t)
#  define _HSHL(r,sa,t)			(t = t)
#  define _HSHR(cmplt,r,sa,t)		(t = t)
#  define _PERMH(c,r,t)			(t = t)
#  define _MIXW(cmplt,r1,r2,t)		(t = t)
#  define _MIXH(cmplt,r1,r2,t)		(t = t)
#  define _MTSARCM(r)			(r = r)
#  define _MFIA(t)			(t = t)
#  define _PROBE(cmplt,s,b,x,t)		(t = t)
#  define _PROBEI(cmplt,s,b,i,t)	(t = t)
#  define _LCI(x,s,b,t)			(t = t)
#  define _IITLBT(r1,r2)		(r1 = r1)
#  define _IDTLBT(r1,r2)		(r1 = r1)

#else  /* ! (lint or _lint) */

#  define _RFI()          (void)_asm("RFI")
#  define _SSM(i,t)       (void)_asm("SSM",i,t)
#  define _RSM(i,t)       (void)_asm("RSM",i,t)
#  define _MTSM(r)        (void)_asm("MTSM",r)
#  define _LDSID(s,b,t)   (void)_asm("LDSID",s,b,t)
#  define _MTSP(r,sr)     (void)_asm("MTSP",r,sr)
#  define _MTCTL(r,t)     (void)_asm("MTCTL",r,t)
#  define _MFSP(sr,t)     (void)_asm("MFSP",sr,t)
#  define _MFCTL(r,t)     (void)_asm("MFCTL",r,t)
#  define _SYNC()         (void)_asm("SYNC")
#  define _SYNCDMA()      (void)_asm("SYNCDMA")
#  define _PROBER(s,b,x,t)  (void)_asm("PROBER",s,b,x,t)
#  define _PROBERI(s,b,i,t) (void)_asm("PROBERI",s,b,i,t)
#  define _PROBEW(s,b,x,t)  (void)_asm("PROBEW",s,b,x,t)
#  define _PROBEWI(s,b,i,t) (void)_asm("PROBEWI",s,b,i,t)
#  define _LPA(x,s,b,t)     (void)_asm("LPA",x,s,b,t)
#  define _LHA(x,s,b,t)     (void)_asm("LHA",x,s,b,t)
#  define _PDTLB(x,s,b)     (void)_asm("PDTLB",x,s,b)
#  define _PITLB(x,s,b)     (void)_asm("PITLB",x,s,b)
#  define _PDTLBE(x,s,b)    (void)_asm("PDTLBE",x,s,b)
#  define _PITLBE(x,s,b)    (void)_asm("PITLBE",x,s,b)
#  define _IDTLBA(r,s,b)    (void)_asm("IDTLBA",r,s,b)
#  define _IITLBA(r,s,b)    (void)_asm("IITLBA",r,s,b)
#  define _IDTLBP(r,s,b)    (void)_asm("IDTLBP",r,s,b)
#  define _IITLBP(r,s,b)    (void)_asm("IITLBP",r,s,b)
#  define _PDC(x,s,b)       (void)_asm("PDC",x,s,b)
#  define _FDC(x,s,b)       (void)_asm("FDC",x,s,b)
#  define _FIC(x,s,b)       (void)_asm("FIC",x,s,b)
#  define _FDCE(x,s,b)      (void)_asm("FDCE",x,s,b)
#  define _FICE(x,s,b)      (void)_asm("FICE",x,s,b)
#  define _BREAK(i1,i2)     (void)_asm("BREAK",i1,i2)
#  define _LDCD(i,s,b,t)    (void)_asm("LDCD",i,s,b,t)
#  define _LDCWS(i,s,b,t)   (void)_asm("LDCWS",i,s,b,t)
#  define _LDCWX(x,s,b,t)   (void)_asm("LDCWX",x,s,b,t)
#  define _SHD(r1,r2,p,t)   (void)_asm("SHD",r1,r2,p,t)

#  define _DEP(r,p,l,t)		(void)_asm("DEP",r,p,l,t)
#  define _DEPI(s,p,l,t)	(void)_asm("DEPI",s,p,l,t)
#  define _EXTRU(r,p,l,t)	(void)_asm("EXTRU",r,p,l,t)
#  define _HADD(r,b,t)		(void)_asm("HADD",r,b,t)
#  define _HADD_ss(r,b,t)	(void)_asm("HADD,ss",r,b,t)
#  define _HADD_us(r,b,t)	(void)_asm("HADD,us",r,b,t)
#  define _HAVE(r,b,t)		(void)_asm("HAVE",r,b,t)
#  define _HSL1ADD(r,b,t)	(void)_asm("HSL1ADD",r,b,t)
#  define _HSL2ADD(r,b,t)	(void)_asm("HSL2ADD",r,b,t)
#  define _HSL3ADD(r,b,t)	(void)_asm("HSL3ADD",r,b,t)
#  define _HSR1ADD(r,b,t)	(void)_asm("HSR1ADD",r,b,t)
#  define _HSR2ADD(r,b,t)	(void)_asm("HSR2ADD",r,b,t)
#  define _HSR3ADD(r,b,t)	(void)_asm("HSR3ADD",r,b,t)
#  define _HSUB(r,b,t)		(void)_asm("HSUB",r,b,t)
#  define _HSUB_ss(r,b,t)	(void)_asm("HSUB,ss",r,b,t)
#  define _HSUB_us(r,b,t)	(void)_asm("HSUB,us",r,b,t)
#  define _LDB(d,s,b,t)		(void)_asm("LDB",d,s,b,t)
#  define _LDBS(d,s,b,t)	(void)_asm("LDBS",d,s,b,t)
#  define _LDW(d,s,b,t)		(void)_asm("LDW",d,s,b,t)
#  define _LDWAX(r,b,t)		(void)_asm("LDWAX",r,b,t)
#  define _LDWS(d,s,b,t)	(void)_asm("LDWS",d,s,b,t)
#  define _STB(r,p,l,t)		(void)_asm("STB",r,p,l,t)
#  define _STBS(r,p,l,t)	(void)_asm("STBS",r,p,l,t)
#  define _STBYS(r,p,l,t)	(void)_asm("STBYS",r,p,l,t)
#  define _STWS(r,p,l,t)	(void)_asm("STWS",r,p,l,t)
#  define _ZDEP(r,p,l,t)	(void)_asm("ZDEP",r,p,l,t)

#  define _PREFETCH(d,b)            (void) _asm("LDW",d,0,b,0)
#  define _ADD(cmplt,r1,r2,t)       (void) _asm("ADD" cmplt,r1,r2,t)
#  define _SHLADD(cmplt,r1,sa,r2,t) (void) _asm("SHLADD" cmplt,r1,sa,r2,t)
#  define _SUB(cmplt,r1,r2,t)       (void) _asm("SUB" cmplt,r1,r2,t)
#  define _DS(cond,r1,r2,t)	  (void) _asm("DS"     cond,r1,r2,t)
#  define _CMPCLR(cond,r1,r2,t)	  (void) _asm("CMPCLR" cond,r1,r2,t)
#  define _OR(cond,r1,r2,t)	  (void) _asm("OR"     cond,r1,r2,t)
#  define _XOR(cond,r1,r2,t)	  (void) _asm("XOR"    cond,r1,r2,t)
#  define _AND(cond,r1,r2,t)	  (void) _asm("AND"    cond,r1,r2,t)
#  define _ANDCM(cond,r1,r2,t)	  (void) _asm("ANDCM"  cond,r1,r2,t)
#  define _UXOR(cond,r1,r2,t)	  (void) _asm("UXOR"   cond,r1,r2,t)
#  define _UADDCM(trapc,r1,r2,t)    (void) _asm("UADDCM" trapc,r1,r2,t)
#  define _DCOR(cmplt,r,t)	  (void) _asm("DCOR" cmplt,r,t)
#  define _ADDI(cmplt,i,r,t)        (void) _asm("ADDI" cmplt,i,r,t)
#  define _SUBI(cmplt,i,r,t)	  (void) _asm("SUBI" cmplt,i,r,t)
#  define _CMPICLR(cond,i,r,t)	  (void) _asm("CMPICLR" cond,i,r,t)
#  define _SHRPD(cond,r1,r2,sa,t)  (void) _asm("SHRPD" cond,r1,r2,sa,t)
#  define _SHRPW(cond,r1,r2,sa,t)  (void) _asm("SHRPW" cond,r1,r2,sa,t)
#  define _EXTRD(cmplt,r,pos,len,t) (void) _asm("EXTRD" cmplt,r,pos,len,t)
#  define _EXTRW(cmplt,r,pos,len,t) (void) _asm("EXTRW" cmplt,r,pos,len,t)
#  define _DEPD(cmplt,r,pos,len,t)  (void) _asm("DEPD" cmplt,r,pos,len,t)
#  define _DEPDI(cmplt,i,pos,len,t) (void) _asm("DEPDI" cmplt,i,pos,len,t)
#  define _DEPW(cmplt,r,pos,len,t)  (void) _asm("DEPW" cmplt,r,pos,len,t)
#  define _DEPWI(cmplt,i,pos,len,t) (void) _asm("DEPWI" cmplt,i,pos,len,t)
#  define _HAVG(r1,r2,t)	 	(void) _asm("HAVG",r1,r2,t)
#  define _HSHLADD(r1,sa,r2,t)	(void) _asm("HSHLADD",r1,sa,r2,t)
#  define _HSHRADD(r1,sa,r2,t)	(void) _asm("HSHRADD",r1,sa,r2,t)
#  define _HSHL(r,sa,t)		(void) _asm("HSHL",r,sa,t)
#  define _HSHR(cmplt,r,sa,t)	(void) _asm("HSHR" cmplt,r,sa,t)
#  define _PERMH(c,r,t)		(void) _asm("PERMH" c,r,t)
#  define _MIXW(cmplt,r1,r2,t)	(void) _asm("MIXW" cmplt,r1,r2,t)
#  define _MIXH(cmplt,r1,r2,t)	(void) _asm("MIXH" cmplt,r1,r2,t)
#  define _MTSARCM(r)		(void) _asm("MTSARCM",r)
#  define _MFIA(t)		(void) _asm("MFIA",t)
#  define _PROBE(cmplt,s,b,r,t)	(void) _asm("PROBE" cmplt,s,b,r,t)
#  define _PROBEI(cmplt,s,b,i,t)(void) _asm("PROBEI" cmplt,s,b,i,t)
#  define _LCI(x,s,b,t)		(void) _asm("LCI",x,s,b,t)
#  define _IITLBT(r1,r2)		(void) _asm("IITLBT",r1,r2)
#  define _IDTLBT(r1,r2)		(void) _asm("IDTLBT",r1,r2)

#endif /* !lint && !_lint */




#if defined(_KERNEL) || defined(RDB_LOADER) || defined(MONITOR)
/*
 * Definitions of special registers for use by the above macros.
 */

/* Hardware Space Registers */
#define	SR0	0
#define	SR1	1
#define	SR2	2
#define	SR3	3
#define	SR4	4
#define	SR5	5
#define	SR6	6
#define	SR7	7
#define	TS1	SR2

/* Hardware Floating Point Registers */
#define	FR0	0
#define	FR1	1
#define	FR2	2
#define	FR3	3
#define	FR4	4
#define	FR5	5
#define	FR6	6
#define	FR7	7
#define	FR8	8
#define	FR9	9
#define	FR10	10
#define	FR11	11
#define	FR12	12
#define	FR13	13
#define	FR14	14
#define	FR15	15

/* Hardware Control Registers */
#define	CR0	0
#define	RCTR	0		/* Recovery Counter Register */

#define	CR8	8		/* Protection ID 1 */
#define	PIDR1	8

#define	CR9	9		/* Protection ID 2 */
#define	PIDR2	9

#define	CR10	10
#define	CCR	10		/* Coprocessor Confiquration Register */

#define	CR11	11
#define	SAR	11		/* Shift Amount Register */

#define	CR12	12
#define	PIDR3	12		/* Protection ID 3 */

#define	CR13	13
#define	PIDR4	13		/* Protection ID 4 */

#define	CR14	14
#define	IVA	14		/* Interrupt Vector Address */

#define	CR15	15
#define	EIEM	15		/* External Interrupt Enable Mask */

#define	CR16	16
#define	ITMR	16		/* Interval Timer */

#define	CR17	17
#define	PCSQ	17		/* Program Counter Space queue */

#define	CR18	18
#define	PCOQ	18		/* Program Counter Offset queue */

#define	CR19	19
#define	IIR	19		/* Interruption Instruction Register */

#define	CR20	20
#define	ISR	20		/* Interruption Space Register */

#define	CR21	21
#define	IOR	21		/* Interruption Offset Register */

#define	CR22	22
#define	IPSW	22		/* Interrpution Processor Status Word */

#define	CR23	23
#define	EIRR	23		/* External Interrupt Request */

#define	CR24	24
#define	PPDA	24		/* Physcial Page Directory Address */
#define	TR0	24		/* Temporary register 0 */

#define	CR25	25
#define	HTA	25		/* Hash Table Address */
#define	TR1	25		/* Temporary register 1 */

#define	CR26	26
#define	TR2	26		/* Temporary register 2 */

#define	CR27	27
#define TLSP	27
#define	TR3	27		/* Temporary register 3 */

#define	CR28	28
#define	TR4	28		/* Temporary register 4 */

#define	CR29	29
#define	TR5	29		/* Temporary register 5 */

#define	CR30	30
#define	TR6	30		/* Temporary register 6 */

#define	CR31	31
#define	CPUID	31		/* MP identifier */

#endif /* _KERNEL */
