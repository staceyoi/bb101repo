/*
 * BEGIN_DESC
 * 
 *  File: 
 *      @(#)B11.23.0409LR	em/scall/scall_defs.h		$Revision: $
 * 
 *  Purpose:
 *	The most basic syscall-related defines.
 *
 *	IMPORTANT:
 *	  This file is included by both C and assembly files, so don't put
 *	  anything in here that both the C compiler and assembler can't
 *	  handle.
 * 
 *  Classification:			Release to Release Consistency Req:
 *		<<please select one of the following:>>
 * 	kernel subsystem private		none
 * 	kernel private				none
 * 	kernel 3rd party private		limited source
 * 	public					binary & source
 * 
 *  BE header:  yes
 *
 *  Shipped:  yes
 *	/usr/include/em/scall_defs.h
 *	/usr/conf/em/scall_defs.h
 *
 *  <<please delete the following note if this is a "public" header>>
 *  NOTE:
 *	This header file contains information specific to the internals
 *	of the HP-UX implementation.  The contents of this header file
 *	are subject to change without notice.  Such changes may affect
 *	source code, object code, or binary compatibility between
 *	releases of HP-UX.  Code which uses the symbols contained within
 *	this header file is inherently non-portable (even between HP-UX
 *	implementations).
 * 
 * END_DESC  
*/

#ifndef _MACHINE_SYS_SCALL_DEFS_INCLUDED
#define _MACHINE_SYS_SCALL_DEFS_INCLUDED

#ifdef __NO_EM_HDRS
    EM header file -- do not include this header file for non-EM builds.
#endif


/*
 * There is a fixed maximum number of arguments that system calls can
 * handle.  This number can easily be raised for any release by
 * increasing this constant.
 */

#define	SCALL_MAXARGS	10

/*
 * LW_MAXNUM -- the largest allowable light-weight syscall #.
 * EM doesn't have the same restriction that PA has.
 * The mksyscalls program expects a limit of some sort,
 * so just set the limit higher than the highest syscall.
 */
#define LW_MAXNUM 1024




#endif /* _MACHINE_SYS_SCALL_DEFS_INCLUDED */
