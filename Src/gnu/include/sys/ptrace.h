/*
 * BEGIN_DESC
 * 
 *  File: 
 *      @(#)B11.23.0409LR	common/sys/ptrace.h		$Revision: $
 * 
 *  Purpose:
 *      definitions for debugging and tracing other processes
 * 
 *  Classification:			Release to Release Consistency Req:
 *		<<please select one of the following:>>
 * 	kernel subsystem private		none
 * 	kernel private				none
 * 	kernel 3rd party private		limited source
 * 	public					binary & source
 * 
 *  BE header:  yes
 *
 *  Shipped:  yes
 *	/usr/include/sys/ptrace.h
 *	/usr/conf/sys/ptrace.h
 *
 *  <<please delete the following note if this is a "public" header>>
 *  NOTE:
 *	This header file contains information specific to the internals
 *	of the HP-UX implementation.  The contents of this header file
 *	are subject to change without notice.  Such changes may affect
 *	source code, object code, or binary compatibility between
 *	releases of HP-UX.  Code which uses the symbols contained within
 *	this header file is inherently non-portable (even between HP-UX
 *	implementations).
 * 
 * END_DESC  
*/


#ifndef _SYS_PTRACE_INCLUDED
#define _SYS_PTRACE_INCLUDED

#include <sys/stdsyms.h>
#include <sys/types.h>
#include <sys/signal.h>

#ifdef _INCLUDE_HPUX_SOURCE

/* Function prototype */

#ifndef _NO_USER_PROTOS
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if defined(__ia64) && !defined(_LIBC)  
  /* pragmas needed to support -B protected */  
#  pragma extern ptrace
#endif /* __ia64 && ! _LIBC */ 

#ifdef _PROTOTYPES
#ifndef _SVID3
#if !defined(__STDC_32_MODE__)
#  if defined(__ia64) && !defined(_LIBC)  
    /* pragmas needed to support -B protected */  
#    pragma extern ptrace64
#  endif /* __ia64 && ! _LIBC */ 
	extern long ptrace(int, pid_t, long, long, long);
	extern int64_t ptrace64(int, pid_t, uint64_t, uint64_t, uint64_t);
#else
	extern int ptrace(int, pid_t, int, int, int);
#endif
#endif /* _SVID3 */

#else /* not _PROTOTYPES */

#ifndef _SVID3
	extern int ptrace();
#endif /* _SVID3 */

#endif /* not _PROTOTYPES */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* not _NO_USER_PROTOS */


/*
 * ptrace 'request' arguments
 */
#define	PT_SETTRC	0	/* Set Trace */
#define	PT_RIUSER	1	/* Read User I Space */
#define	PT_RDUSER	2	/* Read User D Space */
#define	PT_RUAREA	3	/* Read User Area */
#define	PT_WIUSER	4	/* Write User I Space */
#define	PT_WDUSER	5	/* Write User D Space */
#define	PT_WUAREA	6	/* Write User Area */
#define	PT_CONTIN	7	/* Continue */
#define	PT_EXIT		8	/* Exit */
#define	PT_SINGLE	9	/* Single Step */
#define	PT_RUREGS	10	/* Read User Registers */
#define	PT_WUREGS	11	/* Write User Registers */
#define	PT_ATTACH	12	/* Attach To Process */
#define	PT_DETACH	13	/* Detach From Attached Process */
#define	PT_RDTEXT	14	/* Read User I Space (multiple words) */
#define	PT_RDDATA	15	/* Read User D Space (multiple words) */
#define	PT_WRTEXT	16	/* Write User I Space (multiple words) */
#define	PT_WRDATA	17	/* Write User D Space (multiple words) */
#define	PT_CONTIN1	18	/* Continue without clearing pending signals */
#define	PT_SINGLE1	19    /* Single Step without clearing pending signals */
#define	PT_SET_EVENT_MASK	20 /* Set Event Mask */
#define	PT_GET_EVENT_MASK	21 /* Get Event Mask */
#define	PT_GET_PROCESS_STATE	22 /* Get Process State */
#define	PT_GET_PROCESS_PATHNAME	23 /* Get Pathname for exec'd file */

/*lint -emacro(568,IS_PTRACE_REQ)*/
#define IS_PTRACE_REQ(i)   ((i) >= PT_SETTRC && (i) <= PT_GET_PROCESS_PATHNAME)

#ifdef	_KERNEL

/*
 * HP-PA Branch Instruction
 */
typedef struct {
	unsigned bri_opcode:6;
	unsigned bri_link:5;
	unsigned :5;
	unsigned bri_extension:3;
	unsigned bri_bve:1;
} branch_inst_t;

#define	briopcode(x)	(x).bri_opcode
#define	brilink(x)	(x).bri_link
#define	briextension(x)	(x).bri_extension
#define	bribve(x)	(x).bri_bve

#define	BRI_BE		0x38
#define	BRI_BLE		0x39
#define	BRI_Branch	0x3a
#define	BRI_BL		0x00
#define	BRI_BLR		0x02
#define	BRI_BLONG	0x05
#define	BRI_BVE		0x06
#define	BRI_BVEL	0x07
#define	IBL_REG		0x02	/* Implied Branch Link reg. */


/*
 * break instructions and support for single stepping
 */
extern caddr_t user_sstep, user_nullify;
extern sstepsysret();
extern sstepsigsysret(), sstepsigtrapret(), sstepsendsig();

#endif /* _KERNEL */


/*
 * data types and data structures needed for supporting
 * new ptrace requests.
 * events that threads need to/have respond to.
 */
enum _events_t {
	PTRACE_SIGNAL	=	0x00000001,
	PTRACE_FORK	=	0x00000002,
	PTRACE_EXEC	=	0x00000004,
	PTRACE_EXIT	=	0x00000008,
	PTRACE_VFORK	=	0x00000010 
};

/*
 * Default events traced processes will report.
 */
#define PTEVT_DEFAULT	0

typedef enum _events_t events_t;


/*
 * Structure associated with a traced process used for holding event and
 * signal information sent to it, by it's debugger.  This structure is
 * visible to user programs.
 */
typedef struct ptrace_event {
	sigset_t pe_signals;		/* signals that the debugged process
					   must mask from the debugger */
	events_t pe_set_event;		/* event that a debugged process is
					   required to respond to */
	char	 pe_spare[28];		/* reserved for future expansion */
} ptrace_event_t;


/*
 * Structure associated with a traced process, used for returning to the
 * debugger, state information associated with events occuring in the context
 * of the traced process.  This structure is visible to user programs. 
 */
typedef struct ptrace_state {
	events_t pe_report_event;	/* event that the debugged process has 
					   responded to */
	int	 pe_path_len;		/* length of pathname string in the
					   pathname buffer */
	pid_t	 pe_other_pid;		/* pid of child/parent process during
					   a fork or vfork event */
	char	 pe_spare[20];		/* reserved for future expansion */
} ptrace_state_t;


#endif /* _INCLUDE_HPUX_SOURCE */

#endif /* _SYS_PTRACE_INCLUDED */
