/*
 * BEGIN_DESC
 * 
 *  File: 
 *      @(#)B11.23.0409LR	common/sys/ttrace.h		$Revision: $
 * 
 *  Purpose:
 *      definitions for debugging and tracing multithreaded processes
 * 
 *  Classification:			Release to Release Consistency Req:
 * 	public					binary & source
 * 
 *  BE header:  yes
 *
 *  Shipped:  yes
 *	/usr/include/sys/ttrace.h
 *	/usr/conf/sys/ttrace.h
 * 
 * END_DESC  
*/


#ifndef _SYS_TTRACE_INCLUDED
#define _SYS_TTRACE_INCLUDED

#include <sys/stdsyms.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/ptrace.h>
#include <sys/param.h>
#include <machine/sys/scall_defs.h>

/*
 * Argument to attach/settrc commands. For all intents and purposes, this
 * is an opaque value to the users of this interface.
 */
#define TT_VERSION		TT_VERS1

typedef enum {
	TT_PROC_SETTRC = 0x400,		/* Set trace */
	TT_PROC_ATTACH,			/* Attach process */
	TT_PROC_DETACH,			/* Detach process */
	TT_PROC_RDTEXT,			/* Read text */
	TT_PROC_WRTEXT,			/* Write text */
	TT_PROC_RDDATA,			/* Read data */
	TT_PROC_WRDATA,			/* Write data */
	TT_PROC_STOP,			/* Stop process */
	TT_PROC_CONTINUE,		/* Continue process */
	TT_PROC_GET_PATHNAME,		/* Get executable's name */
	TT_PROC_GET_EVENT_MASK,		/* Get process's event mask */
	TT_PROC_SET_EVENT_MASK,		/* Set process's event mask */
	TT_PROC_GET_FIRST_LWP_STATE,	/* Get state of 1st lwp on stop list */
	TT_PROC_GET_NEXT_LWP_STATE,	/* Get state of next lwp on stop list */
	TT_PROC_EXIT,			/* Exit process */
	TT_PROC_GET_MPROTECT,		/* Get protection */
	TT_PROC_SET_MPROTECT,		/* Set protection */
	TT_PROC_SET_SCBM,		/* Set syscall bitmap */
	TT_PROC_CORE,			/* Dump process core */
	TT_PROC_GET_ARGS,		/* Get arguments to the microloader */
	TT_PROC_SET_IBPT_REGS,		/* Set the inst bpt reg for process */
	TT_PROC_SET_DBPT_REGS,		/* Set the data bpt reg for process */
	TT_PROC_GET_IBPT_REGS,		/* Get the inst bpt reg for process */
	TT_PROC_GET_DBPT_REGS,		/* Get the data bpt reg for process */
	TT_PROC_GET_NUM_IBPT_REGS,	/* Get the number of IBR */
	TT_PROC_GET_NUM_DBPT_REGS,	/* Get the number of DBR */
	TT_PROC_GET_SIGMASK,		/* Get process's ttrace() signal mask */
	TT_PROC_SET_SIGMASK,		/* Set process's ttrace() signal mask */
	TT_PROC_RDTEXT_NOATTACH,	/* Read text */
	TT_PROC_RDDATA_NOATTACH,	/* Read data */

	TT_LWP_STOP = 0x800,		/* Stop lwp */
	TT_LWP_CONTINUE,		/* Resume lwp */
	TT_LWP_SINGLE,			/* Single step lwp */
	TT_LWP_RUREGS,			/* Read lwp registers */
	TT_LWP_WUREGS,			/* Write lwp registers */
	TT_LWP_GET_EVENT_MASK,		/* Get lwp's event mask */
	TT_LWP_SET_EVENT_MASK,		/* Set process's event mask */
	TT_LWP_GET_STATE,		/* Get state of specific lwp */
	TT_LWP_SET_IBPT_REGS,		/* Set the inst bpt reg for lwp */
	TT_LWP_SET_DBPT_REGS,		/* Set the data bpt reg for lwp */
	TT_LWP_GET_IBPT_REGS,		/* Get the inst bpt reg for lwp */
	TT_LWP_GET_DBPT_REGS,		/* Get the data bpt reg for lwp */
	TT_LWP_RDRSEBS,			/* Read the kernel's RSE BS */
	TT_LWP_WRRSEBS,			/* Write the kernel's RSE BS */
	TT_LWP_TBRANCH,			/* Taken Branch lwp */
	TT_LWP_GET_SIGMASK,		/* Get lwp's ttrace() signal mask */
	TT_LWP_SET_SIGMASK,		/* Set lwp's ttrace() signal mask */
	TT_LWP_RUREGS_NOATTACH,		/* Read lwp registers */
	TT_LWP_RDRSEBS_NOATTACH,	/* Read the kernel's RSE BS */

	TT_NDR_GET_FLEV = 0xc00		/* Non-debug related: feature level */
} ttreq_t;

/*
 * Argument validation (process-wide, lwp-based, pthread-based, all).
 * NOTE: Update these when adding new commands.
 */
#define IS_TTRACE_PROCREQ(i) \
		((i) >= TT_PROC_SETTRC && (i) <= TT_PROC_RDDATA_NOATTACH)

#define IS_TTRACE_LWPREQ(i) \
		((i) >= TT_LWP_STOP && (i) <= TT_LWP_RDRSEBS_NOATTACH)

#define IS_TTRACE_REQ(i) \
		(IS_TTRACE_PROCREQ(i) || IS_TTRACE_LWPREQ(i))
		



/*
 * For ttrace arguments.
 */
#define TT_NULLARG		0L
#define TT_NOPC			1L

/*
 * For syscall bitmap.
 */
#define TTSCBM_SELECT		0L
#define TTSCBM_UNSELECT		1L

/*
 * For TT_PROC_ATTACH. For backward compatibility, TT_KILL_ON_EXIT must be 0.
 */
typedef enum {
	TT_KILL_ON_EXIT	  = 0,	/* Kill debuggees when debugger exits */
	TT_DETACH_ON_EXIT = 1	/* Detach debuggees when debugger exits */
} ttattopt_t;

/*
 * Bitmap type.
 */
typedef unsigned char ttbm_t;

/*
 * ttrace_wait 'options' arguments.
 */
typedef enum {
	TTRACE_WAITOK = 0,	/* wait until stopped thread found */
	TTRACE_NOWAIT		/* don't wait */
} ttwopt_t;

/*
 * Do not use these. Use TTVERSION.
 */
#define TT_VERSMAGIC		0x0dad0000
#define TT_VERS1		(TT_VERSMAGIC|1)

/*
 * This is used to determine what features are available. Unless TT_VERSION
 * changes, all behavior is backward compatible.
 *
 * Feature level 1: 10.30
 * Feature level 2: add TT_PROC_CORE
 * Feature level 3: add single-step info in SIGTRAP siginfo for traced processes
 * Feature level 4: allow normal behavior for non single-stepping SIGTRAPs
 * Feature level 5: internal change
 * Feature level 6: allow detach on debugger exit, add first NDR request
 * Feature level 7: add TTEVT_BPT_SSTEP event
 * Feature level 8: add TT_{PROC,LWP}_{GET,SET}_[ID]BPT_REGS,
 *                  TT_PROC_GET_NUM_[ID]BPT_REGS,
 *                  TT_LWP_RDRSEBS, TT_LWP_WRRSEBS and TT_LWP_TBRANCH
 * Feature level 9: add TT_{PROC,LWP}_{GET,SET}_SIGMASK requests
 * Feature level 10: add TTEVT_{PREFORK,FORK_FAIL} events
 * Feature level 11: allow TT_PROC_CORE to generate user given core file names
 * Feature level 12: allow reading memory and regs w/o attaching
 * Feature level 13: add TTEVT_UT_CREATE and TTEVT_UT_EXIT events
 */
#define TT_FEATURE_LEVEL		13

/*
 * Tracing events.
 */
typedef enum {
	TTEVT_NONE		=	0,
	TTEVT_SIGNAL		=	PTRACE_SIGNAL,
	TTEVT_FORK		=	PTRACE_FORK,
	TTEVT_EXEC		=	PTRACE_EXEC,
	TTEVT_EXIT		=	PTRACE_EXIT,
	TTEVT_VFORK		=	PTRACE_VFORK,
	TTEVT_SYSCALL_RETURN	=	0x00000020,
	TTEVT_LWP_CREATE	=	0x00000040,
	TTEVT_LWP_TERMINATE	=	0x00000080,
	TTEVT_LWP_EXIT		=	0x00000100,
	TTEVT_LWP_ABORT_SYSCALL	=	0x00000200,
	TTEVT_SYSCALL_ENTRY	=	0x00000400,
	TTEVT_SYSCALL_RESTART	=	0x00000800,
	TTEVT_BPT_SSTEP		=	0x00001000,
	TTEVT_PREFORK		=	0x00002000,
	TTEVT_FORK_FAIL		= 	0x00004000,
	TTEVT_UT_CREATE		=	0x00008000,	/* reserved */
	TTEVT_UT_EXIT		= 	0x00010000	/* reserved */
} ttevents_t;

/*
 * Backward compatibility.
 */
#define TTEVT_SYSCALL		TTEVT_SYSCALL_RETURN

/*
 * Default events traced processes will report.
 */
#define TTEVT_DEFAULT	0

/*
 * Event options.
 */
typedef enum {
	TTEO_NONE	  = 0x0, /* No options */
	TTEO_NOSTRCCHLD	  = 0x1, /* Child is not automatically traced */
	TTEO_PROC_INHERIT = 0x2, /* Child inherits parent events (proc) */
	TTEO_LWP_INHERIT  = 0x4, /* Child inherits parent events (lwp) */
	TTEO_NORM_SIGTRAP = 0x8  /* Non single-stepping SIGTRAPS are delivered*/
} tteopt_t;

#define TTEO_ALL							\
	(TTEO_NOSTRCCHLD|TTEO_PROC_INHERIT|TTEO_LWP_INHERIT|TTEO_NORM_SIGTRAP)

/*
 * Structure associated with a traced process used for holding event and
 * signal information sent to it by its debugger.
 */
typedef struct {
	sigset_t	tte_signals;	/* signals that the debugged process
					   must mask from the debugger */
	ttevents_t	tte_events;	/* events that a debugged process is
					   required to respond to */
	tteopt_t	tte_opts;	/* options */
	char		tte_spare[24];	/* to keep same size as ptrace */
} ttevent_t;


/*
 * Data associated with an exec event.
 */
typedef struct {
	int	tts_pathlen;		/* for subsequent path name request */
} ttexec_data_t;

/*
 * Data associated with a fork event.
 */
typedef struct {
	pid_t	tts_fpid;		/* other pid involved in fork */
	lwpid_t	tts_flwpid;		/* other lwpid involved in fork */
	int	tts_isparent;		/* if 1, this is the fork parent */
	int	tts_errno;  		/* error return */
} ttfork_data_t;

/*
 * Values for tts_type
 */
typedef enum {
	TTS_FORK,	/* Generic fork process */
	TTS_VFORK	/* vfork process */
} ttpf_t;

/*
 * Data associated with pre-fork event.
 */
typedef struct {
	pid_t	tts_fpid;		/* other pid involved in fork */
	lwpid_t	tts_flwpid;		/* other lwpid involved in fork */
	ttpf_t tts_type;		/* type of fork */
} ttprefork_data_t;	
	
/*
 * Values for tts_sigflags.
 */
typedef enum {
	TTSF_USERSIGINFO=	0x0001	/* siginfo was delivered */
} ttsigf_t;

/*
 * Data associated with a signal event.
 */
typedef struct {
	int		tts_signo;	/* signal being delivered */
	ttsigf_t	tts_sigflags;	/* signal flags */
	uint64_t	tts_sigaction;	/* signal action */
	siginfo_t	tts_siginfo;	/* and its siginfo */
} ttsignal_data_t;

/*
 * Data associated with a thread event.
 */
typedef struct {
	lwpid_t		tts_target_lwpid; /* target lwpid, if applicable */
} ttthread_data_t;

/*
 * Data associated with a syscall event.
 */
typedef struct {
	int64_t		tts_rval[2];	/* return values */
	int		tts_errno;	/* error return */
} ttsyscall_data_t;

/*
 * Data associated with an exit event.
 */
typedef struct {
	int		tts_exitcode;	/* status parent will get in wait() */
} ttexit_data_t;

/*
 * Data associated with a breakpoint/single-step event.
 */
typedef enum {
	TTBPT_SSTEP	=	0,	/* Single Step */
	TTBPT_BPT,			/* Breakpoint Instruction */
	TTBPT_TBRANCH,			/* Taken Branch */
	TTBPT_IBPT,			/* Instruction Breakpoint */
	TTBPT_DBPT,			/* Data Breakpoint */
	TTBPT_NONE	=	-1
} ttbpt_type_t;

typedef struct {
	ttbpt_type_t	tts_isbpt;	/* true if the event is a breakpoint */
	uint32_t	tts_imm;	/* Break immediate value for TTBPT_BPT */
	uint64_t	tts_addr[2];	/* Associated address value(s) */
	uint32_t	tts_instr[4];	/* Instruction Value(s) */
} ttbpt_data_t;

/*
 * Mask Options 
 */
typedef enum {
	TTMO_NONE	= 0x0,	/* No options */
	TTMO_PROC_INHERIT	= 0x1,	/* Child inherits parent masks (proc) */
	TTMO_LWP_INHERIT	= 0x2	/* Child inherits parent masks (lwp) */
} ttmopt_t;

/*
 * Structure associated with a traced process used for holding 
 * signals that are masked by the debugger - basically a ttrace()
 * signal mask. This signal mask is separate from the original 
 * thread's/process's signal mask. The behavior is that when a 
 * signal is received, the thread will OR it's signal mask and 
 * it's ttrace() signal mask to determine if it should process 
 * the signal.
 */
typedef struct {
	sigset_t	ttm_signals;	/* signals that the debugged process
					   must mask from delivery */
	ttmopt_t	ttm_opts;	/* options */
} ttmask_t;

/*
 * Values for tts_flags.
 */
typedef enum {
	TTS_WASSUSPENDED=	0x0001,	/* thread was user-suspended */
	TTS_WASSLEEPING	=	0x0002,	/* thread was sleeping */
	TTS_WASRUNNING	=	0x0004,	/* thread was runn{ing,able} */
	TTS_WAITEDFOR	=	0x0008,	/* thread has already been "seen" */
	TTS_INSYSCALL	=	0x0010,	/* thread is performing a syscall */
	TTS_32BIT	=	0x0020,	/* 32-bit process */
	TTS_ATEXIT	=	0x0040	/* thread is at exit() */
} ttsf_t;

#define TTS_STATEMASK	(TTS_WASSUSPENDED|TTS_WASSLEEPING|TTS_WASRUNNING)

/*
 * Structure associated with a traced process, used for returning to the
 * debugger, state information associated with events occuring in the context
 * of the traced process. The event dictates which data is valid.
 */
typedef struct {
	pid_t		    tts_pid;	/* pid of process getting the event */
	lwpid_t		    tts_lwpid;	/* lwpid of thread getting the event */
	uint64_t	    tts_user_tid; /* user thread ID */
	ttevents_t	    tts_event;	/* event the debugged process has
					   responded to */
	ttsf_t		    tts_flags;	/* prior state, etc... */
	int		    tts_scno;	/* syscall number */
	int		    tts_scnargs;/* number of args to syscall */
	uint64_t	    tts_scarg[SCALL_MAXARGS]; /* arguments to syscall */
	union {
	    ttexec_data_t   tts_exec;	/* data associated with exec event */
	    ttfork_data_t   tts_fork;	/* data associated with fork events */
	    ttprefork_data_t   tts_prefork; /* data associated with fork events */
	    ttsignal_data_t tts_signal;	/* data associated with signal event */
	    ttthread_data_t tts_thread;	/* data associated with thread events */
	    ttsyscall_data_t tts_syscall; /* data associated with a syscall */
	    ttexit_data_t   tts_exit;   /* data associated with an exit */
	    ttbpt_data_t    tts_bpt_sstep; /* data associated with bpt/sstep */
	    char	    tts_fill[128]; /* space for expansion */
	} tts_u;
} ttstate_t;

/*
 * Structure used by a MxN process to communicate requests between the
 * pthread library debug daemon thread and the kernel debug daemon
 * thread.
 */

typedef struct tt_debug_request {
	ttreq_t		ttd_request;	/* Debug request */
	int		ttd_data;	/* Target user thread */
	ttstate_t	ttd_state;	/* State of the thread */
	char		ttd_pad[40];
} tt_debug_request_t;

#define TTS_SEEN	TTS_WAITEDFOR

#ifndef _NO_USER_PROTOS
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if defined(__ia64) && !defined(_LIBC)  
  /* pragmas needed to support -B protected */  
#  ifndef _SVID3
#    pragma extern ttrace, ttrace_wait
#  endif /* _SVID3 */
#endif /* __ia64 && ! _LIBC */ 

#ifdef _PROTOTYPES

#ifndef _SVID3
	extern int ttrace(ttreq_t, pid_t, lwpid_t, uint64_t, uint64_t, uint64_t);
	extern int ttrace_wait(pid_t, lwpid_t, ttwopt_t, ttstate_t *, size_t);

	extern int ttrace_notify(ttevents_t, int); /* "int" is for user thread id */

	extern int ttrace_proc_traced(void);
#endif /* _SVID3 */

#else /* not _PROTOTYPES */

#ifndef _SVID3
	extern int ttrace();
	extern int ttrace_wait();
	extern int ttrace_notify();
	extern int ttrace_proc_traced();
#endif /* _SVID3 */

#endif /* not _PROTOTYPES */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* not _NO_USER_PROTOS */

#endif /* _SYS_TTRACE_INCLUDED */
