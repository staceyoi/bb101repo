/*
 * BEGIN_DESC
 * 
 *  File: 
 *      @(#)	common/sys/user.h		$Revision: $
 * 
 *  Purpose:
 *      <<please update with a synopis of the functionality provided by this file>>
 * 
 *  Classification:			Release to Release Consistency Req:
 *		<<please select one of the following:>>
 * 	kernel subsystem private		none
 * 	kernel private				none
 * 	kernel 3rd party private		limited source
 * 	public					binary & source
 * 
 *  BE header:  yes
 *
 *  Shipped:  yes
 *	/usr/include/sys/user.h
 *	/usr/conf/sys/user.h
 *
 *  <<please delete the following note if this is a "public" header>>
 *  NOTE:
 *	This header file contains information specific to the internals
 *	of the HP-UX implementation.  The contents of this header file
 *	are subject to change without notice.  Such changes may affect
 *	source code, object code, or binary compatibility between
 *	releases of HP-UX.  Code which uses the symbols contained within
 *	this header file is inherently non-portable (even between HP-UX
 *	implementations).
 * 
 * END_DESC  
*/


#ifndef _SYS_USER_INCLUDED /* allows multiple inclusion */
#define _SYS_USER_INCLUDED

#ifndef _SYS_STDSYMS_INCLUDED
#  include <sys/stdsyms.h>
#endif   /* _SYS_STDSYMS_INCLUDED  */

#include <machine/pcb.h>	/* kernel src: <machine/sys/pcb.h> */
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/vmparam.h>
#include <sys/param.h>
#include <sys/signal.h>
#include <sys/vmmac.h>
#include <machine/save_state.h>	/* kernel src: <machine/sys/save_state.h> */
#include <machine/sys/setjmp.h>
#include <sys/ucontext.h>
#include <sys/types.h>
#include <machine/scall_defs.h>	/* kernel src: <machine/scall/scall_defs.h> */

/* 
 * NFDCHUNKS = number of file descriptor chunks of size SFDCHUNK available
 * per process.  SFDCHUNK must be NBTSPW = number of bits per int for
 * select to work.
 */
#define SFDCHUNK	NBTSPW
#define NWORDS(n)       ((((n) & (SFDCHUNK - 1)) == 0) ? (n >> 5) :	\
							 ((n >> 5) + 1))
/* NWORDS is the number of words necessary for n file descriptors to allow 
   for one bit per file descriptor. */

#define NFDCHUNKS(n)    NWORDS(n)

/*
 * Some constants for fast multiplying, dividing, and mod-ing (%) by SFDCHUNK
 */

#define SFDMASK	0x1f
#define SFDSHIFT 5

typedef enum __pofile_flags {
	UF_EXCLOSE = 0x1,	/* auto-close on exec */
	UF_MAPPED = 0x2,	/* mapped from device */
	UF_FDLOCK = 0x4,	/* lockf was done,see vno_lockrelease */
	UF_INUSE  = 0x8		/* Set when allocated.  (multithreaded) */
} pofile_flags_t;

/*
 * maxfiles is maximum number of open files per process.  
 * This is also the "soft limit" for the maximum number of open files per 
 * process.  maxfiles_lim is the "hard limit" for the maximum number of open
 * files per process.
 */
extern	int maxfiles; 
extern  int maxfiles_lim;

#define LOCK_TRACK_MAX          10              /* for qfs lock tracking */

/* Macros to update the savestate used by the VM system's HDL */

#define SET_PFAULTSS(newss, savess) {   \
	savess = u.u_pfaultssp;         \
	u.u_pfaultssp = newss;          \
}

#define RESET_PFAULTSS(savess)      {   \
	u.u_pfaultssp = savess;         \
}

/* u_eosys values */
typedef enum user_eosys {
	EOSYS_NOTSYSCALL      = 0,      /* not in kernel via syscall() */
	EOSYS_NORMAL          = 1,      /* in syscall but nothing notable */
	EOSYS_INTERRUPTED     = 2,      /* signal is not yet fully processed */
	EOSYS_RESTART         = 3,      /* user has requested restart */
	EOSYS_NORESTART       = 4,      /* user has requested error return */
	RESTARTSYS            = EOSYS_INTERRUPTED, /* temporary!!! */
	EOSYS_EXEC            = 6       /* exec() has loaded new image okay */
} user_eosys_t;

/* ESCXXX PA-PDK */
typedef enum user_sstep {
	ULINK   = 0x01f,                  /* link register */
	USSTEP  = 0x020,                  /* process is single stepping */
	UPCQM   = 0x040,                  /* pc queue modified */
	UBL     = 0x080,                  /* branch and link at pcq head */
	UBE     = 0x100                   /* branch external at pcq head */
} user_sstep_t;

#ifndef _SIGWORDS
#define _SIGWORDS
#define SIGWORDS 2
#endif

#ifndef _KSIGSET_T
#define _KSIGSET_T
typedef struct __ksigset {
          int sigset[SIGWORDS];
} ksigset_t;
#endif    /* _KSIGSET_T */


/*
 * Per process structure containing data that
 * isn't needed in core when the process is swapped out.
 */

typedef struct user {
	struct	pcb u_pcb;
	struct	proc *u_procp;		/* pointer to proc structure */
	struct	kthread *u_kthreadp;	/* pointer to thread structure */
	save_state_t	*u_sstatep;	/* pointer to a saved state */
	save_state_t	*u_pfaultssp; /* most recent savestate */

/* syscall parameters, results and catches */
	long    u_arg[2*SCALL_MAXARGS]; /* arguments to current system call */
					/* ARGS MUST BE LONG OR UNSIGNED LONG */
					/* BECAUSE OF ILP32/LP64 DECISION. */
					/* Note: We reserve 2*SCALL_MAXARGS */
					/* because we now support long long */
					/* args which, from 32-bit programs, */
					/* are passed using 2 registers each. */
	caddr_t	u_ap;			/* pointer to arglist */

/*ESCXXX PA-PDK BEGIN 951127 */
	label_t	u_qsave;		/* for non-local gotos on interrupts */
/*ESCXXX PA-PDK BEGIN 951127 */

	u_short u_nargs;		/* number of arguments to syscall */
	u_short u_error;                /* return error code */

	union {				/* syscall return values */
		struct	{
			long	R_val1;
			long	R_val2;
		} u_rv;
#define	r_val1	u_rv.R_val1
#define	r_val2	u_rv.R_val2

/* Bell-to-Berkeley translations */
#define u_rval1		u_r.r_val1
#define u_rval2		u_r.r_val2

#ifdef _KERNEL
		/* HPUX_REVISIT_64_BIT - This field is used by 64on32 ptrace(),
		 * and currently won't work on the 64-bit kernel; need to
		 * assign r_val[12] separately like 64on32 lseek().
		 */
		uint64_t r_val64;
#define u_rval64	u_r.r_val64
#endif

		long	r_off;
		long	r_time;
	} u_r;
	user_eosys_t u_eosys;		/* special action on end of syscall */
	u_short	u_syscall;		/* syscall # passed to signal handler */

/* 1.1 - processes and protection */

	/*
	 * The user credentials pointer (for uid, gid, etc) is now accessed
	 * through the p_cred() accessor for the proc structure and
	 * through the kt_cred() accessor for the kthread structure.
	 */

	struct	audit_filename *u_audpath;	/* ptr to audit pathname info */
	struct	audit_string *u_audstr;	/* ptr to string data for auditing */
	struct	audit_sock *u_audsock;	/* ptr to sockaddr data for auditing */
	char    *u_audxparam;           /* generic loc. to attach audit data */

/* 1.2 - memory management */
/*ESCXXX PA-PDK BEGIN 951127 */
	label_t u_ssave;		/* label variable for swapping */
#ifdef _KERNEL
	tlabel_t u_psave;		/* trap recovery vector - machine dep */
#else
	int reserved[12];		/* because vmparam.h doesn't define */
					/* tlabel_t unless _KERNEL defined */
#endif
/*ESCXXX PA-PDK END 951127 */

	time_t	u_outime;		/* user time at last sample */

/* 1.3 - signal management */
		/* same for users and the kernel; see signal.h		  */
	ksigset_t u_oldmask;		/* saved mask from before sigpause */
	int	u_code;			/* ``code'' to trap */
 	struct	{
 		void		*uss_alternatesp;
 		size_t		uss_size;
 		unsigned int	uss_enabled:1;
 		unsigned int	uss_inuse:1;
 	} u_signalstack;
 

/* BEGIN TRASH */
	struct squ_s {		/* Streams per process run queue */
		struct sqe_s * squ_next;  /* Points to 1st request on queue */
		struct sqe_s * squ_prev;  /* Points to last request on queue */
		unsigned long squ_flags;  /* See ../streams/str_stream.h. If
					   * SQ_IN_STREAMS set, process is
					   * executing in streams environment.
					   * If not set, process is outside
					   * of streams.
					   */
        } u_strsqh;

/* END TRASH */

/* ucontext processing fields */
 	stack_t u_curstack;  		/* info of stack context is using */
 	ucontext_t *u_prevcontext; 	/* ptr to context to be resumed   */
/* end ucontext processing fields */
 
	dev_t	u_devsused;		/* count of locked devices */

/*ESCXXX PA-PDK BEGIN 951127 */
	user_sstep_t u_sstep;		/* process single stepping flags */
	unsigned long u_pcsq_head;	/* pc space and offset queue */
	unsigned long u_pcoq_head;	/*   values for single stepping */
	unsigned long u_pcsq_tail;
	unsigned long u_pcoq_tail;
	union {
	  struct {
#if !defined(__STDC_32_MODE__)
	    uint64_t u_ipsw;		/* ipsw for single stepping */
	    int64_t u_gr1;		/* value for general register 1 */
	    int64_t u_gr2;		/* value for general register 2 */
#else
	    uint32_t u_ipsw[2];		/* ipsw for single stepping */
	    int32_t u_gr1[2];		/* value for general register 1 */
	    int32_t u_gr2[2];		/* value for general register 2 */
#endif
	  } u_64;
	  struct {
	    uint32_t u_ipsw_hi;		/* ipsw for single stepping */
	    uint32_t u_ipsw_lo;
	    int32_t u_gr1_hi;		/* value for general register 1 */
	    int32_t u_gr1_lo;
	    int32_t u_gr2_hi;		/* value for general register 2 */
	    int32_t u_gr2_lo;
	  } u_32;
	} u_wide;
/* ESCXXX PA-PDK END 951127 */

	/* KI data structures in u-area */
	kt_t     u_syscall_time;        /* KI syscall timestamp */
	kt_t     u_accumin;             /* KI accum'ed timestamp (init time)  */
	char    *u_vp;                  /* KI device location of this kthread */
	tid_t    u_threadid;            /* KI user thread id */
	uint32_t  u_ki_site;            /* KI ksite_t address if remote request */
	uint32_t u_swtchd_for_sema;     /* KI used by missed semaphores */
	unsigned char u_ktc_enabled;    /* KI kboolean_t flag to clockstack
                                                                mgnt routines*/
#define u_ktc_flag    u_ktc_enabled
	unsigned char u_ki_charpad[3];
	char  *u_ki_clk_tos_ptr;        /* KI top-of-stack ptr to clk stack */
#define KI_CLK_STACK_SIZE 16
	char u_ki_clk_stack[KI_CLK_STACK_SIZE]; /* KI: clock stack */

	/* This field is used by the Lowfat project               */
	int *send_term_ptr;              /* pointer to the list of Send */
			 		 /* Terminus used by the process*/

	int	u_debug:1,		/* Live Symbolic Debug Flags */
		u_debug2:1,
		u_debug3:1,
		u_debug4:1,
		u_debug5:1,
		u_debug6:1,
		u_debug7:1,
		u_debug_spare:24;

	/*
	 * Convex specific features.
	 */
	void   *u_cnx_features;

	u_short	u_syscalldm;		/* data-model for the current syscall */

	union {				/* double word aligned stack */
	 double	s_dummy;
	 int	s_stack[1];
	} u_s;				/* must be last thing in user_t */
#define	u_stack	u_s.s_stack
} user_t;
#define _USER_T

#ifndef _CLEAN_BE
#include <sys/cred.h>
#endif /* _CLEAN_BE */

#ifdef _UNSUPPORTED

	/*
	 * NOTE: The following header file contains information specific
	 * to the internals of the HP-UX implementation. The contents of
	 * this header file are subject to change without notice. Such
	 * changes may affect source code, object code, or binary
	 * compatibility between releases of HP-UX. Code which uses
	 * the symbols contained within this header file is inherently
	 * non-portable (even between HP-UX implementations).
	 */
#ifdef _KERNEL
#	include <machine/vm/hdl_user.h>
#endif /* _KERNEL */
#endif /* _UNSUPPORTED */

#endif /* ! _SYS_USER_INCLUDED */
