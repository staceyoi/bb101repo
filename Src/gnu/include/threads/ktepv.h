/* @(#) $Revision: ../hdr/threads/ktepv.h@@/main/i80/7 $ */

/*
 * Offsets into kernel threads library entry point vector
 */


#ifndef _KTEPV_INCLUDED
#define _KTEPV_INCLUDED


#define _KTEP_MUTEX_INIT	0	/* 26 */
#define _KTEP_MUTEX_DESTROY	1	/* 27 */
#define _KTEP_MUTEX_LOCK	2	/* 28 */
#define _KTEP_MUTEX_UNLOCK	3	/* 29 */
#define _KTEP_MUTEX_TRYLOCK	4	/* 30 */
#define _KTEP_SELF			5	/* 31 */
#define _KTEP_DELAY_NP		6	/* 32 */

#define _KTEP_KEYCREATE		7	/* 40 */
#define _KTEP_GETSPECIFIC	8	/* 41 */
#define _KTEP_SETSPECIFIC	9	/* 42 */

#define _KTEP_ONCE			10	/* 58 */
#define _KTEP_EQUAL			11	/* 59 */
#define _KTEP_SETASYNCCANCEL	12	/* 60 */
#define _KTEP_SETCANCEL 	13	/* 61 */
#define _KTEP_TESTCANCEL 	14	/* 62 */
#define _KTEP_CREATE		15	/* 63 */

#define _KTEP_EXC_PUSH_CTX	16	/* 65	 USER threads only */
#define _KTEP_EXC_POP_CTX	17	/* 66	 USER threads only */
#define _KTEP_CTXCB_HPUX	18	/* 67    User threads only: For C++ */

#define	_KTEP_MUTEXATTR_INIT	19	/* 76 */
#define	_KTEP_MUTEXATTR_SETKIND	20	/* 77 */
#define	_KTEP_GETSPECIFIC_ERRNO	21	/* 78 */
#define	_KTEP_EXIT		22	/* 79 */
#define	_KTEP_THREAD_MAIN	23	/* 80 */
#define	_KTEP_COND_DESTROY	24	/* 81 */
#define	_KTEP_COND_INIT		25	/* 82 */
#define	_KTEP_COND_WAIT		26	/* 83 */
#define	_KTEP_COND_SIGNAL	27	/* 84 */
#define	_KTEP_COND_TIMEDWAIT	28	/* 85 */
#define	_KTEP_COND_BROADCAST	29	/* 86 */
#define	_KTEP_SIGMASK		30	/* 87 */
#define	_KTEP_ATFORK		31	/* 88 */
#define _KTEP_ATTR_INIT         37
#define	_KTEP_DETACH		41	
#define _KTEP_SETSTACKSIZE      48
#define	_KTEP_GETCANCELSTACK	79	/* 88 */
#define	_KTEP_CALLBACK		81	
#define _KTEP_ISBOUND		82
#define _KTEP_USER_SLEEP	83
#define _KTEP_SETSAINFO		84
#define _KTEP_GETSAINFO		85

/*
 * Threads library entry point vector
 */

#ifdef __ia64 
#  pragma extern _ktepv
#endif /* __ia64 */ 
#ifdef __cplusplus
extern "C" {
extern int (**_ktepv)(...);
}
#else
extern int (**_ktepv)();
#endif


#endif /* _KTEPV_INCLUDED */

