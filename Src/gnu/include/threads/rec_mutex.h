/* @(#) $Revision: ../hdr/threads/rec_mutex.h@@/main/i80/11 $ */

/* Revision created Dec 26,1995.
 * Header file names cleanup.
 * Coexistence of both macro flavors.
 */

#ifndef _REC_MUTEX_INCLUDED
#define _REC_MUTEX_INCLUDED

/* 
 * Directive for removing cma support
 */

#ifdef __ia64
#define __CMA_UNSUPPORTED
#endif /* __ia64 */

#ifdef _KERNEL_THREADS

#ifdef _PROTOTYPES
#define exc_push_ctx(x) \
    ((void (*) (volatile exc_context_t *))\
    (*_ktepv[_KTEP_EXC_PUSH_CTX]))(x)
#define exc_pop_ctx(x) \
    ((void (*) (volatile exc_context_t *))\
    (*_ktepv[_KTEP_EXC_POP_CTX]))(x)
#else
#define exc_push_ctx(x) \
    (*_ktepv[_KTEP_EXC_PUSH_CTX])(x)
#define exc_pop_ctx(x) \
    (*_ktepv[_KTEP_EXC_POP_CTX])(x)
#endif


/*
 * Recursive mutex definition.
 */


#include <sys/stdsyms.h>

#ifndef __CMA_UNSUPPORTED
#include <dce/cma_pthread.h>    /* Localized version of CMA pthread.h file */
#else /* __CMA_UNSUPPORTED */
#include <stdlib.h>
#endif /* !__CMA_UNSUPPORTED */

#include <sys/pthread.h>	/* The POSIX threads pthread.h file. */
#ifndef __CMA_UNSUPPORTED
#include <threads/tepv.h>
#endif /* !__CMA_UNSUPPORTED */
#include <threads/ktepv.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Exported Types */
/* The cma data types  are excluded for IA64 */
typedef  union t_u {
#ifndef __CMA_UNSUPPORTED
		cma_t  cma;
#endif /* !__CMA_UNSUPPORTED */
		pthread_t pthread;
		} __thread_t;          /* sizeof struct cma_handle */

typedef  union m_u {
#ifndef __CMA_UNSUPPORTED
		 cma_t_handle	cma;
#endif /* !__CMA_UNSUPPORTED */
		 pthread_mutex_t pthread;
		 } __thread_mutex_t;    /* sizeof kthreads mutex_t */
typedef  union k_t {
#ifndef __CMA_UNSUPPORTED
		 cma_key_t cma;
#endif /* !__CMA_UNSUPPORTED */
		 pthread_key_t pthread;
		 } __thread_key_t ;	
#ifndef __CMA_UNSUPPORTED
typedef  cma_addr_t      __thread_addr_t ;    /* void * */
typedef  cma_cleanup_t   __thread_cleanup_t;  /* void (*)(void*)*/
#else /* __CMA_UNSUPPORTED */
typedef void * __thread_addr_t ;    /* void * */

#ifdef _PROTOTYPES
typedef void (*__thread_cleanup_t)(void *);  /* void (*)(void*)*/
#else /* !_PROTOTYPES */
typedef void (*__thread_cleanup_t)();  /* void (*)(void*)*/
#endif /* _PROTOTYPES */

#endif /* !__CMA_UNSUPPORTED */

typedef union c_u {
#ifndef __CMA_UNSUPPORTED
		cma_cond_t cma;
#endif /* !__CMA_UNSUPPORTED */
		pthread_cond_t pthread;
		} 
__thread_cond_t;
#ifndef __CMA_UNSUPPORTED 
typedef cma_t_handle     __thread_condattr_t;
#endif /* !__CMA_UNSUPPORTED */


typedef struct {
	char executing;
	char completed ;		
	__thread_mutex_t  mutex ; 
	}  __thread_once_t;          	/* The essence of pthread_once_t.*/

/*  Initializer for __thread_once_t   */
#define	__THREAD_ONCE_INIT    {0 }        

#if defined(__ia64) && ! defined(_LIBC)  
  /* pragmas needed to support -B protected */  
#  pragma extern __ismt, __thread_cleanup_handlerq

#endif /* __ia64 && ! _LIBC */ 
extern int __ismt ;                     /* __ismt !=0 for multithreaded apps.*/
extern int __isrealmt ;                 /* set when the 1st thread is created */
extern int __is_mxn_enabled ;			/* = 0 if MxN pthread not linked in */
extern pthread_key_t  __thread_cleanup_handlerq ;     

#define  _KTHREADS             (__ismt&0x02)
#define  _KTHREADSMT           (__isrealmt&0x02)
#ifndef __CMA_UNSUPPORTED
#define  _CMATHREADS           (__ismt&0x01)
#endif /* !__CMA_UNSUPPORTED */

/* Define REC_MUTEX for  backward compat.,equivalent to  __thread_mutex_t */

#define REC_MUTEX __thread_mutex_t 

/*
 * Mutex management macros
 */
# ifdef __CMA_UNSUPPORTED
#define __mutex_alloc(x)	(_KTHREADS ? __thread_mutex_alloc(x) : 0)
#define __rec_mutex_init(x)	(_KTHREADS ? __thread_rec_mutex_init(x) : 0)
#define __fast_mutex_init(x)	(_KTHREADS ? __thread_fast_mutex_init(x) : 0)
#define __dbg_mutex_init(x)	(_KTHREADS ? __thread_dbg_mutex_init(x) : 0)
#define __mutex_free(x)		(_KTHREADS ? __thread_mutex_free(x) : 0)
#define __mutex_lock(x)		(_KTHREADS ? __thread_mutex_lock(x) : 0)
#define __mutex_unlock(x)	(_KTHREADS ? __thread_mutex_unlock(x) : 0)
#define __mutex_trylock(x)	(_KTHREADS ? __thread_mutex_trylock(x) : 0)
#else /* !__CMA_UNSUPPRTED */
#define __mutex_alloc(x)	(__ismt ? __thread_mutex_alloc(x) : 0)
#define __rec_mutex_init(x)	(__ismt ? __thread_rec_mutex_init(x) : 0)
#define __fast_mutex_init(x)	(__ismt ? __thread_fast_mutex_init(x) : 0)
#define __dbg_mutex_init(x)	(__ismt ? __thread_dbg_mutex_init(x) : 0)
#define __mutex_free(x)		(__ismt ? __thread_mutex_free(x) : 0)
#define __mutex_lock(x)		(__ismt ? __thread_mutex_lock(x) : 0)
#define __mutex_unlock(x)	(__ismt ? __thread_mutex_unlock(x) : 0)
#define __mutex_trylock(x)	(__ismt ? __thread_mutex_trylock(x) : 0)
#endif /* __CMA_UNSUPPORTED */

/*
 * Cancellation cleanup handling macros:
 * These macros are completely unreadable now. Here is how thread safers
 * should use them.
 *  __mutex_push(x,y)        : Install a handler that will unlock a mutex.
 *  __mutex_push_file(x,y)   : Install a handler that will unlock a filelock.
 *  __thread_push(func,x,y)  : Install a generic cleanup handler. func() will
 *                             do the necessary cleanup.
 *  _rec_mutex_push(x,y)     : Supported for backward compatibility
 *  _rec_mutex_push_file(x,y)  : Supported for backward compatibility
 *
 *  __mutex_pop(x,y)         : Close scope, optionally execute handler.
 *  __mutex_pop(x,y)         : Close scope, optionally execute handler.
 *  __thread_pop(x,y)        : Close scope, optionally execute handler.
 *
 *  _rec_mutex_pop(x,y)     : Supported for backward compatibility
 *  _rec_mutex_pop_file(x,y)  : Supported for backward compatibility
 */

/* Thread management macros have been modified to exclude the cma handlers for IA64.
 * They are gaurded by __CMA_UNSUPPORTED so as to retain the functionality for PA and exclude for IA64
 */
  
#ifdef _TLS_SUPPORT
#if defined(__ia64) && ! defined(_LIBC)  
  /* pragmas needed to support -B protected */  
#  pragma extern __thread_lib_stktop 
#endif /* __ia64 && ! _LIBC */ 
extern __thread __pthread_cleanup_handler_t ** __thread_lib_stktop;

#define __libc_stack \
	__thread_lib_stktop
#else /* _TLS_SUPPORT */
#ifdef _PROTOTYPES
#define __libc_stack \
((__pthread_cleanup_handler_t  ** (*) (void)) \
		 _ktepv[_KTEP_GETCANCELSTACK])()
#else /* _PROTOTYPES */
#define __libc_stack \
		 _ktepv[_KTEP_GETCANCELSTACK]()
#endif /* _PROTOTYPES */
#endif /* _TLS_SUPPORT */

#ifdef __LP64__

#define __thread_push(func,x,y) \
  if (!_KTHREADS) goto y; \
  else __pth_cleanup_push((__thread_cleanup_t)func,x,__libc_stack) y:

#define __thread_pop(x,y) \
	if (!_KTHREADS) goto y; pthread_cleanup_pop(x) y:

#define __mutex_pop(x,y)	__thread_pop(x,y)
#define __mutex_push(x,y)	__thread_push(__thread_mutex_unlock,x,y)

#define __mutex_push_file(x,y) \
  if ((!_KTHREADS)||(!x)) goto y; \
  else __pth_cleanup_push((__thread_cleanup_t)__thread_mutex_unlock,x,__libc_stack) y:
#define __mutex_pop_file(x,y,z) \
	if ((!_KTHREADS)||(!x)) goto z; pthread_cleanup_pop(y) z:

#else /* __LP64__ 32bit version of the macros*/

#ifndef _PROTOTYPES
#define __name(x) x
#endif

#ifdef _PROTOTYPES

#ifndef __CMA_UNSUPPORTED
#define __thread_push(func,x,y) \
	if (!__ismt) goto y; \
	if (_KTHREADS) goto y##_UnIqUe; \
	cma_cleanup_push(func,x) goto y;\
	y##_UnIqUe: \
         __pth_cleanup_push((__thread_cleanup_t)func,x,__libc_stack) y: 
#else /* __CMA_UNSUPPORTED */
#define __thread_push(func,x,y) \
	if (!_KTHREADS) goto y; \
         __pth_cleanup_push((__thread_cleanup_t)func,x,__libc_stack) y: 
#endif /* !__CMA_UNSUPPORTED */

#else /* !_PROTOTYPES */

#ifndef __CMA_UNSUPPORTED
#define __thread_push(func,x,y) \
	if (!__ismt) goto y; \
	if (_KTHREADS) goto __name(y)/**/_UnIqUe; \
	cma_cleanup_push(func,x) goto y;\
	__name(y)/**/_UnIqUe:  \
        __pth_cleanup_push((__thread_cleanup_t)func,x,__libc_stack) y: 
#else /* __CMA_UNSUPPORTED */
#define __thread_push(func,x,y) \
	if (!_KTHREADS) goto y; \
        __pth_cleanup_push((__thread_cleanup_t)func,x,__libc_stack) y: 
#endif /* !__CMA_UNSUPPORTED */

#endif /* PROTOTYPES */

#define __mutex_push(x,y)	__thread_push(__thread_mutex_unlock,x,y)

#ifndef __CMA_UNSUPPORTED
#define __thread_pop(x,y) \
	if (!__ismt) goto y; \
	if (_KTHREADS) {pthread_cleanup_pop(x) if (_KTHREADS) goto y;} \
	cma_cleanup_pop(x) y:
#else /* __CMA_UNSUPPORTED */
#define __thread_pop(x,y) \
	if (!_KTHREADS) goto y; \
	{pthread_cleanup_pop(x) if (_KTHREADS) goto y;} \
	y:
#endif /* !__CMA_UNSUPPORTED */

#define __mutex_pop(x,y)	__thread_pop(x,y)


/*
 *  Push and pop for file locks.
 */



#ifdef _PROTOTYPES

#ifndef __CMA_UNSUPPORTED 
#define __mutex_push_file(x,y) \
  if ((!__ismt)||(!x)) goto y; \
  if (_KTHREADS) goto y##_UnIqUe; \
  cma_cleanup_push(__thread_mutex_unlock,x) goto y; \
  y##_UnIqUe:  \
        __pth_cleanup_push((__thread_cleanup_t)__thread_mutex_unlock,x,__libc_stack) y: 
#else /* __CMA_UNSUPPORTED */
#define __mutex_push_file(x,y) \
  if ((!_KTHREADS)||(!x)) goto y; \
        __pth_cleanup_push((__thread_cleanup_t)__thread_mutex_unlock,x,__libc_stack) y: 
#endif /* !__CMA_UNSUPPORTED */

#else /* ! _PROTOTYPES */

#ifndef __CMA_UNSUPPORTED
#define __mutex_push_file(x,y) \
  if ((!__ismt)||(!x)) goto y; \
  if (_KTHREADS) goto __name(y)/**/_UnIqUe; \
  cma_cleanup_push(__thread_mutex_unlock,x) goto y; \
  __name(y)/**/_UnIqUe:  \
        __pth_cleanup_push((__thread_cleanup_t)__thread_mutex_unlock,x,__libc_stack) y: 
#else /* __CMA_UNSUPPORTED */
#define __mutex_push_file(x,y) \
  if ((!_KTHREADS)||(!x)) goto y; \
        __pth_cleanup_push((__thread_cleanup_t)__thread_mutex_unlock,x,__libc_stack) y: 
#endif /* !__CMA_UNSUPPORTED */

#endif /* _PROTOTYPES */

#ifndef __CMA_UNSUPPORTED
#define __mutex_pop_file(x,y,z) \
	if ((!__ismt)||(!x)) goto z; \
	if (_KTHREADS) {pthread_cleanup_pop(y) if (_KTHREADS) goto z;} \
	cma_cleanup_pop(y) z:
#else /* __CMA_UNSUPPORTED */
#define __mutex_pop_file(x,y,z) \
	if ((!_KTHREADS)||(!x)) goto z; \
	{pthread_cleanup_pop(y) if (_KTHREADS) goto z;} \
	z:
#endif /* !__CMA_UNSUPPORTED */

#endif /* __LP64__ */

/*
 * Recursive mutex macros: for backward source compatibility.
 */

#ifdef __CMA_UNSUPPORTED
#define _rec_mutex_alloc(x)	(_KTHREADS ? __libc_mutex_alloc(x) : 0)
#else /* !__CMA_UNSUPPORTED */
#define _rec_mutex_alloc(x)	(__ismt ? __libc_mutex_alloc(x) : 0)
#endif /* __CMA_UNSUPPORTED */

#define _rec_mutex_free(x)	__mutex_free(x)
#define _rec_mutex_init(x)	__rec_mutex_init(x)
#define _rec_mutex_lock(x) 	__mutex_lock(x)	
#define _rec_mutex_unlock(x) 	__mutex_unlock(x)

#define _rec_mutex_push(x,y) 	__mutex_push(x,y) 

#define _rec_mutex_pop(x,y) 	__mutex_pop(x,y) 

#define _rec_mutex_push_file(x,y) 	__mutex_push_file(x,y) 

#define _rec_mutex_pop_file(x,y,z) 	__mutex_pop_file(x,y,z)

#ifdef __CMA_UNSUPPORTED
#define _rec_mutex_trylock(x)	(_KTHREADS ? __libc_mutex_trylock(x) : 1)
#else /* !__CMA_UNSUPPORTED */
#define _rec_mutex_trylock(x)	(__ismt ? __libc_mutex_trylock(x) : 1)
#endif /* __CMA_UNSUPPORTED */

/* __libc_mutex_trylock() maintains compatibility with the old style
 * behavior where 1 is returned for a successful lock.
 */

/*  Cancellation type/state related macros */

#define __THREAD_CANCEL_DISABLE 	0
#define __THREAD_CANCEL_ENABLE 		1

#define __THREAD_CANCEL_DEFERRED 	0
#define __THREAD_CANCEL_ASYNC 		1


/*
 * push and pop macros for file locks test for NULL pointers
 * which are used in _IODUMMY FILEX structs.
 */

#define POP_ONLY	0
#define POP_UNLOCK	1

/*
 * Actual mutex functions
 */

#  if defined(__STDC__) || defined(__cplusplus)
#    if defined(__ia64) && ! defined(_LIBC)  
      /* pragmas needed to support -B protected */  
#      pragma extern __thread_mutex_alloc, __thread_mutex_free
#      pragma extern __thread_rec_mutex_init, __thread_fast_mutex_init
#      pragma extern __thread_dbg_mutex_init, __thread_mutex_init
#      pragma extern __thread_mutex_lock, __thread_mutex_trylock
#      pragma extern __thread_mutex_unlock, __thread_mutex_destroy
#      pragma extern __thread_self, __thread_equal, __thread_delay_np
#      pragma extern __thread_key_create, __thread_getspecific
#      pragma extern __thread_setspecific, __thread_once
#      pragma extern __thread_create_default, __thread_testcancel
#      pragma extern __thread_setcancelstate, __thread_setcanceltype
#      pragma extern __thread_exit, __thread_main, __thread_cond_destroy
#      pragma extern __thread_cond_init_default, __thread_cond_wait
#      pragma extern __thread_cond_signal, __thread_cond_timedwait
#      pragma extern __thread_cond_broadcast, __thread_sigmask, __thread_atfork
#      pragma extern __libc_mutex_trylock, __libc_mutex_alloc, __thread_detach
#    endif /* __ia64 && ! _LIBC */ 


/* New thread safing API -- use these functions instead of tepv. */
extern int 	__thread_mutex_alloc(__thread_mutex_t ** );
extern int 	__thread_mutex_free(__thread_mutex_t * );

extern int 	__thread_rec_mutex_init(__thread_mutex_t * );
extern int 	__thread_fast_mutex_init(__thread_mutex_t * );
extern int 	__thread_dbg_mutex_init(__thread_mutex_t * );
extern int 	__thread_mutex_init(__thread_mutex_t * );

/*  
 * For better performance, call the __mutex_lock, __mutex_unlock and
 *  __mutex_trylock macros instead of the following three functions
 */
extern int 	__thread_mutex_lock(__thread_mutex_t *);
extern int 	__thread_mutex_trylock(__thread_mutex_t *);
extern int 	__thread_mutex_unlock(__thread_mutex_t *);

extern int 	__thread_mutex_destroy(__thread_mutex_t *);
extern __thread_t __thread_self(void);
extern int 	__thread_equal(__thread_t , __thread_t );
extern int 	__thread_delay_np(struct timespec *);
extern int 	__thread_key_create(__thread_key_t * key, 
			void (*)(void *) );

extern void *	__thread_getspecific(__thread_key_t);
extern int 	__thread_setspecific(__thread_key_t, void *);
extern int 	__thread_once(__thread_once_t *, void (*)(void));
extern int 	__thread_create_default (__thread_t *, 
			void *(*)(void *), void *);
extern int      __thread_create_stacksize (__thread_t *,
                        void *(*)(void *), void *, size_t);
extern void 	__thread_testcancel(void);
extern int 	__thread_setcancelstate(int, int *);
extern int 	__thread_setcanceltype(int, int *);


extern void __thread_exit  (__thread_addr_t );
extern int __thread_main(void);
extern int __thread_cond_destroy(__thread_cond_t *);
extern int __thread_cond_init_default (__thread_cond_t *);
extern int __thread_cond_wait  (__thread_cond_t *,__thread_mutex_t *);
extern int __thread_cond_signal(__thread_cond_t *);
extern int __thread_cond_timedwait(__thread_cond_t *, __thread_mutex_t *,
                             struct timespec *);

extern int __thread_cond_broadcast(__thread_cond_t *);
extern int __thread_sigmask( int, const sigset_t *, sigset_t *);
extern int __thread_atfork( void (*)(void), void (*)(void), void (*)(void));

extern int 	__libc_mutex_trylock(__thread_mutex_t *);
extern int 	__libc_mutex_alloc(__thread_mutex_t **);

extern int	__thread_detach(__thread_t *);
#  else /* not __STDC__ || __cplusplus */

extern int 	__thread_mutex_alloc( );
extern int 	__thread_mutex_free( );
extern int 	__thread_rec_mutex_init( );
extern int 	__thread_fast_mutex_init( );
extern int 	__thread_dbg_mutex_init();

extern int 	__thread_mutex_lock();
extern int 	__thread_mutex_trylock();
extern int 	__thread_mutex_unlock();

extern int 	__thread_mutex_destroy();
extern __thread_t __thread_self();
extern int 	__thread_equal();
extern int 	__thread_delay_np();
extern int 	__thread_key_create();

extern void *	__thread_getspecific();
extern int 	__thread_setspecific();
extern int 	__thread_once();
extern int 	__thread_create_default ();
extern int      __thread_create_stacksize ();
extern void 	__thread_testcancel();
extern int 	__thread_setcancelstate();
extern int 	__thread_setcanceltype();

extern void __thread_exit();
extern int __thread_main();
extern int __thread_cond_destroy();
extern int __thread_cond_init_default();
extern int __thread_cond_wait();
extern int __thread_cond_signal();
extern int __thread_cond_timedwait();

extern int __thread_cond_broadcast();
extern int __thread_sigmask();
extern int __thread_atfork();
extern int __thread_callback_np();

extern int 	__libc_mutex_trylock();
extern int 	__libc_mutex_alloc();
extern int	__thread_detach();
#  endif /* __STDC__ || __cplusplus */

#ifdef __cplusplus
}
#endif

#else /* ! _KERNEL_THREADS */

#ifndef __CMA_UNSUPPORTED
#define exc_pop_ctx(x)	(*_tepv[_TEP_EXC_POP_CTX])(x)
#define exc_push_ctx(x)	(*_tepv[_TEP_EXC_PUSH_CTX])(x)

/*
 * Recursive mutex definition.
 */
#include <pthread.h>
#include <threads/tepv.h>

#ifdef __cplusplus
extern "C" {
#endif

struct rec_mutex {
	pthread_t	thread_id;	/* id of thread holding the lock */
	int		count;		/* Number of outstanding locks */
        pthread_mutex_t	mutex;		/* The mutex itself */
};

typedef struct rec_mutex	REC_MUTEX;

/*
 * Recursive mutex macros
 */
#define _rec_mutex_alloc(x)	(_tepv ? _p_mutex_alloc(x) : 0)
#define _rec_mutex_init(x)	(_tepv ? _p_mutex_init(x) : 0)
#define _rec_mutex_free(x)	(_tepv ? _p_mutex_free(x) : 0)
#define _rec_mutex_lock(x)	(_tepv ? _p_mutex_lock(x) : 0)
#define _rec_mutex_unlock(x)	(_tepv ? _p_mutex_unlock(x) : 0)
#define _rec_mutex_pop(x,y) \
	if (!_tepv) goto y; pthread_cleanup_pop(x) y:
#define _rec_mutex_push(x,y) \
  if (!_tepv) goto y; else pthread_cleanup_push(_p_mutex_unlock,x) y:
/*
 * push and pop macros for file locks test for NULL pointers
 * which are used in _IODUMMY FILEX structs.
 */
#define _rec_mutex_pop_file(x,y,z) \
	if ((!_tepv)||(!x)) goto z; pthread_cleanup_pop(y) z:
#define _rec_mutex_push_file(x,y) \
  if ((!_tepv)||(!x)) goto y; else pthread_cleanup_push(_p_mutex_unlock,x) y:
#define _rec_mutex_trylock(x)	(_tepv ? _p_mutex_trylock(x) : 1)

#define POP_ONLY	0
#define POP_UNLOCK	1
/*
 * Actual mutex functions
 */
#if defined(__ia64) && ! defined(_LIBC)  
  /* pragmas needed to support -B protected */  
#  pragma extern _p_mutex_alloc, _p_mutex_init, _p_mutex_free, _p_mutex_lock
#  pragma extern _p_mutex_unlock, _p_mutex_trylock
#endif /* __ia64 && ! _LIBC */ 

#  if defined(__STDC__) || defined(__cplusplus)
extern int	_p_mutex_alloc(REC_MUTEX **);
extern int	_p_mutex_init(REC_MUTEX *);
extern int	_p_mutex_free(REC_MUTEX *);
extern int	_p_mutex_lock(REC_MUTEX *);
extern int	_p_mutex_unlock(REC_MUTEX *);
extern int	_p_mutex_trylock(REC_MUTEX *);
#  else /* not __STDC__ || __cplusplus */
extern int	_p_mutex_alloc();
extern int	_p_mutex_init();
extern int	_p_mutex_free();
extern int	_p_mutex_lock();
extern int	_p_mutex_unlock();
extern int	_p_mutex_trylock();
#  endif /* __STDC__ || __cplusplus */

#ifdef __cplusplus
}
#endif

#endif /* !__CMA_UNSUPPORTED */
#endif /* _KERNEL_THREADS */
#endif /* _REC_MUTEX_INCLUDED */

