/* @(#) $Revision: ../hdr/threads/stdio_lock.h@@/main/i80/3 $ */
/*
 * @OSF_COPYRIGHT@
 */
/*
 * Thread safe locking extenstions for stdio.h
 */

/***** COMMENTS ************************************
**  Comments about _flockfile() and _ftestfile()
**  macros.
**  These macros traverse the following logic flow:
**
**                     __ismt
**          TRUE          |      FALSE
**       ----------------------------------------
**       |                                      |
**       |                                    NULL
**      (iop)->__flag & _IOEXT
**          Is it a FILEX?
**                 |
**         TRUE    |       FALSE
**        -----------------------------------------
**        |                                       |
**      (_FILEX *)(iop)->__lock                   |
**               |                                |
**         TRUE  | FALSE               (iop)->__flag & _IODUMMY
**         --------------                 Is it a dummy file for
**         |            |                 doscan() or doprnt()?
**  __thread_mutex()   NULL                       |
**       OR                                TRUE   | FALSE
**  __libc_mutex_trylock                 --------------------
**                                       |                  |
**                                    __thread_mutex()    NULL
**                                        OR
**                                  __libc_mutex_trylock
**
******* END COMMENTS *******************************/

#ifndef	_STDIO_LOCK_INCLUDED
#define	_STDIO_LOCK_INCLUDED

#include <stdio.h>
#include <threads/rec_mutex.h>

/*
 * per-FILE lock
 */
typedef	REC_MUTEX *	filelock_t;
#if defined(__ia64) && !defined(_LIBC)
  /* pragmas needed to support -B protected */ 
#  pragma extern _stdio_buf_rmutex
#endif /* __ia64 && !_LIBC */

#ifdef __ia64
 extern REC_MUTEX	 *_stdio_buf_rmutex;
#else /* PA */
 extern REC_MUTEX	 _stdio_buf_rmutex[];
#endif /* __ia64 */

#define	_funlockfile(filelock) \
	    (__ismt ? \
		(((filelock) == (REC_MUTEX *) NULL) ? 0 : \
		(((void)__thread_mutex_unlock(filelock)), 0)) : \
	     0)

#define	_flockfile(iop)	\
(filelock_t)(__ismt ? \
	    (((iop)->__flag & _IOEXT) ? \
	      (((_FILEX *)(iop))->__lock ? \
	        (((void)__thread_mutex_lock(((_FILEX *)(iop))->__lock)), \
			        ((_FILEX *)(iop))->__lock) : \
		NULL) : \
 	      (((iop)->__flag & _IODUMMY) ? \
		NULL : \
		(((void)__thread_mutex_lock(&_stdio_buf_rmutex[(FILE *)(iop)-__iob])), \
			&_stdio_buf_rmutex[(FILE *)(iop)-__iob]))) : \
	    NULL)	

#define	_ftestfile(iop)	\
(filelock_t)(__ismt ? \
	    (((iop)->__flag & _IOEXT) ? \
	      (((_FILEX *)(iop))->__lock ? \
	        (__libc_mutex_trylock(((_FILEX *)(iop))->__lock) ? \
		  ((_FILEX *)(iop))->__lock : \
	 	  NULL) : \
		NULL) : \
	      (((iop)->__flag & _IODUMMY) ? \
		NULL : \
		(__libc_mutex_trylock(&_stdio_buf_rmutex[(FILE *)(iop)-__iob]) ? \
		  &_stdio_buf_rmutex[(FILE *)(iop)-__iob] : \
		  NULL))) : \
	    (filelock_t) 1L)

#endif /* _STDIO_LOCK_INCLUDED */
