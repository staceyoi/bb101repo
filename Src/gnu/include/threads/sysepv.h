/* @(#) $Revision: ../hdr/threads/sysepv.h@@/main/i80/3 $ */

/*
 * Offsets into system call intercept entry point vector
 */

#ifndef _SYSEPV_INCLUDED
#define _SYSEPV_INCLUDED


#define _SYSEP_ACCEPT		 0
#define _SYSEP_CLOSE		 1
#define _SYSEP_CONNECT	 	 2
#define _SYSEP_CREAT		 3
#define _SYSEP_DUP		 4
#define _SYSEP_DUP2		 5
#define _SYSEP_FCNTL		 6
#define _SYSEP_FORK		 7
#define _SYSEP_IOCTL		 8
#define _SYSEP_OPEN		 9
#define _SYSEP_PIPE		10
#define _SYSEP_READ		11
#define _SYSEP_READV		12
#define _SYSEP_RECV		13 
#define _SYSEP_RECVFROM		14 
#define _SYSEP_RECVMSG		15
#define _SYSEP_SELECT		16
#define _SYSEP_SEND		17
#define _SYSEP_SENDMSG		18
#define _SYSEP_SENDTO		19
#define _SYSEP_SETRLIMIT	20
#define _SYSEP_SIGACTION	21
#define _SYSEP_SOCKET		22
#define _SYSEP_SOCKETPAIR	23
#define _SYSEP_WRITE		24
#define _SYSEP_WRITEV		25

#define _SYSEP_MSGRCV		26	
#define _SYSEP_MSGSND		27	
#define _SYSEP_SEMOP		28	
#define _SYSEP_WAIT		29	
#define _SYSEP_WAIT3		30	
#define _SYSEP_WAITPID		31	
#define _SYSEP_VFORK		32	

#define _SYSEP_SYSTEM		33	
#define _SYSEP_SLEEP		34	
#define _SYSEP_EXECVE		35	
#define _SYSEP_GETMSG		36	
#define _SYSEP_GETPMSG		37	
#define _SYSEP_POLL		38	
#define _SYSEP_PUTMSG		39	
#define _SYSEP_PUTPMSG		40	

#define _SYSEP_RECVMSG2		41	
#define _SYSEP_SENDMSG2		42	
#define _SYSEP_SOCKET2		43	
#define _SYSEP_SOCKETPAIR2	44	
#define _SYSEP_WAITID		45	

#define _SYSEP_SETRLIMIT64	46	
#define _SYSEP_SEM_WAIT		47	
#define _SYSEP_NANOSLEEP	48	

#define _SYSEP_SIGWAIT	49	
#define _SYSEP_SIGWAITINFO	50	
#define _SYSEP_SIGTIMEDWAIT	51	

#define _SYSEP_MQ_SEND	52	
#define _SYSEP_MQ_RECEIVE	53	
#define _SYSEP_SCHED_SETSCHEDULER       54
#define _SYSEP_SCHED_SETPARAM   55

#define	_SYSEP_RECV2	56
#define	_SYSEP_RECVFROM2	57
#define	_SYSEP_SEND2	58
#define	_SYSEP_SENDTO2	59


/*
 * System call intercept entry point vector
 */
#ifdef __ia64 
#  pragma extern _sysepv
#endif /* __ia64 */ 
#ifdef __cplusplus
extern "C" {
extern int (**_sysepv)(...);
}
#else
extern int (**_sysepv)();
#endif

#endif /* _SYSEPV_INCLUDED */
