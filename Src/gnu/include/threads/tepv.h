/* @(#) $Revision: ../hdr/threads/tepv.h@@/main/i80/1 $ */
/*
 * Offsets into threads library entry point vector
 */

#ifndef _TEPV_INCLUDED
#define _TEPV_INCLUDED

#define _TEP_ACCEPT		 0
#define _TEP_CLOSE		 1
#define _TEP_CONNECT		 2
#define _TEP_CREAT		 3
#define _TEP_DUP		 4
#define _TEP_DUP2		 5
#define _TEP_FCNTL		 6
#define _TEP_FORK		 7
#define _TEP_IOCTL		 8
#define _TEP_OPEN		 9
#define _TEP_PIPE		10
#define _TEP_READ		11
#define _TEP_READV		12
#define _TEP_RECV		13
#define _TEP_RECVFROM		14
#define _TEP_RECVMSG		15
#define _TEP_SELECT		16
#define _TEP_SEND		17
#define _TEP_SENDMSG		18
#define _TEP_SENDTO		19
#define _TEP_SETRLIMIT		20
#define _TEP_SIGACTION		21
#define _TEP_SOCKET		22
#define _TEP_SOCKETPAIR		23
#define _TEP_WRITE		24
#define _TEP_WRITEV		25
#define _TEP_MUTEX_INIT		26
#define _TEP_MUTEX_DESTROY	27
#define _TEP_MUTEX_LOCK		28
#define _TEP_MUTEX_UNLOCK	29
#define _TEP_MUTEX_TRYLOCK	30
#define _TEP_SELF		31
#define _TEP_DELAY_NP		32
#define _TEP_MSGRCV		33
#define _TEP_MSGSND		34
#define _TEP_SEMOP		35
#define _TEP_WAIT		36
#define _TEP_WAIT3		37
#define _TEP_WAITPID		38
#define _TEP_VFORK		39
#define _TEP_KEYCREATE		40
#define _TEP_GETSPECIFIC	41
#define _TEP_SETSPECIFIC	42
#define _TEP_SYSTEM		43
#define _TEP_SLEEP		44
/* unused entries
 *                		45
 *                 		46
 *                 		47
 *                		48
 */
#define _TEP_EXECVE		49
/* unused entries
 *                 		50
 *				51
 *				52
 */
#define _TEP_GETMSG		53
#define _TEP_GETPMSG		54
#define _TEP_POLL		55
#define _TEP_PUTMSG		56
#define _TEP_PUTPMSG		57
#define _TEP_ONCE		58
#define _TEP_EQUAL		59
#define _TEP_SETASYNCCANCEL	60
#define _TEP_SETCANCEL  	61
#define _TEP_TESTCANCEL 	62
#define _TEP_CREATE		63
#define _TEP_SIGWAIT		64
#define _TEP_EXC_PUSH_CTX	65
#define _TEP_EXC_POP_CTX	66
#define _TEP_CTXCB_HPUX		67
#define _TEP_RECVMSG2		68
#define _TEP_SENDMSG2		69
#define _TEP_SOCKET2		70
#define _TEP_SOCKETPAIR2	71
#define _TEP_WAITID		72

#define _TEP_SETRLIMIT64	73
#define _TEP_SEM_WAIT		74
#define _TEP_NANOSLEEP		75

/*
 * Threads library entry point vector
 */
#ifdef __ia64 
#  pragma extern _tepv
#endif /* __ia64 */ 
#ifdef __cplusplus
extern "C" {
extern long (**_tepv)(...);
}
#else
extern long (**_tepv)();
#endif

#endif /* _TEPV_INCLUDED */
