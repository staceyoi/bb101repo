/*
 * (c) Copyright 1990, 1991, 1992, 1993 OPEN SOFTWARE FOUNDATION, INC. 
 * ALL RIGHTS RESERVED 
 */
/*
 * OSF/1 1.2
 */

/* @(#)$RCSfile: ts_supp.h,v $ $Revision: 72.2 $ (OSF) $Date: 93/12/01 15:50:23 $ */

/*
 * Macros for thread safe work.
 *
 * The idea is to make the shared library code easier to read
 * and maintain by avoiding the loathsome #ifdef.
 * Sometimes this works.
 */

#ifndef _TS_SUPP_H_
#define	_TS_SUPP_H_

#if defined(_THREAD_SAFE) || defined(_REENTRANT)

#include <osf_hp.h>
#include <threads/rec_mutex.h>

#include	<errno.h>

#define	TS_LOCK(lock)		_rec_mutex_lock(lock)
#define	TS_TRYLOCK(lock)	_rec_mutex_trylock(lock)
#define	TS_UNLOCK(lock)		_rec_mutex_unlock(lock)

#define	TS_FDECLARELOCK(lock)	filelock_t lock;
#define	TS_FLOCK(lock, iop)	(lock = _flockfile(iop))
#define	TS_FTRYLOCK(lock, iop)	(lock = _ftestfile(iop))
#define	TS_FUNLOCK(lock)	_funlockfile(lock)

#define	TS_EINVAL(arg)		if (arg) return (_Seterrno(EINVAL), -1)
#define	TS_ERROR(e)		_Seterrno(e)
#define	TS_RETURN(ts, nts)	return (ts)

#define	TS_SUCCESS		0
#define	TS_FAILURE		-1
#define	TS_FOUND(ret)		(TS_SUCCESS)
#define	TS_NOTFOUND		(_Seterrno(ESRCH), TS_FAILURE)

#else

#define	TS_LOCK(lock)
#define	TS_TRYLOCK(lock)	1
#define	TS_UNLOCK(lock)

#define	TS_FDECLARELOCK(lock)
#define	TS_FLOCK(lock, iop)
#define	TS_FTRYLOCK(lock, iop)	1
#define	TS_FUNLOCK(lock)

#define	TS_EINVAL(arg)
#define	TS_ERROR(e)
#define	TS_RETURN(ts, nts)	return (nts)

#define	TS_SUCCESS		1
#define	TS_FAILURE		0
#define	TS_FOUND(ret)		(ret)
#define	TS_NOTFOUND		(TS_FAILURE)

#endif	/* _THREAD_SAFE || _REENTRANT */

#endif	/* _TS_SUPP_H_ */
