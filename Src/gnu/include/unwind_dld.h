#ifndef _unwind_dld_h_
#define _unwind_dld_h_

/* For size_t */
#include <sys/types.h>

#ifdef HP_IA64
#include "langtypes.h"
#endif

/* For struct load_module_desc and dlmodinfo prototype: */
#include <dlfcn.h>

/*  Note that the unwind_header structure is also defined in:
**  /CLO/Products/PHOENIX/Src/linker/Contributions/unwind_hdr_con.h
*/
#ifdef HP_IA64
struct unwind_header {
    UInt64  unwind_header_version;      /* Version of this header   */
    UInt64  unwind_start;               /* Start of unwind table    */
    UInt64  unwind_end;                 /* End of unwind table      */
};
#endif
enum { UNWIND_HDR_VERSION=1 };  /* Current version of header */

#endif /* _unwind_dld_h_ */
