/* Sorting algorithms.
   Copyright (C) 2000 Free Software Foundation, Inc.
   Contributed by Mark Mitchell <mark@codesourcery.com>.

This file is part of GNU CC.
   
GNU CC is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU CC is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU CC; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "libiberty.h"
#include "sort.h"
#include <limits.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifndef UCHAR_MAX
#define UCHAR_MAX ((unsigned char)(-1))
#endif

/* POINTERS and WORK are both arrays of N pointers.  When this
   function returns POINTERS will be sorted in ascending order.  */

void sort_pointers (size_t n, void **pointers, void **work)
{
  /* The type of a single digit.  This can be any unsigned integral
     type.  When changing this, DIGIT_MAX should be changed as 
     well.  */
  typedef unsigned char digit_t;

  /* The maximum value a single digit can have.  */
#define DIGIT_MAX (UCHAR_MAX + 1)

  /* The Ith entry is the number of elements in *POINTERSP that have I
     in the digit on which we are currently sorting.  */
  unsigned int count[DIGIT_MAX];
  /* Nonzero if we are running on a big-endian machine.  */
  int big_endian_p;
  size_t i;
  size_t j;

  /* The algorithm used here is radix sort which takes time linear in
     the number of elements in the array.  */

  /* The algorithm here depends on being able to swap the two arrays
     an even number of times.  */
  if ((sizeof (void *) / sizeof (digit_t)) % 2 != 0)
    abort ();

  /* Figure out the endianness of the machine.  */
  for (i = 0, j = 0; i < sizeof (size_t); ++i)
    {
      j *= (UCHAR_MAX + 1);
      j += i;
    }
  big_endian_p = (((char *)&j)[0] == 0);

  /* Move through the pointer values from least significant to most
     significant digits.  */
  for (i = 0; i < sizeof (void *) / sizeof (digit_t); ++i)
    {
      digit_t *digit;
      digit_t *bias;
      digit_t *top;
      unsigned int *countp;
      void **pointerp;

      /* The offset from the start of the pointer will depend on the
	 endianness of the machine.  */
      if (big_endian_p)
	j = sizeof (void *) / sizeof (digit_t) - i;
      else
	j = i;
	
      /* Now, perform a stable sort on this digit.  We use counting
	 sort.  */
      memset (count, 0, DIGIT_MAX * sizeof (unsigned int));

      /* Compute the address of the appropriate digit in the first and
	 one-past-the-end elements of the array.  On a little-endian
	 machine, the least-significant digit is closest to the front.  */
      bias = ((digit_t *) pointers) + j;
      top = ((digit_t *) (pointers + n)) + j;

      /* Count how many there are of each value.  At the end of this
	 loop, COUNT[K] will contain the number of pointers whose Ith
	 digit is K.  */
      for (digit = bias; 
	   digit < top; 
	   digit += sizeof (void *) / sizeof (digit_t))
	++count[*digit];

      /* Now, make COUNT[K] contain the number of pointers whose Ith
	 digit is less than or equal to K.  */
      for (countp = count + 1; countp < count + DIGIT_MAX; ++countp)
	*countp += countp[-1];

      /* Now, drop the pointers into their correct locations.  */
      for (pointerp = pointers + n - 1; pointerp >= pointers; --pointerp)
	work[--count[((digit_t *) pointerp)[j]]] = *pointerp;

      /* Swap WORK and POINTERS so that POINTERS contains the sorted
	 array.  */
      pointerp = pointers;
      pointers = work;
      work = pointerp;
    }
}

/* msort
 *  merge sort routine with the same signature as qsort for convenient
 *   replacement.  The reason to use a merge sort is if you need the sort
 *   to be stable, i.e., records with equal keys remain in the original order.
 *   qsort is not stable.
 *
 * msort allocates a region of memory large enough to hold a copy of the
 *	data.  It starts out with regions of 1 record each.  It merges back
 *	and forth between the two data buffers, merging adjacent regions
 *	(note that the last region might not have a partner for a merge pass
 *      or might have a short partner).
 *	At the end, if the data is in the extra memory, it is copied to
 *	the original data area and the extra memory is freed.
 *
 *	If malloc fails, an error message is printed and exit is called.
 */

void 
msort (void *base,
       size_t nel,
       size_t size,
       int (*compar)(const void *, const void *))
{
  /* DIAGRAM
   *  Merging adjacent regions of size sorted_size from region_a to region_b
   *  The smaller of *from_1 and *from_2 is copied to to_ptr, modulo
   *  limit_1 and limit_2.  When from_1 and from_2 are exhausted, to_ptr
   *  will point at to_limit and we will go on to merge the next two areas.
   *
   *	       region_a                                 region_b
   *	^
   *	|
   *	| area before from_1 already 
   *	| copied, adjacent regions merged
   *	| in corresponding area in region_b
   *	|
   *	V
   *	from_1->
   *                                                   to_ptr->
   *	limit_1->
   *
   *	from_2->
   *     
   *	limit_2->                                      to_limit-> same offset
   *	.							  as limit_2
   *	.
   *	.
   *	.
   *    limit_a->  points to just past region_a
   */
  char *	from_1;
  char *	from_2;
  char *	limit_1;
  char *	limit_2;
  char *	limit_a;
  char *	orig_region;
  char *	region_a;
  char *	region_b;
  size_t	sorted_size;
  char *	temp_ptr;
  char *	temp_region;
  char *	to_limit;
  char *	to_ptr;
  size_t	total_size;

  orig_region = (char*) base;
  total_size = nel * size;
  temp_region = malloc (total_size);
  if (!temp_region)
    {
      fprintf (stderr, "virtual memory exhausted\n");
	exit (1);
    }
  region_a = orig_region;
  region_b = temp_region;
  sorted_size = size;

  /* Each iteration of this loop, doubles the size of the sorted regions
   * which startout at 1 element, i.e. sorted_size == size.
   */
  while (sorted_size < total_size)
    {
      limit_a = region_a + total_size;
      to_ptr = region_b;
      from_1 = region_a;

      while (from_1 < limit_a)
	{ 
	  /* Merge the region from_1 and the following region, placing
	   * the result at to_ptr 
	   */

	  limit_1 = from_2 = from_1 + sorted_size;
	  if (limit_1 >= limit_a)
	    {
	      limit_1 = from_2 = limit_2 = limit_a;
	    }
	  else
	    {
	      limit_2 = from_2 + sorted_size;
	      if (limit_2 > limit_a)
		limit_2 = limit_a;
	    }

	  to_limit = to_ptr + (limit_1 - from_1) + (limit_2 - from_2);

	  while (to_ptr < to_limit)
	    {
	      if (from_1 >= limit_1)
		{
		  memcpy (to_ptr, from_2, size);
		  from_2 += size;
		}
	      else if (from_2 >= limit_2)
		{
		  memcpy (to_ptr, from_1, size);
		  from_1 += size;
		}
	      else if ( (*compar)((void*) from_1, (void *) from_2) > 0)
		{ /* from_2 goes before from_1 */
		  memcpy (to_ptr, from_2, size);
		  from_2 += size;
		}
	      else
		{ /* less than or equal, choose from_1 to make stable */
		  memcpy (to_ptr, from_1, size);
		  from_1 += size;
		}
	      to_ptr += size;
	    }

	  from_1 = limit_2;  /* prepare to merge the next pair of regions */
	} // merge and copy regions untill all copied

      /* We have copied all of  region_a to region_b.  Prepare to
       * go in the other direction.  If we fall out, region_a is sorted.
       */

      temp_ptr = region_a;
      region_a = region_b;
      region_b = temp_ptr;
      sorted_size = 2 * sorted_size;
    } // do merges until the entire buffer is sorted

    /* region_a is now sorted.  We need the sorted data to be 
     * in orig_region.
     */
    
    if (region_a != orig_region)
      memcpy (orig_region, region_a, total_size);

    free (temp_region);  // We are done with this buffer

} // end msort

/* Everything below here is a unit test for the routines in this
   file.  */

#ifdef UNIT_TEST

#include <stdio.h>

void *xmalloc (n)
     size_t n;
{
  return malloc (n);
}

int main (int argc, char **argv)
{
  int k;
  int result;
  size_t i;
  void **pointers;
  void **work;

  if (argc > 1)
    k = atoi (argv[1]);
  else
    k = 10;

  pointers = xmalloc (k * sizeof (void *));
  work = xmalloc (k * sizeof (void *));

  for (i = 0; i < k; ++i)
    {
      pointers[i] = (void *) random ();
      printf ("%x\n", pointers[i]);
    }

  sort_pointers (k, pointers, work);

  printf ("\nSorted\n\n");

  result = 0;

  for (i = 0; i < k; ++i)
    {
      printf ("%x\n", pointers[i]);
      if (i > 0 && (char*) pointers[i] < (char*) pointers[i - 1])
	result = 1;
    }

  free (pointers);
  free (work);

  return result;
}

#endif
