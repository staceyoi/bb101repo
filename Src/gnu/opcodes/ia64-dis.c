/* Disassembler for the PA-RISC. Somewhat derived from sparc-pinsn.c.
   Copyright 1989, 1990, 1992, 1993 Free Software Foundation, Inc.

   Contributed by the Center for Software Science at the
   University of Utah (pa-gdb-bugs@cs.utah.edu).

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

/* Define this name if you want to restrict the
 * disassembler to host-native formats.
 */
/* #define LOCAL_ONLY 1 */ 
/* ia64-gambit-libdas: carrying over Bob's changes */
#include "libdas.h"
#include "libsym.h" 
/* ia64-gambit-libdas */

#include <ansidecl.h>
#include "sysdep.h"
#include "dis-asm.h"

unsigned long long curr_pc;

extern void strcat_to_buf_with_fmt (char *, int, char *, ...);  /* from gdb/tui/tui.h */
#define fprintf_func(_info, _fmt, _buf, _len, _args)                 \
               (_buf != (char *)NULL && _len > 0) ?                  \
                 strcat_to_buf_with_fmt(_buf, _len, _fmt, _args) :   \
                 (void) (*_info->fprintf_func)(_info->stream, _fmt, _args)

extern void 
symAddrtoName (unsigned long long adr, char sname[], 
	       int rmdr, int add0x, int width);

unsigned long long 
dasAddress(void)
{
  return curr_pc & ~((long long)3);
}

unsigned int
dasRelocation(unsigned long long addr, DasRelocation * reloc)
{
  return 0;
}


/* From Michael Morrell on 19 Apr 2000:  I was trying some new stuff and 
   symAddrtoDefFunc was my current solution.  It's purpose is to limit the 
   matching symbols -- symAddrtoName will return the first name in the 
   symbol table that is "close" to the passed address, but symAddrtoDefFunc 
   will only consider symbols that are of type STT_FUNC and whose 
   shndx != SHN_UNDEF.  This can be important when this is used to 
   disassemble .o files.  You can just define it to call symAddrtoName 
   (the arguments are identical)
*/

void symAddrtoDefFunc(unsigned long long adr,
                      char sname[],
                      int rmdr,
                      int add0x,
                      int width)
{
    symAddrtoName (adr, sname, rmdr, add0x, width);
}

/* Print or concatenate one instruction.  If BUF is non-NULL and BUFLEN
   is > 0, then the instruction will be concatenated in the input buffer 
   rather than printed.
 */

int
printOrStrcat_insn_ia64 (bfd_vma memaddr,
                         disassemble_info *info,
                         char *buf,
                         int  buflen)
{
  bfd_byte buffer[16];
  Bundle b;
  char s1[255], s2[255], s3[255];
  unsigned int insn, i;
  int slot;
  static int das_initialized = 0;

  if (!das_initialized)
    {
      dasInit(DasTemplate | DasTemplateCommas | DasPseudoOps | DasRegNames, 78);
      das_initialized = 1;
    }

  /* Get the instruction to disassemble.
   */
  {
    int status =
      (*info->read_memory_func) (memaddr & ~0x3, buffer, sizeof (buffer), info);
    if (status != 0)
      {
	(*info->memory_error_func) (status, memaddr & ~0x3, info);
	return -1;
      }
  }
/* ia64-gambit-libdas */
  curr_pc = memaddr;

  memcpy (&b, buffer, sizeof(b));

/* Temporary hack.  There should be a new library and .h file that defines
 * dasBundle to call dasBundleSBT which has an extra parameter to return
 * template information (which can be NULL if you don't want it.)
 */
  dasBundle (&b, s1, s2, s3);
  slot = (int)(memaddr & 0x3);
  if (slot == 0)
    {
      fprintf_func(info, "%s",buf, buflen, s1);
      return 1;
    }
  if (slot == 1)
    {
      fprintf_func(info, "%s", buf, buflen, s2);
      return 1;
    }
  if (slot == 2)
    {
      fprintf_func(info, "%s", buf, buflen, s3);
      return 14;       /* 16 bytes disassembled */
    }
  /* redundant return to silence aCC6 warning - this box only has 3 slots. */
  return NULL;
/* ia64-gambit-libdas */
}

/* Concatenate one instruction.  */
int
strcat_insn_ia64 (bfd_vma memaddr,
                  disassemble_info *info,
                  char *buf,
                  int buflen)
{
  return printOrStrcat_insn_ia64 (memaddr, info, buf, buflen);
}

/* Print one instruction.  */
int
print_insn_ia64 (bfd_vma memaddr,
                 disassemble_info *info)
{
  return printOrStrcat_insn_ia64 (memaddr, info, (char *)NULL, 0);
}
